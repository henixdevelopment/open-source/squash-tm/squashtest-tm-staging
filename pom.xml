<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>org.squashtest.tm</groupId>
  <artifactId>squash-tm</artifactId>
  <version>${revision}${sha1}${changelist}</version>
  <packaging>pom</packaging>

  <name>squash-tm</name>
  <description>Squash Test Manager - Test repository management application</description>

  <modules>
    <!-- bom is used almost everywhere, we build it early -->
    <module>bom</module>
    <!-- tools are used by the actual app, we build it early -->
    <module>tools</module>
    <module>database.h2.fragment</module>
    <module>database</module>
    <module>core</module>
    <module>tm</module>
    <module>plugins</module>
  </modules>

  <properties>

    <!-- aspectj version - should be syncd w/ spring boot version -->
    <!-- yet we're stuck at 1.8.5 - won't compile with newer versions -->
    <!-- JTH : Migration java 11 : I upgraded to new version, and fixed the compilation problem. -->
    <aspectj.version>1.9.19</aspectj.version>

    <!--  Last part of variable version number, mainly used to publish snapshots  -->
    <changelist>-SNAPSHOT</changelist>

    <groovy-eclipse-batch.version>3.0.10-02</groovy-eclipse-batch.version>
    <groovy-eclipse-compiler.version>3.7.0</groovy-eclipse-compiler.version>

    <groovy.version>3.0.22</groovy.version> <!-- 3.0.19 in Spring Boot -->
    <jasperreports.version>6.21.3</jasperreports.version>

    <!-- Overrided versions -->
    <h2.version>2.3.230</h2.version> <!-- 2.1.214 in Spring Boot -->
    <liquibase.version>4.19.0</liquibase.version> <!-- 4.9.1 in Spring Boot -->

    <gmavenplus.version>1.13.0</gmavenplus.version>
    <java.version>17</java.version>

    <maven-failsafe-plugin.version>3.3.1</maven-failsafe-plugin.version>
    <maven-surefire-plugin.version>3.3.1</maven-surefire-plugin.version>

    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

    <!-- Compose the version number with ${revision}${sha1}${changelist}. can be overridden by command line arguments on CI-->
    <!-- ${revision} is the main version number  -->
    <revision>10.0.0</revision>
    <!--  ${sha1} is the qualifier like IT, RC part  -->
    <sha1/>

    <sonar.exclusions>
      **/*.sql,
      **/database/src/main/dbdoc/dbdoc.html,
      **/jasperreports/processors/CustomHtmlProcessorFactory.java,
      **/liquibase/core/*,
      **/liquibase/dev/*,
      **/liquibase/tm/*,
      **/liquibase/upgrade/*,
      **/org/springframework/**,
      **/scripts/ckeditor/**,
      **/scripts/datatables/**,
      **/scripts/jquery/**,
      **/scripts/lib/**,
      **/scripts/lib/**,
      **/scripts/require-min.js,
      **/scripts/require.js,
      **/scripts/sinon.js,
      **/scripts/text.js,
      **/sqtm-core/src/lib/**,
      **/styles/bootstrap*,
      **/styles/ckeditor/**,
      **/styles/images/**,
      **/styles/jqplot/**,
      **/styles/jquery/**,
      **/styles/spectrum/**,
      **/styles/switchButton/**,
      **/tm-front/dist/**,
      **/tm-front/projects/sqtm-app/src/assets/js/**,
      **/tm-front/projects/sqtm-core/src/assets/sqtm-core/js/**,
      **/tm.service/src/test/groovy/org/squashtest/tm/internal/test/**,
      **/tm/tm-front/coverage/**,
      **/tm/tm-front/cypress/**,
      **/webapp/js/**
    </sonar.exclusions>
    <sonar.coverage.exclusions>
      *.js,
      **/*.js,
      **/sqtm-core/**/*.ts,
      **/tm-front/**/*.less,
      **/tm-front/**/*.scss,
      **/tm-front/**/*.spec.ts
      **/tm-front/**/*.logger.ts
      **/tm-front/**/*.html
      **/tm-front/cypress/**
    </sonar.coverage.exclusions>
    <sonar.javascript.lcov.reportPaths>**/lcov.info</sonar.javascript.lcov.reportPaths>
    <!--
      Sonar properties. It is unfortunate, but the sonar-maven-plugin won't read its configuration properties elsewhere.
    -->
    <sonar.scm.disabled>true</sonar.scm.disabled>
    <spring-boot-maven-plugin.version>3.3.2</spring-boot-maven-plugin.version>
    <spring.boot.version>2.7.18</spring.boot.version>

    <verbose.compiler>false</verbose.compiler>
    <wro4j.version>1.7.8</wro4j.version>

    <!-- MAVEN PLUGINS -->
    <apt-maven-plugin.version>1.1.3</apt-maven-plugin.version>
    <aspectj-maven-plugin.version>1.15.0</aspectj-maven-plugin.version>
    <axistools-maven-plugin.version>1.4</axistools-maven-plugin.version>
    <build-helper-maven-plugin.version>3.6.0</build-helper-maven-plugin.version>
    <ci-friendly.version>1.0.20</ci-friendly.version>
    <extra-enforcer-rules.version>1.8.0</extra-enforcer-rules.version>
    <gmaven-plugin.version>1.5</gmaven-plugin.version>
    <google-java-format.version>1.22.0</google-java-format.version>
    <jasperreports-plugin.version>2.8</jasperreports-plugin.version>
    <license-maven-plugin.version>4.5</license-maven-plugin.version>
    <maven-compiler-plugin.version>3.13.0</maven-compiler-plugin.version>
    <maven-enforcer-plugin.version>3.5.0</maven-enforcer-plugin.version>
    <maven-jar-plugin.version>3.4.2</maven-jar-plugin.version>
    <maven-release-plugin.version>3.1.1</maven-release-plugin.version>
    <maven-remote-resources-plugin.version>3.2.0</maven-remote-resources-plugin.version>
    <maven-resources-plugin.version>3.3.1</maven-resources-plugin.version>
    <maven-sortpom-plugin.version>4.0.0</maven-sortpom-plugin.version>
    <spotless-maven-plugin.version>2.43.0</spotless-maven-plugin.version>
    <xml-maven-plugin.version>1.1.0</xml-maven-plugin.version>

    <xml-apis.version>1.4.01</xml-apis.version>
  </properties>

  <pluginRepositories>
    <pluginRepository>
      <id>groovy-plugins-release</id>
      <url>https://groovy.jfrog.io/artifactory/plugins-release</url>
    </pluginRepository>
  </pluginRepositories>
  <build>

    <!--
      This POM should only declare plugin mgt and plugins.
      It should not declare dependency mgt (they should be in squash-tm-bom)
      It should not declare dependencies
      If you need any of these plgins in a submodule, simply declare it in the plugins section without version. You
      probably don't need to reconfigure the plugin either.
    -->
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-compiler-plugin</artifactId>
          <version>${maven-compiler-plugin.version}</version>
          <configuration>
            <!-- Std java compiler plugin is configured to compile groovy using groovy-eclipse-compiler -->
            <source>${java.version}</source>
            <target>${java.version}</target>
            <compilerId>groovy-eclipse-compiler</compilerId>
          </configuration>
          <dependencies>
            <dependency>
              <groupId>org.codehaus.groovy</groupId>
              <artifactId>groovy-eclipse-batch</artifactId>
              <version>${groovy-eclipse-batch.version}</version>
            </dependency>
            <dependency>
              <groupId>org.codehaus.groovy</groupId>
              <artifactId>groovy-eclipse-compiler</artifactId>
              <version>${groovy-eclipse-compiler.version}</version>
            </dependency>
          </dependencies>
        </plugin>

        <plugin>
          <groupId>org.codehaus.mojo</groupId>
          <artifactId>aspectj-maven-plugin</artifactId>
          <version>${aspectj-maven-plugin.version}</version>
          <configuration>
            <Xlint>ignore</Xlint>
            <source>${java.version}</source>
            <target>${java.version}</target>
            <complianceLevel>${java.version}</complianceLevel>
          </configuration>
          <dependencies>
            <dependency>
              <groupId>org.aspectj</groupId>
              <artifactId>aspectjtools</artifactId>
              <version>${aspectj.version}</version>
            </dependency>
          </dependencies>
          <executions>
            <execution>
              <goals>
                <goal>compile</goal>
                <goal>test-compile</goal>
              </goals>
            </execution>
          </executions>
        </plugin>

        <plugin>
          <!-- Configures groovy sources location -->
          <groupId>org.codehaus.mojo</groupId>
          <artifactId>build-helper-maven-plugin</artifactId>
          <version>${build-helper-maven-plugin.version}</version>
          <executions>
            <execution>
              <id>add-source</id>
              <goals>
                <goal>add-source</goal>
              </goals>
              <phase>generate-sources</phase>
              <configuration>
                <sources>
                  <source>src/main/groovy</source>
                </sources>
              </configuration>
            </execution>
            <execution>
              <id>add-test-source</id>
              <goals>
                <goal>add-test-source</goal>
              </goals>
              <phase>generate-test-sources</phase>
              <configuration>
                <sources>
                  <source>src/test/groovy</source>
                </sources>
              </configuration>
            </execution>
          </executions>
        </plugin>

        <plugin>
          <!-- Mostly the same as surefire, but used for integration tests -->
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-failsafe-plugin</artifactId>
          <version>${maven-failsafe-plugin.version}</version>
        </plugin>

        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-surefire-plugin</artifactId>
          <version>${maven-surefire-plugin.version}</version>
          <configuration>
            <systemPropertyVariables>
              <property>
                <name>log4j.configuration</name>
                <value>test-log4j.properties</value>
              </property>
            </systemPropertyVariables>
          </configuration>
        </plugin>

        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-resources-plugin</artifactId>
          <version>${maven-resources-plugin.version}</version>
          <configuration>
            <nonFilteredFileExtensions>
              <nonFilteredFileExtension>ico</nonFilteredFileExtension>
              <nonFilteredFileExtension>zip</nonFilteredFileExtension>
            </nonFilteredFileExtensions>
          </configuration>
        </plugin>

        <plugin>
          <groupId>org.liquibase</groupId>
          <artifactId>liquibase-maven-plugin</artifactId>
          <version>${liquibase.version}</version>
          <dependencies>
            <dependency>
              <groupId>org.squashtest.tm</groupId>
              <artifactId>database.h2.fragment</artifactId>
              <version>${project.version}</version>
            </dependency>
            <dependency>
              <groupId>org.liquibase</groupId>
              <artifactId>liquibase-core</artifactId>
              <version>${liquibase.version}</version>
            </dependency>
          </dependencies>
        </plugin>

        <plugin>
          <groupId>org.codehaus.mojo</groupId>
          <artifactId>xml-maven-plugin</artifactId>
          <version>${xml-maven-plugin.version}</version>
        </plugin>

        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-release-plugin</artifactId>
          <version>${maven-release-plugin.version}</version>
        </plugin>

        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-remote-resources-plugin</artifactId>
          <version>${maven-remote-resources-plugin.version}</version>
        </plugin>

        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-jar-plugin</artifactId>
          <version>${maven-jar-plugin.version}</version>
        </plugin>

        <plugin>
          <!-- wro4j processes some web assets such as less CSS -->
          <groupId>ro.isdc.wro4j</groupId>
          <artifactId>wro4j-maven-plugin</artifactId>
          <version>${wro4j.version}</version>
          <dependencies>
            <dependency>
              <groupId>ro.isdc.wro4j</groupId>
              <artifactId>wro4j-extensions</artifactId>
              <version>${wro4j.version}</version>
            </dependency>
            <dependency>
              <!-- without this, old 1.3 version may be pulled outta nowhere and reports wont compile -->
              <groupId>xml-apis</groupId>
              <artifactId>xml-apis</artifactId>
              <version>${xml-apis.version}</version>
            </dependency>
          </dependencies>
        </plugin>

        <plugin>
          <groupId>com.mysema.maven</groupId>
          <artifactId>apt-maven-plugin</artifactId>
          <version>${apt-maven-plugin.version}</version>
        </plugin>

        <plugin>
          <groupId>com.alexnederlof</groupId>
          <artifactId>jasperreports-plugin</artifactId>
          <version>{jasperreports-plugin.version}</version>
          <dependencies>
            <dependency>
              <!-- required to make sure jr runtime version is in sync with compile-time version -->
              <groupId>net.sf.jasperreports</groupId>
              <artifactId>jasperreports</artifactId>
              <version>${jasperreports.version}</version>
              <exclusions>
                <exclusion>
                  <!-- without this, old 1.3 version may be pulled outta nowhere and warious problems occur -->
                  <groupId>xml-apis</groupId>
                  <artifactId>xml-apis</artifactId>
                </exclusion>
              </exclusions>
            </dependency>
            <dependency>
              <!-- without this, old 1.3 version may be pulled outta nowhere and reports wont compile -->
              <groupId>xml-apis</groupId>
              <artifactId>xml-apis</artifactId>
              <version>${xml-apis.version}</version>
            </dependency>
          </dependencies>
          <executions>
            <execution>
              <goals>
                <goal>jasper</goal>
              </goals>
              <phase>process-sources</phase>
            </execution>
          </executions>
        </plugin>

        <plugin>
          <groupId>org.springframework.boot</groupId>
          <artifactId>spring-boot-maven-plugin</artifactId>
          <version>${spring-boot-maven-plugin.version}</version>
        </plugin>

        <plugin>
          <!-- This is used to run groovy scripts during the build (not to compile groovy sources, see maven-compiler-plugin) -->
          <groupId>org.codehaus.gmaven</groupId>
          <artifactId>gmaven-plugin</artifactId>
          <version>${gmaven-plugin.version}</version>
        </plugin>

        <plugin>
          <groupId>org.codehaus.mojo</groupId>
          <artifactId>axistools-maven-plugin</artifactId>
          <version>${axistools-maven-plugin.version}</version>
        </plugin>

        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-enforcer-plugin</artifactId>
          <version>${maven-enforcer-plugin.version}</version>
          <dependencies>
            <dependency>
              <groupId>org.codehaus.mojo</groupId>
              <artifactId>extra-enforcer-rules</artifactId>
              <version>${extra-enforcer-rules.version}</version>
            </dependency>
          </dependencies>
        </plugin>

        <plugin>
          <groupId>com.diffplug.spotless</groupId>
          <artifactId>spotless-maven-plugin</artifactId>
          <version>${spotless-maven-plugin.version}</version>
          <configuration>
            <java>
              <googleJavaFormat>
                <version>${google-java-format.version}</version>
              </googleJavaFormat>
              <!--
              Indentation works as follows:
              - Google Format indents with 2 spaces
              - We force each 2 spaces to become a tabulation
              - We force each tabulation to become 4 spaces
              -->
              <indent>
                <tabs>true</tabs>
                <spacesPerTab>2</spacesPerTab>
              </indent>
              <indent>
                <spaces>true</spaces>
                <spacesPerTab>4</spacesPerTab>
              </indent>
              <removeUnusedImports />
            </java>
          </configuration>
        </plugin>

      </plugins>
    </pluginManagement>

    <plugins>
      <plugin>
        <!--
      		We don't want commons-logging, we want to bridge it to slf4j using
      		spring-jcl implementation (not jcl-over-slf4j).
      		So here we check that no commons-logging gets included by mistake.
      	  -->
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-enforcer-plugin</artifactId>
        <executions>
          <execution>
            <id>ban-unwanted-stuffs</id>
            <goals>
              <goal>enforce</goal>
            </goals>
            <configuration>
              <rules>
                <bannedDependencies>
                  <excludes>
                    <exclude>commons-logging:commons-logging</exclude>
                    <exclude>org.eclipse.jdt.core.compiler:ecj</exclude>
                  </excludes>
                </bannedDependencies>
                <banDuplicateClasses>
                  <findAllDuplicates>true</findAllDuplicates>
                  <!--
                  	Known harmless duplicates :

                  		1/ com.google.code.findbugs:jsr305 and com.google.code.findbugs:annotations.

                  		jsr305 should be removed in favor of annotations (a superset) unfortunately
                  		it is so ubiquitous that excluding it from every depending artifacts
                  		would be a pain. The weight of the involved jars are negligible and
                  		both can be shipped without significant memory penalty.


                  		2/ io.cucumber.gherkin (5.0.0) and io.cucumber.gherkin-jvm-deps.

                  		Gherkin already inlines every classes from the jvm-deps artifact,
                  		while still depending on it. However this might not be true for
                  		future versions of Gherkin (the pom for version 6.0.0 looks very
                  		different) so I prefer to let them live together. The weight of
                  		both jar is negligible here too (barely 250k of duplicate classes)


                  		3/ org.apache.tomcat.embed:tomcat-embed-core and javax.servlet:javax.servlet-api

                  		See https://github.com/spring-projects/spring-boot/issues/8943 for discussion.

                  		Ideally we should exclude spring-boot-starter-tomcat in tm.web and we could still compile
                  		(the customization of a Tomcat container only depend on Spring Boot core artifact),
                  		as long as the official javax.* apis are available.

                  		Tomcat embed core would be needed only at runtime, or not at all if deployed in
                  		an external Tomcat. But for this to happen we need it in the classpath. So we must
                  		live with the class conflict here untill a better option comes around.
                   -->
                  <ignoreWhenIdentical>true</ignoreWhenIdentical>
                  <ignoreClasses>
                    <!-- conflict in the mantis plugin but I suspect no severe consequences (it worked until now right ?) -->
                    <ignoreClass>javax.xml.namespace.QName</ignoreClass>
                    <!-- conflict with the javax.servlet-api, see comment above -->
                    <ignoreClass>javax.servlet.*</ignoreClass>
                  </ignoreClasses>
                </banDuplicateClasses>
                <requireMavenVersion>
                  <version>3.9.8</version>
                </requireMavenVersion>
              </rules>
            </configuration>
          </execution>
        </executions>
        </plugin>

      <plugin>
        <!-- This plugin sorts the poms so that they are simpler to manage and merge -->
        <groupId>com.github.ekryd.sortpom</groupId>
        <artifactId>sortpom-maven-plugin</artifactId>
        <version>${maven-sortpom-plugin.version}</version>
        <configuration>
          <predefinedSortOrder>custom_1</predefinedSortOrder>
          <lineSeparator>\n</lineSeparator>
          <encoding>${project.build.sourceEncoding}</encoding>
          <sortProperties>true</sortProperties>
          <keepBlankLines>true</keepBlankLines>
          <sortDependencies>scope, artifactId</sortDependencies>
          <nrOfIndentSpace>2</nrOfIndentSpace>
          <expandEmptyElements>false</expandEmptyElements>
        </configuration>
        <executions>
          <execution>
            <goals>
              <goal>sort</goal>
            </goals>
            <phase>verify</phase>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <!-- Checks license headers throughout the project -->
        <groupId>com.mycila</groupId>
        <artifactId>license-maven-plugin</artifactId>
        <version>${license-maven-plugin.version}</version>
        <inherited>false</inherited>
        <configuration>
          <useDefaultExcludes>true</useDefaultExcludes>
          <useDefaultMapping>true</useDefaultMapping>
          <aggregate>true</aggregate>
          <strictCheck>true</strictCheck>

          <properties>
            <license.copyrightOwner>${project.organization.name}</license.copyrightOwner>
          </properties>
          <licenseSets>
            <licenseSet>
              <header>tools/tools-resources/src/main/resources/licenses/header.txt</header>
              <excludes>
                <!-- license files -->
                <exclude>**/COPYING.*</exclude>
                <exclude>**/LICENSE.*</exclude>
                <exclude>**/NOTICE.*</exclude>
                <exclude>**/README.*</exclude>
                <exclude>**/COPYING</exclude>
                <exclude>**/LICENSE</exclude>
                <exclude>**/NOTICE</exclude>
                <exclude>**/README</exclude>
                <exclude>**/*README.*</exclude>
                <exclude>**/*readme.*</exclude>
                <exclude>**/README*.*</exclude>
                <exclude>**/readme*.*</exclude>
                <exclude>**/banner.txt</exclude>
                <!-- manifest templates -->
                <exclude>**/*.mf</exclude>
                <exclude>**/bnd.bnd</exclude>
                <!-- resource modules -->
                <exclude>tools/license-resources/src/main/resources/**/*.*</exclude>
                <exclude>tools/tools-resources/src/main/resources/**/*.*</exclude>
                <!-- mercurial files-->
                <exclude>**/*.orig</exclude>
                <exclude>**/.hgtags</exclude>
                <exclude>**/.hgignore</exclude>
                <exclude>fix-versions.sh</exclude>

                <!-- other IDE / tools files -->
                <exclude>**/.idea/**/*</exclude>
                <exclude>**/nbactions.xml</exclude>
                <exclude>**/nb-configuration.xml</exclude>
                <exclude>**/.groovy/**/*</exclude>
                <exclude>**/.profiler/**/*</exclude>
                <exclude>**/.mvn/**/*</exclude>
                <exclude>**/*.versionsBackup</exclude>
                <exclude>**/*.bak</exclude>
                <exclude>**/*.swp</exclude>
                <exclude>**/*.log</exclude>
                <exclude>**/Thumbs.db</exclude>
                <exclude>**/*.swp</exclude>
                <exclude>**/.settings/**</exclude>
                <exclude>**/*.iml</exclude>
                <exclude>**/*.editorconfig</exclude>
                <!-- third-party javascript libraries -->
                <exclude>**/bower_modules/**/*</exclude>
                <exclude>**/node_modules/**/*</exclude>
                <exclude>**/node/**/*</exclude>
                <exclude>**/js/**/*.*</exclude>
                <exclude>**/scripts/ckeditor/**/*.*</exclude>
                <exclude>**/scripts/datatables/**/*.*</exclude>
                <exclude>**/scripts/jquery/**/*.*</exclude>
                <exclude>**/scripts/jqgrid/**/*.*</exclude>
                <exclude>**/scripts/jqplot/**/*.*</exclude>
                <exclude>**/scripts/iesupport/**/*.*</exclude>
                <exclude>**/scripts/thirdparties/**/*.*</exclude>
                <exclude>**/scripts/lib/**/*.*</exclude>
                <exclude>**/scripts/require*.js</exclude>
                <exclude>**/styles/ckeditor/**/*.*</exclude>
                <exclude>**/styles/datatables/**/*.*</exclude>
                <exclude>**/styles/jquery/**/*.*</exclude>
                <exclude>**/styles/jqgrid/**/*.*</exclude>
                <exclude>**/styles/jqplot/**/*.*</exclude>
                <exclude>**/styles/switchButton/*.*</exclude>
                <exclude>**/styles/**/thymol.js</exclude>
                <exclude>**/r.js</exclude>
                <exclude>**/scripts/sinon*.js</exclude>
                <exclude>**/styles/**/bootstrap*.css</exclude>
                <exclude>**/pubsub-boot.js</exclude>

                <exclude>**/*.properties</exclude>

                <!-- generated stylesheets -->
                <exclude>**/styles/**/squash.*.css</exclude>
                <!-- third-party wslds -->
                <exclude>**/*.wsdl</exclude>
                <!-- misc system / ide files -->
                <exclude>**/build.properties</exclude>
                <!-- felix configuration files -->
                <exclude>**/felix.*.properties</exclude>
                <!-- SQL files used by liquibase, breaks on mysql import when licensed -->
                <exclude>**/liquibase/**/*.sql</exclude>
                <!-- IzPack files -->
                <exclude>**/izpack/**/*.*</exclude>
                <!-- m2e generated files -->
                <exclude>**/pom.properties</exclude>
                <exclude>**/pom.xml</exclude>
                <exclude>**/pom.xml.tag</exclude>
                <exclude>**/pom.xml.versionsBackup</exclude>
                <!-- webapp resources -->
                <exclude>**/webapp/images/*</exclude>
                <!-- jasper files -->
                <exclude>**/*.jasper</exclude>
                <!-- test resources -->
                <exclude>tm-integration/integration-tests/src/it/resources/**</exclude>
                <exclude>**/*.xlsx</exclude>
                <exclude>**/*.feature</exclude>
                <!--other -->
                <exclude>**/new_report.rptdesign</exclude>
                <!-- generated xml (dev) -->
                <exclude>**/tm.service/META-INF/**</exclude>
                <exclude>**/config/META-INF/**</exclude>
                <!-- jetty config for dev -->
                <exclude>**/provision/**/etc/jetty.xml</exclude>
                <exclude>**/provision/**/etc/jetty*.xml</exclude>
                <exclude>tm/tm.web/scripts/**</exclude>
                <exclude>tm/tm.web/images/**</exclude>
                <exclude>tm/tm.web/styles/**</exclude>
                <!-- excluding node module for jenkins -->
                <exclude>tm/tm.web/node_module/**</exclude>
                <exclude>tm/tm-front/**</exclude>
                <!-- for gitlab ci to allow proper caching of maven dep -->
                <exclude>.m2/**</exclude>
                <!-- generated pom.xml by ci-friendly-flatten-maven-plugin -->
                <exclude>**/.ci-friendly-pom.xml</exclude>
              </excludes>
            </licenseSet>
          </licenseSets>
          <mapping>
            <tag>DYNASCRIPT_STYLE</tag>
            <jrxml>XML_STYLE</jrxml>
            <java>SLASHSTAR_STYLE</java>
            <groovy>SLASHSTAR_STYLE</groovy>
          </mapping>
        </configuration>
        <executions>
          <execution>
            <goals>
              <goal>check</goal>
            </goals>
            <phase>validate</phase>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>com.outbrain.swinfra</groupId>
        <artifactId>ci-friendly-flatten-maven-plugin</artifactId>
        <version>${ci-friendly.version}</version>
        <executions>
          <execution>
            <goals>
              <!-- Ensure proper cleanup. Will run on clean phase-->
              <goal>clean</goal>
              <!-- Enable ci-friendly version resolution. Will run on process-resources phase-->
              <goal>flatten</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

    </plugins>
  </build>

  <inceptionYear>2010</inceptionYear>
  <organization>
    <name>Henix, henix.fr</name>
  </organization>

  <licenses>
    <license>
      <name>GNU Lesser General Public License V3</name>
    </license>
  </licenses>

  <scm>
    <connection>scm:git:${project.basedir}</connection>
    <developerConnection>scm:git:${project.basedir}</developerConnection>
    <tag>HEAD</tag>
  </scm>



  <repositories>

    <!--
      Redeclaring the default Maven Central repository, as declared by mvn help:effective-pom.
      This way we ensure it will be queried first, and hopefully would speed up the initial build
      of the project.
    -->
    <repository>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
      <id>central</id>
      <name>Central Repository</name>
      <url>https://repo.maven.apache.org/maven2</url>
    </repository>

    <!--
      Declaration of the Squash official repositories
    -->
    <repository>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
      <id>squash-release-repo</id>
      <url>https://repo.squashtest.org/maven2/releases</url>
    </repository>

    <repository>
      <releases>
        <enabled>false</enabled>
      </releases>
      <snapshots>
        <enabled>true</enabled>
      </snapshots>
      <id>squash-snapshot-repo</id>
      <url>https://repo.squashtest.org/maven2/snapshots</url>
    </repository>

    <!--
      Jasper Reports hold unique artifacts in there
    -->
    <repository>
      <id>jasper-reports</id>
      <url>https://jasperreports.sourceforge.net/maven2</url>
    </repository>
  </repositories>

  <distributionManagement>
    <repository>
      <id>squash-release-deploy-repo</id>
      <name>Squash releases deployment repo</name>
      <url>${deploy-repo.release.url}</url>
    </repository>
    <snapshotRepository>
      <id>squash-snapshot-deploy-repo</id>
      <name>Squash snapshot deployment repo</name>
      <url>${deploy-repo.snapshot.url}</url>
    </snapshotRepository>
  </distributionManagement>

  <!-- ============================================================

      PROFILES

  =============================================================== -->

  <profiles>

    <profile>
      <!-- profile for integration testing. It adds the module 'tm-integration' to the reactor. -->
      <id>integration</id>
      <activation>
        <property>
          <name>stage</name>
          <value>integration</value>
        </property>
      </activation>

      <modules>
        <module>tm-integration</module>
      </modules>

      <build>
        <plugins>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-surefire-plugin</artifactId>
            <executions>
              <execution>
                <id>default-test</id>
                <phase>none</phase>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
    </profile>

    <!--
      This profile forbids the javadoc plugin to fail for linter reasons.
      The proper fix (that is, really documenting parameters and such) must
      wait a bit.
    -->
    <profile>
      <id>disable-java8-doclint</id>
      <activation>
        <jdk>[1.8,)</jdk>
      </activation>
      <properties>
        <additionalparam>-Xdoclint:none</additionalparam>
      </properties>
    </profile>

    <!--    This profile enable angular unit test, that are deactivated by default to prevent issues on CI env -->
    <!--    The standard pipeline run ng tests in a distinct step, executed by a dedicated container provided with chrome -->
    <profile>
      <id>ng-test</id>
    </profile>

    <profile>
      <id>enforce-dep-release</id>
      <build>
        <plugins>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-enforcer-plugin</artifactId>
            <executions>
              <execution>
                <id>enforce-dep-releasse</id>
                <goals>
                  <goal>enforce</goal>
                </goals>
                <configuration>
                  <rules>
                    <requireReleaseDeps>
                      <message>No Snapshots Dependencies Allowed in release !</message>
                    </requireReleaseDeps>
                  </rules>
                </configuration>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
    </profile>
  </profiles>
</project>
