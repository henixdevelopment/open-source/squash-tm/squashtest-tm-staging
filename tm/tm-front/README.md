# tm-front

## Project overview

This project has two main modules :

1. `sqtm-core`: A module containing the common core for squash-tm main application and plugins.
2. `sqtm-app`: A module containing the main squash tm application.

The Cypress tests (integration and end-to-end tests) are in `./cypress` folder.

### Notes on sqtm-core exports

-   For exports to be visible outside the `sqtm-core` module, you have to re-export them in `projects/sqtm-core/src/public_api.ts`.
-   From `sqtm-app`, always import from `sqtm-core` and do NOT use relative paths.

```
// NOT OK
import {Identifier} from '../../../../../../../../projects/sqtm-core/src/lib/ui/grid/model/data-row.model';

// OK
import {Identifier} from 'sqtm-core';
```

## Development environment setup

You'll need NodeJS and Yarn installed.

### Installing NodeJS dependencies

To install the dependencies, run `yarn install --frozen-lockfile` from `tm/tmfront`.

The option `--frozen-lockfile` ensures that the same dependency versions are used for all developers.
Only do `yarn install` without the option when changes are made to the `package.json` dependencies.

### Start the development server

The development server allows hot module reloading on both sqtm-core and sqtm-app. Start it with :

```
yarn start
```

The application should be then accessible at `http://localhost:4200/squash`.

## Running the front-end tests

See the dedicated documentation at [docs/RUNNING_TESTS](docs/RUNNING_TESTS.md).

## Releasing `sqtm-core`

See the dedicated documentation at [docs/CORE_RELEASE_PROCEDURE](docs/CORE_RELEASE_PROCEDURE.md).
