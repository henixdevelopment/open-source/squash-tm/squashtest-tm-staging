const TerserPlugin = require('terser-webpack-plugin')

// little hack to know if we are building in prod mode...
// i have not found any tag in process.env or any other way to spot the build mode from here...
const isProd = process.argv.indexOf('production') !== -1;

// "tests" environment will enable instrumentation of code in order to measure code coverage
const isTests = process.argv.indexOf('tests') !== -1;

const prodPlugins = [
  new TerserPlugin({
    terserOptions: {
      compress: false,
    },
  }),
];

const devPlugins = [];

const plugins = (isProd || isTests) ? prodPlugins : devPlugins;

const buildMode = getBuildMode();

function getBuildMode() {
  if (isProd) {
    return 'production';
  } else if (isTests) {
    return 'tests';
  } else {
    return 'development';
  }
}

console.log(`Building sqtm-app in ${buildMode} mode. See angular.json and extra-webpack.config.js.`);

module.exports = {
  node:{
    global: true
  },
  plugins,
  resolve: {
    fallback: {
      "fs": false,
      "path": false,
      "assert": false,
      "stream": require.resolve("stream-browserify")
    },
  },
  module: isTests ? {
    rules: [
      {
        test: /\.ts$/,
        loader: 'babel-loader',
        options: {
          // presets: ['@babel/preset-env'],
          plugins: ['babel-plugin-istanbul']
        },
        enforce: 'post',
        include: require('path').join(__dirname, 'projects'),
        exclude: [
          /\.(e2e|spec)\.ts$/,
          /node_modules/,
          /(ngfactory|ngstyle)\.js/
        ]
      }
    ]
  } : {},
};
