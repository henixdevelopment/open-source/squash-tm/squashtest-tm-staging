#!/bin/sh -xe
# /bin/sh -xe ensure that each line that return an error code will make the jenkins step crash and so make the build crash.

# Adding ./node_module/.bin to PATH
# Adding ./node/node to PATH
# Adding ./node/yarn/dist/bin to PATH
# The local installs from maven front end plugin must be found before any globally installed version of node or yarn
PATH=$PWD/node_modules/.bin:$PWD/node:$PWD/node/yarn/dist/bin:$PATH

# Launching cypress in
yarn run e2e-cypress


