#!/bin/sh
# this scipt should be used in post step of cypress build, to kill the angular server
PROJECT_HOME=$PWD
PID_FILE=$PROJECT_HOME/target/ng-server.pid

read -r PID_TO_KILL < $PID_FILE
echo "KILL SERVER WITH PID : " $PID_TO_KILL
kill $PID_TO_KILL
