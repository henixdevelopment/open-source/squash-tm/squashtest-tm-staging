### Definition

Pre-push hook script is defined in the file `./pre-push`.

### Usage

As its name implies, the script will run before pushing local commits to the remote repository.

It applies Java format on the worktree using command `mvn spotless:apply` and compares changes of committed Java files before and after formatting.
If format of at least one Java file was modified, the push is aborted, letting you commit the format changes.
Otherwise, the push is resumed.

Different cases are considered:
- If no Java files were committed locally, the push is directly resumed
- If the only changes of your worktree before formatting were format deviations, the push is resumed
- If a Java file was formatted by the script, but is not included in a commit yet, it will not block the push
- If the local or the remote branch is not defined, then the changes of all Java files will be compared

### Limits

The script simply compares Java files before and after their formatting in your current worktree. It considers your worktree formatted when there is no difference between the state before and the state after. It cannot verify that the format of the worktree about to be pushed is compliant.

⚠ In other words, the push will happen if your worktree is completely formatted even if part of these changes were not committed. This situation would end in a pipeline failure during format job.  
**Notably, if your Java code is not formatted, performing a first push will format your code, fail, and advise you to commit the changes, but directly performing a second push without committing format changes will succeed, ending in a pipeline failure.**

### Diagram

```mermaid
---
title: Squash pre-push hook
---
flowchart

    START[Push command] --> LOCAL_AND_REMOTE_BRANCH?{Local and remote\n branch detected ?}
    
    LOCAL_AND_REMOTE_BRANCH? --> |No\n Format will be compared\n on all Java files | FORMAT[mvn spotless:apply]
    LOCAL_AND_REMOTE_BRANCH? --> |Yes| ANY_JAVA_FILES_MODIFIED?{Any Java files\n modified in\n local commits ?}
    
    ANY_JAVA_FILES_MODIFIED? --> |Yes\n Format will be compared\n on committed Java files | FORMAT
    ANY_JAVA_FILES_MODIFIED? --> |No\n Ignore hook | RESUME

    FORMAT --> ANY_GIT_DIFF?{Any remaining\n changes ?}
    
    ANY_GIT_DIFF? --> |Yes| GIT_STATUS_CHANGED?{Format changed\n after formatting ?}
    ANY_GIT_DIFF? --> |No\n Worktree is clean| RESUME
    GIT_STATUS_CHANGED? --> |Yes\n Java code\n has been\n formatted| ABORT
    GIT_STATUS_CHANGED? --> |No\n Worktree is\n already formatted| RESUME
    
    ABORT[Abort Push]
    RESUME[Resume Push]
```
