## How to build CKEditor unminified : 

> git clone https://github.com/ckeditor/ckeditor-presets.git

> cd ckeditor-presets

> git submodule update --init

> build.sh full all  --leave-js-unminified 

To have all needed ckeditor files for Squash TM:
- add the onchange plugin
- add the moonocolor skin

## How to update CKEditor source files: 

- Get link to download source files from the header in build-config.js .
- Use the second download link to get the source files as .zip . 
- Extract the files.
- Save a local copy of the current ckeditor folder before replacing it with the new source files.
- Copy the missing skins from the old files into the new ckeditor/skins folder.
- Launch the front and make sure everything is working correctly. If some plugins are missing, 
repeat the process from step 2 while adding the missing plugins to configuration.
- If any plugins are missing some configuration, you can build ckeditor unminified and use the generated plugin.js from the relevant plugin.

