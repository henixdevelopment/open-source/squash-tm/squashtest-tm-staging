# Running the front-end tests

### Running the unit tests (Karma)

Both module has its own set of unit tests. You can run them with :

```
ng test sqtm-core
```

```
ng test sqtm-app
```

### Running the integration tests (Cypress)

With the development server started on port 4200, you can launch test integrations tests in interactive mode with :

```
yarn watch-cypress
```

You can also run the tests in headless mode with :

```
yarn integration-tests-cypress-dev
```

### Running the end-to-end tests (Cypress)

To run the E2E tests, you'll need Cypress installed, dedicated databases and set up a few cypress environments variables.

#### Creating a database

The process is the same as for production and development databases. You can create a new PostgreSQL or MariaDB schema and provision it with the database install scripts.

Make sure to dedicate these databases to E2E tests only as they will be cleaned up between each test.

#### Setup the environment variables

The simplest way to provide environment variables for Cypress is to add a `cypress.env.json` file, next to `cypress.json` (the root of the front-end project).
This file should contain the connection infos to your databases:

```
{
  "profiles": {
    "postgresql": {
      "database": "squashtm_e2e",
      "port": 5432,
      "user": "postgres",
      "password": "root",
      "host": "localhost"
    },
    "mariadb": {
      "database": "squashtm_e2e",
      "port": 3306,
      "user": "root",
      "password": "root",
      "host": "localhost"
    }
  }
}
```

Both `mariadb` and `postgresql` profiles are required. Just put empty string values if you don't want to use one of these profile.

#### Running the tests

To execute the tests, run one of those commands from the front-end root folder :
- `yarn e2e-cypress-postgres` : runs on PostgreSQL in headless mode
- `yarn e2e-cypress-mariadb` : runs on MariaDB in headless mode
- `yarn watch-cypress-e2e-postgres` : runs on PostgreSQL in interactive mode
- `yarn watch-cypress-e2e-mariadb` : runs on MariaDB in interactive mode
