# Releasing the sqtm-core module

## Release procedure

### 1. Update versions

The published version is defined in the `package.json` file.
For this project, there are two `package.json` files : one at the front-end's root folder and another in `projects/sqtm-core`.

If you want to publish version `7.0.0-it.9.snapshot.1`, set this value in the `version` field of both `package.json` files.

> Notes on versions :
>  - Versions in the root `package.json` and the `sqtm-app`'s one should always be synchronized.
>  - For a snapshot release, use a version number like "2.0.0-it.28.snapshot.1".
>  - If you need to publish an iteration version or a release candidate, use a version number like "2.0.0-it.28" or "2.0.0-rc.1".
>  - If you need to publish a release, use a version number like "2.0.0" or "2.0.1".
>  - Take care of what *real* version your plugins `yarn install` will resolve to be sure you have the desired version.
>    The exact resolved version can be found in the `yarn.lock` file.
>  - Try to avoid dependency to snapshot version once your dev is over.

### 2. Build `sqtm-core`

At the front-end root folder, run `yarn build-core`. It will build `sqtm-core` and create the distribution into `dist/sqtm-core`.

### 3. Publish `sqtm-core`

- From `dist/sqtm-core`, publish to your NPM repository by running `yarn publish --no-git-tag-version`.
- When you're prompted "question New version", you should put the version defined at step 1.
- You'll be asked credentials for the NPM account. Contact someone from the Squash TM development team to obtain them.

### 4. Commit the version bump

Commit your changes to the `package.json` files with a message such as "Manual NPM version bump to <NEXT_VERSION>".

### 5. Dependencies bump

To access the newest code from a plugin, you have to upgrade the `sqtm-core` dependency in the plugin's `package.json`
and run `yarn install` in the plugin's frontend directory.

If the newly published `sqtm-core` version includes breaking changes (e.g. modification of a backend URL, changes in shared
UI components...), all plugins with an Angular frontend should be upgraded to point to the newest `sqtm-core` version.

## Note on automation

All this release process could be automated in the future, and the CI should perform release itself.
In the meantime we must handle it manually.

However, for local development, it could be necessary to handle snapshot release if a plugin require a modification in sqtm-core.

