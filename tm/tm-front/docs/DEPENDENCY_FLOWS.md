# Dependency flows in the web-app and Cypress sources

There are a few rules to follow when importing ES modules from one part of the repository
to another :
- When importing from `projects/sqtm-core/` into `projects/sqtm-app/`, always import 
from `'sqtm-core'`.
- When importing from `projects/sqtm-core/` into `cypress/`, always use relative paths
- In other cases, always use relative paths

Additionally, modules located inside `projects/sqtm-core/lib/model` should **never**
import code from the outside world. Imports from external dependencies or from outside
the `projects/sqtm-core/lib/model` folder are banned.

Here is a flow chart describing the dependency flows.

```mermaid
flowchart
subgraph tm/tm-front/
    projects/sqtm-app/ -- "import from 'sqtm-core'" --> sqtm-core
    
    cypress/ --> lib/model/
    
    subgraph sqtm-core["projects/sqtm-core/"]
        lib/core/ --> lib/model/
        lib/ui/ --> lib/model/
    end
end
```

These rules are enforced by ESLint (see the `import/no-restricted-paths` rules in the various
`.eslintrc` files).

## Why should I import from 'sqtm-core'?

All the code in `projects/sqtm-core/` gets packaged into the `sqtm-core` NPM artifact
which is then consumed by both the main web app (code in `projects/sqtm-app`) and by
plugins' frontends using the following syntax :
```
import { Foo } from 'sqtm-core'
``` 

Using relative imports from `projects/sqtm-core/` into `projects/sqtm-app` would break
the NPM package encapsulation. It might lead to sources duplication in the final package.

## Why using relative paths when importing into Cypress test sources?

The Cypress test sources (inside `cypress/`) are compiled by Cypress before the tests'
execution. These test sources may import the app models (TypeScript counterparts of JSON
payloads received from the backend), for example to create request stubs in *back-mocked*
tests.

The test execution environment should not depend on the web-app dependencies (such as the
Angular runtime) so the models themselves cannot depend on external sources.

> Note on Cypress dependencies handling :
> 
> As of May 2023 (Squash TM v5), the Cypress test sources belong to the same NPM package
> as the rest of the web-app (they do not have their own `package.json` file).
> 
> The E2E execution environment won't `npm install` the web-app dependencies as it would
> install too much unnecessary dependencies on the test execution environment.
