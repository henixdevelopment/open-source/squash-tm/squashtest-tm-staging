module.exports = {
  "root": true,
  "env": {
    "browser": true,
    "es2021": true
  },
  "extends": [
    "eslint:recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:import/recommended",
    "plugin:import/typescript",
    "prettier"
  ],
  "overrides": [
    {
      "env": {
        "node": true
      },
      "files": [
        ".eslintrc.{js,cjs}",
      ],
      "parserOptions": {
        "sourceType": "script"
      }
    }
  ],
  "settings": {
    "import/parsers": {
      "@typescript-eslint/parser": [".ts"]
    },
    "import/resolver": {
      "typescript": true,
      "node": true
    },
    "project": [
      "projects/**/tsconfig*.json",
    ]
  },
  "parser": "@typescript-eslint/parser",
  "parserOptions": {
    "ecmaVersion": "latest",
    "sourceType": "module"
  },
  "plugins": [
    "@typescript-eslint",
    "import",
    "no-only-tests",
    "unused-imports"
  ],
  "rules": {
    // Ban tests focus
    "no-only-tests/no-only-tests": "error",

    // Remove unused import rules
    "import/export": "off",
    "import/no-unresolved": "off",
    "import/no-duplicates": "off",
    "import/namespace": "off",

    // Avoid unused imports
    "unused-imports/no-unused-imports": "error",

    // Avoid unused vars, except variables prefixed with an underscore
    "@typescript-eslint/no-unused-vars": "off",
    "unused-imports/no-unused-vars": [
      "error",
      { "vars": "all", "varsIgnorePattern": "^_", "args": "after-used", "argsIgnorePattern": "^_" }
    ],

    // These rules were not covered with the legacy TSLint configuration.
    // Feel free to remove these overrides!
    "@typescript-eslint/no-explicit-any": "off",
    "no-prototype-builtins": "warn",
  }
}
