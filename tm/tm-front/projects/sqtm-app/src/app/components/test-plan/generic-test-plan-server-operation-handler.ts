import { Inject, Injectable } from '@angular/core';
import { combineLatest, Observable, of } from 'rxjs';
import { distinctUntilChanged, filter, finalize, map, switchMap, take } from 'rxjs/operators';
import { AbstractServerOperationHandler, GridService, GridState, Identifier } from 'sqtm-core';
import {
  GENERIC_TEST_PLAN_ITEM_MOVER,
  GenericTestPlanItemMover,
} from './generic-test-plan-item-mover';

@Injectable()
export class GenericTestPlanServerOperationHandler extends AbstractServerOperationHandler {
  // We're only interested in drag and drop internal reordering
  canCopy$: Observable<boolean> = of(false);
  canPaste$: Observable<boolean> = of(false);
  canDelete$: Observable<boolean> = of(false);
  canCreate$: Observable<boolean> = of(false);
  canDrag$: Observable<boolean>;

  private _grid: GridService;

  constructor(
    @Inject(GENERIC_TEST_PLAN_ITEM_MOVER) private testPlanItemMover: GenericTestPlanItemMover,
  ) {
    super();
  }

  get grid(): GridService {
    return this._grid;
  }

  set grid(gridService: GridService) {
    this._grid = gridService;

    this.canDrag$ = combineLatest([
      this._grid.isSortedOrFiltered$,
      this.testPlanItemMover.canMoveTestPlanItems$,
    ]).pipe(
      map(
        ([isSortedOrFiltered, canMoveTestPlanItems]) => !isSortedOrFiltered && canMoveTestPlanItems,
      ),
      distinctUntilChanged(),
    );
  }

  copy(): void {
    // Cannot copy test plan items
  }

  paste(): void {
    // Cannot paste in test plan
  }

  delete(): void {
    // Cannot delete with delete key
  }

  allowDropSibling(): boolean {
    return true;
  }

  allowDropInto(): boolean {
    return false;
  }

  notifyInternalDrop(): void {
    this.grid.isSortedOrFiltered$
      .pipe(
        take(1),
        filter((isSortedOrFiltered) => !isSortedOrFiltered),
        switchMap(() => this.grid.gridState$),
        take(1),
      )
      .subscribe((gridState) => {
        const dragState = gridState.uiState.dragState;
        if (dragState.dragging && dragState.currentDndTarget) {
          this.reorderOptions(gridState);
        } else {
          this.grid.cancelDrag();
        }
      });
  }

  reorderOptions(gridState: GridState) {
    const filteredList = (gridState.dataRowState.ids as Identifier[]).filter(
      (id) => Boolean(id) && !gridState.uiState.dragState.draggedRowIds.includes(id),
    );

    const newPosition = this.findDropPosition(gridState, filteredList);
    const draggedRowIds = gridState.uiState.dragState.draggedRowIds;

    this.grid.beginAsyncOperation();
    this.testPlanItemMover
      .changeTestPlanItemsPosition(draggedRowIds, newPosition)
      .pipe(
        finalize(() => {
          this.grid.refreshDataAndKeepSelectedRows();
          this.grid.completeAsyncOperation();
        }),
      )
      .subscribe();
  }

  findDropPosition(gridState: GridState, filteredList: Identifier[]): number {
    const targetId = gridState.uiState.dragState.currentDndTarget.id as string;
    const zone = gridState.uiState.dragState.currentDndTarget.zone;

    let newPosition = filteredList.indexOf(targetId);

    if (zone === 'below') {
      newPosition++;
    }

    newPosition = this.calculateNewPositionInGridPage(newPosition);

    return newPosition;
  }

  /**
   * [SQUASH-5216] Calculates the general position  in the test-plan of the dropped item, so that it appears on the correct
   * page when the grid is paginated. The position of the tc on the current grid page is different from its index in the
   * test plan list if there are multiple pages.
   * @param newPositionInPage position index of tc in current grid page
   */
  calculateNewPositionInGridPage(newPositionInPage: number) {
    let newPositionInOverallGrid: number = newPositionInPage;
    this.grid.paginationDisplay$.pipe(take(1)).subscribe((paginationDisplay) => {
      if (paginationDisplay.active && paginationDisplay.page > 0) {
        const currentGridPageNumber = paginationDisplay.page;
        const numberOfTCsPerGridPage = paginationDisplay.size;
        newPositionInOverallGrid =
          currentGridPageNumber * numberOfTCsPerGridPage + newPositionInPage;
      }
    });
    return newPositionInOverallGrid;
  }
}
