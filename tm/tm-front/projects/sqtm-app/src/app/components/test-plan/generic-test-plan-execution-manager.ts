import { Observable } from 'rxjs';
import { InjectionToken } from '@angular/core';

export interface GenericTestPlanExecutionManager {
  canExecute$: Observable<boolean>;
  canDeleteExecutions$: Observable<boolean>;
}

export const GENERIC_TEST_PLAN_EXECUTION_MANAGER =
  new InjectionToken<GenericTestPlanExecutionManager>('GenericTestPlanExecutionManager');
