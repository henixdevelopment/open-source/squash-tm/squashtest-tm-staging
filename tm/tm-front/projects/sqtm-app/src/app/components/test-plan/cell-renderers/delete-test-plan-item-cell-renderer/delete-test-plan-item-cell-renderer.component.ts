import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
} from '@angular/core';
import { Subject } from 'rxjs';
import { catchError, concatMap, filter, takeUntil } from 'rxjs/operators';
import {
  AbstractCellRendererComponent,
  ActionErrorDisplayService,
  ColumnDefinitionBuilder,
  DialogService,
  Fixed,
  GridColumnId,
  GridService,
} from 'sqtm-core';
import { NzIconModule } from 'ng-zorro-antd/icon';
import {
  GENERIC_TEST_PLAN_ITEM_DELETER,
  GenericTestPlanItemDeleter,
} from '../../generic-test-plan-item-deleter';

@Component({
  selector: 'sqtm-app-delete-test-plan-item-cell-renderer',
  template: `
    @if (genericTestPlanItemDeleter.allowTestPlanItemDeletion() && canDelete()) {
      <div
        class="full-height full-width flex-column justify-content-center current-workspace-main-color __hover_pointer"
        (click)="removeItem()"
      >
        <i nz-icon [nzType]="getIcon()" nzTheme="outline" class="table-icon-size"></i>
      </div>
    }
  `,
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [NzIconModule],
})
export class DeleteTestPlanItemCellRendererComponent
  extends AbstractCellRendererComponent
  implements OnDestroy
{
  unsub$ = new Subject<void>();

  constructor(
    public grid: GridService,
    cdr: ChangeDetectorRef,
    private dialogService: DialogService,
    @Inject(GENERIC_TEST_PLAN_ITEM_DELETER)
    public readonly genericTestPlanItemDeleter: GenericTestPlanItemDeleter,
    private readonly actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    super(grid, cdr);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  getIcon(): string {
    return this.rowHasExecution() ? 'sqtm-core-generic:delete' : 'sqtm-core-generic:unlink';
  }

  removeItem() {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.campaign-workspace.dialog.title.remove-association',
      messageKey: this.rowHasExecution()
        ? 'sqtm-core.campaign-workspace.dialog.message.remove-association.delete'
        : 'sqtm-core.campaign-workspace.dialog.message.remove-association.unbind',
      level: this.rowHasExecution() ? 'DANGER' : 'WARNING',
    });

    dialogReference.dialogClosed$
      .pipe(
        takeUntil(this.unsub$),
        filter((result) => result),
        concatMap(() => this.genericTestPlanItemDeleter.deleteTestPlanItems([this.getItemId()])),
        catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
      )
      .subscribe(() => {
        this.grid.refreshData();
      });
  }

  private getItemId(): number {
    // In iterations, the item ID is stored in a different column.
    return this.row.data[GridColumnId.testPlanItemId] ?? this.row.data[GridColumnId.itemTestPlanId];
  }

  private rowHasExecution(): boolean {
    return this.row.data[GridColumnId.latestExecutionId] != null;
  }

  canDelete(): boolean {
    return (
      this.row.simplePermissions &&
      (this.rowHasExecution()
        ? this.row.simplePermissions.canExtendedDelete
        : this.row.simplePermissions.canDelete)
    );
  }
}

export function deleteTestPlanItemCellRendererComponent(): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder('delete')
    .withRenderer(DeleteTestPlanItemCellRendererComponent)
    .withLabel('')
    .disableSort()
    .changeWidthCalculationStrategy(new Fixed(40))
    .withViewport('rightViewport');
}
