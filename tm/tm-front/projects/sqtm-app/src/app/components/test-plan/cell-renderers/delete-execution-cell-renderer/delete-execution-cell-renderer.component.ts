import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractDeleteCellRenderer,
  ActionErrorDisplayService,
  CellRendererCommonModule,
  ColumnDefinitionBuilder,
  ConfirmDeleteLevel,
  DataRow,
  DialogService,
  Fixed,
  GridColumnId,
  GridService,
  RestService,
} from 'sqtm-core';
import { catchError, finalize } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-delete-execution-cell-renderer',
  template: `
    @if (row && canDelete(row)) {
      <sqtm-core-delete-icon
        [iconName]="'sqtm-core-generic:delete'"
        (delete)="showDeleteConfirm()"
      ></sqtm-core-delete-icon>
    }
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CellRendererCommonModule],
  standalone: true,
})
export class DeleteExecutionCellRendererComponent extends AbstractDeleteCellRenderer {
  constructor(
    private readonly restService: RestService,
    grid: GridService,
    cdr: ChangeDetectorRef,
    dialogService: DialogService,
    private readonly actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    super(grid, cdr, dialogService);
  }

  canDelete(row: DataRow): boolean {
    return (
      row.simplePermissions && row.simplePermissions.canDelete && !row.data.boundToBlockingMilestone
    );
  }

  override doDelete(): any {
    this.grid.beginAsyncOperation();

    this.restService
      .delete([
        'test-plan-item',
        this.row.data.testPlanItemId.toString(),
        'execution',
        this.row.data.executionId,
      ])
      .pipe(
        catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
        finalize(() => this.grid.completeAsyncOperation()),
      )
      .subscribe(() => this.grid.refreshData());
  }

  protected override getLevel(): ConfirmDeleteLevel {
    return 'DANGER';
  }

  protected override getMessageKey(): string {
    return 'sqtm-core.campaign-workspace.dialog.message.remove-execution';
  }

  protected override getTitleKey(): string {
    return 'sqtm-core.campaign-workspace.dialog.title.remove-execution';
  }
}

export function deleteExecutionColumn(): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(GridColumnId.delete)
    .withRenderer(DeleteExecutionCellRendererComponent)
    .withLabel('')
    .disableSort()
    .changeWidthCalculationStrategy(new Fixed(30));
}
