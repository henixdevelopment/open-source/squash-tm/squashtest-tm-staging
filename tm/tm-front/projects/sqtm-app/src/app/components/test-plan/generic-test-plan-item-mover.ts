import { Observable } from 'rxjs';
import { InjectionToken } from '@angular/core';
import { Identifier } from 'sqtm-core';

export interface GenericTestPlanItemMover {
  canMoveTestPlanItems$: Observable<boolean>;

  changeTestPlanItemsPosition(itemsToMove: Identifier[], position: number): Observable<unknown>;
}

export const GENERIC_TEST_PLAN_ITEM_MOVER = new InjectionToken<GenericTestPlanItemMover>(
  'GenericTestPlanItemMover',
);
