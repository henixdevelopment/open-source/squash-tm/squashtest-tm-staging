import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  InjectionToken,
  OnDestroy,
  OnInit,
} from '@angular/core';
import {
  ActionErrorDisplayService,
  CampaignPermissions,
  centredTextColumn,
  convertSqtmLiterals,
  DataRow,
  dateTimeColumn,
  DialogModule,
  DialogReference,
  DialogService,
  executionModeColumn,
  executionStatusColumn,
  Extendable,
  Fixed,
  grid,
  GridColumnId,
  GridDefinition,
  GridModule,
  GridService,
  gridServiceFactory,
  indexColumn,
  numericColumn,
  ProjectDataMap,
  ReferentialDataService,
  RestService,
  sortDate,
  SquashTmDataRowType,
  StyleDefinitionBuilder,
  testCaseImportanceColumn,
  textColumn,
  withLinkColumn,
  WorkspaceCommonModule,
} from 'sqtm-core';
import { AsyncPipe } from '@angular/common';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { TranslateModule } from '@ngx-translate/core';
import { Observable, Subject } from 'rxjs';
import { catchError, concatMap, filter, finalize, map, take, takeUntil, tap } from 'rxjs/operators';
import { SuccessRateComponent } from '../../../../pages/campaign-workspace/iteration-view/components/cell-renderers/success-rate/success-rate.component';
import { deleteExecutionColumn } from '../../cell-renderers/delete-execution-cell-renderer/delete-execution-cell-renderer.component';
import {
  GENERIC_TEST_PLAN_EXECUTION_MANAGER,
  GenericTestPlanExecutionManager,
} from '../../generic-test-plan-execution-manager';
import { toSignal } from '@angular/core/rxjs-interop';

const TABLE_CONF = new InjectionToken('Test plan item execution history table configuration');
const TABLE = new InjectionToken('Test plan item execution history table');

@Component({
  selector: 'sqtm-app-test-plan-item-execution-history',
  templateUrl: './test-plan-item-execution-history.component.html',
  styleUrl: './test-plan-item-execution-history.component.less',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: TABLE_CONF,
      useFactory: tableDefinition,
    },
    {
      provide: TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, TABLE_CONF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: TABLE,
    },
  ],
  imports: [
    AsyncPipe,
    NzButtonModule,
    NzIconModule,
    NzToolTipModule,
    TranslateModule,
    WorkspaceCommonModule,
    DialogModule,
    GridModule,
  ],
})
export class TestPlanItemExecutionHistoryComponent implements OnInit, OnDestroy {
  private readonly unsub$ = new Subject<void>();

  protected readonly $canDeleteExecutions = toSignal(
    this.genericTestPlanExecutionManager.canDeleteExecutions$,
  );

  protected readonly canDeleteExecution$ = this.gridService.selectedRows$.pipe(
    map((rows: DataRow[]) => this.$canDeleteExecutions() && this.canDeleteExecution(rows)),
  );

  constructor(
    private readonly gridService: GridService,
    private readonly dialogRef: DialogReference<TestPlanItemExecutionHistoryDialogConfiguration>,
    private readonly dialogService: DialogService,
    private readonly restService: RestService,
    @Inject(GENERIC_TEST_PLAN_EXECUTION_MANAGER)
    private readonly genericTestPlanExecutionManager: GenericTestPlanExecutionManager,
    private readonly actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    this.genericTestPlanExecutionManager.canDeleteExecutions$
      .pipe(takeUntil(this.unsub$))
      .subscribe((canDelete) => {
        this.gridService.setColumnVisibility(GridColumnId.delete, canDelete);
      });
  }

  ngOnInit() {
    const testPlanItemId = this.dialogRef.data.testPlanItemId;
    this.gridService.setServerUrl(['test-plan-item', testPlanItemId.toString(), 'executions']);
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  showMassDeleteExecutionDialog() {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        filter((rows: DataRow[]) => this.canDeleteExecution(rows)),
        concatMap((rows: DataRow[]) => this.openDeleteExecutionDialog(rows)),
        filter(({ confirmDelete }) => confirmDelete),
        tap(() => this.gridService.beginAsyncOperation()),
        concatMap(({ rows }) => this.removeExecutionsServerSide(rows)),
        catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
        finalize(() => this.gridService.completeAsyncOperation()),
      )
      .subscribe(() => this.gridService.refreshData());
  }

  private canDeleteExecution(rows: DataRow[]) {
    return (
      rows.length > 0 &&
      !rows.some((row) => row.data.boundToBlockingMilestone) &&
      !rows.some((row) => !(row.simplePermissions as CampaignPermissions).canDeleteExecution)
    );
  }

  private openDeleteExecutionDialog(
    rows: DataRow[],
  ): Observable<{ confirmDelete: boolean; rows: DataRow[] }> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.campaign-workspace.dialog.title.mass-remove-execution',
      messageKey: 'sqtm-core.campaign-workspace.dialog.message.mass-remove-execution',
      level: 'DANGER',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map((confirmDelete) => ({ confirmDelete, rows })),
    );
  }

  private removeExecutionsServerSide(rows: DataRow[]) {
    const executionIds = rows.map((row) => row.data.executionId);

    return this.restService.delete([
      'test-plan-item',
      this.dialogRef.data.testPlanItemId.toString(),
      'execution',
      executionIds.join(','),
    ]);
  }
}

export interface TestPlanItemExecutionHistoryDialogConfiguration {
  testPlanItemId: number;
}

function tableDefinition(): GridDefinition {
  return grid('test-plan-item-execution-history')
    .withColumns([
      indexColumn().withViewport('leftViewport'),
      withLinkColumn(GridColumnId.executionOrder, {
        kind: 'link',
        baseUrl: '/execution',
        columnParamId: 'executionId',
        saveGridStateBeforeNavigate: true,
      })
        .withI18nKey('sqtm-core.entity.execution-plan.execution-number.short')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.execution-number.long')
        .changeWidthCalculationStrategy(new Fixed(60)),
      executionModeColumn(GridColumnId.inferredExecutionMode)
        .withI18nKey('sqtm-core.entity.execution.mode.label')
        .withTitleI18nKey('sqtm-core.entity.execution.mode.label')
        .changeWidthCalculationStrategy(new Fixed(60)),
      textColumn(GridColumnId.executionName)
        .withI18nKey('sqtm-core.entity.name')
        .changeWidthCalculationStrategy(new Extendable(150, 0.2)),
      testCaseImportanceColumn(GridColumnId.importance)
        .withI18nKey('sqtm-core.entity.test-case.importance.label-short-dot')
        .withTitleI18nKey('sqtm-core.entity.test-case.importance.label')
        .isEditable(false)
        .changeWidthCalculationStrategy(new Fixed(80)),
      textColumn(GridColumnId.datasetName)
        .withI18nKey('sqtm-core.entity.dataset.label.short')
        .withTitleI18nKey('sqtm-core.entity.dataset.label.singular')
        .changeWidthCalculationStrategy(new Fixed(100)),
      centredTextColumn(GridColumnId.successRate)
        .withRenderer(SuccessRateComponent)
        .withI18nKey('sqtm-core.entity.execution-plan.success-rate.label.short')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.success-rate.label.long')
        .changeWidthCalculationStrategy(new Fixed(60)),
      executionStatusColumn(GridColumnId.executionStatus)
        .withI18nKey('sqtm-core.entity.execution.status.label')
        .withTitleI18nKey('sqtm-core.entity.execution.status.long-label')
        .changeWidthCalculationStrategy(new Fixed(60)),
      numericColumn(GridColumnId.issueCount)
        .withI18nKey('sqtm-core.entity.execution-plan.ano-number.short')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.ano-number.long')
        .changeWidthCalculationStrategy(new Fixed(60)),
      textColumn(GridColumnId.user)
        .withI18nKey('sqtm-core.generic.label.user')
        .changeWidthCalculationStrategy(new Extendable(100, 0.2)),
      dateTimeColumn(GridColumnId.lastExecutedOn)
        .withSortFunction(sortDate)
        .withI18nKey('sqtm-core.entity.execution-plan.last-execution.label.short-dot')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.last-execution.label.long')
        .changeWidthCalculationStrategy(new Extendable(130, 0.2)),
      deleteExecutionColumn(),
    ])
    .server()
    .withRowConverter(executionConverter)
    .disableRightToolBar()
    .withStyle(new StyleDefinitionBuilder().enableInitialLoadAnimation().showLines())
    .withRowHeight(35)
    .build();
}

function executionConverter(
  literals: Partial<DataRow>[],
  projectDataMap: ProjectDataMap,
): DataRow[] {
  const executions = literals.map((li) => ({
    ...li,
    type: SquashTmDataRowType.Execution,
    data: {
      ...li.data,
      executionOrder: li.data.executionOrder + 1,
    },
  }));
  return convertSqtmLiterals(executions, projectDataMap);
}
