import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  ViewContainerRef,
} from '@angular/core';
import { AbstractExecutionStatusesCell } from '../../../../pages/campaign-workspace/campaign-workspace/components/test-plan/abstract-execution-statuses-cell';
import { Observable } from 'rxjs';
import { Overlay } from '@angular/cdk/overlay';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import {
  ActionErrorDisplayService,
  ColumnDefinitionBuilder,
  ExecutionStatusKeys,
  Fixed,
  GridColumnId,
  GridService,
  ListPanelItem,
  ReferentialDataService,
  RestService,
  TableValueChange,
  WorkspaceCommonModule,
} from 'sqtm-core';
import { catchError, map, take, takeUntil } from 'rxjs/operators';
import { AsyncPipe } from '@angular/common';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { TestPlanOwnerView } from '../../../../pages/campaign-workspace/test-plan-owner-view';
import { TEST_PLAN_OWNER_VIEW } from '../../../../pages/campaign-workspace/campaign-workspace.constant';

@Component({
  selector: 'sqtm-app-test-plan-item-execution-statuses-cell-renderer',
  templateUrl: './test-plan-item-execution-statuses-cell-renderer.component.html',
  styleUrl: './test-plan-item-execution-statuses-cell-renderer.component.less',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [AsyncPipe, TranslateModule, NzToolTipModule, WorkspaceCommonModule],
})
export class TestPlanItemExecutionStatusesCellRendererComponent extends AbstractExecutionStatusesCell {
  canEdit$: Observable<boolean>;
  panelItems$: Observable<ListPanelItem[]>;

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    public readonly overlay: Overlay,
    public readonly vcr: ViewContainerRef,
    public readonly translateService: TranslateService,
    public readonly restService: RestService,
    public readonly actionErrorDisplayService: ActionErrorDisplayService,
    @Inject(TEST_PLAN_OWNER_VIEW) public viewService: TestPlanOwnerView,
    public referentialDataService: ReferentialDataService,
  ) {
    super(
      grid,
      cdRef,
      overlay,
      vcr,
      translateService,
      restService,
      actionErrorDisplayService,
      referentialDataService,
    );

    this.canEdit$ = this.viewService.canUpdateExecutionStatus$.pipe(takeUntil(this.unsub$));

    this.panelItems$ = this.viewService.componentData$.pipe(
      takeUntil(this.unsub$),
      map((componentData) => componentData.projectData),
      map((projectData) => this.getFilteredExecutionStatusKeys(projectData)),
      map((statuses: string[]) => this.asListItemOptions(statuses)),
    );
  }

  change(executionStatusKey: string) {
    this.authenticatedUser$.pipe(take(1)).subscribe((authenticatedUser) => {
      this.viewService
        .updateExecutionStatus(
          executionStatusKey as ExecutionStatusKeys,
          this.row.data[GridColumnId.testPlanItemId],
        )
        .pipe(catchError((error) => this.actionErrorDisplayService.handleActionError(error)))
        .subscribe(() => {
          const changes: TableValueChange[] = [
            { columnId: this.columnDisplay.id, value: executionStatusKey },
            { columnId: GridColumnId.assigneeFullName, value: this.getUser(authenticatedUser) },
            { columnId: GridColumnId.lastExecutedOn, value: new Date() },
          ];

          this.grid.editRows([this.row.id], changes);
          this.close();
        });
    });
  }

  isExploratoryExecution() {
    return this.row.data[GridColumnId.inferredExecutionMode] === 'EXPLORATORY';
  }
}

export function testPlanItemExecutionStatusesColumn(): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(GridColumnId.executionStatus)
    .withRenderer(TestPlanItemExecutionStatusesCellRendererComponent)
    .withI18nKey('sqtm-core.entity.execution.status.label')
    .withAssociatedFilter()
    .changeWidthCalculationStrategy(new Fixed(70))
    .withHeaderPosition('left');
}
