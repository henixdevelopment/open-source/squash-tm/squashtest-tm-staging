import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {
  DataRow,
  DefaultGridDraggedContentComponent,
  DRAG_AND_DROP_DATA,
  DragAndDropData,
  GridColumnId,
  GridService,
} from 'sqtm-core';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { AsyncPipe } from '@angular/common';

const DELETED_TC_KEY = 'sqtm-core.generic.label.deleted.masculine';

@Component({
  selector: 'sqtm-app-test-plan-dragged-content',
  template: `
    <div class="rows">
      @if (grid.isSortedOrFiltered$ | async) {
        <i
          nz-icon
          nzType="sqtm-core-infolist-item:unavailable"
          nzTheme="outline"
          class="cannot-drag-icon"
          [attr.data-test-element-id]="'cannot-drag-icon'"
        ></i>
      } @else {
        @for (dataRow of dataRows; track dataRow) {
          <div [attr.data-test-grid-row-dragged-content]="dataRow.id" class="row txt-ellipsis">
            {{ getText(dataRow) }}
          </div>
        }
      }
    </div>
  `,
  styleUrl: './test-plan-dragged-content.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [NzIconModule, AsyncPipe],
})
export class TestPlanDraggedContentComponent extends DefaultGridDraggedContentComponent {
  constructor(
    @Inject(DRAG_AND_DROP_DATA) public dragAnDropData: DragAndDropData,
    public readonly grid: GridService,
    public readonly translateService: TranslateService,
  ) {
    super(dragAnDropData, grid);
  }

  getText(dataRow: Readonly<DataRow>): string {
    return dataRow.data[GridColumnId.testCaseName] ?? this.translateService.instant(DELETED_TC_KEY);
  }
}
