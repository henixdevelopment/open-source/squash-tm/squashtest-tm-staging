import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  signal,
  ViewContainerRef,
} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ActionErrorDisplayService,
  CampaignPermissions,
  ColumnDefinitionBuilder,
  DialogConfiguration,
  DialogReference,
  DialogService,
  Fixed,
  GridColumnId,
  GridService,
  GridWithStatePersistence,
  RestService,
  SprintReqVersionTestPlanItem,
} from 'sqtm-core';
import { TranslateModule } from '@ngx-translate/core';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { AsyncPipe } from '@angular/common';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { catchError, map, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { ExecutionRunnerOpenerService } from '../../../../pages/execution/execution-runner/services/execution-runner-opener.service';
import {
  TestPlanItemExecutionHistoryComponent,
  TestPlanItemExecutionHistoryDialogConfiguration,
} from '../../dialogs/test-plan-item-execution-history/test-plan-item-execution-history.component';
import {
  GENERIC_TEST_PLAN_EXECUTION_MANAGER,
  GenericTestPlanExecutionManager,
} from '../../generic-test-plan-execution-manager';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sqtm-app-test-plan-item-play-cell-renderer',
  standalone: true,
  imports: [TranslateModule, NzToolTipModule, NzIconModule, AsyncPipe, NzPopoverModule],
  templateUrl: './test-plan-item-play-cell-renderer.component.html',
  styleUrl: './test-plan-item-play-cell-renderer.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestPlanItemPlayCellRendererComponent
  extends AbstractCellRendererComponent
  implements OnDestroy
{
  protected readonly i18nKeys = {
    tooltip: 'sqtm-core.campaign-workspace.test-plan-execution.access-session',
    executeAutomated: 'sqtm-core.campaign-workspace.test-plan.launch-execution.automated',
    executeInPopup: 'sqtm-core.campaign-workspace.test-plan.launch-execution.manual',
    executeInPage: 'sqtm-core.campaign-workspace.test-plan.launch-execution.page',
    showHistory: 'sqtm-core.campaign-workspace.test-plan.launch-execution.history',
  };

  protected get rowData(): SprintReqVersionTestPlanItem {
    return this.row.data as SprintReqVersionTestPlanItem;
  }

  protected get isExploratory(): boolean {
    return this.rowData.inferredExecutionMode === 'EXPLORATORY';
  }

  protected get isAutomated(): boolean {
    return this.rowData.inferredExecutionMode === 'AUTOMATED';
  }

  private readonly unsub$ = new Subject<void>();

  protected readonly $popoverVisible = signal(false);

  private readonly $serviceAllowsExecution = toSignal(
    this.genericTestPlanExecutionManager.canExecute$,
  );

  private executionHistoryDialogReference: DialogReference<unknown> | null = null;

  constructor(
    gridService: GridService,
    changeDetectorRef: ChangeDetectorRef,
    private readonly gridWithStatePersistence: GridWithStatePersistence,
    private readonly actionErrorDisplayService: ActionErrorDisplayService,
    private readonly router: Router,
    private readonly restService: RestService,
    private readonly executionRunnerOpenerService: ExecutionRunnerOpenerService,
    private readonly viewContainerRef: ViewContainerRef,
    private readonly dialogService: DialogService,
    @Inject(GENERIC_TEST_PLAN_EXECUTION_MANAGER)
    private readonly genericTestPlanExecutionManager: GenericTestPlanExecutionManager,
  ) {
    super(gridService, changeDetectorRef);
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();

    if (this.executionHistoryDialogReference) {
      this.executionHistoryDialogReference.close();
    }
  }

  get canExecute(): boolean {
    const campaignPermissions = this.row.simplePermissions as CampaignPermissions;
    return campaignPermissions.canExecute && this.$serviceAllowsExecution();
  }

  startExecutionInPage() {
    if (!this.canExecute) {
      return;
    }

    const { testPlanItemId, inferredExecutionMode } = this.rowData;

    this.createManualExecution(testPlanItemId).subscribe((executionId) => {
      this.gridWithStatePersistence.saveSnapshot();

      if (inferredExecutionMode !== 'EXPLORATORY') {
        this.router.navigate(['execution', executionId]);
      } else {
        this.router.navigate(['execution', executionId, 'charter']);
      }
    });
  }

  startExecutionInPopup() {
    if (!this.canExecute) {
      return;
    }

    this.$popoverVisible.set(false);

    this.createManualExecution(this.rowData.testPlanItemId).subscribe((executionId) => {
      this.grid.refreshData();
      this.executionRunnerOpenerService.openExecutionPrologue(executionId);
    });
  }

  private createManualExecution(testPlanItemId: number): Observable<number> {
    if (testPlanItemId == null) {
      throw Error(`Unable to start execution for TestPlanItem #${testPlanItemId}`);
    }

    return this.restService
      .post<{
        executionId: number;
      }>(['test-plan-item', testPlanItemId.toString(), 'new-manual-execution'], {})
      .pipe(
        map((responseBody) => responseBody.executionId),
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
      );
  }

  accessSessionOverview() {
    this.router.navigate(['session-overview', this.row.data.overviewId]);
  }

  displayExecutionHistory() {
    this.$popoverVisible.set(false);

    const testPlanItemId = this.row.data.testPlanItemId;

    const dialogConfiguration: DialogConfiguration<TestPlanItemExecutionHistoryDialogConfiguration> =
      {
        id: 'test-plan-execution-history-dialog',
        viewContainerReference: this.viewContainerRef,
        component: TestPlanItemExecutionHistoryComponent,
        width: 1010,
        data: {
          testPlanItemId,
        },
      };

    const dialogReference = this.dialogService.openDialog(dialogConfiguration);

    dialogReference.dialogClosed$.pipe(takeUntil(this.unsub$)).subscribe(() => {
      this.grid.refreshData();
      this.executionHistoryDialogReference = null;
    });

    this.executionHistoryDialogReference = dialogReference;
  }
}

export function testPlanItemPlayColumn(): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(GridColumnId.startExecution)
    .withRenderer(TestPlanItemPlayCellRendererComponent)
    .withLabel('')
    .disableSort()
    .changeWidthCalculationStrategy(new Fixed(40))
    .withViewport('rightViewport');
}
