import { Observable } from 'rxjs';
import { InjectionToken } from '@angular/core';

export interface GenericTestPlanItemDeleter {
  deleteTestPlanItems(itemIds: number[]): Observable<unknown>;

  allowTestPlanItemDeletion(): boolean;
}

export const GENERIC_TEST_PLAN_ITEM_DELETER = new InjectionToken<GenericTestPlanItemDeleter>(
  'GenericTestPlanItemDeleter',
);
