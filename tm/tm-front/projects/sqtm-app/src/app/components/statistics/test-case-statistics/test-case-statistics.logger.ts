import { sqtmAppLogger } from '../../../app-logger';

export const testCaseStatisticsLogger = sqtmAppLogger.compose('test-case-statistics');
