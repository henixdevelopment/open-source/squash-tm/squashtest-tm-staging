import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import {
  CampaignExecutionScopeFilter,
  CampaignExecutionScopeFilterKeys,
  Conclusiveness,
  ConclusivenessKeys,
  ConclusivenessStatusCount,
  ExecutionStatus,
  ExecutionStatusCount,
  ExecutionStatusKeys,
  ExecutionStatusLevelEnumItem,
  FilterOperation,
  FilterValueKind,
  I18nEnumItem,
  LevelEnumItem,
  Logger,
  SimpleFilter,
  StatisticsBundle,
  TerminalExecutionStatus,
  TestCaseImportanceKeys,
  TestCaseImportanceLevelEnumItem,
  TestCaseWeight,
} from 'sqtm-core';
import { campaignStatisticsLogger } from '../campaign-statistics.logger';
import { AbstractStatisticsPanelComponent } from '../../abstract-statistics-panel.component';
import { Layout, newPlot } from 'plotly.js';
import * as _ from 'lodash';

const logger = campaignStatisticsLogger.compose('CampaignStatisticsPanelComponent');

@Component({
  selector: 'sqtm-app-campaign-statistics-charts-panel',
  templateUrl: './campaign-statistics-charts-panel.component.html',
  styleUrls: ['./campaign-statistics-charts-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CampaignStatisticsChartsPanelComponent
  extends AbstractStatisticsPanelComponent
  implements OnInit, AfterViewInit, OnDestroy
{
  @ViewChild('statusChart', { read: ElementRef })
  statusChart: ElementRef;

  @ViewChild('conclusivenessChart', { read: ElementRef })
  conclusivenessChart: ElementRef;

  @ViewChild('conclusivenessChartWrapper', { read: ElementRef })
  conclusivenessChartWrapper: ElementRef;

  @ViewChild('conclusivenessLegendWrapper', { read: ElementRef })
  conclusivenessLegendWrapper: ElementRef;

  @ViewChild('importanceChart', { read: ElementRef })
  importanceChart: ElementRef;

  @Input()
  set statistics(value: StatisticsBundle) {
    logger.debug('new value in campaign/iteration statistics :', [value]);
    const iterationStatisticsBundle = value;
    this._testCaseStatusStatistics = iterationStatisticsBundle.testCaseStatusStatistics;
    this._testCaseNonExecutedImportanceStatistics =
      iterationStatisticsBundle.nonExecutedTestCaseImportanceStatistics;
    this._testCaseSuccessRateStatistics =
      iterationStatisticsBundle.testCaseSuccessRateStatistics.conclusiveness;
    this.updateAllCharts(false);
  }

  @Input()
  lastExecutionScope: boolean;

  @Input()
  disabledExecutionStatus: ExecutionStatusKeys[];

  private _testCaseStatusStatistics: ExecutionStatusCount;
  private _testCaseNonExecutedImportanceStatistics: { [K in TestCaseImportanceKeys]: number };
  private _testCaseSuccessRateStatistics: {
    [K in TestCaseImportanceKeys]: ConclusivenessStatusCount;
  };

  protected readonly CUSTOM_CHART_MARGIN = {
    t: 28,
    b: 42,
    l: 10,
    r: 0,
  };

  readonly SUCCESS_COLOR = '#006f57';
  readonly FAILURE_COLOR = '#cb1524';
  readonly UNDEFINED_COLOR = '#b8b8b8';

  readonly TestCaseImportance = TestCaseWeight;

  constructor(
    protected translateService: TranslateService,
    protected router: Router,
    protected renderer: Renderer2,
    protected zone: NgZone,
  ) {
    super(translateService, router, renderer, zone);
  }

  private createStatusChart(executionScopeFilter: SimpleFilter) {
    this.removeDisabledStatuses();
    const keys = Object.keys(this._testCaseStatusStatistics);
    const values = Object.values<number>(this._testCaseStatusStatistics);
    const colors = keys.map((k) => ExecutionStatus[k].color);
    const labels = keys
      .map((k) => ExecutionStatus[k].i18nKey)
      .map((i18n) => this.translateService.instant(i18n));
    const orderedStatus = keys.map((k) => ExecutionStatus[k]);
    const filters: SimpleFilter[][] = this.buildStatusFilters(orderedStatus, executionScopeFilter);
    this.plotPieChart({
      titleKey: 'sqtm-core.campaign-workspace.campaign-page.statistics.charts.status.title',
      elementRef: this.statusChart,
      colors,
      labels,
      values,
      customData: filters,
    });
  }

  private removeDisabledStatuses() {
    const filteredOutKeys = Object.keys(this._testCaseStatusStatistics).filter(
      (statusKey) =>
        this.isDisabledExecutionStatus(statusKey) ||
        this.hideSomeStatusesWithNoStatistics(statusKey),
    ) as ExecutionStatusKeys[];

    filteredOutKeys.forEach((key) => delete this._testCaseStatusStatistics[key]);
  }

  private isDisabledExecutionStatus(statusKey: string): boolean {
    return this.disabledExecutionStatus.includes(ExecutionStatus[statusKey].id);
  }
  private hideSomeStatusesWithNoStatistics(statusKey: string): boolean {
    return (
      (statusKey === ExecutionStatus.SKIPPED.id || statusKey === ExecutionStatus.CANCELLED.id) &&
      this._testCaseStatusStatistics[statusKey] === 0
    );
  }

  private buildStatusFilters(
    orderedStatus: LevelEnumItem<ExecutionStatusKeys>[],
    executionScopeFilter: SimpleFilter,
  ): SimpleFilter[][] {
    return orderedStatus
      .map((status: LevelEnumItem<ExecutionStatusKeys>) => this.buildAllStatusFilters(status))
      .map((filter) => [filter, executionScopeFilter]);
  }

  private buildAllStatusFilters(status: LevelEnumItem<ExecutionStatusKeys>) {
    return this.buildOneExecutionStatusFilter(status.id);
  }

  private createNonExecutedImportanceChart(executionScopeFilter: SimpleFilter) {
    const keys = Object.keys(
      this._testCaseNonExecutedImportanceStatistics,
    ) as TestCaseImportanceKeys[];
    const values = Object.values<number>(this._testCaseNonExecutedImportanceStatistics);
    const colors = keys.map((k) => TestCaseWeight[k].chartColor);
    const labels = keys
      .map((k) => TestCaseWeight[k].i18nKey)
      .map((i18n) => this.translateService.instant(i18n));
    const orderedStatus = keys.map((k) => TestCaseWeight[k]);
    const statusFilter: SimpleFilter =
      this.buildMultiValueExecutedStatusFilters(TerminalExecutionStatus);
    const filters: SimpleFilter[][] = this.buildImportanceFilters(orderedStatus).map((f) => [
      f,
      statusFilter,
      executionScopeFilter,
    ]);
    this.plotPieChart({
      titleKey: 'sqtm-core.campaign-workspace.campaign-page.statistics.charts.importance.title',
      elementRef: this.importanceChart,
      colors,
      labels,
      values,
      customData: filters,
    });
  }

  private buildImportanceFilters(
    orderedImportance: LevelEnumItem<TestCaseImportanceKeys>[],
  ): SimpleFilter[] {
    return orderedImportance.map((status: LevelEnumItem<TestCaseImportanceKeys>) =>
      this.buildOneImportanceFilter(status),
    );
  }

  private buildOneImportanceFilter(status: LevelEnumItem<TestCaseImportanceKeys>) {
    return {
      id: 'importance',
      operation: FilterOperation.IN,
      value: {
        kind: 'multiple-discrete-value' as FilterValueKind,
        value: [
          {
            id: status.id,
            i18nLabelKey: status.i18nKey,
          },
        ],
      },
    };
  }

  private buildMultiValueExecutedStatusFilters(
    executionStatus: ExecutionStatusLevelEnumItem<ExecutionStatusKeys>[],
  ): SimpleFilter {
    const orderedStatus: ExecutionStatusLevelEnumItem<ExecutionStatusKeys>[] = executionStatus;
    return {
      id: 'executionStatus',
      operation: FilterOperation.IN,
      value: {
        kind: 'multiple-discrete-value' as FilterValueKind,
        value: orderedStatus.map((s) => ({ id: s.id, i18nLabelKey: s.i18nKey })),
      },
    };
  }

  private createConclusivenessChart() {
    const conclusivenessStatistics = this._testCaseSuccessRateStatistics;
    const data = [
      this.buildOneConclusivenessDomain(
        TestCaseWeight.VERY_HIGH,
        conclusivenessStatistics.VERY_HIGH.SUCCESS,
        conclusivenessStatistics.VERY_HIGH.FAILURE,
        conclusivenessStatistics.VERY_HIGH.NON_CONCLUSIVE,
        0.81,
        0,
      ),
      this.buildOneConclusivenessDomain(
        TestCaseWeight.HIGH,
        conclusivenessStatistics.HIGH.SUCCESS,
        conclusivenessStatistics.HIGH.FAILURE,
        conclusivenessStatistics.HIGH.NON_CONCLUSIVE,
        0.73,
        0.105,
      ),
      this.buildOneConclusivenessDomain(
        TestCaseWeight.MEDIUM,
        conclusivenessStatistics.MEDIUM.SUCCESS,
        conclusivenessStatistics.MEDIUM.FAILURE,
        conclusivenessStatistics.MEDIUM.NON_CONCLUSIVE,
        0.62,
        0.22,
      ),
      this.buildOneConclusivenessDomain(
        TestCaseWeight.LOW,
        conclusivenessStatistics.LOW.SUCCESS,
        conclusivenessStatistics.LOW.FAILURE,
        conclusivenessStatistics.LOW.NON_CONCLUSIVE,
        0.4,
        0.335,
      ),
    ] as any;

    const layout: Partial<Layout> = { ...this.BASE_LAYOUT };
    const config: Partial<Plotly.Config> = { displayModeBar: false };
    newPlot(this.conclusivenessChart.nativeElement, data, layout, config).then((chart) => {
      this.initializeClickAndSearchOnCustomCriteria(chart);
    });
  }

  protected get logger(): Logger {
    return logger;
  }

  protected get searchBaseUrl(): string {
    return 'campaign';
  }

  protected getAllCharts(): ElementRef[] {
    return [this.statusChart, this.importanceChart];
  }

  protected getChartByLineCount(): number {
    return 3;
  }

  protected buildAllCharts(): void {
    const executionScopeFilter: SimpleFilter = this.createFilterOnLastExecutionScope();
    this.createStatusChart(executionScopeFilter);
    this.createConclusivenessChart();
    this.createNonExecutedImportanceChart(executionScopeFilter);
  }

  protected get totalCount(): number {
    return 1;
  }

  protected getCustomCharts(): any[] {
    return [this.conclusivenessChart];
  }

  protected getAllCustomChartWrappers(): ElementRef[] {
    return [this.conclusivenessChartWrapper];
  }

  protected getAllCustomChartsLegends(): ElementRef[] {
    return [this.conclusivenessLegendWrapper];
  }

  protected getSideLegendThreshold() {
    return 1700;
  }

  protected getSingleColumnThreshold() {
    return 880;
  }

  private buildOneConclusivenessDomain(
    importance: TestCaseImportanceLevelEnumItem<TestCaseImportanceKeys>,
    success: number,
    failure: number,
    nonConclusive: number,
    hole: number,
    domainShrink: number,
  ) {
    const total = success + failure + nonConclusive;
    if (total > 0) {
      return this.buildOneFilledConclusivenessDomain(
        importance,
        success,
        failure,
        nonConclusive,
        hole,
        domainShrink,
      );
    } else {
      const importanceLabel = this.translateService.instant(importance.i18nKey);
      const emptySelectionLabel = this.translateService.instant(
        'sqtm-core.campaign-workspace.campaign-page.statistics.charts.success-rate.empty',
        { importanceLabel },
      );
      return this.buildEmptyDomain(
        this.translateService.instant(importance.i18nKey),
        domainShrink,
        hole,
        emptySelectionLabel,
      );
    }
  }

  private buildOneFilledConclusivenessDomain(
    importance: TestCaseImportanceLevelEnumItem<TestCaseImportanceKeys>,
    success: number,
    failure: number,
    nonConclusive: number,
    hole: number,
    domainShrink: number,
  ) {
    const name = this.translateService.instant(importance.i18nKey);
    const successLabel = this.translateService.instant(Conclusiveness.SUCCESS.i18nKey);
    const failureLabel = this.translateService.instant(Conclusiveness.FAILURE.i18nKey);
    const nonConclusiveLabel = this.translateService.instant(Conclusiveness.NON_CONCLUSIVE.i18nKey);
    const values = [success, failure, nonConclusive];
    const labels = [successLabel, failureLabel, nonConclusiveLabel];
    const colors = [this.SUCCESS_COLOR, this.FAILURE_COLOR, this.UNDEFINED_COLOR];
    const filters = this.buildOneConclusivenessDomainFilters(importance);
    return this.buildOneFilledDomain(values, labels, name, domainShrink, hole, colors, filters);
  }

  private buildOneConclusivenessDomainFilters(
    importance: TestCaseImportanceLevelEnumItem<TestCaseImportanceKeys>,
  ) {
    const terminalAndSuccess = this.buildOneTerminalAndConclusiveStatusFilter(
      Conclusiveness.SUCCESS,
    );
    const terminalAndFailure = this.buildOneTerminalAndConclusiveStatusFilter(
      Conclusiveness.FAILURE,
    );
    const terminalAndNonConclusive = this.buildOneTerminalAndConclusiveStatusFilter(
      Conclusiveness.NON_CONCLUSIVE,
    );
    const importanceFilter = this.buildOneImportanceFilter(importance);

    const filters: SimpleFilter[][] = [
      [importanceFilter, terminalAndSuccess],
      [importanceFilter, terminalAndFailure],
      [importanceFilter, terminalAndNonConclusive],
    ];

    return filters;
  }

  private buildOneTerminalAndConclusiveStatusFilter(
    conclusiveness: LevelEnumItem<ConclusivenessKeys>,
  ) {
    return this.buildMultiValueExecutedStatusFilters(
      Object.values<ExecutionStatusLevelEnumItem<ExecutionStatusKeys>>(ExecutionStatus).filter(
        (e) => e.terminal && e.conclusiveness === conclusiveness,
      ),
    );
  }

  /***
   * Get execution status filters by id or canonicalStatus. For automated statuses, the attribute returns the
   * equivalent canonical status. E.g : "Blocked" -> "Blocked" and "Error" -> "Blocked"
   * @param executionStatus
   * @private
   */
  private buildOneExecutionStatusFilter(executionStatus?: ExecutionStatusKeys) {
    return this.buildMultiValueExecutedStatusFilters(
      Object.values<ExecutionStatusLevelEnumItem<ExecutionStatusKeys>>(ExecutionStatus).filter(
        (e) => e.id === executionStatus || e.canonicalStatus === executionStatus,
      ),
    );
  }

  protected switchToMultipleColumnsLayout() {
    this.renderer.addClass(this.panel.nativeElement, 'grid-three-columns');
    this.renderer.removeClass(this.panel.nativeElement, 'grid-one-column');
  }

  protected switchToOneColumnLayout() {
    this.renderer.removeClass(this.panel.nativeElement, 'grid-three-columns');
    this.renderer.addClass(this.panel.nativeElement, 'grid-one-column');
  }

  calculateGlobalSuccessRate() {
    const conclusiveness: ConclusivenessStatusCount[] = Object.values(
      this._testCaseSuccessRateStatistics,
    );
    const success = conclusiveness.map((c) => c.SUCCESS).reduce(_.add, 0);
    const total = conclusiveness
      .map((c) => Object.values<number>(c))
      .flat()
      .reduce(_.add, 0);
    return Math.floor((success / total) * 100) || 0; // if Nan because no executed test plan item, let's assign 0%
  }

  calculateGlobalFailureRate() {
    const conclusiveness: ConclusivenessStatusCount[] = Object.values(
      this._testCaseSuccessRateStatistics,
    );
    const success = conclusiveness.map((c) => c.FAILURE).reduce(_.add, 0);
    const total = conclusiveness
      .map((c) => Object.values<number>(c))
      .flat()
      .reduce(_.add, 0);
    return Math.ceil((success / total) * 100) || 0;
  }

  initializeClickAndSearchOnCustomCriteria(chart) {
    const executionScopeFilter: SimpleFilter = this.createFilterOnLastExecutionScope();
    chart.on('plotly_click', ($event: Plotly.PlotMouseEvent) => {
      const customData = $event.points[0]?.customdata?.[0]; // Because slices may not contain custom data!
      if (customData) {
        $event.points[0].customdata[0].push(executionScopeFilter);
        this.searchOnCriteria($event.points[0].customdata[0]);
      }
    });
  }

  public createFilterOnLastExecutionScope(): SimpleFilter {
    const enumItem: I18nEnumItem<CampaignExecutionScopeFilterKeys> = this.lastExecutionScope
      ? CampaignExecutionScopeFilter.LAST_EXECUTED
      : CampaignExecutionScopeFilter.ALL;
    return {
      id: 'itemTestPlanExecutionScope',
      operation: FilterOperation.IN,
      value: {
        kind: 'multiple-discrete-value' as FilterValueKind,
        value: [{ id: enumItem.id, i18nLabelKey: enumItem.i18nKey }],
      },
    };
  }
}
