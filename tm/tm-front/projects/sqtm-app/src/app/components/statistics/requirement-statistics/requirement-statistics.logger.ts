import { sqtmAppLogger } from '../../../app-logger';

export const requirementStatisticsLogger = sqtmAppLogger.compose('requirement-statistics');
