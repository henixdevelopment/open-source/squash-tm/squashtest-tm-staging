import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import {
  ExecutionStatus,
  ExecutionStatusCount,
  ExecutionStatusKeys,
  TestInventoryStatistics,
} from 'sqtm-core';
import * as _ from 'lodash';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-campaign-inventory',
  templateUrl: './campaign-inventory.component.html',
  styleUrls: ['./campaign-inventory.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CampaignInventoryComponent {
  private _inventory: IterationAggregate[];

  ExecutionStatus = ExecutionStatus;

  get extendedInventory() {
    return this._inventory;
  }

  @Input()
  set inventory(inventory: TestInventoryStatistics[]) {
    const iterationAggregates: IterationAggregate[] = this.getIterationAggregates(inventory);
    const resumeRow = this.getResumeRow(iterationAggregates);
    iterationAggregates.push(resumeRow);
    this._inventory = iterationAggregates;
  }

  @Input()
  disabledExecutionStatus: ExecutionStatusKeys[];

  constructor(private readonly translateService: TranslateService) {}

  private getExecutionRate(executed: number, total: number) {
    return Math.floor((executed / total) * 100) || 0;
  }

  private getIterationAggregates(inventory: TestInventoryStatistics[]) {
    return inventory.map((iteration) => {
      const total: number = Object.values<number>(iteration.statistics).reduce(_.add);
      const executed: number = Object.entries<number>(iteration.statistics)
        .filter((e) => ExecutionStatus[e[0]].terminal)
        .map((e) => e[1])
        .reduce(_.add);
      return {
        isTotal: false,
        total,
        executed,
        executionRate: this.getExecutionRate(executed, total),
        iterationName: iteration.name,
        ...iteration.statistics,
      };
    });
  }

  private getResumeRow(iterationAggregates: IterationAggregate[]) {
    const resumeRow = iterationAggregates.reduce(
      (totalAggregate, iterationAggregate: IterationAggregate) => {
        return {
          ...totalAggregate,
          total: totalAggregate.total + iterationAggregate.total,
          executed: totalAggregate.executed + iterationAggregate.executed,
          FAILURE: totalAggregate.FAILURE + iterationAggregate.FAILURE,
          SUCCESS: totalAggregate.SUCCESS + iterationAggregate.SUCCESS,
          RUNNING: totalAggregate.RUNNING + iterationAggregate.RUNNING,
          READY: totalAggregate.READY + iterationAggregate.READY,
          BLOCKED: totalAggregate.BLOCKED + iterationAggregate.BLOCKED,
          UNTESTABLE: totalAggregate.UNTESTABLE + iterationAggregate.UNTESTABLE,
          SETTLED: totalAggregate.SETTLED + iterationAggregate.SETTLED,
        };
      },
      {
        total: 0,
        executed: 0,
        executionRate: 0,
        iterationName: this.translateService.instant('sqtm-core.generic.label.total'),
        FAILURE: 0,
        SUCCESS: 0,
        RUNNING: 0,
        READY: 0,
        BLOCKED: 0,
        UNTESTABLE: 0,
        SETTLED: 0,
        isTotal: true,
      },
    );

    resumeRow.executionRate = this.getExecutionRate(resumeRow.executed, resumeRow.total);
    return resumeRow;
  }
}

export interface IterationAggregate extends ExecutionStatusCount {
  total: number;
  executed: number;
  executionRate?: number;
  iterationName: string;
  isTotal: boolean;
}
