import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { LevelEnumItem, TestCaseImportanceKeys, TestCaseWeight } from 'sqtm-core';

@Component({
  selector: 'sqtm-app-denormalized-importance',
  templateUrl: './denormalized-importance.component.html',
  styleUrls: ['./denormalized-importance.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DenormalizedImportanceComponent {
  private _importance: LevelEnumItem<TestCaseImportanceKeys>;

  @Input()
  set importanceKey(key: TestCaseImportanceKeys) {
    this._importance = TestCaseWeight[key];
  }

  get importance() {
    return this._importance;
  }

  get importanceKey() {
    return this._importance.id;
  }

  get importanceI18n() {
    return this._importance.i18nKey;
  }
}
