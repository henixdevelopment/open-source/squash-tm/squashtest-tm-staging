import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Report, StandardReportCategory } from 'sqtm-core';

@Component({
  selector: 'sqtm-app-report-list',
  templateUrl: './report-list.component.html',
  styleUrls: ['./report-list.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReportListComponent {
  get reportDisplayed(): ReportDisplay[] {
    return this._reports;
  }

  @Input()
  set reports(reports: Report[]) {
    this._reports = reports.map((report) => ({
      ...report,
      imageUrl: this.getImageUrl(report),
    }));
  }

  private _reports: ReportDisplay[];

  @Output()
  reportChanged = new EventEmitter<string>();

  private getImageUrl(report: Report) {
    return report.category === StandardReportCategory.PREPARATION_PHASE
      ? 'assets/sqtm-core/img/reports/report_spreadsheet.png'
      : 'assets/sqtm-core/img/reports/report_word.png';
  }

  changeReport(report: ReportDisplay) {
    this.reportChanged.emit(report.id);
  }
}

interface ReportDisplay extends Report {
  imageUrl: string;
}
