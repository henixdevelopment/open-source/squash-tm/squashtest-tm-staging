import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { AbstractFormPicker, ReportNodeReference } from '../abstract-form-picker';
import { Overlay } from '@angular/cdk/overlay';
import {
  buildTreePickerDialogDefinition,
  DataRow,
  DialogService,
  TreePickerDialogConfiguration,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { filter, map, take, takeUntil, tap } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-form-campaign-tree-picker',
  templateUrl: './form-campaign-tree-picker.component.html',
  styleUrls: ['./form-campaign-tree-picker.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: FormCampaignTreePickerComponent,
    },
  ],
})
export class FormCampaignTreePickerComponent
  extends AbstractFormPicker
  implements ControlValueAccessor
{
  @Input()
  enabledMultiSelection = true;

  @Input()
  restrictedRowType: string;

  selectedDataRows: DataRow[] = [];

  constructor(
    overlay: Overlay,
    private dialogService: DialogService,
    private changeDetectorRef: ChangeDetectorRef,
    private translateService: TranslateService,
  ) {
    super(overlay);
  }

  protected updateSelectedNames() {
    if (this.enabledMultiSelection) {
      this.concatenatedNames = this.translateService.instant(
        'sqtm-core.custom-report-workspace.create-report.element-selected',
        { nb: this.selectedIds.length.toString() },
      );
    } else {
      if (this.selectedDataRows.length > 0) {
        this.concatenatedNames = this.selectedDataRows[0].data['NAME'];
      } else {
        this.concatenatedNames = this.translateService.instant(
          'sqtm-core.custom-report-workspace.create-report.element-selected',
          { nb: this.selectedIds.length.toString() },
        );
      }
    }
  }

  handleOpen() {
    const selectedReportNodeReferences = this.selectedIds as ReportNodeReference[];
    const selectedNodes = selectedReportNodeReferences
      .map((r) => this.reportNodeReferenceToRowIdentifier(r))
      .map((r) => r.asString());
    const dialogReference = this.dialogService.openDialog<TreePickerDialogConfiguration, DataRow[]>(
      buildTreePickerDialogDefinition(
        'campaign-picker-dialog',
        selectedNodes,
        this.enabledMultiSelection,
        this.restrictedRowType,
      ),
    );
    dialogReference.dialogClosed$
      .pipe(
        takeUntil(this.unsub$),
        take(1),
        filter((result) => Boolean(result)),
        tap((rows: DataRow[]) => (this.selectedDataRows = rows)),
        map((rows: DataRow[]) => this.convertRowsToReportNodeReference(rows)),
      )
      .subscribe((reportNodeReferences) => {
        this.selectedIds = reportNodeReferences;
        this.onChange(reportNodeReferences);
        this.changeDetectorRef.detectChanges();
      });
  }
}
