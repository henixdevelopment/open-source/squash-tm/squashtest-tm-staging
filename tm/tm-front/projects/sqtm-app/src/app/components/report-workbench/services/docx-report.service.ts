import { Inject, Injectable, NgZone } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import {
  CORE_MODULE_CONFIGURATION,
  DialogService,
  Report,
  RestService,
  SqtmCoreModuleConfiguration,
} from 'sqtm-core';
import { HttpClient } from '@angular/common/http';
import { catchError, concatMap, map } from 'rxjs/operators';
import { customReportWorkspaceLogger } from '../../../pages/custom-report-workspace/custom-report-workspace.logger';
import Docxtemplater from 'docxtemplater';
import { BehaviorSubject, of } from 'rxjs';
import { saveAs } from 'file-saver';
import PizZip from 'pizzip';
import { TranslateService } from '@ngx-translate/core';

declare const openXml: any;
const logger = customReportWorkspaceLogger.compose('DocxReportService');

@Injectable()
export class DocxReportService {
  private _generatingReportSubject = new BehaviorSubject<boolean>(false);
  public generatingReport$ = this._generatingReportSubject.asObservable();

  _keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

  constructor(
    @Inject(APP_BASE_HREF) private baseHref: string,
    @Inject(CORE_MODULE_CONFIGURATION)
    private sqtmCoreModuleConfiguration: SqtmCoreModuleConfiguration,
    private http: HttpClient,
    private ngZone: NgZone,
    private restService: RestService,
    private dialogService: DialogService,
    private translateService: TranslateService,
  ) {}

  generateReport(reportDefinitionId: number, report: Report | string, params: any) {
    const reportId = typeof report === 'string' ? report : report.id;
    this.emitStartGeneratingReport();
    const templateUrl = `${this.baseHref}${this.sqtmCoreModuleConfiguration.backendRootUrl}/reports/${reportId}/views/0/docxtemplate`;
    // {responseType: 'arraybuffer' as any} is mandatory to properly handle binary data
    const templateFileName = this.retrieveTemplateFileNameFromParams(reportDefinitionId, params);
    const requestBody = {
      reportDefinitionId: reportDefinitionId,
      templateFileName: templateFileName,
    };
    this.http
      .post<ReportPayload>(templateUrl, requestBody, { responseType: 'arraybuffer' as any })
      .pipe(
        catchError((err) => {
          this.emitStopGeneratingReport();
          return of(err);
        }),
        concatMap((template) =>
          this.restService.get(['reports', reportId, 'views', '0', 'data', 'docx'], params).pipe(
            map((data: any) => ({ template, data })),
            catchError((err) => {
              this.emitStopGeneratingReport();
              return of(err);
            }),
          ),
        ),
      )
      .subscribe(({ template, data }) => {
        logger.debug(`Data for template report ${reportId}`, [data]);
        let doc;

        try {
          doc = new Docxtemplater(new PizZip(template), { paragraphLoop: true, linebreaks: true });
          doc.setData(data.data).render();
        } catch (error) {
          this.logErrors(error);
          this.emitStopGeneratingReport();
          throw error;
        }
        const output = doc.getZip().generate({ type: 'base64' }); // Output the document using Data-URI

        const html = data.html;

        const docx = new openXml.OpenXmlPackage(output);
        try {
          for (let i = 0; i < html.length; i++) {
            const alt_chunk_id = 'toto' + i;
            const alt_chunk_uri = '/word/' + i + '.html';
            // Add Alternative Format Import Part to document
            // we can't use btoa method to encode html string
            // because we have some errors when an user copy/paste "'" character
            // in description from Word [SQUASH-3407]. So we use old stuff of 1.2x of SquashTm.

            docx.addPart(alt_chunk_uri, 'text/html', 'base64', this.encode(html[i]));

            // Add Alternative Format Import Relationship to the document
            docx
              .mainDocumentPart()
              .addRelationship(
                alt_chunk_id,
                openXml.relationshipTypes.alternativeFormatImport,
                alt_chunk_uri,
                'Internal',
              );
          }
        } catch (error) {
          this.dialogService.openAlert({
            id: 'invalid-file-report-generation-error',
            level: 'DANGER',
            messageKey:
              'sqtm-core.custom-report-workspace.report-definition-view.errors.invalid-file-error',
          });
          this.emitStopGeneratingReport();
          throw error;
        }
        const theContent = docx.saveToBlob();
        saveAs(theContent, `${data.fileName}.${data.format}`);
        this.emitStopGeneratingReport();
      });
  }

  private retrieveTemplateFileNameFromParams(reportDefinitionId: number, params: any) {
    return reportDefinitionId == null || typeof params.json.templateFileName !== 'undefined'
      ? params.json.templateFileName.val
      : '';
  }

  private encode(input) {
    let output = '';
    let chr1, chr2, chr3, enc1, enc2, enc3, enc4;
    let i = 0;

    input = this.utf8_encode(input);

    while (i < input.length) {
      chr1 = input.charCodeAt(i++);
      chr2 = input.charCodeAt(i++);
      chr3 = input.charCodeAt(i++);

      enc1 = chr1 >> 2;
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      enc4 = chr3 & 63;

      if (isNaN(chr2)) {
        enc3 = enc4 = 64;
      } else if (isNaN(chr3)) {
        enc4 = 64;
      }

      output =
        output +
        this._keyStr.charAt(enc1) +
        this._keyStr.charAt(enc2) +
        this._keyStr.charAt(enc3) +
        this._keyStr.charAt(enc4);
    }

    return output;
  }

  private utf8_encode(input: string) {
    input = input.replace(/\r\n/g, '\n');
    let utftext = '';

    for (let n = 0; n < input.length; n++) {
      const c = input.charCodeAt(n);

      if (c < 128) {
        utftext += String.fromCharCode(c);
      } else if (c > 127 && c < 2048) {
        utftext += String.fromCharCode((c >> 6) | 192);
        utftext += String.fromCharCode((c & 63) | 128);
      } else {
        utftext += String.fromCharCode((c >> 12) | 224);
        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
        utftext += String.fromCharCode((c & 63) | 128);
      }
    }

    return utftext;
  }

  private logErrors(errors) {
    if (errors.properties && errors.properties.errors instanceof Array) {
      const errorMessages = errors.properties.errors
        .filter((error) => error.message !== 'Raw tag should be the only text in paragraph') // filter rich text value tag errors
        .map(function (error) {
          return error.properties.explanation;
        })
        .join('<br/>');

      this.dialogService.openAlert({
        id: 'report-generation-error',
        level: 'DANGER',
        messageKey: this.translateService.instant(
          'sqtm-core.custom-report-workspace.report-definition-view.errors.template-generation-error',
          { errors: errorMessages },
        ),
      });

      // errorMessages is a humanly readable message looking like this :
      // 'The tag beginning with "foobar" is unopened'
    }
  }

  private replaceErrors(_key, value) {
    if (value instanceof Error) {
      return Object.getOwnPropertyNames(value).reduce(function (error, key) {
        error[key] = value[key];
        return error;
      }, {});
    }
    return value;
  }

  private emitStartGeneratingReport() {
    this._generatingReportSubject.next(true);
  }

  private emitStopGeneratingReport() {
    this._generatingReportSubject.next(false);
  }

  complete() {
    this._generatingReportSubject.complete();
  }
}

interface ReportPayload {
  data: any;
  html: string[];
  fileName: string;
  format: string;
}
