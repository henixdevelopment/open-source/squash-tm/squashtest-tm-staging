import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { Report, StandardReportCategory } from 'sqtm-core';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export interface ReportWorkbenchState {
  availableReports: EntityState<Report>;
  selectedReport: string;
  showSelectReportPanel: boolean;
}

export const reportEntityAdapter = createEntityAdapter<Report>();

export function initialReportWorkbenchState(): Readonly<ReportWorkbenchState> {
  return {
    availableReports: reportEntityAdapter.getInitialState(),
    selectedReport: null,
    showSelectReportPanel: true,
  };
}

const reportStateSelector = createFeatureSelector<EntityState<Report>>('availableReports');
export const selectReportIdSelector = createFeatureSelector<string>('selectedReport');
export const showSelectReportPanelSelector =
  createFeatureSelector<boolean>('showSelectReportPanel');

const availableReportsSelector = createSelector<
  ReportWorkbenchState,
  [EntityState<Report>],
  Report[]
>(reportStateSelector, reportEntityAdapter.getSelectors().selectAll);

export const availableExecutionPhaseReportsSelector = createSelector<
  ReportWorkbenchState,
  [Report[]],
  Report[]
>(availableReportsSelector, (reports) =>
  reports.filter((r) => r.category === StandardReportCategory.EXECUTION_PHASE),
);

export const availablePreparationPhaseReportsSelector = createSelector<
  ReportWorkbenchState,
  [Report[]],
  Report[]
>(availableReportsSelector, (reports) =>
  reports.filter((r) => r.category === StandardReportCategory.PREPARATION_PHASE),
);

export const availableVariousReportsSelector = createSelector<
  ReportWorkbenchState,
  [Report[]],
  Report[]
>(availableReportsSelector, (reports) =>
  reports.filter((r) => r.category === StandardReportCategory.VARIOUS),
);

export const selectedReportSelector = createSelector(
  reportStateSelector,
  selectReportIdSelector,
  (reportState, selectedReport) => {
    return reportState.entities[selectedReport];
  },
);
