import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { AbstractFormPicker, ReportNodeReference } from '../abstract-form-picker';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Overlay } from '@angular/cdk/overlay';
import {
  buildTreePickerDialogDefinition,
  DataRow,
  DialogService,
  TreePickerDialogConfiguration,
} from 'sqtm-core';
import { filter, map, take, takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-form-requirement-tree-picker',
  templateUrl: './form-requirement-tree-picker.component.html',
  styleUrls: ['./form-requirement-tree-picker.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: FormRequirementTreePickerComponent,
    },
  ],
})
export class FormRequirementTreePickerComponent
  extends AbstractFormPicker
  implements ControlValueAccessor
{
  constructor(
    overlay: Overlay,
    private dialogService: DialogService,
    private changeDetectorRef: ChangeDetectorRef,
    private translateService: TranslateService,
  ) {
    super(overlay);
  }

  protected updateSelectedNames() {
    this.concatenatedNames = this.translateService.instant(
      'sqtm-core.custom-report-workspace.create-report.element-selected',
      { nb: this.selectedIds.length.toString() },
    );
  }

  handleOpen() {
    const selectedReportNodeReferences = this.selectedIds as ReportNodeReference[];
    const selectedNodes = selectedReportNodeReferences
      .map((r) => this.reportNodeReferenceToRowIdentifier(r))
      .map((r) => r.asString());
    const dialogReference = this.dialogService.openDialog<TreePickerDialogConfiguration, DataRow[]>(
      buildTreePickerDialogDefinition('requirement-picker-dialog', selectedNodes),
    );
    dialogReference.dialogClosed$
      .pipe(
        takeUntil(this.unsub$),
        take(1),
        filter((result) => Boolean(result)),
        map((rows) => this.convertRowsToReportNodeReference(rows)),
      )
      .subscribe((reportNodeReferences) => {
        this.selectedIds = reportNodeReferences;
        this.onChange(reportNodeReferences);
        this.changeDetectorRef.detectChanges();
      });
  }

  // private reportNodeReferenceToRowIdentifier(r: ReportNodeReference): EntityRowReference {
  //   const typeMapping: { [K in ReportResType]: SquashTmDataRowType } = {
  //     'requirement-folders': SquashTmDataRowType.RequirementFolder,
  //     'requirements': SquashTmDataRowType.Requirement
  //   };
  //   const squashTmDataRowType = typeMapping[r.restype];
  //   if (!squashTmDataRowType) {
  //     throw Error('No conversion for restype ' + r.restype);
  //   }
  //   return new EntityRowReference(parseInt(r.resid, 10), squashTmDataRowType);
  // }
}
