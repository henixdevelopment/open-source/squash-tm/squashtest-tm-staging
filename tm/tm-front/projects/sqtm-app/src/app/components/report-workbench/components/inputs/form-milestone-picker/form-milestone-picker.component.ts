import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewContainerRef,
} from '@angular/core';
import { AbstractFormPicker } from '../abstract-form-picker';
import { Overlay } from '@angular/cdk/overlay';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import {
  buildSingleMilestonePickerDialogDefinition,
  DialogService,
  Milestone,
  ReferentialDataService,
  SingleSelectMilestoneDialogConfiguration,
} from 'sqtm-core';
import { filter, map, take, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-form-milestone-picker',
  templateUrl: './form-milestone-picker.component.html',
  styleUrls: ['./form-milestone-picker.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: FormMilestonePickerComponent,
    },
  ],
})
export class FormMilestonePickerComponent extends AbstractFormPicker {
  constructor(
    overlay: Overlay,
    private dialogService: DialogService,
    private vcr: ViewContainerRef,
    private changeDetectorRef: ChangeDetectorRef,
    private referentialDataService: ReferentialDataService,
  ) {
    super(overlay);
  }

  protected updateSelectedNames() {
    if (this.selectedIds.length > 0) {
      this.referentialDataService.milestoneModeData$
        .pipe(
          take(1),
          map((data) => data.milestones),
          map((milestones) => milestones.filter((m) => this.selectedIds.includes(m.id))),
        )
        .subscribe((milestones) => {
          this.concatenatedNames = milestones.map((m) => m.label).join(',');
          this.changeDetectorRef.detectChanges();
        });
    } else {
      this.concatenatedNames = '';
    }
  }

  handleOpen() {
    this.referentialDataService.milestoneModeData$.pipe(take(1)).subscribe((data) => {
      const milestoneId =
        this.selectedIds && this.selectedIds.length > 0 ? this.selectedIds[0] : undefined;
      const dialogReference = this.dialogService.openDialog<
        SingleSelectMilestoneDialogConfiguration,
        Milestone
      >(buildSingleMilestonePickerDialogDefinition(data.milestones, { id: milestoneId }));

      dialogReference.dialogClosed$
        .pipe(
          takeUntil(this.unsub$),
          take(1),
          filter((result) => Boolean(result)),
        )
        .subscribe((milestone) => {
          const ids = [milestone.id];
          this.selectedIds = ids;
          this.onChange(ids);
          this.changeDetectorRef.detectChanges();
        });
    });
  }
}
