import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ReportInputsGroup, ReportInputType } from 'sqtm-core';
import { FormGroup, ValidationErrors } from '@angular/forms';

@Component({
  selector: 'sqtm-app-inputs-group-report',
  templateUrl: './inputs-group-report.component.html',
  styleUrls: ['./inputs-group-report.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InputsGroupReportComponent {
  @Input()
  reportInput: ReportInputsGroup;

  @Input()
  subFormGroup: FormGroup;

  @Input()
  errors: ValidationErrors;

  ReportInputType = ReportInputType;
}
