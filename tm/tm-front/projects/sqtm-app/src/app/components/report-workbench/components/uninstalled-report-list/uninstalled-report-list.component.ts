import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { DialogService, UnInstallReport } from 'sqtm-core';

@Component({
  selector: 'sqtm-app-uninstalled-report-list',
  templateUrl: './uninstalled-report-list.component.html',
  styleUrls: ['./uninstalled-report-list.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UninstalledReportListComponent {
  get reportDisplayed(): UnInstallReportDisplay[] {
    return this._reports;
  }

  @Input()
  set reports(reports: UnInstallReport[]) {
    this._reports = reports.map((report) => ({
      ...report,
      imageUrl: 'assets/sqtm-core/icons/assets/sqtm-core-custom-report/premium.svg',
    }));
  }

  private _reports: UnInstallReportDisplay[];

  constructor(private dialogService: DialogService) {}

  changeReport(report: UnInstallReportDisplay) {
    this.dialogService.openAlert({
      id: 'uninstall-report-dialog',
      titleKey: report.labelKey,
      messageKey: report.messageKey,
      level: 'INFO',
    });
  }
}

interface UnInstallReportDisplay extends UnInstallReport {
  imageUrl: string;
}
