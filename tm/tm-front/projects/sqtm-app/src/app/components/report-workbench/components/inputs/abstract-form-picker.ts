import { Subject } from 'rxjs';
import { Directive, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';
import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { DataRow, EntityRowReference, SquashTmDataRowType, toEntityRowReference } from 'sqtm-core';

@Directive()
export abstract class AbstractFormPicker implements OnDestroy, ControlValueAccessor {
  protected overlayRef: OverlayRef;

  get selectedIds(): any[] {
    return this._selectedIds;
  }

  set selectedIds(selectedIds: any[]) {
    this._selectedIds = selectedIds;
    this.updateSelectedNames();
  }

  protected unsub$ = new Subject<void>();

  @ViewChild('button', { read: ElementRef })
  buttonElement: ElementRef;

  private _selectedIds: number[] = [];

  concatenatedNames = '';

  tooltipNames = '';

  touched = false;

  disabled = false;

  protected constructor(protected overlay: Overlay) {}

  ngOnDestroy(): void {
    this.removeOverlay();
    this.unsub$.next();
    this.unsub$.complete();
  }

  protected abstract updateSelectedNames();

  protected onChange = (_ids) => {};

  protected onTouched = () => {};

  registerOnChange(onChangeFunction: any): void {
    this.onChange = onChangeFunction;
  }

  registerOnTouched(onTouchedFunction: any): void {
    this.onTouched = onTouchedFunction;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  writeValue(obj: any): void {
    this.selectedIds = obj;
  }

  markAsTouched() {
    if (!this.touched) {
      this.onTouched();
      this.touched = true;
    }
  }

  protected removeOverlay() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
    }
  }

  protected createOverlayConfig() {
    const positionStrategy = this.overlay
      .position()
      .flexibleConnectedTo(this.buttonElement)
      .withPositions([
        { originX: 'end', overlayX: 'start', originY: 'center', overlayY: 'center', offsetX: 20 },
        {
          originX: 'start',
          overlayX: 'start',
          originY: 'center',
          overlayY: 'center',
          offsetX: -50,
        },
      ]);
    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      width: 680,
      height: 768,
    };
    return overlayConfig;
  }

  /**
   * utility methode used to convert our standard rows to the cumbersome report API...
   * @param rows to convert
   * @protected
   */
  protected convertRowsToReportNodeReference(rows: DataRow[]): ReportNodeReference[] {
    const entityRowReferences = rows
      .map(toEntityRowReference)
      .filter(
        (ref) =>
          ![
            SquashTmDataRowType.RequirementLibrary,
            SquashTmDataRowType.TestCaseLibrary,
            SquashTmDataRowType.CampaignLibrary,
          ].includes(ref.entityType),
      );

    return entityRowReferences.map(AbstractFormPicker.convertEntityReference);
  }

  protected reportNodeReferenceToRowIdentifier(r: ReportNodeReference): EntityRowReference {
    const typeMapping: { [K in ReportResType]: SquashTmDataRowType } = {
      'requirement-folders': SquashTmDataRowType.RequirementFolder,
      requirements: SquashTmDataRowType.Requirement,
      'high-level-requirements': SquashTmDataRowType.HighLevelRequirement,
      'test-case-folders': SquashTmDataRowType.TestCaseFolder,
      'test-cases': SquashTmDataRowType.TestCase,
      'campaign-folders': SquashTmDataRowType.CampaignFolder,
      campaigns: SquashTmDataRowType.Campaign,
      iterations: SquashTmDataRowType.Iteration,
    };
    const squashTmDataRowType = typeMapping[r.restype];
    if (!squashTmDataRowType) {
      throw Error('No conversion for restype ' + r.restype);
    }
    return new EntityRowReference(parseInt(r.resid, 10), squashTmDataRowType);
  }

  private static convertEntityReference(ref: EntityRowReference) {
    return {
      resid: ref.id.toString(),
      restype: AbstractFormPicker.findReportResourceType(ref),
    };
  }

  private static findReportResourceType(ref: EntityRowReference): ReportResType {
    let restype: ReportResType;
    switch (ref.entityType) {
      case SquashTmDataRowType.RequirementFolder:
        restype = 'requirement-folders';
        break;
      case SquashTmDataRowType.Requirement:
        restype = 'requirements';
        break;
      case SquashTmDataRowType.HighLevelRequirement:
        restype = 'high-level-requirements';
        break;
      case SquashTmDataRowType.TestCaseFolder:
        restype = 'test-case-folders';
        break;
      case SquashTmDataRowType.TestCase:
        restype = 'test-cases';
        break;
      case SquashTmDataRowType.Campaign:
        restype = 'campaigns';
        break;
      case SquashTmDataRowType.CampaignFolder:
        restype = 'campaign-folders';
        break;
      case SquashTmDataRowType.Iteration:
        restype = 'iterations';
        break;
      default:
        throw Error('No conversion for type : ' + ref.entityType);
    }
    return restype;
  }
}

export type ReportResType =
  | 'requirements'
  | 'high-level-requirements'
  | 'requirement-folders'
  | 'test-case-folders'
  | 'test-cases'
  | 'campaigns'
  | 'campaign-folders'
  | 'iterations';

export interface ReportNodeReference {
  // id as string... yeah this API sucks severely.
  // Ids are sometimes string, sometime numbers...
  resid: string;
  restype: ReportResType;
}
