import { ChangeDetectionStrategy, Component, Input, ViewContainerRef } from '@angular/core';
import {
  DialogService,
  Permissions,
  ProjectDataMap,
  ReferentialDataService,
  Report,
  ReportDefinitionModel,
  ReportDefinitionService,
  ReportDefinitionViewModel,
} from 'sqtm-core';
import { DocxReportService } from '../../services/docx-report.service';
import { Observable } from 'rxjs';
import { filter, map, take } from 'rxjs/operators';
import { JasperReportDialogConfiguration } from '../jasper-report-dialog/jasper-report-dialog.configuration';
import { JasperReportDialogComponent } from '../jasper-report-dialog/jasper-report-dialog.component';
import { DirectDownloadableReportService } from '../../services/direct-downloadable-report.service';

@Component({
  selector: 'sqtm-app-custom-report-report',
  templateUrl: './custom-report-report.component.html',
  styleUrls: ['./custom-report-report.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DocxReportService, DirectDownloadableReportService],
})
export class CustomReportReportComponent {
  @Input()
  reportDefinition: ReportDefinitionModel;

  @Input()
  reportId: number;

  showSpinner$: Observable<boolean>;

  constructor(
    private docxReportService: DocxReportService,
    private dialogService: DialogService,
    private vcr: ViewContainerRef,
    private reportDefinitionService: ReportDefinitionService,
    private directDownloadableReportService: DirectDownloadableReportService,
    private referentialDataService: ReferentialDataService,
  ) {
    this.showSpinner$ = this.docxReportService.generatingReport$;
  }

  downloadReport($event: MouseEvent) {
    $event.stopPropagation();

    this.reportDefinitionService
      .getReportDefinitionViewModelByReportDefinitionId(this.reportId)
      .pipe(
        take(1),
        filter((reportDefView: ReportDefinitionViewModel) => !reportDefView.missingPlugin),
      )
      .subscribe((reportDefView: ReportDefinitionViewModel) => {
        if (reportDefView.report.isDirectDownloadableReport) {
          this.directDownloadableReportService.directDownloadReport(
            reportDefView.report,
            JSON.parse(reportDefView.parameters),
          );
        } else if (reportDefView.report.isDocxTemplate) {
          this.docxReportService.generateReport(this.reportId, reportDefView.pluginNamespace, {
            json: reportDefView.parameters,
          });
        } else {
          this.openJasperReportDialog(reportDefView.report, reportDefView.parameters);
        }
      });
  }

  canDownload(): Observable<boolean> {
    return this.referentialDataService.projectDatas$.pipe(
      take(1),
      map((projectDataMap: ProjectDataMap) => {
        return projectDataMap[
          this.reportDefinition.projectId
        ].permissions.CUSTOM_REPORT_LIBRARY.includes(Permissions.EXPORT);
      }),
    );
  }

  private openJasperReportDialog(report: Report, params: any) {
    const configuration: JasperReportDialogConfiguration = { report, params: JSON.parse(params) };

    this.dialogService.openDialog<JasperReportDialogConfiguration, void>({
      id: 'jasper-report-dialog',
      component: JasperReportDialogComponent,
      viewContainerReference: this.vcr,
      width: 1230,
      height: 768,
      data: configuration,
    });
  }
}
