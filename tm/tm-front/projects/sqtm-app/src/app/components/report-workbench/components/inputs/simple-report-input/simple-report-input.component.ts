import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ReportInput, ReportInputType } from 'sqtm-core';
import { FormGroup, ValidationErrors } from '@angular/forms';

@Component({
  selector: 'sqtm-app-simple-report-input',
  templateUrl: './simple-report-input.component.html',
  styleUrls: ['./simple-report-input.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SimpleReportInputComponent {
  @Input()
  reportInput: ReportInput;

  @Input()
  subFormGroup: FormGroup;

  @Input()
  errors: ValidationErrors;

  ReportInputType = ReportInputType;
}
