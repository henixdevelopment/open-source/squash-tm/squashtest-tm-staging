import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ComponentRef,
  ViewContainerRef,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Project, ProjectPickerComponent, ReferentialDataService } from 'sqtm-core';
import { Overlay } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { map, take, takeUntil } from 'rxjs/operators';
import { combineLatest, race } from 'rxjs';
import { AbstractFormPicker } from '../abstract-form-picker';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-form-project-picker',
  templateUrl: './form-project-picker.component.html',
  styleUrls: ['./form-project-picker.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: FormProjectPickerComponent,
    },
  ],
})
export class FormProjectPickerComponent extends AbstractFormPicker implements ControlValueAccessor {
  private componentRef: ComponentRef<ProjectPickerComponent>;

  constructor(
    overlay: Overlay,
    private vcr: ViewContainerRef,
    private changeDetectorRef: ChangeDetectorRef,
    private referentialDataService: ReferentialDataService,
    private translateService: TranslateService,
  ) {
    super(overlay);
  }

  handleOpen() {
    combineLatest([
      this.referentialDataService.projects$,
      this.referentialDataService.filteredProjectIds$,
      this.referentialDataService.filterActivated$,
    ])
      .pipe(
        take(1),
        map(([projects, ids, filterActivated]: [Project[], number[], boolean]) => {
          if (filterActivated) {
            return projects.filter((p) => ids.includes(p.id));
          } else {
            return projects;
          }
        }),
      )
      .subscribe((projects) => {
        const overlayConfig = this.createOverlayConfig();
        this.overlayRef = this.overlay.create(overlayConfig);
        const componentPortal = new ComponentPortal(ProjectPickerComponent, this.vcr);
        this.componentRef = this.overlayRef.attach(componentPortal);
        this.componentRef.instance.projects = projects;
        this.componentRef.instance.initiallySelectedProjects = this.selectedIds;
        this.componentRef.changeDetectorRef.detectChanges();

        race(this.componentRef.instance.cancelRequired, this.overlayRef.backdropClick())
          .pipe(takeUntil(this.unsub$), take(1))
          .subscribe(() => this.removeOverlay());

        this.componentRef.instance.selectedProjects
          .pipe(takeUntil(this.unsub$), take(1))
          .subscribe((selectedProjects) => {
            const ids = selectedProjects.map((p) => p.id);
            this.selectedIds = ids;
            this.onChange(ids);
            this.changeDetectorRef.detectChanges();
            this.removeOverlay();
          });
      });
  }

  protected updateSelectedNames() {
    if (this.selectedIds.length === 1) {
      this.referentialDataService.projects$
        .pipe(
          take(1),
          map((projects) => projects.filter((p) => this.selectedIds.includes(p.id))),
        )
        .subscribe((projects) => {
          this.concatenatedNames = projects.map((p) => p.name).join();
          this.tooltipNames = '';
          this.changeDetectorRef.detectChanges();
        });
    } else {
      this.concatenatedNames = this.translateService.instant(
        'sqtm-core.custom-report-workspace.create-report.element-selected',
        { nb: this.selectedIds.length.toString() },
      );

      this.referentialDataService.projects$
        .pipe(
          take(1),
          map((projects) => projects.filter((p) => this.selectedIds.includes(p.id))),
        )
        .subscribe((projects) => {
          this.tooltipNames = projects.map((p) => p.name).join(', ');
          this.changeDetectorRef.detectChanges();
        });
    }
  }
}
