import { TestBed } from '@angular/core/testing';

import { DocxReportService } from './docx-report.service';
import { AppTestingUtilsModule } from '../../../utils/testing-utils/app-testing-utils.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateService } from '@ngx-translate/core';
import { mockPassThroughTranslateService } from '../../../utils/testing-utils/mocks.service';

describe('DocxReportService', () => {
  let service: DocxReportService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule],
      providers: [
        DocxReportService,
        {
          provide: TranslateService,
          useValue: mockPassThroughTranslateService(),
        },
      ],
    });
    service = TestBed.inject(DocxReportService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
