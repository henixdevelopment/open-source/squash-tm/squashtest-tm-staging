import { sqtmAppLogger } from '../../app-logger';

export const reportWorkbenchLogger = sqtmAppLogger.compose('report-workbench');
