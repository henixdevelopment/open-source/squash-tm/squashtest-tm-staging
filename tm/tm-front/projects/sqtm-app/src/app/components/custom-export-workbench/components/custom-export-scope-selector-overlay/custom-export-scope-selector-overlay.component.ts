import { ChangeDetectionStrategy, Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { filter, map, take } from 'rxjs/operators';
import {
  CampaignLimitedPickerComponent,
  DataRow,
  SquashTmDataRowType,
  CustomExportScopeNode,
} from 'sqtm-core';
import { Observable, of } from 'rxjs';

const ACCEPTED_ROW_TYPES: SquashTmDataRowType[] = [
  SquashTmDataRowType.Campaign,
  SquashTmDataRowType.Iteration,
  SquashTmDataRowType.TestSuite,
  SquashTmDataRowType.CampaignFolder,
];

@Component({
  selector: 'sqtm-app-custom-export-scope-selector-overlay',
  templateUrl: './custom-export-scope-selector-overlay.component.html',
  styleUrls: ['./custom-export-scope-selector-overlay.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomExportScopeSelectorOverlayComponent {
  @Output()
  scopeChanged = new EventEmitter<CustomExportScopeNode[]>();

  @ViewChild('picker')
  set picker(pickerElement: CampaignLimitedPickerComponent) {
    this._picker = pickerElement;

    if (pickerElement?.tree != null) {
      this.canConfirm$ = pickerElement.tree.selectedRows$.pipe(
        map((rows: DataRow[]) => this.filterOutIllegalRows(rows)),
        map((rows: DataRow[]) => rows.length > 0),
      );
    }
  }

  get picker(): CampaignLimitedPickerComponent {
    return this._picker;
  }

  initialSelectedNodes = [];

  public canConfirm$: Observable<boolean> = of(false);

  @Output()
  closeRequired = new EventEmitter<void>();

  private _picker: CampaignLimitedPickerComponent;

  private filterOutIllegalRows(dataRows: DataRow[]): DataRow[] {
    if (dataRows.some((row) => !ACCEPTED_ROW_TYPES.includes(row.type))) {
      this.picker.tree.unselectAllRows();
      return [];
    }

    return dataRows;
  }

  handleConfirm(): void {
    if (this.picker != null) {
      // has to access directly to the tree as the picker EventEmitter can't give actual values, but just emit changes
      this.picker.tree.selectedRows$
        .pipe(
          take(1),
          filter((rows: DataRow[]) => rows.length > 0),
        )
        .subscribe((rows: DataRow[]) => this.emitScope(rows));
    }
  }

  private emitScope(rows: DataRow[]) {
    const scope = [];
    if (rows && rows.length > 0) {
      rows.forEach((row: DataRow) => {
        scope.push({
          id: row.id,
          name: row.data.NAME,
          projectId: row.projectId,
        });
      });
      this.scopeChanged.emit(scope);
    } else {
      this.scopeChanged.emit(null);
    }
  }

  handleClose() {
    this.closeRequired.emit();
  }
}
