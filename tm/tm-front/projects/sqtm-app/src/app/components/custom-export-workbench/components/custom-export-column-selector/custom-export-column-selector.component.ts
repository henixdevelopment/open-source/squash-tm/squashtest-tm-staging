import { ChangeDetectionStrategy, Component, InjectionToken, OnDestroy } from '@angular/core';
import {
  column,
  CustomExportColumnUtils,
  DataRow,
  Extendable,
  GridColumnId,
  GridDefinition,
  GridDefinitionBuilder,
  GridService,
  gridServiceFactory,
  GridType,
  pagination,
  ReferentialDataService,
  RestService,
  StyleDefinitionBuilder,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { CustomExportAttributeRow, CustomExportEntityRow } from './custom-export-column-row';
import { CustomExportColumnCellComponent } from '../custom-export-column-cell/custom-export-column-cell.component';
import { CustomExportWorkbenchService } from '../../services/custom-export-workbench.service';
import { select } from '@ngrx/store';
import {
  availableColumnsSelector,
  CustomExportIdentifiedColumnsByEntityType,
  IdentifiedExportColumn,
} from '../../state/custom-export-workbench.state';
import { map } from 'rxjs/operators';

export const CUSTOM_EXPORT_COLUMN_SELECTOR_GRID_CONFIG = new InjectionToken(
  'Token to inject custom export column grid config',
);

export function customExportColumnSelectorGridConfig(): GridDefinition {
  return new GridDefinitionBuilder('custom-export-column-selector', GridType.TREE)
    .withColumns([
      column(GridColumnId.NAME)
        .enableDnd()
        .changeWidthCalculationStrategy(new Extendable(300))
        .withRenderer(CustomExportColumnCellComponent),
    ])
    .withRowHeight(30)
    .disableHeaders()
    .enableMultipleColumnsFiltering(['NAME'])
    .disableRightToolBar()
    .withStyle(
      new StyleDefinitionBuilder()
        .switchOffSelectedRows()
        .switchOffHoveredRows()
        .enableInitialLoadAnimation(),
    )
    .withPagination(pagination().inactive())
    .enableDrag()
    .build();
}

@Component({
  selector: 'sqtm-app-custom-export-column-selector',
  templateUrl: './custom-export-column-selector.component.html',
  styleUrls: ['./custom-export-column-selector.component.less'],
  providers: [
    {
      provide: CUSTOM_EXPORT_COLUMN_SELECTOR_GRID_CONFIG,
      useFactory: customExportColumnSelectorGridConfig,
    },
    {
      provide: GridService,
      useFactory: gridServiceFactory,
      deps: [RestService, CUSTOM_EXPORT_COLUMN_SELECTOR_GRID_CONFIG, ReferentialDataService],
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomExportColumnSelectorComponent implements OnDestroy {
  constructor(
    private gridService: GridService,
    private translateService: TranslateService,
    private readonly workbenchService: CustomExportWorkbenchService,
  ) {
    this.gridService.connectToDataRowSource(
      this.workbenchService.state$.pipe(
        select(availableColumnsSelector),
        map((availableColumns) => this.asDataRows(availableColumns)),
      ),
    );
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }

  private asDataRows(columnsByEntityType: CustomExportIdentifiedColumnsByEntityType): DataRow[] {
    return Object.entries(columnsByEntityType)
      .filter(
        ([_parentEntity, attributes]: [string, IdentifiedExportColumn[]]) => attributes.length > 0,
      )
      .map(([parentEntity, attributes]: [string, IdentifiedExportColumn[]]) => [
        this.createEntityRow(parentEntity, attributes),
        ...attributes.map((attr) => this.createAttributeRow(attr, parentEntity)),
      ])
      .flat();
  }

  private createEntityRow(entityId: string, children: IdentifiedExportColumn[]): DataRow {
    const name = this.translateService.instant(CustomExportColumnUtils.getEntityI18nKey(entityId));
    return new CustomExportEntityRow(
      entityId,
      name,
      children.map((col) => col.id),
    );
  }

  private createAttributeRow(attribute: IdentifiedExportColumn, parentRowId: string): DataRow {
    const isCuf = Boolean(attribute.cufId);
    const name = isCuf
      ? attribute.label
      : this.translateService.instant(CustomExportColumnUtils.getColumnI18nKey(attribute.id));
    return new CustomExportAttributeRow(attribute.id, name, parentRowId, attribute.cufId);
  }
}
