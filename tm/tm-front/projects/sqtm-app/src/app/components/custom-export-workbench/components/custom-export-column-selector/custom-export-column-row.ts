import { DataRow, DataRowOpenState } from 'sqtm-core';

export enum CustomExportColumnSelectorRowType {
  Entity = 'Entity',
  Attribute = 'Attribute',
}

export class CustomExportEntityRow extends DataRow {
  constructor(
    public id: string,
    public readonly name: string,
    public children: string[],
  ) {
    super();

    this.state = DataRowOpenState.open;
    this.data = {
      NAME: name,
      [ENTITY_KEY]: id,
    };
  }

  readonly type = CustomExportColumnSelectorRowType.Entity;
  allowMoves = false;
  allowedChildren = [CustomExportAttributeRow];
}

export class CustomExportAttributeRow extends DataRow {
  constructor(
    public id: string,
    public readonly name: string,
    public parentRowId: string,
    public cufId: number,
  ) {
    super();

    this.data = {
      NAME: name,
      [ENTITY_KEY]: parentRowId,
      cufId,
    };
  }

  readonly type = CustomExportColumnSelectorRowType.Attribute;
  allowMoves = true;
  allowedChildren = [];
  state = DataRowOpenState.leaf;
  children = [];
}

export const ENTITY_KEY = 'ENTITY';
