import { Injectable } from '@angular/core';
import {
  BindableEntity,
  createStore,
  CustomExportColumn,
  CustomExportEntityType,
  CustomExportModel,
  CustomExportWorkbenchData,
  CustomFieldExportColumnData,
  GlobalConfigurationState,
  ReferentialDataService,
  CustomExportScopeNode,
  RestService,
} from 'sqtm-core';
import {
  CraftedExport,
  CustomExportIdentifiedColumnsByEntityType,
  CustomExportWorkbenchState,
  getIdentifiedColumnsByEntityType,
  IdentifiedExportColumn,
  initialCustomExportWorkbenchState,
} from '../state/custom-export-workbench.state';
import { map, switchMap, take, tap } from 'rxjs/operators';
import { combineLatest, Observable, of } from 'rxjs';
import * as _ from 'lodash';

@Injectable()
export class CustomExportWorkbenchService {
  constructor(
    private readonly referentialDataService: ReferentialDataService,
    private readonly restService: RestService,
  ) {}

  private store = createStore<CustomExportWorkbenchState>(initialCustomExportWorkbenchState, {
    id: 'CustomExportWorkbench',
    logDiff: 'simple',
  });

  public state$: Observable<CustomExportWorkbenchState> = this.store.state$;

  load(workbenchData: CustomExportWorkbenchData): void {
    this.state$
      .pipe(
        take(1),
        map((state) => ({ ...state, containerId: workbenchData.containerId })),
      )
      .subscribe((state: CustomExportWorkbenchState) =>
        this.loadFromModel(workbenchData.customExport, state),
      );
  }

  changeName(name: string): Observable<any> {
    return this.state$.pipe(
      take(1),
      tap((state) => this.store.commit(withUpdatedCraftedExport({ name }, state))),
    );
  }

  changeScope(scope: CustomExportScopeNode[]): Observable<any> {
    return this.state$.pipe(
      take(1),
      switchMap((state) => this.updateScopeAndAvailableColumns(scope, state)),
      tap((newState) => this.store.commit(newState)),
    );
  }

  addSelectedColumns(ids: string[]): void {
    this.state$
      .pipe(
        take(1),
        map((state: CustomExportWorkbenchState) => this.withAddedSelectedColumns(ids, state)),
        tap((state) => this.store.commit(state)),
      )
      .subscribe();
  }

  removeSelectedColumn(columnToRemoveId: string): void {
    this.state$
      .pipe(
        take(1),
        map((state: CustomExportWorkbenchState) => {
          const columns = state.craftedExport.columns.filter((col) => col.id !== columnToRemoveId);
          return { ...state, craftedExport: { ...state.craftedExport, columns } };
        }),
        tap((state) => this.store.commit(state)),
      )
      .subscribe();
  }

  complete(): void {
    this.store.complete();
  }

  private loadFromModel(
    customExportModel: CustomExportModel,
    state: CustomExportWorkbenchState,
  ): void {
    if (customExportModel == null) {
      this.withUpdatedAvailableColumns(state).subscribe((updatedState) =>
        this.store.commit(updatedState),
      );
    } else {
      const scope: CustomExportScopeNode[] = customExportModel.scopeNodes;

      this.updateScopeAndAvailableColumns(scope, state)
        .pipe(
          map((newState) => {
            const columnIds = customExportModel.columns.map((col) => buildColumnTemporaryId(col));
            const updatedState: CustomExportWorkbenchState = {
              ...newState,
              craftedExport: {
                ...newState.craftedExport,
                existingNodeId: customExportModel.customReportLibraryNodeId,
                name: customExportModel.name,
              },
            };

            return this.withAddedSelectedColumns(columnIds, updatedState);
          }),
        )
        .subscribe((finalState) => this.store.commit(finalState));
    }
  }

  private updateScopeAndAvailableColumns(
    scope: CustomExportScopeNode[],
    state: CustomExportWorkbenchState,
  ): Observable<CustomExportWorkbenchState> {
    if (!oneScopeNodeWasDeleted(scope)) {
      const updatedState = withUpdatedCraftedExport({ scope }, state);
      return this.withUpdatedAvailableColumns(updatedState, scope);
    } else {
      return this.withUpdatedAvailableColumns(state, scope);
    }
  }

  private withAddedSelectedColumns(
    ids: string[],
    state: CustomExportWorkbenchState,
  ): CustomExportWorkbenchState {
    const addedColumns = ids
      .map((id) => findIdentifiedColumnById(id, state.availableColumns))
      .filter(Boolean);
    const columns = sortColumns(
      _.uniq([...state.craftedExport.columns, ...addedColumns]),
      Object.values(state.availableColumns).flat(1),
    );
    return { ...state, craftedExport: { ...state.craftedExport, columns } };
  }

  private withUpdatedAvailableColumns(
    currentState: CustomExportWorkbenchState,
    scope?: CustomExportScopeNode[],
  ): Observable<CustomExportWorkbenchState> {
    const availableCufData$ = scope ? this.getAvailableCufServerSide(scope).pipe(take(1)) : of([]);

    return combineLatest([
      availableCufData$,
      this.referentialDataService.globalConfiguration$,
    ]).pipe(
      map(([availableCufData, globalConf]: any) =>
        this.withAvailableColumns(currentState, globalConf, availableCufData),
      ),
      map((updatedState: CustomExportWorkbenchState) =>
        withUnavailableCustomFieldAttributesRemoved(updatedState),
      ),
    );
  }

  private getAvailableCufServerSide(
    scope: CustomExportScopeNode[],
  ): Observable<CustomFieldExportColumnData[]> {
    const url: string[] = ['custom-exports/available-cuf-columns'];
    const scopesProjectIds: number[] = scope.map((s) => s.projectId);
    return this.restService.post<CustomFieldExportColumnData[]>(url, scopesProjectIds);
  }

  // Build the available column list from the standard column and the available custom fields in the current scope's project.
  private withAvailableColumns(
    state: CustomExportWorkbenchState,
    globalConfiguration: GlobalConfigurationState,
    availableCufData: CustomFieldExportColumnData[],
  ): CustomExportWorkbenchState {
    const showMilestoneColumns = globalConfiguration.milestoneFeatureEnabled;
    const availableColumns = getIdentifiedColumnsByEntityType(showMilestoneColumns);

    if (availableCufData.length > 0) {
      Object.keys(availableColumns).forEach((entity: BindableEntity) => {
        const availableCufColumns: IdentifiedExportColumn[] = availableCufData
          .filter((cuf) => cuf.bindableEntity === entity)
          .map((cufData) => identifiedExportColumnFromCuf(cufData));

        this.updateEntityColumnsWithCustomFields(availableCufColumns, entity, availableColumns);
      });
    }

    return { ...state, availableColumns };
  }

  private updateEntityColumnsWithCustomFields(
    availableCufColumns: IdentifiedExportColumn[],
    entity,
    availableColumns,
  ) {
    const availableColumnsForEntity: IdentifiedExportColumn[] = [
      // Not inlined to preserve type safety
      ...availableColumns[entity],
      ...availableCufColumns,
    ];
    availableColumns[entity] = availableColumnsForEntity;
  }
}

function withUnavailableCustomFieldAttributesRemoved(
  state: CustomExportWorkbenchState,
): CustomExportWorkbenchState {
  const columns = state.craftedExport.columns.filter(
    (col) => findIdentifiedColumnById(col.id, state.availableColumns) != null,
  );
  return withUpdatedCraftedExport({ columns }, state);
}

function withUpdatedCraftedExport(
  changes: Partial<CraftedExport>,
  state: CustomExportWorkbenchState,
): CustomExportWorkbenchState {
  return { ...state, craftedExport: { ...state.craftedExport, ...changes } };
}

function findIdentifiedColumnById(
  id: string,
  availableColumns: CustomExportIdentifiedColumnsByEntityType,
): IdentifiedExportColumn {
  return Object.values(availableColumns)
    .flat(1)
    .find((available: IdentifiedExportColumn) => available.id === id);
}

function sortColumns(
  columns: Readonly<IdentifiedExportColumn[]>,
  flatColumnArray: IdentifiedExportColumn[],
): IdentifiedExportColumn[] {
  const copy = [...columns];
  copy.sort((a, b) => {
    const indexA = flatColumnArray.indexOf(flatColumnArray.find((col) => col.id === a.id));
    const indexB = flatColumnArray.indexOf(flatColumnArray.find((col) => col.id === b.id));
    return indexA - indexB;
  });
  return copy;
}

function makeTemporaryCufColumnId(bindableEntity: BindableEntity, customFieldId: number): any {
  return `${bindableEntity}-${customFieldId}`;
}

function buildColumnTemporaryId(col: CustomExportColumn): string {
  return col.cufId
    ? makeTemporaryCufColumnId(asBindableEntity(col.entityType), col.cufId)
    : col.columnName;
}

function asBindableEntity(entityType: CustomExportEntityType): BindableEntity {
  switch (entityType) {
    case CustomExportEntityType.CAMPAIGN:
      return BindableEntity.CAMPAIGN;
    case CustomExportEntityType.EXECUTION_STEP:
      return BindableEntity.EXECUTION_STEP;
    case CustomExportEntityType.EXECUTION:
      return BindableEntity.EXECUTION;
    case CustomExportEntityType.ITERATION:
      return BindableEntity.ITERATION;
    case CustomExportEntityType.TEST_CASE:
      return BindableEntity.TEST_CASE;
    case CustomExportEntityType.TEST_SUITE:
      return BindableEntity.TEST_SUITE;
    case CustomExportEntityType.TEST_STEP:
      return BindableEntity.TEST_STEP;
    default:
      throw new Error('Invalid custom export entity type for CUF binding : ' + entityType);
  }
}

function oneScopeNodeWasDeleted(customExportScopeNodes: CustomExportScopeNode[]): boolean {
  // We check if we have an empty scope node name, which means the scope node was deleted.
  let deletedNodes = 0;
  customExportScopeNodes.forEach((node) => (node.name ? deletedNodes : deletedNodes++));
  return deletedNodes > 0;
}

function identifiedExportColumnFromCuf(
  cufData: CustomFieldExportColumnData,
): IdentifiedExportColumn {
  return {
    id: makeTemporaryCufColumnId(cufData.bindableEntity, cufData.cufId),
    label: cufData.label,
    cufId: cufData.cufId,
    entityType: CustomExportEntityType[cufData.bindableEntity],
    // CUFs do have column names server-side such as 'CAMPAIGN_CUF' but we don't need it (and don't know it)
    columnName: null,
  };
}
