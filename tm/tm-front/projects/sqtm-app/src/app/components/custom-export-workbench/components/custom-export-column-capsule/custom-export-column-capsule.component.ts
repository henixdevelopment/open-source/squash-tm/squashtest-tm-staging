import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IdentifiedExportColumn } from '../../state/custom-export-workbench.state';
import { CustomExportWorkbenchUtils } from '../../custom-export-workbench.utils';
import { CustomExportColumnUtils } from 'sqtm-core';

@Component({
  selector: 'sqtm-app-custom-export-column-capsule',
  templateUrl: './custom-export-column-capsule.component.html',
  styleUrls: ['./custom-export-column-capsule.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomExportColumnCapsuleComponent {
  readonly borderColor = 'var(--current-workspace-main-color)';

  @Input() column: IdentifiedExportColumn;
  @Input() showEntityName: boolean;
  @Input() showRemoveButton: boolean;

  @Output() removeClicked = new EventEmitter<void>();

  constructor(public readonly translateService: TranslateService) {}

  get isCuf(): boolean {
    return Boolean(this.column.cufId);
  }

  get workspace(): string {
    return CustomExportWorkbenchUtils.getWorkspace(this.column.entityType);
  }

  get label(): string {
    const keyOrLabel = this.isCuf
      ? this.column.label
      : CustomExportColumnUtils.getColumnI18nKey(this.column.columnName);
    // We still have to translate CUF labels because it could be a message translation key (e.g. the CUF was deleted)
    const localeLabel = this.translateService.instant(keyOrLabel);

    if (this.showEntityName) {
      const entityName = this.translateService.instant(
        CustomExportColumnUtils.getEntityI18nKey(this.column.entityType),
      );
      return `${entityName} | ${localeLabel}`;
    } else {
      return localeLabel;
    }
  }

  handleRemove(): void {
    this.removeClicked.emit();
  }
}
