import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import {
  AbstractCellRendererComponent,
  CustomExportEntityType,
  DataRowOpenState,
  GridService,
} from 'sqtm-core';
import { Subject } from 'rxjs';
import {
  CustomExportColumnSelectorRowType,
  ENTITY_KEY,
} from '../custom-export-column-selector/custom-export-column-row';
import { CustomExportWorkbenchUtils } from '../../custom-export-workbench.utils';

@Component({
  selector: 'sqtm-app-custom-export-column-cell',
  templateUrl: './custom-export-column-cell.component.html',
  styleUrls: ['./custom-export-column-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomExportColumnCellComponent
  extends AbstractCellRendererComponent
  implements OnDestroy
{
  // base width of 1 index increment, in pixels.
  readonly INDEX_BASE_WIDTH = 20;

  private unsub$ = new Subject<void>();

  @ViewChild('openable', { read: ElementRef })
  containerRef: ElementRef;

  get capsuleBorderColor() {
    return 'var(--current-workspace-main-color)';
  }

  get depthArray(): boolean[] {
    return this.depthMap.slice(0, this.depth);
  }

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  toggleRow($event: MouseEvent) {
    $event.stopPropagation();
    if (this.row.state === DataRowOpenState.closed) {
      this.grid.openRow(this.row.id);
    } else if (this.row.state === DataRowOpenState.open) {
      this.grid.closeRow(this.row.id);
    }
  }

  get stateIcon() {
    if (this.row.state === DataRowOpenState.closed) {
      return 'plus';
    } else {
      return 'minus';
    }
  }

  get entityIcon() {
    const entityType: string = this.row.data[ENTITY_KEY];
    const iconName: string = entityType.toLowerCase().replace(/_/g, '-');
    return `sqtm-core-custom-report:${iconName}`;
  }

  get nodeName() {
    return this.row.data[this.columnDisplay.id];
  }

  get templateName(): string {
    let templateName;
    switch (this.row.type) {
      case CustomExportColumnSelectorRowType.Entity:
        templateName = 'entity';
        break;
      case CustomExportColumnSelectorRowType.Attribute:
        templateName = 'attribute';
        break;
      default:
        throw Error(`Unable to find template for type ${this.row.type}`);
    }
    return templateName;
  }

  getWorkspace() {
    const entity = this.row.data[ENTITY_KEY] as CustomExportEntityType;
    return CustomExportWorkbenchUtils.getWorkspace(entity);
  }

  isCustomField() {
    return Boolean(this.row.data.cufId);
  }
}
