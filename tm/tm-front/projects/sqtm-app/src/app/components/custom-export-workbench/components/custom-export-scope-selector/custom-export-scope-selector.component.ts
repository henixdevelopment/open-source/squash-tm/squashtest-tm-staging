import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ComponentRef,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { ComponentPortal } from '@angular/cdk/portal';
import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { CustomExportScopeSelectorOverlayComponent } from '../custom-export-scope-selector-overlay/custom-export-scope-selector-overlay.component';
import { race, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { CustomExportScopeNode } from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-custom-export-scope-selector',
  templateUrl: './custom-export-scope-selector.component.html',
  styleUrls: ['./custom-export-scope-selector.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomExportScopeSelectorComponent implements OnDestroy {
  _scope: CustomExportScopeNode[] = [];

  overlayRef: OverlayRef;

  @ViewChild('fieldElement')
  fieldElement: ElementRef;

  @Input()
  set scope(newScope: CustomExportScopeNode[]) {
    this._scope = newScope;
  }

  @Output() scopeChanged = new EventEmitter<CustomExportScopeNode[]>();

  private overlayComponentComponentRef: ComponentRef<CustomExportScopeSelectorOverlayComponent>;

  private unsub$ = new Subject<void>();

  constructor(
    private overlay: Overlay,
    private cdRef: ChangeDetectorRef,
    private vcr: ViewContainerRef,
    public readonly translateService: TranslateService,
  ) {}

  get hasSelectedScope(): boolean {
    return this._scope != null && this._scope.length > 0;
  }

  get currentScope(): string {
    if (this._scope?.length > 1) {
      return this.translateService.instant(
        'sqtm-core.custom-report-workspace.create-custom-export.workbench.label.multiple-export',
      );
    } else {
      return this._scope[0]?.name;
    }
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  openScopeSelector() {
    this.createOverlay();
    const customScopeSelectorPortal = new ComponentPortal(
      CustomExportScopeSelectorOverlayComponent,
      this.vcr,
    );
    this.overlayComponentComponentRef = this.overlayRef.attach(customScopeSelectorPortal);

    this.overlayComponentComponentRef.instance.scopeChanged.subscribe((newScope) => {
      this.setScopeAndNotify(newScope);
      this.cdRef.detectChanges();
      this.closeOverlay();
    });

    race([
      this.overlayRef.backdropClick(),
      this.overlayComponentComponentRef.instance.closeRequired,
    ])
      .pipe(takeUntil(this.unsub$), take(1))
      .subscribe(() => this.closeOverlay());
  }

  private setScopeAndNotify(newScope: CustomExportScopeNode[]): void {
    this._scope = newScope;
    this.scopeChanged.emit(newScope);
  }

  private createOverlay() {
    const positionStrategy = this.overlay
      .position()
      .flexibleConnectedTo(this.fieldElement)
      .withPositions([
        { originX: 'end', overlayX: 'start', originY: 'center', overlayY: 'top', offsetX: 20 },
      ]);
    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
      width: 350,
      height: '80%',
    };
    this.overlayRef = this.overlay.create(overlayConfig);
  }

  closeOverlay() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
      this.overlayComponentComponentRef = null;
    }
  }
}
