import { CustomExportEntityType } from 'sqtm-core';
import { IdentifiedExportColumn } from './state/custom-export-workbench.state';

export class CustomExportWorkbenchUtils {
  static getWorkspace(entity: CustomExportEntityType) {
    switch (entity) {
      case CustomExportEntityType.TEST_CASE:
      case CustomExportEntityType.TEST_STEP:
        return 'test-case-workspace';
      case CustomExportEntityType.CAMPAIGN:
      case CustomExportEntityType.ITERATION:
      case CustomExportEntityType.TEST_SUITE:
      case CustomExportEntityType.EXECUTION:
      case CustomExportEntityType.EXECUTION_STEP:
        return 'campaign-workspace';
      case CustomExportEntityType.ISSUE:
        return 'bugtracker-workspace';
      default:
        throw Error(`Unable to find workspace theme for type ${entity}`);
    }
  }

  static getEntityNameI18nKey(entityType: CustomExportEntityType) {
    return `sqtm-core.custom-report-workspace.chart.entity.${entityType}`;
  }

  static getColumnNameI18nKey(exportColumn: IdentifiedExportColumn): string {
    return exportColumn.cufId
      ? exportColumn.label
      : `sqtm-core.custom-report-workspace.chart.columns.${exportColumn.label}`;
  }

  static getColumnEntityNameI18nKey(column: IdentifiedExportColumn): string {
    const entityType = column.entityType;
    return this.getEntityNameI18nKey(entityType);
  }
}
