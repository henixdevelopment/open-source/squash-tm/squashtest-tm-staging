import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { createStore, FailureDetail, FailureDetailComponentData } from 'sqtm-core';
import { FailureDetailService } from '../../services/failure-detail.service';

export type FailureDetailParentType = 'automated-suite-details-modal' | 'execution-page';

@Component({
  selector: 'sqtm-app-orchestrator-execution-failure-details',
  templateUrl: './orchestrator-execution-failure-details.component.html',
  styleUrl: './orchestrator-execution-failure-details.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: FailureDetailService,
      useClass: FailureDetailService,
    },
  ],
})
export class OrchestratorExecutionFailureDetailsComponent implements OnInit, OnDestroy {
  @Input()
  extenderId: number;

  @Input()
  parentType: FailureDetailParentType;

  @Output()
  refreshExecutionIssue = new EventEmitter<void>();

  private store = createStore<FailureDetailComponentData>({
    failureDetailList: null,
    uiState: { loading: true },
  });

  componentData$ = this.store.state$;
  constructor(private readonly failureDetailService: FailureDetailService) {}

  ngOnInit(): void {
    this.failureDetailService
      .getExecutionExtenderFailureDetailList(this.extenderId)
      .subscribe((failureDetailList: FailureDetail[]) => {
        let sortedFailureDetailList: FailureDetail[] = [];
        if (failureDetailList.length > 0) {
          sortedFailureDetailList = failureDetailList.slice().sort((a, b) => a.id - b.id);
        }
        this.store.commit({
          failureDetailList: sortedFailureDetailList,
          uiState: { loading: false },
        });
      });
  }

  ngOnDestroy(): void {
    this.store.complete();
  }

  relayRefreshExecutionIssue() {
    if (this.parentType == 'execution-page' || this.parentType == 'automated-suite-details-modal') {
      this.refreshExecutionIssue.emit();
    }
  }
}
