import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {
  BoundEnvironmentVariableService,
  BoundEnvironmentVariableState,
} from '../../../services/bound-environment-variable.service';
import { take, takeUntil, tap } from 'rxjs/operators';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
  AutomationEnvironmentTagHolder,
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-reinitialize-value-cell',
  template: `
    @if (componentData$ | async; as componentData) {
      @if (isReinitializeButtonVisible()) {
        <sqtm-core-toggle-icon
          [attr.data-test-button-id]="'reset-ev-value-button=' + this.row.data['id']"
          [active]="isReinitializeButtonVisible()"
          class="full-height full-width icon-container current-workspace-main-color"
        >
          <i
            class="table-icon-size reset-tags-button"
            nz-icon
            nzType="sqtm-core-administration:synchronize"
            nzTheme="outline"
            nz-tooltip
            [nzTooltipTitle]="
              'sqtm-core.entity.environment-variable.tooltip.reset-value' | translate
            "
            (click)="resetDefaultValue()"
          >
          </i>
        </sqtm-core-toggle-icon>
      }
    }
  `,
  styleUrls: ['./reinitialize-value-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReinitializeValueCellComponent extends AbstractCellRendererComponent {
  componentData$: Observable<BoundEnvironmentVariableState>;

  serverEntity: AutomationEnvironmentTagHolder =
    AutomationEnvironmentTagHolder.TEST_AUTOMATION_SERVER;
  executionDialogType: AutomationEnvironmentTagHolder =
    AutomationEnvironmentTagHolder.EXECUTION_DIALOG;

  private entityType: AutomationEnvironmentTagHolder;
  unsub$ = new Subject<void>();
  constructor(
    public grid: GridService,
    cdRef: ChangeDetectorRef,
    private boundEnvironmentVariableService: BoundEnvironmentVariableService,
  ) {
    super(grid, cdRef);
    this.componentData$ = this.boundEnvironmentVariableService.state$.pipe(takeUntil(this.unsub$));
    this.initializeEntityType();
  }

  initializeEntityType() {
    this.componentData$
      .pipe(
        take(1),
        tap((state) => (this.entityType = state.entityType)),
      )
      .subscribe();
  }

  resetDefaultValue() {
    const rowData = this.row.data;
    this.boundEnvironmentVariableService
      .resetEnvironmentVariableDefaultValue(rowData['bindingId'], rowData['id'])
      .subscribe();
  }

  isReinitializeButtonVisible(): boolean {
    return (
      (this.entityType !== this.serverEntity && this.isBoundToServer()) ||
      this.entityType === this.executionDialogType
    );
  }

  isBoundToServer() {
    return this.row.data[GridColumnId.boundToServer];
  }
}

export function environmentVariableReinitializeColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(ReinitializeValueCellComponent)
    .disableHeader();
}
