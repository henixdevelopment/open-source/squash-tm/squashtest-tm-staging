import {
  ChangeDetectionStrategy,
  Component,
  InjectionToken,
  Input,
  OnInit,
  Signal,
} from '@angular/core';
import {
  buildFilters,
  dateRangeFilter,
  dateTimeColumn,
  executionStatusColumn,
  executionStatusFilter,
  Extendable,
  Fixed,
  GRID_PERSISTENCE_KEY,
  GridColumnId,
  GridDefinition,
  GridFilter,
  GridService,
  gridServiceFactory,
  GridWithStatePersistence,
  LocalPersistenceService,
  OrchestratorResponse,
  PaginationConfigBuilder,
  ReferentialDataService,
  ResearchColumnPrototype,
  RestService,
  smallGrid,
  Sort,
  sortDate,
  textColumn,
  textResearchFilter,
  Workflow,
} from 'sqtm-core';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { toObservable } from '@angular/core/rxjs-interop';
import { OrchestratorWorkflowsService } from '../services/orchestrator-workflows.service';
import { workflowShutdownColumn } from '../cell-renderers/workflow-shutdown/orchestrator-workflow-shutdown.component';

export const PROJECT_WORKFLOWS_TABLE_CONF = new InjectionToken<GridDefinition>(
  'Grid config for the project workflows',
);
export const PROJECT_WORKFLOWS_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the project workflows',
);

export function projectWorkflowsTableDefinition(
  localPersistenceService: LocalPersistenceService,
): GridDefinition {
  return smallGrid('squash-autom-project-workflows')
    .withColumns([
      textColumn(GridColumnId.id)
        .withI18nKey('sqtm-core.entity.generic.id.capitalize')
        .changeWidthCalculationStrategy(new Extendable(100, 1))
        .withAssociatedFilter(),
      textColumn(GridColumnId.name)
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Extendable(100, 1))
        .withAssociatedFilter(),
      textColumn(GridColumnId.namespace)
        .withI18nKey('Namespace')
        .changeWidthCalculationStrategy(new Fixed(100))
        .withAssociatedFilter(),
      dateTimeColumn(GridColumnId.createdOn)
        .withI18nKey('sqtm-core.entity.generic.created-on.masculine')
        .changeWidthCalculationStrategy(new Fixed(140))
        .withSortFunction(sortDate)
        .withAssociatedFilter(),
      executionStatusColumn(GridColumnId.status)
        .withI18nKey('sqtm-core.entity.execution.status.label')
        .changeWidthCalculationStrategy(new Fixed(100))
        .withAssociatedFilter(),
      dateTimeColumn(GridColumnId.completedOn)
        .withI18nKey('sqtm-core.entity.generic.completed-on.masculine')
        .changeWidthCalculationStrategy(new Fixed(140))
        .withSortFunction(sortDate)
        .withAssociatedFilter(),
      workflowShutdownColumn().changeWidthCalculationStrategy(new Fixed(100)),
    ])
    .withRowHeight(35)
    .withInitialSortedColumns([{ id: GridColumnId.createdOn, sort: Sort.DESC }])
    .withPagination(new PaginationConfigBuilder().initialSize(10))
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
}

@Component({
  selector: 'sqtm-app-orchestrator-workflows',
  templateUrl: './orchestrator-workflows.component.html',
  styleUrls: ['./orchestrator-workflows.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: PROJECT_WORKFLOWS_TABLE_CONF,
      useFactory: projectWorkflowsTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: PROJECT_WORKFLOWS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, PROJECT_WORKFLOWS_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: PROJECT_WORKFLOWS_TABLE,
    },
    {
      provide: GRID_PERSISTENCE_KEY,
      useValue: 'squash-autom-project-workflows',
    },
    GridWithStatePersistence,
  ],
})
export class OrchestratorWorkflowsComponent implements OnInit {
  $loading: Signal<boolean> = this.orchestratorService.$loading;

  $orchestratorResponse: Signal<OrchestratorResponse<Workflow[]>> =
    this.orchestratorService.$orchestratorResponse;

  @Input({ required: true })
  projectId: number;

  @Input({ required: true })
  serverId: number;

  workflows$: Observable<Workflow[]>;

  constructor(
    private gridService: GridService,
    private orchestratorService: OrchestratorWorkflowsService,
  ) {
    this.workflows$ = toObservable(this.orchestratorService.$orchestratorResponse).pipe(
      map((orchestrator) => orchestrator?.response ?? []),
    );
  }

  ngOnInit(): void {
    this.loadWorkflows();
  }

  private loadWorkflows() {
    this.orchestratorService
      .initializeWorkflows(this.serverId, this.projectId)
      .pipe(filter((response) => response.reachable))
      .subscribe(() => this.initializeTable());
  }

  private initializeTable() {
    this.gridService.connectToDatasource(this.workflows$, 'id');
    this.gridService.addFilters(this.buildGridFilters());
  }

  private buildGridFilters(): GridFilter[] {
    return buildFilters([
      textResearchFilter(GridColumnId.id, ResearchColumnPrototype.WORKFLOW_ID).alwaysActive(),
      textResearchFilter(GridColumnId.name, ResearchColumnPrototype.WORKFLOW_NAME).alwaysActive(),
      textResearchFilter(
        GridColumnId.namespace,
        ResearchColumnPrototype.WORKFLOW_NAMESPACE,
      ).alwaysActive(),
      executionStatusFilter(
        GridColumnId.status,
        ResearchColumnPrototype.WORKFLOW_STATUS,
      ).alwaysActive(),
      dateRangeFilter(GridColumnId.createdOn, ResearchColumnPrototype.WORKFLOW_CREATED_ON),
      dateRangeFilter(GridColumnId.completedOn, ResearchColumnPrototype.WORKFLOW_COMPLETED_ON),
    ]);
  }

  handleTokenChanged(): void {
    this.orchestratorService.loading();
    this.loadWorkflows();
  }
}
