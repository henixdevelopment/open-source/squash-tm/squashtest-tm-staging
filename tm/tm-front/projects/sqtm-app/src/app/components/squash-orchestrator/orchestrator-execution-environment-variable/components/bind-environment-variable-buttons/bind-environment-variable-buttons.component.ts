import { ChangeDetectionStrategy, Component, EventEmitter, Inject, Output } from '@angular/core';
import { GridService } from 'sqtm-core';
import { EV_ASSOCIATION_TABLE } from '../orchestrator-execution-environment-variable-panel/orchestrator-execution-environment-variable-panel.component';

@Component({
  selector: 'sqtm-app-bind-environment-variable-buttons',
  templateUrl: './bind-environment-variable-buttons.component.html',
  styleUrls: ['./bind-environment-variable-buttons.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BindEnvironmentVariableButtonsComponent {
  @Output()
  isBindEvent = new EventEmitter<boolean>();

  @Output()
  isUnbindEvent = new EventEmitter<boolean>();

  constructor(@Inject(EV_ASSOCIATION_TABLE) public gridService: GridService) {}

  openEnvironmentVariableSelector($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.isBindEvent.next(true);
  }

  deleteEnvironmentVariableBindings($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.isUnbindEvent.next(true);
  }
}
