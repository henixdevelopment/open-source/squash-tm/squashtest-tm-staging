import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import {
  AbstractCellRendererComponent,
  AutomationEnvironmentTagHolder,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
} from 'sqtm-core';
import {
  BoundEnvironmentVariableService,
  BoundEnvironmentVariableState,
} from '../../../services/bound-environment-variable.service';
import { take, takeUntil, tap } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'sqtm-app-ev-link-cell',
  template: ` @if (row) {
    <div class="sqtm-grid-cell-txt-renderer full-width full-height flex-column">
      @if (isServerType()) {
        <a
          [routerLink]="getUrl()"
          class="sqtm-grid-cell-txt-renderer m-auto-0"
          nz-tooltip
          [sqtmCoreLabelTooltip]="row.data['name']"
          [class.disabled-row]="row.disabled"
          [nzTooltipPlacement]="'topLeft'"
          >{{ row.data['name'] }}</a
        >
      } @else {
        <span
          class="sqtm-grid-cell-txt-renderer m-auto-0"
          [class.disabled-row]="row.disabled"
          nz-tooltip
          [nzTooltipPlacement]="'topLeft'"
        >
          {{ row.data['name'] }}
        </span>
      }
    </div>
  }`,
  styleUrls: ['./ev-link-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvLinkCellComponent extends AbstractCellRendererComponent implements OnDestroy {
  componentData$: Observable<BoundEnvironmentVariableState>;

  unsub$ = new Subject<void>();
  entityType: AutomationEnvironmentTagHolder;
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    private boundEnvironmentVariableService: BoundEnvironmentVariableService,
  ) {
    super(grid, cdRef);
    this.componentData$ = this.boundEnvironmentVariableService.state$.pipe(takeUntil(this.unsub$));
    this.initializeEntityType();
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private initializeEntityType() {
    this.componentData$
      .pipe(
        take(1),
        tap((state) => (this.entityType = state.entityType)),
      )
      .subscribe();
  }

  isServerType(): boolean {
    return this.entityType === AutomationEnvironmentTagHolder.TEST_AUTOMATION_SERVER;
  }

  getUrl() {
    return [
      '/',
      'administration-workspace',
      'entities-customization',
      'environment-variables',
      'detail',
      this.row.data.id,
      'content',
    ];
  }
}

export function environmentVariableNameLinkColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(EvLinkCellComponent);
}
