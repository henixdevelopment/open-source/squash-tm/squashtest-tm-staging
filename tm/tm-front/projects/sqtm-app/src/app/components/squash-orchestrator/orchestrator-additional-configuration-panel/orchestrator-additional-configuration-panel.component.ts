import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  ViewChild,
} from '@angular/core';

import * as ace from 'ace-builds';
import 'ace-builds/webpack-resolver';
import 'ace-builds/src-noconflict/ext-language_tools';
import 'ace-builds/src-noconflict/ext-split';
import { TranslateService } from '@ngx-translate/core';
import { SessionPingService } from 'sqtm-core';
import * as CssElementQuery from 'css-element-queries';
import { ResizeSensor } from 'css-element-queries';

@Component({
  selector: 'sqtm-app-orchestrator-additional-configuration-panel',
  templateUrl: './orchestrator-additional-configuration-panel.component.html',
  styleUrls: ['./orchestrator-additional-configuration-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrchestratorAdditionalConfigurationPanelComponent implements AfterViewInit, OnDestroy {
  @ViewChild('codeEditor') codeEditorElmRef?: ElementRef;

  private codeEditor;

  @Input()
  theme = 'ace/theme/chrome';

  @Input()
  value: string;

  @Input()
  canWrite = true;

  edit = false;

  split: any;

  additionalConfigurationHelpKey =
    'sqtm-core.administration-workspace.servers.test-automation-servers.additional-configuration.help';

  @Output()
  confirmEvent = new EventEmitter<string>();

  @Output()
  checkSyntaxEvent = new EventEmitter<string>();

  private readonly aceBaseField = 'assets/js/ace/';

  private _resizeSensor: ResizeSensor;

  constructor(
    private translateService: TranslateService,
    private readonly sessionPingService: SessionPingService,
  ) {}

  ngAfterViewInit(): void {
    this.configureAce();
    this.initializeEditor();
  }

  ngOnDestroy(): void {
    if (this.edit) {
      this.sessionPingService.endLongTermEdition();
    }
  }

  private configureAce(): void {
    ace.config.set('basePath', this.aceBaseField);
    ace.config.set('modePath', this.aceBaseField);
    ace.config.set('themePath', this.aceBaseField);
    ace.config.setModuleUrl('ace/snippets/yaml', `${this.aceBaseField}snippets/yaml.js`);
  }

  private initializeEditor(): void {
    ace.config.loadModule('ace/ext/split', (splitModule) => {
      const container = this.codeEditorElmRef.nativeElement;
      this.split = new splitModule.Split(container, 'ace/theme/chrome-grey', 1);
      this.codeEditor = this.split.getEditor(0);
      if (this.value == null) {
        this.value = '';
      }
      this.codeEditor.getSession().setValue(this.value);
      this.codeEditor.setTheme('ace/theme/chrome-grey');
      this.codeEditor.getSession().setUseWrapMode(true);
      this.codeEditor.getSession().setWrapLimitRange(120, 120);
      this.codeEditor.setOptions({
        tabSize: 2,
        useSoftTabs: true,
        printMarginColumn: 140,
        readOnly: true,
        highlightActiveLine: false,
        highlightGutterLine: false,
        enableBasicAutocompletion: true,
        enableSnippets: true,
        enableLiveAutocompletion: true,
      });
      this.codeEditor.getSession().setMode('ace/mode/yaml');
      this.codeEditor.setShowFoldWidgets(true);
      this.codeEditor.getSession().setUndoManager(new ace.UndoManager());

      this.addKeyboardBindings();
      this.initializeResizeSensor();
    });

    this.addSnippedFeature();
  }

  private addSnippedFeature(): void {
    ace.config.loadModule('ace/snippets', (snippetModule) => {
      const snippetManager = snippetModule.snippetManager;
      ace.config.loadModule('ace/snippets/yaml', (m) => {
        if (m) {
          snippetManager.files.gherkin = m;
          m.snippets = snippetManager.parseSnippetFile(m.snippetText);
          snippetManager.register(m.snippets, m.scope);
        }
      });
    });
  }

  private addKeyboardBindings(): void {
    this.codeEditor.commands.addCommand({
      name: 'saveYamlScript',
      bindKey: { win: 'Ctrl-Alt-S', mac: 'Command-Alt-S' },
      exec: () => this.confirm(),
      readOnly: false,
    });

    this.codeEditor.commands.addCommand({
      name: 'undo',
      bindKey: { win: 'Ctrl-Z', mac: 'Command-Z' },
      exec: (editor) => editor.session.getUndoManager().undo(),
    });

    this.codeEditor.commands.addCommand({
      name: 'redo',
      bindKey: { win: 'Ctrl-Y', mac: 'Command-Y' },
      exec: (editor) => editor.session.getUndoManager().redo(),
    });
  }

  toggleToEditMode(): void {
    if (this.canWrite && !this.edit) {
      this.edit = true;
      this.codeEditor.setOption('readOnly', false);
      this.codeEditor.setTheme(this.theme);

      this.sessionPingService.beginLongTermEdition();
    }
  }

  disableEditMode(): void {
    if (this.edit) {
      this.edit = false;
      this.restoreNotEditMode();

      this.sessionPingService.endLongTermEdition();
    }
  }

  cancel(): void {
    this.codeEditor.getSession().setValue(this.value);
    this.disableEditMode();
  }

  confirm(): void {
    this.confirmEvent.emit(this.codeEditor.getSession().getValue());
    this.disableEditMode();
  }
  private restoreNotEditMode(): void {
    this.codeEditor.setTheme('ace/theme/chrome-grey');
    this.codeEditor.setOptions({
      readOnly: true,
      highlightActiveLine: false,
      highlightGutterLine: false,
    });
  }

  scriptValue(): void {
    return this.codeEditor.session.getValue();
  }

  private updateSplitSize(): void {
    this.split.resize();
  }

  private initializeResizeSensor(): void {
    this._resizeSensor = new CssElementQuery.ResizeSensor(this.codeEditorElmRef.nativeElement, () =>
      this.updateSplitSize(),
    );
  }

  toggleShowHelp(): void {
    if (this.split.getSplits() === 2) {
      this.split.setSplits(1);
      this.codeEditor.getSession().setWrapLimitRange(120, 120);
      return;
    }
    this.split.setSplits(2);
    this.codeEditor.getSession().setWrapLimitRange(60, 60);
    const documentationEditor = this.split.getEditor(1);
    documentationEditor.getSession().setMode('ace/mode/yaml');
    documentationEditor.setReadOnly(true);
    documentationEditor.setOptions({
      showPrintMargin: false,
      tabSize: 2,
      useSoftTabs: true,
      readOnly: true,
      highlightActiveLine: false,
      highlightGutterLine: false,
    });
    documentationEditor.session.setValue(
      this.translateService.instant(this.additionalConfigurationHelpKey),
    );
    documentationEditor.getSession().setUseWrapMode(true);
    documentationEditor.getSession().setWrapLimitRange(60, 60);
  }

  checkSyntax() {
    this.checkSyntaxEvent.emit(this.codeEditor.session.getValue());
  }
}
