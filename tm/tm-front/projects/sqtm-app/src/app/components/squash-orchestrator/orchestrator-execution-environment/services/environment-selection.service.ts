import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { concatMap, Observable, Subject } from 'rxjs';
import {
  catchError,
  distinctUntilChanged,
  filter,
  map,
  switchMap,
  take,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import {
  AutomatedExecutionEnvironment,
  AutomationEnvironmentTagHolder,
  createStore,
  RestService,
} from 'sqtm-core';
import {
  EnvironmentSelectionPanelState,
  getInitialEnvironmentSelectionState,
} from './environment-selection.state';

@Injectable()
export class EnvironmentSelectionService {
  private readonly PROJECT_URL_PREFIX = 'generic-projects';
  private readonly SERVER_URL_PREFIX = 'test-automation-servers';
  private readonly UNRESTRICTED_URL_PREFIX = 'test-automation';

  private readonly store = createStore<EnvironmentSelectionPanelState>(
    getInitialEnvironmentSelectionState(),
  );

  public readonly selectedTagsChanged$: Observable<string[]>;

  private _projectTokenChanged: Subject<void> = new Subject<void>();

  projectTokenChanged$: Observable<void>;

  public readonly filteredAvailableEnvironments$ = this.store.state$.pipe(
    map((state: EnvironmentSelectionPanelState) => applyEnvironmentsFiltering(state)),
  );

  public state$: Observable<EnvironmentSelectionPanelState> = this.store.state$;

  constructor(public readonly restService: RestService) {
    this.projectTokenChanged$ = this._projectTokenChanged.asObservable();
    this.selectedTagsChanged$ = this.store.state$.pipe(
      map((state) => state.selectedTags),
      distinctUntilChanged(),
    );
  }

  public load(entityId: number, entityType: AutomationEnvironmentTagHolder): Observable<void> {
    return this.initializeAutomatedExecutionEnvironments(entityId, entityType);
  }

  public unload(): void {
    this.store.commit(getInitialEnvironmentSelectionState());
  }

  public complete(): void {
    this.store.complete();
  }

  public handleServerCredentialsChange(isRegistered: boolean): void {
    this.store.state$
      .pipe(
        take(1),
        map((state: EnvironmentSelectionPanelState) => {
          return {
            ...state,
            hasMissingCredentials: !isRegistered,
          };
        }),
        tap((newState) => this.store.commit(newState)),
        tap(() => this.reloadAvailableEnvironments()),
      )
      .subscribe();
  }

  public handleBoundServerChanged(testAutomationServerId: number): void {
    this.store.state$
      .pipe(
        take(1),
        tap((state) => this.store.commit({ ...state, testAutomationServerId })),
        tap(() => this.reloadAvailableEnvironments()),
      )
      .subscribe();
  }

  private initializeAutomatedExecutionEnvironments(
    entityId: number,
    entityType: AutomationEnvironmentTagHolder,
  ): Observable<void> {
    this.notifyIsLoadingEnvironments();
    return this.getAvailableAutomatedExecutionEnvironments(entityId, entityType).pipe(
      withLatestFrom(this.store.state$),
      map(([response, state]) => mergeResponseIntoState(response, state, entityType)),
      tap((state: EnvironmentSelectionPanelState) => this.store.commit(state)),
      catchError((err) => this.handleLoadError(err, entityId, entityType)),
    );
  }

  private notifyIsLoadingEnvironments(): void {
    this.store.state$
      .pipe(
        take(1),
        tap((state: any) =>
          this.store.commit({
            ...state,
            hasEnvironmentLoadingError: false,
            isLoadingEnvironments: true,
          }),
        ),
      )
      .subscribe();
  }

  private handleLoadError(
    error: Error,
    tagHolderId: number,
    tagHolderType: AutomationEnvironmentTagHolder,
  ): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      tap((state: any) =>
        this.store.commit({
          ...state,

          testAutomationServerId:
            tagHolderType === AutomationEnvironmentTagHolder.TEST_AUTOMATION_SERVER
              ? tagHolderId
              : null,
          tagHolderId,
          tagHolderType,
          selectedTags: [],
          hasEnvironmentLoadingError: true,
          isLoadingEnvironments: false,
          errorMessage: error.message,
        }),
      ),
    );
  }

  private getAvailableAutomatedExecutionEnvironments(
    entityId: number,
    entityType: AutomationEnvironmentTagHolder,
  ): Observable<EnvironmentSelectionPanelDto> {
    const urlPrefix: string = this.resolveUrlPrefix(entityType);
    const urlParts = [urlPrefix, entityId.toString(), 'automated-execution-environments', 'all'];
    return this.restService.getWithoutErrorHandling<EnvironmentSelectionPanelDto>(urlParts);
  }

  updateEnvironmentTags(tags: string[]): void {
    this.store.state$
      .pipe(
        take(1),
        switchMap((state: EnvironmentSelectionPanelState) =>
          this.doUpdateEnvironmentTags(state.tagHolderId, state.tagHolderType, tags),
        ),
        withLatestFrom(this.store.state$),
        map(([, state]: [any, EnvironmentSelectionPanelState]) => {
          return {
            ...state,
            selectedTags: tags,
            areProjectTagsInherited: false,
          };
        }),
        tap((nextState: EnvironmentSelectionPanelState) => this.store.commit(nextState)),
      )
      .subscribe();
  }

  updateEnvironmentTagsOnlyInState(tags: string[]): void {
    this.store.state$
      .pipe(
        take(1),
        map((state: EnvironmentSelectionPanelState) => {
          return {
            ...state,
            selectedTags: tags,
          };
        }),
        tap((nextState: EnvironmentSelectionPanelState) => this.store.commit(nextState)),
      )
      .subscribe();
  }

  private doUpdateEnvironmentTags(
    tagHolderId: number,
    tagHolderType: AutomationEnvironmentTagHolder,
    tags: string[],
  ): Observable<any> {
    const urlPrefix =
      tagHolderType === AutomationEnvironmentTagHolder.TEST_AUTOMATION_SERVER
        ? this.SERVER_URL_PREFIX
        : this.PROJECT_URL_PREFIX;
    const parts = [urlPrefix, tagHolderId.toString(), 'environment-tags'];
    return this.restService.post(parts, { list: tags });
  }

  clearTagOverridesOnlyInState() {
    this.store.state$
      .pipe(
        take(1),
        map((state: EnvironmentSelectionPanelState) => {
          const defaultTags = state.areProjectTagsInherited ? state.serverTags : state.projectTags;
          return {
            ...state,
            selectedTags: defaultTags,
          };
        }),
        tap((nextState: EnvironmentSelectionPanelState) => this.store.commit(nextState)),
      )
      .subscribe();
  }

  clearTagOverrides() {
    this.store.state$
      .pipe(
        take(1),
        switchMap((state: EnvironmentSelectionPanelState) =>
          this.doClearTagOverrides(state.tagHolderId, state.tagHolderType),
        ),
        withLatestFrom(this.store.state$),
        map(([, state]: [any, EnvironmentSelectionPanelState]) => {
          return {
            ...state,
            selectedTags: state.serverTags,
            areProjectTagsInherited: true,
          };
        }),
        tap((nextState: EnvironmentSelectionPanelState) => this.store.commit(nextState)),
      )
      .subscribe();
  }

  private doClearTagOverrides(
    tagHolderId: number,
    tagHolderType: AutomationEnvironmentTagHolder,
  ): Observable<any> {
    if (tagHolderType !== AutomationEnvironmentTagHolder.PROJECT) {
      throw new Error('Only project tags override can be removed.');
    }

    return this.restService.delete([
      this.PROJECT_URL_PREFIX,
      tagHolderId.toString(),
      'environment-tags',
    ]);
  }

  updateProjectTokenOverride(token: string): void {
    this.store.state$
      .pipe(
        take(1),
        switchMap((state: EnvironmentSelectionPanelState) =>
          this.doUpdateProjectTokenOverride(state.tagHolderId, state.testAutomationServerId, token),
        ),
      )
      .subscribe(() => this.handleProjectTokenChanged());
  }

  private doUpdateProjectTokenOverride(
    projectId: number,
    serverId: number,
    token: string,
  ): Observable<any> {
    const parts = [
      this.PROJECT_URL_PREFIX,
      projectId.toString(),
      'automated-execution-environments/tokens',
      serverId.toString(),
    ];

    return this.restService.post(parts, { token });
  }

  clearProjectTokenOverride(): void {
    this.store.state$
      .pipe(
        take(1),
        switchMap((state: EnvironmentSelectionPanelState) => {
          return this.doClearProjectTokenOverride(state.tagHolderId, state.testAutomationServerId);
        }),
      )
      .subscribe(() => this.handleProjectTokenChanged());
  }

  private handleProjectTokenChanged(): void {
    this.reloadAvailableEnvironments();
    this._projectTokenChanged.next();
  }

  private doClearProjectTokenOverride(projectId: number, serverId: number): Observable<any> {
    const urlParts = [
      this.PROJECT_URL_PREFIX,
      projectId.toString(),
      'automated-execution-environments/tokens',
      serverId.toString(),
    ];

    return this.restService.delete(urlParts);
  }

  // To use after initial load to re-fetch data
  reloadAvailableEnvironments(): void {
    this.state$
      .pipe(
        take(1),
        filter((state) => state.testAutomationServerId != null),
        tap((state) => this.load(state.tagHolderId, state.tagHolderType).subscribe()),
      )
      .subscribe();
  }

  public reloadAvailableTags(): void {
    this.state$
      .pipe(
        take(1),
        filter((state) => state.testAutomationServerId != null),
        concatMap((state) => {
          const urlPrefix: string = this.resolveUrlPrefix(state.tagHolderType);
          const urlParts = [urlPrefix, state.tagHolderId.toString(), 'available-tags'];
          return this.restService.get<{ list: string[] }>(urlParts);
        }),
        withLatestFrom(this.state$),
        map(([response, state]) => ({
          ...state,
          availableEnvironmentTags: response.list,
        })),
        tap((nextState) => this.store.commit(nextState)),
      )
      .subscribe();
  }

  private resolveUrlPrefix(type: AutomationEnvironmentTagHolder): string {
    switch (type) {
      case AutomationEnvironmentTagHolder.TEST_AUTOMATION_SERVER:
        return this.SERVER_URL_PREFIX;
      case AutomationEnvironmentTagHolder.PROJECT:
        return this.PROJECT_URL_PREFIX;
      default:
        return this.UNRESTRICTED_URL_PREFIX;
    }
  }

  preventNullServerId(serverId: number) {
    this.state$
      .pipe(
        filter((state) => state.testAutomationServerId == null),
        map((state) => ({
          ...state,
          testAutomationServerId: serverId,
        })),
        tap((nextState) => this.store.commit(nextState)),
      )
      .subscribe();
  }
}

function applyEnvironmentsFiltering(
  state: EnvironmentSelectionPanelState,
): AutomatedExecutionEnvironment[] {
  const { selectedTags, availableEnvironments } = state;

  if (selectedTags.length === 0) {
    return availableEnvironments;
  }

  return availableEnvironments.filter((environment) => {
    const environmentTags = environment.tags;
    return selectedTags.every((selectedTag) => environmentTags.includes(selectedTag));
  });
}

function extractEnvironmentTags(availableEnvironments: AutomatedExecutionEnvironment[]): string[] {
  if (availableEnvironments == null) {
    return null;
  }

  return _.uniq(availableEnvironments.map((env) => env.tags).flat()).sort((a, b) =>
    a.localeCompare(b),
  );
}

// Maps to EnvironmentSelectionPanelDto server-side
export interface EnvironmentSelectionPanelDto {
  environments: {
    environments?: AutomatedExecutionEnvironment[];
  };

  server: {
    testAutomationServerId: number;
    defaultTags: string[];
    hasServerCredentials: boolean;
  };

  project?: {
    projectId: number;
    hasProjectToken: boolean;
    projectTags: string[];
    areProjectTagsInherited: boolean;
  };

  errorMessage?: string;
}

function mergeResponseIntoState(
  response: EnvironmentSelectionPanelDto,
  state: EnvironmentSelectionPanelState,
  tagHolderType: AutomationEnvironmentTagHolder,
): EnvironmentSelectionPanelState {
  const errorMessage = response.errorMessage;
  const nonNullDefaultTags = response.server.defaultTags ?? [];
  const nonNullEnvironments = response.environments?.environments ?? [];
  const hasEnvironmentLoadingError = response.environments?.environments == null;
  const testAutomationServerId = response.server.testAutomationServerId;
  const hasServerCredentials = response.server.hasServerCredentials;

  const nonNullProjectTags = response.project?.projectTags ?? [];
  const hasProjectToken = response.project?.hasProjectToken;

  switch (tagHolderType) {
    case AutomationEnvironmentTagHolder.TEST_AUTOMATION_SERVER:
      return {
        ...state,
        tagHolderId: testAutomationServerId,
        tagHolderType,
        testAutomationServerId,
        serverTags: nonNullDefaultTags,
        selectedTags: nonNullDefaultTags,
        availableEnvironments: nonNullEnvironments,
        availableEnvironmentTags: extractEnvironmentTags(nonNullEnvironments),
        hasMissingCredentials: !hasServerCredentials,
        hasEnvironmentLoadingError,
        isLoadingEnvironments: false,
        projectId: null,
        projectTags: null,
        areProjectTagsInherited: null,
        errorMessage,
      };
    case AutomationEnvironmentTagHolder.PROJECT:
    case AutomationEnvironmentTagHolder.EXECUTION_DIALOG:
      return {
        ...state,
        tagHolderId: response.project.projectId,
        tagHolderType,
        testAutomationServerId,
        serverTags: nonNullDefaultTags,
        projectId: response.project.projectId,
        projectTags: nonNullProjectTags,
        areProjectTagsInherited: response.project.areProjectTagsInherited,
        selectedTags: response.project.areProjectTagsInherited
          ? nonNullDefaultTags
          : nonNullProjectTags,
        availableEnvironments: nonNullEnvironments,
        availableEnvironmentTags: extractEnvironmentTags(nonNullEnvironments),
        hasMissingCredentials: !(hasServerCredentials || hasProjectToken),
        hasProjectToken,
        hasEnvironmentLoadingError,
        isLoadingEnvironments: false,
        errorMessage,
      };
    default:
      throw new Error(`Unsupported tag holder type ${tagHolderType}`);
  }
}
