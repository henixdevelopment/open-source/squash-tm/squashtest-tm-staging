import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnDestroy,
  OnInit,
  signal,
  ViewContainerRef,
  WritableSignal,
} from '@angular/core';
import {
  ExecutionEnvironmentCountProjectInfoState,
  ExecutionEnvironmentCountState,
} from '../../services/execution-environment-count.state';
import { DialogService } from 'sqtm-core';
import {
  EnvironmentsStatusesSummaryDialogComponent,
  EnvironmentStatusesSummaryDialogConfiguration,
} from '../environments-statuses-summary-dialog/environments-statuses-summary-dialog.component';
import { ExecutionEnvironmentSummaryService } from '../../services/execution-environment-summary.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-environments-statuses-count',
  templateUrl: './environments-statuses-count.component.html',
  styleUrl: './environments-statuses-count.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EnvironmentsStatusesCountComponent implements OnInit, OnDestroy {
  unsub$ = new Subject<void>();

  execEnvStatuses: string[] = ['IDLE', 'BUSY', 'PENDING', 'UNREACHABLE'];
  private readonly execEnvCountGlobalStatusIcon: Record<string, string> = {
    success: 'check-circle',
    error: 'close',
    warningNoToken: 'warning',
    warningUnreachable: 'warning',
  };
  private readonly execEnvCountGlobalStatusColor: Record<string, string> = {
    success: 'green',
    error: 'red',
    warningNoToken: 'orange',
    warningUnreachable: 'darkorange',
  };
  private readonly execEnvCountGlobalStatusI18nKeys: Record<string, string> = {
    success: 'sqtm-core.campaign-workspace.automation.execution-environments-count.success',
    error: 'sqtm-core.campaign-workspace.automation.execution-environments-count.error',
    warningNoToken:
      'sqtm-core.campaign-workspace.automation.execution-environments-count.warning-no-token',
    warningUnreachable:
      'sqtm-core.campaign-workspace.automation.execution-environments-count.warning-unreachable',
  };

  statusesCount: { [key: string]: number };
  projects: ExecutionEnvironmentCountProjectInfoState[];
  nbOfSquashOrchestratorServers: number;
  nbOfUnreachableSquashOrchestrators: number;
  nbOfServersWithMissingToken: number;
  @Input()
  holderId: number;
  @Input()
  holderType: string;
  $isLoading: WritableSignal<boolean> = signal(true);

  constructor(
    private executionEnvironmentSummaryService: ExecutionEnvironmentSummaryService,
    private vcr: ViewContainerRef,
    private readonly dialogService: DialogService,
  ) {}

  ngOnInit(): void {
    this.getStatusesCount();
    this.executionEnvironmentSummaryService.refreshView$
      .pipe(takeUntil(this.unsub$))
      .subscribe(() => {
        this.refreshCount();
      });
  }

  refreshCount(): void {
    this.$isLoading.set(true);
    this.getStatusesCount();
  }

  showExecutionEnvironments(): void {
    if (this.statusesCount == null) {
      return;
    }

    const data: EnvironmentStatusesSummaryDialogConfiguration = {
      id: 'environments-statuses-summary',
      projects: this.projects,
    };

    this.dialogService.openDialog({
      id: 'environments-statuses-summary',
      component: EnvironmentsStatusesSummaryDialogComponent,
      viewContainerReference: this.vcr,
      data: {
        ...data,
      },
      maxWidth: 800,
      maxHeight: 680,
    });
    this.refreshCount();
  }

  getCount(status: string): number {
    return this.statusesCount != null && this.statusesCount[status] > 0
      ? this.statusesCount[status]
      : 0;
  }

  getIconType(status: string): string {
    return status == 'IDLE'
      ? 'sqtm-core-generic:settings'
      : 'sqtm-core-campaign:exec-env-' + status.toLowerCase();
  }

  private getStatusesCount(): void {
    this.executionEnvironmentSummaryService
      .getEnvironmentsStatusesCount(this.holderType, this.holderId)
      .subscribe((executionEnvironmentCountState: ExecutionEnvironmentCountState) => {
        this.statusesCount = executionEnvironmentCountState.statuses;
        this.projects = executionEnvironmentCountState.projects;
        this.nbOfSquashOrchestratorServers =
          executionEnvironmentCountState.nbOfSquashOrchestratorServers;
        this.nbOfUnreachableSquashOrchestrators =
          executionEnvironmentCountState.nbOfUnreachableSquashOrchestrators;
        this.nbOfServersWithMissingToken =
          executionEnvironmentCountState.nbOfServersWithMissingToken;
        this.$isLoading.set(false);
      });
  }

  getIconsColor(): string {
    return this.nbOfUnreachableSquashOrchestrators == this.nbOfSquashOrchestratorServers
      ? '#ffcdc8'
      : '#62737f';
  }

  getGlobalEnvBadgeType(): string {
    return this.getGlobalEnvValue(this.execEnvCountGlobalStatusIcon);
  }

  getGlobalEnvBadgeColor(): string {
    return this.getGlobalEnvValue(this.execEnvCountGlobalStatusColor);
  }

  getGlobalEnvBadgeTooltipMessage(): string {
    return this.getGlobalEnvValue(this.execEnvCountGlobalStatusI18nKeys);
  }

  private getGlobalEnvValue(values: Record<string, string>): string {
    if (this.nbOfServersWithMissingToken == 0 && this.nbOfUnreachableSquashOrchestrators == 0) {
      return values.success;
    }
    if (this.nbOfUnreachableSquashOrchestrators == this.nbOfSquashOrchestratorServers) {
      return values.error;
    }
    if (this.nbOfServersWithMissingToken > 0 && this.nbOfUnreachableSquashOrchestrators == 0) {
      return values.warningNoToken;
    }
    return values.warningUnreachable;
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
