import { Component, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';
import {
  AbstractDeleteCellRenderer,
  AutomationEnvironmentTagHolder,
  DialogService,
  GridColumnId,
  GridService,
} from 'sqtm-core';
import { Observable, Subject } from 'rxjs';
import { finalize, map, take, takeUntil, tap } from 'rxjs/operators';
import {
  BoundEnvironmentVariableService,
  BoundEnvironmentVariableState,
} from '../../../services/bound-environment-variable.service';

@Component({
  selector: 'sqtm-app-delete-ev-binding-cell',
  template: `
    @if (componentData$ | async; as componentData) {
      @if (componentData.entityType !== executionDialogType) {
        <sqtm-core-toggle-icon
          [attr.data-test-button-id]="'remove-options'"
          class="full-height full-width icon-container current-workspace-main-color __hover_pointer"
        >
          <i
            nz-icon
            nzType="sqtm-core-generic:unlink"
            nzTheme="outline"
            class="table-icon-size"
            (click)="showDeleteConfirm()"
          ></i>
        </sqtm-core-toggle-icon>
      }
    }
  `,
  styleUrls: ['./delete-ev-binding-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteEvBindingCellComponent extends AbstractDeleteCellRenderer implements OnDestroy {
  componentData$: Observable<BoundEnvironmentVariableState>;
  unsub$ = new Subject<void>();

  messageKey: string;

  executionDialogType: AutomationEnvironmentTagHolder =
    AutomationEnvironmentTagHolder.EXECUTION_DIALOG;

  constructor(
    public grid: GridService,
    cdRef: ChangeDetectorRef,
    protected dialogService: DialogService,
    private boundEnvironmentVariableService: BoundEnvironmentVariableService,
  ) {
    super(grid, cdRef, dialogService);
    this.componentData$ = this.boundEnvironmentVariableService.state$.pipe(takeUntil(this.unsub$));
    this.initializeMessageKey();
  }

  protected doDelete() {
    this.grid.beginAsyncOperation();
    this.boundEnvironmentVariableService
      .unbindEnvironmentVariables([this.row.data[GridColumnId.id]])
      .pipe(finalize(() => this.grid.completeAsyncOperation()))
      .subscribe();
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  protected getTitleKey(): string {
    return 'sqtm-core.administration-workspace.servers.test-automation-servers.environment-variables.dialog.title.unbind-one';
  }

  initializeMessageKey() {
    this.componentData$
      .pipe(
        take(1),
        map((state) =>
          state.entityType === AutomationEnvironmentTagHolder.PROJECT
            ? 'sqtm-core.administration-workspace.servers.test-automation-servers.environment-variables.dialog.message.unbind-one-from-project'
            : 'sqtm-core.administration-workspace.servers.test-automation-servers.environment-variables.dialog.message.unbind-one-from-server',
        ),
        tap((key) => (this.messageKey = key)),
      )
      .subscribe();
  }
  protected getMessageKey(): string {
    return this.messageKey;
  }
}
