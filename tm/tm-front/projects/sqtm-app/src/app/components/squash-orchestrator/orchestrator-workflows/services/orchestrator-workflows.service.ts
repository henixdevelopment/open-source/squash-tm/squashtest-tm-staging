import { computed, Injectable, Signal, signal, WritableSignal } from '@angular/core';
import { OrchestratorResponse, RestService, Workflow } from 'sqtm-core';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class OrchestratorWorkflowsService {
  private $state: WritableSignal<OrchestratorWorkflowState> = signal(null);

  $orchestratorResponse: Signal<OrchestratorResponse<Workflow[]>> = computed(
    () => this.$state()?.orchestratorResponse,
  );

  $loading: WritableSignal<boolean> = signal(true);

  constructor(private restService: RestService) {}

  initializeWorkflows(
    serverId: number,
    projectId: number,
  ): Observable<OrchestratorResponse<Workflow[]>> {
    const urlPart: string[] = [
      'orchestrator-operation',
      serverId.toString(),
      'project',
      projectId.toString(),
      'workflows',
    ];
    return this.restService
      .get<OrchestratorResponse<Workflow[]>>(urlPart)
      .pipe(tap((response) => this.handleWorkflowsResponse(response, serverId, projectId)));
  }

  private handleWorkflowsResponse(
    response: OrchestratorResponse<Workflow[]>,
    serverId: number,
    projectId: number,
  ) {
    this.$state.set({
      orchestratorResponse: response,
      projectId,
      serverId,
    });
    this.$loading.set(false);
  }

  stopWorkflow(workflowId: string) {
    const state = this.$state();
    const urlPart: string[] = [
      'orchestrator-operation',
      state.serverId.toString(),
      'project',
      state.projectId.toString(),
      'kill-workflow',
    ];
    const body = { workflowId };
    return this.restService.post<OrchestratorResponse<void>>(urlPart, body);
  }

  reloadWorkflows() {
    const state = this.$state();
    this.initializeWorkflows(state.serverId, state.projectId).subscribe();
  }

  loading() {
    this.$loading.set(true);
  }
}

export interface OrchestratorWorkflowState {
  orchestratorResponse: OrchestratorResponse<Workflow[]>;
  projectId: number;
  serverId: number;
}
