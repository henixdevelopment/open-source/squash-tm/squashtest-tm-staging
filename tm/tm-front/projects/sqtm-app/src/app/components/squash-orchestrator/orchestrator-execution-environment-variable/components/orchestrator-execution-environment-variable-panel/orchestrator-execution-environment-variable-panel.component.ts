import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  InjectionToken,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewContainerRef,
} from '@angular/core';
import {
  ActionErrorDisplayService,
  AutomationEnvironmentTagHolder,
  booleanColumn,
  BoundEnvironmentVariable,
  DataRow,
  deleteColumn,
  DialogService,
  EnvironmentVariable,
  Extendable,
  Fixed,
  GridColumnId,
  GridDefinition,
  GridService,
  Identifier,
  indexColumn,
  ListItem,
  smallGrid,
  Sort,
  StyleDefinitionBuilder,
} from 'sqtm-core';
import {
  BoundEnvironmentVariableService,
  BoundEnvironmentVariableState,
} from '../../../orchestrator-execution-environment-variable/services/bound-environment-variable.service';
import { concatMap, Observable, Subject } from 'rxjs';
import { catchError, filter, finalize, map, take, takeUntil, tap } from 'rxjs/operators';
import { environmentVariableValueColumn } from '../cell-renderers/ev-field-value-cell/ev-field-value-cell.component';
import { DeleteEvBindingCellComponent } from '../cell-renderers/delete-ev-binding-cell/delete-ev-binding-cell.component';
import {
  EnvironmentVariableBindingDialogComponent,
  EnvironmentVariableBindingDialogConfig,
} from '../dialog/environment-variable-binding-dialog/environment-variable-binding-dialog.component';
import { Overlay } from '@angular/cdk/overlay';
import { environmentVariableReinitializeColumn } from '../cell-renderers/reinitialize-value-cell/reinitialize-value-cell.component';
import { environmentVariableNameLinkColumn } from '../cell-renderers/ev-link-cell/ev-link-cell.component';

export const EV_ASSOCIATION_TABLE_CONF = new InjectionToken('EV_ASSOCIATION_TABLE_CONF');
export const EV_ASSOCIATION_TABLE = new InjectionToken('EV_ASSOCIATION_TABLE');

export function EnvironmentVariableAssociationsTableDefinition(): GridDefinition {
  return smallGrid('environment-variable-associations')
    .withColumns([
      indexColumn().withViewport('leftViewport'),
      environmentVariableNameLinkColumn(GridColumnId.name)
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5))
        .disableSort(),
      environmentVariableValueColumn(GridColumnId.value)
        .withI18nKey('sqtm-core.generic.label.value')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5))
        .disableSort(),
      booleanColumn(GridColumnId.interpreted)
        .withI18nKey('sqtm-core.grid.header.interpreted')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5))
        .disableSort(),
      environmentVariableReinitializeColumn(GridColumnId.id).changeWidthCalculationStrategy(
        new Fixed(60),
      ),
      deleteColumn(DeleteEvBindingCellComponent),
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withInitialSortedColumns([{ id: GridColumnId.name, sort: Sort.ASC }])
    .withRowHeight(35)
    .build();
}
@Component({
  selector: 'sqtm-app-orchestrator-execution-environment-variable-panel',
  templateUrl: './orchestrator-execution-environment-variable-panel.component.html',
  styleUrls: ['./orchestrator-execution-environment-variable-panel.component.less'],
  providers: [
    {
      provide: BoundEnvironmentVariableService,
      useClass: BoundEnvironmentVariableService,
    },
    {
      provide: GridService,
      useExisting: EV_ASSOCIATION_TABLE,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrchestratorExecutionEnvironmentVariablePanelComponent
  implements AfterViewInit, OnInit, OnDestroy
{
  state$: Observable<BoundEnvironmentVariableState>;

  private unsub$ = new Subject<void>();

  @Input()
  entityId: number;

  @Input()
  entityType: AutomationEnvironmentTagHolder;

  @Input()
  iterationId: number = null;

  projectType: AutomationEnvironmentTagHolder = AutomationEnvironmentTagHolder.PROJECT;
  environmentVariablesDialogData: EnvironmentVariable[];
  listItems: ListItem[] = [];

  @Output()
  valueChanged = new EventEmitter<ListItem[]>();

  constructor(
    public boundEnvironmentVariableService: BoundEnvironmentVariableService,
    private gridService: GridService,
    private dialogService: DialogService,
    private overlay: Overlay,
    private viewContainerRef: ViewContainerRef,
    public readonly cdRef: ChangeDetectorRef,
    private actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    this.state$ = boundEnvironmentVariableService.state$;
  }

  ngOnInit(): void {
    this.initializeTable();
  }

  private initializeTable() {
    const environmentVariables$ = this.state$.pipe(
      takeUntil(this.unsub$),
      map((response) => response?.boundEnvironmentVariables),
    );

    this.gridService.connectToDatasource(environmentVariables$, 'name');
  }

  ngAfterViewInit(): void {
    if (this.iterationId) {
      this.boundEnvironmentVariableService
        .loadForAutomatedExecution(this.iterationId, this.entityId)
        .subscribe();
    } else {
      this.boundEnvironmentVariableService.load(this.entityId, this.entityType).subscribe();
    }
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  get selectedItems(): ListItem[] {
    return this.listItems.filter((item) => item.selected);
  }

  set selectedItems(newSelection: ListItem[]) {
    this.listItems = this.listItems.map((item) => ({
      ...item,
      selected: newSelection.map((i) => i.id).includes(item.id),
    }));
    this.cdRef.detectChanges();
  }

  openBindEnvironmentVariables(): void {
    this.boundEnvironmentVariableService
      .getAllEnvironmentVariables()
      .pipe(take(1))
      .subscribe((references) => {
        this.initializeListItem(references);
        this.displayOverlay(this.listItems);
      });
  }

  private initializeListItem(references: EnvironmentVariable[]) {
    this.boundEnvironmentVariableService.state$
      .pipe(
        take(1),
        map((state) => state.boundEnvironmentVariables),
        map((boundEnvironmentVariables: BoundEnvironmentVariable[]) =>
          boundEnvironmentVariables.map((variable) => variable.id),
        ),
      )
      .subscribe((boundVariableIds) => {
        this.environmentVariablesDialogData = references;
        this.listItems = references
          .filter((reference) => !boundVariableIds.includes(reference.id))
          .map((reference) => ({
            id: reference.id,
            label: reference.name,
            customTemplate: null,
            selected: false,
          }));
      });
  }

  bindEnvironmentVariables(items: Identifier[]): Observable<void> {
    return this.boundEnvironmentVariableService.bindEnvironmentVariables(items);
  }

  displayOverlay(listItems: ListItem[]): void {
    const dialogReference = this.dialogService.openDialog<
      EnvironmentVariableBindingDialogConfig,
      ListItem[]
    >({
      component: EnvironmentVariableBindingDialogComponent,
      viewContainerReference: this.viewContainerRef,
      data: {
        titleKey:
          'sqtm-core.administration-workspace.entities-customization.environment-variables.bind-many',
        environmentVariables: listItems,
        dialogData: this.environmentVariablesDialogData,
      },
      id: 'bind-environment-variable-dialog',
      width: 600,
      height: 500,
    });
    dialogReference.dialogResultChanged$
      .pipe(
        takeUntil(dialogReference.dialogClosed$),
        filter((result: ListItem[]) => result != null && result.length > 0),
      )
      .subscribe((items) => this.valueChanged.emit(items));
  }

  handleBoundServerChanged() {
    this.boundEnvironmentVariableService.handleBoundServerChanged(this.entityId, this.entityType);
  }
  getBoundEnvironmentVariables(): Observable<BoundEnvironmentVariable[]> {
    return this.boundEnvironmentVariableService.state$.pipe(
      take(1),
      map((state) => state.boundEnvironmentVariables),
    );
  }

  openUnbindEnvironmentVariableDialog(): void {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        filter((rows: DataRow[]) => rows.length > 0),
        map((rows: DataRow[]) => rows.map((row) => row.data.id)),
        concatMap((ids: number[]) => this.showConfirmUnbindEnvironmentVariableDialog(ids)),
        filter(({ confirmUnbind }) => confirmUnbind),
        tap(() => this.gridService.beginAsyncOperation()),
        concatMap(({ environmentVariableIds }) =>
          this.boundEnvironmentVariableService.unbindEnvironmentVariables(environmentVariableIds),
        ),
        catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
        finalize(() => this.gridService.completeAsyncOperation()),
      )
      .subscribe();
  }

  private showConfirmUnbindEnvironmentVariableDialog(environmentVariableIds: number[]): Observable<{
    confirmUnbind: boolean;
    environmentVariableIds: number[];
  }> {
    const messageKey =
      this.entityType === AutomationEnvironmentTagHolder.PROJECT
        ? 'sqtm-core.administration-workspace.servers.test-automation-servers.environment-variables.dialog.message.unbind-many-from-project'
        : 'sqtm-core.administration-workspace.servers.test-automation-servers.environment-variables.dialog.message.unbind-many-from-server';

    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey:
        'sqtm-core.administration-workspace.servers.test-automation-servers.environment-variables.dialog.title.unbind-many',
      messageKey: messageKey,
      level: 'WARNING',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map((confirmUnbind) => ({ confirmUnbind, environmentVariableIds })),
    );
  }
}
