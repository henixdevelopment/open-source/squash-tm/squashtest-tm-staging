import { ChangeDetectionStrategy, Component } from '@angular/core';
import { DialogReference } from 'sqtm-core';
import { ExecutionEnvironmentCountProjectInfoState } from '../../services/execution-environment-count.state';

@Component({
  selector: 'sqtm-app-environments-statuses-summary-dialog',
  templateUrl: './environments-statuses-summary-dialog.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EnvironmentsStatusesSummaryDialogComponent {
  projects: ExecutionEnvironmentCountProjectInfoState[];
  data: EnvironmentStatusesSummaryDialogConfiguration;

  constructor(
    private readonly dialogReference: DialogReference<EnvironmentStatusesSummaryDialogConfiguration>,
  ) {
    this.data = this.dialogReference.data;
    this.projects = this.data['projects'];
  }
}

export interface EnvironmentStatusesSummaryDialogConfiguration {
  id: string;
  projects: ExecutionEnvironmentCountProjectInfoState[];
}
