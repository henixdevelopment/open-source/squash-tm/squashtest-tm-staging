import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnDestroy,
  OnInit,
  signal,
  WritableSignal,
} from '@angular/core';
import {
  EnvironmentSelectionPanelDto,
  EnvironmentSelectionService,
} from '../../services/environment-selection.service';
import {
  asAutomatedExecutionEnvironmentRows,
  availableExecutionEnvironmentsProviders,
} from '../environment-selection-panel/test-automation-server-available-environments-table';
import { AutomatedExecutionEnvironment, GridService } from 'sqtm-core';
import { catchError, takeUntil } from 'rxjs/operators';
import { Observable, of, Subject } from 'rxjs';
import { ExecutionEnvironmentCountProjectInfoState } from '../../services/execution-environment-count.state';
import { ExecutionEnvironmentSummaryService } from '../../services/execution-environment-summary.service';

@Component({
  selector: 'sqtm-app-environments-statuses-summary-grid',
  templateUrl: './environments-statuses-summary-grid.component.html',
  styleUrl: './environments-statuses-summary-grid.component.less',
  providers: [
    availableExecutionEnvironmentsProviders,
    {
      provide: EnvironmentSelectionService,
      useClass: EnvironmentSelectionService,
    },
  ],

  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EnvironmentsStatusesSummaryGridComponent implements OnInit, OnDestroy {
  unsub$ = new Subject<void>();

  @Input()
  project: ExecutionEnvironmentCountProjectInfoState;

  $isLoading: WritableSignal<boolean> = signal(true);
  state: EnvironmentSelectionPanelDto;

  constructor(
    private readonly executionEnvironmentSummaryService: ExecutionEnvironmentSummaryService,
    private readonly gridService: GridService,
  ) {}

  ngOnInit(): void {
    this.executionEnvironmentSummaryService
      .getEnvironmentsByProjectId(this.project.projectId)
      .pipe(
        takeUntil(this.unsub$),
        catchError((err) => this.handleLoadingError(err)),
      )
      .subscribe((environmentSelectionPanel: EnvironmentSelectionPanelDto) => {
        this.state = environmentSelectionPanel;
        const automEnvs: AutomatedExecutionEnvironment[] = environmentSelectionPanel.errorMessage
          ? null
          : environmentSelectionPanel.environments.environments;

        if (automEnvs != null) {
          this.gridService.loadInitialData(
            asAutomatedExecutionEnvironmentRows(automEnvs),
            automEnvs.length,
          );
        }
        this.$isLoading.set(false);
      });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  getErrorMessage(): string {
    return this.state.errorMessage
      ? this.state.errorMessage
      : 'sqtm-core.administration-workspace.servers.test-automation-servers.environments.connection-error';
  }

  private handleLoadingError(error: Error): Observable<EnvironmentSelectionPanelDto> {
    this.$isLoading.set(false);
    return of({
      environments: null,
      server: null,
      project: null,
      errorMessage: error.message,
    });
  }
}
