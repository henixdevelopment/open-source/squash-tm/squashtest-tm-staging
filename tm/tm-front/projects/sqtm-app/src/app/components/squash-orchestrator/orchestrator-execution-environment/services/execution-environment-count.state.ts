export interface ExecutionEnvironmentCountState {
  statuses: { [key: string]: number };
  projects: ExecutionEnvironmentCountProjectInfoState[];
  nbOfSquashOrchestratorServers: number;
  nbOfUnreachableSquashOrchestrators: number;
  nbOfServersWithMissingToken: number;
}

export interface ExecutionEnvironmentCountProjectInfoState {
  projectId: number;
  projectName: string;
  squashOrchestratorId: number;
  squashOrchestratorName: string;
}
