import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  InjectionToken,
  Input,
  OnDestroy,
  OnInit,
  Output,
  signal,
  Signal,
  ViewContainerRef,
  WritableSignal,
} from '@angular/core';
import {
  DataRow,
  DataRowModel,
  DialogService,
  EntityViewService,
  FailureDetail,
  GridDefinition,
  GridId,
  GridService,
  gridServiceFactory,
  IssuesService,
  ReferentialDataService,
  RestService,
  SimplePermissions,
  smallGrid,
  SqtmEntityState,
} from 'sqtm-core';
import { FailureDetailService } from '../../services/failure-detail.service';
import { filter, map, switchMap, takeUntil } from 'rxjs/operators';
import {
  getRemoteIssueDialogConfiguration,
  RemoteIssueDialogData,
} from '../../../../remote-issue/containers/remote-issue-dialog/remote-issue-dialog.component';
import { Observable, Subject } from 'rxjs';
import { Dictionary } from 'lodash';
import { toSignal } from '@angular/core/rxjs-interop';

export const FAILURE_DETAIL_ISSUE_TABLE_DEF = new InjectionToken('FAILURE_DETAIL_ISSUE_TABLE_DEF');
export const FAILURE_DETAIL_ISSUE_TABLE = new InjectionToken('FAILURE_DETAIL_ISSUE_TABLE');

export function failureDetailIssueTableDefinition(): GridDefinition {
  return smallGrid(GridId.FAILURE_DETAIL_ISSUE).server().build();
}

@Component({
  selector: 'sqtm-app-failure-detail-component',
  templateUrl: './failure-detail.component.html',
  styleUrl: './failure-detail.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: FailureDetailService,
      useClass: FailureDetailService,
    },
    {
      provide: FAILURE_DETAIL_ISSUE_TABLE_DEF,
      useFactory: failureDetailIssueTableDefinition,
    },
    {
      provide: FAILURE_DETAIL_ISSUE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, FAILURE_DETAIL_ISSUE_TABLE_DEF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: FAILURE_DETAIL_ISSUE_TABLE,
    },
    IssuesService,
  ],
})
export class FailureDetailComponent<
    S extends SqtmEntityState,
    T extends string,
    P extends SimplePermissions,
  >
  implements OnInit, OnDestroy
{
  @Input()
  failureDetail: FailureDetail;

  @Input()
  failureDetailIndex: number;

  @Input()
  extenderId: number;

  @Output()
  refreshExecutionIssue = new EventEmitter<void>();

  $issueComponentData: Signal<DataRowModel[]>;
  $issuesLoaded: Signal<boolean> = signal(false);
  $projectState: Signal<BugtrackerState>;
  $isAuthenticatedToBugtracker: WritableSignal<boolean> = signal(null);

  private unsub$ = new Subject<void>();

  constructor(
    private readonly dialogService: DialogService,
    private readonly viewContainerRef: ViewContainerRef,
    private readonly failureDetailService: FailureDetailService,
    @Inject(FAILURE_DETAIL_ISSUE_TABLE) private readonly gridService: GridService,
    public readonly viewService: EntityViewService<S, T, P>,
    private issueService: IssuesService,
  ) {
    this.$projectState = toSignal(
      this.viewService.componentData$.pipe(
        map((componentData: any) => ({
          projectId: componentData.projectData.id,
          bugtrackerId: componentData.projectData.bugTracker?.id ?? null,
          canExecute: componentData.permissions.canExecute,
        })),
      ),
    );
    this.loadIssues();
  }

  ngOnInit(): void {
    this.issueService
      .loadModel(this.failureDetail.id, 'FAILURE_DETAIL_TYPE')
      .subscribe((issueModel) => {
        const isAuthenticatedToBugtracker =
          issueModel.modelLoaded && issueModel.bugTrackerStatus === 'AUTHENTICATED';
        this.$isAuthenticatedToBugtracker.set(isAuthenticatedToBugtracker);
        if (this.$isAuthenticatedToBugtracker()) {
          this.gridService.setServerUrl([
            `issues/failure-detail/${this.failureDetail.id}/known-issues`,
          ]);
        }
      });
  }

  ngOnDestroy(): void {
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  private loadIssues(): void {
    this.$issueComponentData = toSignal(
      this.gridService.dataRows$.pipe(
        map((dataRowsDictionary: Dictionary<DataRow>) =>
          Object.values(dataRowsDictionary).map((dataRow) => ({
            ...dataRow,
            data: dataRow.data,
          })),
        ),
      ),
    );
    this.$issuesLoaded = toSignal(this.gridService.loaded$.pipe(map((loaded) => !!loaded)));
  }

  canAddIssue(): boolean {
    return this.$projectState().bugtrackerId !== null && this.$projectState().canExecute;
  }

  createIssue() {
    this.addIssue(false);
  }

  attachIssue() {
    this.addIssue(true);
  }

  addIssue(attachMode: boolean) {
    if (this.$projectState().bugtrackerId == null) {
      return;
    }
    const dialogConf = getRemoteIssueDialogConfiguration(
      {
        bugTrackerId: this.$projectState().bugtrackerId,
        squashProjectId: this.$projectState().projectId,
        boundEntityId: this.failureDetail.id,
        bindableEntity: 'FAILURE_DETAIL_TYPE',
        attachMode: attachMode,
        extenderId: this.extenderId,
      },
      this.viewContainerRef,
    );

    const dialogRef = this.dialogService.openDialog<RemoteIssueDialogData, any>(dialogConf);

    dialogRef.dialogResultChanged$.subscribe(() => {
      this.gridService.refreshData();
      this.refreshExecutionIssue.emit();
    });
  }

  deleteIssue(issueIds: number[]) {
    this.showConfirmDeleteAssociatedIssueDialog()
      .pipe(
        takeUntil(this.unsub$),
        filter((confirmDelete) => confirmDelete),
        switchMap(() => {
          return this.failureDetailService.removeIssues(issueIds);
        }),
      )
      .subscribe(() => {
        this.gridService.refreshData();
        this.refreshExecutionIssue.emit();
      });
  }

  private showConfirmDeleteAssociatedIssueDialog(): Observable<boolean> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.campaign-workspace.execution-page.dialog.title.remove-issue.delete-one',
      messageKey:
        'sqtm-core.campaign-workspace.execution-page.dialog.message.remove-issue.delete-one-from-failure-detail',
      level: 'WARNING',
    });

    return dialogReference.dialogClosed$;
  }
}

interface BugtrackerState {
  projectId: number;
  bugtrackerId: number | null;
  canExecute: boolean;
}
