import { TestBed } from '@angular/core/testing';

import { EnvironmentSelectionService } from './environment-selection.service';
import { RestService } from 'sqtm-core';
import { mockRestService } from '../../../../utils/testing-utils/mocks.service';

describe('EnvironmentSelectionService', () => {
  let service: EnvironmentSelectionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: RestService, useValue: mockRestService() },
        { provide: EnvironmentSelectionService, useClass: EnvironmentSelectionService },
      ],
    });
    service = TestBed.inject(EnvironmentSelectionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
