import { TestBed } from '@angular/core/testing';

import { OrchestratorWorkflowsService } from './orchestrator-workflows.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('OrchestratorWorkflowsService', () => {
  let service: OrchestratorWorkflowsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(OrchestratorWorkflowsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
