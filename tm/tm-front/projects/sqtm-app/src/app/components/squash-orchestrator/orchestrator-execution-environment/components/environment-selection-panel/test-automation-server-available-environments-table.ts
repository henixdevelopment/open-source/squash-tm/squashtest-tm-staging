import { InjectionToken } from '@angular/core';
import {
  AutomatedExecutionEnvironment,
  Fixed,
  GridColumnId,
  GridDefinition,
  GridId,
  GridService,
  gridServiceFactory,
  indexColumn,
  Limited,
  LocalPersistenceService,
  PaginationConfigBuilder,
  ReferentialDataService,
  RestService,
  smallGrid,
  StyleDefinitionBuilder,
  textColumn,
} from 'sqtm-core';

const AVAILABLE_ENV_TABLE_DEF = new InjectionToken<GridDefinition>(
  'Grid config for the available execution environments table',
);
const AVAILABLE_ENV_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the available execution environments table',
);

function availableExecutionEnvironmentsTable(
  localPersistenceService: LocalPersistenceService,
): GridDefinition {
  return smallGrid(GridId.AVAILABLE_EXECUTION_ENVIRONMENTS)
    .withColumns([
      indexColumn().changeWidthCalculationStrategy(new Fixed(70)).withViewport('leftViewport'),
      textColumn(GridColumnId.name)
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Limited(150))
        .disableSort(),
      textColumn(GridColumnId.joinedTags)
        .withI18nKey('sqtm-core.entity.server.environment.tags.label')
        .changeWidthCalculationStrategy(new Limited(500))
        .disableSort(),
      textColumn(GridColumnId.status)
        .withI18nKey('sqtm-core.entity.generic.status.label')
        .changeWidthCalculationStrategy(new Limited(150))
        .disableSort(),
    ])
    .disableRightToolBar()
    .withStyle(new StyleDefinitionBuilder().switchOffSelectedRows().switchOffHoveredRows())
    .withPagination(new PaginationConfigBuilder().initialSize(10))
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
}

export const availableExecutionEnvironmentsProviders = [
  {
    provide: AVAILABLE_ENV_TABLE_DEF,
    useFactory: availableExecutionEnvironmentsTable,
    deps: [LocalPersistenceService],
  },
  {
    provide: AVAILABLE_ENV_TABLE,
    useFactory: gridServiceFactory,
    deps: [RestService, AVAILABLE_ENV_TABLE_DEF, ReferentialDataService],
  },
  {
    provide: GridService,
    useExisting: AVAILABLE_ENV_TABLE,
  },
];

export interface AutomatedExecutionEnvironmentRow extends AutomatedExecutionEnvironment {
  id: string;
  joinedTags: string;
}

export function asAutomatedExecutionEnvironmentRows(
  environments: AutomatedExecutionEnvironment[],
): AutomatedExecutionEnvironmentRow[] {
  if (environments == null) {
    return [];
  }

  return environments.map((env, idx) => asAutomatedExecutionEnvironmentRow(env, idx));
}

function asAutomatedExecutionEnvironmentRow(
  environment: AutomatedExecutionEnvironment,
  rowIndex: number,
): AutomatedExecutionEnvironmentRow {
  const sortedTags = [...environment.tags].sort((a, b) => a.localeCompare(b));

  return {
    ...environment,
    id: environment.name + rowIndex, // environment names are not unique but row ID must
    joinedTags: sortedTags.join(', '),
  };
}
