import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { RestService } from 'sqtm-core';
import { ExecutionEnvironmentCountState } from './execution-environment-count.state';
import { EnvironmentSelectionPanelDto } from './environment-selection.service';

@Injectable({
  providedIn: 'root',
})
export class ExecutionEnvironmentSummaryService {
  private readonly UNRESTRICTED_URL_PREFIX = 'test-automation';

  private _refreshView: Subject<void> = new Subject<void>();
  refreshView$: Observable<void>;

  constructor(public readonly restService: RestService) {
    this.refreshView$ = this._refreshView.asObservable();
  }

  refreshView() {
    this._refreshView.next();
  }

  getEnvironmentsStatusesCount(
    entityType: string,
    entityId: number,
  ): Observable<ExecutionEnvironmentCountState> {
    const urlParts = [
      this.UNRESTRICTED_URL_PREFIX,
      entityType,
      entityId.toString(),
      'automated-execution-environments-statuses-count',
    ];
    return this.restService.get<ExecutionEnvironmentCountState>(urlParts);
  }

  getEnvironmentsByProjectId(projectId: number): Observable<EnvironmentSelectionPanelDto> {
    const urlParts = [
      this.UNRESTRICTED_URL_PREFIX,
      projectId.toString(),
      'automated-execution-environments/all',
    ];
    return this.restService.getWithoutErrorHandling<EnvironmentSelectionPanelDto>(urlParts);
  }
}
