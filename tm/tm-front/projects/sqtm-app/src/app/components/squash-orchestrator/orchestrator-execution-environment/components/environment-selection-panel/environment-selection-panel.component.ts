import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  ViewChild,
} from '@angular/core';
import * as _ from 'lodash';
import { combineLatest, Observable, of } from 'rxjs';
import { filter, map, take, tap, withLatestFrom } from 'rxjs/operators';
import {
  AutomatedExecutionEnvironment,
  AutomationEnvironmentTagHolder,
  EditableSelectFieldComponent,
  GridService,
} from 'sqtm-core';
import { EnvironmentSelectionService } from '../../services/environment-selection.service';
import { EnvironmentSelectionPanelState } from '../../services/environment-selection.state';
import {
  asAutomatedExecutionEnvironmentRows,
  availableExecutionEnvironmentsProviders,
} from './test-automation-server-available-environments-table';

@Component({
  selector: 'sqtm-app-environment-selection-panel',
  templateUrl: './environment-selection-panel.component.html',
  styleUrls: ['./environment-selection-panel.component.less'],
  providers: [
    availableExecutionEnvironmentsProviders,
    {
      provide: EnvironmentSelectionService,
      useClass: EnvironmentSelectionService,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EnvironmentSelectionPanelComponent implements AfterViewInit, OnDestroy {
  // For use in template
  readonly Layout = Layout;
  readonly I18nKeys = I18nKeys;

  private readonly DUMMY_TOKEN_VALUE = 'dummy';

  state$: Observable<EnvironmentSelectionPanelState>;
  currentLayout$: Observable<Layout> = of(Layout.LOADING);
  noMatchingEnvironments$: Observable<boolean> = of(true);
  isResetTagsButtonVisible$: Observable<boolean> = of(false);
  isResetTokenButtonVisible$: Observable<boolean> = of(false);
  readonly missingCredentialsMessage$: Observable<string>;
  readonly isTokenFieldVisible$: Observable<boolean>;
  readonly tokenTooltip$: Observable<string>;
  readonly isTagInputDisabled$: Observable<boolean>;

  @Input()
  isAvailableEnvironmentsPanelExpanded = true;

  @Input()
  tagHolderId: number;

  @Input()
  tagHolderType: AutomationEnvironmentTagHolder;

  @Input()
  environmentTagsLabel: string;

  @Input()
  selectInputTooltip?: string;

  @Input()
  taServerId: number;

  @Output()
  selectedTagChanged = new EventEmitter<string[]>();

  @Output()
  projectTokenChanged = new EventEmitter<void>();

  private _tokenField: EditableSelectFieldComponent;

  @ViewChild('tokenField', { static: false })
  set tokenField(field: EditableSelectFieldComponent) {
    this._tokenField = field;

    if (field != null) {
      this.initializeTokenFieldDummyValue();
    }
  }

  private initializeTokenFieldDummyValue(): void {
    this.panelService.state$
      .pipe(
        filter((state) => state.tagHolderId != null),
        take(1),
        map((state) => (state.hasProjectToken ? this.DUMMY_TOKEN_VALUE : null)),
        tap((value) => (this._tokenField.value = value)),
      )
      .subscribe();
  }

  get isTokenBeingEdited(): boolean {
    return this._tokenField?.edit;
  }

  constructor(
    public readonly gridService: GridService,
    public readonly cdRef: ChangeDetectorRef,
    public readonly panelService: EnvironmentSelectionService,
  ) {
    this.state$ = panelService.state$;

    const isAtProjectLevel$ = this.state$.pipe(
      map((state) => state.tagHolderType === AutomationEnvironmentTagHolder.PROJECT),
    );

    const isAtProjectOrExecutionDialogLevel$ = this.state$.pipe(
      map(
        (state) =>
          state.tagHolderType === AutomationEnvironmentTagHolder.PROJECT ||
          state.tagHolderType === AutomationEnvironmentTagHolder.EXECUTION_DIALOG,
      ),
    );

    this.missingCredentialsMessage$ = isAtProjectOrExecutionDialogLevel$.pipe(
      map((isAtProjectOrExecutionDialogLevel) =>
        isAtProjectOrExecutionDialogLevel
          ? I18nKeys.NO_PROJECT_CREDENTIALS
          : I18nKeys.NO_SERVER_CREDENTIALS,
      ),
    );

    this.isTokenFieldVisible$ = isAtProjectLevel$;

    this.tokenTooltip$ = isAtProjectLevel$.pipe(
      map((isAtProjectLevel) => (isAtProjectLevel ? I18nKeys.PROJECT_TOKEN_TOOLTIP : '')),
    );

    this.currentLayout$ = this.state$.pipe(
      map(EnvironmentSelectionPanelComponent.getCurrentLayout),
    );

    const gridDataSource$ = this.panelService.filteredAvailableEnvironments$.pipe(
      map(asAutomatedExecutionEnvironmentRows),
    );

    this.gridService.connectToDatasource(gridDataSource$);

    this.noMatchingEnvironments$ = this.panelService.filteredAvailableEnvironments$.pipe(
      map((envs) => envs.length === 0),
    );

    this.isTagInputDisabled$ = this.currentLayout$.pipe(
      map((currentLayout) => currentLayout !== Layout.READY),
    );

    this.handleProjectTokenChanged();
  }

  ngAfterViewInit(): void {
    this.panelService
      .load(this.tagHolderId, this.tagHolderType)
      .pipe(withLatestFrom(this.state$))
      .subscribe(([, state]) => this.selectedTagChanged.emit(state.selectedTags));

    if (this.tagHolderType === AutomationEnvironmentTagHolder.PROJECT) {
      this.isResetTagsButtonVisible$ = this.panelService.state$.pipe(
        map((state) => !state.areProjectTagsInherited && !state.hasEnvironmentLoadingError),
      );

      this.isResetTokenButtonVisible$ = this.panelService.state$.pipe(
        map((state) => state.hasProjectToken),
      );

      // On the view of a project the token can be modified so we must ensure that the testAutomationId is not null
      this.panelService.preventNullServerId(this.taServerId);
    } else if (this.tagHolderType === AutomationEnvironmentTagHolder.EXECUTION_DIALOG) {
      this.isResetTagsButtonVisible$ = this.panelService.state$.pipe(
        map((state) => {
          const defaultTags = state.areProjectTagsInherited ? state.serverTags : state.projectTags;
          return (
            defaultTags != null &&
            (state.selectedTags.length !== defaultTags.length ||
              !state.selectedTags.every((tag) => defaultTags.includes(tag)))
          );
        }),
      );
    }
  }

  ngOnDestroy() {
    this.panelService.complete();
  }

  updateEnvironmentTags(tags: string[]) {
    if (this.tagHolderType === AutomationEnvironmentTagHolder.EXECUTION_DIALOG) {
      this.panelService.updateEnvironmentTagsOnlyInState(tags);
    } else {
      this.panelService.updateEnvironmentTags(tags);
    }
    this.selectedTagChanged.emit(tags);
  }

  toggleAvailableEnvironmentsPanelVisibility(): void {
    this.isAvailableEnvironmentsPanelExpanded = !this.isAvailableEnvironmentsPanelExpanded;
  }

  handleServerCredentialsChange(isRegistered: boolean): void {
    this.panelService.handleServerCredentialsChange(isRegistered);
  }

  refreshExecutionEnvironments(): void {
    this.panelService.reloadAvailableEnvironments();
  }

  handleBoundServerChanged(taServerId: number): void {
    this.panelService.handleBoundServerChanged(taServerId);
  }

  private static getCurrentLayout(state: EnvironmentSelectionPanelState): Layout {
    if (state == null || state.isLoadingEnvironments) {
      return Layout.LOADING;
    } else if (state.hasMissingCredentials) {
      return Layout.NO_CREDENTIALS;
    } else if (state.hasEnvironmentLoadingError) {
      return Layout.ERROR;
    } else {
      return Layout.READY;
    }
  }

  handleResetButtonClicked($event: MouseEvent): void {
    $event.stopPropagation();
    if (this.tagHolderType === AutomationEnvironmentTagHolder.EXECUTION_DIALOG) {
      this.panelService.clearTagOverridesOnlyInState();
    } else {
      this.panelService.clearTagOverrides();
    }
  }

  handleTokenConfirm($event: string): void {
    this.panelService.updateProjectTokenOverride($event);
    this._tokenField.value = this.DUMMY_TOKEN_VALUE;
  }

  handleClearTokenButtonClicked($event: MouseEvent): void {
    $event.stopPropagation();
    this.panelService.clearProjectTokenOverride();
    this._tokenField.value = null;
  }

  isTokenModificationWarningVisible(state: EnvironmentSelectionPanelState): boolean {
    return this.isTokenBeingEdited && !state.areProjectTagsInherited;
  }

  handleTagsFieldFocus(): void {
    this.panelService.reloadAvailableTags();
  }

  isOneEnvironmentAvailable(): Observable<boolean> {
    return combineLatest([this.currentLayout$, this.noMatchingEnvironments$]).pipe(
      map(
        ([currentLayout, noMatchinEnvironments]) =>
          currentLayout === Layout.READY && !noMatchinEnvironments,
      ),
    );
  }

  getNamespacesOfSelectedEnvironments(): Observable<string[]> {
    return this.panelService.filteredAvailableEnvironments$.pipe(
      map((environments: AutomatedExecutionEnvironment[]) =>
        _.uniq(environments.flatMap((environment) => environment.namespaces)),
      ),
    );
  }

  getErrorMessage(): Observable<string> {
    return this.state$.pipe(
      take(1),
      map((state) => (state.errorMessage ? state.errorMessage : I18nKeys.ERROR)),
    );
  }

  isProjectMissingCredentialsMessage(): Observable<boolean> {
    return this.missingCredentialsMessage$.pipe(
      map((message) => message === I18nKeys.NO_PROJECT_CREDENTIALS),
    );
  }

  handleProjectTokenChanged(): void {
    this.panelService.projectTokenChanged$.subscribe(() => this.projectTokenChanged.emit());
  }
}

enum Layout {
  LOADING,
  READY,
  ERROR,
  NO_CREDENTIALS,
}

enum I18nKeys {
  NO_SERVER_CREDENTIALS = 'sqtm-core.administration-workspace.servers.test-automation-servers.environments.no-credentials-message',
  NO_PROJECT_CREDENTIALS = 'sqtm-core.administration-workspace.views.project.automation.environments.no-credentials-message',
  ERROR = 'sqtm-core.administration-workspace.servers.test-automation-servers.environments.connection-error',
  AVAILABLE_ENVIRONMENTS = 'sqtm-core.entity.server.environment.available-environments.label',
  NO_MATCHING_ENVIRONMENT = 'sqtm-core.administration-workspace.servers.test-automation-servers.environments.no-matching-environment',
  NO_MATCHING_ENVIRONMENT_IN_EXECUTION_DIALOG = 'sqtm-core.campaign-workspace.dialog.message.automated-tests-execution-supervision.no-matching-environment',
  PROJECT_TOKEN_TOOLTIP = 'sqtm-core.administration-workspace.views.project.automation.environments.token.tooltip',
  CHANGE_TOKEN_WARNING = 'sqtm-core.administration-workspace.views.project.automation.environments.token-modification-warning',
}
