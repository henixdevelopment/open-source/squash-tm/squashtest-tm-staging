import { AutomatedExecutionEnvironment, AutomationEnvironmentTagHolder } from 'sqtm-core';

export interface EnvironmentSelectionPanelState {
  // Common state
  tagHolderId: number;
  tagHolderType: AutomationEnvironmentTagHolder;
  testAutomationServerId: number;
  serverTags: string[];
  availableEnvironments: AutomatedExecutionEnvironment[];
  availableEnvironmentTags: string[];
  selectedTags: string[];
  hasMissingCredentials: boolean;

  // Project specific attributes
  projectId?: number;
  projectTags?: string[];
  areProjectTagsInherited: boolean;
  hasProjectToken: boolean;

  // UI attributes
  isLoadingEnvironments: boolean;
  hasEnvironmentLoadingError: boolean;

  errorMessage: string;
}

export function getInitialEnvironmentSelectionState(): EnvironmentSelectionPanelState {
  return {
    tagHolderId: null,
    tagHolderType: null,
    testAutomationServerId: null,
    serverTags: [],
    projectId: null,
    projectTags: null,
    areProjectTagsInherited: false,
    hasProjectToken: false,

    availableEnvironmentTags: [],
    availableEnvironments: [],
    hasEnvironmentLoadingError: false,
    hasMissingCredentials: false,
    selectedTags: [],
    isLoadingEnvironments: true,
    errorMessage: null,
  };
}
