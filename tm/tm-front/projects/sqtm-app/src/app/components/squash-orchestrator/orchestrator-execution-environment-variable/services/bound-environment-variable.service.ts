import { Injectable } from '@angular/core';
import {
  AutomationEnvironmentTagHolder,
  BoundEnvironmentVariable,
  createStore,
  EnvironmentVariable,
  Identifier,
  RestService,
} from 'sqtm-core';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap, take, tap, withLatestFrom } from 'rxjs/operators';

@Injectable()
export class BoundEnvironmentVariableService {
  constructor(public readonly restService: RestService) {}

  private readonly store = createStore<BoundEnvironmentVariableState>(
    getInitialAutomatedExecutionEnvironmentVariableState(),
  );

  public state$: Observable<BoundEnvironmentVariableState> = this.store.state$;

  public load(id: number, entityType: AutomationEnvironmentTagHolder): Observable<any> {
    return this.initializeAutomatedExecutionEnvironmentVariables(id, entityType);
  }

  loadForAutomatedExecution(iterationId: number, projectId: number) {
    return this.initializeEnvironmentVariablesForAutomatedExecution(iterationId, projectId);
  }

  protected getRootUrl(): string {
    return 'bound-environment-variables';
  }

  getAllEnvironmentVariables(): Observable<EnvironmentVariable[]> {
    const urlParts = ['environment-variables'];
    return this.restService.get<EnvironmentVariableResponse>(urlParts).pipe(
      take(1),
      map((response: EnvironmentVariableResponse) => response.environmentVariables),
    );
  }

  bindEnvironmentVariables(selectedItemIds: Identifier[]) {
    return this.store.state$.pipe(
      take(1),
      switchMap((state: BoundEnvironmentVariableState) =>
        this.bindEnvironmentVariablesServerSide(state, selectedItemIds),
      ),
      withLatestFrom(this.store.state$),
      map(([result, state]) => this.updateStateWithNewBindings(result, state)),
      map((nextState) => {
        this.store.commit(nextState);
      }),
    );
  }

  private bindEnvironmentVariablesServerSide(
    state: BoundEnvironmentVariableState,
    selectedItemIds: Identifier[],
  ): Observable<BoundEnvironmentVariable[]> {
    const urlPart = [this.getRootUrl(), 'bind'];
    const requestBody = {
      entityType: state.entityType,
      entityId: state.entityId,
      environmentVariableIds: selectedItemIds,
    };

    return this.restService
      .post<{ boundEnvironmentVariables: BoundEnvironmentVariable[] }>(urlPart, requestBody)
      .pipe(
        map(
          (response: { boundEnvironmentVariables: BoundEnvironmentVariable[] }) =>
            response?.boundEnvironmentVariables,
        ),
      );
  }

  private updateStateWithNewBindings(
    result: BoundEnvironmentVariable[],
    state: BoundEnvironmentVariableState,
  ) {
    return {
      ...state,
      boundEnvironmentVariables: result,
    };
  }

  unbindEnvironmentVariables(
    environmentVariableIds: any,
  ): Observable<BoundEnvironmentVariableState> {
    return this.state$.pipe(
      take(1),
      switchMap((state: BoundEnvironmentVariableState) =>
        this.unbindEnvironmentVariablesServerSide(state, environmentVariableIds),
      ),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, BoundEnvironmentVariableState]) =>
        this.updateStateWithUnbindEnvironmentVariables(state, environmentVariableIds),
      ),
      map((nextState) => this.store.commit(nextState)),
    );
  }

  private unbindEnvironmentVariablesServerSide(
    state: BoundEnvironmentVariableState,
    environmentVariableIds: number[],
  ) {
    const urlParts = [this.getRootUrl(), 'unbind'];
    const requestBody = {
      entityType: state.entityType,
      entityId: state.entityId,
      environmentVariableIds,
    };
    return this.restService.post(urlParts, requestBody);
  }

  private updateStateWithUnbindEnvironmentVariables(
    state: BoundEnvironmentVariableState,
    environmentVariableIds: number[],
  ) {
    const updatedEnvironmentVariableBinding = [...state.boundEnvironmentVariables].filter(
      (boundEnvironmentVariable: BoundEnvironmentVariable) =>
        !environmentVariableIds.includes(boundEnvironmentVariable.id),
    );
    return {
      ...state,
      boundEnvironmentVariables: updatedEnvironmentVariableBinding,
    };
  }

  setEnvironmentVariableValue(bindingId: number, value: string, environmentVariableId: number) {
    return this.store.state$.pipe(
      take(1),
      switchMap((state: BoundEnvironmentVariableState) =>
        this.checkEntityTypeToUpdateValueServerSide(state, bindingId, value),
      ),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, BoundEnvironmentVariableState]) =>
        this.updateStateWithNewBoundEVValue(state, value, environmentVariableId),
      ),
      tap(([state]: [BoundEnvironmentVariableState, any]) => this.store.commit(state)),
    );
  }

  private checkEntityTypeToUpdateValueServerSide(
    state: BoundEnvironmentVariableState,
    bindingId: number,
    value: string,
  ) {
    if (state.entityType !== AutomationEnvironmentTagHolder.EXECUTION_DIALOG) {
      return this.setEnvironmentVariableValueServerSide(bindingId, value);
    } else {
      return of(null);
    }
  }

  private setEnvironmentVariableValueServerSide(
    bindingId: number,
    value: string,
  ): Observable<void> {
    const urlPart = [this.getRootUrl(), 'value'];
    return this.restService.post<any>(urlPart, { value: value, bindingId: bindingId });
  }

  private updateStateWithNewBoundEVValue(
    state: BoundEnvironmentVariableState,
    value: string,
    environmentVariableId: number,
  ): any {
    const boundEnvironmentVariables: BoundEnvironmentVariable[] = state.boundEnvironmentVariables;

    const boundEnvironmentVariable: BoundEnvironmentVariable = boundEnvironmentVariables.find(
      (environmentVariable) => environmentVariable.id === environmentVariableId,
    );

    const index = boundEnvironmentVariables.indexOf(boundEnvironmentVariable);

    boundEnvironmentVariable.value = value;
    boundEnvironmentVariables[index] = boundEnvironmentVariable;

    const responseState: BoundEnvironmentVariableState = {
      ...state,
      boundEnvironmentVariables: boundEnvironmentVariables,
    };

    return [responseState, value];
  }

  public handleBoundServerChanged(
    entityId: number,
    entityType: AutomationEnvironmentTagHolder,
  ): void {
    this.reloadBoundEnvironmentVariables(entityId, entityType);
  }

  resetEnvironmentVariableDefaultValue(
    bindingId: number,
    boundEnvironmentVariableId: number,
  ): Observable<string> {
    return this.store.state$.pipe(
      take(1),
      switchMap((state: BoundEnvironmentVariableState) =>
        this.resetEvDefaultValueServerSide(bindingId, state.entityType),
      ),
      withLatestFrom(this.state$),
      map(([value, state]: [any, BoundEnvironmentVariableState]) =>
        this.updateStateWithNewBoundEVValue(state, value.defaultValue, boundEnvironmentVariableId),
      ),
      map(([state, value]: [BoundEnvironmentVariableState, string]) => {
        this.store.commit(state);
        return value;
      }),
    );
  }

  private resetEvDefaultValueServerSide(
    bindingId: number,
    entityType: AutomationEnvironmentTagHolder,
  ): Observable<any> {
    const urlPart = [this.getRootUrl(), 'reset'];
    return this.restService.post<any>(urlPart, { bindingId, entityType });
  }

  private initializeAutomatedExecutionEnvironmentVariables(
    id: number,
    entityType: AutomationEnvironmentTagHolder,
  ) {
    return this.getEnvironmentVariablesByEntityViewServerSide(id, entityType).pipe(
      withLatestFrom(this.store.state$),
      map(([response, state]: [BoundEnvironmentVariable[], BoundEnvironmentVariableState]) =>
        this.mergeResponseIntoState(response, state, id, entityType),
      ),
      tap((state: BoundEnvironmentVariableState) => this.store.commit(state)),
      catchError(() => this.handleLoadError()),
    );
  }

  private initializeEnvironmentVariablesForAutomatedExecution(
    iterationId: number,
    projectId: number,
  ) {
    return this.getEnvironmentVariablesForAutomatedExecution(iterationId, projectId).pipe(
      withLatestFrom(this.store.state$),
      map(([response, state]: [BoundEnvironmentVariable[], BoundEnvironmentVariableState]) =>
        this.mergeResponseIntoState(
          response,
          state,
          projectId,
          AutomationEnvironmentTagHolder.EXECUTION_DIALOG,
        ),
      ),
      tap((state: BoundEnvironmentVariableState) => this.store.commit(state)),
      catchError(() => this.handleLoadError()),
    );
  }

  private getEnvironmentVariablesByEntityViewServerSide(
    entityId: number,
    entityViewType: AutomationEnvironmentTagHolder,
  ): Observable<BoundEnvironmentVariable[]> {
    const type: string =
      entityViewType === AutomationEnvironmentTagHolder.TEST_AUTOMATION_SERVER
        ? 'test-automation-server'
        : 'project';

    const urlParts = [this.getRootUrl(), type, entityId.toString()];

    return this.restService
      .get<{ boundEnvironmentVariables: BoundEnvironmentVariable[] }>(urlParts)
      .pipe(
        map(
          (response: { boundEnvironmentVariables: BoundEnvironmentVariable[] }) =>
            response?.boundEnvironmentVariables,
        ),
      );
  }

  private getEnvironmentVariablesForAutomatedExecution(
    iterationId: number,
    projectId: number,
  ): Observable<BoundEnvironmentVariable[]> {
    const urlParts = [
      this.getRootUrl(),
      'project',
      projectId.toString(),
      'iteration',
      iterationId.toString(),
    ];

    return this.restService
      .get<{ boundEnvironmentVariables: BoundEnvironmentVariable[] }>(urlParts)
      .pipe(
        map(
          (response: { boundEnvironmentVariables: BoundEnvironmentVariable[] }) =>
            response?.boundEnvironmentVariables,
        ),
      );
  }

  private mergeResponseIntoState(
    response: BoundEnvironmentVariable[],
    state: BoundEnvironmentVariableState,
    entityId: number,
    entityType: AutomationEnvironmentTagHolder,
  ) {
    return {
      ...state,
      entityId: entityId,
      entityType: entityType,
      boundEnvironmentVariables: response,
    };
  }

  private handleLoadError() {
    return this.store.state$.pipe(
      take(1),
      tap((state: any) =>
        this.store.commit({
          ...state,
          boundEnvironmentVariables: [],
        }),
      ),
    );
  }

  private reloadBoundEnvironmentVariables(
    entityId: number,
    entityType: AutomationEnvironmentTagHolder,
  ) {
    this.load(entityId, entityType).subscribe();
  }
}

interface EnvironmentVariableResponse {
  environmentVariables: EnvironmentVariable[];
}

export interface BoundEnvironmentVariableState {
  entityId: number;
  entityType: AutomationEnvironmentTagHolder;
  boundEnvironmentVariables: BoundEnvironmentVariable[];
}
export function getInitialAutomatedExecutionEnvironmentVariableState(): BoundEnvironmentVariableState {
  return {
    entityId: undefined,
    entityType: undefined,
    boundEnvironmentVariables: [],
  };
}
