import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { DialogModule, GridModule, WorkspaceCommonModule, IssuesModule } from 'sqtm-core';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzFormModule } from 'ng-zorro-antd/form';
import { FormsModule } from '@angular/forms';
import { RouterLink } from '@angular/router';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { OrchestratorExecutionFailureDetailsComponent } from './orchestrator-execution-failure-details/components/failure-details-container/orchestrator-execution-failure-details.component';
import { DeleteEvBindingCellComponent } from './orchestrator-execution-environment-variable/components/cell-renderers/delete-ev-binding-cell/delete-ev-binding-cell.component';
import { EvFieldValueCellComponent } from './orchestrator-execution-environment-variable/components/cell-renderers/ev-field-value-cell/ev-field-value-cell.component';
import { EnvironmentVariableBindingDialogComponent } from './orchestrator-execution-environment-variable/components/dialog/environment-variable-binding-dialog/environment-variable-binding-dialog.component';
import { ReinitializeValueCellComponent } from './orchestrator-execution-environment-variable/components/cell-renderers/reinitialize-value-cell/reinitialize-value-cell.component';
import { EvLinkCellComponent } from './orchestrator-execution-environment-variable/components/cell-renderers/ev-link-cell/ev-link-cell.component';
import { EnvironmentSelectionPanelComponent } from './orchestrator-execution-environment/components/environment-selection-panel/environment-selection-panel.component';
import { EnvironmentsStatusesCountComponent } from './orchestrator-execution-environment/components/environments-statuses-count/environments-statuses-count.component';
import { EnvironmentsStatusesSummaryDialogComponent } from './orchestrator-execution-environment/components/environments-statuses-summary-dialog/environments-statuses-summary-dialog.component';
import { EnvironmentsStatusesSummaryGridComponent } from './orchestrator-execution-environment/components/environments-statuses-summary-grid/environments-statuses-summary-grid.component';
import { OrchestratorAdditionalConfigurationPanelComponent } from './orchestrator-additional-configuration-panel/orchestrator-additional-configuration-panel.component';
import { OrchestratorExecutionEnvironmentVariablePanelComponent } from './orchestrator-execution-environment-variable/components/orchestrator-execution-environment-variable-panel/orchestrator-execution-environment-variable-panel.component';
import { BindEnvironmentVariableButtonsComponent } from './orchestrator-execution-environment-variable/components/bind-environment-variable-buttons/bind-environment-variable-buttons.component';
import { FailureDetailComponent } from './orchestrator-execution-failure-details/components/failure-detail/failure-detail.component';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { RemoteIssueModule } from '../remote-issue/remote-issue.module';

@NgModule({
  declarations: [
    OrchestratorExecutionFailureDetailsComponent,
    OrchestratorExecutionEnvironmentVariablePanelComponent,
    DeleteEvBindingCellComponent,
    EvFieldValueCellComponent,
    BindEnvironmentVariableButtonsComponent,
    EnvironmentVariableBindingDialogComponent,
    ReinitializeValueCellComponent,
    EvLinkCellComponent,
    EnvironmentSelectionPanelComponent,
    EnvironmentsStatusesCountComponent,
    EnvironmentsStatusesSummaryDialogComponent,
    EnvironmentsStatusesSummaryGridComponent,
    OrchestratorAdditionalConfigurationPanelComponent,
    FailureDetailComponent,
  ],
  exports: [
    OrchestratorExecutionFailureDetailsComponent,
    OrchestratorExecutionEnvironmentVariablePanelComponent,
    DeleteEvBindingCellComponent,
    EvFieldValueCellComponent,
    BindEnvironmentVariableButtonsComponent,
    ReinitializeValueCellComponent,
    EnvironmentSelectionPanelComponent,
    EnvironmentsStatusesCountComponent,
    OrchestratorAdditionalConfigurationPanelComponent,
    FailureDetailComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    TranslateModule.forChild(),
    NzCollapseModule,
    NzToolTipModule,
    NzIconModule,
    GridModule,
    DialogModule,
    NzCheckboxModule,
    NzButtonModule,
    NzFormModule,
    FormsModule,
    WorkspaceCommonModule,
    RouterLink,
    NzBadgeModule,
    IssuesModule,
    NzMenuModule,
    NzDropDownModule,
    RemoteIssueModule,
  ],
})
export class SquashOrchestratorModule {}
