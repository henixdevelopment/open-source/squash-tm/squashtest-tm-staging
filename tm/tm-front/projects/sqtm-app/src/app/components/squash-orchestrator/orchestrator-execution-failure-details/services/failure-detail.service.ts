import { ActionErrorDisplayService, FailureDetail, RestService } from 'sqtm-core';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';

@Injectable()
export class FailureDetailService {
  constructor(
    protected restService: RestService,
    private actionErrorDisplayService: ActionErrorDisplayService,
  ) {}

  getExecutionExtenderFailureDetailList(extenderId: number): Observable<FailureDetail[]> {
    return this.restService.get<FailureDetail[]>([
      'execution-extender',
      extenderId.toString(),
      'failure-detail-list',
    ]);
  }

  removeIssues(issueIds: number[]): Observable<any> {
    return this.removeIssuesServerSide(issueIds).pipe(
      catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
    );
  }

  private removeIssuesServerSide(issueIds: number[]) {
    const issuesIds = issueIds.join(',');
    return this.restService.delete(['issues', issuesIds]);
  }
}
