import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  DialogService,
  GridColumnId,
  GridService,
} from 'sqtm-core';
import { OrchestratorWorkflowsService } from '../../services/orchestrator-workflows.service';
import { filter, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-orchestrator-workflow-shutdown',
  template: `
    @if (row) {
      <sqtm-core-toggle-icon
        [attr.data-test-button-id]="'stop-workflow-' + getWorkflowId()"
        [active]="isRunning()"
        (click)="stopWorkflow()"
        class="full-height full-width icon-container current-workspace-main-color"
      >
        <i
          class="table-icon-size"
          [style.color]="isRunning() ? 'red' : ''"
          nz-icon
          nzType="close-circle"
          nzTheme="outline"
          [nzTooltipTitle]="'sqtm-core.orchestrator.tooltip.stop-workflow' | translate"
          nz-tooltip
        >
        </i>
      </sqtm-core-toggle-icon>
    }
  `,
  styleUrl: './orchestrator-workflow-shutdown.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrchestratorWorkflowShutdownComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    cdRef: ChangeDetectorRef,
    private dialogService: DialogService,
    private orchestratorService: OrchestratorWorkflowsService,
  ) {
    super(grid, cdRef);
  }

  isRunning() {
    return this.row.data[GridColumnId.status] === 'RUNNING';
  }

  stopWorkflow() {
    if (this.isRunning()) {
      const dialogRef = this.dialogService.openConfirm({
        id: 'confirm-stop-orchestrator-workflow',
        titleKey: 'sqtm-core.orchestrator.dialog.title.stop-workflow',
        messageKey: 'sqtm-core.orchestrator.dialog.message.stop-workflow',
        level: 'DANGER',
      });

      dialogRef.dialogClosed$
        .pipe(
          filter((confirm) => Boolean(confirm)),
          tap(() => this.grid.beginAsyncOperation()),
          switchMap(() => this.orchestratorService.stopWorkflow(this.getWorkflowId())),
        )
        .subscribe((response) => {
          if (response.reachable) {
            this.orchestratorService.reloadWorkflows();
          } else {
            this.openErrorDialog();
          }
          this.grid.completeAsyncOperation();
        });
    }
  }

  private openErrorDialog() {
    this.dialogService.openAlert({
      id: 'stop-orchestrator-workflow-error',
      titleKey: 'sqtm-core.generic.label.error',
      level: 'DANGER',
      messageKey: 'sqtm-core.orchestrator.error.stop-workflow',
    });
  }

  getWorkflowId(): string {
    return this.row.data[GridColumnId.id];
  }
}

export function workflowShutdownColumn(): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder('shutdown')
    .withRenderer(OrchestratorWorkflowShutdownComponent)
    .disableHeader();
}
