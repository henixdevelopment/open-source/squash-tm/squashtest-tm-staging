import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import {
  AbstractListCellRendererComponent,
  ActionErrorDisplayService,
  AlertDialogComponent,
  AutomationEnvironmentTagHolder,
  buildErrorI18nKeyProvider,
  ColumnDefinitionBuilder,
  DialogService,
  EditableTextFieldComponent,
  EnvironmentVariableOption,
  ErrorI18nKeyProvider,
  EvInputType,
  getEvInputTypeI18nCustomPatternErrorMessage,
  getTextFieldInputTypes,
  GridColumnId,
  GridService,
  ListPanelItem,
  RestService,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { Overlay } from '@angular/cdk/overlay';
import { ActivatedRoute } from '@angular/router';
import { catchError, finalize, take, tap } from 'rxjs/operators';
import { Observable, Subject, timer } from 'rxjs';
import { ValidationErrors, Validators } from '@angular/forms';
import {
  BoundEnvironmentVariableService,
  BoundEnvironmentVariableState,
} from '../../../services/bound-environment-variable.service';

@Component({
  selector: 'sqtm-app-ev-field-value-cell',
  templateUrl: './ev-field-value-cell.component.html',
  styleUrls: ['./ev-field-value-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvFieldValueCellComponent
  extends AbstractListCellRendererComponent
  implements OnDestroy
{
  readonly unsub$ = new Subject<void>();
  private readonly INTERPRETED_TEXT_REGEX = '^[a-zA-Z0-9_$]*';
  private readonly PLAIN_TEXT_REGEX = '^[\x20-\x7E]+$';

  @ViewChild('templatePortalContent', { read: TemplateRef })
  templatePortalContent: TemplateRef<any>;

  @ViewChild('optionName', { read: ElementRef })
  optionNameElementRef: ElementRef;

  @ViewChild('editableTextFieldComponent')
  editableTextFieldComponent: EditableTextFieldComponent;

  panelItems: ListPanelItem[] = [];

  options: EnvironmentVariableOption[] = [];

  componentData$: Observable<BoundEnvironmentVariableState>;

  constructor(
    private readonly route: ActivatedRoute,
    public grid: GridService,
    public cdr: ChangeDetectorRef,
    public readonly translateService: TranslateService,
    public readonly overlay: Overlay,
    public readonly vcr: ViewContainerRef,
    public readonly restService: RestService,
    public readonly actionErrorDisplayService: ActionErrorDisplayService,
    private boundEnvironmentVariableService: BoundEnvironmentVariableService,
    protected dialogService: DialogService,
  ) {
    super(grid, cdr, overlay, vcr, translateService, restService, actionErrorDisplayService);
    this.componentData$ = this.boundEnvironmentVariableService.state$.pipe(take(1));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  getValue() {
    return this.row.data[GridColumnId.value];
  }

  getValidators() {
    const validators = [Validators.maxLength(255)];
    const regexPattern = this.addRegexValidators(this.row.data[GridColumnId.inputType]);
    validators.push(Validators.pattern(regexPattern));
    return validators;
  }

  addRegexValidators(inputType: string) {
    let regexPattern: string;

    this.componentData$.pipe(take(1)).subscribe((state: BoundEnvironmentVariableState) => {
      if (this.isExecutionDialog(state) && EvInputType.INTERPRETED_TEXT === inputType) {
        regexPattern = this.INTERPRETED_TEXT_REGEX;
      } else if (this.isExecutionDialog(state) && EvInputType.PLAIN_TEXT === inputType) {
        regexPattern = this.PLAIN_TEXT_REGEX;
      }
    });

    return regexPattern;
  }

  isExecutionDialog(state: BoundEnvironmentVariableState) {
    return AutomationEnvironmentTagHolder.EXECUTION_DIALOG.toString() === state.entityType;
  }

  isTextFieldInputType(inputType: EvInputType): boolean {
    return getTextFieldInputTypes().includes(inputType);
  }

  change(valueChange: any) {
    const environmentVariableId = this.row.data[GridColumnId.id];
    const bindingId = this.row.data[GridColumnId.bindingId];
    this.grid.beginAsyncOperation();
    this.boundEnvironmentVariableService
      .setEnvironmentVariableValue(bindingId, valueChange, environmentVariableId)
      .pipe(
        catchError((error) => {
          this.editableTextFieldComponent.cancel();
          return this.actionErrorDisplayService.handleActionError(error);
        }),
        finalize(() => {
          this.grid.completeAsyncOperation();
        }),
      )
      .subscribe();
    this.close();
  }

  cellText(): string {
    if (this.row.data[GridColumnId.value] === '') {
      return '-';
    }
    return this.row.data[GridColumnId.value];
  }

  getPanelItems(): ListPanelItem[] {
    const panel = this.row.data[GridColumnId.options].map((option) => {
      return {
        label: option.label,
        id: option.label,
      };
    });
    panel.unshift({
      label: this.translateService.instant('sqtm-core.generic.label.none.feminine'),
      id: '',
    });
    return panel;
  }

  formErrors(errors: any) {
    const i18nKey = this.generateI18nErrorKey(errors);

    this.editableTextFieldComponent.cancel();
    timer(1, 10)
      .pipe(
        take(1),
        tap(() => {
          this.dialogService.openDialog({
            id: 'alert',
            component: AlertDialogComponent,
            viewContainerReference: this.vcr,
            data: {
              id: 'param-error',
              level: 'DANGER',
              titleKey: 'sqtm-core.generic.label.error',
              messageKey: i18nKey,
            },
          });
        }),
      )
      .subscribe();
  }

  private generateI18nErrorKey(errors: ValidationErrors): string {
    if ('pattern' in errors) {
      return getEvInputTypeI18nCustomPatternErrorMessage(this.row.data[GridColumnId.inputType]);
    } else {
      const clientSideErrors = this.generateClientSideErrors(errors);
      return clientSideErrors[0].provideI18nKey();
    }
  }

  private generateClientSideErrors(errors: ValidationErrors): ErrorI18nKeyProvider[] {
    return Object.entries(errors).map(([fieldName, errorValue]: [string, any]) =>
      buildErrorI18nKeyProvider(fieldName, errorValue),
    );
  }
}

export function environmentVariableValueColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(EvFieldValueCellComponent);
}
