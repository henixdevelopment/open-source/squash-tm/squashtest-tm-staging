import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ChartDefinitionService, ChartType, ReferentialDataService } from 'sqtm-core';
import { Layout, newPlot } from 'plotly.js';
import { AbstractCustomReportChart } from '../abstract-custom-report-chart';
import { ChartsTranslateService } from '../../../services/charts-translate.service';
import { chartsLogger } from '../../../charts.logger';
import { TranslateService } from '@ngx-translate/core';

const logger = chartsLogger.compose('CustomReportBarChartComponent');

@Component({
  selector: 'sqtm-app-custom-report-bar-chart',
  templateUrl: './custom-report-bar-chart.component.html',
  styleUrls: ['./custom-report-bar-chart.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomReportBarChartComponent extends AbstractCustomReportChart {
  constructor(
    protected chartsTranslateService: ChartsTranslateService,
    protected chartDefinitionService: ChartDefinitionService,
    protected referentialDataService: ReferentialDataService,
    protected translateService: TranslateService,
  ) {
    super(chartsTranslateService, chartDefinitionService, referentialDataService, translateService);
  }

  protected generateChart() {
    const rawLabels = this.chartDefinition.abscissa.flat(2);
    const labels = this.chartsTranslateService.convertAxisLabels(
      this.chartDefinition.axis[0],
      rawLabels,
      this.chartDefinition,
    );
    const yValues = Object.values(this.chartDefinition.series)[0];
    logger.debug('converted labels for bar chart ', [rawLabels, labels]);

    const colors = this.getColors(rawLabels, this.chartDefinition.axis[0]);

    const data: any = [
      {
        x: labels,
        y: yValues,
        type: 'bar',
        marker: {
          color: colors,
        },
        text: yValues,
        textposition: 'inside',
        hoverinfo: 'none',
        textfont: this.legendFont,
      },
    ];

    const layout: Partial<Layout> = {
      title: this.chartTitle,
      legend: {
        font: this.legendFont,
      },
      dragmode: false,
      showlegend: false,
      yaxis: {
        title: {
          text: this.getMeasureText(),
          font: this.axisTitleFont,
        },
        tickfont: this.legendFont,
        fixedrange: true,
      },
      xaxis: {
        type: 'category',
        title: {
          text: this.getAxisText(),
          font: this.axisTitleFont,
        },
        fixedrange: true,
        tickfont: this.legendFont,
      },
    };

    if (this.isCompactLayout()) {
      layout.margin = {
        t: 25,
      };
    }

    const config: Partial<Plotly.Config> = { staticPlot: true };

    newPlot(this.chartContainer.nativeElement, data, layout, config);
  }

  protected getHandledType(): ChartType {
    return ChartType.BAR;
  }
}
