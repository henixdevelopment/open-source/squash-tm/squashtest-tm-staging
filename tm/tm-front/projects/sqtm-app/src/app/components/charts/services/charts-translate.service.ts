import { Inject, Injectable } from '@angular/core';
import { AxisLabelsConverter } from '../converters/axis-labels-converters';
import { AXIS_CONVERTERS } from '../charts.constants';
import { chartsLogger } from '../charts.logger';
import { AxisColumn, ChartDefinitionModel } from 'sqtm-core';
import { PassTroughAxisConverter } from '../converters/pass-trough-axis-converter';

const logger = chartsLogger.compose('ChartsTranslateService');

@Injectable()
export class ChartsTranslateService {
  constructor(
    @Inject(AXIS_CONVERTERS) private axisLabelsConverters: ReadonlyArray<AxisLabelsConverter>,
    private passTroughAxisConverter: PassTroughAxisConverter,
  ) {
    logger.debug(
      `Loading translate service with ${axisLabelsConverters.length} axis label converters`,
    );
  }

  convertAxisLabels(
    axis: AxisColumn,
    rawLabels: any[],
    chartDefinition: ChartDefinitionModel,
  ): string[] {
    const converter = this.findConverter(axis, chartDefinition);
    return converter.convertAxisLabels(axis, rawLabels, chartDefinition);
  }

  findConverter(axis: AxisColumn, chartDefinition: ChartDefinitionModel): AxisLabelsConverter {
    const converters = this.findConvertersForAxis(axis, chartDefinition);
    this.assertConverterIsUnique(converters, axis);
    return converters[0];
  }

  private assertConverterIsUnique(converters: AxisLabelsConverter[], axis: AxisColumn) {
    if (converters.length > 1) {
      const message = `Multiple converters for axis : ${axis.column.label}`;
      logger.error(message, [converters]);
      throw Error(message);
    }
  }

  private findConvertersForAxis(
    axis: AxisColumn,
    chartDefinition?: ChartDefinitionModel,
  ): AxisLabelsConverter[] {
    const converters = this.axisLabelsConverters.filter((converter) =>
      converter.handleAxis(axis, chartDefinition),
    );
    if (converters.length === 0) {
      converters[0] = this.passTroughAxisConverter;
    }
    return converters;
  }
}
