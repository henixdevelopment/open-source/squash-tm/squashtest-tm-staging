import { AxisLabelsConverter } from './axis-labels-converters';
import {
  AutomationRequestStatus,
  AxisColumn,
  ExecutionStatus,
  LevelEnum,
  RequirementCriticality,
  RequirementStatus,
  TestCaseAutomatable,
  TestCaseStatus,
  TestCaseWeight,
} from 'sqtm-core';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class LevelEnumAxisConverter implements AxisLabelsConverter {
  private levelEnumByColumnPrototype: { [K in string]: LevelEnum<any> } = {
    EXECUTION_STATUS: ExecutionStatus,
    TEST_CASE_IMPORTANCE: TestCaseWeight,
    TEST_CASE_STATUS: TestCaseStatus,
    REQUIREMENT_STATUS: RequirementStatus,
    REQUIREMENT_VERSION_STATUS: RequirementStatus,
    REQUIREMENT_CRITICALITY: RequirementCriticality,
    REQUIREMENT_VERSION_CRITICALITY: RequirementCriticality,
    ITEM_TEST_PLAN_STATUS: ExecutionStatus,
    EXECUTION_EXECUTION_STATUS: ExecutionStatus,
    TEST_CASE_AUTOMATABLE: TestCaseAutomatable,
    AUTOMATION_REQUEST_STATUS: AutomationRequestStatus,
  };

  constructor(private translateService: TranslateService) {}

  convertAxisLabels(axis: AxisColumn, rawLabels: any[]): string[] {
    const map = rawLabels
      .map((rawLabel) => this.levelEnumByColumnPrototype[axis.column.label][rawLabel])
      .map((levelEnumItem) => this.translateService.instant(levelEnumItem.i18nKey));
    return map;
  }

  handleAxis(axis: AxisColumn): boolean {
    return Boolean(this.levelEnumByColumnPrototype[axis.column.label]);
  }
}
