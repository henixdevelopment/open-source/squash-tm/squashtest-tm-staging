import { AxisColumn, ChartDefinitionModel } from 'sqtm-core';

export interface AxisLabelsConverter {
  convertAxisLabels(
    axis: AxisColumn,
    rawLabels: any[],
    chartDefinition?: ChartDefinitionModel,
  ): string[];
  handleAxis(axis: AxisColumn, chartDefinition?: ChartDefinitionModel): boolean;
}
