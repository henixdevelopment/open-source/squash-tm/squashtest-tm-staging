import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ChartDefinitionModel } from 'sqtm-core';

@Component({
  selector: 'sqtm-app-custom-chart',
  templateUrl: './custom-chart.component.html',
  styleUrls: ['./custom-chart.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomChartComponent {
  @Input()
  chartDefinition: ChartDefinitionModel;

  @Input()
  layout: 'full' | 'compact' = 'full';
}
