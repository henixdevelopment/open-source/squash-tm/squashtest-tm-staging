import { convertSqtmLiterals, DataRow, ProjectDataMap, SquashTmDataRowType } from 'sqtm-core';

export function automationRequestLiteralConverter(
  literals: Partial<DataRow>[],
  projectDataMap: ProjectDataMap,
): DataRow[] {
  const itpiLiterals = literals.map((li) => ({
    ...li,
    type: SquashTmDataRowType.AutomationRequest,
    projectId: li.data.projectId,
  }));
  return convertSqtmLiterals(itpiLiterals, projectDataMap);
}
