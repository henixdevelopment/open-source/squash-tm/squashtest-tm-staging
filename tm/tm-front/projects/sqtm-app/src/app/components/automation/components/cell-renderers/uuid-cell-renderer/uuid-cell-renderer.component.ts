import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-uuid-cell-renderer',
  template: `
    @if (columnDisplay && row) {
      <div class="full-width full-height flex-column" style="align-items: center">
        <i
          nz-typography
          nzCopyable
          [nzCopyIcons]="['sqtm-core-generic:copy', 'sqtm-core-generic:confirm']"
          [nzCopyText]="getUuid()"
          class="action-icon-size"
        ></i>
      </div>
    }
  `,
  styleUrls: ['./uuid-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UuidCellRendererComponent extends AbstractCellRendererComponent {
  constructor(gridService: GridService, cdr: ChangeDetectorRef) {
    super(gridService, cdr);
  }

  getUuid() {
    return this.row.data[this.columnDisplay.id];
  }
}

export function uuidColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withHeaderPosition('center')
    .withRenderer(UuidCellRendererComponent);
}
