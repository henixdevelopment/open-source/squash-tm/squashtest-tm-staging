import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  signal,
} from '@angular/core';
import { TestCaseDisplayData, WorkspaceCommonModule } from 'sqtm-core';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { TranslateModule } from '@ngx-translate/core';
import { NgClass } from '@angular/common';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

@Component({
  selector: 'sqtm-app-generated-test-case-foldable-row',
  templateUrl: './generated-test-case-foldable-row.component.html',
  styleUrl: './generated-test-case-foldable-row.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [WorkspaceCommonModule, NzIconModule, TranslateModule, NgClass, NzToolTipModule],
})
export class GeneratedTestCaseFoldableRowComponent {
  @Input()
  testCase: TestCaseDisplayData;

  @Input()
  isSimulation: boolean = false;

  @Output()
  isTestCaseSelected = new EventEmitter<boolean>();

  $isRowOpen = signal<boolean>(false);

  getGridElementClass(stepIndex: number, columnNumber: number): string {
    if (columnNumber === 1) {
      if (stepIndex === 0) {
        return 'grid-item-full';
      } else {
        return 'grid-item-left-right-bottom';
      }
    } else {
      if (stepIndex === 0) {
        return 'grid-item-right-top-bottom';
      } else {
        return 'grid-item-right-bottom';
      }
    }
  }

  getIconName() {
    return this.$isRowOpen() ? 'down' : 'right';
  }

  changeRowState() {
    if (this.$isRowOpen()) {
      this.$isRowOpen.set(false);
    } else {
      this.$isRowOpen.set(true);
    }
  }

  updateSelectedTestCase(isSelected: boolean) {
    this.isTestCaseSelected.emit(isSelected);
  }
}
