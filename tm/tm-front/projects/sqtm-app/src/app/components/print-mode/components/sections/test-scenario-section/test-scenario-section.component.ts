import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import {
  KeywordTestCaseScenarioSection,
  PrintTestStepKind,
  ScriptedTestCaseScenarioSection,
  StandardTestCaseScenarioSection,
  StepRow,
  TestCaseScenarioSection,
} from '../../../state/PrintData.state';

function asRow(
  step: StepRow,
  index: number,
  showAttachments: boolean,
  showLinkedReqs: boolean,
): Row {
  if (step.kind === PrintTestStepKind.action) {
    const cells: Cell[] = [
      {
        content: (index + 1).toString(10),
      },
      {
        content: step.action,
        renderAsHtml: true,
      },
      {
        content: step.expectedResult,
        renderAsHtml: true,
      },
    ];

    step.customFieldValues.forEach((cfv) => {
      cells.push({
        content: cfv.value,
        renderAsHtml: cfv.renderAsHtml,
      });
    });

    if (showAttachments) {
      cells.push({
        content: step.attachmentCount.toString(),
      });
    }

    if (showLinkedReqs) {
      cells.push({
        content: step.linkedRequirementCount.toString(),
      });
    }

    return {
      kind: PrintTestStepKind.action,
      cells,
    };
  } else {
    return {
      kind: PrintTestStepKind.call,
      calledTestCase: step.calledTestCaseName,
      calledTestSteps: step.steps,
      calledDataset: step.datasetName,
    };
  }
}

@Component({
  selector: 'sqtm-app-test-scenario-section',
  templateUrl: './test-scenario-section.component.html',
  styleUrls: ['../sections-common.less', './test-scenario-section.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestScenarioSectionComponent {
  @Input() section: TestCaseScenarioSection;

  get isStandardScenario(): boolean {
    return this.section.testCaseKind === 'STANDARD';
  }

  get standardScenario(): StandardTestCaseScenarioSection {
    return this.section as StandardTestCaseScenarioSection;
  }

  get isScriptedScenario(): boolean {
    return this.section.testCaseKind === 'GHERKIN';
  }

  get script(): string {
    if (!this.isScriptedScenario) {
      return '';
    }

    const scriptedSection = this.section as ScriptedTestCaseScenarioSection;
    return scriptedSection.script;
  }

  get isKeywordScenario(): boolean {
    return this.section.testCaseKind === 'KEYWORD';
  }

  get keywordScenario(): KeywordTestCaseScenarioSection {
    return this.section as KeywordTestCaseScenarioSection;
  }

  get columnCount(): number {
    if (this.isStandardScenario) {
      const showAttachmentCount = this.standardScenario.steps.some((step) =>
        stepHasAttachments(step),
      );
      const showLinkedReqCount = this.standardScenario.steps.some((step) =>
        stepHasLinkedRequirements(step),
      );
      const numCustomFields = countCustomFields(this.standardScenario.steps);
      const numFixedColumns = 3;

      return (
        numFixedColumns +
        numCustomFields +
        (showAttachmentCount ? 1 : 0) +
        (showLinkedReqCount ? 1 : 0)
      );
    } else if (this.isKeywordScenario) {
      return 3;
    } else {
      return 0;
    }
  }

  get headers(): string[] {
    if (this.isKeywordScenario) {
      return [
        '#',
        'sqtm-core.entity.test-step.keyword.singular',
        'sqtm-core.entity.test-step.action.singular',
      ];
    }

    return this.standardHeaders;
  }

  get rows(): Row[] {
    if (this.isKeywordScenario) {
      return this.keywordRows;
    } else if (this.isStandardScenario) {
      const showAttachmentCount = this.standardScenario.steps.some((step) =>
        stepHasAttachments(step),
      );
      const showLinkedReqCount = this.standardScenario.steps.some((step) =>
        stepHasLinkedRequirements(step),
      );
      return this.standardScenario.steps.map((s, idx) =>
        asRow(s, idx, showAttachmentCount, showLinkedReqCount),
      );
    }

    return [];
  }

  private get keywordRows(): Row[] {
    return this.keywordScenario.steps.map((stepRow, idx) => {
      const actionCell: Cell = {
        content: stepRow.action,
        renderAsHtml: true,
      };
      if (stepRow.datatable) {
        actionCell.datatable = stepRow.datatable;
      }
      if (stepRow.docstring) {
        actionCell.docstring = stepRow.docstring;
      }
      if (stepRow.comment) {
        actionCell.comment = stepRow.comment;
      }
      return {
        kind: PrintTestStepKind.action,
        cells: [{ content: (idx + 1).toString(10) }, { content: stepRow.keyword }, actionCell],
      };
    });
  }

  private get standardHeaders(): string[] {
    const showAttachmentCount = this.standardScenario.steps.some((step) =>
      stepHasAttachments(step),
    );
    const showLinkedReqCount = this.standardScenario.steps.some((step) =>
      stepHasLinkedRequirements(step),
    );
    const cufNames = getCufNames(this.standardScenario.steps);

    const headers = [
      '#',
      'sqtm-core.entity.test-step.action.singular',
      'sqtm-core.entity.test-step.expected-result.label',
      ...cufNames,
    ];

    if (showAttachmentCount) {
      headers.push('sqtm-core.search.test-case.grid.header.attachments.label');
    }

    if (showLinkedReqCount) {
      headers.push('sqtm-core.search.test-case.grid.header.coverages.label');
    }

    return headers;
  }
}

function stepHasAttachments(step: StepRow): boolean {
  return step.kind === PrintTestStepKind.action && step.attachmentCount > 0;
}

function stepHasLinkedRequirements(step: StepRow): boolean {
  return step.kind === PrintTestStepKind.action && step.linkedRequirementCount > 0;
}

function countCustomFields(steps: StepRow[]): number {
  return getCufNames(steps).length;
}

function getCufNames(steps: StepRow[]): string[] {
  const knownMap = {};

  steps.forEach((s) => {
    if (s.kind === PrintTestStepKind.action) {
      s.customFieldValues.forEach((cfv) => {
        knownMap[cfv.name] = true;
      });
    }
  });

  return Object.keys(knownMap);
}

interface ActionRow {
  kind: PrintTestStepKind.action;
  cells: Cell[];
}

interface CallRow {
  kind: PrintTestStepKind.call;
  calledTestCase: string;
  calledDataset: string;
  calledTestSteps: StepRow[];
}

type Row = ActionRow | CallRow;

interface Cell {
  content: string;
  datatable?: string;
  docstring?: string;
  comment?: string;
  renderAsHtml?: boolean;
}
