import { Injectable } from '@angular/core';

const DIALOG_WIDTH = 1000;
const DIALOG_HEIGHT = 800;

@Injectable({
  providedIn: 'root',
})
export class PrintModeService {
  static openPrintTestCaseWindow(testCaseId: number): void {
    const url = `print/test-case/${testCaseId}`;
    window.open(url, '', `width=${DIALOG_WIDTH},height=${DIALOG_HEIGHT}`);
  }

  static openPrintRequirementVersionWindow(
    requirementId: number,
    requirementVersionId: number,
  ): void {
    const url = `print/requirement/${requirementId}/version/${requirementVersionId}`;
    window.open(url, '', `width=${DIALOG_WIDTH},height=${DIALOG_HEIGHT}`);
  }

  static openPrintDialog(): void {
    window.print();
  }
}
