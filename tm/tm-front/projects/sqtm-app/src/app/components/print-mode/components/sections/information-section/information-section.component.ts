import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { InformationSection } from '../../../state/PrintData.state';

@Component({
  selector: 'sqtm-app-information-section',
  templateUrl: './information-section.component.html',
  styleUrls: ['../sections-common.less', './information-section.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InformationSectionComponent {
  @Input() section: InformationSection;
}
