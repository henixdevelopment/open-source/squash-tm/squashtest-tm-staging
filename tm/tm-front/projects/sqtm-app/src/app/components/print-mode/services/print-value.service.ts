import {
  BindableEntity,
  CUF_DATE_FORMAT,
  CustomField,
  CustomFieldBinding,
  CustomFieldValueModel,
  DataRowModel,
  getSupportedBrowserLang,
  GridColumnId,
  I18nEnum,
  Identifier,
  InputType,
  isSystemInfoList,
  Project,
  ReferentialDataState,
  SystemRequirementCodeEnum,
} from 'sqtm-core';
import { formatDate } from '@angular/common';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { parse } from 'date-fns';
import { PrintField } from '../state/PrintData.state';

@Injectable({
  providedIn: 'root',
})
export class PrintValueService {
  constructor(public readonly translateService: TranslateService) {}

  public extractEnumKey<M>(model: M, key: keyof M, i18nEnum: I18nEnum<any>): string {
    const value = model[key] as any;
    return i18nEnum[value]?.i18nKey;
  }

  public extractInfoListItemLabel(
    projectId: Identifier,
    referentialData: ReferentialDataState,
    infoListItemId: number,
    infoListIdAttributeName: keyof Project,
  ) {
    const listId: any = referentialData.projectState.entities[projectId][infoListIdAttributeName];
    const listEntity = referentialData.infoListState.entities[listId];
    const listItem = listEntity.items.find((item) => item.id === infoListItemId);

    if (isSystemInfoList(listEntity)) {
      return `sqtm-core.entity.${listItem.label}`;
    } else {
      return listItem.label;
    }
  }

  public formatAuditableValue(dateAsString: string, user: string): string {
    const dateFormatted = this.formatDate(dateAsString);
    return `${dateFormatted} (${user})`;
  }

  public printCustomFieldsSorted(
    customFieldValues: CustomFieldValueModel[],
    referentialData: ReferentialDataState,
    bindableEntity: BindableEntity,
    projectId: number,
  ) {
    const customFieldBindings =
      referentialData.projectState.entities[projectId].customFieldBindings[bindableEntity];
    return this.getCUFPosition(customFieldValues, customFieldBindings)
      .sort((cfv1, cfv2) => cfv1.position - cfv2.position)
      .map((cfv) => this.asCustomPrintField(cfv, referentialData));
  }

  private getCUFPosition(
    customFieldValues: CustomFieldValueModel[],
    customFieldBindings: CustomFieldBinding[],
  ) {
    return customFieldValues.map((cfv) => {
      const customFieldBinding = customFieldBindings.find(
        (binding) => binding.customFieldId === cfv.cufId,
      );
      return {
        ...cfv,
        position: customFieldBinding.position,
      };
    });
  }

  private asCustomPrintField(
    cfv: CustomFieldValueModel,
    referentialData: ReferentialDataState,
  ): PrintField {
    const customField = referentialData.customFieldState.entities[cfv.cufId];
    if (customField) {
      return {
        name: customField.label,
        value: PrintValueService.formatCustomFieldValue(cfv, customField),
        renderAsHtml: customField.inputType === InputType.RICH_TEXT,
      };
    } else {
      return null;
    }
  }

  public formatDate(dateAsString: string | Date): string {
    if (dateAsString == null) {
      return null;
    }

    return formatDate(dateAsString, 'short', getSupportedBrowserLang(this.translateService));
  }

  private static formatCustomFieldValue(
    cfv: CustomFieldValueModel,
    customField: CustomField,
  ): string {
    switch (customField.inputType) {
      case InputType.TAG:
        return cfv.value.split('|').join(', ');
      case InputType.CHECKBOX:
        return cfv.value === 'true'
          ? 'sqtm-core.generic.label.true'
          : 'sqtm-core.generic.label.false';
      case InputType.DATE_PICKER:
        if (cfv.value === '') {
          return null;
        } else {
          return parse(cfv.value, CUF_DATE_FORMAT, new Date()).toLocaleDateString();
        }
      case InputType.PLAIN_TEXT:
      case InputType.NUMERIC:
      case InputType.RICH_TEXT:
      default:
        return cfv.value;
    }
  }

  public formatHistoryEvent(row: DataRowModel): string {
    const event = row.data.event;
    if (event == null) {
      return this.formatSynReqEvent(row);
    }
    const modificationPrefix = this.translateService.instant(
      'sqtm-core.entity.requirement.requirement-version.modification-history.action.modification',
    );
    if (['category', 'criticality', 'name', 'reference', 'status', 'description'].includes(event)) {
      const target = this.translateService.instant(
        'sqtm-core.entity.requirement.requirement-version.modification-history.element.' + event,
      );
      return modificationPrefix + ' ' + target;
    }
    return event;
  }

  private formatSynReqEvent(row: DataRowModel) {
    if (row.data[GridColumnId.syncReqCreationSource] != null) {
      return this.translateService.instant(
        'sqtm-core.entity.requirement.requirement-version.modification-history.action.sync-creation',
      );
    }
    if (row.data[GridColumnId.syncReqUpdateSource] != null) {
      return this.translateService.instant(
        'sqtm-core.entity.requirement.requirement-version.modification-history.action.sync-modification',
      );
    }
    return '';
  }

  public formatHistoryChangedValue(target, event) {
    target = PrintValueService.updateCategoryEventTarget(event, target);
    if (['criticality', 'status'].includes(event)) {
      return this.translateService.instant(`sqtm-core.entity.requirement.${event}.${target}`);
    }
    if (event === 'category') {
      return Object.values(SystemRequirementCodeEnum).includes(target)
        ? this.translateService.instant(`sqtm-core.entity.requirement.category.${target}`)
        : target;
    }
    if (['name', 'reference'].includes(event)) {
      return target;
    }
    return '';
  }

  private static updateCategoryEventTarget(event, target) {
    if ('category' === event) {
      target = target.replace('requirement.category.', '');
    }
    return target;
  }

  public formatReqLinksRole(role: string): string {
    if (role.includes('requirement-version.link.type')) {
      return this.translateService.instant('sqtm-core.entity.requirement.' + role);
    } else {
      return role;
    }
  }
}
