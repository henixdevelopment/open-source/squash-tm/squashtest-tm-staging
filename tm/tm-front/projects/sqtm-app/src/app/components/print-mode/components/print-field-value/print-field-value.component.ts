import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { PrintField } from '../../state/PrintData.state';

@Component({
  selector: 'sqtm-app-print-field-value',
  template: `
    @if (field && field.value) {
      @if (field.renderAsHtml) {
        <span [innerHTML]="field.value | safeRichContent"> </span>
      } @else {
        <span>
          {{ field.value | translate }}
        </span>
      }
    } @else {
      <span>-</span>
    }
  `,
  styleUrls: ['./print-field-value.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PrintFieldValueComponent {
  @Input() field: PrintField;
}
