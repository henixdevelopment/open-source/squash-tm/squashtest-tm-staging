import { ChangeDetectionStrategy, Component } from '@angular/core';
import {
  column,
  DataRow,
  Extendable,
  Fixed,
  GenericDataRow,
  GridColumnId,
  GridDefinition,
  GridService,
  indexColumn,
  levelEnumColumn,
  OpenCloseCellRendererComponent,
  RequirementCriticality,
  StyleDefinitionBuilder,
  table,
  textColumn,
} from 'sqtm-core';
import { RequirementFoldableRowComponent } from '../requirement-foldable-row/requirement-foldable-row.component';
import { LinkedToStepCellRendererComponent } from '../cell-renderers/linked-to-step-cell-renderer/linked-to-step-cell-renderer.component';
import { DeleteCoverageCellRendererComponent } from '../cell-renderers/delete-coverage-cell-renderer/delete-coverage-cell-renderer.component';
import { DETAILED_COVERAGE_TABLE } from '../../detailed-step-view.constant.ts.constant';

export function detailedStepCoverageTableDefinition(): GridDefinition {
  return table('detailed-test-step-view-coverages')
    .withColumns([
      indexColumn(), // do not put in leftViewport we have a special row renderer here
      column(GridColumnId.open)
        .changeWidthCalculationStrategy(new Fixed(20))
        .withRenderer(OpenCloseCellRendererComponent)
        .disableHeader()
        .disableSort(),
      column(GridColumnId.linkedToCurrentStep)
        .changeWidthCalculationStrategy(new Fixed(70))
        .withRenderer(LinkedToStepCellRendererComponent)
        .withI18nKey('sqtm-core.detailed-test-step-view.coverages.linked-step.short')
        .withTitleI18nKey('sqtm-core.detailed-test-step-view.coverages.linked-step.long')
        .withHeaderPosition('center')
        .disableSort(),
      textColumn(GridColumnId.projectName)
        .changeWidthCalculationStrategy(new Extendable(100, 1))
        .withI18nKey('sqtm-core.entity.project.label.singular'),
      textColumn(GridColumnId.reference).withI18nKey('sqtm-core.entity.generic.reference.label'),
      textColumn(GridColumnId.name)
        .changeWidthCalculationStrategy(new Extendable(100, 1.5))
        .withI18nKey('sqtm-core.entity.requirement.label.singular'),
      levelEnumColumn(GridColumnId.criticality, RequirementCriticality)
        .withI18nKey('sqtm-core.entity.generic.criticality.label')
        .changeWidthCalculationStrategy(new Fixed(78))
        .isEditable(false),
      column(GridColumnId.delete)
        .changeWidthCalculationStrategy(new Fixed(50))
        .withRenderer(DeleteCoverageCellRendererComponent)
        .disableHeader()
        .disableSort(),
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .withRowConverter(foldRequirementLiteralConverter)
    .build();
}

export function foldRequirementLiteralConverter(literals: Partial<DataRow>[]): DataRow[] {
  return literals.reduce((datarows, literal) => {
    const dataRow = new GenericDataRow();
    Object.assign(dataRow, literal);
    dataRow.component = RequirementFoldableRowComponent;
    datarows.push(dataRow);
    return datarows;
  }, []);
}

@Component({
  selector: 'sqtm-app-detailed-step-coverage-table',
  templateUrl: './detailed-step-coverage-table.component.html',
  styleUrls: ['./detailed-step-coverage-table.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: DETAILED_COVERAGE_TABLE,
    },
  ],
})
export class DetailedStepCoverageTableComponent {}
