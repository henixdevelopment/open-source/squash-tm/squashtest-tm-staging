import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  NgZone,
  OnDestroy,
  Renderer2,
  ViewChild,
} from '@angular/core';
import {
  ColumnDisplay,
  GridDisplay,
  GridNode,
  GridService,
  GridViewportService,
  RowRenderer,
  ViewportDisplay,
} from 'sqtm-core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-requirement-foldable-row',
  templateUrl: './requirement-foldable-row.component.html',
  styleUrls: ['./requirement-foldable-row.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RequirementFoldableRowComponent implements OnDestroy, RowRenderer, AfterViewInit {
  @Input()
  gridDisplay: GridDisplay;

  @Input()
  gridNode: GridNode;

  @Input()
  viewport: ViewportDisplay;

  @ViewChild('details', { read: ElementRef })
  details: ElementRef;

  private unsub$ = new Subject<void>();

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    private gridViewportService: GridViewportService,
    private ngZone: NgZone,
    private renderer: Renderer2,
  ) {}

  ngAfterViewInit(): void {
    this.ngZone.runOutsideAngular(() => {
      this.gridViewportService.renderedGridViewport$
        .pipe(takeUntil(this.unsub$))
        .subscribe((renderedGridViewport) => {
          if (this.details) {
            this.renderer.setStyle(
              this.details.nativeElement,
              'width',
              `${renderedGridViewport.mainViewport.totalWidth}px`,
            );
          }
        });
    });
  }

  trackByFn(index: number, columnDisplay: ColumnDisplay) {
    return columnDisplay.id;
  }

  shouldDrawCell(columnDisplay: ColumnDisplay) {
    return columnDisplay.show;
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
