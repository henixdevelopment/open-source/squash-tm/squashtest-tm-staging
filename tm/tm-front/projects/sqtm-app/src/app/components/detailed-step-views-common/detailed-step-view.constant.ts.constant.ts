import { InjectionToken } from '@angular/core';
import { StepViewRequirementLinkService } from './step-view-requirement-link.service';
import { GridDefinition, GridService } from 'sqtm-core';

export const STEP_VIEW_REQUIREMENT_LINK_SERVICE =
  new InjectionToken<StepViewRequirementLinkService>('Step view Service');
export const DETAILED_STEP_COVERAGE_TABLE_CONF = new InjectionToken<GridDefinition>(
  'Grid config for the coverage table of detailed test step view',
);
export const DETAILED_COVERAGE_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the coverage table of detailed test step view',
);
