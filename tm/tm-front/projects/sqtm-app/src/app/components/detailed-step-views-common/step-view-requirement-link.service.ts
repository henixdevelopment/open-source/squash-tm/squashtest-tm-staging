import { Observable } from 'rxjs';
import { EntityViewComponentData, TestCasePermissions } from 'sqtm-core';

export interface StepViewRequirementLinkService {
  componentData$: Observable<EntityViewComponentData<any, any, TestCasePermissions>>;

  stepCreationMode$: Observable<boolean>;

  deleteStepCoverages(requirementVersionIds: number[]): Observable<any>;

  deleteCoverages(requirementVersionIds: number[]);

  linkRequirementToCurrentStep(requirementVersionId: number): Observable<any>;
}
