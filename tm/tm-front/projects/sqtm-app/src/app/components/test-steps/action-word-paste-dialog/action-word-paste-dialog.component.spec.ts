import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionWordPasteDialogComponent } from './action-word-paste-dialog.component';
import { TranslateService } from '@ngx-translate/core';
import { DialogReference } from 'sqtm-core';
import { mockPassThroughTranslateService } from '../../../utils/testing-utils/mocks.service';
import { AppTestingUtilsModule } from '../../../utils/testing-utils/app-testing-utils.module';

describe('ActionWordPasteDialogComponent', () => {
  let component: ActionWordPasteDialogComponent;
  let fixture: ComponentFixture<ActionWordPasteDialogComponent>;

  const dialogRef = jasmine.createSpyObj(['close']);
  dialogRef.data = { hasLocalPassword: true };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [
        {
          provide: DialogReference,
          useValue: dialogRef,
        },
        {
          provide: TranslateService,
          useValue: mockPassThroughTranslateService(),
        },
      ],
      imports: [AppTestingUtilsModule],
      declarations: [ActionWordPasteDialogComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionWordPasteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
