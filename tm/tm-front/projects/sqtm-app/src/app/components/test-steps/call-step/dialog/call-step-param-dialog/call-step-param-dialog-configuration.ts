import { CallStepView } from '../../../../../pages/test-case-workspace/test-case-view/state/test-step.state';
import { Dataset } from 'sqtm-core';

export class CallStepParamDialogConfiguration {
  id: string;
  callStep: CallStepView;
  testCaseName: string;
  dataSets: Dataset[];
}
