import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { DialogReference, Option } from 'sqtm-core';
import { CallStepParamDialogConfiguration } from './call-step-param-dialog-configuration';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-call-step-param-dialog',
  templateUrl: './call-step-param-dialog.component.html',
  styleUrls: ['./call-step-param-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CallStepParamDialogComponent implements OnInit {
  data: CallStepParamDialogConfiguration;
  translateParams;
  datasetOptions: Option[];

  constructor(
    private dialogReference: DialogReference<CallStepParamDialogConfiguration>,
    private translateService: TranslateService,
  ) {
    this.data = dialogReference.data;
    this.translateParams = {
      calledTestCase: this.data.callStep.calledTcName,
      testCase: this.data.testCaseName,
    };
  }

  ngOnInit(): void {
    this.datasetOptions = this.convertDataSetsToOptions();
  }

  convertDataSetsToOptions() {
    const options: Option[] = [];
    options.push({
      label: this.translateService.instant('sqtm-core.generic.label.none.masculine'),
      value: '',
    });
    this.data.dataSets.forEach((dataset) => {
      options.push({ label: dataset.name, value: dataset.id.toString() });
    });
    return options;
  }

  changeDataSet(datasetId: string) {
    if (datasetId !== '') {
      this.data.callStep.calledDatasetId = parseInt(datasetId, 10);
      this.data.callStep.calledDatasetName = this.data.dataSets.find(
        (dataset) => dataset.id === parseInt(datasetId, 10),
      ).name;
    } else {
      this.data.callStep.calledDatasetId = null;
      this.data.callStep.calledDatasetName = null;
    }
  }

  confirm() {
    this.dialogReference.result = {
      datasetId: this.data.callStep.delegateParam ? null : this.data.callStep.calledDatasetId,
      delegateParam: this.data.callStep.delegateParam,
      datasetName: this.data.callStep.calledDatasetName,
    };

    this.dialogReference.close();
  }
}
