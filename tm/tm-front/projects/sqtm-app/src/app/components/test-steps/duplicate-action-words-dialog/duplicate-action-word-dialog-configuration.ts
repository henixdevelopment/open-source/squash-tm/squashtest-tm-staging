import { DuplicateActionWord } from 'sqtm-core';

export interface DuplicateActionWordDialogConfiguration {
  duplicateActionWords: DuplicateActionWord[];
}
