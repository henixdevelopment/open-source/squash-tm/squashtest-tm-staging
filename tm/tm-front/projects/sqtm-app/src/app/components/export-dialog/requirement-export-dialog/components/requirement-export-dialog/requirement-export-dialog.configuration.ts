import { RequirementCurrentVersionFilterKeys } from 'sqtm-core';

export class RequirementExportDialogConfiguration {
  id: string;
  nodes: number[];
  libraries: number[];
  requirementVersionFilter: RequirementCurrentVersionFilterKeys;
}
