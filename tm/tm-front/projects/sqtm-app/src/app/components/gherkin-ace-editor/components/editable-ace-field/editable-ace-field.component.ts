import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  ViewChild,
} from '@angular/core';
import * as ace from 'ace-builds';
import 'ace-builds/webpack-resolver';
import 'ace-builds/src-noconflict/ext-language_tools';
import 'ace-builds/src-noconflict/ext-split';
import { TranslateService } from '@ngx-translate/core';
import { DOC_DE, DOC_EN, DOC_ES, DOC_FR } from './gherkin-documentation.const';
import { SessionPingService } from 'sqtm-core';
import * as CssElementQuery from 'css-element-queries';
import { ResizeSensor } from 'css-element-queries';

@Component({
  selector: 'sqtm-app-editable-ace-field',
  templateUrl: './editable-ace-field.component.html',
  styleUrls: ['./editable-ace-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditableAceFieldComponent implements AfterViewInit, OnDestroy {
  @ViewChild('codeEditor') codeEditorElmRef?: ElementRef;

  private codeEditor;

  @Input()
  theme = 'ace/theme/chrome';

  @Input()
  value;

  @Input()
  canWrite = true;

  edit = false;

  split: any;

  @Output()
  confirmEvent = new EventEmitter<string>();

  @Output()
  checkSyntaxEvent = new EventEmitter<string>();

  private readonly aceBaseField = 'assets/js/ace/';

  private _resizeSensor: ResizeSensor;

  constructor(
    private translateService: TranslateService,
    private readonly sessionPingService: SessionPingService,
  ) {}

  ngAfterViewInit(): void {
    this.configureAce();
    this.initializeEditor();
  }

  ngOnDestroy(): void {
    if (this.edit) {
      this.sessionPingService.endLongTermEdition();
    }
  }

  private configureAce(): void {
    ace.config.set('basePath', this.aceBaseField);
    ace.config.set('modePath', this.aceBaseField);
    ace.config.set('themePath', this.aceBaseField);
    ace.config.setModuleUrl('ace/snippets/gherkin', `${this.aceBaseField}snippets/gherkin.js`);
    ace.config.setModuleUrl(
      'ace/snippets/gherkin-fr',
      `${this.aceBaseField}snippets/gherkin-fr.js`,
    );
    ace.config.setModuleUrl(
      'ace/snippets/gherkin-es',
      `${this.aceBaseField}snippets/gherkin-es.js`,
    );
    ace.config.setModuleUrl(
      'ace/snippets/gherkin-de',
      `${this.aceBaseField}snippets/gherkin-de.js`,
    );
  }

  private initializeEditor(): void {
    ace.config.loadModule('ace/ext/language_tools', (languageModule) => {
      ace.config.loadModule('ace/ext/split', (splitModule) => {
        const container = this.codeEditorElmRef.nativeElement;
        this.split = new splitModule.Split(container, 'ace/theme/chrome-grey', 1);
        this.codeEditor = this.split.getEditor(0);

        languageModule.setCompleters([languageModule.snippetCompleter]);

        this.codeEditor.getSession().setValue(this.value);
        this.codeEditor.setTheme('ace/theme/chrome-grey');
        this.codeEditor.getSession().setUseWrapMode(true);
        this.codeEditor.getSession().setWrapLimitRange(160, 160);
        this.codeEditor.setOptions({
          tabSize: 2,
          useSoftTabs: true,
          printMarginColumn: 160,
          readOnly: true,
          highlightActiveLine: false,
          highlightGutterLine: false,
          enableBasicAutocompletion: true,
          enableSnippets: true,
          enableLiveAutocompletion: true,
        });
        this.codeEditor.getSession().setMode(this.setMode());
        this.codeEditor.setShowFoldWidgets(true);
        this.codeEditor.getSession().setUndoManager(new ace.UndoManager());

        this.addKeyboardBindings();
        this.initializeResizeSensor();
      });
    });

    this.addSnippedFeature();
  }

  private addSnippedFeature(): void {
    ace.config.loadModule('ace/snippets', (snippetModule) => {
      const snippetManager = snippetModule.snippetManager;
      ace.config.loadModule(this.getSnippet(), (m) => {
        if (m) {
          snippetManager.files.gherkin = m;
          m.snippets = snippetManager.parseSnippetFile(m.snippetText);
          snippetManager.register(m.snippets, m.scope);
        }
      });
    });
  }

  private addKeyboardBindings(): void {
    this.codeEditor.commands.addCommand({
      name: 'saveGherkinScript',
      bindKey: { win: 'Ctrl-Alt-S', mac: 'Command-Alt-S' },
      exec: () => this.confirm(),
      readOnly: false,
    });

    this.codeEditor.commands.addCommand({
      name: 'undo',
      bindKey: { win: 'Ctrl-Z', mac: 'Command-Z' },
      exec: (editor) => editor.session.getUndoManager().undo(),
    });

    this.codeEditor.commands.addCommand({
      name: 'redo',
      bindKey: { win: 'Ctrl-Y', mac: 'Command-Y' },
      exec: (editor) => editor.session.getUndoManager().redo(),
    });
  }

  private getSnippet(): string {
    const lang = this.getLang();
    let snippetFile = 'ace/snippets/gherkin';

    if (lang !== 'en') {
      snippetFile = snippetFile + '-' + lang;
    }
    return snippetFile;
  }

  private setMode(): string {
    const lang = this.getLang();
    let mode = 'ace/mode/gherkin';
    if (lang !== 'en') {
      mode = `${mode}-${lang}`;
    }
    return mode;
  }

  private getDocumentation(): string {
    const language = this.getLang();
    switch (language) {
      case 'en':
        return DOC_EN;
      case 'fr':
        return DOC_FR;
      case 'es':
        return DOC_ES;
      case 'de':
        return DOC_DE;
      default:
        return DOC_EN;
    }
  }

  private getLang(): string {
    let scriptFirstLine: string = this.codeEditor.session.getLine(0);
    let language = 'en';
    if (scriptFirstLine != null) {
      scriptFirstLine = scriptFirstLine.trim();
      if (scriptFirstLine.includes('language:')) {
        language = scriptFirstLine.substring(scriptFirstLine.length - 2);
      }
    }
    return language;
  }

  toggleToEditMode(): void {
    if (this.canWrite && !this.edit) {
      this.edit = true;
      this.codeEditor.setOption('readOnly', false);
      this.codeEditor.setTheme(this.theme);

      this.sessionPingService.beginLongTermEdition();
    }
  }

  disableEditMode(): void {
    if (this.edit) {
      this.edit = false;
      this.restoreNotEditMode();

      this.sessionPingService.endLongTermEdition();
    }
  }

  showSnippet(): void {
    this.codeEditor.execCommand('startAutocomplete');
  }

  cancel(): void {
    this.codeEditor.getSession().setValue(this.value);
    this.disableEditMode();
  }

  confirm(): void {
    this.confirmEvent.emit(this.codeEditor.getSession().getValue());
    this.disableEditMode();
  }

  private restoreNotEditMode(): void {
    this.codeEditor.setTheme('ace/theme/chrome-grey');
    this.codeEditor.setOptions({
      readOnly: true,
      highlightActiveLine: false,
      highlightGutterLine: false,
    });
  }

  scriptValue(): void {
    return this.codeEditor.session.getValue();
  }

  checkSyntax(): void {
    this.checkSyntaxEvent.emit(this.codeEditor.getSession().getValue());
  }

  toggleShowHelp(): void {
    if (this.split.getSplits() === 2) {
      this.split.setSplits(1);
      this.codeEditor.getSession().setWrapLimitRange(160, 160);
      this.codeEditor.setOption('printMarginColumn', 160);
      return;
    }
    this.split.setSplits(2);
    this.codeEditor.getSession().setWrapLimitRange(80, 80);
    this.codeEditor.setOption('printMarginColumn', 80);
    const documentationEditor = this.split.getEditor(1);
    documentationEditor.setReadOnly(true);
    documentationEditor.setOptions({
      showPrintMargin: false,
      tabSize: 2,
      useSoftTabs: true,
      readOnly: true,
      highlightActiveLine: false,
      highlightGutterLine: false,
    });
    documentationEditor.session.setValue(this.getDocumentation());
    documentationEditor.getSession().setUseWrapMode(true);
    documentationEditor.getSession().setWrapLimitRange(80, 80);
    documentationEditor.getSession().setMode(this.setMode());
  }

  private updateSplitSize(): void {
    this.split.resize();
  }

  private initializeResizeSensor(): void {
    this._resizeSensor = new CssElementQuery.ResizeSensor(this.codeEditorElmRef.nativeElement, () =>
      this.updateSplitSize(),
    );
  }
}
