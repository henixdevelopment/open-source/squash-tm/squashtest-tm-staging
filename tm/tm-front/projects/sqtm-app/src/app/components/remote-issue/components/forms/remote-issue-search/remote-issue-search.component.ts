import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output,
  QueryList,
  signal,
  ViewChildren,
  WritableSignal,
} from '@angular/core';
import {
  Field,
  KeyNames,
  RemoteIssue,
  RemoteIssueSearchForm,
  RemoteIssueSearchTerms,
} from 'sqtm-core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  findInitialValue,
  isFieldGroupField,
  isSelectorField,
} from '../new-advanced-issue-form/new-advanced-issue-form.model';
import { RemoteIssueWidgetComponent } from '../../widgets/remote-issue-widget/remote-issue-widget.component';
import { RemoteIssueService } from '../../../services/remote-issue.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CommonConfigurationKey } from '../new-advanced-issue-form/common-configuration-keys';

@Component({
  selector: 'sqtm-app-remote-issue-search',
  templateUrl: './remote-issue-search.component.html',
  styleUrls: [
    './remote-issue-search.component.less',
    '../../../styles/remote-issue-dialog-commons.less',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RemoteIssueSearchComponent implements OnInit {
  loading: boolean;

  @Input()
  errorMessage: string;

  @Input()
  set searchForm(searchForm: RemoteIssueSearchForm) {
    this._searchForm = searchForm;
    this.initializeFormGroup();
  }

  get searchForm(): RemoteIssueSearchForm {
    return this._searchForm;
  }

  @Input() foundRemoteIssue: RemoteIssue | undefined;

  @Output()
  searchRequested = new EventEmitter<RemoteIssueSearchTerms>();

  @ViewChildren(RemoteIssueWidgetComponent)
  remoteIssueWidgets: QueryList<RemoteIssueWidgetComponent>;

  $shouldDisplaySearchButton: WritableSignal<boolean> = signal(true);

  private unsub$ = new Subject<void>();

  formGroup: FormGroup;

  private _searchForm: RemoteIssueSearchForm;

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly fb: FormBuilder,
    private remoteIssueService: RemoteIssueService,
  ) {}

  ngOnInit() {
    this.deduceSearchButtonVisibility();
  }

  private deduceSearchButtonVisibility() {
    this.remoteIssueService.visibleFieldsChanged$.pipe(takeUntil(this.unsub$)).subscribe(() => {
      if (this.searchForm.fields == null) {
        this.$shouldDisplaySearchButton.set(true);
        return;
      }

      const anyFieldHasDirectSearchFieldKey = this.anyFieldHasDirectSearchFieldKey(
        this.searchForm.fields,
      );

      this.$shouldDisplaySearchButton.set(!anyFieldHasDirectSearchFieldKey);
    });
  }

  private anyFieldHasDirectSearchFieldKey(fields: Field[]): boolean {
    for (const subfield of fields) {
      const subfieldHasDirectSearchFieldKey = this.hasDirectSearchFieldKey(subfield);
      if (subfieldHasDirectSearchFieldKey) {
        return true;
      }
    }
    return false;
  }

  private hasDirectSearchFieldKey(field: Field): boolean {
    const hasDirectSearchFieldKey =
      field.rendering.inputType.configuration[CommonConfigurationKey.DIRECT_SEARCH_FIELD_KEY];

    if (hasDirectSearchFieldKey) {
      return true;
    }

    if (isSelectorField(field)) {
      const selectedFieldId = this.formGroup.controls[field.id].value;
      const selectedField = field.fields.find((subfield) => subfield.id === selectedFieldId);

      if (this.hasDirectSearchFieldKey(selectedField)) {
        return true;
      }
    }

    if (isFieldGroupField(field)) {
      if (this.anyFieldHasDirectSearchFieldKey(field.fields)) {
        return true;
      }
    }

    return false;
  }

  searchForIssue(): void {
    if (!this.formGroup.valid) {
      this.showValidationErrors();
    } else {
      this.loading = true;
      this.searchRequested.emit(this.getRemoteIssueSearchTerms());
      this.showValidationErrors();
    }
  }

  private getRemoteIssueSearchTerms(): RemoteIssueSearchTerms {
    return this.formGroup.value;
  }

  endAsync(): void {
    this.loading = false;
    this.cdr.detectChanges();
  }

  private initializeFormGroup(): void {
    if (this.searchForm == null) {
      this.formGroup = this.fb.group({});
    } else {
      const controls = {};

      this.searchForm.fields.forEach((field) => {
        this.appendFieldToFormGroup(field, controls);
      });

      this.formGroup = this.fb.group(controls);
    }
  }

  @HostListener('window:keyup', ['$event'])
  handleKeyUp(event: KeyboardEvent) {
    if (
      event.key === KeyNames.ENTER &&
      !this.anyFieldHasDirectSearchFieldKey(this.searchForm.fields)
    ) {
      this.searchForIssue();
    }
  }

  private appendFieldToFormGroup(field: Field, controls: any) {
    const initialValue = findInitialValue(field);
    const required = field.rendering.required;
    controls[field.id] = this.fb.control(initialValue, required ? Validators.required : null);
  }

  private showValidationErrors() {
    this.remoteIssueWidgets.forEach((widget) => widget.showClientSideError());
  }

  isTooltipDisplayed(field: Field) {
    const controls = this.formGroup['controls'];
    return (
      Object.keys(controls).includes(field.id) &&
      Object.keys(controls).includes(
        field.rendering.inputType.configuration[
          CommonConfigurationKey.FIELD_LINKED_TO_HELP_MESSAGE
        ],
      )
    );
  }
}
