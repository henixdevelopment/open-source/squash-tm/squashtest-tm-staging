export enum CommonConfigurationKey {
  FORMAT = 'format',
  TIME_FORMAT = 'time-format',
  ONCHANGE = 'onchange',
  MAX_LENGTH = 'max-length',
  CONFIRM_MESSAGE = 'confirmMessage',
  HELP_MESSAGE = 'help-message',
  FIELD_LINKED_TO_HELP_MESSAGE = 'field_linked_to_help_message',
  FILTER_POSSIBLE_VALUES = 'filter-possible-values',
  RENDER_AS_HTML = 'render-as-html',
  DIRECT_SEARCH_FIELD_KEY = 'direct-search-field-key',
}
