import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { AbstractFormField, DisplayOption, Field, FieldValidationError } from 'sqtm-core';
import { FormGroup } from '@angular/forms';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'sqtm-app-remote-combo-box-field',
  templateUrl: './remote-combo-box-field.component.html',
  styleUrls: ['./remote-combo-box-field.component.less'],
  providers: [
    {
      provide: AbstractFormField,
      useExisting: RemoteComboBoxFieldComponent,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RemoteComboBoxFieldComponent extends AbstractFormField implements OnInit {
  @Input() field: Field;
  @Input() formGroup: FormGroup;
  @Input() options: DisplayOption[];
  @ViewChild('textInput') textInput: ElementRef;

  private autocompleteOptions$: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);

  get fieldName(): string {
    return this.field.id;
  }

  // Inherited but not used for now
  serverSideFieldValidationError: FieldValidationError[];

  public constructor(cdRef: ChangeDetectorRef) {
    super(cdRef);
  }

  private get currentValue(): string {
    return this.formGroup.controls[this.field.id].value;
  }

  ngOnInit() {
    this.formGroup.get(this.field.id).valueChanges.subscribe((value) => {
      this.updateAutocompleteOptions(value);
    });
  }

  private updateAutocompleteOptions(value: string): void {
    if (!Array.isArray(this.options)) {
      this.autocompleteOptions$.next([]);
      return;
    }

    const filteredOptions: string[] = this.options
      .map((option: DisplayOption) => option.label)
      .filter((label: string) => label.includes(value));

    this.autocompleteOptions$.next(filteredOptions);
  }

  getAutocompleteOptions(): Observable<string[]> {
    return this.autocompleteOptions$.asObservable();
  }

  resetValue() {
    this.formControl.setValue('');
    this.textInput.nativeElement.focus();
  }
}
