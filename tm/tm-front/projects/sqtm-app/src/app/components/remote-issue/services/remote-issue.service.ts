import { Injectable } from '@angular/core';
import {
  AdvancedIssue,
  AdvancedProject,
  ChangeSet,
  createStore,
  doesHttpErrorContainsSquashActionError,
  extractSquashActionError,
  Field,
  FieldValue,
  isAdvancedIssue,
  isBTIssue,
  isChangeSet,
  IssueBindableEntity,
  ReferentialDataService,
  RemoteIssue,
  RemoteIssueSearchForm,
  RemoteIssueSearchTerms,
  RestService,
  Store,
} from 'sqtm-core';
import { BehaviorSubject, Observable, of, Subject, throwError } from 'rxjs';
import {
  catchError,
  concatMap,
  distinctUntilChanged,
  filter,
  finalize,
  map,
  switchMap,
  take,
  tap,
} from 'rxjs/operators';
import { BugTrackerInfo, RemoteIssueDialogState } from '../states/remote-issue-dialog.state';
import { RemoteIssueDialogData } from '../containers/remote-issue-dialog/remote-issue-dialog.component';

/**
 * Fetch remote issue creation form models and submit new remote issues.
 *
 * There are two workflows possible :
 * - Creation mode where you call loadInitialState to fetch remote infos related to the squash project, eventually
 *    call switchToRemoteProject, and complete the use case with submitIssue and submitAttachments.
 * - Attach mode where you call loadInitialState, search existing issues with searchForIssues and complete
 *    the use case with attachToExistingIssue.
 *
 * These workflows are not dispatched into different services because we may one day be able to switch from one workflow
 * to another within the same dialog as SQTM 1.x allowed it.
 */
@Injectable()
export class RemoteIssueService {
  public readonly store: Store<RemoteIssueDialogState>;
  public readonly state$: Observable<RemoteIssueDialogState>;
  public readonly reportForm$: Observable<RemoteIssue>;

  public readonly loading$: Observable<boolean>;
  public readonly searchResultLoaded$: Observable<boolean>;
  public readonly errorMessage$: Observable<string>;
  public readonly changeSet$: Observable<ChangeSet>;
  public readonly legacyDelegateCommandResponse$: Observable<any>;
  public readonly visibleFieldsChanged$: Observable<void>;
  public readonly autocompleteSearchLoading$: Observable<boolean>;

  private readonly _loading = new BehaviorSubject<boolean>(false);
  private readonly _searchResultLoaded = new BehaviorSubject<boolean>(false);
  private readonly _errorMessage = new BehaviorSubject<string>(null);
  private readonly _changeSet = new Subject<ChangeSet>();
  private readonly _legacyDelegateCommandResponse: Subject<any> = new Subject<any>();
  private readonly _visibleFieldsChanged: Subject<void> = new Subject<void>();
  private readonly _autocompleteSearchLoading: Subject<boolean> = new Subject<boolean>();

  constructor(
    public readonly restService: RestService,
    public readonly referentialDataService: ReferentialDataService,
  ) {
    this.loading$ = this._loading.pipe(distinctUntilChanged());
    this.searchResultLoaded$ = this._searchResultLoaded.pipe(distinctUntilChanged());
    this.errorMessage$ = this._errorMessage.pipe(distinctUntilChanged());
    this.changeSet$ = this._changeSet.asObservable();
    this.legacyDelegateCommandResponse$ = this._legacyDelegateCommandResponse.asObservable();
    this.visibleFieldsChanged$ = this._visibleFieldsChanged.asObservable();
    this.autocompleteSearchLoading$ = this._autocompleteSearchLoading.asObservable();

    this.store = createStore<RemoteIssueDialogState>(getInitialRemoteIssueDialogState());
    this.state$ = this.store.state$;
    this.reportForm$ = this.store.state$.pipe(map((state) => state.reportForm));
  }

  loadInitialState(data: RemoteIssueDialogData): Observable<any> {
    const { bindableEntity, boundEntityId, attachMode, bugTrackerId, squashProjectId, extenderId } =
      data;

    this._loading.next(true);

    return this.store.state$.pipe(
      take(1),
      map(
        (state: RemoteIssueDialogState) =>
          ({
            ...state,
            boundEntity: { bindableEntity, boundEntityId },
            bugTrackerId,
            attachMode,
            squashProjectId,
            extenderId,
          }) as RemoteIssueDialogState,
      ),
      switchMap((state: RemoteIssueDialogState) => this.fetchIssueSearchFormIfNeeded(state)),
      switchMap((state: RemoteIssueDialogState) => this.fetchBugTrackerInfo(state)),
      switchMap((state: RemoteIssueDialogState) => {
        if (!data.attachMode || isOslcBugTrackerKind(state.bugTrackerInfo.kind)) {
          return this.loadForProject(state.remoteProjectName, state, state.extenderId).pipe(
            catchError(() => of(state)),
          );
        } else {
          return of(state);
        }
      }),
      tap((state: RemoteIssueDialogState) => this.store.commit(state)),
      finalize(() => this._loading.next(false)),
    );
  }

  switchToRemoteProjectAndCommit(remoteProjectName: string): Observable<RemoteIssueDialogState> {
    this._loading.next(true);

    return this.state$.pipe(
      take(1),
      map((state) => this.withRemoteIssue(null, state)),
      tap((state) => this.store.commit(state)),
      concatMap((state) => this.loadForProject(remoteProjectName, state)),
      tap((newState) => this.store.commit(newState)),
      finalize(() => this._loading.next(false)),
    );
  }

  changeSchemeAndCommit(field: Field, newValue: string): Observable<any> {
    const newSchemeName = field.id + ':' + newValue;

    return this.store.state$.pipe(
      take(1),
      filter((state: RemoteIssueDialogState) =>
        RemoteIssueService.isSchemeNameDifferent(state, newSchemeName),
      ),
      filter((state: RemoteIssueDialogState) =>
        RemoteIssueService.isSchemeNameSupported(state, newSchemeName),
      ),
      map((state: RemoteIssueDialogState) => ({
        ...state,
        reportForm: {
          ...state.reportForm,
          currentScheme: newSchemeName,
        },
      })),
      tap((newState) => this.store.commit(newState)),
    );
  }

  // Send a command to the remote bugtracker. Used for fields with auto-completion.
  sendDelegateCommand<R>(command: string, argument: any, formGroupValue: any): Observable<R> {
    return this.state$.pipe(
      take(1),
      tap(() => this._autocompleteSearchLoading.next(true)),
      switchMap((state: RemoteIssueDialogState) => {
        const context = {
          remoteProject: state.remoteProjectName,
          reportForm: state.reportForm,
          remoteIssueCurrentFormRequest: formGroupValue,
          boundEntityInfo: state.boundEntity,
        };
        const requestBody = { command, argument, context };

        const url = ['issues', state.bugTrackerInfo.bugTrackerName, 'command'];
        return this.restService.post<R>(url, requestBody) as Observable<R>;
      }),
      tap((response) => this.processChangeSetOrEmitLegacyResponse(response)),
      tap(() => this._autocompleteSearchLoading.next(false)),
      catchError((err) => {
        this._autocompleteSearchLoading.next(false);
        throw err;
      }),
    );
  }

  submitIssue(remoteIssue: RemoteIssue): Observable<SubmitIssueResponse> {
    return this.state$.pipe(
      take(1),
      tap((a) => console.log(a)),
      switchMap((state: RemoteIssueDialogState) => {
        const { bindableEntity, boundEntityId } = state.boundEntity;
        const url = [
          'issues',
          getEntityUrlPart(bindableEntity),
          boundEntityId.toString(),
          getPostUrlPart(remoteIssue),
        ];
        if (state.extenderId) {
          const params = {
            ...(state.extenderId && { 'extender-id': state.extenderId.toString() }),
          };
          return this.restService.post<SubmitIssueResponse>(url, remoteIssue, {
            params,
          }) as Observable<SubmitIssueResponse>;
        } else {
          return this.restService.post<SubmitIssueResponse>(
            url,
            remoteIssue,
          ) as Observable<SubmitIssueResponse>;
        }
      }),
      catchError((error) => this.handleServerSideError(error)),
    );
  }

  submitOslcIssue(issueId: string): Observable<any> {
    return this.state$.pipe(
      take(1),
      switchMap((state: RemoteIssueDialogState) => {
        const { bindableEntity, boundEntityId } = state.boundEntity;
        const url = [
          'issues',
          getEntityUrlPart(bindableEntity),
          boundEntityId.toString(),
          'new-oslc-issue',
        ];
        if (state.extenderId) {
          const params = {
            ...(state.extenderId && { 'extender-id': state.extenderId.toString() }),
          };
          return this.restService.post(url, { issueId }, { params });
        } else {
          return this.restService.post(url, { issueId });
        }
      }),
    );
  }

  submitAttachments(files: any[], issueId: string): Observable<any> {
    if (!Array.isArray(files) || files.length === 0) {
      return of(null);
    }

    return this.state$.pipe(
      take(1),
      switchMap((state) => this.postAttachments(files, state, issueId)),
      catchError((error) => this.handleAttachmentUploadError(error)),
    );
  }

  private postAttachments(files: any[], state: RemoteIssueDialogState, issueId: string) {
    const formData = new FormData();
    files.forEach((file) => formData.append('attachment[]', file, file.name));
    const url = ['issues', state.reportForm.bugtracker, issueId, 'attachments'];
    return this.restService.post(url, formData);
  }

  private handleAttachmentUploadError(httpError: any): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      map((state) => ({
        ...state,
        hasAttachmentUploadError: true,
      })),
      tap((nextState) => this.store.commit(nextState)),
      switchMap(() => this.handleServerSideError(httpError)),
    );
  }

  getRemoteIssueSearchForm(
    previousState: RemoteIssueDialogState,
  ): Observable<RemoteIssueDialogState> {
    const urlParts = ['issues', 'issue-search-form'];
    const body = {
      bugTrackerId: previousState.bugTrackerId,
      projectId: previousState.squashProjectId,
    };
    return this.restService
      .post<RemoteIssueSearchForm>(urlParts, body)
      .pipe(map((searchForm) => ({ ...previousState, searchForm })));
  }

  searchForIssue(searchTerms: RemoteIssueSearchTerms): Observable<RemoteIssue> {
    this.clearRemoteIssueAndCommit();
    this._searchResultLoaded.next(false);

    return this.state$.pipe(
      take(1),
      switchMap((state) => this.doSearchForIssue(searchTerms, state)),
      finalize(() => this._searchResultLoaded.next(true)),
    );
  }

  private doSearchForIssue(
    values: RemoteIssueSearchTerms,
    previousState: RemoteIssueDialogState,
  ): Observable<any> {
    this.clearRemoteIssueAndCommit();

    const url = ['issues', 'search-issue'];
    return this.restService
      .post<RemoteIssue>(url, {
        bugTrackerId: previousState.bugTrackerId,
        values,
      })
      .pipe(
        map((foundIssue) => ({
          ...this.withRemoteIssue(foundIssue, previousState),
          remoteProjectName: foundIssue?.project.name,
        })),
        tap((newState) => this.store.commit(newState)),
      );
  }

  attachToExistingIssue(): Observable<any> {
    return this.state$.pipe(
      take(1),
      switchMap((state: RemoteIssueDialogState) => {
        const { bindableEntity, boundEntityId } = state.boundEntity;
        const url = [
          'issues',
          getEntityUrlPart(bindableEntity),
          boundEntityId.toString(),
          getPostUrlPart(state.reportForm),
        ];
        if (state.extenderId) {
          const params = {
            ...(state.extenderId && { 'extender-id': state.extenderId.toString() }),
          };
          return this.restService.post<SubmitIssueResponse>(url, state.reportForm, {
            params,
          }) as Observable<SubmitIssueResponse>;
        } else {
          return this.restService.post<SubmitIssueResponse>(
            url,
            state.reportForm,
          ) as Observable<SubmitIssueResponse>;
        }
      }),
    );
  }

  complete(): void {
    this.store.complete();
    this._loading.complete();
    this._searchResultLoaded.complete();
  }

  private clearRemoteIssueAndCommit(): void {
    this.state$
      .pipe(
        take(1),
        map((state) => this.withRemoteIssue(null, state)),
      )
      .subscribe((state) => this.store.commit(state));
  }

  private static isSchemeNameSupported(
    state: RemoteIssueDialogState,
    newSchemeName: string,
  ): boolean {
    return Object.keys((state.reportForm as AdvancedIssue)?.project.schemes).includes(
      newSchemeName,
    );
  }

  private static isSchemeNameDifferent(
    state: RemoteIssueDialogState,
    newSchemeName: string,
  ): boolean {
    return (state.reportForm as AdvancedIssue)?.currentScheme !== newSchemeName;
  }

  private loadForProject(
    remoteProjectName: string,
    state: RemoteIssueDialogState,
    extenderId?: number,
  ): Observable<RemoteIssueDialogState> {
    return this.fetchRemoteIssue(remoteProjectName, state, extenderId).pipe(
      catchError((err) => this.handleServerSideError(err)),
    );
  }

  private handleServerSideError(httpErrorResponse: any): Observable<any> {
    const userFriendlyMessage = extractErrorMessageFromBugTracker(httpErrorResponse);

    if (userFriendlyMessage) {
      this.emitError(userFriendlyMessage);
    } else {
      console.error(httpErrorResponse);
    }

    return throwError(() => httpErrorResponse);
  }

  private fetchBugTrackerInfo(
    previousState: RemoteIssueDialogState,
  ): Observable<RemoteIssueDialogState> {
    const url = ['issues/projects', previousState.squashProjectId.toString(), 'bugtracker'];

    return this.restService.get<BugTrackerInfo>(url).pipe(
      map((bugTrackerInfo) => ({
        ...previousState,
        bugTrackerInfo,
        remoteProjectName: bugTrackerInfo.projectNames[0],
      })),
    );
  }

  private fetchRemoteIssue(
    remoteProjectName: string,
    state: RemoteIssueDialogState,
    extenderId?: number,
  ): Observable<RemoteIssueDialogState> {
    const url = [
      'issues',
      getEntityUrlPart(state.boundEntity.bindableEntity),
      state.boundEntity.boundEntityId.toString(),
      'new-issue',
    ];
    const params = {
      'project-name': remoteProjectName,
      ...(extenderId && { 'extender-id': extenderId }),
    };
    return this.restService.get<RemoteIssue>(url, params).pipe(
      map((remoteIssue) => this.withRemoteIssue(remoteIssue, state)),
      map((updatedState) => ({ ...updatedState, remoteProjectName })),
    );
  }

  private withRemoteIssue(
    remoteIssue: RemoteIssue,
    state: RemoteIssueDialogState,
  ): RemoteIssueDialogState {
    sortRequiredFieldsOnTop(remoteIssue);
    return { ...state, reportForm: remoteIssue };
  }

  emitError(errorMessage: string): void {
    this._errorMessage.next(errorMessage);
  }

  clearError(): void {
    this._errorMessage.next(null);
  }

  private fetchIssueSearchFormIfNeeded(
    previousState: RemoteIssueDialogState,
  ): Observable<RemoteIssueDialogState> {
    if (previousState.attachMode) {
      return this.getRemoteIssueSearchForm(previousState);
    } else {
      return of(previousState);
    }
  }

  private processChangeSetOrEmitLegacyResponse(response: any): void {
    if (isChangeSet(response)) {
      this._changeSet.next(response);
    } else {
      this._legacyDelegateCommandResponse.next(response);
    }
  }

  replaceFieldPossibleValues(id: string, newPossibleValues: FieldValue[]) {
    this.updateFieldPossibleValues(id, () => newPossibleValues);
  }

  appendFieldPossibleValues(id: string, newPossibleValues: FieldValue[]) {
    this.updateFieldPossibleValues(id, (values) => [...values, ...newPossibleValues]);
  }

  private updateFieldPossibleValues(id: string, updater: (values: FieldValue[]) => FieldValue[]) {
    const state = this.store.getSnapshot();
    const advancedReportForm = state.reportForm as AdvancedIssue;
    const currentScheme = advancedReportForm?.currentScheme;

    if (currentScheme) {
      const field = advancedReportForm.project.schemes[currentScheme].find((f) => f.id === id);
      const newField = { ...field, possibleValues: updater(field?.possibleValues) };

      // Replace the field in the scheme
      const newScheme = advancedReportForm.project.schemes[currentScheme].map((f) =>
        f.id === id ? newField : f,
      );

      // Replace the scheme in the project
      const newSchemes = {
        ...advancedReportForm.project.schemes,
        [currentScheme]: newScheme,
      };

      this.store.commit({
        ...state,
        reportForm: {
          ...(state.reportForm as AdvancedIssue),
          project: {
            ...(state.reportForm.project as AdvancedProject),
            schemes: newSchemes,
          },
        } as AdvancedIssue,
      });
    }
  }

  triggerSelectedFieldChanged() {
    this._visibleFieldsChanged.next();
  }

  resetResultForm() {
    this._searchResultLoaded.next(false);
  }
}

function getInitialRemoteIssueDialogState(): RemoteIssueDialogState {
  return {
    squashProjectId: null,
    bugTrackerId: null,
    attachMode: false,
    boundEntity: null,
    reportForm: null,
    bugTrackerInfo: null,
    remoteProjectName: null,
    searchForm: null,
  };
}

function getEntityUrlPart(bindableEntity: IssueBindableEntity): string {
  switch (bindableEntity) {
    case 'EXECUTION_TYPE':
      return 'execution';
    case 'EXECUTION_STEP_TYPE':
      return 'execution-step';
    case 'SESSION_NOTE_TYPE':
      return 'session-note';
    case 'FAILURE_DETAIL_TYPE':
      return 'failure-detail';
    default:
      throw new Error('Unhandled entity type for issue binding ' + bindableEntity);
  }
}

function getPostUrlPart(remoteIssue: RemoteIssue): string {
  if (isBTIssue(remoteIssue)) {
    return 'new-issue';
  } else if (isAdvancedIssue(remoteIssue)) {
    return 'new-advanced-issue';
  } else {
    throw new Error('Issue type is not handled.');
  }
}

function sortRequiredFieldsOnTop(remoteIssue: RemoteIssue): void {
  if (isAdvancedIssue(remoteIssue)) {
    Object.values(remoteIssue.project.schemes).forEach((fields: Field[]) => {
      fields.sort(compareRequired);
    });
  }

  function compareRequired(a: Field, b: Field): number {
    const valA = a.rendering.required ? 0 : 1;
    const valB = b.rendering.required ? 0 : 1;
    return valA - valB;
  }
}

interface SubmitIssueResponse {
  issueId: string;
  url: string;
}

export function extractErrorMessageFromBugTracker(httpErrorResponse: any): string | undefined {
  if (doesHttpErrorContainsSquashActionError(httpErrorResponse)) {
    const actionError = extractSquashActionError(httpErrorResponse);
    return actionError.actionValidationError.i18nKey;
  } else {
    // We may receive old-school field validation errors with non exploitable field name ('bugtracker').
    return httpErrorResponse.error?.fieldValidationErrors?.[0]?.errorMessage;
  }
}

function isOslcBugTrackerKind(kind: string): boolean {
  return kind === 'rtc';
}
