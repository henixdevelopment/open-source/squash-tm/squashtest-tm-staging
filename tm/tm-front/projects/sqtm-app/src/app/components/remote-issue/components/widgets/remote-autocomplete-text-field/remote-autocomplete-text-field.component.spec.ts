import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoteAutocompleteTextFieldComponent } from './remote-autocomplete-text-field.component';
import { RemoteIssueService } from '../../../services/remote-issue.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { mockRemoteIssueService } from '../../../../../utils/testing-utils/mocks.service';
import SpyObj = jasmine.SpyObj;

describe('RemoteAutocompleteTextFieldComponent', () => {
  let component: RemoteAutocompleteTextFieldComponent;
  let fixture: ComponentFixture<RemoteAutocompleteTextFieldComponent>;
  let remoteIssueService: SpyObj<RemoteIssueService>;

  beforeEach(async () => {
    remoteIssueService = mockRemoteIssueService();

    await TestBed.configureTestingModule({
      declarations: [RemoteAutocompleteTextFieldComponent],
      providers: [{ provide: RemoteIssueService, useValue: remoteIssueService }],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoteAutocompleteTextFieldComponent);
    component = fixture.componentInstance;
    component.formGroup = new FormBuilder().group({ field: '' });
    component.field = {
      id: 'field',
      label: '',
      rendering: {
        inputType: {
          name: '',
          original: '',
          fieldSchemeSelector: false,
          dataType: '',
          configuration: {},
        },
        operations: [],
        required: false,
      },
      possibleValues: [],
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should reset field', () => {
    spyOn(component.textInput.nativeElement, 'focus');
    expect(component.showClearButton).toBeFalse();

    component.formGroup.controls[component.field.id].setValue('w');
    expect(component.showClearButton).toBeTrue();

    component.clearInputAndShowList();
    expect(component.showClearButton).toBeFalse();
    expect(component.textInput.nativeElement.focus).toHaveBeenCalled();
  });
});
