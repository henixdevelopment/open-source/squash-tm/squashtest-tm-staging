import { ChangeDetectionStrategy, Component, Input, OnInit, ViewChildren } from '@angular/core';
import { DisplayOption, Field, SelectorField } from 'sqtm-core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { distinctUntilChanged } from 'rxjs/operators';
import {
  findInitialValue,
  isCheckBoxField,
  isFieldGroupField,
} from '../../forms/new-advanced-issue-form/new-advanced-issue-form.model';
import { RemoteIssueWidgetComponent } from '../remote-issue-widget/remote-issue-widget.component';
import { RemoteIssueService } from '../../../services/remote-issue.service';

@Component({
  selector: 'sqtm-app-remote-selector-field',
  templateUrl: './remote-selector-field.component.html',
  styleUrl: './remote-selector-field.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RemoteSelectorFieldComponent implements OnInit {
  @Input({ transform: (value: Field) => value as SelectorField })
  selectorField: SelectorField;

  @Input()
  formGroup: FormGroup;

  @Input({ required: true })
  placeholder: string;

  @ViewChildren('widget')
  remoteIssueWidgets: RemoteIssueWidgetComponent[];

  displayOptions: DisplayOption[] = [];

  protected readonly isFieldGroupField = isFieldGroupField;
  protected readonly isCheckBoxField = isCheckBoxField;

  constructor(
    private fb: FormBuilder,
    private remoteIssueService: RemoteIssueService,
  ) {}

  ngOnInit() {
    this.selectorField.fields.forEach((field) => this.displayOptions.push(field));

    const control = this.formGroup.controls[this.selectorField.id];

    control.valueChanges.pipe(distinctUntilChanged()).subscribe(() => {
      this.updateForm();
      this.remoteIssueService.triggerSelectedFieldChanged();
    });

    if (!control.value) {
      const firstFieldId = this.selectorField.fields[0].id;
      control.setValue(firstFieldId);
    }

    this.updateForm();
  }

  private updateForm() {
    this.selectorField.fields.forEach((field) => this.formGroup.removeControl(field.id));
    this.appendFieldToFormGroup();
  }

  private appendFieldToFormGroup() {
    const selectedField = this.getSelectedField();
    this.registerField(selectedField);
  }

  private registerField(field: Field) {
    if (isFieldGroupField(field)) {
      field.fields.forEach((subField) => {
        this.registerField(subField);
      });
    } else {
      const initialValue = findInitialValue(field);
      const required = field.rendering.required;

      this.formGroup.setControl(
        field.id,
        this.fb.control(initialValue, required ? Validators.required : null),
      );
    }
  }

  getSelectedField(): Field {
    return this.selectorField.fields.find((field) => field.id === this.getSelectedFieldId());
  }

  getSelectedFieldId(): string {
    return this.formGroup.controls[this.selectorField.id].value;
  }

  showClientSideError(): void {
    this.remoteIssueWidgets.forEach((widget) => widget.showClientSideError());
  }

  getSubfieldCssClasses(subField: Field) {
    return isCheckBoxField(subField) ? 'm-b-10 flex' : 'm-b-10';
  }
}
