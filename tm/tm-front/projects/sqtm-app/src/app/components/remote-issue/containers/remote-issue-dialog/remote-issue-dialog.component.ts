import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostListener,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import {
  ActionErrorDisplayService,
  AdvancedIssueReportForm,
  DialogConfiguration,
  DialogReference,
  DialogService,
  Identifier,
  isAdvancedIssue,
  isOslcIssue,
  IssueBindableEntity,
  KeyNames,
  ReferentialDataService,
  RemoteIssueSearchForm,
  RemoteIssueSearchTerms,
} from 'sqtm-core';
import {
  extractErrorMessageFromBugTracker,
  RemoteIssueService,
} from '../../services/remote-issue.service';
import { catchError, filter, finalize, map, take, tap } from 'rxjs/operators';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { NewAdvancedIssueFormComponent } from '../../components/forms/new-advanced-issue-form/new-advanced-issue-form.component';
import { NewRemoteIssueFormComponent } from '../../components/forms/new-remote-issue-form/new-remote-issue-form.component';
import { HttpErrorResponse } from '@angular/common/http';
import { RemoteIssueSearchComponent } from '../../components/forms/remote-issue-search/remote-issue-search.component';
import { NzSelectOptionInterface } from 'ng-zorro-antd/select';
import { RemoteIssueDialogState } from '../../states/remote-issue-dialog.state';

@Component({
  selector: 'sqtm-app-remote-issue-dialog',
  templateUrl: './remote-issue-dialog.component.html',
  styleUrls: [
    './remote-issue-dialog.component.less',
    '../../styles/remote-issue-dialog-commons.less',
  ],
  providers: [
    {
      provide: RemoteIssueService,
      useClass: RemoteIssueService,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RemoteIssueDialogComponent implements OnInit, OnDestroy {
  data: RemoteIssueDialogData;
  readonly isAdvancedIssue$: Observable<boolean>;
  readonly isOslcIssue$: Observable<boolean>;

  errorMessage$: Observable<string>;
  searchForm$: Observable<RemoteIssueSearchForm>;
  searchErrorMessage$: Observable<string>;
  canConfirm$: Observable<boolean>;
  shouldShowCacheTip$: Observable<boolean>;
  shouldShowCacheWarning$: Observable<boolean>;

  @ViewChild('advancedForm')
  advancedForm: NewAdvancedIssueFormComponent;

  @ViewChild('btForm')
  btForm: NewRemoteIssueFormComponent;

  @ViewChild(RemoteIssueSearchComponent)
  issueSearchComponent: RemoteIssueSearchComponent;

  @ViewChild('submitButton', { read: ElementRef })
  submitButton: ElementRef<HTMLButtonElement>;

  private _searchErrorMessage = new BehaviorSubject<string>(null);

  currentRemoteProject: Identifier = null;
  remoteProjectOptions: NzSelectOptionInterface[] = [];

  constructor(
    public dialogReference: DialogReference<RemoteIssueDialogData>,
    public readonly remoteIssueService: RemoteIssueService,
    public readonly cdRef: ChangeDetectorRef,
    public readonly actionErrorDisplayService: ActionErrorDisplayService,
    public readonly dialogService: DialogService,
    private readonly referentialDataService: ReferentialDataService,
  ) {
    this.data = this.dialogReference.data;

    this.isAdvancedIssue$ = this.remoteIssueService.reportForm$.pipe(
      map((issue) => isAdvancedIssue(issue)),
    );
    this.isOslcIssue$ = this.remoteIssueService.reportForm$.pipe(
      map((issue) => isOslcIssue(issue)),
    );
    this.errorMessage$ = this.remoteIssueService.errorMessage$;
    this.searchErrorMessage$ = this._searchErrorMessage.asObservable();
    this.searchForm$ = remoteIssueService.state$.pipe(map((state) => state.searchForm));
    this.canConfirm$ = this.data.attachMode
      ? this.remoteIssueService.reportForm$.pipe(map((reportForm) => reportForm?.id != null))
      : this.remoteIssueService.reportForm$.pipe(map(Boolean));

    this.shouldShowCacheTip$ = this.referentialDataService.bugTrackers$.pipe(
      map((bugTrackers) => {
        if (this.data.attachMode) {
          return false;
        }

        const bugTracker = bugTrackers.find((bt) => bt.id === this.data.bugTrackerId);

        if (bugTracker == null) {
          return false;
        }

        return bugTracker.cacheAllowed && !bugTracker.cacheConfigured;
      }),
    );

    this.shouldShowCacheWarning$ = this.remoteIssueService.reportForm$.pipe(
      map((reportForm) => {
        if (reportForm == null) {
          return false;
        }

        return (reportForm as AdvancedIssueReportForm).hasCacheError;
      }),
    );

    this.subscribeToAttachmentUploadErrors();
  }

  ngOnInit(): void {
    this.remoteIssueService
      .loadInitialState(this.data)
      .subscribe((remoteIssueDialogState) =>
        this.prepareRemoteProjectOptions(remoteIssueDialogState),
      );
  }

  get titleKey(): string {
    return this.data.attachMode
      ? 'sqtm-core.campaign-workspace.dialog.title.attach-issue'
      : 'sqtm-core.campaign-workspace.dialog.title.report-new-issue';
  }

  ngOnDestroy(): void {
    this.remoteIssueService.complete();
  }

  @HostListener('window:dragover', ['$event'])
  preventDefaultFileDragOver($event: Event): void {
    $event.preventDefault();
  }

  @HostListener('window:drop', ['$event'])
  preventDefaultFileDrop($event: Event): void {
    $event.preventDefault();
  }

  handleConfirmation(): void {
    if (this.data.attachMode) {
      this.remoteIssueService.attachToExistingIssue().subscribe({
        next: () => this.handleSubmitSuccess(),
        error: (err) => this.handleAttachError(err),
      });
    } else {
      const formToUse = this.btForm || this.advancedForm;

      if (formToUse != null) {
        formToUse.submitForm().subscribe(() => this.handleSubmitSuccess());
      }
    }
  }

  handleAddAnother(): void {
    this.remoteIssueService.attachToExistingIssue().subscribe({
      next: () => {
        this.addAnotherIssue();
      },
      error: (err) => this.handleAttachError(err),
    });
  }

  private addAnotherIssue(): void {
    const controls = this.issueSearchComponent.formGroup['controls'];

    this.searchForm$
      .pipe(
        take(1),
        tap((searchForm) => {
          if (searchForm.fieldsToRetain == null) {
            for (const fieldId of Object.keys(controls)) {
              this.issueSearchComponent.formGroup.controls[fieldId].reset();
            }
          } else {
            for (const fieldId of Object.keys(controls)) {
              if (!searchForm.fieldsToRetain.includes(fieldId)) {
                this.issueSearchComponent.formGroup.controls[fieldId].reset();
              }
            }
          }
          this.remoteIssueService.resetResultForm();
          this.cdRef.markForCheck();
        }),
      )
      .subscribe(() => {
        this.dialogReference.result = true;
      });
  }

  searchForIssue(searchTerms: RemoteIssueSearchTerms): void {
    this._searchErrorMessage.next('');

    const formToUse = this.btForm || this.advancedForm;

    if (formToUse != null) {
      formToUse.clearForm();
    }

    this.remoteIssueService
      .searchForIssue(searchTerms)
      .pipe(
        catchError((err) => this.handleSearchError(err)),
        finalize(() => this.issueSearchComponent.endAsync()),
      )
      .subscribe(() => {
        this.focusOnSubmitButton();
      });
  }

  private handleSubmitSuccess(): void {
    this.dialogReference.result = true;
    this.dialogReference.close();
  }

  private handleSearchError(httpErrorResponse: HttpErrorResponse): Observable<any> {
    const message = extractErrorMessageFromBugTracker(httpErrorResponse) ?? httpErrorResponse.error;
    this._searchErrorMessage.next(message);
    return of(null);
  }

  errorOverlayClicked(): void {
    this.remoteIssueService.clearError();
  }

  closeDialog(): void {
    this.dialogReference.close();
  }

  handleCloseOslc(result: boolean): void {
    if (result) {
      this.handleSubmitSuccess();
    } else {
      // Close without marking success so that grid doesn't get refreshed
      this.dialogReference.close();
    }
  }

  private prepareRemoteProjectOptions(remoteIssueDialogState: RemoteIssueDialogState): void {
    this.remoteProjectOptions = remoteIssueDialogState.bugTrackerInfo.projectNames.map((name) => ({
      value: name,
      label: name,
    }));
    this.currentRemoteProject = remoteIssueDialogState.remoteProjectName;
  }

  handleProjectSelectChange($event: Identifier): void {
    this.remoteIssueService
      .switchToRemoteProjectAndCommit($event as any)
      .subscribe(() => (this.currentRemoteProject = $event));
  }

  private handleAttachError(err: any): void {
    this.actionErrorDisplayService.showActionError(err);
  }

  private subscribeToAttachmentUploadErrors(): void {
    this.remoteIssueService.state$
      .pipe(
        map((state) => state.hasAttachmentUploadError),
        filter(Boolean),
        tap(() => this.showAttachmentUploadErrorDialog()),
      )
      .subscribe();
  }

  private showAttachmentUploadErrorDialog(): void {
    this.dialogService
      .openAlert({
        level: 'WARNING',
        messageKey:
          'sqtm-core.campaign-workspace.remote-issue.report-dialog.attachment-upload-error.message',
      })
      .dialogClosed$.pipe(take(1))
      .subscribe(() => this.handleSubmitSuccess());
  }

  private focusOnSubmitButton() {
    // we need to wait for the submit button to be enabled
    setTimeout(() => this.submitButton.nativeElement.focus());
  }

  consumeConfirmationEvent($event: KeyboardEvent) {
    $event.preventDefault();
    $event.stopPropagation();
  }

  @HostListener('window:keyup', ['$event'])
  handleKeyUp(event: KeyboardEvent) {
    if (event.key === KeyNames.ESCAPE) {
      this.closeDialog();
    }
  }
}

export interface RemoteIssueDialogData {
  bugTrackerId: number;
  squashProjectId: Identifier;
  bindableEntity: IssueBindableEntity;
  boundEntityId: Identifier;
  attachMode: boolean;
  extenderId?: number;
}

export function getRemoteIssueDialogConfiguration(
  data: RemoteIssueDialogData,
  vcRef: ViewContainerRef,
): DialogConfiguration {
  return {
    component: RemoteIssueDialogComponent,
    id: 'remote-issue-dialog',
    viewContainerReference: vcRef,
    minWidth: 1100,
    maxWidth: 1300,
    minHeight: 620,
    maxHeight: '95%',
    data,
  };
}
