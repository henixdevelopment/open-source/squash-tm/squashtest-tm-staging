import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { RemoteIssueDialogState } from '../../../states/remote-issue-dialog.state';
import {
  BTIssue,
  BTProject,
  DisplayOption,
  isBTIssue,
  Option,
  RemoteAttribute,
  SelectFieldComponent,
  TextAreaFieldComponent,
  TextFieldComponent,
} from 'sqtm-core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RemoteIssueService } from '../../../services/remote-issue.service';
import { AbstractRemoteIssueForm, ServerSideErrorHandler } from '../abstract-remote-issue-form';
import { Observable, of } from 'rxjs';
import { RemoteAttachmentFieldComponent } from '../../widgets/remote-attachment-field/remote-attachment-field.component';

@Component({
  selector: 'sqtm-app-new-remote-issue-form',
  templateUrl: './new-remote-issue-form.component.html',
  styleUrls: [
    './new-remote-issue-form.component.less',
    '../../../styles/remote-issue-dialog-commons.less',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewRemoteIssueFormComponent extends AbstractRemoteIssueForm {
  formGroup: FormGroup;

  @ViewChildren(SelectFieldComponent)
  selectFields: QueryList<SelectFieldComponent>;

  @ViewChildren(TextFieldComponent)
  textFields: QueryList<TextFieldComponent>;

  @ViewChildren(TextAreaFieldComponent)
  textAreaFields: QueryList<TextAreaFieldComponent>;

  get widgetsWithErrorHandling(): Iterable<ServerSideErrorHandler> {
    return [...this.selectFields, ...this.textFields, ...this.textAreaFields];
  }

  get attachmentFields(): RemoteAttachmentFieldComponent[] {
    return [];
  }

  @Input()
  set remoteIssueDialogState(state: RemoteIssueDialogState) {
    this._remoteIssueDialogState = state;

    if (!isBTIssue(state.reportForm)) {
      throw new Error('Unexpected issue type. This component may not work as intended.');
    }

    this.buildForm();
  }

  get remoteIssueDialogState(): RemoteIssueDialogState {
    return this._remoteIssueDialogState;
  }

  categoryOptions: DisplayOption[] = [];
  severityOptions: DisplayOption[] = [];
  versionOptions: DisplayOption[] = [];
  assigneeOptions: DisplayOption[] = [];

  private _remoteIssueDialogState: RemoteIssueDialogState;

  constructor(
    public readonly fb: FormBuilder,
    public readonly remoteIssueService: RemoteIssueService,
    public readonly cdRef: ChangeDetectorRef,
  ) {
    super(remoteIssueService, cdRef, fb);
  }

  get btProject(): BTProject {
    return this._remoteIssueDialogState.reportForm.project as BTProject;
  }

  buildForm(): void {
    this.initFormGroup();
    this.prepareCategoryOptions();
    this.prepareAssigneeOptions();
    this.prepareSeverityOptions();
    this.prepareVersionOptions();
    this.cdRef.detectChanges();
  }

  isSelectDisabled(options: (DisplayOption | Option)[]): boolean {
    return this._remoteIssueDialogState.attachMode || options == null || options.length < 2;
  }

  protected getNewRemoteIssue(): Observable<BTIssue> {
    const btIssue = this._remoteIssueDialogState.reportForm as BTIssue;

    btIssue.summary = this.formGroup.controls['summary'].value;
    btIssue.description = this.formGroup.controls['description'].value;
    btIssue.comment = this.formGroup.controls['comment'].value;
    btIssue.priority = this.findFieldValueAmongPriorities(
      this.formGroup.controls['severity'].value,
    );
    btIssue.version = this.findFieldValueAmongVersions(this.formGroup.controls['version'].value);
    btIssue.category = this.findFieldValueAmongCategories(
      this.formGroup.controls['category'].value,
    );
    btIssue.assignee = this.findFieldValueAmongUsers(this.formGroup.controls['assignee'].value);

    return of(btIssue);
  }

  private prepareCategoryOptions(): void {
    this.categoryOptions = this.extractAsDisplayOptions('categories');
    const defaultValue =
      this._remoteIssueDialogState.reportForm.category?.id || this.categoryOptions[0]?.id;
    this.formGroup.get('category').setValue(defaultValue);
  }

  private prepareSeverityOptions(): void {
    this.severityOptions = this.extractAsDisplayOptions('priorities');
    const defaultValue =
      this._remoteIssueDialogState.reportForm.priority?.id ||
      this.btProject.defaultIssuePriority.id;
    this.formGroup.get('severity').setValue(defaultValue);
  }

  private prepareAssigneeOptions(): void {
    this.assigneeOptions = this.extractAsDisplayOptions('users');
    const defaultValue =
      this._remoteIssueDialogState.reportForm.assignee?.id || this.assigneeOptions[0]?.id;
    this.formGroup.get('assignee').setValue(defaultValue);
  }

  private prepareVersionOptions(): void {
    this.versionOptions = this.extractAsDisplayOptions('versions');
    const defaultValue =
      this._remoteIssueDialogState.reportForm.version?.id || this.versionOptions[0]?.id;
    this.formGroup.get('version').setValue(defaultValue);
  }

  private findFieldValueAmongCategories(formValue: string): RemoteAttribute {
    return this.btProject.categories.find((category) => category.id === formValue);
  }

  private findFieldValueAmongPriorities(formValue: string): RemoteAttribute {
    return this.btProject.priorities.find((priority) => priority.id === formValue);
  }

  private findFieldValueAmongVersions(formValue: string): RemoteAttribute {
    return this.btProject.versions.find((version) => version.id === formValue);
  }

  private findFieldValueAmongUsers(formValue: string): RemoteAttribute {
    return this.btProject.users.find((user) => user.id === formValue);
  }

  private extractAsDisplayOptions(key: keyof BTProject): DisplayOption[] {
    const remoteArray = this.btProject[key];

    if (Array.isArray(remoteArray)) {
      return remoteArray.map((item) => ({
        id: item.id,
        label: item.name,
      }));
    }

    return [];
  }

  private initFormGroup(): void {
    const defaultProject = this._remoteIssueDialogState.reportForm.project?.id ?? '';
    const defaultCategory = this._remoteIssueDialogState.reportForm.category?.id ?? '';
    const defaultSummary = this._remoteIssueDialogState.reportForm.summary ?? '';
    const defaultComment = this._remoteIssueDialogState.reportForm.comment ?? '';
    const defaultSeverity = this._remoteIssueDialogState.reportForm.priority?.id ?? '';
    const defaultVersion = this._remoteIssueDialogState.reportForm.version?.id ?? '';
    const defaultAssignee = this._remoteIssueDialogState.reportForm.assignee?.id ?? '';
    const defaultDescription = this._remoteIssueDialogState.reportForm.description ?? '';

    const disabled = this._remoteIssueDialogState.attachMode;

    this.formGroup = this.fb.group({
      remoteProject: this.fb.control(defaultProject),
      category: this.fb.control(defaultCategory),
      summary: this.fb.control({ value: defaultSummary, disabled }, Validators.required),
      comment: this.fb.control({ value: defaultComment, disabled }),
      severity: this.fb.control(defaultSeverity),
      version: this.fb.control(defaultVersion),
      assignee: this.fb.control(defaultAssignee),
      description: this.fb.control({ value: defaultDescription, disabled }, Validators.required),
    });
  }

  errorOverlayClicked(): void {
    this.resetErrorMessage();
  }
}
