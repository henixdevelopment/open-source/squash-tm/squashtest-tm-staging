import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { AbstractFormField, ChangeSet, Field, FieldValidationError } from 'sqtm-core';
import { FormGroup } from '@angular/forms';
import { RemoteIssueService } from '../../../services/remote-issue.service';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {
  AutocompleteDataSourceItem,
  NzAutocompleteOptionComponent,
} from 'ng-zorro-antd/auto-complete';
import { CommonConfigurationKey } from '../../forms/new-advanced-issue-form/common-configuration-keys';

@Component({
  selector: 'sqtm-app-remote-autocomplete-text-field',
  templateUrl: './remote-autocomplete-text-field.component.html',
  styleUrls: ['./remote-autocomplete-text-field.component.less'],
  providers: [
    {
      provide: AbstractFormField,
      useExisting: RemoteAutocompleteTextFieldComponent,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RemoteAutocompleteTextFieldComponent extends AbstractFormField implements OnDestroy {
  @Input() field: Field;
  @Input() formGroup: FormGroup;
  @Input() size: 'small' | 'default' | 'large' = 'default';
  @Input() hasFocus: boolean;
  @Input() placeholder: string = null;

  // Inherited but unused for now
  serverSideFieldValidationError: FieldValidationError[];

  @ViewChild('textInput') set textInput(elem: ElementRef<HTMLInputElement>) {
    this._textInput = elem;

    if (elem != null && this.hasFocus) {
      elem.nativeElement.focus();
    }
  }

  get fieldName(): string {
    return this.field?.id;
  }

  get textInput(): ElementRef {
    return this._textInput;
  }

  private _textInput: ElementRef;
  private unsub$ = new Subject<void>();

  visibleOptionLabels: AutocompleteDataSourceItem[] = [];
  isLoading$: Observable<boolean>;

  constructor(
    public readonly remoteIssueService: RemoteIssueService,
    cdr: ChangeDetectorRef,
  ) {
    super(cdr);

    this.observeChangeSets();
    this.observeLegacyDelegateCommand();
    this.isLoading$ = this.remoteIssueService.autocompleteSearchLoading$;
  }

  get command(): string {
    return this.field?.rendering.inputType.configuration?.onchange;
  }

  get showSearchIcon(): boolean {
    return this.command != null;
  }

  get showClearButton(): boolean {
    return Boolean(this.formControl?.value);
  }

  get isNumeric(): boolean {
    return this.field?.rendering.inputType.dataType === 'number';
  }

  get inputType(): string {
    return this.isNumeric ? 'number' : 'text';
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  processLegacyDelegateCommandResponse(response: any): void {
    if (response != null) {
      // This is the legacy implementation for auto-completion : the connector responds with a Field whose field values
      // are the one used to auto-completion (e.g. Jira Server and Jira Cloud connectors).
      // When the server responds with a ChangeSet object, result is null.
      this.visibleOptionLabels =
        response.composite?.map((v) => ({
          value: v.name,
          label: v.name,
        })) ?? [];
      this.cdr.detectChanges();
    }
  }

  clearInputAndShowList(): void {
    if (this.formControl) {
      this.formControl.setValue('');
    }

    if (this.textInput) {
      this.textInput.nativeElement.value = '';
      this.forceRefreshAutocompleteOptions();
    }
  }

  private forceRefreshAutocompleteOptions(): void {
    this.textInput?.nativeElement.focus();
  }

  private observeChangeSets(): void {
    this.remoteIssueService.changeSet$
      .pipe(takeUntil(this.unsub$))
      .subscribe((changeSet) => this.processChangeSet(changeSet));
  }

  private observeLegacyDelegateCommand(): void {
    this.remoteIssueService.legacyDelegateCommandResponse$
      .pipe(takeUntil(this.unsub$))
      .subscribe((changeSet) => this.processLegacyDelegateCommandResponse(changeSet));
  }

  private processChangeSet(changeSet: ChangeSet): void {
    changeSet.changes.forEach((change) => {
      if (change.fieldId === this.field.id) {
        if (change.newValue) {
          this.formGroup.controls[this.field.id].setValue(change.newValue.scalar);
        }

        if (change.newPossibleValues) {
          this.visibleOptionLabels = [];
          change.newPossibleValues.forEach((value) =>
            this.visibleOptionLabels.push({
              value: value.id,
              label: value.autocompleteLabel ?? value.scalar,
            }),
          );
          this.cdr.detectChanges();
        }
      }
    });
  }

  onSelectOption(selectedOption: NzAutocompleteOptionComponent) {
    const directSearchKey =
      this.field.rendering.inputType.configuration[CommonConfigurationKey.DIRECT_SEARCH_FIELD_KEY];

    if (directSearchKey) {
      const searchTerms = {
        ...this.formGroup.value,
        [directSearchKey]: selectedOption.nzValue,
      };
      this.remoteIssueService.searchForIssue(searchTerms).subscribe();
    }
  }

  resetAutocompleteOptions() {
    this.visibleOptionLabels = [];
    this.cdr.detectChanges();
  }
}
