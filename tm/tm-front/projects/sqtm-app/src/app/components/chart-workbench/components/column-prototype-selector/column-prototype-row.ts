import { DataRow } from 'sqtm-core';

export enum ColumnPrototypeSelectorRowType {
  Entity = 'Entity',
  ColumnPrototype = 'ColumnPrototype',
}

export class EntityRow extends DataRow {
  readonly type = ColumnPrototypeSelectorRowType.Entity;
  allowMoves = false;
  allowedChildren = [ColumnPrototypeRow];
}

export class ColumnPrototypeRow extends DataRow {
  id: string;
  readonly type = ColumnPrototypeSelectorRowType.ColumnPrototype;
  allowMoves = true;
  allowedChildren = [];
}

export const ENTITY_KEY = 'ENTITY';
