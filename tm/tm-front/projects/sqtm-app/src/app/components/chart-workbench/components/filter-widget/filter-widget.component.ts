import { ChangeDetectionStrategy, Component, ViewContainerRef } from '@angular/core';
import { AbstractFilterField } from 'sqtm-core';
import { Overlay } from '@angular/cdk/overlay';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-filter-widget',
  templateUrl: './filter-widget.component.html',
  styleUrls: ['./filter-widget.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterWidgetComponent extends AbstractFilterField {
  constructor(
    protected overlay: Overlay,
    protected translateService: TranslateService,
    protected vcr: ViewContainerRef,
  ) {
    super(overlay, translateService, vcr);
  }
}
