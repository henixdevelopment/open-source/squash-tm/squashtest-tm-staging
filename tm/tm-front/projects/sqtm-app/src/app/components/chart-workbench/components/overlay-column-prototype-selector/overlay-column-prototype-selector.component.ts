import {
  ChangeDetectionStrategy,
  Component,
  InjectionToken,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import {
  column,
  CustomChartEntityType,
  Extendable,
  GridColumnId,
  GridDefinition,
  GridDefinitionBuilder,
  GridService,
  gridServiceFactory,
  GridType,
  Identifier,
  pagination,
  ReferentialDataService,
  RestService,
  StyleDefinitionBuilder,
} from 'sqtm-core';
import { ColumnProtoCellRendererComponent } from '../column-proto-cell-renderer/column-proto-cell-renderer.component';
import { Observable, Subject } from 'rxjs';
import { filter, map, takeUntil } from 'rxjs/operators';
import { EnhancedColumnPrototype } from '../../state/chart-workbench.state';

export const OVERLAY_COLUMN_PROTO_SELECTOR_GRID_CONFIG = new InjectionToken(
  'Token to inject column proto grid config',
);

export function overlayColumnPrototypeSelectorGridConfig(): GridDefinition {
  return new GridDefinitionBuilder('overlay-column-prototype-selector', GridType.TREE)
    .withColumns([
      column(GridColumnId.NAME)
        .enableDnd()
        .changeWidthCalculationStrategy(new Extendable(300))
        .withRenderer(ColumnProtoCellRendererComponent),
    ])
    .withRowHeight(30)
    .disableHeaders()
    .enableMultipleColumnsFiltering(['NAME'])
    .disableRightToolBar()
    .withStyle(
      new StyleDefinitionBuilder()
        .switchOffSelectedRows()
        .switchOffHoveredRows()
        .enableInitialLoadAnimation(),
    )
    .withPagination(pagination().inactive())
    .build();
}

@Component({
  selector: 'sqtm-app-overlay-column-prototype-selector',
  templateUrl: './overlay-column-prototype-selector.component.html',
  styleUrls: ['./overlay-column-prototype-selector.component.less'],
  providers: [
    {
      provide: OVERLAY_COLUMN_PROTO_SELECTOR_GRID_CONFIG,
      useFactory: overlayColumnPrototypeSelectorGridConfig,
    },
    {
      provide: GridService,
      useFactory: gridServiceFactory,
      deps: [RestService, OVERLAY_COLUMN_PROTO_SELECTOR_GRID_CONFIG, ReferentialDataService],
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OverlayColumnPrototypeSelectorComponent implements OnInit, OnDestroy {
  @Input()
  columnPrototypes: { [K in CustomChartEntityType]: EnhancedColumnPrototype[] };

  selectedPrototype$: Observable<string>;

  private unsub$ = new Subject<void>();

  constructor(private gridService: GridService) {}

  ngOnInit(): void {
    this.selectedPrototype$ = this.gridService.selectedRowIds$.pipe(
      takeUntil(this.unsub$),
      filter((ids: Identifier[]) => ids.length === 1),
      map((ids) => ids[0] as string),
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
    this.gridService.complete();
  }
}
