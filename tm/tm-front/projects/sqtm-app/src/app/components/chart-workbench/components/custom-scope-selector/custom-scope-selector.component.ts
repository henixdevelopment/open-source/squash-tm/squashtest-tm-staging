import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  ViewChild,
} from '@angular/core';
import {
  CampaignLimitedPickerComponent,
  ChartScopeType,
  DataRow,
  EntityReference,
  EntityRowReference,
  EntityType,
  RequirementPickerComponent,
  SquashTmDataRowType,
  TestCasePickerComponent,
  toEntityRowReference,
  TreeWithStatePersistence,
  Workspaces,
} from 'sqtm-core';
import { filter, take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { chartWorkbenchLogger } from '../../chart-workbench.logger';
import * as _ from 'lodash';
import { CompleteScope } from '../../state/chart-workbench.state';

const logger = chartWorkbenchLogger.compose('CustomScopeSelectorComponent');

const UNSELECTABLE_ROW_TYPES: SquashTmDataRowType[] = [
  SquashTmDataRowType.Sprint,
  SquashTmDataRowType.SprintGroup,
];

@Component({
  selector: 'sqtm-app-custom-scope-selector',
  templateUrl: './custom-scope-selector.component.html',
  styleUrls: ['./custom-scope-selector.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomScopeSelectorComponent implements OnDestroy {
  selectedIndex = 0;

  initiallySelectedRequirement: string[] = [];
  initiallySelectedTestCase: string[] = [];
  initiallySelectedCampaign: string[] = [];

  @Input()
  set initiallySelectedNodes(entityReferences: EntityReference[]) {
    if (entityReferences.length > 0) {
      const entityReference = entityReferences[0];
      this.selectedIndex = this.tabIndexByEntityType[entityReference.type] || 0;
      if (entityReference.type !== EntityType.PROJECT) {
        this.initiallySelectedRequirement = this.extractIds(entityReferences, 0);
        this.initiallySelectedTestCase = this.extractIds(entityReferences, 1);
        this.initiallySelectedCampaign = this.extractIds(entityReferences, 2);
      } else {
        this.initiallySelectedRequirement = [];
        this.initiallySelectedTestCase = [];
        this.initiallySelectedCampaign = [];
      }
    }
  }

  @Output()
  scopeChanged = new EventEmitter<CompleteScope>();

  @Output()
  closeRequired = new EventEmitter<void>();

  @ViewChild(RequirementPickerComponent)
  requirementPicker: RequirementPickerComponent;

  @ViewChild(TestCasePickerComponent)
  testCasePicker: TestCasePickerComponent;

  private _campaignPicker: CampaignLimitedPickerComponent;
  @ViewChild(CampaignLimitedPickerComponent)
  set campaignPicker(pickerElement: CampaignLimitedPickerComponent) {
    this._campaignPicker = pickerElement;

    if (pickerElement?.tree != null) {
      pickerElement.tree.selectedRows$
        .pipe(takeUntil(this.unsub$))
        .subscribe((rows) => this.filterUnselectableRowTypes(rows));
    }
  }

  get campaignPicker(): CampaignLimitedPickerComponent {
    return this._campaignPicker;
  }

  private filterUnselectableRowTypes(dataRows: DataRow[]): void {
    dataRows.forEach((row) => {
      if (UNSELECTABLE_ROW_TYPES.includes(row.type)) {
        this.campaignPicker.tree.toggleRowSelection(row.id);
      }
    });
  }

  private entityTypeByDataRowType: Partial<{ [K in SquashTmDataRowType]: EntityType }> = {
    [SquashTmDataRowType.RequirementLibrary]: EntityType.REQUIREMENT_LIBRARY,
    [SquashTmDataRowType.RequirementFolder]: EntityType.REQUIREMENT_FOLDER,
    [SquashTmDataRowType.Requirement]: EntityType.REQUIREMENT,
    [SquashTmDataRowType.HighLevelRequirement]: EntityType.HIGH_LEVEL_REQUIREMENT,
    [SquashTmDataRowType.TestCaseLibrary]: EntityType.TEST_CASE_LIBRARY,
    [SquashTmDataRowType.TestCaseFolder]: EntityType.TEST_CASE_FOLDER,
    [SquashTmDataRowType.TestCase]: EntityType.TEST_CASE,
    [SquashTmDataRowType.CampaignLibrary]: EntityType.CAMPAIGN_LIBRARY,
    [SquashTmDataRowType.CampaignFolder]: EntityType.CAMPAIGN_FOLDER,
    [SquashTmDataRowType.Campaign]: EntityType.CAMPAIGN,
    [SquashTmDataRowType.Sprint]: EntityType.SPRINT,
    [SquashTmDataRowType.SprintGroup]: EntityType.SPRINT_GROUP,
  };

  private dataRowTypeByEntityTpe: Partial<{ [K in EntityType]: SquashTmDataRowType }> = _.invert(
    this.entityTypeByDataRowType,
  );

  private tabIndexByEntityType: Partial<{ [K in EntityType]: number }> = {
    [EntityType.REQUIREMENT_LIBRARY]: 0,
    [EntityType.REQUIREMENT_FOLDER]: 0,
    [EntityType.REQUIREMENT]: 0,
    [EntityType.HIGH_LEVEL_REQUIREMENT]: 0,
    [EntityType.TEST_CASE_LIBRARY]: 1,
    [EntityType.TEST_CASE_FOLDER]: 1,
    [EntityType.TEST_CASE]: 1,
    [EntityType.CAMPAIGN_LIBRARY]: 2,
    [EntityType.CAMPAIGN_FOLDER]: 2,
    [EntityType.CAMPAIGN]: 2,
  };

  private workspaces = [
    Workspaces['requirement-workspace'],
    Workspaces['test-case-workspace'],
    Workspaces['campaign-workspace'],
  ];

  private unsub$ = new Subject<void>();
  private changedTab$ = new Subject<void>();

  get selectedWorkspace() {
    return this.workspaces[this.selectedIndex];
  }

  ngOnDestroy(): void {
    this.changedTab$.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  changeSelectedWorkspace(index: number) {
    this.selectedIndex = index;
  }

  handleConfirm(picker: TreeWithStatePersistence) {
    logger.debug('Retrieve selection for test cases : ', [picker]);
    if (picker) {
      // has to access directly to the tree as the picker EventEmitter can't give actual values, but just emit changes
      picker.tree.selectedRows$
        .pipe(
          takeUntil(this.unsub$),
          take(1),
          filter((rows: DataRow[]) => rows.length > 0),
        )
        .subscribe((rows: DataRow[]) => this.emitScope(rows));
    }
  }

  private emitScope(rows: DataRow[]) {
    const scope = this.convertToScope(rows);
    logger.debug('Emitting new selection for custom scope :', [scope]);
    this.scopeChanged.emit(scope);
  }

  private convertToScope(rows: DataRow[]): CompleteScope {
    return {
      scopeType: ChartScopeType.CUSTOM,
      scope: rows.map((row) => this.convertRowIntoEntityReference(row)),
      projectScope: this.extractProjectScope(rows),
    };
  }

  private convertRowIntoEntityReference(row) {
    const rowReference = toEntityRowReference(row);
    const entityType = this.entityTypeByDataRowType[rowReference.entityType];
    return new EntityReference(rowReference.id, entityType);
  }

  handleClose() {
    this.closeRequired.emit();
  }

  private extractIds(entityReferences: EntityReference[], index: number) {
    return entityReferences
      .filter((ref) => this.tabIndexByEntityType[ref.type] === index)
      .map((ref) => new EntityRowReference(ref.id, this.dataRowTypeByEntityTpe[ref.type]))
      .map((ref) => ref.asString());
  }

  private extractProjectScope(rows: DataRow[]): number[] {
    return _.uniq(rows.map((r) => r.projectId));
  }
}
