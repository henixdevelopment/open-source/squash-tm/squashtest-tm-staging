import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import {
  AbstractCellRendererComponent,
  CustomChartEntityType,
  DataRowOpenState,
  GridService,
} from 'sqtm-core';
import { Subject } from 'rxjs';
import {
  ColumnPrototypeSelectorRowType,
  ENTITY_KEY,
} from '../column-prototype-selector/column-prototype-row';

@Component({
  selector: 'sqtm-app-column-proto-cell-renderer',
  templateUrl: './column-proto-cell-renderer.component.html',
  styleUrls: ['./column-proto-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ColumnProtoCellRendererComponent
  extends AbstractCellRendererComponent
  implements OnDestroy
{
  // base width of 1 index increment, in pixels.
  readonly INDEX_BASE_WIDTH = 20;

  private unsub$ = new Subject<void>();

  @ViewChild('openable', { read: ElementRef })
  containerRef: ElementRef;

  get columnPrototypeBorderColor() {
    return 'var(--current-workspace-main-color)';
  }

  get depthArray(): boolean[] {
    return this.depthMap.slice(0, this.depth);
  }

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  toggleRow($event: MouseEvent) {
    $event.stopPropagation();
    if (this.row.state === DataRowOpenState.closed) {
      this.grid.openRow(this.row.id);
    } else if (this.row.state === DataRowOpenState.open) {
      this.grid.closeRow(this.row.id);
    }
  }

  get stateIcon() {
    if (this.row.state === DataRowOpenState.closed) {
      return 'plus';
    } else {
      return 'minus';
    }
  }

  get entityIcon() {
    const entityType: string = this.row.data[ENTITY_KEY];
    const iconName: string = entityType.toLowerCase().replace(/_/g, '-');
    return `sqtm-core-custom-report:${iconName}`;
  }

  get nodeName() {
    return this.row.data[this.columnDisplay.id];
  }

  get templateName(): string {
    let templateName;
    switch (this.row.type) {
      case ColumnPrototypeSelectorRowType.Entity:
        templateName = 'entity';
        break;
      case ColumnPrototypeSelectorRowType.ColumnPrototype:
        templateName = 'columnPrototype';
        break;
      default:
        throw Error(`Unable to find template for type ${this.row.type}`);
    }
    return templateName;
  }

  getWorkspace() {
    const entity = this.row.data[ENTITY_KEY] as CustomChartEntityType;
    switch (entity) {
      case CustomChartEntityType.REQUIREMENT:
      case CustomChartEntityType.REQUIREMENT_VERSION:
        return 'requirement-workspace';
      case CustomChartEntityType.TEST_CASE:
        return 'test-case-workspace';
      case CustomChartEntityType.CAMPAIGN:
      case CustomChartEntityType.ITERATION:
      case CustomChartEntityType.ITEM_TEST_PLAN:
      case CustomChartEntityType.EXECUTION:
        return 'campaign-workspace';
      default:
        throw Error(`Unable to find workspace theme for type ${entity}`);
    }
  }

  isCustomField() {
    return Boolean(this.row.data.cufId);
  }
}
