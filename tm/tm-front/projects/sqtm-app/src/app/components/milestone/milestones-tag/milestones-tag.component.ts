import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
} from '@angular/core';
import {
  DialogModule,
  DialogService,
  getSupportedBrowserLang,
  GridModule,
  Milestone,
  milestoneAllowModification,
  MultipleMilestonePickerDialogComponent,
  MultipleSelectMilestoneDialogConfiguration,
  SingleMilestonePickerDialogComponent,
  SingleSelectMilestoneDialogConfiguration,
  WorkspaceCommonModule,
} from 'sqtm-core';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CommonModule } from '@angular/common';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { FormsModule } from '@angular/forms';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

@Component({
  selector: 'sqtm-app-milestones-tag',
  templateUrl: './milestones-tag.component.html',
  styleUrls: ['./milestones-tag.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [
    CommonModule,
    GridModule,
    WorkspaceCommonModule,
    TranslateModule,
    NzTagModule,
    NzPopoverModule,
    NzButtonModule,
    NzIconModule,
    NzDividerModule,
    NzCheckboxModule,
    FormsModule,
    DialogModule,
    NzToolTipModule,
  ],
})
export class MilestonesTagComponent implements OnDestroy {
  @Input()
  milestones: Milestone[] = [];

  @Input()
  selectedMilestone: Milestone;

  @Input()
  possibleMilestones: Milestone[] = [];

  @Input()
  titleKey;

  @Output()
  deleteMilestone = new EventEmitter<number>();

  @Output()
  bindMilestones = new EventEmitter<number[]>();

  @Output()
  bindMilestone = new EventEmitter<Milestone>();

  @Input()
  editable = true;

  @Input()
  multiple = true;

  @Input()
  warningMessageKey: string;

  private unsub$ = new Subject<void>();

  constructor(
    public translateService: TranslateService,
    private dialogService: DialogService,
  ) {}

  openDialog() {
    if (this.possibleMilestones.length > 0) {
      const configuration = this.getConfiguration(this.multiple);
      const dialogReference = this.getMilestoneDialogReference(this.multiple, configuration);

      if (this.multiple) {
        dialogReference.dialogClosed$
          .pipe(takeUntil(this.unsub$))
          .subscribe((milestones: Milestone[]) => {
            if (milestones != null && milestones.length > 0) {
              this.bindMilestones.next(milestones.map((milestone) => milestone.id));
            }
          });
      } else {
        dialogReference.dialogClosed$
          .pipe(takeUntil(this.unsub$))
          .subscribe((milestone: Milestone) => {
            if (milestone != null) {
              this.bindMilestone.next(milestone);
            }
          });
      }
    } else {
      this.dialogService.openAlert({
        id: 'no-binding-dialog',
        level: 'WARNING',
        titleKey: 'sqtm-core.dialog.milestone.no-binding.title',
        messageKey: 'sqtm-core.dialog.milestone.no-binding.message',
      });
    }
  }

  getConfiguration(multiple: boolean): MilestoneDialogConfig {
    const configuration = {
      id: 'milestone-dialog',
      titleKey: this.titleKey,
      milestones: this.possibleMilestones,
      disableColumns: ['range'],
    };
    if (multiple) {
      return {
        ...configuration,
        selectedMilestones: [],
        warningMessageKey: this.warningMessageKey,
      };
    } else {
      return {
        ...configuration,
        selectedMilestone: this.selectedMilestone != null ? this.selectedMilestone.id : null,
      };
    }
  }

  getBrowserLanguage() {
    return getSupportedBrowserLang(this.translateService);
  }

  getMilestoneDialogReference(multiple: boolean, configuration: MilestoneDialogConfig) {
    const dialogReference = {
      id: 'milestone-entity-bound',
      data: configuration,
      height: 500,
      width: 800,
    };
    if (multiple) {
      return this.dialogService.openDialog({
        ...dialogReference,
        component: MultipleMilestonePickerDialogComponent,
      });
    } else {
      return this.dialogService.openDialog({
        ...dialogReference,
        component: SingleMilestonePickerDialogComponent,
      });
    }
  }

  removeMilestone(id) {
    this.deleteMilestone.emit(id);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  milestoneAllowModification(milestone: Milestone) {
    return this.editable && milestoneAllowModification(milestone);
  }
}

export type MilestoneDialogConfig =
  | MultipleSelectMilestoneDialogConfiguration
  | SingleSelectMilestoneDialogConfiguration;
