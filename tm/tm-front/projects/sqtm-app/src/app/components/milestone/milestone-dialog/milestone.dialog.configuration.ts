import { MilestoneView } from 'sqtm-core';

export interface MilestoneDialogConfiguration {
  id: string;
  titleKey: string;
  milestoneViews: MilestoneView[];
  checkedIds?: number[];
  samePerimeter: boolean;
}
