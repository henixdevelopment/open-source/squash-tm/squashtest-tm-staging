import { Logger, LoggerFactory } from 'sqtm-core';

export const sqtmAppLogger: Logger = LoggerFactory.getLogger('sqtm-app');
