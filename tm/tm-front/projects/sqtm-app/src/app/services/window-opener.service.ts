import { Injectable } from '@angular/core';
import { filter } from 'rxjs/operators';
import {
  InterWindowCommunicationService,
  InterWindowMessages,
  SquashPlatformNavigationService,
} from 'sqtm-core';

@Injectable()
export class WindowOpenerService {
  constructor(
    private interWindowCommunicationService: InterWindowCommunicationService,
    private platformNavigationService: SquashPlatformNavigationService,
  ) {}

  public initialize() {
    this.interWindowCommunicationService.interWindowMessages$
      .pipe(filter((message) => message.isTypeOf('REQUIRE-OPENING-NEW-WINDOW')))
      .subscribe((message) => this.platformNavigationService.openNewTab(message.payload.url));
  }
}

export class InterWindowOpenWindowMessage extends InterWindowMessages {
  constructor(url: string | string[]) {
    super('REQUIRE-OPENING-NEW-WINDOW', { url });
  }
}
