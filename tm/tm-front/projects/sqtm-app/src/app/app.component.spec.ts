import { TestBed, waitForAsync } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { RouterTestingModule } from '@angular/router/testing';
import { SvgModule } from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { AppTestingUtilsModule } from './utils/testing-utils/app-testing-utils.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { WindowOpenerService } from './services/window-opener.service';

describe('AppComponent', () => {
  const fakeTranslateService = jasmine.createSpyObj('TranslateService', [
    'setDefaultLang',
    'use',
    'getBrowserLang',
  ]);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, SvgModule, AppTestingUtilsModule, HttpClientTestingModule],
      declarations: [AppComponent],
      providers: [
        WindowOpenerService,
        { provide: TranslateService, useValue: fakeTranslateService },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'sqtm-app'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('sqtm-app');
  });
});
