import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NavBarModule, UiManagerModule } from 'sqtm-core';
import { OAuthSuccessComponent } from './pages/oauth-success/oauth-success.component';
import { OAuthFailureComponent } from './pages/oauth-failure/oauth-failure.component';

export const routes: Routes = [
  {
    path: 'success',
    component: OAuthSuccessComponent,
  },
  {
    path: 'failure',
    component: OAuthFailureComponent,
  },
];

@NgModule({
  declarations: [OAuthSuccessComponent, OAuthFailureComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule,
    UiManagerModule,
    NavBarModule,
  ],
})
export class OauthPagesModule {}
