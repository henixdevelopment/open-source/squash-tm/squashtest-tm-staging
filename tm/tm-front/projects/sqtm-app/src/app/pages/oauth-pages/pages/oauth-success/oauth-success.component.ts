import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { InterWindowCommunicationService, InterWindowMessages } from 'sqtm-core';

@Component({
  selector: 'sqtm-app-oauth-success',
  template: `
    <div
      class="full-height full-width flex-column login-page neutral-background-color overflow-hidden"
      [sqtmCoreWorkspace]="'home-workspace'"
    >
      <sqtm-core-horizontal-logout-bar class="flex-fixed-size full-width" [squashVersion]="null">
      </sqtm-core-horizontal-logout-bar>
      <div
        class="flex-fixed-size m-auto p-30 container-background-color container-border"
        style="max-width: 80%;"
      >
        <h2>{{ 'sqtm-core.oauth-pages.success.title' | translate }}</h2>
        <p>{{ 'sqtm-core.oauth-pages.success.message' | translate }}</p>
      </div>
    </div>
  `,
  styleUrls: ['./oauth-success.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OAuthSuccessComponent implements OnInit {
  constructor(public readonly interWindowCommunicationService: InterWindowCommunicationService) {}

  ngOnInit(): void {
    this.interWindowCommunicationService.sendMessage(
      new InterWindowMessages('OAUTH-CONNECT-SUCCESS'),
    );
  }
}
