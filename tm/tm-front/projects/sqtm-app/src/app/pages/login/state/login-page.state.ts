import { LicenseInformationState, LoginPageModel } from 'sqtm-core';

export interface LoginPageState extends LoginPageModel {
  showLoginError: boolean;
  licenseInformation: LicenseInformationState;
}

export function initialLoginPageState(): Readonly<LoginPageState> {
  return {
    squashVersion: null,
    loginMessage: null,
    isH2: false,
    showLoginError: false,
    licenseInformation: null,
    isOpenIdConnectPluginInstalled: false,
    oAuth2ProviderNames: [],
  };
}
