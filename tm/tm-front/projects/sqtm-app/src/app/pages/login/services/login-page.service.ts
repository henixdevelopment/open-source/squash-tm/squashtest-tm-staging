import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { createStore, RestService } from 'sqtm-core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { initialLoginPageState, LoginPageState } from '../state/login-page.state';
import { map, take, withLatestFrom } from 'rxjs/operators';

const loginUrl = 'backend/login';

@Injectable()
export class LoginPageService {
  private store = createStore<LoginPageState>(initialLoginPageState());

  public componentData$ = this.store.state$;

  constructor(
    private http: HttpClient,
    private restService: RestService,
  ) {}

  public load() {
    this.restService
      .get<Partial<LoginPageState>>(['login-page'])
      .pipe(
        withLatestFrom(this.store.state$),
        map(([response, state]) => ({ ...state, ...response })),
      )
      .subscribe((state) => this.store.commit(state));
  }

  public complete() {
    this.store.complete();
  }

  public login(
    username: string,
    password: string,
  ): Observable<{ authenticated: boolean; showInformation: boolean }> {
    const body = new URLSearchParams();
    body.set('username', username);
    body.set('password', password);

    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    };

    return this.http.post<any>(loginUrl, body.toString(), options);
  }

  showLoginErrorMessage() {
    this.store.state$
      .pipe(
        take(1),
        map((state) => ({ ...state, showLoginError: true })),
      )
      .subscribe((state) => this.store.commit(state));
  }
}
