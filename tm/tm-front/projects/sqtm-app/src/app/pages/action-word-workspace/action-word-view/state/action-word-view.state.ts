export interface ActionWordViewState {
  id: number;
  actionWordLibraryNodeId: number;
  word: string;
  description: string;
  projectName: string;
  createdBy: string;
  createdOn: Date;
  lastModifiedBy: string;
  lastModifiedOn: Date;
  writable: boolean;
  entityNotFound: boolean;
  initialLoadError: any;
}

export type ActionWordViewModel = ActionWordViewState;

export type ActionWordViewComponentData = ActionWordViewState;

export function initialActionWordViewState(): Readonly<ActionWordViewState> {
  return {
    id: null,
    actionWordLibraryNodeId: null,
    word: null,
    description: null,
    projectName: null,
    createdBy: null,
    createdOn: null,
    lastModifiedBy: null,
    lastModifiedOn: null,
    writable: null,
    entityNotFound: false,
    initialLoadError: null,
  };
}
