import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ActionWordViewContentComponent } from './action-word-view-content.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { ActionWordViewService } from '../../service/action-word-view.service';
import { ChangeDetectorRef, NO_ERRORS_SCHEMA } from '@angular/core';
import { RestService } from 'sqtm-core';
import { OverlayModule } from '@angular/cdk/overlay';

describe('ActionWordViewContentComponent', () => {
  let component: ActionWordViewContentComponent;
  let fixture: ComponentFixture<ActionWordViewContentComponent>;
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        RouterTestingModule,
        OverlayModule,
      ],
      declarations: [ActionWordViewContentComponent],
      providers: [
        ActionWordViewService,
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: ChangeDetectorRef,
          useValue: jasmine.createSpyObj(['detectChanges']),
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionWordViewContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
