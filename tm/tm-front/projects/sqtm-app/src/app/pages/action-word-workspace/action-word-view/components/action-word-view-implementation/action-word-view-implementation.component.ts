import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import {
  BddImplementationTechnology,
  dateTimeColumn,
  Extendable,
  GridDefinition,
  GridService,
  gridServiceFactory,
  Limited,
  LocalPersistenceService,
  ReferentialDataService,
  RestService,
  smallGrid,
  textColumn,
} from 'sqtm-core';
import { ActionWordViewService } from '../../service/action-word-view.service';
import {
  AW_WS_IMPLEMENTATION_TABLE,
  AW_WS_IMPLEMENTATION_TABLE_CONF,
} from '../../../action-word-workspace/containers/action-word-workspace.constant';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-action-word-view-implementation',
  template: ` <sqtm-core-grid></sqtm-core-grid> `,
  styleUrls: ['./action-word-view-implementation.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: AW_WS_IMPLEMENTATION_TABLE_CONF,
      useFactory: awWsImplementationTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: AW_WS_IMPLEMENTATION_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, AW_WS_IMPLEMENTATION_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: AW_WS_IMPLEMENTATION_TABLE,
    },
  ],
})
export class ActionWordViewImplementationComponent implements OnInit, OnDestroy {
  constructor(
    private gridService: GridService,
    private actionWordViewService: ActionWordViewService,
  ) {}

  ngOnInit(): void {
    this.actionWordViewService.componentData$.pipe(take(1)).subscribe((componentData) => {
      this.gridService.setServerUrl([`action-word-view/${componentData.id}/implementation`]);
    });
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }
}

export function awWsImplementationTableDefinition(
  localPersistenceService: LocalPersistenceService,
): GridDefinition {
  return smallGrid('action-word-view-implementation')
    .withColumns([
      textColumn('lastImplementationTechnology')
        .withI18nKey('sqtm-core.action-word-workspace.implementation.label.technology')
        .withEnumRenderer(BddImplementationTechnology, false, true)
        .changeWidthCalculationStrategy(new Limited(350)),
      dateTimeColumn('lastImplementationDate')
        .withI18nKey(
          'sqtm-core.action-word-workspace.implementation.label.last-implementation-date',
        )
        .changeWidthCalculationStrategy(new Extendable(200)),
    ])
    .server()
    .disableRightToolBar()
    .enableColumnWidthPersistence(localPersistenceService)
    .withRowHeight(35)
    .build();
}
