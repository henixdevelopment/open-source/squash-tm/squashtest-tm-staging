import { TestBed, waitForAsync } from '@angular/core/testing';

import { ActionWordViewService } from './action-word-view.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RestService } from 'sqtm-core';
import { OverlayModule } from '@angular/cdk/overlay';
import { ChangeDetectorRef } from '@angular/core';
import { mockRestService } from '../../../../utils/testing-utils/mocks.service';
import SpyObj = jasmine.SpyObj;
import { of } from 'rxjs';
import { take } from 'rxjs/operators';
import { ActionWordViewModel } from '../state/action-word-view.state';

describe('ActionWordViewService', () => {
  let service: ActionWordViewService;
  let restService: SpyObj<RestService>;

  beforeEach(() => {
    restService = mockRestService();

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, OverlayModule],
      providers: [
        {
          provide: ActionWordViewService,
          useClass: ActionWordViewService,
        },
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: ChangeDetectorRef,
          useValue: jasmine.createSpyObj(['detectChanges']),
        },
      ],
    });
    service = TestBed.inject(ActionWordViewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should load action word view data', waitForAsync(() => {
    const actionWordId = '1';
    restService.getWithoutErrorHandling.and.returnValue(of(mockDefaultActionWordViewModel()));
    service.load(actionWordId);

    service.componentData$
      .pipe(take(1))
      .subscribe((state) => expect(state.id).toBe(parseInt(actionWordId)));
  }));
});

function mockDefaultActionWordViewModel(): ActionWordViewModel {
  return {
    id: 1,
    actionWordLibraryNodeId: 1,
    word: 'je me connecte',
    description: 'une description',
    projectName: 'Project-1',
    createdBy: 'Créée par',
    createdOn: new Date('2021-04-01 11:24'),
    lastModifiedBy: 'Modifiée par',
    lastModifiedOn: new Date('2021-04-01 11:26'),
    writable: true,
    initialLoadError: null,
    entityNotFound: false,
  };
}
