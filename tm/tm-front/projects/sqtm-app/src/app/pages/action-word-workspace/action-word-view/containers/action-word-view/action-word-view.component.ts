import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { ActionWordViewService } from '../../service/action-word-view.service';
import { ActivatedRoute } from '@angular/router';
import { concatMap, filter, take, takeUntil } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { ActionWordViewComponentData } from '../../state/action-word-view.state';
import { ReferentialDataService } from 'sqtm-core';

@Component({
  selector: 'app-action-word-view',
  templateUrl: './action-word-view.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ActionWordViewService],
})
export class ActionWordViewComponent implements OnInit, OnDestroy {
  public readonly entityNotFound$: Observable<boolean>;
  public readonly initialLoadError$: Observable<any>;

  private unsub$ = new Subject<void>();
  componentData$: Observable<ActionWordViewComponentData>;

  constructor(
    private actionWordViewService: ActionWordViewService,
    private referentialDataService: ReferentialDataService,
    private activatedRoute: ActivatedRoute,
  ) {
    this.componentData$ = this.actionWordViewService.componentData$;
    this.entityNotFound$ = this.actionWordViewService.entityNotFound$;
    this.initialLoadError$ = this.actionWordViewService.initialLoadError$;
  }

  ngOnInit(): void {
    this.referentialDataService.loaded$
      .pipe(
        takeUntil(this.unsub$),
        filter((loaded) => loaded),
        take(1),
        concatMap(() => this.activatedRoute.paramMap),
      )
      .subscribe((params) => {
        const id = params.get('actionWordLibraryNodeId');
        if (id != null) {
          this.actionWordViewService.load(id);
        } else {
          console.error('Could not find action word library node ID in route');
        }
      });
  }

  ngOnDestroy(): void {
    this.actionWordViewService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }
}
