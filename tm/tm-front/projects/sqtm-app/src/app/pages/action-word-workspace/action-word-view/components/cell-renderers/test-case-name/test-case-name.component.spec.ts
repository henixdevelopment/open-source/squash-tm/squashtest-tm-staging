import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TestCaseNameComponent } from './test-case-name.component';
import { ActionWordViewService } from '../../../service/action-word-view.service';
import { GridService } from 'sqtm-core';

describe('TestCaseNameComponent', () => {
  let component: TestCaseNameComponent;
  let fixture: ComponentFixture<TestCaseNameComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TestCaseNameComponent],
      providers: [ActionWordViewService, GridService],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCaseNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
