import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { AbstractCellRendererComponent, ColumnDefinitionBuilder, GridService } from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-automation-status',
  template: ` <ng-container *ngIf="columnDisplay && row">
    <div class="full-width full-height flex-column">
      <span class="m-auto-0">{{ getAutomationStatus() }}</span>
    </div>
  </ng-container>`,
  styleUrls: ['./automation-status.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AutomationStatusComponent extends AbstractCellRendererComponent {
  constructor(
    grid: GridService,
    cdr: ChangeDetectorRef,
    private translateService: TranslateService,
  ) {
    super(grid, cdr);
  }

  getAutomationStatus() {
    if (this.row.data['allowAutomationWorkflow'] && 'Y' === this.row.data['automatable']) {
      const value = this.row.data[this.columnDisplay.id];
      if (value != null && value != '') {
        return this.row.data['remoteStatus'] != null
          ? value
          : this.translateService.instant(`sqtm-core.entity.automation-request.status.${value}`);
      } else {
        return '-';
      }
    } else {
      return '-';
    }
  }
}

export function automationStatusColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(AutomationStatusComponent);
}
