import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ActionWordWorkspaceTreeComponent } from './action-word-workspace-tree.component';
import { GridService, RestService } from 'sqtm-core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { AW_WS_TREE } from '../action-word-workspace.constant';
import { EMPTY, of } from 'rxjs';
import { OverlayModule } from '@angular/cdk/overlay';
import { TranslateModule } from '@ngx-translate/core';

describe('ActionWordWorkspaceTreeComponent', () => {
  let component: ActionWordWorkspaceTreeComponent;
  let fixture: ComponentFixture<ActionWordWorkspaceTreeComponent>;
  const restService = {};
  const tableMock = jasmine.createSpyObj('tableMock', [
    'load',
    'setColumnSorts',
    'addFilters',
    'inactivateFilter',
    'activateFilter',
  ]);
  tableMock.selectedRows$ = EMPTY;
  tableMock.openedRowIds$ = EMPTY;
  tableMock.sortedColumns$ = EMPTY;
  tableMock.loaded$ = of(false);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        NzDropDownModule,
        RouterTestingModule,
        OverlayModule,
        TranslateModule.forRoot(),
      ],
      declarations: [ActionWordWorkspaceTreeComponent],
      providers: [
        {
          provide: RestService,
          useValue: restService,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    TestBed.overrideProvider(AW_WS_TREE, { useValue: tableMock });
    TestBed.overrideProvider(GridService, { useValue: tableMock });
    fixture = TestBed.createComponent(ActionWordWorkspaceTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
