import { Component, ChangeDetectionStrategy, HostListener } from '@angular/core';
import { DialogReference, KeyNames } from 'sqtm-core';
import { ActionWordTreeConfirmDeleteConfiguration } from './action-word-tree-confirm-delete-configuration';

@Component({
  selector: 'app-action-word-tree-confirm-delete-dialog',
  templateUrl: './action-word-tree-confirm-delete-dialog.component.html',
  styleUrls: ['./action-word-tree-confirm-delete-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActionWordTreeConfirmDeleteDialogComponent {
  configuration: ActionWordTreeConfirmDeleteConfiguration;

  constructor(
    private dialogReference: DialogReference<ActionWordTreeConfirmDeleteConfiguration, boolean>,
  ) {
    this.configuration = dialogReference.data;
  }

  confirmDeletion() {
    this.dialogReference.result = true;
    this.dialogReference.close();
  }

  @HostListener('window:keyup', ['$event'])
  handleKeyUp(event: KeyboardEvent) {
    if (event.key === KeyNames.ENTER) {
      this.confirmDeletion();
    } else if (event.key === KeyNames.ESCAPE) {
      this.dialogReference.close();
    }
  }
}
