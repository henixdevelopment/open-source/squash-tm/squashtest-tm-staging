import { Component, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  EditableTextFieldComponent,
  GridService,
  TableValueChange,
} from 'sqtm-core';
import { ActionWordViewService } from '../../../service/action-word-view.service';
import { catchError, finalize, map } from 'rxjs/operators';

@Component({
  selector: 'app-editable-parameter-default-value',
  template: ` <ng-container *ngIf="columnDisplay && row">
    <div class="full-width full-height flex-column">
      <sqtm-core-editable-text-field
        #editableTextField
        style="margin: auto 5px;"
        [value]="row.data[columnDisplay.id]"
        [editable]="canEdit"
        [showPlaceHolder]="false"
        [displayInGrid]="true"
        [layout]="'no-buttons'"
        [size]="'small'"
        (confirmEvent)="updateDefaultValue($event)"
      >
      </sqtm-core-editable-text-field>
    </div>
  </ng-container>`,
  styleUrls: ['./editable-parameter-default-value.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditableParameterDefaultValueComponent extends AbstractCellRendererComponent {
  @ViewChild(EditableTextFieldComponent)
  editableTextField: EditableTextFieldComponent;

  canEdit = true;

  constructor(
    grid: GridService,
    cdr: ChangeDetectorRef,
    private actionWordViewService: ActionWordViewService,
  ) {
    super(grid, cdr);
    this.actionWordViewService.componentData$
      .pipe(map((componentData) => componentData.writable))
      .subscribe((writable) => (this.canEdit = writable));
  }

  updateDefaultValue(value: string) {
    this.actionWordViewService
      .changeTextField(this.row.id, value)
      .pipe(
        catchError((error) =>
          this.actionWordViewService.handleError(error, this.editableTextField),
        ),
        finalize(() => this.editableTextField.endAsync()),
      )
      .subscribe(() => {
        const tableValueChange: TableValueChange = { columnId: this.columnDisplay.id, value };
        this.grid.editRows([this.row.id], [tableValueChange]);
      });
  }
}

export function editableParameterDefaultValueColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(EditableParameterDefaultValueComponent);
}
