import { Component, OnInit, ChangeDetectionStrategy, Input, OnDestroy } from '@angular/core';
import { ActionWordViewComponentData } from '../../state/action-word-view.state';
import {
  Extendable,
  GridDefinition,
  GridService,
  gridServiceFactory,
  indexColumn,
  Limited,
  LocalPersistenceService,
  ReferentialDataService,
  RestService,
  smallGrid,
  StyleDefinitionBuilder,
} from 'sqtm-core';
import { ActionWordViewService } from '../../service/action-word-view.service';
import { take } from 'rxjs/operators';
import {
  AW_WS_PARAMETER_TABLE,
  AW_WS_PARAMETER_TABLE_CONF,
} from '../../../action-word-workspace/containers/action-word-workspace.constant';
import { editableParameterDefaultValueColumn } from '../cell-renderers/editable-parameter-default-value/editable-parameter-default-value.component';
import { editableParameterNameColumn } from '../cell-renderers/editable-parameter-name/editable-parameter-name.component';

@Component({
  selector: 'app-action-word-view-parameters',
  template: ` <sqtm-core-grid></sqtm-core-grid> `,
  styleUrls: ['./action-word-view-parameters.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: AW_WS_PARAMETER_TABLE_CONF,
      useFactory: awWsParameterTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: AW_WS_PARAMETER_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, AW_WS_PARAMETER_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: AW_WS_PARAMETER_TABLE,
    },
  ],
})
export class ActionWordViewParametersComponent implements OnInit, OnDestroy {
  @Input()
  actionWordViewComponentData: ActionWordViewComponentData;

  constructor(
    private gridService: GridService,
    private actionWordViewService: ActionWordViewService,
  ) {}

  ngOnInit(): void {
    this.actionWordViewService.componentData$.pipe(take(1)).subscribe((componentData) => {
      this.gridService.setServerUrl([`action-word-view/${componentData.id}/parameters`]);
    });
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }
}

export function awWsParameterTableDefinition(
  localPersistenceService: LocalPersistenceService,
): GridDefinition {
  return smallGrid('action-word-view-parameters')
    .withColumns([
      indexColumn().withViewport('leftViewport'),
      editableParameterNameColumn('name')
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Limited(350)),
      editableParameterDefaultValueColumn('defaultValue')
        .withI18nKey('sqtm-core.action-word-workspace.parameters.label.default-value')
        .changeWidthCalculationStrategy(new Extendable(200)),
    ])
    .server()
    .disableRightToolBar()
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
}
