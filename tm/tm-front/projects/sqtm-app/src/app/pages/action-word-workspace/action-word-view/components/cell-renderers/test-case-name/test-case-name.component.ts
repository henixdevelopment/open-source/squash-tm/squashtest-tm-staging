import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { AbstractCellRendererComponent, ColumnDefinitionBuilder, GridService } from 'sqtm-core';

@Component({
  selector: 'app-test-case-name',
  template: ` <ng-container *ngIf="columnDisplay && row">
    <div class="full-width full-height flex-column">
      <a
        class="txt-ellipsis m-auto-0"
        nz-tooltip
        [nzTooltipTitle]="row.data[columnDisplay.id]"
        [nzTooltipPlacement]="'topLeft'"
        [sqtmCorePlatformLink]="['test-case-workspace', 'test-case', 'detail', row.data['id']]"
      >
        {{ row.data[columnDisplay.id] }}
      </a>
    </div>
  </ng-container>`,
  styleUrls: ['./test-case-name.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestCaseNameComponent extends AbstractCellRendererComponent {
  constructor(grid: GridService, cdr: ChangeDetectorRef) {
    super(grid, cdr);
  }
}

export function testCaseNameColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(TestCaseNameComponent);
}
