import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EditableParameterDefaultValueComponent } from './editable-parameter-default-value.component';
import {
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import { ChangeDetectorRef, NO_ERRORS_SCHEMA } from '@angular/core';
import { ActionWordViewService } from '../../../service/action-word-view.service';
import { OverlayModule } from '@angular/cdk/overlay';

describe('EditableParameterDefaultValueComponent', () => {
  let component: EditableParameterDefaultValueComponent;
  let fixture: ComponentFixture<EditableParameterDefaultValueComponent>;
  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [OverlayModule],
      declarations: [EditableParameterDefaultValueComponent],
      providers: [
        ActionWordViewService,
        {
          provide: GridDefinition,
          useValue: gridConfig,
        },
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService],
        },
        {
          provide: ChangeDetectorRef,
          useValue: jasmine.createSpyObj(['detectChanges']),
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableParameterDefaultValueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
