import {
  Component,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ViewChild,
  Optional,
  ViewContainerRef,
} from '@angular/core';
import {
  AbstractCellRendererComponent,
  AlertDialogComponent,
  ColumnDefinitionBuilder,
  DialogService,
  EditableTextFieldComponent,
  GridService,
  TableValueChange,
  WorkspaceWithTreeComponent,
} from 'sqtm-core';
import { ActionWordViewService } from '../../../service/action-word-view.service';
import { catchError, finalize, map } from 'rxjs/operators';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-editable-parameter-name',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height flex-column">
        <sqtm-core-editable-text-field
          #editableTextField
          style="margin: auto 5px;"
          [value]="row.data[columnDisplay.id]"
          [editable]="canEdit"
          [showPlaceHolder]="false"
          [validators]="getValidators()"
          (validatorErrorEvent)="formErrors()"
          [layout]="'no-buttons'"
          [size]="'small'"
          (confirmEvent)="renameParameter($event)"
        >
        </sqtm-core-editable-text-field>
      </div>
    </ng-container>
  `,
  styleUrls: ['./editable-parameter-name.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditableParameterNameComponent extends AbstractCellRendererComponent {
  @ViewChild(EditableTextFieldComponent)
  editableTextField: EditableTextFieldComponent;

  canEdit = true;

  constructor(
    grid: GridService,
    cdr: ChangeDetectorRef,
    private dialogService: DialogService,
    private actionWordViewService: ActionWordViewService,
    private vcr: ViewContainerRef,
    @Optional() public workspaceWithTree: WorkspaceWithTreeComponent,
  ) {
    super(grid, cdr);
    this.actionWordViewService.componentData$
      .pipe(map((componentData) => componentData.writable))
      .subscribe((writable) => (this.canEdit = writable));
  }

  renameParameter(value: string) {
    this.actionWordViewService
      .renameParameter(this.row.id, value)
      .pipe(
        catchError((error) => {
          this.editableTextField.cancel();
          return this.actionWordViewService.handleError(error, this.editableTextField);
        }),
        finalize(() => {
          this.editableTextField.endAsync();
        }),
      )
      .subscribe(() => {
        const tableValueChange: TableValueChange = { columnId: this.columnDisplay.id, value };
        this.grid.editRows([this.row.id], [tableValueChange]);
        this.workspaceWithTree.requireNodeRefresh([`ActionWord-${this.row.data['awlnId']}`]);
      });
  }

  getValidators() {
    return [Validators.required, Validators.pattern('[A-Za-z0-9_-]{1,255}')];
  }

  formErrors() {
    this.editableTextField.cancel();
    setTimeout(() => {
      this.dialogService.openDialog({
        id: 'alert',
        component: AlertDialogComponent,
        viewContainerReference: this.vcr,
        data: {
          id: 'param-error',
          level: 'DANGER',
          titleKey: 'sqtm-core.generic.label.error',
          messageKey: 'sqtm-core.validation.errors.paramName',
        },
      });
    });
  }
}

export function editableParameterNameColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(EditableParameterNameComponent);
}
