import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, Input } from '@angular/core';
import {
  AW_WS_USING_TEST_CASE_TABLE,
  AW_WS_USING_TEST_CASE_TABLE_CONF,
} from '../../../action-word-workspace/containers/action-word-workspace.constant';
import {
  Extendable,
  Fixed,
  GridDefinition,
  GridService,
  gridServiceFactory,
  indexColumn,
  levelEnumColumn,
  Limited,
  LocalPersistenceService,
  ReferentialDataService,
  RestService,
  smallGrid,
  Sort,
  StyleDefinitionBuilder,
  TestCaseStatus,
  TestCaseWeight,
  textColumn,
} from 'sqtm-core';
import { ActionWordViewComponentData } from '../../state/action-word-view.state';
import { ActionWordViewService } from '../../service/action-word-view.service';
import { testCaseNameColumn } from '../cell-renderers/test-case-name/test-case-name.component';
import { take } from 'rxjs/operators';
import { automationStatusColumn } from '../cell-renderers/automation-status/automation-status.component';

@Component({
  selector: 'app-action-word-view-using-test-cases',
  template: ` <sqtm-core-grid></sqtm-core-grid> `,
  styleUrls: ['./action-word-view-using-test-cases.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: AW_WS_USING_TEST_CASE_TABLE_CONF,
      useFactory: awWsUsingTestCaseTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: AW_WS_USING_TEST_CASE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, AW_WS_USING_TEST_CASE_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: AW_WS_USING_TEST_CASE_TABLE,
    },
  ],
})
export class ActionWordViewUsingTestCasesComponent implements OnInit, OnDestroy {
  @Input()
  actionWordViewComponentData: ActionWordViewComponentData;

  constructor(
    private gridService: GridService,
    private actionWordViewService: ActionWordViewService,
  ) {}

  ngOnInit(): void {
    this.actionWordViewService.componentData$.pipe(take(1)).subscribe((componentData) => {
      this.gridService.setServerUrl([`action-word-view/${componentData.id}/using-test-cases`]);
    });
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }
}

export function awWsUsingTestCaseTableDefinition(
  localPersistenceService: LocalPersistenceService,
): GridDefinition {
  return smallGrid('action-word-view-using-test-cases')
    .withColumns([
      indexColumn().withViewport('leftViewport'),
      textColumn('projectName')
        .changeWidthCalculationStrategy(new Limited(300))
        .withI18nKey('sqtm-core.entity.project.label.singular'),
      textColumn('reference')
        .changeWidthCalculationStrategy(new Limited(175))
        .withI18nKey('sqtm-core.entity.generic.reference.label'),
      testCaseNameColumn('name')
        .changeWidthCalculationStrategy(new Limited(300))
        .withI18nKey('sqtm-core.entity.generic.name.label'),
      levelEnumColumn('importance', TestCaseWeight)
        .changeWidthCalculationStrategy(new Fixed(50))
        .withI18nKey('sqtm-core.search.test-case.grid.header.weight.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.weight.title'),
      levelEnumColumn('status', TestCaseStatus)
        .changeWidthCalculationStrategy(new Fixed(50))
        .withI18nKey('sqtm-core.search.test-case.grid.header.status.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.status.title'),
      automationStatusColumn('automationStatus')
        .changeWidthCalculationStrategy(new Extendable(50))
        .withI18nKey('sqtm-core.action-word-workspace.using-test-cases.label.automation-status'),
    ])
    .server()
    .disableRightToolBar()
    .withInitialSortedColumns([
      { id: 'projectName', sort: Sort.ASC },
      { id: 'reference', sort: Sort.ASC },
      { id: 'name', sort: Sort.ASC },
    ])
    .withRowHeight(35)
    .withStyle(new StyleDefinitionBuilder().showLines())
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
}
