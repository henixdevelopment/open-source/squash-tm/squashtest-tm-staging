import { Injectable, ViewContainerRef } from '@angular/core';
import {
  ActionErrorDisplayService,
  DataRow,
  DeleteData,
  DialogConfiguration,
  DialogService,
  EntityPathHeaderService,
  GridClipboardService,
  Identifier,
  ReferentialDataService,
  RestService,
  TreeNodeServerOperationHandler,
} from 'sqtm-core';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { ActionWordTreeConfirmDeleteConfiguration } from '../components/action-word-tree-confirm-delete-dialog/action-word-tree-confirm-delete-configuration';
import { ActionWordTreeConfirmDeleteDialogComponent } from '../components/action-word-tree-confirm-delete-dialog/action-word-tree-confirm-delete-dialog.component';

@Injectable()
export class ActionWordTreeNodeServerOperationHandler extends TreeNodeServerOperationHandler {
  constructor(
    protected restService: RestService,
    protected dialogService: DialogService,
    protected referentialDataService: ReferentialDataService,
    protected actionErrorDisplayService: ActionErrorDisplayService,
    private readonly vcr: ViewContainerRef,
    entityPathHeaderService: EntityPathHeaderService,
    gridClipboardService: GridClipboardService,
  ) {
    super(
      restService,
      dialogService,
      referentialDataService,
      actionErrorDisplayService,
      entityPathHeaderService,
      gridClipboardService,
    );
  }

  protected shouldCheckInterProjectMoves(): boolean {
    return false;
  }

  protected shouldVerifyPasteOrMoveInto(): boolean {
    return true;
  }

  private simulatePaste(destination: string, draggedRowIds: Identifier[]): Observable<boolean> {
    const urlParts = ['action-word-tree', destination, 'content/paste-simulation'];
    return this.restService.post<boolean>(urlParts, { references: draggedRowIds });
  }

  protected showPasteOrMoveIntoInfoDialog(destination: string, draggedRowIds: Identifier[]) {
    this.simulatePaste(destination, draggedRowIds).subscribe((isOk) => {
      if (!isOk) {
        this.dialogService.openAlert({
          titleKey: 'sqtm-core.action-word-workspace.tree.dialog.action-word-already-exists.title',
          messageKey:
            'sqtm-core.action-word-workspace.tree.dialog.action-word-already-exists.message',
          level: 'INFO',
        });
      }
    });
  }

  delete() {
    this._grid.selectedRows$.pipe(take(1)).subscribe((dataRows: DataRow[]) => {
      const nodeIds = dataRows.map((dataRow) => dataRow.data.AWLN_ID);
      const urlParts = ['action-word-tree/are-all-associated-to-keyword-steps', nodeIds.toString()];

      this.restService.get<boolean>(urlParts).subscribe((areAllAssociated) => {
        if (areAllAssociated) {
          this.dialogService.openAlert({
            level: 'INFO',
            titleKey: 'sqtm-core.action-word-workspace.tree.dialog.no-deletion.title',
            messageKey: 'sqtm-core.action-word-workspace.tree.dialog.no-deletion.message',
          });
        } else {
          super.delete();
        }
      });
    });
  }

  protected showConfirmDeleteDialog(deleteData: DeleteData) {
    const data: Partial<ActionWordTreeConfirmDeleteConfiguration> = {
      messageKey: `sqtm-core.dialog.delete.element.explain`,
      simulationReportTitle:
        'sqtm-core.action-word-workspace.tree.dialog.deletion.simulation-report.title',
      simulationReportMessages: deleteData.deleteSimulation.messageCollection,
      suffixMessageKey: 'sqtm-core.dialog.delete.element.suffix',
      titleKey: 'sqtm-core.dialog.delete.element.title',
      id: 'confirm-delete',
      level: 'DANGER',
    };
    const configuration: DialogConfiguration = {
      id: 'confirm-delete',
      component: ActionWordTreeConfirmDeleteDialogComponent,
      data,
      viewContainerReference: this.vcr,
      width: 550,
    };

    return this.dialogService
      .openDialog<ActionWordTreeConfirmDeleteConfiguration, any>(configuration)
      .dialogClosed$.pipe(map((confirm) => ({ ...deleteData, confirm })));
  }
}
