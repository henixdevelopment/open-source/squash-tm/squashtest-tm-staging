import { TestBed } from '@angular/core/testing';

import { ActionWordLibraryViewService } from './action-word-library-view.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('ActionWordLibraryViewService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TranslateModule.forRoot()],
      providers: [ActionWordLibraryViewService],
    });
  });

  it('should be created', () => {
    const service: ActionWordLibraryViewService = TestBed.inject(ActionWordLibraryViewService);
    expect(service).toBeTruthy();
  });
});
