export type ConfirmDeleteLevel = 'WARNING' | 'DANGER' | 'INFO';

export class ActionWordTreeConfirmDeleteConfiguration {
  id: string;
  titleKey: string;
  level: ConfirmDeleteLevel = 'DANGER';
  messageKey?: string;
  simulationReportTitle?: string;
  simulationReportMessages?: string[];
  suffixMessageKey?: string;
}

export const defaultActionWordTreeDeleteConfirmConfiguration: Readonly<ActionWordTreeConfirmDeleteConfiguration> =
  {
    id: 'confirm-delete',
    titleKey: 'sqtm-core.generic.label.confirm-deletion',
    level: 'DANGER',
    messageKey: 'sqtm-core.dialog.delete.definitive',
  };
