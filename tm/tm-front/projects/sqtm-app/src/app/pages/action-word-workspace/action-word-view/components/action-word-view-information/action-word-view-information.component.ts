import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { ActionWordViewComponentData } from '../../state/action-word-view.state';
import { TranslateService } from '@ngx-translate/core';
import { DatePipe } from '@angular/common';
import { EntityViewService, getSupportedBrowserLang } from 'sqtm-core';
import { ActionWordViewService } from '../../service/action-word-view.service';

@Component({
  selector: 'app-action-word-view-information',
  templateUrl: './action-word-view-information.component.html',
  styleUrls: ['./action-word-view-information.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    DatePipe,
    {
      provide: EntityViewService,
      useExisting: ActionWordViewService,
    },
  ],
})
export class ActionWordViewInformationComponent {
  @Input()
  actionWordViewComponentData: ActionWordViewComponentData;

  constructor(
    private translateService: TranslateService,
    private datePipe: DatePipe,
    private actionWordViewService: ActionWordViewService,
  ) {}

  changeDescription(description: string): void {
    this.actionWordViewService
      .changeDescription(this.actionWordViewComponentData.id, description)
      .subscribe();
  }

  getAuditableText(date: Date, userLogin: string) {
    if (date != null) {
      const formattedDate = this.formatDate(date);
      return `${formattedDate} (${userLogin})`;
    } else {
      return this.translateService.instant('sqtm-core.generic.label.never');
    }
  }

  private formatDate(date: Date): string {
    return this.datePipe.transform(date, 'short', getSupportedBrowserLang(this.translateService));
  }
}
