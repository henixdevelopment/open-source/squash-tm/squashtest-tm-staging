import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ViewChild } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {
  ActionWordLibraryViewComponentData,
  ActionWordLibraryViewService,
} from '../service/action-word-library-view.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import {
  AttachmentDrawerComponent,
  EntityViewService,
  GenericEntityViewService,
  ReferentialDataService,
} from 'sqtm-core';
import { filter, map, take, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-action-word-library-view',
  templateUrl: './action-word-library-view.component.html',
  styleUrls: ['./action-word-library-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: ActionWordLibraryViewService,
      useClass: ActionWordLibraryViewService,
    },
    {
      provide: EntityViewService,
      useExisting: ActionWordLibraryViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: ActionWordLibraryViewService,
    },
  ],
})
export class ActionWordLibraryViewComponent implements OnInit, OnDestroy {
  componentData$: Observable<ActionWordLibraryViewComponentData>;
  private unsub$ = new Subject<void>();

  @ViewChild(AttachmentDrawerComponent)
  attachmentDrawer: AttachmentDrawerComponent;

  constructor(
    private referentialDataService: ReferentialDataService,
    private route: ActivatedRoute,
    private actionWordLibraryViewService: ActionWordLibraryViewService,
  ) {}

  ngOnInit(): void {
    this.referentialDataService.loaded$
      .pipe(
        takeUntil(this.unsub$),
        filter((loaded) => loaded),
        take(1),
      )
      .subscribe(() => {
        this.loadData();
        this.componentData$ = this.actionWordLibraryViewService.componentData$;
      });
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('actionWordLibraryNodeId')),
      )
      .subscribe((id) => {
        this.actionWordLibraryViewService.load(id);
      });
  }

  toggleAttachmentPanel() {
    this.attachmentDrawer.open();
  }

  ngOnDestroy() {
    this.actionWordLibraryViewService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }
}
