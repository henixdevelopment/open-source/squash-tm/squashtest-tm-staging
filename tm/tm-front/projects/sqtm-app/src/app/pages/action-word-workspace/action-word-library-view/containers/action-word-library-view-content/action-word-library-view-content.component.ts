import { Component, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {
  ActionWordLibraryViewComponentData,
  ActionWordLibraryViewService,
} from '../service/action-word-library-view.service';

@Component({
  selector: 'app-action-word-library-view-content',
  templateUrl: './action-word-library-view-content.component.html',
  styleUrls: ['./action-word-library-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActionWordLibraryViewContentComponent implements OnDestroy {
  private unsub$ = new Subject<void>();
  componentData$: Observable<ActionWordLibraryViewComponentData>;

  constructor(private actionWordLibraryViewService: ActionWordLibraryViewService) {
    this.componentData$ = actionWordLibraryViewService.componentData$;
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
