import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import {
  column,
  Extendable,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
  tree,
  TreeNodeCellRendererComponent,
} from 'sqtm-core';
import {
  actionWordWorkspaceTreeId,
  AW_WS_TREE,
  AW_WS_TREE_CONFIG,
} from '../action-word-workspace.constant';
import { ActionWordTreeNodeServerOperationHandler } from '../../service/action-word-tree-node-server-operation-handler';

export function actionWordWorkspaceTreeConfigFactory(): GridDefinition {
  return tree(actionWordWorkspaceTreeId)
    .server()
    .withServerUrl(['action-word-tree'])
    .withColumns([
      column('NAME')
        .enableDnd()
        .changeWidthCalculationStrategy(new Extendable(300))
        .withRenderer(TreeNodeCellRendererComponent),
    ])
    .build();
}

@Component({
  selector: 'app-action-word-workspace',
  templateUrl: './action-word-workspace.component.html',
  styleUrls: ['./action-word-workspace.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: AW_WS_TREE_CONFIG,
      useFactory: actionWordWorkspaceTreeConfigFactory,
      deps: [],
    },
    ActionWordTreeNodeServerOperationHandler,
    {
      provide: AW_WS_TREE,
      useFactory: gridServiceFactory,
      deps: [
        RestService,
        AW_WS_TREE_CONFIG,
        ReferentialDataService,
        ActionWordTreeNodeServerOperationHandler,
      ],
    },
    {
      provide: GridService,
      useExisting: AW_WS_TREE,
    },
  ],
})
export class ActionWordWorkspaceComponent implements OnInit, OnDestroy {
  constructor(
    private referentialDataService: ReferentialDataService,
    private workspaceTree: GridService,
  ) {}

  ngOnInit(): void {
    this.referentialDataService.refresh().subscribe();
  }

  ngOnDestroy(): void {
    this.workspaceTree.complete();
  }
}
