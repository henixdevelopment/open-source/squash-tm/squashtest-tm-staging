import { Injectable } from '@angular/core';
import {
  ActionWordPermissions,
  AttachmentService,
  CustomFieldValueService,
  EntityViewAttachmentHelperService,
  EntityViewComponentData,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  ProjectData,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {
  ActionWordLibraryViewState,
  provideInitialActionWordLibraryView,
} from '../state/action-word-library-view.state';
import { TranslateService } from '@ngx-translate/core';
import { ActionWordLibraryState } from '../state/action-word-library.state';
import { ActionWordLibraryModel } from '../model/action-word-library.model';

@Injectable({
  providedIn: 'root',
})
export class ActionWordLibraryViewService extends EntityViewService<
  ActionWordLibraryState,
  'actionWordLibrary',
  ActionWordPermissions
> {
  constructor(
    protected restService: RestService,
    protected referentialDataService: ReferentialDataService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected customFieldValueService: CustomFieldValueService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
    protected customFieldHelper: EntityViewCustomFieldHelperService,
  ) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper,
    );
  }

  addSimplePermissions(projectData: ProjectData): ActionWordPermissions {
    return new ActionWordPermissions(projectData);
  }

  getInitialState(): ActionWordLibraryViewState {
    return provideInitialActionWordLibraryView();
  }

  load(id: string) {
    this.restService
      .getWithoutErrorHandling<ActionWordLibraryModel>(['action-word-library-view', id.toString()])
      .subscribe({
        next: (actionWordLibraryModel: ActionWordLibraryModel) => {
          const actionWordLibrary = this.initializeActionWordLibraryState(actionWordLibraryModel);
          this.initializeEntityState(actionWordLibrary);
        },
        error: (err) => this.notifyEntityNotFound(err),
      });
  }

  private initializeActionWordLibraryState(
    actionWordLibraryModel: ActionWordLibraryModel,
  ): ActionWordLibraryState {
    const attachmentEntityState = this.initializeAttachmentState(
      actionWordLibraryModel.attachmentList.attachments,
    );
    const customFieldValueState = this.initializeCustomFieldValueState(
      actionWordLibraryModel.customFieldValues,
    );
    return {
      ...actionWordLibraryModel,
      attachmentList: {
        id: actionWordLibraryModel.attachmentList.id,
        attachments: attachmentEntityState,
      },
      customFieldValues: customFieldValueState,
    };
  }
}

export interface ActionWordLibraryViewComponentData
  extends EntityViewComponentData<
    ActionWordLibraryState,
    'actionWordLibrary',
    ActionWordPermissions
  > {}
