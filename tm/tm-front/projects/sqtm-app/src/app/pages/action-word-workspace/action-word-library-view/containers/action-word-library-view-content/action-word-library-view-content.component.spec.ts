import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionWordLibraryViewContentComponent } from './action-word-library-view-content.component';
import SpyObj = jasmine.SpyObj;
import { ActionWordLibraryViewService } from '../service/action-word-library-view.service';
import { EMPTY } from 'rxjs';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('ActionWordLibraryViewContentComponent', () => {
  let component: ActionWordLibraryViewContentComponent;
  let fixture: ComponentFixture<ActionWordLibraryViewContentComponent>;

  const actionWordLibraryViewService: SpyObj<ActionWordLibraryViewService> = jasmine.createSpyObj(
    'actionWordLibraryViewService',
    ['load'],
  );
  actionWordLibraryViewService.componentData$ = EMPTY;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ActionWordLibraryViewContentComponent],
      providers: [
        {
          provide: ActionWordLibraryViewService,
          useValue: actionWordLibraryViewService,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionWordLibraryViewContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
