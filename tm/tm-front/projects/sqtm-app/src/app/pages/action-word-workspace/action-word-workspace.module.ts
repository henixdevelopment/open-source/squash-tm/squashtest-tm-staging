import { RouterModule, Routes } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { FormsModule } from '@angular/forms';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import {
  AnchorModule,
  AttachmentModule,
  DialogModule,
  GridModule,
  NavBarModule,
  PlatformNavigationModule,
  UiManagerModule,
  WorkspaceCommonModule,
  WorkspaceLayoutModule,
} from 'sqtm-core';
import { NgModule } from '@angular/core';
import { ActionWordWorkspaceComponent } from './action-word-workspace/containers/action-word-workspace/action-word-workspace.component';
import { ActionWordLibraryViewComponent } from './action-word-library-view/containers/action-word-library-view/action-word-library-view.component';
import { ActionWordLibraryViewContentComponent } from './action-word-library-view/containers/action-word-library-view-content/action-word-library-view-content.component';
import { ActionWordViewComponent } from './action-word-view/containers/action-word-view/action-word-view.component';
import { ActionWordViewContentComponent } from './action-word-view/containers/action-word-view-content/action-word-view-content.component';
import { ActionWordLevelTwoComponent } from './action-word-view/containers/action-word-level-two/action-word-level-two.component';
import { ActionWordWorkspaceTreeComponent } from './action-word-workspace/containers/action-word-workspace-tree/action-word-workspace-tree.component';
import { ActionWordViewInformationComponent } from './action-word-view/components/action-word-view-information/action-word-view-information.component';
import { ActionWordViewImplementationComponent } from './action-word-view/components/action-word-view-implementation/action-word-view-implementation.component';
import { ActionWordViewUsingTestCasesComponent } from './action-word-view/components/action-word-view-using-test-cases/action-word-view-using-test-cases.component';
import { TestCaseNameComponent } from './action-word-view/components/cell-renderers/test-case-name/test-case-name.component';
import { AutomationStatusComponent } from './action-word-view/components/cell-renderers/automation-status/automation-status.component';
import { ActionWordViewParametersComponent } from './action-word-view/components/action-word-view-parameters/action-word-view-parameters.component';
import { EditableParameterDefaultValueComponent } from './action-word-view/components/cell-renderers/editable-parameter-default-value/editable-parameter-default-value.component';
import { EditableParameterNameComponent } from './action-word-view/components/cell-renderers/editable-parameter-name/editable-parameter-name.component';
import { ActionWordTreeConfirmDeleteDialogComponent } from './action-word-workspace/components/action-word-tree-confirm-delete-dialog/action-word-tree-confirm-delete-dialog.component';
import { TranslateModule } from '@ngx-translate/core';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

const routes: Routes = [
  {
    path: '',
    component: ActionWordWorkspaceComponent,
    children: [
      {
        path: 'action-word-library/:actionWordLibraryNodeId',
        component: ActionWordLibraryViewComponent,
        children: [
          {
            path: 'content',
            component: ActionWordLibraryViewContentComponent,
          },
        ],
      },
      {
        path: 'action-word/:actionWordLibraryNodeId',
        component: ActionWordViewComponent,
        children: [
          {
            path: 'content',
            component: ActionWordViewContentComponent,
          },
        ],
      },
    ],
  },
  {
    path: 'action-word/detail/:actionWordLibraryNodeId',
    component: ActionWordLevelTwoComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'content' },
      { path: 'content', component: ActionWordViewContentComponent },
    ],
  },
];

@NgModule({
  declarations: [
    ActionWordWorkspaceComponent,
    ActionWordWorkspaceTreeComponent,
    ActionWordViewComponent,
    ActionWordLibraryViewComponent,
    ActionWordViewContentComponent,
    ActionWordViewInformationComponent,
    ActionWordViewImplementationComponent,
    ActionWordViewUsingTestCasesComponent,
    TestCaseNameComponent,
    AutomationStatusComponent,
    ActionWordViewParametersComponent,
    EditableParameterDefaultValueComponent,
    EditableParameterNameComponent,
    ActionWordLevelTwoComponent,
    ActionWordLibraryViewContentComponent,
    ActionWordTreeConfirmDeleteDialogComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    WorkspaceLayoutModule,
    GridModule,
    WorkspaceCommonModule,
    AnchorModule,
    NzToolTipModule,
    TranslateModule,
    NzCollapseModule,
    NavBarModule,
    UiManagerModule,
    NzIconModule,
    AttachmentModule,
    PlatformNavigationModule,
    NzDropDownModule,
    NzRadioModule,
    FormsModule,
    NzPopoverModule,
    NzDrawerModule,
    DialogModule,
    NzButtonModule,
    DialogModule,
  ],
  providers: [DatePipe],
})
export class ActionWordWorkspaceModule {}
