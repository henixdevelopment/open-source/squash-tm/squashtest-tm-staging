import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewContainerRef,
} from '@angular/core';
import { actionWordWorkspaceTreeId, AW_WS_TREE } from '../action-word-workspace.constant';
import {
  CreateEntityDialogComponent,
  CreationDialogConfiguration,
  DataRow,
  DialogService,
  EntityCreationDialogData,
  GridPersistenceService,
  GridService,
  ReferentialDataService,
  RestService,
  TreeWithStatePersistence,
} from 'sqtm-core';
import { Router } from '@angular/router';
import { take, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-action-word-workspace-tree',
  templateUrl: './action-word-workspace-tree.component.html',
  styleUrls: ['./action-word-workspace-tree.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: AW_WS_TREE,
    },
  ],
})
export class ActionWordWorkspaceTreeComponent
  extends TreeWithStatePersistence
  implements OnInit, OnDestroy
{
  creationMenuButtonActivation: ActionWordTreeCreationPermission = { canCreateActionWord: false };

  constructor(
    protected restService: RestService,
    public tree: GridService,
    public referentialDataService: ReferentialDataService,
    private cdRef: ChangeDetectorRef,
    protected dialogService: DialogService,
    protected vcr: ViewContainerRef,
    protected gridPersistenceService: GridPersistenceService,
    protected router: Router,
  ) {
    super(
      tree,
      referentialDataService,
      gridPersistenceService,
      restService,
      router,
      dialogService,
      vcr,
      actionWordWorkspaceTreeId,
      'action-word-tree',
    );
  }

  ngOnInit(): void {
    this.initEntityIdFromInitialUrl();
    this.initData();
    this.registerStatePersistence();
    this.initTreeSortMenu();
    this.initCreationMenu();
  }

  ngOnDestroy(): void {
    this.unregisterStatePersistence();
    this.unsub$.next();
    this.unsub$.complete();
  }

  private initCreationMenu() {
    this.tree.selectedRows$.pipe(takeUntil(this.unsub$)).subscribe((selectedRows: DataRow[]) => {
      this.updateCreationMenu(selectedRows);
      this.cdRef.markForCheck();
    });
  }

  private updateCreationMenu(selectedRows: DataRow[]): void {
    let permissions = { ...AW_TREE_NO_CREATION };
    const creationAllowed = selectedRows.length === 1;
    if (creationAllowed) {
      const selectedRow = selectedRows[0];
      const canCreate = selectedRow.simplePermissions.canCreate;
      permissions = {
        canCreateActionWord: canCreate,
      };
    }
    this.creationMenuButtonActivation = permissions;
    this.cdRef.detectChanges();
  }

  openCreateActionWord(canCreate: boolean): void {
    if (!canCreate) {
      return;
    }

    this.tree.selectedRows$.pipe(take(1)).subscribe((dataRows: DataRow[]) => {
      // The creation menu item is enabled if there's only 1 selected element, so we can safely take the first one
      const selectedDataRow = dataRows[0];
      const selectedDataRowId =
        selectedDataRow.type === 'ActionWord'
          ? selectedDataRow.parentRowId.toString()
          : selectedDataRow.id.toString();
      const configuration: CreationDialogConfiguration<EntityCreationDialogData> = {
        viewContainerReference: this.vcr,
        formComponent: CreateEntityDialogComponent,
        data: {
          titleKey: 'sqtm-core.action-word-workspace.tree.button.new-action-word',
          id: 'add-action-word',
          projectId: selectedDataRow.projectId,
          parentEntityReference: selectedDataRowId,
          postUrl: 'action-word-tree/new-action-word',
          addAnotherLabelKey: 'sqtm-core.generic.label.add-another.feminine',
        },
      };
      this.dialogService.openEntityCreationDialog<EntityCreationDialogData, DataRow>(
        configuration,
        this.tree,
      );
    });
  }

  copy() {
    this.tree.copy();
  }

  paste() {
    this.tree.paste();
  }

  delete() {
    this.tree.deleteRows();
  }
}

export interface ActionWordTreeCreationPermission {
  canCreateActionWord: boolean;
}

export const AW_TREE_NO_CREATION: Readonly<ActionWordTreeCreationPermission> = {
  canCreateActionWord: false,
};
