import { InjectionToken } from '@angular/core';
import { GridDefinition, GridService } from 'sqtm-core';

export const AW_WS_TREE_CONFIG = new InjectionToken<GridDefinition>(
  'Grid config instance for the main action word workspace tree',
);
export const AW_WS_TREE = new InjectionToken<GridService>(
  'Grid service instance for the main action word workspace tree',
);

export const actionWordWorkspaceTreeId = 'action-word-workspace-main-tree';

export const AW_WS_IMPLEMENTATION_TABLE_CONF = new InjectionToken<GridDefinition>(
  'Grid config for the implementation table of action word view',
);
export const AW_WS_IMPLEMENTATION_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the implementation table of action word view',
);

export const AW_WS_USING_TEST_CASE_TABLE_CONF = new InjectionToken<GridDefinition>(
  'Grid config for the using test case table of action word view',
);
export const AW_WS_USING_TEST_CASE_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the using test case table of action word view',
);

export const AW_WS_PARAMETER_TABLE_CONF = new InjectionToken<GridDefinition>(
  'Grid config for the parameter table of action word view',
);
export const AW_WS_PARAMETER_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the parameter table of action word view',
);
