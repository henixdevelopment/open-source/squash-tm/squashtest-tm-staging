import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ActionWordViewInformationComponent } from './action-word-view-information.component';
import { ActionWordViewService } from '../../service/action-word-view.service';
import { TranslateService } from '@ngx-translate/core';
import { RestService } from 'sqtm-core';
import { OverlayModule } from '@angular/cdk/overlay';
import { ChangeDetectorRef } from '@angular/core';
import { DatePipe } from '@angular/common';

describe('ActionWordViewInformationComponent', () => {
  let component: ActionWordViewInformationComponent;
  let fixture: ComponentFixture<ActionWordViewInformationComponent>;
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [OverlayModule],
      declarations: [ActionWordViewInformationComponent],
      providers: [
        {
          provide: ActionWordViewService,
          useValue: {},
        },
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj(['instant']),
        },
        {
          provide: RestService,
          useValue: restService,
        },
        DatePipe,
        {
          provide: ChangeDetectorRef,
          useValue: jasmine.createSpyObj(['detectChanges']),
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionWordViewInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
