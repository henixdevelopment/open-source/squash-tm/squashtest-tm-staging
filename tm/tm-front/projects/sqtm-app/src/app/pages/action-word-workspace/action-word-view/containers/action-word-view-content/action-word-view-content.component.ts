import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActionWordViewService } from '../../service/action-word-view.service';
import { ActionWordViewComponentData } from '../../state/action-word-view.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-action-word-view-content',
  templateUrl: './action-word-view-content.component.html',
  styleUrls: ['./action-word-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActionWordViewContentComponent implements OnInit {
  componentData$: Observable<ActionWordViewComponentData>;

  constructor(private actionWordViewService: ActionWordViewService) {}

  ngOnInit(): void {
    this.componentData$ = this.actionWordViewService.componentData$;
  }
}
