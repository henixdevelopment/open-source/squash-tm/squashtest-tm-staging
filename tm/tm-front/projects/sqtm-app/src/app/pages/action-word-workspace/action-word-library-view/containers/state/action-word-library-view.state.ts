import { EntityViewState, provideInitialViewState } from 'sqtm-core';
import { ActionWordLibraryState } from './action-word-library.state';

export interface ActionWordLibraryViewState
  extends EntityViewState<ActionWordLibraryState, 'actionWordLibrary'> {
  actionWordLibrary: ActionWordLibraryState;
}

export function provideInitialActionWordLibraryView(): Readonly<ActionWordLibraryViewState> {
  return provideInitialViewState<ActionWordLibraryState, 'actionWordLibrary'>('actionWordLibrary');
}
