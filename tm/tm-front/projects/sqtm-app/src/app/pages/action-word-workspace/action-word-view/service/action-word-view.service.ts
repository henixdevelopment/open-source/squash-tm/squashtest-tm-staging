import { ChangeDetectorRef, Injectable } from '@angular/core';
import {
  ActionErrorDisplayService,
  createStore,
  EditableTextFieldComponent,
  RestService,
} from 'sqtm-core';
import {
  ActionWordViewComponentData,
  ActionWordViewModel,
  ActionWordViewState,
  initialActionWordViewState,
} from '../state/action-word-view.state';
import { filter, map, take, withLatestFrom } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export class ActionWordViewService {
  public readonly entityNotFound$: Observable<boolean>;
  public readonly initialLoadError$: Observable<any>;

  private store = createStore<ActionWordViewState>(initialActionWordViewState());

  public componentData$: Observable<ActionWordViewComponentData> = this.store.state$.pipe(
    filter((s) => Boolean(s.id)),
  );

  constructor(
    private restService: RestService,
    private actionErrorDisplayService: ActionErrorDisplayService,
    private cdr: ChangeDetectorRef,
  ) {
    this.entityNotFound$ = this.store.state$.pipe(map((state) => state.entityNotFound));
    this.initialLoadError$ = this.store.state$.pipe(map((state) => state.initialLoadError));
  }

  complete() {
    this.store.complete();
  }

  load(id: string) {
    const url = ['action-word-view', id.toString()];

    this.resetState();
    this.restService
      .getWithoutErrorHandling<ActionWordViewModel>(url)
      .pipe(
        withLatestFrom(this.store.state$),
        map(([model, state]: [ActionWordViewModel, ActionWordViewState]) => {
          return { ...state, ...model };
        }),
      )
      .subscribe({
        next: (state) => this.store.commit(state),
        error: (err) => this.notifyEntityNotFound(err),
      });
  }

  changeDescription(actionWordId, description: string): Observable<any> {
    const body = {};
    body['description'] = description;
    return this.restService.post([`action-word-view/${actionWordId}/description`], body).pipe(
      withLatestFrom(this.store.state$),
      map(([, state]) => {
        const updatedState: ActionWordViewState = { ...state };
        updatedState['description'] = description;
        this.store.commit(updatedState);
        return updatedState;
      }),
    );
  }

  changeTextField(parameterId, defaultValue: string): Observable<any> {
    const body = {};
    body['defaultValue'] = defaultValue;
    return this.restService.post([`action-word-view/${parameterId}/parameter-default-value`], body);
  }

  renameParameter(parameterId, name: string): Observable<any> {
    return this.changeParameterNameServerSide(parameterId, name).pipe(
      take(1),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, ActionWordViewState]) => {
        return this.restService
          .get([`action-word-view/${state.actionWordLibraryNodeId}/title`])
          .subscribe((response: ActionWordViewModel) => {
            this.store.commit({
              ...state,
              word: response.word,
            });
            return state;
          });
      }),
    );
  }

  changeParameterNameServerSide(parameterId, name: string) {
    const body = {};
    body['name'] = name;
    return this.restService.post([`action-word-view/${parameterId}/parameter-name`], body);
  }

  handleError(error: any, editableTextField: EditableTextFieldComponent) {
    editableTextField.endAsync();
    this.cdr.markForCheck();
    return this.actionErrorDisplayService.handleActionError(error);
  }

  private resetState(): void {
    this.store.commit(initialActionWordViewState());
  }

  private notifyEntityNotFound(error: any): void {
    this.store.state$.pipe(take(1)).subscribe((state) =>
      this.store.commit({
        ...state,
        entityNotFound: true,
        initialLoadError: error,
      }),
    );
  }
}
