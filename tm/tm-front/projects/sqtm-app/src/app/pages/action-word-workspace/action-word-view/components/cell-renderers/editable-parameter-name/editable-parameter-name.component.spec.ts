import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EditableParameterNameComponent } from './editable-parameter-name.component';
import { ActionWordViewService } from '../../../service/action-word-view.service';
import {
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import { ChangeDetectorRef, NO_ERRORS_SCHEMA } from '@angular/core';
import { OverlayModule } from '@angular/cdk/overlay';

describe('EditableParameterNameComponent', () => {
  let component: EditableParameterNameComponent;
  let fixture: ComponentFixture<EditableParameterNameComponent>;
  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [OverlayModule],
      declarations: [EditableParameterNameComponent],
      providers: [
        ActionWordViewService,
        {
          provide: ChangeDetectorRef,
          useValue: jasmine.createSpyObj(['detectChanges']),
        },
        {
          provide: GridDefinition,
          useValue: gridConfig,
        },
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService],
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableParameterNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
