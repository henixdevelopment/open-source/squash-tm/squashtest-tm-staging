import { EntityModel } from 'sqtm-core';

export interface ActionWordLibraryModel extends EntityModel {
  name: string;
  description: string;
}
