import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import {
  gridServiceFactory,
  LocalPersistenceService,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import { ActionWordViewService } from '../../service/action-word-view.service';
import {
  AW_WS_IMPLEMENTATION_TABLE,
  AW_WS_IMPLEMENTATION_TABLE_CONF,
  AW_WS_PARAMETER_TABLE,
  AW_WS_PARAMETER_TABLE_CONF,
  AW_WS_USING_TEST_CASE_TABLE,
  AW_WS_USING_TEST_CASE_TABLE_CONF,
} from '../../../action-word-workspace/containers/action-word-workspace.constant';
import { awWsParameterTableDefinition } from '../../components/action-word-view-parameters/action-word-view-parameters.component';
import { awWsUsingTestCaseTableDefinition } from '../../components/action-word-view-using-test-cases/action-word-view-using-test-cases.component';
import { awWsImplementationTableDefinition } from '../../components/action-word-view-implementation/action-word-view-implementation.component';

@Component({
  selector: 'app-action-word-level-two',
  templateUrl: './action-word-level-two.component.html',
  styleUrls: ['./action-word-level-two.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: AW_WS_PARAMETER_TABLE_CONF,
      useFactory: awWsParameterTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: AW_WS_PARAMETER_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, AW_WS_PARAMETER_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: AW_WS_USING_TEST_CASE_TABLE_CONF,
      useFactory: awWsUsingTestCaseTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: AW_WS_USING_TEST_CASE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, AW_WS_USING_TEST_CASE_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: AW_WS_IMPLEMENTATION_TABLE_CONF,
      useFactory: awWsImplementationTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: AW_WS_IMPLEMENTATION_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, AW_WS_IMPLEMENTATION_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: ActionWordViewService,
      useClass: ActionWordViewService,
      deps: [
        RestService,
        ReferentialDataService,
        AW_WS_IMPLEMENTATION_TABLE,
        AW_WS_USING_TEST_CASE_TABLE,
        AW_WS_PARAMETER_TABLE,
      ],
    },
  ],
})
export class ActionWordLevelTwoComponent implements OnInit {
  constructor(private referentialDataService: ReferentialDataService) {}

  ngOnInit(): void {
    this.referentialDataService.refresh().subscribe();
  }

  back() {
    history.back();
  }

  hasBackPage() {
    return history.length > 1;
  }
}
