import { AutomationRequestPermissions, ColumnDisplay, DataRow, GridColumnId } from 'sqtm-core';

export function isAutomReqEditable(columnDisplay: ColumnDisplay, row: DataRow): boolean {
  const noLockedMilestones = row.data[GridColumnId.tcMilestoneLocked] === 0;
  const permissions = row.simplePermissions as AutomationRequestPermissions;
  return (
    columnDisplay.editable &&
    (permissions.canWrite || permissions.canWriteAsAutomation) &&
    noLockedMilestones
  );
}

export function canEditRows(rows: DataRow[]) {
  return (
    rows.filter((row) => {
      const noLockedMilestones = row.data[GridColumnId.tcMilestoneLocked] === 0;
      const permissions = row.simplePermissions as AutomationRequestPermissions;
      return (permissions.canWrite || permissions.canWriteAsAutomation) && noLockedMilestones;
    }).length > 0
  );
}

export function filterEditableAutomReq(rows: DataRow[]) {
  return rows.filter((row) => {
    const noLockedMilestones = row.data[GridColumnId.tcMilestoneLocked] === 0;
    const permissions = row.simplePermissions as AutomationRequestPermissions;
    return (permissions.canWrite || permissions.canWriteAsAutomation) && noLockedMilestones;
  });
}
