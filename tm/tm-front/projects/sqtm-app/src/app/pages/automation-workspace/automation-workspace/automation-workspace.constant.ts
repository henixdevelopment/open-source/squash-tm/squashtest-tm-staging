import { InjectionToken } from '@angular/core';
import { GridDefinition, GridService } from 'sqtm-core';

export const ATW_ASSIGNEE_TABLE_CONFIG = new InjectionToken<GridDefinition>(
  'Grid config instance for the automation programmer workspace assignee table',
);
export const ATW_ASSIGNEE_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the automation programmer workspace assignee table',
);

export const ATW_TREAT_TABLE_CONFIG = new InjectionToken<GridDefinition>(
  'Grid config instance for the automation programmer workspace treat table',
);
export const ATW_TREAT_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the automation programmer workspace treat table',
);

export const ATW_GLOBAL_TABLE_CONFIG = new InjectionToken<GridDefinition>(
  'Grid config instance for the automation programmer workspace global table',
);
export const ATW_GLOBAL_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the automation programmer workspace global table',
);
