import { ResearchColumnPrototype, UserHistorySearchProvider, UserListElement } from 'sqtm-core';
import { FunctionalTesterWorkspaceService } from '../../services/functional-tester-workspace.service';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { AutomationTesterWorkspaceDataModel } from '../../state/functional-tester-workspace.state';
import { Injectable } from '@angular/core';

@Injectable()
export class ReadyForTransmissionUserProvider extends UserHistorySearchProvider {
  constructor(private automationTesterWorkspaceService: FunctionalTesterWorkspaceService) {
    super();
  }

  provideUserList(_columnPrototype: ResearchColumnPrototype): Observable<UserListElement[]> {
    return this.automationTesterWorkspaceService.componentData$.pipe(
      take(1),
      map((data: AutomationTesterWorkspaceDataModel) =>
        data.usersWhoModifiedTestCasesReadyView.map((u) => ({ ...u, selected: false })),
      ),
    );
  }
}
