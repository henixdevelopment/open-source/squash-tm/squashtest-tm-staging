import { AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import {
  ActionErrorDisplayService,
  automatedTestTechnologyColumn,
  AutomationRequestStatus,
  buildFilters,
  DataRow,
  dateColumn,
  dateRangeFilter,
  DialogService,
  Extendable,
  grid,
  GridColumnId,
  GridDefinition,
  GridFilter,
  GridFilterUtils,
  GridId,
  GridService,
  gridServiceFactory,
  i18nEnumResearchFilter,
  indexColumn,
  ItemListSearchProvider,
  Limited,
  LocalPersistenceService,
  ReferentialDataService,
  ResearchColumnPrototype,
  RestService,
  selectableTextColumn,
  serverBackedGridEqualFilter,
  serverBackedGridMultiValueFilter,
  serverBackedGridTextFilter,
  Sort,
  StyleDefinitionBuilder,
  TestCaseKind,
  textCellWithToolTipColumn,
  textColumn,
  userHistoryResearchFilter,
  UserHistorySearchProvider,
} from 'sqtm-core';
import { ATW_ASSIGNEE_TABLE, ATW_ASSIGNEE_TABLE_CONFIG } from '../../automation-workspace.constant';
import { AutomationWorkspaceService } from '../../services/automation-workspace.service';
import { Observable, of, Subject } from 'rxjs';
import { AutomationWorkspaceState } from '../../states/automation-workspace.state';
import {
  catchError,
  concatMap,
  filter,
  finalize,
  map,
  switchMap,
  take,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { AutomationAssigneeUserProvider } from './automation-assignee-user-provider';
import { automationRequestLiteralConverter } from '../../../../../components/automation/automation-utils';
import {
  canEditRows,
  filterEditableAutomReq,
  isAutomReqEditable,
} from '../../automation-workspace.utils';
import { squashTFColumn } from '../../components/cell-renderers/automation-squash-tf-renderer/automation-squash-tf-renderer.component';
import { scmRepositoriesColumn } from '../../components/cell-renderers/scm-autocomplete-cell-renderer/scm-autocomplete-cell-renderer.component';
import { ScmRepositoryProvider } from '../scm-repository.provider';

export function atwAssigneeTableDefinition(
  localPersistenceService: LocalPersistenceService,
): GridDefinition {
  return grid(GridId.AUTOMATION_PROGRAMMER_ASSIGNED)
    .withColumns([
      indexColumn().withViewport('leftViewport'),
      textCellWithToolTipColumn(GridColumnId.projectName, 'testCasePath')
        .withI18nKey('sqtm-core.entity.project.label.singular')
        .changeWidthCalculationStrategy(new Limited(170))
        .withAssociatedFilter(),
      textColumn(GridColumnId.tclnId)
        .withI18nKey('sqtm-core.entity.generic.id.capitalize')
        .changeWidthCalculationStrategy(new Limited(60))
        .withAssociatedFilter(),
      textColumn(GridColumnId.reference)
        .withI18nKey('sqtm-core.automation-workspace.grid.headers.reference')
        .withTitleI18nKey('sqtm-core.grid.header.reference')
        .changeWidthCalculationStrategy(new Limited(125))
        .withAssociatedFilter(),
      selectableTextColumn(GridColumnId.name)
        .withI18nKey('sqtm-core.entity.test-case.label.singular')
        .changeWidthCalculationStrategy(new Limited(170))
        .withAssociatedFilter(),
      textColumn(GridColumnId.kind)
        .withEnumRenderer(TestCaseKind, false, true)
        .withI18nKey('sqtm-core.entity.test-case.kind.label')
        .changeWidthCalculationStrategy(new Limited(40)),
      textColumn(GridColumnId.login)
        .withI18nKey('sqtm-core.automation-workspace.grid.headers.tester')
        .changeWidthCalculationStrategy(new Limited(60))
        .withAssociatedFilter('login'),
      textColumn(GridColumnId.automationPriority)
        .withI18nKey('sqtm-core.grid.header.priority.short')
        .withTitleI18nKey('sqtm-core.grid.header.priority.long')
        .changeWidthCalculationStrategy(new Limited(80))
        .withAssociatedFilter(),
      textColumn(GridColumnId.requestStatus)
        .withEnumRenderer(AutomationRequestStatus, false, true)
        .isEditable(false)
        .withI18nKey('sqtm-core.entity.automation-request.status.label.short')
        .withTitleI18nKey('sqtm-core.entity.automation-request.status.label.full')
        .changeWidthCalculationStrategy(new Limited(60))
        .withAssociatedFilter(),
      automatedTestTechnologyColumn()
        .isEditable(isAutomReqEditable)
        .withI18nKey('sqtm-core.automation-workspace.grid.headers.technology.label.short')
        .withTitleI18nKey('sqtm-core.automation-workspace.grid.headers.technology.label.full')
        .changeWidthCalculationStrategy(new Limited(60))
        .withAssociatedFilter(),
      scmRepositoriesColumn(GridColumnId.scmRepositoryId)
        .isEditable(isAutomReqEditable)
        .withI18nKey('sqtm-core.automation-workspace.grid.headers.scm-url.label.short')
        .withTitleI18nKey('sqtm-core.automation-workspace.grid.headers.scm-url.label.full')
        .changeWidthCalculationStrategy(new Limited(125))
        .withAssociatedFilter(),
      textColumn(GridColumnId.automatedTestReference)
        .isEditable(isAutomReqEditable)
        .withI18nKey('sqtm-core.automation-workspace.grid.headers.ref-automated-test.label.short')
        .withTitleI18nKey(
          'sqtm-core.automation-workspace.grid.headers.ref-automated-test.label.full',
        )
        .changeWidthCalculationStrategy(new Limited(150))
        .withAssociatedFilter(),
      squashTFColumn(GridColumnId.automatedTestFullName)
        .withI18nKey('sqtm-core.automation-workspace.grid.headers.squash-tf.label.short')
        .withTitleI18nKey('sqtm-core.automation-workspace.grid.headers.squash-tf.label.full')
        .changeWidthCalculationStrategy(new Limited(150))
        .withAssociatedFilter(),
      dateColumn(GridColumnId.transmittedOn)
        .withI18nKey('sqtm-core.automation-workspace.grid.headers.transmitted-on')
        .changeWidthCalculationStrategy(new Limited(80))
        .withAssociatedFilter(),
      dateColumn(GridColumnId.assignedOn)
        .withI18nKey('sqtm-core.entity.automation-request.assigned-on')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1)),
    ])
    .server()
    .withServerUrl(['automation-workspace/assignee-autom-req'])
    .withRowConverter(automationRequestLiteralConverter)
    .disableRightToolBar()
    .withRowHeight(35)
    .withModificationUrl(['automation-requests'])
    .withStyle(new StyleDefinitionBuilder().enableInitialLoadAnimation().showLines())
    .withInitialSortedColumns([
      { id: GridColumnId.automationPriority, sort: Sort.DESC },
      { id: GridColumnId.requestStatus, sort: Sort.ASC },
      { id: GridColumnId.kind, sort: Sort.ASC },
    ])
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
}

@Component({
  selector: 'sqtm-app-automation-workspace-assignee',
  templateUrl: './automation-workspace-assignee.component.html',
  styleUrls: ['./automation-workspace-assignee.component.less'],
  providers: [
    {
      provide: ATW_ASSIGNEE_TABLE_CONFIG,
      useFactory: atwAssigneeTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: ATW_ASSIGNEE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, ATW_ASSIGNEE_TABLE_CONFIG, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: ATW_ASSIGNEE_TABLE,
    },
    { provide: UserHistorySearchProvider, useClass: AutomationAssigneeUserProvider },
    { provide: ItemListSearchProvider, useClass: ScmRepositoryProvider },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AutomationWorkspaceAssigneeComponent implements AfterViewInit, OnDestroy {
  canEditRows$: Observable<boolean>;
  componentData$: Observable<AutomationWorkspaceState>;
  unsub$ = new Subject<void>();
  activeFilters$: Observable<GridFilter[]>;

  constructor(
    public referentialDataService: ReferentialDataService,
    public gridService: GridService,
    private automationWorkspaceService: AutomationWorkspaceService,
    private actionErrorDisplayService: ActionErrorDisplayService,
    private dialogService: DialogService,
  ) {
    this.componentData$ = this.automationWorkspaceService.componentData$;
    this.canEditRows$ = this.gridService.selectedRows$.pipe(map((rows) => canEditRows(rows)));
    this.activeFilters$ = this.gridService.activeFilters$.pipe(
      takeUntil(this.unsub$),
      map((gridFilters: GridFilter[]) =>
        gridFilters.filter((gridFilter) => GridFilterUtils.mustIncludeFilter(gridFilter)),
      ),
    );
  }

  ngAfterViewInit(): void {
    this.componentData$
      .pipe(
        filter((component) => component.nbAssignedAutomReq > 0),
        take(1),
        tap(() => this.gridService.addFilters(this.buildGridFilters())),
      )
      .subscribe(() => {
        this.gridService.refreshData();
      });
  }

  private buildGridFilters(): GridFilter[] {
    return buildFilters([
      serverBackedGridTextFilter(GridColumnId.projectName),
      serverBackedGridEqualFilter(GridColumnId.tclnId),
      serverBackedGridTextFilter(GridColumnId.reference),
      serverBackedGridTextFilter(GridColumnId.name),
      serverBackedGridEqualFilter(GridColumnId.automationPriority),
      serverBackedGridMultiValueFilter(GridColumnId.scmRepositoryId),
      serverBackedGridTextFilter(GridColumnId.automatedTestReference),
      serverBackedGridTextFilter(GridColumnId.automatedTestFullName),
      serverBackedGridTextFilter(GridColumnId.automatedTestTechnology),
      serverBackedGridTextFilter(GridColumnId.conflictAssociation),
      i18nEnumResearchFilter(
        GridColumnId.requestStatus,
        ResearchColumnPrototype.AUTOMATION_REQUEST_STATUS,
      ).alwaysActive(),
      userHistoryResearchFilter(
        GridColumnId.login,
        ResearchColumnPrototype.TEST_CASE_MODIFIED_BY,
      ).alwaysActive(),
      dateRangeFilter(
        GridColumnId.transmittedOn,
        ResearchColumnPrototype.ITEM_TEST_PLAN_LASTEXECON,
      ),
    ]);
  }

  unAssignAutomationRequest() {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        filter((rows) => filterEditableAutomReq(rows).length > 0),
        tap(() => this.gridService.beginAsyncOperation()),
        switchMap((rows) => this.showConfirmDialogIfNeeded(rows)),
        concatMap((rows) =>
          this.automationWorkspaceService.unAssignAutomationRequest(
            rows.map((row) => row.id as number),
          ),
        ),
        finalize(() => this.gridService.completeAsyncOperation()),
      )
      .subscribe(() => {
        this.gridService.refreshData();
      });
  }

  changeToAutomatedStatus() {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        filter((rows) => filterEditableAutomReq(rows).length > 0),
        switchMap((rows) => this.showConfirmDialogIfNeeded(rows)),
        tap(() => this.gridService.beginAsyncOperation()),
        concatMap((rows: DataRow[]) => {
          return this.automationWorkspaceService
            .changeStatusToAutomated(rows.map((row) => row.id as number))
            .pipe(catchError((err) => this.actionErrorDisplayService.handleActionError(err)));
        }),
        finalize(() => this.gridService.completeAsyncOperation()),
      )
      .subscribe(() => {
        this.gridService.refreshData();
      });
  }

  rejectAutomRequest() {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        filter((rows) => filterEditableAutomReq(rows).length > 0),
        switchMap((rows) => this.showConfirmDialogIfNeeded(rows)),
        tap(() => this.gridService.beginAsyncOperation()),
        concatMap((rows) =>
          this.automationWorkspaceService
            .changeStatusToRejected(rows.map((row) => row.id as number))
            .pipe(catchError((err) => this.actionErrorDisplayService.handleActionError(err))),
        ),
        finalize(() => this.gridService.completeAsyncOperation()),
      )
      .subscribe(() => {
        this.gridService.refreshData();
      });
  }

  automRequestInProgress() {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        filter((rows) => filterEditableAutomReq(rows).length > 0),
        switchMap((rows) => this.showConfirmDialogIfNeeded(rows)),
        tap(() => this.gridService.beginAsyncOperation()),
        concatMap((rows) =>
          this.automationWorkspaceService
            .changeStatusToAutomationInProgress(rows.map((row) => row.id as number))
            .pipe(catchError((err) => this.actionErrorDisplayService.handleActionError(err))),
        ),
        finalize(() => this.gridService.completeAsyncOperation()),
      )
      .subscribe(() => {
        this.gridService.refreshData();
      });
  }

  private showConfirmDialogIfNeeded(rows: DataRow[]): Observable<DataRow[]> {
    const editableRows: DataRow[] = filterEditableAutomReq(rows);
    if (editableRows.length !== rows.length) {
      const dialogRef = this.dialogService.openAlert({
        id: 'change-automation-state-warning',
        titleKey: 'sqtm-core.automation-workspace.functional-tester-view.dialog.title',
        messageKey: 'sqtm-core.automation-workspace.functional-tester-view.dialog.message',
      });

      return dialogRef.dialogClosed$.pipe(map((_confirm) => editableRows));
    } else {
      return of(editableRows);
    }
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }

  shouldShowResetFilterLink(activeFilters: GridFilter[]): boolean {
    return activeFilters.length > 0;
  }

  resetFilters() {
    this.gridService.resetFilters();
  }
}
