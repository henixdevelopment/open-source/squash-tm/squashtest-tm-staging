import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import {
  ActionErrorDisplayService,
  DialogReference,
  DialogService,
  EditableTaTestComponent,
  RestService,
  TestAutomation,
  TestAutomationServerKind,
} from 'sqtm-core';
import { SquashTfDialogConfiguration } from './squash-tf-dialog.configuration';
import { ConflictAssociationDialogConfiguration } from '../conflict-association-dialog/conflict-association-dialog.configuration';
import { ConflictAssociationDialogComponent } from '../conflict-association-dialog/conflict-association-dialog.component';
import { catchError, finalize, tap } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-squash-tf-dialog',
  templateUrl: './squash-tf-dialog.component.html',
  styleUrls: ['./squash-tf-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SquashTfDialogComponent {
  data: SquashTfDialogConfiguration;

  @ViewChild('taTestField')
  taTestField: EditableTaTestComponent;

  constructor(
    private dialogReference: DialogReference<SquashTfDialogConfiguration>,
    private dialogService: DialogService,
    private vcr: ViewContainerRef,
    private restService: RestService,
    private actionErrorDisplayService: ActionErrorDisplayService,
    private cdr: ChangeDetectorRef,
  ) {
    this.data = this.dialogReference.data;
  }

  changeTaScript($event: any) {
    this.restService
      .post<TestAutomation>([`test-case/${this.data.testCaseId}/test-automation/tests`], {
        path: $event,
      })
      .pipe(
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => this.taTestField.endAsync()),
        tap(() => {
          this.taTestField.disableEditMode();
          this.taTestField.cdRef.detectChanges();
        }),
      )
      .subscribe((testAutomation: TestAutomation) => {
        if (testAutomation != null) {
          this.data.scriptAuto = testAutomation.fullLabel;
          this.dialogReference.result = testAutomation.fullLabel;
          this.data.isManual = true;
        } else {
          this.data.scriptAuto = null;
          this.dialogReference.result = null;
        }
        this.cdr.markForCheck();
      });
  }

  openConflictAssociationDialog() {
    const conflictAssos = this.data.conflictAssociation.replace(',', '').split('#');
    this.dialogService.openDialog<ConflictAssociationDialogConfiguration, any>({
      id: 'conflict-association',
      viewContainerReference: this.vcr,
      component: ConflictAssociationDialogComponent,
      data: { conflictAssociation: conflictAssos },
    });
  }

  canEditScript() {
    const isStandardTc = this.data.tcKind === 'STANDARD';
    const conflictAssociationIsEmpty =
      this.data.conflictAssociation == null || this.data.conflictAssociation === '';
    const canWrite = this.data.canWrite;

    return (
      isStandardTc &&
      (this.data.isManual || this.data.scriptAuto == null) &&
      canWrite &&
      conflictAssociationIsEmpty &&
      this.hasJenkinsServer()
    );
  }

  hasJenkinsServer() {
    return (
      this.data.taServerId != null &&
      this.data.testAutomationServerKind === TestAutomationServerKind.jenkins
    );
  }
}
