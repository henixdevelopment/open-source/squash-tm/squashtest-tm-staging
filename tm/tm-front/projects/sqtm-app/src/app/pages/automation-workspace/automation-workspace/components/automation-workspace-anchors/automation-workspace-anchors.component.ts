import { ChangeDetectionStrategy, Component } from '@angular/core';
import { WorkspaceWithGridComponent } from 'sqtm-core';
import { AutomationWorkspaceService } from '../../services/automation-workspace.service';
import { Observable } from 'rxjs';
import { AutomationWorkspaceState } from '../../states/automation-workspace.state';

@Component({
  selector: 'sqtm-app-automation-workspace-anchors',
  templateUrl: './automation-workspace-anchors.component.html',
  styleUrls: ['./automation-workspace-anchors.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AutomationWorkspaceAnchorsComponent {
  componentData$: Observable<AutomationWorkspaceState>;

  constructor(
    private workspaceWithGrid: WorkspaceWithGridComponent,
    private automationWorkspaceService: AutomationWorkspaceService,
  ) {
    this.componentData$ = this.automationWorkspaceService.componentData$;
  }

  hideContextualContent() {
    this.workspaceWithGrid.switchToNoRowLayout();
  }
}
