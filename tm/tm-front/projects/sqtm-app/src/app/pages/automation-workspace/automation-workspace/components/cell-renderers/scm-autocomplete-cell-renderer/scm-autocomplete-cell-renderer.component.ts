import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import {
  AbstractListCellRendererComponent,
  ActionErrorDisplayService,
  buildScmRepositoryUrl,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
  ListPanelItem,
  ReferentialDataService,
  RestService,
  ScmServer,
  TableValueChange,
} from 'sqtm-core';
import { Observable } from 'rxjs';
import { ConnectedPosition, Overlay, OverlayConfig } from '@angular/cdk/overlay';
import { TranslateService } from '@ngx-translate/core';
import { catchError, finalize, take } from 'rxjs/operators';
import { TemplatePortal } from '@angular/cdk/portal';

@Component({
  selector: 'sqtm-app-scm-autocomplete-cell-renderer',
  templateUrl: './scm-autocomplete-cell-renderer.component.html',
  styleUrls: ['./scm-autocomplete-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScmAutocompleteCellRendererComponent
  extends AbstractListCellRendererComponent
  implements OnInit
{
  protected edit = false;

  @ViewChild('templatePortalContent', { read: TemplateRef })
  templatePortalContent: TemplateRef<any>;

  @ViewChild('infoListItemRef', { read: ElementRef })
  infoListItemRef: ElementRef;

  asyncOperationRunning = false;

  scmServers$: Observable<ScmServer[]>;

  listPanelItems: ListPanelItem[];

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    public overlay: Overlay,
    public vcr: ViewContainerRef,
    private referentialDataService: ReferentialDataService,
    public translateService: TranslateService,
    public restService: RestService,
    public actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    super(grid, cdRef, overlay, vcr, translateService, restService, actionErrorDisplayService);
    this.scmServers$ = this.referentialDataService.scmServers$;
  }

  ngOnInit(): void {
    this.initializeListPanelItems();
  }

  private initializeListPanelItems() {
    this.scmServers$.pipe(take(1)).subscribe((scmServers) => {
      this.listPanelItems = this.getListPanelItems(scmServers);
    });
  }

  private getListPanelItems(scmServers: ScmServer[]): ListPanelItem[] {
    const listPanelItems: ListPanelItem[] = [];
    listPanelItems.push({
      id: 0,
      label: this.translateService.instant('sqtm-core.generic.label.none.masculine'),
    });

    scmServers.forEach((server) => {
      const repositories: ListPanelItem[] = server.repositories.map((repo) => {
        return {
          id: repo.scmRepositoryId,
          label: buildScmRepositoryUrl(server, repo),
        };
      });
      listPanelItems.push(...repositories);
    });
    return listPanelItems;
  }

  showTemplate() {
    if (this.isEditable()) {
      this.showList(this.infoListItemRef, this.templatePortalContent);
    }
  }

  showList(
    elementRef: ElementRef,
    templateRef: TemplateRef<any>,
    _positions?: ConnectedPosition[],
  ) {
    const positionStrategy = this.overlay
      .position()
      .flexibleConnectedTo(elementRef)
      .withPositions([
        { originX: 'start', overlayX: 'start', originY: 'center', overlayY: 'top', offsetY: 10 },
        { originX: 'start', overlayX: 'start', originY: 'center', overlayY: 'top', offsetY: -10 },
      ]);
    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
      width: 350,
    };
    this.overlayRef = this.overlay.create(overlayConfig);
    const templatePortal = new TemplatePortal(templateRef, this.vcr);
    this.overlayRef.attach(templatePortal);
    this.overlayRef.backdropClick().subscribe(() => this.close());
  }

  close() {
    this.removeOverlay();
    this.cdRef.detectChanges();
  }

  private removeOverlay() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
    }
  }

  getValue(scmServers: ScmServer[], itemId: number) {
    const listPanel = this.getListPanelItems(scmServers);
    return this.getOptionLabel(listPanel.find((item) => item.id === itemId));
  }

  getOptionLabel(item: ListPanelItem): string {
    if (item) {
      return item.label;
    } else {
      return this.translateService.instant('sqtm-core.generic.label.none.masculine');
    }
  }

  change(repositoryId: number) {
    const testCaseId = this.row.data[GridColumnId.tclnId];
    this.beginAsyncOperation();
    this.restService
      .post(['test-case', testCaseId, 'scm-repository-id'], { scmRepositoryId: repositoryId })
      .pipe(
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => this.endAsyncOperation()),
      )
      .subscribe(() => {
        const changes: TableValueChange[] = [
          { columnId: this.columnDisplay.id, value: repositoryId == null ? 0 : repositoryId },
        ];
        this.grid.editRows([this.row.id], changes);
        this.close();
      });
  }
}

export function scmRepositoriesColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(ScmAutocompleteCellRendererComponent);
}
