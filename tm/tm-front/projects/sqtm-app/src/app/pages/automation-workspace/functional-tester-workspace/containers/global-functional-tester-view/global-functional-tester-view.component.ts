import { AfterViewInit, ChangeDetectionStrategy, Component } from '@angular/core';
import {
  AFTW_GLOBAL_VIEW_TABLE,
  AFTW_GLOBAL_VIEW_TABLE_CONFIG,
} from '../../functional-tester-workspace.constant';
import {
  AutomationRequestStatus,
  buildFilters,
  DataRow,
  dateColumn,
  dateRangeFilter,
  DialogService,
  Extendable,
  Fixed,
  grid,
  GridColumnId,
  GridFilter,
  GridFilterUtils,
  GridId,
  GridService,
  gridServiceFactory,
  i18nEnumResearchFilter,
  indexColumn,
  Limited,
  LocalPersistenceService,
  ReferentialDataService,
  ResearchColumnPrototype,
  RestService,
  selectableTextColumn,
  serverBackedGridEqualFilter,
  serverBackedGridTextFilter,
  Sort,
  StyleDefinitionBuilder,
  TestCaseKind,
  textCellWithToolTipColumn,
  textColumn,
  userHistoryResearchFilter,
  UserHistorySearchProvider,
} from 'sqtm-core';
import { Observable, of, Subject } from 'rxjs';
import { FunctionalTesterWorkspaceState } from '../../state/functional-tester-workspace.state';
import { FunctionalTesterWorkspaceService } from '../../services/functional-tester-workspace.service';
import { automationRequestLiteralConverter } from '../../../../../components/automation/automation-utils';
import { filter, map, switchMap, take, takeUntil, tap } from 'rxjs/operators';
import { uuidColumn } from '../../../../../components/automation/components/cell-renderers/uuid-cell-renderer/uuid-cell-renderer.component';
import { GlobalFunctionalTesterUserProvider } from './global-functional-tester-user-provider';
import { canEditRow, isAutomReqEditable } from '../../functional-tester-workspace.utils';

export function aftwGlobalViewTableDefinition(localPersistenceService: LocalPersistenceService) {
  return grid(GridId.FUNCTIONAL_TESTER_GLOBAL_VIEW)
    .withColumns([
      indexColumn().changeWidthCalculationStrategy(new Fixed(60)).withViewport('leftViewport'),
      textCellWithToolTipColumn(GridColumnId.projectName, 'testCasePath')
        .withI18nKey('sqtm-core.entity.project.label.singular')
        .changeWidthCalculationStrategy(new Limited(255))
        .withAssociatedFilter(),
      textColumn(GridColumnId.tclnId)
        .withI18nKey('sqtm-core.entity.generic.id.capitalize')
        .changeWidthCalculationStrategy(new Limited(60))
        .withAssociatedFilter(),
      textColumn(GridColumnId.reference)
        .withI18nKey('sqtm-core.automation-workspace.grid.headers.reference')
        .withTitleI18nKey('sqtm-core.grid.header.reference')
        .changeWidthCalculationStrategy(new Limited(125))
        .withAssociatedFilter(),
      selectableTextColumn(GridColumnId.name)
        .withI18nKey('sqtm-core.entity.test-case.label.singular')
        .changeWidthCalculationStrategy(new Limited(255))
        .withAssociatedFilter(),
      uuidColumn(GridColumnId.uuid)
        .withI18nKey('sqtm-core.entity.test-case.uuid.label')
        .changeWidthCalculationStrategy(new Limited(70))
        .disableSort(),
      textColumn(GridColumnId.kind)
        .withEnumRenderer(TestCaseKind, false, true)
        .withI18nKey('sqtm-core.entity.test-case.kind.label')
        .changeWidthCalculationStrategy(new Limited(80)),
      textColumn(GridColumnId.login)
        .withI18nKey('sqtm-core.automation-workspace.grid.headers.tester')
        .changeWidthCalculationStrategy(new Limited(80))
        .withAssociatedFilter(),
      textColumn(GridColumnId.automationPriority)
        .isEditable(isAutomReqEditable)
        .withI18nKey('sqtm-core.grid.header.priority.short')
        .withTitleI18nKey('sqtm-core.grid.header.priority.long')
        .changeWidthCalculationStrategy(new Limited(70))
        .withAssociatedFilter(),
      textColumn(GridColumnId.requestStatus)
        .withEnumRenderer(AutomationRequestStatus, false, true)
        .isEditable(false)
        .withI18nKey('sqtm-core.entity.automation-request.status.label.short')
        .withTitleI18nKey('sqtm-core.entity.automation-request.status.label.full')
        .changeWidthCalculationStrategy(new Limited(120))
        .withAssociatedFilter(),
      dateColumn(GridColumnId.transmittedOn)
        .withI18nKey('sqtm-core.automation-workspace.grid.headers.transmitted-on')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1))
        .withAssociatedFilter(),
    ])
    .server()
    .withServerUrl(['automation-tester-workspace/global-view'])
    .withRowConverter(automationRequestLiteralConverter)
    .withInitialSortedColumns([
      { id: GridColumnId.automationPriority, sort: Sort.DESC },
      { id: GridColumnId.requestStatus, sort: Sort.ASC },
      { id: GridColumnId.kind, sort: Sort.DESC },
    ])
    .withModificationUrl(['automation-requests'])
    .disableRightToolBar()
    .withRowHeight(35)
    .withStyle(new StyleDefinitionBuilder().enableInitialLoadAnimation().showLines())
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
}

@Component({
  selector: 'sqtm-app-global-functional-tester-view',
  templateUrl: './global-functional-tester-view.component.html',
  styleUrls: ['./global-functional-tester-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: AFTW_GLOBAL_VIEW_TABLE_CONFIG,
      useFactory: aftwGlobalViewTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: AFTW_GLOBAL_VIEW_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, AFTW_GLOBAL_VIEW_TABLE_CONFIG, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: AFTW_GLOBAL_VIEW_TABLE,
    },
    { provide: UserHistorySearchProvider, useClass: GlobalFunctionalTesterUserProvider },
  ],
})
export class GlobalFunctionalTesterViewComponent implements AfterViewInit {
  componentData$: Observable<FunctionalTesterWorkspaceState>;
  unsub$ = new Subject<void>();

  canEditRows$: Observable<boolean>;
  activeFilters$: Observable<GridFilter[]>;

  constructor(
    public referentialDataService: ReferentialDataService,
    public gridService: GridService,
    private dialogService: DialogService,
    private automationTesterWorkspaceService: FunctionalTesterWorkspaceService,
  ) {
    this.componentData$ = this.automationTesterWorkspaceService.componentData$;
    this.canEditRows$ = this.gridService.selectedRows$.pipe(
      map((rows) => {
        return this.filterNonEditableRows(rows).length > 0;
      }),
    );
    this.activeFilters$ = this.gridService.activeFilters$.pipe(
      takeUntil(this.unsub$),
      map((gridFilters: GridFilter[]) =>
        gridFilters.filter((gridFilter) => GridFilterUtils.mustIncludeFilter(gridFilter)),
      ),
    );
  }

  ngAfterViewInit() {
    this.componentData$
      .pipe(
        filter((component) => component.nbGlobalAutomReq > 0),
        take(1),
        tap(() => this.gridService.addFilters(this.buildGridFilters())),
      )
      .subscribe(() => {
        this.gridService.refreshData();
      });
  }

  private buildGridFilters(): GridFilter[] {
    return buildFilters([
      serverBackedGridTextFilter(GridColumnId.projectName),
      serverBackedGridEqualFilter(GridColumnId.tclnId),
      serverBackedGridTextFilter(GridColumnId.reference),
      serverBackedGridTextFilter(GridColumnId.name),
      serverBackedGridTextFilter(GridColumnId.uuid),
      serverBackedGridEqualFilter(GridColumnId.automationPriority),
      i18nEnumResearchFilter(
        GridColumnId.requestStatus,
        ResearchColumnPrototype.AUTOMATION_REQUEST_STATUS,
      ).alwaysActive(),
      userHistoryResearchFilter(
        GridColumnId.login,
        ResearchColumnPrototype.TEST_CASE_MODIFIED_BY,
      ).alwaysActive(),
      dateRangeFilter(
        GridColumnId.transmittedOn,
        ResearchColumnPrototype.ITEM_TEST_PLAN_LASTEXECON,
      ),
    ]);
  }

  transmitSelection() {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        filter((rows) => this.filterNonEditableRows(rows).length > 0),
        switchMap((rows) => this.showConfirmDialogIfNeeded(rows)),
        switchMap((editableRows) => {
          const rowIds = this.filterNonTransmittedAutomationRequests(editableRows).map(
            (row) => row.id,
          );
          return this.automationTesterWorkspaceService.changeStatusToTransmitted(
            rowIds as number[],
          );
        }),
      )
      .subscribe(() => this.gridService.refreshData());
  }

  changeStatusToSuspended() {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        filter((rows) => this.filterNonEditableRows(rows).length > 0),
        switchMap((rows) => this.showConfirmDialogIfNeeded(rows)),
        switchMap((editableRows) => {
          const rowIds = editableRows.map((row) => row.id);
          return this.automationTesterWorkspaceService.changeStatusToSuspended(rowIds as number[]);
        }),
      )
      .subscribe(() => this.gridService.refreshData());
  }

  changeStatusToReady() {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        filter((rows) => this.filterNonEditableRows(rows).length > 0),
        switchMap((rows) => this.showConfirmDialogIfNeeded(rows)),
        switchMap((editableRows) => {
          const rowIds = editableRows.map((row) => row.id);
          return this.automationTesterWorkspaceService.changeStatusToReady(rowIds as number[]);
        }),
      )
      .subscribe(() => this.gridService.refreshData());
  }

  changeStatusToWorkInProgress() {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        filter((rows) => this.filterNonEditableRows(rows).length > 0),
        switchMap((rows) => this.showConfirmDialogIfNeeded(rows)),
        switchMap((editableRows) => {
          const rowIds = editableRows.map((row) => row.id);
          return this.automationTesterWorkspaceService.changeStatusToWorkInProgress(
            rowIds as number[],
          );
        }),
      )
      .subscribe(() => this.gridService.refreshData());
  }

  private showConfirmDialogIfNeeded(rows: DataRow[]): Observable<DataRow[]> {
    const editableRows: DataRow[] = this.filterNonEditableRows(rows);
    if (editableRows.length !== rows.length) {
      const dialogRef = this.dialogService.openAlert({
        id: 'change-automation-state-warning',
        titleKey: 'sqtm-core.automation-workspace.functional-tester-view.dialog.title',
        messageKey: 'sqtm-core.automation-workspace.functional-tester-view.dialog.message',
      });

      return dialogRef.dialogClosed$.pipe(map((_confirm) => editableRows));
    } else {
      return of(editableRows);
    }
  }

  private filterNonEditableRows(dataRows: DataRow[]) {
    return dataRows.filter((row) => canEditRow(row));
  }

  private filterNonTransmittedAutomationRequests(rows: DataRow[]): DataRow[] {
    return rows.filter(
      (row) => row.data[GridColumnId.requestStatus] !== AutomationRequestStatus.TRANSMITTED.id,
    );
  }

  shouldShowResetFilterLink(activeFilters: GridFilter[]): boolean {
    return activeFilters.length > 0;
  }

  resetFilters() {
    this.gridService.resetFilters();
  }
}
