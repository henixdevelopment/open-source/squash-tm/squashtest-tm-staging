import { AutomationRequestPermissions, ColumnDisplay, DataRow, GridColumnId } from 'sqtm-core';

export function canEditRow(row: DataRow): boolean {
  const noLockedMilestones = row.data[GridColumnId.tcMilestoneLocked] === 0;
  const permissions = row.simplePermissions as AutomationRequestPermissions;
  return permissions.canWriteAsFunctional && noLockedMilestones;
}

export function isAutomReqEditable(columnDisplay: ColumnDisplay, row: DataRow): boolean {
  const noLockedMilestones = row.data[GridColumnId.tcMilestoneLocked] === 0;
  const permissions = row.simplePermissions as AutomationRequestPermissions;
  return columnDisplay.editable && permissions.canWriteAsFunctional && noLockedMilestones;
}
