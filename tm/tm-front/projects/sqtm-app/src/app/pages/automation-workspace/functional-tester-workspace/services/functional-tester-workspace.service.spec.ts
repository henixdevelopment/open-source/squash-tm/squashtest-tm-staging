import { FunctionalTesterWorkspaceService } from './functional-tester-workspace.service';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppTestingUtilsModule } from '../../../../utils/testing-utils/app-testing-utils.module';
import { RestService } from 'sqtm-core';

describe('AutomationTesterWorkspaceService', () => {
  let service: FunctionalTesterWorkspaceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule],
      providers: [
        {
          provide: FunctionalTesterWorkspaceService,
          useClass: FunctionalTesterWorkspaceService,
          deps: [RestService],
        },
      ],
    });
    service = TestBed.inject(FunctionalTesterWorkspaceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
