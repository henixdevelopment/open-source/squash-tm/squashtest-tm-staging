import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewContainerRef,
} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  DialogService,
  GridColumnId,
  GridService,
} from 'sqtm-core';
import { SquashTfDialogConfiguration } from '../../dialogs/squash-tf-dialog/squash-tf-dialog.configuration';
import { SquashTfDialogComponent } from '../../dialogs/squash-tf-dialog/squash-tf-dialog.component';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-automation-squash-tf-renderer',
  templateUrl: './automation-squash-tf-renderer.component.html',
  styleUrls: ['./automation-squash-tf-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AutomationSquashTfRendererComponent extends AbstractCellRendererComponent {
  constructor(
    public gridService: GridService,
    cdr: ChangeDetectorRef,
    private dialogService: DialogService,
    private vcr: ViewContainerRef,
  ) {
    super(gridService, cdr);
  }

  openScriptDialog() {
    const dialog = this.dialogService.openDialog<SquashTfDialogConfiguration, any>({
      id: 'squash-tf-dialog',
      viewContainerReference: this.vcr,
      component: SquashTfDialogComponent,
      width: 600,

      data: {
        tcKind: this.row.data[GridColumnId.kind],
        testCaseId: this.row.data[GridColumnId.tclnId],
        scriptAuto: this.row.data[this.columnDisplay.id],
        uuid: this.row.data[GridColumnId.uuid],
        conflictAssociation: this.row.data[GridColumnId.conflictAssociation],
        canWrite: this.row.simplePermissions.canWrite,
        isManual: this.row.data[GridColumnId.isManual],
        taServerId: this.row.data[GridColumnId.taServerId],
        testAutomationServerKind: this.row.data[GridColumnId.testAutomationServerKind],
      },
    });

    dialog.dialogResultChanged$.pipe(takeUntil(dialog.dialogClosed$)).subscribe((result) => {
      this.row.data[GridColumnId.isManual] = true;
      return this.gridService.editRows(
        [this.row.id],
        [{ columnId: this.columnDisplay.id, value: result }],
      );
    });
  }
}

export function squashTFColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(AutomationSquashTfRendererComponent);
}
