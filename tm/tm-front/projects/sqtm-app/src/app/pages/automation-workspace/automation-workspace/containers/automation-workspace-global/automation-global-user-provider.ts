import { ResearchColumnPrototype, UserHistorySearchProvider, UserListElement } from 'sqtm-core';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { AutomationWorkspaceService } from '../../services/automation-workspace.service';
import { AutomationWorkspaceState } from '../../states/automation-workspace.state';

type userGVColumnPrototype =
  | ResearchColumnPrototype.AUTOMATION_REQUEST_ASSIGNED_TO
  | ResearchColumnPrototype.TEST_CASE_MODIFIED_BY;

@Injectable()
export class AutomationGlobalUserProvider extends UserHistorySearchProvider {
  private columnProtoToUserList: { [K in userGVColumnPrototype]: keyof AutomationWorkspaceState } =
    {
      [ResearchColumnPrototype.TEST_CASE_MODIFIED_BY]: 'usersWhoModifiedTestCasesGlobalView',
      [ResearchColumnPrototype.AUTOMATION_REQUEST_ASSIGNED_TO]: 'usersAssignedTo',
    };

  constructor(private automationWorkspaceService: AutomationWorkspaceService) {
    super();
  }

  provideUserList(columnPrototype: ResearchColumnPrototype): Observable<UserListElement[]> {
    return this.automationWorkspaceService.componentData$.pipe(
      take(1),
      map((data) =>
        data[this.columnProtoToUserList[columnPrototype]].map((u) => ({ ...u, selected: false })),
      ),
    );
  }
}
