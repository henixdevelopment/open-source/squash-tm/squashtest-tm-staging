import { AfterViewInit, ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { FunctionalTesterWorkspaceState } from '../../state/functional-tester-workspace.state';
import {
  buildFilters,
  DataRow,
  DialogService,
  Extendable,
  grid,
  GridColumnId,
  GridFilter,
  GridFilterUtils,
  GridId,
  GridService,
  gridServiceFactory,
  indexColumn,
  Limited,
  LocalPersistenceService,
  ReferentialDataService,
  ResearchColumnPrototype,
  RestService,
  selectableTextColumn,
  serverBackedGridEqualFilter,
  serverBackedGridTextFilter,
  Sort,
  StyleDefinitionBuilder,
  TestCaseKind,
  textCellWithToolTipColumn,
  textColumn,
  userHistoryResearchFilter,
  UserHistorySearchProvider,
} from 'sqtm-core';
import { FunctionalTesterWorkspaceService } from '../../services/functional-tester-workspace.service';
import {
  AFTW_READY_FOR_TRANSMISSION_TABLE,
  AFTW_READY_FOR_TRANSMISSION_TABLE_CONFIG,
} from '../../functional-tester-workspace.constant';
import { filter, map, switchMap, take, takeUntil, tap } from 'rxjs/operators';
import { automationRequestLiteralConverter } from '../../../../../components/automation/automation-utils';
import { uuidColumn } from '../../../../../components/automation/components/cell-renderers/uuid-cell-renderer/uuid-cell-renderer.component';
import { ReadyForTransmissionUserProvider } from './ready-for-transmission-user-provider';
import { canEditRow, isAutomReqEditable } from '../../functional-tester-workspace.utils';

export function aftwReadyForTransmissionTableDefinition(
  localPersistenceService: LocalPersistenceService,
) {
  return grid(GridId.FUNCTIONAL_TESTER_READY_FOR_TRANSMISSION)
    .withColumns([
      indexColumn()
        .changeWidthCalculationStrategy(new Limited(60))
        .disableSort()
        .withViewport('leftViewport'),
      textCellWithToolTipColumn(GridColumnId.projectName, 'testCasePath')
        .withI18nKey('sqtm-core.entity.project.label.singular')
        .changeWidthCalculationStrategy(new Limited(290))
        .withAssociatedFilter(),
      textColumn(GridColumnId.tclnId)
        .withI18nKey('sqtm-core.entity.generic.id.capitalize')
        .changeWidthCalculationStrategy(new Limited(60))
        .withAssociatedFilter(),
      textColumn(GridColumnId.reference)
        .withI18nKey('sqtm-core.automation-workspace.grid.headers.reference')
        .withTitleI18nKey('sqtm-core.grid.header.reference')
        .changeWidthCalculationStrategy(new Limited(125))
        .withAssociatedFilter(),
      selectableTextColumn(GridColumnId.name)
        .withI18nKey('sqtm-core.entity.test-case.label.singular')
        .changeWidthCalculationStrategy(new Limited(290))
        .withAssociatedFilter(),
      uuidColumn(GridColumnId.uuid)
        .withI18nKey('sqtm-core.entity.test-case.uuid.label')
        .changeWidthCalculationStrategy(new Limited(80))
        .disableSort(),
      textColumn(GridColumnId.kind)
        .withEnumRenderer(TestCaseKind, false, true)
        .withI18nKey('sqtm-core.entity.test-case.kind.label')
        .changeWidthCalculationStrategy(new Limited(100)),
      textColumn(GridColumnId.login)
        .withI18nKey('sqtm-core.automation-workspace.grid.headers.tester')
        .changeWidthCalculationStrategy(new Limited(200))
        .withAssociatedFilter(),
      textColumn(GridColumnId.automationPriority)
        .isEditable(isAutomReqEditable)
        .withI18nKey('sqtm-core.grid.header.priority.short')
        .withTitleI18nKey('sqtm-core.grid.header.priority.long')
        .changeWidthCalculationStrategy(new Extendable(60, 0.2))
        .withAssociatedFilter(),
    ])
    .server()
    .withServerUrl(['automation-tester-workspace/ready-for-transmission'])
    .withRowConverter(automationRequestLiteralConverter)
    .withModificationUrl(['automation-requests'])
    .disableRightToolBar()
    .withInitialSortedColumns([
      { id: GridColumnId.automationPriority, sort: Sort.DESC },
      { id: GridColumnId.kind, sort: Sort.DESC },
    ])
    .withRowHeight(35)
    .withStyle(new StyleDefinitionBuilder().enableInitialLoadAnimation().showLines())
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
}

@Component({
  selector: 'sqtm-app-ready-for-transmission-functional-tester-view',
  templateUrl: './ready-for-transmission-functional-tester-view.component.html',
  styleUrls: ['./ready-for-transmission-functional-tester-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: AFTW_READY_FOR_TRANSMISSION_TABLE_CONFIG,
      useFactory: aftwReadyForTransmissionTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: AFTW_READY_FOR_TRANSMISSION_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, AFTW_READY_FOR_TRANSMISSION_TABLE_CONFIG, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: AFTW_READY_FOR_TRANSMISSION_TABLE,
    },
    { provide: UserHistorySearchProvider, useClass: ReadyForTransmissionUserProvider },
  ],
})
export class ReadyForTransmissionFunctionalTesterViewComponent implements AfterViewInit {
  componentData$: Observable<FunctionalTesterWorkspaceState>;
  unsub$ = new Subject<void>();

  canEditRows$: Observable<boolean>;
  activeFilters$: Observable<GridFilter[]>;

  constructor(
    public referentialDataService: ReferentialDataService,
    public gridService: GridService,
    private dialogService: DialogService,
    private automationTesterWorkspaceService: FunctionalTesterWorkspaceService,
  ) {
    this.componentData$ = this.automationTesterWorkspaceService.componentData$;
    this.canEditRows$ = this.gridService.selectedRows$.pipe(
      map((rows) => {
        return this.filterNonEditableRows(rows).length > 0;
      }),
    );
    this.activeFilters$ = this.gridService.activeFilters$.pipe(
      takeUntil(this.unsub$),
      map((gridFilters: GridFilter[]) =>
        gridFilters.filter((gridFilter) => GridFilterUtils.mustIncludeFilter(gridFilter)),
      ),
    );
  }

  ngAfterViewInit() {
    this.componentData$
      .pipe(
        filter((component) => component.nbReadyForTransmissionAutomReq > 0),
        take(1),
        tap(() => this.gridService.addFilters(this.buildGridFilters())),
      )
      .subscribe(() => {
        this.gridService.refreshData();
      });
  }

  private buildGridFilters(): GridFilter[] {
    return buildFilters([
      serverBackedGridTextFilter(GridColumnId.projectName),
      serverBackedGridEqualFilter(GridColumnId.tclnId),
      serverBackedGridTextFilter(GridColumnId.reference),
      serverBackedGridTextFilter(GridColumnId.name),
      serverBackedGridTextFilter(GridColumnId.uuid),
      serverBackedGridEqualFilter(GridColumnId.automationPriority),
      userHistoryResearchFilter(
        GridColumnId.login,
        ResearchColumnPrototype.TEST_CASE_MODIFIED_BY,
      ).alwaysActive(),
    ]);
  }

  transmitSelection() {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        filter((rows) => this.filterNonEditableRows(rows).length > 0),
        switchMap((rows) => this.showConfirmDialogIfNeeded(rows)),
        switchMap((editableRows) => {
          const rowIds = editableRows.map((row) => row.id);
          return this.automationTesterWorkspaceService.changeStatusToTransmitted(
            rowIds as number[],
          );
        }),
      )
      .subscribe(() => this.gridService.refreshData());
  }

  private showConfirmDialogIfNeeded(rows: DataRow[]): Observable<DataRow[]> {
    const editableRows: DataRow[] = this.filterNonEditableRows(rows);
    if (editableRows.length !== rows.length) {
      const dialogRef = this.dialogService.openAlert({
        id: 'change-automation-state-warning',
        titleKey: 'sqtm-core.automation-workspace.functional-tester-view.dialog.title',
        messageKey: 'sqtm-core.automation-workspace.functional-tester-view.dialog.message',
      });

      return dialogRef.dialogClosed$.pipe(map((_confirm) => editableRows));
    } else {
      return of(editableRows);
    }
  }

  private filterNonEditableRows(dataRows: DataRow[]) {
    return dataRows.filter((row) => canEditRow(row));
  }

  shouldShowResetFilterLink(activeFilters: GridFilter[]): boolean {
    return activeFilters.length > 0;
  }

  resetFilters() {
    this.gridService.resetFilters();
  }
}
