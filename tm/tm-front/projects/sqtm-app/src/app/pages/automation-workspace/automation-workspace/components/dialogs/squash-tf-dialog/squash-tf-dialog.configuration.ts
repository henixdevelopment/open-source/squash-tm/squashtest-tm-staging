import { TestAutomationServerKind, TestCaseKindKeys } from 'sqtm-core';

export class SquashTfDialogConfiguration {
  tcKind: TestCaseKindKeys;
  testCaseId: number;
  scriptAuto: string;
  uuid: string;
  conflictAssociation: string;
  canWrite: boolean;
  isManual: boolean;
  taServerId: number;
  testAutomationServerKind: TestAutomationServerKind;
}
