import { ChangeDetectionStrategy, Component, OnDestroy, ViewContainerRef } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { UserAccountService } from '../../../services/user-account.service';
import { UserAccountViewState } from '../../../state/user-account.model';
import { DialogService, UsersGroupHelpers } from 'sqtm-core';
import {
  ChangeUserPasswordDialogComponent,
  ChangeUserPasswordDialogConfiguration,
} from '../../dialogs/change-user-password-dialog/change-user-password-dialog.component';

@Component({
  selector: 'sqtm-app-user-account-information-panel',
  templateUrl: './user-account-information-panel.component.html',
  styleUrls: ['./user-account-information-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserAccountInformationPanelComponent implements OnDestroy {
  currentUser$: Observable<UserAccountViewState>;

  unsub$ = new Subject<void>();

  constructor(
    public readonly userAccountService: UserAccountService,
    private readonly dialogService: DialogService,
    private readonly viewContainerReference: ViewContainerRef,
  ) {
    this.currentUser$ = userAccountService.componentData$.pipe(takeUntil(this.unsub$));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  getUserGroup(viewState: UserAccountViewState): any {
    return UsersGroupHelpers.getI18nKey(viewState.userAccount.userGroup);
  }

  getUsername(viewState: UserAccountViewState): string {
    const user = viewState.userAccount;
    if (user.firstName) {
      return `${user.firstName} ${user.lastName}`;
    }

    return user.lastName;
  }

  openChangePasswordDialog(): void {
    this.currentUser$.pipe(take(1)).subscribe((userAccount) => {
      this.dialogService.openDialog<ChangeUserPasswordDialogConfiguration, any>({
        id: 'change-user-password',
        viewContainerReference: this.viewContainerReference,
        component: ChangeUserPasswordDialogComponent,
        width: 550,
        data: { hasLocalPassword: userAccount.userAccount.hasLocalPassword },
      });
    });
  }
}
