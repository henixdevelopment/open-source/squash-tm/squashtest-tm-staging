import {
  ChangeDetectionStrategy,
  Component,
  HostListener,
  Inject,
  OnInit,
  signal,
  ViewChild,
  ViewContainerRef,
  WritableSignal,
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import {
  DatePickerComponent,
  DialogModule,
  DialogReference,
  FieldValidationError,
  KeyNames,
  ListItem,
  TextFieldComponent,
  WorkspaceCommonModule,
} from 'sqtm-core';
import { TranslateModule } from '@ngx-translate/core';
import { API_TOKEN_GENERATOR_TOKEN, ApiTokenGenerator } from './api-token-generator';

@Component({
  selector: 'sqtm-app-create-api-token-dialog',
  templateUrl: './create-api-token-dialog.component.html',
  styleUrl: './create-api-token-dialog.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [DialogModule, ReactiveFormsModule, TranslateModule, WorkspaceCommonModule, FormsModule],
})
export class CreateApiTokenDialogComponent implements OnInit {
  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  permissions: ListItem[] = [];

  selectedPermissions$: WritableSignal<string> = signal('READ');

  disabledDate = (current: Date): boolean => {
    const today: Date = new Date();
    const oneYearFromToday: Date = new Date(
      today.getFullYear() + 1,
      today.getMonth(),
      today.getDate(),
    );
    return current > oneYearFromToday || current <= today;
  };

  @ViewChild('nameField')
  nameField: TextFieldComponent;

  @ViewChild('expiryDateField')
  expiryDateField: DatePickerComponent;

  constructor(
    private fb: FormBuilder,
    public readonly dialogReference: DialogReference,
    @Inject(API_TOKEN_GENERATOR_TOKEN) private readonly apiTokenGenerator: ApiTokenGenerator,
  ) {}

  ngOnInit(): void {
    this.initializeFormGroup();

    this.permissions = [
      {
        id: 1,
        i18nLabelKey: 'sqtm-core.user-account-page.api-token.dialog.create.permissions.read',
        selected: true,
        label: 'READ',
      },
      {
        id: 2,
        i18nLabelKey: 'sqtm-core.user-account-page.api-token.dialog.create.permissions.read-write',
        selected: false,
        label: 'READ_WRITE',
      },
    ];
  }

  private initializeFormGroup() {
    const today: Date = new Date();
    const defaultExpiryDate: Date = new Date(
      today.getFullYear() + 1,
      today.getMonth(),
      today.getDate(),
    );

    this.formGroup = this.fb.group({
      name: this.fb.control('', [Validators.required, Validators.maxLength(255)]),
      expiryDate: this.fb.control(defaultExpiryDate, [Validators.required]),
    });
  }

  addApiToken() {
    if (this.formIsValid()) {
      this.apiTokenGenerator
        .generateApiToken(
          this.formGroup.controls['name'].value,
          this.selectedPermissions$(),
          new Date(this.formGroup.controls['expiryDate'].value).toISOString(),
        )
        .subscribe((newToken: string) => {
          this.dialogReference.result = newToken;
          this.dialogReference.close();
        });
    } else {
      this.showClientSideErrors();
    }
  }

  private formIsValid(): boolean {
    return this.formGroup.status === 'VALID' && this.selectedPermissions$() != null;
  }

  private showClientSideErrors(): void {
    this.nameField.showClientSideError();
    this.expiryDateField.showClientSideError();
  }

  changePermissionsSelection(newPermission: ListItem) {
    this.selectedPermissions$.update(() => newPermission.label);
  }

  @HostListener('window:keyup', ['$event'])
  handleKeyUp(event: KeyboardEvent) {
    if (event.key === KeyNames.ENTER) {
      this.addApiToken();
    } else if (event.key === KeyNames.ESCAPE) {
      this.dialogReference.close();
    }
  }
}

export function buildCreateApiTokenDialogConfiguration(vcr: ViewContainerRef) {
  return {
    component: CreateApiTokenDialogComponent,
    viewContainerReference: vcr,
    data: {
      titleKey: 'sqtm-core.user-account-page.api-token.dialog.create.title',
    },
    id: 'api-token-creation-dialog',
    width: 600,
  };
}
