import { InjectionToken } from '@angular/core';

export interface ApiTokenGenerator {
  generateApiToken(name: string, permissions: string, expiryDate: string): any;
}

export const API_TOKEN_GENERATOR_TOKEN = new InjectionToken<ApiTokenGenerator>(
  'Injection token for API token generator',
);
