import {
  ChangeDetectionStrategy,
  Component,
  HostListener,
  OnInit,
  ViewContainerRef,
} from '@angular/core';
import { DialogModule, DialogReference, KeyNames, SecretFieldComponent } from 'sqtm-core';
import { TranslateModule } from '@ngx-translate/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';

@Component({
  selector: 'sqtm-app-display-new-api-token-dialog',
  templateUrl: './display-new-api-token-dialog.component.html',
  styleUrl: './display-new-api-token-dialog.component.less',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [DialogModule, TranslateModule, NzButtonModule, SecretFieldComponent, NzIconModule],
})
export class DisplayNewApiTokenDialogComponent implements OnInit {
  newToken: string;

  constructor(public readonly dialogReference: DialogReference) {}

  ngOnInit() {
    this.newToken = this.dialogReference.data.token;
  }

  @HostListener('window:keyup', ['$event'])
  handleKeyUp(event: KeyboardEvent) {
    if (event.key === KeyNames.ESCAPE) {
      this.dialogReference.close();
    }
  }
}

export function buildDisplayNewApiTokenDialogConfiguration(token: string, vcr: ViewContainerRef) {
  return {
    component: DisplayNewApiTokenDialogComponent,
    viewContainerReference: vcr,
    data: {
      token: token,
    },
    id: 'display-new-api-token-dialog',
    width: 600,
  };
}
