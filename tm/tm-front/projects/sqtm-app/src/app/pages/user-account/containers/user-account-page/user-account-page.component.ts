import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { AuthenticatedUser, GenericEntityViewService, ReferentialDataService } from 'sqtm-core';
import { UserAccountService } from '../../services/user-account.service';
import { Observable, Subject } from 'rxjs';
import { UserAccountViewState } from '../../state/user-account.model';
import { map, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-user-account-page',
  templateUrl: './user-account-page.component.html',
  styleUrls: ['./user-account-page.component.less'],
  providers: [
    {
      provide: UserAccountService,
      useClass: UserAccountService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: UserAccountService,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserAccountPageComponent implements OnDestroy {
  workspaceName = 'administration-workspace';

  titleKey = 'sqtm-core.user-account-page.label.full';

  componentData$: Observable<UserAccountViewState>;
  authenticatedUser$: Observable<AuthenticatedUser>;

  private unsub$ = new Subject<void>();

  hasBugTrackerCredentials$: Observable<boolean>;

  constructor(
    public readonly referentialService: ReferentialDataService,
    private readonly userAccountService: UserAccountService,
  ) {
    referentialService.refresh().subscribe();
    userAccountService.load();
    this.componentData$ = userAccountService.componentData$.pipe(takeUntil(this.unsub$));
    this.authenticatedUser$ = referentialService.authenticatedUser$.pipe(takeUntil(this.unsub$));
    this.hasBugTrackerCredentials$ = userAccountService.componentData$.pipe(
      map((componentData) => componentData.userAccount.bugTrackerCredentials.length > 0),
    );
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
