import { Injectable } from '@angular/core';
import {
  AttachmentService,
  AuthenticatedUser,
  AuthenticationProtocol,
  BugTracker,
  Credentials,
  EntityViewAttachmentHelperService,
  GenericEntityViewService,
  OAuth2Credentials,
  RestService,
} from 'sqtm-core';
import {
  provideInitialUserAccountView,
  UserAccountEntityState,
  UserAccountViewState,
} from '../state/user-account.model';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { map, take, tap, withLatestFrom } from 'rxjs/operators';
import { ApiTokenGenerator } from '../components/dialogs/create-api-token-dialog/api-token-generator';

@Injectable()
export class UserAccountService
  extends GenericEntityViewService<UserAccountEntityState, 'userAccount'>
  implements ApiTokenGenerator
{
  constructor(
    protected restService: RestService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
  ) {
    super(restService, attachmentService, translateService, attachmentHelper);
  }

  getInitialState(): UserAccountViewState {
    return provideInitialUserAccountView();
  }

  load(): void {
    this.restService
      .get<UserAccountEntityState>([this.getRootUrl()])
      .subscribe((userAccount: UserAccountEntityState) => {
        this.initializeEntityState(userAccount);
      });
  }

  protected getRootUrl(_initialState?: any): string {
    return 'user-account';
  }

  changePassword(oldPassword: string, newPassword: string, initializing: boolean): Observable<any> {
    const body = { oldPassword, newPassword, initializing };
    return this.restService.post(['user-account/password'], body).pipe(
      withLatestFrom(this.componentData$),
      tap(([, state]: [any, UserAccountViewState]) => {
        const updatedState: UserAccountViewState = {
          ...state,
          userAccount: {
            ...state.userAccount,
            hasLocalPassword: true,
          },
        };
        this.store.commit(updatedState);
      }),
    );
  }

  changeBugTrackerMode(isAutomatic: boolean): Observable<any> {
    const bugTrackerMode = isAutomatic ? 'Automatic' : 'Manual';
    return this.restService.post(['user-account/bug-tracker-mode'], { bugTrackerMode }).pipe(
      withLatestFrom(this.state$),
      map(([, state]) => ({
        ...state,
        userAccount: {
          ...state.userAccount,
          bugTrackerMode,
        },
      })),
      tap((newState) => this.commit(newState)),
    );
  }

  changeBugTrackerCredentials(bugTrackerId: number, credentials: Credentials): Observable<any> {
    return this.restService
      .post(['user-account', 'bugtracker', bugTrackerId.toString(), 'credentials'], credentials)
      .pipe(
        withLatestFrom(this.state$),
        map(([, state]: [any, UserAccountViewState]) =>
          updateBugTrackerCredentials(state, bugTrackerId, { ...credentials, registered: true }),
        ),
        tap((newState) => this.commit(newState)),
      );
  }

  deleteBugTrackerCredentials(bugTracker: BugTracker): Observable<any> {
    return this.restService
      .delete(['user-account', 'bugtracker', bugTracker.id.toString(), 'credentials'])
      .pipe(
        withLatestFrom(this.state$),
        map(([, state]: [any, UserAccountViewState]) => {
          if (bugTracker.authProtocol === AuthenticationProtocol.BASIC_AUTH) {
            return updateBugTrackerCredentials(state, bugTracker.id, {
              implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
              type: AuthenticationProtocol.BASIC_AUTH,
              registered: false,
              username: '',
              password: '',
            });
          }
        }),
        tap((newState) => this.commit(newState)),
      );
  }

  getCurrentUser(): Observable<AuthenticatedUser> {
    throw new Error('Not implemented.');
  }

  openOAuthDialog(bugTrackerId: number, protocol: string): Window {
    const oauthUrl =
      this.restService.backendRootUrl + `servers/${bugTrackerId}/authentication/${protocol}`;
    const dialogConf = 'height=690, width=810, resizable, scrollbars, dialog, alwaysRaised';
    return window.open(oauthUrl, `${protocol} authorizations`, dialogConf);
  }

  askOauth2TokenToBackend(bugTrackerId: number, code: string): Observable<void> {
    return this.restService.post(
      ['user-account', 'bugtracker', bugTrackerId.toString(), 'authentication/oauth2/token'],
      {},
      { params: { code: code } },
    );
  }

  updateStateWithSuccessfulOauthTokenRegistration(bugtrackerId: number): Observable<any> {
    const registeredCreds: OAuth2Credentials = {
      accessToken: '',
      implementedProtocol: AuthenticationProtocol.OAUTH_2,
      type: AuthenticationProtocol.OAUTH_2,
      registered: true,
    };

    return this.componentData$.pipe(
      take(1),
      map((state) => updateBugTrackerCredentials(state, bugtrackerId, registeredCreds)),
      tap((nextState: UserAccountViewState) => this.store.commit(nextState)),
    );
  }

  generateApiToken(name: string, permissions: string, expiryDate: string): Observable<string> {
    return this.restService
      .post(['api-token/generate-api-token'], {
        name: name,
        expiryDate: expiryDate,
        permissions: permissions,
      })
      .pipe(map((data: { token: string }) => atob(data.token)));
  }
}

function updateBugTrackerCredentials(
  state: UserAccountViewState,
  bugTrackerId: number,
  credentials?: Credentials,
): UserAccountViewState {
  const updatedCredentials = state.userAccount.bugTrackerCredentials.map((btCredentials) => {
    if (btCredentials.bugTracker.id === bugTrackerId) {
      return {
        ...btCredentials,
        credentials,
      };
    } else {
      return { ...btCredentials };
    }
  });

  return {
    ...state,
    userAccount: {
      ...state.userAccount,
      bugTrackerCredentials: updatedCredentials,
    },
  };
}
