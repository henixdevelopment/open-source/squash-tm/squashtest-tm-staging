import {
  ChangeDetectionStrategy,
  Component,
  InjectionToken,
  Signal,
  ViewContainerRef,
} from '@angular/core';
import { UserAccountService } from '../../services/user-account.service';
import { Observable } from 'rxjs';
import { filter, map, takeUntil } from 'rxjs/operators';
import { buildCreateApiTokenDialogConfiguration } from '../../components/dialogs/create-api-token-dialog/create-api-token-dialog.component';
import { buildDisplayNewApiTokenDialogConfiguration } from '../../components/dialogs/display-new-api-token-dialog/display-new-api-token-dialog.component';
import {
  convertSqtmLiterals,
  DataRow,
  dateColumn,
  dateTimeColumn,
  dateWithExpiredInfoColumn,
  deleteColumn,
  DialogService,
  Extendable,
  GridColumnId,
  GridDefinition,
  GridService,
  gridServiceFactory,
  indexColumn,
  ProjectDataMap,
  ReferentialDataService,
  RestService,
  smallGrid,
  StyleDefinitionBuilder,
  textColumn,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { DeletePersonalApiTokenCellRendererComponent } from '../../components/cell-renderers/delete-personal-api-token-cell-renderer/delete-personal-api-token-cell-renderer.component';
import { API_TOKEN_GENERATOR_TOKEN } from '../../components/dialogs/create-api-token-dialog/api-token-generator';
import { toSignal } from '@angular/core/rxjs-interop';
import {
  ApiTokenPanelServerOperationHandler,
  DELETE_API_TOKEN_URL_INJECTION_TOKEN,
} from '../../../administration/user-workspace/user-view/components/panels/user-api-token-panel/api-token-panel-server-operation-handler.service';

export const API_TOKEN_TABLE_CONF = new InjectionToken('API_TOKEN_TABLE_CONF');
export const API_TOKEN_TABLE = new InjectionToken('API_TOKEN');

export function buildApiTokenPermissionsRowConverter(translateService: TranslateService) {
  return (literals: Partial<DataRow>[], projectDataMap: ProjectDataMap) => {
    return convertSqtmLiterals(
      literals.map((lit) => {
        return translateApiTokenPermissions(lit, translateService);
      }),
      projectDataMap,
    );
  };
}

function translateApiTokenPermissions(
  literal: Partial<DataRow>,
  translateService: TranslateService,
) {
  if (literal.data.permissions === 'READ') {
    literal.data.permissions = translateService.instant(
      'sqtm-core.user-account-page.api-token.dialog.create.permissions.read',
    );
  } else if (literal.data.permissions === 'READ_WRITE') {
    literal.data.permissions = translateService.instant(
      'sqtm-core.user-account-page.api-token.dialog.create.permissions.read-write',
    );
  } else {
    throw Error('The provided permissions are not supported.');
  }
  return literal;
}

export function apiTokenTableDefinition(translateService: TranslateService): GridDefinition {
  return smallGrid('api-token-grid')
    .withColumns([
      indexColumn(),
      textColumn(GridColumnId.name)
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5)),
      textColumn(GridColumnId.permissions)
        .withI18nKey('sqtm-core.user-account-page.api-token.grid.permissions')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5)),
      dateColumn(GridColumnId.createdOn)
        .withI18nKey('sqtm-core.entity.generic.created-on.masculine')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5)),
      dateTimeColumn(GridColumnId.lastUsage)
        .withI18nKey('sqtm-core.user-account-page.api-token.grid.last-usage')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5)),
      dateWithExpiredInfoColumn(GridColumnId.expiryDate)
        .withI18nKey('sqtm-core.user-account-page.api-token.grid.expiry-date')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5)),
      deleteColumn(DeletePersonalApiTokenCellRendererComponent),
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .withServerUrl(['user-account', 'personal-api-tokens'])
    .withRowConverter(buildApiTokenPermissionsRowConverter(translateService))
    .disableMultiSelection()
    .server()
    .build();
}

@Component({
  selector: 'sqtm-app-user-account-view',
  templateUrl: './user-account-view.component.html',
  styleUrls: ['./user-account-view.component.less'],
  providers: [
    {
      provide: DELETE_API_TOKEN_URL_INJECTION_TOKEN,
      useValue: 'api-token',
    },
    {
      provide: API_TOKEN_TABLE_CONF,
      useFactory: apiTokenTableDefinition,
      deps: [TranslateService],
    },
    {
      provide: API_TOKEN_TABLE,
      useFactory: gridServiceFactory,
      deps: [
        RestService,
        API_TOKEN_TABLE_CONF,
        ReferentialDataService,
        ApiTokenPanelServerOperationHandler,
      ],
    },
    {
      provide: ApiTokenPanelServerOperationHandler,
      useClass: ApiTokenPanelServerOperationHandler,
    },
    {
      provide: GridService,
      useExisting: API_TOKEN_TABLE,
    },
    {
      provide: API_TOKEN_GENERATOR_TOKEN,
      useExisting: UserAccountService,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserAccountViewComponent {
  public readonly hasBugTrackerCredentials$: Observable<boolean>;
  $jwtSecretDefined: Signal<boolean> = toSignal(this.referentialDataService.isJwtSecretDefined$);

  constructor(
    public readonly userAccountService: UserAccountService,
    private readonly referentialDataService: ReferentialDataService,
    private readonly dialogService: DialogService,
    private readonly vcr: ViewContainerRef,
    private readonly gridService: GridService,
  ) {
    this.hasBugTrackerCredentials$ = userAccountService.componentData$.pipe(
      map((componentData) => componentData.userAccount.bugTrackerCredentials.length > 0),
    );
  }

  openCreateTokenDialog(event: MouseEvent) {
    event.preventDefault();
    event.stopPropagation();

    if (this.$jwtSecretDefined()) {
      this.doOpenCreateApiTokenDialog();
    } else {
      this.dialogService.openAlert({
        level: 'DANGER',
        messageKey: 'sqtm-core.user-account-page.api-token.no-jwt-secret',
      });
    }
  }

  private doOpenCreateApiTokenDialog() {
    const dialogReference = this.dialogService.openDialog(
      buildCreateApiTokenDialogConfiguration(this.vcr),
    );

    dialogReference.dialogResultChanged$
      .pipe(
        takeUntil(dialogReference.dialogClosed$),
        filter((result) => result != null),
      )
      .subscribe((token: string) => {
        this.gridService.refreshData();
        this.openShowNewTokenDialog(token);
      });
  }

  private openShowNewTokenDialog(token: string) {
    this.dialogService.openDialog(buildDisplayNewApiTokenDialogConfiguration(token, this.vcr));
  }
}
