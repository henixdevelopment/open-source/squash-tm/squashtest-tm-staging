import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { TranslateModule } from '@ngx-translate/core';
import { GridModule, GridService, WorkspaceLayoutModule } from 'sqtm-core';
import { Subject } from 'rxjs';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { API_TOKEN_TABLE } from '../../../containers/user-account-view/user-account-view.component';

@Component({
  selector: 'sqtm-app-user-account-api-token-panel',
  template: `<sqtm-core-grid sqtmCoreTreeKeyboardShortcut></sqtm-core-grid>`,
  standalone: true,
  providers: [
    {
      provide: GridService,
      useExisting: API_TOKEN_TABLE,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [GridModule, NzButtonModule, TranslateModule, NzIconModule, WorkspaceLayoutModule],
})
export class UserAccountApiTokenPanelComponent implements OnInit, OnDestroy {
  private unsub$ = new Subject<void>();

  constructor(private readonly gridService: GridService) {}

  ngOnInit(): void {
    this.gridService.refreshData();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
