import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { RequirementImportLog } from 'sqtm-core';
import { ImportRequirementService } from '../../services/import-requirement.service';
import { ImportRequirementState } from '../../state/import-requirement.state';

@Component({
  selector: 'sqtm-app-import-report',
  templateUrl: './import-report.component.html',
  styleUrls: ['./import-report.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImportReportComponent {
  @Input()
  componentData: ImportRequirementState;

  constructor(private importRequirementService: ImportRequirementService) {}

  getFileName(templateOk: RequirementImportLog) {
    return this.importRequirementService.getDownLoadReportUrl(templateOk.reportUrl);
  }
}
