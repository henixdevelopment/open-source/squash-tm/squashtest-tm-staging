import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AbstractDeleteCellRenderer, DialogService, GridColumnId, GridService } from 'sqtm-core';
import { RequirementVersionViewService } from '../../../services/requirement-version-view.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-delete-verifying-test-case',
  template: `
    @if (canLink && checkIfTestCaseIsDirectlyLinked()) {
      <sqtm-core-delete-icon
        (delete)="showDeleteConfirm()"
        [iconName]="'sqtm-core-generic:unlink'"
      ></sqtm-core-delete-icon>
    }
  `,
  styleUrls: ['./delete-verifying-test-case.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteVerifyingTestCaseComponent extends AbstractDeleteCellRenderer implements OnInit {
  canLink: boolean;

  constructor(
    public grid: GridService,
    public cdr: ChangeDetectorRef,
    protected dialogService: DialogService,
    private requirementVersionService: RequirementVersionViewService,
  ) {
    super(grid, cdr, dialogService);
  }

  ngOnInit(): void {
    this.requirementVersionService.componentData$
      .pipe(takeUntil(this.unsub$))
      .subscribe((componentData) => {
        this.canLink =
          componentData.permissions.canLink &&
          componentData.requirementVersion.status !== 'OBSOLETE';
        this.cdr.detectChanges();
      });
  }

  protected doDelete(): any {
    this.grid.beginAsyncOperation();
    const testCaseId = this.row.data.id;
    this.requirementVersionService
      .removeVerifyingTestCasesById([testCaseId])
      .subscribe(() => this.grid.completeAsyncOperation());
  }

  protected getTitleKey(): string {
    return 'sqtm-core.requirement-workspace.dialog.unlink-test-case.title';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.requirement-workspace.dialog.unlink-test-case.message';
  }

  public checkIfTestCaseIsDirectlyLinked(): boolean {
    return this.row.data[GridColumnId.directlyLinked];
  }
}
