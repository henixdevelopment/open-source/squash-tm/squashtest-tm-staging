import { ChangeDetectionStrategy, Component, HostListener } from '@angular/core';
import { DialogReference, KeyNames } from 'sqtm-core';
import { DescriptionChangeDialogConfiguration } from './description-change-dialog-configuration';

@Component({
  selector: 'sqtm-app-alert-requirement-version-description-change-dialog',
  templateUrl: './description-change-dialog.component.html',
  styleUrls: ['./description-change-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DescriptionChangeDialogComponent {
  configuration: DescriptionChangeDialogConfiguration;

  constructor(
    private dialogReference: DialogReference<DescriptionChangeDialogConfiguration, boolean>,
  ) {
    this.configuration = dialogReference.data;
  }

  @HostListener('window:keyup', ['$event'])
  handleKeyUp(event: KeyboardEvent) {
    if (event.key === KeyNames.ENTER || event.key === KeyNames.ESCAPE) {
      this.dialogReference.close();
    }
  }
}
