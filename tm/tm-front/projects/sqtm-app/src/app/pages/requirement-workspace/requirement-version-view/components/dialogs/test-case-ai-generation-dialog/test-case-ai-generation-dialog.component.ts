import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  HostListener,
  signal,
  ViewChild,
  ViewContainerRef,
  WritableSignal,
} from '@angular/core';
import { TestCaseAiGenerationDialogConfiguration } from './test-case-ai-generation-dialog-configuration';
import { RequirementVersionViewService } from '../../../services/requirement-version-view.service';
import {
  ActionErrorDisplayService,
  AnchorService,
  buildTreePickerDialogDefinition,
  DataRow,
  DialogReference,
  DialogService,
  KeyNames,
  MAX_TEST_CASE_NAME_LENGTH,
  RequirementVersionService,
  SelectedTestCaseFromAiCreationData,
  TestCaseFromAi,
  TestCaseDisplayData,
  TreePickerDialogConfiguration,
  truncateTextIfTooLong,
} from 'sqtm-core';
import { catchError, concatMap, filter, switchMap, take, takeUntil, tap } from 'rxjs/operators';
import { BehaviorSubject, Subject, throwError } from 'rxjs';
import { RequirementVersionViewComponentData } from '../../../containers/requirement-version-view/requirement-version-view.component';

@Component({
  selector: 'sqtm-app-test-case-ai-generation-dialog',
  templateUrl: './test-case-ai-generation-dialog.component.html',
  styleUrl: './test-case-ai-generation-dialog.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestCaseAiGenerationDialogComponent implements AfterViewInit {
  componentData: RequirementVersionViewComponentData;
  testCasesDisplayData: TestCaseDisplayData[] = [];
  requirementVersionId: number;
  projectId: number;
  testCasesDestination: { projectId: number; dataRowId: string };
  tclnId: number;
  folderName: string;

  readonly isLoading$ = new BehaviorSubject<boolean>(false);
  private unsub$ = new Subject<void>();

  readonly shouldDisplayNoFolderSelectedError$ = signal(false);
  readonly shouldDisplayNoTestCaseSelectedError$ = signal(false);
  readonly isTestCaseGenerated$ = signal(false);

  destinationFolderPath$: WritableSignal<string> = signal(null);
  errorFromServer$: WritableSignal<boolean> = signal(false);

  @ViewChild('generateButton', { read: ElementRef })
  generateButton: ElementRef<HTMLButtonElement>;

  constructor(
    private dialogReference: DialogReference<TestCaseAiGenerationDialogConfiguration>,
    private dialogService: DialogService,
    private requirementVersionViewService: RequirementVersionViewService,
    private actionErrorDisplayService: ActionErrorDisplayService,
    private anchorService: AnchorService,
    private requirementVersionService: RequirementVersionService,
    private vcr: ViewContainerRef,
  ) {
    this.componentData = this.dialogReference.data.componentData;
    this.requirementVersionId = this.componentData.requirementVersion.id;
    this.projectId = this.componentData.requirementVersion.projectId;
  }

  ngAfterViewInit() {
    this.generateButton.nativeElement.focus();
  }

  sendApiRequest() {
    this.isTestCaseGenerated$.set(false);
    this.errorFromServer$.set(false);
    this.testCasesDisplayData = [];

    this.isLoading$.next(true);

    this.requirementVersionViewService
      .generateTestCaseWithAi(this.componentData.requirementVersion.description, this.projectId)
      .pipe(
        catchError((error) => {
          this.isLoading$.next(false);
          this.errorFromServer$.set(true);
          throw error;
        }),
      )
      .subscribe((response: { testCases: TestCaseFromAi[] }) => {
        this.isLoading$.next(false);
        this.createTestCasesDisplayData(response);
        this.isTestCaseGenerated$.set(true);
      });
  }

  private createTestCasesDisplayData(testCasesReceivedFromAi: { testCases: TestCaseFromAi[] }) {
    const testCasesToDisplay = testCasesReceivedFromAi.testCases;
    this.testCasesDisplayData = testCasesToDisplay.map(
      (testCase: TestCaseFromAi, index: number) => {
        return this.createTestCaseDisplayData(testCase, index, testCasesToDisplay.length);
      },
    );
  }

  private createTestCaseDisplayData(
    testCaseFromAi: TestCaseFromAi,
    index: number,
    testCasesArrayLength: number,
  ): TestCaseDisplayData {
    return {
      name: truncateTextIfTooLong(testCaseFromAi.name, MAX_TEST_CASE_NAME_LENGTH),
      description: testCaseFromAi.description,
      prerequisites: testCaseFromAi.prerequisites,
      testSteps: testCaseFromAi.testSteps,
      index: index,
      isLastIndex: index === testCasesArrayLength - 1,
      isSelectedByUser: false,
    };
  }

  confirm() {
    const hasAtLeastOneTestCaseSelected = this.testCasesDisplayData.some(
      (testCase) => testCase.isSelectedByUser,
    );

    if (!this.testCasesDestination) {
      this.shouldDisplayNoFolderSelectedError$.set(true);
    } else if (!hasAtLeastOneTestCaseSelected) {
      this.shouldDisplayNoTestCaseSelectedError$.set(true);
    } else {
      this.createTestCases();
    }
  }

  private createTestCases() {
    this.requirementVersionViewService
      .createTestCasesFromAi(
        this.getSelectedTestCases(),
        this.requirementVersionId,
        this.testCasesDestination,
      )
      .pipe(
        catchError((err) => {
          this.actionErrorDisplayService.handleActionError(err);
          return throwError(() => err);
        }),
        switchMap(() => this.requirementVersionService.loadVersion(this.requirementVersionId)),
        tap((versionModel) => this.requirementVersionViewService.load(versionModel)),
      )
      .subscribe(() => {
        this.dialogReference.close();
        this.anchorService.scrollToAnchor(this.dialogReference.data.viewId, 'linked-test-case');
      });
  }

  private getSelectedTestCases(): SelectedTestCaseFromAiCreationData[] {
    return this.testCasesDisplayData
      .filter((testCase) => testCase.isSelectedByUser)
      .map((selectedTestCase) => ({
        requirementVersionId: this.requirementVersionId,
        name: selectedTestCase.name,
        description: selectedTestCase.description,
        prerequisites: selectedTestCase.prerequisites,
        steps: selectedTestCase.testSteps,
      }));
  }

  updateTestCaseSelection(testCase: TestCaseDisplayData) {
    testCase.isSelectedByUser = !testCase.isSelectedByUser;
    if (testCase.isSelectedByUser) {
      this.shouldDisplayNoTestCaseSelectedError$.set(false);
    }
  }

  openTreePicker() {
    const dialogReference = this.dialogService.openDialog<TreePickerDialogConfiguration, DataRow[]>(
      buildTreePickerDialogDefinition(
        'test-case-folder-picker-dialog',
        null,
        false,
        null,
        this.vcr,
      ),
    );

    dialogReference.dialogClosed$
      .pipe(
        takeUntil(this.unsub$),
        take(1),
        filter((result) => Boolean(result)),
        tap((selectedRows: DataRow[]) => {
          const dataRow = selectedRows[0];
          this.testCasesDestination = {
            projectId: dataRow.projectId,
            dataRowId: dataRow.id.toString(),
          };
          this.folderName = dataRow.data.NAME;
          if (this.testCasesDestination.dataRowId.includes('TestCaseFolder')) {
            this.tclnId = dataRow.data.TCLN_ID;
          } else {
            this.tclnId = null;
            this.destinationFolderPath$.set(this.folderName);
            this.shouldDisplayNoFolderSelectedError$.set(false);
          }
        }),
        filter(() => this.tclnId !== null),
        concatMap(() => {
          return this.requirementVersionViewService.getChosenFolderFullPath(this.tclnId);
        }),
        tap((path) => {
          this.destinationFolderPath$.set(path.folderPath + ' > ' + this.folderName);
          this.shouldDisplayNoFolderSelectedError$.set(false);
        }),
      )
      .subscribe();
  }

  @HostListener('window:keyup', ['$event'])
  handleKeyUp(event: KeyboardEvent) {
    if (event.key === KeyNames.ESCAPE) {
      this.dialogReference.close();
    }
  }
}
