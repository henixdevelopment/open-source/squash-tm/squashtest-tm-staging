import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { RequirementMilestoneViewState } from '../../state/requirement-milestone-view-state';
import { RequirementMilestoneViewService } from '../../service/requirement-milestone-view.service';
import { ReferentialDataService } from 'sqtm-core';
import { filter, take, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-requirement-milestone-view',
  templateUrl: './requirement-milestone-view.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [RequirementMilestoneViewService],
})
export class RequirementMilestoneViewComponent implements OnInit, OnDestroy {
  public componentData$: Observable<Readonly<RequirementMilestoneViewState>>;

  private unsub$ = new Subject<void>();

  constructor(
    private viewService: RequirementMilestoneViewService,
    private referentialDataService: ReferentialDataService,
    private cdRef: ChangeDetectorRef,
  ) {}

  ngOnInit(): void {
    this.referentialDataService.loaded$
      .pipe(
        takeUntil(this.unsub$),
        filter((loaded) => loaded),
        take(1),
      )
      .subscribe(() => {
        this.viewService.init();
        this.componentData$ = this.viewService.componentData$;
        this.cdRef.detectChanges();
      });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
