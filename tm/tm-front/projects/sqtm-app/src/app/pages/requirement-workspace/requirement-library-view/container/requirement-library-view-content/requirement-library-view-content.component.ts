import { ChangeDetectionStrategy, Component, OnDestroy, Signal } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { RequirementLibraryViewService } from '../../service/requirement-library-view.service';
import { RequirementLibraryViewComponentData } from '../requirement-library-view/requirement-library-view.component';
import {
  CustomDashboardBinding,
  CustomDashboardModel,
  EntityRowReference,
  EntityScope,
  ReferentialDataService,
  SquashTmDataRowType,
} from 'sqtm-core';
import { RequirementLibraryState } from '../../state/requirement-library.state';
import { RequirementViewService } from '../../../services/requirement-view.service';

@Component({
  selector: 'sqtm-app-requirement-library-view-content',
  templateUrl: './requirement-library-view-content.component.html',
  styleUrls: ['./requirement-library-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RequirementLibraryViewContentComponent implements OnDestroy {
  $extendHighLvlReqScope: Signal<boolean>;
  private unsub$ = new Subject<void>();
  componentData$: Observable<RequirementLibraryViewComponentData>;

  constructor(
    private requirementLibraryViewService: RequirementLibraryViewService,
    protected requirementViewService: RequirementViewService,
    public referentialDataService: ReferentialDataService,
  ) {
    this.$extendHighLvlReqScope = requirementViewService.$extendHighLvlReqScope;
    this.componentData$ = requirementLibraryViewService.componentData$;
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  refreshStats($event: MouseEvent, componentData: RequirementLibraryViewComponentData) {
    $event.stopPropagation();

    this.doRefreshDashboard(componentData.requirementLibrary);
  }

  getStatisticScope(requirementLibraryState: RequirementLibraryState): EntityScope[] {
    const ref = new EntityRowReference(
      requirementLibraryState.id,
      SquashTmDataRowType.RequirementLibrary,
    ).asString();
    return [
      {
        id: ref,
        label: requirementLibraryState.name,
        projectId: requirementLibraryState.projectId,
      },
    ];
  }

  displayFavoriteDashboard($event: MouseEvent, extendHighLvlReqScope: boolean) {
    $event.stopPropagation();
    this.requirementLibraryViewService.changeDashboardToDisplay('dashboard', extendHighLvlReqScope);
  }

  displayDefaultDashboard($event: MouseEvent, extendHighLvlReqScope: boolean) {
    $event.stopPropagation();
    this.requirementLibraryViewService.changeDashboardToDisplay('default', extendHighLvlReqScope);
  }

  getChartBindings(dashboard: CustomDashboardModel) {
    return [...dashboard.chartBindings, ...dashboard.reportBindings] as CustomDashboardBinding[];
  }

  changeExtendedScope(extendedScope: boolean, requirementLibrary: RequirementLibraryState) {
    this.requirementViewService.setExtendHighLvlReqScope(extendedScope);
    this.doRefreshDashboard(requirementLibrary);
  }

  private doRefreshDashboard(requirementLibrary: RequirementLibraryState) {
    if (requirementLibrary.statistics) {
      this.requirementLibraryViewService.refreshStatistics(this.$extendHighLvlReqScope());
    }
    if (requirementLibrary.dashboard) {
      this.requirementLibraryViewService.refreshDashboard(this.$extendHighLvlReqScope());
    }
  }
}
