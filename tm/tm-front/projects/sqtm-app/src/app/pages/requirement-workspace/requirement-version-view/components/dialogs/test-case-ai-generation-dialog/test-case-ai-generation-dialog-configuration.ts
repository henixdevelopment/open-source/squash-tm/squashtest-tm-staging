import { RequirementVersionViewComponentData } from '../../../containers/requirement-version-view/requirement-version-view.component';

export interface TestCaseAiGenerationDialogConfiguration {
  componentData: RequirementVersionViewComponentData;
  viewId: string;
}
