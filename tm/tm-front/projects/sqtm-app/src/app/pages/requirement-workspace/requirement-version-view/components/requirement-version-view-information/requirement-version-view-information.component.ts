import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { RequirementVersionViewComponentData } from '../../containers/requirement-version-view/requirement-version-view.component';
import {
  ActionErrorDisplayService,
  CustomFieldData,
  EditableSelectLevelEnumFieldComponent,
  getSupportedBrowserLang,
  InfoList,
  ReferentialDataService,
  RequirementStatus,
  RequirementStatusLevelEnumItem,
  SynchronizationPluginId,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { DatePipe } from '@angular/common';
import { RequirementVersionViewService } from '../../services/requirement-version-view.service';
import { Observable, Subject } from 'rxjs';
import { catchError, finalize, map, take, takeUntil } from 'rxjs/operators';
import { RequirementVersionState } from '../../state/requirement-version-view.state';

@Component({
  selector: 'sqtm-app-requirement-view-information',
  templateUrl: './requirement-version-view-information.component.html',
  styleUrls: ['./requirement-version-view-information.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe],
})
export class RequirementVersionViewInformationComponent implements OnInit, OnDestroy {
  readonly SQTM_CORE_INFOLIST_ITEM_ICON_PREFIX = 'sqtm-core-infolist-item';

  @Input()
  set requirementVersionViewComponentData(
    requirementVersionViewComponentData: RequirementVersionViewComponentData,
  ) {
    this._requirementVersionViewComponentData = requirementVersionViewComponentData;
    this.syncStatus = `sqtm-core.entity.requirement.synchronization-status.${this._requirementVersionViewComponentData.requirementVersion.syncStatus}`;
  }

  @Input()
  customFieldData: CustomFieldData[];

  @ViewChild('statusField')
  statusField: EditableSelectLevelEnumFieldComponent;

  @ViewChild('criticalityField')
  criticalityField: EditableSelectLevelEnumFieldComponent;

  milestoneFeatureEnabled$: Observable<boolean>;

  syncStatus: string;

  multipleSyncReqsOnSameRemoteIdWarning = '';

  unsub$ = new Subject<void>();

  private _requirementVersionViewComponentData: RequirementVersionViewComponentData;

  get requirementVersionViewComponentData(): RequirementVersionViewComponentData {
    return this._requirementVersionViewComponentData;
  }

  constructor(
    private translateService: TranslateService,
    private datePipe: DatePipe,
    private requirementVersionViewService: RequirementVersionViewService,
    public readonly referentialDataService: ReferentialDataService,
    private actionErrorDisplayService: ActionErrorDisplayService,
  ) {}

  ngOnInit(): void {
    this.milestoneFeatureEnabled$ = this.referentialDataService.globalConfiguration$.pipe(
      take(1),
      map((globalConfiguration) => globalConfiguration.milestoneFeatureEnabled),
    );

    this.initializeMultipleSyncWarningMessage();
    this.initializeRefreshStatusFieldObserver();
  }

  private initializeMultipleSyncWarningMessage(): void {
    if (this.hasMoreThanOneSyncReqIdForRemoteSyncId()) {
      const reqVersionState: RequirementVersionState =
        this.requirementVersionViewComponentData.requirementVersion;
      this.multipleSyncReqsOnSameRemoteIdWarning = this.translateService.instant(
        'sqtm-core.entity.requirement.multiple-syncs-on-same-remote-id-warning',
        {
          remoteReqId:
            reqVersionState.remoteSynchronisationKind === SynchronizationPluginId.XSQUASH4GITLAB
              ? this.extractGitlabProjectAndIidFromUrl(reqVersionState.remoteReqUrl)
              : reqVersionState.remoteReqId,
          serverName: reqVersionState.serverName,
          syncReqIdsForRemoteSyncId: reqVersionState.syncReqIdsForRemoteSyncId.join(', '),
        },
      );
    }
  }

  public hasMoreThanOneSyncReqIdForRemoteSyncId(): boolean {
    return (
      this.requirementVersionViewComponentData.requirementVersion.syncReqIdsForRemoteSyncId &&
      this.requirementVersionViewComponentData.requirementVersion.syncReqIdsForRemoteSyncId.length >
        1
    );
  }

  getAuditableText(date: Date, userLogin: string) {
    if (date != null) {
      const formattedDate = this.formatDate(date);
      return `${formattedDate} (${userLogin})`;
    } else {
      return this.translateService.instant('sqtm-core.generic.label.never');
    }
  }

  private formatDate(date: Date): string {
    return this.datePipe.transform(date, 'short', getSupportedBrowserLang(this.translateService));
  }

  deleteMilestone(id: number, milestoneId: number) {
    this.requirementVersionViewService.unbindMilestone(id, milestoneId);
  }

  bindMilestones(id: number, milestoneIds: number[]) {
    this.requirementVersionViewService.bindMilestones(id, milestoneIds);
  }

  trackCfd(cfd: CustomFieldData) {
    return cfd.id;
  }

  getIcon(requirementCategory: InfoList, categoryId: number) {
    const items = requirementCategory.items;
    const category = items.find((item) => item.id === categoryId);
    if (category.iconName !== '' && category.iconName !== 'noicon') {
      return `${this.SQTM_CORE_INFOLIST_ITEM_ICON_PREFIX}:${category.iconName}`;
    } else {
      return '';
    }
  }

  allowRequirementModification(componentData: RequirementVersionViewComponentData) {
    return (
      RequirementStatus[componentData.requirementVersion.status].allowModifications &&
      componentData.milestonesAllowModification
    );
  }

  updateStatus(status: RequirementStatusLevelEnumItem<any>) {
    this.requirementVersionViewService
      .updateRequirementStatus(status.id)
      .pipe(
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => {
          this.statusField.endAsync();
          this.statusField.disableEditMode();
          this.statusField.markForCheck();
        }),
      )
      .subscribe();
  }

  get remoteReqPerimeterStatus() {
    return `sqtm-core.entity.requirement.remote-req-perimeter-status.${this._requirementVersionViewComponentData.requirementVersion.remoteReqPerimeterStatus}`;
  }

  private initializeRefreshStatusFieldObserver() {
    this.requirementVersionViewService.refreshStatusField$
      .pipe(takeUntil(this.unsub$))
      .subscribe(() => this.statusField.markForCheck());
  }

  extractGitlabProjectAndIidFromUrl(url: string) {
    if (!url) {
      return '';
    }
    const splittedUrl: string[] = url.split('/');
    return splittedUrl[splittedUrl.length - 4] + '#' + splittedUrl[splittedUrl.length - 1];
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  protected readonly SynchronizationPluginId = SynchronizationPluginId;
}
