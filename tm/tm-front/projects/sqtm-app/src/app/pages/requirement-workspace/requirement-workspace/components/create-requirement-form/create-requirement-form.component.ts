import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import {
  AbstractCreateEntityForm,
  DisplayOption,
  EntityFormModel,
  InfoListItem,
  levelEnumToOptions,
  ReferentialDataService,
  RequirementCriticality,
} from 'sqtm-core';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-create-requirement-form',
  templateUrl: './create-requirement-form.component.html',
  styleUrls: ['./create-requirement-form.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateRequirementFormComponent extends AbstractCreateEntityForm implements OnInit {
  CRITICALITY_FIELD_NAME = 'criticality';
  CATEGORY_FIELD_NAME = 'category';
  infoListItemsAsOptions$: Observable<DisplayOption[]>;
  defaultItem: InfoListItem;

  get criticalityOptions(): DisplayOption[] {
    return levelEnumToOptions(RequirementCriticality);
  }

  infoListItem$: Observable<InfoListItem[]>;

  constructor(
    protected fb: FormBuilder,
    protected translateService: TranslateService,
    protected referentialDataService: ReferentialDataService,
    protected cdr: ChangeDetectorRef,
  ) {
    super(fb, translateService, referentialDataService, cdr);
  }

  ngOnInit() {
    this.infoListItem$ = this.referentialDataService.connectToProjectData(this.data.projectId).pipe(
      take(1),
      map((projectData) => projectData.requirementCategory.items),
    );
    this.infoListItemsAsOptions$ = this.infoListItem$.pipe(
      map((items) =>
        items.map((item) => ({
          id: item.code,
          label: isSystemLabel(item.label) ? `sqtm-core.entity.${item.label}` : item.label,
        })),
      ),
    );
    super.ngOnInit();
  }

  protected initializeAdditionalField() {
    this.formGroup.addControl(
      this.CRITICALITY_FIELD_NAME,
      this.fb.control(RequirementCriticality.MINOR.id),
    );
    this.infoListItem$.pipe(take(1)).subscribe((infoListItems) => {
      this.defaultItem = infoListItems.find((item) => item.default);
      this.formGroup.addControl(this.CATEGORY_FIELD_NAME, this.fb.control(this.defaultItem.code));
      this.cdr.detectChanges();
    });
  }

  protected addAdditionalField(formModel: Partial<EntityFormModel>) {
    formModel[this.CRITICALITY_FIELD_NAME] = this.formGroup.get(this.CRITICALITY_FIELD_NAME).value;
    formModel[this.CATEGORY_FIELD_NAME] = this.formGroup.get(this.CATEGORY_FIELD_NAME).value;
  }

  protected resetAdditionalField() {
    this.formGroup.get(this.CRITICALITY_FIELD_NAME).reset(RequirementCriticality.MINOR.id);
    this.formGroup.get(this.CATEGORY_FIELD_NAME).reset(this.defaultItem.code);
  }
}

function isSystemLabel(rawLabel: string): boolean {
  return rawLabel.includes('requirement.category.');
}
