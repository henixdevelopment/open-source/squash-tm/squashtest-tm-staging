import { sqtmAppLogger } from '../../../../app-logger';

export const highLevelRequirementViewLogger = sqtmAppLogger.compose('high-level-requirement-view');
