import { createEntityAdapter } from '@ngrx/entity';
import {
  InfoListItem,
  ManagementMode,
  SimpleUser,
  SprintReqVersionValidationStatusKeys,
  SprintStatusKeys,
  SqtmEntityState,
} from 'sqtm-core';

export interface SprintReqVersionState extends SqtmEntityState {
  name: string;
  reference: string;
  createdOn: Date;
  createdBy: string;
  lastModifiedBy: string;
  lastModifiedOn: Date;
  categoryId: number;
  categoryLabel: string;
  criticality: string;
  status: string;
  versionId: number;
  requirementId: number;
  categoryInfoListItem: InfoListItem[];
  testPlanId: number;
  description: string;
  mode: ManagementMode;
  nbIssues: number;
  assignableUsers: SimpleUser[];
  sprintStatus: SprintStatusKeys;
  validationStatus: SprintReqVersionValidationStatusKeys;
  remoteReqUrl: string;
  remoteReqState: string;
}

export const sprintReqVersionEntityAdapter = createEntityAdapter<SprintReqVersionState>({
  selectId: (sprintReqVersion) => sprintReqVersion.id,
});

export const sprintReqVersionEntitySelectors = sprintReqVersionEntityAdapter.getSelectors();
