import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { RequirementMultiSelectionService } from '../../services/requirement-multi-selection.service';
import { PartyPreferencesService, RestService } from 'sqtm-core';
import { Observable } from 'rxjs';
import { RequirementMultiViewState } from '../../state/requirement-multi-view.state';

@Component({
  selector: 'sqtm-app-requirement-workspace-multi-select-view',
  templateUrl: './requirement-workspace-multi-select-view.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: RequirementMultiSelectionService,
      useClass: RequirementMultiSelectionService,
      deps: [RestService, PartyPreferencesService],
    },
  ],
})
export class RequirementWorkspaceMultiSelectViewComponent implements OnInit, OnDestroy {
  public componentData$: Observable<RequirementMultiViewState>;

  constructor(private viewService: RequirementMultiSelectionService) {}

  ngOnInit(): void {
    this.componentData$ = this.viewService.componentData$;
  }

  ngOnDestroy(): void {
    this.viewService.complete();
  }
}
