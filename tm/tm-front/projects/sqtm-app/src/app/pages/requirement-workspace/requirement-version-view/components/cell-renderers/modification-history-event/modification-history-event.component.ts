import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Type } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  Extendable,
  GridColumnId,
  GridService,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-modification-history',
  template: `
    @if (row) {
      <div class="full-width full-height flex-column">
        <span class="txt-ellipsis m-auto-0" nz-tooltip [sqtmCoreLabelTooltip]="displayString">{{
          displayString
        }}</span>
      </div>
    }
  `,
  styleUrls: ['./modification-history-event.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModificationHistoryEventComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    private translator: TranslateService,
  ) {
    super(grid, cdRef);
  }

  get displayString(): string {
    const event = this.row.data[GridColumnId.event];
    if (event == null) {
      return this.displaySynReqEvent();
    }
    const modificationPrefix = this.translator.instant(
      'sqtm-core.entity.requirement.requirement-version.modification-history.action.modification',
    );
    if (['category', 'criticality', 'name', 'reference', 'status', 'description'].includes(event)) {
      const target = this.translator.instant(
        'sqtm-core.entity.requirement.requirement-version.modification-history.element.' + event,
      );
      return modificationPrefix + ' ' + target;
    }
    return event;
  }

  private displaySynReqEvent() {
    if (this.row.data[GridColumnId.syncReqCreationSource] != null) {
      return this.translator.instant(
        'sqtm-core.entity.requirement.requirement-version.modification-history.action.sync-creation',
      );
    }
    if (this.row.data[GridColumnId.syncReqUpdateSource] != null) {
      return this.translator.instant(
        'sqtm-core.entity.requirement.requirement-version.modification-history.action.sync-modification',
      );
    }
    return '';
  }
}

export function eventColumn(
  renderer: Type<any>,
  id = GridColumnId.event,
  label = 'event',
): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(renderer)
    .withLabel(label)
    .changeWidthCalculationStrategy(new Extendable(100));
}
