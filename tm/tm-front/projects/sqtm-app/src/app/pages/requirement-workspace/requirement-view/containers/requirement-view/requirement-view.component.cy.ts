import { RequirementViewComponent } from './requirement-view.component';
import { APP_BASE_HREF } from '@angular/common';
import {
  BACKEND_CONTEXT_PATH,
  RestService,
  WorkspaceCommonModule,
  WorkspaceWithTreeComponent,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';

it('mount', () => {
  cy.mount(RequirementViewComponent, {
    providers: PROVIDERS,
    imports: [WorkspaceCommonModule],
  });
});

const PROVIDERS = [
  {
    provide: APP_BASE_HREF,
    useFactory: () => '/',
  },
  {
    provide: BACKEND_CONTEXT_PATH,
    useFactory: () => '/',
  },
  { provide: RestService, useValue: {} },
  { provide: TranslateService, useValue: {} },
  { provide: ActivatedRoute, useValue: {} },
  { provide: WorkspaceWithTreeComponent, useValue: {} },
];
