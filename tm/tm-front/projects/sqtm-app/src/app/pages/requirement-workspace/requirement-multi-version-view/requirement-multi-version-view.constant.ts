import { InjectionToken } from '@angular/core';
import { GridDefinition, GridService } from 'sqtm-core';

export const REQ_VERSIONS_WS_GRID = new InjectionToken<GridService>(
  'Grid service instance for the main workspace grid',
);

export const REQ_VERSIONS_WS_GRID_CONFIG = new InjectionToken<GridDefinition>(
  'Grid config instance for the main workspace grid',
);
