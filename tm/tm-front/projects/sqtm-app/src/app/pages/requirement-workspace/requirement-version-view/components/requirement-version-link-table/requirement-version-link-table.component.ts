import { ChangeDetectionStrategy, Component } from '@angular/core';
import {
  column,
  deleteColumn,
  Fixed,
  GridColumnId,
  GridDefinition,
  GridId,
  GridService,
  indexColumn,
  Limited,
  LocalPersistenceService,
  milestoneLabelColumn,
  requirementRoleColumn,
  smallGrid,
  Sort,
  StyleDefinitionBuilder,
  textCellWithToolTipColumn,
  textColumn,
  withLinkColumn,
} from 'sqtm-core';
import { RVW_REQUIREMENT_VERSION_LINK_TABLE } from '../../requirement-version-view.constant';
import { DeleteRequirementLinkComponent } from '../cell-renderers/delete-requirement-link/delete-requirement-link.component';
import { ModifyRequirementLinkComponent } from '../cell-renderers/modify-requirement-link/modify-requirement-link.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-requirement-version-link-table',
  template: ` <sqtm-core-grid></sqtm-core-grid> `,
  styleUrls: ['./requirement-version-link-table.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: RVW_REQUIREMENT_VERSION_LINK_TABLE,
    },
  ],
})
export class RequirementVersionLinkTableComponent {}

export function rvwRequirementVersionLinkTableDefinition(
  translateService: TranslateService,
  localPersistenceService: LocalPersistenceService,
): GridDefinition {
  return smallGrid(GridId.REQUIREMENT_VERSION_VIEW_LINK)
    .withColumns([
      indexColumn().withViewport('leftViewport'),
      textCellWithToolTipColumn(GridColumnId.projectName, 'path')
        .changeWidthCalculationStrategy(new Limited(200))
        .withI18nKey('sqtm-core.entity.project.label.singular'),
      textColumn(GridColumnId.reference)
        .changeWidthCalculationStrategy(new Limited(120))
        .withI18nKey('sqtm-core.entity.generic.reference.label'),
      withLinkColumn(GridColumnId.name, {
        kind: 'link',
        baseUrl: '/requirement-workspace/requirement-version/detail',
        columnParamId: 'id',
      })
        .changeWidthCalculationStrategy(new Limited(350))
        .withI18nKey('sqtm-core.entity.requirement.label.singular'),
      milestoneLabelColumn(GridColumnId.milestoneLabels).changeWidthCalculationStrategy(
        new Limited(150),
      ),
      textColumn(GridColumnId.versionNumber)
        .withI18nKey('sqtm-core.entity.requirement.version-number.short')
        .withHeaderPosition('center')
        .withContentPosition('center')
        .changeWidthCalculationStrategy(new Fixed(100)),
      requirementRoleColumn(GridColumnId.role)
        .isEditable(false)
        .withI18nKey('sqtm-core.entity.requirement.requirement-version.link.role.label')
        .withHeaderPosition('center')
        .changeWidthCalculationStrategy(new Fixed(100))
        .withSortFunction(buildSortRole(translateService)),
      column(GridColumnId.edit)
        .withViewport('rightViewport')
        .withRenderer(ModifyRequirementLinkComponent)
        .withLabel('')
        .changeWidthCalculationStrategy(new Fixed(40)),
      deleteColumn(DeleteRequirementLinkComponent).withViewport('rightViewport'),
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .withInitialSortedColumns([{ id: GridColumnId.role, sort: Sort.ASC }])
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
}

export function buildSortRole(translateService: TranslateService) {
  return (keyA: any, keyB: any) => sortRole(keyA, keyB, translateService);
}

function sortRole(roleA: string, roleB: string, translateService: TranslateService) {
  const translatedRoleA = getTranslatedRole(roleA, translateService);
  const translatedRoleB = getTranslatedRole(roleB, translateService);
  return translatedRoleA.localeCompare(translatedRoleB);
}

function getTranslatedRole(role: string, translateService: TranslateService): string {
  if (role.includes('requirement-version.link.type')) {
    return translateService.instant('sqtm-core.entity.requirement.' + role);
  } else {
    return role;
  }
}
