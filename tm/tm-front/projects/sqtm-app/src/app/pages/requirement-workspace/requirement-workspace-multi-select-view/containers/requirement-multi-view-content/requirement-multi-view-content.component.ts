import { ChangeDetectionStrategy, Component, OnInit, Signal } from '@angular/core';
import { RequirementMultiViewState } from '../../state/requirement-multi-view.state';
import { Observable } from 'rxjs';
import { RequirementMultiSelectionService } from '../../services/requirement-multi-selection.service';
import {
  CustomDashboardBinding,
  FavoriteDashboardValue,
  GridService,
  ReferentialDataService,
} from 'sqtm-core';
import { filter, switchMap, take } from 'rxjs/operators';
import { RequirementViewService } from '../../../services/requirement-view.service';

@Component({
  selector: 'sqtm-app-requirement-multi-view-content',
  templateUrl: './requirement-multi-view-content.component.html',
  styleUrls: ['./requirement-multi-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RequirementMultiViewContentComponent implements OnInit {
  $extendHighLvlReqScope: Signal<boolean>;
  componentData$: Observable<RequirementMultiViewState>;

  constructor(
    public referentialDataService: ReferentialDataService,
    protected requirementViewService: RequirementViewService,
    private viewService: RequirementMultiSelectionService,
    private tree: GridService,
  ) {
    this.$extendHighLvlReqScope = requirementViewService.$extendHighLvlReqScope;
  }

  ngOnInit(): void {
    this.componentData$ = this.viewService.componentData$;
  }

  getStatisticsScope(componentData: RequirementMultiViewState) {
    return componentData.scope;
  }

  refreshStats($event: MouseEvent, extendedScope: boolean) {
    this.doRefreshDashboard(extendedScope);
    $event.stopPropagation();
  }

  displayFavoriteDashboard($event: MouseEvent, extendedScope: boolean) {
    $event.stopPropagation();
    this.changeDashboardToDisplay('dashboard', extendedScope);
  }

  displayDefaultDashboard($event: MouseEvent, extendedScope: boolean) {
    $event.stopPropagation();
    this.changeDashboardToDisplay('default', extendedScope);
  }

  private changeDashboardToDisplay(
    preferenceValue: FavoriteDashboardValue,
    extendedScope: boolean,
  ) {
    this.viewService
      .changeDashboardToDisplay(preferenceValue)
      .pipe(
        take(1),
        switchMap(() => this.tree.selectedRows$),
        filter((rows) => rows.length > 1),
      )
      .subscribe((rows) => {
        this.viewService.init(rows, extendedScope);
      });
  }

  getChartBindings(dashboard: any) {
    return [...dashboard.chartBindings, ...dashboard.reportBindings] as CustomDashboardBinding[];
  }

  changeExtendedScope(extendedScope: boolean): void {
    this.requirementViewService.setExtendHighLvlReqScope(extendedScope);
    this.doRefreshDashboard(this.$extendHighLvlReqScope());
  }

  private doRefreshDashboard(extendedScope: boolean) {
    this.tree.selectedRows$
      .pipe(
        take(1),
        filter((rows) => rows.length > 1),
      )
      .subscribe((rows) => {
        this.viewService.init(rows, extendedScope);
      });
  }
}
