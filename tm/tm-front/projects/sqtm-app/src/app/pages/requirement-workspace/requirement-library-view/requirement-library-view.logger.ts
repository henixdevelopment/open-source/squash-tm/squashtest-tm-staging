import { sqtmAppLogger } from '../../../app-logger';

export const requirementLibraryViewLogger = sqtmAppLogger.compose('requirement-library-view');
