export interface BindReqToHighLevelReqDialogConfiguration {
  titleKey: string;
  requirementWithNotLinkableStatus: string[];
  alreadyLinked: string[];
  alreadyLinkedToAnotherHighLevelRequirement: string[];
  highLevelRequirementsInSelection: string[];
  childRequirementsInSelection: string[];
}
