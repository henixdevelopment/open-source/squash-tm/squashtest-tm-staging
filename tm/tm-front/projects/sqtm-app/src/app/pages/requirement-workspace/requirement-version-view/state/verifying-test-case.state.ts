import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { VerifyingTestCase } from 'sqtm-core';

export interface VerifyingTestCaseState extends EntityState<VerifyingTestCase> {}

export const verifyingTestCaseEntityAdapter = createEntityAdapter<VerifyingTestCase>({
  selectId: (vtc) => vtc.id,
});

export const verifyingTestCaseEntitySelectors = verifyingTestCaseEntityAdapter.getSelectors();
