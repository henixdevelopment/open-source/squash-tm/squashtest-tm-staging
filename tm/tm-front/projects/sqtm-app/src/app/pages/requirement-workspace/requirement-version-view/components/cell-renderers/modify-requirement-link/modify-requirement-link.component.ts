import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewContainerRef,
} from '@angular/core';
import {
  AbstractCellRendererComponent,
  DialogService,
  GridColumnId,
  GridService,
  ReferentialDataService,
  RequirementVersionLinkDialogComponent,
  RequirementVersionLinkDialogConfiguration,
  RequirementVersionLinkDialogResult,
} from 'sqtm-core';
import { RequirementVersionViewService } from '../../../services/requirement-version-view.service';
import { take } from 'rxjs/operators';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'sqtm-app-modify-requirement-link',
  template: `
    <div class="full-height full-width icon-container current-workspace-main-color __hover_pointer">
      <i
        class="table-icon-size"
        nz-icon
        [nzType]="'sqtm-core-generic:edit'"
        [nzTheme]="'outline'"
        (click)="showRequirementLinkDialog()"
      ></i>
    </div>
  `,
  styleUrls: ['./modify-requirement-link.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModifyRequirementLinkComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    cdr: ChangeDetectorRef,
    private dialogService: DialogService,
    private requirementVersionViewService: RequirementVersionViewService,
    private referentialDataService: ReferentialDataService,
    private vcr: ViewContainerRef,
  ) {
    super(grid, cdr);
  }

  showRequirementLinkDialog() {
    const relatedVersionId = this.row.data[GridColumnId.id];
    const relatedVersionName = this.row.data[GridColumnId.name];
    combineLatest([
      this.requirementVersionViewService.componentData$,
      this.referentialDataService.requirementVersionLinkTypes$,
    ])
      .pipe(take(1))
      .subscribe(([componentData, requirementVersionLinkTypes]) => {
        const dialogRef = this.dialogService.openDialog<
          RequirementVersionLinkDialogConfiguration,
          RequirementVersionLinkDialogResult
        >({
          id: 'requirement-version-link',
          component: RequirementVersionLinkDialogComponent,
          viewContainerReference: this.vcr,
          data: {
            requirementVersionLinkTypes: requirementVersionLinkTypes,
            requirementVersionName: componentData.requirementVersion.name,
            requirementVersionNodesName: relatedVersionName,
            titleKey: 'sqtm-core.requirement-workspace.dialog.requirement-links.title',
          },
          width: 600,
        });
        dialogRef.dialogClosed$.subscribe((result) => {
          if (result) {
            this.requirementVersionViewService.updateRequirementLinks(
              [relatedVersionId],
              result.linkTypeId,
              result.linkDirection,
              false,
            );
          }
        });
      });
  }
}
