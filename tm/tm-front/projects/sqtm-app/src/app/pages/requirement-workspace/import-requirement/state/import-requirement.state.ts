import { ProjectData, RequirementXlsReport } from 'sqtm-core';

export class ImportRequirementState {
  currentStep: ImportRequirementSteps;
  format: RequirementImportFileFormatKeys;
  importFailed: boolean;
  file: File;
  projects: ProjectData[];
  selectedProject: number;
  loadingData: boolean;
  simulationReport: RequirementXlsReport;
  xlsReport: RequirementXlsReport;
  showXlsImportErrorMessage: boolean;
  errorMessage: string;
}

export function initialImportRequirementState(): Readonly<ImportRequirementState> {
  return {
    currentStep: 'CONFIGURATION',
    format: 'XLS',
    importFailed: false,
    file: null,
    projects: [],
    selectedProject: null,
    loadingData: false,
    simulationReport: null,
    xlsReport: null,
    showXlsImportErrorMessage: false,
    errorMessage: '',
  };
}

export type RequirementImportFileFormatKeys = 'XLS';

export interface FileFormatItem {
  id: string;
  i18nKey: string;
  value: string;
}

export type FileFormat<K extends string> = {
  [id in K]: FileFormatItem;
};

export const RequirementFileFormat: FileFormat<RequirementImportFileFormatKeys> = {
  XLS: { id: 'XLS', i18nKey: 'sqtm-core.generic.label.file.format.xls', value: 'xls' },
};

export type ImportRequirementSteps =
  | 'CONFIGURATION'
  | 'SIMULATION_REPORT'
  | 'CONFIRMATION'
  | 'CONFIRMATION_REPORT';

export const XLS_TYPE =
  'application/vnd.ms-excel,application/vnd.ms-excel.sheet.macroEnabled.12, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
