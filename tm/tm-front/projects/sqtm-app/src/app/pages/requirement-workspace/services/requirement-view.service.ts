import { effect, Injectable, signal, WritableSignal } from '@angular/core';
import { LocalPersistenceService } from 'sqtm-core';
import { distinctUntilChanged, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class RequirementViewService {
  private readonly extendHighLvlReqScopeKey: string = 'extendHighLvlReqScope';

  $extendHighLvlReqScope: WritableSignal<boolean> = signal<boolean>(true);

  constructor(private localPersistenceService: LocalPersistenceService) {
    this.initializeExtendHighLvlReqScope();

    effect(() => {
      this.localPersistenceService
        .set(this.extendHighLvlReqScopeKey, this.$extendHighLvlReqScope())
        .pipe(distinctUntilChanged())
        .subscribe();
    });
  }

  getExtendHighLvlReqScope(): boolean {
    return this.$extendHighLvlReqScope();
  }

  setExtendHighLvlReqScope(value: boolean) {
    this.$extendHighLvlReqScope.set(value);
  }

  private initializeExtendHighLvlReqScope(): void {
    this.localPersistenceService
      .get(this.extendHighLvlReqScopeKey)
      .pipe(take(1))
      .subscribe((storedValue: boolean | null) => {
        if (storedValue != null) {
          this.$extendHighLvlReqScope.set(storedValue);
        }
      });
  }
}
