import { TestBed } from '@angular/core/testing';

import {
  AttachmentService,
  CustomFieldValueService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  Permissions,
  ReferentialDataService,
  RequirementVersionService,
  RestService,
} from 'sqtm-core';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { RequirementVersionViewService } from './requirement-version-view.service';
import { AppTestingUtilsModule } from '../../../../utils/testing-utils/app-testing-utils.module';
import { RVW_VERIFYING_TEST_CASE_TABLE } from '../requirement-version-view.constant';

describe('RequirementViewService', () => {
  const tableMock = jasmine.createSpyObj('tableMock', ['load']);

  const restServiceMock = {} as RestService;

  const referentialDataService = {} as ReferentialDataService;

  referentialDataService.connectToProjectData = jasmine.createSpy().and.returnValue(
    of({
      permissions: { REQUIREMENT: [Permissions.WRITE] },
    }),
  );

  referentialDataService.globalConfiguration$ = of(null);

  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, TranslateModule.forRoot()],
      providers: [
        { provide: RestService, useValue: restServiceMock },
        { provide: RVW_VERIFYING_TEST_CASE_TABLE, useValue: tableMock },
        {
          provide: RequirementVersionViewService,
          useClass: RequirementVersionViewService,
          deps: [
            RestService,
            ReferentialDataService,
            AttachmentService,
            TranslateService,
            RequirementVersionService,
            CustomFieldValueService,
            EntityViewAttachmentHelperService,
            EntityViewCustomFieldHelperService,
            RVW_VERIFYING_TEST_CASE_TABLE,
          ],
        },
        {
          provide: ReferentialDataService,
          useValue: referentialDataService,
        },
      ],
    }),
  );

  it('should be created', () => {
    const service: RequirementVersionViewService = TestBed.get(RequirementVersionViewService);
    expect(service).toBeTruthy();
  });
});
