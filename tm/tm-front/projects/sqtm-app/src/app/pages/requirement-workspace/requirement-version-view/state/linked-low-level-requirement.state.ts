import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { LinkedLowLevelRequirement } from 'sqtm-core';

export interface LinkedLowLevelRequirementState extends EntityState<LinkedLowLevelRequirement> {}

export const linkedLowLevelRequirementEntityAdapter =
  createEntityAdapter<LinkedLowLevelRequirement>({
    selectId: (req) => req.requirementId,
  });
