import { TestBed } from '@angular/core/testing';
import { ImportRequirementService } from './import-requirement.service';
import { ProjectData, ReferentialDataService, RestService } from 'sqtm-core';
import { of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

describe('ImportRequirementService', () => {
  let service: ImportRequirementService;
  const restServiceMock = {} as RestService;
  const referentialDataService = {} as ReferentialDataService;
  referentialDataService.projectsManaged$ = of([{ id: 1 } as ProjectData]);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: RestService, useValue: restServiceMock },
        { provide: ReferentialDataService, useValue: referentialDataService },
        { provide: TranslateService, useValue: jasmine.createSpyObj(['instant']) },
      ],
    });
    service = TestBed.inject(ImportRequirementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
