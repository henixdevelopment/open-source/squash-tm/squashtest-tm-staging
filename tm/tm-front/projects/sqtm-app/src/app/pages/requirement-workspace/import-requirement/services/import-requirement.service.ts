import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import {
  ImportRequirementState,
  ImportRequirementSteps,
  initialImportRequirementState,
} from '../state/import-requirement.state';
import { concatMap, map, take, tap, withLatestFrom } from 'rxjs/operators';
import {
  createStore,
  doesHttpErrorContainsSquashActionError,
  FRONT_END_ERROR_IS_HANDLED_PARAM,
  ProjectData,
  ReferentialDataService,
  RequirementXlsReport,
  RestService,
  Store,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root',
})
export class ImportRequirementService {
  store: Store<ImportRequirementState>;

  state$: Observable<ImportRequirementState>;

  constructor(
    private restService: RestService,
    private referentialData: ReferentialDataService,
    private translateService: TranslateService,
  ) {
    const initialState = initialImportRequirementState();
    this.store = createStore(initialState);
    this.state$ = this.store.state$;
    this.initializeComponentData();
  }

  private initializeComponentData() {
    this.state$
      .pipe(
        take(1),
        withLatestFrom(this.referentialData.projectsManaged$),
        map(([state, projects]: [ImportRequirementState, ProjectData[]]) => {
          const componentData: ImportRequirementState = {
            ...state,
            projects,
            selectedProject: projects[0].id,
          };
          return componentData;
        }),
        tap((state) => this.store.commit(state)),
      )
      .subscribe();
  }

  restoreState() {
    this.state$
      .pipe(
        take(1),
        map((_state) => initialImportRequirementState()),
      )
      .subscribe((state) => this.store.commit(state));
  }

  saveFile(file: File) {
    this.state$
      .pipe(
        take(1),
        map((state: ImportRequirementState) => {
          return { ...state, file };
        }),
      )
      .subscribe((state) => this.store.commit(state));
  }

  simulateImport(file: File) {
    this.state$
      .pipe(
        take(1),
        map((state: ImportRequirementState) => this.changeCurrentStep(state, 'SIMULATION_REPORT')),
        map((state: ImportRequirementState) => this.beginAsync(state)),
        concatMap((state: ImportRequirementState) => this.doXlsImport(state, file, true)),
        map((state: ImportRequirementState) => this.endAsync(state)),
      )
      .subscribe({
        next: (state) => this.store.commit(state),
        error: (error) => this.handleXlsImportError(error),
      });
  }

  handleXlsImportError(httpError: any) {
    if (doesHttpErrorContainsSquashActionError(httpError)) {
      this.handleSquashActionError(httpError);
    } else {
      this.handleOtherHttpErrors(httpError);
    }
  }

  private handleSquashActionError(httpError: any) {
    const squashError = httpError.error.squashTMError;
    const errorMessage = this.translateService.instant(
      squashError.actionValidationError.i18nKey ?? 'sqtm-core.generic.label.exception.message',
      squashError.actionValidationError.i18nParams,
    );

    this.state$.pipe(take(1)).subscribe((state: ImportRequirementState) => {
      this.store.commit({
        ...state,
        showXlsImportErrorMessage: true,
        errorMessage: errorMessage,
        loadingData: false,
      });
    });
    throwError(() => httpError);
  }

  private handleOtherHttpErrors(httpError: any) {
    let errorMessage = this.translateService.instant('sqtm-core.generic.label.exception.message');

    // The HTTP 413 Content Too Large client error response status code indicates that
    // the request entity was larger than limits defined by server. This error typically happens
    // if you attempt to upload an overly large file
    if (httpError.status === 413) {
      errorMessage = this.translateService.instant(
        'sqtm-core.exception.import-size-limit-exceeded',
        [httpError.error.maxUploadError.maxSize],
      );
    }

    this.state$.pipe(take(1)).subscribe((state: ImportRequirementState) => {
      this.store.commit({
        ...state,
        showXlsImportErrorMessage: true,
        errorMessage: errorMessage,
        loadingData: false,
      });
    });
    throwError(() => httpError);
  }

  goToConfirmationImportPage() {
    this.state$
      .pipe(
        take(1),
        map((state: ImportRequirementState) => this.changeCurrentStep(state, 'CONFIRMATION')),
      )
      .subscribe((state) => this.store.commit(state));
  }

  private changeCurrentStep(state: ImportRequirementState, step: ImportRequirementSteps) {
    const newState = { ...state, currentStep: step };
    this.store.commit(newState);
    return newState;
  }

  private beginAsync(state: ImportRequirementState) {
    const newState = { ...state, loadingData: true };
    this.store.commit(newState);
    return state;
  }

  private endAsync(state: ImportRequirementState) {
    const newState = { ...state, loadingData: false };
    this.store.commit(newState);
    return state;
  }

  private doXlsImport(state: ImportRequirementState, file: File, simulation?: boolean) {
    const formData = new FormData();
    formData.append('archive', file, file.name);
    if (simulation) {
      formData.append('dry-run', 'true');
    }

    const params = { [FRONT_END_ERROR_IS_HANDLED_PARAM]: 'true' };

    return this.restService
      .post<RequirementXlsReport>([`requirement/importer/xls`], formData, {
        params,
      })
      .pipe(
        map((res: RequirementXlsReport) => {
          const importFailed = res.importFormatFailure != null;
          if (simulation) {
            return { ...state, simulationReport: res, importFailed };
          }
          return { ...state, xlsReport: res, importFailed };
        }),
      );
  }

  getDownLoadReportUrl(reportUrl: string) {
    return `${this.restService.backendRootUrl}${reportUrl}`;
  }

  importRequirementXls(): Observable<any> {
    return this.state$.pipe(
      take(1),
      map((state: ImportRequirementState) => this.changeCurrentStep(state, 'CONFIRMATION_REPORT')),
      map((state: ImportRequirementState) => this.beginAsync(state)),
      concatMap((state: ImportRequirementState) => this.doXlsImport(state, state.file)),
      map((state: ImportRequirementState) => this.endAsync(state)),
      tap((state) => this.store.commit(state)),
    );
  }
}
