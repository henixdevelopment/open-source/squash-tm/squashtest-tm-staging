import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  ElementRef,
  Input,
  OnInit,
  Renderer2,
  ViewContainerRef,
} from '@angular/core';
import { RequirementVersionViewService } from '../../services/requirement-version-view.service';
import {
  ActionErrorDisplayService,
  DialogService,
  DragAndDropService,
  EntityViewComponentData,
  GridPersistenceService,
  ReferentialDataService,
  RequirementPermissions,
} from 'sqtm-core';
import { RequirementVersionState } from '../../state/requirement-version-view.state';
import { TranslateService } from '@ngx-translate/core';
import { AbstractRequirementVersionView } from '../abstract-requirement-version-view';
import { Router } from '@angular/router';
import { ReferentialDataState } from 'sqtm-core/lib/core/referential/state/referential-data.state';

@Component({
  selector: 'sqtm-app-requirement-version-view',
  templateUrl: './requirement-version-view.component.html',
  styleUrls: ['./requirement-version-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RequirementVersionViewComponent
  extends AbstractRequirementVersionView
  implements OnInit
{
  @ContentChild('entityProjection')
  entityProjection: ElementRef;

  @Input()
  showNewVersionCreationMenuItem = true;

  constructor(
    requirementViewService: RequirementVersionViewService,
    translateService: TranslateService,
    dndService: DragAndDropService,
    renderer: Renderer2,
    referentialDataService: ReferentialDataService,
    dialogService: DialogService,
    vcr: ViewContainerRef,
    router: Router,
    gridPersistenceService: GridPersistenceService,
    actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    super(
      requirementViewService,
      translateService,
      dndService,
      renderer,
      referentialDataService,
      dialogService,
      vcr,
      router,
      gridPersistenceService,
      actionErrorDisplayService,
    );
  }

  ngOnInit() {
    super.ngOnInit();
  }

  getViewId(): string {
    return 'requirement-version-view';
  }

  showCreateNewVersionMenuItem(): boolean {
    return this.showNewVersionCreationMenuItem;
  }

  showActionMenu(
    componentData: RequirementVersionViewComponentData,
    referentialData: ReferentialDataState,
  ): boolean {
    return (
      this.showCreateNewVersionMenuItem() ||
      this.isConvertToHighLevelVisible(componentData, referentialData) ||
      componentData.permissions.canExport ||
      this.isAiTestCaseGenerationVisible(componentData)
    );
  }
}

export interface RequirementVersionViewComponentData
  extends EntityViewComponentData<
    RequirementVersionState,
    'requirementVersion',
    RequirementPermissions
  > {}
