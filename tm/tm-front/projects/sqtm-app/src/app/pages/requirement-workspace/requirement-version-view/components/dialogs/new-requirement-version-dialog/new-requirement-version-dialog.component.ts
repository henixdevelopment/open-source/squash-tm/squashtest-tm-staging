import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import {
  DialogReference,
  MilestoneModeData,
  NewReqVersionParams,
  ReferentialDataService,
} from 'sqtm-core';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'sqtm-app-new-requirement-version-dialog',
  templateUrl: './new-requirement-version-dialog.component.html',
  styleUrls: ['./new-requirement-version-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewRequirementVersionDialogComponent implements OnInit {
  formGroup: FormGroup;

  milestoneModeData$: Observable<MilestoneModeData>;

  constructor(
    private dialogRef: DialogReference,
    private referentialDataService: ReferentialDataService,
    private fb: FormBuilder,
  ) {
    this.milestoneModeData$ = this.referentialDataService.milestoneModeData$;
  }

  ngOnInit(): void {
    this.initFormGroup();
  }

  initFormGroup() {
    this.formGroup = this.fb.group({
      inheritReqLinks: this.fb.control(false),
      inheritTestCases: this.fb.control(false),
    });
  }

  confirmNewVersion() {
    this.dialogRef.result = {
      inheritReqLinks: this.formGroup.controls['inheritReqLinks'].value,
      inheritTestCases: this.formGroup.controls['inheritTestCases'].value,
    } as NewReqVersionParams;
    this.dialogRef.close();
  }
}
