import { Injectable } from '@angular/core';
import {
  AttachmentService,
  CustomDashboardService,
  CustomFieldValueService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  FavoriteDashboardValue,
  PartyPreferencesService,
  ProjectData,
  ReferentialDataService,
  RequirementLibraryModel,
  RequirementPermissions,
  RequirementStatisticsService,
  RestService,
} from 'sqtm-core';
import { RequirementLibraryState } from '../state/requirement-library.state';
import { TranslateService } from '@ngx-translate/core';
import {
  provideInitialRequirementLibraryView,
  RequirementLibraryViewState,
} from '../state/requirement-library-view.state';
import { concatMap, map, take, tap, withLatestFrom } from 'rxjs/operators';
import { of } from 'rxjs';
import { RequirementViewService } from '../../services/requirement-view.service';

@Injectable()
export class RequirementLibraryViewService extends EntityViewService<
  RequirementLibraryState,
  'requirementLibrary',
  RequirementPermissions
> {
  constructor(
    protected restService: RestService,
    protected referentialDataService: ReferentialDataService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected customFieldValueService: CustomFieldValueService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
    protected customFieldHelper: EntityViewCustomFieldHelperService,
    private requirementStatisticsService: RequirementStatisticsService,
    private customDashboardService: CustomDashboardService,
    private partyPreferencesService: PartyPreferencesService,
    private requirementViewService: RequirementViewService,
  ) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper,
    );
  }

  addSimplePermissions(projectData: ProjectData): RequirementPermissions {
    return new RequirementPermissions(projectData);
  }

  getInitialState(): RequirementLibraryViewState {
    return provideInitialRequirementLibraryView();
  }

  load(id: number) {
    const extendHighLvlReqScope: boolean = this.requirementViewService.$extendHighLvlReqScope();
    this.restService
      .getWithoutErrorHandling<RequirementLibraryModel>(
        ['requirement-library-view', id.toString()],
        { extendHighLvlReqScope: extendHighLvlReqScope },
      )
      .subscribe({
        next: (requirementLibraryModel: RequirementLibraryModel) => {
          const requirementLibrary =
            this.initializeRequirementLibraryState(requirementLibraryModel);
          this.initializeEntityState(requirementLibrary);
        },
        error: (err) => this.notifyEntityNotFound(err),
      });
  }

  refreshStatistics(extendHighLvlReqScope: boolean) {
    this.state$
      .pipe(
        take(1),
        concatMap((initialState) =>
          this.requirementStatisticsService
            .fetchStatistics(
              [`RequirementLibrary-${initialState.requirementLibrary.id}`],
              extendHighLvlReqScope,
            )
            .pipe(
              withLatestFrom(this.state$),
              map(([statistics, state]) => ({
                ...state,
                requirementLibrary: {
                  ...state.requirementLibrary,
                  statistics: { ...statistics },
                  shouldShowFavoriteDashboard: false,
                  dashboard: null,
                  generatedDashboardOn: new Date(),
                },
              })),
            ),
        ),
      )
      .subscribe((state) => this.commit(state));
  }

  private initializeRequirementLibraryState(
    requirementLibraryModel: RequirementLibraryModel,
  ): RequirementLibraryState {
    const attachmentEntityState = this.initializeAttachmentState(
      requirementLibraryModel.attachmentList.attachments,
    );
    const customFieldValueState = this.initializeCustomFieldValueState(
      requirementLibraryModel.customFieldValues,
    );
    return {
      ...requirementLibraryModel,
      attachmentList: {
        id: requirementLibraryModel.attachmentList.id,
        attachments: attachmentEntityState,
      },
      customFieldValues: customFieldValueState,
      generatedDashboardOn: new Date(),
    };
  }

  changeDashboardToDisplay(
    preferenceValue: FavoriteDashboardValue,
    extendHighLvlReqScope: boolean,
  ) {
    this.partyPreferencesService
      .changeRequirementWorkspaceFavoriteDashboard(preferenceValue)
      .pipe(
        tap(() => {
          if (preferenceValue === 'default') {
            this.refreshStatistics(extendHighLvlReqScope);
          } else {
            this.refreshDashboard(extendHighLvlReqScope);
          }
        }),
      )
      .subscribe();
  }

  refreshDashboard(extendHighLvlReqScope: boolean) {
    this.state$
      .pipe(
        take(1),
        concatMap((initialState: RequirementLibraryViewState) =>
          this.refreshFavoriteDashboardIfAllowed(initialState, extendHighLvlReqScope),
        ),
      )
      .subscribe((state) => this.commit(state));
  }

  private refreshFavoriteDashboardIfAllowed(
    initialState: RequirementLibraryViewState,
    extendHighLvlReqScope: boolean,
  ) {
    if (initialState.requirementLibrary.canShowFavoriteDashboard) {
      return this.customDashboardService
        .getDashboardWithDynamicScope(initialState.requirementLibrary.favoriteDashboardId, {
          milestoneDashboard: false,
          workspaceName: 'REQUIREMENT',
          requirementLibraryIds: [initialState.requirementLibrary.id],
          extendedHighLvlReqScope: extendHighLvlReqScope,
        })
        .pipe(
          withLatestFrom(this.state$),
          map(([dashboard, state]) => ({
            ...state,
            requirementLibrary: {
              ...state.requirementLibrary,
              statistics: null,
              dashboard: { ...dashboard },
              generatedDashboardOn: new Date(),
              shouldShowFavoriteDashboard: true,
            },
          })),
        );
    } else {
      return of({
        ...initialState,
        requirementLibrary: {
          ...initialState.requirementLibrary,
          statistics: null,
          dashboard: null,
          shouldShowFavoriteDashboard: true,
        },
      });
    }
  }
}
