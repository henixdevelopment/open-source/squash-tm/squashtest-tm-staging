import { EntityViewState, provideInitialViewState } from 'sqtm-core';
import { SprintReqVersionState } from './sprint-req-version.state';

export interface SprintReqVersionViewState
  extends EntityViewState<SprintReqVersionState, 'sprintReqVersion'> {
  sprintReqVersion: SprintReqVersionState;
}

export function provideInitialSprintReqVersionView(): Readonly<SprintReqVersionViewState> {
  return provideInitialViewState<SprintReqVersionState, 'sprintReqVersion'>('sprintReqVersion');
}
