import { TestBed } from '@angular/core/testing';

import { RequirementViewService } from './requirement-view.service';
import { LocalPersistenceService } from 'sqtm-core';
import { of } from 'rxjs';

describe('RequirementViewService', () => {
  let service: RequirementViewService;
  const localPersistenceServiceSpy = jasmine.createSpyObj('localPersistence', ['get', 'set']);

  beforeEach(() => {
    localPersistenceServiceSpy.set.and.returnValue(of(null));
    localPersistenceServiceSpy.get.and.returnValue(of(null));

    TestBed.configureTestingModule({
      imports: [],
      providers: [
        {
          provide: LocalPersistenceService,
          useValue: localPersistenceServiceSpy,
        },
      ],
    });
    service = TestBed.inject(RequirementViewService);
  });

  afterEach(() => {
    localPersistenceServiceSpy.get.calls.reset();
    localPersistenceServiceSpy.set.calls.reset();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should initialize $extendHighLvlReqScope with true', () => {
    localPersistenceServiceSpy.get.and.returnValue(of(true));

    service['initializeExtendHighLvlReqScope']();

    expect(localPersistenceServiceSpy.get).toHaveBeenCalledWith('extendHighLvlReqScope');
    expect(service.getExtendHighLvlReqScope()).toBeTrue();
  });

  it('should initialize $extendHighLvlReqScope with false', () => {
    localPersistenceServiceSpy.get.and.returnValue(of(false));

    service['initializeExtendHighLvlReqScope']();

    expect(service.getExtendHighLvlReqScope()).toBeFalse();
  });

  it('should not update $extendHighLvlReqScope if the stored value is null', () => {
    localPersistenceServiceSpy.get.and.returnValue(of(null));

    service['initializeExtendHighLvlReqScope']();

    expect(service.getExtendHighLvlReqScope()).toBeTrue();
  });

  it('should set and get the current value of $extendHighLvlReqScope', () => {
    service.setExtendHighLvlReqScope(false);
    expect(service.getExtendHighLvlReqScope()).toBeFalse();

    service.setExtendHighLvlReqScope(true);
    expect(service.getExtendHighLvlReqScope()).toBeTrue();
  });

  it('should call localPersistenceService.set when $extendHighLvlReqScope changes', (done) => {
    localPersistenceServiceSpy.set.calls.reset();

    service.setExtendHighLvlReqScope(false);

    setTimeout(() => {
      expect(localPersistenceServiceSpy.set).toHaveBeenCalledWith('extendHighLvlReqScope', false);
      done();
    });
  });
});
