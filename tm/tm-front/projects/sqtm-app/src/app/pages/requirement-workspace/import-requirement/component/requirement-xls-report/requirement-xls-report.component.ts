import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { RequirementImportFormatFailure, RequirementImportLog } from 'sqtm-core';
import { ImportRequirementService } from '../../services/import-requirement.service';

@Component({
  selector: 'sqtm-app-requirement-xls-report',
  templateUrl: './requirement-xls-report.component.html',
  styleUrls: ['./requirement-xls-report.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RequirementXlsReportComponent {
  @Input()
  templateOk: RequirementImportLog;

  @Input()
  templateKo: RequirementImportFormatFailure;

  constructor(private importRequirementService: ImportRequirementService) {}

  getFileName(templateOk: RequirementImportLog) {
    return this.importRequirementService.getDownLoadReportUrl(templateOk.reportUrl);
  }
}
