import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RequirementWorkspaceComponent } from './requirement-workspace/containers/requirement-workspace/requirement-workspace.component';
import {
  AnchorModule,
  AttachmentModule,
  CellRendererCommonModule,
  CustomFieldModule,
  DialogModule,
  GridExportModule,
  GridModule,
  IssuesModule,
  NavBarModule,
  PlatformNavigationModule,
  SqtmDragAndDropModule,
  SvgModule,
  UiManagerModule,
  WorkspaceCommonModule,
  WorkspaceLayoutModule,
} from 'sqtm-core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { RequirementWorkspaceTreeComponent } from './requirement-workspace/containers/requirement-workspace-tree/requirement-workspace-tree.component';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RequirementViewComponent } from './requirement-view/containers/requirement-view/requirement-view.component';
import { RequirementVersionViewContentComponent } from './requirement-version-view/components/requirement-version-view-content/requirement-version-view-content.component';
import { RequirementLibraryViewComponent } from './requirement-library-view/container/requirement-library-view/requirement-library-view.component';
import { RequirementLibraryViewContentComponent } from './requirement-library-view/container/requirement-library-view-content/requirement-library-view-content.component';
import { RequirementFolderViewComponent } from './requirement-folder-view/container/requirement-folder-view/requirement-folder-view.component';
import { RequirementFolderViewContentComponent } from './requirement-folder-view/container/requirement-folder-view-content/requirement-folder-view-content.component';
import { RequirementVersionViewModificationHistoryComponent } from './requirement-version-view/components/requirement-version-view-modification-history/requirement-version-view-modification-history.component';
import { CreateRequirementDialogComponent } from './requirement-workspace/components/create-requirement-dialog/create-requirement-dialog.component';
import { CreateRequirementFormComponent } from './requirement-workspace/components/create-requirement-form/create-requirement-form.component';
import { RequirementVersionIssuesComponent } from './requirement-version-view/components/requirement-version-issues/requirement-version-issues.component';
import { RequirementWorkspaceMultiSelectViewComponent } from './requirement-workspace-multi-select-view/containers/requirement-workspace-multi-select-view/requirement-workspace-multi-select-view.component';
import { RequirementMultiViewContentComponent } from './requirement-workspace-multi-select-view/containers/requirement-multi-view-content/requirement-multi-view-content.component';
import { RequirementMilestoneViewComponent } from './requirement-milestone-dashboard/containers/requirement-milestone-view/requirement-milestone-view.component';
import { RequirementMilestoneViewContentComponent } from './requirement-milestone-dashboard/containers/requirement-milestone-view-content/requirement-milestone-view-content.component';
import { RequirementStatisticsModule } from '../../components/statistics/requirement-statistics/requirement-statistics.module';
import { RequirementVersionViewComponent } from './requirement-version-view/containers/requirement-version-view/requirement-version-view.component';
import { RequirementVersionViewInformationComponent } from './requirement-version-view/components/requirement-version-view-information/requirement-version-view-information.component';
import { VerifyingTestCaseTableComponent } from './requirement-version-view/components/verifying-test-case-table/verifying-test-case-table.component';
import { DeleteVerifyingTestCaseComponent } from './requirement-version-view/components/cell-renderers/delete-verifying-test-case/delete-verifying-test-case.component';
import { RequirementVersionLinkTableComponent } from './requirement-version-view/components/requirement-version-link-table/requirement-version-link-table.component';
import { DeleteRequirementLinkComponent } from './requirement-version-view/components/cell-renderers/delete-requirement-link/delete-requirement-link.component';
import { ModifyRequirementLinkComponent } from './requirement-version-view/components/cell-renderers/modify-requirement-link/modify-requirement-link.component';
import { RequirementVersionRateComponent } from './requirement-version-view/components/requirement-vesion-rate/requirement-version-rate.component';
import { RequirementVersionViewRatePanelComponent } from './requirement-version-view/components/requirement-version-view-rate-panel/requirement-version-view-rate-panel.component';
import { ModificationHistoryEventComponent } from './requirement-version-view/components/cell-renderers/modification-history-event/modification-history-event.component';
import { ModificationHistoryChangedValueComponent } from './requirement-version-view/components/cell-renderers/modification-history-changed-value/modification-history-changed-value.component';
import { DescriptionChangeDialogComponent } from './requirement-version-view/components/cell-renderers/description-change-dialog/description-change-dialog.component';
import { NewRequirementVersionDialogComponent } from './requirement-version-view/components/dialogs/new-requirement-version-dialog/new-requirement-version-dialog.component';
import { RequirementVersionDetailViewComponent } from './requirement-version-view/containers/requirement-version-detail-view/requirement-version-detail-view.component';
import { RequirementMultiVersionViewComponent } from './requirement-multi-version-view/container/requirement-multi-version-view/requirement-multi-version-view.component';
import { RequirementMultiVersionGridComponent } from './requirement-multi-version-view/components/requirement-multi-version-grid/requirement-multi-version-grid.component';
import { ImportRequirementModule } from './import-requirement/import-requirement.module';
import { RequirementExportDialogModule } from '../../components/export-dialog/requirement-export-dialog/requirement-export-dialog.module';
import { RequirementVersionLevelTwoComponent } from './requirement-version-level-two/container/requirement-version-level-two.component';
import { CustomDashboardModule } from '../../components/custom-dashboard/custom-dashboard.module';
import { SynchronizeRequirementsDialogComponent } from './requirement-workspace/components/synchronize-requirements-dialog/synchronize-requirements-dialog.component';
import { HighLevelRequirementSelectorComponent } from './requirement-version-view/components/fields/high-level-requirement-selector/high-level-requirement-selector.component';
import { HighLevelRequirementSelectorDialogComponent } from './requirement-version-view/components/dialogs/high-level-requirement-selector-dialog/high-level-requirement-selector-dialog.component';
import { HighLevelRequirementViewComponent } from './high-level-req/hl-req-view/containers/hl-req-view/high-level-requirement-view.component';
import { HighLevelRequirementVersionViewComponent } from './high-level-req/hl-req-version-view/containers/hl-req-version-view/high-level-requirement-version-view.component';
import { LinkedLowLvlRequirementTableComponent } from './high-level-req/hl-req-version-view/components/linked-low-lvl-requirement-table/linked-low-lvl-requirement-table.component';
import { BindReqToHighLevelReqDialogComponent } from './requirement-version-view/components/dialogs/bind-req-to-high-level-req-dialog/bind-req-to-high-level-req-dialog.component';
import { DeleteLinkedLowLvlRequirementComponent } from './high-level-req/hl-req-version-view/components/delete-linked-low-lvl-requirement/delete-linked-low-lvl-requirement.component';
import { MilestoneDialogComponent } from '../../components/milestone/milestone-dialog/milestone-dialog.component';
import { MilestonesTagComponent } from '../../components/milestone/milestones-tag/milestones-tag.component';
import { TestCaseAiGenerationDialogComponent } from './requirement-version-view/components/dialogs/test-case-ai-generation-dialog/test-case-ai-generation-dialog.component';
import { GeneratedTestCaseFoldableRowComponent } from '../../components/artificial-intelligence/generated-test-case-foldable-row/generated-test-case-foldable-row.component';

export const routes: Routes = [
  {
    path: '',
    component: RequirementWorkspaceComponent,
    children: [
      {
        path: 'requirement/:requirementId',
        component: RequirementViewComponent,
        children: [
          { path: 'content', component: RequirementVersionViewContentComponent },
          { path: 'history', component: RequirementVersionViewModificationHistoryComponent },
          { path: 'issues', component: RequirementVersionIssuesComponent },
        ],
      },
      {
        path: 'requirement-library/:requirementLibraryId',
        component: RequirementLibraryViewComponent,
        children: [{ path: 'content', component: RequirementLibraryViewContentComponent }],
      },
      {
        path: 'requirement-folder/:requirementFolderId',
        component: RequirementFolderViewComponent,
        children: [{ path: 'content', component: RequirementFolderViewContentComponent }],
      },
      {
        path: 'requirement-workspace-multi',
        component: RequirementWorkspaceMultiSelectViewComponent,
        children: [
          { path: '', pathMatch: 'full', redirectTo: 'content' },
          { path: 'content', component: RequirementMultiViewContentComponent },
        ],
      },
      {
        path: 'milestone-dashboard',
        component: RequirementMilestoneViewComponent,
        children: [{ path: 'content', component: RequirementMilestoneViewContentComponent }],
      },
      {
        path: 'high-level-requirement/:requirementId',
        component: HighLevelRequirementViewComponent,
        children: [
          { path: 'content', component: RequirementVersionViewContentComponent },
          { path: 'history', component: RequirementVersionViewModificationHistoryComponent },
          { path: 'issues', component: RequirementVersionIssuesComponent },
        ],
      },
    ],
  },
  {
    path: 'requirement/:requirementId/version',
    component: RequirementMultiVersionViewComponent,
    children: [
      {
        path: ':versionId',
        component: RequirementVersionDetailViewComponent,
        children: [
          { path: 'content', component: RequirementVersionViewContentComponent },
          { path: 'history', component: RequirementVersionViewModificationHistoryComponent },
          { path: 'issues', component: RequirementVersionIssuesComponent },
        ],
      },
    ],
  },
  {
    path: 'requirement-version/detail/:versionId',
    component: RequirementVersionLevelTwoComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'content' },
      { path: 'content', component: RequirementVersionViewContentComponent },
      { path: 'history', component: RequirementVersionViewModificationHistoryComponent },
      { path: 'issues', component: RequirementVersionIssuesComponent },
    ],
  },
];

@NgModule({
  declarations: [
    RequirementWorkspaceComponent,
    RequirementWorkspaceTreeComponent,
    CreateRequirementDialogComponent,
    CreateRequirementFormComponent,
    RequirementFolderViewComponent,
    RequirementFolderViewContentComponent,
    RequirementLibraryViewComponent,
    RequirementLibraryViewContentComponent,
    RequirementMilestoneViewComponent,
    RequirementMilestoneViewContentComponent,
    RequirementViewComponent,
    RequirementVersionViewComponent,
    RequirementVersionViewContentComponent,
    RequirementVersionViewInformationComponent,
    VerifyingTestCaseTableComponent,
    DeleteVerifyingTestCaseComponent,
    RequirementVersionLinkTableComponent,
    DeleteRequirementLinkComponent,
    ModifyRequirementLinkComponent,
    RequirementVersionViewModificationHistoryComponent,
    RequirementVersionRateComponent,
    RequirementVersionViewRatePanelComponent,
    RequirementVersionIssuesComponent,
    ModificationHistoryEventComponent,
    ModificationHistoryChangedValueComponent,
    DescriptionChangeDialogComponent,
    NewRequirementVersionDialogComponent,
    RequirementVersionDetailViewComponent,
    RequirementWorkspaceMultiSelectViewComponent,
    RequirementMultiVersionViewComponent,
    RequirementMultiViewContentComponent,
    RequirementMultiVersionGridComponent,
    RequirementVersionLevelTwoComponent,
    SynchronizeRequirementsDialogComponent,
    HighLevelRequirementSelectorComponent,
    HighLevelRequirementSelectorDialogComponent,
    HighLevelRequirementViewComponent,
    HighLevelRequirementVersionViewComponent,
    LinkedLowLvlRequirementTableComponent,
    BindReqToHighLevelReqDialogComponent,
    DeleteLinkedLowLvlRequirementComponent,
    TestCaseAiGenerationDialogComponent,
  ],
  imports: [
    AnchorModule,
    AttachmentModule,
    CommonModule,
    CellRendererCommonModule,
    CustomFieldModule,
    DialogModule,
    FormsModule,
    GridModule,
    IssuesModule,
    MilestoneDialogComponent,
    MilestonesTagComponent,
    NavBarModule,
    NzButtonModule,
    NzCollapseModule,
    NzDropDownModule,
    NzIconModule,
    NzInputModule,
    NzMenuModule,
    NzPopoverModule,
    NzRadioModule,
    NzSelectModule,
    NzToolTipModule,
    ReactiveFormsModule,
    RequirementStatisticsModule,
    RouterModule.forChild(routes),
    SqtmDragAndDropModule,
    SvgModule,
    TranslateModule.forChild(),
    UiManagerModule,
    WorkspaceCommonModule,
    WorkspaceLayoutModule,
    ImportRequirementModule,
    RequirementExportDialogModule,
    CustomDashboardModule,
    NzCheckboxModule,
    PlatformNavigationModule,
    GridExportModule,
    WorkspaceCommonModule,
    GeneratedTestCaseFoldableRowComponent,
  ],
})
export class RequirementWorkspaceModule {}
