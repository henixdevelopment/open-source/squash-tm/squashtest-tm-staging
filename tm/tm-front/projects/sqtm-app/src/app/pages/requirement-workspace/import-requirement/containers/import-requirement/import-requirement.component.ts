import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { DialogReference, ReferentialDataService, RestService } from 'sqtm-core';
import { ImportRequirementService } from '../../services/import-requirement.service';
import { Observable } from 'rxjs';
import { ImportRequirementState, XLS_TYPE } from '../../state/import-requirement.state';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-import-requirement',
  templateUrl: './import-requirement.component.html',
  styleUrls: ['./import-requirement.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: ImportRequirementService,
      useClass: ImportRequirementService,
      deps: [RestService, ReferentialDataService, TranslateService],
    },
  ],
})
export class ImportRequirementComponent implements OnInit {
  componentData$: Observable<ImportRequirementState>;
  wrongFileFormat: boolean;

  constructor(
    private dialogReference: DialogReference,
    private importRequirementService: ImportRequirementService,
    private translator: TranslateService,
  ) {}

  ngOnInit(): void {
    this.initializeComponentData();
  }

  initializeComponentData() {
    this.componentData$ = this.importRequirementService.state$;
  }

  cancelImport(componentData: ImportRequirementState) {
    if (componentData.importFailed) {
      this.importRequirementService.restoreState();
    } else {
      this.dialogReference.close();
    }
  }

  simulateImport(componentData: ImportRequirementState) {
    if (this.checkFileType(componentData)) {
      this.importRequirementService.simulateImport(componentData.file);
    } else {
      this.wrongFileFormat = true;
    }
  }

  private checkFileType(componentData: ImportRequirementState) {
    const file = componentData.file;

    if (file != null) {
      const fileType = file.type;
      if (componentData.format === 'XLS') {
        return XLS_TYPE.includes(fileType);
      }
    } else {
      return false;
    }
  }

  confirmImport(componentData: ImportRequirementState) {
    if (this.checkFileType(componentData)) {
      this.importRequirementService.goToConfirmationImportPage();
    } else {
      this.wrongFileFormat = true;
    }
  }

  getDialogTitle(componentData: ImportRequirementState) {
    switch (componentData.currentStep) {
      case 'CONFIGURATION':
        return this.translator.instant(
          'sqtm-core.requirement-workspace.dialog.title.config-import',
        );
      case 'SIMULATION_REPORT':
        return this.translator.instant(
          'sqtm-core.requirement-workspace.dialog.title.simulate-import',
        );
      case 'CONFIRMATION':
        return this.translator.instant(
          'sqtm-core.requirement-workspace.dialog.title.confirm-import',
        );
      case 'CONFIRMATION_REPORT':
        return this.translator.instant(
          'sqtm-core.requirement-workspace.dialog.title.report-import',
        );
      default:
        return '';
    }
  }

  importRequirement(componentData: ImportRequirementState) {
    if (this.checkFileType(componentData)) {
      this.doImport();
    } else {
      this.wrongFileFormat = true;
    }
  }

  private doImport() {
    this.importRequirementService.importRequirementXls().subscribe({
      next: () => {
        this.dialogReference.result = true;
      },
      error: (error) => this.importRequirementService.handleXlsImportError(error),
    });
  }
}
