import { CustomDashboardModel, RequirementStatistics, SqtmEntityState } from 'sqtm-core';

export interface RequirementLibraryState extends SqtmEntityState {
  name: string;
  description: string;
  statistics: RequirementStatistics;
  dashboard: CustomDashboardModel;
  generatedDashboardOn: Date;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
}
