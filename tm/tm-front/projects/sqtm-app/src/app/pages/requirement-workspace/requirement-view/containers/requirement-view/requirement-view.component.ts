import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { requirementViewLogger } from '../../requirement-view.logger';
import {
  AttachmentService,
  CustomFieldValueService,
  EntityPathHeaderService,
  EntityRowReference,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  GenericEntityViewService,
  gridServiceFactory,
  Identifier,
  RichTextAttachmentDelegate,
  ReferentialDataService,
  RequirementVersionModel,
  RequirementVersionService,
  RestService,
  SquashTmDataRowType,
  WorkspaceWithTreeComponent,
  LocalPersistenceService,
} from 'sqtm-core';
import { concatMap, filter, map, take, takeUntil, withLatestFrom } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { RequirementVersionViewService } from '../../../requirement-version-view/services/requirement-version-view.service';
import { TranslateService } from '@ngx-translate/core';
import { RequirementVersionViewComponentData } from '../../../requirement-version-view/containers/requirement-version-view/requirement-version-view.component';
import {
  RVW_REQUIREMENT_VERSION_LINK_TABLE,
  RVW_REQUIREMENT_VERSION_LINK_TABLE_CONF,
  RVW_VERIFYING_TEST_CASE_TABLE,
  RVW_VERIFYING_TEST_CASE_TABLE_CONF,
} from '../../../requirement-version-view/requirement-version-view.constant';
import { rvwVerifyingTCTableDefinition } from '../../../requirement-version-view/components/verifying-test-case-table/verifying-test-case-table.component';
import { rvwRequirementVersionLinkTableDefinition } from '../../../requirement-version-view/components/requirement-version-link-table/requirement-version-link-table.component';

const logger = requirementViewLogger.compose('RequirementViewComponent');

@Component({
  selector: 'sqtm-app-requirement-view',
  templateUrl: './requirement-view.component.html',
  styleUrls: ['./requirement-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: RVW_VERIFYING_TEST_CASE_TABLE_CONF,
      useFactory: rvwVerifyingTCTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: RVW_VERIFYING_TEST_CASE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, RVW_VERIFYING_TEST_CASE_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: RVW_REQUIREMENT_VERSION_LINK_TABLE_CONF,
      useFactory: rvwRequirementVersionLinkTableDefinition,
      deps: [TranslateService, LocalPersistenceService],
    },
    {
      provide: RVW_REQUIREMENT_VERSION_LINK_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, RVW_REQUIREMENT_VERSION_LINK_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: RequirementVersionViewService,
      useClass: RequirementVersionViewService,
      deps: [
        RestService,
        ReferentialDataService,
        AttachmentService,
        TranslateService,
        RequirementVersionService,
        CustomFieldValueService,
        EntityViewAttachmentHelperService,
        EntityViewCustomFieldHelperService,
        RVW_VERIFYING_TEST_CASE_TABLE,
        RVW_REQUIREMENT_VERSION_LINK_TABLE,
      ],
    },
    {
      provide: EntityViewService,
      useExisting: RequirementVersionViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: RequirementVersionViewService,
    },
    {
      provide: RichTextAttachmentDelegate,
      useExisting: RequirementVersionViewService,
    },
  ],
})
export class RequirementViewComponent implements OnInit, OnDestroy {
  unsub$ = new Subject<void>();

  constructor(
    public readonly requirementVersionService: RequirementVersionService,
    private referentialDataService: ReferentialDataService,
    private requirementViewService: RequirementVersionViewService,
    private route: ActivatedRoute,
    private workspaceWithTree: WorkspaceWithTreeComponent,
    private entityPathHeaderService: EntityPathHeaderService,
  ) {}

  ngOnInit(): void {
    logger.debug(`ngOnInit RequirementView`);
    this.referentialDataService.loaded$
      .pipe(
        takeUntil(this.unsub$),
        filter((loaded) => loaded),
        take(1),
      )
      .subscribe(() => {
        logger.debug(`Loading RequirementView Data by http request`);
        this.loadData();
        this.initializeTreeSynchronisation();
        this.initializeRefreshViewObserver();
        this.initializeRefreshPathObserver();
      });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('requirementId')),
        concatMap((requirementId) =>
          this.requirementVersionService.loadCurrentVersion(parseInt(requirementId, 10)),
        ),
      )
      .subscribe({
        next: (requirementVersionModel: RequirementVersionModel) => {
          logger.debug(`Loading RequirementView ${requirementVersionModel.id}`);
          this.requirementViewService.load(requirementVersionModel);
        },
        error: (err) => this.requirementViewService.notifyEntityNotFound(err),
      });
  }

  private initializeTreeSynchronisation() {
    this.requirementViewService.simpleAttributeRequiringRefresh = [
      'name',
      'reference',
      'criticality',
      'category',
      'status',
      'description',
      'verifyingTestCases',
      'versionNumber',
    ];
    this.requirementViewService.externalRefreshRequired$
      .pipe(
        takeUntil(this.unsub$),
        withLatestFrom(this.requirementViewService.componentData$),
        map(([, componentData]: [any, RequirementVersionViewComponentData]) =>
          new EntityRowReference(
            componentData.requirementVersion.requirementId,
            SquashTmDataRowType.Requirement,
          ).asString(),
        ),
      )
      .subscribe((identifier: Identifier) => {
        this.workspaceWithTree.requireNodeRefresh([identifier]);
      });
  }

  private initializeRefreshViewObserver() {
    this.requirementVersionService.refreshView$
      .pipe(takeUntil(this.unsub$))
      .subscribe(() => this.loadData());
  }

  newVersion(requirementId: number) {
    const id = new EntityRowReference(requirementId, SquashTmDataRowType.Requirement).asString();
    this.workspaceWithTree.requireCurrentContainerRefresh(id);
  }

  private initializeRefreshPathObserver() {
    this.entityPathHeaderService.refreshPath$
      .pipe(takeUntil(this.unsub$))
      .subscribe(() => this.requirementViewService.updateEntityPath());
  }
}
