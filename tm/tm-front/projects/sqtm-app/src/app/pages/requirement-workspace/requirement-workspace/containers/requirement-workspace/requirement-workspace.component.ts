import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import {
  column,
  Extendable,
  GridColumnId,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RequirementTreeNodeServerOperationHandler,
  RestService,
  tree,
  TreeNodeCellRendererComponent,
  TreeNodeRendererDisplayOptions,
} from 'sqtm-core';
import { REQ_WS_TREE, REQ_WS_TREE_CONFIG } from '../../requirement-workspace.constant';

export function reqTreeConfigFactory(): GridDefinition {
  const options: TreeNodeRendererDisplayOptions = {
    ellipsisOnLeft: false,
    kind: 'treeNodeRendererDisplay',
  };
  return tree('requirement-workspace-main-tree')
    .server()
    .withServerUrl(['requirement-tree'])
    .withColumns([
      column(GridColumnId.NAME)
        .enableDnd()
        .changeWidthCalculationStrategy(new Extendable(300))
        .withRenderer(TreeNodeCellRendererComponent)
        .withOptions(options),
    ])
    .build();
}

@Component({
  selector: 'sqtm-app-requirement-workspace',
  templateUrl: './requirement-workspace.component.html',
  styleUrls: ['./requirement-workspace.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: REQ_WS_TREE_CONFIG,
      useFactory: reqTreeConfigFactory,
      deps: [],
    },
    RequirementTreeNodeServerOperationHandler,
    {
      provide: REQ_WS_TREE,
      useFactory: gridServiceFactory,
      deps: [
        RestService,
        REQ_WS_TREE_CONFIG,
        ReferentialDataService,
        RequirementTreeNodeServerOperationHandler,
      ],
    },
    {
      provide: GridService,
      useExisting: REQ_WS_TREE,
    },
  ],
})
export class RequirementWorkspaceComponent implements OnInit, OnDestroy {
  constructor(
    public readonly referentialDataService: ReferentialDataService,
    private requirementWorkspaceTree: GridService,
  ) {}

  ngOnInit(): void {
    this.referentialDataService.refresh().subscribe();
  }

  ngOnDestroy(): void {
    this.requirementWorkspaceTree.complete();
  }
}
