import {
  entitySelector,
  EntityViewState,
  provideInitialViewState,
  RequirementVersionCoverage,
  SqtmEntityState,
} from 'sqtm-core';
import {
  coverageEntitySelectors,
  CoverageState,
} from '../../../test-case-workspace/test-case-view/state/requirement-version-coverage.state';
import { createSelector } from '@ngrx/store';
import { TestCaseViewUiState } from '../../../test-case-workspace/test-case-view/state/test-case.state';

export interface ModifDuringExecStepState extends SqtmEntityState {
  id: number;
  actionStepTestCaseId: number;
  actionStepTestCaseName: string;
  actionStepTestCaseReference: string;
  executionTestCaseId: number;
  executionTestCaseName: string;
  executionTestCaseReference: string;
  action: string;
  expectedResult: string;
  coverages: CoverageState;
  uiState: TestCaseViewUiState;
}

export interface ModifDuringExecStepViewState
  extends EntityViewState<ModifDuringExecStepState, 'actionStep'> {
  actionStep: ModifDuringExecStepState;
}

export function provideModifDuringExecStepViewState(): Readonly<ModifDuringExecStepViewState> {
  return provideInitialViewState<ModifDuringExecStepState, 'actionStep'>('actionStep');
}

const coverageStateSelector = createSelector<
  ModifDuringExecStepViewState,
  [ModifDuringExecStepState],
  CoverageState
>(entitySelector, (actionStep: ModifDuringExecStepState) => {
  return actionStep.coverages;
});

const idSelector = createSelector<ModifDuringExecStepViewState, [ModifDuringExecStepState], number>(
  entitySelector,
  (actionStep: ModifDuringExecStepState) => {
    return actionStep.id;
  },
);

const testCaseIdSelector = createSelector<
  ModifDuringExecStepViewState,
  [ModifDuringExecStepState],
  number
>(entitySelector, (actionStep: ModifDuringExecStepState) => {
  return actionStep.actionStepTestCaseId;
});

const coveragesSelector = createSelector<
  ModifDuringExecStepViewState,
  [CoverageState],
  RequirementVersionCoverage[]
>(coverageStateSelector, coverageEntitySelectors.selectAll);

const directCoveragesSelector = createSelector<
  ModifDuringExecStepViewState,
  [RequirementVersionCoverage[], number],
  RequirementVersionCoverage[]
>(coveragesSelector, testCaseIdSelector, (coverages, _id) => {
  return coverages.filter((coverage) => coverage.directlyVerified);
});

export const linkedCoverageSelector = createSelector<
  ModifDuringExecStepViewState,
  [RequirementVersionCoverage[], number],
  (RequirementVersionCoverage & { linkedToStep: boolean })[]
>(directCoveragesSelector, idSelector, (coverages, actionStepId) => {
  return coverages.map((coverage) => {
    const linkedToStep =
      coverage.coverageStepInfos.filter((stepInfo) => stepInfo.id === actionStepId).length > 0;
    return { ...coverage, linkedToStep };
  });
});
