import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ReferentialDataService } from 'sqtm-core';
import { take } from 'rxjs/operators';
import { modifDuringExecStepViewLogger } from '../../modif-during-exec-step-view.logger';
import { ModifDuringExecViewService } from '../../services/modif-during-exec-view.service';
import { Observable } from 'rxjs';
import { ModifDuringExecState } from '../../states/modif-during-exec.state';

const logger = modifDuringExecStepViewLogger.compose('ModifDuringExecComponent');

@Component({
  selector: 'sqtm-app-modif-during-exec',
  templateUrl: './modif-during-exec.component.html',
  styleUrls: ['./modif-during-exec.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ModifDuringExecViewService],
})
export class ModifDuringExecComponent implements OnInit, OnDestroy {
  componentData$: Observable<ModifDuringExecState>;

  currentActionStepId$: Observable<number>;

  constructor(
    private route: ActivatedRoute,
    private referentialDataService: ReferentialDataService,
    private modifDuringExecViewService: ModifDuringExecViewService,
  ) {
    this.componentData$ = this.modifDuringExecViewService.componentData$;
  }

  ngOnInit(): void {
    this.fetchData();
  }

  ngOnDestroy(): void {
    this.modifDuringExecViewService.complete();
  }

  private fetchData() {
    this.referentialDataService.refresh().subscribe(() => {
      this.loadData();
    });
  }

  private loadData() {
    this.route.paramMap.pipe(take(1)).subscribe((params: ParamMap) => {
      const executionId = parseInt(params.get('executionId'), 10);
      logger.debug(`Loading modification view during exec ${executionId}`);
      this.modifDuringExecViewService.load(executionId);
    });
  }
}
