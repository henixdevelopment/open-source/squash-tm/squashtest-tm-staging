import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { DetailedTestStepViewService } from '../../services/detailed-test-step-view.service';
import { TestStepViewService } from '../../../../test-case-workspace/test-case-view/service/test-step-view.service';
import {
  AttachmentService,
  CopierService,
  createTestCaseCoverageMessageDialogConfiguration,
  CustomFieldValueService,
  DialogReference,
  DialogService,
  EntityViewAttachmentHelperService,
  EntityViewComponentData,
  EntityViewCustomFieldHelperService,
  GenericEntityViewService,
  GridService,
  gridServiceFactory,
  isDndDataFromRequirementTreePicker,
  ReferentialDataService,
  RestService,
  shouldShowCoverageMessageDialog,
  SqtmDropEvent,
  TestCasePermissions,
  TestCaseService,
  TestStepService,
  Workspaces,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import {
  DetailedTestCaseState,
  DetailedTestStepViewState,
} from '../../state/detailed-test-step-view.state';
import { combineLatest, Observable } from 'rxjs';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { concatMap, filter, map, take, takeUntil } from 'rxjs/operators';
import { detailedTestStepViewLogger } from '../../detailed-test-step-view.logger';
import { detailedStepCoverageTableDefinition } from '../../../../../components/detailed-step-views-common/components/detailed-step-coverage-table/detailed-step-coverage-table.component';
import { ActionStepState } from '../../../../test-case-workspace/test-case-view/state/test-step.state';
import {
  DETAILED_COVERAGE_TABLE,
  DETAILED_STEP_COVERAGE_TABLE_CONF,
  STEP_VIEW_REQUIREMENT_LINK_SERVICE,
} from '../../../../../components/detailed-step-views-common/detailed-step-view.constant.ts.constant';
import { AbstractRequirementLinkableStepComponent } from '../../../../../components/detailed-step-views-common/components/abstract-requirement-linkable-step.component';

const logger = detailedTestStepViewLogger.compose('DetailedTestStepViewComponent');

@Component({
  selector: 'sqtm-app-detailed-test-step-view',
  templateUrl: './detailed-test-step-view.component.html',
  styleUrls: ['./detailed-test-step-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: DETAILED_STEP_COVERAGE_TABLE_CONF,
      useFactory: detailedStepCoverageTableDefinition,
    },
    {
      provide: DETAILED_COVERAGE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, DETAILED_STEP_COVERAGE_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: TestStepViewService,
      useClass: TestStepViewService,
      deps: [
        TestStepService,
        EntityViewAttachmentHelperService,
        EntityViewCustomFieldHelperService,
        ReferentialDataService,
        CopierService,
      ],
    },
    {
      provide: DetailedTestStepViewService,
      useClass: DetailedTestStepViewService,
      deps: [
        RestService,
        ReferentialDataService,
        AttachmentService,
        TranslateService,
        TestCaseService,
        CustomFieldValueService,
        TestStepViewService,
        EntityViewAttachmentHelperService,
        EntityViewCustomFieldHelperService,
        DETAILED_COVERAGE_TABLE,
      ],
    },
    {
      provide: STEP_VIEW_REQUIREMENT_LINK_SERVICE,
      useExisting: DetailedTestStepViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: DetailedTestStepViewService,
    },
  ],
})
export class DetailedTestStepViewComponent
  extends AbstractRequirementLinkableStepComponent
  implements OnInit, OnDestroy
{
  @ViewChild('dropFileZone', { read: ElementRef })
  private dropFileZone: ElementRef;

  requirementWorkspace = Workspaces['requirement-workspace'];

  componentData$: Observable<DetailedTestStepViewComponentData>;

  constructor(
    private route: ActivatedRoute,
    public detailedTestStepViewService: DetailedTestStepViewService,
    router: Router,
    private referentialDataService: ReferentialDataService,
    renderer: Renderer2,
    dialogService: DialogService,
    @Inject(DETAILED_COVERAGE_TABLE) coverageGrid: GridService,
  ) {
    super(detailedTestStepViewService, router, renderer, dialogService, coverageGrid);
  }

  ngOnInit(): void {
    this.initializeObservables();
    this.fetchData();
  }

  private fetchData() {
    this.referentialDataService.refresh().subscribe(() => {
      this.loadData();
    });
  }

  private initializeObservables() {
    this.componentData$ = this.detailedTestStepViewService.componentData$;
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        take(1),
        map((params: ParamMap) => params.get('testCaseId')),
      )
      .subscribe((id) => {
        logger.debug(`Loading DetailedTestStepView ${id}`);
        this.detailedTestStepViewService.load(parseInt(id, 10));
      });
  }

  ngOnDestroy(): void {
    this.detailedTestStepViewService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  navigateBack() {
    // to be updated with a param in url when the return behavior will be defined for other views.
    this.router.navigate(['/test-case-workspace']);
  }

  handleNavigateTo(stepIndex: number) {
    this.detailedTestStepViewService.componentData$
      .pipe(
        take(1),
        map((state) => {
          const validatedIndex = DetailedTestStepViewService.validateIndex(stepIndex, state);
          return { testCaseId: state.testCase.id, validatedIndex };
        }),
      )
      .subscribe((value) => {
        this.router.navigate(['step', value.validatedIndex], { relativeTo: this.route });
      });
  }

  openRequirementTreePicker() {
    this.detailedTestStepViewService.openRequirementTreePicker();
  }

  closeRequirementPicker() {
    this.detailedTestStepViewService.closeRequirementTreePicker();
  }

  drop($event: SqtmDropEvent) {
    if (isDndDataFromRequirementTreePicker($event)) {
      const requirementIds = this.extractRequirementIdsFromDragAndDropData($event);
      if (requirementIds.length > 0) {
        this.detailedTestStepViewService
          .dropRequirement(requirementIds)
          .subscribe((operationReport) => {
            if (shouldShowCoverageMessageDialog(operationReport)) {
              this.dialogService.openDialog(
                createTestCaseCoverageMessageDialogConfiguration(operationReport),
              );
            }
            this.removeBorderOnRequirementTable();
          });
      }
    }
  }

  dropFiles($event: File[]) {
    combineLatest([this.detailedTestStepViewService.currentStepData$, this.componentData$])
      .pipe(
        take(1),
        filter(
          ([step, componentData]) =>
            step.kind === 'action-step' &&
            componentData.permissions.canAttach &&
            componentData.milestonesAllowModification,
        ),
      )
      .subscribe(([step]) => {
        const actionStep = step as ActionStepState;
        this.unmarkAsFileDropZone();
        this.detailedTestStepViewService.addAttachmentsToStep(
          $event,
          actionStep.id,
          actionStep.attachmentList.id,
        );
      });
  }

  dragFilesEnter() {
    combineLatest([this.detailedTestStepViewService.currentStepData$, this.componentData$])
      .pipe(
        take(1),
        filter(
          ([step, componentData]) =>
            step.kind === 'action-step' &&
            componentData.permissions.canAttach &&
            componentData.milestonesAllowModification,
        ),
      )
      .subscribe(() => this.markAsFileDropZone());
  }

  dragFileLeave() {
    this.detailedTestStepViewService.currentStepData$
      .pipe(
        take(1),
        filter((step) => step.kind === 'action-step'),
      )
      .subscribe(() => this.unmarkAsFileDropZone());
  }

  private unmarkAsFileDropZone() {
    this.renderer.removeClass(
      this.dropFileZone.nativeElement,
      'sqtm-core-border-current-workspace-color',
    );
  }

  private markAsFileDropZone() {
    this.renderer.addClass(
      this.dropFileZone.nativeElement,
      'sqtm-core-border-current-workspace-color',
    );
  }

  showAddActionStepForm() {
    this.detailedTestStepViewService
      .addActionStepForm()
      .subscribe((state) => this.handleNavigateTo(state.testCase.currentStepIndex + 1));
  }

  handleAddActionTestStep() {
    this.detailedTestStepViewService.notifyCreateStep();
  }

  handleCancelAddActionTestStep() {
    this.detailedTestStepViewService
      .cancelAddTestStep()
      .subscribe((state) => this.handleNavigateTo(state.testCase.currentStepIndex));
  }

  showDeleteStepDialog() {
    const dialogReference: DialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.test-case-workspace.dialog.title.remove-test-step.singular',
      messageKey: 'sqtm-core.test-case-workspace.dialog.message.remove-test-step.singular',
    });
    dialogReference.dialogClosed$
      .pipe(
        takeUntil(this.unsub$),
        take(1),
        filter((confirm) => confirm),
        concatMap(() => this.detailedTestStepViewService.deleteCurrentStep()),
      )
      .subscribe((state) => this.handlePostStepSuppressionNavigation(state));
  }

  private handlePostStepSuppressionNavigation(state: DetailedTestStepViewState) {
    const currentStepIndex = state.testCase.currentStepIndex;
    const numberOfSteps = state.testCase.testSteps.ids.length;
    if (numberOfSteps === 0) {
      this.navigateBack();
    } else {
      this.handleNavigateTo(currentStepIndex);
    }
  }

  getTestCaseName(componentData: DetailedTestStepViewComponentData) {
    const reference = componentData.testCase.reference;

    if (reference != null && reference !== '') {
      return `${reference} - ${componentData.testCase.name}`;
    } else {
      return componentData.testCase.name;
    }
  }
}

export interface DetailedTestStepViewComponentData
  extends EntityViewComponentData<DetailedTestCaseState, 'testCase', TestCasePermissions> {}
