import { AfterViewInit, ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { DetailedTestStepViewService } from '../../services/detailed-test-step-view.service';
import { map, take } from 'rxjs/operators';
import { CustomField, CustomFieldBindingDataByEntity } from 'sqtm-core';

@Component({
  selector: 'sqtm-app-test-step-informations',
  templateUrl: './test-step-informations.component.html',
  styleUrls: ['./test-step-informations.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestStepInformationsComponent implements AfterViewInit {
  constructor(
    private route: ActivatedRoute,
    public detailedTestStepViewService: DetailedTestStepViewService,
  ) {}

  ngAfterViewInit(): void {
    this.initNavigation();
  }

  private initNavigation() {
    // had to setTimeout or the async pipe is not able to detect change correctly at first modification...
    setTimeout(() => {
      this.route.paramMap
        .pipe(
          take(1),
          map((params: ParamMap) => params.get('stepIndex')),
        )
        .subscribe((index) => {
          this.detailedTestStepViewService.changeCurrentStepIndex(Number.parseInt(index, 10));
        });
    });
  }

  extractCustomFields(customFieldBinding: CustomFieldBindingDataByEntity): CustomField[] {
    return customFieldBinding.TEST_STEP.map((binding) => binding.customField);
  }
}
