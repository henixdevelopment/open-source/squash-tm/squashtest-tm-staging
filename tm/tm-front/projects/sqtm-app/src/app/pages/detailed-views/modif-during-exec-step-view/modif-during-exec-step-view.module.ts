import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModifDuringExecStepViewComponent } from './containers/modif-during-exec-step-view/modif-during-exec-step-view.component';
import { RouterModule, Routes } from '@angular/router';
import { ModifDuringExecComponent } from './containers/modif-during-exec/modif-during-exec.component';
import {
  AttachmentModule,
  CustomFieldModule,
  GridModule,
  NavBarModule,
  SqtmDragAndDropModule,
  UiManagerModule,
  WorkspaceCommonModule,
} from 'sqtm-core';
import { TranslateModule } from '@ngx-translate/core';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { ExecTestCaseStepInformationComponent } from './components/exec-test-case-step-information/exec-test-case-step-information.component';
import { DetailedStepViewsCommonModule } from '../../../components/detailed-step-views-common/detailed-step-views-common.module';

export const routes: Routes = [
  {
    path: 'execution/:executionId',
    component: ModifDuringExecComponent,
    children: [
      {
        path: 'step/:stepId',
        component: ModifDuringExecStepViewComponent,
      },
    ],
  },
];

@NgModule({
  declarations: [
    ModifDuringExecStepViewComponent,
    ModifDuringExecComponent,
    ExecTestCaseStepInformationComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NavBarModule,
    UiManagerModule,
    WorkspaceCommonModule,
    TranslateModule,
    NzIconModule,
    NzToolTipModule,
    NzDropDownModule,
    SqtmDragAndDropModule,
    AttachmentModule,
    CustomFieldModule,
    DetailedStepViewsCommonModule,
    GridModule,
  ],
})
export class ModifDuringExecStepViewModule {}
