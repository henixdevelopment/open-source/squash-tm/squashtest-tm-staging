import { TestBed } from '@angular/core/testing';

import { ModifDuringExecStepViewService } from './modif-during-exec-step-view.service';
import { AppTestingUtilsModule } from '../../../../utils/testing-utils/app-testing-utils.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';
import { GridTestingModule } from 'sqtm-core';

describe('ModifDuringExecStepViewService', () => {
  let service: ModifDuringExecStepViewService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        AppTestingUtilsModule,
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        GridTestingModule,
      ],
      providers: [ModifDuringExecStepViewService],
    });
    service = TestBed.inject(ModifDuringExecStepViewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
