import { TestBed } from '@angular/core/testing';

import { DetailedTestStepViewService } from './detailed-test-step-view.service';
import {
  AttachmentService,
  CustomFieldValueService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  Permissions,
  ReferentialDataService,
  RestService,
  TestCaseService,
} from 'sqtm-core';
import { of } from 'rxjs';
import { TestStepViewService } from '../../../test-case-workspace/test-case-view/service/test-step-view.service';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

describe('DetailedTestStepViewService', () => {
  let service: DetailedTestStepViewService;
  const stepViewServiceMock = jasmine.createSpyObj('testStepViewService', [
    'writeAction',
    'updateAction',
    'initializeStepState',
  ]);

  const restServiceMock = {} as RestService;

  const referentialDataService = {} as ReferentialDataService;

  referentialDataService.connectToProjectData = jasmine.createSpy().and.returnValue(
    of({
      permissions: { TEST_CASE: [Permissions.WRITE] },
    }),
  );

  referentialDataService.globalConfiguration$ = of(null);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      providers: [
        { provide: TestStepViewService, useValue: stepViewServiceMock },
        { provide: RestService, useValue: restServiceMock },
        {
          provide: ReferentialDataService,
          useValue: referentialDataService,
        },
        {
          provide: DetailedTestStepViewService,
          useClass: DetailedTestStepViewService,
          deps: [
            RestService,
            ReferentialDataService,
            AttachmentService,
            TranslateService,
            TestCaseService,
            CustomFieldValueService,
            TestStepViewService,
            EntityViewAttachmentHelperService,
            EntityViewCustomFieldHelperService,
          ],
        },
      ],
    });
    service = TestBed.inject(DetailedTestStepViewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
