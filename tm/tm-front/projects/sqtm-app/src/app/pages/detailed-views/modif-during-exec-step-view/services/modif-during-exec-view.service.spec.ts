import { TestBed } from '@angular/core/testing';

import { ModifDuringExecViewService } from './modif-during-exec-view.service';
import { AppTestingUtilsModule } from '../../../../utils/testing-utils/app-testing-utils.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

describe('ModifDuringExecViewService', () => {
  let service: ModifDuringExecViewService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, RouterTestingModule],
      providers: [ModifDuringExecViewService],
    });
    service = TestBed.inject(ModifDuringExecViewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
