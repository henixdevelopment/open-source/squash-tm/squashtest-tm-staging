import { sqtmAppLogger } from '../../../app-logger';

export const modifDuringExecStepViewLogger = sqtmAppLogger.compose('modif-during-exec-step-view');
