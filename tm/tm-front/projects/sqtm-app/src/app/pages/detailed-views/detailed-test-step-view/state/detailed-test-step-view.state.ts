import {
  entitySelector,
  EntityViewState,
  Milestone,
  provideInitialViewState,
  RequirementVersionCoverage,
  SqtmEntityState,
  TestCaseKindKeys,
} from 'sqtm-core';
import {
  TestStepsState,
  TestStepState,
} from '../../../test-case-workspace/test-case-view/state/test-step.state';
import {
  coverageEntitySelectors,
  CoverageState,
} from '../../../test-case-workspace/test-case-view/state/requirement-version-coverage.state';
import { TestCaseViewUiState } from '../../../test-case-workspace/test-case-view/state/test-case.state';
import { createSelector } from '@ngrx/store';
import { ParameterState } from '../../../test-case-workspace/test-case-view/state/parameter.state';
import { DataSetState } from '../../../test-case-workspace/test-case-view/state/dataset.state';
import { DatasetParamValueState } from '../../../test-case-workspace/test-case-view/state/dataset-param-value.state';

export interface DetailedTestStepViewState
  extends EntityViewState<DetailedTestCaseState, 'testCase'> {
  testCase: DetailedTestCaseState;
}

export function provideInitialDetailedTestStepView(): Readonly<DetailedTestStepViewState> {
  return provideInitialViewState<DetailedTestCaseState, 'testCase'>('testCase');
}

export interface DetailedTestCaseState extends SqtmEntityState {
  name: string;
  reference: string;
  kind: TestCaseKindKeys;
  testSteps: TestStepsState;
  coverages: CoverageState;
  milestones: Milestone[];
  uiState: TestCaseViewUiState;
  lastModifiedOn: Date;
  lastModifiedBy: string;
  createdOn: Date;
  createdBy: string;
  currentStepIndex: number;
  parameters: ParameterState;
  datasets: DataSetState;
  datasetParamValues: DatasetParamValueState;
}

export const testStepStateSelector = createSelector<
  DetailedTestStepViewState,
  [DetailedTestCaseState],
  TestStepsState
>(entitySelector, (testCase: DetailedTestCaseState) => {
  return testCase.testSteps;
});

export const currentStepIndexSelector = createSelector<
  DetailedTestStepViewState,
  [DetailedTestCaseState],
  number
>(entitySelector, (testCase: DetailedTestCaseState) => {
  return testCase.currentStepIndex;
});

export const currentTestStepStateSelector = createSelector<
  DetailedTestStepViewState,
  [TestStepsState, number],
  TestStepState
>(testStepStateSelector, currentStepIndexSelector, (testStepSate, currentStepIndex) => {
  const id = testStepSate.ids[currentStepIndex];
  return testStepSate.entities[id];
});

export const coverageStateSelector = createSelector<
  DetailedTestStepViewState,
  [DetailedTestCaseState],
  CoverageState
>(entitySelector, (testCase: DetailedTestCaseState) => {
  return testCase.coverages;
});

const coveragesSelector = createSelector<
  DetailedTestStepViewState,
  [CoverageState],
  RequirementVersionCoverage[]
>(coverageStateSelector, coverageEntitySelectors.selectAll);

const directlyVerifiedCoveragesSelector = createSelector<
  DetailedTestStepViewState,
  [RequirementVersionCoverage[]],
  RequirementVersionCoverage[]
>(coveragesSelector, (coverages) => coverages.filter((cov) => cov.directlyVerified));

export const coveragesWithCurrentStepSelector = createSelector<
  DetailedTestStepViewState,
  [RequirementVersionCoverage[], TestStepState],
  RequirementVersionCoverage[]
>(
  directlyVerifiedCoveragesSelector,
  currentTestStepStateSelector,
  (coverages, testStepState: TestStepState) => {
    if (testStepState) {
      return coverages.map((coverage) => {
        const linkedToStep =
          coverage.coverageStepInfos.filter((stepInfo) => stepInfo.id === testStepState.id).length >
          0;
        return { ...coverage, linkedToStep };
      });
    } else {
      return coverages;
    }
  },
);
