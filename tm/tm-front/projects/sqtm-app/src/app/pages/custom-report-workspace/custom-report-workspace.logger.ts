import { Logger } from 'sqtm-core';
import { sqtmAppLogger } from '../../app-logger';

export const customReportWorkspaceLogger: Logger = sqtmAppLogger.compose('custom-report-workspace');
