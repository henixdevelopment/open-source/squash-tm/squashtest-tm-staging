import { ReportDefinitionViewModel, SqtmEntityState } from 'sqtm-core';

export interface ReportDefinitionState extends SqtmEntityState, ReportDefinitionViewModel {}
