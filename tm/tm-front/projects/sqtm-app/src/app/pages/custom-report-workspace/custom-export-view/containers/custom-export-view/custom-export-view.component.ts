import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { CustomExportViewService } from '../../services/custom-export-view.service';
import { map, takeUntil, withLatestFrom } from 'rxjs/operators';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Subject } from 'rxjs';
import {
  CustomReportPermissions,
  EntityRowReference,
  EntityViewComponentData,
  EntityViewService,
  GenericEntityViewService,
  Identifier,
  SquashTmDataRowType,
  WorkspaceWithTreeComponent,
} from 'sqtm-core';
import { CustomExportState } from '../../state/custom-export.state';

@Component({
  selector: 'sqtm-app-custom-export-view',
  templateUrl: './custom-export-view.component.html',
  styleUrls: ['./custom-export-view.component.less'],
  providers: [
    { provide: CustomExportViewService, useClass: CustomExportViewService },
    { provide: GenericEntityViewService, useExisting: CustomExportViewService },
    { provide: EntityViewService, useExisting: CustomExportViewService },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomExportViewComponent implements OnInit, OnDestroy {
  private unsub$ = new Subject<void>();

  constructor(
    public readonly route: ActivatedRoute,
    public readonly customExportViewService: CustomExportViewService,
    private readonly workspaceWithTree: WorkspaceWithTreeComponent,
  ) {}

  ngOnInit(): void {
    this.loadData();
    this.initializeTreeSynchronisation();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
    this.customExportViewService.complete();
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('customReportLibraryNodeId')),
      )
      .subscribe((id) => {
        this.customExportViewService.load(parseInt(id, 10));
      });
  }

  private initializeTreeSynchronisation() {
    this.customExportViewService.simpleAttributeRequiringRefresh = ['name'];

    this.customExportViewService.externalRefreshRequired$
      .pipe(
        takeUntil(this.unsub$),
        withLatestFrom(this.customExportViewService.componentData$),
        map(([, componentData]) =>
          new EntityRowReference(
            componentData.customExport.customReportLibraryNodeId,
            SquashTmDataRowType.CustomReportCustomExport,
          ).asString(),
        ),
      )
      .subscribe((identifier: Identifier) => {
        this.workspaceWithTree.requireNodeRefresh([identifier]);
      });
  }
}

export interface CustomExportViewComponentData
  extends EntityViewComponentData<CustomExportState, 'customExport', CustomReportPermissions> {}
