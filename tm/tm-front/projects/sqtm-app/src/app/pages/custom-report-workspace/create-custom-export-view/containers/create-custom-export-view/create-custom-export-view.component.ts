import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CustomExportWorkbenchData, ReferentialDataService } from 'sqtm-core';
import { concatMap, map, withLatestFrom } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { CreateCustomExportViewService } from '../../services/create-custom-export-view.service';

@Component({
  selector: 'sqtm-app-create-custom-export-view',
  templateUrl: './create-custom-export-view.component.html',
  styleUrls: ['./create-custom-export-view.component.less'],
  providers: [{ provide: CreateCustomExportViewService, useClass: CreateCustomExportViewService }],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateCustomExportViewComponent implements OnInit {
  private _workbenchData = new Subject<CustomExportWorkbenchData>();
  public workbenchData$ = this._workbenchData.asObservable();

  constructor(
    private referentialDataService: ReferentialDataService,
    private activatedRoute: ActivatedRoute,
    private createCustomExportViewService: CreateCustomExportViewService,
  ) {}

  ngOnInit(): void {
    this.referentialDataService
      .refresh()
      .pipe(
        withLatestFrom(this.activatedRoute.params),
        map(([, params]) => Number.parseInt(params['customReportLibraryNodeId'], 10)),
        concatMap((containerId) =>
          this.createCustomExportViewService.fetchWorkbenchData(containerId),
        ),
      )
      .subscribe((workbenchData: CustomExportWorkbenchData) => {
        this._workbenchData.next(workbenchData);
      });
  }
}
