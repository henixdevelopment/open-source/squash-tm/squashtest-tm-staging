import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable } from 'rxjs';
import {
  CustomReportFolderViewComponentData,
  CustomReportFolderViewService,
} from '../../services/custom-report-folder-view.service';

@Component({
  selector: 'sqtm-app-custom-report-folder-view-content',
  templateUrl: './custom-report-folder-view-content.component.html',
  styleUrls: ['./custom-report-folder-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomReportFolderViewContentComponent {
  componentData$: Observable<CustomReportFolderViewComponentData>;

  constructor(private customReportFolderViewService: CustomReportFolderViewService) {
    this.componentData$ = customReportFolderViewService.componentData$;
  }
}
