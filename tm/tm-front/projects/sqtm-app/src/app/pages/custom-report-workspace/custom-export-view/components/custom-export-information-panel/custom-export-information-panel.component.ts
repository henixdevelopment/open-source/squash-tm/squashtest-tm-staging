import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { CustomExportState } from '../../state/custom-export.state';
import { IdentifiedExportColumn } from '../../../../../components/custom-export-workbench/state/custom-export-workbench.state';
import { CustomExportColumn, DisplayOption, ReferentialDataService } from 'sqtm-core';
import { DatePipe } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'sqtm-app-custom-export-information-panel',
  templateUrl: './custom-export-information-panel.component.html',
  styleUrls: ['./custom-export-information-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe],
})
export class CustomExportInformationPanelComponent implements OnInit {
  identifiedColumns: IdentifiedExportColumn[] = [];
  printExecutionOptions: DisplayOption[];
  formGroup: FormGroup;

  @Input()
  set customExport(customExport: CustomExportState) {
    this._customExport = customExport;
    this.refreshIdentifiedColumns();
  }

  get customExport(): CustomExportState {
    return this._customExport;
  }

  get customExportName(): string {
    return this._customExport.scopeNodes.map((scopeNode) => scopeNode.name).join(', ');
  }

  private _customExport: CustomExportState;

  constructor(
    public readonly referentialDataService: ReferentialDataService,
    public readonly cdr: ChangeDetectorRef,
    public readonly fb: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.printExecutionOptions = [
      {
        id: ExecutionPrintOptions.PRINT_LAST_EXECUTION,
        label:
          'sqtm-core.custom-report-workspace.create-custom-export.workbench.label.options.last-execution',
      },
      {
        id: ExecutionPrintOptions.PRINT_ALL_EXECUTIONS,
        label:
          'sqtm-core.custom-report-workspace.create-custom-export.workbench.label.options.all-executions',
      },
    ];
    this.formGroup = this.fb.group({
      printExecutionOption: this.fb.control(ExecutionPrintOptions.PRINT_LAST_EXECUTION, [
        Validators.required,
      ]),
    });
  }

  private refreshIdentifiedColumns(): void {
    if (this._customExport) {
      this.identifiedColumns = this.asIdentifiedColumns(
        this._customExport.columns,
        this._customExport.customFieldsOnScope,
      );
      this.cdr.markForCheck();
    }
  }

  private asIdentifiedColumns(
    columns: CustomExportColumn[],
    cufsOnScope: { [key: number]: string },
  ): IdentifiedExportColumn[] {
    return columns.map((col) => ({
      columnName: col.columnName,
      entityType: col.entityType,
      cufId: col.cufId,
      label: this.findLabel(col, cufsOnScope),
      id: undefined,
    }));
  }

  private findLabel(column: CustomExportColumn, cufsOnScope: { [key: number]: string }): string {
    if (column.cufId) {
      return this.findCufLabel(column, cufsOnScope);
    } else {
      return column.columnName;
    }
  }

  private findCufLabel(column: CustomExportColumn, cufsOnScope: { [key: number]: string }): string {
    const customFieldLabel: string = cufsOnScope[column.cufId];

    if (customFieldLabel) {
      return customFieldLabel;
    } else {
      return 'sqtm-core.generic.label.deleted.masculine';
    }
  }
}

export enum ExecutionPrintOptions {
  PRINT_ALL_EXECUTIONS = 'allExec',
  PRINT_LAST_EXECUTION = 'lastExec',
}
