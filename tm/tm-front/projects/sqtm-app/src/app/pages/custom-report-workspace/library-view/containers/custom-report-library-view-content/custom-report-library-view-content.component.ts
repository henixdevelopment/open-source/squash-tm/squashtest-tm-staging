import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {
  CustomReportLibraryViewComponentData,
  CustomReportLibraryViewService,
} from '../../service/custom-report-library-view.service';

@Component({
  selector: 'sqtm-app-custom-report-library-view-content',
  templateUrl: './custom-report-library-view-content.component.html',
  styleUrls: ['./custom-report-library-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomReportLibraryViewContentComponent implements OnDestroy {
  private unsub$ = new Subject<void>();
  componentData$: Observable<CustomReportLibraryViewComponentData>;

  constructor(private customReportLibraryViewService: CustomReportLibraryViewService) {
    this.componentData$ = customReportLibraryViewService.componentData$;
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
