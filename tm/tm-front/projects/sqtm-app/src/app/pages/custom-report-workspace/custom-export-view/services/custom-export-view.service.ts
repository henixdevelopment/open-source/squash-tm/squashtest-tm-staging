import { Injectable } from '@angular/core';
import {
  AttachmentService,
  CustomExportModel,
  CustomFieldValueService,
  CustomReportPermissions,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  ProjectData,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { CustomExportState } from '../state/custom-export.state';
import {
  CustomExportViewState,
  provideInitialCustomExportView,
} from '../state/custom-export-view.state';
import { map, switchMap, take, withLatestFrom } from 'rxjs/operators';
import { HttpClient, HttpEvent, HttpEventType } from '@angular/common/http';

@Injectable()
export class CustomExportViewService extends EntityViewService<
  CustomExportState,
  'customExport',
  CustomReportPermissions
> {
  private static downloadUrlBase = 'custom-exports/generate';

  constructor(
    protected restService: RestService,
    protected referentialDataService: ReferentialDataService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected customFieldValueService: CustomFieldValueService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
    protected customFieldHelper: EntityViewCustomFieldHelperService,
    private http: HttpClient,
  ) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper,
    );
  }

  addSimplePermissions(projectData: ProjectData) {
    return new CustomReportPermissions(projectData);
  }

  getInitialState(): CustomExportViewState {
    return provideInitialCustomExportView();
  }

  downloadReport(printOnlyLastExec: boolean) {
    return this.componentData$
      .pipe(
        take(1),
        map((componentData) => this.buildDownloadUrl(componentData, printOnlyLastExec)),
        switchMap((url: string) => {
          return this.http
            .get(url, {
              responseType: 'blob',
              observe: 'events',
            })
            .pipe(map((resp) => this.downloadFile(resp)));
        }),
      )
      .subscribe();
  }
  private buildDownloadUrl(componentData: any, printOnlyLastExec: boolean): string {
    const nodeId = componentData.customExport.customReportLibraryNodeId;
    const base = CustomExportViewService.downloadUrlBase;
    const suffix = printOnlyLastExec ? '-with-last-exec/' : '/';
    return `${this.restService.backendRootUrl}${base}${suffix}${nodeId}`;
  }

  private downloadFile(resp: HttpEvent<Blob>) {
    if (resp.type === HttpEventType.Response) {
      const a = document.createElement('a');
      const blob = new Blob([resp.body]);
      const url = window.URL.createObjectURL(blob);
      const filename = resp.headers.get('Content-Disposition').split('filename=')[1];
      a.href = url;
      a.download = filename;
      a.click();
      a.remove();
      window.URL.revokeObjectURL(url);
    }
  }

  load(id: number): void {
    this.restService
      .getWithoutErrorHandling<CustomExportModel>([
        'custom-report-custom-export-view',
        id.toString(),
      ])
      .pipe(withLatestFrom(this.state$))
      .subscribe({
        next: ([model, state]: [CustomExportModel, CustomExportViewState]) => {
          const customExportState = { ...state.customExport, ...model };
          this.store.commit(this.updateEntity(customExportState, state));
        },
        error: (err) => this.notifyEntityNotFound(err),
      });
  }
}
