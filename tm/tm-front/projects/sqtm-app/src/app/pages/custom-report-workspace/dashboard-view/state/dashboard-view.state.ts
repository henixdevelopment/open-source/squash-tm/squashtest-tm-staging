import {
  ChartBinding,
  CustomDashboardBinding,
  entitySelector,
  EntityViewState,
  provideInitialViewState,
  ReportBinding,
  SqtmEntityState,
  WorkspaceKeys,
} from 'sqtm-core';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createSelector } from '@ngrx/store';

export const chartBindingAdapter = createEntityAdapter<ChartBinding>();
export const reportBindingAdapter = createEntityAdapter<ReportBinding>();

export interface DashboardState extends SqtmEntityState {
  name: string;
  createdBy: string;
  createdOn: Date;
  customReportLibraryNodeId: number;
  chartBindings: EntityState<ChartBinding>;
  reportBindings: EntityState<ReportBinding>;
  generatedOn: Date;
  favoriteWorkspaces: WorkspaceKeys[];
}

export interface DashboardViewState extends EntityViewState<DashboardState, 'dashboard'> {
  dashboard: DashboardState;
}

export function provideInitialDashboardViewState(): Readonly<DashboardViewState> {
  return provideInitialViewState<DashboardState, 'dashboard'>('dashboard');
}

const chartBindingsStateSelector = createSelector<
  DashboardViewState,
  [DashboardState],
  EntityState<ChartBinding>
>(entitySelector, (dashboardState: DashboardState) => {
  return dashboardState.chartBindings;
});

const reportBindingsStateSelector = createSelector<
  DashboardViewState,
  [DashboardState],
  EntityState<ReportBinding>
>(entitySelector, (dashboardState: DashboardState) => {
  return dashboardState.reportBindings;
});

const chartBindingSelector = createSelector<
  DashboardViewState,
  [EntityState<ChartBinding>],
  ChartBinding[]
>(chartBindingsStateSelector, chartBindingAdapter.getSelectors().selectAll);

const reportBindingSelector = createSelector<
  DashboardViewState,
  [EntityState<ReportBinding>],
  ReportBinding[]
>(reportBindingsStateSelector, reportBindingAdapter.getSelectors().selectAll);

export const allBindingSelector = createSelector<
  DashboardViewState,
  [ChartBinding[], ReportBinding[]],
  CustomDashboardBinding[]
>(chartBindingSelector, reportBindingSelector, (chartBindings, reportBindings) => {
  return [...chartBindings, ...reportBindings] as CustomDashboardBinding[];
});
