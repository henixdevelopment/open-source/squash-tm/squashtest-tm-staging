import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { of, Subject } from 'rxjs';
import {
  CustomReportPermissions,
  EditableTextFieldComponent,
  EntityRowReference,
  EntityViewComponentData,
  EntityViewService,
  extractSquashFirstFieldError,
  GenericEntityViewService,
  Identifier,
  ReferentialDataService,
  SquashTmDataRowType,
  WorkspaceWithTreeComponent,
} from 'sqtm-core';
import { ChartDefinitionViewService } from '../../services/chart-definition-view.service';
import { ChartDefinitionState } from '../../state/chart-definition.state';
import { filter, map, take, takeUntil, withLatestFrom } from 'rxjs/operators';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { chartDefinitionViewLogger } from '../../chart-definition-view.logger';
import { ChartDefinitionViewState } from '../../state/chart-definition-view.state';
import { TranslateService } from '@ngx-translate/core';

const logger = chartDefinitionViewLogger.compose('ChartDefinitionViewComponent');

@Component({
  selector: 'sqtm-app-chart-definition-view',
  templateUrl: './chart-definition-view.component.html',
  styleUrls: ['./chart-definition-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: ChartDefinitionViewService,
      useClass: ChartDefinitionViewService,
    },
    {
      provide: EntityViewService,
      useExisting: ChartDefinitionViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: ChartDefinitionViewService,
    },
  ],
})
export class ChartDefinitionViewComponent implements OnInit, OnDestroy {
  @ViewChild('chartNameEditableField')
  private chartNameEditableField: EditableTextFieldComponent;

  private unsub$ = new Subject<void>();

  constructor(
    public readonly chartDefinitionViewService: ChartDefinitionViewService,
    private referentialDataService: ReferentialDataService,
    private route: ActivatedRoute,
    private workspaceWithTree: WorkspaceWithTreeComponent,
    private translateService: TranslateService,
  ) {}

  ngOnInit() {
    this.referentialDataService.loaded$
      .pipe(
        takeUntil(this.unsub$),
        filter((loaded) => loaded),
        take(1),
      )
      .subscribe(() => {
        logger.debug(`Loading ChartDefinitionViewComponent Data by http request`);
        this.loadData();
        this.initializeTreeSynchronisation();
      });
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('customReportLibraryNodeId')),
      )
      .subscribe((id) => {
        this.chartDefinitionViewService.load(parseInt(id, 10));
      });
  }

  private initializeTreeSynchronisation() {
    this.chartDefinitionViewService.externalRefreshRequired$
      .pipe(
        takeUntil(this.unsub$),
        withLatestFrom(this.chartDefinitionViewService.componentData$),
        map(([, componentData]: [any, ChartDefinitionViewComponentData]) =>
          new EntityRowReference(
            componentData.chartDefinition.customReportLibraryNodeId,
            SquashTmDataRowType.ChartDefinition,
          ).asString(),
        ),
      )
      .subscribe((identifier: Identifier) => {
        this.workspaceWithTree.requireNodeRefresh([identifier]);
      });
  }

  changeChartName(newChartName: string, customReportLibraryNodeId: number) {
    this.chartDefinitionViewService
      .changeChartName(newChartName, customReportLibraryNodeId)
      .subscribe({
        next: (state: ChartDefinitionViewState) => {
          this.chartNameEditableField.value = state.chartDefinition.name;
        },
        error: (error) => {
          this.handleError(error, 'name');
        },
      });
  }

  private handleError(error, fieldName: string) {
    const squashTMerror = extractSquashFirstFieldError(error);
    if (
      squashTMerror.kind === 'FIELD_VALIDATION_ERROR' &&
      squashTMerror.fieldValidationErrors[0].fieldName === fieldName
    ) {
      const i18nkey = this.translateService.instant(squashTMerror.fieldValidationErrors[0].i18nKey);
      this.chartNameEditableField.showExternalErrorMessage([i18nkey]);
    } else {
      console.error(error);
    }
    return of(error);
  }

  ngOnDestroy(): void {
    this.chartDefinitionViewService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }
}

export interface ChartDefinitionViewComponentData
  extends EntityViewComponentData<
    ChartDefinitionState,
    'chartDefinition',
    CustomReportPermissions
  > {}
