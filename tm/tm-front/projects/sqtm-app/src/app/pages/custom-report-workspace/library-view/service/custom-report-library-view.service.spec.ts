import { TestBed } from '@angular/core/testing';

import { CustomReportLibraryViewService } from './custom-report-library-view.service';
import { AppTestingUtilsModule } from '../../../../utils/testing-utils/app-testing-utils.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('CustomReportLibraryViewService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, TranslateModule.forRoot()],
      providers: [CustomReportLibraryViewService],
    }),
  );

  it('should be created', () => {
    const service: CustomReportLibraryViewService = TestBed.inject(CustomReportLibraryViewService);
    expect(service).toBeTruthy();
  });
});
