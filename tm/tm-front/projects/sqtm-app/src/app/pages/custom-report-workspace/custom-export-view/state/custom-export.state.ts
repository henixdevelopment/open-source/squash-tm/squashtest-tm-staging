import { CustomExportModel, SqtmEntityState } from 'sqtm-core';

export interface CustomExportState extends SqtmEntityState, CustomExportModel {}
