import { TestBed } from '@angular/core/testing';

import { CustomExportViewService } from './custom-export-view.service';
import {
  mockPassThroughTranslateService,
  mockRestService,
} from '../../../../utils/testing-utils/mocks.service';
import { RestService } from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('CustomExportViewService', () => {
  let service: CustomExportViewService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: RestService, useValue: mockRestService() },
        { provide: CustomExportViewService, useClass: CustomExportViewService },
        { provide: TranslateService, useValue: mockPassThroughTranslateService() },
      ],
    });
    service = TestBed.inject(CustomExportViewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
