import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { concatMap, map, withLatestFrom } from 'rxjs/operators';
import { ActivatedRoute, Params } from '@angular/router';
import { ReferentialDataService, ReportDefinitionService, ReportWorkbenchData } from 'sqtm-core';
import { Subject } from 'rxjs';

@Component({
  selector: 'sqtm-app-create-report-view',
  templateUrl: './create-report-view.component.html',
  styleUrls: ['./create-report-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateReportViewComponent implements OnInit {
  public workbenchData$ = new Subject<ReportWorkbenchData>();

  constructor(
    private referentialDataService: ReferentialDataService,
    private reportDefinitionService: ReportDefinitionService,
    private activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.referentialDataService
      .refresh()
      .pipe(
        withLatestFrom(this.activatedRoute.params),
        concatMap(([, params]: [boolean, Params]) => {
          const id = Number.parseInt(params['customReportLibraryNodeId'], 10);
          return this.reportDefinitionService
            .getWorkbenchData(id)
            .pipe(
              map((workbenchData: ReportWorkbenchData) => ({ ...workbenchData, containerId: id })),
            );
        }),
      )
      .subscribe((workbenchData: ReportWorkbenchData) => {
        this.workbenchData$.next(workbenchData);
      });
  }
}
