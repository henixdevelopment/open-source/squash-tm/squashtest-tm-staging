import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { ChartDefinitionService, ChartWorkbenchData, ReferentialDataService } from 'sqtm-core';
import { ActivatedRoute, Params } from '@angular/router';
import { concatMap, withLatestFrom } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'sqtm-app-create-chart-view',
  templateUrl: './create-chart-view.component.html',
  styleUrls: ['./create-chart-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateChartViewComponent implements OnInit, OnDestroy {
  public workbenchData$ = new Subject<ChartWorkbenchData>();

  constructor(
    private referentialDataService: ReferentialDataService,
    private chartDefinitionService: ChartDefinitionService,
    private activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.referentialDataService
      .refresh()
      .pipe(
        withLatestFrom(this.activatedRoute.params),
        concatMap(([, params]: [boolean, Params]) => {
          const id = Number.parseInt(params['customReportLibraryNodeId'], 10);
          return this.chartDefinitionService.getWorkbenchData(id);
        }),
      )
      .subscribe((workbenchData: ChartWorkbenchData) => {
        this.workbenchData$.next(workbenchData);
      });
  }

  ngOnDestroy(): void {
    this.workbenchData$.complete();
  }
}
