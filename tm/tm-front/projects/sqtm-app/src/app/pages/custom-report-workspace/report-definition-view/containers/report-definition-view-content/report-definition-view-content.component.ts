import { ChangeDetectionStrategy, Component, OnDestroy, ViewContainerRef } from '@angular/core';
import { Observable } from 'rxjs';
import {
  ReportDefinitionViewComponentData,
  ReportDefinitionViewService,
} from '../../services/report-definition-view.service';
import { filter, map, take } from 'rxjs/operators';
import { DocxReportService } from '../../../../../components/report-workbench/services/docx-report.service';
import { DialogService, getSupportedBrowserLang, Report } from 'sqtm-core';
import { JasperReportDialogConfiguration } from '../../../../../components/report-workbench/containers/jasper-report-dialog/jasper-report-dialog.configuration';
import { JasperReportDialogComponent } from '../../../../../components/report-workbench/containers/jasper-report-dialog/jasper-report-dialog.component';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { DatePipe } from '@angular/common';
import { DirectDownloadableReportService } from '../../../../../components/report-workbench/services/direct-downloadable-report.service';

interface ReportDefinitionAttribute {
  name: string;
  value: string;
}

@Component({
  selector: 'sqtm-app-report-definition-view-content',
  templateUrl: './report-definition-view-content.component.html',
  styleUrls: ['./report-definition-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DocxReportService, DatePipe, DirectDownloadableReportService],
})
export class ReportDefinitionViewContentComponent implements OnDestroy {
  componentData$: Observable<ReportDefinitionViewComponentData>;
  formattedAttributes$: Observable<ReportDefinitionAttribute[]>;
  showSpinner$: Observable<boolean>;

  constructor(
    private reportDefinitionViewService: ReportDefinitionViewService,
    private docxReportService: DocxReportService,
    private dialogService: DialogService,
    private vcr: ViewContainerRef,
    private router: Router,
    private translateService: TranslateService,
    private datePipe: DatePipe,
    private directDownloadableReportService: DirectDownloadableReportService,
  ) {
    this.showSpinner$ = this.docxReportService.generatingReport$;
    this.componentData$ = this.reportDefinitionViewService.componentData$;
    this.formattedAttributes$ = this.componentData$.pipe(
      map((componentData) => componentData.reportDefinition.attributes),
      map((attributes) => {
        return Object.entries<string[]>(attributes).reduce((acc, entry) => {
          return acc.concat({ name: entry[0], value: entry[1] });
        }, []);
      }),
    );
  }

  ngOnDestroy(): void {
    this.docxReportService.complete();
  }

  downloadReport($event: MouseEvent) {
    $event.stopPropagation();
    this.componentData$
      .pipe(
        take(1),
        filter((componentData) => !componentData.reportDefinition.missingPlugin),
      )
      .subscribe((componentData: ReportDefinitionViewComponentData) => {
        if (componentData.reportDefinition.report.isDirectDownloadableReport) {
          this.directDownloadableReportService.directDownloadReport(
            componentData.reportDefinition.report,
            JSON.parse(componentData.reportDefinition.parameters),
          );
        } else if (componentData.reportDefinition.report.isDocxTemplate) {
          this.docxReportService.generateReport(
            componentData.reportDefinition.id,
            componentData.reportDefinition.report,
            { json: componentData.reportDefinition.parameters },
          );
        } else {
          this.openJasperReportDialog(
            componentData.reportDefinition.report,
            componentData.reportDefinition.parameters,
          );
        }
      });
  }

  modifyReport($event: MouseEvent, customReportLibraryNodeId: number) {
    $event.stopPropagation();
    this.router.navigate([
      'custom-report-workspace',
      'modify-report-definition',
      customReportLibraryNodeId,
    ]);
  }

  private openJasperReportDialog(report: Report, params: any) {
    const configuration: JasperReportDialogConfiguration = { report, params: JSON.parse(params) };

    this.dialogService.openDialog<JasperReportDialogConfiguration, void>({
      id: 'jasper-report-dialog',
      component: JasperReportDialogComponent,
      viewContainerReference: this.vcr,
      width: 1230,
      height: 768,
      data: configuration,
    });
  }

  getAuditableText(date: Date, userLogin: string) {
    if (date != null) {
      const formattedDate = this.formatDate(date);
      return `${formattedDate} (${userLogin})`;
    } else {
      return this.translateService.instant('sqtm-core.generic.label.never');
    }
  }

  private formatDate(date: Date): string {
    return this.datePipe.transform(date, 'short', getSupportedBrowserLang(this.translateService));
  }
}
