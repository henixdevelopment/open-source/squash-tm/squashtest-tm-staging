import { ChangeDetectionStrategy, Component, OnInit, Optional } from '@angular/core';
import {
  EntityRowReference,
  EntityViewService,
  GenericEntityViewService,
  ReferentialDataService,
  SquashTmDataRowType,
  WorkspaceWithTreeComponent,
} from 'sqtm-core';
import {
  ReportDefinitionViewComponentData,
  ReportDefinitionViewService,
} from '../../services/report-definition-view.service';
import { filter, map, take, takeUntil, withLatestFrom } from 'rxjs/operators';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'sqtm-app-report-definition-view',
  templateUrl: './report-definition-view.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: ReportDefinitionViewService,
      useClass: ReportDefinitionViewService,
    },
    {
      provide: EntityViewService,
      useExisting: ReportDefinitionViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: ReportDefinitionViewService,
    },
  ],
})
export class ReportDefinitionViewComponent implements OnInit {
  private unsub$ = new Subject<void>();
  componentData$: Observable<ReportDefinitionViewComponentData>;

  constructor(
    private referentialDataService: ReferentialDataService,
    private reportDefinitionViewService: ReportDefinitionViewService,
    private route: ActivatedRoute,
    @Optional() private workspaceWithTree: WorkspaceWithTreeComponent,
  ) {
    this.componentData$ = this.reportDefinitionViewService.componentData$;
  }

  ngOnInit(): void {
    this.referentialDataService.loaded$
      .pipe(
        takeUntil(this.unsub$),
        filter((loaded) => loaded),
        take(1),
      )
      .subscribe(() => {
        this.loadData();
      });
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('customReportLibraryNodeId')),
      )
      .subscribe((id) => {
        this.reportDefinitionViewService.load(parseInt(id, 10));
        this.initializeTreeSynchronization();
      });
  }

  private initializeTreeSynchronization() {
    if (this.workspaceWithTree) {
      this.reportDefinitionViewService.simpleAttributeRequiringRefresh = ['name'];
      this.reportDefinitionViewService.externalRefreshRequired$
        .pipe(takeUntil(this.unsub$), withLatestFrom(this.componentData$))
        .subscribe(([, componentData]) => {
          const customReportLibraryNodeId =
            componentData.reportDefinition.customReportLibraryNodeId;
          const entityRowReference = new EntityRowReference(
            customReportLibraryNodeId,
            SquashTmDataRowType.ReportDefinition,
          );
          this.workspaceWithTree.requireNodeRefresh([entityRowReference.asString()]);
        });
    }
  }
}
