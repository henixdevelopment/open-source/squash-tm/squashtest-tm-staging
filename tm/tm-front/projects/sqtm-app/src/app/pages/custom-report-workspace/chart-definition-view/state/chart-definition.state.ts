import { ChartDefinitionModel, SqtmEntityState } from 'sqtm-core';

export interface ChartDefinitionState extends SqtmEntityState, ChartDefinitionModel {
  generatedOn?: Date;
}
