import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  Optional,
  ViewChild,
} from '@angular/core';
import {
  EntityViewService,
  GenericEntityViewService,
  ReferentialDataService,
  WorkspaceWithTreeComponent,
} from 'sqtm-core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { filter, map, take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { CustomReportLibraryViewService } from '../../service/custom-report-library-view.service';

@Component({
  selector: 'sqtm-app-custom-report-library-view',
  templateUrl: './custom-report-library-view.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: CustomReportLibraryViewService,
      useClass: CustomReportLibraryViewService,
    },
    {
      provide: EntityViewService,
      useExisting: CustomReportLibraryViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: CustomReportLibraryViewService,
    },
  ],
})
export class CustomReportLibraryViewComponent implements OnInit, OnDestroy {
  private unsub$ = new Subject<void>();

  @ViewChild('content', { read: ElementRef })
  content: ElementRef;

  constructor(
    private referentialDataService: ReferentialDataService,
    private route: ActivatedRoute,
    public readonly customReportLibraryViewService: CustomReportLibraryViewService,
    @Optional() private workspaceWithTree: WorkspaceWithTreeComponent,
  ) {}

  ngOnInit(): void {
    this.referentialDataService.loaded$
      .pipe(
        takeUntil(this.unsub$),
        filter((loaded) => loaded),
        take(1),
      )
      .subscribe(() => {
        this.loadData();
      });
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('customReportLibraryNodeId')),
      )
      .subscribe((id) => {
        this.customReportLibraryViewService.load(parseInt(id, 10));
      });
  }

  ngOnDestroy(): void {
    this.customReportLibraryViewService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }
}
