import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { CustomExportViewService } from '../../services/custom-export-view.service';
import { Router } from '@angular/router';
import { map, take } from 'rxjs/operators';
import { DialogService } from 'sqtm-core';
import {
  CustomExportInformationPanelComponent,
  ExecutionPrintOptions,
} from '../../components/custom-export-information-panel/custom-export-information-panel.component';

@Component({
  selector: 'sqtm-app-custom-export-view-content',
  templateUrl: './custom-export-view-content.component.html',
  styleUrls: ['./custom-export-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomExportViewContentComponent {
  @ViewChild(CustomExportInformationPanelComponent, { static: false })
  informationPanel: CustomExportInformationPanelComponent;

  constructor(
    public readonly customExportViewService: CustomExportViewService,
    public readonly router: Router,
    private dialogService: DialogService,
  ) {}

  handleModify(): void {
    this.customExportViewService.componentData$
      .pipe(
        take(1),
        map((componentData) => componentData.customExport),
      )
      .subscribe((customExport) =>
        this.router.navigate([
          'custom-report-workspace',
          'create-custom-export',
          customExport.customReportLibraryNodeId,
        ]),
      );
  }

  openAlertForDeletedScope($event: MouseEvent) {
    $event.stopPropagation();
    this.dialogService.openAlert({
      id: 'deleted-scope-alert',
      messageKey: 'sqtm-core.custom-report-workspace.custom-export.scope.deleted',
      level: 'DANGER',
    });
  }

  downloadReport() {
    if (this.informationPanel) {
      const printExecutionOptionValue =
        this.informationPanel.formGroup.get('printExecutionOption').value;
      const printOnlyLastExec =
        printExecutionOptionValue === ExecutionPrintOptions.PRINT_LAST_EXECUTION;
      this.customExportViewService.downloadReport(printOnlyLastExec);
    } else {
      this.customExportViewService.downloadReport(false);
    }
  }
}
