import {
  ChangeDetectionStrategy,
  Component,
  NgZone,
  OnDestroy,
  OnInit,
  ViewContainerRef,
} from '@angular/core';
import {
  BindableEntity,
  CreateEntityDialogComponent,
  CreationDialogConfiguration,
  DataRow,
  DialogService,
  EntityCreationDialogData,
  EntityRowReference,
  GridPersistenceService,
  GridService,
  ReferentialDataService,
  RestService,
  SquashTmDataRowType,
  toEntityRowReference,
  TreeWithStatePersistence,
} from 'sqtm-core';
import { CR_WS_TREE, customReportWorkspaceTreeId } from '../../../custom-report-workspace.constant';
import { Router } from '@angular/router';
import { filter, map, take, takeUntil } from 'rxjs/operators';
import { customReportWorkspaceLogger } from '../../../custom-report-workspace.logger';
import { Observable } from 'rxjs';

const logger = customReportWorkspaceLogger.compose('CustomReportWorkspaceTreeComponent');

@Component({
  selector: 'sqtm-app-custom-report-workspace-tree',
  templateUrl: './custom-report-workspace-tree.component.html',
  styleUrls: ['./custom-report-workspace-tree.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: CR_WS_TREE,
    },
  ],
})
export class CustomReportWorkspaceTreeComponent
  extends TreeWithStatePersistence
  implements OnInit, OnDestroy
{
  public createMenuPermissions$: Observable<CustomReportTreeCreationPermission>;
  public canCreateSomething$: Observable<boolean>;
  canBeCollapsed$: Observable<boolean>;

  constructor(
    protected restService: RestService,
    public tree: GridService,
    public referentialDataService: ReferentialDataService,
    protected dialogService: DialogService,
    protected vcr: ViewContainerRef,
    protected gridPersistenceService: GridPersistenceService,
    protected router: Router,
    protected ngZone: NgZone,
  ) {
    super(
      tree,
      referentialDataService,
      gridPersistenceService,
      restService,
      router,
      dialogService,
      vcr,
      customReportWorkspaceTreeId,
      'custom-report-tree',
    );
  }

  ngOnInit(): void {
    this.initEntityIdFromInitialUrl();
    this.initData();
    this.registerStatePersistence();
    this.initTruncateNameMenu();
    this.initCanCollapse();

    this.createMenuPermissions$ = this.tree.selectedRows$.pipe(
      takeUntil(this.unsub$),
      map((rows) => {
        let menu: CustomReportTreeCreationPermission = {
          ...readOnly,
        };
        if (rows.length === 1) {
          const row = rows[0];
          const canCreate = row.simplePermissions.canCreate;
          menu = {
            canCreateFolder:
              canCreate && row.allowedChildren.includes(SquashTmDataRowType.CustomReportFolder),
            canCreateChart: canCreate,
            canCreateDashboard: canCreate,
            canCreateReport: canCreate,
            canCreateCustomExport: canCreate,
          };
        }
        return menu;
      }),
    );

    this.canCreateSomething$ = this.createMenuPermissions$.pipe(
      map((perms) => Object.values(perms).find((v) => v)),
    );
  }

  ngOnDestroy(): void {
    this.unregisterStatePersistence();
    this.unsub$.next();
    this.unsub$.complete();
  }

  addDashboard(canCreate: boolean): void {
    if (!canCreate) {
      return;
    }

    this.tree.selectedRows$.pipe(take(1)).subscribe((dataRows: DataRow[]) => {
      // The creation menu item is enabled if there's only 1 selected element, so we can safely take the first one
      const selectedDataRow = dataRows[0];
      logger.debug(`DataRow: ${JSON.stringify(selectedDataRow)}`);
      const isContainer = this.isContainer(selectedDataRow);

      const configuration: CreationDialogConfiguration<EntityCreationDialogData> = {
        viewContainerReference: this.vcr,
        formComponent: CreateEntityDialogComponent,
        data: {
          titleKey: 'sqtm-core.custom-report-workspace.tree.button.new-dashboard',
          id: 'add-dashboard',
          projectId: selectedDataRow.projectId,
          parentEntityReference: isContainer
            ? selectedDataRow.id.toString()
            : selectedDataRow.parentRowId.toString(),
          postUrl: 'custom-report-tree/new-dashboard',
          hideDescriptionField: true,
        },
      };
      this.dialogService.openEntityCreationDialog<EntityCreationDialogData, DataRow>(
        configuration,
        this.tree,
      );
    });
  }

  private isContainer(selectedDataRow) {
    return [
      SquashTmDataRowType.CustomReportFolder,
      SquashTmDataRowType.CustomReportLibrary,
    ].includes(selectedDataRow.type);
  }

  openCreateFolder(canCreate: boolean): void {
    if (!canCreate) {
      return;
    }

    this.tree.selectedRows$.pipe(take(1)).subscribe((dataRows: DataRow[]) => {
      // The creation menu item is enabled if there's only 1 selected element, so we can safely take the first one
      const selectedDataRow = dataRows[0];

      const configuration: CreationDialogConfiguration<EntityCreationDialogData> = {
        viewContainerReference: this.vcr,
        formComponent: CreateEntityDialogComponent,
        data: {
          titleKey: 'sqtm-core.test-case-workspace.dialog.title.new-folder',
          id: 'add-custom-report-folder',
          projectId: selectedDataRow.projectId,
          bindableEntity: BindableEntity.CUSTOM_REPORT_FOLDER,
          parentEntityReference: selectedDataRow.id.toString(),
          postUrl: 'custom-report-tree/new-folder',
        },
      };
      this.dialogService.openEntityCreationDialog<EntityCreationDialogData, DataRow>(
        configuration,
        this.tree,
      );
    });
  }

  copy() {
    this.tree.copy();
  }

  paste() {
    this.tree.paste();
  }

  delete() {
    this.tree.deleteRows();
  }

  navigateToCreateChartView(canCreate: boolean): void {
    if (!canCreate) {
      return;
    }

    this.getFirstSelectedRowAsEntityRowReference().subscribe((entityReference) => {
      this.ngZone.run(() =>
        this.router.navigate([
          'custom-report-workspace',
          'create-chart-definition',
          entityReference.id.toString(),
        ]),
      );
    });
  }

  navigateToCreateReportView(canCreate: boolean): void {
    if (!canCreate) {
      return;
    }

    this.getFirstSelectedRowAsEntityRowReference().subscribe((entityReference) => {
      this.ngZone.run(() =>
        this.router.navigate([
          'custom-report-workspace',
          'create-report-definition',
          entityReference.id.toString(),
        ]),
      );
    });
  }

  navigateToCreateCustomExportView(canCreate: boolean): void {
    if (!canCreate) {
      return;
    }

    this.getFirstSelectedRowAsEntityRowReference().subscribe((entityReference) => {
      this.ngZone.run(() =>
        this.router.navigate([
          'custom-report-workspace',
          'create-custom-export',
          entityReference.id.toString(),
        ]),
      );
    });
  }

  private getFirstSelectedRowAsEntityRowReference(): Observable<EntityRowReference> {
    return this.tree.selectedRows$.pipe(
      take(1),
      filter((rows) => rows.length === 1),
      map((rows) => rows[0]),
      map((row) => {
        if (this.isContainer(row)) {
          return row.id;
        } else {
          return row.parentRowId;
        }
      }),
      map((rowId) => toEntityRowReference(rowId)),
    );
  }

  private initCanCollapse() {
    this.canBeCollapsed$ = this.tree.canBeCollapsed$;
  }

  collapseTree() {
    this.tree.closeAllRows();
  }
}

export interface CustomReportTreeCreationPermission {
  canCreateFolder: boolean;
  canCreateChart: boolean;
  canCreateDashboard: boolean;
  canCreateReport: boolean;
  canCreateCustomExport: boolean;
}

const readOnly: CustomReportTreeCreationPermission = {
  canCreateFolder: false,
  canCreateChart: false,
  canCreateDashboard: false,
  canCreateReport: false,
  canCreateCustomExport: false,
};
