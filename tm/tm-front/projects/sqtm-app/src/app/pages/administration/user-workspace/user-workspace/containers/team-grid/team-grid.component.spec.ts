import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { adminTeamTableDefinition, TeamGridComponent } from './team-grid.component';
import { RouterTestingModule } from '@angular/router/testing';
import {
  DataRow,
  DialogModule,
  DialogService,
  GRID_PERSISTENCE_KEY,
  GridService,
  GridTestingModule,
  GridWithStatePersistence,
  RestService,
  WorkspaceWithGridComponent,
} from 'sqtm-core';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ADMIN_WS_TEAM_TABLE_CONFIG } from '../../../user-workspace.constant';
import { AppTestingUtilsModule } from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {
  mockClosableDialogService,
  mockGridService,
  mockRestService,
  mockRouter,
} from '../../../../../../utils/testing-utils/mocks.service';
import { of } from 'rxjs';
import { mockMouseEvent } from '../../../../../../utils/testing-utils/test-component-generator';
import { ActivatedRoute, Router } from '@angular/router';
import createSpyObj = jasmine.createSpyObj;

describe('TeamGridComponent', () => {
  let component: TeamGridComponent;
  let fixture: ComponentFixture<TeamGridComponent>;

  const gridService = mockGridService();
  const restService = mockRestService();
  const dialogMock = mockClosableDialogService();
  const dialogService = dialogMock.service;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TeamGridComponent],
      imports: [RouterTestingModule, AppTestingUtilsModule, GridTestingModule, DialogModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: ADMIN_WS_TEAM_TABLE_CONFIG,
          useFactory: adminTeamTableDefinition,
        },
        {
          provide: GridService,
          useValue: gridService,
        },
        {
          provide: DialogService,
          useValue: dialogService,
        },
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: Router,
          useValue: mockRouter(),
        },
        {
          provide: WorkspaceWithGridComponent,
          useValue: {},
        },
        {
          provide: GRID_PERSISTENCE_KEY,
          useValue: 'team-workspace-main-grid',
        },
        {
          provide: ActivatedRoute,
          useValue: { snapshot: { paramMap: createSpyObj<Map<any, any>>(['get']) } },
        },
        GridWithStatePersistence,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    gridService.refreshData.calls.reset();
    dialogMock.resetSubjects();
    dialogMock.resetCalls();
    restService.delete.and.returnValue(of({}));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open creation dialog', () => {
    component.openTeamDialog();
    dialogMock.closeDialogsWithResult(true);

    expect(dialogService.openDialog).toHaveBeenCalled();
    expect(gridService.refreshDataAsync).toHaveBeenCalled();
  });

  it('should open creation dialog', () => {
    gridService.selectedRows$ = of([{ data: {} } as DataRow]);

    component.deleteTeams(mockMouseEvent());
    dialogMock.closeDialogsWithResult(true);

    expect(dialogService.openDeletionConfirm).toHaveBeenCalled();
    expect(restService.delete).toHaveBeenCalled();
    expect(gridService.refreshData).toHaveBeenCalled();
  });
});
