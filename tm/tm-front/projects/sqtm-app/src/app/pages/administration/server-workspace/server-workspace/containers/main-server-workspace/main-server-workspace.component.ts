import { ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { AdminReferentialDataService, AuthenticatedUser } from 'sqtm-core';
import { Observable, Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'sqtm-app-main-server-workspace',
  templateUrl: './main-server-workspace.component.html',
  styleUrls: ['./main-server-workspace.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MainServerWorkspaceComponent implements OnInit, OnDestroy {
  @Input()
  baseUrl: string;

  authenticatedAdmin$: Observable<AuthenticatedUser>;

  private unsub$ = new Subject<void>();

  constructor(
    private adminReferentialDataService: AdminReferentialDataService,
    private router: Router,
  ) {}

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  ngOnInit(): void {
    this.adminReferentialDataService.refresh().subscribe();

    this.authenticatedAdmin$ = this.adminReferentialDataService.authenticatedUser$.pipe(
      takeUntil(this.unsub$),
      filter((authUser: AuthenticatedUser) => authUser.admin),
    );

    this.adminReferentialDataService.authenticatedUser$
      .pipe(
        takeUntil(this.unsub$),
        filter((authUser: AuthenticatedUser) => !authUser.admin),
      )
      .subscribe(() => this.router.navigate(['home-workspace']));
  }
}
