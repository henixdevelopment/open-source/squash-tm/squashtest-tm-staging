import { ChangeDetectionStrategy, Component, Input, OnInit, ViewChild } from '@angular/core';
import { AuthConfiguration, AuthenticationProtocol } from 'sqtm-core';
import { AdminBugTrackerViewState } from '../../../states/admin-bug-tracker-view-state';
import { BugTrackerViewService } from '../../../services/bug-tracker-view.service';
import { TranslateService } from '@ngx-translate/core';
import {
  AuthProtocolFormComponent,
  AuthProtocolFormData,
  DefaultOAuth2FormValues,
} from '../../../../components/auth-protocol-form/auth-protocol-form.component';
import { Observable, of, switchMap } from 'rxjs';
import { map, take, tap, withLatestFrom } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-bug-tracker-authentication-protocol-panel',
  templateUrl: './bug-tracker-auth-protocol-panel.component.html',
  styleUrls: ['./bug-tracker-auth-protocol-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BugTrackerAuthProtocolPanelComponent implements OnInit {
  @Input()
  componentData: AdminBugTrackerViewState;

  @ViewChild(AuthProtocolFormComponent)
  authProtocolForm: AuthProtocolFormComponent;

  authProtocolFormData$: Observable<AuthProtocolFormData>;

  constructor(
    public readonly bugTrackerViewService: BugTrackerViewService,
    public readonly translateService: TranslateService,
  ) {}

  ngOnInit(): void {
    this.authProtocolFormData$ = this.getAuthProtocolFormData();
  }

  handleConfigurationChange(authConf: AuthConfiguration): void {
    this.bugTrackerViewService.setAuthenticationConfiguration(authConf).subscribe({
      next: () => this.authProtocolForm.handleServerSuccess(),
      error: (err) => {
        console.error(err);
        this.authProtocolForm.handleServerError();
      },
    });
  }

  handleProtocolChange(protocol: AuthenticationProtocol): void {
    const defaultUrl = this.componentData.bugTracker.url;

    if (this.isOAuth2(protocol) && !this.hasConfiguration()) {
      this.bugTrackerViewService
        .setAuthenticationProtocol(protocol)
        .pipe(
          take(1),
          switchMap(() => this.getDefaultOAuth2FormData(protocol, defaultUrl)),
        )
        .subscribe((oauth2FormData: AuthProtocolFormData) => {
          this.authProtocolForm.authConfigurationFormData = oauth2FormData;
        });
    } else {
      this.bugTrackerViewService
        .setAuthenticationProtocol(protocol)
        .pipe(take(1))
        .subscribe(
          () =>
            (this.authProtocolForm.authConfigurationFormData =
              this.getDefaultAuthFormData(protocol)),
        );
    }
  }

  getAuthProtocolFormData(): Observable<AuthProtocolFormData> {
    const protocol: AuthenticationProtocol = this.componentData.bugTracker.authProtocol;
    const defaultUrl = this.componentData.bugTracker.url;

    if (protocol === AuthenticationProtocol.OAUTH_2 && !this.hasConfiguration()) {
      return this.getDefaultOAuth2FormData(protocol, defaultUrl);
    } else {
      return of(this.getDefaultAuthFormData(protocol));
    }
  }

  private getDefaultOAuth2FormData(
    protocol: AuthenticationProtocol,
    defaultUrl: string,
  ): Observable<AuthProtocolFormData> {
    return this.bugTrackerViewService
      .getDefaultOAuth2FormValues(this.componentData.bugTracker.id)
      .pipe(
        take(1),
        map((defaultOAuth2FormValues: DefaultOAuth2FormValues) => {
          return {
            authenticationProtocol: protocol,
            authConfiguration: this.componentData.bugTracker.authConfiguration,
            defaultUrl,
            defaultOAuth2FormValues: defaultOAuth2FormValues,
          };
        }),
      );
  }

  private getDefaultAuthFormData(protocol: AuthenticationProtocol): AuthProtocolFormData {
    return {
      authenticationProtocol: protocol,
      authConfiguration: this.componentData.bugTracker.authConfiguration,
      defaultUrl: this.componentData.bugTracker.url,
    };
  }

  private hasConfiguration() {
    return Boolean(this.componentData.bugTracker.authConfiguration);
  }

  handleBugTrackerKindChange(bugTrackerKind: string) {
    const defaultUrl = this.componentData.bugTracker.url;

    this.bugTrackerViewService
      .changeBugTrackerKind(bugTrackerKind)
      .pipe(take(1), withLatestFrom(this.bugTrackerViewService.componentData$))
      .subscribe(([, componentData]) => {
        const protocol = componentData.bugTracker.authProtocol;

        if (this.isOAuth2(protocol) && !this.hasConfiguration()) {
          this.getDefaultOAuth2FormData(protocol, defaultUrl)
            .pipe(take(1))
            .subscribe((oauth2FormData: AuthProtocolFormData) => {
              this.authProtocolForm.authConfigurationFormData = oauth2FormData;
            });
        } else {
          this.authProtocolForm.authConfigurationFormData = this.getDefaultAuthFormData(protocol);
        }
      });
  }

  handleUrlChanged(bugTrackerUrl: string) {
    const protocol: AuthenticationProtocol = this.componentData.bugTracker.authProtocol;

    if (this.isOAuth2(protocol) && !this.hasConfiguration()) {
      this.getDefaultOAuth2FormData(protocol, bugTrackerUrl)
        .pipe(
          tap((oauth2FormData: AuthProtocolFormData) => {
            this.authProtocolForm.authConfigurationFormData = oauth2FormData;
          }),
        )
        .subscribe();
    }
  }

  isOAuth2(protocol: AuthenticationProtocol) {
    return protocol === AuthenticationProtocol.OAUTH_2;
  }
}
