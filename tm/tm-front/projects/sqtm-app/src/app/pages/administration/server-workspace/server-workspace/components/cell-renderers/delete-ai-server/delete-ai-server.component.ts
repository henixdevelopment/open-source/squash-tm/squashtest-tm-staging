import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  DataRow,
  DialogService,
  Fixed,
  GridColumnId,
  GridService,
  RestService,
} from 'sqtm-core';
import { Subject } from 'rxjs';
import { concatMap, filter, takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-delete-ai-server',
  template: `
    @if (row) {
      <div
        class="full-height full-width flex-column icon-container current-workspace-main-color"
        (click)="removeItem(row)"
      >
        <i nz-icon [nzType]="getIcon()" nzTheme="outline" class="table-icon-size"></i>
      </div>
    }
  `,
  styleUrl: './delete-ai-server.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteAiServerComponent extends AbstractCellRendererComponent implements OnDestroy {
  unsub$ = new Subject<void>();

  constructor(
    grid: GridService,
    cdr: ChangeDetectorRef,
    private dialogService: DialogService,
    private restService: RestService,
    private translateService: TranslateService,
  ) {
    super(grid, cdr);
  }

  getIcon(): string {
    return 'sqtm-core-generic:delete';
  }
  removeItem(row: DataRow) {
    const serverName: string = row.data.name;
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.servers.ai.dialog.title.delete-one-title',
      messageKey: this.translateService.instant(
        'sqtm-core.administration-workspace.servers.ai.dialog.delete-one',
        { serverName: serverName },
      ),
      level: 'DANGER',
    });
    dialogReference.dialogClosed$
      .pipe(
        takeUntil(this.unsub$),
        filter((result) => result === true),
        concatMap(() => this.restService.delete([`ai-servers/${row.data[GridColumnId.serverId]}`])),
      )
      .subscribe(() => {
        this.grid.refreshData();
      });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
export function deleteAiServerColumn(
  id: GridColumnId,
  label: string = '',
): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(DeleteAiServerComponent)
    .withLabel(label)
    .disableSort()
    .changeWidthCalculationStrategy(new Fixed(50));
}
