import {
  AuthenticationPolicy,
  AuthenticationProtocol,
  Credentials,
  GenericEntityViewState,
  provideInitialGenericViewState,
  SqtmGenericEntityState,
} from 'sqtm-core';

export interface AdminAiServerState extends SqtmGenericEntityState {
  id: number;
  createdBy: string;
  createdOn: Date;
  lastModifiedBy: string;
  lastModifiedOn: Date;
  name: string;
  url: string;
  authPolicy: AuthenticationPolicy;
  authProtocol: AuthenticationProtocol;
  description: string;
  payloadTemplate: string;
  credentials?: Credentials;
  jsonPath: string;
}

export interface AdminAiServerViewState
  extends GenericEntityViewState<AdminAiServerState, 'aiServer'> {
  aiServer: AdminAiServerState;
}

export function provideInitialAdminAiServerView(): Readonly<AdminAiServerViewState> {
  return provideInitialGenericViewState<AdminAiServerState, 'aiServer'>('aiServer');
}
