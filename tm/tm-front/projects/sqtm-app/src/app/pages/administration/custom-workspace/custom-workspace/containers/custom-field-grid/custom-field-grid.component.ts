import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  ViewContainerRef,
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { concatMap, filter, map, take, takeUntil, tap } from 'rxjs/operators';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  booleanColumn,
  DataRow,
  deleteColumn,
  DialogService,
  Extendable,
  FilterOperation,
  Fixed,
  grid,
  GridColumnId,
  GridDefinition,
  GridService,
  indexColumn,
  RestService,
  selectableTextColumn,
  textColumn,
  WorkspaceWithGridComponent,
} from 'sqtm-core';
import { CustomFieldCreationDialogComponent } from '../../components/dialogs/custom-field-creation-dialog/custom-field-creation-dialog.component';
import { inputTypeColumn } from '../../components/cell-renderers/input-type-cell-renderer/input-type-cell-renderer.component';
import { DeleteCustomFieldComponent } from '../../components/cell-renderers/delete-custom-field/delete-custom-field.component';
import { AbstractAdministrationNavigation } from '../../../../components/abstract-administration-navigation';
import { ActivatedRoute, Router } from '@angular/router';

export function adminCustomFieldTableDefinition(): GridDefinition {
  return grid('customFields')
    .withColumns([
      indexColumn().changeWidthCalculationStrategy(new Fixed(60)).withViewport('leftViewport'),
      selectableTextColumn(GridColumnId.name)
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Extendable(180, 0.2)),
      textColumn(GridColumnId.label)
        .withI18nKey('sqtm-core.entity.generic.label')
        .changeWidthCalculationStrategy(new Extendable(100, 0.2)),
      textColumn(GridColumnId.code)
        .withI18nKey('sqtm-core.generic.label.code')
        .changeWidthCalculationStrategy(new Extendable(100, 0.1)),
      inputTypeColumn(GridColumnId.inputType)
        .withI18nKey('sqtm-core.entity.custom-field.input-type.label')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1)),
      booleanColumn(GridColumnId.optional)
        .withI18nKey('sqtm-core.entity.custom-field.optional.label')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1))
        .withHeaderPosition('center')
        .withContentPosition('center'),
      deleteColumn(DeleteCustomFieldComponent).withViewport('rightViewport'),
    ])
    .server()
    .withServerUrl(['custom-fields'])
    .disableRightToolBar()
    .withRowHeight(35)
    .enableMultipleColumnsFiltering([GridColumnId.name, GridColumnId.label])
    .build();
}

@Component({
  selector: 'sqtm-app-custom-field-grid',
  templateUrl: './custom-field-grid.component.html',
  styleUrls: ['./custom-field-grid.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomFieldGridComponent
  extends AbstractAdministrationNavigation
  implements AfterViewInit, OnDestroy
{
  authenticatedUser$: Observable<AuthenticatedUser>;

  unsub$ = new Subject<void>();

  protected readonly entityIdPositionInUrl = 3;

  constructor(
    public gridService: GridService,
    private restService: RestService,
    private dialogService: DialogService,
    private adminReferentialDataService: AdminReferentialDataService,
    private viewContainerRef: ViewContainerRef,
    private workspaceWithGrid: WorkspaceWithGridComponent,
    protected route: ActivatedRoute,
    protected router: Router,
  ) {
    super(route, router);
    this.workspaceWithGrid.entityIdPositionInUrl = this.entityIdPositionInUrl;
    this.authenticatedUser$ = adminReferentialDataService.authenticatedUser$;
  }

  ngAfterViewInit() {
    this.addFilters();
    this.gridService.refreshData();
  }

  private addFilters() {
    this.gridService.addFilters([
      {
        id: GridColumnId.name,
        active: false,
        initialValue: { kind: 'single-string-value', value: '' },
        tiedToPerimeter: false,
        operation: FilterOperation.LIKE,
      },
      {
        id: GridColumnId.label,
        active: false,
        initialValue: { kind: 'single-string-value', value: '' },
        tiedToPerimeter: false,
        operation: FilterOperation.LIKE,
      },
    ]);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  openCreateDialog() {
    const dialogReference = this.dialogService.openDialog({
      component: CustomFieldCreationDialogComponent,
      viewContainerReference: this.viewContainerRef,
      data: {
        titleKey: 'sqtm-core.administration-workspace.custom-fields.dialog.title.new-custom-field',
      },
      id: 'create-custom-field-dialog',
      width: 600,
    });

    dialogReference.dialogResultChanged$
      .pipe(
        takeUntil(dialogReference.dialogClosed$),
        filter((result) => result != null),
        concatMap((result: any) => this.gridService.refreshDataAsync().pipe(map(() => result.id))),
        tap((id: string) => super.navigateToNewEntity(id)),
      )
      .subscribe();
  }

  deleteCustomFields() {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        filter((rows: DataRow[]) => rows.length > 0),
        concatMap((rows: DataRow[]) => this.showConfirmDeleteCustomFieldDialog(rows)),
        filter(({ confirmDelete }) => confirmDelete),
        tap(() => this.gridService.beginAsyncOperation()),
        concatMap(({ rows }) => this.deleteCustomFieldsServerSide(rows)),
        tap(() => this.gridService.completeAsyncOperation()),
      )
      .subscribe(() => this.gridService.refreshData());
  }

  private showConfirmDeleteCustomFieldDialog(
    rows,
  ): Observable<{ confirmDelete: boolean; rows: string[] }> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.custom-fields.dialog.title.delete-many',
      messageKey: 'sqtm-core.administration-workspace.custom-fields.dialog.message.delete-many',
      level: 'DANGER',
    });
    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map((confirmDelete) => ({ confirmDelete, rows })),
    );
  }

  private deleteCustomFieldsServerSide(rows): Observable<void> {
    const pathVariable = rows.map((row) => row.data[GridColumnId.cfId]).join(',');
    return this.restService.delete([`custom-fields`, pathVariable]);
  }

  filterCustomFields($event: any) {
    this.gridService.applyMultiColumnsFilter($event);
  }
}
