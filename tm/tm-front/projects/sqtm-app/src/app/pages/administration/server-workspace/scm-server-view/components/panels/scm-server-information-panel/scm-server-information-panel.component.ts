import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { AdminScmServerViewState } from '../../../states/admin-scm-server-view-state';
import { getScmServerKindI18nKey, ScmServerKind } from 'sqtm-core';

@Component({
  selector: 'sqtm-app-scm-server-information-panel',
  templateUrl: './scm-server-information-panel.component.html',
  styleUrls: ['./scm-server-information-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScmServerInformationPanelComponent {
  @Input()
  componentData: AdminScmServerViewState;

  getScmServerKind(componentData: AdminScmServerViewState): string {
    return getScmServerKindI18nKey(componentData.scmServer.kind as ScmServerKind);
  }
}
