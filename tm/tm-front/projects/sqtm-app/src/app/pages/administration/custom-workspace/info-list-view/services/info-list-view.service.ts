import { Injectable } from '@angular/core';
import {
  AdminInfoList,
  AdminInfoListItem,
  AdminReferentialDataService,
  AttachmentService,
  AuthenticatedUser,
  DateFormatUtils,
  EntityViewAttachmentHelperService,
  GenericEntityViewService,
  Identifier,
  RestService,
} from 'sqtm-core';
import { AdminInfoListState } from '../states/admin-info-list-state';
import { TranslateService } from '@ngx-translate/core';
import {
  AdminInfoListViewState,
  provideInitialAdminInfoListView,
} from '../states/admin-info-list-view-state';
import { createFeatureSelector } from '@ngrx/store';
import { map, switchMap, take, tap, withLatestFrom } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export class InfoListViewService extends GenericEntityViewService<AdminInfoListState, 'infoList'> {
  constructor(
    protected restService: RestService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
    private adminReferentialDataService: AdminReferentialDataService,
  ) {
    super(restService, attachmentService, translateService, attachmentHelper);
  }

  public getInitialState(): AdminInfoListViewState {
    return provideInitialAdminInfoListView();
  }

  protected getRootUrl(_initialState?): string {
    return 'info-lists';
  }

  load(infoListId: number) {
    return this.restService
      .getWithoutErrorHandling<AdminInfoList>(['info-list-view', infoListId.toString()])
      .subscribe({
        next: (infoList) => this.initializeInfoList(infoList),
        error: (err) => this.notifyEntityNotFound(err),
      });
  }

  private initializeInfoList(infoList: AdminInfoList): void {
    const infoListState: AdminInfoListState = {
      ...infoList,
      createdOn: DateFormatUtils.createDateFromIsoString(infoList.createdOn),
      lastModifiedOn: DateFormatUtils.createDateFromIsoString(infoList.lastModifiedOn),
      attachmentList: { id: null, attachments: null },
    };
    this.initializeEntityState(infoListState);
  }

  changeItemCode(itemId: Identifier, code: string): Observable<any> {
    return this.changeItemProperty(itemId, 'code', code);
  }

  changeItemLabel(itemId: Identifier, label: string): Observable<any> {
    return this.changeItemProperty(itemId, 'label', label);
  }

  changeItemColour(itemId: Identifier, colour: string): Observable<any> {
    return this.changeItemProperty(itemId, 'colour', colour);
  }

  changeDefaultItem(itemId: Identifier): Observable<any> {
    const urlParts = ['info-list-items', itemId.toString(), 'default'];

    return this.restService.post(urlParts, {}).pipe(
      withLatestFrom(this.store.state$),
      map(([, state]) => ({
        ...state,
        infoList: {
          ...state.infoList,
          items: state.infoList.items.map((item: AdminInfoListItem) => ({
            ...item,
            isDefault: item.id.toString() === itemId.toString(),
          })),
        },
      })),
      tap((newState) => this.store.commit(newState)),
    );
  }

  changeItemIcon(itemId: Identifier, newIcon: string): Observable<any> {
    newIcon = stripIconNamespace(newIcon);
    return this.changeItemProperty(itemId, 'iconName', newIcon);
  }

  private changeItemProperty(
    itemId: Identifier,
    property: keyof AdminInfoListItem,
    value: string,
  ): Observable<any> {
    const requestBody = {};
    requestBody[property] = value;
    const urlParts = ['info-list-items', itemId.toString(), kebabCased(property)];

    return this.restService.post(urlParts, requestBody).pipe(
      withLatestFrom(this.store.state$),
      map(([, state]) => ({
        ...state,
        infoList: {
          ...state.infoList,
          items: state.infoList.items.map((item: AdminInfoListItem) => {
            if (item.id.toString() === itemId.toString()) {
              const updated: any = { ...item };
              updated[property] = value;
              return updated;
            } else {
              return item;
            }
          }),
        },
      })),
      tap((newState) => this.store.commit(newState)),
    );
  }

  addItem(itemToAdd: NewInfoListItem): Observable<any> {
    itemToAdd.iconName = stripIconNamespace(itemToAdd.iconName);

    return this.state$.pipe(
      take(1),
      switchMap((state: AdminInfoListViewState) => this.addItemServerSide(state, itemToAdd)),
      withLatestFrom(this.store.state$),
      map(([response, state]) => ({
        ...state,
        infoList: {
          ...state.infoList,
          items: response.items,
        },
      })),
      map((nextState) => this.store.commit(nextState)),
    );
  }

  private addItemServerSide(
    state: AdminInfoListViewState,
    itemToAdd: NewInfoListItem,
  ): Observable<AdminInfoList> {
    const urlParts = [this.getRootUrl(), state.infoList.id.toString(), 'items', 'new'];
    return this.restService.post<AdminInfoList>(urlParts, itemToAdd);
  }

  deleteInfoListItems(itemIds: number[]): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      switchMap((state: AdminInfoListViewState) =>
        this.deleteInfoListItemsServerSide(state, itemIds),
      ),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminInfoListViewState]) =>
        this.updateStateWithDeletedItems(state, itemIds),
      ),
      map((newState) => this.store.commit(newState)),
    );
  }

  private deleteInfoListItemsServerSide(
    adminInfoListViewState: AdminInfoListViewState,
    itemIds: number[],
  ) {
    const urlParts = [
      'info-list-items',
      adminInfoListViewState.infoList.id.toString(),
      'items',
      itemIds.join(','),
    ];
    return this.restService.delete(urlParts);
  }

  private updateStateWithDeletedItems(state: AdminInfoListViewState, itemIds: number[]) {
    const items = [...state.infoList.items];
    const filteredItems = items.filter((value) => !itemIds.includes(value.id));
    return {
      ...state,
      infoList: {
        ...state.infoList,
        items: filteredItems,
      },
    };
  }

  public changeOptionsPosition(optionIdsToMove: number[], position: number): Observable<void> {
    return this.state$.pipe(
      take(1),
      switchMap(({ infoList }) => {
        const infoListId = infoList.id;
        const urlParts = ['info-list-items', infoListId.toString(), 'items', 'positions'];
        const body = {
          infoListItemIds: optionIdsToMove,
          position: position,
        };
        return this.restService.post<AdminInfoList>(urlParts, body);
      }),
      withLatestFrom(this.store.state$),
      map(([infoList, state]) => ({
        ...state,
        infoList: {
          ...state.infoList,
          ...infoList,
          createdOn: DateFormatUtils.createDateFromIsoString(infoList.createdOn),
          lastModifiedOn: DateFormatUtils.createDateFromIsoString(infoList.lastModifiedOn),
        },
      })),
      map((nextState) => this.store.commit(nextState)),
    );
  }

  protected getCurrentUser(): Observable<AuthenticatedUser> {
    return this.adminReferentialDataService.authenticatedUser$;
  }
}

export interface NewInfoListItem {
  label: string;
  code: string;
  colour: string;
  iconName: string;
}

export const getInfoListViewState = createFeatureSelector<AdminInfoListState>('infoList');

function stripIconNamespace(iconId: string): string {
  if (!iconId || iconId.length === 0) {
    return iconId;
  }

  if (iconId.includes(':')) {
    const tokens = iconId.split(':');
    return tokens[tokens.length - 1];
  }

  return iconId;
}

function kebabCased(input: string): string {
  if (!input || input.length === 0) {
    return input;
  }

  const sep = '-';
  return (
    input[0].toLowerCase() +
    input.slice(1, input.length).replace(/[A-Z]/g, (letter) => `${sep}${letter.toLowerCase()}`)
  );
}
