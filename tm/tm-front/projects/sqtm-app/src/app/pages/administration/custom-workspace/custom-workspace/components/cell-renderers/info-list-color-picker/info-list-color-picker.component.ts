import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
} from 'sqtm-core';
import { InfoListOptionService } from '../../../services/info-list-option.service';

@Component({
  selector: 'sqtm-app-info-list-option-color-picker-cell',
  template: ` @if (row) {
    <div class="full-width full-height flex-column" style="justify-content: center;">
      <sqtm-core-color-picker-select-field [color]="color" (colorChanged)="changeColor($event)">
      </sqtm-core-color-picker-select-field>
    </div>
  }`,
  styleUrls: ['./info-list-color-picker.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InfoListColorPickerComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    private infoListOptionService: InfoListOptionService,
  ) {
    super(grid, cdRef);
  }

  get color(): string {
    return this.row.data[this.columnDisplay.id];
  }

  changeColor(newColor: string) {
    this.infoListOptionService.changeColor(this.row.data[GridColumnId.label], newColor);
  }
}

export function infoListOptionColorPickerColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(InfoListColorPickerComponent)
    .withHeaderPosition('center');
}
