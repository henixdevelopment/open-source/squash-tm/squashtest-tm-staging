import { ChangeDetectionStrategy, Component, Input, OnInit, ViewChild } from '@angular/core';
import { AdminTestAutomationServerViewComponentData } from '../../../containers/test-automation-server-view/test-automation-server-view.component';
import { AuthConfiguration, AuthenticationProtocol } from 'sqtm-core';
import { TestAutomationServerViewService } from '../../../services/test-automation-server-view.service';
import {
  AuthProtocolFormComponent,
  AuthProtocolFormData,
} from '../../../../components/auth-protocol-form/auth-protocol-form.component';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'sqtm-app-taserver-auth-protocol-panel',
  templateUrl: './taserver-auth-protocol-panel.component.html',
  styleUrls: ['./taserver-auth-protocol-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TAServerAuthProtocolPanelComponent implements OnInit {
  @Input()
  componentData: AdminTestAutomationServerViewComponentData;

  @ViewChild(AuthProtocolFormComponent)
  authProtocolForm: AuthProtocolFormComponent;

  // authConfiguration is only used for OAuth protocol. No TAServer use OAuth credentials so far
  authConfiguration: AuthConfiguration = null;

  authProtocolFormData$: Observable<AuthProtocolFormData>;

  constructor(public readonly taServerViewService: TestAutomationServerViewService) {}

  ngOnInit(): void {
    this.authProtocolFormData$ = this.getAuthProtocolFormData();
  }

  handleProtocolChange(protocol: AuthenticationProtocol): void {
    this.taServerViewService.setAuthenticationProtocol(protocol).subscribe(
      () =>
        (this.authProtocolForm.authConfigurationFormData = {
          authenticationProtocol: protocol,
          authConfiguration: this.authConfiguration,
          defaultUrl: this.componentData.testAutomationServer.baseUrl,
        }),
    );
  }

  getAuthProtocolFormData(): Observable<AuthProtocolFormData> {
    return of({
      authenticationProtocol: this.componentData.testAutomationServer.authProtocol,
      authConfiguration: this.authConfiguration,
      defaultUrl: this.componentData.testAutomationServer.baseUrl,
    });
  }
}
