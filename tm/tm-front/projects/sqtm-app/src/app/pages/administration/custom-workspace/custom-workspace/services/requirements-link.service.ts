import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { RestService } from 'sqtm-core';

@Injectable()
export class RequirementsLinkService {
  private _refresh: Subject<void> = new Subject<void>();
  refresh$: Observable<void>;

  constructor(private restService: RestService) {
    this.refresh$ = this._refresh.asObservable();
  }

  refreshData() {
    this._refresh.next();
  }

  updateRequirementLinks(requirementLinksData: {
    linkTypeId: string;
    role1: string;
    role1Code: string;
    role2: string;
    role2Code: string;
  }): Observable<any> {
    const requestBody = {
      role1: requirementLinksData.role1,
      role1Code: requirementLinksData.role1Code,
      role2: requirementLinksData.role2,
      role2Code: requirementLinksData.role2Code,
    };

    return this.restService.post(
      ['requirements-links', requirementLinksData.linkTypeId, 'edit'],
      requestBody,
    );
  }
}
