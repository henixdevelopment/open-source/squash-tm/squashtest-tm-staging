import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractDeleteCellRenderer,
  ConfirmDeleteLevel,
  DialogService,
  GridService,
  RestService,
} from 'sqtm-core';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-delete-custom-field-cell-renderer',
  template: ` <sqtm-core-delete-icon (delete)="showDeleteConfirm()"></sqtm-core-delete-icon> `,
  styleUrls: ['./delete-custom-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteCustomFieldComponent extends AbstractDeleteCellRenderer {
  constructor(
    public grid: GridService,
    cdr: ChangeDetectorRef,
    protected dialogService: DialogService,
    private restService: RestService,
  ) {
    super(grid, cdr, dialogService);
  }

  doDelete() {
    this.grid.beginAsyncOperation();
    const cfId = this.row.data.cfId;
    this.restService
      .delete([`custom-fields/${cfId}`])
      .pipe(finalize(() => this.grid.completeAsyncOperation()))
      .subscribe(() => this.grid.refreshData());
  }

  protected getTitleKey(): string {
    return 'sqtm-core.administration-workspace.custom-fields.dialog.title.delete-one';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.administration-workspace.custom-fields.dialog.message.delete-one';
  }

  protected getLevel(): ConfirmDeleteLevel {
    return 'DANGER';
  }
}
