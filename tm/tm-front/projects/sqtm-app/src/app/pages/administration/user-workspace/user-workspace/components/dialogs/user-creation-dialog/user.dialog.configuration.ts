import { DisplayOption } from 'sqtm-core';

export interface UserDialogConfiguration {
  titleKey: string;
  usersGroups: DisplayOption[];
  canManageLocalPassword: boolean;
}
