import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ImportFromXrayState } from '../../state/import-from-xray.state';

@Component({
  selector: 'sqtm-app-import-from-xray-import',
  templateUrl: './import-from-xray-import.component.html',
  styleUrl: './import-from-xray-import.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImportFromXrayImportComponent {
  @Input({ required: true }) componentData: ImportFromXrayState;
}
