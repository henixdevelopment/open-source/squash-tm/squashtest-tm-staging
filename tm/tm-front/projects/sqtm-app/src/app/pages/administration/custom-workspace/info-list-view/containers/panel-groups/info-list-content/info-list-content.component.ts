import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { AdminInfoListViewComponentData } from '../../info-list-view/info-list-view.component';
import { takeUntil } from 'rxjs/operators';
import { InfoListViewService } from '../../../services/info-list-view.service';
import { GridService, gridServiceFactory, ReferentialDataService, RestService } from 'sqtm-core';
import {
  INFO_LIST_ITEMS_TABLE,
  INFO_LIST_ITEMS_TABLE_CONF,
  InfoListItemsPanelComponent,
  infoListItemsTableDefinition,
} from '../../../components/panels/info-list-items-panel/info-list-items-panel.component';
import { InfoListOptionsServerOperationHandler } from '../../../services/info-list-options-server-operation-handler';

@Component({
  selector: 'sqtm-app-info-list-content',
  templateUrl: './info-list-content.component.html',
  styleUrls: ['./info-list-content.component.less'],
  providers: [
    {
      provide: INFO_LIST_ITEMS_TABLE_CONF,
      useFactory: infoListItemsTableDefinition,
    },
    InfoListOptionsServerOperationHandler,
    {
      provide: INFO_LIST_ITEMS_TABLE,
      useFactory: gridServiceFactory,
      deps: [
        RestService,
        INFO_LIST_ITEMS_TABLE_CONF,
        ReferentialDataService,
        InfoListOptionsServerOperationHandler,
      ],
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InfoListContentComponent implements OnInit, OnDestroy {
  componentData$: Observable<AdminInfoListViewComponentData>;
  unsub$ = new Subject<void>();

  @ViewChild(InfoListItemsPanelComponent)
  private infoListItemsPanelComponent;

  constructor(
    public infoListViewService: InfoListViewService,
    @Inject(INFO_LIST_ITEMS_TABLE) public infoListItemsGridService: GridService,
  ) {}

  ngOnInit(): void {
    this.componentData$ = this.infoListViewService.componentData$.pipe(takeUntil(this.unsub$));
  }

  ngOnDestroy(): void {
    this.infoListItemsGridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  addOptions($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.infoListItemsPanelComponent.openAddItemDialog();
  }

  deleteOptions($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.infoListItemsPanelComponent.removeInfoListItems();
  }
}
