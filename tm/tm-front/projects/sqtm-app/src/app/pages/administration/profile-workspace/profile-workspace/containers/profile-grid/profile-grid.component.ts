import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  Signal,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  deleteColumn,
  DialogService,
  Extendable,
  FilterOperation,
  Fixed,
  grid,
  GridColumnId,
  GridDefinition,
  GridService,
  indexColumn,
  selectableTextColumn,
  Sort,
  textColumn,
  TextResearchFieldComponent,
  WorkspaceWithGridComponent,
} from 'sqtm-core';
import { Subject } from 'rxjs';
import { AbstractAdministrationNavigation } from '../../../../components/abstract-administration-navigation';
import { ActivatedRoute, Router } from '@angular/router';
import { concatMap, filter, map, takeUntil, tap } from 'rxjs/operators';
import { profileTypeColumn } from '../../components/cell-renderers/profile-type-cell-renderer/profile-type-cell-renderer.component';
import { profileStateColumn } from '../../components/cell-renderers/profile-state-cell-renderer/profile-state-cell-renderer.component';
import { DeleteProfileCellRendererComponent } from '../../components/cell-renderers/delete-profile-cell-renderer/delete-profile-cell-renderer.component';
import { ProfileService } from '../../services/profile.service';
import { toSignal } from '@angular/core/rxjs-interop';
import { ProfileCreationDialogComponent } from '../../components/dialogs/profile-creation-dialog/profile-creation-dialog.component';

export function adminProfileTableDefinition(): GridDefinition {
  return grid('profiles')
    .withColumns([
      indexColumn().changeWidthCalculationStrategy(new Fixed(60)).withViewport('leftViewport'),
      selectableTextColumn(GridColumnId.name)
        .withI18nKey('sqtm-core.administration-workspace.views.project.permissions.profile.label')
        .changeWidthCalculationStrategy(new Extendable(180, 0.2)),
      textColumn(GridColumnId.partyCount)
        .withI18nKey('sqtm-core.entity.profile.party-count.short')
        .withTitleI18nKey('sqtm-core.entity.profile.party-count.full')
        .changeWidthCalculationStrategy(new Extendable(100, 0.2)),
      profileStateColumn(GridColumnId.active)
        .withI18nKey('sqtm-core.entity.user.state.label')
        .changeWidthCalculationStrategy(new Extendable(100, 0.1)),
      profileTypeColumn(GridColumnId.system)
        .withI18nKey('sqtm-core.administration-workspace.views.project.permissions.type')
        .changeWidthCalculationStrategy(new Extendable(100, 0.1)),
      deleteColumn(DeleteProfileCellRendererComponent).withViewport('rightViewport'),
    ])
    .disableRightToolBar()
    .withRowHeight(35)
    .enableMultipleColumnsFiltering([GridColumnId.name])
    .withInitialSortedColumns([{ id: GridColumnId.name, sort: Sort.ASC }])
    .build();
}

@Component({
  selector: 'sqtm-app-profile-grid',
  templateUrl: './profile-grid.component.html',
  styleUrls: ['./profile-grid.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileGridComponent
  extends AbstractAdministrationNavigation
  implements AfterViewInit, OnDestroy
{
  unsub$ = new Subject<void>();
  $authenticatedUser: Signal<AuthenticatedUser>;
  readonly $isUltimate: Signal<boolean>;
  protected readonly entityIdPositionInUrl = 3;

  @ViewChild(TextResearchFieldComponent)
  searchField: TextResearchFieldComponent;

  constructor(
    public gridService: GridService,
    protected route: ActivatedRoute,
    protected router: Router,
    private adminReferentialDataService: AdminReferentialDataService,
    private dialogService: DialogService,
    private viewContainerRef: ViewContainerRef,
    public workspaceWithGrid: WorkspaceWithGridComponent,
    private profileService: ProfileService,
  ) {
    super(route, router);
    this.$authenticatedUser = toSignal(adminReferentialDataService.authenticatedUser$);
    this.$isUltimate = toSignal(adminReferentialDataService.isUltimateLicenseAvailable$);
    this.workspaceWithGrid.entityIdPositionInUrl = this.entityIdPositionInUrl;
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  ngAfterViewInit() {
    this.initializeGrid();
  }

  private initializeGrid() {
    this.profileService.getDataRowProfiles().subscribe(() => {
      this.addFilters();
    });
  }

  private addFilters() {
    this.gridService.addFilters([
      {
        id: GridColumnId.name,
        active: false,
        initialValue: { kind: 'single-string-value', value: '' },
        tiedToPerimeter: false,
        operation: FilterOperation.LIKE,
      },
    ]);
  }

  openCustomProfileCreationDialog() {
    if (this.$isUltimate()) {
      const dialogReference = this.dialogService.openDialog({
        component: ProfileCreationDialogComponent,
        viewContainerReference: this.viewContainerRef,
        data: {
          titleKey: 'sqtm-core.administration-workspace.profiles.dialog.title.new-profile',
        },
        id: 'custom-profile-creation-dialog',
        width: 600,
      });

      dialogReference.dialogResultChanged$
        .pipe(
          takeUntil(dialogReference.dialogClosed$),
          filter((result) => result != null),
          concatMap((result: any) =>
            this.profileService.getDataRowProfiles(result.id).pipe(map(() => result.id)),
          ),
          tap((id: string) => {
            super.navigateToNewEntity(id);
          }),
        )
        .subscribe();
    } else {
      this.dialogService.openAlert({
        titleKey: 'sqtm-core.dialog.title.squash-ultimate-customers-only',
        messageKey:
          'sqtm-core.administration-workspace.profiles.dialog.message.new-profile.advertising',
        level: 'INFO',
      });
    }
  }

  filterProfiles($event: any) {
    this.gridService.applyMultiColumnsFilter($event);
  }
}
