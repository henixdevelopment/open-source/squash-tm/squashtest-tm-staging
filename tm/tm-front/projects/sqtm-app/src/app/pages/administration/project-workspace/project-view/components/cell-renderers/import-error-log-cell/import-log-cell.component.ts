import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewContainerRef,
} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  DialogService,
  FileViewerComponent,
  FileViewerDialogConfiguration,
  GridColumnId,
  GridService,
  PivotFormatImportStatus,
  FileViewerFilePathType,
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-import-log-cell',
  template: `
    @if (row.data[columnDisplay.id] && (hasFailureStatus() || hasWarningStatus())) {
      <sqtm-core-toggle-icon
        class="full-height full-width icon-container current-workspace-main-color"
      >
        <a (click)="openFilePreviewDialog($event)">
          <i
            class="action-icon-size"
            nz-icon
            nzType="sqtm-core-generic:file-alert"
            nzTheme="outline"
            nz-tooltip
            [nzTooltipTitle]="getToolTipMessage() | translate"
          >
          </i>
        </a>
      </sqtm-core-toggle-icon>
    }
  `,
  styleUrl: './import-log-cell.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImportLogCellComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    cdRef: ChangeDetectorRef,
    private vcr: ViewContainerRef,
    private dialogService: DialogService,
  ) {
    super(grid, cdRef);
  }

  hasFailureStatus() {
    return this.row.data[GridColumnId.status] === PivotFormatImportStatus.FAILURE;
  }

  hasWarningStatus() {
    return this.row.data[GridColumnId.status] === PivotFormatImportStatus.WARNING;
  }

  openFilePreviewDialog(event: any) {
    event.preventDefault();

    this.dialogService.openDialog<FileViewerDialogConfiguration, any>({
      id: 'project-import-file-viewer',
      component: FileViewerComponent,
      viewContainerReference: this.vcr,
      data: {
        filePath: this.row.data[GridColumnId.importLogFilePath],
        origin: FileViewerFilePathType.IMPORT,
      },
      minHeight: '100%',
      minWidth: '100%',
    });
  }

  getToolTipMessage() {
    return this.hasWarningStatus()
      ? 'sqtm-core.generic.label.download-report'
      : 'sqtm-core.generic.label.download-error-log';
  }
}

export function importLogColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(ImportLogCellComponent).disableHeader();
}
