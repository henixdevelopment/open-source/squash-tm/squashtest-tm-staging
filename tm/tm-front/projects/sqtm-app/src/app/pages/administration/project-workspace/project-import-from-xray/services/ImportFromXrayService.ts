import { Observable, of } from 'rxjs';
import { catchError, map, switchMap, take } from 'rxjs/operators';
import {
  createStore,
  doesHttpErrorContainsSquashActionError,
  extractSquashActionError,
  ImportType,
  ProjectImportService,
  ProjectImportXrayService,
  SquashActionError,
  Store,
  XrayImportModel,
  XrayInfoModel,
} from 'sqtm-core';
import {
  FileFormatItem,
  type ImportFromXrayState,
  ImportFromXraySteps,
  initializeImportFromXrayState,
  initialStateImportErrors,
  XrayFileFormat,
} from '../state/import-from-xray.state';
import { Injectable, Signal, signal } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { toSignal } from '@angular/core/rxjs-interop';

@Injectable()
export class ImportFromXrayService {
  store: Store<ImportFromXrayState>;
  readonly $state: Signal<ImportFromXrayState> = signal(null);

  constructor(
    private projectImportXrayService: ProjectImportXrayService,
    private projectImportService: ProjectImportService,
    private translate: TranslateService,
  ) {
    const initialState = initializeImportFromXrayState();
    this.store = createStore(initialState);
    this.$state = toSignal(this.store.state$, { initialValue: { ...initialState } });
  }

  addProjectId(projectId: number): void {
    this.store.commit({ ...this.$state(), projectId: projectId });
  }

  addImportName(name: string): void {
    this.store.commit({ ...this.$state(), importName: name });
  }

  addFiles(files: File[]): void {
    this.store.commit({
      ...this.$state(),
      errors: initialStateImportErrors(),
    });
    if (this.checkFormatFile(files)) {
      const allFiles: File[] = [...this.$state().files, ...files];
      const uniqueFiles: File[] = this.filterUniqueFile(
        allFiles,
        (file) => `${file.name}-${file.type}-${file.size}`,
      );
      this.store.commit({ ...this.$state(), files: uniqueFiles });
    }
  }

  checkFormatFile(files: File[]): boolean {
    const format: FileFormatItem = XrayFileFormat[this.$state().format];
    if (files.some((file) => file.type !== format.type)) {
      this.store.commit({
        ...this.$state(),
        errors: { ...this.$state().errors, errorFormatFile: true },
      });
      return false;
    }
    return true;
  }

  filterUniqueFile<T>(array: T[], key: (item: T) => string): T[] {
    const seen: Set<string> = new Set<string>();
    return array.filter((item) => {
      const k: string = key(item);
      return seen.has(k) ? false : seen.add(k);
    });
  }

  removeFile(file: File): void {
    const files: File[] = this.$state().files.filter((f: File) => f !== file);
    this.store.commit({ ...this.$state(), files: files });
  }

  isMissingFile(): boolean {
    if (this.$state().files.length === 0) {
      this.store.commit({
        ...this.$state(),
        errors: { ...this.$state().errors, errorMissingFile: true },
      });
      return true;
    }
    return false;
  }

  isErrors(state: ImportFromXrayState): boolean {
    return (
      state.errors.importFailed || state.errors.errorMissingFile || state.errors.errorFormatFile
    );
  }

  accessToConfirmationImport(): void {
    this.startAsync(ImportFromXraySteps.CONFIRMATION);
    this.doGetInfoFromXrayFormatFile(this.$state()).subscribe((response: ImportFromXrayState) =>
      this.endAsync(response),
    );
  }

  backToConfiguration(): void {
    this.store.commit({
      ...this.$state(),
      errors: initialStateImportErrors(),
      currentStep: ImportFromXraySteps.CONFIGURATION,
    });
  }

  backToConfirmation(): void {
    this.store.commit({
      ...this.$state(),
      errors: initialStateImportErrors(),
      currentStep: ImportFromXraySteps.CONFIRMATION,
    });
  }

  accessToGeneratePivot(): void {
    this.startAsync(ImportFromXraySteps.GENERATE);
    this.generatePivotFormat(this.$state())
      .pipe(
        take(1),
        map((state: ImportFromXrayState) => this.getPivotLog(state)),
      )
      .subscribe((state: ImportFromXrayState) => this.endAsync(state));
  }

  accessToImport(): void {
    this.startAsync(ImportFromXraySteps.IMPORTING);
    this.getPivotFile(this.$state())
      .pipe(
        take(1),
        switchMap((state: ImportFromXrayState) => this.importPivotFromXray(state)),
      )
      .subscribe((state: ImportFromXrayState) => this.endAsync(state));
  }

  deletePivotTempFiles(): void {
    this.deleteTempFiles();
  }

  private extractActionError(error: any): string | undefined {
    if (doesHttpErrorContainsSquashActionError(error)) {
      const actionError: SquashActionError = extractSquashActionError(error);
      let interpolateParams: object = {
        fileName: actionError.actionValidationError.i18nParams[0],
        message: actionError.actionValidationError.i18nParams[1],
      };
      if (actionError.actionValidationError.i18nParams.length > 2) {
        interpolateParams = {
          ...interpolateParams,
          line: actionError.actionValidationError.i18nParams[2],
        };
      }
      if (actionError.actionValidationError.i18nParams.length > 3) {
        interpolateParams = {
          ...interpolateParams,
          itemName: actionError.actionValidationError.i18nParams[3],
        };
      }
      return this.translate.instant(actionError.actionValidationError.i18nKey, interpolateParams);
    }
  }

  private doGetInfoFromXrayFormatFile(state: ImportFromXrayState): Observable<ImportFromXrayState> {
    return this.projectImportXrayService.getXrayInfoModelFromXML(state.files, state.projectId).pipe(
      take(1),
      map((response: XrayInfoModel) => ({ ...state, xrayItem: response })),
      catchError((_error) => {
        const actionErrorMessage = this.extractActionError(_error);
        return of({
          ...state,
          errors: {
            ...state.errors,
            importFailed: true,
            actionErrorMessage: actionErrorMessage,
          },
        });
      }),
    );
  }

  private generatePivotFormat(state: ImportFromXrayState): Observable<ImportFromXrayState> {
    return this.projectImportXrayService
      .getXrayImportModelFromXML(state.files, state.projectId)
      .pipe(
        take(1),
        map((response: XrayImportModel) => {
          return { ...state, xrayImportModel: response };
        }),
        catchError((_error) => of({ ...state, errors: { ...state.errors, importFailed: true } })),
      );
  }

  private getPivotLog(state: ImportFromXrayState): ImportFromXrayState {
    if (!this.isErrors(state) && state.xrayImportModel.pivotLogFileName) {
      const url: string = this.projectImportXrayService.getPivotLogFile(
        state.projectId,
        state.xrayImportModel.pivotLogFileName,
      );
      return { ...state, pivotLogUrl: url };
    }
    return state;
  }

  private getPivotFile(state: ImportFromXrayState): Observable<ImportFromXrayState> {
    if (state.xrayImportModel.pivotFileName) {
      return this.projectImportXrayService
        .getPivotFileXray(state.projectId, state.xrayImportModel.pivotFileName)
        .pipe(
          take(1),
          map((response: any) => {
            const file: File = new File([response], 'pivot.zip', {
              type: 'application/zip',
            });
            return { ...state, pivotFile: file };
          }),
          catchError((_error) => of({ ...state, errors: { ...state.errors, importFailed: true } })),
        );
    } else {
      return of({ ...state, errors: { ...state.errors, importFailed: true } });
    }
  }

  private importPivotFromXray(state: ImportFromXrayState): Observable<ImportFromXrayState> {
    if (!this.isErrors(state)) {
      return this.projectImportService
        .importProjectFromPivotFormatFile(
          [state.pivotFile],
          state.importName,
          ImportType.XRAY,
          state.projectId,
        )
        .pipe(
          take(1),
          map((response: any) => {
            return { ...state, importResult: { existingImports: response.existingImports } };
          }),
          catchError((_error) => of({ ...state, errors: { ...state.errors, importFailed: true } })),
        );
    } else {
      return of({ ...state, errors: { ...state.errors, importFailed: true } });
    }
  }

  private deleteTempFiles(): void {
    if (this.$state().xrayImportModel) {
      this.projectImportXrayService
        .deletePivotTempFiles(
          this.$state().projectId,
          this.$state().xrayImportModel.pivotFileName,
          this.$state().xrayImportModel.pivotLogFileName,
        )
        .pipe(take(1))
        .subscribe();
    }
  }

  private startAsync(currentStep: ImportFromXraySteps): void {
    this.store.commit({
      ...this.$state(),
      currentStep: currentStep,
      loadingData: true,
    });
  }

  private endAsync(state: ImportFromXrayState): void {
    this.store.commit({
      ...state,
      loadingData: false,
    });
  }

  complete() {
    this.store.complete();
  }
}
