import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {
  adminMilestonesTableDefinition,
  MilestoneWorkspaceGridComponent,
} from './milestone-workspace-grid.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  DataRow,
  DialogService,
  GRID_PERSISTENCE_KEY,
  GridService,
  GridTestingModule,
  GridWithStatePersistence,
  Identifier,
  MilestoneRange,
  MilestoneStatus,
  RestService,
  WorkspaceWithGridComponent,
} from 'sqtm-core';
import {
  ADMIN_WS_MILESTONE_TABLE,
  ADMIN_WS_MILESTONE_TABLE_CONFIG,
} from '../../../milestone-workspace.constant';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppTestingUtilsModule } from '../../../../../../utils/testing-utils/app-testing-utils.module';
import { BehaviorSubject, of } from 'rxjs';
import { take } from 'rxjs/operators';
import {
  mockClosableDialogService,
  mockGridService,
  mockPassThroughTranslateService,
  mockRestService,
  mockRouter,
} from '../../../../../../utils/testing-utils/mocks.service';
import { mockMouseEvent } from '../../../../../../utils/testing-utils/test-component-generator';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import createSpyObj = jasmine.createSpyObj;

describe('MilestoneWorkspaceGridComponent', () => {
  let component: MilestoneWorkspaceGridComponent;
  let fixture: ComponentFixture<MilestoneWorkspaceGridComponent>;

  let dialogService;
  let restService;
  let translateService;

  let gridService;
  let selectedRows$;
  let selectedRowIds$;
  let hasSelectedRows$;

  let loggedAsAdmin$;
  let authenticatedUser$;
  let milestoneFeatureEnabled$;
  let adminReferentialDataService;

  beforeEach(waitForAsync(() => {
    dialogService = mockClosableDialogService();
    restService = mockRestService();
    restService.post.and.returnValue(of({ milestones: [] }));
    translateService = mockPassThroughTranslateService();

    gridService = mockGridService();
    selectedRows$ = new BehaviorSubject<DataRow[]>([]);
    selectedRowIds$ = new BehaviorSubject<Identifier[]>([]);
    hasSelectedRows$ = new BehaviorSubject<boolean>(false);
    gridService.hasSelectedRows$ = hasSelectedRows$;
    gridService.selectedRowIds$ = selectedRowIds$;
    gridService.selectedRows$ = selectedRows$;

    loggedAsAdmin$ = new BehaviorSubject<boolean>(false);
    authenticatedUser$ = new BehaviorSubject<AuthenticatedUser>(null);
    milestoneFeatureEnabled$ = new BehaviorSubject<boolean>(true);

    adminReferentialDataService = createSpyObj(['setMilestoneFeatureEnabled']);
    adminReferentialDataService.authenticatedUser$ = authenticatedUser$;
    adminReferentialDataService.loggedAsAdmin$ = loggedAsAdmin$;
    adminReferentialDataService.milestoneFeatureEnabled$ = milestoneFeatureEnabled$;
    adminReferentialDataService.setMilestoneFeatureEnabled.and.returnValue(of(null));

    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, GridTestingModule],
      declarations: [MilestoneWorkspaceGridComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: ADMIN_WS_MILESTONE_TABLE_CONFIG,
          useFactory: adminMilestonesTableDefinition,
        },
        {
          provide: ADMIN_WS_MILESTONE_TABLE,
          useValue: gridService,
        },
        {
          provide: GridService,
          useValue: gridService,
        },
        {
          provide: AdminReferentialDataService,
          useValue: adminReferentialDataService,
        },
        {
          provide: DialogService,
          useValue: dialogService.service,
        },
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: Router,
          useValue: mockRouter(),
        },
        {
          provide: WorkspaceWithGridComponent,
          useValue: {},
        },
        {
          provide: TranslateService,
          useValue: translateService,
        },
        {
          provide: GRID_PERSISTENCE_KEY,
          useValue: 'milestone-workspace-main-grid',
        },
        {
          provide: ActivatedRoute,
          useValue: { snapshot: { paramMap: createSpyObj<Map<any, any>>(['get']) } },
        },
        GridWithStatePersistence,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MilestoneWorkspaceGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show milestone feature switch for admin if feature is disabled', waitForAsync(() => {
    const dataSets = [
      { input: { admin: true, featureEnabled: false }, expected: true },
      { input: { admin: true, featureEnabled: true }, expected: false },
      { input: { admin: false, featureEnabled: false }, expected: false },
      { input: { admin: false, featureEnabled: true }, expected: false },
    ];

    dataSets.forEach((dataSet) => {
      loggedAsAdmin$.next(dataSet.input.admin);
      milestoneFeatureEnabled$.next(dataSet.input.featureEnabled);

      component.milestoneSwitchVisible$
        .pipe(take(1))
        .subscribe((enabled) =>
          expect(enabled).toBe(dataSet.expected, `input: ${JSON.stringify(dataSet.input)}`),
        );
    });
  }));

  it('should activate buttons based on grid selection', waitForAsync(() => {
    const dataSets = [
      {
        input: { selection: [] },
        expectations: { deleteButton: false, duplicateButton: false, synchronizeButton: false },
      },
      {
        input: { selection: [1] },
        expectations: { deleteButton: true, duplicateButton: true, synchronizeButton: true },
      },
      {
        input: { selection: [1, 2] },
        expectations: { deleteButton: true, duplicateButton: false, synchronizeButton: true },
      },
    ];

    dataSets.forEach((dataSet) => {
      selectedRowIds$.next(dataSet.input.selection);
      hasSelectedRows$.next(dataSet.input.selection.length > 0);

      milestoneFeatureEnabled$.next(true);

      component.isDeleteButtonActive$
        .pipe(take(1))
        .subscribe((enabled) => expect(enabled).toBe(dataSet.expectations.deleteButton));

      component.isDuplicateButtonActive$
        .pipe(take(1))
        .subscribe((enabled) => expect(enabled).toBe(dataSet.expectations.duplicateButton));

      component.isSynchronizeButtonActive$
        .pipe(take(1))
        .subscribe((enabled) => expect(enabled).toBe(dataSet.expectations.synchronizeButton));
    });
  }));

  it('should open creation dialog', waitForAsync(() => {
    component.openCreateDialog();
    dialogService.closeDialogsWithResult(true);

    expect(dialogService.service.openDialog).toHaveBeenCalled();
    expect(gridService.refreshDataAsync).toHaveBeenCalled();
  }));

  it('should open delete dialog', waitForAsync(() => {
    restService.delete.and.returnValue(of(null));
    selectedRows$.next([
      {
        data: {},
      } as DataRow,
    ]);
    const user = { admin: true } as AuthenticatedUser;
    component.openDeleteDialog(mockMouseEvent(), user);
    dialogService.closeDialogsWithResult(true);

    expect(dialogService.service.openDeletionConfirm).toHaveBeenCalled();
    expect(restService.delete).toHaveBeenCalled();
    expect(gridService.refreshData).toHaveBeenCalled();
  }));

  it('should forbid deleting non owned milestones for a project manager', waitForAsync(() => {
    restService.delete.and.returnValue(of(null));
    selectedRows$.next([
      {
        data: { ownerId: 2 },
      } as unknown as DataRow,
    ]);
    const user = { admin: false, userId: 1 } as AuthenticatedUser;
    component.openDeleteDialog(mockMouseEvent(), user);
    dialogService.closeDialogsWithResult(true);

    expect(dialogService.service.openAlert).toHaveBeenCalled();
    expect(restService.delete).not.toHaveBeenCalled();
  }));

  it('should allow project manager to delete his owned milestones', waitForAsync(() => {
    restService.delete.and.returnValue(of(null));
    selectedRows$.next([
      {
        data: { ownerId: 2 },
      } as unknown as DataRow,
    ]);
    const user = { admin: false, userId: 2 } as AuthenticatedUser;
    component.openDeleteDialog(mockMouseEvent(), user);
    dialogService.closeDialogsWithResult(true);

    expect(dialogService.service.openAlert).not.toHaveBeenCalled();
    expect(restService.delete).toHaveBeenCalled();
  }));

  it('should toggle milestone feature off', waitForAsync(() => {
    milestoneFeatureEnabled$.next(true);

    component.handleMilestoneFeatureToggle();
    dialogService.closeDialogsWithResult(true);

    expect(dialogService.service.openDeletionConfirm).toHaveBeenCalled();
    expect(adminReferentialDataService.setMilestoneFeatureEnabled).toHaveBeenCalledWith(false);
  }));

  it('should toggle milestone feature on', waitForAsync(() => {
    milestoneFeatureEnabled$.next(false);

    component.handleMilestoneFeatureToggle();
    dialogService.closeDialogsWithResult(true);

    expect(dialogService.service.openConfirm).toHaveBeenCalled();
    expect(adminReferentialDataService.setMilestoneFeatureEnabled).toHaveBeenCalledWith(true);
  }));

  it('should duplicate a milestone', waitForAsync(() => {
    selectedRows$.next([
      {
        data: {
          status: MilestoneStatus.FINISHED.id,
        },
      } as unknown as DataRow,
    ]);

    component.duplicateMilestone();
    dialogService.closeDialogsWithResult(true);

    expect(dialogService.service.openAlert).not.toHaveBeenCalled();
    expect(dialogService.service.openDialog).toHaveBeenCalled();
    expect(gridService.refreshDataAsync).toHaveBeenCalled();
  }));

  it('should forbid to duplicate a milestone because its status is PLANNED', waitForAsync(() => {
    selectedRows$.next([
      {
        data: {
          status: MilestoneStatus.PLANNED.id,
        },
      } as unknown as DataRow,
    ]);

    component.duplicateMilestone();
    dialogService.closeDialogsWithResult(true);

    expect(dialogService.service.openAlert).toHaveBeenCalled();
    expect(dialogService.service.openDialog).not.toHaveBeenCalled();
  }));

  it('should show alert if selection length is not 2', () => {
    const dataSets = [
      { count: 1, shouldShowAlert: true },
      { count: 2, shouldShowAlert: false },
      { count: 3, shouldShowAlert: true },
    ];

    dataSets.forEach((dataSet) => {
      authenticatedUser$.next({ admin: true } as AuthenticatedUser);

      dialogService.resetCalls();

      const selection = [];
      for (let i = 0; i < dataSet.count; ++i) {
        selection.push({
          data: { range: MilestoneRange.GLOBAL.id, status: MilestoneStatus.FINISHED.id },
        });
      }

      selectedRows$.next(selection);

      component.openSynchronizeMilestoneDialog();

      if (dataSet.shouldShowAlert) {
        expect(dialogService.service.openAlert).toHaveBeenCalled();
      } else {
        expect(dialogService.service.openAlert).not.toHaveBeenCalled();
      }
    });
  });

  describe('Synchronization:', () => {
    const dataSets = [
      {
        name: 'should allow project manager to synchronize milestones if one is restricted with a legal status',
        input: {
          user: { admin: false },
          sourceMilestone: {
            range: MilestoneRange.RESTRICTED.id,
            status: MilestoneStatus.FINISHED.id,
          },
          targetMilestone: { range: MilestoneRange.GLOBAL.id, status: MilestoneStatus.FINISHED.id },
        },
        expectations: {
          hasAlert: false,
        },
      },
      {
        name: 'should prevent project manager from synchronizing global milestones',
        input: {
          user: { admin: false },
          sourceMilestone: { range: MilestoneRange.GLOBAL.id, status: MilestoneStatus.FINISHED.id },
          targetMilestone: { range: MilestoneRange.GLOBAL.id, status: MilestoneStatus.FINISHED.id },
        },
        expectations: {
          hasAlert: true,
        },
      },
      {
        name: 'should forbid project manager to synchronize restricted milestones if status is not "in progress" or "finished"',
        input: {
          user: { admin: false },
          sourceMilestone: {
            range: MilestoneRange.RESTRICTED.id,
            status: MilestoneStatus.PLANNED.id,
          },
          targetMilestone: { range: MilestoneRange.GLOBAL.id, status: MilestoneStatus.FINISHED.id },
        },
        expectations: {
          hasAlert: true,
        },
      },
      {
        name: 'should allow admin to synchronize global milestones',
        input: {
          user: { admin: true },
          sourceMilestone: { range: MilestoneRange.GLOBAL.id, status: MilestoneStatus.FINISHED.id },
          targetMilestone: { range: MilestoneRange.GLOBAL.id, status: MilestoneStatus.FINISHED.id },
        },
        expectations: {
          hasAlert: false,
        },
      },
      {
        name: 'should forbid admin to synchronize milestones with "planned" status',
        input: {
          user: { admin: true },
          sourceMilestone: { range: MilestoneRange.GLOBAL.id, status: MilestoneStatus.PLANNED.id },
          targetMilestone: { range: MilestoneRange.GLOBAL.id, status: MilestoneStatus.FINISHED.id },
        },
        expectations: {
          hasAlert: true,
        },
      },
      {
        name: 'should forbid admin to synchronize milestones if none has "finished" or "in progress" status.',
        input: {
          user: { admin: true },
          sourceMilestone: { range: MilestoneRange.GLOBAL.id, status: MilestoneStatus.LOCKED.id },
          targetMilestone: { range: MilestoneRange.GLOBAL.id, status: MilestoneStatus.LOCKED.id },
        },
        expectations: {
          hasAlert: true,
        },
      },
    ];

    dataSets.forEach((dataSet) => {
      it(dataSet.name, () => {
        authenticatedUser$.next({
          userId: 1,
          admin: dataSet.input.user.admin,
          hasAnyReadPermission: true,
          projectManager: true,
          milestoneManager: true,
          clearanceManager: true,
          functionalTester: true,
          automationProgrammer: true,
          username: '',
          lastName: '',
          firstName: '',
        });
        selectedRows$.next([
          {
            data: { ...dataSet.input.sourceMilestone },
          } as unknown as DataRow,
          {
            data: { ...dataSet.input.targetMilestone },
          } as unknown as DataRow,
        ]);

        component.openSynchronizeMilestoneDialog();

        if (dataSet.expectations.hasAlert) {
          expect(dialogService.service.openAlert).toHaveBeenCalled();
          expect(dialogService.service.openDialog).not.toHaveBeenCalled();
        } else {
          expect(dialogService.service.openAlert).not.toHaveBeenCalled();
          expect(dialogService.service.openDialog).toHaveBeenCalled();
        }
      });
    });
  });
});
