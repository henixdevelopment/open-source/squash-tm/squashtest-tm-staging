import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import {
  CreationDialogData,
  DialogReference,
  DisplayOption,
  FieldValidationError,
  NOT_ONLY_SPACES_REGEX,
  RestService,
} from 'sqtm-core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AbstractAdministrationCreationDialogDirective } from '../../../../../components/abstract-administration-creation-dialog';
import { of } from 'rxjs';
import { ProfileService } from '../../../services/profile.service';
import { TranslateService } from '@ngx-translate/core';

const Controls = {
  name: 'name',
  description: 'description',
  referenceProfileId: 'referenceProfileId',
};

@Component({
  selector: 'sqtm-app-profile-creation-dialog',
  templateUrl: './profile-creation-dialog.component.html',
  styleUrls: ['./profile-creation-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileCreationDialogComponent
  extends AbstractAdministrationCreationDialogDirective
  implements OnInit
{
  Controls = Controls;
  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  data: CreationDialogData;
  profileOptions: DisplayOption[];

  constructor(
    private fb: FormBuilder,
    private profileService: ProfileService,
    private translateService: TranslateService,
    dialogReference: DialogReference,
    restService: RestService,
    cdr: ChangeDetectorRef,
  ) {
    super('profiles/new', dialogReference, restService, cdr);
  }

  get getReferenceProfileExplainMessage(): string {
    return this.translateService.instant(
      'sqtm-core.administration-workspace.profiles.dialog.message.new-profile.reference-profile-explain',
    );
  }

  get textFieldToFocus(): string {
    return 'name';
  }

  ngOnInit() {
    this.initializeFormGroup();
    this.initializeProfileSelectField();
  }

  private initializeProfileSelectField() {
    this.profileService.getProfiles().subscribe((profiles) => {
      const options = this.profileService.retrieveProfilesAsDisplayOptions(profiles);

      // Sort options by locale label
      options.sort((a, b) => {
        return a.label.localeCompare(b.label);
      });

      this.profileOptions = [...options];
      if (options.length > 0) {
        this.getFormControl(Controls.referenceProfileId).enable();
      }
      this.cdr.detectChanges();
    });
  }

  protected getRequestPayload() {
    return of({
      name: this.getFormControlValue(Controls.name),
      referenceProfileId: this.getFormControlValue(Controls.referenceProfileId),
      description: this.getFormControlValue(Controls.description),
    });
  }

  protected doResetForm() {
    this.resetFormControl(Controls.name, '');
    this.resetFormControl(Controls.referenceProfileId, '');
    this.resetFormControl(Controls.description, '');
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      name: this.fb.control('', [
        Validators.required,
        Validators.pattern(NOT_ONLY_SPACES_REGEX),
        Validators.maxLength(50),
      ]),
      referenceProfileId: this.fb.control('', [Validators.required]),
      description: this.fb.control(''),
    });
  }
}
