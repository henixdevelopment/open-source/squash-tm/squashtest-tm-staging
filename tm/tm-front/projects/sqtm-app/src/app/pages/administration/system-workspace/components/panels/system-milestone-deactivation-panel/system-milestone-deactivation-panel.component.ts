import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Input,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { filter, switchMap, take, takeUntil, tap } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { SystemViewState } from '../../../states/system-view.state';
import {
  AdminReferentialDataService,
  DialogReference,
  DialogService,
  SwitchFieldComponent,
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-system-milestone-deactivation-panel',
  templateUrl: './system-milestone-deactivation-panel.component.html',
  styleUrls: [
    './system-milestone-deactivation-panel.component.less',
    '../../../styles/system-workspace.common.less',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SystemMilestoneDeactivationPanelComponent implements AfterViewInit, OnDestroy {
  @Input()
  componentData: SystemViewState;

  @ViewChild(SwitchFieldComponent)
  enableMilestoneFeatureSwitch: SwitchFieldComponent;

  milestoneFeatureEnabled$: Observable<boolean>;
  unsub$ = new Subject<void>();

  constructor(
    private dialogService: DialogService,
    public adminReferentialDataService: AdminReferentialDataService,
  ) {
    this.milestoneFeatureEnabled$ = adminReferentialDataService.milestoneFeatureEnabled$;
  }

  ngAfterViewInit(): void {
    this.milestoneFeatureEnabled$.pipe(takeUntil(this.unsub$)).subscribe((enabled) => {
      if (this.enableMilestoneFeatureSwitch != null) {
        this.enableMilestoneFeatureSwitch.value = enabled;
      }
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  handleMilestoneFeatureToggle(): void {
    this.milestoneFeatureEnabled$
      .pipe(
        take(1),
        switchMap((currentlyEnabled) =>
          this.showConfirmToggleMilestoneFeatureDialog(!currentlyEnabled),
        ),
        filter((confirm) => Boolean(confirm)),
        switchMap(() => this.toggleMilestoneFeature()),
      )
      .subscribe();
  }

  private showConfirmToggleMilestoneFeatureDialog(enable: boolean): Observable<boolean> {
    let dialogReference: DialogReference;

    if (enable) {
      dialogReference = this.dialogService.openConfirm({
        id: 'confirm-enable-milestone-feature',
        titleKey:
          'sqtm-core.administration-workspace.milestones.dialog.title.confirm-enable-milestone-feature',
        messageKey:
          'sqtm-core.administration-workspace.milestones.dialog.message.confirm-enable-milestone-feature',
        level: 'INFO',
      });
    } else {
      dialogReference = this.dialogService.openDeletionConfirm({
        id: 'confirm-disable-milestone-feature',
        titleKey:
          'sqtm-core.administration-workspace.milestones.dialog.title.confirm-disable-milestone-feature',
        messageKey:
          'sqtm-core.administration-workspace.milestones.dialog.message.confirm-disable-milestone-feature',
        level: 'DANGER',
      });
    }

    return dialogReference.dialogClosed$.pipe(
      tap((confirm) => {
        if (!confirm) {
          this.enableMilestoneFeatureSwitch.endAsyncMode();
        }
      }),
    );
  }

  private toggleMilestoneFeature(): Observable<any> {
    return this.milestoneFeatureEnabled$.pipe(
      take(1),
      switchMap((enabled) => this.adminReferentialDataService.setMilestoneFeatureEnabled(!enabled)),
    );
  }
}
