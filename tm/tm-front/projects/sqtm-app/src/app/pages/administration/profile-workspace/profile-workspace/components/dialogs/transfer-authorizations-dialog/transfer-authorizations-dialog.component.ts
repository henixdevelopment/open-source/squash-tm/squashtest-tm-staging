import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { DialogReference, DisplayOption } from 'sqtm-core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProfileService } from '../../../services/profile.service';
import { TransferAuthorizationsDialogConfiguration } from './transfer-authorizations-dialog-configuration';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

const Controls = {
  targetProfile: 'targetProfile',
};

@Component({
  selector: 'sqtm-app-transfer-authorizations-dialog',
  templateUrl: './transfer-authorizations-dialog.component.html',
  styleUrl: './transfer-authorizations-dialog.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransferAuthorizationsDialogComponent implements OnInit {
  profileOptions: DisplayOption[];
  formGroup: FormGroup;
  Controls = Controls;
  data: TransferAuthorizationsDialogConfiguration;
  selectedTargetProfile$: Observable<any>;

  constructor(
    private profileService: ProfileService,
    private fb: FormBuilder,
    protected cdr: ChangeDetectorRef,
    private dialogRef: DialogReference,
    private translateService: TranslateService,
  ) {
    this.data = this.dialogRef.data;
  }

  ngOnInit() {
    this.initializeFormGroup();
    this.initializeProfileSelectField();
  }

  private initializeProfileSelectField() {
    this.profileService.getProfiles().subscribe((profiles) => {
      const options = this.profileService.retrieveProfilesAsDisplayOptions(profiles);
      options.sort((a, b) => {
        return a.label.localeCompare(b.label);
      });

      const filteredOptions = options.filter((option) => option.id !== this.data.profile.id);

      this.profileOptions = [...filteredOptions];
      if (filteredOptions.length > 0) {
        this.formGroup.controls[this.Controls.targetProfile].enable();
      }
      this.cdr.detectChanges();
    });
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      targetProfile: this.fb.control(''),
    });
    this.selectedTargetProfile$ = this.formGroup.controls[this.Controls.targetProfile].valueChanges;
  }

  confirmTransfer(targetProfileId: string) {
    const profileId: string = this.data.profile.id.toString();
    this.profileService.transferProfileAuthorizations(profileId, targetProfileId).subscribe({
      next: () => {
        this.cdr.detectChanges();
        this.dialogRef.result = targetProfileId;
        this.dialogRef.close();
      },
    });
  }

  getProfileName(profileId: number): string {
    const selectedProfile = this.profileOptions.find((profile) => profile.id == profileId);
    return selectedProfile ? selectedProfile.label : '';
  }

  getMessageToDisplay(targetProfileId: number): string {
    const partyCount: number = this.data.profile.partyCount;
    const targetProfile: string = this.getProfileName(targetProfileId);
    return this.translateService.instant(
      'sqtm-core.administration-workspace.profiles.dialog.message.confirm-transfer-clearances',
      { partyCount, targetProfile },
    );
  }
}
