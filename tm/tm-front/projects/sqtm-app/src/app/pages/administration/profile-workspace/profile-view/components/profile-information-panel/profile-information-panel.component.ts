import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { AdminProfileViewComponentData } from '../../containers/panel-groups/profile-content/profile-content.component';
import { AdminProfileState } from '../../states/admin-profile-state';
import { ProfileViewService } from '../../services/profile-view.service';
import { GridService } from 'sqtm-core';
import { ProfileService } from '../../../profile-workspace/services/profile.service';

@Component({
  selector: 'sqtm-app-profile-information-panel',
  templateUrl: './profile-information-panel.component.html',
  styleUrls: ['./profile-information-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileInformationPanelComponent {
  @Input() componentData: AdminProfileViewComponentData;
  @Input() isUltimate: boolean;

  constructor(
    private readonly profileViewService: ProfileViewService,
    private readonly profileService: ProfileService,
    private gridService: GridService,
  ) {}

  get inactiveColor(): string {
    const isActive = this.componentData.profile.active;
    return isActive ? 'disabled' : 'inactive';
  }

  get activeColor(): string {
    const isActive = this.componentData.profile.active;
    return isActive ? 'active' : 'disabled';
  }

  getProfileType(profile: AdminProfileState): string {
    return profile.system
      ? 'sqtm-core.nav-bar.administration.system.label'
      : 'sqtm-core.entity.profile.custom.label';
  }

  changeProfileStatus(): void {
    const isActive = this.componentData.profile.active;
    isActive
      ? this.profileViewService.deactivateProfile()
      : this.profileViewService.activateProfile();
  }
}
