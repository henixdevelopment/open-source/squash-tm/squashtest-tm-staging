import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { SystemViewState } from '../../../states/system-view.state';
import { Observable, Subject } from 'rxjs';
import { SystemViewService } from '../../../services/system-view.service';
import { takeUntil } from 'rxjs/operators';
import { AdminReferentialDataService } from 'sqtm-core';

@Component({
  selector: 'sqtm-app-system-view-information',
  templateUrl: './system-view-information.component.html',
  styleUrls: ['./system-view-information.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SystemViewInformationComponent implements OnDestroy {
  componentData$: Observable<SystemViewState>;

  unsub$ = new Subject<void>();

  constructor(
    public readonly systemViewService: SystemViewService,
    public adminReferentialDataService: AdminReferentialDataService,
  ) {
    this.componentData$ = this.systemViewService.componentData$.pipe(takeUntil(this.unsub$));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
