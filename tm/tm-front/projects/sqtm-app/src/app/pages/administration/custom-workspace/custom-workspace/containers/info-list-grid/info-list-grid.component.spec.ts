import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { adminInfoListsTableDefinition, InfoListGridComponent } from './info-list-grid.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  DataRow,
  DialogService,
  GridService,
  GridTestingModule,
  RestService,
  WorkspaceWithGridComponent,
} from 'sqtm-core';
import {
  ADMIN_WS_INFO_LISTS_TABLE,
  ADMIN_WS_INFO_LISTS_TABLE_CONFIG,
} from '../../../custom-workspace.constant';
import { AppTestingUtilsModule } from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {
  mockClosableDialogService,
  mockGridService,
  mockRestService,
  mockRouter,
} from '../../../../../../utils/testing-utils/mocks.service';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import createSpyObj = jasmine.createSpyObj;

describe('InfoListGridComponent', () => {
  let component: InfoListGridComponent;
  let fixture: ComponentFixture<InfoListGridComponent>;

  const dialogMock = mockClosableDialogService();
  const dialogService = dialogMock.service;
  const restService = mockRestService();
  const gridService = mockGridService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [InfoListGridComponent],
      imports: [GridTestingModule, AppTestingUtilsModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: ADMIN_WS_INFO_LISTS_TABLE_CONFIG,
          useFactory: adminInfoListsTableDefinition,
        },
        {
          provide: ADMIN_WS_INFO_LISTS_TABLE,
          useValue: gridService,
        },
        {
          provide: GridService,
          useValue: gridService,
        },
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: DialogService,
          useValue: dialogService,
        },
        {
          provide: Router,
          useValue: mockRouter(),
        },
        {
          provide: WorkspaceWithGridComponent,
          useValue: {},
        },
        {
          provide: ActivatedRoute,
          useValue: { snapshot: { paramMap: createSpyObj<Map<any, any>>(['get']) } },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoListGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    gridService.refreshData.calls.reset();
    dialogMock.resetSubjects();
    dialogMock.resetCalls();
    restService.delete.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open creation dialog', () => {
    component.openCreateDialog();

    dialogMock.closeDialogsWithResult(true);

    expect(dialogService.openDialog).toHaveBeenCalled();
    expect(gridService.refreshDataAsync).toHaveBeenCalled();
  });

  it('should delete info list', () => {
    gridService.selectedRows$ = of([
      { data: { infoListId: 1, projectCount: 0 } } as unknown as DataRow,
      { data: { infoListId: 2, projectCount: 0 } } as unknown as DataRow,
    ]);
    restService.delete.and.returnValue(of({}));

    component.deleteInfoLists();

    dialogMock.closeDialogsWithResult(true);

    expect(restService.delete).toHaveBeenCalledWith(['info-lists', '1,2']);
    expect(gridService.refreshData).toHaveBeenCalled();
  });
});
