import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  AdminReferentialDataService,
  AdminReferentialDataState,
  DialogReference,
  DisplayOption,
  FieldValidationError,
  RestService,
  SelectFieldComponent,
  TemplateConfigurablePlugin,
} from 'sqtm-core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ProjectViewService } from '../../../services/project-view.service';
import { map, take } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-associate-template-dialog',
  templateUrl: './associate-template-dialog.component.html',
  styleUrls: ['./associate-template-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AssociateTemplateDialogComponent implements OnInit {
  public templateOptions: DisplayOption[];
  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  templateConfigurablePlugins: TemplateConfigurablePlugin[] = [];

  @ViewChild(SelectFieldComponent)
  templateSelectField: SelectFieldComponent;

  private readonly noTemplateOptionId: string = '';

  constructor(
    private dialogReference: DialogReference<any, void>,
    private restService: RestService,
    private cdr: ChangeDetectorRef,
    private fb: FormBuilder,
    private projectViewService: ProjectViewService,
    private translateService: TranslateService,
    private adminReferentialDataService: AdminReferentialDataService,
  ) {}

  ngOnInit(): void {
    this.adminReferentialDataService.adminReferentialData$
      .pipe(
        take(1),
        map((refData: AdminReferentialDataState) => refData.templateConfigurablePlugins),
      )
      .subscribe((plugins) => {
        this.templateConfigurablePlugins = plugins;
        this.initializeFormGroup();
        this.prepareTemplateSelectField();
      });
  }

  private prepareTemplateSelectField() {
    this.restService
      .get<{ templates: { id: string; name: string }[] }>(['generic-projects/templates'])
      .subscribe((response) => {
        const defaultOption: DisplayOption = this.retrieveDefaultDisplayOption();
        const options = retrieveTemplatesAsDisplayOptions(response.templates);

        this.templateOptions = [defaultOption, ...options];

        if (options.length > 0) {
          this.templateSelectField.disabled = false;
        }

        this.formGroup.controls['template'].setValue(this.noTemplateOptionId);

        this.cdr.detectChanges();
      });
  }

  private retrieveDefaultDisplayOption() {
    return {
      id: this.noTemplateOptionId,
      label: this.translateService.instant(
        'sqtm-core.administration-workspace.projects.dialog.message.associate-template-default-option',
      ),
    };
  }

  private initializeFormGroup() {
    const controlsConfig = {
      template: this.fb.control(null, [Validators.required]),
    };

    this.templateConfigurablePlugins.forEach((plugin) => {
      controlsConfig[this.getKeepPluginBindingControlId(plugin)] = this.fb.control(false);
    });

    this.formGroup = this.fb.group(controlsConfig);
  }

  confirm() {
    if (!this.formGroup.valid) {
      this.templateSelectField.showClientSideError();
      return;
    }
    const selectedTemplateId = this.formGroup.controls['template'].value;
    const boundTemplatePlugins = this.templateConfigurablePlugins
      .filter((plugin) => this.formGroup.controls[this.getKeepPluginBindingControlId(plugin)].value)
      .map((plugin) => plugin.id);

    this.projectViewService.associateWithTemplate(selectedTemplateId, boundTemplatePlugins);
    this.dialogReference.close();
  }

  public getKeepPluginBindingControlId(plugin: TemplateConfigurablePlugin): string {
    return 'keepPluginBinding_' + plugin.id;
  }
}

function retrieveTemplatesAsDisplayOptions(templates: { id: string; name: string }[]) {
  return templates.map((ref) => ({ id: ref.id, label: ref.name }));
}
