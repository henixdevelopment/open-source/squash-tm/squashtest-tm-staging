import { XrayImportModel, XrayInfoModel } from 'sqtm-core';
import { AddPivotFormatImportResponse } from '../../project-view/components/dialogs/import-from-pivot-format-dialog/import-from-pivot-format-dialog.component';

export interface ImportFromXrayState {
  projectId: number;
  currentStep: ImportFromXraySteps;
  format: XrayImportFileFormatKeys;
  importName: string;
  files: File[];
  xrayItem: XrayInfoModel;
  errors: ImportErrors;
  loadingData: boolean;
  importResult: AddPivotFormatImportResponse;
  xrayImportModel: XrayImportModel;
  pivotFile: File;
  pivotLogUrl: string;
}

export function initializeImportFromXrayState(): Readonly<ImportFromXrayState> {
  return {
    projectId: null,
    currentStep: ImportFromXraySteps.CONFIGURATION,
    format: 'XML',
    importName: null,
    files: [],
    xrayItem: null,
    errors: initialStateImportErrors(),
    loadingData: false,
    importResult: { existingImports: [] },
    xrayImportModel: null,
    pivotFile: null,
    pivotLogUrl: null,
  };
}

export interface ImportErrors {
  importFailed: boolean;
  errorMissingFile: boolean;
  errorFormatFile: boolean;
  actionErrorMessage: string;
}

export function initialStateImportErrors(): Readonly<ImportErrors> {
  return {
    importFailed: false,
    errorMissingFile: false,
    errorFormatFile: false,
    actionErrorMessage: null,
  };
}

export type XrayImportFileFormatKeys = 'XML';

export type FileFormat<K extends string> = { [id in K]: FileFormatItem };

export interface FileFormatItem {
  id: string;
  i18nKey: string;
  value: string;
  type: string;
}

export const XrayFileFormat: FileFormat<XrayImportFileFormatKeys> = {
  XML: { id: 'XML', i18nKey: 'XML', value: 'xml', type: 'text/xml' },
};

export enum ImportFromXraySteps {
  CONFIGURATION = 'CONFIGURATION',
  CONFIRMATION = 'CONFIRMATION',
  GENERATE = 'GENERATE',
  IMPORTING = 'IMPORTING',
}

export enum XrayInfoEnum {
  TEST = 'Test',
  PRECONDITION = 'Pre-Condition',
  TEST_PLAN = 'Test Plan',
  TEST_EXECUTION = 'Test Execution',
}

export const xrayInfoRecord: Record<XrayInfoEnum, XrayInfoRecordStructure> = {
  [XrayInfoEnum.TEST]: {
    entityName: XrayInfoEnum.TEST,
    i18nKey:
      'sqtm-core.administration-workspace.views.project.import.dialog.import-from-xray.label.confirmation.xray-test',
  },
  [XrayInfoEnum.PRECONDITION]: {
    entityName: XrayInfoEnum.PRECONDITION,
    i18nKey:
      'sqtm-core.administration-workspace.views.project.import.dialog.import-from-xray.label.confirmation.xray-precondition',
  },
  [XrayInfoEnum.TEST_PLAN]: {
    entityName: XrayInfoEnum.TEST_PLAN,
    i18nKey:
      'sqtm-core.administration-workspace.views.project.import.dialog.import-from-xray.label.confirmation.xray-test-plan',
  },
  [XrayInfoEnum.TEST_EXECUTION]: {
    entityName: XrayInfoEnum.TEST_EXECUTION,
    i18nKey:
      'sqtm-core.administration-workspace.views.project.import.dialog.import-from-xray.label.confirmation.test-execution-sub',
  },
};

export interface XrayInfoRecordStructure {
  entityName: string;
  i18nKey: string;
}

export interface XrayInfoDisplay {
  identifier: string;
  i18nKey: string;
  entityCount: number;
}
