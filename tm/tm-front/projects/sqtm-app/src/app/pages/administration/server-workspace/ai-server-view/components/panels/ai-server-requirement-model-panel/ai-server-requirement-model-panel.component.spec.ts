import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { AiServerRequirementModelPanelComponent } from './ai-server-requirement-model-panel.component';
import { AiServerViewService } from '../../../services/ai-server-view.service';
import { TranslateModule } from '@ngx-translate/core';

describe('AiServerRequirementModelPanel component', () => {
  let component: AiServerRequirementModelPanelComponent;
  let fixture: ComponentFixture<AiServerRequirementModelPanelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      providers: [
        {
          provide: AiServerViewService,
          useValue: {
            backendRootUrl: '',
            backendContextPath: '',
          },
        },
      ],
      declarations: [AiServerRequirementModelPanelComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AiServerRequirementModelPanelComponent);
    component = fixture.componentInstance;
  });

  it('should replace reserved characters with their entity equivalent', () => {
    const textWithSpecialCharacters = `This is 'a' text with <s>html reserved characters &</s>`;
    const resultText = component.replaceReservedHtmlCharsWithEntity(textWithSpecialCharacters);
    expect(resultText).toBe(
      `This is &#039;a&#039; text with &lt;s&gt;html reserved characters &amp;&lt;/s&gt;`,
    );
  });
});
