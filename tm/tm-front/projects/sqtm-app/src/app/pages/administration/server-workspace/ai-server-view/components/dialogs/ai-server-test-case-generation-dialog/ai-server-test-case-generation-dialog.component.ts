import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { DialogReference, TestCaseDisplayData, TestCaseFromAi } from 'sqtm-core';
import { AiServerTestCaseGenerationDialogConfiguration } from './ai-server-test-case-generation-dialog-configuration';

@Component({
  selector: 'sqtm-app-ai-server-test-case-generation-dialog',
  templateUrl: './ai-server-test-case-generation-dialog.component.html',
  styleUrl: './ai-server-test-case-generation-dialog.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AiServerTestCaseGenerationDialogComponent implements OnInit {
  serverResponse: { testCases: TestCaseFromAi[] };
  testCasesDisplayData: TestCaseDisplayData[] = [];

  constructor(
    public readonly dialogReference: DialogReference<AiServerTestCaseGenerationDialogConfiguration>,
  ) {
    this.serverResponse = this.dialogReference.data.serverResponse;
  }

  ngOnInit() {
    const testCasesToDisplay = this.serverResponse.testCases;
    this.testCasesDisplayData = testCasesToDisplay.map(
      (testCase: TestCaseFromAi, index: number) => {
        return this.createTestCaseDisplayData(testCase, index, testCasesToDisplay.length);
      },
    );
  }

  private createTestCaseDisplayData(
    testCaseFromAi: TestCaseFromAi,
    index: number,
    testCasesArrayLength: number,
  ): TestCaseDisplayData {
    return {
      name: testCaseFromAi.name,
      description: testCaseFromAi.description,
      prerequisites: testCaseFromAi.prerequisites,
      testSteps: testCaseFromAi.testSteps,
      index: index,
      isLastIndex: index === testCasesArrayLength - 1,
      isSelectedByUser: false,
    };
  }
}
