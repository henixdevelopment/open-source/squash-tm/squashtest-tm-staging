import {
  ChangeDetectionStrategy,
  Component,
  InjectionToken,
  OnDestroy,
  OnInit,
  ViewContainerRef,
} from '@angular/core';
import {
  booleanColumn,
  DataRow,
  deleteColumn,
  DialogService,
  Extendable,
  GridColumnId,
  GridDefinition,
  GridFilter,
  GridService,
  indexColumn,
  RestService,
  smallGrid,
  Sort,
  StyleDefinitionBuilder,
} from 'sqtm-core';
import { Observable, Subject } from 'rxjs';
import { concatMap, filter, finalize, map, switchMap, take, takeUntil, tap } from 'rxjs/operators';
import { createSelector, select } from '@ngrx/store';
import { AdminMilestoneViewComponentData } from '../../../containers/milestone-view/milestone-view.component';
import {
  getMilestoneViewState,
  MilestoneViewService,
} from '../../../services/milestone-view.service';
import { AdminMilestoneState } from '../../../states/admin-milestone-state';
import { UnbindProjectFromMilestoneCellComponent } from '../../cell-renderers/unbind-project-from-milestone-cell/unbind-project-from-milestone-cell.component';
import { projectBoundToMilestoneEditableBooleanColumn } from '../../cell-renderers/project-bound-to-milestone-cell/project-bound-to-milestone-cell.component';
import {
  BindableProject,
  BindProjectsToMilestoneDialogComponent,
} from '../../dialogs/bind-projects-to-milestone-dialog/bind-projects-to-milestone-dialog.component';
import { clickableProjectNameColumn } from '../../../../../cell-renderer.builders';

export const MILESTONE_PROJECTS_TABLE_CONF = new InjectionToken('MILESTONE_PROJECTS_TABLE_CONF');
export const MILESTONE_PROJECTS_TABLE = new InjectionToken('MILESTONE_PROJECTS_TABLE');
export function milestoneProjectsTableDefinition(): GridDefinition {
  return smallGrid('milestone-projects')
    .withColumns([
      indexColumn().withViewport('leftViewport'),
      clickableProjectNameColumn(GridColumnId.projectName)
        .withI18nKey('sqtm-core.entity.project.label.singular')
        .changeWidthCalculationStrategy(new Extendable(200, 0.5)),
      projectBoundToMilestoneEditableBooleanColumn(GridColumnId.boundToMilestone)
        .changeWidthCalculationStrategy(new Extendable(100, 0.2))
        .withHeaderPosition('center'),
      booleanColumn(GridColumnId.milestoneBoundToOneObjectOfProject)
        .withI18nKey('sqtm-core.entity.milestone.used')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1))
        .withHeaderPosition('center')
        .withContentPosition('center'),
      deleteColumn(UnbindProjectFromMilestoneCellComponent),
    ])
    .withInitialSortedColumns([{ id: GridColumnId.projectName, sort: Sort.ASC }])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .enableMultipleColumnsFiltering([GridColumnId.projectName])
    .build();
}

@Component({
  selector: 'sqtm-app-milestone-project-panel',
  template: `
    <div class="m-t-10 m-l-10 m-b-15 flex-fixed-size" style="width: 200px">
      <sqtm-core-text-research-field
        (newResearchValue)="handleResearchInput($event)"
      ></sqtm-core-text-research-field>
    </div>
    <sqtm-core-grid></sqtm-core-grid>
  `,
  styleUrls: ['./milestone-project-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: MILESTONE_PROJECTS_TABLE,
    },
  ],
})
export class MilestoneProjectPanelComponent implements OnInit, OnDestroy {
  private componentData$: Observable<AdminMilestoneViewComponentData>;
  private unsub$ = new Subject<void>();

  constructor(
    private milestoneViewService: MilestoneViewService,
    public gridService: GridService,
    private dialogService: DialogService,
    private vcRef: ViewContainerRef,
    private restService: RestService,
  ) {}

  ngOnInit(): void {
    this.componentData$ = this.milestoneViewService.componentData$;
    this.initializeTable();
    this.initializeFilters();
  }

  bindProjectsToMilestone() {
    this.componentData$
      .pipe(
        take(1),
        map((componentData) => componentData.milestone.id),
        switchMap((milestoneId) => this.getBindableProjectsServerSide(milestoneId)),
        switchMap((response) => this.openBindProjectsToMilestoneDialog(response.bindableProjects)),
        filter((result) => Boolean(result)),
        tap(() => this.gridService.beginAsyncOperation()),
        switchMap((result: number[]) => this.milestoneViewService.bindProjectsToMilestone(result)),
        finalize(() => this.gridService.completeAsyncOperation()),
      )
      .subscribe();
  }

  private getBindableProjectsServerSide(
    milestoneId: number,
  ): Observable<{ bindableProjects: BindableProject[] }> {
    const urlParts = ['milestone-binding', milestoneId.toString(), 'bindable-projects'];
    return this.restService.get<{ bindableProjects: BindableProject[] }>(urlParts);
  }

  private openBindProjectsToMilestoneDialog(bindableProjects: BindableProject[]) {
    const dialogRef = this.dialogService.openDialog({
      id: 'bind-milestones-dialog',
      component: BindProjectsToMilestoneDialogComponent,
      viewContainerReference: this.vcRef,
      data: {
        bindableProjects: bindableProjects,
      },
      width: 720,
      height: 650,
    });
    return dialogRef.dialogClosed$;
  }

  unbindProjectsFromMilestone(): void {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        filter((rows: DataRow[]) => rows.length > 0),
        map((rows: DataRow[]) => rows.map((row) => row.data.projectId)),
        concatMap((projectIds: number[]) =>
          this.showConfirmUnbindProjectsFromMilestoneDialog(projectIds),
        ),
        filter(({ confirmDelete }) => confirmDelete),
        tap(() => this.gridService.beginAsyncOperation()),
        switchMap(({ projectIds }) =>
          this.milestoneViewService.unbindProjectsFromMilestone(projectIds),
        ),
        finalize(() => this.gridService.completeAsyncOperation()),
      )
      .subscribe();
  }

  private showConfirmUnbindProjectsFromMilestoneDialog(
    projectIds,
  ): Observable<ConfirmDeleteDialogResult> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey:
        'sqtm-core.administration-workspace.milestones.dialog.title.unbind-project-from-milestone.unbind-many',
      messageKey:
        'sqtm-core.administration-workspace.milestones.dialog.message.unbind-project-from-milestone.unbind-many',
      level: 'WARNING',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map((confirmDelete) => ({ confirmDelete, projectIds })),
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private initializeFilters(): void {
    const filters: GridFilter[] = [
      {
        id: GridColumnId.projectName,
        active: false,
        initialValue: { kind: 'single-string-value', value: '' },
        tiedToPerimeter: false,
      },
    ];
    this.gridService.addFilters(filters);
  }

  handleResearchInput($event: string): void {
    this.gridService.applyMultiColumnsFilter($event);
  }

  private initializeTable(): void {
    const projectsTable = this.componentData$.pipe(
      takeUntil(this.unsub$),
      select(
        createSelector(
          getMilestoneViewState,
          (milestoneState: AdminMilestoneState) => milestoneState.boundProjectsInformation,
        ),
      ),
    );

    this.gridService.connectToDatasource(projectsTable, 'projectId');
  }
}

interface ConfirmDeleteDialogResult {
  confirmDelete: boolean;
  projectIds: number[];
}
