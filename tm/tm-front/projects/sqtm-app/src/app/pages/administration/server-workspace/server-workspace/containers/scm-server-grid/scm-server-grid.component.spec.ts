import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ScmServerGridComponent } from './scm-server-grid.component';
import {
  DataRow,
  DialogService,
  GridService,
  GridTestingModule,
  RestService,
  WorkspaceWithGridComponent,
} from 'sqtm-core';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  ADMIN_WS_BUGTRACKERS_TABLE,
  ADMIN_WS_BUGTRACKERS_TABLE_CONFIG,
} from '../../../server-workspace.constant';
import { adminBugtrackersTableDefinition } from '../bugtracker-grid/bugtracker-grid.component';
import { AppTestingUtilsModule } from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {
  mockClosableDialogService,
  mockGridService,
  mockRestService,
  mockRouter,
} from '../../../../../../utils/testing-utils/mocks.service';
import { of } from 'rxjs';
import { mockMouseEvent } from '../../../../../../utils/testing-utils/test-component-generator';
import { ActivatedRoute, Router } from '@angular/router';
import createSpyObj = jasmine.createSpyObj;

describe('ScmServerGridComponent', () => {
  let component: ScmServerGridComponent;
  let fixture: ComponentFixture<ScmServerGridComponent>;

  const dialogMock = mockClosableDialogService();
  const dialogService = dialogMock.service;
  const restService = mockRestService();
  const gridService = mockGridService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ScmServerGridComponent],
      imports: [GridTestingModule, AppTestingUtilsModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: ADMIN_WS_BUGTRACKERS_TABLE_CONFIG,
          useFactory: adminBugtrackersTableDefinition(),
        },
        {
          provide: ADMIN_WS_BUGTRACKERS_TABLE,
          useValue: gridService,
        },
        {
          provide: GridService,
          useValue: gridService,
        },
        {
          provide: DialogService,
          useValue: dialogService,
        },
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: Router,
          useValue: mockRouter(),
        },
        {
          provide: WorkspaceWithGridComponent,
          useValue: {},
        },
        {
          provide: ActivatedRoute,
          useValue: { snapshot: { paramMap: createSpyObj<Map<any, any>>(['get']) } },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScmServerGridComponent);
    component = fixture.componentInstance;
    restService.get.and.returnValue(of({ scmServerKinds: ['git'] }));
    restService.delete.and.returnValue(of({}));

    dialogMock.resetCalls();
    dialogMock.resetSubjects();
    restService.delete.calls.reset();
    gridService.refreshData.calls.reset();
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should warn user if no server kind is available (no plugin)', () => {
    restService.get.and.returnValue(of({ scmServerKinds: [] }));
    fixture.detectChanges();

    component.openScmServerDialog();

    expect(dialogService.openAlert).toHaveBeenCalled();
    expect(dialogService.openDialog).not.toHaveBeenCalled();
  });

  it('should open creation dialog', () => {
    fixture.detectChanges();
    component.openScmServerDialog();

    dialogMock.closeDialogsWithResult(true);

    expect(dialogService.openDialog).toHaveBeenCalled();
    expect(gridService.refreshDataAsync).toHaveBeenCalled();
  });

  it('should delete scm servers', () => {
    fixture.detectChanges();
    gridService.selectedRows$ = of([{ data: { serverId: 42 } } as unknown as DataRow]);
    component.deleteScmServers(mockMouseEvent());

    dialogMock.closeDialogsWithResult(true);

    expect(dialogService.openDeletionConfirm).toHaveBeenCalled();
    expect(restService.delete).toHaveBeenCalledWith(['scm-servers', '42']);
    expect(gridService.refreshData).toHaveBeenCalled();
  });
});
