import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractDeleteCellRenderer,
  CellRendererCommonModule,
  ConfirmDeleteLevel,
  DialogService,
  GridColumnId,
  GridService,
  RestService,
} from 'sqtm-core';
import { TranslateModule } from '@ngx-translate/core';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-delete-test-auto-server-api-token-cell-renderer',
  template: ` <sqtm-core-delete-icon
    nz-tooltip
    [nzTooltipTitle]="'sqtm-core.generic.label.delete' | translate"
    (delete)="showDeleteConfirm()"
  >
  </sqtm-core-delete-icon>`,
  standalone: true,
  imports: [CellRendererCommonModule, NzToolTipModule, TranslateModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteTestAutoServerApiTokenCellRendererComponent extends AbstractDeleteCellRenderer {
  constructor(
    private readonly gridService: GridService,
    private readonly cdr: ChangeDetectorRef,
    protected readonly dialogService: DialogService,
    private readonly restService: RestService,
  ) {
    super(gridService, cdr, dialogService);
  }

  doDelete() {
    this.grid.beginAsyncOperation();
    const tokenId = this.row.data[GridColumnId.tokenId];
    this.restService
      .delete([`user-view/delete-token`, tokenId])
      .pipe(finalize(() => this.grid.completeAsyncOperation()))
      .subscribe(() => this.grid.refreshData());
  }

  protected getTitleKey(): string {
    return 'sqtm-core.user-account-page.api-token.dialog.delete.title';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.user-account-page.api-token.dialog.delete.message-basic';
  }

  protected getLevel(): ConfirmDeleteLevel {
    return 'DANGER';
  }
}
