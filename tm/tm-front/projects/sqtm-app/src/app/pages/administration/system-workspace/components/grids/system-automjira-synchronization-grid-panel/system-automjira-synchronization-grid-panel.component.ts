import { ChangeDetectionStrategy, Component, InjectionToken } from '@angular/core';
import {
  AdminReferentialDataService,
  GridId,
  GridService,
  gridServiceFactory,
  LocalPersistenceService,
  ReferentialDataService,
  RestService,
  SynchronizationPluginId,
} from 'sqtm-core';

import {
  AbstractSynchronizationGridPanelComponent,
  remoteSyncTableDefinition,
} from '../abstract-synchronization-grid-panel/abstract-synchronization-grid-panel.component';
import { SystemViewService } from '../../../services/system-view.service';

export const AUTOM_JIRA_REMOTE_SYNC_TABLE_CONF = new InjectionToken(
  'AUTOM_JIRA_REMOTE_SYNC_TABLE_CONF',
);
export const AUTOM_JIRA_REMOTE_SYNC_TABLE = new InjectionToken('AUTOM_JIRA_REMOTE_SYNC_TABLE');

@Component({
  selector: 'sqtm-app-system-automjira-synchronization-grid-panel',
  templateUrl:
    '../abstract-synchronization-grid-panel/abstract-synchronization-grid-panel.component.html',
  styleUrls: [
    '../abstract-synchronization-grid-panel/abstract-synchronization-grid-panel.component.less',
  ],
  providers: [
    {
      provide: AUTOM_JIRA_REMOTE_SYNC_TABLE_CONF,
      useFactory: (localPersistenceService) =>
        remoteSyncTableDefinition(
          GridId.SYSTEM_AUTOM_JIRA_SYNCHRONISATIONS,
          localPersistenceService,
        ),
      deps: [LocalPersistenceService],
    },
    {
      provide: AUTOM_JIRA_REMOTE_SYNC_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, AUTOM_JIRA_REMOTE_SYNC_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: AUTOM_JIRA_REMOTE_SYNC_TABLE,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SystemAutomjiraSynchronizationGridPanelComponent extends AbstractSynchronizationGridPanelComponent {
  constructor(
    public adminReferentialDataService: AdminReferentialDataService,
    protected systemViewService: SystemViewService,
    protected gridService: GridService,
  ) {
    super(adminReferentialDataService, systemViewService, gridService);
    this.pluginId = SynchronizationPluginId.AUTOM_JIRA;
  }
}
