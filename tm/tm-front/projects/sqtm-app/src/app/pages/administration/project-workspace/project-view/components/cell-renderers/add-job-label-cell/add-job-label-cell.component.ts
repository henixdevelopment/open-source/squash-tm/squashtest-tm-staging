import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  Fixed,
  GridColumnId,
  GridService,
  Identifier,
} from 'sqtm-core';
import { Subject } from 'rxjs';
import { AddJobDialogService } from '../../../services/add-job-dialog.service';

@Component({
  selector: 'sqtm-app-add-job-label-cell',
  templateUrl: './add-job-label-cell.component.html',
  styleUrls: ['./add-job-label-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddJobLabelCellComponent extends AbstractCellRendererComponent implements OnDestroy {
  private readonly unsub$ = new Subject<void>();

  constructor(
    grid: GridService,
    cdRef: ChangeDetectorRef,
    private addJobDialogService: AddJobDialogService,
  ) {
    super(grid, cdRef);
  }

  // Use a hidden data attribute to stock transient values (SQUASH-3402)
  get editedValue(): string {
    if (this.row.data.editedLabel == null) {
      this.row.data.editedLabel = this.row.data.label;
    }

    return this.row.data.editedLabel;
  }

  set editedValue(newValue: string) {
    this.row.data.editedLabel = newValue;
  }

  isRowSelected(selectedRowIds: Identifier[]): boolean {
    return selectedRowIds.includes(this.row.id);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  updateState(): void {
    this.addJobDialogService.setJobLabel(this.row.id as string, this.editedValue);
  }
}

export function addJobNameColumn(id: GridColumnId, label = ''): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(AddJobLabelCellComponent)
    .withLabel(label)
    .disableSort()
    .changeWidthCalculationStrategy(new Fixed(50));
}
