import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EditProjectAutomationJobDialogComponent } from './edit-project-automation-job-dialog.component';
import { DialogReference, FieldValidationError, RestService } from 'sqtm-core';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { AppTestingUtilsModule } from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import { ProjectViewService } from '../../../services/project-view.service';
import { mockRestService } from '../../../../../../../utils/testing-utils/mocks.service';
import { of, throwError } from 'rxjs';
import { makeHttpFieldValidationError } from '../../../../../../../utils/testing-utils/test-error-response-generator';
import { mockTextField } from '../../../../../../../utils/testing-utils/test-component-generator';
import createSpyObj = jasmine.createSpyObj;

describe('EditProjectAutomationJobDialogComponent', () => {
  let component: EditProjectAutomationJobDialogComponent;
  let fixture: ComponentFixture<EditProjectAutomationJobDialogComponent>;

  const restService = mockRestService();
  const projectViewService = createSpyObj(['updateTestAutomationProject']);
  const dialogReference = createSpyObj(['close']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        AppTestingUtilsModule,
        ReactiveFormsModule,
        TranslateModule.forRoot(),
        NzCheckboxModule,
      ],
      providers: [
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: DialogReference,
          useValue: dialogReference,
        },
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj(['instant']),
        },
        TranslateService,
        {
          provide: ProjectViewService,
          useValue: projectViewService,
        },
      ],
      declarations: [EditProjectAutomationJobDialogComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditProjectAutomationJobDialogComponent);
    component = fixture.componentInstance;
    component.data = {
      remoteName: 'test',
      taProjectId: -1,
      canRunBdd: false,
      label: 'test',
      executionEnvironment: 'tfServer',
    };
    fixture.detectChanges();

    dialogReference.close.calls.reset();
    projectViewService.updateTestAutomationProject.calls.reset();
    projectViewService.updateTestAutomationProject.and.returnValue(of({}));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should edit and close', () => {
    component.confirm();

    expect(dialogReference.result).toBe(true);
    expect(dialogReference.close).toHaveBeenCalled();
  });

  it('should handle server-side errors', () => {
    const validationErrors = [{ i18nKey: 'BABBBOUUUM' } as FieldValidationError];
    projectViewService.updateTestAutomationProject.and.returnValue(
      throwError(() => makeHttpFieldValidationError(validationErrors)),
    );

    component.confirm();

    expect(dialogReference.close).not.toHaveBeenCalled();
    expect(component.serverSideValidationErrors).toBe(validationErrors);
  });

  it('should show client-side validation errors', () => {
    const tf = mockTextField('epic', component.formGroup);
    component.textFields.reset([tf]);
    component.formGroup.setErrors({ epic: 'fail' });

    component.confirm();

    expect(dialogReference.close).not.toHaveBeenCalled();
    expect(tf.showClientSideError).toHaveBeenCalled();
  });
});
