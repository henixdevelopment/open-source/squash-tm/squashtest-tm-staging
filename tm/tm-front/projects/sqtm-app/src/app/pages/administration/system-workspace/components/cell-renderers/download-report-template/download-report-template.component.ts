import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
} from 'sqtm-core';
import { SystemViewService } from '../../../services/system-view.service';

@Component({
  selector: 'sqtm-app-download-report-template',
  template: `
    <sqtm-core-toggle-icon
      class="full-height full-width icon-container current-workspace-main-color m-t-2"
    >
      <a [href]="downloadTemplate()" target="_blank">
        <i
          class="table-icon-size reset-tags-button"
          nz-icon
          nzType="sqtm-core-generic:download"
          nzTheme="outline"
          nz-tooltip
          [nzTooltipTitle]="'sqtm-core.generic.label.download' | translate"
        >
        </i>
      </a>
    </sqtm-core-toggle-icon>
  `,
  styleUrls: ['./download-report-template.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DownloadReportTemplateComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    private systemViewService: SystemViewService,
    cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }

  downloadTemplate() {
    return (
      this.systemViewService.downloadTemplate(
        this.row.data[GridColumnId.reportId],
        this.row.data[GridColumnId.fileName],
      ) ?? window.location.origin
    );
  }
}

export function reportTemplateDownloadColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(DownloadReportTemplateComponent)
    .disableHeader();
}
