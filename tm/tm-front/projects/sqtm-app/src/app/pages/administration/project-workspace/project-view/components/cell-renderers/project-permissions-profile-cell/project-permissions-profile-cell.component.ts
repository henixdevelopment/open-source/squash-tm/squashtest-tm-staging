import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  Signal,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import {
  AbstractListCellRendererComponent,
  AclGroup,
  ActionErrorDisplayService,
  AdminReferentialDataService,
  AuthenticatedUser,
  ColumnDefinitionBuilder,
  getAclGroupI18nKey,
  GridColumnId,
  GridService,
  isDefaultSystemAclGroup,
  LicenseInformationMessageProvider,
  ListPanelItem,
  Permissions,
  Profile,
  RestService,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { ConnectedPosition, Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ProjectViewService } from '../../../services/project-view.service';
import { map, take, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AdminProjectViewState } from '../../../state/admin-project-view-state';
import { ProfileService } from '../../../../../profile-workspace/profile-workspace/services/profile.service';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sqtm-app-project-permissions-profile-cell',
  templateUrl: './project-permissions-profile-cell.component.html',
  styleUrls: ['./project-permissions-profile-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ProfileService],
})
export class ProjectPermissionsProfileCellComponent
  extends AbstractListCellRendererComponent
  implements OnInit
{
  @ViewChild('templatePortalContent', { read: TemplateRef })
  templatePortalContent: TemplateRef<any>;

  @ViewChild('profileName', { read: ElementRef })
  profileName: ElementRef;

  overlayRef: OverlayRef;

  panelItems: ListPanelItem[] = [];

  _licenseAllowsUserCreation = false;

  $componentData: Signal<AdminProjectViewState>;

  private _currentUser: AuthenticatedUser;

  constructor(
    public readonly grid: GridService,
    public readonly cdRef: ChangeDetectorRef,
    public readonly translateService: TranslateService,
    public readonly overlay: Overlay,
    public readonly vcr: ViewContainerRef,
    public readonly restService: RestService,
    public readonly projectViewService: ProjectViewService,
    public readonly actionErrorDisplayService: ActionErrorDisplayService,
    public readonly adminReferentialDataService: AdminReferentialDataService,
    private router: Router,
    private readonly profileService: ProfileService,
  ) {
    super(grid, cdRef, overlay, vcr, translateService, restService, actionErrorDisplayService);
    this.$componentData = toSignal(this.projectViewService.componentData$);
    this.cacheCurrentUser();
  }

  ngOnInit() {
    this.preparePanelItems();
    this.initialiseLicenseLock();
  }

  getCellText(profiles: Profile[]): string {
    if (profiles == null) {
      return '';
    }

    const permissionGroup = this.row.data[GridColumnId.permissionGroup] as Profile;

    return this.profileService.getProfileName(permissionGroup);
  }

  canEdit(): boolean {
    return this._licenseAllowsUserCreation && this.isAdminOrClearanceManager();
  }

  private isAdminOrClearanceManager(): boolean {
    return (
      this._currentUser.admin ||
      this.$componentData().project.permissions.PROJECT?.includes(
        Permissions.MANAGE_PROJECT_CLEARANCE,
      )
    );
  }

  showProfileList(): void {
    if (this.canEdit()) {
      this.showList(this.profileName, this.templatePortalContent, LIST_POSITIONS);
    }
  }

  private preparePanelItems(): void {
    const profiles = this.$componentData().project.availableProfiles;
    const options = this.profileService.retrieveProfilesAsDisplayOptions(profiles);

    // Sort options by locale label
    options.sort((a, b) => {
      return a.label.localeCompare(b.label);
    });

    this.panelItems = [...options];
  }

  private initialiseLicenseLock(): void {
    this.adminReferentialDataService.licenseInformation$
      .pipe(
        takeUntil(this.unsub$),
        map(
          (licenseInfo) =>
            new LicenseInformationMessageProvider(licenseInfo, this.translateService),
        ),
      )
      .subscribe((messageHelper) => {
        this._licenseAllowsUserCreation = messageHelper.licenseInformation.allowCreateUsers;
        this.cdRef.detectChanges();
      });
  }

  private cacheCurrentUser(): void {
    this.adminReferentialDataService.authenticatedUser$
      .pipe(take(1))
      .subscribe((user) => (this._currentUser = user));
  }

  change(newValue: number): void {
    const currentUserId = this.row.data[GridColumnId.partyId];
    this.projectViewService.setPartyProjectPermissions([currentUserId], newValue).subscribe({
      error: (error) => this.handleChangeError(newValue, error),
    });

    this.close();
  }

  private handleChangeError(newValue: any, error): void {
    if (this.checkIfProjectManagerRemovesHisOwnPermissions(newValue)) {
      this.projectViewService.forceProjectWorkspaceGridRefresh().subscribe(() => {
        this.router.navigate(['administration-workspace/projects']);
      });
    }
    console.error(error);
  }

  private checkIfProjectManagerRemovesHisOwnPermissions(newValue: string): boolean {
    const currentUserId = this.row.data[GridColumnId.partyId];
    return (
      !this._currentUser.admin &&
      this._currentUser.userId === currentUserId &&
      newValue !== AclGroup.PROJECT_MANAGER
    );
  }
}

export function permissionsProfileColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(ProjectPermissionsProfileCellComponent)
    .withI18nKey('sqtm-core.administration-workspace.views.project.permissions.profile.label');
}

export function buildPermissionProfile(translateService: TranslateService) {
  return (keyA: any, keyB: any) => sortPermissionProfile(keyA, keyB, translateService);
}

function sortPermissionProfile(roleA: any, roleB: any, translateService: TranslateService) {
  const translatedRoleA = isDefaultSystemAclGroup(roleA.qualifiedName)
    ? translateService.instant(getAclGroupI18nKey(roleA.qualifiedName))
    : roleA.qualifiedName;
  const translatedRoleB = isDefaultSystemAclGroup(roleB.qualifiedName)
    ? translateService.instant(getAclGroupI18nKey(roleB.qualifiedName))
    : roleB.qualifiedName;

  return translatedRoleA.localeCompare(translatedRoleB);
}

const LIST_POSITIONS: ConnectedPosition[] = [
  {
    originX: 'start',
    overlayX: 'start',
    originY: 'bottom',
    overlayY: 'top',
    offsetX: -10,
    offsetY: 6,
  },
  {
    originX: 'start',
    overlayX: 'start',
    originY: 'top',
    overlayY: 'bottom',
    offsetX: -10,
    offsetY: -6,
  },
];
