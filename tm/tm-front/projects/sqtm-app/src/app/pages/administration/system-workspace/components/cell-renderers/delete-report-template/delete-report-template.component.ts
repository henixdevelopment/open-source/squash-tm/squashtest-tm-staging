import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import { Observable, Subject, takeUntil } from 'rxjs';
import {
  AbstractDeleteCellRenderer,
  DialogService,
  GridService,
  ReportTemplateModel,
} from 'sqtm-core';
import { SystemViewService } from '../../../services/system-view.service';
import { TranslateService } from '@ngx-translate/core';
import { filter, take } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-delete-report-template',
  template: ` <sqtm-core-delete-icon (delete)="doDelete()"></sqtm-core-delete-icon> `,
  styleUrls: ['./delete-report-template.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteReportTemplateComponent extends AbstractDeleteCellRenderer implements OnDestroy {
  unsub$ = new Subject<void>();

  constructor(
    public grid: GridService,
    cdr: ChangeDetectorRef,
    protected dialogService: DialogService,
    private systemViewService: SystemViewService,
    private translateService: TranslateService,
  ) {
    super(grid, cdr, dialogService);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  protected doDelete() {
    const selectedTemplate: ReportTemplateModel = {
      reportId: this.row.data['reportId'],
      fileName: this.row.data['fileName'],
    };
    this.isTemplateUsed(selectedTemplate)
      .pipe(take(1))
      .subscribe((isUsed) => {
        this.openDeletionDialog(isUsed, selectedTemplate);
      });
  }

  doDeleteTemplate(selectedTemplate: ReportTemplateModel) {
    this.systemViewService.deleteCustomReportTemplate(selectedTemplate).subscribe();
  }

  private isTemplateUsed(templateModel: ReportTemplateModel): Observable<boolean> {
    return this.systemViewService.isTemplateUsedInReport(templateModel);
  }

  private openDeletionDialog(isTemplateUsed: boolean, selectedTemplate: ReportTemplateModel) {
    let dialogConfiguration;
    if (isTemplateUsed) {
      dialogConfiguration = {
        titleKey: 'sqtm-core.administration-workspace.system.report-templates.dialog.delete.title',
        messageKey: this.translateService.instant(
          'sqtm-core.administration-workspace.system.report-templates.dialog.delete.in-use-warning',
          { fileName: selectedTemplate.fileName },
        ),
        level: 'DANGER',
      };
    } else {
      dialogConfiguration = {
        titleKey: 'sqtm-core.administration-workspace.system.report-templates.dialog.delete.title',
        messageKey: this.translateService.instant(
          'sqtm-core.administration-workspace.system.report-templates.dialog.delete.simple-warning',
          { fileName: selectedTemplate.fileName },
        ),
        level: 'WARNING',
      };
    }

    const dialogReference = this.dialogService.openDeletionConfirm(dialogConfiguration);

    dialogReference.dialogClosed$
      .pipe(
        takeUntil(this.unsub$),
        filter((result) => result === true),
      )
      .subscribe(() => {
        this.doDeleteTemplate(selectedTemplate);
      });
  }
}
