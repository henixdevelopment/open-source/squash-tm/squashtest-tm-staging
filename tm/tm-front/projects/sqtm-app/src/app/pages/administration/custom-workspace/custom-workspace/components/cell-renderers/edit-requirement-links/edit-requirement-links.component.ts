import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewContainerRef,
} from '@angular/core';
import { AbstractCellRendererComponent, DialogService, GridColumnId, GridService } from 'sqtm-core';
import { EditRequirementLinksDialogComponent } from '../../dialogs/edit-requirement-links-dialog/edit-requirement-links-dialog.component';

@Component({
  selector: 'sqtm-app-edit-requirement-links',
  templateUrl: './edit-requirement-links.component.html',
  styleUrl: './edit-requirement-links.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditRequirementLinksComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    cdr: ChangeDetectorRef,
    protected dialogService: DialogService,
    public vcr: ViewContainerRef,
  ) {
    super(grid, cdr);
  }

  handleClick() {
    this.grid.completeAsyncOperation();
    this.dialogService.openDialog({
      id: 'edit-requirement-links-dialog',
      component: EditRequirementLinksDialogComponent,
      viewContainerReference: this.vcr,
      data: {
        requirementLinkId: this.row.data[GridColumnId.id],
        role1: this.row.data[GridColumnId.role],
        role2: this.row.data[GridColumnId.role2],
        role1Code: this.row.data[GridColumnId.role1Code],
        role2Code: this.row.data[GridColumnId.role2Code],
      },
      width: 400,
    });
  }
}
