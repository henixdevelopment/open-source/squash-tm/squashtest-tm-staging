import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  AdminReferentialDataService,
  DialogReference,
  DisplayOption,
  FieldValidationError,
  getScmServerKindI18nKey,
  RestService,
  ScmServerKind,
  TextFieldComponent,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { AbstractAdministrationCreationDialogDirective } from '../../../../../components/abstract-administration-creation-dialog';
import { of } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-scm-server-creation-dialog',
  templateUrl: './scm-server-creation-dialog.component.html',
  styleUrls: ['./scm-server-creation-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScmServerCreationDialogComponent
  extends AbstractAdministrationCreationDialogDirective
  implements OnInit
{
  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  availableKindOptions: DisplayOption[] = [];

  @ViewChildren(TextFieldComponent)
  textFields: QueryList<TextFieldComponent>;

  constructor(
    private fb: FormBuilder,
    private translateService: TranslateService,
    dialogReference: DialogReference,
    restService: RestService,
    cdr: ChangeDetectorRef,
    public readonly adminReferentialDataService: AdminReferentialDataService,
  ) {
    super('scm-servers/new', dialogReference, restService, cdr);
  }

  get textFieldToFocus(): string {
    return 'name';
  }

  ngOnInit() {
    this.adminReferentialDataService.adminReferentialData$.pipe(take(1)).subscribe((refData) => {
      this.prepareServerKindOptions(refData.availableScmServerKinds);
      this.initializeFormGroup();
    });
  }

  protected getRequestPayload() {
    return of({
      name: this.getFormControlValue('name'),
      kind: this.getFormControlValue('kind'),
      url: this.getFormControlValue('url'),
      description: this.getFormControlValue('description'),
    });
  }

  protected doResetForm() {
    this.resetFormControl('name', '');
    this.resetFormControl('kind', '');
    this.resetFormControl('url', '');
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      name: this.fb.control('', [Validators.required, Validators.maxLength(50)]),
      kind: this.fb.control(this.availableKindOptions[0].id, [Validators.required]),
      url: this.fb.control('', [Validators.required, Validators.maxLength(255)]),
      description: this.fb.control(''),
    });
  }

  private prepareServerKindOptions(availableScmServerKinds: string[]): void {
    this.availableKindOptions = availableScmServerKinds.map((kind: ScmServerKind) => ({
      label: getScmServerKindI18nKey(kind),
      id: kind,
    }));
  }
}
