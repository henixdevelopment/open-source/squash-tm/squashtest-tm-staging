import { PartyProfileAuthorization, ProfilePermissions, SqtmGenericEntityState } from 'sqtm-core';

export interface AdminProfileState extends SqtmGenericEntityState {
  id: number;
  name: string;
  qualifiedName: string; // for system profiles, this is not the real name
  partyCount: number;
  description: string;
  active: boolean;
  system: boolean;
  createdBy: string;
  createdOn: Date;
  lastModifiedBy: string;
  lastModifiedOn: Date;
  permissions: ProfilePermissions[];
  partyProfileAuthorizations: PartyProfileAuthorization[];
}
