import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  InjectionToken,
  OnInit,
} from '@angular/core';
import {
  CreationDialogData,
  deleteColumn,
  DialogReference,
  Extendable,
  FieldValidationError,
  Fixed,
  GridColumnId,
  GridDefinition,
  GridService,
  gridServiceFactory,
  isInfoListCodePatternValid,
  ReferentialDataService,
  RestService,
  smallGrid,
  StyleDefinitionBuilder,
  textColumn,
} from 'sqtm-core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { BehaviorSubject, Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { AbstractAdministrationCreationDialogDirective } from '../../../../../components/abstract-administration-creation-dialog';
import { InfoListOption, InfoListOptionService } from '../../../services/info-list-option.service';
import { map, take } from 'rxjs/operators';
import { defaultInfoListOptionColumn } from '../../cell-renderers/default-info-list-option/default-info-list-option.component';
import { infoListOptionIconColumn } from '../../cell-renderers/info-list-option-icon/info-list-option-icon.component';
import { HttpErrorResponse } from '@angular/common/http';
import { infoListOptionColorPickerColumn } from '../../cell-renderers/info-list-color-picker/info-list-color-picker.component';
import { DeleteInfoListOptionComponent } from '../../cell-renderers/delete-info-list-option/delete-info-list-option.component';

enum Fields {
  label = 'label',
  code = 'code',
  description = 'description',
  infoListOptionLabel = 'infoListOptionLabel',
  infoListOptionCode = 'infoListOptionCode',
}

export const INFO_LIST_OPTIONS_TABLE_CONF = new InjectionToken('INFO_LIST_OPTIONS_TABLE_CONF');
export const INFO_LIST_OPTIONS_TABLE = new InjectionToken('INFO_LIST_OPTIONS_TABLE');

export function infoListOptionsTableDefinition(): GridDefinition {
  return smallGrid('info-list-options')
    .withColumns([
      textColumn(GridColumnId.label)
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Extendable(100, 1))
        .disableSort(),
      textColumn(GridColumnId.code)
        .withI18nKey('sqtm-core.generic.label.code')
        .changeWidthCalculationStrategy(new Extendable(100, 1))
        .disableSort(),
      infoListOptionIconColumn(GridColumnId.iconName)
        .withI18nKey('sqtm-core.entity.info-list-item.icon.label')
        .changeWidthCalculationStrategy(new Fixed(75))
        .withHeaderPosition('center')
        .disableSort(),
      infoListOptionColorPickerColumn(GridColumnId.colour)
        .withI18nKey('sqtm-core.entity.generic.color.label')
        .changeWidthCalculationStrategy(new Fixed(70))
        .disableSort(),
      defaultInfoListOptionColumn(GridColumnId.isDefault)
        .withI18nKey('sqtm-core.generic.label.default')
        .changeWidthCalculationStrategy(new Fixed(70))
        .withHeaderPosition('center')
        .disableSort(),
      deleteColumn(DeleteInfoListOptionComponent),
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .build();
}

@Component({
  selector: 'sqtm-app-info-list-creation-dialog',
  templateUrl: './info-list-creation-dialog.component.html',
  styleUrls: ['./info-list-creation-dialog.component.less'],
  providers: [
    {
      provide: INFO_LIST_OPTIONS_TABLE_CONF,
      useFactory: infoListOptionsTableDefinition,
    },
    {
      provide: INFO_LIST_OPTIONS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, INFO_LIST_OPTIONS_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: INFO_LIST_OPTIONS_TABLE,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InfoListCreationDialogComponent
  extends AbstractAdministrationCreationDialogDirective
  implements OnInit
{
  formGroup: FormGroup;
  infoListOptionsFormGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  data: CreationDialogData;
  private infoListOptions$: Observable<InfoListOption[]>;

  private _hasInfoListOptionDefaultError = new BehaviorSubject<boolean>(false);
  public hasInfoListOptionDefaultError$ = this._hasInfoListOptionDefaultError.asObservable();

  constructor(
    private fb: FormBuilder,
    private translateService: TranslateService,
    private gridService: GridService,
    private infoListOptionService: InfoListOptionService,
    dialogReference: DialogReference,
    restService: RestService,
    cdr: ChangeDetectorRef,
  ) {
    super('info-lists/new', dialogReference, restService, cdr);
    this.data = this.dialogReference.data;
  }

  get textFieldToFocus(): string {
    return Fields.label;
  }

  ngOnInit(): void {
    this.initializeFormGroup();
    this.initializeInfoListOptionsFormGroup();
    this.initializeInfoListTable();
  }

  addOption(): void {
    const optionLabel = this.getInfoListOptionsFormControlValue(Fields.infoListOptionLabel);
    const optionCode = this.getInfoListOptionsFormControlValue(Fields.infoListOptionCode);

    if (this.infoListListFormIsValid()) {
      this.infoListOptionService.addOption(optionLabel, optionCode).subscribe({
        next: () => {
          this.resetInfoListOptionsForm();
        },
        error: (error) => {
          this.handleDuplicateCodeFailure(error);
          this.showInfoListOptionsFormGroupErrors();
        },
      });
    } else {
      this.showInfoListOptionsFormGroupErrors();
    }
  }

  protected handleDuplicateCodeFailure(error: HttpErrorResponse) {
    if (error.status === 412) {
      const squashError = error.error.squashTMError;
      if (squashError.kind === 'FIELD_VALIDATION_ERROR') {
        this.getInfoListOptionsFormControl(Fields.infoListOptionCode).setErrors({
          optionCodeAlreadyExists: true,
        });
      }
    }
  }

  protected getRequestPayload(): Observable<any> {
    return this.infoListOptions$.pipe(
      take(1),
      map((options: InfoListOption[]) => {
        return {
          label: this.getFormControlValue(Fields.label),
          code: this.getFormControlValue(Fields.code),
          description: this.getFormControlValue(Fields.description),
          items: options,
        };
      }),
    );
  }

  addEntity(addAnother?: boolean): void {
    this._hasInfoListOptionDefaultError.next(false);
    this.checkMandatoryInfoListDefaultValue()
      .pipe(take(1))
      .subscribe((result: boolean) => {
        if (result) {
          super.addEntity(addAnother);
        } else {
          this._hasInfoListOptionDefaultError.next(true);
        }
      });
  }

  private checkMandatoryInfoListDefaultValue(): Observable<boolean> {
    return this.infoListOptions$.pipe(
      take(1),
      map((options: InfoListOption[]) => {
        return options.find((option) => option.isDefault === true) != null;
      }),
    );
  }

  private infoListListFormIsValid(): boolean {
    this.initializeErrorsForDuplicateOptionNameOrCode();
    return this.infoListOptionsFormGroup.status === 'VALID';
  }

  private initializeErrorsForDuplicateOptionNameOrCode(): void {
    const optionLabel = this.getInfoListOptionsFormControlValue(Fields.infoListOptionLabel);
    const optionCode = this.getInfoListOptionsFormControlValue(Fields.infoListOptionCode);
    this.infoListOptions$
      .pipe(
        take(1),
        map((options) => {
          if (this.checkOptionCodeAlreadyExist(optionCode, options)) {
            this.getInfoListOptionsFormControl(Fields.infoListOptionCode).setErrors({
              optionCodeAlreadyExists: true,
            });
          }

          if (this.checkOptionLabelAlreadyExist(optionLabel, options)) {
            this.getInfoListOptionsFormControl(Fields.infoListOptionLabel).setErrors({
              optionNameAlreadyExists: true,
            });
          }
        }),
      )
      .subscribe();
  }

  private checkOptionCodeAlreadyExist(code: string, options: InfoListOption[]): boolean {
    return options.find((option) => option.code === code) != null;
  }

  private checkOptionLabelAlreadyExist(label: string, options: InfoListOption[]): boolean {
    return options.find((option) => option.label === label) != null;
  }

  private showInfoListOptionsFormGroupErrors(): void {
    this.textFields
      .filter((textField) => {
        return (
          textField.fieldName === Fields.infoListOptionLabel ||
          textField.fieldName === Fields.infoListOptionCode
        );
      })
      .forEach((filteredTextField) => filteredTextField.showClientSideError());
  }

  protected doResetForm(): void {
    this.resetFormControl(Fields.label, '');
    this.resetFormControl(Fields.code, '');
    this.resetFormControl(Fields.description, '');
    this.resetInfoListOptionsForm();
    this.infoListOptionService.initialize();
  }

  private resetInfoListOptionsForm(): void {
    this.resetInfoListOptionsFormControl(Fields.infoListOptionLabel, '');
    this.resetInfoListOptionsFormControl(Fields.infoListOptionCode, '');
    this.removeInfoListFormGroupClientSideError();
    this.textFields
      .filter((textField) => textField.fieldName === Fields.infoListOptionLabel)
      .forEach((textField) => textField.grabFocus());
  }

  private removeInfoListFormGroupClientSideError(): void {
    this.serverSideValidationErrors = [];
    this.cdr.markForCheck();
  }

  private initializeInfoListTable(): void {
    this.infoListOptionService.initialize();
    this.infoListOptions$ = this.infoListOptionService.infoListOptions$;
    this.gridService.connectToDatasource(this.infoListOptions$, 'id');
  }

  private initializeFormGroup(): void {
    this.formGroup = this.fb.group({
      label: this.fb.control('', [Validators.required, Validators.maxLength(100)]),
      code: this.fb.control('', [
        Validators.required,
        Validators.maxLength(30),
        this.codeValidator,
      ]),
      description: this.fb.control(''),
    });
  }

  get codeValidator(): ValidatorFn {
    return function (formControl: AbstractControl): ValidationErrors {
      const isCodeValid = isInfoListCodePatternValid(formControl.value);
      return isCodeValid ? null : { invalidCodePattern: true };
    };
  }

  private initializeInfoListOptionsFormGroup(): void {
    this.infoListOptionsFormGroup = this.fb.group({
      infoListOptionLabel: this.fb.control('', [Validators.maxLength(100), Validators.required]),
      infoListOptionCode: this.fb.control('', [
        Validators.maxLength(30),
        Validators.required,
        this.codeValidator,
      ]),
    });
  }

  protected getInfoListOptionsFormControl(fieldName: string): AbstractControl {
    return this.infoListOptionsFormGroup.controls[fieldName];
  }

  protected getInfoListOptionsFormControlValue(fieldName: string): any {
    return this.getInfoListOptionsFormControl(fieldName).value;
  }

  protected resetInfoListOptionsFormControl(fieldName: string, value: any): void {
    this.getInfoListOptionsFormControl(fieldName).reset(value);
  }
}
