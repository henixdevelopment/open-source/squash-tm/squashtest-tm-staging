import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BugtrackerGridComponent } from './bugtracker-grid.component';
import {
  DataRow,
  DialogService,
  GridService,
  gridServiceFactory,
  GridTestingModule,
  ReferentialDataService,
  RestService,
  WorkspaceWithGridComponent,
} from 'sqtm-core';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { adminProjectTableDefinition } from '../../../../project-workspace/project-workspace/containers/project-grid/project-grid.component';
import {
  ADMIN_WS_BUGTRACKERS_TABLE,
  ADMIN_WS_BUGTRACKERS_TABLE_CONFIG,
} from '../../../server-workspace.constant';
import { AppTestingUtilsModule } from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {
  mockClosableDialogService,
  mockGridService,
  mockRestService,
  mockRouter,
} from '../../../../../../utils/testing-utils/mocks.service';
import { of } from 'rxjs';
import { mockMouseEvent } from '../../../../../../utils/testing-utils/test-component-generator';
import { ActivatedRoute, Router } from '@angular/router';
import createSpyObj = jasmine.createSpyObj;

describe('BugtrackerGridComponent', () => {
  let component: BugtrackerGridComponent;
  let fixture: ComponentFixture<BugtrackerGridComponent>;

  const dialogMock = mockClosableDialogService();
  const gridService = mockGridService();
  const restService = mockRestService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [BugtrackerGridComponent],
      imports: [GridTestingModule, AppTestingUtilsModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: ADMIN_WS_BUGTRACKERS_TABLE_CONFIG,
          useFactory: adminProjectTableDefinition,
        },
        {
          provide: ADMIN_WS_BUGTRACKERS_TABLE,
          useFactory: gridServiceFactory,
          deps: [RestService, ADMIN_WS_BUGTRACKERS_TABLE_CONFIG, ReferentialDataService],
        },
        {
          provide: GridService,
          useValue: gridService,
        },
        {
          provide: DialogService,
          useValue: dialogMock.service,
        },
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: Router,
          useValue: mockRouter(),
        },
        {
          provide: WorkspaceWithGridComponent,
          useValue: {},
        },
        {
          provide: ActivatedRoute,
          useValue: { snapshot: { paramMap: createSpyObj<Map<any, any>>(['get']) } },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BugtrackerGridComponent);
    component = fixture.componentInstance;
    restService.get.and.returnValue(of({ bugtrackerKinds: [] }));
    fixture.detectChanges();

    restService.delete.calls.reset();
    dialogMock.resetSubjects();
    dialogMock.resetCalls();
    gridService.refreshData.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create', () => {
    component.openBugtrackerDialog();

    dialogMock.closeDialogsWithResult(true);

    expect(dialogMock.service.openDialog).toHaveBeenCalled();
    expect(gridService.refreshDataAsync).toHaveBeenCalled();
  });

  it('should delete bugtrackers', () => {
    gridService.selectedRows$ = of([
      { data: { id: 1, synchronisationCount: 0 } } as unknown as DataRow,
    ]);

    component.deleteBugtrackers(mockMouseEvent());

    dialogMock.closeDialogsWithResult(true);

    expect(dialogMock.service.openDeletionConfirm).toHaveBeenCalled();
    expect(restService.delete).toHaveBeenCalled();
    expect(gridService.refreshData).toHaveBeenCalled();
  });

  it('should forbid to delete bugtrackers used in synchronizations', () => {
    gridService.selectedRows$ = of([
      { data: { id: 1, synchronisationCount: 5 } } as unknown as DataRow,
    ]);

    component.deleteBugtrackers(mockMouseEvent());

    dialogMock.closeDialogsWithResult(true);

    expect(dialogMock.service.openAlert).toHaveBeenCalled();
    expect(restService.delete).not.toHaveBeenCalled();
    expect(gridService.refreshData).not.toHaveBeenCalled();
  });

  it('should fetch bugtracker kinds and map them to display options', () => {
    const bugtrackerKinds = ['jira.rest', 'jira.cloud', 'polarion', 'mantis'];
    restService.get.and.returnValue(of({ bugtrackerKinds }));
    component.getBugtrackerKinds();
    const options = bugtrackerKinds.map((kind) => ({ id: kind, label: kind })) as any;
    expect(component.bugtrackerKinds).toEqual(options);
  });
});
