import { administrationLogger } from '../administration.logger';

export const milestoneWorkspaceLogger = administrationLogger.compose('milestone');
