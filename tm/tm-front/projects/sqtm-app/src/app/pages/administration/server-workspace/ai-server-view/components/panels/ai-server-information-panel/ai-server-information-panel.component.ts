import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { AdminAiServerViewState } from '../../../states/admin-ai-server-view-state';

@Component({
  selector: 'sqtm-app-ai-server-information-panel',
  templateUrl: './ai-server-information-panel.component.html',
  styleUrl: './ai-server-information-panel.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AiServerInformationPanelComponent {
  @Input()
  date: string;

  @Input()
  userName: string;

  @Input()
  componentData: AdminAiServerViewState;

  @Output()
  urlChanged = new EventEmitter<string>();

  handleUrlChanged(serverUrl: string) {
    this.urlChanged.emit(serverUrl);
  }
}
