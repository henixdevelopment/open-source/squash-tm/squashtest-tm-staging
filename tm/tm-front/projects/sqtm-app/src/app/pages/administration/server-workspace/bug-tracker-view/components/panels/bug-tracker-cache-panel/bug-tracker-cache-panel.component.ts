import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  InjectionToken,
  Input,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import {
  AdminBugTrackerState,
  AdminBugTrackerViewState,
} from '../../../states/admin-bug-tracker-view-state';
import { map, take, takeUntil } from 'rxjs/operators';
import {
  AdminReferentialDataService,
  BugTrackerCacheInfoEntry,
  Credentials,
  dateTimeColumn,
  doesHttpErrorContainsSquashActionError,
  Extendable,
  extractSquashActionError,
  Fixed,
  GridColumnId,
  GridDefinition,
  GridService,
  gridServiceFactory,
  indexColumn,
  Limited,
  ReferentialDataService,
  RestService,
  smallGrid,
  Sort,
  StyleDefinitionBuilder,
  SynchronizationStatus,
  syncStatusColumn,
  textColumn,
  ThirdPartyCredentialsFormComponent,
} from 'sqtm-core';
import { Observable, of, Subject } from 'rxjs';
import { BugTrackerViewService } from '../../../services/bug-tracker-view.service';

const CACHE_INFO_GRID = new InjectionToken('CACHE_INFO_GRID');
const CACHE_INFO_GRID_CONF = new InjectionToken('CACHE_INFO_GRID_CONF');

function cacheInfoGrid(): GridDefinition {
  return smallGrid('bugtracker-cache-info')
    .withColumns([
      indexColumn().withViewport('leftViewport'),
      textColumn(GridColumnId.project)
        .withI18nKey(
          'sqtm-core.administration-workspace.bugtrackers.cache-configuration.cache-info.project',
        )
        .changeWidthCalculationStrategy(new Limited(400, 100, 1000)),
      dateTimeColumn(GridColumnId.lastSuccessfulUpdateDate)
        .withI18nKey(
          'sqtm-core.administration-workspace.bugtrackers.cache-configuration.cache-info.last-successful-update.short',
        )
        .withTitleI18nKey(
          'sqtm-core.administration-workspace.bugtrackers.cache-configuration.cache-info.last-successful-update.long',
        )
        .changeWidthCalculationStrategy(new Extendable(120)),
      dateTimeColumn(GridColumnId.lastUpdateDate)
        .withI18nKey(
          'sqtm-core.administration-workspace.bugtrackers.cache-configuration.cache-info.last-update',
        )
        .changeWidthCalculationStrategy(new Extendable(120)),
      syncStatusColumn(GridColumnId.status)
        .withI18nKey('sqtm-core.generic.label.status')
        .withViewport('rightViewport')
        .withHeaderPosition('center')
        .withContentPosition('center')
        .changeWidthCalculationStrategy(new Fixed(80)),
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withInitialSortedColumns([
      {
        id: GridColumnId.status,
        sort: Sort.ASC,
      },
      {
        id: GridColumnId.lastSyncDate,
        sort: Sort.DESC,
      },
    ])
    .withRowHeight(35)
    .build();
}

@Component({
  selector: 'sqtm-app-bug-tracker-reporting-cache-panel',
  templateUrl: './bug-tracker-cache-panel.component.html',
  styleUrl: './bug-tracker-cache-panel.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: CACHE_INFO_GRID_CONF,
      useFactory: cacheInfoGrid,
    },
    {
      provide: CACHE_INFO_GRID,
      useFactory: gridServiceFactory,
      deps: [RestService, CACHE_INFO_GRID_CONF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: CACHE_INFO_GRID,
    },
  ],
})
export class BugTrackerCachePanelComponent implements AfterViewInit, OnDestroy {
  @Input()
  componentData: AdminBugTrackerViewState;

  @ViewChild(ThirdPartyCredentialsFormComponent)
  credentialsForm: ThirdPartyCredentialsFormComponent;

  loginFieldKey$: Observable<string>;
  passwordFieldKey$: Observable<string>;

  credentialsStatusMessage = '';
  statusIcon: 'DANGER' | 'WARNING' | 'INFO' = null;

  private readonly unsub$ = new Subject<void>();

  constructor(
    private readonly adminRefDataService: AdminReferentialDataService,
    private readonly bugTrackerViewService: BugTrackerViewService,
    private readonly gridService: GridService,
  ) {
    this.prepareLoginAndPasswordKeys();
    this.initialiseCacheMessage();
  }

  ngAfterViewInit() {
    this.initialiseGrid();
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  prepareLoginAndPasswordKeys(): void {
    const selectedBt$ = this.adminRefDataService.bugTrackers$.pipe(
      takeUntil(this.unsub$),
      map((bugtrackers) => bugtrackers.find((bt) => bt.id === this.componentData.bugTracker.id)),
    );
    this.loginFieldKey$ = selectedBt$.pipe(map((bt) => bt?.loginFieldKey));
    this.passwordFieldKey$ = selectedBt$.pipe(map((bt) => bt?.passwordFieldKey));
  }

  requestAndSaveOauth2Token() {
    throw new Error('Method not implemented.');
  }

  sendCredentialsForm(credentials: Credentials) {
    this.clearMessage();
    this.bugTrackerViewService.setCacheCredentials(credentials).subscribe({
      next: () => this.handleCredentialsSaveSuccess(),
      error: (err) => this.handleCredentialsError(err),
    });
  }

  private handleCredentialsSaveSuccess(): void {
    this.credentialsForm.formGroup.markAsPristine();
    this.credentialsForm.endAsync();
    this.credentialsStatusMessage =
      'sqtm-core.administration-workspace.bugtrackers.authentication-policy.credentials.save-success';
    this.statusIcon = 'INFO';
  }

  private handleCredentialsError(error: any): void {
    this.credentialsForm.endAsync();

    if (doesHttpErrorContainsSquashActionError(error)) {
      const squashError = extractSquashActionError(error);
      this.credentialsStatusMessage = squashError.actionValidationError.i18nKey;
      this.statusIcon = 'DANGER';
    } else {
      // Default error handling
      console.error(error);
    }
  }

  private clearMessage(): void {
    this.credentialsStatusMessage = undefined;
    this.statusIcon = undefined;
  }

  deleteCredentials(bugTracker: AdminBugTrackerState) {
    this.bugTrackerViewService
      .deleteCacheCredentials(bugTracker.id)
      .subscribe(() => this.clearMessage());
  }

  private initialiseCacheMessage() {
    this.bugTrackerViewService.componentData$
      .pipe(
        take(1),
        map((componentData) => componentData.bugTracker),
      )
      .subscribe((bugTracker) => {
        const { hasCacheCredentialsError, allowsReportingCache, reportingCacheCredentials } =
          bugTracker;

        if (!allowsReportingCache) {
          return;
        }

        if (hasCacheCredentialsError) {
          this.credentialsStatusMessage =
            'sqtm-core.administration-workspace.bugtrackers.cache-configuration.cache-credentials-error-message';
          this.statusIcon = 'DANGER';
        }

        if (reportingCacheCredentials == null) {
          this.credentialsStatusMessage =
            'sqtm-core.administration-workspace.bugtrackers.cache-configuration.no-cache-credentials-message';
          this.statusIcon = 'WARNING';
        }
      });
  }

  getCacheInfoEntries(): BugTrackerCacheInfoEntry[] {
    return (
      this.componentData.bugTracker.cacheInfo?.entries.filter(
        (entry) => entry.bugTrackerId === this.componentData.bugTracker.id,
      ) ?? []
    );
  }

  private initialiseGrid() {
    const dataSource$ = of(
      this.getCacheInfoEntries().map((entry) => ({
        [GridColumnId.project]: entry.projectNameOrPath,
        [GridColumnId.lastUpdateDate]: entry.lastUpdate,
        [GridColumnId.lastSuccessfulUpdateDate]: entry.lastSuccessfulUpdate,
        [GridColumnId.status]: this.getStatus(entry),
      })),
    );
    this.gridService.connectToDatasource(dataSource$, GridColumnId.project);
  }

  private getStatus(entry: BugTrackerCacheInfoEntry): string {
    if (entry.lastUpdate == null) {
      return SynchronizationStatus.NEVER_EXECUTED;
    } else {
      return entry.hasError ? SynchronizationStatus.FAILURE : SynchronizationStatus.SUCCESS;
    }
  }
}
