import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  Signal,
  ViewContainerRef,
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil, tap, withLatestFrom } from 'rxjs/operators';
import { ProfileViewService } from '../../../services/profile-view.service';
import {
  AdminReferentialDataService,
  DialogService,
  GenericEntityViewComponentData,
  gridServiceFactory,
  ProfileView,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import { AdminProfileState } from '../../../states/admin-profile-state';
import { toSignal } from '@angular/core/rxjs-interop';
import { TranslateService } from '@ngx-translate/core';
import {
  PROFILE_AUTHORIZATIONS_TABLE,
  PROFILE_AUTHORIZATIONS_TABLE_CONF,
} from '../../../../profile-workspace.constant';
import { profileAuthorizationTableDefinition } from '../../../components/profile-authorizations-panel/profile-authorizations-panel.component';
import { TransferAuthorizationsDialogComponent } from '../../../../profile-workspace/components/dialogs/transfer-authorizations-dialog/transfer-authorizations-dialog.component';
import { Router } from '@angular/router';

@Component({
  selector: 'sqtm-app-profile-content',
  templateUrl: './profile-content.component.html',
  styleUrls: ['./profile-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: PROFILE_AUTHORIZATIONS_TABLE_CONF,
      useFactory: profileAuthorizationTableDefinition,
      deps: [TranslateService],
    },
    {
      provide: PROFILE_AUTHORIZATIONS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, PROFILE_AUTHORIZATIONS_TABLE_CONF, ReferentialDataService],
    },
  ],
})
export class ProfileContentComponent implements OnInit, OnDestroy {
  componentData$: Observable<AdminProfileViewComponentData>;
  unsub$ = new Subject<void>();
  readonly $isUltimate: Signal<boolean>;

  constructor(
    public profileViewService: ProfileViewService,
    private adminReferentialDataService: AdminReferentialDataService,
    private dialogService: DialogService,
    private vcr: ViewContainerRef,
    private router: Router,
  ) {
    this.$isUltimate = toSignal(this.adminReferentialDataService.isUltimateLicenseAvailable$);
  }

  ngOnInit(): void {
    this.componentData$ = this.profileViewService.componentData$.pipe(takeUntil(this.unsub$));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  openTransferProfileAuthorizations($event: MouseEvent, profile: ProfileView) {
    $event.stopPropagation();
    $event.preventDefault();

    const dialogRef = this.dialogService.openDialog({
      id: 'transfer-authorizations-dialog',
      component: TransferAuthorizationsDialogComponent,
      viewContainerReference: this.vcr,
      data: {
        profile: profile,
      },
      maxWidth: 500,
    });

    dialogRef.dialogClosed$
      .pipe(
        takeUntil(this.unsub$),
        withLatestFrom(dialogRef.dialogResultChanged$),
        tap(([_, result]: [any, number]) => {
          this.router
            .navigate(['administration-workspace', 'profiles', 'manage', result, 'content'], {
              queryParams: { anchor: 'authorizations' },
            })
            .then(() => {});
        }),
      )
      .subscribe();
  }
}

export interface AdminProfileViewComponentData
  extends GenericEntityViewComponentData<AdminProfileState, 'profile'> {}
