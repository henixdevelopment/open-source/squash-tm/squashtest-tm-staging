import { BindableProject } from './bind-projects-to-milestone-dialog.component';

export interface BindProjectsToMilestoneDialogConfiguration {
  bindableProjects: BindableProject[];
}
