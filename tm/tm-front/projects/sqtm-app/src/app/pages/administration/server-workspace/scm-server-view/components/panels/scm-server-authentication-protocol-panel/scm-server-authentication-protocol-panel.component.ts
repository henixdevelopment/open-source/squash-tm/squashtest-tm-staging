import { ChangeDetectionStrategy, Component, Input, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthenticationProtocol } from 'sqtm-core';
import {
  AuthProtocolFormComponent,
  AuthProtocolFormData,
} from '../../../../components/auth-protocol-form/auth-protocol-form.component';
import { ScmServerViewService } from '../../../services/scm-server-view.service';
import { AdminScmServerViewState } from '../../../states/admin-scm-server-view-state';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'sqtm-app-scm-server-authentication-protocol-panel',
  templateUrl: './scm-server-authentication-protocol-panel.component.html',
  styleUrls: ['./scm-server-authentication-protocol-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScmServerAuthenticationProtocolPanelComponent implements OnInit {
  @Input()
  componentData: AdminScmServerViewState;

  @ViewChild(AuthProtocolFormComponent)
  authProtocolForm: AuthProtocolFormComponent;

  authProtocolFormData$: Observable<AuthProtocolFormData>;

  constructor(
    public readonly translateService: TranslateService,
    public readonly scmServerViewService: ScmServerViewService,
  ) {}

  ngOnInit(): void {
    this.authProtocolFormData$ = this.getAuthProtocolFormData();
  }

  handleProtocolChange(protocol: AuthenticationProtocol): void {
    this.scmServerViewService.setAuthenticationProtocol(protocol).subscribe(
      () =>
        (this.authProtocolForm.authConfigurationFormData = {
          authenticationProtocol: protocol,
          authConfiguration: null,
          defaultUrl: this.componentData.scmServer.url,
        }),
    );
  }

  getAuthProtocolFormData(): Observable<AuthProtocolFormData> {
    return of({
      authenticationProtocol: this.componentData.scmServer.authProtocol,
      authConfiguration: null,
      defaultUrl: this.componentData.scmServer.url,
    });
  }
}
