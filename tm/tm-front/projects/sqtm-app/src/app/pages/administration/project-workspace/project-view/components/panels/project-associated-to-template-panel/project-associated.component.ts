import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import {
  Extendable,
  GridColumnId,
  GridDefinition,
  indexColumn,
  smallGrid,
  Sort,
  StyleDefinitionBuilder,
  GridService,
  withLinkColumn,
  DataRow,
  gridServiceFactory,
  RestService,
  ReferentialDataService,
} from 'sqtm-core';
import {
  PROJECT_ASSOCIATED_TEMPLATE_TABLE,
  PROJECT_ASSOCIATED_TEMPLATE_TABLE_CONF,
} from '../../../project-view.constant';
import { ProjectViewService } from '../../../services/project-view.service';
import { Subject } from 'rxjs';
import { map, take } from 'rxjs/operators';

export function projectAssociatedWithTemplateTableDefinition(): GridDefinition {
  const urlFunction = (row: DataRow) => {
    return [
      '/',
      'administration-workspace',
      'projects',
      'detail',
      row.data.id.toString(),
      'content',
    ];
  };

  return smallGrid('project-associated-template')
    .withColumns([
      indexColumn().withViewport('leftViewport'),
      withLinkColumn(GridColumnId.name, {
        kind: 'link',
        createUrlFunction: urlFunction,
        saveGridStateBeforeNavigate: true,
      })
        .withI18nKey('sqtm-core.custom-report-workspace.custom-export.columns.TEST_CASE_PROJECT')
        .changeWidthCalculationStrategy(new Extendable(100, 0.2)),
    ])
    .withInitialSortedColumns([{ id: GridColumnId.name, sort: Sort.ASC }])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .build();
}

@Component({
  selector: 'sqtm-app-project-associated',
  template: `<sqtm-core-grid></sqtm-core-grid>`,
  styleUrls: ['./project-associated.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: PROJECT_ASSOCIATED_TEMPLATE_TABLE_CONF,
      useFactory: projectAssociatedWithTemplateTableDefinition,
      deps: [],
    },
    {
      provide: PROJECT_ASSOCIATED_TEMPLATE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, PROJECT_ASSOCIATED_TEMPLATE_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: PROJECT_ASSOCIATED_TEMPLATE_TABLE,
    },
  ],
})
export class ProjectAssociatedComponent implements OnInit, OnDestroy {
  constructor(
    private projectViewService: ProjectViewService,
    private gridService: GridService,
  ) {}

  private unsub$ = new Subject<void>();

  ngOnInit(): void {
    this.initializeTable();
  }

  private initializeTable() {
    const boundProjectToTemplate = this.projectViewService.componentData$.pipe(
      take(1),
      map((componentData) => componentData.project.allProjectBoundToTemplate),
    );
    this.gridService.connectToDatasource(boundProjectToTemplate, 'id');
  }

  ngOnDestroy(): void {
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }
}
