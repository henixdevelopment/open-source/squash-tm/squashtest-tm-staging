import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  getEvInputTypeI18n,
  getInputTypeI18n,
  GridColumnId,
  GridService,
  GridState,
} from 'sqtm-core';
import { map, take } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-input-type-cell-renderer',
  template: `
    @if (columnDisplay && row) {
      <div class="full-width full-height flex-column">
        @if (row.data[columnDisplay.id]) {
          <span
            class="sqtm-grid-cell-txt-renderer m-auto-0"
            nz-tooltip
            [sqtmCoreLabelTooltip]="inputType | translate"
          >
            {{ inputType | translate }}
          </span>
        }
      </div>
    }
  `,
  styleUrls: ['./input-type-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InputTypeCellRendererComponent extends AbstractCellRendererComponent {
  isEnvironmentVariableGrid: boolean;
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
    this.initializeIsEnvironmentVariableGrid();
  }

  private initializeIsEnvironmentVariableGrid() {
    this.grid.gridState$
      .pipe(
        take(1),
        map((state: GridState) => state.definitionState.id),
      )
      .subscribe((id: string) => (this.isEnvironmentVariableGrid = id === 'environmentVariables'));
  }

  get inputType(): string {
    const inputType = this.row.data[this.columnDisplay.id];
    if (this.isEnvironmentVariableGrid) {
      return getEvInputTypeI18n(inputType);
    }
    return getInputTypeI18n(inputType);
  }
}

export function inputTypeColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(InputTypeCellRendererComponent);
}
