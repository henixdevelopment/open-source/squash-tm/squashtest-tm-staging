import {
  CreationDialogData,
  DatePickerComponent,
  DialogReference,
  FieldValidationError,
  RestService,
  RichTextFieldComponent,
  SelectFieldComponent,
} from 'sqtm-core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { TranslateService } from '@ngx-translate/core';
import { of, throwError } from 'rxjs';
import { AbstractAdministrationCreationDialogDirective } from './abstract-administration-creation-dialog';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  NO_ERRORS_SCHEMA,
  OnInit,
} from '@angular/core';
import { makeHttpFieldValidationError } from '../../../utils/testing-utils/test-error-response-generator';
import { mockTextField } from '../../../utils/testing-utils/test-component-generator';
import { mockRestService } from '../../../utils/testing-utils/mocks.service';
import SpyObj = jasmine.SpyObj;

@Component({
  selector: 'sqtm-app-test-creation-dialog',
  template: ` <sqtm-core-creation-dialog
    (add)="addEntity()"
    (addAnother)="addAnother()"
    [attr.data-test-dialog-id]="'add-info-list-item'"
  >
    <div class="m-10" [formGroup]="formGroup">
      <div class="sqtm-core-dialog-form">
        <label class="label label__align_right">name</label>
        <sqtm-core-text-field
          [formGroup]="formGroup"
          [fieldName]="'name'"
          [serverSideFieldValidationError]="serverSideValidationErrors"
        >
        </sqtm-core-text-field>
      </div>
    </div>
  </sqtm-core-creation-dialog>`,
  styleUrls: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
class TestCreationDialogComponent
  extends AbstractAdministrationCreationDialogDirective
  implements OnInit
{
  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  data: CreationDialogData;

  constructor(
    private fb: FormBuilder,
    private translateService: TranslateService,
    dialogReference: DialogReference,
    restService: RestService,
    cdr: ChangeDetectorRef,
  ) {
    super('test', dialogReference, restService, cdr);
  }

  get textFieldToFocus(): string {
    return 'name';
  }

  ngOnInit() {
    this.initializeFormGroup();
  }

  protected getRequestPayload() {
    return of({
      name: this.getFormControlValue('name'),
    });
  }

  protected doResetForm() {
    this.resetFormControl('name', '');
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      name: '',
    });
  }
}

describe('AbstractAdministrationCreationDialogDirective', () => {
  let component: TestCreationDialogComponent;
  let fixture: ComponentFixture<TestCreationDialogComponent>;
  let restService: SpyObj<RestService>;
  let dialogReference: SpyObj<DialogReference>;

  beforeEach(waitForAsync(() => {
    restService = mockRestService();
    dialogReference = jasmine.createSpyObj(['close']);

    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [TestCreationDialogComponent],
      providers: [
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj(['instant']),
        },
        {
          provide: DialogReference,
          useValue: dialogReference,
        },
        {
          provide: RestService,
          useValue: restService,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(waitForAsync(() => {
    fixture = TestBed.createComponent(TestCreationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not send request if form is not valid', () => {
    expect(component).toBeTruthy();
    component.formGroup.setErrors({ someError: 'BOOM' });

    component.addEntity();

    expect(restService.post).not.toHaveBeenCalled();
  });

  it('should send creation request and close dialog', () => {
    component.addEntity(false);

    expect(restService.post).toHaveBeenCalled();
    expect(dialogReference.close).toHaveBeenCalled();
  });

  it('should send creation request, keep dialog open and reset focus', () => {
    const textField = mockTextField('name', component.formGroup);
    component.textFields.reset([textField]);

    component.addAnother();

    expect(restService.post).toHaveBeenCalled();
    expect(dialogReference.close).not.toHaveBeenCalled();
    expect(textField.grabFocus).toHaveBeenCalled();
  });

  it('should show server-side field validation errors', () => {
    const fve: FieldValidationError = {
      fieldValue: '',
      messageArgs: [],
      objectName: '',
      fieldName: 'name',
      i18nKey: 'badaboom',
    };

    restService.post.and.returnValue(throwError(() => makeHttpFieldValidationError([fve])));

    component.addAnother();

    expect(dialogReference.close).not.toHaveBeenCalled();
    expect(component.serverSideValidationErrors).toEqual([fve]);
  });

  it('should show client side errors', () => {
    const textField = mockTextField('name', component.formGroup);
    component.textFields.reset([textField]);

    const richTextField = mockTextField(
      'name',
      component.formGroup,
    ) as unknown as RichTextFieldComponent;
    component.richTextFields.reset([richTextField]);

    const dateField = mockTextField('name', component.formGroup) as unknown as DatePickerComponent;
    component.dateFields.reset([dateField]);

    const selectField = mockTextField(
      'name',
      component.formGroup,
    ) as unknown as SelectFieldComponent;
    component.selectFields.reset([selectField]);

    component.formGroup.setErrors({
      name: 'required',
    });

    component.addAnother();

    expect(textField.showClientSideError).toHaveBeenCalled();
    expect(richTextField.showClientSideError).toHaveBeenCalled();
    expect(dateField.showClientSideError).toHaveBeenCalled();
    expect(selectField.showClientSideError).toHaveBeenCalled();
  });
});
