import { InjectionToken } from '@angular/core';
import { GridDefinition, GridService } from 'sqtm-core';

export const ADMIN_WS_MILESTONE_TABLE_CONFIG = new InjectionToken<GridDefinition>(
  'Grid config instance for the table of milestones in admin workspace',
);
export const ADMIN_WS_MILESTONE_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the table of milestones in admin workspace',
);
