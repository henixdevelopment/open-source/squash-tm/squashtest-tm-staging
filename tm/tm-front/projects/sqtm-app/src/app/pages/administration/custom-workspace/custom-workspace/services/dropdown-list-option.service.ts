import { Injectable } from '@angular/core';
import { createStore, Identifier, Store } from 'sqtm-core';
import { Observable } from 'rxjs';
import { map, take, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class DropdownListOptionService {
  public readonly store: Store<DropdownListOptionState>;
  public readonly state$: Observable<DropdownListOptionState>;
  public readonly dropdownListOptions$: Observable<DropdownListOption[]>;

  constructor() {
    this.store = createStore<DropdownListOptionState>(getInitialDropdownListOptionState());
    this.state$ = this.store.state$;
    this.dropdownListOptions$ = this.store.state$.pipe(map((state) => state.dropdownListOptions));
  }

  initialize() {
    this.store.commit(getInitialDropdownListOptionState());
  }

  addOption(optionName: string, optionCode: string) {
    this.store.state$
      .pipe(
        take(1),
        map((state) => {
          const updateDropdownListOptions: DropdownListOption[] = [...state.dropdownListOptions];
          const option: DropdownListOption = {
            id: optionName.toString(),
            optionName: optionName,
            optionCode: optionCode,
            default: false,
            color: null,
          };

          updateDropdownListOptions.push(option);

          return {
            ...state,
            dropdownListOptions: updateDropdownListOptions,
          };
        }),
      )
      .subscribe((state) => {
        this.store.commit(state);
      });
  }

  removeOption(optionName: string): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      map((state) => {
        const updateDropdownListOptions = [...state.dropdownListOptions].filter(
          (option) => option.optionName !== optionName,
        );

        return {
          ...state,
          dropdownListOptions: updateDropdownListOptions,
        };
      }),
      tap((state) => this.store.commit(state)),
    );
  }

  toggleDefault(optionName: string) {
    this.store.state$
      .pipe(
        take(1),
        map((state) => {
          const updateDropdownListOptions = [...state.dropdownListOptions].map(
            (option: DropdownListOption) => {
              const isSelectedOption = option.optionName === optionName;
              const isCurrentDefault = option.default;

              return {
                ...option,
                default: isCurrentDefault ? false : isSelectedOption,
              };
            },
          );

          return {
            ...state,
            dropdownListOptions: updateDropdownListOptions,
          };
        }),
      )
      .subscribe((state) => {
        this.store.commit(state);
      });
  }

  changeColor(optionName: string, newColor: string) {
    this.store.state$
      .pipe(
        take(1),
        map((state: DropdownListOptionState) => {
          const updateDropdownListOptions = [...state.dropdownListOptions].map(
            (option: DropdownListOption) => {
              const isSelected = option.optionName === optionName;
              return {
                ...option,
                color: isSelected ? newColor : option.color,
              };
            },
          );
          return {
            ...state,
            dropdownListOptions: updateDropdownListOptions,
          };
        }),
      )
      .subscribe((state) => {
        this.store.commit(state);
      });
  }
}

interface DropdownListOptionState {
  dropdownListOptions: DropdownListOption[];
}

export interface DropdownListOption {
  id: Identifier;
  optionName: string;
  optionCode: string;
  default: boolean;
  color: string;
}

function getInitialDropdownListOptionState(): DropdownListOptionState {
  return {
    dropdownListOptions: [],
  };
}
