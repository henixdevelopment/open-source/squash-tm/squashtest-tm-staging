import { Injectable } from '@angular/core';
import {
  AclGroup,
  DataRow,
  getAclGroupI18nKey,
  GridService,
  Profile,
  ProfilesAndPermissions,
  ProfileView,
  RestService,
} from 'sqtm-core';
import { Observable, switchMap } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class ProfileService {
  constructor(
    private restService: RestService,
    private gridService: GridService,
    private translateService: TranslateService,
  ) {}

  public getDataRowProfiles(profileId?: number): Observable<DataRow[]> {
    return this.restService.get(['profiles']).pipe(
      map((response: { profiles: ProfileView[] }) => {
        return response.profiles.map((profile) => {
          return this.transformProfileToDataRow(profile);
        });
      }),
      tap((dataRows) => {
        if (profileId) {
          this.gridService.loadInitialDataRows(dataRows, dataRows.length, [profileId]);
        } else {
          const url = window.location.href;
          const regex = /profiles\/manage\/(\d+)\//;
          const match = url.match(regex);

          if (match) {
            const id = parseInt(match[1]);
            this.gridService.loadInitialDataRows(dataRows, dataRows.length, [id]);
          } else {
            this.gridService.loadInitialDataRows(dataRows, dataRows.length);
          }
        }
      }),
    );
  }

  public getProfiles(): Observable<ProfileView[]> {
    return this.restService.get(['profiles']).pipe(
      map((response: { profiles: ProfileView[] }) => {
        return response.profiles.map((profile) => {
          return this.populateProfileName(profile);
        });
      }),
    );
  }

  public getAllPermissions(): Observable<ProfilesAndPermissions[]> {
    return this.restService.get(['profiles', 'all-permissions']);
  }

  public deleteProfile(profileId: string): Observable<void> {
    return this.restService.delete(['profiles', profileId]);
  }

  public retrieveProfilesAsDisplayOptions(profiles: Profile[]) {
    return profiles.map((ref) => ({
      id: ref.id,
      label: this.getProfileName(ref),
    }));
  }

  private populateProfileName(profile: ProfileView): ProfileView {
    profile.name = this.getProfileName(profile);
    return profile;
  }

  private transformProfileToDataRow(profile: ProfileView): DataRow {
    const transformedProfile = { ...profile };

    transformedProfile.name = this.getProfileName(profile);

    return { id: transformedProfile.id, data: { ...transformedProfile } } as unknown as DataRow;
  }

  public getProfileName(profile: Profile): string {
    return profile.system
      ? this.translateService.instant(getAclGroupI18nKey(profile.qualifiedName as AclGroup))
      : profile.qualifiedName;
  }

  public transferProfileAuthorizations(
    profileId: string,
    targetProfileId: string,
  ): Observable<DataRow<any>[]> {
    return this.restService
      .post(['profiles', 'transfer-authorizations', profileId, targetProfileId])
      .pipe(switchMap(() => this.getDataRowProfiles(Number(targetProfileId))));
  }
}
