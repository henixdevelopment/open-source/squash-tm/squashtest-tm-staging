import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  AdminReferentialDataService,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-server-link-for-admin-cell',
  template: `
    @if (adminReferentialDataService.loggedAsAdmin$ | async) {
      @if (row) {
        <div class="full-width full-height flex-column">
          <a
            [routerLink]="getUrl()"
            class="sqtm-grid-cell-txt-renderer m-auto-0"
            nz-tooltip
            [sqtmCoreLabelTooltip]="row.data[columnDisplay.id]"
            [nzTooltipTitle]=""
            [class.disabled-row]="row.disabled"
            target="_blank"
            >{{ row.data[columnDisplay.id] }}</a
          >
        </div>
      }
    } @else {
      <div class="full-width full-height flex-column">
        <span
          class="sqtm-grid-cell-txt-renderer m-auto-0"
          nz-tooltip
          [sqtmCoreLabelTooltip]="row.data[columnDisplay.id]"
          [class.disabled-row]="row.disabled"
          [nzTooltipTitle]=""
          [nzTooltipPlacement]="'topLeft'"
          >{{ row.data[columnDisplay.id] }}</span
        >
      </div>
    }
  `,
  styleUrls: ['./server-link-cell-for-admin.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServerLinkCellForAdminComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    public adminReferentialDataService: AdminReferentialDataService,
  ) {
    super(grid, cdRef);
  }

  getUrl() {
    return [
      '/',
      'administration-workspace',
      'servers',
      'bugtrackers',
      this.row.data.serverId.toString(),
      'content',
    ];
  }
}

export function withServerLinkColumnForAdmin(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(ServerLinkCellForAdminComponent);
}
