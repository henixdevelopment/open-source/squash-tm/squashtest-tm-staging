import {
  customFieldBoundEntityColumn,
  DataRow,
  Extendable,
  GridColumnId,
  GridDefinition,
  GridService,
  gridServiceFactory,
  indexColumn,
  ReferentialDataService,
  RestService,
  smallGrid,
  Sort,
  StyleDefinitionBuilder,
  withLinkColumn,
} from 'sqtm-core';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import {
  PROJECT_BOUND_TO_CUF,
  PROJECT_BOUND_TO_CUF_TABLE,
} from '../../states/admin-custom-field-view-state';
import { CustomFieldViewService } from '../../services/custom-field-view.service';
import { map, take } from 'rxjs/operators';

export function projectAssociatedWithCustomField(): GridDefinition {
  const urlFunction = (row: DataRow) => {
    return [
      '/',
      'administration-workspace',
      'projects',
      'detail',
      row.data.projectId.toString(),
      'custom',
    ];
  };

  return smallGrid('projects-bound-to-cuf')
    .withColumns([
      indexColumn().withViewport('leftViewport'),
      withLinkColumn(GridColumnId.projectName, {
        kind: 'link',
        createUrlFunction: urlFunction,
        saveGridStateBeforeNavigate: false,
      })
        .withI18nKey('sqtm-core.custom-report-workspace.custom-export.columns.TEST_CASE_PROJECT')
        .changeWidthCalculationStrategy(new Extendable(100, 0.2)),
      customFieldBoundEntityColumn(GridColumnId.bindableEntity).changeWidthCalculationStrategy(
        new Extendable(100, 0.2),
      ),
    ])
    .withInitialSortedColumns([{ id: GridColumnId.projectName, sort: Sort.ASC }])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .build();
}

@Component({
  selector: 'sqtm-app-projects-bound-to-cuf',
  template: `<sqtm-core-grid></sqtm-core-grid>`,
  styleUrls: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: PROJECT_BOUND_TO_CUF,
      useFactory: projectAssociatedWithCustomField,
      deps: [],
    },
    {
      provide: PROJECT_BOUND_TO_CUF_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, PROJECT_BOUND_TO_CUF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: PROJECT_BOUND_TO_CUF_TABLE,
    },
  ],
})
export class ProjectsBoundToCufComponent implements OnInit {
  constructor(
    private gridService: GridService,
    private cufViewService: CustomFieldViewService,
  ) {}

  ngOnInit(): void {
    this.initializeTable();
  }

  private initializeTable() {
    const boundProjectsToCuf = this.cufViewService.componentData$.pipe(
      take(1),
      map((componentData) => componentData.customField.boundProjectsToCuf),
    );
    this.gridService.connectToDatasource(boundProjectsToCuf, 'customFieldBindingId');
  }
}
