import { TestBed } from '@angular/core/testing';

import { MilestoneViewService } from './milestone-view.service';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  MilestoneAdminView,
  ProjectInfoForMilestoneAdminView,
  RestService,
  MilestonePossibleOwner,
} from 'sqtm-core';
import { of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppTestingUtilsModule } from '../../../../../utils/testing-utils/app-testing-utils.module';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { mockRestService } from '../../../../../utils/testing-utils/mocks.service';
import SpyObj = jasmine.SpyObj;

describe('MilestoneViewService', () => {
  const restService = mockRestService();
  const adminReferentialDataService: SpyObj<AdminReferentialDataService> =
    jasmine.createSpyObj<AdminReferentialDataService>(['refresh']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        AppTestingUtilsModule,
        TranslateModule.forRoot(),
        RouterTestingModule,
      ],
      providers: [
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: MilestoneViewService,
          useClass: MilestoneViewService,
        },
        {
          provide: AdminReferentialDataService,
          useValue: adminReferentialDataService,
        },
      ],
    });
  });

  it('should load a milestone', () => {
    restService.getWithoutErrorHandling.and.returnValue(of(getInitialModel()));
    const service: MilestoneViewService = TestBed.inject(MilestoneViewService);

    service.componentData$.subscribe((data) => {
      expect(data.milestone.id).toEqual(1);
    });

    expect(service).toBeTruthy();

    service.load(1);
  });

  describe('Permissions evaluation', () => {
    it(`should evaluate if user "jdoe" is milestone's owner`, async () => {
      restService.getWithoutErrorHandling.and.returnValue(of(getInitialModel()));
      adminReferentialDataService.authenticatedUser$ = of(getProjectManager('jdoe'));
      const service: MilestoneViewService = TestBed.inject(MilestoneViewService);

      service.isMilestoneOwner$.subscribe((isOwner) => {
        expect(isOwner).toBeTruthy();
      });

      service.load(1);
    });

    it(`should evaluate if user "jane.doe" is milestone's owner`, async () => {
      restService.getWithoutErrorHandling.and.returnValue(of(getInitialModel()));
      adminReferentialDataService.authenticatedUser$ = of(getProjectManager('jane.doe'));
      const service: MilestoneViewService = TestBed.inject(MilestoneViewService);

      service.isMilestoneOwner$.subscribe((isOwner) => {
        expect(isOwner).toBeFalsy();
      });

      service.load(1);
    });

    const usersTestData = [
      { login: 'jdoe', admin: false, range: 'RESTRICTED', canEdit: true },
      { login: 'jane.doe', admin: false, range: 'RESTRICTED', canEdit: false },
      { login: 'admin', admin: true, range: 'RESTRICTED', canEdit: true },
      { login: 'jdoe', admin: false, range: 'GLOBAL', canEdit: false },
      { login: 'admin', admin: true, range: 'GLOBAL', canEdit: false },
    ];

    usersTestData.forEach((testSet) => {
      it(`should evaluate if user "${testSet.login}" can edit owner field (${testSet.range})`, async () => {
        const milestone = getInitialModel();
        milestone.range = testSet.range;

        const user = getProjectManager(testSet.login);
        if (testSet.admin) {
          user.admin = true;
        }

        restService.getWithoutErrorHandling.and.returnValue(of(milestone));
        adminReferentialDataService.authenticatedUser$ = of(getProjectManager(testSet.login));
        adminReferentialDataService.loggedAsAdmin$ = of(testSet.admin);
        const service: MilestoneViewService = TestBed.inject(MilestoneViewService);

        service.canEditOwnerField$.subscribe((canEdit) => {
          expect(canEdit).toBe(testSet.canEdit);
        });

        service.load(1);
      });
    });
  });

  it('should fetch the list of possible owners', async () => {
    const service: MilestoneViewService = TestBed.inject(MilestoneViewService);

    const milestone = getInitialModel();
    restService.getWithoutErrorHandling.and.returnValue(of(milestone));
    service.load(1);

    const usersResponse = [
      { id: -1, login: 'jdoe' } as MilestonePossibleOwner,
      { id: -2, login: 'jane.doe' } as MilestonePossibleOwner,
    ];
    restService.get.and.returnValue(of(usersResponse));

    service.findPossibleOwners().subscribe((users) => {
      expect(users).toEqual(usersResponse);
    });
  });

  it('should change owner', async () => {
    const service: MilestoneViewService = TestBed.inject(MilestoneViewService);

    const milestone = getInitialModel();
    restService.getWithoutErrorHandling.and.returnValue(of(milestone));
    service.load(1);

    const changeOwnerResponse = {
      ...milestone,
      ownerLogin: 'newOwner',
    };
    restService.post.and.returnValue(of(changeOwnerResponse));

    service.setOwner('newOwner').subscribe(() => {
      service.componentData$.subscribe((componentData) => {
        expect(componentData.milestone.ownerLogin).toEqual(changeOwnerResponse.ownerLogin);
      });
    });
  });

  it('should change range', async () => {
    const service: MilestoneViewService = TestBed.inject(MilestoneViewService);

    const milestone = getInitialModel();
    restService.getWithoutErrorHandling.and.returnValue(of(milestone));
    service.load(1);

    const changeRangeResponse = {
      ...milestone,
      range: 'RESTRICTED',
    };
    restService.post.and.returnValue(of(changeRangeResponse));

    service.setRange('RESTRICTED').subscribe(() => {
      service.componentData$.subscribe((componentData) => {
        expect(componentData.milestone.range).toEqual(changeRangeResponse.range);
      });
    });
  });

  describe('Milestone-Project bindings', () => {
    function getProjectBindingInfos(id: number): ProjectInfoForMilestoneAdminView {
      return {
        projectId: id,
        projectName: 'project' + id,
        boundToMilestone: true,
        milestoneBoundToOneObjectOfProject: true,
        template: false,
      };
    }

    it('should bind to projects', async () => {
      const service: MilestoneViewService = TestBed.inject(MilestoneViewService);
      const unboundProjectInfo = getProjectBindingInfos(1);
      const unknownProjectInfo = getProjectBindingInfos(2);

      const milestone = getInitialModel();
      milestone.boundProjectsInformation = [unboundProjectInfo];

      restService.getWithoutErrorHandling.and.returnValue(of(milestone));
      service.load(1);

      const projectsIdsToBind: number[] = [1, 2];

      restService.post.and.returnValue(
        of({ ...milestone, boundProjectsInformation: [unboundProjectInfo, unknownProjectInfo] }),
      );

      service.bindProjectsToMilestone(projectsIdsToBind).subscribe(() => {
        service.componentData$.subscribe((componentData) => {
          expect(componentData.milestone.boundProjectsInformation[0].boundToMilestone).toEqual(
            true,
          );
          expect(componentData.milestone.boundProjectsInformation[0].projectName).toEqual(
            'project1',
          );
          expect(componentData.milestone.boundProjectsInformation[1].boundToMilestone).toEqual(
            true,
          );
          expect(componentData.milestone.boundProjectsInformation[1].projectName).toEqual(
            'project2',
          );
        });
      });
    });

    it('should unbind from projects', async () => {
      const service: MilestoneViewService = TestBed.inject(MilestoneViewService);
      const milestone = getInitialModel();
      milestone.boundProjectsInformation = [getProjectBindingInfos(1), getProjectBindingInfos(2)];

      restService.getWithoutErrorHandling.and.returnValue(of(milestone));
      service.load(1);

      restService.delete.and.returnValue(of({}));

      service.unbindProjectsFromMilestone([1, 2]).subscribe(() => {
        service.componentData$.subscribe((componentData) => {
          expect(componentData.milestone.boundProjectsInformation.length).toEqual(0);
        });
      });
    });

    it('should unbind from projects and keep in perimeter', async () => {
      const service: MilestoneViewService = TestBed.inject(MilestoneViewService);
      const milestone = getInitialModel();
      milestone.boundProjectsInformation = [getProjectBindingInfos(1), getProjectBindingInfos(2)];

      restService.getWithoutErrorHandling.and.returnValue(of(milestone));
      service.load(1);

      restService.delete.and.returnValue(of({}));

      service.unbindProjectsFromMilestoneAndKeepInPerimeter([1, 2]).subscribe(() => {
        service.componentData$.subscribe((componentData) => {
          expect(
            componentData.milestone.boundProjectsInformation.map((proj) => proj.projectId),
          ).toEqual([1, 2]);

          expect(
            componentData.milestone.boundProjectsInformation.map((proj) => proj.boundToMilestone),
          ).toEqual([false, false]);
        });
      });
    });
  });

  function getInitialModel(): MilestoneAdminView {
    return {
      id: 1,
      label: 'milestone 1',
      range: 'GLOBAL',
      endDate: null,
      description: '',
      status: 'IN_PROGRESS',
      ownerFistName: 'John',
      ownerLastName: 'Doe',
      ownerLogin: 'jdoe',
      createdBy: '',
      createdOn: null,
      lastModifiedBy: null,
      lastModifiedOn: null,
      canEdit: true,
      boundProjectsInformation: [],
    };
  }
});

function getProjectManager(login: string): AuthenticatedUser {
  return {
    admin: false,
    username: login,
    firstName: '',
    lastName: '',
    hasAnyReadPermission: true,
    projectManager: true,
    milestoneManager: true,
    clearanceManager: true,
    functionalTester: true,
    automationProgrammer: true,
    userId: 1,
    canDeleteFromFront: true,
  };
}
