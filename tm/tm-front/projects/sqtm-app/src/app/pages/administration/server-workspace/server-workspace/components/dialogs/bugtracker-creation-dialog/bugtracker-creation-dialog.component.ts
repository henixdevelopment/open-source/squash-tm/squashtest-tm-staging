import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { DialogReference, FieldValidationError, RestService } from 'sqtm-core';
import { BugtrackerDialogConfiguration } from './bugtracker.dialog.configuration';
import { AbstractAdministrationCreationDialogDirective } from '../../../../../components/abstract-administration-creation-dialog';
import { of } from 'rxjs';

@Component({
  selector: 'sqtm-app-bugtracker-creation-dialog',
  templateUrl: './bugtracker-creation-dialog.component.html',
  styleUrls: ['./bugtracker-creation-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BugtrackerCreationDialogComponent
  extends AbstractAdministrationCreationDialogDirective
  implements OnInit
{
  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  data: BugtrackerDialogConfiguration;

  constructor(
    private fb: FormBuilder,
    dialogReference: DialogReference<BugtrackerDialogConfiguration>,
    restService: RestService,
    cdr: ChangeDetectorRef,
  ) {
    super('bugtracker/new', dialogReference, restService, cdr);
    this.data = this.dialogReference.data;
  }

  get textFieldToFocus(): string {
    return 'name';
  }

  ngOnInit() {
    this.initializeFormGroup();
  }

  protected getRequestPayload() {
    return of({
      name: this.getFormControlValue('name'),
      kind: this.getFormControlValue('kind'),
      url: this.getFormControlValue('url'),
      description: this.getFormControlValue('description'),
    });
  }

  protected doResetForm() {
    this.resetFormControl('name', '');
    this.resetFormControl('kind', '');
    this.resetFormControl('url', '');
    this.resetFormControl('description', '');
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      name: this.fb.control('', [Validators.required, Validators.maxLength(50)]),
      kind: this.fb.control(this.data.bugtrackerKinds[0].id, [Validators.required]),
      url: this.fb.control('', [Validators.required, Validators.maxLength(255)]),
      description: this.fb.control(''),
    });
  }
}
