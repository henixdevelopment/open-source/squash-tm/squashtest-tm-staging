import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { UserViewService } from '../../../services/user-view.service';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-user-permissions-panel',
  templateUrl: './user-permissions-panel.component.html',
  styleUrls: ['./user-permissions-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserPermissionsPanelComponent implements OnDestroy {
  userCanDeleteFromFront$: Observable<boolean>;

  private readonly unsub$ = new Subject<void>();

  constructor(public readonly userViewService: UserViewService) {
    this.userCanDeleteFromFront$ = this.userViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      map((state) => state.user.canDeleteFromFront),
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  changeCanDeleteFromFront(): void {
    this.userViewService.toggleCanDeleteFromFront();
  }
}
