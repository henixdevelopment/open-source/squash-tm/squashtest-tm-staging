import { Injectable } from '@angular/core';
import { createFeatureSelector } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { map, switchMap, take, tap, withLatestFrom } from 'rxjs/operators';
import {
  AdminReferentialDataService,
  AdminScmServer,
  AttachmentService,
  AuthenticatedUser,
  AuthenticationProtocol,
  Credentials,
  DateFormatUtils,
  EntityViewAttachmentHelperService,
  GenericEntityViewService,
  RestService,
  ScmRepository,
} from 'sqtm-core';
import { ScmRepositoryPayload } from '../components/dialogs/add-scm-repository-dialog/add-scm-repository-dialog.component';
import { AdminScmServerState } from '../states/admin-scm-server-state';
import {
  AdminScmServerViewState,
  provideInitialAdminScmServerView,
} from '../states/admin-scm-server-view-state';

@Injectable()
export class ScmServerViewService extends GenericEntityViewService<
  AdminScmServerState,
  'scmServer'
> {
  constructor(
    protected restService: RestService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
    private adminReferentialDataService: AdminReferentialDataService,
  ) {
    super(restService, attachmentService, translateService, attachmentHelper);
  }

  public getInitialState(): AdminScmServerViewState {
    return provideInitialAdminScmServerView();
  }

  protected getRootUrl(_initialState?): string {
    return 'scm-servers';
  }

  load(scmServerId: number) {
    return this.restService
      .getWithoutErrorHandling<AdminScmServer>(['scm-server-view', scmServerId.toString()])
      .subscribe({
        next: (scmServer) => this.initializeScmServer(scmServer),
        error: (err) => this.notifyEntityNotFound(err),
      });
  }

  private initializeScmServer(scmServer: AdminScmServer): void {
    const scmServerState: AdminScmServerState = {
      ...scmServer,
      id: scmServer.serverId,
      attachmentList: { id: null, attachments: null },
      createdOn: DateFormatUtils.createDateFromIsoString(scmServer.createdOn),
      lastModifiedOn: DateFormatUtils.createDateFromIsoString(scmServer.lastModifiedOn),
    };
    this.initializeEntityState(scmServerState);
  }

  setBasicAuthCredentials(username: any, password: any) {
    return this.doSetCredentials({
      implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
      type: AuthenticationProtocol.BASIC_AUTH,
      username,
      password,
    });
  }

  setTokenAuthCredentials(token: string) {
    return this.doSetCredentials({
      implementedProtocol: AuthenticationProtocol.TOKEN_AUTH,
      type: AuthenticationProtocol.TOKEN_AUTH,
      token,
    });
  }

  private doSetCredentials(credentials: Credentials): Observable<any> {
    return this.componentData$.pipe(
      take(1),
      switchMap((state: AdminScmServerViewState) =>
        this.restService.post(
          [this.getRootUrl(), state.scmServer.serverId.toString(), 'credentials'],
          credentials,
        ),
      ),
      withLatestFrom(this.componentData$),
      map(([, state]: [any, AdminScmServerViewState]) => {
        credentials.registered = true;
        return {
          ...state,
          scmServer: {
            ...state.scmServer,
            credentials,
          },
        };
      }),
      tap((nextState: AdminScmServerViewState) => this.store.commit(nextState)),
    );
  }

  addRepository(payload: ScmRepositoryPayload): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      switchMap((state: AdminScmServerViewState) =>
        this.addScmRepositoryServerSide(state, payload),
      ),
      withLatestFrom(this.store.state$),
      map(([response, state]: [any, AdminScmServerViewState]) =>
        this.updateStateWithRepositoriesResponse(state, response.repositories),
      ),
      tap((state) => this.store.commit(state)),
    );
  }

  private addScmRepositoryServerSide(
    state: AdminScmServerViewState,
    payload: ScmRepositoryPayload,
  ) {
    const urlParts = ['scm-repositories', state.scmServer.serverId.toString(), 'new'];
    return this.restService.post(urlParts, payload);
  }

  private updateStateWithRepositoriesResponse(
    state: AdminScmServerViewState,
    scmRepositories: ScmRepository[],
  ): AdminScmServerViewState {
    return {
      ...state,
      scmServer: {
        ...state.scmServer,
        repositories: scmRepositories,
      },
    };
  }

  deleteRepositories(scmRepositoryIds: any) {
    return this.store.state$.pipe(
      take(1),
      switchMap(() => this.deleteScmRepositoriesServerSide(scmRepositoryIds)),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminScmServerViewState]) =>
        this.updateStateWithDeletedRepositories(state, scmRepositoryIds),
      ),
      tap((newState) => this.store.commit(newState)),
    );
  }

  private updateStateWithDeletedRepositories(
    state: AdminScmServerViewState,
    scmRepositoryIds: number[],
  ): AdminScmServerViewState {
    const scmRepositories = [...state.scmServer.repositories];
    const filteredScmRepositories = scmRepositories.filter(
      (value) => !scmRepositoryIds.includes(value.scmRepositoryId),
    );
    return {
      ...state,
      scmServer: {
        ...state.scmServer,
        repositories: filteredScmRepositories,
      },
    };
  }

  private deleteScmRepositoriesServerSide(scmRepositoryIds: number[]) {
    const urlParts = ['scm-repositories', scmRepositoryIds.join(',')];
    return this.restService.delete(urlParts);
  }

  changeRepositoryWorkingBranch(scmRepositoryId: number, branch: string): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      switchMap((state: AdminScmServerViewState) =>
        this.changeRepositoryWorkingBranchServerSide(state, scmRepositoryId, branch),
      ),
      withLatestFrom(this.store.state$),
      map(([response, state]: [any, AdminScmServerViewState]) =>
        this.updateStateWithRepositoriesResponse(state, response.repositories),
      ),
      tap((state) => this.store.commit(state)),
    );
  }

  private changeRepositoryWorkingBranchServerSide(
    state: AdminScmServerViewState,
    scmRepositoryId: number,
    branch: string,
  ) {
    const urlParts = [
      'scm-repositories',
      state.scmServer.serverId.toString(),
      scmRepositoryId.toString(),
      'branch',
    ];
    return this.restService.post(urlParts, { branch: branch });
  }

  setAuthenticationProtocol(authProtocol: AuthenticationProtocol): Observable<any> {
    return this.componentData$.pipe(
      take(1),
      switchMap((data: AdminScmServerViewState) =>
        this.restService.post([this.getRootUrl(), data.scmServer.id.toString(), 'auth-protocol'], {
          authProtocol,
        }),
      ),
      withLatestFrom(this.componentData$),
      map(([, state]: [any, AdminScmServerViewState]) => ({
        ...state,
        scmServer: {
          ...state.scmServer,
          authProtocol: authProtocol,
        },
      })),
      tap((nextState: AdminScmServerViewState) => this.store.commit(nextState)),
      tap((state) => this.requireExternalUpdate(state.scmServer.id, 'authProtocol', authProtocol)),
    );
  }

  getCurrentUser(): Observable<AuthenticatedUser> {
    return this.adminReferentialDataService.authenticatedUser$;
  }

  getScmServerCredentialsNotShared(): Observable<boolean> {
    return this.componentData$.pipe(
      take(1),
      switchMap((state: AdminScmServerViewState) =>
        this.restService.get<any>([
          this.getRootUrl(),
          state.scmServer.serverId.toString(),
          'credentials-not-shared',
        ]),
      ),
      map((response: any) => response.credentialsNotShared),
    );
  }

  updateScmServerCredentialsNotShared(isNotShared: boolean): Observable<any> {
    return this.componentData$.pipe(
      take(1),
      switchMap((state: AdminScmServerViewState) =>
        this.restService.post(
          [this.getRootUrl(), state.scmServer.serverId.toString(), 'credentials-not-shared'],
          { credentialsNotShared: isNotShared },
        ),
      ),
      withLatestFrom(this.componentData$),
      map(([, state]: [any, AdminScmServerViewState]) => ({
        ...state,
        scmServer: {
          ...state.scmServer,
          credentialsNotShared: isNotShared,
        },
      })),
      tap((nextState: AdminScmServerViewState) => this.store.commit(nextState)),
    );
  }
}

export const getScmServerViewState = createFeatureSelector<AdminScmServerState>('scmServer');
