import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  InjectionToken,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { AdminProjectViewComponentData } from '../../../containers/project-view/project-view.component';
import {
  AdminReferentialDataService,
  AuthenticationProtocol,
  AutomationDeletionCount,
  AutomationEnvironmentTagHolder,
  AutomationWorkflowTypes,
  BddImplementationTechnology,
  deleteColumn,
  DialogService,
  EditableSelectFieldComponent,
  EditableSelectLevelEnumFieldComponent,
  EditableTextFieldComponent,
  Extendable,
  Fixed,
  GridColumnId,
  GridDefinition,
  GridService,
  gridServiceFactory,
  Identifier,
  indexColumn,
  ListItem,
  Option,
  ReferentialDataService,
  RestService,
  smallGrid,
  Sort,
  StyleDefinitionBuilder,
  TestAutomationServer,
  TestAutomationServerKind,
  textColumn,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { ProjectViewService } from '../../../services/project-view.service';
import {
  distinctUntilChanged,
  filter,
  finalize,
  map,
  skip,
  switchMap,
  take,
  takeUntil,
} from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { projectViewLogger } from '../../../project-view.logger';
import { projectJobCanRunBddColumn } from '../../cell-renderers/project-job-can-run-bdd-cell/project-job-can-run-bdd-cell.component';
import { editJobColumn } from '../../cell-renderers/edit-project-job-cell/edit-project-job-cell.component';
import { AddJobDialogComponent } from '../../dialogs/add-job-dialog/add-job-dialog.component';
import { projectJobLabelColumn } from '../../cell-renderers/project-job-label-cell/project-job-label-cell.component';
import { DeleteProjectJobCellComponent } from '../../cell-renderers/delete-project-job-cell/delete-project-job-cell.component';
import { TAServerConnectConfiguration } from '../../dialogs/taserver-connect-dialog/taserver-connect-configuration';
import { TAServerConnectDialogComponent } from '../../dialogs/taserver-connect-dialog/taserver-connect-dialog.component';
import { EnvironmentSelectionPanelComponent } from '../../../../../../../components/squash-orchestrator/orchestrator-execution-environment/components/environment-selection-panel/environment-selection-panel.component';
import {
  OrchestratorExecutionEnvironmentVariablePanelComponent,
  EnvironmentVariableAssociationsTableDefinition,
  EV_ASSOCIATION_TABLE,
  EV_ASSOCIATION_TABLE_CONF,
} from '../../../../../../../components/squash-orchestrator/orchestrator-execution-environment-variable/components/orchestrator-execution-environment-variable-panel/orchestrator-execution-environment-variable-panel.component';
import { OrchestratorWorkflowsComponent } from '../../../../../../../components/squash-orchestrator/orchestrator-workflows/components/orchestrator-workflows.component';
import {
  CleaningTypes,
  CleanAutomatedSuitesAndExecutionProjectConfiguration,
} from '../../dialogs/automated-suites-cleaning-dialog/clean-automated-suites-and-execution-project-configuration';
import { CleanAutomatedSuitesAndExecutionDialogComponent } from '../../dialogs/automated-suites-cleaning-dialog/clean-automated-suites-and-execution-dialog.component';

const logger = projectViewLogger.compose('ProjectAutomationPanelComponent');

const AUTOMATED_WORKFLOW_DOC_URL_EN =
  'https://tm-en.doc.squashtest.com/latest/user-guide/manage-automated-tests/index.html?utm_source=Appli_squash&utm_medium=link';
const AUTOMATED_WORKFLOW_DOC_URL_FR =
  'https://tm-fr.doc.squashtest.com/latest/user-guide/gestion-tests-automatises/index.html?utm_source=Appli_squash&utm_medium=link';

export const PROJECT_TA_JOBS_TABLE_CONF = new InjectionToken('PROJECT_TA_JOBS_TABLE_CONF');
export const PROJECT_TA_JOBS_TABLE = new InjectionToken('PROJECT_TA_JOBS_TABLE');

export function projectJobsTableDefinition(): GridDefinition {
  return smallGrid('project-ta-jobs')
    .withColumns([
      indexColumn().withViewport('leftViewport'),
      projectJobLabelColumn(GridColumnId.label)
        .withI18nKey('sqtm-core.generic.label.label')
        .changeWidthCalculationStrategy(new Extendable(80, 0.2)),
      textColumn(GridColumnId.remoteName)
        .withI18nKey('sqtm-core.administration-workspace.views.project.automation.jobs.remote-name')
        .changeWidthCalculationStrategy(new Extendable(80, 0.3)),
      textColumn(GridColumnId.baseUrl)
        .withI18nKey('sqtm-core.entity.generic.url.label')
        .changeWidthCalculationStrategy(new Extendable(80, 0.2)),
      projectJobCanRunBddColumn(GridColumnId.canRunBdd).changeWidthCalculationStrategy(
        new Fixed(120),
      ),
      editJobColumn(GridColumnId.edit),
      deleteColumn(DeleteProjectJobCellComponent),
    ])
    .withInitialSortedColumns([{ id: GridColumnId.label, sort: Sort.ASC }])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .build();
}

@Component({
  selector: 'sqtm-app-project-automation-panel',
  templateUrl: './project-automation-panel.component.html',
  styleUrls: ['./project-automation-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: PROJECT_TA_JOBS_TABLE_CONF,
      useFactory: projectJobsTableDefinition,
    },
    {
      provide: PROJECT_TA_JOBS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, PROJECT_TA_JOBS_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: PROJECT_TA_JOBS_TABLE,
    },
    {
      provide: EV_ASSOCIATION_TABLE_CONF,
      useFactory: EnvironmentVariableAssociationsTableDefinition,
    },
    {
      provide: EV_ASSOCIATION_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, EV_ASSOCIATION_TABLE_CONF, ReferentialDataService],
    },
  ],
})
export class ProjectAutomationPanelComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('serverSelectField')
  serverSelectField: EditableSelectFieldComponent;

  @ViewChild('repositorySelectField')
  repositorySelectField: EditableSelectFieldComponent;

  @ViewChild('executionServerField')
  executionServerField: EditableSelectFieldComponent;

  @ViewChild('automatedSuitesLifetimeField')
  automatedSuitesLifetimeField: EditableTextFieldComponent;

  @ViewChild('bddTechnologySelectField')
  bddTechnologySelectField: EditableSelectLevelEnumFieldComponent;

  @ViewChild('automationWorkflowField')
  automationWorkflowField: EditableSelectFieldComponent;

  @ViewChild(EnvironmentSelectionPanelComponent)
  environmentSelectionPanel: EnvironmentSelectionPanelComponent;

  @ViewChild(OrchestratorExecutionEnvironmentVariablePanelComponent)
  environmentVariablePanel: OrchestratorExecutionEnvironmentVariablePanelComponent;

  @ViewChild(OrchestratorWorkflowsComponent)
  squashAutomWorkflowsComponent: OrchestratorWorkflowsComponent;

  readonly workflowOptions: Option[];

  scmServerOptions: Option[];
  repositoryOptions: Option[];

  executionServerOptions: Option[];

  isAdmin$: Observable<boolean>;

  selected = false;

  componentData$: Observable<AdminProjectViewComponentData>;

  selectedTAServerId$: Observable<number | null>;

  public readonly environmentPanelTagHolderType = AutomationEnvironmentTagHolder.PROJECT;

  // Allow to dynamically load repositories options based on currently selected server
  private repositoryOptionsMap: Map<number, Option[]> = new Map();

  // It's easier to manage our view state locally because these two data fields represent a single binding server-side
  private selectedServerId: number;
  private selectedRepositoryId: number;

  private unsub$ = new Subject<void>();

  get automatedWorkflowDocUrl(): string {
    return this.translateService.getBrowserLang() === 'fr'
      ? AUTOMATED_WORKFLOW_DOC_URL_FR
      : AUTOMATED_WORKFLOW_DOC_URL_EN;
  }

  constructor(
    private readonly translateService: TranslateService,
    private readonly restService: RestService,
    private readonly cdr: ChangeDetectorRef,
    public readonly projectViewService: ProjectViewService,
    private readonly gridService: GridService,
    private readonly dialogService: DialogService,
    private readonly vcRef: ViewContainerRef,
    private readonly adminReferentialDataService: AdminReferentialDataService,
    private vcr: ViewContainerRef,
    @Inject(EV_ASSOCIATION_TABLE) public evGridService: GridService,
  ) {
    this.componentData$ = this.projectViewService.componentData$;

    this.selectedTAServerId$ = this.componentData$.pipe(
      map((componentData) => componentData.project.taServerId),
    );

    this.workflowOptions = [
      {
        value: 'REMOTE_WORKFLOW',
        label: this.translateService.instant(this.getWorkflowLabelKey('REMOTE_WORKFLOW')),
        hide: true,
      },
      {
        value: 'NONE',
        label: this.translateService.instant(this.getWorkflowLabelKey('NONE')),
      },
      {
        value: 'NATIVE',
        label: this.translateService.instant(this.getWorkflowLabelKey('NATIVE')),
      },
      {
        value: 'NATIVE_SIMPLIFIED',
        label: this.translateService.instant(this.getWorkflowLabelKey('NATIVE_SIMPLIFIED')),
      },
    ];

    this.prepareJobsTable();
  }

  private static extractNumericValueFromOption(option: Option) {
    if (option == null || option.value == null) {
      return null;
    }

    return Number(option.value);
  }

  ngOnInit() {
    this.isAdmin$ = this.adminReferentialDataService.authenticatedUser$.pipe(
      takeUntil(this.unsub$),
      map((user) => user.admin),
    );
  }

  ngAfterViewInit(): void {
    this.buildScmServerSection();
    this.buildTestAutomationServerSection();
    this.initSelectedAutomationServerObserver();

    this.projectViewService.componentData$
      .pipe(
        takeUntil(this.unsub$),
        map((componentData) => componentData.project.automationWorkflowType),
        distinctUntilChanged(),
        skip(1),
        filter((type) => type !== 'NONE'),
      )
      .subscribe(() => {
        // TODO PCK : setTimeout smells bad
        setTimeout(() => this.loadScmServerState());
      });

    this.projectViewService.componentData$
      .pipe(
        takeUntil(this.unsub$),
        map((componentData) => componentData.project.boundTestAutomationProjects.length),
        distinctUntilChanged(),
        skip(1),
      )
      .subscribe(() => {
        // TODO PCK : setTimeout smells bad
        setTimeout(() => this.gridService.refreshData());
      });
  }

  ngOnDestroy(): void {
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  showsScmBlock(componentData: AdminProjectViewComponentData): boolean {
    return (
      componentData.project.automationWorkflowType !== 'NONE' && Boolean(this.scmServerOptions)
    );
  }

  showsScmRepositoryField(): boolean {
    return this.selectedServerId != null;
  }

  showsExecutionServerBlock(): boolean {
    return Boolean(this.executionServerOptions);
  }

  showsJobsBlock(componentData: AdminProjectViewComponentData): boolean {
    return (
      Boolean(componentData.project.taServerId) &&
      this.checkIfAutomationServerIsNotSquashAutomKind(componentData)
    );
  }

  showsEnvironmentsBlock(componentData: AdminProjectViewComponentData): boolean {
    return (
      componentData.project.taServerId != null &&
      this.doesServerSupportsEnvironments(componentData.project.taServerId, componentData)
    );
  }

  private doesServerSupportsEnvironments(
    taServerId: Identifier,
    componentData: AdminProjectViewComponentData,
  ) {
    const server = componentData.project.availableTestAutomationServers.find(
      (taServer) => taServer.id === taServerId,
    );
    return server?.supportsAutomatedExecutionEnvironments ?? false;
  }

  private checkIfAutomationServerIsNotSquashAutomKind(
    componentData: AdminProjectViewComponentData,
  ): boolean {
    const availableServers = componentData.project.availableTestAutomationServers;
    const selectedServer: TestAutomationServer = availableServers.find(
      (server) => server.id === componentData.project.taServerId,
    );
    return selectedServer.kind !== TestAutomationServerKind.squashOrchestrator;
  }

  handleScmServerConfirm(selectedOption: Option, componentData: AdminProjectViewComponentData) {
    const serverId = ProjectAutomationPanelComponent.extractNumericValueFromOption(selectedOption);

    // Just disable edit mode and exit if value hasn't changed
    if (this.serverSelectField.value === serverId?.toString()) {
      this.serverSelectField.disableEditMode();
      return;
    }

    this.serverSelectField.value = serverId?.toString();
    this.selectedRepositoryId = null;

    this.setSelectedScmServer(serverId);

    if (componentData.project.scmRepositoryId) {
      this.projectViewService.unbindFromScmRepository();
    }
  }

  handleScmRepositoryConfirm(option: Option) {
    const repoId = option?.value;
    if (repoId == null) {
      this.projectViewService.unbindFromScmRepository();
    } else {
      this.projectViewService.bindToScmRepository(Number(repoId));
    }

    this.repositorySelectField.value = repoId;
  }

  handleExecutionServerChange(
    selectedOption: Option,
    componentData: AdminProjectViewComponentData,
  ) {
    const newServerId =
      ProjectAutomationPanelComponent.extractNumericValueFromOption(selectedOption);
    const valueChanged = componentData.project.taServerId !== newServerId;

    if (!valueChanged) {
      this.executionServerField.value = selectedOption?.value;
      return;
    }

    if (this.shouldShowConfirmChangeTAServerDialog(componentData)) {
      this.showConfirmChangeTAServerDialog(newServerId, componentData);
    } else {
      this.projectViewService.bindToTestAutomationServer(newServerId);
      this.executionServerField.value = newServerId?.toString();
      this.environmentSelectionPanel?.handleBoundServerChanged(newServerId);
      this.environmentVariablePanel?.handleBoundServerChanged();
    }
  }

  private shouldShowConfirmChangeTAServerDialog(
    componentData: AdminProjectViewComponentData,
  ): boolean {
    const hasServer = componentData.project.taServerId != null;

    if (!hasServer) {
      return false;
    }

    const hasJobs = componentData.project.boundTestAutomationProjects.length > 0;

    if (hasJobs) {
      return true;
    }

    const taServer = componentData.project.availableTestAutomationServers.find(
      (server) => server.id === componentData.project.taServerId,
    );
    return taServer.supportsAutomatedExecutionEnvironments;
  }

  private showConfirmChangeTAServerDialog(
    newServerId: number,
    componentData: AdminProjectViewComponentData,
  ) {
    const hasJobs = componentData.project.boundTestAutomationProjects.length > 0;
    const messageKey =
      'sqtm-core.administration-workspace.projects.dialog.message.confirm-unbind-ta-server' +
      (hasJobs ? '-with-jobs' : '-with-envs');

    const dialogRef = this.dialogService.openConfirm(
      {
        id: 'confirm-unbind-server',
        titleKey:
          'sqtm-core.administration-workspace.projects.dialog.title.confirm-unbind-ta-server',
        messageKey,
        level: 'DANGER',
      },
      700,
    );

    dialogRef.dialogClosed$.pipe(take(1)).subscribe((result) => {
      let taServerId: number;
      if (result) {
        this.projectViewService.bindToTestAutomationServer(newServerId);
        this.executionServerField.value = newServerId?.toString();
        taServerId = newServerId;
      } else {
        this.executionServerField.value = componentData.project.taServerId.toString();
        taServerId = componentData.project.taServerId;
      }

      this.executionServerField.cdRef.detectChanges();
      this.environmentSelectionPanel?.handleBoundServerChanged(taServerId);
      this.environmentVariablePanel?.handleBoundServerChanged();
    });
  }

  bindEnvironmentVariable(items: ListItem[]) {
    this.evGridService.beginAsyncOperation();
    const selectedItemIds: Identifier[] = items.map((item) => item.id);
    this.environmentVariablePanel
      .bindEnvironmentVariables(selectedItemIds)
      .pipe(
        finalize(() => {
          this.evGridService.completeAsyncOperation();
        }),
      )
      .subscribe();
  }

  handleAutomatedSuitesLifetimeChange(newLifetime: any, state: AdminProjectViewComponentData) {
    if (this.checkLifetimeConformity(newLifetime)) {
      this.projectViewService
        .updateAutomatedSuitesLifetime(state, newLifetime)
        .subscribe(() => (this.automatedSuitesLifetimeField.value = newLifetime));
    } else {
      this.dialogService
        .openAlert({
          titleKey:
            'sqtm-core.administration-workspace.views.project.automation.automated-suites-cleaning.lifetime.dialog.title',
          messageKey:
            'sqtm-core.administration-workspace.views.project.automation.automated-suites-cleaning.lifetime.dialog.message',
          level: 'DANGER',
        })
        .dialogClosed$.pipe(take(1))
        .subscribe(
          (_result) =>
            (this.automatedSuitesLifetimeField.value =
              state.project.automatedSuitesLifetime?.toString()),
        );
    }
  }

  handleAutomationWorkflowTypeChange(
    workflowType: any,
    componentData: AdminProjectViewComponentData,
  ) {
    if (componentData.project.automationWorkflowType === AutomationWorkflowTypes.REMOTE_WORKFLOW) {
      this.projectViewService
        .disabledWorkflowAutomJiraPlugin(componentData.project.id, workflowType, true)
        .subscribe(() => this.editAutomationWorkflowType());
    } else {
      this.projectViewService
        .changeAutomationWorkflowTypeAndDeactivatePlugin(componentData.project.id, workflowType)
        .subscribe(() => {
          this.automationWorkflowField.value = workflowType;
          this.editAutomationWorkflowType();
        });
    }
  }

  getAutomatedSuiteLifetimeValue(componentData: AdminProjectViewComponentData): string | number {
    return componentData.project.automatedSuitesLifetime != null
      ? componentData.project.automatedSuitesLifetime.toString()
      : '';
  }

  checkLifetimeConformity(lifetime: any): boolean {
    return (
      lifetime === '' ||
      (Number.isInteger(Number(lifetime)) &&
        lifetime >= 0 &&
        lifetime <= ProjectViewService.MAX_AUTOMATED_SUITES_LIFETIME)
    );
  }

  showCleaningDialog(projectId: string): void {
    this.showDeletionDialog(
      projectId,
      'sqtm-core.administration-workspace.views.project.automation.automated-suites-cleaning.delete-all.dialog.title',
      'sqtm-core.administration-workspace.views.project.automation.automated-suites-cleaning.delete-all.dialog.message',
      CleaningTypes.AUTOMATED_SUITE,
    );
  }

  showPruneDialog(projectId: string, pruneType: string): void {
    this.showDeletionDialog(
      projectId,
      `sqtm-core.administration-workspace.views.project.automation.automated-suites-cleaning.attachment-prune.${pruneType}.dialog.title`,
      `sqtm-core.administration-workspace.views.project.automation.automated-suites-cleaning.attachment-prune.${pruneType}.dialog.message`,
      `${pruneType}-prune`,
    );
  }

  private showDeletionDialog(
    projectId: string,
    titleKey: string,
    messageKey: string,
    cleaningType: string,
  ): void {
    this.projectViewService
      .getOldAutomatedSuitesAndExecutionsCount(projectId)
      .pipe(
        take(1),
        switchMap((count: AutomationDeletionCount) =>
          this.openConfirmDeletionDialog(count, projectId, titleKey, messageKey, cleaningType),
        ),
      )
      .subscribe();
  }

  private openConfirmDeletionDialog(
    count: AutomationDeletionCount,
    projectId: string,
    titleKey: string,
    messageKey: string,
    cleaningType: string,
  ): Observable<boolean> {
    const dialogReference = this.dialogService.openDialog<
      CleanAutomatedSuitesAndExecutionProjectConfiguration,
      boolean
    >({
      id: 'clean-project-automated-suite-and-executions',
      component: CleanAutomatedSuitesAndExecutionDialogComponent,
      viewContainerReference: this.vcr,
      data: {
        id: 'clean-project-automated-suite-and-executions',
        titleKey: titleKey,
        messageKey: messageKey,
        level: 'DANGER',
        projectId: projectId,
        automationDeletionCount: count,
        cleaningType: cleaningType,
      },
      width: 800,
    });

    return dialogReference.dialogClosed$.pipe(takeUntil(this.unsub$));
  }

  addJob() {
    const dialogRef = this.dialogService.openDialog({
      id: 'add-job-dialog',
      component: AddJobDialogComponent,
      viewContainerReference: this.vcRef,
      width: 700,
    });

    dialogRef.dialogClosed$
      .pipe(
        take(1),
        filter((result) => Boolean(result)),
      )
      .subscribe(() => {
        this.gridService.refreshData();
      });
  }

  connectToAddJob(componentData: AdminProjectViewComponentData) {
    const dialogRef = this.dialogService.openDialog<TAServerConnectConfiguration, boolean>({
      id: 'taserver-connect-dialog',
      viewContainerReference: this.vcRef,
      component: TAServerConnectDialogComponent,
      data: {
        taServer: {
          id: componentData.project.taServerId,
          authProtocol: this.getTAServerAuthProtocol(componentData),
        },
      },
    });
    dialogRef.dialogClosed$
      .pipe(
        takeUntil(this.unsub$),
        filter((result) => Boolean(result)),
      )
      .subscribe(() => {
        this.addJob();
      });
  }

  private getTAServerAuthProtocol(componentData: AdminProjectViewComponentData) {
    const boundServer = componentData.project.availableTestAutomationServers.find(
      (server) => server.id === componentData.project.taServerId,
    );

    if (boundServer != null) {
      return boundServer.authProtocol;
    }

    return AuthenticationProtocol.BASIC_AUTH;
  }

  private setSelectedScmServer(serverId: number) {
    this.selectedServerId = serverId;

    if (this.repositorySelectField) {
      this.repositorySelectField.value = null;
    }

    if (serverId == null) {
      this.repositoryOptions = [];
    } else {
      this.repositoryOptions = this.repositoryOptionsMap.get(serverId);
    }

    this.cdr.detectChanges();

    if (
      Boolean(this.repositorySelectField) &&
      this.selectedRepositoryId != null &&
      serverId === this.getServerIdForRepository(this.selectedRepositoryId)
    ) {
      this.repositorySelectField.value = this.selectedRepositoryId.toString();
    }
  }

  private loadScmServerState() {
    this.componentData$.pipe(take(1)).subscribe((componentData) => {
      this.selectedRepositoryId = componentData.project.scmRepositoryId;

      const actualServerId = this.getServerIdForRepository(this.selectedRepositoryId);

      if (actualServerId != null && Boolean(this.serverSelectField)) {
        this.setSelectedScmServer(actualServerId);
        this.serverSelectField.value =
          typeof actualServerId === 'number' ? this.selectedServerId.toString() : null;
        this.serverSelectField.markForCheck();
      }
    });
  }

  private getServerIdForRepository(repositoryId: number) {
    if (repositoryId == null) {
      return null;
    }

    let foundServerId = null;

    this.repositoryOptionsMap.forEach((options, serverId) => {
      if (foundServerId == null) {
        if (options.map((opt) => opt.value).includes(repositoryId.toString())) {
          foundServerId = serverId;
        }
      }
    });

    if (foundServerId == null) {
      logger.error(`Cannot find serverId for repositoryId ${repositoryId}`);
    }

    return foundServerId;
  }

  private buildScmServerSection() {
    this.componentData$
      .pipe(
        filter((componentData) => Boolean(componentData.project)),
        take(1),
      )
      .subscribe((componentData) => {
        if (!componentData?.project?.availableScmServers) {
          return;
        }

        this.scmServerOptions = componentData.project.availableScmServers
          .filter((server) => server.repositories.length > 0)
          .map((server) => ({
            value: server.serverId.toString(),
            label: `${server.name} (${server.url})`,
          }));

        componentData.project.availableScmServers.forEach((server) => {
          this.repositoryOptionsMap.set(
            server.serverId,
            server.repositories.map((repo) => ({
              value: repo.scmRepositoryId.toString(),
              label: `${repo.name} (${repo.workingBranch})`,
            })),
          );
        });

        this.cdr.detectChanges();

        this.loadScmServerState();
      });
  }

  private buildTestAutomationServerSection() {
    this.componentData$
      .pipe(
        filter((componentData) => Boolean(componentData.project)),
        take(1),
      )
      .subscribe((componentData) => {
        this.executionServerOptions = componentData.project.availableTestAutomationServers.map(
          (server) => ({
            value: server.id.toString(),
            label: server.name,
          }),
        );

        this.cdr.detectChanges();

        this.executionServerField.value = componentData.project.taServerId?.toString();
      });
  }

  private prepareJobsTable() {
    const taProjectsTable = this.projectViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      filter((componentData: AdminProjectViewComponentData) => Boolean(componentData.project)),
      map((componentData: AdminProjectViewComponentData) => {
        const baseUrl = componentData.project.availableTestAutomationServers.filter(
          (server) => server.id === componentData.project.taServerId,
        )[0]?.baseUrl;

        return componentData.project.boundTestAutomationProjects.map((project) => ({
          ...project,
          baseUrl,
        }));
      }),
    );

    this.gridService.connectToDatasource(taProjectsTable, 'taProjectId');
  }

  canEditScriptLanguage(componentData: AdminProjectViewComponentData) {
    return (
      componentData.project.bddImplementationTechnology ===
        BddImplementationTechnology.CUCUMBER_4.id ||
      componentData.project.bddImplementationTechnology ===
        BddImplementationTechnology.CUCUMBER_5_PLUS.id
    );
  }

  isRemoteWorkflow(automationWorkflowType: string) {
    return automationWorkflowType === AutomationWorkflowTypes.REMOTE_WORKFLOW;
  }

  editAutomationWorkflowType() {
    this.selected = true;
  }

  getDefaultWorkflowValue(componentData: AdminProjectViewComponentData) {
    return componentData.project.automationWorkflowType === AutomationWorkflowTypes.REMOTE_WORKFLOW
      ? AutomationWorkflowTypes.NONE
      : componentData.project.automationWorkflowType;
  }

  getValue(componentData: AdminProjectViewComponentData) {
    const workflowType = componentData.project.automationWorkflowType;
    return this.translateService.instant(this.getWorkflowLabelKey(workflowType));
  }

  private getWorkflowLabelKey(workflowType: string): string {
    return `sqtm-core.administration-workspace.views.project.automation.workflow.${workflowType}.label`;
  }

  private initSelectedAutomationServerObserver() {
    this.projectViewService.componentData$
      .pipe(
        takeUntil(this.unsub$),
        map((componentData: AdminProjectViewComponentData) => componentData.project.taServerId),
      )
      .subscribe((taServerId) => {
        if (this.executionServerField) {
          this.executionServerField.value = taServerId?.toString();
          this.executionServerField.cdRef.detectChanges();
        }
      });
  }

  refreshWorkflows() {
    this.squashAutomWorkflowsComponent.handleTokenChanged();
  }

  protected readonly CleaningTypes = CleaningTypes;
}
