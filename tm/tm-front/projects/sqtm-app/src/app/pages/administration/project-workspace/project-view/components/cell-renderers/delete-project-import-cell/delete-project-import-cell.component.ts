import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Signal } from '@angular/core';
import {
  ActionErrorDisplayService,
  AbstractDeleteCellRenderer,
  DataRow,
  DialogService,
  GridService,
  PivotFormatImportStatus,
  AuthenticatedUser,
  AdminReferentialDataService,
  Permissions,
  ConfirmDeleteLevel,
} from 'sqtm-core';
import { ProjectViewService } from '../../../services/project-view.service';
import { catchError, finalize } from 'rxjs/operators';
import { AdminProjectViewComponentData } from '../../../containers/project-view/project-view.component';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sqtm-app-delete-project-import-cell',
  template: `
    @if (row && canDelete(row)) {
      <sqtm-core-delete-icon
        [iconName]="icon"
        (delete)="showDeleteConfirm()"
      ></sqtm-core-delete-icon>
    }
  `,
  styleUrl: './delete-project-import-cell.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteProjectImportCellComponent extends AbstractDeleteCellRenderer {
  protected $componentData: Signal<AdminProjectViewComponentData>;
  protected $authenticatedUser: Signal<AuthenticatedUser>;

  constructor(
    public grid: GridService,
    cdr: ChangeDetectorRef,
    protected dialogService: DialogService,
    protected projectViewService: ProjectViewService,
    protected actionErrorDisplayService: ActionErrorDisplayService,
    private readonly adminReferentialDataService: AdminReferentialDataService,
  ) {
    super(grid, cdr, dialogService);
    this.$componentData = toSignal(this.projectViewService.componentData$);
    this.$authenticatedUser = toSignal(adminReferentialDataService.authenticatedUser$);
  }

  get icon(): string {
    return 'sqtm-core-generic:delete';
  }

  canDelete(row: DataRow): boolean {
    return row.data.status !== PivotFormatImportStatus.RUNNING && this.isAdminOrImportManager();
  }

  isAdminOrImportManager(): boolean {
    return (
      this.$authenticatedUser().admin ||
      this.$componentData().project.permissions.PROJECT?.includes(Permissions.IMPORT)
    );
  }

  protected doDelete(): any {
    this.grid.beginAsyncOperation();
    const importId = this.row.data.id;
    this.projectViewService
      .deleteProjectImportRequests(importId)
      .pipe(
        catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
        finalize(() => this.grid.completeAsyncOperation()),
      )
      .subscribe(() => this.grid.refreshData());
  }

  protected getTitleKey(): string {
    return 'sqtm-core.administration-workspace.projects.dialog.title.delete-import-request.delete-one';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.administration-workspace.projects.dialog.message.delete-import-request.delete-one';
  }

  protected getLevel(): ConfirmDeleteLevel {
    return 'DANGER';
  }
}
