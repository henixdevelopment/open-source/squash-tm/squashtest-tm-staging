import { ChangeDetectionStrategy, Component, OnDestroy, ViewChild } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { AdminBugTrackerViewState } from '../../../states/admin-bug-tracker-view-state';
import { BugTrackerViewService } from '../../../services/bug-tracker-view.service';
import { takeUntil } from 'rxjs/operators';
import { BugTrackerAuthProtocolPanelComponent } from '../../../components/panels/bug-tracker-auth-protocol-panel/bug-tracker-auth-protocol-panel.component';

@Component({
  selector: 'sqtm-app-bug-tracker-content',
  templateUrl: './bug-tracker-content.component.html',
  styleUrls: ['./bug-tracker-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BugTrackerContentComponent implements OnDestroy {
  componentData$: Observable<AdminBugTrackerViewState>;

  @ViewChild(BugTrackerAuthProtocolPanelComponent)
  bugTrackerAuthProtocolPanelComponent: BugTrackerAuthProtocolPanelComponent;

  unsub$ = new Subject<void>();

  constructor(public readonly bugTrackerViewService: BugTrackerViewService) {
    this.componentData$ = bugTrackerViewService.componentData$.pipe(takeUntil(this.unsub$));
  }

  handleBugTrackerKindChange(bugTrackerKind: string) {
    this.bugTrackerAuthProtocolPanelComponent.handleBugTrackerKindChange(bugTrackerKind);
  }

  handleUrlChanged(bugTrackerUrl: string) {
    this.bugTrackerAuthProtocolPanelComponent.handleUrlChanged(bugTrackerUrl);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
