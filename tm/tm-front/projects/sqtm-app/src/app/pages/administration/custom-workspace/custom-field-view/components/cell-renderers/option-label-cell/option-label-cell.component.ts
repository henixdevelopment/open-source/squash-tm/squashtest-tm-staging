import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewChild } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ActionErrorDisplayService,
  ColumnDefinitionBuilder,
  DialogService,
  EditableTextFieldComponent,
  GridColumnId,
  GridService,
} from 'sqtm-core';
import { CustomFieldViewService } from '../../../services/custom-field-view.service';
import { catchError, finalize } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-option-label-cell',
  template: ` @if (columnDisplay && row) {
    <div class="full-width full-height flex-column">
      <sqtm-core-editable-text-field
        #editableTextField
        style="margin: auto 5px;"
        class="sqtm-grid-cell-txt-renderer"
        [showPlaceHolder]="false"
        [value]="row.data[columnDisplay.id]"
        [layout]="'no-buttons'"
        [size]="'small'"
        (confirmEvent)="updateValue($event)"
      ></sqtm-core-editable-text-field>
    </div>
  }`,
  styleUrls: ['./option-label-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OptionLabelCellComponent extends AbstractCellRendererComponent {
  @ViewChild('editableTextField')
  editableTextField: EditableTextFieldComponent;

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    private dialogService: DialogService,
    private customFieldViewService: CustomFieldViewService,
    private actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    super(grid, cdRef);
  }

  updateValue(newLabel: string) {
    const currentLabel = this.row.data[GridColumnId.label];
    this.customFieldViewService
      .changeOptionLabel(currentLabel, newLabel)
      .pipe(
        catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
        finalize(() => this.editableTextField.endAsync()),
      )
      .subscribe();
  }
}

export function optionLabelColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(OptionLabelCellComponent);
}
