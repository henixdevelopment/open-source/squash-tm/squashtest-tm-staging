import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SystemLogsPanelComponent } from './system-logs-panel.component';
import { RestService } from 'sqtm-core';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { SystemViewState } from '../../../states/system-view.state';

describe('SystemLogsPanelComponent', () => {
  let component: SystemLogsPanelComponent;
  let fixture: ComponentFixture<SystemLogsPanelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SystemLogsPanelComponent],
      providers: [
        {
          provide: RestService,
          useValue: {
            backendRootUrl: '',
          },
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemLogsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should sort previous log files', () => {
    component.componentData = {
      logFiles: [
        'squash-tm.log.20210509',
        'squash-tm.log.20210502',
        'squash-tm.log.20210426',
        'squash-tm.log.20210430',
        'squash-tm.log.20210501',
      ],
    } as SystemViewState;

    expect(component.hasPreviousLogFiles).toBeTrue();
    expect(component.previousLogFiles).toEqual([
      'squash-tm.log.20210509',
      'squash-tm.log.20210502',
      'squash-tm.log.20210501',
      'squash-tm.log.20210430',
      'squash-tm.log.20210426',
    ]);
  });
});
