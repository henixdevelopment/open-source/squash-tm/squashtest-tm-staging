import { TestBed } from '@angular/core/testing';

import { InfoListViewService, NewInfoListItem } from './info-list-view.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppTestingUtilsModule } from '../../../../../utils/testing-utils/app-testing-utils.module';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import {
  AdminInfoList,
  AdminInfoListItem,
  AdminReferentialDataService,
  RestService,
} from 'sqtm-core';
import { of } from 'rxjs';
import { switchMap, take, withLatestFrom } from 'rxjs/operators';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { mockRestService } from '../../../../../utils/testing-utils/mocks.service';
import SpyObj = jasmine.SpyObj;

describe('InfoListViewService', () => {
  const restService = mockRestService();
  const adminReferentialDataService: SpyObj<AdminReferentialDataService> =
    jasmine.createSpyObj<AdminReferentialDataService>(['refresh']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        AppTestingUtilsModule,
        TranslateModule.forRoot(),
        RouterTestingModule,
      ],
      providers: [
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: InfoListViewService,
          useClass: InfoListViewService,
        },
        {
          provide: AdminReferentialDataService,
          useValue: adminReferentialDataService,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
  });

  it('should load an info list', () => {
    restService.getWithoutErrorHandling.and.returnValue(of(getInitialModel()));
    const service: InfoListViewService = TestBed.inject(InfoListViewService);

    service.componentData$.subscribe((data) => {
      expect(data.infoList.id).toEqual(1);
    });

    expect(service).toBeTruthy();

    service.load(1);
  });

  it('should set item code', async () => {
    restService.getWithoutErrorHandling.and.returnValue(of(getInitialModel()));
    const service: InfoListViewService = TestBed.inject(InfoListViewService);
    service.load(1);

    service
      .changeItemCode(2, 'NEW')
      .pipe(
        switchMap(() => service.componentData$),
        take(1),
      )
      .subscribe((data) => {
        expect(data.infoList.items[0].code).toBe('code1');
        expect(data.infoList.items[1].code).toBe('NEW');

        const expectedUrl = ['info-list-items', '2', 'code'];
        expect(restService.post).toHaveBeenCalledWith(expectedUrl, { code: 'NEW' });
      });
  });

  it('should set item label', async () => {
    restService.getWithoutErrorHandling.and.returnValue(of(getInitialModel()));
    const service: InfoListViewService = TestBed.inject(InfoListViewService);
    service.load(1);

    service
      .changeItemLabel(2, 'NEW')
      .pipe(
        switchMap(() => service.componentData$),
        take(1),
      )
      .subscribe((data) => {
        expect(data.infoList.items[0].label).toBe('label1');
        expect(data.infoList.items[1].label).toBe('NEW');

        const expectedUrl = ['info-list-items', '2', 'label'];
        expect(restService.post).toHaveBeenCalledWith(expectedUrl, { label: 'NEW' });
      });
  });

  it('should set item colour', async () => {
    restService.getWithoutErrorHandling.and.returnValue(of(getInitialModel()));
    const service: InfoListViewService = TestBed.inject(InfoListViewService);
    service.load(1);

    service
      .changeItemColour(2, 'NEW')
      .pipe(
        switchMap(() => service.componentData$),
        take(1),
      )
      .subscribe((data) => {
        expect(data.infoList.items[0].colour).toBe(null);
        expect(data.infoList.items[1].colour).toBe('NEW');

        const expectedUrl = ['info-list-items', '2', 'colour'];
        expect(restService.post).toHaveBeenCalledWith(expectedUrl, { colour: 'NEW' });
      });
  });

  it('should set default item', async () => {
    restService.getWithoutErrorHandling.and.returnValue(of(getInitialModel()));
    const service: InfoListViewService = TestBed.inject(InfoListViewService);
    service.load(1);

    service
      .changeDefaultItem(2)
      .pipe(
        switchMap(() => service.componentData$),
        take(1),
      )
      .subscribe(() => {
        const expectedUrl = ['info-list-items', '2', 'default'];
        expect(restService.post).toHaveBeenCalledWith(expectedUrl, {});
      });
  });

  it('should set item icon name', async () => {
    restService.getWithoutErrorHandling.and.returnValue(of(getInitialModel()));
    const service: InfoListViewService = TestBed.inject(InfoListViewService);
    service.load(1);

    service
      .changeItemIcon(2, 'some-namespace:NEW')
      .pipe(
        switchMap(() => service.componentData$),
        take(1),
      )
      .subscribe((data) => {
        expect(data.infoList.items[0].iconName).toBe('noicon');
        expect(data.infoList.items[1].iconName).toBe('NEW');
        const expectedUrl = ['info-list-items', '2', 'icon-name'];
        expect(restService.post).toHaveBeenCalledWith(expectedUrl, { iconName: 'NEW' });
      });
  });

  it('should add new item', async () => {
    const initialModel = getInitialModel();
    restService.getWithoutErrorHandling.and.returnValue(of(initialModel));
    const service: InfoListViewService = TestBed.inject(InfoListViewService);

    restService.post.and.returnValue(
      of({
        ...initialModel,
        items: [
          ...initialModel.items,
          {
            ...createInfoListItem(3),
            iconName: 'NEW',
          },
        ],
      }),
    );

    const itemToAdd: NewInfoListItem = {
      code: 'code3',
      label: 'label3',
      colour: null,
      iconName: 'some-namespace-to-strip:NEW',
    };

    service.load(1);

    service
      .addItem(itemToAdd)
      .pipe(
        switchMap(() => service.componentData$),
        take(1),
      )
      .subscribe((data) => {
        const expectedUrl = ['info-lists', '1', 'items', 'new'];
        expect(restService.post).toHaveBeenCalledWith(expectedUrl, itemToAdd);

        expect(data.infoList.items.length).toBe(3);
        expect(data.infoList.items[2].iconName).toBe('NEW');
      });
  });

  it('should delete items', async () => {
    const initialModel = getInitialModel();
    restService.getWithoutErrorHandling.and.returnValue(of(initialModel));
    const service: InfoListViewService = TestBed.inject(InfoListViewService);

    restService.delete.and.returnValue(of({}));

    service.load(1);

    service
      .deleteInfoListItems([1, 2])
      .pipe(
        switchMap(() => service.componentData$),
        take(1),
      )
      .subscribe((data) => {
        const expectedUrl = ['info-list-items', '1', 'items', '1,2'];
        expect(restService.delete).toHaveBeenCalledWith(expectedUrl);
        expect(data.infoList.items.length).toBe(0);
      });
  });

  it('change info list option positions', () => {
    const service: InfoListViewService = TestBed.inject(InfoListViewService);
    const initialModel = getInitialModel();
    initialModel.items = [
      {
        id: 1,
        label: 'label1',
        code: 'code1',
        colour: null,
        uri: '',
        friendlyLabel: '',
        iconName: '',
        isDefault: false,
        system: false,
        itemIndex: 0,
      },
      {
        id: 2,
        label: 'label2',
        code: 'code2',
        colour: null,
        uri: '',
        friendlyLabel: '',
        iconName: '',
        isDefault: false,
        system: false,
        itemIndex: 1,
      },
      {
        id: 3,
        label: 'label3',
        code: 'code3',
        colour: null,
        uri: '',
        friendlyLabel: '',
        iconName: '',
        isDefault: false,
        system: false,
        itemIndex: 2,
      },
      {
        id: 4,
        label: 'label4',
        code: 'code4',
        colour: null,
        uri: '',
        friendlyLabel: '',
        iconName: '',
        isDefault: false,
        system: false,
        itemIndex: 3,
      },
    ];

    restService.getWithoutErrorHandling.and.returnValue(of(initialModel));
    service.load(1);

    restService.post.and.returnValue(
      of({
        ...initialModel,
        items: [
          { ...initialModel.items[1], itemIndex: 0 },
          { ...initialModel.items[3], itemIndex: 1 },
          { ...initialModel.items[0], itemIndex: 2 },
          { ...initialModel.items[2], itemIndex: 3 },
        ],
      }),
    );

    service
      .changeOptionsPosition([1, 3], 1)
      .pipe(withLatestFrom(service.componentData$))
      .subscribe(([, data]) => {
        expect(data.infoList.items.map((item) => item.label)).toEqual([
          'label2',
          'label4',
          'label1',
          'label3',
        ]);
      });
  });

  function getInitialModel(): AdminInfoList {
    return {
      id: 1,
      label: 'infolist 1',
      code: '01',
      description: '',
      createdBy: '',
      createdOn: null,
      lastModifiedBy: null,
      lastModifiedOn: null,
      items: [
        {
          ...createInfoListItem(1),
          isDefault: true,
        },
        createInfoListItem(2),
      ],
    };
  }

  function createInfoListItem(id: number): AdminInfoListItem {
    return {
      id,
      label: 'label' + id,
      code: 'code' + id,
      colour: null,
      iconName: 'noicon',
      isDefault: false,
      itemIndex: 0,
      system: false,
      uri: '',
      friendlyLabel: '',
    };
  }
});
