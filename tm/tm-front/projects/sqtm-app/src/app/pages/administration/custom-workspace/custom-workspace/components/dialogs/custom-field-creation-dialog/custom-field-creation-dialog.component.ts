import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  InjectionToken,
  OnInit,
} from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import {
  CreationDialogData,
  CUF_DATE_FORMAT,
  deleteColumn,
  DialogReference,
  DisplayOption,
  Extendable,
  FieldValidationError,
  Fixed,
  GridColumnId,
  GridDefinition,
  GridService,
  gridServiceFactory,
  InputType,
  isCUFCodePatternValid,
  NOT_ONLY_SPACES_REGEX,
  ReferentialDataService,
  RestService,
  smallGrid,
  Sort,
  StyleDefinitionBuilder,
  textColumn,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { AbstractAdministrationCreationDialogDirective } from '../../../../../components/abstract-administration-creation-dialog';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, take } from 'rxjs/operators';
import {
  DropdownListOption,
  DropdownListOptionService,
} from '../../../services/dropdown-list-option.service';
import { DeleteDropdownListOptionComponent } from '../../cell-renderers/delete-dropdown-list-option/delete-dropdown-list-option.component';
import { dropdownListOptionColorPickerColumn } from '../../cell-renderers/dropdown-list-color-picker/dropdown-list-color-picker.component';
import { defaultDropdownListOptionColumn } from '../../cell-renderers/default-dropdown-list-option/default-dropdown-list-option.component';
import { format } from 'date-fns';

// Local enum just to avoid plain strings...
// It's only exported for test purposes
export enum CustomFieldCreationDialogFields {
  name = 'name',
  label = 'label',
  code = 'code',
  inputType = 'inputType',
  optional = 'optional',
  defaultText = 'defaultText',
  defaultRichText = 'defaultRichText',
  defaultChecked = 'defaultChecked',
  defaultDate = 'defaultDate',
  defaultTags = 'defaultTags',
  defaultNumeric = 'defaultNumeric',
  dropdownListOptionName = 'dropdownListOptionName',
  dropdownListOptionCode = 'dropdownListOptionCode',
}

const Fields = CustomFieldCreationDialogFields;

export const DROPDOWN_LIST_OPTIONS_TABLE_CONF = new InjectionToken(
  'DROPDOWN_LIST_OPTIONS_TABLE_CONF',
);
export const DROPDOWN_LIST_OPTIONS_TABLE = new InjectionToken('DROPDOWN_LIST_OPTIONS_TABLE');

export function dropdownListOptionsTableDefinition(): GridDefinition {
  return smallGrid('dropdown-list-options')
    .withColumns([
      textColumn(GridColumnId.optionName)
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Extendable(100, 1))
        .disableSort(),
      textColumn(GridColumnId.optionCode)
        .withI18nKey('sqtm-core.generic.label.code')
        .changeWidthCalculationStrategy(new Extendable(100, 1))
        .disableSort(),
      dropdownListOptionColorPickerColumn(GridColumnId.color)
        .withI18nKey('sqtm-core.entity.generic.color.label')
        .changeWidthCalculationStrategy(new Fixed(70))
        .disableSort(),
      defaultDropdownListOptionColumn(GridColumnId.default)
        .withI18nKey('sqtm-core.generic.label.default')
        .changeWidthCalculationStrategy(new Fixed(70))
        .withHeaderPosition('center')
        .disableSort(),
      deleteColumn(DeleteDropdownListOptionComponent),
    ])
    .withInitialSortedColumns([{ id: GridColumnId.label, sort: Sort.ASC }])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .build();
}

@Component({
  selector: 'sqtm-app-custom-field-creation-dialog',
  templateUrl: './custom-field-creation-dialog.component.html',
  styleUrls: ['./custom-field-creation-dialog.component.less'],
  providers: [
    {
      provide: DROPDOWN_LIST_OPTIONS_TABLE_CONF,
      useFactory: dropdownListOptionsTableDefinition,
    },
    {
      provide: DROPDOWN_LIST_OPTIONS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, DROPDOWN_LIST_OPTIONS_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: DROPDOWN_LIST_OPTIONS_TABLE,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomFieldCreationDialogComponent
  extends AbstractAdministrationCreationDialogDirective
  implements OnInit
{
  formGroup: FormGroup;
  dropdownListFormGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  data: CreationDialogData;
  inputTypeOptions: DisplayOption[];
  defaultCheckedOptions: DisplayOption[];
  private dropdownListOptions$: Observable<DropdownListOption[]>;
  private _hasDropdownListDefaultError = new BehaviorSubject<boolean>(false);
  public hasDropdownListDefaultError$ = this._hasDropdownListDefaultError.asObservable();

  private static getDefaultValueFieldName(inputType: InputType): string {
    switch (inputType) {
      case InputType.PLAIN_TEXT:
        return Fields.defaultText;
      case InputType.DATE_PICKER:
        return Fields.defaultDate;
      case InputType.TAG:
        return Fields.defaultTags;
      case InputType.RICH_TEXT:
        return Fields.defaultRichText;
      case InputType.CHECKBOX:
        return Fields.defaultChecked;
      case InputType.NUMERIC:
        return Fields.defaultNumeric;
      case InputType.DROPDOWN_LIST:
        return null;
    }

    throw new Error(`Unknown input type: ${inputType}.`);
  }

  private static getDropdownListOptionsForRequestPayload(
    opts: DropdownListOption[],
  ): DropdownRequest[] {
    return opts.map((option: DropdownListOption) => {
      return [option.optionName, option.optionCode, option.color];
    });
  }

  constructor(
    private fb: FormBuilder,
    private translateService: TranslateService,
    private gridService: GridService,
    private dropdownListOptionService: DropdownListOptionService,
    dialogReference: DialogReference,
    restService: RestService,
    cdr: ChangeDetectorRef,
  ) {
    super('custom-fields/new', dialogReference, restService, cdr);
    this.data = this.dialogReference.data;

    this.initializeDefaultCheckedOptions();
    this.initializeInputTypeField();
  }

  get textFieldToFocus(): string {
    return Fields.name;
  }

  get isDropdownList(): boolean {
    return this.getFormControlValue(Fields.inputType) === InputType.DROPDOWN_LIST;
  }

  get isCheckbox(): boolean {
    return this.getFormControlValue(Fields.inputType) === InputType.CHECKBOX;
  }

  get isMandatory(): boolean {
    return !this.getFormControlValue(Fields.optional);
  }

  ngOnInit(): void {
    this.initializeFormGroup();
    this.initializeDropdownListFormGroup();
    this.initializeDropdownListTable();
  }

  protected getRequestPayload(): Observable<any> {
    const inputType = this.getFormControlValue(Fields.inputType);

    return this.dropdownListOptions$.pipe(
      take(1),
      map((opts: DropdownListOption[]) => {
        const dropdownListOptions =
          CustomFieldCreationDialogComponent.getDropdownListOptionsForRequestPayload(opts);
        const defaultValue = this.retrieveDefaultValue(inputType, opts);
        const optional = this.isCheckbox || this.getFormControlValue(Fields.optional);

        return {
          name: this.getFormControlValue(Fields.name),
          label: this.getFormControlValue(Fields.label),
          code: this.getFormControlValue(Fields.code),
          inputType,
          optional,
          options: dropdownListOptions,
          defaultValue,
        };
      }),
    );
  }

  private retrieveDefaultValue(inputType: InputType, options: DropdownListOption[]) {
    const defaultValueFieldName =
      CustomFieldCreationDialogComponent.getDefaultValueFieldName(inputType);
    let defaultValue =
      defaultValueFieldName != null ? this.getFormControlValue(defaultValueFieldName) : null;

    switch (inputType) {
      case 'TAG':
        defaultValue = defaultValue.join('|');
        break;

      case 'CHECKBOX':
        defaultValue = defaultValue === 'true';
        break;

      case 'DROPDOWN_LIST': {
        const defaultOption: DropdownListOption = options.find((option) => option.default === true);
        defaultValue = defaultOption != null ? defaultOption.optionName : '';
        break;
      }
      case 'DATE_PICKER':
        defaultValue = defaultValue ? format(defaultValue, CUF_DATE_FORMAT) : '';
        break;
    }

    return defaultValue;
  }

  addEntity(addAnother?: boolean): void {
    this._hasDropdownListDefaultError.next(false);
    this.checkMandatoryDropdownListDefaultValue()
      .pipe(take(1))
      .subscribe((result: boolean) => {
        if (result) {
          super.addEntity(addAnother);
        } else {
          this._hasDropdownListDefaultError.next(true);
        }
      });
  }

  protected doResetForm(): void {
    this.resetFormControl(Fields.name, '');
    this.resetFormControl(Fields.label, '');
    this.resetFormControl(Fields.code, '');
    this.resetFormControl(Fields.inputType, this.inputTypeOptions[0].id);
    this.resetFormControl(Fields.optional, true);
    this.resetFormControl(Fields.defaultText, '');
    this.resetFormControl(Fields.defaultRichText, '');
    this.resetFormControl(Fields.defaultChecked, 'false');
    this.resetFormControl(Fields.defaultDate, null);
    this.resetFormControl(Fields.defaultTags, []);
    this.resetFormControl(Fields.defaultNumeric, null);
    this.resetDropdownListForm();
    this.dropdownListOptionService.initialize();
  }

  private initializeInputTypeField(): void {
    this.inputTypeOptions = this.retrieveAllInputTypesAsDisplayOption();
  }

  private retrieveAllInputTypesAsDisplayOption(): DisplayOption[] {
    return [
      this.transformInputTypeInDisplayOption(InputType.PLAIN_TEXT),
      this.transformInputTypeInDisplayOption(InputType.CHECKBOX),
      this.transformInputTypeInDisplayOption(InputType.DROPDOWN_LIST),
      this.transformInputTypeInDisplayOption(InputType.RICH_TEXT),
      this.transformInputTypeInDisplayOption(InputType.DATE_PICKER),
      this.transformInputTypeInDisplayOption(InputType.TAG),
      this.transformInputTypeInDisplayOption(InputType.NUMERIC),
    ];
  }

  private transformInputTypeInDisplayOption(type: InputType): DisplayOption {
    return {
      id: type,
      label: this.translateService.instant('sqtm-core.entity.custom-field.' + type),
    };
  }

  private initializeDefaultCheckedOptions(): void {
    this.defaultCheckedOptions = [
      {
        id: 'false',
        label: this.translateService.instant('sqtm-core.generic.label.false'),
      },
      {
        id: 'true',
        label: this.translateService.instant('sqtm-core.generic.label.true'),
      },
    ];
  }

  private initializeFormGroup(): void {
    this.formGroup = this.fb.group(
      {
        name: this.fb.control('', [
          Validators.required,
          Validators.pattern(NOT_ONLY_SPACES_REGEX),
          Validators.maxLength(255),
        ]),
        label: this.fb.control('', [Validators.required, Validators.maxLength(255)]),
        code: this.fb.control('', [
          Validators.required,
          Validators.maxLength(30),
          this.codeValidator,
        ]),
        inputType: this.fb.control(this.inputTypeOptions[0].id),
        optional: this.fb.control(true),
        defaultText: this.fb.control(''),
        defaultRichText: this.fb.control(''),
        defaultChecked: this.fb.control('false'),
        defaultDate: this.fb.control(null),
        defaultTags: this.fb.control([]),
        defaultNumeric: this.fb.control(null),
      },
      {
        validators: [this.validateDefaultValue],
      },
    );
  }

  private validateDefaultValue(group: FormGroup): void {
    // Reset errors
    [
      Fields.defaultText,
      Fields.defaultDate,
      Fields.defaultTags,
      Fields.defaultRichText,
      Fields.defaultNumeric,
    ].forEach((field) => group.controls[field].setErrors(null));

    const optional = group.get(Fields.optional).value;
    const inputType = group.get(Fields.inputType).value;

    if (!optional && inputType !== InputType.CHECKBOX) {
      CustomFieldCreationDialogComponent.validateRequiredDefaultValue(inputType, group);
    } else {
      CustomFieldCreationDialogComponent.validateOptionalDefaultValue(inputType, group);
    }
  }

  private static validateRequiredDefaultValue(inputType, group: FormGroup) {
    const fieldName = CustomFieldCreationDialogComponent.getDefaultValueFieldName(inputType);

    switch (inputType) {
      case InputType.PLAIN_TEXT:
      case InputType.RICH_TEXT:
        if (group.get(fieldName).value.trim() === '') {
          group.controls[fieldName].setErrors({ required: true });
        }
        break;
      case InputType.DATE_PICKER: {
        const dateCandidate = group.get(fieldName).value;
        if (!dateCandidate || dateCandidate === '') {
          group.controls[fieldName].setErrors({ required: true });
        }
        break;
      }
      case InputType.TAG: {
        const tags = group.get(fieldName).value;

        if (tags == null || tags.length === 0) {
          group.controls[fieldName].setErrors({ required: true });
        }
        break;
      }
      case InputType.NUMERIC: {
        const numericValueCandidate = group.get(fieldName).value;
        if (numericValueCandidate == null) {
          group.controls[fieldName].setErrors({ required: true });
        } else {
          CustomFieldCreationDialogComponent.validateNumber(
            numericValueCandidate,
            group,
            fieldName,
          );
        }
        break;
      }
      default:
        break;
    }
  }

  private static validateOptionalDefaultValue(inputType: InputType, group: FormGroup) {
    const fieldName = CustomFieldCreationDialogComponent.getDefaultValueFieldName(inputType);
    if (inputType === InputType.NUMERIC) {
      const numericValueCandidate = group.get(fieldName).value;
      CustomFieldCreationDialogComponent.validateNumber(numericValueCandidate, group, fieldName);
    }
  }

  private static validateNumber(numericValueCandidate, group: FormGroup, fieldName: string) {
    if (numericValueCandidate && numericValueCandidate.length > 0) {
      const asFloat = Number.parseFloat(numericValueCandidate);
      if (isNaN(asFloat)) {
        group.controls[fieldName].setErrors({ invalidNumber: true });
      }
    }
  }

  /* Dropdown list options */

  addDropdownListOption(): void {
    const optionName = this.getDropdownListFormControlValue(Fields.dropdownListOptionName);
    const optionCode = this.getDropdownListFormControlValue(Fields.dropdownListOptionCode);

    if (this.dropdownListFormIsValid()) {
      this.dropdownListOptionService.addOption(optionName, optionCode);
      this.resetDropdownListForm();
    } else {
      this.showDropdownListFormGroupClientSideError();
    }
  }

  private initializeDropdownListFormGroup(): void {
    this.dropdownListFormGroup = this.fb.group({
      dropdownListOptionName: this.fb.control('', [Validators.maxLength(255), Validators.required]),
      dropdownListOptionCode: this.fb.control('', [
        Validators.maxLength(30),
        Validators.required,
        this.codeValidator,
      ]),
    });
  }

  private get codeValidator(): ValidatorFn {
    return function (formControl: AbstractControl): ValidationErrors {
      const isCodeValid = isCUFCodePatternValid(formControl.value);
      return isCodeValid ? null : { invalidCodePattern: true };
    };
  }

  private getDropdownListFormControl(fieldName: string): AbstractControl {
    return this.dropdownListFormGroup.controls[fieldName];
  }

  private getDropdownListFormControlValue(fieldName: string): any {
    return this.getDropdownListFormControl(fieldName).value;
  }

  private resetDropdownListFormControl(fieldName: string, value: any): void {
    this.getDropdownListFormControl(fieldName).reset(value);
  }

  private checkMandatoryDropdownListDefaultValue(): Observable<boolean> {
    if (this.isMandatoryDropdownList()) {
      return this.dropdownListOptions$.pipe(
        take(1),
        map((options: DropdownListOption[]) => {
          return options.find((option) => option.default === true) != null;
        }),
      );
    } else {
      return of(true);
    }
  }

  private dropdownListFormIsValid(): boolean {
    this.initializeErrorsForDuplicateOptionNameOrCode();
    return this.dropdownListFormGroup.status === 'VALID';
  }

  private initializeErrorsForDuplicateOptionNameOrCode(): void {
    const optionName = this.getDropdownListFormControlValue(Fields.dropdownListOptionName);
    const optionCode = this.getDropdownListFormControlValue(Fields.dropdownListOptionCode);
    this.dropdownListOptions$
      .pipe(
        take(1),
        map((options) => {
          if (this.checkOptionCodeAlreadyExist(optionCode, options)) {
            this.getDropdownListFormControl(Fields.dropdownListOptionCode).setErrors({
              optionCodeAlreadyExists: true,
            });
          }

          if (this.checkOptionNameAlreadyExist(optionName, options)) {
            this.getDropdownListFormControl(Fields.dropdownListOptionName).setErrors({
              optionNameAlreadyExists: true,
            });
          }
        }),
      )
      .subscribe();
  }

  private resetDropdownListForm(): void {
    this.resetDropdownListFormControl(Fields.dropdownListOptionName, '');
    this.resetDropdownListFormControl(Fields.dropdownListOptionCode, '');
    this.removeDropdownListFormGroupClientSideError();
    this.textFields
      .filter((textField) => textField.fieldName === Fields.dropdownListOptionName)
      .forEach((textField) => textField.grabFocus());
  }

  private initializeDropdownListTable(): void {
    this.dropdownListOptionService.initialize();
    this.dropdownListOptions$ = this.dropdownListOptionService.dropdownListOptions$;
    this.gridService.connectToDatasource(this.dropdownListOptions$, 'id');
  }

  private removeDropdownListFormGroupClientSideError(): void {
    this.serverSideValidationErrors = [];
    this.cdr.markForCheck();
  }

  private checkOptionCodeAlreadyExist(code: string, options: DropdownListOption[]): boolean {
    return options.find((option) => option.optionCode === code) != null;
  }

  private checkOptionNameAlreadyExist(name: string, options: DropdownListOption[]): boolean {
    return options.find((option) => option.optionName === name) != null;
  }

  private showDropdownListFormGroupClientSideError(): void {
    this.textFields
      .filter((textField) => {
        return (
          textField.fieldName === Fields.dropdownListOptionName ||
          textField.fieldName === Fields.dropdownListOptionCode
        );
      })
      .forEach((filteredTextField) => filteredTextField.showClientSideError());
  }

  private isMandatoryDropdownList(): boolean {
    return this.isMandatory && this.isDropdownList;
  }
}

type DropdownRequest = string[];
