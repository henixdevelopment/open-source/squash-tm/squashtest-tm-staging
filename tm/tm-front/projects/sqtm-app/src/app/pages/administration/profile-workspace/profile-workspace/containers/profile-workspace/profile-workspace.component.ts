import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  GRID_PERSISTENCE_KEY,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import { Observable, Subject } from 'rxjs';
import { DatePipe } from '@angular/common';
import {
  ADMIN_WS_PROFILE_TABLE,
  ADMIN_WS_PROFILE_TABLE_CONFIG,
} from '../../../profile-workspace.constant';
import { adminProfileTableDefinition } from '../profile-grid/profile-grid.component';
import { filter, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ProfileService } from '../../services/profile.service';

@Component({
  selector: 'sqtm-app-profile-workspace',
  templateUrl: './profile-workspace.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    DatePipe,
    {
      provide: ADMIN_WS_PROFILE_TABLE_CONFIG,
      useFactory: adminProfileTableDefinition,
      deps: [],
    },
    {
      provide: ADMIN_WS_PROFILE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, ADMIN_WS_PROFILE_TABLE_CONFIG, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: ADMIN_WS_PROFILE_TABLE,
    },
    {
      provide: GRID_PERSISTENCE_KEY,
      useValue: 'profile-workspace-main-grid',
    },
    {
      provide: ProfileService,
    },
  ],
})
export class ProfileWorkspaceComponent implements OnInit, OnDestroy {
  authenticatedAdmin$: Observable<AuthenticatedUser>;

  private unsub$ = new Subject<void>();

  constructor(
    public readonly adminReferentialDataService: AdminReferentialDataService,
    private gridService: GridService,
    protected router: Router,
  ) {}

  ngOnInit(): void {
    this.adminReferentialDataService.refresh().subscribe();

    this.authenticatedAdmin$ = this.adminReferentialDataService.authenticatedUser$.pipe(
      takeUntil(this.unsub$),
      filter((authUser: AuthenticatedUser) => authUser.admin),
    );

    this.adminReferentialDataService.authenticatedUser$
      .pipe(
        takeUntil(this.unsub$),
        filter((authUser: AuthenticatedUser) => !authUser.admin),
      )
      .subscribe(() => this.router.navigate(['home-workspace']));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
    this.gridService.complete();
  }
}
