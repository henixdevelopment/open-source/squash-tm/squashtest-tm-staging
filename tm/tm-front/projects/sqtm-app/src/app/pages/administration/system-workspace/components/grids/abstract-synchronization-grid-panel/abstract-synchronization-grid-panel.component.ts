import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';

import {
  AdminReferentialDataService,
  booleanColumn,
  booleanFilter,
  buildFilters,
  DataRow,
  dateTimeColumn,
  Extendable,
  FilterBuilder,
  FilterOperation,
  Fixed,
  grid,
  GridColumnId,
  GridDefinition,
  GridFilter,
  GridId,
  GridService,
  indexColumn,
  Limited,
  LocalPersistenceService,
  NumericFilterComponent,
  serverBackedGridTextFilter,
  Sort,
  StyleDefinitionBuilder,
  SynchronizationPlugin,
  SynchronizationPluginId,
  SynchronizationViewPlugin,
  synchronizedRequirementsRatioColumn,
  synchronizedSprintTicketsRatioColumn,
  syncStatusColumn,
  textColumn,
  withLinkColumn,
  WithOperationFilterValueRendererComponent,
} from 'sqtm-core';
import { synchronizationStatusFilter } from '../../filters/filter-synchronization-status/filter-synchronization-status.component';
import { filter, map, take, takeUntil, withLatestFrom } from 'rxjs/operators';
import { SystemViewService } from '../../../services/system-view.service';
import { Observable, Subject } from 'rxjs';
import { SystemViewState } from '../../../states/system-view.state';
import { withServerLinkColumnForAdmin } from '../../cell-renderers/server-link-for-admin-cell/server-link-cell-for-admin.component';
import { syncErrorLogDownloadColumn } from '../../cell-renderers/download-error-log-file-cell/download-error-log-file-cell.component';
import { remoteSelectTypeColumn } from '../../cell-renderers/remote-select-type-cell/remote-select-type-cell.component';

export function remoteSyncTableDefinition(
  gridId: GridId,
  localPersistenceService: LocalPersistenceService,
): GridDefinition {
  const projectUrlFunction = (row: DataRow) => {
    return [
      '/',
      'administration-workspace',
      'projects',
      'detail',
      row.data.projectId.toString(),
      'plugins',
    ];
  };

  return grid(gridId)
    .withColumns([
      indexColumn().withViewport('leftViewport'),
      textColumn(GridColumnId.id)
        .withI18nKey(GridColumnId.id)
        .changeWidthCalculationStrategy(new Fixed(60))
        .withAssociatedFilter(),
      textColumn(GridColumnId.name)
        .withI18nKey('sqtm-core.entity.name')
        .changeWidthCalculationStrategy(new Limited(200))
        .withAssociatedFilter(),
      withLinkColumn(GridColumnId.projectName, {
        kind: 'link',
        createUrlFunction: projectUrlFunction,
        saveGridStateBeforeNavigate: false,
      })
        .withI18nKey('sqtm-core.entity.project.label.singular')
        .changeWidthCalculationStrategy(new Limited(200))
        .withAssociatedFilter(),
      withServerLinkColumnForAdmin(GridColumnId.serverName)
        .withI18nKey('sqtm-core.entity.server.label')
        .changeWidthCalculationStrategy(new Limited(200))
        .withAssociatedFilter(),
      textColumn(GridColumnId.perimeter)
        .withI18nKey('sqtm-core.generic.label.perimeter')
        .changeWidthCalculationStrategy(new Limited(150))
        .withAssociatedFilter(),
      remoteSelectTypeColumn(GridColumnId.remoteSelectType)
        .withI18nKey('sqtm-core.entity.remote-sync.select-type.label')
        .changeWidthCalculationStrategy(new Limited(200))
        .withAssociatedFilter(),
      dateTimeColumn(GridColumnId.lastSyncDate)
        .withI18nKey('sqtm-core.entity.remote-sync.last-sync-date')
        .changeWidthCalculationStrategy(new Extendable(120)),
      dateTimeColumn(GridColumnId.lastSuccessfulSyncDate)
        .withI18nKey('sqtm-core.entity.remote-sync.last-successful-sync-date')
        .changeWidthCalculationStrategy(new Extendable(120)),
      booleanColumn(GridColumnId.enabled)
        .withI18nKey('sqtm-core.entity.remote-sync.activated')
        .withHeaderPosition('center')
        .withContentPosition('center')
        .changeWidthCalculationStrategy(new Fixed(100))
        .withAssociatedFilter(),
      synchronizedRequirementsRatioColumn(GridColumnId.synchronizedRequirementsRatio)
        .withI18nKey('sqtm-core.entity.remote-sync.synchronized-requirements-ratio.short')
        .withTitleI18nKey('sqtm-core.entity.remote-sync.synchronized-requirements-ratio.full')
        .disableSort()
        .changeWidthCalculationStrategy(new Limited(100)),
      synchronizedSprintTicketsRatioColumn(GridColumnId.synchronizedSprintTicketsRatio)
        .withI18nKey('sqtm-core.entity.remote-sync.synchronized-sprint-tickets-ratio.short')
        .withTitleI18nKey('sqtm-core.entity.remote-sync.synchronized-sprint-tickets-ratio.full')
        .disableSort()
        .changeWidthCalculationStrategy(new Limited(100)),
      syncErrorLogDownloadColumn(GridColumnId.syncErrorLogFilePath).changeWidthCalculationStrategy(
        new Fixed(40),
      ),
      syncStatusColumn(GridColumnId.status)
        .withI18nKey('sqtm-core.generic.label.status')
        .withHeaderPosition('center')
        .withContentPosition('center')
        .withViewport('rightViewport')
        .changeWidthCalculationStrategy(new Fixed(80))
        .withAssociatedFilter(),
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withInitialSortedColumns([{ id: GridColumnId.status, sort: Sort.ASC }])
    .enableColumnWidthPersistence(localPersistenceService)
    .disableRightToolBar()
    .withRowHeight(35)
    .build();
}

@Component({
  selector: 'sqtm-app-abstract-synchronization-grid-panel',
  templateUrl: './abstract-synchronization-grid-panel.component.html',
  styleUrls: ['./abstract-synchronization-grid-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AbstractSynchronizationGridPanelComponent implements OnInit, OnDestroy {
  unsub$ = new Subject<void>();
  componentData$: Observable<SystemViewState>;

  protected pluginId: SynchronizationPluginId;

  constructor(
    public adminReferentialDataService: AdminReferentialDataService,
    protected systemViewService: SystemViewService,
    protected gridService: GridService,
  ) {
    this.componentData$ = this.systemViewService.componentData$.pipe(takeUntil(this.unsub$));
  }

  ngOnInit(): void {
    this.adminReferentialDataService.adminReferentialData$
      .pipe(
        take(1),
        filter((adminReferentialData) =>
          this.isPluginInstalled(adminReferentialData.synchronizationPlugins),
        ),
        withLatestFrom(this.componentData$),
      )
      .subscribe(([, componentData]) => {
        this.initializeGrid();
        const syncPlugin = componentData.synchronisationPlugins.find(
          (plugin) => plugin.id === this.pluginId,
        );
        this.initializeFilters(syncPlugin);
        this.toggleColumnsVisibility(syncPlugin);
      });
  }

  private initializeGrid() {
    this.gridService.connectToDatasource(this.getGridDatasource(), GridColumnId.id);
  }

  private initializeFilters(syncPlugin: SynchronizationViewPlugin) {
    const filters = this.buildDefaultFilters();

    if (syncPlugin.supervisionScreenData.hasSyncNameColumn) {
      filters.push(serverBackedGridTextFilter(GridColumnId.name).build());
    }

    if (syncPlugin.supervisionScreenData.hasPerimeterColumn) {
      filters.push(serverBackedGridTextFilter(GridColumnId.perimeter).build());
    }

    this.gridService.addFilters(filters);
  }

  private toggleColumnsVisibility(syncPlugin: SynchronizationViewPlugin) {
    this.gridService.setColumnVisibility(
      GridColumnId.name,
      syncPlugin.supervisionScreenData.hasSyncNameColumn,
    );

    this.gridService.setColumnVisibility(
      GridColumnId.perimeter,
      syncPlugin.supervisionScreenData.hasPerimeterColumn,
    );

    this.gridService.setColumnVisibility(
      GridColumnId.remoteSelectType,
      syncPlugin.supervisionScreenData.hasRemoteSelectTypeColumn,
    );

    this.gridService.setColumnVisibility(
      GridColumnId.synchronizedRequirementsRatio,
      syncPlugin.supervisionScreenData.hasSynchronizedRequirementsRatioColumn,
    );
    this.gridService.setColumnVisibility(
      GridColumnId.synchronizedSprintTicketsRatio,
      syncPlugin.supervisionScreenData.hasSynchronizedSprintTicketsRatioColumn,
    );
  }

  private getGridDatasource() {
    return this.componentData$.pipe(
      takeUntil(this.unsub$),
      map((componentData) => {
        const syncPlugin = componentData.synchronisationPlugins.find(
          (plugin) => plugin.id === this.pluginId,
        );
        return syncPlugin?.remoteSynchronisations;
      }),
    );
  }

  buildDefaultFilters(): GridFilter[] {
    return buildFilters([
      this.buildIdFilter(),
      serverBackedGridTextFilter(GridColumnId.projectName),
      serverBackedGridTextFilter(GridColumnId.serverName),
      booleanFilter(GridColumnId.enabled).alwaysActive().withInitialValue({
        kind: 'multiple-boolean-value',
        value: [],
      }),
      synchronizationStatusFilter(GridColumnId.status, []),
    ]);
  }

  private buildIdFilter() {
    return new FilterBuilder(GridColumnId.id)
      .alwaysActive()
      .withWidget(NumericFilterComponent)
      .withInitialTextValue('')
      .withValueRenderer(WithOperationFilterValueRendererComponent)
      .withOperations(FilterOperation.LIKE)
      .withAvailableOperations([FilterOperation.LIKE]);
  }

  getPluginTitle() {
    return this.componentData$.pipe(
      take(1),
      map((componentData) => {
        const syncPlugin = componentData.synchronisationPlugins.find(
          (plugin) => plugin.id === this.pluginId,
        );
        return syncPlugin?.name;
      }),
    );
  }

  isPluginInstalled(synchronizationPlugins: SynchronizationPlugin[]) {
    const installedSyncPluginIds = synchronizationPlugins.map((plugin) => plugin.id);
    return installedSyncPluginIds.includes(this.pluginId);
  }

  ngOnDestroy(): void {
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  hasSupervisionScreen() {
    return this.componentData$.pipe(
      take(1),
      map((componentData) => {
        const syncPlugin = componentData.synchronisationPlugins.find(
          (plugin) => plugin.id === this.pluginId,
        );
        return syncPlugin?.hasSupervisionScreen;
      }),
    );
  }
}
