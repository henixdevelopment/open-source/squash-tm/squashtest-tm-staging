import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  DataRow,
  GridColumnId,
  GridService,
} from 'sqtm-core';
import { DropdownListOptionService } from '../../../services/dropdown-list-option.service';

@Component({
  selector: 'sqtm-app-default-dropdown-list-option-cell-renderer',
  template: ` @if (columnDisplay && row) {
    <div class="full-width full-height flex-column">
      <label
        nz-checkbox
        style="margin: auto"
        [nzChecked]="isCurrentDefault"
        (nzCheckedChange)="handleClick(row)"
      >
      </label>
    </div>
  }`,
  styleUrls: ['./default-dropdown-list-option.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DefaultDropdownListOptionComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    private dropdownListOptionService: DropdownListOptionService,
  ) {
    super(grid, cdRef);
  }

  get isCurrentDefault(): boolean {
    const isDefault = this.row.data[this.columnDisplay.id];
    return Boolean(isDefault);
  }

  handleClick(row: DataRow) {
    this.dropdownListOptionService.toggleDefault(row.data[GridColumnId.optionName]);
  }
}

export function defaultDropdownListOptionColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(DefaultDropdownListOptionComponent);
}
