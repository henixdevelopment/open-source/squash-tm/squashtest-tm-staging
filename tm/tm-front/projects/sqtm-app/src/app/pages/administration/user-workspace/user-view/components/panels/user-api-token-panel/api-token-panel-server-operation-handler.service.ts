import {
  AbstractServerOperationHandler,
  DialogService,
  GridService,
  GridState,
  Identifier,
  RestService,
} from 'sqtm-core';
import { Inject, Injectable, InjectionToken } from '@angular/core';
import { Observable, of, Subject, switchMap } from 'rxjs';
import { filter, finalize, map, takeUntil, tap, withLatestFrom } from 'rxjs/operators';

export const DELETE_API_TOKEN_URL_INJECTION_TOKEN = new InjectionToken<string>(
  'Injection token for the url to delete an API token',
);

@Injectable()
export class ApiTokenPanelServerOperationHandler extends AbstractServerOperationHandler {
  canDelete$: Observable<boolean> = of(true);
  private unsub$ = new Subject<void>();
  private _grid: GridService;

  constructor(
    @Inject(DELETE_API_TOKEN_URL_INJECTION_TOKEN) private readonly url: string,
    private readonly dialogService: DialogService,
    private readonly restService: RestService,
  ) {
    super();
  }

  get grid() {
    return this._grid;
  }

  set grid(grid: GridService) {
    this._grid = grid;
    this.canDelete$ = grid.selectedRowIds$.pipe(map((rowIds: Identifier[]) => rowIds.length > 0));
  }

  delete(): void {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.user-account-page.api-token.dialog.delete.title',
      messageKey: 'sqtm-core.user-account-page.api-token.dialog.delete.message-basic',
      level: 'DANGER',
    });

    dialogReference.dialogClosed$
      .pipe(
        takeUntil(this.unsub$),
        filter((result) => result === true),
        tap(() => this.grid.beginAsyncOperation()),
        withLatestFrom(this.grid.selectedRowIds$),
        switchMap(([, selectedRowIds]: [any, Identifier[]]) =>
          this.restService.delete([this.url, selectedRowIds[0].toString()]),
        ),
        finalize(() => this.grid.completeAsyncOperation()),
      )
      .subscribe(() => this.grid.refreshData());
  }

  copy(): void {
    // NOOP
  }
  paste(): void {
    // NOOP
  }
  notifyInternalDrop(): void {
    // NOOP
  }
  allowDropSibling(_id: Identifier, _state: GridState): boolean {
    // NOOP
    return false;
  }
  allowDropInto(_id: Identifier, _state: GridState): boolean {
    // NOOP
    return false;
  }
}
