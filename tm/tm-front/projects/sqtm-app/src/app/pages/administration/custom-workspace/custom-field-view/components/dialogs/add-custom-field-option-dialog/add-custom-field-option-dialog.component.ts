import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import {
  DialogReference,
  FieldValidationError,
  GridService,
  isOptionCodePatternValid,
  RestService,
} from 'sqtm-core';
import { CustomFieldViewService } from '../../../services/custom-field-view.service';
import { AbstractAdministrationCreationDialogDirective } from '../../../../../components/abstract-administration-creation-dialog';
import { of } from 'rxjs';
import { finalize, switchMap, take } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-add-custom-field-option-dialog',
  templateUrl: './add-custom-field-option-dialog.component.html',
  styleUrls: ['./add-custom-field-option-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddCustomFieldOptionDialogComponent
  extends AbstractAdministrationCreationDialogDirective
  implements OnInit
{
  color = null;
  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];

  constructor(
    protected dialogReference: DialogReference,
    protected restService: RestService,
    protected cdr: ChangeDetectorRef,
    private fb: FormBuilder,
    private customFieldViewService: CustomFieldViewService,
    private grid: GridService,
  ) {
    super('', dialogReference, restService, cdr);
  }

  get textFieldToFocus(): string {
    return 'label';
  }

  ngOnInit(): void {
    this.initializeFormGroup();
  }

  changeColor(newColor: string) {
    this.color = newColor;
  }

  addEntity(addAnother?: boolean) {
    if (this.formIsValid()) {
      this.beginAsync();
      this.grid.beginAsyncOperation();
      this.getRequestPayload()
        .pipe(
          take(1),
          switchMap((payload) => this.customFieldViewService.addOption(payload)),
          finalize(() => this.grid.completeAsyncOperation()),
        )
        .subscribe({
          next: (result) => {
            this.handleCreationSuccess(result, addAnother);
          },
          error: (error) => {
            this.handleCreationFailure(error);
          },
        });
    } else {
      this.showClientSideErrors();
    }
  }

  protected getRequestPayload() {
    return of({
      label: this.getFormControlValue('label'),
      code: this.getFormControlValue('code'),
      colour: this.color,
    });
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      label: this.fb.control('', [Validators.maxLength(255), Validators.required]),
      code: this.fb.control('', [
        Validators.maxLength(30),
        Validators.required,
        this.codeValidator,
      ]),
    });
  }

  get codeValidator(): ValidatorFn {
    return function (formControl: AbstractControl): ValidationErrors {
      const isCodeValid = isOptionCodePatternValid(formControl.value);
      return isCodeValid ? null : { invalidCodePattern: true };
    };
  }

  protected doResetForm(): void {
    this.resetFormControl('label', '');
    this.resetFormControl('code', '');
    this.color = '';
  }
}
