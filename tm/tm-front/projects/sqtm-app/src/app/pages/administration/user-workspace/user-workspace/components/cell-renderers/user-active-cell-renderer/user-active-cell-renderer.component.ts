import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import {
  AbstractCellRendererComponent,
  AdminReferentialDataService,
  AuthenticatedUser,
  BooleanValueRenderer,
  ColumnDefinitionBuilder,
  DialogService,
  GridColumnId,
  GridService,
  RestService,
} from 'sqtm-core';
import { Observable, Subject } from 'rxjs';
import { concatMap, filter, take } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-user-active-cell-renderer',
  template: ` @if (columnDisplay && row) {
    <div class="full-width full-height flex-column">
      <span class="template-icon-container" (click)="changeUserStatus()">
        <i
          nz-icon
          [ngClass]="iconColor"
          nzType="sqtm-core-administration:user"
          nzTheme="outline"
          nz-tooltip
          [nzTooltipTitle]="tooltipTitle | translate"
          class="table-icon-size"
        ></i>
      </span>
    </div>
  }`,
  styleUrls: ['./user-active-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserActiveCellRendererComponent
  extends AbstractCellRendererComponent
  implements OnDestroy
{
  unsub$ = new Subject<void>();
  authenticatedUser$: Observable<AuthenticatedUser>;

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    private restService: RestService,
    private adminReferentialDataService: AdminReferentialDataService,
    private dialogService: DialogService,
  ) {
    super(grid, cdRef);
    this.authenticatedUser$ = adminReferentialDataService.authenticatedUser$;
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  get iconColor(): string {
    const isActive = this.row.data[this.columnDisplay.id];
    return isActive ? 'active' : 'inactive';
  }

  get tooltipTitle(): string {
    const isActive = this.row.data[this.columnDisplay.id];
    return isActive
      ? 'sqtm-core.entity.user.tooltip.deactivate.single'
      : 'sqtm-core.entity.user.tooltip.activate.single';
  }

  changeUserStatus() {
    const isActive = this.row.data[this.columnDisplay.id];
    const userdId = this.row.data[GridColumnId.partyId];
    isActive ? this.deactivateUser(userdId) : this.activateUser(userdId);
  }

  private activateUser(userdId: string) {
    this.restService.post(['users', userdId, 'activate']).subscribe(() => {
      this.grid.refreshData();
    });
  }

  private deactivateUser(userId: string) {
    this.showCurrentUserWarningIfRequired(userId);
    this.authenticatedUser$
      .pipe(
        take(1),
        filter((user) => user.userId.toString() !== userId.toString()),
        concatMap(() => this.deactivateUserServerSide(userId)),
      )
      .subscribe(() => this.grid.refreshData());
  }

  private showCurrentUserWarningIfRequired(userId) {
    this.authenticatedUser$
      .pipe(
        take(1),
        filter((user) => user.userId.toString() === userId.toString()),
      )
      .subscribe(() => this.openAlertForSelectedUserIsCurrentUser());
  }

  private openAlertForSelectedUserIsCurrentUser() {
    this.dialogService.openAlert({
      level: 'DANGER',
      messageKey:
        'sqtm-core.administration-workspace.users.dialog.message.deactivate-current-user-alert',
      titleKey: 'sqtm-core.generic.label.error',
    });
  }

  private deactivateUserServerSide(userId) {
    return this.restService.post([`users`, userId, 'deactivate']);
  }
}

export function userActiveColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(UserActiveCellRendererComponent)
    .withExportValueRenderer(BooleanValueRenderer);
}
