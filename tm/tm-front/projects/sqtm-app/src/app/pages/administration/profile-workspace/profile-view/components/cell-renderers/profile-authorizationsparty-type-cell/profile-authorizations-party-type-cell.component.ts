import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-profile-authorizations-party-type-cell',
  template: ` @if (columnDisplay && row) {
    <div class="full-width full-height flex-column" [class.disabled-row]="row.disabled">
      <span class="m-auto-0">
        {{ cellText | translate }}
      </span>
    </div>
  }`,
  styleUrls: ['./profile-authorizations-party-type-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileAuthorizationsPartyTypeCellComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }

  get cellText(): string {
    const isUser = !this.row.data[GridColumnId.team];
    return isUser ? 'sqtm-core.entity.user.label.singular' : 'sqtm-core.entity.team.label';
  }
}

export function partyTypeColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(ProfileAuthorizationsPartyTypeCellComponent)
    .withI18nKey('sqtm-core.administration-workspace.views.project.permissions.type');
}

export function buildPartyType(translateService: TranslateService) {
  return (keyA: any, keyB: any) => sortProfile(keyA, keyB, translateService);
}

function sortProfile(roleA: boolean, roleB: boolean, translateService: TranslateService) {
  const keyPermissionTypeA = roleA
    ? 'sqtm-core.entity.user.label.singular'
    : 'sqtm-core.entity.team.label';
  const keyPermissionTypeB = roleB
    ? 'sqtm-core.entity.user.label.singular'
    : 'sqtm-core.entity.team.label';
  const translatedRoleA = translateService.instant(keyPermissionTypeA);
  const translatedRoleB = translateService.instant(keyPermissionTypeB);
  return translatedRoleA.localeCompare(translatedRoleB);
}
