import { ChangeDetectionStrategy, Component, Input, ViewChild } from '@angular/core';
import { SystemViewState } from '../../../states/system-view.state';
import { SystemViewService } from '../../../services/system-view.service';
import { EditableNumericFieldComponent, EditableTextFieldComponent } from 'sqtm-core';

@Component({
  selector: 'sqtm-app-system-attachments-panel',
  templateUrl: './system-attachments-panel.component.html',
  styleUrls: [
    './system-attachments-panel.component.less',
    '../../../styles/system-workspace.common.less',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SystemAttachmentsPanelComponent {
  @Input()
  componentData: SystemViewState;

  @ViewChild(EditableTextFieldComponent)
  private whiteListEditableField: EditableTextFieldComponent;

  @ViewChild('uploadSizeLimitNumericField')
  private uploadSizeLimitNumericField: EditableNumericFieldComponent;

  @ViewChild('importSizeLimitNumericField')
  private importSizeLimitNumericField: EditableNumericFieldComponent;

  constructor(private systemViewService: SystemViewService) {}

  changeWhiteList(whiteList: string) {
    this.systemViewService.changeWhiteList(whiteList).subscribe((state: SystemViewState) => {
      this.whiteListEditableField.value = state.whiteList;
    });
  }

  changeUploadSizeLimit(size: number) {
    this.systemViewService
      .changeUploadSizeLimit(size.toString())
      .subscribe((state: SystemViewState) => {
        this.uploadSizeLimitNumericField.value = Number(state.uploadSizeLimit);
      });
  }

  changeImportSizeLimit(size: number) {
    this.systemViewService
      .changeImportSizeLimit(size.toString())
      .subscribe((state: SystemViewState) => {
        this.importSizeLimitNumericField.value = Number(state.importSizeLimit);
      });
  }

  changeUnsafeAttachmentPreviewEnabled() {
    this.systemViewService.changeUnsafeAttachmentPreviewEnabled(
      !this.componentData.unsafeAttachmentPreviewEnabled,
    );
  }
}
