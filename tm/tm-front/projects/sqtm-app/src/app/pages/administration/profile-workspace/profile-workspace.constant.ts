import { InjectionToken } from '@angular/core';
import { GridDefinition, GridService } from 'sqtm-core';

export const ADMIN_WS_PROFILE_TABLE_CONFIG = new InjectionToken<GridDefinition>(
  'Grid config instance for the admin workspace profile table',
);
export const ADMIN_WS_PROFILE_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the admin workspace profile table',
);

export const PROFILE_AUTHORIZATIONS_TABLE_CONF = new InjectionToken(
  'PROFILE_AUTHORIZATIONS_TABLE_CONF',
);
export const PROFILE_AUTHORIZATIONS_TABLE = new InjectionToken('PROFILE_AUTHORIZATIONS_TABLE');
