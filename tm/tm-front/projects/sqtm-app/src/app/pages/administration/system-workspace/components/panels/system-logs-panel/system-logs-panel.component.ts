import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { SystemViewState } from '../../../states/system-view.state';
import { RestService } from 'sqtm-core';

@Component({
  selector: 'sqtm-app-system-logs-panel',
  templateUrl: './system-logs-panel.component.html',
  styleUrls: ['./system-logs-panel.component.less', '../../../styles/system-workspace.common.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SystemLogsPanelComponent {
  @Input() componentData: SystemViewState;

  get latestLink(): string {
    return `${this.restService.backendRootUrl}system/logs/latest`;
  }

  get hasPreviousLogFiles(): boolean {
    return this.previousLogFiles.length > 0;
  }

  get previousLogFiles(): string[] {
    const files =
      this.componentData.logFiles?.filter((filename) => !filename.endsWith('.log')) || [];
    files.sort((a, b) => a.localeCompare(b));
    files.reverse();
    return files;
  }

  constructor(private readonly restService: RestService) {}

  getLink(filename: string): any {
    return this.restService.buildExportUrlWithParams('system/logs', { filename });
  }
}
