import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  ViewContainerRef,
} from '@angular/core';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  basicExternalLinkColumn,
  DataRow,
  DialogService,
  Extendable,
  FilterOperation,
  Fixed,
  grid,
  GridColumnId,
  GridDefinition,
  GridService,
  indexColumn,
  RestService,
  selectableTextColumn,
  textColumn,
  WorkspaceWithGridComponent,
} from 'sqtm-core';
import { Observable, Subject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { AbstractAdministrationNavigation } from '../../../../components/abstract-administration-navigation';
import { concatMap, filter, map, switchMap, take, takeUntil, tap } from 'rxjs/operators';
import { AiServerCreationDialogComponent } from '../../components/dialogs/ai-server-creation-dialog/ai-server-creation-dialog.component';
import { deleteAiServerColumn } from '../../components/cell-renderers/delete-ai-server/delete-ai-server.component';

export function adminAiServersTableDefinition(): GridDefinition {
  return grid('ai-servers')
    .withColumns([
      indexColumn().changeWidthCalculationStrategy(new Fixed(60)).withViewport('leftViewport'),
      selectableTextColumn(GridColumnId.name)
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Extendable(120, 0.2)),
      basicExternalLinkColumn(GridColumnId.url)
        .withI18nKey('sqtm-core.entity.generic.url.label')
        .changeWidthCalculationStrategy(new Extendable(300, 0.1)),
      textColumn(GridColumnId.description)
        .withI18nKey('sqtm-core.grid.header.description.title')
        .changeWidthCalculationStrategy(new Extendable(300, 0.1)),
      deleteAiServerColumn(GridColumnId.delete).withViewport('rightViewport'),
    ])
    .server()
    .withServerUrl(['ai-servers'])
    .disableRightToolBar()
    .withRowHeight(35)
    .enableMultipleColumnsFiltering([GridColumnId.name])
    .build();
}

@Component({
  selector: 'sqtm-app-ai-server-grid',
  templateUrl: './ai-server-grid.component.html',
  styleUrls: ['./ai-server-grid.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AiServerGridComponent
  extends AbstractAdministrationNavigation
  implements AfterViewInit, OnDestroy
{
  authenticatedUser$: Observable<AuthenticatedUser>;

  unsub$ = new Subject<void>();

  protected readonly entityIdPositionInUrl = 3;

  constructor(
    public gridService: GridService,
    private restService: RestService,
    private dialogService: DialogService,
    private adminReferentialDataService: AdminReferentialDataService,
    private viewContainerRef: ViewContainerRef,
    public workspaceWithGrid: WorkspaceWithGridComponent,
    protected route: ActivatedRoute,
    protected router: Router,
  ) {
    super(route, router);
    this.workspaceWithGrid.entityIdPositionInUrl = this.entityIdPositionInUrl;

    this.authenticatedUser$ = adminReferentialDataService.authenticatedUser$;
  }

  ngAfterViewInit() {
    this.addFilters();
    this.gridService.refreshData();
  }

  private addFilters() {
    this.gridService.addFilters([
      {
        id: GridColumnId.name,
        active: false,
        initialValue: { kind: 'single-string-value', value: '' },
        tiedToPerimeter: false,
        operation: FilterOperation.LIKE,
      },
    ]);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  openAiServerDialog() {
    const dialogReference = this.dialogService.openDialog({
      component: AiServerCreationDialogComponent,
      viewContainerReference: this.viewContainerRef,
      data: {
        titleKey: 'sqtm-core.administration-workspace.servers.ai.dialog.title.new-ai-title',
      },
      id: 'ai-server-dialog',
      width: 600,
    });

    dialogReference.dialogResultChanged$
      .pipe(
        takeUntil(dialogReference.dialogClosed$),
        filter((result) => result != null),
        concatMap((result: any) => this.gridService.refreshDataAsync().pipe(map(() => result.id))),
        tap((id: string) => super.navigateToNewEntity(id)),
        switchMap(() => this.adminReferentialDataService.refresh()),
      )
      .subscribe();
  }

  deleteAiServers($event: MouseEvent) {
    $event.stopPropagation();
    this.gridService.selectedRows$
      .pipe(
        take(1),
        filter((rows: DataRow[]) => rows.length > 0),
        concatMap((rows) => this.showConfirmDeleteAiServerDialog(rows)),
        filter(({ confirmDelete }) => confirmDelete),
        tap(() => this.gridService.beginAsyncOperation()),
        concatMap(({ rows }) => this.deleteAiServersServerSide(rows)),
        tap(() => this.gridService.completeAsyncOperation()),
      )
      .subscribe(() => this.gridService.refreshData());
  }

  private showConfirmDeleteAiServerDialog(
    rows: DataRow[],
  ): Observable<{ confirmDelete: boolean; rows: DataRow[] }> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.servers.ai.dialog.title.delete-many-title',
      messageKey: 'sqtm-core.administration-workspace.servers.ai.dialog.delete-many',
      level: 'DANGER',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map((confirmDelete) => ({ confirmDelete, rows })),
    );
  }

  private deleteAiServersServerSide(rows): Observable<void> {
    const pathVariable = rows.map((row) => row.data[GridColumnId.serverId]).join(',');
    return this.restService.delete([`ai-servers`, pathVariable]);
  }

  filterCodeSourceServer($event: any) {
    this.gridService.applyMultiColumnsFilter($event);
  }
}
