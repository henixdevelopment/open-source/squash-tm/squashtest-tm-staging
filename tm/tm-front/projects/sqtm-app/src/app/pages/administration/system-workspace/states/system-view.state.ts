import { SystemViewModel } from 'sqtm-core';

export interface SystemViewState extends SystemViewModel {
  isAutomaticRefreshForSupervisionPageActivated: boolean;
  lastSupervisionPageRefresh: Date;
}
