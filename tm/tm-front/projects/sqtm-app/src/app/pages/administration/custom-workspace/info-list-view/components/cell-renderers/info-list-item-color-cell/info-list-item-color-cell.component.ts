import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
} from 'sqtm-core';
import { InfoListViewService } from '../../../services/info-list-view.service';

@Component({
  selector: 'sqtm-app-info-list-item-color-cell',
  template: ` @if (row) {
    <div class="full-width full-height flex-column" style="justify-content: center;">
      <sqtm-core-color-picker-select-field
        [color]="color"
        [cpPosition]="'top-left'"
        (colorChanged)="changeColor($event)"
      >
      </sqtm-core-color-picker-select-field>
    </div>
  }`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InfoListItemColorCellComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    private infoListViewService: InfoListViewService,
  ) {
    super(grid, cdRef);
  }

  get color(): string {
    return this.row.data[this.columnDisplay.id];
  }

  changeColor(newColor: string) {
    this.infoListViewService.changeItemColour(this.row.data[GridColumnId.id], newColor).subscribe();
  }
}

export function infoListItemColorColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(InfoListItemColorCellComponent);
}
