import { ChangeDetectionStrategy, Component, InjectionToken } from '@angular/core';
import {
  AdminReferentialDataService,
  GridId,
  GridService,
  gridServiceFactory,
  LocalPersistenceService,
  ReferentialDataService,
  RestService,
  SynchronizationPluginId,
} from 'sqtm-core';

import { SystemViewService } from '../../../services/system-view.service';
import {
  AbstractSynchronizationGridPanelComponent,
  remoteSyncTableDefinition,
} from '../abstract-synchronization-grid-panel/abstract-synchronization-grid-panel.component';

export const XSQUASH4GITLAB_REMOTE_SYNC_TABLE_CONF = new InjectionToken(
  'XSQUASH4JIRA_REMOTE_SYNC_TABLE_CONF',
);
export const XSQUASH4GITLAB_REMOTE_SYNC_TABLE = new InjectionToken(
  'XSQUASH4JIRA_REMOTE_SYNC_TABLE',
);

@Component({
  selector: 'sqtm-app-system-gitlab-synchronization-grid-panel',
  templateUrl:
    '../abstract-synchronization-grid-panel/abstract-synchronization-grid-panel.component.html',
  styleUrls: [
    '../abstract-synchronization-grid-panel/abstract-synchronization-grid-panel.component.less',
  ],
  providers: [
    {
      provide: XSQUASH4GITLAB_REMOTE_SYNC_TABLE_CONF,
      useFactory: (localPersistenceService) =>
        remoteSyncTableDefinition(
          GridId.SYSTEM_XSQUASH4GITLAB_SYNCHRONISATIONS,
          localPersistenceService,
        ),
      deps: [LocalPersistenceService],
    },
    {
      provide: XSQUASH4GITLAB_REMOTE_SYNC_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, XSQUASH4GITLAB_REMOTE_SYNC_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: XSQUASH4GITLAB_REMOTE_SYNC_TABLE,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SystemGitlabSynchronizationGridPanelComponent extends AbstractSynchronizationGridPanelComponent {
  constructor(
    public adminReferentialDataService: AdminReferentialDataService,
    protected systemViewService: SystemViewService,
    protected gridService: GridService,
  ) {
    super(adminReferentialDataService, systemViewService, gridService);
    this.pluginId = SynchronizationPluginId.XSQUASH4GITLAB;
  }
}
