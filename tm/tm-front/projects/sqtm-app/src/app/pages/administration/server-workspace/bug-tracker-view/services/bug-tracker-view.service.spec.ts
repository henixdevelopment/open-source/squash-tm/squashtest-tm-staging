import { TestBed } from '@angular/core/testing';

import { BugTrackerViewService } from './bug-tracker-view.service';
import {
  AttachmentService,
  AuthenticationPolicy,
  AuthenticationProtocol,
  BasicAuthCredentials,
  EntityViewAttachmentHelperService,
  isBasicAuthCredentials,
  RestService,
} from 'sqtm-core';
import { AppTestingUtilsModule } from '../../../../../utils/testing-utils/app-testing-utils.module';
import { TranslateService } from '@ngx-translate/core';
import { take, withLatestFrom } from 'rxjs/operators';
import { of } from 'rxjs';
import {
  AdminBugTrackerState,
  AdminBugTrackerViewState,
} from '../states/admin-bug-tracker-view-state';
import { mockRestService } from '../../../../../utils/testing-utils/mocks.service';
import createSpyObj = jasmine.createSpyObj;

describe('BugTrackerViewService', () => {
  let service: BugTrackerViewService;
  const restService = mockRestService();

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      providers: [
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: TranslateService,
          useValue: createSpyObj(['instant']),
        },
        {
          provide: BugTrackerViewService,
          useClass: BugTrackerViewService,
          deps: [
            RestService,
            AttachmentService,
            TranslateService,
            EntityViewAttachmentHelperService,
          ],
        },
      ],
    });
    service = TestBed.inject(BugTrackerViewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should load a bugtracker', async () => {
    const model = getInitialBugTracker();
    restService.getWithoutErrorHandling.and.returnValue(of(model));
    service.load(1).subscribe();

    service.componentData$.pipe(take(1)).subscribe((componentData) => {
      expect(componentData.bugTracker).toEqual(model);
    });
  });

  it('should change auth policy', async () => {
    restService.getWithoutErrorHandling.and.returnValue(of(getInitialBugTracker()));
    service.load(1).subscribe();

    service
      .setAuthPolicy(AuthenticationPolicy.APP_LEVEL)
      .pipe(withLatestFrom(service.componentData$))
      .subscribe(([, componentData]: [any, AdminBugTrackerViewState]) => {
        expect(componentData.bugTracker.authPolicy).toEqual(AuthenticationPolicy.APP_LEVEL);
      });
  });

  it('should change basic auth credentials', async () => {
    restService.getWithoutErrorHandling.and.returnValue(of(getInitialBugTracker()));
    service.load(1).subscribe();

    service
      .setCredentials({
        implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
        type: AuthenticationProtocol.BASIC_AUTH,
        username: 'login',
        password: 'password',
      })
      .pipe(withLatestFrom(service.componentData$))
      .subscribe(([, componentData]: [any, AdminBugTrackerViewState]) => {
        expect(componentData.bugTracker.credentials).toEqual({
          type: AuthenticationProtocol.BASIC_AUTH,
          implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
          password: 'password',
          username: 'login',
          registered: true,
        });
      });
  });

  it('should discriminate basic auth credentials', () => {
    const validBasicAuthCredentials: BasicAuthCredentials = {
      type: AuthenticationProtocol.BASIC_AUTH,
      implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
      username: 'username',
      password: 'password',
    };

    expect(isBasicAuthCredentials(validBasicAuthCredentials)).toBeTruthy();
    expect(isBasicAuthCredentials(null)).toBeFalsy();
    expect(isBasicAuthCredentials({ type: 'titi' })).toBeFalsy();
  });

  it('should change credentials based on deduced type (basic)', async () => {
    restService.getWithoutErrorHandling.and.returnValue(of(getInitialBugTracker()));
    service.load(1).subscribe();

    const credentials: BasicAuthCredentials = {
      type: AuthenticationProtocol.BASIC_AUTH,
      implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
      username: 'username',
      password: 'password',
      registered: true,
    };

    service
      .setCredentials(credentials)
      .pipe(withLatestFrom(service.componentData$))
      .subscribe(([, componentData]: [any, AdminBugTrackerViewState]) => {
        expect(componentData.bugTracker.credentials).toEqual(credentials);
      });
  });
});

function getInitialBugTracker(): AdminBugTrackerState {
  return {
    id: 1,
    kind: 'bugTracker',
    name: 'name',
    url: 'http://url.com',
    authPolicy: AuthenticationPolicy.USER,
    authProtocol: AuthenticationProtocol.BASIC_AUTH,
    iframeFriendly: false,
    bugTrackerKinds: [],
    authConfiguration: null,
    credentials: null,
    supportedAuthenticationProtocols: [],
    attachmentList: { id: null, attachments: null },
    createdBy: 'admin',
    createdOn: new Date('2024-02-14 15:00'),
    lastModifiedOn: null,
  };
}
