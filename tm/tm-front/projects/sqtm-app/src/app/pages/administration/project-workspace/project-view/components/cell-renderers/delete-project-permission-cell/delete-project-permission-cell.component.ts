import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  Signal,
} from '@angular/core';
import {
  AbstractDeleteCellRenderer,
  AdminReferentialDataService,
  AuthenticatedUser,
  DialogService,
  GridService,
  Permissions,
} from 'sqtm-core';
import { finalize } from 'rxjs/operators';
import { ProjectViewService } from '../../../services/project-view.service';
import { toSignal } from '@angular/core/rxjs-interop';
import { AdminProjectViewComponentData } from '../../../containers/project-view/project-view.component';

@Component({
  selector: 'sqtm-app-delete-project-permission-cell',
  template: `
    <sqtm-core-delete-icon
      [iconName]="icon"
      (delete)="showDeleteConfirm()"
      [show]="canDelete"
    ></sqtm-core-delete-icon>
  `,
  styleUrls: ['./delete-project-permission-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteProjectPermissionCellComponent
  extends AbstractDeleteCellRenderer
  implements OnDestroy
{
  protected $componentData: Signal<AdminProjectViewComponentData>;
  protected $authenticatedUser: Signal<AuthenticatedUser>;

  constructor(
    public grid: GridService,
    cdr: ChangeDetectorRef,
    protected dialogService: DialogService,
    private projectViewService: ProjectViewService,
    private readonly adminReferentialDataService: AdminReferentialDataService,
  ) {
    super(grid, cdr, dialogService);
    this.$componentData = toSignal(this.projectViewService.componentData$);
    this.$authenticatedUser = toSignal(adminReferentialDataService.authenticatedUser$);
  }

  get icon(): string {
    return 'sqtm-core-generic:unlink';
  }

  get canDelete(): boolean {
    return (
      this.$authenticatedUser().admin ||
      this.$componentData().project.permissions.PROJECT?.includes(
        Permissions.MANAGE_PROJECT_CLEARANCE,
      )
    );
  }

  doDelete() {
    this.grid.beginAsyncOperation();
    const partyId = this.row.data.partyId;
    this.projectViewService
      .removePartyProjectPermissions([partyId])
      .pipe(finalize(() => this.grid.completeAsyncOperation()))
      .subscribe();
  }

  protected getTitleKey(): string {
    return 'sqtm-core.administration-workspace.projects.dialog.title.delete-project-permission.delete-one';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.administration-workspace.projects.dialog.message.delete-project-permission.delete-one';
  }
}
