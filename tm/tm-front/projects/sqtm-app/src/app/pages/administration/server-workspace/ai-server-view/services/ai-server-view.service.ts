import {
  AdminAiServer,
  AdminReferentialDataService,
  AttachmentService,
  AuthenticationProtocol,
  AuthenticatedUser,
  Credentials,
  DateFormatUtils,
  EntityViewAttachmentHelperService,
  GenericEntityViewService,
  LocalPersistenceService,
  RestService,
} from 'sqtm-core';
import {
  AdminAiServerState,
  AdminAiServerViewState,
  provideInitialAdminAiServerView,
} from '../states/admin-ai-server-view-state';
import { TranslateService } from '@ngx-translate/core';
import { catchError, concatMap, map, switchMap, take, tap, withLatestFrom } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class AiServerViewService extends GenericEntityViewService<AdminAiServerState, 'aiServer'> {
  protected readonly requirementModelKey = 'requirement-model';

  constructor(
    protected restService: RestService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
    private adminReferentialDataService: AdminReferentialDataService,
    protected localPersistenceService: LocalPersistenceService,
  ) {
    super(restService, attachmentService, translateService, attachmentHelper);
  }

  public getInitialState(): AdminAiServerViewState {
    return provideInitialAdminAiServerView();
  }

  protected getRootUrl(_initialState?): string {
    return 'ai-server';
  }

  load(aiServerId: number) {
    return this.restService
      .getWithoutErrorHandling<AdminAiServer>(['ai-server', aiServerId.toString()])
      .pipe(
        take(1),
        map((aiServer) => this.initializeAiServer(aiServer)),
        catchError((err) => {
          this.notifyEntityNotFound(err);
          return throwError(() => err);
        }),
      );
  }

  private initializeAiServer(aiServer: AdminAiServer): void {
    const aiServerState: AdminAiServerState = {
      ...aiServer,
      attachmentList: { id: null, attachments: null },
      createdOn: DateFormatUtils.createDateFromIsoString(aiServer.createdOn),
      lastModifiedOn: DateFormatUtils.createDateFromIsoString(aiServer.lastModifiedOn),
    };
    this.initializeEntityState(aiServerState);
  }

  getCurrentUser(): Observable<AuthenticatedUser> {
    return this.adminReferentialDataService.authenticatedUser$;
  }

  setTokenAuthCredentials(token: string) {
    return this.doSetTokenAuthCredentials({
      implementedProtocol: AuthenticationProtocol.TOKEN_AUTH,
      type: AuthenticationProtocol.TOKEN_AUTH,
      token,
    });
  }

  private doSetTokenAuthCredentials(credentials: Credentials): Observable<any> {
    return this.componentData$.pipe(
      take(1),
      switchMap((state: AdminAiServerViewState): Observable<any> => {
        return this.restService.post(
          [this.getRootUrl(), state.aiServer.id.toString(), 'credentials'],
          credentials,
        );
      }),
      withLatestFrom(this.componentData$),
      map(([, state]: [any, AdminAiServerViewState]) => this.saveNewState(credentials, state)),
      concatMap((state) => this.updateLastModificationIfNecessary(state)),
      tap((nextState: AdminAiServerViewState) => this.store.commit(nextState)),
    );
  }

  private saveNewState(
    credentials: Credentials,
    state: AdminAiServerViewState,
  ): AdminAiServerViewState {
    credentials.registered = true;
    return {
      ...state,
      aiServer: {
        ...state.aiServer,
        credentials,
      },
    };
  }

  deleteAiServerCredentials(): Observable<any> {
    return this.restService
      .delete([this.getRootUrl(), this.getSnapshot().aiServer.id.toString(), 'credentials'])
      .pipe(
        withLatestFrom(this.state$),
        map(([, state]: [any, AdminAiServerViewState]) =>
          this.updateAiServerCredentials(state, {
            implementedProtocol: AuthenticationProtocol.TOKEN_AUTH,
            type: AuthenticationProtocol.TOKEN_AUTH,
            registered: false,
            token: null,
          }),
        ),
        concatMap((state) => this.updateLastModificationIfNecessary(state)),
        tap((newState) => this.commit(newState)),
      );
  }

  private updateAiServerCredentials(
    state: AdminAiServerViewState,
    credentials?: Credentials,
  ): AdminAiServerViewState {
    return {
      ...state,
      aiServer: {
        ...state.aiServer,
        credentials,
      },
    };
  }

  savePayload(payload: string): Observable<AdminAiServerViewState> {
    return this.savePayloadServerSide(payload).pipe(
      withLatestFrom(this.store.state$),
      map(([, state]) => this.updateTemplatePayloadState(state, payload)),
      tap((updatedState) => this.store.commit(updatedState)),
    );
  }

  savePayloadServerSide(payload: string) {
    const urlParts = ['ai-server', this.getSnapshot().aiServer.id.toString(), 'new-payload'];
    return this.restService.post(urlParts, { payload });
  }

  saveRequirementModelInLocalStorage(newValue: string) {
    this.localPersistenceService.set(this.requirementModelKey, newValue).subscribe();
  }

  getModelFromLocalStorage(): Observable<string> {
    return this.localPersistenceService.get(this.requirementModelKey);
  }

  private updateTemplatePayloadState(
    state: AdminAiServerViewState,
    payloadTemplate: string,
  ): AdminAiServerViewState {
    return {
      ...state,
      aiServer: {
        ...state.aiServer,
        payloadTemplate,
      },
    } satisfies AdminAiServerViewState;
  }

  testConfiguration(aiServerId: number, requirementDescription: string) {
    return this.restService.post(['ai-server', aiServerId.toString(), 'test-api'], {
      requirementDescription,
    });
  }

  saveJsonPath(jsonPath: string, aiServerId: number): Observable<AdminAiServerViewState> {
    return this.saveJsonPathServerSide(jsonPath, aiServerId).pipe(
      withLatestFrom(this.store.state$),
      map(([, state]) => this.updateJsonPathState(state, jsonPath)),
      tap((updatedState) => this.store.commit(updatedState)),
    );
  }

  private saveJsonPathServerSide(jsonPath: string, aiServerId: number) {
    const urlParts = ['ai-server', aiServerId.toString(), 'new-jsonpath'];
    return this.restService.post(urlParts, { jsonPath });
  }

  private updateJsonPathState(
    state: AdminAiServerViewState,
    jsonPath: string,
  ): AdminAiServerViewState {
    return {
      ...state,
      aiServer: {
        ...state.aiServer,
        jsonPath,
      },
    } satisfies AdminAiServerViewState;
  }
}
