import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewChild } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ActionErrorDisplayService,
  ColumnDefinitionBuilder,
  DialogService,
  EditableTextFieldComponent,
  GridColumnId,
  GridService,
  TableValueChange,
} from 'sqtm-core';
import { ScmServerViewService } from '../../../services/scm-server-view.service';
import { catchError, finalize } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-editable-working-branch-column',
  template: ` @if (columnDisplay && row) {
    <div class="full-width full-height flex-column">
      <sqtm-core-editable-text-field
        #editableTextField
        style="margin: auto 5px;"
        class="sqtm-grid-cell-txt-renderer"
        [showPlaceHolder]="false"
        [value]="row.data[columnDisplay.id]"
        [layout]="'no-buttons'"
        [size]="'small'"
        (confirmEvent)="updateBranch($event)"
      ></sqtm-core-editable-text-field>
    </div>
  }`,
  styleUrls: ['./editable-working-branch-column.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditableWorkingBranchColumnComponent extends AbstractCellRendererComponent {
  @ViewChild('editableTextField')
  editableTextField: EditableTextFieldComponent;

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    public scmServerViewService: ScmServerViewService,
    public dialogService: DialogService,
    private actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    super(grid, cdRef);
  }

  updateBranch(value: string) {
    this.scmServerViewService
      .changeRepositoryWorkingBranch(this.row.data.scmRepositoryId, value)
      .pipe(
        catchError((error) => this.handleError(error)),
        finalize(() => this.editableTextField.endAsync()),
      )
      .subscribe(() => {
        const tableValueChange: TableValueChange = { columnId: this.columnDisplay.id, value };
        this.grid.editRows([this.row.id], [tableValueChange]);
      });
  }

  handleError(error) {
    this.editableTextField.endAsync();
    this.cdRef.markForCheck();
    return this.actionErrorDisplayService.handleActionError(error);
  }
}

export function editableWorkingBranchColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(EditableWorkingBranchColumnComponent);
}
