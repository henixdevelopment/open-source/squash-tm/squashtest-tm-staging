import { ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit } from '@angular/core';
import {
  Extendable,
  GridColumnId,
  GridDefinition,
  GridFilter,
  GridService,
  indexColumn,
  smallGrid,
  Sort,
  StyleDefinitionBuilder,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { activeUserConverter } from '../../../../user-workspace/user-workspace/containers/user-grid/user-grid.component';
import { AdminProfileViewComponentData } from '../../containers/panel-groups/profile-content/profile-content.component';
import { PROFILE_AUTHORIZATIONS_TABLE } from '../../../profile-workspace.constant';
import { of } from 'rxjs';
import {
  buildPartyType,
  partyTypeColumn,
} from '../cell-renderers/profile-authorizationsparty-type-cell/profile-authorizations-party-type-cell.component';
import { clickablePartyColumn } from '../../../../cell-renderer.builders';

export function profileAuthorizationTableDefinition(
  translateService: TranslateService,
): GridDefinition {
  return smallGrid('profile-authorizations')
    .withColumns([
      indexColumn().withViewport('leftViewport'),
      clickablePartyColumn(GridColumnId.partyName)
        .withI18nKey(
          'sqtm-core.administration-workspace.views.project.permissions.user-or-team.singular',
        )
        .changeWidthCalculationStrategy(new Extendable(200, 0.5)),
      partyTypeColumn(GridColumnId.team)
        .withSortFunction(buildPartyType(translateService))
        .changeWidthCalculationStrategy(new Extendable(100, 0.2)),
    ])
    .withInitialSortedColumns([{ id: GridColumnId.partyName, sort: Sort.ASC }])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .enableMultipleColumnsFiltering([GridColumnId.partyName])
    .withRowConverter(activeUserConverter)
    .build();
}

@Component({
  selector: 'sqtm-app-profile-authorizations-panel',
  template: `
    <div class="m-t-10 m-l-10 m-b-15 flex-fixed-size" style="width: 200px">
      <sqtm-core-text-research-field
        (newResearchValue)="handleResearchInput($event)"
      ></sqtm-core-text-research-field>
    </div>
    <sqtm-core-grid></sqtm-core-grid>
  `,
  styleUrls: ['./profile-authorizations-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: PROFILE_AUTHORIZATIONS_TABLE,
    },
  ],
})
export class ProfileAuthorizationsPanelComponent implements OnInit, OnDestroy {
  @Input() componentData: AdminProfileViewComponentData;

  constructor(private gridService: GridService) {}

  ngOnInit(): void {
    this.initializeTable();
    this.initializeFilters();
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }

  handleResearchInput($event: string): void {
    this.gridService.applyMultiColumnsFilter($event);
  }

  private initializeFilters() {
    const filters: GridFilter[] = [
      {
        id: GridColumnId.partyName,
        active: false,
        initialValue: { kind: 'single-string-value', value: '' },
        tiedToPerimeter: false,
      },
    ];
    this.gridService.addFilters(filters);
  }

  private initializeTable() {
    this.gridService.connectToDatasource(
      of(this.componentData.profile.partyProfileAuthorizations),
      GridColumnId.partyId,
    );
  }
}
