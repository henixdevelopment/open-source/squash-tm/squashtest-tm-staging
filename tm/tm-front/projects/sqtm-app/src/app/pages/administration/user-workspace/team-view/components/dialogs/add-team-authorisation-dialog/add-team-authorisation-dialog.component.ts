import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  Signal,
  ViewChild,
} from '@angular/core';
import {
  DialogReference,
  DisplayOption,
  FieldValidationError,
  GridService,
  GroupedMultiListFieldComponent,
  ListItem,
  RestService,
  SelectFieldComponent,
} from 'sqtm-core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { finalize, switchMap, take } from 'rxjs/operators';
import { TeamViewService } from '../../../services/team-view.service';
import { AdminTeamViewState } from '../../../states/admin-team-view-state';
import { ProfileService } from '../../../../../profile-workspace/profile-workspace/services/profile.service';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sqtm-app-add-team-authorisation-dialog',
  templateUrl: './add-team-authorisation-dialog.component.html',
  styleUrls: ['./add-team-authorisation-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: ProfileService,
      useClass: ProfileService,
    },
  ],
})
export class AddTeamAuthorisationDialogComponent implements OnInit, AfterViewInit {
  listItems: ListItem[] = [];
  profileOptions: DisplayOption[];
  formGroup: FormGroup;
  errorsOnMultiListField: string[] = [];
  serverSideValidationErrors: FieldValidationError[] = [];
  $componentData: Signal<AdminTeamViewState>;

  @ViewChild(GroupedMultiListFieldComponent)
  projectList: GroupedMultiListFieldComponent;

  @ViewChild(SelectFieldComponent)
  profileSelectField: SelectFieldComponent;

  constructor(
    private dialogReference: DialogReference,
    private restService: RestService,
    private cdr: ChangeDetectorRef,
    private fb: FormBuilder,
    private teamViewService: TeamViewService,
    private grid: GridService,
    private profileService: ProfileService,
  ) {
    this.$componentData = toSignal(this.teamViewService.componentData$);
  }

  ngOnInit(): void {
    this.initializeFormGroup();
  }

  ngAfterViewInit(): void {
    this.prepareProfileSelectField();
    this.prepareProjectsMultiListField();
  }

  selectedProjectsChanged($event: ListItem[]) {
    this.projectList.selectedItems = $event;
  }

  confirm(addAnother?: boolean) {
    if (this.formIsValid()) {
      this.doPost(addAnother);
    } else {
      this.showClientSideErrors();
    }
  }

  addAnother() {
    this.confirm(true);
  }

  private prepareProfileSelectField(): void {
    const profiles = this.$componentData().team.profiles;
    const options = this.profileService.retrieveProfilesAsDisplayOptions(profiles);

    // Sort options by locale label
    options.sort((a, b) => {
      return a.label.localeCompare(b.label);
    });

    this.profileOptions = [...options];

    if (options.length > 0 && this.profileSelectField != null) {
      this.profileSelectField.disabled = false;
    }
  }

  private prepareProjectsMultiListField() {
    this.teamViewService.componentData$
      .pipe(
        take(1),
        switchMap((componentData) => {
          const userId = componentData.team.id.toString();

          return this.restService.get<ProjectWithoutPermission[]>([
            'team-view',
            userId,
            'projects-without-permission',
          ]);
        }),
      )
      .subscribe((response) => {
        const projects = this.retrieveProjectsAsListItem(response);

        projects.sort((partyA, partyB) => partyA.label.localeCompare(partyB.label));

        this.listItems = [...projects];
        this.cdr.detectChanges();
      });
  }

  private retrieveProjectsAsListItem(response: ProjectWithoutPermission[]): ListItem[] {
    return response.map((project) => {
      return {
        id: project.id,
        label: project.name,
        selected: false,
      };
    });
  }

  private getSelectedProjectIds(): number[] {
    return this.projectList.selectedItems.map((project) => Number(project.id));
  }

  private doPost(addAnother?: boolean) {
    const profile = this.formGroup.controls['profile'].value;
    this.grid.beginAsyncOperation();
    this.teamViewService
      .setTeamAuthorisation(this.getSelectedProjectIds(), profile)
      .pipe(finalize(() => this.grid.completeAsyncOperation()))
      .subscribe();
    this.dialogReference.result = true;
    if (addAnother) {
      this.refreshDialog();
    } else {
      this.dialogReference.close();
    }
  }

  private formIsValid() {
    return this.formGroup.status === 'VALID' && this.projectList.selectedItems.length > 0;
  }

  private showClientSideErrors() {
    this.profileSelectField.showClientSideError();

    if (this.projectList.selectedItems.length === 0) {
      const requiredKey = 'sqtm-core.validation.errors.required';
      this.errorsOnMultiListField.push(requiredKey);
    }
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      profile: this.fb.control(null, [Validators.required]),
    });
  }

  private refreshDialog() {
    this.projectList.listItems = this.projectList.listItems.filter(
      (item) => !this.projectList.selectedItems.includes(item),
    );
    this.listItems = [...this.projectList.listItems];
    this.projectList.selectedItems = [];
    this.serverSideValidationErrors = [];
    this.initializeFormGroup();
  }
}

interface ProjectWithoutPermission {
  id: string;
  name: string;
}
