import { GenericEntityViewState, provideInitialGenericViewState } from 'sqtm-core';
import { AdminProfileState } from './admin-profile-state';

export interface AdminProfileViewState
  extends GenericEntityViewState<AdminProfileState, 'profile'> {
  profile: AdminProfileState;
}

export function provideInitialAdminProfileView(): Readonly<AdminProfileViewState> {
  return provideInitialGenericViewState<AdminProfileState, 'profile'>('profile');
}
