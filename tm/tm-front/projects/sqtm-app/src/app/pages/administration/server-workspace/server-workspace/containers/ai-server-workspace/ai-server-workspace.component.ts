import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {
  ADMIN_WS_AI_SERVERS_TABLE,
  ADMIN_WS_AI_SERVERS_TABLE_CONFIG,
} from '../../../server-workspace.constant';
import { adminAiServersTableDefinition } from '../ai-server-grid/ai-server-grid.component';

@Component({
  selector: 'sqtm-app-ai-server-workspace',
  templateUrl: './ai-server-workspace.component.html',
  styleUrl: './ai-server-workspace.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: ADMIN_WS_AI_SERVERS_TABLE_CONFIG,
      useFactory: adminAiServersTableDefinition,
      deps: [],
    },
    {
      provide: ADMIN_WS_AI_SERVERS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, ADMIN_WS_AI_SERVERS_TABLE_CONFIG, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: ADMIN_WS_AI_SERVERS_TABLE,
    },
  ],
})
export class AiServerWorkspaceComponent implements OnInit, OnDestroy {
  authenticatedAdmin$: Observable<AuthenticatedUser>;
  constructor(
    public readonly adminReferentialDataService: AdminReferentialDataService,
    private gridService: GridService,
  ) {}
  ngOnInit(): void {
    this.authenticatedAdmin$ = this.adminReferentialDataService.authenticatedUser$;
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }
}
