import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, Signal } from '@angular/core';
import {
  ActionErrorDisplayService,
  AdminReferentialDataService,
  DialogService,
  GenericEntityViewService,
} from 'sqtm-core';
import { Observable, Subject, switchMap } from 'rxjs';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { catchError, concatMap, filter, map, take, takeUntil } from 'rxjs/operators';
import { ProfileViewService } from '../../services/profile-view.service';
import { ProfileService } from '../../../profile-workspace/services/profile.service';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sqtm-app-profile-view',
  templateUrl: './profile-view.component.html',
  providers: [
    {
      provide: ProfileViewService,
      useClass: ProfileViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: ProfileViewService,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileViewComponent implements OnInit, OnDestroy {
  private unsub$ = new Subject<void>();
  readonly $isUltimate: Signal<boolean>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public readonly profileViewService: ProfileViewService,
    private dialogService: DialogService,
    private profileService: ProfileService,
    private actionErrorDisplayService: ActionErrorDisplayService,
    private adminReferentialDataService: AdminReferentialDataService,
  ) {
    this.$isUltimate = toSignal(this.adminReferentialDataService.isUltimateLicenseAvailable$);
  }

  ngOnInit(): void {
    this.profileViewService.simpleAttributeRequiringRefresh = ['name', 'active'];
    this.profileViewService.externalRefreshRequired$
      .pipe(
        takeUntil(this.unsub$),
        concatMap(() => this.profileService.getDataRowProfiles()),
      )
      .subscribe();

    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('profileId')),
      )
      .subscribe((id) => this.profileViewService.load(parseInt(id, 10)));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
    this.profileViewService.complete();
  }

  handleDelete() {
    this.profileViewService.componentData$
      .pipe(
        take(1),
        filter((componentData) => this.isProfileNotUsed(componentData.profile.partyCount)),
        switchMap((componentData) => this.showConfirmDeleteProfileDialog(componentData.profile.id)),
        filter(({ confirmDelete }) => confirmDelete),
        concatMap(({ profileId }) =>
          this.deleteProfileServerSide(profileId).pipe(
            catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
          ),
        ),
        switchMap(() => this.profileService.getDataRowProfiles()),
      )
      .subscribe(() => this.router.navigate(['administration-workspace', 'profiles', 'manage']));
  }

  private isProfileNotUsed(partyCount: number): boolean {
    if (partyCount === 0) {
      return true;
    }

    this.dialogService.openAlert({
      titleKey: 'sqtm-core.action-word-workspace.tree.dialog.no-deletion.title',
      messageKey: 'sqtm-core.exception.profile.forbid-deletion',
      level: 'INFO',
    });
    return false;
  }

  private showConfirmDeleteProfileDialog(
    profileId,
  ): Observable<{ confirmDelete: boolean; profileId: string }> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.profiles.dialog.title.delete-one',
      messageKey: 'sqtm-core.administration-workspace.profiles.dialog.message.delete-one',
      level: 'DANGER',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map((confirmDelete) => ({ confirmDelete, profileId })),
    );
  }

  private deleteProfileServerSide(profileId: string): Observable<void> {
    return this.profileService.deleteProfile(profileId);
  }
}
