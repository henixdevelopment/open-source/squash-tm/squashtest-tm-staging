import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { BugTrackerViewService } from '../../services/bug-tracker-view.service';
import { catchError, concatMap, filter, map, switchMap, take, takeUntil } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import {
  ActionErrorDisplayService,
  DialogService,
  GenericEntityViewService,
  GridService,
  RestService,
} from 'sqtm-core';
import { AdminBugTrackerViewState } from '../../states/admin-bug-tracker-view-state';

@Component({
  selector: 'sqtm-app-bug-tracker-view',
  templateUrl: './bug-tracker-view.component.html',
  styleUrls: ['./bug-tracker-view.component.less'],
  providers: [
    {
      provide: BugTrackerViewService,
      useClass: BugTrackerViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: BugTrackerViewService,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BugTrackerViewComponent implements OnInit, OnDestroy {
  componentData$: Observable<AdminBugTrackerViewState>;

  private readonly unsub$ = new Subject<void>();

  constructor(
    private readonly route: ActivatedRoute,
    public readonly bugTrackerViewService: BugTrackerViewService,
    private readonly gridService: GridService,
    private readonly restService: RestService,
    private readonly dialogService: DialogService,
    private readonly actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    this.componentData$ = bugTrackerViewService.componentData$.pipe(takeUntil(this.unsub$));
  }

  ngOnInit(): void {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('bugTrackerId')),
        switchMap((id) => this.bugTrackerViewService.load(parseInt(id, 10))),
      )
      .subscribe();

    this.observeStateChanges();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
    this.bugTrackerViewService.complete();
  }

  handleDelete(): void {
    this.componentData$
      .pipe(
        take(1),
        concatMap((componentData: AdminBugTrackerViewState) =>
          this.showConfirmDeleteDialog(componentData.bugTracker.id),
        ),
        filter(({ confirmDelete }) => confirmDelete),
        concatMap(({ bugTrackerId }) => this.deleteServerSide(bugTrackerId)),
        catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
      )
      .subscribe(() => this.gridService.refreshData());
  }

  private showConfirmDeleteDialog(
    bugTrackerId,
  ): Observable<{ confirmDelete: boolean; bugTrackerId: string }> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.servers.bugtrackers.dialog.title.delete-one',
      messageKey:
        'sqtm-core.administration-workspace.servers.bugtrackers.dialog.message.delete-one',
      level: 'DANGER',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map((confirmDelete) => ({ confirmDelete, bugTrackerId })),
    );
  }

  private deleteServerSide(serverId): Observable<void> {
    return this.restService.delete(['bugtracker', serverId]);
  }

  private observeStateChanges(): void {
    this.bugTrackerViewService.simpleAttributeRequiringRefresh = [
      'name',
      'url',
      'kind',
      'description',
    ];
    this.bugTrackerViewService.externalRefreshRequired$
      .pipe(takeUntil(this.unsub$))
      .subscribe(() => this.gridService.refreshDataAndKeepSelectedRows());
  }
}
