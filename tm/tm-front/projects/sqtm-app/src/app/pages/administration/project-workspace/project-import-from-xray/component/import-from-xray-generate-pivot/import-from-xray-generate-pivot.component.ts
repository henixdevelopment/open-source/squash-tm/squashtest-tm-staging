import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ImportFromXrayState } from '../../state/import-from-xray.state';

@Component({
  selector: 'sqtm-app-import-from-xray-generate-pivot',
  templateUrl: './import-from-xray-generate-pivot.component.html',
  styleUrl: './import-from-xray-generate-pivot.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImportFromXrayGeneratePivotComponent {
  @Input({ required: true }) componentData: ImportFromXrayState;
}
