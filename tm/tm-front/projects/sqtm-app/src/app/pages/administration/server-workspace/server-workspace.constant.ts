import { InjectionToken } from '@angular/core';
import { GridDefinition, GridService } from 'sqtm-core';

export const ADMIN_WS_BUGTRACKERS_TABLE_CONFIG = new InjectionToken<GridDefinition>(
  'Grid config instance for the table of bugtrackers in admin workspace',
);
export const ADMIN_WS_BUGTRACKERS_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the table of bugtrackers in admin workspace',
);

export const ADMIN_WS_TEST_AUTOMATION_SERVER_TABLE_CONFIG = new InjectionToken<GridDefinition>(
  'Grid config instance for the table of automation servers in admin workspace',
);
export const ADMIN_WS_TEST_AUTOMATION_SERVER_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the table of automation servers in admin workspace',
);

export const ADMIN_WS_SCM_SERVERS_TABLE_CONFIG = new InjectionToken<GridDefinition>(
  'Grid config instance for the table of SCM servers in admin workspace',
);
export const ADMIN_WS_SCM_SERVERS_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the table of SCM servers in admin workspace',
);

export const ADMIN_WS_AI_SERVERS_TABLE_CONFIG = new InjectionToken<GridDefinition>(
  'Grid config instance for the table of AI servers in admin workspace',
);
export const ADMIN_WS_AI_SERVERS_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the table of AI servers in admin workspace',
);
