import {
  AuthConfiguration,
  AuthenticationPolicy,
  AuthenticationProtocol,
  BugTrackerCacheInfo,
  Credentials,
  GenericEntityViewState,
  provideInitialGenericViewState,
  SqtmGenericEntityState,
} from 'sqtm-core';

export interface AdminBugTrackerState extends SqtmGenericEntityState {
  id: number;
  name: string;
  url: string;
  kind: string;
  authPolicy: AuthenticationPolicy;
  authProtocol: AuthenticationProtocol;
  iframeFriendly: boolean;
  bugTrackerKinds: string[];
  supportedAuthenticationProtocols: AuthenticationProtocol[];
  authConfiguration?: AuthConfiguration;
  credentials?: Credentials;
  createdBy: string;
  createdOn: Date;
  lastModifiedBy?: string;
  lastModifiedOn?: Date;
  description?: string;
  allowsReportingCache?: boolean;
  reportingCacheCredentials?: Credentials;
  hasCacheCredentialsError?: boolean;
  cacheInfo?: BugTrackerCacheInfo;
}

export interface AdminBugTrackerViewState
  extends GenericEntityViewState<AdminBugTrackerState, 'bugTracker'> {
  bugTracker: AdminBugTrackerState;
}

export function provideInitialAdminBugTrackerView(): Readonly<AdminBugTrackerViewState> {
  return provideInitialGenericViewState<AdminBugTrackerState, 'bugTracker'>('bugTracker');
}
