import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  DataRow,
  DialogService,
  Fixed,
  GridColumnId,
  GridService,
  RestService,
} from 'sqtm-core';
import { Observable, Subject, switchMap } from 'rxjs';
import { concatMap, filter, map, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-delete-scm-server-cell-renderer',
  template: `
    @if (row) {
      @if (canDelete()) {
        <div
          class="full-height full-width flex-column icon-container current-workspace-main-color"
          (click)="removeItem(row)"
        >
          <i nz-icon [nzType]="getIcon()" nzTheme="outline" class="table-icon-size"></i>
        </div>
      }
    }
  `,
  styleUrls: ['./delete-scm-server.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteScmServerComponent extends AbstractCellRendererComponent implements OnDestroy {
  @Input()
  row: DataRow;

  unsub$ = new Subject<void>();

  constructor(
    public grid: GridService,
    cdr: ChangeDetectorRef,
    private dialogService: DialogService,
    private restService: RestService,
  ) {
    super(grid, cdr);
  }

  getIcon(): string {
    return 'sqtm-core-generic:delete';
  }

  private checkServerUsage(): Observable<boolean> {
    const serverId = this.row.id;
    return this.restService
      .get<{ areUsed: boolean }>(['scm-server-view', serverId.toString(), 'are-used-by-test-cases'])
      .pipe(map(({ areUsed }) => areUsed));
  }

  removeItem(row: DataRow) {
    this.checkServerUsage()
      .pipe(
        switchMap((areReposUsedByTestCases) => this.openConfirmDialog(areReposUsedByTestCases)),
        takeUntil(this.unsub$),
        filter((result) => Boolean(result)),
        concatMap(() =>
          this.restService.delete([`scm-servers/${row.data[GridColumnId.serverId]}`]),
        ),
      )
      .subscribe(() => {
        this.grid.refreshData();
      });
  }

  private openConfirmDialog(areReposUsedByTestCases: boolean): Observable<boolean | undefined> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.servers.scm-servers.dialog.title.delete-one',
      messageKey: this.getDeleteMessageConfirmKey(areReposUsedByTestCases),
      level: 'DANGER',
    });

    return dialogReference.dialogClosed$;
  }

  private getDeleteMessageConfirmKey(areReposUsedByTestCases: boolean) {
    const i18nBase =
      'sqtm-core.administration-workspace.servers.scm-servers.dialog.message.delete-one-';

    if (areReposUsedByTestCases) {
      return i18nBase + 'with-test-cases';
    }

    if (this.serverIsUsedByProject) {
      return i18nBase + 'with-project';
    }

    return i18nBase + 'without-project';
  }

  get serverIsUsedByProject(): boolean {
    return this.row.data[GridColumnId.projectCount] > 0;
  }

  canDelete(): boolean {
    return true;
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}

export function deleteScmServerColumn(id: GridColumnId, label = ''): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(DeleteScmServerComponent)
    .withLabel(label)
    .disableSort()
    .changeWidthCalculationStrategy(new Fixed(50));
}
