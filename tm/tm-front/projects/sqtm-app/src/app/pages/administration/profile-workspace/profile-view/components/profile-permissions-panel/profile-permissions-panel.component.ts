import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { Subject, switchMap } from 'rxjs';
import { AdminProfileViewComponentData } from '../../containers/panel-groups/profile-content/profile-content.component';
import { TranslateService } from '@ngx-translate/core';
import {
  ActivePermission,
  ExperimentalFeatureService,
  Permissions,
  PermissionWithSubPermissions,
  PermissionWorkspaceType,
  ProfilePermissions,
  ProfilePermissionsPanel,
  WorkspaceActivePermissions,
  workspacePermissionsList,
} from 'sqtm-core';
import { ProfileViewService } from '../../services/profile-view.service';
import { AbstractPermissionsComponent } from '../../../profile-workspace/components/abstract-permissions.component';

@Component({
  selector: 'sqtm-app-profile-permissions-panel',
  templateUrl: './profile-permissions-panel.component.html',
  styleUrls: ['./profile-permissions-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class ProfilePermissionsPanelComponent
  extends AbstractPermissionsComponent
  implements OnInit, OnDestroy
{
  @Input() componentData: AdminProfileViewComponentData;
  @Input() isUltimate: boolean;

  @ViewChild('emptyTableMessage')
  emptyTableMessage: ElementRef;

  private unsub$ = new Subject<void>();
  editMode: boolean = false;
  isContentVisible: boolean = true;
  permissionWorkspacePanels: ProfilePermissionsPanel[] = [];
  private editModeElement: ElementRef;

  constructor(
    protected translateService: TranslateService,
    protected elementRef: ElementRef,
    protected renderer: Renderer2,
    private profileViewService: ProfileViewService,
    private experimentalFeatureService: ExperimentalFeatureService,
  ) {
    super(elementRef, translateService, renderer);
  }

  ngOnInit(): void {
    this.initPermissionWorkspacePanels();
  }

  @HostListener('window:click', ['$event'])
  documentClick(event: MouseEvent) {
    if (
      this.editMode &&
      this.editModeElement &&
      !this.editModeElement.nativeElement.contains(event.target)
    ) {
      this.doConfirm();
    }
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  initPermissionWorkspacePanels() {
    this.permissionWorkspacePanels = [];
    const permissions = this.componentData.profile.permissions;

    for (const workspacePermissions of workspacePermissionsList) {
      const projectPermissions = permissions.find(
        (permission) =>
          this.toSnakeCase(permission.simplifiedClassName) === workspacePermissions.workspace,
      );

      this.addPanel(
        workspacePermissions.workspace,
        projectPermissions.className,
        workspacePermissions.permissions,
        projectPermissions.permissions,
      );
    }
  }

  private addPanel(
    workspace: PermissionWorkspaceType,
    className: string,
    permissions: PermissionWithSubPermissions[],
    projectPermissions: Permissions[],
  ) {
    const formattedPanelCode = this.formatCode(workspace);
    const panel: ProfilePermissionsPanel = {
      code: formattedPanelCode,
      id: className,
      active: true,
      items: this.buildPermissionTree(formattedPanelCode, permissions, projectPermissions),
    };
    this.permissionWorkspacePanels.push(panel);
  }

  private buildPermissionTree(
    formattedPanelCode: string,
    permissions: PermissionWithSubPermissions[] | Permissions[],
    projectPermissions: Permissions[],
  ) {
    return permissions.map((permission) => {
      const permissionName = permission.mainPermission ?? permission;
      const formattedPermissionName = this.formatCode(permissionName);
      return {
        code: `${formattedPanelCode}-${formattedPermissionName}`,
        id: permissionName,
        active: projectPermissions.includes(permissionName),
        explainMessage: this.getExplainMessage(formattedPanelCode, formattedPermissionName),
        items: permission.subPermissions
          ? this.buildPermissionTree(
              formattedPanelCode,
              permission.subPermissions,
              projectPermissions,
            )
          : [],
      };
    });
  }

  private toSnakeCase(input: string): string {
    return input
      .replace(/[A-Z]/g, (match) => '_' + match)
      .replace(/(^_)+/, '')
      .toUpperCase();
  }

  openAllPanels() {
    this.isContentVisible = true;
    this.permissionWorkspacePanels.forEach((panel) => (panel.active = true));
  }

  closeAllPanels() {
    this.isContentVisible = false;
    this.permissionWorkspacePanels.forEach((panel) => (panel.active = false));
  }

  getPermissionLabel(panelCode: string, permissionId: string): string {
    return this.translateService.instant(
      `sqtm-core.administration-workspace.profiles.permission.${this.formatCode(panelCode)}.${this.formatCode(permissionId)}.label`,
    );
  }

  enableEditMode(event: MouseEvent) {
    event.stopPropagation();
    event.preventDefault();
    if (this.canEdit()) {
      this.editMode = true;
      this.editModeElement = this.elementRef;
      const element = this.elementRef.nativeElement.querySelector('#permissionsPanel');
      if (element) {
        this.renderer.removeClass(element, 'panels-border');
      }
    }
  }

  canEdit(): boolean {
    return !this.componentData.profile.system && this.isUltimate;
  }

  get isFeatureDisabled(): boolean {
    return !this.experimentalFeatureService.isEnabled('CONFIGURE_PROFILES');
  }

  forbidChangePermissions(panelCode: string): boolean {
    return (
      this.isFeatureDisabled &&
      !['project', 'requirement-library', 'test-case-library', 'action-word-library'].includes(
        panelCode.toLowerCase(),
      )
    );
  }

  disableEditMode() {
    this.editMode = false;
    const element = this.elementRef.nativeElement.querySelector('#permissionsPanel');
    if (element) {
      this.renderer.addClass(element, 'panels-border');
    }
  }

  getPanelTitleClasses(panelCode: string): string {
    let classes = 'profile-permission-panel workspace-panel';

    if (panelCode.toUpperCase() !== 'PROJECT') {
      classes += ` ${this.formatCode(panelCode)}-title`;
    }

    return classes;
  }

  getPanelName(panelCode: string): string {
    return this.translateService
      .instant(`sqtm-core.administration-workspace.profiles.permission.${panelCode}.title`)
      .toUpperCase();
  }

  handleConfirm(event: MouseEvent) {
    event.stopPropagation();
    event.preventDefault();
    this.doConfirm();
  }

  doConfirm() {
    this.disableEditMode();

    const transformedData = this.permissionWorkspacePanels.map((panel) => {
      const options: ActivePermission[] = [];
      const foundPanel = this.componentData.profile.permissions.find(
        (profilePermission) => profilePermission.className === panel.id,
      );
      this.handlePermissionsRecursively(panel.items, foundPanel, options);

      return {
        className: panel.id,
        permissions: options,
      };
    });

    const hasPermissions = transformedData.some((data) => data.permissions.length > 0);
    if (hasPermissions) {
      this.updatePermissions(transformedData);
    }
  }
  private handlePermissionsRecursively(
    permissions: ProfilePermissionsPanel[],
    foundPanel: ProfilePermissions,
    options: ActivePermission[],
  ): void {
    for (const permission of permissions) {
      if (
        (permission.active && !foundPanel.permissions.includes(permission.id as Permissions)) ||
        (!permission.active && foundPanel.permissions.includes(permission.id as Permissions))
      ) {
        options.push({ permission: permission.id as Permissions, active: permission.active });
      }
      if (permission.items) {
        this.handlePermissionsRecursively(permission.items, foundPanel, options);
      }
    }
  }

  private updatePermissions(transformedData: WorkspaceActivePermissions[]): void {
    this.profileViewService
      .changePermissions(this.componentData.profile.id, transformedData)
      .pipe(
        switchMap(() =>
          this.profileViewService.getProfilePermissions(this.componentData.profile.id),
        ),
      )
      .subscribe((permissions) => {
        this.componentData.profile.permissions = permissions;
        this.initPermissionWorkspacePanels();
      });
  }

  handleClose(event: MouseEvent) {
    event.stopPropagation();
    event.preventDefault();
    this.initPermissionWorkspacePanels();
    this.disableEditMode();
  }

  handlePermissionCheckboxChange(
    isChecked: boolean,
    mainPermission: ProfilePermissionsPanel,
    workspaceCode: string,
  ): void {
    if (isChecked && mainPermission.id !== Permissions.READ) {
      const workspacePanel = this.permissionWorkspacePanels.find(
        (panel) => panel.code === workspaceCode,
      );
      const readPermission = workspacePanel.items.find(
        (permission) => permission.id === Permissions.READ,
      );
      if (readPermission && !readPermission.active) {
        readPermission.active = true;
      }
    }

    if (isChecked || !mainPermission.items) {
      return;
    }

    mainPermission.items.forEach((subPermission) => (subPermission.active = false));

    if (mainPermission.id === Permissions.READ) {
      const workspacePanel = this.permissionWorkspacePanels.find(
        (panel) => panel.code === workspaceCode,
      );
      if (workspacePanel) {
        workspacePanel.items.forEach((permission) => {
          permission.active = false;
          if (permission.items) {
            permission.items.forEach((subPermission) => (subPermission.active = false));
          }
        });
      }
    }
  }

  handleSubPermissionCheckboxChange(isChecked: boolean, parentCode: string): void {
    if (isChecked) {
      const workspacePanel = this.permissionWorkspacePanels.find((panel) =>
        panel.items.some((permission) => permission.code === parentCode),
      );
      if (workspacePanel) {
        workspacePanel.items.find((permission) => permission.code === parentCode).active = true;
        const readPermission = workspacePanel.items.find(
          (permission) => permission.id === Permissions.READ,
        );
        if (readPermission && !readPermission.active) {
          readPermission.active = true;
        }
      }
    }
  }

  get permissionsPanelClasses(): string {
    let classes = 'collapse-panel sqtm-core-prevent-selection';

    if (this.canEdit()) {
      classes += ' panels-border __hover_pointer';
    }

    return classes;
  }
}
