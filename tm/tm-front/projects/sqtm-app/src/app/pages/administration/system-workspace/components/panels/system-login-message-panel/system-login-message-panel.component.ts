import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { SystemViewService } from '../../../services/system-view.service';
import { catchError, finalize } from 'rxjs/operators';
import { ActionErrorDisplayService, EditableTextFieldComponent } from 'sqtm-core';

@Component({
  selector: 'sqtm-app-system-login-message-panel',
  template: `
    <sqtm-core-editable-rich-text
      #loginMessageTextField
      [value]="(systemViewService.componentData$ | async).loginMessage"
      [focus]="true"
      (confirmEvent)="changeMessage($event)"
      [attr.data-test-field-id]="'loginMessage'"
    >
    </sqtm-core-editable-rich-text>
  `,
  styleUrls: ['./system-login-message-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SystemLoginMessagePanelComponent {
  @ViewChild('loginMessageTextField')
  loginMessageTextField: EditableTextFieldComponent;

  constructor(
    public readonly systemViewService: SystemViewService,
    private actionErrorDisplayService: ActionErrorDisplayService,
  ) {}

  changeMessage($event: string): void {
    this.systemViewService
      .setLoginMessage($event)
      .pipe(
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => this.loginMessageTextField.endAsync()),
      )
      .subscribe();
  }
}
