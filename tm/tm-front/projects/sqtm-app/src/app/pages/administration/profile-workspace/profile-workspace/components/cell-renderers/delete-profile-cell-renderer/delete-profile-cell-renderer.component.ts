import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractDeleteCellRenderer,
  ActionErrorDisplayService,
  ConfirmDeleteLevel,
  DialogService,
  GridColumnId,
  GridService,
} from 'sqtm-core';
import { ProfileService } from '../../../services/profile.service';
import { concatMap } from 'rxjs';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-delete-profile-cell-renderer',
  template: `
    <sqtm-core-delete-icon
      [show]="canDelete()"
      (delete)="showDeleteConfirm()"
    ></sqtm-core-delete-icon>
  `,
  styleUrls: ['./delete-profile-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteProfileCellRendererComponent extends AbstractDeleteCellRenderer {
  constructor(
    public grid: GridService,
    cdr: ChangeDetectorRef,
    protected dialogService: DialogService,
    private profileService: ProfileService,
    private router: Router,
    private actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    super(grid, cdr, dialogService);
  }

  canDelete(): boolean {
    return !this.row.data.system;
  }

  public showDeleteConfirm(): void {
    if (this.rowIsUsed()) {
      this.dialogService.openAlert({
        titleKey: 'sqtm-core.action-word-workspace.tree.dialog.no-deletion.title',
        messageKey: 'sqtm-core.exception.profile.forbid-deletion',
        level: 'INFO',
      });
    } else {
      super.showDeleteConfirm();
    }
  }

  protected doDelete(): any {
    this.profileService
      .deleteProfile(this.row.id as string)
      .pipe(
        concatMap(() => this.profileService.getDataRowProfiles()),
        catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
      )
      .subscribe(() => this.router.navigate(['administration-workspace', 'profiles', 'manage']));
  }

  protected getTitleKey(): string {
    return 'sqtm-core.administration-workspace.profiles.dialog.title.delete-one';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.administration-workspace.profiles.dialog.message.delete-one';
  }

  protected getLevel(): ConfirmDeleteLevel {
    return 'DANGER';
  }

  private rowIsUsed(): boolean {
    return this.row.data[GridColumnId.partyCount] > 0;
  }
}
