import { ChangeDetectionStrategy, Component } from '@angular/core';
import { WorkspaceWithGridComponent } from 'sqtm-core';

@Component({
  selector: 'sqtm-app-user-workspace-anchors',
  templateUrl: './user-workspace-anchors.component.html',
  styleUrls: ['./user-workspace-anchors.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserWorkspaceAnchorsComponent {
  constructor(private workspaceWithGrid: WorkspaceWithGridComponent) {}

  hideContextualContent() {
    this.workspaceWithGrid.switchToNoRowLayout();
  }
}
