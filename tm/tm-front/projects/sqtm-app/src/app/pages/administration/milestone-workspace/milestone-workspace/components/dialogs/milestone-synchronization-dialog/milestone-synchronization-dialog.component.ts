import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { DialogReference, FieldValidationError, Identifier, RestService } from 'sqtm-core';
import { milestoneWorkspaceLogger } from '../../../../milestone-workspace.logger';

type MilestoneSynchronizationMode = 'targetA' | 'targetB' | 'union';

const TRANSLATE_KEYS_BASE =
  'sqtm-core.administration-workspace.milestones.dialog.message.synchronization.';

const logger = milestoneWorkspaceLogger.compose('MilestoneSynchronizationDialogComponent');

@Component({
  selector: 'sqtm-app-milestone-synchronization-dialog',
  templateUrl: './milestone-synchronization-dialog.component.html',
  styleUrls: ['./milestone-synchronization-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MilestoneSynchronizationDialogComponent implements OnInit {
  translateKeys = {
    synchronizeMilestonesTargetMode: TRANSLATE_KEYS_BASE + 'synchronize-milestone-target-mode',
    synchronizeMilestonesUnionMode: TRANSLATE_KEYS_BASE + 'synchronize-milestone-union-mode',
    invalidSynchronizationTarget: TRANSLATE_KEYS_BASE + 'invalid-synchronization-target',
    invalidSynchronizationUnion: TRANSLATE_KEYS_BASE + 'invalid-synchronization-union',
    synchronizeMilestonesWarning: TRANSLATE_KEYS_BASE + 'synchronize-milestone-warning',
    extendMilestonePerimeter: TRANSLATE_KEYS_BASE + 'extend-milestone-perimeter',
    extendMilestonePerimeterInfo: TRANSLATE_KEYS_BASE + 'extend-milestone-perimeter-info',
    milestoneRangeForbidsUnion: TRANSLATE_KEYS_BASE + 'milestone-range-forbids-union',
    milestoneRangeForbidsTarget: TRANSLATE_KEYS_BASE + 'milestone-range-forbids-target',
  };

  data: MilestoneSynchronizationDialogData;

  serverSideValidationErrors: FieldValidationError[] = [];

  synchronizationMode: MilestoneSynchronizationMode = 'targetA';
  extendPerimeter = false;

  firstTargetModeParams: TargetModeTranslateParams;
  secondTargetModeParams: TargetModeTranslateParams;

  constructor(
    private dialogReference: DialogReference<MilestoneSynchronizationDialogData>,
    private restService: RestService,
  ) {
    this.data = dialogReference.data;
    logger.debug('Initialize synchronisation dialog with configuration: ', [dialogReference.data]);

    if (this.isFirstTargetDisabled) {
      this.synchronizationMode = 'targetB';
    }
  }

  get getFirstTargetTooltip(): string {
    return this.getTargetTooltip(this.data.milestoneA);
  }

  get getSecondTargetTooltip(): string {
    return this.getTargetTooltip(this.data.milestoneB);
  }

  get isFirstTargetDisabled(): boolean {
    const isProjectManagerAndIsGlobal = this.data.isMilestoneManager && this.data.milestoneA.global;
    return this.data.milestoneA.locked || isProjectManagerAndIsGlobal;
  }

  get isSecondTargetDisabled(): boolean {
    const isProjectManagerAndIsGlobal = this.data.isMilestoneManager && this.data.milestoneB.global;
    return this.data.milestoneB.locked || isProjectManagerAndIsGlobal;
  }

  get isUnionDisabled(): boolean {
    const isProjectManagerAndAnyIsGlobal =
      this.data.isMilestoneManager && (this.data.milestoneA.global || this.data.milestoneB.global);
    const anyLocked = this.data.milestoneA.locked || this.data.milestoneB.locked;
    return anyLocked || isProjectManagerAndAnyIsGlobal;
  }

  get isExtendPerimeterDisabled(): boolean {
    const projectManagerDoesNotOwnAnyMilestone =
      this.data.isMilestoneManager && !(this.data.milestoneA.owned || this.data.milestoneB.owned);
    return projectManagerDoesNotOwnAnyMilestone;
  }

  get getUnionTooltip(): string {
    const isProjectManagerAndAnyIsGlobal =
      this.data.isMilestoneManager && (this.data.milestoneA.global || this.data.milestoneB.global);

    if (isProjectManagerAndAnyIsGlobal) {
      return this.translateKeys.milestoneRangeForbidsUnion;
    }

    const anyLocked = this.data.milestoneA.locked || this.data.milestoneB.locked;
    return anyLocked ? this.translateKeys.invalidSynchronizationUnion : '';
  }

  ngOnInit(): void {
    this.firstTargetModeParams = {
      source: this.data.milestoneB.label,
      target: this.data.milestoneA.label,
    };

    this.secondTargetModeParams = {
      source: this.data.milestoneA.label,
      target: this.data.milestoneB.label,
    };
  }

  confirm(): void {
    if (!this.synchronizationMode || this.synchronizationMode.length === 0) {
      return;
    }

    let sourceId, targetId, union;

    switch (this.synchronizationMode) {
      case 'targetA':
        sourceId = this.data.milestoneB.id;
        targetId = this.data.milestoneA.id;
        union = false;
        break;
      case 'targetB':
        sourceId = this.data.milestoneA.id;
        targetId = this.data.milestoneB.id;
        union = false;
        break;
      case 'union':
        sourceId = this.data.milestoneA.id;
        targetId = this.data.milestoneB.id;
        union = true;
        break;
      default:
        throw new Error('Invalid synchronization mode... How did you get there ?');
    }

    const urlParts = ['milestones', sourceId.toString(), 'synchronize', targetId.toString()];
    const body = {
      union,
      extendPerimeter: this.data.isMilestoneManager && this.extendPerimeter,
    };

    this.restService.post(urlParts, body).subscribe(() => {
      this.dialogReference.close();
    });
  }

  private getTargetTooltip(milestoneInfo: MilestoneSynchronizationInfo): string {
    const isProjectManagerAndMilestoneIsGlobal =
      this.data.isMilestoneManager && milestoneInfo.global;

    if (isProjectManagerAndMilestoneIsGlobal) {
      return this.translateKeys.milestoneRangeForbidsTarget;
    }

    const isMilestoneLocked = milestoneInfo.locked;
    return isMilestoneLocked ? this.translateKeys.invalidSynchronizationTarget : '';
  }
}

export interface MilestoneSynchronizationDialogData {
  dialogId: string;
  titleKey: string;
  milestoneA: MilestoneSynchronizationInfo;
  milestoneB: MilestoneSynchronizationInfo;
  isMilestoneManager: boolean;
}

export interface TargetModeTranslateParams {
  source: string;
  target: string;
}

export interface MilestoneSynchronizationInfo {
  id: Identifier;
  label: string;
  locked: boolean;
  owned: boolean;
  global: boolean;
}
