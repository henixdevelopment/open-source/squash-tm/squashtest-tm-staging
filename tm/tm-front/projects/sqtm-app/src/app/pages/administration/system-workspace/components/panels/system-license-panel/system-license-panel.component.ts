import { ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { SystemViewState } from '../../../states/system-view.state';
import { SystemViewService } from '../../../services/system-view.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'sqtm-app-system-license-panel',
  templateUrl: './system-license-panel.component.html',
  styleUrls: [
    './system-license-panel.component.less',
    '../../../styles/system-workspace.common.less',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SystemLicensePanelComponent implements OnInit, OnDestroy {
  @Input() componentData: SystemViewState;

  currentActiveUsersCountIsLoaded$ = new BehaviorSubject<boolean>(false);

  constructor(public readonly systemViewService: SystemViewService) {}

  ngOnInit(): void {
    this.systemViewService.retrieveCurrentActiveUsersCount().subscribe(() => {
      this.currentActiveUsersCountIsLoaded$.next(true);
    });
  }

  ngOnDestroy(): void {
    this.currentActiveUsersCountIsLoaded$.complete();
  }
}
