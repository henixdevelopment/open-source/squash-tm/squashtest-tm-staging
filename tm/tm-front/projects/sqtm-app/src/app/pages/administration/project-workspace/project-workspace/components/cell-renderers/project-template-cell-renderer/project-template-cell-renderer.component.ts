import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  BooleanValueRenderer,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-template-cell-renderer',
  template: ` @if (columnDisplay && row) {
    <div
      class="full-width full-height flex-column"
      nz-tooltip
      [nzTooltipTitle]="tooltipKey | translate"
    >
      @if (row.data[columnDisplay.id]) {
        <span class="template-icon-container">
          <i
            nz-icon
            nzType="sqtm-core-administration:template"
            nzTheme="outline"
            class="table-icon-size"
          ></i>
        </span>
      }
    </div>
  }`,
  styleUrls: ['./project-template-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectTemplateCellRendererComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }

  get tooltipKey(): string {
    const isTemplate = this.row.data[this.columnDisplay.id];
    return isTemplate
      ? 'sqtm-core.entity.project.template.label.singular'
      : 'sqtm-core.entity.project.label.singular';
  }
}

export function templateColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(ProjectTemplateCellRendererComponent)
    .withExportValueRenderer(BooleanValueRenderer);
}
