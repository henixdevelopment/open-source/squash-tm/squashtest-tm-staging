import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  Output,
  QueryList,
  ViewChildren,
} from '@angular/core';
import {
  AuthConfiguration,
  AuthenticationProtocol,
  isKnownAuthenticationProtocol,
  OAuth2Configuration,
  Option,
  RestService,
  SignatureMethod,
  TextAreaFieldComponent,
  TextFieldComponent,
} from 'sqtm-core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

const OAUTH2_TRANSLATE_KEY_BASE = 'sqtm-core.administration-workspace.bugtrackers.oauth2.';

@Component({
  selector: 'sqtm-app-auth-protocol-form',
  templateUrl: './auth-protocol-form.component.html',
  styleUrls: ['./auth-protocol-form.component.less'],
  changeDetection: ChangeDetectionStrategy.Default,
})
export class AuthProtocolFormComponent {
  private _protocol: AuthenticationProtocol;

  private _configuration: AuthConfiguration;

  private _defaultUrl: string;

  private _defaultOAuth2FormValues: DefaultOAuth2FormValues;

  @Input()
  supportedAuthenticationProtocols: AuthenticationProtocol[];

  @Input()
  set authConfigurationFormData(formData: AuthProtocolFormData) {
    try {
      this._configuration = formData.authConfiguration;
      this._protocol = formData.authenticationProtocol;
      this._defaultUrl = formData.defaultUrl;
      this._defaultOAuth2FormValues = formData?.defaultOAuth2FormValues;

      this.buildFormGroup();
    } catch (e) {
      console.error(`Unknown authentication protocol ${e} or bad configuration`);
    }
    this.resetState();
  }

  get authConfigurationFormData(): AuthProtocolFormData {
    return {
      authenticationProtocol: this._protocol,
      authConfiguration: this._configuration,
      defaultUrl: this._defaultUrl,
      defaultOAuth2FormValues: this._defaultOAuth2FormValues,
    };
  }

  @Output()
  configurationChanged = new EventEmitter<AuthConfiguration>();

  @Output()
  authProtocolChanged = new EventEmitter<AuthenticationProtocol>();

  @ViewChildren(TextFieldComponent)
  textFields: QueryList<TextFieldComponent>;

  @ViewChildren(TextAreaFieldComponent)
  textAreaFields: QueryList<TextAreaFieldComponent>;

  requestMethodOptions = [
    { id: 'GET', label: 'GET' },
    { id: 'POST', label: 'POST' },
  ];

  oauth2GrantTypeOptions = [{ id: 'code', label: 'Authorisation Code' }];

  signatureMethodOptions = [
    { id: SignatureMethod.HMAC_SHA1, label: 'HMAC-SHA1' },
    { id: SignatureMethod.RSA_SHA1, label: 'RSA-SHA1' },
  ];

  oauth2FormGroup: FormGroup;

  translateKeys = {
    grantType: OAUTH2_TRANSLATE_KEY_BASE + 'grant-type',
    clientId: OAUTH2_TRANSLATE_KEY_BASE + 'client-id',
    clientSecret: OAUTH2_TRANSLATE_KEY_BASE + 'client-secret',
    authorizationUrl: OAUTH2_TRANSLATE_KEY_BASE + 'authorization-url',
    requestTokenUrl: OAUTH2_TRANSLATE_KEY_BASE + 'request-token-url',
    callbackUrl: OAUTH2_TRANSLATE_KEY_BASE + 'callback-url',
    scope: OAUTH2_TRANSLATE_KEY_BASE + 'scope',

    unsavedChangesKey: 'sqtm-core.administration-workspace.servers.label.unsaved-changes',
    saveSuccessKey: 'sqtm-core.administration-workspace.servers.label.save-success',
    saveFailKey: 'sqtm-core.error.generic.label',
  };

  private readonly _authProtocolOptions: Option[];
  private hasServerError = false;
  private hasSaved = false;

  get canShowSelectField(): boolean {
    return (
      this.authConfigurationFormData.authenticationProtocol &&
      this.supportedAuthenticationProtocols &&
      this.visibleProtocolOptions
        .map((opt) => opt.value)
        .includes(this.authConfigurationFormData.authenticationProtocol)
    );
  }

  constructor(
    public readonly translateService: TranslateService,
    private readonly formBuilder: FormBuilder,
    private readonly cdRef: ChangeDetectorRef,
    private readonly restService: RestService,
  ) {
    this._authProtocolOptions = [
      {
        value: AuthenticationProtocol.BASIC_AUTH,
        label: translateService.instant('sqtm-core.entity.server.auth-protocol.basic-auth.label'),
      },
      {
        value: AuthenticationProtocol.TOKEN_AUTH,
        label: translateService.instant('sqtm-core.entity.server.auth-protocol.token-auth.label'),
      },
      {
        value: AuthenticationProtocol.OAUTH_2,
        label: translateService.instant('sqtm-core.entity.server.auth-protocol.oauth_2.label'),
      },
    ];
  }

  private buildFormGroup(): void {
    const hasConfiguration = Boolean(this.authConfigurationFormData.authConfiguration);
    const protocol = this.authConfigurationFormData.authenticationProtocol;
    if (hasConfiguration) {
      const conf = this.authConfigurationFormData.authConfiguration;
      if (conf.implementedProtocol === AuthenticationProtocol.OAUTH_2) {
        this.oauth2FormGroup = this.buildFormFromConfiguration(conf);
      } else {
        throw new Error(`Unknown authentication protocol ${conf.implementedProtocol}`);
      }
    } else {
      switch (protocol) {
        case AuthenticationProtocol.OAUTH_2:
          this.buildDefaultFormGroupForOAuth2();
          break;
        case AuthenticationProtocol.BASIC_AUTH:
        case AuthenticationProtocol.TOKEN_AUTH:
          // NOOP
          break;
        default:
          console.error(`Unknown authentication protocol : ${protocol}`);
      }
    }
  }

  private buildFormFromConfiguration(conf: AuthConfiguration): FormGroup {
    const controlsConfig = {};
    Object.keys(conf).forEach(
      (key) => (controlsConfig[key] = this.formBuilder.control(conf[key], [Validators.required])),
    );
    return this.formBuilder.group(controlsConfig);
  }

  /*OAUTH 2*/
  public get isOAuth2(): boolean {
    return this.authConfigurationFormData.authenticationProtocol === AuthenticationProtocol.OAUTH_2;
  }

  private buildDefaultFormGroupForOAuth2(): void {
    this.oauth2FormGroup = this.formBuilder.group({
      grantType: this.oauth2GrantTypeOptions[0].id,
      clientId: this.formBuilder.control('', [Validators.required]),
      clientSecret: this.formBuilder.control('', [Validators.required]),
      authorizationUrl: this.formBuilder.control(
        this.authConfigurationFormData.defaultOAuth2FormValues.authorizationUrl,
        [Validators.required],
      ),
      requestTokenUrl: this.formBuilder.control(
        this.authConfigurationFormData.defaultOAuth2FormValues.requestTokenUrl,
        [Validators.required],
      ),
      callbackUrl: this.formBuilder.control(
        `${window.location.origin}${this.restService.backendContextPath}oauth2/authentication`,
        [Validators.required],
      ),
      scope: this.formBuilder.control(
        this.authConfigurationFormData.defaultOAuth2FormValues.scope,
        [Validators.required],
      ),
    });
    this.cdRef.detectChanges();
  }

  sendOAuth2Form() {
    if (this.authConfigurationFormData.authenticationProtocol === AuthenticationProtocol.OAUTH_2) {
      if (this.checkOAuthFormValidity()) {
        const auth2Conf: OAuth2Configuration = {
          implementedProtocol: this.authConfigurationFormData.authenticationProtocol,
          type: this.authConfigurationFormData.authenticationProtocol,
          grantType: this.oauth2FormGroup.controls['grantType'].value,
          clientId: this.oauth2FormGroup.controls['clientId'].value,
          clientSecret: this.oauth2FormGroup.controls['clientSecret'].value,
          authorizationUrl: this.oauth2FormGroup.controls['authorizationUrl'].value,
          requestTokenUrl: this.oauth2FormGroup.controls['requestTokenUrl'].value,
          callbackUrl: this.oauth2FormGroup.controls['callbackUrl'].value,
          scope: this.oauth2FormGroup.controls['scope'].value,
        };
        this.configurationChanged.emit(auth2Conf);
      }
    }
  }

  handleServerSuccess(): void {
    this.refreshFieldErrors();
    this.hasServerError = false;
    this.hasSaved = true;
    this.getFormGroup().markAsPristine();
  }

  handleServerError(): void {
    this.refreshFieldErrors();
    this.hasServerError = true;
    this.hasSaved = false;
    this.cdRef.detectChanges();
  }

  public get visibleProtocolOptions(): Option[] {
    return this._authProtocolOptions.filter((option) =>
      this.supportedAuthenticationProtocols.includes(AuthenticationProtocol[option.value]),
    );
  }

  public get authConfigurationStatusMessage(): string {
    if (this.hasServerError) {
      return this.translateKeys.saveFailKey;
    }

    if (this.getFormGroup().dirty) {
      return this.translateKeys.unsavedChangesKey;
    }

    if (this.hasSaved) {
      return this.translateKeys.saveSuccessKey;
    }

    return '';
  }

  private getFormGroup(): FormGroup {
    return this.oauth2FormGroup;
  }

  private checkOAuthFormValidity(): boolean {
    if (this.getFormGroup().valid) {
      return true;
    } else {
      this.refreshFieldErrors();
      return false;
    }
  }

  private refreshFieldErrors(): void {
    this.textFields.forEach((textField) => textField.showClientSideError());
    this.textAreaFields.forEach((textField) => textField.showClientSideError());
  }

  confirmAuthProtocol(option: Option): void {
    if (isKnownAuthenticationProtocol(option.value)) {
      this.authProtocolChanged.emit(option.value);
    }
  }

  private resetState(): void {
    this.hasSaved = false;
    this.hasServerError = false;
  }
}

export interface AuthProtocolFormData {
  authenticationProtocol: AuthenticationProtocol;
  authConfiguration: AuthConfiguration;
  defaultUrl: string;
  defaultOAuth2FormValues?: DefaultOAuth2FormValues;
}

export interface DefaultOAuth2FormValues {
  authorizationUrl: string;
  requestTokenUrl: string;
  scope: string;
}
