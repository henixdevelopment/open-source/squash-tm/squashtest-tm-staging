import { TestCaseFromAi } from 'sqtm-core';

export interface AiServerTestCaseGenerationDialogConfiguration {
  serverResponse: { testCases: TestCaseFromAi[] };
}
