import { TestBed } from '@angular/core/testing';

import { AddJobDialogService } from './add-job-dialog.service';
import { RestService, TestAutomationProject } from 'sqtm-core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ProjectViewService } from './project-view.service';
import { of, throwError } from 'rxjs';
import { AppTestingUtilsModule } from '../../../../../utils/testing-utils/app-testing-utils.module';
import { take } from 'rxjs/operators';
import { mockRestService } from '../../../../../utils/testing-utils/mocks.service';

describe('AddJobDialogService', () => {
  const restService = mockRestService();

  let service: AddJobDialogService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule],
      providers: [
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: ProjectViewService,
          useClass: ProjectViewService,
        },
      ],
    });

    service = TestBed.inject(AddJobDialogService);
  });

  it('should be created in "loading" state', async () => {
    // when
    expect(service).toBeTruthy();

    // then
    service.dataState$.subscribe((dataState) => {
      expect(dataState).toBe('loading');
    });
  });

  it('should load project jobs', async () => {
    // given
    restService.get.and.returnValue(
      of({
        taProjects: taProjectsTestData(),
      }),
    );

    // when
    service.load(1, true);

    // then
    service.dataState$.subscribe((dataState) => {
      expect(dataState).toBe('ready');

      service.taProjects$.subscribe((taProjects) => {
        expect(taProjects.length).toBe(2);
      });
    });
  });

  it('should handle loading errors', async () => {
    // given
    restService.get.and.returnValue(
      throwError(() => ({
        squashTMError: {
          error: 'boom!',
        },
      })),
    );

    // when
    service.load(1, true);

    // then
    service.dataState$.subscribe((dataState) => {
      expect(dataState).toBe('error');
    });
  });

  it('should unload jobs', async () => {
    // given
    restService.get.and.returnValue(
      of({
        taProjects: taProjectsTestData(),
      }),
    );

    service.load(1, true);

    // when
    service.unload();

    // then
    service.dataState$.subscribe((dataState) => {
      expect(dataState).toBe('loading');

      service.taProjects$.subscribe((taProjects) => {
        expect(taProjects.length).toBe(0);
      });
    });
  });

  it('should set a job label', async () => {
    // given
    restService.get.and.returnValue(
      of({
        taProjects: taProjectsTestData(),
      }),
    );

    service.load(1, true);

    // when
    service.setJobLabel('remoteName2', 'label3');

    // then
    service.taProjects$.subscribe((taProjects) => {
      expect(taProjects.find((job) => job.remoteName === 'remoteName2')?.label).toBe('label3');
    });
  });

  it('should set a unique job that can run BDD', async () => {
    // given
    restService.get.and.returnValue(
      of({
        taProjects: taProjectsTestData(),
      }),
    );

    service.load(1, true);

    // when
    service.setCanRunBdd('remoteName1', true);
    service.setCanRunBdd('remoteName2', true);

    // then
    service.state$.pipe(take(1)).subscribe((state) => {
      const firstJob = state.taProjects.find((job) => job.remoteName === 'remoteName1');
      const secondJob = state.taProjects.find((job) => job.remoteName === 'remoteName2');
      expect(firstJob.canRunBdd).toBeFalsy();
      expect(secondJob.canRunBdd).toBeTruthy();
    });
  });

  it('should toggle off a job that can run BDD', async () => {
    // given
    restService.get.and.returnValue(
      of({
        taProjects: taProjectsTestData(),
      }),
    );

    service.load(1, true);

    // when
    service.setCanRunBdd('remoteName1', true);
    service.setCanRunBdd('remoteName1', false);

    // then
    service.state$.pipe(take(1)).subscribe((state) => {
      const firstJob = state.taProjects.find((job) => job.remoteName === 'remoteName1');
      expect(firstJob.canRunBdd).toBeFalsy();
    });
  });
});

function taProjectsTestData(): TestAutomationProject[] {
  return [
    {
      taProjectId: 1,
      tmProjectId: 1,
      remoteName: 'remoteName1',
      label: 'label1',
      serverId: 1,
      executionEnvironments: 'executionEnvironments1',
      canRunBdd: false,
    },
    {
      taProjectId: 2,
      tmProjectId: 1,
      remoteName: 'remoteName2',
      label: 'label2',
      serverId: 1,
      executionEnvironments: 'executionEnvironments2',
      canRunBdd: false,
    },
  ];
}
