import { Injectable } from '@angular/core';
import {
  AclGroup,
  AdminReferentialDataService,
  AttachmentService,
  AuthenticatedUser,
  EntityViewAttachmentHelperService,
  GenericEntityViewService,
  GenericEntityViewState,
  getAclGroupDescriptionI18nKey,
  getAclGroupI18nKey,
  ProfilePermissions,
  ProfileView,
  RestService,
  WorkspaceActivePermissions,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { AdminProfileState } from '../states/admin-profile-state';
import {
  AdminProfileViewState,
  provideInitialAdminProfileView,
} from '../states/admin-profile-view-state';
import { map, switchMap, take, withLatestFrom } from 'rxjs/operators';
import { ProfileService } from '../../profile-workspace/services/profile.service';
import { Router } from '@angular/router';

@Injectable()
export class ProfileViewService extends GenericEntityViewService<AdminProfileState, 'profile'> {
  public readonly authorizationCount$: Observable<number>;

  public getInitialState(): GenericEntityViewState<AdminProfileState, 'profile'> {
    return provideInitialAdminProfileView();
  }

  constructor(
    protected restService: RestService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
    protected adminReferentialData: AdminReferentialDataService,
    protected profileService: ProfileService,
    private router: Router,
  ) {
    super(restService, attachmentService, translateService, attachmentHelper);
    this.authorizationCount$ = this.componentData$.pipe(
      map((componentData) => componentData.profile.partyProfileAuthorizations.length),
    );
  }

  protected getRootUrl(_initialState?): string {
    return 'profile-view';
  }

  load(profileId: number) {
    return this.restService
      .getWithoutErrorHandling<ProfileView>(['profile-view', profileId.toString()])
      .subscribe({
        next: (profile) => this.initializeProfile(profile),
        error: (err) => this.notifyEntityNotFound(err),
      });
  }

  private initializeProfile(profile: ProfileView): void {
    const profileState: AdminProfileState = {
      ...profile,
      attachmentList: { id: null, attachments: null },
    };
    this.initializeEntityState(profileState);
    this.populateProfileName(profileState);
    this.populateSystemProfileDescription(profileState);
  }

  populateProfileName(profileState: AdminProfileState): void {
    profileState.name = profileState.system
      ? this.translateService.instant(getAclGroupI18nKey(profileState.qualifiedName as AclGroup))
      : profileState.qualifiedName;
  }

  populateSystemProfileDescription(profileState: AdminProfileState): void {
    if (profileState.system) {
      profileState.description = this.translateService.instant(
        getAclGroupDescriptionI18nKey(profileState.qualifiedName as AclGroup),
      );
    }
  }

  getCurrentUser(): Observable<AuthenticatedUser> {
    return this.adminReferentialData.authenticatedUser$;
  }

  changePermissions(profileId: number, panels: WorkspaceActivePermissions[]) {
    const urlParts = [this.getRootUrl(), profileId.toString(), 'permissions'];

    return this.restService.post(urlParts, panels);
  }

  getProfilePermissions(profileId: number): Observable<ProfilePermissions[]> {
    const urlParts = [this.getRootUrl(), profileId.toString(), 'permissions'];
    return this.restService.get<ProfilePermissions[]>(urlParts);
  }

  activateProfile() {
    this.store.state$
      .pipe(
        take(1),
        switchMap((adminProfileViewState: AdminProfileViewState) =>
          this.activateProfileServerSide(adminProfileViewState),
        ),
        withLatestFrom(this.store.state$),
        map(([, state]: [any, AdminProfileViewState]) =>
          this.updateStateForActivatedProfile(state),
        ),
        switchMap((updatedState) =>
          this.profileService
            .getDataRowProfiles(updatedState.profile.id)
            .pipe(map(() => updatedState)),
        ),
      )
      .subscribe((state) => {
        this.store.commit(state);
        this.navigateToInformationAnchor(state);
      });
  }

  private navigateToInformationAnchor(state: AdminProfileViewState) {
    this.router
      .navigate(['administration-workspace', 'profiles', 'manage', state.profile.id, 'content'], {
        queryParams: { anchor: 'information' },
      })
      .then(() => {});
  }

  private activateProfileServerSide(adminProfileViewState: AdminProfileViewState) {
    const profileId = adminProfileViewState.profile.id.toString();
    return this.restService.post([this.getRootUrl(), profileId, 'activate']);
  }

  private updateStateForActivatedProfile(state: AdminProfileViewState) {
    return {
      ...state,
      profile: {
        ...state.profile,
        active: true,
      },
    };
  }

  deactivateProfile() {
    this.store.state$
      .pipe(
        take(1),
        switchMap((adminProfileViewState: AdminProfileViewState) =>
          this.deactivateProfileServerSide(adminProfileViewState),
        ),
        withLatestFrom(this.store.state$),
        map(([, state]: [any, AdminProfileViewState]) =>
          this.updateStateForDeactivatedProfile(state),
        ),
        switchMap((updatedState) =>
          this.profileService
            .getDataRowProfiles(updatedState.profile.id)
            .pipe(map(() => updatedState)),
        ),
      )
      .subscribe((state) => {
        this.store.commit(state);
        this.navigateToInformationAnchor(state);
      });
  }

  private deactivateProfileServerSide(adminProfileViewState: AdminProfileViewState) {
    const profileId = adminProfileViewState.profile.id.toString();
    return this.restService.post([this.getRootUrl(), profileId, 'deactivate']);
  }

  private updateStateForDeactivatedProfile(state: AdminProfileViewState) {
    return {
      ...state,
      profile: {
        ...state.profile,
        active: false,
      },
    };
  }
}
