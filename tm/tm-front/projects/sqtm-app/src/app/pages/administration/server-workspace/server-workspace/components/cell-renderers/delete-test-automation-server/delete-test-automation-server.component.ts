import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  DataRow,
  DialogService,
  Fixed,
  GridColumnId,
  GridService,
  RestService,
  TestAutomationServerKind,
} from 'sqtm-core';
import { Subject } from 'rxjs';
import { concatMap, filter, takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-delete-test-automation-server-cell-renderer',
  template: `
    @if (row) {
      <div
        class="full-height full-width flex-column icon-container current-workspace-main-color"
        (click)="removeItem(row)"
      >
        <i nz-icon [nzType]="getIcon()" nzTheme="outline" class="table-icon-size"></i>
      </div>
    }
  `,
  styleUrls: ['./delete-test-automation-server.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteTestAutomationServerComponent
  extends AbstractCellRendererComponent
  implements OnDestroy
{
  unsub$ = new Subject<void>();

  constructor(
    public grid: GridService,
    cdr: ChangeDetectorRef,
    private dialogService: DialogService,
    private restService: RestService,
    private translateService: TranslateService,
  ) {
    super(grid, cdr);
  }

  getIcon(): string {
    return 'sqtm-core-generic:delete';
  }

  public get rowHasAutomationProjectWithExecution(): boolean {
    return this.row.data[GridColumnId.executionCount] > 0;
  }

  public get rowHasAutomationProject(): boolean {
    return this.row.data[GridColumnId.projectCount] > 0;
  }

  public get isJenkinsServerKind(): boolean {
    return this.row.data[GridColumnId.kind] === TestAutomationServerKind.jenkins;
  }

  removeItem(row: DataRow) {
    const serverName: string = row.data.name;
    let messageKey = '';

    if (this.isJenkinsServerKind) {
      messageKey = this.translateService.instant(
        this.rowHasAutomationProjectWithExecution
          ? 'sqtm-core.administration-workspace.servers.test-automation-servers.dialog.message.delete-one-with-bound-project'
          : 'sqtm-core.administration-workspace.servers.test-automation-servers.dialog.message.delete-one-without-bound-project',
        { serverName: serverName },
      );
    } else {
      messageKey = this.translateService.instant(
        this.rowHasAutomationProject
          ? 'sqtm-core.administration-workspace.servers.test-automation-servers.dialog.message.delete-one-orchestrator-with-bound-project'
          : 'sqtm-core.administration-workspace.servers.test-automation-servers.dialog.message.delete-one-without-bound-project',
        { serverName: serverName },
      );
    }

    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey:
        'sqtm-core.administration-workspace.servers.test-automation-servers.dialog.title.delete-one',
      messageKey: messageKey,
      level: 'DANGER',
    });

    dialogReference.dialogClosed$
      .pipe(
        takeUntil(this.unsub$),
        filter((result) => result === true),
        concatMap(() =>
          this.restService.delete([`test-automation-servers/${row.data[GridColumnId.serverId]}`]),
        ),
      )
      .subscribe(() => {
        this.grid.refreshData();
      });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}

export function deleteTestAutomationServerColumn(
  id: GridColumnId,
  label = '',
): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(DeleteTestAutomationServerComponent)
    .withLabel(label)
    .disableSort()
    .changeWidthCalculationStrategy(new Fixed(50));
}
