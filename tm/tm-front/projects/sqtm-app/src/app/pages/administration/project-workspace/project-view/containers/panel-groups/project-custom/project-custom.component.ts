import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { filter, take, takeUntil } from 'rxjs/operators';
import { AdminProjectViewComponentData } from '../../project-view/project-view.component';
import { ProjectViewService } from '../../../services/project-view.service';
import {
  ProjectCustomFieldsPanelComponent,
  projectCustomFieldsTableDefinition,
} from '../../../components/panels/project-custom-fields-panel/project-custom-fields-panel.component';
import {
  DialogService,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {
  PROJECT_CUSTOM_FIELDS_TABLE,
  PROJECT_CUSTOM_FIELDS_TABLE_CONF,
} from '../../../project-view.constant';

@Component({
  selector: 'sqtm-app-project-custom',
  templateUrl: './project-custom.component.html',
  styleUrls: ['./project-custom.component.less'],
  providers: [
    {
      provide: PROJECT_CUSTOM_FIELDS_TABLE_CONF,
      useFactory: projectCustomFieldsTableDefinition,
    },
    {
      provide: PROJECT_CUSTOM_FIELDS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, PROJECT_CUSTOM_FIELDS_TABLE_CONF, ReferentialDataService],
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectCustomComponent implements OnInit, OnDestroy {
  @ViewChild(ProjectCustomFieldsPanelComponent)
  private projectPermissionsPanelComponent: ProjectCustomFieldsPanelComponent;

  componentData$: Observable<AdminProjectViewComponentData>;

  unsub$ = new Subject<void>();

  @ViewChild(ProjectCustomFieldsPanelComponent)
  private projectCustomFieldsPanelComponent;

  constructor(
    public readonly projectViewService: ProjectViewService,
    public readonly translateService: TranslateService,
    public readonly dialogService: DialogService,
    public readonly restService: RestService,
    @Inject(PROJECT_CUSTOM_FIELDS_TABLE) public customFieldGrid: GridService,
  ) {}

  ngOnInit(): void {
    this.componentData$ = this.projectViewService.componentData$.pipe(takeUntil(this.unsub$));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  addCustomField(event: MouseEvent) {
    event.stopPropagation();
    event.preventDefault();

    this.componentData$
      .pipe(
        takeUntil(this.unsub$),
        filter((componentData: AdminProjectViewComponentData) => Boolean(componentData.project.id)),
        take(1),
      )
      .subscribe((componentData) => {
        if (componentData.project.linkedTemplateId != null) {
          this.openAlertForLockedParameter();
        } else {
          this.projectPermissionsPanelComponent.openBindCustomFieldDialog();
        }
      });
  }

  unbindCustomFields($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.componentData$
      .pipe(
        takeUntil(this.unsub$),
        filter((componentData: AdminProjectViewComponentData) => Boolean(componentData.project.id)),
        take(1),
      )
      .subscribe((componentData) => {
        if (componentData.project.linkedTemplateId != null) {
          this.openAlertForLockedParameter();
        } else {
          this.projectCustomFieldsPanelComponent.openUnbindCustomFieldsDialog();
        }
      });
  }

  private openAlertForLockedParameter() {
    this.dialogService.openAlert({
      titleKey:
        'sqtm-core.administration-workspace.projects.dialog.title.parameter-locked-because-project-is-bound-to-a-template',
      messageKey:
        'sqtm-core.administration-workspace.projects.dialog.message.parameter-locked-because-project-is-bound-to-a-template',
      level: 'WARNING',
    });
  }

  stopPropagation($event: MouseEvent): void {
    $event.stopPropagation();
  }
}
