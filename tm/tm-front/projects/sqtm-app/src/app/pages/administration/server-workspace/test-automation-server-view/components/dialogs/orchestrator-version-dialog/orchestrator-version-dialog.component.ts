import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import {
  createStore,
  DialogConfiguration,
  DialogReference,
  OrchestratorConfVersions,
  OrchestratorResponse,
  RestService,
  Store,
} from 'sqtm-core';
import { Observable } from 'rxjs';

@Component({
  selector: 'sqtm-app-orchestrator-version-dialog',
  templateUrl: './orchestrator-version-dialog.component.html',
  styleUrls: ['./orchestrator-version-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrchestratorVersionDialogComponent implements OnInit {
  private store: Store<OrchestratorResponse<OrchestratorConfVersions>>;

  componentData$: Observable<OrchestratorResponse<OrchestratorConfVersions>>;

  data: any;
  constructor(
    private restService: RestService,
    private dialogRef: DialogReference<DialogConfiguration>,
  ) {
    this.data = this.dialogRef.data;
    this.store = createStore<OrchestratorResponse<OrchestratorConfVersions>>(null);
    this.componentData$ = this.store.state$;
  }

  ngOnInit(): void {
    this.restService
      .get<
        OrchestratorResponse<OrchestratorConfVersions>
      >(['orchestrator-operation', this.data.serverId, 'orchestrator-configuration'])
      .subscribe((response: OrchestratorResponse<OrchestratorConfVersions>) =>
        this.store.commit(response),
      );
  }
}
