import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  Signal,
} from '@angular/core';
import {
  AdminReferentialDataService,
  CreationDialogData,
  DialogReference,
  DisplayOption,
  FieldValidationError,
  JsonProjectFromTemplate,
  NOT_ONLY_SPACES_REGEX,
  RestService,
} from 'sqtm-core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AbstractAdministrationCreationDialogDirective } from '../../../../../components/abstract-administration-creation-dialog';
import { of } from 'rxjs';
import { toSignal } from '@angular/core/rxjs-interop';

const Controls = {
  name: 'name',
  description: 'description',
  label: 'label',
  keepTemplateBinding: 'keepTemplateBinding',
  keepPermissions: 'keepPermissions',
  keepCustomFields: 'keepCustomFields',
  keepInfoLists: 'keepInfoLists',
  keepBugtracker: 'keepBugtracker',
  keepAiServer: 'keepAiServer',
  keepAutomation: 'keepAutomation',
  keepMilestones: 'keepMilestones',
  keepAllowTcModificationsFromExecution: 'keepAllowTcModificationsFromExecution',
  keepOptionalExecutionStatuses: 'keepOptionalExecutionStatuses',
  keepPluginsActivation: 'keepPluginsActivation',
  template: 'template',
  keepPluginsBinding: 'keepPluginsBinding',
  copyPluginsConfiguration: 'copyPluginsConfiguration',
};

@Component({
  selector: 'sqtm-app-project-creation-dialog',
  templateUrl: './project-creation-dialog.component.html',
  styleUrls: ['./project-creation-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectCreationDialogComponent
  extends AbstractAdministrationCreationDialogDirective
  implements OnInit
{
  Controls = Controls;
  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  data: CreationDialogData;
  templateOptions: DisplayOption[];

  private readonly noTemplateOptionId = '';
  readonly isUltimate$: Signal<boolean>;

  constructor(
    private fb: FormBuilder,
    private translateService: TranslateService,
    dialogReference: DialogReference,
    restService: RestService,
    cdr: ChangeDetectorRef,
    public readonly adminReferentialDataService: AdminReferentialDataService,
  ) {
    super('projects/new', dialogReference, restService, cdr);
    this.data = this.dialogReference.data;
    this.isUltimate$ = toSignal(adminReferentialDataService.isUltimateLicenseAvailable$);
  }

  get textFieldToFocus(): string {
    return 'name';
  }

  get hasTemplateSelected(): boolean {
    return this.formGroup.controls['template'].value !== this.noTemplateOptionId;
  }

  ngOnInit() {
    this.initializeFormGroup();
    this.initializeTemplateSelectField();
  }

  protected getRequestPayload() {
    const payload: JsonProjectFromTemplate = {
      name: this.getFormControlValue(Controls.name),
      description: this.getFormControlValue(Controls.description),
      label: this.getFormControlValue(Controls.label),
      keepTemplateBinding: this.getFormControlValue(Controls.keepTemplateBinding),
      copyPermissions: this.getFormControlValue(Controls.keepPermissions),
      copyCUF: this.getFormControlValue(Controls.keepCustomFields),
      copyInfolists: this.getFormControlValue(Controls.keepInfoLists),
      copyBugtrackerBinding: this.getFormControlValue(Controls.keepBugtracker),
      copyAiServerBinding: this.getFormControlValue(Controls.keepAiServer),
      copyAutomatedProjects: this.getFormControlValue(Controls.keepAutomation),
      copyMilestone: this.getFormControlValue(Controls.keepMilestones),
      copyAllowTcModifFromExec: this.getFormControlValue(
        Controls.keepAllowTcModificationsFromExecution,
      ),
      copyOptionalExecStatuses: this.getFormControlValue(Controls.keepOptionalExecutionStatuses),
      copyPluginsActivation: this.getFormControlValue(Controls.keepPluginsActivation),
      fromTemplate: this.getFormControlValue(Controls.template) !== this.noTemplateOptionId,
      templateId: this.getFormControlValue(Controls.template),
      keepPluginsBinding: this.getFormControlValue(Controls.keepPluginsBinding),
      copyPluginsConfiguration: this.getFormControlValue(Controls.copyPluginsConfiguration),
    };

    return of(payload);
  }

  protected doResetForm(): void {
    this.resetFormControl(Controls.name, '');
    this.resetFormControl(Controls.description, '');
    this.resetFormControl(Controls.label, '');
    this.resetFormControl(Controls.template, this.noTemplateOptionId);
    this.resetFormControl(Controls.keepTemplateBinding, true);
    this.resetFormControl(Controls.keepPermissions, true);
    this.resetFormControl(Controls.keepCustomFields, true);
    this.resetFormControl(Controls.keepInfoLists, true);
    this.resetFormControl(Controls.keepBugtracker, true);
    this.resetFormControl(Controls.keepAiServer, true);
    this.resetFormControl(Controls.keepAutomation, true);
    this.resetFormControl(Controls.keepMilestones, true);
    this.resetFormControl(Controls.keepAllowTcModificationsFromExecution, true);
    this.resetFormControl(Controls.keepOptionalExecutionStatuses, true);
    this.resetFormControl(Controls.keepPluginsActivation, true);
    this.resetFormControl(Controls.keepPluginsBinding, true);
    this.resetFormControl(Controls.copyPluginsConfiguration, true);

    this.refreshControlsActivation();
  }

  private initializeFormGroup() {
    const controlsConfig = {
      [Controls.name]: this.fb.control('', [
        Validators.required,
        Validators.pattern(NOT_ONLY_SPACES_REGEX),
        Validators.maxLength(255),
      ]),
      [Controls.description]: this.fb.control(''),
      [Controls.label]: this.fb.control('', [Validators.maxLength(255)]),
      [Controls.template]: this.fb.control(this.noTemplateOptionId),
      [Controls.keepTemplateBinding]: this.fb.control(true),
      [Controls.keepPermissions]: this.fb.control(true),
      [Controls.keepCustomFields]: this.fb.control(true),
      [Controls.keepInfoLists]: this.fb.control(true),
      [Controls.keepBugtracker]: this.fb.control(true),
      [Controls.keepAiServer]: this.fb.control(true),
      [Controls.keepAutomation]: this.fb.control(true),
      [Controls.keepMilestones]: this.fb.control(true),
      [Controls.keepAllowTcModificationsFromExecution]: this.fb.control(true),
      [Controls.keepOptionalExecutionStatuses]: this.fb.control(true),
      [Controls.keepPluginsActivation]: this.fb.control(true),
      [Controls.keepPluginsBinding]: this.fb.control(true),
      [Controls.copyPluginsConfiguration]: this.fb.control(true),
    };

    this.formGroup = this.fb.group(controlsConfig);

    this.refreshControlsActivation();
  }

  private refreshControlsActivation(): void {
    const keepTemplateBinding = this.getFormControlValue(Controls.keepTemplateBinding);
    this.handleKeepTemplateBindingChange(keepTemplateBinding);

    const keepPluginConfBinding = this.getFormControlValue(Controls.keepPluginsBinding);
    this.handleKeepPluginConfBindingChange(keepPluginConfBinding);
  }

  private initializeTemplateSelectField() {
    this.restService
      .get<{ templates: { id: string; name: string }[] }>(['generic-projects/templates'])
      .subscribe((response) => {
        const options = retrieveTemplatesAsDisplayOptions(response.templates);
        const defaultOption: DisplayOption = this.retrieveDefaultOption();
        this.templateOptions = [defaultOption, ...options];

        if (options.length > 0) {
          this.getFormControl(Controls.template).enable();
        }

        this.cdr.detectChanges();
      });
  }

  private retrieveDefaultOption() {
    return {
      id: this.noTemplateOptionId,
      label: this.translateService.instant(
        'sqtm-core.administration-workspace.projects.dialog.message.new-project.no-template',
      ),
    };
  }

  /**
   * Handles changes to the `keepTemplateBinding` control in the form.
   * This method updates the values and enables/disables multiple form controls related to template binding
   * based on the value of `keepTemplateBinding`.
   *
   * - Updates the values and states of the `keepCustomFields`, `keepInfoLists`, 'keepOptionalExecutionStatuses'
   *   and `keepAllowTcModificationsFromExecution` controls :
   *   - If `keepTemplateBinding` is `true`, sets the value to `true` and disables the controls.
   *   - If `keepTemplateBinding` is `false`, sets the value to `false` and enables the controls,
   *      unless they are `keepCustomFields` or `keepInfoLists`.
   * - Updates the state of `keepPluginsBinding` and `copyPluginsConfiguration`:
   *   - `keepPluginsBinding` mirrors the value of `keepTemplateBinding`.
   *   - `copyPluginsConfiguration` is set to `true` and disabled if `keepTemplateBinding` is `true`.
   *
   * @param keepTemplateBinding - Indicates whether the template binding should be maintained (`true` to keep it).
   */
  handleKeepTemplateBindingChange(keepTemplateBinding: boolean) {
    const impactedFields: string[] = [
      Controls.keepCustomFields,
      Controls.keepInfoLists,
      Controls.keepAllowTcModificationsFromExecution,
      Controls.keepOptionalExecutionStatuses,
    ];

    impactedFields.forEach((field: string) => {
      const control = this.formGroup.get(field);
      const shouldUpdate: boolean = !(
        keepTemplateBinding === false &&
        (field === Controls.keepCustomFields || field === Controls.keepInfoLists)
      );

      if (shouldUpdate) {
        control.setValue(keepTemplateBinding);
        keepTemplateBinding ? control.disable() : control.enable();
      }
    });

    const pluginsBindingControl = this.getFormControl(Controls.keepPluginsBinding);
    const pluginsConfigControl = this.getFormControl(Controls.copyPluginsConfiguration);

    pluginsBindingControl.setValue(keepTemplateBinding);
    keepTemplateBinding ? pluginsBindingControl.enable() : pluginsBindingControl.disable();

    pluginsConfigControl.setValue(true);
    keepTemplateBinding ? pluginsConfigControl.disable() : pluginsConfigControl.enable();
  }

  /**
   * Handles changes to the `keepPluginConfBinding` control in the form.
   * This method updates the state of the `copyPluginsConfiguration`, `keepCustomFields`,
   * and `keepInfoLists` controls based on the value of `keepPluginConfBinding`.
   *
   * - When `keepPluginConfBinding` is `true`:
   *   - The `copyPluginsConfiguration`, `keepCustomFields`, and `keepInfoLists` controls are set to `true` and disabled.
   * - When `keepPluginConfBinding` is `false`:
   *   - The `copyPluginsConfiguration` control is enabled, allowing user input.
   *
   * @param keepPluginConfBinding - Indicates whether the plugin configuration binding should be kept (`true` to keep it).
   */
  handleKeepPluginConfBindingChange(keepPluginConfBinding: boolean) {
    if (keepPluginConfBinding) {
      this.toggleControl(Controls.copyPluginsConfiguration);
      this.toggleControl(Controls.keepCustomFields);
      this.toggleControl(Controls.keepInfoLists);
    } else {
      this.getFormControl(Controls.copyPluginsConfiguration).enable();
    }
  }

  /**
   * Handles changes to the `copyPluginsConfiguration` control in the form.
   * This method updates the values and enables/disables the associated controls
   * for `keepCustomFields` and `keepInfoLists` based on the value of `copyPluginsConfiguration`.
   *
   * - When `copyPluginsConfiguration` is `true`:
   *   - The `keepCustomFields` and `keepInfoLists` controls are set to `true` and disabled.
   * - When `copyPluginsConfiguration` is `false`:
   *   - If `keepTemplateBinding` is also `false`, the `keepCustomFields` and `keepInfoLists` controls are enabled.
   *
   * @param copyPluginsConfiguration - Indicates whether the plugins configuration should be copied (`true` to enable copying).
   */
  handleCopyPluginsConfigurationChange(copyPluginsConfiguration: boolean) {
    if (copyPluginsConfiguration) {
      this.toggleControl(Controls.keepCustomFields);
      this.toggleControl(Controls.keepInfoLists);
    } else {
      if (this.getFormControl(Controls.keepTemplateBinding).value === false) {
        this.getFormControl(Controls.keepCustomFields).enable();
        this.getFormControl(Controls.keepInfoLists).enable();
      }
    }
  }

  private toggleControl(control: string): void {
    this.getFormControl(control).setValue(true);
    this.getFormControl(control).disable();
  }
}

function retrieveTemplatesAsDisplayOptions(templates: { id: string; name: string }[]) {
  return templates.map((ref) => ({ id: ref.id, label: ref.name }));
}
