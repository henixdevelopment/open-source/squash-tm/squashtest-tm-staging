import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { SystemViewState } from '../../../states/system-view.state';
import { SystemViewService } from '../../../services/system-view.service';
import { takeUntil } from 'rxjs/operators';
import { AdminReferentialDataService } from 'sqtm-core';

@Component({
  selector: 'sqtm-app-system-view-settings',
  templateUrl: './system-view-settings.component.html',
  styleUrls: ['./system-view-settings.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SystemViewSettingsComponent implements OnDestroy {
  componentData$: Observable<SystemViewState>;

  unsub$ = new Subject<void>();

  constructor(
    public readonly systemViewService: SystemViewService,
    public adminReferentialDataService: AdminReferentialDataService,
  ) {
    this.componentData$ = this.systemViewService.componentData$.pipe(takeUntil(this.unsub$));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
