import { AfterViewInit, ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { AbstractMassEditDialog } from '../../../../../../search/test-case-search-page/abstract-mass-edit-dialog';
import { TranslateService } from '@ngx-translate/core';
import {
  ActionErrorDisplayService,
  AdminReferentialDataService,
  DialogReference,
  DialogService,
  DisplayOption,
  Identifier,
  OptionalSelectFieldComponent,
  RestService,
} from 'sqtm-core';
import { concatMap, Observable, of } from 'rxjs';
import { catchError, filter, map, take, tap } from 'rxjs/operators';

const ACTIVATE_OPTION_ID = 'activate';
const CAN_DELETE_OPTION_ID = 'canDelete';

@Component({
  selector: 'sqtm-app-user-multi-edit-dialog',
  templateUrl: './user-multi-edit-dialog.component.html',
  styleUrls: ['./user-multi-edit-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserMultiEditDialogComponent extends AbstractMassEditDialog implements AfterViewInit {
  data: UserMultiEditDialogConfiguration;

  stateDisplayOptions: DisplayOption[];
  canDeleteDisplayOptions: DisplayOption[];

  @ViewChild('stateField')
  stateField: OptionalSelectFieldComponent;

  @ViewChild('canDeleteField')
  canDeleteField: OptionalSelectFieldComponent;

  constructor(
    translateService: TranslateService,
    public adminReferentialDataService: AdminReferentialDataService,
    public dialogReference: DialogReference<UserMultiEditDialogConfiguration>,
    restService: RestService,
    private dialogService: DialogService,
    private actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    super(translateService, restService);
    this.data = this.dialogReference.data;

    this.stateDisplayOptions = [
      {
        id: ACTIVATE_OPTION_ID,
        label: this.translateService.instant('sqtm-core.entity.user.tooltip.activate.multiple'),
      },
      {
        id: 'deactivate',
        label: this.translateService.instant('sqtm-core.entity.user.tooltip.deactivate.multiple'),
      },
    ];

    this.canDeleteDisplayOptions = [
      {
        id: CAN_DELETE_OPTION_ID,
        label: this.translateService.instant(
          'sqtm-core.administration-workspace.users.permission.allow-delete-from-front',
        ),
      },
      {
        id: 'cannotDelete',
        label: this.translateService.instant(
          'sqtm-core.administration-workspace.users.permission.forbid-delete-from-front',
        ),
      },
    ];
  }

  ngAfterViewInit(): void {
    this.stateField.selectedValue = ACTIVATE_OPTION_ID;

    if (this.canDeleteField) {
      this.canDeleteField.selectedValue = CAN_DELETE_OPTION_ID;
    }
  }

  confirm() {
    if (!this.stateField.check && !this.canDeleteField?.check) {
      this.dialogReference.result = false;
      this.dialogReference.close();
      return;
    }

    const payload: UserMassEditPatch = {};

    if (this.stateField.check) {
      payload.changeState = true;
      payload.activated = this.stateField.selectedValue === ACTIVATE_OPTION_ID;
    }

    if (this.canDeleteField?.check) {
      payload.changeCanDelete = true;
      payload.canDelete = this.canDeleteField.selectedValue === CAN_DELETE_OPTION_ID;
    }

    this.isTryingToDeactivateCurrentUser$(payload)
      .pipe(
        filter((isTryingToDeactivateCurrentUser) => !isTryingToDeactivateCurrentUser),
        concatMap(() =>
          this.restService.post(['users', this.data.userIds.join(','), 'mass-update'], payload),
        ),
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
      )
      .subscribe(() => {
        this.dialogReference.result = true;
        this.dialogReference.close();
      });
  }

  private isTryingToDeactivateCurrentUser$(payload: UserMassEditPatch): Observable<boolean> {
    if (!payload.changeState || payload.activated) {
      return of(false);
    }

    return this.adminReferentialDataService.authenticatedUser$.pipe(
      take(1),
      map((user) => this.data.userIds.includes(user.userId.toString())),
      tap((isTryingToDeactivateCurrentUser) => {
        if (isTryingToDeactivateCurrentUser) {
          this.openAlertForSelectedUserIsCurrentUser();
        }
      }),
    );
  }

  private openAlertForSelectedUserIsCurrentUser() {
    this.dialogService.openAlert({
      level: 'DANGER',
      messageKey:
        'sqtm-core.administration-workspace.users.dialog.message.deactivate-current-user-in-multiple-selection',
      titleKey: 'sqtm-core.generic.label.error',
    });
  }
}

export interface UserMultiEditDialogConfiguration {
  id: string;
  titleKey: string;
  userIds: Identifier[];
}

export interface UserMassEditPatch {
  changeState?: boolean;
  activated?: boolean;
  changeCanDelete?: boolean;
  canDelete?: boolean;
}
