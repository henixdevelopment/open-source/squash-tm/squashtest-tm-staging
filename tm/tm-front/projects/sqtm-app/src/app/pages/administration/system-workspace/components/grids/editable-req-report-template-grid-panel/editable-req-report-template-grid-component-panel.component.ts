import {
  ChangeDetectionStrategy,
  Component,
  InjectionToken,
  OnInit,
  ViewContainerRef,
} from '@angular/core';
import {
  AdminReferentialDataService,
  DialogService,
  GridDefinition,
  GridService,
  RestService,
} from 'sqtm-core';
import { SystemViewService } from '../../../services/system-view.service';
import {
  AbstractReportTemplateGridPanelComponent,
  getReportTemplateSmallGrid,
} from '../abstract-report-template-grid-panel/abstract-report-template-grid-panel.component';

export const SYSTEM_WS_EDITABLE_REQ_REPORT_TEMPLATE_GRID_CONFIG =
  new InjectionToken<GridDefinition>(
    'Grid config instance for the editable requirement report template grid in report templates anchor of system workspace',
  );
export const SYSTEM_WS_EDITABLE_REQ_REPORT_TEMPLATE_GRID = new InjectionToken<GridService>(
  'Grid service instance for the editable requirement report template grid in report templates anchor of system workspace',
);

export function editableReqReportTemplateGridDefinition(): GridDefinition {
  return getReportTemplateSmallGrid('editable-req-report-templates');
}

@Component({
  selector: 'sqtm-app-editable-req-report-template-grid-panel',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="admin-workspace-grid-toolbar" style="display:flex; justify-content:space-between">
      <a style="font-size:14px" [href]="downloadDefaultTemplate()">
        {{
          'sqtm-core.administration-workspace.system.report-templates.download-default-template'
            | translate
        }}
      </a>
      <i
        nz-icon
        nz-dropdown
        nzType="sqtm-core-generic:add"
        nzTheme="outline"
        class="current-workspace-button action-icon-size"
        nz-tooltip
        nzTooltipPlacement="topRight"
        [nzTooltipTitle]="
          'sqtm-core.administration-workspace.system.report-templates.dialog.add.title' | translate
        "
        (click)="addTemplate()"
      >
      </i>
    </div>
    <sqtm-core-grid></sqtm-core-grid>
  `,
  providers: [
    {
      provide: GridService,
      useExisting: SYSTEM_WS_EDITABLE_REQ_REPORT_TEMPLATE_GRID,
    },
  ],
})
export class EditableReqReportTemplateGridComponentPanel
  extends AbstractReportTemplateGridPanelComponent
  implements OnInit
{
  constructor(
    public gridService: GridService,
    public systemViewService: SystemViewService,
    public readonly adminReferentialDataService: AdminReferentialDataService,
    protected dialogService: DialogService,
    public vcr: ViewContainerRef,
    public restService: RestService,
  ) {
    super(
      gridService,
      systemViewService,
      adminReferentialDataService,
      dialogService,
      vcr,
      restService,
    );
  }
}
