import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { AdminScmServerViewState } from '../../../states/admin-scm-server-view-state';

@Component({
  selector: 'sqtm-app-scm-server-commit-policy-panel',
  templateUrl: './scm-server-commit-policy-panel.component.html',
  styleUrls: ['./scm-server-commit-policy-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScmServerCommitPolicyPanelComponent {
  @Input()
  componentData: AdminScmServerViewState;
}
