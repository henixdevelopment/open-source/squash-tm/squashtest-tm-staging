import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AbstractAdministrationCreationDialogDirective } from '../../../../../components/abstract-administration-creation-dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  DialogReference,
  DisplayOption,
  FieldValidationError,
  MilestoneStatus,
  NOT_ONLY_SPACES_REGEX,
  RestService,
} from 'sqtm-core';
import { MilestoneDuplicationDialogConfiguration } from './milestone-duplication-dialog-configuration';
import { TranslateService } from '@ngx-translate/core';
import { of } from 'rxjs';

@Component({
  selector: 'sqtm-app-milestone-duplication-dialog',
  templateUrl: './milestone-duplication-dialog.component.html',
  styleUrls: ['./milestone-duplication-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MilestoneDuplicationDialogComponent
  extends AbstractAdministrationCreationDialogDirective
  implements OnInit
{
  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  data: MilestoneDuplicationDialogConfiguration;
  milestoneStatusOptions: DisplayOption[] = [];

  constructor(
    private fb: FormBuilder,
    private translateService: TranslateService,
    dialogReference: DialogReference<MilestoneDuplicationDialogConfiguration>,
    restService: RestService,
    cdr: ChangeDetectorRef,
  ) {
    super(
      `milestones/${dialogReference.data.milestoneId.toString()}/clone`,
      dialogReference,
      restService,
      cdr,
    );
  }

  get textFieldToFocus(): string {
    return 'label';
  }

  ngOnInit() {
    this.getMilestoneStatusOptions();
    this.initializeFormGroup();
  }

  private getMilestoneStatusOptions() {
    const inProgress = MilestoneStatus.IN_PROGRESS;
    const finished = MilestoneStatus.FINISHED;
    this.milestoneStatusOptions.push({
      label: this.translateService.instant(inProgress.i18nKey),
      id: inProgress.id,
    });
    this.milestoneStatusOptions.push({
      label: this.translateService.instant(finished.i18nKey),
      id: finished.id,
    });
  }

  protected getRequestPayload() {
    return of({
      label: this.getFormControlValue('label'),
      status: this.getFormControlValue('status'),
      endDate: this.getFormControlValue('endDate'),
      description: this.getFormControlValue('description'),
      bindToRequirements: this.getFormControlValue('bindToRequirements'),
      bindToTestCases: this.getFormControlValue('bindToTestCases'),
    });
  }

  protected doResetForm() {
    // NOOP
  }

  confirm() {
    this.addEntity();
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      label: this.fb.control('', [
        Validators.required,
        Validators.pattern(NOT_ONLY_SPACES_REGEX),
        Validators.maxLength(255),
      ]),
      status: this.fb.control('IN_PROGRESS', [Validators.required]),
      endDate: this.fb.control('', [Validators.required]),
      description: this.fb.control(''),
      bindToRequirements: this.fb.control(true),
      bindToTestCases: this.fb.control(true),
    });
  }
}
