import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import { AbstractCellRendererComponent, DialogService, GridService, RestService } from 'sqtm-core';
import { Subject, switchMap } from 'rxjs';
import { filter, take } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-reclone-scm-repository-cell',
  templateUrl: './reclone-scm-repository-cell.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RecloneScmRepositoryCellComponent
  extends AbstractCellRendererComponent
  implements OnDestroy
{
  unsub$ = new Subject<void>();

  constructor(
    public grid: GridService,
    cdr: ChangeDetectorRef,
    protected dialogService: DialogService,
    private restService: RestService,
  ) {
    super(grid, cdr);
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  recloneScmRepository() {
    const dialogRef = this.dialogService.openConfirm({
      id: 'reclone-repository',
      titleKey: 'sqtm-core.administration-workspace.servers.scm-servers.clone.reclone.title',
      messageKey: 'sqtm-core.administration-workspace.servers.scm-servers.clone.reclone.message',
      level: 'INFO',
    });

    dialogRef.dialogClosed$
      .pipe(
        take(1),
        filter((confirm) => confirm),
        switchMap(() =>
          this.restService.post([
            'scm-repositories',
            this.row.data.scmRepositoryId,
            'recreate-local-repository',
          ]),
        ),
      )
      .subscribe();
  }
}
