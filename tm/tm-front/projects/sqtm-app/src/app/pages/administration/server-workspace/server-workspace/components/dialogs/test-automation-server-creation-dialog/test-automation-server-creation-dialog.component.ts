import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import {
  AdminReferentialDataService,
  CreationDialogData,
  DialogReference,
  DisplayOption,
  FieldValidationError,
  getTestAutomationServerKindI18nKey,
  RestService,
  NOT_ONLY_SPACES_REGEX,
  TestAutomationServerKind,
} from 'sqtm-core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AbstractAdministrationCreationDialogDirective } from '../../../../../components/abstract-administration-creation-dialog';
import { of } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-test-automation-server-creation-dialog',
  templateUrl: './test-automation-server-creation-dialog.component.html',
  styleUrls: ['./test-automation-server-creation-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestAutomationServerCreationDialogComponent
  extends AbstractAdministrationCreationDialogDirective
  implements OnInit
{
  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  data: CreationDialogData;

  availableKindOptions: DisplayOption[] = [];

  constructor(
    private fb: FormBuilder,
    private translateService: TranslateService,
    dialogReference: DialogReference,
    restService: RestService,
    cdr: ChangeDetectorRef,
    public readonly adminReferentialDataService: AdminReferentialDataService,
  ) {
    super('test-automation-servers/new', dialogReference, restService, cdr);
  }

  get textFieldToFocus(): string {
    return 'name';
  }

  ngOnInit() {
    this.adminReferentialDataService.adminReferentialData$.pipe(take(1)).subscribe((refData) => {
      this.prepareServerKindOptions(refData.availableTestAutomationServerKinds);
      this.initializeFormGroup();
    });
  }

  protected doResetForm() {
    this.resetFormControl('name', '');
    this.resetFormControl('baseUrl', '');
    this.resetFormControl('kind', this.availableKindOptions[0]?.id);
    this.resetFormControl('description', '');
  }

  protected getRequestPayload() {
    return of({
      name: this.getFormControlValue('name'),
      baseUrl: this.getFormControlValue('baseUrl'),
      kind: this.getFormControlValue('kind'),
      description: this.getFormControlValue('description'),
    });
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      name: this.fb.control('', [
        Validators.required,
        Validators.pattern(NOT_ONLY_SPACES_REGEX),
        Validators.maxLength(255),
      ]),
      baseUrl: this.fb.control('', [Validators.required, Validators.maxLength(255)]),
      kind: this.fb.control(this.availableKindOptions[0]?.id, [Validators.required]),
      description: this.fb.control(''),
    });
  }

  private prepareServerKindOptions(availableTestAutomationServerKinds: string[]): void {
    const sortedKinds = [...availableTestAutomationServerKinds].sort(sortServerKinds);
    this.availableKindOptions = sortedKinds.map((kind: TestAutomationServerKind) => ({
      label: getTestAutomationServerKindI18nKey(kind),
      id: kind,
    }));
  }
}

// Sort by order within TestAutomationServerKind enum, unknown values last.
function sortServerKinds(first: string, second: string): number {
  if (first === TestAutomationServerKind.squashOrchestrator) {
    return -1;
  } else if (second === TestAutomationServerKind.squashOrchestrator) {
    return 1;
  }

  const arbitraryBigIndex = 999;
  let firstIndex = Object.values(TestAutomationServerKind).indexOf(
    first as TestAutomationServerKind,
  );
  let secondIndex = Object.values(TestAutomationServerKind).indexOf(
    second as TestAutomationServerKind,
  );

  if (firstIndex < 0) {
    firstIndex = arbitraryBigIndex;
  }

  if (secondIndex < 0) {
    secondIndex = arbitraryBigIndex;
  }

  return firstIndex - secondIndex;
}
