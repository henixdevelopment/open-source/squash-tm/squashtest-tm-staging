import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  Signal,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import {
  AbstractListCellRendererComponent,
  ActionErrorDisplayService,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
  ListPanelItem,
  Profile,
  RestService,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { ConnectedPosition, Overlay } from '@angular/cdk/overlay';
import { TeamViewService } from '../../../services/team-view.service';
import { finalize } from 'rxjs/operators';
import { ProfileService } from '../../../../../profile-workspace/profile-workspace/services/profile.service';
import { AdminTeamViewState } from '../../../states/admin-team-view-state';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sqtm-app-team-authorisation-profile-cell',
  template: ` @if (columnDisplay && row) {
    @if ($componentData(); as componentData) {
      <div
        class="full-width full-height __hover_pointer interactive-container"
        (click)="showProfileList()"
      >
        <span
          #profileName
          class="text-ellipsis m-auto-0"
          nz-tooltip
          [sqtmCoreLabelTooltip]="getCellText(componentData.team.profiles)"
        >
          {{ getCellText(componentData.team.profiles) }}
        </span>
        <ng-template #templatePortalContent>
          <sqtm-core-list-panel
            [selectedItem]="row.data[columnDisplay.id]"
            (itemSelectionChanged)="change($event)"
            [items]="panelItems"
          >
          </sqtm-core-list-panel>
        </ng-template>
      </div>
    }
  }`,
  styleUrls: ['./team-authorisation-profile-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ProfileService],
})
export class TeamAuthorisationProfileCellComponent
  extends AbstractListCellRendererComponent
  implements OnInit
{
  $componentData: Signal<AdminTeamViewState>;
  panelItems: ListPanelItem[] = [];

  @ViewChild('templatePortalContent', { read: TemplateRef })
  templatePortalContent: TemplateRef<any>;

  @ViewChild('profileName', { read: ElementRef })
  profileName: ElementRef;

  constructor(
    public readonly grid: GridService,
    public readonly cdRef: ChangeDetectorRef,
    public readonly translateService: TranslateService,
    public readonly overlay: Overlay,
    public readonly vcr: ViewContainerRef,
    public readonly restService: RestService,
    public readonly teamViewService: TeamViewService,
    public readonly actionErrorDisplayService: ActionErrorDisplayService,
    private readonly profileService: ProfileService,
  ) {
    super(grid, cdRef, overlay, vcr, translateService, restService, actionErrorDisplayService);
    this.$componentData = toSignal(this.teamViewService.componentData$);
  }

  ngOnInit() {
    this.initialisePanelItems();
  }

  getCellText(profiles: Profile[]): string {
    if (profiles == null) {
      return '';
    }

    const permissionGroup = this.row.data[GridColumnId.permissionGroup] as Profile;

    return this.profileService.getProfileName(permissionGroup);
  }

  change(newValue: any) {
    this.grid.beginAsyncOperation();
    this.teamViewService
      .setTeamAuthorisation([this.row.data[GridColumnId.projectId]], newValue)
      .pipe(finalize(() => this.grid.completeAsyncOperation()))
      .subscribe();
    this.close();
  }

  canEdit(): boolean {
    return true;
  }

  showProfileList() {
    if (this.canEdit()) {
      this.showList(this.profileName, this.templatePortalContent, LIST_POSITIONS);
    }
  }

  private initialisePanelItems(): void {
    const profiles = this.$componentData().team.profiles;
    const options = this.profileService.retrieveProfilesAsDisplayOptions(profiles);

    // Sort options by locale label
    options.sort((a, b) => {
      return a.label.localeCompare(b.label);
    });

    this.panelItems = [...options];
  }
}

export function teamAuthorisationProfileColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(TeamAuthorisationProfileCellComponent)
    .withI18nKey('sqtm-core.administration-workspace.views.project.permissions.profile.label');
}

const LIST_POSITIONS: ConnectedPosition[] = [
  {
    originX: 'start',
    overlayX: 'start',
    originY: 'bottom',
    overlayY: 'top',
    offsetX: -10,
    offsetY: 6,
  },
  {
    originX: 'start',
    overlayX: 'start',
    originY: 'top',
    overlayY: 'bottom',
    offsetX: -10,
    offsetY: -6,
  },
];
