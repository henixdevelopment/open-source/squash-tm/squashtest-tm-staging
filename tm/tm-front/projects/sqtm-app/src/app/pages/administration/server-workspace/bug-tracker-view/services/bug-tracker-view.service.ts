import { Injectable } from '@angular/core';
import {
  AdminBugTrackerModel,
  AdminReferentialDataService,
  AttachmentService,
  AuthConfiguration,
  AuthenticatedUser,
  AuthenticationPolicy,
  AuthenticationProtocol,
  Credentials,
  DateFormatUtils,
  EntityViewAttachmentHelperService,
  GenericEntityViewService,
  isBasicAuthCredentials,
  isOAuth2Credentials,
  isTokenAuthCredentials,
  OAuth2Credentials,
  RestService,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import {
  AdminBugTrackerState,
  AdminBugTrackerViewState,
  provideInitialAdminBugTrackerView,
} from '../states/admin-bug-tracker-view-state';
import { Observable, throwError } from 'rxjs';
import { catchError, map, switchMap, take, tap, withLatestFrom } from 'rxjs/operators';
import { DefaultOAuth2FormValues } from '../../components/auth-protocol-form/auth-protocol-form.component';

@Injectable()
export class BugTrackerViewService extends GenericEntityViewService<
  AdminBugTrackerState,
  'bugTracker'
> {
  constructor(
    protected restService: RestService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
    private adminReferentialDataService: AdminReferentialDataService,
  ) {
    super(restService, attachmentService, translateService, attachmentHelper);
  }

  public getInitialState(): AdminBugTrackerViewState {
    return provideInitialAdminBugTrackerView();
  }

  protected getRootUrl(_initialState?): string {
    return 'bugtracker';
  }

  load(bugTrackerId: number) {
    return this.restService
      .getWithoutErrorHandling<AdminBugTrackerModel>(['bugtracker-view', bugTrackerId.toString()])
      .pipe(
        take(1),
        map((bugtracker) => this.initializeBugTracker(bugtracker)),
        catchError((err) => {
          this.notifyEntityNotFound(err);
          return throwError(() => err);
        }),
      );
  }

  private initializeBugTracker(bugTracker: AdminBugTrackerModel): void {
    bugTracker.bugTrackerKinds?.sort((a, b) => a.localeCompare(b));
    const btState: AdminBugTrackerState = {
      ...bugTracker,
      attachmentList: { id: null, attachments: null },
      createdOn: DateFormatUtils.createDateFromIsoString(bugTracker.createdOn),
      lastModifiedOn: DateFormatUtils.createDateFromIsoString(bugTracker.lastModifiedOn),
    };
    this.initializeEntityState(btState);
  }

  setAuthenticationConfiguration(authConf: AuthConfiguration): Observable<any> {
    return this.componentData$.pipe(
      take(1),
      switchMap((data: AdminBugTrackerViewState) =>
        this.setAuthConfigurationServerSide(data, authConf),
      ),
      withLatestFrom(this.componentData$),
      map(([, state]: [any, AdminBugTrackerViewState]) => ({
        ...state,
        bugTracker: {
          ...state.bugTracker,
          authConfiguration: authConf,
        },
      })),
      tap((nextState: AdminBugTrackerViewState) => this.store.commit(nextState)),
    );
  }

  private setAuthConfigurationServerSide(data: AdminBugTrackerViewState, authConf) {
    return this.restService.post(
      [this.getRootUrl(), data.bugTracker.id.toString(), 'auth-protocol', 'configuration'],
      authConf,
    );
  }

  setAuthPolicy(authPolicy: AuthenticationPolicy): Observable<any> {
    return this.componentData$.pipe(
      take(1),
      switchMap((data: AdminBugTrackerViewState) =>
        this.restService.post([this.getRootUrl(), data.bugTracker.id.toString(), 'auth-policy'], {
          authPolicy,
        }),
      ),
      withLatestFrom(this.componentData$),
      map(([, state]: [any, AdminBugTrackerViewState]) => ({
        ...state,
        bugTracker: {
          ...state.bugTracker,
          authPolicy: authPolicy,
        },
      })),
      tap((nextState: AdminBugTrackerViewState) => this.store.commit(nextState)),
    );
  }

  setCredentials(credentials: Credentials): Observable<any> {
    return this.doSetCredentials(this.buildCredentialsPayload(credentials));
  }

  deleteBugTrackerCredentials(bugTrackerId: number): Observable<any> {
    return this.restService
      .delete([this.getRootUrl(), bugTrackerId.toString(), 'credentials'])
      .pipe(
        withLatestFrom(this.state$),
        map(([, state]: [any, AdminBugTrackerViewState]) => ({
          ...state,
          bugTracker: {
            ...state.bugTracker,
            credentials: buildEmptyCredentials(),
          },
        })),
        tap((newState) => this.commit(newState)),
      );
  }

  private doSetCredentials(credentials: Credentials): Observable<any> {
    return this.componentData$.pipe(
      take(1),
      switchMap((data: AdminBugTrackerViewState) =>
        this.restService.post(
          [this.getRootUrl(), data.bugTracker.id.toString(), 'credentials'],
          credentials,
        ),
      ),
      withLatestFrom(this.componentData$),
      map(([, state]: [any, AdminBugTrackerViewState]) => {
        credentials.registered = true;
        return {
          ...state,
          bugTracker: {
            ...state.bugTracker,
            credentials,
          },
        };
      }),
      tap((nextState: AdminBugTrackerViewState) => this.store.commit(nextState)),
    );
  }

  setAuthenticationProtocol(authProtocol: AuthenticationProtocol): Observable<any> {
    return this.componentData$.pipe(
      take(1),
      switchMap((data: AdminBugTrackerViewState) =>
        this.restService.post([this.getRootUrl(), data.bugTracker.id.toString(), 'auth-protocol'], {
          authProtocol,
        }),
      ),
      withLatestFrom(this.componentData$),
      map(([, state]: [any, AdminBugTrackerViewState]) => ({
        ...state,
        bugTracker: {
          ...state.bugTracker,
          authProtocol: authProtocol,
        },
      })),
      tap((nextState: AdminBugTrackerViewState) => this.store.commit(nextState)),
      tap((state) => this.requireExternalUpdate(state.bugTracker.id, 'authProtocol', authProtocol)),
    );
  }

  getCurrentUser(): Observable<AuthenticatedUser> {
    return this.adminReferentialDataService.authenticatedUser$;
  }

  openOAuthDialog(bugTrackerId: number, protocol: string): Window {
    const oauthUrl =
      this.restService.backendRootUrl + `servers/${bugTrackerId}/authentication/${protocol}`;
    const dialogConf = 'height=690, width=810, resizable, scrollbars, dialog, alwaysRaised';
    return window.open(oauthUrl, `${protocol} authorizations`, dialogConf);
  }

  askOauth2TokenToBackend(bugTrackerId: number, code: string): Observable<void> {
    return this.restService.post(
      ['bugtracker', bugTrackerId.toString(), 'authentication/oauth2/token'],
      {},
      { params: { code: code } },
    );
  }

  updateStateWithSuccessfulOauthTokenRegistration(): Observable<any> {
    return this.componentData$.pipe(
      take(1),
      map((state) => this.getUpdatedStateWithRegisteredCredentials(state)),
      tap((nextState: AdminBugTrackerViewState) => this.store.commit(nextState)),
    );
  }

  private getUpdatedStateWithRegisteredCredentials(
    state: AdminBugTrackerViewState,
  ): AdminBugTrackerViewState {
    const registeredCreds: OAuth2Credentials = {
      accessToken: '',
      implementedProtocol: AuthenticationProtocol.OAUTH_2,
      type: AuthenticationProtocol.OAUTH_2,
      registered: true,
    };

    return {
      ...state,
      bugTracker: {
        ...state.bugTracker,
        credentials: registeredCreds,
      },
    };
  }

  getDefaultOAuth2FormValues(bugTrackerId: number): Observable<DefaultOAuth2FormValues> {
    return this.restService.get<DefaultOAuth2FormValues>([
      'bugtracker-view',
      bugTrackerId.toString(),
      'oauth2',
      'default-form-values',
    ]);
  }

  changeBugTrackerKind(bugTrackerKind: string) {
    return this.componentData$.pipe(
      take(1),
      switchMap((data: AdminBugTrackerViewState) =>
        this.restService.post([this.getRootUrl(), data.bugTracker.id.toString(), 'kind'], {
          kind: bugTrackerKind,
        }),
      ),
      withLatestFrom(this.componentData$),
      tap(([, state]: [any, AdminBugTrackerViewState]) =>
        this.requireExternalUpdate(state.bugTracker.id, 'kind', bugTrackerKind),
      ),
      switchMap(([, state]: [any, AdminBugTrackerViewState]) => this.load(state.bugTracker.id)),
      switchMap(() => this.adminReferentialDataService.refresh()),
    );
  }

  setCacheCredentials(credentials: Credentials): Observable<unknown> {
    return this.doSetCacheCredentials(this.buildCredentialsPayload(credentials));
  }

  private doSetCacheCredentials(credentials: Credentials): Observable<unknown> {
    const bugTrackerId = this.getSnapshot().bugTracker.id;

    return this.restService
      .post([this.getRootUrl(), bugTrackerId.toString(), 'cache-credentials'], credentials)
      .pipe(
        withLatestFrom(this.componentData$),
        map(([, state]: [any, AdminBugTrackerViewState]) => {
          return {
            ...state,
            bugTracker: {
              ...state.bugTracker,
              reportingCacheCredentials: {
                ...credentials,
                registered: true,
              },
            },
          } satisfies AdminBugTrackerViewState;
        }),
        tap((nextState: AdminBugTrackerViewState) => this.store.commit(nextState)),
      );
  }

  private buildCredentialsPayload(credentials: Credentials): Credentials {
    if (isBasicAuthCredentials(credentials)) {
      return {
        implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
        type: AuthenticationProtocol.BASIC_AUTH,
        username: credentials.username,
        password: credentials.password,
      };
    } else if (isTokenAuthCredentials(credentials)) {
      return {
        implementedProtocol: AuthenticationProtocol.TOKEN_AUTH,
        type: AuthenticationProtocol.TOKEN_AUTH,
        token: credentials.token,
      };
    } else if (isOAuth2Credentials(credentials)) {
      return {
        implementedProtocol: AuthenticationProtocol.OAUTH_2,
        type: AuthenticationProtocol.OAUTH_2,
        accessToken: credentials.accessToken,
        refreshToken: credentials.refreshToken,
      };
    }
  }

  deleteCacheCredentials(bugTrackerId: number) {
    return this.restService
      .delete([this.getRootUrl(), bugTrackerId.toString(), 'cache-credentials'])
      .pipe(
        withLatestFrom(this.state$),
        map(
          ([, state]: [any, AdminBugTrackerViewState]) =>
            ({
              ...state,
              bugTracker: {
                ...state.bugTracker,
                reportingCacheCredentials: buildEmptyCredentials(),
              },
            }) satisfies AdminBugTrackerViewState,
        ),
        tap((newState) => this.commit(newState)),
      );
  }
}

function buildEmptyCredentials(): Credentials {
  return {
    implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
    type: AuthenticationProtocol.BASIC_AUTH,
    registered: false,
    username: '',
    password: '',
  };
}
