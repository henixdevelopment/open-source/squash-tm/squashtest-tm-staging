import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { SystemViewState } from '../../states/system-view.state';
import { SystemViewService } from '../../services/system-view.service';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  GenericEntityViewService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {
  editableReqReportTemplateGridDefinition,
  SYSTEM_WS_EDITABLE_REQ_REPORT_TEMPLATE_GRID,
  SYSTEM_WS_EDITABLE_REQ_REPORT_TEMPLATE_GRID_CONFIG,
} from '../../components/grids/editable-req-report-template-grid-panel/editable-req-report-template-grid-component-panel.component';
import {
  editableTCReportTemplateGridDefinition,
  SYSTEM_WS_EDITABLE_TC_REPORT_TEMPLATE_GRID,
  SYSTEM_WS_EDITABLE_TC_REPORT_TEMPLATE_GRID_CONFIG,
} from '../../components/grids/editable-tc-report-template-grid-panel/editable-tc-report-template-grid-component-panel.component';
import {
  campaignReportTemplateGridDefinition,
  SYSTEM_WS_CAMPAIGN_REPORT_TEMPLATE_GRID,
  SYSTEM_WS_CAMPAIGN_REPORT_TEMPLATE_GRID_CONFIG,
} from '../../components/grids/campaign-report-template-grid-panel/campaign-report-template-grid-component-panel.component';
import {
  iterationReportTemplateGridDefinition,
  SYSTEM_WS_ITERATION_REPORT_TEMPLATE_GRID,
  SYSTEM_WS_ITERATION_REPORT_TEMPLATE_GRID_CONFIG,
} from '../../components/grids/iteration-report-template-grid-panel/iteration-report-template-grid-component-panel.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-system-workspace',
  templateUrl: './system-workspace.component.html',
  styleUrls: ['./system-workspace.component.less'],
  providers: [
    {
      provide: SystemViewService,
      useClass: SystemViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: SystemViewService,
    },
    {
      provide: SYSTEM_WS_EDITABLE_REQ_REPORT_TEMPLATE_GRID_CONFIG,
      useFactory: editableReqReportTemplateGridDefinition,
      deps: [TranslateService],
    },
    {
      provide: SYSTEM_WS_EDITABLE_REQ_REPORT_TEMPLATE_GRID,
      useFactory: gridServiceFactory,
      deps: [
        RestService,
        SYSTEM_WS_EDITABLE_REQ_REPORT_TEMPLATE_GRID_CONFIG,
        ReferentialDataService,
      ],
    },
    {
      provide: SYSTEM_WS_EDITABLE_TC_REPORT_TEMPLATE_GRID_CONFIG,
      useFactory: editableTCReportTemplateGridDefinition,
      deps: [TranslateService],
    },
    {
      provide: SYSTEM_WS_EDITABLE_TC_REPORT_TEMPLATE_GRID,
      useFactory: gridServiceFactory,
      deps: [
        RestService,
        SYSTEM_WS_EDITABLE_TC_REPORT_TEMPLATE_GRID_CONFIG,
        ReferentialDataService,
      ],
    },
    {
      provide: SYSTEM_WS_CAMPAIGN_REPORT_TEMPLATE_GRID_CONFIG,
      useFactory: campaignReportTemplateGridDefinition,
      deps: [TranslateService],
    },
    {
      provide: SYSTEM_WS_CAMPAIGN_REPORT_TEMPLATE_GRID,
      useFactory: gridServiceFactory,
      deps: [RestService, SYSTEM_WS_CAMPAIGN_REPORT_TEMPLATE_GRID_CONFIG, ReferentialDataService],
    },
    {
      provide: SYSTEM_WS_ITERATION_REPORT_TEMPLATE_GRID_CONFIG,
      useFactory: iterationReportTemplateGridDefinition,
      deps: [TranslateService],
    },
    {
      provide: SYSTEM_WS_ITERATION_REPORT_TEMPLATE_GRID,
      useFactory: gridServiceFactory,
      deps: [RestService, SYSTEM_WS_ITERATION_REPORT_TEMPLATE_GRID_CONFIG, ReferentialDataService],
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SystemWorkspaceComponent implements OnInit {
  componentData$: Observable<SystemViewState>;
  workspaceName = 'administration-workspace-system';
  titleKey = 'sqtm-core.administration-workspace.system.title';
  authenticatedUser$: Observable<AuthenticatedUser>;

  constructor(
    private readonly systemViewService: SystemViewService,
    public readonly adminReferentialDataService: AdminReferentialDataService,
  ) {
    adminReferentialDataService.refresh().subscribe();
    this.authenticatedUser$ = adminReferentialDataService.authenticatedUser$;
  }

  ngOnInit(): void {
    this.systemViewService.load();
  }
}
