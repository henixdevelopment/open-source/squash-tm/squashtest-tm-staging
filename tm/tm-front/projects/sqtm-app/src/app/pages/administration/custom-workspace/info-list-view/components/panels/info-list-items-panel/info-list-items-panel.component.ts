import {
  ChangeDetectionStrategy,
  Component,
  InjectionToken,
  OnDestroy,
  OnInit,
  ViewContainerRef,
} from '@angular/core';
import { InfoListViewService } from '../../../services/info-list-view.service';
import {
  ActionErrorDisplayService,
  DataRow,
  DefaultGridDraggedContentComponent,
  deleteColumn,
  DialogService,
  Extendable,
  Fixed,
  GenericDataRow,
  GridColumnId,
  GridDefinition,
  GridService,
  indexColumn,
  smallGrid,
  StyleDefinitionBuilder,
} from 'sqtm-core';
import {
  catchError,
  concatMap,
  filter,
  finalize,
  map,
  switchMap,
  take,
  takeUntil,
  tap,
} from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { AdminInfoListViewState } from '../../../states/admin-info-list-view-state';
import { infoListItemDefaultColumn } from '../../cell-renderers/info-list-item-default-cell/info-list-item-default-cell.component';
import { RemoveInfoListItemCellComponent } from '../../cell-renderers/remove-info-list-item-cell/remove-info-list-item-cell.component';
import { infoListItemLabelColumn } from '../../cell-renderers/info-list-item-label-cell/info-list-item-label-cell.component';
import { infoListItemCodeColumn } from '../../cell-renderers/info-list-item-code-cell/info-list-item-code-cell.component';
import { infoListItemColorColumn } from '../../cell-renderers/info-list-item-color-cell/info-list-item-color-cell.component';
import { infoListItemIconColumn } from '../../cell-renderers/info-list-item-icon-cell/info-list-item-icon-cell.component';
import { AddInfoListItemDialogComponent } from '../../dialogs/add-info-list-item-dialog/add-info-list-item-dialog.component';
import { DummyPermissions } from '../../../../../services/dummy-permissions';

export const INFO_LIST_ITEMS_TABLE_CONF = new InjectionToken('INFO_LIST_ITEMS_TABLE_CONF');
export const INFO_LIST_ITEMS_TABLE = new InjectionToken('INFO_LIST_ITEMS_TABLE');

function infoListOptionDataRowFactory(literals: Partial<DataRow>[]): GenericDataRow[] {
  return literals.map((literal) => {
    const row = new GenericDataRow();
    row.data = literal.data;
    row.id = literal.data.id;
    row.simplePermissions = new DummyPermissions();

    // This is a way to set the DefaultGridDraggedContent text.
    row.data['NAME'] = literal.data.label;
    return row;
  });
}

export function infoListItemsTableDefinition(): GridDefinition {
  return smallGrid('infoListItems')
    .withColumns([
      indexColumn().enableDnd().withViewport('leftViewport'),
      infoListItemLabelColumn(GridColumnId.label)
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Extendable(100, 0.6))
        .disableSort(),
      infoListItemCodeColumn(GridColumnId.code)
        .withI18nKey('sqtm-core.generic.label.code')
        .changeWidthCalculationStrategy(new Extendable(100, 0.4))
        .disableSort(),
      infoListItemIconColumn(GridColumnId.iconName)
        .withI18nKey('sqtm-core.entity.info-list-item.icon.label')
        .changeWidthCalculationStrategy(new Fixed(75))
        .withHeaderPosition('center')
        .disableSort(),
      infoListItemColorColumn(GridColumnId.colour)
        .withI18nKey('sqtm-core.entity.generic.color.label')
        .changeWidthCalculationStrategy(new Fixed(100))
        .withHeaderPosition('center')
        .disableSort(),
      infoListItemDefaultColumn(GridColumnId.isDefault)
        .withI18nKey('sqtm-core.generic.label.default')
        .changeWidthCalculationStrategy(new Fixed(70))
        .withHeaderPosition('center')
        .disableSort(),
      deleteColumn(RemoveInfoListItemCellComponent),
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .enableInternalDrop()
    .enableDrag()
    .withDraggedContentRenderer(DefaultGridDraggedContentComponent)
    .withRowConverter(infoListOptionDataRowFactory)
    .build();
}

@Component({
  selector: 'sqtm-app-info-list-items-panel',
  template: ` <sqtm-core-grid class="sqtm-core-prevent-selection-in-grid"></sqtm-core-grid> `,
  providers: [
    {
      provide: GridService,
      useExisting: INFO_LIST_ITEMS_TABLE,
    },
  ],
  styleUrls: ['./info-list-items-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InfoListItemsPanelComponent implements OnInit, OnDestroy {
  private componentData$: Observable<AdminInfoListViewState>;
  private unsub$ = new Subject<void>();

  constructor(
    private infoListViewService: InfoListViewService,
    private gridService: GridService,
    private dialogService: DialogService,
    private vcRef: ViewContainerRef,
    private actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    this.componentData$ = this.infoListViewService.componentData$.pipe(takeUntil(this.unsub$));
  }

  ngOnInit(): void {
    this.initializeTable();
  }

  private initializeTable() {
    const listItems = this.componentData$.pipe(
      map((componentData) => componentData.infoList.items),
    );
    this.gridService.connectToDatasource(listItems, 'id');
  }

  removeInfoListItems(): void {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        filter((rows: DataRow[]) => rows.length > 0),
        map((rows: DataRow[]) => rows.map((row) => row.data.id)),
        concatMap((itemIds: number[]) => this.showConfirmDeleteInfoListItemDialog(itemIds)),
        filter(({ confirmDelete }) => confirmDelete),
        tap(() => this.gridService.beginAsyncOperation()),
        switchMap(({ itemIds }) => this.infoListViewService.deleteInfoListItems(itemIds)),
        catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
        finalize(() => this.gridService.completeAsyncOperation()),
      )
      .subscribe();
  }

  private showConfirmDeleteInfoListItemDialog(itemIds): Observable<ConfirmDeleteDialogResult> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey:
        'sqtm-core.administration-workspace.entities-customization.generic.dialog.option.title.delete-many',
      messageKey: 'sqtm-core.administration-workspace.info-lists.dialog.message.option.delete-many',
      level: 'WARNING',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map((confirmDelete) => ({ confirmDelete, itemIds })),
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  openAddItemDialog(): void {
    this.dialogService.openDialog({
      id: 'add-custom-field-option-dialog',
      component: AddInfoListItemDialogComponent,
      viewContainerReference: this.vcRef,
      data: {
        titleKey: 'sqtm-core.administration-workspace.info-lists.dialog.title.add-item',
        addAnotherLabelKey: 'sqtm-core.generic.label.add-another.feminine',
      },
      width: 720,
    });
  }
}

interface ConfirmDeleteDialogResult {
  confirmDelete: boolean;
  itemIds: number[];
}
