import { ChangeDetectionStrategy, Component, Input, ViewChild } from '@angular/core';
import { AdminProjectViewComponentData } from '../../../containers/project-view/project-view.component';
import { AiServer, EditableSelectFieldComponent, Option } from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { ProjectViewService } from '../../../services/project-view.service';

@Component({
  selector: 'sqtm-app-project-ai-server-panel',
  templateUrl: './project-ai-server-panel.component.html',
  styleUrl: './project-ai-server-panel.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectAiServerPanelComponent {
  @Input()
  adminProjectViewComponentData: AdminProjectViewComponentData;

  @ViewChild(EditableSelectFieldComponent)
  aiServerSelectField: EditableSelectFieldComponent;

  private readonly noAiServerOptionId: string = 'default';

  constructor(
    public readonly translateService: TranslateService,
    public readonly projectViewService: ProjectViewService,
  ) {}

  getAiServers(availableAiServers: AiServer[]) {
    const options: Option[] = [];
    const defaultOption = new Option();

    defaultOption.value = this.noAiServerOptionId;
    defaultOption.label = this.translateService.instant(
      'sqtm-core.administration-workspace.views.project.no-associated-ai-server.label',
    );

    options.push(defaultOption);

    const currentAiServerId = this.adminProjectViewComponentData.project.aiServerId;

    if (
      currentAiServerId != null &&
      !availableAiServers.find((aiServer) => aiServer.id === currentAiServerId)
    ) {
      options.push({
        value: currentAiServerId.toString(),
        label: currentAiServerId.toString(),
        hide: true,
      });
    }

    availableAiServers.forEach((aiServer) => {
      const option = new Option();

      option.value = aiServer.id.toString();
      option.label = aiServer.name;
      options.push(option);
    });

    return options;
  }

  getSelectedValue() {
    if (this.adminProjectViewComponentData.project.aiServerId !== null) {
      return this.adminProjectViewComponentData.project.aiServerId.toString();
    } else {
      return this.noAiServerOptionId;
    }
  }

  confirmAiServer(aiServerOption: Option) {
    this.aiServerSelectField.beginAsync();
    const aiServerId = aiServerOption.value === 'default' ? null : Number(aiServerOption.value);
    this.projectViewService.confirmAiServer(aiServerId);
    this.aiServerSelectField.value = aiServerOption.value;
  }
}
