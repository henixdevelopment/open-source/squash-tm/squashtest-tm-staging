import { ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { AdminReferentialDataService, AuthenticatedUser } from 'sqtm-core';
import { Router } from '@angular/router';
import { filter, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-main-custom-workspace',
  templateUrl: './main-custom-workspace.component.html',
  styleUrls: ['./main-custom-workspace.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MainCustomWorkspaceComponent implements OnInit, OnDestroy {
  @Input()
  baseUrl: string;

  authenticatedAdmin$: Observable<AuthenticatedUser>;

  private unsub$ = new Subject<void>();

  constructor(
    private adminReferentialDataService: AdminReferentialDataService,
    private router: Router,
  ) {}

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  ngOnInit(): void {
    this.adminReferentialDataService.refresh().subscribe();

    this.authenticatedAdmin$ = this.adminReferentialDataService.authenticatedUser$.pipe(
      takeUntil(this.unsub$),
      filter((authUser: AuthenticatedUser) => authUser.admin),
    );

    this.adminReferentialDataService.authenticatedUser$
      .pipe(
        takeUntil(this.unsub$),
        filter((authUser: AuthenticatedUser) => !authUser.admin),
      )
      .subscribe(() => this.router.navigate(['home-workspace']));
  }
}
