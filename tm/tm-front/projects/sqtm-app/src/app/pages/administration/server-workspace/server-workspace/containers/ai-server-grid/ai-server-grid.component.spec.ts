import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AiServerGridComponent } from './ai-server-grid.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  ADMIN_WS_AI_SERVERS_TABLE,
  ADMIN_WS_AI_SERVERS_TABLE_CONFIG,
} from '../../../server-workspace.constant';
import { adminProjectTableDefinition } from '../../../../project-workspace/project-workspace/containers/project-grid/project-grid.component';
import { ActivatedRoute, Router } from '@angular/router';
import {
  mockClosableDialogService,
  mockGridService,
  mockRestService,
  mockRouter,
} from '../../../../../../utils/testing-utils/mocks.service';
import {
  DataRow,
  DialogService,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
  WorkspaceWithGridComponent,
} from 'sqtm-core';
import createSpyObj = jasmine.createSpyObj;
import { ServerWorkspaceModule } from '../../../server-workspace.module';
import { of } from 'rxjs';
import { mockMouseEvent } from '../../../../../../utils/testing-utils/test-component-generator';

describe('AiServerGridComponent', () => {
  let component: AiServerGridComponent;
  let fixture: ComponentFixture<AiServerGridComponent>;

  const gridService = mockGridService();
  const dialogMock = mockClosableDialogService();
  const restService = mockRestService();

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [AiServerGridComponent],
      imports: [ServerWorkspaceModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: ADMIN_WS_AI_SERVERS_TABLE_CONFIG,
          useFactory: adminProjectTableDefinition,
        },
        {
          provide: ADMIN_WS_AI_SERVERS_TABLE,
          useFactory: gridServiceFactory,
          deps: [RestService, ADMIN_WS_AI_SERVERS_TABLE_CONFIG, ReferentialDataService],
        },
        {
          provide: GridService,
          useValue: gridService,
        },
        {
          provide: DialogService,
          useValue: dialogMock.service,
        },
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: Router,
          useValue: mockRouter(),
        },
        {
          provide: WorkspaceWithGridComponent,
          useValue: {},
        },
        {
          provide: ActivatedRoute,
          useValue: { snapshot: { paramMap: createSpyObj<Map<any, any>>(['get']) } },
        },
      ],
    });

    fixture = TestBed.createComponent(AiServerGridComponent);
    component = fixture.componentInstance;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AiServerGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    restService.delete.calls.reset();
    dialogMock.resetSubjects();
    dialogMock.resetCalls();
    gridService.refreshData.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create new AiServer', () => {
    component.openAiServerDialog();

    dialogMock.closeDialogsWithResult(true);

    expect(dialogMock.service.openDialog).toHaveBeenCalled();
    expect(gridService.refreshDataAsync).toHaveBeenCalled();
  });

  it('should delete AiServers', () => {
    gridService.selectedRows$ = of([
      { data: { id: 1, synchronisationCount: 0 } } as unknown as DataRow,
    ]);

    component.deleteAiServers(mockMouseEvent());

    dialogMock.closeDialogsWithResult(true);

    expect(dialogMock.service.openDeletionConfirm).toHaveBeenCalled();
    expect(restService.delete).toHaveBeenCalled();
    expect(gridService.refreshData).toHaveBeenCalled();
  });
});
