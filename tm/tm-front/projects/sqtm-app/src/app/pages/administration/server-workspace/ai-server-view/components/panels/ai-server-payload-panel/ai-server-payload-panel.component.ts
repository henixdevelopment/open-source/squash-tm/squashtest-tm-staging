import {
  ChangeDetectionStrategy,
  Component,
  Input,
  signal,
  ViewChild,
  WritableSignal,
} from '@angular/core';
import { AdminAiServerViewState } from '../../../states/admin-ai-server-view-state';
import { AiServerViewService } from '../../../services/ai-server-view.service';
import {
  DialogService,
  EditableTextAreaFieldComponent,
  EditableTextFieldComponent,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-ai-server-payload-panel',
  templateUrl: './ai-server-payload-panel.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AiServerPayloadPanelComponent {
  @Input()
  value: string;

  @Input()
  componentData: AdminAiServerViewState;

  @ViewChild(EditableTextAreaFieldComponent)
  textAreaFieldComponent: EditableTextAreaFieldComponent;

  @ViewChild(EditableTextFieldComponent)
  editableTextFieldComponent: EditableTextFieldComponent;

  $wrongJsonFormat: WritableSignal<boolean> = signal(false);

  constructor(
    public aiServerViewService: AiServerViewService,
    public translateService: TranslateService,
    public dialogService: DialogService,
  ) {}

  updatePayload(newValue: string) {
    if (newValue !== this.componentData.aiServer.payloadTemplate) {
      this.aiServerViewService.savePayload(newValue).subscribe();
    } else {
      this.textAreaFieldComponent.disableEditMode();
    }
  }

  updateJsonPath(jsonPath: string) {
    if (jsonPath !== this.componentData.aiServer.jsonPath) {
      this.aiServerViewService.saveJsonPath(jsonPath, this.componentData.aiServer.id).subscribe({
        error: (error) => {
          if (error.status === 412) {
            this.$wrongJsonFormat.set(true);
          } else {
            this.dialogService.openAlert({
              id: 'generic-error-message',
              level: 'DANGER',
              messageKey: 'sqtm-core.generic.label.exception.message',
            });
          }
        },
      });
    }
    return this.editableTextFieldComponent.disableEditMode();
  }
}
