import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
  MilestoneStatus,
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-milestone-status-cell-renderer',
  template: `
    @if (columnDisplay && row) {
      <div class="full-width full-height flex-column" style="justify-content: center;">
        @if (row.data[columnDisplay.id]) {
          <span class="sqtm-grid-cell-txt-renderer">
            {{ milestoneStatus | translate }}
          </span>
        }
      </div>
    }
  `,
  styleUrls: ['./milestone-status-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MilestoneStatusCellRendererComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }

  get milestoneStatus(): string {
    // This component is used in the milestone workspace grid and in the project view's milestones panel.
    // In the first case, the keys are already translated but in the second, we have untranslated keys
    // coming... Hence this trick to determine if we do need to translate the keys or not.
    const status = this.row.data[this.columnDisplay.id];
    if (isRawStatusKey(status)) {
      return 'sqtm-core.entity.milestone.status.' + status;
    } else {
      return status;
    }
  }
}

function isRawStatusKey(status): boolean {
  return Object.keys(MilestoneStatus).includes(status);
}

export function milestoneStatusColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(MilestoneStatusCellRendererComponent);
}
