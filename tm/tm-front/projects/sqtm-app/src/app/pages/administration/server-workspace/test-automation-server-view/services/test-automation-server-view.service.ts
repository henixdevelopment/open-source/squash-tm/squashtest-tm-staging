import { Injectable } from '@angular/core';
import {
  AdminReferentialDataService,
  AdminTestAutomationServer,
  AttachmentService,
  AuthenticatedUser,
  AuthenticationProtocol,
  Credentials,
  DateFormatUtils,
  EntityViewAttachmentHelperService,
  GenericEntityViewService,
  isBasicAuthCredentials,
  isTokenAuthCredentials,
  RestService,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import {
  AdminTestAutomationServerViewState,
  provideInitialAdminTestAutomationServerView,
} from '../states/admin-test-automation-server-view-state';
import { AdminTestAutomationServerState } from '../states/admin-test-automation-server-state';
import { concatMap, map, switchMap, take, tap, withLatestFrom } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export class TestAutomationServerViewService extends GenericEntityViewService<
  AdminTestAutomationServerState,
  'testAutomationServer'
> {
  constructor(
    protected restService: RestService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
    private adminReferentialDataService: AdminReferentialDataService,
  ) {
    super(restService, attachmentService, translateService, attachmentHelper);
  }

  public getInitialState(): AdminTestAutomationServerViewState {
    return provideInitialAdminTestAutomationServerView();
  }

  protected getRootUrl(): string {
    return 'test-automation-servers';
  }

  load(testAutomationServerId: number) {
    const parts = ['test-automation-server-view', testAutomationServerId.toString()];
    return this.restService.getWithoutErrorHandling<AdminTestAutomationServer>(parts).subscribe({
      next: (testAutomationServer) => this.initializeTestAutomationServer(testAutomationServer),
      error: (err) => this.notifyEntityNotFound(err),
    });
  }

  private initializeTestAutomationServer(testAutomationServer: AdminTestAutomationServer): void {
    const testAutomationServerState: AdminTestAutomationServerState = {
      ...testAutomationServer,
      attachmentList: { id: null, attachments: null },
      createdOn: DateFormatUtils.createDateFromIsoString(testAutomationServer.createdOn),
      lastModifiedOn: DateFormatUtils.createDateFromIsoString(testAutomationServer.lastModifiedOn),
    };
    this.initializeEntityState(testAutomationServerState);
  }

  changeManualSlaveSelection(selected: boolean): void {
    this.store.state$
      .pipe(
        take(1),
        switchMap((state: AdminTestAutomationServerViewState) =>
          this.changeManualSlaveSelectionServerSide(state, selected),
        ),
        withLatestFrom(this.store.state$),
        map(([, state]: [any, AdminTestAutomationServerViewState]) =>
          this.updateStateWithNewManualSlaveSelection(state, selected),
        ),
      )
      .subscribe((state) => {
        this.store.commit(state);
      });
  }

  private changeManualSlaveSelectionServerSide(
    state: AdminTestAutomationServerViewState,
    selected: boolean,
  ) {
    const urlParts = [
      'test-automation-servers',
      state.testAutomationServer.id.toString(),
      'manual-selection',
    ];
    return this.restService.post(urlParts, { manualSlaveSelection: selected });
  }

  private updateStateWithNewManualSlaveSelection(
    state: AdminTestAutomationServerViewState,
    selected: boolean,
  ) {
    return {
      ...state,
      testAutomationServer: {
        ...state.testAutomationServer,
        manualSlaveSelection: selected,
      },
    };
  }

  setCredentials(credentials: Credentials): Observable<any> {
    if (isBasicAuthCredentials(credentials)) {
      return this.setBasicAuthCredentials(credentials.username, credentials.password);
    } else if (isTokenAuthCredentials(credentials)) {
      return this.setTokenAuthCredentials(credentials.token);
    }
  }

  setBasicAuthCredentials(username: string, password: string): Observable<any> {
    return this.doSetCredentials({
      implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
      type: AuthenticationProtocol.BASIC_AUTH,
      username,
      password,
    });
  }

  reloadEnvironmentFromUrlChange(url: string, serverId: any, key: string) {
    return this.componentData$.pipe(
      take(1),
      map((data: AdminTestAutomationServerViewState) => ({
        ...data,
        testAutomationServer: {
          ...data.testAutomationServer,
          id: serverId,
        },
      })),
      tap((data: AdminTestAutomationServerViewState) => {
        // The baseUrl key is present in a field containing a directive that performs an external update.
        // So we don't need it in this table.
        const validKeys: string[] = ['observerUrl', 'eventBusUrl', 'killSwitchUrl'];
        if (validKeys.includes(key)) {
          this.requireExternalUpdate(data.testAutomationServer.id, key as any, url);
        }
      }),
    );
  }

  private setTokenAuthCredentials(token: string): Observable<any> {
    return this.doSetCredentials({
      implementedProtocol: AuthenticationProtocol.TOKEN_AUTH,
      type: AuthenticationProtocol.TOKEN_AUTH,
      token,
    });
  }

  private doSetCredentials(credentials: Credentials): Observable<any> {
    return this.componentData$.pipe(
      take(1),
      switchMap((data: AdminTestAutomationServerViewState) =>
        this.restService.post(
          [this.getRootUrl(), data.testAutomationServer.id.toString(), 'credentials'],
          credentials,
        ),
      ),
      withLatestFrom(this.componentData$),
      map(([, state]: [any, AdminTestAutomationServerViewState]) => {
        credentials.registered = true;
        return {
          ...state,
          testAutomationServer: {
            ...state.testAutomationServer,
            credentials,
          },
        };
      }),
      concatMap((state) => this.updateLastModificationIfNecessary(state)),
      tap((state) =>
        this.requireExternalUpdate(state.testAutomationServer.id, 'credentials', credentials),
      ),
      tap((nextState: AdminTestAutomationServerViewState) => this.store.commit(nextState)),
    );
  }

  setAuthenticationProtocol(authProtocol: AuthenticationProtocol): Observable<any> {
    return this.componentData$.pipe(
      take(1),
      switchMap((data: AdminTestAutomationServerViewState) =>
        this.restService.post(
          [this.getRootUrl(), data.testAutomationServer.id.toString(), 'auth-protocol'],
          { authProtocol },
        ),
      ),
      withLatestFrom(this.componentData$),
      map(([, state]: [any, AdminTestAutomationServerViewState]) => ({
        ...state,
        testAutomationServer: {
          ...state.testAutomationServer,
          authProtocol: authProtocol,
        },
      })),
      tap((nextState: AdminTestAutomationServerViewState) => this.store.commit(nextState)),
      tap((state) =>
        this.requireExternalUpdate(state.testAutomationServer.id, 'authProtocol', authProtocol),
      ),
    );
  }

  getCurrentUser(): Observable<AuthenticatedUser> {
    return this.adminReferentialDataService.authenticatedUser$;
  }

  // This is called after tags were successfully changed in EnvironmentSelectionService so backend request has already been made
  handleDefaultTagsChanged(newTags: string[]): void {
    this.state$
      .pipe(
        take(1),
        map((state) => ({
          ...state,
          testAutomationServer: {
            ...state.testAutomationServer,
            environmentTags: newTags,
          },
        })),
        tap((nextState) => this.store.commit(nextState)),
      )
      .subscribe();
  }

  deleteCredentials(testAutomationServerId: number) {
    return this.restService
      .delete([this.getRootUrl(), testAutomationServerId.toString(), 'credentials'])
      .pipe(
        withLatestFrom(this.store.state$),
        map(([, state]: [any, AdminTestAutomationServerViewState]) =>
          this.updateTestAutomationServerCredentials(state, {
            implementedProtocol: AuthenticationProtocol.TOKEN_AUTH,
            type: AuthenticationProtocol.TOKEN_AUTH,
            registered: false,
            token: null,
          }),
        ),
        concatMap((state) => this.updateLastModificationIfNecessary(state)),
        tap((newState) => this.commit(newState)),
        tap((newState) =>
          this.requireExternalUpdate(
            newState.testAutomationServer.id,
            'credentials',
            newState.testAutomationServer.credentials,
          ),
        ),
      );
  }

  private updateTestAutomationServerCredentials(
    state: AdminTestAutomationServerViewState,
    credentials?: Credentials,
  ): AdminTestAutomationServerViewState {
    return {
      ...state,
      testAutomationServer: {
        ...state.testAutomationServer,
        credentials,
      },
    };
  }

  updateAdditionalConfiguration(configuration: string) {
    this.state$
      .pipe(
        take(1),
        switchMap((state: AdminTestAutomationServerViewState) =>
          this.restService.post(
            [
              this.getRootUrl(),
              state.testAutomationServer.id.toString(),
              'additional-configuration',
            ],
            { additionalConfiguration: configuration },
          ),
        ),
        withLatestFrom(this.store.state$),
        map(([, state]) => this.updateAdditionalConfigurationState(state, configuration)),
        tap((updatedState) => this.store.commit(updatedState)),
      )
      .subscribe();
  }

  private updateAdditionalConfigurationState(
    state: AdminTestAutomationServerViewState,
    additionalConfiguration: string,
  ) {
    return {
      ...state,
      testAutomationServer: {
        ...state.testAutomationServer,
        additionalConfiguration,
      },
    };
  }

  checkAdditionalConfigurationSyntax(additionalConfiguration: string): Observable<boolean> {
    return this.state$.pipe(
      take(1),
      switchMap((state: AdminTestAutomationServerViewState) =>
        this.restService.post<boolean>(
          [
            this.getRootUrl(),
            state.testAutomationServer.id.toString(),
            'check-additional-configuration',
          ],
          { additionalConfiguration },
        ),
      ),
    );
  }
}
