import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import {
  ActionErrorDisplayService,
  AdminReferentialDataService,
  AdminReferentialDataState,
  AuthenticatedUser,
  bigGrid,
  DataRow,
  dateColumn,
  dateTimeColumn,
  deleteColumn,
  DialogService,
  DisplayOption,
  Extendable,
  FilterOperation,
  Fixed,
  GenericDataRow,
  GridColumnId,
  GridDefinition,
  GridService,
  GridWithStatePersistence,
  Identifier,
  indexColumn,
  LicenseInformationMessageProvider,
  LicenseMessagePlacement,
  RestService,
  selectableTextColumn,
  textColumn,
  TextResearchFieldComponent,
  UsersGroup,
  UsersGroupHelpers,
  WorkspaceWithGridComponent,
} from 'sqtm-core';
import { userActiveColumn } from '../../components/cell-renderers/user-active-cell-renderer/user-active-cell-renderer.component';
import { userGroupColumn } from '../../components/cell-renderers/user-group-cell-renderer/user-group-cell-renderer.component';
import { Observable, Subject } from 'rxjs';
import { UserCreationDialogComponent } from '../../components/dialogs/user-creation-dialog/user-creation-dialog.component';
import {
  catchError,
  concatMap,
  filter,
  finalize,
  map,
  switchMap,
  take,
  takeUntil,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import { DeleteUserCellRendererComponent } from '../../components/cell-renderers/delete-user-cell-renderer/delete-user-cell-renderer.component';
import { TranslateService } from '@ngx-translate/core';
import { UserDialogConfiguration } from '../../components/dialogs/user-creation-dialog/user.dialog.configuration';
import { AbstractAdministrationNavigation } from '../../../../components/abstract-administration-navigation';
import { ActivatedRoute, Router } from '@angular/router';
import {
  UserMultiEditDialogComponent,
  UserMultiEditDialogConfiguration,
} from '../../components/dialogs/user-multi-edit-dialog/user-multi-edit-dialog.component';

export function adminUserTableDefinition(): GridDefinition {
  return bigGrid('users')
    .withColumns([
      indexColumn().changeWidthCalculationStrategy(new Fixed(60)).withViewport('leftViewport'),
      userActiveColumn(GridColumnId.active)
        .withI18nKey('sqtm-core.entity.user.active.label')
        .withHeaderPosition('center')
        .changeWidthCalculationStrategy(new Fixed(60)),
      selectableTextColumn(GridColumnId.login)
        .withI18nKey('sqtm-core.entity.user.login.label')
        .changeWidthCalculationStrategy(new Extendable(90, 0.1)),
      userGroupColumn(GridColumnId.userGroup)
        .withI18nKey('sqtm-core.entity.user.group.label')
        .changeWidthCalculationStrategy(new Extendable(90, 0.1)),
      textColumn(GridColumnId.firstName)
        .withI18nKey('sqtm-core.entity.user.first-name.label')
        .changeWidthCalculationStrategy(new Extendable(90, 0.1)),
      textColumn(GridColumnId.lastName)
        .withI18nKey('sqtm-core.entity.user.last-name.label')
        .changeWidthCalculationStrategy(new Extendable(90, 0.1)),
      textColumn(GridColumnId.email)
        .withI18nKey('sqtm-core.entity.user.email.label')
        .changeWidthCalculationStrategy(new Extendable(90, 0.2)),
      dateColumn(GridColumnId.createdOn)
        .withI18nKey('sqtm-core.entity.generic.created-on.masculine')
        .changeWidthCalculationStrategy(new Fixed(100)),
      textColumn(GridColumnId.createdBy)
        .withI18nKey('sqtm-core.entity.generic.created-by.masculine')
        .changeWidthCalculationStrategy(new Fixed(100)),
      textColumn(GridColumnId.habilitationCount)
        .withTitleI18nKey('sqtm-core.entity.user.habilitation-count.long')
        .withI18nKey('sqtm-core.entity.user.habilitation-count.short-dot')
        .changeWidthCalculationStrategy(new Fixed(90))
        .withHeaderPosition('center')
        .withContentPosition('center'),
      textColumn(GridColumnId.teamCount)
        .withTitleI18nKey('sqtm-core.entity.user.team-count.long')
        .withI18nKey('sqtm-core.entity.user.team-count.short')
        .changeWidthCalculationStrategy(new Fixed(90))
        .withHeaderPosition('center')
        .withContentPosition('center'),
      dateTimeColumn(GridColumnId.lastConnectedOn)
        .withI18nKey('sqtm-core.entity.user.last-connection.label')
        .changeWidthCalculationStrategy(new Fixed(150)),
      deleteColumn(DeleteUserCellRendererComponent).withViewport('rightViewport'),
    ])
    .server()
    .withServerUrl(['users'])
    .disableRightToolBar()
    .withRowHeight(35)
    .withRowConverter(activeUserConverter)
    .enableMultipleColumnsFiltering([
      GridColumnId.login,
      GridColumnId.firstName,
      GridColumnId.lastName,
      GridColumnId.email,
      GridColumnId.createdBy,
    ])
    .build();
}

@Component({
  selector: 'sqtm-app-user-grid',
  templateUrl: './user-grid.component.html',
  styleUrls: ['./user-grid.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserGridComponent
  extends AbstractAdministrationNavigation
  implements OnInit, AfterViewInit, OnDestroy
{
  authenticatedUser$: Observable<AuthenticatedUser>;
  unsub$ = new Subject<void>();

  private rowsWithUser$: Observable<{ rowIds: string[]; currentUserIsInSelection: boolean }>;

  protected readonly entityIdPositionInUrl = 3;

  private isUserCreationForbiddenByLicense: boolean;
  private userCreationForbiddenMessage: string;

  @ViewChild(TextResearchFieldComponent)
  searchField: TextResearchFieldComponent;

  constructor(
    public gridService: GridService,
    private restService: RestService,
    private dialogService: DialogService,
    private adminReferentialDataService: AdminReferentialDataService,
    private viewContainerRef: ViewContainerRef,
    private actionErrorDisplayService: ActionErrorDisplayService,
    private translateService: TranslateService,
    private workspaceWithGrid: WorkspaceWithGridComponent,
    private gridWithStatePersistence: GridWithStatePersistence,
    protected route: ActivatedRoute,
    protected router: Router,
  ) {
    super(route, router);
    this.workspaceWithGrid.entityIdPositionInUrl = this.entityIdPositionInUrl;
    this.authenticatedUser$ = adminReferentialDataService.authenticatedUser$;
    this.initializeLicenseLock();
  }

  ngOnInit(): void {
    this.rowsWithUser$ = this.gridService.selectedRowIds$.pipe(
      takeUntil(this.unsub$),
      withLatestFrom(this.authenticatedUser$),
      map(([rowIds, authUser]) => {
        return {
          rowIds: rowIds as string[],
          currentUserIsInSelection: rowIds.includes(authUser.userId.toString()),
        };
      }),
    );
  }

  ngAfterViewInit() {
    this.addFilters();
    this.gridWithStatePersistence.popGridState().subscribe((snapshot) => {
      GridWithStatePersistence.updateMultipleColumnSearchField(snapshot, this.searchField);
      this.gridService.refreshData();
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  addUser() {
    this.restService
      .get<{ usersGroups: UsersGroup[] }>(['users/get-users-groups'])
      .pipe(
        map(({ usersGroups }) => usersGroups),
        map((usersGroupsList: UsersGroup[]) =>
          usersGroupsList.map((group) => ({
            id: group.id,
            label: UsersGroupHelpers.getI18nKey(group),
          })),
        ),
      )
      .subscribe((response: DisplayOption[]) => {
        this.openAddUserDialog(response);
      });
  }

  private addFilters() {
    this.gridService.addFilters([
      {
        id: GridColumnId.login,
        active: false,
        initialValue: { kind: 'single-string-value', value: '' },
        tiedToPerimeter: false,
        operation: FilterOperation.LIKE,
      },
      {
        id: GridColumnId.firstName,
        active: false,
        initialValue: { kind: 'single-string-value', value: '' },
        tiedToPerimeter: false,
        operation: FilterOperation.LIKE,
      },
      {
        id: GridColumnId.lastName,
        active: false,
        initialValue: { kind: 'single-string-value', value: '' },
        tiedToPerimeter: false,
        operation: FilterOperation.LIKE,
      },
      {
        id: GridColumnId.createdBy,
        active: false,
        initialValue: { kind: 'single-string-value', value: '' },
        tiedToPerimeter: false,
        operation: FilterOperation.LIKE,
      },
      {
        id: GridColumnId.email,
        active: false,
        initialValue: { kind: 'single-string-value', value: '' },
        tiedToPerimeter: false,
        operation: FilterOperation.LIKE,
      },
    ]);
  }

  private openAddUserDialog(usersGroups: DisplayOption[]) {
    if (this.isUserCreationForbiddenByLicense) {
      this.openCannotCreateUserAlert();
    } else {
      this.doOpenCreationDialog(usersGroups);
    }
  }

  private doOpenCreationDialog(usersGroups: DisplayOption[]) {
    this.adminReferentialDataService.adminReferentialData$
      .pipe(
        take(1),
        switchMap((adminReferentialData: AdminReferentialDataState) => {
          const dialogReference = this.dialogService.openDialog<UserDialogConfiguration, any>({
            component: UserCreationDialogComponent,
            viewContainerReference: this.viewContainerRef,
            data: {
              titleKey: 'sqtm-core.administration-workspace.users.dialog.title.new-user',
              usersGroups: usersGroups,
              canManageLocalPassword: adminReferentialData.canManageLocalPassword,
            },
            id: 'user-dialog',
            width: 600,
          });

          return dialogReference.dialogResultChanged$.pipe(
            takeUntil(dialogReference.dialogClosed$),
          );
        }),
        filter((result) => result != null),
        concatMap((result: any) => this.gridService.refreshDataAsync().pipe(map(() => result.id))),
        tap((id: string) => super.navigateToNewEntity(id)),
      )
      .subscribe();
  }

  deleteUsers($event: MouseEvent) {
    $event.stopPropagation();
    this.showCurrentUserWarningIfRequired();
    this.rowsWithUser$
      .pipe(
        take(1),
        filter(({ rowIds }) => rowIds.length > 0),
        filter(({ currentUserIsInSelection }) => !currentUserIsInSelection),
        concatMap(({ rowIds }) => this.showConfirmDeleteUserDialog(rowIds)),
        filter(({ confirmDelete }) => confirmDelete),
        tap(() => this.gridService.beginAsyncOperation()),
        concatMap(({ rowIds }) => this.deleteUsersServerSide(rowIds)),
        catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
        finalize(() => this.gridService.completeAsyncOperation()),
      )
      .subscribe(() => this.gridService.refreshData());
  }

  private deleteUsersServerSide(rowIds): Observable<void> {
    return this.restService.delete([`users`, rowIds.join(',')]);
  }

  private showConfirmDeleteUserDialog(
    rowIds,
  ): Observable<{ confirmDelete: boolean; rowIds: string[] }> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.users.dialog.title.delete-many',
      messageKey: 'sqtm-core.administration-workspace.users.dialog.message.delete-many',
      level: 'DANGER',
    });
    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map((confirmDelete) => ({ confirmDelete, rowIds })),
    );
  }

  filterUsers($event: any) {
    this.gridService.applyMultiColumnsFilter($event);
  }

  activateUsers($event: MouseEvent) {
    $event.stopPropagation();

    this.gridService.selectedRowIds$
      .pipe(
        take(1),
        filter((rows: Identifier[]) => rows.length > 0),
        tap(() => this.gridService.beginAsyncOperation()),
        concatMap((rows: Identifier[]) => this.activateUsersServerSide(rows)),
        tap(() => this.gridService.completeAsyncOperation()),
      )
      .subscribe(() => this.gridService.refreshData());
  }

  deactivateUsers($event: MouseEvent) {
    $event.stopPropagation();
    this.showCurrentUserWarningIfRequired();

    this.rowsWithUser$
      .pipe(
        take(1),
        filter(({ rowIds }) => rowIds.length > 0),
        filter(({ currentUserIsInSelection }) => !currentUserIsInSelection),
        tap(() => this.gridService.beginAsyncOperation()),
        concatMap(({ rowIds }) => this.deactivateUsersServerSide(rowIds)),
        tap(() => this.gridService.completeAsyncOperation()),
      )
      .subscribe(() => this.gridService.refreshData());
  }

  private showCurrentUserWarningIfRequired() {
    this.rowsWithUser$
      .pipe(
        take(1),
        filter(({ currentUserIsInSelection }) => currentUserIsInSelection),
      )
      .subscribe(() => this.openAlertForSelectedUserIsCurrentUser());
  }

  private activateUsersServerSide(rowIds: Identifier[]): Observable<any> {
    const pathVariable = rowIds.join(',');
    return this.restService.post([`users`, pathVariable, 'activate']);
  }

  private deactivateUsersServerSide(rowIds: Identifier[]): Observable<any> {
    const pathVariable = rowIds.join(',');
    return this.restService.post([`users`, pathVariable, 'deactivate']);
  }

  private openAlertForSelectedUserIsCurrentUser() {
    this.dialogService.openAlert({
      level: 'DANGER',
      messageKey:
        'sqtm-core.administration-workspace.users.dialog.message.deactivate-current-user-in-multiple-selection',
      titleKey: 'sqtm-core.generic.label.error',
    });
  }

  private initializeLicenseLock(): void {
    this.adminReferentialDataService.licenseInformation$
      .pipe(
        take(1),
        map(
          (licenseInfo) =>
            new LicenseInformationMessageProvider(licenseInfo, this.translateService),
        ),
      )
      .subscribe((messageHelper) => {
        this.isUserCreationForbiddenByLicense = !messageHelper.licenseInformation.allowCreateUsers;
        this.userCreationForbiddenMessage = messageHelper.getLongMessage(
          LicenseMessagePlacement.USER_CREATION,
          true,
        );
      });
  }

  private openCannotCreateUserAlert(): void {
    this.dialogService.openAlert(
      {
        messageKey: this.userCreationForbiddenMessage,
        titleKey: 'sqtm-core.license.title',
        id: 'license-information-detail',
        level: null,
      },
      650,
    );
  }

  openMultiEditDialog(): void {
    this.gridService.selectedRowIds$.pipe(take(1)).subscribe((userIds) => {
      this.dialogService
        .openDialog<UserMultiEditDialogConfiguration, any>({
          id: 'user-multi-edit-dialog',
          component: UserMultiEditDialogComponent,
          viewContainerReference: this.viewContainerRef,
          data: {
            id: 'user-multi-edit-dialog',
            titleKey: 'sqtm-core.search.generic.modify.selection',
            userIds,
          },
        })
        .dialogClosed$.pipe(
          take(1),
          filter((result) => result),
        )
        .subscribe(() => this.gridService.refreshData());
    });
  }
}

export function activeUserConverter(literals: Partial<DataRow>[]): DataRow[] {
  return literals.reduce((datarows, literal) => {
    const dataRow = new GenericDataRow();
    Object.assign(dataRow, literal);
    dataRow.disabled = !literal.data[GridColumnId.active];
    datarows.push(dataRow);
    return datarows;
  }, []);
}
