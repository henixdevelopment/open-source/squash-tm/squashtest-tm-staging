import { ChangeDetectionStrategy, Component } from '@angular/core';
import { GridService } from 'sqtm-core';

@Component({
  selector: 'sqtm-app-milestone-view-with-grid',
  templateUrl: './milestone-view-with-grid.component.html',
  styleUrls: ['./milestone-view-with-grid.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MilestoneViewWithGridComponent {
  constructor(public readonly gridService: GridService) {}

  refreshGrid(keepSelectedRows: boolean): void {
    if (keepSelectedRows) {
      this.gridService.refreshDataAndKeepSelectedRows();
    } else {
      this.gridService.refreshData();
    }
  }
}
