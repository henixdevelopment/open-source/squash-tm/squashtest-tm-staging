import {
  AuthenticationPolicy,
  AuthenticationProtocol,
  Credentials,
  ScmRepository,
  SqtmGenericEntityState,
} from 'sqtm-core';

export interface AdminScmServerState extends SqtmGenericEntityState {
  serverId: number;
  name: string;
  url: string;
  kind: string;
  committerMail: string;
  credentialsNotShared: boolean;
  repositories: ScmRepository[];
  authPolicy: AuthenticationPolicy;
  authProtocol: AuthenticationProtocol;
  supportedAuthenticationProtocols: AuthenticationProtocol[];
  credentials?: Credentials;
  createdBy: string;
  createdOn: Date;
  lastModifiedBy?: string;
  lastModifiedOn?: Date;
  description?: string;
}
