import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  Signal,
  ViewChild,
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { AdminUserViewComponentData } from '../../user-view/user-view.component';
import { UserViewService } from '../../../services/user-view.service';
import { map, takeUntil } from 'rxjs/operators';
import {
  USER_AUTHORISATIONS_TABLE,
  USER_AUTHORISATIONS_TABLE_CONF,
  UserAuthorisationsPanelComponent,
  userAuthorisationsTableDefinition,
} from '../../../components/panels/user-authorisations-panel/user-authorisations-panel.component';
import {
  AdminReferentialDataService,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {
  USER_TEAMS_TABLE,
  USER_TEAMS_TABLE_CONF,
  UserTeamsPanelComponent,
  userTeamsTableDefinition,
} from '../../../components/panels/user-teams-panel/user-teams-panel.component';
import { UserLicenseInformationDialogService } from '../../../../../services/user-license-information-dialog.service';
import {
  USER_VIEW_API_TOKEN_TABLE,
  USER_VIEW_API_TOKEN_TABLE_CONF,
  UserApiTokenPanelComponent,
  userViewApiTokenTableDefinition,
} from '../../../components/panels/user-api-token-panel/user-api-token-panel.component';
import { toSignal } from '@angular/core/rxjs-interop';
import { TranslateService } from '@ngx-translate/core';
import {
  ApiTokenPanelServerOperationHandler,
  DELETE_API_TOKEN_URL_INJECTION_TOKEN,
} from '../../../components/panels/user-api-token-panel/api-token-panel-server-operation-handler.service';

@Component({
  selector: 'sqtm-app-user-content',
  templateUrl: './user-content.component.html',
  styleUrls: ['./user-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: USER_AUTHORISATIONS_TABLE_CONF,
      useFactory: userAuthorisationsTableDefinition,
      deps: [TranslateService],
    },
    {
      provide: USER_AUTHORISATIONS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, USER_AUTHORISATIONS_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: USER_TEAMS_TABLE_CONF,
      useFactory: userTeamsTableDefinition,
    },
    {
      provide: USER_TEAMS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, USER_TEAMS_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: USER_VIEW_API_TOKEN_TABLE_CONF,
      useFactory: userViewApiTokenTableDefinition,
      deps: [TranslateService],
    },
    {
      provide: DELETE_API_TOKEN_URL_INJECTION_TOKEN,
      useValue: 'user-view/delete-token',
    },
    {
      provide: USER_VIEW_API_TOKEN_TABLE,
      useFactory: gridServiceFactory,
      deps: [
        RestService,
        USER_VIEW_API_TOKEN_TABLE_CONF,
        ReferentialDataService,
        ApiTokenPanelServerOperationHandler,
      ],
    },
    {
      provide: UserLicenseInformationDialogService,
      useClass: UserLicenseInformationDialogService,
    },
    {
      provide: ApiTokenPanelServerOperationHandler,
      useClass: ApiTokenPanelServerOperationHandler,
    },
  ],
})
export class UserContentComponent implements OnInit, OnDestroy {
  componentData$: Observable<AdminUserViewComponentData>;

  unsub$ = new Subject<void>();

  @ViewChild(UserAuthorisationsPanelComponent)
  private userAuthorisationsPanelComponent;

  @ViewChild(UserTeamsPanelComponent)
  private userTeamsPanelComponent;

  @ViewChild(UserApiTokenPanelComponent)
  private userApiTokenPanelComponent;

  isPremium: boolean;
  public readonly $isTestAutoServerUser: Signal<boolean> = toSignal(
    this.userViewService.componentData$.pipe(map((data) => data.user.usersGroupBinding === 4)),
  );
  constructor(
    public readonly userViewService: UserViewService,
    @Inject(USER_AUTHORISATIONS_TABLE) public readonly authorisationsGridService: GridService,
    @Inject(USER_TEAMS_TABLE) public readonly teamsGridService: GridService,
    @Inject(USER_VIEW_API_TOKEN_TABLE) public readonly apiTokenGridService: GridService,
    public readonly adminReferentialDataService: AdminReferentialDataService,
    private readonly userLicenseInformationDialogService: UserLicenseInformationDialogService,
  ) {}

  ngOnInit(): void {
    this.componentData$ = this.userViewService.componentData$.pipe(takeUntil(this.unsub$));

    this.adminReferentialDataService.isPremiumPluginInstalled$
      .pipe(takeUntil(this.unsub$))
      .subscribe((isPremium) => (this.isPremium = isPremium));
  }

  addUserAuthorisation($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();

    if (!this.userLicenseInformationDialogService.isUserCreationAllowed()) {
      this.userLicenseInformationDialogService.openLicenseDialog();
    } else {
      this.userAuthorisationsPanelComponent.openAddUserAuthorisationDialog();
    }
  }

  deletePermissions($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();

    this.userAuthorisationsPanelComponent.removeAuthorisations();
  }

  associateTeam($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.userTeamsPanelComponent.openAssociateTeamToUserDialog();
  }

  removeAssociatedTeams($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.userTeamsPanelComponent.removeTeamAssociations();
  }

  createApiToken($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.userApiTokenPanelComponent.createApiToken();
  }

  ngOnDestroy(): void {
    this.authorisationsGridService.complete();
    this.teamsGridService.complete();
    this.apiTokenGridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }
}
