import {
  AdminReferentialDataService,
  DialogService,
  DocXReportId,
  GridService,
  RestService,
  ReportTemplateModel,
  smallGrid,
  indexColumn,
  textColumn,
  GridColumnId,
  Extendable,
  deleteColumn,
  Fixed,
  StyleDefinitionBuilder,
  Sort,
} from 'sqtm-core';
import { SystemViewService } from '../../../services/system-view.service';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnDestroy,
  OnInit,
  ViewContainerRef,
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { SystemViewState } from '../../../states/system-view.state';
import { filter, map, take, takeUntil, tap } from 'rxjs/operators';
import { ImportReportTemplateDialogComponent } from '../../dialogs/import-report-template-dialog/import-report-template-dialog.component';
import { reportTemplateDownloadColumn } from '../../cell-renderers/download-report-template/download-report-template.component';
import { DeleteReportTemplateComponent } from '../../cell-renderers/delete-report-template/delete-report-template.component';

@Component({
  selector: 'sqtm-app-abstract-report-template-grid-panel',
  template: ``,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AbstractReportTemplateGridPanelComponent implements OnInit, OnDestroy {
  @Input()
  reportId: DocXReportId;

  componentData$: Observable<SystemViewState>;
  templateModels$: Observable<ReportTemplateModel[]>;
  unsub$ = new Subject<void>();

  constructor(
    public gridService: GridService,
    public systemViewService: SystemViewService,
    public readonly adminReferentialDataService: AdminReferentialDataService,
    protected dialogService: DialogService,
    public vcr: ViewContainerRef,
    public restService: RestService,
  ) {
    this.componentData$ = systemViewService.componentData$;
  }

  ngOnInit(): void {
    this.gridService.refreshData();
    this.initializeTable();
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  addTemplate() {
    this.adminReferentialDataService.loggedAsAdmin$
      .pipe(
        take(1),
        filter((isAdmin: boolean) => isAdmin),
        tap(() => this.openImportTemplateDialog()),
      )
      .subscribe();
  }

  downloadDefaultTemplate() {
    return `${window.location.origin}${this.restService.backendRootUrl}reports/${this.reportId}/download-default-template`;
  }

  private openImportTemplateDialog() {
    this.gridService.beginAsyncOperation();
    const importDialog = this.dialogService.openDialog({
      id: 'import',
      viewContainerReference: this.vcr,
      component: ImportReportTemplateDialogComponent,
      data: { reportId: this.reportId },
      width: 600,
    });

    importDialog.dialogClosed$.pipe(takeUntil(this.unsub$)).subscribe(() => {
      this.gridService.refreshData();
      this.gridService.completeAsyncOperation();
    });
  }

  private initializeTable() {
    this.templateModels$ = this.componentData$.pipe(
      takeUntil(this.unsub$),
      map((state: SystemViewState) => {
        return state.reportTemplateModels.filter(
          (templateModel) => templateModel.reportId === this.reportId,
        );
      }),
    );

    this.gridService.connectToDatasource(this.templateModels$, 'fileName');
  }
}

export function getReportTemplateSmallGrid(gridId: string) {
  return smallGrid(gridId)
    .withColumns([
      indexColumn().withViewport('leftViewport'),
      textColumn(GridColumnId.fileName)
        .changeWidthCalculationStrategy(new Extendable(200, 1))
        .withI18nKey('sqtm-core.administration-workspace.system.report-templates.template-name'),
      reportTemplateDownloadColumn(GridColumnId.id).changeWidthCalculationStrategy(new Fixed(60)),
      deleteColumn(DeleteReportTemplateComponent),
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .withInitialSortedColumns([{ id: GridColumnId.fileName, sort: Sort.ASC }])
    .build();
}
