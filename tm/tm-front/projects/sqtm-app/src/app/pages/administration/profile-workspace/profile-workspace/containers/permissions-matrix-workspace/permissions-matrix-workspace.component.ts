import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { AdminReferentialDataService, AuthenticatedUser } from 'sqtm-core';
import { Observable, Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ProfileService } from '../../services/profile.service';

@Component({
  selector: 'sqtm-app-permissions-matrix-workspace',
  templateUrl: './permissions-matrix-workspace.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: ProfileService,
    },
  ],
})
export class PermissionsMatrixWorkspaceComponent implements OnInit, OnDestroy {
  authenticatedUser$: Observable<AuthenticatedUser>;

  private unsub$ = new Subject<void>();

  constructor(
    public readonly adminReferentialDataService: AdminReferentialDataService,
    protected router: Router,
  ) {}

  ngOnInit(): void {
    this.adminReferentialDataService.refresh().subscribe();

    this.authenticatedUser$ = this.adminReferentialDataService.authenticatedUser$.pipe(
      takeUntil(this.unsub$),
      filter((authUser: AuthenticatedUser) => authUser.admin || authUser.clearanceManager),
    );

    this.adminReferentialDataService.authenticatedUser$
      .pipe(
        takeUntil(this.unsub$),
        filter((authUser: AuthenticatedUser) => !authUser.admin && !authUser.clearanceManager),
      )
      .subscribe(() => this.router.navigate(['home-workspace']));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
