import { GenericEntityViewState, provideInitialGenericViewState } from 'sqtm-core';
import { AdminEnvironmentVariableState } from './admin-environment-variable-state';

export interface AdminEnvironmentVariableViewState
  extends GenericEntityViewState<AdminEnvironmentVariableState, 'environmentVariable'> {
  environmentVariable: AdminEnvironmentVariableState;
}

export function provideInitialAdminEnvironmentVariableView(): Readonly<AdminEnvironmentVariableViewState> {
  return provideInitialGenericViewState<AdminEnvironmentVariableState, 'environmentVariable'>(
    'environmentVariable',
  );
}
