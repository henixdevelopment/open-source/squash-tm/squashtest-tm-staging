import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { SystemViewState } from '../../../states/system-view.state';
import { SystemViewService } from '../../../services/system-view.service';
import { takeUntil } from 'rxjs/operators';
import { AdminReferentialDataService, AdminReferentialDataState, DocXReportId } from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';

const REPORT_TEMPLATE_DOC_URL_EN =
  'https://tm-en.doc.squashtest.com/latest/admin-guide/manage-system/report-templates.html?utm_source=Appli_squash&utm_medium=link';
const REPORT_TEMPLATE_DOC_URL_FR =
  'https://tm-fr.doc.squashtest.com/latest/admin-guide/gestion-systeme/templates-rapports.html?utm_source=Appli_squash&utm_medium=link';

@Component({
  selector: 'sqtm-app-system-view-report-templates',
  templateUrl: './system-view-report-templates.component.html',
  styleUrls: ['./system-view-report-templates.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SystemViewReportTemplatesComponent implements OnDestroy {
  componentData$: Observable<SystemViewState>;
  adminReferentialData$: Observable<AdminReferentialDataState>;
  docXReportIdEnum = DocXReportId;

  unsub$ = new Subject<void>();

  get reportTemplateDocUrl(): string {
    return this.translateService.getBrowserLang() === 'fr'
      ? REPORT_TEMPLATE_DOC_URL_FR
      : REPORT_TEMPLATE_DOC_URL_EN;
  }

  constructor(
    public readonly adminReferentialDataService: AdminReferentialDataService,
    public readonly systemViewService: SystemViewService,
    private readonly translateService: TranslateService,
  ) {
    this.componentData$ = this.systemViewService.componentData$.pipe(takeUntil(this.unsub$));
    this.adminReferentialData$ = this.adminReferentialDataService.adminReferentialData$.pipe(
      takeUntil(this.unsub$),
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  hasReportPlugin(templatePluginIds: string[], reportId: DocXReportId): boolean {
    return templatePluginIds.some((pluginId) => pluginId === reportId);
  }
}
