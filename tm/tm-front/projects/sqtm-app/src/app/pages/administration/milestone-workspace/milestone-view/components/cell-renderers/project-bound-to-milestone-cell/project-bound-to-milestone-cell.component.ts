import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import {
  AbstractListCellRendererComponent,
  ActionErrorDisplayService,
  ColumnDefinitionBuilder,
  DialogService,
  GridColumnId,
  GridService,
  ListPanelItem,
  RestService,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { ConnectedPosition, Overlay } from '@angular/cdk/overlay';
import { MilestoneViewService } from '../../../services/milestone-view.service';
import { filter, finalize, map, switchMap, take, takeUntil, tap } from 'rxjs/operators';
import { AdminMilestoneViewState } from '../../../states/admin-milestone-view-state';
import { Observable } from 'rxjs';

@Component({
  selector: 'sqtm-app-project-bound-to-milestone-cell',
  template: ` @if (columnDisplay && row) {
    <div
      class="full-width full-height"
      [ngClass]="getComponentClasses() | async"
      (click)="showYesNoList()"
    >
      <span
        #projectBoundToMilestoneStatus
        class="m-auto-0"
        nz-tooltip
        [sqtmCoreLabelTooltip]="cellText"
      >
        {{ cellText }}
      </span>
      <ng-template #templatePortalContent>
        <sqtm-core-list-panel
          [selectedItem]="row.data[columnDisplay.id]"
          (itemSelectionChanged)="change($event)"
          [items]="panelItems"
        >
        </sqtm-core-list-panel>
      </ng-template>
    </div>
  }`,
  styleUrls: ['./project-bound-to-milestone-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectBoundToMilestoneCellComponent extends AbstractListCellRendererComponent {
  readonly panelItems: ListPanelItem[] = [];

  @ViewChild('templatePortalContent', { read: TemplateRef })
  templatePortalContent: TemplateRef<any>;

  @ViewChild('projectBoundToMilestoneStatus', { read: ElementRef })
  projectBoundToMilestoneStatus: ElementRef;

  constructor(
    public readonly grid: GridService,
    public readonly cdRef: ChangeDetectorRef,
    public readonly translateService: TranslateService,
    public readonly overlay: Overlay,
    public readonly vcr: ViewContainerRef,
    public readonly restService: RestService,
    public readonly milestoneViewService: MilestoneViewService,
    public readonly actionErrorDisplayService: ActionErrorDisplayService,
    public readonly dialogService: DialogService,
  ) {
    super(grid, cdRef, overlay, vcr, translateService, restService, actionErrorDisplayService);

    this.panelItems = this.getPanelItems();
  }

  get cellText(): string {
    const isBoundToMilestone: boolean = this.row.data[GridColumnId.boundToMilestone];
    const foundItem = this.panelItems.find((item) => item.id === String(isBoundToMilestone));
    return foundItem?.label ?? '';
  }

  private getPanelItems(): ListPanelItem[] {
    return [
      {
        id: 'true',
        label: this.translateService.instant('sqtm-core.generic.label.yes'),
      },
      {
        id: 'false',
        label: this.translateService.instant('sqtm-core.generic.label.no'),
      },
    ];
  }

  getComponentClasses() {
    return this.milestoneViewService.componentData$.pipe(
      take(1),
      map((state: AdminMilestoneViewState) => {
        const classes = [];
        if (state.milestone.range === 'RESTRICTED' && state.milestone.canEdit) {
          classes.push('__hover_pointer interactive-container');
        } else {
          classes.push('basic-container');
        }
        return classes;
      }),
    );
  }

  change(newValue: any) {
    if (newValue === 'true') {
      const projectId = this.row.data[GridColumnId.projectId];
      this.grid.beginAsyncOperation();
      this.milestoneViewService
        .bindProjectsToMilestone([projectId])
        .pipe(finalize(() => this.grid.completeAsyncOperation()))
        .subscribe();
    } else {
      const projectId = this.row.data[GridColumnId.projectId];
      this.showConfirmUnbindProjectFromMilestoneDialog()
        .pipe(
          take(1),
          filter((confirmDelete: boolean) => confirmDelete),
          tap(() => this.grid.beginAsyncOperation()),
          switchMap(() =>
            this.milestoneViewService.unbindProjectsFromMilestoneAndKeepInPerimeter([projectId]),
          ),
          finalize(() => this.grid.completeAsyncOperation()),
        )
        .subscribe();
    }
    this.close();
  }

  private showConfirmUnbindProjectFromMilestoneDialog(): Observable<boolean> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey:
        'sqtm-core.administration-workspace.milestones.dialog.title.unbind-project-from-milestone.unbind-one',
      messageKey:
        'sqtm-core.administration-workspace.milestones.dialog.message.unbind-project-from-milestone.unbind-one',
      level: 'WARNING',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map((confirmDelete) => confirmDelete),
    );
  }

  showYesNoList() {
    this.milestoneViewService.componentData$
      .pipe(
        take(1),
        filter(
          (state: AdminMilestoneViewState) =>
            state.milestone.range === 'RESTRICTED' && state.milestone.canEdit,
        ),
      )
      .subscribe(() => {
        this.showList(
          this.projectBoundToMilestoneStatus,
          this.templatePortalContent,
          LIST_POSITIONS,
        );
      });
  }
}

export function projectBoundToMilestoneEditableBooleanColumn(
  id: GridColumnId,
): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(ProjectBoundToMilestoneCellComponent)
    .withI18nKey('sqtm-core.entity.milestone.bound-to-milestone');
}

const LIST_POSITIONS: ConnectedPosition[] = [
  {
    originX: 'start',
    overlayX: 'start',
    originY: 'bottom',
    overlayY: 'top',
    offsetX: -10,
    offsetY: 6,
  },
  {
    originX: 'start',
    overlayX: 'start',
    originY: 'top',
    overlayY: 'bottom',
    offsetX: -10,
    offsetY: -6,
  },
];
