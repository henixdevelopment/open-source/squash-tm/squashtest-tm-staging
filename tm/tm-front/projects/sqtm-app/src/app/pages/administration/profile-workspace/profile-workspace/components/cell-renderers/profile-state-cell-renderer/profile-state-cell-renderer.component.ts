import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-profile-state-cell-renderer',
  template: ` @if (columnDisplay && row) {
    <div class="full-width full-height flex-column">
      <span class="txt-ellipsis m-auto-0">
        {{ stateKey | translate }}
      </span>
    </div>
  }`,
  styleUrls: ['./profile-state-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileStateCellRendererComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }

  get stateKey(): string {
    return this.row.data[this.columnDisplay.id]
      ? 'sqtm-core.entity.user.active.label'
      : 'sqtm-core.entity.user.inactive.label';
  }
}

export function profileStateColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(ProfileStateCellRendererComponent);
}
