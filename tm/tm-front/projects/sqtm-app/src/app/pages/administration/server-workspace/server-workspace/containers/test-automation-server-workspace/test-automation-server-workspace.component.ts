import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {
  ADMIN_WS_TEST_AUTOMATION_SERVER_TABLE,
  ADMIN_WS_TEST_AUTOMATION_SERVER_TABLE_CONFIG,
} from '../../../server-workspace.constant';
import { adminTestAutomationServersTableDefinition } from '../test-automation-server-grid/test-automation-server-grid.component';

@Component({
  selector: 'sqtm-app-test-automation-server-workspace',
  templateUrl: './test-automation-server-workspace.component.html',
  styleUrls: ['./test-automation-server-workspace.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: ADMIN_WS_TEST_AUTOMATION_SERVER_TABLE_CONFIG,
      useFactory: adminTestAutomationServersTableDefinition,
      deps: [],
    },
    {
      provide: ADMIN_WS_TEST_AUTOMATION_SERVER_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, ADMIN_WS_TEST_AUTOMATION_SERVER_TABLE_CONFIG, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: ADMIN_WS_TEST_AUTOMATION_SERVER_TABLE,
    },
  ],
})
export class TestAutomationServerWorkspaceComponent implements OnInit, OnDestroy {
  authenticatedAdmin$: Observable<AuthenticatedUser>;

  constructor(
    public readonly adminReferentialDataService: AdminReferentialDataService,
    private gridService: GridService,
  ) {}

  ngOnInit(): void {
    this.authenticatedAdmin$ = this.adminReferentialDataService.authenticatedUser$;
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }
}
