import {
  ChangeDetectionStrategy,
  Component,
  InjectionToken,
  OnDestroy,
  OnInit,
  Signal,
  ViewContainerRef,
} from '@angular/core';
import {
  ActionErrorDisplayService,
  AdminReferentialDataService,
  dateTimeColumn,
  deleteColumn,
  DialogService,
  Extendable,
  Fixed,
  grid,
  GridColumnId,
  GridDefinition,
  GridService,
  gridServiceFactory,
  indexColumn,
  Limited,
  Permissions,
  ProjectPermissions,
  ReferentialDataService,
  RestService,
  StyleDefinitionBuilder,
  Sort,
  textColumn,
  DataRow,
} from 'sqtm-core';
import { Observable, Subject, switchMap } from 'rxjs';
import { AdminProjectViewComponentData } from '../../project-view/project-view.component';
import { getProjectViewState, ProjectViewService } from '../../../services/project-view.service';
import { DeleteProjectImportCellComponent } from '../../../components/cell-renderers/delete-project-import-cell/delete-project-import-cell.component';
import { catchError, concatMap, filter, finalize, map, take, takeUntil, tap } from 'rxjs/operators';
import {
  AddPivotFormatImportResponse,
  ImportFromPivotFormatDialogComponent,
} from '../../../components/dialogs/import-from-pivot-format-dialog/import-from-pivot-format-dialog.component';
import { ImportFromXrayDialogComponent } from '../../../../project-import-from-xray/containers/import-from-xray/import-from-xray-dialog.component';
import { createSelector, select } from '@ngrx/store';
import { AdminProjectState } from '../../../state/admin-project-state';
import { importStatusColumn } from '../../../components/cell-renderers/import-status-cell/import-status-cell.component';
import { importTypeColumn } from '../../../components/cell-renderers/import-type-cell/import-type-cell.component';
import { importLogColumn } from '../../../components/cell-renderers/import-error-log-cell/import-log-cell.component';
import { toSignal } from '@angular/core/rxjs-interop';

export const PROJECT_IMPORTS_TABLE_CONF = new InjectionToken('PROJECT_IMPORTS_TABLE_CONF');
export const PROJECT_IMPORTS_TABLE = new InjectionToken('PROJECT_IMPORTS_TABLE');

export function projectImportsTableDefinition(): GridDefinition {
  return grid('project-imports')
    .withColumns([
      indexColumn(),
      textColumn(GridColumnId.name)
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Limited(300)),
      importTypeColumn(GridColumnId.type)
        .withI18nKey('sqtm-core.generic.label.type')
        .changeWidthCalculationStrategy(new Limited(120)),
      textColumn(GridColumnId.createdBy)
        .withI18nKey('sqtm-core.entity.generic.created-by.masculine')
        .changeWidthCalculationStrategy(new Limited(80)),
      dateTimeColumn(GridColumnId.createdOn)
        .withI18nKey('sqtm-core.entity.generic.created-on.masculine')
        .changeWidthCalculationStrategy(new Limited(160)),
      dateTimeColumn(GridColumnId.successfullyImportedOn)
        .withI18nKey(
          'sqtm-core.administration-workspace.views.project.import.successfully-imported-on',
        )
        .changeWidthCalculationStrategy(new Extendable(140, 0.1)),
      importLogColumn(GridColumnId.importLogFilePath)
        .disableHeader()
        .changeWidthCalculationStrategy(new Fixed(40)),
      importStatusColumn(GridColumnId.status)
        .withI18nKey('sqtm-core.generic.label.status')
        .withHeaderPosition('center')
        .withContentPosition('center')
        .changeWidthCalculationStrategy(new Fixed(80)),
      deleteColumn(DeleteProjectImportCellComponent),
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .disableRightToolBar()
    .withRowHeight(35)
    .withInitialSortedColumns([{ id: GridColumnId.createdOn, sort: Sort.DESC }])
    .build();
}
@Component({
  selector: 'sqtm-app-project-imports',
  templateUrl: './project-imports.component.html',
  styleUrl: './project-imports.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: PROJECT_IMPORTS_TABLE_CONF,
      useFactory: projectImportsTableDefinition,
    },
    {
      provide: PROJECT_IMPORTS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, PROJECT_IMPORTS_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: PROJECT_IMPORTS_TABLE,
    },
  ],
})
export class ProjectImportsComponent implements OnInit, OnDestroy {
  componentData$: Observable<AdminProjectViewComponentData>;
  readonly $isLoggedAsAdmin: Signal<boolean>;
  private unsub$ = new Subject<void>();

  constructor(
    public gridService: GridService,
    public readonly adminReferentialDataService: AdminReferentialDataService,
    private vcr: ViewContainerRef,
    private dialogService: DialogService,
    private projectViewService: ProjectViewService,
    private actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    this.$isLoggedAsAdmin = toSignal(this.adminReferentialDataService.loggedAsAdmin$);
  }

  ngOnInit(): void {
    this.componentData$ = this.projectViewService.componentData$;
    this.initializeTable();
  }

  ngOnDestroy(): void {
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  private initializeTable() {
    const existingImports = this.componentData$.pipe(
      takeUntil(this.unsub$),
      select(
        createSelector(
          getProjectViewState,
          (projectState: AdminProjectState) => projectState.existingImports,
        ),
      ),
    );

    this.gridService.connectToDatasource(existingImports, GridColumnId.id);
  }

  openImportFromPivotFormatDialog(files: File[], projectId: number) {
    const dialogReference = this.dialogService.openDialog({
      id: 'import-from-pivot-format',
      component: ImportFromPivotFormatDialogComponent,
      data: {
        files: files,
        projectId: projectId,
      },
      viewContainerReference: this.vcr,
      height: 300,
      width: 600,
    });

    return dialogReference.dialogClosed$
      .pipe(
        take(1),
        filter((response: AddPivotFormatImportResponse): boolean => {
          return response && response.existingImports.length > 0;
        }),
        switchMap((response: AddPivotFormatImportResponse) =>
          this.projectViewService.updateProjectExistingImportsState(response.existingImports),
        ),
      )
      .subscribe(() => this.gridService.refreshData());
  }

  openImportFromXrayDialog(projectId: number) {
    const dialogReference = this.dialogService.openDialog({
      id: 'import-from-xray-format',
      component: ImportFromXrayDialogComponent,
      data: {
        projectId: projectId,
      },
      viewContainerReference: this.vcr,
      width: 600,
    });

    return dialogReference.dialogClosed$
      .pipe(
        take(1),
        filter(
          (response: AddPivotFormatImportResponse): boolean => response.existingImports.length > 0,
        ),
        switchMap((response: AddPivotFormatImportResponse) =>
          this.projectViewService.updateProjectExistingImportsState(response.existingImports),
        ),
      )
      .subscribe(() => this.gridService.refreshData());
  }

  deleteImports() {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        filter((rows: DataRow[]) => rows.length > 0),
        map((rows: DataRow[]) => rows.map((row) => row.data.id)),
        concatMap((importRequestIds: number[]) =>
          this.showConfirmDeleteImportRequests(importRequestIds),
        ),
        filter(({ confirmDelete }) => confirmDelete),
        tap(() => this.gridService.beginAsyncOperation()),
        concatMap(({ importRequestIds }) => {
          const importRequestIdsConcat = importRequestIds.join(',');
          return this.projectViewService.deleteProjectImportRequests(importRequestIdsConcat);
        }),
        catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
        finalize(() => this.gridService.completeAsyncOperation()),
      )
      .subscribe(() => this.gridService.refreshData());
  }

  private showConfirmDeleteImportRequests(
    importRequestIds: number[],
  ): Observable<{ confirmDelete: boolean; importRequestIds: number[] }> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey:
        'sqtm-core.administration-workspace.projects.dialog.title.delete-import-request.delete-many',
      messageKey:
        'sqtm-core.administration-workspace.projects.dialog.message.delete-import-request.delete-many',
      level: 'DANGER',
    });
    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map((confirmDelete) => ({ confirmDelete, importRequestIds })),
    );
  }

  refresh(projectId: string) {
    this.projectViewService.refreshProjectImports(projectId);
  }

  isAdminOrImportManager(permissions: ProjectPermissions): boolean {
    return this.$isLoggedAsAdmin() || permissions.PROJECT?.includes(Permissions.IMPORT);
  }
}
