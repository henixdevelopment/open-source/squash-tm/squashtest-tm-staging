import {
  AuthenticationProtocol,
  BoundEnvironmentVariable,
  Credentials,
  SqtmGenericEntityState,
} from 'sqtm-core';

export interface AdminTestAutomationServerState extends SqtmGenericEntityState {
  id: number;
  name: string;
  baseUrl: string;
  kind: string;
  description: string;
  createdBy: string;
  createdOn: Date;
  lastModifiedBy: string;
  lastModifiedOn: Date;
  manualSlaveSelection: boolean;
  credentials: Credentials;
  supportedAuthenticationProtocols: AuthenticationProtocol[];
  authProtocol: AuthenticationProtocol;
  supportsAutomatedExecutionEnvironments: boolean;
  boundEnvironmentVariables?: BoundEnvironmentVariable[];
  environmentTags: string[];
  observerUrl?: string | null;
  eventBusUrl?: string | null;
  killSwitchUrl?: string | null;
  additionalConfiguration?: string | null;
}
