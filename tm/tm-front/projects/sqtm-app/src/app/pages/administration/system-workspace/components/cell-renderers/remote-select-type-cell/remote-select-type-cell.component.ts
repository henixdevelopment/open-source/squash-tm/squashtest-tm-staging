import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
  RemoteSelectType,
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-remote-select-type-cell',
  templateUrl: './remote-select-type-cell.component.html',
  styleUrls: ['./remote-select-type-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RemoteSelectTypeCellComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }

  getRemoteSelectTypeI18nKey() {
    return `sqtm-core.entity.remote-sync.select-type.${this.row.data[GridColumnId.remoteSelectType]}`;
  }

  isNotIssueType() {
    return this.row.data[GridColumnId.remoteSelectType] !== RemoteSelectType.ISSUE;
  }
}

export function remoteSelectTypeColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(RemoteSelectTypeCellComponent);
}
