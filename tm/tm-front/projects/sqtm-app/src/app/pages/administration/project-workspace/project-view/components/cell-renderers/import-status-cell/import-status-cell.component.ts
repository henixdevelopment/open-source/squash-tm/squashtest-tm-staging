import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
  PivotFormatImportStatus,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-import-status-cell',
  template: `
    @if (row) {
      <div
        class="status-wrapper"
        nz-tooltip
        [nzTooltipTitle]="getTooltipText(row.data[columnDisplay.id])"
      >
        <div class="status" [ngClass]="statusColor"></div>
      </div>
    }
  `,
  styleUrl: './import-status-cell.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImportStatusCellComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdr: ChangeDetectorRef,
    private translateService: TranslateService,
  ) {
    super(grid, cdr);
  }

  get statusColor(): string[] {
    const classes = ['status-indicator'];
    const status: PivotFormatImportStatus = this.row.data[GridColumnId.status];

    switch (status) {
      case PivotFormatImportStatus.FAILURE:
        classes.push('failure');
        break;
      case PivotFormatImportStatus.RUNNING:
        classes.push('running');
        break;
      case PivotFormatImportStatus.SUCCESS:
        classes.push('success');
        break;
      case PivotFormatImportStatus.WARNING:
        classes.push('warning');
        break;
      case PivotFormatImportStatus.PENDING:
        classes.push('pending');
        break;
    }

    return classes;
  }

  getTooltipText(status: PivotFormatImportStatus) {
    return this.translateService.instant(`sqtm-core.entity.pivot-format-import.status.${status}`);
  }
}

export function importStatusColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(ImportStatusCellComponent);
}
