import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import {
  AnchorModule,
  AttachmentModule,
  CellRendererCommonModule,
  DialogModule,
  GridModule,
  NavBarModule,
  UiManagerModule,
  WorkspaceCommonModule,
  WorkspaceLayoutModule,
} from 'sqtm-core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CKEditorModule } from 'ckeditor4-angular';
import { AdminViewHeaderModule } from '../components/admin-view-header/admin-view-header.module';
import { ProfileWorkspaceComponent } from './profile-workspace/containers/profile-workspace/profile-workspace.component';
import { ProfileGridComponent } from './profile-workspace/containers/profile-grid/profile-grid.component';
import { ProfileTypeCellRendererComponent } from './profile-workspace/components/cell-renderers/profile-type-cell-renderer/profile-type-cell-renderer.component';
import { ProfileStateCellRendererComponent } from './profile-workspace/components/cell-renderers/profile-state-cell-renderer/profile-state-cell-renderer.component';
import { DeleteProfileCellRendererComponent } from './profile-workspace/components/cell-renderers/delete-profile-cell-renderer/delete-profile-cell-renderer.component';
import { ProfileContentComponent } from './profile-view/containers/panel-groups/profile-content/profile-content.component';
import { ProfileViewComponent } from './profile-view/containers/profile-view/profile-view.component';
import { ProfileInformationPanelComponent } from './profile-view/components/profile-information-panel/profile-information-panel.component';
import { ProfileCreationDialogComponent } from './profile-workspace/components/dialogs/profile-creation-dialog/profile-creation-dialog.component';
import { ProfilePermissionsPanelComponent } from './profile-view/components/profile-permissions-panel/profile-permissions-panel.component';
import { MainProfileWorkspaceComponent } from './profile-workspace/containers/main-profile-workspace/main-profile-workspace.component';
import { ProfileWorkspaceAnchorsComponent } from './profile-workspace/components/profile-workspace-anchors/profile-workspace-anchors.component';
import { PermissionsMatrixWorkspaceComponent } from './profile-workspace/containers/permissions-matrix-workspace/permissions-matrix-workspace.component';
import { PermissionsMatrixGridComponent } from './profile-workspace/containers/permissions-matrix-grid/permissions-matrix-grid.component';
import { ProfileAuthorizationsPanelComponent } from './profile-view/components/profile-authorizations-panel/profile-authorizations-panel.component';
import { ProfileAuthorizationsPartyTypeCellComponent } from './profile-view/components/cell-renderers/profile-authorizationsparty-type-cell/profile-authorizations-party-type-cell.component';
import { TransferAuthorizationsDialogComponent } from './profile-workspace/components/dialogs/transfer-authorizations-dialog/transfer-authorizations-dialog.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'manage',
    pathMatch: 'full',
  },
  {
    path: '',
    component: MainProfileWorkspaceComponent,
    children: [
      {
        path: 'manage',
        component: ProfileWorkspaceComponent,
        children: [
          {
            path: ':profileId',
            component: ProfileViewComponent,
            children: [
              {
                path: 'content',
                component: ProfileContentComponent,
              },
            ],
          },
        ],
      },
      {
        path: 'permissions',
        component: PermissionsMatrixWorkspaceComponent,
      },
    ],
  },
];

@NgModule({
  declarations: [
    DeleteProfileCellRendererComponent,
    MainProfileWorkspaceComponent,
    PermissionsMatrixGridComponent,
    PermissionsMatrixWorkspaceComponent,
    ProfileAuthorizationsPanelComponent,
    ProfileContentComponent,
    ProfileAuthorizationsPartyTypeCellComponent,
    ProfileCreationDialogComponent,
    ProfileWorkspaceComponent,
    ProfileGridComponent,
    ProfileInformationPanelComponent,
    ProfilePermissionsPanelComponent,
    ProfileStateCellRendererComponent,
    ProfileTypeCellRendererComponent,
    ProfileViewComponent,
    ProfileWorkspaceAnchorsComponent,
    TransferAuthorizationsDialogComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild(),
    NzDropDownModule,
    GridModule,
    WorkspaceLayoutModule,
    NzIconModule,
    NzCheckboxModule,
    NzFormModule,
    NzDividerModule,
    DialogModule,
    ReactiveFormsModule,
    CKEditorModule,
    UiManagerModule,
    AnchorModule,
    NzToolTipModule,
    AttachmentModule,
    NzBadgeModule,
    NzButtonModule,
    NzCollapseModule,
    NzSwitchModule,
    FormsModule,
    NzInputModule,
    NzSpinModule,
    NzSelectModule,
    NzTabsModule,
    CellRendererCommonModule,
    AdminViewHeaderModule,
    NavBarModule,
    WorkspaceCommonModule,
  ],
  providers: [],
})
export class ProfileWorkspaceModule {}
