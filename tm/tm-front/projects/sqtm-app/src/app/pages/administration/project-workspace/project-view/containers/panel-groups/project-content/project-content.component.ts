import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  Signal,
  ViewChild,
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';
import { AdminProjectViewComponentData } from '../../project-view/project-view.component';
import { ProjectViewService } from '../../../services/project-view.service';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  DialogService,
  GridService,
  gridServiceFactory,
  Permissions,
  ProjectPermissions,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {
  ProjectPermissionsPanelComponent,
  projectPermissionTableDefinition,
} from '../../../components/panels/project-permissions-panel/project-permissions-panel.component';
import {
  PROJECT_PERMISSIONS_TABLE,
  PROJECT_PERMISSIONS_TABLE_CONF,
} from '../../../project-view.constant';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sqtm-app-project-content',
  templateUrl: './project-content.component.html',
  styleUrls: ['./project-content.component.less'],
  providers: [
    {
      provide: PROJECT_PERMISSIONS_TABLE_CONF,
      useFactory: projectPermissionTableDefinition,
      deps: [TranslateService],
    },
    {
      provide: PROJECT_PERMISSIONS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, PROJECT_PERMISSIONS_TABLE_CONF, ReferentialDataService],
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectContentComponent implements OnInit, OnDestroy {
  componentData$: Observable<AdminProjectViewComponentData>;

  unsub$ = new Subject<void>();

  readonly $isUltimate: Signal<boolean>;
  readonly $authenticatedUser: Signal<AuthenticatedUser>;

  @ViewChild(ProjectPermissionsPanelComponent)
  private projectPermissionsPanelComponent;

  constructor(
    public projectViewService: ProjectViewService,
    public translateService: TranslateService,
    public dialogService: DialogService,
    public readonly adminReferentialDataService: AdminReferentialDataService,
    @Inject(PROJECT_PERMISSIONS_TABLE) public permissionGrid: GridService,
  ) {
    this.$isUltimate = toSignal(adminReferentialDataService.isUltimateLicenseAvailable$);
    this.$authenticatedUser = toSignal(adminReferentialDataService.authenticatedUser$);
  }

  ngOnInit(): void {
    this.componentData$ = this.projectViewService.componentData$.pipe(takeUntil(this.unsub$));
  }

  openAddPermissionDropdownMenu($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
  }

  addUserPermission($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();

    this.projectPermissionsPanelComponent.openAddUsersPermissionDialog();
  }

  addTeamPermission($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();

    this.projectPermissionsPanelComponent.openAddTeamsPermissionDialog();
  }

  deletePermissions($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();

    this.projectPermissionsPanelComponent.openDeletePermissionsDialog();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  stopPropagation($event: MouseEvent): void {
    $event.stopPropagation();
  }

  isAdminOrClearanceManager(isAdmin: boolean, permissions: ProjectPermissions): boolean {
    return isAdmin || permissions.PROJECT?.includes(Permissions.MANAGE_PROJECT_CLEARANCE);
  }
}
