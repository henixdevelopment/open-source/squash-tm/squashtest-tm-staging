import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MainCustomWorkspaceComponent } from './custom-workspace/containers/main-custom-workspace/main-custom-workspace.component';
import { CustomFieldGridComponent } from './custom-workspace/containers/custom-field-grid/custom-field-grid.component';
import { InfoListGridComponent } from './custom-workspace/containers/info-list-grid/info-list-grid.component';
import { RequirementsLinkGridComponent } from './custom-workspace/containers/requirements-link-grid/requirements-link-grid.component';
import { TranslateModule } from '@ngx-translate/core';
import { CustomAnchorsComponent } from './custom-workspace/components/custom-anchors/custom-anchors.component';
import {
  AnchorModule,
  CellRendererCommonModule,
  DialogModule,
  GridModule,
  NavBarModule,
  UiManagerModule,
  WorkspaceCommonModule,
  WorkspaceLayoutModule,
} from 'sqtm-core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { DeleteCustomFieldComponent } from './custom-workspace/components/cell-renderers/delete-custom-field/delete-custom-field.component';
import { DeleteInfoListComponent } from './custom-workspace/components/cell-renderers/delete-info-list/delete-info-list.component';
import { InputTypeCellRendererComponent } from './custom-workspace/components/cell-renderers/input-type-cell-renderer/input-type-cell-renderer.component';
import { DefaultRequirementsLinksComponent } from './custom-workspace/components/cell-renderers/default-requirements-links/default-requirements-links.component';
import { RequirementsLinksCreationDialogComponent } from './custom-workspace/components/dialogs/requirements-links-creation-dialog/requirements-links-creation-dialog.component';
import { CustomFieldCreationDialogComponent } from './custom-workspace/components/dialogs/custom-field-creation-dialog/custom-field-creation-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CKEditorModule } from 'ckeditor4-angular';
import { InfoListWorkspaceComponent } from './custom-workspace/containers/info-list-workspace/info-list-workspace.component';
import { CustomFieldWorkspaceComponent } from './custom-workspace/containers/custom-field-workspace/custom-field-workspace.component';
import { RequirementsLinkWorkspaceComponent } from './custom-workspace/containers/requirements-link-workspace/requirements-link-workspace.component';
import { DeleteDropdownListOptionComponent } from './custom-workspace/components/cell-renderers/delete-dropdown-list-option/delete-dropdown-list-option.component';
import { CustomFieldViewComponent } from './custom-field-view/containers/custom-field-view/custom-field-view.component';
import { CustomFieldContentComponent } from './custom-field-view/containers/panel-groups/custom-field-content/custom-field-content.component';
import { InfoListViewComponent } from './info-list-view/containers/info-list-view/info-list-view.component';
import { InfoListContentComponent } from './info-list-view/containers/panel-groups/info-list-content/info-list-content.component';
import { InfoListCreationDialogComponent } from './custom-workspace/components/dialogs/info-list-creation-dialog/info-list-creation-dialog.component';
import { DefaultInfoListOptionComponent } from './custom-workspace/components/cell-renderers/default-info-list-option/default-info-list-option.component';
import { CustomFieldOptionsPanelComponent } from './custom-field-view/components/panels/custom-field-options-panel/custom-field-options-panel.component';
import { OptionLabelCellComponent } from './custom-field-view/components/cell-renderers/option-label-cell/option-label-cell.component';
import { OptionCodeCellComponent } from './custom-field-view/components/cell-renderers/option-code-cell/option-code-cell.component';
import { OptionColorCellComponent } from './custom-field-view/components/cell-renderers/option-color-cell/option-color-cell.component';
import { OptionDefaultCellComponent } from './custom-field-view/components/cell-renderers/option-default-cell/option-default-cell.component';
import { RemoveOptionCellComponent } from './custom-field-view/components/cell-renderers/remove-option-cell/remove-option-cell.component';
import { CustomFieldInformationPanelComponent } from './custom-field-view/components/panels/custom-field-information-panel/custom-field-information-panel.component';
import { AddCustomFieldOptionDialogComponent } from './custom-field-view/components/dialogs/add-custom-field-option-dialog/add-custom-field-option-dialog.component';
import { AdminViewHeaderModule } from '../components/admin-view-header/admin-view-header.module';
import { InfoListInformationPanelComponent } from './info-list-view/components/panels/info-list-information-panel/info-list-information-panel.component';
import { InfoListItemsPanelComponent } from './info-list-view/components/panels/info-list-items-panel/info-list-items-panel.component';
import { InfoListItemCodeCellComponent } from './info-list-view/components/cell-renderers/info-list-item-code-cell/info-list-item-code-cell.component';
import { InfoListItemColorCellComponent } from './info-list-view/components/cell-renderers/info-list-item-color-cell/info-list-item-color-cell.component';
import { InfoListItemDefaultCellComponent } from './info-list-view/components/cell-renderers/info-list-item-default-cell/info-list-item-default-cell.component';
import { InfoListItemLabelCellComponent } from './info-list-view/components/cell-renderers/info-list-item-label-cell/info-list-item-label-cell.component';
import { RemoveInfoListItemCellComponent } from './info-list-view/components/cell-renderers/remove-info-list-item-cell/remove-info-list-item-cell.component';
import { InfoListItemIconCellComponent } from './info-list-view/components/cell-renderers/info-list-item-icon-cell/info-list-item-icon-cell.component';
import { AddInfoListItemDialogComponent } from './info-list-view/components/dialogs/add-info-list-item-dialog/add-info-list-item-dialog.component';
import { DropdownListColorPickerComponent } from './custom-workspace/components/cell-renderers/dropdown-list-color-picker/dropdown-list-color-picker.component';
import { DeleteRequirementsLinksComponent } from './custom-workspace/components/cell-renderers/delete-requirements-links/delete-requirements-links.component';
import { DeleteInfoListOptionComponent } from './custom-workspace/components/cell-renderers/delete-info-list-option/delete-info-list-option.component';
import { InfoListColorPickerComponent } from './custom-workspace/components/cell-renderers/info-list-color-picker/info-list-color-picker.component';
import { DefaultDropdownListOptionComponent } from './custom-workspace/components/cell-renderers/default-dropdown-list-option/default-dropdown-list-option.component';
import { InfoListOptionIconComponent } from './custom-workspace/components/cell-renderers/info-list-option-icon/info-list-option-icon.component';
import { EnvironmentVariableWorkspaceComponent } from './custom-workspace/containers/environment-variable-workspace/environment-variable-workspace.component';
import { EnvironmentVariableGridComponent } from './custom-workspace/containers/environment-variable-grid/environment-variable-grid.component';
import { DeleteEnvironmentVariableComponent } from './custom-workspace/components/cell-renderers/delete-environment-variable/delete-environment-variable.component';
import { EnvironmentVariableCreationDialogComponent } from './custom-workspace/components/dialogs/environment-variable-creation-dialog/environment-variable-creation-dialog.component';
import { DeleteEnvironmentVariableOptionComponent } from './custom-workspace/components/cell-renderers/delete-environment-variable-option/delete-environment-variable-option.component';
import { EnvironmentVariableViewComponent } from './environment-variable-view/containers/environment-variable-view/environment-variable-view.component';
import { EnvironmentVariableContentComponent } from './environment-variable-view/containers/panel-groups/environment-variable-content/environment-variable-content.component';
import { EnvironmentVariableInformationPanelComponent } from './environment-variable-view/components/panels/environment-variable-information-panel/environment-variable-information-panel.component';
import { EnvironmentVariableOptionsPanelComponent } from './environment-variable-view/components/panels/environment-variable-options-panel/environment-variable-options-panel.component';
import { EvOptionLabelCellComponent } from './environment-variable-view/components/cell-renderers/ev-option-label-cell/ev-option-label-cell.component';
import { AddEnvironmentVariableOptionDialogComponent } from './environment-variable-view/components/dialogs/add-environment-variable-option-dialog/add-environment-variable-option-dialog.component';
import { EvOptionRemoveCellComponent } from './environment-variable-view/components/cell-renderers/ev-option-remove-cell/ev-option-remove-cell.component';
import { EnvironmentVariableViewDetailComponent } from './environment-variable-view/containers/environment-variable-view-detail/environment-variable-view-detail.component';
import { ProjectsBoundToCufComponent } from './custom-field-view/components/panels/project-bound-to-cuf.component';
import { EditRequirementLinksComponent } from './custom-workspace/components/cell-renderers/edit-requirement-links/edit-requirement-links.component';
import { EditRequirementLinksDialogComponent } from './custom-workspace/components/dialogs/edit-requirement-links-dialog/edit-requirement-links-dialog.component';
import { NzInputModule } from 'ng-zorro-antd/input';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'custom-fields',
    pathMatch: 'full',
  },
  {
    path: '',
    component: MainCustomWorkspaceComponent,
    children: [
      {
        path: 'custom-fields',
        component: CustomFieldWorkspaceComponent,
        children: [
          {
            path: ':customFieldId',
            component: CustomFieldViewComponent,
            children: [
              {
                path: 'content',
                component: CustomFieldContentComponent,
              },
            ],
          },
        ],
      },
      {
        path: 'info-lists',
        component: InfoListWorkspaceComponent,
        children: [
          {
            path: ':infoListId',
            component: InfoListViewComponent,
            children: [
              {
                path: 'content',
                component: InfoListContentComponent,
              },
            ],
          },
        ],
      },
      {
        path: 'requirements-links',
        component: RequirementsLinkWorkspaceComponent,
      },
      {
        path: 'environment-variables',
        component: EnvironmentVariableWorkspaceComponent,
        children: [
          {
            path: ':environmentVariableId',
            component: EnvironmentVariableViewComponent,
            children: [
              {
                path: 'content',
                component: EnvironmentVariableContentComponent,
              },
            ],
          },
        ],
      },
      {
        path: 'environment-variables/detail/:environmentVariableId',
        component: EnvironmentVariableViewDetailComponent,
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'content',
          },
          {
            path: 'content',
            component: EnvironmentVariableContentComponent,
          },
        ],
      },
    ],
  },
];

@NgModule({
  declarations: [
    MainCustomWorkspaceComponent,
    CustomFieldGridComponent,
    InfoListGridComponent,
    RequirementsLinkGridComponent,
    CustomAnchorsComponent,
    CustomFieldCreationDialogComponent,
    EnvironmentVariableCreationDialogComponent,
    RequirementsLinksCreationDialogComponent,
    DeleteCustomFieldComponent,
    DeleteRequirementsLinksComponent,
    DefaultRequirementsLinksComponent,
    DeleteInfoListComponent,
    InputTypeCellRendererComponent,
    InfoListWorkspaceComponent,
    CustomFieldWorkspaceComponent,
    RequirementsLinkWorkspaceComponent,
    DeleteDropdownListOptionComponent,
    DefaultDropdownListOptionComponent,
    DropdownListColorPickerComponent,
    InfoListCreationDialogComponent,
    DefaultInfoListOptionComponent,
    DeleteInfoListOptionComponent,
    InfoListColorPickerComponent,
    InfoListOptionIconComponent,
    CustomFieldViewComponent,
    CustomFieldContentComponent,
    CustomFieldOptionsPanelComponent,
    OptionLabelCellComponent,
    OptionCodeCellComponent,
    OptionColorCellComponent,
    OptionDefaultCellComponent,
    RemoveOptionCellComponent,
    CustomFieldInformationPanelComponent,
    AddCustomFieldOptionDialogComponent,
    InfoListViewComponent,
    InfoListContentComponent,
    InfoListInformationPanelComponent,
    InfoListItemsPanelComponent,
    InfoListItemCodeCellComponent,
    InfoListItemColorCellComponent,
    InfoListItemDefaultCellComponent,
    InfoListItemLabelCellComponent,
    RemoveInfoListItemCellComponent,
    InfoListItemIconCellComponent,
    AddInfoListItemDialogComponent,
    EnvironmentVariableWorkspaceComponent,
    EnvironmentVariableGridComponent,
    DeleteEnvironmentVariableComponent,
    DeleteEnvironmentVariableOptionComponent,
    EnvironmentVariableViewComponent,
    EnvironmentVariableContentComponent,
    EnvironmentVariableInformationPanelComponent,
    EnvironmentVariableOptionsPanelComponent,
    EvOptionLabelCellComponent,
    AddEnvironmentVariableOptionDialogComponent,
    EvOptionRemoveCellComponent,
    EnvironmentVariableViewDetailComponent,
    ProjectsBoundToCufComponent,
    EditRequirementLinksComponent,
    EditRequirementLinksDialogComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule,
    WorkspaceLayoutModule,
    NzToolTipModule,
    NzIconModule,
    NzDropDownModule,
    GridModule,
    AnchorModule,
    DialogModule,
    WorkspaceCommonModule,
    NzDividerModule,
    ReactiveFormsModule,
    CKEditorModule,
    NzCheckboxModule,
    FormsModule,
    NzFormModule,
    NzButtonModule,
    CellRendererCommonModule,
    NavBarModule,
    NzRadioModule,
    NzCollapseModule,
    AdminViewHeaderModule,
    UiManagerModule,
    NzInputModule,
  ],
})
export class CustomWorkspaceModule {}
