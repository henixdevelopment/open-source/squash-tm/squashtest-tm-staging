import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ActionErrorDisplayService,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
} from 'sqtm-core';
import { Subject } from 'rxjs';
import { InfoListViewService } from '../../../services/info-list-view.service';
import { catchError, finalize, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-info-list-item-default-cell',
  template: ` @if (columnDisplay && row) {
    <div class="full-width full-height flex-column">
      <label nz-radio style="margin: auto" [ngModel]="isChecked" (click)="handleClick()"> </label>
    </div>
  }`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InfoListItemDefaultCellComponent
  extends AbstractCellRendererComponent
  implements OnInit, OnDestroy
{
  unsub$ = new Subject<void>();

  get isChecked(): boolean {
    return Boolean(this.row.data[this.columnDisplay.id]);
  }

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    private infoListViewService: InfoListViewService,
    private actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    super(grid, cdRef);
  }

  handleClick() {
    if (!this.isChecked) {
      this.grid.beginAsyncOperation();
      const optionId = this.row.data[GridColumnId.id];
      this.infoListViewService
        .changeDefaultItem(optionId)
        .pipe(
          catchError((error) => {
            return this.actionErrorDisplayService.handleActionError(error);
          }),
          finalize(() => this.grid.completeAsyncOperation()),
        )
        .subscribe();
    }
  }

  ngOnInit(): void {
    this.infoListViewService.componentData$
      .pipe(takeUntil(this.unsub$))
      .subscribe(() => this.cdRef.markForCheck());
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}

export function infoListItemDefaultColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(InfoListItemDefaultCellComponent);
}
