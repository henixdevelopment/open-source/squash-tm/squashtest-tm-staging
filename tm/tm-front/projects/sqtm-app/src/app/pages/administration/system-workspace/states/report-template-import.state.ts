export class ReportTemplateImportState {
  file: File;
  currentStep: string;
}

export function initialReportTemplateImportState() {
  return {
    file: null,
    currentStep: 'CONFIGURATION',
  };
}

export class ReportTemplateImportComponentData extends ReportTemplateImportState {}
