import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import {
  AbstractDeleteCellRenderer,
  DialogService,
  GridService,
  RestService,
  TAUsageStatus,
} from 'sqtm-core';
import { filter, finalize, switchMap, takeUntil, tap } from 'rxjs/operators';
import { ProjectViewService } from '../../../services/project-view.service';

@Component({
  selector: 'sqtm-app-delete-project-job-cell',
  template: ` <sqtm-core-delete-icon (delete)="doDelete()"></sqtm-core-delete-icon> `,
  styleUrls: ['./delete-project-job-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteProjectJobCellComponent extends AbstractDeleteCellRenderer implements OnDestroy {
  constructor(
    public grid: GridService,
    cdr: ChangeDetectorRef,
    protected dialogService: DialogService,
    private restService: RestService,
    private projectViewService: ProjectViewService,
  ) {
    super(grid, cdr, dialogService);
  }

  doDelete() {
    this.restService
      .get<{ usageStatus: TAUsageStatus }>([
        'test-automation-projects',
        this.row.data.taProjectId,
        'usage-status',
      ])
      .pipe(
        switchMap((response) => this.showDeleteDialog(response.usageStatus)),
        takeUntil(this.unsub$),
        filter((result) => result === true),
        tap(() => this.grid.beginAsyncOperation()),
        switchMap(() =>
          this.projectViewService.removeTestAutomationProjects([this.row.data.taProjectId]),
        ),
        finalize(() => this.grid.completeAsyncOperation()),
      )
      .subscribe();
  }

  private showDeleteDialog(usageStatus: TAUsageStatus) {
    const hasExecutedTests = usageStatus.hasExecutedTests;

    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.projects.dialog.title.delete-job',
      messageKey: hasExecutedTests
        ? 'sqtm-core.administration-workspace.projects.dialog.message.delete-job-with-executions'
        : 'sqtm-core.administration-workspace.projects.dialog.message.delete-job-without-execution',
      level: hasExecutedTests ? 'DANGER' : 'WARNING',
    });

    return dialogReference.dialogClosed$;
  }
}
