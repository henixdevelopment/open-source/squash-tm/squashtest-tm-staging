import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewContainerRef,
} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  DialogService,
  FileViewerComponent,
  FileViewerDialogConfiguration,
  GridColumnId,
  GridService,
  SynchronizationStatus,
  FileViewerFilePathType,
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-download-error-log-file-cell',
  template: `
    @if (row.data[columnDisplay.id] && hasFailureStatus()) {
      <sqtm-core-toggle-icon
        class="full-height full-width icon-container current-workspace-main-color"
      >
        <a (click)="openFilePreviewDialog($event)">
          <i
            class="action-icon-size"
            nz-icon
            nzType="sqtm-core-generic:file-alert"
            nzTheme="outline"
            nz-tooltip
            [nzTooltipTitle]="'sqtm-core.generic.label.download-error-log' | translate"
          >
          </i>
        </a>
      </sqtm-core-toggle-icon>
    }
  `,
  styleUrls: ['./download-error-log-file-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DownloadErrorLogFileCellComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    cdRef: ChangeDetectorRef,
    private vcr: ViewContainerRef,
    private dialogService: DialogService,
  ) {
    super(grid, cdRef);
  }
  hasFailureStatus() {
    return this.row.data[GridColumnId.status] === SynchronizationStatus.FAILURE;
  }

  openFilePreviewDialog(event: any) {
    event.preventDefault();

    this.dialogService.openDialog<FileViewerDialogConfiguration, any>({
      id: 'synchronisations-supervision-file-viewer',
      component: FileViewerComponent,
      viewContainerReference: this.vcr,
      data: {
        filePath: this.row.data[GridColumnId.syncErrorLogFilePath],
        origin: FileViewerFilePathType.SYNCHRO,
      },
      minHeight: '100%',
      minWidth: '100%',
    });
  }
}

export function syncErrorLogDownloadColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(DownloadErrorLogFileCellComponent)
    .withViewport('rightViewport')
    .disableHeader();
}
