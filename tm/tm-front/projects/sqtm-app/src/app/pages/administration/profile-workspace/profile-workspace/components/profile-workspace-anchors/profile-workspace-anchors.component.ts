import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AdminReferentialDataService, AuthenticatedUser } from 'sqtm-core';
import { Observable } from 'rxjs';

@Component({
  selector: 'sqtm-app-profile-workspace-anchors',
  templateUrl: './profile-workspace-anchors.component.html',
  styleUrls: ['./profile-workspace-anchors.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileWorkspaceAnchorsComponent {
  authenticatedUser$: Observable<AuthenticatedUser>;
  constructor(public readonly adminReferentialDataService: AdminReferentialDataService) {
    this.authenticatedUser$ = adminReferentialDataService.authenticatedUser$;
  }
}
