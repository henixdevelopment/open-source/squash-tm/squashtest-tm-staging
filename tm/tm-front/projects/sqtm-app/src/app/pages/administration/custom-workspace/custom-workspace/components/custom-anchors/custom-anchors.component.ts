import { ChangeDetectionStrategy, Component } from '@angular/core';
import { WorkspaceWithGridComponent } from 'sqtm-core';

@Component({
  selector: 'sqtm-app-custom-anchors',
  templateUrl: './custom-anchors.component.html',
  styleUrls: ['./custom-anchors.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomAnchorsComponent {
  constructor(private workspaceWithGrid: WorkspaceWithGridComponent) {}

  hideContextualContent() {
    this.workspaceWithGrid.switchToNoRowLayout();
  }
}
