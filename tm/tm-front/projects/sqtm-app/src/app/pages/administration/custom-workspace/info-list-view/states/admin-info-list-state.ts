import { AdminInfoListItem, SqtmGenericEntityState } from 'sqtm-core';

export interface AdminInfoListState extends SqtmGenericEntityState {
  id: number;
  label: string;
  code: string;
  description: string;
  createdBy: string;
  createdOn: Date;
  lastModifiedBy: string;
  lastModifiedOn: Date;
  items: AdminInfoListItem[];
}
