import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import { AbstractDeleteCellRenderer, DialogService, GridService } from 'sqtm-core';
import { Subject } from 'rxjs';
import { TeamViewService } from '../../../services/team-view.service';

@Component({
  selector: 'sqtm-app-remove-team-authorisation-cell',
  template: `
    <sqtm-core-delete-icon
      [iconName]="getIcon()"
      (delete)="showDeleteConfirm()"
    ></sqtm-core-delete-icon>
  `,
  styleUrls: ['./remove-team-authorisation-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RemoveTeamAuthorisationCellComponent
  extends AbstractDeleteCellRenderer
  implements OnDestroy
{
  unsub$ = new Subject<void>();

  constructor(
    public grid: GridService,
    cdr: ChangeDetectorRef,
    protected dialogService: DialogService,
    private teamViewService: TeamViewService,
  ) {
    super(grid, cdr, dialogService);
  }

  getIcon(): string {
    return 'sqtm-core-generic:unlink';
  }

  doDelete() {
    this.grid.beginAsyncOperation();
    const projectId = this.row.data.projectId;
    this.teamViewService
      .removeAuthorisation([projectId])
      .subscribe(() => this.grid.completeAsyncOperation());
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  protected getTitleKey(): string {
    return 'sqtm-core.administration-workspace.teams.dialog.title.remove-authorisation.remove-one';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.administration-workspace.teams.dialog.message.remove-authorisation.remove-one';
  }
}
