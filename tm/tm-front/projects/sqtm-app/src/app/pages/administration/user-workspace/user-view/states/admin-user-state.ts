import { Profile, SqtmGenericEntityState, UsersGroup } from 'sqtm-core';

export interface AdminUserState extends SqtmGenericEntityState {
  id: number;
  login: string;
  firstName: string;
  lastName: string;
  email: string;
  active: boolean;
  createdOn: Date;
  createdBy: string;
  lastModifiedBy: string;
  lastModifiedOn: Date;
  lastConnectedOn: Date;
  usersGroupBinding: number;
  usersGroups: UsersGroup[];
  profiles: Profile[];
  projectPermissions: ProjectPermission[];
  teams: AssociatedTeam[];
  canManageLocalPassword: boolean;
  canDeleteFromFront: boolean;
}
export interface ProjectPermission {
  projectId: number;
  projectName: string;
  permissionGroup: Profile;
}
export interface AssociatedTeam {
  partyId: number;
  name: string;
}

export interface AssociatedTeamReport {
  nonAssociatedTeamNames: string[];
  teams: AssociatedTeam[];
}
