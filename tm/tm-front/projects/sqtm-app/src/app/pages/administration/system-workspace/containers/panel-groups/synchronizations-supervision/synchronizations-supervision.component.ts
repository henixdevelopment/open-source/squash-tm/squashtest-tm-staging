import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { interval, Observable, Subject, Subscription } from 'rxjs';
import { SystemViewService } from '../../../services/system-view.service';
import { SystemViewState } from '../../../states/system-view.state';
import { filter, switchMap, take, takeUntil, withLatestFrom } from 'rxjs/operators';
import {
  AdminReferentialDataService,
  LocalPersistenceService,
  getSupportedBrowserLang,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'sqtm-app-synchronizations-supervision',
  templateUrl: './synchronizations-supervision.component.html',
  styleUrls: ['./synchronizations-supervision.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SynchronizationsSupervisionComponent implements OnInit, OnDestroy {
  unsub$ = new Subject<void>();

  componentData$: Observable<SystemViewState>;
  autoRefreshInterval$: Observable<any> = interval(60000);
  intervalSubscription: Subscription;

  constructor(
    public readonly systemViewService: SystemViewService,
    public readonly adminReferentialDataService: AdminReferentialDataService,
    public translateService: TranslateService,
    public datePipe: DatePipe,
    private persistenceService: LocalPersistenceService,
  ) {
    this.componentData$ = this.systemViewService.componentData$.pipe(takeUntil(this.unsub$));
  }

  ngOnInit() {
    this.loadRefreshSelectionFromLocalStorage();
    this.initializeAutoRefreshMode();
  }

  private loadRefreshSelectionFromLocalStorage() {
    this.persistenceService
      .get(SYNC_SUPERVISION_AUTO_REFRESH_PERSISTENCE_KEY)
      .pipe(
        take(1),
        filter(Boolean),
        switchMap((isActivated: boolean) =>
          this.systemViewService.toggleAutomaticRefresh(isActivated),
        ),
      )
      .subscribe();
  }

  private initializeAutoRefreshMode() {
    this.intervalSubscription = this.autoRefreshInterval$
      .pipe(
        withLatestFrom(this.componentData$),
        filter(([, state]) => state.isAutomaticRefreshForSupervisionPageActivated),
      )
      .subscribe(() => this.refresh());
  }

  toggleAutomaticRefresh(componentData: SystemViewState) {
    const newValue = !componentData.isAutomaticRefreshForSupervisionPageActivated;
    this.systemViewService
      .toggleAutomaticRefresh(newValue)
      .subscribe((_state) => this.persistAutoRefreshSelection(newValue));
  }

  refresh() {
    this.systemViewService.load();
  }

  formatRefreshDate(date: Date) {
    return this.datePipe.transform(date, 'medium', getSupportedBrowserLang(this.translateService));
  }

  private persistAutoRefreshSelection(activated: boolean) {
    this.persistenceService
      .set(SYNC_SUPERVISION_AUTO_REFRESH_PERSISTENCE_KEY, activated)
      .subscribe();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
    this.intervalSubscription.unsubscribe();
  }
}

export const SYNC_SUPERVISION_AUTO_REFRESH_PERSISTENCE_KEY = 'sync-supervision-auto-refresh';
