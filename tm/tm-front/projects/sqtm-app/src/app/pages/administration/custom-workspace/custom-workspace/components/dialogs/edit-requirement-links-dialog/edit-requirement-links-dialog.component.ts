import { ChangeDetectionStrategy, Component, OnInit, signal, WritableSignal } from '@angular/core';
import { RequirementsLinkService } from '../../../services/requirements-link.service';
import { DialogReference, extractSquashFirstFieldError, GridService } from 'sqtm-core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EditRequirementLinksDialogConfiguration } from './edit-requirement-links-dialog.configuration';
import { catchError, finalize, tap } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-edit-requirement-links-dialog',
  templateUrl: './edit-requirement-links-dialog.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditRequirementLinksDialogComponent implements OnInit {
  data: EditRequirementLinksDialogConfiguration;
  $serverSideError: WritableSignal<string> = signal(null);
  $translatedErrorParams: WritableSignal<string> = signal(null);
  protected formGroup: FormGroup;

  constructor(
    private requirementLinksService: RequirementsLinkService,
    private dialogRef: DialogReference,
    private fb: FormBuilder,
    private grid: GridService,
  ) {
    this.data = this.dialogRef.data;
  }

  ngOnInit(): void {
    this.initializeFormGroup();
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      role1: this.fb.control(this.data.role1, [Validators.required, Validators.maxLength(50)]),
      role1Code: this.fb.control(this.data.role1Code, [
        Validators.required,
        Validators.maxLength(30),
      ]),
      role2: this.fb.control(this.data.role2, [Validators.required, Validators.maxLength(50)]),
      role2Code: this.fb.control(this.data.role2Code, [
        Validators.required,
        Validators.maxLength(30),
      ]),
    });
  }

  confirm(): void {
    this.$serverSideError.set(null);
    this.$translatedErrorParams.set(null);

    this.requirementLinksService
      .updateRequirementLinks({
        linkTypeId: this.data.requirementLinkId.toString(),
        role1: this.formGroup.controls['role1'].value,
        role1Code: this.formGroup.controls['role1Code'].value,
        role2: this.formGroup.controls['role2'].value,
        role2Code: this.formGroup.controls['role2Code'].value,
      })
      .pipe(
        tap(() => this.grid.beginAsyncOperation()),
        catchError((err) => {
          this.$serverSideError.set(
            extractSquashFirstFieldError(err).fieldValidationErrors[0].i18nKey,
          );
          this.$translatedErrorParams.set(
            extractSquashFirstFieldError(err).fieldValidationErrors[0].messageArgs[0],
          );
          throw err;
        }),
        finalize(() => this.grid.completeAsyncOperation()),
      )
      .subscribe(() => {
        this.dialogRef.close();
        this.requirementLinksService.refreshData();
      });
  }
}
