import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import {
  ImportFromXrayState,
  XrayInfoDisplay,
  xrayInfoRecord,
  XrayInfoRecordStructure,
} from '../../state/import-from-xray.state';

@Component({
  selector: 'sqtm-app-import-from-xray-confirmation',
  templateUrl: './import-from-xray-confirmation.component.html',
  styleUrl: './import-from-xray-confirmation.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImportFromXrayConfirmationComponent {
  @Input({ required: true }) componentData: ImportFromXrayState;

  getXrayEntities(): XrayInfoDisplay[] {
    return Object.values(xrayInfoRecord)
      .filter(
        (entity: XrayInfoRecordStructure) =>
          this.componentData.xrayItem.entities[entity.entityName],
      )
      .map((entity: XrayInfoRecordStructure): XrayInfoDisplay => {
        return {
          identifier: entity.entityName,
          i18nKey: entity.i18nKey,
          entityCount: this.componentData.xrayItem.entities[entity.entityName].entityCount,
        };
      });
  }
}
