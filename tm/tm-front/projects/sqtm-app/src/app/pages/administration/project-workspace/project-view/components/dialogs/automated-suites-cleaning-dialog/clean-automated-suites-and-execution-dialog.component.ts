import { ChangeDetectionStrategy, Component } from '@angular/core';
import {
  CleaningTypes,
  CleanAutomatedSuitesAndExecutionProjectConfiguration,
} from './clean-automated-suites-and-execution-project-configuration';
import { DialogReference, ActionErrorDisplayService } from 'sqtm-core';
import { ProjectViewService } from '../../../services/project-view.service';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Component({
  selector: 'sqtm-app-clean-automated-suites-and-execution-dialog',
  templateUrl: './clean-automated-suites-and-execution-dialog.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CleanAutomatedSuitesAndExecutionDialogComponent {
  configuration: CleanAutomatedSuitesAndExecutionProjectConfiguration;

  loading = false;

  constructor(
    private dialogReference: DialogReference<
      CleanAutomatedSuitesAndExecutionProjectConfiguration,
      boolean
    >,
    private projectViewService: ProjectViewService,
    private actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    this.configuration = this.dialogReference.data;
  }

  confirm() {
    this.loading = true;
    this.projectViewService
      .cleanAutomatedSuitesAndExecutions(
        this.configuration.projectId,
        this.configuration.cleaningType,
      )
      .pipe(
        catchError((err) => {
          this.actionErrorDisplayService.handleActionError(err);
          return throwError(() => err);
        }),
      )
      .subscribe(() => this.dialogReference.close());
  }

  protected readonly CleaningTypes = CleaningTypes;
}
