import { Injectable } from '@angular/core';
import {
  AdminReferentialDataService,
  AutomationDeletionCount,
  createStore,
  DocXReportId,
  ReportTemplateModel,
  RestService,
  Store,
  SystemViewModel,
} from 'sqtm-core';
import { Observable } from 'rxjs';
import { SystemViewState } from '../states/system-view.state';
import { filter, map, take, tap, withLatestFrom } from 'rxjs/operators';
import decamelize from 'decamelize';

@Injectable()
export class SystemViewService {
  public componentData$: Observable<SystemViewState>;

  private readonly store: Store<SystemViewState>;

  public readonly rootUrl = 'system';

  constructor(
    protected restService: RestService,
    protected adminReferentialDataService: AdminReferentialDataService,
  ) {
    this.store = createStore<SystemViewState>(SystemViewService.getInitialState());
    this.componentData$ = this.store.state$.pipe(
      withLatestFrom(this.adminReferentialDataService.loggedAsAdmin$),
      filter(
        ([state, loggedAsAdmin]) =>
          (Boolean(state) && Boolean(state.appVersion)) ||
          (!loggedAsAdmin && Boolean(state.synchronisationPlugins)),
      ),
      map(([state]) => state),
    );
  }

  load(): void {
    this.restService
      .get<SystemViewModel>(['system-view'])
      .pipe(take(1), withLatestFrom(this.store.state$))
      .subscribe(([model, state]) => {
        this.commit({
          ...model,
          isAutomaticRefreshForSupervisionPageActivated:
            state.isAutomaticRefreshForSupervisionPageActivated,
          lastSupervisionPageRefresh: new Date(),
        });
      });
  }

  retrieveCurrentActiveUsersCount(): Observable<void> {
    return this.restService.get(['system-view', 'current-active-users-count']).pipe(
      take(1),
      withLatestFrom(this.store.state$),
      map(([response, state]: [any, SystemViewState]) =>
        this.updateCurrentActiveUsersCountState(response.currentActiveUsersCount, state),
      ),
      map((state) => this.store.commit(state)),
    );
  }

  changeWhiteList(whiteList: string): Observable<SystemViewState> {
    return this.update('whiteList', whiteList, ['settings']);
  }

  changeUploadSizeLimit(size: string): Observable<SystemViewState> {
    return this.update('uploadSizeLimit', size, ['settings']);
  }

  changeImportSizeLimit(size: string): Observable<SystemViewState> {
    return this.update('importSizeLimit', size, ['settings']);
  }

  changeCallbackUrl(callbackUrl: string): Observable<any> {
    return this.update('callbackUrl', callbackUrl, ['settings']);
  }

  setLoginMessage(message: string): Observable<SystemViewState> {
    return this.update('loginMessage', message, ['messages']);
  }

  setBannerMessage(message: string): Observable<SystemViewState> {
    return this.update('bannerMessage', message, ['messages']);
  }

  setWelcomeMessage(message: string): Observable<SystemViewState> {
    return this.update('welcomeMessage', message, ['messages']);
  }

  changeStackTraceFeatureEnabled(enabled: boolean): void {
    this.updateFeature('stackTraceFeatureIsEnabled', 'stack-trace', enabled);
  }

  changeCaseInsensitiveLoginEnabled(enabled: boolean): void {
    this.updateFeature('caseInsensitiveLogin', 'case-insensitive-login', enabled);
  }

  changeCaseInsensitiveActionsEnabled(enabled: boolean): void {
    this.updateFeature('caseInsensitiveActions', 'case-insensitive-actions', enabled);
  }

  changeAutoconnectOnConnection(enabled: boolean): void {
    this.updateFeature('autoconnectOnConnection', 'autoconnect-on-connection', enabled);
  }

  changeUnsafeAttachmentPreviewEnabled(enabled: boolean): void {
    this.updateFeature('unsafeAttachmentPreviewEnabled', 'unsafe-attachment-preview', enabled);
  }

  getOldAutomatedSuitesAndExecutionsCount(): Observable<AutomationDeletionCount> {
    return this.restService.get<AutomationDeletionCount>(['cleaning/count']);
  }

  cleanAutomatedSuitesAndExecutions(): Observable<void> {
    return this.restService.post(['cleaning']);
  }

  addTemplateModels(fileName: string, reportId: DocXReportId): void {
    this.componentData$
      .pipe(
        take(1),
        map((state) => this.addTemplateModel(state, fileName, reportId)),
        map((state: SystemViewState) => this.updateStateWithTemplateModels(state)),
      )
      .subscribe((state) => {
        this.store.commit(state);
      });
  }

  downloadTemplate(reportId: DocXReportId, fileName: string): string {
    return `${window.location.origin}${this.restService.backendRootUrl}reports/${reportId}/download-custom-template/${fileName}`;
  }

  deleteCustomReportTemplate(selectedTemplate: ReportTemplateModel): Observable<SystemViewState> {
    return this.restService
      .post(['reports', selectedTemplate.reportId, 'delete-custom-template'], {
        templateFileName: selectedTemplate.fileName,
      })
      .pipe(
        withLatestFrom(this.store.state$),
        map(([, state]) => this.removeTemplateModel(state, selectedTemplate)),
        tap((state: SystemViewState) => this.store.commit(state)),
      );
  }

  isTemplateUsedInReport(templateModel: ReportTemplateModel): Observable<boolean> {
    return this.restService.post(
      ['reports', templateModel.reportId, 'is-template-used-in-report'],
      { templateFileName: templateModel.fileName },
    );
  }

  private static getInitialState(): SystemViewState {
    return {
      appVersion: null,
      statistics: null,
      plugins: null,
      whiteList: '',
      uploadSizeLimit: '',
      importSizeLimit: '',
      callbackUrl: '',
      stackTracePanelIsVisible: false,
      stackTraceFeatureIsEnabled: false,
      caseInsensitiveLogin: false,
      caseInsensitiveActions: false,
      duplicateLogins: [],
      duplicateActions: [],
      welcomeMessage: '',
      loginMessage: '',
      bannerMessage: '',
      logFiles: [],
      autoconnectOnConnection: false,
      licenseInfo: null,
      currentActiveUsersCount: 0,
      reportTemplateModels: [],
      synchronisationPlugins: null,
      isAutomaticRefreshForSupervisionPageActivated: false,
      lastSupervisionPageRefresh: null,
      unsafeAttachmentPreviewEnabled: false,
    };
  }

  private commit(state: SystemViewState) {
    this.store.commit(state);
  }

  private update(
    fieldName: string,
    value: any,
    urlParts: string[] = [],
  ): Observable<SystemViewState> {
    const body = { [fieldName]: value };
    const snakeCased = decamelize(fieldName, '-');
    return this.restService.post([this.rootUrl, ...urlParts, snakeCased], body).pipe(
      withLatestFrom(this.store.state$),
      map(([, state]) => {
        const updatedState: SystemViewState = { ...state };
        updatedState[fieldName] = value;
        this.commit(updatedState);
        return updatedState;
      }),
    );
  }

  private updateFeature(propertyName: keyof SystemViewState, urlPart: string, value: boolean) {
    const options = { params: { enabled: value.toString() } };
    return this.restService
      .post(['features', urlPart], {}, options)
      .pipe(
        take(1),
        withLatestFrom(this.store.state$),
        map(([, state]: [any, SystemViewState]) => ({ ...state, [propertyName]: value })),
      )
      .subscribe((state) => {
        this.store.commit(state);
      });
  }

  private updateCurrentActiveUsersCountState(
    currentActiveUsersCount: number,
    state: SystemViewState,
  ): SystemViewState {
    return {
      ...state,
      currentActiveUsersCount,
    };
  }

  private updateStateWithTemplateModels(state: SystemViewState): SystemViewState {
    return {
      ...state,
      reportTemplateModels: state.reportTemplateModels,
    };
  }

  private addTemplateModel(
    state: SystemViewState,
    fileName: string,
    reportId: DocXReportId,
  ): SystemViewState {
    const templateModel: ReportTemplateModel = state.reportTemplateModels
      .filter((template) => template.reportId === reportId)
      .find((template) => template.fileName === fileName);

    if (typeof templateModel === 'undefined') {
      state.reportTemplateModels.push({ reportId: reportId, fileName: fileName });
    }
    return state;
  }

  private removeTemplateModel(
    state: SystemViewState,
    selectedTemplate: ReportTemplateModel,
  ): SystemViewState {
    const updatedTemplateModelList: ReportTemplateModel[] = [];
    state.reportTemplateModels.forEach((model) => {
      if (
        model.reportId !== selectedTemplate.reportId ||
        model.fileName !== selectedTemplate.fileName
      ) {
        updatedTemplateModelList.push(model);
      }
    });

    return {
      ...state,
      reportTemplateModels: updatedTemplateModelList,
    };
  }

  toggleAutomaticRefresh(newValue: boolean): Observable<SystemViewState> {
    return this.store.state$.pipe(
      take(1),
      map((state) => {
        const updatedState = { ...state, isAutomaticRefreshForSupervisionPageActivated: newValue };
        this.store.commit(updatedState);
        return updatedState;
      }),
    );
  }
}
