import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import {
  CreationDialogData,
  DialogReference,
  DisplayOption,
  FieldValidationError,
  MilestoneStatus,
  NOT_ONLY_SPACES_REGEX,
  RestService,
} from 'sqtm-core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AbstractAdministrationCreationDialogDirective } from '../../../../../components/abstract-administration-creation-dialog';
import { of } from 'rxjs';

@Component({
  selector: 'sqtm-app-milestone-creation-dialog',
  templateUrl: './milestone-creation-dialog.component.html',
  styleUrls: ['./milestone-creation-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MilestoneCreationDialogComponent
  extends AbstractAdministrationCreationDialogDirective
  implements OnInit
{
  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  data: CreationDialogData;
  milestoneStatusOptions: DisplayOption[] = [];

  constructor(
    private fb: FormBuilder,
    private translateService: TranslateService,
    dialogReference: DialogReference,
    restService: RestService,
    cdr: ChangeDetectorRef,
  ) {
    super('milestones/new', dialogReference, restService, cdr);
  }

  get textFieldToFocus(): string {
    return 'label';
  }

  ngOnInit() {
    this.getMilestoneStatusOptions();
    this.initializeFormGroup();
  }

  private getMilestoneStatusOptions() {
    for (const milestoneStatus of Object.keys(MilestoneStatus)) {
      const item = MilestoneStatus[milestoneStatus];
      this.milestoneStatusOptions.push({
        label: this.translateService.instant(item.i18nKey),
        id: item.id,
      });
    }
  }

  protected getRequestPayload() {
    return of({
      label: this.getFormControlValue('label'),
      status: this.getFormControlValue('status'),
      endDate: this.getFormControlValue('endDate'),
      description: this.getFormControlValue('description'),
    });
  }

  protected doResetForm() {
    this.resetFormControl('label', '');
    this.resetFormControl('description', '');
    this.resetFormControl('endDate', '');
    this.resetFormControl('status', 'PLANNED');
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      label: this.fb.control('', [
        Validators.required,
        Validators.pattern(NOT_ONLY_SPACES_REGEX),
        Validators.maxLength(255),
      ]),
      status: this.fb.control('PLANNED', [Validators.required]),
      endDate: this.fb.control('', [Validators.required]),
      description: this.fb.control(''),
    });
  }
}
