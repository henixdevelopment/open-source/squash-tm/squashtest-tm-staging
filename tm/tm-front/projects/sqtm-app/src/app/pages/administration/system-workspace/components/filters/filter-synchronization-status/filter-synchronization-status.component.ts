import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractFilterWidget,
  DiscreteFilterValue,
  Filter,
  FilterBuilder,
  FilterOperation,
  isDiscreteValue,
  ListItem,
  SynchronizationStatus,
  Workspaces,
} from 'sqtm-core';

@Component({
  selector: 'sqtm-core-filter-synchronization-status',
  templateUrl: './filter-synchronization-status.component.html',
  styleUrls: ['./filter-synchronization-status.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterSynchronizationStatusComponent extends AbstractFilterWidget {
  items: ListItem[];
  workspace: Workspaces;

  filterValue: DiscreteFilterValue[];

  constructor(public cdRef: ChangeDetectorRef) {
    super(cdRef);
  }

  setFilter(filter: Filter) {
    const filterValue = filter.value;
    if (isDiscreteValue(filterValue)) {
      this._filter = filter;
      const filterValues = filterValue.value.map((value) => value.id);
      this.items = Object.values(SynchronizationStatus).map((status) => {
        return {
          id: status,
          selected: filterValues.includes(status),
          i18nLabelKey: `sqtm-core.entity.remote-sync.synchronization-status.${status}`,
        };
      });

      this.filterValue = filterValue.value;
      this.workspace = filter.uiOptions?.workspace;
    } else {
      throw Error('Not handled kind ' + filter.value.kind);
    }
    this.cdRef.detectChanges();
  }

  changeSelection($event: ListItem) {
    const nextValue: DiscreteFilterValue[] = $event.selected
      ? this.filterValue.concat({ id: $event.id.toString() })
      : this.filterValue.filter((v) => v.id !== $event.id.toString());

    this.filteringChanged.next({
      filterValue: { kind: 'multiple-discrete-value', value: nextValue },
    });
  }
}

export function synchronizationStatusFilter(id: string, initialValue: string[]) {
  return new FilterBuilder(id)
    .alwaysActive()
    .withWidget(FilterSynchronizationStatusComponent)
    .withInitialValue({ kind: 'multiple-discrete-value', value: initialValue })
    .withOperations(FilterOperation.IN)
    .withAvailableOperations([FilterOperation.IN]);
}
