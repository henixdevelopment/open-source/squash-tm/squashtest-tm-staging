import { GenericEntityViewState, provideInitialGenericViewState } from 'sqtm-core';
import { AdminInfoListState } from './admin-info-list-state';

export interface AdminInfoListViewState
  extends GenericEntityViewState<AdminInfoListState, 'infoList'> {
  infoList: AdminInfoListState;
}

export function provideInitialAdminInfoListView(): Readonly<AdminInfoListViewState> {
  return provideInitialGenericViewState<AdminInfoListState, 'infoList'>('infoList');
}
