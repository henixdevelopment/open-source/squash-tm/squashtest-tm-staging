import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import { AbstractDeleteCellRenderer, DialogService, GridService } from 'sqtm-core';
import { Observable, Subject } from 'rxjs';
import { MilestoneViewService } from '../../../services/milestone-view.service';
import { AdminMilestoneViewState } from '../../../states/admin-milestone-view-state';

@Component({
  selector: 'sqtm-app-unbind-project-from-milestone-cell',
  template: `
    @if (componentData$ | async; as componentData) {
      @if (componentData.milestone.canEdit) {
        <sqtm-core-delete-icon
          [iconName]="getIcon()"
          (delete)="showDeleteConfirm()"
        ></sqtm-core-delete-icon>
      }
    }
  `,
  styleUrls: ['./unbind-project-from-milestone-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UnbindProjectFromMilestoneCellComponent
  extends AbstractDeleteCellRenderer
  implements OnDestroy
{
  componentData$: Observable<AdminMilestoneViewState>;
  unsub$ = new Subject<void>();

  constructor(
    public grid: GridService,
    cdr: ChangeDetectorRef,
    protected dialogService: DialogService,
    private milestoneViewService: MilestoneViewService,
  ) {
    super(grid, cdr, dialogService);
    this.componentData$ = this.milestoneViewService.componentData$;
  }

  getIcon(): string {
    return 'sqtm-core-generic:unlink';
  }

  doDelete() {
    this.grid.beginAsyncOperation();
    const projectId = this.row.data.projectId;
    this.milestoneViewService
      .unbindProjectsFromMilestone([projectId])
      .subscribe(() => this.grid.completeAsyncOperation());
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  protected getTitleKey(): string {
    return 'sqtm-core.administration-workspace.milestones.dialog.title.unbind-project-from-milestone.unbind-one';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.administration-workspace.milestones.dialog.message.unbind-project-from-milestone.unbind-one';
  }
}
