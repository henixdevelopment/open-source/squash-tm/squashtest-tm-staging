import { TestBed } from '@angular/core/testing';

import { TestAutomationServerViewService } from './test-automation-server-view.service';
import {
  AdminReferentialDataService,
  AdminTestAutomationServer,
  AttachmentService,
  AuthenticationProtocol,
  AutomationEnvironmentTagHolder,
  EntityViewAttachmentHelperService,
  EvInputType,
  RestService,
  TestAutomationServerKind,
} from 'sqtm-core';
import { AppTestingUtilsModule } from '../../../../../utils/testing-utils/app-testing-utils.module';
import { TranslateService } from '@ngx-translate/core';
import { of } from 'rxjs';
import { take } from 'rxjs/operators';
import {
  mockAdminReferentialDataService,
  mockRestService,
} from '../../../../../utils/testing-utils/mocks.service';
import {
  BoundEnvironmentVariableService,
  BoundEnvironmentVariableState,
} from '../../../../../components/squash-orchestrator/orchestrator-execution-environment-variable/services/bound-environment-variable.service';
import createSpyObj = jasmine.createSpyObj;
import SpyObj = jasmine.SpyObj;

describe('TestAutomationServerViewService', () => {
  let service: TestAutomationServerViewService;
  let boundEnvironmentVariableService: BoundEnvironmentVariableService;
  let restService: SpyObj<RestService>;

  beforeEach(() => {
    restService = mockRestService();

    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      providers: [
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: TranslateService,
          useValue: createSpyObj(['instant']),
        },
        {
          provide: AdminReferentialDataService,
          useValue: mockAdminReferentialDataService(),
        },
        {
          provide: TestAutomationServerViewService,
          useClass: TestAutomationServerViewService,
          deps: [
            RestService,
            AttachmentService,
            TranslateService,
            EntityViewAttachmentHelperService,
            AdminReferentialDataService,
          ],
        },
        {
          provide: BoundEnvironmentVariableService,
          useClass: BoundEnvironmentVariableService,
        },
      ],
    });
    service = TestBed.inject(TestAutomationServerViewService);
    boundEnvironmentVariableService = TestBed.inject(BoundEnvironmentVariableService);
  });

  it('should load a test automation server', () => {
    restService.getWithoutErrorHandling.and.returnValue(of(getInitialModel()));

    service.componentData$.subscribe((data) => {
      expect(data.testAutomationServer.id).toEqual(1);
    });

    expect(service).toBeTruthy();

    service.load(1);
  });

  it('should change manual slave selection', async () => {
    const testAutomationServer = getInitialModel();
    restService.getWithoutErrorHandling.and.returnValue(of(testAutomationServer));
    service.load(1);

    const changeManualSlaveSelectionResponse = {
      ...testAutomationServer,
      manualSlaveSelection: true,
    };
    restService.post.and.returnValue(of(changeManualSlaveSelectionResponse));

    service.changeManualSlaveSelection(true);
    service.componentData$.pipe(take(1)).subscribe((componentData) => {
      expect(componentData.testAutomationServer.manualSlaveSelection).toEqual(
        changeManualSlaveSelectionResponse.manualSlaveSelection,
      );
    });
  });

  it('should bind environment variables', async () => {
    const boundEnvironmentVariableState = getInitialBoundEnvironmentVariableModel();
    restService.getWithoutErrorHandling.and.returnValue(of(boundEnvironmentVariableState));
    boundEnvironmentVariableService.load(1, AutomationEnvironmentTagHolder.TEST_AUTOMATION_SERVER);

    const boundEv = [
      {
        id: 2,
        name: 'environmentvariable',
        inputType: EvInputType.PLAIN_TEXT,
        boundToServer: false,
        options: [],
        value: undefined,
      },
    ];

    const response = {
      ...boundEnvironmentVariableState,
      boundEnvironmentVariables: boundEv,
    };

    restService.post.and.returnValue(of(response));

    boundEnvironmentVariableService.bindEnvironmentVariables([2]).subscribe(() => {
      boundEnvironmentVariableService.state$.pipe(take(1)).subscribe((state) => {
        expect(state.boundEnvironmentVariables).toEqual(boundEv);
      });
    });
  });

  it('should unbind environment variables', async () => {
    const boundEnvironmentVariableState = getInitialBoundEnvironmentVariableModel();
    boundEnvironmentVariableState.boundEnvironmentVariables = [
      {
        id: 1,
        name: 'environmentvariable',
        inputType: EvInputType.PLAIN_TEXT,
        boundToServer: true,
        options: [],
        value: undefined,
      },
    ];

    restService.getWithoutErrorHandling.and.returnValue(of(boundEnvironmentVariableState));
    boundEnvironmentVariableService.load(1, AutomationEnvironmentTagHolder.TEST_AUTOMATION_SERVER);

    const response = {
      ...boundEnvironmentVariableState,
      boundEnvironmentVariables: [],
    };

    restService.post.and.returnValue(of(response));

    boundEnvironmentVariableService.unbindEnvironmentVariables([1]).subscribe(() => {
      boundEnvironmentVariableService.state$.pipe(take(1)).subscribe((state) => {
        expect(state.boundEnvironmentVariables.length).toEqual(0);
      });
    });
  });

  it('should set environment variable value', async () => {
    restService.get.and.returnValue(of(getInitialBoundEnvironmentVariableModel()));
    boundEnvironmentVariableService
      .load(1, AutomationEnvironmentTagHolder.TEST_AUTOMATION_SERVER)
      .subscribe();

    boundEnvironmentVariableService.setEnvironmentVariableValue(1, 'newValue', 1).subscribe(() => {
      boundEnvironmentVariableService.state$.pipe(take(1)).subscribe((state) => {
        expect(state.boundEnvironmentVariables[0].value).toEqual('newValue');
      });
    });
  });

  function getInitialModel(): AdminTestAutomationServer {
    return {
      id: 1,
      name: 'milestone 1',
      kind: TestAutomationServerKind.jenkins,
      baseUrl: 'http://127.0.0.01:8080',
      description: '',
      createdBy: '',
      createdOn: null,
      lastModifiedBy: null,
      lastModifiedOn: null,
      manualSlaveSelection: true,
      credentials: null,
      authProtocol: AuthenticationProtocol.BASIC_AUTH,
      supportedAuthenticationProtocols: [AuthenticationProtocol.BASIC_AUTH],
      supportsAutomatedExecutionEnvironments: false,
      availableEnvironments: [],
      environmentTags: [],
      availableEnvironmentTags: [],
      observerUrl: null,
      eventBusUrl: null,
      killSwitchUrl: null,
    };
  }

  function getInitialBoundEnvironmentVariableModel(): BoundEnvironmentVariableState {
    return {
      entityId: 1,
      entityType: AutomationEnvironmentTagHolder.TEST_AUTOMATION_SERVER,
      boundEnvironmentVariables: [
        {
          id: 1,
          name: 'environmentvariable',
          inputType: EvInputType.PLAIN_TEXT,
          boundToServer: true,
          options: [],
          value: '',
        },
      ],
    };
  }
});
