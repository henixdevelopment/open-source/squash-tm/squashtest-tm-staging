import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainServerWorkspaceComponent } from './server-workspace/containers/main-server-workspace/main-server-workspace.component';
import { ScmServerGridComponent } from './server-workspace/containers/scm-server-grid/scm-server-grid.component';
import { BugtrackerGridComponent } from './server-workspace/containers/bugtracker-grid/bugtracker-grid.component';
import { TestAutomationServerGridComponent } from './server-workspace/containers/test-automation-server-grid/test-automation-server-grid.component';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { AdminServersAnchorsComponent } from './server-workspace/components/admin-servers-anchors/admin-servers-anchors.component';
import { DeleteBugtrackerComponent } from './server-workspace/components/cell-renderers/delete-bugtracker/delete-bugtracker.component';
import { DeleteScmServerComponent } from './server-workspace/components/cell-renderers/delete-scm-server/delete-scm-server.component';
import { DeleteTestAutomationServerComponent } from './server-workspace/components/cell-renderers/delete-test-automation-server/delete-test-automation-server.component';
import { BugtrackerCreationDialogComponent } from './server-workspace/components/dialogs/bugtracker-creation-dialog/bugtracker-creation-dialog.component';
import { ScmServerCreationDialogComponent } from './server-workspace/components/dialogs/scm-server-creation-dialog/scm-server-creation-dialog.component';
import { TestAutomationServerCreationDialogComponent } from './server-workspace/components/dialogs/test-automation-server-creation-dialog/test-automation-server-creation-dialog.component';
import { RouterModule, Routes } from '@angular/router';
import {
  AnchorModule,
  CellRendererCommonModule,
  DialogModule,
  GridModule,
  NavBarModule,
  WorkspaceCommonModule,
  WorkspaceLayoutModule,
} from 'sqtm-core';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CKEditorModule } from 'ckeditor4-angular';
import { BugtrackerWorkspaceComponent } from './server-workspace/containers/bugtracker-workspace/bugtracker-workspace.component';
import { ScmServerWorkspaceComponent } from './server-workspace/containers/scm-server-workspace/scm-server-workspace.component';
import { TestAutomationServerWorkspaceComponent } from './server-workspace/containers/test-automation-server-workspace/test-automation-server-workspace.component';
import { BugTrackerViewComponent } from './bug-tracker-view/containers/bug-tracker-view/bug-tracker-view.component';
import { BugTrackerContentComponent } from './bug-tracker-view/containers/panel-groups/bug-tracker-content/bug-tracker-content.component';
import { TestAutomationServerViewComponent } from './test-automation-server-view/containers/test-automation-server-view/test-automation-server-view.component';
import { TestAutomationServerContentComponent } from './test-automation-server-view/containers/panel-groups/test-automation-server-content/test-automation-server-content.component';
import { ScmServerViewComponent } from './scm-server-view/containers/scm-server-view/scm-server-view.component';
import { ScmServerContentComponent } from './scm-server-view/containers/panel-groups/scm-server-content/scm-server-content.component';
import { BugTrackerAuthPolicyPanelComponent } from './bug-tracker-view/components/panels/bug-tracker-auth-policy-panel/bug-tracker-auth-policy-panel.component';
import { BugTrackerAuthProtocolPanelComponent } from './bug-tracker-view/components/panels/bug-tracker-auth-protocol-panel/bug-tracker-auth-protocol-panel.component';
import { BugTrackerInformationPanelComponent } from './bug-tracker-view/components/panels/bug-tracker-information-panel/bug-tracker-information-panel.component';
import { AdminViewHeaderModule } from '../components/admin-view-header/admin-view-header.module';
import { ScmServerInformationPanelComponent } from './scm-server-view/components/panels/scm-server-information-panel/scm-server-information-panel.component';
import { ScmServerAuthenticationProtocolPanelComponent } from './scm-server-view/components/panels/scm-server-authentication-protocol-panel/scm-server-authentication-protocol-panel.component';
import { ScmServerCommitPolicyPanelComponent } from './scm-server-view/components/panels/scm-server-commit-policy-panel/scm-server-commit-policy-panel.component';
import { ScmServerAuthenticationPolicyPanelComponent } from './scm-server-view/components/panels/scm-server-authentication-policy-panel/scm-server-authentication-policy-panel.component';
import { ScmServerRepositoriesPanelComponent } from './scm-server-view/components/panels/scm-server-repositories-panel/scm-server-repositories-panel.component';
import { DeleteScmRepositoryCellComponent } from './scm-server-view/components/cell-renderers/delete-scm-repository-cell/delete-scm-repository-cell.component';
import { EditableWorkingBranchColumnComponent } from './scm-server-view/components/cell-renderers/editable-working-branch-column/editable-working-branch-column.component';
import { AddScmRepositoryDialogComponent } from './scm-server-view/components/dialogs/add-scm-repository-dialog/add-scm-repository-dialog.component';
import { TestAutoServerInfoPanelComponent } from './test-automation-server-view/components/panels/test-auto-server-info-panel/test-auto-server-info-panel.component';
import { AuthProtocolFormComponent } from './components/auth-protocol-form/auth-protocol-form.component';
import { TAServerAuthPolicyPanelComponent } from './test-automation-server-view/components/panels/taserver-auth-policy-panel/taserver-auth-policy-panel.component';
import { TAServerAuthProtocolPanelComponent } from './test-automation-server-view/components/panels/taserver-auth-protocol-panel/taserver-auth-protocol-panel.component';
import { TestAutomationServerEnvironmentPanelComponent } from './test-automation-server-view/components/panels/test-automation-server-environment-panel/test-automation-server-environment-panel.component';
import { AiServerWorkspaceComponent } from './server-workspace/containers/ai-server-workspace/ai-server-workspace.component';
import { AiServerGridComponent } from './server-workspace/containers/ai-server-grid/ai-server-grid.component';
import { AiServerCreationDialogComponent } from './server-workspace/components/dialogs/ai-server-creation-dialog/ai-server-creation-dialog.component';
import { DeleteAiServerComponent } from './server-workspace/components/cell-renderers/delete-ai-server/delete-ai-server.component';
import { AiServerViewComponent } from './ai-server-view/containers/ai-server-view/ai-server-view.component';
import { AiServerContentComponent } from './ai-server-view/containers/panel-groups/ai-server-content/ai-server-content.component';
import { AiServerInformationPanelComponent } from './ai-server-view/components/panels/ai-server-information-panel/ai-server-information-panel.component';
import { AiServerAuthPolicyPanelComponent } from './ai-server-view/components/panels/ai-server-auth-policy-panel/ai-server-auth-policy-panel.component';
import { OrchestratorVersionDialogComponent } from './test-automation-server-view/components/dialogs/orchestrator-version-dialog/orchestrator-version-dialog.component';
import { AiServerPayloadPanelComponent } from './ai-server-view/components/panels/ai-server-payload-panel/ai-server-payload-panel.component';
import { AiServerRequirementModelPanelComponent } from './ai-server-view/components/panels/ai-server-requirement-model-panel/ai-server-requirement-model-panel.component';
import { AiServerTestCaseGenerationDialogComponent } from './ai-server-view/components/dialogs/ai-server-test-case-generation-dialog/ai-server-test-case-generation-dialog.component';
import { GeneratedTestCaseFoldableRowComponent } from '../../../components/artificial-intelligence/generated-test-case-foldable-row/generated-test-case-foldable-row.component';
import { BugTrackerCachePanelComponent } from './bug-tracker-view/components/panels/bug-tracker-cache-panel/bug-tracker-cache-panel.component';
import { RecloneScmRepositoryCellComponent } from './scm-server-view/components/cell-renderers/reclone-scm-repository-cell/reclone-scm-repository-cell.component';
import { SquashOrchestratorModule } from '../../../components/squash-orchestrator/squash-orchestrator.module';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'bugtrackers',
    pathMatch: 'full',
  },
  {
    path: '',
    component: MainServerWorkspaceComponent,
    children: [
      {
        path: 'bugtrackers',
        component: BugtrackerWorkspaceComponent,
        children: [
          {
            path: ':bugTrackerId',
            component: BugTrackerViewComponent,
            children: [
              {
                path: 'content',
                component: BugTrackerContentComponent,
              },
            ],
          },
        ],
      },
      {
        path: 'test-automation-servers',
        component: TestAutomationServerWorkspaceComponent,
        children: [
          {
            path: ':testAutomationServerId',
            component: TestAutomationServerViewComponent,
            children: [
              {
                path: 'content',
                component: TestAutomationServerContentComponent,
              },
            ],
          },
        ],
      },
      {
        path: 'scm-servers',
        component: ScmServerWorkspaceComponent,
        children: [
          {
            path: ':scmServerId',
            component: ScmServerViewComponent,
            children: [
              {
                path: 'content',
                component: ScmServerContentComponent,
              },
            ],
          },
        ],
      },
      {
        path: 'ai-server',
        component: AiServerWorkspaceComponent,
        children: [
          {
            path: ':aiServerId',
            component: AiServerViewComponent,
            children: [
              {
                path: 'content',
                component: AiServerContentComponent,
              },
            ],
          },
        ],
      },
    ],
  },
];

@NgModule({
  declarations: [
    AiServerContentComponent,
    AiServerCreationDialogComponent,
    AiServerGridComponent,
    AiServerInformationPanelComponent,
    AiServerViewComponent,
    AiServerWorkspaceComponent,
    MainServerWorkspaceComponent,
    ScmServerGridComponent,
    BugtrackerGridComponent,
    TestAutomationServerGridComponent,
    AdminServersAnchorsComponent,
    DeleteAiServerComponent,
    DeleteBugtrackerComponent,
    DeleteScmServerComponent,
    DeleteTestAutomationServerComponent,
    BugtrackerCreationDialogComponent,
    ScmServerCreationDialogComponent,
    TestAutomationServerCreationDialogComponent,
    BugtrackerWorkspaceComponent,
    ScmServerWorkspaceComponent,
    TestAutomationServerWorkspaceComponent,
    BugTrackerAuthPolicyPanelComponent,
    BugTrackerAuthProtocolPanelComponent,
    BugTrackerContentComponent,
    BugTrackerInformationPanelComponent,
    BugTrackerViewComponent,
    ScmServerViewComponent,
    ScmServerContentComponent,
    ScmServerInformationPanelComponent,
    ScmServerAuthenticationProtocolPanelComponent,
    ScmServerCommitPolicyPanelComponent,
    ScmServerAuthenticationPolicyPanelComponent,
    ScmServerRepositoriesPanelComponent,
    DeleteScmRepositoryCellComponent,
    EditableWorkingBranchColumnComponent,
    AddScmRepositoryDialogComponent,
    TestAutomationServerViewComponent,
    TestAutomationServerContentComponent,
    TestAutoServerInfoPanelComponent,
    AuthProtocolFormComponent,
    TAServerAuthPolicyPanelComponent,
    TAServerAuthProtocolPanelComponent,
    TestAutomationServerEnvironmentPanelComponent,
    OrchestratorVersionDialogComponent,
    AiServerAuthPolicyPanelComponent,
    AiServerPayloadPanelComponent,
    AiServerRequirementModelPanelComponent,
    AiServerTestCaseGenerationDialogComponent,
    BugTrackerCachePanelComponent,
    RecloneScmRepositoryCellComponent,
  ],
  imports: [
    CommonModule,
    NzIconModule,
    NzDropDownModule,
    GridModule,
    WorkspaceLayoutModule,
    TranslateModule.forChild(),
    AnchorModule,
    ReactiveFormsModule,
    WorkspaceCommonModule,
    RouterModule.forChild(routes),
    NzToolTipModule,
    CKEditorModule,
    DialogModule,
    NzCheckboxModule,
    NzFormModule,
    FormsModule,
    NavBarModule,
    AdminViewHeaderModule,
    NzButtonModule,
    NzCollapseModule,
    NzRadioModule,
    CellRendererCommonModule,
    SquashOrchestratorModule,
    GeneratedTestCaseFoldableRowComponent,
  ],
})
export class ServerWorkspaceModule {}
