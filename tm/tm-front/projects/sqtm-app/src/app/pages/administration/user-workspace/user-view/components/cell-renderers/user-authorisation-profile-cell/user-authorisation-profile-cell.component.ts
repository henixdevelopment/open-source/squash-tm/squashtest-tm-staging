import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  Signal,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import {
  AbstractListCellRendererComponent,
  ActionErrorDisplayService,
  AdminReferentialDataService,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
  LicenseInformationMessageProvider,
  ListPanelItem,
  Profile,
  RestService,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { Overlay } from '@angular/cdk/overlay';
import { UserViewService } from '../../../services/user-view.service';
import { finalize, map, takeUntil } from 'rxjs/operators';
import { ProfileService } from '../../../../../profile-workspace/profile-workspace/services/profile.service';
import { AdminUserViewState } from '../../../states/admin-user-view-state';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sqtm-app-user-auhtorisation-profile-cell',
  template: ` @if (columnDisplay && row) {
    @if ($componentData(); as componentData) {
      <div
        class="full-width full-height container"
        [class.container-interactive]="canEdit()"
        [class.__hover_pointer]="canEdit()"
        (click)="showProfileList()"
      >
        <span
          #profileName
          class="text-ellipsis m-auto-0"
          nz-tooltip
          [sqtmCoreLabelTooltip]="getCellText(componentData.user.profiles)"
        >
          {{ getCellText(componentData.user.profiles) }}
        </span>
        <ng-template #templatePortalContent>
          <sqtm-core-list-panel
            [selectedItem]="row.data[columnDisplay.id]"
            (itemSelectionChanged)="change($event)"
            [items]="panelItems"
          >
          </sqtm-core-list-panel>
        </ng-template>
      </div>
    }
  }`,
  styleUrls: ['./user-authorisation-profile-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ProfileService],
})
export class UserAuthorisationProfileCellComponent
  extends AbstractListCellRendererComponent
  implements OnInit
{
  $componentData: Signal<AdminUserViewState>;

  constructor(
    public readonly grid: GridService,
    public readonly cdRef: ChangeDetectorRef,
    public readonly translateService: TranslateService,
    public readonly overlay: Overlay,
    public readonly vcr: ViewContainerRef,
    public readonly restService: RestService,
    public readonly userViewService: UserViewService,
    public readonly actionErrorDisplayService: ActionErrorDisplayService,
    public readonly adminReferentialDataService: AdminReferentialDataService,
    private readonly profileService: ProfileService,
  ) {
    super(grid, cdRef, overlay, vcr, translateService, restService, actionErrorDisplayService);
    this.$componentData = toSignal(this.userViewService.componentData$);
  }

  @ViewChild('templatePortalContent', { read: TemplateRef })
  templatePortalContent: TemplateRef<any>;

  @ViewChild('profileName', { read: ElementRef })
  profileName: ElementRef;

  panelItems: ListPanelItem[] = [];

  private _licenseAllowsUserCreation: boolean;

  ngOnInit() {
    this.initialisePanelItems();
    this.initialiseLicenseLock();
  }

  getCellText(profiles: Profile[]): string {
    if (profiles.length === 0) {
      return '';
    }

    const permissionGroup = this.row.data[GridColumnId.permissionGroup] as Profile;

    return this.profileService.getProfileName(permissionGroup);
  }

  change(newValue: any) {
    this.grid.beginAsyncOperation();
    this.userViewService
      .setUserAuthorisation([this.row.data[GridColumnId.projectId]], newValue)
      .pipe(finalize(() => this.grid.completeAsyncOperation()))
      .subscribe();
    this.close();
  }

  canEdit(): boolean {
    return this._licenseAllowsUserCreation;
  }

  showProfileList() {
    if (this.canEdit()) {
      this.showList(this.profileName, this.templatePortalContent, [
        {
          originX: 'start',
          overlayX: 'start',
          originY: 'bottom',
          overlayY: 'top',
          offsetX: -10,
          offsetY: 6,
        },
        {
          originX: 'start',
          overlayX: 'start',
          originY: 'top',
          overlayY: 'bottom',
          offsetX: -10,
          offsetY: -6,
        },
      ]);
    }
  }

  private initialiseLicenseLock(): void {
    this.adminReferentialDataService.licenseInformation$
      .pipe(
        takeUntil(this.unsub$),
        map(
          (licenseInfo) =>
            new LicenseInformationMessageProvider(licenseInfo, this.translateService),
        ),
      )
      .subscribe((messageHelper) => {
        this._licenseAllowsUserCreation = messageHelper.licenseInformation.allowCreateUsers;
        this.cdRef.detectChanges();
      });
  }

  private initialisePanelItems(): void {
    const profiles = this.$componentData().user.profiles;
    const options = this.profileService.retrieveProfilesAsDisplayOptions(profiles);

    // Sort options by locale label
    options.sort((a, b) => {
      return a.label.localeCompare(b.label);
    });

    this.panelItems = [...options];
  }
}

export function userAuthorisationProfileColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(UserAuthorisationProfileCellComponent)
    .withI18nKey('sqtm-core.administration-workspace.views.project.permissions.profile.label');
}
