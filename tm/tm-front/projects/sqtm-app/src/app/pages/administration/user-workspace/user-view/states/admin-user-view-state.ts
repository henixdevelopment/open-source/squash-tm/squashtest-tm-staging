import { GenericEntityViewState, provideInitialGenericViewState } from 'sqtm-core';
import { AdminUserState } from './admin-user-state';

export interface AdminUserViewState extends GenericEntityViewState<AdminUserState, 'user'> {
  user: AdminUserState;
}

export function provideInitialAdminUserView(): Readonly<AdminUserViewState> {
  return provideInitialGenericViewState<AdminUserState, 'user'>('user');
}
