import { ElementRef, Renderer2 } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

export abstract class AbstractPermissionsComponent {
  protected constructor(
    protected elementRef: ElementRef,
    protected translateService: TranslateService,
    protected renderer: Renderer2,
  ) {}

  protected formatCode(code: string) {
    return code.toLowerCase().replace(/_/g, '-');
  }

  protected getExplainMessage(
    formattedPanelCode: string,
    formattedPermissionName: string,
  ): boolean {
    const explainKey = `sqtm-core.administration-workspace.profiles.permission.${formattedPanelCode}.${formattedPermissionName}.explain`;
    let explainMessage = this.translateService.instant(explainKey);

    if (explainMessage === explainKey) {
      explainMessage = null; // no translation found, we do not show explain message
    }

    return explainMessage;
  }

  protected search(valueToSearch: string): void {
    this.filterSubPermissions(valueToSearch);
    this.filterPermissions(valueToSearch);
    this.filterWorkspaces();
  }

  protected abstract openAllPanels(): void;
  protected abstract closeAllPanels(): void;

  private filterSubPermissions(valueToSearch: string): void {
    this.elementRef.nativeElement
      .querySelectorAll('.sub-permission-label')
      .forEach((subPermissionLabel: HTMLElement) => {
        if (
          this.getSubPermissionSearchLabel(subPermissionLabel)
            .innerHTML.toLowerCase()
            .includes(valueToSearch.toLowerCase())
        ) {
          subPermissionLabel.parentElement.classList.remove('hide-permission');
        } else {
          subPermissionLabel.parentElement.classList.add('hide-permission');
        }
      });
  }

  private filterPermissions(valueToSearch: string): void {
    this.elementRef.nativeElement
      .querySelectorAll('.permission-label')
      .forEach((permissionLabel: HTMLElement) => {
        const nbVisible = this.countVisibleSubPermissions(permissionLabel);
        if (
          nbVisible === 0 &&
          !this.getPermissionSearchLabel(permissionLabel)
            .innerHTML.toLowerCase()
            .includes(valueToSearch.toLowerCase())
        ) {
          permissionLabel.parentElement.classList.add('hide-permission');
        } else {
          permissionLabel.parentElement.classList.remove('hide-permission');
        }
      });
  }

  private countVisibleSubPermissions(permissionLabel: HTMLElement): number {
    let nbVisible = 0;
    const parentPermission = permissionLabel.getAttribute('data-permission');
    this.elementRef.nativeElement
      .querySelectorAll(`[data-parent-permission='${parentPermission}']`)
      .forEach((label: HTMLElement) => {
        if (!label.parentElement.classList.contains('hide-permission')) {
          nbVisible++;
        }
      });
    return nbVisible;
  }

  private filterWorkspaces(): void {
    let nbVisibleItemsInWorkspaces = 0;
    this.elementRef.nativeElement
      .querySelectorAll('.workspace-panel')
      .forEach((workspace: HTMLElement) => {
        const nbVisible = this.countVisiblePermissions(workspace);
        nbVisibleItemsInWorkspaces += nbVisible;
        if (nbVisible === 0) {
          workspace.classList.add('hide-permission');
        } else {
          workspace.classList.remove('hide-permission');
        }
      });

    if (nbVisibleItemsInWorkspaces === 0) {
      this.renderer.removeClass(this.emptyTableMessage.nativeElement, 'hide-permission');
    } else {
      this.renderer.addClass(this.emptyTableMessage.nativeElement, 'hide-permission');
    }
  }

  private countVisiblePermissions(workspace: HTMLElement): number {
    let nbVisible = 0;
    const workspaceId = workspace.getAttribute('id');
    this.elementRef.nativeElement
      .querySelectorAll(`[data-workspace='${workspaceId}']`)
      .forEach((label: HTMLElement) => {
        if (!label.parentElement.classList.contains('hide-permission')) {
          nbVisible++;
        }
      });
    return nbVisible;
  }

  protected abstract get emptyTableMessage(): ElementRef;

  protected getSubPermissionSearchLabel(subPermissionLabel: Element): Element {
    return subPermissionLabel;
  }

  protected getPermissionSearchLabel(permissionLabel: Element): Element {
    return permissionLabel;
  }
}
