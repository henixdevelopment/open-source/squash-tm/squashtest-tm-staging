import { AfterViewInit, ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { TestAutomationServerViewService } from '../../../services/test-automation-server-view.service';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { EnvironmentSelectionPanelComponent } from '../../../../../../../components/squash-orchestrator/orchestrator-execution-environment/components/environment-selection-panel/environment-selection-panel.component';
import { AutomationEnvironmentTagHolder } from 'sqtm-core';

@Component({
  selector: 'sqtm-app-test-automation-server-environment-panel',
  templateUrl: './test-automation-server-environment-panel.component.html',
  styleUrls: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestAutomationServerEnvironmentPanelComponent implements AfterViewInit {
  readonly serverId$: Observable<number>;

  @ViewChild(EnvironmentSelectionPanelComponent)
  environmentSelectionPanelComponent: EnvironmentSelectionPanelComponent;

  public tagHolderType = AutomationEnvironmentTagHolder.TEST_AUTOMATION_SERVER;

  constructor(public readonly testAutomationServerViewService: TestAutomationServerViewService) {
    this.serverId$ = this.testAutomationServerViewService.componentData$.pipe(
      filter((state) => state.testAutomationServer?.supportsAutomatedExecutionEnvironments),
      map((state) => state.testAutomationServer.id),
    );
  }

  ngAfterViewInit(): void {
    this.connectServices();
    this.reconnectService();
  }

  private connectServices(): void {
    this.testAutomationServerViewService.externalRefreshRequired$
      .pipe(filter((refresh) => refresh.key === 'credentials'))
      .subscribe((refresh) =>
        this.environmentSelectionPanelComponent.handleServerCredentialsChange(
          refresh.value.registered,
        ),
      );

    this.environmentSelectionPanelComponent.panelService.selectedTagsChanged$.subscribe((newTags) =>
      this.testAutomationServerViewService.handleDefaultTagsChanged(newTags),
    );
  }

  private reconnectService(): void {
    const validKeys: string[] = ['baseUrl', 'observerUrl', 'eventBusUrl', 'killSwitchUrl'];
    this.testAutomationServerViewService.externalRefreshRequired$
      .pipe(filter((refresh) => validKeys.includes(refresh.key.toString())))
      .subscribe(() => this.environmentSelectionPanelComponent.refreshExecutionEnvironments());

    this.environmentSelectionPanelComponent.panelService.selectedTagsChanged$.subscribe((newTags) =>
      this.testAutomationServerViewService.handleDefaultTagsChanged(newTags),
    );
  }
}
