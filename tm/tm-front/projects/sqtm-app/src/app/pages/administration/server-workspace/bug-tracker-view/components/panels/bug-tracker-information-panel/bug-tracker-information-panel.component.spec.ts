import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BugTrackerInformationPanelComponent } from './bug-tracker-information-panel.component';
import { BugTrackerViewService } from '../../../services/bug-tracker-view.service';
import { of } from 'rxjs';
import {
  AdminBugTrackerState,
  AdminBugTrackerViewState,
} from '../../../states/admin-bug-tracker-view-state';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslatePipe } from '@ngx-translate/core';
import { AppTestingUtilsModule } from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import SpyObj = jasmine.SpyObj;

describe('BugTrackerInformationPanelComponent', () => {
  let component: BugTrackerInformationPanelComponent;
  let fixture: ComponentFixture<BugTrackerInformationPanelComponent>;
  const viewService: SpyObj<BugTrackerViewService> = jasmine.createSpyObj(['load']);
  const componentData: AdminBugTrackerViewState = {
    bugTracker: getInitialBugTracker(),
    type: 'bugTracker',
    uiState: null,
  };

  viewService['componentData$'] = of(componentData);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      declarations: [BugTrackerInformationPanelComponent],
      providers: [
        {
          provide: BugTrackerViewService,
          useValue: viewService,
        },
        {
          provide: TranslatePipe,
          useValue: jasmine.createSpyObj(['instant']),
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BugTrackerInformationPanelComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    component.componentData = componentData;
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(component.kindOptions).toEqual([
      {
        label: componentData.bugTracker.kind,
        value: componentData.bugTracker.kind,
        hide: true,
      },
    ]);
  });

  it('should not add hidden value if current bugtracker kind is handled', () => {
    component.componentData = {
      ...componentData,
      bugTracker: {
        ...componentData.bugTracker,
        bugTrackerKinds: [componentData.bugTracker.kind],
      },
    };
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(component.kindOptions).toEqual([
      {
        label: componentData.bugTracker.kind,
        value: componentData.bugTracker.kind,
        hide: false,
      },
    ]);
  });
});

function getInitialBugTracker(): AdminBugTrackerState {
  return {
    id: 1,
    kind: 'bugTracker',
    name: 'name',
    url: 'http://url.com',
    authPolicy: null,
    authProtocol: null,
    iframeFriendly: false,
    bugTrackerKinds: [],
    authConfiguration: null,
    credentials: null,
    supportedAuthenticationProtocols: [],
    attachmentList: null,
    createdBy: 'admin',
    createdOn: new Date('2024-02-14 15:00'),
  };
}
