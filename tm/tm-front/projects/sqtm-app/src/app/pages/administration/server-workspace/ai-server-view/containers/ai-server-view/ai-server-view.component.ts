import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, Signal } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import {
  GenericEntityViewService,
  GridService,
  ActionErrorDisplayService,
  DialogService,
  RestService,
} from 'sqtm-core';
import { Observable, Subject, switchMap } from 'rxjs';
import { AiServerViewService } from '../../services/ai-server-view.service';
import { catchError, concatMap, filter, map, take, takeUntil } from 'rxjs/operators';
import { AdminAiServerViewState } from '../../states/admin-ai-server-view-state';
import { toSignal } from '@angular/core/rxjs-interop';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-ai-server-view',
  templateUrl: './ai-server-view.component.html',
  styleUrl: './ai-server-view.component.less',
  providers: [
    {
      provide: AiServerViewService,
      useClass: AiServerViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: AiServerViewService,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AiServerViewComponent implements OnInit, OnDestroy {
  componentData$: Signal<AdminAiServerViewState>;

  private readonly unsub$ = new Subject<void>();

  constructor(
    private readonly route: ActivatedRoute,
    public readonly aiServerViewService: AiServerViewService,
    private readonly gridService: GridService,
    private dialogService: DialogService,
    private restService: RestService,
    private translateService: TranslateService,
    private readonly actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    this.componentData$ = toSignal(aiServerViewService.componentData$.pipe(takeUntil(this.unsub$)));
  }

  ngOnInit(): void {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('aiServerId')),
        switchMap((id) => this.aiServerViewService.load(parseInt(id, 10))),
      )
      .subscribe();

    this.prepareGridRefreshOnEntityChanges();
  }

  private prepareGridRefreshOnEntityChanges(): void {
    this.aiServerViewService.simpleAttributeRequiringRefresh = ['name', 'url', 'description'];

    this.aiServerViewService.externalRefreshRequired$
      .pipe(takeUntil(this.unsub$))
      .subscribe(() => this.gridService.refreshDataAndKeepSelectedRows());
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
    this.aiServerViewService.complete();
  }

  handleDelete() {
    this.showConfirmDeleteAiServerDialog(
      this.componentData$().aiServer.id,
      this.componentData$().aiServer.name,
    )
      .pipe(
        take(1),
        filter(({ confirmDelete }) => confirmDelete),
        concatMap(({ id }) => this.deleteAiServersServerSide(id)),
        catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
      )
      .subscribe(() => this.gridService.refreshData());
  }

  private showConfirmDeleteAiServerDialog(
    id: any,
    serverName: string,
  ): Observable<{ confirmDelete: boolean; id: any }> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.servers.ai.dialog.title.delete-one-title',
      messageKey: this.translateService.instant(
        'sqtm-core.administration-workspace.servers.ai.dialog.delete-one',
        { serverName: serverName },
      ),
      level: 'DANGER',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map((confirmDelete) => ({ confirmDelete, id })),
    );
  }

  private deleteAiServersServerSide(aiServerId): Observable<void> {
    return this.restService.delete([`ai-servers`, aiServerId]);
  }
}
