/*
 * All-pass dummy permissions used in grids where permissions don't apply.
 */
export class DummyPermissions {
  public readonly canRead = true;
  public readonly canWrite = true;
  public readonly canAttach = true;
  public readonly canCreate = true;
  public readonly canDelete = true;
  public readonly canExtendedDelete = true;
  public readonly canLink = true;
  public readonly canImport = true;
  public readonly canExport = true;
  public readonly canWriteAsFunctional = true;
  protected readonly permissions = [];

  protected checkPermission(/*permission: Permissions*/) {
    return true;
  }
}
