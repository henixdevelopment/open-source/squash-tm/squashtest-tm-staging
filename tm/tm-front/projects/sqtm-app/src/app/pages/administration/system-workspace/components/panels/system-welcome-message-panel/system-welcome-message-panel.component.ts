import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { SystemViewService } from '../../../services/system-view.service';
import { ActionErrorDisplayService, EditableTextFieldComponent } from 'sqtm-core';
import { catchError, finalize } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-system-welcome-message-panel',
  template: `
    <sqtm-core-editable-rich-text
      #welcomeMessageTextField
      [value]="(systemViewService.componentData$ | async).welcomeMessage"
      [focus]="true"
      (confirmEvent)="changeMessage($event)"
      [attr.data-test-field-id]="'welcomeMessage'"
    >
    </sqtm-core-editable-rich-text>
  `,
  styleUrls: ['./system-welcome-message-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SystemWelcomeMessagePanelComponent {
  @ViewChild('welcomeMessageTextField')
  welcomeMessageTextField: EditableTextFieldComponent;

  constructor(
    public readonly systemViewService: SystemViewService,
    private actionErrorDisplayService: ActionErrorDisplayService,
  ) {}

  changeMessage($event: string): void {
    this.systemViewService
      .setWelcomeMessage($event)
      .pipe(
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => this.welcomeMessageTextField.endAsync()),
      )
      .subscribe();
  }
}
