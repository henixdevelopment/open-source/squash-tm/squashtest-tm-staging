import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  Signal,
  ViewChild,
} from '@angular/core';
import {
  DialogReference,
  DisplayOption,
  FieldValidationError,
  GroupedMultiListFieldComponent,
  ListItem,
  RestService,
  SelectFieldComponent,
} from 'sqtm-core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProjectViewService } from '../../../services/project-view.service';
import { switchMap, take } from 'rxjs/operators';
import { AddPermissionDialogConfiguration } from './add-permission-dialog-configuration';
import { AdminProjectViewState } from '../../../state/admin-project-view-state';
import { ProfileService } from '../../../../../profile-workspace/profile-workspace/services/profile.service';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sqtm-app-add-permission-dialog',
  templateUrl: './add-permission-dialog.component.html',
  styleUrls: ['./add-permission-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: ProfileService,
      useClass: ProfileService,
    },
  ],
})
export class AddPermissionDialogComponent implements OnInit, AfterViewInit {
  public listItems: ListItem[] = [];
  public profileOptions: DisplayOption[];
  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  errorsOnMultiListField: string[] = [];
  data: AddPermissionDialogConfiguration;
  $componentData: Signal<AdminProjectViewState>;

  @ViewChild(GroupedMultiListFieldComponent)
  partyList: GroupedMultiListFieldComponent;

  @ViewChild(SelectFieldComponent)
  profileSelectField: SelectFieldComponent;

  constructor(
    private dialogReference: DialogReference<AddPermissionDialogConfiguration>,
    private restService: RestService,
    private cdr: ChangeDetectorRef,
    private fb: FormBuilder,
    private projectViewService: ProjectViewService,
    private profileService: ProfileService,
  ) {
    this.data = this.dialogReference.data;
    this.$componentData = toSignal(this.projectViewService.componentData$);
  }

  ngOnInit(): void {
    this.initializeFormGroup();
  }

  ngAfterViewInit(): void {
    this.prepareProfileSelectField();
    this.preparePartiesSelectField();
  }

  private preparePartiesSelectField() {
    this.projectViewService.componentData$
      .pipe(
        take(1),
        switchMap((componentData) => {
          const projectId = componentData.project.id.toString();

          return this.restService.get<UnboundPartiesResponse>([
            'generic-projects',
            projectId,
            'unbound-parties',
          ]);
        }),
      )
      .subscribe((response) => {
        const parties = this.retrievePartiesAsListItems(response);

        parties.sort((partyA, partyB) => partyA.label.localeCompare(partyB.label));

        this.listItems = [...parties];
        this.cdr.detectChanges();
      });
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      group: this.fb.control(null, [Validators.required]),
    });
  }

  private prepareProfileSelectField(): void {
    const profiles = this.$componentData().project.availableProfiles;
    const options = this.profileService.retrieveProfilesAsDisplayOptions(profiles);

    // Sort options by locale label
    options.sort((a, b) => {
      return a.label.localeCompare(b.label);
    });

    this.profileOptions = [...options];

    if (options.length > 0 && this.profileSelectField != null) {
      this.profileSelectField.disabled = false;
    }
  }

  private retrievePartiesAsListItems(response: UnboundPartiesResponse) {
    return response[this.data.permissionScope].map((party) => {
      return {
        id: party.id,
        label: party.label,
        selected: false,
      };
    });
  }

  selectedPartiesChanged($event: ListItem[]) {
    this.partyList.selectedItems = $event;
  }

  confirm(addAnother?: boolean) {
    if (this.formIsValid()) {
      this.doPost(addAnother);
    } else {
      this.showClientSideErrors();
    }
  }

  addAnother() {
    this.confirm(true);
  }

  private getSelectedPartyIds(): number[] {
    return this.partyList.selectedItems.map((party) => Number(party.id));
  }

  private doPost(addAnother?: boolean) {
    this.projectViewService.componentData$.pipe(take(1)).subscribe(() => {
      const groupId = this.formGroup.controls['group'].value;
      this.projectViewService
        .setPartyProjectPermissions(this.getSelectedPartyIds(), groupId)
        .subscribe();

      this.dialogReference.result = true;
      if (addAnother) {
        this.refreshDialog();
      } else {
        this.dialogReference.close();
      }
    });
  }

  refreshDialog() {
    this.partyList.listItems = this.partyList.listItems.filter(
      (item) => !this.partyList.selectedItems.includes(item),
    );
    this.listItems = [...this.partyList.listItems];
    this.partyList.selectedItems = [];
    this.serverSideValidationErrors = [];
    this.initializeFormGroup();
  }

  private formIsValid() {
    return this.formGroup.status === 'VALID' && this.partyList.selectedItems.length > 0;
  }

  private showClientSideErrors() {
    this.profileSelectField.showClientSideError();

    if (this.partyList.selectedItems.length === 0) {
      const requiredKey = 'sqtm-core.validation.errors.required';
      this.errorsOnMultiListField.push(requiredKey);
    }
  }
}

interface UnboundPartiesResponse {
  users: { id: string; label: string }[];
  teams: { id: string; label: string }[];
}
