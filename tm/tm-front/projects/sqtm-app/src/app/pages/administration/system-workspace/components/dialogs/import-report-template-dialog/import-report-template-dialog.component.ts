import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ReportTemplateImportComponentData } from '../../../states/report-template-import.state';
import { DialogReference, DocXReportId } from 'sqtm-core';
import { SystemViewService } from '../../../services/system-view.service';
import { ImportReportTemplateService } from '../../../services/import-report-template.service';

export const DOCX_TYPE = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';

@Component({
  selector: 'sqtm-app-import-report-template-dialog',
  templateUrl: './import-report-template-dialog.component.html',
  styleUrls: ['./import-report-template-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImportReportTemplateDialogComponent implements OnInit {
  componentData$: Observable<ReportTemplateImportComponentData>;
  data: ImportReportTemplateDialogConfiguration;
  wrongFileFormat: boolean;

  constructor(
    private importReportTemplateService: ImportReportTemplateService,
    private dialogReference: DialogReference<ImportReportTemplateDialogConfiguration>,
    private systemViewService: SystemViewService,
  ) {
    this.data = this.dialogReference.data;
  }

  ngOnInit(): void {
    this.componentData$ = this.importReportTemplateService.componentData$;
  }

  handleAdd(event$: MouseEvent, file: File) {
    event$.stopPropagation();
    if (file?.type === DOCX_TYPE) {
      this.importReportTemplateService
        .checkFileExistsInFolder(this.data.reportId, file.name)
        .subscribe((templateExists) => {
          if (!templateExists) {
            this.handleConfirm(file);
          }
        });
    } else {
      this.wrongFileFormat = true;
    }
  }

  handleConfirm(file: File) {
    this.importReportTemplateService
      .addTemplateInFolder(this.data.reportId, file)
      .subscribe((templateFileUploaded) => {
        if (templateFileUploaded) {
          this.systemViewService.addTemplateModels(file.name, this.data.reportId);
          this.importReportTemplateService.resetToInitialState();
          this.dialogReference.close();
        }
      });
  }

  addAttachment(files: File[]) {
    this.importReportTemplateService.saveFile(files[0]);
  }

  getRestrictionFiles() {
    return DOCX_TYPE;
  }

  getWrongFileKey() {
    return 'sqtm-core.administration-workspace.system.report-templates.dialog.add.wrong-format';
  }

  handleCancel() {
    this.importReportTemplateService.resetToInitialState();
  }
}

export class ImportReportTemplateDialogConfiguration {
  reportId: DocXReportId;
}
