import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import {
  ADMIN_WS_INFO_LISTS_TABLE,
  ADMIN_WS_INFO_LISTS_TABLE_CONFIG,
} from '../../../custom-workspace.constant';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import { adminInfoListsTableDefinition } from '../info-list-grid/info-list-grid.component';
import { Observable } from 'rxjs';

@Component({
  selector: 'sqtm-app-info-list-workspace',
  templateUrl: './info-list-workspace.component.html',
  styleUrls: ['./info-list-workspace.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: ADMIN_WS_INFO_LISTS_TABLE_CONFIG,
      useFactory: adminInfoListsTableDefinition,
      deps: [],
    },
    {
      provide: ADMIN_WS_INFO_LISTS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, ADMIN_WS_INFO_LISTS_TABLE_CONFIG, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: ADMIN_WS_INFO_LISTS_TABLE,
    },
  ],
})
export class InfoListWorkspaceComponent implements OnInit, OnDestroy {
  authenticatedAdmin$: Observable<AuthenticatedUser>;

  constructor(
    public readonly adminReferentialDataService: AdminReferentialDataService,
    private gridService: GridService,
  ) {}

  ngOnInit(): void {
    this.authenticatedAdmin$ = this.adminReferentialDataService.authenticatedUser$;
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }
}
