import {
  ChangeDetectionStrategy,
  Component,
  InjectionToken,
  OnDestroy,
  OnInit,
  Signal,
  ViewContainerRef,
} from '@angular/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { NzIconModule } from 'ng-zorro-antd/icon';
import {
  AdminReferentialDataService,
  dateColumn,
  dateTimeColumn,
  dateWithExpiredInfoColumn,
  deleteColumn,
  DialogService,
  Extendable,
  Fixed,
  GridColumnId,
  GridDefinition,
  GridModule,
  GridService,
  indexColumn,
  smallGrid,
  StyleDefinitionBuilder,
  textColumn,
  WorkspaceLayoutModule,
} from 'sqtm-core';
import { Subject } from 'rxjs';
import { buildCreateApiTokenDialogConfiguration } from '../../../../../../user-account/components/dialogs/create-api-token-dialog/create-api-token-dialog.component';
import { filter, takeUntil } from 'rxjs/operators';
import { UserViewService } from '../../../services/user-view.service';
import { API_TOKEN_GENERATOR_TOKEN } from '../../../../../../user-account/components/dialogs/create-api-token-dialog/api-token-generator';
import { toSignal } from '@angular/core/rxjs-interop';
import { buildDisplayNewApiTokenDialogConfiguration } from '../../../../../../user-account/components/dialogs/display-new-api-token-dialog/display-new-api-token-dialog.component';
import { buildApiTokenPermissionsRowConverter } from '../../../../../../user-account/containers/user-account-view/user-account-view.component';
import { AdminUserViewComponentData } from '../../../containers/user-view/user-view.component';
import { DeleteTestAutoServerApiTokenCellRendererComponent } from '../../cell-renderers/delete-personal-api-token-cell-renderer/delete-test-auto-server-api-token-cell-renderer.component';

export const USER_VIEW_API_TOKEN_TABLE_CONF = new InjectionToken('USER_VIEW_API_TOKEN_TABLE_CONF');
export const USER_VIEW_API_TOKEN_TABLE = new InjectionToken('USER_VIEW_API_TOKEN_TABLE');

export function userViewApiTokenTableDefinition(
  translateService: TranslateService,
): GridDefinition {
  return smallGrid('user-view-api-token-grid')
    .withColumns([
      indexColumn(),
      textColumn(GridColumnId.name)
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5)),
      textColumn(GridColumnId.permissions)
        .withI18nKey('sqtm-core.user-account-page.api-token.grid.permissions')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5)),
      dateColumn(GridColumnId.createdOn)
        .withI18nKey('sqtm-core.entity.generic.created-on.masculine')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5)),
      dateTimeColumn(GridColumnId.lastUsage)
        .withI18nKey('sqtm-core.user-account-page.api-token.grid.last-usage')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5)),
      textColumn(GridColumnId.createdBy)
        .withI18nKey('sqtm-core.entity.generic.created-by.masculine')
        .changeWidthCalculationStrategy(new Fixed(100)),
      dateWithExpiredInfoColumn(GridColumnId.expiryDate)
        .withI18nKey('sqtm-core.user-account-page.api-token.grid.expiry-date')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5)),
      deleteColumn(DeleteTestAutoServerApiTokenCellRendererComponent),
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .withRowConverter(buildApiTokenPermissionsRowConverter(translateService))
    .disableMultiSelection()
    .server()
    .build();
}

@Component({
  selector: 'sqtm-app-user-api-token-panel',
  template: `<sqtm-core-grid sqtmCoreTreeKeyboardShortcut></sqtm-core-grid>`,
  standalone: true,
  providers: [
    {
      provide: GridService,
      useExisting: USER_VIEW_API_TOKEN_TABLE,
    },
    {
      provide: API_TOKEN_GENERATOR_TOKEN,
      useExisting: UserViewService,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [GridModule, NzButtonModule, TranslateModule, NzIconModule, WorkspaceLayoutModule],
})
export class UserApiTokenPanelComponent implements OnInit, OnDestroy {
  private unsub$ = new Subject<void>();
  $isJwtSecretDefined: Signal<boolean> = toSignal(
    this.adminReferentialDataService.isJwtSecretDefined$,
  );
  $componentData: Signal<AdminUserViewComponentData> = toSignal(
    this.userViewService.componentData$,
  );

  constructor(
    private readonly gridService: GridService,
    private readonly dialogService: DialogService,
    private readonly adminReferentialDataService: AdminReferentialDataService,
    private readonly vcr: ViewContainerRef,
    private readonly userViewService: UserViewService,
  ) {}

  ngOnInit() {
    this.gridService.setServerUrl([
      'user-view',
      this.$componentData().user.id.toString(),
      'api-tokens',
    ]);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  createApiToken() {
    if (this.$isJwtSecretDefined()) {
      this.doOpenCreateApiTokenDialog();
    } else {
      this.dialogService.openAlert({
        level: 'DANGER',
        messageKey: 'sqtm-core.user-account-page.api-token.no-jwt-secret',
      });
    }
  }

  private doOpenCreateApiTokenDialog() {
    const dialogReference = this.dialogService.openDialog(
      buildCreateApiTokenDialogConfiguration(this.vcr),
    );

    dialogReference.dialogResultChanged$
      .pipe(
        takeUntil(dialogReference.dialogClosed$),
        filter((result) => result != null),
      )
      .subscribe((token: string) => {
        this.gridService.refreshData();
        this.openShowNewTokenDialog(token);
      });
  }

  private openShowNewTokenDialog(token: string) {
    this.dialogService.openDialog(buildDisplayNewApiTokenDialogConfiguration(token, this.vcr));
  }
}
