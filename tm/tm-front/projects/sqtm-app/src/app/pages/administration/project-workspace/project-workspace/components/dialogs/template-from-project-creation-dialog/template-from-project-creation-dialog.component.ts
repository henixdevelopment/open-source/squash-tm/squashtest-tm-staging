import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  Signal,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  AdminReferentialDataService,
  CreationDialogData,
  DialogReference,
  FieldValidationError,
  NOT_ONLY_SPACES_REGEX,
  RestService,
} from 'sqtm-core';
import { AbstractAdministrationCreationDialogDirective } from '../../../../../components/abstract-administration-creation-dialog';
import { of } from 'rxjs';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sqtm-app-template-from-project-creation-dialog',
  templateUrl: './template-from-project-creation-dialog.component.html',
  styleUrls: ['./template-from-project-creation-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TemplateFromProjectCreationDialogComponent
  extends AbstractAdministrationCreationDialogDirective
  implements OnInit
{
  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  data: TemplateFromProjectCreationDialogData;
  isUltimate$: Signal<boolean>;

  constructor(
    private fb: FormBuilder,
    public readonly dialogReference: DialogReference,
    public readonly restService: RestService,
    public readonly cdr: ChangeDetectorRef,
    public readonly adminRefDataService: AdminReferentialDataService,
  ) {
    super('project-templates/new', dialogReference, restService, cdr);
    this.data = this.dialogReference.data;
    this.isUltimate$ = toSignal(adminRefDataService.isUltimateLicenseAvailable$);
  }

  get textFieldToFocus(): string {
    return 'name';
  }

  ngOnInit() {
    // Prepare form group
    this.formGroup = this.fb.group({
      name: this.fb.control('', [
        Validators.required,
        Validators.pattern(NOT_ONLY_SPACES_REGEX),
        Validators.maxLength(255),
      ]),
      description: this.fb.control(''),
      label: this.fb.control('', [Validators.maxLength(255)]),
      keepPermissions: this.fb.control(true),
      keepCustomFields: this.fb.control(true),
      keepInfoLists: this.fb.control(true),
      keepBugtracker: this.fb.control(true),
      keepAiServer: this.fb.control(true),
      keepAutomation: this.fb.control(true),
      keepMilestones: this.fb.control(true),
      keepAllowTcModificationsFromExecution: this.fb.control(true),
      keepOptionalExecutionStatuses: this.fb.control(true),
      keepPlugins: this.fb.control(true),
      keepPluginsConfiguration: this.fb.control(true),
    });

    this.refreshControlsActivation();
  }

  protected getRequestPayload() {
    const formValues = this.formGroup.getRawValue();

    return of({
      name: formValues.name,
      description: formValues.description,
      label: formValues.label,
      copyPermissions: formValues.keepPermissions,
      copyCUF: formValues.keepCustomFields,
      copyInfolists: formValues.keepInfoLists,
      copyBugtrackerBinding: formValues.keepBugtracker,
      copyAiServerBinding: formValues.keepAiServer,
      copyAutomatedProjects: formValues.keepAutomation,
      copyMilestone: formValues.keepMilestones,
      copyAllowTcModifFromExec: formValues.keepAllowTcModificationsFromExecution,
      copyOptionalExecStatuses: formValues.keepOptionalExecutionStatuses,
      copyPluginsActivation: formValues.keepPlugins,
      templateId: this.data.projectId,
      copyPluginsConfiguration: formValues.keepPluginsConfiguration,
    });
  }

  protected doResetForm(): void {
    const defaultValues = {
      name: '',
      description: '',
      label: '',
      keepPermissions: true,
      keepCustomFields: true,
      keepInfoLists: true,
      keepBugtracker: true,
      keepAiServer: true,
      keepAutomation: true,
      keepMilestones: true,
      keepAllowTcModificationsFromExecution: true,
      keepOptionalExecutionStatuses: true,
      keepPlugins: true,
      keepPluginsConfiguration: true,
    };

    Object.keys(defaultValues).forEach((key) => {
      this.formGroup.get(key).reset(defaultValues[key]);
    });
  }

  private refreshControlsActivation(): void {
    const keepPluginBindings = this.formGroup.get('keepPluginsConfiguration').value;
    this.handleKeepPluginsConfigurationChange(keepPluginBindings);
  }

  handleKeepPluginsConfigurationChange(keepPluginsConfiguration: boolean): void {
    if (keepPluginsConfiguration) {
      this.toggleControl('keepPlugins');
      this.toggleControl('keepCustomFields');
      this.toggleControl('keepInfoLists');
    } else {
      this.formGroup.get('keepPlugins').enable();
      this.formGroup.get('keepCustomFields').enable();
      this.formGroup.get('keepInfoLists').enable();
    }
  }

  private toggleControl(control: string): void {
    this.formGroup.get(control).reset(true);
    this.formGroup.get(control).disable();
  }
}

export interface TemplateFromProjectCreationDialogData extends CreationDialogData {
  id: string;
  titleKey: string;
  projectName: string;
  projectId: number;
}
