export interface ChangeExecStatusUsedDialogConfiguration {
  selectedStatus: string;
}

export const CHANGE_EXEC_STATUS_USED_DIALOG_ID = 'change-execution-status-in-use-dialog';
