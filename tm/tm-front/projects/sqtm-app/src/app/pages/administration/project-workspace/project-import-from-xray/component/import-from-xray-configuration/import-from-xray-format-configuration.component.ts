import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { ImportFromXrayState } from '../../state/import-from-xray.state';
import { ImportFromXrayService } from '../../services/ImportFromXrayService';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TextFieldComponent, NOT_ONLY_SPACES_REGEX } from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';

const XRAY_IMPORT_DOC_URL_EN =
  'https://tm-en.doc.squashtest.com/latest/admin-guide/manage-projects/xray-import.html?utm_source=Appli_squash&utm_medium=link';
const XRAY_IMPORT_DOC_URL_FR =
  'https://tm-fr.doc.squashtest.com/latest/admin-guide/gestion-projets/import-xray.html?utm_source=Appli_squash&utm_medium=link';

@Component({
  selector: 'sqtm-app-import-from-xray-configuration',
  templateUrl: './import-from-xray-format-configuration.component.html',
  styleUrl: './import-from-xray-format-configuration.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImportFromXrayFormatConfigurationComponent implements OnInit {
  formGroup: FormGroup;

  @ViewChildren(TextFieldComponent)
  textFields: QueryList<TextFieldComponent>;

  @Input({ required: true }) componentData: ImportFromXrayState;

  constructor(
    private fb: FormBuilder,
    private importFromXrayService: ImportFromXrayService,
    public readonly translateService: TranslateService,
  ) {}

  ngOnInit(): void {
    this.initializeFormGroup();
  }

  private initializeFormGroup() {
    let initialImportName = '';
    if (this.componentData.importName) {
      initialImportName = this.componentData.importName;
    }
    this.formGroup = this.fb.group({
      importName: this.fb.control(initialImportName, [
        Validators.required,
        Validators.pattern(NOT_ONLY_SPACES_REGEX),
        Validators.maxLength(255),
      ]),
    });
  }

  addFile(files: File[]) {
    this.importFromXrayService.addFiles(files);
  }

  removeFile(file: File) {
    this.importFromXrayService.removeFile(file);
  }

  importNameChanged() {
    if (this.isValidForm()) {
      const value = this.formGroup.get('importName').value || '';
      this.importFromXrayService.addImportName(value.trim());
    }
  }

  isValidForm(): boolean {
    this.showClientSideErrors();
    return this.formGroup.status === 'VALID';
  }

  showClientSideErrors() {
    this.textFields.forEach((textField) => textField.showClientSideError());
  }

  get xrayImportDocUrl(): string {
    return this.translateService.getBrowserLang() === 'fr'
      ? XRAY_IMPORT_DOC_URL_FR
      : XRAY_IMPORT_DOC_URL_EN;
  }
}
