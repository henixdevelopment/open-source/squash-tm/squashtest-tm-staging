import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-import-type-cell',
  template: ` @if (columnDisplay && row) {
    <div class="full-width full-height flex-column">
      <span
        class="m-auto-0"
        nz-tooltip
        [sqtmCoreLabelTooltip]="cellText | translate"
        [ellipsis]="true"
      >
        {{ cellText | translate }}
      </span>
    </div>
  }`,
  styleUrl: './import-type-cell.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImportTypeCellComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }

  get cellText(): string {
    return `sqtm-core.entity.pivot-format-import.type.${this.row.data[this.columnDisplay.id]}`;
  }
}

export function importTypeColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(ImportTypeCellComponent);
}
