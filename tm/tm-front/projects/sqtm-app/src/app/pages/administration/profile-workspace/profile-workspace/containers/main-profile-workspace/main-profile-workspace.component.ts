import { ChangeDetectionStrategy, Component, Input, OnInit, Signal } from '@angular/core';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  gridServiceFactory,
  GridService,
  GRID_PERSISTENCE_KEY,
  RestService,
  ReferentialDataService,
  WorkspaceWithGridComponent,
} from 'sqtm-core';
import {
  ADMIN_WS_PROFILE_TABLE,
  ADMIN_WS_PROFILE_TABLE_CONFIG,
} from '../../../profile-workspace.constant';
import { adminProfileTableDefinition } from '../profile-grid/profile-grid.component';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sqtm-app-main-profile-workspace',
  templateUrl: './main-profile-workspace.component.html',
  styleUrls: ['./main-profile-workspace.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: ADMIN_WS_PROFILE_TABLE_CONFIG,
      useFactory: adminProfileTableDefinition,
      deps: [],
    },
    {
      provide: ADMIN_WS_PROFILE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, ADMIN_WS_PROFILE_TABLE_CONFIG, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: ADMIN_WS_PROFILE_TABLE,
    },
    {
      provide: GRID_PERSISTENCE_KEY,
      useValue: 'profile-workspace-main-grid',
    },
    {
      provide: WorkspaceWithGridComponent,
      useValue: {},
    },
  ],
})
export class MainProfileWorkspaceComponent implements OnInit {
  @Input()
  baseUrl: string;

  $authenticatedUser: Signal<AuthenticatedUser>;

  constructor(private adminReferentialDataService: AdminReferentialDataService) {
    this.$authenticatedUser = toSignal(this.adminReferentialDataService.authenticatedUser$);
  }

  ngOnInit(): void {
    this.adminReferentialDataService.refresh().subscribe();
  }
}
