import { AdminCustomFieldState } from './admin-custom-field-state';
import { GenericEntityViewState, provideInitialGenericViewState } from 'sqtm-core';
import { InjectionToken } from '@angular/core';

export interface AdminCustomFieldViewState
  extends GenericEntityViewState<AdminCustomFieldState, 'customField'> {
  customField: AdminCustomFieldState;
}

export function provideInitialAdminCustomFieldView(): Readonly<AdminCustomFieldViewState> {
  return provideInitialGenericViewState<AdminCustomFieldState, 'customField'>('customField');
}

export const PROJECT_BOUND_TO_CUF = new InjectionToken('PROJECT_BOUND_TO_CUF');
export const PROJECT_BOUND_TO_CUF_TABLE = new InjectionToken('PROJECT_BOUND_TO_CUF_TABLE');
