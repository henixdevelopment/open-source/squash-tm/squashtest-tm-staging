import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'sqtm-app-import-from-xray-error-dialog',
  templateUrl: './import-from-xray-error-dialog.component.html',
  styleUrl: './import-from-xray-error-dialog.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class ImportFromXrayErrorDialogComponent {}
