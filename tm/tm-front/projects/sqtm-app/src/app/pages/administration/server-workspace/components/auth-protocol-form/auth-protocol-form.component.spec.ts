import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AuthProtocolFormComponent } from './auth-protocol-form.component';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthenticationProtocol, RestService } from 'sqtm-core';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('AuthProtocolFormComponent', () => {
  let component: AuthProtocolFormComponent;
  let fixture: ComponentFixture<AuthProtocolFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), ReactiveFormsModule],
      providers: [
        {
          provide: RestService,
          useValue: {
            backendRootUrl: '',
            backendContextPath: '',
          },
        },
      ],
      declarations: [AuthProtocolFormComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthProtocolFormComponent);
    component = fixture.componentInstance;

    component.supportedAuthenticationProtocols = [
      AuthenticationProtocol.BASIC_AUTH,
      AuthenticationProtocol.OAUTH_2,
    ];
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should handle save error', waitForAsync(() => {
    fixture.detectChanges();

    component.handleServerError();

    expect(component['hasSaved']).toBeFalsy();
    expect(component['hasServerError']).toBeTruthy();
  }));

  it('should emit event when auth protocol changed', waitForAsync(() => {
    fixture.detectChanges();
    spyOn(component.authProtocolChanged, 'emit');

    component.confirmAuthProtocol({ value: 'OAUTH_2', label: 'OAuth 2' });

    expect(component.authProtocolChanged.emit).toHaveBeenCalled();
  }));
});
