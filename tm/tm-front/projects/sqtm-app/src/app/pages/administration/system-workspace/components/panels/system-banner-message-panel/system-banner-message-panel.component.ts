import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { SystemViewService } from '../../../services/system-view.service';
import { catchError, finalize } from 'rxjs/operators';
import {
  ActionErrorDisplayService,
  AdminReferentialDataService,
  EditableTextFieldComponent,
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-system-banner-message-panel',
  template: `
    <sqtm-core-editable-rich-text
      #bannerMessageTextField
      [value]="(systemViewService.componentData$ | async).bannerMessage"
      [focus]="true"
      (confirmEvent)="changeMessage($event)"
      [attr.data-test-field-id]="'bannerMessage'"
    >
    </sqtm-core-editable-rich-text>
  `,
  styleUrl: './system-banner-message-panel.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SystemBannerMessagePanelComponent {
  @ViewChild(EditableTextFieldComponent)
  bannerMessageTextField: EditableTextFieldComponent;

  constructor(
    public readonly systemViewService: SystemViewService,
    private readonly adminReferentialDataService: AdminReferentialDataService,
    private readonly actionErrorDisplayService: ActionErrorDisplayService,
  ) {}

  changeMessage($event: string): void {
    this.systemViewService
      .setBannerMessage($event)
      .pipe(
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => this.bannerMessageTextField.endAsync()),
      )
      .subscribe(() => {
        this.adminReferentialDataService.refresh().subscribe();
      });
  }
}
