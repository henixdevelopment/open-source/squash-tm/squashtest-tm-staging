import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  isDevMode,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NzDropdownMenuComponent } from 'ng-zorro-antd/dropdown';
import {
  AttachmentListState,
  createFieldSelector,
  DialogService,
  EditableGenericEntityFieldState,
  EditableTextFieldComponent,
  GenericEntityViewService,
  getPersistedAttachmentCount,
  SqtmGenericEntityState,
} from 'sqtm-core';
import { Observable, Subject } from 'rxjs';
import { filter, map, take, takeUntil } from 'rxjs/operators';
import { select } from '@ngrx/store';

@Component({
  selector: 'sqtm-app-admin-view-header',
  templateUrl: './admin-view-header.component.html',
  styleUrls: ['./admin-view-header.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminViewHeaderComponent<E extends SqtmGenericEntityState, T extends string>
  implements OnInit, OnDestroy
{
  @Input() entityNameKey: keyof E;
  @Input() editableTitle = true;

  @Input() nameChangeConfirmationMessageKey: string;
  @Input() showsNameChangeConfirmDialog: boolean;
  @Output() nameChanged = new EventEmitter<void>();

  @Input() showMenu: boolean;
  @Input() dropdownMenu: NzDropdownMenuComponent;

  @Input() hasAttachments = false;
  @Input() attachmentList: AttachmentListState;
  @Output() attachmentBadgeClickEvent = new EventEmitter<void>();

  @ViewChild('entityNameField') entityNameField: EditableTextFieldComponent;

  entityName$: Observable<E[keyof E]>;

  unsub$ = new Subject<void>();

  get attachmentCount(): number {
    if (this.attachmentList?.attachments) {
      return getPersistedAttachmentCount(this.attachmentList.attachments);
    } else {
      return 0;
    }
  }

  constructor(
    private readonly genericViewService: GenericEntityViewService<E, T>,
    private readonly dialogService: DialogService,
  ) {}

  ngOnInit(): void {
    this.entityName$ = this.genericViewService.componentData$.pipe(
      map((componentData) => this.genericViewService.getEntity(componentData)[this.entityNameKey]),
    );

    this.observeErrors();
    this.observeSuccess();
  }

  attachmentBadgeClick() {
    this.attachmentBadgeClickEvent.next();
  }

  handleNameChange($event: string): void {
    if (this.entityNameField.value === $event) {
      this.entityNameField.value = $event;
      return;
    }

    if (this.showsNameChangeConfirmDialog) {
      this.openConfirmDialog($event);
    } else {
      this.genericViewService.update(this.entityNameKey, $event as any);
    }
  }

  private openConfirmDialog($event: string) {
    if (this.nameChangeConfirmationMessageKey == null) {
      if (isDevMode()) {
        throw new Error('You forgot to give your confirmation dialog a message !');
      } else {
        return;
      }
    }

    const dialogRef = this.dialogService.openConfirm({
      id: 'confirm-change-current-user-login',
      messageKey: this.nameChangeConfirmationMessageKey,
    });

    dialogRef.dialogClosed$.subscribe((result) => {
      if (result) {
        this.genericViewService.update(this.entityNameKey, $event as any);
      } else {
        this.entityName$
          .pipe(take(1))
          .subscribe((currentName: any) => (this.entityNameField.value = currentName));
      }
    });
  }

  private observeErrors() {
    this.genericViewService.componentData$
      .pipe(
        takeUntil(this.unsub$),
        select(createFieldSelector(this.entityNameKey as string)),
        filter(
          (fieldState: EditableGenericEntityFieldState<E>) =>
            Boolean(fieldState) && fieldState.status === 'SERVER_ERROR',
        ),
      )
      .subscribe((fieldState: EditableGenericEntityFieldState<E>) =>
        this.entityNameField.showExternalErrorMessage(fieldState.errorMsg),
      );
  }

  private observeSuccess() {
    this.genericViewService.operationSuccessful$.pipe(takeUntil(this.unsub$)).subscribe(() => {
      if (this.showsNameChangeConfirmDialog) {
        this.nameChanged.emit();
      }
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
