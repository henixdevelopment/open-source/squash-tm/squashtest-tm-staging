import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AbstractAdministrationCreationDialogDirective } from '../../../../../components/abstract-administration-creation-dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  AdminReferentialDataService,
  CreationDialogData,
  DialogReference,
  FieldValidationError,
  RestService,
} from 'sqtm-core';
import { of } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-ai-server-creation-dialog',
  templateUrl: './ai-server-creation-dialog.component.html',
  styleUrl: './ai-server-creation-dialog.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AiServerCreationDialogComponent
  extends AbstractAdministrationCreationDialogDirective
  implements OnInit
{
  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  data: CreationDialogData;

  constructor(
    private fb: FormBuilder,
    dialogReference: DialogReference,
    restService: RestService,
    cdr: ChangeDetectorRef,
    public readonly adminReferentialDataService: AdminReferentialDataService,
  ) {
    super('ai-servers/new', dialogReference, restService, cdr);
  }

  ngOnInit() {
    this.adminReferentialDataService.adminReferentialData$
      .pipe(take(1))
      .subscribe(() => this.initializeFormGroup());
  }

  get textFieldToFocus(): string {
    return 'name';
  }

  protected doResetForm() {
    this.resetFormControl('name', '');
    this.resetFormControl('baseUrl', '');
    this.resetFormControl('description', '');
  }

  protected getRequestPayload() {
    return of({
      name: this.getFormControlValue('name'),
      baseUrl: this.getFormControlValue('baseUrl'),
      description: this.getFormControlValue('description'),
    });
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      name: this.fb.control('', [Validators.required, Validators.maxLength(50)]),
      baseUrl: this.fb.control('', [Validators.required, Validators.maxLength(255)]),
      description: this.fb.control(''),
    });
  }
}
