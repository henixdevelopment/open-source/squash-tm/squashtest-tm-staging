import { DeleteAutomatedSuitesAndExecutionConfiguration } from '../../../../../system-workspace/components/dialogs/delete-automated-suites-and-execution-dialog/delete-automated-suites-and-execution-configuration';

export class CleanAutomatedSuitesAndExecutionProjectConfiguration extends DeleteAutomatedSuitesAndExecutionConfiguration {
  projectId: string;
  cleaningType: string;
}

export enum CleaningTypes {
  AUTOMATED_SUITE = 'automated-suite',
  COMPLETE_PRUNE = 'complete-prune',
  PARTIAL_PRUNE = 'partial-prune',
}
