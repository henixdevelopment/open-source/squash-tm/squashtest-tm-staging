import { ProfileView } from 'sqtm-core';

export interface TransferAuthorizationsDialogConfiguration {
  profile: ProfileView;
}
