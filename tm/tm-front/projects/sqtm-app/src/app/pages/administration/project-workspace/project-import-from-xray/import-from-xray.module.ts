import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImportFromXrayDialogComponent } from './containers/import-from-xray/import-from-xray-dialog.component';
import { ImportFromXrayFormatConfigurationComponent } from './component/import-from-xray-configuration/import-from-xray-format-configuration.component';
import { ImportFromXrayConfirmationComponent } from './component/import-from-xray-confirmation/import-from-xray-confirmation.component';
import { ImportFromXrayGeneratePivotComponent } from './component/import-from-xray-generate-pivot/import-from-xray-generate-pivot.component';
import {
  AttachmentModule,
  DialogModule,
  NavBarModule,
  SvgModule,
  WorkspaceCommonModule,
  WorkspaceLayoutModule,
} from 'sqtm-core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ImportFromXrayImportComponent } from './component/import-from-xray-import/import-from-xray-import.component';
import { ImportFromXrayErrorDialogComponent } from './shared/import-from-xray-error-dialog/import-from-xray-error-dialog.component';

@NgModule({
  declarations: [
    ImportFromXrayDialogComponent,
    ImportFromXrayFormatConfigurationComponent,
    ImportFromXrayConfirmationComponent,
    ImportFromXrayGeneratePivotComponent,
    ImportFromXrayImportComponent,
    ImportFromXrayErrorDialogComponent,
  ],
  imports: [
    CommonModule,
    SvgModule,
    WorkspaceCommonModule,
    NavBarModule,
    NzMenuModule,
    NzIconModule,
    NzToolTipModule,
    NzPopoverModule,
    NzInputModule,
    DialogModule,
    WorkspaceLayoutModule,
    NzDropDownModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    NzButtonModule,
    FormsModule,
    NzSelectModule,
    AttachmentModule,
  ],
})
export class ImportFromXrayModule {}
