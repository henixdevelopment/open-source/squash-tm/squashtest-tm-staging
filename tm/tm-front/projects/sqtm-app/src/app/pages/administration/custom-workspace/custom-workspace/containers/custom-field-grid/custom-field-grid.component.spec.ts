import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {
  adminCustomFieldTableDefinition,
  CustomFieldGridComponent,
} from './custom-field-grid.component';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  DataRow,
  DialogModule,
  DialogService,
  GridService,
  GridTestingModule,
  RestService,
  WorkspaceWithGridComponent,
} from 'sqtm-core';
import {
  ADMIN_WS_CUSTOM_FIELD_TABLE,
  ADMIN_WS_CUSTOM_FIELD_TABLE_CONFIG,
} from '../../../custom-workspace.constant';
import { AppTestingUtilsModule } from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {
  mockClosableDialogService,
  mockGridService,
  mockRestService,
  mockRouter,
} from '../../../../../../utils/testing-utils/mocks.service';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import createSpyObj = jasmine.createSpyObj;

describe('CustomFieldGridComponent', () => {
  let component: CustomFieldGridComponent;
  let fixture: ComponentFixture<CustomFieldGridComponent>;

  const gridService = mockGridService();
  const restService = mockRestService();
  const dialogMock = mockClosableDialogService();
  const dialogService = dialogMock.service;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [CustomFieldGridComponent],
      imports: [RouterTestingModule, AppTestingUtilsModule, GridTestingModule, DialogModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: ADMIN_WS_CUSTOM_FIELD_TABLE_CONFIG,
          useFactory: adminCustomFieldTableDefinition,
        },
        {
          provide: ADMIN_WS_CUSTOM_FIELD_TABLE,
          useValue: gridService,
        },
        {
          provide: GridService,
          useValue: gridService,
        },
        {
          provide: DialogService,
          useValue: dialogService,
        },
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: Router,
          useValue: mockRouter(),
        },
        {
          provide: WorkspaceWithGridComponent,
          useValue: {},
        },
        {
          provide: ActivatedRoute,
          useValue: { snapshot: { paramMap: createSpyObj<Map<any, any>>(['get']) } },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomFieldGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    dialogMock.resetCalls();
    dialogMock.resetSubjects();
    restService.delete.calls.reset();
    restService.delete.and.returnValue(of({}));
    gridService.refreshData.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open creation dialog', () => {
    component.openCreateDialog();
    dialogMock.closeDialogsWithResult(true);

    expect(dialogService.openDialog).toHaveBeenCalled();
    expect(gridService.refreshDataAsync).toHaveBeenCalled();
  });

  it('should delete CUFs', () => {
    gridService.selectedRows$ = of([
      { data: { cfId: 1 } } as unknown as DataRow,
      { data: { cfId: 12 } } as unknown as DataRow,
    ]);

    component.deleteCustomFields();
    dialogMock.closeDialogsWithResult(true);

    expect(dialogService.openDeletionConfirm).toHaveBeenCalled();
    expect(gridService.refreshData).toHaveBeenCalled();
    expect(restService.delete).toHaveBeenCalledWith(['custom-fields', '1,12']);
  });
});
