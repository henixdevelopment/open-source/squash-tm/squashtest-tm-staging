import { Profile, SqtmGenericEntityState } from 'sqtm-core';
import { ProjectPermission } from '../../user-view/states/admin-user-state';

export interface AdminTeamState extends SqtmGenericEntityState {
  id: number;
  name: string;
  description: string;
  createdOn: Date;
  createdBy: string;
  lastModifiedBy: string;
  lastModifiedOn: Date;
  projectPermissions: ProjectPermission[];
  profiles: Profile[];
  members: Member[];
}

export interface Member {
  partyId: number;
  active: boolean;
  firstName: string;
  lastName: string;
  login: string;
  fullName: string;
}
