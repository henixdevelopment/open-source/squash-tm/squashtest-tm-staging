import { ChangeDetectionStrategy, Component, OnInit, Signal } from '@angular/core';
import { AdminReferentialDataService, DialogService, WorkspaceWithGridComponent } from 'sqtm-core';
import { toSignal } from '@angular/core/rxjs-interop';
import { filter, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-administration-servers-anchors',
  templateUrl: './admin-servers-anchors.component.html',
  styleUrls: ['./admin-servers-anchors.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminServersAnchorsComponent implements OnInit {
  readonly isUltimate$: Signal<boolean>;
  routerLinkPath: string;

  constructor(
    private workspaceWithGrid: WorkspaceWithGridComponent,
    protected adminReferentialDataService: AdminReferentialDataService,
    protected dialogService: DialogService,
  ) {
    this.isUltimate$ = toSignal(adminReferentialDataService.isUltimateLicenseAvailable$);
  }

  ngOnInit(): void {
    this.routerLinkPath = this.isUltimate$() ? '../ai-server' : '../';
  }

  hideContextualContent() {
    this.workspaceWithGrid.switchToNoRowLayout();
  }

  navigateIfUltimate() {
    this.isUltimate$() ? this.hideContextualContent() : this.openNeedsUltimateLicenseDialog();
  }

  openNeedsUltimateLicenseDialog() {
    const dialogReference = this.dialogService.openAlert({
      id: 'ai-server-grid-needs-ultimate',
      level: 'INFO',
      titleKey: 'sqtm-core.dialog.needs-ultimate.ai-feature.title',
      messageKey: 'sqtm-core.dialog.needs-ultimate.ai-feature.message',
    });

    dialogReference.dialogResultChanged$
      .pipe(
        takeUntil(dialogReference.dialogClosed$),
        filter((result) => result != null),
      )
      .subscribe();
  }
}
