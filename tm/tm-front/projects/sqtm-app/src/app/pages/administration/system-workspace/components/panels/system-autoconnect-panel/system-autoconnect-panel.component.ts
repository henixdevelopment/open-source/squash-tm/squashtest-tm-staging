import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { SystemViewState } from '../../../states/system-view.state';
import { SystemViewService } from '../../../services/system-view.service';

@Component({
  selector: 'sqtm-app-system-autoconnect-panel',
  templateUrl: './system-autoconnect-panel.component.html',
  styleUrls: [
    './system-autoconnect-panel.component.less',
    '../../../styles/system-workspace.common.less',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SystemAutoconnectPanelComponent {
  @Input()
  componentData: SystemViewState;

  constructor(public readonly systemViewService: SystemViewService) {}

  changeAutoconnect(enabled: boolean): void {
    this.systemViewService.changeAutoconnectOnConnection(enabled);
  }
}
