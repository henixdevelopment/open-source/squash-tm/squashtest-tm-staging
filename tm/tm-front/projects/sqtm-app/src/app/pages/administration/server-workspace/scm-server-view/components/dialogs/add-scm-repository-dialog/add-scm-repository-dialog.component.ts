import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { AbstractAdministrationCreationDialogDirective } from '../../../../../components/abstract-administration-creation-dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  ActionErrorDisplayService,
  CreationDialogData,
  DialogReference,
  extractSquashActionError,
  extractSquashFirstFieldError,
  FieldValidationError,
  RestService,
  TextFieldComponent,
} from 'sqtm-core';
import { Observable, of } from 'rxjs';
import { switchMap, take } from 'rxjs/operators';
import { ScmServerViewService } from '../../../services/scm-server-view.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'sqtm-app-add-scm-repository-dialog',
  templateUrl: './add-scm-repository-dialog.component.html',
  styleUrls: ['./add-scm-repository-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddScmRepositoryDialogComponent
  extends AbstractAdministrationCreationDialogDirective
  implements OnInit
{
  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  data: CreationDialogData;

  @ViewChildren(TextFieldComponent)
  textFields: QueryList<TextFieldComponent>;

  constructor(
    private fb: FormBuilder,
    dialogReference: DialogReference,
    restService: RestService,
    private scmServerViewService: ScmServerViewService,
    cdr: ChangeDetectorRef,
    private actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    super('', dialogReference, restService, cdr);
    this.data = this.dialogReference.data;
  }

  get textFieldToFocus(): string {
    return 'repositoryName';
  }

  ngOnInit() {
    this.initializeFormGroup();
  }

  addEntity(addAnother?: boolean) {
    if (this.formIsValid()) {
      this.beginAsync();
      this.getRequestPayload()
        .pipe(
          take(1),
          switchMap((payload) => this.scmServerViewService.addRepository(payload)),
        )
        .subscribe({
          next: (result) => {
            this.handleCreationSuccess(result, addAnother);
          },
          error: (error) => {
            this.handleCreationFailure(error);
          },
        });
    } else {
      this.showClientSideErrors();
    }
  }

  protected handleCreationFailure(error: HttpErrorResponse) {
    this.endAsync();
    if (error.status === 412) {
      const squashFieldError = extractSquashFirstFieldError(error);
      if (squashFieldError.kind === 'FIELD_VALIDATION_ERROR') {
        const fieldValidationErrors = squashFieldError.fieldValidationErrors;
        if (
          fieldValidationErrors != null &&
          fieldValidationErrors.length > 0 &&
          fieldValidationErrors[0].fieldName == null
        ) {
          // case where no fieldName is specified in the error: it is then displayed in a dialog
          this.actionErrorDisplayService.handleActionError(error).subscribe();
        } else {
          this.serverSideValidationErrors = fieldValidationErrors;
          this.cdr.markForCheck();
        }
      }
      const squashActionError = extractSquashActionError(error);
      if (squashActionError.kind === 'ACTION_ERROR') {
        this.actionErrorDisplayService.handleActionError(error).subscribe();
      }
    }
  }

  protected getRequestPayload(): Observable<ScmRepositoryPayload> {
    return of({
      name: this.getFormControlValue('repositoryName'),
      workingBranch: this.getFormControlValue('branch'),
      workingFolderPath: this.getFormControlValue('cloneRepository')
        ? this.getFormControlValue('workingFolderPath')
        : '',
      cloneRepository: this.getFormControlValue('cloneRepository'),
    });
  }

  protected doResetForm() {
    this.resetFormControl('repositoryName', '');
    this.resetFormControl('branch', 'main');
    this.resetFormControl('workingFolderPath', '');
    this.resetFormControl('cloneRepository', false);
    this.updateDisabledFields();
  }

  updateDisabledFields() {
    if (this.getFormControlValue('cloneRepository')) {
      this.getFormControl('workingFolderPath').enable({ emitEvent: true });
    } else {
      this.getFormControl('workingFolderPath').disable({ emitEvent: true });
    }
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      repositoryName: this.fb.control('', [Validators.required, Validators.maxLength(255)]),
      branch: this.fb.control('main', [Validators.required, Validators.maxLength(255)]),
      workingFolderPath: this.fb.control('', [Validators.maxLength(255)]),
      cloneRepository: this.fb.control(false),
    });
  }

  cloneRepositoryIsChecked() {
    return this.getFormControlValue('cloneRepository');
  }
}

export interface ScmRepositoryPayload {
  name: string;
  workingFolderPath: string;
  workingBranch: string;
  cloneRepository: boolean;
}
