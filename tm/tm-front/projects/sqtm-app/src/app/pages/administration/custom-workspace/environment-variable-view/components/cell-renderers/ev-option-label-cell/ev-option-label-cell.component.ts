import { Component, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ActionErrorDisplayService,
  ColumnDefinitionBuilder,
  EditableTextFieldComponent,
  GridColumnId,
  GridService,
} from 'sqtm-core';
import { EnvironmentVariableViewService } from '../../../services/environment-variable-view.service';
import { catchError, finalize } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-ev-option-label-cell',
  template: ` @if (columnDisplay && row) {
    <div class="full-width full-height flex-column">
      <sqtm-core-editable-text-field
        #editableTextField
        style="margin: auto 5px;"
        class="sqtm-grid-cell-txt-renderer"
        [showPlaceHolder]="false"
        [value]="row.data[columnDisplay.id]"
        [layout]="'no-buttons'"
        [size]="'small'"
        (confirmEvent)="updateValue($event)"
      ></sqtm-core-editable-text-field>
    </div>
  }`,
  styleUrls: ['./ev-option-label-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvOptionLabelCellComponent extends AbstractCellRendererComponent {
  @ViewChild('editableTextField')
  editableTextField: EditableTextFieldComponent;

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    private environmentVariableViewService: EnvironmentVariableViewService,
    private actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    super(grid, cdRef);
  }

  updateValue(newLabel: string) {
    const currentLabel = this.row.data[GridColumnId.label];
    this.environmentVariableViewService
      .changeOptionLabel(currentLabel, newLabel)
      .pipe(
        catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
        finalize(() => this.editableTextField.endAsync()),
      )
      .subscribe();
  }
}

export function evOptionLabelColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(EvOptionLabelCellComponent);
}
