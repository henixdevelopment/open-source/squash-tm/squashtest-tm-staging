import { TestBed, waitForAsync } from '@angular/core/testing';

import { SystemViewService } from './system-view.service';
import { AdminReferentialDataService, RestService } from 'sqtm-core';
import { SystemViewState } from '../states/system-view.state';
import { TranslateService } from '@ngx-translate/core';
import { AppTestingUtilsModule } from '../../../../utils/testing-utils/app-testing-utils.module';
import {
  mockAdminReferentialDataService,
  mockRestService,
} from '../../../../utils/testing-utils/mocks.service';
import { of } from 'rxjs';
import { take } from 'rxjs/operators';
import createSpyObj = jasmine.createSpyObj;

describe('SystemViewService', () => {
  let service: SystemViewService;
  const restService = mockRestService();

  const adminReferentialDataService = mockAdminReferentialDataService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      providers: [
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: TranslateService,
          useValue: createSpyObj(['instant']),
        },
        {
          provide: SystemViewService,
          useClass: SystemViewService,
        },
        {
          provide: AdminReferentialDataService,
          useValue: adminReferentialDataService,
        },
      ],
    });
    service = TestBed.inject(SystemViewService);
    service['restService'] = restService; // The mock won't be properly set without this line... wtf?
  }));

  it('should load system view', async () => {
    const model = getInitialSystemView();
    restService.get.and.returnValue(of(model));
    service.load();

    service.componentData$.pipe(take(1)).subscribe((componentData) => {
      expect(componentData).toEqual(model);
    });
  });

  it('should change white list', async () => {
    const model = getInitialSystemView();
    restService.get.and.returnValue(of(model));
    service.load();

    service.changeWhiteList('pdf').subscribe();
    service.componentData$.pipe(take(1)).subscribe((componentData) => {
      expect(componentData.whiteList).toEqual('pdf');
    });
  });

  it('should change upload size limit', async () => {
    const model = getInitialSystemView();
    restService.get.and.returnValue(of(model));
    service.load();

    service.changeUploadSizeLimit('10000').subscribe();
    service.componentData$.pipe(take(1)).subscribe((componentData) => {
      expect(componentData.uploadSizeLimit).toEqual('10000');
    });
  });

  it('should change import size limit', async () => {
    const model = getInitialSystemView();
    restService.get.and.returnValue(of(model));
    service.load();

    service.changeImportSizeLimit('10000').subscribe();
    service.componentData$.pipe(take(1)).subscribe((componentData) => {
      expect(componentData.importSizeLimit).toEqual('10000');
    });
  });

  it('should change callback url', async () => {
    const model = getInitialSystemView();
    restService.get.and.returnValue(of(model));
    service.load();

    service.changeCallbackUrl('http://192.168.0.1:8080/squash').subscribe();
    service.componentData$.pipe(take(1)).subscribe((componentData) => {
      expect(componentData.callbackUrl).toEqual('http://192.168.0.1:8080/squash');
    });
  });

  it('should enable stack trace feature', async () => {
    const model = getInitialSystemView();
    restService.get.and.returnValue(of(model));
    service.load();

    service.changeStackTraceFeatureEnabled(true);
    service.componentData$.pipe(take(1)).subscribe((componentData) => {
      expect(componentData.stackTraceFeatureIsEnabled).toEqual(true);
    });
  });

  it('should enable case insensitive login feature', async () => {
    const model = getInitialSystemView();
    restService.get.and.returnValue(of(model));
    service.load();

    service.changeCaseInsensitiveLoginEnabled(true);
    service.componentData$.pipe(take(1)).subscribe((componentData) => {
      expect(componentData.caseInsensitiveLogin).toEqual(true);
    });
  });
});

function getInitialSystemView(): SystemViewState {
  return {
    appVersion: '2.0',
    statistics: null,
    plugins: null,
    whiteList: '',
    uploadSizeLimit: '',
    importSizeLimit: '',
    callbackUrl: '',
    stackTracePanelIsVisible: false,
    stackTraceFeatureIsEnabled: false,
    autoconnectOnConnection: false,
    caseInsensitiveLogin: false,
    duplicateLogins: [],
    welcomeMessage: '',
    loginMessage: '',
    bannerMessage: null,
    logFiles: [],
    caseInsensitiveActions: false,
    duplicateActions: [],
    licenseInfo: null,
    currentActiveUsersCount: 0,
    reportTemplateModels: [],
    synchronisationPlugins: null,
    isAutomaticRefreshForSupervisionPageActivated: false,
    lastSupervisionPageRefresh: null,
    unsafeAttachmentPreviewEnabled: false,
  };
}
