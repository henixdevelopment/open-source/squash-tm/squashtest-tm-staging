export interface EditRequirementLinksDialogConfiguration {
  requirementLinkId: number;
  role1: string;
  role1Code: string;
  role2: string;
  role2Code: string;
}
