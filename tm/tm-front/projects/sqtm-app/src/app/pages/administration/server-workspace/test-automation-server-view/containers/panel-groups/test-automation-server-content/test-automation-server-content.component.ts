import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  OnDestroy,
  Inject,
  ViewChild,
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { finalize, map, takeUntil } from 'rxjs/operators';
import {
  GridService,
  gridServiceFactory,
  Identifier,
  ListItem,
  ReferentialDataService,
  RestService,
  AutomationEnvironmentTagHolder,
  DialogService,
  AdminReferentialDataService,
} from 'sqtm-core';
import { BoundEnvironmentVariableService } from '../../../../../../../components/squash-orchestrator/orchestrator-execution-environment-variable/services/bound-environment-variable.service';
import { TestAutomationServerViewService } from '../../../services/test-automation-server-view.service';
import { AdminTestAutomationServerViewState } from '../../../states/admin-test-automation-server-view-state';
import {
  OrchestratorExecutionEnvironmentVariablePanelComponent,
  EnvironmentVariableAssociationsTableDefinition,
  EV_ASSOCIATION_TABLE,
  EV_ASSOCIATION_TABLE_CONF,
} from '../../../../../../../components/squash-orchestrator/orchestrator-execution-environment-variable/components/orchestrator-execution-environment-variable-panel/orchestrator-execution-environment-variable-panel.component';

const OPENTESTFACTORY_WORKFLOW_SYNTAX_DOC_URL =
  'https://opentestfactory.org/specification/workflows.html?utm_source=Appli_squash&utm_medium=link';

@Component({
  selector: 'sqtm-app-test-automation-server-content',
  templateUrl: './test-automation-server-content.component.html',
  styleUrls: ['./test-automation-server-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: EV_ASSOCIATION_TABLE_CONF,
      useFactory: EnvironmentVariableAssociationsTableDefinition,
    },
    {
      provide: EV_ASSOCIATION_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, EV_ASSOCIATION_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: BoundEnvironmentVariableService,
      useClass: BoundEnvironmentVariableService,
    },
  ],
})
export class TestAutomationServerContentComponent implements OnInit, OnDestroy {
  componentData$: Observable<AdminTestAutomationServerViewState>;
  supportsAutomatedExecutionEnvironments$: Observable<boolean>;
  unsub$ = new Subject<void>();
  isUltimate$: Observable<boolean>;

  @ViewChild(OrchestratorExecutionEnvironmentVariablePanelComponent)
  private environmentVariableBindingPanelComponent;

  public readonly environmentPanelTagHolderType =
    AutomationEnvironmentTagHolder.TEST_AUTOMATION_SERVER;

  constructor(
    public testAutomationServerViewService: TestAutomationServerViewService,
    @Inject(EV_ASSOCIATION_TABLE) public evGridService: GridService,
    private dialogService: DialogService,
    private adminReferentialDataService: AdminReferentialDataService,
  ) {
    this.componentData$ = testAutomationServerViewService.componentData$.pipe(
      takeUntil(this.unsub$),
    );
    this.isUltimate$ = this.adminReferentialDataService.isUltimateLicenseAvailable$;
  }

  ngOnInit(): void {
    this.componentData$ = this.testAutomationServerViewService.componentData$.pipe(
      takeUntil(this.unsub$),
    );
    this.supportsAutomatedExecutionEnvironments$ = this.componentData$.pipe(
      map((data) => data.testAutomationServer.supportsAutomatedExecutionEnvironments),
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  openBindEnvironmentVariableDialog() {
    this.environmentVariableBindingPanelComponent.openBindEnvironmentVariables();
  }

  openUnbindEnvironmentVariableDialog() {
    this.environmentVariableBindingPanelComponent.openUnbindEnvironmentVariableDialog();
  }

  bindEnvironmentVariable(items: ListItem[]) {
    this.evGridService.beginAsyncOperation();
    const selectedItemIds: Identifier[] = items.map((item) => item.id);
    this.environmentVariableBindingPanelComponent
      .bindEnvironmentVariables(selectedItemIds)
      .pipe(finalize(() => this.evGridService.completeAsyncOperation()))
      .subscribe();
  }

  updateAdditionalConfiguration(input: string) {
    this.testAutomationServerViewService.updateAdditionalConfiguration(input);
  }

  get workflowSyntaxDocUrl(): string {
    return OPENTESTFACTORY_WORKFLOW_SYNTAX_DOC_URL;
  }

  checkAdditionalConfigurationSyntax(additionalConfiguration: string) {
    this.testAutomationServerViewService
      .checkAdditionalConfigurationSyntax(additionalConfiguration)
      .subscribe((isValid) =>
        this.dialogService.openAlert({
          id: 'check-additional-configuration-validity-dialog',
          titleKey: 'sqtm-core.generic.label.information.singular',
          messageKey: isValid
            ? 'sqtm-core.administration-workspace.servers.test-automation-servers.additional-configuration.valid-script'
            : 'sqtm-core.administration-workspace.servers.test-automation-servers.additional-configuration.invalid-script',
          level: isValid ? 'INFO' : 'DANGER',
        }),
      );
  }
}
