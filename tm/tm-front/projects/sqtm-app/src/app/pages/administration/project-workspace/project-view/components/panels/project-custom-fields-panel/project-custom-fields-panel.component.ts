import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  ViewContainerRef,
} from '@angular/core';
import {
  booleanColumn,
  customFieldBoundEntityColumn,
  CustomField,
  deleteColumn,
  DialogService,
  Extendable,
  Fixed,
  GridColumnId,
  GridDefinition,
  GridService,
  indexColumn,
  RestService,
  smallGrid,
  Sort,
  StyleDefinitionBuilder,
  textColumn,
} from 'sqtm-core';
import { Observable, Subject } from 'rxjs';
import { AdminProjectViewComponentData } from '../../../containers/project-view/project-view.component';
import { ProjectViewService } from '../../../services/project-view.service';
import { filter, map, switchMap, take, takeUntil } from 'rxjs/operators';
import { customFieldBindingTypeColumn } from '../../cell-renderers/custom-field-binding-type-cell/custom-field-binding-type-cell.component';
import {
  BindCustomFieldDialogComponent,
  SelectedCustomFieldsDialogResult,
} from '../../dialogs/bind-custom-field-dialog/bind-custom-field-dialog.component';
import { UnbindCustomFieldFromProjectCellComponent } from '../../cell-renderers/unbind-custom-field-from-project-cell/unbind-custom-field-from-project-cell.component';
import { PROJECT_CUSTOM_FIELDS_TABLE } from '../../../project-view.constant';

export function projectCustomFieldsTableDefinition(): GridDefinition {
  return smallGrid('project-custom-fields')
    .withColumns([
      indexColumn().withViewport('leftViewport'),
      customFieldBoundEntityColumn(GridColumnId.bindableEntity).changeWidthCalculationStrategy(
        new Extendable(100, 0.2),
      ),
      textColumn(GridColumnId.name)
        .changeWidthCalculationStrategy(new Extendable(100, 0.2))
        .withI18nKey('sqtm-core.entity.custom-field.label.singular'),
      customFieldBindingTypeColumn(GridColumnId.inputType)
        .changeWidthCalculationStrategy(new Extendable(100, 0.2))
        .withI18nKey('sqtm-core.entity.custom-field.input-type.label'),
      textColumn(GridColumnId.code)
        .withI18nKey('sqtm-core.generic.label.code')
        .changeWidthCalculationStrategy(new Extendable(100, 0.1)),
      booleanColumn(GridColumnId.optional)
        .withI18nKey('sqtm-core.entity.custom-field.optional.label')
        .changeWidthCalculationStrategy(new Fixed(100)),
      deleteColumn(UnbindCustomFieldFromProjectCellComponent),
    ])
    .withInitialSortedColumns([{ id: GridColumnId.bindableEntity, sort: Sort.ASC }])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .build();
}

@Component({
  selector: 'sqtm-app-project-custom-fields-panel',
  template: ` <sqtm-core-grid></sqtm-core-grid>`,
  styleUrls: ['./project-custom-fields-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: PROJECT_CUSTOM_FIELDS_TABLE,
    },
  ],
})
export class ProjectCustomFieldsPanelComponent implements OnInit, OnDestroy {
  private componentData$: Observable<AdminProjectViewComponentData>;

  private unsub$ = new Subject<void>();

  constructor(
    private readonly projectViewService: ProjectViewService,
    private readonly gridService: GridService,
    private readonly dialogService: DialogService,
    private readonly restService: RestService,
    private readonly viewContainerRef: ViewContainerRef,
  ) {}

  ngOnInit(): void {
    this.componentData$ = this.projectViewService.componentData$;
    this.gridService.connectToDatasource(this.projectViewService.boundCustomFields$, 'id');
  }

  ngOnDestroy(): void {
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  openUnbindCustomFieldsDialog() {
    this.gridService.selectedRowIds$.pipe(take(1)).subscribe((customFieldBindingIds: number[]) => {
      const dialogReference = this.dialogService.openDeletionConfirm({
        titleKey:
          'sqtm-core.administration-workspace.projects.dialog.title.unbind-custom-field-from-project.unbind-many',
        messageKey:
          'sqtm-core.administration-workspace.projects.dialog.message.unbind-custom-field-from-project.unbind-many',
        level: 'WARNING',
      });

      dialogReference.dialogClosed$
        .pipe(
          take(1),
          filter((result) => Boolean(result)),
          switchMap(() => this.projectViewService.unbindCustomFields(customFieldBindingIds)),
        )
        .subscribe(() => this.gridService.refreshData());
    });
  }

  openBindCustomFieldDialog() {
    this.restService
      .get<CustomFieldsResponse>(['custom-fields'])
      .pipe(map((response) => response.customFields.sort((a, b) => a.name.localeCompare(b.name))))
      .subscribe((customFields: CustomField[]) => {
        const dialogReference = this.dialogService.openDialog({
          id: 'project-custom-field-binding',
          component: BindCustomFieldDialogComponent,
          viewContainerReference: this.viewContainerRef,
          data: {
            titleKey: 'sqtm-core.administration-workspace.projects.dialog.title.bind-custom-fields',
            customFields,
          },
        });

        dialogReference.dialogResultChanged$
          .pipe(
            takeUntil(dialogReference.dialogClosed$),
            filter((result) => Boolean(result)),
          )
          .subscribe((result: SelectedCustomFieldsDialogResult) => {
            this.projectViewService.bindCustomFields(
              result.selectedEntity,
              result.selectedCustomFields,
            );
            this.gridService.refreshData();
          });
      });
  }
}

interface CustomFieldsResponse {
  customFields: CustomField[];
}
