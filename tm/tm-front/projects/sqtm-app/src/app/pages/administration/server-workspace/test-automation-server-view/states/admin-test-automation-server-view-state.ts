import { GenericEntityViewState, provideInitialGenericViewState } from 'sqtm-core';
import { AdminTestAutomationServerState } from './admin-test-automation-server-state';

export interface AdminTestAutomationServerViewState
  extends GenericEntityViewState<AdminTestAutomationServerState, 'testAutomationServer'> {
  testAutomationServer: AdminTestAutomationServerState;
}

export function provideInitialAdminTestAutomationServerView(): Readonly<AdminTestAutomationServerViewState> {
  return provideInitialGenericViewState<AdminTestAutomationServerState, 'testAutomationServer'>(
    'testAutomationServer',
  );
}
