import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { SystemViewState } from '../../../states/system-view.state';

@Component({
  selector: 'sqtm-app-system-plugins-panel',
  templateUrl: './system-plugins-panel.component.html',
  styleUrls: ['./system-plugins-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SystemPluginsPanelComponent {
  @Input() componentData: SystemViewState;

  get plugins(): string[] {
    const plugins = this.componentData.plugins;

    if (plugins == null) {
      return [];
    }

    plugins.sort((a, b) => a.localeCompare(b));
    return plugins;
  }
}
