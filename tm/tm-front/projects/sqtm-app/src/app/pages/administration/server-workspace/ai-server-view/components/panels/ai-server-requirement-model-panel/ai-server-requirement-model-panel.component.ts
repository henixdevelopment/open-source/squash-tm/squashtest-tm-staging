import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  signal,
  ViewChild,
  ViewContainerRef,
  ViewEncapsulation,
  WritableSignal,
} from '@angular/core';

import { DialogService, EditableTextAreaFieldComponent, TestCaseFromAi } from 'sqtm-core';
import { AiServerViewService } from '../../../services/ai-server-view.service';
import { AdminAiServerViewState } from '../../../states/admin-ai-server-view-state';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AiServerTestCaseGenerationDialogConfiguration } from '../../dialogs/ai-server-test-case-generation-dialog/ai-server-test-case-generation-dialog-configuration';
import { AiServerTestCaseGenerationDialogComponent } from '../../dialogs/ai-server-test-case-generation-dialog/ai-server-test-case-generation-dialog.component';

@Component({
  selector: 'sqtm-app-ai-server-requirement-model-panel',
  templateUrl: './ai-server-requirement-model-panel.component.html',
  styleUrl: './ai-server-requirement-model-panel.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class AiServerRequirementModelPanelComponent implements OnInit, AfterViewInit {
  @Input()
  value: string;

  @Input()
  componentData: AdminAiServerViewState;

  @ViewChild(EditableTextAreaFieldComponent)
  textAreaFieldComponent: EditableTextAreaFieldComponent;

  errorDisplayed: string;
  localStorageValue$: Observable<string>;
  isRequirementModelDefined: boolean;

  $loading: WritableSignal<boolean> = signal(false);
  $isError: WritableSignal<boolean> = signal(false);

  constructor(
    public aiServerViewService: AiServerViewService,
    public translateService: TranslateService,
    public readonly dialogService: DialogService,
    public readonly vcr: ViewContainerRef,
  ) {}

  ngOnInit(): void {
    this.localStorageValue$ = this.aiServerViewService.getModelFromLocalStorage();
  }

  ngAfterViewInit(): void {
    this.inferRequirementModelState();
  }

  handleBlur(requirementModel: string): void {
    this.aiServerViewService.saveRequirementModelInLocalStorage(requirementModel);
    this.textAreaFieldComponent.disableEditMode();
    this.inferRequirementModelState();
  }

  private inferRequirementModelState(): void {
    this.isRequirementModelDefined =
      this.textAreaFieldComponent.value !== '' &&
      this.textAreaFieldComponent.value !== undefined &&
      this.textAreaFieldComponent.value !== null;
  }

  testConfiguration(): void {
    const knownErrors = [
      'AiServerBadPayloadTemplateException',
      'AiServerResponseInoperableException',
      'AiServerWrongTokenException',
      'AiServerWrongUrlException',
      'JsonResponseParsingException',
      'RemoteAiServerActionException',
      'UnextractableAiServerResponseException',
    ];
    this.$loading.set(true);
    this.$isError.set(false);

    this.aiServerViewService
      .testConfiguration(this.componentData.aiServer.id, this.textAreaFieldComponent.value)
      .pipe(
        catchError((error) => {
          this.$loading.set(false);
          this.$isError.set(true);
          const squashError = error.error.squashTMError;
          if (knownErrors.includes(squashError.actionValidationError.exception)) {
            this.setHandledError(squashError);
          } else {
            this.errorDisplayed = this.translateService.instant(
              this.replaceReservedHtmlCharsWithEntity(squashError.actionValidationError.i18nKey),
            );
          }
          throw error;
        }),
      )
      .subscribe((response: { testCases: TestCaseFromAi[] }) => {
        this.$loading.set(false);
        try {
          this.displayTestCasesData(response);
        } catch (e) {
          this.dialogService.openAlert({
            level: 'DANGER',
            messageKey: 'sqtm-core.administration-workspace.servers.ai.dialog.inoperable-response',
          });
        }
      });
  }

  private setHandledError(squashError: any): void {
    this.errorDisplayed = this.translateService.instant(squashError.actionValidationError.i18nKey, {
      error: squashError.actionValidationError.i18nParams[0] ?? null,
      message: this.prettyJsonString(squashError.actionValidationError.i18nParams[1]),
    });
  }

  private prettyJsonString(errorMessage: string) {
    try {
      const object = JSON.parse(this.replaceReservedHtmlCharsWithEntity(errorMessage));
      return JSON.stringify(object, null, 2);
    } catch (error) {
      console.error('Error parsing JSON: ', error);
      return errorMessage;
    }
  }

  // Some AI models use special tokens in their response to mark the beginning and end of a string.
  // But they are reserved HTML characters, so we need to remove them to prevent the error message styling.
  public replaceReservedHtmlCharsWithEntity(errorMessage: string) {
    return errorMessage
      .replace(/&/g, '&amp;')
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;')
      .replace(/'/g, '&#039;');
  }

  private displayTestCasesData(response: { testCases: TestCaseFromAi[] }): void {
    this.dialogService.openDialog<AiServerTestCaseGenerationDialogConfiguration, boolean>({
      component: AiServerTestCaseGenerationDialogComponent,
      viewContainerReference: this.vcr,
      id: 'ai-generate-test-cases',
      width: 600,
      height: 600,
      data: {
        serverResponse: response,
      },
    });
  }
}
