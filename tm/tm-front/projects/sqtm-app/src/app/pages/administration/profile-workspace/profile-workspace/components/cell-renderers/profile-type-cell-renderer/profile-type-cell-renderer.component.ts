import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-profile-type-cell-renderer',
  template: ` @if (columnDisplay && row) {
    <div class="full-width full-height flex-column">
      <span class="txt-ellipsis m-auto-0">
        {{ typeKey | translate }}
      </span>
    </div>
  }`,
  styleUrls: ['./profile-type-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileTypeCellRendererComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }

  get typeKey(): string {
    return this.row.data[this.columnDisplay.id]
      ? 'sqtm-core.nav-bar.administration.system.label'
      : 'sqtm-core.entity.profile.custom.label';
  }
}

export function profileTypeColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(ProfileTypeCellRendererComponent);
}
