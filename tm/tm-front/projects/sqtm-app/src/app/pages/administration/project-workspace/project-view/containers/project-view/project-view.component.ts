import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
  Signal,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { Subject } from 'rxjs';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { catchError, concatMap, filter, map, take, takeUntil } from 'rxjs/operators';
import { AssociateTemplateDialogComponent } from '../../components/dialogs/associate-template-dialog/associate-template-dialog.component';
import { AdminProjectState } from '../../state/admin-project-state';
import { ProjectAction, ProjectViewService } from '../../services/project-view.service';
import {
  ActionErrorDisplayService,
  AdminReferentialDataService,
  AdminViewAttachmentHelperService,
  AttachmentDrawerComponent,
  AttachmentService,
  DialogService,
  GenericEntityViewComponentData,
  GenericEntityViewService,
  ProjectDataInfo,
  RestService,
  RichTextAttachmentDelegate,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { toSignal } from '@angular/core/rxjs-interop';
@Component({
  selector: 'sqtm-app-project-view',
  templateUrl: './project-view.component.html',
  styleUrls: ['./project-view.component.less'],
  providers: [
    {
      provide: ProjectViewService,
      useClass: ProjectViewService,
      deps: [
        RestService,
        AttachmentService,
        TranslateService,
        AdminReferentialDataService,
        AdminViewAttachmentHelperService,
      ],
    },
    {
      provide: GenericEntityViewService,
      useExisting: ProjectViewService,
    },
    {
      provide: RichTextAttachmentDelegate,
      useExisting: ProjectViewService,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectViewComponent implements OnInit, OnDestroy {
  @ViewChild(AttachmentDrawerComponent)
  attachmentDrawer: AttachmentDrawerComponent;

  @Output()
  gridRefreshRequired = new EventEmitter<void>();

  @Output()
  projectDeleted = new EventEmitter<void>();

  private unsub$ = new Subject<void>();
  readonly isUltimate$: Signal<boolean>;

  constructor(
    private route: ActivatedRoute,
    public readonly projectViewService: ProjectViewService,
    private cdRef: ChangeDetectorRef,
    private dialogService: DialogService,
    private restService: RestService,
    public readonly adminReferentialDataService: AdminReferentialDataService,
    private vcr: ViewContainerRef,
    private readonly actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    this.isUltimate$ = toSignal(adminReferentialDataService.isUltimateLicenseAvailable$);
  }

  ngOnInit() {
    this.prepareGridRefreshOnEntityChanges();

    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('projectId')),
      )
      .subscribe((id) => {
        this.projectViewService.load(parseInt(id, 10));
        this.cdRef.detectChanges();
      });
  }

  openAttachmentPanel() {
    this.attachmentDrawer.open();
  }

  getDeleteItemLabel(isTemplate: boolean) {
    return isTemplate
      ? 'sqtm-core.administration-workspace.projects.dialog.title.delete-template'
      : 'sqtm-core.administration-workspace.projects.dialog.title.delete-project';
  }

  handleDelete() {
    this.projectViewService.componentData$
      .pipe(take(1), takeUntil(this.unsub$))
      .subscribe((componentData) => {
        this.deleteProject(componentData.project);
      });
  }

  associateTemplate() {
    this.projectViewService.componentData$
      .pipe(take(1), takeUntil(this.unsub$))
      .subscribe((componentData) => {
        if (componentData.project.linkedTemplateId) {
          // Disassociate project from template
          const dialogReference = this.dialogService.openDeletionConfirm({
            titleKey:
              'sqtm-core.administration-workspace.projects.dialog.title.disassociate-from-template',
            messageKey:
              'sqtm-core.administration-workspace.projects.dialog.message.disassociate-from-template',
            level: 'WARNING',
          });

          dialogReference.dialogClosed$
            .pipe(
              takeUntil(this.unsub$),
              filter((result) => result === true),
            )
            .subscribe(() => {
              this.projectViewService.disassociateFromTemplate();
            });
        } else {
          // Associate with template
          this.dialogService.openDialog({
            id: 'associate-template-dialog',
            component: AssociateTemplateDialogComponent,
            viewContainerReference: this.vcr,
            width: 720,
          });
        }
      });
  }

  coerceIntoTemplate() {
    this.projectViewService
      .getProjectDataInfoOnAction(ProjectAction.TRANSFORMATION)
      .subscribe((response: ProjectDataInfo) => {
        // If this project has no data (non folder entities or plugins)
        if (response.hasData) {
          this.dialogService.openAlert({
            titleKey:
              'sqtm-core.administration-workspace.projects.dialog.title.coerce-into-template.long',
            messageKey:
              'sqtm-core.administration-workspace.projects.dialog.message.cannot-coerce-into-template',
            level: 'INFO',
            id: 'cannot-coerce-into-template',
          });
        } else {
          const dialogRef = this.dialogService.openConfirm({
            id: 'associate-template-dialog',
            titleKey:
              'sqtm-core.administration-workspace.projects.dialog.title.coerce-into-template.long',
            messageKey:
              'sqtm-core.administration-workspace.projects.dialog.message.coerce-into-template',
          });

          dialogRef.dialogClosed$.subscribe((confirm) => {
            if (confirm) {
              this.projectViewService.coerceProjectIntoTemplate();
            }
          });
        }
      });
  }

  private openCannotDeleteProjectAlert(): void {
    this.dialogService.openAlert({
      level: 'INFO',
      titleKey:
        'sqtm-core.administration-workspace.projects.dialog.title.cannot-delete-project-with-data',
      messageKey:
        'sqtm-core.administration-workspace.projects.dialog.message.cannot-delete-project-with-data',
    });
  }

  private deleteProject(projectState: AdminProjectState) {
    this.projectViewService
      .getProjectDataInfoOnAction(ProjectAction.DELETION)
      .subscribe((response: ProjectDataInfo) => {
        if (!response.hasData) {
          this.openDeleteProjectDialog(projectState, response);
        } else {
          this.openCannotDeleteProjectAlert();
        }
      });
  }

  private openDeleteProjectDialog(
    projectState: AdminProjectState,
    response: ProjectDataInfo,
  ): void {
    const titleKey: string = projectState.template
      ? 'sqtm-core.administration-workspace.projects.dialog.title.delete-template'
      : 'sqtm-core.administration-workspace.projects.dialog.title.delete-project';

    const messageKey: string = this.getDeleteProjectMessageKey(
      projectState,
      response.i18nMessageKey,
    );

    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey,
      messageKey,
      level: 'DANGER',
    });

    dialogReference.dialogClosed$
      .pipe(
        takeUntil(this.unsub$),
        filter((result) => result === true),
        concatMap(() => this.restService.delete([`projects/${projectState.id}`])),
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
      )
      .subscribe(() => this.notifyProjectWasDeleted());
  }

  private getDeleteProjectMessageKey(
    projectState: AdminProjectState,
    projectMessageKey: string,
  ): string {
    if (projectState.template) {
      return projectState.templateLinkedToProjects
        ? 'sqtm-core.administration-workspace.projects.dialog.message.delete-template-with-linked-projects'
        : 'sqtm-core.administration-workspace.projects.dialog.message.delete-template-without-linked-projects';
    }
    return projectMessageKey;
  }

  ngOnDestroy(): void {
    this.projectViewService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  private prepareGridRefreshOnEntityChanges(): void {
    this.projectViewService.simpleAttributeRequiringRefresh = [
      'name',
      'label',
      'bugTrackerBinding',
      'taServerId',
      'lastModifiedBy',
      'lastModifiedOn',
    ];

    this.projectViewService.externalRefreshRequired$
      .pipe(takeUntil(this.unsub$))
      .subscribe(() => this.requestGridRefresh());
  }

  private requestGridRefresh(): void {
    this.gridRefreshRequired.emit();
  }

  private notifyProjectWasDeleted(): void {
    this.projectDeleted.emit();
  }
}

export interface AdminProjectViewComponentData
  extends GenericEntityViewComponentData<AdminProjectState, 'project'> {}
