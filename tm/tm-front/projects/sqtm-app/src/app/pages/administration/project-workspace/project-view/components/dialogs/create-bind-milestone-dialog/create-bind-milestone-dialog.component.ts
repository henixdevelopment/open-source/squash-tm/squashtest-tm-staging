import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  DialogReference,
  FieldValidationError,
  NOT_ONLY_SPACES_REGEX,
  RestService,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { AbstractAdministrationCreationDialogDirective } from '../../../../../components/abstract-administration-creation-dialog';
import { CreateBindMilestoneDialogConfiguration } from './create-bind-milestone-dialog-configuration';
import { map, switchMap, take } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'sqtm-app-create-bind-milestone-dialog',
  templateUrl: './create-bind-milestone-dialog.component.html',
  styleUrls: ['./create-bind-milestone-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateBindMilestoneDialogComponent
  extends AbstractAdministrationCreationDialogDirective
  implements OnInit
{
  formGroup: FormGroup;

  data: CreateBindMilestoneDialogConfiguration;

  serverSideValidationErrors: FieldValidationError[] = [];

  constructor(
    private fb: FormBuilder,
    private translateService: TranslateService,
    dialogReference: DialogReference<CreateBindMilestoneDialogConfiguration>,
    restService: RestService,
    cdr: ChangeDetectorRef,
  ) {
    super('', dialogReference, restService, cdr);
    this.data = dialogReference.data;
  }

  get textFieldToFocus(): string {
    return 'label';
  }

  ngOnInit(): void {
    this.initializeFormGroup();
  }

  createMilestoneAndBindToProject() {
    this.createMilestoneServerSide('create-milestone-and-bind-to-project', 'PLANNED');
  }

  createMilestoneAndBindToProjectAndObjects() {
    this.createMilestoneServerSide(
      'create-milestone-and-bind-to-project-and-objects',
      'IN_PROGRESS',
    );
  }

  private createMilestoneServerSide(urlEnding: string, defaultStatus: string) {
    if (this.formIsValid()) {
      this.getRequestPayload()
        .pipe(
          take(1),
          map((payload) => {
            return { ...payload, status: defaultStatus };
          }),
          switchMap((payload) => {
            const parts = ['milestone-binding/project', this.data.projectId, urlEnding];

            return this.restService.post(parts, payload);
          }),
        )
        .subscribe({
          next: (result) => {
            this.dialogReference.result = result;
            this.dialogReference.close();
          },
          error: (error) => {
            this.handleCreationFailure(error);
          },
        });
    } else {
      this.showClientSideErrors();
    }
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      label: this.fb.control('', [
        Validators.required,
        Validators.pattern(NOT_ONLY_SPACES_REGEX),
        Validators.maxLength(255),
      ]),
      endDate: this.fb.control('', [Validators.required]),
      description: this.fb.control(''),
    });
  }

  protected getRequestPayload() {
    return of({
      label: this.getFormControlValue('label'),
      endDate: this.getFormControlValue('endDate'),
      description: this.getFormControlValue('description'),
    });
  }

  protected doResetForm() {
    // NOOP
  }
}
