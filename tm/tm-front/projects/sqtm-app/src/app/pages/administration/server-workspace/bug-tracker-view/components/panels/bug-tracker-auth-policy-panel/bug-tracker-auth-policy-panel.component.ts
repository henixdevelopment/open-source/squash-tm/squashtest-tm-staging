import { ChangeDetectionStrategy, Component, Input, OnDestroy, ViewChild } from '@angular/core';
import { AdminBugTrackerViewState } from '../../../states/admin-bug-tracker-view-state';
import {
  AdminReferentialDataService,
  AuthenticationPolicy,
  BugTracker,
  Credentials,
  doesHttpErrorContainsSquashActionError,
  extractSquashActionError,
  InterWindowCommunicationService,
  InterWindowMessages,
  ListItem,
  ThirdPartyCredentialsFormComponent,
} from 'sqtm-core';
import { BugTrackerViewService } from '../../../services/bug-tracker-view.service';
import { catchError, filter, map, take, takeUntil } from 'rxjs/operators';
import { Observable, Subject, switchMap, throwError } from 'rxjs';

const TRANSLATE_KEYS_BASE = 'sqtm-core.administration-workspace.bugtrackers.authentication-policy.';
const OAUTH2_ENDPOINT = 'oauth2';

@Component({
  selector: 'sqtm-app-bug-tracker-authentication-policy-panel',
  templateUrl: './bug-tracker-auth-policy-panel.component.html',
  styleUrls: ['./bug-tracker-auth-policy-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BugTrackerAuthPolicyPanelComponent implements OnDestroy {
  @Input()
  componentData: AdminBugTrackerViewState;

  @ViewChild(ThirdPartyCredentialsFormComponent)
  credentialsForm: ThirdPartyCredentialsFormComponent;
  credentialsStatusMessage = '';
  statusIcon: 'DANGER' | 'INFO' = null;
  loginFieldKey$: Observable<string>;
  passwordFieldKey$: Observable<string>;

  serverKind = ['jira.xsquash', 'jira.rest', 'jira.cloud'];

  translateKeys = {
    userAuthenticationLabel: TRANSLATE_KEYS_BASE + 'user-authentication.label',
    userAuthenticationTooltip: TRANSLATE_KEYS_BASE + 'user-authentication.tooltip',
    appPolicy: TRANSLATE_KEYS_BASE + 'user-authentication.app-policy.label',
    usersPolicy: TRANSLATE_KEYS_BASE + 'user-authentication.users-policy.label',
    appPolicyInfo: TRANSLATE_KEYS_BASE + 'user-authentication.app-policy.info',
    usersPolicyInfo: TRANSLATE_KEYS_BASE + 'user-authentication.users-policy.info',

    credentialsLabel: TRANSLATE_KEYS_BASE + 'credentials.label',
    btCredentialsTooltip: TRANSLATE_KEYS_BASE + 'credentials.bt-tooltip',
    serverCredentialsTooltip: TRANSLATE_KEYS_BASE + 'credentials.server-tooltip',

    saveSuccessKey: TRANSLATE_KEYS_BASE + 'credentials.save-success',
    saveFailKey: TRANSLATE_KEYS_BASE + 'credentials.bad-credentials',
    connectionRefused: TRANSLATE_KEYS_BASE + 'credentials.connection-refused',
  };

  get radioItems(): ListItem[] {
    return [
      {
        id: AuthenticationPolicy.USER,
        i18nLabelKey: this.translateKeys.usersPolicy,
        displayInfoKey: this.translateKeys.usersPolicyInfo,
        selected: this.componentData.bugTracker.authPolicy === AuthenticationPolicy.USER,
      },
      {
        id: AuthenticationPolicy.APP_LEVEL,
        i18nLabelKey: this.translateKeys.appPolicy,
        displayInfoKey: this.translateKeys.appPolicyInfo,
        selected: this.componentData.bugTracker.authPolicy === AuthenticationPolicy.APP_LEVEL,
      },
    ];
  }

  unsub$ = new Subject<void>();

  constructor(
    public readonly bugTrackerViewService: BugTrackerViewService,
    private readonly interWindowCommunicationService: InterWindowCommunicationService,
    private readonly adminRefDataService: AdminReferentialDataService,
  ) {
    this.observeAuthProtocolChange();
    this.prepareLoginAndPasswordKeys();
  }

  prepareLoginAndPasswordKeys(): void {
    const selectedBt$ = this.adminRefDataService.bugTrackers$.pipe(
      takeUntil(this.unsub$),
      map((bugtrackers) => bugtrackers.find((bt) => bt.id === this.componentData.bugTracker.id)),
    );
    this.loginFieldKey$ = selectedBt$.pipe(map((bt) => bt?.loginFieldKey));
    this.passwordFieldKey$ = selectedBt$.pipe(map((bt) => bt?.passwordFieldKey));
  }

  private observeAuthProtocolChange(): void {
    this.bugTrackerViewService.externalRefreshRequired$.subscribe((update) => {
      if (update.key === 'authProtocol') {
        this.credentialsForm.authenticationProtocol = update.value;
        this.clearMessage();
      }
    });
  }

  sendCredentialsForm(credentials: Credentials) {
    this.clearMessage();
    this.bugTrackerViewService.setCredentials(credentials).subscribe(
      () => this.handleCredentialsSaveSuccess(),
      (err) => this.handleCredentialsError(err),
    );
  }

  deleteCredentials(bugTracker: BugTracker): void {
    this.bugTrackerViewService
      .deleteBugTrackerCredentials(bugTracker.id)
      .subscribe(() => this.clearMessage());
  }

  private clearMessage(): void {
    this.credentialsStatusMessage = undefined;
    this.statusIcon = undefined;
  }

  private handleCredentialsSaveSuccess(): void {
    this.credentialsStatusMessage = this.translateKeys.saveSuccessKey;
    this.credentialsForm.formGroup.markAsPristine();
    this.credentialsForm.endAsync();
    this.statusIcon = 'INFO';
  }

  private handleCredentialsError(error: any): void {
    this.credentialsForm.endAsync();

    if (doesHttpErrorContainsSquashActionError(error)) {
      const squashError = extractSquashActionError(error);
      this.credentialsStatusMessage = squashError.actionValidationError.i18nKey;
      this.statusIcon = 'DANGER';
    } else {
      // Default error handling
      console.error(error);
    }
  }

  handleAuthPolicyChange($event: ListItem): void {
    const policy = $event.id.toString();
    if (policy === AuthenticationPolicy.APP_LEVEL || policy === AuthenticationPolicy.USER) {
      this.bugTrackerViewService.setAuthPolicy(policy).subscribe();
    } else {
      throw new Error('Unhandled authentication policy : ' + policy);
    }
  }

  getCredentialsTooltip(kind: string): string {
    if (this.serverKind.includes(kind)) {
      return this.translateKeys.serverCredentialsTooltip;
    }
    return this.translateKeys.btCredentialsTooltip;
  }

  requestAndSaveOauth2Token(bugtrackerId: number) {
    const currentOpenedWindow = this.bugTrackerViewService.openOAuthDialog(
      bugtrackerId,
      OAUTH2_ENDPOINT,
    );
    this.handleOauth2Connexion(bugtrackerId, currentOpenedWindow);
  }

  private handleOauth2Connexion(bugtrackerId: number, currentOpenedWindow: Window) {
    this.interWindowCommunicationService.interWindowMessages$
      .pipe(
        takeUntil(this.unsub$),
        filter((message) => message.isTypeOf('OAUTH2-SECRET-CODE-RETRIEVED')),
        take(1),
        switchMap((message) =>
          this.bugTrackerViewService.askOauth2TokenToBackend(bugtrackerId, message.payload.code),
        ),
        switchMap(() =>
          this.bugTrackerViewService.updateStateWithSuccessfulOauthTokenRegistration(),
        ),
        catchError((err) => {
          this.interWindowCommunicationService.sendMessageToWindow(
            new InterWindowMessages('OAUTH2-FAILURE'),
            currentOpenedWindow,
          );
          return throwError(() => err);
        }),
      )
      .subscribe(() => {
        this.interWindowCommunicationService.sendMessageToWindow(
          new InterWindowMessages('OAUTH2-SUCCESS'),
          currentOpenedWindow,
        );
      });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
