import { ChangeDetectionStrategy, Component, Signal, ViewChild } from '@angular/core';
import { AiServerViewService } from '../../../services/ai-server-view.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AdminAiServerViewState } from '../../../states/admin-ai-server-view-state';
import { ThirdPartyCredentialsFormComponent, TokenAuthCredentials } from 'sqtm-core';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sqtm-app-ai-server-auth-policy-panel',
  templateUrl: './ai-server-auth-policy-panel.component.html',
  styleUrl: './ai-server-auth-policy-panel.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AiServerAuthPolicyPanelComponent {
  unsub$ = new Subject<void>();
  componentData$: Signal<AdminAiServerViewState>;
  statusIcon: string;
  credentialsStatusMessage: string;

  @ViewChild(ThirdPartyCredentialsFormComponent)
  credentialsForm: ThirdPartyCredentialsFormComponent;

  constructor(public readonly aiServerViewService: AiServerViewService) {
    this.componentData$ = toSignal(aiServerViewService.componentData$.pipe(takeUntil(this.unsub$)));
    const noTokenRegistered = aiServerViewService.getSnapshot().aiServer.credentials == null;
    if (noTokenRegistered) {
      this.setTokenStatusInfo(
        'WARNING',
        'sqtm-core.administration-workspace.servers.ai.view.token.no-token',
      );
    }
  }

  private setTokenStatusInfo(statusIcon: string, statusMessage: string) {
    this.statusIcon = statusIcon;
    this.credentialsStatusMessage = statusMessage;
  }

  sendCredentialsForm(credentials: TokenAuthCredentials) {
    this.aiServerViewService
      .setTokenAuthCredentials(credentials.token)
      .subscribe(() => this.handleCredentialsSaveSuccess());
  }

  private handleCredentialsSaveSuccess() {
    this.credentialsForm.formGroup.markAsPristine();
    this.credentialsForm.endAsync();
    this.setTokenStatusInfo(
      'INFO',
      'sqtm-core.administration-workspace.servers.ai.view.token.save-success',
    );
  }

  deleteCredentials() {
    this.aiServerViewService
      .deleteAiServerCredentials()
      .subscribe(() => this.handleDeleteCredentials());
  }

  private handleDeleteCredentials() {
    this.credentialsForm.formGroup.markAsPristine();
    this.credentialsForm.endAsync();
    this.setTokenStatusInfo(
      'INFO',
      'sqtm-core.administration-workspace.servers.ai.view.token.delete-success',
    );
  }
}
