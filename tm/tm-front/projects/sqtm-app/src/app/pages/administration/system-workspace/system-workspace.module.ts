import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SystemWorkspaceComponent } from './containers/system-workspace/system-workspace.component';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import {
  AnchorModule,
  AttachmentModule,
  CellRendererCommonModule,
  DialogModule,
  GridModule,
  NavBarModule,
  UiManagerModule,
  WorkspaceCommonModule,
} from 'sqtm-core';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { SystemViewInformationComponent } from './containers/panel-groups/system-view-information/system-view-information.component';
import { SystemStatisticsPanelComponent } from './components/panels/system-statistics-panel/system-statistics-panel.component';
import { SystemPluginsPanelComponent } from './components/panels/system-plugins-panel/system-plugins-panel.component';
import { SystemViewSettingsComponent } from './containers/panel-groups/system-view-settings/system-view-settings.component';
import { SystemAttachmentsPanelComponent } from './components/panels/system-attachments-panel/system-attachments-panel.component';
import { SystemLoginMessagePanelComponent } from './components/panels/system-login-message-panel/system-login-message-panel.component';
import { SystemWelcomeMessagePanelComponent } from './components/panels/system-welcome-message-panel/system-welcome-message-panel.component';
import { SystemViewMessagesComponent } from './containers/panel-groups/system-view-messages/system-view-messages.component';
import { SystemCallbackUrlPanelComponent } from './components/panels/system-callback-url-panel/system-callback-url-panel.component';
import { SystemStackTraceManagementPanelComponent } from './components/panels/system-stack-trace-management-panel/system-stack-trace-management-panel.component';
import { FormsModule } from '@angular/forms';
import { SystemCaseInsensitiveLoginPanelComponent } from './components/panels/system-case-insensitive-login-panel/system-case-insensitive-login-panel.component';
import { SystemMilestoneDeactivationPanelComponent } from './components/panels/system-milestone-deactivation-panel/system-milestone-deactivation-panel.component';
import { SystemLogsPanelComponent } from './components/panels/system-logs-panel/system-logs-panel.component';
import { SystemViewDownloadsComponent } from './containers/panel-groups/system-view-downloads/system-view-downloads.component';
import { SystemViewCleaningComponent } from './containers/panel-groups/system-view-cleaning/system-view-cleaning.component';
import { SystemCleaningPanelComponent } from './components/panels/system-cleaning-panel/system-cleaning-panel.component';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { DeleteAutomatedSuitesAndExecutionDialogComponent } from './components/dialogs/delete-automated-suites-and-execution-dialog/delete-automated-suites-and-execution-dialog.component';
import { SystemAutoconnectPanelComponent } from './components/panels/system-autoconnect-panel/system-autoconnect-panel.component';
import { SystemLicensePanelComponent } from './components/panels/system-license-panel/system-license-panel.component';
import { SystemViewReportTemplatesComponent } from './containers/panel-groups/system-view-report-templates/system-view-report-templates.component';
import { EditableReqReportTemplateGridComponentPanel } from './components/grids/editable-req-report-template-grid-panel/editable-req-report-template-grid-component-panel.component';
import { EditableTcReportTemplateGridComponentPanel } from './components/grids/editable-tc-report-template-grid-panel/editable-tc-report-template-grid-component-panel.component';
import { DeleteReportTemplateComponent } from './components/cell-renderers/delete-report-template/delete-report-template.component';
import { DownloadReportTemplateComponent } from './components/cell-renderers/download-report-template/download-report-template.component';
import { CampaignReportTemplateGridComponentPanel } from './components/grids/campaign-report-template-grid-panel/campaign-report-template-grid-component-panel.component';
import { IterationReportTemplateGridComponentPanel } from './components/grids/iteration-report-template-grid-panel/iteration-report-template-grid-component-panel.component';
import { AbstractReportTemplateGridPanelComponent } from './components/grids/abstract-report-template-grid-panel/abstract-report-template-grid-panel.component';
import { ImportReportTemplateDialogComponent } from './components/dialogs/import-report-template-dialog/import-report-template-dialog.component';
import { SynchronizationsSupervisionComponent } from './containers/panel-groups/synchronizations-supervision/synchronizations-supervision.component';
import { SystemGitlabSynchronizationGridPanelComponent } from './components/grids/system-gitlab-synchronization-grid-panel/system-gitlab-synchronization-grid-panel.component';
import { SystemJiraSynchronizationGridPanelComponent } from './components/grids/system-jira-synchronization-grid-panel/system-jira-synchronization-grid-panel.component';
import { ServerLinkCellForAdminComponent } from './components/cell-renderers/server-link-for-admin-cell/server-link-cell-for-admin.component';
import { AbstractSynchronizationGridPanelComponent } from './components/grids/abstract-synchronization-grid-panel/abstract-synchronization-grid-panel.component';
import { FilterSynchronizationStatusComponent } from './components/filters/filter-synchronization-status/filter-synchronization-status.component';
import { SystemAutomjiraSynchronizationGridPanelComponent } from './components/grids/system-automjira-synchronization-grid-panel/system-automjira-synchronization-grid-panel.component';
import { DownloadErrorLogFileCellComponent } from './components/cell-renderers/download-error-log-file-cell/download-error-log-file-cell.component';
import { RemoteSelectTypeCellComponent } from './components/cell-renderers/remote-select-type-cell/remote-select-type-cell.component';
import { SystemBannerMessagePanelComponent } from './components/panels/system-banner-message-panel/system-banner-message-panel.component';
import { SystemSearchActivationPanelComponent } from './components/panels/system-search-activation-panel/system-search-activation-panel.component';

export const routes: Routes = [
  {
    path: '',
    component: SystemWorkspaceComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'information',
      },
      {
        path: 'information',
        component: SystemViewInformationComponent,
      },
      {
        path: 'cleaning',
        component: SystemViewCleaningComponent,
      },
      {
        path: 'settings',
        component: SystemViewSettingsComponent,
      },
      {
        path: 'messages',
        component: SystemViewMessagesComponent,
      },
      {
        path: 'downloads',
        component: SystemViewDownloadsComponent,
      },
      {
        path: 'report-templates',
        component: SystemViewReportTemplatesComponent,
      },
      {
        path: 'synchronizations-supervision',
        component: SynchronizationsSupervisionComponent,
      },
    ],
  },
];

@NgModule({
  declarations: [
    SystemWorkspaceComponent,
    SystemViewInformationComponent,
    SystemStatisticsPanelComponent,
    SystemPluginsPanelComponent,
    SystemViewSettingsComponent,
    SystemAttachmentsPanelComponent,
    SystemViewMessagesComponent,
    SystemLoginMessagePanelComponent,
    SystemWelcomeMessagePanelComponent,
    SystemCallbackUrlPanelComponent,
    SystemStackTraceManagementPanelComponent,
    SystemCaseInsensitiveLoginPanelComponent,
    SystemLogsPanelComponent,
    SystemViewDownloadsComponent,
    SystemMilestoneDeactivationPanelComponent,
    SystemViewCleaningComponent,
    SystemCleaningPanelComponent,
    DeleteAutomatedSuitesAndExecutionDialogComponent,
    SystemAutoconnectPanelComponent,
    SystemLicensePanelComponent,
    SystemViewReportTemplatesComponent,
    EditableReqReportTemplateGridComponentPanel,
    EditableTcReportTemplateGridComponentPanel,
    DeleteReportTemplateComponent,
    DownloadReportTemplateComponent,
    CampaignReportTemplateGridComponentPanel,
    IterationReportTemplateGridComponentPanel,
    AbstractReportTemplateGridPanelComponent,
    ImportReportTemplateDialogComponent,
    SynchronizationsSupervisionComponent,
    SystemGitlabSynchronizationGridPanelComponent,
    SystemJiraSynchronizationGridPanelComponent,
    ServerLinkCellForAdminComponent,
    AbstractSynchronizationGridPanelComponent,
    FilterSynchronizationStatusComponent,
    SystemAutomjiraSynchronizationGridPanelComponent,
    DownloadErrorLogFileCellComponent,
    RemoteSelectTypeCellComponent,
    SystemBannerMessagePanelComponent,
    SystemSearchActivationPanelComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule,
    WorkspaceCommonModule,
    AnchorModule,
    NzToolTipModule,
    UiManagerModule,
    NavBarModule,
    NzCollapseModule,
    NzIconModule,
    NzSwitchModule,
    FormsModule,
    NzButtonModule,
    DialogModule,
    GridModule,
    CellRendererCommonModule,
    AttachmentModule,
  ],
})
export class SystemWorkspaceModule {}
