import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  ViewContainerRef,
} from '@angular/core';
import {
  AbstractCellRendererComponent,
  AutomationWorkflowTypes,
  ColumnDefinitionBuilder,
  DialogService,
  GridColumnId,
  GridService,
  PluginId,
  PluginType,
} from 'sqtm-core';
import { ProjectViewService } from '../../../services/project-view.service';
import { filter, map, take, takeUntil, withLatestFrom } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { AdminProjectViewComponentData } from '../../../containers/project-view/project-view.component';
import {
  DeactivatePluginDialogComponent,
  DeactivatePluginDialogConfiguration,
} from '../../dialogs/deactivate-plugin-dialog/deactivate-plugin-dialog.component';

@Component({
  selector: 'sqtm-app-project-plugin-enabled-cell',
  template: `
    @if (row) {
      <div class="wrapper">
        <nz-switch
          class="switch"
          [(ngModel)]="row.data['enabled']"
          [nzControl]="true"
          [nzCheckedChildren]="checkedTemplate"
          [nzUnCheckedChildren]="unCheckedTemplate"
          [attr.data-test-switch-id]="'change-plugin-availability'"
          (click)="changePluginAvailability()"
        >
        </nz-switch>
      </div>
      <ng-template #checkedTemplate
        ><i nz-icon nzType="check" class="table-icon-size"></i
      ></ng-template>
      <ng-template #unCheckedTemplate
        ><i nz-icon nzType="close" class="table-icon-size"></i
      ></ng-template>
    }
  `,
  styleUrls: ['./project-plugin-enabled-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectPluginEnabledCellComponent
  extends AbstractCellRendererComponent
  implements OnDestroy
{
  unsub$ = new Subject<void>();
  isProjectTemplate$: Observable<boolean>;
  isProjectBound$: Observable<boolean>;

  constructor(
    public grid: GridService,
    public cdr: ChangeDetectorRef,
    private projectViewService: ProjectViewService,
    private dialogService: DialogService,
    private vcRef: ViewContainerRef,
  ) {
    super(grid, cdr);
    this.isProjectTemplate$ = this.projectViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      map((componentData) => componentData.project?.template),
    );
    this.isProjectBound$ = this.projectViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      map((componentData) => componentData.project?.hasTemplateConfigurablePluginBinding),
    );
  }

  changePluginAvailability() {
    this.projectViewService.componentData$
      .pipe(
        takeUntil(this.unsub$),
        take(1),
        withLatestFrom(this.isProjectTemplate$, this.isProjectBound$),
      )
      .subscribe(
        ([componentData, isProjectTemplate, isProjectBound]: [
          AdminProjectViewComponentData,
          boolean,
          boolean,
        ]) => {
          if (this.row.data[GridColumnId.enabled]) {
            this.deactivatePlugin(componentData, isProjectTemplate, isProjectBound);
          } else {
            this.doEnablePlugin(componentData.project.id);
          }
        },
      );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private deactivatePlugin(
    componentData: AdminProjectViewComponentData,
    isProjectTemplate: boolean,
    isProjectBound,
  ): void {
    if (this.row.data.hasValidConfiguration && this.row.data.id !== PluginId.CAMPAIGN_ASSISTANT) {
      const dialogRef = this.dialogService.openDialog<
        DeactivatePluginDialogConfiguration,
        { confirm: boolean; saveConf: boolean }
      >({
        id: 'deactivate-plugin',
        data: {
          id: 'deactivate-plugin',
          titleKey: 'sqtm-core.administration-workspace.projects.dialog.title.deactivate-plugin',
          pluginName: this.row.data.name,
          helpMessage: this.getHelpMessage(isProjectTemplate, isProjectBound),
        },
        width: '80%',
        maxWidth: 600,
        component: DeactivatePluginDialogComponent,
        viewContainerReference: this.vcRef,
      });

      dialogRef.dialogClosed$
        .pipe(
          takeUntil(this.unsub$),
          filter((result) => result?.confirm),
        )
        .subscribe(({ saveConf }) => this.doDisablePlugin(componentData.project.id, saveConf));
    } else {
      this.doDisablePlugin(componentData.project.id, false);
    }
  }

  private getHelpMessage(isProjectTemplate: boolean, isProjectBound: boolean) {
    if (this.row.data.pluginType === PluginType.AUTOMATION) {
      return 'sqtm-core.administration-workspace.projects.dialog.message.deactivate-plugin.help-automation';
    }

    if (
      ['squash.tm.plugin.jirasync', 'squash.tm.plugin.xsquash4gitlab'].includes(this.row.data.id)
    ) {
      if (isProjectTemplate) {
        return 'sqtm-core.administration-workspace.projects.dialog.message.deactivate-plugin.help-jirasync.template';
      }

      if (isProjectBound) {
        return 'sqtm-core.administration-workspace.projects.dialog.message.deactivate-plugin.help-jirasync.bound-project';
      }
      return 'sqtm-core.administration-workspace.projects.dialog.message.deactivate-plugin.help-jirasync.project';
    }
    return 'sqtm-core.administration-workspace.projects.dialog.message.deactivate-plugin.help';
  }

  private doDisablePlugin(projectId: number, saveConf: boolean): void {
    if (this.row.data.pluginType === PluginType.AUTOMATION) {
      this.projectViewService
        .disabledWorkflowAutomJiraPlugin(projectId, AutomationWorkflowTypes.NONE, saveConf)
        .subscribe();
    } else {
      this.projectViewService
        .disablePlugin(projectId, this.row.data[GridColumnId.id], saveConf)
        .subscribe();
    }
  }

  private doEnablePlugin(projectId: number) {
    if (this.row.data.pluginType === PluginType.AUTOMATION) {
      this.projectViewService
        .enableWorkflowAutomJiraPlugin(projectId, AutomationWorkflowTypes.REMOTE_WORKFLOW)
        .subscribe();
    } else {
      this.projectViewService.enablePlugin(projectId, this.row.data[GridColumnId.id]).subscribe();
    }
  }
}

export function enablePluginColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(ProjectPluginEnabledCellComponent);
}
