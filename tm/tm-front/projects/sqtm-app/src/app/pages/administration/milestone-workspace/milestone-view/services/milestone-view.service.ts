import { Injectable } from '@angular/core';
import { AdminMilestoneState } from '../states/admin-milestone-state';
import {
  AdminReferentialDataService,
  AttachmentService,
  AuthenticatedUser,
  DateFormatUtils,
  EntityViewAttachmentHelperService,
  GenericEntityViewService,
  MilestoneAdminView,
  MilestonePossibleOwner,
  MilestoneRangeKeys,
  MilestoneStatusKeys,
  ProjectInfoForMilestoneAdminView,
  RestService,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import {
  AdminMilestoneViewState,
  provideInitialAdminMilestoneView,
} from '../states/admin-milestone-view-state';
import { createFeatureSelector } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, map, switchMap, take, tap, withLatestFrom } from 'rxjs/operators';

@Injectable()
export class MilestoneViewService extends GenericEntityViewService<
  AdminMilestoneState,
  'milestone'
> {
  public readonly isMilestoneOwner$: Observable<boolean>;
  public readonly canEditOwnerField$: Observable<boolean>;
  public readonly isMilestoneBoundToTemplate$: Observable<boolean>;

  constructor(
    protected restService: RestService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
    protected adminReferentialData: AdminReferentialDataService,
  ) {
    super(restService, attachmentService, translateService, attachmentHelper);

    this.isMilestoneOwner$ = this.componentData$.pipe(
      filter((componentData) => Boolean(componentData.milestone.id)),
      map((componentData) => componentData.milestone.ownerLogin),
      withLatestFrom(this.adminReferentialData.authenticatedUser$),
      map(([ownerLogin, currentUser]) => ownerLogin === currentUser.username),
    );

    this.canEditOwnerField$ = this.isMilestoneOwner$.pipe(
      withLatestFrom(this.adminReferentialData.loggedAsAdmin$, this.componentData$),
      map(([isOwner, isAdmin, componentData]) => {
        const rangeIsRestricted = componentData.milestone.range === 'RESTRICTED';
        return rangeIsRestricted && (isAdmin || isOwner);
      }),
    );

    this.isMilestoneBoundToTemplate$ = this.componentData$.pipe(
      map((componentData) => componentData.milestone),
      map((milestoneView: AdminMilestoneState) => {
        const foundTemplate = milestoneView.boundProjectsInformation.find(
          (project) => project.template,
        );
        return foundTemplate != null;
      }),
    );
  }

  public getInitialState(): AdminMilestoneViewState {
    return provideInitialAdminMilestoneView();
  }

  protected getRootUrl(_initialState?): string {
    return 'milestones';
  }

  load(milestoneId: number) {
    return this.restService
      .getWithoutErrorHandling<MilestoneAdminView>(['milestone-view', milestoneId.toString()])
      .subscribe({
        next: (milestone) => this.initializeMilestone(milestone),
        error: (err) => this.notifyEntityNotFound(err),
      });
  }

  private initializeMilestone(milestone: MilestoneAdminView): void {
    const milestoneState: AdminMilestoneState = {
      ...milestone,
      attachmentList: { id: null, attachments: null },
      endDate: DateFormatUtils.createDateFromIsoString(milestone.endDate),
      createdOn: DateFormatUtils.createDateFromIsoString(milestone.createdOn),
      lastModifiedOn: DateFormatUtils.createDateFromIsoString(milestone.lastModifiedOn),
    };
    this.initializeEntityState(milestoneState);
  }

  findPossibleOwners(): Observable<MilestonePossibleOwner[]> {
    const urlParts = ['milestones', 'possible-owners'];
    return this.restService.get<MilestonePossibleOwner[]>(urlParts);
  }

  setOwner(ownerLogin: string): Observable<any> {
    return this.state$.pipe(
      take(1),
      switchMap((state) => {
        const urlParts = ['milestones', state.milestone.id.toString(), 'owner'];
        return this.restService.post<MilestoneAdminView>(urlParts, { ownerLogin });
      }),
      withLatestFrom(this.state$),
      map(([updatedMilestone, state]) => ({
        ...state,
        milestone: {
          ...state.milestone,
          ...updatedMilestone,
          endDate: DateFormatUtils.createDateFromIsoString(updatedMilestone.endDate),
          createdOn: DateFormatUtils.createDateFromIsoString(updatedMilestone.createdOn),
          lastModifiedOn: DateFormatUtils.createDateFromIsoString(updatedMilestone.lastModifiedOn),
        },
      })),
      tap((newState) => this.requireExternalUpdate(newState.milestone.id, 'ownerLogin')),
      map((newState) => this.store.commit(newState)),
    );
  }

  setRange(range: MilestoneRangeKeys): Observable<any> {
    return this.state$.pipe(
      take(1),
      switchMap((state) => {
        const urlParts = ['milestones', state.milestone.id.toString(), 'range'];
        return this.restService.post<MilestoneAdminView>(urlParts, { range });
      }),
      withLatestFrom(this.state$),
      map(([updatedMilestone, state]) => ({
        ...state,
        milestone: {
          ...state.milestone,
          ...updatedMilestone,
          endDate: DateFormatUtils.createDateFromIsoString(updatedMilestone.endDate),
          createdOn: DateFormatUtils.createDateFromIsoString(updatedMilestone.createdOn),
          lastModifiedOn: DateFormatUtils.createDateFromIsoString(updatedMilestone.lastModifiedOn),
        },
      })),
      tap((newState) => this.requireExternalUpdate(newState.milestone.id, 'range')),
      map((newState) => this.store.commit(newState)),
    );
  }

  setStatus(status: MilestoneStatusKeys): Observable<any> {
    return this.state$.pipe(
      take(1),
      switchMap((state) => {
        const urlParts = ['milestones', state.milestone.id.toString(), 'status'];
        return this.restService.post<MilestoneAdminView>(urlParts, { status });
      }),
      withLatestFrom(this.state$),
      map(([updatedMilestone, state]) => ({
        ...state,
        milestone: {
          ...state.milestone,
          ...updatedMilestone,
          endDate: DateFormatUtils.createDateFromIsoString(updatedMilestone.endDate),
          createdOn: DateFormatUtils.createDateFromIsoString(updatedMilestone.createdOn),
          lastModifiedOn: DateFormatUtils.createDateFromIsoString(updatedMilestone.lastModifiedOn),
        },
      })),
      tap((newState) => this.requireExternalUpdate(newState.milestone.id, 'status')),
      map((newState) => this.store.commit(newState)),
    );
  }

  bindProjectsToMilestone(projectIds: number[]): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      switchMap((state: AdminMilestoneViewState) =>
        this.bindProjectsToMilestoneServerSide(state, projectIds),
      ),
      withLatestFrom(this.store.state$),
      map(
        ([response, state]: [
          { boundProjectsInformation: ProjectInfoForMilestoneAdminView[] },
          AdminMilestoneViewState,
        ]) => this.updateStateWithNewBoundProjects(state, response.boundProjectsInformation),
      ),
      tap((newState) =>
        this.requireExternalUpdate(newState.milestone.id, 'boundProjectsInformation'),
      ),
      tap((newState) => this.store.commit(newState)),
    );
  }

  private bindProjectsToMilestoneServerSide(state: AdminMilestoneViewState, projectIds: number[]) {
    const urlParts = [
      'milestone-binding',
      state.milestone.id.toString(),
      'bind-projects',
      projectIds.join(','),
    ];
    return this.restService.post(urlParts);
  }

  private updateStateWithNewBoundProjects(
    state: AdminMilestoneViewState,
    boundProjectsInformation: ProjectInfoForMilestoneAdminView[],
  ) {
    return {
      ...state,
      milestone: {
        ...state.milestone,
        boundProjectsInformation: boundProjectsInformation,
      },
    };
  }

  unbindProjectsFromMilestone(projectIds: number[]): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      switchMap((state: AdminMilestoneViewState) =>
        this.unbindProjectsFromMilestoneServerSide(state, projectIds),
      ),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminMilestoneViewState]) =>
        this.updateStateWithUnboundProjects(state, projectIds),
      ),
      tap((newState) =>
        this.requireExternalUpdate(newState.milestone.id, 'boundProjectsInformation'),
      ),
      tap((newState) => this.store.commit(newState)),
    );
  }

  private unbindProjectsFromMilestoneServerSide(
    adminMilestoneViewState: AdminMilestoneViewState,
    projectIds: number[],
  ) {
    const urlParts = [
      'milestone-binding',
      adminMilestoneViewState.milestone.id.toString(),
      'unbind-projects',
      projectIds.join(','),
    ];
    return this.restService.delete(urlParts);
  }

  private updateStateWithUnboundProjects(state: AdminMilestoneViewState, projectIds: number[]) {
    const boundProjectsInformation = [...state.milestone.boundProjectsInformation];
    const filteredBoundProjects = boundProjectsInformation.filter(
      (value) => !projectIds.includes(value.projectId),
    );
    return {
      ...state,
      milestone: {
        ...state.milestone,
        boundProjectsInformation: filteredBoundProjects,
      },
    };
  }

  unbindProjectsFromMilestoneAndKeepInPerimeter(projectIds: number[]) {
    return this.store.state$.pipe(
      take(1),
      switchMap((state: AdminMilestoneViewState) =>
        this.unbindProjectsFromMilestoneAndKeepInPerimeterServerSide(state, projectIds),
      ),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminMilestoneViewState]) =>
        this.updateStateWithUnboundProjectsKeptInPerimeter(state, projectIds),
      ),
      tap((newState) => this.store.commit(newState)),
    );
  }

  private unbindProjectsFromMilestoneAndKeepInPerimeterServerSide(
    state: AdminMilestoneViewState,
    projectIds: number[],
  ) {
    const urlParts = [
      'milestone-binding',
      state.milestone.id.toString(),
      'unbind-projects-and-keep-in-perimeter',
      projectIds.join(','),
    ];
    return this.restService.delete(urlParts);
  }

  private updateStateWithUnboundProjectsKeptInPerimeter(
    state: AdminMilestoneViewState,
    projectIds: number[],
  ) {
    const updatedBoundProjectsInformation = [...state.milestone.boundProjectsInformation];

    updatedBoundProjectsInformation.forEach((project: ProjectInfoForMilestoneAdminView) => {
      if (projectIds.includes(project.projectId)) {
        project.boundToMilestone = false;
        project.milestoneBoundToOneObjectOfProject = false;
      }
    });

    return {
      ...state,
      milestone: {
        ...state.milestone,
        boundProjectsInformation: updatedBoundProjectsInformation,
      },
    };
  }

  getCurrentUser(): Observable<AuthenticatedUser> {
    return this.adminReferentialData.authenticatedUser$;
  }
}

export const getMilestoneViewState = createFeatureSelector<AdminMilestoneState>('milestone');
