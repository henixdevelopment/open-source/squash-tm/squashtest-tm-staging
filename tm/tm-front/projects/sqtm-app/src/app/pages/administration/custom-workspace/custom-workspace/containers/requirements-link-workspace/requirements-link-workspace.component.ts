import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import {
  ADMIN_WS_REQUIREMENTS_LINKS_TABLE,
  ADMIN_WS_REQUIREMENTS_LINKS_TABLE_CONFIG,
} from '../../../custom-workspace.constant';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import { adminRequirementsLinksTableDefinition } from '../requirements-link-grid/requirements-link-grid.component';
import { Observable } from 'rxjs';
import { RequirementsLinkService } from '../../services/requirements-link.service';

@Component({
  selector: 'sqtm-app-requirements-link-workspace',
  templateUrl: './requirements-link-workspace.component.html',
  styleUrls: ['./requirements-link-workspace.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: ADMIN_WS_REQUIREMENTS_LINKS_TABLE_CONFIG,
      useFactory: adminRequirementsLinksTableDefinition,
      deps: [],
    },
    {
      provide: ADMIN_WS_REQUIREMENTS_LINKS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, ADMIN_WS_REQUIREMENTS_LINKS_TABLE_CONFIG, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: ADMIN_WS_REQUIREMENTS_LINKS_TABLE,
    },
    {
      provide: RequirementsLinkService,
    },
  ],
})
export class RequirementsLinkWorkspaceComponent implements OnInit, OnDestroy {
  authenticatedAdmin$: Observable<AuthenticatedUser>;

  constructor(
    public readonly adminReferentialDataService: AdminReferentialDataService,
    private gridService: GridService,
  ) {}

  ngOnInit(): void {
    this.authenticatedAdmin$ = this.adminReferentialDataService.authenticatedUser$;
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }
}
