import { Injectable } from '@angular/core';
import {
  AttachmentService,
  AuthenticatedUser,
  EntityViewAttachmentHelperService,
  EnvironmentVariable,
  GenericEntityViewService,
  GenericEntityViewState,
  RestService,
  AdminReferentialDataService,
  EnvironmentVariableOption,
} from 'sqtm-core';
import { AdminEnvironmentVariableState } from '../states/admin-environment-variable-state';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import {
  AdminEnvironmentVariableViewState,
  provideInitialAdminEnvironmentVariableView,
} from '../states/admin-environment-variable-view-state';
import { map, switchMap, take, withLatestFrom } from 'rxjs/operators';

@Injectable()
export class EnvironmentVariableViewService extends GenericEntityViewService<
  AdminEnvironmentVariableState,
  'environmentVariable'
> {
  constructor(
    protected restService: RestService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
    private adminReferentialDataService: AdminReferentialDataService,
  ) {
    super(restService, attachmentService, translateService, attachmentHelper);
  }

  getInitialState(): GenericEntityViewState<AdminEnvironmentVariableState, 'environmentVariable'> {
    return provideInitialAdminEnvironmentVariableView();
  }

  protected getRootUrl(): string {
    return 'environment-variables';
  }

  protected getCurrentUser(): Observable<AuthenticatedUser> {
    return this.adminReferentialDataService.authenticatedUser$;
  }

  load(evId: number) {
    return this.restService
      .getWithoutErrorHandling<EnvironmentVariable>(['environment-variable-view', evId.toString()])
      .subscribe({
        next: (environmentVariable) => this.initializeEnvironmentVariable(environmentVariable),
        error: (err) => this.notifyEntityNotFound(err),
      });
  }

  private initializeEnvironmentVariable(environmentVariable: EnvironmentVariable): void {
    const evState: AdminEnvironmentVariableState = {
      ...environmentVariable,
      attachmentList: { id: null, attachments: null },
    };
    this.initializeEntityState(evState);
  }

  changeOptionLabel(optionLabel: string, newLabel: string): Observable<void> {
    return this.state$.pipe(
      take(1),
      switchMap((state: AdminEnvironmentVariableViewState) =>
        this.changeOptionLabelServerSide(state, optionLabel, newLabel),
      ),
      withLatestFrom(this.store.state$),
      map(([, state]) => this.updateStateWithNewOptionLabel(state, optionLabel, newLabel)),
      map((nextState) => this.store.commit(nextState)),
    );
  }

  private changeOptionLabelServerSide(
    state: AdminEnvironmentVariableViewState,
    currentLabel: string,
    newLabel: string,
  ): Observable<void> {
    const requestBody = { currentLabel: currentLabel, newLabel: newLabel };
    const urlParts = [
      'environment-variables',
      state.environmentVariable.id.toString(),
      'options',
      'label',
    ];
    return this.restService.post(urlParts, requestBody);
  }

  private updateStateWithNewOptionLabel(
    state: AdminEnvironmentVariableViewState,
    currentLabel: string,
    newLabel: string,
  ): AdminEnvironmentVariableViewState {
    const updatedOptions = [...state.environmentVariable.options];

    updatedOptions.forEach((option: EnvironmentVariableOption) => {
      if (option.label === currentLabel) {
        option.label = newLabel;
      }
    });

    return {
      ...state,
      environmentVariable: {
        ...state.environmentVariable,
        options: updatedOptions,
      },
    };
  }

  addOption(option: OptionToAdd): Observable<void> {
    return this.state$.pipe(
      take(1),
      switchMap((state: AdminEnvironmentVariableViewState) =>
        this.addOptionServerSide(state, option),
      ),
      withLatestFrom(this.store.state$),
      map(([response, state]) => this.updateStateWithNewOption(state, response)),
      map((nextState) => this.store.commit(nextState)),
    );
  }

  addOptionServerSide(
    state: AdminEnvironmentVariableViewState,
    option: OptionToAdd,
  ): Observable<EnvironmentVariable> {
    const urlParts = [
      'environment-variables',
      state.environmentVariable.id.toString(),
      'options',
      'new',
    ];
    return this.restService.post(urlParts, option);
  }

  updateStateWithNewOption(
    state: AdminEnvironmentVariableViewState,
    response: EnvironmentVariable,
  ): AdminEnvironmentVariableViewState {
    return {
      ...state,
      environmentVariable: {
        ...state.environmentVariable,
        options: response.options,
      },
    };
  }

  deleteOptions(optionLabels: string[]): Observable<void> {
    return this.state$.pipe(
      take(1),
      switchMap((state: AdminEnvironmentVariableViewState) =>
        this.deleteOptionsServerSide(state, optionLabels),
      ),
      withLatestFrom(this.store.state$),
      map(([, state]) => this.updateStateWithRemovedOptions(state, optionLabels)),
      map((nextState) => this.store.commit(nextState)),
    );
  }

  private deleteOptionsServerSide(
    state: AdminEnvironmentVariableViewState,
    optionLabels: string[],
  ): Observable<void> {
    const urlParts = [
      'environment-variables',
      state.environmentVariable.id.toString(),
      'options',
      'remove',
    ];
    return this.restService.post(urlParts, { optionLabels: optionLabels });
  }

  private updateStateWithRemovedOptions(
    state: AdminEnvironmentVariableViewState,
    optionLabels: string[],
  ) {
    const updatedOptions = [...state.environmentVariable.options].filter(
      (option: EnvironmentVariableOption) => !optionLabels.includes(option.label),
    );
    return {
      ...state,
      environmentVariable: {
        ...state.environmentVariable,
        options: updatedOptions,
      },
    };
  }

  changeOptionPosition(optionIds: string[], newPosition: number) {
    return this.state$.pipe(
      take(1),
      switchMap((state: AdminEnvironmentVariableViewState) =>
        this.changeOptionPositionServerSide(optionIds, newPosition, state.environmentVariable.id),
      ),
      withLatestFrom(this.store.state$),
      map(
        ([environmentVariable, currentState]: [
          EnvironmentVariable,
          GenericEntityViewState<AdminEnvironmentVariableState, 'environmentVariable'>,
        ]) => this.updateStateAfterChangedOptionPosition(environmentVariable, currentState),
      ),
      map((nextState) => this.store.commit(nextState)),
    );
  }

  private updateStateAfterChangedOptionPosition(
    environmentVariable: EnvironmentVariable,
    state: GenericEntityViewState<AdminEnvironmentVariableState, 'environmentVariable'>,
  ) {
    return {
      ...state,
      environmentVariable: {
        ...state.environmentVariable,
        ...environmentVariable,
      },
    };
  }

  private changeOptionPositionServerSide(
    optionIds: string[],
    newPosition: number,
    environmentVariableId: number,
  ): Observable<EnvironmentVariable> {
    const urlParts = [
      'environment-variables',
      environmentVariableId.toString(),
      'options',
      'positions',
    ];
    const body = {
      labels: optionIds,
      position: newPosition,
    };
    return this.restService.post<EnvironmentVariable>(urlParts, body);
  }
}
interface OptionToAdd {
  label: string;
}
