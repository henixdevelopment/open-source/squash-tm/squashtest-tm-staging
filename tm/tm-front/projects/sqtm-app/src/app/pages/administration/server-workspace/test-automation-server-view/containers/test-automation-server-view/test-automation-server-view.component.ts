import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import {
  AdminReferentialDataService,
  DataRow,
  DialogService,
  GenericEntityViewComponentData,
  GenericEntityViewService,
  GridColumnId,
  GridService,
  RestService,
  TestAutomationServerKind,
} from 'sqtm-core';
import { TestAutomationServerViewService } from '../../services/test-automation-server-view.service';
import { Observable, Subject } from 'rxjs';
import { AdminTestAutomationServerViewState } from '../../states/admin-test-automation-server-view-state';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { concatMap, filter, map, take, takeUntil, withLatestFrom } from 'rxjs/operators';
import { AdminTestAutomationServerState } from '../../states/admin-test-automation-server-state';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-test-automation-server-view',
  templateUrl: './test-automation-server-view.component.html',
  styleUrls: ['./test-automation-server-view.component.less'],
  providers: [
    {
      provide: TestAutomationServerViewService,
      useClass: TestAutomationServerViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: TestAutomationServerViewService,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestAutomationServerViewComponent implements OnInit, OnDestroy {
  componentData$: Observable<AdminTestAutomationServerViewState>;
  isVisible: boolean;
  isUltimate$: Observable<boolean>;

  private readonly unsub$ = new Subject<void>();

  constructor(
    private readonly route: ActivatedRoute,
    public readonly testAutomationServerViewService: TestAutomationServerViewService,
    private readonly gridService: GridService,
    private cdRef: ChangeDetectorRef,
    private restService: RestService,
    private dialogService: DialogService,
    private adminReferentialDataService: AdminReferentialDataService,
    private translateService: TranslateService,
  ) {
    this.componentData$ = testAutomationServerViewService.componentData$.pipe(
      takeUntil(this.unsub$),
    );
    this.isUltimate$ = this.adminReferentialDataService.isUltimateLicenseAvailable$;
  }

  ngOnInit(): void {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('testAutomationServerId')),
      )
      .subscribe((id) => {
        this.testAutomationServerViewService.load(parseInt(id, 10));
      });

    this.prepareGridRefreshOnEntityChanges();
    this.adminReferentialDataService.callbackUrlIsNull$
      .pipe(takeUntil(this.unsub$), take(1))
      .subscribe((callbackUrlIsNull) => (this.isVisible = callbackUrlIsNull));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
    this.testAutomationServerViewService.complete();
  }

  handleDelete(): void {
    this.componentData$
      .pipe(
        take(1),
        withLatestFrom(this.gridService.selectedRows$),
        concatMap(([componentData, rows]: [AdminTestAutomationServerViewState, DataRow[]]) =>
          this.showConfirmDeleteTestAutomationServerDialog(
            componentData.testAutomationServer.id,
            componentData.testAutomationServer.name,
            rows,
          ),
        ),
        filter(({ confirmDelete }) => confirmDelete),
        concatMap(({ testAutomationServerId }) =>
          this.deleteTestAutomationServersServerSide(testAutomationServerId),
        ),
      )
      .subscribe(() => this.gridService.refreshData());
  }

  private showConfirmDeleteTestAutomationServerDialog(
    testAutomationServerId,
    testAutomationServerName: string,
    rows: DataRow[],
  ): Observable<{ confirmDelete: boolean; testAutomationServerId: string }> {
    const isJenkinsServerKind =
      rows.filter((row) => row.data[GridColumnId.kind] === TestAutomationServerKind.jenkins)
        .length > 0;

    const serverHasAutomationProjectWithExecution =
      rows.filter((row) => row.data[GridColumnId.executionCount] > 0).length > 0;

    const serverHasAutomationProject =
      rows.filter((row) => row.data[GridColumnId.projectCount] > 0).length > 0;

    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey:
        'sqtm-core.administration-workspace.servers.test-automation-servers.dialog.title.delete-one',
      messageKey: this.getDeletionMessageKey(
        isJenkinsServerKind,
        serverHasAutomationProjectWithExecution,
        serverHasAutomationProject,
        testAutomationServerName,
      ),
      level: 'DANGER',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map((confirmDelete) => ({ confirmDelete, testAutomationServerId })),
    );
  }

  private getDeletionMessageKey(
    isJenkinsServerKind: boolean,
    serverHasAutomationProjectWithExecution: boolean,
    serverHasAutomationProject: boolean,
    testAutomationServerName: string,
  ) {
    let messageKey = '';

    if (isJenkinsServerKind) {
      messageKey = this.translateService.instant(
        serverHasAutomationProjectWithExecution
          ? 'sqtm-core.administration-workspace.servers.test-automation-servers.dialog.message.delete-one-with-bound-project'
          : 'sqtm-core.administration-workspace.servers.test-automation-servers.dialog.message.delete-one-without-bound-project',
        { serverName: testAutomationServerName },
      );
    } else {
      messageKey = this.translateService.instant(
        serverHasAutomationProject
          ? 'sqtm-core.administration-workspace.servers.test-automation-servers.dialog.message.delete-one-orchestrator-with-bound-project'
          : 'sqtm-core.administration-workspace.servers.test-automation-servers.dialog.message.delete-one-without-bound-project',
        { serverName: testAutomationServerName },
      );
    }

    return messageKey;
  }

  private deleteTestAutomationServersServerSide(testAutomationServerId): Observable<void> {
    return this.restService.delete([`test-automation-servers`, testAutomationServerId]);
  }

  private prepareGridRefreshOnEntityChanges(): void {
    this.testAutomationServerViewService.simpleAttributeRequiringRefresh = ['name', 'baseUrl'];

    this.testAutomationServerViewService.externalRefreshRequired$
      .pipe(takeUntil(this.unsub$))
      .subscribe(() => this.gridService.refreshDataAndKeepSelectedRows());
  }
  defineRedirectionUrl(): string {
    return '/administration-workspace/system/settings';
  }
  shouldShowCallbackUrlBanner(): boolean {
    return this.isVisible;
  }
}

export interface AdminTestAutomationServerViewComponentData
  extends GenericEntityViewComponentData<AdminTestAutomationServerState, 'testAutomationServer'> {}
