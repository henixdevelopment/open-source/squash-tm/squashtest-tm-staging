import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProjectAssociatedComponent } from './project-associated.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppTestingUtilsModule } from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { ProjectViewService } from '../../../services/project-view.service';
import { GridService } from 'sqtm-core';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { mockGridService } from '../../../../../../../utils/testing-utils/mocks.service';
import { PROJECT_ASSOCIATED_TEMPLATE_TABLE } from '../../../project-view.constant';
import { of } from 'rxjs';
import createSpyObj = jasmine.createSpyObj;

describe('ProjectAssociatedComponent', () => {
  let component: ProjectAssociatedComponent;
  let fixture: ComponentFixture<ProjectAssociatedComponent>;
  const gridService = mockGridService();

  const projectViewService = createSpyObj(['getInitialState']);
  projectViewService.componentData$ = of({
    project: {
      allProjectBoundToTemplate: [],
    },
  });
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        AppTestingUtilsModule,
        TranslateModule.forRoot(),
        RouterTestingModule,
      ],
      declarations: [ProjectAssociatedComponent],
      providers: [
        {
          provide: GridService,
          useValue: gridService,
        },
        {
          provide: PROJECT_ASSOCIATED_TEMPLATE_TABLE,
          useValue: gridService,
        },
        {
          provide: ProjectViewService,
          useValue: {},
        },
        {
          provide: ProjectViewService,
          useValue: projectViewService,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectAssociatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', waitForAsync(() => {
    expect(component).toBeTruthy();
  }));
});
