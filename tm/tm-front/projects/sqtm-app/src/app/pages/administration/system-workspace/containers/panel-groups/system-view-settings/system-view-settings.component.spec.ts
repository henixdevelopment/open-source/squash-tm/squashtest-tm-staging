import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SystemViewSettingsComponent } from './system-view-settings.component';
import { EMPTY } from 'rxjs';
import { SystemViewService } from '../../../services/system-view.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { mockAdminReferentialDataService } from '../../../../../../utils/testing-utils/mocks.service';
import { AdminReferentialDataService } from 'sqtm-core';

describe('SystemViewSettingsComponent', () => {
  let component: SystemViewSettingsComponent;
  let fixture: ComponentFixture<SystemViewSettingsComponent>;

  const systemViewService = jasmine.createSpyObj(['load']);
  systemViewService['componentData$'] = EMPTY;

  const adminReferentialDataService = mockAdminReferentialDataService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SystemViewSettingsComponent],
      providers: [
        {
          provide: SystemViewService,
          useValue: systemViewService,
        },
        {
          provide: AdminReferentialDataService,
          useValue: adminReferentialDataService,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemViewSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
