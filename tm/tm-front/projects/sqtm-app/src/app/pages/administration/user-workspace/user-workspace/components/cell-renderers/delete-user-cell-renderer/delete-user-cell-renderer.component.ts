import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractDeleteCellRenderer,
  ActionErrorDisplayService,
  AdminReferentialDataService,
  ConfirmDeleteLevel,
  DialogService,
  GridService,
  RestService,
} from 'sqtm-core';
import { catchError, concatMap, finalize, take } from 'rxjs/operators';
import { UserWorkspaceService } from '../../../../services/user-workspace.service';

@Component({
  selector: 'sqtm-app-delete-user-cell-renderer',
  template: `
    <sqtm-core-delete-icon
      [show]="canDelete()"
      (delete)="showDeleteConfirm()"
    ></sqtm-core-delete-icon>
  `,
  styleUrls: ['./delete-user-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteUserCellRendererComponent extends AbstractDeleteCellRenderer {
  // We can cache authenticated user ID as it shouldn't change during this component lifetime
  private _currentUserId: number;

  constructor(
    public grid: GridService,
    cdr: ChangeDetectorRef,
    protected dialogService: DialogService,
    private restService: RestService,
    private adminReferentialDataService: AdminReferentialDataService,
    private userWorkspaceService: UserWorkspaceService,
    private actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    super(grid, cdr, dialogService);
    this.cacheCurrentUserId();
  }

  private cacheCurrentUserId(): void {
    this.adminReferentialDataService.authenticatedUser$
      .pipe(take(1))
      .subscribe((user) => (this._currentUserId = user.userId));
  }

  canDelete(): boolean {
    return this.row.data.partyId !== this._currentUserId;
  }

  protected doDelete(): any {
    this.grid.beginAsyncOperation();
    const partyId = this.row.data.partyId;
    this.userWorkspaceService
      .getUserSyncsServerSide(partyId)
      .pipe(
        concatMap((synchronisations) => {
          if (synchronisations.length > 0) {
            const dialogReference = this.userWorkspaceService.openHasSyncsAlert(synchronisations);
            return dialogReference.dialogClosed$.pipe(
              take(1),
              finalize(() => this.grid.completeAsyncOperation()),
            );
          } else {
            return this.restService.delete([`users/${partyId}`]).pipe(
              catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
              finalize(() => this.grid.completeAsyncOperation()),
            );
          }
        }),
      )
      .subscribe(() => this.grid.refreshData());
  }

  protected getTitleKey(): string {
    return 'sqtm-core.administration-workspace.users.dialog.title.delete-one';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.administration-workspace.users.dialog.message.delete-one';
  }

  protected getLevel(): ConfirmDeleteLevel {
    return 'DANGER';
  }
}
