import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { Observable } from 'rxjs';
import {
  AbstractDeleteCellRenderer,
  DialogService,
  GridService,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import { filter, finalize, take } from 'rxjs/operators';
import { ProjectViewService } from '../../../services/project-view.service';
import { AdminProjectViewComponentData } from '../../../containers/project-view/project-view.component';

@Component({
  selector: 'sqtm-app-unbind-custom-field-from-project-cell',
  template: `
    <sqtm-core-delete-icon
      [iconName]="getIcon()"
      (delete)="showDeleteConfirm()"
    ></sqtm-core-delete-icon>
  `,
  styleUrls: ['./unbind-custom-field-from-project-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UnbindCustomFieldFromProjectCellComponent extends AbstractDeleteCellRenderer {
  private componentData$: Observable<AdminProjectViewComponentData>;

  constructor(
    public grid: GridService,
    cdr: ChangeDetectorRef,
    dialogService: DialogService,
    private restService: RestService,
    private referentialDataService: ReferentialDataService,
    private projectViewService: ProjectViewService,
  ) {
    super(grid, cdr, dialogService);
    this.componentData$ = projectViewService.componentData$;
  }

  getIcon(): string {
    return 'sqtm-core-generic:unlink';
  }

  // Override
  showDeleteConfirm() {
    this.componentData$
      .pipe(
        filter((componentData: AdminProjectViewComponentData) => Boolean(componentData.project.id)),
        take(1),
      )
      .subscribe((componentData) => {
        if (componentData.project.linkedTemplateId != null) {
          this.openAlertForLockedParameter();
        } else {
          super.showDeleteConfirm();
        }
      });
  }

  protected doDelete() {
    this.grid.beginAsyncOperation();
    const customFieldBindingId = this.row.data.id;
    this.projectViewService
      .unbindCustomFields([customFieldBindingId])
      .pipe(finalize(() => this.grid.completeAsyncOperation()))
      .subscribe();
  }

  protected getTitleKey(): string {
    return 'sqtm-core.administration-workspace.projects.dialog.title.unbind-custom-field-from-project.unbind-one';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.administration-workspace.projects.dialog.message.unbind-custom-field-from-project.unbind-one';
  }

  private openAlertForLockedParameter() {
    this.dialogService.openAlert({
      titleKey:
        'sqtm-core.administration-workspace.projects.dialog.title.parameter-locked-because-project-is-bound-to-a-template',
      messageKey:
        'sqtm-core.administration-workspace.projects.dialog.message.parameter-locked-because-project-is-bound-to-a-template',
      level: 'WARNING',
    });
  }
}
