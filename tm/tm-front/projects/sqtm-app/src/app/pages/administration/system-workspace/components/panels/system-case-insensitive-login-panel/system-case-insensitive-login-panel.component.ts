import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { SystemViewState } from '../../../states/system-view.state';
import { SystemViewService } from '../../../services/system-view.service';

@Component({
  selector: 'sqtm-app-system-case-insensitive-login-panel',
  templateUrl: './system-case-insensitive-login-panel.component.html',
  styleUrls: [
    './system-case-insensitive-login-panel.component.less',
    '../../../styles/system-workspace.common.less',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SystemCaseInsensitiveLoginPanelComponent {
  @Input()
  componentData: SystemViewState;

  constructor(private systemViewService: SystemViewService) {}

  changeCaseInsensitiveLoginEnabled() {
    const isActive = this.componentData.caseInsensitiveLogin;
    const newStatus = !isActive;
    this.systemViewService.changeCaseInsensitiveLoginEnabled(newStatus);
  }

  changeCaseInsensitiveActionsEnabled() {
    const isActive = this.componentData.caseInsensitiveActions;
    const newStatus = !isActive;
    this.systemViewService.changeCaseInsensitiveActionsEnabled(newStatus);
  }

  checkIdDuplicateLogins() {
    return this.componentData.duplicateLogins.length > 0;
  }

  checkHasDuplicateActions() {
    return this.componentData.duplicateActions.length > 0;
  }
}
