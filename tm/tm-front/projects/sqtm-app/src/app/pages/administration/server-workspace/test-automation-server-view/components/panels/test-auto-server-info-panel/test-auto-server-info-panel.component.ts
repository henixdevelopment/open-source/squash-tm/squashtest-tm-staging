import { ChangeDetectionStrategy, Component, Input, ViewContainerRef } from '@angular/core';
import { AdminTestAutomationServerViewComponentData } from '../../../containers/test-automation-server-view/test-automation-server-view.component';
import { TestAutomationServerViewService } from '../../../services/test-automation-server-view.service';
import {
  getTestAutomationServerKindI18nKey,
  TestAutomationServerKind,
  DialogService,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { OrchestratorVersionDialogComponent } from '../../dialogs/orchestrator-version-dialog/orchestrator-version-dialog.component';

const SQUASH_AUTOM_DOC_URL_EN =
  'https://tm-en.doc.squashtest.com/latest/admin-guide/manage-servers/managing-autom-server.html?utm_source=Appli_squash&utm_medium=link#configure-a-squash-orchestrator-test-automation-server';
const SQUASH_AUTOM_DOC_URL_FR =
  'https://tm-fr.doc.squashtest.com/latest/admin-guide/gestion-serveurs/gerer-serveur-autom.html?utm_source=Appli_squash&utm_medium=link#configurer-un-serveur-dexecution-automatisee-squash-orchestrator';

@Component({
  selector: 'sqtm-app-test-automation-server-information-panel',
  templateUrl: './test-auto-server-info-panel.component.html',
  styleUrls: ['./test-auto-server-info-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestAutoServerInfoPanelComponent {
  @Input()
  componentData: AdminTestAutomationServerViewComponentData;

  get isManualSlaveSelectionVisible(): boolean {
    return this.componentData.testAutomationServer.kind === TestAutomationServerKind.jenkins;
  }

  get orchestratorConfigurationDocUrl(): string {
    return this.translateService.getBrowserLang() === 'fr'
      ? SQUASH_AUTOM_DOC_URL_FR
      : SQUASH_AUTOM_DOC_URL_EN;
  }

  constructor(
    private testAutomationServerViewService: TestAutomationServerViewService,
    private dialogService: DialogService,
    private vcr: ViewContainerRef,
    private translateService: TranslateService,
  ) {}

  changeManualSlaveSelection(selected: boolean) {
    this.testAutomationServerViewService.changeManualSlaveSelection(selected);
  }

  getServerKind(componentData: AdminTestAutomationServerViewComponentData): string {
    return getTestAutomationServerKindI18nKey(
      componentData.testAutomationServer.kind as TestAutomationServerKind,
    );
  }

  isSquashAutomServer(componentData: AdminTestAutomationServerViewComponentData): boolean {
    return componentData.testAutomationServer.kind === TestAutomationServerKind.squashOrchestrator;
  }

  deduceObserverUrl(componentData: AdminTestAutomationServerViewComponentData): string {
    const trimmedObserverUrl = (componentData.testAutomationServer.observerUrl ?? '').trim();
    return trimmedObserverUrl || componentData.testAutomationServer.baseUrl;
  }

  deduceEventBusUrl(componentData: AdminTestAutomationServerViewComponentData): string {
    const trimmedObserverUrl = (componentData.testAutomationServer.eventBusUrl ?? '').trim();
    return trimmedObserverUrl || componentData.testAutomationServer.baseUrl;
  }

  deduceKillSwitchUrl(componentData: AdminTestAutomationServerViewComponentData): string {
    const trimmedKillSwitchUrl = (componentData.testAutomationServer.killSwitchUrl ?? '').trim();
    return trimmedKillSwitchUrl || componentData.testAutomationServer.baseUrl;
  }

  getUrlFieldLabelKey(componentData: AdminTestAutomationServerViewComponentData): string {
    return this.isSquashAutomServer(componentData)
      ? 'sqtm-core.administration-workspace.servers.test-automation-servers.squashautom.receptionist-url.label'
      : 'sqtm-core.entity.generic.url.label';
  }

  reloadEnvironmentFromUrlChange(url: string, key: string) {
    this.testAutomationServerViewService
      .reloadEnvironmentFromUrlChange(url, this.componentData.testAutomationServer.id, key)
      .subscribe();
  }

  openOrchestratorDetailDialog(event: any) {
    event.preventDefault();
    this.dialogService.openDialog({
      id: 'orchestrator-version-information',
      component: OrchestratorVersionDialogComponent,
      viewContainerReference: this.vcr,
      data: {
        serverId: this.componentData.testAutomationServer.id,
      },
      width: 600,
    });
  }
}
