import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {
  AdminReferentialDataService,
  AttachmentService,
  AuthenticatedUser,
  DateFormatUtils,
  EntityViewAttachmentHelperService,
  GenericEntityViewService,
  RestService,
  Team,
} from 'sqtm-core';
import { createFeatureSelector } from '@ngrx/store';
import { AdminTeamState } from '../states/admin-team-state';
import { AdminTeamViewState, provideInitialAdminTeamView } from '../states/admin-team-view-state';
import { concatMap, map, switchMap, take, tap, withLatestFrom } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export class TeamViewService extends GenericEntityViewService<AdminTeamState, 'team'> {
  public readonly memberCount$: Observable<number>;
  public readonly authorisationCount$: Observable<number>;

  constructor(
    protected restService: RestService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
    private adminReferentialDataService: AdminReferentialDataService,
  ) {
    super(restService, attachmentService, translateService, attachmentHelper);

    this.memberCount$ = this.componentData$.pipe(
      map((componentData) => componentData.team.members.length),
    );

    this.authorisationCount$ = this.componentData$.pipe(
      map((componentData) => componentData.team.projectPermissions.length),
    );
  }

  public getInitialState(): AdminTeamViewState {
    return provideInitialAdminTeamView();
  }

  public load(teamId: number) {
    this.restService.getWithoutErrorHandling<Team>(['team-view', teamId.toString()]).subscribe({
      next: (team) => this.initializeTeam(team),
      error: (err) => this.notifyEntityNotFound(err),
    });
  }

  private initializeTeam(team: Team) {
    const teamState: AdminTeamState = {
      ...team,
      attachmentList: { id: null, attachments: null },
      createdOn: DateFormatUtils.createDateFromIsoString(team.createdOn),
      lastModifiedOn: DateFormatUtils.createDateFromIsoString(team.lastModifiedOn),
    };
    this.initializeEntityState(teamState);
  }

  protected getRootUrl(_initialState?): string {
    return 'teams';
  }

  setTeamAuthorisation(projectIds: number[], authorisedProfile: string): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      switchMap((state: AdminTeamViewState) =>
        this.setTeamAuthorisationsServerSide(state, projectIds, authorisedProfile),
      ),
      withLatestFrom(this.store.state$),
      map(([response, state]: [any, AdminTeamViewState]) =>
        this.updateStateWithNewProjectPermissions(state, response),
      ),
      tap((state) => this.store.commit(state)),
    );
  }

  private setTeamAuthorisationsServerSide(
    adminTeamViewState: AdminTeamViewState,
    projectIds: number[],
    authorisedProfile: string,
  ) {
    const urlParts = [
      this.getRootUrl(),
      adminTeamViewState.team.id.toString(),
      'projects',
      projectIds.join(','),
      'permissions',
      authorisedProfile,
    ];
    return this.restService.post(urlParts, {});
  }

  private updateStateWithNewProjectPermissions(state: AdminTeamViewState, response) {
    return {
      ...state,
      team: {
        ...state.team,
        projectPermissions: response.projectPermissions,
      },
    };
  }

  removeAuthorisation(projectIds: number[]): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      switchMap((adminTeamViewState: AdminTeamViewState) =>
        this.removeTeamAuthorisationsServerSide(adminTeamViewState, projectIds),
      ),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminTeamViewState]) =>
        this.updateStateWithRemovedProjectPermissions(state, projectIds),
      ),
      tap((newState) => this.store.commit(newState)),
    );
  }

  private removeTeamAuthorisationsServerSide(
    adminTeamViewState: AdminTeamViewState,
    projectIds: number[],
  ) {
    const urlParts = [
      this.getRootUrl(),
      adminTeamViewState.team.id.toString(),
      'permissions',
      projectIds.join(','),
    ];
    return this.restService.delete(urlParts);
  }

  private updateStateWithRemovedProjectPermissions(
    state: AdminTeamViewState,
    projectIds: number[],
  ) {
    const projectPermissions = [...state.team.projectPermissions];
    const filteredProjectPerm = projectPermissions.filter(
      (value) => !projectIds.includes(value.projectId),
    );
    return {
      ...state,
      team: {
        ...state.team,
        projectPermissions: filteredProjectPerm,
      },
    };
  }

  addTeamMembers(memberLogins: string[]): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      switchMap((adminTeamViewState: AdminTeamViewState) =>
        this.addTeamMembersServerSide(adminTeamViewState, memberLogins),
      ),
      withLatestFrom(this.store.state$),
      map(([response, state]: [any, AdminTeamViewState]) =>
        this.updateStateWithNewMembers(state, response),
      ),
      tap((state) => this.store.commit(state)),
      tap((state: AdminTeamViewState) => this.requireExternalUpdate(state.team.id, 'members')),
    );
  }

  private addTeamMembersServerSide(adminTeamViewState: AdminTeamViewState, memberLogins: string[]) {
    const urlParts = [
      this.getRootUrl(),
      adminTeamViewState.team.id.toString(),
      'members',
      memberLogins.join(','),
    ];
    return this.restService.post(urlParts);
  }

  private updateStateWithNewMembers(state: AdminTeamViewState, response) {
    return {
      ...state,
      team: {
        ...state.team,
        members: response.members,
      },
    };
  }

  removeTeamMember(partyIds: number[]): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      concatMap((adminTeamViewState: AdminTeamViewState) =>
        this.removeTeamMembersServerSide(adminTeamViewState, partyIds),
      ),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminTeamViewState]) =>
        this.updateStateWithRemovedMembers(state, partyIds),
      ),
      tap((state) => this.store.commit(state)),
      tap((state: AdminTeamViewState) => this.requireExternalUpdate(state.team.id, 'members')),
    );
  }

  private removeTeamMembersServerSide(
    adminTeamViewState: AdminTeamViewState,
    partyIds: number[],
  ): Observable<void> {
    const urlParts = [
      this.getRootUrl(),
      adminTeamViewState.team.id.toString(),
      'members',
      partyIds.join(','),
    ];
    return this.restService.delete(urlParts);
  }

  private updateStateWithRemovedMembers(state: AdminTeamViewState, partyIds: number[]) {
    const updatedMembers = [...state.team.members];
    const filteredMembers = updatedMembers.filter((value) => !partyIds.includes(value.partyId));
    return {
      ...state,
      team: {
        ...state.team,
        members: filteredMembers,
      },
    };
  }

  getCurrentUser(): Observable<AuthenticatedUser> {
    return this.adminReferentialDataService.authenticatedUser$;
  }

  hasCustomProfile(teamId: number): Observable<boolean> {
    const urlParts = ['team-view', teamId.toString(), 'has-custom-profile'];

    return this.restService.get<boolean>(urlParts);
  }
}

export const getTeamViewState = createFeatureSelector<AdminTeamState>('team');
