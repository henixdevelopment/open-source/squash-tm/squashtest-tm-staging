import {
  BindableEntity,
  CustomFieldOption,
  Identifier,
  InputType,
  SqtmGenericEntityState,
} from 'sqtm-core';

export interface AdminCustomFieldState extends SqtmGenericEntityState {
  id: number;
  code: string;
  label: string;
  name: string;
  defaultValue?: string;
  largeDefaultValue?: string;
  numericDefaultValue?: number;
  inputType: InputType;
  optional: boolean;
  options: CustomFieldOption[];
  boundProjectsToCuf: BoundProjectsToCuf[];
}

export interface BoundProjectsToCuf {
  projectId: Identifier;
  bindableEntity: BindableEntity;
  projectName: string;
  customFieldBindingId: number;
}
