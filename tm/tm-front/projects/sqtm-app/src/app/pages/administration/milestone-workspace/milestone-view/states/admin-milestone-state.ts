import { ProjectInfoForMilestoneAdminView, SqtmGenericEntityState } from 'sqtm-core';

export interface AdminMilestoneState extends SqtmGenericEntityState {
  id: number;
  label: string;
  description: string;
  endDate: Date;
  status: string;
  range: string;
  ownerFistName: string;
  ownerLastName: string;
  ownerLogin: string;
  createdBy: string;
  createdOn: Date;
  lastModifiedBy: string;
  lastModifiedOn: Date;
  canEdit: boolean;
  boundProjectsInformation: ProjectInfoForMilestoneAdminView[];
}
