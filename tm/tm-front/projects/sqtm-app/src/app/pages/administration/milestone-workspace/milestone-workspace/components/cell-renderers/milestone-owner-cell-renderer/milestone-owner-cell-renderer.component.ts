import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  formatMilestoneOwner,
  GridColumnId,
  GridService,
  Milestone,
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-milestone-owner-cell-renderer',
  template: `
    @if (columnDisplay && row) {
      <div class="full-width full-height flex-column">
        @if (row.data[columnDisplay.id]) {
          <span
            style="margin: auto 5px;"
            class="sqtm-grid-cell-txt-renderer"
            nz-tooltip
            [sqtmCoreLabelTooltip]="milestoneOwner | translate"
          >
            {{ milestoneOwner | translate }}
          </span>
        }
      </div>
    }
  `,
  styleUrls: ['./milestone-owner-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MilestoneOwnerCellRendererComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }

  get milestoneOwner(): string {
    return formatMilestoneOwner(this.row.data as Milestone);
  }
}

export function milestoneOwnerColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(MilestoneOwnerCellRendererComponent);
}
