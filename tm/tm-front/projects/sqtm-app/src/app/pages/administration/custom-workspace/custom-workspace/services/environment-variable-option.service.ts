import { Injectable } from '@angular/core';
import { createStore, Identifier, RestService, Store } from 'sqtm-core';
import { Observable } from 'rxjs';
import { map, take, tap, withLatestFrom } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class EnvironmentVariableOptionService {
  public readonly store: Store<EnvironmentVariableOptionState>;
  public readonly state$: Observable<EnvironmentVariableOptionState>;
  public readonly environmentVariableOptions$: Observable<EnvironmentVariableOption[]>;

  constructor(protected restService: RestService) {
    this.store = createStore<EnvironmentVariableOptionState>(
      getInitialEnvironmentVariableOptionState(),
    );
    this.state$ = this.store.state$;
    this.environmentVariableOptions$ = this.store.state$.pipe(
      map((state) => state.environmentVariableOptions),
    );
  }

  initialize() {
    this.store.commit(getInitialEnvironmentVariableOptionState());
  }

  addOption(optionLabel: string): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, EnvironmentVariableOptionState]) =>
        this.updateStateWithNewOption(optionLabel, state),
      ),
      tap((state) => this.store.commit(state)),
    );
  }

  private updateStateWithNewOption(
    optionLabel: string,
    state: EnvironmentVariableOptionState,
  ): EnvironmentVariableOptionState {
    const updateEnvironmentVariableOptions: EnvironmentVariableOption[] = [
      ...state.environmentVariableOptions,
    ];
    const option: EnvironmentVariableOption = {
      id: optionLabel,
      label: optionLabel,
    };

    updateEnvironmentVariableOptions.push(option);

    return {
      ...state,
      environmentVariableOptions: updateEnvironmentVariableOptions,
    };
  }

  removeOption(optionLabel: string): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      map((state: EnvironmentVariableOptionState) => {
        const updateEnvironmentVariableOptions = [...state.environmentVariableOptions].filter(
          (option) => option.label !== optionLabel,
        );

        return {
          ...state,
          environmentVariableOptions: updateEnvironmentVariableOptions,
        };
      }),
      tap((state) => this.store.commit(state)),
    );
  }
}

interface EnvironmentVariableOptionState {
  environmentVariableOptions: EnvironmentVariableOption[];
}

export interface EnvironmentVariableOption {
  id: Identifier;
  label: string;
}

function getInitialEnvironmentVariableOptionState(): EnvironmentVariableOptionState {
  return {
    environmentVariableOptions: [],
  };
}
