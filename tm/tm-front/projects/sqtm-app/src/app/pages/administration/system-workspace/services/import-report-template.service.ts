import { Injectable } from '@angular/core';
import {
  initialReportTemplateImportState,
  ReportTemplateImportComponentData,
  ReportTemplateImportState,
} from '../states/report-template-import.state';
import { createStore, DocXReportId, RestService, Store } from 'sqtm-core';
import { Observable } from 'rxjs';
import { map, withLatestFrom } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ImportReportTemplateService {
  store: Store<ReportTemplateImportState>;

  state$: Observable<ReportTemplateImportState>;

  componentData$: Observable<ReportTemplateImportComponentData>;

  public getSnapshot(): ReportTemplateImportState {
    return this.store.getSnapshot();
  }

  constructor(private restService: RestService) {
    const initialState = initialReportTemplateImportState();
    this.store = createStore(initialState);

    this.state$ = this.store.state$;

    this.componentData$ = this.store.state$;
  }

  saveFile(file: File) {
    const updatedState = {
      ...this.getSnapshot(),
      file,
    };

    this.store.commit(updatedState);
  }

  resetToInitialState() {
    this.store.commit(initialReportTemplateImportState());
  }

  checkFileExistsInFolder(reportId: DocXReportId, fileName: string): Observable<boolean> {
    return this.restService
      .post(['reports', reportId, 'template-exists-in-folder'], { templateFileName: fileName })
      .pipe(
        withLatestFrom(this.state$),
        map(([templateExists, state]: [boolean, ReportTemplateImportState]) => {
          if (templateExists) {
            state.currentStep = 'CONFIRM';
            this.store.commit(state);
            return templateExists;
          }
        }),
      );
  }

  addTemplateInFolder(reportId: DocXReportId, file: File): Observable<boolean> {
    const formData = new FormData();
    formData.append('archive', file, file.name);

    return this.restService.post(['reports', reportId, 'add-report-template-file'], formData);
  }
}
