import { ChangeDetectionStrategy, Component, OnInit, signal, WritableSignal } from '@angular/core';
import { catchError, take, tap } from 'rxjs/operators';
import {
  doesHttpErrorContainsSquashActionError,
  extractSquashActionError,
  DialogReference,
  ImportType,
  PivotFormatImport,
  ProjectImportService,
  SquashActionError,
} from 'sqtm-core';
import { HttpErrorResponse } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-import-from-pivot-format-dialog',
  templateUrl: './import-from-pivot-format-dialog.component.html',
  styleUrl: './import-from-pivot-format-dialog.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImportFromPivotFormatDialogComponent implements OnInit {
  $allowClose: WritableSignal<boolean> = signal(false);

  $isLoading: WritableSignal<boolean> = signal(true);
  $displayErrorMessage: WritableSignal<boolean> = signal(false);
  $errorMessage: WritableSignal<string> = signal('sqtm-core.error.generic.label');

  constructor(
    public dialogReference: DialogReference,
    private projectImportService: ProjectImportService,
    private translateService: TranslateService,
  ) {}

  handleClose() {
    this.dialogReference.close();
  }

  ngOnInit(): void {
    this.projectImportService
      .importProjectFromPivotFormatFile(
        this.dialogReference.data.files,
        'Standard import placeholder name',
        ImportType.STANDARD,
        this.dialogReference.data.projectId,
      )
      .pipe(
        take(1),
        tap(() => {
          this.$isLoading.set(false);
          this.$allowClose.set(true);
        }),
        catchError((error) => {
          this.handleError(error);
          throw error;
        }),
      )
      .subscribe((response: AddPivotFormatImportResponse) => {
        this.dialogReference.result = response;
      });
  }

  private handleError(error: HttpErrorResponse) {
    this.handleSquashTMError(error);
    this.$isLoading.set(false);
    this.$displayErrorMessage.set(true);
    this.$allowClose.set(true);
  }

  private handleSquashTMError(error: HttpErrorResponse) {
    if (doesHttpErrorContainsSquashActionError(error)) {
      const squashError: SquashActionError = extractSquashActionError(error);
      const errorMessage = this.translateService.instant(
        squashError.actionValidationError.i18nKey ?? 'sqtm-core.error.generic.label',
        squashError.actionValidationError.i18nParams,
      );

      this.$errorMessage.set(errorMessage);
    }
  }
}

export interface AddPivotFormatImportResponse {
  existingImports: PivotFormatImport[];
}
