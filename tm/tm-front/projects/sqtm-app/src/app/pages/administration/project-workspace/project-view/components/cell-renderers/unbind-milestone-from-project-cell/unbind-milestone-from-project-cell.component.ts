import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Signal } from '@angular/core';
import {
  AbstractDeleteCellRenderer,
  AdminReferentialDataService,
  AuthenticatedUser,
  DialogService,
  GridService,
  Permissions,
  RestService,
} from 'sqtm-core';
import { ProjectViewService } from '../../../services/project-view.service';
import { finalize } from 'rxjs/operators';
import { toSignal } from '@angular/core/rxjs-interop';
import { AdminProjectViewComponentData } from '../../../containers/project-view/project-view.component';

@Component({
  selector: 'sqtm-app-unbind-milestone-from-project-cell',
  template: `
    <sqtm-core-delete-icon
      [iconName]="icon"
      (delete)="showDeleteConfirm()"
      [show]="canDelete"
    ></sqtm-core-delete-icon>
  `,
  styleUrls: ['./unbind-milestone-from-project-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UnbindMilestoneFromProjectCellComponent extends AbstractDeleteCellRenderer {
  protected $componentData: Signal<AdminProjectViewComponentData>;
  protected $authenticatedUser: Signal<AuthenticatedUser>;

  constructor(
    public grid: GridService,
    cdr: ChangeDetectorRef,
    dialogService: DialogService,
    private restService: RestService,
    private projectViewService: ProjectViewService,
    private readonly adminReferentialDataService: AdminReferentialDataService,
  ) {
    super(grid, cdr, dialogService);
    this.$componentData = toSignal(this.projectViewService.componentData$);
    this.$authenticatedUser = toSignal(adminReferentialDataService.authenticatedUser$);
  }

  get icon(): string {
    return 'sqtm-core-generic:unlink';
  }

  get canDelete(): boolean {
    return (
      this.$authenticatedUser().admin ||
      this.$componentData().project.permissions.PROJECT?.includes(Permissions.MANAGE_MILESTONE)
    );
  }

  protected doDelete(): any {
    this.grid.beginAsyncOperation();
    const boundMilestonesId = this.row.data.id;
    this.projectViewService
      .unbindMilestones([boundMilestonesId])
      .pipe(finalize(() => this.grid.completeAsyncOperation()))
      .subscribe();
  }

  protected getTitleKey(): string {
    return 'sqtm-core.administration-workspace.projects.dialog.title.unbind-milestone-from-project.unbind-one';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.administration-workspace.projects.dialog.message.unbind-milestone-from-project.unbind-one';
  }
}
