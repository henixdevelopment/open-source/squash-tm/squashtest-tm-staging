import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  Signal,
  ViewChild,
} from '@angular/core';
import { ImportFromXrayState, ImportFromXraySteps } from '../../state/import-from-xray.state';
import { ImportFromXrayService } from '../../services/ImportFromXrayService';
import { DialogReference, ProjectImportService, ProjectImportXrayService } from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { ImportFromXrayFormatConfigurationComponent } from '../../component/import-from-xray-configuration/import-from-xray-format-configuration.component';

@Component({
  selector: 'sqtm-app-import-from-xray-dialog',
  templateUrl: './import-from-xray-dialog.component.html',
  styleUrl: './import-from-xray-dialog.component.less',
  providers: [
    {
      provide: ImportFromXrayService,
      useClass: ImportFromXrayService,
      deps: [ProjectImportXrayService, ProjectImportService, TranslateService],
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImportFromXrayDialogComponent implements OnInit, OnDestroy {
  $componentData: Signal<ImportFromXrayState> = this.importFromXrayService.$state;

  @ViewChild(ImportFromXrayFormatConfigurationComponent)
  configurationComponent: ImportFromXrayFormatConfigurationComponent;

  constructor(
    private importFromXrayService: ImportFromXrayService,
    private dialogReference: DialogReference,
  ) {}

  ngOnInit(): void {
    this.initializeComponentData();
  }

  ngOnDestroy(): void {
    this.importFromXrayService.deletePivotTempFiles();
    this.importFromXrayService.complete();
  }

  initializeComponentData() {
    this.importFromXrayService.addProjectId(this.dialogReference.data.projectId);
  }

  handleClose() {
    this.dialogReference.result = this.$componentData().importResult;
    this.dialogReference.close();
  }

  handlePrevious() {
    switch (this.$componentData().currentStep) {
      case ImportFromXraySteps.CONFIRMATION:
        this.importFromXrayService.backToConfiguration();
        break;
      case ImportFromXraySteps.GENERATE:
        this.importFromXrayService.backToConfirmation();
        break;
    }
  }

  handleNext() {
    switch (this.$componentData().currentStep) {
      case ImportFromXraySteps.CONFIGURATION:
        this.handleNextToConfirmation();
        break;
      case ImportFromXraySteps.CONFIRMATION:
        this.handleNextToGeneratePivot();
        break;
    }
  }

  handleNextToConfirmation() {
    const isMissingFile = this.importFromXrayService.isMissingFile();
    const isValidForm = this.configurationComponent.isValidForm();
    if (!isMissingFile && isValidForm) {
      this.importFromXrayService.accessToConfirmationImport();
    }
  }

  handleNextToGeneratePivot() {
    this.importFromXrayService.accessToGeneratePivot();
  }

  handleNextToImport() {
    this.importFromXrayService.accessToImport();
  }

  canImport() {
    return this.$componentData().loadingData || this.$componentData().errors.importFailed;
  }

  protected readonly ImportFromXraySteps = ImportFromXraySteps;
}
