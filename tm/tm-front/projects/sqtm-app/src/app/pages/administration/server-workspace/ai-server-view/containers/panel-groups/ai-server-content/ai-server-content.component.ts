import { ChangeDetectionStrategy, Component, Signal } from '@angular/core';
import { Subject } from 'rxjs';
import { AiServerViewService } from '../../../services/ai-server-view.service';
import { takeUntil } from 'rxjs/operators';
import { AdminAiServerViewState } from '../../../states/admin-ai-server-view-state';
import { toSignal } from '@angular/core/rxjs-interop';
import { TranslateService } from '@ngx-translate/core';

const AI_SERVER_DOC_URL_EN =
  'https://tm-en.doc.squashtest.com/latest/admin-guide/manage-servers/managing-ai-server.html?utm_source=Appli_squash&utm_medium=link';
const AI_SERVER_DOC_URL_FR =
  'https://tm-fr.doc.squashtest.com/latest/admin-guide/gestion-serveurs/gerer-serveur-ia.html?utm_source=Appli_squash&utm_medium=link';

@Component({
  selector: 'sqtm-app-ai-server-content',
  templateUrl: './ai-server-content.component.html',
  styleUrl: './ai-server-content.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AiServerContentComponent {
  componentData$: Signal<AdminAiServerViewState>;
  unsub$ = new Subject<void>();

  constructor(
    public readonly aiServerViewService: AiServerViewService,
    public readonly translateService: TranslateService,
  ) {
    this.componentData$ = toSignal(aiServerViewService.componentData$.pipe(takeUntil(this.unsub$)));
  }

  get aiServerConfigurationDocUrl(): string {
    return this.translateService.getBrowserLang() === 'fr'
      ? AI_SERVER_DOC_URL_FR
      : AI_SERVER_DOC_URL_EN;
  }
}
