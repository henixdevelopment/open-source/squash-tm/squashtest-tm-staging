import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  HostListener,
  OnDestroy,
  OnInit,
  Renderer2,
  Signal,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import {
  AclGroup,
  AdminReferentialDataService,
  AuthenticatedUser,
  CollapsePanelStatus,
  getAclGroupI18nKey,
  isDefaultSystemAclGroup,
  Permissions,
  PermissionWithSubPermissions,
  PermissionWorkspaceType,
  permissionWorkspaceEntityReferences,
  PermissionsMatrixPanel,
  ProfilePermissions,
  ProfilesAndPermissions,
  systemProfilesOrder,
  TextResearchFieldComponent,
  workspacePermissionsList,
} from 'sqtm-core';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { ProfileService } from '../../services/profile.service';
import { toSignal } from '@angular/core/rxjs-interop';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';
import { AbstractPermissionsComponent } from '../../components/abstract-permissions.component';

@Component({
  selector: 'sqtm-app-permissions-matrix-grid',
  templateUrl: './permissions-matrix-grid.component.html',
  styleUrls: ['./permissions-matrix-grid.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class PermissionsMatrixGridComponent
  extends AbstractPermissionsComponent
  implements OnInit, OnDestroy
{
  unsub$ = new Subject<void>();
  readonly $authenticatedUser: Signal<AuthenticatedUser>;
  readonly $isUltimate: Signal<boolean>;
  $profilesAndPermissions: Signal<ProfilesAndPermissions[]>;

  @ViewChild(TextResearchFieldComponent)
  searchField: TextResearchFieldComponent;

  @ViewChild('emptyTableMessage')
  emptyTableMessage: ElementRef;

  panels: PermissionsMatrixPanel[] = [];
  private draggedColumn: string = null;
  private columnIdOver: number = -1;
  private previousX: number;

  constructor(
    private adminReferentialDataService: AdminReferentialDataService,
    private profileService: ProfileService,
    protected translateService: TranslateService,
    protected elementRef: ElementRef,
    protected renderer: Renderer2,
  ) {
    super(elementRef, translateService, renderer);
    this.$authenticatedUser = toSignal(this.adminReferentialDataService.authenticatedUser$);
    this.$isUltimate = toSignal(this.adminReferentialDataService.isUltimateLicenseAvailable$);
    this.initProfilesAndPermissionsData();
  }

  ngOnInit(): void {
    this.initPermissionsLabels();
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.getTableWidth();
  }

  private initPermissionsLabels(): void {
    for (const workspacePermissions of workspacePermissionsList) {
      this.addPanel(workspacePermissions.workspace, workspacePermissions.permissions);
    }
  }

  private addPanel(
    workspace: PermissionWorkspaceType,
    permissions: PermissionWithSubPermissions[],
  ) {
    const formattedWorkspaceCode = this.formatCode(workspace);
    const panel: PermissionsMatrixPanel = {
      id: formattedWorkspaceCode,
      active: true,
      permissions: this.buildPermissionTree(formattedWorkspaceCode, permissions),
    };
    this.panels.push(panel);
  }

  private buildPermissionTree(
    formattedWorkspaceCode: string,
    permissions: PermissionWithSubPermissions[] | Permissions[],
  ) {
    return permissions.map((permission) => {
      const permissionName = permission.mainPermission ?? permission;
      return {
        id: permissionName,
        active: true,
        explainMessage: this.getExplainMessage(
          formattedWorkspaceCode,
          this.formatCode(permissionName),
        ),
        permissions: permission.subPermissions
          ? this.buildPermissionTree(formattedWorkspaceCode, permission.subPermissions)
          : [],
      };
    });
  }

  private formatId(code: string): string {
    return code.toUpperCase().replace(/-/g, '_');
  }

  getHeaderClasses(isProfileActive: boolean): string {
    const basicClass = 'permissions-header';
    if (isProfileActive) {
      return basicClass;
    }
    return `${basicClass} header-inactive`;
  }

  private initProfilesAndPermissionsData(): void {
    this.$profilesAndPermissions = toSignal(
      this.profileService.getAllPermissions().pipe(
        map((profilesAndPermissions) => {
          return this.transformProfileName(profilesAndPermissions);
        }),
      ),
    );
  }

  private transformProfileName(
    profilesAndPermissions: ProfilesAndPermissions[],
  ): ProfilesAndPermissions[] {
    const isSystemProfile = (profile: ProfilesAndPermissions) =>
      systemProfilesOrder.includes(profile.profileName);

    const systemProfiles = profilesAndPermissions.filter(isSystemProfile);
    const customProfiles = profilesAndPermissions.filter((profile) => !isSystemProfile(profile));

    systemProfiles.sort(
      (a, b) =>
        systemProfilesOrder.indexOf(a.profileName) - systemProfilesOrder.indexOf(b.profileName),
    );
    customProfiles.sort((a, b) => a.profileName.localeCompare(b.profileName));

    return [...systemProfiles, ...customProfiles].map((profile) => ({
      ...profile,
      profileName: isDefaultSystemAclGroup(profile.profileName)
        ? this.translateService.instant(getAclGroupI18nKey(profile.profileName as AclGroup))
        : profile.profileName,
    }));
  }

  handleClickOnColumn(profileId: number): void {
    this.elementRef.nativeElement
      .querySelectorAll(`[data-profile-id='${profileId}']`)
      .forEach((column: HTMLElement) => {
        if (column.classList.contains('column-selected')) {
          column.classList.remove('column-selected');
        } else {
          column.classList.add('column-selected');
        }
      });
  }

  hasPermission(
    profileAndPermissions: ProfilesAndPermissions,
    panelName: string,
    permission: Permissions,
  ): boolean {
    return !!profileAndPermissions.permissions.some(
      (profilePermission: ProfilePermissions) =>
        profilePermission.simplifiedClassName.toUpperCase() ===
          _.camelCase(panelName).toUpperCase() &&
        profilePermission.permissions.includes(permission),
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  getPanelTitleClasses(panelId: string, index: number) {
    let classes = '__hover_pointer panel-title permissions-line-table-header workspace-panel';

    if (index !== 0) {
      classes += ' permissions-line-table-header-border-top';
    }

    if (panelId.toLowerCase() !== 'project') {
      classes += ` ${panelId}-title`;
    }

    return classes;
  }

  getPanelTitle(panelId: string) {
    return this.translateService.instant(
      `sqtm-core.administration-workspace.profiles.permission.${panelId}.title`,
    );
  }

  getPermissionLabel(panelId: string, permissionId: Permissions) {
    return this.translateService.instant(
      `sqtm-core.administration-workspace.profiles.permission.${this.formatCode(panelId)}.${this.formatCode(permissionId)}.label`,
    );
  }

  getTableWidth() {
    const profilesNb: number = this.$profilesAndPermissions().length;
    const columnWidth = 100;
    const firstColumnWidth = 265;

    let tableWidth: number = profilesNb * columnWidth + firstColumnWidth;
    const matrixContainerWidth =
      this.elementRef.nativeElement.querySelector('#permissions-matrix').offsetWidth;
    if (tableWidth < matrixContainerWidth) {
      tableWidth = matrixContainerWidth - 20;
    }
    return `${tableWidth}px`;
  }

  getWorkspaceIcon(panelId: string): string {
    const workspaceType: string = this.formatId(panelId);
    return `sqtm-core-nav:${permissionWorkspaceEntityReferences[workspaceType]}-workspace`;
  }

  getPermissionCode(panelId: string, permissionId: string): string {
    return `${panelId}-${permissionId.toLowerCase()}`;
  }

  protected openAllPanels(): void {
    this.panels.forEach((panel) => {
      this.collapsePanel(panel.id, 'OPEN');
    });
  }

  protected closeAllPanels(): void {
    this.panels.forEach((panel) => {
      this.collapsePanel(panel.id, 'CLOSE');
    });
  }

  collapsePanel(panelId: string, status: CollapsePanelStatus = 'INVERSE'): void {
    const panel = this.findPanelById(panelId);
    if (!panel) {
      return;
    }

    this.updatePanelPermissions(panel, status);

    const isOpen = this.isPanelOpen(panel);
    this.updateWorkspaceAnchorClasses(panelId, isOpen);
    this.updatePermissionsClasses(panelId, isOpen);
  }

  private findPanelById(panelId: string) {
    return this.panels.find((p) => p.id === panelId);
  }

  private updatePanelPermissions(panel: PermissionsMatrixPanel, status: CollapsePanelStatus): void {
    panel.permissions.forEach((item) => {
      item.active = status === 'INVERSE' ? !item.active : status !== 'CLOSE';
    });
  }

  private isPanelOpen(panel: PermissionsMatrixPanel): boolean {
    return panel.permissions.some((p) => p.active);
  }

  private updateWorkspaceAnchorClasses(panelId: string, isOpen: boolean): void {
    const workspaceAnchor = this.elementRef.nativeElement.querySelector(
      `#${panelId} .permissions-workspace-anchor`,
    );

    if (isOpen) {
      workspaceAnchor.classList.remove('anchor-close');
      workspaceAnchor.classList.add('anchor-open');
    } else {
      workspaceAnchor.classList.remove('anchor-open');
      workspaceAnchor.classList.add('anchor-close');
    }
  }

  private updatePermissionsClasses(panelId: string, isOpen: boolean): void {
    const permissionsForWorkspace = this.elementRef.nativeElement.querySelectorAll(
      `[data-workspace='${panelId}']`,
    );

    permissionsForWorkspace.forEach((permission: HTMLElement) => {
      if (isOpen) {
        permission.parentElement.classList.remove('permission-collapse-close');
      } else {
        permission.parentElement.classList.add('permission-collapse-close');
      }
    });
  }

  onDragStart(event: DragEvent, column: string, columnId: number) {
    const columnElements = this.elementRef.nativeElement.querySelectorAll('.column-selected');
    columnElements.forEach((element: HTMLElement) => element.classList.remove('column-selected'));

    const selectedColumnElements = this.elementRef.nativeElement.querySelectorAll(
      `[data-profile-id='${columnId}']`,
    );
    selectedColumnElements.forEach((element: HTMLElement) =>
      element.classList.add('column-selected'),
    );

    this.draggedColumn = column;
    event.dataTransfer?.setData('text/plain', column);
  }

  onDragOver(event: DragEvent, columnId: number) {
    event.preventDefault();
    const isColumnChange = this.columnIdOver !== columnId;

    if (isColumnChange) {
      this.removeDropOverClass(this.columnIdOver);
      this.addDropOverClass(columnId);
      this.columnIdOver = columnId;
      this.updatePermissionsMatrixScroll(event);
    }
  }

  private removeDropOverClass(columnId: number) {
    if (columnId !== -1) {
      const elements = this.elementRef.nativeElement.querySelectorAll(
        `[data-profile-id='${columnId}']`,
      );
      elements.forEach((element: HTMLElement) => element.classList.remove('column-drop-over'));
    }
  }

  private addDropOverClass(columnId: number) {
    const elements = this.elementRef.nativeElement.querySelectorAll(
      `[data-profile-id='${columnId}']`,
    );
    elements.forEach((element: HTMLElement) => element.classList.add('column-drop-over'));
  }

  private updatePermissionsMatrixScroll(event: DragEvent) {
    const permissionsMatrix = this.elementRef.nativeElement.querySelector('#permissions-matrix');
    const currentX = event.clientX;
    const deltaX = currentX - this.previousX;
    if (deltaX < 0) {
      permissionsMatrix.scrollLeft = Math.max(permissionsMatrix.scrollLeft - 50, 0);
    }
    this.previousX = currentX;
  }

  onDrop(event: DragEvent, targetColumn: string) {
    event.preventDefault();
    const draggedColumn = this.draggedColumn;
    if (!draggedColumn || draggedColumn === targetColumn) {
      this.removeSelectionAndDropOverClasses(draggedColumn, targetColumn);
      return;
    }

    const draggedObject = this.getProfileObject(draggedColumn);
    const targetObject = this.getProfileObject(targetColumn);
    const draggedIndex = this.getProfileIndex(draggedColumn);
    const targetIndex = this.getProfileIndex(targetColumn);

    this.swapProfileObjects(draggedIndex, targetIndex, draggedObject, targetObject);
    this.draggedColumn = null;
    this.removeSelectionAndDropOverClasses(draggedColumn, targetColumn);
  }

  private getProfileObject(profileName: string) {
    return this.$profilesAndPermissions().find((it) => it.profileName === profileName);
  }

  private getProfileIndex(profileName: string) {
    return this.$profilesAndPermissions().findIndex((it) => it.profileName === profileName);
  }

  private swapProfileObjects(
    draggedIndex: number,
    targetIndex: number,
    draggedObject: any,
    targetObject: any,
  ) {
    this.$profilesAndPermissions().splice(draggedIndex, 1, targetObject);
    this.$profilesAndPermissions().splice(targetIndex, 1, draggedObject);
  }

  private removeSelectionAndDropOverClasses(draggedColumn: string, targetColumn: string) {
    const draggedObject = this.getProfileObject(draggedColumn);
    const targetObject = this.getProfileObject(targetColumn);

    this.removeClasses(draggedObject.profileId.toString(), 'column-selected', 'column-drop-over');
    this.removeClasses(targetObject.profileId.toString(), 'column-selected', 'column-drop-over');
  }

  private removeClasses(profileId: string, ...classNames: string[]) {
    const elements = this.elementRef.nativeElement.querySelectorAll(
      `[data-profile-id='${profileId}']`,
    );
    elements.forEach((element: HTMLElement) => {
      classNames.forEach((className) => element.classList.remove(className));
    });
  }

  protected override getSubPermissionSearchLabel(subPermissionLabel: Element): Element {
    return subPermissionLabel.firstElementChild?.firstElementChild;
  }

  protected override getPermissionSearchLabel(permissionLabel: Element): Element {
    return permissionLabel.firstElementChild;
  }
}
