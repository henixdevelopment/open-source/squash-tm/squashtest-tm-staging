import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { SystemViewState } from '../../../states/system-view.state';
import { switchMap, take } from 'rxjs/operators';

import { Observable } from 'rxjs';
import { AdminReferentialDataService } from 'sqtm-core';

@Component({
  selector: 'sqtm-app-system-search-activation-panel',
  templateUrl: './system-search-activation-panel.component.html',
  styleUrls: [
    './system-search-activation-panel.component.less',
    '../../../styles/system-workspace.common.less',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SystemSearchActivationPanelComponent {
  @Input()
  componentData: SystemViewState;

  searchActivationFeatureEnabled$: Observable<boolean>;

  constructor(public adminReferentialDataService: AdminReferentialDataService) {
    this.searchActivationFeatureEnabled$ =
      adminReferentialDataService.searchActivationFeatureEnabled$;
  }

  handleSearchActivationFeatureToggle() {
    this.searchActivationFeatureEnabled$
      .pipe(
        take(1),
        switchMap((enabled) =>
          this.adminReferentialDataService.setSearchActivationFeatureEnabled(!enabled),
        ),
      )
      .subscribe();
  }
}
