import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import {
  ADMIN_WS_CUSTOM_FIELD_TABLE,
  ADMIN_WS_CUSTOM_FIELD_TABLE_CONFIG,
} from '../../../custom-workspace.constant';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import { adminCustomFieldTableDefinition } from '../custom-field-grid/custom-field-grid.component';
import { Observable } from 'rxjs';

@Component({
  selector: 'sqtm-app-custom-field-workspace',
  templateUrl: './custom-field-workspace.component.html',
  styleUrls: ['./custom-field-workspace.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: ADMIN_WS_CUSTOM_FIELD_TABLE_CONFIG,
      useFactory: adminCustomFieldTableDefinition,
      deps: [],
    },
    {
      provide: ADMIN_WS_CUSTOM_FIELD_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, ADMIN_WS_CUSTOM_FIELD_TABLE_CONFIG, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: ADMIN_WS_CUSTOM_FIELD_TABLE,
    },
  ],
})
export class CustomFieldWorkspaceComponent implements OnInit, OnDestroy {
  authenticatedAdmin$: Observable<AuthenticatedUser>;

  constructor(
    public readonly adminReferentialDataService: AdminReferentialDataService,
    private gridService: GridService,
  ) {}

  ngOnInit(): void {
    this.authenticatedAdmin$ = this.adminReferentialDataService.authenticatedUser$;
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }
}
