import { ColumnDefinitionBuilder, DataRow, GridColumnId, withLinkColumn } from 'sqtm-core';

export function clickableProjectNameColumn(id: GridColumnId): ColumnDefinitionBuilder {
  const urlFunction = (row: DataRow) => {
    return [
      '/',
      'administration-workspace',
      'projects',
      'detail',
      row.data.projectId.toString(),
      'content',
    ];
  };
  return withLinkColumn(id, {
    kind: 'link',
    createUrlFunction: urlFunction,
    saveGridStateBeforeNavigate: true,
  });
}

export function clickableTeamNameColumn(id: GridColumnId): ColumnDefinitionBuilder {
  const urlFunction = (row: DataRow) => {
    const partyId = row.data.partyId;
    return ['/', 'administration-workspace', 'users', 'detail', 'team', partyId.toString()];
  };
  return withLinkColumn(id, {
    kind: 'link',
    createUrlFunction: urlFunction,
    saveGridStateBeforeNavigate: true,
  });
}
// ['administration-workspace', 'users', 'detail', 'user', partyId]
export function clickableMemberColumn(id: GridColumnId): ColumnDefinitionBuilder {
  const urlFunction = (row: DataRow) => {
    const partyId = row.data.partyId;
    return ['/', 'administration-workspace', 'users', 'detail', 'user', partyId.toString()];
  };
  return withLinkColumn(id, {
    kind: 'link',
    createUrlFunction: urlFunction,
    saveGridStateBeforeNavigate: true,
  });
}

export function clickablePartyColumn(id: GridColumnId): ColumnDefinitionBuilder {
  const urlFunction = (row: DataRow) => {
    const partyId = row.data.partyId;
    const partyEntity = row.data.team ? 'team' : 'user';
    return ['/', 'administration-workspace', 'users', 'detail', partyEntity, partyId.toString()];
  };
  return withLinkColumn(id, {
    kind: 'link',
    createUrlFunction: urlFunction,
    saveGridStateBeforeNavigate: true,
  });
}

export function clickableEnvironmentVariableNameColumn(id: GridColumnId): ColumnDefinitionBuilder {
  const urlFunction = (row: DataRow) => {
    return [
      '/',
      'administration-workspace',
      'entities-customization',
      'environment-variables',
      'detail',
      row.data.id,
      'content',
    ];
  };
  return withLinkColumn(id, {
    kind: 'link',
    createUrlFunction: urlFunction,
    saveGridStateBeforeNavigate: false,
  });
}
