import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractDeleteCellRenderer,
  ConfirmDeleteLevel,
  DialogService,
  GridColumnId,
  GridService,
  RestService,
} from 'sqtm-core';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-delete-automated-suite',
  template: `
    <sqtm-core-toggle-icon
      [active]="canDelete"
      (click)="deleteAutomatedSuite()"
      class="full-height full-width icon-container m-t-2"
    >
      <i
        class="table-icon-size"
        [style.color]="canDelete ? 'current-workspace-main-color' : ''"
        nz-icon
        nzType="sqtm-core-generic:delete"
        nzTheme="outline"
        [nzTooltipTitle]="'sqtm-core.generic.label.delete' | translate"
        nz-tooltip
      ></i>
    </sqtm-core-toggle-icon>
  `,
  styleUrl: './delete-automated-suite.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteAutomatedSuiteComponent extends AbstractDeleteCellRenderer {
  constructor(
    public grid: GridService,
    cdr: ChangeDetectorRef,
    private restService: RestService,
    protected dialogService: DialogService,
  ) {
    super(grid, cdr, dialogService);
  }

  doDelete() {
    const suiteIds: string[] = [this.row.data[GridColumnId.suiteId]];
    this.grid.beginAsyncOperation();
    this.restService
      .post(['automated-suites', 'deletion'], suiteIds)
      .pipe(finalize(() => this.grid.completeAsyncOperation()))
      .subscribe(() => this.grid.refreshData());
  }

  deleteAutomatedSuite() {
    if (this.canDelete) {
      this.showDeleteConfirm();
    }
  }

  get canDelete(): boolean {
    return (
      this.row.simplePermissions &&
      this.row.simplePermissions.canDelete &&
      this.row.data[GridColumnId.executionStatus] !== 'RUNNING' &&
      this.row.data[GridColumnId.executionStatus] !== 'READY'
    );
  }

  protected getTitleKey(): string {
    return 'sqtm-core.campaign-workspace.automated-suite.delete.title';
  }

  protected getMessageKey(): string {
    return this.canDelete
      ? 'sqtm-core.campaign-workspace.automated-suite.delete.message'
      : 'sqtm-core.campaign-workspace.automated-suite.delete.running-message';
  }

  protected getLevel(): ConfirmDeleteLevel {
    return 'DANGER';
  }
}
