import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewContainerRef,
} from '@angular/core';
import { TestSuiteViewComponentData } from '../../../containers/test-suite-view/test-suite-view.component';
import {
  ActionErrorDisplayService,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
  ListPanelItem,
  ReferentialDataService,
  RestService,
  TableValueChange,
} from 'sqtm-core';
import { Overlay } from '@angular/cdk/overlay';
import { TranslateService } from '@ngx-translate/core';
import { TestSuiteViewService } from '../../../services/test-suite-view.service';
import { map, take, takeUntil } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AbstractExecutionStatusesCell } from '../../../../campaign-workspace/components/test-plan/abstract-execution-statuses-cell';

@Component({
  selector: 'sqtm-app-test-suite-execution-status-cell',
  templateUrl: './test-suite-execution-statuses-cell.component.html',
  styleUrls: ['./test-suite-execution-statuses-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestSuiteExecutionStatusesCellComponent extends AbstractExecutionStatusesCell {
  readonly canEdit$: Observable<boolean>;

  readonly panelItems$: Observable<ListPanelItem[]>;

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    public readonly overlay: Overlay,
    public readonly vcr: ViewContainerRef,
    public readonly translateService: TranslateService,
    public readonly restService: RestService,
    public readonly actionErrorDisplayService: ActionErrorDisplayService,
    public referentialDataService: ReferentialDataService,
    public testSuiteViewService: TestSuiteViewService,
  ) {
    super(
      grid,
      cdRef,
      overlay,
      vcr,
      translateService,
      restService,
      actionErrorDisplayService,
      referentialDataService,
    );

    this.canEdit$ = this.testSuiteViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      map((componentData) => this.canEdit(componentData)),
    );

    this.panelItems$ = this.testSuiteViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      map((componentData) => componentData.projectData),
      map((projectData) => this.getFilteredExecutionStatusKeys(projectData)),
      map((statuses: string[]) => this.asListItemOptions(statuses)),
    );
  }

  private canEdit(componentData: TestSuiteViewComponentData): boolean {
    return (
      componentData &&
      componentData.permissions.canWrite &&
      componentData.milestonesAllowModification
    );
  }

  change(executionStatusKey: string) {
    this.authenticatedUser$.pipe(take(1)).subscribe((authenticatedUser) => {
      this.testSuiteViewService
        .updateExecutionStatus(executionStatusKey, this.row.data[GridColumnId.itemTestPlanId])
        .subscribe();
      const changes: TableValueChange[] = [
        { columnId: this.columnDisplay.id, value: executionStatusKey },
        { columnId: GridColumnId.user, value: this.getUser(authenticatedUser) },
        { columnId: GridColumnId.lastExecutedOn, value: new Date() },
      ];
      this.grid.editRows([this.row.id], changes);
      this.close();
    });
  }

  isExploratoryExecution() {
    return this.row.data[GridColumnId.inferredExecutionMode] === 'EXPLORATORY';
  }
}

export function testSuiteExecutionStatusesColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(TestSuiteExecutionStatusesCellComponent)
    .withHeaderPosition('center');
}
