import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { SprintReqVersionViewService } from '../../service/sprint-req-version-view.service';
import { TranslateService } from '@ngx-translate/core';
import {
  ActionErrorDisplayService,
  AttachmentService,
  CustomFieldValueService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  GenericEntityViewService,
  ReferentialDataService,
  RestService,
  RichTextAttachmentDelegate,
} from 'sqtm-core';
import { Subject } from 'rxjs';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { takeUntil, tap } from 'rxjs/operators';
import { TEST_PLAN_OWNER_VIEW } from '../../../campaign-workspace.constant';
import { GENERIC_TEST_PLAN_ITEM_DELETER } from '../../../../../components/test-plan/generic-test-plan-item-deleter';

@Component({
  selector: 'sqtm-app-sprint-req-version-level-two',
  templateUrl: './sprint-req-version-level-two.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: SprintReqVersionViewService,
      useClass: SprintReqVersionViewService,
      deps: [
        RestService,
        ReferentialDataService,
        AttachmentService,
        TranslateService,
        CustomFieldValueService,
        EntityViewAttachmentHelperService,
        EntityViewCustomFieldHelperService,
        ActionErrorDisplayService,
      ],
    },
    {
      provide: EntityViewService,
      useExisting: SprintReqVersionViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: SprintReqVersionViewService,
    },
    {
      provide: RichTextAttachmentDelegate,
      useExisting: SprintReqVersionViewService,
    },
    {
      provide: TEST_PLAN_OWNER_VIEW,
      useExisting: SprintReqVersionViewService,
    },
    {
      provide: GENERIC_TEST_PLAN_ITEM_DELETER,
      useExisting: SprintReqVersionViewService,
    },
  ],
})
export class SprintReqVersionLevelTwoComponent implements OnInit, OnDestroy {
  unsub$ = new Subject<void>();

  constructor(
    private sprintReqVersionViewService: SprintReqVersionViewService,
    private route: ActivatedRoute,
    private referentialDataService: ReferentialDataService,
  ) {}

  ngOnInit(): void {
    this.referentialDataService.refresh().subscribe(() => this.loadData());
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        tap((params: ParamMap) => {
          const sprintReqVersionId = +params.get('sprintReqVersionId');
          this.sprintReqVersionViewService.load(sprintReqVersionId);
        }),
      )
      .subscribe();
  }

  back() {
    history.back();
  }

  hasBackPage() {
    return history.length > 1;
  }

  ngOnDestroy(): void {
    this.sprintReqVersionViewService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }
}
