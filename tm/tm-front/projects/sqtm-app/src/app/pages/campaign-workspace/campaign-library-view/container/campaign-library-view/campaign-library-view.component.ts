import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Subject } from 'rxjs';
import { CampaignLibraryViewService } from '../../service/campaign-library-view.service';
import {
  AttachmentDrawerComponent,
  CampaignPermissions,
  EntityViewComponentData,
  EntityViewService,
  GenericEntityViewService,
  ReferentialDataService,
} from 'sqtm-core';
import { filter, map, take, takeUntil } from 'rxjs/operators';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { CampaignLibraryState } from '../../state/campaign-library.state';

@Component({
  selector: 'sqtm-app-campaign-library-view',
  templateUrl: './campaign-library-view.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: CampaignLibraryViewService,
      useClass: CampaignLibraryViewService,
    },
    {
      provide: EntityViewService,
      useExisting: CampaignLibraryViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: CampaignLibraryViewService,
    },
  ],
})
export class CampaignLibraryViewComponent implements OnInit, OnDestroy {
  unsub$ = new Subject<void>();

  @ViewChild(AttachmentDrawerComponent)
  attachmentDrawer: AttachmentDrawerComponent;

  @ViewChild('content', { read: ElementRef })
  content: ElementRef;

  constructor(
    public readonly campaignLibraryService: CampaignLibraryViewService,
    private referentialDataService: ReferentialDataService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.referentialDataService.loaded$
      .pipe(
        takeUntil(this.unsub$),
        filter((loaded) => loaded),
        take(1),
      )
      .subscribe(() => this.loadData());
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('campaignLibraryId')),
      )
      .subscribe((id) => {
        this.campaignLibraryService.load(parseInt(id, 10));
      });
  }

  ngOnDestroy(): void {
    this.campaignLibraryService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  toggleAttachmentPanel() {
    this.attachmentDrawer.open();
  }
}

export interface CampaignLibraryViewComponentData
  extends EntityViewComponentData<CampaignLibraryState, 'campaignLibrary', CampaignPermissions> {}
