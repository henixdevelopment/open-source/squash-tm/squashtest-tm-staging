export interface CtpiMultiEditDialogConfiguration {
  id: string;
  titleKey: string;
  ctpiIds: number[];
}
