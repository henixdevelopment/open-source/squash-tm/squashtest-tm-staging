import {
  ChangeDetectionStrategy,
  Component,
  computed,
  ElementRef,
  Inject,
  Renderer2,
  Signal,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { Observable } from 'rxjs';
import { SprintViewComponentData } from '../sprint-view/sprint-view.component';
import { SprintViewService } from '../../service/sprint-view.service';
import {
  BindReqVersionToSprintOperationReport,
  CampaignPermissions,
  ConfirmDeleteLevel,
  createBindReqVersionToSprintOperationDialogConfiguration,
  DataRow,
  DialogService,
  GridDndData,
  GridService,
  I18nEnum,
  isDndDataFromRequirementTreePicker,
  parseDataRowId,
  REQUIREMENT_TREE_PICKER_ID,
  shouldShowBindReqVersionToSprintDialog,
  SprintStatus,
  SprintStatusKeys,
  SqtmDragEnterEvent,
  SqtmDragLeaveEvent,
  SqtmDropEvent,
} from 'sqtm-core';
import { logger } from '../../../../test-case-workspace/test-case-workspace/containers/test-case-workspace/test-case-workspace.component';
import { SPRINT_DROP_ZONE_ID } from '../../sprint-view.constant';
import { take, tap } from 'rxjs/operators';
import { SPRINT_REQ_VERSION_TABLE } from '../../../campaign-workspace.constant';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sqtm-app-sprint-view-sprint-req-versions',
  templateUrl: './sprint-view-sprint-req-version.component.html',
  styleUrls: ['./sprint-view-sprint-req-version.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SprintViewSprintReqVersionComponent {
  componentData$: Observable<SprintViewComponentData>;

  @ViewChild('content', { read: ElementRef })
  content: ElementRef;

  dropZoneId = SPRINT_DROP_ZONE_ID;
  protected readonly SprintStatus: I18nEnum<SprintStatusKeys> = SprintStatus;

  $componentData: Signal<SprintViewComponentData> = toSignal(this.sprintViewService.componentData$);
  $isSprintClosed: Signal<boolean> = computed(
    () => this.$componentData().sprint.status === SprintStatus.CLOSED.id,
  );
  $isSprintManual: Signal<boolean> = computed(
    () => this.$componentData().sprint.synchronisationKind == null,
  );

  constructor(
    private sprintViewService: SprintViewService,
    private renderer: Renderer2,
    private gridService: GridService,
    private dialogService: DialogService,
    @Inject(SPRINT_REQ_VERSION_TABLE) public sprintReqVerTable: GridService,
    private vcr: ViewContainerRef,
  ) {
    this.componentData$ = this.sprintViewService.componentData$;
  }

  toggleRequirementPickerDrawer(event) {
    this.stopPropagation(event);
    this.sprintViewService.toggleRequirementTreePicker();
  }

  dragEnter($event: SqtmDragEnterEvent) {
    if (isDndDataFromRequirementTreePicker($event)) {
      this.markAsDropZone();
    }
  }

  private markAsDropZone() {
    this.renderer.addClass(this.content.nativeElement, 'sqtm-core-border-current-workspace-color');
  }

  private unmarkAsDropZone() {
    this.renderer.removeClass(
      this.content.nativeElement,
      'sqtm-core-border-current-workspace-color',
    );
  }

  dragCancel() {
    this.unmarkAsDropZone();
  }

  dragLeave($event: SqtmDragLeaveEvent) {
    if (isDndDataFromRequirementTreePicker($event)) {
      this.unmarkAsDropZone();
    }
  }

  dropIntoSprint($event: SqtmDropEvent) {
    if ($event.dragAndDropData.origin === REQUIREMENT_TREE_PICKER_ID) {
      this.dropRequirementsIntoSprint($event.dragAndDropData.data);
      this.sprintViewService.updateLastModification();
    }
  }

  private dropRequirementsIntoSprint(data: GridDndData) {
    if (logger.isDebugEnabled()) {
      logger.debug(`Dropping requirements into sprint.`, [data]);
    }
    const requirementIds = data.dataRows.map((row) => parseDataRowId(row));

    this.sprintViewService.setDropZoneComponent(this.content);

    this.sprintViewService
      .addRequirementsIntoSprint(requirementIds)
      .pipe(take(1))
      .subscribe((operationReportData: BindReqVersionToSprintOperationReport) => {
        if (shouldShowBindReqVersionToSprintDialog(operationReportData)) {
          this.dialogService.openDialog(
            createBindReqVersionToSprintOperationDialogConfiguration(operationReportData, this.vcr),
          );
        }
        this.gridService.refreshData();
        this.unmarkAsDropZone();
      });
  }

  stopPropagation($event: MouseEvent) {
    $event.stopPropagation();
  }

  deleteSprintReqVersions() {
    this.sprintReqVerTable.selectedRows$
      .pipe(
        take(1),
        tap((selectedRows: DataRow<any>[]) => {
          const sprintReqWithExec: DataRow<any>[] = selectedRows.filter(
            (selectedRow: DataRow<any>) => selectedRow.data.nbExecutions > 0,
          );
          this.openDeleteConfirmSprintReqVersionsDialog(
            sprintReqWithExec.length,
            this.$componentData().permissions,
          );
        }),
      )
      .subscribe();
  }

  private openDeleteConfirmSprintReqVersionsDialog(
    nbSprintReqVerWithExec: number,
    userPermissions: CampaignPermissions,
  ) {
    let messageKey: string;
    if (nbSprintReqVerWithExec === 0) {
      messageKey = 'sqtm-core.campaign-workspace.dialog.message.remove-sprint-req-version-plural';
    } else if (userPermissions.canExtendedDelete) {
      messageKey =
        'sqtm-core.campaign-workspace.dialog.message.remove-selected-sprint-req-versions-with-executions-with-extended-delete';
    } else {
      messageKey =
        'sqtm-core.campaign-workspace.dialog.message.remove-selected-sprint-req-versions-with-executions-without-extended-delete';
    }

    const level: ConfirmDeleteLevel =
      nbSprintReqVerWithExec === 0 || !userPermissions.canExtendedDelete ? 'WARNING' : 'DANGER';
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.campaign-workspace.dialog.title.remove-sprint-req-version',
      messageKey: messageKey,
      level: level,
    });
    dialogReference.dialogClosed$.subscribe((confirm) => {
      if (confirm) {
        this.sprintViewService.deleteSprintReqVersions();
        this.sprintViewService.updateLastModification();
      }
    });
  }
}
