import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { SprintGroupState } from '../../state/sprint-group.state';
import { Observable, Subject } from 'rxjs';
import { ActivatedRoute, ParamMap } from '@angular/router';
import {
  ActionErrorDisplayService,
  AttachmentDrawerComponent,
  AttachmentService,
  CampaignPermissions,
  CustomFieldValueService,
  EntityPathHeaderService,
  EntityRowReference,
  EntityViewAttachmentHelperService,
  EntityViewComponentData,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  GenericEntityViewService,
  Identifier,
  ReferentialDataService,
  RestService,
  RichTextAttachmentDelegate,
  SquashTmDataRowType,
  WorkspaceWithTreeComponent,
} from 'sqtm-core';
import { SprintGroupViewService } from '../../service/sprint-group-view.service';
import { filter, map, take, takeUntil, withLatestFrom } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-sprint-group-view',
  templateUrl: './sprint-group-view.component.html',
  styleUrls: ['./sprint-group-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: SprintGroupViewService,
      useClass: SprintGroupViewService,
      deps: [
        RestService,
        ReferentialDataService,
        AttachmentService,
        TranslateService,
        CustomFieldValueService,
        EntityViewAttachmentHelperService,
        EntityViewCustomFieldHelperService,
        ActionErrorDisplayService,
      ],
    },
    {
      provide: EntityViewService,
      useExisting: SprintGroupViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: SprintGroupViewService,
    },
    {
      provide: RichTextAttachmentDelegate,
      useExisting: SprintGroupViewService,
    },
  ],
})
export class SprintGroupViewComponent implements OnInit, OnDestroy {
  unsub$ = new Subject<void>();
  componentData$: Observable<SprintGroupViewComponentData>;

  @ViewChild(AttachmentDrawerComponent)
  attachmentDrawer: AttachmentDrawerComponent;

  constructor(
    public readonly sprintGroupService: SprintGroupViewService,
    private referentialDataService: ReferentialDataService,
    private route: ActivatedRoute,
    private workspaceWithTree: WorkspaceWithTreeComponent,
    private entityPathHeaderService: EntityPathHeaderService,
  ) {
    this.componentData$ = this.sprintGroupService.componentData$;
  }

  ngOnInit() {
    this.referentialDataService.loaded$
      .pipe(
        takeUntil(this.unsub$),
        filter((loaded) => loaded),
        take(1),
      )
      .subscribe(() => {
        this.loadData();
        this.initializeTreeSynchronization();
        this.initializeRefreshPathObserver();
      });
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('sprintGroupId')),
      )
      .subscribe((id) => {
        this.sprintGroupService.load(parseInt(id, 10));
      });
  }

  private initializeRefreshPathObserver() {
    this.entityPathHeaderService.refreshPath$
      .pipe(takeUntil(this.unsub$))
      .subscribe(() => this.sprintGroupService.updateEntityPath());
  }

  private initializeTreeSynchronization() {
    this.sprintGroupService.simpleAttributeRequiringRefresh = ['name'];
    this.sprintGroupService.externalRefreshRequired$
      .pipe(
        takeUntil(this.unsub$),
        withLatestFrom(this.sprintGroupService.componentData$),
        map(([, componentData]: [any, SprintGroupViewComponentData]) =>
          new EntityRowReference(
            componentData.sprintGroup.id,
            SquashTmDataRowType.SprintGroup,
          ).asString(),
        ),
      )
      .subscribe((identifier: Identifier) => {
        this.workspaceWithTree.requireNodeRefresh([identifier]);
      });
  }

  toggleAttachmentPanel() {
    this.attachmentDrawer.open();
  }

  ngOnDestroy(): void {
    this.sprintGroupService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }
}

export interface SprintGroupViewComponentData
  extends EntityViewComponentData<SprintGroupState, 'sprintGroup', CampaignPermissions> {}
