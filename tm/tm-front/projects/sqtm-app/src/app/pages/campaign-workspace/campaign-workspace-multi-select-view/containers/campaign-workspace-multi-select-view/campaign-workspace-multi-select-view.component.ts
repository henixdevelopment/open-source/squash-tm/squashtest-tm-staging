import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { CampaignMultiSelectionService } from '../../services/campaign-multi-selection.service';
import { PartyPreferencesService, RestService } from 'sqtm-core';

@Component({
  selector: 'sqtm-app-campaign-workspace-multi-select-view',
  templateUrl: './campaign-workspace-multi-select-view.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: CampaignMultiSelectionService,
      useClass: CampaignMultiSelectionService,
      deps: [RestService, PartyPreferencesService],
    },
  ],
})
export class CampaignWorkspaceMultiSelectViewComponent implements OnDestroy {
  constructor(public readonly viewService: CampaignMultiSelectionService) {}

  ngOnDestroy(): void {
    this.viewService.complete();
  }
}
