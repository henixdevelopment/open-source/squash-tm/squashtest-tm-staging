import { CampaignMilestoneViewService } from './campaign-milestone-view.service';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppTestingUtilsModule } from '../../../../utils/testing-utils/app-testing-utils.module';

describe('CampaignMilestoneViewService', () => {
  let service: CampaignMilestoneViewService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule],
      providers: [CampaignMilestoneViewService],
    });
    service = TestBed.inject(CampaignMilestoneViewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
