import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable } from 'rxjs';
import { SprintViewComponentData } from '../sprint-view/sprint-view.component';
import { SprintViewService } from '../../service/sprint-view.service';

@Component({
  selector: 'sqtm-app-sprint-view-issues',
  templateUrl: './sprint-view-issues.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SprintViewIssuesComponent {
  readonly componentData$: Observable<SprintViewComponentData> =
    this.sprintViewService.componentData$;

  constructor(private sprintViewService: SprintViewService) {}
}
