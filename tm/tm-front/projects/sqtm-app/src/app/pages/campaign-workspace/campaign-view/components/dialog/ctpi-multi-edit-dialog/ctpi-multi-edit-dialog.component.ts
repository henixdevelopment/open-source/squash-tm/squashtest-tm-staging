import { AfterViewInit, ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { AbstractMassEditDialog } from '../../../../../search/test-case-search-page/abstract-mass-edit-dialog';
import { TranslateService } from '@ngx-translate/core';
import {
  DialogReference,
  DisplayOption,
  formatFullUserName,
  Identifier,
  OptionalSelectFieldComponent,
  RestService,
  SimpleUser,
} from 'sqtm-core';
import { CampaignViewService } from '../../../service/campaign-view.service';
import { CtpiMultiEditDialogConfiguration } from './ctpi-multi-edit-dialog.configuration';
import { take } from 'rxjs/operators';
import { CampaignState } from '../../../state/campaign.state';

@Component({
  selector: 'sqtm-app-ctpi-multi-edit-dialog',
  templateUrl: './ctpi-multi-edit-dialog.component.html',
  styleUrls: ['./ctpi-multi-edit-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CtpiMultiEditDialogComponent extends AbstractMassEditDialog implements AfterViewInit {
  data: CtpiMultiEditDialogConfiguration;

  userOptions: DisplayOption[];

  @ViewChild('assigneeField')
  assigneeField: OptionalSelectFieldComponent;

  constructor(
    translateService: TranslateService,
    public dialogReference: DialogReference<CtpiMultiEditDialogConfiguration>,
    restService: RestService,
    public campaignViewService: CampaignViewService,
  ) {
    super(translateService, restService);
    this.data = this.dialogReference.data;
  }

  ngAfterViewInit(): void {
    this.campaignViewService.componentData$.pipe(take(1)).subscribe(({ campaign }) => {
      this.buildAssignableUserOptions(campaign);
    });
  }

  private buildAssignableUserOptions(campaign: CampaignState) {
    const options = campaign.users.map((user) => ({
      id: user.id,
      label: this.getFullUsername(user),
    }));

    options.sort((a, b) => a.label.localeCompare(b.label));

    this.userOptions = [
      {
        id: UNASSIGNED_ID,
        label: this.translateService.instant(UNASSIGNED_KEY),
      },
      ...options,
    ];

    this.assigneeField.selectedValue = this.userOptions[0]?.id;
  }

  private getFullUsername(user: SimpleUser): string {
    return formatFullUserName(user);
  }

  confirm() {
    if (!this.assigneeField.check) {
      this.dialogReference.result = false;
      this.dialogReference.close();
      return;
    }

    const payload: CtpiMassEditPatch = {};

    if (this.assigneeField.check) {
      payload.changeAssignee = true;
      const selectedAssignee = this.assigneeField.selectedValue;
      payload.assignee = selectedAssignee === UNASSIGNED_ID ? null : selectedAssignee;
    }

    this.restService
      .post(['campaign/test-plan', this.data.ctpiIds.join(','), 'mass-update'], payload)
      .subscribe(() => {
        this.dialogReference.result = true;
        this.dialogReference.close();
      });
  }
}

const UNASSIGNED_ID = 'UNASSIGNED';

const UNASSIGNED_KEY = 'sqtm-core.campaign-workspace.test-plan.label.user.unassigned';

export interface CtpiMassEditPatch {
  changeAssignee?: boolean;
  assignee?: Identifier;
}
