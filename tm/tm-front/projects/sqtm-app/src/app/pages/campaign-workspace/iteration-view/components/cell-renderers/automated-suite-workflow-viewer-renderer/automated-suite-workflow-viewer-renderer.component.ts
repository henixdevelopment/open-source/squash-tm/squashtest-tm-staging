import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  DialogService,
  GridColumnId,
  GridService,
  RestService,
} from 'sqtm-core';
import { WorkflowLogsViewerComponent } from '../workflow-logs-viewer/workflow-logs-viewer.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-automated-suite-workflow-viewer-renderer',
  templateUrl: './automated-suite-workflow-viewer-renderer.component.html',
  styleUrl: './automated-suite-workflow-viewer-renderer.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AutomatedSuiteWorkflowViewerRendererComponent extends AbstractCellRendererComponent {
  OPEN_TEST_FACTORY_GET_WORKFLOW_URL =
    'https://opentestfactory.org/tools/opentf-ctl/basic.html?utm_source=Appli_squash&utm_medium=link#get-workflow-workflow_id';

  isDialogOpened = false;

  constructor(
    gridService: GridService,
    cdRef: ChangeDetectorRef,
    private dialogService: DialogService,
    private restService: RestService,
    private translateService: TranslateService,
  ) {
    super(gridService, cdRef);
  }

  openWorkflowLogsModal(workflow: AutomatedSuiteWorkflowState): void {
    if (!this.isActive() || this.isDialogOpened) {
      return;
    }
    this.isDialogOpened = true;
    this.restService
      .get<WorkflowLogsState>([
        'workflows',
        workflow.workflowId,
        workflow.projectId.toString(),
        'logs',
      ])
      .subscribe((response: WorkflowLogsState) => {
        if (!response.isStoredInDatabase) {
          this.openWorkflowAlreadyDoneDialog();
        } else if (response.reachable) {
          this.openLogsDialog(workflow, response.logs);
        } else {
          this.openErrorDialog();
        }
      });
  }

  isActive(): boolean {
    return (
      this.row.data['workflows'] != null &&
      (this.row.data[GridColumnId.executionStatus] === 'RUNNING' ||
        this.row.data[GridColumnId.executionStatus] === 'READY')
    );
  }

  getWorkflowUuidLabel(workflowUuid: string) {
    return this.translateService.instant(
      'sqtm-core.campaign-workspace.automated-suite.workflow-viewer.title',
      { workflowUuid: workflowUuid },
    );
  }

  private openLogsDialog(workflow: AutomatedSuiteWorkflowState, logs: string): void {
    const dialogConfig = {
      component: WorkflowLogsViewerComponent,
      data: {
        titleKey: 'sqtm-core.campaign-workspace.automated-suite.workflow-viewer.dialog-title',
        workflow: workflow,
        logs: logs,
      },
      id: 'workflow-logs-dialog',
      width: 1200,
      height: 650,
    };
    const dialogRef = this.dialogService.openDialog(dialogConfig);
    this.handleDialogClose(dialogRef);
  }

  private openErrorDialog(): void {
    const dialogRef = this.dialogService.openAlert(
      {
        titleKey: 'sqtm-core.campaign-workspace.automated-suite.workflow-viewer.dialog-title',
        messageKey:
          'sqtm-core.campaign-workspace.automated-suite.workflow-viewer.unreachable-orchestrator',
        id: 'workflow-logs-error',
        level: 'DANGER',
      },
      650,
    );
    this.handleDialogClose(dialogRef);
  }

  private openWorkflowAlreadyDoneDialog() {
    const dialogRef = this.dialogService.openAlert(
      {
        titleKey: 'sqtm-core.campaign-workspace.automated-suite.workflow-viewer.dialog-title',
        messageKey: 'sqtm-core.campaign-workspace.automated-suite.workflow-viewer.done-workflow',
        id: 'workflow-logs-already-done',
        level: 'INFO',
      },
      500,
    );

    this.handleDialogClose(dialogRef);
  }

  private handleDialogClose(dialogRef: any): void {
    dialogRef.dialogClosed$.subscribe(() => {
      this.grid.refreshData();
      this.isDialogOpened = false;
    });
  }
}

export function automatedSuiteRealTimeWorkflowWiewer(columnId: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(columnId)
    .withRenderer(AutomatedSuiteWorkflowViewerRendererComponent)
    .withHeaderPosition('center');
}

export interface WorkflowLogsState {
  logs: string;
  reachable: boolean;
  isStoredInDatabase: boolean;
}

export interface AutomatedSuiteWorkflowState {
  workflowId: string;
  projectId: number;
}
