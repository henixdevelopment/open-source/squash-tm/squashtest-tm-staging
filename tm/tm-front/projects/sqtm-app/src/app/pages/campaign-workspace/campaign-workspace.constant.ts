import { InjectionToken } from '@angular/core';
import { GridDefinition, GridService } from 'sqtm-core';
import { TestPlanOwnerView } from './test-plan-owner-view';

export const CAMPAIGN_WS_TREE_CONFIG = new InjectionToken<GridDefinition>(
  'Grid config instance for the main workspace tree',
);
export const CAMPAIGN_WS_TREE = new InjectionToken<GridService>(
  'Grid service instance for the main workspace tree',
);

export const CAMPAIGN_ISSUE_TABLE_CONF = new InjectionToken<GridDefinition>(
  'Grid config for the issue table of campaign view',
);
export const CAMPAIGN_ISSUE_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the issue table of campaign view',
);
export const SPRINT_ISSUE_TABLE_CONFIG = new InjectionToken(
  'Grid config for the issues table of sprint view',
);
export const SPRINT_ISSUE_TABLE = new InjectionToken(
  'Grid service instance for the issues table of sprint view',
);
export const SPRINT_REQ_VERSION_TABLE_CONF = new InjectionToken<GridDefinition>(
  'Grid config for the requirements table of sprint view',
);
export const SPRINT_REQ_VERSION_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the requirements table of sprint view',
);

export const SPRINT_REQ_VERSION_ISSUE_TABLE_CONFIG = new InjectionToken(
  'Token for sprint requirement version issue execution config',
);

export const SPRINT_REQ_VERSION_ISSUE_TABLE = new InjectionToken(
  'Token for sprint requirement version issue execution',
);

export const TEST_PLAN_OWNER_VIEW = new InjectionToken<TestPlanOwnerView>(
  'Injection token for the test plan owner view service',
);
