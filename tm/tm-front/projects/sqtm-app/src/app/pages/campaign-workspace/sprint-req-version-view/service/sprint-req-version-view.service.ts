import {
  AssignedUserDelegate,
  AttachmentService,
  AvailableTestPlanItemModel,
  CampaignPermissions,
  CustomFieldValueService,
  DateFormatUtils,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  ExecutionStatusKeys,
  GridColumnId,
  Identifier,
  InfoList,
  InfoListItem,
  ProjectData,
  ReferentialDataService,
  RestService,
  SimpleUser,
  SprintReqVersionValidationStatusKeys,
  SprintReqVersionViewModel,
  SprintStatus,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import {
  provideInitialSprintReqVersionView,
  SprintReqVersionViewState,
} from '../../../requirement-workspace/requirement-version-view/state/sprint-req-version-view.state';
import { SprintReqVersionState } from '../../../requirement-workspace/requirement-version-view/state/sprint-req-version.state';
import { catchError, map, tap, withLatestFrom } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { TestPlanOwnerView } from '../../test-plan-owner-view';
import { GenericTestPlanItemDeleter } from '../../../../components/test-plan/generic-test-plan-item-deleter';
import { GenericTestPlanItemMover } from '../../../../components/test-plan/generic-test-plan-item-mover';
import { GenericTestPlanExecutionManager } from '../../../../components/test-plan/generic-test-plan-execution-manager';
import { SprintReqVersionViewComponentData } from '../containers/sprint-req-version-view-sub-page/sprint-req-version-view-sub-page.component';

@Injectable()
export class SprintReqVersionViewService
  extends EntityViewService<SprintReqVersionState, 'sprintReqVersion', CampaignPermissions>
  implements
    TestPlanOwnerView,
    AssignedUserDelegate,
    GenericTestPlanItemDeleter,
    GenericTestPlanItemMover,
    GenericTestPlanExecutionManager
{
  readonly canUpdateExecutionStatus$ = this.componentData$.pipe(
    map(
      (componentData) =>
        componentData.permissions.canWrite &&
        componentData.sprintReqVersion.sprintStatus !== SprintStatus.CLOSED.id,
    ),
  );

  readonly canMoveTestPlanItems$: Observable<boolean> = this.componentData$.pipe(
    map(
      (componentData) =>
        componentData.permissions.canWrite &&
        componentData.sprintReqVersion.sprintStatus !== SprintStatus.CLOSED.id,
    ),
  );

  readonly canAssign$ = this.componentData$.pipe(
    map(
      (componentData) =>
        componentData.permissions.canWrite &&
        componentData.sprintReqVersion.sprintStatus !== SprintStatus.CLOSED.id,
    ),
  );

  readonly assignableUsers$: Observable<SimpleUser[]> = this.componentData$.pipe(
    map((componentData) => componentData.sprintReqVersion.assignableUsers),
  );

  assignableEntityIdColumnId: GridColumnId = GridColumnId.testPlanItemId;

  readonly canExecute$: Observable<boolean> = this.componentData$.pipe(
    map(
      (componentData) =>
        componentData.permissions.canExecute &&
        componentData.sprintReqVersion.sprintStatus !== SprintStatus.CLOSED.id,
    ),
  );

  readonly canDeleteExecutions$: Observable<boolean> = this.componentData$.pipe(
    map(
      (componentData) =>
        componentData.permissions.canDeleteExecution &&
        componentData.sprintReqVersion.sprintStatus !== SprintStatus.CLOSED.id,
    ),
  );

  constructor(
    protected restService: RestService,
    protected referentialDataService: ReferentialDataService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected customFieldValueService: CustomFieldValueService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
    protected customFieldHelper: EntityViewCustomFieldHelperService,
  ) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper,
    );
  }

  updateAssignedUser(testPlanItemId: number, userId: number) {
    return this.restService.post(
      ['test-plan-item', testPlanItemId.toString(), 'assign-user-to-sprint-req-version'],
      {
        assignee: userId,
      },
    );
  }

  addSimplePermissions(projectData: ProjectData): CampaignPermissions {
    return new CampaignPermissions(projectData);
  }

  getInitialState(): SprintReqVersionViewState {
    return provideInitialSprintReqVersionView();
  }

  load(sprintReqVersionId: number) {
    const url = ['sprint-req-version-view', sprintReqVersionId.toString()];
    this.restService
      .getWithoutErrorHandling<SprintReqVersionViewModel>(url)
      .pipe(
        catchError((err) => {
          this.notifyEntityNotFound(err);
          return throwError(() => err);
        }),
        withLatestFrom(this.referentialDataService.infoLists$),
        tap(([sprintReqVersionModel, infolists]: [SprintReqVersionViewModel, InfoList[]]) => {
          const reqCategoryInfoListItems = this.retrieveReqCategoryInfoListItems(infolists);
          const entityState: SprintReqVersionState = this.initializeSprintReqVersionState(
            sprintReqVersionModel,
            reqCategoryInfoListItems,
          );
          this.initializeEntityState(entityState);
        }),
      )
      .subscribe();
  }

  initializeSprintReqVersionState(
    model: SprintReqVersionViewModel,
    requirementCategory: InfoListItem[],
  ): SprintReqVersionState {
    return {
      ...model,
      attachmentList: null,
      customFieldValues: null,
      categoryInfoListItem: requirementCategory,
      createdOn: DateFormatUtils.createDateFromIsoString(model.createdOn),
      lastModifiedOn: DateFormatUtils.createDateFromIsoString(model.lastModifiedOn),
    };
  }

  retrieveReqCategoryInfoListItems(infoLists: InfoList[]): InfoListItem[] {
    return infoLists.flatMap((infoList) => infoList.items);
  }

  addTestCasesIntoTestPlan(testCaseIds: number[]): Observable<unknown> {
    const state = this.store.getSnapshot();
    const url = `sprint-req-version/${state.sprintReqVersion.id}/test-plan-items`;
    return this.restService
      .post<unknown>([url], { testCaseIds })
      .pipe(tap(() => this.updateLastModification()));
  }

  updateExecutionStatus(
    executionStatus: ExecutionStatusKeys,
    testPlanItemId: number,
  ): Observable<unknown> {
    return this.restService.post(['test-plan-item', testPlanItemId.toString(), 'status'], {
      executionStatus,
    });
  }

  deleteTestPlanItems(itemIds: number[]): Observable<unknown> {
    return this.restService
      .delete([
        'sprint-req-version',
        this.getSnapshot().sprintReqVersion.id.toString(),
        'test-plan-items',
        itemIds.join(','),
      ])
      .pipe(tap(() => this.updateLastModification()));
  }

  allowTestPlanItemDeletion(): boolean {
    return this.getSnapshot().sprintReqVersion.sprintStatus !== SprintStatus.CLOSED.id;
  }

  changeTestPlanItemsPosition(itemsToMove: Identifier[], position: number): Observable<unknown> {
    const sprintReqVersionId = this.getSnapshot().sprintReqVersion.id;
    return this.restService.post([
      'sprint-req-version',
      sprintReqVersionId.toString(),
      'test-plan-items',
      itemsToMove.join(','),
      'position',
      position.toString(),
    ]);
  }

  fetchAvailableTestPlanItems(): Observable<AvailableTestPlanItemModel[]> {
    return this.restService.get<AvailableTestPlanItemModel[]>([
      'sprint-req-version-view',
      this.getSnapshot().sprintReqVersion.id.toString(),
      'available-test-plan-items',
    ]);
  }

  updateValidationStatus(
    newValidationStatus: SprintReqVersionValidationStatusKeys,
  ): Observable<SprintReqVersionViewComponentData> {
    return this.restService
      .post<void>(
        [
          'sprint-req-version',
          this.getSnapshot().sprintReqVersion.id.toString(),
          'validation-status',
        ],
        newValidationStatus,
      )
      .pipe(
        withLatestFrom(this.store.state$),
        map(([_, state]: [void, SprintReqVersionViewComponentData]) => {
          return {
            ...state,
            sprintReqVersion: {
              ...state.sprintReqVersion,
              validationStatus: newValidationStatus,
            },
          };
        }),
        tap((state) => this.store.commit(state)),
      );
  }
}
