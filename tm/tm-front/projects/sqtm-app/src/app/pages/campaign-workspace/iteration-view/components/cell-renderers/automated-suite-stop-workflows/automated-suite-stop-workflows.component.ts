import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  Signal,
} from '@angular/core';
import {
  AbstractCellRendererComponent,
  AuthenticatedUser,
  ColumnDefinitionBuilder,
  DialogReference,
  DialogService,
  EntityViewService,
  GridService,
  RestService,
  SqtmEntityState,
  SimplePermissions,
  ReferentialDataService,
  GridColumnId,
} from 'sqtm-core';
import { filter, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { toSignal } from '@angular/core/rxjs-interop';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-automated-suite-stop-workflows',
  template: `
    @if (row) {
      <sqtm-core-toggle-icon
        [active]="isActive()"
        (click)="stopWorkflows()"
        class="full-height full-width icon-container m-t-2"
      >
        <i
          class="table-icon-size"
          [attr.data-test-button-id]="'stop-workflow'"
          [style.color]="isActive() ? 'current-workspace-main-color' : ''"
          nz-icon
          nzType="close-circle"
          nzTheme="outline"
          [nzTooltipTitle]="contentTemplate"
          [nzTooltipPlacement]="'top'"
          nz-tooltip
        >
        </i>
      </sqtm-core-toggle-icon>
    }

    <ng-template #contentTemplate>
      <div>
        {{ getTooltipMessage() }}
        @if (automatedSuiteHasNoWorkflows()) {
          <a href="{{ OPEN_TEST_FACTORY_KILL_WORKFLOW_URL }}" target="_blank" class="link-color"
            >opentf-ctl</a
          >.
        }
      </div>
    </ng-template>
  `,
  styleUrl: './automated-suite-stop-workflows.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AutomatedSuiteStopWorkflowsComponent<
    S extends SqtmEntityState,
    T extends string,
    P extends SimplePermissions,
  >
  extends AbstractCellRendererComponent
  implements OnDestroy
{
  get workflows(): string[] {
    if (this.row.data['workflows'] != null) {
      return this.row.data['workflows'].map((workflow) => workflow['workflowId']);
    }
    return [];
  }

  OPEN_TEST_FACTORY_KILL_WORKFLOW_URL =
    'https://opentestfactory.org/tools/opentf-ctl/basic.html?utm_source=Appli_squash&utm_medium=link#kill-workflow-workflow_id';

  $canExecute: Signal<boolean>;
  $currentUser: Signal<AuthenticatedUser>;
  unsub$ = new Subject<void>();

  constructor(
    gridService: GridService,
    cdRef: ChangeDetectorRef,
    public readonly viewService: EntityViewService<S, T, P>,
    private dialogService: DialogService,
    private restService: RestService,
    private referentialDataService: ReferentialDataService,
    private translateService: TranslateService,
  ) {
    super(gridService, cdRef);

    this.$canExecute = toSignal(
      this.viewService.componentData$.pipe(
        map((componentData: any) => componentData.permissions.canExecute),
      ),
    );

    this.$currentUser = toSignal(
      this.referentialDataService.authenticatedUser$.pipe(
        takeUntil(this.unsub$),
        map((user) => user),
      ),
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  getTooltipMessage(): string {
    const key = this.automatedSuiteHasNoWorkflows()
      ? 'sqtm-core.campaign-workspace.automated-suite.stop-workflows.devops-disabled-button-message'
      : 'sqtm-core.generic.label.stop';
    return this.translateService.instant(key);
  }

  automatedSuiteHasNoWorkflows(): boolean {
    return this.workflows != null && this.workflows.length == 0;
  }

  isActive(): boolean {
    return (
      this.canStopWorkflow() &&
      this.workflows != null &&
      this.workflows.length > 0 &&
      (this.row.data[GridColumnId.executionStatus] === 'RUNNING' ||
        this.row.data[GridColumnId.executionStatus] === 'READY') &&
      this.$canExecute()
    );
  }

  canStopWorkflow(): boolean {
    return (
      (this.row.simplePermissions && this.row.simplePermissions.canDelete) ||
      this.$currentUser().username == this.row.data.createdBy
    );
  }

  stopWorkflows() {
    if (!this.isActive()) {
      return;
    }
    const dialogRef = this.getDialogRef();

    dialogRef.dialogClosed$
      .pipe(
        filter((confirm) => Boolean(confirm)),
        tap(() => this.grid.beginAsyncOperation()),
        switchMap(() => this.stopWorkflowsServerSide()),
      )
      .subscribe((error: boolean) => {
        this.grid.completeAsyncOperation();
        if (error) {
          this.openErrorDialog();
        } else {
          this.grid.refreshData();
        }
      });
  }

  private stopWorkflowsServerSide(): Observable<boolean> {
    return this.restService.post<boolean>(
      ['automated-suite', this.row.data.suiteId, 'stop-workflows'],
      this.workflows,
    );
  }

  private openErrorDialog() {
    this.dialogService.openAlert({
      id: 'error-stop-orchestrator-workflow',
      titleKey: 'sqtm-core.generic.label.error',
      messageKey: 'sqtm-core.orchestrator.error.stop-workflow',
      level: 'DANGER',
    });
  }

  private getDialogRef(): DialogReference {
    const titleKey: string =
      this.workflows.length > 1
        ? 'sqtm-core.orchestrator.dialog.title.stop-workflows'
        : 'sqtm-core.orchestrator.dialog.title.stop-workflow';
    const messageKey: string =
      this.workflows.length > 1
        ? 'sqtm-core.orchestrator.dialog.message.stop-workflows'
        : 'sqtm-core.orchestrator.dialog.message.stop-workflow';
    return this.dialogService.openConfirm({
      id: 'confirm-stop-orchestrator-workflow',
      titleKey,
      messageKey,
      level: 'DANGER',
    });
  }
}

export function automatedSuiteStopWorkflowsColumn(columnId: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(columnId)
    .withRenderer(AutomatedSuiteStopWorkflowsComponent)
    .withHeaderPosition('center');
}
