import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  signal,
  Signal,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { IterationComponentData } from '../iteration-test-plan-execution/iteration-test-plan-execution.component';
import { IterationViewService } from '../../services/iteration-view.service';
import {
  buildFilters,
  CustomColumnsComponent,
  dateRangeFilter,
  DialogService,
  executionStatusFilter,
  GRID_PERSISTENCE_KEY,
  GridColumnId,
  GridFilter,
  GridFilterUtils,
  GridId,
  GridService,
  gridServiceFactory,
  GridWithStatePersistence,
  LocalPersistenceService,
  ReferentialDataService,
  ResearchColumnPrototype,
  RestService,
  serverBackedGridTextFilter,
  SuiteIdCreatedOn,
  userHistoryResearchFilter,
  UserHistorySearchProvider,
} from 'sqtm-core';
import { DatePipe } from '@angular/common';
import {
  ITERATION_AUTOMATED_SUITE_TABLE,
  ITERATION_AUTOMATED_SUITE_TABLE_CONF,
} from '../../iteration-view.constant';
import {
  filter,
  finalize,
  map,
  switchMap,
  take,
  takeUntil,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import { IterationViewComponentData } from '../../container/iteration-view/iteration-view.component';
import { TestSuiteAutomatedSuiteProvider } from '../../../test-suite-view/components/test-suite-automated-suite/test-suite-automated-suite-provider';
import { AutomatedSuiteExecComparatorDialogComponent } from '../automated-suite-exec-comparator-dialog/automated-suite-exec-comparator-dialog.component';
import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { automatedSuiteTableDefinition } from '../../../automated-suite-grid-conf';
import { takeUntilDestroyed, toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sqtm-app-iteration-automated-suite',
  templateUrl: './iteration-automated-suite.component.html',
  styleUrls: ['./iteration-automated-suite.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    DatePipe,
    {
      provide: ITERATION_AUTOMATED_SUITE_TABLE_CONF,
      useFactory: (localPersistenceService) =>
        automatedSuiteTableDefinition(GridId.ITERATION_AUTOMATED_SUITE, localPersistenceService),
      deps: [LocalPersistenceService],
    },
    {
      provide: ITERATION_AUTOMATED_SUITE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, ITERATION_AUTOMATED_SUITE_TABLE_CONF, ReferentialDataService],
    },
    { provide: GridService, useExisting: ITERATION_AUTOMATED_SUITE_TABLE },
    {
      provide: UserHistorySearchProvider,
      useClass: TestSuiteAutomatedSuiteProvider,
    },
    {
      provide: GRID_PERSISTENCE_KEY,
      useValue: GridId.ITERATION_AUTOMATED_SUITE,
    },
    GridWithStatePersistence,
  ],
})
export class IterationAutomatedSuiteComponent implements OnInit, AfterViewInit, OnDestroy {
  isPremium: boolean;
  componentData$: Observable<IterationComponentData>;
  unsub$ = new Subject<void>();
  canCompare$: Observable<boolean>;
  $hasSelectedRows: Signal<boolean>;
  $selectedRowsContainsUnfinished: Signal<boolean>;
  $canDeleteOrPrune: Signal<boolean>;
  $pruneMenuVisible = signal(false);

  activeFilters$: Observable<GridFilter[]>;
  openSelectFilterMenu = false;
  private overlayRef: OverlayRef;
  @ViewChild('content', { read: ElementRef })
  content: ElementRef;
  @ViewChild('iconColumnElement')
  iconColumnElement: ElementRef;

  constructor(
    private iterationViewService: IterationViewService,
    private gridService: GridService,
    private dialogService: DialogService,
    private vcr: ViewContainerRef,
    private referentialDataService: ReferentialDataService,
    private overlay: Overlay,
    private viewContainerRef: ViewContainerRef,
    private cdRef: ChangeDetectorRef,
    private gridWithStatePersistence: GridWithStatePersistence,
  ) {
    this.referentialDataService.isPremiumPluginInstalled$
      .pipe(takeUntil(this.unsub$))
      .subscribe((isPremium) => {
        this.isPremium = isPremium;
        this.gridService.setColumnVisibility(GridColumnId.workflowViewer, isPremium);
      });

    this.$hasSelectedRows = toSignal(
      this.gridService.selectedRows$.pipe(
        takeUntilDestroyed(),
        map((selectedRows) => selectedRows.length > 0),
      ),
    );

    this.$selectedRowsContainsUnfinished = toSignal(
      this.gridService.selectedRows$.pipe(
        takeUntilDestroyed(),
        map((selectedRows) =>
          selectedRows.some(
            (row) => row.data.executionStatus === 'RUNNING' || row.data.executionStatus === 'READY',
          ),
        ),
      ),
    );

    this.$canDeleteOrPrune = toSignal(
      this.gridService.selectedRows$.pipe(
        takeUntilDestroyed(),
        map(
          (selectedRows) =>
            selectedRows.every((row) => row.simplePermissions && row.simplePermissions.canDelete) &&
            selectedRows.some(
              (row) =>
                row.data.executionStatus !== 'READY' && row.data.executionStatus !== 'RUNNING',
            ),
        ),
      ),
    );

    this.canCompare$ = this.gridService.selectedRows$.pipe(
      takeUntilDestroyed(),
      map((dataRows) => dataRows.length > 1),
    );
  }

  ngOnInit(): void {
    this.componentData$ = this.iterationViewService.componentData$;
    this.initializeGridFilters();
  }

  ngAfterViewInit(): void {
    this.gridWithStatePersistence.popGridState().subscribe(() => this.fetchTestPlan());

    this.activeFilters$ = this.gridService.activeFilters$.pipe(
      takeUntil(this.unsub$),
      map((gridFilters: GridFilter[]) =>
        gridFilters.filter((gridFilter) => GridFilterUtils.mustIncludeFilter(gridFilter)),
      ),
    );

    this.gridWithStatePersistence
      .popGridState()
      .pipe(
        take(1),
        switchMap(() => this.fetchTestPlan()),
      )
      .subscribe();
  }

  private fetchTestPlan(): Observable<[IterationViewComponentData, boolean]> {
    return this.iterationViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      filter((componentData: IterationViewComponentData) => Boolean(componentData.iteration.id)),
      take(1),
      withLatestFrom(this.referentialDataService.isPremiumPluginInstalled$),
      tap(([componentData, isPremium]) =>
        this.gridService.setServerUrl(
          [`iteration/${componentData.iteration.id}/automated-suite`],
          isPremium,
        ),
      ),
    );
  }

  ngOnDestroy(): void {
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  private initializeGridFilters() {
    this.gridService.addFilters(this.buildGridFilters());
  }

  private buildGridFilters(): GridFilter[] {
    return buildFilters([
      executionStatusFilter(
        GridColumnId.executionStatus,
        ResearchColumnPrototype.EXECUTION_STATUS,
      ).alwaysActive(),
      userHistoryResearchFilter(
        GridColumnId.createdBy,
        ResearchColumnPrototype.AUTOMATED_SUITE_USER,
      ).alwaysActive(),
      dateRangeFilter(GridColumnId.createdOn, ResearchColumnPrototype.AUTOMATED_SUITE_CREATED_ON),
      dateRangeFilter(
        GridColumnId.lastModifiedOn,
        ResearchColumnPrototype.AUTOMATED_SUITE_LAST_MODIFIED_ON,
      ),
      serverBackedGridTextFilter(GridColumnId.launchedFrom),
      serverBackedGridTextFilter(GridColumnId.environmentTags),
      serverBackedGridTextFilter(GridColumnId.environmentVariables),
    ]);
  }

  openExecutionComparisonDialog() {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        map((rows) =>
          rows.map((row) => {
            return {
              id: row.data.suiteId,
              createdOn: row.data.createdOn,
              statuses: [
                { nbStatusSuccess: row.data.nbStatusSuccess },
                { nbStatusFailed: row.data.nbStatusFailure },
                { nbStatusOther: row.data.nbStatusOther },
              ],
            };
          }),
        ),
      )
      .subscribe((suites: SuiteIdCreatedOn[]) => {
        this.dialogService.openDialog({
          id: 'automated-suite-execution-comparator',
          component: AutomatedSuiteExecComparatorDialogComponent,
          viewContainerReference: this.vcr,
          data: suites,
          minWidth: 800,
        });
      });
  }

  shouldShowResetFilterLink(activeFilters: GridFilter[]): boolean {
    return activeFilters?.length > 0;
  }

  showAvailableFilters(): void {
    this.openSelectFilterMenu = true;

    const positionStrategy = this.overlay
      .position()
      .flexibleConnectedTo(this.iconColumnElement)
      .withPositions([
        { originX: 'center', overlayX: 'center', originY: 'bottom', overlayY: 'top', offsetY: 0 },
        { originX: 'center', overlayX: 'center', originY: 'top', overlayY: 'bottom', offsetY: -10 },
      ]);

    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
    };

    this.overlayRef = this.overlay.create(overlayConfig);
    const componentPortal = new ComponentPortal(CustomColumnsComponent, this.viewContainerRef);
    const componentComponentRef = this.overlayRef.attach(componentPortal);

    componentComponentRef.instance.projectId =
      this.iterationViewService.getSnapshot().iteration.projectId;

    this.overlayRef.backdropClick().subscribe(() => this.close());
    componentComponentRef.instance.confirmClicked.subscribe(() => this.close());
  }

  close() {
    if (this.overlayRef) {
      this.openSelectFilterMenu = false;
      this.overlayRef.dispose();
      this.cdRef.detectChanges();
    }
  }

  openDeleteAutomatedSuitesDialog() {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        map((rows) =>
          rows
            .filter(
              (row) =>
                row.data[GridColumnId.executionStatus] !== 'RUNNING' &&
                row.data[GridColumnId.executionStatus] !== 'READY',
            )
            .map((row) => row.data.suiteId),
        ),
        switchMap((suiteIds) => this.doOpenDeleteAutomatedSuitesDialog(suiteIds)),
        finalize(() => this.gridService.completeAsyncOperation()),
      )
      .subscribe(() => this.gridService.refreshData());
  }

  showPruneMenu() {
    if (this.$hasSelectedRows && this.$canDeleteOrPrune) {
      this.$pruneMenuVisible.set(true);
    }
  }

  openPruneAutomatedSuitesDialog(isComplete: boolean) {
    this.$pruneMenuVisible.set(false);

    this.gridService.selectedRows$
      .pipe(
        take(1),
        map((rows) =>
          rows
            .filter(
              (row) =>
                row.data[GridColumnId.executionStatus] !== 'RUNNING' &&
                row.data[GridColumnId.executionStatus] !== 'READY',
            )
            .map((row) => row.data.suiteId),
        ),
        switchMap((suiteIds) => this.doOpenPruneAutomatedSuiteDialog(isComplete, suiteIds)),
        finalize(() => this.gridService.completeAsyncOperation()),
      )
      .subscribe(() => this.gridService.refreshData());
  }

  private doOpenPruneAutomatedSuiteDialog(complete: boolean, suiteIds: string[]): Observable<void> {
    let messageKey: string;

    if (!this.$selectedRowsContainsUnfinished()) {
      messageKey = complete
        ? 'sqtm-core.campaign-workspace.automated-suite.attachment-prune.complete.message'
        : 'sqtm-core.campaign-workspace.automated-suite.attachment-prune.partial.message';
    } else {
      messageKey = complete
        ? 'sqtm-core.campaign-workspace.automated-suite.attachment-prune.complete.running-message'
        : 'sqtm-core.campaign-workspace.automated-suite.attachment-prune.partial.running-message';
    }

    const dialogReference = this.dialogService.openConfirm({
      id: 'sqtm-app-automated-suite-attachment-prune',
      titleKey: 'sqtm-core.campaign-workspace.automated-suite.attachment-prune.title',
      messageKey,
      level: 'DANGER',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      filter((result) => result === true),
      tap(() => this.gridService.beginAsyncOperation()),
      switchMap(() => this.iterationViewService.pruneAutomatedSuites(suiteIds, complete)),
    );
  }

  private doOpenDeleteAutomatedSuitesDialog(suiteIds: string[]): Observable<void> {
    const messageKey = this.$selectedRowsContainsUnfinished()
      ? 'sqtm-core.campaign-workspace.automated-suite.delete-many.running-message'
      : 'sqtm-core.campaign-workspace.automated-suite.delete-many.message';

    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.campaign-workspace.automated-suite.delete-many.title',
      messageKey,
      level: 'DANGER',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      filter((result) => result === true),
      tap(() => this.gridService.beginAsyncOperation()),
      switchMap(() => this.iterationViewService.deleteAutomatedSuites(suiteIds)),
    );
  }
}
