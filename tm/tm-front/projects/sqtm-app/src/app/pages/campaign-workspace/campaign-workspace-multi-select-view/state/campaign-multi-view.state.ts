import {
  CustomDashboardModel,
  EntityScope,
  ExecutionStatusKeys,
  StatisticsBundle,
} from 'sqtm-core';

export interface CampaignMultiViewState {
  statistics: StatisticsBundle;
  scope: EntityScope[];
  dashboard: CustomDashboardModel;
  generatedDashboardOn: Date;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
  dashboardLoaded: boolean;
  disabledExecutionStatus: ExecutionStatusKeys[];
}

export function provideInitialCampaignMultiView(): Readonly<CampaignMultiViewState> {
  return {
    statistics: null,
    scope: [],
    dashboard: null,
    canShowFavoriteDashboard: false,
    favoriteDashboardId: null,
    generatedDashboardOn: null,
    shouldShowFavoriteDashboard: false,
    dashboardLoaded: false,
    disabledExecutionStatus: [],
  };
}
