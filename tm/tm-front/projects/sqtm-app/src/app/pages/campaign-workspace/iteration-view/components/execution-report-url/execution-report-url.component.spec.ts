import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExecutionReportUrlComponent } from './execution-report-url.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppTestingUtilsModule } from '../../../../../utils/testing-utils/app-testing-utils.module';

describe('ExecutionReportUrlComponent', () => {
  let component: ExecutionReportUrlComponent;
  let fixture: ComponentFixture<ExecutionReportUrlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule],
      declarations: [ExecutionReportUrlComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ExecutionReportUrlComponent);
    component = fixture.componentInstance;
    component.executionId = -1;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
