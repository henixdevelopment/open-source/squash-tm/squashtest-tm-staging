import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
  ManagementMode,
  RemotePerimeterStatus,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-remote-perimeter-status-cell',
  template: `
    @if (row && isSynchronized()) {
      <div class="full-width full-height flex-column">
        <i
          class="m-l-10 font-18-px m-auto"
          [ngClass]="getRemotePerimeterStatusIconColor()"
          [attr.data-test-icon-id]="'remote-perimeter-status'"
          nz-tooltip
          [nzTooltipTitle]="getRemotePerimeterStatusI18nKey()"
          nz-icon
          [nzType]="'retweet'"
        >
        </i>
      </div>
    }
  `,
  styleUrl: './remote-perimeter-status-cell.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RemotePerimeterStatusCellComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    private translateService: TranslateService,
  ) {
    super(grid, cdRef);
  }

  isSynchronized(): boolean {
    return this.row.data.mode === ManagementMode.SYNCHRONIZED;
  }

  getRemotePerimeterStatusIconColor() {
    const remotePerimeterStatus = this.row.data['remotePerimeterStatus'];
    switch (remotePerimeterStatus) {
      case RemotePerimeterStatus.IN_CURRENT_PERIMETER:
        return 'remote-in-perimeter';
      case RemotePerimeterStatus.OUT_OF_CURRENT_PERIMETER:
        return 'remote-out-of-perimeter';
      case RemotePerimeterStatus.NOT_FOUND:
        return 'remote-deleted';
      case RemotePerimeterStatus.UNKNOWN:
        return '';
      default:
        throw Error(`unknown Remote Perimeter Status: ${remotePerimeterStatus}`);
    }
  }

  getRemotePerimeterStatusI18nKey(): string {
    const remotePerimeterStatus = this.row.data['remotePerimeterStatus'];
    return this.translateService.instant(
      `sqtm-core.entity.requirement.remote-req-perimeter-status.${remotePerimeterStatus}`,
    );
  }
}

export function remotePerimeterStatusColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(RemotePerimeterStatusCellComponent)
    .withLabel('');
}
