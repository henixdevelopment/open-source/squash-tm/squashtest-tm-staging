import { ChangeDetectionStrategy, Component } from '@angular/core';
import { take, tap } from 'rxjs/operators';
import {
  ExportModelBuilderService,
  Extendable,
  Fixed,
  grid,
  GridColumnId,
  GridDefinition,
  GridExportService,
  GridId,
  GridService,
  gridServiceFactory,
  indexColumn,
  issueExecutionsColumn,
  issueKeyColumn,
  Limited,
  LocalPersistenceService,
  PaginationConfigBuilder,
  ReferentialDataService,
  RestService,
  Sort,
  textColumn,
} from 'sqtm-core';
import { ITERATION_ISSUE_TABLE, ITERATION_ISSUE_TABLE_CONF } from '../../iteration-view.constant';
import { IterationViewService } from '../../services/iteration-view.service';

@Component({
  selector: 'sqtm-app-iteration-issues',
  template: ` @if (iterationViewService.componentData$ | async; as componentData) {
    <div class="flex-column full-height full-width p-r-15 overflow-y-hidden">
      <div class="flex-row p-r-10 view-title">
        <div class="sqtm-large-grid-container-title">
          {{ 'sqtm-core.test-case-workspace.title.issues' | translate }}
        </div>
        <div class="collapse-button flex-row">
          @if (componentData.permissions.canExport) {
            <sqtm-core-grid-export-menu
              [exportContent]="'sqtm-core.grid.export.issue' | translate"
              [workspaceName]="'sqtm-core.campaign-workspace.iteration.label.title' | translate"
              [fileFormat]="'csv'"
            ></sqtm-core-grid-export-menu>
          }
        </div>
      </div>
      <div class="sqtm-large-grid-container full-width full-height p-15 m-b-10">
        <sqtm-core-issues-panel
          [entityId]="componentData.iteration.id"
          [entityType]="'ITERATION_TYPE'"
          [bugTracker]="componentData.projectData.bugTracker"
          (loadIssues)="loadData()"
        >
          <sqtm-core-grid></sqtm-core-grid>
        </sqtm-core-issues-panel>
      </div>
    </div>
  }`,
  styleUrls: ['./iteration-issues.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: ITERATION_ISSUE_TABLE_CONF,
      useFactory: iterationIssuesTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: ITERATION_ISSUE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, ITERATION_ISSUE_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: ITERATION_ISSUE_TABLE,
    },
    ExportModelBuilderService,
    GridExportService,
  ],
})
export class IterationIssuesComponent {
  constructor(
    public iterationViewService: IterationViewService,
    private gridService: GridService,
  ) {}

  loadData() {
    this.iterationViewService.componentData$
      .pipe(
        take(1),
        tap((componentData) =>
          this.gridService.setServerUrl([
            `issues/iteration/${componentData.iteration.id}/known-issues`,
          ]),
        ),
      )
      .subscribe();
  }
}

export function iterationIssuesTableDefinition(
  localPersistenceService: LocalPersistenceService,
): GridDefinition {
  return grid(GridId.ITERATION_VIEW_ISSUE)
    .withColumns([
      indexColumn().changeWidthCalculationStrategy(new Fixed(70)).withViewport('leftViewport'),
      issueKeyColumn()
        .withI18nKey('sqtm-core.entity.issue.key.label')
        .changeWidthCalculationStrategy(new Limited(100)),
      textColumn(GridColumnId.btProject)
        .withI18nKey('sqtm-core.entity.issue.project.label')
        .changeWidthCalculationStrategy(new Limited(150))
        .disableSort(),
      textColumn(GridColumnId.summary)
        .withI18nKey('sqtm-core.entity.issue.summary.label')
        .changeWidthCalculationStrategy(new Limited(200))
        .disableSort(),
      textColumn(GridColumnId.priority)
        .withI18nKey('sqtm-core.entity.issue.priority.label')
        .changeWidthCalculationStrategy(new Extendable(100, 0.4))
        .disableSort(),
      textColumn(GridColumnId.status)
        .withI18nKey('sqtm-core.entity.issue.status.label')
        .changeWidthCalculationStrategy(new Limited(80))
        .disableSort(),
      textColumn(GridColumnId.assignee)
        .withI18nKey('sqtm-core.entity.issue.assignee.label')
        .changeWidthCalculationStrategy(new Extendable(100, 1))
        .disableSort(),
      issueExecutionsColumn()
        .withI18nKey('sqtm-core.entity.issue.reported-in.label')
        .changeWidthCalculationStrategy(new Extendable(100, 1.5))
        .disableSort(),
    ])
    .disableRightToolBar()
    .server()
    .withInitialSortedColumns([
      {
        id: GridColumnId.remoteId,
        sort: Sort.DESC,
      },
    ])
    .withPagination(new PaginationConfigBuilder().initialSize(25))
    .withRowHeight(35)
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
}
