import { EntityViewState, provideInitialViewState } from 'sqtm-core';
import { CampaignState } from './campaign.state';

export interface CampaignViewState extends EntityViewState<CampaignState, 'campaign'> {
  campaign: CampaignState;
}

export function provideInitialView(): Readonly<CampaignViewState> {
  return provideInitialViewState<CampaignState, 'campaign'>('campaign');
}
