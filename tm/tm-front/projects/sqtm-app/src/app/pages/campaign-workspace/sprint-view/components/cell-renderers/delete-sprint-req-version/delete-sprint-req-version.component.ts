import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  computed,
  Signal,
} from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import {
  AbstractDeleteCellRenderer,
  ConfirmDeleteLevel,
  DialogService,
  GridService,
  SprintStatus,
} from 'sqtm-core';
import { SprintViewService } from '../../../service/sprint-view.service';
import { SprintViewComponentData } from '../../../containers/sprint-view/sprint-view.component';

@Component({
  selector: 'sqtm-app-delete-sprint-req-version',
  template: ` @if ($canLink() && !$isSprintClosed()) {
    <sqtm-core-delete-icon
      [iconName]="getIcon()"
      (delete)="showDeleteConfirm()"
      nz-tooltip
      [nzTooltipTitle]="
        'sqtm-core.campaign-workspace.dialog.title.remove-sprint-req-version' | translate
      "
    ></sqtm-core-delete-icon>
  }`,
  styleUrls: ['./delete-sprint-req-version.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteSprintReqVersionComponent extends AbstractDeleteCellRenderer {
  $componentData: Signal<SprintViewComponentData> = toSignal(this.sprintViewService.componentData$);
  $canLink: Signal<boolean> = computed(() => {
    return this.row.data.nbExecutions === 0
      ? this.$componentData().permissions.canLink
      : this.$componentData().permissions.canExtendedDelete;
  });
  $isSprintClosed: Signal<boolean> = computed(
    () => this.$componentData().sprint.status === SprintStatus.CLOSED.id,
  );

  constructor(
    grid: GridService,
    protected cdr: ChangeDetectorRef,
    dialogService: DialogService,
    private sprintViewService: SprintViewService,
  ) {
    super(grid, cdr, dialogService);
  }

  getIcon(): string {
    return this.row.data.nbExecutions === 0
      ? 'sqtm-core-generic:unlink'
      : 'sqtm-core-generic:delete';
  }

  protected getTitleKey(): string {
    return 'sqtm-core.campaign-workspace.dialog.title.remove-sprint-req-version';
  }

  protected getMessageKey(): string {
    return this.row.data.nbExecutions === 0
      ? 'sqtm-core.campaign-workspace.dialog.message.remove-sprint-req-version'
      : 'sqtm-core.campaign-workspace.dialog.message.remove-sprint-req-version-with-executions';
  }

  protected getLevel(): ConfirmDeleteLevel {
    return this.row.data.nbExecutions === 0 ? 'WARNING' : 'DANGER';
  }

  doDelete(): any {
    this.sprintViewService.deleteSprintReqVersion(this.row.id as number);
    this.sprintViewService.updateLastModification();
  }
}
