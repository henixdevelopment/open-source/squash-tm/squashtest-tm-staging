import { EntityViewComponentData, ExecutionStatusKeys } from 'sqtm-core';
import { Observable } from 'rxjs';

/**
 * Generic facade of TestPlan owner view services for consumers that don't care about the concrete
 * entities (e.g. TestPlanItemExecutionStatusesCellRendererComponent).
 */
export interface TestPlanOwnerView {
  componentData$: Observable<EntityViewComponentData<any, any, any>>;
  canUpdateExecutionStatus$: Observable<boolean>;

  updateExecutionStatus(
    executionStatus: ExecutionStatusKeys,
    testPlanItemId: number,
  ): Observable<unknown>;
}
