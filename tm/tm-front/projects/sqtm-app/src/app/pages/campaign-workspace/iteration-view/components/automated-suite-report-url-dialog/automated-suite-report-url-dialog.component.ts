import { ChangeDetectionStrategy, Component, ViewContainerRef } from '@angular/core';
import {
  AttachmentModel,
  AttachmentService,
  DialogConfiguration,
  DialogReference,
  DialogService,
  FileViewerComponent,
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-automated-suite-report-url-dialog',
  templateUrl: './automated-suite-report-url-dialog.component.html',
  styleUrls: ['./automated-suite-report-url-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AutomatedSuiteReportUrlDialogComponent {
  conf: any;
  attachments: AttachmentModel[];

  constructor(
    private dialogReference: DialogReference<DialogConfiguration>,
    private attachmentService: AttachmentService,
    private vcr: ViewContainerRef,
    private dialogService: DialogService,
  ) {
    this.conf = this.dialogReference.data;
    this.attachments = this.conf.attachmentList.attachments;
  }

  getAttachmentDownloadURL(attachmentListId: number, attachment: any) {
    return this.attachmentService.getAttachmentDownloadURL(attachmentListId, attachment);
  }

  openFilePreviewDialog(event: any, attachment: AttachmentModel) {
    event.preventDefault();

    const attachmentIndex: number = this.attachments.indexOf(attachment);

    this.dialogService.openDialog({
      id: 'automated-suite-file-viewer',
      component: FileViewerComponent,
      viewContainerReference: this.vcr,
      data: {
        attachmentList: this.conf.attachmentList,
        index: attachmentIndex,
      },
      minHeight: '100%',
      minWidth: '100%',
    });
  }
}
