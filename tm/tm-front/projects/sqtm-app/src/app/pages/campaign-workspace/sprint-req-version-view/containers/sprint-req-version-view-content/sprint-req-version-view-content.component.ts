import {
  ChangeDetectionStrategy,
  Component,
  computed,
  ElementRef,
  InjectionToken,
  OnDestroy,
  Renderer2,
  Signal,
  signal,
  ViewChild,
} from '@angular/core';
import { SprintReqVersionViewService } from '../../service/sprint-req-version-view.service';
import {
  ActionErrorDisplayService,
  ASSIGNED_USER_DELEGATE,
  assignedUserColumn,
  AvailableTestPlanItemModel,
  booleanColumn,
  centredTextColumn,
  ConfirmConfiguration,
  convertSqtmLiterals,
  DataRow,
  DialogService,
  executionModeColumn,
  Fixed,
  grid,
  GRID_PERSISTENCE_KEY,
  GridColumnId,
  GridDefinition,
  GridDndData,
  GridId,
  GridService,
  gridServiceFactory,
  GridWithStatePersistence,
  indexColumn,
  isDndDataFromTestCaseTreePicker,
  Limited,
  LocalPersistenceService,
  ManagementMode,
  parseDataRowId,
  ProjectDataMap,
  ReferentialDataService,
  RestService,
  SqtmDragEnterEvent,
  SqtmDragLeaveEvent,
  SqtmDropEvent,
  SquashTmDataRowType,
  StyleDefinitionBuilder,
  TEST_CASE_TREE_PICKER_ID,
  testCaseImportanceColumn,
  textColumn,
  UserHistorySearchProvider,
} from 'sqtm-core';
import { withProjectLinkColumn, withTestCaseLinkColumn } from '../../../cell-renderer.builders';
import { dataSetColumn } from '../../../campaign-workspace/components/test-plan/dataset-cell-renderer/dataset-cell-renderer.component';
import { SuccessRateComponent } from '../../../iteration-view/components/cell-renderers/success-rate/success-rate.component';
import { testPlanItemPlayColumn } from '../../../../../components/test-plan/cell-renderers/test-plan-item-play-cell-renderer/test-plan-item-play-cell-renderer.component';
import { lastExecutionDateColumn } from '../../../iteration-view/components/cell-renderers/last-execution-date-cell/last-execution-date-cell.component';
import { testPlanItemExecutionStatusesColumn } from '../../../../../components/test-plan/cell-renderers/test-plan-item-execution-statuses-cell-renderer/test-plan-item-execution-statuses-cell-renderer.component';
import { toSignal } from '@angular/core/rxjs-interop';
import { deleteTestPlanItemCellRendererComponent } from '../../../../../components/test-plan/cell-renderers/delete-test-plan-item-cell-renderer/delete-test-plan-item-cell-renderer.component';
import { catchError, concatMap, filter, map, switchMap, take, takeUntil } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { TestPlanDraggedContentComponent } from '../../../../../components/test-plan/test-plan-dragged-content/test-plan-dragged-content.component';
import { GenericTestPlanServerOperationHandler } from '../../../../../components/test-plan/generic-test-plan-server-operation-handler';
import { GENERIC_TEST_PLAN_ITEM_MOVER } from '../../../../../components/test-plan/generic-test-plan-item-mover';
import { SprintReqVersionUpdateTestPlanDialogComponent } from '../../components/sprint-req-version-update-test-plan-dialog/sprint-req-version-update-test-plan-dialog.component';
import { GENERIC_TEST_PLAN_EXECUTION_MANAGER } from '../../../../../components/test-plan/generic-test-plan-execution-manager';
import { SprintReqVersionTestPlanItemAssignableUserProvider } from './sprint-req-version-test-plan-item-assignable-user-provider.service';

const SPRINT_REQ_VERSION_TEST_PLAN_CONFIG = new InjectionToken(
  'Token for sprint requirement version test plan config',
);

const SPRINT_REQ_VERSION_TEST_PLAN = new InjectionToken(
  'Token for sprint requirement version test plan',
);

function testPlanTableDefinition(localPersistenceService: LocalPersistenceService): GridDefinition {
  return grid(GridId.SPRINT_REQ_VERSION_TEST_PLAN)
    .server()
    .withColumns([
      indexColumn().enableDnd().withViewport('leftViewport'),
      withProjectLinkColumn(GridColumnId.projectName)
        .withI18nKey('sqtm-core.entity.project.label.singular')
        .changeWidthCalculationStrategy(new Limited(100))
        .withAssociatedFilter(),
      executionModeColumn(GridColumnId.inferredExecutionMode)
        .withI18nKey('sqtm-core.entity.execution.mode.label')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Fixed(70)),
      textColumn(GridColumnId.testCaseReference)
        .withI18nKey('sqtm-core.entity.generic.reference.label')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Limited(100)),
      withTestCaseLinkColumn(GridColumnId.testCaseName)
        .withI18nKey('sqtm-core.entity.test-case.label.singular')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Limited(160)),
      testCaseImportanceColumn(GridColumnId.importance)
        .withI18nKey('sqtm-core.entity.test-case.importance.label-short-dot')
        .withTitleI18nKey('sqtm-core.entity.test-case.importance.label')
        .isEditable(false)
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Fixed(65)),
      booleanColumn(GridColumnId.linkedToRequirement)
        .withI18nKey('sqtm-core.sprint.req-version.test-plan.linked-to-requirement.short')
        .withTitleI18nKey('sqtm-core.sprint.req-version.test-plan.linked-to-requirement.long')
        .withHeaderPosition('center')
        .withContentPosition('center')
        .changeWidthCalculationStrategy(new Fixed(100))
        .withAssociatedFilter(),
      dataSetColumn(GridColumnId.datasetName, {
        kind: 'dataset',
        itemIdKey: 'testPlanItemId',
        testPlanOwnerType: 'sprint-req-version',
      })
        .withI18nKey('sqtm-core.entity.dataset.label.short')
        .withAssociatedFilter()
        .withTitleI18nKey('sqtm-core.entity.dataset.label.singular')
        .changeWidthCalculationStrategy(new Limited(90)),
      testPlanItemExecutionStatusesColumn(),
      centredTextColumn(GridColumnId.successRate)
        .disableSort()
        .withRenderer(SuccessRateComponent)
        .withI18nKey('sqtm-core.entity.execution-plan.success-rate.label.short')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.success-rate.label.long')
        .changeWidthCalculationStrategy(new Fixed(60)),
      assignedUserColumn(GridColumnId.assigneeFullName)
        .withI18nKey('sqtm-core.generic.label.user')
        .withAssociatedFilter(GridColumnId.assigneeLogin)
        .changeWidthCalculationStrategy(new Limited(200)),
      lastExecutionDateColumn(GridColumnId.lastExecutedOn),
      testPlanItemPlayColumn(),
      deleteTestPlanItemCellRendererComponent(),
    ])
    .server()
    .withRowConverter(testPlanItemLiteralConverter)
    .disableRightToolBar()
    .withRowHeight(35)
    .withStyle(new StyleDefinitionBuilder().enableInitialLoadAnimation().showLines())
    .enableColumnWidthPersistence(localPersistenceService)
    .enableInternalDrop()
    .enableDrag()
    .withDraggedContentRenderer(TestPlanDraggedContentComponent)
    .build();
}

@Component({
  selector: 'sqtm-app-sprint-req-version-view-content',
  templateUrl: './sprint-req-version-view-content.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: SPRINT_REQ_VERSION_TEST_PLAN_CONFIG,
      useFactory: testPlanTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: GENERIC_TEST_PLAN_ITEM_MOVER,
      useExisting: SprintReqVersionViewService,
    },
    GenericTestPlanServerOperationHandler,
    {
      provide: SPRINT_REQ_VERSION_TEST_PLAN,
      useFactory: gridServiceFactory,
      deps: [
        RestService,
        SPRINT_REQ_VERSION_TEST_PLAN_CONFIG,
        ReferentialDataService,
        GenericTestPlanServerOperationHandler,
      ],
    },
    {
      provide: GridService,
      useExisting: SPRINT_REQ_VERSION_TEST_PLAN,
    },
    {
      provide: UserHistorySearchProvider,
      useClass: SprintReqVersionTestPlanItemAssignableUserProvider,
    },
    {
      provide: GRID_PERSISTENCE_KEY,
      useValue: 'sprint-req-version-test-plan-grid',
    },
    GridWithStatePersistence,
    {
      provide: ASSIGNED_USER_DELEGATE,
      useExisting: SprintReqVersionViewService,
    },
    {
      provide: GENERIC_TEST_PLAN_EXECUTION_MANAGER,
      useExisting: SprintReqVersionViewService,
    },
  ],
})
export class SprintReqVersionViewContentComponent implements OnDestroy {
  protected readonly dropZoneId = 'sprintReqVersionTestPlanDropZone';

  protected readonly $componentData = toSignal(this.sprintReqVersionViewService.componentData$);

  protected readonly $isTestCasePickerOpen = signal(false);

  protected readonly $isSprintClosed = computed(() => {
    return this.$componentData().sprintReqVersion.sprintStatus === 'CLOSED';
  });

  protected readonly $isBugTrackerBoundToProject: Signal<boolean> = computed(() => {
    return this.$componentData().projectData.bugTracker !== null;
  });

  protected readonly $hasSelectedRows: Signal<boolean> = toSignal(
    this.testPlanGridService.selectedRows$.pipe(map((rows) => rows.length > 0)),
  );

  protected readonly $canDeleteTestPlanItems = computed(() => {
    return (
      this.$componentData().permissions.canDelete &&
      this.sprintReqVersionViewService.allowTestPlanItemDeletion()
    );
  });

  @ViewChild('dropZone')
  protected dropZone: ElementRef<HTMLDivElement>;

  private unsub$ = new Subject<void>();

  protected readonly ManagementMode = ManagementMode;

  constructor(
    public readonly sprintReqVersionViewService: SprintReqVersionViewService,
    private readonly testPlanGridService: GridService,
    private readonly renderer: Renderer2,
    private readonly dialogService: DialogService,
    private readonly actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    this.initializeActionColumnsVisibility();
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  toggleTestCasePickerDrawer(event: MouseEvent) {
    event.stopPropagation();
    this.$isTestCasePickerOpen.update((open) => !open);
  }

  closeTestCasePickerDrawer() {
    this.$isTestCasePickerOpen.set(false);
  }

  dropIntoTestPlan($event: SqtmDropEvent) {
    if ($event.dragAndDropData.origin === TEST_CASE_TREE_PICKER_ID) {
      this.dropTestCasesIntoTestPlan($event);
    }
  }

  private dropTestCasesIntoTestPlan($event: SqtmDropEvent) {
    const data = $event.dragAndDropData.data as GridDndData;
    const testCaseIds = data.dataRows.map((row) => parseDataRowId(row));

    this.sprintReqVersionViewService
      .addTestCasesIntoTestPlan(testCaseIds)
      .pipe(catchError((error) => this.actionErrorDisplayService.handleActionError(error)))
      .subscribe(() => {
        this.testPlanGridService.refreshData();
        this.unmarkAsDropZone();
      });
  }

  dragEnter($event: SqtmDragEnterEvent) {
    if (isDndDataFromTestCaseTreePicker($event)) {
      this.markAsDropZone();
    }
  }

  dragLeave($event: SqtmDragLeaveEvent) {
    if (isDndDataFromTestCaseTreePicker($event)) {
      this.unmarkAsDropZone();
    }
  }

  dragCancel() {
    this.unmarkAsDropZone();
  }

  private markAsDropZone() {
    this.renderer.addClass(this.dropZone.nativeElement, 'sqtm-core-border-current-workspace-color');
  }

  private unmarkAsDropZone() {
    this.renderer.removeClass(
      this.dropZone.nativeElement,
      'sqtm-core-border-current-workspace-color',
    );
  }

  handleDeleteTestPlanItemsButtonClicked() {
    this.testPlanGridService.selectedRows$
      .pipe(
        take(1),
        switchMap((selectedRows) => this.openMassDeleteDialog(selectedRows)),
      )
      .subscribe(() => {
        this.testPlanGridService.refreshData();
      });
  }

  private openMassDeleteDialog(selectedRows: DataRow[]): Observable<unknown> {
    const deletableRows = selectedRows.filter((row) =>
      row.data.lastExecutedOn == null
        ? row.simplePermissions.canDelete
        : row.simplePermissions.canExtendedDelete,
    );
    const hasExecutions = selectedRows.some((row) => row.data.lastExecutedOn != null);
    const messageKey = this.getDeleteDialogMessageKey(selectedRows, deletableRows, hasExecutions);

    const dialogConf: ConfirmConfiguration = {
      id: 'iteration-tpi-mass-delete-dialog',
      titleKey: 'sqtm-core.campaign-workspace.dialog.title.mass-remove-itpi',
      messageKey: messageKey,
      level: hasExecutions ? 'DANGER' : 'WARNING',
    };

    const dialogReference = this.dialogService.openConfirm(dialogConf, 600);

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      filter((confirm) => confirm),
      concatMap(() => {
        const itemTestPlanIds = deletableRows.map((row) => row.data.testPlanItemId);
        return this.sprintReqVersionViewService.deleteTestPlanItems(itemTestPlanIds);
      }),
      catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
    );
  }

  private getDeleteDialogMessageKey(
    selectedRows: DataRow[],
    deletableRows: DataRow[],
    hasExecutions: boolean,
  ): string {
    const allSelectedRowsAreDeletable = selectedRows.length === deletableRows.length;

    let messageKey = 'sqtm-core.campaign-workspace.dialog.message.mass-remove-association.unbind';

    if (hasExecutions) {
      messageKey = allSelectedRowsAreDeletable
        ? 'sqtm-core.campaign-workspace.dialog.message.mass-remove-association.delete'
        : 'sqtm-core.campaign-workspace.dialog.message.mass-remove-association.delete-only-deletable';
    }
    return messageKey;
  }

  handleUpdateTestPlanButtonClicked($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();

    this.sprintReqVersionViewService.fetchAvailableTestPlanItems().subscribe((items) => {
      if (items.length === 0) {
        this.showNoTestPlanItemsToSynchronizeDialog();
      } else {
        this.showSynchronizeTestPlanDialog(items);
      }
    });
  }

  private showNoTestPlanItemsToSynchronizeDialog() {
    this.dialogService.openAlert({
      id: 'synchronize-test-plan-dialog',
      level: 'INFO',
      titleKey: 'sqtm-core.campaign-workspace.test-plan.update-sprint-req-version.title',
      messageKey:
        'sqtm-core.campaign-workspace.test-plan.update-sprint-req-version.already-up-to-date',
    });
  }

  private showSynchronizeTestPlanDialog(availableItems: AvailableTestPlanItemModel[]) {
    this.dialogService
      .openDialog({
        id: 'sprint-req-version-update-test-plan',
        component: SprintReqVersionUpdateTestPlanDialogComponent,
        data: {
          availableItems,
          sprintReqVersionId: this.$componentData().sprintReqVersion.id,
        },
      })
      .dialogClosed$.pipe(filter((result) => Boolean(result)))
      .subscribe(() => {
        this.sprintReqVersionViewService.updateLastModification();
        this.testPlanGridService.refreshData();
      });
  }

  private initializeActionColumnsVisibility() {
    this.sprintReqVersionViewService.componentData$
      .pipe(
        takeUntil(this.unsub$),
        map((data) => data.sprintReqVersion.sprintStatus),
      )
      .subscribe((sprintStatus) => {
        const isSprintClosed = sprintStatus === 'CLOSED';
        this.testPlanGridService.setColumnVisibility(GridColumnId.delete, !isSprintClosed);
      });
  }
}

export function testPlanItemLiteralConverter(
  literals: Partial<DataRow>[],
  projectDataMap: ProjectDataMap,
): DataRow[] {
  const itpiLiterals = literals.map((li) => ({
    ...li,
    type: SquashTmDataRowType.TestPlanItem,
  }));

  return convertSqtmLiterals(itpiLiterals, projectDataMap);
}
