import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable } from 'rxjs';
import { SprintGroupViewService } from '../../service/sprint-group-view.service';
import { SprintGroupViewComponentData } from '../sprint-group-view/sprint-group-view.component';

@Component({
  selector: 'sqtm-app-sprint-group-view-content',
  templateUrl: './sprint-group-view-content.component.html',
  styleUrls: ['./sprint-group-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SprintGroupViewContentComponent {
  componentData$: Observable<SprintGroupViewComponentData>;
  constructor(private sprintGroupViewService: SprintGroupViewService) {
    this.componentData$ = this.sprintGroupViewService.componentData$;
  }
}
