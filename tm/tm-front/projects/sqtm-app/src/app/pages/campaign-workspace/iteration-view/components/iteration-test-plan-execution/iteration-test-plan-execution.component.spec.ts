import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {
  IterationTestPlanExecutionComponent,
  itvItpeTableDefinition,
} from './iteration-test-plan-execution.component';
import {
  CampaignPermissions,
  DataRow,
  DialogService,
  EntityViewComponentData,
  GridService,
  GridTestingModule,
  GridWithStatePersistence,
  InterWindowCommunicationService,
  InterWindowMessages,
  RestService,
  WorkspaceWithTreeComponent,
} from 'sqtm-core';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ITV_ITPE_TABLE, ITV_ITPE_TABLE_CONF } from '../../iteration-view.constant';
import { BehaviorSubject, of, Subject } from 'rxjs';
import { IterationViewService } from '../../services/iteration-view.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppTestingUtilsModule } from '../../../../../utils/testing-utils/app-testing-utils.module';
import {
  mockClosableDialogService,
  mockGridService,
  mockGridWithStatePersistenceService,
  mockRestService,
} from '../../../../../utils/testing-utils/mocks.service';
import { IterationState } from '../../state/iteration.state';
import { mockGlobalConfiguration } from '../../../../../utils/testing-utils/mocks.data';
import { RouterTestingModule } from '@angular/router/testing';
import SpyObj = jasmine.SpyObj;

type ComponentData = EntityViewComponentData<IterationState, 'iteration', CampaignPermissions>;

describe('IterationTestPlanExecutionComponent', () => {
  let component: IterationTestPlanExecutionComponent;
  let fixture: ComponentFixture<IterationTestPlanExecutionComponent>;
  const executionStatusMap = new Map<number, string>();
  executionStatusMap.set(3, 'READY');
  executionStatusMap.set(5, 'READY');

  const componentData$ = new BehaviorSubject<ComponentData>({
    globalConfiguration: mockGlobalConfiguration({ milestoneFeatureEnabled: true }),
    permissions: {
      canLink: true,
    },
    iteration: {
      id: 2,
      hasDatasets: true,
      uiState: {
        openTestCaseTreePicker: false,
      },
      executionStatusMap,
    } as unknown as IterationState,
  } as ComponentData);

  let iterationViewService: SpyObj<IterationViewService>;

  let gridService: SpyObj<GridService>;
  let interWindowCommunicationService;
  let restService;
  let workspaceWithTree;
  let dialogMock;
  let gridWithStatePersistence;

  beforeEach(waitForAsync(() => {
    gridService = mockGridService();
    interWindowCommunicationService = { interWindowMessages$: new Subject<InterWindowMessages>() };
    restService = mockRestService();
    workspaceWithTree = jasmine.createSpyObj(['requireNodeRefresh']);
    workspaceWithTree.gridService = mockGridService();
    dialogMock = mockClosableDialogService();
    gridWithStatePersistence = mockGridWithStatePersistenceService();

    iterationViewService = jasmine.createSpyObj<IterationViewService>([
      'toggleTestCaseTreePicker',
      'refreshStateAfterDeletingTestPlanItems',
      'load',
      'updateStateAfterNewSuiteCreated',
    ]);
    iterationViewService.componentData$ = componentData$;
    iterationViewService.refreshStateAfterDeletingTestPlanItems.and.returnValue(of(null));
    iterationViewService.load.and.returnValue(of(null));

    TestBed.configureTestingModule({
      declarations: [IterationTestPlanExecutionComponent],
      imports: [
        GridTestingModule,
        AppTestingUtilsModule,
        HttpClientTestingModule,
        RouterTestingModule,
      ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: ITV_ITPE_TABLE_CONF,
          useFactory: itvItpeTableDefinition,
        },
        {
          provide: ITV_ITPE_TABLE,
          useValue: gridService,
        },
        {
          provide: GridService,
          useValue: gridService,
        },
        { provide: IterationViewService, useValue: iterationViewService },
        { provide: DialogService, useValue: dialogMock.service },
        { provide: InterWindowCommunicationService, useValue: interWindowCommunicationService },
        { provide: RestService, useValue: restService },
        { provide: WorkspaceWithTreeComponent, useValue: workspaceWithTree },
        { provide: GridWithStatePersistence, useValue: gridWithStatePersistence },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IterationTestPlanExecutionComponent);
    component = fixture.componentInstance;
    component['gridService'] = gridService; // Needed to override the component's own providers
    component['gridWithStatePersistence'] = gridWithStatePersistence;
    gridService.activeFilters$ = of([]);
    fixture.detectChanges();

    gridService.refreshData.calls.reset();
    dialogMock.resetSubjects();
    dialogMock.resetCalls();
  });

  it('should toggle test case picker drawer', () => {
    component.toggleTestCasePickerDrawer();
    expect(iterationViewService.toggleTestCaseTreePicker).toHaveBeenCalled();
  });

  it('should open mass delete dialog', () => {
    gridService.selectedRows$ = of([
      {
        data: { itemTestPlanId: 3, iterationId: 2 },
        simplePermissions: { canWrite: true, canExtendedDelete: true },
      } as unknown as DataRow,
      {
        data: { itemTestPlanId: 5, iterationId: 2 },
        simplePermissions: { canWrite: true, canExtendedDelete: true },
      } as unknown as DataRow,
    ]);
    component.showMassDeleteItpiDialog(2);
    dialogMock.closeDialogsWithResult(true);

    expect(restService.delete).toHaveBeenCalledWith(['iteration', '2', 'test-plan', '3,5']);
    expect(gridService.refreshData).toHaveBeenCalled();
  });

  it('should open mass edit dialog', () => {
    gridService.selectedRows$ = of([{ data: { itemTestPlanId: 2 } } as unknown as DataRow]);
    component.openMassEditDialog();
    dialogMock.closeDialogsWithResult(true);

    expect(gridService.refreshData).toHaveBeenCalled();
    expect(workspaceWithTree.requireNodeRefresh).toHaveBeenCalledWith(['Iteration-2']);
  });

  it('should refresh data when execution step status changed from external window', () => {
    interWindowCommunicationService.interWindowMessages$.next(
      new InterWindowMessages('EXECUTION-STEP-CHANGED'),
    );

    expect(gridService.refreshData).toHaveBeenCalled();
  });

  it('should open test suite creation dialog', () => {
    restService.post.and.returnValue(of({}));
    gridService.selectedRows$ = of([{ data: { itemTestPlanId: 2 } } as unknown as DataRow]);
    component.openCreateTestSuiteDialog();
    dialogMock.closeDialogsWithResult({
      data: {
        ID: 123,
        parentEntityReference: 'TestSuite-2',
      },
    });

    expect(restService.post).toHaveBeenCalledWith(['test-suites/test-plan/bind'], {
      itemIds: [2],
      testSuiteIds: [123],
    });
    expect(gridService.refreshData).toHaveBeenCalled();
  });
});
