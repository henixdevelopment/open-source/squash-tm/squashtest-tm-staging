import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  QueryList,
  Signal,
  ViewChildren,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BehaviorSubject, combineLatest, EMPTY, interval, Observable, of, Subject } from 'rxjs';
import { catchError, map, switchMap, take, takeUntil, tap, withLatestFrom } from 'rxjs/operators';
import {
  ActionErrorDisplayService,
  AutomatedItemOverview,
  AutomatedSuiteCreationSpecification,
  AutomatedSuiteOverview,
  AutomatedSuitePreview,
  AutomatedSuiteService,
  BoundEnvironmentVariable,
  DataRow,
  DialogReference,
  EntityType,
  EnvironmentVariableValue,
  EvInputType,
  ReferentialDataService,
} from 'sqtm-core';
import { SquashAutomProjectPreviewComponent } from '../squash-autom-project-preview-component/squash-autom-project-preview.component';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sqtm-app-automated-tests-execution-supervision-dialog',
  templateUrl: './automated-tests-execution-supervision-dialog.component.html',
  styleUrls: ['./automated-tests-execution-supervision-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AutomatedTestsExecutionSupervisionDialogComponent
  implements OnInit, AfterViewInit, OnDestroy
{
  readonly BodyLayout = BodyLayout;
  readonly FooterLayout = FooterLayout;

  @ViewChildren(SquashAutomProjectPreviewComponent)
  squashAutomProjectPreviews: QueryList<SquashAutomProjectPreviewComponent>;

  specification: AutomatedSuiteCreationSpecification;
  configuration: AutomatedSuitePreview;

  tfExecutionDataRows: Partial<DataRow>[] = [];
  tfExecutionProgress = 0;
  errorMessage: string = null;
  workflowsUuids: string[] = null;
  statusIcon: 'DANGER' | 'INFO' = null;

  squashAutomExecutionDataRows: Partial<DataRow>[] = [];
  squashAutomExecutionProgress = 0;

  jenkinsProjectsFormGroup: FormGroup;
  squashAutomProjectsFormGroup: FormGroup;

  private automatedSuiteId: string;

  areAllPreviewsLaunchable$ = new BehaviorSubject<boolean>(false);

  currentBodyLayout$: BehaviorSubject<BodyLayout> = new BehaviorSubject<BodyLayout>(
    BodyLayout.LOADING,
  );
  currentFooterLayout: FooterLayout;

  private interval$: Observable<any> = interval(5000);

  private unsub$ = new Subject<void>();
  private isVisible: boolean;

  $jwtSecretDefined: Signal<boolean> = toSignal(this.referentialDataService.isJwtSecretDefined$);

  constructor(
    private dialogRef: DialogReference<AutomatedSuitePreview>,
    private fb: FormBuilder,
    private automatedSuiteService: AutomatedSuiteService,
    private cdr: ChangeDetectorRef,
    private referentialDataService: ReferentialDataService,
    private actionErrorDisplayService: ActionErrorDisplayService,
  ) {}

  ngOnInit(): void {
    this.jenkinsProjectsFormGroup = this.fb.group({});
    this.squashAutomProjectsFormGroup = this.fb.group({});
    this.specification = this.dialogRef.data as unknown as AutomatedSuiteCreationSpecification;

    this.updateScripts()
      .pipe(
        switchMap(() =>
          this.automatedSuiteService.generateAutomatedSuitePreview(this.specification),
        ),
        catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
      )
      .subscribe((preview: AutomatedSuitePreview) => {
        this.configuration = preview;
        // If pop-up contains only automated projects with Jenkins type server
        // The button is enabled.
        if (this.configuration.squashAutomProjects.length === 0) {
          this.areAllPreviewsLaunchable$.next(true);
        } else if (!this.$jwtSecretDefined()) {
          this.currentBodyLayout$.next(BodyLayout.ERROR);
          this.currentFooterLayout = FooterLayout.CLOSE_SIMPLE;
          return;
        }
        if (this.configuration.manualServerSelection) {
          this.currentFooterLayout = FooterLayout.CONFIRM_OR_CANCEL;
          this.currentBodyLayout$.next(BodyLayout.READY);
          this.cdr.detectChanges();
        } else {
          this.loadExecutionPhase();
        }
      });

    this.referentialDataService.callbackUrlIsNull$
      .pipe(takeUntil(this.unsub$), take(1))
      .subscribe((callbackUrlIsNull) => (this.isVisible = callbackUrlIsNull));
  }

  ngAfterViewInit(): void {
    this.initializeLaunchableObservable();
  }

  private updateScripts(): Observable<any> {
    if (this.specification.testPlanSubsetIds.length > 0) {
      return this.automatedSuiteService.updateTaScriptsForItems(
        this.specification.testPlanSubsetIds,
      );
    } else if (this.specification.context.type === EntityType.ITERATION) {
      return this.automatedSuiteService.updateTaScriptsForIteration(this.specification.context.id);
    } else if (this.specification.context.type === EntityType.TEST_SUITE) {
      return this.automatedSuiteService.updateTaScriptsForTestSuite(this.specification.context.id);
    }
    return EMPTY;
  }

  initializeLaunchableObservable() {
    this.squashAutomProjectPreviews.changes
      .pipe(
        takeUntil(this.unsub$),
        switchMap(() =>
          combineLatest(
            this.squashAutomProjectPreviews.map((preview) => preview.areAllTestsLaunchable()),
          ),
        ),
        takeUntil(this.unsub$),
      )
      .subscribe((projectCanBeLaunchedArray) => {
        const allProjectsCanBeLaunched = projectCanBeLaunchedArray.every(
          (launchable) => launchable,
        );
        this.areAllPreviewsLaunchable$.next(allProjectsCanBeLaunched);
      });
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  confirmSelectServerStep() {
    this.currentBodyLayout$.next(BodyLayout.LOADING);
    this.getNamespacesByTmProjectId()
      .pipe(take(1), withLatestFrom(this.getEnvironmentVariablesByProjectId()))
      .subscribe(
        ([namespacesByProjectId, environmentVariablesByProject]: [
          any,
          Map<number, BoundEnvironmentVariable[]>,
        ]) => {
          this.configuration.specification.executionConfigurations = Object.keys(
            this.jenkinsProjectsFormGroup.value,
          ).map((key: string) => ({
            projectId: +key,
            node: this.jenkinsProjectsFormGroup.value[key],
          }));
          this.configuration.specification.squashAutomExecutionConfigurations = Object.keys(
            this.squashAutomProjectsFormGroup.value,
          ).map((key: string) => ({
            projectId: +key,
            namespaces: namespacesByProjectId.get(+key),
            environmentTags: this.squashAutomProjectsFormGroup.value[key],
            environmentVariables: this.convertEnvironmentVariables(
              environmentVariablesByProject.get(Number(key)),
            ),
          }));
          this.loadExecutionPhase();
        },
      );
  }

  private convertEnvironmentVariables(environmentVariables: BoundEnvironmentVariable[]): {
    [key: string]: EnvironmentVariableValue;
  } {
    const environmentVariablesMap: { [key: string]: EnvironmentVariableValue } = {};
    environmentVariables.forEach(
      (environmentVariable) =>
        (environmentVariablesMap[environmentVariable.name] = {
          value: environmentVariable.value,
          verbatim: environmentVariable.inputType !== EvInputType.INTERPRETED_TEXT,
        }),
    );
    return environmentVariablesMap;
  }

  loadExecutionPhase() {
    this.currentBodyLayout$.next(BodyLayout.LOADING);
    this.automatedSuiteService
      .createAndExecuteAutomatedSuite(this.configuration.specification)
      .pipe(take(1))
      .subscribe((overview: AutomatedSuiteOverview) => {
        this.dialogRef.result = true;
        this.automatedSuiteId = overview.suiteId;
        this.setMessageAfterWorkflowExecution(overview.errorMessage, overview.workflowsUUIDs);
        if (overview.tfExecutions.length > 0) {
          this.tfExecutionDataRows = overview.tfExecutions.map((execution) => ({
            id: execution.id,
            data: execution,
          }));
        }
        if (overview.automExecutions.length > 0) {
          this.squashAutomExecutionDataRows = overview.automExecutions.map((execution) => ({
            id: execution.id,
            data: execution,
          }));
        }
        this.currentBodyLayout$.next(BodyLayout.READY);
        this.startPullingExecutionStatus();
        this.cdr.detectChanges();
      });
    this.currentFooterLayout = FooterLayout.CLOSE_SIMPLE;
  }

  setMessageAfterWorkflowExecution(errorMessage: string, workflowsUUIDs: string[]): void {
    if (errorMessage != null) {
      this.statusIcon = 'DANGER';
      this.errorMessage = errorMessage;
    } else if (workflowsUUIDs == null) {
      return;
    } else {
      this.statusIcon = 'INFO';
      this.workflowsUuids = workflowsUUIDs;
    }
  }

  startPullingExecutionStatus(): void {
    this.interval$
      .pipe(
        takeUntil(this.unsub$),
        switchMap(() =>
          this.automatedSuiteService.updateExecutionsInformation(this.automatedSuiteId),
        ),
      )
      .subscribe((overview: AutomatedSuiteOverview) => {
        if (this.tfExecutionDataRows.length > 0) {
          this.updateTfSection(overview);
        }
        if (this.squashAutomExecutionDataRows.length > 0) {
          this.updateSquashAutomSection(overview);
        }
        this.cdr.detectChanges();
        if (this.isExecutionPhaseTerminated(overview)) {
          this.unsub$.next();
        }
      });
  }

  private isExecutionPhaseTerminated(overview: AutomatedSuiteOverview) {
    const isTfExecutionComplete = overview.tfPercentage === 100;
    const isSquashAutomExecutionComplete =
      this.squashAutomExecutionDataRows.length === 0 || this.squashAutomExecutionProgress === 100;
    return isTfExecutionComplete && isSquashAutomExecutionComplete;
  }

  private updateTfSection(overview: AutomatedSuiteOverview) {
    this.tfExecutionDataRows = overview.tfExecutions.map((execution) => ({
      id: execution.id,
      data: execution,
    }));
    this.tfExecutionProgress = overview.tfPercentage;
  }

  private updateSquashAutomSection(overview: AutomatedSuiteOverview) {
    this.updateSquashAutomDataRows(overview);
    this.updateSquashAutomPercentage(overview);
  }

  private updateSquashAutomDataRows(receivedOverview: AutomatedSuiteOverview): void {
    this.squashAutomExecutionDataRows = this.squashAutomExecutionDataRows.map((row: DataRow) => {
      const item: AutomatedItemOverview = receivedOverview.automExecutions.find(
        (automExecOverview) => automExecOverview.id === row.id,
      );
      if (item) {
        row.data = {
          ...row.data,
          status: item.status,
        };
      }
      return row;
    });
  }

  private updateSquashAutomPercentage(overview: AutomatedSuiteOverview) {
    const nbExecutions = this.squashAutomExecutionDataRows.length;
    const nbTerminated = overview.automTerminatedCount;
    this.squashAutomExecutionProgress = Math.round((nbTerminated / nbExecutions) * 100);
  }

  closeExecutionPhase() {
    const isTfExecutionIncomplete =
      this.tfExecutionDataRows.length > 0 && this.tfExecutionProgress !== 100;
    const isSquashAutomExecutionIncomplete =
      this.squashAutomExecutionDataRows.length > 0 && this.squashAutomExecutionProgress !== 100;
    if (isTfExecutionIncomplete || isSquashAutomExecutionIncomplete) {
      this.currentFooterLayout = FooterLayout.CLOSE_WARNING;
    } else {
      this.dialogRef.close();
    }
  }

  resumeExecutionPhase() {
    this.currentFooterLayout = FooterLayout.CLOSE_SIMPLE;
  }

  confirmCloseExecutionPhase() {
    this.dialogRef.close();
  }

  getNamespacesByTmProjectId(): Observable<Map<number, string[]>> {
    const namespacesByProjectId = new Map<number, string[]>();
    if (this.squashAutomProjectPreviews != null && this.squashAutomProjectPreviews.length > 0) {
      return combineLatest(
        this.squashAutomProjectPreviews.map((preview) =>
          preview.getNamespacesOfSelectedEnvironments(),
        ),
      ).pipe(
        tap((arrayOfProjectIdAndNamespaces) =>
          arrayOfProjectIdAndNamespaces.forEach((projectIdAndNamespaces) =>
            namespacesByProjectId.set(
              projectIdAndNamespaces.projectId,
              projectIdAndNamespaces.namespaces,
            ),
          ),
        ),
        map(() => namespacesByProjectId),
      );
    } else {
      // if no squashAutomProjectPreviews exist, then return the empty map
      return of(namespacesByProjectId);
    }
  }

  getEnvironmentVariablesByProjectId(): Observable<Map<number, BoundEnvironmentVariable[]>> {
    const environmentVariablesByProjectId = new Map<number, BoundEnvironmentVariable[]>();
    if (this.squashAutomProjectPreviews != null && this.squashAutomProjectPreviews.length > 0) {
      return combineLatest(
        this.squashAutomProjectPreviews.map((preview) =>
          this.getProjectIdAndBoundEnvironmentVariables(preview),
        ),
      ).pipe(
        tap(
          (
            arrayOfProjectIdAndEnvironmentVariables: {
              environmentVariables: BoundEnvironmentVariable[];
              projectId: number;
            }[],
          ) =>
            arrayOfProjectIdAndEnvironmentVariables.forEach((projectIdAndEnvironmentVariables) =>
              environmentVariablesByProjectId.set(
                projectIdAndEnvironmentVariables.projectId,
                projectIdAndEnvironmentVariables.environmentVariables,
              ),
            ),
        ),
        map(() => environmentVariablesByProjectId),
      );
    } else {
      return of(environmentVariablesByProjectId);
    }
  }

  private getProjectIdAndBoundEnvironmentVariables(
    preview: SquashAutomProjectPreviewComponent,
  ): Observable<{ environmentVariables: BoundEnvironmentVariable[]; projectId: number }> {
    return preview.getBoundEnvironmentVariables().pipe(
      take(1),
      map((boundEnvironmentVariables) => ({
        environmentVariables: boundEnvironmentVariables,
        projectId: preview.squashAutomProject.projectId,
      })),
    );
  }

  defineRedirectionUrl(): string {
    return '/administration-workspace/system/settings';
  }

  shouldShowCallbackUrlBanner(): boolean {
    return this.isVisible;
  }
}

enum BodyLayout {
  LOADING,
  ERROR,
  READY,
}

enum FooterLayout {
  CONFIRM_OR_CANCEL,
  CLOSE_SIMPLE,
  CLOSE_WARNING,
}
