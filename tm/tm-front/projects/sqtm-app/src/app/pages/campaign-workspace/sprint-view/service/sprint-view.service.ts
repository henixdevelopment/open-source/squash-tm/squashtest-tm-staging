import { ElementRef, Injectable, OnDestroy } from '@angular/core';
import {
  ActionErrorDisplayService,
  AssignedUserDelegate,
  AttachmentService,
  BindReqVersionToSprintOperationReport,
  CampaignPermissions,
  ClosedSprintLockService,
  CustomFieldValueService,
  DateFormatUtils,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  ExecutionStatusKeys,
  GridColumnId,
  GridService,
  Identifier,
  InfoList,
  InfoListItem,
  ProjectData,
  ReferentialDataService,
  RestService,
  SimpleUser,
  SprintModel,
  SprintReqVersionModel,
  SprintService,
  SprintStatus,
  SprintStatusKeys,
  SynchronizationPluginId,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { sprintReqVersionsSelector, SprintState } from '../state/sprint.state';
import { provideInitialSprintView, SprintViewState } from '../state/sprint-view.state';
import { combineLatest, Observable, Subject } from 'rxjs';
import {
  catchError,
  concatMap,
  finalize,
  map,
  take,
  takeUntil,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import { select } from '@ngrx/store';
import { GenericTestPlanExecutionManager } from '../../../../components/test-plan/generic-test-plan-execution-manager';
import { TestPlanOwnerView } from '../../test-plan-owner-view';

@Injectable()
export class SprintViewService
  extends EntityViewService<SprintState, 'sprint', CampaignPermissions>
  implements AssignedUserDelegate, GenericTestPlanExecutionManager, OnDestroy, TestPlanOwnerView
{
  private dropZoneComponent: ElementRef;
  private unsub$ = new Subject<void>();

  readonly canWriteAndSprintNotClosed$ = this.componentData$.pipe(
    map(
      (componentData) =>
        componentData.permissions.canWrite &&
        componentData.sprint.status !== SprintStatus.CLOSED.id,
    ),
  );

  readonly canUpdateExecutionStatus$: Observable<boolean> = this.canWriteAndSprintNotClosed$;
  readonly canDeleteExecutions$: Observable<boolean> = this.canWriteAndSprintNotClosed$;
  readonly canAssign$: Observable<boolean> = this.canWriteAndSprintNotClosed$;

  readonly assignableUsers$: Observable<SimpleUser[]> = this.componentData$.pipe(
    map((componentData) => componentData.sprint.assignableUsers),
  );

  readonly canExecute$: Observable<boolean> = this.componentData$.pipe(
    map(
      (componentData) =>
        componentData.permissions.canExecute &&
        componentData.sprint.status !== SprintStatus.CLOSED.id,
    ),
  );

  assignableEntityIdColumnId: GridColumnId = GridColumnId.testPlanItemId;

  constructor(
    protected restService: RestService,
    protected referentialDataService: ReferentialDataService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected customFieldValueService: CustomFieldValueService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
    protected customFieldHelper: EntityViewCustomFieldHelperService,
    protected sprintService: SprintService,
    protected reqVersionGrid: GridService,
    private readonly actionErrorDisplayService: ActionErrorDisplayService,
    protected closedSprintLockService: ClosedSprintLockService,
  ) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper,
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  setDropZoneComponent(component: ElementRef): void {
    this.dropZoneComponent = component;
  }

  private unmarkAsDropZone(): void {
    if (this.dropZoneComponent) {
      this.dropZoneComponent.nativeElement.classList.remove(
        'sqtm-core-border-current-workspace-color',
      );
    }
  }

  addSimplePermissions(projectData: ProjectData): CampaignPermissions {
    return new CampaignPermissions(projectData);
  }

  getInitialState(): SprintViewState {
    return provideInitialSprintView();
  }

  load(id: number) {
    this.restService
      .getWithoutErrorHandling<SprintModel>(['sprint-view', id.toString()])
      .subscribe({
        next: (sprintModel: SprintModel) => {
          const sprint = this.initializeSprintState(sprintModel);
          this.initializeEntityState(sprint);
          this.initializeSprintReqVersionGrid(sprintModel);
          this.closedSprintLockService.setSprintClosed(
            sprintModel.status === SprintStatus.CLOSED.id,
          );
        },
        error: (err) => {
          this.notifyEntityNotFound(err);
        },
      });
  }

  private initializeSprintReqVersionGrid(sprintModel: SprintModel) {
    const sprintReqVersion$: Observable<SprintReqVersionModel[]> = this.componentData$.pipe(
      select(sprintReqVersionsSelector),
    );
    const infoListItems: Observable<InfoListItem[]> = this.buildInfoListItems();
    const table$: Observable<SprintReqVersionModel[]> = combineLatest([
      sprintReqVersion$,
      infoListItems,
    ]).pipe(
      takeUntil(this.unsub$),
      map(([sprintReqVersionModels, listPanelItems]: [SprintReqVersionModel[], InfoListItem[]]) => {
        return this.buildSprintReqVersionRows(sprintReqVersionModels, listPanelItems, sprintModel);
      }),
    );
    this.reqVersionGrid.connectToDatasource(table$, 'id');
  }

  private buildSprintReqVersionRows(
    sprintReqVersionModels: SprintReqVersionModel[],
    infoListItems: InfoListItem[],
    sprintModel: SprintModel,
  ): SprintReqVersionModel[] {
    return sprintReqVersionModels.map((sprintReqVersionModel: SprintReqVersionModel) => {
      const reference: string =
        sprintModel.synchronisationKind === SynchronizationPluginId.XSQUASH4GITLAB
          ? this.extractGitlabProjectAndIidFromUrl(sprintReqVersionModel.remoteReqUrl)
          : sprintReqVersionModel.reference;
      const categoryLabel = this.getDisplayedCategoryLabel(sprintReqVersionModel, infoListItems);
      return {
        ...sprintReqVersionModel,
        reference: reference,
        categoryLabel: categoryLabel,
      };
    });
  }

  private extractGitlabProjectAndIidFromUrl(url: string) {
    if (!url) {
      return '';
    }
    const splittedUrl: string[] = url.split('/');
    return splittedUrl[splittedUrl.length - 4] + '#' + splittedUrl[splittedUrl.length - 1];
  }

  private getDisplayedCategoryLabel(
    sprintReqVersionModel: SprintReqVersionModel,
    infoListItems: InfoListItem[],
  ) {
    const { categoryId, categoryLabel } = sprintReqVersionModel;
    const isSystem: boolean = infoListItems.find(
      (item: InfoListItem) =>
        item.system && (item.id === categoryId || item.label === categoryLabel),
    )?.system;
    const label: string = infoListItems.find(
      (item: InfoListItem) => item.id === categoryId || item.label === categoryLabel,
    )?.label;

    if (isSystem) {
      return this.translateService.instant(`sqtm-core.entity.${label}`);
    } else {
      return label ?? categoryLabel;
    }
  }

  private buildInfoListItems(): Observable<InfoListItem[]> {
    return this.referentialDataService.infoLists$.pipe(
      take(1),
      map((infoLists: InfoList[]) =>
        infoLists.reduce<InfoListItem[]>(
          (acc: InfoListItem[], infoList: InfoList) => [...acc, ...infoList.items],
          [],
        ),
      ),
    );
  }

  private initializeSprintState(sprintModel: SprintModel): SprintState {
    const attachmentEntityState = this.initializeAttachmentState(
      sprintModel.attachmentList.attachments,
    );
    const customFieldValueState = this.initializeCustomFieldValueState(
      sprintModel.customFieldValues,
    );
    return {
      ...sprintModel,
      attachmentList: {
        id: sprintModel.attachmentList.id,
        attachments: attachmentEntityState,
      },
      customFieldValues: customFieldValueState,
      uiState: {
        openRequirementTreePicker: false,
      },
      reference: sprintModel.reference,
      sprintReqVersions: sprintModel.sprintReqVersions,
      createdOn: DateFormatUtils.createDateFromIsoString(sprintModel.createdOn),
      createdBy: sprintModel.createdBy,
      lastModifiedOn: DateFormatUtils.createDateFromIsoString(sprintModel.lastModifiedOn),
      lastModifiedBy: sprintModel.lastModifiedBy,
      startDate: sprintModel.startDate,
      endDate: sprintModel.endDate,
      description: sprintModel.description,
      nbSprintReqVersions: sprintModel.sprintReqVersions.length,
      status: sprintModel.status,
      remoteState: sprintModel.remoteState,
      synchronisationKind: sprintModel.synchronisationKind,
      nbTestPlanItems: sprintModel.nbTestPlanItems,
    };
  }

  updateExecutionStatus(
    executionStatus: ExecutionStatusKeys,
    testPlanItemId: number,
  ): Observable<unknown> {
    return this.restService.post(['test-plan-item', testPlanItemId.toString(), 'status'], {
      executionStatus,
    });
  }

  toggleRequirementTreePicker() {
    this.commit(this.doToggleRequirementTreePicker(this.getSnapshot()));
  }

  private doToggleRequirementTreePicker(state: SprintViewState) {
    const pickerState = state.sprint.uiState.openRequirementTreePicker;
    return {
      ...state,
      sprint: {
        ...state.sprint,
        uiState: {
          ...state.sprint.uiState,
          openRequirementTreePicker: !pickerState,
        },
      },
    };
  }

  closeRequirementPicker() {
    this.commit(this.doCloseRequirementPicker(this.getSnapshot()));
  }

  private doCloseRequirementPicker(state: SprintViewState) {
    return {
      ...state,
      sprint: {
        ...state.sprint,
        uiState: {
          ...state.sprint.uiState,
          openRequirementTreePicker: false,
        },
      },
    };
  }

  addRequirementsIntoSprint(
    requirementIds: number[],
  ): Observable<BindReqVersionToSprintOperationReport> {
    this.reqVersionGrid.beginAsyncOperation();

    return this.sprintService
      .addRequirementsByRlnIds(this.getSnapshot().sprint.id, requirementIds)
      .pipe(
        map(
          (
            operationReport: BindReqVersionToSprintOperationReport,
          ): [BindReqVersionToSprintOperationReport, SprintViewState] => {
            const state = this.getSnapshot();
            return [
              operationReport,
              {
                ...state,
                sprint: {
                  ...state.sprint,
                  sprintReqVersions: [
                    ...state.sprint.sprintReqVersions,
                    ...operationReport.linkedReqVersions,
                  ],
                },
              },
            ];
          },
        ),
        tap(() => this.reqVersionGrid.completeAsyncOperation()),
        map(
          ([operationReport, state]: [BindReqVersionToSprintOperationReport, SprintViewState]) => {
            this.commit(state);
            return operationReport;
          },
        ),
        tap((operationReport: BindReqVersionToSprintOperationReport) => {
          const sprint = this.getSnapshot().sprint;
          this.requireTreeRefreshAfterSprintReqVersionOperation(
            sprint,
            sprint.nbSprintReqVersions + operationReport.linkedReqVersions.length,
          );
        }),
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => {
          this.reqVersionGrid.completeAsyncOperation();
          this.unmarkAsDropZone();
        }),
      );
  }

  private requireTreeRefreshAfterSprintReqVersionOperation(
    sprint: SprintState,
    newNbSprintReqVersions: number,
  ) {
    sprint.nbSprintReqVersions = newNbSprintReqVersions;
    if (sprint.sprintReqVersions.length === 0) {
      this.requireExternalUpdate(sprint.id);
    }
  }

  deleteSprintReqVersions() {
    this.reqVersionGrid.beginAsyncOperation();

    this.reqVersionGrid.selectedRowIds$
      .pipe(
        take(1),
        concatMap((rowIds: Identifier[]) => {
          return this.sprintService.deleteSprintReqVersions(
            this.getSnapshot().sprint.id,
            rowIds as number[],
          );
        }),
        map((operationReport: BindReqVersionToSprintOperationReport) => {
          const state = this.getSnapshot();
          return {
            ...state,
            sprint: {
              ...state.sprint,
              sprintReqVersions: operationReport.linkedReqVersions,
            },
          };
        }),
        tap((state) => {
          this.requireTreeRefreshAfterSprintReqVersionOperation(
            state.sprint,
            state.sprint.sprintReqVersions.length,
          );
        }),
        tap(() => this.reqVersionGrid.completeAsyncOperation()),
        catchError((error) => {
          this.reqVersionGrid.completeAsyncOperation();
          return this.actionErrorDisplayService.handleActionError(error);
        }),
      )
      .subscribe((state) => this.commit(state));
  }

  deleteSprintReqVersion(id: number) {
    this.reqVersionGrid.beginAsyncOperation();

    this.sprintService
      .deleteSprintReqVersions(this.getSnapshot().sprint.id, [id])
      .pipe(
        map((operationReport: BindReqVersionToSprintOperationReport) => {
          const state = this.getSnapshot();
          return {
            ...state,
            sprint: {
              ...state.sprint,
              sprintReqVersions: operationReport.linkedReqVersions,
            },
          };
        }),
        tap((state) => {
          this.requireTreeRefreshAfterSprintReqVersionOperation(
            state.sprint,
            state.sprint.sprintReqVersions.length,
          );
        }),
        tap(() => this.reqVersionGrid.completeAsyncOperation()),
        catchError((error) => {
          this.reqVersionGrid.completeAsyncOperation();
          return this.actionErrorDisplayService.handleActionError(error);
        }),
      )
      .subscribe((state) => this.commit(state));
  }

  refreshSprintReqVersions(sprintId: number) {
    this.reqVersionGrid.beginAsyncOperation();
    this.sprintService
      .fetchSprintReqVersions(sprintId)
      .pipe(
        take(1),
        withLatestFrom(this.store.state$),
        tap(([newSprintReqVersions, state]: [SprintReqVersionModel[], SprintViewState]) => {
          const updateState: SprintViewState = {
            ...state,
            sprint: {
              ...state.sprint,
              sprintReqVersions: newSprintReqVersions,
            },
          };
          this.store.commit(updateState);
          this.reqVersionGrid.completeAsyncOperation();
        }),
      )
      .subscribe();
  }

  changeSprintStatus(sprintId: number, newStatus: SprintStatusKeys): Observable<any> {
    return this.sprintService.changeSprintStatus(sprintId, newStatus).pipe(
      take(1),
      withLatestFrom(this.store.state$),
      tap(([, state]: [any, SprintViewState]) => {
        this.requireExternalUpdate(state.sprint.id, 'status');
      }),
      concatMap(([, state]: [any, SprintViewState]) =>
        this.updateLastModificationIfNecessary(state),
      ),
      map((state: SprintViewState) => this.updateStatusInState(newStatus, state)),
    );
  }

  updateAssignedUser(testPlanItemId: number, userId: number) {
    return this.restService.post(
      ['test-plan-item', testPlanItemId.toString(), 'assign-user-to-sprint-req-version'],
      {
        assignee: userId,
      },
    );
  }

  updateStartDate(sprintId: number, startDate: Date) {
    return this.restService.post(['sprint', sprintId.toString(), 'start-date'], {
      startDate: startDate,
    });
  }

  updateEndDate(sprintId: number, endDate: Date) {
    return this.restService.post(['sprint', sprintId.toString(), 'end-date'], {
      endDate: endDate,
    });
  }

  updateDescription(sprintId: number, description: string) {
    return this.restService.post(['sprint', sprintId.toString(), 'description'], {
      description: description,
    });
  }

  private updateStatusInState(newStatus: SprintStatusKeys, state: SprintViewState) {
    const updateState: SprintViewState = {
      ...state,
      sprint: {
        ...state.sprint,
        status: newStatus,
      },
    };
    this.store.commit(updateState);
    const isSprintClosed: boolean = newStatus === SprintStatus.CLOSED.id;
    this.closedSprintLockService.setSprintClosed(isSprintClosed);
  }
}
