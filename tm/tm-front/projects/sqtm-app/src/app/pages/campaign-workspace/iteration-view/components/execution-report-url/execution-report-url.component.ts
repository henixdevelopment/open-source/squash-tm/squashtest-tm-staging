import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnDestroy,
  OnInit,
  ViewContainerRef,
} from '@angular/core';
import {
  AttachmentListModel,
  createStore,
  RestService,
  AttachmentService,
  AttachmentModel,
  DialogService,
  FileViewerComponent,
} from 'sqtm-core';
import { map, take } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-execution-report-url',
  templateUrl: './execution-report-url.component.html',
  styleUrls: ['./execution-report-url.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExecutionReportUrlComponent implements OnInit, OnDestroy {
  @Input()
  executionId: number;

  private store = createStore<ExecutionReportUrlsComponentData>({
    attachmentList: null,
    uiState: { loading: true },
  });

  componentData$ = this.store.state$;

  constructor(
    private readonly restService: RestService,
    private dialogService: DialogService,
    private vcr: ViewContainerRef,
    private attachmentService: AttachmentService,
  ) {}

  ngOnInit() {
    this.restService
      .get<AttachmentListModel>(['execution', this.executionId.toString(), 'attachments'])
      .subscribe((attachmentList) => {
        this.store.commit({
          attachmentList,
          uiState: { loading: false },
        });
      });
  }

  getAttachmentDownloadURL(attachmentListId: number, attachment: any) {
    return this.attachmentService.getAttachmentDownloadURL(attachmentListId, attachment);
  }

  ngOnDestroy(): void {
    this.store.complete();
  }

  openFilePreview(event: MouseEvent, attachment: AttachmentModel) {
    event.preventDefault();

    this.componentData$
      .pipe(
        take(1),
        map((reportData) => this.openPreviewDialog(reportData.attachmentList, attachment)),
      )
      .subscribe();
  }

  private openPreviewDialog(attachmentList: AttachmentListModel, attachment: AttachmentModel) {
    const attachmentIndex: number = attachmentList.attachments.indexOf(attachment);
    return this.dialogService.openDialog({
      id: 'execution-report-file-viewer',
      component: FileViewerComponent,
      viewContainerReference: this.vcr,
      data: {
        attachmentList: attachmentList,
        index: attachmentIndex,
      },
      minHeight: '100%',
      minWidth: '100%',
    });
  }
}

interface ExecutionReportUrlsComponentData {
  attachmentList: AttachmentListModel;
  uiState: {
    loading: boolean;
  };
}
