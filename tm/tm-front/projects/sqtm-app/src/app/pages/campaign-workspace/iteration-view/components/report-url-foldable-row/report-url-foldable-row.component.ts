import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  NgZone,
  OnDestroy,
  Output,
  Renderer2,
  signal,
  ViewChild,
  WritableSignal,
} from '@angular/core';
import {
  ColumnDisplay,
  GridDisplay,
  GridNode,
  GridService,
  GridViewportService,
  Identifier,
  ReferentialDataService,
  RowRenderer,
  ViewportDisplay,
} from 'sqtm-core';
import { Subject } from 'rxjs';
import { switchMap, take, takeUntil, tap } from 'rxjs/operators';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sqtm-app-report-url-foldable-row',
  templateUrl: './report-url-foldable-row.component.html',
  styleUrls: ['./report-url-foldable-row.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReportUrlFoldableRowComponent implements OnDestroy, RowRenderer, AfterViewInit {
  @Input()
  gridDisplay: GridDisplay;

  @Input()
  gridNode: GridNode;

  @Input()
  viewport: ViewportDisplay;

  @Output()
  refreshExecutionIssue = new EventEmitter<void>();

  @ViewChild('details', { read: ElementRef })
  details: ElementRef;

  $isPremium = toSignal(this.referentialDataService.isPremiumPluginInstalled$);
  $openedRowIds: WritableSignal<Identifier[]> = signal([]);

  private unsub$ = new Subject<void>();

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    private gridViewportService: GridViewportService,
    private ngZone: NgZone,
    private renderer: Renderer2,
    private referentialDataService: ReferentialDataService,
  ) {}

  ngAfterViewInit(): void {
    this.ngZone.runOutsideAngular(() => {
      this.gridViewportService.renderedGridViewport$
        .pipe(takeUntil(this.unsub$))
        .subscribe((renderedGridViewport) => {
          if (this.details) {
            this.renderer.setStyle(
              this.details.nativeElement,
              'width',
              `${renderedGridViewport.mainViewport.totalWidth}px`,
            );
          }
        });
    });
  }

  trackByFn(index: number, columnDisplay: ColumnDisplay) {
    return columnDisplay.id;
  }

  shouldDrawCell(columnDisplay: ColumnDisplay) {
    return columnDisplay.show;
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  refreshPage(): void {
    this.grid.openedRowIds$
      .pipe(
        take(1),
        tap((ids) => this.$openedRowIds.set(ids)),
        switchMap(() => this.grid.refreshDataAsync()),
        tap(() => this.grid.openExternalRows(this.$openedRowIds())),
      )
      .subscribe();
  }
}
