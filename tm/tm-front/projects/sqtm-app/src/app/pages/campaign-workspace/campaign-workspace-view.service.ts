import { effect, Injectable, signal, WritableSignal } from '@angular/core';
import { LocalPersistenceService } from 'sqtm-core';
import { distinctUntilChanged, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class CampaignWorkspaceViewService {
  private readonly lastExecutionScopeKey: string = 'limitLastExecutionScope';

  $lastExecutionScope: WritableSignal<boolean> = signal<boolean>(true);

  constructor(private localPersistenceService: LocalPersistenceService) {
    this.initializeLastExecutionScope();

    effect(() => {
      this.localPersistenceService
        .set(this.lastExecutionScopeKey, this.$lastExecutionScope())
        .pipe(distinctUntilChanged())
        .subscribe();
    });
  }

  getlastExecutionScope(): boolean {
    return this.$lastExecutionScope();
  }

  setLastExecutionScope(value: boolean) {
    this.$lastExecutionScope.set(value);
  }

  private initializeLastExecutionScope(): void {
    this.localPersistenceService
      .get(this.lastExecutionScopeKey)
      .pipe(take(1))
      .subscribe((storedValue: boolean | null) => {
        if (storedValue != null) {
          this.$lastExecutionScope.set(storedValue);
        }
      });
  }
}
