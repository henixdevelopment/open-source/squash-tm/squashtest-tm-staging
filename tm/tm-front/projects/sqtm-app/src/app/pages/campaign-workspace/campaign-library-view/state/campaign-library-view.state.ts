import { EntityViewState, provideInitialViewState } from 'sqtm-core';
import { CampaignLibraryState } from './campaign-library.state';

export interface CampaignLibraryViewState
  extends EntityViewState<CampaignLibraryState, 'campaignLibrary'> {
  campaignLibrary: CampaignLibraryState;
}

export function provideInitialCampaignLibraryView(): Readonly<CampaignLibraryViewState> {
  return provideInitialViewState<CampaignLibraryState, 'campaignLibrary'>('campaignLibrary');
}
