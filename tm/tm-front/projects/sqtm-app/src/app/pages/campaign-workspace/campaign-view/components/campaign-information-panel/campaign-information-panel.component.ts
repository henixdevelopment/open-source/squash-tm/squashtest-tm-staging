import { ChangeDetectionStrategy, Component, Input, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { CampaignViewService } from '../../service/campaign-view.service';
import { TranslateService } from '@ngx-translate/core';
import {
  ActionErrorDisplayService,
  CampaignStatusLevelEnumItem,
  CustomFieldData,
  EditableSelectLevelEnumFieldComponent,
  Milestone,
  MilestoneStatus,
  ReferentialDataService,
  TestPlanStatus,
} from 'sqtm-core';
import { CampaignViewComponentData } from '../../container/campaign-view/campaign-view.component';
import { catchError, finalize, map, take } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-campaign-information-panel',
  templateUrl: './campaign-information-panel.component.html',
  styleUrls: ['./campaign-information-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CampaignInformationPanelComponent implements OnInit {
  isMilestoneFeatureEnabled$: Observable<boolean>;

  @Input()
  milestoneModeTest: boolean;

  milestoneFeatureEnabled$: Observable<boolean>;

  @Input()
  campaignViewComponentData: CampaignViewComponentData;

  @Input()
  customFieldData: CustomFieldData[];

  @ViewChild('statusField')
  statusField: EditableSelectLevelEnumFieldComponent;

  constructor(
    public campaignViewService: CampaignViewService,
    public translateService: TranslateService,
    private referentialDataService: ReferentialDataService,
    private actionErrorDisplayService: ActionErrorDisplayService,
  ) {}

  ngOnInit(): void {
    this.milestoneFeatureEnabled$ = this.referentialDataService.globalConfiguration$.pipe(
      take(1),
      map((configuration) => configuration.milestoneFeatureEnabled),
    );
  }

  trackCfd(cfd: CustomFieldData) {
    return cfd.id;
  }

  updateStatus(status: CampaignStatusLevelEnumItem<any>) {
    this.campaignViewService
      .updateCampaignStatus(status.id)
      .pipe(
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => {
          this.statusField.endAsync();
          this.statusField.disableEditMode();
        }),
      )
      .subscribe();
  }

  deleteMilestone(id: number, milestoneId: number) {
    this.campaignViewService.unbindMilestone(id, milestoneId);
  }

  bindMilestone(id: number, milestone: Milestone) {
    this.campaignViewService.bindMilestone(id, milestone);
  }

  allowMilestoneBinding(componentData: CampaignViewComponentData): boolean {
    return componentData.campaign.milestones.length < 1 && componentData.permissions.canWrite;
  }

  getPossibleMilestones(componentData: CampaignViewComponentData): Milestone[] {
    const projectMilestoneViews = componentData.projectData.milestones;

    return projectMilestoneViews.filter(
      (milestone) =>
        milestone.status === MilestoneStatus.IN_PROGRESS.id ||
        milestone.status === MilestoneStatus.FINISHED.id,
    );
  }

  getProgressStatus(statusKey: any) {
    const testPlanStatus = TestPlanStatus[statusKey];
    return testPlanStatus.i18nKey;
  }

  canLinkMilestone() {
    return (
      this.campaignViewComponentData.permissions.canWrite &&
      this.campaignViewComponentData.milestonesAllowModification
    );
  }
}
