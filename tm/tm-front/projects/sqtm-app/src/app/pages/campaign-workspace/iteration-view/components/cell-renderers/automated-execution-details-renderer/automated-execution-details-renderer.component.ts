import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewContainerRef,
} from '@angular/core';
import { AutomatedSuiteExecutionsDialogComponent } from '../../automated-suite-executions-dialog/automated-suite-executions-dialog.component';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  DialogService,
  GridColumnId,
  GridService,
  ReferentialDataService,
} from 'sqtm-core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-automated-execution-details-renderer',
  templateUrl: './automated-execution-details-renderer.component.html',
  styleUrls: ['./automated-execution-details-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AutomatedExecutionDetailsRendererComponent
  extends AbstractCellRendererComponent
  implements OnInit
{
  unsub$ = new Subject<void>();
  hasBugTracker = false;

  constructor(
    gridService: GridService,
    cdRef: ChangeDetectorRef,
    private dialogService: DialogService,
    private vcr: ViewContainerRef,
    private referentialDataService: ReferentialDataService,
  ) {
    super(gridService, cdRef);
  }

  ngOnInit() {
    this.referentialDataService.projects$.pipe(takeUntil(this.unsub$)).subscribe((projects) => {
      const projectId = this.row.data.projectId;
      const project = projects.find((p) => p.id === projectId);
      this.hasBugTracker = !!(project && project.bugTrackerBinding);
    });
  }

  get hasExecution(): boolean {
    return this.row.data.hasExecution;
  }

  showExecutionHistory() {
    this.dialogService.openDialog({
      id: 'automated-suite-executions',
      component: AutomatedSuiteExecutionsDialogComponent,
      viewContainerReference: this.vcr,
      data: { suiteId: this.row.data.suiteId, hasBugTracker: this.hasBugTracker },
      height: 600,
      width: 1200,
    });
  }
}

export function automatedExecutionDetailsColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withHeaderPosition('center')
    .withRenderer(AutomatedExecutionDetailsRendererComponent);
}
