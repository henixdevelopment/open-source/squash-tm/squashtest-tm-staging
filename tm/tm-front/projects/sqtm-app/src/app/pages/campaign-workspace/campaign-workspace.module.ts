import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzProgressModule } from 'ng-zorro-antd/progress';
import { CampaignWorkspaceComponent } from './campaign-workspace/containers/campaign-workspace/campaign-workspace.component';
import { RouterModule, Routes } from '@angular/router';
import {
  AnchorModule,
  AttachmentModule,
  CellRendererCommonModule,
  CustomFieldModule,
  DialogModule,
  ExperimentalFeatureDirective,
  GridExportModule,
  GridModule,
  IssuesModule,
  NavBarModule,
  SqtmDragAndDropModule,
  SvgModule,
  UiManagerModule,
  WorkspaceCommonModule,
  WorkspaceLayoutModule,
} from 'sqtm-core';
import { CampaignWorkspaceTreeComponent } from './campaign-workspace/containers/campaign-workspace-tree/campaign-workspace-tree.component';
import { CampaignLibraryViewComponent } from './campaign-library-view/container/campaign-library-view/campaign-library-view.component';
import { CampaignLibraryViewContentComponent } from './campaign-library-view/container/campaign-library-view-content/campaign-library-view-content.component';
import { CampaignFolderViewContentComponent } from './campaign-folder-view/container/campaign-folder-view-content/campaign-folder-view-content.component';
import { CampaignFolderViewComponent } from './campaign-folder-view/container/campaign-folder-view/campaign-folder-view.component';
import { IterationViewComponent } from './iteration-view/container/iteration-view/iteration-view.component';
import { IterationViewContentComponent } from './iteration-view/container/iteration-view-content/iteration-view-content.component';
import { IterationTestPlanExecutionComponent } from './iteration-view/components/iteration-test-plan-execution/iteration-test-plan-execution.component';
import { CampaignViewComponent } from './campaign-view/container/campaign-view/campaign-view.component';
import { CampaignViewContentComponent } from './campaign-view/container/campaign-view-content/campaign-view-content.component';
import { TranslateModule } from '@ngx-translate/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CKEditorModule } from 'ckeditor4-angular';
import { CampaignInformationPanelComponent } from './campaign-view/components/campaign-information-panel/campaign-information-panel.component';
import { StartExecutionComponent } from './iteration-view/components/cell-renderers/start-execution/start-execution.component';
import { SuccessRateComponent } from './iteration-view/components/cell-renderers/success-rate/success-rate.component';
import { CampaignStatisticsPanelComponent } from './campaign-statistics-panel/campaign-statistics-panel.component';
import { CampaignViewPlanningPanelComponent } from './campaign-view/components/campaign-view-planning-panel/campaign-view-planning-panel.component';
import { IterationPlanningDialogComponent } from './campaign-view/components/dialog/iteration-planning-dialog/iteration-planning-dialog.component';
import { CampaignViewDashboardComponent } from './campaign-view/container/campaign-view-dashboard/campaign-view-dashboard.component';
import { IterationInformationPanelComponent } from './iteration-view/components/iteration-information-panel/iteration-information-panel.component';
import { IterationViewPlanningPanelComponent } from './iteration-view/components/iteration-view-planning-panel/iteration-view-planning-panel.component';
import { TestSuiteViewComponent } from './test-suite-view/containers/test-suite-view/test-suite-view.component';
import { TestSuiteViewContentComponent } from './test-suite-view/containers/test-suite-view-content/test-suite-view-content.component';
import { TestSuiteInformationPanelComponent } from './test-suite-view/components/test-suite-information-panel/test-suite-information-panel.component';
import { IterationViewDashboardComponent } from './iteration-view/container/iteration-view-dashboard/iteration-view-dashboard.component';
import { CampaignIssuesComponent } from './campaign-view/components/campaign-issues/campaign-issues.component';
import { IterationIssuesComponent } from './iteration-view/components/iteration-issues/iteration-issues.component';
import { TestSuiteIssuesComponent } from './test-suite-view/components/test-suite-issues/test-suite-issues.component';
import { CampaignFolderIssuesComponent } from './campaign-folder-view/components/campaign-folder-issues/campaign-folder-issues.component';
import { ExecutionHistoryComponent } from './iteration-view/components/execution-history/execution-history.component';
import { DeleteExecutionHistoryComponent } from './iteration-view/components/cell-renderers/delete-execution-history/delete-execution-history.component';
import { DatasetCellRendererComponent } from './campaign-workspace/components/test-plan/dataset-cell-renderer/dataset-cell-renderer.component';
import {
  LinkableDateCellRendererComponent,
  LinkableDateTimeCellRendererComponent,
} from './iteration-view/components/cell-renderers/linkable-date-cell-renderer/linkable-date-cell-renderer.component';
import { ItpiMultiEditDialogComponent } from './iteration-view/components/itpi-multi-edit-dialog/itpi-multi-edit-dialog.component';
import { LastExecutionDateCellComponent } from './iteration-view/components/cell-renderers/last-execution-date-cell/last-execution-date-cell.component';
import { OverlayModule } from '@angular/cdk/overlay';
import { TestSuiteTestPlanExecutionComponent } from './test-suite-view/components/test-suite-test-plan-execution/test-suite-test-plan-execution.component';
import { CampaignTestPlanExecutionComponent } from './campaign-view/components/campaign-test-plan-execution/campaign-test-plan-execution.component';
import { DeleteTestSuiteTestPlanItemDialogComponent } from './test-suite-view/components/delete-test-suite-test-plan-item-dialog/delete-test-suite-test-plan-item-dialog.component';
import { DeleteTestSuiteTestPlanItemComponent } from './test-suite-view/components/cell-renderers/delete-test-suite-test-plan-item/delete-test-suite-test-plan-item.component';
import { DeleteCampaignTestPlanItemComponent } from './campaign-view/components/cell-renderers/delete-campaign-test-plan-item/delete-campaign-test-plan-item.component';
import { TestSuiteExecutionStatusesCellComponent } from './test-suite-view/components/cell-renderers/test-suite-execution-status-cell/test-suite-execution-statuses-cell.component';
import { TestSuiteTpiMultiEditDialogComponent } from './test-suite-view/components/test-suite-tpi-multi-edit-dialog/test-suite-tpi-multi-edit-dialog.component';

import { CampaignStatisticsModule } from '../../components/statistics/campaign-statistics/campaign-statistics.module';
import { CtpiMultiEditDialogComponent } from './campaign-view/components/dialog/ctpi-multi-edit-dialog/ctpi-multi-edit-dialog.component';
import { AutomatedSuiteExecutionsDialogComponent } from './iteration-view/components/automated-suite-executions-dialog/automated-suite-executions-dialog.component';
import { IterationAutomatedSuiteComponent } from './iteration-view/components/iteration-automated-suite/iteration-automated-suite.component';
import { AutomatedExecutionReportRendererComponent } from './iteration-view/components/cell-renderers/automated-execution-report-renderer/automated-execution-report-renderer.component';
import { AutomatedExecutionDetailsRendererComponent } from './iteration-view/components/cell-renderers/automated-execution-details-renderer/automated-execution-details-renderer.component';
import { AutomatedExecutionFlagCellRendererComponent } from './iteration-view/components/cell-renderers/automated-execution-flag-cell-renderer/automated-execution-flag-cell-renderer.component';
import { CampaignMilestoneViewComponent } from './campaign-milestone-dashboard/containers/campaign-milestone-view/campaign-milestone-view.component';
import { CampaignMilestoneViewContentComponent } from './campaign-milestone-dashboard/containers/campaign-milestone-view-content/campaign-milestone-view-content.component';
import { CustomDashboardModule } from '../../components/custom-dashboard/custom-dashboard.module';
import { NzAnchorModule } from 'ng-zorro-antd/anchor';
import { AutomatedSuiteReportUrlDialogComponent } from './iteration-view/components/automated-suite-report-url-dialog/automated-suite-report-url-dialog.component';
import { AutomatedSuiteExecDatasetComponent } from './iteration-view/components/cell-renderers/automated-suite-exec-dataset/automated-suite-exec-dataset.component';
import { TestSuiteAutomatedSuiteComponent } from './test-suite-view/components/test-suite-automated-suite/test-suite-automated-suite.component';
import { AutomatedTestsExecutionSupervisionDialogComponent } from './iteration-view/components/automated-tests-execution-supervision-dialog/automated-tests-execution-supervision-dialog.component';
import { AutomationProjectMenuComponent } from './iteration-view/components/automation-project-menu/automation-project-menu.component';
import { TfExecutionTableComponent } from './iteration-view/components/automated-tests-execution-supervision-dialog/tf-execution-table/tf-execution-table.component';
import { SquashAutomExecutionTableComponent } from './iteration-view/components/automated-tests-execution-supervision-dialog/squash-autom-execution-table/squash-autom-execution-table.component';
import { SquashAutomProjectPreviewComponent } from './iteration-view/components/squash-autom-project-preview-component/squash-autom-project-preview.component';
import { SquashAutomTestCasesTableComponent } from './iteration-view/components/automated-tests-execution-supervision-dialog/squash-autom-test-cases-table/squash-autom-test-cases-table.component';
import { ReportUrlFoldableRowComponent } from './iteration-view/components/report-url-foldable-row/report-url-foldable-row.component';
import { ExecutionReportUrlComponent } from './iteration-view/components/execution-report-url/execution-report-url.component';
import { DetailedStepViewsCommonModule } from '../../components/detailed-step-views-common/detailed-step-views-common.module';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { FilteredExecutionStatusesCellComponent } from './iteration-view/components/cell-renderers/filtered-execution-status-cell/filtered-execution-statuses-cell.component';
import { CampaignWorkspaceMultiSelectViewComponent } from './campaign-workspace-multi-select-view/containers/campaign-workspace-multi-select-view/campaign-workspace-multi-select-view.component';
import { CampaignMultiViewContentComponent } from './campaign-workspace-multi-select-view/containers/campaign-multi-view-content/campaign-multi-view-content.component';
import { SprintViewComponent } from './sprint-view/containers/sprint-view/sprint-view.component';
import { SprintViewContentComponent } from './sprint-view/containers/sprint-view-content/sprint-view-content.component';
import { AutomatedSuiteExecComparatorDialogComponent } from './iteration-view/components/automated-suite-exec-comparator-dialog/automated-suite-exec-comparator-dialog.component';
import { AutomSuiteComparisonHeaderRendererComponent } from './iteration-view/components/header-renderers/autom-suite-comparison-header-renderer/autom-suite-comparison-header-renderer.component';
import { AutomatedSuiteStatisticsCellRendererComponent } from './iteration-view/components/cell-renderers/automated-suite-statistics-cell-renderer/automated-suite-statistics-cell-renderer.component';
import { MilestoneDialogComponent } from '../../components/milestone/milestone-dialog/milestone-dialog.component';
import { MilestonesTagComponent } from '../../components/milestone/milestones-tag/milestones-tag.component';
import { AutomatedSuiteStopWorkflowsComponent } from './iteration-view/components/cell-renderers/automated-suite-stop-workflows/automated-suite-stop-workflows.component';
import { SprintReqVersionsLinkedComponent } from './sprint-req-version-view/components/sprint-req-versions-linked/sprint-req-versions-linked.component';
import { DeleteSprintReqVersionComponent } from './sprint-view/components/cell-renderers/delete-sprint-req-version/delete-sprint-req-version.component';
import { SprintInformationPanelComponent } from './sprint-view/components/sprint-information-panel/sprint-information-panel.component';
import { SprintViewSprintReqVersionComponent } from './sprint-view/containers/sprint-view-sprint-req-version/sprint-view-sprint-req-version.component';
import { DeleteAutomatedSuiteComponent } from './iteration-view/components/cell-renderers/delete-automated-suite/delete-automated-suite.component';
import { AutomatedSuiteAttachmentPruneComponent } from './iteration-view/components/cell-renderers/automated-suite-attachment-prune/automated-suite-attachment-prune.component';
import { SprintReqVersionTestingComponent } from './sprint-view/components/cell-renderers/sprint-req-version-testing/sprint-req-version-testing.component';
import { SprintReqVersionViewSubPageComponent } from './sprint-req-version-view/containers/sprint-req-version-view-sub-page/sprint-req-version-view-sub-page.component';
import { SprintReqVersionLevelTwoComponent } from './sprint-req-version-view/containers/sprint-req-version-level-two/sprint-req-version-level-two.component';
import { SprintReqVersionViewContentComponent } from './sprint-req-version-view/containers/sprint-req-version-view-content/sprint-req-version-view-content.component';
import { SprintReqVersionInformationPanelComponent } from './sprint-req-version-view/components/sprint-req-version-information-panel/sprint-req-version-information-panel.component';
import { SprintReqVersionTestPlanPanelComponent } from './sprint-req-version-view/components/sprint-req-version-test-plan-panel/sprint-req-version-test-plan-panel.component';
import { RemotePerimeterStatusCellComponent } from './sprint-view/components/cell-renderers/remote-perimeter-status-cell/remote-perimeter-status-cell.component';
import { SprintGroupViewComponent } from './sprint-group-view/containers/sprint-group-view/sprint-group-view.component';
import { SprintGroupViewContentComponent } from './sprint-group-view/containers/sprint-group-view-content/sprint-group-view-content.component';
import { SprintGroupInformationPanelComponent } from './sprint-group-view/components/sprint-group-information-panel/sprint-group-information-panel.component';
import { SprintReqVersionIssuePanelComponent } from './sprint-req-version-view/components/sprint-req-version-issue-panel/sprint-req-version-issue-panel.component';
import { SprintViewIssuesComponent } from './sprint-view/containers/sprint-view-issues/sprint-view-issues.component';
import { SprintIssuesPanelComponent } from './sprint-view/components/sprint-issues-panel/sprint-issues-panel.component';
import { TestPlanDraggedContentComponent } from '../../components/test-plan/test-plan-dragged-content/test-plan-dragged-content.component';
import { SprintReqVersionUpdateTestPlanDialogComponent } from './sprint-req-version-view/components/sprint-req-version-update-test-plan-dialog/sprint-req-version-update-test-plan-dialog.component';
import { SprintViewOverallExecPlanComponent } from './sprint-view/containers/sprint-view-overall-exec-plan/sprint-view-overall-exec-plan.component';
import { AutomatedSuiteWorkflowViewerRendererComponent } from './iteration-view/components/cell-renderers/automated-suite-workflow-viewer-renderer/automated-suite-workflow-viewer-renderer.component';
import { WorkflowLogsViewerComponent } from './iteration-view/components/cell-renderers/workflow-logs-viewer/workflow-logs-viewer.component';
import { SquashOrchestratorModule } from '../../components/squash-orchestrator/squash-orchestrator.module';

export const routes: Routes = [
  {
    path: '',
    component: CampaignWorkspaceComponent,
    children: [
      {
        path: 'campaign-library/:campaignLibraryId',
        component: CampaignLibraryViewComponent,
        children: [{ path: 'content', component: CampaignLibraryViewContentComponent }],
      },
      {
        path: 'campaign-folder/:campaignFolderId',
        component: CampaignFolderViewComponent,
        children: [
          { path: 'content', component: CampaignFolderViewContentComponent },
          { path: 'issues', component: CampaignFolderIssuesComponent },
        ],
      },
      {
        path: 'iteration/:iterationId',
        component: IterationViewComponent,
        children: [
          { path: 'content', component: IterationViewContentComponent },
          { path: 'test-plan', component: IterationTestPlanExecutionComponent },
          { path: 'dashboard', component: IterationViewDashboardComponent },
          { path: 'issues', component: IterationIssuesComponent },
          {
            path: 'automated-suite',
            component: IterationAutomatedSuiteComponent,
          },
        ],
      },
      {
        path: 'test-suite/:testSuiteId',
        component: TestSuiteViewComponent,
        children: [
          { path: 'content', component: TestSuiteViewContentComponent },
          { path: 'issues', component: TestSuiteIssuesComponent },
          { path: 'test-plan', component: TestSuiteTestPlanExecutionComponent },
          {
            path: 'automated-suite',
            component: TestSuiteAutomatedSuiteComponent,
          },
        ],
      },
      {
        path: 'campaign/:campaignId',
        component: CampaignViewComponent,
        children: [
          { path: 'dashboard', component: CampaignViewDashboardComponent },
          { path: 'content', component: CampaignViewContentComponent },
          { path: 'issues', component: CampaignIssuesComponent },
          { path: 'test-plan', component: CampaignTestPlanExecutionComponent },
        ],
      },
      {
        path: 'sprint/:sprintId',
        component: SprintViewComponent,
        children: [
          { path: 'content', component: SprintViewContentComponent },
          {
            path: 'sprint-req-versions',
            component: SprintViewSprintReqVersionComponent,
          },
          {
            path: 'overall-execution-plan',
            component: SprintViewOverallExecPlanComponent,
          },
          {
            path: 'issues',
            component: SprintViewIssuesComponent,
          },
        ],
      },
      {
        path: 'sprint-group/:sprintGroupId',
        component: SprintGroupViewComponent,
        children: [{ path: 'content', component: SprintGroupViewContentComponent }],
      },
      {
        path: 'campaign-workspace-multi',
        component: CampaignWorkspaceMultiSelectViewComponent,
        children: [
          { path: '', pathMatch: 'full', redirectTo: 'content' },
          { path: 'content', component: CampaignMultiViewContentComponent },
        ],
      },
      {
        path: 'milestone-dashboard',
        component: CampaignMilestoneViewComponent,
        children: [{ path: 'content', component: CampaignMilestoneViewContentComponent }],
      },
    ],
  },
  {
    path: 'sprint-req-version/:sprintReqVersionId',
    component: SprintReqVersionLevelTwoComponent,
    children: [{ path: 'content', component: SprintReqVersionViewContentComponent }],
  },
];

@NgModule({
  declarations: [
    CampaignWorkspaceComponent,
    CampaignWorkspaceTreeComponent,
    CampaignFolderViewComponent,
    CampaignFolderViewContentComponent,
    CampaignLibraryViewComponent,
    CampaignLibraryViewContentComponent,
    CampaignViewComponent,
    CampaignViewContentComponent,
    CampaignInformationPanelComponent,
    CampaignStatisticsPanelComponent,
    CampaignTestPlanExecutionComponent,
    DeleteAutomatedSuiteComponent,
    IterationViewComponent,
    IterationTestPlanExecutionComponent,
    IterationViewContentComponent,
    StartExecutionComponent,
    SuccessRateComponent,
    CampaignViewPlanningPanelComponent,
    IterationPlanningDialogComponent,
    CampaignViewDashboardComponent,
    IterationInformationPanelComponent,
    IterationViewPlanningPanelComponent,
    TestSuiteViewComponent,
    TestSuiteViewContentComponent,
    TestSuiteInformationPanelComponent,
    IterationViewDashboardComponent,
    CampaignIssuesComponent,
    IterationIssuesComponent,
    TestSuiteIssuesComponent,
    CampaignFolderIssuesComponent,
    FilteredExecutionStatusesCellComponent,
    ExecutionHistoryComponent,
    DeleteExecutionHistoryComponent,
    DeleteSprintReqVersionComponent,
    DatasetCellRendererComponent,
    LinkableDateCellRendererComponent,
    LinkableDateTimeCellRendererComponent,
    LastExecutionDateCellComponent,
    ItpiMultiEditDialogComponent,
    TestSuiteTestPlanExecutionComponent,
    DeleteTestSuiteTestPlanItemDialogComponent,
    DeleteTestSuiteTestPlanItemComponent,
    DeleteCampaignTestPlanItemComponent,
    TestSuiteExecutionStatusesCellComponent,
    TestSuiteTpiMultiEditDialogComponent,
    CtpiMultiEditDialogComponent,
    IterationAutomatedSuiteComponent,
    AutomatedExecutionReportRendererComponent,
    AutomatedExecutionDetailsRendererComponent,
    AutomatedExecutionFlagCellRendererComponent,
    AutomatedSuiteExecutionsDialogComponent,
    CampaignMilestoneViewComponent,
    CampaignMilestoneViewContentComponent,
    AutomatedSuiteReportUrlDialogComponent,
    AutomatedSuiteExecDatasetComponent,
    AutomatedSuiteStopWorkflowsComponent,
    TestSuiteAutomatedSuiteComponent,
    AutomatedTestsExecutionSupervisionDialogComponent,
    AutomationProjectMenuComponent,
    TfExecutionTableComponent,
    SquashAutomExecutionTableComponent,
    SquashAutomProjectPreviewComponent,
    SquashAutomTestCasesTableComponent,
    ReportUrlFoldableRowComponent,
    ExecutionReportUrlComponent,
    CampaignWorkspaceMultiSelectViewComponent,
    CampaignMultiViewContentComponent,
    AutomatedSuiteExecComparatorDialogComponent,
    AutomSuiteComparisonHeaderRendererComponent,
    AutomatedSuiteStatisticsCellRendererComponent,
    SprintGroupInformationPanelComponent,
    SprintGroupViewComponent,
    SprintGroupViewContentComponent,
    SprintInformationPanelComponent,
    SprintReqVersionsLinkedComponent,
    SprintViewComponent,
    SprintViewContentComponent,
    SprintViewSprintReqVersionComponent,
    AutomatedSuiteExecComparatorDialogComponent,
    AutomatedSuiteAttachmentPruneComponent,
    SprintReqVersionTestingComponent,
    SprintReqVersionViewSubPageComponent,
    SprintReqVersionLevelTwoComponent,
    SprintReqVersionViewContentComponent,
    SprintReqVersionInformationPanelComponent,
    SprintReqVersionTestPlanPanelComponent,
    RemotePerimeterStatusCellComponent,
    SprintReqVersionIssuePanelComponent,
    SprintViewIssuesComponent,
    SprintIssuesPanelComponent,
    SprintReqVersionUpdateTestPlanDialogComponent,
    AutomatedSuiteWorkflowViewerRendererComponent,
    WorkflowLogsViewerComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NavBarModule,
    SvgModule,
    WorkspaceCommonModule,
    UiManagerModule,
    WorkspaceLayoutModule,
    GridModule,
    CellRendererCommonModule,
    TranslateModule.forChild(),
    NzDropDownModule,
    ReactiveFormsModule,
    FormsModule,
    CKEditorModule,
    CustomFieldModule,
    DialogModule,
    NzIconModule,
    NzTypographyModule,
    NzInputModule,
    NzPopoverModule,
    NzToolTipModule,
    NzRadioModule,
    AnchorModule,
    NzAnchorModule,
    AttachmentModule,
    SqtmDragAndDropModule,
    NzCollapseModule,
    IssuesModule,
    MilestoneDialogComponent,
    MilestonesTagComponent,
    NzButtonModule,
    OverlayModule,
    CampaignStatisticsModule,
    CustomDashboardModule,
    NzProgressModule,
    GridExportModule,
    DetailedStepViewsCommonModule,
    NzCheckboxModule,
    ExperimentalFeatureDirective,
    TestPlanDraggedContentComponent,
    SquashOrchestratorModule,
  ],
})
export class CampaignWorkspaceModule {}
