import { APP_BASE_HREF, DatePipe } from '@angular/common';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { Router } from '@angular/router';
import { Dictionary } from '@ngrx/entity/src/models';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import {
  catchError,
  concatMap,
  filter,
  finalize,
  map,
  switchMap,
  take,
  takeUntil,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import {
  ActionErrorDisplayService,
  ASSIGNED_USER_DELEGATE,
  assignedUserColumn,
  assigneeFilter,
  AutomatedSuiteCreationSpecification,
  AutomatedSuiteService,
  BindableEntity,
  buildFilters,
  CampaignPermissions,
  centredTextColumn,
  ConfirmConfiguration,
  ConfirmDeleteConfiguration,
  convertSqtmLiterals,
  CreateEntityDialogComponent,
  CustomColumnsComponent,
  DataRow,
  dateRangeFilter,
  DialogConfiguration,
  DialogService,
  EntityReference,
  EntityRowReference,
  EntityType,
  EntityViewComponentData,
  executionModeColumn,
  ExecutionStatus,
  executionStatusFilter,
  FilterValueModel,
  Fixed,
  grid,
  GRID_PERSISTENCE_KEY,
  GridColumnId,
  GridDefinition,
  GridDndData,
  GridFilter,
  GridFilterUtils,
  GridId,
  GridService,
  gridServiceFactory,
  GridWithStatePersistence,
  i18nEnumResearchFilter,
  indexColumn,
  InterWindowCommunicationService,
  InterWindowMessages,
  isDndDataFromTestCaseTreePicker,
  IterationService,
  Limited,
  LocalPersistenceService,
  milestoneLabelColumn,
  parseDataRowId,
  ProjectDataMap,
  ReferentialDataService,
  ResearchColumnPrototype,
  RestService,
  serverBackedGridTextFilter,
  SqtmDragEnterEvent,
  SqtmDragLeaveEvent,
  SqtmDropEvent,
  SquashTmDataRowType,
  StyleDefinitionBuilder,
  TEST_CASE_TREE_PICKER_ID,
  testCaseImportanceColumn,
  TestPlanResumeModel,
  textColumn,
  UserHistorySearchProvider,
  Workspaces,
  WorkspaceWithTreeComponent,
} from 'sqtm-core';
import {
  EXECUTION_DIALOG_RUNNER_URL,
  EXECUTION_RUNNER_PROLOGUE_URL,
} from '../../../../execution/execution-runner/execution-runner.constant';
import { dataSetColumn } from '../../../campaign-workspace/components/test-plan/dataset-cell-renderer/dataset-cell-renderer.component';
import { IterationViewComponentData } from '../../container/iteration-view/iteration-view.component';
import {
  ITERATION_TEST_PLAN_DROP_ZONE_ID,
  ITV_ITPE_TABLE,
  ITV_ITPE_TABLE_CONF,
} from '../../iteration-view.constant';
import { iterationViewLogger } from '../../iteration-view.logger';
import { IterationViewService } from '../../services/iteration-view.service';
import { IterationState } from '../../state/iteration.state';
import { AutomatedTestsExecutionSupervisionDialogComponent } from '../automated-tests-execution-supervision-dialog/automated-tests-execution-supervision-dialog.component';
import { filteredExecutionStatusesColumn } from '../cell-renderers/filtered-execution-status-cell/filtered-execution-statuses-cell.component';
import { lastExecutionDateColumn } from '../cell-renderers/last-execution-date-cell/last-execution-date-cell.component';
import { startExecutionColumn } from '../cell-renderers/start-execution/start-execution.component';
import { SuccessRateComponent } from '../cell-renderers/success-rate/success-rate.component';
import { ItpiMultiEditDialogComponent } from '../itpi-multi-edit-dialog/itpi-multi-edit-dialog.component';
import { ItpiMultiEditDialogConfiguration } from '../itpi-multi-edit-dialog/itpi-multi-edit-dialog.configuration';
import { IterationAssignableUsersProvider } from './iteration-assignable-user-provider';
import { ExecutionRunnerOpenerService } from '../../../../execution/execution-runner/services/execution-runner-opener.service';
import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { IterationViewState } from '../../state/iteration-view.state';
import { withProjectLinkColumn, withTestCaseLinkColumn } from '../../../cell-renderer.builders';
import { ExecutionEnvironmentSummaryService } from '../../../../../components/squash-orchestrator/orchestrator-execution-environment/services/execution-environment-summary.service';
import { deleteTestPlanItemCellRendererComponent } from '../../../../../components/test-plan/cell-renderers/delete-test-plan-item-cell-renderer/delete-test-plan-item-cell-renderer.component';
import { GENERIC_TEST_PLAN_ITEM_DELETER } from '../../../../../components/test-plan/generic-test-plan-item-deleter';
import { TestPlanDraggedContentComponent } from '../../../../../components/test-plan/test-plan-dragged-content/test-plan-dragged-content.component';
import { GenericTestPlanServerOperationHandler } from '../../../../../components/test-plan/generic-test-plan-server-operation-handler';
import { GENERIC_TEST_PLAN_ITEM_MOVER } from '../../../../../components/test-plan/generic-test-plan-item-mover';

export function itvItpeTableDefinition(
  localPersistenceService: LocalPersistenceService,
): GridDefinition {
  return grid(GridId.ITERATION_TEST_PLAN)
    .withColumns([
      indexColumn().enableDnd().withViewport('leftViewport'),
      withProjectLinkColumn(GridColumnId.projectName)
        .withI18nKey('sqtm-core.entity.project.label.singular')
        .changeWidthCalculationStrategy(new Limited(100))
        .withAssociatedFilter(),
      milestoneLabelColumn(GridColumnId.milestoneLabels).changeWidthCalculationStrategy(
        new Limited(80),
      ),
      executionModeColumn(GridColumnId.inferredExecutionMode)
        .withI18nKey('sqtm-core.entity.execution.mode.label')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Fixed(70)),
      textColumn(GridColumnId.testCaseReference)
        .withI18nKey('sqtm-core.entity.generic.reference.label')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Limited(100)),
      withTestCaseLinkColumn(GridColumnId.testCaseName)
        .withI18nKey('sqtm-core.entity.test-case.label.singular')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Limited(160)),
      testCaseImportanceColumn(GridColumnId.importance)
        .withI18nKey('sqtm-core.entity.test-case.importance.label-short-dot')
        .withTitleI18nKey('sqtm-core.entity.test-case.importance.label')
        .isEditable(false)
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Fixed(65)),
      dataSetColumn(GridColumnId.datasetName, {
        kind: 'dataset',
        itemIdKey: 'itemTestPlanId',
        testPlanOwnerType: 'iteration',
      })
        .withI18nKey('sqtm-core.entity.dataset.label.short')
        .withAssociatedFilter()
        .withTitleI18nKey('sqtm-core.entity.dataset.label.singular')
        .changeWidthCalculationStrategy(new Limited(90)),
      textColumn(GridColumnId.testSuites)
        .withI18nKey('sqtm-core.entity.test-suite.label.singular')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Limited(130)),
      filteredExecutionStatusesColumn(GridColumnId.executionStatus)
        .withI18nKey('sqtm-core.entity.execution.status.label')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Fixed(70)),
      centredTextColumn(GridColumnId.successRate)
        .disableSort()
        .withRenderer(SuccessRateComponent)
        .withI18nKey('sqtm-core.entity.execution-plan.success-rate.label.short')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.success-rate.label.long')
        .changeWidthCalculationStrategy(new Fixed(100)),
      assignedUserColumn(GridColumnId.assigneeFullName)
        .withI18nKey('sqtm-core.generic.label.user')
        .withAssociatedFilter(GridColumnId.assigneeLogin)
        .changeWidthCalculationStrategy(new Limited(200)),
      lastExecutionDateColumn(GridColumnId.lastExecutedOn),
      startExecutionColumn(GridColumnId.startExecution)
        .withLabel('')
        .disableSort()
        .changeWidthCalculationStrategy(new Fixed(40))
        .withViewport('rightViewport'),
      deleteTestPlanItemCellRendererComponent(),
    ])
    .server()
    .withRowConverter(itpiLiteralConverter)
    .disableRightToolBar()
    .withRowHeight(35)
    .withStyle(new StyleDefinitionBuilder().enableInitialLoadAnimation().showLines())
    .enableInternalDrop()
    .enableDrag()
    .withDraggedContentRenderer(TestPlanDraggedContentComponent)
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
}

const logger = iterationViewLogger.compose('IterationTestPlanExecutionComponent');

@Component({
  selector: 'sqtm-app-iteration-test-plan-execution',
  templateUrl: './iteration-test-plan-execution.component.html',
  styleUrls: ['./iteration-test-plan-execution.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    DatePipe,
    {
      provide: ITV_ITPE_TABLE_CONF,
      useFactory: itvItpeTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: GENERIC_TEST_PLAN_ITEM_MOVER,
      useExisting: IterationViewService,
    },
    GenericTestPlanServerOperationHandler,
    {
      provide: ITV_ITPE_TABLE,
      useFactory: gridServiceFactory,
      deps: [
        RestService,
        ITV_ITPE_TABLE_CONF,
        ReferentialDataService,
        GenericTestPlanServerOperationHandler,
      ],
    },
    { provide: GridService, useExisting: ITV_ITPE_TABLE },
    { provide: UserHistorySearchProvider, useClass: IterationAssignableUsersProvider },
    {
      provide: GRID_PERSISTENCE_KEY,
      useValue: 'iteration-test-plan-grid',
    },
    GridWithStatePersistence,
    {
      provide: ASSIGNED_USER_DELEGATE,
      useExisting: IterationViewService,
    },
    {
      provide: GENERIC_TEST_PLAN_ITEM_DELETER,
      useExisting: IterationViewService,
    },
  ],
})
export class IterationTestPlanExecutionComponent implements OnInit, AfterViewInit, OnDestroy {
  dropZoneId = ITERATION_TEST_PLAN_DROP_ZONE_ID;

  testCaseWorkspace = Workspaces['test-case-workspace'];

  @ViewChild('content', { read: ElementRef })
  content: ElementRef;
  @ViewChild('iconColumnElement')
  iconColumnElement: ElementRef;
  private overlayRef: OverlayRef;

  launchAutomatedMenuVisible$ = new BehaviorSubject<boolean>(false);

  relaunchTestPlanButtonAsync$ = new BehaviorSubject<boolean>(false);

  resumeTestPlanButtonAsync$ = new BehaviorSubject<boolean>(false);

  unsub$ = new Subject<void>();

  hasSelectedRows$: Observable<boolean>;
  hasSelectedAutomatedRows$: Observable<boolean>;
  activeFilters$: Observable<GridFilter[]>;

  componentData$: Observable<IterationViewComponentData>;

  private areAllItemsReady$: Observable<boolean>;
  private isOneItemReadyOrRunning$: Observable<boolean>;
  isOneItemAutomated$: Observable<boolean>;

  isLaunchButtonVisible$: Observable<boolean>;
  isReLaunchButtonVisible$: Observable<boolean>;
  isResumeButtonVisible$: Observable<boolean>;
  canEditExecPlan$: Observable<boolean>;
  canDeleteInExecPlan$: Observable<boolean>;
  openSelectFilterMenu = false;

  constructor(
    private gridService: GridService,
    private iterationViewService: IterationViewService,
    private renderer: Renderer2,
    private interWindowCommunicationService: InterWindowCommunicationService,
    private dialogService: DialogService,
    private restService: RestService,
    private viewContainerRef: ViewContainerRef,
    private workspaceWithTree: WorkspaceWithTreeComponent,
    private iterationService: IterationService,
    private automatedSuiteService: AutomatedSuiteService,
    private actionErrorDisplayService: ActionErrorDisplayService,
    private router: Router,
    private cdRef: ChangeDetectorRef,
    private gridWithStatePersistence: GridWithStatePersistence,
    private executionRunnerOpenerService: ExecutionRunnerOpenerService,
    private overlay: Overlay,
    protected referentialDataService: ReferentialDataService,
    private executionEnvironmentSummaryService: ExecutionEnvironmentSummaryService,
    @Inject(APP_BASE_HREF) private baseUrl: string,
  ) {}

  ngOnInit() {
    this.componentData$ = this.iterationViewService.componentData$;
    this.initializeCommunicationWithExecutionDialog();
    this.initializeGridFilters();
    this.initializeStateObservables();
    this.enableDragInGridAccordingToPermissions();
  }

  private enableDragInGridAccordingToPermissions() {
    this.componentData$.pipe(take(1)).subscribe((componentData: IterationComponentData) => {
      this.gridService.setEnableDrag(componentData.permissions.canLink);
    });
  }

  private initializeStateObservables() {
    this.hasSelectedRows$ = this.gridService.hasSelectedRows$.pipe(takeUntil(this.unsub$));

    this.hasSelectedAutomatedRows$ = this.gridService.selectedRows$.pipe(
      takeUntil(this.unsub$),
      filter((rows: DataRow[]) => rows.length > 0),
      map(
        (rows: DataRow[]) =>
          rows.filter(
            (row: DataRow) => row.data[GridColumnId.inferredExecutionMode] === 'AUTOMATED',
          ).length > 0,
      ),
    );

    this.areAllItemsReady$ = this.componentData$.pipe(
      takeUntil(this.unsub$),
      map((componentData) =>
        Array.from(componentData.iteration.executionStatusMap.values()).every(
          (status) => status === ExecutionStatus.READY.id,
        ),
      ),
    );

    this.isOneItemReadyOrRunning$ = this.componentData$.pipe(
      takeUntil(this.unsub$),
      map((componentData) =>
        Array.from(componentData.iteration.executionStatusMap.values()).some(
          (status) => status === ExecutionStatus.READY.id || status === ExecutionStatus.RUNNING.id,
        ),
      ),
    );

    this.isOneItemAutomated$ = this.gridService.dataRows$.pipe(
      takeUntil(this.unsub$),
      map((rows: Dictionary<DataRow>) => Object.values(rows)),
      map((rows: DataRow[]) =>
        rows.some((row) => row.data[GridColumnId.inferredExecutionMode] === 'AUTOMATED'),
      ),
    );
    this.initializeButtonObservables();
  }

  private initializeButtonObservables() {
    this.isLaunchButtonVisible$ = this.areAllItemsReady$;
    this.isReLaunchButtonVisible$ = this.areAllItemsReady$.pipe(
      map((areAllItemsReady) => !areAllItemsReady),
    );
    this.isResumeButtonVisible$ = this.areAllItemsReady$.pipe(
      map((areAllItemsReady) => !areAllItemsReady),
      withLatestFrom(this.isOneItemReadyOrRunning$),
      map(([notAllReady, oneReadyOrRunning]) => notAllReady && oneReadyOrRunning),
    );
    this.canEditExecPlan$ = combineLatest([this.hasSelectedRows$, this.componentData$]).pipe(
      takeUntil(this.unsub$),
      map(
        ([hasSelectedRow, componentData]: [boolean, IterationComponentData]) =>
          hasSelectedRow && componentData.permissions.canWrite,
      ),
    );
  }

  private canDeleteAllRows(selectedRows: DataRow[]) {
    return this.filterDeletableRows(selectedRows).length === selectedRows.length;
  }

  private initializeGridFilters() {
    this.gridService.addFilters(this.buildGridFilters());
  }

  private initializeCommunicationWithExecutionDialog() {
    this.interWindowCommunicationService.interWindowMessages$
      .pipe(
        takeUntil(this.unsub$),
        filter(
          (message: InterWindowMessages) =>
            message.isTypeOf('EXECUTION-STEP-CHANGED') ||
            message.isTypeOf('MODIFICATION-DURING-EXECUTION'),
        ),
        switchMap(() => this.refreshComponentData()),
      )
      .subscribe(() => {
        this.gridService.refreshData();
      });
  }

  ngAfterViewInit(): void {
    this.gridWithStatePersistence.popGridState().subscribe(() => this.fetchTestPlan());

    this.activeFilters$ = this.gridService.activeFilters$.pipe(
      takeUntil(this.unsub$),
      map((gridFilters: GridFilter[]) =>
        gridFilters.filter((gridFilter) => GridFilterUtils.mustIncludeFilter(gridFilter)),
      ),
    );

    this.gridWithStatePersistence
      .popGridState()
      .pipe(
        take(1),
        switchMap(() => this.fetchTestPlan()),
      )
      .subscribe();

    this.gridService.loaded$
      .pipe(
        filter((loaded) => loaded),
        take(1),
        switchMap(() => this.componentData$),
        filter((componentData) => !componentData.permissions.canLink),
        tap(() => {
          this.gridService.setColumnVisibility(GridColumnId.delete, false);
        }),
      )
      .subscribe();
  }

  private fetchTestPlan(): Observable<void> {
    return this.iterationViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      filter((componentData: IterationViewComponentData) => Boolean(componentData.iteration.id)),
      take(1),
      switchMap((componentData) => {
        return this.referentialDataService.isPremiumPluginInstalled$.pipe(
          take(1),
          map((isPremium) => {
            const showMilestones = componentData.globalConfiguration.milestoneFeatureEnabled;
            this.gridService.setColumnVisibility(GridColumnId.milestoneLabels, showMilestones);
            this.gridService.setColumnVisibility(GridColumnId.hasExecutions, false);
            this.gridService.setServerUrl(
              [`iteration/${componentData.iteration.id}/test-plan`],
              isPremium,
            );
          }),
        );
      }),
    );
  }

  private refreshComponentData() {
    return this.componentData$.pipe(
      take(1),
      switchMap((componentData) => this.iterationViewService.load(componentData.iteration.id)),
    );
  }

  private buildGridFilters(): GridFilter[] {
    return buildFilters([
      serverBackedGridTextFilter(GridColumnId.projectName),
      serverBackedGridTextFilter(GridColumnId.testCaseName),
      serverBackedGridTextFilter(GridColumnId.testCaseReference),
      serverBackedGridTextFilter(GridColumnId.testSuites),
      serverBackedGridTextFilter(GridColumnId.datasetName),
      executionStatusFilter(
        GridColumnId.executionStatus,
        ResearchColumnPrototype.EXECUTION_STATUS,
      ).alwaysActive(),
      i18nEnumResearchFilter(
        GridColumnId.importance,
        ResearchColumnPrototype.TEST_CASE_IMPORTANCE,
      ).alwaysActive(),
      i18nEnumResearchFilter(
        GridColumnId.inferredExecutionMode,
        ResearchColumnPrototype.EXECUTION_EXECUTION_MODE,
      ).alwaysActive(),
      assigneeFilter(
        GridColumnId.assigneeLogin,
        ResearchColumnPrototype.ITEM_TEST_PLAN_TESTER,
      ).alwaysActive(),
      dateRangeFilter(
        GridColumnId.lastExecutedOn,
        ResearchColumnPrototype.ITEM_TEST_PLAN_LASTEXECON,
      ),
    ]);
  }

  ngOnDestroy(): void {
    this.gridService.complete();
    this.relaunchTestPlanButtonAsync$.complete();
    this.resumeTestPlanButtonAsync$.complete();
    this.launchAutomatedMenuVisible$.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  toggleTestCasePickerDrawer() {
    this.iterationViewService.toggleTestCaseTreePicker();
  }

  dropIntoTestPlan($event: SqtmDropEvent) {
    if ($event.dragAndDropData.origin === TEST_CASE_TREE_PICKER_ID) {
      this.dropTestCasesIntoTestPlan($event);
    }
  }

  private dropTestCasesIntoTestPlan($event: SqtmDropEvent) {
    const data = $event.dragAndDropData.data as GridDndData;
    if (logger.isDebugEnabled()) {
      logger.debug(`Dropping test cases into iteration test plan.`, [data]);
    }
    const testCaseIds = data.dataRows.map((row) => parseDataRowId(row));
    this.iterationViewService.addTestCaseIntoTestPlan(testCaseIds).subscribe(() => {
      this.gridService.refreshData();
      this.unmarkAsDropZone();
      this.executionEnvironmentSummaryService.refreshView();
    });
  }

  dragEnter($event: SqtmDragEnterEvent) {
    if (isDndDataFromTestCaseTreePicker($event)) {
      this.markAsDropZone();
    }
  }

  dragLeave($event: SqtmDragLeaveEvent) {
    if (isDndDataFromTestCaseTreePicker($event)) {
      this.unmarkAsDropZone();
    }
  }

  dragCancel() {
    this.unmarkAsDropZone();
  }

  private markAsDropZone() {
    this.renderer.addClass(this.content.nativeElement, 'sqtm-core-border-current-workspace-color');
  }

  private unmarkAsDropZone() {
    this.renderer.removeClass(
      this.content.nativeElement,
      'sqtm-core-border-current-workspace-color',
    );
  }

  showMassDeleteItpiDialog(iterationId: number) {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        map((rows: DataRow[]) => ({
          selectedRows: rows,
          deletableRows: this.filterDeletableRows(rows),
        })),
        switchMap(({ selectedRows, deletableRows }) =>
          this.doOpenMassDeleteDialog(selectedRows, deletableRows, iterationId),
        ),
      )
      .subscribe(() => {
        this.gridService.refreshData();
        this.executionEnvironmentSummaryService.refreshView();
      });
  }

  private doOpenMassDeleteDialog(
    selectedRows: DataRow[],
    selectedDeletableRows: DataRow[],
    iterationId: number,
  ): Observable<void> {
    const itemTestPlanIds = selectedDeletableRows.map((row) => row.data.itemTestPlanId);
    const hasExecutions = selectedRows.filter((row) => row.data.lastExecutedOn != null).length > 0;

    const dialogReference = this.getDialogReference(
      selectedRows.length === selectedDeletableRows.length,
      hasExecutions,
    );

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      filter((confirm) => confirm),
      concatMap(() =>
        this.iterationService.deleteTestPlanItemsFromIteration(iterationId, itemTestPlanIds),
      ),
    );
  }

  private filterDeletableRows(rows: DataRow[]): DataRow[] {
    return rows.filter((row) => {
      if (row.data.lastExecutedOn == null) {
        return row.simplePermissions.canWrite;
      } else {
        return row.simplePermissions.canExtendedDelete;
      }
    });
  }

  getDialogReference(allSelectedRowsAreDeletable: boolean, hasExecutions: boolean) {
    let messageKey = 'sqtm-core.campaign-workspace.dialog.message.mass-remove-association.unbind';

    if (hasExecutions) {
      messageKey = allSelectedRowsAreDeletable
        ? 'sqtm-core.campaign-workspace.dialog.message.mass-remove-association.delete'
        : 'sqtm-core.campaign-workspace.dialog.message.mass-remove-association.delete-only-deletable';
    }

    const dialogConf = {
      id: 'iteration-tpi-mass-delete-dialog',
      titleKey: 'sqtm-core.campaign-workspace.dialog.title.mass-remove-itpi',
      messageKey: messageKey,
      level: hasExecutions ? 'DANGER' : 'WARNING',
    } satisfies ConfirmConfiguration;

    return this.dialogService.openConfirm(dialogConf, 600);
  }

  openMassEditDialog(): void {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        switchMap((rows: DataRow[]) => this.doOpenMassEditDialog(rows)),
        filter((dialogResult) => Boolean(dialogResult)),
      )
      .subscribe(() => {
        this.gridService.refreshData();
        this.refreshIterationNode();
      });
  }

  private refreshIterationNode(): void {
    this.iterationViewService.componentData$
      .pipe(
        take(1),
        map(({ iteration }) =>
          new EntityRowReference(iteration.id, SquashTmDataRowType.Iteration).asString(),
        ),
      )
      .subscribe((identifier) => this.workspaceWithTree.requireNodeRefresh([identifier]));
  }

  private doOpenMassEditDialog(rows: DataRow[]): Observable<void | undefined> {
    const dialogReference = this.dialogService.openDialog<ItpiMultiEditDialogConfiguration, void>({
      id: 'itpi-multi-edit',
      component: ItpiMultiEditDialogComponent,
      viewContainerReference: this.viewContainerRef,
      data: {
        id: 'itpi-multi-edit',
        titleKey: 'sqtm-core.search.generic.modify.selection',
        rows,
      },
    });

    return dialogReference.dialogClosed$;
  }

  openCreateTestSuiteDialog(): void {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        map((dataRows) => dataRows.map((row) => row.data.itemTestPlanId)),
        withLatestFrom(this.componentData$),
        switchMap(([itpiIds, componentData]) =>
          this.doOpenCreateTestSuiteDialog(itpiIds, componentData.iteration),
        ),
        filter((dialogResult) => Boolean(dialogResult)),
        takeUntil(this.unsub$),
      )
      .subscribe();
  }

  private doOpenCreateTestSuiteDialog(
    selectedItems: number[],
    iteration: IterationState,
  ): Observable<any> {
    const parentEntityReference = new EntityRowReference(
      iteration.id,
      SquashTmDataRowType.Iteration,
    ).asString();
    const projectId = iteration.projectId;

    const configuration: DialogConfiguration = {
      id: 'add-suite',
      width: 800,
      viewContainerReference: this.viewContainerRef,
      component: CreateEntityDialogComponent,
      data: {
        titleKey: 'sqtm-core.campaign-workspace.dialog.title.new-suite',
        id: 'add-suite',
        projectId,
        bindableEntity: BindableEntity.TEST_SUITE,
        parentEntityReference,
        postUrl: 'campaign-tree/new-test-suite',
      },
    };

    const dialogReference = this.dialogService.openDialog(configuration);

    return dialogReference.dialogResultChanged$.pipe(
      takeUntil(dialogReference.dialogClosed$),
      takeUntil(this.unsub$),
      filter((addedRow) => addedRow != null),
      switchMap((addedRow: DataRow) => this.bindSelectionToTestSuite(selectedItems, addedRow)),
      tap((addedRow) => this.iterationViewService.updateStateAfterNewSuiteCreated(addedRow)),
      tap(() => this.refreshIterationNode()),
      tap(() => this.gridService.refreshData()),
    );
  }

  private bindSelectionToTestSuite(
    selectedItems: number[],
    testSuiteRow: DataRow,
  ): Observable<DataRow> {
    const testSuiteId: number = testSuiteRow.data.ID;
    return this.restService
      .post(['test-suites/test-plan/bind'], {
        itemIds: selectedItems,
        testSuiteIds: [testSuiteId],
      })
      .pipe(map(() => testSuiteRow));
  }

  resumeExecution() {
    this.componentData$
      .pipe(
        take(1),
        map((d) => d.iteration.id),
        tap(() => this.resumeTestPlanButtonAsync$.next(true)),
        withLatestFrom(this.gridService.activeFilters$),
        map(([id, filters]: [number, GridFilter[]]) => [
          id,
          GridFilterUtils.createRequestFilters(filters),
        ]),
        concatMap(([id, filters]: [number, FilterValueModel[]]) => {
          if (filters.length > 0) {
            return this.iterationService.resumeFiltered(id, filters);
          } else {
            return this.iterationService.resume(id);
          }
        }),
        catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
        finalize(() => this.resumeTestPlanButtonAsync$.next(false)),
      )
      .subscribe((resume: TestPlanResumeModel) => this.navigateToTestPlanRunner(resume));
  }

  relaunchExecution() {
    this.gridService.activeFilters$
      .pipe(
        take(1),
        map((filters: GridFilter[]) => GridFilterUtils.createRequestFilters(filters)),
        map((filters: FilterValueModel[]) => {
          if (filters.length > 0) {
            return this.getFilteredTestPlanExecutionsDeletionDialogConfiguration();
          } else {
            return this.getIterationExecutionsDeletionDialogConfiguration();
          }
        }),
        switchMap(
          (dialogConfiguration: Partial<ConfirmDeleteConfiguration>) =>
            this.dialogService.openDeletionConfirm(dialogConfiguration).dialogClosed$,
        ),
        filter((confirm: boolean) => confirm),
        tap(() => this.relaunchTestPlanButtonAsync$.next(true)),
        withLatestFrom(this.componentData$, this.gridService.activeFilters$),
        map(([, componentData, filters]) => [
          componentData,
          GridFilterUtils.createRequestFilters(filters),
        ]),
        concatMap(([componentData, filters]: [IterationComponentData, FilterValueModel[]]) => {
          if (filters.length > 0) {
            return this.iterationService.relaunchFiltered(componentData.iteration.id, filters);
          } else {
            return this.iterationService.relaunch(componentData.iteration.id);
          }
        }),
        catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
        finalize(() => this.relaunchTestPlanButtonAsync$.next(false)),
        map((resume: TestPlanResumeModel) => this.navigateToTestPlanRunner(resume)),
        switchMap(() => this.refreshComponentData()),
      )
      .subscribe(() => this.gridService.refreshData());
  }

  private getIterationExecutionsDeletionDialogConfiguration(): Partial<ConfirmDeleteConfiguration> {
    return {
      id: 'confirm-delete-all-execution',
      level: 'DANGER',
      titleKey: 'sqtm-core.campaign-workspace.dialog.title.iteration.mass-delete-execution',
      messageKey: 'sqtm-core.campaign-workspace.dialog.message.iteration.mass-delete-execution',
    };
  }

  private getFilteredTestPlanExecutionsDeletionDialogConfiguration(): Partial<ConfirmDeleteConfiguration> {
    return {
      id: 'confirm-delete-filtered-execution',
      level: 'DANGER',
      titleKey:
        'sqtm-core.campaign-workspace.dialog.title.filtered-test-plan.mass-delete-execution',
      messageKey:
        'sqtm-core.campaign-workspace.dialog.message.filtered-test-plan.mass-delete-execution',
    };
  }

  private navigateToTestPlanRunner(resume: TestPlanResumeModel) {
    let baseUrl = `${this.baseUrl}${EXECUTION_DIALOG_RUNNER_URL}/iteration/${resume.iterationId}/test-plan/${resume.testPlanItemId}/execution/${resume.executionId}`;
    if (resume.initialStepIndex && resume.initialStepIndex > 0) {
      baseUrl += `/step/${resume.initialStepIndex + 1}`;
    } else {
      baseUrl += `/${EXECUTION_RUNNER_PROLOGUE_URL}`;
    }
    const url = this.router
      .createUrlTree([baseUrl], {
        queryParams: {
          hasNextTestCase: resume.hasNextTestCase,
          partialTestPlanItemIds: JSON.stringify(resume.partialTestPlanItemIds),
        },
      })
      .toString();
    this.executionRunnerOpenerService.openExecutionWithProvidedUrl(url);
  }

  launchAllAutomatedTests() {
    this.launchAutomatedMenuVisible$.next(false);
    this.componentData$
      .pipe(
        take(1),
        map((componentData: IterationViewState): number => componentData.iteration.id),
        withLatestFrom(this.gridService.activeFilters$),
        map(
          ([iterationId, filters]: [
            number,
            GridFilter[],
          ]): AutomatedSuiteCreationSpecification => ({
            context: new EntityReference(iterationId, EntityType.ITERATION),
            testPlanSubsetIds: [],
            iterationId: this.iterationViewService.getSnapshot().iteration.id,
            filterValues: GridFilterUtils.createRequestFilters(filters),
          }),
        ),
      )
      .subscribe((specification: AutomatedSuiteCreationSpecification) =>
        this.openAutomatedExecutionSupervisionDialog(specification),
      );
  }

  launchSelectedAutomatedTests() {
    this.launchAutomatedMenuVisible$.next(false);
    this.gridService.selectedRows$
      .pipe(
        take(1),
        map((rows: DataRow[]) =>
          rows.filter(
            (row: DataRow) => row.data[GridColumnId.inferredExecutionMode] === 'AUTOMATED',
          ),
        ),
        filter((rows: DataRow[]) => rows.length > 0),
        map((rows: DataRow[]) => rows.map((row) => row.id)),
        withLatestFrom(this.componentData$),
        map(
          ([itemIds, componentData]: [
            any,
            IterationViewState,
          ]): AutomatedSuiteCreationSpecification => ({
            context: new EntityReference(componentData.iteration.id, EntityType.ITERATION),
            testPlanSubsetIds: itemIds,
            iterationId: this.iterationViewService.getSnapshot().iteration.id,
            filterValues: [],
          }),
        ),
      )
      .subscribe((specification: AutomatedSuiteCreationSpecification) =>
        this.openAutomatedExecutionSupervisionDialog(specification),
      );
  }

  private openAutomatedExecutionSupervisionDialog(data: AutomatedSuiteCreationSpecification) {
    const automatedExecutionDialog = this.dialogService.openDialog({
      id: 'automated-tests-execution-supervision',
      viewContainerReference: this.viewContainerRef,
      component: AutomatedTestsExecutionSupervisionDialogComponent,
      data,
      height: 680,
      width: 800,
    });
    automatedExecutionDialog.dialogClosed$
      .pipe(
        filter((result) => Boolean(result)),
        switchMap(() => this.iterationViewService.incrementAutomatedSuiteCount()),
      )
      .subscribe(() => {
        this.gridService.refreshData();
        this.executionEnvironmentSummaryService.refreshView();
      });
  }

  openLaunchAutomatedMenu() {
    this.launchAutomatedMenuVisible$.next(true);
  }

  shouldShowResetFilterLink(activeFilters: GridFilter[]): boolean {
    return activeFilters?.length > 0;
  }

  resetFilters() {
    this.gridService.resetFilters();
  }

  canBeTriggered(canExecute: boolean) {
    return canExecute ? 'click' : null;
  }

  showAvailableFilters(): void {
    this.openSelectFilterMenu = true;

    const positionStrategy = this.overlay
      .position()
      .flexibleConnectedTo(this.iconColumnElement)
      .withPositions([
        { originX: 'center', overlayX: 'center', originY: 'bottom', overlayY: 'top', offsetY: 0 },
        { originX: 'center', overlayX: 'center', originY: 'top', overlayY: 'bottom', offsetY: -10 },
      ]);

    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
    };

    this.overlayRef = this.overlay.create(overlayConfig);
    const componentPortal = new ComponentPortal(CustomColumnsComponent, this.viewContainerRef);
    const componentComponentRef = this.overlayRef.attach(componentPortal);

    componentComponentRef.instance.projectId =
      this.iterationViewService.getSnapshot().iteration.projectId;

    this.overlayRef.backdropClick().subscribe(() => this.close());
    componentComponentRef.instance.confirmClicked.subscribe(() => this.close());
  }

  close() {
    if (this.overlayRef) {
      this.openSelectFilterMenu = false;
      this.overlayRef.dispose();
      this.cdRef.detectChanges();
    }
  }
}

export function itpiLiteralConverter(
  literals: Partial<DataRow>[],
  projectDataMap: ProjectDataMap,
): DataRow[] {
  const itpiLiterals = literals.map((li) => ({
    ...li,
    type: SquashTmDataRowType.IterationTestPlanItem,
  }));

  return convertSqtmLiterals(itpiLiterals, projectDataMap);
}

export type IterationComponentData = EntityViewComponentData<
  IterationState,
  'iteration',
  CampaignPermissions
>;
