import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { CampaignMilestoneViewService } from '../../services/campaign-milestone-view.service';
import { CampaignMilestoneViewState } from '../../state/campaign-milestone-view-state';
import { Observable, Subject } from 'rxjs';
import { ReferentialDataService } from 'sqtm-core';
import { filter, take, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-campaign-milestone-view',
  templateUrl: './campaign-milestone-view.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [CampaignMilestoneViewService],
})
export class CampaignMilestoneViewComponent implements OnInit, OnDestroy {
  public componentData$: Observable<Readonly<CampaignMilestoneViewState>>;

  private unsub$ = new Subject<void>();

  constructor(
    private viewService: CampaignMilestoneViewService,
    private referentialDataService: ReferentialDataService,
    private cdRef: ChangeDetectorRef,
  ) {}

  ngOnInit(): void {
    this.referentialDataService.loaded$
      .pipe(
        takeUntil(this.unsub$),
        filter((loaded) => loaded),
        take(1),
      )
      .subscribe(() => {
        this.componentData$ = this.viewService.componentData$;
        this.viewService.init();
        this.cdRef.detectChanges();
      });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
