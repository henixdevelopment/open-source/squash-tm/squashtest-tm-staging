import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewContainerRef,
} from '@angular/core';
import {
  ActionErrorDisplayService,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
  ListPanelItem,
  ReferentialDataService,
  RestService,
  TableValueChange,
} from 'sqtm-core';
import { Overlay } from '@angular/cdk/overlay';
import { TranslateService } from '@ngx-translate/core';
import { IterationViewService } from '../../../services/iteration-view.service';
import { map, take, takeUntil } from 'rxjs/operators';
import { IterationViewComponentData } from '../../../container/iteration-view/iteration-view.component';
import { Observable } from 'rxjs';
import { AbstractExecutionStatusesCell } from '../../../../campaign-workspace/components/test-plan/abstract-execution-statuses-cell';

@Component({
  selector: 'sqtm-app-filtered-execution-status-cell',
  templateUrl: './filtered-execution-statuses-cell.component.html',
  styleUrls: ['./filtered-execution-statuses-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilteredExecutionStatusesCellComponent extends AbstractExecutionStatusesCell {
  canEdit$: Observable<boolean>;
  panelItems$: Observable<ListPanelItem[]>;

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    public readonly overlay: Overlay,
    public readonly vcr: ViewContainerRef,
    public readonly translateService: TranslateService,
    public readonly restService: RestService,
    public readonly actionErrorDisplayService: ActionErrorDisplayService,
    public iterationViewService: IterationViewService,
    public referentialDataService: ReferentialDataService,
  ) {
    super(
      grid,
      cdRef,
      overlay,
      vcr,
      translateService,
      restService,
      actionErrorDisplayService,
      referentialDataService,
    );

    this.canEdit$ = this.iterationViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      map((componentData) => this.canEdit(componentData)),
    );

    this.panelItems$ = this.iterationViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      map((componentData) => componentData.projectData),
      map((projectData) => this.getFilteredExecutionStatusKeys(projectData)),
      map((statuses: string[]) => this.asListItemOptions(statuses)),
    );
  }

  private canEdit(componentData: IterationViewComponentData): boolean {
    return (
      componentData &&
      componentData.permissions.canWrite &&
      componentData.milestonesAllowModification
    );
  }

  change(executionStatusKey: string) {
    this.authenticatedUser$.pipe(take(1)).subscribe((authenticatedUser) => {
      this.iterationViewService
        .updateExecutionStatus(executionStatusKey, this.row.data[GridColumnId.itemTestPlanId])
        .subscribe();

      const changes: TableValueChange[] = [
        { columnId: this.columnDisplay.id, value: executionStatusKey },
        { columnId: GridColumnId.assigneeFullName, value: this.getUser(authenticatedUser) },
        { columnId: GridColumnId.lastExecutedOn, value: new Date() },
      ];

      this.grid.editRows([this.row.id], changes);
      this.close();
    });
  }

  isExploratoryExecution() {
    return this.row.data[GridColumnId.inferredExecutionMode] === 'EXPLORATORY';
  }
}

export function filteredExecutionStatusesColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(FilteredExecutionStatusesCellComponent)
    .withHeaderPosition('left');
}
