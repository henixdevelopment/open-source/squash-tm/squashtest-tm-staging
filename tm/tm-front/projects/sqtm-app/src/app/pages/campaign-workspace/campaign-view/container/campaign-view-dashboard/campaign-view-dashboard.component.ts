import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  Signal,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { CampaignViewService } from '../../service/campaign-view.service';
import { Observable } from 'rxjs';
import { CampaignViewComponentData } from '../campaign-view/campaign-view.component';
import {
  CampaignService,
  CustomDashboardModel,
  DialogService,
  EntityRowReference,
  EntityScope,
  ExecutionStatusCount,
  IterationPlanning,
  SquashTmDataRowType,
} from 'sqtm-core';
import { baseIterationPlanningDialogConfiguration } from '../../components/dialog/iteration-planning-dialog/iteration-planning-dialog.configuration';
import { concatMap, take } from 'rxjs/operators';
import * as _ from 'lodash';
import { CampaignState } from '../../state/campaign.state';
import { CustomDashboardComponent } from '../../../../../components/custom-dashboard/containers/custom-dashboard/custom-dashboard.component';
import { CampaignWorkspaceViewService } from '../../../campaign-workspace-view.service';

@Component({
  selector: 'sqtm-app-campaign-view-dashboard',
  templateUrl: './campaign-view-dashboard.component.html',
  styleUrls: ['./campaign-view-dashboard.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CampaignViewDashboardComponent implements OnInit {
  $lastExecutionScope: Signal<boolean>;
  componentData$: Observable<CampaignViewComponentData>;

  @ViewChild(CustomDashboardComponent)
  dashboardPanel: CustomDashboardComponent;

  constructor(
    private campaignViewService: CampaignViewService,
    private campaignService: CampaignService,
    private dialogService: DialogService,
    private vcr: ViewContainerRef,
    protected campaignWorkspaceViewService: CampaignWorkspaceViewService,
  ) {
    this.componentData$ = this.campaignViewService.componentData$;
    this.$lastExecutionScope = campaignWorkspaceViewService.$lastExecutionScope;
  }

  ngOnInit(): void {
    this.componentData$
      .pipe(
        take(1),
        concatMap(() => this.campaignViewService.refreshStats(this.$lastExecutionScope())),
      )
      .subscribe();
  }

  campaignProgressionChartHasErrors(componentData: CampaignViewComponentData) {
    const i18nErrors = componentData.campaign.campaignStatisticsBundle.progressionStatistics.errors;
    return i18nErrors && i18nErrors.length > 0;
  }

  openPlanningDialog(event, id: number) {
    event.stopPropagation();
    this.campaignService.findIterationsPlanning(id).subscribe((result) => {
      const dialogRef = this.dialogService.openDialog({
        ...baseIterationPlanningDialogConfiguration,
        viewContainerReference: this.vcr,
        data: { iterationPlannings: result },
      });

      dialogRef.dialogClosed$.subscribe((plannings: IterationPlanning[]) => {
        if (plannings != null) {
          this.campaignService
            .setIterationsPlanning(id, plannings)
            .pipe(
              concatMap(() => this.campaignViewService.refreshStats(this.$lastExecutionScope())),
            )
            .subscribe();
        }
      });
    });
  }

  getScope(componentData: CampaignViewComponentData): EntityScope[] {
    const id = new EntityRowReference(
      componentData.campaign.id,
      SquashTmDataRowType.Campaign,
    ).asString();
    return [{ id, label: componentData.campaign.name, projectId: componentData.projectData.id }];
  }

  hasTestPlanItems(executionStatusCount: ExecutionStatusCount) {
    return Object.values(executionStatusCount).reduce(_.add) > 0;
  }

  getChartBindings(dashboard: CustomDashboardModel) {
    return [...dashboard.chartBindings, ...dashboard.reportBindings];
  }

  displayFavoriteDashboard($event, lastExecutionScope: boolean) {
    $event.stopPropagation();
    this.campaignViewService.changeDashboardToDisplay('dashboard', lastExecutionScope);
  }

  displayDefaultDashboard($event, lastExecutionScope: boolean) {
    $event.stopPropagation();
    this.campaignViewService.changeDashboardToDisplay('default', lastExecutionScope);
  }

  changeExtendedScope(lastExecutionScope: boolean, campaign: CampaignState) {
    this.campaignWorkspaceViewService.setLastExecutionScope(lastExecutionScope);
    this.doRefreshDashboard(campaign);
  }

  private doRefreshDashboard(campaign: CampaignState) {
    if (campaign.campaignStatisticsBundle && this.dashboardPanel == null) {
      this.campaignViewService.refreshStats(this.$lastExecutionScope()).subscribe();
    }
    if (campaign.dashboard && this.dashboardPanel != null) {
      this.dashboardPanel.beginAsync();
      this.campaignViewService.refreshDashboard(this.$lastExecutionScope()).subscribe(() => {
        this.dashboardPanel.endAsync();
      });
    }
  }
}
