import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import {
  SprintReqVersionValidationStatus,
  SprintReqVersionValidationStatusKeys,
  WorkspaceCommonModule,
} from 'sqtm-core';
import { TranslateModule } from '@ngx-translate/core';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

@Component({
  selector: 'sqtm-app-validation-status',
  template: ` <div
    class="validation-status txt__semi-bold"
    nz-tooltip
    [attr.data-test-element-id]="'validation-status'"
    [nzTooltipTitle]="getTooltip() | translate"
    [style.background-color]="getStatusColor()"
  ></div>`,
  styleUrls: ['./validation-status.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [TranslateModule, WorkspaceCommonModule, NzToolTipModule],
})
export class ValidationStatusComponent {
  get validationStatus() {
    return SprintReqVersionValidationStatus[this.status];
  }

  @Input()
  status: SprintReqVersionValidationStatusKeys;

  @Output()
  changeStatus = new EventEmitter<SprintReqVersionValidationStatusKeys>();

  constructor() {}

  getStatusColor(): string {
    return this.validationStatus.color;
  }

  getTooltip(): string {
    return this.validationStatus.i18nKey;
  }
}
