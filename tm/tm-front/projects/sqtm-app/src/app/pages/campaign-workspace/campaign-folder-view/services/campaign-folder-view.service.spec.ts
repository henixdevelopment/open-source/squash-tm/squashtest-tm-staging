import { TestBed } from '@angular/core/testing';

import { CampaignFolderViewService } from './campaign-folder-view.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';
import { AppTestingUtilsModule } from '../../../../utils/testing-utils/app-testing-utils.module';

describe('CampaignFolderViewService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: CampaignFolderViewService,
          useClass: CampaignFolderViewService,
        },
      ],
    }),
  );

  it('should be created', () => {
    const service: CampaignFolderViewService = TestBed.get(CampaignFolderViewService);
    expect(service).toBeTruthy();
  });
});
