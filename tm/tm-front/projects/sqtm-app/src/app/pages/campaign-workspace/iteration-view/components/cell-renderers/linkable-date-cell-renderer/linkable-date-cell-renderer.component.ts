import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { DatePipe } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  getSupportedBrowserLang,
  GridColumnId,
  GridService,
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-linkable-date-cell-renderer',
  template: ` @if (row) {
    <div class="full-width full-height flex-column">
      @if (hasExecutions) {
        <a class="txt-ellipsis m-auto-0" nz-tooltip [nzTooltipTitle]="displayString" href="">{{
          displayString
        }}</a>
      } @else {
        <span class="txt-ellipsis m-auto-0" nz-tooltip [nzTooltipTitle]="displayString">{{
          displayString
        }}</span>
      }
    </div>
  }`,
  styleUrls: ['./linkable-date-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe],
})
export class LinkableDateCellRendererComponent extends AbstractCellRendererComponent {
  protected format = 'shortDate';

  get hasExecutions(): boolean {
    return this.row.data[GridColumnId.hasExecutions];
  }

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    public translateService: TranslateService,
    public datePipe: DatePipe,
  ) {
    super(grid, cdRef);
  }

  get displayString(): string {
    return this.formatDate(this.row.data[this.columnDisplay.id]);
  }

  private formatDate(date: Date): string {
    return this.datePipe.transform(
      date,
      this.format,
      '',
      getSupportedBrowserLang(this.translateService),
    );
  }
}

export function linkableDateColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(LinkableDateCellRendererComponent)
    .withHeaderPosition('center');
}

@Component({
  selector: 'sqtm-app-linkable-date-time-cell-renderer',
  template: ` @if (row) {
    <div class="full-width full-height flex-column">
      @if (hasExecutions) {
        <a class="txt-ellipsis m-auto-0" nz-tooltip [nzTooltipTitle]="displayString" href="">{{
          displayString
        }}</a>
      } @else {
        <span class="txt-ellipsis m-auto-0" nz-tooltip [nzTooltipTitle]="displayString">{{
          displayString
        }}</span>
      }
    </div>
  }`,
  styleUrls: ['./linkable-date-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LinkableDateTimeCellRendererComponent extends LinkableDateCellRendererComponent {
  protected format = 'short';
}

export function linkableDateTimeColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(LinkableDateTimeCellRendererComponent)
    .withHeaderPosition('center');
}
