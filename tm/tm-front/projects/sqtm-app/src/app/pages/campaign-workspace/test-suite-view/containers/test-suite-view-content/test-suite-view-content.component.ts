import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { TestSuiteViewComponentData } from '../test-suite-view/test-suite-view.component';
import { BindableEntity, createCustomFieldValueDataSelector, CustomFieldData } from 'sqtm-core';
import { TestSuiteViewService } from '../../services/test-suite-view.service';
import { takeUntil } from 'rxjs/operators';
import { select } from '@ngrx/store';

@Component({
  selector: 'sqtm-app-test-suite-view-content',
  templateUrl: './test-suite-view-content.component.html',
  styleUrls: ['./test-suite-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestSuiteViewContentComponent implements OnInit {
  private unsub$ = new Subject<void>();
  componentData$: Observable<TestSuiteViewComponentData>;
  customFieldData: CustomFieldData[];

  constructor(private testSuiteViewService: TestSuiteViewService) {
    this.componentData$ = this.testSuiteViewService.componentData$;
  }

  ngOnInit() {
    this.componentData$
      .pipe(
        takeUntil(this.unsub$),
        select(createCustomFieldValueDataSelector(BindableEntity.TEST_SUITE)),
      )
      .subscribe((customFieldData: CustomFieldData[]) => {
        this.customFieldData = customFieldData;
      });
  }
}
