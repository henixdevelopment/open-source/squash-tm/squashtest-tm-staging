import { ColumnDefinitionBuilder, DataRow, GridColumnId, withLinkColumn } from 'sqtm-core';

/*
 * Warning! This cell renderer require a GridWithStatePersistence in the dep tree.
 */
export function withTestCaseLinkColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return withLinkColumn(id, {
    kind: 'link',
    createUrlFunction: urlFunctionToTestCase,
    saveGridStateBeforeNavigate: true,
    fallbackStringKey: 'sqtm-core.generic.label.deleted.masculine',
  });
}
const urlFunctionToTestCase = (row: DataRow): string => {
  return '/test-case-workspace/test-case/detail/' + row.data[GridColumnId.testCaseId];
};

export function withProjectLinkColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return withLinkColumn(id, {
    kind: 'link',
    createUrlFunction: urlFunctionToProject,
    saveGridStateBeforeNavigate: true,
    createTooltipTitleFunction: projectPath,
  });
}

const urlFunctionToProject = (row: DataRow): string => {
  return `/test-case-workspace/test-case/${row.data[GridColumnId.testCaseId]}/content`;
};
const projectPath = (row: DataRow): string => {
  return row.data[GridColumnId.testCasePath];
};

export function withSprintReqVersionLinkColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return withLinkColumn(id, {
    kind: 'link',
    createUrlFunction: urlFunctionToSprintReqVersion,
    saveGridStateBeforeNavigate: true,
  })
    .withI18nKey('sqtm-core.entity.sprint.overall-exec-plan.linked-req-version.short')
    .withTitleI18nKey('sqtm-core.entity.sprint.overall-exec-plan.linked-req-version.long');
}
const urlFunctionToSprintReqVersion = (row: DataRow): string => {
  return `/campaign-workspace/sprint-req-version/${row.data[GridColumnId.sprintReqVersionId]}/content`;
};
