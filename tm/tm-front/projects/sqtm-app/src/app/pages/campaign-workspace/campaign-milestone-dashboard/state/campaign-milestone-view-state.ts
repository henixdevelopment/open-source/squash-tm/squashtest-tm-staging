import {
  StatisticsBundle,
  CustomDashboardModel,
  EntityScope,
  ExecutionStatusKeys,
  Milestone,
} from 'sqtm-core';

export interface CampaignMilestoneViewState {
  milestone: Milestone;
  statistics: StatisticsBundle;
  scope: EntityScope[];
  dashboard: CustomDashboardModel;
  generatedDashboardOn: Date;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
  disabledExecutionStatus: ExecutionStatusKeys[];
  lastExecutionScope: boolean;
}

export function initialCampaignMilestoneViewState(): Readonly<CampaignMilestoneViewState> {
  return {
    milestone: null,
    statistics: null,
    scope: [],
    dashboard: null,
    generatedDashboardOn: null,
    shouldShowFavoriteDashboard: false,
    canShowFavoriteDashboard: false,
    favoriteDashboardId: null,
    disabledExecutionStatus: [],
    lastExecutionScope: true,
  };
}
