import { InjectionToken } from '@angular/core';
import { GridDefinition, GridService } from 'sqtm-core';

export const TEST_SUITE_TEST_PLAN_DROP_ZONE_ID = 'testSuiteTestPlanDropZone';

export const TEST_SUITE_ISSUE_TABLE_CONF = new InjectionToken<GridDefinition>(
  'Grid config for the issue table of test suite view',
);
export const TEST_SUITE_ISSUE_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the issue table of test suite view',
);

export const TSV_ITPE_TABLE_CONF = new InjectionToken<GridDefinition>(
  'Grid config for the test suite test plan execution table of test suite view',
);
export const TSV_ITPE_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the test suite test plan execution table of test suite view',
);

export const TS_AUTOMATED_SUITE_TABLE_CONF = new InjectionToken<GridDefinition>(
  'Grid config for the automated suite table of test suite view',
);

export const TS_AUTOMATED_SUITE_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the automated suite table of test suite view',
);
