import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import {
  ExportModelBuilderService,
  Extendable,
  Fixed,
  grid,
  GRID_PERSISTENCE_KEY,
  GridColumnId,
  GridDefinition,
  GridExportService,
  GridId,
  GridService,
  gridServiceFactory,
  GridWithStatePersistence,
  indexColumn,
  issueExecutionsColumn,
  issueKeyColumn,
  Limited,
  LocalPersistenceService,
  PaginationConfigBuilder,
  ReferentialDataService,
  RestService,
  textColumn,
} from 'sqtm-core';
import {
  SPRINT_ISSUE_TABLE,
  SPRINT_ISSUE_TABLE_CONFIG,
} from '../../../campaign-workspace.constant';
import { SprintViewComponentData } from '../../containers/sprint-view/sprint-view.component';

export function sprintIssueTableDefinition(
  localPersistenceService: LocalPersistenceService,
): GridDefinition {
  return grid(GridId.SPRINT_VIEW_ISSUE)
    .withColumns([
      indexColumn().changeWidthCalculationStrategy(new Fixed(70)).withViewport('leftViewport'),
      issueKeyColumn()
        .withI18nKey('sqtm-core.entity.issue.key.label')
        .changeWidthCalculationStrategy(new Limited(100)),
      textColumn(GridColumnId.btProject)
        .withI18nKey('sqtm-core.entity.issue.project.label')
        .changeWidthCalculationStrategy(new Limited(150))
        .disableSort(),
      textColumn(GridColumnId.summary)
        .withI18nKey('sqtm-core.entity.issue.summary.label')
        .changeWidthCalculationStrategy(new Limited(200))
        .disableSort(),
      textColumn(GridColumnId.priority)
        .withI18nKey('sqtm-core.entity.issue.priority.label')
        .changeWidthCalculationStrategy(new Extendable(100, 0.4))
        .disableSort(),
      textColumn(GridColumnId.status)
        .withI18nKey('sqtm-core.entity.issue.status.label')
        .changeWidthCalculationStrategy(new Limited(80))
        .disableSort(),
      textColumn(GridColumnId.assignee)
        .withI18nKey('sqtm-core.entity.issue.assignee.label')
        .changeWidthCalculationStrategy(new Extendable(100, 1))
        .disableSort(),
      issueExecutionsColumn()
        .withI18nKey('sqtm-core.entity.issue.reported-in.label')
        .changeWidthCalculationStrategy(new Extendable(100, 1.5))
        .disableSort(),
    ])
    .disableRightToolBar()
    .server()
    .withPagination(new PaginationConfigBuilder().initialSize(25))
    .withRowHeight(35)
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
}

@Component({
  selector: 'sqtm-app-sprint-issues-panel',
  templateUrl: './sprint-issues-panel.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: SPRINT_ISSUE_TABLE_CONFIG,
      useFactory: sprintIssueTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: SPRINT_ISSUE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, SPRINT_ISSUE_TABLE_CONFIG, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: SPRINT_ISSUE_TABLE,
    },
    {
      provide: GRID_PERSISTENCE_KEY,
      useValue: 'sprint-req-version-issue-grid',
    },
    GridWithStatePersistence,
    ExportModelBuilderService,
    GridExportService,
  ],
})
export class SprintIssuesPanelComponent {
  @Input({ required: true })
  sprintViewComponentData!: SprintViewComponentData;

  constructor(private gridService: GridService) {}

  loadData() {
    this.gridService.setServerUrl([
      `issues/sprint/${this.sprintViewComponentData.sprint.id}/known-issues`,
    ]);
  }
}
