import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SprintGroupViewComponentData } from '../../containers/sprint-group-view/sprint-group-view.component';

@Component({
  selector: 'sqtm-app-sprint-group-information-panel',
  templateUrl: './sprint-group-information-panel.component.html',
  styleUrls: ['./sprint-group-information-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SprintGroupInformationPanelComponent {
  @Input()
  sprintGroupViewComponentData: SprintGroupViewComponentData;

  constructor(public translateService: TranslateService) {}
}
