import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import {
  buildFilters,
  column,
  DataRow,
  deleteColumn,
  EntityScope,
  Extendable,
  externalLinkColumn,
  Fixed,
  GenericDataRow,
  grid,
  GridColumnId,
  GridDefinition,
  GridFilter,
  GridId,
  GridService,
  i18nEnumResearchFilter,
  indexColumn,
  levelEnumColumn,
  Limited,
  LocalPersistenceService,
  ManagementMode,
  numericColumn,
  RequirementCriticality,
  RequirementStatus,
  ResearchColumnPrototype,
  Scope,
  StyleDefinitionBuilder,
  textColumn,
  textResearchFilter,
  withLinkColumn,
} from 'sqtm-core';
import { SPRINT_REQ_VERSION_TABLE } from '../../../campaign-workspace.constant';
import { DeleteSprintReqVersionComponent } from '../../../sprint-view/components/cell-renderers/delete-sprint-req-version/delete-sprint-req-version.component';
import { takeUntil, tap, withLatestFrom } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { SprintViewComponentData } from '../../../sprint-view/containers/sprint-view/sprint-view.component';
import { SprintViewService } from '../../../sprint-view/service/sprint-view.service';
import { remotePerimeterStatusColumn } from '../../../sprint-view/components/cell-renderers/remote-perimeter-status-cell/remote-perimeter-status-cell.component';
import { SprintReqVersionTestingComponent } from '../../../sprint-view/components/cell-renderers/sprint-req-version-testing/sprint-req-version-testing.component';
import { validationStatusColumn } from '../../cell-renderers/validation-status-cell-renderer.component';
import { TranslateService } from '@ngx-translate/core';

export function sprintReqVersionsTableDefinition(
  localPersistenceService: LocalPersistenceService,
): GridDefinition {
  const urlFunctionToProject = (row: DataRow) => {
    return row.data.requirementId
      ? `/requirement-workspace/requirement/${row.data.requirementId.toString()}/content`
      : null;
  };

  const urlFunctionToRequirement = (row: DataRow) => {
    return row.data.versionId
      ? `/requirement-workspace/requirement-version/detail/${row.data.versionId.toString()}/content`
      : null;
  };

  const urlFunctionToRemoteTicket = (row: DataRow) => {
    return row.data.mode === ManagementMode.SYNCHRONIZED ? row.data.remoteReqUrl : null;
  };

  return grid(GridId.SPRINT_REQ_VERSIONS_LINKED)
    .withColumns([
      indexColumn().changeWidthCalculationStrategy(new Fixed(60)).withViewport('leftViewport'),
      withLinkColumn(GridColumnId.requirementVersionProjectName, {
        kind: 'link',
        createUrlFunction: urlFunctionToProject,
        saveGridStateBeforeNavigate: true,
      })
        .withI18nKey('sqtm-core.entity.project.label.singular')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Limited(100)),
      externalLinkColumn(GridColumnId.reference, {
        kind: 'link',
        createUrlFunction: urlFunctionToRemoteTicket,
        saveGridStateBeforeNavigate: true,
      })
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Limited(100)),
      withLinkColumn(GridColumnId.name, {
        kind: 'link',
        createUrlFunction: urlFunctionToRequirement,
        saveGridStateBeforeNavigate: true,
        fallbackStringKey: 'sqtm-core.generic.label.deleted.feminine',
      })
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Limited(175)),
      textColumn(GridColumnId.categoryLabel)
        .withI18nKey('sqtm-core.entity.requirement.category.label')
        .disableSort()
        .changeWidthCalculationStrategy(new Extendable(60, 0.2)),
      levelEnumColumn(GridColumnId.criticality, RequirementCriticality)
        .withI18nKey('sqtm-core.entity.requirement.criticality.label')
        .disableSort()
        .withContentPosition('center')
        .changeWidthCalculationStrategy(new Extendable(60, 0.2)),
      levelEnumColumn(GridColumnId.status, RequirementStatus)
        .withI18nKey('sqtm-core.entity.generic.status.label')
        .disableSort()
        .withContentPosition('center')
        .changeWidthCalculationStrategy(new Extendable(60, 0.2)),
      textColumn(GridColumnId.remoteReqStatusAndState)
        .withI18nKey('sqtm-core.entity.generic.status.label')
        .disableSort()
        .changeWidthCalculationStrategy(new Limited(80)),
      numericColumn(GridColumnId.nbTests)
        .withTitleI18nKey('sqtm-core.sprint.req-version.nb-tests-full')
        .withI18nKey('sqtm-core.sprint.req-version.nb-tests-short')
        .changeWidthCalculationStrategy(new Extendable(60, 0.2)),
      validationStatusColumn(GridColumnId.validationStatus),
      remotePerimeterStatusColumn(
        GridColumnId.remotePerimeterStatus,
      ).changeWidthCalculationStrategy(new Fixed(40)),
      column(GridColumnId.testSprintReqVersion)
        .withRenderer(SprintReqVersionTestingComponent)
        .withLabel('')
        .disableSort()
        .changeWidthCalculationStrategy(new Fixed(80))
        .withViewport('rightViewport'),
      deleteColumn(DeleteSprintReqVersionComponent).withViewport('rightViewport'),
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .disableRightToolBar()
    .withRowHeight(35)
    .withRowConverter(convertSprintReqVersionLiterals)
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
}

export function convertSprintReqVersionLiteral(literal: Partial<DataRow>): DataRow {
  const dataRow: DataRow = new GenericDataRow();
  dataRow.projectId = literal.data.projectId;
  Object.assign(dataRow, literal);
  return dataRow;
}

export function convertSprintReqVersionLiterals(literals: Partial<DataRow>[]): DataRow[] {
  return literals.map((literal) => convertSprintReqVersionLiteral(literal));
}

@Component({
  selector: 'sqtm-app-sprint-req-versions-linked',
  template: `
    <sqtm-core-grid class="full-width overflow-hidden flex-column p-10"></sqtm-core-grid>
  `,
  styleUrl: './sprint-req-versions-linked.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: SPRINT_REQ_VERSION_TABLE,
    },
  ],
})
export class SprintReqVersionsLinkedComponent implements OnInit, OnDestroy {
  private unsub$ = new Subject<void>();
  componentData$: Observable<SprintViewComponentData>;

  constructor(
    private gridService: GridService,
    private sprintViewService: SprintViewService,
    private translateService: TranslateService,
  ) {
    this.componentData$ = this.sprintViewService.componentData$;
  }

  ngOnInit() {
    this.initializeFilters();
    this.setScope();
    this.getReferenceColumnTitle();
    this.keepOrRemoveProjectColumn();
  }

  private getReferenceColumnTitle() {
    this.componentData$
      .pipe(
        takeUntil(this.unsub$),
        tap((componentData: SprintViewComponentData) => {
          const referenceColumnLabelKey =
            componentData.sprint.synchronisationKind == null
              ? 'sqtm-core.entity.generic.reference.label'
              : 'sqtm-core.entity.issue.key.label';
          const referenceColumnLabel = this.translateService.instant(referenceColumnLabelKey);
          this.gridService.renameColumn(GridColumnId.reference, referenceColumnLabel);
        }),
      )
      .subscribe();
  }

  private keepOrRemoveProjectColumn() {
    this.componentData$
      .pipe(
        takeUntil(this.unsub$),
        tap((componentData: SprintViewComponentData) => {
          if (componentData.sprint.synchronisationKind !== null) {
            this.gridService.deleteColumns([GridColumnId.requirementVersionProjectName]);
          }
          if (componentData.sprint.sprintReqVersions[0]?.mode === ManagementMode.SYNCHRONIZED) {
            this.gridService.deleteColumns([GridColumnId.status]);
          } else {
            this.gridService.deleteColumns([GridColumnId.remoteReqStatusAndState]);
          }
        }),
      )
      .subscribe();
  }

  private initializeFilters() {
    const filters: GridFilter[] = buildFilters([
      textResearchFilter(
        GridColumnId.requirementVersionProjectName,
        ResearchColumnPrototype.TEST_CASE_PROJECT_NAME,
      ).alwaysActive(),
      textResearchFilter(
        GridColumnId.reference,
        ResearchColumnPrototype.REQUIREMENT_VERSION_REFERENCE,
      ).alwaysActive(),
      textResearchFilter(GridColumnId.name, ResearchColumnPrototype.TEST_CASE_NAME).alwaysActive(),
      textResearchFilter(
        GridColumnId.categoryLabel,
        ResearchColumnPrototype.REQUIREMENT_VERSION_CATEGORY_LABEL,
      ).alwaysActive(),
      i18nEnumResearchFilter(
        GridColumnId.criticality,
        ResearchColumnPrototype.REQUIREMENT_CRITICALITY,
      ).alwaysActive(),
      i18nEnumResearchFilter(
        GridColumnId.status,
        ResearchColumnPrototype.REQUIREMENT_STATUS,
      ).alwaysActive(),
      i18nEnumResearchFilter(
        GridColumnId.validationStatus,
        ResearchColumnPrototype.VALIDATION_STATUS,
      ).alwaysActive(),
    ]);
    this.gridService.addFilters(filters);
  }

  private setScope() {
    this.componentData$
      .pipe(
        takeUntil(this.unsub$),
        withLatestFrom(this.gridService.scope$),
        tap(([sprintView, scope]: [SprintViewComponentData, Scope]) => {
          const projectIds: number[] = sprintView.sprint.sprintReqVersions.map(
            (req) => req.projectId,
          );
          const values: EntityScope[] = projectIds.map((projectId) => {
            return { id: '', label: '', projectId: projectId };
          });
          const scopeToAdd = { ...scope, value: values };
          this.gridService.changeScope(scopeToAdd);
        }),
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
