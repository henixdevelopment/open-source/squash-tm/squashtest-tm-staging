import { Milestone, SimpleUser, SqtmEntityState } from 'sqtm-core';
import { TestPlanStatistics } from '../../campaign-view/state/campaign.state';

export interface TestSuiteState extends SqtmEntityState {
  uuid: string;
  name: string;
  description: string;
  executionStatus: string;
  createdOn: Date;
  createdBy: string;
  lastModifiedOn: Date;
  lastModifiedBy: string;
  testPlanStatistics: TestPlanStatistics;
  nbIssues: number;
  users: SimpleUser[];
  milestones: Milestone[];
  hasDatasets: boolean;
  executionStatusMap: Map<number, string>;
  uiState: {
    openTestCaseTreePicker: boolean;
  };
  nbAutomatedSuites: number;
  nbTestPlanItems: number;
  iterationId: number;
}
