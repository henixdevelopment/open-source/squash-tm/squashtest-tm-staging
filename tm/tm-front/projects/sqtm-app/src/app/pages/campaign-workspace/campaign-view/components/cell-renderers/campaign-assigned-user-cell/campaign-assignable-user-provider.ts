import { Injectable } from '@angular/core';
import { ResearchColumnPrototype, UserHistorySearchProvider, UserListElement } from 'sqtm-core';
import { CampaignViewService } from '../../../service/campaign-view.service';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

@Injectable()
export class CampaignAssignableUsersProvider extends UserHistorySearchProvider {
  constructor(private campaignViewService: CampaignViewService) {
    super();
  }

  provideUserList(_columnPrototype: ResearchColumnPrototype): Observable<UserListElement[]> {
    return this.campaignViewService.componentData$.pipe(
      take(1),
      map((data) => data.campaign.users.map((u) => ({ ...u, selected: false }))),
    );
  }
}
