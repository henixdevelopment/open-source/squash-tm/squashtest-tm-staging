import { ChangeDetectionStrategy, Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  AutomationEnvironmentTagHolder,
  BoundEnvironmentVariable,
  DataRow,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
  SquashAutomProjectPreview,
} from 'sqtm-core';
import { EnvironmentSelectionPanelComponent } from '../../../../../components/squash-orchestrator/orchestrator-execution-environment/components/environment-selection-panel/environment-selection-panel.component';
import {
  OrchestratorExecutionEnvironmentVariablePanelComponent,
  EnvironmentVariableAssociationsTableDefinition,
  EV_ASSOCIATION_TABLE,
  EV_ASSOCIATION_TABLE_CONF,
} from '../../../../../components/squash-orchestrator/orchestrator-execution-environment-variable/components/orchestrator-execution-environment-variable-panel/orchestrator-execution-environment-variable-panel.component';

@Component({
  selector: 'sqtm-app-squash-autom-project-preview',
  templateUrl: './squash-autom-project-preview.component.html',
  styleUrls: ['./squash-autom-project-preview.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: EV_ASSOCIATION_TABLE_CONF,
      useFactory: EnvironmentVariableAssociationsTableDefinition,
    },
    {
      provide: EV_ASSOCIATION_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, EV_ASSOCIATION_TABLE_CONF, ReferentialDataService],
    },
  ],
})
export class SquashAutomProjectPreviewComponent implements OnInit {
  @Input()
  squashAutomProject: SquashAutomProjectPreview;

  @Input({ required: true })
  iterationId: number;

  @Input()
  formGroup: FormGroup;

  @ViewChild(EnvironmentSelectionPanelComponent)
  environmentPanel: EnvironmentSelectionPanelComponent;

  @ViewChild(OrchestratorExecutionEnvironmentVariablePanelComponent)
  environmentVariablePanel: OrchestratorExecutionEnvironmentVariablePanelComponent;

  formControl: FormControl;

  public readonly tagHolderType = AutomationEnvironmentTagHolder.EXECUTION_DIALOG;

  squashAutomTestCaseDataRows: Partial<DataRow>[];

  constructor() {
    this.formControl = new FormControl('');
  }

  ngOnInit(): void {
    this.formGroup.addControl(this.squashAutomProject.projectId.toString(), this.formControl);
    this.squashAutomTestCaseDataRows = this.squashAutomProject.testCases.map((testCase, index) => ({
      id: index,
      data: testCase,
    }));
  }

  updateSelectedTagsValue(tags: string[]) {
    this.formControl.setValue(tags);
  }

  areAllTestsLaunchable(): Observable<boolean> {
    return this.environmentPanel.isOneEnvironmentAvailable();
  }

  getNamespacesOfSelectedEnvironments(): Observable<{ projectId: number; namespaces: string[] }> {
    return this.environmentPanel.getNamespacesOfSelectedEnvironments().pipe(
      map((namespaces) => ({
        projectId: this.squashAutomProject.projectId,
        namespaces: namespaces,
      })),
    );
  }

  getBoundEnvironmentVariables(): Observable<BoundEnvironmentVariable[]> {
    return this.environmentVariablePanel.getBoundEnvironmentVariables();
  }
}
