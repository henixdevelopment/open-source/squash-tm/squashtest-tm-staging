import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { AbstractCellRendererComponent, GridService } from 'sqtm-core';

@Component({
  selector: 'sqtm-app-automated-suite-statistics-cell-renderer',
  template: ` @if (columnDisplay && row) {
    <div class="full-width full-height flex-column">
      <span
        style="text-align: center; margin: auto 5px; user-select: text;"
        [ngClass]="textClass"
        nz-tooltip="{{ row.data[this.columnDisplay.id] }}"
        [nzTooltipPlacement]="'topLeft'"
      >
        {{ displayedText }}
      </span>
    </div>
  }`,
  styleUrls: ['./automated-suite-statistics-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AutomatedSuiteStatisticsCellRendererComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }

  get textClass(): string {
    return 'align-' + this.columnDisplay.contentPosition;
  }

  get displayedText(): string {
    const value = this.row.data[this.columnDisplay.id];
    if (value == null) {
      return '-';
    } else if (value < 1000) {
      return `${value}`;
    } else if (value < 1000000000) {
      const abbreviations = ['', 'k', 'M', 'B'];
      const exponent = Math.floor(Math.log10(value) / 3);
      const formattedValue = (value / Math.pow(1000, exponent)).toFixed(1);

      return `${formattedValue} ${abbreviations[exponent]}`;
    } else {
      return `> 1 B`;
    }
  }
}
