import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewContainerRef,
} from '@angular/core';
import {
  AbstractCellRendererComponent,
  AttachmentListModel,
  ColumnDefinitionBuilder,
  DataRow,
  DialogService,
  GridColumnId,
  GridService,
  RestService,
} from 'sqtm-core';
import { AutomatedSuiteReportUrlDialogComponent } from '../../automated-suite-report-url-dialog/automated-suite-report-url-dialog.component';

@Component({
  selector: 'sqtm-app-automated-execution-report-renderer',
  template: `
    @if (row) {
      <div class="full-height full-width icon-container">
        @if (canShowIcon(row)) {
          <div
            class=" current-workspace-main-color __hover_pointer"
            (click)="showExecutionReport()"
          >
            <i
              nz-icon
              nzType="sqtm-core-campaign:exec_report"
              nzTheme="outline"
              class="table-icon-size"
            ></i>
          </div>
        } @else {
          <span>/</span>
        }
      </div>
    }
  `,
  styleUrls: ['./automated-execution-report-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AutomatedExecutionReportRendererComponent extends AbstractCellRendererComponent {
  constructor(
    gridService: GridService,
    cdRef: ChangeDetectorRef,
    private dialogService: DialogService,
    private restService: RestService,
    private vcr: ViewContainerRef,
  ) {
    super(gridService, cdRef);
  }

  canShowIcon(row: DataRow) {
    return (
      row.data[GridColumnId.hasResultUrl] ||
      row.data[GridColumnId.automatedSuiteHasAttachment] ||
      row.data[GridColumnId.executionExtenderHasAttachment]
    );
  }

  showExecutionReport() {
    this.restService
      .get<{
        reportUrls: string[];
        attachmentList: AttachmentListModel;
      }>(['automated-suite', this.row.data.suiteId, 'report-urls'])
      .subscribe((result) => {
        this.dialogService.openDialog({
          id: 'automated-suite-execution-report',
          component: AutomatedSuiteReportUrlDialogComponent,
          viewContainerReference: this.vcr,
          data: { reportUrls: result.reportUrls, attachmentList: result.attachmentList },
          height: 600,
          width: 800,
        });
      });
  }
}

export function automatedExecutionReportColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(AutomatedExecutionReportRendererComponent)
    .withHeaderPosition('center');
}
