import { Observable } from 'rxjs';
import { InjectionToken } from '@angular/core';
import { EntityReference } from 'sqtm-core';

export interface GenericTestPlanViewService {
  incrementAutomatedSuiteCount(): Observable<any>;
  getEntityReference(): Observable<EntityReference>;
  updateStateAfterExecutionDeletedInTestPlanItem(nbIssues: number): Observable<any>;
  getIterationId(): number;
}

export const GENERIC_TEST_PLAN_VIEW_SERVICE = new InjectionToken('GENERIC_TEST_PLAN_VIEW_SERVICE');
