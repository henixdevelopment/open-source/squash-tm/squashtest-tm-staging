import { buildDatasetIdsByTestCaseId } from './sprint-req-version-update-test-plan-dialog.component';
import { DataRow } from 'sqtm-core';

describe('SprintReqVersionUpdateTestPlanDialogComponent', () => {
  it('should build ID map from available test plan items', () => {
    expect(
      buildDatasetIdsByTestCaseId([
        { id: 1, data: { testCaseId: 1, datasetId: 1 } } as DataRow,
        { id: 2, data: { testCaseId: 1, datasetId: 2 } } as DataRow,
        { id: 3, data: { testCaseId: 2, datasetId: 3 } } as DataRow,
        { id: 4, data: { testCaseId: 2, datasetId: 4 } } as DataRow,
        { id: 4, data: { testCaseId: 3, datasetId: null } } as DataRow,
      ]),
    ).toEqual({
      1: [1, 2],
      2: [3, 4],
      3: [null],
    });
  });
});
