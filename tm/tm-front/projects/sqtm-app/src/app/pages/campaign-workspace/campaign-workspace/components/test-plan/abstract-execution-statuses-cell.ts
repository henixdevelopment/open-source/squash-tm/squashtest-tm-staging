import {
  AbstractListCellRendererComponent,
  ActionErrorDisplayService,
  AuthenticatedUser,
  ExecutionSummaryDto,
  ExecutionStatus,
  ExecutionStatusKeys,
  formatFullUserName,
  getSupportedBrowserLang,
  GridColumnId,
  GridService,
  ListPanelItem,
  ProjectData,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {
  ChangeDetectorRef,
  Directive,
  ElementRef,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { filter, map, take, takeUntil } from 'rxjs/operators';
import { formatDate } from '@angular/common';

@Directive()
export abstract class AbstractExecutionStatusesCell extends AbstractListCellRendererComponent {
  @ViewChild('templatePortalContent', { read: TemplateRef })
  templatePortalContent: TemplateRef<any>;

  @ViewChild('executionStatus', { read: ElementRef })
  executionStatus: ElementRef;

  overlayRef: OverlayRef;

  readonly authenticatedUser$: Observable<AuthenticatedUser>;

  abstract canEdit$: Observable<boolean>;

  abstract panelItems$: Observable<ListPanelItem[]>;

  maxLength$: Observable<number>;

  protected constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    public readonly overlay: Overlay,
    public readonly vcr: ViewContainerRef,
    public readonly translateService: TranslateService,
    public readonly restService: RestService,
    public readonly actionErrorDisplayService: ActionErrorDisplayService,
    public referentialDataService: ReferentialDataService,
  ) {
    super(grid, cdRef, overlay, vcr, translateService, restService, actionErrorDisplayService);

    this.authenticatedUser$ = this.referentialDataService.authenticatedUser$.pipe(
      takeUntil(this.unsub$),
    );

    this.maxLength$ = this.grid.dataRows$.pipe(
      map((dataRows) =>
        Object.keys(dataRows).map(
          (key) => dataRows[key].data.lastExecutionStatuses as ExecutionSummaryDto[],
        ),
      ),
      map((summariesTab: ExecutionSummaryDto[][]) =>
        summariesTab.reduce((max, summaries) => Math.max(max, summaries.length), 0),
      ),
    );
  }
  get lastStatus(): ExecutionStatusKeys {
    return this.row.data[this.columnDisplay.id];
  }

  get lastStatusColor(): string {
    return ExecutionStatus[this.lastStatus].color;
  }

  get lastStatusI18Key(): string {
    return ExecutionStatus[this.lastStatus].i18nKey;
  }

  get historyStatusKeys(): ExecutionSummaryDto[] {
    return this.row.data[GridColumnId.lastExecutionStatuses];
  }

  protected getColor(id: number): string {
    return ExecutionStatus[this.historyStatusKeys[id].executionStatus].color;
  }

  protected getExecutionStatusTooltip(id: number): string {
    const executionStatus: ExecutionSummaryDto = this.historyStatusKeys[id];
    const status: string = this.translateService.instant(
      ExecutionStatus[executionStatus.executionStatus].i18nKey,
    );

    if (executionStatus.executedOn != null) {
      const date: string = formatDate(
        executionStatus.executedOn,
        'short',
        getSupportedBrowserLang(this.translateService),
      );
      return `${status} ${this.translateService.instant('sqtm-core.generic.label.on')} ${date}`;
    }

    return status;
  }

  public showExecutionStatusList() {
    this.canEdit$
      .pipe(
        take(1),
        filter((canEdit) => canEdit),
      )
      .subscribe(() => {
        this.showList(this.executionStatus, this.templatePortalContent, [
          {
            originX: 'start',
            overlayX: 'start',
            originY: 'bottom',
            overlayY: 'top',
            offsetX: -10,
            offsetY: 6,
          },
          {
            originX: 'start',
            overlayX: 'start',
            originY: 'top',
            overlayY: 'bottom',
            offsetX: -10,
            offsetY: -6,
          },
        ]);
      });
  }

  protected getFilteredExecutionStatusKeys(projectData: ProjectData): string[] {
    const filteredExecutionStatusKeys = ['READY', 'RUNNING', 'SUCCESS', 'FAILURE', 'BLOCKED'];

    if (this.isExecutionStatusEnabled('SETTLED', projectData)) {
      filteredExecutionStatusKeys.push('SETTLED');
    }

    if (this.isExecutionStatusEnabled('UNTESTABLE', projectData)) {
      filteredExecutionStatusKeys.push('UNTESTABLE');
    }

    return filteredExecutionStatusKeys;
  }

  protected isExecutionStatusEnabled(statusKey: string, projectData: ProjectData): boolean {
    return !projectData.disabledExecutionStatus.includes(statusKey as any);
  }

  protected getUser(user: AuthenticatedUser): string {
    return formatFullUserName(user);
  }

  protected asListItemOptions(statuses: string[]): ListPanelItem[] {
    return statuses.map((key) => ({
      id: ExecutionStatus[key].id,
      label: this.translateService.instant(ExecutionStatus[key].i18nKey),
      icon: ExecutionStatus[key].icon,
      color: ExecutionStatus[key].color,
    }));
  }
}
