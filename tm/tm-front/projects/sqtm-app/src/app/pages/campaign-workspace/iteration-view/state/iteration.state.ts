import {
  CustomDashboardModel,
  Milestone,
  NamedReference,
  SimpleUser,
  SqtmEntityState,
  StatisticsBundle,
} from 'sqtm-core';
import { TestPlanStatistics } from '../../campaign-view/state/campaign.state';

export interface IterationState extends SqtmEntityState {
  name: string;
  reference: string;
  description: string;
  uuid: string;
  iterationStatus: string;
  createdOn: Date;
  createdBy: string;
  lastModifiedOn: Date;
  lastModifiedBy: string;
  testPlanStatistics: TestPlanStatistics;
  actualStartDate: Date;
  actualEndDate: Date;
  actualStartAuto: boolean;
  actualEndAuto: boolean;
  scheduledStartDate: Date;
  scheduledEndDate: Date;
  hasDatasets: boolean;
  executionStatusMap: Map<number, string>;
  uiState: {
    openTestCaseTreePicker: boolean;
  };
  nbIssues: number;
  users: SimpleUser[];
  milestones: Milestone[];
  testSuites: NamedReference[];
  iterationStatisticsBundle?: StatisticsBundle;
  nbAutomatedSuites: number;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
  dashboard?: CustomDashboardModel;
  nbTestPlanItems: number;
}
