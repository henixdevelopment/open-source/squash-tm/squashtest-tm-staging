import { campaignWorkspaceLogger } from '../campaign-workspace.logger';

export const campaignFolderViewLogger = campaignWorkspaceLogger.compose('campaign-folder-view');
