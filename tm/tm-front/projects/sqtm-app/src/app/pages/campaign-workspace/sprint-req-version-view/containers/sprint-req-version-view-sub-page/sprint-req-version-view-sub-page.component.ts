import { ChangeDetectionStrategy, Component, computed, Signal } from '@angular/core';
import {
  ActionErrorDisplayService,
  CampaignPermissions,
  CapsuleInformationData,
  EntityViewComponentData,
  LevelEnumItem,
  ReferentialDataService,
  ReferentialDataState,
  SprintReqVersionValidationStatus,
  SprintReqVersionValidationStatusKeys,
} from 'sqtm-core';
import { SprintReqVersionViewService } from '../../service/sprint-req-version-view.service';
import { SprintReqVersionState } from '../../../../requirement-workspace/requirement-version-view/state/sprint-req-version.state';
import { toSignal } from '@angular/core/rxjs-interop';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-sprint-req-version-view-sub-page',
  templateUrl: './sprint-req-version-view-sub-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SprintReqVersionViewSubPageComponent {
  $referentialData: Signal<ReferentialDataState>;
  $componentData: Signal<SprintReqVersionViewComponentData>;
  $validationStatusData: Signal<CapsuleInformationData>;

  protected readonly SprintReqVersionValidationStatus = SprintReqVersionValidationStatus;

  constructor(
    private sprintReqVersionViewService: SprintReqVersionViewService,
    private referentialDataService: ReferentialDataService,
    private actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    this.$componentData = toSignal(this.sprintReqVersionViewService.componentData$);
    this.$referentialData = toSignal(this.referentialDataService.referentialData$);

    this.$validationStatusData = computed(() => {
      const $validationStatus =
        SprintReqVersionValidationStatus[this.$componentData().sprintReqVersion.validationStatus];

      return {
        id: $validationStatus.id,
        icon: $validationStatus.icon,
        labelI18nKey: $validationStatus.i18nKey,
        titleI18nKey: 'sqtm-core.sprint.req-version.validation-status.title',
        color: $validationStatus.color,
      };
    });
  }

  updateValidationStatus(newValidationStatus: LevelEnumItem<SprintReqVersionValidationStatusKeys>) {
    this.sprintReqVersionViewService
      .updateValidationStatus(newValidationStatus.id)
      .pipe(catchError((err) => this.actionErrorDisplayService.handleActionError(err)))
      .subscribe();
  }
}

export interface SprintReqVersionViewComponentData
  extends EntityViewComponentData<SprintReqVersionState, 'sprintReqVersion', CampaignPermissions> {}
