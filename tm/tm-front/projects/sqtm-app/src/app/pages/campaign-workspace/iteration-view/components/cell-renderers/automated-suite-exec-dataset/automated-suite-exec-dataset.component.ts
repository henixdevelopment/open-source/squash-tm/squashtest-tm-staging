import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-automated-suite-exec-dataset',
  template: ` @if (columnDisplay && row) {
    <div class="flex full-width full-height">
      <span
        nz-tooltip
        class="sqtm-grid-cell-txt-renderer m-auto-0"
        [sqtmCoreLabelTooltip]="dataSet"
      >
        {{ dataSet }}
      </span>
    </div>
  }`,
  styleUrls: ['./automated-suite-exec-dataset.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AutomatedSuiteExecDatasetComponent extends AbstractCellRendererComponent {
  constructor(
    gridService: GridService,
    cdrRef: ChangeDetectorRef,
    private translateService: TranslateService,
  ) {
    super(gridService, cdrRef);
  }

  get dataSet() {
    if (this.row.data[this.columnDisplay.id] == null) {
      return this.translateService.instant(
        'sqtm-core.campaign-workspace.test-plan.label.dataset.none',
      );
    } else {
      return this.row.data[this.columnDisplay.id];
    }
  }
}

export function automatedSuiteDatasetColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(AutomatedSuiteExecDatasetComponent);
}
