import {
  dateTimeColumn,
  executionStatusColumn,
  Fixed,
  grid,
  GridColumnId,
  GridDefinition,
  GridId,
  indexColumn,
  Limited,
  LocalPersistenceService,
  Sort,
  sortDate,
  textColumn,
  deleteColumn,
  DataRow,
  ProjectDataMap,
  SquashTmDataRowType,
  convertSqtmLiterals,
} from 'sqtm-core';
import { automatedExecutionDetailsColumn } from './iteration-view/components/cell-renderers/automated-execution-details-renderer/automated-execution-details-renderer.component';
import { automatedExecutionReportColumn } from './iteration-view/components/cell-renderers/automated-execution-report-renderer/automated-execution-report-renderer.component';
import { AutomatedSuiteStatisticsCellRendererComponent } from './iteration-view/components/cell-renderers/automated-suite-statistics-cell-renderer/automated-suite-statistics-cell-renderer.component';
import { automatedSuiteStopWorkflowsColumn } from './iteration-view/components/cell-renderers/automated-suite-stop-workflows/automated-suite-stop-workflows.component';
import { DeleteAutomatedSuiteComponent } from './iteration-view/components/cell-renderers/delete-automated-suite/delete-automated-suite.component';
import { automatedSuitePruneColumn } from './iteration-view/components/cell-renderers/automated-suite-attachment-prune/automated-suite-attachment-prune.component';
import { automatedSuiteRealTimeWorkflowWiewer } from './iteration-view/components/cell-renderers/automated-suite-workflow-viewer-renderer/automated-suite-workflow-viewer-renderer.component';

function automatedSuiteConverter(
  literals: Partial<DataRow>[],
  projectDataMap: ProjectDataMap,
): DataRow[] {
  const automatedSuites = literals.map((li) => ({
    ...li,
    type: SquashTmDataRowType.AutomatedSuite,
  }));
  return convertSqtmLiterals(automatedSuites, projectDataMap);
}

export function automatedSuiteTableDefinition(
  gridId: GridId,
  localPersistenceService: LocalPersistenceService,
): GridDefinition {
  return grid(gridId)
    .withColumns([
      indexColumn().withViewport('leftViewport'),
      dateTimeColumn(GridColumnId.createdOn)
        .withI18nKey('sqtm-core.entity.generic.created-on.feminine')
        .changeWidthCalculationStrategy(new Limited(140))
        .withAssociatedFilter(),
      executionStatusColumn(GridColumnId.executionStatus)
        .withI18nKey('sqtm-core.entity.execution.status.label')
        .changeWidthCalculationStrategy(new Limited(100))
        .withAssociatedFilter(),
      automatedExecutionDetailsColumn(GridColumnId.executionDetails)
        .withI18nKey('sqtm-core.campaign-workspace.automated-suite.label.details')
        .changeWidthCalculationStrategy(new Limited(120))
        .disableSort(),
      automatedExecutionReportColumn(GridColumnId.executionReport)
        .withI18nKey('sqtm-core.campaign-workspace.automated-suite.label.report')
        .changeWidthCalculationStrategy(new Limited(120))
        .disableSort(),
      textColumn(GridColumnId.createdBy)
        .withI18nKey('sqtm-core.grid.header.created-by.feminine')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Limited(120)),
      textColumn(GridColumnId.launchedFrom)
        .withI18nKey('sqtm-core.campaign-workspace.automated-suite.label.launched-from')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Limited(120)),
      dateTimeColumn(GridColumnId.lastModifiedOn)
        .withSortFunction(sortDate)
        .withI18nKey('sqtm-core.entity.generic.last-modified-on.feminine')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Limited(140)),
      textColumn(GridColumnId.nbStatusTotal)
        .withI18nKey('sqtm-core.campaign-workspace.automated-suite.label.nb-itpi')
        .withRenderer(AutomatedSuiteStatisticsCellRendererComponent)
        .disableSort()
        .withHeaderPosition('center')
        .withContentPosition('center')
        .changeWidthCalculationStrategy(new Limited(70)),
      textColumn(GridColumnId.nbStatusSuccess)
        .withI18nKey('sqtm-core.campaign-workspace.automated-suite.label.nb-success')
        .withRenderer(AutomatedSuiteStatisticsCellRendererComponent)
        .disableSort()
        .withHeaderPosition('center')
        .withContentPosition('center')
        .changeWidthCalculationStrategy(new Limited(70)),
      textColumn(GridColumnId.nbStatusFailure)
        .withI18nKey('sqtm-core.campaign-workspace.automated-suite.label.nb-failure')
        .withRenderer(AutomatedSuiteStatisticsCellRendererComponent)
        .disableSort()
        .withHeaderPosition('center')
        .withContentPosition('center')
        .changeWidthCalculationStrategy(new Limited(70)),
      textColumn(GridColumnId.nbStatusOther)
        .withI18nKey('sqtm-core.campaign-workspace.automated-suite.label.nb-other')
        .withRenderer(AutomatedSuiteStatisticsCellRendererComponent)
        .disableSort()
        .withHeaderPosition('center')
        .withContentPosition('center')
        .changeWidthCalculationStrategy(new Limited(70)),
      textColumn(GridColumnId.environmentTags)
        .withI18nKey('sqtm-core.entity.server.environment.label.plural')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Limited(200)),
      textColumn(GridColumnId.environmentVariables)
        .withI18nKey(
          'sqtm-core.administration-workspace.entities-customization.environment-variables.title',
        )
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Limited(200)),
      automatedSuiteStopWorkflowsColumn(GridColumnId.stopWorkflows)
        .disableHeader()
        .disableSort()
        .changeWidthCalculationStrategy(new Fixed(40))
        .withViewport('rightViewport'),
      deleteColumn(DeleteAutomatedSuiteComponent).withViewport('rightViewport'),
      automatedSuitePruneColumn(GridColumnId.attachmentPrune)
        .disableHeader()
        .disableSort()
        .changeWidthCalculationStrategy(new Fixed(40))
        .withViewport('rightViewport'),
      automatedSuiteRealTimeWorkflowWiewer(GridColumnId.workflowViewer)
        .disableHeader()
        .disableSort()
        .changeWidthCalculationStrategy(new Fixed(40))
        .withViewport('rightViewport'),
    ])
    .server()
    .withInitialSortedColumns([{ id: GridColumnId.createdOn, sort: Sort.DESC }])
    .withRowConverter(automatedSuiteConverter)
    .disableRightToolBar()
    .withRowHeight(35)
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
}
