import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TestSuiteTpiMultiEditDialogComponent } from './test-suite-tpi-multi-edit-dialog.component';
import {
  DataRow,
  DialogReference,
  ProjectDataMap,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {
  mockReferentialDataService,
  mockRestService,
} from '../../../../../utils/testing-utils/mocks.service';
import { Observable, of } from 'rxjs';
import { ItpiMultiEditDialogConfiguration } from '../../../iteration-view/components/itpi-multi-edit-dialog/itpi-multi-edit-dialog.configuration';
import { TranslateModule } from '@ngx-translate/core';
import { TestSuiteViewService } from '../../services/test-suite-view.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TestSuiteViewComponentData } from '../../containers/test-suite-view/test-suite-view.component';
import SpyObj = jasmine.SpyObj;

describe('TestSuiteTpiMultiEditDialogComponent', () => {
  let component: TestSuiteTpiMultiEditDialogComponent;
  let fixture: ComponentFixture<TestSuiteTpiMultiEditDialogComponent>;
  let referentialDataService: SpyObj<ReferentialDataService>;
  let dialogRef: SpyObj<DialogReference>;
  let restService: SpyObj<RestService>;
  let testSuiteViewService: SpyObj<TestSuiteViewService>;

  beforeEach(waitForAsync(() => {
    referentialDataService = mockReferentialDataService();
    referentialDataService.projectDatas$ = of({
      1: { disabledExecutionStatus: [] },
    } as unknown as ProjectDataMap);

    const data: ItpiMultiEditDialogConfiguration = {
      id: 'anything',
      rows: [
        {
          id: 50,
          data: {
            itemTestPlanId: 50,
            projectId: 1,
          },
          projectId: 1,
        } as unknown as DataRow,
        {
          id: 51,
          data: {
            itemTestPlanId: 51,
            projectId: 1,
          },
          projectId: 1,
        } as unknown as DataRow,
      ],
      titleKey: 'title',
    };

    const executionStatusMap = {
      50: 'READY',
      51: 'READY',
    };

    testSuiteViewService = jasmine.createSpyObj<TestSuiteViewService>([
      'refreshExecutionStatusMapAfterMassEdit',
    ]);
    testSuiteViewService.componentData$ = of({
      testSuite: {
        users: [{ id: 12, login: 'admin', lastName: 'admin' }],
        executionStatusMap: new Map(Object.entries(executionStatusMap)),
      },
    }) as unknown as Observable<TestSuiteViewComponentData>;
    testSuiteViewService.refreshExecutionStatusMapAfterMassEdit.and.returnValue(of(null));

    dialogRef = jasmine.createSpyObj(['close']);
    (dialogRef as any).data = data;

    restService = mockRestService();

    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      providers: [
        {
          provide: ReferentialDataService,
          useValue: referentialDataService,
        },
        {
          provide: DialogReference,
          useValue: dialogRef,
        },
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: TestSuiteViewService,
          useValue: testSuiteViewService,
        },
      ],
      declarations: [TestSuiteTpiMultiEditDialogComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  describe('with all statuses enabled', () => {
    beforeEach(waitForAsync(() => {
      fixture = TestBed.createComponent(TestSuiteTpiMultiEditDialogComponent);
      component = fixture.componentInstance;
      component.assigneeField = {} as any;
      component.statusField = {} as any;
      fixture.detectChanges();
    }));

    it('should create', waitForAsync(() => {
      expect(component).toBeTruthy();
      expect(component.assigneeField.selectedValue).toBe('UNASSIGNED');
      expect(component.statusOptions.length).toBe(7);
    }));

    it('should confirm and close', waitForAsync(() => {
      component.assigneeField.check = true;
      component.assigneeField.selectedValue = 'toto';
      component.statusField.check = true;
      component.statusField.selectedValue = 'READY';

      const expectedPath = ['test-suite/test-plan', '50,51', 'mass-update'];
      const expectedPayload = {
        assignee: 'toto',
        executionStatus: 'READY',
        changeAssignee: true,
      };

      component.confirm();
      expect(restService.post).toHaveBeenCalledWith(expectedPath, expectedPayload);
      expect(dialogRef.close).toHaveBeenCalled();
    }));

    it('should not send request and close when confirmed with no field checked', waitForAsync(() => {
      component.assigneeField.check = false;
      component.statusField.check = false;
      component.confirm();
      expect(restService.post).not.toHaveBeenCalled();
      expect(dialogRef.close).toHaveBeenCalled();
    }));
  });

  it('should create with restricted status options', waitForAsync(() => {
    referentialDataService.projectDatas$ = of({
      1: { id: 1, disabledExecutionStatus: ['UNTESTABLE', 'SETTLED'] },
      2: { id: 2, disabledExecutionStatus: [] },
    } as unknown as ProjectDataMap);

    fixture = TestBed.createComponent(TestSuiteTpiMultiEditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(component).toBeTruthy();
    expect(component.statusOptions.length).toBe(5);
  }));
});
