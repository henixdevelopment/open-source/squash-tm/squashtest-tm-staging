import { DialogConfiguration, IterationPlanning } from 'sqtm-core';
import {
  ITERATION_PLANNING_DIALOG_ID,
  IterationPlanningDialogComponent,
} from './iteration-planning-dialog.component';

export interface IterationPlanningDialogConfiguration {
  iterationPlannings: IterationPlanning[];
}

export const baseIterationPlanningDialogConfiguration: DialogConfiguration = {
  id: ITERATION_PLANNING_DIALOG_ID,
  width: 700,
  component: IterationPlanningDialogComponent,
  viewContainerReference: null,
  data: null,
};
