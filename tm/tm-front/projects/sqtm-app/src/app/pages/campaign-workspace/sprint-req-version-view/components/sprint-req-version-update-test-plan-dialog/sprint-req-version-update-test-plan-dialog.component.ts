import { ChangeDetectionStrategy, Component, InjectionToken } from '@angular/core';
import {
  ActionErrorDisplayService,
  AvailableTestPlanItemModel,
  DataRow,
  DialogReference,
  Fixed,
  grid,
  GridColumnId,
  GridDefinition,
  GridId,
  GridService,
  gridServiceFactory,
  Limited,
  ReferentialDataService,
  RestService,
  selectRowColumn,
  StyleDefinitionBuilder,
  textColumn,
  ToggleSelectionHeaderRendererComponent,
  withLinkColumn,
} from 'sqtm-core';
import { of, switchMap } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';
import { toSignal } from '@angular/core/rxjs-interop';

const AVAILABLE_ITEMS_TABLE_CONF = new InjectionToken('AVAILABLE_ITEMS_TABLE_CONF');
const AVAILABLE_ITEMS_TABLE = new InjectionToken('AVAILABLE_ITEMS_TABLE');

export function availableItemsTableDefinition(): GridDefinition {
  return grid(GridId.AVAILABLE_TEST_PLAN_ITEMS)
    .withColumns([
      selectRowColumn()
        .changeWidthCalculationStrategy(new Fixed(40))
        .withHeaderRenderer(ToggleSelectionHeaderRendererComponent),
      textColumn(GridColumnId.testCaseReference)
        .withI18nKey('sqtm-core.entity.generic.reference.label')
        .changeWidthCalculationStrategy(new Limited(100)),
      withLinkColumn(GridColumnId.testCaseName, {
        kind: 'link',
        createUrlFunction: (row: DataRow) =>
          `/test-case-workspace/test-case/detail/${row.data.testCaseId}`,
        target: '_blank',
      })
        .withI18nKey('sqtm-core.entity.test-case.label.singular')
        .changeWidthCalculationStrategy(new Limited(150)),
      textColumn(GridColumnId.datasetName)
        .withI18nKey('sqtm-core.entity.dataset.label.short')
        .withTitleI18nKey('sqtm-core.entity.dataset.label.singular')
        .changeWidthCalculationStrategy(new Limited(100)),
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .disableRightToolBar()
    .build();
}

@Component({
  selector: 'sqtm-app-sprint-req-version-update-test-plan-dialog',
  templateUrl: './sprint-req-version-update-test-plan-dialog.component.html',
  styleUrl: './sprint-req-version-update-test-plan-dialog.component.less',
  providers: [
    {
      provide: AVAILABLE_ITEMS_TABLE_CONF,
      useFactory: availableItemsTableDefinition,
    },
    {
      provide: AVAILABLE_ITEMS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, AVAILABLE_ITEMS_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: AVAILABLE_ITEMS_TABLE,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SprintReqVersionUpdateTestPlanDialogComponent {
  $canConfirm = toSignal(this.gridService.selectedRows$.pipe(map((rows) => rows.length > 0)));

  constructor(
    private readonly gridService: GridService,
    public readonly dialogReference: DialogReference<SprintReqVersionUpdateTestPlanDialogData>,
    private readonly restService: RestService,
    private readonly actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    gridService.connectToDatasource(
      of(
        dialogReference.data.availableItems.map((item, index) => ({
          ...item,
          id: index,
        })),
      ),
      'id',
    );
  }

  handleConfirm() {
    if (!this.$canConfirm()) {
      return;
    }

    const url = [
      'sprint-req-version',
      this.dialogReference.data.sprintReqVersionId.toString(),
      'test-plan-items',
    ];

    this.gridService.selectedRows$
      .pipe(
        take(1),
        map((rows) => buildDatasetIdsByTestCaseId(rows)),
        switchMap((datasetIdsByTestCaseId) =>
          this.restService.post(url, {
            datasetIdsByTestCaseId,
          }),
        ),
        catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
      )
      .subscribe(() => {
        this.dialogReference.result = true;
        this.dialogReference.close();
      });
  }
}

export interface SprintReqVersionUpdateTestPlanDialogData {
  availableItems: AvailableTestPlanItemModel[];
  sprintReqVersionId: number;
}

// Exported for testing purposes
export function buildDatasetIdsByTestCaseId(rows: DataRow[]): Record<number, number[]> {
  return rows.reduce((acc, row) => {
    const testCaseId = row.data.testCaseId;
    const datasetId = row.data.datasetId;

    if (acc[testCaseId]) {
      acc[testCaseId].push(datasetId);
    } else {
      acc[testCaseId] = [datasetId];
    }

    return acc;
  }, {});
}
