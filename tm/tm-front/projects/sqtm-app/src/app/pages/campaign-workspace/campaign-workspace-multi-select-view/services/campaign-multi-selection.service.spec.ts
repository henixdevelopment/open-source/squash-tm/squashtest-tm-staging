import { TestBed } from '@angular/core/testing';

import { CampaignMultiSelectionService } from './campaign-multi-selection.service';
import { AppTestingUtilsModule } from '../../../../utils/testing-utils/app-testing-utils.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('CampaignMultiSelectionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, TranslateModule.forRoot()],
      providers: [CampaignMultiSelectionService],
    });
  });

  it('should be created', () => {
    const service: CampaignMultiSelectionService = TestBed.get(CampaignMultiSelectionService);
    expect(service).toBeTruthy();
  });
});
