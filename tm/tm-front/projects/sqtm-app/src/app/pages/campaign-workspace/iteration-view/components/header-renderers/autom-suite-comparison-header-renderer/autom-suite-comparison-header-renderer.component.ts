import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ComponentRef,
  Input,
} from '@angular/core';
import {
  AbstractFilterWidget,
  AbstractHeaderRendererComponent,
  ColumnDisplay,
  ColumnWithFilter,
  GridDisplay,
  GridService,
  Sort,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-autom-suite-comparison-date-header-renderer',
  templateUrl: './autom-suite-comparison-header-renderer.component.html',
  styleUrls: ['./autom-suite-comparison-header-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AutomSuiteComparisonHeaderRendererComponent extends AbstractHeaderRendererComponent {
  protected _columnDisplay: ColumnWithFilter;

  public get columnDisplay(): ColumnWithFilter {
    return this._columnDisplay;
  }

  @Input()
  public set columnDisplay(value: ColumnWithFilter) {
    this._columnDisplay = value;
    if (this.componentRef) {
      this.componentRef.instance.setFilter(value.filter, this.gridDisplay.scope);
    }
  }

  private componentRef: ComponentRef<AbstractFilterWidget>;

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    public translateService: TranslateService,
  ) {
    super(grid, cdRef);
  }

  toggleSort() {
    if (this._columnDisplay.sortable) {
      this.grid.toggleColumnSort(this._columnDisplay.id);
    }
  }

  shouldDisplaySortIcon(column: ColumnDisplay) {
    return column.sort !== Sort.NO_SORT;
  }

  iconName(column: ColumnDisplay) {
    return column.sort === Sort.ASC ? 'arrow-up' : 'arrow-down';
  }

  getStatusKind(status: any): string {
    const statusKey = Object.keys(status)[0];
    const labels = {
      nbStatusSuccess: 'sqtm-core.campaign-workspace.automated-suite.label.nb-success',
      nbStatusFailed: 'sqtm-core.campaign-workspace.automated-suite.label.nb-failure',
      nbStatusOther: 'sqtm-core.campaign-workspace.automated-suite.label.nb-other',
    };
    const labelKey = labels[statusKey];
    if (labelKey) {
      const label = this.translateService.instant(labelKey);
      const nbStatusValue =
        status[Object.keys(status)[0]] !== undefined ? status[Object.keys(status)[0]] : '0';
      return `${label} : ${nbStatusValue}`;
    }
    return '';
  }

  getBackgroundColor(status: any): string {
    const statusKey = Object.keys(status)[0];
    const colorMap = {
      nbStatusSuccess: 'green',
      nbStatusFailed: 'red',
      nbStatusOther: '#faad14',
    };
    return colorMap[statusKey] || 'transparent';
  }

  isStatusPresentInAutomSuite(status: any): boolean {
    const statusNumber = Object.values(status)[0];
    return statusNumber !== undefined;
  }

  getRoundNbStatusValue(status: any): string {
    const value = status[Object.keys(status)[0]];
    if (value === undefined) {
      return '-';
    } else if (value < 1000) {
      return `${value}`;
    } else if (value < 1000000000) {
      const abbreviations = ['', 'k', 'M', 'B'];
      const exponent = Math.floor(Math.log10(value) / 3);
      const formattedValue = Math.round(value / Math.pow(1000, exponent));
      return `${formattedValue}${abbreviations[exponent]}`;
    } else {
      return `>1B`;
    }
  }

  calculateRowHeight(gridDisplay: GridDisplay): string {
    return `${gridDisplay.rowHeight}px`;
  }
}
