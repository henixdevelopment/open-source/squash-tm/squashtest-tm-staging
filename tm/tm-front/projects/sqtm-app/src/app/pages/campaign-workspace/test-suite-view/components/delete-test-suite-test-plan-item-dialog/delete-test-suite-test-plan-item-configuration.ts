import { ConfirmDeleteLevel } from 'sqtm-core';

export class DeleteTestSuiteTestPlanItemConfiguration {
  id: string;
  titleKey: string;
  level: ConfirmDeleteLevel;
  messageKey: string;
  testSuiteId: number;
  itemTestPlanIds: number[];
}
