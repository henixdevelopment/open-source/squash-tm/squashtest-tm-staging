import { Injectable } from '@angular/core';
import {
  DataRow,
  GridService,
  UserHistorySearchProvider,
  UserListElement,
  ResearchColumnPrototype,
} from 'sqtm-core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Dictionary } from '@ngrx/entity/src/models';

@Injectable()
export class TestSuiteAutomatedSuiteProvider extends UserHistorySearchProvider {
  constructor(private gridService: GridService) {
    super();
  }

  provideUserList(_columnPrototype: ResearchColumnPrototype): Observable<UserListElement[]> {
    return this.gridService.dataRows$.pipe(
      map((rows: Dictionary<DataRow>) => Object.values(rows)),
      map((dataRows: DataRow[]) => this.mapToUserListElement(dataRows)),
    );
  }

  private mapToUserListElement(dataRows: DataRow[]) {
    return dataRows.reduce((users: UserListElement[], row: DataRow) => {
      const user: UserListElement = { id: row.data.id, login: row.data.createdBy, selected: false };
      if (!users.find((u) => u.login === user.login)) {
        users.push(user);
      }
      return users;
    }, []);
  }
}
