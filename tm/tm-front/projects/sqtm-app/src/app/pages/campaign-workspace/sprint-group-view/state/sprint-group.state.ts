import { SqtmEntityState } from 'sqtm-core';

export interface SprintGroupState extends SqtmEntityState {
  name: string;
  description: string;
  createdOn: Date;
  createdBy: string;
  lastModifiedOn: Date;
  lastModifiedBy: string;
}
