import {
  entitySelector,
  SimpleUser,
  SprintReqVersionModel,
  SprintStatusKeys,
  SqtmEntityState,
  SynchronizationPluginId,
} from 'sqtm-core';
import { createSelector } from '@ngrx/store';
import { SprintViewState } from './sprint-view.state';

export interface SprintState extends SqtmEntityState {
  name: string;
  reference: string;
  createdOn: Date;
  createdBy: string;
  description?: string;
  startDate?: string;
  endDate?: string;
  lastModifiedOn: Date;
  lastModifiedBy: string;
  uiState: {
    openRequirementTreePicker: boolean;
  };
  sprintReqVersions: SprintReqVersionModel[];
  nbSprintReqVersions: number;
  status: SprintStatusKeys;
  remoteState?: SprintStatusKeys;
  synchronisationKind?: SynchronizationPluginId;
  nbIssues: number;
  nbTestPlanItems: number;
  assignableUsers: SimpleUser[];
}

export const sprintReqVersionsSelector = createSelector<
  SprintViewState,
  [SprintState],
  SprintReqVersionModel[]
>(entitySelector, (sprint: SprintState) => {
  return sprint.sprintReqVersions;
});
