import {
  ChangeDetectionStrategy,
  Component,
  computed,
  Input,
  OnInit,
  Signal,
  signal,
  ViewChild,
  WritableSignal,
} from '@angular/core';
import {
  ActionErrorDisplayService,
  EditableSelectLevelEnumFieldComponent,
  InfoListItem,
  LevelEnumItem,
  ManagementMode,
  SprintReqVersionValidationStatusKeys,
  SprintStatus,
} from 'sqtm-core';
import { SprintReqVersionViewComponentData } from '../../containers/sprint-req-version-view-sub-page/sprint-req-version-view-sub-page.component';
import { SprintReqVersionState } from '../../../../requirement-workspace/requirement-version-view/state/sprint-req-version.state';
import { TranslateService } from '@ngx-translate/core';

import { SprintReqVersionViewService } from '../../service/sprint-req-version-view.service';
import { catchError, finalize } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-sprint-req-version-information-panel',
  templateUrl: './sprint-req-version-information-panel.component.html',
  styleUrl: './sprint-req-version-information-panel.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SprintReqVersionInformationPanelComponent implements OnInit {
  @Input()
  componentData: SprintReqVersionViewComponentData;

  @ViewChild('validationStatusField')
  validationStatusField: EditableSelectLevelEnumFieldComponent;

  isDescriptionExtended$: WritableSignal<boolean> = signal(false);
  $isSprintClosed: Signal<boolean>;
  readonly SQTM_CORE_INFOLIST_ITEM_ICON_PREFIX = 'sqtm-core-infolist-item';

  protected readonly ManagementMode = ManagementMode;

  constructor(
    private translateService: TranslateService,
    private sprintReqVersionViewService: SprintReqVersionViewService,
    private actionErrorDisplayService: ActionErrorDisplayService,
  ) {}

  ngOnInit() {
    this.$isSprintClosed = computed(() => {
      return this.componentData.sprintReqVersion.sprintStatus !== SprintStatus.CLOSED.id;
    });
  }

  get isSynchronized() {
    return this.componentData.sprintReqVersion.mode === ManagementMode.SYNCHRONIZED;
  }

  getIcon(sprintReqVersion: SprintReqVersionState): string {
    const { categoryInfoListItem, categoryId, categoryLabel } = sprintReqVersion;
    const category: InfoListItem = categoryInfoListItem.find(
      (item) => item.id === categoryId || item.label === categoryLabel,
    );

    const iconName: string = category?.iconName;
    return iconName && iconName !== 'noicon'
      ? `${this.SQTM_CORE_INFOLIST_ITEM_ICON_PREFIX}:${iconName}`
      : '';
  }

  getText(property: string) {
    return this.componentData.sprintReqVersion[property] || '-';
  }

  getCategoryLabel(sprintReqVersion: SprintReqVersionState) {
    const { categoryId, categoryLabel, categoryInfoListItem, mode } = sprintReqVersion;
    if (!categoryId && !categoryLabel) {
      return '';
    }
    if (mode === ManagementMode.SYNCHRONIZED) {
      return categoryLabel || '-';
    }
    const category: InfoListItem = categoryInfoListItem.find(
      (item) => item.id === categoryId || item.label === categoryLabel,
    );
    if (!category) {
      return categoryLabel;
    }
    return category?.system
      ? this.translateService.instant(`sqtm-core.entity.${category.label}`)
      : category.label;
  }

  toggleSprintReqDescription() {
    this.isDescriptionExtended$.update((isDescriptionExtended: boolean) => !isDescriptionExtended);
  }

  updateValidationStatus(newValidationStatus: LevelEnumItem<SprintReqVersionValidationStatusKeys>) {
    this.validationStatusField.beginAsync();
    this.sprintReqVersionViewService
      .updateValidationStatus(newValidationStatus.id)
      .pipe(
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => this.validationStatusField.endAsync()),
      )
      .subscribe();
  }

  extractGitlabProjectAndIidFromUrl(url: string) {
    if (!url) {
      return '';
    }
    const splittedUrl: string[] = url.split('/');
    return splittedUrl[splittedUrl.length - 4] + '#' + splittedUrl[splittedUrl.length - 1];
  }

  protected readonly SprintStatus = SprintStatus;
}
