import { APP_BASE_HREF, DatePipe } from '@angular/common';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { Router } from '@angular/router';
import { Dictionary } from '@ngrx/entity/src/models';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import {
  catchError,
  concatMap,
  filter,
  finalize,
  map,
  switchMap,
  take,
  takeUntil,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import {
  ActionErrorDisplayService,
  ASSIGNED_USER_DELEGATE,
  assignedUserColumn,
  assigneeFilter,
  AutomatedSuiteCreationSpecification,
  AutomatedSuiteService,
  buildFilters,
  centredTextColumn,
  ConfirmDeleteConfiguration,
  CustomColumnsComponent,
  DataRow,
  dateRangeFilter,
  DialogService,
  EntityReference,
  EntityRowReference,
  EntityType,
  executionModeColumn,
  ExecutionStatus,
  executionStatusFilter,
  FilterValueModel,
  Fixed,
  grid,
  GRID_PERSISTENCE_KEY,
  GridColumnId,
  GridDefinition,
  GridDndData,
  GridFilter,
  GridFilterUtils,
  GridId,
  GridService,
  gridServiceFactory,
  GridWithStatePersistence,
  i18nEnumResearchFilter,
  indexColumn,
  InterWindowCommunicationService,
  InterWindowMessages,
  isDndDataFromTestCaseTreePicker,
  Limited,
  LocalPersistenceService,
  milestoneLabelColumn,
  parseDataRowId,
  ReferentialDataService,
  ResearchColumnPrototype,
  RestService,
  serverBackedGridTextFilter,
  SqtmDragEnterEvent,
  SqtmDragLeaveEvent,
  SqtmDropEvent,
  SquashTmDataRowType,
  StyleDefinitionBuilder,
  TEST_CASE_TREE_PICKER_ID,
  testCaseImportanceColumn,
  TestPlanResumeModel,
  TestSuiteService,
  textColumn,
  UserHistorySearchProvider,
  Workspaces,
  WorkspaceWithTreeComponent,
} from 'sqtm-core';
import {
  EXECUTION_DIALOG_RUNNER_URL,
  EXECUTION_RUNNER_PROLOGUE_URL,
} from '../../../../execution/execution-runner/execution-runner.constant';
import { dataSetColumn } from '../../../campaign-workspace/components/test-plan/dataset-cell-renderer/dataset-cell-renderer.component';
import { AutomatedTestsExecutionSupervisionDialogComponent } from '../../../iteration-view/components/automated-tests-execution-supervision-dialog/automated-tests-execution-supervision-dialog.component';
import { lastExecutionDateColumn } from '../../../iteration-view/components/cell-renderers/last-execution-date-cell/last-execution-date-cell.component';
import { startExecutionColumn } from '../../../iteration-view/components/cell-renderers/start-execution/start-execution.component';
import { SuccessRateComponent } from '../../../iteration-view/components/cell-renderers/success-rate/success-rate.component';
import { itpiLiteralConverter } from '../../../iteration-view/components/iteration-test-plan-execution/iteration-test-plan-execution.component';
import { ItpiMultiEditDialogConfiguration } from '../../../iteration-view/components/itpi-multi-edit-dialog/itpi-multi-edit-dialog.configuration';
import { TestSuiteViewComponentData } from '../../containers/test-suite-view/test-suite-view.component';
import { TestSuiteViewService } from '../../services/test-suite-view.service';
import {
  TEST_SUITE_TEST_PLAN_DROP_ZONE_ID,
  TSV_ITPE_TABLE,
  TSV_ITPE_TABLE_CONF,
} from '../../test-suite-view.constant';
import { testSuiteViewLogger } from '../../test-suite-view.logger';
import { deleteTestSuiteTestPlanItemColumn } from '../cell-renderers/delete-test-suite-test-plan-item/delete-test-suite-test-plan-item.component';
import { testSuiteExecutionStatusesColumn } from '../cell-renderers/test-suite-execution-status-cell/test-suite-execution-statuses-cell.component';
import { DeleteTestSuiteTestPlanItemConfiguration } from '../delete-test-suite-test-plan-item-dialog/delete-test-suite-test-plan-item-configuration';
import { DeleteTestSuiteTestPlanItemDialogComponent } from '../delete-test-suite-test-plan-item-dialog/delete-test-suite-test-plan-item-dialog.component';
import { TestSuiteTpiMultiEditDialogComponent } from '../test-suite-tpi-multi-edit-dialog/test-suite-tpi-multi-edit-dialog.component';
import { TestSuiteAssignableUserProvider } from './test-suite-assignable-user-provider';
import { ExecutionRunnerOpenerService } from '../../../../execution/execution-runner/services/execution-runner-opener.service';
import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { TestSuiteViewState } from '../../state/test-suite-view.state';
import { withProjectLinkColumn, withTestCaseLinkColumn } from '../../../cell-renderer.builders';
import { ExecutionEnvironmentSummaryService } from '../../../../../components/squash-orchestrator/orchestrator-execution-environment/services/execution-environment-summary.service';
import { TestPlanDraggedContentComponent } from '../../../../../components/test-plan/test-plan-dragged-content/test-plan-dragged-content.component';
import { GENERIC_TEST_PLAN_ITEM_MOVER } from '../../../../../components/test-plan/generic-test-plan-item-mover';
import { GenericTestPlanServerOperationHandler } from '../../../../../components/test-plan/generic-test-plan-server-operation-handler';

export function tsvItpeTableDefinition(
  localPersistenceService: LocalPersistenceService,
): GridDefinition {
  return grid(GridId.TEST_SUITE_TEST_PLAN)
    .withColumns([
      indexColumn().enableDnd().withViewport('leftViewport'),
      withProjectLinkColumn(GridColumnId.projectName)
        .withI18nKey('sqtm-core.entity.project.label.singular')
        .changeWidthCalculationStrategy(new Limited(100))
        .withAssociatedFilter(),
      milestoneLabelColumn(GridColumnId.milestoneLabels).changeWidthCalculationStrategy(
        new Limited(80),
      ),
      executionModeColumn(GridColumnId.inferredExecutionMode)
        .withI18nKey('sqtm-core.entity.execution.mode.label')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Fixed(70)),
      textColumn(GridColumnId.testCaseReference)
        .withI18nKey('sqtm-core.entity.generic.reference.label')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Limited(100)),
      withTestCaseLinkColumn(GridColumnId.testCaseName)
        .withI18nKey('sqtm-core.entity.test-case.label.singular')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Limited(250)),
      testCaseImportanceColumn(GridColumnId.importance)
        .withI18nKey('sqtm-core.entity.test-case.importance.label-short-dot')
        .withTitleI18nKey('sqtm-core.entity.test-case.importance.label')
        .isEditable(false)
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Fixed(65)),
      dataSetColumn(GridColumnId.datasetName, {
        kind: 'dataset',
        itemIdKey: 'itemTestPlanId',
        testPlanOwnerType: 'test-suite',
      })
        .withI18nKey('sqtm-core.entity.dataset.label.short')
        .withAssociatedFilter()
        .withTitleI18nKey('sqtm-core.entity.dataset.label.singular')
        .changeWidthCalculationStrategy(new Limited(130)),
      testSuiteExecutionStatusesColumn(GridColumnId.executionStatus)
        .withI18nKey('sqtm-core.entity.execution.status.label')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Fixed(70)),
      centredTextColumn(GridColumnId.successRate)
        .withRenderer(SuccessRateComponent)
        .withI18nKey('sqtm-core.entity.execution-plan.success-rate.label.short')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.success-rate.label.long')
        .changeWidthCalculationStrategy(new Fixed(100)),
      assignedUserColumn(GridColumnId.assigneeFullName)
        .withI18nKey('sqtm-core.generic.label.user')
        .withAssociatedFilter(GridColumnId.assigneeLogin)
        .changeWidthCalculationStrategy(new Limited(200)),
      lastExecutionDateColumn(GridColumnId.lastExecutedOn),
      startExecutionColumn(GridColumnId.startExecution)
        .withLabel('')
        .disableSort()
        .changeWidthCalculationStrategy(new Fixed(40))
        .withViewport('rightViewport'),
      deleteTestSuiteTestPlanItemColumn(GridColumnId.delete)
        .withLabel('')
        .disableSort()
        .changeWidthCalculationStrategy(new Fixed(40))
        .withViewport('rightViewport'),
    ])
    .server()
    .withRowConverter(itpiLiteralConverter)
    .disableRightToolBar()
    .withRowHeight(35)
    .withStyle(new StyleDefinitionBuilder().enableInitialLoadAnimation().showLines())
    .enableInternalDrop()
    .enableDrag()
    .withDraggedContentRenderer(TestPlanDraggedContentComponent)
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
}

const logger = testSuiteViewLogger.compose('TestSuiteTestPlanExecutionComponent');

@Component({
  selector: 'sqtm-app-test-suite-test-plan-execution',
  templateUrl: './test-suite-test-plan-execution.component.html',
  styleUrls: ['./test-suite-test-plan-execution.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    DatePipe,
    {
      provide: TSV_ITPE_TABLE_CONF,
      useFactory: tsvItpeTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: GENERIC_TEST_PLAN_ITEM_MOVER,
      useExisting: TestSuiteViewService,
    },
    GenericTestPlanServerOperationHandler,
    {
      provide: TSV_ITPE_TABLE,
      useFactory: gridServiceFactory,
      deps: [
        RestService,
        TSV_ITPE_TABLE_CONF,
        ReferentialDataService,
        GenericTestPlanServerOperationHandler,
      ],
    },
    { provide: GridService, useExisting: TSV_ITPE_TABLE },
    { provide: UserHistorySearchProvider, useClass: TestSuiteAssignableUserProvider },
    {
      provide: GRID_PERSISTENCE_KEY,
      useValue: 'test-suite-test-plan-grid',
    },
    GridWithStatePersistence,
    {
      provide: ASSIGNED_USER_DELEGATE,
      useExisting: TestSuiteViewService,
    },
  ],
})
export class TestSuiteTestPlanExecutionComponent implements OnInit, AfterViewInit, OnDestroy {
  dropZoneId = TEST_SUITE_TEST_PLAN_DROP_ZONE_ID;

  testCaseWorkspace = Workspaces['test-case-workspace'];

  @ViewChild('content', { read: ElementRef })
  content: ElementRef;

  @ViewChild('iconColumnElement')
  iconColumnElement: ElementRef;
  private overlayRef: OverlayRef;

  launchAutomatedMenuVisible$ = new BehaviorSubject<boolean>(false);

  unsub$ = new Subject<void>();

  hasSelectedRows$: Observable<boolean>;
  hasSelectedAutomatedRows$: Observable<boolean>;
  activeFilters$: Observable<GridFilter[]>;

  componentData$: Observable<TestSuiteViewComponentData>;

  private areAllItemsReady$: Observable<boolean>;
  private isOneItemReadyOrRunning$: Observable<boolean>;
  private isOneItemAutomated$: Observable<boolean>;

  isLaunchButtonVisible$: Observable<boolean>;
  isReLaunchButtonVisible$: Observable<boolean>;
  isResumeButtonVisible$: Observable<boolean>;
  isLaunchAutomatedButtonVisible$: Observable<boolean>;

  relaunchTestPlanButtonAsync$ = new BehaviorSubject<boolean>(false);
  resumeTestPlanButtonAsync$ = new BehaviorSubject<boolean>(false);

  canEditExecPlan$: Observable<boolean>;
  openSelectFilterMenu = false;

  constructor(
    private gridService: GridService,
    private testSuiteViewService: TestSuiteViewService,
    private renderer: Renderer2,
    private interWindowCommunicationService: InterWindowCommunicationService,
    private dialogService: DialogService,
    private viewContainerRef: ViewContainerRef,
    private testSuiteService: TestSuiteService,
    private automatedSuiteService: AutomatedSuiteService,
    private workspaceWithTree: WorkspaceWithTreeComponent,
    private actionErrorDisplayService: ActionErrorDisplayService,
    private router: Router,
    private gridWithStatePersistence: GridWithStatePersistence,
    private executionRunnerOpenerService: ExecutionRunnerOpenerService,
    private overlay: Overlay,
    protected referentialDataService: ReferentialDataService,
    private cdRef: ChangeDetectorRef,
    private executionEnvironmentSummaryService: ExecutionEnvironmentSummaryService,
    @Inject(APP_BASE_HREF) private baseUrl: string,
  ) {}

  ngOnInit(): void {
    this.componentData$ = this.testSuiteViewService.componentData$;
    this.initializeCommunicationWithExecutionDialog();
    this.initializeGridFilters();
    this.initializeStateObservables();
    this.enableDragInGridAccordingToPermissions();
  }
  ngAfterViewInit(): void {
    this.gridWithStatePersistence
      .popGridState()
      .pipe(
        take(1),
        switchMap(() => this.fetchTestPlan()),
      )
      .subscribe();

    this.activeFilters$ = this.gridService.activeFilters$.pipe(
      takeUntil(this.unsub$),
      map((gridFilters: GridFilter[]) =>
        gridFilters.filter((gridFilter) => GridFilterUtils.mustIncludeFilter(gridFilter)),
      ),
    );

    this.gridService.loaded$
      .pipe(
        filter((loaded) => loaded),
        take(1),
        switchMap(() => this.componentData$),
        filter((componentData) => !componentData.permissions.canLink),
        tap(() => {
          this.gridService.setColumnVisibility(GridColumnId.delete, false);
        }),
      )
      .subscribe();
  }

  private enableDragInGridAccordingToPermissions() {
    this.componentData$.pipe(take(1)).subscribe((componentData: TestSuiteViewComponentData) => {
      this.gridService.setEnableDrag(componentData.permissions.canLink);
    });
  }

  private initializeStateObservables() {
    this.hasSelectedRows$ = this.gridService.hasSelectedRows$.pipe(takeUntil(this.unsub$));

    this.hasSelectedAutomatedRows$ = this.gridService.selectedRows$.pipe(
      takeUntil(this.unsub$),
      filter((rows: DataRow[]) => rows.length > 0),
      map(
        (rows: DataRow[]) =>
          rows.filter(
            (row: DataRow) => row.data[GridColumnId.inferredExecutionMode] === 'AUTOMATED',
          ).length > 0,
      ),
    );

    this.areAllItemsReady$ = this.componentData$.pipe(
      takeUntil(this.unsub$),
      map((componentData) =>
        Array.from(componentData.testSuite.executionStatusMap.values()).every(
          (status) => status === ExecutionStatus.READY.id,
        ),
      ),
    );

    this.isOneItemReadyOrRunning$ = this.componentData$.pipe(
      takeUntil(this.unsub$),
      map((componentData) =>
        Array.from(componentData.testSuite.executionStatusMap.values()).some(
          (status) => status === ExecutionStatus.READY.id || status === ExecutionStatus.RUNNING.id,
        ),
      ),
    );

    this.isOneItemAutomated$ = this.gridService.dataRows$.pipe(
      takeUntil(this.unsub$),
      map((rows: Dictionary<DataRow>) => Object.values(rows)),
      map((rows) =>
        rows.some((row) => row.data[GridColumnId.inferredExecutionMode] === 'AUTOMATED'),
      ),
    );

    this.isLaunchButtonVisible$ = this.areAllItemsReady$;
    this.isReLaunchButtonVisible$ = this.areAllItemsReady$.pipe(
      map((areAllItemsReady) => !areAllItemsReady),
    );
    this.isResumeButtonVisible$ = this.areAllItemsReady$.pipe(
      map((areAllItemsReady) => !areAllItemsReady),
      withLatestFrom(this.isOneItemReadyOrRunning$),
      map(([notAllReady, oneReadyOrRunning]) => notAllReady && oneReadyOrRunning),
    );
    this.isLaunchAutomatedButtonVisible$ = this.isOneItemAutomated$;
    this.canEditExecPlan$ = combineLatest([this.hasSelectedRows$, this.componentData$]).pipe(
      takeUntil(this.unsub$),
      map(
        ([hasSelectedRow, componentData]: [boolean, TestSuiteViewComponentData]) =>
          hasSelectedRow && componentData.permissions.canWrite,
      ),
    );
  }

  private initializeGridFilters() {
    this.gridService.addFilters(this.buildGridFilters());
  }

  private initializeCommunicationWithExecutionDialog() {
    this.interWindowCommunicationService.interWindowMessages$
      .pipe(
        takeUntil(this.unsub$),
        filter(
          (message: InterWindowMessages) =>
            message.isTypeOf('EXECUTION-STEP-CHANGED') ||
            message.isTypeOf('MODIFICATION-DURING-EXECUTION'),
        ),
        switchMap(() => this.refreshComponentData()),
      )
      .subscribe(() => {
        this.gridService.refreshData();
      });
  }

  private fetchTestPlan(): Observable<void> {
    return this.testSuiteViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      filter((componentData: TestSuiteViewComponentData) => Boolean(componentData.testSuite.id)),
      take(1),
      switchMap((componentData) => {
        return this.referentialDataService.isPremiumPluginInstalled$.pipe(
          take(1),
          map((isPremium) => {
            const showMilestones = componentData.globalConfiguration.milestoneFeatureEnabled;

            this.gridService.setColumnVisibility(GridColumnId.milestoneLabels, showMilestones);
            this.gridService.setColumnVisibility(GridColumnId.hasExecutions, false);

            this.gridService.setServerUrl(
              [`test-suite/${componentData.testSuite.id}/test-plan`],
              isPremium,
            );
          }),
        );
      }),
    );
  }

  private refreshComponentData() {
    return this.componentData$.pipe(
      take(1),
      concatMap((componentData: TestSuiteViewComponentData) =>
        this.testSuiteViewService.load(componentData.testSuite.id),
      ),
    );
  }

  private buildGridFilters(): GridFilter[] {
    return buildFilters([
      serverBackedGridTextFilter(GridColumnId.projectName),
      serverBackedGridTextFilter(GridColumnId.testCaseName),
      serverBackedGridTextFilter(GridColumnId.testCaseReference),
      serverBackedGridTextFilter(GridColumnId.testSuites),
      serverBackedGridTextFilter(GridColumnId.datasetName),
      executionStatusFilter(
        GridColumnId.executionStatus,
        ResearchColumnPrototype.EXECUTION_STATUS,
      ).alwaysActive(),
      i18nEnumResearchFilter(
        GridColumnId.importance,
        ResearchColumnPrototype.TEST_CASE_IMPORTANCE,
      ).alwaysActive(),
      i18nEnumResearchFilter(
        GridColumnId.inferredExecutionMode,
        ResearchColumnPrototype.EXECUTION_EXECUTION_MODE,
      ).alwaysActive(),
      assigneeFilter(
        GridColumnId.assigneeLogin,
        ResearchColumnPrototype.ITEM_TEST_PLAN_TESTER,
      ).alwaysActive(),
      dateRangeFilter(
        GridColumnId.lastExecutedOn,
        ResearchColumnPrototype.ITEM_TEST_PLAN_LASTEXECON,
      ),
    ]);
  }

  ngOnDestroy(): void {
    this.relaunchTestPlanButtonAsync$.complete();
    this.resumeTestPlanButtonAsync$.complete();
    this.launchAutomatedMenuVisible$.complete();
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  showMassDeleteItpiDialog(componentData: TestSuiteViewComponentData) {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        map((rows: DataRow[]) => ({
          selectedRows: rows,
          deletableRows: this.filterDeletableRows(rows),
        })),
        switchMap(({ selectedRows, deletableRows }) =>
          this.doOpenMassDeleteDialog(selectedRows, deletableRows, componentData),
        ),
      )
      .subscribe(() => {
        this.gridService.refreshData();
        this.executionEnvironmentSummaryService.refreshView();
      });
  }

  private doOpenMassDeleteDialog(
    selectedRows: DataRow[],
    selectedDeletableRows: DataRow[],
    componentData: TestSuiteViewComponentData,
  ): Observable<boolean | { testSuiteId: number; rowIds: number[] }> {
    const itemTestPlanIds = selectedDeletableRows.map((row) => row.data.itemTestPlanId);
    const hasExecutions = selectedRows.filter((row) => row.data.lastExecutedOn != null).length > 0;
    const testSuiteId = componentData.testSuite.id;

    const dialogReference = this.getDialogReference(
      selectedRows.length === selectedDeletableRows.length,
      itemTestPlanIds,
      hasExecutions,
      testSuiteId,
    );

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      filter((result) => Boolean(result)),
    );
  }

  filterDeletableRows(rows: DataRow[]): DataRow[] {
    if (rows.some((row) => !row.simplePermissions.canExtendedDelete)) {
      return rows.filter((row) => row.data.lastExecutedOn == null);
    }
    return rows;
  }

  getDialogReference(
    allSelectedRowsAreDeletable: boolean,
    itemTestPlanIds: number[],
    hasExecutions: boolean,
    testSuiteId: number,
  ) {
    let messageKey =
      'sqtm-core.campaign-workspace.test-suite.test-plan.message.no-executions.plural';

    if (hasExecutions) {
      messageKey = allSelectedRowsAreDeletable
        ? 'sqtm-core.campaign-workspace.test-suite.test-plan.message.has-executions.plural'
        : 'sqtm-core.campaign-workspace.test-suite.test-plan.message.has-executions.only-deletable-plural';
    }

    return this.dialogService.openDialog<DeleteTestSuiteTestPlanItemConfiguration, boolean>({
      id: 'delete-test-suite-tpi',
      component: DeleteTestSuiteTestPlanItemDialogComponent,
      viewContainerReference: this.viewContainerRef,
      data: {
        id: 'delete-test-suite-tpi',
        titleKey: 'sqtm-core.campaign-workspace.test-suite.test-plan.title.plural',
        messageKey: messageKey,
        level: hasExecutions ? 'DANGER' : 'WARNING',
        testSuiteId,
        itemTestPlanIds,
      },
      width: 600,
    });
  }

  openMassEditDialog(): void {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        switchMap((rows: DataRow[]) => this.doOpenMassEditDialog(rows)),
        filter((dialogResult) => Boolean(dialogResult)),
      )
      .subscribe(() => {
        this.gridService.refreshData();
        this.refreshTestSuiteNode();
      });
  }

  private doOpenMassEditDialog(rows: DataRow[]): Observable<void | undefined> {
    const dialogReference = this.dialogService.openDialog<ItpiMultiEditDialogConfiguration, void>({
      id: 'itpi-multi-edit',
      component: TestSuiteTpiMultiEditDialogComponent,
      viewContainerReference: this.viewContainerRef,
      data: {
        id: 'itpi-multi-edit',
        titleKey: 'sqtm-core.search.generic.modify.selection',
        rows,
      },
    });

    return dialogReference.dialogClosed$;
  }

  private refreshTestSuiteNode(): void {
    this.testSuiteViewService.componentData$
      .pipe(
        take(1),
        map(({ testSuite }) =>
          new EntityRowReference(testSuite.id, SquashTmDataRowType.TestSuite).asString(),
        ),
      )
      .subscribe((identifier) => this.workspaceWithTree.requireNodeRefresh([identifier]));
  }

  toggleTestCasePickerDrawer() {
    this.testSuiteViewService.toggleTestCaseTreePicker();
  }

  dropIntoTestPlan($event: SqtmDropEvent) {
    if ($event.dragAndDropData.origin === TEST_CASE_TREE_PICKER_ID) {
      this.dropTestCasesIntoTestPlan($event);
    }
  }

  dragEnter($event: SqtmDragEnterEvent) {
    if (isDndDataFromTestCaseTreePicker($event)) {
      this.markAsDropZone();
    }
  }

  dragLeave($event: SqtmDragLeaveEvent) {
    if (isDndDataFromTestCaseTreePicker($event)) {
      this.unmarkAsDropZone();
    }
  }

  dragCancel() {
    this.unmarkAsDropZone();
  }

  private markAsDropZone() {
    this.renderer.addClass(this.content.nativeElement, 'sqtm-core-border-current-workspace-color');
  }

  private unmarkAsDropZone() {
    this.renderer.removeClass(
      this.content.nativeElement,
      'sqtm-core-border-current-workspace-color',
    );
  }

  private dropTestCasesIntoTestPlan($event: SqtmDropEvent) {
    const data = $event.dragAndDropData.data as GridDndData;
    if (logger.isDebugEnabled()) {
      logger.debug(`Dropping test cases into test-suite test plan.`, [data]);
    }
    const testCaseIds = data.dataRows.map((row) => parseDataRowId(row));
    this.testSuiteViewService.addTestCaseIntoTestPlan(testCaseIds).subscribe(() => {
      this.gridService.refreshData();
      this.unmarkAsDropZone();
      this.executionEnvironmentSummaryService.refreshView();
    });
  }

  resumeExecution() {
    this.componentData$
      .pipe(
        take(1),
        map((data: TestSuiteViewComponentData) => data.testSuite.id),
        tap(() => this.resumeTestPlanButtonAsync$.next(true)),
        withLatestFrom(this.gridService.activeFilters$),
        map(([id, filters]: [number, GridFilter[]]) => [
          id,
          GridFilterUtils.createRequestFilters(filters),
        ]),
        concatMap(([id, filters]: [number, FilterValueModel[]]) => {
          if (filters.length > 0) {
            return this.testSuiteService.resumeFiltered(id, filters);
          } else {
            return this.testSuiteService.resume(id);
          }
        }),
        catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
        finalize(() => this.resumeTestPlanButtonAsync$.next(false)),
      )
      .subscribe((resume: TestPlanResumeModel) => this.navigateToTestPlanRunner(resume));
  }

  relaunchExecution() {
    this.gridService.activeFilters$
      .pipe(
        take(1),
        map((filters: GridFilter[]) => GridFilterUtils.createRequestFilters(filters)),
        map((filters: FilterValueModel[]) => {
          if (filters.length > 0) {
            return this.getFilteredTestPlanExecutionsDeletionDialogConfiguration();
          } else {
            return this.getTestSuiteExecutionsDeletionDialogConfiguration();
          }
        }),
        switchMap(
          (dialogConfiguration: Partial<ConfirmDeleteConfiguration>) =>
            this.dialogService.openDeletionConfirm(dialogConfiguration).dialogClosed$,
        ),
        filter((confirm: boolean) => confirm),
        tap(() => this.relaunchTestPlanButtonAsync$.next(true)),
        withLatestFrom(this.componentData$, this.gridService.activeFilters$),
        map(([, componentData, filters]) => [
          componentData,
          GridFilterUtils.createRequestFilters(filters),
        ]),
        concatMap(([componentData, filters]: [TestSuiteViewComponentData, FilterValueModel[]]) => {
          if (filters.length > 0) {
            return this.testSuiteService.relaunchFiltered(componentData.testSuite.id, filters);
          } else {
            return this.testSuiteService.relaunch(componentData.testSuite.id);
          }
        }),
        catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
        finalize(() => this.relaunchTestPlanButtonAsync$.next(false)),
        map((resume: TestPlanResumeModel) => this.navigateToTestPlanRunner(resume)),
        switchMap(() => this.refreshComponentData()),
      )
      .subscribe(() => this.gridService.refreshData());
  }

  private getTestSuiteExecutionsDeletionDialogConfiguration(): Partial<ConfirmDeleteConfiguration> {
    return {
      id: 'confirm-delete-all-execution',
      level: 'DANGER',
      titleKey: 'sqtm-core.campaign-workspace.dialog.title.test-suite.mass-delete-execution',
      messageKey: 'sqtm-core.campaign-workspace.dialog.message.test-suite.mass-delete-execution',
    };
  }

  private getFilteredTestPlanExecutionsDeletionDialogConfiguration(): Partial<ConfirmDeleteConfiguration> {
    return {
      id: 'confirm-delete-filtered-execution',
      level: 'DANGER',
      titleKey:
        'sqtm-core.campaign-workspace.dialog.title.filtered-test-plan.mass-delete-execution',
      messageKey:
        'sqtm-core.campaign-workspace.dialog.message.filtered-test-plan.mass-delete-execution',
    };
  }

  private navigateToTestPlanRunner(resume: TestPlanResumeModel) {
    let baseUrl = `${this.baseUrl}${EXECUTION_DIALOG_RUNNER_URL}/test-suite/${resume.testSuiteId}/test-plan/${resume.testPlanItemId}/execution/${resume.executionId}`;
    if (resume.initialStepIndex && resume.initialStepIndex > 0) {
      baseUrl += `/step/${resume.initialStepIndex + 1}`;
    } else {
      baseUrl += `/${EXECUTION_RUNNER_PROLOGUE_URL}`;
    }
    const url = this.router
      .createUrlTree([baseUrl], {
        queryParams: {
          hasNextTestCase: resume.hasNextTestCase,
          partialTestPlanItemIds: JSON.stringify(resume.partialTestPlanItemIds),
        },
      })
      .toString();

    this.executionRunnerOpenerService.openExecutionWithProvidedUrl(url);
  }

  launchAllAutomatedTests() {
    this.launchAutomatedMenuVisible$.next(false);
    this.componentData$
      .pipe(
        take(1),
        map((componentData: TestSuiteViewState) => componentData.testSuite.id),
        withLatestFrom(this.gridService.activeFilters$),
        map(
          ([testSuiteId, filters]: [
            number,
            GridFilter[],
          ]): AutomatedSuiteCreationSpecification => ({
            context: new EntityReference(testSuiteId, EntityType.TEST_SUITE),
            testPlanSubsetIds: [],
            iterationId: this.testSuiteViewService.getIterationId(),
            filterValues: GridFilterUtils.createRequestFilters(filters),
          }),
        ),
      )
      .subscribe((specification: AutomatedSuiteCreationSpecification) =>
        this.openAutomatedExecutionSupervisionDialog(specification),
      );
  }

  launchSelectedAutomatedTests() {
    this.launchAutomatedMenuVisible$.next(false);
    this.gridService.selectedRows$
      .pipe(
        take(1),
        map((rows: DataRow[]) =>
          rows.filter(
            (row: DataRow) => row.data[GridColumnId.inferredExecutionMode] === 'AUTOMATED',
          ),
        ),
        filter((rows: DataRow[]) => rows.length > 0),
        map((rows: DataRow[]) => rows.map((row) => row.id)),
        withLatestFrom(this.componentData$),
        map(
          ([itemIds, componentData]: [
            any,
            TestSuiteViewState,
          ]): AutomatedSuiteCreationSpecification => ({
            context: new EntityReference(componentData.testSuite.id, EntityType.TEST_SUITE),
            testPlanSubsetIds: itemIds,
            iterationId: componentData.testSuite.iterationId,
            filterValues: [],
          }),
        ),
      )
      .subscribe((specification: AutomatedSuiteCreationSpecification) =>
        this.openAutomatedExecutionSupervisionDialog(specification),
      );
  }

  private openAutomatedExecutionSupervisionDialog(data: AutomatedSuiteCreationSpecification) {
    const automatedExecutionDialog = this.dialogService.openDialog({
      id: 'automated-tests-execution-supervision',
      viewContainerReference: this.viewContainerRef,
      component: AutomatedTestsExecutionSupervisionDialogComponent,
      data,
      height: 680,
      width: 800,
    });
    automatedExecutionDialog.dialogClosed$
      .pipe(
        filter((result) => Boolean(result)),
        switchMap(() => this.testSuiteViewService.incrementAutomatedSuiteCount()),
      )
      .subscribe(() => {
        this.gridService.refreshData();
        this.executionEnvironmentSummaryService.refreshView();
      });
  }

  openLaunchAutomatedMenu() {
    this.launchAutomatedMenuVisible$.next(true);
  }

  shouldShowResetFilterLink(activeFilters: GridFilter[]): boolean {
    return activeFilters?.length > 0;
  }

  resetFilters() {
    this.gridService.resetFilters();
  }

  showAvailableFilters(): void {
    this.openSelectFilterMenu = true;

    const positionStrategy = this.overlay
      .position()
      .flexibleConnectedTo(this.iconColumnElement)
      .withPositions([
        { originX: 'center', overlayX: 'center', originY: 'bottom', overlayY: 'top', offsetY: 0 },
        { originX: 'center', overlayX: 'center', originY: 'top', overlayY: 'bottom', offsetY: -10 },
      ]);

    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
    };

    this.overlayRef = this.overlay.create(overlayConfig);
    const componentPortal = new ComponentPortal(CustomColumnsComponent, this.viewContainerRef);
    const componentComponentRef = this.overlayRef.attach(componentPortal);

    componentComponentRef.instance.projectId =
      this.testSuiteViewService.getSnapshot().testSuite.projectId;

    this.overlayRef.backdropClick().subscribe(() => this.close());
    componentComponentRef.instance.confirmClicked.subscribe(() => this.close());
  }

  close() {
    if (this.overlayRef) {
      this.openSelectFilterMenu = false;
      this.overlayRef.dispose();
      this.cdRef.detectChanges();
    }
  }
}
