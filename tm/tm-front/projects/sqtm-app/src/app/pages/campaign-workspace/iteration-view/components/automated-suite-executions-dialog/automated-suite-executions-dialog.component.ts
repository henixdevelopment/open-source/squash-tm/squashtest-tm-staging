import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  signal,
  WritableSignal,
} from '@angular/core';
import {
  buildFilters,
  column,
  ColumnDefinitionBuilder,
  DataRow,
  DataRowOpenState,
  dateTimeColumn,
  DialogConfiguration,
  DialogReference,
  executionStatusColumn,
  Extendable,
  Fixed,
  GenericDataRow,
  GridColumnId,
  GridDefinition,
  GridService,
  gridServiceFactory,
  i18nEnumResearchFilter,
  Identifier,
  indexColumn,
  numericDurationColumn,
  OpenCloseCellRendererComponent,
  ReferentialDataService,
  ResearchColumnPrototype,
  RestService,
  sortDate,
  StyleDefinitionBuilder,
  table,
  textColumn,
  withLinkColumn,
} from 'sqtm-core';
import { DatePipe } from '@angular/common';
import {
  AUTOMATED_SUITE_EXECUTION_TABLE,
  AUTOMATED_SUITE_EXECUTION_TABLE_CONF,
} from '../../iteration-view.constant';
import { filter, switchMap, take, takeUntil, tap } from 'rxjs/operators';
import { NavigationStart, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { automatedSuiteDatasetColumn } from '../cell-renderers/automated-suite-exec-dataset/automated-suite-exec-dataset.component';
import { ReportUrlFoldableRowComponent } from '../report-url-foldable-row/report-url-foldable-row.component';
import { AutomatedExecutionFlagCellRendererComponent } from '../cell-renderers/automated-execution-flag-cell-renderer/automated-execution-flag-cell-renderer.component';

@Component({
  selector: 'sqtm-app-automated-suite-executions-dialog',
  templateUrl: './automated-suite-executions-dialog.component.html',
  styleUrls: ['./automated-suite-executions-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    DatePipe,
    {
      provide: AUTOMATED_SUITE_EXECUTION_TABLE_CONF,
      useFactory: automatedSuiteExecutionTableDefinition,
    },
    {
      provide: AUTOMATED_SUITE_EXECUTION_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, AUTOMATED_SUITE_EXECUTION_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: AUTOMATED_SUITE_EXECUTION_TABLE,
    },
  ],
})
export class AutomatedSuiteExecutionsDialogComponent implements OnInit, AfterViewInit, OnDestroy {
  isPremium: boolean;
  conf: any;

  unsub$ = new Subject<void>();
  $openedRowIds: WritableSignal<Identifier[]> = signal([]);

  constructor(
    private dialogRef: DialogReference<DialogConfiguration>,
    private gridService: GridService,
    private router: Router,
    private referentialDataService: ReferentialDataService,
  ) {
    this.referentialDataService.isPremiumPluginInstalled$
      .pipe(takeUntil(this.unsub$))
      .subscribe((isPremium) => {
        this.isPremium = isPremium;
      });
    this.gridService.addFilters(
      buildFilters([
        i18nEnumResearchFilter(
          GridColumnId.flag,
          ResearchColumnPrototype.EXECUTION_FLAG,
        ).alwaysActive(),
      ]),
    );
  }

  ngOnInit(): void {
    this.conf = this.dialogRef.data;
    this.gridService.setColumnVisibility(
      GridColumnId.flag,
      this.isPremium && this.conf.hasBugTracker,
    );
    this.router.events
      .pipe(
        takeUntil(this.unsub$),
        filter((event) => event instanceof NavigationStart),
      )
      .subscribe((event: NavigationStart) => {
        if (event.url.includes('/execution/')) {
          this.dialogRef.close();
        }
      });
  }

  ngAfterViewInit(): void {
    this.gridService.setServerUrl(['automated-suite', this.conf.suiteId, 'executions']);
  }

  ngOnDestroy(): void {
    this.unsub$.complete();
  }

  refreshPage(): void {
    this.gridService.openedRowIds$
      .pipe(
        take(1),
        tap((ids) => this.$openedRowIds.set(ids)),
        switchMap(() => this.gridService.refreshDataAsync()),
        tap(() => this.gridService.openExternalRows(this.$openedRowIds())),
      )
      .subscribe();
  }
}

export function automatedSuiteExecutionTableDefinition(): GridDefinition {
  return table('automated-suite-execution')
    .withColumns([
      indexColumn(),
      column(GridColumnId.open)
        .withI18nKey('sqtm-core.entity.attachment.label.short')
        .changeWidthCalculationStrategy(new Fixed(20))
        .withRenderer(OpenCloseCellRendererComponent)
        .disableSort(),
      executionStatusColumn(GridColumnId.executionStatus)
        .withI18nKey('sqtm-core.entity.execution.status.label')
        .changeWidthCalculationStrategy(new Fixed(60)),
      new ColumnDefinitionBuilder(GridColumnId.flag)
        .withRenderer(AutomatedExecutionFlagCellRendererComponent)
        .withI18nKey('sqtm-core.entity.execution-plan.execution-flag.label')
        .changeWidthCalculationStrategy(new Fixed(80))
        .withAssociatedFilter(),
      withLinkColumn(GridColumnId.name, {
        kind: 'link',
        baseUrl: '/execution',
        columnParamId: 'executionId',
      })
        .withI18nKey('sqtm-core.entity.name')
        .changeWidthCalculationStrategy(new Extendable(100, 1)),
      automatedSuiteDatasetColumn(GridColumnId.datasetLabel)
        .withI18nKey('sqtm-core.entity.dataset.label.short')
        .withTitleI18nKey('sqtm-core.entity.dataset.label.singular')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5)),
      textColumn(GridColumnId.environmentTags)
        .withI18nKey('sqtm-core.entity.server.environment.label.plural')
        .changeWidthCalculationStrategy(new Extendable(100, 1)),
      textColumn(GridColumnId.environmentVariables)
        .withI18nKey(
          'sqtm-core.administration-workspace.entities-customization.environment-variables.title',
        )
        .changeWidthCalculationStrategy(new Extendable(120, 1)),
      dateTimeColumn(GridColumnId.lastExecutedOn)
        .withSortFunction(sortDate)
        .withI18nKey('sqtm-core.entity.execution-plan.last-execution.label.short-dot')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.last-execution.label.long')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5)),
      numericDurationColumn(GridColumnId.duration)
        .withI18nKey('sqtm-core.entity.execution-plan.execution-duration.label')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5))
        .withContentPosition('left'),
    ])
    .server()
    .withStyle(new StyleDefinitionBuilder().enableInitialLoadAnimation().showLines())
    .withRowHeight(35)
    .withRowConverter(foldReportUrlLiteralConverter)
    .build();
}

export function foldReportUrlLiteralConverter(literals: Partial<DataRow>[]): DataRow[] {
  return literals.reduce((datarows, literal) => {
    const dataRow = new GenericDataRow();
    Object.assign(dataRow, literal);
    dataRow.state = DataRowOpenState.closed;
    dataRow.component = ReportUrlFoldableRowComponent;
    datarows.push(dataRow);
    return datarows;
  }, []);
}
