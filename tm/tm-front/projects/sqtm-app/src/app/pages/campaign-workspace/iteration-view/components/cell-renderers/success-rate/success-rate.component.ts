import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  GridColumnId,
  GridService,
  TestCaseExecutionMode,
  TestCaseKind,
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-success-rate-renderer',
  template: `@if (columnDisplay && row) {
    <div class="full-width full-height flex-column">
      <span
        style="margin: auto 5px;"
        class="sqtm-grid-cell-txt-renderer"
        [ngClass]="textClass"
        [class.show-as-filtered-parent]="showAsFilteredParent"
        nz-tooltip
        [sqtmCoreLabelTooltip]="displayedText"
        [nzTooltipPlacement]="'topLeft'"
      >
        {{ displayedText }}
      </span>
    </div>
  }`,
  styleUrls: ['./success-rate.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuccessRateComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }

  get textClass(): string {
    return 'align-' + this.columnDisplay.contentPosition;
  }

  get displayedText(): string {
    const value = this.row.data[this.columnDisplay.id];
    const isSuccessRateNA: boolean = this.shouldSuccessRateBeHidden();

    if (isSuccessRateNA) {
      return '-';
    }

    if (value == null) {
      return '0 %';
    }
    return `${Math.floor(value)} %`;
  }

  private shouldSuccessRateBeHidden(): boolean {
    const tcFormat: string = this.row.data[GridColumnId.kind];
    const latestExecutionId: string = this.row.data[GridColumnId.latestExecutionId];
    const executionId: string = this.row.data[GridColumnId.executionId];
    const isExecutionHistoryGrid: boolean = latestExecutionId === undefined;
    let executionMode: string;
    let hasNoExecution: boolean;

    const isKeywordTC: boolean = TestCaseKind.KEYWORD.id === tcFormat;

    if (isExecutionHistoryGrid) {
      // inferredExecutionMode is linked to the execution mode on Execution
      hasNoExecution = executionId === null;
      executionMode = this.row.data[GridColumnId.inferredExecutionMode];
    } else {
      // inferredExecutionMode is linked to execution mode on Test Case, so we look at lastExecutionMode instead
      hasNoExecution = latestExecutionId === null;
      executionMode = this.row.data[GridColumnId.lastExecutionMode];
    }

    const isAutomatedExecution: boolean = TestCaseExecutionMode.AUTOMATED.id === executionMode;
    const isFastPass: boolean =
      (this.row.data[GridColumnId.lastExecutedOn] !=
        this.row.data[GridColumnId.executionLastExecutedOn] &&
        this.row.data[GridColumnId.executionLastExecutedOn] !== undefined) ||
      (this.row.data[GridColumnId.executionStatus] !=
        this.row.data[GridColumnId.executionLatestStatus] &&
        this.row.data[GridColumnId.executionLatestStatus] !== undefined);

    return hasNoExecution || (isAutomatedExecution && !isKeywordTC) || isFastPass;
  }
}
