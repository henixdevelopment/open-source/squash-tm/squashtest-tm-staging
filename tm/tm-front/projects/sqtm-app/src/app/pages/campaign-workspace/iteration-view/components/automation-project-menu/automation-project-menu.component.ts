import { APP_BASE_HREF } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnInit,
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import {
  AutomatedSuiteCreationSpecification,
  AutomatedSuiteService,
  DisplayOption,
  TestAutomationProjectPreview,
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-automation-project-menu',
  templateUrl: './automation-project-menu.component.html',
  styleUrls: ['./automation-project-menu.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AutomationProjectMenuComponent implements OnInit {
  @Input()
  automationProject: TestAutomationProjectPreview;

  @Input()
  formGroup: FormGroup;

  @Input()
  specification: AutomatedSuiteCreationSpecification;

  testList: string[] = [];

  constructor(
    private automatedSuiteService: AutomatedSuiteService,
    private translateService: TranslateService,
    private cdr: ChangeDetectorRef,
    @Inject(APP_BASE_HREF) private baseUrl: string,
  ) {}

  ngOnInit(): void {
    this.formGroup.addControl(this.automationProject.projectId.toString(), new FormControl(''));
    this.cdr.detectChanges();
  }

  buildNodesDisplayOptions(): DisplayOption[] {
    const nodesAsOptions = this.automationProject.nodes
      .filter((node: string) => node.length > 0)
      .map((node: string) => ({ id: node, label: node }));
    return [this.buildCurrentServerOption(), ...nodesAsOptions, this.buildIndifferentOption()];
  }

  buildIndifferentOption(): DisplayOption {
    return {
      id: '',
      label: this.translateService.instant('sqtm-core.generic.label.indifferent'),
    };
  }

  buildCurrentServerOption(): DisplayOption {
    return {
      id: 'master',
      label: this.automationProject.server,
    };
  }

  openTestList(isActive: boolean) {
    if (isActive && this.testList.length === 0) {
      this.automatedSuiteService
        .fetchTestListForAutomationProject(this.automationProject.projectId, this.specification)
        .subscribe((testList: string[]) => {
          this.testList = testList;
          this.cdr.detectChanges();
        });
    }
  }
}
