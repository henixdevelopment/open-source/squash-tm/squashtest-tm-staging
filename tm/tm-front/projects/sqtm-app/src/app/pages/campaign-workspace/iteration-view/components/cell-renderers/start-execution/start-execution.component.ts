import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  ViewContainerRef,
} from '@angular/core';
import {
  AbstractCellRendererComponent,
  AclGroup,
  ActionErrorDisplayService,
  AutomatedSuiteCreationSpecification,
  ColumnDefinitionBuilder,
  DialogConfiguration,
  DialogService,
  EntityReference,
  EntityViewService,
  GridColumnId,
  GridService,
  GridWithStatePersistence,
  IterationService,
  ProjectPermission,
  ReferentialDataService,
  ReferentialDataState,
  SimplePermissions,
  SqtmEntityState,
  TestCaseExecutionModeKeys,
} from 'sqtm-core';
import { iterationViewLogger } from '../../../iteration-view.logger';
import { Router } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { AutomatedTestsExecutionSupervisionDialogComponent } from '../../automated-tests-execution-supervision-dialog/automated-tests-execution-supervision-dialog.component';
import { ExecutionHistoryComponent } from '../../execution-history/execution-history.component';
import { catchError, filter, finalize, map, switchMap, takeUntil } from 'rxjs/operators';
import { IterationViewComponentData } from '../../../container/iteration-view/iteration-view.component';
import { ExecutionRunnerOpenerService } from '../../../../../execution/execution-runner/services/execution-runner-opener.service';
import {
  GENERIC_TEST_PLAN_VIEW_SERVICE,
  GenericTestPlanViewService,
} from '../../../../generic-test-plan-view-service';
import { ExecutionEnvironmentSummaryService } from '../../../../../../components/squash-orchestrator/orchestrator-execution-environment/services/execution-environment-summary.service';

const logger = iterationViewLogger.compose('StartExecutionComponent');

@Component({
  selector: 'sqtm-app-start-execution',
  templateUrl: 'start-execution.component.html',
  styleUrls: ['./start-execution.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{ provide: IterationService, useClass: IterationService }],
})
export class StartExecutionComponent<
    S extends SqtmEntityState,
    T extends string,
    P extends SimplePermissions,
  >
  extends AbstractCellRendererComponent
  implements OnDestroy
{
  menuVisible$ = new BehaviorSubject<boolean>(false);

  unsub$ = new Subject<void>();

  private _startingAutomatedExecution$ = new BehaviorSubject<boolean>(false);

  constructor(
    grid: GridService,
    protected dialogService: DialogService,
    public cdRef: ChangeDetectorRef,
    private viewContainerRef: ViewContainerRef,
    private iterationService: IterationService,
    private router: Router,
    @Inject(APP_BASE_HREF) private baseUrl: string,
    public readonly viewService: EntityViewService<S, T, P>,
    private actionErrorDisplayService: ActionErrorDisplayService,
    private gridWithStatePersistence: GridWithStatePersistence,
    private executionRunnerOpenerService: ExecutionRunnerOpenerService,
    @Inject(GENERIC_TEST_PLAN_VIEW_SERVICE)
    private genericTestPlanViewService: GenericTestPlanViewService,
    private referentialDataService: ReferentialDataService,
    private executionEnvironmentSummaryService: ExecutionEnvironmentSummaryService,
  ) {
    super(grid, cdRef);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  canExecute(componentData: IterationViewComponentData): Observable<boolean> {
    const hasTestCase: boolean = Boolean(this.row.data.testCaseId);
    return this.referentialDataService.referentialData$.pipe(
      takeUntil(this.unsub$),
      map((referentialData: ReferentialDataState) => {
        return (
          componentData.permissions.canExecute &&
          componentData.milestonesAllowModification &&
          hasTestCase &&
          !this._startingAutomatedExecution$.getValue() &&
          this.checkIfCurrentUserCanLaunchExecution(referentialData)
        );
      }),
    );
  }

  private checkIfCurrentUserCanLaunchExecution(referentialData: ReferentialDataState): boolean {
    const { admin, userId }: { admin: boolean; userId: number } = referentialData.userState;
    const {
      inferredExecutionMode,
      projectId,
      assigneeId,
    }: { inferredExecutionMode: string; projectId: number; assigneeId: number | null } =
      this.row.data;

    if (admin || inferredExecutionMode === 'EXPLORATORY') {
      return true;
    }

    const projectPermissions: ProjectPermission[] = referentialData.projectPermissions;
    const isTesterOnProject: boolean = projectPermissions.some(
      (projectPermission: ProjectPermission) =>
        projectPermission.projectId === projectId &&
        projectPermission.permissionGroup.qualifiedName === AclGroup.TEST_RUNNER,
    );
    const isNotAssigned: boolean = assigneeId == null || assigneeId !== userId;
    return !(isTesterOnProject && isNotAssigned);
  }

  openExecutionMenu() {
    this.menuVisible$.next(true);
  }

  startExecutionInDialog(componentData: IterationViewComponentData) {
    if (!this.canExecute(componentData)) {
      return;
    }

    const iterationId = this.row.data.iterationId;
    const testPlanItemId = this.row.data.itemTestPlanId;
    logger.debug(`Try to start execution in iteration ${iterationId} for itpi ${testPlanItemId}`);
    if (iterationId && testPlanItemId) {
      this.persistExecutionAndOpenDialog(iterationId, testPlanItemId);
    } else {
      throw Error(
        `Unable to start execution in iteration ${iterationId} for itpi ${testPlanItemId}`,
      );
    }
  }

  private persistExecutionAndOpenDialog(iterationId, testPlanItemId) {
    this.iterationService
      .persistManualExecution(iterationId, testPlanItemId)
      .pipe(catchError((err) => this.actionErrorDisplayService.handleActionError(err)))
      .subscribe((executionId) => {
        logger.debug(`Successfully created execution ${executionId}`);
        this.menuVisible$.next(false);
        this.grid.refreshData();
        this.executionRunnerOpenerService.openExecutionPrologue(executionId);
      });
  }

  startAutomatedExecution(componentData: IterationViewComponentData) {
    if (!this.canExecute(componentData)) {
      return;
    }

    this._startingAutomatedExecution$.next(true);
    this.menuVisible$.next(false);

    const itemIdAsArray: number[] = [this.row.data.itemTestPlanId];
    this.genericTestPlanViewService
      .getEntityReference()
      .pipe(
        map(
          (entityRef: EntityReference): AutomatedSuiteCreationSpecification => ({
            context: entityRef,
            testPlanSubsetIds: itemIdAsArray,
            iterationId: this.genericTestPlanViewService.getIterationId(),
          }),
        ),
        finalize(() => this._startingAutomatedExecution$.next(false)),
      )
      .subscribe((specification: AutomatedSuiteCreationSpecification) =>
        this.openAutomatedExecutionSupervisionDialog(specification),
      );
  }

  private openAutomatedExecutionSupervisionDialog(data: AutomatedSuiteCreationSpecification) {
    const automatedExecutionDialog = this.dialogService.openDialog({
      id: 'automated-tests-execution-supervision',
      viewContainerReference: this.viewContainerRef,
      component: AutomatedTestsExecutionSupervisionDialogComponent,
      data,
      height: 680,
      width: 800,
    });
    automatedExecutionDialog.dialogClosed$
      .pipe(
        filter((result) => Boolean(result)),
        switchMap(() => this.genericTestPlanViewService.incrementAutomatedSuiteCount()),
      )
      .subscribe(() => {
        this.grid.refreshData();
        this.executionEnvironmentSummaryService.refreshView();
      });
  }

  startExecutionInPage(componentData: IterationViewComponentData) {
    if (!this.canExecute(componentData)) {
      return;
    }

    const iterationId = this.row.data.iterationId;
    const testPlanItemId = this.row.data.itemTestPlanId;
    const executionMode = this.row.data.inferredExecutionMode;
    logger.debug(`Try to start execution in iteration ${iterationId} for itpi ${testPlanItemId}`);
    if (iterationId && testPlanItemId) {
      this.persistExecutionAndGoToExecutionPage(iterationId, testPlanItemId, executionMode);
    } else {
      throw Error(
        `Unable to start execution in iteration ${iterationId} for itpi ${testPlanItemId}`,
      );
    }
  }

  displayExecutionHistory() {
    const iterationId = this.row.data.iterationId;
    const testPlanItemId = this.row.data.itemTestPlanId;
    const executionMode = this.row.data.inferredExecutionMode;

    const displayTestPlanExecutionHistoryDialogConfiguration: DialogConfiguration<DisplayExecutionsDialogConf> =
      {
        id: 'test-plan-execution-history-dialog',
        viewContainerReference: this.viewContainerRef,
        component: ExecutionHistoryComponent,
        width: 1010,
        data: {
          iterationId,
          testPlanItemId,
          executionMode,
        },
      };

    this.menuVisible$.next(false);
    const dialogReference = this.dialogService.openDialog(
      displayTestPlanExecutionHistoryDialogConfiguration,
    );

    dialogReference.dialogClosed$.pipe(takeUntil(this.unsub$)).subscribe(() => {
      this.grid.refreshData();
    });
  }

  accessSessionOverview() {
    this.router.navigate(['session-overview', this.row.data.overviewId]);
  }

  private persistExecutionAndGoToExecutionPage(
    iterationId: number,
    testPlanItemId: number,
    executionMode: string,
  ) {
    this.iterationService
      .persistManualExecution(iterationId, testPlanItemId)
      .pipe(catchError((err) => this.actionErrorDisplayService.handleActionError(err)))
      .subscribe((executionId) => {
        logger.debug(`Successfully created execution ${executionId}`);
        this.gridWithStatePersistence.saveSnapshot();
        if (executionMode !== 'EXPLORATORY') {
          this.router.navigate(['execution', executionId]);
        } else {
          this.router.navigate(['execution', executionId, 'charter']);
        }
      });
  }

  public isExploratorySessionOverview() {
    return this.row.data.inferredExecutionMode === 'EXPLORATORY';
  }
}

export function startExecutionColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(StartExecutionComponent);
}

export interface DisplayExecutionsDialogConf {
  iterationId: number;
  testPlanItemId: number;
  executionMode: TestCaseExecutionModeKeys;
}
