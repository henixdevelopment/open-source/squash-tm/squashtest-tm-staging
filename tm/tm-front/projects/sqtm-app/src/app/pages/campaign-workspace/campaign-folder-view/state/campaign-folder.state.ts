import { StatisticsBundle, CustomDashboardModel, SqtmEntityState } from 'sqtm-core';

export interface CampaignFolderState extends SqtmEntityState {
  name: string;
  description: string;
  nbIssues: number;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
  campaignFolderStatisticsBundle?: StatisticsBundle;
  dashboard?: CustomDashboardModel;
}
