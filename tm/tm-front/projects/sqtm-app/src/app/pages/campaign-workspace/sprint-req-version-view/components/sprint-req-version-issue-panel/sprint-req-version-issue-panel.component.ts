import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import {
  ExportModelBuilderService,
  Extendable,
  Fixed,
  GRID_PERSISTENCE_KEY,
  GridColumnId,
  GridDefinition,
  GridExportService,
  GridId,
  GridService,
  GridWithStatePersistence,
  InterWindowCommunicationService,
  InterWindowMessages,
  Limited,
  LocalPersistenceService,
  PaginationConfigBuilder,
  ReferentialDataService,
  RestService,
  grid,
  gridServiceFactory,
  indexColumn,
  issueExecutionsColumn,
  issueKeyColumn,
  textColumn,
} from 'sqtm-core';
import {
  SPRINT_REQ_VERSION_ISSUE_TABLE,
  SPRINT_REQ_VERSION_ISSUE_TABLE_CONFIG,
} from '../../../campaign-workspace.constant';
import { SprintReqVersionViewComponentData } from '../../containers/sprint-req-version-view-sub-page/sprint-req-version-view-sub-page.component';
import { filter, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

export function sprintReqVersionIssueTableDefinition(
  localPersistenceService: LocalPersistenceService,
): GridDefinition {
  return grid(GridId.SPRINT_REQ_VERSION_ISSUE_EXECUTION)
    .withColumns([
      indexColumn().changeWidthCalculationStrategy(new Fixed(70)).withViewport('leftViewport'),
      issueKeyColumn()
        .withI18nKey('sqtm-core.entity.issue.key.label')
        .changeWidthCalculationStrategy(new Limited(100)),
      textColumn(GridColumnId.btProject)
        .withI18nKey('sqtm-core.entity.issue.project.label')
        .changeWidthCalculationStrategy(new Limited(150))
        .disableSort(),
      textColumn(GridColumnId.summary)
        .withI18nKey('sqtm-core.entity.issue.summary.label')
        .changeWidthCalculationStrategy(new Limited(200))
        .disableSort(),
      textColumn(GridColumnId.priority)
        .withI18nKey('sqtm-core.entity.issue.priority.label')
        .changeWidthCalculationStrategy(new Extendable(100, 0.4))
        .disableSort(),
      textColumn(GridColumnId.status)
        .withI18nKey('sqtm-core.entity.issue.status.label')
        .changeWidthCalculationStrategy(new Limited(80))
        .disableSort(),
      textColumn(GridColumnId.assignee)
        .withI18nKey('sqtm-core.entity.issue.assignee.label')
        .changeWidthCalculationStrategy(new Extendable(100, 1))
        .disableSort(),
      issueExecutionsColumn()
        .withI18nKey('sqtm-core.entity.issue.reported-in.label')
        .changeWidthCalculationStrategy(new Extendable(100, 1.5))
        .disableSort(),
    ])
    .disableRightToolBar()
    .server()
    .withPagination(new PaginationConfigBuilder().initialSize(25))
    .withRowHeight(35)
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
}

@Component({
  selector: 'sqtm-app-sprint-req-version-issue-panel',
  templateUrl: `./sprint-req-version-issue-panel.component.html`,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: SPRINT_REQ_VERSION_ISSUE_TABLE_CONFIG,
      useFactory: sprintReqVersionIssueTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: SPRINT_REQ_VERSION_ISSUE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, SPRINT_REQ_VERSION_ISSUE_TABLE_CONFIG, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: SPRINT_REQ_VERSION_ISSUE_TABLE,
    },
    {
      provide: GRID_PERSISTENCE_KEY,
      useValue: 'sprint-req-version-issue-grid',
    },
    GridWithStatePersistence,
    ExportModelBuilderService,
    GridExportService,
  ],
})
export class SprintReqVersionIssuePanelComponent {
  @Input({ required: true })
  sprintReqVersionViewComponentData!: SprintReqVersionViewComponentData;

  private readonly unsub$ = new Subject<void>();

  constructor(
    private gridService: GridService,
    private readonly interWindowCommunicationService: InterWindowCommunicationService,
  ) {
    this.interWindowCommunicationService.interWindowMessages$
      .pipe(
        takeUntil(this.unsub$),
        filter(
          (message: InterWindowMessages) =>
            message.isTypeOf('EXECUTION-STEP-CHANGED') ||
            message.isTypeOf('MODIFICATION-DURING-EXECUTION'),
        ),
      )
      .subscribe(() => {
        this.gridService.refreshData();
      });
  }

  loadData() {
    this.gridService.setServerUrl([
      `issues/sprint-req-version/${this.sprintReqVersionViewComponentData.sprintReqVersion.id}/known-issues`,
    ]);
  }
}
