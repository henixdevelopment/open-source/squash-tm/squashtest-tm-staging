import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  signal,
} from '@angular/core';
import { DialogReference, RestService } from 'sqtm-core';
import { DatePipe } from '@angular/common';
import { interval, Subscription, switchMap } from 'rxjs';
import {
  AutomatedSuiteWorkflowState,
  WorkflowLogsState,
} from '../automated-suite-workflow-viewer-renderer/automated-suite-workflow-viewer-renderer.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-workflow-logs-viewer',
  templateUrl: './workflow-logs-viewer.component.html',
  styleUrl: './workflow-logs-viewer.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe],
})
export class WorkflowLogsViewerComponent implements OnInit, OnDestroy {
  dialogId: string;
  private readonly refreshInterval: number = 1000;
  $logs = signal<string>('');
  $statusKey = signal<'in-progress' | 'done' | 'error'>('in-progress');
  workflow: AutomatedSuiteWorkflowState;
  private logsSubscription: Subscription | null = null;

  constructor(
    private restService: RestService,
    private dialogReference: DialogReference<any>,
    private cdr: ChangeDetectorRef,
    private translateService: TranslateService,
  ) {
    this.dialogId = this.dialogReference.id;
    this.workflow = this.dialogReference.data['workflow'];
    this.$logs.set(this.dialogReference.data['logs']);
  }

  ngOnInit() {
    this.logsSubscription = interval(this.refreshInterval)
      .pipe(
        switchMap(() =>
          this.restService.get<WorkflowLogsState>([
            'workflows',
            this.workflow.workflowId,
            this.workflow.projectId.toString(),
            'logs',
          ]),
        ),
      )
      .subscribe((response) => {
        if (!response.reachable) {
          this.$statusKey.set('error');
          this.stopLogsPolling();
        } else if (!response.isStoredInDatabase) {
          this.restService
            .get<WorkflowLogsState>([
              'workflows',
              this.workflow.workflowId,
              this.workflow.projectId.toString(),
              'logs',
            ])
            .subscribe(() => {
              this.$statusKey.set('done');
              this.$logs.set(response.logs);
              this.stopLogsPolling();
            });
        } else {
          this.$logs.set(response.logs);
        }
        this.cdr.markForCheck();
      });
  }

  ngOnDestroy() {
    this.stopLogsPolling();
  }

  private stopLogsPolling() {
    if (this.logsSubscription) {
      this.logsSubscription.unsubscribe();
      this.logsSubscription = null;
    }
  }

  getWorkflowTitle(): string {
    const title = this.translateService.instant(
      'sqtm-core.campaign-workspace.automated-suite.workflow-viewer.dialog-title',
    );
    const status = this.translateService.instant(
      'sqtm-core.campaign-workspace.automated-suite.workflow-viewer.workflow-status.' +
        this.$statusKey(),
    );

    return `${title} (${status})`;
  }
}
