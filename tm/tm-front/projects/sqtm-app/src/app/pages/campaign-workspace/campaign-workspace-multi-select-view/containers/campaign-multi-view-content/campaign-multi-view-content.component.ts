import { ChangeDetectionStrategy, Component, OnInit, Signal, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { CampaignMultiViewState } from '../../state/campaign-multi-view.state';
import { CampaignMultiSelectionService } from '../../services/campaign-multi-selection.service';
import {
  CustomDashboardBinding,
  DataRow,
  EntityScope,
  ExecutionStatusCount,
  FavoriteDashboardValue,
  GridService,
  SquashTmDataRowType,
} from 'sqtm-core';
import { filter, switchMap, take } from 'rxjs/operators';
import * as _ from 'lodash';
import { CustomDashboardComponent } from '../../../../../components/custom-dashboard/containers/custom-dashboard/custom-dashboard.component';
import { CampaignWorkspaceViewService } from '../../../campaign-workspace-view.service';

@Component({
  selector: 'sqtm-app-campaign-multi-view-content',
  templateUrl: './campaign-multi-view-content.component.html',
  styleUrls: ['./campaign-multi-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CampaignMultiViewContentComponent implements OnInit {
  $lastExecutionScope: Signal<boolean>;
  componentData$: Observable<CampaignMultiViewState>;

  @ViewChild(CustomDashboardComponent)
  dashboardPanel: CustomDashboardComponent;

  constructor(
    private viewService: CampaignMultiSelectionService,
    private tree: GridService,
    protected campaignWorkspaceViewService: CampaignWorkspaceViewService,
  ) {
    this.$lastExecutionScope = campaignWorkspaceViewService.$lastExecutionScope;
  }

  ngOnInit(): void {
    this.componentData$ = this.viewService.componentData$;
  }

  handleStatisticsRefresh($event: MouseEvent): void {
    this.refreshStatistics(this.$lastExecutionScope());
    $event.stopPropagation();
  }

  displayFavoriteDashboard($event: MouseEvent): void {
    $event.stopPropagation();
    this.changeDashboardToDisplay('dashboard', this.$lastExecutionScope());
  }

  displayDefaultDashboard($event: MouseEvent): void {
    $event.stopPropagation();
    this.changeDashboardToDisplay('default', this.$lastExecutionScope());
  }

  private changeDashboardToDisplay(
    preferenceValue: FavoriteDashboardValue,
    lastExecutionScope: boolean,
  ) {
    this.viewService
      .changeDashboardToDisplay(preferenceValue)
      .pipe(
        take(1),
        switchMap(() => this.tree.selectedRows$),
        filter((rows: DataRow[]) => rows.length > 1),
      )
      .subscribe((rows: DataRow[]) => {
        this.viewService.init(rows, lastExecutionScope).subscribe();
      });
  }

  getChartBindings(dashboard: any) {
    return [...dashboard.chartBindings, ...dashboard.reportBindings] as CustomDashboardBinding[];
  }

  hasTestPlanItems(executionStatusCount: ExecutionStatusCount) {
    return Object.values(executionStatusCount).reduce(_.add) > 0;
  }

  getStatisticsTitleI18nKey(scope: EntityScope[]): string {
    let statisticsI18nTitleKey = 'sqtm-core.generic.label.dashboard.title';

    if (
      (scope.length > 0 && this.shouldShowInventoryByCampaign(scope)) ||
      (scope.length > 0 && this.shouldShowInventoryByIteration(scope))
    ) {
      statisticsI18nTitleKey =
        'sqtm-core.campaign-workspace.statistics.multi-selection.statistics-by-campaign';
    } else if (scope.length > 0 && this.shouldShowInventoryByTestSuite(scope)) {
      statisticsI18nTitleKey =
        'sqtm-core.campaign-workspace.statistics.multi-selection.statistics-by-iteration';
    }
    return statisticsI18nTitleKey;
  }

  getInventoryTitleI18nKey(scope: EntityScope[]): string {
    let inventoryI18nTitleKey =
      'sqtm-core.campaign-workspace.statistics.multi-selection.inventory-by-campaign';
    if (this.shouldShowInventoryByIteration(scope)) {
      inventoryI18nTitleKey =
        'sqtm-core.campaign-workspace.statistics.multi-selection.inventory-by-iteration';
    } else if (this.shouldShowInventoryByTestSuite(scope)) {
      inventoryI18nTitleKey =
        'sqtm-core.campaign-workspace.statistics.multi-selection.inventory-by-test-suite';
    }
    return inventoryI18nTitleKey;
  }

  shouldShowInventoryByCampaign(scope: EntityScope[]) {
    return (
      !this.doesScopeContainAGivenEntityType(scope, SquashTmDataRowType.Iteration) &&
      !this.shouldShowInventoryByIteration(scope) &&
      !this.shouldShowInventoryByTestSuite(scope)
    );
  }
  shouldShowInventoryByIteration(scope: EntityScope[]) {
    return this.scopeContainsOnlyGivenEntityType(scope, SquashTmDataRowType.Campaign);
  }
  shouldShowInventoryByTestSuite(scope: EntityScope[]) {
    return this.scopeContainsOnlyGivenEntityType(scope, SquashTmDataRowType.Iteration);
  }

  scopeContainsOnlyGivenEntityType(scope: EntityScope[], entityType: SquashTmDataRowType): boolean {
    const filteredScopeByType: EntityScope[] = scope.filter((entityScope) =>
      entityScope.id.includes(entityType + '-'),
    );
    return filteredScopeByType.length === scope.length;
  }

  doesScopeContainAGivenEntityType(scope: EntityScope[], entityType: SquashTmDataRowType): boolean {
    const filteredScopeByType: EntityScope[] = scope.filter((entityScope) =>
      entityScope.id.includes(entityType + '-'),
    );
    return filteredScopeByType.length >= 1;
  }

  // scope is invalid if selection includes test suites or mixes iterations with any other entity type
  isScopeInvalid(scope: EntityScope[]) {
    return (
      (this.doesScopeContainAGivenEntityType(scope, SquashTmDataRowType.Iteration) &&
        !this.scopeContainsOnlyGivenEntityType(scope, SquashTmDataRowType.Iteration)) ||
      this.doesScopeContainAGivenEntityType(scope, SquashTmDataRowType.TestSuite)
    );
  }

  shouldDisplayTestInventory(componentData: CampaignMultiViewState) {
    return (
      !componentData.shouldShowFavoriteDashboard &&
      componentData.statistics &&
      !this.isScopeInvalid(componentData.scope)
    );
  }

  changeExtendedScope(lastExecutionScope: boolean): void {
    this.campaignWorkspaceViewService.setLastExecutionScope(lastExecutionScope);
    if (this.dashboardPanel != null) {
      this.dashboardPanel.beginAsync();
    }
    this.refreshStatistics(lastExecutionScope);
  }

  private refreshStatistics(lastExecutionScope: boolean): void {
    this.tree.selectedRows$
      .pipe(
        take(1),
        filter((rows) => rows.length > 1),
      )
      .subscribe((rows) => {
        this.viewService.init(rows, lastExecutionScope).subscribe(() => {
          if (this.dashboardPanel != null) {
            this.dashboardPanel.endAsync();
          }
        });
      });
  }
}
