import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { filter, map, switchMap, take, takeUntil, withLatestFrom } from 'rxjs/operators';
import { ActivatedRoute, ParamMap } from '@angular/router';
import {
  ActionErrorDisplayService,
  AttachmentDrawerComponent,
  AttachmentService,
  CampaignPermissions,
  ClosedSprintLockService,
  CustomFieldValueService,
  DialogService,
  EntityPathHeaderService,
  EntityRowReference,
  EntityViewAttachmentHelperService,
  EntityViewComponentData,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  GenericEntityViewService,
  GRID_PERSISTENCE_KEY,
  gridServiceFactory,
  GridWithStatePersistence,
  I18nEnum,
  Identifier,
  LocalPersistenceService,
  ReferentialDataService,
  RestService,
  RichTextAttachmentDelegate,
  SprintService,
  SprintStatus,
  SprintStatusKeys,
  SquashTmDataRowType,
  SynchronizationPluginId,
  Workspaces,
  WorkspaceWithTreeComponent,
} from 'sqtm-core';
import { Observable, Subject } from 'rxjs';
import { SprintState } from '../../state/sprint.state';
import { SprintViewService } from '../../service/sprint-view.service';
import { TranslateService } from '@ngx-translate/core';
import {
  SPRINT_REQ_VERSION_TABLE,
  SPRINT_REQ_VERSION_TABLE_CONF,
} from '../../../campaign-workspace.constant';
import { sprintReqVersionsTableDefinition } from '../../../sprint-req-version-view/components/sprint-req-versions-linked/sprint-req-versions-linked.component';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sqtm-app-sprint-view',
  templateUrl: './sprint-view.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: SPRINT_REQ_VERSION_TABLE_CONF,
      useFactory: sprintReqVersionsTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: SPRINT_REQ_VERSION_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, SPRINT_REQ_VERSION_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: GRID_PERSISTENCE_KEY,
      useValue: 'sprint-req-version-test-plan-grid',
    },
    GridWithStatePersistence,
    {
      provide: SprintViewService,
      useClass: SprintViewService,
      deps: [
        RestService,
        ReferentialDataService,
        AttachmentService,
        TranslateService,
        CustomFieldValueService,
        EntityViewAttachmentHelperService,
        EntityViewCustomFieldHelperService,
        SprintService,
        SPRINT_REQ_VERSION_TABLE,
        ActionErrorDisplayService,
        ClosedSprintLockService,
      ],
    },
    {
      provide: EntityViewService,
      useExisting: SprintViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: SprintViewService,
    },
    {
      provide: RichTextAttachmentDelegate,
      useExisting: SprintViewService,
    },
  ],
})
export class SprintViewComponent implements OnInit, OnDestroy {
  unsub$ = new Subject<void>();
  componentData$: Observable<SprintViewComponentData>;
  $componentData = toSignal(this.sprintService.componentData$);

  @ViewChild(AttachmentDrawerComponent)
  attachmentDrawer: AttachmentDrawerComponent;

  @ViewChild('startStopButton')
  startStopButton: ElementRef<HTMLButtonElement>;

  requirementWorkspace = Workspaces['requirement-workspace'];

  constructor(
    public readonly sprintService: SprintViewService,
    private referentialDataService: ReferentialDataService,
    private route: ActivatedRoute,
    private workspaceWithTree: WorkspaceWithTreeComponent,
    private entityPathHeaderService: EntityPathHeaderService,
    private dialogService: DialogService,
  ) {
    this.componentData$ = this.sprintService.componentData$;
  }

  protected readonly SprintStatus: I18nEnum<SprintStatusKeys> = SprintStatus;

  ngOnInit() {
    this.referentialDataService.loaded$
      .pipe(
        takeUntil(this.unsub$),
        filter((loaded) => loaded),
        take(1),
      )
      .subscribe(() => {
        this.loadData();
        this.initializeTreeSynchronization();
        this.initializeRefreshPathObserver();
      });
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('sprintId')),
      )
      .subscribe((id) => {
        this.sprintService.load(parseInt(id, 10));
      });
  }

  private initializeRefreshPathObserver() {
    this.entityPathHeaderService.refreshPath$
      .pipe(takeUntil(this.unsub$))
      .subscribe(() => this.sprintService.updateEntityPath());
  }

  private initializeTreeSynchronization() {
    this.sprintService.simpleAttributeRequiringRefresh = ['name', 'reference'];
    this.sprintService.externalRefreshRequired$
      .pipe(
        takeUntil(this.unsub$),
        withLatestFrom(this.sprintService.componentData$),
        map(([, componentData]: [any, SprintViewComponentData]) =>
          new EntityRowReference(componentData.sprint.id, SquashTmDataRowType.Sprint).asString(),
        ),
      )
      .subscribe((identifier: Identifier) => {
        this.workspaceWithTree.requireNodeRefresh([identifier]);
      });
  }

  get statusAndRemoteStatusDontMatch(): boolean {
    const sprint = this.$componentData().sprint;
    return sprint.remoteState && sprint.status !== sprint.remoteState;
  }

  get differentStatusMessage() {
    return this.$componentData().sprint.synchronisationKind ===
      SynchronizationPluginId.XSQUASH4GITLAB
      ? 'sqtm-core.sprint.status.different-status-error.gitlab'
      : 'sqtm-core.sprint.status.different-status-error.jira';
  }

  getSprintStatusInformationData(status: string) {
    const sprintStatus = SprintStatus[status];
    return {
      id: sprintStatus.id,
      color: sprintStatus.color,
      icon: sprintStatus.icon,
      labelI18nKey: sprintStatus.i18nKey,
      titleI18nKey: 'sqtm-core.sprint.status.capsule-tooltip',
    };
  }

  startOrStopSprint($event: MouseEvent, sprint: SprintState) {
    $event.preventDefault();
    $event.stopPropagation();

    const status: SprintStatusKeys = sprint.status;
    const sprintId: number = sprint.id;
    let newStatus: SprintStatusKeys;
    switch (status) {
      case SprintStatus.CLOSED.id:
      case SprintStatus.UPCOMING.id:
        newStatus = SprintStatus.OPEN.id;
        break;
      case SprintStatus.OPEN.id:
        newStatus = SprintStatus.CLOSED.id;
        break;
      default:
        break;
    }

    if (newStatus == SprintStatus.CLOSED.id) {
      this.startStopButton.nativeElement.blur();

      const dialogRef = this.dialogService.openConfirm({
        titleKey: 'sqtm-core.sprint.dialog.close-sprint.title',
        messageKey: 'sqtm-core.sprint.dialog.close-sprint.message',
      });

      dialogRef.dialogClosed$
        .pipe(
          filter((confirm) => confirm),
          switchMap(() => this.sprintService.changeSprintStatus(sprintId, newStatus)),
        )
        .subscribe(() => {
          this.sprintService.refreshSprintReqVersions(sprintId);
          this.startStopButton.nativeElement.focus();
        });

      dialogRef.dialogClosed$.pipe(filter((confirm) => !confirm)).subscribe(() => {
        this.startStopButton.nativeElement.focus();
      });
    } else {
      this.sprintService
        .changeSprintStatus(sprintId, newStatus)
        .subscribe(() => this.sprintService.refreshSprintReqVersions(sprintId));
    }
  }

  getTextToDisplay(status: string) {
    switch (status) {
      case SprintStatus.UPCOMING.id:
        return 'sqtm-core.sprint.button.start';
      case SprintStatus.OPEN.id:
        return 'sqtm-core.sprint.button.close';
      case SprintStatus.CLOSED.id:
        return 'sqtm-core.sprint.button.reopen';
      default:
        return status;
    }
  }

  closeRequirementPicker() {
    this.sprintService.closeRequirementPicker();
  }

  toggleAttachmentPanel() {
    this.attachmentDrawer.open();
  }

  ngOnDestroy(): void {
    this.sprintService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }
}

export interface SprintViewComponentData
  extends EntityViewComponentData<SprintState, 'sprint', CampaignPermissions> {}
