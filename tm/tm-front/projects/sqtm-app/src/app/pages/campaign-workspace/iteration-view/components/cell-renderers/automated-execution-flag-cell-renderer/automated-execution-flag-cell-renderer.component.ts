import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  GridService,
  ExecutionFlag,
  ExecutionFlagKeys,
  RestService,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { catchError, finalize, tap } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-execution-flag-cell-renderer',
  templateUrl: './automated-execution-flag-cell-renderer.component.html',
  styleUrls: ['./automated-execution-flag-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AutomatedExecutionFlagCellRendererComponent extends AbstractCellRendererComponent {
  flagKeys: ExecutionFlagKeys[] = Object.keys(ExecutionFlag) as ExecutionFlagKeys[];
  constructor(
    public readonly translateService: TranslateService,
    public readonly gridService: GridService,
    public readonly cdRef: ChangeDetectorRef,
    private restService: RestService,
  ) {
    super(gridService, cdRef);
  }

  get flag(): ExecutionFlagKeys {
    return this.row.data.flag;
  }

  isInactive(flag: ExecutionFlagKeys): boolean {
    return this.row.data.flag !== flag;
  }

  updateFlagOnClick(flag: ExecutionFlagKeys): void {
    if (flag !== this.row.data.flag) {
      this.updateFlag(flag);
    } else {
      this.updateFlag(null);
    }
  }

  private updateFlag(flag: string) {
    const currentFlag = this.row.data.flag;
    const extenderId = this.row.data.extenderId;
    const updateFlagRequest = { flag: flag };

    this.restService
      .post([`execution-extender/${extenderId}/flag`], updateFlagRequest)
      .pipe(
        tap(() => {
          this.row.data.flag = flag;
        }),
        catchError((error) => {
          console.error('Error occurred while updating flag:', error);
          this.row.data.flag = currentFlag;
          return [];
        }),
        finalize(() => {
          this.cdRef.markForCheck();
        }),
      )
      .subscribe();
  }

  getTextColor(flag: ExecutionFlagKeys): string {
    return ExecutionFlag[flag].textColor;
  }

  getBackgroundColor(flag: ExecutionFlagKeys): string {
    return ExecutionFlag[flag].backgroundColor;
  }

  getTooltipText(flag: ExecutionFlagKeys): string {
    return this.translateService.instant(ExecutionFlag[flag].i18nKey);
  }

  getIconType(flag: ExecutionFlagKeys) {
    return ExecutionFlag[flag].icon || '';
  }
}
