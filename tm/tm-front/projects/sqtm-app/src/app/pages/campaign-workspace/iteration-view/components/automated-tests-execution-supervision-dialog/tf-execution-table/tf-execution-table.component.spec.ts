import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppTestingUtilsModule } from '../../../../../../utils/testing-utils/app-testing-utils.module';

import { TfExecutionTableComponent } from './tf-execution-table.component';

describe('TfExecutionTableComponent', () => {
  let component: TfExecutionTableComponent;
  let fixture: ComponentFixture<TfExecutionTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TfExecutionTableComponent],
      imports: [AppTestingUtilsModule, HttpClientTestingModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TfExecutionTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
