import { Injectable } from '@angular/core';
import {
  AttachmentService,
  CampaignLibraryModel,
  CampaignPermissions,
  StatisticsBundle,
  CustomDashboardService,
  CustomFieldValueService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  FavoriteDashboardValue,
  Identifier,
  PartyPreferencesService,
  ProjectData,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import {
  CampaignLibraryViewState,
  provideInitialCampaignLibraryView,
} from '../state/campaign-library-view.state';
import { CampaignLibraryState } from '../state/campaign-library.state';
import { concatMap, map, take, tap, withLatestFrom } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class CampaignLibraryViewService extends EntityViewService<
  CampaignLibraryState,
  'campaignLibrary',
  CampaignPermissions
> {
  constructor(
    protected restService: RestService,
    protected referentialDataService: ReferentialDataService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected customFieldValueService: CustomFieldValueService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
    protected customFieldHelper: EntityViewCustomFieldHelperService,
    private partyPreferencesService: PartyPreferencesService,
    private customDashboardService: CustomDashboardService,
  ) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper,
    );
  }

  addSimplePermissions(projectData: ProjectData): CampaignPermissions {
    return new CampaignPermissions(projectData);
  }

  getInitialState(): CampaignLibraryViewState {
    return provideInitialCampaignLibraryView();
  }

  load(id: number) {
    this.restService
      .getWithoutErrorHandling<CampaignLibraryModel>(['campaign-library-view', id.toString()])
      .subscribe({
        next: (campaignLibraryModel: CampaignLibraryModel) => {
          const campaignLibrary = this.initializeCampaignLibraryState(campaignLibraryModel);
          this.initializeEntityState(campaignLibrary);
        },
        error: (err) => this.notifyEntityNotFound(err),
      });
  }

  private initializeCampaignLibraryState(
    campaignLibraryModel: CampaignLibraryModel,
  ): CampaignLibraryState {
    const attachmentEntityState = this.initializeAttachmentState(
      campaignLibraryModel.attachmentList.attachments,
    );
    const customFieldValueState = this.initializeCustomFieldValueState(
      campaignLibraryModel.customFieldValues,
    );
    return {
      ...campaignLibraryModel,
      attachmentList: {
        id: campaignLibraryModel.attachmentList.id,
        attachments: attachmentEntityState,
      },
      customFieldValues: customFieldValueState,
    };
  }

  refreshStats(lastExecutionScope: boolean) {
    return this.state$.pipe(
      take(1),
      concatMap((state: CampaignLibraryViewState) =>
        this.getCampaignLibraryStatisticsServerSide(
          state.campaignLibrary.id,
          lastExecutionScope,
        ).pipe(
          map((campaignLibraryStatisticsBundle: StatisticsBundle) => {
            return {
              ...state,
              campaignLibrary: {
                ...state.campaignLibrary,
                campaignLibraryStatisticsBundle,
                shouldShowFavoriteDashboard: false,
              },
            };
          }),
        ),
      ),
      tap((state) => this.commit(state)),
    );
  }

  private getCampaignLibraryStatisticsServerSide(
    campaignLibraryId: Identifier,
    lastExecutionScope: boolean,
  ) {
    return this.restService.post(
      ['campaign-library-view', campaignLibraryId.toString(), 'statistics'],
      { lastExecutionScope },
    );
  }

  changeDashboardToDisplay(preferenceValue: FavoriteDashboardValue, lastExecutionScope: boolean) {
    this.partyPreferencesService
      .changeCampaignWorkspaceFavoriteDashboard(preferenceValue)
      .pipe(
        concatMap(() =>
          preferenceValue === 'default'
            ? this.refreshStats(lastExecutionScope)
            : this.refreshDashboard(lastExecutionScope),
        ),
        map((state) => ({
          ...state,
          campaignLibrary: {
            ...state.campaignLibrary,
            shouldShowFavoriteDashboard: preferenceValue === 'dashboard',
          },
        })),
      )
      .subscribe((state) => this.commit(state));
  }

  refreshDashboard(lastExecutionScope: boolean) {
    return this.state$.pipe(
      take(1),
      concatMap((initialState: CampaignLibraryViewState) => {
        if (initialState.campaignLibrary.canShowFavoriteDashboard) {
          return this.customDashboardService
            .getDashboardWithDynamicScope(initialState.campaignLibrary.favoriteDashboardId, {
              milestoneDashboard: false,
              workspaceName: 'CAMPAIGN',
              campaignLibraryIds: [initialState.campaignLibrary.id],
              extendedHighLvlReqScope: false,
              lastExecutionScope: lastExecutionScope,
            })
            .pipe(
              withLatestFrom(this.state$),
              map(([dashboard, state]) => ({
                ...state,
                campaignLibrary: {
                  ...state.campaignLibrary,
                  dashboard: { ...dashboard },
                  generatedDashboardOn: new Date(),
                  shouldShowFavoriteDashboard: true,
                },
              })),
            );
        } else {
          return of({
            ...initialState,
            campaign: { ...initialState.campaignLibrary, shouldShowFavoriteDashboard: true },
          });
        }
      }),
      tap((state: CampaignLibraryViewState) => this.commit(state)),
    );
  }
}
