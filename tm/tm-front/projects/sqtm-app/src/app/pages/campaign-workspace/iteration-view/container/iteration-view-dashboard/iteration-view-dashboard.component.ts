import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  Signal,
  ViewChild,
} from '@angular/core';
import { concatMap, filter, map, take, takeUntil } from 'rxjs/operators';
import { IterationViewService } from '../../services/iteration-view.service';
import { Observable, Subject } from 'rxjs';
import { IterationViewComponentData } from '../iteration-view/iteration-view.component';
import {
  ProgressionStatistics,
  CustomDashboardModel,
  EntityRowReference,
  EntityScope,
  ExecutionStatusCount,
  SquashTmDataRowType,
} from 'sqtm-core';
import * as _ from 'lodash';
import { IterationState } from '../../state/iteration.state';
import { CustomDashboardComponent } from '../../../../../components/custom-dashboard/containers/custom-dashboard/custom-dashboard.component';
import { CampaignWorkspaceViewService } from '../../../campaign-workspace-view.service';

@Component({
  selector: 'sqtm-app-iteration-view-dashboard',
  templateUrl: './iteration-view-dashboard.component.html',
  styleUrls: ['./iteration-view-dashboard.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IterationViewDashboardComponent implements OnInit, OnDestroy {
  $lastExecutionScope: Signal<boolean>;
  componentData$: Observable<IterationViewComponentData>;
  iterationProgressionData$: Observable<ProgressionStatistics>;
  private unsub$: Subject<void> = new Subject<void>();

  @ViewChild(CustomDashboardComponent)
  dashboardPanel: CustomDashboardComponent;

  constructor(
    private readonly iterationViewService: IterationViewService,
    protected campaignWorkspaceViewService: CampaignWorkspaceViewService,
  ) {
    this.componentData$ = iterationViewService.componentData$;
    this.$lastExecutionScope = campaignWorkspaceViewService.$lastExecutionScope;

    // mapping IterationProgressionStatistics into CampaignProgressionStatistics to allow reuse of campaign progression chart.
    this.iterationProgressionData$ = this.componentData$.pipe(
      takeUntil(this.unsub$),
      filter((componentData) => Boolean(componentData.iteration.iterationStatisticsBundle)),
      filter((componentData) => {
        const errors =
          componentData.iteration.iterationStatisticsBundle.progressionStatistics.errors;
        return !errors || errors.length === 0;
      }),
      map((componentData) => {
        const progressionStatistics =
          componentData.iteration.iterationStatisticsBundle.progressionStatistics;
        return {
          cumulativeExecutionsPerDate: progressionStatistics.cumulativeExecutionsPerDate || [],
          errors: progressionStatistics.errors,
          scheduledIterations: progressionStatistics.scheduledIterations,
        };
      }),
    );
  }

  ngOnInit(): void {
    this.componentData$
      .pipe(
        take(1),
        concatMap(() => this.iterationViewService.refreshStats(this.$lastExecutionScope())),
      )
      .subscribe();
  }

  campaignProgressionChartHasErrors(componentData: IterationViewComponentData) {
    const i18nErrors =
      componentData.iteration.iterationStatisticsBundle.progressionStatistics.errors;
    return i18nErrors && i18nErrors.length > 0;
  }

  hasTestPlanItems(executionStatusCount: ExecutionStatusCount) {
    return Object.values(executionStatusCount).reduce(_.add, 0) > 0;
  }

  getScope(componentData: IterationViewComponentData): EntityScope[] {
    const id = new EntityRowReference(
      componentData.iteration.id,
      SquashTmDataRowType.Iteration,
    ).asString();
    return [{ id, label: componentData.iteration.name, projectId: componentData.projectData.id }];
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  getChartBindings(dashboard: CustomDashboardModel) {
    return [...dashboard.chartBindings, ...dashboard.reportBindings];
  }

  displayFavoriteDashboard($event: Event): void {
    $event.stopPropagation();
    this.iterationViewService.changeDashboardToDisplay('dashboard', this.$lastExecutionScope());
  }

  displayDefaultDashboard($event: Event): void {
    $event.stopPropagation();
    this.iterationViewService.changeDashboardToDisplay('default', this.$lastExecutionScope());
  }

  changeExtendedScope(lastExecutionScope: boolean, iteration: IterationState): void {
    this.campaignWorkspaceViewService.setLastExecutionScope(lastExecutionScope);
    this.doRefreshDashboard(iteration);
  }

  private doRefreshDashboard(iteration: IterationState): void {
    if (iteration.iterationStatisticsBundle && this.dashboardPanel == null) {
      this.iterationViewService.refreshStats(this.$lastExecutionScope()).subscribe();
    }
    if (iteration.dashboard && this.dashboardPanel != null) {
      this.dashboardPanel.beginAsync();
      this.iterationViewService.refreshDashboard(this.$lastExecutionScope()).subscribe(() => {
        this.dashboardPanel.endAsync();
      });
    }
  }
}
