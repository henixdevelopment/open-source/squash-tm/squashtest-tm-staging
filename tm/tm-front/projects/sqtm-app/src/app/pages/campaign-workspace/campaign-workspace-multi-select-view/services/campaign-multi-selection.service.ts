import { Injectable } from '@angular/core';
import {
  createStore,
  CustomDashboardModel,
  DataRow,
  EntityScope,
  ExecutionStatusKeys,
  FavoriteDashboardValue,
  Identifier,
  PartyPreferencesService,
  RestService,
  StatisticsBundle,
} from 'sqtm-core';
import {
  CampaignMultiViewState,
  provideInitialCampaignMultiView,
} from '../state/campaign-multi-view.state';
import { Observable } from 'rxjs';
import { map, tap, withLatestFrom } from 'rxjs/operators';

@Injectable()
export class CampaignMultiSelectionService {
  private store = createStore<CampaignMultiViewState>(provideInitialCampaignMultiView());

  componentData$: Observable<CampaignMultiViewState> = this.store.state$;

  constructor(
    private restService: RestService,
    private partyPreferencesService: PartyPreferencesService,
  ) {}

  init(rows: DataRow[], lastExecutionScope: boolean) {
    const references: Identifier[] = rows.map((row) => row.id);
    const scope: EntityScope[] = rows.map((row) => ({
      id: row.id.toString(),
      label: row.data['NAME'],
      projectId: row.projectId,
    }));

    return this.fetchCampaignMultiViewStatisticsServerSide(references, lastExecutionScope).pipe(
      withLatestFrom(this.store.state$),
      map(([response, _state]: [CampaignMultiSelectionModel, CampaignMultiViewState]) => {
        return {
          ...response,
          scope,
          generatedDashboardOn: new Date(),
          dashboardLoaded: true,
        };
      }),
      tap((state) => this.store.commit(state)),
    );
  }

  complete() {
    this.store.complete();
  }

  changeDashboardToDisplay(preferenceValue: FavoriteDashboardValue): Observable<void> {
    return this.partyPreferencesService.changeCampaignWorkspaceFavoriteDashboard(preferenceValue);
  }

  private fetchCampaignMultiViewStatisticsServerSide(
    references: Identifier[],
    lastExecutionScope: boolean,
  ) {
    return this.restService.post(['campaign-workspace-multi-view'], {
      lastExecutionScope: lastExecutionScope,
      references: { references },
    });
  }
}

interface CampaignMultiSelectionModel {
  statistics: StatisticsBundle;
  dashboard: CustomDashboardModel;
  generatedDashboardOn: Date;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
  disabledExecutionStatus: ExecutionStatusKeys[];
  lastExecutionScope: boolean;
}
