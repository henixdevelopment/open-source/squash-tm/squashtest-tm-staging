import { TestBed } from '@angular/core/testing';

import { CampaignWorkspaceViewService } from './campaign-workspace-view.service';
import { of } from 'rxjs';
import { LocalPersistenceService } from 'sqtm-core';

describe('CampaignWorkspaceViewService', () => {
  let service: CampaignWorkspaceViewService;
  const localPersistenceServiceSpy = jasmine.createSpyObj('localPersistence', ['get', 'set']);

  beforeEach(() => {
    localPersistenceServiceSpy.set.and.returnValue(of(null));
    localPersistenceServiceSpy.get.and.returnValue(of(null));

    TestBed.configureTestingModule({
      imports: [],
      providers: [
        {
          provide: LocalPersistenceService,
          useValue: localPersistenceServiceSpy,
        },
      ],
    });
    service = TestBed.inject(CampaignWorkspaceViewService);
  });

  afterEach(() => {
    localPersistenceServiceSpy.get.calls.reset();
    localPersistenceServiceSpy.set.calls.reset();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should initialize $lastExecutionScope with true', () => {
    localPersistenceServiceSpy.get.and.returnValue(of(true));

    service['initializeLastExecutionScope']();

    expect(localPersistenceServiceSpy.get).toHaveBeenCalledWith('limitLastExecutionScope');
    expect(service.getlastExecutionScope()).toBeTrue();
  });

  it('should initialize $lastExecutionScope with false', () => {
    localPersistenceServiceSpy.get.and.returnValue(of(false));

    service['initializeLastExecutionScope']();

    expect(service.getlastExecutionScope()).toBeFalse();
  });

  it('should not update $lastExecutionScope if the stored value is null', () => {
    localPersistenceServiceSpy.get.and.returnValue(of(null));

    service['initializeLastExecutionScope']();

    expect(service.getlastExecutionScope()).toBeTrue();
  });

  it('should set and get the current value of $lastExecutionScope', () => {
    service.setLastExecutionScope(false);
    expect(service.getlastExecutionScope()).toBeFalse();

    service.setLastExecutionScope(true);
    expect(service.getlastExecutionScope()).toBeTrue();
  });

  it('should call localPersistenceService.set when $lastExecutionScope changes', (done) => {
    localPersistenceServiceSpy.set.calls.reset();

    service.setLastExecutionScope(false);

    setTimeout(() => {
      expect(localPersistenceServiceSpy.set).toHaveBeenCalledWith('limitLastExecutionScope', false);
      done();
    });
  });
});
