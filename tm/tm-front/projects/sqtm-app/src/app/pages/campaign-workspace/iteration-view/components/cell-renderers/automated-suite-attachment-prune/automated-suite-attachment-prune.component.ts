import { ChangeDetectionStrategy, ChangeDetectorRef, Component, signal } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  DialogReference,
  DialogService,
  GridColumnId,
  GridService,
  RestService,
} from 'sqtm-core';
import { filter, switchMap, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'sqtm-app-automated-suite-attachment-prune',
  templateUrl: './automated-suite-attachment-prune.component.html',
  styleUrl: './automated-suite-attachment-prune.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AutomatedSuiteAttachmentPruneComponent extends AbstractCellRendererComponent {
  private readonly COMPLETE_KEY: string =
    'sqtm-core.campaign-workspace.automated-suite.attachment-prune.complete.message';
  private readonly PARTIAL_KEY: string =
    'sqtm-core.campaign-workspace.automated-suite.attachment-prune.partial.message';

  constructor(
    gridService: GridService,
    cdRef: ChangeDetectorRef,
    private dialogService: DialogService,
    private restService: RestService,
  ) {
    super(gridService, cdRef);
  }

  get canPrune(): boolean {
    return (
      this.row.simplePermissions &&
      this.row.simplePermissions.canDelete &&
      this.row.data[GridColumnId.executionStatus] !== 'RUNNING' &&
      this.row.data[GridColumnId.executionStatus] !== 'READY'
    );
  }

  $menuVisible = signal(false);

  showMenu() {
    if (this.canPrune) {
      this.$menuVisible.set(true);
    }
  }

  doPrune(isComplete: boolean) {
    this.$menuVisible.set(false);

    const dialogRef = this.getPruneDialogRef(isComplete);

    dialogRef.dialogClosed$
      .pipe(
        filter((confirm) => Boolean(confirm)),
        tap(() => this.grid.beginAsyncOperation()),
        switchMap(() => this.pruneAutomatedSuiteServerSide(isComplete)),
      )
      .subscribe(() => this.grid.completeAsyncOperation());
  }

  private getPruneDialogRef(complete: boolean): DialogReference {
    const messageKey: string = complete ? this.COMPLETE_KEY : this.PARTIAL_KEY;

    return this.dialogService.openConfirm({
      id: 'sqtm-app-automated-suite-attachment-prune',
      titleKey: 'sqtm-core.campaign-workspace.automated-suite.attachment-prune.title',
      messageKey,
      level: 'DANGER',
    });
  }

  private pruneAutomatedSuiteServerSide(complete: boolean): Observable<void> {
    const options = { params: { complete: complete.toString() } };
    return this.restService.post(
      ['automated-suite', this.row.data.suiteId, 'attachment-prune'],
      {},
      options,
    );
  }
}

export function automatedSuitePruneColumn(columnId: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(columnId)
    .withRenderer(AutomatedSuiteAttachmentPruneComponent)
    .withHeaderPosition('center');
}
