import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  ColumnDefinitionBuilder,
  DateTimeCellRendererComponent,
  Fixed,
  GridColumnId,
  GridService,
  GridWithStatePersistence,
  sortDate,
} from 'sqtm-core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'sqtm-app-last-execution-date-cell',
  template: ` @if (row) {
    <div class="full-width full-height flex-column">
      @if (canClick) {
        <a
          style="margin: auto 5px;"
          class="txt-ellipsis"
          [routerLink]="['/', 'execution', latestExecutionId]"
          (click)="saveTestPlanGridState()"
        >
          {{ displayString }}
        </a>
      } @else {
        <span style="margin: auto 5px;" class="txt-ellipsis">
          {{ displayString }}
        </span>
      }
    </div>
  }`,
  styleUrls: ['./last-execution-date-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LastExecutionDateCellComponent extends DateTimeCellRendererComponent {
  constructor(
    grid: GridService,
    public cdRef: ChangeDetectorRef,
    private router: Router,
    public translateService: TranslateService,
    public datePipe: DatePipe,
    private gridWithStatePersistence: GridWithStatePersistence,
  ) {
    super(grid, cdRef, translateService, datePipe);
  }

  get latestExecutionId() {
    return this.row.data[GridColumnId.latestExecutionId];
  }

  get canClick(): boolean {
    const lastExecutedOn = this.row.data[GridColumnId.lastExecutedOn];
    return this.latestExecutionId && lastExecutedOn;
  }

  goToExecutionPage() {
    const latestExecutionId = this.row.data[GridColumnId.latestExecutionId];
    const lastExecutedOn = this.row.data[GridColumnId.lastExecutedOn];
    if (latestExecutionId && lastExecutedOn) {
      this.router.navigate(['execution', latestExecutionId]);
    }
  }

  saveTestPlanGridState() {
    this.gridWithStatePersistence.saveSnapshot();
  }
}

/*
 * Warning! This cell renderer require a GridWithStatePersistence in the dep tree.
 */
export function lastExecutionDateColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(LastExecutionDateCellComponent)
    .withSortFunction(sortDate)
    .withI18nKey('sqtm-core.entity.execution-plan.last-execution.label.short-dot')
    .withTitleI18nKey('sqtm-core.entity.execution-plan.last-execution.label.long')
    .withAssociatedFilter()
    .changeWidthCalculationStrategy(new Fixed(130));
}
