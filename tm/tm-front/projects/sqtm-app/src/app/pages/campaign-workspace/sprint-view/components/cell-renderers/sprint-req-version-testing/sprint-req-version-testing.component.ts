import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Router } from '@angular/router';
import { AbstractCellRendererComponent, GridService } from 'sqtm-core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { SprintViewService } from '../../../service/sprint-view.service';

@Component({
  selector: 'sqtm-app-test-sprint-req-version',
  template: `
    @if (canRead) {
      <div class="flex m-5 justify-content-center">
        <button
          class="flex-fixed-size m-l-5"
          [attr.data-test-element-id]="'test-button'"
          nz-button
          [nzType]="'primary'"
          [nzSize]="'small'"
          (click)="viewSprintReqVersionSubPage()"
        >
          {{ 'sqtm-core.generic.label.test' | translate }}
        </button>
      </div>
    }
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SprintReqVersionTestingComponent
  extends AbstractCellRendererComponent
  implements OnInit, OnDestroy
{
  canRead: boolean;
  unsub$ = new Subject<void>();

  constructor(
    protected router: Router,
    grid: GridService,
    public cdRef: ChangeDetectorRef,
    private sprintViewService: SprintViewService,
    protected cdr: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }

  ngOnInit(): void {
    this.sprintViewService.componentData$
      .pipe(takeUntil(this.unsub$))
      .subscribe((componentData) => {
        this.canRead = componentData.permissions.canRead;
        this.cdr.detectChanges();
      });
  }

  viewSprintReqVersionSubPage() {
    const url = ['/', 'campaign-workspace', 'sprint-req-version', this.row.data.id];
    this.router.navigate(url);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
