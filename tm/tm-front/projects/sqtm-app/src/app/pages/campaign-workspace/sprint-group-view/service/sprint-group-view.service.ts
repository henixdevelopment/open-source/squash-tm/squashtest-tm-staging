import { Injectable } from '@angular/core';
import {
  AttachmentService,
  CampaignPermissions,
  CustomFieldValueService,
  DateFormatUtils,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  ProjectData,
  ReferentialDataService,
  RestService,
  SprintGroupModel,
  SprintModel,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { SprintGroupState } from '../state/sprint-group.state';
import {
  provideInitialSprintGroupView,
  SprintGroupViewState,
} from '../state/sprint-group-view.state';

@Injectable()
export class SprintGroupViewService extends EntityViewService<
  SprintGroupState,
  'sprintGroup',
  CampaignPermissions
> {
  constructor(
    protected restService: RestService,
    protected referentialDataService: ReferentialDataService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected customFieldValueService: CustomFieldValueService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
    protected customFieldHelper: EntityViewCustomFieldHelperService,
  ) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper,
    );
  }

  addSimplePermissions(projectData: ProjectData): CampaignPermissions {
    return new CampaignPermissions(projectData);
  }

  getInitialState(): SprintGroupViewState {
    return provideInitialSprintGroupView();
  }

  load(id: number) {
    this.restService
      .getWithoutErrorHandling<SprintModel>(['sprint-group-view', id.toString()])
      .subscribe({
        next: (sprintGroupModel: SprintGroupModel) => {
          const sprintGroup = this.initializeSprintGroupState(sprintGroupModel);
          this.initializeEntityState(sprintGroup);
        },
        error: (err) => {
          this.notifyEntityNotFound(err);
        },
      });
  }

  private initializeSprintGroupState(sprintGroupModel: SprintGroupModel): SprintGroupState {
    const attachmentEntityState = this.initializeAttachmentState(
      sprintGroupModel.attachmentList.attachments,
    );
    const customFieldValueState = this.initializeCustomFieldValueState(
      sprintGroupModel.customFieldValues,
    );
    return {
      ...sprintGroupModel,
      attachmentList: {
        id: sprintGroupModel.attachmentList.id,
        attachments: attachmentEntityState,
      },
      customFieldValues: customFieldValueState,
      createdOn: DateFormatUtils.createDateFromIsoString(sprintGroupModel.createdOn),
      createdBy: sprintGroupModel.createdBy,
      lastModifiedOn: DateFormatUtils.createDateFromIsoString(sprintGroupModel.lastModifiedOn),
      lastModifiedBy: sprintGroupModel.lastModifiedBy,
      description: sprintGroupModel.description,
    };
  }
}
