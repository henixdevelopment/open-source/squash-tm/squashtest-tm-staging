import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Host,
  Input,
  ViewChild,
} from '@angular/core';
import { CampaignViewComponentData } from '../../container/campaign-view/campaign-view.component';
import { DialogReference, DialogService, EditableDateFieldComponent } from 'sqtm-core';
import { CampaignViewService } from '../../service/campaign-view.service';

@Component({
  selector: 'sqtm-app-campaign-view-planning-panel',
  templateUrl: './campaign-view-planning-panel.component.html',
  styleUrls: ['./campaign-view-planning-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CampaignViewPlanningPanelComponent {
  @ViewChild('actualStartDate')
  actualStartDate: EditableDateFieldComponent;

  @ViewChild('actualEndDate')
  actualEndDate: EditableDateFieldComponent;

  @ViewChild('scheduledStartDate')
  scheduledStartDate: EditableDateFieldComponent;

  @ViewChild('scheduledEndDate')
  scheduledEndDate: EditableDateFieldComponent;

  @Input()
  campaignViewComponentData: CampaignViewComponentData;

  private readonly TWO_COLUMNS_LAYOUT_MIN_SIZE = 600;

  get canEdit(): boolean {
    return (
      this.campaignViewComponentData.permissions.canWrite &&
      this.campaignViewComponentData.milestonesAllowModification
    );
  }

  get isActualStartDateEditable(): boolean {
    return this.canEdit && !this.campaignViewComponentData.campaign.actualStartAuto;
  }

  get isActualEndDateEditable(): boolean {
    return this.canEdit && !this.campaignViewComponentData.campaign.actualEndAuto;
  }

  get isSmallLayout(): boolean {
    return (
      this.hostElement.nativeElement.getBoundingClientRect().width <
      this.TWO_COLUMNS_LAYOUT_MIN_SIZE
    );
  }

  constructor(
    private campaignViewService: CampaignViewService,
    private dialogService: DialogService,
    @Host() private hostElement: ElementRef<HTMLElement>,
  ) {}

  disabledActualStartDate = (actualStart: Date): boolean => {
    if (!actualStart || !this.actualEndDate.value) {
      return false;
    }
    const actualEndDate = new Date(this.actualEndDate.value);
    return this.compareDateOnly(actualStart, actualEndDate);
  };

  disabledActualEndDate = (actualEnd: Date): boolean => {
    if (!this.actualStartDate.value || !actualEnd) {
      return false;
    }
    const actualStartDate = new Date(this.actualStartDate.value);
    return this.compareDateOnly(actualStartDate, actualEnd);
  };

  disabledScheduledStartDate = (scheduledStart: Date): boolean => {
    if (!scheduledStart || !this.scheduledEndDate.value) {
      return false;
    }
    const scheduledEndDate = new Date(this.scheduledEndDate.value);
    return this.compareDateOnly(scheduledStart, scheduledEndDate);
  };

  disabledScheduledEndDate = (scheduledEnd: Date): boolean => {
    if (!this.scheduledStartDate.value || !scheduledEnd) {
      return false;
    }
    const scheduledStartDate = new Date(this.scheduledStartDate.value);
    return this.compareDateOnly(scheduledStartDate, scheduledEnd);
  };

  compareDateOnly(dateOne: Date, dateTwo: Date): boolean {
    const dateOneNoTime = this.removeTime(dateOne);
    const dateTwoNoTime = this.removeTime(dateTwo);
    return dateOneNoTime.getTime() > dateTwoNoTime.getTime();
  }

  removeTime(date: Date) {
    const dateOut = new Date(date);
    dateOut.setHours(0, 0, 0, 0);
    return dateOut;
  }

  updateScheduledStartDate(scheduledStartDate: Date, scheduledEndDate: Date) {
    if (this.canUpdateDate(scheduledStartDate, scheduledEndDate)) {
      this.campaignViewService
        .updateScheduledStartDate(scheduledStartDate)
        .subscribe(() => (this.scheduledStartDate.value = scheduledStartDate));
    } else {
      this.showAlertDateDialog();
      this.scheduledStartDate.endAsync();
      this.scheduledStartDate.cancel();
    }
  }

  updateScheduledEndDate(scheduledEndDate: Date, scheduledStartDate: Date) {
    if (this.canUpdateDate(scheduledStartDate, scheduledEndDate)) {
      this.campaignViewService
        .updateScheduledEndDate(scheduledEndDate)
        .subscribe(() => (this.scheduledEndDate.value = scheduledEndDate));
    } else {
      this.showAlertDateDialog();
      this.scheduledEndDate.endAsync();
      this.scheduledEndDate.cancel();
    }
  }

  updateActualStartDate(actualStartDate: Date, actualEndDate: Date) {
    if (this.canUpdateDate(actualStartDate, actualEndDate)) {
      this.campaignViewService
        .updateActualStartDate(actualStartDate)
        .subscribe(() => (this.actualStartDate.value = actualStartDate));
    } else {
      this.showAlertDateDialog();
      this.actualStartDate.endAsync();
      this.actualStartDate.cancel();
    }
  }

  updateActualEndDate(actualEndDate: Date, actualStartDate: Date) {
    if (this.canUpdateDate(actualStartDate, actualEndDate)) {
      this.campaignViewService
        .updateActualEndDate(actualEndDate)
        .subscribe(() => (this.actualEndDate.value = actualEndDate));
    } else {
      this.showAlertDateDialog();
      this.actualEndDate.endAsync();
      this.actualEndDate.cancel();
    }
  }

  updateActualStartAuto(startAuto: boolean) {
    this.campaignViewService.updateActualStartAuto(startAuto).subscribe();
  }

  updateActualEndAuto(endAuto: boolean) {
    this.campaignViewService.updateActualEndAuto(endAuto).subscribe();
  }

  showAlertDateDialog(): DialogReference {
    return this.dialogService.openAlert({
      level: 'DANGER',
      messageKey: 'sqtm-core.validation.errors.time-period-not-consistent',
    });
  }

  private canUpdateDate(startDate: Date, endDate: Date) {
    if (startDate != null && endDate != null) {
      const start = new Date(startDate);
      const end = new Date(endDate);
      return start <= end;
    } else {
      return true;
    }
  }
}
