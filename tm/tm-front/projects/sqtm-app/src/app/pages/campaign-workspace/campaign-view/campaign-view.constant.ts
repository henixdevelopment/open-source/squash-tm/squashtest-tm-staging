import { InjectionToken } from '@angular/core';
import { GridDefinition, GridService } from 'sqtm-core';

export const CAMPAIGN_TEST_PLAN_DROP_ZONE_ID = 'campaignTestPlanDropZone';

export const CPV_CTPE_TABLE_CONF = new InjectionToken<GridDefinition>(
  'Grid config for the campaign test plan execution table of campaign view',
);

export const CPV_CTPE_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the campaign test plan execution table of campaign view',
);
