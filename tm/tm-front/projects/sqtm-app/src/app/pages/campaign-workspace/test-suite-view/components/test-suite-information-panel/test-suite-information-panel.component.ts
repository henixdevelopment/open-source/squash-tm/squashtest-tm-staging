import { ChangeDetectionStrategy, Component, Input, ViewChild } from '@angular/core';
import { TestSuiteViewComponentData } from '../../containers/test-suite-view/test-suite-view.component';
import {
  CustomFieldData,
  EditableSelectLevelEnumFieldComponent,
  ExecutionStatus,
  Identifier,
  TestPlanStatus,
} from 'sqtm-core';
import { TestSuiteViewService } from '../../services/test-suite-view.service';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'sqtm-app-test-suite-information-panel',
  templateUrl: './test-suite-information-panel.component.html',
  styleUrls: ['./test-suite-information-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestSuiteInformationPanelComponent {
  @Input()
  testSuiteViewComponentData$: Observable<TestSuiteViewComponentData>;

  @Input()
  customFieldData: CustomFieldData[];

  @ViewChild('executionStatusField')
  executionStatusField: EditableSelectLevelEnumFieldComponent;

  constructor(
    public testSuiteViewService: TestSuiteViewService,
    public translateService: TranslateService,
  ) {}

  getUuid(componentData: TestSuiteViewComponentData): string {
    return componentData.testSuite.uuid;
  }

  getId(componentData: TestSuiteViewComponentData): Identifier {
    return componentData.testSuite.id;
  }

  trackCfd(cfd: CustomFieldData) {
    return cfd.id;
  }

  getProgressStatus(componentData: TestSuiteViewComponentData): string {
    if (
      Array.from(componentData.testSuite.executionStatusMap.values()).every(
        (status) => status === ExecutionStatus.READY.id,
      )
    ) {
      return TestPlanStatus.READY.i18nKey;
    } else if (
      Array.from(componentData.testSuite.executionStatusMap.values()).some(
        (status) => status === ExecutionStatus.READY.id || status === ExecutionStatus.RUNNING.id,
      )
    ) {
      return TestPlanStatus.RUNNING.i18nKey;
    } else {
      return TestPlanStatus.DONE.i18nKey;
    }
  }

  getSelectedEnumItem(componentData: TestSuiteViewComponentData) {
    return componentData.testSuite.executionStatus;
  }

  enableExecutionStatus(statusKey: string, componentData: TestSuiteViewComponentData): boolean {
    return (
      componentData.projectData.disabledExecutionStatus.filter((status) => status === statusKey)
        .length === 0
    );
  }

  getExcludedExecutionStatusKeys(componentData: TestSuiteViewComponentData) {
    const excludedKeys = ['WARNING', 'ERROR', 'NOT_RUN', 'NOT_FOUND'];
    if (!this.enableExecutionStatus('SETTLED', componentData)) {
      excludedKeys.push('SETTLED');
    }
    if (!this.enableExecutionStatus('UNTESTABLE', componentData)) {
      excludedKeys.push('UNTESTABLE');
    }
    return excludedKeys;
  }
}
