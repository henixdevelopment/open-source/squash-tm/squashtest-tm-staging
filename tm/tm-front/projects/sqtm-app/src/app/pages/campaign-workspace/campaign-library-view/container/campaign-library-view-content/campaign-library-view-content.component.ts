import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  Signal,
  ViewChild,
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { CampaignLibraryViewComponentData } from '../campaign-library-view/campaign-library-view.component';
import { CampaignLibraryViewService } from '../../service/campaign-library-view.service';
import { concatMap, take } from 'rxjs/operators';
import { CampaignLibraryState } from '../../state/campaign-library.state';
import {
  CustomDashboardBinding,
  CustomDashboardModel,
  EntityRowReference,
  EntityScope,
  ExecutionStatusCount,
  SquashTmDataRowType,
} from 'sqtm-core';
import * as _ from 'lodash';
import { CustomDashboardComponent } from '../../../../../components/custom-dashboard/containers/custom-dashboard/custom-dashboard.component';
import { CampaignWorkspaceViewService } from '../../../campaign-workspace-view.service';

@Component({
  selector: 'sqtm-app-campaign-library-view-content',
  templateUrl: './campaign-library-view-content.component.html',
  styleUrls: ['./campaign-library-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CampaignLibraryViewContentComponent implements OnInit, OnDestroy {
  $lastExecutionScope: Signal<boolean>;
  private unsub$: Subject<void> = new Subject<void>();
  componentData$: Observable<CampaignLibraryViewComponentData>;

  @ViewChild(CustomDashboardComponent)
  customDashboard: CustomDashboardComponent;

  constructor(
    private campaignLibraryViewService: CampaignLibraryViewService,
    protected campaignWorkspaceViewService: CampaignWorkspaceViewService,
  ) {
    this.componentData$ = campaignLibraryViewService.componentData$;
    this.$lastExecutionScope = campaignWorkspaceViewService.$lastExecutionScope;
  }

  ngOnInit() {
    this.componentData$
      .pipe(
        take(1),
        concatMap(() => this.campaignLibraryViewService.refreshStats(this.$lastExecutionScope())),
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  displayFavoriteDashboard($event: MouseEvent) {
    $event.stopPropagation();
    this.campaignLibraryViewService.changeDashboardToDisplay(
      'dashboard',
      this.$lastExecutionScope(),
    );
  }

  displayDefaultDashboard($event: MouseEvent) {
    $event.stopPropagation();
    this.campaignLibraryViewService.changeDashboardToDisplay('default', this.$lastExecutionScope());
  }

  hasTestPlanItems(executionStatusCount: ExecutionStatusCount) {
    return Object.values(executionStatusCount).reduce(_.add) > 0;
  }

  getScope(campaignLibrary: CampaignLibraryState): EntityScope[] {
    const ref = new EntityRowReference(
      campaignLibrary.id,
      SquashTmDataRowType.CampaignLibrary,
    ).asString();
    return [
      {
        id: ref,
        label: campaignLibrary.name,
        projectId: campaignLibrary.projectId,
      },
    ];
  }

  getChartBindings(dashboard: CustomDashboardModel) {
    return [...dashboard.chartBindings, ...dashboard.reportBindings] as CustomDashboardBinding[];
  }

  changeExtendedScope(
    lastExecutionScope: boolean,
    componentData: CampaignLibraryViewComponentData,
  ) {
    this.campaignWorkspaceViewService.setLastExecutionScope(lastExecutionScope);

    if (
      componentData.campaignLibrary.campaignLibraryStatisticsBundle &&
      this.customDashboard == null
    ) {
      return this.campaignLibraryViewService.refreshStats(lastExecutionScope).subscribe();
    }

    if (componentData.campaignLibrary.dashboard && this.customDashboard != null) {
      this.customDashboard.beginAsync();
      this.campaignLibraryViewService.refreshDashboard(lastExecutionScope).subscribe(() => {
        this.customDashboard.endAsync();
      });
    }
  }
}
