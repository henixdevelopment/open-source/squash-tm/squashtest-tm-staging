import { StatisticsBundle, CustomDashboardModel, SqtmEntityState } from 'sqtm-core';

export interface CampaignLibraryState extends SqtmEntityState {
  name: string;
  description: string;
  nbIssues: number;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
  campaignLibraryStatisticsBundle?: StatisticsBundle;
  dashboard?: CustomDashboardModel;
}
