import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  buildLevelEnumKeySort,
  ColumnDefinitionBuilder,
  Fixed,
  GridColumnId,
  GridService,
  SprintReqVersionValidationStatus,
} from 'sqtm-core';
import { ValidationStatusComponent } from '../components/validation-status/validation-status.component';

@Component({
  selector: 'sqtm-app-validation-status-cell-renderer',
  template: `
    @if (row) {
      <div class="full-width full-height flex-column">
        <sqtm-app-validation-status
          class="validation-status-cell flex justify-content-center"
          [status]="row.data[columnDisplay.id]"
        ></sqtm-app-validation-status>
      </div>
    }
  `,
  styleUrls: ['./validation-status-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [ValidationStatusComponent],
})
export class ValidationStatusCellRendererComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }
}

export function validationStatusColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(ValidationStatusCellRendererComponent)
    .withI18nKey('sqtm-core.sprint.sprint-req-version-grid.validation-status.title-short')
    .withTitleI18nKey('sqtm-core.sprint.req-version.validation-status.title')
    .withSortFunction(buildLevelEnumKeySort(SprintReqVersionValidationStatus))
    .withAssociatedFilter()
    .changeWidthCalculationStrategy(new Fixed(60))
    .withHeaderPosition('center');
}
