import { ChangeDetectionStrategy, Component, InjectionToken, OnInit, Signal } from '@angular/core';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { TranslateModule } from '@ngx-translate/core';
import { SprintViewComponentData } from '../sprint-view/sprint-view.component';
import { SprintViewService } from '../../service/sprint-view.service';
import { toSignal } from '@angular/core/rxjs-interop';
import {
  AnchorModule,
  ASSIGNED_USER_DELEGATE,
  assignedUserColumn,
  assigneeFilter,
  buildFilters,
  centredTextColumn,
  dateRangeFilter,
  executionModeColumn,
  Fixed,
  grid,
  GRID_PERSISTENCE_KEY,
  GridColumnId,
  GridDefinition,
  GridFilter,
  GridId,
  GridModule,
  GridService,
  gridServiceFactory,
  i18nEnumResearchFilter,
  indexColumn,
  InterWindowCommunicationService,
  InterWindowMessages,
  Limited,
  LocalPersistenceService,
  ReferentialDataService,
  ResearchColumnPrototype,
  RestService,
  serverBackedGridTextFilter,
  StyleDefinitionBuilder,
  testCaseImportanceColumn,
  textColumn,
  textResearchFilter,
  UserHistorySearchProvider,
} from 'sqtm-core';
import {
  withProjectLinkColumn,
  withSprintReqVersionLinkColumn,
  withTestCaseLinkColumn,
} from '../../../cell-renderer.builders';
import { dataSetColumn } from '../../../campaign-workspace/components/test-plan/dataset-cell-renderer/dataset-cell-renderer.component';
import { SuccessRateComponent } from '../../../iteration-view/components/cell-renderers/success-rate/success-rate.component';
import { lastExecutionDateColumn } from '../../../iteration-view/components/cell-renderers/last-execution-date-cell/last-execution-date-cell.component';
import { testPlanItemLiteralConverter } from '../../../sprint-req-version-view/containers/sprint-req-version-view-content/sprint-req-version-view-content.component';
import { testPlanItemExecutionStatusesColumn } from '../../../../../components/test-plan/cell-renderers/test-plan-item-execution-statuses-cell-renderer/test-plan-item-execution-statuses-cell-renderer.component';
import { TEST_PLAN_OWNER_VIEW } from '../../../campaign-workspace.constant';
import { testPlanItemPlayColumn } from '../../../../../components/test-plan/cell-renderers/test-plan-item-play-cell-renderer/test-plan-item-play-cell-renderer.component';
import { GENERIC_TEST_PLAN_EXECUTION_MANAGER } from '../../../../../components/test-plan/generic-test-plan-execution-manager';
import { filter, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { SprintTestPlanItemAssignableUserProvider } from './sprint-test-plan-item-assignable-user-provider.service';

const SPRINT_OVERALL_EXEC_PLAN_TABLE_CONF = new InjectionToken(
  'Grid config for the overall execution plan table of sprint view',
);
const SPRINT_OVERALL_EXEC_PLAN_TABLE = new InjectionToken(
  'Grid service instance for the overall execution plan table of sprint view',
);

export function sprintOverallExecPlanTableDefinition(
  localPersistenceService: LocalPersistenceService,
): GridDefinition {
  return grid(GridId.SPRINT_VIEW_OVERALL_EXEC_PLAN)
    .server()
    .withColumns([
      indexColumn().enableDnd().withViewport('leftViewport'),
      withProjectLinkColumn(GridColumnId.projectName)
        .withI18nKey('sqtm-core.entity.project.label.singular')
        .changeWidthCalculationStrategy(new Limited(100))
        .withAssociatedFilter(),
      executionModeColumn(GridColumnId.inferredExecutionMode)
        .withI18nKey('sqtm-core.entity.execution.mode.label')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Fixed(70)),
      textColumn(GridColumnId.testCaseReference)
        .withI18nKey('sqtm-core.entity.generic.reference.label')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Limited(100)),
      withTestCaseLinkColumn(GridColumnId.testCaseName)
        .withI18nKey('sqtm-core.entity.test-case.label.singular')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Limited(160)),
      testCaseImportanceColumn(GridColumnId.importance)
        .withI18nKey('sqtm-core.entity.test-case.importance.label-short-dot')
        .withTitleI18nKey('sqtm-core.entity.test-case.importance.label')
        .isEditable(false)
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Fixed(65)),
      dataSetColumn(GridColumnId.datasetName, {
        kind: 'dataset',
        itemIdKey: 'testPlanItemId',
        testPlanOwnerType: 'sprint',
      })
        .isEditable(false)
        .withI18nKey('sqtm-core.entity.dataset.label.short')
        .withAssociatedFilter()
        .withTitleI18nKey('sqtm-core.entity.dataset.label.singular')
        .changeWidthCalculationStrategy(new Limited(90)),
      testPlanItemExecutionStatusesColumn().withHeaderPosition('center'),
      centredTextColumn(GridColumnId.successRate)
        .disableSort()
        .withRenderer(SuccessRateComponent)
        .withI18nKey('sqtm-core.entity.execution-plan.success-rate.label.short')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.success-rate.label.long')
        .changeWidthCalculationStrategy(new Fixed(60)),
      assignedUserColumn(GridColumnId.assigneeFullName)
        .withI18nKey('sqtm-core.generic.label.user')
        .withAssociatedFilter(GridColumnId.assigneeLogin)
        .changeWidthCalculationStrategy(new Limited(200)),
      lastExecutionDateColumn(GridColumnId.lastExecutedOn),
      withSprintReqVersionLinkColumn(GridColumnId.sprintReqVersionNameWithRef),
      testPlanItemPlayColumn(),
    ])
    .server()
    .withRowConverter(testPlanItemLiteralConverter)
    .disableRightToolBar()
    .withRowHeight(35)
    .withStyle(new StyleDefinitionBuilder().enableInitialLoadAnimation().showLines())
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
}

@Component({
  selector: 'sqtm-app-sprint-view-overall-exec-plan',
  templateUrl: './sprint-view-overall-exec-plan.component.html',
  styleUrl: './sprint-view-overall-exec-plan.component.less',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [NzCollapseModule, TranslateModule, AnchorModule, GridModule],
  providers: [
    {
      provide: SPRINT_OVERALL_EXEC_PLAN_TABLE_CONF,
      useFactory: sprintOverallExecPlanTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: SPRINT_OVERALL_EXEC_PLAN_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, SPRINT_OVERALL_EXEC_PLAN_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: SPRINT_OVERALL_EXEC_PLAN_TABLE,
    },
    {
      provide: GRID_PERSISTENCE_KEY,
      useValue: 'sprint-overall-exec-plan-grid',
    },
    {
      provide: TEST_PLAN_OWNER_VIEW,
      useExisting: SprintViewService,
    },
    {
      provide: GENERIC_TEST_PLAN_EXECUTION_MANAGER,
      useExisting: SprintViewService,
    },
    {
      provide: ASSIGNED_USER_DELEGATE,
      useExisting: SprintViewService,
    },
    {
      provide: UserHistorySearchProvider,
      useClass: SprintTestPlanItemAssignableUserProvider,
    },
  ],
})
export class SprintViewOverallExecPlanComponent implements OnInit {
  $componentData: Signal<SprintViewComponentData> = toSignal(this.sprintViewService.componentData$);
  private readonly unsub$ = new Subject<void>();

  constructor(
    private readonly sprintViewService: SprintViewService,
    private readonly gridService: GridService,
    private readonly interWindowCommunicationService: InterWindowCommunicationService,
  ) {
    this.interWindowCommunicationService.interWindowMessages$
      .pipe(
        takeUntil(this.unsub$),
        filter(
          (message: InterWindowMessages) =>
            message.isTypeOf('EXECUTION-STEP-CHANGED') ||
            message.isTypeOf('MODIFICATION-DURING-EXECUTION'),
        ),
      )
      .subscribe(() => {
        this.gridService.refreshData();
      });
  }

  ngOnInit() {
    this.loadData();
    this.initializeFilters();
  }

  private loadData() {
    this.gridService.setServerUrl([
      `sprint-view/${this.$componentData().sprint.id}/overall-exec-plan`,
    ]);
  }

  private initializeFilters() {
    const filters: GridFilter[] = buildFilters([
      serverBackedGridTextFilter(GridColumnId.projectName).alwaysActive(),
      i18nEnumResearchFilter(
        GridColumnId.inferredExecutionMode,
        ResearchColumnPrototype.EXECUTION_EXECUTION_MODE,
      ).alwaysActive(),
      serverBackedGridTextFilter(GridColumnId.testCaseReference),
      serverBackedGridTextFilter(GridColumnId.testCaseName),
      i18nEnumResearchFilter(
        GridColumnId.importance,
        ResearchColumnPrototype.TEST_CASE_IMPORTANCE,
      ).alwaysActive(),
      textResearchFilter(
        GridColumnId.datasetName,
        ResearchColumnPrototype.DATASET_NAME,
      ).alwaysActive(),
      i18nEnumResearchFilter(
        GridColumnId.executionStatus,
        ResearchColumnPrototype.ITEM_TEST_PLAN_STATUS,
      ).alwaysActive(),
      assigneeFilter(
        GridColumnId.assigneeLogin,
        ResearchColumnPrototype.ITEM_TEST_PLAN_TESTER,
      ).alwaysActive(),
      dateRangeFilter(
        GridColumnId.lastExecutedOn,
        ResearchColumnPrototype.ITEM_TEST_PLAN_LASTEXECON,
      ),
    ]);
    this.gridService.addFilters(filters);
  }
}
