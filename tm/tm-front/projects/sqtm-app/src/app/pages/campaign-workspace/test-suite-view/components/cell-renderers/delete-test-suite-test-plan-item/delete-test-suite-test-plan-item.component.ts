import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  ViewContainerRef,
} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  DataRow,
  DialogService,
  GridColumnId,
  GridService,
  RestService,
} from 'sqtm-core';
import { Observable, Subject } from 'rxjs';
import { TestSuiteViewComponentData } from '../../../containers/test-suite-view/test-suite-view.component';
import { TestSuiteViewService } from '../../../services/test-suite-view.service';
import { filter, takeUntil } from 'rxjs/operators';
import { DeleteTestSuiteTestPlanItemConfiguration } from '../../delete-test-suite-test-plan-item-dialog/delete-test-suite-test-plan-item-configuration';
import { DeleteTestSuiteTestPlanItemDialogComponent } from '../../delete-test-suite-test-plan-item-dialog/delete-test-suite-test-plan-item-dialog.component';
import { ExecutionEnvironmentSummaryService } from '../../../../../../components/squash-orchestrator/orchestrator-execution-environment/services/execution-environment-summary.service';

@Component({
  selector: 'sqtm-app-delete-test-suite-test-plan-item',
  template: `
    @if (componentData$ | async; as componentData) {
      @if (row && componentData.milestonesAllowModification) {
        @if (canDelete(row)) {
          <div
            class="full-height full-width flex-column icon-container current-workspace-main-color __hover_pointer"
            (click)="removeItem(row, componentData)"
          >
            <i nz-icon [nzType]="getIcon(row)" nzTheme="outline" class="table-icon-size"></i>
          </div>
        }
      }
    }
  `,
  styleUrls: ['./delete-test-suite-test-plan-item.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteTestSuiteTestPlanItemComponent
  extends AbstractCellRendererComponent
  implements OnDestroy
{
  unsub$ = new Subject<void>();

  componentData$: Observable<TestSuiteViewComponentData>;

  constructor(
    public grid: GridService,
    cdr: ChangeDetectorRef,
    private dialogService: DialogService,
    private restService: RestService,
    private vcr: ViewContainerRef,
    private testSuiteViewService: TestSuiteViewService,
    private executionEnvironmentSummaryService: ExecutionEnvironmentSummaryService,
  ) {
    super(grid, cdr);
    this.componentData$ = this.testSuiteViewService.componentData$;
  }

  getIcon(row: DataRow): string {
    return this.rowHasExecution(row) ? 'sqtm-core-generic:delete' : 'sqtm-core-generic:unlink';
  }

  removeItem(row: DataRow, componentData: TestSuiteViewComponentData) {
    const dialogReference = this.dialogService.openDialog<
      DeleteTestSuiteTestPlanItemConfiguration,
      boolean
    >({
      id: 'delete-test-suite-tpi',
      component: DeleteTestSuiteTestPlanItemDialogComponent,
      viewContainerReference: this.vcr,
      data: {
        id: 'delete-test-suite-tpi',
        titleKey: 'sqtm-core.campaign-workspace.test-suite.test-plan.title.singular',
        messageKey: this.rowHasExecution(row)
          ? 'sqtm-core.campaign-workspace.test-suite.test-plan.message.has-executions.singular'
          : 'sqtm-core.campaign-workspace.test-suite.test-plan.message.no-executions.singular',
        level: this.rowHasExecution(row) ? 'DANGER' : 'WARNING',
        testSuiteId: componentData.testSuite.id,
        itemTestPlanIds: [row.data[GridColumnId.itemTestPlanId]],
      },
      width: 600,
    });

    dialogReference.dialogClosed$
      .pipe(
        takeUntil(this.unsub$),
        filter((result) => Boolean(result)),
      )
      .subscribe(() => {
        this.grid.refreshData();
        this.executionEnvironmentSummaryService.refreshView();
      });
  }

  private rowHasExecution(row: DataRow): boolean {
    return row.data[GridColumnId.latestExecutionId] != null;
  }

  canDelete(row: DataRow): boolean {
    return (
      row.simplePermissions &&
      (this.rowHasExecution(row)
        ? row.simplePermissions.canExtendedDelete
        : row.simplePermissions.canDelete)
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}

export function deleteTestSuiteTestPlanItemColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(DeleteTestSuiteTestPlanItemComponent);
}
