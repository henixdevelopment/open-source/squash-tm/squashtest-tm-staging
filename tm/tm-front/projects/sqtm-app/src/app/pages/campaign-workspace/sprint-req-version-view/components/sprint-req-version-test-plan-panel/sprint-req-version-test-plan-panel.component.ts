import { ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit } from '@angular/core';
import {
  assigneeFilter,
  buildFilters,
  dateRangeFilter,
  executionStatusFilter,
  GridColumnId,
  GridFilter,
  GridService,
  i18nEnumResearchFilter,
  InterWindowCommunicationService,
  InterWindowMessages,
  ReferentialDataService,
  ResearchColumnPrototype,
  serverBackedGridTextFilter,
} from 'sqtm-core';
import { SprintReqVersionViewComponentData } from '../../containers/sprint-req-version-view-sub-page/sprint-req-version-view-sub-page.component';
import { filter, take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'sqtm-app-sprint-req-version-test-plan-panel',
  template: `<sqtm-core-grid class="sqtm-core-prevent-selection-in-grid"> </sqtm-core-grid>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SprintReqVersionTestPlanPanelComponent implements OnInit, OnDestroy {
  @Input({ required: true })
  componentData!: SprintReqVersionViewComponentData;

  private readonly unsub$ = new Subject<void>();

  constructor(
    private readonly gridService: GridService,
    private readonly referentialDataService: ReferentialDataService,
    private readonly interWindowCommunicationService: InterWindowCommunicationService,
  ) {
    this.interWindowCommunicationService.interWindowMessages$
      .pipe(
        takeUntil(this.unsub$),
        filter(
          (message: InterWindowMessages) =>
            message.isTypeOf('EXECUTION-STEP-CHANGED') ||
            message.isTypeOf('MODIFICATION-DURING-EXECUTION'),
        ),
      )
      .subscribe(() => {
        this.gridService.refreshData();
      });
  }

  ngOnInit() {
    this.gridService.addFilters(this.buildGridFilters());
    this.referentialDataService.isPremiumPluginInstalled$.pipe(take(1)).subscribe((isPremium) => {
      this.gridService.setServerUrl(
        ['test-plan', this.componentData.sprintReqVersion.testPlanId.toString()],
        isPremium,
      );
    });
    this.enableDragInGridAccordingToPermissions();
  }

  private enableDragInGridAccordingToPermissions() {
    this.gridService.setEnableDrag(this.componentData.permissions.canLink);
  }

  ngOnDestroy(): void {
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  private buildGridFilters(): GridFilter[] {
    return buildFilters([
      serverBackedGridTextFilter(GridColumnId.projectName).alwaysActive(),
      i18nEnumResearchFilter(
        GridColumnId.inferredExecutionMode,
        ResearchColumnPrototype.EXECUTION_EXECUTION_MODE,
      ).alwaysActive(),
      serverBackedGridTextFilter(GridColumnId.testCaseReference),
      serverBackedGridTextFilter(GridColumnId.testCaseName),
      i18nEnumResearchFilter(
        GridColumnId.importance,
        ResearchColumnPrototype.TEST_CASE_IMPORTANCE,
      ).alwaysActive(),
      serverBackedGridTextFilter(GridColumnId.datasetName),
      executionStatusFilter(
        GridColumnId.executionStatus,
        ResearchColumnPrototype.EXECUTION_STATUS,
      ).alwaysActive(),
      assigneeFilter(
        GridColumnId.assigneeLogin,
        ResearchColumnPrototype.ITEM_TEST_PLAN_TESTER,
      ).alwaysActive(),
      dateRangeFilter(
        GridColumnId.lastExecutedOn,
        ResearchColumnPrototype.ITEM_TEST_PLAN_LASTEXECON,
      ),
    ]);
  }
}
