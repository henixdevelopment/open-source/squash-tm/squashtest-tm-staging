import { EntityViewState, provideInitialViewState } from 'sqtm-core';
import { SprintState } from './sprint.state';

export interface SprintViewState extends EntityViewState<SprintState, 'sprint'> {
  sprint: SprintState;
}

export function provideInitialSprintView(): Readonly<SprintViewState> {
  return provideInitialViewState<SprintState, 'sprint'>('sprint');
}
