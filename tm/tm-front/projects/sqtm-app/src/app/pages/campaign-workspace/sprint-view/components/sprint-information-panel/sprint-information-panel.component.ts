import { ChangeDetectionStrategy, Component, Input, ViewChild } from '@angular/core';
import { SprintViewComponentData } from '../../containers/sprint-view/sprint-view.component';
import { TranslateService } from '@ngx-translate/core';
import {
  ActionErrorDisplayService,
  EditableDateFieldComponent,
  EditableRichTextComponent,
  SprintStatus,
  SynchronizationPluginId,
} from 'sqtm-core';
import { SprintViewService } from '../../service/sprint-view.service';
import { catchError, finalize } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-sprint-information-panel',
  templateUrl: './sprint-information-panel.component.html',
  styleUrls: ['./sprint-information-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SprintInformationPanelComponent {
  @ViewChild('startDate')
  startDate: EditableDateFieldComponent;

  @ViewChild('endDate')
  endDate: EditableDateFieldComponent;

  @ViewChild('description')
  description: EditableRichTextComponent;

  @Input()
  sprintViewComponentData: SprintViewComponentData;

  constructor(
    public translateService: TranslateService,
    private sprintViewService: SprintViewService,
    private actionErrorDisplayService: ActionErrorDisplayService,
  ) {}

  get remoteStateLabel() {
    return this.sprintViewComponentData.sprint.synchronisationKind ===
      SynchronizationPluginId.XSQUASH4GITLAB
      ? 'sqtm-core.sprint.status.label.gitlab'
      : 'sqtm-core.sprint.status.label.jira';
  }

  get sprintStatus() {
    const status: string = this.sprintViewComponentData.sprint.status;
    return 'sqtm-core.sprint.status.' + status;
  }

  get sprintRemoteState() {
    const remoteState: string = this.sprintViewComponentData.sprint.remoteState;
    return 'sqtm-core.sprint.status.' + remoteState;
  }

  get statusHelpTooltipText() {
    return this.sprintViewComponentData.sprint.synchronisationKind ===
      SynchronizationPluginId.XSQUASH4GITLAB
      ? 'sqtm-core.sprint.status.help-tooltip.gitlab'
      : 'sqtm-core.sprint.status.help-tooltip.jira';
  }

  get sprintStatusEnumItem() {
    return SprintStatus[this.sprintViewComponentData.sprint.status];
  }

  get sprintRemoteStateEnumItem() {
    return SprintStatus[this.sprintViewComponentData.sprint.remoteState];
  }

  get isRemoteClosed() {
    return this.sprintViewComponentData.sprint.remoteState === SprintStatus.CLOSED.id;
  }

  get isSynchronized() {
    return !!this.sprintViewComponentData.sprint.remoteState;
  }

  get canEdit(): boolean {
    return this.sprintViewComponentData.permissions.canWrite;
  }

  disabledStartDate = (start: Date): boolean => {
    if (!start || !this.endDate.value) {
      return false;
    }
    const endDate = new Date(this.endDate.value);
    return this.compareDateOnly(start, endDate);
  };

  disabledEndDate = (end: Date): boolean => {
    if (!end || !this.startDate.value) {
      return false;
    }
    const startDate = new Date(this.startDate.value);
    return this.compareDateOnly(startDate, end);
  };

  compareDateOnly(dateOne: Date, dateTwo: Date): boolean {
    const dateOneNoTime = this.removeTime(dateOne);
    const dateTwoNoTime = this.removeTime(dateTwo);
    return dateOneNoTime.getTime() > dateTwoNoTime.getTime();
  }

  removeTime(date: Date) {
    const dateOut = new Date(date);
    dateOut.setHours(0, 0, 0, 0);
    return dateOut;
  }

  updateStartDate(startDate: Date) {
    this.startDate.beginAsync();
    this.sprintViewService
      .updateStartDate(this.sprintViewComponentData.sprint.id, startDate)
      .pipe(
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => {
          this.startDate.endAsync();
          this.startDate.cancel();
        }),
      )
      .subscribe();
  }

  updateEndDate(endDate: Date) {
    this.endDate.beginAsync();
    this.sprintViewService
      .updateEndDate(this.sprintViewComponentData.sprint.id, endDate)
      .pipe(
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => {
          this.endDate.endAsync();
          this.endDate.cancel();
        }),
      )
      .subscribe();
  }

  updateDescription(description: string) {
    this.description.beginAsync();
    this.sprintViewService
      .updateDescription(this.sprintViewComponentData.sprint.id, description)
      .pipe(
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => {
          this.description.endAsync();
          this.description.cancel();
        }),
      )
      .subscribe();
  }

  protected readonly SprintStatus = SprintStatus;
}
