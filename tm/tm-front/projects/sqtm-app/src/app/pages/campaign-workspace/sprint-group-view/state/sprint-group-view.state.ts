import { EntityViewState, provideInitialViewState } from 'sqtm-core';
import { SprintGroupState } from './sprint-group.state';

export interface SprintGroupViewState extends EntityViewState<SprintGroupState, 'sprintGroup'> {
  sprintGroup: SprintGroupState;
}

export function provideInitialSprintGroupView(): Readonly<SprintGroupViewState> {
  return provideInitialViewState<SprintGroupState, 'sprintGroup'>('sprintGroup');
}
