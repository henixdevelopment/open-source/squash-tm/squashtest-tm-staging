import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { ResearchColumnPrototype, UserHistorySearchProvider, UserListElement } from 'sqtm-core';
import { SprintViewService } from '../../service/sprint-view.service';

@Injectable()
export class SprintTestPlanItemAssignableUserProvider extends UserHistorySearchProvider {
  constructor(private sprintViewService: SprintViewService) {
    super();
  }

  provideUserList(_columnPrototype: ResearchColumnPrototype): Observable<UserListElement[]> {
    return this.sprintViewService.componentData$.pipe(
      take(1),
      map((data) => data.sprint.assignableUsers.map((u) => ({ ...u, selected: false }))),
    );
  }
}
