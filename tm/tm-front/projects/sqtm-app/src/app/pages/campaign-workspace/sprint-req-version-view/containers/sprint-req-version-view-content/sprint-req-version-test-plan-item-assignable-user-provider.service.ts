import { Injectable } from '@angular/core';
import { ResearchColumnPrototype, UserHistorySearchProvider, UserListElement } from 'sqtm-core';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { SprintReqVersionViewService } from '../../service/sprint-req-version-view.service';

@Injectable()
export class SprintReqVersionTestPlanItemAssignableUserProvider extends UserHistorySearchProvider {
  constructor(private sprintReqVersionViewService: SprintReqVersionViewService) {
    super();
  }

  provideUserList(_columnPrototype: ResearchColumnPrototype): Observable<UserListElement[]> {
    return this.sprintReqVersionViewService.componentData$.pipe(
      take(1),
      map((data) => data.sprintReqVersion.assignableUsers.map((u) => ({ ...u, selected: false }))),
    );
  }
}
