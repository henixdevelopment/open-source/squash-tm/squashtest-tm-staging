import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable } from 'rxjs';
import { SprintViewComponentData } from '../sprint-view/sprint-view.component';
import { SprintViewService } from '../../service/sprint-view.service';

@Component({
  selector: 'sqtm-app-sprint-view-content',
  templateUrl: './sprint-view-content.component.html',
  styleUrls: ['./sprint-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SprintViewContentComponent {
  readonly componentData$: Observable<SprintViewComponentData> =
    this.sprintViewService.componentData$;

  constructor(private sprintViewService: SprintViewService) {}
}
