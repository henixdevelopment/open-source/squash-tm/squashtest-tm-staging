import {
  ChangeDetectionStrategy,
  Component,
  InjectionToken,
  OnDestroy,
  OnInit,
} from '@angular/core';
import {
  AutomSuiteComparator,
  buildFilters,
  ColumnDefinition,
  ColumnWithFilter,
  DialogConfiguration,
  DialogReference,
  executionStatusColumn,
  executionStatusFilter,
  Extendable,
  FilterBuilder,
  GRID_PERSISTENCE_KEY,
  getSupportedBrowserLang,
  GridId,
  GridColumnId,
  GridDefinition,
  GridFilter,
  GridService,
  grid,
  gridServiceFactory,
  GridWithStatePersistence,
  Limited,
  PaginationConfigBuilder,
  ReferentialDataService,
  ResearchColumnPrototype,
  RestService,
  SuiteIdCreatedOn,
  TestExecutionInfo,
  textColumn,
  textResearchFilter,
} from 'sqtm-core';
import { filter, take, takeUntil, tap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { DatePipe } from '@angular/common';
import { BehaviorSubject, Subject, switchMap } from 'rxjs';
import { NavigationStart, Router } from '@angular/router';
import { AutomSuiteComparisonHeaderRendererComponent } from '../header-renderers/autom-suite-comparison-header-renderer/autom-suite-comparison-header-renderer.component';
import { withTestCaseLinkColumn } from '../../../cell-renderer.builders';

export const AS_EXECUTION_COMPARATOR_TABLE_CONF = new InjectionToken<GridDefinition>(
  'Grid config for the automated suite execution comparator',
);
export const AS_EXECUTION_COMPARATOR_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the automated suite execution comparator',
);

export function automatedSuiteExecutionComparatorTableDefinition(): GridDefinition {
  return grid(GridId.AUTOMATED_SUITE_EXECUTION_COMPARATOR)
    .withColumns([
      withTestCaseLinkColumn(GridColumnId.testCaseName)
        .withI18nKey('sqtm-core.entity.test-case.label.singular')
        .withAssociatedFilter()
        .withViewport('leftViewport')
        .changeWidthCalculationStrategy(new Limited(200)),
      textColumn(GridColumnId.datasetName)
        .withI18nKey('sqtm-core.entity.dataset.label.short')
        .withTitleI18nKey('sqtm-core.entity.dataset.label.singular')
        .withContentPosition('center')
        .withHeaderPosition('center')
        .withViewport('leftViewport')
        .changeWidthCalculationStrategy(new Limited(100))
        .withAssociatedFilter(),
    ])
    .disableRightToolBar()
    .withRowHeight(40)
    .withPagination(new PaginationConfigBuilder().initialSize(25))
    .build();
}

@Component({
  selector: 'sqtm-app-automated-suite-exec-comparator-dialog',
  templateUrl: './automated-suite-exec-comparator-dialog.component.html',
  styleUrls: ['./automated-suite-exec-comparator-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: AS_EXECUTION_COMPARATOR_TABLE_CONF,
      useFactory: automatedSuiteExecutionComparatorTableDefinition,
    },
    {
      provide: AS_EXECUTION_COMPARATOR_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, AS_EXECUTION_COMPARATOR_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: AS_EXECUTION_COMPARATOR_TABLE,
    },
    {
      provide: GRID_PERSISTENCE_KEY,
      useValue: 'automated-suite-comparator-grid',
    },
    GridWithStatePersistence,
  ],
})
export class AutomatedSuiteExecComparatorDialogComponent implements OnInit, OnDestroy {
  private readonly NONE_STATUS: string = 'NONE';

  isInit$ = new BehaviorSubject<boolean>(false);
  unsub$ = new Subject<void>();

  private suites: SuiteIdCreatedOn[];

  constructor(
    private dialogReference: DialogReference<DialogConfiguration>,
    public gridService: GridService,
    public translateService: TranslateService,
    public datePipe: DatePipe,
    private restService: RestService,
    private router: Router,
  ) {
    if (Array.isArray(this.dialogReference.data)) {
      this.suites = this.dialogReference.data;
    }
  }

  ngOnDestroy(): void {
    this.unsub$.complete();
  }

  ngOnInit(): void {
    const suiteIds: string[] = this.suites.map((suite) => suite.id);
    this.restService
      .post<TestExecutionInfo[]>(['automated-suites', 'compare-executions'], suiteIds)
      .pipe(
        switchMap((testExecutionInfos: TestExecutionInfo[]) =>
          this.initializeGrid(testExecutionInfos),
        ),
        tap(() => this.initializeGridFilters()),
        tap(() => this.isInit$.next(true)),
      )
      .subscribe();

    this.router.events
      .pipe(
        takeUntil(this.unsub$),
        filter((event) => event instanceof NavigationStart),
      )
      .subscribe((event: NavigationStart) => {
        if (event.url.includes('/test-case-workspace/')) {
          this.dialogReference.close();
        }
      });
  }

  private initializeGrid(testExecutionInfos: TestExecutionInfo[]) {
    return this.gridService.columns$.pipe(
      take(1),
      tap((columns: ColumnWithFilter[]) => this.addColumns(columns)),
      tap(() => this.loadDatarows(testExecutionInfos)),
    );
  }

  private addColumns(columns: ColumnWithFilter[]): void {
    const comparatorColumns: ColumnDefinition[] = this.suites.map((suite) => {
      const date = this.datePipe.transform(
        suite.createdOn,
        'shortDate',
        '',
        getSupportedBrowserLang(this.translateService),
      );
      const time = this.datePipe.transform(
        suite.createdOn,
        'shortTime',
        '',
        getSupportedBrowserLang(this.translateService),
      );
      const options: AutomSuiteComparator = {
        createdOnDate: date,
        createdOnTime: time,
        statuses: suite.statuses,
        kind: 'automSuiteComparator',
      };
      return executionStatusColumn(suite.id)
        .withOptions(options)
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Extendable(65))
        .withHeaderRenderer(AutomSuiteComparisonHeaderRendererComponent)
        .withContentPosition('center')
        .changeWidthCalculationStrategy(new Extendable(65))
        .build('automated-suite-execution-comparator');
    });
    this.gridService.addColumnAtIndex(comparatorColumns, columns.length);
  }

  private loadDatarows(testExecutionInfos: TestExecutionInfo[]) {
    const dataRows = testExecutionInfos.map((info, i) => {
      const { testName, dataset, testId, statusBySuite } = info;

      const data: any = {
        id: i + 1,
        testCaseName: testName,
        datasetName: dataset,
        testCaseId: testId,
      };

      this.suites.forEach((suite) => {
        data[suite.id] = statusBySuite[suite.id] ? statusBySuite[suite.id] : this.NONE_STATUS;
      });

      return { id: data.id, data };
    });

    this.gridService.loadInitialDataRows(dataRows, dataRows.length);
  }

  private initializeGridFilters() {
    this.gridService.addFilters(this.buildGridFilters());
  }

  private buildGridFilters(): GridFilter[] {
    const filters: FilterBuilder[] = [
      textResearchFilter(
        GridColumnId.testCaseName,
        ResearchColumnPrototype.TEST_CASE_NAME,
      ).alwaysActive(),
      textResearchFilter(
        GridColumnId.datasetName,
        ResearchColumnPrototype.DATASET_NAME,
      ).alwaysActive(),
    ];

    const additionalFilters: FilterBuilder[] = this.suites.map((suite) =>
      executionStatusFilter(suite.id, ResearchColumnPrototype.EXECUTION_STATUS).alwaysActive(),
    );

    return buildFilters([...filters, ...additionalFilters]);
  }
}
