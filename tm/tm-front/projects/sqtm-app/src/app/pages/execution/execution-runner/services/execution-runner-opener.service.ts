import { Inject, Injectable } from '@angular/core';
import {
  EXECUTION_DIALOG_RUNNER_HEIGHT,
  EXECUTION_DIALOG_RUNNER_URL,
  EXECUTION_DIALOG_RUNNER_WIDTH,
  EXECUTION_RUNNER_PROLOGUE_URL,
} from '../execution-runner.constant';
import { APP_BASE_HREF } from '@angular/common';
import { InterWindowCommunicationService } from 'sqtm-core';
import { filter } from 'rxjs/operators';
import { MODIF_DURING_EXEC_STEP_VIEW_URL } from '../../../detailed-views/modif-during-exec-step-view/modif-during-exec-step.constant';

// provided in AppComponent to allow proper opening of execution dialog even if
// navigation has occurred, especially in case of modification during execution
@Injectable()
export class ExecutionRunnerOpenerService {
  private _executionWindow: Window = null;

  constructor(
    @Inject(APP_BASE_HREF) private baseUrl: string,
    private interWindowCommunicationService: InterWindowCommunicationService,
  ) {}

  public initialize() {
    this.interWindowCommunicationService.interWindowMessages$
      .pipe(filter((message) => message.isTypeOf('REQUIRE-MODIFICATION-DURING-EXECUTION-PROLOGUE')))
      .subscribe((message) => {
        const payload = message.payload as RequireModifyDuringExecutionMessagePayload;
        this.openModifyRunningExecutionFromPrologue(payload.testCaseId, payload.executionId);
      });

    this.interWindowCommunicationService.interWindowMessages$
      .pipe(filter((message) => message.isTypeOf('REQUIRE-MODIFICATION-DURING-EXECUTION-STEP')))
      .subscribe((message) => {
        const payload = message.payload as RequireModifyDuringExecutionStepMessagePayload;
        this.openModifyRunningExecutionFromStep(payload.executionId, payload.executionStepId);
      });

    this.interWindowCommunicationService.interWindowMessages$
      .pipe(filter((message) => message.isTypeOf('MODIFICATION-DURING-EXECUTION')))
      .subscribe((message) => {
        const payload = message.payload as BackToExecutionMessagePayload;
        this.openExecutionPrologue(payload.executionId);
      });

    this.interWindowCommunicationService.interWindowMessages$
      .pipe(filter((message) => message.isTypeOf('MODIFICATION-DURING-EXECUTION-STEP')))
      .subscribe((message) => {
        const payload = message.payload as BackToExecutionMessagePayload;
        this.openExecutionAtStep(payload.executionId, payload.executionStepIndex);
      });
  }

  openExecutionPrologue(executionId: number) {
    const url = `${this.baseUrl}${EXECUTION_DIALOG_RUNNER_URL}/execution/${executionId}/${EXECUTION_RUNNER_PROLOGUE_URL}`;
    this.openExecutionWithProvidedUrl(url);
  }

  openModifyRunningExecutionFromPrologue(testCaseId: number, executionId: number) {
    const url = `${this.baseUrl}test-case-workspace/test-case/modification-during-exec/${testCaseId}/execution/${executionId}`;

    this.closeExistingExecutionWindow();

    this._executionWindow = window.open(url, '_blank');
  }

  openModifyRunningExecutionFromStep(executionId: number, stepId: number) {
    const url = `${this.baseUrl}${MODIF_DURING_EXEC_STEP_VIEW_URL}/execution/${executionId}/step/${stepId}`;

    this.closeExistingExecutionWindow();

    this._executionWindow = window.open(url, '_blank');
  }

  openExecutionAtStep(executionId: number, executionStepIndex: number) {
    // execution-runner/execution/40603/step/1
    const url = `${this.baseUrl}${EXECUTION_DIALOG_RUNNER_URL}/execution/${executionId}/step/${executionStepIndex}`;
    this.openExecutionWithProvidedUrl(url);
  }

  openExecutionWithProvidedUrl(url: string) {
    this.closeExistingExecutionWindow();

    this._executionWindow = window.open(
      url,
      '',
      `width=${EXECUTION_DIALOG_RUNNER_WIDTH},height=${EXECUTION_DIALOG_RUNNER_HEIGHT}`,
    );
  }

  private closeExistingExecutionWindow() {
    if (this._executionWindow) {
      this._executionWindow.close();
    }
  }
}

export interface RequireModifyDuringExecutionMessagePayload {
  testCaseId: number;
  executionId: number;
}

export interface RequireModifyDuringExecutionStepMessagePayload {
  executionId: number;
  executionStepId: number;
}

export interface BackToExecutionMessagePayload {
  executionId: number;
  executionStepIndex?: number;
}
