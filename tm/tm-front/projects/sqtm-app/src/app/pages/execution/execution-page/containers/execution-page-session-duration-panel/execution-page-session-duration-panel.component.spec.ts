import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExecutionPageSessionDurationPanelComponent } from './execution-page-session-duration-panel.component';
import { ExecutionPageService } from '../../services/execution-page.service';
import { EMPTY } from 'rxjs';
import { AppTestingUtilsModule } from '../../../../../utils/testing-utils/app-testing-utils.module';
import { TranslateModule } from '@ngx-translate/core';

describe('ExecutionPageSessionDurationPanelComponent', () => {
  let component: ExecutionPageSessionDurationPanelComponent;
  let fixture: ComponentFixture<ExecutionPageSessionDurationPanelComponent>;

  beforeEach(async () => {
    const executionPageService: ExecutionPageService = jasmine.createSpyObj([
      'runExploratoryExecution',
      'pauseExploratoryExecution',
      'stopExploratoryExecution',
    ]);

    executionPageService.componentData$ = EMPTY;

    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: ExecutionPageService,
          useValue: executionPageService,
        },
      ],
      declarations: [ExecutionPageSessionDurationPanelComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ExecutionPageSessionDurationPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
