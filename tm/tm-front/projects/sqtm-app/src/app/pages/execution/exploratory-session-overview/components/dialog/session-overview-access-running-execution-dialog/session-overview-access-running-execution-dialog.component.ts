import { ChangeDetectionStrategy, Component, OnDestroy, ViewContainerRef } from '@angular/core';
import { DialogConfiguration, DialogReference } from 'sqtm-core';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

export interface AccessRunningExecutionDialogConfiguration {
  id: string;
  titleKey: string;
  executionId: number;
  overviewId: number;
  anchor: string;
}

export function buildAccessRunningExecutionDialogDefinition(
  vcr: ViewContainerRef,
  executionId: number,
  overviewId: number,
  anchor: string,
): DialogConfiguration<AccessRunningExecutionDialogConfiguration> {
  const configuration: AccessRunningExecutionDialogConfiguration = {
    id: SessionOverviewAccessRunningExecutionDialogComponent.DIALOG_ID,
    titleKey: 'sqtm-core.campaign-workspace.exploratory-session-overview.dialog.running-execution',
    executionId: executionId,
    overviewId: overviewId,
    anchor: anchor,
  };
  return {
    id: 'access-running-execution',
    component: SessionOverviewAccessRunningExecutionDialogComponent,
    height: 240,
    width: 500,
    data: configuration,
    viewContainerReference: vcr,
  };
}

@Component({
  selector: 'sqtm-app-session-overview-access-running-execution-dialog',
  templateUrl: './session-overview-access-running-execution-dialog.component.html',
  styleUrls: ['./session-overview-access-running-execution-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SessionOverviewAccessRunningExecutionDialogComponent implements OnDestroy {
  static DIALOG_ID = 'access-running-execution-dialog';

  private unsub$ = new Subject<void>();

  configuration: AccessRunningExecutionDialogConfiguration;

  constructor(
    private dialogReference: DialogReference<AccessRunningExecutionDialogConfiguration>,
    private router: Router,
  ) {
    this.configuration = this.dialogReference.data;
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  consultActivity() {
    this.router.navigate(['session-overview', this.configuration.overviewId, 'activity']);
    this.dialogReference.close();
  }

  accessExecution() {
    this.router.navigate(['execution', this.configuration.executionId, 'content'], {
      queryParams: { anchor: this.configuration.anchor },
    });
    this.dialogReference.close();
  }
}
