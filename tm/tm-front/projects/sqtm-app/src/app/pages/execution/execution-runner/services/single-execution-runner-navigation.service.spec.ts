import { TestBed } from '@angular/core/testing';

import { SingleExecutionRunnerNavigationService } from './single-execution-runner-navigation.service';
import { ExecutionRunnerNavigationService } from './execution-runner-navigation.service';
import { AppTestingUtilsModule } from '../../../../utils/testing-utils/app-testing-utils.module';
import { RouterTestingModule } from '@angular/router/testing';
import { OverlayModule } from '@angular/cdk/overlay';

describe('SingleExecutionRunnerNavigationService', () => {
  let service: SingleExecutionRunnerNavigationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, RouterTestingModule, OverlayModule],
      providers: [
        SingleExecutionRunnerNavigationService,
        {
          provide: ExecutionRunnerNavigationService,
          useClass: SingleExecutionRunnerNavigationService,
        },
      ],
    });
    service = TestBed.inject(SingleExecutionRunnerNavigationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
