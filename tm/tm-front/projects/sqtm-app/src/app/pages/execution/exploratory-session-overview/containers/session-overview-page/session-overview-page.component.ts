import {
  ChangeDetectionStrategy,
  Component,
  computed,
  OnInit,
  Signal,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import {
  AttachmentDrawerComponent,
  CampaignPermissions,
  CapsuleInformationData,
  EntityViewComponentData,
  EntityViewService,
  ExecutionStatus,
  GenericEntityViewService,
  ReferentialDataService,
  ReviewStatus,
  RichTextAttachmentDelegate,
  SessionOverviewStatus,
  SprintStatus,
} from 'sqtm-core';
import { map, switchMap, take } from 'rxjs/operators';
import { SessionOverviewPageState } from '../../states/session-overview-page-state';
import { SessionOverviewPageService } from '../../services/session-overview-page.service';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sqtm-app-session-overview-page',
  templateUrl: './session-overview-page.component.html',
  styleUrls: ['./session-overview-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: SessionOverviewPageService,
      useClass: SessionOverviewPageService,
    },
    {
      provide: EntityViewService,
      useExisting: SessionOverviewPageService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: SessionOverviewPageService,
    },
    {
      provide: RichTextAttachmentDelegate,
      useExisting: SessionOverviewPageService,
    },
  ],
})
export class SessionOverviewPageComponent implements OnInit {
  @ViewChild(AttachmentDrawerComponent)
  attachmentDrawer: AttachmentDrawerComponent;

  $componentData: Signal<SessionOverviewPageComponentData> = toSignal(
    this.sessionOverviewPageService.componentData$,
  );

  $canStartSession: Signal<boolean> = computed(() => {
    return (
      !this.isRunning(this.$componentData().exploratorySessionOverview.sessionStatus) &&
      this.$componentData().permissions.canExecute &&
      this.$componentData().milestonesAllowModification &&
      this.$componentData().exploratorySessionOverview.sprintStatus !== SprintStatus.CLOSED.id
    );
  });

  $canEndSession: Signal<boolean> = computed(() => {
    return (
      this.isRunning(this.$componentData().exploratorySessionOverview.sessionStatus) &&
      this.$componentData().permissions.canExecute &&
      this.$componentData().milestonesAllowModification &&
      this.$componentData().exploratorySessionOverview.sprintStatus !== SprintStatus.CLOSED.id
    );
  });

  constructor(
    private route: ActivatedRoute,
    private referentialDataService: ReferentialDataService,
    private readonly sessionOverviewPageService: SessionOverviewPageService,
  ) {}

  ngOnInit(): void {
    this.referentialDataService
      .refresh()
      .pipe(
        switchMap(() => this.route.paramMap),
        take(1),
        map((params: ParamMap) => params.get('sessionOverviewId')),
      )
      .subscribe((exploratorySessionIdParam: string) => {
        const sessionOverviewId = parseInt(exploratorySessionIdParam, 10);
        this.sessionOverviewPageService.load(sessionOverviewId);
      });
  }

  get canAttach() {
    return (
      this.$componentData().permissions.canAttach &&
      this.$componentData().milestonesAllowModification &&
      this.$componentData().exploratorySessionOverview.sprintStatus !== SprintStatus.CLOSED.id
    );
  }

  navigateBack(): void {
    history.back();
  }

  getExploratorySessionStatus(sessionStatus: string): CapsuleInformationData {
    return {
      id: SessionOverviewStatus[sessionStatus].id,
      labelI18nKey: SessionOverviewStatus[sessionStatus].i18nKey,
    };
  }

  getExploratorySessionExecutionStatus(executionStatus: string): CapsuleInformationData {
    return {
      id: ExecutionStatus[executionStatus].id,
      color: ExecutionStatus[executionStatus].color,
      icon: 'sqtm-core-campaign:exec_status',
      labelI18nKey: ExecutionStatus[executionStatus].i18nKey,
    };
  }

  getExploratorySessionReviewStatus(
    componentData: SessionOverviewPageComponentData,
  ): CapsuleInformationData {
    let reviewStatus = componentData.exploratorySessionOverview.inferredSessionReviewStatus;
    if (componentData.exploratorySessionOverview.nbExecutions === 0) {
      reviewStatus = ReviewStatus.TO_DO.id;
    }

    return {
      id: reviewStatus,
      labelI18nKey: ReviewStatus[reviewStatus].i18nKey,
    };
  }

  startOrResumeSessionOverview() {
    this.sessionOverviewPageService.startOrResumeSessionOverview().subscribe();
  }

  endSessionOverview() {
    this.sessionOverviewPageService.endSessionOverview().subscribe();
  }

  getStartLabel(sessionStatus: string): string {
    if (sessionStatus === SessionOverviewStatus.TO_DO.id) {
      return 'sqtm-core.campaign-workspace.exploratory-session-overview.start-session';
    } else {
      return 'sqtm-core.campaign-workspace.exploratory-session-overview.resume-session';
    }
  }

  isRunning(sessionStatus: string) {
    return sessionStatus === SessionOverviewStatus.RUNNING.id;
  }

  isSessionStatusFinished(sessionStatus: string) {
    return sessionStatus === SessionOverviewStatus.FINISHED.id;
  }

  openAttachmentsDrawer() {
    this.attachmentDrawer.open();
  }
}

export interface SessionOverviewPageComponentData
  extends EntityViewComponentData<
    SessionOverviewPageState,
    'exploratorySessionOverview',
    CampaignPermissions
  > {}
