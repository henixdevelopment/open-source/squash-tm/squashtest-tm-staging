import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ExecutionRunnerNavigationService } from './execution-runner-navigation.service';
import { DialogService, TestPlanResumeModel } from 'sqtm-core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export abstract class TestPlanRunnerNavigationService extends ExecutionRunnerNavigationService {
  // no need for immutable state here, we are in a non reactive context
  protected state: TestPlanRunnerNavigationState = {
    // iterationId or testSuiteId
    testPlanOwnerId: null,
    testPlanItemId: null,
    hasNextTestCase: false,
    partialTestPlanItemIds: [],
  };

  _activateFastForwardButton = new BehaviorSubject<boolean>(false);

  activateFastForwardButton$: Observable<boolean> = this._activateFastForwardButton.asObservable();

  protected constructor(router: Router, dialogService: DialogService) {
    super(router, dialogService);
  }

  // allow external change to testPlanOwnerId and testPlanItemId
  // used when opening an execution runner at an arbitrary url
  changeTestPlanItem(
    testPlanOwnerId: number,
    testPlanItemId: number,
    hasNextTestCase: boolean,
    partialTestPlanItemIds?: number[],
  ) {
    this.state = {
      ...this.state,
      testPlanOwnerId,
      testPlanItemId,
      hasNextTestCase,
      partialTestPlanItemIds,
    };
    this._activateFastForwardButton.next(hasNextTestCase);
  }

  handleNextOnLastStep() {
    if (this.state.hasNextTestCase) {
      this.getNextExecution().subscribe((testPlanResumeModel: TestPlanResumeModel) => {
        this.changeTestPlanItem(
          this.extractTestPlanOwnerId(testPlanResumeModel),
          testPlanResumeModel.testPlanItemId,
          testPlanResumeModel.hasNextTestCase,
          testPlanResumeModel.partialTestPlanItemIds,
        );
        if (testPlanResumeModel.initialStepIndex) {
          this.navigateToStep({
            executionId: testPlanResumeModel.executionId,
            stepIndex: testPlanResumeModel.initialStepIndex,
          });
        } else {
          this.navigateToPrologue(testPlanResumeModel.executionId);
        }
        this._activateFastForwardButton.next(testPlanResumeModel.hasNextTestCase);
      });
    } else {
      this.showEndOfExecutionDialog();
    }
  }

  protected abstract extractTestPlanOwnerId(testPlanResumeModel: TestPlanResumeModel): number;

  protected abstract getNextExecution();

  protected getEndOfExecutionKey(): string {
    return 'sqtm-core.campaign-workspace.execution-runner.dialog.message.end-of-bulk-execution';
  }

  hasNextTestCase(): boolean {
    return this.state.hasNextTestCase;
  }

  getPartialTestPlanItemIds(): number[] {
    return this.state.partialTestPlanItemIds;
  }

  complete() {
    this._activateFastForwardButton.complete();
  }
}

interface TestPlanRunnerNavigationState {
  testPlanOwnerId: number;
  testPlanItemId: number;
  hasNextTestCase: boolean;
  partialTestPlanItemIds?: number[];
}
