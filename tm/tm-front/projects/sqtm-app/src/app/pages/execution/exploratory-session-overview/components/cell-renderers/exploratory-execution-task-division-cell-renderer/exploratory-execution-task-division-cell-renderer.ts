import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Signal,
  ViewChild,
} from '@angular/core';
import { catchError, finalize } from 'rxjs/operators';
import {
  AbstractCellRendererComponent,
  ActionErrorDisplayService,
  ColumnDefinitionBuilder,
  EditableTextFieldComponent,
  GridColumnId,
  GridService,
  isEnterKeyboardEvent,
  isEscapeKeyboardEvent,
  SprintStatus,
  TableValueChange,
} from 'sqtm-core';
import { SessionOverviewPageService } from '../../../services/session-overview-page.service';
import { SessionOverviewPageComponentData } from '../../../containers/session-overview-page/session-overview-page.component';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sqtm-app-exploratory-execution-task-division-cell-renderer',
  templateUrl: 'exploratory-execution-task-division-cell-renderer.html',
  styleUrls: ['./exploratory-execution-task-division-cell-renderer.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExploratoryExecutionTaskDivisionCellRenderer extends AbstractCellRendererComponent {
  edit = false;

  $componentData: Signal<SessionOverviewPageComponentData>;

  @ViewChild('input')
  input: ElementRef;

  @ViewChild('editableTextField')
  editableTextField: EditableTextFieldComponent;

  constructor(
    public readonly grid: GridService,
    public readonly cdRef: ChangeDetectorRef,
    public readonly sessionOverviewPageService: SessionOverviewPageService,
    public readonly actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    super(grid, cdRef);
    this.$componentData = toSignal(this.sessionOverviewPageService.componentData$);
  }

  get canEdit() {
    return (
      this.$componentData().permissions.canExecute &&
      this.$componentData().milestonesAllowModification &&
      this.$componentData().exploratorySessionOverview.sprintStatus !== SprintStatus.CLOSED.id
    );
  }

  handleBlur() {
    this.disableEditMode();
  }

  disableEditMode() {
    if (this.edit) {
      this.edit = false;
    }
  }

  handleKeyboardInput($event: KeyboardEvent) {
    if (isEnterKeyboardEvent($event)) {
      const inputElement = this.input.nativeElement as HTMLInputElement;
      const changedValue: TableValueChange = {
        columnId: this.columnDisplay.id,
        value: inputElement.value,
      };
      this.disableEditMode();
      this.grid.editRows([this.row.id], [changedValue]);
    } else if (isEscapeKeyboardEvent($event)) {
      this.disableEditMode();
    }
    return false;
  }

  updateTaskDivision(newTaskDivision: string) {
    this.editableTextField.beginAsync();
    const executionId = Number(this.row.data[GridColumnId.executionId]);
    this.sessionOverviewPageService
      .updateTaskDivision(executionId, newTaskDivision)
      .pipe(
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => {
          this.editableTextField.cancel();
          this.editableTextField.endAsync();
        }),
      )
      .subscribe(() => {
        const tableValueChange: TableValueChange = {
          columnId: this.columnDisplay.id,
          value: newTaskDivision,
        };
        this.grid.editRows([this.row.id], [tableValueChange]);
      });
  }
}

export function exploratoryExecutionTaskDivisionColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(ExploratoryExecutionTaskDivisionCellRenderer)
    .withHeaderPosition('left');
}
