import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  GridService,
  SessionNoteKind,
  SessionNoteKindKeys,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-exploratory-execution-note-types-cell-renderer',
  templateUrl: './exploratory-execution-note-types-cell-renderer.component.html',
  styleUrls: ['./exploratory-execution-note-types-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExploratoryExecutionNoteTypesCellRendererComponent extends AbstractCellRendererComponent {
  constructor(
    public readonly translateService: TranslateService,
    public readonly gridService: GridService,
    public readonly cdRef: ChangeDetectorRef,
  ) {
    super(gridService, cdRef);
  }

  noteTypes: SessionNoteKindKeys[] = ['COMMENT', 'SUGGESTION', 'BUG', 'QUESTION', 'POSITIVE'];

  getTextColor(noteType: SessionNoteKindKeys): string {
    return SessionNoteKind[noteType].textColor;
  }

  getBackgroundColor(noteType: SessionNoteKindKeys): string {
    return SessionNoteKind[noteType].backgroundColor;
  }

  getFirstLetter(noteType: SessionNoteKindKeys): string {
    return this.translateService.instant(SessionNoteKind[noteType].i18nKey)[0];
  }

  getFullTextWithNumber(noteType: SessionNoteKindKeys): string {
    const noteTypes = this.row.data.noteTypes;
    if (noteTypes && noteTypes.includes(noteType)) {
      const noteTypesArray = noteTypes.split(',');
      const count = noteTypesArray.filter((type: string) => type === noteType).length;
      return `${this.translateService.instant(SessionNoteKind[noteType].i18nKey)} (${count})`;
    } else {
      return this.translateService.instant(SessionNoteKind[noteType].i18nKey);
    }
  }

  isNoteTypePresentInExecution(noteType: SessionNoteKindKeys): boolean {
    if (this.row.data.noteTypes) {
      const noteTypes = this.row.data.noteTypes.split(',');
      return noteTypes.includes(noteType);
    }
  }
}
