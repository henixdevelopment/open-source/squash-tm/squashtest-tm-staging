import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';
import { SessionOverviewPageService } from '../../../services/session-overview-page.service';
import {
  DataRowModel,
  DateFormatUtils,
  EntityViewAttachmentHelperService,
  GridColumnId,
  GridResponse,
  GroupedMultiListFieldComponent,
  IssuesService,
  ListItem,
  RestService,
  SessionNoteKind,
  SessionNoteModel,
} from 'sqtm-core';
import { map, switchMap, take, tap } from 'rxjs/operators';
import { ExistingSessionNoteState } from '../../../../states/execution-state';
import _ from 'lodash';

@Component({
  selector: 'sqtm-app-session-overview-page-activity',
  templateUrl: './session-overview-page-activity.component.html',
  styleUrls: ['./session-overview-page-activity.component.less'],
  providers: [IssuesService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SessionOverviewPageActivityComponent implements OnInit {
  @ViewChild('noteKindFilter')
  noteKindsList: GroupedMultiListFieldComponent;

  @ViewChild('userFilter')
  userFilter: GroupedMultiListFieldComponent;

  sessionNotes: ExistingSessionNoteState[] = [];
  filteredNotes: ExistingSessionNoteState[] = [];
  issues: DataRowModel[] = [];
  loading = true;
  users: ListItem[] = [];
  noteKindItems: ListItem[] = [
    { id: SessionNoteKind.COMMENT.id, i18nLabelKey: SessionNoteKind.COMMENT.i18nKey },
    { id: SessionNoteKind.SUGGESTION.id, i18nLabelKey: SessionNoteKind.SUGGESTION.i18nKey },
    { id: SessionNoteKind.BUG.id, i18nLabelKey: SessionNoteKind.BUG.i18nKey },
    { id: SessionNoteKind.QUESTION.id, i18nLabelKey: SessionNoteKind.QUESTION.i18nKey },
    { id: SessionNoteKind.POSITIVE.id, i18nLabelKey: SessionNoteKind.POSITIVE.i18nKey },
  ];

  get hasActivityToShow(): boolean {
    return this.sessionNotes.length > 0;
  }

  constructor(
    public readonly sessionOverviewPageService: SessionOverviewPageService,
    public readonly restService: RestService,
    private readonly attachmentHelper: EntityViewAttachmentHelperService,
    public readonly cdRef: ChangeDetectorRef,
    public readonly issuesService: IssuesService,
  ) {}

  ngOnInit(): void {
    this.loadSessionNotes();
  }

  private loadSessionNotes(): void {
    this.sessionOverviewPageService.componentData$
      .pipe(
        take(1),
        map((componentData) => componentData.exploratorySessionOverview.id),
        tap((overviewId) => this.loadIssues(overviewId)),
        switchMap((sessionOverviewId) => {
          return this.restService.get<SessionNoteModel[]>([
            'session-overview',
            sessionOverviewId.toString(),
            'session-notes',
          ]);
        }),
        tap((sessionNotes) =>
          this.sessionOverviewPageService.updateSessionNoteCount(sessionNotes.length),
        ),
      )
      .subscribe((sessionNotes) => {
        this.sessionNotes = sessionNotes.map((sessionNote) =>
          this.transformSessionNoteModelIntoState(sessionNote),
        );
        this.filteredNotes = this.sessionNotes;
        this.extractUserListItems();
        this.clearFilters();
        this.loading = false;
        this.cdRef.detectChanges();
      });
  }

  private extractUserListItems(): void {
    const distinctUsers = _.uniq(
      this.sessionNotes.map((note) => note.lastModifiedBy ?? note.createdBy),
    );

    distinctUsers.sort((a, b) => a.localeCompare(b));

    this.users = distinctUsers.map((user) => ({ id: user, label: user, selected: false }));
  }

  private loadIssues(overviewId: number): any {
    this.issuesService.loadModel(overviewId, 'SESSION_OVERVIEW_TYPE').subscribe((issueModel) => {
      const isAuthenticated =
        issueModel.modelLoaded && issueModel.bugTrackerStatus === 'AUTHENTICATED';

      if (isAuthenticated) {
        this.restService
          .post([`issues/session-overview/${overviewId}/all-known-issues`])
          .pipe(
            take(1),
            tap((gridResponse: GridResponse) => {
              this.issues = gridResponse.dataRows;
              this.cdRef.detectChanges();
            }),
          )
          .subscribe();
      }
    });
  }

  private transformSessionNoteModelIntoState(
    model: SessionNoteModel,
    expanded = true,
  ): ExistingSessionNoteState {
    const attachmentState = this.attachmentHelper.initializeAttachmentState(
      model.attachmentList.attachments,
    );
    const attachmentList = { id: model.attachmentList.id, attachments: attachmentState };

    return {
      noteContent: model.content,
      noteKind: model.kind,
      id: model.noteId,
      noteOrder: model.noteOrder,
      displayOrder: model.noteOrder,
      createdBy: model.createdBy,
      createdOn: DateFormatUtils.createDateFromIsoString(model.createdOn),
      lastModifiedBy: model.lastModifiedBy,
      lastModifiedOn: DateFormatUtils.createDateFromIsoString(model.lastModifiedOn),
      attachmentList: attachmentList,
      discriminator: 'existing-note',
      showPlaceHolder: false,
      expanded,
    };
  }

  getNoteIssues(note: ExistingSessionNoteState): DataRowModel[] {
    return this.issues.filter((issue) => issue.data[GridColumnId.executionSteps].includes(note.id));
  }

  selectedKindsChanged(noteKinds: ListItem[]) {
    this.noteKindsList.selectedItems = noteKinds;
    this.applyFiltering();
  }

  selectedUsersChanged(users: ListItem[]): void {
    this.userFilter.selectedItems = users;
    this.applyFiltering();
  }

  private applyFiltering() {
    this.filteredNotes = this.sessionNotes;

    if (this.noteKindsList.selectedItems.length > 0) {
      this.filteredNotes = this.filteredNotes.filter((note) =>
        this.noteKindsList.selectedItems.some((noteKind) => noteKind.id === note.noteKind),
      );
    }

    if (this.userFilter.selectedItems.length > 0) {
      this.filteredNotes = this.filteredNotes.filter((note) =>
        this.userFilter.selectedItems.some(
          (user) => user.id === (note.lastModifiedBy ?? note.createdBy),
        ),
      );
    }
  }

  clearFilters(): void {
    this.noteKindsList.selectedItems = [];
    this.userFilter.selectedItems = [];
    this.applyFiltering();
  }

  collapseAllNotes(): void {
    this.sessionNotes.forEach((note) => (note.expanded = false));
  }

  expandAllNotes(): void {
    this.sessionNotes.forEach((note) => (note.expanded = true));
  }

  refresh(): void {
    this.loading = true;
    this.cdRef.detectChanges();
    this.loadSessionNotes();
  }
}
