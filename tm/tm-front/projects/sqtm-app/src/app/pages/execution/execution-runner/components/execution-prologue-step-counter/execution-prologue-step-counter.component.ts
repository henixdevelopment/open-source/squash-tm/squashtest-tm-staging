import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'sqtm-app-execution-prologue-step-counter',
  templateUrl: './execution-prologue-step-counter.component.html',
  styleUrls: ['./execution-prologue-step-counter.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExecutionPrologueStepCounterComponent {
  @Input()
  stepCount: number;
}
