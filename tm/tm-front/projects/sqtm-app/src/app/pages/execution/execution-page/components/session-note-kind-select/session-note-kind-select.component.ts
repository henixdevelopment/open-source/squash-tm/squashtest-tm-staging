import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { ListPanelItem, SessionNoteKind, SessionNoteKindKeys } from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { TemplatePortal } from '@angular/cdk/portal';

@Component({
  selector: 'sqtm-app-session-note-kind-select',
  templateUrl: './session-note-kind-select.component.html',
  styleUrls: ['./session-note-kind-select.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SessionNoteKindSelectComponent {
  _selectedNoteKind: SessionNoteKindKeys;
  private overlayRef: OverlayRef;

  @Output()
  noteKindSelectionChanged = new EventEmitter<SessionNoteKindKeys>();

  @ViewChild('templatePortalContent', { read: TemplateRef })
  templatePortalContent: TemplateRef<any>;

  @ViewChild('listItemRef', { read: ElementRef })
  listItemRef: ElementRef;

  @Input()
  noteKind: SessionNoteKindKeys = 'COMMENT';

  @Input()
  set selectedNoteKind(value: SessionNoteKindKeys) {
    this._selectedNoteKind = value;
  }

  @Input()
  editable: boolean;

  @Input()
  isAddNote: boolean;

  listPanelItems: ListPanelItem[] = [
    { id: 'COMMENT', label: this.translateService.instant(SessionNoteKind.COMMENT.i18nKey) },
    { id: 'SUGGESTION', label: this.translateService.instant(SessionNoteKind.SUGGESTION.i18nKey) },
    { id: 'BUG', label: this.translateService.instant(SessionNoteKind.BUG.i18nKey) },
    { id: 'QUESTION', label: this.translateService.instant(SessionNoteKind.QUESTION.i18nKey) },
    { id: 'POSITIVE', label: this.translateService.instant(SessionNoteKind.POSITIVE.i18nKey) },
  ];
  active: any;

  constructor(
    private translateService: TranslateService,
    private cdRef: ChangeDetectorRef,
    private overlay: Overlay,
    private vcr: ViewContainerRef,
  ) {}

  getBackgroundColor() {
    return SessionNoteKind[this.noteKind].backgroundColor;
  }

  getBorderColor() {
    return SessionNoteKind[this.noteKind].backgroundColor;
  }

  getTextColor() {
    return SessionNoteKind[this.noteKind].textColor;
  }

  getI18nKey() {
    return SessionNoteKind[this.noteKind].i18nKey;
  }

  updateNoteKind(kind: SessionNoteKindKeys) {
    this.selectedNoteKind = kind;
    this.noteKindSelectionChanged.emit(kind);
    this.close();
  }

  close() {
    this.removeOverlay();
    this.cdRef.detectChanges();
  }

  private removeOverlay() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
    }
  }

  showList() {
    if (this.editable) {
      this.showKindList(this.listItemRef, this.templatePortalContent);
    }
  }

  showKindList(elementRef: ElementRef, templateRef: TemplateRef<any>) {
    const positionStrategy = this.overlay
      .position()
      .flexibleConnectedTo(elementRef)
      .withPositions([
        {
          originX: 'center',
          overlayX: 'center',
          originY: 'center',
          overlayY: 'top',
          offsetY: 13,
          offsetX: 0,
        },
      ]);
    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
    };
    this.overlayRef = this.overlay.create(overlayConfig);
    const templatePortal = new TemplatePortal(templateRef, this.vcr);
    this.overlayRef.attach(templatePortal);
    this.overlayRef.backdropClick().subscribe(() => this.close());
  }
}
