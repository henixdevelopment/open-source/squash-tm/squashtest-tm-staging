import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { ExecutionPageService } from '../../services/execution-page.service';
import { isExistingSessionNoteState, SessionNoteState } from '../../../states/execution-state';
import { DraggableItemHandlerDirective, DraggableListItemDirective } from 'sqtm-core';
import { SessionNoteDndData } from '../../session-note-dnd-data';
import { filter, fromEvent, map, Observable, Subject, switchMap, take, takeUntil } from 'rxjs';
import { ExecutionPageComponentData } from '../../containers/abstract-execution-page.component';

@Component({
  selector: 'sqtm-app-session-note-container',
  templateUrl: './session-note-container.component.html',
  styleUrls: ['./session-note-container.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SessionNoteContainerComponent implements OnInit, OnDestroy {
  @Input()
  index: number;

  @Input()
  canMove: boolean;

  @Input()
  canAttach: boolean;

  @Input()
  canDragAndDrop: boolean;

  @Input()
  sessionNote: SessionNoteState;

  @Output()
  refreshIssuesData = new EventEmitter<void>();

  @ViewChild('dropFileZone', { read: ElementRef })
  private dropFileZone: ElementRef;

  @ViewChild(DraggableListItemDirective, { static: true })
  draggableListItem: DraggableListItemDirective;

  @ViewChild(DraggableItemHandlerDirective, { static: true })
  dragHandler: DraggableItemHandlerDirective;

  private draggedSteps = new Subject<SessionNoteDndData>();

  private readonly unsub$ = new Subject<void>();

  selectedNoteIds$: Observable<number[]>;

  get sessionNoteId(): number {
    // Defaults to -1 if there's no session note model: it means that we're in the creation form
    return this.sessionNote?.id ?? -1;
  }

  constructor(
    readonly executionPageService: ExecutionPageService,
    private readonly renderer: Renderer2,
  ) {
    this.selectedNoteIds$ = this.executionPageService.componentData$.pipe(
      map((componentData) => componentData.execution.sessionNotes.selectedNoteIds),
    );
  }

  ngOnInit(): void {
    this.draggableListItem.connectDataSource(this.draggedSteps);

    // fixing dnd data when user mouse down on the drag handler in case of it could be a dnd.
    // we cannot just react to the dnd start, as data must be fixed at dnd init
    fromEvent(this.dragHandler.host.nativeElement, 'mousedown')
      .pipe(
        takeUntil(this.unsub$),
        filter(() => this.canDragAndDrop),
        switchMap(() => this.executionPageService.componentData$.pipe(take(1))),
      )
      .subscribe((componentData: ExecutionPageComponentData) => {
        const sessionNotes = componentData.execution.sessionNotes;
        const selectedNoteIds = sessionNotes.selectedNoteIds;
        let draggedNoteIds = selectedNoteIds;
        if (!selectedNoteIds.includes(this.sessionNote.id)) {
          draggedNoteIds = [this.sessionNote.id];
        }
        const draggedNotes = draggedNoteIds.map((id) => sessionNotes.entities[id]);
        this.draggedSteps.next(new SessionNoteDndData(draggedNotes));
      });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  addAttachment(files: File[]) {
    if (this.canAttach && isExistingSessionNoteState(this.sessionNote)) {
      this.executionPageService.addAttachmentsToNote(
        files,
        this.sessionNote.id,
        this.sessionNote.attachmentList.id,
      );
      this.unmarkAsFileDropZone();
    }
  }

  handleAttachmentEnter() {
    if (this.canAttach && this.sessionNote) {
      this.renderer.addClass(this.dropFileZone.nativeElement, 'file-is-over');
    }
  }

  handleAttachmentLeave() {
    if (this.canAttach && this.sessionNote) {
      this.unmarkAsFileDropZone();
    }
  }

  unmarkAsFileDropZone() {
    this.renderer.removeClass(this.dropFileZone.nativeElement, 'file-is-over');
  }

  moveUp(): void {
    if (this.canMove) {
      this.executionPageService.moveNoteUp(this.sessionNoteId).subscribe(() => {
        this.refreshIssuesData.emit();
      });
    }
  }

  moveDown(): void {
    if (this.canMove) {
      this.executionPageService.moveNoteDown(this.sessionNoteId).subscribe(() => {
        this.refreshIssuesData.emit();
      });
    }
  }
}
