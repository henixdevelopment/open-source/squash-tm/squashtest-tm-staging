import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SessionOverviewPageComponent } from './containers/session-overview-page/session-overview-page.component';
import {
  AnchorModule,
  AttachmentModule,
  CellRendererCommonModule,
  DialogModule,
  GridExportModule,
  GridModule,
  IssuesModule,
  NavBarModule,
  ReferentialDataService,
  UiManagerModule,
  WorkspaceLayoutModule,
  WorkspaceCommonModule,
} from 'sqtm-core';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule, Routes } from '@angular/router';
import { SessionOverviewPageContentComponent } from './containers/panel-groups/session-overview-page-content/session-overview-page-content.component';
import { SessionOverviewPageInformationPanelComponent } from './components/panels/session-overview-page-information-panel/session-overview-page-information-panel.component';
import { SessionOverviewPageExecutionsPanelComponent } from './components/panels/session-overview-page-executions-panel/session-overview-page-executions-panel.component';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { SessionExecutionStartCellRendererComponent } from './components/cell-renderers/session-execution-start-cell-renderer/session-execution-start-cell-renderer.component';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { SessionOverviewPageIssuesComponent } from './containers/panel-groups/session-overview-page-issues/session-overview-page-issues.component';
import { ExploratoryExecutionNoteTypesCellRendererComponent } from './components/cell-renderers/exploratory-execution-note-types-cell-renderer/exploratory-execution-note-types-cell-renderer.component';
import { ExploratoryExecutionTaskDivisionCellRenderer } from './components/cell-renderers/exploratory-execution-task-division-cell-renderer/exploratory-execution-task-division-cell-renderer';
import { SessionOverviewPageCharterPanelComponent } from './components/panels/session-overview-page-charter-panel/session-overview-page-charter-panel.component';
import { DeleteExecutionCellRendererComponent } from './components/cell-renderers/delete-execution-cell-renderer/delete-execution-cell-renderer.component';
import { SessionOverviewAddMultipleExecutionsDialogComponent } from './components/dialog/session-overview-add-multiple-executions-dialog/session-overview-add-multiple-executions-dialog.component';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzWaveModule } from 'ng-zorro-antd/core/wave';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { SessionOverviewAccessRunningExecutionDialogComponent } from './components/dialog/session-overview-access-running-execution-dialog/session-overview-access-running-execution-dialog.component';
import { BooleanReviewLinkCellRendererComponent } from './components/cell-renderers/boolean-review-link-cell-renderer/boolean-review-link-cell-renderer.component';
import { SessionOverviewPageActivityComponent } from './containers/panel-groups/session-overview-page-activity/session-overview-page-activity.component';
import { ReadonlySessionNoteComponent } from './components/readonly-session-note/readonly-session-note.component';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzMenuModule } from 'ng-zorro-antd/menu';

export const routes: Routes = [
  {
    path: ':sessionOverviewId',
    component: SessionOverviewPageComponent,
    children: [
      {
        path: 'content',
        component: SessionOverviewPageContentComponent,
      },
      {
        path: 'issues',
        component: SessionOverviewPageIssuesComponent,
      },
      {
        path: 'activity',
        component: SessionOverviewPageActivityComponent,
      },
    ],
  },
];

@NgModule({
  declarations: [
    SessionOverviewPageComponent,
    SessionOverviewPageContentComponent,
    SessionOverviewPageInformationPanelComponent,
    SessionOverviewPageExecutionsPanelComponent,
    SessionExecutionStartCellRendererComponent,
    SessionOverviewPageIssuesComponent,
    SessionExecutionStartCellRendererComponent,
    ExploratoryExecutionNoteTypesCellRendererComponent,
    ExploratoryExecutionTaskDivisionCellRenderer,
    DeleteExecutionCellRendererComponent,
    SessionOverviewPageCharterPanelComponent,
    SessionOverviewAddMultipleExecutionsDialogComponent,
    SessionOverviewAccessRunningExecutionDialogComponent,
    BooleanReviewLinkCellRendererComponent,
    SessionOverviewPageActivityComponent,
    ReadonlySessionNoteComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    WorkspaceCommonModule,
    AnchorModule,
    NzToolTipModule,
    TranslateModule,
    NavBarModule,
    UiManagerModule,
    WorkspaceLayoutModule,
    NzCollapseModule,
    GridModule,
    NzIconModule,
    IssuesModule,
    GridExportModule,
    CellRendererCommonModule,
    AttachmentModule,
    DialogModule,
    NzButtonModule,
    NzWaveModule,
    NzCheckboxModule,
    WorkspaceCommonModule,
    NzDropDownModule,
    NzMenuModule,
  ],
})
export class SessionOverviewModule implements OnInit {
  constructor(private referentialDataService: ReferentialDataService) {}

  ngOnInit(): void {
    this.referentialDataService.refresh();
  }
}
