import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { ExistingSessionNoteState } from '../../../states/execution-state';
import { ExecutionPageService } from '../../services/execution-page.service';
import { ExecutionPageComponentData } from '../../containers/abstract-execution-page.component';
import {
  ActionErrorDisplayService,
  Attachment,
  AttachmentHolderType,
  DataRowModel,
  DialogReference,
  DialogService,
  EditableRichTextComponent,
  getPersistedAttachmentCount,
  PersistedAttachment,
  RejectedAttachment,
  RichTextAttachmentDelegate,
  SessionNoteKindKeys,
  SprintStatus,
} from 'sqtm-core';
import { catchError, filter, finalize, switchMap, take, takeUntil } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import {
  getRemoteIssueDialogConfiguration,
  RemoteIssueDialogData,
} from '../../../../../components/remote-issue/containers/remote-issue-dialog/remote-issue-dialog.component';

@Component({
  selector: 'sqtm-app-session-note',
  templateUrl: './session-note.component.html',
  styleUrls: ['./session-note.component.less'],
  providers: [
    {
      provide: RichTextAttachmentDelegate,
      useExisting: ExecutionPageService,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SessionNoteComponent implements OnDestroy {
  @Input()
  note: ExistingSessionNoteState;

  @Input()
  editable: boolean;

  @Input()
  isSessionStopped: boolean;

  @Input()
  isSessionRunning: boolean;

  @Input()
  authenticatedToBugtracker: boolean;

  @Input()
  issues: DataRowModel[];

  @Output()
  refreshIssuesData = new EventEmitter<void>();

  @Output()
  addButtonClicked = new EventEmitter<void>();

  @ViewChild('richTextNote')
  richTextNote: EditableRichTextComponent;

  private unsub$ = new Subject<void>();

  protected readonly AttachmentHolderType = AttachmentHolderType;

  get lastModificationDate(): string {
    if (this.note == null) {
      return null;
    }

    return (this.note.lastModifiedOn ?? this.note.createdOn).toString();
  }

  get lastModificationUser(): string {
    if (this.note == null) {
      return null;
    }

    return this.note.lastModifiedBy ?? this.note.createdBy;
  }

  constructor(
    public readonly executionPageService: ExecutionPageService,
    private readonly dialogService: DialogService,
    private readonly viewContainerRef: ViewContainerRef,
    private readonly actionErrorDisplayService: ActionErrorDisplayService,
  ) {}

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  expandNote() {
    this.executionPageService.expandNote(this.note.id);
  }

  collapseNote() {
    this.executionPageService.collapseNote(this.note.id);
  }

  updateKind(kind: SessionNoteKindKeys) {
    this.executionPageService.updateSessionNoteKind(this.note.id, kind);
  }

  updateContent(newContent: string) {
    this.executionPageService
      .updateSessionNoteContent(this.note.id, newContent)
      .pipe(
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => this.richTextNote.endAsync()),
      )
      .subscribe();
  }

  canAttach(componentData: ExecutionPageComponentData): boolean {
    return (
      this.editable &&
      componentData.permissions.canAttach &&
      componentData.milestonesAllowModification &&
      !this.isSessionStopped
    );
  }

  addAttachment(files: File[]) {
    this.executionPageService
      .addAttachmentsToNote(files, this.note.id, this.note.attachmentList.id)
      .subscribe();
  }

  canAddIssue(componentData: ExecutionPageComponentData): boolean {
    return (
      componentData.projectData.bugTracker &&
      this.authenticatedToBugtracker &&
      componentData.permissions.canExecute &&
      componentData.milestonesAllowModification
    );
  }

  createIssue(componentData: ExecutionPageComponentData) {
    this.addIssue(componentData, false);
  }

  attachIssue(componentData: ExecutionPageComponentData) {
    this.addIssue(componentData, true);
  }

  addIssue(componentData: ExecutionPageComponentData, attachMode: boolean) {
    const dialogConf = getRemoteIssueDialogConfiguration(
      {
        bugTrackerId: componentData.projectData.bugTracker.id,
        squashProjectId: componentData.execution.projectId,
        boundEntityId: this.note.id,
        bindableEntity: 'SESSION_NOTE_TYPE',
        attachMode,
      },
      this.viewContainerRef,
    );

    const dialogRef = this.dialogService.openDialog<RemoteIssueDialogData, any>(dialogConf);

    dialogRef.dialogResultChanged$.subscribe(() => {
      this.refreshIssuesData.emit();
      this.executionPageService.refreshIssueCount();
    });
  }

  deleteNote(note: ExistingSessionNoteState): void {
    const dialogReference: DialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.campaign-workspace.execution-page.dialog.title.remove-note',
      messageKey: 'sqtm-core.campaign-workspace.execution-page.dialog.message.remove-note',
    });
    dialogReference.dialogClosed$.pipe(takeUntil(this.unsub$), take(1)).subscribe((confirm) => {
      if (confirm) {
        this.executionPageService.deleteNote(note);
      }
    });
  }

  getAttachments(note: ExistingSessionNoteState): Attachment[] {
    return Object.values(note.attachmentList.attachments.entities);
  }

  deleteAttachment(attachment: PersistedAttachment, noteId: number) {
    this.executionPageService.markAttachmentNoteToDelete(noteId, [attachment.id]);
  }

  confirmDeleteAttachmentEvent(
    attachment: PersistedAttachment,
    noteId: number,
    attachmentListId: number,
  ) {
    this.executionPageService
      .deleteNoteAttachments([attachment.id], noteId, attachmentListId)
      .subscribe();
  }

  cancelDeleteAttachmentEvent(attachment: PersistedAttachment, noteId: number) {
    this.executionPageService.cancelAttachmentNoteToDelete(noteId, [attachment.id]);
  }

  removeRejectedAttachment(attachment: RejectedAttachment, noteId: number) {
    this.executionPageService.removeNoteRejectedAttachments([attachment.id], noteId);
  }

  getUploadedAttachmentsCount(): number {
    return getPersistedAttachmentCount(this.note.attachmentList.attachments);
  }

  deleteIssue(issueIds: number[]) {
    this.showConfirmDeleteAssociatedIssueDialog()
      .pipe(
        takeUntil(this.unsub$),
        filter((confirmDelete) => confirmDelete),
        switchMap(() => {
          return this.executionPageService.removeIssues(issueIds);
        }),
      )
      .subscribe(() => this.refreshIssuesData.emit());
  }

  private showConfirmDeleteAssociatedIssueDialog(): Observable<boolean> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.campaign-workspace.execution-page.dialog.title.remove-issue.delete-one',
      messageKey:
        'sqtm-core.campaign-workspace.execution-page.dialog.message.remove-issue.delete-one-from-note',
      level: 'WARNING',
    });

    return dialogReference.dialogClosed$;
  }

  addNoteAfter(noteOrder: number) {
    this.executionPageService.addNoteForm(noteOrder + 1);
    this.addButtonClicked.emit();
  }

  handleReadOnlyContentClicked(): void {
    this.expandNote();
    setTimeout(() => this.richTextNote.enableEditMode());
  }

  protected readonly SprintStatus = SprintStatus;
}
