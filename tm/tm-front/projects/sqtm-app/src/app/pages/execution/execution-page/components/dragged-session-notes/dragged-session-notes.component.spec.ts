import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DraggedSessionNotesComponent } from './dragged-session-notes.component';
import { DRAG_AND_DROP_DATA } from 'sqtm-core';

describe('DraggedSessionNotesComponent', () => {
  let component: DraggedSessionNotesComponent;
  let fixture: ComponentFixture<DraggedSessionNotesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [{ provide: DRAG_AND_DROP_DATA, useValue: { data: { draggedNotes: [] } } }],
      declarations: [DraggedSessionNotesComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(DraggedSessionNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
