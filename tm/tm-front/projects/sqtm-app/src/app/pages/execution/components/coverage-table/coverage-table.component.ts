import { ChangeDetectionStrategy, Component, InjectionToken } from '@angular/core';
import {
  Fixed,
  GridColumnId,
  GridDefinition,
  GridId,
  GridService,
  indexColumn,
  levelEnumColumn,
  Limited,
  LocalPersistenceService,
  RequirementCriticality,
  smallGrid,
  Sort,
  StyleDefinitionBuilder,
  textColumn,
} from 'sqtm-core';
import { coverageExternalLinkColumn } from '../coverage-external-link/coverage-external-link.component';
import { coverageLiteralConverter } from '../../../test-case-workspace/test-case-view/components/coverage-table/coverage-table.component';

export const EXECUTION_COVERAGE_TABLE_CONF = new InjectionToken('EXECUTION_COVERAGE_TABLE_CONF');
export const EXECUTION_COVERAGE_TABLE = new InjectionToken('EXECUTION_COVERAGE_TABLE');

export function executionCoverageTableDefinition(
  localPersistenceService: LocalPersistenceService,
): GridDefinition {
  return smallGrid(GridId.EXECUTION_VIEW_COVERAGES)
    .withColumns([
      indexColumn().withViewport('leftViewport'),
      textColumn(GridColumnId.projectName)
        .changeWidthCalculationStrategy(new Limited(200))
        .withI18nKey('sqtm-core.entity.project.label.singular'),
      textColumn(GridColumnId.reference)
        .changeWidthCalculationStrategy(new Limited(200))
        .withI18nKey('sqtm-core.entity.generic.reference.label'),
      coverageExternalLinkColumn(GridColumnId.name)
        .changeWidthCalculationStrategy(new Limited(200))
        .withI18nKey('sqtm-core.entity.requirement.label.singular'),
      levelEnumColumn(GridColumnId.criticality, RequirementCriticality)
        .withI18nKey('sqtm-core.entity.generic.criticality.label')
        .changeWidthCalculationStrategy(new Fixed(78))
        .isEditable(false),
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .withRowConverter(coverageLiteralConverter)
    .withInitialSortedColumns([{ id: GridColumnId.criticality, sort: Sort.ASC }])
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
}

@Component({
  selector: 'sqtm-app-execution-coverage-table',
  template: ` <sqtm-core-grid></sqtm-core-grid> `,
  styleUrls: ['./coverage-table.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: EXECUTION_COVERAGE_TABLE,
    },
  ],
})
export class CoverageTableComponent {
  constructor(public grid: GridService) {}
}
