import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { ExecutionPageService } from '../../../services/execution-page.service';
import { map, switchMap, take, takeUntil } from 'rxjs/operators';
import {
  BindableEntity,
  createCustomFieldValueDataSelector,
  CustomFieldData,
  DialogService,
  ExportModelBuilderService,
  GridExportService,
  GridService,
  gridServiceFactory,
  LocalPersistenceService,
  ReferentialDataService,
  RestService,
  RichTextAttachmentDelegate,
  SprintStatus,
  TestCaseExecutionMode,
  TestAutomationServerKind,
} from 'sqtm-core';
import { select } from '@ngrx/store';
import { ExecutionPageComponentData } from '../../abstract-execution-page.component';
import {
  EXECUTION_ISSUE_TABLE,
  EXECUTION_ISSUE_TABLE_CONF,
  executionIssuesTableDefinition,
  ExecutionPageIssuesPanelComponent,
} from '../../../components/panels/execution-page-issues-panel/execution-page-issues-panel.component';
import {
  getRemoteIssueDialogConfiguration,
  RemoteIssueDialogData,
} from '../../../../../../components/remote-issue/containers/remote-issue-dialog/remote-issue-dialog.component';
import { toSignal } from '@angular/core/rxjs-interop';
import { ExecutionState } from '../../../../states/execution-state';

@Component({
  selector: 'sqtm-app-execution-page-content',
  templateUrl: './execution-page-content.component.html',
  styleUrls: ['./execution-page-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: EXECUTION_ISSUE_TABLE_CONF,
      useFactory: executionIssuesTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: EXECUTION_ISSUE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, EXECUTION_ISSUE_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: EXECUTION_ISSUE_TABLE,
    },
    {
      provide: RichTextAttachmentDelegate,
      useExisting: ExecutionPageService,
    },
    ExportModelBuilderService,
    GridExportService,
  ],
})
export class ExecutionPageContentComponent implements OnInit, OnDestroy {
  componentData$: Observable<ExecutionPageComponentData>;

  customFieldData$: Observable<CustomFieldData[]>;

  @ViewChild(ExecutionPageIssuesPanelComponent)
  private executionPageIssuesPanelComponent;

  private authenticatedInBugTracker$ = new BehaviorSubject<boolean>(false);

  canReportIssue$: Observable<boolean>;

  unsub$ = new Subject<void>();
  $isPremium = toSignal(this.referentialDataService.isPremiumPluginInstalled$);

  constructor(
    public executionPageService: ExecutionPageService,
    @Inject(EXECUTION_ISSUE_TABLE) public issuesGridService: GridService,
    public readonly dialogService: DialogService,
    public readonly viewContainerReference: ViewContainerRef,
    public readonly cdRef: ChangeDetectorRef,
    private referentialDataService: ReferentialDataService,
  ) {}

  ngOnInit(): void {
    this.componentData$ = this.executionPageService.componentData$.pipe(takeUntil(this.unsub$));

    this.customFieldData$ = this.componentData$.pipe(
      takeUntil(this.unsub$),
      select(createCustomFieldValueDataSelector(BindableEntity.EXECUTION)),
    );

    this.canReportIssue$ = combineLatest([
      this.authenticatedInBugTracker$,
      this.componentData$,
    ]).pipe(
      map(
        ([authenticated, componentData]) =>
          authenticated &&
          componentData.permissions.canExecute &&
          componentData.execution.parentSprintStatus !== SprintStatus.CLOSED.id &&
          componentData.execution.testCaseId != null, // Test case was deleted
      ),
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
    this.issuesGridService.complete();
  }

  removeIssue() {
    this.executionPageIssuesPanelComponent.removeIssueAssociations();
  }

  addIssue(attachMode: boolean): void {
    this.componentData$
      .pipe(
        take(1),
        switchMap((componentData: ExecutionPageComponentData) => {
          const dialogConf = getRemoteIssueDialogConfiguration(
            {
              bugTrackerId: componentData.projectData.bugTracker.id,
              squashProjectId: componentData.execution.projectId,
              boundEntityId: componentData.execution.id,
              bindableEntity: 'EXECUTION_TYPE',
              attachMode,
            },
            this.viewContainerReference,
          );
          return this.dialogService.openDialog<RemoteIssueDialogData, any>(dialogConf)
            .dialogResultChanged$;
        }),
      )
      .subscribe((result) => {
        if (result) {
          this.issuesGridService.refreshData();
          this.executionPageService.refreshIssueCount();
        }
      });
  }

  notifyIsAuthenticatedInBugTracker(): void {
    this.authenticatedInBugTracker$.next(true);
  }

  getCommentsPanelTitle(executionMode: string): string {
    return this.isExecutionExploratory(executionMode)
      ? 'sqtm-core.campaign-workspace.exploratory-execution.review'
      : 'sqtm-core.generic.label.comments';
  }

  getAnchorId(executionMode: string) {
    return this.isExecutionExploratory(executionMode) ? 'review' : 'comments';
  }

  isExecutionExploratory(executionMode: string): boolean {
    return executionMode === TestCaseExecutionMode.EXPLORATORY.id;
  }

  canEditIssues(componentData: ExecutionPageComponentData): boolean {
    return (
      componentData.permissions.canExecute &&
      componentData.milestonesAllowModification &&
      componentData.execution.parentSprintStatus !== 'CLOSED'
    );
  }

  isJenkinsExecution(testAutomationServerKind: TestAutomationServerKind): boolean {
    return testAutomationServerKind === TestAutomationServerKind.jenkins;
  }

  shouldDisplayFailureDetailPanel(executionState: ExecutionState) {
    return (
      this.$isPremium() &&
      !this.isJenkinsExecution(executionState.testAutomationServerKind) &&
      executionState.executionStatus == 'FAILURE' &&
      executionState.extenderId
    );
  }
}
