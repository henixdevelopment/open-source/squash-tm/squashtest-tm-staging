import { ChangeDetectionStrategy, Component, Input, ViewChild } from '@angular/core';
import { ExecutionPageComponentData } from '../../../containers/abstract-execution-page.component';
import {
  ActionErrorDisplayService,
  EditableCheckBoxComponent,
  EditableRichTextComponent,
  SprintStatus,
  TestCaseExecutionMode,
} from 'sqtm-core';
import { ExecutionPageService } from '../../../services/execution-page.service';
import { catchError, finalize } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-execution-page-comments-panel',
  templateUrl: './execution-page-comments-panel.component.html',
  styleUrls: ['./execution-page-comments-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExecutionPageCommentsPanelComponent {
  @Input()
  executionPageComponentData: ExecutionPageComponentData;

  @ViewChild('reviewCheckBox')
  checkBox: EditableCheckBoxComponent;

  @ViewChild('comment')
  comment: EditableRichTextComponent;

  constructor(
    public readonly executionPageService: ExecutionPageService,
    private readonly actionErrorDisplayService: ActionErrorDisplayService,
  ) {}

  isExploratory(executionMode: string): boolean {
    return executionMode === TestCaseExecutionMode.EXPLORATORY.id;
  }

  canEdit(executionPageComponentData: ExecutionPageComponentData) {
    return (
      executionPageComponentData.milestonesAllowModification &&
      executionPageComponentData.permissions.canExecute &&
      executionPageComponentData.execution.parentSprintStatus !== SprintStatus.CLOSED.id
    );
  }

  updateReviewStatus(isReviewed: boolean) {
    this.executionPageService
      .updateReviewStatus(isReviewed)
      .pipe(
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => (this.checkBox.model = !isReviewed)),
      )
      .subscribe();
  }

  updateComment(newComment: string) {
    this.executionPageService
      .updateComment(newComment)
      .pipe(
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => this.comment.cancel()),
      )
      .subscribe();
  }

  getPlaceholder(executionMode: string) {
    return executionMode === TestCaseExecutionMode.EXPLORATORY.id
      ? 'sqtm-core.campaign-workspace.exploratory-execution.add-review-comments'
      : 'sqtm-core.generic.editable.placeholder';
  }
}
