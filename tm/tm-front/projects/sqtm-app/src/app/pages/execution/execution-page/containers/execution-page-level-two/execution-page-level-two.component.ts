import { ChangeDetectionStrategy, Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location } from '@angular/common';
import {
  ActionErrorDisplayService,
  AttachmentService,
  ClosedSprintLockService,
  CustomFieldValueService,
  DataRow,
  DialogService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  ExecutionService,
  ExecutionStepService,
  ExploratorySessionService,
  Extendable,
  Fixed,
  GenericEntityViewService,
  getProjectPermissions,
  GridColumnId,
  GridDefinition,
  GridId,
  gridServiceFactory,
  indexColumn,
  InterWindowCommunicationService,
  IssuesService,
  levelEnumColumn,
  Limited,
  LocalPersistenceService,
  Permissions,
  ReferentialDataService,
  RequirementCriticality,
  RestService,
  SessionNoteService,
  smallGrid,
  Sort,
  StyleDefinitionBuilder,
  textColumn,
  UserProxyService,
  withLinkColumn,
} from 'sqtm-core';
import { ExecutionPageService } from '../../services/execution-page.service';
import { concatMap, filter, map, switchMap, take, takeUntil, tap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import {
  EXECUTION_COVERAGE_TABLE,
  EXECUTION_COVERAGE_TABLE_CONF,
} from '../../../components/coverage-table/coverage-table.component';
import { Observable, of, Subject } from 'rxjs';
import { coverageLiteralConverter } from '../../../../test-case-workspace/test-case-view/components/coverage-table/coverage-table.component';

function executionCoverageTableDefinition(
  localPersistenceService: LocalPersistenceService,
): GridDefinition {
  const urlFunctionToRequirement = (row: DataRow) => {
    return `/requirement-workspace/requirement-version/detail/${row.data[GridColumnId.requirementVersionId]}`;
  };

  return smallGrid(GridId.EXECUTION_VIEW_COVERAGES)
    .withColumns([
      indexColumn().withViewport('leftViewport'),
      textColumn(GridColumnId.projectName)
        .changeWidthCalculationStrategy(new Limited(300))
        .withI18nKey('sqtm-core.entity.project.label.singular'),
      textColumn(GridColumnId.reference)
        .changeWidthCalculationStrategy(new Limited(250))
        .withI18nKey('sqtm-core.entity.generic.reference.label'),
      withLinkColumn(GridColumnId.name, {
        kind: 'link',
        createUrlFunction: urlFunctionToRequirement,
        saveGridStateBeforeNavigate: true,
      })
        .changeWidthCalculationStrategy(new Extendable(200, 1))
        .withI18nKey('sqtm-core.entity.requirement.label.singular'),
      levelEnumColumn(GridColumnId.criticality, RequirementCriticality)
        .withI18nKey('sqtm-core.entity.generic.criticality.label')
        .changeWidthCalculationStrategy(new Fixed(78))
        .isEditable(false),
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .withRowConverter(coverageLiteralConverter)
    .withInitialSortedColumns([{ id: GridColumnId.criticality, sort: Sort.ASC }])
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
}

@Component({
  selector: 'sqtm-app-execution-page-level-two',
  templateUrl: './execution-page-level-two.component.html',
  styleUrls: ['./execution-page-level-two.component.less'],
  providers: [
    {
      provide: EXECUTION_COVERAGE_TABLE_CONF,
      useFactory: executionCoverageTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: EXECUTION_COVERAGE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, EXECUTION_COVERAGE_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: ExecutionPageService,
      useClass: ExecutionPageService,
      deps: [
        RestService,
        ReferentialDataService,
        AttachmentService,
        TranslateService,
        CustomFieldValueService,
        EntityViewAttachmentHelperService,
        EntityViewCustomFieldHelperService,
        ExecutionService,
        EXECUTION_COVERAGE_TABLE,
        InterWindowCommunicationService,
        ExecutionStepService,
        LocalPersistenceService,
        SessionNoteService,
        ExploratorySessionService,
        ClosedSprintLockService,
        ActionErrorDisplayService,
      ],
    },
    {
      provide: EntityViewService,
      useExisting: ExecutionPageService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: ExecutionPageService,
    },
    IssuesService,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExecutionPageLevelTwoComponent implements OnInit, OnDestroy {
  private unsub$ = new Subject<void>();

  constructor(
    private route: ActivatedRoute,
    private referentialDataService: ReferentialDataService,
    private executionPageService: ExecutionPageService,
    private interWindowCommunicationService: InterWindowCommunicationService,
    private location: Location,
    private dialogService: DialogService,
    private userProxyService: UserProxyService,
    private exploratorySessionService: ExploratorySessionService,
  ) {}

  ngOnInit(): void {
    this.referentialDataService
      .refresh()
      .pipe(
        switchMap(() => this.route.paramMap),
        take(1),
        map((params: ParamMap) => params.get('executionId')),
      )
      .subscribe((executionIdParam: string) => {
        const executionId = parseInt(executionIdParam, 10);
        this.executionPageService.load(executionId);
      });

    // [SQUASH-3880]
    // update view here in reaction from inter-window communication service
    // we need to do this because the id of execution is changed when re-creating the execution
    this.interWindowCommunicationService.interWindowMessages$
      .pipe(
        takeUntil(this.unsub$),
        filter((m) => m.isTypeOf('MODIFICATION-DURING-EXECUTION')),
        take(1),
      )
      .subscribe((message) => {
        const executionId = message.payload.executionId;
        if (executionId) {
          // we need to do like that because a standard navigation will not be triggered properly with a windows in background
          // it's the case when coming back after a modif during exec and thus we need that trick to :
          // 1- update data
          this.executionPageService.load(executionId);
          // 2- update the url in navigator WITHOUT triggering real Router navigation
          this.location.replaceState(`execution/${executionId}`);
        }
      });
  }

  ngOnDestroy(): void {
    this.executionPageService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  @HostListener('window:beforeunload')
  handleBeforeUnload(): boolean {
    const projectId = this.executionPageService.getSnapshot()?.execution?.projectId;
    if (projectId == null) {
      // If there's no project ID, this could mean we're on the error page.
      // We don't want to block the user here.
      return true;
    }

    const canExecute = this.canCurrentUserExecute();
    if (!canExecute) {
      return true;
    }
    return !this.isSessionRunning();
  }

  private canCurrentUserExecute(): boolean {
    const projectId = this.executionPageService.getSnapshot().execution.projectId;
    const currentProject =
      this.referentialDataService.getSnapshot().projectState.entities[projectId];
    const authenticatedUser = this.userProxyService.getAuthenticatedUser();
    const permissions = getProjectPermissions(authenticatedUser, currentProject);
    return permissions.CAMPAIGN_LIBRARY.includes(Permissions.EXECUTE) || authenticatedUser.admin;
  }

  @HostListener('window:unload')
  handleUnload(): void {
    const canExecute = this.canCurrentUserExecute();
    if (!canExecute) {
      return;
    }
    if (this.isSessionRunning()) {
      this.exploratorySessionService.pauseSessionWithKeepAlive(
        this.executionPageService.getSnapshot().execution.id,
      );
    }
  }

  // Called from canDeactivate guard
  userConfirmsNavigation(): Observable<boolean> {
    const projectId = this.executionPageService.getSnapshot()?.execution?.projectId;
    if (projectId == null) {
      // If there's no project ID, this could mean we're on the error page.
      // We don't want to block the user here.
      return of(true);
    }

    const canExecute = this.canCurrentUserExecute();
    if (!canExecute) {
      return of(true);
    }
    return this.executionPageService.componentData$.pipe(
      map(
        (componentData) => componentData.execution.exploratoryExecutionRunningState === 'RUNNING',
      ),
      concatMap((isRunning) => {
        if (!isRunning) {
          return of(true);
        }

        const dialogRef = this.dialogService.openConfirm({
          level: 'WARNING',
          id: 'confirm-pause-exploratory-execution',
          messageKey:
            'sqtm-core.campaign-workspace.exploratory-execution.dialog.confirm-pause-exploratory-execution.message',
          titleKey:
            'sqtm-core.campaign-workspace.exploratory-execution.dialog.confirm-pause-exploratory-execution.title',
        });

        return dialogRef.dialogClosed$;
      }),
      map((result) => Boolean(result)),
      tap((confirm) => {
        if (confirm && this.isSessionRunning()) {
          this.executionPageService
            .pauseExploratoryExecution(this.executionPageService.getSnapshot().execution.id)
            .subscribe();
        }
      }),
    );
  }

  private isSessionRunning(): boolean {
    return (
      this.executionPageService.getSnapshot().execution.exploratoryExecutionRunningState ===
      'RUNNING'
    );
  }
}
