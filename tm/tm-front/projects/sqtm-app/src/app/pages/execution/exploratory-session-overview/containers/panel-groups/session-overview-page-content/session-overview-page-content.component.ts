import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  computed,
  Inject,
  Injectable,
  OnDestroy,
  OnInit,
  Signal,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { combineLatest, concatMap, Observable, of, Subject, switchMap } from 'rxjs';
import {
  ActionErrorDisplayService,
  ASSIGNED_USER_DELEGATE,
  assigneeFilter,
  booleanFilter,
  buildFilters,
  DataRow,
  dateRangeFilter,
  DialogService,
  EditableRichTextComponent,
  ExploratoryExecutionRunningState,
  getTranslationKeyFromRunningState,
  GridColumnId,
  GridService,
  gridServiceFactory,
  ItemListSearchProvider,
  ListItem,
  LocalPersistenceService,
  ReferentialDataService,
  ResearchColumnPrototype,
  RestService,
  serverBackedGridMultiValueFilter,
  serverBackedGridTextFilter,
  SprintStatus,
  UserHistorySearchProvider,
} from 'sqtm-core';
import { SessionOverviewPageService } from '../../../services/session-overview-page.service';
import { SessionOverviewPageComponentData } from '../../session-overview-page/session-overview-page.component';
import { catchError, filter, finalize, map, take, takeUntil, tap } from 'rxjs/operators';
import { ExploratoryExecutionAssignableUsersProvider } from '../../../components/panels/session-overview-page-executions-panel/exploratory-execution-assignable-users-provider';
import { TranslateService } from '@ngx-translate/core';
import {
  exploratorySessionExecutionsTableDefinition,
  SESSION_EXECUTIONS_TABLE,
  SESSION_EXECUTIONS_TABLE_DEF,
} from '../../../components/panels/session-overview-page-executions-panel/session-overview-page-executions-panel.component';
import { buildAddMultipleExecutionsAndAssignUsersDialogDefinition } from '../../../components/dialog/session-overview-add-multiple-executions-dialog/session-overview-add-multiple-executions-dialog.component';
import { toSignal } from '@angular/core/rxjs-interop';

@Injectable()
class ExploratoryExecutionRunningStatusItemListSearchProvider extends ItemListSearchProvider {
  provideList(_listKey: string): Observable<ListItem[]> {
    return of(
      (['NEVER_STARTED', 'RUNNING', 'STOPPED'] as ExploratoryExecutionRunningState[]).map((id) => ({
        id,
        i18nLabelKey: getTranslationKeyFromRunningState(id),
      })),
    );
  }
}

@Component({
  selector: 'sqtm-app-session-overview-page-content',
  templateUrl: './session-overview-page-content.component.html',
  styleUrls: ['./session-overview-page-content.component.less'],
  providers: [
    {
      provide: SESSION_EXECUTIONS_TABLE_DEF,
      useFactory: exploratorySessionExecutionsTableDefinition,
      deps: [TranslateService, LocalPersistenceService],
    },
    {
      provide: SESSION_EXECUTIONS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, SESSION_EXECUTIONS_TABLE_DEF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: SESSION_EXECUTIONS_TABLE,
    },
    {
      provide: ASSIGNED_USER_DELEGATE,
      useExisting: SessionOverviewPageService,
    },
    {
      provide: UserHistorySearchProvider,
      useClass: ExploratoryExecutionAssignableUsersProvider,
    },
    {
      provide: ItemListSearchProvider,
      useClass: ExploratoryExecutionRunningStatusItemListSearchProvider,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SessionOverviewPageContentComponent implements OnInit, OnDestroy {
  $componentData: Signal<SessionOverviewPageComponentData>;
  unsub$ = new Subject<void>();

  $canEdit: Signal<boolean> = computed(() => {
    return (
      this.$componentData().permissions.canExecute &&
      this.$componentData().milestonesAllowModification &&
      this.$componentData().exploratorySessionOverview.sprintStatus !== SprintStatus.CLOSED.id
    );
  });

  $canDeleteExecution: Signal<boolean> = computed(() => {
    return (
      this.$componentData().permissions.canDeleteExecution &&
      this.$componentData().milestonesAllowModification &&
      this.$componentData().exploratorySessionOverview.sprintStatus !== SprintStatus.CLOSED.id
    );
  });

  @ViewChild('comment')
  comment: EditableRichTextComponent;

  constructor(
    @Inject(SESSION_EXECUTIONS_TABLE) public readonly executionTableService: GridService,
    private readonly sessionOverviewPageService: SessionOverviewPageService,
    private readonly cdRef: ChangeDetectorRef,
    private readonly dialogService: DialogService,
    private readonly viewContainerRef: ViewContainerRef,
    private readonly actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    this.$componentData = toSignal(sessionOverviewPageService.componentData$);

    const sessionOverviewId = this.$componentData().exploratorySessionOverview.id;
    executionTableService.setServerUrl([
      'session-overview',
      sessionOverviewId.toString(),
      'executions',
    ]);
  }

  ngOnInit() {
    this.initializeGridFilters();
  }

  ngOnDestroy(): void {
    this.executionTableService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  addExecution($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();

    this.sessionOverviewPageService
      .addExecution()
      .pipe(tap(() => this.executionTableService.refreshData()))
      .subscribe(() => this.cdRef.detectChanges());
  }

  private initializeGridFilters() {
    this.executionTableService.addFilters(this.buildGridFilters());
  }

  private buildGridFilters() {
    return buildFilters([
      serverBackedGridTextFilter(GridColumnId.taskDivision),
      assigneeFilter(
        GridColumnId.assigneeLogin,
        ResearchColumnPrototype.ITEM_TEST_PLAN_TESTER,
      ).alwaysActive(),
      dateRangeFilter(
        GridColumnId.lastExecutedOn,
        ResearchColumnPrototype.EXECUTION_LAST_EXECUTED_ON,
      ),
      booleanFilter(GridColumnId.reviewed).alwaysActive(),
      serverBackedGridMultiValueFilter(GridColumnId.runningState),
    ]);
  }

  deleteSelectedExecutions($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();

    let allExecutionsReviewStatusesExceptCurrentRows: boolean[];

    combineLatest([
      this.executionTableService.selectedRowIds$,
      this.executionTableService.dataRows$,
    ])
      .pipe(
        takeUntil(this.unsub$),
        map(([selectedRowIds, dataRows]) => {
          return Object.values(dataRows)
            .filter((row) => !selectedRowIds.includes(row.id))
            .map((row) => row.data[GridColumnId.reviewed]);
        }),
      )
      .subscribe((statuses) => (allExecutionsReviewStatusesExceptCurrentRows = statuses));

    this.executionTableService.selectedRows$
      .pipe(
        take(1),
        filter((rows: DataRow[]) => this.canDelete(rows)),
        concatMap((rows: DataRow[]) => this.openDeleteExecutionDialog(rows)),
        filter(({ confirmDelete }) => confirmDelete),
        tap(() => this.executionTableService.beginAsyncOperation()),
        map(({ rows }) => [
          rows.map((row) => row.data[GridColumnId.executionId]),
          rows.map((row) => row.data[GridColumnId.assigneeId]),
        ]),
        concatMap(([executionIds, assigneeIds]) => {
          return this.sessionOverviewPageService.deleteExecutions(
            executionIds,
            assigneeIds,
            allExecutionsReviewStatusesExceptCurrentRows,
          );
        }),
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => this.executionTableService.completeAsyncOperation()),
      )
      .subscribe(() => {
        this.executionTableService.completeAsyncOperation();
        this.executionTableService.refreshData();
      });
  }

  private canDelete(rows: DataRow[]) {
    return (
      rows.length > 0 &&
      !rows.some((row) => row.data.boundToBlockingMilestone) &&
      !rows.some((row) => !row.simplePermissions.canDelete)
    );
  }

  private openDeleteExecutionDialog(
    rows: DataRow[],
  ): Observable<{ confirmDelete: boolean; rows: DataRow[] }> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.campaign-workspace.dialog.title.mass-remove-execution',
      messageKey: 'sqtm-core.campaign-workspace.dialog.message.mass-remove-execution',
      level: 'DANGER',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map((confirmDelete) => ({ confirmDelete, rows })),
    );
  }

  updateComments(comments: string) {
    this.sessionOverviewPageService
      .updateComments(comments)
      .pipe(
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => {
          this.comment.pending = false;
          this.comment.cancel();
        }),
      )
      .subscribe();
  }

  openAddMultipleExecutionsDialog($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();

    this.sessionOverviewPageService
      .fetchUnassignedUsers()
      .pipe(
        switchMap(
          (unassignedUsers) =>
            this.dialogService.openDialog(
              buildAddMultipleExecutionsAndAssignUsersDialogDefinition(
                unassignedUsers,
                this.viewContainerRef,
              ),
            ).dialogClosed$,
        ),
        takeUntil(this.unsub$),
      )
      .subscribe(() => this.executionTableService.refreshData());
  }
}
