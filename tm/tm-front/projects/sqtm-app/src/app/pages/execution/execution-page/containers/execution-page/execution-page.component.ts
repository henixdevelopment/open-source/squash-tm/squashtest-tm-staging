import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import {
  ActionErrorDisplayService,
  CapsuleInformationData,
  DialogService,
  ExecutionStatus,
  InterWindowCommunicationService,
  RestService,
  TestCaseExecutionMode,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { ExecutionPageService } from '../../services/execution-page.service';
import { APP_BASE_HREF, DatePipe } from '@angular/common';
import {
  AbstractExecutionPageComponent,
  ExecutionPageComponentData,
} from '../abstract-execution-page.component';
import { ExecutionRunnerOpenerService } from '../../../execution-runner/services/execution-runner-opener.service';
import { isExistingSessionNoteState } from '../../../states/execution-state';

@Component({
  selector: 'sqtm-app-execution-page',
  templateUrl: './execution-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExecutionPageComponent
  extends AbstractExecutionPageComponent
  implements OnInit, OnDestroy
{
  constructor(
    executionPageService: ExecutionPageService,
    translateService: TranslateService,
    datePipe: DatePipe,
    interWindowCommunicationService: InterWindowCommunicationService,
    @Inject(APP_BASE_HREF) baseUrl: string,
    cdRef: ChangeDetectorRef,
    dialogService: DialogService,
    restService: RestService,
    executionRunnerOpenerService: ExecutionRunnerOpenerService,
    actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    super(
      executionPageService,
      translateService,
      datePipe,
      interWindowCommunicationService,
      baseUrl,
      cdRef,
      dialogService,
      restService,
      executionRunnerOpenerService,
      actionErrorDisplayService,
    );
  }

  ngOnInit() {
    super.ngOnInit();
  }

  getExecutionStatus(statusKey: string): CapsuleInformationData {
    const executionStatus = ExecutionStatus[statusKey];
    return {
      id: executionStatus.id,
      color: executionStatus.color,
      icon: executionStatus.icon,
      labelI18nKey: executionStatus.i18nKey,
      titleI18nKey: 'sqtm-core.entity.execution.status.long-label',
    };
  }

  getSessionNotesCount(componentData: ExecutionPageComponentData): number {
    if (componentData.execution.sessionNotes == null) {
      return 0;
    } else {
      return Object.values(componentData.execution.sessionNotes.entities).filter((sessionNote) =>
        isExistingSessionNoteState(sessionNote),
      ).length;
    }
  }

  getTooltipForCommentsAnchor(executionMode: string): string {
    return this.isExecutionExploratory(executionMode)
      ? this.translateService.instant('sqtm-core.campaign-workspace.exploratory-execution.review')
      : this.translateService.instant('sqtm-core.generic.label.comments');
  }

  getIconForCommentsAnchor(executionMode: string): string {
    return this.isExecutionExploratory(executionMode)
      ? 'sqtm-core-campaign:review'
      : 'sqtm-core-campaign:comments';
  }

  getId(executionMode: string): string {
    return this.isExecutionExploratory(executionMode) ? 'review' : 'comments';
  }

  isExecutionExploratory(executionMode: string): boolean {
    return executionMode === TestCaseExecutionMode.EXPLORATORY.id;
  }
}
