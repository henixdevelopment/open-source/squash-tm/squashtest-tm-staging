import { EntityViewState, provideInitialViewState } from 'sqtm-core';
import { ExecutionState } from '../../states/execution-state';

export interface ExecutionPageState extends EntityViewState<ExecutionState, 'execution'> {
  execution: ExecutionState;
}

export function provideInitialPageState(): Readonly<ExecutionPageState> {
  return {
    ...provideInitialViewState<ExecutionState, 'execution'>('execution'),
  };
}
