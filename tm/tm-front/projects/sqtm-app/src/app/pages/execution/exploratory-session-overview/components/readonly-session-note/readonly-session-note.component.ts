import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ExistingSessionNoteState } from '../../../states/execution-state';
import { Attachment, DataRowModel, getPersistedAttachmentCount, SessionNoteKind } from 'sqtm-core';

@Component({
  selector: 'sqtm-app-readonly-session-note',
  templateUrl: './readonly-session-note.component.html',
  styleUrls: ['./readonly-session-note.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReadonlySessionNoteComponent {
  @Input()
  note: ExistingSessionNoteState;

  @Input()
  issues: DataRowModel[] = [];

  get lastModificationDate(): string {
    if (this.note == null) {
      return null;
    }

    return (this.note.lastModifiedOn ?? this.note.createdOn).toString();
  }

  get lastModificationUser(): string {
    if (this.note == null) {
      return null;
    }

    return this.note.lastModifiedBy ?? this.note.createdBy;
  }

  getUploadedAttachmentsCount(): number {
    return getPersistedAttachmentCount(this.note.attachmentList.attachments);
  }

  getAttachments(note: ExistingSessionNoteState): Attachment[] {
    return Object.values(note.attachmentList.attachments.entities);
  }

  getKindBackgroundColor() {
    return SessionNoteKind[this.note.noteKind].backgroundColor;
  }

  getKindBorderColor() {
    return SessionNoteKind[this.note.noteKind].backgroundColor;
  }

  getKindTextColor() {
    return SessionNoteKind[this.note.noteKind].textColor;
  }

  getKindI18nKey() {
    return SessionNoteKind[this.note.noteKind].i18nKey;
  }

  collapseNote(): void {
    this.note.expanded = false;
  }

  expandNote(): void {
    this.note.expanded = true;
  }
}
