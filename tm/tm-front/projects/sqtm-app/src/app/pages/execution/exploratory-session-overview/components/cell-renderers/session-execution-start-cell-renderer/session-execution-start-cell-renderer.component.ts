import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  ViewContainerRef,
} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  DialogService,
  Fixed,
  GridColumnId,
  GridService,
  Permissions,
  ReferentialDataService,
  ReferentialDataState,
} from 'sqtm-core';
import { Router } from '@angular/router';
import { SessionOverviewPageService } from '../../../services/session-overview-page.service';
import { map, take, takeUntil, tap } from 'rxjs/operators';
import { buildAccessRunningExecutionDialogDefinition } from '../../dialog/session-overview-access-running-execution-dialog/session-overview-access-running-execution-dialog.component';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'sqtm-app-session-execution-start-cell-renderer',
  templateUrl: './session-execution-start-cell-renderer.component.html',
  styleUrls: ['./session-execution-start-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SessionExecutionStartCellRendererComponent
  extends AbstractCellRendererComponent
  implements OnDestroy
{
  unsub$ = new Subject<void>();
  constructor(
    grid: GridService,
    public readonly cdRef: ChangeDetectorRef,
    public readonly router: Router,
    public readonly sessionOverviewPageService: SessionOverviewPageService,
    public readonly dialogService: DialogService,
    public readonly vcr: ViewContainerRef,
    private referentialDataService: ReferentialDataService,
  ) {
    super(grid, cdRef);
  }

  navigateToExecution($event: MouseEvent) {
    this.sessionOverviewPageService
      .isExploratoryExecutionRunning(this.row.data[this.columnDisplay.id])
      .pipe(
        take(1),
        tap((isExecutionRunning) => {
          if (isExecutionRunning) {
            this.openAccessRunningExecutionDialog($event);
          } else {
            this.doNavigateToExecution();
          }
        }),
      )
      .subscribe();
  }

  canRead(): Observable<boolean> {
    return this.referentialDataService.referentialData$.pipe(
      takeUntil(this.unsub$),
      map((referentialData: ReferentialDataState) => {
        return this.checkIfCurrentUserCanRead(referentialData);
      }),
    );
  }

  private checkIfCurrentUserCanRead(referentialData: ReferentialDataState): boolean {
    const { admin, userId }: { admin: boolean; userId: number } = referentialData.userState;
    const { projectId, assigneeId }: { projectId: number; assigneeId: number | null } =
      this.row.data;

    if (admin) {
      return true;
    }

    const permissions =
      referentialData.projectState.entities[projectId].permissions.CAMPAIGN_LIBRARY;
    const canReadUnassigned: boolean = permissions.includes(Permissions.READ_UNASSIGNED);
    const isAssigned: boolean = assigneeId === userId;
    return canReadUnassigned || isAssigned;
  }

  private doNavigateToExecution() {
    this.router.navigate(['execution', this.row.data[this.columnDisplay.id], 'charter']);
  }

  private openAccessRunningExecutionDialog($event: MouseEvent) {
    $event.preventDefault();
    $event.stopPropagation();

    const targetedExecutionId = Number(this.row.data[this.columnDisplay.id]);
    const overviewId = this.sessionOverviewPageService.getSnapshot().exploratorySessionOverview.id;

    return this.dialogService.openDialog(
      buildAccessRunningExecutionDialogDefinition(
        this.vcr,
        targetedExecutionId,
        overviewId,
        'notes',
      ),
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
  }
}

export function sessionExecutionStart(): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(GridColumnId.executionId)
    .withRenderer(SessionExecutionStartCellRendererComponent)
    .withViewport('rightViewport')
    .withLabel('')
    .disableSort()
    .changeWidthCalculationStrategy(new Fixed(40));
}
