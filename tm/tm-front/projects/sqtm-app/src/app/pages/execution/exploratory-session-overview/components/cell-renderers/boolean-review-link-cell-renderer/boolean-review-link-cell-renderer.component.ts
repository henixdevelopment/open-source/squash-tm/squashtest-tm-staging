import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewContainerRef,
} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  DialogService,
  GridService,
  StaticOrDynamicColumnId,
} from 'sqtm-core';
import { Router } from '@angular/router';
import { take, tap } from 'rxjs/operators';
import { buildAccessRunningExecutionDialogDefinition } from '../../dialog/session-overview-access-running-execution-dialog/session-overview-access-running-execution-dialog.component';
import { SessionOverviewPageService } from '../../../services/session-overview-page.service';

@Component({
  selector: 'sqtm-core-boolean-review-link-cell-renderer',
  template: ` @if (columnDisplay && row) {
    <div class="full-width full-height flex-column">
      <a class="m-auto-0 txt-ellipsis" (click)="navigateToExecution($event)">
        {{ getStatusReview() | translate }}
      </a>
    </div>
  }`,
  styleUrls: ['./boolean-review-link-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BooleanReviewLinkCellRendererComponent extends AbstractCellRendererComponent {
  constructor(
    public readonly grid: GridService,
    public readonly cdRef: ChangeDetectorRef,
    public readonly router: Router,
    public readonly sessionOverviewPageService: SessionOverviewPageService,
    public readonly dialogService: DialogService,
    public readonly vcr: ViewContainerRef,
  ) {
    super(grid, cdRef);
  }

  getStatusReview() {
    return this.row.data[this.columnDisplay.id] === false
      ? 'sqtm-core.generic.label.no'
      : 'sqtm-core.generic.label.yes';
  }

  navigateToExecution($event: MouseEvent) {
    this.sessionOverviewPageService
      .isExploratoryExecutionRunning(this.row.data.executionId)
      .pipe(
        take(1),
        tap((isExecutionRunning) => {
          if (isExecutionRunning) {
            this.openAccessRunningExecutionDialog($event);
          } else {
            this.doNavigateToExecutionReview();
          }
        }),
      )
      .subscribe();
  }

  private doNavigateToExecutionReview() {
    this.router.navigate(['execution', this.row.data.executionId, 'content'], {
      queryParams: { anchor: 'review' },
    });
  }

  private openAccessRunningExecutionDialog($event: MouseEvent) {
    $event.preventDefault();
    $event.stopPropagation();

    const targetedExecutionId = Number(this.row.data.executionId);
    const overviewId = this.sessionOverviewPageService.getSnapshot().exploratorySessionOverview.id;

    return this.dialogService.openDialog(
      buildAccessRunningExecutionDialogDefinition(
        this.vcr,
        targetedExecutionId,
        overviewId,
        'review',
      ),
    );
  }
}

export function booleanReviewLinkColumn(id: StaticOrDynamicColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(BooleanReviewLinkCellRendererComponent);
}
