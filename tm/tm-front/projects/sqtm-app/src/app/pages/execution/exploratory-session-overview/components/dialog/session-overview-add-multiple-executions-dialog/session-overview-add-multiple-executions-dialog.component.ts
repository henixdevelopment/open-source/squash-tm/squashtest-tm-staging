import { ChangeDetectionStrategy, Component, OnDestroy, ViewContainerRef } from '@angular/core';
import { Subject } from 'rxjs';
import {
  DialogConfiguration,
  DialogReference,
  Execution,
  NULL_VALUE_IN_FILTER,
  SimpleUser,
  UserListElement,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { SessionOverviewPageService } from '../../../services/session-overview-page.service';

export interface AddMultipleExecutionsDialogConfiguration {
  id: string;
  titleKey: string;
  assignableUsers: SimpleUser[];
}

export function buildAddMultipleExecutionsAndAssignUsersDialogDefinition(
  assignableUsers: SimpleUser[],
  viewContainerRef: ViewContainerRef,
): DialogConfiguration<AddMultipleExecutionsDialogConfiguration> {
  const configuration: AddMultipleExecutionsDialogConfiguration = {
    id: SessionOverviewAddMultipleExecutionsDialogComponent.DIALOG_ID,
    titleKey:
      'sqtm-core.campaign-workspace.exploratory-session-overview.add-multiple-executions.title',
    assignableUsers,
  };
  return {
    id: 'add-multiple-executions-assign-users',
    component: SessionOverviewAddMultipleExecutionsDialogComponent,
    height: 500,
    width: 800,
    data: configuration,
    viewContainerReference: viewContainerRef,
  };
}

@Component({
  selector: 'sqtm-app-session-overview-add-multiple-executions-dialog',
  templateUrl: './session-overview-add-multiple-executions-dialog.component.html',
  styleUrls: ['./session-overview-add-multiple-executions-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SessionOverviewAddMultipleExecutionsDialogComponent implements OnDestroy {
  static DIALOG_ID = 'add-multiple-executions-assign-users-dialog';

  private unsub$ = new Subject<void>();

  configuration: AddMultipleExecutionsDialogConfiguration;

  assignableUsers: UserListElement[] = [];

  constructor(
    private dialogReference: DialogReference<AddMultipleExecutionsDialogConfiguration, Execution[]>,
    private translateService: TranslateService,
    private sessionOverviewPageService: SessionOverviewPageService,
  ) {
    this.configuration = this.dialogReference.data;
    this.assignableUsers = this.configuration.assignableUsers.map((user) => ({
      ...user,
      selected: false,
    }));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  confirm() {
    this.sessionOverviewPageService.addMultipleExecutions(
      this.getAssignableUsersAsList()
        .filter((user) => user.selected)
        .map((user) => user.id),
    );
    this.dialogReference.close();
  }

  getAssignableUsersAsList(): UserListElement[] {
    return Array.from(this.assignableUsers.values()).map((user) => {
      return {
        id: user.id,
        selected: user.selected,
        login: user.login,
        firstName: user.firstName,
        lastName: user.lastName,
        label: this.getUserLabel(user),
      };
    });
  }

  changeUsersSelection($event: UserListElement) {
    this.assignableUsers = this.getAssignableUsersAsList().map((user) => {
      if (user.id === $event.id) {
        return {
          ...user,
          selected: $event.selected,
        };
      } else {
        return user;
      }
    });
  }

  private getUserLabel(user: UserListElement) {
    if (user.id) {
      return `${user.firstName} ${user.lastName} (${user.login})`;
    } else if (user.login === NULL_VALUE_IN_FILTER) {
      return this.translateService.instant('sqtm-core.entity.iteration.itpi.unaffected');
    } else {
      return user.login;
    }
  }

  hasSelectedItems() {
    return this.getAssignableUsersAsList().filter((user) => user.selected).length > 0;
  }

  getInfoText(assignableUsers: UserListElement[]) {
    return assignableUsers.length > 0
      ? 'sqtm-core.campaign-workspace.exploratory-session-overview.add-multiple-executions.detailed-text'
      : 'sqtm-core.campaign-workspace.exploratory-session-overview.add-multiple-executions.no-assignable-users';
  }
}
