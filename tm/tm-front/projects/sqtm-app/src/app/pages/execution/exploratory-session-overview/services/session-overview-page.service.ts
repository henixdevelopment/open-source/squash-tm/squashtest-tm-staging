import { Injectable } from '@angular/core';
import { catchError, concatMap, map, take, tap, withLatestFrom } from 'rxjs/operators';
import {
  ActionErrorDisplayService,
  AssignedUserDelegate,
  AttachmentService,
  CampaignPermissions,
  CustomFieldValueService,
  DateFormatUtils,
  DialogService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  ExecutionStatus,
  ExploratorySessionOverviewModel,
  GenericEntityViewState,
  GridColumnId,
  ProjectData,
  ReferentialDataService,
  RestService,
  ReviewStatus,
  SessionOverviewStatus,
  SimpleUser,
  SprintStatus,
} from 'sqtm-core';
import { Observable } from 'rxjs';
import {
  provideInitialSessionOverviewPageState,
  SessionOverviewPageState,
  SessionOverviewPageViewState,
} from '../states/session-overview-page-state';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class SessionOverviewPageService
  extends EntityViewService<
    SessionOverviewPageState,
    'exploratorySessionOverview',
    CampaignPermissions
  >
  implements AssignedUserDelegate
{
  canAssign$: Observable<boolean>;
  canEdit$: Observable<boolean>;
  assignableUsers$: Observable<SimpleUser[]>;
  assignableEntityIdColumnId: GridColumnId = GridColumnId.executionId;

  constructor(
    protected restService: RestService,
    protected referentialDataService: ReferentialDataService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected customFieldValueService: CustomFieldValueService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
    protected customFieldHelper: EntityViewCustomFieldHelperService,
    protected actionErrorDisplayService: ActionErrorDisplayService,
    protected dialogService: DialogService,
  ) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper,
    );
    this.canAssign$ = this.componentData$.pipe(
      map(
        (componentData) =>
          componentData.permissions.canWrite &&
          componentData.milestonesAllowModification &&
          componentData.exploratorySessionOverview.sprintStatus !== SprintStatus.CLOSED.id,
      ),
    );
    this.assignableUsers$ = this.componentData$.pipe(
      map((componentData) => componentData.exploratorySessionOverview.assignableUsers),
    );
  }

  protected getRootUrl(_initialState): string {
    return `session-overview`;
  }

  addSimplePermissions(projectData: ProjectData): CampaignPermissions {
    return new CampaignPermissions(projectData);
  }

  getInitialState(): GenericEntityViewState<
    SessionOverviewPageState,
    'exploratorySessionOverview'
  > {
    return provideInitialSessionOverviewPageState();
  }

  load(sessionId: number) {
    this.fetchSessionOverviewData(sessionId)
      .pipe(
        withLatestFrom(this.state$),
        map(([sessionOverviewModel, state]) =>
          this.loadSessionOverviewModel(sessionOverviewModel, state),
        ),
      )
      .subscribe({
        next: (state) => this.commit(state),
        error: (err) => this.notifyEntityNotFound(err),
      });
  }

  private fetchSessionOverviewData(sessionId: number): Observable<ExploratorySessionOverviewModel> {
    return this.restService.getWithoutErrorHandling<ExploratorySessionOverviewModel>([
      'session-overview',
      sessionId.toString(),
    ]);
  }

  private loadSessionOverviewModel(
    model: ExploratorySessionOverviewModel,
    state: SessionOverviewPageViewState,
  ): SessionOverviewPageViewState {
    const attachmentState = this.attachmentHelper.initializeAttachmentState(
      model.attachmentList.attachments,
    );
    const attachmentList = { id: model.attachmentList.id, attachments: attachmentState };

    return this.updateEntity(
      {
        ...model,
        attachmentList,
        customFieldValues: null,
        inferredSessionReviewStatus: model.inferredSessionReviewStatus,
        createdOn: DateFormatUtils.createDateFromIsoString(model.createdOn),
        lastModifiedOn: DateFormatUtils.createDateFromIsoString(model.lastModifiedOn),
        dueDate: DateFormatUtils.createDateFromIsoString(model.dueDate),
      },
      state,
    );
  }

  addExecution(): Observable<SessionOverviewPageViewState> {
    return this.restService
      .post([
        'session-overview',
        this.getSnapshot().exploratorySessionOverview.id.toString(),
        'new-execution',
      ])
      .pipe(
        withLatestFrom(this.state$),
        map(([, state]) => {
          return {
            ...state,
            exploratorySessionOverview: {
              ...state.exploratorySessionOverview,
              inferredSessionReviewStatus: this.getReviewStatusAfterAddingAnExecution(state),
              nbExecutions: state.exploratorySessionOverview.nbExecutions + 1,
            },
          } satisfies SessionOverviewPageViewState;
        }),
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        map((state) => this.updateSessionStatusAndExecutionStatusToRunning(state)),
        concatMap((state) => this.updateLastModificationIfNecessary(state)),
        tap((nextState) => this.store.commit(nextState)),
      );
  }

  updateAssignedUser(executionId: number, userId: number) {
    return this.restService.post(
      ['exploratory-execution', executionId.toString(), 'assign-user-to-exploratory-execution'],
      { userId: userId },
    );
  }

  updateTaskDivision(executionId: number, newTaskDivision: string) {
    return this.restService.post(
      ['exploratory-execution', executionId.toString(), 'update-task-division'],
      { taskDivision: newTaskDivision },
    );
  }

  startOrResumeSessionOverview(): Observable<SessionOverviewPageViewState> {
    return this.restService
      .post([
        'session-overview',
        this.getSnapshot().exploratorySessionOverview.id.toString(),
        'start-session',
      ])
      .pipe(
        withLatestFrom(this.state$),
        map(([, state]) => {
          return {
            ...state,
            exploratorySessionOverview: {
              ...state.exploratorySessionOverview,
              sessionStatus: 'RUNNING',
              executionStatus: 'RUNNING',
            },
          } satisfies SessionOverviewPageViewState;
        }),
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        map((state) => this.updateSessionStatusAndExecutionStatusToRunning(state)),
        concatMap((state) => this.updateLastModificationIfNecessary(state)),
        tap((nextState) => this.store.commit(nextState)),
      );
  }

  endSessionOverview() {
    return this.restService
      .post([
        'session-overview',
        this.getSnapshot().exploratorySessionOverview.id.toString(),
        'end-session',
      ])
      .pipe(
        withLatestFrom(this.state$),
        map(([, state]) => {
          return {
            ...state,
            exploratorySessionOverview: {
              ...state.exploratorySessionOverview,
              sessionStatus: 'FINISHED',
            },
          } satisfies SessionOverviewPageViewState;
        }),
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        map((state) => this.updateSessionStatusToFinished(state)),
        concatMap((state) => this.updateLastModificationIfNecessary(state)),
        take(1),
        tap((nextState) => this.store.commit(nextState)),
        tap(() =>
          this.dialogService.openAlert({
            id: 'end-session-dialog',
            titleKey:
              'sqtm-core.campaign-workspace.exploratory-session-overview.dialog.end-session.title',
            level: 'INFO',
            messageKey:
              'sqtm-core.campaign-workspace.exploratory-session-overview.dialog.end-session.message',
          }),
        ),
      );
  }

  private updateSessionStatusAndExecutionStatusToRunning(
    sessionOverviewPageViewState: SessionOverviewPageViewState,
  ) {
    return {
      ...sessionOverviewPageViewState,
      sessionStatus: SessionOverviewStatus.RUNNING.id,
      executionStatus: ExecutionStatus.RUNNING.id,
    };
  }

  private updateSessionStatusToFinished(
    sessionOverviewPageViewState: SessionOverviewPageViewState,
  ) {
    return {
      ...sessionOverviewPageViewState,
      sessionStatus: SessionOverviewStatus.FINISHED.id,
    };
  }

  deleteExecutions(
    executionIds: number[],
    assigneeIds: number[],
    allExecutionsReviewStatusesExceptCurrentRows?: boolean[],
  ): any {
    return this.restService
      .delete([
        'session-overview',
        this.getSnapshot().exploratorySessionOverview.id.toString(),
        'executions',
        executionIds.join(),
      ])
      .pipe(
        withLatestFrom(this.state$),
        map(([, state]) => {
          return {
            ...state,
            exploratorySessionOverview: {
              ...state.exploratorySessionOverview,
              inferredSessionReviewStatus: this.getInferredSessionReviewStatusAfterDeletion(
                allExecutionsReviewStatusesExceptCurrentRows,
              ),
              nbExecutions: state.exploratorySessionOverview.nbExecutions - executionIds.length,
            },
          } satisfies SessionOverviewPageViewState;
        }),
        concatMap((state) => this.updateLastModificationIfNecessary(state)),
        tap((nextState) => this.store.commit(nextState)),
      );
  }

  addMultipleExecutions(selectedUsersIds: number[]) {
    const snapshot = this.getSnapshot();
    return this.restService
      .post([
        'session-overview',
        snapshot.exploratorySessionOverview.id.toString(),
        'add-executions-with-users',
        selectedUsersIds.join(),
      ])
      .pipe(
        withLatestFrom(this.state$),
        map(([, state]) => {
          return {
            ...state,
            exploratorySessionOverview: {
              ...state.exploratorySessionOverview,
              inferredSessionReviewStatus: this.getReviewStatusAfterAddingAnExecution(state),
              nbExecutions: state.exploratorySessionOverview.nbExecutions + selectedUsersIds.length,
            },
          } satisfies SessionOverviewPageViewState;
        }),
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        concatMap((state) => this.updateLastModificationIfNecessary(state)),
        tap((nextState) => this.store.commit(nextState)),
      )
      .subscribe();
  }

  private getInferredSessionReviewStatusAfterDeletion(
    allExecutionsReviewStatusesExceptCurrentRows: boolean[],
  ) {
    if (allExecutionsReviewStatusesExceptCurrentRows.every((status) => status === true)) {
      return ReviewStatus.DONE.id;
    } else if (allExecutionsReviewStatusesExceptCurrentRows.some((status) => status === true)) {
      return ReviewStatus.RUNNING.id;
    } else {
      return ReviewStatus.TO_DO.id;
    }
  }

  private getReviewStatusAfterAddingAnExecution(state: SessionOverviewPageViewState): string {
    const hasNoExecutionBeforeAdd = state.exploratorySessionOverview.nbExecutions === 0;

    const reviewIsToDo =
      state.exploratorySessionOverview.inferredSessionReviewStatus === ReviewStatus.TO_DO.id;
    const reviewIsRunning =
      state.exploratorySessionOverview.inferredSessionReviewStatus === ReviewStatus.RUNNING.id;
    const reviewIsDone =
      state.exploratorySessionOverview.inferredSessionReviewStatus === ReviewStatus.DONE.id;

    if (hasNoExecutionBeforeAdd || (!hasNoExecutionBeforeAdd && reviewIsToDo)) {
      return ReviewStatus.TO_DO.id;
    } else if (!hasNoExecutionBeforeAdd && (reviewIsDone || reviewIsRunning)) {
      return ReviewStatus.RUNNING.id;
    }
  }

  updateComments(comments: string): Observable<SessionOverviewPageViewState> {
    return this.restService
      .post(
        [
          'session-overview',
          this.getSnapshot().exploratorySessionOverview.id.toString(),
          'comments',
        ],
        { comments: comments },
      )
      .pipe(
        withLatestFrom(this.state$),
        map(([, state]) => {
          return {
            ...state,
            exploratorySessionOverview: {
              ...state.exploratorySessionOverview,
              comments: comments,
            },
          } satisfies SessionOverviewPageViewState;
        }),
        concatMap((state) => this.updateLastModificationIfNecessary(state)),
        tap((nextState) => this.store.commit(nextState)),
      );
  }

  isExploratoryExecutionRunning(executionId: number): Observable<boolean> {
    return this.restService.get([
      'exploratory-execution',
      executionId.toString(),
      'is-execution-running',
    ]);
  }

  updateSessionNoteCount(count: number) {
    this.store.commit({
      ...this.getSnapshot(),
      exploratorySessionOverview: {
        ...this.getSnapshot().exploratorySessionOverview,
        nbNotes: count,
      },
    });
  }

  fetchUnassignedUsers(): Observable<SimpleUser[]> {
    return this.restService.get([
      'session-overview',
      this.getSnapshot().exploratorySessionOverview.id.toString(),
      'unassigned-users',
    ]);
  }

  updateDueDate(dueDate: Date): Observable<SessionOverviewPageViewState> {
    return this.restService
      .post(
        [
          'session-overview',
          this.getSnapshot().exploratorySessionOverview.id.toString(),
          'due-date',
        ],
        { dueDate: dueDate },
      )
      .pipe(
        withLatestFrom(this.state$),
        map(([, state]) => {
          return {
            ...state,
            exploratorySessionOverview: {
              ...state.exploratorySessionOverview,
              dueDate: dueDate,
            },
          } satisfies SessionOverviewPageViewState;
        }),
        concatMap((state) => this.updateLastModificationIfNecessary(state)),
        tap((nextState) => this.store.commit(nextState)),
      );
  }

  updateSessionDuration(sessionDuration: number): Observable<SessionOverviewPageViewState> {
    return this.restService
      .post(
        [
          'session-overview',
          this.getSnapshot().exploratorySessionOverview.id.toString(),
          'session-duration',
        ],
        { sessionDuration: sessionDuration },
      )
      .pipe(
        withLatestFrom(this.state$),
        map(([, state]) => {
          return {
            ...state,
            exploratorySessionOverview: {
              ...state.exploratorySessionOverview,
              sessionDuration: sessionDuration,
            },
          } satisfies SessionOverviewPageViewState;
        }),
        concatMap((state) => this.updateLastModificationIfNecessary(state)),
        tap((nextState) => this.store.commit(nextState)),
      );
  }
}
