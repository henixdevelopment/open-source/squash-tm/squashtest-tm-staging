import { sqtmAppLogger } from '../../../app-logger';

export const executionRunnerLogger = sqtmAppLogger.compose('execution-runner');
