import { ResearchColumnPrototype, UserHistorySearchProvider, UserListElement } from 'sqtm-core';
import { Injectable } from '@angular/core';
import { SessionOverviewPageService } from '../../../services/session-overview-page.service';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

@Injectable()
export class ExploratoryExecutionAssignableUsersProvider extends UserHistorySearchProvider {
  constructor(private sessionOverviewPageService: SessionOverviewPageService) {
    super();
  }

  provideUserList(_columnPrototype: ResearchColumnPrototype): Observable<UserListElement[]> {
    return this.sessionOverviewPageService.componentData$.pipe(
      take(1),
      map((data) =>
        data.exploratorySessionOverview.assignableUsers.map((u) => ({ ...u, selected: false })),
      ),
    );
  }
}
