import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import {
  AbstractDeleteCellRenderer,
  ActionErrorDisplayService,
  CampaignPermissions,
  DataRow,
  DialogService,
  GridColumnId,
  GridService,
} from 'sqtm-core';
import { Observable, switchMap } from 'rxjs';
import { catchError, finalize, map, take, takeUntil } from 'rxjs/operators';
import { SessionOverviewPageService } from '../../../services/session-overview-page.service';
import { SessionOverviewPageViewState } from '../../../states/session-overview-page-state';

@Component({
  selector: 'sqtm-app-delete-execution-cell-renderer',
  template: ` @if (componentData$ | async; as componentData) {
    @if (canDelete(row)) {
      <div
        nz-tooltip
        [nzTooltipTitle]="
          'sqtm-core.campaign-workspace.test-plan-execution.delete-execution' | translate
        "
        [nzTooltipPlacement]="'topRight'"
        class="inline align-items-center"
      >
        <sqtm-core-delete-icon (delete)="showDeleteConfirm()"></sqtm-core-delete-icon>
      </div>
    }
  }`,
  styleUrls: ['./delete-execution-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteExecutionCellRendererComponent
  extends AbstractDeleteCellRenderer
  implements OnDestroy
{
  componentData$: Observable<SessionOverviewPageViewState>;

  constructor(
    public grid: GridService,
    cdr: ChangeDetectorRef,
    protected dialogService: DialogService,
    private sessionOverviewPageService: SessionOverviewPageService,
    private readonly actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    super(grid, cdr, dialogService);
    this.componentData$ = this.sessionOverviewPageService.componentData$;
  }

  doDelete() {
    this.grid.beginAsyncOperation();

    this.grid.dataRows$
      .pipe(
        takeUntil(this.unsub$),
        take(1),
        map(
          (gridDataRows) =>
            Object.values(gridDataRows)
              .filter((gridDataRow) => gridDataRow !== this.row)
              .map((gridDataRow) => gridDataRow.data[GridColumnId.reviewed]) as boolean[],
        ),
        switchMap((allExecutionsReviewStatusesExceptCurrentRow) =>
          this.sessionOverviewPageService.deleteExecutions(
            [this.row.data[GridColumnId.executionId]],
            [this.row.data[GridColumnId.assigneeId]],
            allExecutionsReviewStatusesExceptCurrentRow,
          ),
        ),
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => this.grid.completeAsyncOperation()),
      )
      .subscribe(() => {
        this.grid.completeAsyncOperation();
        this.grid.refreshData();
      });
  }

  canDelete(row: DataRow): boolean {
    return (
      row.simplePermissions &&
      (row.simplePermissions as CampaignPermissions).canDeleteExecution &&
      !row.data.boundToBlockingMilestone
    );
  }

  protected getTitleKey(): string {
    return 'sqtm-core.campaign-workspace.dialog.title.remove-execution';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.campaign-workspace.dialog.message.remove-execution';
  }
}
