import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  InjectionToken,
  Signal,
} from '@angular/core';
import {
  assignedUserColumn,
  ColumnDefinitionBuilder,
  convertSqtmLiteral,
  DataRow,
  dateTimeColumn,
  deleteColumn,
  Fixed,
  getTranslationKeyFromRunningState,
  GridColumnId,
  GridDefinition,
  GridId,
  GridService,
  indexColumn,
  Limited,
  LocalPersistenceService,
  numericColumn,
  ProjectDataMap,
  smallGrid,
  sortDate,
  SprintStatus,
  SquashTmDataRowType,
  StyleDefinitionBuilder,
  textColumn,
} from 'sqtm-core';
import { sessionExecutionStart } from '../../cell-renderers/session-execution-start-cell-renderer/session-execution-start-cell-renderer.component';
import { TranslateService } from '@ngx-translate/core';
import { exploratoryExecutionTaskDivisionColumn } from '../../cell-renderers/exploratory-execution-task-division-cell-renderer/exploratory-execution-task-division-cell-renderer';
import { ExploratoryExecutionNoteTypesCellRendererComponent } from '../../cell-renderers/exploratory-execution-note-types-cell-renderer/exploratory-execution-note-types-cell-renderer.component';
import { DeleteExecutionCellRendererComponent } from '../../cell-renderers/delete-execution-cell-renderer/delete-execution-cell-renderer.component';
import { SessionOverviewPageService } from '../../../services/session-overview-page.service';
import { filter, switchMap, take, tap } from 'rxjs/operators';
import { booleanReviewLinkColumn } from '../../cell-renderers/boolean-review-link-cell-renderer/boolean-review-link-cell-renderer.component';
import { toSignal } from '@angular/core/rxjs-interop';
import { SessionOverviewPageComponentData } from '../../../containers/session-overview-page/session-overview-page.component';

export const SESSION_EXECUTIONS_TABLE_DEF = new InjectionToken('SESSION_EXECUTIONS_TABLE_DEF');
export const SESSION_EXECUTIONS_TABLE = new InjectionToken('SESSION_EXECUTIONS_TABLE');

export function exploratorySessionExecutionsTableDefinition(
  translateService: TranslateService,
  localPersistenceService: LocalPersistenceService,
): GridDefinition {
  return smallGrid(GridId.EXPLORATORY_SESSION_EXECUTIONS)
    .withColumns([
      indexColumn().withViewport('leftViewport'),
      assignedUserColumn(GridColumnId.assigneeFullName)
        .withI18nKey('sqtm-core.generic.label.user')
        .withAssociatedFilter(GridColumnId.assigneeLogin)
        .changeWidthCalculationStrategy(new Limited(180)),
      exploratoryExecutionTaskDivisionColumn(GridColumnId.taskDivision)
        .withI18nKey('sqtm-core.campaign-workspace.exploratory-execution.task-division')
        .changeWidthCalculationStrategy(new Limited(300))
        .withAssociatedFilter(),
      textColumn(GridColumnId.runningState)
        .withI18nKey('sqtm-core.entity.execution.status.label')
        .changeWidthCalculationStrategy(new Fixed(110))
        .withAssociatedFilter(),
      new ColumnDefinitionBuilder(GridColumnId.noteTypes)
        .withRenderer(ExploratoryExecutionNoteTypesCellRendererComponent)
        .withI18nKey('sqtm-core.campaign-workspace.exploratory-execution.note-types')
        .changeWidthCalculationStrategy(new Fixed(160))
        .disableSort(),
      booleanReviewLinkColumn(GridColumnId.reviewed)
        .withI18nKey('sqtm-core.campaign-workspace.exploratory-execution.review')
        .changeWidthCalculationStrategy(new Fixed(60))
        .withAssociatedFilter(),
      numericColumn(GridColumnId.issueCount)
        .withI18nKey('sqtm-core.entity.execution-plan.ano-number.short')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.ano-number.long')
        .changeWidthCalculationStrategy(new Fixed(60)),
      dateTimeColumn(GridColumnId.lastExecutedOn)
        .withSortFunction(sortDate)
        .withI18nKey('sqtm-core.campaign-workspace.exploratory-execution.date')
        .changeWidthCalculationStrategy(new Limited(150))
        .withAssociatedFilter(),
      sessionExecutionStart(),
      deleteColumn(DeleteExecutionCellRendererComponent)
        .withViewport('rightViewport')
        .withToolTipText(translateService.instant('sqtm-core.generic.label.delete')),
    ])
    .withRowConverter(getRunningStateConverter(translateService))
    .withStyle(new StyleDefinitionBuilder().enableInitialLoadAnimation().showLines())
    .withRowHeight(35)
    .server()
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
}

function getRunningStateConverter(translateService: TranslateService) {
  return (literals: Partial<DataRow>[], projectDataMap: ProjectDataMap) =>
    convertRunningState(literals, projectDataMap, translateService);
}

function convertRunningState(
  literals: Partial<DataRow>[],
  projectDataMap: ProjectDataMap,
  translateService: TranslateService,
): DataRow[] {
  return literals.map((literal) => {
    const dataRow = convertSqtmLiteral(
      { ...literal, type: SquashTmDataRowType.Execution },
      projectDataMap,
    );
    const runningState = literal.data[GridColumnId.runningState];
    literal.data[GridColumnId.runningState] = translateService.instant(
      getTranslationKeyFromRunningState(runningState),
    );
    return dataRow;
  });
}

@Component({
  selector: 'sqtm-app-session-overview-page-executions-panel',
  template: ` <sqtm-core-grid></sqtm-core-grid> `,
  styleUrls: ['./session-overview-page-executions-panel.component.less'],
  providers: [
    {
      provide: GridService,
      useExisting: SESSION_EXECUTIONS_TABLE,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SessionOverviewPageExecutionsPanelComponent implements AfterViewInit {
  $componentData: Signal<SessionOverviewPageComponentData> = toSignal(
    this.sessionOverviewPageService.componentData$,
  );

  constructor(
    public readonly sessionOverviewPageService: SessionOverviewPageService,
    public readonly gridService: GridService,
  ) {}

  ngAfterViewInit(): void {
    this.gridService.loaded$
      .pipe(
        filter((loaded) => loaded),
        take(1),
        switchMap(() => this.sessionOverviewPageService.componentData$),
        filter(
          (componentData) =>
            !componentData.permissions.canDelete ||
            !componentData.milestonesAllowModification ||
            componentData.exploratorySessionOverview.sprintStatus === SprintStatus.CLOSED.id,
        ),
        tap(() => {
          this.gridService.setColumnVisibility(GridColumnId.delete, false);
        }),
      )
      .subscribe();
  }
}
