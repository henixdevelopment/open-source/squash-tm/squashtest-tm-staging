import { Injectable } from '@angular/core';
import {
  ActionErrorDisplayService,
  AttachmentService,
  ClosedSprintLockService,
  CustomFieldValueService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  ExecutionModel,
  ExecutionService,
  ExecutionStatusKeys,
  ExecutionStepService,
  GridService,
  InterWindowCommunicationService,
  InterWindowMessages,
  LocalPersistenceService,
  ReferentialDataService,
  RestService,
  StoreOptions,
  UpdateStatusResponse,
} from 'sqtm-core';
import { ExecutionRunnerState, provideInitialRunnerState } from '../state/execution-runner-state';
import { TranslateService } from '@ngx-translate/core';
import { map, take, tap, withLatestFrom } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AbstractExecutionService } from '../../service/abstract-execution.service';
import { executionStepAdapter } from '../../states/execution-state';

const storeOptions: StoreOptions = {
  id: 'ExecutionRunnerStore',
  logDiff: 'detailed',
};

@Injectable()
export class ExecutionRunnerService extends AbstractExecutionService {
  constructor(
    protected restService: RestService,
    protected referentialDataService: ReferentialDataService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected customFieldValueService: CustomFieldValueService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
    protected customFieldHelper: EntityViewCustomFieldHelperService,
    protected executionService: ExecutionService,
    protected coverageTable: GridService,
    protected interWindowCommunicationService: InterWindowCommunicationService,
    protected executionStepService: ExecutionStepService,
    protected localPersistenceService: LocalPersistenceService,
    closedSprintLockService: ClosedSprintLockService,
    protected actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper,
      executionService,
      coverageTable,
      storeOptions,
      interWindowCommunicationService,
      executionStepService,
      localPersistenceService,
      closedSprintLockService,
      actionErrorDisplayService,
    );
  }

  fetchExecutionData(executionId: number): Observable<ExecutionModel> {
    return this.restService.get<ExecutionModel>(['execution-runner', executionId.toString()]);
  }

  complete() {
    super.complete();
  }

  getInitialState(): ExecutionRunnerState {
    return provideInitialRunnerState();
  }

  navigateToStep(currentStepIndex: number) {
    this.state$
      .pipe(
        take(1),
        map((state) => {
          return { ...state, execution: { ...state.execution, currentStepIndex } };
        }),
      )
      .subscribe((state) => this.commit(state));
  }

  changeExecutionStepStatus(
    executionStepId: number,
    executionStatus: ExecutionStatusKeys,
    executionId: number,
  ): Observable<{ isLastStep: boolean; nextStepIndex: number }> {
    return this.executionStepService
      .changeStatus(executionStepId, executionStatus, executionId)
      .pipe(
        withLatestFrom(this.state$),
        map(([_response, state]: [UpdateStatusResponse, ExecutionRunnerState]) => {
          const executionSteps = executionStepAdapter.updateOne(
            {
              id: executionStepId,
              changes: { executionStatus },
            },
            state.execution.executionSteps,
          );
          return { ...state, execution: { ...state.execution, executionSteps } };
        }),
        tap((state) => this.commit(state)),
        tap(() => {
          this.interWindowCommunicationService.sendMessage(
            new InterWindowMessages('EXECUTION-STEP-CHANGED'),
          );
        }),
        map((state) => ({
          isLastStep: this.isLastStep(state),
          nextStepIndex: state.execution.currentStepIndex + 1,
        })),
      );
  }

  private isLastStep(state: ExecutionRunnerState): boolean {
    return state.execution.currentStepIndex + 1 >= state.execution.executionSteps.ids.length;
  }
}
