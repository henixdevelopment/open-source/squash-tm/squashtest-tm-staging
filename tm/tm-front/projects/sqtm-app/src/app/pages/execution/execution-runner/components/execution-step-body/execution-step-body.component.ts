import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Input,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import {
  ActionErrorDisplayService,
  AttachmentFileSelectorComponent,
  AttachmentHolderType,
  BugTracker,
  CampaignPermissions,
  CustomField,
  CustomFieldBindingData,
  CustomFieldValue,
  DialogService,
  ExecutionStatus,
  GridService,
  gridServiceFactory,
  InterWindowCommunicationService,
  InterWindowMessages,
  LocalPersistenceService,
  PersistedAttachment,
  ReferentialDataService,
  RejectedAttachment,
  RestService,
  RichTextAttachmentDelegate,
} from 'sqtm-core';
import { ExecutionRunnerService } from '../../services/execution-runner.service';
import { ExecutionState, ExecutionStepState } from '../../../states/execution-state';
import { catchError, map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import {
  getRemoteIssueDialogConfiguration,
  RemoteIssueDialogData,
} from '../../../../../components/remote-issue/containers/remote-issue-dialog/remote-issue-dialog.component';
import {
  EXEC_STEP_ISSUE_TABLE,
  EXEC_STEP_ISSUE_TABLE_CONF,
  ExecutionStepIssuesComponent,
  executionStepIssuesTableDefinition,
} from '../execution-step-issues/execution-step-issues.component';

@Component({
  selector: 'sqtm-app-execution-step-body',
  templateUrl: './execution-step-body.component.html',
  styleUrls: ['./execution-step-body.component.less'],
  providers: [
    {
      provide: EXEC_STEP_ISSUE_TABLE_CONF,
      useFactory: executionStepIssuesTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: EXEC_STEP_ISSUE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, EXEC_STEP_ISSUE_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: EXEC_STEP_ISSUE_TABLE,
    },
    {
      provide: RichTextAttachmentDelegate,
      useExisting: ExecutionRunnerService,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExecutionStepBodyComponent {
  readonly bugTracker$: Observable<BugTracker>;

  protected readonly AttachmentHolderType = AttachmentHolderType;

  @ViewChild('issuesPanel')
  issuesPanel: ExecutionStepIssuesComponent;

  get currentStep(): ExecutionStepState {
    const currentStepIndex = this.execution.currentStepIndex;
    const id = this.execution.executionSteps.ids[currentStepIndex];
    return this.execution.executionSteps.entities[id];
  }

  get attachments() {
    return Object.values(this.currentStep.attachmentList.attachments.entities);
  }

  executionStatus = ExecutionStatus;

  @Input()
  execution: ExecutionState;

  @Input()
  permissions: CampaignPermissions;

  @Input()
  milestonesAllowModification: boolean;

  @ViewChild(AttachmentFileSelectorComponent)
  fileSelector: AttachmentFileSelectorComponent;

  private _customFieldBindingData: CustomFieldBindingData[];

  canReportIssue = false;

  get customFields(): CustomField[] {
    return this._customFieldBindingData.map((cufBindingData) => cufBindingData.customField);
  }

  @Input()
  set customFieldBindingData(value: CustomFieldBindingData[]) {
    // Setting cuf bindings only one time else it will instantiate dynamic component at each state emission !
    // cuf bindings will not change during the lifespan of a test case view...
    if (!this._customFieldBindingData) {
      this._customFieldBindingData = value;
    }
  }

  get action() {
    return this.currentStep.action;
  }

  get expectedResult() {
    return this.currentStep.expectedResult;
  }

  constructor(
    private executionRunnerService: ExecutionRunnerService,
    private dialogService: DialogService,
    private viewContainerRef: ViewContainerRef,
    @Inject(EXEC_STEP_ISSUE_TABLE) public readonly issuesTableService: GridService,
    private interWindowCommunicationService: InterWindowCommunicationService,
    private actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    this.bugTracker$ = this.executionRunnerService.componentData$.pipe(
      map((componentData) => componentData.projectData.bugTracker),
    );
  }

  changeComment(comment: string) {
    this.executionRunnerService
      .updateExecutionStepComment(this.currentStep.id, comment)
      .subscribe();
  }

  deleteAttachmentEvent(persistedAttachment: PersistedAttachment) {
    this.executionRunnerService.markAttachmentStepToDelete(this.currentStep.id, [
      persistedAttachment.id,
    ]);
  }

  cancelDeleteAttachment(persistedAttachment: PersistedAttachment) {
    this.executionRunnerService.cancelAttachmentStepToDelete(this.currentStep.id, [
      persistedAttachment.id,
    ]);
  }

  confirmDeleteAttachment(persistedAttachment: PersistedAttachment) {
    this.executionRunnerService
      .deleteStepAttachments(
        [persistedAttachment.id],
        this.currentStep.id,
        this.currentStep.attachmentList.id,
      )
      .subscribe();
  }

  removeRejectedAttachment(rejectedAttachment: RejectedAttachment) {
    this.executionRunnerService.removeStepRejectedAttachments(
      [rejectedAttachment.id],
      this.currentStep.id,
    );
  }

  browseForAttachment(): void {
    this.fileSelector.onClick();
  }

  handleAddAttachments(files: File[]): void {
    this.executionRunnerService
      .addAttachmentsToStep(files, this.currentStep.id, this.currentStep.attachmentList.id)
      .subscribe();
  }

  getCfvValue(cufData: CustomField): string | string[] {
    const cfv = this.getCfv(cufData);
    if (cfv) {
      return cfv.value;
    }
    return '';
  }

  private getCfv(customField: CustomField): CustomFieldValue {
    const cufId: number = customField.id;
    const cfvs: CustomFieldValue[] = Object.values(this.currentStep.customFieldValues.entities);
    return cfvs.find((candidate) => candidate.cufId === cufId);
  }

  updateCustomFieldValue(customField: CustomField, value: string | string[]) {
    const cfv = this.getCfv(customField);
    this.executionRunnerService
      .updateStepCustomFieldValue(this.currentStep.id, cfv.id, value)
      .pipe(catchError((error) => this.actionErrorDisplayService.handleActionError(error)))
      .subscribe();
  }

  shouldShowInformationPanel(): boolean {
    return (
      Boolean(this.execution.datasetLabel) ||
      (this.currentStep.denormalizedCustomFieldValues &&
        this.currentStep.denormalizedCustomFieldValues.length > 0) ||
      (this.execution.kind === 'STANDARD' && this.customFields && this.customFields.length > 0)
    );
  }

  openReportIssueDialog(attachMode: boolean): void {
    this.bugTracker$.pipe(take(1)).subscribe((bugTracker) => {
      const dialogConf = getRemoteIssueDialogConfiguration(
        {
          bugTrackerId: bugTracker.id,
          squashProjectId: this.execution.projectId,
          boundEntityId: this.currentStep.id,
          bindableEntity: 'EXECUTION_STEP_TYPE',
          attachMode,
        },
        this.viewContainerRef,
      );

      const dialogRef = this.dialogService.openDialog<RemoteIssueDialogData, any>(dialogConf);

      dialogRef.dialogResultChanged$.subscribe(() => {
        if (this.issuesPanel) {
          this.issuesPanel.refresh();
          this.interWindowCommunicationService.sendMessage(
            new InterWindowMessages('EXECUTION-STEP-CHANGED'),
          );
        }
      });
    });
  }

  unlinkSelectedIssues($event: MouseEvent): void {
    $event.stopPropagation();
    $event.preventDefault();

    this.issuesPanel.unlinkSelectedIssues();
  }

  notifyIsAuthenticatedInBugTracker(): void {
    // Cannot report issue if test case was deleted
    this.canReportIssue = this.execution.testCaseId != null;
  }
}
