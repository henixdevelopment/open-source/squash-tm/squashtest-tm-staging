import { ChangeDetectionStrategy, Component, Input, OnInit, ViewChild } from '@angular/core';
import {
  ActionErrorDisplayService,
  EditableDateFieldComponent,
  EditableTimeFieldComponent,
  ExecutionStatus,
  ExecutionStatusKeys,
  getFilteredExecutionStatusKeys,
  SessionOverviewStatus,
  SprintStatus,
} from 'sqtm-core';
import { SessionOverviewPageService } from '../../../services/session-overview-page.service';
import { SessionOverviewPageComponentData } from '../../../containers/session-overview-page/session-overview-page.component';
import { catchError, finalize } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-session-overview-page-information-panel',
  templateUrl: './session-overview-page-information-panel.component.html',
  styleUrls: ['./session-overview-page-information-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SessionOverviewPageInformationPanelComponent implements OnInit {
  excludedExecutionStatusKeys: ExecutionStatusKeys[];

  @Input()
  componentData: SessionOverviewPageComponentData;

  @ViewChild('dueDate')
  dueDate: EditableDateFieldComponent;

  @ViewChild('sessionDuration')
  sessionDuration: EditableTimeFieldComponent;

  constructor(
    public readonly sessionOverviewPageService: SessionOverviewPageService,
    private readonly actionErrorDisplayService: ActionErrorDisplayService,
  ) {}

  ngOnInit() {
    const allowedExecutionStatus = getFilteredExecutionStatusKeys(this.componentData.projectData);
    this.excludedExecutionStatusKeys = Object.keys(ExecutionStatus).filter(
      (key) => !allowedExecutionStatus.includes(key as any),
    ) as ExecutionStatusKeys[];
  }

  get canEdit() {
    return (
      this.componentData.permissions.canExecute &&
      this.componentData.milestonesAllowModification &&
      this.componentData.exploratorySessionOverview.sprintStatus !== SprintStatus.CLOSED.id
    );
  }

  get canUpdateExecutionStatus() {
    return (
      this.componentData.exploratorySessionOverview.sessionStatus ===
      SessionOverviewStatus.FINISHED.id
    );
  }

  getSessionStatus(sessionStatus: string): string {
    return SessionOverviewStatus[sessionStatus].i18nKey;
  }

  updateDueDate() {
    this.dueDate.beginAsync();

    this.sessionOverviewPageService
      .updateDueDate(this.dueDate.model)
      .pipe(
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => {
          this.dueDate.cancel();
          this.dueDate.endAsync();
        }),
      )
      .subscribe(() => this.dueDate.endAsync());
  }

  updateSessionDuration() {
    this.sessionDuration.beginAsync();

    this.sessionOverviewPageService
      .updateSessionDuration(this.sessionDuration.model)
      .pipe(
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => {
          this.sessionDuration.endAsync();
          this.sessionDuration.cancel();
          this.sessionDuration.close();
        }),
      )
      .subscribe(() => this.sessionDuration.endAsync());
  }
}
