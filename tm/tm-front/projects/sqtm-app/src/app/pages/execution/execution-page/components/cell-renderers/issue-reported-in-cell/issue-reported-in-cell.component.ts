import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewContainerRef,
} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  DialogService,
  getIssueReportSourceStepsDialogConf,
  GridColumnId,
  GridService,
  IssueReportedInValueRenderer,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { ExecutionPageService } from '../../../services/execution-page.service';
import { ExecutionPageComponentData } from '../../../containers/abstract-execution-page.component';

@Component({
  selector: 'sqtm-app-issue-reported-in-cell',
  template: ` @if (row) {
    <span class="full-width full-height flex-column">
      <label
        class="sqtm-grid-cell-txt-renderer m-auto m-l-1"
        [ngStyle]="labelStyle"
        [sqtmCoreLabelTooltip]="row.data[columnDisplay.id]"
        nz-tooltip
        [nzTooltipTitle]=""
        [nzTooltipPlacement]="'topLeft'"
        (click)="handleClick()"
      >
        {{ getLabelText(executionPageService.componentData$ | async) }}
      </label>
    </span>
  }`,
  styleUrls: ['./issue-reported-in-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IssueReportedInCellComponent extends AbstractCellRendererComponent {
  constructor(
    public gridService: GridService,
    public cdRef: ChangeDetectorRef,
    private translateService: TranslateService,
    private dialogService: DialogService,
    private viewContainerRef: ViewContainerRef,
    public executionPageService: ExecutionPageService,
  ) {
    super(gridService, cdRef);
  }

  get executionSteps(): number[] {
    return this.row.data.executionSteps;
  }

  get isExploratoryExecution(): boolean {
    return this.row.data.reportSites[0].exploratoryExecution;
  }

  get multipleMode(): boolean {
    return this.executionSteps.length > 1;
  }

  get labelStyle() {
    return {
      cursor: this.multipleMode ? 'pointer' : 'default',
      textDecoration: this.multipleMode ? 'underline' : 'default',
      marginLeft: 0,
    };
  }

  getLabelText(componentData: ExecutionPageComponentData) {
    const isExploratory = componentData.execution.executionMode === 'EXPLORATORY';

    if (this.multipleMode) {
      return this.translateService.instant(
        'sqtm-core.campaign-workspace.execution-page.multiple-sources',
      );
    } else {
      const stepOrder: number = this.executionSteps[0];
      if (stepOrder >= 0) {
        const oneBasedStepOrder = stepOrder + 1;
        return this.getReportInLabel(isExploratory, oneBasedStepOrder);
      } else {
        return this.translateService.instant(
          'sqtm-core.campaign-workspace.execution-page.this-execution.label',
        );
      }
    }
  }

  private getReportInLabel(isExploratory: boolean, oneBasedStepOrder: number) {
    const stepLabel = this.translateService.instant(
      'sqtm-core.campaign-workspace.execution-page.step.label',
    );
    const noteLabel = this.translateService.instant(
      'sqtm-core.campaign-workspace.execution-page.note.label',
    );
    if (isExploratory) {
      return `${noteLabel} ${oneBasedStepOrder}`;
    } else {
      return `${stepLabel} ${oneBasedStepOrder}`;
    }
  }

  handleClick(): void {
    if (this.multipleMode) {
      this.dialogService.openDialog(
        getIssueReportSourceStepsDialogConf(
          this.executionSteps,
          this.isExploratoryExecution,
          this.viewContainerRef,
        ),
      );
    }
  }
}

export function issueIssueReportedInColumn(): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(GridColumnId.reportSites)
    .withRenderer(IssueReportedInCellComponent)
    .withExportValueRenderer(IssueReportedInValueRenderer);
}
