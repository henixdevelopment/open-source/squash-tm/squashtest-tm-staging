import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { DRAG_AND_DROP_DATA, DraggedContentRenderer, SessionNoteKind } from 'sqtm-core';
import { SessionNoteState } from '../../../states/execution-state';
import { SessionNoteDndData } from '../../session-note-dnd-data';

@Component({
  selector: 'sqtm-app-dragged-session-note',
  templateUrl: './dragged-session-notes.component.html',
  styleUrls: ['./dragged-session-notes.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DraggedSessionNotesComponent extends DraggedContentRenderer {
  public draggedNotes: SessionNoteState[];

  constructor(@Inject(DRAG_AND_DROP_DATA) public dragAnDropData: SessionNoteDndData) {
    super(dragAnDropData);
    this.draggedNotes = dragAnDropData.data.draggedNotes;
  }

  getNoteKindLabel(note: SessionNoteState): string {
    return SessionNoteKind[note.noteKind].i18nKey;
  }
}
