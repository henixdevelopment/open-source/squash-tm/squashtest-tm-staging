import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { ExecutionStatus, ExecutionStatusKeys } from 'sqtm-core';

@Component({
  selector: 'sqtm-app-execution-runner-status-button',
  templateUrl: './execution-runner-status-button.component.html',
  styleUrls: ['./execution-runner-status-button.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExecutionRunnerStatusButtonComponent {
  @Input()
  status: ExecutionStatusKeys;

  @Input()
  noBorder = true;

  @Output()
  changeStatus = new EventEmitter<ExecutionStatusKeys>();

  @Input()
  tooltip = true;

  getStatusColor(): string {
    return this.executionStatus.color;
  }

  handleClick() {
    this.changeStatus.emit(this.status);
  }

  getI18nKey() {
    return this.executionStatus.i18nKey;
  }

  get executionStatus() {
    return ExecutionStatus[this.status];
  }

  getBorderColor() {
    if (this.noBorder) {
      return this.executionStatus.color;
    }
  }
}
