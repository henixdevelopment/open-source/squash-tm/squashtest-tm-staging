import { Injectable } from '@angular/core';
import {
  ActionErrorDisplayService,
  attachmentEntityAdapter,
  AttachmentHolderType,
  AttachmentService,
  ClosedSprintLockService,
  CustomFieldValueService,
  DateFormatUtils,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  ExecutionModel,
  ExecutionService,
  ExecutionStatusKeys,
  ExecutionStepService,
  ExploratoryExecutionRunningState,
  ExploratorySessionService,
  GenericEntityViewState,
  GridService,
  InterWindowCommunicationService,
  ListItem,
  LocalPersistenceService,
  ReferentialDataService,
  RestService,
  SessionNoteKindKeys,
  SessionNoteModel,
  SessionNoteService,
  StoreOptions,
  UpdateStatusResponse,
  UploadAttachmentEvent,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import {
  catchError,
  concatMap,
  filter,
  map,
  switchMap,
  take,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import { EMPTY, Observable, of } from 'rxjs';
import { AbstractExecutionService } from '../../service/abstract-execution.service';
import { ExecutionPageState, provideInitialPageState } from '../states/execution-page-state';
import {
  ExecutionState,
  executionStepAdapter,
  ExecutionStepState,
  ExistingSessionNoteState,
  isExistingSessionNoteState,
  NewSessionNoteFormState,
  sessionNoteAdapter,
  SessionNoteDropTargetId,
  SessionNotesState,
  SessionNoteState,
} from '../../states/execution-state';
import { Update } from '@ngrx/entity';
import { ExecutionRunnerState } from '../../execution-runner/state/execution-runner-state';

const storeOptions: StoreOptions = {
  id: 'ExecutionPageStore',
  logDiff: 'detailed',
};

@Injectable()
export class ExecutionPageService extends AbstractExecutionService {
  public readonly coverageCount$: Observable<number>;
  private readonly sessionNoteService: SessionNoteService;
  private readonly exploratorySessionService: ExploratorySessionService;
  protected readonly actionErrorDisplayService: ActionErrorDisplayService;

  constructor(
    protected readonly restService: RestService,
    protected readonly referentialDataService: ReferentialDataService,
    protected readonly attachmentService: AttachmentService,
    protected readonly translateService: TranslateService,
    protected readonly customFieldValueService: CustomFieldValueService,
    protected readonly attachmentHelper: EntityViewAttachmentHelperService,
    protected readonly customFieldHelper: EntityViewCustomFieldHelperService,
    protected readonly executionService: ExecutionService,
    protected readonly coverageTable: GridService,
    protected readonly interWindowCommunicationService: InterWindowCommunicationService,
    protected readonly executionStepService: ExecutionStepService,
    protected readonly localPersistenceService: LocalPersistenceService,
    sessionNoteService: SessionNoteService,
    exploratorySessionService: ExploratorySessionService,
    closedSprintLockService: ClosedSprintLockService,
    actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper,
      executionService,
      coverageTable,
      storeOptions,
      interWindowCommunicationService,
      executionStepService,
      localPersistenceService,
      closedSprintLockService,
      actionErrorDisplayService,
    );

    this.sessionNoteService = sessionNoteService;
    this.exploratorySessionService = exploratorySessionService;
    this.actionErrorDisplayService = actionErrorDisplayService;
    this.coverageCount$ = this.componentData$.pipe(
      map((componentData) => componentData.execution.coverages.length),
    );
  }

  fetchExecutionData(executionId: number): Observable<ExecutionModel> {
    return this.restService.getWithoutErrorHandling<ExecutionModel>([
      'execution',
      executionId.toString(),
    ]);
  }

  complete() {
    super.complete();
  }

  getInitialState(): ExecutionPageState {
    return provideInitialPageState();
  }

  removeIssues(issueIds: number[]): Observable<any> {
    return this.removeIssuesServerSide(issueIds).pipe(
      catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
      concatMap(() => this.fetchAndUpdateIssueCount()),
      tap((nextState) => this.store.commit(nextState)),
    );
  }

  private removeIssuesServerSide(issueIds: number[]) {
    const issuesIds = issueIds.join(',');
    return this.restService.delete(['issues', issuesIds]);
  }

  togglePrerequisite() {
    this.store.state$
      .pipe(
        take(1),
        map((state: ExecutionPageState) => {
          const updatedExtentedPrerequisite = !state.execution.extendedPrerequisite;
          return {
            ...state,
            execution: {
              ...state.execution,
              extendedPrerequisite: updatedExtentedPrerequisite,
            },
          };
        }),
      )
      .subscribe((state) => this.commit(state));
  }

  expandAllSteps() {
    this.store.state$
      .pipe(
        take(1),
        map((state: ExecutionPageState) => {
          let executionSteps = { ...state.execution.executionSteps };
          const changes: Update<ExecutionStepState>[] = (executionSteps.ids as number[]).map(
            (id) => {
              return { id: id, changes: { extended: true } };
            },
          );
          executionSteps = executionStepAdapter.updateMany(changes, executionSteps);
          return {
            ...state,
            execution: {
              ...state.execution,
              extendedPrerequisite: true,
              executionSteps: executionSteps,
            },
          };
        }),
      )
      .subscribe((state) => {
        this.commit(state);
        this.localPersistenceService.set(this.stepsExtendedStateStorageKey, true).subscribe();
      });
  }

  collapseAllSteps() {
    this.store.state$
      .pipe(
        take(1),
        map((state: ExecutionPageState) => {
          let executionSteps = { ...state.execution.executionSteps };
          const changes: Update<ExecutionStepState>[] = (executionSteps.ids as number[]).map(
            (id) => {
              return { id: id, changes: { extended: false } };
            },
          );
          executionSteps = executionStepAdapter.updateMany(changes, executionSteps);
          return {
            ...state,
            execution: {
              ...state.execution,
              extendedPrerequisite: false,
              executionSteps: executionSteps,
            },
          };
        }),
      )
      .subscribe((state) => {
        this.commit(state);
        this.localPersistenceService.set(this.stepsExtendedStateStorageKey, false).subscribe();
      });
  }

  updateStepViewScroll(scrollTop: number) {
    this.state$
      .pipe(
        take(1),
        map((state: ExecutionPageState) => {
          return {
            ...state,
            execution: {
              ...state.execution,
              scrollTop: scrollTop,
            },
          };
        }),
      )
      .subscribe((state) => this.commit(state));
  }

  toggleStep(id: number) {
    return this.store.state$
      .pipe(
        take(1),
        map((state: ExecutionPageState) => {
          let executionSteps = { ...state.execution.executionSteps };
          const executionStep = executionSteps.entities[id];
          executionSteps = executionStepAdapter.updateOne(
            {
              id: id,
              changes: { extended: !executionStep.extended },
            },
            executionSteps,
          );
          return { ...state, execution: { ...state.execution, executionSteps } };
        }),
      )
      .subscribe((state) => this.commit(state));
  }

  changeExecutionStepStatus(
    executionStepId: number,
    executionStatus: ExecutionStatusKeys,
    executionId,
  ): Observable<unknown> {
    return this.executionStepService
      .changeStatus(executionStepId, executionStatus, executionId)
      .pipe(
        withLatestFrom(this.state$),
        map(([response, state]: [UpdateStatusResponse, ExecutionPageState]) => {
          const executionSteps = executionStepAdapter.updateOne(
            {
              id: executionStepId,
              changes: {
                executionStatus,
                lastExecutedOn: response.stepLastExecutedOn,
                lastExecutedBy: response.stepLastExecutedBy,
              },
            },
            state.execution.executionSteps,
          );
          return {
            ...state,
            execution: {
              ...state.execution,
              executionStatus: response.executionStatus,
              lastExecutedOn: response.lastExecutedOn,
              lastExecutedBy: response.lastExecutedBy,
              executionSteps,
            },
          } satisfies ExecutionPageState;
        }),
        tap((nextState: ExecutionPageState) => this.store.commit(nextState)),
      );
  }

  refreshIssueCount(): void {
    this.fetchAndUpdateIssueCount()
      .pipe(tap((nextState) => this.store.commit(nextState)))
      .subscribe();
  }

  createSessionNote(noteKind: SessionNoteKindKeys, noteContent: string, noteOrder: number) {
    this.state$
      .pipe(
        take(1),
        map((state) => state.execution.id),
        switchMap((executionId) =>
          this.sessionNoteService.createNote(noteKind, noteContent, noteOrder, executionId),
        ),
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        withLatestFrom(this.state$),
        map(([createResponse, state]) => {
          const newlyCreatedNote: ExistingSessionNoteState =
            this.transformSessionNoteModelIntoState(createResponse);

          const updatedSessionNotes = state.execution.sessionNotes.ids
            .map((id) => state.execution.sessionNotes.entities[id])
            .filter((sessionNote) => isExistingSessionNoteState(sessionNote));

          updatedSessionNotes.splice(noteOrder, 0, newlyCreatedNote);
          updatedSessionNotes.forEach((sessionNote, index) => {
            (sessionNote as ExistingSessionNoteState).noteOrder = index;
            sessionNote.displayOrder = index;
          });
          updatedSessionNotes.push(this.buildNewNoteForm(updatedSessionNotes.length));

          const nextState: ExecutionPageState = {
            ...state,
            execution: {
              ...state.execution,
              sessionNotes: sessionNoteAdapter.setAll(
                updatedSessionNotes,
                state.execution.sessionNotes,
              ),
            },
          };

          return nextState;
        }),
        map((state) => this.updateExecutionMetaData(state)),
      )
      .subscribe((nextState) => this.commit(nextState));
  }

  cancelNewNote() {
    this.state$
      .pipe(
        take(1),
        map((state) => {
          const updatedSessionNotes = state.execution.sessionNotes.ids
            .map((id) => state.execution.sessionNotes.entities[id])
            .filter((sessionNote) => isExistingSessionNoteState(sessionNote));

          updatedSessionNotes.forEach((sessionNote, index) => (sessionNote.displayOrder = index));
          updatedSessionNotes.push(this.buildNewNoteForm(updatedSessionNotes.length));

          const nextState: ExecutionPageState = {
            ...state,
            execution: {
              ...state.execution,
              sessionNotes: sessionNoteAdapter.setAll(
                updatedSessionNotes,
                state.execution.sessionNotes,
              ),
            },
          };

          return nextState;
        }),
      )
      .subscribe((nextState) => this.commit(nextState));
  }

  updateSessionNoteKind(noteId: number, noteKind: SessionNoteKindKeys) {
    return this.sessionNoteService
      .updateNoteKindServerSide(noteId, noteKind)
      .pipe(
        withLatestFrom(this.state$),
        map(([sessionNoteModel, state]) =>
          this.updateExecutionAndSessionNote(noteId, sessionNoteModel, state),
        ),
        map((state) => this.updateExecutionMetaData(state)),
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
      )
      .subscribe((state) => this.commit(state));
  }

  updateSessionNoteContent(
    noteId: number,
    content: string,
  ): Observable<GenericEntityViewState<ExecutionState, 'execution'>> {
    return this.sessionNoteService.updateNoteContentServerSide(noteId, content).pipe(
      withLatestFrom(this.state$),
      map(([noteModel, state]) => this.updateExecutionAndSessionNote(noteId, noteModel, state)),
      map((state) => this.updateExecutionMetaData(state)),
      tap((state) => this.commit(state)),
    );
  }

  deleteNote(note: ExistingSessionNoteState) {
    return this.sessionNoteService
      .deleteNoteServerSide(note.id)
      .pipe(
        withLatestFrom(this.state$),
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        map(([, state]) => {
          const sessionNotesState = state.execution.sessionNotes;
          const updatedNotes = (sessionNotesState.ids as number[])
            .filter((id) => id !== note.id)
            .map((id) => sessionNotesState.entities[id]);

          updatedNotes.forEach((sessionNote, index) => (sessionNote.displayOrder = index));

          const nextState: ExecutionPageState = {
            ...state,
            execution: {
              ...state.execution,
              sessionNotes: sessionNoteAdapter.setAll(updatedNotes, state.execution.sessionNotes),
            },
          };

          return nextState;
        }),
      )
      .subscribe((nextState) => this.commit(nextState));
  }

  addAttachmentsToNote(files: File[], noteId: number, attachmentListId: number) {
    return this.state$.pipe(
      take(1),
      switchMap((state: ExecutionRunnerState) => {
        const entity = this.getEntity(state);
        return this.attachmentHelper.addAttachments(
          files,
          attachmentListId,
          entity.id,
          state.type,
          AttachmentHolderType.SESSION_NOTE,
        );
      }),
      withLatestFrom(this.store.state$),
      map(([event, state]: [UploadAttachmentEvent, ExecutionRunnerState]) => {
        this.commit(this.mapSessionNoteUploadEventToState(noteId, state, event));
      }),
    );
  }

  private updateExecutionAndSessionNote(
    noteId: number,
    noteModel: SessionNoteModel,
    state: ExecutionPageState,
  ): GenericEntityViewState<ExecutionState, 'execution'> {
    const sessionNotes = sessionNoteAdapter.updateOne(
      {
        id: noteId,
        changes: {
          ...this.transformSessionNoteModelIntoState(noteModel),
          displayOrder: state.execution.sessionNotes.entities[noteId].displayOrder,
        },
      },
      state.execution.sessionNotes,
    );

    return {
      ...state,
      execution: {
        ...state.execution,
        lastModifiedBy: noteModel.lastModifiedBy,
        lastModifiedOn: DateFormatUtils.createDateFromIsoString(noteModel.lastModifiedOn),
        sessionNotes,
      },
    };
  }

  private fetchAndUpdateIssueCount(): Observable<ExecutionPageState> {
    return this.state$.pipe(
      take(1),
      concatMap((state) =>
        this.restService.get<{ issueCount: number }>([
          'execution',
          state.execution.id.toString(),
          'issue-count',
        ]),
      ),
      withLatestFrom(this.state$),
      map(([response, state]) => ({
        ...state,
        execution: {
          ...state.execution,
          nbIssues: response.issueCount,
        },
      })),
    );
  }

  markAttachmentNoteToDelete(noteId: number, attachmentIds: string[]) {
    this.changeAttachmentNotePendingDelete(noteId, attachmentIds, true);
  }

  cancelAttachmentNoteToDelete(noteId: number, attachmentIds: string[]) {
    this.changeAttachmentNotePendingDelete(noteId, attachmentIds, false);
  }

  private changeAttachmentNotePendingDelete(
    noteId: number,
    attachmentIds: string[],
    pendingDelete: boolean,
  ) {
    this.state$
      .pipe(
        take(1),
        map((state) => {
          const sessionNote = { ...state.execution.sessionNotes.entities[noteId] };
          if (!isExistingSessionNoteState(sessionNote)) {
            throw new Error('Impossible to attach files in a non-existing note.');
          }
          const attachmentState = this.changeAttachmentDeleteStatus(
            sessionNote.attachmentList.attachments,
            attachmentIds,
            pendingDelete,
          );
          return this.updateSessionNoteAttachmentState(state, attachmentState, noteId);
        }),
      )
      .subscribe((state) => this.commit(state));
  }

  deleteNoteAttachments(attachmentIds: string[], noteId: number, attachmentListId: number) {
    const idsAsNumber = attachmentIds.map((id) => parseInt(id, 10));
    return this.state$.pipe(
      take(1),
      switchMap((state: ExecutionPageState) => {
        const entity = this.getEntity(state);
        return this.attachmentHelper.eraseAttachments(
          idsAsNumber,
          attachmentListId,
          entity.id,
          state.type,
          AttachmentHolderType.SESSION_NOTE,
        );
      }),
      catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
      withLatestFrom(this.store.state$),
      map(([, state]) => {
        const sessionNote = { ...state.execution.sessionNotes.entities[noteId] };
        if (!isExistingSessionNoteState(sessionNote)) {
          throw new Error('Impossible to delete attachments in a non-existing note.');
        }
        const attachmentState = attachmentEntityAdapter.removeMany(
          attachmentIds,
          sessionNote.attachmentList.attachments,
        );
        return this.updateSessionNoteAttachmentState(state, attachmentState, noteId);
      }),
      tap((state) => this.commit(state)),
    );
  }

  removeNoteRejectedAttachments(attachmentIds: string[], noteId: number) {
    this.state$
      .pipe(
        take(1),
        map((state: ExecutionPageState) => {
          const sessionNote = { ...state.execution.sessionNotes.entities[noteId] };
          if (!isExistingSessionNoteState(sessionNote)) {
            throw new Error('Impossible to reject attachments in a non-existing note.');
          }
          const attachmentState = attachmentEntityAdapter.removeMany(
            attachmentIds,
            sessionNote.attachmentList.attachments,
          );
          return this.updateSessionNoteAttachmentState(state, attachmentState, noteId);
        }),
      )
      .subscribe((state) => this.commit(state));
  }

  filterNotesByKinds(
    allNotesBySession: SessionNoteState[],
    selectedKindsItems: ListItem[],
  ): SessionNoteState[] {
    const selectedKinds: string[] = selectedKindsItems.map((item) => item.id.toString());

    return allNotesBySession
      .filter((note) => selectedKinds.includes(note.noteKind))
      .filter((note) => isExistingSessionNoteState(note));
  }

  addNoteForm(noteOrder: number) {
    this.state$
      .pipe(
        take(1),
        map((state) => this.getUpdatedStateWithInsertedNoteForm(noteOrder, state)),
        tap((nextState) => this.store.commit(nextState)),
      )
      .subscribe();
  }

  runExploratoryExecution(executionId: number): Observable<ExecutionPageState> {
    return this.exploratorySessionService.runExploratoryExecution(executionId).pipe(
      withLatestFrom(this.state$),
      map(([, state]) => {
        return {
          ...state,
          execution: {
            ...state.execution,
            exploratoryExecutionRunningState: 'RUNNING',
            latestExploratoryExecutionEvent: {
              timestamp: new Date(),
              timeElapsed: state.execution.latestExploratoryExecutionEvent?.timeElapsed ?? 0,
            },
          },
        } satisfies ExecutionPageState;
      }),
      map((state) => this.updateExecutionMetaData(state)),
      tap((nextState) => this.store.commit(nextState)),
    );
  }

  pauseExploratoryExecution(executionId: number): Observable<ExecutionPageState> {
    return this.exploratorySessionService.pauseExploratoryExecution(executionId).pipe(
      withLatestFrom(this.state$),
      map(([, state]) => this.updateLatestExploratoryExecutionEvent(state, 'PAUSED')),
      map((state) => this.updateExecutionMetaData(state)),
      tap((nextState) => this.store.commit(nextState)),
      tap(() => this.cancelNewNote()),
    );
  }

  stopExploratoryExecution(executionId: number): Observable<ExecutionPageState> {
    return this.state$.pipe(
      take(1),
      filter((state) => state.execution.exploratoryExecutionRunningState !== 'STOPPED'),
      concatMap(() => this.exploratorySessionService.stopExploratoryExecution(executionId)),
      withLatestFrom(this.state$),
      map(([, state]) => this.updateLatestExploratoryExecutionEvent(state, 'STOPPED')),
      map((state) => this.updateExecutionMetaData(state)),
      tap((nextState) => this.store.commit(nextState)),
      tap(() => this.cancelNewNote()),
    );
  }

  updateReviewStatus(reviewed: boolean): Observable<ExecutionPageState> {
    return this.exploratorySessionService
      .updateReviewStatus(this.getSnapshot().execution.id.toString(), reviewed)
      .pipe(
        withLatestFrom(this.state$),
        map(([, state]) => {
          return {
            ...state,
            execution: {
              ...state.execution,
              reviewed: reviewed,
            },
          };
        }),
        tap((nextState) => this.store.commit(nextState)),
      );
  }

  updateComment(newComment: string): Observable<ExecutionPageState> {
    return this.executionService.updateComment(this.getSnapshot().execution.id, newComment).pipe(
      withLatestFrom(this.state$),
      map(([, state]) => {
        return {
          ...state,
          execution: {
            ...state.execution,
            comment: newComment,
          },
        };
      }),
      tap((nextState) => this.store.commit(nextState)),
    );
  }

  private updateLatestExploratoryExecutionEvent(
    executionPageState: ExecutionPageState,
    runningState: ExploratoryExecutionRunningState,
  ): ExecutionPageState {
    const timeElapsedSoFar =
      executionPageState.execution.latestExploratoryExecutionEvent?.timeElapsed ?? 0;
    const latestEventTime =
      executionPageState.execution.latestExploratoryExecutionEvent.timestamp.getTime();
    const timeElapsedSinceLatestEvent = (Date.now() - latestEventTime) / 1000;
    const updatedTimeElapsed = Math.floor(timeElapsedSoFar + timeElapsedSinceLatestEvent);

    return {
      ...executionPageState,
      execution: {
        ...executionPageState.execution,
        exploratoryExecutionRunningState: runningState,
        latestExploratoryExecutionEvent: {
          timestamp: new Date(),
          timeElapsed: updatedTimeElapsed,
        },
      },
    };
  }

  private updateExecutionMetaData(executionPageState: ExecutionPageState): ExecutionPageState {
    return {
      ...executionPageState,
      execution: {
        ...executionPageState.execution,
        lastExecutedBy: this.referentialDataService.getSnapshot().userState.username,
        lastExecutedOn: new Date(),
      },
    };
  }

  private getUpdatedStateWithInsertedNoteForm(
    noteOrder: number,
    state: ExecutionPageState,
  ): ExecutionPageState {
    return {
      ...state,
      execution: {
        ...state.execution,
        sessionNotes: this.insertNoteForm(state.execution.sessionNotes, noteOrder),
      },
    };
  }

  private insertNoteForm(
    sessionNotes: SessionNotesState,
    previousNoteOrder: number,
  ): SessionNotesState {
    const newNoteForm: NewSessionNoteFormState = this.buildNewNoteForm(previousNoteOrder + 1);

    const existingSessionNotes: SessionNoteState[] = sessionNotes.ids
      .map((id) => sessionNotes.entities[id])
      .filter((note) => isExistingSessionNoteState(note));

    existingSessionNotes.splice(previousNoteOrder, 0, newNoteForm);
    existingSessionNotes.forEach((note, index) => (note.displayOrder = index));

    return sessionNoteAdapter.setAll(existingSessionNotes, sessionNotes);
  }

  cancelSessionNoteDrag(): void {
    this.state$
      .pipe(
        take(1),
        map((state) => {
          // Add creation form. The display order doesn't matter as it will be overwritten later.
          let sessionNotesState = sessionNoteAdapter.addOne(
            this.buildNewNoteForm(0),
            state.execution.sessionNotes,
          );

          // Restore initial order
          const updates: Update<SessionNoteState>[] =
            state.execution.sessionNotes.initialStepOrder.map((id, index) => ({
              id,
              changes: { displayOrder: index },
            }));
          sessionNotesState = sessionNoteAdapter.updateMany(updates, sessionNotesState);

          return {
            ...state,
            execution: {
              ...state.execution,
              sessionNotes: {
                ...sessionNotesState,
                initialStepOrder: [],
                draggingNotes: false,
                selectedNoteIds: [],
                currentDndTargetId: null,
              },
            },
          };
        }),
        tap((nextState) => this.store.commit(nextState)),
      )
      .subscribe();
  }

  startDraggingNotes(draggedNoteIds: number[]): void {
    this.componentData$.pipe(take(1)).subscribe((state) => {
      const initialStepOrder: number[] = [...state.execution.sessionNotes.ids] as number[];
      const sessionNotes = this.removeNoteCreationForm(state.execution.sessionNotes);

      const nextState: ExecutionPageState = {
        ...state,
        execution: {
          ...state.execution,
          sessionNotes: {
            ...sessionNoteAdapter.setAll(sessionNotes, state.execution.sessionNotes),
            selectedNoteIds: draggedNoteIds,
            initialStepOrder,
            currentDndTargetId: null,
            draggingNotes: true,
          },
        },
      };

      const currentIndex = initialStepOrder.indexOf(draggedNoteIds[0]);
      const nextValidTarget = initialStepOrder
        .slice(currentIndex)
        .find((id) => !draggedNoteIds.includes(id));

      this.dragOverNote(nextValidTarget, nextState);
    });
  }

  private removeNoteCreationForm(sessionNotesState: SessionNotesState): SessionNoteState[] {
    const sessionNotes = (sessionNotesState.ids as number[])
      // Remove creation form
      .filter((id) => id !== -1)
      .map((id) => sessionNotesState.entities[id]);

    // Re-affect order
    sessionNotes.forEach((note, index) => ((note as ExistingSessionNoteState).noteOrder = index));

    return sessionNotes;
  }

  dragOverNote(targetId: SessionNoteDropTargetId, state: ExecutionPageState): void {
    if (!state.execution.sessionNotes.draggingNotes) {
      return;
    }

    if (state.execution.sessionNotes.currentDndTargetId === targetId) {
      return;
    }

    if (state.execution.sessionNotes.selectedNoteIds.includes(targetId as number)) {
      return;
    }

    const nextState = this.updateStateOnDragOver(targetId, state);
    this.commit(nextState);
  }

  private updateStateOnDragOver(
    targetId: SessionNoteDropTargetId,
    state: Readonly<ExecutionPageState>,
  ): ExecutionPageState {
    const movedStepIds = state.execution.sessionNotes.selectedNoteIds;
    const ids = state.execution.sessionNotes.ids as number[];
    const nextOrder = ids.filter((id) => !movedStepIds.includes(id));
    const dropAtLast = targetId == null || targetId === 'LAST_POSITION_DROP_ZONE';
    const dropIndex = dropAtLast ? nextOrder.length : nextOrder.indexOf(targetId);
    nextOrder.splice(dropIndex, 0, ...movedStepIds);
    const sessionNotes: SessionNotesState = this.reorderNotes(
      nextOrder,
      state.execution.sessionNotes,
    );
    sessionNotes.currentDndTargetId = targetId;
    return { ...state, execution: { ...state.execution, sessionNotes } };
  }

  private reorderNotes(newOrder: number[], state: Readonly<SessionNotesState>): SessionNotesState {
    const updates: Update<SessionNoteState>[] = newOrder.map((id, index) => ({
      id,
      changes: { displayOrder: index },
    }));

    return sessionNoteAdapter.updateMany(updates, state);
  }

  dropSessionNotes(): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap((state) => this.updateSessionNotesOrderAfterDrop(state)),
      tap((state) => this.store.commit(state)),
    );
  }

  private updateSessionNotesOrderAfterDrop(
    state: ExecutionPageState,
  ): Observable<ExecutionPageState> {
    const allSessionNotes = state.execution.sessionNotes.ids.map(
      (id) => state.execution.sessionNotes.entities[id],
    );
    const targetId = state.execution.sessionNotes.currentDndTargetId;
    const unselectedNotes = allSessionNotes.filter(
      (n) => !state.execution.sessionNotes.selectedNoteIds.includes(n.id),
    );
    const unselectedExistingNotes = unselectedNotes.filter((n) => n.id !== -1);
    const isDroppingAtBottom = targetId === 'LAST_POSITION_DROP_ZONE';
    const newIndex = isDroppingAtBottom
      ? unselectedExistingNotes.length
      : unselectedNotes.findIndex((note) => note.id === targetId);
    const requestBody = {
      movedNoteIds: state.execution.sessionNotes.selectedNoteIds,
      newIndex,
    };

    return this.restService
      .post(['execution', state.execution.id.toString(), 'move-notes'], requestBody)
      .pipe(
        withLatestFrom(this.state$),
        map(([, currentState]) => this.getStateAfterDrop(currentState)),
      );
  }

  private getStateAfterDrop(state: ExecutionPageState): ExecutionPageState {
    const sessionNotes = this.removeNoteCreationForm(state.execution.sessionNotes);

    // Add creation form back
    sessionNotes.push(this.buildNewNoteForm(sessionNotes.length));

    return {
      ...state,
      execution: {
        ...state.execution,
        sessionNotes: {
          ...sessionNoteAdapter.setAll(sessionNotes, state.execution.sessionNotes),
          initialStepOrder: [],
          draggingNotes: false,
          selectedNoteIds: [],
          currentDndTargetId: null,
        },
      },
    };
  }

  moveNoteUp(sessionNoteId: number): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap((state) => this.updateSessionNotesOrderAfterMoveUp(sessionNoteId, state)),
      tap((state) => this.store.commit(state)),
    );
  }

  private updateSessionNotesOrderAfterMoveUp(
    sessionNoteId: number,
    state: ExecutionPageState,
  ): Observable<ExecutionPageState> {
    const allSessionNotes = state.execution.sessionNotes.ids.map(
      (id) => state.execution.sessionNotes.entities[id],
    );

    const currentIndex = allSessionNotes.findIndex((note) => note.id === sessionNoteId);
    const nextIndex = currentIndex - 1;
    return this.moveNote(sessionNoteId, allSessionNotes, currentIndex, nextIndex, state);
  }

  moveNoteDown(sessionNoteId: number): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap((state) => this.updateSessionNotesOrderAfterMoveDown(sessionNoteId, state)),
      tap((state) => this.store.commit(state)),
    );
  }

  private updateSessionNotesOrderAfterMoveDown(
    sessionNoteId: number,
    state: ExecutionPageState,
  ): Observable<ExecutionPageState> {
    const allSessionNotes = state.execution.sessionNotes.ids.map(
      (id) => state.execution.sessionNotes.entities[id],
    );

    const currentIndex = allSessionNotes.findIndex((note) => note.id === sessionNoteId);
    const nextIndex = currentIndex + 1;
    return this.moveNote(sessionNoteId, allSessionNotes, currentIndex, nextIndex, state);
  }

  private moveNote(
    sessionNoteId: number,
    allSessionNotes: SessionNoteState[],
    currentIndex: number,
    nextIndex: number,
    state: ExecutionPageState,
  ): Observable<ExecutionPageState> {
    // Cannot move a note out of bounds
    if (nextIndex < 0 || nextIndex >= allSessionNotes.length) {
      return EMPTY;
    }

    const noteToMove = allSessionNotes[currentIndex];
    const noteToSwitchPositionWith = allSessionNotes[nextIndex];

    if (
      !isExistingSessionNoteState(noteToSwitchPositionWith) ||
      !isExistingSessionNoteState(noteToMove)
    ) {
      // The other note is the creation form so that's a front-end change only
      return of(this.getStateAfterMovingNote(currentIndex, nextIndex, state));
    } else {
      const requestBody = {
        movedNoteIds: [sessionNoteId],
        newIndex: nextIndex,
      };

      return this.restService
        .post(['execution', state.execution.id.toString(), 'move-notes'], requestBody)
        .pipe(
          withLatestFrom(this.state$),
          map(([, currentState]) =>
            this.getStateAfterMovingNote(currentIndex, nextIndex, currentState),
          ),
        );
    }
  }

  private getStateAfterMovingNote(
    previousIndex: number,
    newIndex: number,
    state: ExecutionPageState,
  ): ExecutionPageState {
    const sessionNotes = (state.execution.sessionNotes.ids as number[]).map(
      (id) => state.execution.sessionNotes.entities[id],
    );

    sessionNotes.splice(newIndex, 0, ...sessionNotes.splice(previousIndex, 1));

    // Re-affect order
    sessionNotes.forEach((note, index) => {
      if (isExistingSessionNoteState(note)) {
        note.noteOrder = index;
      }

      note.displayOrder = index;
    });

    return {
      ...state,
      execution: {
        ...state.execution,
        sessionNotes: {
          ...sessionNoteAdapter.setAll(sessionNotes, state.execution.sessionNotes),
        },
      },
    };
  }

  expandAllNotes() {
    this.updateAllNotesExpansion(true);
  }

  collapseAllNotes() {
    this.updateAllNotesExpansion(false);
  }

  private updateAllNotesExpansion(shouldBeExpanded: boolean) {
    const state = this.getSnapshot();
    const changes: Update<SessionNoteState>[] = (state.execution.sessionNotes.ids as number[]).map(
      (id) => ({ id: id, changes: { expanded: shouldBeExpanded } }),
    );

    this.commit({
      ...state,
      execution: {
        ...state.execution,
        sessionNotes: sessionNoteAdapter.updateMany(changes, state.execution.sessionNotes),
      },
    });
    this.localPersistenceService
      .set(this.notesExpandedStateStorageKey, shouldBeExpanded)
      .subscribe();
  }

  expandNote(noteId: number) {
    this.updateNoteExpansion(noteId, true);
  }

  collapseNote(noteId: number) {
    this.updateNoteExpansion(noteId, false);
  }

  private updateNoteExpansion(noteId: number, shouldBeExpanded: boolean) {
    const state = this.getSnapshot();
    this.commit({
      ...state,
      execution: {
        ...state.execution,
        sessionNotes: sessionNoteAdapter.updateOne(
          { id: noteId, changes: { expanded: shouldBeExpanded } },
          state.execution.sessionNotes,
        ),
      },
    });
  }
}
