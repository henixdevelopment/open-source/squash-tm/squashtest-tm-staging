import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { CampaignPermissions, ExecutionStatus, ExecutionStatusKeys } from 'sqtm-core';
import { ExecutionState, ExecutionStepState } from '../../../states/execution-state';

@Component({
  selector: 'sqtm-app-execution-runner-toolbar',
  templateUrl: './execution-runner-toolbar.component.html',
  styleUrls: ['./execution-runner-toolbar.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExecutionRunnerToolbarComponent {
  get currentStep(): ExecutionStepState {
    const currentStepIndex = this.execution.currentStepIndex;
    const id = this.execution.executionSteps.ids[currentStepIndex];
    return this.execution.executionSteps.entities[id];
  }

  executionStatus = ExecutionStatus;

  menuVisible = false;

  @Input()
  execution: ExecutionState;

  @Input()
  showFastForwardButton: boolean;

  @Input()
  permissions: CampaignPermissions;

  @Input()
  milestonesAllowModification: boolean;

  @Input()
  allowTcModifDuringExec: boolean;

  @Input()
  disabledExecutionStatus: ExecutionStatusKeys[] = [];

  @Output()
  navigateToPrologue: EventEmitter<number> = new EventEmitter<number>();

  @Output()
  navigateToStep: EventEmitter<{ executionId: number; stepIndex: number }> = new EventEmitter<{
    executionId: number;
    stepIndex: number;
  }>();

  @Output()
  modificationDuringExecutionStep: EventEmitter<{ executionId: number; executionStepId: number }> =
    new EventEmitter<{ executionId: number; executionStepId: number }>();

  @Output()
  changeStatus = new EventEmitter<ChangeStatusEvent>();

  @Output()
  fastForwardToNextExecution: EventEmitter<void> = new EventEmitter<void>();

  get totalStepCount() {
    return this.execution.executionSteps.ids.length;
  }

  handleNavigateTo(newStepIndex: number) {
    if (newStepIndex < 0) {
      this.navigateToPrologue.emit(this.execution.id);
    } else {
      this.navigateToStep.emit({ executionId: this.execution.id, stepIndex: newStepIndex });
    }
  }

  handleChangeStatus(executionStatus: ExecutionStatusKeys) {
    const executionStepId = this.currentStep.id;
    const executionId = this.execution.id;
    this.changeStatus.emit({ executionId, executionStepId, executionStatus });
  }

  handleFastForwardToNextExecution() {
    this.fastForwardToNextExecution.emit();
  }

  handleModificationDuringExecution() {
    this.modificationDuringExecutionStep.emit({
      executionId: this.execution.id,
      executionStepId: this.currentStep.id,
    });
  }
}

export interface ChangeStatusEvent {
  executionId: number;
  executionStepId: number;
  executionStatus: ExecutionStatusKeys;
}
