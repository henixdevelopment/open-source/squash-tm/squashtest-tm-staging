import { ChangeDetectionStrategy, Component } from '@angular/core';
import { SessionOverviewPageService } from '../../../services/session-overview-page.service';
@Component({
  selector: 'sqtm-app-session-overview-page-charter-panel',
  templateUrl: './session-overview-page-charter-panel.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SessionOverviewPageCharterPanelComponent {
  constructor(public readonly sessionOverviewPageService: SessionOverviewPageService) {}
}
