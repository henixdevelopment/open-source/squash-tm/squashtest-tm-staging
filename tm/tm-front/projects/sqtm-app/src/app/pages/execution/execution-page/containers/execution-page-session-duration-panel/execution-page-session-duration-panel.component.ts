import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  Signal,
} from '@angular/core';
import { ExecutionPageService } from '../../services/execution-page.service';
import { catchError, concatMap, filter } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import {
  ActionErrorDisplayService,
  CapsuleInformationData,
  DialogService,
  SprintStatus,
} from 'sqtm-core';
import { ExecutionState } from '../../../states/execution-state';
import { ExecutionPageComponentData } from '../abstract-execution-page.component';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sqtm-app-execution-page-session-duration-panel',
  templateUrl: './execution-page-session-duration-panel.component.html',
  styleUrls: ['./execution-page-session-duration-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExecutionPageSessionDurationPanelComponent implements OnInit, OnDestroy {
  timeElapsedCapsuleInfoData: CapsuleInformationData;
  refreshElapsedTimeIntervalId: any;
  shouldShowDialog = true;

  $componentData: Signal<ExecutionPageComponentData> = toSignal(
    this.executionPageService.componentData$,
  );

  constructor(
    public readonly executionPageService: ExecutionPageService,
    private readonly translateService: TranslateService,
    private readonly actionErrorDisplayService: ActionErrorDisplayService,
    private readonly cdRef: ChangeDetectorRef,
    private readonly dialogService: DialogService,
  ) {}

  ngOnInit(): void {
    this.refreshElapsedTimeIntervalId = setInterval(() => {
      const isSessionDurationSet =
        this.executionPageService.getSnapshot().execution.exploratorySessionOverviewInfo
          .sessionDuration;
      if (isSessionDurationSet && this.shouldShowDialog) {
        this.checkTimeRemaining();
      }
      this.refreshElapsedTime();
    }, 50);
  }

  ngOnDestroy(): void {
    clearInterval(this.refreshElapsedTimeIntervalId);
  }

  get canEdit() {
    return (
      this.$componentData().permissions.canExecute &&
      this.$componentData().milestonesAllowModification &&
      this.$componentData().execution.parentSprintStatus !== SprintStatus.CLOSED.id
    );
  }

  private refreshElapsedTime(): void {
    const state = this.executionPageService.getSnapshot();
    this.timeElapsedCapsuleInfoData = this.getTimeElapsedCapsuleInfoData(state.execution);
    this.cdRef.detectChanges();
  }

  private checkTimeRemaining() {
    const state = this.executionPageService.getSnapshot();

    if (state.execution.exploratoryExecutionRunningState === 'RUNNING') {
      const minutesElapsed = this.getMillisecondsElapsed() / 60000;

      if (minutesElapsed >= state.execution.exploratorySessionOverviewInfo.sessionDuration) {
        this.shouldShowDialog = false;
        this.openTimeOverAlertDialog(state.execution.id);
      }
    }
  }

  private getMillisecondsElapsed() {
    const state = this.executionPageService.getSnapshot();
    const now = new Date(Date.now()).getTime();
    const latestTimestamp = state.execution.latestExploratoryExecutionEvent.timestamp.getTime();
    const latestTimeElapsed = state.execution.latestExploratoryExecutionEvent.timeElapsed * 1000;
    return now - latestTimestamp + latestTimeElapsed;
  }

  isSessionDurationSet(execution: ExecutionState): boolean {
    return (
      execution.exploratorySessionOverviewInfo.sessionDuration !== null &&
      execution.exploratorySessionOverviewInfo.sessionDuration !== 0
    );
  }

  isSessionRunning(execution: ExecutionState): boolean {
    return execution.exploratoryExecutionRunningState === 'RUNNING';
  }

  isExploratoryExecutionStoppedOrNeverStarted(execution: ExecutionState): boolean {
    return (
      execution.exploratoryExecutionRunningState === 'STOPPED' ||
      execution.exploratoryExecutionRunningState === 'NEVER_STARTED'
    );
  }

  private getTimeElapsedCapsuleInfoData(executionState: ExecutionState): CapsuleInformationData {
    return {
      labelI18nKey:
        this.translateService.instant(
          'sqtm-core.campaign-workspace.execution-page.timer.time-elapsed',
        ) + this.getTimeElapsed(executionState),
    };
  }

  getSessionDurationCapsuleInfoData(executionState: ExecutionState): CapsuleInformationData {
    return {
      labelI18nKey:
        this.translateService.instant(
          'sqtm-core.campaign-workspace.execution-page.timer.session-duration',
        ) + this.getSessionDurationTime(executionState),
    };
  }

  getExploratoryExecutionStatusCapsuleInfoData(
    executionState: ExecutionState,
  ): CapsuleInformationData {
    let status: string;
    let statusColor: string;

    switch (executionState.exploratoryExecutionRunningState) {
      case 'RUNNING':
      case 'PAUSED': {
        status = 'sqtm-core.campaign-workspace.exploratory-execution.status.running';
        statusColor = '#0078AE';
        break;
      }
      case 'STOPPED': {
        status = 'sqtm-core.campaign-workspace.exploratory-execution.status.finished';
        statusColor = '#26BE06';
        break;
      }
      default:
        status = 'sqtm-core.campaign-workspace.exploratory-execution.status.to-do';
        statusColor = '#FFCC00';
        break;
    }

    return {
      labelI18nKey: this.translateService.instant(status),
      color: statusColor,
    };
  }

  getTimeElapsed(executionState: ExecutionState): string {
    let millisecondsElapsed;

    if (executionState.exploratoryExecutionRunningState === 'RUNNING') {
      millisecondsElapsed = this.getMillisecondsElapsed();
    } else {
      const timeElapsed = executionState.latestExploratoryExecutionEvent?.timeElapsed ?? 0;
      millisecondsElapsed = timeElapsed * 1000;
    }

    if (millisecondsElapsed >= 86400000) {
      return (
        ' ' +
        this.translateService.instant(
          'sqtm-core.campaign-workspace.exploratory-execution.capsule.one-day',
        )
      );
    } else {
      return this.buildTimeElapsedString(millisecondsElapsed);
    }
  }

  runExploratoryExecution(componentData: ExecutionPageComponentData) {
    if (!componentData.permissions.canExecute || !componentData.milestonesAllowModification) {
      return;
    }

    if (
      this.executionPageService.getSnapshot().execution.latestExploratoryExecutionEvent !== null
    ) {
      const minutesElapsed = this.getMillisecondsElapsed() / 60000;
      if (
        minutesElapsed >=
        this.executionPageService.getSnapshot().execution.exploratorySessionOverviewInfo
          .sessionDuration
      ) {
        this.shouldShowDialog = false;
      }
    }
    const executionId = componentData.execution.id;
    this.executionPageService
      .runExploratoryExecution(executionId)
      .pipe(catchError((err) => this.actionErrorDisplayService.handleActionError(err)))
      .subscribe();
  }

  pauseExploratoryExecution(componentData: ExecutionPageComponentData) {
    if (!componentData.permissions.canExecute) {
      return;
    }

    const executionId = componentData.execution.id;
    this.executionPageService
      .pauseExploratoryExecution(executionId)
      .pipe(catchError((err) => this.actionErrorDisplayService.handleActionError(err)))
      .subscribe();
  }

  stopExploratoryExecution(componentData: ExecutionPageComponentData) {
    if (!componentData.permissions.canExecute) {
      return;
    }

    const executionId = componentData.execution.id;
    this.openEndExploratoryExecutionDialog(executionId);
  }

  getSessionDurationTime(executionState: ExecutionState): string {
    const sessionDurationInMinutes = executionState.exploratorySessionOverviewInfo.sessionDuration;
    const hours = Math.floor(sessionDurationInMinutes / 60);
    const minutes = sessionDurationInMinutes - hours * 60;
    return formatAsHHMMSS(hours, minutes, 0);
  }

  private buildTimeElapsedString(millisecondsElapsed: number): string {
    const secondsElapsed = Math.floor(millisecondsElapsed / 1000);
    const hours = Math.floor(secondsElapsed / 3600);
    const secondsElapsedWithoutHours = secondsElapsed - hours * 3600;
    const minutes = Math.floor(secondsElapsedWithoutHours / 60);
    const seconds = secondsElapsedWithoutHours - minutes * 60;
    return formatAsHHMMSS(hours, minutes, seconds);
  }

  private openEndExploratoryExecutionDialog(executionId: number) {
    const dialogReference = this.dialogService.openConfirm({
      id: 'end-exploratory-execution',
      titleKey:
        'sqtm-core.campaign-workspace.exploratory-execution.dialog.confirm-end-exploratory-execution.title',
      messageKey:
        'sqtm-core.campaign-workspace.exploratory-execution.dialog.confirm-end-exploratory-execution.message',
      level: 'WARNING',
    });

    return dialogReference.dialogClosed$
      .pipe(
        filter((confirm) => confirm),
        concatMap(() => this.executionPageService.stopExploratoryExecution(executionId)),
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
      )
      .subscribe();
  }

  private openTimeOverAlertDialog(executionId: number) {
    const dialogReference = this.dialogService.openConfirm({
      id: 'time-over-exploratory-execution',
      titleKey: 'sqtm-core.campaign-workspace.exploratory-execution.dialog.alert-time-over.title',
      messageKey:
        'sqtm-core.campaign-workspace.exploratory-execution.dialog.alert-time-over.message',
      level: 'WARNING',
      confirmButtonText:
        'sqtm-core.campaign-workspace.exploratory-execution.dialog.alert-time-over.stop-button',
      cancelButtonText:
        'sqtm-core.campaign-workspace.exploratory-execution.dialog.alert-time-over.resume-button',
    });

    return dialogReference.dialogClosed$
      .pipe(
        filter((confirm) => confirm),
        concatMap(() => this.executionPageService.stopExploratoryExecution(executionId)),
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
      )
      .subscribe();
  }
}

function formatAsHHMMSS(hours: number, minutes: number, seconds: number): string {
  return ` ${zeroPaddedLeft(hours)}:${zeroPaddedLeft(minutes)}:${zeroPaddedLeft(seconds)}`;
}

function zeroPaddedLeft(value: number): string {
  return value < 10 ? '0' + value : value.toString();
}
