import { EntityViewState, provideInitialViewState } from 'sqtm-core';
import { ExecutionState } from '../../states/execution-state';

export interface ExecutionRunnerState extends EntityViewState<ExecutionState, 'execution'> {
  execution: ExecutionState;
}

export function provideInitialRunnerState(): Readonly<ExecutionRunnerState> {
  return {
    ...provideInitialViewState<ExecutionState, 'execution'>('execution'),
  };
}
