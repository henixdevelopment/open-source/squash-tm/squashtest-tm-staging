import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { asyncScheduler, BehaviorSubject, Observable, Subject } from 'rxjs';
import { ExecutionPageComponentData } from '../../abstract-execution-page.component';
import { ExecutionPageService } from '../../../services/execution-page.service';
import { filter, map, take, takeUntil, tap } from 'rxjs/operators';
import {
  DataRowModel,
  GridColumnId,
  GridResponse,
  GroupedMultiListFieldComponent,
  IssuesService,
  ListItem,
  Permissions,
  RestService,
  RichTextFieldComponent,
  SessionNoteKind,
  SessionNoteKindKeys,
  SprintStatus,
  SqtmDragOverEvent,
} from 'sqtm-core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {
  ExistingSessionNoteState,
  isExistingSessionNoteState,
  SessionNoteDropTargetId,
  SessionNoteState,
} from '../../../../states/execution-state';
import { SessionNoteComponent } from '../../../components/session-note/session-note.component';
import {
  EXPLORATORY_EXECUTION_PAGE_ORIGIN,
  SessionNoteDndData,
} from '../../../session-note-dnd-data';
import { DraggedSessionNotesComponent } from '../../../components/dragged-session-notes/dragged-session-notes.component';

@Component({
  selector: 'sqtm-app-execution-page-charter',
  templateUrl: './execution-page-charter.component.html',
  styleUrls: ['./execution-page-charter.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExecutionPageCharterComponent implements OnInit, OnDestroy {
  @ViewChild(GroupedMultiListFieldComponent)
  noteKindsList: GroupedMultiListFieldComponent;

  @ViewChild(SessionNoteComponent)
  private sessionNoteComponent: SessionNoteComponent;

  @ViewChild('newNoteContainer', { read: ElementRef<HTMLDivElement> })
  private newNoteContainer: ElementRef<HTMLDivElement>;

  @ViewChild('newNoteContentField')
  private newNoteContentField: RichTextFieldComponent;

  componentData$: Observable<ExecutionPageComponentData>;

  allNotesBySession: SessionNoteState[];
  filteredNotes: SessionNoteState[];

  newNoteKind: SessionNoteKindKeys = ExecutionPageCharterComponent.DEFAULT_KIND_VALUE;

  formGroup: FormGroup;

  private readonly _authenticated: BehaviorSubject<boolean>;

  public authenticated$: Observable<boolean>;

  issues: DataRowModel[] = [];

  private unsub$ = new Subject<void>();

  private static DEFAULT_KIND_VALUE: SessionNoteKindKeys = 'COMMENT';

  noteKindItems: ListItem[] = [
    { id: SessionNoteKind.COMMENT.id, i18nLabelKey: SessionNoteKind.COMMENT.i18nKey },
    { id: SessionNoteKind.SUGGESTION.id, i18nLabelKey: SessionNoteKind.SUGGESTION.i18nKey },
    { id: SessionNoteKind.BUG.id, i18nLabelKey: SessionNoteKind.BUG.i18nKey },
    { id: SessionNoteKind.QUESTION.id, i18nLabelKey: SessionNoteKind.QUESTION.i18nKey },
    { id: SessionNoteKind.POSITIVE.id, i18nLabelKey: SessionNoteKind.POSITIVE.i18nKey },
  ];

  readonly origin = EXPLORATORY_EXECUTION_PAGE_ORIGIN;
  readonly lastPositionDropZone = 'LAST_POSITION_DROP_ZONE';
  readonly draggedSessionNotesComponent = DraggedSessionNotesComponent;

  extendedCharter = false;

  constructor(
    public executionPageService: ExecutionPageService,
    private formBuilder: FormBuilder,
    private issuesService: IssuesService,
    private restService: RestService,
    private cdRef: ChangeDetectorRef,
  ) {
    this.componentData$ = executionPageService.componentData$.pipe(takeUntil(this.unsub$));

    this.formGroup = this.formBuilder.group({
      newNoteContent: this.formBuilder.control(''),
    });

    this._authenticated = new BehaviorSubject<boolean>(false);
    this.authenticated$ = this._authenticated;

    this.componentData$
      .pipe(
        takeUntil(this.unsub$),
        map((data) => data.execution.sessionNotes),
      )
      .subscribe((sessionNotes) => {
        this.allNotesBySession = sessionNotes.ids.map((noteId) => sessionNotes.entities[noteId]);
        if (this.filteredNotes) {
          this.setSelectedKinds(this.noteKindsList.selectedItems);
        } else {
          this.filteredNotes = this.allNotesBySession;
        }
      });
  }

  ngOnInit(): void {
    this.executionPageService.componentData$
      .pipe(
        take(1),
        filter((componentData) => componentData.projectData.bugTracker != null),
      )
      .subscribe((componentData) => {
        this.issuesService
          .loadModel(componentData.execution.id, 'EXECUTION_TYPE')
          .subscribe((issueModel) => {
            const isAuthenticated =
              issueModel.modelLoaded && issueModel.bugTrackerStatus === 'AUTHENTICATED';
            this._authenticated.next(isAuthenticated);
            if (isAuthenticated) {
              this.loadIssuesData(componentData.execution.id);
            }
          });
      });
  }

  loadIssuesData(executionId: number) {
    this.restService
      .post([`issues/execution/${executionId}/all-known-issues`])
      .pipe(
        take(1),
        tap((gridResponse: GridResponse) => {
          this.issues = gridResponse.dataRows;
          this.cdRef.markForCheck();
        }),
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  handleNoteKindChange($event: SessionNoteKindKeys) {
    this.newNoteKind = $event;
  }

  clearNewNote() {
    this.newNoteKind = ExecutionPageCharterComponent.DEFAULT_KIND_VALUE;
    this.formGroup.controls.newNoteContent.setValue('');
  }

  cancelNewNote() {
    this.executionPageService.cancelNewNote();
    this.clearNewNote();
  }

  addNewNote(noteOrder: number) {
    const noteContent = this.formGroup.controls.newNoteContent.value;
    this.executionPageService.createSessionNote(this.newNoteKind, noteContent, noteOrder);
    this.clearNewNote();
  }

  trackNotesFn(index: number, note: SessionNoteState) {
    return note.id;
  }

  collapseAllNotes() {
    this.executionPageService.collapseAllNotes();
  }

  expandAllNotes() {
    this.executionPageService.expandAllNotes();
  }

  hasWriteOrExecutePermission(componentData: ExecutionPageComponentData): boolean {
    return (
      componentData.permissions.canWrite ||
      componentData.projectData.permissions.CAMPAIGN_LIBRARY.includes(Permissions.EXECUTE)
    );
  }

  doMilestonesAllowModification(componentData: ExecutionPageComponentData): boolean {
    return componentData.milestonesAllowModification;
  }

  doesSprintStatusAllowModification(componentData: ExecutionPageComponentData): boolean {
    return componentData.execution.parentSprintStatus !== SprintStatus.CLOSED.id;
  }

  canMove(componentData: ExecutionPageComponentData): boolean {
    return (
      this.hasWriteOrExecutePermission(componentData) &&
      this.doMilestonesAllowModification(componentData) &&
      this.doesSprintStatusAllowModification(componentData) &&
      !this.isFilteringByKind()
    );
  }

  canEdit(componentData: ExecutionPageComponentData): boolean {
    return (
      this.hasWriteOrExecutePermission(componentData) &&
      this.doMilestonesAllowModification(componentData) &&
      this.sprintStatusAllowModification(componentData)
    );
  }

  canAddNote(componentData: ExecutionPageComponentData): boolean {
    return (
      this.canEdit(componentData) &&
      !this.isFilteringByKind() &&
      !this.isDragging(componentData) &&
      this.isSessionRunning(componentData)
    );
  }

  isSessionRunning(componentData: ExecutionPageComponentData): boolean {
    return componentData.execution.exploratoryExecutionRunningState === 'RUNNING';
  }

  isSessionPaused(componentData: ExecutionPageComponentData): boolean {
    return componentData.execution.exploratoryExecutionRunningState === 'PAUSED';
  }

  isSessionStopped(componentData: ExecutionPageComponentData): boolean {
    return componentData.execution.exploratoryExecutionRunningState === 'STOPPED';
  }

  getIssuesForNote(note: ExistingSessionNoteState) {
    return this.issues.filter((issue) =>
      issue.data[GridColumnId.executionSteps].includes(note.noteOrder),
    );
  }

  selectedKindsChanged(noteKinds: ListItem[]) {
    this.setSelectedKinds(noteKinds);
  }

  isExistingNote(note: SessionNoteState): note is ExistingSessionNoteState {
    return isExistingSessionNoteState(note);
  }

  handleAddButtonClicked() {
    const isFiltering = this.isFilteringByKind();
    this.clearNewNote();
    this.setSelectedKinds([]);

    asyncScheduler.schedule(() => {
      // Auto scroll if filter gets removed
      if (isFiltering) {
        this.newNoteContainer.nativeElement.scrollIntoView({
          behavior: 'auto',
        });
      }

      this.newNoteContentField.grabFocusWhenReady();
    }, 200);
  }

  isFilteringByKind(): boolean {
    return this.noteKindsList && this.noteKindsList.selectedItems.length > 0;
  }

  isDragging(componentData: ExecutionPageComponentData): boolean {
    return componentData.execution.sessionNotes.draggingNotes;
  }

  private setSelectedKinds(noteKinds: ListItem[]) {
    this.noteKindsList.selectedItems = noteKinds;

    if (noteKinds.length === 0) {
      this.filteredNotes = this.allNotesBySession;
    } else {
      this.filteredNotes = this.executionPageService.filterNotesByKinds(
        this.allNotesBySession,
        noteKinds,
      );
    }
  }

  dragStart($event: any): void {
    const data = $event.dragAndDropData.data as SessionNoteDndData;
    const draggedNotesIds = data.draggedNotes.map((step) => step.id);
    this.executionPageService.startDraggingNotes(draggedNotesIds);
  }

  dragOver($event: SqtmDragOverEvent): void {
    if ($event.dragAndDropData.origin === EXPLORATORY_EXECUTION_PAGE_ORIGIN) {
      this.executionPageService.componentData$
        .pipe(
          take(1),
          filter((componentData) => this.canMove(componentData)),
        )
        .subscribe((componentData) => {
          this.executionPageService.dragOverNote(
            $event.dndTarget.id as SessionNoteDropTargetId,
            componentData,
          );
        });
    }
  }

  dragCancel(): void {
    this.executionPageService.cancelSessionNoteDrag();
  }

  dragLeave(): void {
    // NOOP
  }

  drop(executionId: number): void {
    this.executionPageService.dropSessionNotes().subscribe(() => this.loadIssuesData(executionId));
  }

  getSessionNotesCount(componentData: ExecutionPageComponentData): number {
    return Object.values(componentData.execution.sessionNotes.entities).filter((sessionNote) =>
      isExistingSessionNoteState(sessionNote),
    ).length;
  }

  toggleCharter() {
    this.extendedCharter = !this.extendedCharter;
  }

  private sprintStatusAllowModification(componentData: ExecutionPageComponentData) {
    return componentData.execution.parentSprintStatus !== 'CLOSED';
  }
}
