import { SessionNoteState } from '../states/execution-state';
import { DragAndDropData } from 'sqtm-core';

export const EXPLORATORY_EXECUTION_PAGE_ORIGIN = 'EXPLORATORY_EXECUTION_PAGE_ORIGIN';

export class SessionNoteDndData extends DragAndDropData {
  constructor(public readonly draggedNotes: SessionNoteState[]) {
    super(EXPLORATORY_EXECUTION_PAGE_ORIGIN, draggedNotes);
  }
}
