import {
  ActionErrorDisplayService,
  attachmentEntityAdapter,
  AttachmentHolderType,
  AttachmentService,
  AttachmentState,
  CampaignPermissions,
  ClosedSprintLockService,
  CustomFieldValueService,
  DateFormatUtils,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  ExecutionModel,
  ExecutionService,
  ExecutionStepModel,
  ExecutionStepService,
  GenericEntityViewState,
  GridService,
  InterWindowCommunicationService,
  InterWindowMessages,
  LatestSessionEventModel,
  LocalPersistenceService,
  ProjectData,
  ReferentialDataService,
  RestService,
  SessionNoteModel,
  SprintStatus,
  StoreOptions,
  TargetOnUpload,
  UploadAttachmentEvent,
  UploadImage,
} from 'sqtm-core';

import { TranslateService } from '@ngx-translate/core';
import { catchError, concatMap, map, switchMap, take, tap, withLatestFrom } from 'rxjs/operators';
import {
  ExecutionState,
  executionStepAdapter,
  ExecutionStepState,
  ExistingSessionNoteState,
  getDefaultSessionNotesState,
  isExistingSessionNoteState,
  LatestSessionEventState,
  NewSessionNoteFormState,
  sessionNoteAdapter,
  SessionNotesState,
  SessionNoteState,
} from '../states/execution-state';
import { Observable, Subject, throwError } from 'rxjs';
import { ExecutionRunnerState } from '../execution-runner/state/execution-runner-state';
import { Update } from '@ngrx/entity';
import { executionRunnerLogger } from '../execution-runner/execution-runner.logger';
import { ExecutionPageState } from '../execution-page/states/execution-page-state';

const logger = executionRunnerLogger.compose('AbstractExecutionService');

export abstract class AbstractExecutionService extends EntityViewService<
  ExecutionState,
  'execution',
  CampaignPermissions
> {
  /*
   * This observable is required because we need to reload when execution windows communicate with us
   * and thus we need to manually detect change because Angular detect change seems to not occurs naturally when the windows loose focus...
   */
  loaded$: Observable<void>;
  private _loaded: Subject<void>;
  protected readonly stepsExtendedStateStorageKey = 'test-steps-extended-state';
  protected readonly notesExpandedStateStorageKey = 'session-notes-extended-state';

  protected constructor(
    protected restService: RestService,
    protected referentialDataService: ReferentialDataService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected customFieldValueService: CustomFieldValueService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
    protected customFieldHelper: EntityViewCustomFieldHelperService,
    protected executionService: ExecutionService,
    protected coverageTable: GridService,
    protected storeOptions: StoreOptions,
    protected interWindowCommunicationService: InterWindowCommunicationService,
    protected executionStepService: ExecutionStepService,
    protected localPersistenceService: LocalPersistenceService,
    protected closedSprintLockService: ClosedSprintLockService,
    protected actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper,
      storeOptions,
    );

    this._loaded = new Subject();
    this.loaded$ = this._loaded.asObservable();
  }

  load(executionId: number) {
    this.fetchExecutionData(executionId)
      .pipe(
        catchError((err) => {
          const dialogRef = this.actionErrorDisplayService.showActionError(err);
          return dialogRef.dialogClosed$.pipe(
            tap(() => window.close()),
            switchMap(() => throwError(() => err)),
          );
        }),
        withLatestFrom(this.state$),
        switchMap(([executionModel, state]) => this.loadExecutionModel(executionModel, state)),
        tap((state) => {
          this.closedSprintLockService.setSprintClosed(
            state.execution.parentSprintStatus === SprintStatus.CLOSED.id,
          );
        }),
      )
      .subscribe({
        next: (state) => {
          this.commit(state);
          this._loaded.next();
        },
        error: (err) => this.notifyEntityNotFound(err),
      });
  }

  abstract fetchExecutionData(executionId: number): Observable<ExecutionModel>;

  complete() {
    super.complete();
    this.coverageTable.complete();
  }

  private loadExecutionModel(
    executionModel: ExecutionModel,
    state,
  ): Observable<GenericEntityViewState<ExecutionState, 'execution'>> {
    return this.localPersistenceService.get(this.stepsExtendedStateStorageKey).pipe(
      map((extended: boolean) => {
        const executionSteps: ExecutionStepState[] = executionModel.executionStepViews.map(
          (executionStepModel: ExecutionStepModel) => {
            return {
              ...executionStepModel,
              attachmentList: this.initializeAttachmentListState(executionStepModel.attachmentList),
              customFieldValues: this.initializeCustomFieldValueState(
                executionStepModel.customFieldValues,
              ),
              extended: extended,
              lastExecutedOn: DateFormatUtils.createDateFromIsoString(
                executionStepModel.lastExecutedOn,
              ),
            };
          },
        );

        const executionStepsState = executionStepAdapter.setAll(
          executionSteps,
          executionStepAdapter.getInitialState(),
        );

        const customFieldValueState = this.initializeCustomFieldValueState(
          executionModel.customFieldValues,
        );

        const executionState: ExecutionState = {
          ...state.execution,
          ...executionModel,
          attachmentList: this.initializeAttachmentListState(executionModel.attachmentList),
          executionSteps: executionStepsState,
          customFieldValues: customFieldValueState,
          extendedPrerequisite: extended,
          scrollTop: 0,
          sessionNotes: this.getSessionNotesEntityState(executionModel),
          latestExploratoryExecutionEvent: this.transformLatestSessionEvent(
            executionModel.latestExploratoryExecutionEvent,
          ),
          lastExecutedOn: DateFormatUtils.createDateFromIsoString(executionModel.lastExecutedOn),
        };

        this.coverageTable.loadInitialData(
          executionState.coverages,
          executionState.coverages.length,
          'requirementVersionId',
        );

        return this.updateEntity(executionState, state);
      }),
    );
  }

  private getSessionNotesEntityState(executionModel: ExecutionModel): SessionNotesState | null {
    if (executionModel.sessionNotes == null) {
      return null;
    }

    const allExpanded =
      this.localPersistenceService.getSync<boolean>(this.notesExpandedStateStorageKey) ?? true;

    const sessionNotesState: SessionNoteState[] = executionModel.sessionNotes.map((sessionNote) =>
      this.transformSessionNoteModelIntoState(sessionNote, allExpanded),
    );

    const newNoteForm = this.buildNewNoteForm(sessionNotesState.length);

    sessionNotesState.push(newNoteForm);

    return getDefaultSessionNotesState(
      sessionNoteAdapter.setAll(sessionNotesState, sessionNoteAdapter.getInitialState()),
    );
  }

  protected buildNewNoteForm(displayOrder: number): NewSessionNoteFormState {
    return {
      id: -1,
      showPlaceHolder: false,
      noteContent: '',
      noteKind: 'COMMENT',
      discriminator: 'new-note-form',
      expanded: true,
      displayOrder,
    };
  }

  addSimplePermissions(projectData: ProjectData): CampaignPermissions {
    return new CampaignPermissions(projectData);
  }

  updateStepCustomFieldValue(executionStepId: number, cfvId: number, value: string | string[]) {
    return this.store.state$.pipe(
      take(1),
      concatMap((state) => this.writeStepCustomFieldValue(executionStepId, cfvId, value, state)),
      withLatestFrom(this.store.state$),
      map(([, initialState]) =>
        this.updateStateStepCustomFieldValue(executionStepId, cfvId, value, initialState),
      ),
      tap((state) => this.commit(state)),
      tap(() => {
        this.interWindowCommunicationService.sendMessage(
          new InterWindowMessages('EXECUTION-STEP-CHANGED'),
        );
      }),
    );
  }

  writeStepCustomFieldValue(
    stepId: number,
    cufValueId: number,
    value: string | string[],
    initialState: Readonly<ExecutionRunnerState>,
  ): Observable<string | string[]> {
    const executionStep = initialState.execution.executionSteps.entities[
      stepId
    ] as ExecutionStepState;
    const customFieldValue = executionStep.customFieldValues.entities[cufValueId];
    return this.customFieldHelper.writeCustomFieldValue(customFieldValue, value);
  }

  private updateStateStepCustomFieldValue(
    stepId: number,
    cufValueId: number,
    value: string | string[],
    initialState: Readonly<ExecutionRunnerState>,
  ): ExecutionRunnerState {
    const executionStep = initialState.execution.executionSteps.entities[
      stepId
    ] as ExecutionStepState;
    const customFieldValueState = this.customFieldHelper.updateCustomFieldValue(
      cufValueId,
      value,
      executionStep.customFieldValues,
    );
    const changes: Update<ExecutionStepState> = {
      id: executionStep.id,
      changes: { customFieldValues: customFieldValueState },
    };
    const executionSteps = executionStepAdapter.updateOne(
      changes,
      initialState.execution.executionSteps,
    );
    const execution: ExecutionState = { ...initialState.execution, executionSteps };
    return { ...initialState, execution };
  }

  updateExecutionStepComment(executionStepId: number, comment: string) {
    return this.executionStepService.changeComment(executionStepId, comment).pipe(
      withLatestFrom(this.state$),
      map(([, state]: [void, ExecutionRunnerState]) => {
        const executionSteps = executionStepAdapter.updateOne(
          {
            id: executionStepId,
            changes: { comment },
          },
          state.execution.executionSteps,
        );
        return { ...state, execution: { ...state.execution, executionSteps } };
      }),
      tap((state) => this.commit(state)),
      tap(() => {
        this.interWindowCommunicationService.sendMessage(
          new InterWindowMessages('EXECUTION-STEP-CHANGED'),
        );
      }),
    );
  }

  addAttachmentsToStep(files: File[], executionStepId: number, attachmentListId: number) {
    if (logger.isDebugEnabled()) {
      logger.debug(`Adding files to step ${executionStepId} : `, [files]);
    }

    return this.state$.pipe(
      take(1),
      switchMap((state: ExecutionRunnerState) => {
        const entity = this.getEntity(state);
        return this.attachmentHelper.addAttachments(
          files,
          attachmentListId,
          entity.id,
          state.type,
          AttachmentHolderType.EXECUTION_STEP,
        );
      }),
      withLatestFrom(this.store.state$),
      map(([event, state]: [UploadAttachmentEvent, ExecutionRunnerState]) => {
        this.commit(this.mapExecutionStepUploadEventToState(executionStepId, state, event));
      }),
      tap(() => {
        this.interWindowCommunicationService.sendMessage(
          new InterWindowMessages('EXECUTION-STEP-CHANGED'),
        );
      }),
    );
  }

  mapExecutionStepUploadEventToState(
    stepId: number,
    state: ExecutionRunnerState,
    event: UploadAttachmentEvent,
  ): ExecutionRunnerState {
    const actionStep = state.execution.executionSteps.entities[stepId] as ExecutionStepState;
    const attachmentState = this.attachmentHelper.mapUploadEventToState(
      actionStep.attachmentList.attachments,
      event,
    );
    return this.updateTestStepAttachmentState(state, attachmentState, stepId);
  }

  mapSessionNoteUploadEventToState(
    noteId: number,
    state: ExecutionPageState,
    event: UploadAttachmentEvent,
  ): ExecutionPageState {
    const sessionNote = state.execution.sessionNotes.entities[noteId];
    if (isExistingSessionNoteState(sessionNote)) {
      const attachmentState = this.attachmentHelper.mapUploadEventToState(
        sessionNote.attachmentList.attachments,
        event,
      );
      return this.updateSessionNoteAttachmentState(state, attachmentState, noteId);
    } else {
      throw new Error('Impossible to upload files in a non-existing note.');
    }
  }

  protected updateSessionNoteAttachmentState(
    state: ExecutionPageState,
    attachmentState: AttachmentState,
    noteId: number,
  ): ExecutionPageState {
    const sessionNote = { ...state.execution.sessionNotes.entities[noteId] };
    if (isExistingSessionNoteState(sessionNote)) {
      const attachmentList = { ...sessionNote.attachmentList };
      attachmentList.attachments = attachmentState;
      const changes: Update<SessionNoteState> = { id: noteId, changes: { attachmentList } };
      const sessionNotes = sessionNoteAdapter.updateOne(changes, state.execution.sessionNotes);
      return { ...state, execution: { ...state.execution, sessionNotes } };
    } else {
      throw new Error('Impossible to update a non-existing note.');
    }
  }

  protected uploadImageFromEditor(
    file: File,
    targetOnUpload?: TargetOnUpload,
  ): Observable<UploadImage> {
    if (targetOnUpload == null) {
      return super.uploadImageFromEditor(file, targetOnUpload);
    } else {
      return this.state$.pipe(
        take(1),
        switchMap((state: ExecutionRunnerState) => {
          const entity = this.getEntity(state);
          return this.attachmentHelper.addImageAttachment(
            file,
            targetOnUpload.attachmentListId,
            entity.id,
            state.type,
            targetOnUpload.holderType,
          );
        }),
        withLatestFrom(this.store.state$),
        map(([event, state]: [UploadAttachmentEvent, ExecutionRunnerState]) => {
          if (state.execution.executionMode === 'EXPLORATORY') {
            this.commit(
              this.mapSessionNoteUploadEventToState(targetOnUpload.entityId, state, event),
            );
          } else {
            this.commit(
              this.mapExecutionStepUploadEventToState(targetOnUpload.entityId, state, event),
            );
          }
          return this.attachmentHelper.mapUploadEventToImageUpload(
            event,
            targetOnUpload.attachmentListId,
          );
        }),
      );
    }
  }

  deleteStepAttachments(
    attachmentIds: string[],
    executionStepId: number,
    attachmentListId: number,
  ) {
    const idsAsNumber = attachmentIds.map((id) => parseInt(id, 10));
    return this.state$.pipe(
      take(1),
      switchMap((state: ExecutionRunnerState) => {
        const entity = this.getEntity(state);
        return this.attachmentHelper.eraseAttachments(
          idsAsNumber,
          attachmentListId,
          entity.id,
          state.type,
          AttachmentHolderType.EXECUTION_STEP,
        );
      }),
      withLatestFrom(this.store.state$),
      map(([, state]) => {
        if (logger.isDebugEnabled()) {
          logger.debug(
            `Removing attachments ${JSON.stringify(attachmentIds)} from ${executionStepId}. Initial state :`,
            [state],
          );
        }
        const executionStep = {
          ...state.execution.executionSteps.entities[executionStepId],
        } as ExecutionStepState;
        const attachmentState = attachmentEntityAdapter.removeMany(
          attachmentIds,
          executionStep.attachmentList.attachments,
        );
        const nextState = this.updateTestStepAttachmentState(
          state,
          attachmentState,
          executionStepId,
        );
        if (logger.isDebugEnabled()) {
          logger.debug(
            `Removed attachments ${JSON.stringify(attachmentIds)} from ${executionStepId}. Next state :`,
            [nextState],
          );
        }
        return nextState;
      }),
      tap((state) => this.commit(state)),
      tap(() => {
        this.interWindowCommunicationService.sendMessage(
          new InterWindowMessages('EXECUTION-STEP-CHANGED'),
        );
      }),
    );
  }

  markAttachmentStepToDelete(stepId: number, attachmentIds: string[]) {
    this.changeAttachmentStepPendingDelete(stepId, attachmentIds, true);
  }

  cancelAttachmentStepToDelete(stepId: number, attachmentIds: string[]) {
    this.changeAttachmentStepPendingDelete(stepId, attachmentIds, false);
  }

  removeStepRejectedAttachments(attachmentIds: string[], stepId: number) {
    this.state$
      .pipe(
        take(1),
        map((state: ExecutionRunnerState) => {
          const executionStep = {
            ...state.execution.executionSteps.entities[stepId],
          } as ExecutionStepState;
          const attachmentState = attachmentEntityAdapter.removeMany(
            attachmentIds,
            executionStep.attachmentList.attachments,
          );
          return this.updateTestStepAttachmentState(state, attachmentState, stepId);
        }),
      )
      .subscribe((state) => this.commit(state));
  }

  protected transformSessionNoteModelIntoState(
    model: SessionNoteModel,
    expanded = true,
  ): ExistingSessionNoteState {
    const attachmentState = this.attachmentHelper.initializeAttachmentState(
      model.attachmentList.attachments,
    );
    const attachmentList = { id: model.attachmentList.id, attachments: attachmentState };

    return {
      noteContent: model.content,
      noteKind: model.kind,
      id: model.noteId,
      noteOrder: model.noteOrder,
      displayOrder: model.noteOrder,
      createdBy: model.createdBy,
      createdOn: DateFormatUtils.createDateFromIsoString(model.createdOn),
      lastModifiedBy: model.lastModifiedBy,
      lastModifiedOn: DateFormatUtils.createDateFromIsoString(model.lastModifiedOn),
      attachmentList: attachmentList,
      discriminator: 'existing-note',
      showPlaceHolder: false,
      expanded,
    };
  }

  private changeAttachmentStepPendingDelete(
    stepId: number,
    attachmentIds: string[],
    pendingDelete: boolean,
  ) {
    this.state$
      .pipe(
        take(1),
        map((state) => {
          const testStep = {
            ...state.execution.executionSteps.entities[stepId],
          } as ExecutionStepState;
          const attachmentState = this.changeAttachmentDeleteStatus(
            testStep.attachmentList.attachments,
            attachmentIds,
            pendingDelete,
          );
          return this.updateTestStepAttachmentState(state, attachmentState, stepId);
        }),
      )
      .subscribe((state) => this.commit(state));
  }

  protected updateTestStepAttachmentState(
    state: ExecutionRunnerState,
    attachmentState: AttachmentState,
    stepId: number,
  ): ExecutionRunnerState {
    const executionStep = {
      ...state.execution.executionSteps.entities[stepId],
    } as ExecutionStepState;
    const attachmentList = { ...executionStep.attachmentList };
    attachmentList.attachments = attachmentState;
    const changes: Update<ExecutionStepState> = {
      id: executionStep.id,
      changes: { attachmentList },
    };
    const executionSteps = executionStepAdapter.updateOne(changes, state.execution.executionSteps);
    return { ...state, execution: { ...state.execution, executionSteps } };
  }

  abstract getInitialState();

  private transformLatestSessionEvent(
    latestExploratoryExecutionEvent: LatestSessionEventModel,
  ): LatestSessionEventState {
    return latestExploratoryExecutionEvent == null
      ? null
      : {
          timestamp: new Date(latestExploratoryExecutionEvent.timestamp),
          timeElapsed: latestExploratoryExecutionEvent.timeElapsed,
        };
  }
}
