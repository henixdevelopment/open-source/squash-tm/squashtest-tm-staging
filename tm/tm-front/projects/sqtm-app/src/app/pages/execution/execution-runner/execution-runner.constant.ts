export const EXECUTION_RUNNER_PROLOGUE_URL = 'prologue';
export const EXECUTION_DIALOG_RUNNER_URL = 'execution-runner';
export const EXECUTION_DIALOG_RUNNER_WIDTH = 1200;
export const EXECUTION_DIALOG_RUNNER_HEIGHT = 700;
