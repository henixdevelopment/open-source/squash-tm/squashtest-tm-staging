import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ExecutionRunnerNavigationService } from '../../services/execution-runner-navigation.service';

@Component({
  selector: 'sqtm-app-start-execution-button',
  templateUrl: './start-execution-button.component.html',
  styleUrls: ['./start-execution-button.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StartExecutionButtonComponent {
  @Input()
  executionId: number;

  constructor(private navigationService: ExecutionRunnerNavigationService) {}

  startExecution() {
    this.navigationService.startExecution(this.executionId);
  }
}
