import {
  AttachmentListState,
  DenormalizedCustomFieldValueModel,
  DenormalizedEnvironmentTag,
  DenormalizedEnvironmentVariable,
  ExecutionKind,
  ExecutionStatusKeys,
  ExploratoryExecutionRunningState,
  ExploratorySessionOverviewInfo,
  Milestone,
  RequirementVersionCoverage,
  SessionNoteKindKeys,
  SprintStatusKeys,
  SqtmEntityState,
  TestAutomationServerKind,
  TestCaseImportanceKeys,
  TestCaseStatusKeys,
} from 'sqtm-core';

import { createEntityAdapter, EntityState } from '@ngrx/entity';

export interface ExecutionState extends SqtmEntityState {
  id: number;
  name: string;
  executionOrder: number;
  executionSteps: EntityState<ExecutionStepState>;
  currentStepIndex: number;
  prerequisite: string;
  testCaseId?: number;
  tcNatLabel: string;
  tcNatIconName: string;
  tcTypeLabel: string;
  tcTypeIconName: string;
  tcStatus: TestCaseStatusKeys;
  tcImportance: TestCaseImportanceKeys;
  tcDescription: string;
  comment: string;
  datasetLabel?: string;
  // no need to create a state for that, it's just a simple array for denormalized cfv display
  denormalizedCustomFieldValues: DenormalizedCustomFieldValueModel[];
  coverages: RequirementVersionCoverage[];
  executionMode: string;
  lastExecutedOn: Date;
  lastExecutedBy: string;
  executionStatus: ExecutionStatusKeys;
  testAutomationServerKind: TestAutomationServerKind;
  automatedExecutionResultUrl: string;
  automatedExecutionResultSummary: string;
  automatedJobUrl: string;
  automatedExecutionDuration: number;
  nbIssues: number;
  iterationId?: number;
  kind: ExecutionKind;
  milestones: Milestone[];
  extendedPrerequisite: boolean;
  scrollTop: number;
  testPlanItemId: number;
  executionsCount: number;
  denormalizedEnvironmentVariables?: DenormalizedEnvironmentVariable[];
  denormalizedEnvironmentTags?: DenormalizedEnvironmentTag;
  exploratorySessionOverviewInfo: ExploratorySessionOverviewInfo;
  exploratoryExecutionRunningState: ExploratoryExecutionRunningState;
  sessionNotes: SessionNotesState;
  latestExploratoryExecutionEvent: LatestSessionEventState;
  lastModifiedOn: Date;
  lastModifiedBy: string;
  reviewed: boolean;
  taskDivision: string;
  parentSprintStatus: SprintStatusKeys | null;
  extenderId: number;
}

export interface LatestSessionEventState {
  timestamp: Date;
  timeElapsed: number;
}

export interface ExecutionStepState extends SqtmEntityState {
  order: number;
  executionStatus: ExecutionStatusKeys;
  action: string;
  expectedResult: string;
  comment: string;
  denormalizedCustomFieldValues: DenormalizedCustomFieldValueModel[];
  lastExecutedOn: Date;
  lastExecutedBy: string;
  // Does this step is shown as full view or reduced view
  extended: boolean;
  testStepId?: number;
}

export interface ExistingSessionNoteState {
  discriminator: 'existing-note';
  id: number;
  noteKind: SessionNoteKindKeys;
  noteContent: string;
  noteOrder: number;
  displayOrder: number;
  createdBy: string;
  createdOn: Date;
  lastModifiedBy: string;
  lastModifiedOn: Date;
  attachmentList: AttachmentListState;
  showPlaceHolder: boolean;
  expanded: boolean;
}

export interface NewSessionNoteFormState {
  discriminator: 'new-note-form';
  id: -1;
  noteKind: SessionNoteKindKeys;
  noteContent: string;
  displayOrder: number;
  showPlaceHolder: boolean;
  expanded: boolean;
}

export type SessionNoteState = ExistingSessionNoteState | NewSessionNoteFormState;

export function isExistingSessionNoteState(
  sessionNoteState: SessionNoteState,
): sessionNoteState is ExistingSessionNoteState {
  return sessionNoteState.discriminator === 'existing-note';
}

export const executionStepAdapter = createEntityAdapter<ExecutionStepState>({
  sortComparer: (a, b) => a.order - b.order,
});

export type SessionNoteDropTargetId = 'LAST_POSITION_DROP_ZONE' | number;

export interface SessionNotesState extends EntityState<SessionNoteState> {
  selectedNoteIds: number[];
  initialStepOrder: number[];
  currentDndTargetId: SessionNoteDropTargetId;
  draggingNotes: boolean;
}

export function getDefaultSessionNotesState(
  entityState: EntityState<SessionNoteState>,
): SessionNotesState {
  return {
    ...entityState,
    selectedNoteIds: [],
    initialStepOrder: [],
    currentDndTargetId: null,
    draggingNotes: false,
  };
}

export const sessionNoteAdapter = createEntityAdapter<SessionNoteState>({
  sortComparer: (a, b) => a.displayOrder - b.displayOrder,
});
