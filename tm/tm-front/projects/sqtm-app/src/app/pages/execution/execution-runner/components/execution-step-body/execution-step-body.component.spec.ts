import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ExecutionStepBodyComponent } from './execution-step-body.component';
import {
  CampaignPermissions,
  DialogService,
  InputType,
  RestService,
  SafeRichContentPipe,
} from 'sqtm-core';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ExecutionRunnerService } from '../../services/execution-runner.service';
import { OnPushComponentTester } from '../../../../../utils/testing-utils/on-push-component-tester';
import { AppTestingUtilsModule } from '../../../../../utils/testing-utils/app-testing-utils.module';
import { TranslateModule } from '@ngx-translate/core';
import { ExecutionState, ExecutionStepState } from '../../../states/execution-state';
import {
  mockDialogService,
  mockGridService,
  mockRestService,
} from '../../../../../utils/testing-utils/mocks.service';
import { EXEC_STEP_ISSUE_TABLE } from '../execution-step-issues/execution-step-issues.component';
import { EMPTY } from 'rxjs';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';

const executionRunnerServiceMock = jasmine.createSpyObj(['changeComment']);
executionRunnerServiceMock.componentData$ = EMPTY;
const simpleDnzCuf = {
  label: 'Mission',
  inputType: InputType.DROPDOWN_LIST,
  denormalizedFieldHolderId: 1,
  fieldType: 'SSF',
  value: 'STS-135',
  id: 1,
};

class ExecutionStepBodyTester extends OnPushComponentTester<ExecutionStepBodyComponent> {
  getActionContent() {
    return this.getElementByDataTestComponentId('execution-step-action').nativeElement.innerHTML;
  }

  getExpectedResultContent() {
    return this.getElementByDataTestComponentId('execution-step-expected-result').nativeElement
      .innerHTML;
  }

  getInformationPanel() {
    return this.getElementByDataTestComponentId('information-panel');
  }
}

describe('ExecutionStepBodyComponent', () => {
  let tester: ExecutionStepBodyTester;
  let fixture: ComponentFixture<ExecutionStepBodyComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        NzDropDownModule,
        NzCollapseModule,
        NoopAnimationsModule,
        AppTestingUtilsModule,
        TranslateModule.forRoot(),
      ],
      declarations: [SafeRichContentPipe, ExecutionStepBodyComponent],
      providers: [
        {
          provide: ExecutionRunnerService,
          useValue: executionRunnerServiceMock,
        },
        {
          provide: DialogService,
          useValue: mockDialogService(),
        },
        {
          provide: RestService,
          useValue: mockRestService(),
        },
        {
          provide: EXEC_STEP_ISSUE_TABLE,
          useValue: mockGridService(),
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionStepBodyComponent);
    tester = new ExecutionStepBodyTester(fixture);
    const dummyStep = {
      action: '<p>action dummy</p>',
      expectedResult: '<p>action dummy</p>',
      attachmentList: { id: 1, attachments: { ids: [], entities: [] } },
    } as unknown as ExecutionStepState;
    tester.componentInstance.execution = {
      executionSteps: { ids: [1, 2, 3], entities: { 1: dummyStep, 2: dummyStep, 3: dummyStep } },
      currentStepIndex: 1,
      kind: 'STANDARD',
    } as unknown as ExecutionState;
    tester.componentInstance.permissions = {
      canExecute: true,
    } as CampaignPermissions;
    tester.componentInstance.customFieldBindingData = [];
    fixture.detectChanges();
  });

  it('should show action and result as rich text ', () => {
    expect(tester.componentInstance).toBeTruthy();
    expect(tester.getActionContent()).toEqual('<p>action dummy</p>');
    expect(tester.getExpectedResultContent()).toEqual('<p>action dummy</p>');
  });

  it('should show information panel according to data', () => {
    expect(tester.componentInstance).toBeTruthy();
    expect(tester.getInformationPanel()).toBeFalsy();
    tester.componentInstance.execution.datasetLabel = 'hello';
    tester.detectChanges();
    expect(tester.getInformationPanel()).toBeTruthy();
    tester.componentInstance.execution.datasetLabel = null;
    tester.componentInstance.execution.executionSteps.entities[1].denormalizedCustomFieldValues = [
      simpleDnzCuf,
    ];
    tester.detectChanges();
    expect(tester.getInformationPanel()).toBeTruthy();
    tester.componentInstance.execution.executionSteps.entities[1].denormalizedCustomFieldValues =
      [];
    tester.detectChanges();
    expect(tester.getInformationPanel()).toBeFalsy();
  });
});
