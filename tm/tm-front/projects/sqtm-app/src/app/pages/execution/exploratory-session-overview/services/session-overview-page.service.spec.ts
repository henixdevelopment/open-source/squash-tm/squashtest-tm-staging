import { TestBed } from '@angular/core/testing';

import { SessionOverviewPageService } from './session-overview-page.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppTestingUtilsModule } from '../../../../utils/testing-utils/app-testing-utils.module';
import { TranslateModule } from '@ngx-translate/core';

describe('SessionOverviewPageService', () => {
  let service: SessionOverviewPageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: SessionOverviewPageService,
          useClass: SessionOverviewPageService,
        },
      ],
    });
    service = TestBed.inject(SessionOverviewPageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
