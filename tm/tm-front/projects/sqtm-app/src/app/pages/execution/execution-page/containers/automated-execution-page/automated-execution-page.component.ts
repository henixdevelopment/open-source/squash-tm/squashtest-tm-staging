import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
} from '@angular/core';
import { ExecutionPageService } from '../../services/execution-page.service';
import { TranslateService } from '@ngx-translate/core';
import { APP_BASE_HREF, DatePipe } from '@angular/common';
import {
  ActionErrorDisplayService,
  CapsuleInformationData,
  DenormalizedEnvironmentTag,
  DenormalizedEnvironmentVariable,
  DialogService,
  DurationFormatUtils,
  ExecutionStatus,
  getSupportedBrowserLang,
  InterWindowCommunicationService,
  ReferentialDataService,
  RestService,
  TestAutomationServerKind,
} from 'sqtm-core';
import { AbstractExecutionPageComponent } from '../abstract-execution-page.component';
import { ExecutionRunnerOpenerService } from '../../../execution-runner/services/execution-runner-opener.service';
import { ExecutionState } from '../../../states/execution-state';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sqtm-app-automated-execution-page',
  templateUrl: './automated-execution-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AutomatedExecutionPageComponent
  extends AbstractExecutionPageComponent
  implements OnInit
{
  $isPremium = toSignal(this.referentialDataService.isPremiumPluginInstalled$);

  constructor(
    executionPageService: ExecutionPageService,
    translateService: TranslateService,
    datePipe: DatePipe,
    interWindowCommunicationService: InterWindowCommunicationService,
    @Inject(APP_BASE_HREF) baseUrl: string,
    cdRef: ChangeDetectorRef,
    dialogService: DialogService,
    restService: RestService,
    executionRunnerOpenerService: ExecutionRunnerOpenerService,
    actionErrorDisplayService: ActionErrorDisplayService,
    private referentialDataService: ReferentialDataService,
  ) {
    super(
      executionPageService,
      translateService,
      datePipe,
      interWindowCommunicationService,
      baseUrl,
      cdRef,
      dialogService,
      restService,
      executionRunnerOpenerService,
      actionErrorDisplayService,
    );
  }

  ngOnInit() {
    super.ngOnInit();
  }

  getExecutionStatus(statusKey: string): CapsuleInformationData {
    const executionStatus = ExecutionStatus[statusKey];
    return {
      id: executionStatus.id,
      color: executionStatus.color,
      icon: 'sqtm-core-campaign:exec_status',
      labelI18nKey: executionStatus.i18nKey,
      titleI18nKey: 'sqtm-core.entity.execution.status.automated.long-label',
    };
  }

  getExecutionDurationCapsuleData(duration: number): CapsuleInformationData {
    return {
      id: 'executionDuration',
      color: 'var(--container-border-color)',
      labelI18nKey: DurationFormatUtils.shortDuration(
        getSupportedBrowserLang(this.translateService),
        duration,
      ),
      titleI18nKey:
        this.translateService.instant('sqtm-core.entity.execution-plan.execution-duration.label') +
        ' : ' +
        DurationFormatUtils.longDuration(getSupportedBrowserLang(this.translateService), duration),
    };
  }

  getExecutionResultUrl(resultUrl: string) {
    return {
      id: 'executionResultUrl',
      color: 'var(--container-border-color)',
      labelI18nKey: resultUrl,
      titleI18nKey: 'sqtm-core.entity.execution.automated.result-url',
    };
  }

  getAutomatedJobUrl(automatedJobUrl: string) {
    return {
      id: 'automatedJobUrl',
      color: 'var(--container-border-color)',
      labelI18nKey: automatedJobUrl,
      titleI18nKey: 'sqtm-core.entity.execution.automated.job-url',
    };
  }

  getDenormalizedEnvironmentVariable(environmentVariable: DenormalizedEnvironmentVariable) {
    return {
      label: environmentVariable.namedValue,
    };
  }

  getDenormalizedEnvironmentTagValues(tags: DenormalizedEnvironmentTag) {
    return {
      icon: 'sqtm-core-campaign:environments',
      label: tags.value,
      titleI18nKey: 'sqtm-core.entity.server.environment.tags.label',
    };
  }

  isSquashOrchestratorServerKind(execution: ExecutionState): boolean {
    return execution.testAutomationServerKind === TestAutomationServerKind.squashOrchestrator;
  }

  isJenkinsServerKind(execution: ExecutionState): boolean {
    return execution.testAutomationServerKind === TestAutomationServerKind.jenkins;
  }

  shouldDisplayFailureDetailSection(executionState: ExecutionState): boolean {
    return (
      this.$isPremium() &&
      executionState.extenderId &&
      executionState.executionStatus == 'FAILURE' &&
      !this.isJenkinsServerKind(executionState)
    );
  }
}
