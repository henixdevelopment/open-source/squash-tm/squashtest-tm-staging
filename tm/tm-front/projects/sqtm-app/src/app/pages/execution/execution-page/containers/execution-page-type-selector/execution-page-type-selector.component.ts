import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable } from 'rxjs';
import { ExecutionPageComponentData } from '../abstract-execution-page.component';
import { ExecutionPageService } from '../../services/execution-page.service';

@Component({
  selector: 'sqtm-app-execution-page-type-selector',
  templateUrl: './execution-page-type-selector.component.html',
  styleUrls: ['./execution-page-type-selector.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExecutionPageTypeSelectorComponent {
  componentData$: Observable<ExecutionPageComponentData>;

  constructor(private executionPageService: ExecutionPageService) {
    this.componentData$ = this.executionPageService.componentData$;
  }

  back() {
    history.back();
  }
}
