import {
  EntityViewState,
  Milestone,
  provideInitialViewState,
  SimpleUser,
  SprintStatusKeys,
  SqtmEntityState,
} from 'sqtm-core';

export interface SessionOverviewPageState extends SqtmEntityState {
  name: string;
  charter: string;
  sessionDuration: number;
  executionStatus: string;
  sessionStatus: string;
  lastModifiedOn: Date;
  lastModifiedBy: string;
  createdOn: Date;
  createdBy: string;
  dueDate: Date;
  assignedUser: string;
  assignableUsers: SimpleUser[];
  nbIssues: number;
  nbExecutions: number;
  nbNotes: number;
  inferredSessionReviewStatus: string;
  comments: string;
  milestones: Milestone[];
  sprintStatus: SprintStatusKeys;
  tclnId: number;
}

export interface SessionOverviewPageViewState
  extends EntityViewState<SessionOverviewPageState, 'exploratorySessionOverview'> {
  exploratorySessionOverview: SessionOverviewPageState;
}

export function provideInitialSessionOverviewPageState(): Readonly<SessionOverviewPageViewState> {
  return provideInitialViewState<SessionOverviewPageState, 'exploratorySessionOverview'>(
    'exploratorySessionOverview',
  );
}
