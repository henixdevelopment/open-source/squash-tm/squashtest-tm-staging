import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
  InterWindowCommunicationService,
  SquashPlatformNavigationService,
} from 'sqtm-core';
import {
  InterWindowOpenWindowMessage,
  WindowOpenerService,
} from '../../../../services/window-opener.service';

@Component({
  selector: 'sqtm-app-coverage-external-link',
  template: `
    @if (columnDisplay && row) {
      <div class="sqtm-grid-cell-txt-renderer flex-column full-height">
        <a
          [class.disabled-row]="row.disabled"
          class="sqtm-grid-cell-txt-renderer"
          nz-tooltip
          [nzTooltipTitle]=""
          [sqtmCoreLabelTooltip]="row.data[columnDisplay.id]"
          [href]="generateHref()"
          (click)="navigateToExternalRequirement($event)"
          >{{ row.data[columnDisplay.id] }}</a
        >
      </div>
    }
  `,
  styleUrls: ['./coverage-external-link.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CoverageExternalLinkComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    private interWindowCommunicationService: InterWindowCommunicationService,
    private windowOpenerService: WindowOpenerService,
    private platformNavigationService: SquashPlatformNavigationService,
  ) {
    super(grid, cdRef);
  }

  generateHref() {
    return this.platformNavigationService.generateHref(this.generateLocalLink());
  }

  private generateLocalLink() {
    return `requirement-workspace/requirement-version/detail/${this.row.data[GridColumnId.requirementVersionId]}`;
  }

  navigateToExternalRequirement($event: MouseEvent) {
    if ($event.button === 0) {
      $event.preventDefault();
      $event.stopPropagation();
      if (this.interWindowCommunicationService.isAbleToSendMessagesToOpener()) {
        const message = new InterWindowOpenWindowMessage(this.generateLocalLink());
        this.interWindowCommunicationService.sendMessage(message);
      } else {
        this.platformNavigationService.navigateExternal(this.generateLocalLink(), null, true);
      }
    }
  }
}

export function coverageExternalLinkColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(CoverageExternalLinkComponent);
}
