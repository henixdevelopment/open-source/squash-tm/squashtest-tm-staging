import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import {
  CampaignPermissions,
  centredTextColumn,
  convertSqtmLiterals,
  DataRow,
  dateTimeColumn,
  DialogService,
  executionModeColumn,
  executionStatusColumn,
  Extendable,
  Fixed,
  grid,
  GridColumnId,
  GridDefinition,
  GridService,
  gridServiceFactory,
  indexColumn,
  numericColumn,
  ProjectDataMap,
  ReferentialDataService,
  RestService,
  sortDate,
  SquashTmDataRowType,
  StyleDefinitionBuilder,
  testCaseImportanceColumn,
  textColumn,
  withLinkColumn,
} from 'sqtm-core';
import { Observable, Subject } from 'rxjs';
import {
  concatMap,
  filter,
  finalize,
  map,
  take,
  takeUntil,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import { ExecutionPageComponentData } from '../../abstract-execution-page.component';
import { ExecutionPageService } from '../../../services/execution-page.service';
import { SuccessRateComponent } from '../../../../../campaign-workspace/iteration-view/components/cell-renderers/success-rate/success-rate.component';
import { DatePipe } from '@angular/common';
import { EXECUTION_HISTORY_TABLE, EXECUTION_HISTORY_TABLE_CONF } from '../../../execution.constant';
import { deleteExecutionPageHistoryColumn } from '../../../components/cell-renderers/delete-execution-page-history/delete-execution-page-history.component';
import { ExecutionPageComponent } from '../../execution-page/execution-page.component';
import { ExecutionState } from '../../../../states/execution-state';

@Component({
  selector: 'sqtm-app-execution-page-history',
  templateUrl: './execution-page-history.component.html',
  styleUrls: ['./execution-page-history.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    DatePipe,
    {
      provide: EXECUTION_HISTORY_TABLE_CONF,
      useFactory: executionHistoryTableDefinition,
    },
    {
      provide: EXECUTION_HISTORY_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, EXECUTION_HISTORY_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: EXECUTION_HISTORY_TABLE,
    },
  ],
})
export class ExecutionPageHistoryComponent implements OnInit, OnDestroy {
  componentData$: Observable<ExecutionPageComponentData>;
  unsub$ = new Subject<void>();
  canDeleteExecution$: Observable<boolean>;

  constructor(
    private gridService: GridService,
    private restService: RestService,
    private dialogService: DialogService,
    private executionPageService: ExecutionPageService,
    private executionPageComponent: ExecutionPageComponent,
  ) {
    this.initializeDeleteColumnVisibility();
  }

  ngOnInit(): void {
    this.componentData$ = this.executionPageService.componentData$;
    this.componentData$.pipe(take(1)).subscribe((componentData) => {
      const iterationId = componentData.execution.iterationId;
      const testPlanItemId = componentData.execution.testPlanItemId;
      this.fetchExecutionHistory(iterationId, testPlanItemId);
    });
    this.canDeleteExecution$ = this.gridService.selectedRows$.pipe(
      takeUntil(this.unsub$),
      map((rows) => this.canDeleteExecution(rows)),
    );
  }

  private fetchExecutionHistory(iterationId: number, testPlanItemId: number) {
    if (iterationId != null) {
      this.gridService.setServerUrl([
        `iteration/${iterationId}/test-plan/${testPlanItemId}/executions`,
      ]);
    } else {
      this.gridService.setServerUrl([`test-plan-item/${testPlanItemId}/executions`]);
    }
  }

  private canDeleteExecution(rows: DataRow[]) {
    return (
      rows.length > 0 &&
      !rows.some((row) => row.data.boundToBlockingMilestone) &&
      !rows.some((row) => !(row.simplePermissions as CampaignPermissions).canDeleteExecution)
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  showMassDeleteExecutionDialog() {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        filter((rows: DataRow[]) => this.canDeleteExecution(rows)),
        concatMap((rows: DataRow[]) => this.openDeleteExecutionDialog(rows)),
        filter(({ confirmDelete }) => confirmDelete),
        tap(() => this.gridService.beginAsyncOperation()),
        withLatestFrom(this.executionPageService.componentData$),
        concatMap(([{ rows }, componentData]) =>
          this.removeExecutionsServerSide(rows, componentData),
        ),
        finalize(() => this.gridService.completeAsyncOperation()),
      )
      .subscribe(() => this.gridService.refreshData());
  }

  private openDeleteExecutionDialog(
    rows: DataRow[],
  ): Observable<{ confirmDelete: boolean; rows: DataRow[] }> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.campaign-workspace.dialog.title.mass-remove-execution',
      messageKey: 'sqtm-core.campaign-workspace.dialog.message.mass-remove-execution',
      level: 'DANGER',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map((confirmDelete) => ({ confirmDelete, rows })),
    );
  }

  private removeExecutionsServerSide(
    rows: DataRow[],
    componentData: ExecutionPageComponentData,
  ): Observable<any> {
    const rowIds = rows.map((row) => row.data[GridColumnId.executionId]);
    const isDisplayedExecutionInSelection = rowIds.includes(componentData.execution.id);

    if (isDisplayedExecutionInSelection) {
      return this.doRemoveExecutionsServerSide(rowIds, componentData.execution).pipe(
        tap(() => this.executionPageComponent.executionDeleted.emit()),
      );
    } else {
      return this.doRemoveExecutionsServerSide(rowIds, componentData.execution).pipe(
        tap(() => this.executionPageComponent.refreshData()),
      );
    }
  }

  private doRemoveExecutionsServerSide(
    executionIds: number[],
    executionState: ExecutionState,
  ): Observable<any> {
    if (executionState.iterationId != null) {
      return this.restService.delete<{
        nbIssues: number;
      }>([
        'iteration',
        executionState.iterationId.toString(),
        'test-plan/execution',
        executionIds.join(','),
      ]);
    } else {
      return this.restService.delete([
        'test-plan-item',
        executionState.testPlanItemId.toString(),
        'execution',
        executionIds.join(','),
      ]);
    }
  }

  private initializeDeleteColumnVisibility() {
    this.executionPageService.componentData$
      .pipe(takeUntil(this.unsub$))
      .subscribe((componentData) => {
        const isSprintClosed = componentData.execution.parentSprintStatus === 'CLOSED';
        this.gridService.setColumnVisibility(GridColumnId.delete, !isSprintClosed);
      });
  }
}

function executionConverter(
  literals: Partial<DataRow>[],
  projectDataMap: ProjectDataMap,
): DataRow[] {
  const executions = literals.map((li) => ({
    ...li,
    type: SquashTmDataRowType.Execution,
    data: {
      ...li.data,
      executionOrder: li.data.executionOrder + 1,
    },
  }));
  return convertSqtmLiterals(executions, projectDataMap);
}

export function executionHistoryTableDefinition(): GridDefinition {
  return grid('execution-page-history')
    .withColumns([
      indexColumn(),
      withLinkColumn(GridColumnId.executionOrder, {
        kind: 'link',
        baseUrl: '/execution',
        columnParamId: 'executionId',
        saveGridStateBeforeNavigate: true,
      })
        .withI18nKey('sqtm-core.entity.execution-plan.execution-number.short')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.execution-number.long')
        .changeWidthCalculationStrategy(new Fixed(60)),
      executionModeColumn(GridColumnId.inferredExecutionMode)
        .withI18nKey('sqtm-core.entity.execution.mode.label')
        .withTitleI18nKey('sqtm-core.entity.execution.mode.label')
        .changeWidthCalculationStrategy(new Fixed(60)),
      textColumn(GridColumnId.executionName)
        .withI18nKey('sqtm-core.entity.name')
        .changeWidthCalculationStrategy(new Extendable(150, 0.2)),
      testCaseImportanceColumn(GridColumnId.importance)
        .withI18nKey('sqtm-core.entity.test-case.importance.label-short-dot')
        .withTitleI18nKey('sqtm-core.entity.test-case.importance.label')
        .isEditable(false)
        .changeWidthCalculationStrategy(new Fixed(80)),
      textColumn(GridColumnId.datasetName)
        .withI18nKey('sqtm-core.entity.dataset.label.short')
        .withTitleI18nKey('sqtm-core.entity.dataset.label.singular')
        .changeWidthCalculationStrategy(new Fixed(100)),
      centredTextColumn(GridColumnId.successRate)
        .withRenderer(SuccessRateComponent)
        .withI18nKey('sqtm-core.entity.execution-plan.success-rate.label.short')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.success-rate.label.long')
        .changeWidthCalculationStrategy(new Fixed(60)),
      executionStatusColumn(GridColumnId.executionStatus)
        .withI18nKey('sqtm-core.entity.execution.status.label')
        .withTitleI18nKey('sqtm-core.entity.execution.status.long-label')
        .changeWidthCalculationStrategy(new Fixed(60)),
      numericColumn(GridColumnId.issueCount)
        .withI18nKey('sqtm-core.entity.execution-plan.ano-number.short')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.ano-number.long')
        .changeWidthCalculationStrategy(new Fixed(60)),
      textColumn(GridColumnId.user)
        .withI18nKey('sqtm-core.generic.label.user')
        .changeWidthCalculationStrategy(new Extendable(100, 0.2)),
      dateTimeColumn(GridColumnId.lastExecutedOn)
        .withSortFunction(sortDate)
        .withI18nKey('sqtm-core.entity.execution-plan.last-execution.label.short-dot')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.last-execution.label.long')
        .changeWidthCalculationStrategy(new Extendable(130, 0.2)),
      deleteExecutionPageHistoryColumn(GridColumnId.delete)
        .withLabel('')
        .disableSort()
        .changeWidthCalculationStrategy(new Fixed(30)),
    ])
    .server()
    .withRowConverter(executionConverter)
    .disableRightToolBar()
    .withStyle(new StyleDefinitionBuilder().enableInitialLoadAnimation().showLines())
    .withRowHeight(35)
    .build();
}
