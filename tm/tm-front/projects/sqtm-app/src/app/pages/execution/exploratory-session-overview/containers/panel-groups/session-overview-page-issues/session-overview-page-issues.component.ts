import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  InjectionToken,
  OnDestroy,
} from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { SessionOverviewPageComponentData } from '../../session-overview-page/session-overview-page.component';
import { SessionOverviewPageService } from '../../../services/session-overview-page.service';
import {
  ExportModelBuilderService,
  Extendable,
  Fixed,
  grid,
  GridColumnId,
  GridDefinition,
  GridExportService,
  GridId,
  GridService,
  gridServiceFactory,
  indexColumn,
  issueExecutionsColumn,
  issueKeyColumn,
  Limited,
  LocalPersistenceService,
  PaginationConfigBuilder,
  ReferentialDataService,
  RestService,
  Sort,
  textColumn,
} from 'sqtm-core';
import { take, tap } from 'rxjs/operators';

export const SESSION_OVERVIEW_ISSUE_TABLE_DEF = new InjectionToken(
  'SESSION_OVERVIEW_ISSUE_TABLE_DEF',
);
export const SESSION_OVERVIEW_ISSUE_TABLE = new InjectionToken('SESSION_OVERVIEW_ISSUE_TABLE');

export function sessionOverviewIssueTableDefinition(
  localPersistenceService: LocalPersistenceService,
): GridDefinition {
  return grid(GridId.SESSION_OVERVIEW_VIEW_ISSUE)
    .withColumns([
      indexColumn().changeWidthCalculationStrategy(new Fixed(70)).withViewport('leftViewport'),
      issueKeyColumn()
        .withI18nKey('sqtm-core.entity.issue.key.label')
        .changeWidthCalculationStrategy(new Limited(100)),
      textColumn(GridColumnId.btProject)
        .withI18nKey('sqtm-core.entity.issue.project.label')
        .changeWidthCalculationStrategy(new Limited(150))
        .disableSort(),
      textColumn(GridColumnId.summary)
        .withI18nKey('sqtm-core.entity.issue.summary.label')
        .changeWidthCalculationStrategy(new Limited(200))
        .disableSort(),
      textColumn(GridColumnId.priority)
        .withI18nKey('sqtm-core.entity.issue.priority.label')
        .changeWidthCalculationStrategy(new Extendable(100, 0.4))
        .disableSort(),
      textColumn(GridColumnId.status)
        .withI18nKey('sqtm-core.entity.issue.status.label')
        .changeWidthCalculationStrategy(new Limited(80))
        .disableSort(),
      textColumn(GridColumnId.assignee)
        .withI18nKey('sqtm-core.entity.issue.assignee.label')
        .changeWidthCalculationStrategy(new Extendable(100, 1))
        .disableSort(),
      issueExecutionsColumn()
        .withI18nKey('sqtm-core.entity.issue.reported-in.label')
        .changeWidthCalculationStrategy(new Extendable(100, 1.5))
        .disableSort(),
    ])
    .disableRightToolBar()
    .server()
    .withInitialSortedColumns([
      {
        id: GridColumnId.remoteId,
        sort: Sort.DESC,
      },
    ])
    .withPagination(new PaginationConfigBuilder().initialSize(25))
    .withRowHeight(35)
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
}

@Component({
  selector: 'sqtm-app-session-overview-page-issues',
  templateUrl: './session-overview-page-issues.component.html',
  styleUrls: ['./session-overview-page-issues.component.less'],
  providers: [
    {
      provide: SESSION_OVERVIEW_ISSUE_TABLE_DEF,
      useFactory: sessionOverviewIssueTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: SESSION_OVERVIEW_ISSUE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, SESSION_OVERVIEW_ISSUE_TABLE_DEF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: SESSION_OVERVIEW_ISSUE_TABLE,
    },
    GridExportService,
    ExportModelBuilderService,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SessionOverviewPageIssuesComponent implements OnDestroy {
  componentData$: Observable<SessionOverviewPageComponentData>;
  private authenticatedInBugTracker$ = new BehaviorSubject<boolean>(false);

  constructor(
    private readonly sessionOverviewPageService: SessionOverviewPageService,
    @Inject(SESSION_OVERVIEW_ISSUE_TABLE) private readonly issueTableService: GridService,
  ) {
    this.componentData$ = sessionOverviewPageService.componentData$;
    this.loadData();
  }

  ngOnDestroy() {
    this.issueTableService.complete();
  }

  notifyIsAuthenticatedInBugTracker() {
    this.authenticatedInBugTracker$.next(true);
  }

  loadData() {
    this.componentData$
      .pipe(
        take(1),
        tap((componentData) =>
          this.issueTableService.setServerUrl([
            `issues/session-overview/${componentData.exploratorySessionOverview.id}/known-issues`,
          ]),
        ),
      )
      .subscribe(() => this.authenticatedInBugTracker$.next(true));
  }
}
