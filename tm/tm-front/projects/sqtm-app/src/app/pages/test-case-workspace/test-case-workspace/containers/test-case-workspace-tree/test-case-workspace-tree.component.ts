import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewContainerRef,
} from '@angular/core';
import { Router } from '@angular/router';
import { combineLatest, Observable, of } from 'rxjs';
import {
  catchError,
  concatMap,
  filter,
  map,
  switchMap,
  take,
  takeUntil,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import {
  ActionErrorDisplayService,
  AutomationWorkflowTypes,
  BindableEntity,
  CreateEntityDialogComponent,
  CreationDialogConfiguration,
  DataRow,
  DialogService,
  EntityCreationDialogData,
  GridPersistenceService,
  GridService,
  isDataRowBoundToBlockingMilestone,
  isMilestoneModeActivated,
  milestoneAllowModification,
  MilestoneModeData,
  Permissions,
  Project,
  ProjectDataMap,
  REFERENCE_FIELD,
  ReferentialDataService,
  RestService,
  SquashTmDataRowType,
  TestCaseKind,
  toEntityRowReference,
  TransWorkspacesNodesCopierService,
  TreeWithStatePersistence,
} from 'sqtm-core';
import { ImportTestCaseComponent } from '../../../import-test-case/containers/import-test-case/import-test-case.component';
import { TC_WS_TREE } from '../../../test-case-workspace.constant';
import { testCaseWorkspaceLogger } from '../../../test-case-workspace.logger';
import { BddScriptExportDialogComponent } from '../../components/dialogs/bdd-script-export-dialog/bdd-script-export-dialog.component';
import { CreateTestCaseDialogComponent } from '../../components/dialogs/create-test-case-dialog/create-test-case-dialog.component';
import { CreateTestCaseFromRequirementDialogConfiguration } from '../../components/dialogs/create-test-case-from-requirement-dialog/create-test-case-from-requirement-dialog-configuration';
import { CreateTestCaseFromRequirementDialogComponent } from '../../components/dialogs/create-test-case-from-requirement-dialog/create-test-case-from-requirement-dialog.component';
import { GherkinScriptExportDialogComponent } from '../../components/dialogs/gherkin-script-export-dialog/gherkin-script-export-dialog.component';
import { TestCaseExportDialogConfiguration } from '../../components/dialogs/test-case-export-dialog/test-case-export-dialog-configuration';
import { TestCaseExportDialogComponent } from '../../components/dialogs/test-case-export-dialog/test-case-export-dialog.component';
import { TransmitResultDialogComponent } from '../../components/dialogs/transmit-result-dialog/transmit-result-dialog.component';

const logger = testCaseWorkspaceLogger.compose('TestCaseWorkspaceTreeComponent');

@Component({
  selector: 'sqtm-app-test-case-workspace-tree',
  templateUrl: './test-case-workspace-tree.component.html',
  styleUrls: ['./test-case-workspace-tree.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: TC_WS_TREE,
    },
  ],
})
export class TestCaseWorkspaceTreeComponent
  extends TreeWithStatePersistence
  implements OnInit, OnDestroy
{
  creationMenuButtonActivation: TestCaseTreeCreationPermission = {
    canCreateTestCase: false,
    canCreateFolder: false,
  };
  isAdminOrTestCaseImporter$: Observable<boolean>;
  canTransmit$: Observable<boolean>;
  canBeCollapsed$: Observable<boolean>;

  constructor(
    protected restService: RestService,
    public tree: GridService,
    public referentialDataService: ReferentialDataService,
    private cdRef: ChangeDetectorRef,
    protected dialogService: DialogService,
    protected vcr: ViewContainerRef,
    protected gridPersistenceService: GridPersistenceService,
    protected router: Router,
    private actionErrorDisplayService: ActionErrorDisplayService,
    private transWorkspacesNodesCopierService: TransWorkspacesNodesCopierService,
  ) {
    super(
      tree,
      referentialDataService,
      gridPersistenceService,
      restService,
      router,
      dialogService,
      vcr,
      'test-case-workspace-main-tree',
      'test-case-tree',
    );
  }

  ngOnInit() {
    this.initEntityIdFromInitialUrl();
    this.initData();
    this.registerStatePersistence();
    this.initTreeSortMenu();
    this.initCreationMenu();
    this.initImportMenu();
    this.initCanTransmit();
    this.initTruncateNameMenu();
    this.initCanCollapse();
  }

  private initImportMenu() {
    this.isAdminOrTestCaseImporter$ = this.referentialDataService.authenticatedUser$.pipe(
      takeUntil(this.unsub$),
      switchMap((authenticatedUser) => {
        return authenticatedUser.admin
          ? of(true)
          : this.referentialDataService.projectDatas$.pipe(
              map((projectData: ProjectDataMap) => this.hasImportPermission(projectData)),
            );
      }),
    );
  }

  private hasImportPermission(projectDataMap: ProjectDataMap): boolean {
    return Object.values(projectDataMap).some((projectData) =>
      projectData.permissions.TEST_CASE_LIBRARY.some(
        (permission: Permissions) => permission === Permissions.IMPORT,
      ),
    );
  }

  private initCanTransmit() {
    this.canTransmit$ = this.tree.selectedRows$.pipe(
      takeUntil(this.unsub$),
      withLatestFrom(this.referentialDataService.projects$),
      map(([rows, projects]: [DataRow[], Project[]]) => {
        if (rows.length === 0) {
          return false;
        } else if (!this.checkAllProjectsAllowAutomationWorkflow(rows, projects)) {
          return false;
        } else if (!this.isCompliantToNativeSimplifiedWorkFlowRules(rows, projects)) {
          return false;
        } else if (rows.filter((row) => row.simplePermissions.canWrite).length !== rows.length) {
          return false;
        } else {
          return rows.filter((dataRow) => isDataRowBoundToBlockingMilestone(dataRow)).length === 0;
        }
      }),
    );
  }

  private checkAllProjectsAllowAutomationWorkflow(rows: DataRow[], projects: Project[]): boolean {
    const projectIds = rows.map((row) => row.projectId);
    const uniqueProjectIds = Array.from(new Set(projectIds));

    const projectAllowed = projects
      .filter((project) => uniqueProjectIds.includes(project.id))
      .filter((project) => project.allowAutomationWorkflow);
    return projectAllowed.length === uniqueProjectIds.length;
  }

  private isCompliantToNativeSimplifiedWorkFlowRules(
    rows: DataRow[],
    projects: Project[],
  ): boolean {
    if (
      this.selectedRowsHaveOnlyProjectWithSimplifiedWorkflow(rows, projects) &&
      this.selectedRowsAreOnlyGherkinOrKeywordTestcases(rows)
    ) {
      return this.atLeastOneRowHasProjectWithConfiguredScm(rows, projects);
    } else {
      return true;
    }
  }

  private selectedRowsAreOnlyGherkinOrKeywordTestcases(rows: DataRow[]) {
    return rows.every(
      (row) =>
        row.data['TC_KIND'] === TestCaseKind.GHERKIN.id ||
        row.data['TC_KIND'] === TestCaseKind.KEYWORD.id,
    );
  }

  private selectedRowsHaveOnlyProjectWithSimplifiedWorkflow(rows: DataRow[], projects: Project[]) {
    const projectIdsWithSimplifiedWorkflow = projects
      .filter(
        (project) => project.automationWorkflowType === AutomationWorkflowTypes.NATIVE_SIMPLIFIED,
      )
      .map((project) => project.id);

    return rows.every((row) => projectIdsWithSimplifiedWorkflow.includes(row.projectId));
  }

  private atLeastOneRowHasProjectWithConfiguredScm(rows: DataRow[], projects: Project[]) {
    const projectIdsWithConfiguredScm = projects
      .filter((project) => project.scmRepositoryId)
      .map((project) => project.id);

    return rows.some((row) => projectIdsWithConfiguredScm.includes(row.projectId));
  }

  private initCreationMenu() {
    combineLatest([this.tree.selectedRows$, this.referentialDataService.milestoneModeData$])
      .pipe(takeUntil(this.unsub$))
      .subscribe(([selectedRows, milestoneFilter]: [DataRow[], MilestoneModeData]) => {
        this.updateCreationMenu(selectedRows, milestoneFilter);
        this.cdRef.markForCheck();
      });
  }

  ngOnDestroy(): void {
    this.unregisterStatePersistence();
    this.unsub$.next();
    this.unsub$.complete();
  }

  openCreateTestCase() {
    if (this.creationMenuButtonActivation.canCreateTestCase) {
      this.tree.selectedRows$.pipe(take(1)).subscribe((dataRows: DataRow[]) => {
        // The creation menu item is enabled if there's only 1 selected element, so we can safely take the first one
        const selectedDataRow = dataRows[0];
        logger.debug(`DataRow: ${JSON.stringify(selectedDataRow)}`);

        const configuration: CreationDialogConfiguration<EntityCreationDialogData> = {
          viewContainerReference: this.vcr,
          formComponent: CreateTestCaseDialogComponent,
          data: {
            titleKey: 'sqtm-core.test-case-workspace.dialog.title.new-test-case',
            id: 'add-test-case',
            projectId: selectedDataRow.projectId,
            bindableEntity: BindableEntity.TEST_CASE,
            parentEntityReference:
              selectedDataRow.type === SquashTmDataRowType.TestCase
                ? selectedDataRow.parentRowId.toString()
                : selectedDataRow.id.toString(),
            postUrl: 'test-case-tree/new-test-case',
            optionalTextFields: [REFERENCE_FIELD],
          },
        };

        this.dialogService.openEntityCreationDialog<EntityCreationDialogData, DataRow>(
          configuration,
          this.tree,
        );
      });
    }
  }

  openCreateFolder() {
    if (this.creationMenuButtonActivation.canCreateFolder) {
      this.tree.selectedRows$.pipe(take(1)).subscribe((dataRows: DataRow[]) => {
        // The creation menu item is enabled if there's only 1 selected element, so we can safely take the first one
        const selectedDataRow = dataRows[0];
        logger.debug(`DataRow: ${JSON.stringify(selectedDataRow)}`);

        const configuration: CreationDialogConfiguration<EntityCreationDialogData> = {
          viewContainerReference: this.vcr,
          formComponent: CreateEntityDialogComponent,
          data: {
            titleKey: 'sqtm-core.test-case-workspace.dialog.title.new-folder',
            id: 'add-test-case-folder',
            projectId: selectedDataRow.projectId,
            bindableEntity: BindableEntity.TESTCASE_FOLDER,
            parentEntityReference: selectedDataRow.id.toString(),
            postUrl: 'test-case-tree/new-folder',
          },
        };
        this.dialogService.openEntityCreationDialog<EntityCreationDialogData, DataRow>(
          configuration,
          this.tree,
        );
      });
    }
  }

  private updateCreationMenu(selectedRows: DataRow[], milestoneState: MilestoneModeData): void {
    let permissions = { ...TC_TREE_NO_CREATION };
    let creationAllowed = selectedRows.length === 1;
    if (
      creationAllowed &&
      milestoneState.milestoneFeatureEnabled &&
      milestoneState.milestoneModeEnabled
    ) {
      creationAllowed =
        milestoneState.selectedMilestone &&
        milestoneAllowModification(milestoneState.selectedMilestone);
    }
    if (creationAllowed) {
      const selectedRow = selectedRows[0];
      const canCreate = selectedRow.simplePermissions.canCreate;
      permissions = {
        canCreateFolder:
          selectedRow.allowedChildren.includes(SquashTmDataRowType.TestCaseFolder) && canCreate,
        canCreateTestCase: canCreate,
      };
    }
    this.creationMenuButtonActivation = permissions;
    this.cdRef.detectChanges();
  }

  copy() {
    this.tree.copy();
  }

  paste() {
    this.tree.paste();
  }

  delete() {
    this.tree.deleteRows();
  }

  navigateToMilestoneDashboard() {
    this.tree.unselectAllRows();
    this.referentialDataService.milestoneModeData$
      .pipe(
        take(1),
        filter((milestoneData) => isMilestoneModeActivated(milestoneData)),
      )
      .subscribe(() => this.router.navigate(['test-case-workspace', 'milestone-dashboard']));
  }

  import() {
    this.isAdminOrTestCaseImporter$
      .pipe(
        take(1),
        filter((isAdminOrTestCaseImporter: boolean) => isAdminOrTestCaseImporter),
        tap(() => this.openImportDialog()),
      )
      .subscribe();
  }

  private openImportDialog() {
    const importDialog = this.dialogService.openDialog({
      id: 'import',
      component: ImportTestCaseComponent,
      width: 600,
    });

    importDialog.dialogResultChanged$
      .pipe(
        take(1),
        filter((result: boolean) => result),
      )
      .subscribe(() => {
        this.tree.gridNodes$.pipe(take(1)).subscribe((nodes) => {
          const libraryNodes = nodes.filter((node) =>
            node.id.toString().includes('TestCaseLibrary'),
          );
          this.tree.refreshSubTree(libraryNodes.map((node) => node.id));
        });
      });
  }

  export() {
    this.tree.canExport$
      .pipe(
        take(1),
        filter((canExport) => canExport),
        switchMap(() => this.tree.selectedRows$),
        take(1),
        filter((rows: DataRow[]) => rows.length > 0),
        concatMap((rows: DataRow[]) => this.extractNodesAndLibraries(rows)),
      )
      .subscribe((result: { nodes: number[]; libraries: number[] }) => {
        this.openExportDialog(result.libraries, result.nodes);
      });
  }

  exportBddScripts() {
    this.tree.canExport$
      .pipe(
        take(1),
        filter((canExport) => canExport),
        switchMap(() => this.tree.selectedRows$),
        take(1),
        map((rows: DataRow[]) =>
          rows.filter(
            (row: DataRow) =>
              row.data['TC_KIND'] === undefined || row.data['TC_KIND'] === 'KEYWORD',
          ),
        ),
        concatMap((rows: DataRow[]) => this.extractNodesAndLibraries(rows)),
      )
      .subscribe((result: { nodes: number[]; libraries: number[] }) => {
        if (result.nodes.length > 0 || result.libraries.length > 0) {
          this.openExportBddScriptDialog(result.libraries, result.nodes);
        } else {
          this.dialogService.openAlert({
            id: 'no-bdd-test-case-selected',
            titleKey: 'sqtm-core.test-case-workspace.dialog.title.export-bdd-script',
            level: 'INFO',
            messageKey: 'sqtm-core.test-case-workspace.dialog.message.no-bdd-test-case-selected',
          });
        }
      });
  }

  exportGherkinScripts() {
    this.tree.canExport$
      .pipe(
        take(1),
        filter((canExport) => canExport),
        switchMap(() => this.tree.selectedRows$),
        take(1),
        map((rows: DataRow[]) =>
          rows.filter(
            (row: DataRow) =>
              row.data['TC_KIND'] === undefined || row.data['TC_KIND'] === 'GHERKIN',
          ),
        ),
        concatMap((rows: DataRow[]) => this.extractNodesAndLibraries(rows)),
      )
      .subscribe((result: { nodes: number[]; libraries: number[] }) => {
        if (result.nodes.length > 0 || result.libraries.length > 0) {
          this.openExportGherkinScriptDialog(result.libraries, result.nodes);
        } else {
          this.dialogService.openAlert({
            id: 'no-gherkin-test-case-selected',
            titleKey: 'sqtm-core.test-case-workspace.dialog.title.export-gherkin-script',
            level: 'INFO',
            messageKey:
              'sqtm-core.test-case-workspace.dialog.message.no-gherkin-test-case-selected',
          });
        }
      });
  }

  private openExportBddScriptDialog(libraries: number[], nodes: number[]) {
    this.dialogService.openDialog<TestCaseExportDialogConfiguration, any>({
      id: 'export-bdd-script',
      component: BddScriptExportDialogComponent,
      viewContainerReference: this.vcr,
      data: { id: 'export-bdd-script', libraries, nodes },
      width: 600,
    });
  }

  private openExportGherkinScriptDialog(libraries: number[], nodes: number[]) {
    this.dialogService.openDialog<TestCaseExportDialogConfiguration, any>({
      id: 'export-gherkin-script',
      component: GherkinScriptExportDialogComponent,
      viewContainerReference: this.vcr,
      data: { id: 'export-gherkin-script', libraries, nodes },
      width: 600,
    });
  }

  private extractNodesAndLibraries(
    rows: DataRow[],
  ): Observable<{ nodes: number[]; libraries: number[] }> {
    const nodes = [];
    const libraries = [];
    rows.forEach((row) => {
      const entityRef = toEntityRowReference(row.id);
      if (entityRef.entityType === SquashTmDataRowType.TestCaseLibrary) {
        libraries.push(entityRef.id);
      } else {
        nodes.push(entityRef.id);
      }
    });
    return of({ nodes, libraries });
  }

  private openExportDialog(libraries: number[], nodes: number[]) {
    this.dialogService.openDialog<TestCaseExportDialogConfiguration, any>({
      id: 'export-test-case',
      component: TestCaseExportDialogComponent,
      viewContainerReference: this.vcr,
      data: { id: 'export-test-case', libraries, nodes },
      width: 600,
    });
  }

  navigateToSearch() {
    this.router.navigate(['/search/test-case']);
  }

  transmit() {
    this.canTransmit$.pipe(take(1)).subscribe((cantTransmit) => {
      if (cantTransmit) {
        this.sendTestToTransmit();
      }
    });
  }

  private sendTestToTransmit() {
    this.tree.selectedRows$
      .pipe(
        take(1),
        map((rows: DataRow[]) => this.buildTransmitNodes(rows)),
        concatMap((transmitNodes) =>
          this.restService.post<TransmitResult>(
            ['test-case-tree/transmit-eligible-tcs'],
            transmitNodes,
          ),
        ),
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
      )
      .subscribe((result) => {
        this.dialogService.openDialog({
          id: 'mass-transmit',
          viewContainerReference: this.vcr,
          data: result,
          component: TransmitResultDialogComponent,
        });
      });
  }

  private buildTransmitNodes(dataRows: DataRow[]): TransmitNodes {
    const folderIds = [];
    const testCaseIds = [];
    const libraryIds = [];
    dataRows.forEach((row) => {
      const entityReference = toEntityRowReference(row.id);
      if (row.type === SquashTmDataRowType.TestCaseFolder) {
        folderIds.push(entityReference.id);
      } else if (row.type === SquashTmDataRowType.TestCaseLibrary) {
        libraryIds.push(entityReference.id);
      } else {
        testCaseIds.push(entityReference.id);
      }
    });
    return { testcases: testCaseIds, folders: folderIds, libraries: libraryIds } as TransmitNodes;
  }

  canAddTcFromRequirement() {
    return (
      this.hasCopiedRequirements() &&
      this.canCreate() &&
      this.hasOnlyOneRowSelected() &&
      this.isLibraryOrFolder()
    );
  }

  isLibraryOrFolder() {
    let libraryOrFolder = false;
    this.tree.selectedRows$.pipe(take(1)).subscribe((rows) => {
      libraryOrFolder = rows[0].type !== 'TestCase';
    });
    return libraryOrFolder;
  }

  hasOnlyOneRowSelected() {
    let onlyOneRow = false;
    this.tree.selectedRows$.pipe(take(1)).subscribe((rows) => {
      onlyOneRow = rows.length === 1;
    });
    return onlyOneRow;
  }

  hasCopiedRequirements() {
    return this.transWorkspacesNodesCopierService.copiedRequirementNodes.length > 0;
  }

  canCreate() {
    let canCreate = false;
    this.tree.canCreate$.subscribe((isAllowed) => {
      canCreate = isAllowed;
    });
    return canCreate;
  }

  addTcFromRequirement() {
    if (this.canAddTcFromRequirement()) {
      const copiedRequirementNodes = this.transWorkspacesNodesCopierService.copiedRequirementNodes;
      let selectedTestCaseRows: DataRow[];
      this.tree.selectedRows$.pipe(take(1)).subscribe((rows) => {
        selectedTestCaseRows = rows;
      });

      this.openCreationDialogAndHandleCreation(copiedRequirementNodes, selectedTestCaseRows);
    }
  }

  private openCreationDialogAndHandleCreation(
    copiedRequirementNodes: DataRow[],
    selectedTestCaseRows: DataRow[],
  ) {
    const config = this.dialogService.openDialog<
      CreateTestCaseFromRequirementDialogConfiguration,
      any
    >({
      id: 'create-test-case-from-requirement',
      component: CreateTestCaseFromRequirementDialogComponent,
      viewContainerReference: this.vcr,
      data: { id: 'create-test-case-from-requirement' },
      width: 600,
    });

    config.dialogClosed$.subscribe((result) => {
      if (result) {
        const destinationId = selectedTestCaseRows.map((row) => row.id).toString();
        const nodeList: string[] = copiedRequirementNodes.map((node) => node.id.toString());
        this.createTcFromRequirement(destinationId, result, nodeList);
      }
    });
  }

  createTcFromRequirement(destinationId: string, tcKind: string, nodeList: string[]) {
    this.tree.beginAsyncOperation();
    this.restService
      .post(['test-case-tree', destinationId, 'content/paste-from-requirement', tcKind], {
        references: nodeList,
      })
      .pipe(
        tap(() => this.tree.refreshSubTree([destinationId])),
        tap(() => this.tree.completeAsyncOperation()),
      )
      .subscribe();
  }

  private initCanCollapse() {
    this.canBeCollapsed$ = this.tree.canBeCollapsed$;
  }

  collapseTree() {
    this.tree.closeAllRows();
  }
}

export interface TestCaseTreeCreationPermission {
  canCreateTestCase: boolean;
  canCreateFolder: boolean;
}

export const TC_TREE_NO_CREATION: Readonly<TestCaseTreeCreationPermission> = {
  canCreateFolder: false,
  canCreateTestCase: false,
};

export interface TransmitNodes {
  testcases: number[];
  folders: number[];
  libraries: number[];
}

export interface TransmitResult {
  areAllEligible: boolean;
  eligibleTcIds: number[];
}
