import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  computed,
  OnDestroy,
  Signal,
} from '@angular/core';
import {
  AbstractCellRendererComponent,
  AuthenticatedUser,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
  Permissions,
  ReferentialDataService,
  ReferentialDataState,
} from 'sqtm-core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sqtm-app-execution',
  template: `@if (row) {
    <div class="full-width full-height flex-column">
      @if ($canConsult()) {
        <a class="m-auto text-overflow" [routerLink]="getExecutionUrl()">{{
          getExecutionOrder()
        }}</a>
      }
      @if (!$canConsult()) {
        <span class="m-auto text-overflow">{{ getExecutionOrder() }}</span>
      }
    </div>
  }`,
  styleUrls: ['./execution-order.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExecutionOrderComponent extends AbstractCellRendererComponent implements OnDestroy {
  authenticatedUser$: Observable<AuthenticatedUser>;
  unsub$ = new Subject<void>();
  $referentialData = toSignal(this.referentialDataService.referentialData$);
  $canConsult: Signal<boolean> = computed(() => this.canConsult(this.$referentialData()));

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    private referentialDataService: ReferentialDataService,
  ) {
    super(grid, cdRef);
    this.authenticatedUser$ = referentialDataService.authenticatedUser$.pipe(
      takeUntil(this.unsub$),
    );
  }

  getExecutionOrder(): string {
    const order = this.row.data[this.columnDisplay.id];
    return `#${order + 1}`;
  }

  getExecutionUrl(): string {
    return `/execution/${this.row.data[GridColumnId.executionId]}`;
  }

  canConsult(referentialData: ReferentialDataState) {
    const userState = referentialData.userState;

    if (userState.admin) {
      return true;
    }

    const projectId: number = this.row.projectId;
    const permissions =
      referentialData.projectState.entities[projectId].permissions.CAMPAIGN_LIBRARY;
    const canReadUnassigned: boolean = permissions.includes(Permissions.READ_UNASSIGNED);
    const isAssigned: boolean =
      this.row.data[GridColumnId.assigneeId] === referentialData.userState.userId;

    return canReadUnassigned || isAssigned;
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}

export function executionOrderColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(ExecutionOrderComponent);
}
