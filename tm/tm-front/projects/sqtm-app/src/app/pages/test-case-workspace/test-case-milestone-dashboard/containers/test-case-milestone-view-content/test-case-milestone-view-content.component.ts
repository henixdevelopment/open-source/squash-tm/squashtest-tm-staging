import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TestCaseMilestoneViewState } from '../../state/test-case-milestone-view-state';
import { TestCaseMilestoneViewService } from '../../services/test-case-milestone-view.service';
import { TranslateService } from '@ngx-translate/core';
import { tap } from 'rxjs/operators';
import {
  CustomDashboardBinding,
  CustomDashboardModel,
  FavoriteDashboardValue,
  getSupportedBrowserLang,
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-test-case-milestone-view-content',
  templateUrl: './test-case-milestone-view-content.component.html',
  styleUrls: ['./test-case-milestone-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestCaseMilestoneViewContentComponent implements OnInit {
  public componentData$: Observable<Readonly<TestCaseMilestoneViewState>>;

  constructor(
    private viewService: TestCaseMilestoneViewService,
    public translateService: TranslateService,
  ) {}

  ngOnInit(): void {
    this.componentData$ = this.viewService.componentData$;
  }

  refreshStats($event: MouseEvent) {
    $event.stopPropagation();
    this.viewService.refreshStatistics();
  }

  changeDashboardToDisplay($event: MouseEvent, preferenceValue: FavoriteDashboardValue) {
    $event.stopPropagation();
    this.viewService
      .changeDashboardToDisplay(preferenceValue)
      .pipe(tap(() => this.viewService.refreshStatistics()))
      .subscribe();
  }

  getBrowserLanguage() {
    return getSupportedBrowserLang(this.translateService);
  }

  getChartBindings(dashboard: CustomDashboardModel) {
    return [...dashboard.chartBindings, ...dashboard.reportBindings] as CustomDashboardBinding[];
  }
}
