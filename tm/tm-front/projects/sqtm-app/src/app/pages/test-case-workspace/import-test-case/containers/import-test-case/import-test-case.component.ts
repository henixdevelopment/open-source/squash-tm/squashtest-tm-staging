import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { DialogReference, ReferentialDataService, RestService } from 'sqtm-core';
import {
  ImportTestCaseComponentData,
  TestCaseFileFormat,
  XLS_TYPE,
} from '../../state/import-test-case.state';
import { ImportTestCaseService } from '../../services/import-test-case.service';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-import-test-case',
  templateUrl: './import-test-case.component.html',
  styleUrls: ['./import-test-case.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: ImportTestCaseService,
      useClass: ImportTestCaseService,
      deps: [RestService, ReferentialDataService, TranslateService],
    },
  ],
})
export class ImportTestCaseComponent implements OnInit {
  wrongFileFormat: boolean;

  componentData$: Observable<ImportTestCaseComponentData>;

  constructor(
    private dialogReference: DialogReference,
    private importTestCaseService: ImportTestCaseService,
    private translator: TranslateService,
  ) {}

  ngOnInit(): void {
    this.initializeComponentData();
  }

  initializeComponentData() {
    this.componentData$ = this.importTestCaseService.componentData$;
  }

  simulateImport(componentData: ImportTestCaseComponentData) {
    if (this.checkFileType(componentData)) {
      this.importTestCaseService.simulateImport(componentData.file);
    } else {
      this.wrongFileFormat = true;
    }
  }

  private checkFileType(componentData: ImportTestCaseComponentData): boolean {
    const file = componentData.file;

    if (file != null) {
      return XLS_TYPE.includes(file.type);
    } else {
      return false;
    }
  }

  confirmImport(componentData: ImportTestCaseComponentData) {
    if (this.checkFileType(componentData)) {
      this.importTestCaseService.goToConfirmationImportPage();
    } else {
      this.wrongFileFormat = true;
    }
  }

  importTestCase(componentData: ImportTestCaseComponentData) {
    if (this.checkFileType(componentData)) {
      this.doImport();
    } else {
      this.wrongFileFormat = true;
    }
  }

  resetWrongFileFormatErrorMessage() {
    this.wrongFileFormat = false;
  }

  private doImport() {
    this.importTestCaseService.importTestCaseXls().subscribe({
      next: () => {
        this.dialogReference.result = true;
      },
      error: (error) => this.importTestCaseService.handleXlsImportError(error),
    });
  }

  cancelImport(componentData: ImportTestCaseComponentData) {
    if (componentData.importFailed) {
      this.importTestCaseService.restoreState();
    } else {
      this.dialogReference.close();
    }
  }

  getDialogTitle(componentData: ImportTestCaseComponentData) {
    switch (componentData.currentStep) {
      case 'CONFIGURATION':
        return this.translator.instant('sqtm-core.test-case-workspace.dialog.title.config-import');
      case 'SIMULATION_REPORT':
        return this.translator.instant(
          'sqtm-core.test-case-workspace.dialog.title.simulate-import',
        );
      case 'CONFIRMATION':
        return (
          this.translator.instant('sqtm-core.test-case-workspace.dialog.title.confirm-import') +
          ' ' +
          TestCaseFileFormat[componentData.format].value
        );
      case 'CONFIRMATION_REPORT':
        return this.translator.instant('sqtm-core.test-case-workspace.dialog.title.report-import');
      default:
        return '';
    }
  }

  getProjectName(componentData: ImportTestCaseComponentData) {
    const project = componentData.projects.find((p) => p.id === componentData.selectedProject);
    return project.name;
  }
}
