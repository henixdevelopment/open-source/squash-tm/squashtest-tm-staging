import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { CreateEntityFormComponent, DialogReference, RestService } from 'sqtm-core';
import {
  NewTestCaseVersionDialogConfiguration,
  NewTestCaseVersionDialogResult,
} from './new-test-case-version-dialog.configuration';
import { HttpErrorResponse } from '@angular/common/http';
import { testCaseViewLogger } from '../../../test-case-view.logger';

const logger = testCaseViewLogger.compose('NewVersionDialogComponent');

@Component({
  selector: 'sqtm-app-new-version-dialog',
  templateUrl: './new-version-dialog.component.html',
  styleUrls: ['./new-version-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewVersionDialogComponent {
  data: NewTestCaseVersionDialogConfiguration;

  @ViewChild(CreateEntityFormComponent)
  private form: CreateEntityFormComponent;

  constructor(
    private dialogReference: DialogReference<
      NewTestCaseVersionDialogConfiguration,
      NewTestCaseVersionDialogResult
    >,
    private restService: RestService,
  ) {
    this.data = dialogReference.data;
  }

  handleConfirm() {
    if (this.form.validateForm()) {
      const model = this.form.buildEntityFormModel();
      model['testCaseKind'] = this.data.tcKind;
      this.restService
        .post<{
          newVersionId: number;
        }>(['test-case-tree', this.data.originalTestCaseId.toString(), 'new-version'], model)
        .subscribe({
          next: ({ newVersionId }) => this.handleSuccess(newVersionId),
          error: (error) => this.handleCreationFailure(error),
        });
    }
  }

  private handleCreationFailure(error: HttpErrorResponse) {
    logger.debug(`Server side error: `, [error]);
    if (error.status === 412) {
      const squashError = error.error.squashTMError;
      if (squashError.kind === 'FIELD_VALIDATION_ERROR') {
        this.form.showServerSideErrors(squashError.fieldValidationErrors);
      }
    }
  }

  private handleSuccess(newVersionId: number) {
    this.dialogReference.result = { newVersionId };
    this.dialogReference.close();
  }
}
