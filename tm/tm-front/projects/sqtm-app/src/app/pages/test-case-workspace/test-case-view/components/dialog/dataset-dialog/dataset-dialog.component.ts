import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import {
  DialogReference,
  FieldValidationError,
  Parameter,
  RestService,
  TestCaseParameterOperationReport,
  TestCaseService,
  TextFieldComponent,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { DatasetDialogConfiguration } from './dataset-dialog-configuration';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'sqtm-app-dataset-dialog',
  templateUrl: './dataset-dialog.component.html',
  styleUrls: ['./dataset-dialog.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DatasetDialogComponent implements OnInit {
  @ViewChildren(TextFieldComponent)
  textFields: QueryList<TextFieldComponent>;

  @ViewChild(TextFieldComponent)
  nameField: TextFieldComponent;

  formGroup: FormGroup;

  serverSideValidationErrors: FieldValidationError[] = [];

  data: DatasetDialogConfiguration;

  parameters: Parameter[] = [];

  constructor(
    private fb: FormBuilder,
    private dialogReference: DialogReference<
      DatasetDialogConfiguration,
      TestCaseParameterOperationReport
    >,
    private translateService: TranslateService,
    private restService: RestService,
    private cdr: ChangeDetectorRef,
    private testCaseService: TestCaseService,
  ) {
    this.data = this.dialogReference.data;
    this.parameters = this.dialogReference.data.parameters;
  }

  ngOnInit(): void {
    this.initFormControls();
  }

  private initFormControls() {
    this.initializeFormGroup();
    this.initializeParameterFormGroup();
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      name: this.fb.control('', [Validators.required, Validators.maxLength(255)]),
    });
  }

  private initializeParameterFormGroup() {
    this.parameters.forEach((param) => {
      this.formGroup.addControl(
        param.id.toString(),
        new FormControl('', [Validators.maxLength(1024)]),
      );
    });
  }

  addAnotherDataset() {
    this.addDataset(true);
  }

  addDataset(addAnother?: boolean) {
    if (this.formGroup.status === 'VALID') {
      const paramValues = [];
      this.data.parameters.forEach((param) => {
        const control = this.formGroup.get(param.id.toString());
        paramValues.push({ paramId: param.id, value: control.value });
      });
      const dataSet = { name: this.formGroup.get('name').value, paramValues: paramValues };
      this.testCaseService.persistDataset(this.dialogReference.data.testCaseId, dataSet).subscribe({
        next: (response) => this.success(response, addAnother),
        error: (error) => this.handleCreationFailure(error),
      });
    } else {
      this.showClientSideErrors();
    }
  }

  private success(result: TestCaseParameterOperationReport, addAnother: boolean) {
    this.dialogReference.result = result;
    if (addAnother) {
      this.resetForm();
    } else {
      this.dialogReference.close();
    }
  }

  private resetForm() {
    this.formGroup.reset();
    this.initFormControls();
    this.serverSideValidationErrors = [];
    this.cdr.detectChanges();
    this.nameField.grabFocus();
  }

  private showClientSideErrors() {
    this.textFields.forEach((item) => item.showClientSideError());
  }

  private handleCreationFailure(error: HttpErrorResponse) {
    if (error.status === 412) {
      const squashError = error.error.squashTMError;
      if (squashError.kind === 'FIELD_VALIDATION_ERROR') {
        this.serverSideValidationErrors = squashError.fieldValidationErrors;
        this.cdr.markForCheck();
      }
    }
  }
}
