import { InjectionToken } from '@angular/core';
import { GridDefinition, GridService } from 'sqtm-core';

export const TC_WS_TREE_CONFIG = new InjectionToken<GridDefinition>(
  'Grid config instance for the main test case workspace tree',
);
export const TC_WS_TREE = new InjectionToken<GridService>(
  'Grid service instance for the main test case workspace tree',
);

export const testCaseWorkspaceTreeId = 'test-case-workspace-main-tree';
