import { Injectable } from '@angular/core';
import {
  FileEncoding,
  FileEncodingKeys,
  ImportSummary,
  ImportTestCaseComponentData,
  ImportTestCaseState,
  ImportTestCaseSteps,
  initialImportTestCaseState,
  TestCaseImportFileFormatKeys,
} from '../state/import-test-case.state';
import {
  createStore,
  doesHttpErrorContainsSquashActionError,
  FRONT_END_ERROR_IS_HANDLED_PARAM,
  ProjectData,
  ReferentialDataService,
  RestService,
  Store,
  TestCaseXlsReport,
} from 'sqtm-core';
import { Observable, throwError } from 'rxjs';
import { concatMap, map, take, tap, withLatestFrom } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root',
})
export class ImportTestCaseService {
  store: Store<ImportTestCaseState>;

  state$: Observable<ImportTestCaseState>;

  componentData$: Observable<ImportTestCaseComponentData>;

  constructor(
    private restService: RestService,
    private referentialData: ReferentialDataService,
    private translateService: TranslateService,
  ) {
    const initialState = initialImportTestCaseState();
    this.store = createStore(initialState);

    this.state$ = this.store.state$;
    this.initializeComponentData();

    this.componentData$ = this.store.state$;
  }

  initializeComponentData() {
    this.state$
      .pipe(
        take(1),
        withLatestFrom(this.referentialData.projectsManaged$),
        map(([state, projects]: [ImportTestCaseState, ProjectData[]]) => {
          const componentData: ImportTestCaseComponentData = {
            ...state,
            projects,
            selectedProject: projects[0].id,
          };
          return componentData;
        }),
        tap((state) => this.store.commit(state)),
      )
      .subscribe();
  }

  restoreState() {
    this.store.state$
      .pipe(
        take(1),
        map((_state) => initialImportTestCaseState()),
      )
      .subscribe((state) => this.store.commit(state));
  }

  simulateImport(file: File) {
    this.store.state$
      .pipe(
        take(1),
        map((state: ImportTestCaseState) => this.changeCurrentStep(state, 'SIMULATION_REPORT')),
        map((state: ImportTestCaseState) => this.beginAsync(state)),
        concatMap((state: ImportTestCaseState) => this.doXlsImport(state, file, true)),
        map((state: ImportTestCaseState) => this.endAsync(state)),
      )
      .subscribe({
        next: (state) => this.store.commit(state),
        error: (error) => this.handleXlsImportError(error),
      });
  }

  goToConfirmationImportPage() {
    this.store.state$
      .pipe(
        take(1),
        map((state: ImportTestCaseState) => this.changeCurrentStep(state, 'CONFIRMATION')),
      )
      .subscribe((state) => this.store.commit(state));
  }

  importTestCaseZip(): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      map((state: ImportTestCaseState) => this.changeCurrentStep(state, 'CONFIRMATION_REPORT')),
      map((state: ImportTestCaseState) => this.beginAsync(state)),
      concatMap((state) => {
        const encoding = FileEncoding[state.encoding];
        const formData = new FormData();
        formData.append('archive', state.file, state.file.name);
        formData.append('projectId', state.selectedProject.toString());
        formData.append('zipEncoding', encoding.value);
        return this.restService.post<ImportSummary>([`test-cases/importer/zip`], formData).pipe(
          map((response) => {
            return { ...state, zipReport: response };
          }),
        );
      }),
      map((state: ImportTestCaseState) => this.endAsync(state)),
      tap((state) => this.store.commit(state)),
    );
  }

  importTestCaseXls(): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      map((state: ImportTestCaseState) => this.changeCurrentStep(state, 'CONFIRMATION_REPORT')),
      map((state: ImportTestCaseState) => this.beginAsync(state)),
      concatMap((state: ImportTestCaseState) => this.doXlsImport(state, state.file)),
      map((state: ImportTestCaseState) => this.endAsync(state)),
      tap((state) => this.store.commit(state)),
    );
  }

  private doXlsImport(
    state: ImportTestCaseState,
    file: File,
    simulation?: boolean,
  ): Observable<ImportTestCaseState> {
    const formData = new FormData();
    formData.append('archive', file, file.name);
    if (simulation) {
      formData.append('dry-run', 'true');
    }

    const params = { [FRONT_END_ERROR_IS_HANDLED_PARAM]: 'true' };

    return this.restService
      .post<TestCaseXlsReport>([`test-cases/importer/xls`], formData, {
        params,
      })
      .pipe(
        map((response: TestCaseXlsReport) => {
          const importFailed = response.importFormatFailure != null;
          if (simulation) {
            return { ...state, simulationReport: response, importFailed };
          } else {
            return { ...state, xlsReport: response, importFailed };
          }
        }),
      );
  }

  handleXlsImportError(httpError: any) {
    if (doesHttpErrorContainsSquashActionError(httpError)) {
      this.handleSquashActionError(httpError);
    } else {
      this.handleOtherHttpErrors(httpError);
    }
  }

  private handleSquashActionError(httpError: any) {
    const squashError = httpError.error.squashTMError;
    const errorMessage = this.translateService.instant(
      squashError.actionValidationError.i18nKey ?? 'sqtm-core.generic.label.exception.message',
      squashError.actionValidationError.i18nParams,
    );

    this.state$.pipe(take(1)).subscribe((state) => {
      this.store.commit({
        ...state,
        showXlsImportErrorMessage: true,
        errorMessage: errorMessage,
        loadingData: false,
      });
    });
    throwError(() => httpError);
  }

  private handleOtherHttpErrors(httpError: any) {
    let errorMessage = this.translateService.instant('sqtm-core.generic.label.exception.message');

    // The HTTP 413 Content Too Large client error response status code indicates that
    // the request entity was larger than limits defined by server. This error typically happens
    // if you attempt to upload an overly large file
    if (httpError.status === 413) {
      errorMessage = this.translateService.instant(
        'sqtm-core.exception.import-size-limit-exceeded',
        [httpError.error.maxUploadError.maxSize],
      );
    }

    this.state$.pipe(take(1)).subscribe((state) => {
      this.store.commit({
        ...state,
        showXlsImportErrorMessage: true,
        errorMessage: errorMessage,
        loadingData: false,
      });
    });
    throwError(() => httpError);
  }

  private changeCurrentStep(
    state: ImportTestCaseState,
    step: ImportTestCaseSteps,
  ): ImportTestCaseState {
    const newState = { ...state, currentStep: step };
    this.store.commit(newState);
    return newState;
  }

  changeFormat(value: TestCaseImportFileFormatKeys) {
    this.store.state$
      .pipe(
        take(1),
        map((state: ImportTestCaseState) => {
          return { ...state, format: value };
        }),
      )
      .subscribe((state) => this.store.commit(state));
  }

  saveFile(file: File) {
    this.store.state$
      .pipe(
        take(1),
        map((state: ImportTestCaseState) => {
          return { ...state, file };
        }),
      )
      .subscribe((state) => this.store.commit(state));
  }

  changeEncoding(value: FileEncodingKeys) {
    this.store.state$
      .pipe(
        take(1),
        map((state: ImportTestCaseState) => {
          return { ...state, encoding: value };
        }),
      )
      .subscribe((state) => this.store.commit(state));
  }

  changeSelectedProject(value: number) {
    this.store.state$
      .pipe(
        take(1),
        map((state: ImportTestCaseState) => {
          return { ...state, selectedProject: value };
        }),
      )
      .subscribe((state) => this.store.commit(state));
  }

  getDownLoadReportUrl(reportUrl: string) {
    return `${this.restService.backendRootUrl}${reportUrl}`;
  }

  private beginAsync(state: ImportTestCaseState) {
    const nextState = { ...state, loadingData: true };
    this.store.commit(nextState);
    return state;
  }

  private endAsync(state: ImportTestCaseState) {
    const nextState = { ...state, loadingData: false };
    this.store.commit(nextState);
    return state;
  }
}
