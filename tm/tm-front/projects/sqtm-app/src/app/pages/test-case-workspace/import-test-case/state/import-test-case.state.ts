import { ProjectData, TestCaseXlsReport } from 'sqtm-core';

export class ImportTestCaseState {
  currentStep: ImportTestCaseSteps;
  format: TestCaseImportFileFormatKeys;
  file: File;
  encoding: FileEncodingKeys;
  projects: ProjectData[];
  selectedProject: number;
  simulationReport: TestCaseXlsReport;
  xlsReport: TestCaseXlsReport;
  loadingData: boolean;
  importFailed: boolean;
  showXlsImportErrorMessage: boolean;
  errorMessage: string;
}

export class ImportSummary {
  total: number;
  success: number;
  renamed: number;
  modified: number;
  failures: number;
  rejected: number;
  milestoneFailures: number;
  milestoneNotActivatedFailures: number;
}

export function initialImportTestCaseState(): Readonly<ImportTestCaseState> {
  return {
    currentStep: 'CONFIGURATION',
    format: 'XLS',
    file: null,
    encoding: 'WINDOWS',
    projects: [],
    selectedProject: null,
    simulationReport: null,
    xlsReport: null,
    loadingData: false,
    importFailed: false,
    showXlsImportErrorMessage: false,
    errorMessage: '',
  };
}

export class ImportTestCaseComponentData extends ImportTestCaseState {}

export type FileFormat<K extends string> = { [id in K]: FileFormatItem };

export interface FileFormatItem {
  id: string;
  i18nKey: string;
  value: string;
}

export type ImportTestCaseSteps =
  | 'CONFIGURATION'
  | 'SIMULATION_REPORT'
  | 'CONFIRMATION'
  | 'CONFIRMATION_REPORT';

export type TestCaseImportFileFormatKeys = 'XLS';

export const TestCaseFileFormat: FileFormat<TestCaseImportFileFormatKeys> = {
  XLS: { id: 'XLS', i18nKey: 'sqtm-core.generic.label.file.format.xls', value: 'xls' },
};

export type FileEncodingKeys = 'WINDOWS';

export const FileEncoding = {
  WINDOWS: {
    id: 'WINDOWS',
    value: 'Cp858',
    i18nKey: 'sqtm-core.test-case-workspace.dialog.field.encoding.windows',
  },
  UTF_8: {
    id: 'UTF_8',
    value: 'UTF8',
    i18nKey: 'sqtm-core.test-case-workspace.dialog.field.encoding.utf-8',
  },
};

export const XLS_TYPE =
  'application/vnd.ms-excel,application/vnd.ms-excel.sheet.macroEnabled.12, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
