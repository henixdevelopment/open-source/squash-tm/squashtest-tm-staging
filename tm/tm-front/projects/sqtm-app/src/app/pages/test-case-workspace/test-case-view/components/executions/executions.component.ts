import { AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import {
  convertSqtmLiterals,
  executionModeColumn,
  executionStatusColumn,
  DataRow,
  Fixed,
  grid,
  GridColumnId,
  GridDefinition,
  GridService,
  gridServiceFactory,
  Limited,
  numericColumn,
  ProjectDataMap,
  ReferentialDataService,
  RestService,
  Sort,
  textColumn,
} from 'sqtm-core';
import { TCW_EXECUTION_TABLE, TCW_EXECUTION_TABLE_CONF } from '../../test-case-view.constant';
import { executionOrderColumn } from '../cell-renderers/execution-order/execution-order.component';
import { executionLastExecutedOn } from '../cell-renderers/execution-last-executed-on/execution-last-executed-on.component';
import { TestCaseViewService } from '../../service/test-case-view.service';
import { take } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

export function convertExecPlanTypeLiterals(translateService: TranslateService) {
  return (literals: Partial<DataRow>[], projectDataMap: ProjectDataMap) => {
    return convertSqtmLiterals(
      literals.map((literal) => {
        return convertExecPlanTypeLiteral(literal, translateService);
      }),
      projectDataMap,
    );
  };
}

export function convertExecPlanTypeLiteral(
  literal: Partial<DataRow>,
  translateService: TranslateService,
) {
  literal.data.execPlanType = translateService.instant(
    'sqtm-core.entity.execution-plan.type.' + literal.data.execPlanType,
  );
  return literal;
}

export function tcwExecutionsTableDefinition(translateService: TranslateService): GridDefinition {
  return grid('test-case-view-execution')
    .withColumns([
      textColumn(GridColumnId.executionPath)
        .withI18nKey('sqtm-core.generic.label.path')
        .changeWidthCalculationStrategy(new Limited(600, 100, 900)),
      textColumn(GridColumnId.execPlanType)
        .withI18nKey('sqtm-core.entity.execution-plan.type.label.short')
        .changeWidthCalculationStrategy(new Fixed(105)),
      executionOrderColumn(GridColumnId.executionOrder)
        .withI18nKey('sqtm-core.entity.execution.label.short-dot')
        .changeWidthCalculationStrategy(new Fixed(50))
        .withTitleI18nKey('sqtm-core.entity.execution.label.singular')
        .withHeaderPosition('center'),
      executionModeColumn(GridColumnId.inferredExecutionMode)
        .withI18nKey('sqtm-core.entity.execution.mode.label')
        .changeWidthCalculationStrategy(new Fixed(80)),
      textColumn(GridColumnId.datasetLabel)
        .withI18nKey('sqtm-core.entity.dataset.label.short')
        .changeWidthCalculationStrategy(new Limited(150))
        .withTitleI18nKey('sqtm-core.entity.dataset.label.singular')
        .withVisibility(false),
      executionStatusColumn(GridColumnId.executionStatus)
        .withI18nKey('sqtm-core.entity.execution.status.label')
        .changeWidthCalculationStrategy(new Fixed(80))
        .withHeaderPosition('center'),
      textColumn(GridColumnId.lastExecutedBy)
        .withI18nKey('sqtm-core.entity.execution.last-executed-by.label')
        .changeWidthCalculationStrategy(new Limited(150)),
      executionLastExecutedOn(GridColumnId.lastExecutedOn)
        .withI18nKey('sqtm-core.entity.execution.last-executed-on.label')
        .changeWidthCalculationStrategy(new Fixed(100)),
      numericColumn(GridColumnId.nbIssues)
        .withI18nKey('sqtm-core.entity.issue.label.short')
        .withTitleI18nKey('sqtm-core.entity.issue.label.count')
        .changeWidthCalculationStrategy(new Fixed(50))
        .withHeaderPosition('center'),
    ])
    .server()
    .withInitialSortedColumns([
      {
        id: GridColumnId.lastExecutedOn,
        sort: Sort.DESC,
      },
    ])
    .disableRightToolBar()
    .withRowHeight(35)
    .withRowConverter(convertExecPlanTypeLiterals(translateService))
    .build();
}

@Component({
  selector: 'sqtm-app-executions',
  template: `
    <div class="flex-column full-height full-width p-r-15 overflow-y-hidden">
      <div class="sqtm-large-grid-container-title">
        {{ 'sqtm-core.test-case-workspace.title.executions' | translate }}
      </div>
      <div class="sqtm-large-grid-container full-width full-height p-15 m-b-10">
        <sqtm-core-grid></sqtm-core-grid>
      </div>
    </div>
  `,
  styleUrls: ['./executions.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: TCW_EXECUTION_TABLE_CONF,
      useFactory: tcwExecutionsTableDefinition,
      deps: [TranslateService],
    },
    {
      provide: TCW_EXECUTION_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, TCW_EXECUTION_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: TCW_EXECUTION_TABLE,
    },
  ],
})
export class ExecutionsComponent implements AfterViewInit, OnDestroy {
  constructor(
    private testCaseViewService: TestCaseViewService,
    private gridService: GridService,
  ) {}

  ngAfterViewInit(): void {
    this.testCaseViewService.componentData$.pipe(take(1)).subscribe((componentData) => {
      if (componentData.testCase.datasets.ids.length > 0) {
        this.gridService.setColumnVisibility(GridColumnId.datasetLabel, true);
      }
      this.gridService.setServerUrl([`test-case/${componentData.testCase.id}/executions`]);
    });
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }
}
