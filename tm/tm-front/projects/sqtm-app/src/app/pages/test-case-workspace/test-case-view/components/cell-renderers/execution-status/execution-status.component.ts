import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-execution-status',
  template: `
    @if (row) {
      <div class="full-width full-height flex-column">
        <sqtm-core-execution-status
          style="margin: auto; width: 80%"
          [status]="row.data[columnDisplay.id]"
        ></sqtm-core-execution-status>
      </div>
    }
  `,
  styleUrls: ['./execution-status.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExecutionStatusComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }
}

export function executionStatusColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(ExecutionStatusComponent);
}
