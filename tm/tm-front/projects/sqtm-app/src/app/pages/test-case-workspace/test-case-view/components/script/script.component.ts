import { ChangeDetectionStrategy, Component } from '@angular/core';
import { TestCaseViewService } from '../../service/test-case-view.service';
import { Observable } from 'rxjs';
import { TestCaseViewComponentData } from '../../containers/test-case-view/test-case-view.component';
import { ActionErrorDisplayService, DialogService } from 'sqtm-core';
import { catchError, map } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-script',
  templateUrl: './script.component.html',
  styleUrls: ['./script.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScriptComponent {
  testCaseIsEditable$: Observable<boolean>;
  componentData$: Observable<TestCaseViewComponentData>;

  constructor(
    private testCaseViewService: TestCaseViewService,
    private actionErrorService: ActionErrorDisplayService,
    private dialogService: DialogService,
  ) {
    this.componentData$ = this.testCaseViewService.componentData$;
    this.testCaseIsEditable$ = this.testCaseViewService.componentData$.pipe(
      map(
        (componentData) =>
          componentData.milestonesAllowModification && componentData.permissions.canWrite,
      ),
    );
  }

  confirm(script: string) {
    this.testCaseViewService.updateScript(script);
  }

  checkSyntax(script: string) {
    this.testCaseViewService
      .validateScript(script)
      .pipe(catchError((error) => this.actionErrorService.handleActionError(error)))
      .subscribe(() => {
        this.dialogService.openAlert({
          titleKey: 'sqtm-core.generic.label.information.singular',
          level: 'INFO',
          messageKey: 'sqtm-core.test-case-workspace.gherkin.valid-script',
        });
      });
  }
}
