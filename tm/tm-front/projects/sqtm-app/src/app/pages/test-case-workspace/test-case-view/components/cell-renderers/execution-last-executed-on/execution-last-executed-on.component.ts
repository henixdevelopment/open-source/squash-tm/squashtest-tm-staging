import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  getSupportedBrowserLang,
  GridColumnId,
  GridService,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-execution-last-executed-on',
  template: ` @if (row) {
    <div class="full-width full-height flex-column">
      <span style="margin: auto 0 auto 0">{{
        row.data[columnDisplay.id] | date: 'shortDate' : '' : getBrowserLanguage()
      }}</span>
    </div>
  }`,
  styleUrls: ['./execution-last-executed-on.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExecutionLastExecutedOnComponent extends AbstractCellRendererComponent {
  constructor(
    public gridService: GridService,
    public cdRef: ChangeDetectorRef,
    public translateService: TranslateService,
  ) {
    super(gridService, cdRef);
  }

  getBrowserLanguage() {
    return getSupportedBrowserLang(this.translateService);
  }
}

export function executionLastExecutedOn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(ExecutionLastExecutedOnComponent);
}
