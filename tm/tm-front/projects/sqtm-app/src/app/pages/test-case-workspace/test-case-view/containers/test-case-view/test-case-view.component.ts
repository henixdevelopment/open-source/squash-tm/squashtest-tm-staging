import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewContainerRef,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { concatMap, filter, map, take, takeUntil, tap } from 'rxjs/operators';
import { TestCaseViewService } from '../../service/test-case-view.service';
import {
  AttachmentService,
  CopierService,
  CustomFieldValueService,
  DialogService,
  DragAndDropService,
  EntityPathHeaderService,
  EntityRowReference,
  EntityViewAttachmentHelperService,
  EntityViewComponentData,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  GenericEntityViewService,
  gridServiceFactory,
  LocalPersistenceService,
  MilestoneModeData,
  ReferentialDataService,
  RestService,
  SquashTmDataRowType,
  TestCasePermissions,
  TestCaseService,
  TestStepService,
  WorkspaceWithTreeComponent,
  RichTextAttachmentDelegate,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { TestCaseState } from '../../state/test-case.state';
import {
  TCW_CALLED_TC_TABLE,
  TCW_CALLED_TC_TABLE_CONF,
  TCW_COVERAGE_TABLE,
  TCW_COVERAGE_TABLE_CONF,
  TCW_DATASET_TABLE,
  TCW_DATASET_TABLE_CONF,
} from '../../test-case-view.constant';
import { tcwCoverageTableDefinition } from '../../components/coverage-table/coverage-table.component';

import { tcwDatasetsTableDefinition } from '../../components/datasets-table/datasets-table.component';
import { TestStepViewService } from '../../service/test-step-view.service';
import { tcwCalledTCTableDefiniton } from '../../components/called-test-case/called-test-case.component';
import { AbstractTestCaseViewComponent } from '../abstract-test-case-view.component';
import { combineLatest } from 'rxjs';
import {
  NewTestCaseVersionDialogConfiguration,
  NewTestCaseVersionDialogResult,
} from '../../components/dialog/new-version-dialog/new-test-case-version-dialog.configuration';

@Component({
  selector: 'sqtm-app-test-case-view',
  templateUrl: './test-case-view.component.html',
  styleUrls: ['./test-case-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: TCW_COVERAGE_TABLE_CONF,
      useFactory: tcwCoverageTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: TCW_COVERAGE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, TCW_COVERAGE_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: TCW_DATASET_TABLE_CONF,
      useFactory: tcwDatasetsTableDefinition,
    },
    {
      provide: TCW_DATASET_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, TCW_DATASET_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: TCW_CALLED_TC_TABLE_CONF,
      useFactory: tcwCalledTCTableDefiniton,
      deps: [LocalPersistenceService],
    },
    {
      provide: TCW_CALLED_TC_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, TCW_CALLED_TC_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: TestStepViewService,
      useClass: TestStepViewService,
      deps: [
        TestStepService,
        EntityViewAttachmentHelperService,
        EntityViewCustomFieldHelperService,
        ReferentialDataService,
        CopierService,
      ],
    },
    {
      provide: TestCaseViewService,
      useClass: TestCaseViewService,
      deps: [
        RestService,
        ReferentialDataService,
        AttachmentService,
        TranslateService,
        TestCaseService,
        CustomFieldValueService,
        TestStepViewService,
        EntityViewAttachmentHelperService,
        EntityViewCustomFieldHelperService,
        TCW_COVERAGE_TABLE,
        TCW_DATASET_TABLE,
        TCW_CALLED_TC_TABLE,
        LocalPersistenceService,
      ],
    },
    {
      provide: EntityViewService,
      useExisting: TestCaseViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: TestCaseViewService,
    },
    {
      provide: RichTextAttachmentDelegate,
      useExisting: TestCaseViewService,
    },
  ],
})
export class TestCaseViewComponent
  extends AbstractTestCaseViewComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  showFoldAndCloseButtons = false;
  showFoldButtonOnly = true;

  @Input()
  testCaseViewComponentData: TestCaseViewComponentData;

  constructor(
    route: ActivatedRoute,
    router: Router,
    testCaseViewService: TestCaseViewService,
    referentialDataService: ReferentialDataService,
    cdRef: ChangeDetectorRef,
    translateService: TranslateService,
    dndService: DragAndDropService,
    renderer: Renderer2,
    public vcr: ViewContainerRef,
    public dialogService: DialogService,
    public workspaceWithTree: WorkspaceWithTreeComponent,
    private entityPathHeaderService: EntityPathHeaderService,
  ) {
    super(
      route,
      router,
      testCaseViewService,
      referentialDataService,
      cdRef,
      translateService,
      dndService,
      renderer,
      vcr,
      dialogService,
    );
  }

  ngOnInit() {
    super.ngOnInit();
    this.initializeTreeSynchronisation();
    this.initializeRefreshPathObserver();
  }

  private initializeTreeSynchronisation() {
    this.testCaseViewService.simpleAttributeRequiringRefresh = [
      'name',
      'reference',
      'importance',
      'importanceAuto',
      'status',
      'nature',
    ];
    this.testCaseViewService.externalRefreshRequired$
      .pipe(
        takeUntil(this.unsub$),
        map(({ id }) => new EntityRowReference(id, SquashTmDataRowType.TestCase).asString()),
      )
      .subscribe((identifier) => this.workspaceWithTree.requireNodeRefresh([identifier]));
  }

  private initializeRefreshPathObserver() {
    this.entityPathHeaderService.refreshPath$
      .pipe(takeUntil(this.unsub$))
      .subscribe(() => this.testCaseViewService.updateEntityPath());
  }

  createNewTestCaseVersion() {
    combineLatest([this.componentData$, this.referentialDataService.milestoneModeData$])
      .pipe(
        take(1),
        filter<[TestCaseViewComponentData, MilestoneModeData]>(
          (data) => data[1].selectedMilestone.status !== 'LOCKED',
        ),
        concatMap(
          ([componentData, milestoneData]: [TestCaseViewComponentData, MilestoneModeData]) => {
            const configuration = this.buildNewVersionDialogConfiguration(
              componentData,
              milestoneData,
            );
            return this.dialogService
              .openDialog<
                NewTestCaseVersionDialogConfiguration,
                NewTestCaseVersionDialogResult
              >(configuration)
              .dialogClosed$.pipe(
                filter((result) => Boolean(result)),
                tap((result: NewTestCaseVersionDialogResult) => {
                  const id = new EntityRowReference(
                    result.newVersionId,
                    SquashTmDataRowType.TestCase,
                  ).asString();
                  this.workspaceWithTree.requireCurrentContainerRefresh(id);
                }),
              );
          },
        ),
      )
      .subscribe();
  }
}

export interface TestCaseViewComponentData
  extends EntityViewComponentData<TestCaseState, 'testCase', TestCasePermissions> {}
