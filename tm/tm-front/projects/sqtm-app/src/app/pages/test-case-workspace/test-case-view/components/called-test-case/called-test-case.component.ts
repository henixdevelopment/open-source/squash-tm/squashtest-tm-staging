import { ChangeDetectionStrategy, Component } from '@angular/core';
import {
  DataRow,
  Extendable,
  GridColumnId,
  GridDefinition,
  GridId,
  GridService,
  indexColumn,
  Limited,
  LocalPersistenceService,
  smallGrid,
  textCellWithToolTipColumn,
  textColumn,
  withLinkColumn,
} from 'sqtm-core';
import { TCW_CALLED_TC_TABLE } from '../../test-case-view.constant';
import { calledTestCaseStepOrder } from '../cell-renderers/called-test-case-order-renderer/called-test-case-order-renderer.component';
import { calledTestCaseDatasetNameColumn } from '../cell-renderers/called-test-case-dataset-name/called-test-case-dataset-name.component';

export function tcwCalledTCTableDefiniton(
  localPersistenceService: LocalPersistenceService,
): GridDefinition {
  const urlFunctionToTestCase = (row: DataRow): string => {
    return `/test-case-workspace/test-case/detail/${row.data[GridColumnId.id]}`;
  };

  return smallGrid(GridId.TEST_CASE_VIEW_CALLED_TEST_CASE)
    .withColumns([
      indexColumn().withViewport('leftViewport'),
      textCellWithToolTipColumn(GridColumnId.projectName, 'path')
        .changeWidthCalculationStrategy(new Limited(200))
        .withI18nKey('sqtm-core.entity.project.label.singular'),
      textColumn(GridColumnId.reference)
        .withI18nKey('sqtm-core.entity.generic.reference.label')
        .changeWidthCalculationStrategy(new Limited(120)),
      withLinkColumn(GridColumnId.name, {
        kind: 'link',
        createUrlFunction: urlFunctionToTestCase,
      })
        .changeWidthCalculationStrategy(new Limited(500))
        .withI18nKey('sqtm-core.entity.test-case.label.singular'),
      calledTestCaseDatasetNameColumn(GridColumnId.datasetName)
        .withI18nKey('sqtm-core.entity.dataset.label.short')
        .withTitleI18nKey('sqtm-core.entity.dataset.label.singular')
        .changeWidthCalculationStrategy(new Limited(150)),
      calledTestCaseStepOrder(GridColumnId.stepOrder)
        .withI18nKey('sqtm-core.entity.test-case.test-step.order.short')
        .withTitleI18nKey('sqtm-core.entity.test-case.test-step.order.label')
        .changeWidthCalculationStrategy(new Extendable(60, 1)),
    ])
    .withRowHeight(35)
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
}

@Component({
  selector: 'sqtm-app-called-test-case',
  template: ` <sqtm-core-grid></sqtm-core-grid>`,
  styleUrls: ['./called-test-case.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: TCW_CALLED_TC_TABLE,
    },
  ],
})
export class CalledTestCaseComponent {}
