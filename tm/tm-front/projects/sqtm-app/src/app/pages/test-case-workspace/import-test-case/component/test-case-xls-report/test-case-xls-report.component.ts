import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { TestCaseImportFormatFailure, TestCaseImportLog } from 'sqtm-core';
import { ImportTestCaseService } from '../../services/import-test-case.service';

@Component({
  selector: 'sqtm-app-test-case-xls-report',
  templateUrl: './test-case-xls-report.component.html',
  styleUrls: ['./test-case-xls-report.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestCaseXlsReportComponent {
  @Input()
  templateOk: TestCaseImportLog;

  @Input()
  templateKo: TestCaseImportFormatFailure;

  constructor(private importTestCaseService: ImportTestCaseService) {}

  getFileName(templateOk: TestCaseImportLog) {
    return this.importTestCaseService.getDownLoadReportUrl(templateOk.reportUrl);
  }
}
