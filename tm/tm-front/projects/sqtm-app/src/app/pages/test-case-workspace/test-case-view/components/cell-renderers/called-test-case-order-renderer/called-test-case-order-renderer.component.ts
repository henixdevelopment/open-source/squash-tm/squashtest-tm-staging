import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-order-renderer',
  template: ` @if (row) {
    <div class="full-width full-height flex-column">
      <span class="m-auto-0">
        {{ getOrder() }}
      </span>
    </div>
  }`,
  styleUrls: ['./called-test-case-order-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CalledTestCaseOrderRendererComponent extends AbstractCellRendererComponent {
  constructor(
    private cdr: ChangeDetectorRef,
    private gridService: GridService,
  ) {
    super(gridService, cdr);
  }

  getOrder() {
    return this.row.data[this.columnDisplay.id] + 1;
  }
}

export function calledTestCaseStepOrder(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(CalledTestCaseOrderRendererComponent);
}
