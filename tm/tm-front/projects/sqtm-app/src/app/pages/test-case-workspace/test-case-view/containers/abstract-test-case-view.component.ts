import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { TestCaseViewService } from '../service/test-case-view.service';
import {
  AttachmentDrawerComponent,
  CapsuleInformationData,
  DialogConfiguration,
  DialogService,
  DragAndDropService,
  ExecutionStatus,
  ExecutionStatusKeys,
  isDndDataFromRequirementTreePicker,
  LevelEnumItem,
  ListPanelItem,
  MilestoneModeData,
  REFERENCE_FIELD,
  ReferentialDataService,
  SqtmDragEnterEvent,
  SqtmDragLeaveEvent,
  TestCaseImportanceKeys,
  TestCaseKind,
  TestCaseStatus,
  TestCaseStatusKeys,
  TestCaseWeight,
  Workspaces,
} from 'sqtm-core';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Directive,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { filter, map, take, takeUntil } from 'rxjs/operators';
import { testCaseViewContent } from '../test-case-view.constant';
import { combineLatest, Observable, Subject } from 'rxjs';
import { TestCaseViewComponentData } from './test-case-view/test-case-view.component';
import { testCaseViewLogger } from '../test-case-view.logger';
import { PrintModeService } from '../../../../components/print-mode/services/print-mode.service';
import {
  NewTestCaseVersionDialogConfiguration,
  NewTestCaseVersionDialogResult,
} from '../components/dialog/new-version-dialog/new-test-case-version-dialog.configuration';
import { NewVersionDialogComponent } from '../components/dialog/new-version-dialog/new-version-dialog.component';

const logger = testCaseViewLogger.compose('TestCaseViewComponent');

@Directive()
export abstract class AbstractTestCaseViewComponent implements OnInit, AfterViewInit, OnDestroy {
  requirementWorkspace = Workspaces['requirement-workspace'];
  testCaseWorkspace = Workspaces['test-case-workspace'];

  componentData$: Observable<TestCaseViewComponentData>;
  statusItems: ListPanelItem[] = [];
  importanceItems: ListPanelItem[] = [];
  status = [];

  @ViewChild(AttachmentDrawerComponent)
  attachmentDrawer: AttachmentDrawerComponent;

  @ViewChild('content', { read: ElementRef })
  content: ElementRef;

  @Input()
  backUrl: string;

  unsub$ = new Subject<void>();

  protected constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    public testCaseViewService: TestCaseViewService,
    public referentialDataService: ReferentialDataService,
    protected cdRef: ChangeDetectorRef,
    public translateService: TranslateService,
    protected dndService: DragAndDropService,
    protected renderer: Renderer2,
    protected vcr: ViewContainerRef,
    protected dialogService: DialogService,
  ) {
    this.componentData$ = this.testCaseViewService.componentData$;
  }

  ngOnInit() {
    logger.debug(`ngOnInit TestCaseView`);
    this.referentialDataService.loaded$
      .pipe(
        takeUntil(this.unsub$),
        filter((loaded) => loaded),
        take(1),
      )
      .subscribe(() => {
        logger.debug(`Loading TestCaseView Data by http request`);
        this.loadData();
        this.initializeDndFromRequirementTreePicker();
      });
    this.initializeStatusPanelItems();
    this.initializeImportancePanelItems();
  }

  ngAfterViewInit(): void {
    this.dndService.dragAndDrop$
      .pipe(
        takeUntil(this.unsub$),
        filter((dnd: boolean) => !dnd && Boolean(this.content)),
      )
      .subscribe(() => this.unmarkAsDropZone());
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('testCaseId')),
      )
      .subscribe((id) => {
        logger.debug(`Loading TestCaseView ${id}`);
        this.testCaseViewService.load(parseInt(id, 10), this.backUrl);
      });
  }

  private initializeDndFromRequirementTreePicker() {
    this.dndService.dragEnter$
      .pipe(
        takeUntil(this.unsub$),
        filter((dragEnterEvent: SqtmDragEnterEvent) => this.shouldShowDndEffect(dragEnterEvent)),
      )
      .subscribe(() => {
        this.markAsDropZone();
      });

    this.dndService.dragLeave$
      .pipe(
        takeUntil(this.unsub$),
        filter((dragLeaveEvent: SqtmDragLeaveEvent) => this.shouldShowDndEffect(dragLeaveEvent)),
      )
      .subscribe(() => {
        this.unmarkAsDropZone();
      });
  }

  private shouldShowDndEffect(dragEnterEvent: SqtmDragEnterEvent | SqtmDragLeaveEvent) {
    return (
      dragEnterEvent.dropTargetId === testCaseViewContent &&
      isDndDataFromRequirementTreePicker(dragEnterEvent)
    );
  }

  private initializeStatusPanelItems() {
    for (const levelEnumKey of Object.keys(TestCaseStatus)) {
      const levelEnumItem = TestCaseStatus[levelEnumKey];
      this.statusItems.push(this.levelEnumItemToListPanelItem(levelEnumItem));
    }
  }
  private initializeImportancePanelItems() {
    for (const levelEnumKey of Object.keys(TestCaseWeight)) {
      const levelEnumItem = TestCaseWeight[levelEnumKey];
      this.importanceItems.push(this.levelEnumItemToListPanelItem(levelEnumItem));
    }
  }

  private levelEnumItemToListPanelItem(levelEnumItem: LevelEnumItem<any>): ListPanelItem {
    return {
      id: levelEnumItem.id,
      label: this.translateService.instant(levelEnumItem.i18nKey),
    };
  }

  toggleAttachmentPanel() {
    this.attachmentDrawer.open();
  }

  private markAsDropZone() {
    this.renderer.addClass(this.content.nativeElement, 'sqtm-core-border-current-workspace-color');
  }

  private unmarkAsDropZone() {
    this.renderer.removeClass(
      this.content.nativeElement,
      'sqtm-core-border-current-workspace-color',
    );
  }

  closeRequirementPicker() {
    this.testCaseViewService.closeRequirementTreePicker();
  }

  closeTestCasePickerDrawer() {
    this.testCaseViewService.closeTestCaseTreePicker();
  }

  getStatusInformationData(statusKey: TestCaseStatusKeys) {
    const testCaseStatus = TestCaseStatus[statusKey];
    const informationDisplayData: CapsuleInformationData = {
      color: testCaseStatus.color,
      titleI18nKey: 'sqtm-core.entity.generic.status.label',
      icon: testCaseStatus.icon,
      id: testCaseStatus.id,
      labelI18nKey: testCaseStatus.i18nKey,
    };

    return informationDisplayData;
  }

  getImportanceInformationData(importanceKey: TestCaseImportanceKeys) {
    const testCaseImportance = TestCaseWeight[importanceKey];
    const informationDisplayData: CapsuleInformationData = {
      color: testCaseImportance.color,
      titleI18nKey: 'sqtm-core.entity.test-case.importance.label',
      icon: testCaseImportance.icon,
      id: testCaseImportance.id,
      labelI18nKey: testCaseImportance.i18nKey,
    };

    return informationDisplayData;
  }

  getLastExecutionInformationData(executionStatusKey: ExecutionStatusKeys): CapsuleInformationData {
    const isExploratory =
      this.testCaseViewService.getSnapshot().testCase.kind === TestCaseKind.EXPLORATORY.id;
    const executionStatus = ExecutionStatus[executionStatusKey];
    const executionInformation: CapsuleInformationData = {
      id: executionStatusKey,
      titleI18nKey: isExploratory
        ? 'sqtm-core.entity.test-case.capsule.last-session-status'
        : 'sqtm-core.entity.test-case.last-execution.label',
    };

    if (executionStatus != null) {
      executionInformation.labelI18nKey = executionStatus.i18nKey;
      executionInformation.color = executionStatus.color;
    } else {
      executionInformation.labelI18nKey = isExploratory
        ? 'sqtm-core.entity.test-case.capsule.no-session'
        : 'sqtm-core.generic.label.no-execution';
    }

    return executionInformation;
  }

  getDraftedByAiInformationData() {
    const draftedByAiCapsule: CapsuleInformationData = {
      id: 'draftedByAi',
      titleI18nKey: 'sqtm-core.test-case-workspace.artificial-intelligence.capsule.drafted-by-ai',
      labelI18nKey: 'sqtm-core.test-case-workspace.artificial-intelligence.capsule.drafted-by-ai',
      color: '#3a2654',
    };
    return draftedByAiCapsule;
  }

  isMilestoneModeEnabled() {
    let isMilestoneModeEnabled = false;
    this.referentialDataService.milestoneModeData$
      .pipe(take(1))
      .subscribe((milestoneModeData: MilestoneModeData) => {
        isMilestoneModeEnabled = milestoneModeData.milestoneModeEnabled;
      });

    return isMilestoneModeEnabled;
  }

  openInPrintMode(componentData: TestCaseViewComponentData): void {
    PrintModeService.openPrintTestCaseWindow(componentData.testCase.id);
  }

  createNewTestCaseVersion() {
    combineLatest([this.componentData$, this.referentialDataService.milestoneModeData$])
      .pipe(take(1))
      .subscribe(
        ([componentData, milestoneData]: [TestCaseViewComponentData, MilestoneModeData]) => {
          const configuration = this.buildNewVersionDialogConfiguration(
            componentData,
            milestoneData,
          );
          this.dialogService
            .openDialog<NewTestCaseVersionDialogConfiguration, NewTestCaseVersionDialogResult>(
              configuration,
            )
            .dialogClosed$.pipe(filter((result) => Boolean(result)))
            .subscribe();
        },
      );
  }

  protected buildNewVersionDialogConfiguration(
    componentData: TestCaseViewComponentData,
    milestoneData: MilestoneModeData,
  ) {
    const name = `${componentData.testCase.name}-${milestoneData.selectedMilestone.label}`;
    const reference = componentData.testCase.reference;
    const description = componentData.testCase.description;
    const tcKind = componentData.testCase.kind;
    const configuration: DialogConfiguration<NewTestCaseVersionDialogConfiguration> = {
      viewContainerReference: this.vcr,
      component: NewVersionDialogComponent,
      id: 'new-test-case-version',
      width: 800,
      data: {
        titleKey: 'sqtm-core.generic.label.add-new-version',
        id: 'new-test-case-version',
        originalTestCaseId: componentData.testCase.id,
        projectId: componentData.projectData.id,
        optionalTextFields: [{ ...REFERENCE_FIELD, defaultValue: reference }],
        defaultValues: {
          name,
          description,
        },
        tcKind,
      },
    };
    return configuration;
  }

  updateTestCaseStatus(status: string) {
    this.testCaseViewService.changeStatus(status);
  }

  updateTestCaseImportance(importance: string) {
    this.testCaseViewService.changeImportance(importance);
  }

  isExploratory(kind: string) {
    return kind === TestCaseKind.EXPLORATORY.id;
  }

  isGherkin(kind: string) {
    return kind === TestCaseKind.GHERKIN.id;
  }

  isStandard(kind: string) {
    return kind === TestCaseKind.STANDARD.id;
  }

  ngOnDestroy(): void {
    this.testCaseViewService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }
}
