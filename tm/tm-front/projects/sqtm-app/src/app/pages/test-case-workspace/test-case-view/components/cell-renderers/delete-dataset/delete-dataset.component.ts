import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractDeleteCellRenderer,
  ColumnDefinitionBuilder,
  GridColumnId,
  ConfirmDeleteLevel,
  DialogService,
  GridService,
} from 'sqtm-core';
import { TestCaseViewService } from '../../../service/test-case-view.service';

@Component({
  selector: 'sqtm-app-delete-dataset',
  template: ` <sqtm-core-delete-icon
    [show]="gridDisplay.allowModifications"
    (delete)="showDeleteConfirm()"
  >
  </sqtm-core-delete-icon>`,
  styleUrls: ['./delete-dataset.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteDatasetComponent extends AbstractDeleteCellRenderer {
  constructor(
    grid: GridService,
    cdr: ChangeDetectorRef,
    dialogService: DialogService,
    private testCaseViewService: TestCaseViewService,
  ) {
    super(grid, cdr, dialogService);
  }

  protected getTitleKey(): string {
    return 'sqtm-core.test-case-workspace.dialog.title.remove-dataset';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.test-case-workspace.dialog.message.remove-dataset';
  }

  protected getLevel(): ConfirmDeleteLevel {
    return 'DANGER';
  }

  protected doDelete(): any {
    this.testCaseViewService.removeDataSet(this.row.data[GridColumnId.id]);
  }
}

export function deleteDataSetColumn(): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(GridColumnId.delete).withRenderer(DeleteDatasetComponent);
}
