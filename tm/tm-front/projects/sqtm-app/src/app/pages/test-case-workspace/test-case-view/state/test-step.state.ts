import {
  AttachmentListState,
  CustomFieldValueState,
  RequirementCriticalityKeys,
  SqtmEntityState,
  TestStepKind,
  TestStepModel,
} from 'sqtm-core';
import { createEntityAdapter, EntityState } from '@ngrx/entity';

export interface TestStepsState extends EntityState<TestStepState> {
  selectedStepIds: number[];
  // copiedStepIds: number[];
  // Property used to store initial step order when user initiate a move operation by drag and drop
  // If user cancel the drag, the steps order is restored to this value.
  initialStepOrder: number[];
  // Does an internal dragging of steps have been initiated
  draggingSteps: boolean;
  // Does a test case from the test case tree is actually being dragged over the step view
  draggingTestCase: boolean;
  // Does a requirement from the requirement tree picker is actually being dragged over the step view
  draggingRequirement: boolean;
  // Should we show placeholder during dragging. It's different of the draggingSteps property because as specified we should be able to
  // interrupt current dnd if user pointer leave the step component and retrieve it when user come back...
  showPlaceHolder: boolean;
  // Current target of the dnd. During dnd hover phase, the drop target will be updated
  // If a drop occurs, the steps will be dropped above this target, even if the drop event is made on the background component
  // or the placeholder component
  currentDndTargetId: number;
  // Should the prerequisite panel which is part of step-view is extended
  extendedPrerequisite: boolean;
  // scrollTop of the step view, to restore scroll when user is navigating between steps and other screens of the same test case.
  scrollTop: number;
}

/**
 * State for one test-step client side.
 * It's a TestStepModel with :
 *  - Optimisations for lists (attachments, custom field values) represented as EntityState.
 *  - Some uiState (aka selection, expended state...)
 */
export interface TestStepState {
  // id is unique across ActionStep and CallStep because PKs are shared in database.
  id: number;
  projectId: number;
  stepOrder: number;
  // Does this step is shown as full view or reduced view
  extended: boolean;
  readonly kind: TestStepKind;
}

export interface ActionStepState extends TestStepState, SqtmEntityState {
  readonly kind: 'action-step';
  action: string;
  expectedResult: string;
  attachmentList: AttachmentListState;
  customFieldValues: CustomFieldValueState;
}

export interface CallStepState extends TestStepState {
  readonly kind: 'call-step';
  calledTcId: number;
  calledTcName: string;
  // we don't need a full EntityState here, because no updates are performed. Just an iteration on the array for displaying called steps
  calledTestCaseSteps: TestStepModel[];
  delegateParam: boolean;
  calledDatasetId: number;
  calledDatasetName: string;
}

export interface KeywordStepState extends TestStepState {
  readonly kind: 'keyword-step';
  keyword: string;
  action: string;
  styledAction: string;
  actionWordId: number;
  datatable: string;
  docstring: string;
  comment: string;
}

export interface CallStepPlaceholderState extends TestStepState {
  readonly kind: 'dnd-call-step-placeholder';
}

export interface CreateActionStepFormState extends TestStepState {
  readonly kind: 'create-action-step-form';
}

/**
 * View Data for step. It's the TestStepState with all data required to render injected inside
 * aka step coverages, cuf configuration... whatever is needed to render the view.
 */
export interface TestStepView {
  // Id is unique across ActionStep and CallStep because PKs are shared in database.
  id: number;
  testCaseId: number;
  projectId: number;
  stepOrder: number;
  // Does this step is selected
  selected: boolean;
  // Does at least on step (this one or another) is selected
  activeSelection: boolean;
  // Does at least one step is currently dragged
  draggingStep: boolean;
  showPlaceHolder: boolean;
  // Does this step is shown as full view or reduced view
  extended: boolean;
  readonly kind: TestStepKind;
}

export interface ActionStepView extends TestStepView {
  readonly kind: 'action-step';
  action: string;
  expectedResult: string;
  attachmentList: AttachmentListState;
  customFieldValues: CustomFieldValueState;
  coverages: StepCoverageView[];
  // Does this step is actually the target of a dnd operation to add a coverage
  coverageDndTarget: boolean;
}

export interface CallStepView extends TestStepView {
  readonly kind: 'call-step';
  calledTcId: number;
  calledTcName: string;
  calledTestCaseSteps: TestStepModel[];
  delegateParam: boolean;
  calledDatasetId: number;
  calledDatasetName: string;
}

export interface KeywordStepView extends TestStepView {
  readonly kind: 'keyword-step';
  keyword: string;
  actionWordId: number;
  actionWordProjectId: number;
  actionWordLibraryNodeId: number;
  canReadActionWord: boolean;
  action: string;
  styledAction: string;
  datatable: string;
  docstring: string;
  comment: string;
}

export interface CallStepPlaceholderView extends TestStepView {
  readonly kind: 'dnd-call-step-placeholder';
}

export interface CreateActionStepFormView extends TestStepView {
  readonly kind: 'create-action-step-form';
}

export interface StepCoverageView {
  requirementVersionId: number;
  projectName: string;
  reference: string;
  name: string;
  criticality: RequirementCriticalityKeys;
}

export const stepsEntityAdapter = createEntityAdapter<TestStepState>({
  sortComparer: (stepA, stepB) => {
    return stepA.stepOrder - stepB.stepOrder;
  },
});
