import { Injectable } from '@angular/core';
import {
  createStore,
  CustomDashboardModel,
  DataRow,
  EntityScope,
  FavoriteDashboardValue,
  PartyPreferencesService,
  RestService,
  TestCaseStatistics,
} from 'sqtm-core';
import {
  provideInitialTestCaseMultiView,
  TestCaseMultiViewState,
} from '../state/test-case-multi-view.state';
import { map, withLatestFrom } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export class TestCaseMultiSelectionService {
  private store = createStore<TestCaseMultiViewState>(provideInitialTestCaseMultiView());

  componentData$: Observable<TestCaseMultiViewState> = this.store.state$;

  constructor(
    private restService: RestService,
    private partyPreferencesService: PartyPreferencesService,
  ) {}

  init(rows: DataRow[]) {
    const references = rows.map((row) => row.id);
    const scope: EntityScope[] = rows.map((row) => ({
      id: row.id.toString(),
      label: row.data['NAME'],
      projectId: row.projectId,
    }));
    this.restService
      .post(['test-case-workspace-multi-view'], { references })
      .pipe(
        withLatestFrom(this.store.state$),
        map(([response, _state]: [TestCaseMultiSelectionModel, TestCaseMultiViewState]) => {
          return { ...response, scope, generatedDashboardOn: new Date(), dashboardLoaded: true };
        }),
      )
      .subscribe((state) => {
        this.store.commit(state);
      });
  }

  complete() {
    this.store.complete();
  }

  changeDashboardToDisplay(preferenceValue: FavoriteDashboardValue): Observable<void> {
    return this.partyPreferencesService.changeTestCaseWorkspaceFavoriteDashboard(preferenceValue);
  }
}

interface TestCaseMultiSelectionModel {
  statistics: TestCaseStatistics;
  dashboard: CustomDashboardModel;
  generatedDashboardOn: Date;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
}
