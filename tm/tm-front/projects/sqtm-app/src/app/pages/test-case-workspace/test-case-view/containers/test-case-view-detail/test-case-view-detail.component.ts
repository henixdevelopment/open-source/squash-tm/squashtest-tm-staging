import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  Renderer2,
  ViewContainerRef,
} from '@angular/core';
import { AbstractTestCaseViewComponent } from '../abstract-test-case-view.component';
import {
  TCW_CALLED_TC_TABLE,
  TCW_CALLED_TC_TABLE_CONF,
  TCW_COVERAGE_TABLE,
  TCW_COVERAGE_TABLE_CONF,
  TCW_DATASET_TABLE,
  TCW_DATASET_TABLE_CONF,
} from '../../test-case-view.constant';
import { tcwCoverageTableDefinition } from '../../components/coverage-table/coverage-table.component';
import {
  AttachmentService,
  CopierService,
  CustomFieldValueService,
  DialogService,
  DragAndDropService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  GenericEntityViewService,
  gridServiceFactory,
  LocalPersistenceService,
  ReferentialDataService,
  RestService,
  RichTextAttachmentDelegate,
  TestCaseService,
  TestStepService,
} from 'sqtm-core';
import { tcwDatasetsTableDefinition } from '../../components/datasets-table/datasets-table.component';
import { tcwCalledTCTableDefiniton } from '../../components/called-test-case/called-test-case.component';
import { TestStepViewService } from '../../service/test-step-view.service';
import { TestCaseViewService } from '../../service/test-case-view.service';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TestCaseViewComponentData } from '../test-case-view/test-case-view.component';

@Component({
  selector: 'sqtm-app-test-case-view-detail',
  // Warning this component use a common template with TestCaseViewComponent
  // to avoid duplicating a huge template
  templateUrl: '../test-case-view/test-case-view.component.html',
  styleUrls: ['../test-case-view/test-case-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: TCW_COVERAGE_TABLE_CONF,
      useFactory: tcwCoverageTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: TCW_COVERAGE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, TCW_COVERAGE_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: TCW_DATASET_TABLE_CONF,
      useFactory: tcwDatasetsTableDefinition,
    },
    {
      provide: TCW_DATASET_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, TCW_DATASET_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: TCW_CALLED_TC_TABLE_CONF,
      useFactory: tcwCalledTCTableDefiniton,
      deps: [LocalPersistenceService],
    },
    {
      provide: TCW_CALLED_TC_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, TCW_CALLED_TC_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: TestStepViewService,
      useClass: TestStepViewService,
      deps: [
        TestStepService,
        EntityViewAttachmentHelperService,
        EntityViewCustomFieldHelperService,
        ReferentialDataService,
        CopierService,
      ],
    },
    {
      provide: TestCaseViewService,
      useClass: TestCaseViewService,
      deps: [
        RestService,
        ReferentialDataService,
        AttachmentService,
        TranslateService,
        TestCaseService,
        CustomFieldValueService,
        TestStepViewService,
        EntityViewAttachmentHelperService,
        EntityViewCustomFieldHelperService,
        TCW_COVERAGE_TABLE,
        TCW_DATASET_TABLE,
        TCW_CALLED_TC_TABLE,
        LocalPersistenceService,
      ],
    },
    {
      provide: EntityViewService,
      useExisting: TestCaseViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: TestCaseViewService,
    },
    {
      provide: RichTextAttachmentDelegate,
      useExisting: TestCaseViewService,
    },
  ],
})
export class TestCaseViewDetailComponent extends AbstractTestCaseViewComponent implements OnInit {
  @Input()
  showFoldAndCloseButtons = true;

  @Input()
  testCaseViewComponentData: TestCaseViewComponentData;

  constructor(
    route: ActivatedRoute,
    router: Router,
    testCaseViewService: TestCaseViewService,
    referentialDataService: ReferentialDataService,
    cdRef: ChangeDetectorRef,
    translateService: TranslateService,
    dndService: DragAndDropService,
    renderer: Renderer2,
    vcr: ViewContainerRef,
    dialogService: DialogService,
  ) {
    super(
      route,
      router,
      testCaseViewService,
      referentialDataService,
      cdRef,
      translateService,
      dndService,
      renderer,
      vcr,
      dialogService,
    );
  }

  ngOnInit() {
    super.ngOnInit();
  }
}
