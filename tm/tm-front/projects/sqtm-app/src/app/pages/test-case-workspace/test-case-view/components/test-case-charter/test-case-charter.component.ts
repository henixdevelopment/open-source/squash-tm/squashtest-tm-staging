import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { TestCaseViewService } from '../../service/test-case-view.service';
import { catchError, filter, finalize, map, take, takeUntil } from 'rxjs/operators';
import { TestCaseViewComponentData } from '../../containers/test-case-view/test-case-view.component';
import {
  ActionErrorDisplayService,
  EditableRichTextComponent,
  EditableTimeFieldComponent,
  RestService,
} from 'sqtm-core';
import { Observable, Subject } from 'rxjs';
import {
  CHARTER_HELP_DIALOG_HEIGHT,
  CHARTER_HELP_DIALOG_URL,
  CHARTER_HELP_DIALOG_WIDTH,
} from '../dialog/charter-help-dialog/charter-help-dialog.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-test-case-charter',
  templateUrl: './test-case-charter.component.html',
  styleUrls: ['./test-case-charter.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestCaseCharterComponent implements OnDestroy {
  @ViewChild('charterField', { read: EditableRichTextComponent })
  charterField: EditableRichTextComponent;

  @ViewChild('durationField', { read: EditableTimeFieldComponent })
  durationField: EditableTimeFieldComponent;

  extendedCharter = true;

  sessionDuration$: Observable<number>;

  unsub$ = new Subject<void>();

  private _executionWindow: Window = null;

  defaultCharter: string;

  constructor(
    public readonly testCaseViewService: TestCaseViewService,
    public readonly cdRef: ChangeDetectorRef,
    public readonly actionErrorDisplayService: ActionErrorDisplayService,
    public readonly translateService: TranslateService,
    public readonly restService: RestService,
  ) {
    this.sessionDuration$ = testCaseViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      map((componentData) => componentData.testCase.sessionDuration),
    );

    this.defaultCharter = this.translateService.instant(
      'sqtm-core.test-case-workspace.charter.default-value',
    );
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  editCollapsedCharter() {
    this.toggleCharter();

    this.testCaseViewService.componentData$
      .pipe(
        take(1),
        filter((componentData) => this.isEditable(componentData)),
      )
      .subscribe(() => {
        this.cdRef.detectChanges();
        this.charterField.enableEditMode();
      });
  }

  isEditable(componentData: TestCaseViewComponentData): boolean {
    return componentData.permissions.canWrite && componentData.milestonesAllowModification;
  }

  toggleCharter() {
    this.extendedCharter = !this.extendedCharter;
  }

  updateCharter(charter: string) {
    if (this.charterField.initialValue !== charter) {
      this.testCaseViewService
        .changeCharter(charter)
        .pipe(
          take(1),
          catchError((err) => {
            this.charterField.endAsync();
            return this.actionErrorDisplayService.handleActionError(err);
          }),
          finalize(() => {
            this.charterField.endAsync();
            this.charterField.disableEditMode();
          }),
        )
        .subscribe();
    } else {
      this.charterField.endAsync();
      this.charterField.disableEditMode();
    }
  }

  updateSessionDuration(sessionDuration: number) {
    if (this.durationField.value !== sessionDuration) {
      this.testCaseViewService
        .changeSessionDuration(sessionDuration)
        .pipe(
          take(1),
          catchError((err) => {
            this.durationField.endAsync();
            return this.actionErrorDisplayService.handleActionError(err);
          }),
        )
        .subscribe();
    } else {
      this.durationField.endAsync();
      this.durationField.disableEditMode();
    }
  }

  canEdit(componentData: TestCaseViewComponentData) {
    return componentData.permissions.canWrite && componentData.milestonesAllowModification;
  }

  openHelpWindow() {
    const url = `test-case-workspace/${CHARTER_HELP_DIALOG_URL}`;

    if (this._executionWindow) {
      this._executionWindow.close();
    }

    this._executionWindow = window.open(
      `${window.location.origin}${this.restService.backendContextPath}${url}`,
      '',
      `width=${CHARTER_HELP_DIALOG_WIDTH},height=${CHARTER_HELP_DIALOG_HEIGHT}`,
    );
  }

  getCharter(componentData: TestCaseViewComponentData): string {
    if (componentData.testCase.charter?.length > 0) {
      return componentData.testCase.charter;
    }

    return this.defaultCharter;
  }
}
