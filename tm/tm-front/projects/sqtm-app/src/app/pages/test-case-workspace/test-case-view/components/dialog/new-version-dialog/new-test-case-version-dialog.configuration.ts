import { EntityCreationFormData, TestCaseKindKeys } from 'sqtm-core';

export class NewTestCaseVersionDialogConfiguration extends EntityCreationFormData {
  originalTestCaseId: number;
  tcKind: TestCaseKindKeys;
}

export class NewTestCaseVersionDialogResult {
  newVersionId: number;
}
