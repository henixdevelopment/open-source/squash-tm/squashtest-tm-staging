import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  DialogReference,
  FieldValidationError,
  TestCaseParameterOperationReport,
  TestCaseService,
  TextFieldComponent,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { ParameterDialogConfiguration } from './parameter-dialog-configuration';
import { HttpErrorResponse } from '@angular/common/http';

const FIELD_NAME = 'name';
const FIELD_DESCRIPTION = 'description';

@Component({
  selector: 'sqtm-app-parameter-dialog',
  templateUrl: './parameter-dialog.component.html',
  styleUrls: ['./parameter-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ParameterDialogComponent implements OnInit {
  @ViewChild(TextFieldComponent)
  nameField: TextFieldComponent;

  formGroup: FormGroup;

  serverSideValidationErrors: FieldValidationError[] = [];

  namePatternError = false;

  constructor(
    private fb: FormBuilder,
    private dialogReference: DialogReference<
      ParameterDialogConfiguration,
      TestCaseParameterOperationReport
    >,
    private translateService: TranslateService,
    private testCaseService: TestCaseService,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit(): void {
    this.initializeFormGroup();
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      name: this.fb.control('', [
        Validators.pattern('^[A-Za-z0-9_-]{1,255}$'),
        Validators.required,
        Validators.maxLength(255),
      ]),
      description: this.fb.control(''),
    });
  }

  addAnotherParam() {
    this.addParam(true);
  }

  addParam(addAnother?: boolean) {
    this.namePatternError = false;
    if (this.formGroup.status === 'VALID') {
      const param = {
        name: this.formGroup.get(FIELD_NAME).value,
        description: this.formGroup.get(FIELD_DESCRIPTION).value,
      };
      this.testCaseService.persistParameter(this.dialogReference.data.testCaseId, param).subscribe({
        next: (response) => {
          this.success(response, addAnother);
        },
        error: (error) => {
          this.handleCreationFailure(error);
        },
      });
    } else {
      this.showClientSideErrors();
    }
  }

  private success(result: TestCaseParameterOperationReport, addAnother: boolean) {
    this.dialogReference.result = result;
    if (addAnother) {
      this.resetForm();
    } else {
      this.dialogReference.close();
    }
  }

  private resetForm() {
    this.formGroup.reset();
    this.initializeFormGroup();
    this.serverSideValidationErrors = [];
    this.cdr.detectChanges();
    this.nameField.grabFocus();
  }

  private showClientSideErrors() {
    const errors = this.formGroup.controls[FIELD_NAME].errors;
    if (errors['pattern'] !== undefined) {
      this.namePatternError = true;
      this.serverSideValidationErrors = [];
    } else {
      this.nameField.showClientSideError();
    }
  }

  private handleCreationFailure(error: HttpErrorResponse) {
    if (error.status === 412) {
      const squashError = error.error.squashTMError;
      if (squashError.kind === 'FIELD_VALIDATION_ERROR') {
        this.serverSideValidationErrors = squashError.fieldValidationErrors;
        this.cdr.markForCheck();
      }
    }
  }
}
