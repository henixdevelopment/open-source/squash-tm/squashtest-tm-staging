import { CreationDialogData } from 'sqtm-core';

export interface ParameterDialogConfiguration extends CreationDialogData {
  testCaseId: number;
}
