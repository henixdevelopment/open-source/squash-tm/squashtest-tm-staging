import { testCaseWorkspaceLogger } from '../test-case-workspace.logger';

export const testCaseFolderViewLogger = testCaseWorkspaceLogger.compose('test-case-folder-view');
