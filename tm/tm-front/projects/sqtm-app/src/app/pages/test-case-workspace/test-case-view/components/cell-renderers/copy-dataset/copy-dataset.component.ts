import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  ViewContainerRef,
} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ActionErrorDisplayService,
  buildDatasetDuplicationTreePickerDialogDefinition,
  DataRow,
  DatasetDuplicationTreePickerDialogConfiguration,
  DatasetParamValue,
  DialogService,
  GridColumnId,
  GridService,
  ReferentialDataService,
  TestCaseParameterOperationReport,
  TestCaseService,
  TreeNodeType,
} from 'sqtm-core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { TestCaseViewComponentData } from '../../../containers/test-case-view/test-case-view.component';
import { TestCaseViewService } from '../../../service/test-case-view.service';
import { catchError, filter, finalize, map, take, takeUntil, tap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-copy-dataset',
  templateUrl: './copy-dataset.component.html',
  styleUrls: ['./copy-dataset.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CopyDatasetComponent extends AbstractCellRendererComponent implements OnDestroy {
  PARAM_COLUMN_PREFIX = 'PARAM_COLUMN_';
  COPY_NAME_SUFFIX_I18N_KEY = 'sqtm-core.test-case-workspace.dataset-table.datasets.copy-suffix';
  MAX_NAME_LENGTH = 255;

  componentData$: Observable<TestCaseViewComponentData>;
  unsub$ = new Subject<void>();

  menuVisible$ = new BehaviorSubject<boolean>(false);

  constructor(
    grid: GridService,
    changeDetectorRef: ChangeDetectorRef,
    private dialogService: DialogService,
    private testCaseViewService: TestCaseViewService,
    private vcr: ViewContainerRef,
    private testCaseService: TestCaseService,
    private actionErrorDisplayService: ActionErrorDisplayService,
    private translateService: TranslateService,
    public referentialDataService: ReferentialDataService,
  ) {
    super(grid, changeDetectorRef);
    this.componentData$ = this.testCaseViewService.componentData$.pipe(takeUntil(this.unsub$));
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  showCopyMenu() {
    this.menuVisible$.next(true);
  }

  canCopy(componentData: TestCaseViewComponentData) {
    return componentData.permissions.canWrite && componentData.milestonesAllowModification;
  }

  copyDataset(componentData: TestCaseViewComponentData) {
    if (this.canCopy(componentData)) {
      this.menuVisible$.next(false);
      const dataset = this.extractParamValuesAndPrepareDataset(componentData);

      this.testCaseViewService
        .copyDataset(dataset)
        .pipe(
          catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
          tap((result: TestCaseParameterOperationReport) =>
            this.testCaseViewService.addDataset(result),
          ),
          finalize(() => this.grid.completeAsyncOperation()),
        )
        .subscribe();
    }
  }

  copyToAnotherTestCase(componentData: TestCaseViewComponentData) {
    this.menuVisible$.next(false);

    const dialogReference = this.dialogService.openDialog<
      DatasetDuplicationTreePickerDialogConfiguration,
      DataRow[]
    >(
      buildDatasetDuplicationTreePickerDialogDefinition(
        'test-case-dataset-duplication-picker-dialog',
        [],
        true,
        componentData.testCase.id,
        TreeNodeType.TEST_CASE,
      ),
    );

    dialogReference.dialogClosed$
      .pipe(
        takeUntil(this.unsub$),
        take(1),
        filter((result) => Boolean(result)),
        tap(() => this.grid.beginAsyncOperation()),
        map((selectedRows: DataRow[]) =>
          this.copyDatasetToOtherTestCases(componentData, selectedRows),
        ),
        finalize(() => this.grid.completeAsyncOperation()),
      )
      .subscribe(() => {
        this.grid.refreshData();
      });
  }

  private extractParamValuesAndPrepareDataset(componentData: TestCaseViewComponentData) {
    const paramValues = [];
    componentData.testCase.parameters.ids.forEach((paramId) => {
      if (this.row.data[this.PARAM_COLUMN_PREFIX + paramId] != null) {
        const parameterValue: DatasetParamValue = this.row.data[this.PARAM_COLUMN_PREFIX + paramId];
        paramValues.push({ paramId: parameterValue.parameterId, value: parameterValue.value });
      }
    });

    const copyNumber = 1;
    const datasetBaseName = this.row.data[GridColumnId.name];
    const suffix = `${this.translateService.instant(this.COPY_NAME_SUFFIX_I18N_KEY)}`;
    const datasetName: string = this.resolveNameConflict(
      componentData,
      datasetBaseName,
      copyNumber,
      suffix,
    );

    return { name: datasetName, paramValues: paramValues };
  }

  private resolveNameConflict(
    componentData: TestCaseViewComponentData,
    datasetBaseName: string,
    copyNumber: number,
    suffix: string,
  ): string {
    let name: string = datasetBaseName + suffix + copyNumber.toString();
    const datasets = Object.values(componentData.testCase.datasets.entities);

    name = this.checkNameLengthAndTruncateIfNecessary(name, suffix);

    if (datasets.find((dataset) => dataset.name === name)) {
      return this.resolveNameConflict(componentData, datasetBaseName, copyNumber + 1, suffix);
    } else {
      return name;
    }
  }

  // truncate dataset name if it exceeds the max character limit when concatenated with copy suffix
  // and version number by splitting into string table with base name and version number
  private checkNameLengthAndTruncateIfNecessary(datasetName: string, suffix: string): string {
    if (datasetName.length > this.MAX_NAME_LENGTH) {
      const sizeDifference = datasetName.length - this.MAX_NAME_LENGTH;
      const splitName: string[] = datasetName.split(suffix);
      const truncatedName = splitName[0].slice(0, this.MAX_NAME_LENGTH - sizeDifference);
      return truncatedName + suffix + splitName[1];
    }
    return datasetName;
  }

  private copyDatasetToOtherTestCases(
    componentData: TestCaseViewComponentData,
    selectedRows: DataRow[],
  ) {
    const sourceDatasetId: number = this.row.data.id;
    const targetTestCaseIds: number[] = selectedRows.map((row) => row.data.TCLN_ID);
    const isCurrentTestCaseSelected =
      selectedRows.filter((row) => row.data.TCLN_ID === componentData.testCase.id).length === 1;

    this.testCaseService
      .copyDataset(sourceDatasetId, componentData.testCase.id, targetTestCaseIds)
      .pipe(
        tap((result: { [key: number]: TestCaseParameterOperationReport }) => {
          if (isCurrentTestCaseSelected) {
            this.testCaseViewService.addParameter(result[componentData.testCase.id]);
          }
        }),
      )
      .subscribe((result: { [key: number]: TestCaseParameterOperationReport }) => {
        this.openDataSetDuplicationRecapDialog(result, targetTestCaseIds);
      });
  }

  private openDataSetDuplicationRecapDialog(
    duplicationResult: { [key: number]: TestCaseParameterOperationReport },
    targetTestCaseIds: number[],
  ) {
    const totalCopied: number = Object.keys(duplicationResult).length;
    const totalFailed: number = targetTestCaseIds.length - totalCopied;

    this.dialogService.openAlert({
      id: 'dataset-duplication-recap-dialog',
      level: 'INFO',
      messageKey: this.getMessageKey(totalCopied, totalFailed, targetTestCaseIds.length),
      titleKey: `${this.translateService.instant('sqtm-core.test-case-workspace.dialog.title.dataset-duplication')}`,
    });
  }

  private getMessageKey(totalCopied: number, totalFailed: number, totalSelectedTCs: number) {
    if (totalFailed !== 0 && totalFailed < totalSelectedTCs) {
      return `${this.translateService.instant(
        'sqtm-core.test-case-workspace.dialog.dataset-duplication.message.partial-failure',
        [totalSelectedTCs - totalFailed, totalSelectedTCs],
      )}`;
    } else if (totalFailed !== 0 && totalFailed === totalSelectedTCs) {
      return `${this.translateService.instant(
        'sqtm-core.test-case-workspace.dialog.dataset-duplication.message.total-failure',
        [totalSelectedTCs - totalFailed, totalSelectedTCs],
      )}`;
    }

    return `${this.translateService.instant(
      'sqtm-core.test-case-workspace.dialog.dataset-duplication.message.success',
      [totalCopied, totalSelectedTCs],
    )}`;
  }
}
