import { TestBed } from '@angular/core/testing';

import {
  bindMilestonesToTC,
  TestCaseViewService,
  unbindMilestoneToTC,
} from './test-case-view.service';
import {
  AttachmentService,
  AutomationRequest,
  CustomFieldValueService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  Milestone,
  Permissions,
  ReferentialDataService,
  RestService,
  TestCaseModel,
  TestCaseService,
} from 'sqtm-core';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import {
  TCW_COVERAGE_TABLE,
  TCW_DATASET_TABLE,
  TCW_PARAMETER_TABLE,
} from '../test-case-view.constant';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestStepViewService } from './test-step-view.service';
import { TestCaseState } from '../state/test-case.state';
import { provideInitialTestCaseView, TestCaseViewState } from '../state/test-case-view.state';
import { of } from 'rxjs';
import { AppTestingUtilsModule } from '../../../../utils/testing-utils/app-testing-utils.module';

function getInitialTestCaseState(): TestCaseViewState {
  const initialState = { ...provideInitialTestCaseView() };
  initialState.testCase = { milestones: [] } as TestCaseState;
  return initialState;
}

describe('TestCaseViewService', () => {
  const tableMock = jasmine.createSpyObj('tableMock', ['load']);
  const stepViewServiceMock = jasmine.createSpyObj('testStepViewService', [
    'writeAction',
    'updateAction',
    'initializeStepState',
  ]);

  const restServiceMock = {} as RestService;

  const referentialDataService = {} as ReferentialDataService;

  referentialDataService.connectToProjectData = jasmine.createSpy().and.returnValue(
    of({
      permissions: { TEST_CASE: [Permissions.WRITE] },
    }),
  );

  referentialDataService.globalConfiguration$ = of(null);

  const milestones: Milestone[] = [
    {
      id: 1,
      endDate: null,
      status: 'IN_PROGRESS',
      description: 'Description of milestone 1',
      label: 'Milestone1',
      ownerFistName: 'admin',
      ownerLastName: 'admin',
      ownerLogin: 'admin',
      range: 'GLOBAL',
      createdBy: 'cypress',
      createdOn: new Date().toISOString(),
      lastModifiedBy: null,
      lastModifiedOn: null,
    },
    {
      id: 2,
      endDate: null,
      status: 'FINISHED',
      description: 'Description of milestone 2',
      label: 'Milestone2',
      ownerFistName: 'admin',
      ownerLastName: 'admin',
      ownerLogin: 'admin',
      range: 'GLOBAL',
      createdBy: '',
      createdOn: null,
      lastModifiedBy: null,
      lastModifiedOn: null,
    },
  ];

  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, TranslateModule.forRoot()],
      providers: [
        { provide: TCW_COVERAGE_TABLE, useValue: tableMock },
        { provide: TCW_PARAMETER_TABLE, useValue: tableMock },
        { provide: TCW_DATASET_TABLE, useValue: tableMock },
        { provide: TestStepViewService, useValue: stepViewServiceMock },
        { provide: RestService, useValue: restServiceMock },
        {
          provide: TestCaseViewService,
          useClass: TestCaseViewService,
          deps: [
            RestService,
            ReferentialDataService,
            AttachmentService,
            TranslateService,
            TestCaseService,
            CustomFieldValueService,
            TestStepViewService,
            EntityViewAttachmentHelperService,
            EntityViewCustomFieldHelperService,
            TCW_COVERAGE_TABLE,
            TCW_PARAMETER_TABLE,
            TCW_DATASET_TABLE,
          ],
        },
        {
          provide: ReferentialDataService,
          useValue: referentialDataService,
        },
      ],
    }),
  );

  it('should be created', () => {
    const service: TestCaseViewService = TestBed.get(TestCaseViewService);
    expect(service).toBeTruthy();
  });

  it('should bind one milestone', () => {
    let testCaseState = getInitialTestCaseState();
    expect(testCaseState.testCase.milestones.length).toBe(0);

    testCaseState = bindMilestonesToTC(milestones, testCaseState, [1]);
    expect(testCaseState.testCase.milestones.length).toBe(1);
    expect(testCaseState.testCase.milestones[0].label).toBe('Milestone1');

    testCaseState = bindMilestonesToTC(milestones, testCaseState, [2]);
    expect(testCaseState.testCase.milestones.length).toBe(2);
    expect(testCaseState.testCase.milestones[0].label).toBe('Milestone1');
    expect(testCaseState.testCase.milestones[1].label).toBe('Milestone2');
  });

  it('should bind an array of milestones', () => {
    let testCaseState = getInitialTestCaseState();
    testCaseState = bindMilestonesToTC(milestones, testCaseState, [1, 2, 3]);
    expect(testCaseState.testCase.milestones.length).toBe(2);
    expect(testCaseState.testCase.milestones[0].label).toBe('Milestone1');
    expect(testCaseState.testCase.milestones[1].label).toBe('Milestone2');
  });

  it('should unbind milestone by id', () => {
    let testCaseState = { ...getInitialTestCaseState() };
    expect(testCaseState.testCase).not.toBeNull();
    testCaseState = {
      ...testCaseState,
      testCase: { ...testCaseState.testCase, milestones: milestones },
    };
    testCaseState = unbindMilestoneToTC(testCaseState, 1);
    expect(testCaseState.testCase.milestones.length).toBe(1);
    expect(testCaseState.testCase.milestones[0].label).toBe('Milestone2');
  });

  it("should update TC's automation request after changing its status", () => {
    const updatedTestCase: TestCaseModel = {
      automatable: 'Y',
      automationRequest: {
        extender: null,
        id: 1,
        transmittedOn: null,
        priority: null,
        requestStatus: 'TRANSMITTED',
      },
    } as TestCaseModel;

    const testCaseState = { ...getInitialTestCaseState() };
    testCaseState.testCase = {
      id: 1,
      automationRequest: null,
      projectId: 1,
    } as unknown as TestCaseState;

    restServiceMock.post = jasmine.createSpy().and.returnValue(of([updatedTestCase]));
    const service: TestCaseViewService = TestBed.inject(TestCaseViewService);
    service['store'].commit(testCaseState);

    let currentTestCaseViewState = {} as TestCaseViewState;
    service.componentData$.subscribe((testCaseViewState) => {
      currentTestCaseViewState = testCaseViewState;
    });

    service.updateAutomationRequestStatus(testCaseState.testCase.id, 'TRANSMITTED').subscribe();
    expect(currentTestCaseViewState.testCase.automationRequest).toEqual(
      updatedTestCase.automationRequest,
    );
  });

  it('should update test case after changing automatable state', () => {
    const response = {
      extender: null,
      id: 1,
      transmittedOn: null,
      priority: null,
      requestStatus: 'TRANSMITTED',
    } as AutomationRequest;

    const testCaseState = { ...getInitialTestCaseState() };
    testCaseState.testCase = {
      id: 1,
      automationRequest: null,
      projectId: 1,
    } as unknown as TestCaseState;

    restServiceMock.post = jasmine.createSpy().and.returnValue(of(response));
    const service: TestCaseViewService = TestBed.get(TestCaseViewService);
    service['store'].commit(testCaseState);

    let currentTestCaseViewState = {} as TestCaseViewState;
    service.componentData$.subscribe((testCaseViewState) => {
      currentTestCaseViewState = testCaseViewState;
    });

    service.updateAutomatable(testCaseState.testCase.id, 'Y');

    expect(currentTestCaseViewState.testCase.automatable).toEqual('Y');
    expect(currentTestCaseViewState.testCase.automationRequest).toEqual(response);
  });
});
