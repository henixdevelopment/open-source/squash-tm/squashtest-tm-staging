import { InjectionToken } from '@angular/core';
import { GridDefinition, GridService } from 'sqtm-core';

export const TCW_COVERAGE_TABLE_CONF = new InjectionToken<GridDefinition>(
  'Grid config for the coverage table of test case view',
);
export const TCW_COVERAGE_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the coverage table of test case view',
);

export const TCW_COVERAGE_TREE_CONF = new InjectionToken<GridDefinition>(
  'Tree config for the coverage tree of test cas view',
);
export const TCW_COVERAGE_TREE = new InjectionToken<GridDefinition>(
  'Grid service instance for the coverage tree of test cas view',
);

export const TCW_PARAMETER_TABLE_CONF = new InjectionToken<GridDefinition>(
  'Grid config for the parameter table of test case view',
);
export const TCW_PARAMETER_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the parameter table of test case view',
);

export const TCW_DATASET_TABLE_CONF = new InjectionToken<GridDefinition>(
  'Grid config for the dataset table of test case view',
);
export const TCW_DATASET_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the dataset table of test case view',
);

export const TCW_EXECUTION_TABLE_CONF = new InjectionToken<GridDefinition>(
  'Grid config for the execution table of test case view',
);
export const TCW_EXECUTION_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the execution table of test case view',
);

export const TCW_SESSIONS_TABLE_CONF = new InjectionToken<GridDefinition>(
  'Grid config for the execution table of test case view',
);
export const TCW_SESSIONS_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the execution table of test case view',
);

export const TCW_CALLED_TC_TABLE_CONF = new InjectionToken<GridDefinition>(
  'Grid config for the called test case table of test case view',
);
export const TCW_CALLED_TC_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the called test case table of test case view',
);

export const TCW_ISSUE_TABLE_CONF = new InjectionToken<GridDefinition>(
  'Grid config for the issue table of test case view',
);
export const TCW_ISSUE_TABLE = new InjectionToken<GridService>(
  'Grid service instance for the issue table of test case view',
);

/**
 * Id of the placeholder injected into steps to materialize the insertion of a call step by dnd from test-case tree
 */
export const callStepDndPlaceholderId = -1;

/**
 * Id of the form used to create step directly at the good index
 * and no more in a dialog
 */
export const createActionStepFormId = -2;

export const testCaseViewContent = 'test-case-view-content';
