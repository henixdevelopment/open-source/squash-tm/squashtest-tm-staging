import { TestBed } from '@angular/core/testing';

import { TestCaseMultiSelectionService } from './test-case-multi-selection.service';
import { RestService } from 'sqtm-core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppTestingUtilsModule } from '../../../../utils/testing-utils/app-testing-utils.module';

describe('TestCaseMultiSelectionService', () => {
  let service: TestCaseMultiSelectionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule],
      providers: [
        {
          provide: TestCaseMultiSelectionService,
          useClass: TestCaseMultiSelectionService,
          deps: [RestService],
        },
      ],
    });
    service = TestBed.inject(TestCaseMultiSelectionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
