import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-parameter-source-test-case',
  template: `
    @if (columnDisplay && row && row.data['isDelegateParam']) {
      <div class="full-width full-height flex-column">
        <a [routerLink]="['/test-case-workspace/test-case/', row.data['sourceTestCaseId']]">{{
          getSourceTestCaseName()
        }}</a>
      </div>
    }
  `,
  styleUrls: ['./parameter-source-test-case.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ParameterSourceTestCaseComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }

  getSourceTestCaseName() {
    let sourceTestCaseName = `${this.row.data[GridColumnId.sourceTestCaseName]} (${this.row.data[GridColumnId.sourceTestCaseProjectName]})`;
    if (this.row.data[GridColumnId.sourceTestCaseReference]) {
      sourceTestCaseName = `${this.row.data[GridColumnId.sourceTestCaseReference]}-${sourceTestCaseName}`;
    }
    return sourceTestCaseName;
  }
}

export function parameterSourceTestCaseColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(ParameterSourceTestCaseComponent);
}
