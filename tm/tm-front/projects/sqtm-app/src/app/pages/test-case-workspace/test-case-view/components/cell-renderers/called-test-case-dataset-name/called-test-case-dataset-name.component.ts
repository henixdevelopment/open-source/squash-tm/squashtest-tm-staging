import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  GridColumnId,
  GridService,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-called-test-case-dataset-name',
  template: ` @if (columnDisplay && row) {
    <div class="full-width full-height flex-column">
      <span
        class="sqtm-grid-cell-txt-renderer m-auto-0"
        [ngClass]="textClass"
        [class.disabled-row]="row.disabled"
        [class.show-as-filtered-parent]="showAsFilteredParent"
        [sqtmCoreLabelTooltip]="getToolTipText()"
        nz-tooltip
        [nzTooltipTitle]=""
        [nzTooltipPlacement]="'topLeft'"
      >
        {{ getText() }}
      </span>
    </div>
  }`,
  styleUrls: ['./called-test-case-dataset-name.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CalledTestCaseDatasetNameComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    private translateService: TranslateService,
  ) {
    super(grid, cdRef);
  }

  get textClass(): string {
    return 'align-' + this.columnDisplay.contentPosition;
  }

  getToolTipText() {
    const hasDelegatedParameter = this.row.data[GridColumnId.hasDelegatedParameter];
    if (hasDelegatedParameter) {
      return this.translateService.instant(
        'sqtm-core.entity.call-step.dataset.delegate-no-parenthesis',
      );
    } else {
      return this.row.data[this.columnDisplay.id];
    }
  }

  getText() {
    const value = this.row.data[this.columnDisplay.id];
    const hasDelegatedParameter = this.row.data[GridColumnId.hasDelegatedParameter];

    if (hasDelegatedParameter) {
      return this.translateService.instant(
        'sqtm-core.entity.call-step.dataset.delegate-no-parenthesis',
      );
    } else if (value == null || value === '') {
      return '-';
    } else {
      return value;
    }
  }
}

export function calledTestCaseDatasetNameColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(CalledTestCaseDatasetNameComponent);
}
