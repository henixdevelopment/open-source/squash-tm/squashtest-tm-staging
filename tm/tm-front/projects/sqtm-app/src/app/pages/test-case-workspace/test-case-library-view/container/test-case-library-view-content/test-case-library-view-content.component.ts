import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { TestCaseLibraryViewComponentData } from '../test-case-library-view/test-case-library-view.component';
import { TestCaseLibraryViewService } from '../../service/test-case-library-view.service';
import { DomSanitizer } from '@angular/platform-browser';
import {
  CustomDashboardBinding,
  CustomDashboardModel,
  EntityRowReference,
  EntityScope,
  SquashTmDataRowType,
} from 'sqtm-core';
import { TestCaseLibraryState } from '../../state/test-case-library.state';

@Component({
  selector: 'sqtm-app-test-case-folder-view-content',
  templateUrl: './test-case-library-view-content.component.html',
  styleUrls: ['./test-case-library-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestCaseLibraryViewContentComponent implements OnDestroy {
  private unsub$ = new Subject<void>();
  componentData$: Observable<TestCaseLibraryViewComponentData>;

  constructor(
    private testCaseLibraryViewService: TestCaseLibraryViewService,
    public sanitizer: DomSanitizer,
  ) {
    this.componentData$ = testCaseLibraryViewService.componentData$;
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  getPluginInnerSrc() {
    return this.sanitizer.bypassSecurityTrustResourceUrl('/plugin/proto-plugin-ng9/inner-page');
  }

  getStatisticScope(testCaseLibrary: TestCaseLibraryState): EntityScope[] {
    const ref = new EntityRowReference(
      testCaseLibrary.id,
      SquashTmDataRowType.TestCaseLibrary,
    ).asString();
    return [
      {
        id: ref,
        label: testCaseLibrary.name,
        projectId: testCaseLibrary.projectId,
      },
    ];
  }

  refreshStats($event: MouseEvent, componentData: TestCaseLibraryViewComponentData) {
    $event.stopPropagation();
    if (componentData.testCaseLibrary.statistics) {
      this.testCaseLibraryViewService.refreshStatistics();
    }
    if (componentData.testCaseLibrary.dashboard) {
      this.testCaseLibraryViewService.refreshDashboard();
    }
  }

  displayFavoriteDashboard($event: MouseEvent) {
    $event.stopPropagation();
    this.testCaseLibraryViewService.changeDashboardToDisplay('dashboard');
  }

  displayDefaultDashboard($event: MouseEvent) {
    $event.stopPropagation();
    this.testCaseLibraryViewService.changeDashboardToDisplay('default');
  }

  getChartBindings(dashboard: CustomDashboardModel) {
    return [...dashboard.chartBindings, ...dashboard.reportBindings] as CustomDashboardBinding[];
  }
}
