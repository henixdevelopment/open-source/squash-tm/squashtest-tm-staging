import { ChangeDetectionStrategy, Component, OnInit, ViewChild } from '@angular/core';
import {
  DialogReference,
  DisplayOption,
  GridService,
  i18NEnumToOptions,
  ReferentialDataService,
  RestService,
  SelectFieldComponent,
  TestCaseKind,
} from 'sqtm-core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CreateTestCaseFromRequirementDialogConfiguration } from './create-test-case-from-requirement-dialog-configuration';

@Component({
  selector: 'sqtm-app-create-test-case-from-requirement-dialog',
  templateUrl: './create-test-case-from-requirement-dialog.component.html',
  styleUrls: ['./create-test-case-from-requirement-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateTestCaseFromRequirementDialogComponent implements OnInit {
  formGroup: FormGroup;
  data: CreateTestCaseFromRequirementDialogConfiguration;
  tree: GridService;

  @ViewChild(SelectFieldComponent)
  selectField: SelectFieldComponent;

  constructor(
    public dialogReference: DialogReference<CreateTestCaseFromRequirementDialogConfiguration>,
    private restService: RestService,
    private referentialDataService: ReferentialDataService,
    private fb: FormBuilder,
  ) {
    this.data = this.dialogReference.data;
  }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      format: this.fb.control('STANDARD', [Validators.required]),
    });
  }

  confirm() {
    this.dialogReference.result = this.formGroup.controls.format.value;
    this.dialogReference.close();
  }

  get kindOptions(): DisplayOption[] {
    return i18NEnumToOptions(TestCaseKind);
  }
}
