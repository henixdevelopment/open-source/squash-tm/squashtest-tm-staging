import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'sqtm-app-charter-help-dialog',
  templateUrl: './charter-help-dialog.component.html',
  styleUrls: ['./charter-help-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class CharterHelpDialogComponent {}

export const CHARTER_HELP_DIALOG_URL = 'charter-help';
export const CHARTER_HELP_DIALOG_WIDTH = 650;
export const CHARTER_HELP_DIALOG_HEIGHT = 600;
