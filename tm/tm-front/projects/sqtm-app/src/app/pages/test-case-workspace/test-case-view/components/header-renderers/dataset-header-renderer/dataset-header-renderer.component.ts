import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewChild } from '@angular/core';
import {
  AbstractHeaderRendererComponent,
  EditableTextFieldComponent,
  GridDisplay,
  GridService,
} from 'sqtm-core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'sqtm-app-dataset-header-renderer',
  template: ` @if (gridDisplay) {
    <div class="full-width flex-row" [style.height]="calculateRowHeight(gridDisplay)">
      <sqtm-core-editable-text-field
        [layout]="'no-buttons'"
        [size]="'small'"
        [formControl]="formControl"
        [editable]="editable"
        [value]="columnDisplay.label"
        #editableTextField
        ngDefaultControl
      >
      </sqtm-core-editable-text-field>
      <div class="full-height m-r-5 flex-fixed-size resize-handler"></div>
    </div>
  }`,
  styleUrls: ['./dataset-header-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DatasetHeaderRendererComponent extends AbstractHeaderRendererComponent {
  formControl: FormControl;

  @ViewChild('editableTextField')
  editableTextField: EditableTextFieldComponent;

  get editable() {
    return this.gridDisplay.allowModifications;
  }

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
    this.formControl = new FormControl('', [Validators.maxLength(255)]);
  }

  calculateRowHeight(gridDisplay: GridDisplay): string {
    return `${gridDisplay.rowHeight}px`;
  }
}
