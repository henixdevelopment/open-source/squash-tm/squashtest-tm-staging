import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ImportTestCaseComponentData } from '../../state/import-test-case.state';
import { TestCaseImportLog } from 'sqtm-core';
import { ImportTestCaseService } from '../../services/import-test-case.service';

@Component({
  selector: 'sqtm-app-import-report',
  templateUrl: './import-report.component.html',
  styleUrls: ['./import-report.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImportReportComponent {
  @Input()
  componentData: ImportTestCaseComponentData;

  constructor(private importTestCaseService: ImportTestCaseService) {}

  getFileName(templateOk: TestCaseImportLog) {
    return this.importTestCaseService.getDownLoadReportUrl(templateOk.reportUrl);
  }
}
