import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AbstractDeleteCellRenderer, DialogService, GridService } from 'sqtm-core';
import { TestCaseViewService } from '../../../service/test-case-view.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-delete-coverage',
  template: ` @if (canLink) {
    <sqtm-core-delete-icon
      [iconName]="getIcon()"
      (delete)="showDeleteConfirm()"
    ></sqtm-core-delete-icon>
  }`,
  styleUrls: ['./delete-coverage.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteCoverageComponent extends AbstractDeleteCellRenderer implements OnInit {
  canLink: boolean;

  constructor(
    grid: GridService,
    protected cdr: ChangeDetectorRef,
    dialogService: DialogService,
    private testCaseViewService: TestCaseViewService,
  ) {
    super(grid, cdr, dialogService);
  }

  getIcon(): string {
    return 'sqtm-core-generic:unlink';
  }

  ngOnInit(): void {
    this.testCaseViewService.componentData$
      .pipe(takeUntil(this.unsub$))
      .subscribe((componentData) => {
        this.canLink = componentData.permissions.canLink;
        this.cdr.detectChanges();
      });
  }

  protected getTitleKey(): string {
    return 'sqtm-core.test-case-workspace.dialog.title.remove-coverage.singular';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.test-case-workspace.dialog.message.remove-coverage.singular';
  }

  doDelete(): any {
    this.testCaseViewService.deleteCoverage(this.row.id as number);
  }
}
