import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { DatasetParamValue } from 'sqtm-core';

export interface DatasetParamValueState extends EntityState<DatasetParamValue> {}

export const datasetParamValueEntityAdapter = createEntityAdapter<DatasetParamValue>({
  selectId: (datasetParamValue) => datasetParamValue.id,
});

export const datasetParamValueEntitySelectors = datasetParamValueEntityAdapter.getSelectors();
