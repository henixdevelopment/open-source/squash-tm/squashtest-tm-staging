import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { CalledTestCase } from 'sqtm-core';

export interface CalledTestCaseState extends EntityState<CalledTestCase> {}

export const calledTestCaseEntityAdapter = createEntityAdapter<CalledTestCase>({
  selectId: (calledTestCase) => calledTestCase.id,
});

export const calledTestCaseEntitySelectors = calledTestCaseEntityAdapter.getSelectors();
