import {
  ActionStepFormModel,
  ActionStepModel,
  AttachmentHolderType,
  AttachmentListModel,
  AttachmentService,
  AutomationRequest,
  AutomationRequestStatus,
  CalledTestCase,
  CallStepModel,
  ChangeCoverageOperationReport,
  CustomFieldValueService,
  Dataset,
  DatasetParamValue,
  DateFormatUtils,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  Execution,
  GridService,
  Identifier,
  KeywordStepFormModel,
  LocalPersistenceService,
  Milestone,
  Parameter,
  PasteTestStepOperationReport,
  ProjectData,
  ProjectDataMap,
  ReferentialDataService,
  RequirementVersionCoverage,
  RestService,
  StoreOptions,
  TargetOnUpload,
  TestAutomation,
  TestCaseModel,
  TestCaseParameterOperationReport,
  TestCasePermissions,
  TestCaseService,
  TestStepActionWordOperationReport,
  TestStepModel,
  UploadAttachmentEvent,
} from 'sqtm-core';
import { EMPTY, Observable } from 'rxjs';
import { concatMap, map, take, tap, withLatestFrom } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import {
  calledTestCasesSelector,
  coveragesCountSelect,
  coveragesSelector,
  datasetCountSelect,
  draggingStepSelector,
  draggingTestCaseSelector,
  stepCountSelector,
  stepCreationModeSelector,
  stepsViewSelector,
  TestCaseState,
} from '../state/test-case.state';
import { provideInitialTestCaseView, TestCaseViewState } from '../state/test-case-view.state';
import { TranslateService } from '@ngx-translate/core';
import {
  ActionStepState,
  KeywordStepView,
  stepsEntityAdapter,
  TestStepsState,
  TestStepState,
  TestStepView,
} from '../state/test-step.state';
import { select } from '@ngrx/store';
import { callStepDndPlaceholderId, createActionStepFormId } from '../test-case-view.constant';
import { ParameterAssignationUpdate, TestStepViewService } from './test-step-view.service';
import { testCaseWorkspaceLogger } from '../../test-case-workspace.logger';
import { coverageEntityAdapter, CoverageState } from '../state/requirement-version-coverage.state';
import { executionEntityAdapter, ExecutionState } from '../state/execution.state';
import { calledTestCaseEntityAdapter, CalledTestCaseState } from '../state/called-test-case.state';
import { TestCaseViewComponentData } from '../containers/test-case-view/test-case-view.component';
import { parameterEntityAdapter, ParameterState } from '../state/parameter.state';
import { datasetEntityAdapter, DataSetState } from '../state/dataset.state';
import {
  datasetParamValueEntityAdapter,
  DatasetParamValueState,
} from '../state/dataset-param-value.state';

const logger = testCaseWorkspaceLogger.compose('TestCaseViewService');
const storeOptions: StoreOptions = {
  id: 'TestCaseViewStore',
  logDiff: 'detailed',
};

/**
 * Facade for the TestCaseView.
 */
@Injectable()
export class TestCaseViewService extends EntityViewService<
  TestCaseState,
  'testCase',
  TestCasePermissions
> {
  public readonly testStep$: Observable<TestStepView[]> = this.componentData$.pipe(
    select(stepsViewSelector),
  );

  public readonly stepDragging$: Observable<boolean> = this.componentData$.pipe(
    select(draggingStepSelector),
  );

  public readonly testCaseDragging$: Observable<boolean> = this.componentData$.pipe(
    select(draggingTestCaseSelector),
  );

  /**
   * Does a step is actually in creation ? If true, all other step drag and drop controls and buttons must be disabled.
   */
  public readonly stepCreationMode$: Observable<boolean> = this.componentData$.pipe(
    select(stepCreationModeSelector),
  );

  /**
   * Number of real steps aka no creation form
   */
  public readonly stepCount$: Observable<number> = this.componentData$.pipe(
    select(stepCountSelector),
  );

  public readonly coveragesCount$: Observable<number> = this.componentData$.pipe(
    select(coveragesCountSelect),
  );

  public readonly datasetCount$: Observable<number> = this.componentData$.pipe(
    select(datasetCountSelect),
  );

  private readonly stepsExtendedStateStorageKey = 'execution-steps-extended-state';

  constructor(
    protected restService: RestService,
    protected referentialDataService: ReferentialDataService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected testCaseService: TestCaseService,
    protected customFieldValueService: CustomFieldValueService,
    protected testStepViewService: TestStepViewService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
    protected customFieldHelper: EntityViewCustomFieldHelperService,
    private coverageGrid: GridService,
    private datasetsTable: GridService,
    private calledTestCaseGrid: GridService,
    private localPersistenceService: LocalPersistenceService,
  ) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper,
      storeOptions,
    );
  }

  load(id: number, backUrl: string): void {
    this.restService
      .getWithoutErrorHandling<TestCaseModel>(['test-case-view', id.toString()])
      .pipe(withLatestFrom(this.referentialDataService.projectDatas$))
      .subscribe({
        next: ([testCaseModel, projectDataMap]: [TestCaseModel, ProjectDataMap]) => {
          this.initializeTestCase(testCaseModel, backUrl, projectDataMap);
          this.initializeCoverageGrid();
          this.initializeCalledTestCaseGrid();
        },
        error: (err) => this.notifyEntityNotFound(err),
      });
  }

  complete() {
    super.complete();
    this.coverageGrid.complete();
    this.datasetsTable.complete();
    this.calledTestCaseGrid.complete();
  }

  private initializeCoverageGrid() {
    const coverage$ = this.componentData$.pipe(select(coveragesSelector));
    this.coverageGrid.connectToDatasource(coverage$, 'requirementVersionId');
    this.componentData$.pipe(take(1)).subscribe((componentData) => {
      const milestoneFeatureEnabled = componentData.globalConfiguration.milestoneFeatureEnabled;
      this.coverageGrid.setColumnVisibility('milestoneIds', milestoneFeatureEnabled);
      const canEdit = componentData.permissions.canLink;
      this.coverageGrid.setColumnVisibility('delete', canEdit);
    });
  }

  private initializeCalledTestCaseGrid() {
    const calledTestCase$ = this.componentData$.pipe(select(calledTestCasesSelector));
    this.calledTestCaseGrid.connectToDatasource(calledTestCase$, 'id');
  }

  public initializeTestCase(
    testCaseModel: TestCaseModel,
    backUrl: string,
    projectDataMap: ProjectDataMap,
  ) {
    const attachmentEntityState = this.initializeAttachmentState(
      testCaseModel.attachmentList.attachments,
    );

    this.localPersistenceService
      .get(this.stepsExtendedStateStorageKey)
      .subscribe((extended: boolean) => {
        const testSteps: TestStepsState = this.testStepViewService.initializeStepState(
          testCaseModel.testSteps,
          testCaseModel.kind,
          projectDataMap,
          extended,
        );
        Object.values(testSteps.entities).forEach((testStepState) => {
          testStepState.extended = extended;
        });

        const customFieldValueState = this.initializeCustomFieldValueState(
          testCaseModel.customFieldValues,
        );
        const coverageState: CoverageState = this.initializeCoverageState(testCaseModel.coverages);
        const executionState: ExecutionState = this.initializeExecutionState(
          testCaseModel.executions,
        );
        const calledTestCaseState: CalledTestCaseState = this.initializeCalledTestCaseState(
          testCaseModel.calledTestCases,
        );
        const parameterState: ParameterState = this.initializeParameterState(
          testCaseModel.parameters,
        );
        const datasetState: DataSetState = this.initializeDatasetState(testCaseModel.datasets);
        const datasetParamValueState: DatasetParamValueState =
          this.initializeDatasetParamValueState(testCaseModel.datasetParamValues);
        const entityState: TestCaseState = {
          ...testCaseModel,
          testSteps,
          attachmentList: {
            id: testCaseModel.attachmentList.id,
            attachments: attachmentEntityState,
          },
          customFieldValues: customFieldValueState,
          coverages: coverageState,
          executions: executionState,
          uiState: { openTestCaseTreePicker: false, openRequirementTreePicker: false },
          calledTestCases: calledTestCaseState,
          parameters: parameterState,
          datasets: datasetState,
          datasetParamValues: datasetParamValueState,
          navigationState: { backUrl },
          createdOn: DateFormatUtils.createDateFromIsoString(testCaseModel.createdOn),
          lastModifiedOn: DateFormatUtils.createDateFromIsoString(testCaseModel.lastModifiedOn),
        };
        this.initializeEntityState(entityState);
      });
  }

  updateTestCaseImportanceAuto(testCaseId: number, importanceAuto: boolean) {
    this.restService
      .post<any>([`test-case/${testCaseId}/importance-auto`], { importanceAuto })
      .subscribe((result) => {
        const state = this.getSnapshot();
        const nextState = {
          ...state,
          testCase: { ...state.testCase, importanceAuto, importance: result.importance },
        };
        this.requireExternalUpdate(state.testCase.id, 'importanceAuto', importanceAuto);
        this.store.commit(nextState);
      });
  }

  unbindMilestone(testCaseId: number, milestoneId: number) {
    this.restService
      .delete<void>([`test-case/${testCaseId}/milestones/${[milestoneId]}`])
      .pipe(map(() => unbindMilestoneToTC(this.getSnapshot(), milestoneId)))
      .subscribe((state) => {
        this.store.commit(state);
        this.updateLastModification();
      });
  }

  bindMilestones(testCaseId: number, milestoneIds: number[]) {
    this.restService
      .post<void>([`test-case/${testCaseId}/milestones/${milestoneIds}`])
      .pipe(
        withLatestFrom(this.componentData$),
        map(([, componentData]: [void, TestCaseViewComponentData]) => {
          return bindMilestonesToTC(
            componentData.projectData.milestones,
            this.getSnapshot(),
            milestoneIds,
          );
        }),
      )
      .subscribe((state) => {
        this.store.commit(state);
        this.updateLastModification();
      });
  }

  updateAutomatable(testCaseId: number, automatable) {
    this.restService
      .post<AutomationRequest>([`test-case/${testCaseId}/automatable`], { automatable })
      .pipe(
        map((automationRequest) => {
          const state = this.getSnapshot();
          return {
            ...state,
            testCase: { ...state.testCase, automatable: automatable, automationRequest },
          };
        }),
      )
      .subscribe((state) => {
        this.store.commit(state);
      });
  }

  transmit(testCaseId: number): Observable<any> {
    return this.updateAutomationRequestStatus(testCaseId, AutomationRequestStatus.TRANSMITTED.id);
  }

  updateAutomationRequestStatus(testCaseId: number, value): Observable<any> {
    const urlParts = [`automation-requests/${testCaseId}/status`];

    return this.restService.post<TestCaseModel[]>(urlParts, value).pipe(
      map((response: TestCaseModel[]) => {
        const state = this.getSnapshot();
        const updatedTestCase = response[0];
        return {
          ...state,
          testCase: {
            ...state.testCase,
            automationRequest: {
              ...state.testCase.automationRequest,
              ...updatedTestCase.automationRequest,
            },
            automatedTestReference: updatedTestCase.automatedTestReference,
            automatedTestTechnology: updatedTestCase.automatedTestTechnology,
            scmRepositoryId: updatedTestCase.scmRepositoryId,
          },
        };
      }),
      map((state) => this.store.commit(state)),
    );
  }

  updateAutomationRequestPriority(testCaseId: number, priority) {
    // In case the provided input is not an integer
    priority = priority === '' ? null : Math.floor(priority);

    this.restService
      .post<void>([`test-case/${testCaseId}/automation-request/priority`], { priority })
      .pipe(
        map(() => {
          const state = this.getSnapshot();
          return {
            ...state,
            testCase: {
              ...state.testCase,
              automationRequest: { ...state.testCase.automationRequest, priority: priority },
            },
          };
        }),
      )
      .subscribe((state) => {
        this.store.commit(state);
      });
  }

  getDatasetByTestCaseId(testCaseId: any): Observable<Dataset[]> {
    return this.restService.get<Dataset[]>(['test-cases', testCaseId, 'datasets']);
  }

  getInitialState(): TestCaseViewState {
    return provideInitialTestCaseView();
  }

  addSimplePermissions(projectData: ProjectData): TestCasePermissions {
    return new TestCasePermissions(projectData);
  }

  changeAction(id: number, action: string): Observable<any> {
    return this.testStepViewService.writeAction(id, action).pipe(
      map((response) => this.updateParameterState(response, this.getSnapshot())),
      map((state) => this.testStepViewService.updateAction(id, action, state)),
      tap((state) => this.commit(state)),
    );
  }

  changeExpectedResult(id: number, expectedResult: string): Observable<any> {
    return this.testStepViewService.writeExpectedResult(id, expectedResult).pipe(
      map((response) => this.updateParameterState(response, this.getSnapshot())),
      map((state) => this.testStepViewService.updateExpectedResult(id, expectedResult, state)),
      tap((state) => this.commit(state)),
    );
  }

  changeKeyword(id: number, keyword: string): Observable<any> {
    return this.testStepViewService.changeKeyword(id, keyword).pipe(
      map(() => this.testStepViewService.updateKeyword(id, keyword, this.getSnapshot())),
      tap((state) => this.commit(state)),
    );
  }

  changeActionWord(id: number, newActionWord: string): Observable<TestCaseViewState> {
    return this.testStepViewService.changeActionWord(id, newActionWord).pipe(
      map((response) => [
        response,
        this.updateParameterState(response.paramOperationReport, this.getSnapshot()),
      ]),
      map(([response, state]: [TestStepActionWordOperationReport, TestCaseViewState]) =>
        this.testStepViewService.updateActionWord(
          id,
          response.action,
          response.styledAction,
          response.actionWordId,
          state,
        ),
      ),
      tap((state) => this.commit(state)),
    );
  }

  changeActionWordWithId(
    id: number,
    newActionWord: string,
    actionWordId: number,
  ): Observable<TestCaseViewState> {
    return this.testStepViewService.changeActionWordWithId(id, newActionWord, actionWordId).pipe(
      map((response) => [
        response,
        this.updateParameterState(response.paramOperationReport, this.getSnapshot()),
      ]),
      map(([response, state]: [TestStepActionWordOperationReport, TestCaseViewState]) =>
        this.testStepViewService.updateActionWord(
          id,
          response.action,
          response.styledAction,
          response.actionWordId,
          state,
        ),
      ),
      tap((state) => this.commit(state)),
    );
  }

  changeDatatable(id: number, newDatatable: string): Observable<TestCaseViewState> {
    return this.testStepViewService.changeDatatable(id, newDatatable).pipe(
      map(() => this.testStepViewService.updateDatatable(id, newDatatable, this.getSnapshot())),
      tap((state) => this.commit(state)),
    );
  }

  changeDocstring(id: number, newDocstring: string): Observable<TestCaseViewState> {
    return this.testStepViewService.changeDocstring(id, newDocstring).pipe(
      map(() => this.testStepViewService.updateDocstring(id, newDocstring, this.getSnapshot())),
      tap((state) => this.commit(state)),
    );
  }

  changeComment(id: number, newComment: string): Observable<TestCaseViewState> {
    return this.testStepViewService.changeComment(id, newComment).pipe(
      map(() => this.testStepViewService.updateComment(id, newComment, this.getSnapshot())),
      tap((state) => this.commit(state)),
    );
  }

  private convertStepModels(testStepModels: TestStepModel[]) {
    const steps: TestStepState[] = testStepModels.map((model) => {
      if (model.kind === 'action-step') {
        return this.convertActionStepModel(model as ActionStepModel);
      } else {
        const callStepModel: CallStepModel = model as CallStepModel;
        return { ...callStepModel, extended: false };
      }
    });
    return steps;
  }

  private convertActionStepModel(model: ActionStepModel): ActionStepState {
    const attachmentEntityState = this.initializeAttachmentState(model.attachmentList.attachments);
    const customFieldValueState = this.initializeCustomFieldValueState(model.customFieldValues);
    return {
      ...model,
      extended: true,
      attachmentList: { id: model.attachmentList.id, attachments: attachmentEntityState },
      customFieldValues: customFieldValueState,
    };
  }

  private initializeCoverageState(coverages: RequirementVersionCoverage[]) {
    return coverageEntityAdapter.setAll(coverages, coverageEntityAdapter.getInitialState());
  }

  private initializeExecutionState(executions: Execution[]) {
    return executionEntityAdapter.setAll(executions, executionEntityAdapter.getInitialState());
  }

  private initializeCalledTestCaseState(calledTestCases: CalledTestCase[]) {
    return calledTestCaseEntityAdapter.setAll(
      calledTestCases,
      calledTestCaseEntityAdapter.getInitialState(),
    );
  }

  private initializeParameterState(parameters: Parameter[]) {
    return parameterEntityAdapter.setAll(parameters, parameterEntityAdapter.getInitialState());
  }

  private initializeDatasetState(datasets: Dataset[]) {
    return datasetEntityAdapter.setAll(datasets, datasetEntityAdapter.getInitialState());
  }

  private initializeDatasetParamValueState(paramValues: DatasetParamValue[]) {
    return datasetParamValueEntityAdapter.setAll(
      paramValues,
      datasetParamValueEntityAdapter.getInitialState(),
    );
  }

  updateStepCustomFieldValue(stepId: number, cufValueId: number, value: string | string[]) {
    this.testStepViewService
      .writeCustomFieldValue(stepId, cufValueId, value, this.getSnapshot())
      .pipe(
        map(() =>
          this.testStepViewService.updateStepCustomFieldValue(
            stepId,
            cufValueId,
            value,
            this.getSnapshot(),
          ),
        ),
      )
      .subscribe((state) => this.commit(state));
  }

  addAttachmentsToStep(files: File[], stepId: number, attachmentListId: number): void {
    if (logger.isDebugEnabled()) {
      logger.debug(`Adding files to step ${stepId} : `, [files]);
    }

    const previousState = this.getSnapshot();
    const entity = this.getEntity(previousState);

    this.attachmentHelper
      .addAttachments(
        files,
        attachmentListId,
        entity.id,
        previousState.type,
        AttachmentHolderType.ACTION_TEST_STEP,
      )
      .subscribe((event: UploadAttachmentEvent) => {
        this.commit(
          this.testStepViewService.mapUploadEventToState(stepId, this.getSnapshot(), event),
        );
      });
  }

  uploadImageFromEditor(file: File, targetOnUpload?: TargetOnUpload) {
    if (targetOnUpload == null) {
      return super.uploadImageFromEditor(file);
    } else {
      const previousState = this.getSnapshot();
      const entity = this.getEntity(previousState);
      return this.attachmentHelper
        .addImageAttachment(
          file,
          targetOnUpload.attachmentListId,
          entity.id,
          previousState.type,
          targetOnUpload.holderType,
        )
        .pipe(
          map((event: UploadAttachmentEvent) => {
            this.commit(
              this.testStepViewService.mapUploadEventToState(
                targetOnUpload.entityId,
                this.getSnapshot(),
                event,
              ),
            );
            return this.attachmentHelper.mapUploadEventToImageUpload(
              event,
              targetOnUpload.attachmentListId,
            );
          }),
        );
    }
  }

  markStepAttachmentsToDelete(ids: string[], stepId: number) {
    const nextState = this.testStepViewService.markStepAttachmentsToDelete(
      ids,
      stepId,
      this.getSnapshot(),
    );
    this.commit(nextState);
  }

  cancelStepAttachmentsToDelete(ids: string[], stepId: number) {
    const nextState = this.testStepViewService.cancelStepAttachmentsToDelete(
      ids,
      stepId,
      this.getSnapshot(),
    );
    this.commit(nextState);
  }

  removeStepRejectedAttachments(ids: string[], stepId: number) {
    const nextState = this.testStepViewService.removeStepRejectedAttachments(
      ids,
      stepId,
      this.getSnapshot(),
    );
    this.commit(nextState);
  }

  deleteStepAttachments(attachmentIds: string[], stepId: number, attachmentListId: number) {
    const idsAsNumber = attachmentIds.map((id) => parseInt(id, 10));
    const entity = this.getEntity(this.getSnapshot());
    const type = this.getSnapshot().type;
    this.attachmentHelper
      .eraseAttachments(
        idsAsNumber,
        attachmentListId,
        entity.id,
        type,
        AttachmentHolderType.ACTION_TEST_STEP,
      )
      .pipe(
        map(() =>
          this.testStepViewService.removeAttachments(attachmentIds, stepId, this.getSnapshot()),
        ),
      )
      .subscribe((state) => this.commit(state));
  }

  addActionStepForm(index: number) {
    const nextState = this.testStepViewService.addActionStepForm(index, this.getSnapshot());
    this.commit(nextState);
  }

  cancelAddTestStep() {
    const nextState = this.testStepViewService.cancelAddTestStep(this.getSnapshot());
    this.commit(nextState);
  }

  confirmAddActionStep(actionStepForm: ActionStepFormModel, testCaseId: number): Observable<any> {
    if (logger.isDebugEnabled()) {
      logger.debug(`Adding step to test case ${testCaseId}`, [actionStepForm]);
    }
    return this.testStepViewService.persistActionStep(actionStepForm, testCaseId).pipe(
      map((newStep) => {
        const newState = this.testStepViewService.addNewActionStep(
          newStep.testStepState as ActionStepState,
          actionStepForm.index,
          this.getSnapshot(),
        );
        return this.updateParameterState(newStep.testCaseParameterOperationReport, newState);
      }),
      tap((state) => this.requireTreeRefreshAfterAddingStep(state.testCase)),
      tap((state) => this.commit(state)),
    );
  }

  confirmAddKeywordStep(
    keywordStepForm: KeywordStepFormModel,
    testCaseId: number,
  ): Observable<any> {
    if (logger.isDebugEnabled()) {
      logger.debug(`Adding keyword step to keyword test case ${testCaseId}`, [keywordStepForm]);
    }
    return this.testStepViewService.persistKeywordStep(keywordStepForm, testCaseId).pipe(
      withLatestFrom(this.state$, this.referentialDataService.projectDatas$),
      map(([newStep, state, projectDataMap]) => {
        const newState = this.testStepViewService.addNewKeywordStep(
          newStep.testStepState as KeywordStepView,
          newStep.testStepState.stepOrder,
          state,
          projectDataMap,
        );
        return this.updateParameterState(newStep.testCaseParameterOperationReport, newState);
      }),
      tap((state) => this.requireTreeRefreshAfterAddingStep(state.testCase)),
      tap((state) => this.commit(state)),
    );
  }

  private requireTreeRefreshAfterAddingStep(testCase: TestCaseState) {
    this.requireExternalUpdate(testCase.id);
  }

  confirmAddAnotherActionStep(
    actionStepForm: ActionStepFormModel,
    testCaseId: number,
  ): Observable<any> {
    return this.testStepViewService.persistActionStep(actionStepForm, testCaseId).pipe(
      map((newStep) => {
        const newState = this.testStepViewService.addNewActionStep(
          newStep.testStepState as ActionStepState,
          actionStepForm.index,
          this.getSnapshot(),
        );
        return this.updateParameterState(newStep.testCaseParameterOperationReport, newState);
      }),
      tap((state) => this.requireTreeRefreshAfterAddingStep(state.testCase)),
      map((state: TestCaseViewState) =>
        this.testStepViewService.addActionStepForm(actionStepForm.index + 1, state),
      ),
      tap((state) => this.commit(state)),
    );
  }

  deleteStep(stepId: number) {
    this.testStepViewService
      .eraseSteps(stepId, this.getSnapshot())
      .pipe(
        map((stepDeleteOperation) => {
          const newState = this.testStepViewService.removeSteps(
            stepDeleteOperation.testStepsToDelete,
            this.getSnapshot(),
          );
          return this.updateParameterState(stepDeleteOperation.operationReport, newState);
        }),
        tap((state) => this.requireTreeRefreshAfterRemoveStep(state.testCase)),
        tap(() => this.refreshCoverages()),
      )
      .subscribe((state) => this.commit(state));
  }

  private requireTreeRefreshAfterRemoveStep(testCase: TestCaseState) {
    const stepIds = testCase.testSteps.ids;
    const numberOfSteps = stepIds.length;
    if (
      (testCase.kind === 'STANDARD' &&
        numberOfSteps === 1 &&
        stepIds[0] === createActionStepFormId) ||
      (testCase.kind === 'KEYWORD' && numberOfSteps === 0)
    ) {
      this.requireExternalUpdate(testCase.id);
    }
  }

  copySteps(stepId: number) {
    this.testStepViewService.copySteps(stepId, this.getSnapshot());
  }

  pasteSteps(targetStepId: number, testCaseId: number, isAssignedToTargetProject?: boolean) {
    this.testStepViewService
      .persistPastedSteps(targetStepId, testCaseId, isAssignedToTargetProject)
      .pipe(
        withLatestFrom(this.state$, this.referentialDataService.projectDatas$),
        map(
          ([operationReport, state, projectDataMap]: [
            PasteTestStepOperationReport,
            TestCaseViewState,
            ProjectDataMap,
          ]) => {
            const updatedState = this.testStepViewService.addPastedStep(
              targetStepId,
              operationReport,
              state,
              projectDataMap,
            );
            return this.updateParameterState(operationReport.operationReport, updatedState);
          },
        ),
        tap((state) => this.requireTreeRefreshAfterAddingStep(state.testCase)),
      )
      .subscribe((state) => this.commit(state));
  }

  compareKeywordProjectsIds(testCaseId: number): Observable<boolean> {
    return this.testStepViewService.compareKeywordProjectsIds(testCaseId);
  }

  selectStep(stepId: number) {
    const nextState = this.testStepViewService.selectStep(stepId, this.getSnapshot());
    this.commit(nextState);
  }

  toggleStepSelection(stepId: number) {
    const nextState = this.testStepViewService.toggleStepSelection(stepId, this.getSnapshot());
    this.commit(nextState);
  }

  extendStepSelection(selectedStepId: number) {
    const nextState = this.testStepViewService.extendStepSelection(
      selectedStepId,
      this.getSnapshot(),
    );
    this.commit(nextState);
  }

  startDraggingSteps(draggedStepIds: number[]) {
    const nextState = this.testStepViewService.startDraggingStep(
      draggedStepIds,
      this.getSnapshot(),
    );
    this.commit(nextState);
  }

  dragOverStep(targetId?: number) {
    const previousState = this.getSnapshot();

    if (
      previousState.testCase.testSteps.draggingSteps &&
      !previousState.testCase.testSteps.selectedStepIds.includes(targetId)
    ) {
      const nextState = this.testStepViewService.dragOverStep(targetId, previousState);
      this.store.commit(nextState);
    }
  }

  dropSteps() {
    const previousState = this.getSnapshot();

    if (previousState.testCase.testSteps.draggingSteps) {
      this.testStepViewService
        .writeNewStepPosition(previousState)
        .pipe(map(() => this.testStepViewService.dropStep(this.getSnapshot())))
        .subscribe((nextState) => this.commit(nextState));
    }
  }

  dropTestCaseForCall(calledTestCaseIds: number[]): Observable<any> {
    const previousState = this.getSnapshot();

    if (previousState.testCase.testSteps.draggingTestCase) {
      const updatedState = this.testStepViewService.cancelAddTestStep(previousState);
      this.commit(updatedState);

      return this.testStepViewService.writeCallTestCase(calledTestCaseIds, updatedState).pipe(
        withLatestFrom(this.state$, this.referentialDataService.projectDatas$),
        map(
          ([operationReport, state, projectDataMap]: [
            PasteTestStepOperationReport,
            TestCaseViewState,
            ProjectDataMap,
          ]) => this.testStepViewService.callTestCase(operationReport, state, projectDataMap),
        ),
        tap((state) => this.commit(state)),
        tap(() => this.refreshCoverages()),
        tap((state) => this.requireTreeRefreshAfterAddingStep(state.testCase)),
      );
    } else {
      return EMPTY;
    }
  }

  cancelDrag() {
    let nextState = this.testStepViewService.cancelDrag(this.getSnapshot());

    if (nextState.testCase.testSteps.ids.length === 0 && nextState.testCase.kind === 'STANDARD') {
      nextState = this.testStepViewService.addActionStepForm(0, nextState);
    }

    this.commit(nextState);
  }

  dragOverStepForCall(targetId?: number) {
    if (targetId !== callStepDndPlaceholderId) {
      const nextState = this.testStepViewService.dragOverStepForCall(this.getSnapshot(), targetId);
      this.commit(nextState);
    }
  }

  suspendStepDrag() {
    const previousState = this.getSnapshot();

    if (previousState.testCase.testSteps.draggingSteps) {
      const nextState = this.testStepViewService.suspendStepDrag(previousState);
      this.commit(nextState);
    }
  }

  suspendTestCaseDrag() {
    const previousState = this.getSnapshot();

    if (previousState.testCase.testSteps.draggingTestCase) {
      const nextState = this.testStepViewService.suspendTestCaseDrag(previousState);
      this.commit(nextState);
    }
  }

  collapseAllSteps() {
    const nextState = this.testStepViewService.collapseAllSteps(this.getSnapshot());
    this.commit(nextState);
    this.localPersistenceService.set(this.stepsExtendedStateStorageKey, false).subscribe();
  }

  expandAllSteps() {
    const nextState = this.testStepViewService.expandAllSteps(this.getSnapshot());
    this.commit(nextState);
    this.localPersistenceService.set(this.stepsExtendedStateStorageKey, true).subscribe();
  }

  toggleStep(id: number) {
    const nextState = this.testStepViewService.toggleStep(id, this.getSnapshot());
    this.commit(nextState);
  }

  moveStepUp(stepId: number) {
    const previousState = this.getSnapshot();
    const ids = previousState.testCase.testSteps.ids as number[];
    const index = ids.indexOf(stepId);

    if (index > 0) {
      const dropIndex = ids.indexOf(stepId) - 1;

      this.testStepViewService
        .writeStepMovement(stepId, dropIndex, previousState)
        .pipe(map(() => this.testStepViewService.moveStep(stepId, dropIndex, this.getSnapshot())))
        .subscribe((state) => this.commit(state));
    }
  }

  moveStepDown(stepId: number) {
    const previousState = this.getSnapshot();
    const ids = previousState.testCase.testSteps.ids as number[];
    const index = ids.indexOf(stepId);

    if (index < ids.length - 1) {
      const dropIndex = ids.indexOf(stepId) + 1;
      this.testStepViewService
        .writeStepMovement(stepId, dropIndex, previousState)
        .pipe(map(() => this.testStepViewService.moveStep(stepId, dropIndex, this.getSnapshot())))
        .subscribe((state) => this.commit(state));
    }
  }

  togglePrerequisite() {
    const nextState = this.testStepViewService.togglePrerequisite(this.getSnapshot());
    this.commit(nextState);
  }

  addCoverages(requirementIds: number[]): Observable<ChangeCoverageOperationReport> {
    this.coverageGrid.beginAsyncOperation();

    return this.testCaseService
      .persistCoverages(this.getSnapshot().testCase.id, requirementIds)
      .pipe(
        tap(() => this.requireTreeRefreshAfterCoverageOperation(this.getSnapshot().testCase)),
        map((operationReport: ChangeCoverageOperationReport) => {
          const state = this.getSnapshot();
          const coverages = coverageEntityAdapter.setAll(
            operationReport.coverages,
            state.testCase.coverages,
          );
          return [operationReport, { ...state, testCase: { ...state.testCase, coverages } }];
        }),
        tap(() => this.coverageGrid.completeAsyncOperation()),
        map(([operationReport, state]: [ChangeCoverageOperationReport, TestCaseViewState]) => {
          this.commit(state);
          return operationReport;
        }),
      );
  }

  refreshCoverages(): void {
    this.coverageGrid.beginAsyncOperation();
    this.testCaseService
      .getCoverages(this.getSnapshot().testCase.id)
      .pipe(
        map((updatedCoverages: RequirementVersionCoverage[]) => {
          const state = this.getSnapshot();
          const coverages = coverageEntityAdapter.setAll(
            updatedCoverages,
            state.testCase.coverages,
          );
          return { ...state, testCase: { ...state.testCase, coverages } };
        }),
        tap(() => this.coverageGrid.completeAsyncOperation()),
        tap((state: TestCaseViewState) => this.commit(state)),
      )
      .subscribe();
  }

  deleteCoverages() {
    this.coverageGrid.beginAsyncOperation();

    this.coverageGrid.selectedRowIds$
      .pipe(
        take(1),
        concatMap((rowIds: Identifier[]) => {
          return this.testCaseService.eraseCoverages(
            this.getSnapshot().testCase.id,
            rowIds as number[],
          );
        }),
        map((operationReport: ChangeCoverageOperationReport) => {
          const state = this.getSnapshot();
          const coverages = coverageEntityAdapter.setAll(
            operationReport.coverages,
            state.testCase.coverages,
          );
          return { ...state, testCase: { ...state.testCase, coverages } };
        }),
        tap((state) => this.requireTreeRefreshAfterCoverageOperation(state.testCase)),
        tap(() => this.coverageGrid.completeAsyncOperation()),
      )
      .subscribe((state) => this.commit(state));
  }

  deleteCoverage(id: number) {
    this.coverageGrid.beginAsyncOperation();

    this.testCaseService
      .eraseCoverages(this.getSnapshot().testCase.id, [id])
      .pipe(
        map((operationReport: ChangeCoverageOperationReport) => {
          const state = this.getSnapshot();
          const coverages = coverageEntityAdapter.setAll(
            operationReport.coverages,
            state.testCase.coverages,
          );
          return { ...state, testCase: { ...state.testCase, coverages } };
        }),
        tap((state) => this.requireTreeRefreshAfterCoverageOperation(state.testCase)),
        tap(() => this.coverageGrid.completeAsyncOperation()),
      )
      .subscribe((state) => this.commit(state));
  }

  dragRequirementOverStep(stepId: number) {
    const nextState = this.testStepViewService.dragRequirementOverStep(stepId, this.getSnapshot());
    this.commit(nextState);
  }

  openRequirementTreePicker() {
    const previousState = this.getSnapshot();
    const nextState = {
      ...previousState,
      testCase: {
        ...previousState.testCase,
        uiState: { ...previousState.testCase.uiState, openRequirementTreePicker: true },
      },
    };
    this.commit(nextState);
  }

  closeRequirementTreePicker() {
    const previousState = this.getSnapshot();
    const nextState = {
      ...previousState,
      testCase: {
        ...previousState.testCase,
        uiState: { ...previousState.testCase.uiState, openRequirementTreePicker: false },
      },
    };
    this.commit(nextState);
  }

  dropRequirementForCoverageInStep(
    requirementIds: number[],
  ): Observable<ChangeCoverageOperationReport> {
    const previousState = this.getSnapshot();

    if (this.isValidCoverageTarget(previousState)) {
      return this.testStepViewService
        .persistStepCoverages(
          requirementIds,
          previousState.testCase.testSteps.currentDndTargetId,
          previousState.testCase.id,
        )
        .pipe(
          tap(() => this.requireTreeRefreshAfterCoverageOperation(this.getSnapshot().testCase)),
          map((operationReport) => [
            operationReport,
            this.testStepViewService.dropRequirementForStepCoverages(
              operationReport,
              this.getSnapshot(),
            ),
          ]),
          map(([operationReport, state]: [ChangeCoverageOperationReport, TestCaseViewState]) => {
            this.commit(state);
            return operationReport;
          }),
        );
    } else {
      return EMPTY;
    }
  }

  private isValidCoverageTarget(state: TestCaseViewState): boolean {
    const currentDndTargetId = state.testCase.testSteps.currentDndTargetId;
    if (currentDndTargetId) {
      const step = state.testCase.testSteps.entities[currentDndTargetId];
      return step.kind === 'action-step';
    }
    return false;
  }

  deleteStepCoverages(requirementVersionIds: number[], stepId: number) {
    this.testStepViewService
      .eraseStepCoverages(this.getSnapshot().testCase.id, stepId, requirementVersionIds)
      .pipe(
        map((operationReport: ChangeCoverageOperationReport) =>
          this.testStepViewService.deleteStepCoverage(operationReport, this.getSnapshot()),
        ),
        tap((state) => this.requireTreeRefreshAfterCoverageOperation(state.testCase)),
      )
      .subscribe((state) => this.commit(state));
  }

  openTestCaseTreePicker() {
    const previousState = this.getSnapshot();
    const nextState = {
      ...previousState,
      testCase: {
        ...previousState.testCase,
        uiState: { ...previousState.testCase.uiState, openTestCaseTreePicker: true },
      },
    };
    this.commit(nextState);
  }

  closeTestCaseTreePicker() {
    const previousState = this.getSnapshot();
    const nextState = {
      ...previousState,
      testCase: {
        ...previousState.testCase,
        uiState: { ...previousState.testCase.uiState, openTestCaseTreePicker: false },
      },
    };
    this.commit(nextState);
  }

  updateStepViewScroll(scrollTop: number) {
    const nextState = this.testStepViewService.updateStepViewScroll(scrollTop, this.getSnapshot());
    this.commit(nextState);
  }

  addParameter(operationParamDataSet: TestCaseParameterOperationReport) {
    const attachmentListId = this.getSnapshot().testCase.attachmentList.id;
    return this.restService
      .get<AttachmentListModel>(['attach-list', attachmentListId.toString()])
      .pipe(
        map((attachmentListModel) => {
          const attachments = this.attachmentHelper.initializeAttachmentState(
            attachmentListModel.attachments,
          );
          return this.updateAttachmentState(this.getSnapshot(), attachments);
        }),
        map((state) => this.updateParameterState(operationParamDataSet, state)),
        tap((state) => this.store.commit(state)),
      )
      .subscribe();
  }

  addDataset(operationParamDataSet: TestCaseParameterOperationReport) {
    const nextState = this.updateParameterState(operationParamDataSet, this.getSnapshot());
    this.commit(nextState);
  }

  copyDataset(dataset): Observable<any> {
    this.datasetsTable.beginAsyncOperation();

    return this.testCaseService
      .persistDataset(this.getSnapshot().testCase.id, dataset)
      .pipe(tap((result: TestCaseParameterOperationReport) => this.addDataset(result)));
  }

  removeDataSet(id) {
    this.datasetsTable.beginAsyncOperation();
    this.restService
      .delete([`datasets/${id}`])
      .pipe(
        map(() => {
          const state = this.getSnapshot();
          const datasets = datasetEntityAdapter.removeOne(id, state.testCase.datasets);
          return { ...state, testCase: { ...state.testCase, datasets } };
        }),
        tap((state) => this.store.commit(state)),
      )
      .subscribe(() => this.datasetsTable.completeAsyncOperation());
  }

  updateParameterState(
    operationParamDataSet: TestCaseParameterOperationReport,
    state: TestCaseViewState,
  ) {
    const datasets = datasetEntityAdapter.setAll(
      operationParamDataSet.dataSets,
      state.testCase.datasets,
    );
    const parameters = parameterEntityAdapter.setAll(
      operationParamDataSet.parameters,
      state.testCase.parameters,
    );
    const datasetParamValues = datasetParamValueEntityAdapter.setAll(
      operationParamDataSet.paramValues,
      state.testCase.datasetParamValues,
    );
    return { ...state, testCase: { ...state.testCase, datasets, parameters, datasetParamValues } };
  }

  paramIsUsed(paramId: string): Observable<boolean> {
    return this.restService
      .get([`parameters/${paramId}/used`])
      .pipe(map((response) => response['PARAMETER_USED']));
  }

  removeParameter(paramId: string) {
    this.restService
      .delete([`parameters/${paramId}`])
      .pipe(
        map(() => {
          const state = this.getSnapshot();
          const parameters = parameterEntityAdapter.removeOne(paramId, state.testCase.parameters);
          return { ...state, testCase: { ...state.testCase, parameters } };
        }),
      )
      .subscribe((state) => this.store.commit(state));
  }

  renameDataset(patch: DatasetPatch) {
    const previousState = this.getSnapshot();
    const datasets = datasetEntityAdapter.updateOne(
      {
        id: patch.id,
        changes: { name: patch.name },
      },
      previousState.testCase.datasets,
    );
    const nextState = { ...previousState, testCase: { ...previousState.testCase, datasets } };
    this.store.commit(nextState);
  }

  updateParamValueData(patch: DatasetParamValuePatch) {
    const previousState = this.getSnapshot();
    const datasetParamValues = datasetParamValueEntityAdapter.updateOne(
      {
        id: patch.id,
        changes: { value: patch.value },
      },
      previousState.testCase.datasetParamValues,
    );
    const nextState = {
      ...previousState,
      testCase: { ...previousState.testCase, datasetParamValues },
    };
    this.store.commit(nextState);
  }

  renameParameter(patch: ParameterPatch, testSteps: TestStepModel[], prerequisite: string) {
    const previousState = this.getSnapshot();
    const parameters = parameterEntityAdapter.updateOne(
      {
        id: patch.id,
        changes: { name: patch.name },
      },
      previousState.testCase.parameters,
    );
    const nextState = { ...previousState, testCase: { ...previousState.testCase, parameters } };

    this.referentialDataService.projectDatas$
      .pipe(
        map((projectDataMap) =>
          this.updateTestCaseStep(testSteps, nextState, projectDataMap, prerequisite),
        ),
      )
      .subscribe((state) => this.store.commit(state));
  }

  changeParameterDescription(patch: ParameterPatch) {
    const previousState = this.getSnapshot();
    const parameters = parameterEntityAdapter.updateOne(
      {
        id: patch.id,
        changes: { description: patch.description },
      },
      previousState.testCase.parameters,
    );
    const nextState = { ...previousState, testCase: { ...previousState.testCase, parameters } };
    this.store.commit(nextState);
  }

  changePrerequisite(prerequisiteValue: string): Observable<any> {
    const tcId = this.getSnapshot().testCase.id;

    return this.restService
      .post<TestCaseParameterOperationReport>([`test-case/${tcId}/prerequisite`], {
        prerequisite: prerequisiteValue,
      })
      .pipe(
        map((operation) => this.updateParameterState(operation, this.getSnapshot())),
        map((state) => ({
          ...state,
          testCase: { ...state.testCase, prerequisite: prerequisiteValue },
        })),
        tap((state) => this.store.commit(state)),
      );
  }

  changeStatus(newStatus: string) {
    this.updateTestCaseStatusServerSide(this.getSnapshot(), newStatus)
      .pipe(
        map(() => this.updateStateWithNewStatus(this.getSnapshot(), newStatus)),
        tap((state) => this.requireExternalUpdate(state.testCase.id, 'status')),
      )
      .subscribe();
  }

  private updateTestCaseStatusServerSide(state: TestCaseViewState, newStatus: string) {
    const urlParts = ['test-case', state.testCase.id.toString(), 'status'];
    return this.restService.post(urlParts, { status: newStatus });
  }

  private updateStateWithNewStatus(state: any, newStatus: string): TestCaseViewState {
    const updatedState = {
      ...state,
      testCase: {
        ...state.testCase,
        status: newStatus,
      },
    };
    this.store.commit(updatedState);
    return updatedState;
  }

  changeImportance(newImportance: string) {
    this.updateTestCaseImportanceServerSide(this.getSnapshot(), newImportance)
      .pipe(
        map(() => this.updateStateWithNewImportance(this.getSnapshot(), newImportance)),
        tap((state) => this.requireExternalUpdate(state.testCase.id, 'importance')),
      )
      .subscribe();
  }

  private updateTestCaseImportanceServerSide(state: TestCaseViewState, newImportance: string) {
    const urlParts = ['test-case', state.testCase.id.toString(), 'importance'];
    return this.restService.post(urlParts, { importance: newImportance });
  }

  private updateStateWithNewImportance(state: any, newImportance: string) {
    const updatedState = {
      ...state,
      testCase: {
        ...state.testCase,
        importance: newImportance,
      },
    };
    this.store.commit(updatedState);
    return updatedState;
  }

  updateParameterAssignationMode(
    stepId: number,
    parameterAssignationMode: ParameterAssignationUpdate,
  ) {
    const previousState = this.getSnapshot();
    const mode = this.testStepViewService.extractParameterMode(parameterAssignationMode);
    this.testStepViewService
      .updateParameterAssignationMode(
        previousState.testCase.id,
        stepId,
        parameterAssignationMode,
        mode,
      )
      .pipe(
        map((report) => {
          const newState = this.testStepViewService.updateCallStepParameter(
            stepId,
            parameterAssignationMode.datasetId,
            parameterAssignationMode.datasetName,
            parameterAssignationMode.delegateParam,
            this.getSnapshot(),
          );
          return this.updateParameterState(report, newState);
        }),
      )
      .subscribe((state) => {
        this.store.commit(state);
      });
  }

  toggleRequirementTreePickerMessageVisibility(visible: boolean) {
    const previousState = this.getSnapshot();
    const updatedState = {
      ...previousState,
      testCase: {
        ...previousState.testCase,
        uiState: {
          ...previousState.testCase.uiState,
          requirementTreePickerMessageVisible: visible,
        },
      },
    };
    this.commit(updatedState);
  }

  private requireTreeRefreshAfterCoverageOperation(testCase: TestCaseState) {
    if (testCase.coverages.ids.length === 0) {
      this.requireExternalUpdate(testCase.id);
    }
  }

  updateTaTest(taTest: string): Observable<any> {
    const tcId = this.getSnapshot().testCase.id;
    return this.restService
      .post<TestAutomation>([`test-case/${tcId}/test-automation/tests`], { path: taTest })
      .pipe(
        map((testAutomation) => {
          const state = this.getSnapshot();
          return { ...state, testCase: { ...state.testCase, automatedTest: testAutomation } };
        }),
        tap((state) => this.store.commit(state)),
      );
  }

  updateScript(script: string) {
    this.testCaseService
      .updateScript(this.getSnapshot().testCase.id, script)
      .pipe(
        map(() => {
          const state = this.getSnapshot();
          return { ...state, testCase: { ...state.testCase, script: script } };
        }),
      )
      .subscribe((state) => this.store.commit(state));
  }

  validateScript(script: string): Observable<any> {
    return this.testCaseService.validateScript(this.getSnapshot().testCase.id, script);
  }

  getMatchingActionWords(searchInput: string, selectedProjectsIds: number[]): Observable<string[]> {
    const projectId = this.getSnapshot().testCase.projectId;
    return this.testCaseService.getMatchingActionWord(projectId, searchInput, selectedProjectsIds);
  }

  getDuplicateActionWords(searchInput: string): Observable<any> {
    const projectId = this.getSnapshot().testCase.projectId;
    return this.testCaseService.getDuplicateActionWords(projectId, searchInput);
  }

  getScriptPreview(): Observable<string> {
    return this.testCaseService.getScriptPreview(this.getSnapshot().testCase.id);
  }

  changeAutomatedTestTechnology(technologyId: number | null): void {
    this.update('automatedTestTechnology', technologyId);
  }

  private updateTestCaseStep(
    testSteps: TestStepModel[],
    state: any,
    projectDataMap: any,
    prerequisite: string,
  ) {
    const testStepsState = this.testStepViewService.convertStepModels(testSteps, projectDataMap);
    const newTestSteps = stepsEntityAdapter.setAll(testStepsState, state.testCase.testSteps);
    return {
      ...state,
      testCase: { ...state.testCase, testSteps: newTestSteps, prerequisite: prerequisite },
    };
  }

  changeCharter(charter: string): Observable<any> {
    const tcId = this.getSnapshot().testCase.id;

    return this.restService.post([`test-case/${tcId}/charter`], { charter }).pipe(
      map(() => {
        const state = this.getSnapshot();
        return { ...state, testCase: { ...state.testCase, charter: charter } };
      }),
      tap((state) => this.store.commit(state)),
    );
  }

  changeSessionDuration(durationInMinutes: number): Observable<TestCaseViewState> {
    const tcId = this.getSnapshot().testCase.id;

    return this.restService
      .post([`test-case/${tcId}/session-duration`], { durationInMinutes })
      .pipe(
        map(() => {
          const state = this.getSnapshot();
          return { ...state, testCase: { ...state.testCase, sessionDuration: durationInMinutes } };
        }),
        tap((state) => this.store.commit(state)),
      );
  }
}

export function bindMilestonesToTC(
  projectMilestones: Milestone[],
  state: TestCaseViewState,
  milestoneIds: number[],
) {
  projectMilestones = projectMilestones.filter((milestone) =>
    milestoneIds.find((id) => id === milestone.id),
  );
  const testCaseMilestones = state.testCase.milestones;
  projectMilestones.forEach((milestoneView) => {
    const milestone: Milestone = { ...milestoneView };
    testCaseMilestones.push(milestone);
  });
  return {
    ...state,
    testCase: {
      ...state.testCase,
      milestones: testCaseMilestones,
    },
  };
}

export function unbindMilestoneToTC(state: TestCaseViewState, milestoneId: number) {
  return {
    ...state,
    testCase: {
      ...state.testCase,
      milestones: state.testCase.milestones.filter((milestone) => milestone.id !== milestoneId),
    },
  };
}

export interface DatasetPatch {
  id;
  name: string;
}

export interface DatasetParamValuePatch {
  id;
  value: string;
}

export interface ParameterPatch {
  id;
  name?: string;
  description?: string;
}
