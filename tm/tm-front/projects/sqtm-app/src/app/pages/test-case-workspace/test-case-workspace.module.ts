import { NgModule } from '@angular/core';
import { TestCaseWorkspaceComponent } from './test-case-workspace/containers/test-case-workspace/test-case-workspace.component';
import { RouterModule, Routes } from '@angular/router';
import { TestCaseViewComponent } from './test-case-view/containers/test-case-view/test-case-view.component';
import { TestCaseLibraryViewComponent } from './test-case-library-view/container/test-case-library-view/test-case-library-view.component';
import { TestCaseViewContentComponent } from './test-case-view/components/test-case-view-content/test-case-view-content.component';
import { TestStepsComponent } from './test-case-view/components/test-steps/test-steps.component';
import { IssuesComponent } from './test-case-view/components/issues/issues.component';
import { ExecutionsComponent } from './test-case-view/components/executions/executions.component';
import { ScriptComponent } from './test-case-view/components/script/script.component';
import { TestCaseFolderViewComponent } from './test-case-folder-view/container/test-case-folder-view/test-case-folder-view.component';
import { TestCaseFolderViewContentComponent } from './test-case-folder-view/container/test-case-folder-view-content/test-case-folder-view-content.component';
import { TestCaseLibraryViewContentComponent } from './test-case-library-view/container/test-case-library-view-content/test-case-library-view-content.component';
import { TestCaseWorkspaceMultiSelectViewComponent } from './test-case-workspace-multi-select-view/containers/test-case-workspace-multi-select-view/test-case-workspace-multi-select-view.component';
import { TestCaseMultiViewContentComponent } from './test-case-workspace-multi-select-view/containers/test-case-multi-view-content/test-case-multi-view-content.component';
import { TestCaseMilestoneViewComponent } from './test-case-milestone-dashboard/containers/test-case-milestone-view/test-case-milestone-view.component';
import { TestCaseMilestoneViewContentComponent } from './test-case-milestone-dashboard/containers/test-case-milestone-view-content/test-case-milestone-view-content.component';
import { TestCaseViewSubPageComponent } from './test-case-view-sub-page/test-case-view-sub-page.component';
import { KeywordTestStepsComponent } from './test-case-view/components/keyword-test-steps/keyword-test-steps.component';
import { TestCaseViewModifDuringExecComponent } from './test-case-view-modif-during-exec/test-case-view-modif-during-exec.component';
import { TestCaseWorkspaceCommonModule } from './test-case-workspace-common.module';
import { TestCaseCharterComponent } from './test-case-view/components/test-case-charter/test-case-charter.component';
import { CharterHelpDialogComponent } from './test-case-view/components/dialog/charter-help-dialog/charter-help-dialog.component';
import { SessionHistoryComponent } from './test-case-view/components/session-history/session-history.component';

export const routes: Routes = [
  {
    path: '',
    component: TestCaseWorkspaceComponent,
    children: [
      {
        path: 'test-case/:testCaseId',
        component: TestCaseViewComponent,
        children: [
          { path: 'content', component: TestCaseViewContentComponent },
          { path: 'steps', component: TestStepsComponent },
          { path: 'keywordSteps', component: KeywordTestStepsComponent },
          { path: 'issues', component: IssuesComponent },
          { path: 'executions', component: ExecutionsComponent },
          { path: 'sessions', component: SessionHistoryComponent },
          { path: 'script', component: ScriptComponent },
          { path: 'charter', component: TestCaseCharterComponent },
        ],
      },
      {
        path: 'test-case-library/:testCaseLibraryId',
        component: TestCaseLibraryViewComponent,
        children: [{ path: 'content', component: TestCaseLibraryViewContentComponent }],
      },
      {
        path: 'test-case-folder/:testCaseFolderId',
        component: TestCaseFolderViewComponent,
        children: [{ path: 'content', component: TestCaseFolderViewContentComponent }],
      },
      {
        path: 'test-case-workspace-multi',
        component: TestCaseWorkspaceMultiSelectViewComponent,
        children: [
          { path: '', pathMatch: 'full', redirectTo: 'content' },
          { path: 'content', component: TestCaseMultiViewContentComponent },
        ],
      },
      {
        path: 'milestone-dashboard',
        component: TestCaseMilestoneViewComponent,
        children: [{ path: 'content', component: TestCaseMilestoneViewContentComponent }],
      },
    ],
  },
  {
    path: 'test-case/detail/:testCaseId',
    component: TestCaseViewSubPageComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'content' },
      { path: 'content', component: TestCaseViewContentComponent },
      { path: 'steps', component: TestStepsComponent },
      { path: 'keywordSteps', component: KeywordTestStepsComponent },
      { path: 'issues', component: IssuesComponent },
      { path: 'executions', component: ExecutionsComponent },
      { path: 'sessions', component: SessionHistoryComponent },
      { path: 'script', component: ScriptComponent },
      { path: 'charter', component: TestCaseCharterComponent },
    ],
  },
  {
    path: 'test-case/modification-during-exec/:testCaseId/execution/:executionId',
    component: TestCaseViewModifDuringExecComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'content' },
      { path: 'content', component: TestCaseViewContentComponent },
      { path: 'steps', component: TestStepsComponent },
      { path: 'keywordSteps', component: KeywordTestStepsComponent },
      { path: 'issues', component: IssuesComponent },
      { path: 'executions', component: ExecutionsComponent },
      { path: 'script', component: ScriptComponent },
    ],
  },
  {
    path: 'charter-help',
    component: CharterHelpDialogComponent,
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes), TestCaseWorkspaceCommonModule],
})
export class TestCaseWorkspaceModule {}
