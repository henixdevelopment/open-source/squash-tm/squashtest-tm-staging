import { AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { TCW_SESSIONS_TABLE, TCW_SESSIONS_TABLE_CONF } from '../../test-case-view.constant';
import {
  executionStatusColumn,
  Fixed,
  grid,
  GridColumnId,
  GridDefinition,
  GridService,
  gridServiceFactory,
  Limited,
  LocalPersistenceService,
  numericColumn,
  ReferentialDataService,
  RestService,
  textColumn,
  withLinkColumn,
} from 'sqtm-core';
import { executionLastExecutedOn } from '../cell-renderers/execution-last-executed-on/execution-last-executed-on.component';
import { TestCaseViewService } from '../../service/test-case-view.service';
import { take } from 'rxjs/operators';

export function tcwSessionsTableDefinition(
  localPersistenceService: LocalPersistenceService,
): GridDefinition {
  return grid('test-case-view-sessions')
    .withColumns([
      textColumn(GridColumnId.executionPath)
        .withI18nKey('sqtm-core.generic.label.path')
        .changeWidthCalculationStrategy(new Limited(220)),
      withLinkColumn(GridColumnId.overviewId, {
        kind: 'link',
        baseUrl: '/session-overview/',
        columnParamId: GridColumnId.overviewId,
        saveGridStateBeforeNavigate: true,
        labelKey: 'sqtm-core.generic.label.consult',
      })
        .withI18nKey('sqtm-core.entity.exploratory-session.label.singular')
        .changeWidthCalculationStrategy(new Fixed(80)),
      executionStatusColumn(GridColumnId.executionStatus)
        .withI18nKey('sqtm-core.entity.execution.status.label')
        .changeWidthCalculationStrategy(new Fixed(60))
        .withHeaderPosition('center'),
      executionLastExecutedOn(GridColumnId.lastExecutedOn)
        .withI18nKey('sqtm-core.entity.execution.last-executed-on.label')
        .changeWidthCalculationStrategy(new Fixed(80)),
      numericColumn(GridColumnId.executionCount)
        .withI18nKey('sqtm-core.entity.execution.label.short-dot')
        .withTitleI18nKey('sqtm-core.entity.execution.label.count')
        .changeWidthCalculationStrategy(new Fixed(60)),
      numericColumn(GridColumnId.nbIssues)
        .withI18nKey('sqtm-core.entity.issue.label.short')
        .withTitleI18nKey('sqtm-core.entity.issue.label.count')
        .changeWidthCalculationStrategy(new Fixed(60))
        .withHeaderPosition('center'),
    ])
    .server()
    .disableRightToolBar()
    .withRowHeight(35)
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
}

@Component({
  selector: 'sqtm-app-session-history',
  template: `
    <div class="flex-column full-height full-width p-r-15 overflow-y-hidden">
      <div class="sqtm-large-grid-container-title">
        {{ 'sqtm-core.test-case-workspace.title.sessions' | translate }}
      </div>
      <div class="sqtm-large-grid-container full-width full-height p-15 m-b-10">
        <sqtm-core-grid></sqtm-core-grid>
      </div>
    </div>
  `,
  providers: [
    {
      provide: TCW_SESSIONS_TABLE_CONF,
      useFactory: tcwSessionsTableDefinition,
      deps: [LocalPersistenceService],
    },
    {
      provide: TCW_SESSIONS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, TCW_SESSIONS_TABLE_CONF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: TCW_SESSIONS_TABLE,
    },
  ],
  styleUrls: ['./session-history.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SessionHistoryComponent implements OnDestroy, AfterViewInit {
  constructor(
    private testCaseViewService: TestCaseViewService,
    private gridService: GridService,
  ) {}

  ngAfterViewInit(): void {
    this.testCaseViewService.componentData$.pipe(take(1)).subscribe((componentData) => {
      this.gridService.setServerUrl([`test-case/${componentData.testCase.id}/sessions`]);
    });
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }
}
