import { ChangeDetectionStrategy, Component, Input, ViewChild } from '@angular/core';
import { TestCaseViewService } from '../../service/test-case-view.service';
import { TestCaseViewComponentData } from '../../containers/test-case-view/test-case-view.component';
import {
  ActionErrorDisplayService,
  EditableTaTestComponent,
  TestAutomationServerKind,
} from 'sqtm-core';
import { catchError, finalize, tap } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-script-auto',
  templateUrl: './script-auto.component.html',
  styleUrls: ['./script-auto.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScriptAutoComponent {
  @Input()
  testCaseViewComponentData: TestCaseViewComponentData;

  @ViewChild('taTestField')
  taTestField: EditableTaTestComponent;

  get editable() {
    return (
      this.canWrite() &&
      this.testCaseViewComponentData.milestonesAllowModification &&
      this.hasJenkinsKindTaServer()
    );
  }

  constructor(
    private testCaseViewService: TestCaseViewService,
    private actionErrorDisplayService: ActionErrorDisplayService,
  ) {}

  getAutomatedTestName(componentData: TestCaseViewComponentData) {
    const taTest = componentData.testCase.automatedTest;
    if (taTest) {
      return taTest.fullLabel;
    } else {
      return '';
    }
  }

  changeAutomatedTest(taTest: string) {
    this.testCaseViewService
      .updateTaTest(taTest)
      .pipe(
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => this.taTestField.endAsync()),
        tap(() => this.taTestField.disableEditMode()),
      )
      .subscribe();
  }

  private canWrite() {
    return (
      this.testCaseViewComponentData.permissions.canWrite ||
      this.testCaseViewComponentData.permissions.canWriteAsAutomation
    );
  }

  private hasJenkinsKindTaServer() {
    return (
      this.testCaseViewComponentData.projectData.taServer.id != null &&
      this.testCaseViewComponentData.projectData.taServer.kind === TestAutomationServerKind.jenkins
    );
  }
}
