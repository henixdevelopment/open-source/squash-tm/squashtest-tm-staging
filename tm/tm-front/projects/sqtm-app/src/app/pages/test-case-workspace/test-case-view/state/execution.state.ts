import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { Execution } from 'sqtm-core';

export interface ExecutionState extends EntityState<Execution> {}

export const executionEntityAdapter = createEntityAdapter<Execution>({
  selectId: (exec) => exec.id,
});

export const executionEntitySelectors = executionEntityAdapter.getSelectors();
