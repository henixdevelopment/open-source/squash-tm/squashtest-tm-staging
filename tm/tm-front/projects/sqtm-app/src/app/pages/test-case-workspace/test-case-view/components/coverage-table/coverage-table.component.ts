import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import {
  coverageVerifiedByColumn,
  DataRow,
  deleteColumn,
  Extendable,
  GenericDataRow,
  GridColumnId,
  GridDefinition,
  GridId,
  GridService,
  indexColumn,
  levelEnumColumn,
  Limited,
  LocalPersistenceService,
  milestoneLabelColumn,
  ReferentialDataService,
  RequirementCriticality,
  RequirementStatus,
  smallGrid,
  Sort,
  StyleDefinitionBuilder,
  textCellWithToolTipColumn,
  textColumn,
  withLinkColumn,
} from 'sqtm-core';
import { TCW_COVERAGE_TABLE } from '../../test-case-view.constant';
import { DeleteCoverageComponent } from '../cell-renderers/delete-coverage/delete-coverage.component';
import { map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

export function tcwCoverageTableDefinition(
  localPersistenceService: LocalPersistenceService,
): GridDefinition {
  const urlFunctionToRequirement = (row: DataRow) => {
    return `/requirement-workspace/requirement-version/detail/${row.data[GridColumnId.requirementVersionId]}`;
  };

  return smallGrid(GridId.TEST_CASE_VIEW_COVERAGES)
    .withColumns([
      indexColumn().withViewport('leftViewport'),
      textCellWithToolTipColumn(GridColumnId.projectName, 'path')
        .changeWidthCalculationStrategy(new Limited(200))
        .withI18nKey('sqtm-core.entity.project.label.singular'),
      textColumn(GridColumnId.reference)
        .changeWidthCalculationStrategy(new Limited(120))
        .withI18nKey('sqtm-core.entity.generic.reference.label'),
      withLinkColumn(GridColumnId.name, {
        kind: 'link',
        createUrlFunction: urlFunctionToRequirement,
        saveGridStateBeforeNavigate: true,
      })
        .changeWidthCalculationStrategy(new Limited(500))
        .withI18nKey('sqtm-core.entity.requirement.label.singular'),
      milestoneLabelColumn(GridColumnId.milestoneLabels).changeWidthCalculationStrategy(
        new Limited(130),
      ),
      levelEnumColumn(GridColumnId.criticality, RequirementCriticality)
        .withTitleI18nKey('sqtm-core.entity.generic.criticality.label')
        .withI18nKey('sqtm-core.entity.generic.criticality.short')
        .isEditable(false)
        .changeWidthCalculationStrategy(new Extendable(40, 0.1)),
      levelEnumColumn(GridColumnId.status, RequirementStatus)
        .withTitleI18nKey('sqtm-core.entity.generic.status.label')
        .withI18nKey('sqtm-core.entity.generic.status.short')
        .isEditable(false)
        .changeWidthCalculationStrategy(new Extendable(40, 0.1)),
      coverageVerifiedByColumn(GridColumnId.verifiedBy)
        .changeWidthCalculationStrategy(new Extendable(60, 0.6))
        .withI18nKey('sqtm-core.entity.generic.verified-by.label'),
      deleteColumn(DeleteCoverageComponent).withViewport('rightViewport'),
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .withRowConverter(coverageLiteralConverter)
    .withInitialSortedColumns([
      { id: GridColumnId.criticality, sort: Sort.ASC },
      { id: GridColumnId.projectName, sort: Sort.ASC },
      { id: GridColumnId.reference, sort: Sort.ASC },
      { id: GridColumnId.name, sort: Sort.ASC },
    ])
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
}

@Component({
  selector: 'sqtm-app-coverage-table',
  template: ` <sqtm-core-grid></sqtm-core-grid> `,
  styleUrls: ['./coverage-table.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: TCW_COVERAGE_TABLE,
    },
  ],
})
export class CoverageTableComponent implements OnDestroy {
  unsub$ = new Subject<void>();

  constructor(
    public grid: GridService,
    public referentialDataService: ReferentialDataService,
  ) {
    this.referentialDataService.globalConfiguration$
      .pipe(
        takeUntil(this.unsub$),
        map((configuration) => configuration.milestoneFeatureEnabled),
      )
      .subscribe((milestoneFeatureEnabled) => {
        this.grid.setColumnVisibility(GridColumnId.milestoneLabels, milestoneFeatureEnabled);
      });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}

export function coverageLiteralConverter(literals: Partial<DataRow>[]): DataRow[] {
  return literals.reduce((datarows, literal) => {
    const dataRow = new GenericDataRow();
    Object.assign(dataRow, literal);
    const directlyVerified = dataRow.data[GridColumnId.directlyVerified];
    const unDirectlyVerified = dataRow.data[GridColumnId.unDirectlyVerified];
    dataRow.disabled = !directlyVerified && unDirectlyVerified;
    datarows.push(dataRow);
    return datarows;
  }, []);
}
