import { Parameter } from 'sqtm-core';

export class ParamInformationConfiguration {
  id: string;
  parameter: Parameter;
  delegate: boolean;
  canWrite: boolean;
}
