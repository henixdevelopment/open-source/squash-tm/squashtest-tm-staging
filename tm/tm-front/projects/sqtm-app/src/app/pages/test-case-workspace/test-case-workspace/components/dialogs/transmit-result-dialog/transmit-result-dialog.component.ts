import { ChangeDetectionStrategy, Component } from '@angular/core';
import { DialogReference } from 'sqtm-core';
import { TransmitResult } from '../../../containers/test-case-workspace-tree/test-case-workspace-tree.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-transmit-result-dialog',
  templateUrl: './transmit-result-dialog.component.html',
  styleUrls: ['./transmit-result-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransmitResultDialogComponent {
  data: TransmitResult;

  constructor(
    private dialogReference: DialogReference<TransmitResult>,
    private translateService: TranslateService,
  ) {
    this.data = this.dialogReference.data;
  }

  getMessage() {
    let message = `${this.translateService.instant('sqtm-core.test-case-workspace.dialog.message.transmit-all.size')} ${this.data.eligibleTcIds.length}`;
    if (!this.data.areAllEligible) {
      message = `${message} <br>
       ${this.translateService.instant('sqtm-core.test-case-workspace.dialog.message.eligible-message')}`;
    }

    return message;
  }
}
