import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Option } from 'sqtm-core';

@Component({
  selector: 'sqtm-app-debug-page',
  templateUrl: './debug-page.component.html',
  styleUrls: ['./debug-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DebugPageComponent {
  options: Option[] = [
    {
      label: 'Vrai',
      value: 'true',
    },
    {
      label: 'Faux',
      value: 'false',
    },
  ];

  selectedValue = 'true';
}
