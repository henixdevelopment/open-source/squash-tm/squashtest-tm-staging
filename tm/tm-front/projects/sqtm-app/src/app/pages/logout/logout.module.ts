import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogoutPageComponent } from './containers/logout-page/logout-page.component';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NavBarModule, UiManagerModule, WorkspaceCommonModule } from 'sqtm-core';

export const routes: Routes = [
  {
    path: '',
    component: LogoutPageComponent,
  },
];

@NgModule({
  declarations: [LogoutPageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild(),
    UiManagerModule,
    NavBarModule,
    WorkspaceCommonModule,
  ],
})
export class LogoutModule {}
