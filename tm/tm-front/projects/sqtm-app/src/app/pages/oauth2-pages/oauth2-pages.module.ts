import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Oauth2AuthenticationComponent } from './pages/oauth2-authentication/oauth2-authentication.component';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NavBarModule, UiManagerModule, WorkspaceCommonModule } from 'sqtm-core';
import { NzIconModule } from 'ng-zorro-antd/icon';

export const routes: Routes = [
  {
    path: 'authentication',
    component: Oauth2AuthenticationComponent,
  },
];

@NgModule({
  declarations: [Oauth2AuthenticationComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule,
    UiManagerModule,
    NavBarModule,
    WorkspaceCommonModule,
    NzIconModule,
  ],
})
export class Oauth2PagesModule {}
