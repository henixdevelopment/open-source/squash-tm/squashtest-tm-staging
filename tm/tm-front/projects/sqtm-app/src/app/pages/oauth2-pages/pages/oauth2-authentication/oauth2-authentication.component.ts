import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { InterWindowCommunicationService, InterWindowMessages } from 'sqtm-core';
import { filter, map, take, takeUntil } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Component({
  selector: 'sqtm-app-oauth2-authentication',
  templateUrl: './oauth2-authentication.component.html',
  styleUrls: ['./oauth2-authentication.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class Oauth2AuthenticationComponent implements OnInit, OnDestroy {
  private unsub$ = new Subject<void>();
  private _tokenRequestStatus: BehaviorSubject<TokenRequestStatus>;
  tokenRequestStatus$: Observable<TokenRequestStatus>;

  constructor(
    public readonly interWindowCommunicationService: InterWindowCommunicationService,
    private route: ActivatedRoute,
    private cdref: ChangeDetectorRef,
  ) {
    this._tokenRequestStatus = new BehaviorSubject<TokenRequestStatus>(
      TokenRequestStatus.IN_PROGRESS,
    );
    this.tokenRequestStatus$ = this._tokenRequestStatus.asObservable();
  }

  ngOnInit(): void {
    this.transmitOauth2SecretCode();
    this.initOauth2Observers();
  }

  private transmitOauth2SecretCode() {
    this.route.queryParams.pipe(map((params) => params.code)).subscribe((code) => {
      this.interWindowCommunicationService.sendMessage(
        new InterWindowMessages('OAUTH2-SECRET-CODE-RETRIEVED', { code }),
      );
    });
  }

  private initOauth2Observers() {
    this.initOauth2SuccessObserver();
    this.initOauth2FailureObserver();
  }

  private initOauth2SuccessObserver() {
    this.interWindowCommunicationService.interWindowMessages$
      .pipe(
        takeUntil(this.unsub$),
        filter((message: InterWindowMessages) => message.isTypeOf('OAUTH2-SUCCESS')),
        take(1),
      )
      .subscribe(() => {
        this._tokenRequestStatus.next(TokenRequestStatus.SUCCESS);
        this.cdref.detectChanges();
      });
  }

  private initOauth2FailureObserver() {
    this.interWindowCommunicationService.interWindowMessages$
      .pipe(
        takeUntil(this.unsub$),
        filter((message: InterWindowMessages) => message.isTypeOf('OAUTH2-FAILURE')),
        take(1),
      )
      .subscribe(() => {
        this._tokenRequestStatus.next(TokenRequestStatus.FAILURE);
        this.cdref.detectChanges();
      });
  }

  isInProgressStatus(): Observable<boolean> {
    return this.tokenRequestStatus$.pipe(
      takeUntil(this.unsub$),
      map((tokenStatus: TokenRequestStatus) => {
        return tokenStatus === TokenRequestStatus.IN_PROGRESS;
      }),
    );
  }

  isSuccessStatus(): Observable<boolean> {
    return this.tokenRequestStatus$.pipe(
      takeUntil(this.unsub$),
      map((tokenStatus: TokenRequestStatus) => tokenStatus === TokenRequestStatus.SUCCESS),
    );
  }

  isFailureStatus(): Observable<boolean> {
    return this.tokenRequestStatus$.pipe(
      takeUntil(this.unsub$),
      map((tokenStatus: TokenRequestStatus) => {
        return tokenStatus === TokenRequestStatus.FAILURE;
      }),
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}

enum TokenRequestStatus {
  IN_PROGRESS = 'IN_PROGRESS',
  SUCCESS = 'SUCCESS',
  FAILURE = 'FAILURE',
}
