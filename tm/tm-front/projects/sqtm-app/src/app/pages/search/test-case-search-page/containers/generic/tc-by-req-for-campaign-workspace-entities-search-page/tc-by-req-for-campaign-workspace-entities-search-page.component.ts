import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  ViewContainerRef,
} from '@angular/core';
import {
  convertAllRowsInTestCase,
  TEST_CASE_RESEARCH_GRID,
  TEST_CASE_RESEARCH_GRID_CONFIG,
} from '../../test-case-search-grid.builders';
import {
  BindableEntity,
  GridColumnId,
  GridId,
  GridService,
  gridServiceFactory,
  LocalPersistenceService,
  ReferentialDataService,
  RestService,
  ScopeDefinitionBuilder,
  searchGrid,
  Sort,
  StyleDefinitionBuilder,
  UserHistorySearchProvider,
} from 'sqtm-core';
import { AbstractTestCaseForCampaignWorkspaceEntitiesSearchPageComponent } from '../../abstract-test-case-for-campaign-workspace-entities-search-page.component';
import { ActivatedRoute, Router } from '@angular/router';
import { RequirementSearchGridBuilders } from '../../../../requirement-search-page/containers/requirement-search-grid.builders';
import { Overlay } from '@angular/cdk/overlay';
import { colDefBuilderTestCaseSearch } from '../../abstract-test-case-search.component';
import { Observable } from 'rxjs';
import { RequirementSearchModel } from '../../../../requirement-search-page/services/requirement-search-model';
import { RequirementSearchViewService } from '../../../../requirement-search-page/services/requirement-search-view.service';

export const searchTestCaseGridByRequirementConfigFactory = (
  localPersistenceService: LocalPersistenceService,
) => {
  return searchGrid(GridId.TC_BY_REQUIREMENT)
    .server()
    .withServerUrl(['search/test-case/by-requirement'])
    .withModificationUrl(['test-case'])
    .withRowConverter(convertAllRowsInTestCase)
    .withFilterCufFrom(BindableEntity.REQUIREMENT_VERSION)
    .withColumns(colDefBuilderTestCaseSearch(true))
    .withInitialSortedColumns([
      { id: GridColumnId.projectName, sort: Sort.ASC },
      { id: GridColumnId.reference, sort: Sort.ASC },
      { id: GridColumnId.name, sort: Sort.ASC },
    ])
    .withStyle(new StyleDefinitionBuilder().enableInitialLoadAnimation())
    .withScopeDefinition(new ScopeDefinitionBuilder().withCustomScope('requirement'))
    .showExtendedScopeOptionInFilterPanel()
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
};

@Component({
  selector: 'sqtm-app-test-case-by-req-for-campaign-workspace-entities-search-page',
  templateUrl: './tc-by-req-for-campaign-workspace-entities-search-page.component.html',
  styleUrls: ['./tc-by-req-for-campaign-workspace-entities-search-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: TEST_CASE_RESEARCH_GRID_CONFIG,
      useFactory: searchTestCaseGridByRequirementConfigFactory,
      deps: [LocalPersistenceService],
    },
    {
      provide: TEST_CASE_RESEARCH_GRID,
      useFactory: gridServiceFactory,
      deps: [RestService, TEST_CASE_RESEARCH_GRID_CONFIG, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: TEST_CASE_RESEARCH_GRID,
    },
    {
      provide: RequirementSearchViewService,
      useClass: RequirementSearchViewService,
      deps: [RestService],
    },
    {
      provide: UserHistorySearchProvider,
      useExisting: RequirementSearchViewService,
    },
  ],
})
export class TcByReqForCampaignWorkspaceEntitiesSearchPageComponent extends AbstractTestCaseForCampaignWorkspaceEntitiesSearchPageComponent {
  @Input()
  workspaceName: string;

  @Input()
  titleKey: string;

  @Input()
  containerType: string;

  @Input()
  url: string;

  constructor(
    referentialDataService: ReferentialDataService,
    gridService: GridService,
    overlay: Overlay,
    localPersistenceService: LocalPersistenceService,
    cdRef: ChangeDetectorRef,
    vcr: ViewContainerRef,
    activatedRoute: ActivatedRoute,
    router: Router,
    private restService: RestService,
    private requirementSearchViewService: RequirementSearchViewService,
  ) {
    super(
      referentialDataService,
      gridService,
      overlay,
      localPersistenceService,
      cdRef,
      vcr,
      activatedRoute,
      router,
    );
  }

  protected persistTestCasesInEntity(testCaseIds: number[]) {
    const containerId = this.activatedRoute.snapshot.paramMap.get(this.containerType);
    const url = `${this.url}/${Number.parseInt(containerId, 10)}/test-plan-items`;
    return this.restService.post<void>([url], { testCaseIds });
  }

  protected searchGridBuilders(): any {
    return new RequirementSearchGridBuilders();
  }

  protected loadResearchData(): Observable<RequirementSearchModel> {
    return this.requirementSearchViewService.loadResearchData();
  }
}
