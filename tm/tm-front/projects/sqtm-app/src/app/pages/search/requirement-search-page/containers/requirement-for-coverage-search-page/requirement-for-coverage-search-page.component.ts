import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewContainerRef,
} from '@angular/core';
import { searchRequirementGridConfigFactory } from '../requirement-search-grid.builders';
import {
  ChangeCoverageOperationReport,
  createTestCaseCoverageMessageDialogConfiguration,
  DataRow,
  DialogService,
  GridNode,
  GridService,
  gridServiceFactory,
  LocalPersistenceService,
  ReferentialDataService,
  RestService,
  shouldShowCoverageMessageDialog,
  TestCaseService,
  TestStepService,
  UserHistorySearchProvider,
} from 'sqtm-core';
import { RequirementSearchViewService } from '../../services/requirement-search-view.service';
import {
  AbstractRequirementSearchComponent,
  REQUIREMENT_RESEARCH_GRID,
  REQUIREMENT_RESEARCH_GRID_CONFIG,
} from '../abstract-requirement-search.component';
import { concatMap, map, take } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { BACK_URL_PARAM } from '../../../search-constants';
import { Overlay } from '@angular/cdk/overlay';

@Component({
  selector: 'sqtm-app-requirement-for-coverage-search-page',
  templateUrl: './requirement-for-coverage-search-page.component.html',
  styleUrls: ['./requirement-for-coverage-search-page.component.less'],
  providers: [
    {
      provide: REQUIREMENT_RESEARCH_GRID_CONFIG,
      useFactory: searchRequirementGridConfigFactory,
      deps: [LocalPersistenceService],
    },
    {
      provide: REQUIREMENT_RESEARCH_GRID,
      useFactory: gridServiceFactory,
      deps: [RestService, REQUIREMENT_RESEARCH_GRID_CONFIG, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: REQUIREMENT_RESEARCH_GRID,
    },
    {
      provide: RequirementSearchViewService,
      useClass: RequirementSearchViewService,
      deps: [RestService],
    },
    {
      provide: UserHistorySearchProvider,
      useExisting: RequirementSearchViewService,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RequirementForCoverageSearchPageComponent
  extends AbstractRequirementSearchComponent
  implements OnInit, OnDestroy
{
  private testCaseId = this.activatedRoute.snapshot.paramMap.get('testCaseId');
  private stepId = this.activatedRoute.snapshot.paramMap.get('stepId');

  constructor(
    referentialDataService: ReferentialDataService,
    requirementSearchViewService: RequirementSearchViewService,
    gridService: GridService,
    localPersistenceService: LocalPersistenceService,
    overlay: Overlay,
    cdRef: ChangeDetectorRef,
    vcr: ViewContainerRef,
    private testCaseService: TestCaseService,
    private testStepService: TestStepService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private dialogService: DialogService,
  ) {
    super(
      referentialDataService,
      gridService,
      overlay,
      localPersistenceService,
      cdRef,
      vcr,
      requirementSearchViewService,
    );
  }

  ngOnInit() {
    super.ngOnInit();
  }

  linkSelection() {
    this.linkFromObservable(this.gridService.selectedRows$);
  }

  linkAll() {
    this.linkFromObservable(
      this.gridService.gridNodes$.pipe(map((nodes) => this.getDataRows(nodes))),
    );
  }

  private getDataRows(nodes: GridNode[]) {
    return nodes.map((node) => node.dataRow);
  }

  linkFromObservable(obs: Observable<DataRow[]>) {
    obs
      .pipe(
        take(1),
        map((rows) => this.extractRequirementIds(rows)),
        concatMap((reqVersionIds) => {
          if (this.stepId) {
            return this.persistNewStepCoverages(reqVersionIds);
          } else {
            return this.persistNewCoverages(reqVersionIds);
          }
        }),
      )
      .subscribe((operationReport) => {
        if (shouldShowCoverageMessageDialog(operationReport)) {
          this.showCoverageReport(operationReport);
        } else {
          this.navigateBack();
        }
      });
  }

  navigateBack() {
    const backUrl =
      this.activatedRoute.snapshot.queryParamMap.get(BACK_URL_PARAM) || 'test-case-workspace';
    this.router.navigate([backUrl]);
  }

  private showCoverageReport(operationReport: ChangeCoverageOperationReport) {
    this.dialogService.openDialog(
      createTestCaseCoverageMessageDialogConfiguration(operationReport),
    );
  }

  private persistNewCoverages(requirementIds: number[]): Observable<ChangeCoverageOperationReport> {
    return this.testCaseService.persistCoveragesFromSearchPage(
      Number.parseInt(this.testCaseId, 10),
      requirementIds,
    );
  }

  private persistNewStepCoverages(
    requirementIds: number[],
  ): Observable<ChangeCoverageOperationReport> {
    return this.testStepService.persistStepCoveragesFromSearchPage(
      requirementIds,
      Number.parseInt(this.testCaseId, 10),
      Number.parseInt(this.stepId, 10),
    );
  }

  private extractRequirementIds(rows: DataRow[]) {
    return rows.map((row) => row.data.id);
  }
}
