import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewContainerRef,
} from '@angular/core';
import {
  DataRow,
  DialogService,
  GridColumnId,
  GridService,
  gridServiceFactory,
  Identifier,
  LocalPersistenceService,
  Milestone,
  MilestoneView,
  ReferentialDataService,
  RestService,
  TestCaseKind,
  TestCaseSearchMilestoneMassEdit,
  UserHistorySearchProvider,
} from 'sqtm-core';
import { concatMap, filter, map, take, withLatestFrom } from 'rxjs/operators';
import { MilestoneDialogConfiguration } from '../../../../../components/milestone/milestone-dialog/milestone.dialog.configuration';
import { MilestoneDialogComponent } from '../../../../../components/milestone/milestone-dialog/milestone-dialog.component';
import { combineLatest, Observable } from 'rxjs';
import { SearchTestCaseExportDialogConfiguration } from '../../components/search-test-case-export-dialog/search-test-case-export-dialog-configuration';
import { SearchTestCaseExportDialogComponent } from '../../components/search-test-case-export-dialog/search-test-case-export-dialog.component';
import { TestCaseMultiEditDialogComponent } from '../../components/test-case-multi-edit-dialog/test-case-multi-edit-dialog.component';
import {
  searchTestCaseGridConfigFactory,
  TEST_CASE_RESEARCH_GRID,
  TEST_CASE_RESEARCH_GRID_CONFIG,
} from '../test-case-search-grid.builders';
import { TestCaseSearchViewService } from '../../services/test-case-search-view.service';
import { AbstractTestCaseSearchComponent } from '../abstract-test-case-search.component';
import { Dictionary } from '@ngrx/entity';
import { TestCaseMultiEditConfiguration } from '../../components/test-case-multi-edit-dialog/test-case-multi-edit.configuration';
import { Overlay } from '@angular/cdk/overlay';
import { TestCaseSearchModel } from '../../services/test-case-search-model';

@Component({
  selector: 'sqtm-app-test-case-simple-search-page',
  templateUrl: './test-case-simple-search-page.component.html',
  styleUrls: ['./test-case-simple-search-page.component.less'],
  providers: [
    {
      provide: TEST_CASE_RESEARCH_GRID_CONFIG,
      useFactory: searchTestCaseGridConfigFactory,
      deps: [LocalPersistenceService],
    },
    {
      provide: TEST_CASE_RESEARCH_GRID,
      useFactory: gridServiceFactory,
      deps: [RestService, TEST_CASE_RESEARCH_GRID_CONFIG, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: TEST_CASE_RESEARCH_GRID,
    },
    {
      provide: TestCaseSearchViewService,
      useClass: TestCaseSearchViewService,
      deps: [RestService],
    },
    {
      provide: UserHistorySearchProvider,
      useExisting: TestCaseSearchViewService,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestCaseSimpleSearchPageComponent
  extends AbstractTestCaseSearchComponent
  implements OnInit
{
  constructor(
    referentialDataService: ReferentialDataService,
    gridService: GridService,
    overlay: Overlay,
    localPersistenceService: LocalPersistenceService,
    cdRef: ChangeDetectorRef,
    vcr: ViewContainerRef,
    private dialogService: DialogService,
    private restService: RestService,
    private testCaseSearchViewService: TestCaseSearchViewService,
  ) {
    super(referentialDataService, gridService, overlay, localPersistenceService, cdRef, vcr);
  }

  protected loadResearchData(): Observable<TestCaseSearchModel> {
    return this.testCaseSearchViewService.loadResearchViewData();
  }

  massEdit() {
    this.gridService.selectedRows$.pipe(take(1)).subscribe((dataRows) => {
      const editableDataRows = this.editableTestCaseIds(dataRows);
      const hasOnlyExploratoryRows = dataRows.every(
        (row) => row.data[GridColumnId.kind] === TestCaseKind.EXPLORATORY.id,
      );
      if (editableDataRows.length > 0) {
        const projectIds = new Set(editableDataRows.map((dataRow) => dataRow.projectId));
        const writingRight = editableDataRows.length === dataRows.length;
        const testCaseIds = editableDataRows.map((dataRow) => dataRow.data[GridColumnId.id]);
        this.showMassEditDialog(testCaseIds, projectIds, writingRight, hasOnlyExploratoryRows);
      } else {
        this.showCantEditDialog('sqtm-core.search.generic.modify.no-line-with-writing-rights');
      }
    });
  }

  massEditMilestones() {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        map((rows: DataRow[]) => this.editableDataRows(rows)),
      )
      .subscribe((rows) => {
        if (rows.length > 0) {
          this.displayMassBindingMilestonesDialog(rows);
        } else {
          this.showCantEditDialog('sqtm-core.search.generic.modify.no-line-with-writing-rights');
        }
      });
  }

  displayMassBindingMilestonesDialog(rows: DataRow[]) {
    const testCaseIds = rows.map((row) => row.data[GridColumnId.id]);
    return this.restService
      .get<TestCaseSearchMilestoneMassEdit>(['search/milestones/test-case', testCaseIds.toString()])
      .pipe(
        map((massEditMilestone: TestCaseSearchMilestoneMassEdit) => {
          massEditMilestone.testCaseIds = testCaseIds;
          return massEditMilestone;
        }),
        concatMap((massEditMilestone: TestCaseSearchMilestoneMassEdit) => {
          const milestoneIds = massEditMilestone.milestoneIds;
          return this.referentialDataService.findMilestones(milestoneIds).pipe(
            take(1),
            map((milestones: Milestone[]) => {
              const result: MilestoneMassEditView = {
                ...massEditMilestone,
                milestoneViews: this.convertToMilestoneViews(milestones, massEditMilestone),
              };
              return result;
            }),
          );
        }),
      )
      .subscribe((milestoneView) => {
        if (milestoneView.milestoneViews.length > 0) {
          this.showMilestoneDialog(milestoneView);
        } else if (milestoneView.milestoneViews.length === 0) {
          this.showCantEditDialog(
            'sqtm-core.search.generic.modify.milestone.wrong-perimeter.test-case',
          );
        } else {
          this.showCantEditDialog('sqtm-core.search.generic.modify.no-line-with-writing-rights');
        }
      });
  }

  showCantEditDialog(messageKey: string) {
    this.dialogService.openAlert({
      titleKey: 'sqtm-core.generic.label.information.singular',
      messageKey,
      level: 'INFO',
    });
  }

  showMilestoneDialog(massEditMilestone: MilestoneMassEditView) {
    const configuration: MilestoneDialogConfiguration = {
      id: 'search-milestone-dialog',
      titleKey: 'sqtm-core.search.generic.modify.milestone.title',
      checkedIds: massEditMilestone.checkedIds,
      milestoneViews: massEditMilestone.milestoneViews,
      samePerimeter: massEditMilestone.samePerimeter,
    };

    const milestoneRef = this.dialogService.openDialog({
      id: 'research-milestone',
      component: MilestoneDialogComponent,
      data: { ...configuration },
      height: 400,
      width: 800,
      viewContainerReference: this.vcr,
    });

    milestoneRef.dialogClosed$
      .pipe(
        take(1),
        filter((result) => result != null),
        concatMap((result) => {
          const data = {};
          data[GridColumnId.milestoneIds] = result;
          data[GridColumnId.bindableObjectIds] = massEditMilestone.testCaseIds;
          return this.restService.post(['search/milestones/test-case'], data);
        }),
      )
      .subscribe(() => {
        this.gridService.refreshData();
      });
  }

  showExportDialog() {
    combineLatest([this.gridService.gridNodes$, this.gridService.selectedRows$])
      .pipe(
        take(1),
        map(([rows, selectedRows]) => this.getExportedRowIds(selectedRows, rows)),
        withLatestFrom(this.gridService.gridState$),
      )
      .subscribe(([ids, gridState]) => {
        this.dialogService.openDialog<SearchTestCaseExportDialogConfiguration, any>({
          id: 'search-test-case-export',
          component: SearchTestCaseExportDialogComponent,
          viewContainerReference: this.vcr,
          data: {
            id: 'search-test-case-export',
            nodes: ids,
            simplifiedColumnDisplayGridIds:
              gridState.configurationState.simplifiedColumnDisplayGridIds,
          },
          width: 600,
        });
      });
  }

  private getExportedRowIds(selectedRows, rows) {
    if (this.hasSelection(selectedRows)) {
      return this.getSelectedRowIds(selectedRows);
    } else {
      return this.getAllRowIds(rows);
    }
  }

  private getAllRowIds(rows) {
    return rows.map((node) => Number.parseInt(node.id.toString(), 10));
  }

  private getSelectedRowIds(selectedRows) {
    return selectedRows.map((dataRow) => Number.parseInt(dataRow.id.toString(), 10));
  }

  private hasSelection(selectedRows) {
    return selectedRows.length > 0;
  }

  showMassEditDialog(
    testCaseIds: number[],
    projectIds: Set<number>,
    writingRight: boolean,
    hasOnlyExploratoryRows: boolean,
  ) {
    const dialogReference = this.dialogService.openDialog<TestCaseMultiEditConfiguration, boolean>({
      id: 'mass-edit',
      component: TestCaseMultiEditDialogComponent,
      data: {
        id: 'mass-edit',
        titleKey: 'sqtm-core.search.generic.modify.selection',
        projectIds: projectIds,
        testCaseIds: testCaseIds,
        writingRightOnLine: writingRight,
        selectedRowsOnlyExploratory: hasOnlyExploratoryRows,
      },
      width: TestCaseMultiEditDialogComponent.DIALOG_WIDTH,
    });
    dialogReference.dialogClosed$.pipe(take(1)).subscribe((result) => {
      if (result) {
        this.gridService.refreshData();
      }
    });
  }

  editableTestCaseIds(dataRows: DataRow[]): DataRow[] {
    return dataRows
      .filter((row) => row.simplePermissions.canWrite)
      .filter((row) => {
        const tcLockedMilestones = row.data[GridColumnId.tcMilestoneLocked];
        return tcLockedMilestones === 0;
      });
  }

  editableDataRows(dataRows: DataRow[]): DataRow[] {
    return dataRows.filter((row) => row.simplePermissions.canWrite);
  }

  convertToMilestoneViews(
    milestones: Milestone[],
    massEdit: TestCaseSearchMilestoneMassEdit,
  ): MilestoneView[] {
    const milestonesViews: MilestoneView[] = [];

    milestones.forEach((milestone) => {
      const mv: MilestoneView = {
        ...milestone,
        boundToObject: massEdit.checkedIds.includes(milestone.id),
      };
      milestonesViews.push(mv);
    });
    return milestonesViews;
  }

  canExport(): Observable<boolean> {
    return combineLatest([this.gridService.dataRows$, this.gridService.selectedRows$]).pipe(
      take(1),
      map(([rows, selectedRows]) => {
        return this.checkExportPermissions(rows, selectedRows);
      }),
    );
  }

  private checkExportPermissions(rows: Dictionary<DataRow>, selectedRows: DataRow[]) {
    if (this.hasSelection(selectedRows)) {
      return selectedRows.some((r) => r.simplePermissions.canExport);
    } else {
      const dataRows = Object.values(rows);
      return dataRows.some((r) => r.simplePermissions.canExport);
    }
  }
}

export interface MilestoneMassEditView {
  checkedIds: number[];
  samePerimeter: boolean;
  milestoneViews: MilestoneView[];
  testCaseIds: Identifier[];
}
