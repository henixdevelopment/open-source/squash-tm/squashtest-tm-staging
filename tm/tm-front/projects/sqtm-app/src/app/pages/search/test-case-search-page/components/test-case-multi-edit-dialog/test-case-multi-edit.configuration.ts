export class TestCaseMultiEditConfiguration {
  id: string;
  titleKey: string;
  projectIds: Set<number>;
  testCaseIds: number[];
  selectedRowsOnlyExploratory: boolean;

  // 'WRITE' permission on row
  writingRightOnLine: boolean;
}
