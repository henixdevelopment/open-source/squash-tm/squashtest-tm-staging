import { ChangeDetectionStrategy, Component, Input, OnDestroy } from '@angular/core';
import {
  createStore,
  EntityScope,
  FilterGroup,
  GridFilter,
  GridService,
  isMilestoneModeActivated,
  MilestoneModeData,
  MultiDiscreteFilterValue,
  Permissions,
  permissionWorkspaceEntityReferences,
  ProjectData,
  ProjectReference,
  ReferentialDataService,
  ReferentialDataState,
  Scope,
  SimpleFilter,
  SimpleScope,
  Store,
} from 'sqtm-core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { combineLatest, Subject } from 'rxjs';
import { map, take, takeUntil } from 'rxjs/operators';
import { searchViewLogger } from '../../search-view.logger';
import {
  FILTER_QUERY_PARAM_KEY,
  MILESTONE_END_DATE,
  MILESTONE_FILTER,
  MILESTONE_STATUS,
} from '../../../search-constants';
import {
  provideInitialSearchViewState,
  SearchViewState,
  selectUiState,
} from '../../state/search-view.state';
import { select } from '@ngrx/store';

const logger = searchViewLogger.compose('SearchViewComponent');

@Component({
  selector: 'sqtm-app-search-view',
  templateUrl: './search-view.component.html',
  styleUrls: ['./search-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchViewComponent implements OnDestroy {
  @Input()
  workspaceName: string;

  @Input()
  titleKey: string;

  @Input()
  resultTitleKey = 'sqtm-core.search.generic.results.title';

  @Input()
  filters: GridFilter[] = [];

  @Input()
  filterGroups: FilterGroup[] = [];

  private unsub$ = new Subject<void>();

  private store: Store<SearchViewState> = createStore(provideInitialSearchViewState());

  private state$ = this.store.state$;

  uiState$ = this.state$.pipe(select(selectUiState));

  constructor(
    public gridService: GridService,
    private referentialDataService: ReferentialDataService,
    private activatedRoute: ActivatedRoute,
    public router: Router,
  ) {}

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  toggleFilterPanel() {
    this.store.state$
      .pipe(
        take(1),
        map((state: SearchViewState) => ({
          ...state,
          uiState: { ...state.uiState, showFilterManager: !state.uiState.showFilterManager },
        })),
      )
      .subscribe((state) => this.store.commit(state));
  }

  initializeResearch(refData: ReferentialDataState) {
    combineLatest([
      this.referentialDataService.filteredProjects$,
      this.referentialDataService.milestoneModeData$,
      this.activatedRoute.queryParamMap,
    ])
      .pipe(takeUntil(this.unsub$))
      .subscribe(([projects, milestoneModeData, paramMap]) => {
        logger.debug('Initialize research with param map ', [paramMap]);
        const filters = this.initializeFilters(paramMap, milestoneModeData);
        const scope = this.initializeScope(projects, paramMap);
        this.initializeGrid(filters, scope, refData);
      });
  }

  private initializeScope(projects: ProjectData[], paramMap: ParamMap) {
    const projectScope = this.extractProjectScope(projects);
    const scope = this.initializeScopeFromUrl(paramMap, projectScope);
    logger.debug('Initialize research with perimeter ', [scope]);
    return scope;
  }

  private initializeFilters(paramMap: ParamMap, milestoneModeData: MilestoneModeData) {
    const filtersMap = this.buildFilterMap(this.filters);
    this.appendFiltersFromUrlParams(filtersMap, paramMap);
    const milestoneLabelFilter: GridFilter = filtersMap.get(MILESTONE_FILTER);

    if (isMilestoneModeActivated(milestoneModeData)) {
      const value: MultiDiscreteFilterValue = {
        kind: 'multiple-discrete-value',
        value: [
          {
            id: milestoneModeData.selectedMilestone.id,
            label: milestoneModeData.selectedMilestone.label,
          },
        ],
      };
      milestoneLabelFilter.value = value;
      milestoneLabelFilter.initialValue = value;
      milestoneLabelFilter.active = true;
      milestoneLabelFilter.alwaysActive = true;
      milestoneLabelFilter.preventOpening = true;
      milestoneLabelFilter.lockedValue = true;

      filtersMap.delete(MILESTONE_STATUS);
      filtersMap.delete(MILESTONE_END_DATE);
    } else {
      milestoneLabelFilter.value = { kind: 'multiple-discrete-value', value: [] };
      milestoneLabelFilter.initialValue = { kind: 'multiple-discrete-value', value: [] };
      milestoneLabelFilter.active = false;
      milestoneLabelFilter.alwaysActive = false;
      milestoneLabelFilter.lockedValue = false;
    }
    return Array.from(filtersMap.values());
  }

  private initializeScopeFromUrl(paramMap: ParamMap, projectScope: EntityScope[]) {
    let scope: Scope;
    if (paramMap.has('scope')) {
      const simpleScope = JSON.parse(paramMap.get('scope')) as SimpleScope;
      scope = { ...simpleScope, active: true, initialValue: projectScope, initialKind: 'project' };
    } else {
      scope = {
        kind: 'project',
        value: projectScope,
        initialValue: projectScope,
        initialKind: 'project',
        active: true,
      };
    }
    return scope;
  }

  private initializeGrid(filters: GridFilter[], scope: Scope, refData: ReferentialDataState) {
    if (!refData.globalConfigurationState.searchActivationFeatureEnabled) {
      this.gridService.replaceFilters(filters, scope, this.filterGroups, refData);
    } else {
      this.gridService.replaceFilterOnSearchPageWithoutInitialRequest(
        filters,
        scope,
        this.filterGroups,
      );
    }
  }

  private extractProjectScope(projects: ProjectData[]) {
    const getLibraryByWorkspace = (value: string): string | undefined => {
      return Object.entries(permissionWorkspaceEntityReferences).find(([_, v]) => v === value)?.[0];
    };
    const workspace: string = this.workspaceName.replace('-workspace', '');
    const library: string = getLibraryByWorkspace(workspace);
    return projects
      .filter((p: ProjectData) => {
        const permissions: Permissions[] = p.permissions[library];
        return permissions?.includes(Permissions.READ);
      })
      .map((p: ProjectData) => {
        const ref = new ProjectReference(p.id).asString();
        return { id: ref, label: p.name, projectId: p.id };
      });
  }

  private appendFiltersFromUrlParams(filtersMap: Map<string, GridFilter>, paramMap: ParamMap) {
    if (paramMap.has(FILTER_QUERY_PARAM_KEY)) {
      const simpleFilters: SimpleFilter[] = JSON.parse(paramMap.get(FILTER_QUERY_PARAM_KEY));
      simpleFilters.forEach((simpleFilter) => {
        const gridFilter: GridFilter = filtersMap.get(simpleFilter.id.toString());
        gridFilter.value = simpleFilter.value;
        gridFilter.operation = simpleFilter.operation;
        gridFilter.active = true;
        gridFilter.preventOpening = true;
      });
    }
  }

  private buildFilterMap(filters: GridFilter[]) {
    return filters.reduce((filterMap, f) => {
      filterMap.set(f.id, f);
      return filterMap;
    }, new Map());
  }
}
