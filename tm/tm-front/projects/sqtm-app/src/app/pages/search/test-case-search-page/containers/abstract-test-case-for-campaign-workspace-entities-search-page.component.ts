import { AbstractTestCaseSearchComponent } from './abstract-test-case-search.component';
import { ChangeDetectorRef, Directive, OnDestroy, OnInit, ViewContainerRef } from '@angular/core';
import {
  DataRow,
  GridNode,
  GridService,
  LocalPersistenceService,
  ReferentialDataService,
} from 'sqtm-core';
import { ActivatedRoute, Router } from '@angular/router';
import { concatMap, map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { BACK_URL_PARAM } from '../../search-constants';
import { Overlay } from '@angular/cdk/overlay';

@Directive()
export abstract class AbstractTestCaseForCampaignWorkspaceEntitiesSearchPageComponent
  extends AbstractTestCaseSearchComponent
  implements OnInit, OnDestroy
{
  protected constructor(
    referentialDataService: ReferentialDataService,
    gridService: GridService,
    overlay: Overlay,
    localPersistenceService: LocalPersistenceService,
    cdRef: ChangeDetectorRef,
    vcr: ViewContainerRef,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
  ) {
    super(referentialDataService, gridService, overlay, localPersistenceService, cdRef, vcr);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  linkSelection() {
    this.linkFromObservable(this.gridService.selectedRows$);
  }

  linkAll() {
    this.linkFromObservable(
      this.gridService.gridNodes$.pipe(map((nodes: GridNode[]) => this.getDataRows(nodes))),
    );
  }

  protected getDataRows(nodes: GridNode[]) {
    return nodes.map((node) => node.dataRow);
  }

  linkFromObservable(obs: Observable<DataRow[]>) {
    obs
      .pipe(
        take(1),
        map((rows) => this.extractTestCaseIds(rows)),
        concatMap((testCaseIds) => this.persistTestCasesInEntity(testCaseIds)),
      )
      .subscribe(() => this.navigateBack());
  }

  protected extractTestCaseIds(rows: DataRow[]) {
    return rows.map((row) => row.data.id);
  }

  navigateBack() {
    const backUrl =
      this.activatedRoute.snapshot.queryParamMap.get(BACK_URL_PARAM) || 'campaign-workspace';
    this.router.navigate([backUrl]);
  }

  protected abstract persistTestCasesInEntity(testCaseIds: number[]);
}
