import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  ViewContainerRef,
} from '@angular/core';
import {
  BindableEntity,
  GridColumnId,
  GridId,
  GridService,
  gridServiceFactory,
  LocalPersistenceService,
  ReferentialDataService,
  RestService,
  ScopeDefinitionBuilder,
  searchGrid,
  Sort,
  StyleDefinitionBuilder,
  UserHistorySearchProvider,
} from 'sqtm-core';
import {
  convertAllRowsInTestCase,
  TEST_CASE_RESEARCH_GRID,
  TEST_CASE_RESEARCH_GRID_CONFIG,
} from '../../test-case-search-grid.builders';
import { TestCaseSearchViewService } from '../../../services/test-case-search-view.service';
import { AbstractTestCaseForCampaignWorkspaceEntitiesSearchPageComponent } from '../../abstract-test-case-for-campaign-workspace-entities-search-page.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Overlay } from '@angular/cdk/overlay';
import { colDefBuilderTestCaseSearch } from '../../abstract-test-case-search.component';
import { Observable } from 'rxjs';
import { TestCaseSearchModel } from '../../../services/test-case-search-model';

export const searchTestCaseGridConfigFactory = (
  localPersistenceService: LocalPersistenceService,
) => {
  return searchGrid(GridId.TEST_CASE_SEARCH)
    .server()
    .withServerUrl(['search/test-case'])
    .withModificationUrl(['test-case'])
    .withRowConverter(convertAllRowsInTestCase)
    .withFilterCufFrom(BindableEntity.TEST_CASE)
    .withColumns(colDefBuilderTestCaseSearch(true))
    .withInitialSortedColumns([
      { id: GridColumnId.projectName, sort: Sort.ASC },
      { id: 'reference', sort: Sort.ASC },
      { id: 'name', sort: Sort.ASC },
    ])
    .withStyle(new StyleDefinitionBuilder().enableInitialLoadAnimation())
    .withScopeDefinition(new ScopeDefinitionBuilder().withCustomScope('test-case'))
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
};

@Component({
  selector: 'sqtm-app-test-case-for-campaign-workspace-entities-search-page',
  templateUrl: './test-case-for-campaign-workspace-entities-search-page.component.html',
  styleUrls: ['./test-case-for-campaign-workspace-entities-search-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: TEST_CASE_RESEARCH_GRID_CONFIG,
      useFactory: searchTestCaseGridConfigFactory,
      deps: [LocalPersistenceService],
    },
    {
      provide: TEST_CASE_RESEARCH_GRID,
      useFactory: gridServiceFactory,
      deps: [RestService, TEST_CASE_RESEARCH_GRID_CONFIG, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: TEST_CASE_RESEARCH_GRID,
    },
    {
      provide: TestCaseSearchViewService,
      useClass: TestCaseSearchViewService,
      deps: [RestService],
    },
    {
      provide: UserHistorySearchProvider,
      useExisting: TestCaseSearchViewService,
    },
  ],
})
export class TestCaseForCampaignWorkspaceEntitiesSearchPageComponent extends AbstractTestCaseForCampaignWorkspaceEntitiesSearchPageComponent {
  @Input()
  workspaceName: string;

  @Input()
  titleKey: string;

  @Input()
  containerType: string;

  @Input()
  url: string;

  constructor(
    referentialDataService: ReferentialDataService,
    gridService: GridService,
    overlay: Overlay,
    localPersistenceService: LocalPersistenceService,
    cdRef: ChangeDetectorRef,
    vcr: ViewContainerRef,
    activatedRoute: ActivatedRoute,
    router: Router,
    private restService: RestService,
    private testCaseSearchViewService: TestCaseSearchViewService,
  ) {
    super(
      referentialDataService,
      gridService,
      overlay,
      localPersistenceService,
      cdRef,
      vcr,
      activatedRoute,
      router,
    );
  }

  protected loadResearchData(): Observable<TestCaseSearchModel> {
    return this.testCaseSearchViewService.loadResearchViewData();
  }

  protected persistTestCasesInEntity(testCaseIds: number[]) {
    const containerId = this.activatedRoute.snapshot.paramMap.get(this.containerType);
    const url = `${this.url}/${Number.parseInt(containerId, 10)}/test-plan-items`;
    return this.restService.post<void>([url], { testCaseIds });
  }
}
