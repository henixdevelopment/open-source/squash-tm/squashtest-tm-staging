import {
  BindableEntity,
  DataRow,
  Extendable,
  FilterGroup,
  FilterOperation,
  FilterTestCaseMilestoneLabelComponent,
  Fixed,
  GridColumnId,
  GridFilter,
  GridId,
  Limited,
  LocalPersistenceService,
  Permissions,
  ProjectDataMap,
  Requirement,
  RequirementCriticality,
  RequirementCurrentVersionFilter,
  RequirementNature,
  RequirementPermissions,
  RequirementStatus,
  ResearchColumnPrototype,
  ScopeDefinitionBuilder,
  Sort,
  SquashTmDataRow,
  StyleDefinitionBuilder,
  WithOperationFilterValueRendererComponent,
  booleanColumn,
  buildFilters,
  concatenatedDatesColumn,
  concatenatedStatusesColumn,
  dateFilter,
  dateTimeColumn,
  fullTextResearchFilter,
  i18nEnumResearchFilter,
  i18nEnumSingleSelectionResearchFilter,
  iconLinkColumn,
  idListSearchFilter,
  indexColumn,
  infoListColumn,
  infoListResearchFilter,
  isRequirementEditable,
  isRequirementStatusEditable,
  levelEnumColumn,
  multipleListResearchFilter,
  numericColumn,
  numericResearchFilter,
  searchGrid,
  textColumn,
  textResearchFilter,
  userHistoryResearchFilter,
  ColumnDefinitionBuilder,
} from 'sqtm-core';
import {
  MILESTONE_END_DATE,
  MILESTONE_FILTER,
  MILESTONE_STATUS,
  REQUIREMENT_KIND,
} from '../../search-constants';
import { FilterLinkTypeComponent } from '../components/filters/filter-link-type/filter-link-type.component';
import { requirementSearchEditableText } from '../components/cell-renderers/requirement-editable-cell-renderer/requirement-editable-cell-renderer.component';
import { navigateToRequirementColumn } from '../components/cell-renderers/navigate-to-requirement-cell/navigate-to-requirement-cell.component';
import { reqVersionBoundToItemColumn } from '../components/cell-renderers/req-version-bound-to-item-checkbox-renderer/req-version-bound-to-item-checkbox-renderer.component';

export class RequirementSearchGridBuilders {
  public buildFilters(): GridFilter[] {
    return buildFilters([
      numericResearchFilter(GridColumnId.id, ResearchColumnPrototype.REQUIREMENT_VERSION_ID)
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.requirement.version.id.full'),
      idListSearchFilter(GridColumnId.idList, ResearchColumnPrototype.REQUIREMENT_ID)
        .withGroupId('informations')
        .withI18nKey('sqtm-core.search.requirement.criteria.id-list.label'),
      textResearchFilter(
        GridColumnId.reference,
        ResearchColumnPrototype.REQUIREMENT_VERSION_REFERENCE,
      )
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.generic.reference.label'),
      textResearchFilter(GridColumnId.name, ResearchColumnPrototype.REQUIREMENT_VERSION_NAME)
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.generic.name.label'),
      fullTextResearchFilter(
        GridColumnId.description,
        ResearchColumnPrototype.REQUIREMENT_VERSION_DESCRIPTION,
      )
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.generic.description.label'),
      i18nEnumResearchFilter(REQUIREMENT_KIND, ResearchColumnPrototype.REQUIREMENT_KIND)
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.requirement.nature.label'),
      dateFilter(GridColumnId.createdOn, ResearchColumnPrototype.REQUIREMENT_VERSION_CREATED_ON)
        .withGroupId('historical')
        .withI18nKey('sqtm-core.entity.generic.created-on.feminine'),
      userHistoryResearchFilter(
        GridColumnId.createdBy,
        ResearchColumnPrototype.REQUIREMENT_VERSION_CREATED_BY,
      )
        .withGroupId('historical')
        .withI18nKey('sqtm-core.entity.generic.created-by.feminine'),
      dateFilter(GridColumnId.modifiedOn, ResearchColumnPrototype.REQUIREMENT_VERSION_MODIFIED_ON)
        .withGroupId('historical')
        .withI18nKey('sqtm-core.entity.generic.last-modified-on.feminine'),
      userHistoryResearchFilter(
        GridColumnId.modifiedBy,
        ResearchColumnPrototype.REQUIREMENT_VERSION_MODIFIED_BY,
      )
        .withGroupId('historical')
        .withI18nKey('sqtm-core.entity.generic.last-modified-by.feminine'),
      multipleListResearchFilter(
        MILESTONE_FILTER,
        ResearchColumnPrototype.REQUIREMENT_VERSION_MILESTONE_ID,
        FilterTestCaseMilestoneLabelComponent,
      )
        .withGroupId('milestones')
        .withI18nKey('sqtm-core.search.generic.criteria.milestone.name'),
      i18nEnumResearchFilter(
        MILESTONE_STATUS,
        ResearchColumnPrototype.REQUIREMENT_VERSION_MILESTONE_STATUS,
      )
        .withGroupId('milestones')
        .withI18nKey('sqtm-core.search.generic.criteria.milestone.status'),
      dateFilter(MILESTONE_END_DATE, ResearchColumnPrototype.REQUIREMENT_VERSION_MILESTONE_END_DATE)
        .withGroupId('milestones')
        .withI18nKey('sqtm-core.entity.milestone.end-date.label'),
      i18nEnumSingleSelectionResearchFilter(
        'requirementVersionCurrentVersion',
        ResearchColumnPrototype.REQUIREMENT_VERSION_CURRENT_VERSION,
        RequirementCurrentVersionFilter.ALL,
      )
        .withI18nKey('sqtm-core.search.requirement.criteria.current-version.title')
        .alwaysActive(),
      i18nEnumResearchFilter(
        GridColumnId.criticality,
        ResearchColumnPrototype.REQUIREMENT_VERSION_CRITICALITY,
      )
        .withGroupId('attributes')
        .withI18nKey('sqtm-core.entity.requirement.criticality.label'),
      infoListResearchFilter(
        GridColumnId.category,
        ResearchColumnPrototype.REQUIREMENT_VERSION_CATEGORY_ID,
      ).withI18nKey('sqtm-core.entity.requirement.category.label'),
      i18nEnumResearchFilter(
        GridColumnId.status,
        ResearchColumnPrototype.REQUIREMENT_VERSION_STATUS,
      )
        .withGroupId('attributes')
        .withI18nKey('sqtm-core.entity.generic.status.label'),
      numericResearchFilter(
        GridColumnId.attachmentCount,
        ResearchColumnPrototype.REQUIREMENT_VERSION_ATTCOUNT,
      )
        .withGroupId('content')
        .withI18nKey('sqtm-core.search.generic.criteria.attachment-count'),
      i18nEnumSingleSelectionResearchFilter(
        GridColumnId.hasDescription,
        ResearchColumnPrototype.REQUIREMENT_VERSION_HAS_DESCRIPTION,
      )
        .withGroupId('content')
        .withI18nKey('sqtm-core.search.requirement.criteria.has-description'),
      numericResearchFilter(
        GridColumnId.coveragesCount,
        ResearchColumnPrototype.REQUIREMENT_VERSION_TCCOUNT_WITH_INDIRECT,
      )
        .withGroupId('associations')
        .withI18nKey('sqtm-core.search.requirement.criteria.coverages'),
      i18nEnumSingleSelectionResearchFilter(
        GridColumnId.hasChildren,
        ResearchColumnPrototype.REQUIREMENT_VERSION_HAS_CHILDREN,
      )
        .withGroupId('associations')
        .withI18nKey('sqtm-core.search.requirement.criteria.has-children.label'),
      i18nEnumSingleSelectionResearchFilter(
        GridColumnId.hasParent,
        ResearchColumnPrototype.REQUIREMENT_VERSION_HAS_PARENT,
      )
        .withGroupId('associations')
        .withI18nKey('sqtm-core.search.requirement.criteria.has-parent.label'),
      multipleListResearchFilter(
        GridColumnId.hasLinkType,
        ResearchColumnPrototype.REQUIREMENT_VERSION_HAS_LINK_TYPE,
        FilterLinkTypeComponent,
      )
        .withGroupId('associations')
        .withI18nKey('sqtm-core.search.requirement.criteria.has-link.label')
        .withAvailableOperations([FilterOperation.AT_LEAST_ONE, FilterOperation.NONE])
        .withOperations(FilterOperation.AT_LEAST_ONE)
        .withValueRenderer(WithOperationFilterValueRendererComponent),
      i18nEnumSingleSelectionResearchFilter(
        GridColumnId.requirementBoundToHighLevelRequirement,
        ResearchColumnPrototype.REQUIREMENT_BOUND_TO_HIGH_LEVEL_REQUIREMENT,
      )
        .withGroupId('associations')
        .withI18nKey('sqtm-core.entity.requirement.high-level.label'),
      numericResearchFilter(
        GridColumnId.linkedStandardRequirement,
        ResearchColumnPrototype.HIGH_LEVEL_REQUIREMENT_LINKEDREQCOUNT,
      )
        .withGroupId('associations')
        .withI18nKey('sqtm-core.search.requirement.criteria.linked-standard-requirements'),
    ]);
  }

  public buildFilterGroups(): FilterGroup[] {
    return [
      {
        id: 'informations',
        i18nLabelKey: 'sqtm-core.generic.label.information.plural',
      },
      {
        id: 'attributes',
        i18nLabelKey: 'sqtm-core.entity.generic.attributes',
      },
      {
        id: 'historical',
        i18nLabelKey: 'sqtm-core.search.generic.criteria.groups.historical',
      },
      {
        id: 'milestones',
        i18nLabelKey: 'sqtm-core.generic.label.milestone',
      },
      {
        id: 'content',
        i18nLabelKey: 'sqtm-core.search.generic.criteria.groups.content',
      },
      {
        id: 'associations',
        i18nLabelKey: 'sqtm-core.search.generic.criteria.groups.associations',
      },
      {
        id: 'custom-fields',
        i18nLabelKey: 'sqtm-core.entity.custom-field.label.plural',
      },
    ];
  }
}

export const searchRequirementGridConfigFactory = (
  localPersistenceService: LocalPersistenceService,
) => {
  return searchGrid(GridId.REQUIREMENT_SEARCH)
    .server()
    .withServerUrl(['search/requirement'])
    .withModificationUrl(['requirement-version'])
    .withRowConverter(convertAllRowsInRequirements)
    .withFilterCufFrom(BindableEntity.REQUIREMENT_VERSION)
    .withColumns(colDefBuilderSprintReqVersion(true))
    .withInitialSortedColumns([
      { id: GridColumnId.projectName, sort: Sort.ASC },
      { id: GridColumnId.reference, sort: Sort.ASC },
      { id: GridColumnId.name, sort: Sort.ASC },
    ])
    .withStyle(new StyleDefinitionBuilder().enableInitialLoadAnimation())
    .withScopeDefinition(new ScopeDefinitionBuilder().withCustomScope('requirement'))
    .showExtendedScopeOptionInFilterPanel()
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
};

function colDefBuilderSprintReqVersion(isTestCaseWorkspace: boolean): ColumnDefinitionBuilder[] {
  const columnsToReturn: ColumnDefinitionBuilder[] = [];

  if (isTestCaseWorkspace) {
    columnsToReturn.push(reqVersionBoundToItemColumn());
  }

  columnsToReturn.push(
    indexColumn().withViewport('leftViewport'),
    textColumn(GridColumnId.projectName)
      .withI18nKey('sqtm-core.grid.header.project')
      .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_PROJECT_NAME)
      .changeWidthCalculationStrategy(new Limited(120)),
    textColumn(GridColumnId.requirementId)
      .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_ID)
      .withI18nKey('sqtm-core.entity.requirement.id.short')
      .withTitleI18nKey('sqtm-core.entity.requirement.id.full')
      .changeWidthCalculationStrategy(new Fixed(60)),
    textColumn(GridColumnId.id)
      .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_ID)
      .withI18nKey('sqtm-core.entity.requirement.version.id.short')
      .withTitleI18nKey('sqtm-core.entity.requirement.version.id.full')
      .changeWidthCalculationStrategy(new Fixed(60)),
    requirementSearchEditableText(GridColumnId.reference)
      .isEditable(isRequirementEditable)
      .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_REFERENCE)
      .withI18nKey('sqtm-core.grid.header.reference')
      .changeWidthCalculationStrategy(new Limited(95)),
    requirementSearchEditableText(GridColumnId.name)
      .isEditable(isRequirementEditable)
      .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_NAME)
      .withI18nKey('sqtm-core.grid.header.name')
      .changeWidthCalculationStrategy(new Limited(300)),
    levelEnumColumn(GridColumnId.status, RequirementStatus)
      .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_STATUS)
      .withTitleI18nKey('sqtm-core.entity.generic.status.label')
      .withI18nKey('sqtm-core.entity.generic.status.short')
      .changeWidthCalculationStrategy(new Fixed(40))
      .isEditable(isRequirementStatusEditable),
    levelEnumColumn(GridColumnId.criticality, RequirementCriticality)
      .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_CRITICALITY)
      .withTitleI18nKey('sqtm-core.entity.requirement.criticality.label')
      .withI18nKey('sqtm-core.entity.requirement.criticality.label-short-dot')
      .changeWidthCalculationStrategy(new Fixed(40))
      .isEditable(isRequirementEditable),
    infoListColumn(GridColumnId.category, {
      kind: 'infoList',
      infolist: 'requirementCategory',
    })
      .withTitleI18nKey('sqtm-core.entity.requirement.category.label')
      .withI18nKey('sqtm-core.entity.requirement.category.label-short-dot')
      .isEditable(isRequirementEditable)
      .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_CATEGORY)
      .changeWidthCalculationStrategy(new Extendable(100, 1)),
    textColumn(GridColumnId.nature)
      .withEnumRenderer(RequirementNature, false, true)
      .withI18nKey('sqtm-core.entity.requirement.nature.label')
      .disableSort()
      .changeWidthCalculationStrategy(new Extendable(90, 1)),
    booleanColumn(GridColumnId.description)
      .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_DESCRIPTION)
      .withI18nKey('sqtm-core.grid.header.description.label')
      .withTitleI18nKey('sqtm-core.grid.header.description.title')
      .changeWidthCalculationStrategy(new Fixed(55))
      .withContentPosition('left'),
    numericColumn(GridColumnId.childOfRequirement)
      .withTitleI18nKey(
        'sqtm-core.search.requirement.grid.header.number-of-child-requirements.title',
      )
      .withI18nKey('sqtm-core.search.requirement.criteria.has-children.label-short-dot')
      .disableSort()
      .changeWidthCalculationStrategy(new Fixed(75)),
    booleanColumn(GridColumnId.hasParent)
      .withTitleI18nKey('sqtm-core.search.requirement.criteria.has-parent.label')
      .withI18nKey('sqtm-core.search.requirement.criteria.has-parent.label-short-dot')
      .disableSort()
      .changeWidthCalculationStrategy(new Fixed(80))
      .withContentPosition('left'),
    booleanColumn(GridColumnId.hasLinkType)
      .withI18nKey('sqtm-core.requirement-workspace.title.requirement-version-link-short-dot')
      .withTitleI18nKey('sqtm-core.requirement-workspace.title.requirement-version-link')
      .disableSort()
      .changeWidthCalculationStrategy(new Fixed(85))
      .withContentPosition('left'),
    booleanColumn(GridColumnId.requirementBoundToHighLevelRequirement)
      .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_BOUND_TO_HIGH_LEVEL_REQUIREMENT)
      .withI18nKey(
        'sqtm-core.search.requirement.grid.header.requirement-bound-to-high-level-req.label',
      )
      .withTitleI18nKey(
        'sqtm-core.search.requirement.grid.header.requirement-bound-to-high-level-req.title',
      )
      .changeWidthCalculationStrategy(new Fixed(55))
      .withContentPosition('left'),
    numericColumn(GridColumnId.linkedStandardRequirement)
      .withI18nKey('sqtm-core.search.requirement.criteria.linked-standard-requirements-short-dot')
      .withTitleI18nKey(
        'sqtm-core.search.requirement.grid.header.number-of-linked-standard-requirements.title',
      )
      .disableSort()
      .changeWidthCalculationStrategy(new Fixed(55)),
    textColumn(GridColumnId.milestoneLabels)
      .withI18nKey('sqtm-core.search.test-case.grid.header.milestones.name.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.milestones.name.title')
      .disableSort()
      .changeWidthCalculationStrategy(new Limited(80)),
    concatenatedStatusesColumn(GridColumnId.milestoneStatus)
      .withI18nKey('sqtm-core.search.test-case.grid.header.milestones.status.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.milestones.status.title')
      .changeWidthCalculationStrategy(new Limited(80)),
    concatenatedDatesColumn(GridColumnId.milestoneEndDate)
      .withI18nKey('sqtm-core.search.test-case.grid.header.milestones.end-date.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.milestones.end-date.title')
      .changeWidthCalculationStrategy(new Limited(80)),
    dateTimeColumn(GridColumnId.createdOn)
      .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_CREATED_ON)
      .withI18nKey('sqtm-core.grid.header.created-on.feminine')
      .changeWidthCalculationStrategy(new Fixed(140)),
    textColumn(GridColumnId.createdBy)
      .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_CREATED_BY)
      .withI18nKey('sqtm-core.grid.header.created-by.feminine')
      .changeWidthCalculationStrategy(new Limited(80)),
    dateTimeColumn(GridColumnId.lastModifiedOn)
      .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_MODIFIED_ON)
      .withI18nKey('sqtm-core.entity.generic.last-modified-on.feminine')
      .changeWidthCalculationStrategy(new Fixed(140)),
    textColumn(GridColumnId.lastModifiedBy)
      .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_MODIFIED_BY)
      .withI18nKey('sqtm-core.grid.header.modified-by.feminine')
      .changeWidthCalculationStrategy(new Limited(95)),
    numericColumn(GridColumnId.versionNumber)
      .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_VERS_NUM)
      .withI18nKey('sqtm-core.entity.requirement.version.label.singular')
      .changeWidthCalculationStrategy(new Extendable(55, 1)),
    numericColumn(GridColumnId.versionsCount)
      .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_NB_VERSIONS)
      .withI18nKey('sqtm-core.search.requirement.grid.header.version-count.label')
      .withTitleI18nKey('sqtm-core.search.requirement.grid.header.version-count.title')
      .changeWidthCalculationStrategy(new Extendable(55, 1)),
    numericColumn(GridColumnId.attachments)
      .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_ATTCOUNT)
      .withI18nKey('sqtm-core.search.test-case.grid.header.attachments.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.attachments.title')
      .changeWidthCalculationStrategy(new Extendable(55, 1)),
    numericColumn(GridColumnId.coverages)
      .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_TCCOUNT)
      .withI18nKey('sqtm-core.search.requirement.grid.header.coverages.label')
      .withTitleI18nKey('sqtm-core.search.requirement.grid.header.coverages.title')
      .changeWidthCalculationStrategy(new Extendable(55, 1)),
    numericColumn(GridColumnId.milestones)
      .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_MILCOUNT)
      .withI18nKey('sqtm-core.search.test-case.grid.header.milestones.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.milestones.title')
      .changeWidthCalculationStrategy(new Extendable(55, 1)),
    iconLinkColumn(GridColumnId.detail, {
      kind: 'iconLink',
      baseUrl: '/requirement-workspace/requirement-version/detail',
      columnParamId: 'id',
      iconName: 'sqtm-core-generic:edit',
    }).withViewport('rightViewport'),
    navigateToRequirementColumn(GridColumnId.folder, {
      kind: 'iconLink',
      columnParamId: 'requirementId',
      iconName: 'folder',
    })
      .withViewport('rightViewport')
      .disableHeader()
      .changeWidthCalculationStrategy(new Fixed(40)),
  );

  return columnsToReturn;
}

export function convertAllRowsInRequirements(
  literals: Partial<DataRow>[],
  projectsData: ProjectDataMap,
): SquashTmDataRow[] {
  return literals
    .filter((literal) => {
      const project = projectsData[literal.projectId];
      return project?.permissions.REQUIREMENT_LIBRARY.some(
        (permission) => permission === Permissions.READ,
      );
    })
    .map((literal) => convertOneInRequirement(literal, projectsData));
}

export function convertOneInRequirement(
  literal: Partial<DataRow>,
  projectsData: ProjectDataMap,
): SquashTmDataRow {
  const dataRow: DataRow = new Requirement();
  dataRow.projectId = literal.projectId;
  const project = projectsData[dataRow.projectId];
  dataRow.simplePermissions = new RequirementPermissions(project);
  Object.assign(dataRow, literal);
  return dataRow;
}
