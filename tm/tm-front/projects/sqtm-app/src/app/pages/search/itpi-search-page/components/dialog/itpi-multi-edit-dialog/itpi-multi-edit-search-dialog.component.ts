import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import {
  CanonicalExecutionStatus,
  DialogReference,
  ExecutionStatus,
  OptionalSelectFieldComponent,
  RestService,
} from 'sqtm-core';
import { TranslateService } from '@ngx-translate/core';
import { AbstractMassEditDialog } from '../../../../test-case-search-page/abstract-mass-edit-dialog';
import { ItpiMultiEditSearchConfiguration } from './itpi-multi-edit-search.configuration';

@Component({
  selector: 'sqtm-app-requirement-multi-edit-search-dialog',
  templateUrl: './itpi-multi-edit-search-dialog.component.html',
  styleUrls: ['./itpi-multi-edit-search-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItpiMultiEditSearchDialogComponent extends AbstractMassEditDialog {
  data: ItpiMultiEditSearchConfiguration;

  @ViewChild(OptionalSelectFieldComponent)
  selectField: OptionalSelectFieldComponent;

  constructor(
    private dialogReference: DialogReference<ItpiMultiEditSearchConfiguration, boolean>,
    translateService: TranslateService,
    restService: RestService,
  ) {
    super(translateService, restService);
    this.data = dialogReference.data;
  }

  confirm() {
    if (!this.selectField.check) {
      this.dialogReference.close();
    }

    const result = {};
    result[this.selectField.fieldName] = this.selectField.selectedValue;

    this.restService
      .post(['iteration/test-plan', this.data.itpiIds.join(','), 'mass-update'], result)
      .subscribe(() => {
        this.dialogReference.result = true;
        this.dialogReference.close();
      });
  }

  getExecutionStatusValues() {
    const canonicalIds = CanonicalExecutionStatus.map((status) => status.id);
    return this.convertLevelEnumToDisplayOptions(ExecutionStatus).filter(
      (item) =>
        !this.data.disabledExecutionStatus.includes(item.id.toString()) &&
        canonicalIds.includes(item.id as any),
    );
  }
}
