import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewChild } from '@angular/core';

import {
  AbstractCellRendererComponent,
  ActionErrorDisplayService,
  ColumnDefinitionBuilder,
  EditableTextFieldComponent,
  GridColumnId,
  GridService,
  RestService,
  TableValueChange,
} from 'sqtm-core';
import { catchError, finalize } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-test-case-editable-text-renderer',
  template: ` @if (columnDisplay && row) {
    <div class="full-width full-height flex-column">
      <sqtm-core-editable-text-field
        #editableTextField
        style="margin: auto 5px;"
        class="sqtm-grid-cell-txt-renderer"
        [editable]="canEdit()"
        [displayInGrid]="true"
        [showPlaceHolder]="false"
        [value]="row.data[columnDisplay.id]"
        [layout]="'no-buttons'"
        [size]="'small'"
        (confirmEvent)="updateValue($event)"
      ></sqtm-core-editable-text-field>
    </div>
  }`,
  styleUrls: ['./test-case-editable-text-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestCaseEditableTextRendererComponent extends AbstractCellRendererComponent {
  @ViewChild('editableTextField')
  editableTextField: EditableTextFieldComponent;

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    public restService: RestService,
    public errorDisplayService: ActionErrorDisplayService,
  ) {
    super(grid, cdRef);
  }

  canEdit() {
    if (typeof this.columnDisplay.editable === 'function') {
      return this.columnDisplay.editable(this.columnDisplay, this.row);
    } else {
      return this.columnDisplay.editable;
    }
  }

  updateValue(value: string) {
    this.editableTextField.beginAsync();
    this.grid
      .updateCellValue(this.row, this.columnDisplay, value)
      .pipe(
        catchError((err) => this.errorDisplayService.handleActionError(err)),
        finalize(() => this.editableTextField.endAsync()),
      )
      .subscribe(() => {
        const tableValueChange: TableValueChange = { columnId: this.columnDisplay.id, value };
        this.grid.editRows([this.row.id], [tableValueChange]);
      });
  }
}

export function testCaseSearchEditableText(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(TestCaseEditableTextRendererComponent);
}
