import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  Fixed,
  GridColumnId,
  GridService,
} from 'sqtm-core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'sqtm-app-requirement-bound-to-item-checkbox-renderer',
  template: `
    @if (row) {
      @if (isRequirementAlreadyBound()) {
        <div class="full-width full-height flex-column">
          <i
            class="current-workspace-main-color vertical-center m-auto"
            nz-icon
            [nzType]="'sqtm-core-generic:confirm'"
            [nzTheme]="'fill'"
          ></i>
        </div>
      }
    }
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReqVersionBoundToItemCheckboxRenderer extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    private cdr: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
  ) {
    super(grid, cdr);
  }

  isRequirementAlreadyBound() {
    const itemType: string = this.activatedRoute.snapshot.paramMap.keys[0];
    const itemId = itemType && this.activatedRoute.snapshot.paramMap.get(itemType);
    let itemIds: string[];
    switch (itemType) {
      case 'testCaseId':
        itemIds = this.row.data[GridColumnId.testCaseList];
        break;
      case 'sprintId':
        itemIds = this.row.data[GridColumnId.sprintList];
        break;
      default:
        console.error("I don't know the item type: ", itemType);
        break;
    }

    return itemIds && itemId && itemIds.includes(itemId);
  }
}

export function reqVersionBoundToItemColumn(): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(GridColumnId.reqVersionBoundToItem)
    .withRenderer(ReqVersionBoundToItemCheckboxRenderer)
    .disableSort()
    .withTitleI18nKey('sqtm-core.grid.header.linked.long')
    .withI18nKey('sqtm-core.grid.header.linked.short')
    .withHeaderPosition('center')
    .changeWidthCalculationStrategy(new Fixed(30));
}
