import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  Fixed,
  GridColumnId,
  GridService,
} from 'sqtm-core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'sqtm-app-test-case-bound-to-item-renderer',
  template: `
    @if (row) {
      @if (isTestCaseAlreadyBound()) {
        <div class="full-width full-height flex-column">
          <i
            class="current-workspace-main-color vertical-center"
            style="margin:auto"
            nz-icon
            [nzType]="'sqtm-core-generic:confirm'"
            [nzTheme]="'fill'"
          ></i>
        </div>
      }
    }
  `,
  styleUrls: ['./test-case-bound-to-item-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestCaseBoundToItemRendererComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    private cdr: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
  ) {
    super(grid, cdr);
  }

  isTestCaseAlreadyBound() {
    const campaignId = this.activatedRoute.snapshot.paramMap.get('campaignId');
    const campaignIds: string[] = this.row.data[GridColumnId.campaignList];

    if (campaignIds != null && campaignId != null) {
      return campaignIds.includes(campaignId);
    }

    const iterationId = this.activatedRoute.snapshot.paramMap.get('iterationId');
    const iterationIds: string[] = this.row.data[GridColumnId.iterationList];

    if (iterationIds != null && iterationId != null) {
      return iterationIds.includes(iterationId);
    }

    const testSuiteId = this.activatedRoute.snapshot.paramMap.get('testSuiteId');
    const testSuiteIds: string[] = this.row.data['testSuiteList'];

    if (testSuiteIds != null && testSuiteId != null) {
      return testSuiteIds.includes(testSuiteId);
    }
  }
}

export function testCaseBoundToItemColumn(): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(GridColumnId.testCaseBoundToItem)
    .withRenderer(TestCaseBoundToItemRendererComponent)
    .withTitleI18nKey('sqtm-core.grid.header.linked.long')
    .withI18nKey('sqtm-core.grid.header.linked.short')
    .withHeaderPosition('center')
    .changeWidthCalculationStrategy(new Fixed(30));
}
