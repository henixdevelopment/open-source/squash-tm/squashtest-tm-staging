import { UserView } from 'sqtm-core';

export class TestCaseSearchModel {
  usersWhoCreatedTestCases: UserView[];
  usersWhoModifiedTestCases: UserView[];
}

export type TestCaseSearchState = TestCaseSearchModel;

export function initialTestCaseSearchState(): Readonly<TestCaseSearchState> {
  return { usersWhoCreatedTestCases: [], usersWhoModifiedTestCases: [] };
}
