import { ChangeDetectorRef, Directive, OnDestroy, OnInit, ViewContainerRef } from '@angular/core';
import {
  automatedTestTechnologyColumn,
  AutomationRequestStatus,
  booleanColumn,
  column,
  ColumnDefinitionBuilder,
  concatenatedDatesColumn,
  concatenatedStatusesColumn,
  CustomFieldBinding,
  dateColumn,
  dateTimeColumn,
  Extendable,
  FalseTrueI18nEnum,
  getBasicColumnIdsForTestCaseSearch,
  getExceptionColumnIdsForTestCaseSearch,
  getExtraPremiumColumnIdsForTestCaseSearch,
  GridColumnId,
  GridId,
  GridService,
  iconLinkColumn,
  indexColumn,
  infoListColumn,
  InputType,
  isTestCaseEditable,
  Limited,
  LocalPersistenceService,
  nullableNumericColumn,
  numericColumn,
  ReferentialDataService,
  ResearchColumnPrototype,
  testCaseImportanceColumn,
  TestCaseKind,
  testCaseSearchAutomatable,
  testCaseStatusColumn,
  textColumn,
} from 'sqtm-core';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { TestCaseSearchGridBuilders } from './test-case-search-grid.builders';
import { Observable, Subject } from 'rxjs';
import { testCaseBoundToItemColumn } from '../components/cell-renderers/test-case-bound-to-item-checkbox-renderer/test-case-bound-to-item-renderer.component';
import { testCaseSearchEditableText } from '../components/cell-renderers/test-case-editable-text-renderer/test-case-editable-text-renderer.component';
import { AbstractSearchComponent } from '../../abstract-search.component';

@Directive()
export abstract class AbstractTestCaseSearchComponent
  extends AbstractSearchComponent
  implements OnInit, OnDestroy
{
  protected unsub$ = new Subject<void>();

  hasSelectedRows$: Observable<boolean>;
  openSelectFilterMenu = false;
  protected overlayRef: OverlayRef;
  gridId = GridId.TEST_CASE_SEARCH;

  protected constructor(
    referentialDataService: ReferentialDataService,
    gridService: GridService,
    overlay: Overlay,
    localPersistenceService: LocalPersistenceService,
    cdRef: ChangeDetectorRef,
    vcr: ViewContainerRef,
  ) {
    super(referentialDataService, gridService, overlay, localPersistenceService, cdRef, vcr);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }
  protected searchGridBuilders(): any {
    return new TestCaseSearchGridBuilders();
  }
  protected getBasicColumnIds(): string[] {
    return getBasicColumnIdsForTestCaseSearch();
  }
  protected getExtraPremiumColumnId(): string[] {
    return getExtraPremiumColumnIdsForTestCaseSearch();
  }
  protected getExceptionColumnIds(): string[] {
    return getExceptionColumnIdsForTestCaseSearch();
  }
  protected getCufBindings(entities, entitiesKey): CustomFieldBinding[] {
    return entities[entitiesKey].customFieldBindings.TEST_CASE;
  }

  protected setExceptionColumnsVisibility(shouldDisplay: boolean) {
    this.gridService.setColumnVisibility(GridColumnId.coverages, shouldDisplay);
    this.gridService.setColumnVisibility(GridColumnId.attachments, shouldDisplay);
    this.gridService.setColumnVisibility(GridColumnId.steps, shouldDisplay);
    this.gridService.setColumnVisibility(GridColumnId.iterations, shouldDisplay);
    this.gridService.setColumnVisibility(GridColumnId.datasets, shouldDisplay);
  }

  protected setColumnType(columnId: string, inputType: string): ColumnDefinitionBuilder {
    switch (inputType) {
      case InputType.DATE_PICKER:
        return dateColumn(columnId as GridColumnId).withColumnPrototype(
          ResearchColumnPrototype.TEST_CASE_CUF_DATE,
        );
      case InputType.NUMERIC:
        return nullableNumericColumn(columnId as GridColumnId).withColumnPrototype(
          ResearchColumnPrototype.TEST_CASE_CUF_NUMERIC,
        );
      case InputType.CHECKBOX:
        return column(columnId as GridColumnId)
          .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_CUF_CHECKBOX)
          .withEnumRenderer(FalseTrueI18nEnum, false, true);
      case InputType.TAG:
        return column(columnId as GridColumnId).withColumnPrototype(
          ResearchColumnPrototype.TEST_CASE_CUF_TAG,
        );
      default:
        return textColumn(columnId as GridColumnId).withColumnPrototype(
          ResearchColumnPrototype.TEST_CASE_CUF_TEXT,
        );
    }
  }
}

export function colDefBuilderTestCaseSearch(
  isCampaignWorkspace: boolean,
): ColumnDefinitionBuilder[] {
  const columnsToReturn = [];
  columnsToReturn.push(indexColumn().withViewport('leftViewport'));

  if (isCampaignWorkspace === true) {
    columnsToReturn.push(testCaseBoundToItemColumn());
  }

  columnsToReturn.push(
    textColumn(GridColumnId.projectName)
      .withI18nKey('sqtm-core.grid.header.project')
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_PROJECT_NAME)
      .changeWidthCalculationStrategy(new Limited(100)),
    textColumn(GridColumnId.id)
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_ID)
      .withI18nKey('sqtm-core.grid.header.id')
      .changeWidthCalculationStrategy(new Limited(60)),
    testCaseSearchEditableText(GridColumnId.reference)
      .isEditable(isTestCaseEditable)
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_REFERENCE)
      .withI18nKey('sqtm-core.grid.header.reference')
      .changeWidthCalculationStrategy(new Limited(90)),
    testCaseSearchEditableText(GridColumnId.name)
      .isEditable(isTestCaseEditable)
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_NAME)
      .withI18nKey('sqtm-core.grid.header.name')
      .changeWidthCalculationStrategy(new Limited(175)),
    testCaseStatusColumn(GridColumnId.status)
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_STATUS)
      .withI18nKey('sqtm-core.search.test-case.grid.header.status.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.status.title')
      .changeWidthCalculationStrategy(new Extendable(40, 1)),
    testCaseImportanceColumn(GridColumnId.importance)
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_IMPORTANCE)
      .withI18nKey('sqtm-core.search.test-case.grid.header.weight.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.weight.title')
      .changeWidthCalculationStrategy(new Extendable(40, 1)),
    infoListColumn(GridColumnId.nature, {
      kind: 'infoList',
      infolist: 'testCaseNature',
    })
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_NATURE)
      .withI18nKey('sqtm-core.search.test-case.grid.header.nature')
      .changeWidthCalculationStrategy(new Extendable(85, 1))
      .isEditable(isTestCaseEditable),
    infoListColumn(GridColumnId.type, {
      kind: 'infoList',
      infolist: 'testCaseType',
    })
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_TYPE)
      .withI18nKey('sqtm-core.search.test-case.grid.header.type')
      .changeWidthCalculationStrategy(new Extendable(75, 1))
      .isEditable(isTestCaseEditable),
    textColumn(GridColumnId.kind)
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_KIND)
      .withEnumRenderer(TestCaseKind, false, true)
      .withI18nKey('sqtm-core.entity.test-case.kind.label')
      .changeWidthCalculationStrategy(new Extendable(75, 1)),
    booleanColumn(GridColumnId.description)
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_DESCRIPTION)
      .withI18nKey('sqtm-core.grid.header.description.label')
      .withTitleI18nKey('sqtm-core.grid.header.description.title')
      .changeWidthCalculationStrategy(new Extendable(40, 1)),
    testCaseSearchAutomatable(GridColumnId.automatable)
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_AUTOMATABLE)
      .isEditable(true)
      .withI18nKey('sqtm-core.search.test-case.grid.header.automatable.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.automatable.title')
      .changeWidthCalculationStrategy(new Limited(75)),
    textColumn(GridColumnId.automationPriority)
      .withColumnPrototype(ResearchColumnPrototype.AUTOMATION_REQUEST_AUTOMATION_PRIORITY)
      .withI18nKey('sqtm-core.search.test-case.grid.header.automation-priority.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.automation-priority.title')
      .changeWidthCalculationStrategy(new Limited(80)),
    dateColumn(GridColumnId.transmittedOn)
      .withColumnPrototype(ResearchColumnPrototype.AUTOMATION_REQUEST_TRANSMISSION_DATE)
      .withI18nKey('sqtm-core.search.test-case.grid.header.automation-transmission-date.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.automation-transmission-date.title')
      .changeWidthCalculationStrategy(new Limited(80))
      .withAssociatedFilter(),
    textColumn(GridColumnId.requestStatus)
      .withColumnPrototype(ResearchColumnPrototype.AUTOMATION_REQUEST_STATUS)
      .withEnumRenderer(AutomationRequestStatus, false, true)
      .isEditable(false)
      .withI18nKey('sqtm-core.entity.automation-request.status.label.short')
      .withTitleI18nKey('sqtm-core.entity.automation-request.status.label.full')
      .changeWidthCalculationStrategy(new Limited(130)),
    booleanColumn(GridColumnId.hasAutoScript)
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_HASAUTOSCRIPT)
      .withI18nKey('sqtm-core.search.test-case.grid.header.automated-script-associated.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.automated-script-associated.title')
      .changeWidthCalculationStrategy(new Limited(90)),
    automatedTestTechnologyColumn()
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_AUTOMATED_TEST_TECHNOLOGY)
      .withI18nKey('sqtm-core.automation-workspace.grid.headers.technology.label.short')
      .withTitleI18nKey('sqtm-core.automation-workspace.grid.headers.technology.label.full')
      .changeWidthCalculationStrategy(new Limited(60)),
    booleanColumn(GridColumnId.hasBoundScmRepository)
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_HAS_BOUND_SCM_REPOSITORY)
      .withI18nKey('sqtm-core.search.test-case.grid.header.bound-source-code-repository.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.bound-source-code-repository.title')
      .changeWidthCalculationStrategy(new Limited(80)),
    booleanColumn(GridColumnId.hasBoundAutomatedTestReference)
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_HAS_BOUND_AUTOMATED_TEST_REFERENCE)
      .withI18nKey('sqtm-core.search.test-case.grid.header.bound-automated-test-ref.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.bound-automated-test-ref.title')
      .changeWidthCalculationStrategy(new Limited(120)),
    textColumn(GridColumnId.milestoneLabels)
      .withI18nKey('sqtm-core.search.test-case.grid.header.milestones.name.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.milestones.name.title')
      .disableSort()
      .changeWidthCalculationStrategy(new Limited(80)),
    concatenatedStatusesColumn(GridColumnId.milestoneStatus)
      .withI18nKey('sqtm-core.search.test-case.grid.header.milestones.status.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.milestones.status.title')
      .changeWidthCalculationStrategy(new Limited(80)),
    concatenatedDatesColumn(GridColumnId.milestoneEndDate)
      .withI18nKey('sqtm-core.search.test-case.grid.header.milestones.end-date.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.milestones.end-date.title')
      .changeWidthCalculationStrategy(new Limited(80)),
    dateTimeColumn(GridColumnId.createdOn)
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_CREATED_ON)
      .withI18nKey('sqtm-core.grid.header.created-on.masculine')
      .changeWidthCalculationStrategy(new Limited(80)),
    textColumn(GridColumnId.createdBy)
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_CREATED_BY)
      .withI18nKey('sqtm-core.grid.header.created-by.masculine')
      .changeWidthCalculationStrategy(new Limited(80)),
    dateTimeColumn(GridColumnId.lastModifiedOn)
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_MODIFIED_ON)
      .withI18nKey('sqtm-core.grid.header.modified-on.masculine')
      .changeWidthCalculationStrategy(new Limited(80)),
    textColumn(GridColumnId.lastModifiedBy)
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_MODIFIED_BY)
      .withI18nKey('sqtm-core.grid.header.modified-by.masculine')
      .changeWidthCalculationStrategy(new Limited(85)),
    numericColumn(GridColumnId.milestones)
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_MILCOUNT)
      .withI18nKey('sqtm-core.search.test-case.grid.header.milestones.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.milestones.title')
      .changeWidthCalculationStrategy(new Extendable(55, 0.5)),
    numericColumn(GridColumnId.attachments)
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_ATTCOUNT)
      .withI18nKey('sqtm-core.search.test-case.grid.header.attachments.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.attachments.title')
      .changeWidthCalculationStrategy(new Extendable(55, 0.5)),
    numericColumn(GridColumnId.coverages)
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_VERSCOUNT)
      .withI18nKey('sqtm-core.search.test-case.grid.header.coverages.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.coverages.title')
      .changeWidthCalculationStrategy(new Extendable(55, 0.5)),
    numericColumn(GridColumnId.steps)
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_STEPCOUNT)
      .withI18nKey('sqtm-core.search.test-case.grid.header.steps.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.steps.title')
      .changeWidthCalculationStrategy(new Extendable(55, 0.5)),
    numericColumn(GridColumnId.paramCount)
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_PARAMCOUNT)
      .withI18nKey('sqtm-core.search.test-case.grid.header.parameters.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.parameters.title')
      .changeWidthCalculationStrategy(new Extendable(55, 0.5)),
    numericColumn(GridColumnId.datasets)
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_DATASETCOUNT)
      .withI18nKey('sqtm-core.search.test-case.grid.header.datasets.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.datasets.title')
      .changeWidthCalculationStrategy(new Extendable(55, 0.5)),
    numericColumn(GridColumnId.callStepCount)
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_CALLSTEPCOUNT)
      .withI18nKey('sqtm-core.search.test-case.grid.header.call-step-count.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.call-step-count.title')
      .disableSort()
      .changeWidthCalculationStrategy(new Extendable(55, 0.5)),
    numericColumn(GridColumnId.iterations)
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_ITERCOUNT)
      .withI18nKey('sqtm-core.search.test-case.grid.header.items.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.items.title')
      .changeWidthCalculationStrategy(new Extendable(55, 0.5)),
    numericColumn(GridColumnId.executionCount)
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_EXECOUNT)
      .withI18nKey('sqtm-core.search.test-case.grid.header.executions.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.executions.title')
      .changeWidthCalculationStrategy(new Extendable(55, 0.5)),
    numericColumn(GridColumnId.issueCount)
      .withColumnPrototype(ResearchColumnPrototype.EXECUTION_ISSUECOUNT)
      .withI18nKey('sqtm-core.search.test-case.grid.header.issues.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.issues.title')
      .changeWidthCalculationStrategy(new Extendable(55, 0.5)),
    booleanColumn(GridColumnId.draftedByAi)
      .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_DRAFTED_BY_AI)
      .withI18nKey('sqtm-core.search.test-case.grid.header.drafted-by-ai.label')
      .withTitleI18nKey('sqtm-core.search.test-case.grid.header.drafted-by-ai.title')
      .disableSort()
      .changeWidthCalculationStrategy(new Limited(90)),
    iconLinkColumn(GridColumnId.detail, {
      kind: 'iconLink',
      columnParamId: 'id',
      iconName: 'sqtm-core-generic:edit',
      baseUrl: '/test-case-workspace/test-case/detail',
    }).withViewport('rightViewport'),
    iconLinkColumn(GridColumnId.folder, {
      kind: 'iconLink',
      columnParamId: 'id',
      iconName: 'folder',
      baseUrl: '/test-case-workspace/test-case',
    }).withViewport('rightViewport'),
  );

  return columnsToReturn;
}
