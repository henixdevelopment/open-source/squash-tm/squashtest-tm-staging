export class SearchTestCaseExportDialogConfiguration {
  id: string;
  nodes: number[];
  simplifiedColumnDisplayGridIds: string[];
}
