import { Logger } from 'sqtm-core';
import { sqtmAppLogger } from '../../../app-logger';

export const searchViewLogger: Logger = sqtmAppLogger.compose('search-view');
