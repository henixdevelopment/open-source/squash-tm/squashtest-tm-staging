import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  Fixed,
  GridColumnId,
  GridService,
  IconLinkOptions,
  isIconLinkOptions,
  RequirementNature,
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-navigate-to-requirement-cell',
  template: `
    @if (row) {
      <div class="full-width full-height flex-column">
        <a
          class="current-workspace-main-color vertical-center"
          style="margin:auto"
          [routerLink]="getUrl()"
          target="_blank"
        >
          <i nz-icon [nzType]="options.iconName" [nzTheme]="'fill'"> </i>
        </a>
      </div>
    }
  `,
  styleUrls: ['./navigate-to-requirement-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavigateToRequirementCellComponent extends AbstractCellRendererComponent {
  get options(): IconLinkOptions {
    const options = this.columnDisplay.options;

    if (!options || !isIconLinkOptions(options)) {
      throw Error('You must provide IconLinkOptions');
    } else {
      return options;
    }
  }

  constructor(
    public grid: GridService,
    private cdr: ChangeDetectorRef,
  ) {
    super(grid, cdr);
  }

  getUrl() {
    return this.getBaseUrl() + '/' + this.row.data[this.options.columnParamId];
  }

  private getBaseUrl() {
    const isHighLevel = this.row.data[GridColumnId.nature] === RequirementNature.HIGH_LEVEL.id;
    return `/requirement-workspace/${isHighLevel ? 'high-level-requirement' : 'requirement'}`;
  }
}

export function navigateToRequirementColumn(
  id: GridColumnId,
  options: IconLinkOptions,
): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(NavigateToRequirementCellComponent)
    .withOptions(options)
    .disableHeader()
    .changeWidthCalculationStrategy(new Fixed(35));
}
