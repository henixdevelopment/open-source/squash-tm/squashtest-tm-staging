import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewContainerRef,
} from '@angular/core';
import {
  CustomFieldBinding,
  DataRow,
  DialogService,
  getBasicColumnIdsForITPISearch,
  getExtraPremiumColumnIdsForITPISearch,
  getExceptionColumnIdsForITPISearch,
  GridService,
  gridServiceFactory,
  LocalPersistenceService,
  ProjectData,
  ProjectState,
  ReferentialDataService,
  RestService,
  UserHistorySearchProvider,
} from 'sqtm-core';
import {
  ITPI_RESEARCH_GRID,
  ITPI_RESEARCH_GRID_CONFIG,
  ItpiSearchGridBuilders,
  searchItpiGridConfigFactory,
} from '../itpi-search-grid.builders';
import { ItpiSearchViewService } from '../../services/itpi-search-view.service';
import { map, switchMap, take } from 'rxjs/operators';
import { ItpiMultiEditSearchDialogComponent } from '../../components/dialog/itpi-multi-edit-dialog/itpi-multi-edit-search-dialog.component';
import { ItpiAddTestPlanDialogComponent } from '../../components/dialog/itpi-add-test-plan-dialog/itpi-add-test-plan-dialog.component';
import { Overlay } from '@angular/cdk/overlay';
import { Observable } from 'rxjs';
import { ItpiSearchModel } from '../../services/itpi-search-model';
import { AbstractSearchComponent } from '../../../abstract-search.component';

@Component({
  selector: 'sqtm-app-itpi-search-page',
  templateUrl: './itpi-search-page.component.html',
  styleUrls: ['./itpi-search-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: ITPI_RESEARCH_GRID_CONFIG,
      useFactory: searchItpiGridConfigFactory,
      deps: [LocalPersistenceService],
    },
    {
      provide: ITPI_RESEARCH_GRID,
      useFactory: gridServiceFactory,
      deps: [RestService, ITPI_RESEARCH_GRID_CONFIG, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: ITPI_RESEARCH_GRID,
    },
    {
      provide: ItpiSearchViewService,
      useClass: ItpiSearchViewService,
      deps: [RestService],
    },
    {
      provide: UserHistorySearchProvider,
      useExisting: ItpiSearchViewService,
    },
  ],
})
export class ItpiSearchPageComponent extends AbstractSearchComponent implements OnInit {
  constructor(
    referentialDataService: ReferentialDataService,
    gridService: GridService,
    overlay: Overlay,
    localPersistenceService: LocalPersistenceService,
    cdRef: ChangeDetectorRef,
    vcr: ViewContainerRef,
    protected itpiSearchViewService: ItpiSearchViewService,
    private dialogService: DialogService,
  ) {
    super(referentialDataService, gridService, overlay, localPersistenceService, cdRef, vcr);
  }

  ngOnInit() {
    super.ngOnInit();
  }
  protected searchGridBuilders(): any {
    return new ItpiSearchGridBuilders();
  }
  protected displayIfNotPremium() {
    getExtraPremiumColumnIdsForITPISearch().forEach((col) => {
      this.gridService.setColumnVisibility(col, false);
    });
  }
  protected addCufPremiumColumns(_referentialData, _columns, _activeColumnIds): void {
    //NOOP
  }

  protected getAllCustomFieldBindings(_projects: ProjectState): number[] {
    return null;
  }
  protected getBasicColumnIds(): string[] {
    return getBasicColumnIdsForITPISearch();
  }
  protected getExtraPremiumColumnId(): string[] {
    return getExtraPremiumColumnIdsForITPISearch();
  }

  protected getExceptionColumnIds(): string[] {
    return getExceptionColumnIdsForITPISearch();
  }
  protected loadResearchData(): Observable<ItpiSearchModel> {
    return this.itpiSearchViewService.loadResearchData();
  }
  protected getCufBindings(_entities, _entitiesKey): CustomFieldBinding[] {
    return null;
  }

  protected setExceptionColumnsVisibility(): void {
    //NOOP
  }

  showMassEditDialog() {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        switchMap((dataRows) => {
          const editableDataRows = dataRows.filter((row) => row.simplePermissions.canWrite);
          const projectIds = new Set(editableDataRows.map((row) => row.projectId));
          return this.referentialDataService.projectDatas$.pipe(
            take(1),
            map((projectDatas) => {
              const filteredProjectDatas: ProjectData[] = [];
              for (const projectId of projectIds) {
                filteredProjectDatas.push(projectDatas[projectId]);
              }
              const sameExecutionStatusConfiguration =
                this.projectDisabledSameExecutionStatus(filteredProjectDatas);
              let disabledExecutionStatus = [];
              if (filteredProjectDatas.length !== 0) {
                disabledExecutionStatus = filteredProjectDatas[0].disabledExecutionStatus;
              }
              const itpiIds = editableDataRows.map((row) => row.id);
              const canEditAllRows = dataRows.length === editableDataRows.length;
              const canEditRows = editableDataRows.length > 0;
              return {
                disabledExecutionStatus,
                sameExecutionStatusConfiguration,
                itpiIds,
                canEditAllRows,
                canEditRows,
              };
            }),
          );
        }),
      )
      .subscribe((datas) => {
        if (!datas.canEditRows) {
          this.showCantEditDialog('sqtm-core.search.generic.modify.no-line-with-writing-rights');
        } else {
          if (datas.sameExecutionStatusConfiguration) {
            const dialogReference = this.dialogService.openDialog({
              viewContainerReference: this.vcr,
              component: ItpiMultiEditSearchDialogComponent,
              data: {
                id: 'mass-edit',
                titleKey: 'sqtm-core.search.generic.modify.selection',
                disabledExecutionStatus: datas.disabledExecutionStatus,
                canEditAllRows: datas.canEditAllRows,
                itpiIds: datas.itpiIds,
              },
              id: 'itpi-multi-edit',
              width: 600,
            });
            dialogReference.dialogClosed$.subscribe((confirm) => {
              if (confirm) {
                this.gridService.refreshData();
              }
            });
          } else {
            this.dialogService.openAlert({
              level: 'DANGER',
              messageKey: 'sqtm-core.search.campaign.dialog.not-same-configuration.message',
            });
          }
        }
      });
  }

  showCantEditDialog(messageKey: string) {
    this.dialogService.openAlert({
      titleKey: 'sqtm-core.generic.label.information.singular',
      messageKey,
      level: 'INFO',
    });
  }

  cantEditAllRows(dataRows: DataRow[]): boolean {
    return dataRows.filter((row) => row.simplePermissions.canWrite).length !== dataRows.length;
  }

  showAddToTestPlanDialog() {
    this.gridService.selectedRows$.pipe(take(1)).subscribe((dataRows) => {
      const itpiIds = dataRows.map((row) => Number(row.id));
      const dialogReference = this.dialogService.openDialog({
        id: 'test-plan-dialog',
        component: ItpiAddTestPlanDialogComponent,
        data: { itpiIds },
        viewContainerReference: this.vcr,
        width: 600,
        height: 600,
      });
      dialogReference.dialogClosed$.subscribe((confirm) => {
        if (confirm) {
          this.gridService.refreshData();
        }
      });
    });
  }

  private projectDisabledSameExecutionStatus(projectData: ProjectData[]): boolean {
    if (projectData.length === 0 || projectData.length === 1) {
      return true;
    }

    const firstProjectDisabledStatus = projectData[0].disabledExecutionStatus;

    for (const project of projectData) {
      if (project.disabledExecutionStatus.length !== firstProjectDisabledStatus.length) {
        return false;
      }

      for (const status of project.disabledExecutionStatus) {
        if (!firstProjectDisabledStatus.includes(status)) {
          return false;
        }
      }
    }

    return true;
  }
}
