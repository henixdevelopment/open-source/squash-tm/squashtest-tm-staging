import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'sqtm-app-test-case-by-requirement-search-page',
  template: `
    <sqtm-app-test-case-by-req-for-campaign-workspace-entities-search-page
      [workspaceName]="'requirement-workspace'"
      [titleKey]="'sqtm-core.search.requirement-coverage.title'"
      [containerType]="'iterationId'"
      [url]="'iteration'"
    >
    </sqtm-app-test-case-by-req-for-campaign-workspace-entities-search-page>
  `,
  styleUrls: ['./test-case-by-requirement-search-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestCaseByRequirementSearchPageComponent {}
