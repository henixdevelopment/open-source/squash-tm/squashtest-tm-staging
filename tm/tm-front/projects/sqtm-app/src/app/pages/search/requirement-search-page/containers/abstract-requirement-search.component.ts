import {
  ChangeDetectorRef,
  Directive,
  InjectionToken,
  OnDestroy,
  OnInit,
  ViewContainerRef,
} from '@angular/core';
import { RequirementSearchGridBuilders } from './requirement-search-grid.builders';
import {
  ColumnDefinitionBuilder,
  CustomFieldBinding,
  FalseTrueI18nEnum,
  GridColumnId,
  GridId,
  GridService,
  InputType,
  LocalPersistenceService,
  ReferentialDataService,
  ResearchColumnPrototype,
  column,
  dateColumn,
  getBasicColumnIdsForRequirementSearch,
  getExceptionColumnIdsForRequirementSearch,
  getExtraPremiumColumnIdsForRequirementSearch,
  nullableNumericColumn,
  textColumn,
} from 'sqtm-core';
import { RequirementSearchViewService } from '../services/requirement-search-view.service';
import { Observable } from 'rxjs';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { AbstractSearchComponent } from '../../abstract-search.component';
import { RequirementSearchModel } from '../services/requirement-search-model';

export const REQUIREMENT_RESEARCH_GRID_CONFIG = new InjectionToken(
  'Token for requirement grid research config',
);
export const REQUIREMENT_RESEARCH_GRID = new InjectionToken('Token for requirement grid research');

@Directive()
export abstract class AbstractRequirementSearchComponent
  extends AbstractSearchComponent
  implements OnInit, OnDestroy
{
  hasSelectedRows$: Observable<boolean>;
  protected overlayRef: OverlayRef;

  openSelectFilterMenu = false;
  gridId = GridId.REQUIREMENT_SEARCH;

  protected constructor(
    referentialDataService: ReferentialDataService,
    gridService: GridService,
    overlay: Overlay,
    localPersistenceService: LocalPersistenceService,
    cdRef: ChangeDetectorRef,
    vcr: ViewContainerRef,
    protected requirementSearchViewService: RequirementSearchViewService,
  ) {
    super(referentialDataService, gridService, overlay, localPersistenceService, cdRef, vcr);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  protected loadResearchData(): Observable<RequirementSearchModel> {
    return this.requirementSearchViewService.loadResearchData();
  }
  protected searchGridBuilders(): any {
    return new RequirementSearchGridBuilders();
  }
  protected getBasicColumnIds(): string[] {
    return getBasicColumnIdsForRequirementSearch();
  }
  protected getExtraPremiumColumnId(): string[] {
    return getExtraPremiumColumnIdsForRequirementSearch();
  }
  protected getExceptionColumnIds(): string[] {
    return getExceptionColumnIdsForRequirementSearch();
  }
  protected getCufBindings(entities, entitiesKey): CustomFieldBinding[] {
    return entities[entitiesKey].customFieldBindings.REQUIREMENT_VERSION;
  }

  protected setColumnType(columnId: string, inputType: string): ColumnDefinitionBuilder {
    switch (inputType) {
      case InputType.DATE_PICKER:
        return dateColumn(columnId as GridColumnId).withColumnPrototype(
          ResearchColumnPrototype.REQUIREMENT_VERSION_CUF_DATE,
        );
      case InputType.NUMERIC:
        return nullableNumericColumn(columnId as GridColumnId).withColumnPrototype(
          ResearchColumnPrototype.REQUIREMENT_VERSION_CUF_NUMERIC,
        );
      case InputType.CHECKBOX:
        return column(columnId as GridColumnId)
          .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_CUF_CHECKBOX)
          .withEnumRenderer(FalseTrueI18nEnum, false, true);
      case InputType.TAG:
        return column(columnId as GridColumnId).withColumnPrototype(
          ResearchColumnPrototype.REQUIREMENT_VERSION_CUF_TAG,
        );
      default:
        return textColumn(columnId as GridColumnId).withColumnPrototype(
          ResearchColumnPrototype.REQUIREMENT_VERSION_CUF_TEXT,
        );
    }
  }

  protected setExceptionColumnsVisibility(shouldDisplay: boolean): void {
    this.gridService.setColumnVisibility(GridColumnId.coverages, shouldDisplay);
    this.gridService.setColumnVisibility(GridColumnId.attachments, shouldDisplay);
  }
}
