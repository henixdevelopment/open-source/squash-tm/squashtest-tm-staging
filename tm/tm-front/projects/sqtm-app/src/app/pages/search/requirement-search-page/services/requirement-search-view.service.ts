import { Injectable } from '@angular/core';
import {
  createStore,
  ResearchColumnPrototype,
  RestService,
  Store,
  UserHistorySearchProvider,
  UserListElement,
} from 'sqtm-core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import {
  initialRequirementSearchState,
  RequirementSearchModel,
  RequirementSearchState,
} from './requirement-search-model';

type userReqColumnPrototype =
  | ResearchColumnPrototype.REQUIREMENT_VERSION_CREATED_BY
  | ResearchColumnPrototype.REQUIREMENT_VERSION_MODIFIED_BY;

@Injectable()
export class RequirementSearchViewService extends UserHistorySearchProvider {
  private store: Store<RequirementSearchState> = createStore(initialRequirementSearchState());
  public state$: Observable<RequirementSearchState>;

  private columnProtoToUserList: { [K in userReqColumnPrototype]: keyof RequirementSearchState } = {
    [ResearchColumnPrototype.REQUIREMENT_VERSION_CREATED_BY]: 'usersWhoCreatedRequirements',
    [ResearchColumnPrototype.REQUIREMENT_VERSION_MODIFIED_BY]: 'usersWhoModifiedRequirements',
  };

  constructor(private restService: RestService) {
    super();
    this.state$ = this.store.state$;
  }

  loadResearchData(): Observable<RequirementSearchModel> {
    return this.restService
      .get<RequirementSearchModel>(['search', 'requirement'])
      .pipe(tap((model) => this.store.commit(model)));
  }

  provideUserList(columnPrototype: ResearchColumnPrototype): Observable<UserListElement[]> {
    return this.state$.pipe(map((state) => state[this.columnProtoToUserList[columnPrototype]]));
  }
}
