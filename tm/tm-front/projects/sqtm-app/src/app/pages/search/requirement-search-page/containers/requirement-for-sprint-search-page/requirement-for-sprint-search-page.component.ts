import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewContainerRef,
} from '@angular/core';
import { searchRequirementGridConfigFactory } from '../requirement-search-grid.builders';
import { Observable } from 'rxjs';
import { catchError, take } from 'rxjs/operators';
import {
  ActionErrorDisplayService,
  BindReqVersionToSprintOperationReport,
  createBindReqVersionToSprintOperationDialogConfiguration,
  DataRow,
  DialogService,
  GridService,
  gridServiceFactory,
  GridState,
  LocalPersistenceService,
  ReferentialDataService,
  RestService,
  shouldShowBindReqVersionToSprintDialog,
  SprintService,
  UserHistorySearchProvider,
} from 'sqtm-core';
import { RequirementSearchViewService } from '../../services/requirement-search-view.service';
import {
  AbstractRequirementSearchComponent,
  REQUIREMENT_RESEARCH_GRID,
  REQUIREMENT_RESEARCH_GRID_CONFIG,
} from '../abstract-requirement-search.component';
import { ActivatedRoute, Router } from '@angular/router';
import { BACK_URL_PARAM } from '../../../search-constants';
import { Overlay } from '@angular/cdk/overlay';

@Component({
  selector: 'sqtm-app-requirement-for-sprint-search-page',
  templateUrl: './requirement-for-sprint-search-page.component.html',
  styleUrls: ['./requirement-for-sprint-search-page.component.less'],
  providers: [
    {
      provide: REQUIREMENT_RESEARCH_GRID_CONFIG,
      useFactory: searchRequirementGridConfigFactory,
      deps: [LocalPersistenceService],
    },
    {
      provide: REQUIREMENT_RESEARCH_GRID,
      useFactory: gridServiceFactory,
      deps: [RestService, REQUIREMENT_RESEARCH_GRID_CONFIG, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: REQUIREMENT_RESEARCH_GRID,
    },
    {
      provide: RequirementSearchViewService,
      useClass: RequirementSearchViewService,
      deps: [RestService],
    },
    {
      provide: UserHistorySearchProvider,
      useExisting: RequirementSearchViewService,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RequirementForSprintSearchPageComponent extends AbstractRequirementSearchComponent {
  private sprintId = this.activatedRoute.snapshot.paramMap.get('sprintId');

  constructor(
    referentialDataService: ReferentialDataService,
    requirementSearchViewService: RequirementSearchViewService,
    gridService: GridService,
    localPersistenceService: LocalPersistenceService,
    overlay: Overlay,
    cdRef: ChangeDetectorRef,
    vcr: ViewContainerRef,
    private readonly sprintService: SprintService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly dialogService: DialogService,
    private readonly actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    super(
      referentialDataService,
      gridService,
      overlay,
      localPersistenceService,
      cdRef,
      vcr,
      requirementSearchViewService,
    );
  }

  linkSelection() {
    this.gridService.selectedRows$.pipe(take(1)).subscribe((dataRows) => {
      this.linkReqVersionsToSprintFromDataRows(dataRows);
    });
  }

  linkAll() {
    this.gridService.dataRows$.pipe(take(1)).subscribe((dataRowsDictionnary) => {
      this.linkReqVersionsToSprintFromDataRows(Object.values(dataRowsDictionnary));
    });
  }

  private linkReqVersionsToSprintFromDataRows(rows: DataRow[]): void {
    const requirementIds: number[] = this.extractRequirementIds(rows);
    this.persistNewSprintReqVersion(requirementIds).subscribe((operationReport) => {
      if (shouldShowBindReqVersionToSprintDialog(operationReport)) {
        this.dialogService.openDialog(
          createBindReqVersionToSprintOperationDialogConfiguration(operationReport, this.vcr),
        );
      }
      this.navigateBack();
    });
  }

  navigateBack() {
    const backUrl =
      this.activatedRoute.snapshot.queryParamMap.get(BACK_URL_PARAM) || 'campaign-workspace';
    this.router.navigate([backUrl]);
  }

  private persistNewSprintReqVersion(
    requirementIds: number[],
  ): Observable<BindReqVersionToSprintOperationReport> {
    return this.sprintService
      .addReqVersions(Number.parseInt(this.sprintId, 10), requirementIds)
      .pipe(
        take(1),
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
      );
  }

  private extractRequirementIds(rows: DataRow[]): number[] {
    return rows.map((row) => Number.parseInt(row.data.id, 10));
  }

  getDisplayColumnTooltip(gridState: GridState, gridId: string): string {
    return gridState.configurationState.simplifiedColumnDisplayGridIds.includes(gridId)
      ? 'sqtm-core.search.show-other-columns'
      : 'sqtm-core.search.hide-other-columns';
  }
}
