import {
  AutomationRequestStatus,
  buildFilters,
  CampaignExecutionScopeFilter,
  CampaignPermissions,
  concatenatedDatesColumn,
  concatenatedStatusesColumn,
  DataRow,
  dateFilter,
  dateTimeColumn,
  executionModeColumn,
  executionStatusColumn,
  executionStatusFilter,
  Extendable,
  FilterGroup,
  FilterTestCaseMilestoneLabelComponent,
  Fixed,
  GridColumnId,
  GridFilter,
  GridId,
  i18nEnumResearchFilter,
  i18nEnumSingleSelectionResearchFilter,
  iconLinkColumn,
  indexColumn,
  IterationTestPlanItemRow,
  Limited,
  LocalPersistenceService,
  multipleListResearchFilter,
  numericColumn,
  numericResearchFilter,
  Permissions,
  ProjectDataMap,
  ResearchColumnPrototype,
  ScopeDefinitionBuilder,
  searchGrid,
  Sort,
  SquashTmDataRow,
  StyleDefinitionBuilder,
  testCaseImportanceColumn,
  testCaseSearchAutomatable,
  textColumn,
  textResearchFilter,
  userHistoryResearchFilter,
} from 'sqtm-core';
import { InjectionToken } from '@angular/core';
import { MILESTONE_FILTER } from '../../search-constants';
import { startExecutionSearchColumn } from '../components/cell-renderers/start-execution-search/start-execution-search.component';

export class ItpiSearchGridBuilders {
  buildFilters(): GridFilter[] {
    return buildFilters([
      numericResearchFilter(GridColumnId.id, ResearchColumnPrototype.TEST_CASE_ID)
        .withGroupId('informations')
        .withI18nKey('sqtm-core.search.generic.criteria.test-case-id'),
      textResearchFilter(GridColumnId.reference, ResearchColumnPrototype.TEST_CASE_REFERENCE)
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.generic.reference.label'),
      textResearchFilter(GridColumnId.name, ResearchColumnPrototype.TEST_CASE_NAME)
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.generic.name.label'),
      i18nEnumResearchFilter(
        GridColumnId.automatable,
        ResearchColumnPrototype.TEST_CASE_AUTOMATABLE,
      )
        .withGroupId('automation')
        .withI18nKey('sqtm-core.generic.label.automation.indicator'),
      i18nEnumResearchFilter(
        GridColumnId.automationRequestStatus,
        ResearchColumnPrototype.AUTOMATION_REQUEST_STATUS,
      )
        .withGroupId('automation')
        .withI18nKey('sqtm-core.generic.label.automation.status'),
      multipleListResearchFilter(
        MILESTONE_FILTER,
        ResearchColumnPrototype.CAMPAIGN_MILESTONE_ID,
        FilterTestCaseMilestoneLabelComponent,
      )
        .withGroupId('milestones')
        .withI18nKey('sqtm-core.search.generic.criteria.milestone.name'),
      i18nEnumResearchFilter(
        GridColumnId.milestoneStatus,
        ResearchColumnPrototype.CAMPAIGN_MILESTONE_STATUS,
      )
        .withGroupId('milestones')
        .withI18nKey('sqtm-core.search.generic.criteria.milestone.status'),
      dateFilter(GridColumnId.milestoneEndDate, ResearchColumnPrototype.CAMPAIGN_MILESTONE_END_DATE)
        .withGroupId('milestones')
        .withI18nKey('sqtm-core.entity.milestone.end-date.label'),
      i18nEnumResearchFilter(GridColumnId.importance, ResearchColumnPrototype.TEST_CASE_IMPORTANCE)
        .withGroupId('attributes')
        .withI18nKey('sqtm-core.entity.test-case.importance.label'),
      userHistoryResearchFilter(
        GridColumnId.assignee,
        ResearchColumnPrototype.ITERATION_TEST_PLAN_ASSIGNED_USER_LOGIN,
      )
        .withGroupId('attributes')
        .withI18nKey('sqtm-core.search.generic.criteria.assignee-user'),
      dateFilter(GridColumnId.executionOn, ResearchColumnPrototype.ITEM_TEST_PLAN_LASTEXECON)
        .withGroupId('execution')
        .withI18nKey('sqtm-core.search.generic.criteria.executed-on'),
      userHistoryResearchFilter(
        GridColumnId.executedBy,
        ResearchColumnPrototype.ITEM_TEST_PLAN_LASTEXECBY,
      )
        .withGroupId('execution')
        .withI18nKey('sqtm-core.search.generic.criteria.executed-by'),
      executionStatusFilter(
        GridColumnId.executionStatus,
        ResearchColumnPrototype.ITEM_TEST_PLAN_STATUS,
      )
        .withGroupId('execution')
        .withI18nKey('sqtm-core.search.generic.criteria.execution-status'),
      i18nEnumResearchFilter(
        GridColumnId.inferredExecutionMode,
        ResearchColumnPrototype.EXECUTION_EXECUTION_MODE,
      )
        .withGroupId('execution')
        .withI18nKey('sqtm-core.search.generic.criteria.execution-mode'),
      i18nEnumSingleSelectionResearchFilter(
        GridColumnId.itemTestPlanExecutionScope,
        ResearchColumnPrototype.ITEM_TEST_PLAN_EXECUTION_SCOPE,
        CampaignExecutionScopeFilter.ALL,
      )
        .withI18nKey('sqtm-core.search.campaign.criteria.last-execution-scope.title')
        .alwaysActive(),
    ]);
  }

  public buildFilterGroups(): FilterGroup[] {
    return [
      {
        id: 'informations',
        i18nLabelKey: 'sqtm-core.generic.label.information.plural',
      },
      {
        id: 'attributes',
        i18nLabelKey: 'sqtm-core.entity.generic.attributes',
      },
      {
        id: 'automation',
        i18nLabelKey: 'sqtm-core.generic.label.automation.label',
      },
      {
        id: 'milestones',
        i18nLabelKey: 'sqtm-core.generic.label.milestone',
      },
      {
        id: 'execution',
        i18nLabelKey: 'sqtm-core.search.generic.criteria.execution',
      },
    ];
  }
}

export const ITPI_RESEARCH_GRID_CONFIG = new InjectionToken('Token for itpi grid research config');
export const ITPI_RESEARCH_GRID = new InjectionToken('Token for itpi grid research');

export const searchItpiGridConfigFactory = (localPersistenceService: LocalPersistenceService) => {
  return searchGrid(GridId.ITPI_SEARCH)
    .server()
    .withServerUrl(['search/campaign'])
    .withRowConverter(convertAllRowsInCampaign)
    .withColumns([
      indexColumn().withViewport('leftViewport'),
      textColumn(GridColumnId.projectName)
        .withI18nKey('sqtm-core.grid.header.project')
        .withColumnPrototype(ResearchColumnPrototype.CAMPAIGN_PROJECT_NAME)
        .changeWidthCalculationStrategy(new Limited(175)),
      textColumn(GridColumnId.campaignName)
        .withColumnPrototype(ResearchColumnPrototype.CAMPAIGN_NAME)
        .withI18nKey('sqtm-core.grid.header.campaign')
        .changeWidthCalculationStrategy(new Limited(175)),
      textColumn(GridColumnId.iterationName)
        .withColumnPrototype(ResearchColumnPrototype.ITERATION_NAME)
        .withI18nKey('sqtm-core.grid.header.iteration')
        .changeWidthCalculationStrategy(new Limited(175)),
      numericColumn(GridColumnId.tclnId)
        .withTitleI18nKey('sqtm-core.custom-report-workspace.chart.columns.TEST_CASE_ID')
        .withI18nKey('sqtm-core.search.generic.criteria.test-case-id-short')
        .disableSort()
        .changeWidthCalculationStrategy(new Fixed(75)),
      textColumn(GridColumnId.reference)
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_REFERENCE)
        .withI18nKey('sqtm-core.grid.header.reference')
        .changeWidthCalculationStrategy(new Limited(95)),
      textColumn(GridColumnId.label)
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_NAME)
        .withI18nKey('sqtm-core.grid.header.name')
        .changeWidthCalculationStrategy(new Limited(175)),
      testCaseImportanceColumn(GridColumnId.importance)
        .isEditable(false)
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_IMPORTANCE)
        .withI18nKey('sqtm-core.search.test-case.grid.header.weight.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.weight.title')
        .changeWidthCalculationStrategy(new Fixed(40)),
      textColumn(GridColumnId.datasetName)
        .withColumnPrototype(ResearchColumnPrototype.DATASET_NAME)
        .withI18nKey('sqtm-core.entity.dataset.label.short')
        .withTitleI18nKey('sqtm-core.grid.header.dataset')
        .changeWidthCalculationStrategy(new Limited(70)),
      textColumn(GridColumnId.testSuites)
        .withColumnPrototype(ResearchColumnPrototype.ITEM_TEST_PLAN_SUITECOUNT)
        .withI18nKey('sqtm-core.grid.header.test-suite')
        .disableSort()
        .changeWidthCalculationStrategy(new Limited(110)),
      testCaseSearchAutomatable(GridColumnId.automatable)
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_AUTOMATABLE)
        .withI18nKey('sqtm-core.search.test-case.grid.header.automatable.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.automatable.title')
        .changeWidthCalculationStrategy(new Fixed(100)),
      textColumn(GridColumnId.requestStatus)
        .withColumnPrototype(ResearchColumnPrototype.AUTOMATION_REQUEST_STATUS)
        .withEnumRenderer(AutomationRequestStatus, false, true)
        .isEditable(false)
        .withI18nKey('sqtm-core.entity.automation-request.status.label.short')
        .withTitleI18nKey('sqtm-core.entity.automation-request.status.label.full')
        .changeWidthCalculationStrategy(new Limited(130)),
      textColumn(GridColumnId.milestoneLabels)
        .withI18nKey('sqtm-core.search.test-case.grid.header.milestones.name.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.milestones.name.title')
        .disableSort()
        .changeWidthCalculationStrategy(new Limited(80)),
      concatenatedStatusesColumn(GridColumnId.milestoneStatus)
        .withI18nKey('sqtm-core.search.test-case.grid.header.milestones.status.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.milestones.status.title')
        .changeWidthCalculationStrategy(new Limited(80)),
      concatenatedDatesColumn(GridColumnId.milestoneEndDate)
        .withI18nKey('sqtm-core.search.test-case.grid.header.milestones.end-date.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.milestones.end-date.title')
        .changeWidthCalculationStrategy(new Limited(80)),
      executionModeColumn(GridColumnId.executionExecutionMode)
        .withColumnPrototype(ResearchColumnPrototype.EXECUTION_EXECUTION_MODE)
        .withI18nKey('sqtm-core.grid.header.execution-mode')
        .changeWidthCalculationStrategy(new Fixed(50)),
      executionStatusColumn(GridColumnId.executionStatus)
        .withI18nKey('sqtm-core.grid.header.execution-status')
        .withColumnPrototype(ResearchColumnPrototype.ITEM_TEST_PLAN_STATUS)
        .changeWidthCalculationStrategy(new Fixed(70)),
      textColumn(GridColumnId.lastExecutedBy)
        .withColumnPrototype(ResearchColumnPrototype.ITEM_TEST_PLAN_LASTEXECBY)
        .withI18nKey('sqtm-core.grid.header.executed-by')
        .changeWidthCalculationStrategy(new Limited(100)),
      textColumn(GridColumnId.assigneeLogin)
        .withColumnPrototype(ResearchColumnPrototype.ITEM_TEST_PLAN_TESTER)
        .withI18nKey('sqtm-core.entity.automation-request.assigned-to.label')
        .changeWidthCalculationStrategy(new Limited(100)),
      dateTimeColumn(GridColumnId.lastExecutedOn)
        .withColumnPrototype(ResearchColumnPrototype.ITEM_TEST_PLAN_LASTEXECON)
        .withI18nKey('sqtm-core.grid.header.executed-on')
        .changeWidthCalculationStrategy(new Extendable(85, 1)),
      startExecutionSearchColumn(GridColumnId.execution)
        .disableHeader()
        .withViewport('rightViewport')
        .changeWidthCalculationStrategy(new Fixed(40)),
      iconLinkColumn(GridColumnId.folder, {
        kind: 'iconLink',
        columnParamId: 'iterationId',
        iconName: 'folder',
        baseUrl: '/campaign-workspace/iteration',
      }).withViewport('rightViewport'),
    ])
    .withInitialSortedColumns([
      { id: GridColumnId.projectName, sort: Sort.ASC },
      { id: GridColumnId.campaignName, sort: Sort.ASC },
      { id: GridColumnId.iterationName, sort: Sort.ASC },
    ])
    .withStyle(new StyleDefinitionBuilder().enableInitialLoadAnimation())
    .withScopeDefinition(new ScopeDefinitionBuilder().withCustomScope('campaign'))
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
};

export function convertAllRowsInCampaign(
  literals: Partial<DataRow>[],
  projectsData: ProjectDataMap,
): SquashTmDataRow[] {
  return literals
    .filter((literal) => {
      const project = projectsData[literal.projectId];
      return project?.permissions.CAMPAIGN_LIBRARY.some(
        (permission) => permission === Permissions.READ,
      );
    })
    .map((literal) => convertOneInCampaign(literal, projectsData));
}

export function convertOneInCampaign(
  literal: Partial<DataRow>,
  projectsData: ProjectDataMap,
): SquashTmDataRow {
  const dataRow: DataRow = new IterationTestPlanItemRow();
  dataRow.projectId = literal.projectId;
  const project = projectsData[dataRow.projectId];
  dataRow.simplePermissions = new CampaignPermissions(project);
  Object.assign(dataRow, literal);
  return dataRow;
}
