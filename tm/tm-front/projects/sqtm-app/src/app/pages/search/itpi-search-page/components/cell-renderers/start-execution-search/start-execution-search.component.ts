import { APP_BASE_HREF } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  ViewContainerRef,
} from '@angular/core';
import { Router } from '@angular/router';
import { catchError, filter, map, take, takeUntil } from 'rxjs/operators';
import {
  AbstractCellRendererComponent,
  ActionErrorDisplayService,
  AutomatedSuiteCreationSpecification,
  AutomatedSuiteService,
  ColumnDefinitionBuilder,
  DialogService,
  EntityReference,
  EntityType,
  GridColumnId,
  GridService,
  IterationService,
  Permissions,
  ReferentialDataService,
  ReferentialDataState,
} from 'sqtm-core';
import { AutomatedTestsExecutionSupervisionDialogComponent } from '../../../../../campaign-workspace/iteration-view/components/automated-tests-execution-supervision-dialog/automated-tests-execution-supervision-dialog.component';
import { ExecutionRunnerOpenerService } from '../../../../../execution/execution-runner/services/execution-runner-opener.service';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Component({
  selector: 'sqtm-app-start-execution-search',
  templateUrl: 'start-execution-search.component.html',
  styleUrls: ['./start-execution-search.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StartExecutionSearchComponent
  extends AbstractCellRendererComponent
  implements OnDestroy
{
  menuVisible$ = new BehaviorSubject<boolean>(false);

  unsub$ = new Subject<void>();

  constructor(
    grid: GridService,
    public cdRef: ChangeDetectorRef,
    private viewContainerRef: ViewContainerRef,
    private iterationService: IterationService,
    private router: Router,
    private dialogService: DialogService,
    private automatedSuiteService: AutomatedSuiteService,
    private executionRunnerOpenerService: ExecutionRunnerOpenerService,
    private actionErrorDisplayService: ActionErrorDisplayService,
    @Inject(APP_BASE_HREF) private baseUrl: string,
    private referentialDataService: ReferentialDataService,
  ) {
    super(grid, cdRef);
  }

  startExecutionInDialog() {
    if (!this.row.data.hasBlockingMilestone) {
      const iterationId = this.row.data.iterationId;
      const testPlanItemId = this.row.data.id;
      if (iterationId && testPlanItemId) {
        this.persistExecutionAndOpenDialog(iterationId, testPlanItemId);
      } else {
        throw Error(
          `Unable to start execution in iteration ${iterationId} for itpi ${testPlanItemId}`,
        );
      }
    }
  }

  private persistExecutionAndOpenDialog(iterationId, testPlanItemId) {
    this.iterationService
      .persistManualExecution(iterationId, testPlanItemId)
      .pipe(
        take(1),
        catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
      )
      .subscribe((executionId) => {
        this.menuVisible$.next(false);
        this.grid.refreshData();
        this.executionRunnerOpenerService.openExecutionPrologue(executionId);
      });
  }

  openExecutionMenu() {
    this.menuVisible$.next(true);
    this.cdRef.detectChanges();
  }

  canExecute(): Observable<boolean> {
    return this.referentialDataService.referentialData$.pipe(
      takeUntil(this.unsub$),
      map((referentialData: ReferentialDataState) => this.canLaunchExecution(referentialData)),
    );
  }

  private canLaunchExecution(referentialData: ReferentialDataState): boolean {
    const { admin, userId }: { admin: boolean; userId: number } = referentialData.userState;
    const {
      executionExecutionMode,
      projectId,
      assigneeId,
    }: { executionExecutionMode: string; projectId: number; assigneeId: number | null } =
      this.row.data;

    if (admin || executionExecutionMode === 'EXPLORATORY') {
      return true;
    }
    const permissions =
      referentialData.projectState.entities[projectId].permissions.CAMPAIGN_LIBRARY;
    const canExecute: boolean = permissions.includes(Permissions.EXECUTE);
    const canReadUnassigned: boolean = permissions.includes(Permissions.READ_UNASSIGNED);
    const isAssigned: boolean = assigneeId === userId;
    return canExecute && (canReadUnassigned || isAssigned);
  }

  startExecutionInPage() {
    if (!this.row.data.hasBlockingMilestone) {
      const iterationId = this.row.data.iterationId;
      const testPlanItemId = this.row.data.id;
      if (iterationId && testPlanItemId) {
        this.persistExecutionAndGoToExecutionPage(iterationId, testPlanItemId);
      } else {
        throw Error(
          `Unable to start execution in iteration ${iterationId} for itpi ${testPlanItemId}`,
        );
      }
    }
  }

  startAutomatedExecution() {
    if (!this.row.data.hasBlockingMilestone) {
      this.menuVisible$.next(false);
      const iterationId = this.row.data.iterationId;
      const itemIdAsArray: number[] = [this.row.data.id];
      this.openAutomatedExecutionSupervisionDialog({
        context: new EntityReference(iterationId, EntityType.ITERATION),
        testPlanSubsetIds: itemIdAsArray,
        iterationId: iterationId,
      });
    }
  }

  private openAutomatedExecutionSupervisionDialog(data: AutomatedSuiteCreationSpecification) {
    const automatedExecutionDialog = this.dialogService.openDialog({
      id: 'automated-tests-execution-supervision',
      viewContainerReference: this.viewContainerRef,
      component: AutomatedTestsExecutionSupervisionDialogComponent,
      data,
      height: 680,
      width: 800,
    });
    automatedExecutionDialog.dialogClosed$
      .pipe(filter((result) => Boolean(result)))
      .subscribe(() => this.grid.refreshData());
  }

  private persistExecutionAndGoToExecutionPage(iterationId: number, testPlanItemId: number) {
    this.iterationService
      .persistManualExecution(iterationId, testPlanItemId)
      .pipe(
        take(1),
        catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
      )
      .subscribe((executionId) => {
        this.router.navigate(['execution', executionId]);
      });
  }

  ngOnDestroy(): void {
    this.menuVisible$.complete();
    this.unsub$.next();
  }

  accessSessionOverview() {
    this.router.navigate(['session-overview', this.row.data.overviewId]);
  }

  public isExploratoryExecution() {
    return this.row.data.executionExecutionMode === 'EXPLORATORY';
  }
}

export function startExecutionSearchColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(StartExecutionSearchComponent);
}
