import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'sqtm-app-test-case-by-req-for-ts-search-page',
  template: `
    <sqtm-app-test-case-by-req-for-campaign-workspace-entities-search-page
      [workspaceName]="'requirement-workspace'"
      [titleKey]="'sqtm-core.search.requirement-coverage.title'"
      [containerType]="'testSuiteId'"
      [url]="'test-suite'"
    >
    </sqtm-app-test-case-by-req-for-campaign-workspace-entities-search-page>
  `,
  styleUrls: ['./test-case-by-req-for-ts-search-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestCaseByReqForTsSearchPageComponent {}
