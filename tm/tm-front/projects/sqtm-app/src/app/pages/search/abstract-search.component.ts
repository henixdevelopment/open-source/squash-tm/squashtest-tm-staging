import {
  ChangeDetectorRef,
  Directive,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import {
  concatMap,
  filter,
  map,
  switchMap,
  take,
  takeUntil,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import {
  ColumnWithFilter,
  CustomColumnsComponent,
  CustomField,
  CustomFieldBinding,
  FilterGroup,
  getMilestonesColumnIds,
  GridColumnId,
  GridFilter,
  GridId,
  GridService,
  GridState,
  InputType,
  Limited,
  LocalPersistenceService,
  ProjectState,
  ReferentialDataService,
  ReferentialDataState,
} from 'sqtm-core';
import { combineLatestWith, Observable, of, Subject } from 'rxjs';
import { SearchViewComponent } from './search-view/containers/search-view/search-view.component';
import { ComponentPortal } from '@angular/cdk/portal';

@Directive()
export abstract class AbstractSearchComponent implements OnInit, OnDestroy {
  protected CUF_COLUMN_PREFIX = 'cuf|';
  protected readonly showColumnsStateStorageKey = 'show-columns-state';
  public readonly NO_QUERY_EMPTY_GRID_MESSAGE = 'sqtm-core.grid.empty-grid.search';

  filters: GridFilter[];
  filterGroups: FilterGroup[];
  @ViewChild(SearchViewComponent)
  protected searchViewComponent: SearchViewComponent;
  @ViewChild('iconColumnElement')
  iconColumnElement: ElementRef;
  protected overlayRef: OverlayRef;
  protected unsub$ = new Subject<void>();
  hasSelectedRows$: Observable<boolean>;
  protected abstract searchGridBuilders(): any;
  protected abstract getBasicColumnIds(): string[];
  protected abstract getExceptionColumnIds(): string[];
  protected abstract getExtraPremiumColumnId(): string[];
  protected abstract loadResearchData(): Observable<any>;
  protected abstract getCufBindings(entities: any, entitiesKey: any): CustomFieldBinding[];
  protected abstract setExceptionColumnsVisibility(shouldDisplay?: boolean): void;

  protected constructor(
    protected referentialDataService: ReferentialDataService,
    protected gridService: GridService,
    protected overlay: Overlay,
    protected localPersistenceService: LocalPersistenceService,
    protected cdRef: ChangeDetectorRef,
    protected vcr: ViewContainerRef,
  ) {
    this.observeMilestoneFeatureEnabled();
  }
  protected observeMilestoneFeatureEnabled(): void {
    this.referentialDataService.globalConfiguration$
      .pipe(
        takeUntil(this.unsub$),
        map((configuration) => configuration.milestoneFeatureEnabled),
      )
      .subscribe((milestoneFeatureEnabled) => {
        this.buildFilters(milestoneFeatureEnabled);
      });
  }
  protected buildFilters(milestoneFeatureEnabled: boolean): void {
    const builders = this.searchGridBuilders();
    this.filters = builders.buildFilters();
    this.filterGroups = builders.buildFilterGroups();

    if (!milestoneFeatureEnabled) {
      this.filterGroups = this.filterGroups.filter((group) => group.id !== 'milestones');
    }
  }

  ngOnInit(): void {
    this.initObservers();

    this.referentialDataService
      .refresh()
      .pipe(
        withLatestFrom(this.referentialDataService.referentialData$),
        tap(([, refData]) =>
          this.buildFilters(refData.globalConfigurationState.milestoneFeatureEnabled),
        ),
        switchMap(() => this.loadSimplifiedColumnDisplayValueFromLocalStorage()),
        concatMap(() => this.loadResearchData()),
        withLatestFrom(this.referentialDataService.referentialData$),
        tap(([, refData]) => this.searchViewComponent.initializeResearch(refData)),
        concatMap(() => this.initializeColumnDisplay()),
      )
      .subscribe();
  }

  protected initObservers() {
    this.hasSelectedRows$ = this.gridService.hasSelectedRows$.pipe(takeUntil(this.unsub$));
  }

  ngOnDestroy(): void {
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }
  protected setColumnType(_columnId: string, _inputType: InputType): any {
    return null;
  }
  protected initializeColumnDisplay(): Observable<any> {
    return this.referentialDataService.referentialData$.pipe(
      takeUntil(this.unsub$),
      combineLatestWith(this.gridService.loaded$),
      filter(([, loaded]) => loaded),
      tap(([refData]) => {
        if (refData.globalConfigurationState.searchActivationFeatureEnabled) {
          this.gridService.setEmptyGridMessage(this.NO_QUERY_EMPTY_GRID_MESSAGE);
        }
        if (!refData.premiumPluginInstalled) {
          this.displayIfNotPremium(refData);
        } else {
          this.displayIfPremium(refData);
        }
      }),
    );
  }
  protected displayIfNotPremium(referentialData?: ReferentialDataState): void {
    this.gridService.gridState$
      .pipe(
        take(1),
        tap((gridState) => {
          const simplifiedColumnDisplayGridIds =
            gridState.configurationState.simplifiedColumnDisplayGridIds;
          const gridId = gridState.definitionState.id;
          const isSimplifiedColumnDisplayGrid = simplifiedColumnDisplayGridIds.includes(gridId);
          this.setExceptionColumnsVisibility(!isSimplifiedColumnDisplayGrid);
          this.hideExtraPremiumColumnsInCommunityVersion();

          if (referentialData.globalConfigurationState.milestoneFeatureEnabled) {
            this.gridService.setColumnVisibility(
              GridColumnId.milestones,
              !isSimplifiedColumnDisplayGrid,
            );
          } else {
            getMilestonesColumnIds().forEach((col) =>
              this.gridService.setColumnVisibility(col, false),
            );
          }
        }),
      )
      .subscribe();
  }
  protected hideExtraPremiumColumnsInCommunityVersion() {
    this.getExtraPremiumColumnId().forEach((col) => {
      this.gridService.setColumnVisibility(col, false);
    });
  }

  showAvailableFilters(): void {
    this.cdRef.detectChanges();

    const positionStrategy = this.overlay
      .position()
      .flexibleConnectedTo(this.iconColumnElement)
      .withPositions([
        {
          originX: 'center',
          overlayX: 'center',
          originY: 'bottom',
          overlayY: 'top',
          offsetY: 0,
        },
        {
          originX: 'center',
          overlayX: 'center',
          originY: 'top',
          overlayY: 'bottom',
          offsetY: -10,
        },
      ]);

    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
    };

    this.overlayRef = this.overlay.create(overlayConfig);
    const componentPortal = new ComponentPortal(CustomColumnsComponent, this.vcr);
    const componentComponentRef = this.overlayRef.attach(componentPortal);
    this.overlayRef.backdropClick().subscribe(() => this.close());
    componentComponentRef.instance.confirmClicked.subscribe(() => this.close());
  }

  close() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.cdRef.detectChanges();
    }
  }

  protected displayIfPremium(referentialData: ReferentialDataState): void {
    this.gridService.gridState$
      .pipe(
        take(1),
        switchMap((gridState: GridState) => this.fetchActiveColumnIds(referentialData, gridState)),
        withLatestFrom(this.gridService.columns$),
        map(([activeColumnIds, columns]: [string[], ColumnWithFilter[]]) => {
          if (activeColumnIds.length === 0) {
            this.hideAllExceptBasicColumns(columns);
          }
          this.hideInactiveColumnsIfSearchFeatureEnabled(referentialData, activeColumnIds);
          this.hideColumnsIfMilestoneFeatureDisabled(referentialData);
          this.addCufPremiumColumns(referentialData, columns, activeColumnIds);
          //  TODO JMA 05/06/24: solve removeSortOnColumnsToHide(gridState) (should not refreshRows) and call it here (issue 1919)
        }),
      )
      .subscribe();
  }
  private fetchActiveColumnIds(
    referentialData: ReferentialDataState,
    gridState: GridState,
  ): Observable<string[]> {
    if (referentialData.globalConfigurationState.searchActivationFeatureEnabled) {
      return this.gridService.getActiveColumnsIds(gridState.definitionState.id);
    } else {
      return of(gridState.configurationState.columnConfigurationState.activeColumnIds);
    }
  }

  private hideInactiveColumnsIfSearchFeatureEnabled(
    referentialData: ReferentialDataState,
    activeColumnIds: string[],
  ) {
    if (this.isSearchFeatureEnabledWithActiveColumnIds(referentialData, activeColumnIds)) {
      const allColumns = [
        ...this.getBasicColumnIds(),
        ...this.getExceptionColumnIds(),
        ...this.getExtraPremiumColumnId(),
      ];

      allColumns
        .filter((col) => !activeColumnIds.includes(col))
        .forEach((col) => {
          this.gridService.setColumnVisibility(col, false);
        });
    }
  }

  private isSearchFeatureEnabledWithActiveColumnIds(
    referentialData: ReferentialDataState,
    activeColumnIds: string[],
  ): boolean {
    return (
      referentialData.globalConfigurationState.searchActivationFeatureEnabled &&
      activeColumnIds.length !== 0
    );
  }

  private hideColumnsIfMilestoneFeatureDisabled(referentialData: ReferentialDataState): void {
    const milestoneFeatureEnabled =
      referentialData.globalConfigurationState.milestoneFeatureEnabled;
    if (!milestoneFeatureEnabled) {
      getMilestonesColumnIds().forEach((col) => this.gridService.setColumnVisibility(col, false));
    }
  }
  protected addCufPremiumColumns(
    referentialData: ReferentialDataState,
    columns: ColumnWithFilter[],
    activeColumnIds: string[],
  ): void {
    const cufColumnIds: CufColumnInfo[] = this.buildCufColumnID(referentialData);
    const columnsToAdd = cufColumnIds.map((cufColumn) => {
      return this.createCufColumn(cufColumn.columnId, cufColumn.customField);
    });
    const columnNumber = columns.length;
    this.gridService.addColumnAtIndex(columnsToAdd, columnNumber);
    columnsToAdd.forEach((columnToAdd) => {
      this.gridService.setColumnVisibility(
        columnToAdd.id,
        activeColumnIds.includes(columnToAdd.id),
      );
    });
  }

  protected createCufColumn(columnId: string, cuf: CustomField) {
    const { id, inputType, label } = cuf;
    const colDefinition = this.setColumnType(columnId, inputType);
    return colDefinition
      .withCufId(id)
      .withI18nKey(label)
      .changeWidthCalculationStrategy(new Limited(100))
      .build(GridId.REQUIREMENT_SEARCH);
  }
  protected buildCufColumnID(referentialData: ReferentialDataState) {
    const cufBindings = this.getAllCustomFieldBindings(referentialData.projectState);
    const cufs = referentialData.customFieldState.entities;
    const columnsToAdd: { columnId: string; customField: CustomField }[] = [];
    cufBindings.forEach((cufBinding) => {
      const cuf = cufs[cufBinding];
      if (cuf.inputType !== InputType.RICH_TEXT) {
        columnsToAdd.push({
          columnId: this.CUF_COLUMN_PREFIX + cuf.id,
          customField: cuf,
        });
      }
    });
    columnsToAdd.sort((cufA, cufB) => cufA.customField.label.localeCompare(cufB.customField.label));
    return columnsToAdd;
  }
  protected getAllCustomFieldBindings(projects: ProjectState): number[] {
    const result = [];
    const entities = projects.entities;
    for (const entitiesKey of Object.keys(entities)) {
      const cufBindings = this.getCufBindings(entities, entitiesKey);
      cufBindings.forEach((cuf) => {
        if (!result.includes(cuf.customFieldId)) {
          result.push(cuf.customFieldId);
        }
      });
    }
    return result;
  }
  toggleAdditionalColumnsVisibility() {
    this.gridService.gridState$
      .pipe(
        take(1),
        switchMap((gridState) => {
          const simplifiedColumnDisplayGridIds =
            this.updateSimplifiedColumnDisplayGridIds(gridState);

          return this.localPersistenceService
            .set(this.showColumnsStateStorageKey, simplifiedColumnDisplayGridIds)
            .pipe(map(() => simplifiedColumnDisplayGridIds));
        }),
        withLatestFrom(
          this.gridService.gridState$,
          this.referentialDataService.globalConfiguration$,
        ),
        switchMap(([simplifiedColumnDisplayGridIds, gridState, globalConf]) => {
          const isSimplifiedColumnDisplayGrid = simplifiedColumnDisplayGridIds.includes(
            gridState.definitionState.id,
          );
          this.setExceptionColumnsVisibility(!isSimplifiedColumnDisplayGrid);

          if (globalConf.milestoneFeatureEnabled) {
            this.gridService.setColumnVisibility(
              GridColumnId.milestones,
              !isSimplifiedColumnDisplayGrid,
            );
          }

          return this.gridService.updateConfigurationState({
            ...gridState.configurationState,
            simplifiedColumnDisplayGridIds,
          });
        }),
        withLatestFrom(this.gridService.gridState$),
        tap(([_, gridState]) => this.removeSortOnColumnsToHide(gridState)),
      )
      .subscribe();
  }
  private updateSimplifiedColumnDisplayGridIds(gridState: GridState): string[] {
    const simplifiedColumnDisplayGridIds = [
      ...gridState.configurationState.simplifiedColumnDisplayGridIds,
    ];
    const gridId = gridState.definitionState.id;
    const index = simplifiedColumnDisplayGridIds.indexOf(gridId);

    if (index !== -1) {
      simplifiedColumnDisplayGridIds.splice(index, 1);
    } else {
      simplifiedColumnDisplayGridIds.push(gridId);
    }

    return simplifiedColumnDisplayGridIds;
  }

  protected removeSortOnColumnsToHide(gridState: GridState): void {
    const sortedColumns = gridState.columnState.sortedColumns;
    const exceptionColumnIds = this.getExceptionColumnIds();
    const activeColumnIds = gridState.configurationState.columnConfigurationState.activeColumnIds;
    const columnsToSort = sortedColumns.filter((sortedColumn) => {
      const isActiveColumn =
        activeColumnIds.length === 0 || activeColumnIds.includes(sortedColumn.id.toString());
      const isExceptionColumn = exceptionColumnIds.includes(sortedColumn.id.toString());
      return !isExceptionColumn && isActiveColumn;
    });
    this.gridService.setColumnSorts(columnsToSort);
  }
  protected hideAllExceptBasicColumns(columns: ColumnWithFilter[]) {
    const basicColumns = this.getBasicColumnIds();
    for (const col of columns) {
      if (!basicColumns.includes(col.id)) {
        this.gridService.setColumnVisibility(col.id, false);
      }
    }
  }

  private loadSimplifiedColumnDisplayValueFromLocalStorage() {
    return this.localPersistenceService.get(this.showColumnsStateStorageKey).pipe(
      withLatestFrom(this.gridService.gridState$),
      switchMap(([simplifiedColumnDisplayGridIds, gridState]: [string[], GridState]) => {
        const gridId = gridState.definitionState.id;
        simplifiedColumnDisplayGridIds = simplifiedColumnDisplayGridIds
          ? [...simplifiedColumnDisplayGridIds]
          : [gridId];
        return this.gridService.updateConfigurationState({
          ...gridState.configurationState,
          simplifiedColumnDisplayGridIds: simplifiedColumnDisplayGridIds,
        });
      }),
    );
  }
}
interface CufColumnInfo {
  columnId: string;
  customField: CustomField;
}
