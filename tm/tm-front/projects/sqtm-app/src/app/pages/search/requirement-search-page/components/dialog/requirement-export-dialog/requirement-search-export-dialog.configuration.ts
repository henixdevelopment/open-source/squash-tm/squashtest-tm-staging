export class RequirementSearchExportDialogConfiguration {
  id: string;
  titleKey: string;
  requirementVersionIds: number[];
  simplifiedColumnDisplayGridIds: string[];
}
