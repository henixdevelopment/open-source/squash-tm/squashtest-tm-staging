import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RequirementSearchPageComponent } from './containers/requirement-search-page/requirement-search-page.component';
import { SearchViewModule } from '../search-view/search-view.module';
import { RequirementEditableCellRendererComponent } from './components/cell-renderers/requirement-editable-cell-renderer/requirement-editable-cell-renderer.component';
import { FilterLinkTypeComponent } from './components/filters/filter-link-type/filter-link-type.component';
import { RequirementForCoverageSearchPageComponent } from './containers/requirement-for-coverage-search-page/requirement-for-coverage-search-page.component';
import { DialogModule, FiltersModule, WorkspaceCommonModule } from 'sqtm-core';
import { TranslateModule } from '@ngx-translate/core';
import { RequirementMultiEditDialogComponent } from './components/dialog/requirement-multi-edit-dialog/requirement-multi-edit-dialog.component';
import { RequirementSearchExportDialogComponent } from './components/dialog/requirement-export-dialog/requirement-search-export-dialog.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NavigateToRequirementCellComponent } from './components/cell-renderers/navigate-to-requirement-cell/navigate-to-requirement-cell.component';
import { ReqVersionBoundToItemCheckboxRenderer } from './components/cell-renderers/req-version-bound-to-item-checkbox-renderer/req-version-bound-to-item-checkbox-renderer.component';
import { RequirementForSprintSearchPageComponent } from './containers/requirement-for-sprint-search-page/requirement-for-sprint-search-page.component';

const routes: Routes = [
  {
    path: '',
    component: RequirementSearchPageComponent,
    pathMatch: 'full',
  },
  {
    path: 'coverage/:testCaseId',
    component: RequirementForCoverageSearchPageComponent,
  },
  {
    path: 'coverage/:testCaseId/:stepId',
    component: RequirementForCoverageSearchPageComponent,
  },
  {
    path: 'sprint/:sprintId',
    component: RequirementForSprintSearchPageComponent,
  },
];

@NgModule({
  declarations: [
    FilterLinkTypeComponent,
    RequirementSearchPageComponent,
    RequirementEditableCellRendererComponent,
    RequirementForCoverageSearchPageComponent,
    RequirementForSprintSearchPageComponent,
    RequirementMultiEditDialogComponent,
    RequirementSearchExportDialogComponent,
    ReqVersionBoundToItemCheckboxRenderer,
    NavigateToRequirementCellComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SearchViewModule,
    WorkspaceCommonModule,
    NzToolTipModule,
    NzIconModule,
    TranslateModule.forChild(),
    DialogModule,
    ReactiveFormsModule,
    NzCheckboxModule,
    NzIconModule,
    NzButtonModule,
    FiltersModule,
  ],
})
export class RequirementSearchPageModule {}
