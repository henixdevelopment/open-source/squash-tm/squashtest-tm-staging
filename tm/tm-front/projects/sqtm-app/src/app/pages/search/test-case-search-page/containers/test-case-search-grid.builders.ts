import {
  BindableEntity,
  DataRow,
  FilterGroup,
  FilterTestCaseMilestoneLabelComponent,
  GridColumnId,
  GridFilter,
  GridId,
  LocalPersistenceService,
  ProjectDataMap,
  ResearchColumnPrototype,
  ScopeDefinitionBuilder,
  Sort,
  SquashTmDataRow,
  StyleDefinitionBuilder,
  TestCase,
  TestCasePermissions,
  automatedTestTechnologyResearchFilter,
  buildFilters,
  dateFilter,
  fullTextResearchFilter,
  i18nEnumResearchFilter,
  i18nEnumSingleSelectionResearchFilter,
  infoListResearchFilter,
  multipleListResearchFilter,
  numericResearchFilter,
  Permissions,
  searchGrid,
  textResearchFilter,
  userHistoryResearchFilter,
} from 'sqtm-core';
import { MILESTONE_FILTER } from '../../search-constants';
import { InjectionToken } from '@angular/core';
import { colDefBuilderTestCaseSearch } from './abstract-test-case-search.component';

export class TestCaseSearchGridBuilders {
  buildFilters(): GridFilter[] {
    return buildFilters([
      numericResearchFilter(GridColumnId.id, ResearchColumnPrototype.TEST_CASE_ID)
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.generic.id.capitalize'),
      textResearchFilter(GridColumnId.reference, ResearchColumnPrototype.TEST_CASE_REFERENCE)
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.generic.reference.label'),
      textResearchFilter(GridColumnId.name, ResearchColumnPrototype.TEST_CASE_NAME)
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.generic.name.label'),
      fullTextResearchFilter(
        GridColumnId.description,
        ResearchColumnPrototype.TEST_CASE_DESCRIPTION,
      )
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.generic.description.label'),
      fullTextResearchFilter(
        GridColumnId.prerequisite,
        ResearchColumnPrototype.TEST_CASE_PREQUISITE,
      )
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.test-case.prerequisites.label'),
      i18nEnumResearchFilter(GridColumnId.kind, ResearchColumnPrototype.TEST_CASE_KIND)
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.test-case.kind.label'),
      i18nEnumResearchFilter(
        GridColumnId.draftedByAi,
        ResearchColumnPrototype.TEST_CASE_DRAFTED_BY_AI,
      )
        .withGroupId('informations')
        .withI18nKey('sqtm-core.test-case-workspace.artificial-intelligence.capsule.drafted-by-ai'),
      dateFilter(GridColumnId.createdOn, ResearchColumnPrototype.TEST_CASE_CREATED_ON)
        .withGroupId('historical')
        .withI18nKey('sqtm-core.entity.generic.created-on.masculine'),
      userHistoryResearchFilter(
        GridColumnId.createdBy,
        ResearchColumnPrototype.TEST_CASE_CREATED_BY,
      )
        .withGroupId('historical')
        .withI18nKey('sqtm-core.entity.generic.created-by.masculine'),
      dateFilter(GridColumnId.modifiedOn, ResearchColumnPrototype.TEST_CASE_MODIFIED_ON)
        .withGroupId('historical')
        .withI18nKey('sqtm-core.entity.generic.last-modified-on.masculine'),
      userHistoryResearchFilter(
        GridColumnId.modifiedBy,
        ResearchColumnPrototype.TEST_CASE_MODIFIED_BY,
      )
        .withGroupId('historical')
        .withI18nKey('sqtm-core.entity.generic.last-modified-by.masculine'),
      i18nEnumResearchFilter(
        GridColumnId.automatable,
        ResearchColumnPrototype.TEST_CASE_AUTOMATABLE,
      )
        .withGroupId('automation')
        .withI18nKey('sqtm-core.generic.label.automation.indicator'),
      numericResearchFilter(
        GridColumnId.automationPriority,
        ResearchColumnPrototype.AUTOMATION_REQUEST_AUTOMATION_PRIORITY,
      )
        .withGroupId('automation')
        .withI18nKey('sqtm-core.entity.automation-request.automation-priority'),
      dateFilter(
        GridColumnId.transmissionDate,
        ResearchColumnPrototype.AUTOMATION_REQUEST_TRANSMISSION_DATE,
      )
        .withGroupId('automation')
        .withI18nKey('sqtm-core.entity.automation-request.transmission-date'),
      i18nEnumResearchFilter(
        GridColumnId.automationRequestStatus,
        ResearchColumnPrototype.AUTOMATION_REQUEST_STATUS,
      )
        .withGroupId('automation')
        .withI18nKey('sqtm-core.generic.label.automation.status'),
      i18nEnumSingleSelectionResearchFilter(
        GridColumnId.hasAutoScript,
        ResearchColumnPrototype.TEST_CASE_HASAUTOSCRIPT,
      )
        .withGroupId('automation')
        .withI18nKey('sqtm-core.entity.test-case.has-auto-script'),
      automatedTestTechnologyResearchFilter(
        GridColumnId.automatedTestTechnology,
        ResearchColumnPrototype.TEST_CASE_AUTOMATED_TEST_TECHNOLOGY,
      )
        .withGroupId('automation')
        .withI18nKey('sqtm-core.entity.test-case.automated-test-technology'),
      i18nEnumSingleSelectionResearchFilter(
        GridColumnId.hasBoundTestCaseRepository,
        ResearchColumnPrototype.TEST_CASE_HAS_BOUND_SCM_REPOSITORY,
      )
        .withGroupId('automation')
        .withI18nKey('sqtm-core.entity.test-case.has-bound-scm-repository'),
      i18nEnumSingleSelectionResearchFilter(
        GridColumnId.hasBoundAutomatedTestReference,
        ResearchColumnPrototype.TEST_CASE_HAS_BOUND_AUTOMATED_TEST_REFERENCE,
      )
        .withGroupId('automation')
        .withI18nKey('sqtm-core.entity.test-case.has-bound-automated-test-case-ref'),
      multipleListResearchFilter(
        MILESTONE_FILTER,
        ResearchColumnPrototype.TEST_CASE_MILESTONE_ID,
        FilterTestCaseMilestoneLabelComponent,
      )
        .withGroupId('milestones')
        .withI18nKey('sqtm-core.search.generic.criteria.milestone.name'),
      i18nEnumResearchFilter(
        GridColumnId.milestoneStatus,
        ResearchColumnPrototype.TEST_CASE_MILESTONE_STATUS,
      )
        .withGroupId('milestones')
        .withI18nKey('sqtm-core.search.generic.criteria.milestone.status'),
      dateFilter(
        GridColumnId.milestoneEndDate,
        ResearchColumnPrototype.TEST_CASE_MILESTONE_END_DATE,
      )
        .withGroupId('milestones')
        .withI18nKey('sqtm-core.entity.milestone.end-date.label'),
      i18nEnumResearchFilter(GridColumnId.status, ResearchColumnPrototype.TEST_CASE_STATUS)
        .withGroupId('attributes')
        .withI18nKey('sqtm-core.entity.generic.status.label'),
      i18nEnumResearchFilter(GridColumnId.importance, ResearchColumnPrototype.TEST_CASE_IMPORTANCE)
        .withGroupId('attributes')
        .withI18nKey('sqtm-core.entity.test-case.importance.label'),
      infoListResearchFilter(
        GridColumnId.nature,
        ResearchColumnPrototype.TEST_CASE_NATURE_ID,
      ).withI18nKey('sqtm-core.entity.test-case.nature.label'),
      infoListResearchFilter(
        GridColumnId.type,
        ResearchColumnPrototype.TEST_CASE_TYPE_ID,
      ).withI18nKey('sqtm-core.entity.test-case.type.label'),
      numericResearchFilter(GridColumnId.stepCount, ResearchColumnPrototype.TEST_CASE_STEPCOUNT)
        .withGroupId('content')
        .withI18nKey('sqtm-core.search.test-case.criteria.step-count'),
      numericResearchFilter(GridColumnId.paramCount, ResearchColumnPrototype.TEST_CASE_PARAMCOUNT)
        .withGroupId('content')
        .withI18nKey('sqtm-core.search.test-case.criteria.param-count'),
      numericResearchFilter(
        GridColumnId.datasetCount,
        ResearchColumnPrototype.TEST_CASE_DATASETCOUNT,
      )
        .withGroupId('content')
        .withI18nKey('sqtm-core.search.test-case.criteria.dataset-count'),
      numericResearchFilter(
        GridColumnId.callStepCount,
        ResearchColumnPrototype.TEST_CASE_CALLSTEPCOUNT,
      )
        .withGroupId('content')
        .withI18nKey('sqtm-core.search.test-case.criteria.call-step-count'),
      numericResearchFilter(
        GridColumnId.attachmentCount,
        ResearchColumnPrototype.TEST_CASE_ATTCOUNT,
      )
        .withGroupId('content')
        .withI18nKey('sqtm-core.search.generic.criteria.attachment-count'),
      numericResearchFilter(
        GridColumnId.coveragesCount,
        ResearchColumnPrototype.TEST_CASE_VERSCOUNT,
      )
        .withGroupId('associations')
        .withI18nKey('sqtm-core.search.generic.criteria.coverage-count'),
      numericResearchFilter(
        GridColumnId.iterationsCount,
        ResearchColumnPrototype.TEST_CASE_ITERCOUNT,
      )
        .withGroupId('associations')
        .withI18nKey('sqtm-core.search.test-case.criteria.iteration-count'),
      numericResearchFilter(
        GridColumnId.executionsCount,
        ResearchColumnPrototype.TEST_CASE_EXECOUNT,
      )
        .withGroupId('associations')
        .withI18nKey('sqtm-core.search.test-case.criteria.exec-count'),
      numericResearchFilter(GridColumnId.issuesCount, ResearchColumnPrototype.EXECUTION_ISSUECOUNT)
        .withGroupId('associations')
        .withI18nKey('sqtm-core.search.generic.criteria.issue-count'),
    ]);
  }

  public buildFilterGroups(): FilterGroup[] {
    return [
      {
        id: 'informations',
        i18nLabelKey: 'sqtm-core.generic.label.information.plural',
      },
      {
        id: 'attributes',
        i18nLabelKey: 'sqtm-core.entity.generic.attributes',
      },
      {
        id: 'historical',
        i18nLabelKey: 'sqtm-core.search.generic.criteria.groups.historical',
      },
      {
        id: 'automation',
        i18nLabelKey: 'sqtm-core.generic.label.automation.label',
      },
      {
        id: 'milestones',
        i18nLabelKey: 'sqtm-core.generic.label.milestone',
      },
      {
        id: 'content',
        i18nLabelKey: 'sqtm-core.search.generic.criteria.groups.content',
      },
      {
        id: 'associations',
        i18nLabelKey: 'sqtm-core.search.generic.criteria.groups.associations',
      },
      {
        id: 'custom-fields',
        i18nLabelKey: 'sqtm-core.entity.custom-field.label.plural',
      },
    ];
  }
}

export const TEST_CASE_RESEARCH_GRID_CONFIG = new InjectionToken(
  'Token for test case grid research config',
);
export const TEST_CASE_RESEARCH_GRID = new InjectionToken('Token for test case grid research');

export const searchTestCaseGridConfigFactory = (
  localPersistenceService: LocalPersistenceService,
) => {
  return searchGrid(GridId.TEST_CASE_SEARCH)
    .server()
    .withServerUrl(['search/test-case'])
    .withModificationUrl(['test-case'])
    .withRowConverter(convertAllRowsInTestCase)
    .withFilterCufFrom(BindableEntity.TEST_CASE)
    .withColumns(colDefBuilderTestCaseSearch(false))
    .withInitialSortedColumns([
      { id: GridColumnId.projectName, sort: Sort.ASC },
      { id: GridColumnId.reference, sort: Sort.ASC },
      { id: GridColumnId.name, sort: Sort.ASC },
    ])
    .withStyle(new StyleDefinitionBuilder().enableInitialLoadAnimation())
    .withScopeDefinition(new ScopeDefinitionBuilder().withCustomScope('test-case'))
    .enableColumnWidthPersistence(localPersistenceService)
    .build();
};

export function convertAllRowsInTestCase(
  literals: Partial<DataRow>[],
  projectsData: ProjectDataMap,
): SquashTmDataRow[] {
  return literals
    .filter((literal) => {
      const project = projectsData[literal.projectId];
      return project?.permissions.TEST_CASE_LIBRARY.some(
        (permission) => permission === Permissions.READ,
      );
    })
    .map((literal) => convertOneInTestCase(literal, projectsData));
}

export function convertOneInTestCase(
  literal: Partial<DataRow>,
  projectsData: ProjectDataMap,
): SquashTmDataRow {
  const dataRow: DataRow = new TestCase();
  dataRow.projectId = literal.projectId;
  const project = projectsData[dataRow.projectId];
  dataRow.simplePermissions = new TestCasePermissions(project);
  Object.assign(dataRow, literal);
  return dataRow;
}
