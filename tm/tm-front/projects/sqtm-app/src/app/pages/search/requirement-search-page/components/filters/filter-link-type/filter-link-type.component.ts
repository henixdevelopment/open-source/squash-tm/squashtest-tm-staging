import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractFilterWidget,
  DiscreteFilterValue,
  Filter,
  FilterOperation,
  isDiscreteValue,
  ListGroup,
  ListItem,
  MultiDiscreteFilterValue,
  ReferentialDataService,
  RequirementVersionLinkType,
  Scope,
} from 'sqtm-core';
import { take } from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-filter-link-type',
  templateUrl: './filter-link-type.component.html',
  styleUrls: ['./filter-link-type.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterLinkTypeComponent extends AbstractFilterWidget {
  items: ListItem[];
  groups: ListGroup[];

  private filterValue: DiscreteFilterValue[];

  constructor(
    protected cdRef: ChangeDetectorRef,
    private referentialDataService: ReferentialDataService,
  ) {
    super(cdRef);
  }

  setFilter(filter: Filter, _scope: Scope) {
    this.referentialDataService.requirementVersionLinkTypes$
      .pipe(take(1))
      .subscribe((requirementVersionLinkTypes: RequirementVersionLinkType[]) => {
        const filterValue = filter.value;
        if (isDiscreteValue(filterValue)) {
          this.initializeItems(filterValue, requirementVersionLinkTypes);
          this._filter = filter;
          this.filterValue = filterValue.value;
          this.cdRef.detectChanges();
        } else {
          throw Error('Not handled kind ' + filter.value.kind);
        }
      });
  }

  private initializeItems(
    filterValue: MultiDiscreteFilterValue,
    linkTypes: RequirementVersionLinkType[],
  ) {
    const selectedIds = filterValue.value.map((v) => v.id);
    this.items = linkTypes
      .map((linkType) => {
        const isSystem = linkType.role.includes(`requirement-version.link.type.`);
        const items = [
          {
            id: linkType.role1Code,
            label: isSystem ? '' : linkType.role,
            i18nLabelKey: isSystem ? `sqtm-core.entity.requirement.${linkType.role}` : '',
            selected: selectedIds.includes(linkType.role1Code),
          },
        ];
        if (linkType.role1Code !== linkType.role2Code) {
          const role2 = {
            id: linkType.role2Code,
            label: isSystem ? '' : linkType.role2,
            i18nLabelKey: isSystem ? `sqtm-core.entity.requirement.${linkType.role2}` : '',
            selected: selectedIds.includes(linkType.role2Code),
          };
          items.push(role2);
        }
        return items;
      })
      .flat();
  }

  changeSelection(item: ListItem) {
    let nextValue: DiscreteFilterValue[] = [...this.filterValue];
    if (item.selected) {
      nextValue = this.filterValue.concat({
        id: item.id,
        label: item.label,
        i18nLabelKey: item.i18nLabelKey,
      });
    } else {
      const index = this.filterValue.findIndex((v) => v.id === item.id);
      if (index >= 0) {
        nextValue.splice(index, 1);
      }
    }
    this.filteringChanged.next({
      filterValue: {
        kind: 'multiple-discrete-value',
        value: nextValue,
      },
    });
  }

  changeOperation(operation: FilterOperation) {
    this.filteringChanged.next({
      operation,
    });
  }
}
