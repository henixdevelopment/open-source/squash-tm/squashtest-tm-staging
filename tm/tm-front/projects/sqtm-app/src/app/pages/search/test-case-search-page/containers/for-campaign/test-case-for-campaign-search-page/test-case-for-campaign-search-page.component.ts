import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'sqtm-app-test-case-for-campaign-search-page',
  template: `
    <sqtm-app-test-case-for-campaign-workspace-entities-search-page
      [workspaceName]="'test-case-workspace'"
      [titleKey]="'sqtm-core.search.test-case.title'"
      [containerType]="'campaignId'"
      [url]="'campaign'"
    >
    </sqtm-app-test-case-for-campaign-workspace-entities-search-page>
  `,
  styleUrls: ['./test-case-for-campaign-search-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestCaseForCampaignSearchPageComponent {}
