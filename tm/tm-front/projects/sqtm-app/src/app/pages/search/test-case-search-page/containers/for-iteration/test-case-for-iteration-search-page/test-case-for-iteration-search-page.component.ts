import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'sqtm-app-test-case-for-iteration-search-page',
  template: `
    <sqtm-app-test-case-for-campaign-workspace-entities-search-page
      [workspaceName]="'test-case-workspace'"
      [titleKey]="'sqtm-core.search.test-case.title'"
      [containerType]="'iterationId'"
      [url]="'iteration'"
    >
    </sqtm-app-test-case-for-campaign-workspace-entities-search-page>
  `,
  styleUrls: ['./test-case-for-iteration-search-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestCaseForIterationSearchPageComponent {}
