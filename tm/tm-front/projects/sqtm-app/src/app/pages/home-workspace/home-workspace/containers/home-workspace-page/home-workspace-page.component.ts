import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { HomeWorkspaceService } from '../../services/home-workspace.service';
import {
  CustomDashboardModel,
  LicenseInformationState,
  LicenseInformationMessageProvider,
  LicenseMessagePlacement,
  ReferentialDataService,
} from 'sqtm-core';
import { map, take, withLatestFrom } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-home-workspace-page',
  templateUrl: './home-workspace-page.component.html',
  styleUrls: ['./home-workspace-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{ provide: HomeWorkspaceService, useClass: HomeWorkspaceService }],
})
export class HomeWorkspacePageComponent implements OnInit {
  licenseMessage: string;

  constructor(
    public homeWorkspaceService: HomeWorkspaceService,
    private referentialDataService: ReferentialDataService,
    private translateService: TranslateService,
  ) {}

  ngOnInit() {
    this.referentialDataService.refresh().subscribe(() => {
      this.homeWorkspaceService.loadWorkspace();
      this.initializeLicenseMessage();
    });
  }

  handleShowWelcomeMessage() {
    this.homeWorkspaceService.showWelcomeMessage();
  }

  handleShowDashboard() {
    this.homeWorkspaceService.showDashboard();
  }

  getChartBindings(dashboard: CustomDashboardModel) {
    return [...dashboard.reportBindings, ...dashboard.chartBindings];
  }

  private initializeLicenseMessage(): void {
    this.referentialDataService.licenseInformation$
      .pipe(
        take(1),
        map(
          (info: LicenseInformationState) =>
            new LicenseInformationMessageProvider(info, this.translateService),
        ),
        withLatestFrom(this.referentialDataService.isAdmin()),
        map(([provider, isAdmin]) =>
          provider.getLongMessage(LicenseMessagePlacement.HOME, isAdmin),
        ),
      )
      .subscribe((longMessage) => (this.licenseMessage = longMessage));
  }
}
