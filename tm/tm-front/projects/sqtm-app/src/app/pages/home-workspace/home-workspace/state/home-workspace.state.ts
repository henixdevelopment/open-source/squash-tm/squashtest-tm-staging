import { CustomDashboardModel } from 'sqtm-core';

export interface HomeWorkspaceState {
  welcomeMessage: string;
  showDashboard: boolean;
  loaded: boolean;
  dashboard: CustomDashboardModel;
}

export function initialHomeWorkspaceState(): HomeWorkspaceState {
  return {
    welcomeMessage: null,
    showDashboard: false,
    loaded: false,
    dashboard: null,
  };
}
