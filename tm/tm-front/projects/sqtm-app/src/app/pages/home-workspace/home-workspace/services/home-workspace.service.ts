import { Injectable } from '@angular/core';
import {
  createStore,
  FavoriteDashboardValue,
  HomeWorkspaceModel,
  PartyPreferencesService,
  RestService,
} from 'sqtm-core';
import { HomeWorkspaceState, initialHomeWorkspaceState } from '../state/home-workspace.state';
import { concatMap, filter, map, take, withLatestFrom } from 'rxjs/operators';

@Injectable()
export class HomeWorkspaceService {
  private store = createStore<HomeWorkspaceState>(initialHomeWorkspaceState());

  public componentData$ = this.store.state$.pipe(filter((state) => state.loaded));

  constructor(
    private rest: RestService,
    private preferencesService: PartyPreferencesService,
  ) {}

  loadWorkspace(): void {
    this.store.state$
      .pipe(
        take(1),
        concatMap((state: HomeWorkspaceState) => {
          return this.rest
            .get<HomeWorkspaceModel>(['home-workspace'])
            .pipe(map((model) => ({ ...state, ...model, loaded: true })));
        }),
      )
      .subscribe((state) => this.store.commit(state));
  }

  showWelcomeMessage() {
    this.switchContent(false);
  }

  showDashboard() {
    this.switchContent(true);
  }

  private switchContent(showDashboard: boolean) {
    const preferenceValue: FavoriteDashboardValue = showDashboard ? 'dashboard' : 'default';
    this.preferencesService
      .changeHomeWorkspaceFavoriteDashboard(preferenceValue)
      .pipe(
        withLatestFrom(this.store.state$),
        map(([, state]: [void, HomeWorkspaceState]) => ({ ...state, showDashboard })),
      )
      .subscribe((state) => this.store.commit(state));
  }
}
