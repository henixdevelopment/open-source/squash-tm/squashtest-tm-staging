/**
 * This is where you would enable or disable experimental feature flags in development mode.
 * Note that this won't have any effect in production mode as the actual value is determined
 * by the backend.
 */
export const experimentalFeatureFlags = ['CONFIGURE_PROFILES'];
