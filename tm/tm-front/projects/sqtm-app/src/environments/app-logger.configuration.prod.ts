import { LoggingConfiguration } from 'sqtm-core';

export const appLoggerConfiguration: LoggingConfiguration = {
  loggers: {
    namespaces: {
      ['sqtm-core']: 'error',
      ['sqtm-app']: 'error',
    },
  },
};
