import { experimentalFeatureFlags } from './experimental-feature-flags';

// Use this if you need to add environment variables for tests environment.

export const environment = {
  production: false,
  sqtmExperimentalFeatureFlags: experimentalFeatureFlags,
};
