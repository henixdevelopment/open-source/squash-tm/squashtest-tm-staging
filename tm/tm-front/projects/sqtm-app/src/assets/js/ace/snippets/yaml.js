ace.define("ace/snippets/yaml",["require","exports","module"],function(require, exports, module){
  exports.snippetText="",
  exports.scope="yaml"
});                (function() {
  ace.require(["ace/snippets/yaml"], function(m) {
    if (typeof module == "object" && typeof exports == "object" && module) {
      module.exports = m;
    }
  });
})();
