module.exports = {
  "root": false,
  "extends": [
    "../../.eslintrc.js"
  ],
  "rules": {
    "import/no-restricted-paths": ["error",
      {
        "zones": [{
          "target": ["./projects/sqtm-app/**"],
          "from": "./projects/sqtm-core/**",
          "message": "You should import from 'sqtm-core' instead of using relative paths."
        }]
      },
    ],
  }
}
