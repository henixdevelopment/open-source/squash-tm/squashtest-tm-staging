module.exports = {
  "root": false,
  "extends": [
    "../../.eslintrc.js"
  ],
  "rules": {
    "no-restricted-imports": ["error", {
      "paths": [
        {
          "name": "sqtm-core",
          "message": "sqtm-core cannot be imported into itself. Use relative paths instead."
        },
      ],
      "patterns": [
        {
          "group": ["rxjs/internal/*", "rxjs/Rx"],
        },
        {
          "group": ["*/sqtm-core/*"],
          "message": "sqtm-core cannot be imported into itself. Use relative paths instead."
        },
        {
          "group": ["*/cypress/*"],
          "message": "sqtm-core cannot import from Cypress sources."
        }
      ],
    }],
    "import/no-restricted-paths": ["error",
      {
        "zones": [{
          "target": ["./projects/sqtm-core/src/lib/model/**"],
          "from": ".",
          "except": ["./projects/sqtm-core/src/lib/model"],
          "message": "sqtm-core models should be self-contained."
        }]
      },
    ],
  }
}
