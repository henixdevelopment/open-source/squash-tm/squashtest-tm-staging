import { TestBed } from '@angular/core/testing';

import { TestCaseStatisticsService } from './test-case-statistics.service';
import { TestingUtilsModule } from '../../../ui/testing-utils/testing-utils.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('TestCaseStatisticsService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, HttpClientTestingModule],
    }),
  );

  it('should be created', () => {
    const service: TestCaseStatisticsService = TestBed.inject(TestCaseStatisticsService);
    expect(service).toBeTruthy();
  });
});
