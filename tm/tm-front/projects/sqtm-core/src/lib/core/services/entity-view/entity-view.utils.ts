import { EntityViewState, SqtmEntityState } from './entity-view.state';

export function updateEntityInViewState<S extends SqtmEntityState, T extends string>(
  entity: S,
  state: EntityViewState<S, T>,
): EntityViewState<S, T> {
  const nextState: EntityViewState<S, T> = { ...state };
  const update: any = {};
  update[state.type] = { ...entity };
  Object.assign(nextState, update);
  return nextState;
}

function capitalize(camelCase: string) {
  return camelCase.charAt(0).toUpperCase() + camelCase.substring(1);
}

export function getEntityRefAsString(entityId: number, entityClassName: string): string {
  return capitalize(entityClassName) + '-' + entityId;
}
