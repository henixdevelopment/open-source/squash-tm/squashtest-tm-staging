import { Injectable } from '@angular/core';
import { AttachmentService } from '../attachment.service';
import { Observable } from 'rxjs';
import { ReferentialDataService } from '../../referential/services/referential-data.service';
import { TranslateService } from '@ngx-translate/core';
import { AdminReferentialDataService } from '../../referential/services/admin-referential-data.service';
import { EntityViewAttachmentHelperService } from '../entity-view/entity-view-attachment-helper.service';
import { GlobalConfigurationState } from '../../referential/state/global-configuration.state';
import { ActionErrorDisplayService } from '../errors-handling/action-error-display.service';

@Injectable({
  providedIn: 'root',
})
export class AdminViewAttachmentHelperService extends EntityViewAttachmentHelperService {
  constructor(
    attachmentService: AttachmentService,
    referentialDataService: ReferentialDataService,
    private adminReferentialDataService: AdminReferentialDataService,
    translateService: TranslateService,
    actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    super(attachmentService, referentialDataService, translateService, actionErrorDisplayService);
  }

  protected getGlobalConfiguration(): Observable<GlobalConfigurationState> {
    return this.adminReferentialDataService.globalConfiguration$;
  }
}
