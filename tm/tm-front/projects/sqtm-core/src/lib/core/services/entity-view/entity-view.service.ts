import { StoreOptions } from '../../store/store';
import { ReferentialDataService } from '../../referential/services/referential-data.service';
import { AttachmentService } from '../attachment.service';
import { Observable } from 'rxjs';
import {
  AttachmentListState,
  AttachmentState,
  customFieldValuesEntityAdapter,
  CustomFieldValueState,
  EntityViewComponentData,
  EntityViewState,
  PartialEntityViewComponentData,
  SqtmEntityState,
} from './entity-view.state';
import { RestService } from '../rest.service';
import decamelize from 'decamelize';
import { concatMap, filter, map, switchMap, take, tap, withLatestFrom } from 'rxjs/operators';
import { ProjectData } from '../../../model/project/project-data.model';
import { AttachmentModel } from '../../../model/attachment/attachment.model';
import { TranslateService } from '@ngx-translate/core';
import { SimplePermissions } from '../../../model/permissions/simple-permissions';
import {
  CustomFieldValue,
  CustomFieldValueModel,
} from '../../../model/customfield/custom-field-value.model';
import { ReferentialDataState } from '../../referential/state/referential-data.state';
import { InputType } from '../../../model/customfield/input-type.model';
import { CustomFieldValueService } from '../custom-field-value.service';
import { EntityViewAttachmentHelperService } from './entity-view-attachment-helper.service';
import { EntityViewCustomFieldHelperService } from './entity-view-custom-field-helper.service';
import { AttachmentListModel } from '../../../model/attachment/attachment-list.model';
import { GenericEntityViewService } from '../genric-entity-view/generic-entity-view.service';
import { doMilestonesAllowModification } from '../../../model/milestone/milestone.model';
import { AuthenticatedUser } from '../../../model/user/authenticated-user.model';

/**
 * Base class providing common functionality to all entity view in SquashTM 2+. Must be extended and typed for each view.
 * For generics types :
 * - E is the entity type handled by the entity view aka TestCase. It must extend EntityModel.
 * - T is the key inside state object aka 'testCase'. Generally the class name in camelCase. Typescript doesn't have reflexion functionality
 * that could allow us to infer the name by type...
 *
 * Note on URL used by function to exchange data with the server:
 *  - All URL are bases on type attribute of EntityViewModel, which in turn is constrained by the type declaration (K in generic signatures)
 *  Ex: testCase
 *  - The view is always loaded by a get request at decamelise(type) + -view. Ex: testCase -> test-case-view
 */
export abstract class EntityViewService<
  S extends SqtmEntityState,
  T extends string,
  P extends SimplePermissions,
> extends GenericEntityViewService<S, T> {
  public componentData$: Observable<EntityViewComponentData<S, T, P>>;

  // protected attachmentEntityAdapter = createEntityAdapter<Attachment>();
  // protected customFieldValuesEntityAdapter = createEntityAdapter<CustomFieldValue>();

  protected constructor(
    protected restService: RestService,
    protected referentialDataService: ReferentialDataService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected customFieldValueService: CustomFieldValueService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
    protected customFieldHelper: EntityViewCustomFieldHelperService,
    storeOptions?: StoreOptions,
  ) {
    super(restService, attachmentService, translateService, attachmentHelper, storeOptions);
  }

  initializeComponentData(): void {
    this.componentData$ = this.store.state$.pipe(
      filter((state) => Boolean(this.getEntity(state)) && Boolean(this.getEntity(state).id)),
      switchMap((state: EntityViewState<S, T>) =>
        this.referentialDataService.connectToProjectData(this.getEntity(state).projectId).pipe(
          map((projectData: ProjectData) => {
            const permissions: P = this.addSimplePermissions(projectData);
            const milestones = this.getEntity(state).milestones;
            const milestonesAllowModification = doMilestonesAllowModification(milestones);
            const stateStatusAllowModification = this.getEntity(state).statusAllowModification;
            const statusAllowModification = this.statusAllowModification(
              stateStatusAllowModification,
            );
            const componentData: PartialEntityViewComponentData<S, T, P> = {
              ...state,
              type: state.type,
              projectData,
              permissions: permissions,
              milestonesAllowModification,
              statusAllowModification: statusAllowModification,
            };
            return componentData;
          }),
        ),
      ),
      switchMap((partialCompData) =>
        this.referentialDataService.globalConfiguration$.pipe(
          map((globalConfiguration) => {
            const componentData: EntityViewComponentData<S, T, P> = {
              ...partialCompData,
              globalConfiguration,
            };
            return componentData;
          }),
        ),
      ),
    );
  }

  updateCustomFieldValue(cfvId: number, value: string | string[]): Observable<unknown> {
    return this.store.state$.pipe(
      take(1),
      concatMap((state: EntityViewState<S, T>) => {
        const entity = { ...this.getEntity(state) };
        const customFieldValue = entity.customFieldValues.entities[cfvId];
        return this.customFieldHelper.writeCustomFieldValue(customFieldValue, value);
      }),
      // we must re-fetch state AFTER the async http call is done to have the last version of state in date,
      // and not the version that was actual when request was sent.
      withLatestFrom(this.store.state$),
      map(([newValue, currentState]: [string | string[], EntityViewState<S, T>]) => {
        const entity = { ...this.getEntity(currentState) };
        const customFieldValues = this.customFieldHelper.updateCustomFieldValue(
          cfvId,
          newValue,
          entity.customFieldValues,
        );
        return this.updateEntity({ ...entity, customFieldValues }, currentState);
      }),
      tap((newState: EntityViewState<S, T>) => this.commit(newState)),
    );
  }

  protected getRootUrl(initialState): string {
    return `${decamelize(initialState.type, '-')}`;
  }

  protected initializeAttachmentListState(
    attachmentListModel: AttachmentListModel,
  ): AttachmentListState {
    const attachmentState = this.attachmentHelper.initializeAttachmentState(
      attachmentListModel.attachments,
    );
    return { id: attachmentListModel.id, attachments: attachmentState };
  }

  protected initializeAttachmentState(attachmentModels: AttachmentModel[]): AttachmentState {
    return this.attachmentHelper.initializeAttachmentState(attachmentModels);
  }

  protected initializeCustomFieldValueState(
    customFieldValueModels: CustomFieldValueModel[],
  ): CustomFieldValueState {
    return this.customFieldHelper.initializeCustomFieldValueState(customFieldValueModels);
  }

  protected buildCfvState(
    customFieldValueModels: CustomFieldValueModel[],
    refData: ReferentialDataState,
  ) {
    const customFieldValues = customFieldValueModels.map((cfvModel) => {
      const cfv: CustomFieldValue = { ...cfvModel };
      const customField = refData.customFieldState.entities[cfv.cufId];
      if (customField && customField.inputType === InputType.TAG) {
        cfv.value = cfvModel.value.split('|');
      }
      return cfv;
    });
    const initialState = customFieldValuesEntityAdapter.getInitialState();
    return customFieldValuesEntityAdapter.setAll(customFieldValues, initialState);
  }

  private statusAllowModification(allowModification: boolean) {
    if (allowModification !== undefined) {
      return allowModification;
    } else {
      return true;
    }
  }

  abstract addSimplePermissions(projectData: ProjectData): P;

  protected getCurrentUser(): Observable<AuthenticatedUser> {
    return this.referentialDataService.authenticatedUser$;
  }

  updateEntityPath() {
    const headerRootUrl = 'entity-path-header';
    this.state$
      .pipe(
        take(1),
        switchMap((state) => {
          const entity = { ...this.getEntity(state) };
          return this.restService
            .get<{ path: string[] }>([headerRootUrl, this.rootUrl, entity.id.toString()])
            .pipe(
              map(({ path }) => path),
              map((path) => ({ path, entity, state })),
            );
        }),
        map(({ path, entity, state }) => ({
          ...state,
          [this.entityType]: { ...entity, path },
        })),
        tap((newState) => this.store.commit(newState)),
      )
      .subscribe();
  }
}
