import { TestBed } from '@angular/core/testing';

import { PartyPreferencesService } from './party-preferences.service';
import { TestingUtilsModule } from '../../../ui/testing-utils/testing-utils.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('PartyPreferencesService', () => {
  let service: PartyPreferencesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, HttpClientTestingModule],
    });
    service = TestBed.inject(PartyPreferencesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
