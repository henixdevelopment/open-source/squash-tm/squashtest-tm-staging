import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Identifier } from '../../../model/entity.model';
import { TestPlanResumeModel } from '../../../model/execution/test-plan-resume.model';
import { RestService } from '../rest.service';
import { FilterValueModel } from '../../../model/filter/filter-value.model';
import { StatisticsBundle } from '../../../model/campaign/campaign-model';

@Injectable({
  providedIn: 'root',
})
export class IterationService {
  constructor(private restService: RestService) {}

  addTestCase(
    testCaseIds: number[],
    iterationId: number,
  ): Observable<AddTestCaseToIterationResponse> {
    const url = `iteration/${iterationId}/test-plan-items`;
    return this.restService.post<AddTestCaseToIterationResponse>([url], { testCaseIds });
  }

  persistManualExecution(iterationId: number, testPlanItemId: number): Observable<number> {
    const url = `iteration/${iterationId}/test-plan/${testPlanItemId}/executions/new-manual`;
    return this.restService
      .post<{ executionId: number }>([url], {})
      .pipe(map((responseBody) => responseBody.executionId));
  }

  updateScheduledStartDate(iterationId: number, scheduledStartDate: Date): Observable<any> {
    return this.restService.post(['iteration', iterationId.toString(), 'scheduled-start-date'], {
      scheduledStartDate: scheduledStartDate,
    });
  }

  updateScheduledEndDate(iterationId: number, scheduledEndDate: Date): Observable<any> {
    return this.restService.post(['iteration', iterationId.toString(), 'scheduled-end-date'], {
      scheduledEndDate: scheduledEndDate,
    });
  }

  updateActualStartDate(iterationId: number, actualStartDate: Date): Observable<any> {
    return this.restService.post(['iteration', iterationId.toString(), 'actual-start-date'], {
      actualStartDate: actualStartDate,
    });
  }

  updateActualEndDate(iterationId: number, actualEndDate: Date): Observable<any> {
    return this.restService.post(['iteration', iterationId.toString(), 'actual-end-date'], {
      actualEndDate: actualEndDate,
    });
  }

  updateActualStartAuto(iterationId: number, actualStartAuto: boolean): Observable<any> {
    return this.restService.post(['iteration', iterationId.toString(), 'actual-start-auto'], {
      actualStartAuto: actualStartAuto,
    });
  }

  updateActualEndAuto(iterationId: number, actualEndAuto: boolean): Observable<any> {
    return this.restService.post(['iteration', iterationId.toString(), 'actual-end-auto'], {
      actualEndAuto: actualEndAuto,
    });
  }

  changeItemsPosition(
    iterationId: number,
    itemsToMove: Identifier[],
    position: number,
  ): Observable<void> {
    return this.restService.post([
      'iteration',
      iterationId.toString(),
      'test-plan',
      itemsToMove.join(','),
      'position',
      position.toString(),
    ]);
  }

  getIterationStatistics(
    iterationId: number,
    lastExecutionScope: boolean,
  ): Observable<StatisticsBundle> {
    return this.restService.post(['iteration-view', iterationId.toString(), 'statistics'], {
      lastExecutionScope,
    });
  }

  resume(iterationId: number): Observable<TestPlanResumeModel> {
    return this.restService.post<TestPlanResumeModel>([
      'iteration',
      iterationId.toString(),
      'test-plan',
      'resume',
    ]);
  }

  resumeFiltered(
    iterationId: number,
    filterValues: FilterValueModel[],
  ): Observable<TestPlanResumeModel> {
    return this.restService.post<TestPlanResumeModel>(
      ['iteration', iterationId.toString(), 'test-plan', 'resume-filtered-selection'],
      { filterValues },
    );
  }

  relaunch(iterationId: number): Observable<TestPlanResumeModel> {
    return this.restService.post<TestPlanResumeModel>([
      'iteration',
      iterationId.toString(),
      'test-plan',
      'relaunch',
    ]);
  }

  relaunchFiltered(
    iterationId: number,
    filterValues: FilterValueModel[],
  ): Observable<TestPlanResumeModel> {
    return this.restService.post<TestPlanResumeModel>(
      ['iteration', iterationId.toString(), 'test-plan', 'relaunch-filtered-selection'],
      { filterValues },
    );
  }

  getNextExecution(iterationId: number, testPlanItemId: number): Observable<TestPlanResumeModel> {
    return this.restService.post<TestPlanResumeModel>([
      'iteration',
      iterationId.toString(),
      'test-plan',
      testPlanItemId.toString(),
      'next-execution',
    ]);
  }

  getNextExecutionOfPartial(
    iterationId: number,
    testPlanItemId: number,
    partialTestPlanItemIds: number[],
  ): Observable<TestPlanResumeModel> {
    return this.restService.post<TestPlanResumeModel>(
      [
        'iteration',
        iterationId.toString(),
        'test-plan',
        testPlanItemId.toString(),
        'next-execution-filtered-selection',
      ],
      { ids: partialTestPlanItemIds },
    );
  }

  deleteTestPlanItemsFromIteration(iterationId: number, itemsToDetach: number[]): Observable<void> {
    return this.restService.delete([
      'iteration',
      iterationId.toString(),
      'test-plan',
      itemsToDetach.toString(),
    ]);
  }
}

export interface AddTestCaseToIterationResponse {
  itemTestPlanIds: number[];
  hasDataSet: boolean;
}
