import {
  AVAILABLE_EXPERIMENTAL_FEATURE_FLAGS,
  SQTM_AVAILABLE_EXPERIMENTAL_FEATURE_FLAGS_TOKEN,
  SQTM_ENABLED_EXPERIMENTAL_FEATURE_FLAGS_TOKEN,
} from './experimental-feature.constants';

export function provideExperimentalFeatureFlags(enabledFeatureFlags: string[]) {
  return [
    {
      provide: SQTM_ENABLED_EXPERIMENTAL_FEATURE_FLAGS_TOKEN,
      useValue: enabledFeatureFlags,
    },
    {
      provide: SQTM_AVAILABLE_EXPERIMENTAL_FEATURE_FLAGS_TOKEN,
      useValue: AVAILABLE_EXPERIMENTAL_FEATURE_FLAGS,
    },
  ];
}
