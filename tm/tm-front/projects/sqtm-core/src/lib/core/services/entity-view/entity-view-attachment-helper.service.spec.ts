import { TestBed, waitForAsync } from '@angular/core/testing';
import { EntityViewAttachmentHelperService } from './entity-view-attachment-helper.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';
import { AttachmentService } from '../attachment.service';
import { getTestingFile } from './entity-view.service.spec';
import { cold } from 'jasmine-marbles';
import { UploadSummary } from '../../../model/attachment/upload-summary.model';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { ReferentialDataService } from '../../referential/services/referential-data.service';
import { toArray } from 'rxjs/operators';
import {
  StartUpload,
  UploadAttachmentEvent,
  UploadComplete,
  UploadProgress,
} from './upload-attachment.event';
import {
  PersistedAttachment,
  RejectedAttachment,
  UploadingAttachment,
} from '../../../model/attachment/attachment.model';
import { ProjectData } from '../../../model/project/project-data.model';
import { NO_PERMISSIONS } from '../../../model/permissions/permissions.model';
import { TestingUtilsModule } from '../../../ui/testing-utils/testing-utils.module';
import { GlobalConfigurationState } from '../../referential/state/global-configuration.state';

function getGlobalConf(): GlobalConfigurationState {
  return {
    milestoneFeatureEnabled: false,
    uploadFileExtensionWhitelist: ['txt'],
    uploadFileSizeLimit: 10000,
    bannerMessage: null,
    searchActivationFeatureEnabled: true,
    unsafeAttachmentPreviewEnabled: false,
  };
}

export function createRejectedAttachment(): RejectedAttachment {
  return {
    id: 'rejected',
    errors: ['error-msg'],
    addedOn: new Date(),
    lastModifiedOn: null,
    kind: 'rejected-attachment',
    name: 'toto.log',
    size: 1000,
  };
}

export function createUploadingAttachment(): UploadingAttachment {
  return {
    id: 'uploading',
    kind: 'uploading-attachment',
    addedOn: new Date(),
    lastModifiedOn: null,
    name: 'toto.txt',
    size: 1000,
    uploadProgress: 0,
  };
}

export function createPersistedAttachment(): PersistedAttachment {
  return {
    id: '3',
    kind: 'persisted-attachment',
    addedOn: new Date(),
    lastModifiedOn: null,
    name: 'toto.txt',
    size: 1000,
    pendingDelete: false,
  };
}

describe('EntityViewAttachmentHelperService', () => {
  const attachmentServiceMock = jasmine.createSpyObj('attachmentService', [
    'uploadAttachments',
    'deleteAttachments',
  ]);
  const referentialDataServiceMock = jasmine.createSpyObj('refDataService', [
    'connectToProjectData',
  ]);
  referentialDataServiceMock.globalConfiguration$ = of(getGlobalConf());

  const projectData: ProjectData = {
    id: 1,
    allowAutomationWorkflow: false,
    customFieldBinding: null,
    label: 'project1',
    name: 'project1',
    requirementCategory: null,
    testCaseNature: null,
    testCaseType: null,
    uri: '',
    permissions: NO_PERMISSIONS,
    bugTracker: null,
    milestones: [],
    taServer: null,
    automationWorkflowType: 'NATIVE',
    disabledExecutionStatus: [],
    keywords: [],
    bddScriptLanguage: 'ENGLISH',
    allowTcModifDuringExec: false,
    activatedPlugins: null,
    scmRepositoryId: null,
  };

  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, HttpClientTestingModule, TranslateModule.forRoot()],
      providers: [
        { provide: AttachmentService, useValue: attachmentServiceMock },
        { provide: ReferentialDataService, useValue: referentialDataServiceMock },
      ],
    }),
  );

  it('should emit correct event stream for an http request', waitForAsync(() => {
    const service: EntityViewAttachmentHelperService = TestBed.inject(
      EntityViewAttachmentHelperService,
    );
    referentialDataServiceMock.connectToProjectData.and.returnValue(of(projectData));
    const start = {
      type: HttpEventType.Sent as any,
    };
    const up1 = {
      type: HttpEventType.UploadProgress as any,
      total: 500,
      loaded: 100,
    };
    const up2 = {
      type: HttpEventType.UploadProgress as any,
      total: 500,
      loaded: 300,
    };
    const up3 = {
      type: HttpEventType.UploadProgress as any,
      total: 500,
      loaded: 450,
    };
    const resp = new HttpResponse<UploadSummary[]>({
      body: [
        {
          attachmentDto: {
            id: 45,
            addedOn: new Date(),
            lastModifiedOn: null,
            name: 'toto.txt',
            size: 500,
          },
          iStatus: 0,
          status: '',
        },
      ],
    });
    const serviceObservable = cold('-s--a---b--c--r|', {
      s: start,
      a: up1,
      b: up2,
      c: up3,
      r: resp,
    });
    attachmentServiceMock.uploadAttachments.and.returnValue(serviceObservable);

    const testingFile = getTestingFile('toto.txt', 'txt');
    const result = service.addAttachments([testingFile], 1, null, null, null);
    result.pipe(toArray()).subscribe((events: UploadAttachmentEvent[]) => {
      expect(events.length).toEqual(5);
      const startEvent = events[0] as StartUpload;
      expect(startEvent.kind).toEqual('start');
      const attachments = startEvent.attachments;
      expect(attachments.length).toEqual(1);
      const attachment = attachments[0] as UploadingAttachment;
      expect(attachment.name).toEqual('toto.txt');
      expect(attachment.uploadProgress).toEqual(0);
      const upload1 = events[1] as UploadProgress;
      expect(upload1.kind).toEqual('progress');
      expect(upload1.attachments.length).toEqual(1);
      expect(upload1.attachments[0]['uploadProgress']).toEqual(20);
      const upload2 = events[2] as UploadProgress;
      expect(upload2.kind).toEqual('progress');
      expect(upload2.attachments.length).toEqual(1);
      expect(upload2.attachments[0]['uploadProgress']).toEqual(60);
      const upload3 = events[3] as UploadProgress;
      expect(upload3.kind).toEqual('progress');
      expect(upload3.attachments.length).toEqual(1);
      expect(upload3.attachments[0]['uploadProgress']).toEqual(90);
      const upload4 = events[4] as UploadComplete;
      expect(upload4.kind).toEqual('complete');
      expect(upload4.attachments.length).toEqual(1);
      const persistedAttachment = upload4.attachments[0];
      expect(persistedAttachment.id).toEqual('45');
      expect(upload4.uploadingAttachmentsToDelete).toEqual([attachment.id]);
    });
  }));

  it('should validate various files', () => {
    const valid = getTestingFile('toto.txt', 'txt');
    const tooBig = getTestingFile('tooBig.txt', 'txt', 50000);
    const badExtension = getTestingFile('toto.lol', 'lol');
    const noExtension = getTestingFile('toto', '');
    const upperCaseExtension = getTestingFile('toto.TXT', '');
    const files = [valid, tooBig, badExtension, noExtension, upperCaseExtension];

    const service: EntityViewAttachmentHelperService = TestBed.inject(
      EntityViewAttachmentHelperService,
    );
    const fileCandidates = service.validateFiles(files, getGlobalConf());
    expect(fileCandidates.length).toEqual(5);
    const invalidCandidates = fileCandidates.filter((candidate) => candidate.isInvalid());
    expect(invalidCandidates.length).toEqual(3);
    const validCandidates = fileCandidates.filter((candidate) => candidate.isValid());
    expect(validCandidates.length).toEqual(2);
  });

  describe('Map Attachments To State', () => {
    it('should add a new attachment when upload start', () => {
      const validAttachment = createUploadingAttachment();
      const rejectedAttachment = createRejectedAttachment();
      const start: StartUpload = new StartUpload([rejectedAttachment, validAttachment]);
      const service: EntityViewAttachmentHelperService = TestBed.inject(
        EntityViewAttachmentHelperService,
      );
      const state = service.mapUploadEventToState({ ids: [], entities: {} }, start);
      expect(state.ids.length).toEqual(2);
    });

    it('should update when upload progress', () => {
      const uploadingAttachment = createUploadingAttachment();
      const service: EntityViewAttachmentHelperService = TestBed.inject(
        EntityViewAttachmentHelperService,
      );
      const id = uploadingAttachment.id;
      const progress: UploadProgress = new UploadProgress([{ id: id, uploadProgress: 20 }]);
      const state = service.mapUploadEventToState(
        { ids: [id], entities: { [id]: uploadingAttachment } },
        progress,
      );
      expect(state.ids.length).toEqual(1);
      const entity = state.entities[id] as UploadingAttachment;
      expect(entity.uploadProgress).toEqual(20);
    });

    it('should update when upload complete', () => {
      const validAttachment = createUploadingAttachment();
      const id = validAttachment.id;
      const persistedAttachment = createPersistedAttachment();
      const complete: UploadComplete = new UploadComplete(
        [persistedAttachment],
        [validAttachment.id],
      );
      const service: EntityViewAttachmentHelperService = TestBed.inject(
        EntityViewAttachmentHelperService,
      );
      const state = service.mapUploadEventToState(
        { ids: [id], entities: { [id]: validAttachment } },
        complete,
      );
      expect(state.ids).toEqual([persistedAttachment.id]);
    });
  });
});
