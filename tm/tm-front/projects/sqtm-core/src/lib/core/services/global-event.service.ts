import { Inject, Injectable, NgZone } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { BehaviorSubject, fromEvent, Observable } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

/** @dynamic */
@Injectable({
  providedIn: 'root',
})
export class GlobalEventService {
  private _ctrlSubject = new BehaviorSubject<boolean>(false);
  private _shiftSubject = new BehaviorSubject<boolean>(false);

  controlKey$: Observable<boolean> = this._ctrlSubject.asObservable().pipe(distinctUntilChanged());

  shiftKey$: Observable<boolean> = this._shiftSubject.asObservable().pipe(distinctUntilChanged());

  constructor(
    private ngZone: NgZone,
    @Inject(DOCUMENT) private document: Document,
  ) {
    this.ngZone.runOutsideAngular(() => {
      fromEvent(document, 'keydown').subscribe((event: KeyboardEvent) => {
        this._shiftSubject.next(event.shiftKey);
        this._ctrlSubject.next(event.ctrlKey);
      });

      fromEvent(document, 'keyup').subscribe((_event: KeyboardEvent) => {
        this._shiftSubject.next(false);
        this._ctrlSubject.next(false);
      });
    });
  }
}
