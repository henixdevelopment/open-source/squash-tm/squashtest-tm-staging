import { Injectable } from '@angular/core';
import { customFieldValuesEntityAdapter, CustomFieldValueState } from './entity-view.state';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CustomFieldValueService } from '../custom-field-value.service';
import { ReferentialDataService } from '../../referential/services/referential-data.service';
import {
  CustomFieldValue,
  CustomFieldValueModel,
} from '../../../model/customfield/custom-field-value.model';

@Injectable({
  providedIn: 'root',
})
export class EntityViewCustomFieldHelperService {
  constructor(
    private customFieldValueService: CustomFieldValueService,
    private referentialDataService: ReferentialDataService,
  ) {}

  public initializeCustomFieldValueState(
    customFieldValueModels: CustomFieldValueModel[],
  ): CustomFieldValueState {
    const initialState = customFieldValuesEntityAdapter.getInitialState();
    if (customFieldValueModels == null) {
      return initialState;
    }

    const customFieldValues = customFieldValueModels.map((cfvModel) => {
      const cfv: CustomFieldValue = { ...cfvModel };
      if (cfv.fieldType === 'TAG') {
        if (cfvModel.value != null) {
          if (cfvModel.value.length === 0) {
            cfv.value = null;
          } else {
            cfv.value = cfvModel.value.split('|');
          }
        }
      }
      return cfv;
    });

    return customFieldValuesEntityAdapter.setAll(customFieldValues, initialState);
  }

  /**
   * Update server side a new value for an existing CustomFieldValue
   * @param cfv :the custom field value
   * @param value:a value as string|string[];
   */
  public writeCustomFieldValue(
    cfv: CustomFieldValue,
    value: string | string[],
  ): Observable<string | string[]> {
    return this.customFieldValueService.postCustomFieldValue(cfv.id, value).pipe(map(() => value));
  }

  public updateCustomFieldValue(
    cfvId: number,
    formattedValue: string | string[],
    state: CustomFieldValueState,
  ): CustomFieldValueState {
    return customFieldValuesEntityAdapter.updateOne(
      {
        id: cfvId,
        changes: { value: formattedValue },
      },
      state,
    );
  }
}
