import { TestBed } from '@angular/core/testing';

import { AttachmentService } from './attachment.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestingUtilsModule } from '../../ui/testing-utils/testing-utils.module';

describe('AttachmentService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TestingUtilsModule],
    }),
  );

  it('should be created', () => {
    const service: AttachmentService = TestBed.inject(AttachmentService);
    expect(service).toBeTruthy();
  });
});
