import { TestBed } from '@angular/core/testing';

import { EntityPathHeaderService } from './entity-path-header.service';

describe('EntityPathHeaderService', () => {
  let service: EntityPathHeaderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EntityPathHeaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
