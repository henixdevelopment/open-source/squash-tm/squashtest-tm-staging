import { Injectable } from '@angular/core';
import { DialogService } from '../../../ui/dialog/services/dialog.service';
import { Observable, of, throwError } from 'rxjs';
import { concatMap, tap } from 'rxjs/operators';
import {
  doesHttpErrorContainsSquashActionError,
  doesHttpErrorContainsSquashFieldError,
  extractSquashActionError,
  extractSquashFirstFieldError,
} from '../../utils/http-error-utils';
import { coreLogger } from '../../core.logger';
import { AlertConfiguration } from '../../../ui/dialog/components/dialog-templates/alert-dialog/alert-configuration';
import { DialogReference } from '../../../ui/dialog/model/dialog-reference';

const logger = coreLogger.compose('ActionErrorDisplayService');

@Injectable({
  providedIn: 'root',
})
export class ActionErrorDisplayService {
  constructor(private dialogService: DialogService) {}

  handleActionError(error: any): Observable<any> {
    return of(error).pipe(
      tap((err) => this.showErrorIfExist(err)),
      // rethrow in all cases so error will interrupt observables chains and log in console
      concatMap((err) => throwError(() => err)),
    );
  }

  private showErrorIfExist(error: any) {
    logger.debug('Receiving http request error from server. Will try to extract handled errors.', [
      error,
    ]);
    if (doesHttpErrorContainsSquashActionError(error)) {
      this.showActionError(error);
    } else if (doesHttpErrorContainsSquashFieldError(error)) {
      this.showFirstFieldError(error);
    }
  }

  private showFirstFieldError(error: any) {
    logger.debug('Error seems to contains field errors. Try to display the first one.');
    const squashError = extractSquashFirstFieldError(error);
    const fieldValidationErrors = squashError.fieldValidationErrors;
    if (fieldValidationErrors.length > 0) {
      const fieldValidationError = fieldValidationErrors[0];
      const dialogConfiguration = {
        messageKey: fieldValidationError.i18nKey,
        translateParameters: fieldValidationError.messageArgs,
      };
      this.dialogService.openAlert(dialogConfiguration);
    }
  }

  showActionError(error: any): DialogReference<AlertConfiguration> {
    logger.debug('Error seems to contains action error. Try to display message.');
    const squashError = extractSquashActionError(error);
    const actionValidationError = squashError.actionValidationError;
    const dialogConfiguration: Partial<AlertConfiguration> = {
      messageKey: actionValidationError.i18nKey,
      translateParameters: actionValidationError.i18nParams,
    };
    return this.dialogService.openAlert(dialogConfiguration);
  }
}
