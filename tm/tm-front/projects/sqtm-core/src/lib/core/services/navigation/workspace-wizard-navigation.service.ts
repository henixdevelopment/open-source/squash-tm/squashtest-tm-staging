import { Injectable } from '@angular/core';
import { SquashPlatformNavigationService } from './squash-platform-navigation.service';
import { GridService } from '../../../ui/grid/services/grid.service';
import { ReferentialDataService } from '../../referential/services/referential-data.service';
import {
  ProjectData,
  ProjectDataMap,
  WorkspaceTypeForPlugins,
} from '../../../model/project/project-data.model';
import {
  WizardMenu,
  WorkspaceWizard,
} from '../../../model/workspace-wizard/workspace-wizard.model';
import { combineLatest, Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { DataRow } from '../../../ui/grid/model/data-row.model';
import { checkAccessRulesForGridSelection } from '../../../model/workspace-wizard/access-rule.utils';
import { UrlBuilder } from '../../utils/url-builder';
import { ParamMap } from '@angular/router';
import { EntityRowReference, toEntityRowReference } from '../../../ui/grid/model/data-row.type';

@Injectable()
export class WorkspaceWizardNavigationService {
  static ENTITY_ROW_REFERENCES = 'rows';

  private unsub$ = new Subject<void>();

  constructor(
    private squashPlatformNavigationService: SquashPlatformNavigationService,
    private tree: GridService,
    private referentialDataService: ReferentialDataService,
  ) {}

  initializeForWorkspace(
    workspaceName: WorkspaceTypeForPlugins,
  ): Observable<WorkspaceWizardMenuItem[]> {
    return combineLatest([
      this.referentialDataService.referentialData$,
      this.tree.selectedRows$,
      this.referentialDataService.projectDatas$,
    ]).pipe(
      takeUntil(this.unsub$),
      map(([referentialData, selectedRows, projectDataMap]) => {
        return this.getWorkspaceWizardMenuItems(
          referentialData.workspaceWizards,
          selectedRows,
          workspaceName,
          projectDataMap,
        ).sort((a, b) => a.wizardMenu.label.localeCompare(b.wizardMenu.label));
      }),
    );
  }

  getWorkspaceWizardMenuItems(
    wizards: WorkspaceWizard[],
    selectedRows: DataRow[],
    workspaceName: WorkspaceTypeForPlugins,
    projectDataMap: ProjectDataMap,
  ): WorkspaceWizardMenuItem[] {
    return wizards
      .filter((wizard) => wizard.displayWorkspace === workspaceName && wizard.wizardMenu != null)
      .map((wizard) => {
        const accessRule = wizard.wizardMenu.accessRule;
        const passesAccessRules = checkAccessRulesForGridSelection(accessRule, selectedRows);
        const isPluginActivatedForRows = selectedRows.every((row) =>
          this.isPluginEnabledOnProject(projectDataMap[row.projectId], wizard.id, workspaceName),
        );
        const enabled = isPluginActivatedForRows && passesAccessRules;
        return {
          wizardMenu: wizard.wizardMenu,
          enabled,
          generatedUrl: new UrlBuilder(wizard.wizardMenu.url)
            .withParamArray(
              WorkspaceWizardNavigationService.ENTITY_ROW_REFERENCES,
              selectedRows.map((row) => row.id),
            )
            .build(),
        };
      });
  }

  /**
   * Extracts a list of entity row refs from a ParamMap. Makes easier to parse URLs from a plugin.
   * Throws if the param could not be found or if any of the references fails to get deserialized.
   */
  static extractEntityRowReferencesFromParams(paramMap: ParamMap): EntityRowReference[] {
    if (paramMap.has(this.ENTITY_ROW_REFERENCES)) {
      const serializedRefs = paramMap.get(this.ENTITY_ROW_REFERENCES) ?? '';
      const refArray = serializedRefs.split(',');
      return refArray.map((ref) => toEntityRowReference(ref));
    }

    throw Error('Could not find entities in route parameters');
  }

  complete(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  isPluginEnabledOnProject(
    projectData: ProjectData,
    workspaceWizardId: string,
    workspaceName: WorkspaceTypeForPlugins,
  ): boolean {
    return projectData.activatedPlugins[workspaceName].includes(workspaceWizardId);
  }
}

export interface WorkspaceWizardMenuItem {
  // Extracted from the workspace wizard plugin
  wizardMenu: WizardMenu;
  enabled: boolean;

  // URL build from the wizardMenu's URL with added query params such as the current grid selection
  // e.g. `path/to/plugin?rows=TestCase-123,TestCaseFolder-42`
  generatedUrl: string;
}
