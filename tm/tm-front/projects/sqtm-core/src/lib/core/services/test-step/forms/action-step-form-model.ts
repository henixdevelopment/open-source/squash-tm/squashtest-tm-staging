import { RawValueMap } from '../../../../model/customfield/customfield.model';

export interface ActionStepFormModel {
  action: string;
  expectedResult: string;
  index: number;
  customFields: RawValueMap;
}
