import { TestBed } from '@angular/core/testing';

import { LocalPersistenceService } from './local-persistence.service';
import { concatMap, tap } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { AuthenticatedUser } from '../../model/user/authenticated-user.model';
import { UserProxyService } from './user-proxy.service';

const user1 = {
  username: 'toto',
  userId: 43,
  admin: false,
  firstName: '',
  lastName: '',
  hasAnyReadPermission: true,
  projectManager: false,
  milestoneManager: false,
  clearanceManager: false,
  functionalTester: true,
  automationProgrammer: true,
  canDeleteFromFront: true,
};
const user2: AuthenticatedUser = {
  username: 'titi',
  userId: 34,
  admin: false,
  firstName: '',
  lastName: '',
  hasAnyReadPermission: true,
  projectManager: true,
  milestoneManager: true,
  clearanceManager: true,
  functionalTester: true,
  automationProgrammer: true,
  canDeleteFromFront: true,
};
describe('LocalPersistenceService', () => {
  const userProxyService = {
    authenticatedUser$: new BehaviorSubject<AuthenticatedUser>(undefined),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: UserProxyService, useValue: userProxyService },
        LocalPersistenceService,
      ],
    });
    userProxyService.authenticatedUser$.next(user1);
  });

  it('should be created', () => {
    const service: LocalPersistenceService = TestBed.inject(LocalPersistenceService);
    expect(service).toBeTruthy();
  });

  it('should store and give back info for current user', (done) => {
    const service: LocalPersistenceService = TestBed.inject(LocalPersistenceService);
    service
      .clearAll()
      .pipe(
        concatMap(() => service.set('myKey', 'myValue')),
        concatMap(() => service.get('myKey')),
        tap((value) => {
          expect(value).toEqual('myValue');
        }),
        tap(() => userProxyService.authenticatedUser$.next({ ...user2 })),
        concatMap(() => service.get('myKey')),
        tap((value) => {
          expect(value).toBeNull();
        }),
        concatMap(() => service.set('myKey', 'valueForTiti')),
        concatMap(() => service.get('myKey')),
        tap((value) => {
          expect(value).toEqual('valueForTiti');
        }),
        tap(() => userProxyService.authenticatedUser$.next({ ...user1 })),
        concatMap(() => service.get('myKey')),
        tap((value) => {
          expect(value).toEqual('myValue');
        }),
      )
      .subscribe(() => {
        done();
      });
  });
});
