import { fakeAsync, TestBed, tick } from '@angular/core/testing';

import { SessionPingService } from './session-ping.service';
import { RestService } from '../rest.service';

describe('SessionPingService', () => {
  let service: SessionPingService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: RestService, useValue: jasmine.createSpyObj(['get']) }],
    });
    service = TestBed.inject(SessionPingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be created', fakeAsync(() => {
    spyOn(service as any, 'doPing');
    const subscription = service.initialize();

    tick(SessionPingService.PING_INTERVAL);
    expect(service['doPing']).toHaveBeenCalledTimes(0);

    service.beginLongTermEdition();
    service.beginLongTermEdition();

    tick(SessionPingService.PING_INTERVAL);
    expect(service['doPing']).toHaveBeenCalledTimes(1);

    service.endLongTermEdition();

    tick(SessionPingService.PING_INTERVAL);
    expect(service['doPing']).toHaveBeenCalledTimes(2);

    service.endLongTermEdition();

    tick(SessionPingService.PING_INTERVAL);
    expect(service['doPing']).toHaveBeenCalledTimes(2);

    subscription.unsubscribe();
  }));
});
