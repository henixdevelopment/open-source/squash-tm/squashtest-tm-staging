import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class InterWindowCommunicationService {
  private _messageQueue = new Subject<InterWindowMessages>();

  public interWindowMessages$: Observable<InterWindowMessages> = this._messageQueue.asObservable();

  private readonly squashTmMessageFunctionKey = 'squashTmMessage';

  constructor() {
    window[this.squashTmMessageFunctionKey] = (message: InterWindowMessages) => {
      this._messageQueue.next(message);
    };
  }

  isAbleToSendMessagesToOpener() {
    let able = false;
    if (window && window.opener) {
      const messageFunction = window.opener[this.squashTmMessageFunctionKey];
      if (messageFunction && typeof messageFunction === 'function') {
        able = true;
      }
    }
    return able;
  }

  sendMessage(message: InterWindowMessages) {
    if (this.isAbleToSendMessagesToOpener()) {
      const messageFunction = window.opener[this.squashTmMessageFunctionKey];
      messageFunction(message);
    }
  }

  isAbleToSendMessagesToParent() {
    let able = false;
    if (window && window.parent) {
      const messageFunction = window.parent[this.squashTmMessageFunctionKey];
      if (messageFunction && typeof messageFunction === 'function') {
        able = true;
      }
    }
    return able;
  }

  sendMessageToParent(message: InterWindowMessages) {
    if (this.isAbleToSendMessagesToParent()) {
      const messageFunction = window.parent[this.squashTmMessageFunctionKey];
      messageFunction(message);
    }
  }

  isAbleToSendMessagesToWindow(targetWindow: Window) {
    let able = false;
    if (window && targetWindow) {
      const messageFunction = targetWindow[this.squashTmMessageFunctionKey];
      if (messageFunction && typeof messageFunction === 'function') {
        able = true;
      }
    }
    return able;
  }

  sendMessageToWindow(message: InterWindowMessages, targetWindow: Window) {
    if (this.isAbleToSendMessagesToWindow(targetWindow)) {
      const messageFunction = targetWindow[this.squashTmMessageFunctionKey];
      messageFunction(message);
    }
  }
}

export class InterWindowMessages {
  constructor(
    private readonly type: InterWindowMessageTypes,
    public readonly payload: any = {},
  ) {}

  isTypeOf(type: InterWindowMessageTypes): boolean {
    return this.type === type;
  }
}

export type InterWindowMessageTypes =
  | 'EXECUTION-STEP-CHANGED'
  | 'MODIFICATION-DURING-EXECUTION'
  | 'MODIFICATION-DURING-EXECUTION-STEP'
  | 'REQUIRE-MODIFICATION-DURING-EXECUTION-PROLOGUE'
  | 'REQUIRE-MODIFICATION-DURING-EXECUTION-STEP'
  | 'REQUIRE-OPENING-NEW-WINDOW'
  | 'PLUGIN-CLOSE-DIALOG'
  | 'OAUTH-CONNECT-SUCCESS'
  | 'OAUTH2-SECRET-CODE-RETRIEVED'
  | 'OAUTH2-FAILURE'
  | 'OAUTH2-SUCCESS';
