import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { LocalPersistenceService } from './local-persistence.service';
import { createStore, Store } from '../store/store';

/**
 * Service that hold the copied things that can pasted in different screens.
 * Aka the test steps that you can copy from a test case to another.
 */
@Injectable({
  providedIn: 'root',
})
export class CopierService {
  private readonly STEP_COPY_PERSISTENCE: string = 'step-copy-persistence';
  private store: Store<LocalStorageStepCopyData> = createStore(
    getDefaultLocalStorageStepCopyData(),
  );

  public copiedTestStep$: Observable<number[]>;
  public copiedTestStepTcKind$: Observable<string>;

  constructor(private localPersistenceService: LocalPersistenceService) {
    this.synchroniseStateWithLocalStorage();
    this.copiedTestStep$ = this.store.state$.pipe(map((state) => state.copiedTestSteps));

    this.copiedTestStepTcKind$ = this.store.state$.pipe(map((state) => state.copiedTestStepTcKind));
  }

  notifyCopySteps(stepIds: number[], testCaseKind: string): void {
    const localStorageStepCopyData: LocalStorageStepCopyData = {
      copiedTestSteps: stepIds,
      copiedTestStepTcKind: testCaseKind,
    };
    this.localPersistenceService
      .set(this.STEP_COPY_PERSISTENCE, localStorageStepCopyData)
      .subscribe(() => this.store.commit(localStorageStepCopyData));
  }

  synchroniseStateWithLocalStorage(): void {
    this.localPersistenceService
      .get<LocalStorageStepCopyData>(this.STEP_COPY_PERSISTENCE)
      .pipe(filter((data) => data != null))
      .subscribe((data) => {
        this.store.commit(data);
      });
  }
}

function getDefaultLocalStorageStepCopyData(): LocalStorageStepCopyData {
  return {
    copiedTestSteps: [],
    copiedTestStepTcKind: '',
  };
}

export interface LocalStorageStepCopyData {
  copiedTestSteps: number[];
  copiedTestStepTcKind: string;
}
