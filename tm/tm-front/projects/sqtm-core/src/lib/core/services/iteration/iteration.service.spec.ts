import { TestBed } from '@angular/core/testing';

import { IterationService } from './iteration.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestingUtilsModule } from '../../../ui/testing-utils/testing-utils.module';

describe('IterationService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, HttpClientTestingModule],
    }),
  );

  it('should be created', () => {
    const service: IterationService = TestBed.inject(IterationService);
    expect(service).toBeTruthy();
  });
});
