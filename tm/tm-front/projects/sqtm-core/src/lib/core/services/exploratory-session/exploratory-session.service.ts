import { Injectable } from '@angular/core';
import { RestService } from '../rest.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ExploratorySessionService {
  constructor(private restService: RestService) {}

  runExploratoryExecution(executionId: number): Observable<void> {
    return this.restService.post(['exploratory-execution', executionId.toString(), 'run']);
  }

  pauseExploratoryExecution(executionId: number): Observable<void> {
    return this.restService.post(['exploratory-execution', executionId.toString(), 'pause']);
  }

  // With Angular 15, there's no way to specify the keepalive flag as the HttpClient is not built on fetch API.
  // In order to reliably send the request on page unload, we have to hack our way around and do the backend request
  // ourselves with the fetch API and do low-level stuff such as building the X-Xsrf-Token header.
  pauseSessionWithKeepAlive(executionId: number): void {
    const url = `${this.restService.backendRootUrl}/exploratory-execution/${executionId}/pause`;
    const xsrfCookie = this.extractCookie('XSRF-TOKEN');

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'X-Xsrf-Token': xsrfCookie,
      },
      keepalive: true,
    });
  }

  private extractCookie(name: string) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);

    if (parts.length === 2) {
      return parts.pop().split(';').shift();
    }
  }

  stopExploratoryExecution(executionId: number): Observable<void> {
    return this.restService.post(['exploratory-execution', executionId.toString(), 'stop']);
  }

  updateReviewStatus(executionId: string, reviewed: boolean): Observable<void> {
    return this.restService.post(['exploratory-execution', executionId, 'update-review-status'], {
      reviewed,
    });
  }
}
