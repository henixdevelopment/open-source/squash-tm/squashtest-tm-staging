import { Observable } from 'rxjs';
import { InjectionToken } from '@angular/core';

// This interface is used to avoid circular dependencies between the service and the component
export interface IExternalNavigationConsentService {
  handleClickEvent(event: MouseEvent, unsub$: Observable<void>): void;
  findDomain(url: string): string;
  addTrustedUrl(url: string): void;
}

export const EXTERNAL_NAVIGATION_CONSENT_SERVICE_TOKEN =
  new InjectionToken<IExternalNavigationConsentService>(
    'EXTERNAL_NAVIGATION_CONSENT_SERVICE_TOKEN',
  );
