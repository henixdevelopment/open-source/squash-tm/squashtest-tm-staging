import { TestBed } from '@angular/core/testing';

import { ExternalNavigationConsentService } from './external-navigation-consent.service';
import { LocalPersistenceService } from '../local-persistence.service';

describe('ExternalNavigationConsentService', () => {
  let service: ExternalNavigationConsentService;

  let storage = [];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: LocalPersistenceService,
          useValue: {
            getSync: () => [...storage],
            set: (_, data) => {
              return {
                subscribe: () => {
                  storage = data;
                },
              };
            },
          },
        },
      ],
    });
    service = TestBed.inject(ExternalNavigationConsentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should add trusted url', () => {
    service.addTrustedUrl('https://www.example.com');

    expect(service.isTrustedUrl('https://www.example.com')).toBeTrue();
    expect(service.isTrustedUrl('https://www.example.com/some-endpoint')).toBeTrue();

    expect(service.isTrustedUrl('https://www.subdomain.example.com/')).toBeFalse();
  });
});
