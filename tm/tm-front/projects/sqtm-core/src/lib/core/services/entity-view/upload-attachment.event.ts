import {
  PersistedAttachment,
  RejectedAttachment,
  UploadingAttachment,
} from '../../../model/attachment/attachment.model';

export interface UploadAttachmentEvent {
  readonly kind: 'start' | 'progress' | 'complete';
}

export class StartUpload implements UploadAttachmentEvent {
  readonly kind = 'start' as const;

  constructor(public attachments: (UploadingAttachment | RejectedAttachment)[]) {}
}

export class UploadProgress implements UploadAttachmentEvent {
  readonly kind = 'progress' as const;

  constructor(public attachments: Pick<UploadingAttachment, 'id' | 'uploadProgress'>[]) {}
}

export class UploadComplete implements UploadAttachmentEvent {
  readonly kind = 'complete' as const;

  constructor(
    public attachments: PersistedAttachment[],
    public uploadingAttachmentsToDelete: string[],
  ) {}
}
