import { Injectable } from '@angular/core';
import { RestService } from '../rest.service';
import { Observable } from 'rxjs';
import { ImportType } from '../../../model/project/pivot-format-import.model';

@Injectable({
  providedIn: 'root',
})
export class ProjectImportService {
  constructor(private restService: RestService) {}

  importProjectFromPivotFormatFile(
    files: File[],
    importName: string,
    importType: ImportType,
    projectId?: number,
  ): Observable<any> {
    const formData = new FormData();
    formData.append('archive', files[0], files[0].name);
    formData.append('importName', importName);
    formData.append('importType', importType);

    if (projectId) {
      return this.restService.post(
        ['generic-projects', projectId.toString(), 'create-import-request'],
        formData,
      );
    }
  }
}
