import { Injectable } from '@angular/core';
import { RestService } from '../rest.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ExecutionService {
  constructor(private restService: RestService) {}

  createNewAfterModificationDuringExecution(executionId: number): Observable<{ id: number }> {
    return this.restService.post<{ id: number }>([
      'execution',
      executionId.toString(),
      'update-from-tc',
    ]);
  }

  updateComment(executionId: number, newComment: string): Observable<void> {
    return this.restService.post(['execution', executionId.toString(), 'comment'], {
      comment: newComment,
    });
  }
}
