import { RestService } from '../rest.service';
import { SessionNoteKindKeys } from '../../../model/level-enums/level-enum';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SessionNoteModel } from '../../../model/execution/execution.model';

@Injectable({
  providedIn: 'root',
})
export class SessionNoteService {
  constructor(private restService: RestService) {}

  createNote(
    noteKind: SessionNoteKindKeys,
    noteContent: string,
    noteOrder: number,
    executionId: number,
  ): Observable<SessionNoteModel> {
    return this.restService.post<SessionNoteModel>(
      ['session-note', 'create'],
      { noteKind, noteContent, noteOrder },
      { params: { executionId: executionId.toString() } },
    );
  }

  updateNoteKindServerSide(
    noteId: number,
    noteKind: SessionNoteKindKeys,
  ): Observable<SessionNoteModel> {
    return this.restService.post<SessionNoteModel>(['session-note', noteId.toString(), 'kind'], {
      noteKind,
    });
  }

  updateNoteContentServerSide(noteId: number, noteContent: string): Observable<SessionNoteModel> {
    return this.restService.post<SessionNoteModel>(['session-note', noteId.toString(), 'content'], {
      noteContent,
    });
  }

  deleteNoteServerSide(noteId: number): Observable<SessionNoteModel> {
    return this.restService.delete<SessionNoteModel>(['session-note', noteId.toString()]);
  }
}
