import { Inject, Injectable, isDevMode } from '@angular/core';
import {
  ExperimentalFeature,
  SQTM_AVAILABLE_EXPERIMENTAL_FEATURE_FLAGS_TOKEN,
  SQTM_ENABLED_EXPERIMENTAL_FEATURE_FLAGS_TOKEN,
} from './experimental-feature.constants';

@Injectable({
  providedIn: 'root',
})
export class ExperimentalFeatureService {
  private readonly enabledFlags: string[];

  constructor(
    @Inject(SQTM_AVAILABLE_EXPERIMENTAL_FEATURE_FLAGS_TOKEN)
    private readonly availableFlags: string[],
    @Inject(SQTM_ENABLED_EXPERIMENTAL_FEATURE_FLAGS_TOKEN) injectedEnabledFlags: string[],
  ) {
    // In prod builds, the experimental feature flags are set on the window object by the backend.
    // See the `sqtmExperimentalFeatureFlags` variable in the index.html file.
    const enabledFlags = isDevMode()
      ? injectedEnabledFlags
      : window['sqtmExperimentalFeatureFlags'];

    this.enabledFlags = this.checkFlagsAvailability(enabledFlags);
  }

  isEnabled(feature: ExperimentalFeature): boolean {
    this.checkExistence(feature);
    return this.enabledFlags.includes(feature);
  }

  check(feature: ExperimentalFeature): void {
    this.checkExistence(feature);
    if (!this.isEnabled(feature)) {
      throw new Error(`The experimental feature "${feature}" is not enabled.`);
    }
  }

  private checkFlagsAvailability(flags: string[]): ExperimentalFeature[] {
    if (flags == null) {
      logIfDevMode('No experimental feature flags provided.');
      return [];
    }

    const knownFlags = flags.filter((flag) => {
      if (!this.availableFlags.includes(flag)) {
        logIfDevMode(`Unknown experimental feature flag: ${flag}`);
        return false;
      }

      return true;
    });

    if (isDevMode()) {
      logIfDevMode('Enabled experimental features:', knownFlags);
    }

    return knownFlags as ExperimentalFeature[];
  }

  private exists(feature: ExperimentalFeature): boolean {
    return this.availableFlags.includes(feature);
  }

  private checkExistence(feature: string) {
    if (!this.exists(feature as ExperimentalFeature)) {
      throw new Error(`The experimental feature "${feature}" does not exist.`);
    }
  }
}

function logIfDevMode(...messages: unknown[]): void {
  if (isDevMode()) {
    const redFlagEmoji = '\u{1F6A9}';
    console.debug(redFlagEmoji, ...messages);
  }
}
