import { TestBed } from '@angular/core/testing';

import { EntityViewCustomFieldHelperService } from './entity-view-custom-field-helper.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CustomFieldValueService } from '../custom-field-value.service';
import { of } from 'rxjs';
import { ReferentialDataService } from '../../referential/services/referential-data.service';
import { CustomFieldValueState } from './entity-view.state';
import {
  CustomFieldValue,
  CustomFieldValueModel,
} from '../../../model/customfield/custom-field-value.model';
import { CustomField } from '../../../model/customfield/customfield.model';
import { InputType } from '../../../model/customfield/input-type.model';
import { TestingUtilsModule } from '../../../ui/testing-utils/testing-utils.module';

describe('EntityViewCustomFieldHelperService', () => {
  const customFieldServiceMock = jasmine.createSpyObj('customFieldService', [
    'postCustomFieldValue',
  ]);
  const referentialDataServiceMock = jasmine.createSpyObj('referentialDataService', ['refresh']);

  // referentialDataServiceMock.referentialData$ = of({customFieldState});

  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, HttpClientTestingModule],
      providers: [
        {
          provide: CustomFieldValueService,
          useValue: customFieldServiceMock,
        },
        {
          provide: ReferentialDataService,
          useValue: referentialDataServiceMock,
        },
      ],
    }),
  );

  it('should be created', () => {
    const service: EntityViewCustomFieldHelperService = TestBed.inject(
      EntityViewCustomFieldHelperService,
    );
    expect(service).toBeTruthy();
  });

  it('should create empty state', () => {
    const service: EntityViewCustomFieldHelperService = TestBed.inject(
      EntityViewCustomFieldHelperService,
    );
    const customFieldValueState = service.initializeCustomFieldValueState([]);
    expect(customFieldValueState.ids.length).toEqual(0);
    expect(customFieldValueState.entities).toEqual({});
  });

  it('should load cuf value models', () => {
    const service: EntityViewCustomFieldHelperService = TestBed.inject(
      EntityViewCustomFieldHelperService,
    );
    const customFieldValueModels: CustomFieldValueModel[] = [
      { id: 1, value: 'value', cufId: 1, fieldType: 'CF' },
      { id: 2, value: 'discovery|endeavour', cufId: 2, fieldType: 'TAG' },
    ];
    const customFieldValueState = service.initializeCustomFieldValueState(customFieldValueModels);
    expect(customFieldValueState.ids).toEqual([1, 2]);
    const first = customFieldValueState.entities[1];
    expect(first.id).toEqual(1);
    expect(first.value).toEqual('value');
    const second = customFieldValueState.entities[2];
    expect(second.id).toEqual(2);
    expect(second.value).toEqual(['discovery', 'endeavour']);
  });

  describe('Update Custom Field Value', () => {
    interface DataType {
      cfv: CustomFieldValue;
      value: string | string[];
      expectedValue: string | string[];
      customFieldValueState: CustomFieldValueState;
    }

    const cuf1: CustomField = { id: 1, inputType: InputType.PLAIN_TEXT } as CustomField;
    const cuf2: CustomField = { id: 2, inputType: InputType.DATE_PICKER } as CustomField;
    const cuf3: CustomField = { id: 3, inputType: InputType.TAG } as CustomField;

    const customFieldState = {
      ids: [1, 2, 3],
      entities: {
        1: cuf1,
        2: cuf2,
        3: cuf3,
      },
    };

    const cfv1: CustomFieldValue = { id: 1, cufId: 1, value: 'oldValue', fieldType: 'CF' };
    const cfv2: CustomFieldValue = { id: 2, cufId: 2, value: '2018-12-12', fieldType: 'CF' };
    const cfv3: CustomFieldValue = {
      id: 3,
      cufId: 3,
      value: ['discovery', 'enterprise'],
      fieldType: 'TAG',
    };

    const customFieldValueState = {
      ids: [1, 2, 3],
      entities: {
        1: cfv1,
        2: cfv2,
        3: cfv3,
      },
    };

    const dataSets: DataType[] = [
      {
        cfv: cfv1,
        value: 'newValue',
        expectedValue: 'newValue',
        customFieldValueState,
      },
      {
        cfv: cfv2,
        value: '2019-11-29',
        expectedValue: '2019-11-29',
        customFieldValueState,
      },
      {
        cfv: cfv3,
        value: ['endeavour', 'atlantis'],
        expectedValue: ['endeavour', 'atlantis'],
        customFieldValueState,
      },
    ];
    dataSets.forEach((data, index) => runTest(data, index));

    referentialDataServiceMock.referentialData$ = of({ customFieldState });

    function runTest(data: DataType, index: number) {
      it(`Dataset ${index} - It should update custom field value with value ${data.value}`, () => {
        const service: EntityViewCustomFieldHelperService = TestBed.inject(
          EntityViewCustomFieldHelperService,
        );
        customFieldServiceMock.postCustomFieldValue.and.returnValue(of(null));
        const nextState = service.updateCustomFieldValue(
          data.cfv.id,
          data.value,
          data.customFieldValueState,
        );
        expect(nextState).not.toBe(data.customFieldValueState);
        expect(nextState.entities[data.cfv.id].value).toEqual(data.expectedValue);
      });
    }
  });
});
