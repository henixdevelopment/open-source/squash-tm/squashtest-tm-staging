import { Injectable } from '@angular/core';
import { RestService } from '../rest.service';
import { Observable } from 'rxjs';
import {
  XrayImportModel,
  XrayInfoModel,
} from '../../../model/project/project-import-from-xray.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ProjectImportXrayService {
  constructor(
    private restService: RestService,
    private http: HttpClient,
  ) {}

  getXrayImportModelFromXML(files: File[], projectId?: number): Observable<XrayImportModel> {
    const formData = new FormData();
    for (const file of files) {
      formData.append('archive', file, file.name);
    }
    return this.restService.post<XrayImportModel>(
      ['generic-projects', projectId.toString(), 'import-from-xray-xml'],
      formData,
    );
  }

  getPivotFileXray(projectId?: number, fileName?: string): Observable<Blob> {
    const url = `generic-projects/${projectId.toString()}/import-from-xray-xml/${fileName}`;
    return this.http.get(`${window.location.origin}${this.restService.backendRootUrl}${url}`, {
      responseType: 'blob',
    });
  }

  getPivotLogFile(projectId?: number, fileName?: string): string {
    const url = `generic-projects/${projectId.toString()}/import-from-xray-xml/${fileName}`;
    return `${window.location.origin}${this.restService.backendRootUrl}${url}`;
  }

  getXrayInfoModelFromXML(files: File[], projectId?: number): Observable<XrayInfoModel> {
    const formData = new FormData();
    for (const file of files) {
      formData.append('archive', file, file.name);
    }
    return this.restService.post<XrayInfoModel>(
      ['generic-projects', projectId.toString(), 'info-from-xray-xml'],
      formData,
    );
  }

  deletePivotTempFiles(
    projectId?: number,
    pivotFileName?: string,
    pivotLogName?: string,
  ): Observable<void> {
    return this.restService.delete<void>(
      ['generic-projects', projectId.toString(), 'import-from-xray-xml'],
      {
        params: {
          pivotFile: pivotFileName,
          pivotLog: pivotLogName,
        },
      },
    );
  }
}
