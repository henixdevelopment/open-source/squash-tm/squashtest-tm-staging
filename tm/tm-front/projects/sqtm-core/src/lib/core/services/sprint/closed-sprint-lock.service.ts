import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ClosedSprintLockService {
  private readonly store = new BehaviorSubject(getInitialState());

  public readonly isSprintClosed$ = this.store
    .asObservable()
    .pipe(map((state) => state.isSprintClosed));

  public get isSprintClosed(): boolean {
    return this.store.value.isSprintClosed;
  }

  public setSprintClosed(isSprintClosed: boolean): void {
    this.store.next({ isSprintClosed });
  }
}

interface ClosedSprintLockState {
  isSprintClosed: boolean;
}

function getInitialState(): ClosedSprintLockState {
  return {
    isSprintClosed: false,
  };
}
