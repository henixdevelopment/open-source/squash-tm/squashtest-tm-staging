import { TestBed } from '@angular/core/testing';

import { ExperimentalFeatureService } from './experimental-feature.service';
import {
  SQTM_AVAILABLE_EXPERIMENTAL_FEATURE_FLAGS_TOKEN,
  SQTM_ENABLED_EXPERIMENTAL_FEATURE_FLAGS_TOKEN,
} from './experimental-feature.constants';

describe('ExperimentalFeatureService', () => {
  let service: ExperimentalFeatureService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: SQTM_AVAILABLE_EXPERIMENTAL_FEATURE_FLAGS_TOKEN,
          useValue: ['FOO', 'BAR'],
        },
        {
          provide: SQTM_ENABLED_EXPERIMENTAL_FEATURE_FLAGS_TOKEN,
          useValue: ['FOO', 'BAZ'],
        },
      ],
    });
    service = TestBed.inject(ExperimentalFeatureService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return true for enabled feature', () => {
    expect(service.isEnabled('FOO' as any)).toBeTrue();
  });

  it('should return false for disabled feature', () => {
    expect(service.isEnabled('BAR' as any)).toBeFalse();
  });

  it('should throw an error for disabled feature', () => {
    expect(() => service.check('BAR' as any)).toThrowError(
      'The experimental feature "BAR" is not enabled.',
    );
  });

  it('should throw an error for unknown feature', () => {
    expect(() => service.check('BAZ' as any)).toThrowError(
      'The experimental feature "BAZ" does not exist.',
    );
  });
});
