import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { SquashTmDataRowType } from '../../../model/grids/data-row.model';

@Injectable({
  providedIn: 'root',
})
export class EntityPathHeaderService {
  private _refreshPath: Subject<void> = new Subject<void>();
  refreshPath$: Observable<void>;
  constructor() {
    this.refreshPath$ = this._refreshPath.asObservable();
  }
  refreshPath() {
    this._refreshPath.next();
  }

  public static getUpdatableEntities(): SquashTmDataRowType[] {
    return [
      SquashTmDataRowType.CampaignFolder,
      SquashTmDataRowType.RequirementFolder,
      SquashTmDataRowType.TestCaseFolder,
      SquashTmDataRowType.Campaign,
      SquashTmDataRowType.Requirement,
      SquashTmDataRowType.HighLevelRequirement,
      SquashTmDataRowType.TestCase,
      SquashTmDataRowType.Iteration,
      SquashTmDataRowType.TestSuite,
    ];
  }
}
