import { InjectionToken } from '@angular/core';

// List of all supported experimental feature flags.
export const AVAILABLE_EXPERIMENTAL_FEATURE_FLAGS = ['CONFIGURE_PROFILES'] as const;

export type ExperimentalFeature = (typeof AVAILABLE_EXPERIMENTAL_FEATURE_FLAGS)[number];

// Injection tokens for the experimental feature flags.

export const SQTM_ENABLED_EXPERIMENTAL_FEATURE_FLAGS_TOKEN = new InjectionToken(
  'Enabled experimental feature flags',
);

export const SQTM_AVAILABLE_EXPERIMENTAL_FEATURE_FLAGS_TOKEN = new InjectionToken(
  'Available experimental feature flags',
);
