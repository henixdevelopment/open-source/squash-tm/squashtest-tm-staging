import { TestBed } from '@angular/core/testing';

import { ParametersService } from './parameters.service';
import { TestingUtilsModule } from '../../../ui/testing-utils/testing-utils.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ParametersService', () => {
  let service: ParametersService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, HttpClientTestingModule],
    });
    service = TestBed.inject(ParametersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
