import { TestBed } from '@angular/core/testing';
import { SquashPlatformNavigationService } from './squash-platform-navigation.service';
import { TestingUtilsModule } from '../../../ui/testing-utils/testing-utils.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('PlatformNavigationService', () => {
  let service: SquashPlatformNavigationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, RouterTestingModule],
    });
    service = TestBed.inject(SquashPlatformNavigationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
