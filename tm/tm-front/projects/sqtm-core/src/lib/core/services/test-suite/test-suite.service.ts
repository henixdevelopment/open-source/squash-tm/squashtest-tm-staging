import { Injectable } from '@angular/core';
import { RestService } from '../rest.service';
import { Identifier } from '../../../model/entity.model';
import { Observable } from 'rxjs';
import { TestPlanResumeModel } from '../../../model/execution/test-plan-resume.model';
import { FilterValueModel } from '../../../model/filter/filter-value.model';

@Injectable({
  providedIn: 'root',
})
export class TestSuiteService {
  constructor(private restService: RestService) {}

  addTestCase(testCaseIds: number[], testSuiteId: number): Observable<any> {
    const url = `test-suite/${testSuiteId}/test-plan-items`;
    return this.restService.post<any>([url], { testCaseIds });
  }

  changeItemsPosition(
    testSuiteId: number,
    itemsToMove: Identifier[],
    position: number,
  ): Observable<void> {
    return this.restService.post([
      'test-suite',
      testSuiteId.toString(),
      'test-plan',
      itemsToMove.join(','),
      'position',
      position.toString(),
    ]);
  }

  detachTestCaseFromTestSuite(
    testSuiteId: number,
    itemsToDetach: number[],
  ): Observable<DeletedTestPlanItemReport> {
    return this.restService.delete<DeletedTestPlanItemReport>([
      'test-suite',
      testSuiteId.toString(),
      'test-plan-items/detach',
      itemsToDetach.toString(),
    ]);
  }

  removeTestCaseFromTestSuiteAndIteration(
    testSuiteId: number,
    itemsToRemove: number[],
  ): Observable<DeletedTestPlanItemReport> {
    return this.restService.delete<DeletedTestPlanItemReport>([
      'test-suite',
      testSuiteId.toString(),
      'test-plan-items/remove',
      itemsToRemove.toString(),
    ]);
  }

  resume(testSuiteId: number): Observable<TestPlanResumeModel> {
    return this.restService.post<TestPlanResumeModel>([
      'test-suite',
      testSuiteId.toString(),
      'test-plan',
      'resume',
    ]);
  }

  resumeFiltered(
    testSuiteId: number,
    filterValues: FilterValueModel[],
  ): Observable<TestPlanResumeModel> {
    return this.restService.post<TestPlanResumeModel>(
      ['test-suite', testSuiteId.toString(), 'test-plan', 'resume-filtered-selection'],
      { filterValues },
    );
  }

  relaunch(testSuiteId: number): Observable<TestPlanResumeModel> {
    return this.restService.post<TestPlanResumeModel>([
      'test-suite',
      testSuiteId.toString(),
      'test-plan',
      'relaunch',
    ]);
  }

  relaunchFiltered(
    testSuiteId: number,
    filterValues: FilterValueModel[],
  ): Observable<TestPlanResumeModel> {
    return this.restService.post<TestPlanResumeModel>(
      ['test-suite', testSuiteId.toString(), 'test-plan', 'relaunch-filtered-selection'],
      { filterValues },
    );
  }

  getNextExecution(testSuiteId: number, testPlanItemId: number): Observable<TestPlanResumeModel> {
    return this.restService.post<TestPlanResumeModel>([
      'test-suite',
      testSuiteId.toString(),
      'test-plan',
      testPlanItemId.toString(),
      'next-execution',
    ]);
  }

  getNextExecutionOfPartial(
    testSuiteId: number,
    testPlanItemId: number,
    partialTestPlanItemIds: number[],
  ): Observable<TestPlanResumeModel> {
    return this.restService.post<TestPlanResumeModel>(
      [
        'test-suite',
        testSuiteId.toString(),
        'test-plan',
        testPlanItemId.toString(),
        'next-execution-filtered-selection',
      ],
      { ids: partialTestPlanItemIds },
    );
  }
}

export interface AddTestCaseToTestSuiteResponse {
  itemTestPlanIds: number[];
  hasDataSet: boolean;
}

export interface DeletedTestPlanItemReport {
  nbIssues: number;
  executionStatus: string;
}
