import { Injectable } from '@angular/core';
import { LocalPersistenceService } from '../local-persistence.service';
import {
  DOC_URL_EN,
  DOC_URL_FR,
  NEWSLETTER_URL_EN,
  NEWSLETTER_URL_FR,
  OPENTESTFACTORY_DOC_URL,
} from '../../../ui/navbar/nav-bar.constants';
import { takeUntil } from 'rxjs/operators';
import { DialogService } from '../../../ui/dialog/services/dialog.service';
import {
  EXTERNAL_NAVIGATION_CONSENT_DIALOG_ID,
  ExternalNavigationConsentDialogComponent,
} from '../../../ui/dialog/components/external-navigation-consent-dialog/external-navigation-consent-dialog.component';
import { Observable } from 'rxjs';
import {
  EXTERNAL_NAVIGATION_CONSENT_SERVICE_TOKEN,
  IExternalNavigationConsentService,
} from './external-navigation-consent.constant';

const TRUSTED_DOMAINS_KEY = 'trustedDomains';
const BUILTIN_TRUSTED_DOMAINS = [
  DOC_URL_FR,
  DOC_URL_EN,
  NEWSLETTER_URL_FR,
  NEWSLETTER_URL_EN,
  OPENTESTFACTORY_DOC_URL,
  'https://gitlab.com/',
  'https://www.squashtest.com',
  'https://opentestfactory.org',
].map((url) => findDomain(url));

@Injectable({
  providedIn: 'root',
})
export class ExternalNavigationConsentService implements IExternalNavigationConsentService {
  constructor(
    private readonly localPersistenceService: LocalPersistenceService,
    private dialogService: DialogService,
  ) {}

  public handleClickEvent(event: MouseEvent, unsub$: Observable<void>): void {
    const target = event.target as HTMLElement;
    const anchor = target.closest('a');

    if (anchor) {
      const href = anchor.href;
      const target = anchor.target;

      if (this.isExternalLink(href) && !this.isTrustedUrl(href)) {
        event.preventDefault();
        event.stopPropagation();
        event.stopImmediatePropagation();
        this.openConsentModal(href, target, unsub$);
      }
    }
  }

  public addTrustedUrl(url: string): void {
    this.addTrustedDomain(this.findDomain(url));
  }

  public isTrustedUrl(url: string): boolean {
    return this.getTrustedDomains().includes(this.findDomain(url));
  }

  public findDomain(url: string) {
    return findDomain(url);
  }

  private isExternalLink(href: string): boolean {
    if (!href?.trim()) {
      return false;
    }

    const url = new URL(href);
    const domain = url.hostname;
    const protocol = url.protocol;
    const handledProtocols = ['http:', 'https:', 'mailto:', 'ftp:'];
    return handledProtocols.includes(protocol) && domain !== window.location.hostname;
  }

  private openConsentModal(url: string, target: string, unsub$: Observable<void>): void {
    this.dialogService
      .openDialog({
        id: EXTERNAL_NAVIGATION_CONSENT_DIALOG_ID,
        component: ExternalNavigationConsentDialogComponent,
        maxWidth: 600,
        data: { url },
      })
      .dialogClosed$.pipe(takeUntil(unsub$))
      .subscribe((confirmed) => {
        if (confirmed) {
          window.open(url, target);
        }
      });
  }

  private getTrustedDomains(): string[] {
    return [
      ...BUILTIN_TRUSTED_DOMAINS,
      ...(this.localPersistenceService.getSync<string[]>(TRUSTED_DOMAINS_KEY) || []),
    ];
  }

  private addTrustedDomain(domain: string): void {
    const trustedDomains = this.getTrustedDomains();

    if (!trustedDomains.includes(domain)) {
      trustedDomains.push(domain);
      this.localPersistenceService.set(TRUSTED_DOMAINS_KEY, trustedDomains).subscribe();
    }
  }
}

function findDomain(url: string): string {
  const domain = new URL(url).hostname;
  return domain.startsWith('www.') ? domain.slice(4) : domain;
}

export function provideExternalNavigationConsentService() {
  return [
    {
      provide: EXTERNAL_NAVIGATION_CONSENT_SERVICE_TOKEN,
      useClass: ExternalNavigationConsentService,
    },
  ];
}
