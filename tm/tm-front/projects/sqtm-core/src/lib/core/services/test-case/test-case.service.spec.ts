import { TestBed } from '@angular/core/testing';

import { TestCaseService } from './test-case.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestingUtilsModule } from '../../../ui/testing-utils/testing-utils.module';

describe('TestCaseService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, HttpClientTestingModule],
    }),
  );

  it('should be created', () => {
    const service: TestCaseService = TestBed.inject(TestCaseService);
    expect(service).toBeTruthy();
  });
});
