import { Injectable } from '@angular/core';
import { RestService } from '../rest.service';
import { interval, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

/**
 * Keeps current session active by sending pings to the server.
 * This service holds a counter to registered clients to determine if it should ping. This means you should both
 * register and unregister if you don't want an infinite ping.
 */
@Injectable({
  providedIn: 'root',
})
export class SessionPingService {
  static PING_INTERVAL = 10 * 60000;

  private _clientCounter = 0;

  constructor(public readonly restService: RestService) {}

  /**
   * Starts the ping timer.
   * The basic usage is to call this method once at the app initialization and let it run.
   */
  initialize(): Subscription {
    return interval(SessionPingService.PING_INTERVAL)
      .pipe(filter(() => this._clientCounter > 0))
      .subscribe(() => this.doPing());
  }

  /**
   * Signals the service that there's a long-term edition going on so the session should be kept alive.
   */
  beginLongTermEdition(): void {
    this._clientCounter++;
  }

  /**
   * Signals the service that a long-term edition has finished.
   */
  endLongTermEdition(): void {
    if (this._clientCounter > 0) {
      this._clientCounter--;
    } else {
      console.warn('No long-term session to end.');
    }
  }

  private doPing(): void {
    this.restService.get(['ping']).subscribe();
  }
}
