import { Injectable } from '@angular/core';
import { RestService } from './rest.service';
import { Observable } from 'rxjs';
import { UploadSummary } from '../../model/attachment/upload-summary.model';
import { HttpEvent } from '@angular/common/http';
import { AttachmentModel, PersistedAttachment } from '../../model/attachment/attachment.model';
import { GridResponse } from '../../model/grids/grid-response.model';
import { FileViewer, FileViewerFilePathType } from '../../model/attachment/file-viewer.model';

@Injectable({
  providedIn: 'root',
})
export class AttachmentService {
  constructor(private restService: RestService) {}

  uploadAttachments(
    files: File[],
    attachmentListId: number,
    entityId: number,
    entityType: string,
    holderType: string,
  ): Observable<HttpEvent<UploadSummary[]>> {
    const formData = new FormData();
    for (const file of files) {
      formData.append('attachment[]', file, file.name);
    }

    formData.append('entityId', entityId.toString());
    formData.append('entityType', entityType);
    formData.append('holderType', holderType);

    return this.restService.postFile<UploadSummary[]>(
      ['attach-list', attachmentListId.toString(), 'attachments', 'upload'],
      formData,
    );
  }

  uploadImage(
    file: File,
    attachmentListId: number,
    entityId: number,
    entityType: string,
    holderType: string,
  ): Observable<HttpEvent<UploadSummary[]>> {
    const formData = new FormData();

    formData.append('attachment', file, file.name);
    formData.append('entityId', entityId.toString());
    formData.append('entityType', entityType);
    formData.append('holderType', holderType);

    return this.restService.postFile<UploadSummary[]>(
      ['attach-list', attachmentListId.toString(), 'attachment', 'upload-image'],
      formData,
    );
  }

  deleteAttachments(
    attachmentIds: number[],
    attachmentListId: number,
    entityId: number,
    entityType: string,
    holderType: string,
  ): Observable<any> {
    const options = {
      params: {
        entityId: entityId.toString(),
        entityType,
        holderType,
      },
    };
    return this.restService.delete(
      ['attach-list', attachmentListId.toString(), 'attachments', attachmentIds.join(',')],
      options,
    );
  }

  getAttachmentDownloadURL(attachmentListId: number, attachment: PersistedAttachment) {
    return `${window.location.origin}${this.restService.backendRootUrl}attach-list/${attachmentListId}/attachments/download/${attachment.id}`;
  }

  getAttachmentPreviewURL(viewer: FileViewer) {
    const attachment: AttachmentModel = viewer.attachment;
    const path = `file-viewer/attachment/${attachment.id}`;
    const params = viewer.isArchive ? { target: viewer.activeFile } : {};
    return this.restService.buildExportUrlWithParams(path, params);
  }

  getFileDownloadUrl(origin: FileViewerFilePathType, filePath: string) {
    return this.restService.buildExportUrlWithParams('file-viewer/download/file', {
      origin: origin,
      source: filePath,
    });
  }

  getArchiveFileNode(attachment: AttachmentModel): Observable<GridResponse> {
    return this.restService.get<GridResponse>(['file-viewer', 'archive', attachment.id.toString()]);
  }

  getFilePreviewURL(origin: FileViewerFilePathType, source: string) {
    return this.restService.buildExportUrlWithParams(`file-viewer/preview/file`, {
      origin: origin,
      source: source,
    });
  }

  getAttachmentPreviewContent(viewer: FileViewer): Observable<string> {
    const parts = ['file-viewer', 'attachment', viewer.attachment.id.toString()];
    const params = viewer.isArchive ? { target: viewer.activeFile } : {};
    return this.restService.getResponseText(parts, params);
  }

  getFilePreviewContent(origin: FileViewerFilePathType, source: string): Observable<string> {
    return this.restService.getResponseText(['file-viewer', 'preview', 'file'], { origin, source });
  }
}
