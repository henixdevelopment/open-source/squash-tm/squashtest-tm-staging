import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

/**
 * Allows to share a global observable for intercepted generic error messages ("generic" means we received a http status code 500).
 * It only dispatches messages to components, it doesn't handle the actual display.
 *
 * @see GenericErrorDisplayComponent
 * @see ServerInternalErrorResponseInterceptor
 */
@Injectable({
  providedIn: 'root',
})
export class GenericErrorDisplayService {
  public readonly errorMessage$: Observable<ErrorMessageEvent>;
  _errorMessage$: Subject<ErrorMessageEvent> = new Subject<ErrorMessageEvent>();

  constructor() {
    this.errorMessage$ = this._errorMessage$.asObservable();
  }

  /**
   * Push a new error message to be displayed.
   * @param error the error object
   * @param stackTraceEnabled true if "Stack-Trace" response header enabled
   */
  showError(error: any, stackTraceEnabled: boolean): void {
    this._errorMessage$.next({ error, stackTraceEnabled });
  }
}

export interface ErrorMessageEvent {
  error: any;
  stackTraceEnabled: boolean;
}
