import { Injectable } from '@angular/core';
import { platformNavigationLogger } from '../../../ui/platform-navigation/platform-navigation.logger';
import { NavigationExtras, Router } from '@angular/router';
import { SQTM_MAIN_APP_IDENTIFIER } from '../../sqtm-core.tokens';

const logger = platformNavigationLogger.compose('PlatformNavigationService');

export interface PlatformLinkDefinition {
  pluginDestination: string;
  command: any[] | 'string';
  extras?: NavigationExtras;
  openInNewTab?: boolean;
}

@Injectable({
  providedIn: 'root',
})
export class SquashPlatformNavigationService {
  set pluginIdentifier(value: string) {
    logger.debug(`Setting pluginIdentifier to ${value}`);
    if (this._pluginIdentifier) {
      throw Error(`Plugin identifier was already defined to ${this._pluginIdentifier}`);
    }
    this._pluginIdentifier = value;
  }

  private _pluginIdentifier: string;

  set backendContextPath(value: string) {
    logger.debug(`Setting backendContextPath to ${value}`);
    if (this._backendContextPath) {
      throw Error(`Backend context path was already defined to ${this._backendContextPath}`);
    }
    this._backendContextPath = value;
  }

  private _backendContextPath: string;

  constructor(private router: Router) {
    logger.debug(`Initialize PlatformNavigationService`);
  }

  public navigate(candidate: PlatformLinkDefinition | string | any[]): void {
    const link = this.convertToLink(candidate);
    logger.debug(
      `begin navigation from module ${link.pluginDestination}. Current module is ${this._pluginIdentifier}`,
      [link],
    );
    if (link.pluginDestination === this._pluginIdentifier) {
      this.navigateInternal(link.command, link.extras, link.openInNewTab);
    } else {
      this.navigateExternal(link.command, link.extras);
    }
  }

  private convertToLink(candidate: PlatformLinkDefinition | string | any[]) {
    const isPlatformLinkDefinition =
      (candidate as any).command && (candidate as any).pluginDestination;
    const link: PlatformLinkDefinition = isPlatformLinkDefinition
      ? (candidate as PlatformLinkDefinition)
      : {
          command: candidate as any,
          pluginDestination: this._pluginIdentifier,
        };
    return link;
  }

  public navigateFromMainApplication(
    url: string,
    extras?: NavigationExtras,
    openInNewTab?: boolean,
  ): void {
    logger.debug(`begin navigation from main app to ${url}`);
    const parts = url.split('/');
    if (parts.length > 1 && parts[0] === 'plugin') {
      this.navigateExternal(url, extras, openInNewTab);
    } else {
      this.navigateInternal(url, extras, openInNewTab);
    }
  }

  public generateHref(candidate: PlatformLinkDefinition | string | any[]): string {
    const link = this.convertToLink(candidate);
    logger.debug(
      `generate navigation href from module ${link.pluginDestination}. Current module is ${this._pluginIdentifier}`,
      [link],
    );
    const url = link.command;
    if (typeof url === 'string') {
      return `${this._backendContextPath}${url}`;
    } else {
      const formattedUrl = this.createUrlFromCommand(url, link.extras);
      return `${this._backendContextPath}${formattedUrl}`;
    }
  }

  public navigateExternal(url: string | any[], extras?: NavigationExtras, openInNewTab?: boolean) {
    if (openInNewTab) {
      this.openNewTab(url, extras);
    } else {
      this.relocate(url, extras);
    }
  }

  // Trigger an external navigation to the application's root, triggering the backend redirection to login page
  navigateToRoot(queryParams: { [p: string]: string }): void {
    const link = this.convertToLink({
      command: [''],
      pluginDestination: SQTM_MAIN_APP_IDENTIFIER,
      extras: { queryParams },
    });

    this.navigateExternal(link.command, link.extras);
  }

  private relocate(url: string | any[], extras: NavigationExtras) {
    if (typeof url === 'string') {
      window.location.assign(`${this._backendContextPath}${url}`);
    } else {
      const formattedUrl = this.createUrlFromCommand(url, extras);
      window.location.assign(`${this._backendContextPath}${formattedUrl}`);
    }
  }

  public openNewTab(url: string | any[], extras?: NavigationExtras) {
    if (typeof url === 'string') {
      window.open(`${this._backendContextPath}${this.removeLeadingSlash(url)}`);
    } else {
      const formattedUrl = this.createUrlFromCommand(url, extras);
      window.open(`${this._backendContextPath}${formattedUrl}`);
    }
  }

  private createUrlFromCommand(url: any[], extras?: NavigationExtras) {
    const urlTree = this.router.createUrlTree(url as any[], extras);
    let formattedUrl = this.router.serializeUrl(urlTree);
    formattedUrl = this.removeLeadingSlash(formattedUrl);
    return formattedUrl;
  }

  private removeLeadingSlash(formattedUrl: string) {
    if (formattedUrl.charAt(0) === '/') {
      // not leading / in url, servlet context already has one
      formattedUrl = formattedUrl.slice(1);
    }
    return formattedUrl;
  }

  private navigateInternal(url: string | any[], extras?: NavigationExtras, openInNewTab?: boolean) {
    logger.debug(`Internal navigation to ${url}`);
    if (openInNewTab) {
      this.navigateExternal(url, extras, true);
    } else {
      if (typeof url === 'string') {
        this.router.navigateByUrl(url, extras);
      } else {
        this.router.navigate(url, extras);
      }
    }
  }
}
