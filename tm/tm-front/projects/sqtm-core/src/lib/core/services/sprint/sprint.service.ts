import { Injectable } from '@angular/core';
import { RestService } from '../rest.service';
import { Observable } from 'rxjs';
import { BindReqVersionToSprintOperationReport } from '../../../model/change-coverage-operation-report';
import { SprintReqVersionModel } from '../../../model/campaign/sprint-req-version-model';

@Injectable({
  providedIn: 'root',
})
export class SprintService {
  constructor(private restService: RestService) {}

  addRequirementsByRlnIds(
    sprintId: number,
    requirementIds: number[],
  ): Observable<BindReqVersionToSprintOperationReport> {
    const url = `sprint/${sprintId}/requirements`;
    return this.restService.post<BindReqVersionToSprintOperationReport>([url], requirementIds);
  }

  addReqVersions(
    sprintId: number,
    requirementIds: number[],
  ): Observable<BindReqVersionToSprintOperationReport> {
    const url = `sprint/${sprintId}/reqversions`;
    return this.restService.post<BindReqVersionToSprintOperationReport>([url], requirementIds);
  }

  deleteSprintReqVersions(
    sprintId: number,
    sprintReqVerIds: number[],
  ): Observable<BindReqVersionToSprintOperationReport> {
    const pathVariable = sprintReqVerIds.map((id) => id.toString()).join(',');
    const url = `sprint/${sprintId}/requirements/${pathVariable}`;
    return this.restService.delete([url]);
  }

  changeSprintStatus(sprintId: number, newStatus: string): Observable<any> {
    const url: string = `sprint/${sprintId}/status`;
    return this.restService.post([url], newStatus);
  }

  fetchSprintReqVersions(sprintId: number): Observable<SprintReqVersionModel[]> {
    const url: string = `sprint-view/${sprintId}/sprintReqVersions`;
    return this.restService.get([url]);
  }
}
