import { Observable } from 'rxjs';
import { TargetOnUpload, UploadImage } from '../../model/attachment/attachment.model';

export abstract class RichTextAttachmentDelegate {
  abstract attach(file: File, targetOnUpload?: TargetOnUpload): Observable<UploadImage>;
}
