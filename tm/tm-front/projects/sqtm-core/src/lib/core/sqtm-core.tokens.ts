import { InjectionToken } from '@angular/core';
import { SqtmCoreModuleConfiguration } from './sqtm-core.module';
import { UnauthorizedResponseInterceptor } from './interceptors/unauthorized-response.interceptor';

export const CORE_MODULE_CONFIGURATION = new InjectionToken<SqtmCoreModuleConfiguration>(
  'Configuration for Squash TM Core Module.',
);
export const CORE_MODULE_USER_CONFIGURATION = new InjectionToken<
  Partial<SqtmCoreModuleConfiguration>
>('Configuration for Squash TM Core Module.');

export const CORE_MODULE_UNAUTHORIZED_RESPONSE_INTERCEPTOR =
  new InjectionToken<UnauthorizedResponseInterceptor>('Token for UnauthorizedResponseInterceptor');

export const SQTM_MAIN_APP_IDENTIFIER = 'org.squashtest.squashtm.main-app.front-end';
export const LOGIN_PAGE_REDIRECT_AFTER_AUTH = 'redirect-after-auth';

export const BACKEND_CONTEXT_PATH = new InjectionToken<string>(
  'Root back end context aka servlet context',
);
