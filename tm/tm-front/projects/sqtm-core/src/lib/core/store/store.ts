import { BehaviorSubject, Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { addedDiff, deletedDiff, diff, updatedDiff } from 'deep-object-diff';
import { coreLogger } from '../core.logger';
import { isDevMode } from '@angular/core';

const storeLogger = coreLogger.compose('Store');

/**
 * Heavily inspired by ngrx/store. However we only want some parts of ngrx aka immutable store and selectors
 * We don't want reducers, effects and all the global stuff, which are rejected by the team.
 *
 * This class is a modular store. It means that you will have a lot's of separated stores in the app. Each store can pipe selectors
 * which are memoized to avoid nasty unnecessary re computation of state derivative and re rendering.
 */
export interface Store<T extends object> {
  readonly state$: Observable<T>;

  getSnapshot(): Readonly<T>;

  commit(nextState: T);

  complete();
}

abstract class AbstractStore<T extends object> implements Store<T> {
  protected _state: BehaviorSubject<T>;

  public state$: Observable<T>;

  public getSnapshot(): Readonly<T> {
    return this._state.getValue();
  }

  protected constructor(initialState: T) {
    this._state = new BehaviorSubject(initialState);
    this.initializeOutput();
  }

  abstract commit(nextState: T);

  protected abstract initializeOutput();

  complete() {
    if (this._state && !this._state.closed) {
      this._state.complete();
      this._state = null;
    }
  }
}

class DevStore<T extends object> extends AbstractStore<T> {
  constructor(
    initialState: T,
    private options: StoreOptions = { id: 'Store', logDiff: 'none' },
  ) {
    super(initialState);
  }

  public commit(nextState: T) {
    this.state$.pipe(take(1)).subscribe((previousState) => {
      this.logDiff(previousState, nextState);
      this._state.next(nextState);
    });
  }

  private logDiff(previousState: T, nextState: T) {
    if (this.options.logDiff === 'simple') {
      storeLogger.debug(`New state in ${this.options.id}. Diff : `, [
        diff(previousState, nextState),
      ]);
    } else if (this.options.logDiff === 'detailed') {
      storeLogger.debug(`New state in ${this.options.id}`);
      const addedProps = addedDiff(previousState, nextState);
      const hasAddedProps = Object.values(addedProps).length > 0;
      const deletedProps = deletedDiff(previousState, nextState);
      const hasDeletedProps = Object.values(deletedProps).length > 0;
      const updatedProps = updatedDiff(previousState, nextState);
      const hasUpdatedProps = Object.values(updatedProps).length > 0;
      if (hasAddedProps) {
        storeLogger.debug(`Props added into ${this.options.id}: `, [addedProps]);
      }
      if (hasDeletedProps) {
        storeLogger.debug(`Props deleted into ${this.options.id}: `, [deletedProps]);
      }
      if (hasUpdatedProps) {
        storeLogger.debug(`Props updated into ${this.options.id}: `, [updatedProps]);
      }
    }
  }

  protected initializeOutput() {
    this.state$ = this._state.asObservable();
  }
}

class ProdStore<T extends object> extends AbstractStore<T> {
  constructor(initialState: T) {
    super(initialState);
  }

  public commit(nextState: T) {
    this._state.next(nextState);
  }

  protected initializeOutput() {
    this.state$ = this._state.asObservable();
  }
}

export function createStore<T extends object>(initialState: T, options?: StoreOptions): Store<T> {
  if (storeLogger.isTraceEnabled()) {
    storeLogger.trace(`Calling Store factory with initial state and options : `, [
      initialState,
      options,
    ]);
  }
  if (isDevMode()) {
    return new DevStore(initialState, options);
  } else {
    return new ProdStore(initialState);
  }
}

export function shallowClone<T extends object>(obj: T): T {
  const copy = Object.create(obj);
  Object.assign(copy, obj);
  return copy;
}

export interface StoreOptions {
  id?: string;
  logDiff?: 'none' | 'simple' | 'detailed';
}
