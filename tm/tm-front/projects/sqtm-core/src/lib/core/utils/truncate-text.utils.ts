export function truncateTextIfTooLong(text: string, maxLength: number) {
  const ellipsis = '\u2026';
  return text.length > maxLength ? text.substring(0, maxLength - 1) + ellipsis : text;
}
