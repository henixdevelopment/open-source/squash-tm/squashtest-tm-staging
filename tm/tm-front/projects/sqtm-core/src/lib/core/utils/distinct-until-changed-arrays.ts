export function arraysAreSame(arrayA: any[], arrayB: any[]) {
  let same = arrayA.length === arrayB.length;
  if (same) {
    let i = 0;
    while (i < arrayA.length && same) {
      const valueA = arrayA[i];
      const valueB = arrayB[i];
      same = valueA === valueB;
      i++;
    }
  }
  return same;
}
