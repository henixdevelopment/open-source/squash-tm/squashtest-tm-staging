import { truncateTextIfTooLong } from './truncate-text.utils';
import { MAX_TEST_CASE_NAME_LENGTH } from '../../model/test-case/test-case.model';

describe('should truncate text if too long', () => {
  it('should truncate test case name', () => {
    const nameWithMoreThan255Chars = `jeveuxunnomavecplusdedeuxcentcinquantecinqcaracteresjeveuxunnomavecplusdedeuxcentcinquantecinqcaracteresjeveuxunnomavecplusdedeuxcentcinquantecinqcaracteresjeveuxunnomavecplusdedeuxcentcinquantecinqcaracteresjeveuxunnomavecplusdedeuxcentcinquantecinqcaracteresjeveuxunnomavecplusdedeuxcentcinquantecinqcaracteresjeveuxunnomavecplusdedeuxcentcinquantecinqcaracteres`;
    const truncatedName = truncateTextIfTooLong(
      nameWithMoreThan255Chars,
      MAX_TEST_CASE_NAME_LENGTH,
    );
    expect(truncatedName).toBe(
      `jeveuxunnomavecplusdedeuxcentcinquantecinqcaracteresjeveuxunnomavecplusdedeuxcentcinquantecinqcaracteresjeveuxunnomavecplusdedeuxcentcinquantecinqcaracteresjeveuxunnomavecplusdedeuxcentcinquantecinqcaracteresjeveuxunnomavecplusdedeuxcentcinquantecinqcara…`,
    );
  });
});
