import { intervalToDuration } from 'date-fns';

export class DurationFormatUtils {
  private static dayFormat = { fr: 'j', en: 'd', es: 'd', de: 't' };
  private static hourFormat = { fr: 'h', en: 'h', es: 'h', de: 'h' };
  private static readonly MINUTE_FORMAT = 'm';
  private static readonly SECOND_FORMAT = 's';
  private static readonly MILLISECOND_FORMAT = 'ms';

  private constructor() {}

  static shortDuration(locale: string, msDuration: number): string {
    const { days, hours, minutes, seconds } = intervalToDuration({ start: 0, end: msDuration });
    const milliseconds = msDuration % 1000;

    if (days > 0) {
      return `${days}${this.dayFormat[locale]}${hours}${this.hourFormat[locale]}`;
    }
    if (hours > 0) {
      return `${hours}${this.hourFormat[locale]}${minutes}${this.MINUTE_FORMAT}`;
    }
    if (minutes > 0) {
      return `${minutes}${this.MINUTE_FORMAT}${seconds}${this.SECOND_FORMAT}`;
    }
    if (seconds > 0) {
      return `${seconds}${this.SECOND_FORMAT}${milliseconds}${this.MILLISECOND_FORMAT}`;
    }
    return `${milliseconds}${this.MILLISECOND_FORMAT}`;
  }

  static longDuration(locale: string, msDuration: number): string {
    const { days, hours, minutes, seconds } = intervalToDuration({ start: 0, end: msDuration });
    const milliseconds = msDuration % 1000;

    const parts: string[] = [];

    if (days > 0) parts.push(`${days}${this.dayFormat[locale]}`);
    if (hours > 0 || days > 0) parts.push(`${hours}${this.hourFormat[locale]}`);
    if (minutes > 0 || hours > 0 || days > 0) parts.push(`${minutes}${this.MINUTE_FORMAT}`);
    if (seconds > 0 || minutes > 0 || hours > 0 || days > 0)
      parts.push(`${seconds}${this.SECOND_FORMAT}`);
    if (milliseconds > 0 || parts.length === 0)
      parts.push(`${milliseconds}${this.MILLISECOND_FORMAT}`);

    return parts.join('');
  }
}
