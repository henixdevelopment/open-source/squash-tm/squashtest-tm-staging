import { arraysAreSame } from './distinct-until-changed-arrays';

describe('ArraysAreSame', () => {
  describe('should say if arrays are same', () => {
    interface DataType {
      arrayA: any[];
      arrayB: any[];
      same: boolean;
    }

    const dataSet: DataType[] = [
      {
        arrayA: [],
        arrayB: [],
        same: true,
      },
      {
        arrayA: [],
        arrayB: [undefined],
        same: false,
      },
      {
        arrayA: [1],
        arrayB: [1],
        same: true,
      },
      {
        arrayA: [1],
        arrayB: ['1'],
        same: false,
      },
      {
        arrayA: [1],
        arrayB: [2],
        same: false,
      },
      {
        arrayA: ['Atlantis'],
        arrayB: ['Atlantis'],
        same: true,
      },
      {
        arrayA: ['Atlantis'],
        arrayB: ['Atlantis', 'Endeavour'],
        same: false,
      },
      {
        arrayA: ['Atlantis', 'Endeavour', 'Discovery'],
        arrayB: ['Atlantis', 'Discovery', 'Endeavour'],
        same: false,
      },
      {
        arrayA: ['Atlantis', 'Endeavour', 'Discovery'],
        arrayB: ['Atlantis', 'Endeavour'],
        same: false,
      },
    ];
    dataSet.forEach((value, index) => runTest(value, index));

    function runTest(data: DataType, index: number) {
      it(`Dataset ${index} - It should says ${data.same} for ${data.arrayA} is same as ${data.arrayB}`, () => {
        expect(arraysAreSame(data.arrayA, data.arrayB)).toEqual(data.same);
      });
    }
  });
});
