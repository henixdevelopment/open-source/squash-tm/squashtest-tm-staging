import { isDevMode } from '@angular/core';

/**
 * Return the base path for static urls. aka css, lazy loaded modules and so on...
 * For main app it will just be servlet context.
 * However for plugins it should be of form <SERVLET_CONTEXT>/plugin/<PLUGIN_ID>
 */
export function getBaseLocation() {
  const basePath = document.getElementById('sqtm-app-base-href').getAttribute('href');

  if (isDevMode()) {
    console.log('SETTING BASE PATH TO ' + basePath);
  }

  return basePath;
}

/**
 * Return the backend context path. Generally equals to server servlet context even for plugins
 */
export function getBackendContext() {
  const backendContext = document
    .getElementById('sqtm-app-base-href')
    .getAttribute('data-base-backend');

  if (isDevMode()) {
    console.log('SETTING BACKEND CONTEXT TO ' + backendContext);
  }

  return backendContext;
}
