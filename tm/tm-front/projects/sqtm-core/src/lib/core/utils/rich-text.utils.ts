export function removeHtmlTags(html: string): string {
  return html.replace(/<[^>]*>/g, '');
}

export function isHtmlFilledWithDigitOrLetter(html: string): boolean {
  const text = removeHtmlTags(html);
  return /[a-zA-Z0-9]/.test(text);
}
