import { TranslateService } from '@ngx-translate/core';

/*
  This function checks if the browser language is supported by squash and choses english if not.
 */

export function getSupportedBrowserLang(translateService: TranslateService) {
  const handledAngularLocales = ['en', 'fr', 'de', 'es'];
  const browserLang = translateService.getBrowserLang();
  if (!handledAngularLocales.includes(browserLang)) {
    return 'en';
  } else {
    return browserLang;
  }
}
