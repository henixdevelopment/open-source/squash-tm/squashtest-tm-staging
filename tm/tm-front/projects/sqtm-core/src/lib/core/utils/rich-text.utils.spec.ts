import { isHtmlFilledWithDigitOrLetter } from './rich-text.utils';

describe('Should check if HTML contains letter or digit', () => {
  it('Should return true if HTML contains at least one letter', () => {
    const htmlString = '<p>a</p>';
    expect(isHtmlFilledWithDigitOrLetter(htmlString)).toBeTruthy();
  });

  it('Should return true if HTML contains at least one digit', () => {
    const htmlString = '<p>1</p>';
    expect(isHtmlFilledWithDigitOrLetter(htmlString)).toBeTruthy();
  });

  it('Should return false if HTML does not contain any letter or digit', () => {
    const htmlString = '<p>""</p>';
    expect(isHtmlFilledWithDigitOrLetter(htmlString)).toBeFalsy();
  });
});
