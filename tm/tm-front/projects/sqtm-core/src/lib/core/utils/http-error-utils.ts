import { SquashActionError, SquashError, SquashFieldError } from '../../model/error/error.model';
import { HttpErrorResponse } from '@angular/common/http';

export function extractSquashActionError(err: any): SquashActionError {
  return (err as HttpErrorResponse).error.squashTMError as SquashActionError;
}

export function extractSquashFirstFieldError(err: any): SquashFieldError {
  return (err as HttpErrorResponse).error.squashTMError as SquashFieldError;
}

export function doesHttpErrorContainsSquashActionError(err: any): boolean {
  const isSquashError = doesHttpErrorContainsSquashError(err);
  if (isSquashError) {
    const squashError: SquashError = err.error.squashTMError;
    return squashError.kind === 'ACTION_ERROR';
  } else {
    return false;
  }
}

export function doesHttpErrorContainsSquashFieldError(err: any): boolean {
  const isSquashError = doesHttpErrorContainsSquashError(err);
  if (isSquashError) {
    const squashError: SquashError = err.error.squashTMError;
    return squashError.kind === 'FIELD_VALIDATION_ERROR';
  } else {
    return false;
  }
}

function doesHttpErrorContainsSquashError(err: any): boolean {
  return err instanceof HttpErrorResponse && err.status === 412 && Boolean(err.error.squashTMError);
}
