export const NOT_ONLY_SPACES_REGEX = '(.|\\s)*\\S(.|\\s)*';

/**
 * This regex verifies the following :
 * <ul>
 *   <li>No multiple consecutive spaces</li>
 *   <li>No space directly before or after a '/'</li>
 *   <li>The string doesn't start or end with a space</li>
 *   <li>The string must be between 2 and 255 characters long</li>
 * </ul>
 */
export const VALID_PATH_REGEX =
  '^(?!.*\\s{2,})(?!.*\\s\\/)(?!.*\\/\\s)(?!^\\s)(?!.*\\s$)(?=.{2,255}$).+$';
