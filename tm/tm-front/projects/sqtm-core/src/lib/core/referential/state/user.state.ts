import { AuthenticatedUser } from '../../../model/user/authenticated-user.model';

export type UserState = AuthenticatedUser;

export type UserStateReadOnly = Readonly<UserState>;

export function initialUserState(): UserStateReadOnly {
  return {
    userId: null,
    username: null,
    admin: false,
    firstName: null,
    lastName: null,
    hasAnyReadPermission: false,
    projectManager: false,
    milestoneManager: false,
    clearanceManager: false,
    functionalTester: false,
    automationProgrammer: false,
    canDeleteFromFront: null,
  };
}
