import { TestBed } from '@angular/core/testing';

import { AdminReferentialDataService } from './admin-referential-data.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestingUtilsModule } from '../../../ui/testing-utils/testing-utils.module';

describe('AdminReferentialDataService', () => {
  let service: AdminReferentialDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TestingUtilsModule],
    });
    service = TestBed.inject(AdminReferentialDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
