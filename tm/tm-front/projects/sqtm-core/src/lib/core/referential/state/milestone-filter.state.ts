import { Milestone } from '../../../model/milestone/milestone.model';

export interface MilestoneFilterState {
  milestoneModeEnabled: boolean;
  selectedMilestoneId: number;
  stateRestored: boolean;
}

export function initialMilestoneFilterState(): Readonly<MilestoneFilterState> {
  return {
    milestoneModeEnabled: false,
    selectedMilestoneId: null,
    stateRestored: false,
  };
}

export interface MilestoneModeData {
  milestoneModeEnabled: boolean;
  milestones: Milestone[];
  milestoneFeatureEnabled: boolean;
  selectedMilestone: Milestone;
}

export function isMilestoneModeActivated(milestoneModeData: MilestoneModeData): boolean {
  return milestoneModeData.milestoneFeatureEnabled && milestoneModeData.milestoneModeEnabled;
}

export type MilestoneStateSnapshot = Pick<
  MilestoneFilterState,
  'milestoneModeEnabled' | 'selectedMilestoneId'
>;
