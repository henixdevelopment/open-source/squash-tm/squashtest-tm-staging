import { coreLogger } from '../core.logger';

export const referentialLogger = coreLogger.compose('referential');
