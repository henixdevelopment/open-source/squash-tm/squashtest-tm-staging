import { Injectable } from '@angular/core';
import { RestService } from '../../services/rest.service';
import {
  AdminReferentialDataState,
  getAdminLoadedState,
  getAdminUserState,
  getGlobalConfiguration,
  getLicenseInformation,
  initialAdminReferentialDataState,
} from '../state/admin-referential-data.state';
import { Observable } from 'rxjs';
import { AuthenticatedUser } from '../../../model/user/authenticated-user.model';
import { createSelector, select } from '@ngrx/store';
import {
  concatMap,
  distinctUntilChanged,
  filter,
  map,
  take,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import { UserState } from '../state/user.state';
import { createStore, Store } from '../../store/store';
import { CustomField } from '../../../model/customfield/customfield.model';
import { createEntityAdapter } from '@ngrx/entity';
import { UserProxyService } from '../../services/user-proxy.service';
import { BugTrackerReferentialDto } from '../../../model/bugtracker/bug-tracker.model';
import { GlobalConfigurationState } from '../state/global-configuration.state';
import { LicenseInformationState } from '../state/license-information.state';
import { AdminReferentialDataModel } from '../../../model/referential-data/admin-referential-data.model';
import { getBugTrackerState } from '../state/referential-data.state';
import { BugTrackerState } from '../state/bug-tracker-state';

@Injectable({
  providedIn: 'root',
})
export class AdminReferentialDataService {
  private store: Store<AdminReferentialDataState> = createStore(
    initialAdminReferentialDataState(),
    {
      logDiff: 'none',
      id: 'AdminReferentialDataStore',
    },
  );

  public adminReferentialData$: Observable<AdminReferentialDataState> = this.store.state$;
  public globalConfiguration$: Observable<GlobalConfigurationState>;
  public loggedAsAdmin$: Observable<boolean>;
  public loaded$: Observable<boolean>;
  public authenticatedUser$: Observable<AuthenticatedUser>;
  public milestoneFeatureEnabled$: Observable<boolean>;
  public licenseInformation$: Observable<LicenseInformationState>;
  public readonly isPremiumPluginInstalled$: Observable<boolean>;
  public readonly isUltimateLicenseAvailable$: Observable<boolean>;
  public readonly callbackUrlIsNull$: Observable<boolean>;
  public bugTrackers$: Observable<BugTrackerReferentialDto[]>;
  public searchActivationFeatureEnabled$: Observable<boolean>;
  public readonly isJwtSecretDefined$: Observable<boolean>;
  public readonly unsafeAttachmentPreviewEnabled$: Observable<boolean>;

  private customFieldAdapter = createEntityAdapter<CustomField>();
  private bugTrackersAdapter = createEntityAdapter<BugTrackerReferentialDto>();

  constructor(
    private restService: RestService,
    private userProxyService: UserProxyService,
  ) {
    this.loaded$ = this.store.state$.pipe(
      select(createSelector(getAdminLoadedState, (loaded) => loaded)),
    );

    this.authenticatedUser$ = this.store.state$.pipe(
      select(getAdminUserState),
      filter((userState) => Boolean(userState.username)),
    );

    this.loggedAsAdmin$ = this.authenticatedUser$.pipe(map((user) => user.admin));

    this.globalConfiguration$ = this.store.state$.pipe(select(getGlobalConfiguration));

    this.milestoneFeatureEnabled$ = this.globalConfiguration$.pipe(
      map((globalConfiguration) => globalConfiguration.milestoneFeatureEnabled),
    );

    this.searchActivationFeatureEnabled$ = this.globalConfiguration$.pipe(
      map((globalConfiguration) => globalConfiguration.searchActivationFeatureEnabled),
    );

    this.licenseInformation$ = this.store.state$.pipe(select(getLicenseInformation));

    this.isPremiumPluginInstalled$ = this.store.state$.pipe(
      map((state) => state.premiumPluginInstalled),
      distinctUntilChanged(),
    );

    this.isUltimateLicenseAvailable$ = this.store.state$.pipe(
      map((state) => state.ultimateLicenseAvailable),
      distinctUntilChanged(),
    );

    this.callbackUrlIsNull$ = this.store.state$.pipe(
      map((state) => state.callbackUrl == null || state.callbackUrl === ''),
    );

    this.bugTrackers$ = this.store.state$.pipe(
      select(
        createSelector(getBugTrackerState, (bugTrackerState: BugTrackerState) =>
          Object.values(bugTrackerState.entities),
        ),
      ),
    );

    this.isJwtSecretDefined$ = this.store.state$.pipe(
      map((state) => state.jwtSecretDefined),
      distinctUntilChanged(),
    );

    this.unsafeAttachmentPreviewEnabled$ = this.store.state$.pipe(
      map((state) => state.globalConfigurationState.unsafeAttachmentPreviewEnabled),
      distinctUntilChanged(),
    );
  }

  public refresh(): Observable<boolean> {
    return this.store.state$.pipe(
      take(1),
      concatMap((state: AdminReferentialDataState) => {
        return this.restService.get<AdminReferentialDataModel>(['referential/admin']).pipe(
          map((model) => {
            const userState: UserState = model.user;
            const globalConfigurationState: GlobalConfigurationState = model.globalConfiguration;
            const customFieldState = this.customFieldAdapter.setAll(
              model.customFields,
              state.customFieldState,
            );
            const bugTrackerState = this.bugTrackersAdapter.setAll(
              model.bugTrackers,
              state.bugTrackerState,
            );
            const refData: AdminReferentialDataState = {
              ...state,
              loaded: true,
              userState,
              globalConfigurationState,
              licenseInformation: model.licenseInformation,
              customFieldState,
              availableTestAutomationServerKinds: model.availableTestAutomationServerKinds,
              availableScmServerKinds: model.availableScmServerKinds,
              canManageLocalPassword: model.canManageLocalPassword,
              templateConfigurablePlugins: model.templateConfigurablePlugins,
              bugTrackerState,
              documentationLinks: model.documentationLinks,
              callbackUrl: model.callbackUrl,
              premiumPluginInstalled: model.premiumPluginInstalled,
              ultimateLicenseAvailable: model.ultimateLicenseAvailable,
              availableReportIds: model.availableReportIds,
              synchronizationPlugins: model.synchronizationPlugins,
              jwtSecretDefined: model.jwtSecretDefined,
            };

            return refData;
          }),
        );
      }),
      tap((nextState) => this.userProxyService.publishUser(nextState.userState)),
      tap((nextState) => this.store.commit(nextState)),
      map(() => true),
    );
  }

  setMilestoneFeatureEnabled(enabled: boolean): Observable<any> {
    return this.restService.post<any>(['features', 'milestones'], { enabled }).pipe(
      withLatestFrom(this.store.state$),
      map(([, state]) => ({
        ...state,
        globalConfigurationState: {
          ...state.globalConfigurationState,
          milestoneFeatureEnabled: enabled,
        },
      })),
      map((newState) => this.store.commit(newState)),
    );
  }

  setSearchActivationFeatureEnabled(enabled: boolean): Observable<any> {
    return this.restService.post<any>(['features', 'search-activation'], { enabled }).pipe(
      withLatestFrom(this.store.state$),
      map(([, state]) => ({
        ...state,
        globalConfigurationState: {
          ...state.globalConfigurationState,
          searchActivationFeatureEnabled: enabled,
        },
      })),
      map((newState) => this.store.commit(newState)),
    );
  }
}
