import { createFeatureSelector, Selector } from '@ngrx/store';
import { initialUserState, UserState } from './user.state';
import {
  GlobalConfigurationState,
  provideGlobalConfigurationInitialState,
} from './global-configuration.state';
import {
  LicenseInformationState,
  provideLicenseInformationInitialState,
} from './license-information.state';
import { CustomFieldState, initialCustomFieldState } from './custom-field-state';
import { TestAutomationServerKind } from '../../../model/test-automation/test-automation-server.model';
import { TemplateConfigurablePlugin } from '../../../model/plugin/template-configurable-plugin.model';
import { BugTrackerState, initialBugTrackerState } from './bug-tracker-state';
import { ApiDocumentationLink } from '../../../model/referential-data/api-documentation-link';
import { SynchronizationPlugin } from '../../../model/plugin/synchronization-plugin.model';
import { ScmServerKind } from '../../../model/scm-server/scm-server.model';
import { AiServer } from '../../../model/artificial-intelligence/ai-server.model';

export interface AdminReferentialDataState {
  loaded: boolean;
  userState: UserState;
  globalConfigurationState: GlobalConfigurationState;
  licenseInformation: LicenseInformationState;
  customFieldState: CustomFieldState;
  availableTestAutomationServerKinds: TestAutomationServerKind[];
  availableScmServerKinds: ScmServerKind[];
  canManageLocalPassword: boolean;
  templateConfigurablePlugins: TemplateConfigurablePlugin[];
  bugTrackerState: BugTrackerState;
  documentationLinks: ApiDocumentationLink[];
  callbackUrl: string;
  premiumPluginInstalled: boolean;
  ultimateLicenseAvailable: boolean;
  availableReportIds: string[];
  synchronizationPlugins: SynchronizationPlugin[];
  aiServers: AiServer[];
  jwtSecretDefined: boolean;
}

export type AdminReferentialDataStateReadOnly = Readonly<AdminReferentialDataState>;

export function initialAdminReferentialDataState(): AdminReferentialDataStateReadOnly {
  return {
    loaded: false,
    userState: initialUserState(),
    globalConfigurationState: provideGlobalConfigurationInitialState(),
    licenseInformation: provideLicenseInformationInitialState(),
    customFieldState: initialCustomFieldState(),
    availableTestAutomationServerKinds: [],
    availableScmServerKinds: [],
    canManageLocalPassword: false,
    templateConfigurablePlugins: [],
    bugTrackerState: initialBugTrackerState(),
    documentationLinks: [],
    callbackUrl: null,
    premiumPluginInstalled: false,
    ultimateLicenseAvailable: false,
    availableReportIds: [],
    synchronizationPlugins: [],
    aiServers: [],
    jwtSecretDefined: null,
  };
}

export const getAdminLoadedState: Selector<AdminReferentialDataState, boolean> =
  createFeatureSelector<boolean>('loaded');

export const getAdminUserState: Selector<AdminReferentialDataState, UserState> =
  createFeatureSelector<UserState>('userState');

export const getGlobalConfiguration: Selector<AdminReferentialDataState, GlobalConfigurationState> =
  createFeatureSelector<GlobalConfigurationState>('globalConfigurationState');

export const getLicenseInformation: Selector<AdminReferentialDataState, LicenseInformationState> =
  createFeatureSelector<LicenseInformationState>('licenseInformation');
export const getCallbackUrl: Selector<AdminReferentialDataState, boolean> =
  createFeatureSelector<boolean>('callbackUrl');
