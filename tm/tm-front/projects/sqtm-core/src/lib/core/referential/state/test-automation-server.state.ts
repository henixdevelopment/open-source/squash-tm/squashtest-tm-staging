import { EntityState } from '@ngrx/entity';
import { TestAutomationServer } from '../../../model/test-automation/test-automation-server.model';

export interface TestAutomationServerState extends EntityState<TestAutomationServer> {}

export type TestAutomationServerStateReadOnly = Readonly<TestAutomationServerState>;

export function initialTestAutomationServerState(): TestAutomationServerStateReadOnly {
  return {
    ids: [],
    entities: {},
  };
}
