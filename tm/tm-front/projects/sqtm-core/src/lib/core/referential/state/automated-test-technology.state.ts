import { EntityState } from '@ngrx/entity';
import { AutomatedTestTechnology } from '../../../model/automation/automated-test-technology.model';

export interface AutomatedTestTechnologyState extends EntityState<AutomatedTestTechnology> {}

export type AutomatedTestTechnologyStateReadonly = Readonly<AutomatedTestTechnologyState>;

export function initialAutomatedTestTechnologyState(): AutomatedTestTechnologyStateReadonly {
  return {
    ids: [],
    entities: {},
  };
}
