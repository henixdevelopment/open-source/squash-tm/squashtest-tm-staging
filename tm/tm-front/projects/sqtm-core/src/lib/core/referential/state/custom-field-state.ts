import { EntityState } from '@ngrx/entity';
import { CustomField } from '../../../model/customfield/customfield.model';

export interface CustomFieldState extends EntityState<CustomField> {}

export type CustomFieldStateReadonly = Readonly<CustomFieldState>;

export function initialCustomFieldState(): CustomFieldStateReadonly {
  return {
    ids: [],
    entities: {},
  };
}
