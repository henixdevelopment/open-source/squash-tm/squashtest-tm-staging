import { EntityState } from '@ngrx/entity';
import { ScmServer } from '../../../model/scm-server/scm-server.model';

export interface ScmServerState extends EntityState<ScmServer> {}

export type ScmServerStateReadOnly = Readonly<ScmServerState>;

export function initialScmServerState(): ScmServerStateReadOnly {
  return {
    ids: [],
    entities: {},
  };
}
