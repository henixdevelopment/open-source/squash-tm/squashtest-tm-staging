import { LicenseInformationModel } from '../../../model/referential-data/license-information.model';

export interface LicenseInformationState extends LicenseInformationModel {}

export function provideLicenseInformationInitialState(): Readonly<LicenseInformationState> {
  return {
    activatedUserExcess: null,
    pluginLicenseExpiration: null,
  };
}
