import { EntityState } from '@ngrx/entity';
import { Project } from '../../../model/project/project.model';

export interface ProjectState extends EntityState<Project> {
  filterActivated: boolean;
  filteredProjectSet: number[];
}

export type ProjectStateReadOnly = Readonly<ProjectState>;

export function initialProjectState(): ProjectStateReadOnly {
  return {
    ids: [],
    entities: {},
    filterActivated: false,
    filteredProjectSet: [],
  };
}
