import { EntityState } from '@ngrx/entity';
import { InfoList } from '../../../model/infolist/infolist.model';

export interface InfoListState extends EntityState<InfoList> {}

export type InfoListStateReadonly = Readonly<InfoListState>;

export function initialInfoListState(): InfoListStateReadonly {
  return {
    ids: [],
    entities: {},
  };
}
