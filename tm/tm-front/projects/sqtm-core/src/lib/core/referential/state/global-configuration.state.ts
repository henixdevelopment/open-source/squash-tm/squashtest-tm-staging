import { GlobalConfigurationModel } from '../../../model/referential-data/global-configuration.model';

export class GlobalConfigurationState extends GlobalConfigurationModel {}

export function provideGlobalConfigurationInitialState(): Readonly<GlobalConfigurationState> {
  return {
    milestoneFeatureEnabled: false,
    uploadFileExtensionWhitelist: [],
    uploadFileSizeLimit: 0,
    bannerMessage: null,
    searchActivationFeatureEnabled: false,
    unsafeAttachmentPreviewEnabled: false,
  };
}
