import { EntityState } from '@ngrx/entity';
import { RequirementVersionLinkType } from '../../../model/requirement/requirement-version-link-type.model';

export interface RequirementVersionLinkTypeState extends EntityState<RequirementVersionLinkType> {}

export function initialRequirementVersionLinkTypeState(): Readonly<RequirementVersionLinkTypeState> {
  return {
    ids: [],
    entities: {},
  };
}
