import { Directive, ElementRef, Input, OnInit } from '@angular/core';
import { ExperimentalFeatureService } from '../../core/services/experimental-feature/experimental-feature.service';
import { ExperimentalFeature } from '../../core/services/experimental-feature/experimental-feature.constants';

/**
 * This directive is used to hide elements that are part of an experimental feature that is not enabled.
 */
@Directive({
  selector: '[sqtmCoreExperimentalFeature]',
  standalone: true,
})
export class ExperimentalFeatureDirective implements OnInit {
  @Input() sqtmCoreExperimentalFeature: ExperimentalFeature;

  constructor(
    private elementRef: ElementRef,
    private experimentalFeatureService: ExperimentalFeatureService,
  ) {}

  ngOnInit() {
    const isFeatureEnabled = this.experimentalFeatureService.isEnabled(
      this.sqtmCoreExperimentalFeature,
    );

    if (!isFeatureEnabled) {
      this.elementRef.nativeElement.style.display = 'none';
    }
  }
}
