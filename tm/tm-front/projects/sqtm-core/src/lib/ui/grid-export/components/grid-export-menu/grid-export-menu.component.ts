import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { GridExportService } from '../../services/grid-export.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'sqtm-core-grid-export-menu',
  templateUrl: './grid-export-menu.component.html',
  styleUrls: ['./grid-export-menu.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GridExportMenuComponent implements OnInit {
  constructor(
    private gridExportService: GridExportService,
    private datePipe: DatePipe,
  ) {}

  @Input()
  exportContent: string;

  @Input()
  workspaceName: string;

  @Input()
  fileFormat: string;

  ngOnInit(): void {}

  exportAllVisibleRows() {
    this.gridExportService.exportAll(this.fileFormat).subscribe((resp) => this.downloadFile(resp));
  }

  exportSelectableRows() {
    this.gridExportService
      .exportSelected(this.fileFormat)
      .subscribe((resp) => this.downloadFile(resp));
  }

  private initFileName() {
    const date = new Date();
    const newDate = this.datePipe.transform(date, 'yyyyMMdd_HHmmss');
    return `${this.exportContent}-${this.workspaceName}_${newDate}`
      .replace(/\s+/g, '-')
      .toLocaleLowerCase();
  }

  private downloadFile(resp: any) {
    const contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    const blob = new Blob([resp], { type: contentType });
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = this.initFileName() + '.' + this.fileFormat;
    a.click();
    a.remove();
    window.URL.revokeObjectURL(url);
  }
}
