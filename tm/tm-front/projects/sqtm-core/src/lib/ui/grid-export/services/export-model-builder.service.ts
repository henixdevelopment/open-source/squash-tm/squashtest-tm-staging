import { Injectable, Injector } from '@angular/core';
import { GridService } from '../../grid/services/grid.service';
import { TranslateService } from '@ngx-translate/core';
import { combineLatest, Observable } from 'rxjs';
import { DataRow } from '../../grid/model/data-row.model';
import { map, take } from 'rxjs/operators';
import { select } from '@ngrx/store';
import { dataRowEntityAdapter } from '../../grid/model/state/datarow.state';
import { ColumnDisplay } from '../../grid/model/column-display.model';
import { PassThroughValueRenderer, ValueRenderer } from '../value-renderer/value-renderer';
import { GridFilter } from '../../grid/model/state/filter.state';

@Injectable()
export class ExportModelBuilderService {
  constructor(
    private grid: GridService,
    private translateService: TranslateService,
    private injector: Injector,
  ) {}

  buildModelAllRows(): Observable<ExportModel> {
    const exportedRows$ = this.allRows$();
    return this.buildModel(exportedRows$);
  }

  buildModelSelectedRows(): Observable<ExportModel> {
    const exportedRows$ = this.grid.selectedRows$;
    return this.buildModel(exportedRows$);
  }

  private allRows$(): Observable<DataRow[]> {
    return this.grid.gridState$.pipe(
      map((grisState) => grisState.dataRowState),
      select(dataRowEntityAdapter.getSelectors().selectAll),
    );
  }

  private buildModel(exportedRows$: Observable<DataRow[]>): Observable<any> {
    const exportedColumns$ = this.grid.columns$;
    return combineLatest([exportedColumns$, exportedRows$]).pipe(
      take(1),
      map(([exportedColumns, exportedRows]: [ColumnDisplay[], DataRow[]]) =>
        this.convertIntoBuildModel(exportedColumns, exportedRows),
      ),
    );
  }

  private convertIntoBuildModel(
    exportedColumns: ColumnDisplay[],
    exportedRows: DataRow[],
  ): ExportModel {
    const columnMap = exportedColumns.reduce((agg, col) => {
      agg[col.id] = col;
      return agg;
    }, {});
    const convertedRows = this.convertRows(exportedRows, columnMap);
    const convertedColumns = exportedColumns
      .filter((column) => Object.keys(exportedRows[0].data).includes(column.id.toString()))
      .map((column) => this.convertOneColumn(column));
    return { exportedColumns: convertedColumns, exportedRows: convertedRows };
  }

  private convertRows(exportedRows: DataRow[], columnMap: { [keyof: string]: ColumnDisplay }) {
    return exportedRows.map((exportedRow: DataRow) => this.convertRow(exportedRow, columnMap));
  }

  private convertRow(exportedRow: DataRow, columnMap: { [p: string]: ColumnDisplay }) {
    const data = Object.entries(exportedRow.data).reduce((convertedData, [key, value]) => {
      const column: ColumnDisplay = columnMap[key];
      let valueRenderer: ValueRenderer = this.injector.get(PassThroughValueRenderer);
      if (column && column.exportValueRenderer) {
        valueRenderer = this.injector.get(column.exportValueRenderer);
      }
      convertedData[key] = valueRenderer.render(value, exportedRow.data);
      return convertedData;
    }, {});
    return { ...exportedRow, data };
  }

  private convertOneColumn(column: ColumnDisplay) {
    return {
      id: column.id,
      label: this.findLabel(column),
    };
  }

  private findLabel(column: ColumnDisplay & { filter?: GridFilter }) {
    let label;
    if (column.label) {
      label = column.label;
    } else if (column.titleI18nKey) {
      label = this.translateService.instant(column.titleI18nKey);
    } else if (column.i18nKey) {
      label = this.translateService.instant(column.i18nKey);
    } else {
      label = column.id;
    }
    return label;
  }
}

export interface ExportModel {
  exportedRows: DataRow[];
  exportedColumns: ExportColumnDefinition[];
}

export type ExportColumnDefinition = Pick<ColumnDisplay, 'id'> & {
  label: string;
};
