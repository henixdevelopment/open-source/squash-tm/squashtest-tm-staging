import { Injectable } from '@angular/core';
import { RestService } from '../../../core/services/rest.service';
import { concatMap, take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ExportModel, ExportModelBuilderService } from './export-model-builder.service';

/**
 * This service is only responsible for downloading file. It is not really testable, hence the segregation
 * with the builder.
 *
 * For usage don't forget to provide the two services along the grid service itself.
 */
@Injectable()
export class GridExportService {
  constructor(
    private restService: RestService,
    private exportModelBuilder: ExportModelBuilderService,
  ) {}

  exportAll(fileFormat: string): Observable<any> {
    const exportModel$ = this.exportModelBuilder.buildModelAllRows();
    return this.doExport(exportModel$, fileFormat);
  }

  exportSelected(fileFormat: string): Observable<any> {
    const exportModel$ = this.exportModelBuilder.buildModelSelectedRows();
    return this.doExport(exportModel$, fileFormat);
  }

  private doExport(exportModel$: Observable<ExportModel>, fileFormat: string): Observable<any> {
    return exportModel$.pipe(
      take(1),
      concatMap((exportModel: ExportModel) => this.fetchExportedFile(exportModel, fileFormat)),
    );
  }

  private fetchExportedFile(exportModel: ExportModel, fileFormat: string): Observable<any> {
    return this.restService.post(['grid-export', fileFormat], exportModel, {
      responseType: 'blob',
    });
  }
}
