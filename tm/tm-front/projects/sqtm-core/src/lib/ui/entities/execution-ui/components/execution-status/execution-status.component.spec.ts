import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ExecutionStatusComponent } from './execution-status.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { TestingUtilsModule } from '../../../../testing-utils/testing-utils.module';

describe('ExecutionStatusComponent', () => {
  let component: ExecutionStatusComponent;
  let fixture: ComponentFixture<ExecutionStatusComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ExecutionStatusComponent],
      imports: [TranslateModule.forRoot(), TestingUtilsModule],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionStatusComponent);
    component = fixture.componentInstance;
    component.status = 'SUCCESS';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
