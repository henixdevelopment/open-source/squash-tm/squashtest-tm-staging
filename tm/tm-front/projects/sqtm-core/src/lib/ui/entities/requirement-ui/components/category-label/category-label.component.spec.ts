import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CategoryLabelComponent } from './category-label.component';
import { TranslateModule } from '@ngx-translate/core';
import { TestingUtilsModule } from '../../../../testing-utils/testing-utils.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('CategoryLabelComponent', () => {
  let component: CategoryLabelComponent;
  let fixture: ComponentFixture<CategoryLabelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), TestingUtilsModule, HttpClientTestingModule],
      declarations: [CategoryLabelComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
