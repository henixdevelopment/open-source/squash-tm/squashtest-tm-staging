import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import {
  LevelEnumItem,
  TestCaseExecutionModeKeys,
} from '../../../../../model/level-enums/level-enum';

@Component({
  selector: 'sqtm-core-execution-mode',
  template: `
    @if (executionMode) {
      <div nz-tooltip [nzTooltipTitle]="executionMode.i18nKey | translate">
        @switch (executionMode.id) {
          @case ('EXPLORATORY') {
            <i nz-icon nzType="sqtm-core-test-case:exploratory" nzTheme="outline"></i>
          }
          @case ('AUTOMATED') {
            <i nz-icon nzType="sqtm-core-nav:automation-workspace" nzTheme="outline"></i>
          }
          @case ('MANUAL') {
            <i nz-icon nzType="sqtm-core-administration:user" nzTheme="outline"></i>
          }
        }
      </div>
    } @else {
      <span>-</span>
    }
  `,
  styleUrls: ['./execution-mode.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExecutionModeComponent {
  @Input()
  executionMode: LevelEnumItem<TestCaseExecutionModeKeys>;

  constructor() {}
}
