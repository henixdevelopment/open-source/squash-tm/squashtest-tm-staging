import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CustomFieldWidgetComponent } from './custom-field-widget.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { EntityViewService } from '../../../core/services/entity-view/entity-view.service';
import { InputType } from '../../../model/customfield/input-type.model';
import { WorkspaceCommonModule } from '../../workspace-common/workspace-common.module';
import { GenericEntityViewService } from '../../../core/services/genric-entity-view/generic-entity-view.service';

describe('CustomFieldWidgetComponent', () => {
  let component: CustomFieldWidgetComponent;
  let fixture: ComponentFixture<CustomFieldWidgetComponent>;
  const entityViewService = jasmine.createSpyObj('EntityViewService', ['instant']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), WorkspaceCommonModule],
      declarations: [CustomFieldWidgetComponent],
      providers: [
        {
          provide: EntityViewService,
          useValue: entityViewService,
        },
        {
          provide: GenericEntityViewService,
          useExisting: entityViewService,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomFieldWidgetComponent);
    component = fixture.componentInstance;
    component.customField = {
      name: 'cuf',
      inputType: InputType.PLAIN_TEXT,
      optional: false,
      id: 1,
      code: 'CODE',
      label: 'cuf',
      options: [],
      boundProjectsToCuf: [],
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
