import { Directive, Host, HostListener, Input, OnDestroy, OnInit } from '@angular/core';
import { CustomFieldWidgetComponent } from '../custom-field-widget/custom-field-widget.component';
import {
  createCustomFieldValueSelector,
  SqtmEntityState,
} from '../../../core/services/entity-view/entity-view.state';
import { SimplePermissions } from '../../../model/permissions/simple-permissions';
import { EntityViewService } from '../../../core/services/entity-view/entity-view.service';
import { CustomFieldData } from '../../../model/customfield/customfield.model';
import { catchError, filter, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { select } from '@ngrx/store';
import { ClosedSprintLockService } from '../../../core/services/sprint/closed-sprint-lock.service';
import { ActionErrorDisplayService } from '../../../core/services/errors-handling/action-error-display.service';

@Directive({
  selector: '[sqtmCoreEntityCustomField]',
})
export class EntityCustomFieldDirective<
    E extends SqtmEntityState,
    T extends string,
    P extends SimplePermissions,
  >
  implements OnInit, OnDestroy
{
  @Input('sqtmCoreEntityCustomField')
  cufData: CustomFieldData;

  private unsub$ = new Subject<void>();

  constructor(
    private entityViewService: EntityViewService<E, T, P>,
    @Host() private widget: CustomFieldWidgetComponent,
    private readonly closedSprintLockService: ClosedSprintLockService,
    private readonly actionErrorDisplayService: ActionErrorDisplayService,
  ) {}

  ngOnInit(): void {
    this.widget.customField = this.cufData;
    this.entityViewService.componentData$
      .pipe(
        takeUntil(this.unsub$),
        select(createCustomFieldValueSelector(this.cufData.cfvId)),
        filter((cfv) => cfv != null),
      )
      .subscribe((cfv) => {
        this.widget.value = cfv.value;
      });

    this.entityViewService.componentData$
      .pipe(takeUntil(this.unsub$))
      .subscribe((componentData) => {
        this.widget.editable =
          componentData.permissions.canWrite &&
          componentData.milestonesAllowModification &&
          componentData.statusAllowModification &&
          !this.closedSprintLockService.isSprintClosed;
      });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  @HostListener('confirmEvent', ['$event'])
  onConfirm(value: string) {
    this.widget.beginAsync();
    this.entityViewService
      .updateCustomFieldValue(this.cufData.cfvId, value as any) // cannot use type system on dynamic inputs...
      .pipe(catchError((error) => this.actionErrorDisplayService.handleActionError(error)))
      .subscribe();
  }
}
