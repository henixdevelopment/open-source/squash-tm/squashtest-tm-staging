import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CloseButtonComponent } from './close-button.component';
import { WorkspaceDirective } from '../../../ui-manager/workspace.directive';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Router } from '@angular/router';

describe('CloseButtonComponent', () => {
  let component: CloseButtonComponent;
  let fixture: ComponentFixture<CloseButtonComponent>;

  const workspaceDirective = jasmine.createSpyObj(['ngOnInit']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: Router, useValue: {} },
        {
          provide: WorkspaceDirective,
          useValue: workspaceDirective,
        },
      ],
      declarations: [CloseButtonComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloseButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
