import { wsLayoutLogger } from './workspace.layout.logger';
import { ElementRef, Renderer2 } from '@angular/core';
import { fromEvent, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { LocalPersistenceService } from '../../core/services/local-persistence.service';

const delegateLogger = wsLayoutLogger.compose('LayoutDelegate');

export interface WorkspaceLikeComponent {
  renderer: Renderer2;
  workspace: ElementRef<HTMLDivElement>;
  contextualContent: ElementRef<HTMLDivElement>;
  gridReference: ElementRef<HTMLDivElement>;
  resizeHandle: ElementRef<HTMLDivElement>;
  unsub$: Observable<void>;
}

/**
 * Delegate for DOM operations.
 * Allows to reduce duplicate code between WorkspaceWithTreeComponent and WorkspaceWithGridComponent
 * without inheritance between components (loose coupling).
 */
export class ResizableWorkspaceLayoutDelegate {
  private readonly persistenceKey = 'workspace-layout';

  private contextualContentWidth = 0;
  private maxContextualContentWidth = 0;

  private isGridVisible = true;
  private isContextualViewVisible = false;

  public readonly MAX_NAV_BAR_WIDTH = 160;
  public readonly RESIZE_HANDLER_WIDTH = 5;
  private readonly MIN_CONTEXTUAL_CONTENT_WIDTH = 600;
  private readonly MIN_TREE_WIDTH = 340;

  constructor(
    public readonly ws: WorkspaceLikeComponent,
    private localPersistenceService: LocalPersistenceService,
  ) {}

  get renderer(): Renderer2 {
    return this.ws.renderer;
  }

  get workspace(): ElementRef<HTMLDivElement> {
    return this.ws.workspace;
  }

  get contextualContent(): ElementRef<HTMLDivElement> {
    return this.ws.contextualContent;
  }

  get gridReference(): ElementRef<HTMLDivElement> {
    return this.ws.gridReference;
  }

  get resizeHandle(): ElementRef<HTMLDivElement> {
    return this.ws.resizeHandle;
  }

  get unsub$(): Observable<void> {
    return this.ws.unsub$;
  }

  public applyMinimalSizes(): void {
    this.ws.renderer.setStyle(
      this.contextualContent.nativeElement,
      'min-width',
      this.MIN_CONTEXTUAL_CONTENT_WIDTH + 'px',
    );
  }

  public handleResize(widthDelta: number): void {
    this.contextualContentWidth = this.contextualContentWidth - widthDelta;
    this.resizeContextualContent();
    this.persistLayout();
  }

  public initializeWidthBoundary(): void {
    this.localPersistenceService
      .get<PersistedLayoutState>(this.persistenceKey)
      .subscribe((persistedState) => {
        if (this.workspace) {
          const desiredContextualWidth =
            persistedState?.contextualContentWidth ||
            this.workspace.nativeElement.clientWidth * 0.7;
          this.contextualContentWidth = Math.max(
            desiredContextualWidth,
            this.MIN_CONTEXTUAL_CONTENT_WIDTH,
          );
          this.maxContextualContentWidth =
            this.workspace.nativeElement.clientWidth -
            this.MAX_NAV_BAR_WIDTH -
            this.RESIZE_HANDLER_WIDTH -
            this.MIN_TREE_WIDTH;
          delegateLogger.debug(
            `Setting contextual content width to ${this.contextualContentWidth}`,
          );
          this.resizeContextualContent();
        }
      });
  }

  public reactToWindowResized(): void {
    fromEvent(window, 'resize')
      .pipe(takeUntil(this.unsub$))
      .subscribe(() => {
        this.initializeWidthBoundary();
        this.resizeContextualContent();
      });
  }

  public resizeContextualContent(): void {
    if (this.contextualContentWidth < this.MIN_CONTEXTUAL_CONTENT_WIDTH) {
      this.contextualContentWidth = this.MIN_CONTEXTUAL_CONTENT_WIDTH;
    } else if (this.contextualContentWidth > this.maxContextualContentWidth) {
      this.contextualContentWidth = this.maxContextualContentWidth;
    }

    if (this.gridReference) {
      this.renderer.setStyle(
        this.contextualContent.nativeElement,
        'width',
        `${this.contextualContentWidth}px`,
      );
    }
  }

  private persistLayout() {
    this.localPersistenceService
      .set<PersistedLayoutState>(this.persistenceKey, {
        contextualContentWidth: this.contextualContentWidth,
      })
      .subscribe();
  }

  public showContextualContent(): void {
    this.setContextualContentVisible(true);
    this.renderer.setStyle(this.gridReference.nativeElement, 'flex-grow', '1');
  }

  public hideContextualContent(): void {
    this.setContextualContentVisible(false);
  }

  public foldTree(): void {
    this.hideGrid();
    this.renderer.setStyle(this.contextualContent.nativeElement, 'flex-grow', 1);
  }

  public unfoldTree(): void {
    this.showGrid();
    this.resizeContextualContent();
  }

  private hideGrid(): void {
    this.isGridVisible = false;
    this.renderer.setStyle(this.gridReference.nativeElement, 'display', 'none');
    this.refreshResizeHandleVisibility();
  }

  private showGrid(): void {
    this.isGridVisible = true;
    this.renderer.setStyle(this.gridReference.nativeElement, 'display', 'block');
    this.refreshResizeHandleVisibility();
  }

  private setContextualContentVisible(shouldBeVisible: boolean): void {
    this.isContextualViewVisible = shouldBeVisible;
    this.renderer.setStyle(
      this.contextualContent.nativeElement,
      'display',
      shouldBeVisible ? 'block' : 'none',
    );
    this.refreshResizeHandleVisibility();
  }

  private refreshResizeHandleVisibility(): void {
    const shouldBeVisible = this.isContextualViewVisible && this.isGridVisible;
    if (shouldBeVisible) {
      this.showResizeHandle();
    } else {
      this.hideResizeHandle();
    }
  }

  private showResizeHandle(): void {
    this.renderer.setStyle(this.resizeHandle.nativeElement, 'display', 'block');
  }

  private hideResizeHandle(): void {
    this.renderer.setStyle(this.resizeHandle.nativeElement, 'display', 'none');
  }
}

interface PersistedLayoutState {
  contextualContentWidth: number;
}
