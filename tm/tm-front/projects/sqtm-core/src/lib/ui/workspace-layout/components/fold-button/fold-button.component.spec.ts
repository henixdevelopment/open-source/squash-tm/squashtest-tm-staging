import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FoldButtonComponent } from './fold-button.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { WorkspaceDirective } from '../../../ui-manager/workspace.directive';
import { Router } from '@angular/router';

describe('FoldButtonComponent', () => {
  let component: FoldButtonComponent;
  let fixture: ComponentFixture<FoldButtonComponent>;

  const workspaceDirective = jasmine.createSpyObj(['ngOnInit']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: Router, useValue: {} },
        {
          provide: WorkspaceDirective,
          useValue: workspaceDirective,
        },
      ],
      declarations: [FoldButtonComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoldButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
