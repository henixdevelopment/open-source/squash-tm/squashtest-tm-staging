import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { UiManagerService } from '../../../ui-manager/ui-manager.service';
import { WorkspaceDirective } from '../../../ui-manager/workspace.directive';
import { Observable, Subject } from 'rxjs';
import { distinctUntilChanged, filter, takeUntil } from 'rxjs/operators';
import { WorkspaceLayoutState } from '../../../ui-manager/ui-manager-state';

@Component({
  selector: 'sqtm-core-fold-button',
  templateUrl: './fold-button.component.html',
  styleUrls: ['./fold-button.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FoldButtonComponent implements OnInit, OnDestroy {
  private unsub$ = new Subject<void>();
  workspaceLayout$: Observable<WorkspaceLayoutState>;
  toggleButtonTooltipIsVisible: boolean;

  constructor(
    private uiManager: UiManagerService,
    private workspaceDirective: WorkspaceDirective,
  ) {
    this.workspaceLayout$ = this.uiManager
      .createWorkspaceLayoutObservable(this.workspaceDirective.workspaceName)
      .pipe(
        takeUntil(this.unsub$),
        filter((workspaceLayoutState) => Boolean(workspaceLayoutState)),
        distinctUntilChanged(),
      );
  }

  ngOnInit() {}

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  getIcon(folded: boolean) {
    return folded ? 'sqtm-core-generic:show' : 'sqtm-core-generic:hide';
  }

  toggleTree() {
    this.uiManager.toggleTree(this.workspaceDirective.workspaceName);
    this.toggleButtonTooltipIsVisible = false;
  }

  getTitleKey(folded: boolean): string {
    return folded ? 'sqtm-core.generic.label.collapse' : 'sqtm-core.generic.label.expand';
  }
}
