import { Directive, ElementRef, Host, HostListener, Inject } from '@angular/core';
import { wsLayoutLogger } from '../workspace.layout.logger';
import { DOCUMENT } from '@angular/common';
import { GridService } from '../../grid/services/grid.service';
import { filter, take, withLatestFrom } from 'rxjs/operators';
import { ReferentialDataService } from '../../../core/referential/services/referential-data.service';
import { KeyNames } from '../../utils/key-names';

const logger = wsLayoutLogger.compose('TreeKeyboardShortcutDirective');

/** @dynamic */
@Directive({
  selector: '[sqtmCoreTreeKeyboardShortcut]',
})
export class TreeKeyboardShortcutDirective {
  constructor(
    @Host() private host: ElementRef,
    @Inject(DOCUMENT) private document: Document,
    private grid: GridService,
    private referentialDataService: ReferentialDataService,
  ) {}

  @HostListener('document:keyup', ['$event'])
  clicked(event: KeyboardEvent) {
    const activeElement = this.document.activeElement;
    if (activeElement === this.document.body) {
      this.handleBodyEvent(event);
    }
  }

  private handleBodyEvent(event: KeyboardEvent) {
    logger.debug('Handling keyboard stroke in TreeKeyboardShortcutDirective : ', [event]);
    if (event.ctrlKey || event.metaKey) {
      this.handleCtrlKeyPressedEvents(event);
    } else {
      this.handleSimpleKeyEvents(event);
    }
  }

  private handleCtrlKeyPressedEvents(event: KeyboardEvent) {
    switch (event.key) {
      case 'c':
        this.copyNodes();
        break;
      case 'v':
        this.pasteNodes();
        break;
    }
  }

  private copyNodes() {
    logger.debug('Keyboard copy shortcut activated');
    this.grid.canCopy$
      .pipe(
        take(1),
        filter((canCopy) => canCopy),
      )
      .subscribe(() => {
        this.grid.copy();
      });
  }

  private pasteNodes() {
    logger.debug('Keyboard paste shortcut activated');
    this.grid.canPaste$
      .pipe(
        take(1),
        filter((canPaste) => canPaste),
      )
      .subscribe(() => {
        this.grid.paste();
      });
  }

  private handleSimpleKeyEvents(event: KeyboardEvent) {
    if (event.key === KeyNames.DELETE) {
      this.grid.canDelete$
        .pipe(
          take(1),
          filter((canDelete) => canDelete),
          withLatestFrom(
            this.referentialDataService.canDeleteFromFront$,
            this.referentialDataService.isAdmin$,
          ),
          filter(([_, canDeleteFromFront, isAdmin]) => canDeleteFromFront || isAdmin),
        )
        .subscribe(() => {
          this.grid.deleteRows();
        });
    }
  }
}
