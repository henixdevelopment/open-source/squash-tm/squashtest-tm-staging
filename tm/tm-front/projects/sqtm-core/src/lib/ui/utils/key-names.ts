export enum KeyNames {
  DELETE = 'Delete',
  ENTER = 'Enter',
  ESCAPE = 'Escape',
  TAB = 'Tab',
}
