import { KeyCodes } from './key-codes';

export function isEnterKeyboardEvent(event: KeyboardEvent) {
  // MS Edge doesn't support key, so we rely on deprecated keyCode.
  return event.keyCode === KeyCodes.ENTER;
}

export function isEscapeKeyboardEvent(event: KeyboardEvent) {
  // MS Edge doesn't support key, so we rely on deprecated keyCode.
  return event.keyCode === KeyCodes.ESC;
}
