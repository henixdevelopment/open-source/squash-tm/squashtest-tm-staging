import { CKEditor4 } from 'ckeditor4-angular';

export function createCkEditorConfig(browserLang: string, height?: number): CKEditor4.Config {
  return {
    language: browserLang,

    toolbar: 'Squash',
    skin: 'moono-lisa',
    toolbarCanCollapse: true,
    toolbar_Squash: [
      ['Bold', 'Italic', 'Underline', 'Strike', 'NumberedList', 'BulletedList'],
      ['Link'],
      ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
      ['TextColor'],
      ['Font'],
      ['FontSize'],
      ['Scayt'],
      ['Table', 'Image'],
      ['Maximize'],
    ],
    height: height,
    resize_minHeight: 175,
    resize_minWidth: 2001,
    removePlugins: 'elementspath, flash, image',
    extraPlugins: 'onchange,autogrow,uploadimage',
    filebrowserImageUploadUrl: 'upload',
    uploadUrl: 'upload',
    clipboard_handleImages: false,
    autoGrow_minHeight: 200,
    autoGrow_maxHeight: 500,
  };
}

// Static flag to avoid adding multiple listeners.
let dialogDefinitionListenerAdded = false;

export function overrideCKEditorDefaultLinkTarget() {
  if (dialogDefinitionListenerAdded) {
    return;
  }
  const ckeSingleton = window['CKEDITOR'];

  if (ckeSingleton) {
    ckeSingleton.on('dialogDefinition', handleDialogDefinition);
    dialogDefinitionListenerAdded = true;
  } else {
    console.error('Could not access CKEDITOR singleton.');
  }
}

function handleDialogDefinition(ev: any) {
  try {
    const dialogName = ev.data.name;
    const dialogDefinition = ev.data.definition;
    dialogDefinition.minWidth = 375;
    dialogDefinition.minHeight = 250;
    if (dialogName === 'link') {
      const informationTab = dialogDefinition.getContents('target');
      const targetField = informationTab.get('linkTargetType');
      targetField['default'] = '_blank';
    }
  } catch (exception) {
    console.error('Error while preparing CKEditor : ' + ev.message);
  }
}

export function isAllowedImageType(file: File) {
  const acceptedImageTypes = [
    'image/jpg',
    'image/jpeg',
    'image/png',
    'image/gif',
    'image/svg',
    'image/bmp',
  ];
  return acceptedImageTypes.includes(file['type']);
}

export function closeCkEditorDialog() {
  const currentDialog = window['CKEDITOR'].dialog.getCurrent();
  if (currentDialog !== null) {
    currentDialog.hide();
  }
}

export function hideNotification(notification: any) {
  setTimeout(() => {
    if (notification) {
      notification.hide();
    }
  }, 1000);
}
