import { TranslateService } from '@ngx-translate/core';
import { IssueReportSite } from '../../model/issue/issue-report-site.model';

export function getIssueReportSiteAsString(
  reportSite: IssueReportSite,
  translateService: TranslateService,
): string {
  const { executionOrder, executionName, suiteNames } = reportSite;

  const oneBasedOrder = executionOrder + 1;

  if (suiteNames.length > 0) {
    const testSuite = translateService
      .instant('sqtm-core.entity.test-suite.label.singular')
      .toLowerCase();
    return `${executionName} (${testSuite} '${suiteNames}', exe. #${oneBasedOrder})`;
  } else {
    return `${executionName} (exe. #${oneBasedOrder})`;
  }
}
