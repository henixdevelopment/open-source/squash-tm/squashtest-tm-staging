import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogReference } from '../../../dialog/model/dialog-reference';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TestingUtilsModule } from '../../../testing-utils/testing-utils.module';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { IssueReportSourceStepsDialogComponent } from './issue-report-source-steps-dialog.component';

describe('IssueReportSourceStepsDialogComponent', () => {
  let component: IssueReportSourceStepsDialogComponent;
  let fixture: ComponentFixture<IssueReportSourceStepsDialogComponent>;

  beforeEach(async () => {
    const dialogRef: any = jasmine.createSpyObj<DialogReference>(['close']);
    dialogRef.data = {
      executionSteps: [],
    };

    await TestBed.configureTestingModule({
      imports: [TestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule],
      declarations: [IssueReportSourceStepsDialogComponent],
      providers: [{ provide: DialogReference, useValue: dialogRef }],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IssueReportSourceStepsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
