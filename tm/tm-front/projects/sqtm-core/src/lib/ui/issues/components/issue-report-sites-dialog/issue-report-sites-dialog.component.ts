import { ChangeDetectionStrategy, Component, ViewContainerRef } from '@angular/core';
import { DialogReference } from '../../../dialog/model/dialog-reference';
import { DialogConfiguration } from '../../../dialog/services/dialog-feature.state';
import { IssueReportSite } from '../../../../model/issue/issue-report-site.model';
import { TranslateService } from '@ngx-translate/core';
import { getIssueReportSiteAsString } from '../../../utils/issue-report-site-utils';

const DIALOG_ID = 'issue-declaration-sites-dialog';

@Component({
  selector: 'sqtm-core-issue-report-sites-dialog',
  templateUrl: './issue-report-sites-dialog.component.html',
  styleUrls: ['./issue-report-sites-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IssueReportSitesDialogComponent {
  readonly dialogId = DIALOG_ID;
  readonly data: DialogData;

  constructor(
    public readonly dialogRef: DialogReference<DialogData>,
    public readonly translateService: TranslateService,
  ) {
    this.data = dialogRef.data;
  }

  getLabel(site: IssueReportSite): string {
    return getIssueReportSiteAsString(site, this.translateService);
  }

  getExecutionPath(issueReportSite: IssueReportSite): any[] {
    return ['/', 'execution', issueReportSite.executionId];
  }

  closeDialog(): void {
    this.dialogRef.close();
  }
}

interface DialogData {
  reportSites: IssueReportSite[];
}

export function getIssueReportSitesDialogConf(
  reportSites: IssueReportSite[],
  viewContainerRef: ViewContainerRef,
): DialogConfiguration<DialogData> {
  return {
    id: DIALOG_ID,
    data: { reportSites },
    component: IssueReportSitesDialogComponent,
    viewContainerReference: viewContainerRef,
  };
}
