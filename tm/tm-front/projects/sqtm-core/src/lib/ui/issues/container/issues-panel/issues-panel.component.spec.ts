import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { IssuesPanelComponent } from './issues-panel.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { IssuesService } from '../../service/issues.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestingUtilsModule } from '../../../testing-utils/testing-utils.module';
import { DialogService } from '../../../dialog/services/dialog.service';

describe('IssuesPanelComponent', () => {
  let component: IssuesPanelComponent;
  let fixture: ComponentFixture<IssuesPanelComponent>;
  const dialogService = jasmine.createSpyObj('dialogService', ['create']);
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [IssuesPanelComponent],
      imports: [TestingUtilsModule, HttpClientTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [IssuesService, { provide: DialogService, useValue: dialogService }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssuesPanelComponent);
    component = fixture.componentInstance;
    component.entityType = 'TEST_CASE_TYPE';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
