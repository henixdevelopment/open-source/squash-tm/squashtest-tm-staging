import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { DataRowModel } from '../../../../model/grids/data-row.model';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'sqtm-core-issue-list-compact',
  templateUrl: './issue-list-compact.component.html',
  styleUrls: ['./issue-list-compact.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IssueListCompactComponent {
  private readonly DATA_TYPE_LABELS: Record<string, string> = {
    btProject: 'sqtm-core.entity.issue.project.label',
    summary: 'sqtm-core.entity.issue.summary.label',
    priority: 'sqtm-core.entity.issue.priority.label',
    status: 'sqtm-core.entity.issue.status.label',
  };

  @Input()
  issues: DataRowModel[];

  @Input()
  canDelete: boolean;

  @Output()
  issuesDeleted = new EventEmitter<number[]>();

  constructor(private translateService: TranslateService) {}

  deleteIssue(issueIds: number[]) {
    this.issuesDeleted.emit(issueIds);
  }

  getTooltip(dataType: string, data: string): string {
    const labelKey = this.DATA_TYPE_LABELS[dataType];
    return labelKey ? `${this.translateService.instant(labelKey)}: ${data}` : data;
  }
}
