import { AuthenticationProtocol } from '../../../../model/third-party-server/authentication.model';
import { BugTracker } from '../../../../model/bugtracker/bug-tracker.model';

export interface BugTrackerConnectInfo {
  id: number;
  authProtocol: AuthenticationProtocol;
}

export class BugtrackerConnectConfiguration {
  bugTracker: BugTracker;
}
