import { ChangeDetectionStrategy, Component, ViewContainerRef } from '@angular/core';
import { DialogReference } from '../../../dialog/model/dialog-reference';
import { DialogConfiguration } from '../../../dialog/services/dialog-feature.state';
import { TranslateService } from '@ngx-translate/core';

const DIALOG_ID = 'issue-report-source-steps-dialog';

@Component({
  selector: 'sqtm-core-issue-report-source-steps-dialog',
  templateUrl: './issue-report-source-steps-dialog.component.html',
  styleUrls: ['./issue-report-source-steps-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IssueReportSourceStepsDialogComponent {
  readonly dialogId = DIALOG_ID;
  readonly data: DialogData;

  constructor(
    public readonly dialogRef: DialogReference<DialogData>,
    public readonly translateService: TranslateService,
  ) {
    this.data = dialogRef.data;
  }

  getLabel(stepOrder: number): string {
    if (stepOrder >= 0) {
      const oneBasedItemOrder = stepOrder + 1;
      const itemLabelKey = this.data.isExploratoryExecution
        ? 'sqtm-core.campaign-workspace.execution-page.note.label'
        : 'sqtm-core.campaign-workspace.execution-page.step.label';

      const itemLabel = this.translateService.instant(itemLabelKey);
      return `${itemLabel} ${oneBasedItemOrder}`;
    } else {
      return this.translateService.instant(
        'sqtm-core.campaign-workspace.execution-page.this-execution.label',
      );
    }
  }

  closeDialog(): void {
    this.dialogRef.close();
  }
}

interface DialogData {
  executionSteps: number[];
  isExploratoryExecution: boolean;
}

export function getIssueReportSourceStepsDialogConf(
  executionSteps: number[],
  isExploratoryExecution: boolean,
  viewContainerRef: ViewContainerRef,
): DialogConfiguration<DialogData> {
  return {
    id: DIALOG_ID,
    data: { executionSteps, isExploratoryExecution },
    component: IssueReportSourceStepsDialogComponent,
    viewContainerReference: viewContainerRef,
    width: 400,
    minWidth: 400,
  };
}
