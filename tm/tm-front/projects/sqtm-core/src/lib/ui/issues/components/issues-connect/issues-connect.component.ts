import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  ViewContainerRef,
} from '@angular/core';
import { BugtrackerConnectDialogComponent } from '../bugtracker-connect-dialog/bugtracker-connect-dialog.component';
import { Subject, throwError } from 'rxjs';
import { BugTracker } from '../../../../model/bugtracker/bug-tracker.model';
import { DialogService } from '../../../dialog/services/dialog.service';
import { AuthenticationProtocol } from '../../../../model/third-party-server/authentication.model';
import { IssuesService } from '../../service/issues.service';
import {
  InterWindowCommunicationService,
  InterWindowMessages,
} from '../../../../core/services/inter-window-communication.service';
import { catchError, filter, switchMap, take, takeUntil } from 'rxjs/operators';
import { BugtrackerConnectConfiguration } from '../bugtracker-connect-dialog/bugtracker-connect-configuration';

const OAUTH2_ENDPOINT = 'oauth2';

@Component({
  selector: 'sqtm-core-issues-connect',
  templateUrl: 'issues-connect.component.html',
  styleUrls: ['./issues-connect.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IssuesConnectComponent implements OnDestroy {
  @Input()
  bugTracker: BugTracker;

  @Output()
  connectSuccess = new EventEmitter<void>();

  unsub$ = new Subject<void>();

  constructor(
    private readonly dialogService: DialogService,
    public readonly issuesService: IssuesService,
    private readonly vcr: ViewContainerRef,
    private readonly interWindowCommunicationService: InterWindowCommunicationService,
  ) {}

  openDialog() {
    switch (this.bugTracker.authProtocol) {
      case AuthenticationProtocol.OAUTH_2:
        this.openOAuthDialog(OAUTH2_ENDPOINT);
        break;
      case AuthenticationProtocol.BASIC_AUTH:
        this.openStandardAuthDialog();
        break;
      case AuthenticationProtocol.TOKEN_AUTH:
        this.openStandardAuthDialog();
        break;
      default:
        throw new Error(`This protocol is not handle : ${this.bugTracker.authProtocol}`);
    }
  }

  private openStandardAuthDialog(): void {
    const dialogRef = this.dialogService.openDialog<BugtrackerConnectConfiguration, boolean>({
      id: BugtrackerConnectDialogComponent.DIALOG_ID,
      viewContainerReference: this.vcr,
      component: BugtrackerConnectDialogComponent,
      data: { bugTracker: this.bugTracker },
    });

    dialogRef.dialogClosed$
      .pipe(
        takeUntil(this.unsub$),
        filter((result) => Boolean(result)),
      )
      .subscribe(() => {
        this.connectSuccess.emit();
      });
  }

  private openOAuthDialog(protocol: string): void {
    const currentOpenedWindow = this.issuesService.openOAuthDialog(this.bugTracker.id, protocol);
    this.handleOauth2Connexion(currentOpenedWindow);
  }

  private handleOauth2Connexion(currentOpenedWindow: Window) {
    this.interWindowCommunicationService.interWindowMessages$
      .pipe(
        takeUntil(this.unsub$),
        filter((message) => message.isTypeOf('OAUTH2-SECRET-CODE-RETRIEVED')),
        take(1),
        switchMap((message) =>
          this.issuesService.askOauth2TokenToBackend(this.bugTracker.id, message.payload.code),
        ),
        catchError((err) => {
          this.interWindowCommunicationService.sendMessageToWindow(
            new InterWindowMessages('OAUTH2-FAILURE'),
            currentOpenedWindow,
          );
          return throwError(() => err);
        }),
      )
      .subscribe(() => {
        this.interWindowCommunicationService.sendMessageToWindow(
          new InterWindowMessages('OAUTH2-SUCCESS'),
          currentOpenedWindow,
        );
        this.connectSuccess.emit();
      });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
