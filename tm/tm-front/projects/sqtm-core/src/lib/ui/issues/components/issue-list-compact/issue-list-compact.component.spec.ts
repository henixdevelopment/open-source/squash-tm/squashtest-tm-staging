import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IssueListCompactComponent } from './issue-list-compact.component';
import { TranslateModule } from '@ngx-translate/core';

describe('IssueListCompactComponent', () => {
  let component: IssueListCompactComponent;
  let fixture: ComponentFixture<IssueListCompactComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [IssueListCompactComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(IssueListCompactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
