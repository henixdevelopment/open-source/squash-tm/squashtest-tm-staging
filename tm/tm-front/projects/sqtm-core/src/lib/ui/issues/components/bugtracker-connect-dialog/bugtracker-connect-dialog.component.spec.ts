import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BugtrackerConnectDialogComponent } from './bugtracker-connect-dialog.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { IssuesService } from '../../service/issues.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestingUtilsModule } from '../../../testing-utils/testing-utils.module';
import { DialogReference } from '../../../dialog/model/dialog-reference';
import {
  AuthenticationPolicy,
  AuthenticationProtocol,
} from '../../../../model/third-party-server/authentication.model';
import { ReferentialDataService } from '../../../../core/referential/services/referential-data.service';
import { of } from 'rxjs';

describe('BugtrackerConnectDialogComponent', () => {
  let component: BugtrackerConnectDialogComponent;
  let fixture: ComponentFixture<BugtrackerConnectDialogComponent>;
  const dialogRef = jasmine.createSpyObj('DialogReference', ['instant']);
  const issuesService = jasmine.createSpyObj('IssuesService', ['instant']);
  const referentialDataService = jasmine.createSpyObj('ReferentialDataService', ['instant']);
  referentialDataService.bugTrackers$ = of([
    {
      id: 123,
      loginFieldKey: 'sqtm-core.generic.label.organization-name',
      passwordFieldKey: 'sqtm-core.generic.label.password',
    },
  ]);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [BugtrackerConnectDialogComponent],
      imports: [TestingUtilsModule, HttpClientTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: DialogReference, useValue: dialogRef },
        { provide: IssuesService, useValue: issuesService },
        { provide: ReferentialDataService, useValue: referentialDataService },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BugtrackerConnectDialogComponent);
    component = fixture.componentInstance;
    component.configuration = {
      bugTracker: {
        id: 123,
        name: 'Azure-BT',
        url: 'https://azuredevops.com',
        kind: 'azuredevops.bugtracker',
        authPolicy: AuthenticationPolicy.USER,
        authProtocol: AuthenticationProtocol.BASIC_AUTH,
        iframeFriendly: true,
      },
    };
    fixture.detectChanges();
  });

  it('should create and have right fields for login and password', () => {
    expect(component).toBeTruthy();
    expect(component.loginFieldKey).toBe('sqtm-core.generic.label.organization-name');
    expect(component.passwordFieldKey).toBe('sqtm-core.generic.label.password');
  });
});
