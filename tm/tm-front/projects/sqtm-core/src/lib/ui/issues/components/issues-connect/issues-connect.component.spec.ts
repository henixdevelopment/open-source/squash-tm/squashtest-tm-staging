import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { IssuesConnectComponent } from './issues-connect.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { OverlayModule } from '@angular/cdk/overlay';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestingUtilsModule } from '../../../testing-utils/testing-utils.module';
import { DialogService } from '../../../dialog/services/dialog.service';
import { IssuesService } from '../../service/issues.service';

describe('IssuesConnectComponent', () => {
  let component: IssuesConnectComponent;
  let fixture: ComponentFixture<IssuesConnectComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [IssuesConnectComponent],
      imports: [TestingUtilsModule, OverlayModule, HttpClientTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: IssuesService, useValue: jasmine.createSpyObj(['openOAuthDialog']) },
        DialogService,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssuesConnectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
