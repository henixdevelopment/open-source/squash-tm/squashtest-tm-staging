import { TestBed } from '@angular/core/testing';

import { IssuesService } from './issues.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestingUtilsModule } from '../../testing-utils/testing-utils.module';
import { RestService } from '../../../core/services/rest.service';

describe('IssuesService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, HttpClientTestingModule],
      providers: [{ provide: IssuesService, useClass: IssuesService, deps: [RestService] }],
    }),
  );

  it('should be created', () => {
    const service: IssuesService = TestBed.inject(IssuesService);
    expect(service).toBeTruthy();
  });
});
