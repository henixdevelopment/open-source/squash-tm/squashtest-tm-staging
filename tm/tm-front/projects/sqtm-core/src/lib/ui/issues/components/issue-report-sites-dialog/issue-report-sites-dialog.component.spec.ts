import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogReference } from '../../../dialog/model/dialog-reference';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TestingUtilsModule } from '../../../testing-utils/testing-utils.module';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { IssueReportSitesDialogComponent } from './issue-report-sites-dialog.component';

describe('IssueReportSitesDialogComponent', () => {
  let component: IssueReportSitesDialogComponent;
  let fixture: ComponentFixture<IssueReportSitesDialogComponent>;

  beforeEach(async () => {
    const dialogRef: any = jasmine.createSpyObj<DialogReference>(['close']);
    dialogRef.data = {
      reportSites: [],
    };

    await TestBed.configureTestingModule({
      imports: [TestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule],
      declarations: [IssueReportSitesDialogComponent],
      providers: [{ provide: DialogReference, useValue: dialogRef }],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IssueReportSitesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
