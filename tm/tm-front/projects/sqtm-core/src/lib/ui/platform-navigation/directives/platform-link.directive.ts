import { Directive, ElementRef, Host, HostListener, Input, OnInit, Renderer2 } from '@angular/core';
import { SquashPlatformNavigationService } from '../../../core/services/navigation/squash-platform-navigation.service';
import { SQTM_MAIN_APP_IDENTIFIER } from '../../../core/sqtm-core.tokens';

@Directive({
  selector: '[sqtmCorePlatformLink]',
})
export class PlatformLinkDirective implements OnInit {
  @Input('sqtmCorePlatformLink')
  link: string | string[];

  @Input('sqtmCorePlatformLinkPluginIdentifier')
  pluginIdentifier: string = SQTM_MAIN_APP_IDENTIFIER;

  constructor(
    private platformNavigationService: SquashPlatformNavigationService,
    private renderer: Renderer2,
    @Host() private host: ElementRef,
  ) {}

  @HostListener('click', ['$event'])
  clicked(event: MouseEvent) {
    const url: string[] = typeof this.link === 'string' ? this.link.split('/') : this.link;
    this.platformNavigationService.navigate({
      command: url,
      pluginDestination: this.pluginIdentifier,
      openInNewTab: event.ctrlKey,
    });
    event.preventDefault();
    event.stopPropagation();
  }

  ngOnInit(): void {
    const href = this.platformNavigationService.generateHref({
      command: this.link as any,
      pluginDestination: this.pluginIdentifier,
    });
    this.renderer.setAttribute(this.host.nativeElement, 'href', href);
  }
}
