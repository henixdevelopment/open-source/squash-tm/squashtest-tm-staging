import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OptionalGroupedMultiListComponent } from './optional-grouped-multi-list.component';
import { TranslateModule } from '@ngx-translate/core';

describe('OptionalGroupedMultiListComponent', () => {
  let component: OptionalGroupedMultiListComponent;
  let fixture: ComponentFixture<OptionalGroupedMultiListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [OptionalGroupedMultiListComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionalGroupedMultiListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
