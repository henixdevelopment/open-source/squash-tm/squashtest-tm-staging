import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EditableTagFieldComponent } from './editable-tag-field.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TestingUtilsModule } from '../../../../testing-utils/testing-utils.module';
import { TranslateService } from '@ngx-translate/core';

describe('EditableTagFieldComponent', () => {
  let component: EditableTagFieldComponent;
  let fixture: ComponentFixture<EditableTagFieldComponent>;
  const translateService = jasmine.createSpyObj('TranslateService', ['instant']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [BrowserAnimationsModule, TestingUtilsModule],
      declarations: [EditableTagFieldComponent],
      providers: [
        {
          provide: TranslateService,
          useValue: translateService,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableTagFieldComponent);
    component = fixture.componentInstance;
    const tags = ['tag', 'tag2'];
    const options = ['tag', 'tag2', 'first tag'];

    component.options = options;
    component.value = tags;
    component.editable = true;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display one tag by string in value', () => {
    const select = fixture.nativeElement.querySelector('nz-select');
    expect(select.children.length).toBe(3);
  });

  it('should emit the values when tags changed', () => {
    const newValue = ['tag1', 'tag2', 'tag3', 'tag4'];
    let outputValue = [];
    component.confirmEvent.asObservable().subscribe((value) => (outputValue = value));
    component['model'] = newValue;
    fixture.detectChanges();
    component.confirm();
    fixture.detectChanges();
    expect(outputValue).toEqual(newValue);
    // Component is not allowed to manipulate itself the value attribute.
    // Only when store is updated, the value is set.
    expect(component.value).toEqual(['tag', 'tag2']);
  });
});
