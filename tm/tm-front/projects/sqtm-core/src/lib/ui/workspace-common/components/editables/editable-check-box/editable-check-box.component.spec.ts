import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EditableCheckBoxComponent } from './editable-check-box.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('EditableCheckBoxComponent', () => {
  let component: EditableCheckBoxComponent;
  let fixture: ComponentFixture<EditableCheckBoxComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [EditableCheckBoxComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableCheckBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
