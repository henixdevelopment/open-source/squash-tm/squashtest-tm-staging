import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionAutocompleteFieldComponent } from './action-autocomplete-field.component';

describe('ActionAutocompleteFieldComponent', () => {
  let component: ActionAutocompleteFieldComponent;
  let fixture: ComponentFixture<ActionAutocompleteFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ActionAutocompleteFieldComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionAutocompleteFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show and hide option menu according to the options', () => {
    component.options = [];
    fixture.detectChanges();
    expect(component.isMenuOpen).toBeFalse();
    component.options = [
      'je me trouve sur la page de connexion',
      'je renseigne <login> dans le champ "login"',
      'je renseigne <password> dans le champ "password"',
      'je clique sur le bouton "connexion"',
      'je suis connecté',
    ];
    fixture.detectChanges();
    expect(component.isMenuOpen).toBeTrue();
    component.options = [];
    fixture.detectChanges();
    expect(component.isMenuOpen).toBeFalse();
  });
});
