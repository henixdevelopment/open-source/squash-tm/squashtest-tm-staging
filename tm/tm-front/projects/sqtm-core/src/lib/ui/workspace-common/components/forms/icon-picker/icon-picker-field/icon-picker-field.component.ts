import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EmbeddedViewRef,
  EventEmitter,
  Input,
  Output,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { TemplatePortal } from '@angular/cdk/portal';

@Component({
  selector: 'sqtm-core-icon-picker-field',
  templateUrl: './icon-picker-field.component.html',
  styleUrls: ['./icon-picker-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IconPickerFieldComponent {
  @Input()
  icon: string;

  @Input()
  icons: string[];

  @Input()
  placeholder = 'sqtm-core.generic.label.none.feminine';

  @Input()
  editable = true;

  @Output()
  iconChanged = new EventEmitter<string>();

  @ViewChild('field')
  iconSelectField: ElementRef;

  @ViewChild('template')
  templateRef: TemplateRef<any>;

  private overlayRef: OverlayRef;
  private componentRef: EmbeddedViewRef<any>;

  constructor(
    private overlay: Overlay,
    private viewContainerRef: ViewContainerRef,
    public readonly cdRef: ChangeDetectorRef,
  ) {}

  get hasIcon(): boolean {
    return this.icon != null && this.icon.trim().length > 0;
  }

  get selectedIcon(): string {
    return this.icon;
  }

  set selectedIcon(newIcon: string) {
    this.icon = newIcon;
    this.cdRef.detectChanges();
  }

  selectAndClose(icon: string) {
    this.selectedIcon = icon;
    this.close();
  }

  edit(): void {
    const positionStrategy = this.overlay
      .position()
      .flexibleConnectedTo(this.iconSelectField)
      .withPositions([
        { originX: 'start', overlayX: 'start', originY: 'top', overlayY: 'bottom', offsetY: -8 },
      ]);

    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
    };

    this.overlayRef = this.overlay.create(overlayConfig);
    const templatePortal = new TemplatePortal(this.templateRef, this.viewContainerRef);
    this.componentRef = this.overlayRef.attach(templatePortal);

    this.overlayRef.backdropClick().subscribe(() => this.close());
  }

  private close() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
      this.componentRef = null;
    }

    this.cdRef.detectChanges();
  }

  getComponentClasses() {
    const cssClass = [];
    if (this.editable) {
      cssClass.push('editable');
    } else {
      cssClass.push('read');
    }

    return cssClass;
  }

  handleIconClick($event: string): void {
    if (this.selectedIcon === $event) {
      this.selectedIcon = null;
    } else {
      this.selectedIcon = $event;
    }

    this.close();
    this.iconChanged.emit(this.selectedIcon);
  }
}
