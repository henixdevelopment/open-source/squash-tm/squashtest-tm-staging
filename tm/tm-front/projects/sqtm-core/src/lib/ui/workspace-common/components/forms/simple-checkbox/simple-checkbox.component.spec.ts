import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SimpleCheckboxComponent } from './simple-checkbox.component';

describe('SimpleCheckboxComponent', () => {
  let component: SimpleCheckboxComponent;
  let fixture: ComponentFixture<SimpleCheckboxComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SimpleCheckboxComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
