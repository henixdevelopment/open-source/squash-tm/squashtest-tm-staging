// same as SquashTmDataRowType but for nodes in trees

export enum SquashTmNodeTypeEnum {
  TestCaseLibrary = 'TestCaseLibrary',
  TestCaseFolder = 'TestCaseFolder',
  TestCase = 'TestCase',
  RequirementLibrary = 'RequirementLibrary',
  RequirementFolder = 'RequirementFolder',
  Requirement = 'Requirement',
  CampaignLibrary = 'CampaignLibrary',
  CampaignFolder = 'CampaignFolder',
  Campaign = 'Campaign',
  Iteration = 'Iteration',
  TestSuite = 'TestSuite',
  CustomReportLibrary = 'CustomReportLibrary',
  CustomReportFolder = 'CustomReportFolder',
  ChartDefinition = 'ChartDefinition',
  ReportDefinition = 'ReportDefinition',
  CustomReportDashboard = 'CustomReportDashboard',
  CustomReportCustomExport = 'CustomReportCustomExport',
  ActionWord = 'ActionWord',
  ActionWordLibrary = 'ActionWordLibrary',
  HighLevelRequirement = 'HighLevelRequirement',
}
