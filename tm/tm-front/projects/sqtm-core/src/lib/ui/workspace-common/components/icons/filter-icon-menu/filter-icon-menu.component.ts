import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'sqtm-core-filter-icon-menu',
  templateUrl: './filter-icon-menu.component.html',
  styleUrls: ['./filter-icon-menu.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterIconMenuComponent {
  @Input()
  isChecked: boolean;

  @Input()
  checkBoxLabel: string;

  @Input()
  helpTooltip: string;

  @Output()
  confirmEvent = new EventEmitter<boolean>();

  confirm(newValue: boolean) {
    this.confirmEvent.emit(newValue);
  }

  getFilterIconStyle(isChecked: boolean) {
    return isChecked ? 'fill' : 'outline';
  }

  preventClickPropagation($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
  }
}
