import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionTextFieldComponent } from './action-text-field.component';

describe('ActionTextFieldComponent', () => {
  let component: ActionTextFieldComponent;
  let fixture: ComponentFixture<ActionTextFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ActionTextFieldComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionTextFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
