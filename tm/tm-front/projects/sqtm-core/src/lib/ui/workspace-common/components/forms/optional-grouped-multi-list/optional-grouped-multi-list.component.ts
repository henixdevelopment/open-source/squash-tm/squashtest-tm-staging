import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { ListItem } from '../grouped-multi-list/grouped-multi-list.component';
import { GroupedMultiListFieldComponent } from '../grouped-multi-list-field/grouped-multi-list-field.component';

@Component({
  selector: 'sqtm-core-optional-grouped-multi-list',
  template: ` <div [attr.data-test-field-id]="fieldName" class="flex-row p-r-5 optionals-center">
    <label
      class="optional-width"
      nz-checkbox
      [nzChecked]="check"
      (nzCheckedChange)="editField($event)"
      [nzDisabled]="disable"
      [attr.data-test-element-id]="'activate-checkbox'"
    >
      {{ i18nKey | translate }}
    </label>
    <sqtm-core-grouped-multi-list-field
      class="optional-width"
      [disabled]="!selectable()"
      [listItems]="listItems"
      [virtualScroll]="virtualScroll"
      [separateSelectedItems]="separateSelectedItems"
      [placeHolder]="placeHolder"
      [showClearButton]="showClearButton"
      (valueChanged)="selectedItemsChanged($event)"
    >
    </sqtm-core-grouped-multi-list-field>
  </div>`,
  styleUrls: ['./optional-grouped-multi-list.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OptionalGroupedMultiListComponent {
  @Input()
  disable: boolean;

  @Input()
  fieldName: string;

  @Input()
  listItems: ListItem[];

  @Input()
  placeHolder = '';

  @Input()
  virtualScroll = true;

  @Input()
  separateSelectedItems = true;

  @Input()
  i18nKey: string;

  check = false;

  @Input()
  select = true;

  @Input()
  showClearButton = true;

  @Output()
  checkEvent = new EventEmitter();

  @Output()
  multiListValueChanged = new EventEmitter<ListItem[]>();

  @ViewChild(GroupedMultiListFieldComponent)
  multiListField: GroupedMultiListFieldComponent;

  constructor() {}

  editField($event) {
    this.check = $event;
    this.checkEvent.emit($event);
  }

  selectable() {
    return this.check && this.select;
  }

  selectedItemsChanged($event: ListItem[]): void {
    this.multiListValueChanged.next($event);
  }
}
