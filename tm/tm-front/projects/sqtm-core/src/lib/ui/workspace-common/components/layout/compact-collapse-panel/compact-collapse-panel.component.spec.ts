import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CompactCollapsePanelComponent } from './compact-collapse-panel.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

describe('CompactCollapsePanelComponent', () => {
  let component: CompactCollapsePanelComponent;
  let fixture: ComponentFixture<CompactCollapsePanelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [CompactCollapsePanelComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompactCollapsePanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
