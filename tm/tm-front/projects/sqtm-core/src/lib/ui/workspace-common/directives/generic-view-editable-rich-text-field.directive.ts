import { Directive, Host, Input } from '@angular/core';
import { EditableRichTextComponent } from '../components/editables/editable-rich-text/editable-rich-text.component';
import { SqtmGenericEntityState } from '../../../core/services/genric-entity-view/generic-entity-view-state';
import { AbstractGenericViewEditableField } from './abstract-generic-view-editable-field';
import { GenericEntityViewService } from '../../../core/services/genric-entity-view/generic-entity-view.service';

@Directive({
  selector: '[sqtmCoreGenericViewEditableRichTextField]',
})
export class GenericViewEditableRichTextFieldDirective<
  E extends SqtmGenericEntityState,
  T extends string,
> extends AbstractGenericViewEditableField<EditableRichTextComponent, E, T> {
  @Input('sqtmCoreGenericViewEditableRichTextField')
  fieldName: keyof E;

  constructor(
    protected genericEntityViewService: GenericEntityViewService<E, T>,
    @Host() protected editable: EditableRichTextComponent,
  ) {
    super(genericEntityViewService, editable);
  }

  protected override showExternalErrorMessages(_errorMessages: string[]): void {}

  protected setValue(newValue: any): void {
    this.editable.value = newValue;
  }
}
