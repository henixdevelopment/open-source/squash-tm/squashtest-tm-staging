import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  ViewChild,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { EditableCustomField } from '../../../../custom-field/editable-custom-field';
import { ValidatorFn } from '@angular/forms';
import { AbstractEditableField, EditableField, EditableLayout } from '../abstract-editable-field';
import {
  closeCkEditorDialog,
  createCkEditorConfig,
  hideNotification,
  isAllowedImageType,
  overrideCKEditorDefaultLinkTarget,
} from '../../../../utils/ck-editor-config';
import { CKEditor4, CKEditorComponent } from 'ckeditor4-angular';
import { SessionPingService } from '../../../../../core/services/session-ping/session-ping.service';
import { getSupportedBrowserLang } from '../../../../../core/utils/browser-langage.utils';
import { RichTextAttachmentDelegate } from '../../../../../core/services/rich-text-attachment-delegate';
import { TargetOnUpload, UploadImage } from '../../../../../model/attachment/attachment.model';
import { take } from 'rxjs/operators';
import { ExternalNavigationConsentService } from '../../../../../core/services/external-navigation-consent/external-navigation-consent.service';
import { Subject } from 'rxjs';
import EditorType = CKEditor4.EditorType;

@Component({
  selector: 'sqtm-core-editable-rich-text',
  templateUrl: './editable-rich-text.component.html',
  styleUrls: ['./editable-rich-text.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditableRichTextComponent
  extends AbstractEditableField
  implements OnInit, AfterViewInit, OnDestroy, EditableField, EditableCustomField
{
  private readonly REQUIRED_KEY = 'sqtm-core.validation.errors.required';

  dirty = false;

  externalErrors: string[] = [];

  editorModel: { editorData: string } = { editorData: '' };

  type = EditorType.CLASSIC;

  @Input()
  set value(value: string) {
    this.updateAndClose(value);
  }

  @Input()
  focus = false;

  get value(): string {
    return this.editorModel.editorData;
  }

  @Input()
  required = false;

  @Input()
  placeholder;

  edit = false;

  @Input()
  size: 'small' | 'default' | 'large' = 'default';

  @Input()
  border = false;

  @Input()
  layout: EditableLayout = 'default';

  @Input()
  height = 200;

  @Input()
  set validators(validators: ValidatorFn[]) {
    throw Error('Validators are not handled in Rich Text Field.');
  }

  @Input()
  targetOnUpload: TargetOnUpload;

  pending = false;

  config = {};

  // initial value when editor is active. Allow to restore previous value if user press cancel.
  initialValue;

  @Output()
  readonly confirmEvent = new EventEmitter<string>();

  @Output()
  readonly cancelEvent = new EventEmitter<void>();

  @Output()
  readonly ckEditorReady = new EventEmitter<void>();

  @ViewChild('confirmButton', { read: ElementRef })
  confirmButton: ElementRef;

  @ViewChild('cancelButton', { read: ElementRef })
  cancelButton: ElementRef;

  @ViewChild('ckeditor')
  ckeditor: CKEditorComponent;

  private readonly unsub$ = new Subject<void>();

  constructor(
    cdRef: ChangeDetectorRef,
    private ngxTranslateService: TranslateService,
    private readonly sessionPingService: SessionPingService,
    @Optional() private readonly richTextAttachmentDelegate: RichTextAttachmentDelegate,
    private readonly externalNavigationConsentService: ExternalNavigationConsentService,
  ) {
    super(cdRef);
    if (!this.placeholder) {
      this.placeholder = this.ngxTranslateService.instant('sqtm-core.generic.editable.placeholder');
    }
  }

  ngOnInit() {
    this.config = createCkEditorConfig(
      getSupportedBrowserLang(this.ngxTranslateService),
      this.height,
    );
  }

  ngAfterViewInit(): void {
    if (this.focus) {
      this.focusInputField();
    }
  }

  ngOnDestroy(): void {
    if (this.edit) {
      this.sessionPingService.endLongTermEdition();
    }

    this.unsub$.next();
    this.unsub$.complete();
  }

  handleCKEditorReady(): void {
    overrideCKEditorDefaultLinkTarget();

    this.ckEditorReady.next();
  }

  handleClick(event?: MouseEvent) {
    // event is undefined in unit tests
    const target = event?.target as HTMLElement;
    const anchor = target?.closest('a');

    if (anchor) {
      event.stopPropagation();
      event.stopImmediatePropagation();
      this.externalNavigationConsentService.handleClickEvent(event, this.unsub$);
    } else {
      this.enableEditMode();
    }
  }

  enableEditMode(grabFocus?: boolean) {
    if (!this.edit && this.editable) {
      this.dirty = false;
      this.edit = true;
      this.initialValue = this.editorModel.editorData;
      this.cdRef.detectChanges();

      this.sessionPingService.beginLongTermEdition();

      if (grabFocus) {
        this.ckEditorReady.pipe(take(1)).subscribe(() => this.grabFocus());
      }
    }
  }

  disableEditMode() {
    if (this.edit) {
      this.edit = false;
      this.pending = false;
      this.cdRef.detectChanges();

      this.sessionPingService.endLongTermEdition();
    }
  }

  /**
   * Put the focus inside CKEditor's edition area.
   * This only works if the component is in edit mode and the CKEditor instance is properly initialized.
   *
   * @param cursorPosition: where to put the cursor (before or after content).
   */
  grabFocus(cursorPosition: 'start' | 'end' = 'end') {
    this.ckeditor.instance.focus();

    if (cursorPosition === 'end') {
      const range = this.ckeditor.instance.createRange();
      range.moveToElementEditEnd(range.root);
      this.ckeditor.instance.getSelection().selectRanges([range]);
    }
  }

  handleClickOut(event) {
    const isFullScreen: boolean = event.editor.getCommand('maximize').state === 1;
    if (!isFullScreen) {
      if (this.editorModel.editorData === this.initialValue) {
        this.cancel();
      } else {
        if (this.hasCancelButtonBeenClicked()) {
          this.cancel();
        } else if (!this.hasConfirmButtonBeenClicked()) {
          this.confirm();
        }
      }
    }
  }

  private hasConfirmButtonBeenClicked() {
    return document.activeElement === this.confirmButton.nativeElement;
  }

  private hasCancelButtonBeenClicked() {
    return document.activeElement === this.cancelButton.nativeElement;
  }

  cancel() {
    this.editorModel.editorData = this.initialValue;
    this.disableEditMode();
    this.cancelEvent.emit();
  }

  confirm() {
    this.dirty = true;
    this.removeExternalErrors();
    this.editorModel.editorData = this.extractImageHeightWidthAsHtmlAttributes(
      this.editorModel.editorData,
    );

    if (this.required && this.isBlank(this.editorModel.editorData)) {
      this.externalErrors.push(this.ngxTranslateService.instant(this.REQUIRED_KEY));
      this.cdRef.detectChanges();
    } else if (this.initialValue === this.editorModel.editorData) {
      this.cancel();
    } else {
      this.executeAutoAsync();
      this.confirmEvent.emit(this.editorModel.editorData);
    }
  }

  /**
   * Extracts an image's height value and width value from the inline style set by ckEditor if measured in px,
   * and adds the values as html height and width attributes within the image tag.
   * Regex : selects the number values between " width:" and "px" and "height:" and "px"
   * @param value string containing the html content of an rtf
   * @private
   */
  private extractImageHeightWidthAsHtmlAttributes(value: string): string {
    if (
      value.includes('<img') &&
      value.includes('(?<=\\swidth:)([0-9]+)(?=px)') &&
      value.includes('(?<=height:)([0-9]+)(?=px)')
    ) {
      const extractedWidth = value.match('(?<=\\swidth:)([0-9]+)(?=px)');
      const width = extractedWidth.toString().split(',')[1];
      const widthAttribute = 'width="' + width + '" ';

      const extractedHeight = value.match('(?<=height:)([0-9]+)(?=px)');
      const height = extractedHeight.toString().split(',')[1];
      const heightAttribute = ' height="' + height + '" ';

      const splitValues = value.split(' style');
      return splitValues[0] + heightAttribute + widthAttribute + 'style' + splitValues[1];
    } else {
      return value;
    }
  }

  beginAsync() {
    this.pending = true;
  }

  endAsync() {
    this.pending = false;
    this.cdRef.detectChanges();
  }

  updateAndClose(value: string) {
    this.endAsync();
    this.disableEditMode();
    this.initialValue = value;
    this.editorModel.editorData = value;
    this.cdRef.detectChanges();
  }

  private removeExternalErrors() {
    this.externalErrors = [];
  }

  private isBlank(text: string) {
    // Extract text
    const div = document.createElement('div');
    div.innerHTML = text;
    return (div.textContent || div.innerText).trim().length === 0;
  }

  private focusInputField() {
    this.config['startupFocus'] = true;
  }

  handleFileUploadRequest(event: CKEditor4.EventInfo) {
    const editor = event.editor;
    const fileLoader = event.data.fileLoader;
    const fileName = fileLoader.fileName;
    const file: File = fileLoader.file;

    // Cancel default ckEditor behavior and close dialog
    event.cancel();
    closeCkEditorDialog();

    const notification = editor.showNotification(
      this.ngxTranslateService.instant('sqtm-core.attachment-drawer.image-upload.progress', {
        fileName,
      }),
      0,
    );

    if (!isAllowedImageType(file)) {
      this.showInvalidImageFormatNotification(notification, fileName);
      return;
    }

    if (this.richTextAttachmentDelegate == null) {
      this.insertBase64Image(fileLoader, editor, fileName, notification);
    } else {
      this.richTextAttachmentDelegate.attach(file, this.targetOnUpload).subscribe({
        next: (uploadImage: UploadImage) =>
          this.showUploadImageNotification(uploadImage, notification, editor, fileName),
        error: (err) => this.showUploadImageErrorNotification(err, notification),
        complete: () => hideNotification(notification),
      });
    }
  }

  private showInvalidImageFormatNotification(notification: any, fileName: string) {
    notification.update({
      type: 'warning',
      message: this.ngxTranslateService.instant(
        'sqtm-core.attachment-drawer.image-upload.invalid',
        { fileName },
      ),
    });
    hideNotification(notification);
  }

  private insertBase64Image(fileLoader, editor, fileName, notification: any) {
    const imageSource = fileLoader.data;
    editor.insertHtml('<img src="' + imageSource + '" alt="' + fileName + '">');
    notification.update({
      type: 'success',
      message: this.ngxTranslateService.instant(
        'sqtm-core.attachment-drawer.image-upload.complete',
        { fileName },
      ),
    });
    hideNotification(notification);
  }

  private showUploadImageNotification(
    uploadImage: UploadImage,
    notification: any,
    editor: any,
    fileName: string,
  ) {
    if (uploadImage.kind === 'progress') {
      notification.update({ progress: uploadImage.uploadProgress });
    } else if (uploadImage.kind === 'complete') {
      editor.insertHtml('<img src="' + uploadImage.url + '" alt="' + fileName + '">');
      notification.update({
        type: 'success',
        message: this.ngxTranslateService.instant(
          'sqtm-core.attachment-drawer.image-upload.complete',
          { fileName },
        ),
      });
    }
  }

  private showUploadImageErrorNotification(error: any, notification: any) {
    notification.update({
      type: 'warning',
      message: error.message,
    });
  }

  getButtonBarCss() {
    return this.hideButtons ? ['hide-buttons', 'm-t-10'] : 'm-t-10';
  }
}
