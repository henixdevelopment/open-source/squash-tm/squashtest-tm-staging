import { Pipe, PipeTransform } from '@angular/core';

export function capitalizeString(value: any) {
  if (typeof value === 'string' && Boolean(value)) {
    try {
      return value.charAt(0).toLocaleUpperCase() + value.substring(1);
    } catch (e) {
      console.warn(`Unable to capitalize ${value}`);
      return value;
    }
  }
  return '';
}

@Pipe({
  name: 'capitalize',
})
export class CapitalizePipe implements PipeTransform {
  transform(value: any, _args?: any): any {
    return capitalizeString(value);
  }
}
