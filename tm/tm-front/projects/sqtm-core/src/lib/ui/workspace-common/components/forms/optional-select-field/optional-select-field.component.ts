import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { EditableSelectFieldComponent } from '../../editables/editable-select-field/editable-select-field.component';
import { DisplayOption } from '../../../../../model/display-option';

@Component({
  selector: 'sqtm-core-optional-select-field',
  template: `
    <div [attr.data-test-field-id]="fieldName" class="flex-row p-r-5 optionals-center">
      <label
        [style]="computedLabelStyle"
        nz-checkbox
        [nzChecked]="check"
        (nzCheckedChange)="editField($event)"
        [nzDisabled]="disable"
        [attr.data-test-element-id]="'activate-checkbox'"
      >
        {{ i18nKey | translate }}
      </label>
      <nz-select
        #selectField
        [nzShowSearch]="searchable"
        [style]="computedSelectStyle"
        [ngModel]="selectedValue"
        (ngModelChange)="change($event)"
        [nzDisabled]="!selectable()"
        [nzPlaceHolder]="selectPlaceHolder"
      >
        @for (option of displayOptions; track option) {
          <nz-option [nzValue]="option.id" [nzLabel]="option.label">{{ option.label }}</nz-option>
        }
      </nz-select>
    </div>
  `,
  styleUrls: ['./optional-select-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OptionalSelectFieldComponent {
  @Input()
  displayOptions: DisplayOption[] = [];

  @Input()
  selectedValue: string | number;

  @Input()
  disable: boolean;

  @Input()
  fieldName: string;

  @Input()
  i18nKey: string;

  @Input()
  selectPlaceHolder = '';

  check = false;

  @Input()
  select = true;

  // That's probably bad design to have styling inputs but I couldn't find a way around without
  //  splitting this component in two
  @Input()
  labelWidthRatio = 0.5;

  @Input()
  searchable = false;

  @Output()
  checkEvent = new EventEmitter();

  @ViewChild('selectField')
  selectField: EditableSelectFieldComponent;

  get computedLabelStyle(): string {
    return `width: ${this.labelWidthRatio * 100}%`;
  }

  get computedSelectStyle(): string {
    return `width: ${(1 - this.labelWidthRatio) * 100}%`;
  }

  change($event) {
    this.selectedValue = $event;
  }

  editField($event) {
    this.check = $event;
    this.checkEvent.emit($event);
  }

  selectable() {
    return this.check && this.select;
  }
}
