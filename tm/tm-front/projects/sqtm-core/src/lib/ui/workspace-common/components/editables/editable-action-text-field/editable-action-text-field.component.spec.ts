import { OverlayModule } from '@angular/cdk/overlay';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { TestingUtilsModule } from '../../../../testing-utils/testing-utils.module';

import { EditableActionTextFieldComponent } from './editable-action-text-field.component';

describe('EditableActionTextFieldComponent', () => {
  let component: EditableActionTextFieldComponent;
  let fixture: ComponentFixture<EditableActionTextFieldComponent>;

  const translateService = jasmine.createSpyObj('TranslateService', ['instant']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TestingUtilsModule, ReactiveFormsModule, OverlayModule],
      declarations: [EditableActionTextFieldComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [{ provide: TranslateService, useValue: translateService }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableActionTextFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
