import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FullPageLoadingIconComponent } from './full-page-loading-icon.component';

describe('FullPageLoadingIconComponent', () => {
  let component: FullPageLoadingIconComponent;
  let fixture: ComponentFixture<FullPageLoadingIconComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [FullPageLoadingIconComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FullPageLoadingIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
