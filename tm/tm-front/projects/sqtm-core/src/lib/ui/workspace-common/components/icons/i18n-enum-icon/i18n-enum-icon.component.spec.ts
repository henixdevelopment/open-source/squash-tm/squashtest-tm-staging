import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { I18nEnumIconComponent } from './i18n-enum-icon.component';
import { TestingUtilsModule } from '../../../../testing-utils/testing-utils.module';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { OnPushComponentTester } from '../../../../testing-utils/on-push-component-tester';
import { TestCaseStatus, TestCaseWeight } from '../../../../../model/level-enums/level-enum';

export class I18nEnumIconComponentTester extends OnPushComponentTester<I18nEnumIconComponent<any>> {
  assertIconNameEquals(expectedName: string) {
    return this.getIcon().classes.includes(`anticon-sqtm-core-${expectedName}`);
  }

  extractColor() {
    const style = window.getComputedStyle(this.getIcon().nativeElement);
    return style.color;
  }

  private getIcon() {
    return this.element('i');
  }
}

describe('I18nEnumIconComponent', () => {
  let tester: I18nEnumIconComponentTester;
  let fixture: ComponentFixture<I18nEnumIconComponent<any>>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, NzIconModule, NzToolTipModule],
      declarations: [I18nEnumIconComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(I18nEnumIconComponent);
    tester = new I18nEnumIconComponentTester(fixture);
  });

  it('should display icon for test case status', () => {
    tester.componentInstance.i18nEnumItem = TestCaseStatus.APPROVED;
    fixture.detectChanges();
    expect(tester.componentInstance).toBeTruthy();
    expect(tester.assertIconNameEquals('test-case:status')).toBeTruthy();
    expect(tester.extractColor()).toEqual('rgb(31, 191, 5)');
  });

  it('should display icon for test case importance by couple key/value', () => {
    tester.componentInstance.i18nEnum = TestCaseWeight;
    tester.componentInstance.i18nEnumItem = 'VERY_HIGH';
    fixture.detectChanges();
    expect(tester.componentInstance).toBeTruthy();
    expect(tester.assertIconNameEquals('test-case:double_up')).toBeTruthy();
  });
});
