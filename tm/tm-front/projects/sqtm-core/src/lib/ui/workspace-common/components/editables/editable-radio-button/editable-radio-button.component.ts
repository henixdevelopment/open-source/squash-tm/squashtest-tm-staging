import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { AbstractEditableField, EditableField, EditableLayout } from '../abstract-editable-field';
import { Option } from '../../../../../model/option.model';

@Component({
  selector: 'sqtm-core-editable-radio-button',
  templateUrl: './editable-radio-button.component.html',
  styleUrls: ['./editable-radio-button.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditableRadioButtonComponent
  extends AbstractEditableField
  implements OnInit, EditableField
{
  private _value;

  model: any;

  @Input()
  set value(value: any) {
    this._value = value;
    this.model = value;
    this.cdRef.detectChanges();
  }

  get value() {
    return this._value;
  }

  @Input()
  options: Option[] = [];

  layout: EditableLayout;

  @Output()
  confirmEvent = new EventEmitter<any>();

  constructor(public cdRef: ChangeDetectorRef) {
    super(cdRef);
  }

  ngOnInit() {}

  beginAsync() {}

  cancel() {}

  confirm() {
    this.confirmEvent.emit(this.value);
  }

  disableEditMode() {}

  enableEditMode() {}

  endAsync() {}
}
