import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditableTimeFieldComponent } from './editable-time-field.component';
import { TestingUtilsModule } from '../../../../testing-utils/testing-utils.module';
import { TranslateModule } from '@ngx-translate/core';
import { NzInputNumberComponent } from 'ng-zorro-antd/input-number';

describe('EditableTimeFieldComponent', () => {
  let component: EditableTimeFieldComponent;
  let fixture: ComponentFixture<EditableTimeFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TestingUtilsModule, TranslateModule.forRoot()],
      declarations: [EditableTimeFieldComponent, NzInputNumberComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(EditableTimeFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.hourInput = {
      inputElement: {
        nativeElement: jasmine.createSpyObj(['select', 'click']),
      },
    } as NzInputNumberComponent;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should format time value', () => {
    const testData = [
      { input: null, expectedResult: '00' },
      { input: 2, expectedResult: '02' },
      { input: 22, expectedResult: '22' },
      { input: 222, expectedResult: '222' },
      { input: '02', expectedResult: '02' },
      { input: '2', expectedResult: '02' },
      { input: '22', expectedResult: '22' },
      { input: '222', expectedResult: '222' },
    ];

    testData.forEach((element) => {
      const formattedValue = component.formatValue(element.input);
      expect(formattedValue).toBe(element.expectedResult);
    });
  });

  it('should not go in edit mode if is not editable', () => {
    component.editable = false;
    component.enableEditMode();
    expect(component.edit).toBeFalse();
  });

  it('should go in edit mode if is editable', () => {
    component.editable = true;
    component.enableEditMode();
    expect(component.edit).toBeTrue();
  });

  it('should synchronize hour and minute model', () => {
    const testData = [
      { input: null, expectedHour: 0, expectedMinute: 0 },
      { input: 1, expectedHour: 0, expectedMinute: 1 },
      { input: 60, expectedHour: 1, expectedMinute: 0 },
      { input: 119, expectedHour: 1, expectedMinute: 59 },
      { input: 0, expectedHour: 0, expectedMinute: 0 },
    ];

    testData.forEach((element) => {
      component.value = element.input;
      component.synchronizeHourAndMinuteModel();
      expect(component.hourModel).toBe(element.expectedHour);
      expect(component.minuteModel).toBe(element.expectedMinute);
    });
  });

  it('should throw an error if time value is negative', () => {
    expect(() => (component.value = -22)).toThrowError();
  });
});
