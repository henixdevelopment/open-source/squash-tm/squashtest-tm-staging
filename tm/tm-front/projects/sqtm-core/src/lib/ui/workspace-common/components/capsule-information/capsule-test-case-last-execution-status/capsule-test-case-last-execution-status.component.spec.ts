import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CapsuleTestCaseLastExecutionStatusComponent } from './capsule-test-case-last-execution-status.component';
import { TranslateModule } from '@ngx-translate/core';

describe('CapsuleTestCaseLastExecutionStatusComponent', () => {
  let component: CapsuleTestCaseLastExecutionStatusComponent;
  let fixture: ComponentFixture<CapsuleTestCaseLastExecutionStatusComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [CapsuleTestCaseLastExecutionStatusComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapsuleTestCaseLastExecutionStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
