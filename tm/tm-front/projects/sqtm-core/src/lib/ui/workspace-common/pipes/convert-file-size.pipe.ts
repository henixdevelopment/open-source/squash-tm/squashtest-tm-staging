import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

/**
 * Convert a file size into the largest suitable unit and translate the unit notation.
 * Precision can be set as an argument (default is 2).
 */
@Pipe({
  name: 'convertFileSize',
})
export class ConvertFileSizePipe implements PipeTransform {
  private unitI18nKeys = [
    'sqtm-core.generic.file.size.unit.byte',
    'sqtm-core.generic.file.size.unit.kilobyte',
    'sqtm-core.generic.file.size.unit.megabyte',
    'sqtm-core.generic.file.size.unit.gigabyte',
    'sqtm-core.generic.file.size.unit.terabyte',
  ];

  constructor(private translateService: TranslateService) {}

  /**
   * Convert the given file size into the largest suitable unit and translate the unit notation.
   * @param bytes The file size expressed in bytes
   * @param precision The precision(number of digits in decimal part)
   */
  transform(bytes: number, precision: number = 2): string {
    // check if byte size is well defined
    if (bytes < 0 || bytes === undefined || bytes === null) {
      return '?';
    }
    // coerce precision number
    if (precision < 0) {
      precision = 0;
    }
    let unitIndex = 0;
    while (bytes >= 1024) {
      bytes /= 1024;
      unitIndex++;
    }
    // if unit is bytes, showing decimals is not useful
    if (unitIndex === 0) {
      precision = 0;
    }
    return (
      bytes.toFixed(precision).replace('.', ',') +
      ' ' +
      this.translateService.instant(this.unitI18nKeys[unitIndex])
    );
  }
}
