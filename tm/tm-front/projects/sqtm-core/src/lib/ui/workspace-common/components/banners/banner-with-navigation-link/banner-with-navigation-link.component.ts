import { ChangeDetectionStrategy, Component, Input, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { SquashPlatformNavigationService } from '../../../../../core/services/navigation/squash-platform-navigation.service';

@Component({
  selector: 'sqtm-core-banner-with-navigation-link',
  templateUrl: './banner-with-navigation-link.component.html',
  styleUrls: ['./banner-with-navigation-link.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BannerWithNavigationLinkComponent implements OnDestroy {
  @Input()
  messageKey: string;
  @Input()
  linkKey: string;
  @Input()
  permissionKey: string;
  @Input()
  redirectionUrl: string;
  @Input()
  isVisible: boolean;
  readonly unsub$ = new Subject<void>();

  constructor(private readonly platformService: SquashPlatformNavigationService) {}

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  openPageInNewTab(): void {
    this.platformService.openNewTab(this.redirectionUrl);
  }

  hideBanner() {
    this.isVisible = false;
  }
}
