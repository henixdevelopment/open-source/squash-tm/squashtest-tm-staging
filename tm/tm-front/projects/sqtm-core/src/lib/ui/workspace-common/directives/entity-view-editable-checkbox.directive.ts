import { Directive, Host, HostListener, Input, OnInit } from '@angular/core';
import { EntityViewService } from '../../../core/services/entity-view/entity-view.service';
import { Subject } from 'rxjs';
import { EditableCheckBoxComponent } from '../components/editables/editable-check-box/editable-check-box.component';
import { distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { SqtmEntityState } from '../../../core/services/entity-view/entity-view.state';
import { SimplePermissions } from '../../../model/permissions/simple-permissions';

@Directive({
  selector: '[sqtmCoreEntityViewEditableCheckbox]',
})
export class EntityViewEditableCheckboxDirective<
  E extends SqtmEntityState,
  T extends string,
  P extends SimplePermissions,
> implements OnInit
{
  @Input('sqtmCoreEntityViewEditableCheckbox')
  fieldName: keyof E;

  unsub$ = new Subject<void>();

  constructor(
    private entityViewService: EntityViewService<E, T, P>,
    @Host() private editable: EditableCheckBoxComponent,
  ) {}

  ngOnInit(): void {
    this.entityViewService.componentData$
      .pipe(
        takeUntil(this.unsub$),
        map((componentData) => componentData[componentData.type]),
        // If value has changed in store, the update is a success.
        // If there is any error server side, the error stream will emit, and the value will not be changed.
        distinctUntilChanged(),
      )
      .subscribe((entity) => {
        this.editable.value = entity[this.fieldName as string];
      });

    this.entityViewService.componentData$
      .pipe(takeUntil(this.unsub$))
      .subscribe((componentData) => {
        this.editable.editable =
          componentData.permissions.canWrite &&
          componentData.milestonesAllowModification &&
          this.editable.editable;
        this.editable.cdRef.detectChanges();
      });
  }

  @HostListener('confirmEvent', ['$event'])
  onConfirm(value: boolean) {
    this.editable.beginAsync();
    this.entityViewService.update(this.fieldName, value as any); // cannot use type system on dynamic inputs...
  }
}
