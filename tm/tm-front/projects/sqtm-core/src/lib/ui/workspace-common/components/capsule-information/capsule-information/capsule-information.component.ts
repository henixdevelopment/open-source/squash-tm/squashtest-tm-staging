import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { AbstractCapsuleInformationDirective } from '../abstract-capsule-information';
import { TranslateService } from '@ngx-translate/core';
import { ListPanelItem } from '../../forms/list-panel/list-panel.component';
import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { TemplatePortal } from '@angular/cdk/portal';

@Component({
  selector: 'sqtm-core-capsule-information',
  templateUrl: './capsule-information.component.html',
  styleUrls: ['./capsule-information.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CapsuleInformationComponent
  extends AbstractCapsuleInformationDirective
  implements OnInit
{
  @Input()
  customIconSize: number = null;

  @Input()
  editable = false;

  @Input()
  listPanelItems: ListPanelItem[];

  _selectedItem: string | number;

  @Input()
  set selectedItem(value: string | number) {
    this._selectedItem = value;
  }

  @Output()
  itemSelectionChanged = new EventEmitter<string | number>();

  @ViewChild('templatePortalContent', { read: TemplateRef })
  templatePortalContent: TemplateRef<any>;

  @ViewChild('listItemRef', { read: ElementRef })
  listItemRef: ElementRef;

  overlayRef: OverlayRef;

  constructor(
    translateService: TranslateService,
    public overlay: Overlay,
    public vcr: ViewContainerRef,
    public cdRef: ChangeDetectorRef,
  ) {
    super(translateService);
  }

  ngOnInit(): void {}

  getCustomIconSize() {
    if (this.customIconSize != null) {
      return {
        'font-size': `${this.customIconSize.toString()}px`,
      };
    }
  }

  change(id: string | number) {
    this.selectedItem = id;
    this.itemSelectionChanged.emit(id);
    this.close();
  }

  showTemplate() {
    if (this.editable) {
      this.showList(this.listItemRef, this.templatePortalContent);
    }
  }

  showList(elementRef: ElementRef, templateRef: TemplateRef<any>) {
    const positionStrategy = this.overlay
      .position()
      .flexibleConnectedTo(elementRef)
      .withPositions([
        { originX: 'start', overlayX: 'start', originY: 'center', overlayY: 'top', offsetY: 15 },
        { originX: 'start', overlayX: 'start', originY: 'top', overlayY: 'bottom', offsetY: -10 },
      ]);
    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
      width: 150,
    };
    this.overlayRef = this.overlay.create(overlayConfig);
    const templatePortal = new TemplatePortal(templateRef, this.vcr);
    this.overlayRef.attach(templatePortal);
    this.overlayRef.backdropClick().subscribe(() => this.close());
  }

  close() {
    this.removeOverlay();
    this.cdRef.detectChanges();
  }

  private removeOverlay() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
    }
  }
}
