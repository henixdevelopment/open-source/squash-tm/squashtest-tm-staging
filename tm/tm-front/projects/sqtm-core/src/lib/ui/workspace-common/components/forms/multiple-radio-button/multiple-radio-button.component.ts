import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { ListItem } from '../grouped-multi-list/grouped-multi-list.component';
import { Identifier } from '../../../../../model/entity.model';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-core-multiple-radio-button',
  templateUrl: './multiple-radio-button.component.html',
  styleUrls: ['./multiple-radio-button.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MultipleRadioButtonComponent {
  private _listItems: ListItem[] = [];

  get listItems(): ListItem[] {
    return this._listItems;
  }

  @Input()
  set listItems(items: ListItem[]) {
    this._listItems = items;
    this.updateSelectedOption();
  }

  @Input()
  selectionRequired = true;

  @Output()
  itemSelectionChanged = new EventEmitter<ListItem>();

  selectedItemId: Identifier;

  constructor(private translateService: TranslateService) {}

  private updateSelectedOption() {
    const selectedItems = this._listItems.filter((item) => item.selected);
    if (selectedItems.length !== 1 && this.selectionRequired) {
      throw Error('You must provide a list of items with one item selected');
    }
    if (selectedItems.length > 0) {
      this.selectedItemId = selectedItems[0].id;
    }
  }

  changeSelection() {
    const selectedItem = this._listItems.find((item) => item.id === this.selectedItemId);
    this.itemSelectionChanged.next(selectedItem);
  }

  getItemValue(listItem: ListItem): string {
    return listItem.i18nLabelKey
      ? this.translateService.instant(listItem.i18nLabelKey)
      : listItem.label;
  }
}
