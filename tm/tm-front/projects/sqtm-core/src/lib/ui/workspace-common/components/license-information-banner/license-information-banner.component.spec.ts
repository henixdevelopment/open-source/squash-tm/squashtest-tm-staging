import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseInformationBannerComponent } from './license-information-banner.component';
import { TestingUtilsModule } from '../../../testing-utils/testing-utils.module';
import { TranslateService } from '@ngx-translate/core';
import { DialogService } from '../../../dialog/services/dialog.service';
import { LicenseInformationState } from '../../../../core/referential/state/license-information.state';
import { licenseI18nKeys, LicenseMessagePlacement } from './license-information.helpers';
import Spy = jasmine.Spy;

describe('LicenseInformationBannerComponent', () => {
  let component: LicenseInformationBannerComponent;
  let fixture: ComponentFixture<LicenseInformationBannerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TestingUtilsModule],
      declarations: [LicenseInformationBannerComponent],
      providers: [
        { provide: TranslateService, useValue: jasmine.createSpyObj(['instant']) },
        { provide: DialogService, useValue: jasmine.createSpyObj(['openAlert']) },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseInformationBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    (component.translateService.instant as Spy).and.callFake((input) => input);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.isVisible).toBeFalse();
  });

  describe('it should show appropriate messages', () => {
    const dataSets: DataSet[] = [
      {
        name: 'No license information',
        placement: LicenseMessagePlacement.HOME,
        licenseInformation: null,
        expected: { hasMessage: false, short: '', long: '' },
      },
      {
        name: 'No messages',
        placement: LicenseMessagePlacement.HOME,
        licenseInformation: {
          pluginLicenseExpiration: null,
          activatedUserExcess: null,
        },
        expected: { hasMessage: false, short: '', long: '' },
      },
      {
        name: 'Date first warning',
        placement: LicenseMessagePlacement.HOME,
        licenseInformation: {
          pluginLicenseExpiration: '60',
          activatedUserExcess: null,
        },
        expected: {
          hasMessage: true,
          short: licenseI18nKeys.SHORT_DATE_WARNING,
          long: licenseI18nKeys.LONG_DATE_WARNING1,
        },
      },
      {
        name: 'Date second warning',
        placement: LicenseMessagePlacement.HOME,
        licenseInformation: {
          pluginLicenseExpiration: '12',
          activatedUserExcess: null,
        },
        expected: {
          hasMessage: true,
          short: licenseI18nKeys.SHORT_DATE_WARNING,
          long: licenseI18nKeys.LONG_DATE_WARNING2,
        },
      },
      {
        name: 'Date expired',
        placement: LicenseMessagePlacement.HOME,
        licenseInformation: {
          pluginLicenseExpiration: '-52',
          activatedUserExcess: null,
        },
        expected: {
          hasMessage: true,
          short: licenseI18nKeys.SHORT_DATE_EXPIRED,
          long: licenseI18nKeys.LONG_DATE_EXPIRED,
        },
      },
      {
        name: 'User excess, can create',
        placement: LicenseMessagePlacement.HOME,
        licenseInformation: {
          pluginLicenseExpiration: null,
          activatedUserExcess: '1-1-true',
        },
        expected: {
          hasMessage: true,
          short: licenseI18nKeys.SHORT_USER_WARNING,
          long: licenseI18nKeys.LONG_USER_WARNING,
        },
      },
      {
        name: 'User excess, cannot create',
        placement: LicenseMessagePlacement.HOME,
        licenseInformation: {
          pluginLicenseExpiration: null,
          activatedUserExcess: '1-1-false',
        },
        expected: {
          hasMessage: true,
          short: licenseI18nKeys.SHORT_USER_EXCESS,
          long: licenseI18nKeys.LONG_USER_EXCESS,
        },
      },
      {
        name: 'Both messages',
        placement: LicenseMessagePlacement.HOME,
        licenseInformation: {
          pluginLicenseExpiration: '12',
          activatedUserExcess: '1-1-false',
        },
        expected: {
          hasMessage: true,
          short: licenseI18nKeys.SHORT_DATE_WARNING + ' - ' + licenseI18nKeys.SHORT_USER_EXCESS,
          long: licenseI18nKeys.LONG_DATE_WARNING2 + '<hr/>' + licenseI18nKeys.LONG_USER_EXCESS,
        },
      },
    ];

    dataSets.forEach((dataSet) =>
      it(dataSet.name, () => {
        component.licenseInformation = dataSet.licenseInformation;
        expect(component.messageHelper.getShortMessage(dataSet.placement, true)).toBe(
          dataSet.expected.short,
        );
        expect(component.messageHelper.getLongMessage(dataSet.placement, true)).toBe(
          dataSet.expected.long,
        );
      }),
    );
  });

  const visibilityRules = {
    // users1 | users2 | date1 | date2 | date3
    [LicenseMessagePlacement.HOME]: ['a', 'au', 'a', 'a', 'au'],
    [LicenseMessagePlacement.WORKSPACES]: ['a', 'au', '', 'a', 'au'],
    [LicenseMessagePlacement.ADMIN_WORKSPACE]: ['a', 'au', 'a', 'a', 'au'],
    [LicenseMessagePlacement.USER_CREATION]: ['au', 'au', '', '', ''],
    [LicenseMessagePlacement.ON_LOGGED_IN]: ['', 'a', '', '', 'a'],
  };

  it('should show short license message based on placement and user profile', () => {
    const doCheck = (admin: boolean, entry: [string, string[]]) => {
      const [u1, u2, d1, d2, d3] = entry[1];
      const token = admin ? 'a' : 'u';

      component.isAdmin = admin;
      component.placement = entry[0] as LicenseMessagePlacement;
      component.licenseInformation = {
        activatedUserExcess: '1-1-true',
        pluginLicenseExpiration: null,
      };

      const expected1 = u1.includes(token) ? licenseI18nKeys.SHORT_USER_WARNING : '';
      const output1 = token + ' first user warning ' + entry[0];
      expect(component.shortMessage).toBe(expected1, output1);

      component.licenseInformation = {
        activatedUserExcess: '1-1-false',
        pluginLicenseExpiration: null,
      };

      const expected2 = u2.includes(token) ? licenseI18nKeys.SHORT_USER_EXCESS : '';
      const output2 = token + ' second user warning ' + entry[0];
      expect(component.shortMessage).toBe(expected2, output2);

      component.licenseInformation = {
        activatedUserExcess: null,
        pluginLicenseExpiration: '50',
      };

      const expected3 = d1.includes(token) ? licenseI18nKeys.SHORT_DATE_WARNING : '';
      const output3 = token + ' first date warning ' + entry[0];
      expect(component.shortMessage).toBe(expected3, output3);

      component.licenseInformation = {
        activatedUserExcess: null,
        pluginLicenseExpiration: '20',
      };

      const expected4 = d2.includes(token) ? licenseI18nKeys.SHORT_DATE_WARNING : '';
      const output4 = token + ' second date warning ' + entry[0];
      expect(component.shortMessage).toBe(expected4, output4);

      component.licenseInformation = {
        activatedUserExcess: null,
        pluginLicenseExpiration: '-1',
      };

      const expected5 = d3.includes(token) ? licenseI18nKeys.SHORT_DATE_EXPIRED : '';
      const output5 = token + ' third date warning ' + entry[0];
      expect(component.shortMessage).toBe(expected5, output5);
    };

    Object.entries(visibilityRules).forEach((entry) => {
      doCheck(true, entry);
      doCheck(false, entry);
    });
  });
});

interface DataSet {
  name: string;
  licenseInformation: LicenseInformationState;
  placement: LicenseMessagePlacement;
  expected: {
    hasMessage: boolean;
    short: string;
    long: string;
  };
}
