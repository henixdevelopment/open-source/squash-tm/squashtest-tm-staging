import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  Optional,
} from '@angular/core';
import { ReferentialDataService } from '../../../../../core/referential/services/referential-data.service';
import { AdminReferentialDataService } from '../../../../../core/referential/services/admin-referential-data.service';
import { takeUntil } from 'rxjs/operators';
import { combineLatest, Subject } from 'rxjs';

@Component({
  selector: 'sqtm-core-configurable-banner',
  template: `@if (bannerMessage) {
    <div
      class="p-5 container-background-color wrapper overflow-hidden"
      [innerHTML]="bannerMessage | safeRichContent"
    ></div>
  }`,
  styleUrl: './configurable-banner.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfigurableBannerComponent implements OnInit, OnDestroy {
  bannerMessage: string;

  private readonly _unsub$ = new Subject<void>();

  constructor(
    @Optional() private readonly adminReferentialDataService: AdminReferentialDataService,
    @Optional() private readonly referentialDataService: ReferentialDataService,
    private readonly changeDetectorRef: ChangeDetectorRef,
  ) {}

  ngOnDestroy(): void {
    this._unsub$.next();
    this._unsub$.complete();
  }

  ngOnInit(): void {
    combineLatest([
      this.adminReferentialDataService.globalConfiguration$,
      this.referentialDataService.globalConfiguration$,
    ])
      .pipe(takeUntil(this._unsub$))
      .subscribe(([adminGlobalConfig, globalConfig]) => {
        this.bannerMessage = adminGlobalConfig.bannerMessage ?? globalConfig.bannerMessage;
        this.changeDetectorRef.detectChanges();
      });
  }
}
