import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'sqtm-core-third-party-credentials-form-status',
  templateUrl: './third-party-credentials-form-status.component.html',
  styleUrl: './third-party-credentials-form-status.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ThirdPartyCredentialsFormStatusComponent {
  @Input()
  statusIcon: 'DANGER' | 'WARNING' | 'INFO' = null;

  @Input()
  statusMessage: string = null;

  getIconType() {
    switch (this.statusIcon) {
      case 'DANGER':
        return 'exclamation-circle';
      default:
        return 'info-circle';
    }
  }

  getIconColor() {
    switch (this.statusIcon) {
      case 'DANGER':
        return '#FF0000';
      case 'WARNING':
        return '#FFCC00';
      case 'INFO':
        return '#1890FF';
      default:
        return 'black';
    }
  }
}
