import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GroupedMultiListFieldComponent } from './grouped-multi-list-field.component';
import { OverlayModule } from '@angular/cdk/overlay';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

describe('GroupedMultiListFieldComponent', () => {
  let component: GroupedMultiListFieldComponent;
  let fixture: ComponentFixture<GroupedMultiListFieldComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [OverlayModule],
      declarations: [GroupedMultiListFieldComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj(['instant']),
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupedMultiListFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
