import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MultipleRadioButtonComponent } from './multiple-radio-button.component';
import { TranslateModule } from '@ngx-translate/core';

describe('MultipleRadioButtonComponent', () => {
  let component: MultipleRadioButtonComponent;
  let fixture: ComponentFixture<MultipleRadioButtonComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [MultipleRadioButtonComponent],
      imports: [TranslateModule.forRoot()],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipleRadioButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
