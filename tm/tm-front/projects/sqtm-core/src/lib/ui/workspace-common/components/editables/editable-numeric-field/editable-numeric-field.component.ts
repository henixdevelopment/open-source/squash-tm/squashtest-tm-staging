import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { ValidationErrors, ValidatorFn } from '@angular/forms';
import { EditableCustomField } from '../../../../custom-field/editable-custom-field';
import { AbstractEditableField, EditableField, EditableLayout } from '../abstract-editable-field';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { wsCommonLogger } from '../../../workspace.common.logger';
import { isEnterKeyboardEvent, isEscapeKeyboardEvent } from '../../../../utils/key-utils';

const logger = wsCommonLogger.compose('EditableNumericFieldComponent');

@Component({
  selector: 'sqtm-core-editable-numeric-field',
  templateUrl: './editable-numeric-field.component.html',
  styleUrls: ['./editable-numeric-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditableNumericFieldComponent
  extends AbstractEditableField
  implements OnInit, OnDestroy, EditableField, EditableCustomField
{
  private readonly REQUIRED_KEY = 'sqtm-core.validation.errors.required';

  edit = false;

  dirty = false;

  pending = false;

  @Input()
  placeholder: string;

  @Input()
  size: 'small' | 'default' | 'large' = 'default';

  @Input()
  max: number;

  @Input()
  min: number;

  @Input()
  layout: EditableLayout = 'default';

  @Input()
  required = false;

  @Input()
  set validators(validators: ValidatorFn[]) {
    throw Error(
      'Validators are not handled in Numeric Field. Used direct controls of NgZorro component',
    );
  }

  @Output()
  confirmEvent = new EventEmitter<string>();

  // The bounded input's value can be either a number or an empty string
  model: string | number;

  private _value: number;

  @Input()
  set value(value: number) {
    this.updateAndClose(value);
  }

  get value() {
    return this._value;
  }

  externalErrors: string[] = [];

  private disableKeyboardEvent = new Subject<void>();

  // ChangeDetectorRef is public for testing purpose. Classic detectChange() on fixture didn't work in that case.
  constructor(
    cdRef: ChangeDetectorRef,
    private translateService: TranslateService,
  ) {
    super(cdRef);
  }

  ngOnInit() {}

  ngOnDestroy(): void {
    this.disableKeyboardEvent.next();
    this.disableKeyboardEvent.complete();
  }

  enableEditMode() {
    if (this.editable && !this.edit) {
      this.removeExternalErrors();
      this.edit = true;
      this.model = this._value == null ? null : this._value.toString();
    }
    return false;
  }

  disableEditMode() {
    this.edit = false;
    this.pending = false;
  }

  cancel() {
    this.disableEditMode();
  }

  confirm() {
    this.dirty = true;
    this.removeExternalErrors();

    if (this.required && this.isInputBlank()) {
      this.externalErrors.push(this.translateService.instant(this.REQUIRED_KEY));
    } else {
      this.executeAutoAsync();
      this.confirmEvent.emit(this.getInputFieldValue());
    }
  }

  beginAsync() {
    this.pending = true;
  }

  endAsync() {
    this.pending = false;
  }

  private updateAndClose(value: number) {
    logger.debug('updating editable numeric field with value : ' + value);
    this.endAsync();
    this.disableEditMode();
    this._value = value;
    this.model = value == null ? null : value.toString();
    this.cdRef.detectChanges();
  }

  private getInputFieldValue() {
    return typeof this.model === 'string' ? this.model : this.model.toString();
  }

  getErrors(errors: ValidationErrors): string[] {
    const messages = [];
    if (errors) {
      Object.keys(errors).forEach((errorKey) => {
        messages.push(this.translateService.instant('validation.errors.' + errorKey));
      });
    }
    return messages;
  }

  handleKeyboardInput(event: KeyboardEvent) {
    if (isEnterKeyboardEvent(event)) {
      this.confirm();
    } else if (isEscapeKeyboardEvent(event)) {
      this.cancel();
    }
    return false;
  }

  handleBlur(event) {
    if (this.checkIfConfirmOrCancelButtonHaveNotBeenClicked(event)) {
      this.confirm();
    }
  }

  checkIfConfirmOrCancelButtonHaveNotBeenClicked(event) {
    // We don't want the blur event to have any impact when we click on the confirm and cancel buttons, so we check that
    // the related target (where the click occurs when the focus is lost) does not have the two CSS classes below.
    // If the click is outside the editable-field component, the related target of focus event is always null
    return (
      event.relatedTarget == null ||
      (!event.relatedTarget.className.includes('editable-numeric-field-cancel-button') &&
        !event.relatedTarget.className.includes('editable-numeric-field-confirm-button'))
    );
  }

  getComponentClasses() {
    const cssClass = [];
    if (this.edit) {
      cssClass.push('edit');
    } else {
      cssClass.push('read');
      if (this.editable) {
        cssClass.push('editable');
      }
    }
    return cssClass;
  }

  showExternalErrorMessage(externalErrors: string[]) {
    this.endAsync();
    this.externalErrors = externalErrors;
    this.cdRef.detectChanges();
  }

  private removeExternalErrors() {
    this.externalErrors = [];
  }

  private isInputBlank() {
    return this.getInputFieldValue().trim() === '';
  }
}
