import {
  ComponentFactoryResolver,
  ComponentRef,
  Directive,
  Type,
  ViewContainerRef,
} from '@angular/core';

@Directive({
  selector: '[sqtmCoreDynamicComponent]',
})
export class DynamicComponentDirective<T> {
  type: Type<T>;

  constructor(
    private viewContainerRef: ViewContainerRef,
    private componentFactoryResolver: ComponentFactoryResolver,
  ) {}

  instantiateComponent(): ComponentRef<T> {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.type);
    this.viewContainerRef.clear();
    return this.viewContainerRef.createComponent(componentFactory);
  }
}
