import { ChangeDetectorRef, Directive, ElementRef, Host, OnDestroy, OnInit } from '@angular/core';
import * as CssElementQuery from 'css-element-queries';
import { ResizeSensor } from 'css-element-queries';

/**
 * Triggers a change detection whenever the host element changes its size.
 * Note: the host element must have appropriate CSS properties (e.g. display: block) so that its size is computable.
 */
@Directive({
  selector: '[sqtmCoreRefreshOnResize]',
})
export class RefreshOnResizeDirective implements OnInit, OnDestroy {
  private resizeSensor: ResizeSensor;

  constructor(
    @Host() private readonly hostElement: ElementRef,
    private cdRef: ChangeDetectorRef,
  ) {}

  ngOnInit(): void {
    this.initResizeSensor();
  }

  ngOnDestroy(): void {
    this.detachResizeSensor();
  }

  private initResizeSensor(): void {
    this.resizeSensor = new CssElementQuery.ResizeSensor(this.hostElement.nativeElement, () =>
      this.cdRef.detectChanges(),
    );
  }

  private detachResizeSensor() {
    if (this.resizeSensor) {
      this.resizeSensor.detach();
    }
  }
}
