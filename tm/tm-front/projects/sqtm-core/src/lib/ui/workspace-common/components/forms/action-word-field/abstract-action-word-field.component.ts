import { ChangeDetectorRef, Directive, OnInit } from '@angular/core';
import { AbstractFormField } from '../abstract-form-field';

@Directive({
  providers: [
    {
      provide: AbstractFormField,
      useExisting: AbstractActionWordFieldComponent,
    },
  ],
})
export abstract class AbstractActionWordFieldComponent extends AbstractFormField implements OnInit {
  protected constructor(cdr: ChangeDetectorRef) {
    super(cdr);
  }

  ngOnInit(): void {}

  abstract focusInput();

  abstract blurInput();
}
