import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { Option } from '../../../../../model/option.model';
import { EditableCustomField } from '../../../../custom-field/editable-custom-field';
import { ValidatorFn } from '@angular/forms';
import { AbstractEditableField, EditableField, EditableLayout } from '../abstract-editable-field';
import { wsCommonLogger } from '../../../workspace.common.logger';
import { TranslateService } from '@ngx-translate/core';
import { NzSelectComponent } from 'ng-zorro-antd/select';

const LOGGER = wsCommonLogger.compose('EditableSelectFieldComponent');

@Component({
  selector: 'sqtm-core-editable-select-field',
  templateUrl: 'editable-select-field.component.html',
  styleUrls: ['./editable-select-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditableSelectFieldComponent
  extends AbstractEditableField
  implements EditableField, EditableCustomField
{
  @ViewChild('selectFieldWrapper', { static: true })
  selectFieldWrapper: ElementRef;

  @ViewChild('valueWrapper', { static: true })
  valueWrapper: ElementRef;

  @ViewChild('selectField')
  selectField: NzSelectComponent;

  @Input()
  layout: EditableLayout = 'default';

  @Input()
  set edit(isEdit: boolean) {
    this._edit = isEdit;
    this.toggleEditEvent.emit(isEdit);
    this.removeEditMarkerClasses();
    this.addMarkerClasses();
  }

  get edit() {
    return this._edit;
  }

  @Input()
  set editable(editable: boolean) {
    this._editable = editable;
    this.removeEditMarkerClasses();
    this.addMarkerClasses();
  }

  get editable(): boolean {
    return this._editable;
  }

  @Input()
  allowEmptyValue = false;

  @Input()
  showsPlaceholderAsFirstOption = true;

  @Input()
  pending = false;

  @Input()
  size: 'small' | 'default' | 'large' = 'default';

  @Input()
  set validators(validators: ValidatorFn[]) {
    throw Error('Validators are not handled in Select Field');
  }

  /**
   * Temp value set to user choice in NzSelect, before the confirm or cancel action is fired.
   */
  transientValue: string;

  @Input()
  options: Option[] = [];

  @Input()
  excludedKeys: any[] = [];

  @Input()
  placeHolder: string;

  @Input()
  searchable = true;

  private _edit: boolean;
  private _value: string;

  /**
   * Set the value, end all async operations and close editable mode
   * @param value the new value
   */
  @Input()
  set value(value: string) {
    this._value = value;
    this.transientValue = value;
    this.updateAndClose();
  }

  get value(): string {
    return this._value;
  }

  @Output()
  readonly confirmEvent = new EventEmitter<Option>();

  @Output()
  readonly toggleEditEvent = new EventEmitter<boolean>();

  constructor(
    cdRef: ChangeDetectorRef,
    public readonly translateService: TranslateService,
    private renderer: Renderer2,
  ) {
    super(cdRef);
  }

  get allOptions(): Option[] {
    if (this.showsPlaceholderAsFirstOption && this.allowEmptyValue) {
      return [
        {
          value: null,
          label: this.placeHolder,
        },
        ...this.options,
      ];
    }

    return this.options;
  }

  disableEditModeIfSelectedValueIsTheSameInNoButtonMode($event) {
    if ($event === false && this.layout === 'no-buttons' && this.transientValue === this.value) {
      this.cancel();
    }
  }

  enableEditMode() {
    if (this.editable) {
      if (this.excludedKeys.includes(this._value)) {
        this.transientValue = '';
      } else {
        this.transientValue = this.value;
      }
      this.displaySelectField();
    }
    LOGGER.debug(`Transient Value = ${this.transientValue}`);
    return false;
  }

  cancel() {
    if (this.excludedKeys.includes(this._value)) {
      this.transientValue = '';
    } else {
      this.transientValue = this.value;
    }
    this.disableEditMode();
  }

  confirm() {
    if (this.transientValue !== this.value) {
      this.executeAutoAsync();
      this.confirmEvent.emit(this.getOptionFromValue(this.transientValue));
    } else {
      this.cancel();
    }
  }

  change($event: string) {
    this.transientValue = $event;
    if (this.layout === 'no-buttons') {
      this.confirm();
    }
  }

  beginAsync() {
    this.pending = true;
  }

  endAsync() {
    this.pending = false;
  }

  private updateAndClose() {
    this.endAsync();
    this.disableEditMode();
  }

  private getOptionFromValue(value: string | number): Option {
    const optionFound = this.options.find((option) => option.value === value);
    if (optionFound) {
      return optionFound;
    } else {
      if (!value && this.allowEmptyValue) {
        return null;
      }
    }
    throw Error(
      `Unable to find option ${value} inside available options : ${JSON.stringify(this.options)}`,
    );
  }

  disableEditMode() {
    this.displayValue();
    this.pending = false;
  }

  getOptionLabel(key: string | number) {
    return this.getOptionFromValue(key).label;
  }

  getVisibleOptions(options: Option[], excludedKeys: string[]): Option[] {
    return options.filter((opt) => !excludedKeys.includes(opt.value));
  }

  getComponentClasses(): string[] {
    const cssClass = [];
    if (this.edit) {
      cssClass.push('edit');
    } else {
      cssClass.push('read');
      if (this.editable) {
        cssClass.push('editable');
      }
    }
    return cssClass;
  }

  trackByOptionValue(index: number, option: Option): any {
    return option.value;
  }

  // avoid flash of placeholder in native entity field if no value and no-optional field
  // cuf have no problems because the cuf widget will only instantiate after entity has been fetched and loaded in store. So no flash.
  mustShowPlaceHolder(value: string, allowEmptyValue: boolean) {
    return this.editable && allowEmptyValue && !value;
  }

  getActualPlaceHolder() {
    return (
      this.placeHolder || this.translateService.instant('sqtm-core.generic.editable.placeholder')
    );
  }

  displaySelectField() {
    this.hideValueWrapper();
    this.renderer.removeClass(this.selectFieldWrapper.nativeElement, 'hide');
    this.renderer.addClass(this.selectFieldWrapper.nativeElement, 'flex-column');
    this.selectField.setOpenState(true);
  }

  private hideValueWrapper() {
    this.removeEditMarkerClasses();
    this.renderer.addClass(this.valueWrapper.nativeElement, 'hide');
  }

  private removeEditMarkerClasses() {
    this.renderer.removeClass(this.valueWrapper.nativeElement, 'edit');
    this.renderer.removeClass(this.valueWrapper.nativeElement, 'read');
    this.renderer.removeClass(this.valueWrapper.nativeElement, 'editable');
  }

  displayValue() {
    this.renderer.removeClass(this.valueWrapper.nativeElement, 'hide');
    this.addMarkerClasses();
    this.renderer.addClass(this.selectFieldWrapper.nativeElement, 'hide');
  }

  private addMarkerClasses() {
    const classes = this.getComponentClasses();
    for (const cssClass of classes) {
      this.renderer.addClass(this.valueWrapper.nativeElement, cssClass);
    }
  }

  handleClickOut() {
    this.cancel();
  }
}
