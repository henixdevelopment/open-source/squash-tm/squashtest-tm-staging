import { Directive, Input } from '@angular/core';
import { throwError } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Directive()
export class AbstractCapsuleInformationDirective {
  _informationData: CapsuleInformationData;

  @Input()
  set informationData(informationData: CapsuleInformationData) {
    this._informationData = informationData;
  }

  get informationData() {
    return this._informationData;
  }

  constructor(public translateService: TranslateService) {}

  getTooltipTitle(informationData: CapsuleInformationData) {
    if (informationData.title) {
      return informationData.title;
    } else if (informationData.titleI18nKey) {
      return this.translateService.instant(informationData.titleI18nKey);
    }
  }

  getDisplayValue(informationData: CapsuleInformationData) {
    if (informationData.label) {
      return informationData.label;
    } else if (informationData.labelI18nKey) {
      return this.translateService.instant(informationData.labelI18nKey);
    } else {
      throwError(() => 'No value to display');
    }
  }

  getStyle(color: string) {
    if (color) {
      return { border: `1px solid ${color}` };
    } else {
      return { border: `1px solid var(--container-border-color)` };
    }
  }

  getIconColor(color: string) {
    if (color) {
      return color;
    } else {
      return '#666666';
    }
  }
}

export interface CapsuleInformationData {
  id?: string | number;
  label?: string;
  labelI18nKey?: string;
  color?: string;
  title?: string;
  titleI18nKey?: string;
  icon?: string;
}
