import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { formatDate } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { getSupportedBrowserLang } from '../../../../../core/utils/browser-langage.utils';

// A simple component that displays a time and a user, used for 'Created' and 'Modified' fields
@Component({
  selector: 'sqtm-core-auditable-field',
  template: `<span nz-tooltip [sqtmCoreLabelTooltip]="text" class="text-ellipsis">{{
    text
  }}</span>`,
  styleUrls: ['./auditable-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuditableFieldComponent implements OnInit {
  @Input() date: string | Date;
  @Input() userName: string;

  constructor(private readonly translateService: TranslateService) {}

  ngOnInit(): void {}

  get text(): string {
    if (this.date == null) {
      return this.translateService.instant('sqtm-core.generic.label.never');
    }

    const dateFormatted = formatDate(
      this.date,
      'short',
      getSupportedBrowserLang(this.translateService),
    );
    return `${dateFormatted} (${this.userName})`;
  }
}
