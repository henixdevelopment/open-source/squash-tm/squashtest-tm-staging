import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'sqtm-core-back-button',
  template: `
    <div
      class="back-button flex-column __hover_pointer"
      (click)="handleBackButtonClicked()"
      [attr.data-test-button-id]="'back'"
    >
      <i
        nz-icon
        nzType="sqtm-core-generic:back"
        nzTheme="outline"
        class="current-workspace-button action-icon-size m-auto"
        nz-tooltip
        [nzTooltipTitle]="'sqtm-core.generic.label.return' | translate"
      >
      </i>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BackButtonComponent {
  @Output()
  buttonPressed = new EventEmitter<void>();

  constructor() {}

  handleBackButtonClicked(): void {
    this.buttonPressed.emit();
  }
}
