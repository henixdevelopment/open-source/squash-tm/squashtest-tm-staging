import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  OnDestroy,
  Output,
  ViewChild,
} from '@angular/core';
import { fromEvent, Subject } from 'rxjs';
import { debounceTime, map, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'sqtm-core-text-research-field',
  templateUrl: './text-research-field.component.html',
  styleUrls: ['./text-research-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TextResearchFieldComponent implements AfterViewInit, OnDestroy {
  researchValue = '';

  @Output()
  newResearchValue = new EventEmitter<string>();

  @ViewChild('textInput') textInput: ElementRef;

  private unsub$ = new Subject<void>();

  ngAfterViewInit(): void {
    fromEvent(this.textInput.nativeElement, 'input')
      .pipe(
        takeUntil(this.unsub$),
        debounceTime(500),
        map((event: any) => event.target.value),
      )
      .subscribe(() => this.handleKeyboardInput());
  }

  handleKeyboardInput() {
    this.emitNewValue();
  }

  resetValue() {
    this.researchValue = '';
    this.emitNewValue();
    this.textInput.nativeElement.focus();
  }

  private emitNewValue() {
    this.newResearchValue.next(this.researchValue);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
