import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { ListItem } from '../grouped-multi-list/grouped-multi-list.component';
import { NULL_VALUE_IN_FILTER } from '../../../../filters/state/filter.state';
import { TranslateService } from '@ngx-translate/core';

export interface UserListElement extends ListItem {
  id: number; // sometimes null if user was deleted
  login: string;
  firstName?: string;
  lastName?: string;
  selected: boolean;
}

@Component({
  selector: 'sqtm-core-user-list-selector',
  templateUrl: './user-list-selector.component.html',
  styleUrls: ['./user-list-selector.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserListSelectorComponent implements OnInit {
  private _users: UserListElement[];

  private userMap = new Map<string, UserListElement>();

  @Input()
  showDropShadow = true;

  @Input()
  set users(userListElements: UserListElement[]) {
    this._users = userListElements;
    this.userMap = new Map<string, UserListElement>(userListElements.map((u) => [u.login, u]));
  }

  get usersAsList(): ListItem[] {
    return Array.from(this.userMap.values()).map((user) => {
      const label = this.getUserLabel(user);
      return {
        id: user.login,
        selected: user.selected,
        label,
      };
    });
  }

  @Output()
  userToggled = new EventEmitter<UserListElement>();

  constructor(private translateService: TranslateService) {}

  ngOnInit() {}

  private getUserLabel(user: UserListElement) {
    if (user.id) {
      return `${user.firstName} ${user.lastName} (${user.login})`;
    } else if (user.login === NULL_VALUE_IN_FILTER) {
      return this.translateService.instant('sqtm-core.entity.iteration.itpi.unaffected');
    } else {
      return user.login;
    }
  }

  changeUserSelection(listItem: ListItem) {
    const userListElement = { ...this.userMap.get(listItem.id.toString()) };
    userListElement.selected = listItem.selected;
    this.userToggled.emit(userListElement);
  }
}
