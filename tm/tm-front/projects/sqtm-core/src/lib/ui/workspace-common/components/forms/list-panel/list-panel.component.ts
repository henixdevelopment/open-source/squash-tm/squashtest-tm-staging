import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface ListPanelItem {
  id: string | number;
  label: string;
  icon?: string;
  color?: string;
}

@Component({
  selector: 'sqtm-core-list-panel',
  templateUrl: './list-panel.component.html',
  styleUrls: ['./list-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListPanelComponent {
  initialItems: ListPanelItem[] = [];

  filteredItems$ = new BehaviorSubject<ListPanelItem[]>([]);

  _selectedItem: string | number;

  searchInput: string;

  @Input()
  searchable = false;

  @Input()
  set selectedItem(value: string | number) {
    this._selectedItem = value;
  }

  @Input()
  set items(items: ListPanelItem[]) {
    this.initialItems = items;
    this.updateFilteredItems();
  }

  @Output()
  itemSelectionChanged = new EventEmitter<string | number>();

  change(id) {
    this.selectedItem = id;
    this.itemSelectionChanged.emit(id);
  }

  searchList(searchInput: string) {
    this.searchInput = searchInput;
    this.updateFilteredItems();
  }

  private updateFilteredItems() {
    if (this.searchInput == null || this.searchInput === '') {
      this.filteredItems$.next(this.initialItems);
    } else {
      const filteredList = this.initialItems.filter((item) =>
        item.label.toLowerCase().includes(this.searchInput.toLowerCase()),
      );
      this.filteredItems$.next(filteredList);
    }
  }

  trackByFn(index: number, listPanelItem: ListPanelItem) {
    return listPanelItem.id;
  }
}
