import { ChangeDetectorRef, Directive, Input } from '@angular/core';

export interface EditableField {
  editable: boolean;

  autoAsync: boolean;

  confirm();

  enableEditMode();

  cancel();

  beginAsync();

  endAsync();

  disableEditMode();

  markForCheck();
}

@Directive()
export abstract class AbstractEditableField implements EditableField {
  protected _editable = true;

  @Input()
  autoAsync = true;

  @Input()
  hideButtons = false;

  @Input()
  set editable(editable: boolean) {
    this._editable = editable;
    this.cdRef.detectChanges();
  }

  get editable(): boolean {
    return this._editable;
  }

  abstract layout: EditableLayout;

  // ChangeDetectorRef is public for testing purpose. Classic detectChange() on fixture didn't work in that case.
  constructor(public cdRef: ChangeDetectorRef) {}

  mustShowButtonLabels(): boolean {
    return this.layout === 'default';
  }

  mustShowButtons(): boolean {
    return this.layout !== 'no-buttons';
  }

  getInputFieldCss() {
    const classes = [];
    if (this.layout === 'compact') {
      classes.push('no-right-border-radius');
    }
    return classes;
  }

  getConfirmButtonCss() {
    const classes = [];
    if (this.layout === 'compact') {
      classes.push('no-border-radius');
    } else {
      classes.push('m-l-10');
    }
    return classes;
  }

  getCancelButtonCss() {
    const classes = [];
    if (this.layout === 'compact') {
      classes.push('no-left-border-radius');
    } else {
      classes.push('m-l-10');
    }
    return classes;
  }

  protected executeAutoAsync() {
    if (this.autoAsync) {
      this.beginAsync();
    }
  }

  public markForCheck() {
    this.cdRef.markForCheck();
  }

  abstract confirm();

  abstract enableEditMode();

  abstract cancel();

  abstract beginAsync();

  abstract endAsync();

  abstract disableEditMode();
}

export type EditableLayout = 'compact' | 'no-buttons' | 'default';
