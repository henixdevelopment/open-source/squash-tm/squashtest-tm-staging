import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ColorPickerSelectFieldComponent } from './color-picker-select-field.component';
import { OverlayModule } from '@angular/cdk/overlay';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('ColorPickerSelectFieldComponent', () => {
  let component: ColorPickerSelectFieldComponent;
  let fixture: ComponentFixture<ColorPickerSelectFieldComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [OverlayModule],
      declarations: [ColorPickerSelectFieldComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColorPickerSelectFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
