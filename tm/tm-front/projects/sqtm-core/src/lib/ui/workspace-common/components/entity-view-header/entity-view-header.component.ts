import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import {
  AttachmentListState,
  getPersistedAttachmentCount,
} from '../../../../core/services/entity-view/entity-view.state';

/**
 * This component is intended to be used as a header in pages which displays a node information (Library, Folder, TestCase...).
 * It contains a left part including a title and an area which displays the content projection. A right part contains
 * the 'more' icon and the 'attachment' icon that displays the number of Attachments.
 */
@Component({
  selector: 'sqtm-core-entity-view-header',
  templateUrl: './entity-view-header.component.html',
  styleUrls: ['./entity-view-header.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EntityViewHeaderComponent {
  @Input()
  attachmentList: AttachmentListState;

  @Input()
  displayDefaultTitle = true;

  @Output()
  attachmentBadgeClickEvent = new EventEmitter<void>();

  @Input()
  nameEditable = true;

  @Input()
  hasAttachmentList = true;

  @Input()
  entityPath: string;

  get attachmentCount(): number {
    if (this.attachmentList?.attachments) {
      return getPersistedAttachmentCount(this.attachmentList.attachments);
    } else {
      return 0;
    }
  }

  constructor() {}

  attachmentBadgeClick() {
    this.attachmentBadgeClickEvent.next();
  }
}
