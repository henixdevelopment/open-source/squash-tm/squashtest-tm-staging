import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EditableSelectLevelEnumFieldComponent } from './editable-select-level-enum-field.component';
import { TranslateModule } from '@ngx-translate/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('EditableSelectLevelEnumFieldComponent', () => {
  let component: EditableSelectLevelEnumFieldComponent;
  let fixture: ComponentFixture<EditableSelectLevelEnumFieldComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [EditableSelectLevelEnumFieldComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableSelectLevelEnumFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /*
  it('should not pass in edit mode when clicking on it if edition is forbidden', () => {
    component.editable = false;
    fixture.detectChanges();
    const select = fixture.debugElement.query(By.css('sqtm-core-editable-select-field'));
    expect(select.nativeElement.getAttribute('editable')).toEqual('false');
  });
   */
});
