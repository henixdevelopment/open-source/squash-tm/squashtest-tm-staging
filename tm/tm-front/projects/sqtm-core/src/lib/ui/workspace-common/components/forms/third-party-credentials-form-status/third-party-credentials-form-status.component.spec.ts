import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThirdPartyCredentialsFormStatusComponent } from './third-party-credentials-form-status.component';

describe('ThirdPartyCredentialsFormStatusComponent', () => {
  let component: ThirdPartyCredentialsFormStatusComponent;
  let fixture: ComponentFixture<ThirdPartyCredentialsFormStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ThirdPartyCredentialsFormStatusComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ThirdPartyCredentialsFormStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
