import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  ViewChild,
} from '@angular/core';
import { AbstractFormField } from '../abstract-form-field';
import { FormGroup } from '@angular/forms';
import { FieldValidationError } from '../../../../../model/error/error.model';
import { CKEditor4, CKEditorComponent } from 'ckeditor4-angular';
import { TranslateService } from '@ngx-translate/core';
import {
  closeCkEditorDialog,
  createCkEditorConfig,
  hideNotification,
  isAllowedImageType,
  overrideCKEditorDefaultLinkTarget,
} from '../../../../utils/ck-editor-config';
import { SessionPingService } from '../../../../../core/services/session-ping/session-ping.service';
import { getSupportedBrowserLang } from '../../../../../core/utils/browser-langage.utils';
import { delay, take } from 'rxjs/operators';
import EditorType = CKEditor4.EditorType;

@Component({
  selector: 'sqtm-core-rich-text-field',
  templateUrl: './rich-text-field.component.html',
  styleUrls: ['./rich-text-field.component.less'],
  providers: [
    {
      provide: AbstractFormField,
      useExisting: RichTextFieldComponent,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RichTextFieldComponent extends AbstractFormField implements AfterViewInit, OnDestroy {
  @Input()
  formGroup: FormGroup;

  @Input()
  fieldName: string;

  /** If set to true, it will give the focus to this component at page initialisation */
  @Input()
  initialFocus = false;

  @Input()
  reducedHeight = false;

  @Input()
  height = 200;

  @ViewChild(CKEditorComponent)
  ckEditorComponent: CKEditorComponent;

  @Input()
  set serverSideFieldValidationError(fieldsValidationErrors: FieldValidationError[]) {
    this.showServerSideError(fieldsValidationErrors);
  }

  @Output()
  ckEditorReady = new EventEmitter<void>();

  ckConfig: CKEditor4.Config;

  ckEditorType = EditorType.CLASSIC;

  // Used for internal styling purposes. Should not be used externally.
  ckeditorVisible = false;

  constructor(
    cdRef: ChangeDetectorRef,
    private translateService: TranslateService,
    private readonly sessionPingService: SessionPingService,
  ) {
    super(cdRef);
    this.ckConfig = createCkEditorConfig(getSupportedBrowserLang(translateService), this.height);
  }

  ngAfterViewInit(): void {
    this.sessionPingService.beginLongTermEdition();

    if (this.initialFocus) {
      this.focusInputField();
    }
  }

  ngOnDestroy(): void {
    this.sessionPingService.endLongTermEdition();
  }

  public handleCKEditorReady($event: CKEditor4.EventInfo): void {
    this.reduceHeightForRichTextField($event);
    overrideCKEditorDefaultLinkTarget();
    this.ckeditorVisible = true;

    this.ckEditorReady.next();
  }

  private reduceHeightForRichTextField($event: CKEditor4.EventInfo) {
    if (this.reducedHeight) {
      $event.editor.resize('100%', 200);
    }
  }

  /**
   * Put the focus inside CKEditor's edition area.
   * Beware that the CKEditor instance must be properly initialized for this to work. You can observe the
   * `ckEditorReady` output if needed.
   *
   * @param cursorPosition: where to put the cursor (before or after content).
   */
  grabFocus(cursorPosition: 'start' | 'end' = 'end') {
    this.ckEditorComponent.instance.focus();

    if (cursorPosition === 'end') {
      const range = this.ckEditorComponent.instance.createRange();
      range.moveToElementEditEnd(range.root);
      this.ckEditorComponent.instance.getSelection().selectRanges([range]);
    }
  }

  grabFocusWhenReady(cursorPosition: 'start' | 'end' = 'end') {
    if (this.ckEditorComponent?.instance) {
      this.grabFocus(cursorPosition);
    } else {
      this.ckEditorReady
        .pipe(
          take(1),
          delay(1), // Wait for next cycle
        )
        .subscribe(() => this.grabFocus(cursorPosition));
    }
  }

  private focusInputField() {
    this.ckConfig['startupFocus'] = true;
  }

  handleFileUploadRequest(event: CKEditor4.EventInfo) {
    event.cancel();
    closeCkEditorDialog();

    const fileLoader = event.data.fileLoader;
    const imageSource = fileLoader.data;
    const fileName = fileLoader.fileName;
    const file: File = fileLoader.file;

    let notification: any;

    if (!isAllowedImageType(file)) {
      notification = event.editor.showNotification(
        this.translateService.instant('sqtm-core.attachment-drawer.image-upload.invalid', {
          fileName,
        }),
        'warning',
      );
    } else {
      this.ckEditorComponent.instance.insertHtml(
        '<img src="' + imageSource + '" alt="' + fileName + '">',
      );
      notification = event.editor.showNotification(
        this.translateService.instant('sqtm-core.attachment-drawer.image-upload.complete', {
          fileName,
        }),
        'success',
      );
    }
    hideNotification(notification);
  }
}
