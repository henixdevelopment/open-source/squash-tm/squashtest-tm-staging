import {
  AfterViewInit,
  Directive,
  Host,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { EditableRichTextComponent } from '../components/editables/editable-rich-text/editable-rich-text.component';
import { distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { SimplePermissions } from '../../../model/permissions/simple-permissions';
import { SqtmEntityState } from '../../../core/services/entity-view/entity-view.state';
import { EntityViewService } from '../../../core/services/entity-view/entity-view.service';

@Directive({
  selector: '[sqtmCoreEntityViewEditableRichTextField]',
})
export class EntityViewEditableRichTextFieldDirective<
    E extends SqtmEntityState,
    T extends string,
    P extends SimplePermissions,
  >
  implements OnInit, OnDestroy, AfterViewInit
{
  @Input('sqtmCoreEntityViewEditableRichTextField')
  fieldName: keyof E;

  unsub$ = new Subject<void>();

  constructor(
    private entityViewService: EntityViewService<E, T, P>,
    @Host() private editable: EditableRichTextComponent,
  ) {}

  @HostListener('confirmEvent', ['$event'])
  onConfirm(value: string) {
    this.editable.beginAsync();
    this.entityViewService.update(this.fieldName, value as any); // cannot use type system on dynamic inputs...
  }

  ngAfterViewInit(): void {}

  ngOnInit(): void {
    this.entityViewService.componentData$
      .pipe(
        takeUntil(this.unsub$),
        map((componentData) => componentData[componentData.type]),
        // If value has changed in store, the update is a success.
        // If there is any error server side, the error stream will emit, and the value will not be changed.
        distinctUntilChanged((previous, current) => this.fieldChanged(previous, current)),
      )
      .subscribe((entity) => {
        this.editable.value = entity[this.fieldName as string];
      });

    this.entityViewService.componentData$
      .pipe(takeUntil(this.unsub$))
      .subscribe((componentData) => {
        this.editable.editable =
          componentData.permissions.canWrite &&
          componentData.milestonesAllowModification &&
          this.editable.editable;
        this.editable.cdRef.detectChanges();
      });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private fieldChanged(previous: SqtmEntityState, current: SqtmEntityState) {
    return previous[this.fieldName as string] === current[this.fieldName as string];
  }
}
