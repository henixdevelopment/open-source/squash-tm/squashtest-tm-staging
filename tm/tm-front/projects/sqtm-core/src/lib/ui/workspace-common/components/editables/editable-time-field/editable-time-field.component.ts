import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { AbstractEditableField, EditableLayout } from '../abstract-editable-field';
import { EditableCustomField } from '../../../../custom-field/editable-custom-field';
import { NzInputNumberComponent } from 'ng-zorro-antd/input-number';
import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { TemplatePortal } from '@angular/cdk/portal';
import { ListPanelItem } from '../../forms/list-panel/list-panel.component';
import { KeyNames } from '../../../../utils/key-names';
import { NzButtonSize } from 'ng-zorro-antd/button';

@Component({
  selector: 'sqtm-core-editable-time-field',
  templateUrl: './editable-time-field.component.html',
  styleUrls: ['./editable-time-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditableTimeFieldComponent
  extends AbstractEditableField
  implements EditableCustomField, OnInit
{
  private overlayRef: OverlayRef;

  @ViewChild('templatePortalHoursContent', { read: TemplateRef })
  templatePortalHoursContent: TemplateRef<any>;

  @ViewChild('templatePortalMinutesContent', { read: TemplateRef })
  templatePortalMinutesContent: TemplateRef<any>;

  @ViewChild('listItemHoursRef', { read: ElementRef })
  listItemHoursRef: ElementRef;

  @ViewChild('listItemMinutesRef', { read: ElementRef })
  listItemMinutesRef: ElementRef;

  @ViewChild('hourInput', { read: NzInputNumberComponent })
  hourInput: NzInputNumberComponent;

  @ViewChild('minuteInput', { read: NzInputNumberComponent })
  minuteInput: NzInputNumberComponent;

  @Output()
  confirmEvent = new EventEmitter<number>();

  @Input()
  layout: EditableLayout = 'default';

  @Input()
  size: NzButtonSize = 'default';

  @Input()
  placeHolder: string;

  @Input()
  allowEmptyValue = false;

  @Input()
  set value(minutes: number) {
    if (minutes < 0) {
      throw new Error('Time value cannot be negative.');
    }

    this._value = minutes;
    this.synchronizeHourAndMinuteModel();
    this.updateAndClose(minutes);
  }

  get value(): number {
    return this._value;
  }

  listPanelHoursItems: ListPanelItem[] = [
    { id: 0, label: '00' },
    { id: 1, label: '01' },
    { id: 2, label: '02' },
    { id: 3, label: '03' },
    { id: 4, label: '04' },
    { id: 5, label: '05' },
    { id: 6, label: '06' },
    { id: 7, label: '07' },
    { id: 8, label: '08' },
    { id: 9, label: '09' },
    { id: 10, label: '10' },
    { id: 11, label: '11' },
    { id: 12, label: '12' },
  ];

  listPanelMinutesItems: ListPanelItem[] = [
    { id: 0, label: '00' },
    { id: 10, label: '10' },
    { id: 20, label: '20' },
    { id: 30, label: '30' },
    { id: 40, label: '40' },
    { id: 50, label: '50' },
  ];

  // last registered value expressed in minutes
  private _value: number;
  // current value expressed in minutes
  model = 0;
  hourModel: number;
  minuteModel: number;

  edit = false;
  pending: boolean;

  constructor(
    cdRef: ChangeDetectorRef,
    private overlay: Overlay,
    private vcr: ViewContainerRef,
  ) {
    super(cdRef);
  }

  ngOnInit() {
    this.synchronizeHourAndMinuteModel();
  }

  formatValue(value: number | string): string {
    if (value == null) {
      return '00';
    }
    const stringValue = value.toString();
    if (stringValue.length < 2) {
      return '0' + stringValue;
    } else {
      return stringValue;
    }
  }

  enableEditMode() {
    if (!this.edit && this.editable) {
      this.edit = true;
      this.model = this.value;

      this.synchronizeHourAndMinuteModel();
    }

    setTimeout(() => {
      this.hourInput.inputElement.nativeElement.select();
      this.hourInput.inputElement.nativeElement.click();
    });
  }

  private updateAndClose(value: number) {
    this._value = value;
    this.model = value;
    this.disableEditMode();
    this.endAsync();
    this.cdRef.detectChanges();
  }

  confirm() {
    this.model = this.hourModel * 60 + this.minuteModel;

    if (this.hourModel === 0) {
      this.hourModel = null;
    }

    if (this.model === this._value) {
      this.cancel();
    } else {
      this.executeAutoAsync();
      this.emitMinutes();
    }
  }

  private emitMinutes() {
    if (this.model == null) {
      this.confirmEvent.next(null);
    } else {
      this.confirmEvent.next(this.model);
    }
  }

  cancel() {
    this.edit = false;
    this.model = this._value;
    this.synchronizeHourAndMinuteModel();
  }

  beginAsync() {
    this.pending = true;
  }

  endAsync() {
    this.pending = false;
  }

  disableEditMode() {
    this.edit = false;
  }

  public synchronizeHourAndMinuteModel() {
    if (this.value == null) {
      this.hourModel = 0;
      this.minuteModel = 0;
    } else {
      this.hourModel = Math.floor(this.value / 60);
      this.minuteModel = this.value - this.hourModel * 60;
    }
  }

  handleHourChange($event: Event) {
    this.handleTimeChange($event, this.hourInput);
  }

  handleMinuteChange($event: Event) {
    this.handleTimeChange($event, this.minuteInput);
  }

  private handleTimeChange($event: Event, input: NzInputNumberComponent) {
    $event.stopPropagation();
    $event.preventDefault();

    let model: number;
    if (input === this.hourInput) {
      model = this.hourModel;
    } else if (input === this.minuteInput) {
      model = this.minuteModel;
    }

    const entryValue = input.inputElement.nativeElement.value;
    input.updateDisplayValue(this.constrainTimeInput(input, entryValue, model));
    if (input === this.hourInput) {
      this.hourModel = Number(input.displayValue);
    } else if (input === this.minuteInput) {
      this.minuteModel = Number(input.displayValue);
    }
    this.model = this.hourModel * 60 + this.minuteModel;
  }

  constrainTimeInput(input: NzInputNumberComponent, entryValue: string, model: number): number {
    let baseValue;
    if (model === this.hourModel) {
      baseValue = Math.floor(this.value / 60);
    } else if (model === this.minuteModel) {
      baseValue = this.value - this.hourModel * 60;
    }

    if (!entryValue && !model) {
      /* It refers to the case when the field is highlighted, no correct value has ever been entered since the
      initialization, and the user enters a special character: the character is not taken into account, so there is no value.
      As no correct value has ever been entered, there is no model. Thus, we need to return the value registered in database.
       */
      return baseValue;
    } else {
      return model;
    }
  }

  getPositiveEntryValue(entryValue: string) {
    if (isNaN(Number(entryValue))) {
      return 0;
    }
    return Number(entryValue.substring(1));
  }

  handlePickHourChange(item: ListPanelItem) {
    this.hourInput.updateDisplayValue(Number(item));
    this.hourInput.setValue(Number(item));
    this.model = this.hourModel * 60 + this.minuteModel;
    this.removeOverlay();
    this.showMinutesList();
  }

  handlePickMinuteChange(item: ListPanelItem) {
    this.minuteInput.updateDisplayValue(Number(item));
    this.minuteInput.setValue(Number(item));
    this.model = this.hourModel * 60 + this.minuteModel;
    this.removeOverlay();
  }

  showHoursList() {
    this.hourInput.inputElement.nativeElement.select();
    this.showListOverlay(this.listItemHoursRef, this.templatePortalHoursContent);
  }

  showListOverlay(elementRef: ElementRef, templateRef: TemplateRef<any>) {
    const positionStrategy = this.overlay
      .position()
      .flexibleConnectedTo(elementRef)
      .withPositions([
        {
          originX: 'center',
          overlayX: 'center',
          originY: 'center',
          overlayY: 'top',
          offsetY: 16,
          offsetX: 0,
        },
      ]);
    const scrollStrategy = this.overlay.scrollStrategies.block();

    const overlayConfig: OverlayConfig = {
      width: 50,
      scrollStrategy,
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
    };
    this.overlayRef = this.overlay.create(overlayConfig);
    const templatePortal = new TemplatePortal(templateRef, this.vcr);
    this.overlayRef.attach(templatePortal);
    this.overlayRef.backdropClick().subscribe(() => this.close());
  }

  showMinutesList() {
    this.minuteInput.inputElement.nativeElement.select();
    this.showListOverlay(this.listItemMinutesRef, this.templatePortalMinutesContent);
  }

  close() {
    this.removeOverlay();
    this.cdRef.detectChanges();
  }

  private removeOverlay() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
    }
  }

  @HostListener('window:keyup', ['$event'])
  handleKeyUp(event: KeyboardEvent) {
    if (event.key === KeyNames.TAB) {
      this.handleTabEvent();
    } else if (event.key === KeyNames.ESCAPE) {
      this.handleEscapeEvent();
    } else if (event.key === KeyNames.ENTER) {
      this.handleEnterEvent();
    }
  }

  private handleEnterEvent() {
    if (this.hourInput || this.minuteInput) {
      if (this.hourInput.isFocused) {
        this.removeOverlayAndSelectMinutesInput();
      } else if (this.minuteInput.isFocused) {
        this.removeOverlay();
        this.confirm();
      }
    }
  }

  private handleEscapeEvent() {
    this.cancel();
    this.removeOverlay();
    this.disableEditMode();
  }

  private handleTabEvent() {
    if (this.hourInput.isFocused) {
      this.removeOverlayAndSelectHourInput();
    } else if (this.minuteInput.isFocused) {
      this.removeOverlayAndSelectMinutesInput();
    } else {
      this.removeOverlay();
    }
  }

  private removeOverlayAndSelectHourInput() {
    this.removeOverlay();
    this.hourInput.inputElement.nativeElement.select();
    this.hourInput.inputElement.nativeElement.click();
  }

  private removeOverlayAndSelectMinutesInput() {
    this.removeOverlay();
    this.minuteInput.inputElement.nativeElement.select();
    this.minuteInput.inputElement.nativeElement.click();
  }

  handleInputKeyDown($event: KeyboardEvent): void {
    const excludedKeys = ['-', '.'];

    if (excludedKeys.includes($event.key)) {
      $event.stopPropagation();
      $event.preventDefault();

      const input = $event.target as HTMLInputElement;
      input.setSelectionRange(input.value.length, input.value.length);
    }
  }
}
