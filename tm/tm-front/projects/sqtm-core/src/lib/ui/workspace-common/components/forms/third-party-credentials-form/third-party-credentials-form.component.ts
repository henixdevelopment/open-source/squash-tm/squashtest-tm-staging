import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  ViewChildren,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TextFieldComponent } from '../text-field/text-field.component';
import {
  BasicAuthCredentials,
  Credentials,
  isBasicAuthCredentials,
  isOAuth2Credentials,
  isTokenAuthCredentials,
  OAuth2Credentials,
  TokenAuthCredentials,
} from '../../../../../model/third-party-server/credentials.model';
import { AuthenticationProtocol } from '../../../../../model/third-party-server/authentication.model';
import { BehaviorSubject } from 'rxjs';

const TRANSLATE_KEYS_BASE = 'sqtm-core.administration-workspace.bugtrackers.authentication-policy.';

const REVOCABLE_PROTOCOLS: AuthenticationProtocol[] = [
  AuthenticationProtocol.TOKEN_AUTH,
  AuthenticationProtocol.OAUTH_2,
];

@Component({
  selector: 'sqtm-core-third-party-credentials-form',
  templateUrl: './third-party-credentials-form.component.html',
  styleUrls: ['./third-party-credentials-form.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ThirdPartyCredentialsFormComponent implements OnDestroy {
  formGroup: FormGroup;

  @Input() useRevokeButton = false;
  @Input() useRemoveButton = false;

  /**
   * Authentication protocol defined at the bugtracker level. Beware that the configured protocol MAY NOT
   * match the stored credentials. E.g. if you configure a BT to use basic auth, have users register their
   * credentials and then change the BT protocol to OAuth. Users will still have Basic Auth credentials registered!
   */
  private _authenticationProtocol: AuthenticationProtocol;

  @Input()
  set authenticationProtocol(value: AuthenticationProtocol) {
    this._authenticationProtocol = value;
    this.buildCredentialsFormGroup();
  }

  get authenticationProtocol(): AuthenticationProtocol {
    return this._authenticationProtocol;
  }

  @Input()
  set credentials(credentials: Credentials) {
    this._credentials = credentials;
    this.buildCredentialsFormGroup();
  }

  get credentials(): Credentials {
    if (this._authenticationProtocol !== this._credentials?.implementedProtocol) {
      return this.makeEmptyCredentials();
    } else {
      // Reconstitute the "full" credentials object in order to have `implementedProtocol` and
      // `type` properties that may not be defined in `_credentials`
      return { ...this.makeEmptyCredentials(), ...(this._credentials ?? {}) };
    }
  }

  /**
   * If provided, the message will show under the form when any field gets focused or when the form is marked as dirty.
   */
  @Input()
  customModificationWarning?: string;

  @Input()
  loginFieldKey?: string;

  @Input()
  passwordFieldKey?: string;

  @ViewChildren(TextFieldComponent)
  textFields: TextFieldComponent[];

  @Output()
  submit = new EventEmitter<Credentials>();

  @Output()
  submitOauth2 = new EventEmitter<void>();

  @Output()
  revokeTokens = new EventEmitter<void>();

  // Used in template, should be kept internal
  readonly asyncMode$ = new BehaviorSubject<boolean>(false);
  private _credentials: Credentials;
  private readonly _obfuscatedPassword = '••••••';
  private _hasRegisteredPassword = false;
  private focusedFields: any[] = [];

  constructor(
    private readonly fb: FormBuilder,
    private readonly cdRef: ChangeDetectorRef,
  ) {}

  get passwordPlaceholder(): string {
    const shouldObfuscate = this._hasRegisteredPassword && this.formGroup?.pristine;
    return shouldObfuscate ? this._obfuscatedPassword : '';
  }

  get isEditable(): boolean {
    return this._authenticationProtocol !== AuthenticationProtocol.OAUTH_2;
  }

  get isRevokeButtonVisible(): boolean {
    return this.useRevokeButton && REVOCABLE_PROTOCOLS.includes(this._authenticationProtocol);
  }

  get isRemoveCredentialsVisible(): boolean {
    return (
      this.useRemoveButton && this._authenticationProtocol === AuthenticationProtocol.BASIC_AUTH
    );
  }

  get isRevokeOrRemoveButtonEnabled(): boolean {
    return this.credentials.registered;
  }

  get isOauth2SubmitButtonVisible(): boolean {
    return this.isOAuth2;
  }

  get shouldShowModificationWarning(): boolean {
    return this.customModificationWarning != null && (this.hasFocusedField || this.formGroup.dirty);
  }

  get hasFocusedField(): boolean {
    return this.focusedFields.length > 0;
  }

  get loginTranslateKey(): string {
    if (this.loginFieldKey != null) {
      return this.loginFieldKey;
    } else {
      return 'sqtm-core.generic.label.login.singular';
    }
  }

  get passwordTranslateKey(): string {
    if (this.isTokenAuth) {
      return TRANSLATE_KEYS_BASE + 'credentials.token';
    } else if (this.passwordFieldKey != null) {
      return this.passwordFieldKey;
    } else {
      return 'sqtm-core.generic.label.password';
    }
  }

  get isTokenAuth(): boolean {
    return this._authenticationProtocol === AuthenticationProtocol.TOKEN_AUTH;
  }

  get isOAuth2(): boolean {
    return this._authenticationProtocol === AuthenticationProtocol.OAUTH_2;
  }

  ngOnDestroy(): void {
    this.asyncMode$.complete();
  }

  handleSubmit(): void {
    if (this.checkCredentialsFormValidity()) {
      this.beginAsync();
      if (this.isTokenAuth) {
        const token = this.formGroup.controls['password'].value;
        const newCredentials = {
          ...this.credentials,
          token,
        };
        this.submit.emit(newCredentials);
      } else {
        const username = this.formGroup.controls['username'].value;
        const password = this.formGroup.controls['password'].value;
        const newCredentials = {
          ...this.credentials,
          username,
          password,
        };
        this.submit.emit(newCredentials);
      }
    }
  }

  handleOauth2Submit(): void {
    this.submitOauth2.emit();
  }

  beginAsync(): void {
    this.asyncMode$.next(true);
  }

  endAsync(): void {
    this.asyncMode$.next(false);
  }

  private buildCredentialsFormGroup(): void {
    if (this.credentials) {
      this.buildFormGroupFromCredentials(this.credentials);
    } else {
      this.formGroup = this.fb.group({
        username: this.fb.control('', [Validators.required]),
        password: this.fb.control('', [Validators.required]),
      });
    }

    this._hasRegisteredPassword = Boolean(this.credentials) && this.credentials.registered;
    this.cdRef.detectChanges();
  }

  private buildFormGroupFromCredentials(currentCredentials: Credentials): void {
    if (isBasicAuthCredentials(currentCredentials)) {
      this.formGroup = this.fb.group({
        username: this.fb.control(currentCredentials.username, [Validators.required]),
        password: this.fb.control('', [Validators.required]),
      });
    } else if (isTokenAuthCredentials(currentCredentials)) {
      this.formGroup = this.fb.group({
        password: this.fb.control('', [Validators.required]),
      });
    } else if (isOAuth2Credentials(currentCredentials)) {
      this.formGroup = this.fb.group({
        username: this.fb.control('', [Validators.required]),
        password: this.fb.control('', [Validators.required]),
      });
    } else {
      throw new Error('Unhandled credentials type.');
    }
  }

  private checkCredentialsFormValidity(): boolean {
    this.textFields.forEach((textField) => textField.showClientSideError());
    return this.formGroup.valid;
  }

  handleRevoke(): void {
    this.revokeTokens.emit();
  }

  private makeEmptyCredentials(): Credentials {
    switch (this._authenticationProtocol) {
      case AuthenticationProtocol.BASIC_AUTH:
        return makeEmptyBasicAuthCredentials();
      case AuthenticationProtocol.TOKEN_AUTH:
        return makeEmptyTokenAuthCredentials();
      case AuthenticationProtocol.OAUTH_2:
        return makeEmptyOAuth2Credentials();
      default:
        return makeEmptyBasicAuthCredentials();
    }
  }

  handleFieldFocused(event: any) {
    this.focusedFields.push(event.target);
  }

  handleFieldBlurred(event: any) {
    this.focusedFields = this.focusedFields.filter((elem) => elem !== event.target);
  }
}

function makeEmptyBasicAuthCredentials(): BasicAuthCredentials {
  return {
    type: AuthenticationProtocol.BASIC_AUTH,
    implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
    username: '',
    password: '',
    registered: false,
  };
}

function makeEmptyTokenAuthCredentials(): TokenAuthCredentials {
  return {
    type: AuthenticationProtocol.TOKEN_AUTH,
    implementedProtocol: AuthenticationProtocol.TOKEN_AUTH,
    token: '',
    registered: false,
  };
}

function makeEmptyOAuth2Credentials(): OAuth2Credentials {
  return {
    type: AuthenticationProtocol.OAUTH_2,
    implementedProtocol: AuthenticationProtocol.OAUTH_2,
    accessToken: '',
    refreshToken: '',
    registered: false,
  };
}
