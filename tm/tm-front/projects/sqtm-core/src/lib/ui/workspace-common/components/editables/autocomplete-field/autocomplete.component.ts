import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  QueryList,
  TemplateRef,
  ViewChild,
  ViewChildren,
  ViewContainerRef,
} from '@angular/core';

import { fromEvent, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { FieldValidationError } from '../../../../../model/error/error.model';
import { AutocompleteOptionComponent } from './autocomplete-option/autocomplete-option.component';
import { ConnectedPosition, Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { TemplatePortal } from '@angular/cdk/portal';

@Component({
  selector: 'sqtm-core-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AutocompleteComponent implements OnInit, AfterViewInit, OnDestroy {
  overlayRef: OverlayRef;

  @ViewChild('templatePortalContent', { read: TemplateRef })
  templatePortalContent: TemplateRef<any>;

  @Input() serverSideFieldValidationError: FieldValidationError[];

  _options: string[];

  get options() {
    return this._options;
  }

  @Input()
  set options(paramOptions: string[]) {
    this._options = paramOptions;
    if (paramOptions.length > 0) {
      this.openOptionMenu();
    } else {
      this.isMenuOpen = false;
      this.deactivateOptions();
    }
  }

  private _isMenuOpen: boolean;

  get isMenuOpen(): boolean {
    return this._isMenuOpen;
  }

  set isMenuOpen(value: boolean) {
    this._isMenuOpen = value;
  }

  private _value: string;

  get value(): string {
    return this._value;
  }

  @Input()
  set value(value: string) {
    this._value = value;
  }

  @Output() valueChanged = new EventEmitter<any>();

  @Output() confirmValueEvent = new EventEmitter<any>();

  @Output() cancelEvent = new EventEmitter<any>();

  private unsub$ = new Subject<void>();

  @ViewChild('textInput')
  textInput: ElementRef;

  @ViewChildren(AutocompleteOptionComponent)
  optionElements: QueryList<AutocompleteOptionComponent>;

  constructor(
    private cdr: ChangeDetectorRef,
    public overlay: Overlay,
    public vcr: ViewContainerRef,
  ) {}

  ngOnInit(): void {}

  ngAfterViewInit() {
    fromEvent(this.textInput.nativeElement, 'input')
      .pipe(
        takeUntil(this.unsub$),
        debounceTime(500),
        map((event: any) => event.target.value),
        distinctUntilChanged(),
      )
      .subscribe((value) => this.valueChanged.emit(value));
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  confirmValue() {
    this.confirmValueEvent.emit(this.value);
    // prevent triggering 'input' event with former value when typing and adding were done quickly
  }

  handleKeyupEnter() {
    if (this.isMenuOpen && this.isOneOptionActive()) {
      this.selectOptionWithEnter();
    } else {
      this.confirmValue();
    }
  }

  selectOptionWithMouse(event, option: string) {
    // this.formGroup.get(this.fieldName).setValue(option);
    this.value = option;
    this.textInput.nativeElement.value = option;
    this.close();
    this.closeOptionMenu();
    this.focusInputField();
    event.preventDefault();
  }

  closeOptionMenu() {
    if (this.isMenuOpen) {
      this.isMenuOpen = false;
      this.deactivateOptions();
    } else {
      this.cancelEvent.emit();
    }
  }

  openOptionMenu() {
    if (!this.isMenuOpen && this.options.length > 0) {
      this.isMenuOpen = true;
      this.showList(this.textInput, this.templatePortalContent);
    }
  }

  activateOption(paramIndex: number) {
    if (paramIndex === undefined) {
      return;
    }
    this.deactivateOptions();
    const activeOption = this.optionElements.find((element, index) => index === paramIndex);
    if (activeOption) {
      activeOption.isActive = true;
    }
  }

  private focusInputField() {
    const textInputNativeElement = this.textInput.nativeElement as HTMLInputElement;
    textInputNativeElement.focus();
  }

  private deactivateOptions() {
    if (this.optionElements) {
      const activeOption = this.optionElements.find((option) => option.isActive);
      if (activeOption) {
        activeOption.isActive = false;
      }
    }
  }

  private isOneOptionActive(): boolean {
    return this.optionElements.filter((option) => option.isActive).length > 0;
  }

  /* Keyboard navigation in menu */

  selectOptionWithEnter() {
    const option = this.getActiveOptionValue();
    // this.formGroup.get(this.fieldName).setValue(option);
    this.value = option;
    this.textInput.nativeElement.value = option;
    this.close();
    this.closeOptionMenu();
    this.focusInputField();
  }

  activateOptionBelow(event) {
    event.preventDefault();
    this.openOptionMenu();
    this.activateOption(this.getBelowOptionIndex(this.getActiveOptionIndex()));
  }

  activateOptionAbove(event) {
    event.preventDefault();
    this.openOptionMenu();
    this.activateOption(this.getAboveOptionIndex(this.getActiveOptionIndex()));
  }

  private getActiveOptionValue(): string {
    return this.optionElements.find((option) => option.isActive).value;
  }

  private getActiveOptionIndex(): number {
    return this.optionElements.toArray().findIndex((option) => option.isActive);
  }

  private getBelowOptionIndex(index: number) {
    const optionLength = this.optionElements.length;
    if (optionLength === 0) {
      return undefined;
    } else if (index === -1 || index === undefined) {
      return 0;
    } else if (index === optionLength - 1) {
      return 0;
    } else {
      return index + 1;
    }
  }

  private getAboveOptionIndex(index: number) {
    const optionLength = this.optionElements.length;
    if (optionLength === 0) {
      return undefined;
    } else if (index === -1 || index === undefined) {
      return optionLength - 1;
    } else if (index === 0) {
      return optionLength - 1;
    } else {
      return index - 1;
    }
  }

  showList(elementRef: ElementRef, templateRef: TemplateRef<any>, positions?: ConnectedPosition[]) {
    // Default positions
    positions = positions || [
      { originX: 'center', overlayX: 'start', originY: 'bottom', overlayY: 'top', offsetY: 10 },
      { originX: 'center', overlayX: 'start', originY: 'top', overlayY: 'bottom', offsetY: -10 },
    ];

    const positionStrategy = this.overlay
      .position()
      .flexibleConnectedTo(elementRef)
      .withPositions(positions);
    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
      width: 200,
    };
    this.overlayRef = this.overlay.create(overlayConfig);
    const templatePortal = new TemplatePortal(templateRef, this.vcr);
    this.overlayRef.attach(templatePortal);
    this.overlayRef.backdropClick().subscribe(() => this.close());
  }

  close() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
    }
  }
}
