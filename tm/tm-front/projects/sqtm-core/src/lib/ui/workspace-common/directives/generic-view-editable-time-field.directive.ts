import { Directive, Host, Input } from '@angular/core';
import { GenericEntityViewService } from '../../../core/services/genric-entity-view/generic-entity-view.service';
import { SqtmGenericEntityState } from '../../../core/services/genric-entity-view/generic-entity-view-state';
import { EditableDateFieldComponent } from '../components/editables/editable-date-field/editable-date-field.component';
import { AbstractGenericViewEditableField } from './abstract-generic-view-editable-field';
import { EditableTimeFieldComponent } from '../components/editables/editable-time-field/editable-time-field.component';

@Directive({
  selector: '[sqtmCoreGenericViewEditableTimeField]',
})
export class GenericViewEditableTimeFieldDirective<
  E extends SqtmGenericEntityState,
  T extends string,
> extends AbstractGenericViewEditableField<EditableDateFieldComponent, E, T> {
  @Input('sqtmCoreGenericViewEditableTimeField')
  fieldName: keyof E;

  constructor(
    protected genericEntityViewService: GenericEntityViewService<E, T>,
    @Host() protected editable: EditableTimeFieldComponent,
  ) {
    super(genericEntityViewService, editable);
  }

  protected override showExternalErrorMessages(_errorMessages: string[]): void {}

  protected setValue(newValue: any): void {
    if (!newValue) {
      this.editable.value = null;
    } else if (typeof newValue === 'number') {
      this.editable.value = newValue;
    } else {
      console.warn(
        'Received unhandled time value. Expect number but received ' + typeof newValue,
        newValue,
      );
      this.editable.value = null;
    }
  }
}
