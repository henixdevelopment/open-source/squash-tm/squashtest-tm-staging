import { Directive, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[sqtmCoreKeyupStopPropagation]',
})
export class KeyupStopPropagationDirective {
  @Input('propagatesKeyUp')
  propagatesKeyUp = false;

  constructor() {}

  @HostListener('keyup', ['$event'])
  public onEvent($event: MouseEvent): void {
    if (!this.propagatesKeyUp) {
      $event.stopPropagation();
    }
  }
}
