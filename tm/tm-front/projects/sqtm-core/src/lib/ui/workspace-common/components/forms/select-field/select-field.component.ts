import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FieldValidationError } from '../../../../../model/error/error.model';
import { AbstractFormField } from '../abstract-form-field';
import { NzSelectModeType } from 'ng-zorro-antd/select';
import { DisplayOption } from '../../../../../model/display-option';

@Component({
  selector: 'sqtm-core-select-field',
  template: ` <div
    class="full-width"
    [formGroup]="formGroup"
    [attr.data-test-field-name]="fieldName"
    sqtmCoreKeyupStopPropagation
  >
    <nz-select
      class="full-width"
      [nzShowArrow]="mode !== 'tags'"
      [nzShowSearch]="true"
      [nzDisabled]="disabled"
      [formControlName]="fieldName"
      [nzMode]="mode"
      [nzPlaceHolder]="placeHolder"
    >
      @for (option of options; track option) {
        <nz-option [nzValue]="option.id" [nzLabel]="option.label | translate"> </nz-option>
      }
    </nz-select>
    @for (error of errors; track error) {
      <div class="has-error">
        <span [attr.data-test-error-key]="error.provideI18nKey()" class="sqtm-core-error-message">
          {{ error.provideI18nKey() | translate }}
        </span>
      </div>
    }
  </div>`,
  styleUrls: ['./select-field.component.less'],
  providers: [
    {
      provide: AbstractFormField,
      useExisting: SelectFieldComponent,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectFieldComponent extends AbstractFormField {
  @Input()
  formGroup: FormGroup;

  @Input()
  fieldName: string;

  @Input()
  options: DisplayOption[] = [];

  @Input()
  placeHolder: string;

  @Input()
  showSearch: boolean;

  @Input()
  set serverSideFieldValidationError(fieldsValidationErrors: FieldValidationError[]) {
    this.showServerSideError(fieldsValidationErrors);
  }

  @Input()
  set disabled(isDisabled: boolean) {
    this._disabled = isDisabled;
  }

  get disabled(): boolean {
    return this._disabled;
  }

  @Input()
  set mode(mode: NzSelectModeType) {
    this._mode = mode;
  }

  get mode(): NzSelectModeType {
    return this._mode;
  }

  private _mode: NzSelectModeType = 'default';

  private _disabled: boolean;

  constructor(cdr: ChangeDetectorRef) {
    super(cdr);
  }
}
