import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'sqtm-core-color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ColorPickerComponent {
  get color() {
    return this._color;
  }

  @Input()
  set color(newColor: string) {
    this._color = newColor;
  }

  @Input()
  cpPosition:
    | 'auto'
    | 'top'
    | 'bottom'
    | 'left'
    | 'right'
    | 'top-left'
    | 'top-right'
    | 'bottom-left'
    | 'bottom-right' = 'auto';

  private _color: string;

  @Output()
  colorSelectionChanged = new EventEmitter<string>();

  constructor() {}

  changeColor(newColor: string) {
    this.colorSelectionChanged.next(newColor);
  }
}
