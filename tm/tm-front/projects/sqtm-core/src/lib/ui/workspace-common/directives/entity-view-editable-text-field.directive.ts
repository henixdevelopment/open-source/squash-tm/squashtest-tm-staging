import { Directive, Host, HostListener, Input, OnDestroy, OnInit } from '@angular/core';
import {
  createFieldSelector,
  EditableFieldState,
  SqtmEntityState,
} from '../../../core/services/entity-view/entity-view.state';
import { EntityViewService } from '../../../core/services/entity-view/entity-view.service';
import { distinctUntilChanged, filter, map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { EditableTextFieldComponent } from '../components/editables/editable-text-field/editable-text-field.component';
import { FormControl } from '@angular/forms';
import { select } from '@ngrx/store';
import { SimplePermissions } from '../../../model/permissions/simple-permissions';

@Directive({
  selector: '[sqtmCoreEntityViewEditableTextField]',
})
export class EntityViewEditableTextFieldDirective<
    E extends SqtmEntityState,
    T extends string,
    P extends SimplePermissions,
  >
  implements OnInit, OnDestroy
{
  @Input('sqtmCoreEntityViewEditableTextField')
  fieldName: keyof E;

  @Input()
  customFieldId: number;

  formControl = new FormControl();

  unsub$ = new Subject<void>();

  constructor(
    private entityViewService: EntityViewService<E, T, P>,
    @Host() private editable: EditableTextFieldComponent,
  ) {}

  @HostListener('confirmEvent', ['$event'])
  onConfirm(value: string) {
    this.entityViewService.update(this.fieldName, value as any); // cannot use type system on dynamic inputs...
  }

  ngOnInit(): void {
    this.entityViewService.componentData$
      .pipe(
        takeUntil(this.unsub$),
        map((componentData) => componentData[componentData.type]),
        // If value has changed in store, the update is a success.
        // If there is any error server side, the error stream will emit, and the value will not be changed.
        distinctUntilChanged(),
      )
      .subscribe((entity) => {
        this.editable.value = entity[this.fieldName as string];
      });

    this.entityViewService.componentData$
      .pipe(
        takeUntil(this.unsub$),
        select(createFieldSelector(this.fieldName as string)),
        filter(
          (fieldState: EditableFieldState<E>) =>
            Boolean(fieldState) && fieldState.status === 'SERVER_ERROR',
        ),
      )
      .subscribe((fieldState: EditableFieldState<E>) =>
        this.editable.showExternalErrorMessage(fieldState.errorMsg),
      );

    this.entityViewService.componentData$
      .pipe(takeUntil(this.unsub$))
      .subscribe((componentData) => {
        this.editable.editable =
          componentData.permissions.canWrite &&
          componentData.milestonesAllowModification &&
          this.editable.editable;
        this.editable.cdRef.detectChanges();
      });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
