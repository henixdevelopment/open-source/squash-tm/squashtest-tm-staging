import { Directive, Host, Input } from '@angular/core';
import { GenericEntityViewService } from '../../../core/services/genric-entity-view/generic-entity-view.service';
import { EditableTextFieldComponent } from '../components/editables/editable-text-field/editable-text-field.component';
import { AbstractGenericViewEditableField } from './abstract-generic-view-editable-field';
import { SqtmGenericEntityState } from '../../../core/services/genric-entity-view/generic-entity-view-state';

@Directive({
  selector: '[sqtmCoreGenericViewEditableTextField]',
})
export class GenericViewEditableTextFieldDirective<
  E extends SqtmGenericEntityState,
  T extends string,
> extends AbstractGenericViewEditableField<EditableTextFieldComponent, E, T> {
  @Input('sqtmCoreGenericViewEditableTextField')
  fieldName: keyof E;

  constructor(
    protected genericEntityViewService: GenericEntityViewService<E, T>,
    @Host() protected editable: EditableTextFieldComponent,
  ) {
    super(genericEntityViewService, editable);
  }

  protected showExternalErrorMessages(errorMessages: string[]): void {
    this.editable.showExternalErrorMessage(errorMessages);
  }

  protected setValue(newValue: any): void {
    this.editable.value = newValue;
  }
}
