import { AfterViewInit, Directive, Host, HostListener, Input, OnDestroy } from '@angular/core';
import { distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { EditableSelectLevelEnumFieldComponent } from '../components/editables/editable-select-level-enum-field/editable-select-level-enum-field.component';
import { LevelEnumItem } from '../../../model/level-enums/level-enum';
import { GenericEntityViewService } from '../../../core/services/genric-entity-view/generic-entity-view.service';
import { SqtmEntityState } from '../../../core/services/entity-view/entity-view.state';

/*
 * This class is almost a straight copy pasta from entity-view-editable-select-level-enum-field...
 * The only differences are related to permissions management.
 * AbstractGenericViewEditableField couldn't be used as a base class because level enum field component don't
 * inherit from AbstractEditableField, which is a shame since it is just a wrapper around an editable select field...
 */
@Directive({
  selector: '[sqtmCoreGenericViewEditableSelectLevelEnum]',
})
export class GenericViewEditableSelectLevelEnumFieldDirective<
    E extends SqtmEntityState,
    T extends string,
  >
  implements AfterViewInit, OnDestroy
{
  @Input('sqtmCoreGenericViewEditableSelectLevelEnum')
  fieldName: keyof E;

  unsub$ = new Subject<void>();

  constructor(
    private entityViewService: GenericEntityViewService<E, T>,
    @Host() private editable: EditableSelectLevelEnumFieldComponent,
  ) {}

  @HostListener('confirmEvent', ['$event'])
  onConfirm(levelEnumItem: LevelEnumItem<any>) {
    this.editable.beginAsync();
    this.entityViewService.update(this.fieldName, levelEnumItem.id as any);
  }

  ngAfterViewInit(): void {
    this.entityViewService.componentData$
      .pipe(
        takeUntil(this.unsub$),
        map((componentData) => componentData[componentData.type]),
        distinctUntilChanged(),
      )
      .subscribe((entity) => {
        this.editable.selectedEnumItem = entity[this.fieldName as string];
        this.editable.markForCheck();
      });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
