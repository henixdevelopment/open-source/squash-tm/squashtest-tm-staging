import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
} from '@angular/core';

@Component({
  selector: 'sqtm-core-action-autocomplete-field-option',
  templateUrl: './action-autocomplete-field-option.component.html',
  styleUrls: ['./action-autocomplete-field-option.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActionAutocompleteFieldOptionComponent implements OnInit {
  @Input()
  value: string;

  private _isActive = false;

  get isActive(): boolean {
    return this._isActive;
  }

  set isActive(value: boolean) {
    this._isActive = value;
    this.cdr.detectChanges();
  }

  constructor(private cdr: ChangeDetectorRef) {}

  ngOnInit(): void {}
}
