import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import {
  LicenseInformationMessageProvider,
  LicenseMessagePlacement,
} from './license-information.helpers';
import { TranslateService } from '@ngx-translate/core';
import { DialogService } from '../../../dialog/services/dialog.service';
import { LicenseInformationState } from '../../../../core/referential/state/license-information.state';

@Component({
  selector: 'sqtm-core-license-information-banner',
  templateUrl: './license-information-banner.component.html',
  styleUrls: ['./license-information-banner.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LicenseInformationBannerComponent {
  public messageHelper: LicenseInformationMessageProvider;

  @Input() set licenseInformation(info: LicenseInformationState) {
    this._licenseInformation = info;
    this.messageHelper = new LicenseInformationMessageProvider(info, this.translateService);
  }

  @Input() set placement(newPlacement: LicenseMessagePlacement | string) {
    if (!Object.values(LicenseMessagePlacement).includes(newPlacement as LicenseMessagePlacement)) {
      throw new Error('Received invalid license message placement value : ' + newPlacement);
    }

    this._placement = newPlacement as LicenseMessagePlacement;
  }

  @Input() set isAdmin(admin: boolean) {
    this._isAdmin = admin;
  }

  private _licenseInformation: LicenseInformationState;
  private _placement: LicenseMessagePlacement = LicenseMessagePlacement.WORKSPACES;
  private _isAdmin: boolean;

  constructor(
    public readonly translateService: TranslateService,
    public readonly dialogService: DialogService,
  ) {}

  get isVisible(): boolean {
    return Boolean(this._licenseInformation) && Boolean(this.shortMessage);
  }

  get shortMessage(): string {
    return this.messageHelper?.getShortMessage(this._placement, this._isAdmin);
  }

  openDetailDialog(): void {
    this.dialogService.openAlert(
      {
        messageKey: this.messageHelper.getLongMessage(this._placement, this._isAdmin),
        titleKey: 'sqtm-core.license.title',
        id: 'license-information-detail',
        level: null,
      },
      650,
    );
  }
}
