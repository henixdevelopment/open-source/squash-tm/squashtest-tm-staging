import { FieldValidationError } from '../../../model/error/error.model';

export abstract class ErrorI18nKeyProvider {
  abstract provideI18nKey(): string;
}

export class ClientSideErrorI18nProvider extends ErrorI18nKeyProvider {
  constructor(protected errorKey: string) {
    super();
  }

  provideI18nKey(): string {
    return `sqtm-core.validation.errors.${this.errorKey}`;
  }
}

export class ServerSideErrorI18nProvider extends ErrorI18nKeyProvider {
  constructor(protected squashError: FieldValidationError) {
    super();
  }

  provideI18nKey(): string {
    return this.squashError.i18nKey;
  }
}

export function buildErrorI18nKeyProvider(field: string | FieldValidationError, errorValue?: any) {
  if (typeof field === 'string') {
    if (errorValue) {
      return new ClientSideErrorI18nProvider(field);
    } else {
      return new ClientSideErrorI18nProvider(field);
    }
  } else {
    return new ServerSideErrorI18nProvider(field);
  }
}
