import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OptionalSelectFieldComponent } from './optional-select-field.component';
import { TranslateModule } from '@ngx-translate/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('OptionalSelectFieldComponent', () => {
  let component: OptionalSelectFieldComponent;
  let fixture: ComponentFixture<OptionalSelectFieldComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [OptionalSelectFieldComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionalSelectFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
