import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TextResearchFieldComponent } from './text-research-field.component';

describe('TextResearchFieldComponent', () => {
  let component: TextResearchFieldComponent;
  let fixture: ComponentFixture<TextResearchFieldComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TextResearchFieldComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextResearchFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
