import { EditableField } from '../components/editables/abstract-editable-field';
import {
  EditableGenericEntityFieldState,
  SqtmGenericEntityState,
} from '../../../core/services/genric-entity-view/generic-entity-view-state';
import { distinctUntilChanged, filter, map, takeUntil } from 'rxjs/operators';
import { select } from '@ngrx/store';
import { Subject } from 'rxjs';
import { Directive, Host, HostListener, OnDestroy, OnInit } from '@angular/core';
import { createFieldSelector } from '../../../core/services/entity-view/entity-view.state';
import { GenericEntityViewService } from '../../../core/services/genric-entity-view/generic-entity-view.service';

@Directive()
export abstract class AbstractGenericViewEditableField<
    _Editable extends EditableField,
    E extends SqtmGenericEntityState,
    T extends string,
  >
  implements OnInit, OnDestroy
{
  fieldName: keyof E;

  private unsub$ = new Subject<void>();

  protected constructor(
    protected genericViewService: GenericEntityViewService<E, T>,
    @Host() protected editable: EditableField,
  ) {}

  protected initializeEditableDirective() {
    this.observeValueChanges();
    this.observeErrors();
  }

  private observeValueChanges() {
    this.genericViewService.componentData$
      .pipe(
        takeUntil(this.unsub$),
        map((componentData) => componentData[componentData.type]),
        distinctUntilChanged((previous, current) => this.fieldChanged(previous, current)),
      )
      .subscribe((entity) => {
        this.setValue(entity[this.fieldName as string]);
      });
  }

  private observeErrors() {
    this.genericViewService.componentData$
      .pipe(
        takeUntil(this.unsub$),
        select(createFieldSelector(this.fieldName as string)),
        filter(
          (fieldState: EditableGenericEntityFieldState<E>) =>
            Boolean(fieldState) && fieldState.status === 'SERVER_ERROR',
        ),
      )
      .subscribe((fieldState: EditableGenericEntityFieldState<E>) =>
        this.showExternalErrorMessages(fieldState.errorMsg),
      );
  }

  ngOnInit(): void {
    this.initializeEditableDirective();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  @HostListener('confirmEvent', ['$event'])
  onConfirm(value: any) {
    this.editable.beginAsync();
    this.genericViewService.update(this.fieldName as any, value);
  }

  protected abstract showExternalErrorMessages(errorMessages: string[]): void;

  protected abstract setValue(newValue: any): void;

  private fieldChanged(previous: any, current: any) {
    return previous[this.fieldName as string] === current[this.fieldName as string];
  }
}
