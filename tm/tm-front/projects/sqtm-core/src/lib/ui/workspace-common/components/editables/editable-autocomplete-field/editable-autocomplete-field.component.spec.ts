import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EditableAutocompleteFieldComponent } from './editable-autocomplete-field.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TestingUtilsModule } from '../../../../testing-utils/testing-utils.module';
import { TranslateModule } from '@ngx-translate/core';

describe('EditableAutocompleteFieldComponent', () => {
  let component: EditableAutocompleteFieldComponent;
  let fixture: ComponentFixture<EditableAutocompleteFieldComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, TranslateModule.forRoot()],
      declarations: [EditableAutocompleteFieldComponent],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(EditableAutocompleteFieldComponent);
        component = fixture.componentInstance;
        component.options = ['Label', 'Label2', 'Label3'];
        component.value = 'Value3';
        component.editable = true;
        fixture.detectChanges();
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('It should show initial data', () => {
  //   const element = fixture.nativeElement;
  //   const span = element.querySelector('span');
  //   expect(span.textContent).toContain('Label3');
  // });
  //
  // it('should pass on edit mode when clicking on it if edition is allowed', () => {
  //   activateEditMode();
  //   const select = fixture.debugElement.query(By.css('nz-select'));
  //   expect(select).toBeTruthy();
  //   expect(select.properties['ngModel']).toBe('Value3');
  //   expect(select.nativeElement.children.length).toBe(3);
  // });
  //
  // it('should not pass in edit mode when clicking on it if edition is forbidden', () => {
  //   component.editable = false;
  //   fixture.detectChanges();
  //   activateEditMode();
  //   const select = fixture.debugElement.query(By.css('nz-select'));
  //   expect(select).toBeFalsy();
  // });
  //
  // it('should emit new value in confirm output', () => {
  //   let outputValue: string;
  //   component.confirmEvent.asObservable().subscribe(
  //     value => outputValue = value
  //   );
  //   activateEditMode();
  //   component.transientValue = 'Value';
  //   component.confirm();
  //   expect(outputValue).toEqual('Value');
  //   expect(fixture.debugElement.queryAll(By.css('button')).length).toBe(2);
  // });
  //
  // function activateEditMode() {
  //   const span = fixture.debugElement.query(By.css('span'));
  //   span.triggerEventHandler('click', null);
  //   fixture.detectChanges();
  // }
});
