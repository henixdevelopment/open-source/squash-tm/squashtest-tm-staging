import {
  ChangeDetectionStrategy,
  Component,
  computed,
  ElementRef,
  Input,
  Signal,
  signal,
  ViewChild,
  WritableSignal,
} from '@angular/core';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzWaveModule } from 'ng-zorro-antd/core/wave';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

@Component({
  selector: 'sqtm-core-secret-field',
  templateUrl: './secret-field.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [
    NzIconModule,
    NzButtonModule,
    NzWaveModule,
    TranslateModule,
    FormsModule,
    NzInputModule,
    NzToolTipModule,
  ],
})
export class SecretFieldComponent {
  @Input()
  value: string;

  @ViewChild('input')
  input: ElementRef<HTMLInputElement>;

  public readonly obfuscatedToken = '••••••';

  showValue$: WritableSignal<boolean> = signal(false);
  displayedText$: WritableSignal<string> = signal(this.obfuscatedToken);
  isCopied$: WritableSignal<boolean> = signal(false);
  copyTooltipDelay$: WritableSignal<number> = signal(0);

  showHideTooltipTitle$: Signal<string> = computed(() => {
    return this.showValue$()
      ? 'sqtm-core.user-account-page.api-token.dialog.display-token.tooltip.hide'
      : 'sqtm-core.user-account-page.api-token.dialog.display-token.tooltip.show';
  });

  copyTooltipTitle$: Signal<string> = computed(() => {
    return this.isCopied$()
      ? 'sqtm-core.generic.label.copy-done'
      : 'sqtm-core.generic.label.copy-one';
  });

  constructor() {
    this.displayedText$.set(this.obfuscatedToken);
  }

  getText() {
    return this.displayedText$();
  }

  updateVisibility() {
    this.showValue$.set(!this.showValue$());
    if (this.showValue$()) {
      this.displayedText$.set(this.value);
    } else {
      this.displayedText$.set(this.obfuscatedToken);
    }
  }

  copyValue() {
    this.input.nativeElement.select();
    navigator.clipboard.writeText(this.value).then(() => {
      this.isCopied$.set(true);
      this.copyTooltipDelay$.set(2);
      setTimeout(() => {
        this.copyTooltipDelay$.set(0);
        this.isCopied$.set(false);
      }, 2500);
    });
  }
}
