import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { IconPickerFieldComponent } from './icon-picker-field.component';
import { OverlayModule } from '@angular/cdk/overlay';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TestingUtilsModule } from '../../../../../testing-utils/testing-utils.module';

describe('IconPickerFieldComponent', () => {
  let component: IconPickerFieldComponent;
  let fixture: ComponentFixture<IconPickerFieldComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [OverlayModule, TestingUtilsModule],
      declarations: [IconPickerFieldComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconPickerFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
