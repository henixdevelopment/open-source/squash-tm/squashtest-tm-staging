import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EditableSelectInfolistFieldComponent } from './editable-select-infolist-field.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

describe('EditableSelectInfolistFieldComponent', () => {
  let component: EditableSelectInfolistFieldComponent;
  let fixture: ComponentFixture<EditableSelectInfolistFieldComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [EditableSelectInfolistFieldComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableSelectInfolistFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
