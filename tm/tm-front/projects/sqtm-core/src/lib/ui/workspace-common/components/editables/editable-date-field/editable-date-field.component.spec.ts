import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EditableDateFieldComponent } from './editable-date-field.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { TestingUtilsModule } from '../../../../testing-utils/testing-utils.module';

describe('EditableDateFieldComponent', () => {
  let component: EditableDateFieldComponent;
  let fixture: ComponentFixture<EditableDateFieldComponent>;
  const translateService = jasmine.createSpyObj('TranslateService', ['instant']);
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [EditableDateFieldComponent],
      imports: [TestingUtilsModule],
      providers: [
        {
          provide: TranslateService,
          useValue: translateService,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableDateFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
