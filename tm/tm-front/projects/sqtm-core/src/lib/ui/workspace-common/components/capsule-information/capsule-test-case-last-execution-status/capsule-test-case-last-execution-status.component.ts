import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { AbstractCapsuleInformationDirective } from '../abstract-capsule-information';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sqtm-core-capsule-test-case-last-execution-status',
  templateUrl: './capsule-test-case-last-execution-status.component.html',
  styleUrls: ['./capsule-test-case-last-execution-status.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CapsuleTestCaseLastExecutionStatusComponent
  extends AbstractCapsuleInformationDirective
  implements OnInit
{
  constructor(translateService: TranslateService) {
    super(translateService);
  }

  ngOnInit(): void {}

  getExecutionStatusBorder() {
    const executionStatus = this.informationData.id;
    if (executionStatus != null) {
      return this.getStyle(this.informationData.color);
    } else {
      return { border: `1px solid var(--container-border-color)` };
    }
  }
}
