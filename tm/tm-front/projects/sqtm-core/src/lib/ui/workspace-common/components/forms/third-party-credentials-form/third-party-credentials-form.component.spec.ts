import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ThirdPartyCredentialsFormComponent } from './third-party-credentials-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { AuthenticationProtocol } from '../../../../../model/third-party-server/authentication.model';
import { Credentials } from '../../../../../model/third-party-server/credentials.model';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('ThirdPartyCredentialsFormComponent', () => {
  let component: ThirdPartyCredentialsFormComponent;
  let fixture: ComponentFixture<ThirdPartyCredentialsFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, TranslateModule.forRoot()],
      declarations: [ThirdPartyCredentialsFormComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThirdPartyCredentialsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create with basic auth credentials', waitForAsync(() => {
    component.authenticationProtocol = AuthenticationProtocol.BASIC_AUTH;
    component.credentials = {
      implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
      type: AuthenticationProtocol.BASIC_AUTH,
      username: 'username',
      password: 'password',
    };

    fixture.detectChanges();

    expect(component).toBeTruthy();
    expect(component.formGroup.controls['username'].value).toBe('username');
    expect(component.formGroup.controls['password'].value).toBe('');
  }));

  it('should throw if given unhandled credentials type', waitForAsync(() => {
    const badCredentials = {
      implementedProtocol: 'WTF',
      type: 'WTF',
      titi: 'toto',
    } as unknown as Credentials;
    const badProtocol = 'WTF' as unknown as AuthenticationProtocol;
    expect(() => {
      component.authenticationProtocol = badProtocol;
      component.credentials = badCredentials;
    }).toThrow();
  }));

  it('should not send form if there are required empty fields', waitForAsync(() => {
    component.authenticationProtocol = AuthenticationProtocol.BASIC_AUTH;
    component.credentials = {
      implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
      type: AuthenticationProtocol.BASIC_AUTH,
      username: '',
      password: '',
    };

    const spy = jasmine.createSpy('submitSubscription');
    component.submit.subscribe(spy);

    fixture.detectChanges();

    component.handleSubmit();

    expect(spy).not.toHaveBeenCalled();
  }));
});
