import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'sqtm-core-toggle-icon',
  templateUrl: './toggle-icon.component.html',
  styleUrls: ['./toggle-icon.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToggleIconComponent {
  @Input()
  active = false;

  @Input()
  asyncRunning = false;

  @Input()
  stopPropagation = false;

  @Output()
  clickedInActiveMode = new EventEmitter<MouseEvent>();

  handleClick($event: MouseEvent) {
    if (this.stopPropagation) {
      $event.stopPropagation();
    }
    if (this.active && !this.asyncRunning) {
      this.clickedInActiveMode.emit($event);
    }
  }
}
