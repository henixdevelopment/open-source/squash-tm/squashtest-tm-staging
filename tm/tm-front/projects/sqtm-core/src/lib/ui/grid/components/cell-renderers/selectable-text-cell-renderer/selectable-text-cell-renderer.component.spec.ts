import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SelectableTextCellRendererComponent } from './selectable-text-cell-renderer.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { GridDefinition } from '../../../model/grid-definition.model';
import { RestService } from '../../../../../core/services/rest.service';
import { GridService } from '../../../services/grid.service';
import { gridServiceFactory } from '../../../grid.service.provider';
import { grid } from '../../../model/grid-builders';
import { ReferentialDataService } from '../../../../../core/referential/services/referential-data.service';

describe('SelectableTextCellRendererComponent', () => {
  let component: SelectableTextCellRendererComponent;
  let fixture: ComponentFixture<SelectableTextCellRendererComponent>;
  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SelectableTextCellRendererComponent],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig,
        },
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService],
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectableTextCellRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
