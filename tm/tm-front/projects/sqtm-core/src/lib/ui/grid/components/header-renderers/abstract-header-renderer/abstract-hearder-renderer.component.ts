import { ChangeDetectorRef, Directive, Input } from '@angular/core';
import { HeaderRenderer } from '../../../model/cell-renderer';
import { ColumnWithFilter } from '../../../model/column-display.model';
import { GridDisplay } from '../../../model/grid-display.model';
import { GridService } from '../../../services/grid.service';
import { GridViewportName } from '../../../../../model/grids/grid-viewport-name';

@Directive()
export abstract class AbstractHeaderRendererComponent implements HeaderRenderer {
  protected _columnDisplay: ColumnWithFilter;

  public get columnDisplay(): ColumnWithFilter {
    return this._columnDisplay;
  }

  @Input()
  public set columnDisplay(value: ColumnWithFilter) {
    this._columnDisplay = value;
  }

  @Input()
  gridDisplay: GridDisplay;

  @Input()
  viewportName: GridViewportName;

  protected constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {}
}
