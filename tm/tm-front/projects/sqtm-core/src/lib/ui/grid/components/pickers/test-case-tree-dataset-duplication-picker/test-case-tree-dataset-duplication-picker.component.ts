import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewContainerRef,
} from '@angular/core';
import { GridDefinition } from '../../../model/grid-definition.model';
import {
  TEST_CASE_TREE_DATASET_DUPLICATION_PICKER_CONFIG,
  TEST_CASE_TREE_DATASET_DUPLICATION_PICKER_ID,
} from '../../../tree-pickers.constant';
import { column } from '../../../model/common-column-definition.builders';
import { Extendable } from '../../../model/column-definition.model';
import { treePicker } from '../../../model/grid-builders';
import { GridService } from '../../../services/grid.service';
import { gridServiceFactory } from '../../../grid.service.provider';
import { RestService } from '../../../../../core/services/rest.service';
import { ReferentialDataService } from '../../../../../core/referential/services/referential-data.service';
import { Identifier } from '../../../../../model/entity.model';
import { DialogService } from '../../../../dialog/services/dialog.service';
import { GridPersistenceService } from '../../../../../core/services/grid-persistence/grid-persistence.service';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { TreeWithStatePersistence } from '../../../../workspace-common/components/tree/tree-with-state-persistence';
import { TreeNodeCellRendererComponent } from '../../../../cell-renderer-common/tree-node-cell-renderer/tree-node-cell-renderer.component';
import { ProjectDataMap } from '../../../../../model/project/project-data.model';

import { throwError } from 'rxjs';
import { TestCasePermissions } from '../../../../../model/permissions/simple-permissions';
import { DataRow } from '../../../model/data-row.model';
import { SquashTmDataRowType } from '../../../../../model/grids/data-row.model';
import {
  extractDataRowType,
  SquashTmDataRow,
  TestCase,
  TestCaseFolder,
  TestCaseLibrary,
} from '../../../model/data-row.type';
import { GridColumnId } from '../../../../../shared/constants/grid/grid-column-id';

export function convertRestrictedTestCaseLiterals(
  literals: Partial<DataRow>[],
  projectsData: ProjectDataMap,
) {
  // DON'T INLINE THE VAR, IT WOULD MAKE BUILD CRASH WITH ERROR :
  // {"__symbolic":"error","message":"Lambda not supported","line":42,"character":22}
  const converted: SquashTmDataRow[] = literals.map((literal) =>
    convertRestrictedTestCaseLiteral(literal, projectsData),
  );
  return converted;
}

export function convertRestrictedTestCaseLiteral(
  literal: Partial<DataRow>,
  projectsData: ProjectDataMap,
): SquashTmDataRow {
  let dataRow: DataRow;
  const type = extractDataRowType(literal);

  switch (type) {
    case SquashTmDataRowType.TestCaseLibrary:
      dataRow = new TestCaseLibrary();
      break;
    case SquashTmDataRowType.TestCaseFolder:
      dataRow = new TestCaseFolder();
      break;
    case SquashTmDataRowType.TestCase:
      dataRow = new TestCase();
      break;
    default:
      throwError(() => 'Not handled type ' + type);
  }

  dataRow.projectId = literal.projectId;
  const project = projectsData[dataRow.projectId];
  dataRow.simplePermissions = new TestCasePermissions(project);

  Object.assign(dataRow, literal);

  if (type !== SquashTmDataRowType.TestCase) {
    dataRow.selectable = false;
  }
  return dataRow;
}

export function testCaseTreeDatasetDuplicationPickerConfigFactory(): GridDefinition {
  return treePicker(TEST_CASE_TREE_DATASET_DUPLICATION_PICKER_ID)
    .server()
    .withServerUrl(['test-case-tree/dataset-duplication'])
    .withColumns([
      column(GridColumnId.NAME)
        .changeWidthCalculationStrategy(new Extendable(300))
        .withRenderer(TreeNodeCellRendererComponent),
    ])
    .withRowConverter(convertRestrictedTestCaseLiterals)
    .build();
}

@Component({
  selector: 'sqtm-core-test-case-tree-dataset-duplication-picker',
  template: ` <sqtm-core-grid class="sqtm-core-prevent-selection-in-grid"></sqtm-core-grid> `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: TEST_CASE_TREE_DATASET_DUPLICATION_PICKER_CONFIG,
      useFactory: testCaseTreeDatasetDuplicationPickerConfigFactory,
    },
    {
      provide: GridService,
      useFactory: gridServiceFactory,
      deps: [RestService, TEST_CASE_TREE_DATASET_DUPLICATION_PICKER_CONFIG, ReferentialDataService],
    },
  ],
})
export class TestCaseTreeDatasetDuplicationPickerComponent
  extends TreeWithStatePersistence
  implements OnInit, OnDestroy
{
  @Input()
  testCaseId: Identifier;

  @Output()
  selectedRows = new EventEmitter<DataRow[]>();

  constructor(
    public tree: GridService,
    protected referentialDataService: ReferentialDataService,
    protected restService: RestService,
    protected dialogService: DialogService,
    protected vcr: ViewContainerRef,
    protected gridPersistenceService: GridPersistenceService,
    protected router: Router,
  ) {
    super(
      tree,
      referentialDataService,
      gridPersistenceService,
      restService,
      router,
      dialogService,
      vcr,
      null,
      'test-case-tree/dataset-duplication',
    );
  }

  ngOnInit() {
    this.tree.selectedRows$.pipe(takeUntil(this.unsub$)).subscribe((rows) => {
      this.selectedRows.next(rows);
    });
    this.initData();

    this.tree.setServerUrl(['test-case-tree/dataset-duplication', this.testCaseId.toString()]);
  }

  ngOnDestroy(): void {
    this.tree.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }
}
