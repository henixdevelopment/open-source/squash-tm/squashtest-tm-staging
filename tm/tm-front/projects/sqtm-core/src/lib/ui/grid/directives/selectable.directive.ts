import { Directive, ElementRef, Host, Input, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { GridService } from '../services/grid.service';
import { fromEvent, Subject } from 'rxjs';
import { DragAndDropService } from '../../drag-and-drop/drag-and-drop.service';
import { filter, takeUntil, withLatestFrom } from 'rxjs/operators';
import { Identifier } from '../../../model/entity.model';
import { GridState } from '../model/state/grid.state';

@Directive({
  selector: '[sqtmCoreSelectable]',
})
export class SelectableDirective implements OnInit, OnDestroy {
  private unsub$ = new Subject<void>();

  @Input('sqtmCoreSelectable')
  rowId: Identifier;

  @Input('sqtmCoreSelectableDisabled')
  set disabled(value: boolean) {
    this._disabled = value;
    this.updateStyles();
  }

  private _disabled = false;

  constructor(
    private grid: GridService,
    @Host() private host: ElementRef,
    private dndService: DragAndDropService,
    private renderer: Renderer2,
  ) {}

  ngOnInit(): void {
    this.initSelectRowsEventHandler();
    this.updateStyles();
  }

  private initSelectRowsEventHandler() {
    fromEvent(this.host.nativeElement, 'click')
      .pipe(
        takeUntil(this.unsub$),
        withLatestFrom(this.dndService.dragAndDrop$, this.grid.gridState$),
        filter(([_$event, dnd]) => !dnd),
        filter(() => !this._disabled),
      )
      .subscribe(([$event, _dnd, state]: [MouseEvent, boolean, GridState]) => {
        $event.preventDefault();
        if (
          ($event.ctrlKey && state.definitionState.enableMultiRowSelection) ||
          ($event.metaKey && state.definitionState.enableMultiRowSelection)
        ) {
          this.grid.toggleRowSelection(this.rowId);
        } else if (this.shouldUnselectOnSimpleClick(state)) {
          this.grid.toggleRowSelection(this.rowId);
        } else if ($event.shiftKey && state.definitionState.enableMultiRowSelection) {
          this.grid.addToSelection(this.rowId);
        } else {
          this.grid.selectSingleRow(this.rowId);
        }
      });
  }

  // [SQUASH-1858] When user has selected only the referenced row, and unselectRowOnSimpleClick is activated we unselect row.
  private shouldUnselectOnSimpleClick(state: GridState) {
    return (
      state.definitionState.unselectRowOnSimpleClick &&
      state.dataRowState.selectedRows.length === 1 &&
      state.dataRowState.selectedRows.includes(this.rowId)
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private updateStyles() {
    if (this._disabled) {
      this.renderer.removeAttribute(this.host.nativeElement, 'data-test-grid-selectable');
      this.renderer.removeStyle(this.host.nativeElement, 'cursor');
    } else {
      // adding a custom attr to ease automated tests, please don't remove or modify
      this.renderer.setAttribute(this.host.nativeElement, 'data-test-grid-selectable', 'true');
      this.renderer.setStyle(this.host.nativeElement, 'cursor', 'pointer');
    }
  }
}
