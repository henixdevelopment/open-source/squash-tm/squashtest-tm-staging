import {
  ActionWordPermissions,
  AutomationRequestPermissions,
  CampaignPermissions,
  CustomReportPermissions,
  ReadOnlyPermissions,
  RequirementPermissions,
  TestCasePermissions,
} from '../../../model/permissions/simple-permissions';
import { Identifier } from '../../../model/entity.model';
import { DataRow, GenericDataRow } from './data-row.model';
import { ProjectDataMap } from '../../../model/project/project-data.model';
import { SquashTmDataRowType } from '../../../model/grids/data-row.model';

// Test Case Workspace
export class TestCaseLibrary extends DataRow {
  readonly type = SquashTmDataRowType.TestCaseLibrary;
  allowMoves = false;
  allowedChildren = [SquashTmDataRowType.TestCaseFolder, SquashTmDataRowType.TestCase];
  selectable = true;
}

export class TestCaseFolder extends DataRow {
  readonly type = SquashTmDataRowType.TestCaseFolder;
  allowMoves = true;
  allowedChildren = [SquashTmDataRowType.TestCaseFolder, SquashTmDataRowType.TestCase];
  selectable = true;
}

export class TestCase extends DataRow {
  readonly type = SquashTmDataRowType.TestCase;
  allowMoves = true;
  allowedChildren = [];
  selectable = true;
}

// Requirement Workspace
export class RequirementLibrary extends DataRow {
  readonly type = SquashTmDataRowType.RequirementLibrary;
  allowMoves = false;
  allowedChildren = [
    SquashTmDataRowType.RequirementFolder,
    SquashTmDataRowType.Requirement,
    SquashTmDataRowType.HighLevelRequirement,
  ];
  selectable = true;
}

export class RequirementFolder extends DataRow {
  readonly type = SquashTmDataRowType.RequirementFolder;
  allowMoves = true;
  allowedChildren = [
    SquashTmDataRowType.RequirementFolder,
    SquashTmDataRowType.Requirement,
    SquashTmDataRowType.HighLevelRequirement,
  ];
  selectable = true;
}

export class Requirement extends DataRow {
  readonly type = SquashTmDataRowType.Requirement;
  allowMoves = true;
  allowedChildren = [SquashTmDataRowType.Requirement];
  selectable = true;
}

export class HighLevelRequirement extends DataRow {
  readonly type = SquashTmDataRowType.HighLevelRequirement;
  allowMoves = true;
  allowedChildren = [SquashTmDataRowType.Requirement];
  selectable = true;
}

// Campaign Workspace
export class CampaignLibrary extends DataRow {
  readonly type = SquashTmDataRowType.CampaignLibrary;
  allowMoves = false;
  allowedChildren = [
    SquashTmDataRowType.CampaignFolder,
    SquashTmDataRowType.Campaign,
    SquashTmDataRowType.Sprint,
    SquashTmDataRowType.SprintGroup,
  ];
  selectable = true;
}

export class CampaignFolder extends DataRow {
  readonly type = SquashTmDataRowType.CampaignFolder;
  allowMoves = true;
  allowedChildren = [
    SquashTmDataRowType.CampaignFolder,
    SquashTmDataRowType.Campaign,
    SquashTmDataRowType.Sprint,
    SquashTmDataRowType.SprintGroup,
  ];
  selectable = true;
}

export class Campaign extends DataRow {
  readonly type = SquashTmDataRowType.Campaign;
  allowMoves = true;
  allowedChildren = [SquashTmDataRowType.Iteration];
  selectable = true;
}

export class Sprint extends DataRow {
  readonly type = SquashTmDataRowType.Sprint;
  allowMoves = true;
  allowedChildren = [];
  selectable = true;
}

export class SprintGroup extends DataRow {
  readonly type = SquashTmDataRowType.SprintGroup;
  allowMoves = true;
  allowedChildren = [SquashTmDataRowType.CampaignFolder, SquashTmDataRowType.Sprint];
  selectable = true;
}

export class Iteration extends DataRow {
  readonly type = SquashTmDataRowType.Iteration;
  allowMoves = true;
  allowedChildren = [SquashTmDataRowType.TestSuite];
  selectable = true;
}

export class TestSuite extends DataRow {
  readonly type = SquashTmDataRowType.TestSuite;
  allowMoves = true;
  allowedChildren = [];
  selectable = true;
}

export class TaProject extends DataRow {
  readonly type = SquashTmDataRowType.TAProject;
  allowMoves = false;
  allowedChildren = [SquashTmDataRowType.TAFolder, SquashTmDataRowType.TATest];
  selectable = true;
}

export class Directory extends DataRow {
  readonly type = SquashTmDataRowType.Directory;
  allowMoves = false;
  allowedChildren = [SquashTmDataRowType.Directory, SquashTmDataRowType.File];
  selectable = false;
}

export class File extends DataRow {
  readonly type = SquashTmDataRowType.File;
  allowMoves = false;
  allowedChildren = [];
  selectable = true;
}

export class TaFolder extends DataRow {
  readonly type = SquashTmDataRowType.TAFolder;
  allowMoves = false;
  allowedChildren = [SquashTmDataRowType.TAFolder, SquashTmDataRowType.TATest];
  selectable = true;
}

export class TaTest extends DataRow {
  readonly type = SquashTmDataRowType.TATest;
  allowMoves = false;
  allowedChildren = [];
  selectable = true;
}

// Custom Report Workspace
export class CustomReportLibrary extends DataRow {
  readonly type = SquashTmDataRowType.CustomReportLibrary;
  allowMoves = false;
  allowedChildren = [
    SquashTmDataRowType.CustomReportFolder,
    SquashTmDataRowType.ChartDefinition,
    SquashTmDataRowType.CustomReportCustomExport,
    SquashTmDataRowType.CustomReportDashboard,
    SquashTmDataRowType.ReportDefinition,
  ];
  selectable = true;
}

export class CustomReportFolder extends DataRow {
  readonly type = SquashTmDataRowType.CustomReportFolder;
  allowMoves = true;
  allowedChildren = [
    SquashTmDataRowType.CustomReportFolder,
    SquashTmDataRowType.ChartDefinition,
    SquashTmDataRowType.CustomReportCustomExport,
    SquashTmDataRowType.CustomReportDashboard,
    SquashTmDataRowType.ReportDefinition,
  ];
  selectable = true;
}

export class ChartDefinition extends DataRow {
  readonly type = SquashTmDataRowType.ChartDefinition;
  allowMoves = true;
  allowedChildren = [];
  selectable = true;
}

export class CustomReportCustomExport extends DataRow {
  readonly type = SquashTmDataRowType.CustomReportCustomExport;
  allowMoves = true;
  allowedChildren = [];
  selectable = true;
}

export class CustomReportDashboard extends DataRow {
  readonly type = SquashTmDataRowType.CustomReportDashboard;
  allowMoves = true;
  allowedChildren = [];
  selectable = true;
}

export class ReportDefinition extends DataRow {
  readonly type = SquashTmDataRowType.ReportDefinition;
  allowMoves = true;
  allowedChildren = [];
  selectable = true;
}

// useful ? probably a custom row factory for the given grid should be enough ?
export class ExecutionRow extends DataRow {
  readonly type = SquashTmDataRowType.Execution;
  allowMoves = true;
  allowedChildren = [];
  selectable = true;
}

export class AutomatedSuiteRow extends DataRow {
  readonly type = SquashTmDataRowType.AutomatedSuite;
  allowMoves = true;
  allowedChildren = [];
  selectable = true;
}

export class ActionWordLibrary extends DataRow {
  readonly type = SquashTmDataRowType.ActionWordLibrary;
  allowMoves = false;
  allowedChildren = [SquashTmDataRowType.ActionWord];
  selectable = true;
}

export class ActionWord extends DataRow {
  readonly type = SquashTmDataRowType.ActionWord;
  allowMoves = true;
  allowedChildren = [];
  selectable = true;
}

export class AutomationRequestRow extends DataRow {
  readonly type = SquashTmDataRowType.AutomationRequest;
  allowMoves = false;
  allowedChildren = [];
  selectable = true;
}

export type SquashTmDataRow =
  | GenericDataRow
  | TestCaseLibrary
  | TestCaseFolder
  | TestCase
  | RequirementLibrary
  | RequirementFolder
  | Requirement
  | HighLevelRequirement
  | CampaignLibrary
  | CampaignFolder
  | Campaign
  | Iteration
  | TestSuite
  | IterationTestPlanItemRow
  | CampaignTestPlanItemRow
  | TaProject
  | TaFolder
  | TaTest
  | CustomReportLibrary
  | CustomReportFolder
  | CustomReportDashboard
  | CustomReportCustomExport
  | ChartDefinition
  | ReportDefinition
  | ActionWordLibrary
  | ActionWord
  | ExecutionRow
  | AutomationRequestRow
  | Directory
  | File
  | AutomatedSuiteRow;

export class TestPlanItemRow extends DataRow {
  readonly type = SquashTmDataRowType.TestPlanItem;
  allowMoves = true;
  allowedChildren = [];
  selectable = true;
}

export class IterationTestPlanItemRow extends DataRow {
  readonly type = SquashTmDataRowType.IterationTestPlanItem;
  allowMoves = true;
  allowedChildren = [];
  selectable = true;
}

export class CampaignTestPlanItemRow extends DataRow {
  readonly type = SquashTmDataRowType.CampaignTestPlanItem;
  allowMoves = true;
  allowedChildren = [];
  selectable = true;
}

/**
 * Little factory function used to create real Squash TM entities instances from literal objects.
 * Will use type attribute to get the class of row. If no type is provided, it will try to convert the id with the Entity Reference pattern.
 * Aka EntityClass-<ENTITY_ID> (TestCaseLibrary-1)
 * @param literal a partial of data row.
 * @param projectsData A mapping from project ids to project ProjectData used to determine permissions.
 */
export function convertSqtmLiteral(
  literal: Partial<DataRow>,
  projectsData: ProjectDataMap,
): SquashTmDataRow {
  let dataRow: DataRow;
  const type = extractDataRowType(literal);

  switch (type) {
    // TestCase WS
    case SquashTmDataRowType.TestCaseLibrary:
      dataRow = new TestCaseLibrary();
      break;
    case SquashTmDataRowType.TestCaseFolder:
      dataRow = new TestCaseFolder();
      break;
    case SquashTmDataRowType.TestCase:
      dataRow = new TestCase();
      break;
    // Requirement WS
    case SquashTmDataRowType.RequirementLibrary:
      dataRow = new RequirementLibrary();
      break;
    case SquashTmDataRowType.RequirementFolder:
      dataRow = new RequirementFolder();
      break;
    case SquashTmDataRowType.Requirement:
      dataRow = new Requirement();
      break;
    case SquashTmDataRowType.HighLevelRequirement:
      dataRow = new HighLevelRequirement();
      break;
    // Campaign WS
    case SquashTmDataRowType.CampaignLibrary:
      dataRow = new CampaignLibrary();
      break;
    case SquashTmDataRowType.CampaignFolder:
      dataRow = new CampaignFolder();
      break;
    case SquashTmDataRowType.Campaign:
      dataRow = new Campaign();
      break;
    case SquashTmDataRowType.Sprint:
      dataRow = new Sprint();
      break;
    case SquashTmDataRowType.SprintGroup:
      dataRow = new SprintGroup();
      break;
    case SquashTmDataRowType.Iteration:
      dataRow = new Iteration();
      break;
    case SquashTmDataRowType.TestSuite:
      dataRow = new TestSuite();
      break;
    // Non tree-node entities
    case SquashTmDataRowType.TestPlanItem:
      dataRow = new TestPlanItemRow();
      break;
    case SquashTmDataRowType.IterationTestPlanItem:
      dataRow = new IterationTestPlanItemRow();
      break;
    case SquashTmDataRowType.CampaignTestPlanItem:
      dataRow = new CampaignTestPlanItemRow();
      break;
    case SquashTmDataRowType.Execution:
      dataRow = new ExecutionRow();
      break;
    case SquashTmDataRowType.TAProject:
      dataRow = new TaProject();
      break;
    case SquashTmDataRowType.TAFolder:
      dataRow = new TaFolder();
      break;
    case SquashTmDataRowType.TATest:
      dataRow = new TaTest();
      break;
    case SquashTmDataRowType.CustomReportLibrary:
      dataRow = new CustomReportLibrary();
      break;
    case SquashTmDataRowType.CustomReportFolder:
      dataRow = new CustomReportFolder();
      break;
    case SquashTmDataRowType.ChartDefinition:
      dataRow = new ChartDefinition();
      break;
    case SquashTmDataRowType.ReportDefinition:
      dataRow = new ReportDefinition();
      break;
    case SquashTmDataRowType.CustomReportDashboard:
      dataRow = new CustomReportDashboard();
      break;
    case SquashTmDataRowType.CustomReportCustomExport:
      dataRow = new CustomReportCustomExport();
      break;
    case SquashTmDataRowType.ActionWordLibrary:
      dataRow = new ActionWordLibrary();
      break;
    case SquashTmDataRowType.ActionWord:
      dataRow = new ActionWord();
      break;
    case SquashTmDataRowType.AutomationRequest:
      dataRow = new AutomationRequestRow();
      break;
    case SquashTmDataRowType.Directory:
      dataRow = new Directory();
      break;
    case SquashTmDataRowType.File:
      dataRow = new File();
      break;
    case SquashTmDataRowType.AutomatedSuite:
      dataRow = new AutomatedSuiteRow();
      break;
    default:
      dataRow = new GenericDataRow();
  }

  dataRow.projectId = literal.projectId;
  attachPermissionsToRow(dataRow, projectsData);
  Object.assign(dataRow, literal);
  return dataRow;
}

export function extractDataRowType(literal: Partial<DataRow>) {
  let type = 'Generic';
  if (literal.type) {
    type = literal.type;
  } else {
    if (typeof literal.id === 'string') {
      try {
        type = literal.id.split('-')[0];
      } catch (e) {
        console.log(
          `Unable to auto assign row type from id : ${literal.id}. Will keep generic type`,
        );
      }
    }
  }
  return type;
}

function attachPermissionsToRow(dataRow: DataRow, projectData: ProjectDataMap) {
  if (dataRow.projectId == null) {
    dataRow.simplePermissions = new ReadOnlyPermissions();
    return;
  }

  const project = projectData[dataRow.projectId];

  if (project == null) {
    dataRow.simplePermissions = new ReadOnlyPermissions();
    return;
  }

  switch (dataRow.type) {
    // TestCase WS
    case SquashTmDataRowType.TestCaseLibrary:
    case SquashTmDataRowType.TestCaseFolder:
    case SquashTmDataRowType.TestCase:
      dataRow.simplePermissions = new TestCasePermissions(project);
      break;
    // Requirement WS
    case SquashTmDataRowType.RequirementLibrary:
    case SquashTmDataRowType.RequirementFolder:
    case SquashTmDataRowType.HighLevelRequirement:
    case SquashTmDataRowType.Requirement:
      dataRow.simplePermissions = new RequirementPermissions(project);
      break;
    // Campaign WS
    case SquashTmDataRowType.CampaignLibrary:
    case SquashTmDataRowType.CampaignFolder:
    case SquashTmDataRowType.Campaign:
    case SquashTmDataRowType.Sprint:
    case SquashTmDataRowType.SprintGroup:
    case SquashTmDataRowType.Iteration:
    case SquashTmDataRowType.TestSuite:
    case SquashTmDataRowType.TestPlanItem:
    case SquashTmDataRowType.IterationTestPlanItem:
    case SquashTmDataRowType.CampaignTestPlanItem:
    case SquashTmDataRowType.Execution:
    case SquashTmDataRowType.AutomatedSuite:
      dataRow.simplePermissions = new CampaignPermissions(project);
      break;
    // Custom Report WS
    case SquashTmDataRowType.CustomReportLibrary:
    case SquashTmDataRowType.CustomReportFolder:
    case SquashTmDataRowType.ChartDefinition:
    case SquashTmDataRowType.ReportDefinition:
    case SquashTmDataRowType.CustomReportCustomExport:
    case SquashTmDataRowType.CustomReportDashboard:
      dataRow.simplePermissions = new CustomReportPermissions(project);
      break;
    case SquashTmDataRowType.AutomationRequest:
      dataRow.simplePermissions = new AutomationRequestPermissions(project);
      break;
    case SquashTmDataRowType.ActionWordLibrary:
    case SquashTmDataRowType.ActionWord:
      dataRow.simplePermissions = new ActionWordPermissions(project);
      break;
    default:
      dataRow.simplePermissions = new ReadOnlyPermissions();
  }
}

export function convertSqtmLiterals(
  literals: Partial<DataRow>[],
  projectsData: ProjectDataMap,
): SquashTmDataRow[] {
  return literals.map((literal) => convertSqtmLiteral(literal, projectsData));
}

// return the numeric id from the concatenated form type-id
export function parseDataRowId(candidate: SquashTmDataRow | Identifier): number {
  let id: string | number;
  if (typeof candidate === 'string') {
    id = candidate;
  } else if (typeof candidate === 'object') {
    id = candidate.id;
  }
  if (typeof id === 'string') {
    const splitNode = id.split('-');
    return Number.parseInt(splitNode[1], 10);
  }
  throw Error(`Unable to parse id ${id}`);
}

export function toEntityRowReference(candidate: SquashTmDataRow | Identifier): EntityRowReference {
  let id: string | number;
  if (typeof candidate === 'string') {
    id = candidate;
  } else if (typeof candidate === 'object') {
    id = candidate.id;
  }
  if (typeof id === 'string') {
    const splitNode = id.split('-');
    const candidateType = splitNode[0];
    const typeElement = SquashTmDataRowType[candidateType];
    if (!typeElement) {
      throw Error(`Unable to parse id ${id}`);
    }
    return new EntityRowReference(Number.parseInt(splitNode[1], 10), typeElement);
  }
  throw Error(`Unable to parse id ${id}`);
}

export class EntityRowReference {
  constructor(
    public id: number,
    public entityType: SquashTmDataRowType,
  ) {}

  asString(): string {
    return `${this.entityType}-${this.id}`;
  }
}

export class ProjectReference {
  constructor(public id: number) {}

  static fromString(str: string): ProjectReference {
    const splitNode = str.split('-');
    const projectType = splitNode[0];
    if (!projectType || projectType !== 'Project') {
      throw Error(`Unable to parse ${str} as ProjectReference`);
    }
    return new ProjectReference(Number.parseInt(splitNode[1], 10));
  }

  asString(): string {
    return `Project-${this.id}`;
  }
}
