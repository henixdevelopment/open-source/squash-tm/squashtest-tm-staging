import { GridNodeGenerator } from './grid-node-generator';
import { convert, flatTree } from './client-grid-node-generator';
import { PaginationState } from '../../model/state/pagination.state';
import { GridState } from '../../model/state/grid.state';
import { Observable, of } from 'rxjs';
import { GridNodeState } from '../../model/grid-node.model';
import { AbstractNodeGenerator } from './abstract-node-generator';
import { gridNodeStateAdapter } from '../../model/state/grid-nodes.state';

export class ServerGridNodeGenerator extends AbstractNodeGenerator implements GridNodeGenerator {
  computeNodeTree(state: Readonly<GridState>): Observable<GridState> {
    const paginationState: Readonly<PaginationState> = state.paginationState;
    let nodes: GridNodeState[];
    if (paginationState.active) {
      const ids = state.dataRowState.ids as any[];
      nodes = ids.map((id, index) => {
        const dataRow = state.dataRowState.entities[id];
        return convert(
          dataRow,
          0,
          index,
          state.dataRowState.selectedRows.includes(dataRow.id),
          false,
          false,
          [false],
        );
      });
    } else {
      nodes = flatTree(state.dataRowState, state.columnState, state.filterState, state.uiState);
    }
    if (nodes.length === 0) {
      const emptyNode = this.createPlaceholderNode();
      nodes.push(emptyNode);
    }
    const nodeState = gridNodeStateAdapter.setAll(nodes, state.nodesState);
    return of({ ...state, nodesState: nodeState });
  }
}
