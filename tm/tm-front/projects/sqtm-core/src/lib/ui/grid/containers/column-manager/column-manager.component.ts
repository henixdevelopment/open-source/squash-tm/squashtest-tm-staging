import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { GridDisplay } from '../../model/grid-display.model';
import { GridService } from '../../services/grid.service';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { Identifier } from '../../../../model/entity.model';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { ColumnDisplay } from '../../model/column-display.model';
import { GridViewportName } from '../../../../model/grids/grid-viewport-name';

@Component({
  selector: 'sqtm-core-column-manager',
  template: `
    <div class="full-width full-height">
      <!--          <div>-->
      <!--              <i nz-icon nzType="pushpin" nzTheme="fill"></i>-->
      <!--          </div>-->
      <div
        #leftViewport="cdkDropList"
        cdkDropList
        [cdkDropListConnectedTo]="[mainViewport, leftViewport, rightViewport]"
        (cdkDropListDropped)="dropColumn($event, 'leftViewport')"
        style="background-color: #00C875"
      >
        @for (columnDisplay of leftViewportColumns$ | async; track columnDisplay) {
          <div cdkDrag cdkDragLockAxis="y" [cdkDragData]="columnDisplay">
            <input
              type="checkbox"
              [checked]="columnDisplay.show"
              (click)="toggleColumnVisibility($event, columnDisplay.id)"
            />
            {{ columnDisplay.id }}
          </div>
        }
        @if ((leftViewportColumns$ | async).length === 0) {
          <span>DND HERE</span>
        }
      </div>

      <div
        #mainViewport="cdkDropList"
        cdkDropList
        [cdkDropListConnectedTo]="[mainViewport, leftViewport, rightViewport]"
        (cdkDropListDropped)="dropColumn($event, 'mainViewport')"
      >
        @for (columnDisplay of mainViewportColumns$ | async; track columnDisplay) {
          <div cdkDrag cdkDragLockAxis="y" [cdkDragData]="columnDisplay">
            <input
              type="checkbox"
              [checked]="columnDisplay.show"
              (click)="toggleColumnVisibility($event, columnDisplay.id)"
            />
            {{ columnDisplay.id }}
          </div>
        }
      </div>

      <div
        #rightViewport="cdkDropList"
        cdkDropList
        [cdkDropListConnectedTo]="[mainViewport, leftViewport, rightViewport]"
        (cdkDropListDropped)="dropColumn($event, 'rightViewport')"
        style="background-color: #00C875"
      >
        @for (columnDisplay of rightViewportColumns$ | async; track columnDisplay) {
          <div cdkDrag cdkDragLockAxis="y" [cdkDragData]="columnDisplay">
            <input
              type="checkbox"
              [checked]="columnDisplay.show"
              (click)="toggleColumnVisibility($event, columnDisplay.id)"
            />
            {{ columnDisplay.id }}
          </div>
        }
        @if ((rightViewportColumns$ | async).length === 0) {
          <span>DND HERE</span>
        }
      </div>
    </div>
  `,
  styleUrls: ['./column-manager.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ColumnManagerComponent implements OnInit, OnDestroy {
  gridDisplay$: Observable<GridDisplay>;
  leftViewportColumns$: Observable<ColumnDisplay[]>;
  mainViewportColumns$: Observable<ColumnDisplay[]>;
  rightViewportColumns$: Observable<ColumnDisplay[]>;

  private unsub$ = new Subject<void>();

  constructor(private grid: GridService) {}

  ngOnInit() {
    this.gridDisplay$ = this.grid.gridDisplay$.pipe(takeUntil(this.unsub$));

    this.leftViewportColumns$ = this.getViewportColumns('leftViewport');
    this.mainViewportColumns$ = this.getViewportColumns('mainViewport');
    this.rightViewportColumns$ = this.getViewportColumns('rightViewport');
  }

  private getViewportColumns(viewportName: GridViewportName) {
    return this.grid.gridDisplay$.pipe(
      takeUntil(this.unsub$),
      map((gridDisplay) => gridDisplay[viewportName].columnDisplays),
    );
  }

  toggleColumnVisibility($event: MouseEvent, id: string) {
    this.grid.toggleColumnVisibility(id);
    return false;
  }

  dropColumn($event: CdkDragDrop<any>, moveToViewport: GridViewportName) {
    const index = $event.currentIndex;
    const id: Identifier = $event.item.data['id'];
    this.grid.moveColumn({ id, index, moveToViewport });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
