import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { AbstractCellRendererComponent } from '../abstract-cell-renderer/abstract-cell-renderer.component';
import { GridService } from '../../../services/grid.service';

@Component({
  selector: 'sqtm-core-selectable-numeric-cell-renderer',
  template: `
    @if (columnDisplay && row) {
      <div class="full-width full-height" [sqtmCoreSelectable]="row.id">
        <sqtm-core-numeric-cell-renderer
          [columnDisplay]="columnDisplay"
          [row]="row"
          [depth]="depth"
          [index]="index"
          [gridDisplay]="gridDisplay"
          [selected]="selected"
          [showAsFilteredParent]="showAsFilteredParent"
        >
        </sqtm-core-numeric-cell-renderer>
      </div>
    }
  `,
  styleUrls: ['./selectable-numeric-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectableNumericCellRendererComponent extends AbstractCellRendererComponent {
  constructor(gridService: GridService, cdr: ChangeDetectorRef) {
    super(gridService, cdr);
  }
}
