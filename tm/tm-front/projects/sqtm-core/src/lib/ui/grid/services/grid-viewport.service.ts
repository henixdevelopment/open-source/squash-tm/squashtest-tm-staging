import { Injectable } from '@angular/core';
import { GridService } from './grid.service';
import { combineLatest, Observable, Subject } from 'rxjs';
import { Identifier } from '../../../model/entity.model';
import { GridDisplay, ViewportDisplay } from '../model/grid-display.model';
import { map, shareReplay } from 'rxjs/operators';
import { gridLogger } from '../grid.logger';
import { Extendable, ExtensibleWidthCalculationStrategy } from '../model/column-definition.model';
import { ColumnDisplay } from '../model/column-display.model';
import { RenderedGridViewportName } from '../../../model/grids/grid-viewport-name';

const logger = gridLogger.compose('GridViewportService');

@Injectable()
export class GridViewportService {
  private resizeSubject: Subject<ResizeEvent> = new Subject();
  private resize$ = this.resizeSubject.asObservable();

  public renderedGridViewport$: Observable<RenderedGridViewport>;

  constructor(private grid: GridService) {
    this.renderedGridViewport$ = combineLatest([grid.gridDisplay$, this.resize$]).pipe(
      map(([gridDisplay, resize]: [GridDisplay, ResizeEvent]) => {
        const leftViewport = GridViewportService.handleAuxiliaryViewport(gridDisplay.leftViewport);
        const rightViewport = GridViewportService.handleAuxiliaryViewport(
          gridDisplay.rightViewport,
        );
        return {
          leftViewport: leftViewport,
          mainViewport: GridViewportService.handleMainViewport(gridDisplay, resize.width),
          rightViewport: rightViewport,
        };
      }),
      shareReplay(1),
    );
  }

  notifyContainerViewPortResize(resizeEvent: ResizeEvent) {
    this.resizeSubject.next(resizeEvent);
  }

  private static handleAuxiliaryViewport(viewport: ViewportDisplay): GridViewportColumns {
    const gridViewportColumn = viewport.columnDisplays
      .filter((column) => column.show)
      .reduce(
        (gridViewportColumns, column) => {
          const width = column.widthCalculationStrategy.width;
          gridViewportColumns.columns[column.id] = {
            calculatedWidth: width,
            left: gridViewportColumns.totalWidth,
          };
          gridViewportColumns.totalWidth += width;
          gridViewportColumns.viewportWidth += width;
          return gridViewportColumns;
        },
        {
          columns: {},
          totalWidth: 0,
          viewportWidth: 0,
        },
      );
    return gridViewportColumn;
  }

  public static handleMainViewport(gridDisplay: GridDisplay, viewportScreenWidth: number) {
    const availableWidth = viewportScreenWidth - gridDisplay.mainViewport.totalWidth;
    logger.trace(`Width ${viewportScreenWidth}`);
    logger.trace(`Grid Display total width ${gridDisplay.mainViewport.totalWidth}`);
    logger.trace(`Available width ${availableWidth}`);

    // Total fraction calculation
    const totalFraction = GridViewportService.calculateTotalFraction(gridDisplay.mainViewport);

    // Assignation of pixels to each variant columns
    // The rule is :
    // - Each variable column have his width initialized at min-width previously
    // - Now we add remaining pixels according to fraction allocation. This way we guaranty that no column can have width < min-size
    // and avoid complex calculations that could slow the grid for no functional gain.
    // - It's mean that a column with fraction 3 will not be 3 time larger than a fraction 1 column.
    // It will just have 3/4 of available pixel assigned.
    const columnToRenderDict = GridViewportService.expendColumns(
      gridDisplay.mainViewport,
      totalFraction,
      availableWidth,
    );

    // 3 - Calculate final view port width (NOT container width, but width of each row) and add left offset property to columns
    return Object.values(columnToRenderDict).reduce<GridViewportColumns>(
      (reduced, columnToRender) => {
        columnToRender.left = reduced.totalWidth;
        reduced.totalWidth += columnToRender.calculatedWidth;
        return reduced;
      },
      {
        totalWidth: 0,
        viewportWidth: viewportScreenWidth,
        columns: columnToRenderDict,
      },
    );
  }

  private static calculateTotalFraction(viewportDisplay: ViewportDisplay): number {
    const total = viewportDisplay.columnDisplays
      .filter((columnDisplay) => columnDisplay.show)
      .map((columnDisplay) => columnDisplay.widthCalculationStrategy)
      .filter((strategy) => strategy.isExtensible)
      .reduce<number>((fraction: number, strategy: ExtensibleWidthCalculationStrategy) => {
        return fraction + strategy.fraction;
      }, 0);
    return total;
  }

  private static expendColumns(
    viewportDisplay,
    totalFraction: number,
    availableWidth: number,
  ): ColumnToRenderDict {
    const columnToRenderDict: ColumnToRenderDict = {};
    viewportDisplay.columnDisplays
      .filter((column: ColumnDisplay) => column.show)
      .reduce((pixelsAddedPreviously, columnDisplay, currentIndex, array) => {
        // if last column we assign all remaining pixels, so rounding in previous columns doesn't cause any blank pixels
        if (currentIndex === array.length - 1) {
          const rest = Math.max(availableWidth - pixelsAddedPreviously, 0);
          columnToRenderDict[columnDisplay.id] = {
            calculatedWidth: columnDisplay.widthCalculationStrategy.width + rest,
          };
        } else {
          let pixelToAdd = 0;
          if (columnDisplay.widthCalculationStrategy.isExtensible && availableWidth > 0) {
            const extendable = columnDisplay.widthCalculationStrategy as Extendable;
            pixelToAdd = Math.floor((extendable.fraction / totalFraction) * availableWidth);
          }
          columnToRenderDict[columnDisplay.id] = {
            calculatedWidth: columnDisplay.widthCalculationStrategy.width + pixelToAdd,
          };
          return pixelToAdd + pixelsAddedPreviously;
        }
      }, 0);

    return columnToRenderDict;
  }
}

interface ResizeEvent {
  width: number;
  height: number;
  hasVerticalOverflow: boolean;
}

type ColumnToRenderDict = { [K in Identifier]: ColumnToRender };

export type RenderedGridViewport = { [K in RenderedGridViewportName]: GridViewportColumns };

export interface GridViewportColumns {
  columns: ColumnToRenderDict;
  totalWidth: number;
  viewportWidth: number;
}

export interface ColumnToRender {
  calculatedWidth: number;
  left?: number;
}
