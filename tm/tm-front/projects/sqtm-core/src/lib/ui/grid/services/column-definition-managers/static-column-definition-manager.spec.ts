import { TestBed, waitForAsync } from '@angular/core/testing';

import { StaticColumnDefinitionManager } from './static-column-definition.manager';
import { ColumnDefinition, Limited, Sort, SortedColumn } from '../../model/column-definition.model';
import { Identifier } from '../../../../model/entity.model';
import { GridState, initialGridState } from '../../model/state/grid.state';
import { tap } from 'rxjs/operators';
import { ColumnState, initialColumnState } from '../../model/state/column.state';
import { createColumn } from '../../grid-testing/grid-testing-utils';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReferentialDataService } from '../../../../core/referential/services/referential-data.service';

describe('StaticColumnDefinitionManager', () => {
  const refDataService = {};
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: ReferentialDataService, useValue: refDataService },
        StaticColumnDefinitionManager,
      ],
    }),
  );

  describe('Toggle column sorts', () => {
    interface DataType {
      initialSortedColumns: SortedColumn[];
      expectedSortedColumns: SortedColumn[];
      toggledColumn: Identifier;
      resetSort?: boolean;
    }

    const dataSets: DataType[] = [
      {
        initialSortedColumns: [],
        expectedSortedColumns: [{ id: 'name', sort: Sort.ASC }],
        toggledColumn: 'name',
      },
      {
        initialSortedColumns: [{ id: 'name', sort: Sort.ASC }],
        expectedSortedColumns: [{ id: 'name', sort: Sort.DESC }],
        toggledColumn: 'name',
      },
      {
        initialSortedColumns: [{ id: 'name', sort: Sort.DESC }],
        expectedSortedColumns: [],
        toggledColumn: 'name',
      },
      {
        initialSortedColumns: [{ id: 'name', sort: Sort.DESC }],
        expectedSortedColumns: [
          { id: 'name', sort: Sort.DESC },
          { id: 'severity', sort: Sort.ASC },
        ],
        toggledColumn: 'severity',
      },
      {
        initialSortedColumns: [
          { id: 'name', sort: Sort.DESC },
          { id: 'severity', sort: Sort.ASC },
        ],
        expectedSortedColumns: [
          { id: 'name', sort: Sort.DESC },
          { id: 'severity', sort: Sort.ASC },
          {
            id: 'reference',
            sort: Sort.ASC,
          },
        ],
        toggledColumn: 'reference',
      },
      {
        initialSortedColumns: [
          { id: 'name', sort: Sort.DESC },
          { id: 'severity', sort: Sort.ASC },
          {
            id: 'reference',
            sort: Sort.ASC,
          },
        ],
        expectedSortedColumns: [
          { id: 'name', sort: Sort.DESC },
          { id: 'severity', sort: Sort.DESC },
          {
            id: 'reference',
            sort: Sort.ASC,
          },
        ],
        toggledColumn: 'severity',
      },
      {
        initialSortedColumns: [
          { id: 'name', sort: Sort.DESC },
          { id: 'severity', sort: Sort.ASC },
          { id: 'reference', sort: Sort.ASC },
        ],
        expectedSortedColumns: [
          { id: 'name', sort: Sort.DESC },
          { id: 'severity', sort: Sort.DESC },
          {
            id: 'reference',
            sort: Sort.ASC,
          },
        ],
        toggledColumn: 'severity',
      },
      {
        initialSortedColumns: [
          { id: 'name', sort: Sort.DESC },
          { id: 'severity', sort: Sort.DESC },
          { id: 'reference', sort: Sort.ASC },
        ],
        expectedSortedColumns: [
          { id: 'name', sort: Sort.DESC },
          { id: 'reference', sort: Sort.ASC },
        ],
        toggledColumn: 'severity',
      },
      {
        initialSortedColumns: [
          { id: 'name', sort: Sort.DESC },
          { id: 'severity', sort: Sort.DESC },
          { id: 'reference', sort: Sort.ASC },
        ],
        expectedSortedColumns: [
          { id: 'severity', sort: Sort.DESC },
          { id: 'reference', sort: Sort.ASC },
        ],
        toggledColumn: 'name',
      },
      {
        initialSortedColumns: [
          { id: 'name', sort: Sort.DESC },
          { id: 'severity', sort: Sort.DESC },
          { id: 'reference', sort: Sort.ASC },
        ],
        expectedSortedColumns: [{ id: 'name', sort: Sort.ASC }],
        toggledColumn: 'name',
        resetSort: true,
      },
      {
        initialSortedColumns: [
          { id: 'severity', sort: Sort.DESC },
          { id: 'reference', sort: Sort.ASC },
        ],
        expectedSortedColumns: [{ id: 'name', sort: Sort.ASC }],
        toggledColumn: 'name',
        resetSort: true,
      },
    ];

    dataSets.forEach((dataSet, index) => runTest(dataSet, index));

    function runTest(data: DataType, index: number) {
      it(`
      DataSet ${index} -
      Should toggle column ${data.toggledColumn} with sorted column : ${JSON.stringify(data.initialSortedColumns)}`, function (done) {
        const columnDefinitionManager: StaticColumnDefinitionManager = TestBed.inject(
          StaticColumnDefinitionManager,
        );
        const gridState: GridState = initialGridState();
        gridState.columnState.sortedColumns = data.initialSortedColumns;
        gridState.columnState.shouldResetSorts = data.resetSort;

        columnDefinitionManager
          .toggleSortColumn(data.toggledColumn, gridState)
          .pipe(
            tap((nextState) => {
              const expectedState = initialGridState();
              expectedState.columnState.sortedColumns = data.expectedSortedColumns;
              expect(nextState).toEqual(expectedState);
            }),
          )
          .subscribe(() => done());
      });
    }
  });

  describe('Toggle Column Visibility', function () {
    interface DataType {
      toggledColumn: Identifier;
      order: Identifier[];
      initialDisplayedColumns: Identifier[];
      expectedDisplayedColumns: Identifier[];
    }

    const dataSets: DataType[] = [
      {
        initialDisplayedColumns: [],
        toggledColumn: 'name',
        order: ['name'],
        expectedDisplayedColumns: ['name'],
      },
      {
        initialDisplayedColumns: ['name'],
        toggledColumn: 'name',
        order: ['name'],
        expectedDisplayedColumns: [],
      },
      {
        initialDisplayedColumns: ['name', 'reference'],
        toggledColumn: 'severity',
        order: ['reference', 'name', 'severity'],
        expectedDisplayedColumns: ['name', 'reference', 'severity'],
      },
      {
        initialDisplayedColumns: ['name', 'severity', 'reference'],
        toggledColumn: 'severity',
        order: ['reference', 'name', 'severity'],
        expectedDisplayedColumns: ['name', 'reference'],
      },
    ];

    dataSets.forEach((dataSet, index) => runTest(dataSet, index));

    function runTest(data: DataType, index: number) {
      it(`Dataset ${index} - Should toggle column ${data.toggledColumn} visibility.`, function (done) {
        const columnDefinitionManager: StaticColumnDefinitionManager = TestBed.inject(
          StaticColumnDefinitionManager,
        );
        const gridState: GridState = initialGridState();
        gridState.columnState.entities = {
          reference: {
            ...createColumn('reference', true),
            show: data.initialDisplayedColumns.includes('reference'),
          },
          name: {
            ...createColumn('name', true),
            show: data.initialDisplayedColumns.includes('name'),
          },
          severity: {
            ...createColumn('severity', true),
            show: data.initialDisplayedColumns.includes('severity'),
          },
        };
        gridState.columnState.ids = ['name', 'severity', 'reference'];

        columnDefinitionManager
          .toggleColumnVisibility(data.toggledColumn, gridState)
          .pipe(
            tap((nextState) => {
              const visibleColumns = Object.values(nextState.columnState.entities)
                .filter((columnDefinition) => columnDefinition.show)
                .map((columnDefinition) => columnDefinition.id as Identifier);
              visibleColumns.forEach((columnId) => {
                expect(data.expectedDisplayedColumns).toContain(columnId);
              });
            }),
          )
          .subscribe(() => done());
      });
    }
  });

  describe('Change Column Size', function () {
    interface DataType {
      columnId: Identifier;
      width: number;
      offset: number;
      expectedWidth: number;
      columnState: ColumnState;
    }

    const columnState: ColumnState = {
      ...initialColumnState(),
      ids: ['name'],
      entities: { name: createColumn('name', true) },
      sortedColumns: [{ id: 'name', sort: Sort.ASC }],
    };

    const dataSets: DataType[] = [
      {
        columnId: 'name',
        width: 200,
        offset: 0,
        expectedWidth: 200,
        columnState,
      },
      {
        columnId: 'name',
        width: 40,
        offset: 20,
        expectedWidth: 60,
        columnState,
      },
      {
        columnId: 'name',
        width: 200,
        offset: -20,
        expectedWidth: 180,
        columnState,
      },
      {
        columnId: 'name',
        width: 200,
        offset: -200,
        // should refuse to resize under 40px
        expectedWidth: 40,
        columnState,
      },
    ];

    dataSets.forEach((data, index) => runTest(data, index));

    function runTest(data: DataType, index: number) {
      it(`Dataset ${index} - Should resize column. Width: ${data.width}. Offset: ${data.offset}. Expected: ${data.expectedWidth}`, (done) => {
        const gridState: GridState = { ...initialGridState(), columnState: data.columnState };
        gridState.columnState.entities[data.columnId].widthCalculationStrategy = new Limited(
          data.width,
          40,
          500,
        );
        const columnDefinitionManager: StaticColumnDefinitionManager = TestBed.inject(
          StaticColumnDefinitionManager,
        );

        columnDefinitionManager
          .resizeColumn(data.columnId, data.offset, gridState)
          .subscribe((state) => {
            const modifiedSize = state.columnState.entities[data.columnId]
              .widthCalculationStrategy as Limited;
            expect(modifiedSize.width).toEqual(data.expectedWidth);
            done();
          });
      });
    }
  });

  describe('Should append columns at determined index', function () {
    interface DataType {
      index: number;
      initialColumnState: ColumnState;
      insertedColumns: ColumnDefinition[];
      expectedMainViewportOrder: string[];
    }

    function createBasicDataset(): Pick<DataType, 'initialColumnState' | 'insertedColumns'> {
      return {
        initialColumnState: {
          ids: ['name', 'reference', 'nature'],
          entities: {
            name: createColumn('name'),
            reference: createColumn('reference'),
            nature: createColumn('nature'),
          },
          sortedColumns: [],
          leftViewport: { order: [] },
          mainViewport: { order: ['reference', 'nature', 'name'] },
          rightViewport: { order: [] },
        },
        insertedColumns: [createColumn('inserted-1'), createColumn('inserted-2')],
      };
    }

    const dataSets: DataType[] = [
      {
        ...createBasicDataset(),
        index: 1,
        expectedMainViewportOrder: ['reference', 'inserted-1', 'inserted-2', 'nature', 'name'],
      },
      {
        ...createBasicDataset(),
        index: 0,
        expectedMainViewportOrder: ['inserted-1', 'inserted-2', 'reference', 'nature', 'name'],
      },
      {
        ...createBasicDataset(),
        index: 2,
        expectedMainViewportOrder: ['reference', 'nature', 'inserted-1', 'inserted-2', 'name'],
      },
      {
        ...createBasicDataset(),
        index: null,
        expectedMainViewportOrder: ['reference', 'nature', 'name', 'inserted-1', 'inserted-2'],
      },
      {
        ...createBasicDataset(),
        index: 3,
        expectedMainViewportOrder: ['reference', 'nature', 'name', 'inserted-1', 'inserted-2'],
      },
      {
        ...createBasicDataset(),
        index: 150,
        expectedMainViewportOrder: ['reference', 'nature', 'name', 'inserted-1', 'inserted-2'],
      },
    ];

    dataSets.forEach((data, index) => runTest(data, index));

    function runTest(data: DataType, index: number) {
      it(`Dataset ${index} - Should insert columns ${JSON.stringify(data.insertedColumns)} at index ${data.index}`, waitForAsync(() => {
        const gridState: GridState = {
          ...initialGridState(),
          columnState: data.initialColumnState,
        };
        const columnDefinitionManager: StaticColumnDefinitionManager = TestBed.inject(
          StaticColumnDefinitionManager,
        );
        columnDefinitionManager
          .addColumnAtIndex(data.insertedColumns, gridState, data.index)
          .subscribe((state) => {
            expect(state.columnState.mainViewport.order).toEqual(data.expectedMainViewportOrder);
            data.insertedColumns.forEach((colDef) => {
              const columnState = state.columnState;
              expect(columnState.entities[colDef.id]).toBe(colDef);
            });
          });
      }));
    }
  });
});
