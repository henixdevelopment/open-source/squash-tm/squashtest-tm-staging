import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DummyCellComponent } from './dummy-cell.component';

describe('DummyCellComponent', () => {
  let component: DummyCellComponent;
  let fixture: ComponentFixture<DummyCellComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DummyCellComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DummyCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
