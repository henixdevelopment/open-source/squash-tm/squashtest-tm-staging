import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProjectScopeComponent } from './project-scope.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ReferentialDataService } from '../../../../../core/referential/services/referential-data.service';

describe('ProjectScopeComponent', () => {
  let component: ProjectScopeComponent;
  let fixture: ComponentFixture<ProjectScopeComponent>;

  const referentialDataServiceMock = jasmine.createSpyObj(['load']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: ReferentialDataService,
          useValue: referentialDataServiceMock,
        },
      ],
      declarations: [ProjectScopeComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectScopeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
