import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { RowRenderer } from '../RowRenderer';
import { GridDisplay, ViewportDisplay } from '../../../model/grid-display.model';
import { GridNode } from '../../../model/grid-node.model';
import { GridService } from '../../../services/grid.service';

@Component({
  selector: 'sqtm-core-empty-grid-message',
  template: `
    @if (gridService.gridState$ | async; as gridState) {
      @if (gridDisplay?.loaded && viewport.name === 'mainViewport') {
        <div class="full-width full-height">
          {{ gridState.uiState.emptyGridMessage | translate }}
        </div>
      }
    }
  `,
  styleUrls: ['./empty-grid-message.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmptyGridMessageComponent implements RowRenderer {
  gridDisplay: GridDisplay;
  gridNode: GridNode;
  viewport: ViewportDisplay;

  constructor(
    public cdRef: ChangeDetectorRef,
    protected gridService: GridService,
  ) {}
}
