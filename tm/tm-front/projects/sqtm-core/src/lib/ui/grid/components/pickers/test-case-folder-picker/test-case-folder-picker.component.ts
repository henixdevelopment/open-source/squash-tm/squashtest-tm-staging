import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
  ViewContainerRef,
} from '@angular/core';
import { GridService } from '../../../services/grid.service';
import { gridServiceFactory } from '../../../grid.service.provider';
import { RestService } from '../../../../../core/services/rest.service';
import { ReferentialDataService } from '../../../../../core/referential/services/referential-data.service';
import { DataRow } from '../../../model/data-row.model';
import { TreeWithStatePersistence } from '../../../../workspace-common/components/tree/tree-with-state-persistence';
import { Subject, throwError } from 'rxjs';
import { DialogService } from '../../../../dialog/services/dialog.service';
import { GridPersistenceService } from '../../../../../core/services/grid-persistence/grid-persistence.service';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { GridDefinition } from '../../../model/grid-definition.model';
import { treePicker } from '../../../model/grid-builders';
import {
  TEST_CASE_TREE_FOLDER_PICKER_CONFIG,
  TEST_CASE_TREE_FOLDER_PICKER_ID,
} from '../../../tree-pickers.constant';
import { column } from '../../../model/common-column-definition.builders';
import { GridColumnId } from '../../../../../shared/constants/grid/grid-column-id';
import { Extendable } from '../../../model/column-definition.model';
import { TreeNodeCellRendererComponent } from '../../../../cell-renderer-common/tree-node-cell-renderer/tree-node-cell-renderer.component';
import { ProjectDataMap } from '../../../../../model/project/project-data.model';
import {
  extractDataRowType,
  SquashTmDataRow,
  TestCase,
  TestCaseFolder,
  TestCaseLibrary,
} from '../../../model/data-row.type';
import { SquashTmDataRowType } from '../../../../../model/grids/data-row.model';
import { TestCasePermissions } from '../../../../../model/permissions/simple-permissions';

export function convertTestCaseLibraryAndFolderLiterals(
  literals: Partial<DataRow>[],
  projectsData: ProjectDataMap,
) {
  // DON'T INLINE THE VAR, IT WOULD MAKE BUILD CRASH WITH ERROR :
  // {"__symbolic":"error","message":"Lambda not supported","line":42,"character":22}
  const converted: SquashTmDataRow[] = literals
    .map((literal) => convertTestCaseLibraryAndFolderLiteral(literal, projectsData))
    .filter((row) => row.simplePermissions.canCreate);
  return converted;
}

export function convertTestCaseLibraryAndFolderLiteral(
  literal: Partial<DataRow>,
  projectsData: ProjectDataMap,
): SquashTmDataRow {
  let dataRow: DataRow;
  const type = extractDataRowType(literal);

  switch (type) {
    case SquashTmDataRowType.TestCaseLibrary:
      dataRow = new TestCaseLibrary();
      break;
    case SquashTmDataRowType.TestCaseFolder:
      dataRow = new TestCaseFolder();
      break;
    case SquashTmDataRowType.TestCase:
      dataRow = new TestCase();
      break;
    default:
      throwError(() => 'Not handled type ' + type);
  }

  dataRow.projectId = literal.projectId;
  const project = projectsData[dataRow.projectId];
  dataRow.simplePermissions = new TestCasePermissions(project);

  Object.assign(dataRow, literal);

  if (type == SquashTmDataRowType.TestCase) {
    dataRow.selectable = false;
  }

  return dataRow;
}

export function testCaseTreeFolderPickerConfigFactory(): GridDefinition {
  return treePicker(TEST_CASE_TREE_FOLDER_PICKER_ID)
    .server()
    .withServerUrl(['test-case-tree'])
    .withColumns([
      column(GridColumnId.NAME)
        .changeWidthCalculationStrategy(new Extendable(300))
        .withRenderer(TreeNodeCellRendererComponent),
    ])
    .disableMultiSelection()
    .withRowConverter(convertTestCaseLibraryAndFolderLiterals)
    .build();
}

@Component({
  selector: 'sqtm-core-test-case-folder-picker',
  template: ` <sqtm-core-grid class="sqtm-core-prevent-selection-in-grid"></sqtm-core-grid> `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: TEST_CASE_TREE_FOLDER_PICKER_CONFIG,
      useFactory: testCaseTreeFolderPickerConfigFactory,
    },
    {
      provide: GridService,
      useFactory: gridServiceFactory,
      deps: [RestService, TEST_CASE_TREE_FOLDER_PICKER_CONFIG, ReferentialDataService],
    },
  ],
})
export class TestCaseFolderPickerComponent
  extends TreeWithStatePersistence
  implements OnInit, OnDestroy
{
  @Output()
  selectedRows = new EventEmitter<DataRow[]>();

  unsub$ = new Subject<void>();

  constructor(
    public tree: GridService,
    protected referentialDataService: ReferentialDataService,
    protected restService: RestService,
    protected dialogService: DialogService,
    protected vcr: ViewContainerRef,
    protected gridPersistenceService: GridPersistenceService,
    protected router: Router,
  ) {
    super(
      tree,
      referentialDataService,
      gridPersistenceService,
      restService,
      router,
      dialogService,
      vcr,
      null,
      'test-case-tree',
    );
  }

  ngOnInit() {
    this.tree.selectedRows$.pipe(takeUntil(this.unsub$)).subscribe((rows) => {
      this.selectedRows.next(rows);
    });
    this.initData();
  }

  ngOnDestroy(): void {
    this.tree.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }
}
