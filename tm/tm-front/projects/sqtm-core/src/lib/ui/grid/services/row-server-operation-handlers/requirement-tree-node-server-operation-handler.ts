import { Injectable } from '@angular/core';
import { RestService } from '../../../../core/services/rest.service';
import { DialogService } from '../../../dialog/services/dialog.service';
import { ReferentialDataService } from '../../../../core/referential/services/referential-data.service';
import { ActionErrorDisplayService } from '../../../../core/services/errors-handling/action-error-display.service';
import {
  DropOperationData,
  TreeNodeServerOperationHandler,
} from './tree-node-server-operation-handler';
import { finalize, take } from 'rxjs/operators';
import { getSelectedRows } from '../data-row-loaders/data-row-utils';
import { RequirementVersionService } from '../../../../core/services/requirement/requirement-version.service';
import { DataRow } from '../../model/data-row.model';
import { EntityPathHeaderService } from '../../../../core/services/entity-path-header/entity-path-header.service';
import { GridClipboardService } from '../../persistence/grid-clipboard.service';
import { SquashTmDataRowType } from '../../../../model/grids/data-row.model';

@Injectable()
export class RequirementTreeNodeServerOperationHandler extends TreeNodeServerOperationHandler {
  constructor(
    protected restService: RestService,
    protected dialogService: DialogService,
    protected referentialDataService: ReferentialDataService,
    protected actionErrorDisplayService: ActionErrorDisplayService,
    private requirementVersionService: RequirementVersionService,
    protected entityPathHeaderService: EntityPathHeaderService,
    protected gridClipboardService: GridClipboardService,
  ) {
    super(
      restService,
      dialogService,
      referentialDataService,
      actionErrorDisplayService,
      entityPathHeaderService,
      gridClipboardService,
    );
  }

  notifyInternalDrop(): void {
    this.internalDrop()
      .pipe(
        take(1),
        finalize(() => this.grid.completeAsyncOperation()),
      )
      .subscribe({
        next: (data: DropOperationData) => {
          this.grid.commit(data.gridState);
          const dataRows = getSelectedRows(data.gridState.dataRowState);
          if (this.isSingleRowWithUpdatablePathSelected(dataRows)) {
            if (this.singleRowSelectedIsrequirementOrHighLvlReq(dataRows)) {
              this.requirementVersionService.refreshView();
            } else {
              this.entityPathHeaderService.refreshPath();
            }
          }
        },
        error: (err) => this.handleServerDropError(err),
      });
  }

  private singleRowSelectedIsrequirementOrHighLvlReq(dataRows: DataRow[]): boolean {
    return (
      dataRows.length === 1 &&
      (dataRows[0].type === SquashTmDataRowType.Requirement ||
        dataRows[0].type === SquashTmDataRowType.HighLevelRequirement)
    );
  }
}
