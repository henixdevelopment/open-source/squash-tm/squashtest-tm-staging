import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { AbstractHeaderRendererComponent } from '../abstract-header-renderer/abstract-hearder-renderer.component';
import { GridService } from '../../../services/grid.service';
import { GridDisplay } from '../../../model/grid-display.model';

@Component({
  selector: 'sqtm-core-icon-header-renderer',
  template: ` @if (gridDisplay) {
    <div class="full-width flex-row" [style.height]="calculateRowHeight(gridDisplay)">
      <i
        nz-icon
        style="margin: auto"
        [nzType]="columnDisplay?.iconName"
        [nzTheme]="columnDisplay?.iconTheme"
      ></i>
      <div class="full-height m-r-5 flex-fixed-size resize-handler"></div>
    </div>
  }`,
  styleUrls: ['./icon-header-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IconHeaderRendererComponent extends AbstractHeaderRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }

  calculateRowHeight(gridDisplay: GridDisplay): string {
    return `${gridDisplay.rowHeight}px`;
  }
}
