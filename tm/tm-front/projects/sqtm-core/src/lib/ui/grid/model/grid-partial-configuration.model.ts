import { SortedColumn } from './column-definition.model';
import { GridFilterSnapshot } from '../../../core/services/grid-persistence/grid-state-snapshot';

export interface GridPartialConfiguration {
  pagination?: GridPartialPagination;
  sortedColumns?: SortedColumn[];
  filters?: GridFilterSnapshot[];
}

export interface GridPartialPagination {
  page?: number;
  // Actual pagination size. If -1 it will show all available lines.
  size?: number;
}
