import { TestBed } from '@angular/core/testing';

import { GridViewportService } from './grid-viewport.service';
import { initialGridState } from '../model/state/grid.state';
import { createColumn } from '../grid-testing/grid-testing-utils';
import { Extendable, Limited, Fixed } from '../model/column-definition.model';
import { GridServiceImpl } from './grid-service-impl';
import { TestingUtilsModule } from '../../testing-utils/testing-utils.module';
import { GridTestingModule } from '../grid-testing/grid-testing.module';
import { ColumnState } from '../model/state/column.state';

describe('GridViewportService', () => {
  let service: GridViewportService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, GridTestingModule],
      providers: [GridViewportService],
    });
    service = TestBed.inject(GridViewportService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should compute main ViewportDisplay without available size to expend', () => {
    const gridState = { ...initialGridState() };
    const columnState = gridState.columnState;
    columnState.mainViewport = {
      order: ['column-7', 'column-2', 'column-5', 'column-12'],
    };
    columnState.leftViewport = {
      order: ['column-1', 'column-3'],
    };
    columnState.ids = ['column-2', 'column-5', 'column-7', 'column-12'];
    columnState.entities = {
      'column-2': { ...createColumn('column-2', false), widthCalculationStrategy: new Fixed(200) },
      'column-5': { ...createColumn('column-5', false), widthCalculationStrategy: new Limited(75) },
      'column-7': { ...createColumn('column-7', false), show: false },
      'column-12': {
        ...createColumn('column-12', false),
        widthCalculationStrategy: new Extendable(50),
      },
    };
    const mainViewportColumnDisplay = GridServiceImpl.transformGridDisplay(
      gridState.columnState,
      gridState.definitionState,
      [],
      gridState.uiState,
      gridState.filterState,
    );
    const gridViewportColumns = GridViewportService.handleMainViewport(
      mainViewportColumnDisplay,
      325,
    );
    gridViewportColumns.totalWidth = 325;
    gridViewportColumns.viewportWidth = 325;
    gridViewportColumns.columns['column-2'].calculatedWidth = 200;
    gridViewportColumns.columns['column-2'].left = 0;
    gridViewportColumns.columns['column-5'].calculatedWidth = 75;
    gridViewportColumns.columns['column-5'].left = 200;
  });

  describe('Compute main ViewPortDisplay', () => {
    interface DataType {
      name: string;
      columnState: ColumnState;
      column2ExpectedWidth: number;
      column4ExpectedWidth: number;
      totalWidthExpected: number;
      mainViewportContainerWidth: number;
    }

    const columnState: ColumnState = {
      ids: ['column-1', 'column-2', 'column-3', 'column-4'],
      entities: {},
      sortedColumns: [],
      leftViewport: {
        order: [],
      },
      mainViewport: {
        order: ['column-3', 'column-2', 'column-1', 'column-4'],
      },
      rightViewport: {
        order: [],
      },
    };

    const dataSets: DataType[] = [
      {
        name: '2 Extendable columns',
        columnState: {
          ...columnState,
          entities: {
            'column-1': {
              ...createColumn('column-1', false),
              widthCalculationStrategy: new Fixed(200),
            },
            'column-2': {
              ...createColumn('column-2', false),
              widthCalculationStrategy: new Extendable(50),
            },
            'column-3': { ...createColumn('column-3', false), show: false },
            'column-4': {
              ...createColumn('column-4', false),
              widthCalculationStrategy: new Extendable(50),
            },
          },
        },
        column2ExpectedWidth: 350,
        column4ExpectedWidth: 350,
        mainViewportContainerWidth: 900,
        totalWidthExpected: 900,
      },
      {
        name: '2 Extendable columns with different fractions',
        columnState: {
          ...columnState,
          entities: {
            'column-1': {
              ...createColumn('column-1', false),
              widthCalculationStrategy: new Fixed(200),
            },
            'column-2': {
              ...createColumn('column-2', false),
              widthCalculationStrategy: new Extendable(50, 3),
            },
            'column-3': { ...createColumn('column-3', false), show: false },
            'column-4': {
              ...createColumn('column-4', false),
              widthCalculationStrategy: new Extendable(50),
            },
          },
        },
        column2ExpectedWidth: 500,
        column4ExpectedWidth: 200,
        mainViewportContainerWidth: 900,
        totalWidthExpected: 900,
      },
      {
        name: '2 Extendable columns with different fractions but no space available',
        columnState: {
          ...columnState,
          entities: {
            'column-1': {
              ...createColumn('column-1', false),
              widthCalculationStrategy: new Fixed(200),
            },
            'column-2': {
              ...createColumn('column-2', false),
              widthCalculationStrategy: new Extendable(50, 3),
            },
            'column-3': { ...createColumn('column-3', false), show: false },
            'column-4': {
              ...createColumn('column-4', false),
              widthCalculationStrategy: new Extendable(50),
            },
          },
        },
        column2ExpectedWidth: 50,
        column4ExpectedWidth: 50,
        mainViewportContainerWidth: 200,
        totalWidthExpected: 300,
      },
    ];

    dataSets.forEach((data, index) => runTest(data, index));

    function runTest(data: DataType, index: number) {
      it(`Dataset ${index} - It should compute viewport for ${data.name}`, () => {
        const gridState = { ...initialGridState() };
        gridState.columnState = data.columnState;
        const mainViewportColumnDisplay = GridServiceImpl.transformGridDisplay(
          gridState.columnState,
          gridState.definitionState,
          [],
          gridState.uiState,
          gridState.filterState,
        );
        const gridViewportColumns = GridViewportService.handleMainViewport(
          mainViewportColumnDisplay,
          data.mainViewportContainerWidth,
        );
        expect(Object.values(gridViewportColumns.columns).length).toEqual(3);
        expect(gridViewportColumns.columns['column-2'].calculatedWidth).toEqual(
          data.column2ExpectedWidth,
        );
        expect(gridViewportColumns.columns['column-4'].calculatedWidth).toEqual(
          data.column4ExpectedWidth,
        );
        expect(gridViewportColumns.totalWidth).toEqual(data.totalWidthExpected);
      });
    }
  });
});
