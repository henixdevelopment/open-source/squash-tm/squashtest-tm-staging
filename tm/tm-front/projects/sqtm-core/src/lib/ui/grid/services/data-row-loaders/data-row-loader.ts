import { Observable } from 'rxjs';
import { DataRow } from '../../model/data-row.model';
import { Identifier } from '../../../../model/entity.model';
import { GridState } from '../../model/state/grid.state';
import { TableValueChange } from '../../model/actions/table-value-change';
import { RemoveRowResult } from './remove-row-result';
import { Store } from '../../../../core/store/store';
import { ProjectDataMap } from '../../../../model/project/project-data.model';

/**
 * A dataRow loader is responsible for feeding the grid with data and managing the rows state.
 * It's the living heart of grid service layer and it's mainly a strategy for data loading that will be pushed into grid service.
 */
export interface DataRowLoader {
  setStore(store: Store<GridState>);

  loadInitialData(
    literals: Partial<DataRow>[],
    count: number,
    selectedRows: Identifier[],
    state: GridState,
    projectData: ProjectDataMap,
  ): Observable<GridState>;

  /**
   * Refresh the data according to actual pagination, filters, orders... If the grid is backed by a server side engine,
   * the request is fired, so this method is async and can be quite long to execute.
   * @param state actual state
   * @param projectData project data for giving permissions on rows.
   * @param keepSelectedRows choose if you want to keep selected rows while refreshing data
   * @param isPremium return if premium plugin is installed
   */
  refreshData(
    state: GridState,
    projectData: ProjectDataMap,
    keepSelectedRows: boolean,
    isPremium?: boolean,
  ): Observable<GridState>;

  openRows(
    ids: Identifier[],
    state$: Observable<GridState>,
    projectData: ProjectDataMap,
  ): Observable<GridState>;

  refreshSubTrees(
    ids: Identifier[],
    state$: Observable<GridState>,
    projectData: ProjectDataMap,
    forceOpen?: boolean,
  ): Observable<GridState>;

  closeRows(ids: Identifier[], state: GridState): Observable<GridState>;

  toggleRowSelection(id: Identifier, state: GridState): Observable<GridState>;

  /**
   * Add rows between the given id and previous last selected row. It is generally called when user select a row
   * while holding the shift keyboard key in grids.
   * @param id the id of the row to add
   * @param state grid state
   */
  addToSelection(id: Identifier, state: GridState): Observable<GridState>;

  unselectAllRows(state: GridState): Observable<GridState>;

  invertSelection(state: GridState): Observable<GridState>;

  selectSingleRow(id: Identifier, state: GridState): Observable<GridState>;

  /**
   * Insert the rows given in insertedRows argument, inside the row container designed by targetId argument.
   * IMPORTANT : This method DO NOT perform any check, all insert are considered valid. Use allowDropSibling,
   *  allowDropInto and AclService to check the insert BEFORE calling this method.
   * If targetId is null or undefined, the rows will be inserted as root rows. Which will always be the case if no tree is present in table
   * If inserted rows are subtrees, the hierarchy will be preserved. However, your root rows must have parentRowId set to null or undefined.
   * @param insertedRows an array of DataRows to insert
   * @param targetId the target container identifier
   * @param state grid state
   * @param index an optional index. If no index provided, rows will be appended after the actual content.
   */
  insertRows(
    insertedRows: DataRow[],
    targetId: Identifier,
    state: GridState,
    index?: number,
  ): Observable<GridState>;

  /**
   * Remove the rows designed by the param removedRowIds AND their children if any.
   * Do not send any request server side, the delete is meant to be resolved
   * before calling this method.
   */
  removeRows(removedRowIds: Identifier[], state: GridState): Observable<RemoveRowResult>;

  updateRows(
    ids: Identifier[],
    changes: TableValueChange[],
    state: GridState,
  ): Observable<GridState>;

  selectAllRows(state: GridState): Observable<GridState>;

  addRows(
    dataRows: DataRow[],
    state: GridState,
    parentId?: Identifier,
    position?: number,
  ): GridState;

  updateCell(url: string[], data: any): Observable<any>;

  beginAsync(state: Readonly<GridState>): GridState;

  endAsync(state: Readonly<GridState>): GridState;

  openExternalRows(
    ids: Identifier[],
    state$: Observable<GridState>,
    projectData: ProjectDataMap,
  ): Observable<GridState>;

  resetDataRowState();
}
