import { Injectable, ViewContainerRef } from '@angular/core';
import { RestService } from '../../../../core/services/rest.service';
import { DialogService } from '../../../dialog/services/dialog.service';
import { ReferentialDataService } from '../../../../core/referential/services/referential-data.service';
import { ActionErrorDisplayService } from '../../../../core/services/errors-handling/action-error-display.service';
import {
  DeleteData,
  DeleteSimulation,
  TreeNodeServerOperationHandler,
} from './tree-node-server-operation-handler';
import { map } from 'rxjs/operators';
import { getDraggedRows } from '../data-row-loaders/data-row-utils';
import { GridState } from '../../model/state/grid.state';
import { CampaignTreeConfirmDeleteConfiguration } from '../../../dialog/components/campaign-tree-confirm-delete-dialog/campaign-tree-confirm-delete-configuration';
import { DialogConfiguration } from '../../../dialog/services/dialog-feature.state';
import { CampaignTreeConfirmDeleteDialogComponent } from '../../../dialog/components/campaign-tree-confirm-delete-dialog/campaign-tree-confirm-delete-dialog.component';
import { DataRow } from '../../model/data-row.model';
import { Identifier } from '../../../../model/entity.model';
import { EntityPathHeaderService } from '../../../../core/services/entity-path-header/entity-path-header.service';
import { GridClipboardService } from '../../persistence/grid-clipboard.service';
import { SquashTmDataRowType } from '../../../../model/grids/data-row.model';

// Handle special case of campaign tree where additional rules apply to executions and suites when dragging or deleting nodes
@Injectable()
export class CampaignTreeNodeServerOperationHandler extends TreeNodeServerOperationHandler {
  constructor(
    restService: RestService,
    dialogService: DialogService,
    referentialDataService: ReferentialDataService,
    actionErrorDisplayService: ActionErrorDisplayService,
    protected entityPathHeaderService: EntityPathHeaderService,
    protected gridClipboardService: GridClipboardService,
    private readonly vcr: ViewContainerRef,
  ) {
    super(
      restService,
      dialogService,
      referentialDataService,
      actionErrorDisplayService,
      entityPathHeaderService,
      gridClipboardService,
    );
  }

  protected initCanCopy() {
    this.canCopy$ = this._grid.selectedRows$.pipe(
      map((rows) => {
        if (!rows.some((row) => row.allowMoves)) {
          // No selected row can be moved
          return false;
        }

        // Check if selection is homogeneous (there's only one type selected)
        const selectedRowTypes = [...new Set(rows.map((row) => row.type))]; // remove duplicates
        return selectedRowTypes.length === 1;
      }),
    );
  }

  protected doDeleteServerSide(deleteData: CampaignDeleteData) {
    const ids = deleteData.selectedRows.map((row) => row.id.toString());
    const pathParam = ids.join(',');
    return this.restService
      .delete([...deleteData.gridState.definitionState.serverRootUrl, pathParam], {
        params: { remove_from_iter: String(deleteData.removeFromIter) },
      })
      .pipe(
        map(() => {
          const nodesToRefreshIds = Array.from(
            new Set(deleteData.selectedRows.map((row) => row.parentRowId)),
          );
          return { ...deleteData, nodesToRefreshIds };
        }),
      );
  }

  protected showConfirmDeleteDialog(deleteData: CampaignDeleteData) {
    const containsSuites = deleteData.selectedRows.some(
      (row) => row.type === SquashTmDataRowType.TestSuite,
    );
    const data: Partial<CampaignTreeConfirmDeleteConfiguration> = {
      messageKey: `sqtm-core.dialog.delete.element.explain`,
      simulationReportMessages: deleteData.deleteSimulation.messageCollection,
      suffixMessageKey: 'sqtm-core.dialog.delete.element.suffix',
      titleKey: 'sqtm-core.dialog.delete.element.title',
      showRemoveFromIterCheckbox: containsSuites,
      level: 'DANGER',
      id: 'confirm-delete',
    };

    const configuration: DialogConfiguration = {
      id: 'confirm-delete',
      component: CampaignTreeConfirmDeleteDialogComponent,
      data: data,
      viewContainerReference: this.vcr,
      width: 600,
    };

    return this.dialogService
      .openDialog<CampaignTreeConfirmDeleteConfiguration, any>(configuration)
      .dialogClosed$.pipe(
        map((confirm) => {
          if (confirm != null) {
            return { ...deleteData, confirm: true, removeFromIter: confirm.removeFromIter };
          } else {
            return { ...deleteData, confirm: false };
          }
        }),
      );
  }

  allowDropSibling(id: Identifier, state: GridState): boolean {
    const dataRowState = state.dataRowState;
    const entities = dataRowState.entities;
    const dataRow: DataRow = entities[id];
    const draggedRows = getDraggedRows(state);
    const rowTypesInSelection = new Set(draggedRows.map((row) => row.type));

    if (!super.allowDropSibling(id, state)) {
      return false;
    }

    if (rowTypesInSelection.size > 1) {
      return false;
    }

    // Iterations and test suites cannot be moved to a different campaign
    for (const draggedRow of draggedRows) {
      if (
        draggedRow.type === SquashTmDataRowType.Iteration ||
        draggedRow.type === SquashTmDataRowType.TestSuite
      ) {
        if (dataRow.parentRowId !== draggedRow.parentRowId) {
          return false;
        }
      }
    }

    return true;
  }

  // drop into containers are allowed even if there is sorts and filters actives.
  allowDropInto(id: Identifier, state: GridState): boolean {
    const draggedRows = getDraggedRows(state);

    if (!super.allowDropInto(id, state)) {
      return false;
    }

    // Iterations and test suites cannot be moved to a different campaign
    for (const draggedRow of draggedRows) {
      if (
        draggedRow.type === SquashTmDataRowType.Iteration ||
        draggedRow.type === SquashTmDataRowType.TestSuite
      ) {
        return false;
      }
    }

    return true;
  }

  protected fetchSimulationData(deleteData: DeleteData) {
    const ids = deleteData.selectedRows.map((row) => row.id.toString());
    const pathParam = ids.join(',');
    const serverRootUrl: string[] = deleteData.gridState.definitionState.serverRootUrl;
    return this.restService
      .get<DeleteSimulation>([...serverRootUrl, 'deletion-simulation', pathParam])
      .pipe(map((deleteSimulation) => ({ ...deleteData, deleteSimulation })));
  }
}

interface CampaignDeleteData extends DeleteData {
  removeFromIter?: boolean;
}
