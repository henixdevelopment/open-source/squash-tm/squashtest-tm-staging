import { Injectable } from '@angular/core';
import { RestService } from '../../../../core/services/rest.service';
import { DialogService } from '../../../dialog/services/dialog.service';
import { ReferentialDataService } from '../../../../core/referential/services/referential-data.service';
import { ActionErrorDisplayService } from '../../../../core/services/errors-handling/action-error-display.service';
import { TreeNodeServerOperationHandler } from './tree-node-server-operation-handler';
import { EntityPathHeaderService } from '../../../../core/services/entity-path-header/entity-path-header.service';
import { GridClipboardService } from '../../persistence/grid-clipboard.service';

@Injectable()
export class CustomReportTreeNodeServerOperationHandler extends TreeNodeServerOperationHandler {
  constructor(
    protected restService: RestService,
    protected dialogService: DialogService,
    protected referentialDataService: ReferentialDataService,
    protected actionErrorDisplayService: ActionErrorDisplayService,
    protected entityPathHeaderService: EntityPathHeaderService,
    protected gridClipboardService: GridClipboardService,
  ) {
    super(
      restService,
      dialogService,
      referentialDataService,
      actionErrorDisplayService,
      entityPathHeaderService,
      gridClipboardService,
    );
  }

  protected shouldCheckInterProjectMoves(): boolean {
    return false;
  }
}
