import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewContainerRef,
} from '@angular/core';
import { TreeWithStatePersistence } from '../../../../workspace-common/components/tree/tree-with-state-persistence';
import { Identifier } from '../../../../../model/entity.model';
import { Subject, throwError } from 'rxjs';
import { GridService } from '../../../services/grid.service';
import { ReferentialDataService } from '../../../../../core/referential/services/referential-data.service';
import { RestService } from '../../../../../core/services/rest.service';
import { DialogService } from '../../../../dialog/services/dialog.service';
import { GridPersistenceService } from '../../../../../core/services/grid-persistence/grid-persistence.service';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import {
  RESTRICTED_CAMPAIGN_TREE_PICKER_CONFIG,
  RESTRICTED_CAMPAIGN_TREE_PICKER_ID,
} from '../../../tree-pickers.constant';
import { gridServiceFactory } from '../../../grid.service.provider';
import { ProjectDataMap } from '../../../../../model/project/project-data.model';
import { GridDefinition } from '../../../model/grid-definition.model';
import { Extendable } from '../../../model/column-definition.model';
import { TreeNodeCellRendererComponent } from '../../../../cell-renderer-common/tree-node-cell-renderer/tree-node-cell-renderer.component';
import { CampaignPermissions } from '../../../../../model/permissions/simple-permissions';
import { column } from '../../../model/common-column-definition.builders';
import { DataRowOpenState, SquashTmDataRowType } from '../../../../../model/grids/data-row.model';
import { treePicker } from '../../../model/grid-builders';
import { DataRow } from '../../../model/data-row.model';
import {
  Campaign,
  CampaignFolder,
  CampaignLibrary,
  extractDataRowType,
  Sprint,
  SprintGroup,
  SquashTmDataRow,
} from '../../../model/data-row.type';
import { GridColumnId } from '../../../../../shared/constants/grid/grid-column-id';

export function convertRestrictedCampaignLiterals(
  literals: Partial<DataRow>[],
  projectsData: ProjectDataMap,
) {
  // DON'T INLINE THE VAR, IT WOULD MAKE BUILD CRASH WITH ERROR :
  // {"__symbolic":"error","message":"Lambda not supported","line":42,"character":22}
  const converted: SquashTmDataRow[] = literals.map((literal) =>
    convertRestrictedCampaignLiteral(literal, projectsData),
  );
  return converted;
}

export function convertRestrictedCampaignLiteral(
  literal: Partial<DataRow>,
  projectsData: ProjectDataMap,
): SquashTmDataRow {
  let dataRow: DataRow;
  const type = extractDataRowType(literal);

  switch (type) {
    // Campaign WS
    case SquashTmDataRowType.CampaignLibrary:
      dataRow = new CampaignLibrary();
      break;
    case SquashTmDataRowType.CampaignFolder:
      dataRow = new CampaignFolder();
      break;
    case SquashTmDataRowType.Campaign:
      dataRow = new Campaign();
      break;
    case SquashTmDataRowType.Sprint:
      dataRow = new Sprint();
      break;
    case SquashTmDataRowType.SprintGroup:
      dataRow = new SprintGroup();
      break;
    default:
      throwError(() => 'Not handled type ' + type);
  }

  dataRow.projectId = literal.projectId;
  const project = projectsData[dataRow.projectId];
  dataRow.simplePermissions = new CampaignPermissions(project);

  Object.assign(dataRow, literal);
  // we disable hierarchy for campaign so no other nodes will be displayed
  if (type === SquashTmDataRowType.Campaign) {
    dataRow.state = DataRowOpenState.leaf;
    dataRow.children = [];
  }

  if (type !== SquashTmDataRowType.Campaign) {
    dataRow.selectable = false;
  }
  return dataRow;
}

/** @dynamic */
export function restrictedCampaignTreePickerConfigFactory(): GridDefinition {
  return treePicker(RESTRICTED_CAMPAIGN_TREE_PICKER_ID)
    .server()
    .withServerUrl(['campaign-tree'])
    .withColumns([
      column(GridColumnId.NAME)
        .changeWidthCalculationStrategy(new Extendable(300))
        .withRenderer(TreeNodeCellRendererComponent),
    ])
    .withRowConverter(convertRestrictedCampaignLiterals)
    .enableDrag()
    .build();
}

@Component({
  selector: 'sqtm-core-restricted-campaign-picker',
  template: ` <sqtm-core-grid class="sqtm-core-prevent-selection-in-grid"></sqtm-core-grid>`,
  styleUrls: ['./restricted-campaign-picker.component.less'],
  providers: [
    {
      provide: RESTRICTED_CAMPAIGN_TREE_PICKER_CONFIG,
      useFactory: restrictedCampaignTreePickerConfigFactory,
    },
    {
      provide: GridService,
      useFactory: gridServiceFactory,
      deps: [RestService, RESTRICTED_CAMPAIGN_TREE_PICKER_CONFIG, ReferentialDataService],
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RestrictedCampaignPickerComponent
  extends TreeWithStatePersistence
  implements OnInit, OnDestroy
{
  @Input()
  set pickerPersistenceKey(persistenceKey) {
    if (!this.persistenceKey) {
      this.persistenceKey = persistenceKey;
    } else {
      throw Error('Cannot change the persistence key dynamically ' + this.persistenceKey);
    }
  }

  @Input()
  initialSelectedNodes: Identifier[] = [];

  @Output()
  selectedRows = new EventEmitter<DataRow[]>();

  @Input()
  set enableMultiRowSelection(shouldBeEnabled: boolean) {
    this.tree.setMultiSelectionEnabled(shouldBeEnabled);
  }

  unsub$ = new Subject<void>();

  constructor(
    public tree: GridService,
    protected referentialDataService: ReferentialDataService,
    protected restService: RestService,
    protected dialogService: DialogService,
    protected vcr: ViewContainerRef,
    protected gridPersistenceService: GridPersistenceService,
    protected router: Router,
  ) {
    super(
      tree,
      referentialDataService,
      gridPersistenceService,
      restService,
      router,
      dialogService,
      vcr,
      null,
      'campaign-tree',
    );
  }

  ngOnInit() {
    this.tree.selectedRows$.pipe(takeUntil(this.unsub$)).subscribe((rows) => {
      this.selectedRows.next(rows);
    });

    this.initData({ selectedNodes: this.initialSelectedNodes });
    if (this.persistenceKey) {
      this.registerStatePersistence();
    }
  }

  ngOnDestroy(): void {
    if (this.persistenceKey) {
      this.unregisterStatePersistence();
    }
    this.tree.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }
}
