import { InjectionToken } from '@angular/core';
import { GridDefinition } from './model/grid-definition.model';
import { AbstractSqtmDragAndDropEvent } from '../drag-and-drop/events';

export const TEST_CASE_TREE_PICKER_ID = 'test-case-tree-picker';
export const TEST_CASE_TREE_DATASET_DUPLICATION_PICKER_ID =
  'test-case-tree-dataset-duplication-picker';
export const TEST_CASE_TREE_FOLDER_PICKER_ID = 'test-case-tree-folder-picker';
export const REQUIREMENT_TREE_PICKER_ID = 'requirement-tree-picker';
export const HIGH_LEVEL_REQUIREMENT_CHILDREN_TREE_PICKER_ID =
  'high-level-requirement-children-tree-picker';
export const HIGH_LEVEL_REQUIREMENT_TREE_PICKER_ID = 'high-level-requirement-tree-picker';
export const CAMPAIGN_TREE_PICKER_ID = 'campaign-tree-picker';
export const ITERATION_TREE_PICKER_ID = 'iteration-tree-picker';
export const CAMPAIGN_LIMITED_TREE_PICKER_ID = 'campaign-limited-tree-picker';
export const RESTRICTED_CAMPAIGN_TREE_PICKER_ID = 'restricted-campaign-tree-picker';

export function isDndDataFromTestCaseTreePicker(dndEvent: AbstractSqtmDragAndDropEvent) {
  return (
    dndEvent &&
    dndEvent.dragAndDropData &&
    dndEvent.dragAndDropData.origin === TEST_CASE_TREE_PICKER_ID
  );
}

export function isDndDataFromRequirementTreePicker(dndEvent: AbstractSqtmDragAndDropEvent) {
  return (
    dndEvent &&
    dndEvent.dragAndDropData &&
    dndEvent.dragAndDropData.origin === REQUIREMENT_TREE_PICKER_ID
  );
}

export function isDndDataFromRequirementMainTree(dndEvent: AbstractSqtmDragAndDropEvent) {
  return (
    dndEvent &&
    dndEvent.dragAndDropData &&
    dndEvent.dragAndDropData.origin === 'requirement-workspace-main-tree'
  );
}

export const TEST_CASE_TREE_PICKER_CONFIG = new InjectionToken<GridDefinition>(
  'Grid config instance for a test case tree picker',
);
export const TEST_CASE_TREE_FOLDER_PICKER_CONFIG = new InjectionToken<GridDefinition>(
  'Grid config instance for a test case tree picker',
);
export const TEST_CASE_TREE_DATASET_DUPLICATION_PICKER_CONFIG = new InjectionToken<GridDefinition>(
  'Grid config instance for a test case tree dataset duplication picker',
);
export const REQUIREMENT_TREE_PICKER_CONFIG = new InjectionToken<GridDefinition>(
  'Grid config instance for a requirement tree picker',
);
export const MONO_HIGH_LEVEL_REQUIREMENT_TREE_PICKER_CONFIG = new InjectionToken<GridDefinition>(
  'Grid config instance for a high level requirement tree picker. Unique selection only',
);
export const CAMPAIGN_TREE_PICKER_CONFIG = new InjectionToken<GridDefinition>(
  'Grid config instance for a campaign workspace tree picker',
);
export const CAMPAIGN_LIMITED_TREE_PICKER_CONFIG = new InjectionToken<GridDefinition>(
  'Grid config instance for a campaign workspace tree picker limited to campaign',
);

export const ITERATION_TREE_PICKER_CONFIG = new InjectionToken<GridDefinition>(
  'Grid instance for a campaign workspace iteration tree picker',
);
export const RESTRICTED_CAMPAIGN_TREE_PICKER_CONFIG = new InjectionToken<GridDefinition>(
  'Grid config instance for a campaign workspace restricted campaign tree picker',
);
