import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Subject } from 'rxjs';
import { map, take, takeUntil } from 'rxjs/operators';
import { ReferentialDataService } from '../../../../../core/referential/services/referential-data.service';
import { DataRow } from '../../../model/data-row.model';
import { Identifier } from '../../../../../model/entity.model';
import { ListItem } from '../../../../workspace-common/components/forms/grouped-multi-list/grouped-multi-list.component';
import { CustomScopeKind } from '../../../model/state/definition.state';
import { ProjectReference } from '../../../model/data-row.type';
import { EntityScope, Scope } from '../../../../../model/filter/filter.model';

import { SquashTmDataRowType } from '../../../../../model/grids/data-row.model';
import { CampaignPickerComponent } from '../../pickers/campaign-picker/campaign-picker.component';

const UNSELECTABLE_ROW_TYPES: SquashTmDataRowType[] = [
  SquashTmDataRowType.Sprint,
  SquashTmDataRowType.SprintGroup,
];
@Component({
  selector: 'sqtm-core-project-scope',
  templateUrl: './project-scope.component.html',
  styleUrls: ['./project-scope.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectScopeComponent implements OnInit, OnDestroy {
  valueChanged = new Subject<Scope>();

  close = new Subject<void>();

  private unsub$ = new Subject<void>();

  items: ListItem[];

  private scope: Scope;

  customScopeKind: CustomScopeKind;

  customScopeEntities: EntityScope[] = [];

  private _picker: CampaignPickerComponent;

  @ViewChild('campaignPicker')
  set picker(pickerElement: CampaignPickerComponent) {
    this._picker = pickerElement;

    if (pickerElement?.tree != null) {
      pickerElement.tree.selectedRows$
        .pipe(takeUntil(this.unsub$))
        .subscribe((rows) => this.filterUnselectableRowTypes(rows));
    }
  }

  get picker(): CampaignPickerComponent {
    return this._picker;
  }

  private filterUnselectableRowTypes(dataRows: DataRow[]): void {
    dataRows.forEach((row) => {
      if (UNSELECTABLE_ROW_TYPES.includes(row.type)) {
        this.picker.tree.toggleRowSelection(row.id);
      }
    });
  }

  get nzTabSelected(): number {
    if (this.scope && this.scope.kind === 'custom') {
      return 1;
    }
    return 0;
  }

  get initialSelectedEntities(): Identifier[] {
    if (this.scope && this.scope.kind === 'custom') {
      return this.scope.value.map((e) => e.id);
    }
    return [];
  }

  constructor(
    protected cdRef: ChangeDetectorRef,
    private referentialDataService: ReferentialDataService,
  ) {}

  ngOnInit() {}

  changeSelection(item: ListItem) {
    let nextValue: EntityScope[] = this.scope.kind === 'project' ? [...this.scope.value] : [];
    if (item.selected) {
      const projectId = ProjectReference.fromString(item.id.toString()).id;
      nextValue = nextValue.concat({ id: item.id.toString(), label: item.label, projectId });
    } else {
      const index = nextValue.findIndex((v) => v.id === item.id);
      if (index >= 0) {
        nextValue.splice(index, 1);
      }
    }

    const updatedScope: Scope = { ...this.scope, kind: 'project', value: nextValue };
    this.valueChanged.next(updatedScope);
    this.setScope(updatedScope);
  }

  setScope(scope: Scope) {
    this.referentialDataService.projectDatas$
      .pipe(
        take(1),
        map((projectDataMap) => Object.values(projectDataMap)),
      )
      .subscribe((projectDatas) => {
        const scopeValue = scope.value;
        const ids = scopeValue.map((value) => value.id);
        const initIds = scope.initialValue.map((value) => value.id);
        this.items = projectDatas
          .sort((pa, pb) => {
            return pa.name.localeCompare(pb.name);
          })
          .filter((projectData) =>
            initIds.includes(new ProjectReference(projectData.id).asString()),
          )
          .map((projectData) => {
            const ref = new ProjectReference(projectData.id).asString();
            return {
              id: ref,
              selected: ids.includes(ref),
              label: projectData.name,
            };
          });
        this.scope = scope;
        this.cdRef.detectChanges();
      });
  }

  handleConfirm() {
    this.valueChanged.next({ ...this.scope, kind: 'custom', value: this.customScopeEntities });
    this.close.next();
  }

  changeTreeSelection(dataRows: DataRow[]) {
    this.customScopeEntities = dataRows.map((row) => ({
      id: row.id as string,
      label: row.data['NAME'],
      projectId: row.projectId,
    }));
  }

  handleCancel() {
    this.close.next();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
