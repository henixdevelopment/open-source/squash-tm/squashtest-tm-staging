import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { RowRenderer } from '../RowRenderer';
import { GridDisplay, ViewportDisplay } from '../../../model/grid-display.model';
import { GridNode } from '../../../model/grid-node.model';

@Component({
  selector: 'sqtm-core-drag-and-drop-placeholder',
  template: `
    <div
      class="full-width full-height drag-and-drop-placeholder"
      [attr.data-test-element-id]="'grid-dnd-placeholder'"
      [attr.data-test-index]="gridNode?.index"
    ></div>
  `,
  styleUrls: ['./drag-and-drop-placeholder.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DragAndDropPlaceholderComponent implements OnInit, RowRenderer {
  gridDisplay: GridDisplay;
  gridNode: GridNode;
  viewport: ViewportDisplay;

  constructor(public cdRef: ChangeDetectorRef) {}

  ngOnInit() {}
}
