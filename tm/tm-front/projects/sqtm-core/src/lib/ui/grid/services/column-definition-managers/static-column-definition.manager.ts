import { Observable, of } from 'rxjs';
import { ColumnDefinition, SortedColumn } from '../../model/column-definition.model';
import { ColumnDefinitionManager } from './column-definition-manager';
import { GridState } from '../../model/state/grid.state';
import { AbstractColumnDefinitionManager } from './abstract-column-definition.manager';
import { BindableEntity } from '../../../../model/bindable-entity.model';
import { Identifier } from '../../../../model/entity.model';
import { GridViewportName } from '../../../../model/grids/grid-viewport-name';

export class StaticColumnDefinitionManager
  extends AbstractColumnDefinitionManager
  implements ColumnDefinitionManager
{
  constructor() {
    super();
  }

  public initializeColumns(
    columnDefinitions: ColumnDefinition[],
    state: GridState,
    initialSortedColumns: SortedColumn[],
    bindableEntity: BindableEntity,
    shouldResetSorts: boolean,
  ): Observable<GridState> {
    const columnState = state.columnState;
    const mainViewport = { ...columnState.mainViewport };
    const leftViewport = { ...columnState.leftViewport };
    const rightViewport = { ...columnState.rightViewport };

    mainViewport.order = columnDefinitions
      .filter((col) => col.viewportName === 'mainViewport')
      .map((colDef) => colDef.id);
    leftViewport.order = columnDefinitions
      .filter((col) => col.viewportName === 'leftViewport')
      .map((colDef) => colDef.id);
    rightViewport.order = columnDefinitions
      .filter((col) => col.viewportName === 'rightViewport')
      .map((colDef) => colDef.id);
    columnState.mainViewport = mainViewport;
    columnState.leftViewport = leftViewport;
    columnState.rightViewport = rightViewport;
    columnState.sortedColumns = initialSortedColumns;
    columnState.shouldResetSorts = shouldResetSorts;
    return of({ ...state, columnState: this.adapter.setAll(columnDefinitions, columnState) });
  }

  public addColumns(
    _columnDefinitions: ColumnDefinition[],
    _viewport: GridViewportName,
    _state: GridState,
  ): Observable<GridState> {
    throw new Error(
      'Programmatic error. You cannot add columns to a StaticColumnDefinitionManager',
    );
  }

  addColumnAtIndex(
    newColumns: ColumnDefinition[],
    state: GridState,
    index?: number,
  ): Observable<GridState> {
    const columnState = { ...state.columnState };
    const mainViewPort = { ...columnState.mainViewport };

    const columnIds = Object.values(columnState.entities).map((columnDef) => columnDef.id);
    const newIds = newColumns.map((column) => column.id);

    const filteredIds = newIds.filter((id) => !columnIds.includes(id));
    const filteredColumns = newColumns.filter((column) => filteredIds.includes(column.id));
    if (index != null) {
      mainViewPort.order.splice(index, 0, ...filteredIds);
    } else {
      mainViewPort.order = mainViewPort.order.concat(filteredIds);
    }

    columnState.mainViewport = mainViewPort;
    return of({ ...state, columnState: this.adapter.addMany(filteredColumns, columnState) });
  }

  removeColumn(columnIds: Identifier[], state: GridState): Observable<GridState> {
    const columnState = { ...state.columnState };
    const mainViewPort = { ...columnState.mainViewport };
    mainViewPort.order = mainViewPort.order.filter((order) => !columnIds.includes(order));

    columnState.mainViewport = mainViewPort;
    const ids = columnIds.map((id) => id.toString());

    return of({ ...state, columnState: this.adapter.removeMany(ids, columnState) });
  }
}
