import { Identifier } from '../../../../model/entity.model';
import { DataRow } from '../data-row.model';

export interface InsertRowAction {
  dataRows: DataRow[];
  targetRow: Identifier;
  index?: number;
}
