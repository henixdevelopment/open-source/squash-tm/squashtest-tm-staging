import {
  AfterViewInit,
  Directive,
  ElementRef,
  Host,
  Input,
  NgZone,
  OnDestroy,
  Renderer2,
} from '@angular/core';
import { filter, takeUntil } from 'rxjs/operators';
import { GridService } from '../services/grid.service';
import { Subject } from 'rxjs';
import { Identifier } from '../../../model/entity.model';

@Directive({
  selector: '[sqtmCoreGridHoverRow]',
})
export class GridHoverRowDirective implements AfterViewInit, OnDestroy {
  @Input('sqtmCoreGridHoverRow')
  rowId: Identifier;

  private unsub$ = new Subject<void>();

  constructor(
    private grid: GridService,
    private zone: NgZone,
    private renderer: Renderer2,
    @Host() private host: ElementRef,
  ) {}

  ngAfterViewInit(): void {
    this.zone.runOutsideAngular(() => {
      this.grid.enterRow$.pipe(takeUntil(this.unsub$)).subscribe((id) => {
        if (id === this.rowId) {
          this.renderer.addClass(this.host.nativeElement, 'sqtm-core-hovered-row');
        } else {
          this.unmarkAsHovered();
        }
      });

      this.grid.leaveRow$
        .pipe(
          takeUntil(this.unsub$),
          filter((id) => id === this.rowId),
        )
        .subscribe(() => {
          this.unmarkAsHovered();
        });
    });
  }

  private unmarkAsHovered() {
    this.renderer.removeClass(this.host.nativeElement, 'sqtm-core-hovered-row');
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
