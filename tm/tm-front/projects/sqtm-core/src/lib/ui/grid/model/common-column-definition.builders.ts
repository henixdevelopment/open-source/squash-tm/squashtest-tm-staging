import { BooleanCellRendererComponent } from '../components/cell-renderers/boolean-cell-renderer/boolean-cell-renderer.component';
import {
  ConcatenatedDatesCellRendererComponent,
  DateCellRendererComponent,
  DateTimeCellRendererComponent,
} from '../components/cell-renderers/date-cell-renderer/date-cell-renderer.component';
import {
  buildLevelEnumKeySort,
  I18nEnum,
  LevelEnum,
  TestCaseAutomatable,
  TestCaseStatus,
  TestCaseWeight,
} from '../../../model/level-enums/level-enum';
import {
  isTestCaseEditable,
  isTestCaseImportanceEditable,
} from '../../cell-renderer-common/editable-functions';
import { EditableDateCellRendererComponent } from '../components/cell-renderers/editable-date-cell-renderer/editable-date-cell-renderer.component';
import {
  NullableNumberCellRendererComponent,
  NumericCellRendererComponent,
} from '../components/cell-renderers/numeric-cell-renderer/numeric-cell-renderer.component';
import { NumericDurationCellRendererComponent } from '../components/cell-renderers/numeric-duration-cell-renderer/numeric-duration-cell-renderer.component';
import { EditableTextCellRendererComponent } from '../components/cell-renderers/editable-text-cell-renderer/editable-text-cell-renderer.component';
import { TextCellerWithToolTipRenderComponent } from '../components/cell-renderers/text-cell-with-tool-tip-renderer/text-celler-with-tool-tip-render.component';
import { IndexCellRendererComponent } from '../components/cell-renderers/index-cell-renderer/index-cell-renderer.component';
import { Fixed } from './column-definition.model';
import { EditableRichTextRendererComponent } from '../components/cell-renderers/editable-rich-text-renderer/editable-rich-text-renderer.component';
import { Type } from '@angular/core';
import { TextCellRendererComponent } from '../components/cell-renderers/text-cell-renderer/text-cell-renderer.component';
import { CheckBoxCellRendererComponent } from '../components/cell-renderers/check-box-cell-renderer/check-box-cell-renderer.component';
import { EditableNumericCellRendererComponent } from '../components/cell-renderers/editable-numeric-cell-renderer/editable-numeric-cell-renderer.component';
import { RichTextRendererComponent } from '../components/cell-renderers/rich-text-renderer/rich-text-renderer.component';
import { RadioCellRendererComponent } from '../components/cell-renderers/radio-cell-renderer/radio-cell-renderer.component';
import { SelectableNumericCellRendererComponent } from '../components/cell-renderers/selectable-numeric-cell-renderer/selectable-numeric-cell-renderer.component';
import { ColumnDefinitionBuilder } from './column-definition.builder';
import {
  BooleanValueRenderer,
  DateExportValueRenderer,
  DateTimeExportValueRenderer,
} from '../../grid-export/value-renderer/value-renderer';
import {
  GridColumnId,
  StaticOrDynamicColumnId,
} from '../../../shared/constants/grid/grid-column-id';
import { ConcatenatedStatusesCellRendererComponent } from '../components/cell-renderers/concatenated-statuses-cell-renderer/concatenated-statuses-cell-renderer.component';
import { SystemSyncStatusCellComponent } from '../components/cell-renderers/system-sync-status-cell/system-sync-status-cell.component';
import { SynchronizedRequirementsRatioCellComponent } from '../components/cell-renderers/synchronized-requirements-ratio-cell.component/synchronized-requirements-ratio-cell.component';
import { SynchronizedSprintTicketsRatioCellComponent } from '../components/cell-renderers/synchronized-sprint-tickets-ratio-cell.component/synchronized-sprint-tickets-ratio-cell.component';

export function column(id: StaticOrDynamicColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id);
}

export function textColumn(id: StaticOrDynamicColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(TextCellRendererComponent);
}

export function textCellWithToolTipColumn(
  id: StaticOrDynamicColumnId,
  toolTipText: string,
): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withToolTipText(toolTipText)
    .withRenderer(TextCellerWithToolTipRenderComponent);
}

export function centredTextColumn(id: StaticOrDynamicColumnId): ColumnDefinitionBuilder {
  return textColumn(id).withHeaderPosition('center').withContentPosition('center');
}

export function editableTextColumn(id: StaticOrDynamicColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(EditableTextCellRendererComponent);
}

export function checkboxColumn(id: StaticOrDynamicColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(CheckBoxCellRendererComponent);
}

export function booleanColumn(id: StaticOrDynamicColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(BooleanCellRendererComponent)
    .withExportValueRenderer(BooleanValueRenderer);
}

export function editableDateColumn(id: StaticOrDynamicColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(EditableDateCellRendererComponent);
}

export function dateColumn(id: StaticOrDynamicColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(DateCellRendererComponent)
    .withExportValueRenderer(DateExportValueRenderer);
}

export function dateTimeColumn(id: StaticOrDynamicColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(DateTimeCellRendererComponent)
    .withExportValueRenderer(DateTimeExportValueRenderer);
}

export function editableNumericColumn(id: StaticOrDynamicColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(EditableNumericCellRendererComponent);
}

export function numericColumn(id: StaticOrDynamicColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(NumericCellRendererComponent)
    .withHeaderPosition('center');
}

export function nullableNumericColumn(id: StaticOrDynamicColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(NullableNumberCellRendererComponent)
    .withHeaderPosition('center');
}

export function numericDurationColumn(id: StaticOrDynamicColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(NumericDurationCellRendererComponent)
    .withHeaderPosition('left');
}

export function concatenatedDatesColumn(id: StaticOrDynamicColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(ConcatenatedDatesCellRendererComponent)
    .withExportValueRenderer(DateTimeExportValueRenderer)
    .disableSort();
}

export function concatenatedStatusesColumn(id: StaticOrDynamicColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(ConcatenatedStatusesCellRendererComponent)
    .disableSort();
}

export function indexColumn(): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(GridColumnId['#'])
    .withHeaderPosition('center')
    .withRenderer(IndexCellRendererComponent)
    .changeWidthCalculationStrategy(new Fixed(50))
    .disableSort();
}

export function editableRichTextColumn(id: StaticOrDynamicColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(EditableRichTextRendererComponent);
}

export function richTextColumn(id: StaticOrDynamicColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(RichTextRendererComponent);
}

export function selectRowColumn(): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(GridColumnId['select-row-column'])
    .changeWidthCalculationStrategy(new Fixed(40))
    .disableSort()
    .enableForceRenderOnNoValue()
    .withRenderer(CheckBoxCellRendererComponent);
}

export function singleSelectRowColumn(): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(GridColumnId['single-select-row-column'])
    .changeWidthCalculationStrategy(new Fixed(40))
    .disableSort()
    .enableForceRenderOnNoValue()
    .disableHeader()
    .withRenderer(RadioCellRendererComponent);
}

export function deleteColumn(
  renderer: Type<any>,
  id = GridColumnId.delete,
  label = '',
): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(renderer)
    .withLabel(label)
    .disableSort()
    .changeWidthCalculationStrategy(new Fixed(40));
}

export function editColumn(
  renderer: Type<any>,
  id = GridColumnId.edit,
  label = '',
): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(renderer)
    .withLabel(label)
    .disableSort()
    .changeWidthCalculationStrategy(new Fixed(40));
}

export function iconActionColumn(
  renderer: Type<any>,
  id: StaticOrDynamicColumnId,
  iconName: string,
): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(renderer)
    .withIconName(iconName)
    .disableHeader()
    .disableSort()
    .changeWidthCalculationStrategy(new Fixed(40));
}

export function copyDatasetColumn(renderer: Type<any>): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(GridColumnId.copy)
    .withRenderer(renderer)
    .withLabel('')
    .disableSort()
    .changeWidthCalculationStrategy(new Fixed(40));
}

export function testCaseImportanceColumn(id: StaticOrDynamicColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withEnumRenderer(TestCaseWeight)
    .isEditable(isTestCaseImportanceEditable)
    .withHeaderPosition('center');
}

export function testCaseStatusColumn(id: StaticOrDynamicColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withEnumRenderer(TestCaseStatus)
    .isEditable(isTestCaseEditable)
    .withHeaderPosition('center');
}

export function testCaseAutomatableColumn(id: StaticOrDynamicColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withEnumRenderer(TestCaseAutomatable)
    .isEditable(isTestCaseEditable)
    .withHeaderPosition('center');
}

export function i18nEnumColumn(
  id: StaticOrDynamicColumnId,
  i18nEnum: I18nEnum<any>,
): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withEnumRenderer(i18nEnum).withHeaderPosition('center');
}

export function levelEnumColumn(
  id: StaticOrDynamicColumnId,
  levelEnum: LevelEnum<any>,
): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withEnumRenderer(levelEnum)
    .withHeaderPosition('center')
    .withSortFunction(buildLevelEnumKeySort(levelEnum));
}

export function selectableNumericColumn(id: StaticOrDynamicColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(SelectableNumericCellRendererComponent);
}

export function testSprintReqVersionColumn(
  renderer: Type<any>,
  label = '',
): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(GridColumnId.testSprintReqVersion)
    .withRenderer(renderer)
    .withLabel(label)
    .disableSort()
    .changeWidthCalculationStrategy(new Fixed(80));
}

export function syncStatusColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(SystemSyncStatusCellComponent);
}

export function synchronizedRequirementsRatioColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(SynchronizedRequirementsRatioCellComponent);
}

export function synchronizedSprintTicketsRatioColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(SynchronizedSprintTicketsRatioCellComponent);
}
