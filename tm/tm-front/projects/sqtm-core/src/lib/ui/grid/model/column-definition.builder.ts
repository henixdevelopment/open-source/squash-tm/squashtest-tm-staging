import {
  ColumnDefinition,
  ColumnDefinitionOption,
  ContentDriven,
  I18nEnumOptions,
  Limited,
  WidthCalculationStrategy,
} from './column-definition.model';
import { TextCellRendererComponent } from '../components/cell-renderers/text-cell-renderer/text-cell-renderer.component';
import { I18nEnum } from '../../../model/level-enums/level-enum';
import { Type } from '@angular/core';
import { GridHeaderRendererComponent } from '../components/header-renderers/grid-header-renderer/grid-header-renderer.component';
import { Identifier } from '../../../model/entity.model';
import { DataRow, DataRowSortFunction } from './data-row.model';
import { ResearchColumnPrototype } from '../../filters/state/filter.state';
import { EnumEditableCellComponent } from '../../cell-renderer-common/enum-editable-cell/enum-editable-cell.component';
import { ColumnDisplay } from './column-display.model';
import {
  PassThroughValueRenderer,
  ValueRenderer,
} from '../../grid-export/value-renderer/value-renderer';
import { GridViewportName } from '../../../model/grids/grid-viewport-name';
import { StaticOrDynamicColumnId } from '../../../shared/constants/grid/grid-column-id';
import { GridId } from '../../../shared/constants/grid/grid-id';
import { LocalPersistenceService } from '../../../core/services/local-persistence.service';

export class ColumnDefinitionBuilder {
  gridId: GridId | string;
  private widthCalculationStrategy: WidthCalculationStrategy = new Limited(100);
  private i18nKey: string;
  private label: string;
  private toolTipText: string;
  private sortable = true;
  private draggable = true;
  private associateToFilter: Identifier;
  private resizable = true;
  private show = true;
  private sortIndex = 0;
  private cellRenderer: Type<any> = TextCellRendererComponent;
  private forceRenderOnNoValue = false;
  private headerRenderer: Type<any> = GridHeaderRendererComponent;
  private iconName: string;
  private iconTheme = 'outline';
  private cufId: Identifier;
  private titleI18nKey: string;
  private sortFunction: DataRowSortFunction;
  private headerPosition: POSITION = 'left';
  private contentPosition: POSITION = 'left';
  private editable: boolean | ((columnDisplay: ColumnDisplay, row: DataRow) => boolean);
  private columnPrototype: ResearchColumnPrototype;
  private showHeader = true;
  private viewportName: GridViewportName = 'mainViewport';
  private options?: ColumnDefinitionOption;
  private exportValueRenderer: Type<ValueRenderer> = PassThroughValueRenderer;

  constructor(private id: StaticOrDynamicColumnId) {}

  static column(id: StaticOrDynamicColumnId) {
    return new ColumnDefinitionBuilder(id);
  }

  changeWidthCalculationStrategy(strategy: WidthCalculationStrategy): ColumnDefinitionBuilder {
    this.widthCalculationStrategy = strategy;
    return this;
  }

  withI18nKey(i18nKey: string): ColumnDefinitionBuilder {
    this.i18nKey = i18nKey;
    return this;
  }

  disableSort() {
    this.sortable = false;
    return this;
  }

  withHeaderRenderer(renderer: Type<any>) {
    this.headerRenderer = renderer;
    return this;
  }

  withIconName(iconName: string) {
    this.iconName = iconName;
    return this;
  }

  withIconTheme(iconTheme: string) {
    this.iconTheme = iconTheme;
    return this;
  }

  enableDnd() {
    this.draggable = true;
    return this;
  }

  enableForceRenderOnNoValue() {
    this.forceRenderOnNoValue = true;
    return this;
  }

  withRenderer(cellRendererType: Type<any>) {
    this.cellRenderer = cellRendererType;
    return this;
  }

  withEnumRenderer(i18nEnum: I18nEnum<any>, showIcon = true, showLabel = false) {
    this.cellRenderer = EnumEditableCellComponent;
    const options: I18nEnumOptions = { kind: 'i18nEnum', i18nEnum, showIcon, showLabel };
    this.options = options;
    return this;
  }

  withLabel(label: string) {
    this.label = label;
    return this;
  }

  withToolTipText(toolTipText: string) {
    this.toolTipText = toolTipText;
    return this;
  }

  defaultAutoResize() {
    this.widthCalculationStrategy = new ContentDriven();
    this.resizable = false;
    return this;
  }

  withCufId(cufId: Identifier) {
    this.cufId = cufId;
    return this;
  }

  withTitleI18nKey(i18nKey: string) {
    this.titleI18nKey = i18nKey;
    return this;
  }

  withSortFunction(sortFunction: DataRowSortFunction) {
    this.sortFunction = sortFunction;
    return this;
  }

  withHeaderPosition(headerPosition: POSITION) {
    this.headerPosition = headerPosition;
    return this;
  }

  withContentPosition(contentPosition: POSITION) {
    this.contentPosition = contentPosition;
    return this;
  }

  withVisibility(visible: boolean) {
    this.show = visible;
    return this;
  }

  withColumnPrototype(columnPrototype: ResearchColumnPrototype) {
    this.columnPrototype = columnPrototype;
    return this;
  }

  withAssociatedFilter(filterId?: Identifier) {
    this.associateToFilter = filterId || this.id;
    return this;
  }

  isEditable(editable: boolean | ((columnDisplay: ColumnDisplay, row: DataRow) => boolean)) {
    this.editable = editable;
    return this;
  }

  disableHeader() {
    this.showHeader = false;
    return this;
  }

  withViewport(viewportName: GridViewportName) {
    this.viewportName = viewportName;
    return this;
  }

  withOptions(options: ColumnDefinitionOption) {
    this.options = options;
    return this;
  }

  withExportValueRenderer(exportValueRenderer: Type<ValueRenderer>): ColumnDefinitionBuilder {
    this.exportValueRenderer = exportValueRenderer;
    return this;
  }

  build(
    gridId: string,
    shouldPersistWidth?: boolean,
    localPersistenceService?: LocalPersistenceService,
  ): ColumnDefinition {
    if (this.widthCalculationStrategy != null) {
      this.widthCalculationStrategy.gridColumnId = this.id;
      this.widthCalculationStrategy.gridId = gridId;
      this.widthCalculationStrategy.shouldPersistWidth = shouldPersistWidth;
      this.widthCalculationStrategy.localPersistenceService = localPersistenceService;

      if (this.widthCalculationStrategy instanceof Limited) {
        this.widthCalculationStrategy.initializeWidth();
      }
    }

    return { ...(this as object as ColumnDefinition) };
  }
}

export type POSITION = 'left' | 'center' | 'right';
