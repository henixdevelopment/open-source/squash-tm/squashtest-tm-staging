import { ChangeDetectionStrategy, Component, OnInit, ViewContainerRef } from '@angular/core';
import { GridService } from '../../services/grid.service';
import { DialogService } from '../../../dialog/services/dialog.service';

@Component({
  selector: 'sqtm-core-side-bar-navigator',
  template: `
    <div class="full-height flex-column" style="width: 20px">
      <div class="m-t-100 __hover_pointer">
        <span><i nz-icon nzType="filter" nzTheme="fill"></i></span>
      </div>
    </div>
  `,
  styleUrls: ['./side-bar-navigator.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SideBarNavigatorComponent implements OnInit {
  constructor(
    private grid: GridService,
    private dialogService: DialogService,
    private vcr: ViewContainerRef,
  ) {}

  ngOnInit() {}
}
