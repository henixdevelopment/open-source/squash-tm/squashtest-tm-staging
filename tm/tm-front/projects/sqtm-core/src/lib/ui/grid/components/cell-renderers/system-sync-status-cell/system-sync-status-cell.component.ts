import { AbstractCellRendererComponent } from '../abstract-cell-renderer/abstract-cell-renderer.component';
import { GridService } from '../../../services/grid.service';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SynchronizationStatus } from '../../../../../model/system/system-view.model';
import { GridColumnId } from '../../../../../shared/constants/grid/grid-column-id';

@Component({
  selector: 'sqtm-app-system-sync-status-cell',
  template: `
    @if (row) {
      <div
        class="status-wrapper"
        nz-tooltip
        [nzTooltipTitle]="getTooltipText(row.data[columnDisplay.id])"
      >
        <div class="status" [ngClass]="statusColor"></div>
      </div>
    }
  `,
  styleUrls: ['./system-sync-status-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SystemSyncStatusCellComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdr: ChangeDetectorRef,
    private translateService: TranslateService,
  ) {
    super(grid, cdr);
  }

  get statusColor(): string[] {
    const classes = ['status-indicator'];
    const status: SynchronizationStatus = this.row.data[GridColumnId.status];

    switch (status) {
      case SynchronizationStatus.FAILURE:
        classes.push('failure');
        break;
      case SynchronizationStatus.RUNNING:
        classes.push('running');
        break;
      case SynchronizationStatus.SUCCESS:
        classes.push('success');
        break;
      case SynchronizationStatus.NEVER_EXECUTED:
        classes.push('never-executed');
        break;
    }

    return classes;
  }

  getTooltipText(status: SynchronizationStatus) {
    return this.translateService.instant(
      `sqtm-core.entity.remote-sync.synchronization-status.${status}`,
    );
  }
}
