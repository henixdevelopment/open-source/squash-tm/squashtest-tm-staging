import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TextCellRendererComponent } from './text-cell-renderer.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { GridDefinition } from '../../../model/grid-definition.model';
import { RestService } from '../../../../../core/services/rest.service';
import { GridService } from '../../../services/grid.service';
import { gridServiceFactory } from '../../../grid.service.provider';
import { getBasicGridDisplay } from '../../../grid-testing/grid-testing-utils';
import { grid } from '../../../model/grid-builders';
import { ReferentialDataService } from '../../../../../core/referential/services/referential-data.service';
import { DialogService } from '../../../../dialog/services/dialog.service';
import { TestCaseLibrary } from '../../../model/data-row.type';
import { Fixed } from '../../../model/column-definition.model';

describe('TextCellRendererComponent', () => {
  let component: TextCellRendererComponent;
  let fixture: ComponentFixture<TextCellRendererComponent>;
  const gridConfig = grid('grid-test').build();
  const restService = {};
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TextCellRendererComponent],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig,
        },
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService],
        },
        { provide: DialogService, useValue: DialogService },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(TextCellRendererComponent);
        component = fixture.componentInstance;
        component.gridDisplay = getBasicGridDisplay();
        component.columnDisplay = {
          id: 'column-1',
          show: true,
          widthCalculationStrategy: new Fixed(200),
          headerPosition: 'left',
          contentPosition: 'left',
          showHeader: true,
          viewportName: 'mainViewport',
        };
        component.row = {
          ...new TestCaseLibrary(),
          id: 'tcln-1',
          data: { id: 'tcln-1' },
        };
        fixture.detectChanges();
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
