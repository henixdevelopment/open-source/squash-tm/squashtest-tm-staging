import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HighLevelRequirementChildrenPickerComponent } from './high-level-requirement-children-picker.component';
import { TestingUtilsModule } from '../../../../testing-utils/testing-utils.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { OverlayModule } from '@angular/cdk/overlay';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('HighLevelRequirementChildrenPickerComponent', () => {
  let component: HighLevelRequirementChildrenPickerComponent;
  let fixture: ComponentFixture<HighLevelRequirementChildrenPickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TestingUtilsModule, HttpClientTestingModule, OverlayModule, RouterTestingModule],
      declarations: [HighLevelRequirementChildrenPickerComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HighLevelRequirementChildrenPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
