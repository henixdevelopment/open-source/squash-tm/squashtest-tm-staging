import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DefaultTreeDraggedContentComponent } from './default-tree-dragged-content.component';
import { DRAG_AND_DROP_DATA } from '../../../../drag-and-drop/constants';

describe('DefaultTreeDraggedContentComponent', () => {
  let component: DefaultTreeDraggedContentComponent;
  let fixture: ComponentFixture<DefaultTreeDraggedContentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: DRAG_AND_DROP_DATA, useValue: { data: { dataRows: [] } } }],
      declarations: [DefaultTreeDraggedContentComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultTreeDraggedContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
