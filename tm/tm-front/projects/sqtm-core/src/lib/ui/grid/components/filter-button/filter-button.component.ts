import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ComponentRef,
  ElementRef,
  Input,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { ColumnWithFilter } from '../../model/column-display.model';
import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { AbstractFilterWidget } from '../../../filters/components/abstract-filter-widget';
import { GridService } from '../../services/grid.service';
import { Subject } from 'rxjs';
import { GridDisplay } from '../../model/grid-display.model';
import { ComponentPortal } from '@angular/cdk/portal';
import { GridFilterUtils } from '../../services/filter-managers/grid-filter-utils';
import { Identifier } from '../../../../model/entity.model';
import { FilteringChange } from '../../../filters/state/filter.state';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'sqtm-core-filter-button',
  templateUrl: './filter-button.component.html',
  styleUrls: ['./filter-button.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterButtonComponent {
  @Input()
  gridDisplay: GridDisplay;

  protected _columnDisplay: ColumnWithFilter;

  public get columnDisplay(): ColumnWithFilter {
    return this._columnDisplay;
  }
  @Input()
  public set columnDisplay(value: ColumnWithFilter) {
    this._columnDisplay = value;
    if (this.componentRef) {
      this.componentRef.instance.setFilter(value.filter, this.gridDisplay.scope);
    }
  }

  @ViewChild('resizeHandler', { read: ElementRef })
  resizeHandler: ElementRef;

  @ViewChild('filterButton', { read: ElementRef })
  private filterButton: ElementRef;

  private overlayRef: OverlayRef;
  private componentRef: ComponentRef<AbstractFilterWidget>;
  private unsub$ = new Subject<void>();

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    private overlay: Overlay,
    private vcr: ViewContainerRef,
  ) {}

  showFilterWidget($event: MouseEvent) {
    $event.stopPropagation();
    const positionStrategy = this.overlay
      .position()
      .flexibleConnectedTo(this.filterButton)
      .withPositions([
        { originX: 'center', overlayX: 'center', originY: 'bottom', overlayY: 'top', offsetY: 10 },
        { originX: 'end', overlayX: 'start', originY: 'center', overlayY: 'center', offsetX: 10 },
        { originX: 'center', overlayX: 'center', originY: 'top', overlayY: 'bottom', offsetY: -10 },
      ]);
    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
    };
    this.overlayRef = this.overlay.create(overlayConfig);
    const componentPortal = new ComponentPortal(this._columnDisplay.filter.widget, this.vcr);
    this.componentRef = this.overlayRef.attach(componentPortal);
    this.componentRef.instance.setFilter(this._columnDisplay.filter, this.gridDisplay.scope);
    this.componentRef.instance.filteringChanged
      .pipe(takeUntil(this.unsub$))
      .subscribe((value) => this.changeFiltering(this._columnDisplay.filter.id, value));
    this.componentRef.instance.close
      .pipe(takeUntil(this.unsub$))
      .subscribe(() => this.closeFilterWidget());
    this.overlayRef.backdropClick().subscribe(() => this.closeFilterWidget());
  }

  closeFilterWidget() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
      this.componentRef = null;
    }
  }

  private changeFiltering(filterId: Identifier, filteringChange: FilteringChange) {
    this.grid.changeFilterValue({ id: filterId, ...filteringChange });
  }

  filterIsActiveAndValid() {
    return (
      this._columnDisplay.filter && GridFilterUtils.mustIncludeFilter(this._columnDisplay.filter)
    );
  }
}
