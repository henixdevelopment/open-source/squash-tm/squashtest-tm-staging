import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TextCellerWithToolTipRenderComponent } from './text-celler-with-tool-tip-render.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { GridDefinition } from '../../../model/grid-definition.model';
import { RestService } from '../../../../../core/services/rest.service';
import { GridService } from '../../../services/grid.service';
import { gridServiceFactory } from '../../../grid.service.provider';
import { getBasicGridDisplay } from '../../../grid-testing/grid-testing-utils';
import { grid } from '../../../model/grid-builders';
import { ReferentialDataService } from '../../../../../core/referential/services/referential-data.service';
import { DialogService } from '../../../../dialog/services/dialog.service';
import { Fixed } from '../../../model/column-definition.model';
import { TestCaseLibrary } from '../../../model/data-row.type';

describe('TextCellRendererComponent', () => {
  let component: TextCellerWithToolTipRenderComponent;
  let fixture: ComponentFixture<TextCellerWithToolTipRenderComponent>;
  const gridConfig = grid('grid-test').build();
  const restService = {};
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TextCellerWithToolTipRenderComponent],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig,
        },
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService],
        },
        { provide: DialogService, useValue: DialogService },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(TextCellerWithToolTipRenderComponent);
        component = fixture.componentInstance;
        component.gridDisplay = getBasicGridDisplay();
        component.columnDisplay = {
          id: 'column-1',
          show: true,
          widthCalculationStrategy: new Fixed(200),
          headerPosition: 'left',
          contentPosition: 'left',
          showHeader: true,
          viewportName: 'mainViewport',
        };
        component.row = {
          ...new TestCaseLibrary(),
          id: 'tcln-1',
          data: { id: 'tcln-1' },
        };
        fixture.detectChanges();
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
