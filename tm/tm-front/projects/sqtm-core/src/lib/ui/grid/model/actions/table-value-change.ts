import { Identifier } from '../../../../model/entity.model';

export class TableValueChange {
  columnId: Identifier;
  value: any;
}
