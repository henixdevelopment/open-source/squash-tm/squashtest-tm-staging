import { GridNodeState } from '../grid-node.model';
import { createEntityAdapter, EntityState } from '@ngrx/entity';

export interface GridNodesState extends EntityState<GridNodeState> {}

export type GridNodesStateReadOnly = Readonly<GridNodesState>;

export function initialNodeState(): GridNodesStateReadOnly {
  return {
    ids: [],
    entities: {},
  };
}

export const gridNodeStateAdapter = createEntityAdapter<GridNodeState>({
  sortComparer: (nodeA, nodeB) => {
    return nodeA.index - nodeB.index;
  },
});
