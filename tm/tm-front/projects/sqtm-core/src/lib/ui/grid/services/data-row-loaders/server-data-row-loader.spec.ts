import { DataRow } from '../../model/data-row.model';
import { TestBed } from '@angular/core/testing';
import { DATA_ROW_LOADER } from '../../token';
import { dataRowLoaderFactory } from '../../grid.service.provider';
import { RestService } from '../../../../core/services/rest.service';
import { DataRowLoader } from './data-row-loader';
import { of } from 'rxjs';
import { GridState, initialGridState } from '../../model/state/grid.state';
import { createEntityAdapter } from '@ngrx/entity';
import { defaultSqtmConfiguration } from '../../../../core/sqtm-core.module';
import apply = Reflect.apply;
import { grid } from '../../model/grid-builders';
import { convertSqtmLiteral, convertSqtmLiterals } from '../../model/data-row.type';
import { DataRowOpenState } from '../../../../model/grids/data-row.model';
import { GridResponse } from '../../../../model/grids/grid-response.model';

describe('Server Data Row Loader', () => {
  const gridConfig = grid('grid-test').server().withServerUrl(['backend']).build();
  const restService = new RestService(null);
  restService.backendRootUrl = defaultSqtmConfiguration.backendRootUrl;
  restService.backendContextPath = '';

  const dataRowLoader: DataRowLoader = apply(dataRowLoaderFactory(gridConfig), this, [restService]);

  beforeEach(function () {
    return TestBed.configureTestingModule({
      providers: [
        {
          provide: DATA_ROW_LOADER,
          useValue: dataRowLoader,
        },
        {
          provide: RestService,
          useValue: restService,
        },
      ],
    });
  });

  it('should open row', function (done) {
    // creating output to test
    const state: GridState = initialGridState();
    state.definitionState.literalDataRowConverter = convertSqtmLiterals;
    // loading data into internal behavior subjects
    const dataRows = getRoot();
    state.dataRowState = createEntityAdapter<DataRow>().setAll(dataRows, state.dataRowState);

    spyOn(restService, 'get').and.returnValue(of(getTestCaseLibraryOneOpeningResponse()));

    dataRowLoader.openRows(['TestCaseLibrary-1'], of(state), []).subscribe((nextState) => {
      const dataRowMap = nextState.dataRowState.entities;
      expect(dataRowMap['TestCaseLibrary-1'].state).toEqual(DataRowOpenState.open);
      expect(nextState.dataRowState.ids.length).toEqual(5);
      expect(dataRowMap['TestCaseLibrary-1'].children).toEqual([
        'TestCaseFolder-1',
        'TestCaseFolder-2',
        'TestCase-12',
      ]);

      expect(dataRowMap['TestCase-12']).toBeTruthy();
      expect(dataRowMap['TestCase-12'].state).toEqual(DataRowOpenState.leaf);
      expect(dataRowMap['TestCase-12'].parentRowId).toEqual('TestCaseLibrary-1');
      done();
    });
  });

  function getRoot(): DataRow[] {
    const tcl1: DataRow = convertSqtmLiteral(
      {
        id: 'TestCaseLibrary-1',
        state: DataRowOpenState.closed,
        data: {
          name: 'Project-1',
          projectId: 1,
        },
      },
      [],
    );

    const tcl2: DataRow = convertSqtmLiteral(
      {
        id: 'TestCaseLibrary-2',
        state: DataRowOpenState.closed,
        data: {},
      },
      [],
    );

    return [tcl1, tcl2];
  }

  function getTestCaseLibraryOneOpeningResponse(): GridResponse {
    const tcl1 = {
      id: 'TestCaseLibrary-1',
      state: DataRowOpenState.open,
      parentRowId: 'TestCaseLibrary-1',
      children: ['TestCaseFolder-1', 'TestCaseFolder-2', 'TestCase-12'],
      data: {},
    };

    const tcf1 = {
      id: 'TestCaseFolder-1',
      state: DataRowOpenState.closed,
      parentRowId: 'TestCaseLibrary-1',
      children: [],
      data: {},
    };

    const tcf2 = {
      id: 'TestCaseFolder-2',
      state: DataRowOpenState.closed,
      parentRowId: 'TestCaseLibrary-1',
      children: [],
      data: {},
    };

    const tc12 = {
      id: 'TestCase-12',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseLibrary-1',
      children: [],
      data: {},
    };

    return {
      dataRows: convertSqtmLiterals([tcl1, tcf1, tcf2, tc12], []),
      count: 3,
    };
  }
});
