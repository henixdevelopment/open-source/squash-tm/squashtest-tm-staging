import { EntityState } from '@ngrx/entity';
import { Identifier } from '../../../../model/entity.model';
import { SortedColumn } from '../column-definition.model';
import { GridFilter } from './filter.state';

export interface GridConfigurationState {
  filterConfigurationState: FilterConfigurationState;
  layoutConfigurationState: LayoutConfigurationState;
  columnConfigurationState: ColumnConfigurationState;
  simplifiedColumnDisplayGridIds: string[];
}

export interface LayoutConfigurationState {
  selectedConfiguration: Identifier;
}

export interface FilterConfigurationState extends EntityState<FilterConfiguration> {
  /**
   * Default configuration as defined by developers. Can always be called to reset the grid filter config to original one.
   */
  defaultConfiguration: DefaultFilterConfiguration;
  /**
   * Configuration loaded in conf manager. This is NOT the configuration active in the grid.
   * The active configuration is spread across the whole grid state (in datarow state, filter state...)
   */
  loadedConfiguration: FilterConfiguration;
  /**
   * The last applied configuration.
   */
  selectedConfiguration: Identifier;
}

export interface FilterConfiguration {
  sortedColumns: SortedColumn[];
  filters: GridFilter[];
  kind:
    | 'sqtm-grid-default-filter-configuration'
    | 'sqtm-grid-system-filter-configuration'
    | 'sqtm-grid-user-filter-configuration';
}

export interface DefaultFilterConfiguration extends FilterConfiguration {
  kind: 'sqtm-grid-default-filter-configuration';
}

export interface SystemFilterConfiguration extends FilterConfiguration {
  id: Identifier;
  kind: 'sqtm-grid-system-filter-configuration';
}

export interface UserFilterConfiguration extends FilterConfiguration {
  id: Identifier;
  kind: 'sqtm-grid-user-filter-configuration';
}

export interface ColumnConfigurationState {
  activeColumnIds: string[];
}

export function initialUserConfigurationState(): Readonly<GridConfigurationState> {
  return {
    filterConfigurationState: {
      ids: [],
      entities: {},
      defaultConfiguration: {
        kind: 'sqtm-grid-default-filter-configuration',
        sortedColumns: [],
        filters: [],
      },
      selectedConfiguration: null,
      loadedConfiguration: null,
    },
    layoutConfigurationState: {
      selectedConfiguration: null,
    },
    columnConfigurationState: {
      activeColumnIds: [],
    },
    simplifiedColumnDisplayGridIds: [],
  };
}
