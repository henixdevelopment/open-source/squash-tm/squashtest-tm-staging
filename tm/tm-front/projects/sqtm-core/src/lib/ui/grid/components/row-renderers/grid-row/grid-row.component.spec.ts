import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { GridService } from '../../../services/grid.service';
import { gridServiceFactory } from '../../../grid.service.provider';
import { RestService } from '../../../../../core/services/rest.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { GridDefinition } from '../../../model/grid-definition.model';
import { GridRowComponent } from './grid-row.component';
import { getBasicGridDisplay } from '../../../grid-testing/grid-testing-utils';
import { getDataRowDataset } from '../../../services/data-row-loaders/data-row-dataset.spec';
import { ReferentialDataService } from '../../../../../core/referential/services/referential-data.service';
import { DataRow } from '../../../model/data-row.model';
import { TestingUtilsModule } from '../../../../testing-utils/testing-utils.module';
import { grid } from '../../../model/grid-builders';

describe('GridRowComponent', () => {
  let component: GridRowComponent;
  let fixture: ComponentFixture<GridRowComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TestingUtilsModule],
      declarations: [GridRowComponent],
      providers: [
        RestService,
        {
          provide: GridDefinition,
          useValue: grid('grid-test').build(),
        },
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService],
        },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    const gridService: GridService = TestBed.inject(GridService);
    const dataRowDataset = getDataRowDataset();
    gridService.loadInitialDataRows(dataRowDataset, dataRowDataset.length);
    fixture = TestBed.createComponent(GridRowComponent);
    component = fixture.componentInstance;
    component.gridDisplay = getBasicGridDisplay();
    component.viewport = getBasicGridDisplay().mainViewport;
    component.gridNode = {
      id: 'TestCaseLibrary-1',
      showAsDndTarget: false,
      showAsFilteredParent: false,
      index: 0,
      dataRow: {} as DataRow,
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
