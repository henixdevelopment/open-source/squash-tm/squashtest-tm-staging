import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { AbstractCellRendererComponent } from '../abstract-cell-renderer/abstract-cell-renderer.component';
import { GridService } from '../../../services/grid.service';

@Component({
  selector: 'sqtm-core-editable-rich-text-renderer',
  template: `
    @if (row.data[columnDisplay.id]) {
      <div>
        @if (!canEdit) {
          <span [innerHTML]="row.data[columnDisplay.id]"></span>
        }
        @if (canEdit) {
          <sqtm-core-editable-rich-text
            [size]="'small'"
            [value]="row.data[columnDisplay?.id]"
          ></sqtm-core-editable-rich-text>
        }
      </div>
    }
  `,
  styleUrls: ['./editable-rich-text-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditableRichTextRendererComponent extends AbstractCellRendererComponent {
  get canEdit(): boolean {
    return this.row.data['type'] !== 'C';
  }

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }
}
