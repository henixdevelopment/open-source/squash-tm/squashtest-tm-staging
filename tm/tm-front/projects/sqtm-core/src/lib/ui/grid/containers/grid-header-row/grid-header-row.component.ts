import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  NgZone,
  OnDestroy,
  QueryList,
  Renderer2,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { ColumnDisplay } from '../../model/column-display.model';
import { Subject } from 'rxjs';
import { GridDisplay } from '../../model/grid-display.model';
import { GridViewportService, RenderedGridViewport } from '../../services/grid-viewport.service';
import { takeUntil, withLatestFrom } from 'rxjs/operators';
import { gridLogger } from '../../grid.logger';
import { GridViewportName } from '../../../../model/grids/grid-viewport-name';

const LOGGER = gridLogger.compose('GridHeaderRowComponent');

@Component({
  selector: 'sqtm-core-grid-header-row',
  template: `
    @if (gridDisplay) {
      <div #row [attr.data-test-row-id]="ROW_ID" [style.height]="calculateRowHeight(gridDisplay)">
        @for (
          columnDisplay of gridDisplay[viewportName].columnDisplays;
          track trackByFn($index, columnDisplay)
        ) {
          @if (columnDisplay.show) {
            <div
              #headers
              [attr.data-test-cell-id]="columnDisplay.id"
              [attr.data-column-id]="columnDisplay.id"
              class="sqtm-grid-header-cell full-height"
            >
              <ng-container
                *sqtmCoreGridHeader="
                  columnDisplay.headerRenderer;
                  columnDisplay: columnDisplay;
                  gridDisplay: gridDisplay;
                  viewportName: viewportName
                "
              >
              </ng-container>
            </div>
          }
        }
      </div>
    }
  `,
  styleUrls: ['./grid-header-row.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GridHeaderRowComponent implements AfterViewInit, OnDestroy {
  ROW_ID = 'sqtm-core-grid-header-row';

  @Input()
  viewportName: GridViewportName;

  @Input()
  gridDisplay: GridDisplay;

  @ViewChildren('headers')
  headers: QueryList<ElementRef>;

  @ViewChild('row', { read: ElementRef })
  row: ElementRef;

  private unsub$ = new Subject<void>();

  constructor(
    private gridViewportService: GridViewportService,
    private ngZone: NgZone,
    private renderer: Renderer2,
  ) {}

  ngAfterViewInit(): void {
    this.ngZone.runOutsideAngular(() => {
      this.gridViewportService.renderedGridViewport$
        .pipe(takeUntil(this.unsub$))
        .subscribe((renderedGridViewport) => this.resizeHeaders(renderedGridViewport));
    });

    this.headers.changes
      .pipe(takeUntil(this.unsub$), withLatestFrom(this.gridViewportService.renderedGridViewport$))
      .subscribe(([_changes, renderedGridViewport]) => {
        this.resizeHeaders(renderedGridViewport);
      });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  calculateRowHeight(gridDisplay: GridDisplay): string {
    return `${gridDisplay.rowHeight}px`;
  }

  trackByFn(index: number, columnDisplay: ColumnDisplay) {
    return columnDisplay.id;
  }

  private resizeHeaders(renderedGridViewport: RenderedGridViewport) {
    const viewportElement = renderedGridViewport[this.viewportName];
    this.renderer.setStyle(this.row.nativeElement, 'width', `${viewportElement.totalWidth}px`);
    LOGGER.trace(this.headers.length.toString());
    this.headers.forEach((header) => this.resizeHeader(header, renderedGridViewport));
  }

  private resizeHeader(header: ElementRef, renderedGridViewport: RenderedGridViewport) {
    const columnId = header.nativeElement.dataset['columnId'];
    const column = renderedGridViewport[this.viewportName].columns[columnId];
    LOGGER.trace('resize ' + columnId, [column]);
    if (column) {
      this.renderer.setStyle(header.nativeElement, 'width', `${column.calculatedWidth}px`);
      this.renderer.setStyle(header.nativeElement, 'left', `${column.left}px`);
    }
  }
}
