/**
 * A dataRow loader is responsible for feeding the grid with data.
 * It's mainly a strategy for data loading that will be pushed into grid service.
 */
import { Observable, of } from 'rxjs';
import { AbstractDataRowLoader } from './abstract-data-row-loader';
import { RestService } from '../../../../core/services/rest.service';
import { GridState } from '../../model/state/grid.state';
import { ProjectDataMap } from '../../../../model/project/project-data.model';
import { GridFilters } from '../../model/state/filter.state';

export class ClientDataRowLoader extends AbstractDataRowLoader {
  constructor(restService: RestService) {
    super(restService);
  }

  public refreshData(
    state: GridState,
    _projectData: ProjectDataMap,
    _keepSelectedRows: boolean,
  ): Observable<GridState> {
    // NO OP for client grid, all data is on client. We just push next state into the behavior subject
    return of(state);
  }

  updateCell(_url: string[], _data: any): Observable<any> {
    return of(null);
  }

  selectAllRows(state: GridState): Observable<GridState> {
    const gridFilters: GridFilters = state.filterState.filters;
    const activeFilters = Object.values(gridFilters.entities).filter((filter) => filter.active);
    const dataRowState = { ...state.dataRowState };
    if (activeFilters.length > 0) {
      dataRowState.selectedRows = [...gridFilters.matchingRowIds];
    } else {
      dataRowState.selectedRows = [...dataRowState.ids];
    }
    dataRowState.lastToggledRowId = null;
    return of({ ...state, dataRowState });
  }

  beginAsync(state: Readonly<GridState>): GridState {
    return state;
  }

  endAsync(state: Readonly<GridState>): GridState {
    return state;
  }
}
