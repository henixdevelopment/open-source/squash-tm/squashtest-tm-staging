import { ColumnDefinition, Fixed } from '../model/column-definition.model';
import { GridDisplay } from '../model/grid-display.model';
import { GridType } from '../model/grid-definition.model';
import { StaticOrDynamicColumnId } from '../../../shared/constants/grid/grid-column-id';

export function createColumn(
  id: StaticOrDynamicColumnId,
  sortable: boolean = true,
): ColumnDefinition {
  return {
    id,
    draggable: false,
    sortable: sortable,
    forceRenderOnNoValue: false,
    resizable: true,
    show: true,
    widthCalculationStrategy: new Fixed(40),
    headerPosition: 'left',
    contentPosition: 'left',
    editable: false,
    showHeader: true,
    viewportName: 'mainViewport',
  };
}

export function getBasicGridDisplay(): GridDisplay {
  return {
    id: 'testing-grid',
    rowHeight: 25,
    gridType: GridType.GRID,
    enableRightToolBar: false,
    enableHeaders: false,
    allowShowAll: false,
    dynamicHeight: false,
    enableDrag: false,
    enableInternalDrop: false,
    loaded: true,
    allowModifications: true,
    mainViewport: {
      columnDisplays: [],
      totalWidth: 0,
      name: 'mainViewport',
    },
    leftViewport: {
      columnDisplays: [],
      totalWidth: 0,
      name: 'leftViewport',
    },
    rightViewport: {
      columnDisplays: [],
      totalWidth: 0,
      name: 'rightViewport',
    },
    style: {
      showLines: false,
      showOverlayBeforeLoad: false,
      highlightSelectedRows: true,
      highlightHoveredRows: true,
    },
    scopeDefinition: {
      allowCustomScope: false,
    },
    scope: null,
    draggedContentRenderer: null,
  };
}
