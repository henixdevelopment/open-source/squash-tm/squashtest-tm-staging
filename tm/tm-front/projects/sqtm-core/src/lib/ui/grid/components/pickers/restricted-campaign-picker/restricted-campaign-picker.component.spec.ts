import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RestrictedCampaignPickerComponent } from './restricted-campaign-picker.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestingUtilsModule } from '../../../../testing-utils/testing-utils.module';
import { OverlayModule } from '@angular/cdk/overlay';
import { RouterTestingModule } from '@angular/router/testing';

describe('RestrictedCampaignPickerComponent', () => {
  let component: RestrictedCampaignPickerComponent;
  let fixture: ComponentFixture<RestrictedCampaignPickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TestingUtilsModule, OverlayModule, RouterTestingModule],
      declarations: [RestrictedCampaignPickerComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RestrictedCampaignPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
