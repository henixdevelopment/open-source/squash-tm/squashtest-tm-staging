import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { AbstractCellRendererComponent } from '../abstract-cell-renderer/abstract-cell-renderer.component';
import { GridService } from '../../../services/grid.service';

@Component({
  selector: 'sqtm-core-rich-text-renderer',
  template: ` @if (columnDisplay) {
    <div class="flex">
      <span
        class="txt-ellipsis m-auto-0"
        nz-tooltip
        [sqtmCoreLabelTooltip]="convertHtmlToPlainText(row.data[columnDisplay.id])"
      >
        {{ convertHtmlToPlainText(row.data[columnDisplay.id]) }}
      </span>
    </div>
  }`,
  styleUrls: ['./rich-text-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RichTextRendererComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }

  convertHtmlToPlainText(html: string) {
    const tempDivElement = document.createElement('div');
    tempDivElement.innerHTML = html;
    return tempDivElement.textContent || tempDivElement.innerText || '';
  }
}
