import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewContainerRef,
} from '@angular/core';
import { GridDefinition } from '../../../model/grid-definition.model';
import {
  HIGH_LEVEL_REQUIREMENT_TREE_PICKER_ID,
  MONO_HIGH_LEVEL_REQUIREMENT_TREE_PICKER_CONFIG,
} from '../../../tree-pickers.constant';
import { Extendable } from '../../../model/column-definition.model';
import { TreeNodeCellRendererComponent } from '../../../../cell-renderer-common/tree-node-cell-renderer/tree-node-cell-renderer.component';
import { DataRow } from '../../../model/data-row.model';
import { ProjectDataMap } from '../../../../../model/project/project-data.model';
import { Subject, throwError } from 'rxjs';
import { RequirementPermissions } from '../../../../../model/permissions/simple-permissions';
import { GridService } from '../../../services/grid.service';
import { gridServiceFactory } from '../../../grid.service.provider';
import { RestService } from '../../../../../core/services/rest.service';
import { ReferentialDataService } from '../../../../../core/referential/services/referential-data.service';
import { Identifier } from '../../../../../model/entity.model';
import { DialogService } from '../../../../dialog/services/dialog.service';
import { GridPersistenceService } from '../../../../../core/services/grid-persistence/grid-persistence.service';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { TreeWithStatePersistence } from '../../../../workspace-common/components/tree/tree-with-state-persistence';
import { column } from '../../../model/common-column-definition.builders';
import {
  HighLevelRequirement,
  Requirement,
  RequirementFolder,
  RequirementLibrary,
  SquashTmDataRow,
} from '../../../model/data-row.type';
import { treePicker } from '../../../model/grid-builders';
import { SquashTmDataRowType } from '../../../../../model/grids/data-row.model';
import { GridColumnId } from '../../../../../shared/constants/grid/grid-column-id';

export function convertHighLevelRequirementLiterals(
  literals: Partial<DataRow>[],
  projectsData: ProjectDataMap,
) {
  // DON'T INLINE THE VAR, IT WOULD MAKE BUILD CRASH WITH ERROR :
  // {"__symbolic":"error","message":"Lambda not supported","line":42,"character":22}
  const converted: SquashTmDataRow[] = literals.map((literal) =>
    convertHighLevelRequirementLiteral(literal, projectsData),
  );
  return converted;
}

export function highLevelRequirementTreePickerDefinition(): GridDefinition {
  return treePicker(HIGH_LEVEL_REQUIREMENT_TREE_PICKER_ID)
    .server()
    .withServerUrl(['requirement-tree'])
    .withColumns([
      column(GridColumnId.NAME)
        .changeWidthCalculationStrategy(new Extendable(300))
        .withRenderer(TreeNodeCellRendererComponent),
    ])
    .enableDrag()
    .disableMultiSelection()
    .withRowConverter(convertHighLevelRequirementLiterals)
    .build();
}

export function convertHighLevelRequirementLiteral(
  literal: Partial<DataRow>,
  projectsData: ProjectDataMap,
): SquashTmDataRow {
  let dataRow: DataRow;
  let type = 'Generic';
  if (literal.type) {
    type = literal.type;
  } else {
    if (typeof literal.id === 'string') {
      try {
        type = literal.id.split('-')[0];
      } catch (e) {
        console.log(
          `Unable to auto assign row type from id : ${literal.id}. Will keep generic type`,
        );
      }
    }
  }

  switch (type) {
    case SquashTmDataRowType.RequirementLibrary:
      dataRow = new RequirementLibrary();
      break;
    case SquashTmDataRowType.RequirementFolder:
      dataRow = new RequirementFolder();
      break;
    case SquashTmDataRowType.Requirement:
      dataRow = new Requirement();
      break;
    case SquashTmDataRowType.HighLevelRequirement:
      dataRow = new HighLevelRequirement();
      break;
    default:
      throwError(() => 'Not handled type ' + type);
  }

  dataRow.projectId = literal.projectId;
  const project = projectsData[dataRow.projectId];
  dataRow.simplePermissions = new RequirementPermissions(project);
  Object.assign(dataRow, literal);
  if (type !== SquashTmDataRowType.HighLevelRequirement) {
    dataRow.selectable = false;
  }
  return dataRow;
}

@Component({
  selector: 'sqtm-core-high-level-requirement-picker',
  template: '<sqtm-core-grid class="sqtm-core-prevent-selection-in-grid"></sqtm-core-grid>',
  styleUrls: ['./high-level-requirement-picker.component.less'],
  providers: [
    {
      provide: MONO_HIGH_LEVEL_REQUIREMENT_TREE_PICKER_CONFIG,
      useFactory: highLevelRequirementTreePickerDefinition,
    },
    {
      provide: GridService,
      useFactory: gridServiceFactory,
      deps: [RestService, MONO_HIGH_LEVEL_REQUIREMENT_TREE_PICKER_CONFIG, ReferentialDataService],
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HighLevelRequirementPickerComponent
  extends TreeWithStatePersistence
  implements OnInit, OnDestroy
{
  @Input()
  set pickerPersistenceKey(persistenceKey) {
    if (!this.persistenceKey) {
      this.persistenceKey = persistenceKey;
    } else {
      throw Error('Cannot change the persistence key dynamically ' + this.persistenceKey);
    }
  }

  @Input()
  initialSelectedNodes: Identifier[] = [];

  @Output()
  selectedRows = new EventEmitter<DataRow[]>();

  @Input()
  set enableMultiRowSelection(shouldBeEnabled: boolean) {
    this.tree.setMultiSelectionEnabled(shouldBeEnabled);
  }

  unsub$ = new Subject<void>();

  constructor(
    public tree: GridService,
    protected referentialDataService: ReferentialDataService,
    protected restService: RestService,
    protected dialogService: DialogService,
    protected vcr: ViewContainerRef,
    protected gridPersistenceService: GridPersistenceService,
    protected router: Router,
  ) {
    super(
      tree,
      referentialDataService,
      gridPersistenceService,
      restService,
      router,
      dialogService,
      vcr,
      null,
      'requirement-tree',
    );
  }

  ngOnInit() {
    this.tree.selectedRows$.pipe(takeUntil(this.unsub$)).subscribe((rows) => {
      this.selectedRows.next(rows);
    });
    this.initData({ selectedNodes: this.initialSelectedNodes });
    if (this.persistenceKey) {
      this.registerStatePersistence();
    }
  }

  ngOnDestroy(): void {
    if (this.persistenceKey) {
      this.unregisterStatePersistence();
    }
    this.tree.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }
}
