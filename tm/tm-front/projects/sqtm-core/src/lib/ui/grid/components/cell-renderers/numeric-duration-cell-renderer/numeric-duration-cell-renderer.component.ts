import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { AbstractCellRendererComponent } from '../abstract-cell-renderer/abstract-cell-renderer.component';
import { GridService } from '../../../services/grid.service';
import { TranslateService } from '@ngx-translate/core';
import { DurationFormatUtils } from '../../../../../core/utils/duration-format.utils';
import { getSupportedBrowserLang } from '../../../../../core/utils/browser-langage.utils';

@Component({
  selector: 'sqtm-core-numeric-duration-cell-renderer',
  template: ` <ng-container *ngIf="columnDisplay">
    <div class="full-width full-height flex-column">
      <span
        class="txt-ellipsis"
        [ngClass]="textClass"
        nz-tooltip
        [nzTooltipTitle]="getFormattedValue()"
        [nzTooltipPlacement]="'topLeft'"
        >{{ getShortFormatValue() }}</span
      >
    </div>
  </ng-container>`,
  styleUrls: ['./numeric-duration-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NumericDurationCellRendererComponent extends AbstractCellRendererComponent {
  displayValueForNulls = '-';
  constructor(
    grid: GridService,
    cdr: ChangeDetectorRef,
    private translateService: TranslateService,
  ) {
    super(grid, cdr);
  }

  getFormattedValue(): string {
    const duration = this.row.data[this.columnDisplay.id];
    return duration != null
      ? DurationFormatUtils.longDuration(getSupportedBrowserLang(this.translateService), duration)
      : null;
  }

  getShortFormatValue() {
    const duration = this.row.data[this.columnDisplay.id];
    return duration != null
      ? DurationFormatUtils.shortDuration(getSupportedBrowserLang(this.translateService), duration)
      : this.displayValueForNulls;
  }

  get textClass(): string {
    return 'text-align-' + this.columnDisplay.contentPosition;
  }
}
