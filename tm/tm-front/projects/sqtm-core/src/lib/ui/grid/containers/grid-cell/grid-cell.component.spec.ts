import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GridCellComponent } from './grid-cell.component';
import { GridViewportService } from '../../services/grid-viewport.service';
import { GridTestingModule } from '../../grid-testing/grid-testing.module';
import { TestingUtilsModule } from '../../../testing-utils/testing-utils.module';

describe('GridCellComponent', () => {
  let component: GridCellComponent;
  let fixture: ComponentFixture<GridCellComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, GridTestingModule],
      declarations: [GridCellComponent],
      providers: [GridViewportService],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GridCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
