import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GridViewportComponent, VirtualScrollDirective } from './grid-viewport.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { GridViewportDirective } from '../../directives/grid-viewport.directive';
import { GridDefinition } from '../../model/grid-definition.model';
import { RestService } from '../../../../core/services/rest.service';
import { GridService } from '../../services/grid.service';
import { gridServiceFactory } from '../../grid.service.provider';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { ReferentialDataService } from '../../../../core/referential/services/referential-data.service';
import { GridViewportService } from '../../services/grid-viewport.service';
import { grid } from '../../model/grid-builders';

describe('GridViewportComponent', () => {
  let component: GridViewportComponent;
  let fixture: ComponentFixture<GridViewportComponent>;

  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule, ScrollingModule],
      declarations: [GridViewportComponent, GridViewportDirective, VirtualScrollDirective],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig,
        },
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService],
        },
        GridViewportService,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GridViewportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
