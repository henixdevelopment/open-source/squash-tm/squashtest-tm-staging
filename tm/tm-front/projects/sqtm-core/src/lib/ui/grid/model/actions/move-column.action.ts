import { Identifier } from '../../../../model/entity.model';
import { GridViewportName } from '../../../../model/grids/grid-viewport-name';

export class MoveColumnAction {
  id: Identifier;
  index: number;
  moveToViewport: GridViewportName;
}
