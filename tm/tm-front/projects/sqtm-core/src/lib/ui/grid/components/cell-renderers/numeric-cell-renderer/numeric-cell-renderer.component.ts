import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input } from '@angular/core';
import { AbstractCellRendererComponent } from '../abstract-cell-renderer/abstract-cell-renderer.component';
import { GridService } from '../../../services/grid.service';

@Component({
  selector: 'sqtm-core-numeric-cell-renderer',
  template: ` @if (columnDisplay) {
    <div class="full-width full-height flex-column">
      <span
        class="txt-ellipsis m-auto"
        [sqtmCoreLabelTooltip]="getValue()"
        nz-tooltip
        [nzTooltipTitle]=""
        [nzTooltipPlacement]="'topLeft'"
        >{{ getValue() }}</span
      >
    </div>
  }`,
  styleUrls: ['./numeric-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NumericCellRendererComponent extends AbstractCellRendererComponent {
  @Input()
  displayValueForNulls = '0';
  constructor(grid: GridService, cdr: ChangeDetectorRef) {
    super(grid, cdr);
  }

  getValue() {
    const value = this.row.data[this.columnDisplay.id];
    if (value == null || value === '') {
      return this.displayValueForNulls;
    } else {
      return value;
    }
  }
}

@Component({
  selector: 'sqtm-core-nullable-number-cell-renderer',
  template: `
    @if (columnDisplay && row) {
      <div class="full-width full-height" [sqtmCoreSelectable]="row.id">
        <sqtm-core-numeric-cell-renderer
          [columnDisplay]="columnDisplay"
          [row]="row"
          [depth]="depth"
          [index]="index"
          [gridDisplay]="gridDisplay"
          [selected]="selected"
          [showAsFilteredParent]="showAsFilteredParent"
          [displayValueForNulls]="'-'"
        >
        </sqtm-core-numeric-cell-renderer>
      </div>
    }
  `,
  styleUrls: ['./numeric-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NullableNumberCellRendererComponent extends AbstractCellRendererComponent {
  constructor(grid: GridService, cdr: ChangeDetectorRef) {
    super(grid, cdr);
  }
}
