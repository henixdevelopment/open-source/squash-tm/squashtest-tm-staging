import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ConcatenatedStatusesCellRendererComponent } from './concatenated-statuses-cell-renderer.component';
import { grid } from '../../../model/grid-builders';
import { GridDefinition } from '../../../model/grid-definition.model';
import { RestService } from '../../../../../core/services/rest.service';
import { GridService } from '../../../services/grid.service';
import { gridServiceFactory } from '../../../grid.service.provider';
import { ReferentialDataService } from '../../../../../core/referential/services/referential-data.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { getBasicGridDisplay } from '../../../grid-testing/grid-testing-utils';
import { Fixed } from '../../../model/column-definition.model';
import { TestCaseLibrary } from '../../../model/data-row.type';
import { TranslateService } from '@ngx-translate/core';

describe('ConcatenatedStatusesCellRendererComponent', () => {
  let component: ConcatenatedStatusesCellRendererComponent;
  let fixture: ComponentFixture<ConcatenatedStatusesCellRendererComponent>;
  const gridConfig = grid('grid-test').build();
  const restService = {};
  const translateService = jasmine.createSpyObj('TranslateService', ['instant']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ConcatenatedStatusesCellRendererComponent],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig,
        },
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService],
        },
        {
          provide: TranslateService,
          useValue: translateService,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(ConcatenatedStatusesCellRendererComponent);
        component = fixture.componentInstance;
        component.gridDisplay = getBasicGridDisplay();
        component.columnDisplay = {
          id: 'column-1',
          show: true,
          widthCalculationStrategy: new Fixed(200),
          headerPosition: 'left',
          contentPosition: 'left',
          showHeader: true,
          viewportName: 'mainViewport',
        };
        component.row = {
          ...new TestCaseLibrary(),
          id: 'tcln-1',
          data: { id: 'tcln-1' },
        };
        fixture.detectChanges();
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
