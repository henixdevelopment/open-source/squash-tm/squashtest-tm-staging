import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomColumnsComponent } from './custom-columns.component';
import { TranslateService } from '@ngx-translate/core';
import { GridService } from '../../../services/grid.service';
import {
  mockGridService,
  mockPassThroughTranslateService,
  mockReferentialDataService,
} from '../../../../../../../../sqtm-app/src/app/utils/testing-utils/mocks.service';
import { ReferentialDataService } from '../../../../../core/referential/services/referential-data.service';
import { EMPTY } from 'rxjs';
import { AppTestingUtilsModule } from '../../../../../../../../sqtm-app/src/app/utils/testing-utils/app-testing-utils.module';

describe('CustomColumnsComponent', () => {
  let component: CustomColumnsComponent;
  let fixture: ComponentFixture<CustomColumnsComponent>;
  const gridService = mockGridService();
  const refDataService = mockReferentialDataService();
  refDataService.globalConfiguration$ = EMPTY;
  gridService.gridState$ = EMPTY;
  gridService.columns$ = EMPTY;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      declarations: [CustomColumnsComponent],
      providers: [
        {
          provide: TranslateService,
          useValue: mockPassThroughTranslateService(),
        },
        {
          provide: GridService,
          useValue: gridService,
        },
        {
          provide: ReferentialDataService,
          useValue: refDataService,
          deps: [TranslateService],
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(CustomColumnsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
