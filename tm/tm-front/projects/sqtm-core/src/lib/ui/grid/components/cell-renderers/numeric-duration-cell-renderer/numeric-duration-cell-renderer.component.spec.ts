import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NumericDurationCellRendererComponent } from './numeric-duration-cell-renderer.component';
import { grid } from '../../../model/grid-builders';
import { GridDefinition } from '../../../model/grid-definition.model';
import { RestService } from '../../../../../core/services/rest.service';
import { GridService } from '../../../services/grid.service';
import { gridServiceFactory } from '../../../grid.service.provider';
import { ReferentialDataService } from '../../../../../core/referential/services/referential-data.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { TestingUtilsModule } from '../../../../testing-utils/testing-utils.module';

describe('NumericDurationCellRendererComponent', () => {
  let component: NumericDurationCellRendererComponent;
  let fixture: ComponentFixture<NumericDurationCellRendererComponent>;

  const gridConfig = grid('grid-test').build();
  const restService = {};
  const translateService = jasmine.createSpyObj('TranslateService', ['getBrowserLang']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [NumericDurationCellRendererComponent],
      imports: [TestingUtilsModule],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig,
        },
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService],
        },
        {
          provide: TranslateService,
          useValue: translateService,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumericDurationCellRendererComponent);
    component = fixture.componentInstance;
    component.row = {
      allowMoves: false,
      allowedChildren: [],
      component: undefined,
      id: undefined,
      projectId: 0,
      selectable: false,
      simplePermissions: undefined,
      type: undefined,
      data: {
        columnId: 0,
      },
    };
    component.columnDisplay = {
      contentPosition: undefined,
      headerPosition: undefined,
      show: false,
      showHeader: false,
      viewportName: undefined,
      widthCalculationStrategy: undefined,
      id: 'columnId',
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should handle milliseconds correctly for 32ms', () => {
    component.row.data['columnId'] = 32;
    expect(component.getShortFormatValue()).toEqual('32ms');
  });

  it('should format correctly for 3214ms', () => {
    component.row.data['columnId'] = 3214;
    expect(component.getShortFormatValue()).toEqual('3s214ms');
  });

  it('should format minutes and seconds correctly for 703010ms', () => {
    component.row.data['columnId'] = 703010;
    expect(component.getShortFormatValue()).toEqual('11m43s');
  });

  it('should format hours, minutes, and seconds correctly for 8030100ms', () => {
    component.row.data['columnId'] = 8030100;
    expect(component.getShortFormatValue()).toEqual('2h13m');
  });

  it('should display "-" for null values', () => {
    component.row.data['columnId'] = null;
    expect(component.getShortFormatValue()).toEqual('-');
  });

  it('should return the correct value with "ms" suffix for 8030100ms', () => {
    component.row.data['columnId'] = 8030100;
    expect(component.getFormattedValue()).toEqual('2h13m50s100ms');
  });

  it('should return null for null values', () => {
    component.row.data['columnId'] = null;
    expect(component.getFormattedValue()).toBeNull();
  });
});
