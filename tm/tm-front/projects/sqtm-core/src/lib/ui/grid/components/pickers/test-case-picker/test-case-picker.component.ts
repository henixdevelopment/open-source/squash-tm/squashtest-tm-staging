import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewContainerRef,
} from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { GridDefinition } from '../../../model/grid-definition.model';
import {
  TEST_CASE_TREE_PICKER_CONFIG,
  TEST_CASE_TREE_PICKER_ID,
} from '../../../tree-pickers.constant';
import { Extendable, TreeNodeRendererDisplayOptions } from '../../../model/column-definition.model';
import { GridService } from '../../../services/grid.service';
import { gridServiceFactory } from '../../../grid.service.provider';
import { RestService } from '../../../../../core/services/rest.service';
import { ReferentialDataService } from '../../../../../core/referential/services/referential-data.service';
import { Subject } from 'rxjs';
import { DataRow } from '../../../model/data-row.model';
import { Identifier } from '../../../../../model/entity.model';
import { TreeNodeCellRendererComponent } from '../../../../cell-renderer-common/tree-node-cell-renderer/tree-node-cell-renderer.component';
import { DialogService } from '../../../../dialog/services/dialog.service';
import { Router } from '@angular/router';
import { TreeWithStatePersistence } from '../../../../workspace-common/components/tree/tree-with-state-persistence';
import { GridPersistenceService } from '../../../../../core/services/grid-persistence/grid-persistence.service';
import { column } from '../../../model/common-column-definition.builders';
import { treePicker } from '../../../model/grid-builders';
import { GridColumnId } from '../../../../../shared/constants/grid/grid-column-id';

export function testCaseTreePickerConfigFactory(): GridDefinition {
  const options: TreeNodeRendererDisplayOptions = {
    ellipsisOnLeft: false,
    kind: 'treeNodeRendererDisplay',
  };

  return treePicker(TEST_CASE_TREE_PICKER_ID)
    .server()
    .withServerUrl(['test-case-tree'])
    .withColumns([
      column(GridColumnId.NAME)
        .changeWidthCalculationStrategy(new Extendable(300))
        .withOptions(options)
        .withRenderer(TreeNodeCellRendererComponent),
    ])
    .enableDrag()
    .build();
}

@Component({
  selector: 'sqtm-core-test-case-picker',
  templateUrl: './test-case-picker.component.html',
  styleUrls: ['./test-case-picker.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: TEST_CASE_TREE_PICKER_CONFIG,
      useFactory: testCaseTreePickerConfigFactory,
    },
    {
      provide: GridService,
      useFactory: gridServiceFactory,
      deps: [RestService, TEST_CASE_TREE_PICKER_CONFIG, ReferentialDataService],
    },
  ],
})
export class TestCasePickerComponent extends TreeWithStatePersistence implements OnInit, OnDestroy {
  @Input()
  set pickerPersistenceKey(persistenceKey) {
    if (!this.persistenceKey) {
      this.persistenceKey = persistenceKey;
    } else {
      throw Error('Cannot change the persistence key dynamically ' + this.persistenceKey);
    }
  }

  @Input()
  initialSelectedNodes: Identifier[] = [];

  @Output()
  selectedRows = new EventEmitter<DataRow[]>();

  unsub$ = new Subject<void>();

  constructor(
    public tree: GridService,
    protected referentialDataService: ReferentialDataService,
    protected restService: RestService,
    protected dialogService: DialogService,
    protected vcr: ViewContainerRef,
    protected gridPersistenceService: GridPersistenceService,
    protected router: Router,
  ) {
    super(
      tree,
      referentialDataService,
      gridPersistenceService,
      restService,
      router,
      dialogService,
      vcr,
      null,
      'test-case-tree',
    );
  }

  ngOnInit() {
    this.tree.selectedRows$.pipe(takeUntil(this.unsub$)).subscribe((rows) => {
      this.selectedRows.next(rows);
    });

    this.initData({ selectedNodes: this.initialSelectedNodes });
    if (this.persistenceKey) {
      this.registerStatePersistence();
    }
    this.initTruncateNameMenu();
  }

  ngOnDestroy(): void {
    if (this.persistenceKey) {
      this.unregisterStatePersistence();
    }
    this.tree.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }
}
