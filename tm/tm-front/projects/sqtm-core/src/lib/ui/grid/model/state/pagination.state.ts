export interface PaginationState {
  active: boolean;
  page: number;
  // Actual pagination size. If -1 it will show all available lines.
  size: number;
  allowedPageSizes: number[];
  fixedPaginationSize: boolean;
}

type PaginationStateReadOnly = Readonly<PaginationState>;

export function initialPaginationState(): PaginationStateReadOnly {
  return {
    size: 25,
    page: 0,
    allowedPageSizes: [10, 25, 50, 100],
    active: true,
    fixedPaginationSize: false,
  };
}
