import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { AbstractCellRendererComponent } from '../abstract-cell-renderer/abstract-cell-renderer.component';
import { GridService } from '../../../services/grid.service';

@Component({
  selector: 'sqtm-core-editable-numeric-cell-renderer',
  template: ` @if (row) {
    <div class="flex-column full-height full-width">
      <sqtm-core-editable-numeric-field
        style="margin: auto 0 auto 0;"
        [size]="'small'"
        [value]="row.data[columnDisplay.id]"
      ></sqtm-core-editable-numeric-field>
    </div>
  }`,
  styleUrls: ['./editable-numeric-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditableNumericCellRendererComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }
}
