import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {
  catchError,
  distinctUntilChanged,
  filter,
  finalize,
  map,
  switchMap,
  take,
} from 'rxjs/operators';
import { GridService } from '../grid.service';
import { AbstractServerOperationHandler } from './abstract-server-operation-handler';
import { GridState } from '../../model/state/grid.state';
import { Identifier } from '../../../../model/entity.model';

@Injectable()
export abstract class AbstractAdminServerOperationHandler extends AbstractServerOperationHandler {
  canCopy$: Observable<boolean> = of(false);
  canPaste$: Observable<boolean> = of(false);
  canDelete$: Observable<boolean> = of(false);
  canCreate$: Observable<boolean> = of(false);
  canDrag$: Observable<boolean>;

  private _grid: GridService;

  get grid(): GridService {
    return this._grid;
  }

  set grid(gridService: GridService) {
    this._grid = gridService;

    this.canDrag$ = this._grid.isSortedOrFiltered$.pipe(
      map((isSortedOrFiltered) => !isSortedOrFiltered),
      distinctUntilChanged(),
    );
  }

  notifyInternalDrop(): void {
    this.canDrag$
      .pipe(
        take(1),
        filter((canDrag) => canDrag),
        switchMap(() => this.grid.gridState$),
        take(1),
      )
      .subscribe((gridState) => this.reorderOptions(gridState));
  }

  copy(): void {}

  paste(): void {}

  delete(): void {}

  allowDropSibling(): boolean {
    return true;
  }

  allowDropInto(): boolean {
    return false;
  }

  private reorderOptions(gridState: GridState) {
    const filteredList = (gridState.dataRowState.ids as string[]).filter(
      (id) => Boolean(id) && !gridState.uiState.dragState.draggedRowIds.includes(id),
    );

    const newPosition = this.findDropPosition(gridState, filteredList);
    const draggedRows = gridState.uiState.dragState.draggedRowIds as string[];

    this.grid.beginAsyncOperation();
    this.doChangePosition(draggedRows, newPosition)
      .pipe(
        catchError((error) => {
          this.grid.refreshDataAndKeepSelectedRows();
          return of(error); // If we don't return an observable, we won't hit 'finalize'
        }),
        finalize(() => this.grid.completeAsyncOperation()),
      )
      .subscribe();
  }

  abstract doChangePosition(draggedRows: Identifier[], newPosition: number): Observable<any>;

  private findDropPosition(gridState: GridState, filteredList: string[]): number {
    const targetId = gridState.uiState.dragState.currentDndTarget.id as string;
    const zone = gridState.uiState.dragState.currentDndTarget.zone;

    let newPosition = filteredList.indexOf(targetId);

    if (zone === 'below') {
      newPosition++;
    }

    return newPosition;
  }
}
