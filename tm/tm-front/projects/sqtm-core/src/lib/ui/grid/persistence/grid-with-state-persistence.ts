import { Inject, Injectable, InjectionToken } from '@angular/core';
import { concatMap, filter, map, take } from 'rxjs/operators';
import { GridService } from '../services/grid.service';
import { GridPersistenceService } from '../../../core/services/grid-persistence/grid-persistence.service';
import { GridStateSnapshot } from '../../../core/services/grid-persistence/grid-state-snapshot';
import { Observable, of } from 'rxjs';
import { TextResearchFieldComponent } from '../../workspace-common/components/forms/text-research-field/text-research-field.component';
import { StringFilterValue } from '../../filters/state/filter.state';

export const GRID_PERSISTENCE_KEY = new InjectionToken<string>(
  'Injection token for grid persistence key',
);

@Injectable()
export class GridWithStatePersistence {
  constructor(
    private grid: GridService,
    private gridPersistenceService: GridPersistenceService,
    @Inject(GRID_PERSISTENCE_KEY) private persistenceKey: string,
  ) {}

  registerStatePersistence() {
    this.grid.loaded$
      .pipe(
        filter((init) => init),
        take(1),
      )
      .subscribe(() => {
        this.gridPersistenceService.register(this.persistenceKey, this.grid);
      });
  }

  saveSnapshot() {
    this.grid.loaded$
      .pipe(
        take(1),
        filter((init) => init),
        concatMap(() => this.gridPersistenceService.saveSnapshot(this.persistenceKey, this.grid)),
      )
      .subscribe();
  }

  restoreGridState(): Observable<any> {
    return this.gridPersistenceService.ready$.pipe(
      take(1),
      filter((ready) => ready),
      concatMap(() =>
        this.gridPersistenceService.selectGridSnapshot(this.persistenceKey).pipe(take(1)),
      ),
      concatMap((snapshot) => this.addConfigurationToGrid(snapshot)),
    );
  }

  popGridState(): Observable<GridStateSnapshot> {
    return this.gridPersistenceService.ready$.pipe(
      take(1),
      filter((ready) => ready),
      concatMap(() => this.gridPersistenceService.popGridSnapshot(this.persistenceKey)),
      concatMap((snapshot) => this.addConfigurationToGrid(snapshot).pipe(map(() => snapshot))),
    );
  }

  private addConfigurationToGrid(snapshot: GridStateSnapshot): Observable<any> {
    if (!snapshot.noSnapshotExisting) {
      return this.grid.addInitialConfiguration({
        pagination: snapshot.pagination,
        sortedColumns: snapshot.sortedColumns,
        filters: snapshot.filters,
      });
    } else {
      return of(false);
    }
  }

  public static updateMultipleColumnSearchField(
    snapshot: GridStateSnapshot,
    textResearchFieldComponent: TextResearchFieldComponent,
  ) {
    if (
      snapshot &&
      !snapshot.noSnapshotExisting &&
      snapshot.filters.length > 0 &&
      textResearchFieldComponent
    ) {
      const filterValue = snapshot.filters[0].value;
      const multiFilterValue = filterValue as StringFilterValue;
      textResearchFieldComponent.researchValue = multiFilterValue.value;
    }
  }
}
