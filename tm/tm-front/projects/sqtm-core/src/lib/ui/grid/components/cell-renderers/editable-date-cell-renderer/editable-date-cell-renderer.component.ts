import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { AbstractCellRendererComponent } from '../abstract-cell-renderer/abstract-cell-renderer.component';
import { GridService } from '../../../services/grid.service';

@Component({
  selector: 'sqtm-core-editable-date-cell-renderer',
  template: ` @if (row) {
    <div class="full-width full-height flex-column">
      <sqtm-core-editable-date-field
        style="margin: auto 0 auto 0;"
        [value]="row.data[columnDisplay.id]"
      ></sqtm-core-editable-date-field>
    </div>
  }`,
  styleUrls: ['./editable-date-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditableDateCellRendererComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }
}
