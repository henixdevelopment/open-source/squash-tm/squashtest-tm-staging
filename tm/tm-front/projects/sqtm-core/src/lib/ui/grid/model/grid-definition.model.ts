import type { ColumnDefinition, SortedColumn } from './column-definition.model';
import type {
  GridScopeDefinition,
  GridStyleDefinition,
  LiteralDataRowConverter,
} from './state/definition.state';
import type { Type } from '@angular/core';
import type { BindableEntity } from '../../../model/bindable-entity.model';
import type { DraggedContentRenderer } from '../../drag-and-drop/dragged-content-renderer';

export interface PaginationConfig {
  active: boolean;
  size: number;
  showAll: boolean;
  allowedPageSizes: number[];
  fixedPaginationSize: boolean;
}

export interface DragAndDropConfig {
  // does the grid is allowed to start dnd operation
  enableDrag: boolean;
  // does the grid is allowed to perform internal dnd, aka moving nodes into grid.
  enableInternalDrop: boolean;
}

export enum GridDataProvider {
  SERVER = 'SERVER',
  CLIENT = 'CLIENT',
}

// Enum representing the different flavor of grids
// GRID is the standard grid with fixed row height and all functionality activated
// TREE is a grid with fixed row height designed for representation of hierarchical data. Some functionality are not possible in this mode
//  because they enter in conflict with hierarchy : Pagination, Row grouping...
// TABLE is a grid with variable row height. Useful when having to treat with rich text... However some functionality are disabled in
//  this mode : Pinned columns...
export enum GridType {
  GRID = 'GRID',
  TREE = 'TREE',
  TABLE = 'TABLE',
}

export class GridDefinition {
  id: string;
  rowHeight;
  dataProvider: GridDataProvider = GridDataProvider.CLIENT;
  gridType: GridType;
  serverRootUrl: string[];
  columnDefinitions: ColumnDefinition[];
  pagination: Partial<PaginationConfig>;
  allowShowAll: boolean;
  enableHeaders: boolean;
  enableRightToolBar: boolean;
  enableMultiRowSelection: boolean;
  unselectRowOnSimpleClick: boolean;
  showExtendedScopeOptionInFilterPanel: boolean;
  // If true, the total grid height will be dynamic and will follow the number of rows displayed.
  // Mainly used for small table like coverages in TC, parameters...
  // DO NOT USE if pagination can be disabled (TREE or enableShowAll), if you allow very large pagination size or if you can't predict the
  // size of each row (aka TABLE)
  dynamicHeight: boolean;
  // This function is called when rows are loaded from server or other source.
  // The rows loaded as object literal will be converted in true object with behavior by this function.
  // By this way, we can define custom row type and give them to the grid to have custom rules for hierarchy, filters...
  literalDataRowConverter: LiteralDataRowConverter;
  // does the perimeter of the table must react to global filter changes.
  // todo we should probably recenter that on scope concept that has been developed for filters
  cufColumnsByGlobalFilter: boolean;
  // Bindable entity used for the display of columns for custom fields.
  appendCufFrom: BindableEntity;
  // Bindable entity used for the display of filters for custom fields. Decoupled from columns cuf because some grids could
  // filters on one domain and display another one (aka research on test-case and display requirements)
  addFilterCufFrom: BindableEntity;
  initialSortedColumns: SortedColumn[];
  // does the grid is allowed to start dnd operation
  enableDrag: boolean;
  // does the grid is allowed to perform internal dnd, aka moving nodes into grid.
  enableInternalDrop: boolean;
  draggedContentRenderer: Type<DraggedContentRenderer>;
  style: GridStyleDefinition;
  scopeDefinition: GridScopeDefinition;
  serverModificationUrl?: string[];
  multipleColumnsFilter?: string[];
  shouldResetSorts?: boolean;
}
