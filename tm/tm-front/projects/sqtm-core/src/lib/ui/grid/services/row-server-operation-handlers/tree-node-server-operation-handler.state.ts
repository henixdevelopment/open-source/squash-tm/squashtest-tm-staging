export class TreeNodeServerOperationHandlerState {
  deleteOperationIsRunning: boolean;
}

export function initialTreeNodeOperationHandlerState(): Readonly<TreeNodeServerOperationHandlerState> {
  return {
    deleteOperationIsRunning: false,
  };
}
