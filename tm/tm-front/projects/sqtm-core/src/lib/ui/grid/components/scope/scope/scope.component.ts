import {
  ChangeDetectionStrategy,
  Component,
  ComponentRef,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { Subject } from 'rxjs';
import { ComponentPortal } from '@angular/cdk/portal';
import { filter, takeUntil, withLatestFrom } from 'rxjs/operators';
import { ProjectScopeComponent } from '../project-scope/project-scope.component';
import { CustomScopeKind } from '../../../model/state/definition.state';
import { Scope } from '../../../../../model/filter/filter.model';

@Component({
  selector: 'sqtm-core-scope',
  templateUrl: './scope.component.html',
  styleUrls: ['./scope.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScopeComponent implements OnInit, OnDestroy {
  get scope(): Scope {
    return this._scope;
  }

  @Input()
  set scope(scope: Scope) {
    this._scope = scope;
    if (this.componentRef && this.overlayRef) {
      this.componentRef.instance.setScope(scope);
    }
  }

  private _scope: Scope;

  @Input()
  allowCustomScope: boolean;

  @Input()
  customScopeKind: CustomScopeKind;

  @Output()
  valueChanged = new EventEmitter<Scope>();

  @ViewChild('scopeField', { read: ElementRef })
  private scopeField: ElementRef;

  private overlayRef: OverlayRef;

  private unsub$ = new Subject<void>();

  private componentRef: ComponentRef<ProjectScopeComponent>;

  private maxTooltipScopeSize = 20;

  constructor(private overlay: Overlay) {}

  ngOnInit() {}

  edit() {
    const positionStrategy = this.overlay
      .position()
      .flexibleConnectedTo(this.scopeField)
      .withPositions([
        {
          originX: 'end',
          overlayX: 'start',
          originY: 'center',
          overlayY: 'center',
          offsetX: 10,
          offsetY: 20,
        },
      ]);
    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
      panelClass: 'sqtm-overlay__background',
    };
    this.overlayRef = this.overlay.create(overlayConfig);
    const componentPortal = new ComponentPortal(ProjectScopeComponent);
    this.componentRef = this.overlayRef.attach(componentPortal);
    if (this.allowCustomScope) {
      this.componentRef.instance.customScopeKind = this.customScopeKind;
    }
    this.componentRef.instance.setScope({ ...this.scope });
    this.componentRef.instance.close.pipe(takeUntil(this.unsub$)).subscribe(() => this.close());

    this.componentRef.instance.valueChanged
      .pipe(
        takeUntil(this.unsub$),
        filter((scope: Scope) => scope.kind !== 'project'),
      )
      .subscribe((value) => this.changeValue(value));

    this.overlayRef
      .backdropClick()
      .pipe(
        withLatestFrom(this.componentRef.instance.valueChanged),
        filter(([, scope]: [any, Scope]) => scope.kind === 'project'),
      )
      .subscribe(([, scope]: [any, Scope]) => this.changeValue(scope));

    this.overlayRef.backdropClick().subscribe(() => this.close());
  }

  getDisplayValue() {
    if (this.scope.value.length <= this.maxTooltipScopeSize) {
      const labels = this.scope.value.map((v) => v.label);
      labels.sort((a, b) => a.localeCompare(b));
      return labels.join(', ');
    } else {
      const tooltipScope = this.scope.value.slice(0, this.maxTooltipScopeSize);
      const labels = tooltipScope.map((v) => v.label);
      labels.sort((a, b) => a.localeCompare(b));
      return labels.join(', ').concat(' ...');
    }
  }

  close() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
      this.componentRef = null;
    }
  }

  changeValue(value: Scope) {
    this.valueChanged.next(value);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
