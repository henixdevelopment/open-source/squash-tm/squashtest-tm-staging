import { Injectable } from '@angular/core';
import { DataRow } from '../model/data-row.model';
import { createStore, Store } from '../../../core/store/store';
import { LocalPersistenceService } from '../../../core/services/local-persistence.service';
import { filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class GridClipboardService {
  private readonly COPY_PERSISTENCE: string = 'copy-persistence';
  private store: Store<LocalStorageCopyData> = createStore(getDefaultLocalStorageCopyData());
  localStorageCopyData$ = this.store.state$;

  constructor(private localPersistenceService: LocalPersistenceService) {
    this.synchroniseStateWithLocalStorage();
  }

  setLocalStorageCopyData(localStorageCopyData: LocalStorageCopyData): void {
    this.localPersistenceService
      .set(this.COPY_PERSISTENCE, localStorageCopyData)
      .subscribe(() => this.store.commit(localStorageCopyData));
  }

  synchroniseStateWithLocalStorage(): void {
    this.localPersistenceService
      .get<LocalStorageCopyData>(this.COPY_PERSISTENCE)
      .pipe(filter((data) => data != null))
      .subscribe((data) => this.store.commit(data));
  }
}

function getDefaultLocalStorageCopyData() {
  return {
    copiedNodes: [],
    whiteListContent: [],
  };
}

export interface LocalStorageCopyData {
  copiedNodes: DataRow[];
  whiteListContent: string[];
}
