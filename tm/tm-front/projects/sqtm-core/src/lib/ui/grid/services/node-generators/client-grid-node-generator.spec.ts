import { DataRow, placeholderDataRowId, placeholderDataRowType } from '../../model/data-row.model';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { GRID_NODE_GENERATOR } from '../../token';
import { GridNodeGenerator } from './grid-node-generator';
import { gridNodeGeneratorFactory } from '../../grid.service.provider';
import { applyFilters, filterAndSortRows, flatTree } from './client-grid-node-generator';
import { ColumnDefinition, Sort } from '../../model/column-definition.model';
import { extractRootIds } from '../data-row-loaders/data-row-utils';
import { ColumnState, initialColumnState } from '../../model/state/column.state';
import { DataRowState } from '../../model/state/datarow.state';
import { createEntityAdapter } from '@ngrx/entity';
import { FilterState, initialFilterState } from '../../model/state/filter.state';
import { Identifier } from '../../../../model/entity.model';
import { createColumn } from '../../grid-testing/grid-testing-utils';
import { initialGridState } from '../../model/state/grid.state';
import { GridNodeState } from '../../model/grid-node.model';
import { initialUiState } from '../../model/state/ui.state';
import apply = Reflect.apply;
import { treeGrid } from '../../model/grid-builders';
import { convertSqtmLiterals } from '../../model/data-row.type';
import { DataRowOpenState } from '../../../../model/grids/data-row.model';

describe('Client Grid Node Generator', () => {
  const gridConfig = treeGrid('grid-test').build();
  const gridNodeGenerator: GridNodeGenerator = apply(
    gridNodeGeneratorFactory(gridConfig),
    this,
    [],
  );

  beforeEach(function () {
    return TestBed.configureTestingModule({
      providers: [
        {
          provide: GRID_NODE_GENERATOR,
          useValue: gridNodeGenerator,
        },
      ],
    });
  });

  it('should create transformer', function () {
    const generator: GridNodeGenerator = TestBed.inject(GRID_NODE_GENERATOR);
    expect(generator).toBeTruthy();
  });

  it('should flat tree with only two libraries', function () {
    const dataRowState = getDataRowState(getDataRows());
    const filterState = { ...initialFilterState() };
    const gridNodes = flatTree(dataRowState, initialColumnState(), filterState, initialUiState());
    expect(gridNodes.length).toEqual(2);
    let lib = gridNodes[0];
    expect(lib.depth).toEqual(0);
    expect(lib.index).toEqual(0);

    lib = gridNodes[1];
    expect(lib.depth).toEqual(0);
    expect(lib.index).toEqual(1);
  });

  it('should flat tree with all rows opened', function () {
    const dataRowState = getDataRowState(getDataRows());
    Object.values(dataRowState.entities).forEach((dataRow) => {
      const state = dataRow.state;
      if (state === DataRowOpenState.closed) {
        dataRow.state = DataRowOpenState.open;
      }
    });
    const filterState = { ...initialFilterState() };
    const gridNodes = flatTree(dataRowState, initialColumnState(), filterState, initialUiState());
    expect(gridNodes.length).toEqual(14);
    expect(gridNodes.map((node) => [node.id, node.index, node.depth])).toEqual([
      // @formatter:off
      ['TestCaseLibrary-1', 0, 0],
      ['TestCase-12', 1, 1],
      ['TestCaseFolder-1', 2, 1],
      ['TestCaseFolder-3', 3, 2],
      ['TestCaseFolder-7', 4, 3],
      ['TestCase-9', 5, 4],
      ['TestCase-8', 6, 3],
      ['TestCase-4', 7, 2],
      ['TestCase-5', 8, 2],
      ['TestCaseFolder-6', 9, 2],
      ['TestCaseFolder-2', 10, 1],
      ['TestCaseLibrary-2', 11, 0],
      ['TestCase-10', 12, 1],
      ['TestCaseFolder-11', 13, 1],
      // @formatter:on
    ]);
  });

  it('should flat tree with all rows flat', function () {
    const dataRowState = getDataRowState(getDataRows());
    dataRowState.rootRowIds = [];
    Object.values(dataRowState.entities).forEach((dataRow) => {
      const state = dataRow.state;
      if (state === DataRowOpenState.closed) {
        dataRow.state = DataRowOpenState.leaf;
      }
      dataRow.children = [];
      dataRow.parentRowId = null;
      dataRowState.rootRowIds.push(dataRow.id);
    });
    const filterState = { ...initialFilterState() };
    const gridNodes = flatTree(dataRowState, initialColumnState(), filterState, initialUiState());
    expect(gridNodes.length).toEqual(14);

    const expected = Object.values(dataRowState.entities).reduce((array, dataRow, index) => {
      array.push([dataRow.id, index, 0]);
      return array;
    }, []);

    expect(gridNodes.map((row) => [row.id, row.index, row.depth])).toEqual(expected);
  });

  it('should sort rows asc on single string attribute', function () {
    const dataRowState = getDataRowState(getDataRows());
    const columnState: ColumnState = {
      ...initialColumnState(),
      ids: ['name'],
      entities: { name: createColumn('name', true) },
      sortedColumns: [{ id: 'name', sort: Sort.ASC }],
    };

    const sortedRows = filterAndSortRows(dataRowState, columnState, initialFilterState());
    expect(sortedRows.length).toEqual(14);
    expect(sortedRows.map((row) => row.id)).toEqual([
      'TestCaseLibrary-2',
      'TestCase-9',
      'TestCase-4',
      'TestCase-12',
      'TestCaseFolder-3',
      'TestCaseFolder-7',
      'TestCaseFolder-6',
      'TestCaseFolder-1',
      'TestCaseLibrary-1',
      'TestCaseFolder-2',
      'TestCase-5',
      'TestCase-10',
      'TestCase-8',
      'TestCaseFolder-11',
    ]);
  });

  it('should sort rows on root order if no sort is provided', function () {
    const dataRowState = getDataRowState(getDataRows());
    dataRowState.rootRowIds = [];
    Object.values(dataRowState.entities).forEach((row) => dataRowState.rootRowIds.push(row.id));
    const columnState: ColumnState = initialColumnState();

    const sortedRows = filterAndSortRows(dataRowState, columnState, initialFilterState());
    expect(sortedRows.length).toEqual(14);
    expect(sortedRows.map((row) => row.id)).toEqual(dataRowState.rootRowIds);
  });

  it('should sort rows asc on single number attribute', function () {
    const dataRowSate = getDataRowState(getDataRows());
    const columnState: ColumnState = {
      ...initialColumnState(),
      ids: ['num'],
      entities: { num: createColumn('num', true) },
      sortedColumns: [{ id: 'num', sort: Sort.ASC }],
    };

    const sortedRows = filterAndSortRows(dataRowSate, columnState, initialFilterState());
    expect(sortedRows.length).toEqual(14);
    expect(sortedRows.map((row) => [row.data.num, row.id])).toEqual([
      [undefined, 'TestCaseLibrary-1'],
      [NaN, 'TestCaseFolder-2'],
      [null, 'TestCase-12'],
      [-12, 'TestCaseFolder-3'],
      [-1, 'TestCaseFolder-6'],
      [0, 'TestCase-8'],
      [1, 'TestCase-5'],
      [3, 'TestCase-4'],
      [5, 'TestCaseFolder-7'],
      [7, 'TestCaseFolder-1'],
      [9, 'TestCaseFolder-11'],
      [45, 'TestCase-9'],
      [78, 'TestCaseLibrary-2'],
      [1000, 'TestCase-10'],
    ]);
  });

  it('should sort rows desc on single string attribute with a custom sort function', function () {
    const dataRowSate = getDataRowState(getDataRows());
    const columnState: ColumnState = {
      ...initialColumnState(),
      ids: ['name'],
      entities: {
        name: {
          ...createColumn('name', true),
          sortFunction: (valueA: string, valueB: string) => {
            return valueA.length - valueB.length;
          },
        },
      },
      sortedColumns: [{ id: 'name', sort: Sort.DESC }],
    };

    const sortedRows = filterAndSortRows(dataRowSate, columnState, initialFilterState());
    expect(sortedRows.map((row) => row.id)).toEqual([
      'TestCase-8',
      'TestCase-9',
      'TestCase-5',
      'TestCaseFolder-7',
      'TestCaseFolder-1',
      'TestCase-10',
      'TestCase-12',
      'TestCaseLibrary-1',
      'TestCaseFolder-2',
      'TestCaseFolder-3',
      'TestCaseFolder-6',
      'TestCase-4',
      'TestCaseLibrary-2',
      'TestCaseFolder-11',
    ]);
  });

  it('should sort rows on multiple attribute', function () {
    const dataRowSate = getDataRowState(getDataRows());
    let columnState: ColumnState = {
      ...initialColumnState(),
      ids: [],
      entities: {},
      sortedColumns: [
        { id: 'reference', sort: Sort.DESC },
        { id: 'name', sort: Sort.ASC },
      ],
    };
    columnState = createEntityAdapter<ColumnDefinition>().setAll(
      [createColumn('name', true), createColumn('reference', true)],
      columnState,
    );
    const sortedRows = filterAndSortRows(dataRowSate, columnState, initialFilterState());
    expect(sortedRows.length).toEqual(14);
    expect(sortedRows.map((row) => [row.data.reference, row.data.name, row.id])).toEqual([
      ['T4', 'A-TNR', 'TestCaseLibrary-2'],
      ['B', 'zzz', 'TestCaseFolder-11'],
      ['A', 'Project-1', 'TestCaseLibrary-1'],
      [undefined, "Hibernate shouldn't make n + 1 !!!", 'TestCase-9'],
      [undefined, 'Login', 'TestCase-4'],
      [null, 'Login test', 'TestCase-12'],
      [undefined, 'Page 1', 'TestCaseFolder-3'],
      [undefined, 'Page 1 Tab 1', 'TestCaseFolder-7'],
      [undefined, 'Page 2', 'TestCaseFolder-6'],
      [undefined, 'Performance', 'TestCaseFolder-1'],
      [undefined, 'Security', 'TestCaseFolder-2'],
      ['', 'Show in 2 seconds', 'TestCase-5'],
      [undefined, 'TestCase 10', 'TestCase-10'],
      [undefined, 'This report should not make the server hang', 'TestCase-8'],
    ]);
  });

  it('should flat a simple flat tree and sort rows asc on single number attribute', function () {
    const dataRowState = getDataRowState(getDataRows());
    dataRowState.rootRowIds = [];
    Object.values(dataRowState.entities).forEach((dataRow: DataRow) => {
      const state = dataRow.state;
      if (state === DataRowOpenState.closed) {
        dataRow.state = DataRowOpenState.leaf;
      }
      dataRow.children = [];
      dataRow.parentRowId = null;
      dataRowState.rootRowIds.push(dataRow.id);
    });
    const columnState: ColumnState = {
      ...initialColumnState(),
      ids: ['num'],
      entities: { num: createColumn('num', true) },
      sortedColumns: [{ id: 'num', sort: Sort.ASC }],
    };

    const filterState = { ...initialFilterState() };
    const sortedRows = flatTree(dataRowState, columnState, filterState, initialUiState());
    expect(sortedRows.length).toEqual(14);
    expect(sortedRows.map((row) => [dataRowState.entities[row.id].data['num'], row.id])).toEqual([
      [undefined, 'TestCaseLibrary-1'],
      [NaN, 'TestCaseFolder-2'],
      [null, 'TestCase-12'],
      [-12, 'TestCaseFolder-3'],
      [-1, 'TestCaseFolder-6'],
      [0, 'TestCase-8'],
      [1, 'TestCase-5'],
      [3, 'TestCase-4'],
      [5, 'TestCaseFolder-7'],
      [7, 'TestCaseFolder-1'],
      [9, 'TestCaseFolder-11'],
      [45, 'TestCase-9'],
      [78, 'TestCaseLibrary-2'],
      [1000, 'TestCase-10'],
    ]);
  });

  it('should flat a complex tree and sort rows desc on single number attribute', function () {
    const dataRowState = getDataRowState(getDataRows());
    Object.values(dataRowState.entities).forEach((dataRow: DataRow) => {
      const state = dataRow.state;
      if (state === DataRowOpenState.closed) {
        dataRow.state = DataRowOpenState.open;
      }
    });
    const columnState: ColumnState = {
      ...initialColumnState(),
      ids: ['num'],
      entities: { num: createColumn('num', true) },
      sortedColumns: [{ id: 'num', sort: Sort.DESC }],
    };
    const filterState = { ...initialFilterState() };
    const gridNode = flatTree(dataRowState, columnState, filterState, initialUiState());
    expect(gridNode.length).toEqual(14);
    expect(gridNode.map((node) => [node.id, node.index, node.depth])).toEqual([
      // @formatter:off
      ['TestCaseLibrary-2', 0, 0],
      ['TestCase-10', 1, 1],
      ['TestCaseFolder-11', 2, 1],
      ['TestCaseLibrary-1', 3, 0],
      ['TestCaseFolder-1', 4, 1],
      ['TestCase-4', 5, 2],
      ['TestCase-5', 6, 2],
      ['TestCaseFolder-6', 7, 2],
      ['TestCaseFolder-3', 8, 2],
      ['TestCaseFolder-7', 9, 3],
      ['TestCase-9', 10, 4],
      ['TestCase-8', 11, 3],
      ['TestCase-12', 12, 1],
      ['TestCaseFolder-2', 13, 1],
    ]);
    // @formatter:on
  });

  it('should flat a complex tree and sort rows on multiple attributes', function () {
    const dataRowState = getDataRowState(getDataRowsMultipleAttributeTesting());
    Object.values(dataRowState.entities).forEach((dataRow) => {
      const state = dataRow.state;
      if (state === DataRowOpenState.closed) {
        dataRow.state = DataRowOpenState.open;
      }
    });
    let columnState: ColumnState = {
      ...initialColumnState(),
      ids: [],
      entities: {},
      sortedColumns: [
        { id: 'weight', sort: Sort.DESC },
        { id: 'name', sort: Sort.ASC },
      ],
    };

    columnState = createEntityAdapter<ColumnDefinition>().setAll(
      [createColumn('name', true), createColumn('weight', true)],
      columnState,
    );
    const filterState = { ...initialFilterState() };
    const gridNodes = flatTree(dataRowState, columnState, filterState, initialUiState());
    expect(gridNodes.length).toEqual(7);
    expect(gridNodes.map((node) => [node.id, node.index, node.depth])).toEqual([
      ['TestCaseLibrary-1', 0, 0],
      ['TestCaseFolder-1', 1, 1],
      ['TestCase-4', 2, 2],
      ['TestCase-5', 3, 2],
      ['TestCase-3', 4, 2],
      ['TestCase-1', 5, 2],
      ['TestCase-2', 6, 2],
    ]);
  });

  it('should append a specific grid node if no data must be shown', waitForAsync(function () {
    const generator: GridNodeGenerator = TestBed.inject(GRID_NODE_GENERATOR);
    const gridState = { ...initialGridState() };
    generator.computeNodeTree(gridState).subscribe((state) => {
      expect(state.nodesState.ids.length).toEqual(1);
      const id = state.nodesState.ids[0];
      expect(id).toEqual(placeholderDataRowId);
      const node: GridNodeState = state.nodesState.entities[id];
      expect(node.dataRow.type).toEqual(placeholderDataRowType);
    });
  }));

  describe(`Filters`, () => {
    interface DataType {
      columnState: ColumnState;
      filterState: FilterState;
      dataRowState: DataRowState;
      expectedDataRowIds: Identifier[];
    }

    const columnState = {
      ...initialColumnState(),
      ids: ['name'],
      entities: { name: createColumn('name', true) },
      sortedColumns: [{ id: 'name', sort: Sort.ASC }],
    };

    const dataSets: DataType[] = [
      {
        columnState: columnState,
        filterState: initialFilterState(),
        dataRowState: getDataRowState(getDataRows()),
        expectedDataRowIds: getDataRows().map((row) => row.id),
      },
      {
        columnState: columnState,
        filterState: {
          ...initialFilterState(),
          filters: {
            ids: ['name'],
            entities: {
              name: {
                id: 'name',
                value: { kind: 'single-string-value', value: 'Login' },
                i18nLabelKey: '',
                active: true,
                initialValue: { kind: 'single-string-value', value: 'Login' },
                tiedToPerimeter: true,
              },
            },
            matchingRowIds: ['TestCase-12', 'TestCase-4'],
            ancestorMatchingRowIds: [],
          },
        },
        dataRowState: getDataRowState(getDataRows()),
        expectedDataRowIds: ['TestCase-12', 'TestCase-4'],
      },
    ];

    dataSets.forEach((data, index) => runTest(data, index));

    function runTest(data: DataType, index: number) {
      it(`Dataset ${index} - It should filter with filters : ${JSON.stringify(data.filterState.filters.entities)}`, () => {
        const result = applyFilters(data.dataRowState, data.columnState, data.filterState);
        expect(result.map((row) => row.id)).toEqual(data.expectedDataRowIds);
      });
    }
  });

  function getDataRows(): DataRow[] {
    const tcl1 = {
      id: 'TestCaseLibrary-1',
      state: DataRowOpenState.closed,
      children: ['TestCase-12', 'TestCaseFolder-1', 'TestCaseFolder-2'],
      data: {
        name: 'Project-1',
        reference: 'A',
      },
    };

    const tcl2 = {
      id: 'TestCaseLibrary-2',
      state: DataRowOpenState.closed,
      children: ['TestCase-10', 'TestCaseFolder-11'],
      data: {
        name: 'A-TNR',
        reference: 'T4',
        num: 78,
      },
    };

    const tcf1 = {
      id: 'TestCaseFolder-1',
      state: DataRowOpenState.closed,
      parentRowId: 'TestCaseLibrary-1',
      children: ['TestCaseFolder-3', 'TestCase-4', 'TestCase-5', 'TestCaseFolder-6'],
      data: {
        name: 'Performance',
        num: 7,
      },
    };

    const tcf2 = {
      id: 'TestCaseFolder-2',
      state: DataRowOpenState.closed,
      parentRowId: 'TestCaseLibrary-1',
      children: [],
      data: {
        name: 'Security',
        num: NaN,
      },
    };

    const tcf3 = {
      id: 'TestCaseFolder-3',
      state: DataRowOpenState.closed,
      parentRowId: 'TestCaseFolder-1',
      children: ['TestCaseFolder-7', 'TestCase-8'],
      data: {
        name: 'Page 1',
        num: -12,
      },
    };

    const tcf6 = {
      id: 'TestCaseFolder-6',
      state: DataRowOpenState.closed,
      parentRowId: 'TestCaseFolder-1',
      children: [],
      data: {
        name: 'Page 2',
        num: -1,
      },
    };

    const tcf7 = {
      id: 'TestCaseFolder-7',
      state: DataRowOpenState.closed,
      parentRowId: 'TestCaseFolder-3',
      children: ['TestCase-9'],
      data: {
        name: 'Page 1 Tab 1',
        num: 5,
      },
    };

    const tcf11 = {
      id: 'TestCaseFolder-11',
      state: DataRowOpenState.closed,
      parentRowId: 'TestCaseLibrary-2',
      children: [],
      data: {
        name: 'zzz',
        reference: 'B',
        num: 9,
      },
    };

    const tc12 = {
      id: 'TestCase-12',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseLibrary-1',
      children: [],
      data: {
        name: 'Login test',
        reference: null,
        num: null,
      },
    };

    const tc4 = {
      id: 'TestCase-4',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseFolder-1',
      children: [],
      data: {
        name: 'Login',
        num: 3,
      },
    };

    const tc5 = {
      id: 'TestCase-5',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseFolder-1',
      children: [],
      data: {
        name: 'Show in 2 seconds',
        reference: '',
        num: 1,
      },
    };

    const tc8 = {
      id: 'TestCase-8',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseFolder-3',
      children: [],
      data: {
        name: 'This report should not make the server hang',
        num: 0,
      },
    };

    const tc9 = {
      id: 'TestCase-9',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseFolder-7',
      children: [],
      data: {
        name: "Hibernate shouldn't make n + 1 !!!",
        num: 45,
      },
    };

    const tc10 = {
      id: 'TestCase-10',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseLibrary-2',
      children: [],
      data: {
        name: 'TestCase 10',
        num: 1000,
      },
    };

    return convertSqtmLiterals(
      [tcl1, tcf1, tcf2, tcf3, tcf6, tcf7, tc12, tc4, tc5, tcl2, tc8, tc9, tc10, tcf11],
      [],
    );
  }

  function getDataRowsMultipleAttributeTesting(): DataRow[] {
    const tcl1 = {
      id: 'TestCaseLibrary-1',
      state: DataRowOpenState.closed,
      children: ['TestCaseFolder-1'],
      data: {
        name: 'Project-1',
      },
    };

    const tcf1 = {
      id: 'TestCaseFolder-1',
      state: DataRowOpenState.closed,
      parentRowId: 'TestCaseLibrary-1',
      children: ['TestCase-1', 'TestCase-2', 'TestCase-3', 'TestCase-4', 'TestCase-5'],
      data: {
        name: 'TC Folder',
      },
    };

    const tc1 = {
      id: 'TestCase-1',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseFolder-1',
      children: [],
      data: {
        name: 'Show in 2 seconds',
        weight: 1,
      },
    };

    const tc2 = {
      id: 'TestCase-2',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseFolder-1',
      children: [],
      data: {
        name: 'Tutu',
        weight: 1,
      },
    };

    const tc3 = {
      id: 'TestCase-3',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseFolder-1',
      children: [],
      data: {
        name: 'Ahhh',
        weight: 1,
      },
    };

    const tc4 = {
      id: 'TestCase-4',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseFolder-1',
      children: [],
      data: {
        name: 'TC-4',
        weight: 2,
      },
    };

    const tc5 = {
      id: 'TestCase-5',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseFolder-1',
      children: [],
      data: {
        name: 'Three in a row',
        weight: 2,
      },
    };

    return convertSqtmLiterals([tcl1, tcf1, tc1, tc2, tc3, tc4, tc5], []);
  }

  function getDataRowState(datarows: DataRow[]): DataRowState {
    const adapter = createEntityAdapter<DataRow>();
    const initialState = {
      ids: [],
      entities: {},
      selectedRows: [],
      rootRowIds: extractRootIds(Array.from(datarows)),
      count: datarows.length,
      lastToggledRowId: null,
    };
    return adapter.setAll(datarows, initialState);
  }
});
