import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { IndexCellRendererComponent } from './index-cell-renderer.component';
import { GridDefinition } from '../../../model/grid-definition.model';
import { RestService } from '../../../../../core/services/rest.service';
import { GridService } from '../../../services/grid.service';
import { gridServiceFactory } from '../../../grid.service.provider';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { grid } from '../../../model/grid-builders';
import { DataRow } from '../../../model/data-row.model';
import { ReferentialDataService } from '../../../../../core/referential/services/referential-data.service';

describe('IndexCellRendererComponent', () => {
  let component: IndexCellRendererComponent;
  let fixture: ComponentFixture<IndexCellRendererComponent>;

  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [IndexCellRendererComponent],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig,
        },
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService],
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexCellRendererComponent);
    component = fixture.componentInstance;
    component.row = { id: 1 } as DataRow;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
