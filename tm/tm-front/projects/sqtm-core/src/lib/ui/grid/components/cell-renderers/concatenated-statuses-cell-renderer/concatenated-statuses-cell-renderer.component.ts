import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { AbstractCellRendererComponent } from '../abstract-cell-renderer/abstract-cell-renderer.component';
import { GridService } from '../../../services/grid.service';
import { TranslateService } from '@ngx-translate/core';
import { ColumnDisplay } from '../../../model/column-display.model';
import { GridDisplay } from '../../../model/grid-display.model';
import { DataRow } from '../../../model/data-row.model';
import { GridType } from '../../../model/grid-definition.model';

@Component({
  selector: 'sqtm-core-concatenated-statuses-cell-renderer',
  template: `
    @if (columnDisplay && row) {
      <div class="full-width full-height flex-column" style="justify-content: center;">
        @if (row.data[columnDisplay.id]) {
          <span
            class="sqtm-grid-cell-txt-renderer"
            nz-tooltip
            [sqtmCoreLabelTooltip]="getToolTipText(columnDisplay, gridDisplay, row)"
          >
            {{ milestoneStatus }}
          </span>
        } @else {
          <span class="sqtm-grid-cell-txt-renderer">-</span>
        }
      </div>
    }
  `,
  styleUrls: ['./concatenated-statuses-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConcatenatedStatusesCellRendererComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    public translateService: TranslateService,
  ) {
    super(grid, cdRef);
  }

  get milestoneStatus(): string {
    const statuses = this.row.data[this.columnDisplay.id];
    return this.translateConcatenatedStatuses(statuses);
  }

  getToolTipText(_columnDisplay: ColumnDisplay, gridDisplay: GridDisplay, _row: DataRow) {
    if (gridDisplay.gridType !== GridType.TREE) {
      const statuses = this.row.data[this.columnDisplay.id];
      return this.translateConcatenatedStatuses(statuses);
    } else {
      return '';
    }
  }

  private translateConcatenatedStatuses(concatenatedStatuses: string): string {
    const statuses = concatenatedStatuses
      .split(', ')
      .map((stat) => this.translateService.instant('sqtm-core.entity.milestone.status.' + stat));
    return statuses.join(', ');
  }
}
