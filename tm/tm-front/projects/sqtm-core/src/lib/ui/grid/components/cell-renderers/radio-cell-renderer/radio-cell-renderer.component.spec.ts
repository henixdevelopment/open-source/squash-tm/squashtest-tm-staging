import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RadioCellRendererComponent } from './radio-cell-renderer.component';
import { grid } from '../../../model/grid-builders';
import { GridDefinition } from '../../../model/grid-definition.model';
import { GridService } from '../../../services/grid.service';
import { gridServiceFactory } from '../../../grid.service.provider';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ReferentialDataService } from '../../../../../core/referential/services/referential-data.service';
import { RestService } from '../../../../../core/services/rest.service';

describe('RadioCellRendererComponent', () => {
  let component: RadioCellRendererComponent;
  let fixture: ComponentFixture<RadioCellRendererComponent>;

  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [RadioCellRendererComponent],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig,
        },
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService],
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioCellRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
