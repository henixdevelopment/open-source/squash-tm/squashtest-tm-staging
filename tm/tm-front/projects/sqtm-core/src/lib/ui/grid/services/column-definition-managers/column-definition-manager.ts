import { ColumnDefinition, SortedColumn } from '../../model/column-definition.model';
import { GridState } from '../../model/state/grid.state';
import { Identifier } from '../../../../model/entity.model';
import { MoveColumnAction } from '../../model/actions/move-column.action';
import { Observable } from 'rxjs';
import { BindableEntity } from '../../../../model/bindable-entity.model';
import { GridViewportName } from '../../../../model/grids/grid-viewport-name';
import { ListItem } from '../../../workspace-common/components/forms/grouped-multi-list/grouped-multi-list.component';

export interface ColumnDefinitionManager {
  initializeColumns(
    columnDefinitions: ColumnDefinition[],
    state: GridState,
    initialSortedColumns: SortedColumn[],
    bindableEntity?: BindableEntity,
    shouldResetSorts?: boolean,
  ): Observable<GridState>;

  addColumns(
    columnDefinitions: ColumnDefinition[],
    viewport: GridViewportName,
    state: GridState,
  ): Observable<GridState>;

  toggleSortColumn(id: Identifier, state: GridState): Observable<GridState>;

  setSortColumn(sortedColumns: SortedColumn[], state: GridState): Observable<GridState>;

  toggleColumnVisibility(id: Identifier, state: GridState): Observable<GridState>;

  setColumnVisibility(show: boolean, id: Identifier, state: GridState): Observable<GridState>;
  setColumnsVisibility(updatedItems: ListItem[], state: GridState): Observable<GridState>;

  moveColumn(moveColumn: MoveColumnAction, state: GridState): Observable<GridState>;

  resizeColumn(id: Identifier, offset: number, state: GridState): Observable<GridState>;

  addColumnAtIndex(
    newColumns: ColumnDefinition[],
    state: GridState,
    index?: number,
  ): Observable<GridState>;

  removeColumn(columnIds: Identifier[], state: GridState): Observable<GridState>;

  renameColumn(columnId: Identifier, state: GridState, newLabel: string): Observable<GridState>;
}
