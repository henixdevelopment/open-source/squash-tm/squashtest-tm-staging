import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { BehaviorSubject, combineLatest, Observable, Subject, Subscription } from 'rxjs';
import {
  GroupedMultiListComponent,
  ListItem,
} from '../../../../workspace-common/components/forms/grouped-multi-list/grouped-multi-list.component';
import { filter, map, switchMap, take, takeUntil, tap, withLatestFrom } from 'rxjs/operators';
import { GridService } from '../../../services/grid.service';
import { GridState } from '../../../model/state/grid.state';
import { TranslateService } from '@ngx-translate/core';
import {
  getBasicColumnIdsForITPISearch,
  getBasicColumnIdsForRequirementSearch,
  getBasicColumnIdsForTestCaseSearch,
  GridColumnId,
} from '../../../../../shared/constants/grid/grid-column-id';
import { ColumnWithFilter } from '../../../model/column-display.model';
import { ReferentialDataService } from '../../../../../core/referential/services/referential-data.service';
import { GridId, isSearchGrid } from '../../../../../shared/constants/grid/grid-id';
import { ReferentialDataState } from '../../../../../core/referential/state/referential-data.state';

@Component({
  selector: 'sqtm-core-custom-columns',
  templateUrl: './custom-columns.component.html',
  styleUrls: ['./custom-columns.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomColumnsComponent extends GroupedMultiListComponent implements OnInit, OnDestroy {
  private unsub$ = new Subject<void>();
  private _multiListDataSubject = new BehaviorSubject<ListItem[]>([]);
  multiListData$: Observable<ListItem[]> = new Subject<ListItem[]>();
  private originalListItems: ListItem[] = [];

  private observeMilestoneSubscription: Subscription;

  private milestoneFeatureEnabled: boolean;

  @Output()
  confirmClicked = new EventEmitter<void>();

  @Input()
  projectId?: number;
  showButton = false;
  activateButton = true;

  isResettingConfiguration$ = new BehaviorSubject(false);

  constructor(
    protected referentialDataService: ReferentialDataService,
    protected translateService: TranslateService,
    protected cdRef: ChangeDetectorRef,
    private gridService: GridService,
  ) {
    super(translateService, cdRef);
    this.multiListData$ = this._multiListDataSubject.asObservable();
  }

  ngOnInit() {
    this.observeMilestoneFeatureEnabled();
    combineLatest([this.gridService.gridState$, this.gridService.columns$])
      .pipe(
        take(1),
        switchMap(([gridState, columns]: [GridState, ColumnWithFilter[]]) => {
          const definitionStateId = gridState.definitionState.id;
          return this.transformColumns(definitionStateId, columns);
        }),
        tap((data: ListItem[]) => {
          this.originalListItems = [...data];
          this._multiListDataSubject.next(data);
        }),
      )
      .subscribe();
  }

  private filterColumnsByGridState(
    columns: ColumnWithFilter[],
    gridStateId: string,
  ): ColumnWithFilter[] {
    const excludedColumns: Record<string, GridColumnId[]> = {
      [GridId.CAMPAIGN_TEST_PLAN]: [GridColumnId.testCaseName],
      [GridId.ITERATION_TEST_PLAN]: [
        GridColumnId.testCaseName,
        GridColumnId.executionStatus,
        GridColumnId.lastExecutedOn,
      ],
      [GridId.TEST_SUITE_TEST_PLAN]: [
        GridColumnId.testCaseName,
        GridColumnId.executionStatus,
        GridColumnId.lastExecutedOn,
      ],
      [GridId.ITERATION_AUTOMATED_SUITE]: [
        GridColumnId.createdOn,
        GridColumnId.executionStatus,
        GridColumnId.executionDetails,
        GridColumnId.executionReport,
      ],
      [GridId.TEST_SUITE_AUTOMATED_SUITE]: [
        GridColumnId.createdOn,
        GridColumnId.executionStatus,
        GridColumnId.executionDetails,
        GridColumnId.executionReport,
      ],
      [GridId.ITPI_SEARCH]: [GridColumnId.label, GridColumnId.executionStatus],
      [GridId.REQUIREMENT_SEARCH]: [GridColumnId.name, GridColumnId.reqVersionBoundToItem],
      [GridId.TEST_CASE_SEARCH]: [GridColumnId.name, GridColumnId.testCaseBoundToItem],
      [GridId.TC_BY_REQUIREMENT]: [GridColumnId.name, GridColumnId.testCaseBoundToItem],
    };

    const gridState: GridId = gridStateId as GridId;

    return columns.filter(
      (column: ColumnWithFilter) =>
        column.i18nKey && !excludedColumns[gridState].includes(<GridColumnId>column.id),
    );
  }

  private transformColumns(
    definitionStateId: string,
    columns: ColumnWithFilter[],
  ): Observable<ListItem[]> {
    return this.gridService.getActiveColumnsIds(definitionStateId, this.projectId).pipe(
      map((activeColumnIds: string[]) => {
        if (activeColumnIds.length !== 0) {
          this.showButton = true;
        }
        const filteredColumns = this.filterColumnsByGridState(columns, definitionStateId);
        const transformedColumns = filteredColumns.map((column) => ({
          ...column,
          id: column.id,
          i18nLabelKey: column.titleI18nKey ? column.titleI18nKey : column.i18nKey,
          selected: column.show,
        }));
        if (!this.milestoneFeatureEnabled) {
          return transformedColumns.filter(
            (column) =>
              column.id !== GridColumnId.milestones &&
              column.id !== GridColumnId.milestoneLabels &&
              column.id !== GridColumnId.milestoneStatus &&
              column.id !== GridColumnId.milestoneEndDate,
          );
        }
        return transformedColumns as ListItem[];
      }),
    );
  }

  private observeMilestoneFeatureEnabled(): void {
    this.observeMilestoneSubscription = this.referentialDataService.globalConfiguration$
      .pipe(
        takeUntil(this.unsub$),
        map((configuration) => configuration.milestoneFeatureEnabled),
      )
      .subscribe((milestoneFeatureEnabled) => {
        this.milestoneFeatureEnabled = milestoneFeatureEnabled;
      });
  }

  toggleSelectionForColumnsOverlay(updatedItem: ListItem) {
    this.multiListData$.pipe(take(1)).subscribe((listItems) => {
      const updatedListItems = listItems.map((listItem) => {
        return listItem.id === updatedItem.id ? updatedItem : listItem;
      });
      this._multiListDataSubject.next(updatedListItems);
    });
  }

  ngOnDestroy(): void {
    if (this.observeMilestoneSubscription) {
      this.observeMilestoneSubscription.unsubscribe();
    }
    this.confirmClicked.complete();
    this._multiListDataSubject.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  confirm(updatedItems: ListItem[]) {
    const activeColumnIds: string[] = updatedItems
      .filter((item) => item.selected)
      .map((item) => item.id.toString());

    combineLatest([this.gridService.gridState$, this.referentialDataService.referentialData$])
      .pipe(
        take(1),
        switchMap(([gridState, refData]: [GridState, ReferentialDataState]) =>
          this.updateGridConfigurationAndVisibility(
            gridState,
            refData,
            updatedItems,
            activeColumnIds,
            this.projectId,
          ),
        ),
        takeUntil(this.unsub$),
      )
      .subscribe(([gridState, refData]: [GridState, ReferentialDataState]) => {
        if (!refData.globalConfigurationState.searchActivationFeatureEnabled) {
          this.removeSortOnColumnsToHide(activeColumnIds, gridState);
        }
        this.confirmClicked.emit();
      });
  }

  private removeSortOnColumnsToHide(activeColumnsIds: string[], gridState: GridState): void {
    const originalColumns = this.originalListItems.filter((column) => column.selected);
    const removedColumns = originalColumns.filter(
      (column) => !activeColumnsIds.includes(column.id.toString()),
    );
    const sortedColumns = gridState.columnState.sortedColumns;
    const columnsToSort = sortedColumns.filter(
      (sortedColumn) =>
        !removedColumns.includes(
          removedColumns.find((removedColumn) => removedColumn.id === sortedColumn.id),
        ),
    );
    this.gridService.setColumnSorts(columnsToSort);
  }

  updateGridColumnVisibility(updatedItems: ListItem[]) {
    return this.gridService.setColumnsVisibility(updatedItems);
  }

  resetColumnConfiguration() {
    this.gridService.gridState$
      .pipe(
        take(1),
        withLatestFrom(this.isResettingConfiguration$),
        filter(([, isResettingConfiguration]: [GridState, boolean]) => !isResettingConfiguration),
        tap(() => this.isResettingConfiguration$.next(true)),
        switchMap(([gridState]) =>
          this.gridService
            .resetColumnConfigurationServerSide(gridState.definitionState.id, this.projectId)
            .pipe(
              withLatestFrom(this.gridService.gridState$),
              switchMap(([, newGridState]) => {
                const updatedOriginalItems: ListItem[] =
                  this.findUpdatedOriginalItems(newGridState);
                return this.updateGridColumnVisibility(updatedOriginalItems).pipe(
                  map(() => [newGridState, updatedOriginalItems]),
                );
              }),
            ),
        ),
        withLatestFrom(this.referentialDataService.referentialData$),
        tap(
          ([[gridState, updatedOriginalItems], refData]: [
            [GridState, ListItem[]],
            ReferentialDataState,
          ]) => {
            const activeColumnIds: string[] = updatedOriginalItems
              .filter((item) => item.selected)
              .map((item) => item.id.toString());

            if (!refData.globalConfigurationState.searchActivationFeatureEnabled) {
              this.removeSortOnColumnsToHide(activeColumnIds, gridState);
            }
          },
        ),
      )
      .subscribe(() => {
        this.isResettingConfiguration$.next(false);
        this.confirmClicked.emit();
      });
  }

  private findUpdatedOriginalItems(gridState: GridState): ListItem[] {
    let basicColumns: string[] = [];
    const searchGrid = isSearchGrid(gridState.definitionState.id);

    switch (gridState.definitionState.id) {
      case GridId.REQUIREMENT_SEARCH:
        basicColumns = getBasicColumnIdsForRequirementSearch();
        break;

      case GridId.TEST_CASE_SEARCH:
      case GridId.TC_BY_REQUIREMENT:
        basicColumns = getBasicColumnIdsForTestCaseSearch();
        break;

      case GridId.ITPI_SEARCH:
        basicColumns = getBasicColumnIdsForITPISearch();
        break;

      default:
        break;
    }

    return this.originalListItems.map((item) => ({
      ...item,
      id: item.id,
      selected: !searchGrid || basicColumns.includes(item.id.toString()),
    }));
  }

  cancel() {
    this.confirmClicked.emit();
  }
  private updateGridConfigurationAndVisibility(
    gridState: GridState,
    refData: ReferentialDataState,
    updatedItems: ListItem[],
    activeColumnIds: string[],
    projectId: number,
  ): Observable<[GridState, ReferentialDataState]> {
    return this.gridService
      .updateGridColumnConfigurationServerSide(
        gridState.definitionState.id,
        activeColumnIds,
        projectId,
      )
      .pipe(
        switchMap(() => this.updateGridColumnVisibility(updatedItems)),
        map(() => [gridState, refData]),
      );
  }
}
