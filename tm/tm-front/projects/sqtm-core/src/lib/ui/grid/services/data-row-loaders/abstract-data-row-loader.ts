import { Observable, of } from 'rxjs';
import { DataRowLoader } from './data-row-loader';
import { DataRow } from '../../model/data-row.model';
import { RestService } from '../../../../core/services/rest.service';
import {
  areRowsSibling,
  extractRootIds,
  getAllSiblingIds,
  getAncestorIds,
  getDataRow,
  getDescendantIds,
  getDescendants,
  getLastSelectedRow,
  hasRowSelected,
} from './data-row-utils';
import { Identifier } from '../../../../model/entity.model';
import { DataRowState, initialDataRowState } from '../../model/state/datarow.state';
import { GridState } from '../../model/state/grid.state';
import { map, take } from 'rxjs/operators';
import { createEntityAdapter, Update } from '@ngrx/entity';
import { TableValueChange } from '../../model/actions/table-value-change';
import { RemoveRowResult } from './remove-row-result';
import { Store } from '../../../../core/store/store';
import { ProjectDataMap } from '../../../../model/project/project-data.model';
import { gridLogger } from '../../grid.logger';
import { DataRowOpenState } from '../../../../model/grids/data-row.model';

const logger = gridLogger.compose('AbstractDataRowLoader');

export abstract class AbstractDataRowLoader implements DataRowLoader {
  protected adapter = createEntityAdapter<DataRow>();
  protected store: Store<GridState>;

  protected constructor(protected restService: RestService) {}

  public loadInitialData(
    literals: Partial<DataRow>[],
    count: number,
    selectedRows: Identifier[],
    state: GridState,
    projectData: ProjectDataMap,
  ): Observable<GridState> {
    const dataRows = state.definitionState.literalDataRowConverter(literals, projectData);
    let dataRowState: DataRowState = {
      ...state.dataRowState,
      count,
      rootRowIds: extractRootIds(dataRows),
      selectedRows: [...selectedRows],
    };
    dataRowState = this.adapter.setAll(dataRows, dataRowState);
    return of({ ...state, dataRowState });
  }

  abstract refreshData(
    state: GridState,
    projectData: ProjectDataMap,
    keepSelectedRows: boolean,
  ): Observable<GridState>;

  public openRows(
    ids: Identifier[],
    state$: Observable<GridState>,
    _projectData: ProjectDataMap,
  ): Observable<GridState> {
    return state$.pipe(
      take(1),
      map((state) => {
        let dataRowState = { ...state.dataRowState };
        const updates: Update<any>[] = ids
          .map((id) => dataRowState.entities[id])
          .filter((dataRow) => dataRow && dataRow.state === DataRowOpenState.closed)
          .map((dataRow) => ({
            id: dataRow.id as any,
            changes: { state: DataRowOpenState.open },
          }));
        if (updates.length > 0) {
          dataRowState = this.adapter.updateMany(updates, state.dataRowState);
          return { ...state, dataRowState };
        } else {
          return state;
        }
      }),
    );
  }

  // todo we probably must remove all descendants for server backed grids
  //  to avoid stale rows in case of a modification occurs server side by other user
  public closeRows(ids: Identifier[], state: GridState): Observable<GridState> {
    let dataRowState = { ...state.dataRowState };
    const dataRows = ids.map((id) => state.dataRowState.entities[id]).filter((row) => Boolean(row));
    if (dataRows.length > 0) {
      const descendantIds = getDescendantIds(dataRowState, dataRows);
      const changes = [...descendantIds, ...ids]
        .filter(
          (rowId) =>
            dataRowState.entities[rowId] &&
            dataRowState.entities[rowId].state === DataRowOpenState.open,
        )
        .map((rowId) => ({
          id: rowId as any,
          changes: { state: DataRowOpenState.closed },
        }));
      dataRowState = this.adapter.updateMany(changes, dataRowState);
      return of({ ...state, dataRowState });
    } else {
      return of(state);
    }
  }

  public toggleRowSelection(id: Identifier, state: GridState): Observable<GridState> {
    return of(this.doToggleRowSelection(state, id));
  }

  addToSelection(id: Identifier, state: GridState): Observable<GridState> {
    // case 1 no selection exist -> single select
    // case 2 take the last selected one and
    // case 2-a the id represent a row not in the same container than last selection -> single selection
    // case 2-b the id represent a row in the same container -> select all rows between last selected and id
    if (!hasRowSelected(state)) {
      return this.selectSingleRow(id, state);
    }
    const previouslySelectedRows = state.dataRowState.selectedRows;
    const lastSelectedRow = getLastSelectedRow(state);
    const selectedRow = getDataRow(id, state);
    const targetWasPreviouslySelected = state.dataRowState.selectedRows.includes(id);
    if (areRowsSibling(lastSelectedRow, selectedRow)) {
      const siblingIds = getAllSiblingIds(id, state);
      const lastSelectedRowIndex = siblingIds.indexOf(lastSelectedRow.id);
      const selectedRowIndex = siblingIds.indexOf(selectedRow.id);
      const range = {
        min: Math.min(lastSelectedRowIndex, selectedRowIndex),
        max: Math.max(lastSelectedRowIndex, selectedRowIndex),
      };
      const selectedRowIds = siblingIds.slice(range.min, range.max + 1);
      let finalSelection: Identifier[];
      if (targetWasPreviouslySelected) {
        finalSelection = this.shrinkSelection(selectedRowIds, previouslySelectedRows);
      } else {
        // do not reselect last selected row if it was not selected already
        const filteredRowId = selectedRowIds.filter((rowId) => rowId !== lastSelectedRow.id);
        finalSelection = this.expendSelection(filteredRowId, previouslySelectedRows, state);
      }
      return of({
        ...state,
        dataRowState: { ...state.dataRowState, lastToggledRowId: id, selectedRows: finalSelection },
      });
    } else {
      return this.selectSingleRow(id, state);
    }
  }

  private expendSelection(
    selectedRowIds: Identifier[],
    previouslySelectedRows: Identifier[],
    state: GridState,
  ): Identifier[] {
    const selectedRows = selectedRowIds.map((id) => getDataRow(id, state));
    const descendantIds = getDescendantIds(state.dataRowState, selectedRows);
    const newSelectedRows = selectedRowIds.filter(
      (rowId) => !previouslySelectedRows.includes(rowId),
    );
    const filteredPreviouslySelectedRows = previouslySelectedRows.filter(
      (row) => !descendantIds.includes(row),
    );
    return [...filteredPreviouslySelectedRows, ...newSelectedRows];
  }

  private shrinkSelection(selectedRowIds: Identifier[], previouslySelectedRows: Identifier[]) {
    return previouslySelectedRows.filter((id) => !selectedRowIds.includes(id));
  }

  public selectSingleRow(id: Identifier, state: GridState): Observable<GridState> {
    const dataRowState = { ...state.dataRowState };
    dataRowState.selectedRows = [id];
    dataRowState.lastToggledRowId = id;
    return of({ ...state, dataRowState });
  }

  private doToggleRowSelection(state: GridState, id: Identifier): GridState {
    const dataRowState = { ...state.dataRowState };
    const selectedRows = [...dataRowState.selectedRows];

    // if row was previously selected, we just toggle it
    if (selectedRows.includes(id)) {
      selectedRows.splice(selectedRows.indexOf(id), 1);
      dataRowState.selectedRows = selectedRows;
    } else {
      this.selectRow(dataRowState, id);
    }
    dataRowState.lastToggledRowId = id;
    return { ...state, dataRowState };
  }

  // Take care, this method mutate datarow state, and thus should be kept private
  private selectRow(dataRowState: DataRowState, id: Identifier) {
    const selectedRows = [...dataRowState.selectedRows];
    // if we have already selected an ancestor of the selected row we must ignore the selection and do nothing.
    // else deselect all previously selected children and sub-children and select the given row.
    const selectedRow = dataRowState.entities[id];
    const ancestors = getAncestorIds(dataRowState, [selectedRow]);
    const intersection = selectedRows.filter((rowId) => ancestors.includes(rowId));
    // if the selection is not forbidden ie if no ancestor of selected row was previously selected we can proceed
    if (intersection.length === 0) {
      // The rules are : select the row. Deselect all children.
      const descendantIds: Identifier[] = getDescendantIds(dataRowState, [selectedRow]);
      descendantIds.forEach((descendantId) => {
        const index = selectedRows.indexOf(descendantId);
        if (index !== -1) {
          selectedRows.splice(index, 1);
        }
      });
      selectedRows.push(id);
      dataRowState.selectedRows = selectedRows;
    }
  }

  unselectAllRows(state: GridState): Observable<GridState> {
    const dataRowState = { ...state.dataRowState };
    dataRowState.selectedRows = [];
    dataRowState.lastToggledRowId = null;
    return of({ ...state, dataRowState });
  }

  addRows(
    dataRows: DataRow[],
    state: GridState,
    parentId?: Identifier,
    _position?: number,
  ): GridState {
    let dataRowState = { ...state.dataRowState };
    dataRows.forEach((dataRow) => (dataRow.parentRowId = parentId));
    dataRowState = this.adapter.addMany(dataRows, dataRowState);
    const parent = dataRowState.entities[parentId];
    const addedChildrenIds = dataRows.map((value) => value.id);
    const newChildrenIds = [...parent.children, ...addedChildrenIds];
    dataRowState = this.adapter.updateOne(
      {
        id: parentId as any,
        changes: {
          state:
            parent.state === DataRowOpenState.leaf
              ? DataRowOpenState.closed
              : DataRowOpenState.open,
          children: newChildrenIds,
          data: {
            ...parent.data,
            CHILD_COUNT: newChildrenIds.length,
          },
        },
      },
      dataRowState,
    );
    logger.debug('Parent State ' + JSON.stringify(dataRowState));
    return { ...state, dataRowState };
  }

  invertSelection(state: GridState): Observable<GridState> {
    const nodeState = { ...state.nodesState };
    const dataRowState = { ...state.dataRowState };
    const newSelectedRows = [];

    nodeState.ids.forEach((id) => {
      const index = dataRowState.selectedRows.indexOf(id);

      if (index === -1) {
        newSelectedRows.push(id);
      }
    });

    dataRowState.selectedRows = newSelectedRows;
    dataRowState.lastToggledRowId = null;
    return of({ ...state, dataRowState });
  }

  removeRows(removedRowIds: Identifier[], state: GridState): Observable<RemoveRowResult> {
    let dataRowState: DataRowState = { ...state.dataRowState };
    let removedRows = removedRowIds.map((id) => dataRowState.entities[id]);
    const childRowIds = removedRows.filter((row) => Boolean(row.parentRowId)).map((row) => row.id);
    const rootRowIds = removedRows.filter((row) => !row.parentRowId).map((row) => row.id);
    const nextRootRowState: Identifier[] = dataRowState.rootRowIds.filter(
      (rowId) => !rootRowIds.includes(rowId),
    );

    dataRowState = this.removeRowsFromParents(childRowIds, dataRowState);

    // now we can make the root rows of our selection with no parent id
    // useful if you needs the rows after the delete, typically for a move operation.
    removedRows = removedRows.map((row) => {
      return { ...row, parentRowId: undefined };
    });

    // now we get the descendant of or deleted rows and push them into the suppressed row array
    // without modify the hierarchy so we preserve the subtrees structure.
    const descendants = getDescendants(dataRowState, removedRows);
    removedRows.push(...descendants);

    // now we remove the rows
    const allIdsToRemove = removedRows.map((row) => row.id);
    dataRowState = this.adapter.removeMany(allIdsToRemove as any[], dataRowState);
    dataRowState.rootRowIds = nextRootRowState;
    return of({
      state: { ...state, dataRowState },
      removedRows,
    });
  }

  insertRows(
    insertedRows: DataRow[],
    targetId: Identifier,
    state: GridState,
    index?: number,
  ): Observable<GridState> {
    let dataRowState = { ...state.dataRowState };
    const target = dataRowState.entities[targetId];
    // extracting the ids of the roots of inserted rows.
    const rootInsertedRowIds = extractRootIds(insertedRows);

    // first we simply add the inserted rows to the map
    // no relationship is created at this stage, nether rows are added to rootRowIds
    dataRowState = this.adapter.addMany(insertedRows, dataRowState);

    // setting the parent id of root inserted subtree to targetId.
    insertedRows
      .filter((row) => rootInsertedRowIds.includes(row.id))
      .forEach((row) => (row.parentRowId = targetId));

    // if we have a valid target we must update it's state or it's children.
    if (target) {
      let openState = target.state;
      if (openState === DataRowOpenState.leaf) {
        openState = DataRowOpenState.closed;
      }

      dataRowState = this.adapter.updateOne(
        {
          id: targetId as any,
          changes: { state: openState },
        },
        dataRowState,
      );

      // normalize illegal values of index (undefined/null...)
      // in that case, new rows are added at the end of the container row.
      index = this.normalizeIndex(index, target.children.length);

      const nextChildrenState = [...target.children];
      nextChildrenState.splice(index, 0, ...rootInsertedRowIds);
      dataRowState = this.adapter.updateOne(
        {
          id: targetId as any,
          changes: {
            children: nextChildrenState,
            data: {
              CHILD_COUNT: nextChildrenState.length,
            },
            state: openState,
          },
        },
        dataRowState,
      );
    } else {
      // root dataRow, so we must update the roots rows array
      const rootRowsIds = dataRowState.rootRowIds;
      // normalize illegal values of index (undefined/null...)
      // in that case, new rows are added at the end of root rows
      index = this.normalizeIndex(index, rootRowsIds.length);
      const nextRootRowsIds = [...rootRowsIds];
      nextRootRowsIds.splice(index, 0, ...rootInsertedRowIds);
      dataRowState.rootRowIds = nextRootRowsIds;
    }

    return of({ ...state, dataRowState });
  }

  private normalizeIndex(index: number, length: number) {
    if (index == null || index >= length) {
      index = length;
    } else if (index < 0) {
      index = 0;
    }
    return index;
  }

  private removeRowsFromParents(removedRowIds: Identifier[], state: DataRowState) {
    const removedRows = removedRowIds.map((id) => state.entities[id]);
    const changes: Update<DataRow>[] = [];

    const parentIds = removedRows.reduce((acc, selectedRow) => {
      if (selectedRow.parentRowId) {
        acc.add(selectedRow.parentRowId);
      }
      return acc;
    }, new Set<Identifier>());

    parentIds.forEach((parentRowId) => {
      const parentRow = state.entities[parentRowId];
      const children = parentRow.children.filter(
        (identifier) => !removedRowIds.includes(identifier),
      );

      // remove row from original parent children
      changes.push({
        id: parentRowId as any,
        changes: { children },
      });
    });

    return this.adapter.updateMany(changes, state);
  }

  updateRows(
    ids: Identifier[],
    changes: TableValueChange[],
    state: GridState,
  ): Observable<GridState> {
    let dataRowState = { ...state.dataRowState };
    const partialDataRow: Partial<DataRow> = changes.reduce(
      (partial: Partial<DataRow>, change) => {
        partial.data[change.columnId] = change.value;
        return partial;
      },
      { data: {} },
    );
    const updates: Update<DataRow>[] = ids.map((id) => {
      const nextData = { ...state.dataRowState.entities[id].data, ...partialDataRow.data };
      return {
        id: id as any,
        changes: { data: nextData },
      };
    });
    dataRowState = this.adapter.updateMany(updates, dataRowState);
    return of({ ...state, dataRowState });
  }

  setStore(store: Store<GridState>) {
    this.store = store;
  }

  abstract updateCell(url: string[], data: any): Observable<any>;

  refreshSubTrees(
    ids: Identifier[],
    state$: Observable<GridState>,
    _projectData: ProjectDataMap,
  ): Observable<GridState> {
    // NOOP by default, only server backed grids should refresh nodes
    return state$;
  }

  abstract selectAllRows(state: GridState): Observable<GridState>;

  abstract beginAsync(state: Readonly<GridState>): GridState;

  abstract endAsync(state: Readonly<GridState>): GridState;

  openExternalRows(
    ids: Identifier[],
    state$: Observable<GridState>,
    projectData: ProjectDataMap,
  ): Observable<GridState> {
    return this.openRows(ids, state$, projectData);
  }

  resetDataRowState() {
    this.store.state$
      .pipe(take(1))
      .subscribe((state) => this.store.commit({ ...state, dataRowState: initialDataRowState() }));
  }
}
