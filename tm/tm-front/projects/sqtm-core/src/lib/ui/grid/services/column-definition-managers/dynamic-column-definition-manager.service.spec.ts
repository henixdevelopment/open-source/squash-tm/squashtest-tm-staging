import { TestBed } from '@angular/core/testing';

import { DynamicColumnDefinitionManagerService } from './dynamic-column-definition-manager.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReferentialDataService } from '../../../../core/referential/services/referential-data.service';

describe('DynamicColumnDefinitionManagerService', () => {
  const refDataService = jasmine.createSpyObj<ReferentialDataService>(['refresh']);

  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: ReferentialDataService, useValue: refDataService },
        {
          provide: DynamicColumnDefinitionManagerService,
          useClass: DynamicColumnDefinitionManagerService,
          deps: [ReferentialDataService],
        },
      ],
    }),
  );

  it('should be created', () => {
    const service: DynamicColumnDefinitionManagerService = TestBed.inject(
      DynamicColumnDefinitionManagerService,
    );
    expect(service).toBeTruthy();
  });
});
