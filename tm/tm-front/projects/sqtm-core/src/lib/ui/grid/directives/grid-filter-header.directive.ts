import {
  ComponentFactoryResolver,
  Directive,
  Injector,
  Input,
  Type,
  ViewContainerRef,
} from '@angular/core';
import { ColumnDisplay } from '../model/column-display.model';
import { GridDisplay } from '../model/grid-display.model';
import { AbstractDynamicGridElement } from './abstract-dynamic-grid-element';
import { GridFilter } from '../model/state/filter.state';

@Directive({
  selector: '[sqtmCoreGridFilterHeader]',
})
export class GridFilterHeaderDirective extends AbstractDynamicGridElement {
  @Input()
  sqtmCoreGridFilterHeaderColumnDisplay: ColumnDisplay;

  @Input()
  sqtmCoreGridFilterHeaderGridDisplay: GridDisplay;

  @Input()
  sqtmCoreGridFilterHeaderFilterValue: GridFilter;

  @Input()
  sqtmCoreGridFilterHeader: Type<any>;

  constructor(
    _viewContainerRef: ViewContainerRef,
    componentFactoryResolver: ComponentFactoryResolver,
    injector: Injector,
  ) {
    super(_viewContainerRef, componentFactoryResolver, injector);
  }

  getComponentType(): Type<any> {
    return this.sqtmCoreGridFilterHeader;
  }

  getPrefix(): string {
    return 'sqtmCoreGridFilterHeader';
  }
}
