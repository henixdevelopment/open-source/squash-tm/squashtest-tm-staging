import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { AbstractCellRendererComponent } from '../abstract-cell-renderer/abstract-cell-renderer.component';
import { GridService } from '../../../services/grid.service';
import { TranslateService } from '@ngx-translate/core';
import { DatePipe } from '@angular/common';
import { getSupportedBrowserLang } from '../../../../../core/utils/browser-langage.utils';
import { now } from 'lodash';
import { WorkspaceCommonModule } from '../../../../workspace-common/workspace-common.module';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { StaticOrDynamicColumnId } from '../../../../../shared/constants/grid/grid-column-id';
import { ColumnDefinitionBuilder } from '../../../model/column-definition.builder';
import { DateExportValueRenderer } from '../../../../grid-export/value-renderer/value-renderer';

@Component({
  selector: 'sqtm-core-date-cell-renderer',
  template: ` @if (row) {
    <div class="full-width full-height flex-column">
      <span
        class="txt-ellipsis m-auto-0"
        [class.disabled-row]="row.disabled"
        nz-tooltip
        [sqtmCoreLabelTooltip]="displayString"
        >{{ displayString }}</span
      >
    </div>
  }`,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe],
})
export class DateCellRendererComponent extends AbstractCellRendererComponent {
  protected format = 'shortDate';

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    public translateService: TranslateService,
    public datePipe: DatePipe,
  ) {
    super(grid, cdRef);
  }

  get displayString(): string {
    const dateString = this.row.data[this.columnDisplay.id];
    if (dateString == null || dateString == '') {
      return '-';
    } else {
      return this.formatDate(dateString);
    }
  }

  protected formatDate(dateString: string): string {
    return this.datePipe.transform(
      dateString,
      this.format,
      '',
      getSupportedBrowserLang(this.translateService),
    );
  }
}

@Component({
  selector: 'sqtm-core-date-time-cell-renderer',
  template: ` @if (row) {
    <div class="full-width full-height flex-column">
      <span
        style="margin: auto 5px; user-select: text"
        class="txt-ellipsis"
        [class.disabled-row]="row.disabled"
        nz-tooltip
        [sqtmCoreLabelTooltip]="displayString"
        >{{ displayString }}</span
      >
    </div>
  }`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DateTimeCellRendererComponent extends DateCellRendererComponent {
  protected format = 'short';
}

export function sortDate(dateA: Date, dateB: Date): number {
  const dA = new Date(dateA);
  const dB = new Date(dateB);
  let result = 0;

  const same = dA.getTime() === dB.getTime();

  if (same) {
    result = 0;
  }

  if (dA > dB) {
    result = 1;
  }

  if (dA < dB) {
    result = -1;
  }

  return result;
}

@Component({
  selector: 'sqtm-core-concatenated-dates-cell-renderer',
  template: ` @if (row) {
    <div class="full-width full-height flex-column">
      <span
        style="margin: auto 5px; user-select: text"
        class="txt-ellipsis"
        [class.disabled-row]="row.disabled"
        nz-tooltip
        [sqtmCoreLabelTooltip]="displayString"
        >{{ displayString }}</span
      >
    </div>
  }`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConcatenatedDatesCellRendererComponent extends DateCellRendererComponent {
  protected format = 'shortDate';

  get displayString(): string {
    const datesString: string = this.row.data[this.columnDisplay.id];
    if (datesString == null) {
      return '-';
    } else {
      const datesArray = datesString.split(',');
      return datesArray
        .map((date) => this.formatDate(date))
        .join(', ')
        .toString();
    }
  }
}

@Component({
  selector: 'sqtm-core-date-with-expired-cell-renderer',
  template: ` @if (row) {
    <div class="full-width full-height flex-column">
      <span
        class="txt-ellipsis m-auto-0"
        [attr.data-test-element-id]="'date-with-expired-info'"
        [class.disabled-row]="row.disabled"
        [style.color]="color"
        nz-tooltip
        [sqtmCoreLabelTooltip]="displayString"
        >{{ displayString }}</span
      >
    </div>
  }`,
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe],
  imports: [WorkspaceCommonModule, NzToolTipModule],
})
export class DateWithExpiredInfoCellRendererComponent extends DateCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    public translateService: TranslateService,
    public datePipe: DatePipe,
  ) {
    super(grid, cdRef, translateService, datePipe);
  }

  get displayString(): string {
    const dateString = this.row.data[this.columnDisplay.id];

    if (dateString == null) {
      return '-';
    } else if (new Date(dateString) <= new Date(now())) {
      return (
        this.formatDate(dateString) +
        ' (' +
        this.translateService.instant('sqtm-core.generic.label.expired') +
        ')'
      );
    } else {
      return this.formatDate(dateString);
    }
  }

  get color(): string {
    const expiryDate = this.row.data[this.columnDisplay.id];
    return new Date(expiryDate) <= new Date(now()) ? 'red' : 'black';
  }
}

export function dateWithExpiredInfoColumn(id: StaticOrDynamicColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(DateWithExpiredInfoCellRendererComponent)
    .withExportValueRenderer(DateExportValueRenderer);
}
