import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewChild } from '@angular/core';
import { GridService } from '../../../services/grid.service';
import { AbstractCellRendererComponent } from '../abstract-cell-renderer/abstract-cell-renderer.component';
import { RestService } from '../../../../../core/services/rest.service';
import { DialogService } from '../../../../dialog/services/dialog.service';
import { EditableTextFieldComponent } from '../../../../workspace-common/components/editables/editable-text-field/editable-text-field.component';

@Component({
  selector: 'sqtm-core-tool-tip-text-cell-renderer',
  template: ` @if (columnDisplay && row) {
    <div class="full-width full-height flex-column">
      <span
        class="sqtm-grid-cell-txt-renderer m-auto-0"
        [ngClass]="textClass"
        [class.disabled-row]="row.disabled"
        [class.show-as-filtered-parent]="showAsFilteredParent"
        nz-tooltip
        [nzTooltipTitle]="toolTipText"
        [nzTooltipPlacement]="'topLeft'"
      >
        {{ cellText }}
      </span>
    </div>
  }`,
  styleUrls: ['./text-celler-with-tool-tip-render.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TextCellerWithToolTipRenderComponent extends AbstractCellRendererComponent {
  @ViewChild('editableTextField')
  editableTextField: EditableTextFieldComponent;

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    public restService: RestService,
    public dialogService: DialogService,
  ) {
    super(grid, cdRef);
  }

  get toolTipText() {
    return this.row.data[this.columnDisplay.toolTipText];
  }

  get cellText() {
    return this.row.data[this.columnDisplay.id];
  }

  get textClass(): string {
    return 'align-' + this.columnDisplay.contentPosition;
  }
}
