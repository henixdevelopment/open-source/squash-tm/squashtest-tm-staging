import { FilterState, GridFilter, initialFilterState } from '../../model/state/filter.state';
import { ClientFilterManager } from './client-filter-manager';
import { GridState, initialGridState } from '../../model/state/grid.state';
import {
  CustomFieldBindingData,
  ProjectData,
  ProjectDataMap,
} from '../../../../model/project/project-data.model';
import { BindableEntity } from '../../../../model/bindable-entity.model';
import { InputType } from '../../../../model/customfield/input-type.model';

describe(`Abstract Filter Manager`, function () {
  describe(`Add Filters`, () => {
    interface DataType {
      addedFilters: GridFilter[];
      filterState: FilterState;
      expectedIds: string[];
    }

    const dataSets: DataType[] = [
      {
        addedFilters: [
          {
            id: 'reference',
            value: null,
            availableOperations: [],
            i18nLabelKey: '',
            active: true,
            initialValue: { kind: 'single-string-value', value: 'REF' },
            tiedToPerimeter: false,
          },
        ],
        filterState: {
          ...initialFilterState(),
          filters: {
            ids: ['name'],
            entities: {
              name: {
                id: 'name',
                value: { kind: 'single-string-value', value: 'Login Modif' },
                availableOperations: [],
                i18nLabelKey: '',
                active: true,
                initialValue: { kind: 'single-string-value', value: 'Login' },
                tiedToPerimeter: false,
              },
            },
            matchingRowIds: [],
            ancestorMatchingRowIds: [],
          },
        },
        expectedIds: ['name', 'reference'],
      },
      {
        addedFilters: [
          {
            id: 'reference',
            value: null,
            availableOperations: [],
            i18nLabelKey: '',
            active: true,
            initialValue: { kind: 'single-string-value', value: 'REF' },
            tiedToPerimeter: false,
          },
          {
            id: 'importance',
            value: null,
            availableOperations: [],
            i18nLabelKey: '',
            active: true,
            initialValue: { kind: 'multiple-discrete-value', value: [{ id: 'HIGH' }] },
            tiedToPerimeter: false,
          },
        ],
        filterState: {
          ...initialFilterState(),
          filters: {
            ids: ['name'],
            entities: {
              name: {
                id: 'name',
                value: { kind: 'single-string-value', value: 'Login Modif' },
                availableOperations: [],
                i18nLabelKey: '',
                active: true,
                initialValue: { kind: 'single-string-value', value: 'Login' },
                tiedToPerimeter: false,
              },
            },
            matchingRowIds: [],
            ancestorMatchingRowIds: [],
          },
        },
        expectedIds: ['name', 'reference', 'importance'],
      },
      {
        addedFilters: [
          {
            id: 'name',
            value: null,
            availableOperations: [],
            i18nLabelKey: '',
            active: true,
            initialValue: { kind: 'single-string-value', value: 'lol' },
            tiedToPerimeter: false,
          },
        ],
        filterState: {
          ...initialFilterState(),
          filters: {
            ids: ['name', 'reference'],
            entities: {
              name: {
                id: 'name',
                value: { kind: 'single-string-value', value: 'Login Modif' },
                availableOperations: [],
                i18nLabelKey: '',
                active: true,
                initialValue: { kind: 'single-string-value', value: 'Login' },
                tiedToPerimeter: false,
              },
              reference: {
                id: 'name',
                value: { kind: 'single-string-value', value: 'REF' },
                availableOperations: [],
                i18nLabelKey: '',
                active: true,
                initialValue: { kind: 'single-string-value', value: 'REF' },
                tiedToPerimeter: false,
              },
            },
            matchingRowIds: [],
            ancestorMatchingRowIds: [],
          },
        },
        expectedIds: ['name', 'reference'],
      },
    ];

    dataSets.forEach((data, index) => runTest(data, index));

    function runTest(data: DataType, index: number) {
      it(`Dataset ${index} - It should add filters and reset value if needed`, () => {
        const gridState = { ...initialGridState(), ...data };
        const nextState = new ClientFilterManager().addFilters(data.addedFilters, gridState);
        const filters = nextState.filterState.filters;
        expect(filters.ids).toEqual(data.expectedIds);
        expect(filters.entities['name'].value.value).toEqual('Login Modif');
        expect(filters.entities['reference'].value.value).toEqual('REF');
      });
    }
  });

  describe('Change Scope without custom fields', () => {
    it('Should change scope', () => {
      const nextState = new ClientFilterManager().replaceScope(
        {
          kind: 'project',
          value: [
            { id: 'Project-1', projectId: 1, label: 'Projet 1' },
            { id: 'Project-2', projectId: 2, label: 'Projet 2' },
          ],
          initialValue: [],
          initialKind: 'project',
          active: true,
        },
        {},
        initialGridState(),
      );

      expect(nextState.filterState.scope.value.map((v) => v.id)).toEqual([
        'Project-1',
        'Project-2',
      ]);
    });
  });

  describe('Change Scope with custom fields', () => {
    it('Should change scope', () => {
      const customFieldData: CustomFieldBindingData = {
        id: 1,
        bindableEntity: BindableEntity.TEST_CASE,
        boundProjectId: 1,
        position: 0,
        renderingLocations: [],
        customField: {
          id: 1,
          name: 'cuf tag',
          inputType: InputType.TAG,
          options: [],
          label: '',
          code: 'CODE',
          optional: true,
          boundProjectsToCuf: [],
        },
      };

      const projectDataMap: ProjectDataMap = {
        1: {
          id: 1,
          customFieldBinding: {
            TEST_CASE: [customFieldData],
          } as unknown as CustomFieldBindingData,
        } as unknown as ProjectData,
        2: {
          id: 2,
          customFieldBinding: {
            TEST_CASE: [],
          } as unknown as CustomFieldBindingData,
        } as unknown as ProjectData,
      };

      const initialState = { ...initialGridState() };
      const state: GridState = {
        ...initialState,
        definitionState: {
          ...initialState.definitionState,
          addFilterCufFrom: BindableEntity.TEST_CASE,
        },
      };
      const nextState = new ClientFilterManager().replaceScope(
        {
          kind: 'project',
          value: [
            { id: 'Project-1', projectId: 1, label: 'Projet 1' },
            { id: 'Project-2', projectId: 2, label: 'Projet 2' },
          ],
          initialValue: [],
          initialKind: 'project',
          active: true,
        },
        projectDataMap,
        state,
      );

      expect(nextState.filterState.scope.value.map((v) => v.id)).toEqual([
        'Project-1',
        'Project-2',
      ]);
      expect(nextState.filterState.filters.ids).toEqual(['CUF_FILTER_1']);
    });
  });
});
