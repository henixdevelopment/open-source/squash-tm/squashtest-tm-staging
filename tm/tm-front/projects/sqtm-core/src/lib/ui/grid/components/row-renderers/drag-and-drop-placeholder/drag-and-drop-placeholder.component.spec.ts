import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DragAndDropPlaceholderComponent } from './drag-and-drop-placeholder.component';

describe('DragAndDropPlaceholderComponent', () => {
  let component: DragAndDropPlaceholderComponent;
  let fixture: ComponentFixture<DragAndDropPlaceholderComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DragAndDropPlaceholderComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DragAndDropPlaceholderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
