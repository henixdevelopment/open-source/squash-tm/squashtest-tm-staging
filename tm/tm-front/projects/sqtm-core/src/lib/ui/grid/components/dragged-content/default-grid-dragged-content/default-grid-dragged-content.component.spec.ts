import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DefaultGridDraggedContentComponent } from './default-grid-dragged-content.component';
import { DRAG_AND_DROP_DATA } from '../../../../drag-and-drop/constants';
import { GridService } from '../../../services/grid.service';
import { EMPTY } from 'rxjs';

describe('DefaultGridDraggedContentComponent', () => {
  let component: DefaultGridDraggedContentComponent;
  let fixture: ComponentFixture<DefaultGridDraggedContentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: DRAG_AND_DROP_DATA, useValue: { data: { dataRows: [] } } },
        { provide: GridService, useValue: { isSortedOrFiltered$: EMPTY } },
      ],
      declarations: [DefaultGridDraggedContentComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultGridDraggedContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
