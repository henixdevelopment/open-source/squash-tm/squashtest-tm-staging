import { GridNodeGenerator } from './grid-node-generator';
import { PaginationState } from '../../model/state/pagination.state';
import { DataRow, DataRowSortFunction, defaultSortFunction } from '../../model/data-row.model';
import { columnSortMultiplier, SortedColumn } from '../../model/column-definition.model';
import { DataRowState } from '../../model/state/datarow.state';
import { ColumnState } from '../../model/state/column.state';
import { Identifier } from '../../../../model/entity.model';
import { FilterState } from '../../model/state/filter.state';
import { Observable, of } from 'rxjs';
import { GridState } from '../../model/state/grid.state';
import { GridNodeState } from '../../model/grid-node.model';
import { AbstractNodeGenerator } from './abstract-node-generator';
import { gridLogger } from '../../grid.logger';
import { UiState } from '../../model/state/ui.state';
import { gridNodeStateAdapter } from '../../model/state/grid-nodes.state';
import { DataRowOpenState } from '../../../../model/grids/data-row.model';

const logger = gridLogger.compose('ClientGridNodeGenerator');

/**
 * For a client grid the transformer is responsible for a lot of operations, so it can be a quite complex pipeline
 *  Flattening the tree
 *  Ordering
 *  Filtering (denormalized into filter state)
 *  Pagination
 *  Row Grouping ...
 */
export class ClientGridNodeGenerator extends AbstractNodeGenerator implements GridNodeGenerator {
  computeNodeTree(state: Readonly<GridState>): Observable<GridState> {
    const paginationState: Readonly<PaginationState> = state.paginationState;
    let nodes: GridNodeState[];
    // pagination active means that the table is pageable, ie it's not a tree !!
    if (paginationState.active) {
      nodes = performPagination(
        state.dataRowState,
        state.columnState,
        state.paginationState,
        state.filterState,
      );
    } else {
      nodes = flatTree(state.dataRowState, state.columnState, state.filterState, state.uiState);
    }
    if (nodes.length === 0) {
      const placeholderNode = this.createPlaceholderNode();
      nodes.push(placeholderNode);
    }
    const nodeState = gridNodeStateAdapter.setAll(nodes, state.nodesState);
    return of({ ...state, nodesState: nodeState });
  }
}

function performPagination(
  dataRowsState: DataRowState,
  columnState,
  pagination,
  filterState: FilterState,
) {
  const sortedRows = filterAndSortRows(dataRowsState, columnState, filterState);
  const paginatedRows = paginateRows(sortedRows, pagination);
  return paginatedRows.map((dataRow, index) => {
    return convert(
      dataRow,
      0,
      index,
      dataRowsState.selectedRows.includes(dataRow.id),
      false,
      false,
      [false],
    );
  });
}

function paginateRows(dataRows: DataRow[], pagination: PaginationState): DataRow[] {
  if (pagination.active && pagination.size !== -1) {
    const minIndex = pagination.page * pagination.size;
    const maxIndex = (pagination.page + 1) * pagination.size;
    return dataRows.slice(minIndex, maxIndex);
  }
  return dataRows;
}

export function flatTree(
  dataRowState: DataRowState,
  columnState: ColumnState,
  filterState: FilterState,
  uiState: UiState,
): GridNodeState[] {
  const begin = Date.now();
  if (logger.isTraceEnabled()) {
    logger.trace(`Begin flat tree ${begin}`);
  }
  const root = getRootRows(dataRowState, filterState, uiState);
  const stack = createInitialStack(root, columnState, filterState);
  const tree = computeTree(stack, dataRowState, filterState, columnState, uiState);
  const gridNodes = transformTreeToGridNodes(columnState, tree, dataRowState);
  if (logger.isTraceEnabled()) {
    logger.trace(`End flat tree ${Date.now() - begin}`);
  }
  return gridNodes;
}

function computeTree(
  stack: RowReference[],
  dataRowState: DataRowState,
  filterState: FilterState,
  columnState: ColumnState,
  uiState: UiState,
) {
  const tree: RowReference[] = [];
  let currentIndex = 0;

  while (stack.length) {
    const rowReference = stack.pop();
    rowReference.index = currentIndex;
    currentIndex++;
    tree.push(rowReference);
    const row = dataRowState.entities[rowReference.id];
    if (row.children && row.state === DataRowOpenState.open) {
      const children = row.children
        .filter((childId) => rowAllowToBeDisplayed(filterState, uiState, childId))
        .map((childId) => dataRowState.entities[childId]);
      const sortedChildren = doSortRows(children, columnState);
      for (let i = sortedChildren.length - 1; i >= 0; i--) {
        const childId = sortedChildren[i].id;
        const isLast = i === sortedChildren.length - 1;
        const depth = rowReference.depth + 1;

        const childReference = {
          id: childId,
          depth: depth,
          index: 0,
          showAsFilteredParent: false,
          isLast,
          depthMap: [...rowReference.depthMap, isLast],
        };

        if (filterState.filters.ids.length > 0) {
          childReference.showAsFilteredParent = mustBeShownAsFilteredParent(filterState, childId);
        }

        stack.push(childReference);
      }
    }
  }
  return tree;
}

function getRootRows(dataRowState: DataRowState, filterState: FilterState, uiState: UiState) {
  // Get root rows, in the order defined by rootRows.
  return dataRowState.rootRowIds.reduce((rootRows, id) => {
    const mustAdd = rowAllowToBeDisplayed(filterState, uiState, id);
    if (mustAdd) {
      rootRows.push(dataRowState.entities[id]);
    }
    return rootRows;
  }, [] as DataRow[]);
}

function filtersAllowDisplay(filterState: FilterState, id) {
  let show = false;
  if (filterState.filters.ids.length > 0) {
    if (
      filterState.filters.matchingRowIds.includes(id) ||
      filterState.filters.ancestorMatchingRowIds.includes(id)
    ) {
      show = true;
    }
  } else {
    show = true;
  }
  return show;
}

function dragAndDropAllowDisplay(uiState: UiState, id) {
  let shownWithFilter = false;
  if (uiState.dragState.dragging) {
    if (!uiState.dragState.draggedRowIds.includes(id)) {
      shownWithFilter = true;
    }
  } else {
    shownWithFilter = true;
  }
  return shownWithFilter;
}

function rowAllowToBeDisplayed(filterState: FilterState, uiState: UiState, id): boolean {
  const filtersAllowedDisplay = filtersAllowDisplay(filterState, id);
  const dndAllowedDisplay = dragAndDropAllowDisplay(uiState, id);
  return filtersAllowedDisplay && dndAllowedDisplay;
}

function mustBeShownAsFilteredParent(filterState: FilterState, id: Identifier): boolean {
  return (
    filterState.filters.ancestorMatchingRowIds.includes(id) &&
    !filterState.filters.matchingRowIds.includes(id)
  );
}

function createInitialStack(
  root,
  columnState: ColumnState,
  filterState: FilterState,
): RowReference[] {
  // init the stack with root rows converted in RowReference
  return doSortRows(root, columnState)
    .map((dataRow) => {
      const gridNode = {
        id: dataRow.id,
        depth: 0,
        index: 0,
        showAsFilteredParent: false,
        isLast: false,
        depthMap: [false],
      };
      if (filterState.filters.ids.length > 0) {
        gridNode.showAsFilteredParent = mustBeShownAsFilteredParent(filterState, dataRow.id);
      }
      return gridNode;
    })
    .reverse();
}

function transformTreeToGridNodes(
  columnState: ColumnState,
  tree: RowReference[],
  dataRowState: DataRowState,
) {
  return tree.map((rowReference) =>
    convert(
      dataRowState.entities[rowReference.id],
      rowReference.depth,
      rowReference.index,
      dataRowState.selectedRows.includes(rowReference.id),
      rowReference.showAsFilteredParent,
      rowReference.isLast,
      rowReference.depthMap,
    ),
  );
  // }
}

export function convert(
  dataRow: DataRow,
  depth: number,
  index: number,
  selected: boolean,
  showAsFilteredParent: boolean,
  isLast: boolean,
  depthMap: boolean[],
): GridNodeState {
  return {
    id: dataRow.id,
    dataRow: dataRow,
    depth,
    index,
    selected,
    showAsFilteredParent,
    isLast,
    depthMap,
  };
}

function doSortRows(dataRows: DataRow[], columnState: ColumnState): DataRow[] {
  if (columnState.sortedColumns.length === 0) {
    return dataRows;
  }
  return [...dataRows].sort((rowA, rowB) => {
    let result = 0;
    const sortedColumnsStack = [...columnState.sortedColumns].reverse();
    while (result === 0 && sortedColumnsStack.length > 0) {
      const sort = sortedColumnsStack.pop();
      const sortedColumn = columnState.entities[sort.id];
      if (sortedColumn) {
        const valueA = rowA.data[sort.id];
        const valueB = rowB.data[sort.id];
        let sortFunction: DataRowSortFunction;
        if (sortedColumn.sortFunction && typeof sortedColumn.sortFunction === 'function') {
          sortFunction = sortedColumn.sortFunction;
        } else {
          sortFunction = defaultSortFunction;
        }
        result = columnSortMultiplier(sort.sort) * sortFunction(valueA, valueB);
      }
    }
    return result;
  });
}

export function filterAndSortRows(
  dataRowState: DataRowState,
  columnState: ColumnState,
  filterState: FilterState,
): DataRow[] {
  const sortedColumns: SortedColumn[] = columnState.sortedColumns;
  const filteredRows = applyFilters(dataRowState, columnState, filterState).reduce((map, row) => {
    map.set(row.id, row);
    return map;
  }, new Map());
  // no sort, just return row in the root ids order
  if (sortedColumns.length === 0) {
    return dataRowState.rootRowIds.reduce((rootRows, id) => {
      if (filteredRows.has(id)) {
        rootRows.push(filteredRows.get(id));
      }
      return rootRows;
    }, []);
  }
  return doSortRows(Array.from(filteredRows.values()), columnState);
}

export function applyFilters(
  dataRowState: DataRowState,
  columnState: ColumnState,
  filterState: FilterState,
): DataRow[] {
  if (filterState.filters.ids.length === 0) {
    return Object.values(dataRowState.entities);
  } else {
    return filterState.filters.matchingRowIds
      .map((id) => dataRowState.entities[id])
      .filter((row) => Boolean(row));
  }
}

export interface RowReference {
  id: Identifier;
  index: number;
  depth: number;
  showAsFilteredParent: boolean;
  // does this node is the last in container.
  isLast: boolean;
  // a map representing if vertical line must be shown for de given depth.
  depthMap: boolean[];
}
