import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { AbstractCellRendererComponent } from '../abstract-cell-renderer/abstract-cell-renderer.component';
import { GridService } from '../../../services/grid.service';

@Component({
  selector: 'sqtm-app-requirement-ratio-cell',
  template: ` @if (row) {
    <div class="full-width full-height flex-column">
      <div class="sqtm-grid-cell-txt-renderer m-auto-0">
        <span> {{ synchronizedSprintTicketsRatio }} </span>
      </div>
    </div>
  }`,
  styleUrl: './synchronized-sprint-tickets-ratio-cell.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SynchronizedSprintTicketsRatioCellComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdr: ChangeDetectorRef,
  ) {
    super(grid, cdr);
  }

  get synchronizedSprintTicketsRatio() {
    return this.row.data.synchronizedSprintTicketsCount != null
      ? this.row.data.synchronizedSprintTicketsCount +
          '/' +
          (this.row.data.unprocessedSprintTicketsCount +
            this.row.data.synchronizedSprintTicketsCount)
      : '-';
  }
}
