import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
} from '@angular/core';

import { GridService } from '../../../services/grid.service';
import { AbstractHeaderRendererComponent } from '../abstract-header-renderer/abstract-hearder-renderer.component';
import { combineLatest } from 'rxjs';
import { DataRow } from '../../../model/data-row.model';
import { map } from 'rxjs/operators';
import { Dictionary } from '@ngrx/entity';
import { GridViewportName } from '../../../../../model/grids/grid-viewport-name';

@Component({
  selector: 'sqtm-core-toggle-selection-header-renderer',
  template: ` @if (grid.gridDisplay$ | async; as gridDisplay) {
    <div class="full-width flex-row">
      <label
        style="margin: auto"
        nz-checkbox
        [(ngModel)]="checked"
        (ngModelChange)="toggleRowSelection($event)"
      ></label>
    </div>
  }`,
  styleUrls: ['./toggle-selection-header-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToggleSelectionHeaderRendererComponent
  extends AbstractHeaderRendererComponent
  implements OnInit
{
  checked: boolean;

  @Input()
  viewportName: GridViewportName;

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }

  ngOnInit() {
    combineLatest([this.grid.dataRows$, this.grid.selectedRows$])
      .pipe(
        map(([dataRows, selectedRows]: [Dictionary<DataRow>, DataRow[]]) => {
          return Object.values(dataRows).length === selectedRows.length;
        }),
      )
      .subscribe((allChecked) => {
        this.checked = allChecked;
        this.cdRef.detectChanges();
      });
  }

  toggleRowSelection(event) {
    if (event) {
      this.grid.selectAllRows();
    } else {
      this.grid.unselectAllRows();
    }
  }
}
