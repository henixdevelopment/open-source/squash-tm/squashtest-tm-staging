import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { AbstractCellRendererComponent } from '../../abstract-cell-renderer/abstract-cell-renderer.component';
import { GridService } from '../../../../services/grid.service';

@Component({
  selector: 'sqtm-core-custom-field-check-box-renderer',
  template: ` @if (row) {
    <div class="full-width full-height flex-column">
      <span
        style="margin: auto 0 auto 0"
        nz-checkbox
        [(nzChecked)]="row.data[columnDisplay?.id]"
      ></span>
    </div>
  }`,
  styleUrls: ['./custom-field-check-box-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomFieldCheckBoxRendererComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }
}
