import {
  ComponentFactoryResolver,
  Directive,
  Injector,
  Input,
  Type,
  ViewContainerRef,
} from '@angular/core';
import { ColumnDisplay } from '../model/column-display.model';
import { GridDisplay } from '../model/grid-display.model';
import { AbstractDynamicGridElement } from './abstract-dynamic-grid-element';
import { GridViewportName } from '../../../model/grids/grid-viewport-name';

@Directive({
  selector: '[sqtmCoreGridHeader]',
})
export class GridHeaderDirective extends AbstractDynamicGridElement {
  @Input()
  sqtmCoreGridHeaderColumnDisplay: ColumnDisplay;

  @Input()
  sqtmCoreGridHeaderGridDisplay: GridDisplay;

  @Input()
  sqtmCoreGridHeader: Type<any>;

  @Input()
  sqtmCoreGridHeaderViewportName: GridViewportName;

  constructor(
    _viewContainerRef: ViewContainerRef,
    componentFactoryResolver: ComponentFactoryResolver,
    injector: Injector,
  ) {
    super(_viewContainerRef, componentFactoryResolver, injector);
  }

  getComponentType(): Type<any> {
    return this.sqtmCoreGridHeader;
  }

  getPrefix(): string {
    return 'sqtmCoreGridHeader';
  }
}
