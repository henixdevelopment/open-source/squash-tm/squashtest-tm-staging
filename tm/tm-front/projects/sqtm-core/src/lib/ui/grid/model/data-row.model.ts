import { Identifier } from '../../../model/entity.model';
import { GridFilter } from './state/filter.state';
import { RowRenderer } from '../components/row-renderers/RowRenderer';
import { Type } from '@angular/core';
import { GridRowComponent } from '../components/row-renderers/grid-row/grid-row.component';
import { EmptyGridMessageComponent } from '../components/row-renderers/empty-grid-message/empty-grid-message.component';
import { DragAndDropPlaceholderComponent } from '../components/row-renderers/drag-and-drop-placeholder/drag-and-drop-placeholder.component';
import { ResearchColumnPrototype } from '../../filters/state/filter.state';
import { Sort } from './column-definition.model';
import { SimplePermissions } from '../../../model/permissions/simple-permissions';
import { FilterValueModel } from '../../../model/filter/filter-value.model';
import { DataRowModel, DataRowOpenState } from '../../../model/grids/data-row.model';

export abstract class DataRow<T = any> implements DataRowModel<T> {
  id: Identifier;
  readonly type;
  data: T;
  children?: Identifier[] = [];
  state?: DataRowOpenState = DataRowOpenState.closed;
  parentRowId?: Identifier;
  abstract readonly allowedChildren: any[];
  abstract readonly allowMoves: boolean;
  component: Type<RowRenderer> = GridRowComponent;
  projectId: number;
  simplePermissions: SimplePermissions;
  disabled?: boolean;
  // allow to mark a row as target of async operation. Typically used when you want to indicate an async operation
  // without making all grid pass in async mode (aka you don't want the overlay on all grid)
  async?: boolean;
  selectable: boolean;
}

export class GenericDataRow<T = any> extends DataRow<T> {
  readonly type = 'Generic';
  allowMoves = true;
  allowedChildren: any[] = [];
  selectable = true;
}

export const placeholderDataRowId = 'SQTM-CORE-EMPTY-GRID-MESSAGE';
export const placeholderDataRowType = 'SqtmCorePlaceholderDataRow';
export const dndPlaceholderDataRowId = 'SQTM-CORE-GRID-DND-PLACEHOLDER';
export const dndPlaceholderDataRowType = 'SqtmCoreDndPlaceholderDataRow';

export class PlaceholderDataRow<T = any> extends DataRow<T> {
  id = placeholderDataRowId;
  readonly type = placeholderDataRowType;
  allowMoves = false;
  allowedChildren: any[] = [];
  component = EmptyGridMessageComponent;
}

export class DragAndDropPlaceHolderDataRow<T = any> extends DataRow<T> {
  readonly id = dndPlaceholderDataRowId;
  readonly type = dndPlaceholderDataRowType;
  allowMoves = false;
  allowedChildren: any[] = [];
  component = DragAndDropPlaceholderComponent;

  constructor(dataRow: DataRow) {
    super();
    this.data = dataRow.data;
  }
}

export interface GridRequestSort {
  property: string;
  direction: Sort;
  columnPrototype?: ResearchColumnPrototype;
}

export interface GridRequest {
  page: number;
  size: number;
  sort?: GridRequestSort[];
  filterValues?: FilterValueModel[];
  scope?: string[];
  extendedHighLvlReqScope?: boolean;
  selectedRows?: Identifier[];
  searchOnMultiColumns?: boolean;
  simplifiedColumnDisplayGridIds?: string[];
  gridId?: string;
}

export interface TreeRequest {
  openedNodes: Identifier[];
  selectedNodes: Identifier[];
}

export type DataRowSortFunction = (valueA: any, valueB: any) => number;

export type DataRowFilterFunction = (filter: GridFilter, row: DataRow) => boolean;

export function defaultSortFunction(valueA: string | number, valueB: string | number): number {
  let result = 0;
  // == on null will be true for undefined and null
  // DO NOT change to === or undefined values will not be filtered
  // concerning string we consider '' as null or undefined
  // concerning numbers we consider NaN as null or undefined
  let valueAisNullish = valueA == null || valueA === '';
  let valueBisNullish = valueB == null || valueB === '';
  let viciousValues = false;

  if (!valueAisNullish && typeof valueA === 'number' && isNaN(valueA)) {
    valueAisNullish = true;
  }

  if (!valueBisNullish && typeof valueB === 'number' && isNaN(valueB)) {
    valueBisNullish = true;
  }

  // handling nullish values
  if (valueAisNullish && valueBisNullish) {
    viciousValues = true;
  } else if (valueAisNullish && !valueBisNullish) {
    viciousValues = true;
    result = -1;
  } else if (!valueAisNullish && valueBisNullish) {
    viciousValues = true;
    result = 1;
  }

  // if we have no nullish values let's compare real values
  if (!viciousValues) {
    if (typeof valueA === 'string' || typeof valueB === 'string') {
      result = doCompareStrings(valueA as string, valueB as string);
    } else if (typeof valueA === 'number' || typeof valueB === 'number') {
      result = doCompareNumbers(valueA, valueB);
    } else if (typeof valueA === 'boolean' || typeof valueB === 'boolean') {
      result = doCompareBooleans(valueA, valueB);
    } else {
      console.log(`unable to sort type, valueA ${typeof valueA} valueB ${typeof valueB}.
            Please provide a custom sorter in your column definition`);
    }
  }
  return result;
}

function doCompareStrings(valueA: string, valueB: string) {
  return valueA.localeCompare(valueB);
}

function doCompareNumbers(valueA: number, valueB: number) {
  return valueA - valueB;
}

function doCompareBooleans(valueA: boolean, valueB: boolean) {
  return Number(valueA) - Number(valueB);
}
