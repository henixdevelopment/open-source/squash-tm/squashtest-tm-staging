import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { AbstractCellRendererComponent } from '../abstract-cell-renderer/abstract-cell-renderer.component';
import { GridService } from '../../../services/grid.service';

@Component({
  selector: 'sqtm-app-requirement-ratio-cell',
  template: ` @if (row) {
    <div class="full-width full-height flex-column">
      <div class="sqtm-grid-cell-txt-renderer m-auto-0">
        <span> {{ synchronizedRequirementsRatio }} </span>
      </div>
    </div>
  }`,
  styleUrl: './synchronized-requirements-ratio-cell.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SynchronizedRequirementsRatioCellComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdr: ChangeDetectorRef,
  ) {
    super(grid, cdr);
  }

  get synchronizedRequirementsRatio() {
    return this.row.data.synchronizedRequirementsCount != null
      ? this.row.data.synchronizedRequirementsCount +
          '/' +
          (this.row.data.unprocessedRequirementsCount + this.row.data.synchronizedRequirementsCount)
      : '-';
  }
}
