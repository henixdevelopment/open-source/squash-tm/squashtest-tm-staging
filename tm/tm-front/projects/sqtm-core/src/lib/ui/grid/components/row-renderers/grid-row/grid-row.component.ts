import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
} from '@angular/core';
import type { GridDisplay, ViewportDisplay } from '../../../model/grid-display.model';
import { GridService } from '../../../services/grid.service';
import type { ColumnDisplay } from '../../../model/column-display.model';
import { Subject } from 'rxjs';
import type { GridNode } from '../../../model/grid-node.model';
import type { RowRenderer } from '../RowRenderer';

@Component({
  selector: 'sqtm-core-grid-row',
  template: `
    @if (gridDisplay && gridNode && viewport) {
      <div
        [sqtmCoreGridHoverRow]="gridNode.id"
        class="full-height full-width grid-row"
        [class.flex-row]="gridDisplay.gridType === 'TABLE'"
        [class.selected]="gridDisplay.style.highlightSelectedRows && gridNode.selected"
        [class.grid-row__underline]="gridDisplay.style.showLines"
        [attr.data-test-row-id]="gridNode.id"
        [attr.data-test-parent-id]="gridNode.dataRow.parentRowId"
        [attr.data-test-dnd-container]="gridNode.showAsDndTarget"
      >
        @for (columnDisplay of viewport.columnDisplays; track trackByFn($index, columnDisplay)) {
          @if (shouldDrawCell(columnDisplay)) {
            <sqtm-core-grid-cell
              [gridNode]="gridNode"
              [columnDisplay]="columnDisplay"
              [gridDisplay]="gridDisplay"
              [viewportName]="viewport.name"
              [attr.data-test-cell-id]="columnDisplay.id"
              [attr.data-test-cuf-cell-id]="columnDisplay.cufId"
            ></sqtm-core-grid-cell>
          }
        }
      </div>
    }
  `,
  styleUrls: ['./grid-row.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GridRowComponent implements OnDestroy, RowRenderer {
  @Input()
  gridDisplay: GridDisplay;

  @Input()
  gridNode: GridNode;

  @Input()
  viewport: ViewportDisplay;

  private unsub$ = new Subject<void>();

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {}

  trackByFn(index: number, columnDisplay: ColumnDisplay) {
    return columnDisplay.id;
  }

  shouldDrawCell(columnDisplay: ColumnDisplay) {
    return columnDisplay.show;
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
