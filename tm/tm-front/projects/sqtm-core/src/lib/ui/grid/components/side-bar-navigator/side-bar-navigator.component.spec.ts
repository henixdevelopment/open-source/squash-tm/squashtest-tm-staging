import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SideBarNavigatorComponent } from './side-bar-navigator.component';
import { GridTestingModule } from '../../grid-testing/grid-testing.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { OverlayModule } from '@angular/cdk/overlay';
import { WorkspaceCommonModule } from '../../../workspace-common/workspace-common.module';
import { TestingUtilsModule } from '../../../testing-utils/testing-utils.module';

describe('SideBarNavigatorComponent', () => {
  let component: SideBarNavigatorComponent;
  let fixture: ComponentFixture<SideBarNavigatorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SideBarNavigatorComponent],
      imports: [GridTestingModule, TestingUtilsModule, OverlayModule, WorkspaceCommonModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideBarNavigatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
