import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GridHeaderRendererComponent } from './grid-header-renderer.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { GridTestingModule } from '../../../grid-testing/grid-testing.module';
import { Fixed } from '../../../model/column-definition.model';
import { OverlayModule } from '@angular/cdk/overlay';
import { TestingUtilsModule } from '../../../../testing-utils/testing-utils.module';

describe('GridHeaderRendererComponent', () => {
  let component: GridHeaderRendererComponent;
  let fixture: ComponentFixture<GridHeaderRendererComponent>;
  const translateService = jasmine.createSpyObj('TranslateService', ['instant']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [GridHeaderRendererComponent],
      imports: [GridTestingModule, OverlayModule, TestingUtilsModule],
      providers: [
        {
          provide: TranslateService,
          useValue: translateService,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GridHeaderRendererComponent);
    component = fixture.componentInstance;
    component.columnDisplay = {
      id: 'id',
      show: true,
      widthCalculationStrategy: new Fixed(200),
      headerPosition: 'left',
      contentPosition: 'left',
      showHeader: true,
      viewportName: 'mainViewport',
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
