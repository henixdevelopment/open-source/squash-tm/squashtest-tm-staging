import { DataRow } from '../data-row.model';

export class GridDndData {
  constructor(public readonly dataRows: Readonly<DataRow>[]) {}
}
