import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MultiValueCufFilterComponent } from './multi-value-cuf-filter.component';
import { ReferentialDataService } from '../../../../../core/referential/services/referential-data.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TestingUtilsModule } from '../../../../testing-utils/testing-utils.module';

describe('MultiValueCufFilterComponent', () => {
  let component: MultiValueCufFilterComponent;
  let fixture: ComponentFixture<MultiValueCufFilterComponent>;

  const referentialDataServiceMock = jasmine.createSpyObj(['load']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule],
      declarations: [MultiValueCufFilterComponent],
      providers: [
        {
          provide: ReferentialDataService,
          useValue: referentialDataServiceMock,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiValueCufFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
