import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FilterTestCaseMilestoneLabelComponent } from './filter-test-case-milestone-label.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ReferentialDataService } from '../../../../core/referential/services/referential-data.service';

describe('FilterTestCaseMilestoneLabelComponent', () => {
  let component: FilterTestCaseMilestoneLabelComponent;
  let fixture: ComponentFixture<FilterTestCaseMilestoneLabelComponent>;

  const referentialDataServiceMock = jasmine.createSpyObj(['load']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [FilterTestCaseMilestoneLabelComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: ReferentialDataService,
          useValue: referentialDataServiceMock,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterTestCaseMilestoneLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
