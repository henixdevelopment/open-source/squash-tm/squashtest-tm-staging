import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';
import { AbstractFilterWidget } from '../abstract-filter-widget';
import { UserListElement } from '../../../workspace-common/components/forms/user-list-selector/user-list-selector.component';
import { Filter, MultiDiscreteFilterValue } from '../../state/filter.state';
import { UserHistorySearchProvider } from '../../user-history-search-provider.service';
import { Scope } from '../../../../model/filter/filter.model';
import { DiscreteFilterValue } from '../../../../model/filter/filter-value.model';

@Component({
  selector: 'sqtm-core-filter-user-history',
  templateUrl: './filter-user-history.component.html',
  styleUrls: ['./filter-user-history.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterUserHistoryComponent extends AbstractFilterWidget implements OnInit {
  users: UserListElement[];

  private filterValue: DiscreteFilterValue[];

  constructor(
    protected cdRef: ChangeDetectorRef,
    private userProvider: UserHistorySearchProvider,
  ) {
    super(cdRef);
  }

  ngOnInit() {}

  setFilter(filter: Filter, _scope: Scope): any {
    this.userProvider
      .provideUserList(filter.columnPrototype)
      .pipe(take(1))
      .subscribe((providedUsers) => {
        const selectedUserIds = (filter.value as MultiDiscreteFilterValue).value.map((v) => v.id);
        this.users = providedUsers
          .filter((user) => user.login !== null)
          .map((user) => ({
            ...user,
            selected: selectedUserIds.includes(user.login),
          }));
        this.filterValue = (filter.value as MultiDiscreteFilterValue).value;
        this.cdRef.detectChanges();
      });
  }

  changeFilterValue($event: UserListElement) {
    let nextValue: DiscreteFilterValue[] = [...this.filterValue];
    if ($event.selected) {
      nextValue = this.filterValue.concat({ id: $event.login, label: $event.login });
    } else {
      const index = this.filterValue.findIndex((v) => v.id === $event.login);
      if (index >= 0) {
        nextValue.splice(index, 1);
      }
    }
    this.filteringChanged.next({
      filterValue: {
        kind: 'multiple-discrete-value',
        value: nextValue,
      },
    });
  }
}
