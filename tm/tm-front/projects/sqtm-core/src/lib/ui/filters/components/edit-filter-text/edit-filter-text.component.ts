import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { AbstractFilterWidget } from '../abstract-filter-widget';
import { Filter, isStringValue } from '../../state/filter.state';
import { Workspaces } from '../../../ui-manager/theme.model';
import { FilterOperation } from '../../../../model/filter/filter.model';
import { isEnterKeyboardEvent, isEscapeKeyboardEvent } from '../../../utils/key-utils';

@Component({
  selector: 'sqtm-core-edit-filter-text',
  templateUrl: './edit-filter-text.component.html',
  styleUrls: ['./edit-filter-text.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditFilterTextComponent extends AbstractFilterWidget implements OnInit, OnDestroy {
  @ViewChild('input', { static: true })
  input: ElementRef;

  value: string;

  workspace: Workspaces;

  private operation: FilterOperation;

  constructor(protected cdRef: ChangeDetectorRef) {
    super(cdRef);
  }

  ngOnInit() {
    this.input.nativeElement.focus();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  setFilter(filter: Filter) {
    this._filter = filter;
    this.workspace = filter.uiOptions?.workspace;
    if (isStringValue(filter.value)) {
      this.value = filter.value.value;
      this.cdRef.detectChanges();
    } else {
      throw Error('Not handled kind ' + filter.value.kind);
    }
  }

  closeTextField() {
    this.close.next();
  }

  changeValue() {
    if (this.value !== null) {
      this.filteringChanged.next({
        filterValue: {
          kind: 'single-string-value',
          value: this.operationIsCaseSensitive() ? this.value : this.value.toLowerCase(),
        },
        operation: this.getSelectedOperation(),
      });
      this.closeTextField();
    }
  }

  private getSelectedOperation() {
    return this.operation || this.filter.operation;
  }

  private operationIsCaseSensitive() {
    return [FilterOperation.FULLTEXT].includes(this.getSelectedOperation());
  }

  handleKeyboardInput(event: KeyboardEvent) {
    if (isEnterKeyboardEvent(event)) {
      this.changeValue();
      this.closeTextField();
    } else if (isEscapeKeyboardEvent(event)) {
      this.closeTextField();
    }
    return false;
  }

  operationChanged(operation: FilterOperation) {
    this.operation = operation;
  }
}
