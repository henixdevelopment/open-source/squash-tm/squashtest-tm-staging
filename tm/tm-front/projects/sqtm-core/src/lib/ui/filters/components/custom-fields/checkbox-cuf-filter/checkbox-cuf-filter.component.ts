import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AbstractFilterWidget } from '../../abstract-filter-widget';
import { ListItem } from '../../../../workspace-common/components/forms/grouped-multi-list/grouped-multi-list.component';
import { Filter, isDiscreteValue } from '../../../state/filter.state';
import { ReferentialDataService } from '../../../../../core/referential/services/referential-data.service';
import { take } from 'rxjs/operators';
import { InputType } from '../../../../../model/customfield/input-type.model';
import { Workspaces } from '../../../../ui-manager/theme.model';
import { FilterOperation, Scope } from '../../../../../model/filter/filter.model';
import { DiscreteFilterValue } from '../../../../../model/filter/filter-value.model';

@Component({
  selector: 'sqtm-core-checkbox-cuf-filter',
  templateUrl: './checkbox-cuf-filter.component.html',
  styleUrls: ['./checkbox-cuf-filter.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CheckboxCufFilterComponent extends AbstractFilterWidget implements OnInit {
  items: ListItem[];

  private filterValue: DiscreteFilterValue[];

  operation: string;

  isTag = false;

  workspace: Workspaces;

  constructor(
    protected cdRef: ChangeDetectorRef,
    private referentialDataService: ReferentialDataService,
  ) {
    super(cdRef);
  }

  ngOnInit() {}

  setFilter(filter: Filter, _scope: Scope) {
    this.referentialDataService
      .connectToCustomField(filter.cufId)
      .pipe(take(1))
      .subscribe((cuf) => {
        const filterValue = filter.value;
        if (isDiscreteValue(filterValue)) {
          this.workspace = filter.uiOptions?.workspace;
          const selectedIds = filterValue.value.map((v) => v.id);
          this.items = [
            {
              id: 'true',
              i18nLabelKey: 'sqtm-core.generic.label.true',
              selected: selectedIds.includes('true'),
            },
            {
              id: 'false',
              i18nLabelKey: 'sqtm-core.generic.label.false',
              selected: selectedIds.includes('false'),
            },
          ];
          this.isTag = cuf.inputType === InputType.TAG;
          this._filter = filter;
          this.filterValue = filterValue.value;
          this.operation = filter.operation;
          this.cdRef.detectChanges();
        } else {
          throw Error('Not handled kind ' + filter.value.kind);
        }
      });
    this._filter = filter;
  }

  changeSelection(item: ListItem) {
    let nextValue: DiscreteFilterValue[] = [...this.filterValue];
    if (item.selected) {
      nextValue = this.filterValue.concat({
        id: item.id,
        label: item.label,
        i18nLabelKey: item.i18nLabelKey,
      });
    } else {
      const index = this.filterValue.findIndex((v) => v.id === item.id);
      if (index >= 0) {
        nextValue.splice(index, 1);
      }
    }
    this.filteringChanged.next({
      filterValue: { kind: 'multiple-discrete-value', value: nextValue },
    });
  }

  changeOperation() {
    const newOperation = FilterOperation[this.operation];
    this.filteringChanged.next({ operation: newOperation });
  }
}
