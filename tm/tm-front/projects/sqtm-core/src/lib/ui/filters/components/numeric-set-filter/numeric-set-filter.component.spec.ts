import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NumericSetFilterComponent } from './numeric-set-filter.component';
import { TestingUtilsModule } from '../../../testing-utils/testing-utils.module';
import { FormsModule } from '@angular/forms';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FilterOperation } from '../../../../model/filter/filter.model';

describe('NumericSetFilterComponent', () => {
  let component: NumericSetFilterComponent;
  let fixture: ComponentFixture<NumericSetFilterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, FormsModule, NzInputNumberModule],
      declarations: [NumericSetFilterComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumericSetFilterComponent);
    component = fixture.componentInstance;
    component.setFilter({
      id: 'filter',
      availableOperations: [FilterOperation.IN],
      value: {
        kind: 'multiple-numeric-value',
        value: [1, 2, 3],
      },
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should throw if not given the right input type', () => {
    expect(() =>
      component.setFilter({
        id: 'filter',
        availableOperations: [FilterOperation.IN],
        value: {
          kind: 'single-string-value',
          value: 'hello there',
        },
      }),
    ).toThrowError();
  });

  describe('should convert string values to numeric sets', () => {
    const testDataSets = [
      {
        name: 'remove duplicates',
        input: {
          integersOnly: true,
          stringValue: '1,2,2,3',
        },
        expected: [1, 2, 3],
      },
      {
        name: 'ignore not integers',
        input: {
          integersOnly: true,
          stringValue: '1,abcd,,è,2,3',
        },
        expected: [1, 2, 3],
      },
      {
        name: 'ignore not numbers',
        input: {
          integersOnly: false,
          stringValue: 'abcd,1,,,è,0.2,3,df5g',
        },
        expected: [0.2, 1, 3],
      },
      {
        name: 'sort numbers',
        input: {
          integersOnly: false,
          stringValue: '12, 0.5 , 2,-123',
        },
        expected: [-123, 0.5, 2, 12],
      },
      {
        name: 'null',
        input: {
          integersOnly: false,
          stringValue: null,
        },
        expected: null,
      },
      {
        name: 'empty string',
        input: {
          integersOnly: false,
          stringValue: '',
        },
        expected: [],
      },
    ];

    testDataSets.forEach((dataSet, i) => {
      it(`DataSet ${i} - ${dataSet.name}`, () => {
        component.integersOnly = dataSet.input.integersOnly;
        expect(component['parseNumericSet'](dataSet.input.stringValue)).toEqual(dataSet.expected);
      });
    });
  });
});
