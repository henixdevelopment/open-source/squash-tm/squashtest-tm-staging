import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AbstractFilterWidget } from '../abstract-filter-widget';
import { ListItem } from '../../../workspace-common/components/forms/grouped-multi-list/grouped-multi-list.component';
import { Filter, isDiscreteValue } from '../../state/filter.state';
import { ReferentialDataService } from '../../../../core/referential/services/referential-data.service';
import { of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { Milestone } from '../../../../model/milestone/milestone.model';
import { Scope } from '../../../../model/filter/filter.model';
import { DiscreteFilterValue } from '../../../../model/filter/filter-value.model';

@Component({
  selector: 'sqtm-core-filter-test-case-milestone-label',
  templateUrl: './filter-test-case-milestone-label.component.html',
  styleUrls: ['./filter-test-case-milestone-label.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterTestCaseMilestoneLabelComponent extends AbstractFilterWidget implements OnInit {
  items: ListItem[];

  private filterValue: DiscreteFilterValue[];

  constructor(
    protected cdRef: ChangeDetectorRef,
    private referentialDataService: ReferentialDataService,
  ) {
    super(cdRef);
  }

  ngOnInit(): void {}

  setFilter(filter: Filter, scope: Scope) {
    of(scope)
      .pipe(
        map((sc) => sc.value.map((entity) => entity.projectId)),
        switchMap((ids) => this.referentialDataService.findMilestonesInProjects(ids as number[])),
      )
      .subscribe((milestones: Milestone[]) => {
        const filterValue = filter.value;
        if (isDiscreteValue(filterValue)) {
          const ids = filterValue.value.map((value) => value.id);
          this.items = milestones.map((milestone) => {
            return {
              id: milestone.id,
              label: milestone.label,
              selected: ids.includes(milestone.id),
            };
          });
          this._filter = filter;
          this.filterValue = filterValue.value;
          this.cdRef.detectChanges();
        } else {
          throw Error('Not handled kind ' + filter.value.kind);
        }
      });
  }

  changeSelection(item: ListItem) {
    let nextValue: DiscreteFilterValue[] = [...this.filterValue];
    if (item.selected) {
      nextValue = this.filterValue.concat({ id: item.id, label: item.label });
    } else {
      const index = this.filterValue.findIndex((v) => v.id === item.id);
      if (index >= 0) {
        nextValue.splice(index, 1);
      }
    }
    this.filteringChanged.next({
      filterValue: {
        kind: 'multiple-discrete-value',
        value: nextValue,
      },
    });
  }
}
