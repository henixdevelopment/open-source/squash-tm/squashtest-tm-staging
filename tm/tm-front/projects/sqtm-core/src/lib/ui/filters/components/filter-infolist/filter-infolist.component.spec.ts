import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FilterInfolistComponent } from './filter-infolist.component';
import { TestingUtilsModule } from '../../../testing-utils/testing-utils.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('FilterInfolistComponent', () => {
  let component: FilterInfolistComponent;
  let fixture: ComponentFixture<FilterInfolistComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TestingUtilsModule],
      declarations: [FilterInfolistComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterInfolistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
