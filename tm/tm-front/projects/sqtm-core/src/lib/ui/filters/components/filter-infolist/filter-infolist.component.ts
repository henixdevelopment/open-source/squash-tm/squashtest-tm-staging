import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import {
  ListGroup,
  ListItem,
} from '../../../workspace-common/components/forms/grouped-multi-list/grouped-multi-list.component';
import {
  Filter,
  isDiscreteValue,
  MultiDiscreteFilterValue,
  ResearchColumnPrototype,
} from '../../state/filter.state';
import { of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { InfoList } from '../../../../model/infolist/infolist.model';
import { AbstractFilterWidget } from '../abstract-filter-widget';
import { InfoListAttribute } from '../../../../model/project/project.model';
import { Workspaces } from '../../../ui-manager/theme.model';
import { ReferentialDataService } from '../../../../core/referential/services/referential-data.service';
import { Scope } from '../../../../model/filter/filter.model';
import { DiscreteFilterValue } from '../../../../model/filter/filter-value.model';

type columnProtoRelatedToInfoList =
  | ResearchColumnPrototype.TEST_CASE_NATURE_ID
  | ResearchColumnPrototype.TEST_CASE_NATURE
  | ResearchColumnPrototype.TEST_CASE_TYPE_ID
  | ResearchColumnPrototype.TEST_CASE_TYPE
  | ResearchColumnPrototype.REQUIREMENT_CATEGORY
  | ResearchColumnPrototype.REQUIREMENT_VERSION_CATEGORY
  | ResearchColumnPrototype.REQUIREMENT_VERSION_CATEGORY_ID;

@Component({
  selector: 'sqtm-core-filter-infolist',
  templateUrl: './filter-infolist.component.html',
  styleUrls: ['./filter-infolist.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterInfolistComponent extends AbstractFilterWidget implements OnInit {
  private readonly infolistAttributeByColumnPrototype: {
    [K in columnProtoRelatedToInfoList]: InfoListAttribute;
  } = {
    REQUIREMENT_CATEGORY: 'requirementCategoryId',
    REQUIREMENT_VERSION_CATEGORY_ID: 'requirementCategoryId',
    REQUIREMENT_VERSION_CATEGORY: 'requirementCategoryId',
    TEST_CASE_NATURE_ID: 'testCaseNatureId',
    TEST_CASE_NATURE: 'testCaseNatureId',
    TEST_CASE_TYPE_ID: 'testCaseTypeId',
    TEST_CASE_TYPE: 'testCaseTypeId',
  };

  private readonly i18nPrefixByColumnPrototype: {
    [K in columnProtoRelatedToInfoList]:
      | 'test-case.type'
      | 'test-case.nature'
      | 'requirement.category';
  } = {
    REQUIREMENT_CATEGORY: 'requirement.category',
    REQUIREMENT_VERSION_CATEGORY_ID: 'requirement.category',
    REQUIREMENT_VERSION_CATEGORY: 'requirement.category',
    TEST_CASE_NATURE_ID: 'test-case.nature',
    TEST_CASE_NATURE: 'test-case.nature',
    TEST_CASE_TYPE_ID: 'test-case.type',
    TEST_CASE_TYPE: 'test-case.type',
  };

  items: ListItem[];
  groups: ListGroup[];
  workspace: Workspaces;

  private filterValue: DiscreteFilterValue[];

  constructor(
    protected cdRef: ChangeDetectorRef,
    private referentialDataService: ReferentialDataService,
  ) {
    super(cdRef);
  }

  ngOnInit(): void {}

  setFilter(filter: Filter, scope: Scope) {
    of(scope)
      .pipe(
        map((sc) => sc.value.map((entity) => entity.projectId)),
        switchMap((ids) => this.getInfoLists(ids, filter)),
      )
      .subscribe((infoLists: InfoList[]) => {
        const filterValue = filter.value;
        if (isDiscreteValue(filterValue)) {
          this.initializeItemsAndGroups(filterValue, infoLists, filter);
          this._filter = filter;
          this.filterValue = filterValue.value;
          this.workspace = filter.uiOptions?.workspace;
          this.cdRef.detectChanges();
        } else {
          throw Error('Not handled kind ' + filter.value.kind);
        }
      });
  }

  private initializeItemsAndGroups(
    filterValue: MultiDiscreteFilterValue,
    infoLists: InfoList[],
    filter: Filter,
  ) {
    const allGroups: ListGroup[] = [];
    const selectedIds = filterValue.value.map((v) => v.id);
    let isSystem;
    const allListItems: ListItem[] = [];
    infoLists.forEach((infoList) => {
      const listItems: ListItem[] = infoList.items.map((infoListItem) => {
        isSystem = infoListItem.system;
        const selectedAttribute = filter.infoListSelectorOptions?.selectedAttribute || 'id';
        const id = infoListItem[selectedAttribute];
        return {
          id: id,
          label: isSystem ? '' : infoListItem.label,
          i18nLabelKey: isSystem
            ? `sqtm-core.entity.${this.getI18nPrefix(filter)}.${infoListItem.code}`
            : '',
          groupId: infoList.id.toString(),
          selected: selectedIds.includes(id),
        };
      });
      allListItems.push(...listItems);
      allGroups.push({
        id: infoList.id.toString(),
        label: isSystem ? '' : infoList.label,
        i18nLabelKey: isSystem ? 'sqtm-core.generic.label.default-list' : '',
      });
    });
    this.items = allListItems.sort((a, b) => a.label.localeCompare(b.label));
    this.groups = allGroups.sort((a, b) => a.label.localeCompare(b.label));
  }

  private getInfoLists(ids: number[], filter: Filter) {
    return this.referentialDataService.findInfoListInProjects(
      ids as number[],
      this.infolistAttributeByColumnPrototype[filter.columnPrototype],
    );
  }

  changeSelection(item: ListItem) {
    let nextValue: DiscreteFilterValue[] = [...this.filterValue];
    if (item.selected) {
      nextValue = this.filterValue.concat({
        id: item.id,
        label: item.label,
        i18nLabelKey: item.i18nLabelKey,
      });
    } else {
      const index = this.filterValue.findIndex((v) => v.id === item.id);
      if (index >= 0) {
        nextValue.splice(index, 1);
      }
    }
    this.filteringChanged.next({
      filterValue: {
        kind: 'multiple-discrete-value',
        value: nextValue,
      },
    });
  }

  private getI18nPrefix(filter: Filter) {
    return this.i18nPrefixByColumnPrototype[filter.columnPrototype];
  }
}
