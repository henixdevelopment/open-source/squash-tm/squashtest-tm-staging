import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AbstractFilterWidget } from '../../abstract-filter-widget';
import { ReferentialDataService } from '../../../../../core/referential/services/referential-data.service';
import { Filter, isDiscreteValue } from '../../../state/filter.state';
import { take } from 'rxjs/operators';
import { ListItem } from '../../../../workspace-common/components/forms/grouped-multi-list/grouped-multi-list.component';
import { InputType } from '../../../../../model/customfield/input-type.model';
import { Workspaces } from '../../../../ui-manager/theme.model';
import { FilterOperation, Scope } from '../../../../../model/filter/filter.model';
import { DiscreteFilterValue } from '../../../../../model/filter/filter-value.model';

@Component({
  selector: 'sqtm-core-multi-value-cuf-filter',
  templateUrl: './multi-value-cuf-filter.component.html',
  styleUrls: ['./multi-value-cuf-filter.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MultiValueCufFilterComponent extends AbstractFilterWidget implements OnInit {
  items: ListItem[];

  private filterValue: DiscreteFilterValue[];

  operation: string;

  isTag = false;

  workspace: Workspaces;

  constructor(
    protected cdRef: ChangeDetectorRef,
    private referentialDataService: ReferentialDataService,
  ) {
    super(cdRef);
  }

  ngOnInit() {}

  setFilter(filter: Filter, _scope: Scope) {
    this.referentialDataService
      .connectToCustomField(filter.cufId)
      .pipe(take(1))
      .subscribe((cuf) => {
        const filterValue = filter.value;
        if (isDiscreteValue(filterValue)) {
          const selectedIds = filterValue.value.map((v) => v.id);
          this.items = cuf.options
            .filter((option) => Boolean(option.label))
            .map((option) => ({
              id: option.label,
              label: option.label,
              selected: selectedIds.includes(option.label),
            }));
          this.isTag = cuf.inputType === InputType.TAG;
          this._filter = filter;
          this.filterValue = filterValue.value;
          this.operation = filter.operation;
          this.workspace = filter.uiOptions?.workspace;
          this.cdRef.detectChanges();
        } else {
          throw Error('Not handled kind ' + filter.value.kind);
        }
      });
    this._filter = filter;
  }

  changeSelection(item: ListItem) {
    let nextValue: DiscreteFilterValue[] = [...this.filterValue];
    if (item.selected) {
      nextValue = this.filterValue.concat({
        id: item.id,
        label: item.label,
        i18nLabelKey: item.i18nLabelKey,
      });
    } else {
      const index = this.filterValue.findIndex((v) => v.id === item.id);
      if (index >= 0) {
        nextValue.splice(index, 1);
      }
    }
    this.filteringChanged.next({
      filterValue: { kind: 'multiple-discrete-value', value: nextValue },
    });
  }

  changeOperation() {
    const newOperation = FilterOperation[this.operation];
    this.filteringChanged.next({ operation: newOperation });
  }
}
