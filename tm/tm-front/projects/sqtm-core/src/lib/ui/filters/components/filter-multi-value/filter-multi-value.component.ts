import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AbstractFilterWidget } from '../abstract-filter-widget';
import { Filter, MultiDiscreteFilterValue } from '../../state/filter.state';
import { take } from 'rxjs/operators';
import { ItemListSearchProvider } from '../../item-list-search-provider.service';
import {
  ListGroup,
  ListItem,
} from '../../../workspace-common/components/forms/grouped-multi-list/grouped-multi-list.component';
import { Scope } from '../../../../model/filter/filter.model';
import { DiscreteFilterValue } from '../../../../model/filter/filter-value.model';

@Component({
  selector: 'sqtm-core-filter-multi-value',
  templateUrl: './filter-multi-value.component.html',
  styleUrls: ['./filter-multi-value.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterMultiValueComponent extends AbstractFilterWidget implements OnInit {
  items: ListItem[];
  groups: ListGroup[];
  private filterValue: DiscreteFilterValue[];

  constructor(
    protected cdRef: ChangeDetectorRef,
    private listProvider: ItemListSearchProvider,
  ) {
    super(cdRef);
  }

  ngOnInit(): void {}

  setFilter(filter: Filter, _scope: Scope) {
    this.listProvider
      .provideList(filter.id.toString())
      .pipe(take(1))
      .subscribe((providedItems) => {
        const selectedItemIds = (filter.value as MultiDiscreteFilterValue).value.map((v) => v.id);
        this.items = providedItems.map((item) => ({
          ...item,
          selected: selectedItemIds.includes(item.id),
        }));
        this.filterValue = (filter.value as MultiDiscreteFilterValue).value;
        this.cdRef.detectChanges();
      });
  }

  changeSelection($event: ListItem) {
    let nextValue: DiscreteFilterValue[] = [...this.filterValue];
    if ($event.selected) {
      nextValue = this.filterValue.concat({ id: $event.id, label: $event.label });
    } else {
      const index = this.filterValue.findIndex((v) => v.id === $event.id);
      if (index >= 0) {
        nextValue.splice(index, 1);
      }
    }
    this.filteringChanged.next({
      filterValue: {
        kind: 'multiple-discrete-value',
        value: nextValue,
      },
    });
  }
}
