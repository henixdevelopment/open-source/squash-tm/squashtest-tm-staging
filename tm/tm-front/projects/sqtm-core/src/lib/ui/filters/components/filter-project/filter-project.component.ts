import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AbstractFilterWidget } from '../abstract-filter-widget';
import { Filter, isDiscreteValue } from '../../state/filter.state';
import { ReferentialDataService } from '../../../../core/referential/services/referential-data.service';
import { map, take } from 'rxjs/operators';
import { ListItem } from '../../../workspace-common/components/forms/grouped-multi-list/grouped-multi-list.component';
import { DiscreteFilterValue } from '../../../../model/filter/filter-value.model';

@Component({
  selector: 'sqtm-core-filter-project',
  templateUrl: './filter-project.component.html',
  styleUrls: ['./filter-project.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterProjectComponent extends AbstractFilterWidget implements OnInit {
  items: ListItem[];

  private filterValue: DiscreteFilterValue[];

  constructor(
    protected cdRef: ChangeDetectorRef,
    private referentialDataService: ReferentialDataService,
  ) {
    super(cdRef);
  }

  ngOnInit() {}

  changeSelection(item: ListItem) {
    let nextValue: DiscreteFilterValue[] = [...this.filterValue];
    if (item.selected) {
      nextValue = this.filterValue.concat({ id: item.id, label: item.label });
    } else {
      const index = this.filterValue.findIndex((v) => v.id === item.id);
      if (index >= 0) {
        nextValue.splice(index, 1);
      }
    }
    this.filteringChanged.next({
      filterValue: { kind: 'multiple-discrete-value', value: nextValue },
    });
  }

  setFilter(filter: Filter) {
    this.referentialDataService.projectDatas$
      .pipe(
        take(1),
        map((projectDataMap) => Object.values(projectDataMap)),
      )
      .subscribe((projectDatas) => {
        const filterValue = filter.value;
        if (isDiscreteValue(filterValue)) {
          const ids = filterValue.value.map((value) => value.id);
          this.items = projectDatas
            .sort((pa, pb) => {
              return pa.name.localeCompare(pb.name);
            })
            .map((projectData) => {
              return {
                id: projectData.id,
                selected: ids.includes(projectData.id),
                label: projectData.name,
              };
            });
          this._filter = filter;
          this.filterValue = filterValue.value;
          this.cdRef.detectChanges();
        } else {
          throw Error('Not handled kind ' + filter.value.kind);
        }
      });
  }
}
