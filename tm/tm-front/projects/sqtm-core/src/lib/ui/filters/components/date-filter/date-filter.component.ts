import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { AbstractFilterWidget } from '../abstract-filter-widget';
import { Filter, isDateValue, isMultiDateValue } from '../../state/filter.state';

import { format, parse } from 'date-fns';
import { TranslateService } from '@ngx-translate/core';
import { DateFormatUtils } from '../../../../core/utils/date-format.utils';
import { getSupportedBrowserLang } from '../../../../core/utils/browser-langage.utils';
import { FilterOperation } from '../../../../model/filter/filter.model';

const DATE_FORMAT = 'yyyy-MM-dd';

@Component({
  selector: 'sqtm-core-date-filter',
  templateUrl: './date-filter.component.html',
  styleUrls: ['./date-filter.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DateFilterComponent extends AbstractFilterWidget implements OnInit, OnDestroy {
  startDate: Date = new Date();

  rangeDate: Date[] = [];

  operation: FilterOperation;

  between: FilterOperation = FilterOperation.BETWEEN;

  get datePickerFormat(): string {
    return DateFormatUtils.shortDate(getSupportedBrowserLang(this.translateService));
  }

  constructor(
    protected cdRef: ChangeDetectorRef,
    protected translateService: TranslateService,
  ) {
    super(cdRef);
  }

  ngOnInit() {}

  setFilter(filter: Filter) {
    this._filter = filter;
    this.operation = filter.operation;
    const filterValue = filter.value;

    if (isDateValue(filterValue)) {
      this.startDate = this.parseDateValue(filterValue.value);
      this.cdRef.detectChanges();
    } else if (isMultiDateValue(filterValue)) {
      this.rangeDate = filterValue.value.map((value) => this.parseDateValue(value));
      this.cdRef.detectChanges();
    } else {
      throw Error('Not handled kind ' + filterValue.kind);
    }
  }

  changeOperation(operation: FilterOperation) {
    this.operation = operation;
    this.cdRef.markForCheck();
  }

  changeValue() {
    if (this.operation === FilterOperation.BETWEEN) {
      const values = this.rangeDate.map((date) => this.formatDate(date));
      this.filteringChanged.next({
        filterValue: {
          kind: 'multiple-date-value',
          value: values,
        },
        operation: this.operation,
      });
    } else {
      if (this.startDate != null) {
        const value = this.formatDate(this.startDate);
        this.filteringChanged.next({
          filterValue: {
            kind: 'single-date-value',
            value: value,
          },
          operation: this.operation,
        });
      }
    }
    this.closeDateField();
  }

  closeDateField() {
    this.close.next();
  }

  parseDateValue(value: string) {
    if (!value) {
      return null;
    }
    return parse(value as string, DATE_FORMAT, new Date());
  }

  formatDate(date: Date) {
    return format(date, DATE_FORMAT);
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }
}
