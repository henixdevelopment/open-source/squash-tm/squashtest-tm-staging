import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CheckboxCufFilterComponent } from './checkbox-cuf-filter.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestingUtilsModule } from '../../../../testing-utils/testing-utils.module';

describe('CheckboxCufFilterComponent', () => {
  let component: CheckboxCufFilterComponent;
  let fixture: ComponentFixture<CheckboxCufFilterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TestingUtilsModule],
      declarations: [CheckboxCufFilterComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxCufFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
