import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { TranslateService } from '@ngx-translate/core';
import {
  AfterViewInit,
  ComponentRef,
  Directive,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import {
  FilterData,
  FilterGroup,
  FilteringChange,
  isDiscreteValue,
  isMultiStringValue,
  isStringValue,
  MultiDiscreteFilterValue,
  MultipleStringFilterValue,
} from '../state/filter.state';
import { AbstractFilterWidget } from './abstract-filter-widget';
import { DynamicComponentDirective } from '../../workspace-common/directives/dynamic-component.directive';
import { AbstractFilterValueRenderer } from './value-renderers/abstract-filter-value-renderer';
import { Subject } from 'rxjs';
import { ComponentPortal } from '@angular/cdk/portal';
import { takeUntil } from 'rxjs/operators';
import { DefaultValueRendererComponent } from './value-renderers/default-value-renderer/default-value-renderer.component';
import { FilterOperation } from '../../../model/filter/filter.model';

@Directive()
export abstract class AbstractFilterField implements AfterViewInit, OnDestroy, OnInit {
  private componentRef: ComponentRef<AbstractFilterWidget>;

  private _filterData: FilterData;

  @Input()
  filterGroups: FilterGroup[];

  @Input()
  allowDelete = true;

  @Input()
  preventOpening = false;

  @Input()
  lockedValue = false;

  @Output()
  valueChanged = new EventEmitter<FilteringChange>();

  @Output()
  filterInactivated = new EventEmitter<void>();

  @ViewChild('filterField', { read: ElementRef })
  private filterField: ElementRef;

  @ViewChild(DynamicComponentDirective, { static: true })
  dynamicContainer: DynamicComponentDirective<AbstractFilterValueRenderer>;

  // reference to the eventually rendered FilterValueRenderer
  private valueRendererComponentRef: ComponentRef<AbstractFilterValueRenderer>;

  private overlayRef: OverlayRef;

  private unsub$ = new Subject<void>();

  protected constructor(
    protected overlay: Overlay,
    protected translateService: TranslateService,
    protected vcr: ViewContainerRef,
  ) {}

  get filterData(): FilterData {
    return this._filterData;
  }

  @Input()
  set filterData(filterData: FilterData) {
    this._filterData = filterData;
    if (this.componentRef && this.overlayRef) {
      this.componentRef.instance.setFilter(filterData.filter, filterData.scope);
    }
    this.updateFilterValueComponent();
  }

  ngOnInit() {}

  edit() {
    if (this._filterData.filter.widget && !this.lockedValue) {
      const positionStrategy = this.overlay
        .position()
        .flexibleConnectedTo(this.filterField)
        .withPositions([
          {
            originX: 'center',
            overlayX: 'center',
            originY: 'bottom',
            overlayY: 'top',
            offsetY: 10,
          },
          { originX: 'end', overlayX: 'start', originY: 'center', overlayY: 'center', offsetX: 10 },
          {
            originX: 'center',
            overlayX: 'center',
            originY: 'top',
            overlayY: 'bottom',
            offsetY: -10,
          },
        ]);
      const overlayConfig: OverlayConfig = {
        positionStrategy,
        hasBackdrop: true,
        disposeOnNavigation: true,
        backdropClass: 'transparent-overlay-backdrop',
      };
      this.overlayRef = this.overlay.create(overlayConfig);
      const componentPortal = new ComponentPortal(this._filterData.filter.widget, this.vcr);
      this.componentRef = this.overlayRef.attach(componentPortal);
      this.componentRef.instance.setFilter(this._filterData.filter, this.filterData.scope);
      this.componentRef.instance.filteringChanged
        .pipe(takeUntil(this.unsub$))
        .subscribe((value) => this.changeFiltering(value));
      this.componentRef.instance.close.pipe(takeUntil(this.unsub$)).subscribe(() => this.close());
      this.overlayRef.backdropClick().subscribe(() => this.close());
    }
  }

  close() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
      this.componentRef = null;
    }
  }

  changeFiltering(value: FilteringChange) {
    this.valueChanged.next(value);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  updateFilterValueComponent() {
    if (!this.valueIsRendered()) {
      this.dynamicContainer.type =
        this.filterData.filter.valueRenderer || DefaultValueRendererComponent;
      const valueRendererComponentRef = this.dynamicContainer.instantiateComponent();
      this.valueRendererComponentRef = valueRendererComponentRef;
      this.valueRendererComponentRef.instance.filterInactivated
        .pipe(takeUntil(this.unsub$))
        .subscribe(() => this.doInactivateFilter());
      this.doUpdateRenderedFilterValue();
    } else {
      this.doUpdateRenderedFilterValue();
    }
  }

  private valueIsRendered() {
    return Boolean(this.valueRendererComponentRef);
  }

  getDisplayValue(): string {
    const filterValue = this.filterData.filter.value;
    if (isStringValue(filterValue)) {
      return filterValue.value || '';
    } else if (isMultiStringValue(filterValue)) {
      if (Boolean(filterValue) && filterValue.value.length > 0) {
        return this.buildMultiStringValue(filterValue, this.filterData.filter.operation);
      } else if (isDiscreteValue(filterValue)) {
        if (Boolean(filterValue) && filterValue.value.length > 0) {
          return this.buildDiscreteValue(filterValue);
        } else {
          return '';
        }
      }
    }
  }

  private buildDiscreteValue(filterValue: MultiDiscreteFilterValue) {
    const parts: string[] = filterValue.value.map((value) => {
      if (value.label) {
        return value.label;
      } else if (value.i18nLabelKey) {
        return this.translateService.instant(value.i18nLabelKey);
      } else {
        throw Error('Cannot display a value with no label and no i18nKey ' + value.id);
      }
    });

    parts.sort((a, b) => a.localeCompare(b));
    return parts.join(', ');
  }

  inactivateFilter($event: MouseEvent) {
    $event.stopPropagation();
    this.doInactivateFilter();
  }

  private doInactivateFilter() {
    this.close();
    this.filterInactivated.emit();
  }

  ngAfterViewInit(): void {
    if (!this.preventOpening) {
      this.edit();
    }
  }

  private buildMultiStringValue(
    filterValue: MultipleStringFilterValue,
    operation: FilterOperation,
  ): string {
    if (operation === FilterOperation.BETWEEN) {
      const and = this.translateService.instant('sqtm-core.generic.label.and');
      return `${filterValue.value[0]} ${and} ${filterValue.value[1]}`;
    } else {
      return filterValue.value.join(', ');
    }
  }

  private doUpdateRenderedFilterValue() {
    this.valueRendererComponentRef.instance.filter = this.filterData.filter;
    this.valueRendererComponentRef.instance.detectChanges();
  }
}
