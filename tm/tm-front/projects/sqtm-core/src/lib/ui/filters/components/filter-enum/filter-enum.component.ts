import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ListItem } from '../../../workspace-common/components/forms/grouped-multi-list/grouped-multi-list.component';
import { Filter, isDiscreteValue } from '../../state/filter.state';
import { AbstractFilterWidget } from '../abstract-filter-widget';
import { Workspaces } from '../../../ui-manager/theme.model';
import { SearchColumnPrototypeUtils } from '../../state/search-column-prototypes';
import { DiscreteFilterValue } from '../../../../model/filter/filter-value.model';

@Component({
  selector: 'sqtm-core-filter-enum',
  templateUrl: './filter-enum.component.html',
  styleUrls: ['./filter-enum.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterEnumComponent extends AbstractFilterWidget implements OnInit {
  items: ListItem[];
  workspace: Workspaces;

  private filterValue: DiscreteFilterValue[];

  constructor(protected cdRef: ChangeDetectorRef) {
    super(cdRef);
  }

  ngOnInit() {}

  changeSelection($event: ListItem) {
    const i18nEnum = SearchColumnPrototypeUtils.findEnum(this.filter.columnPrototype);
    const i18nEnumItem = i18nEnum[$event.id];
    let nextValue: DiscreteFilterValue[] = [...this.filterValue];
    if ($event.selected) {
      nextValue = this.filterValue.concat({
        id: i18nEnumItem.id,
        i18nLabelKey: i18nEnumItem.i18nKey,
      });
    } else {
      const index = this.filterValue.findIndex((v) => v.id === $event.id);
      if (index >= 0) {
        nextValue.splice(index, 1);
      }
    }
    this.filteringChanged.next({
      filterValue: { kind: 'multiple-discrete-value', value: nextValue },
    });
  }

  setFilter(filter: Filter) {
    const filterValue = filter.value;
    if (isDiscreteValue(filterValue)) {
      this._filter = filter;
      const ids = filterValue.value.map((value) => value.id);
      const i18nEnum = SearchColumnPrototypeUtils.findEnum(filter.columnPrototype);
      this.items = Object.values(i18nEnum).map((levelEnumItem) => {
        return {
          id: levelEnumItem.id,
          selected: ids.includes(levelEnumItem.id),
          i18nLabelKey: levelEnumItem.i18nKey,
        };
      });
      this.filterValue = filterValue.value;
      this.workspace = filter.uiOptions?.workspace;
      this.cdRef.detectChanges();
    } else {
      throw Error('Not handled kind ' + filter.value.kind);
    }
  }
}
