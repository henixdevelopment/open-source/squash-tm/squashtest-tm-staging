import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';

import { EditFilterTextComponent } from './edit-filter-text.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TestButton } from 'ngx-speculoos/lib/test-button';
import { OnPushComponentTester } from '../../../testing-utils/on-push-component-tester';
import { TestingUtilsModule } from '../../../testing-utils/testing-utils.module';

class EditFilterTextComponentTester extends OnPushComponentTester<EditFilterTextComponent> {
  getUpdateButton(): TestButton {
    return this.button('[data-test-element-id="update-button"]');
  }

  getCancelButton() {
    return this.button('[data-test-element-id="cancel-button"]');
  }
}

describe('EditFilterTextComponent', () => {
  let tester: EditFilterTextComponentTester;
  let fixture: ComponentFixture<EditFilterTextComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, FormsModule],
      declarations: [EditFilterTextComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFilterTextComponent);
    tester = new EditFilterTextComponentTester(fixture);
    tester.componentInstance.setFilter({
      id: 'thing',
      i18nLabelKey: '',
      value: {
        kind: 'single-string-value',
        value: 'Space Shuttle',
      },
      availableOperations: [],
    });

    fixture.detectChanges();
  });

  it('should show value', () => {
    expect(tester.componentInstance).toBeTruthy();
    expect(tester.componentInstance.value).toContain('Space Shuttle');
  });

  it('should close when clicking on cancel', fakeAsync(() => {
    let count = 0;
    expect(tester.componentInstance).toBeTruthy();
    tester.componentInstance.close.subscribe(() => {
      count++;
      expect(count).toEqual(1);
    });
    tester.getCancelButton().debugElement.nativeElement.click();
    tick();
  }));

  it('should fill and transfer value when clicking on update', fakeAsync(() => {
    tester.input('input').fillWith('Saturn 5');
    tick();
    tester.componentInstance.filteringChanged.subscribe((change) => {
      expect(change.filterValue.kind).toEqual('single-string-value');
      expect(change.filterValue.value).toEqual('saturn 5');
    });
    tester.getUpdateButton().debugElement.nativeElement.click();
    tick();
  }));
});
