import { TestBed } from '@angular/core/testing';

import { DialogService } from './dialog.service';
import { OverlayModule } from '@angular/cdk/overlay';

describe('DialogService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [OverlayModule],
      providers: [DialogService],
    }),
  );

  it('should be created', () => {
    const service: DialogService = TestBed.inject(DialogService);
    expect(service).toBeTruthy();
  });
});
