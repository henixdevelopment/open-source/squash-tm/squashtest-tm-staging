import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { CampaignTreeConfirmDeleteDialogComponent } from './campaign-tree-confirm-delete-dialog.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { DialogReference } from '../../model/dialog-reference';
import { CampaignTreeConfirmDeleteConfiguration } from './campaign-tree-confirm-delete-configuration';
import { EMPTY } from 'rxjs';
import { TestingUtilsModule } from '../../../testing-utils/testing-utils.module';

describe('DeleteConfirmDialogComponent', () => {
  let component: CampaignTreeConfirmDeleteDialogComponent;
  let fixture: ComponentFixture<CampaignTreeConfirmDeleteDialogComponent>;

  const overlayReference = jasmine.createSpyObj(['attachments']);

  overlayReference.attachments.and.returnValue(EMPTY);

  const dialogReference: DialogReference<CampaignTreeConfirmDeleteConfiguration, boolean> =
    new DialogReference<CampaignTreeConfirmDeleteConfiguration, boolean>(
      'delete',
      null,
      overlayReference,
      { titleKey: 'titleKey', level: 'DANGER', id: 'delete', showRemoveFromIterCheckbox: false },
    );

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, HttpClientTestingModule],
      providers: [
        {
          provide: DialogReference,
          useValue: dialogReference,
        },
      ],
      declarations: [CampaignTreeConfirmDeleteDialogComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignTreeConfirmDeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
