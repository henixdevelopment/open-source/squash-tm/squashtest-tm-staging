import { ChangeDetectionStrategy, Component, OnInit, ViewChild } from '@angular/core';
import { EntityCreationService } from '../../../../core/services/entity-creation.service';
import { DialogReference } from '../../model/dialog-reference';
import { DataRow } from '../../../grid/model/data-row.model';
import { EntityCreationDialogData } from './entity-creation-dialog-data';
import { AbstractCreateEntityDialog } from './abstract-create-entity-dialog';
import { AbstractCreateEntityForm } from '../create-entity-form/abstract-create-entity-form';
import { CreateEntityFormComponent } from '../create-entity-form/create-entity-form.component';
import { ActionErrorDisplayService } from '../../../../core/services/errors-handling/action-error-display.service';
import { SessionPingService } from '../../../../core/services/session-ping/session-ping.service';

@Component({
  selector: 'sqtm-core-create-entity-dialog',
  templateUrl: './create-entity-dialog.component.html',
  styleUrls: ['./create-entity-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateEntityDialogComponent extends AbstractCreateEntityDialog implements OnInit {
  @ViewChild(CreateEntityFormComponent)
  private form: CreateEntityFormComponent;

  constructor(
    dialogReference: DialogReference<EntityCreationDialogData, DataRow>,
    entityCreationService: EntityCreationService,
    actionErrorDisplayService: ActionErrorDisplayService,
    sessionPingService: SessionPingService,
  ) {
    super(dialogReference, entityCreationService, actionErrorDisplayService, sessionPingService);
  }

  get createEntityForm(): AbstractCreateEntityForm {
    return this.form;
  }
}
