import { ChangeDetectionStrategy, Component, HostListener, OnInit } from '@angular/core';

import { CampaignTreeConfirmDeleteConfiguration } from './campaign-tree-confirm-delete-configuration';
import { DialogReference } from '../../model/dialog-reference';
import { KeyNames } from '../../../utils/key-names';

@Component({
  selector: 'sqtm-core-confirm-delete-dialog',
  templateUrl: './campaign-tree-confirm-delete-dialog.component.html',
  styleUrls: ['./campaign-tree-confirm-delete-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CampaignTreeConfirmDeleteDialogComponent implements OnInit {
  configuration: CampaignTreeConfirmDeleteConfiguration;

  removeFromIter = false;

  constructor(
    private dialogReference: DialogReference<
      CampaignTreeConfirmDeleteConfiguration,
      { removeFromIter: boolean }
    >,
  ) {
    this.configuration = dialogReference.data;
  }

  ngOnInit() {}

  confirmDeletion() {
    this.dialogReference.result = { removeFromIter: this.removeFromIter };
    this.dialogReference.close();
  }

  @HostListener('window:keyup', ['$event'])
  handleKeyUp(event: KeyboardEvent) {
    if (event.key === KeyNames.ENTER) {
      this.confirmDeletion();
    } else if (event.key === KeyNames.ESCAPE) {
      this.dialogReference.close();
    }
  }

  changeRemoveFromIter(value: boolean) {
    this.removeFromIter = value;
  }
}
