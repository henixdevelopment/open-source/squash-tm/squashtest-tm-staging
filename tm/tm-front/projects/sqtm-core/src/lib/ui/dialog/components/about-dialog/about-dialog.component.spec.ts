import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AboutDialogComponent } from './about-dialog.component';
import { TranslateModule } from '@ngx-translate/core';
import { DialogReference } from '../../model/dialog-reference';
import { ReferentialDataService } from '../../../../core/referential/services/referential-data.service';
import createSpyObj = jasmine.createSpyObj;

describe('AboutDialogComponent', () => {
  let component: AboutDialogComponent;
  let fixture: ComponentFixture<AboutDialogComponent>;

  const referentialData = {} as ReferentialDataService;
  const dialogReference = createSpyObj(['close']);
  dialogReference.data = '2.0.0';
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [AboutDialogComponent],
      providers: [
        { provide: ReferentialDataService, useValue: referentialData },
        {
          provide: DialogReference,
          useValue: dialogReference,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
