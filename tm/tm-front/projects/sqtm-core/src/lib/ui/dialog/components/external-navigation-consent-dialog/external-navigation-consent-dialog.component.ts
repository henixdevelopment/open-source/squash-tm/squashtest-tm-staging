import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { DialogReference } from '../../model/dialog-reference';
import { TranslateService } from '@ngx-translate/core';
import {
  EXTERNAL_NAVIGATION_CONSENT_SERVICE_TOKEN,
  IExternalNavigationConsentService,
} from '../../../../core/services/external-navigation-consent/external-navigation-consent.constant';

export const EXTERNAL_NAVIGATION_CONSENT_DIALOG_ID = 'external-navigation-consent-dialog';

@Component({
  selector: 'sqtm-core-external-navigation-consent-dialog',
  templateUrl: './external-navigation-consent-dialog.component.html',
  styleUrl: './external-navigation-consent-dialog.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExternalNavigationConsentDialogComponent implements OnInit {
  protected readonly EXTERNAL_NAVIGATION_CONSENT_DIALOG_ID = EXTERNAL_NAVIGATION_CONSENT_DIALOG_ID;

  protected dontAskAgain = false;
  protected dontAskAgainLabel: string = '';

  constructor(
    @Inject(EXTERNAL_NAVIGATION_CONSENT_SERVICE_TOKEN)
    private readonly externalNavigationConsentService: IExternalNavigationConsentService,
    private readonly translateService: TranslateService,
    protected readonly dialogReference: DialogReference<
      ExternalNavigationConsentDialogConfiguration,
      boolean
    >,
  ) {}

  ngOnInit() {
    this.dontAskAgainLabel = this.translateService.instant(
      'sqtm-core.dialog.external-navigation-consent.do-not-ask-again',
      {
        domain: this.externalNavigationConsentService.findDomain(this.dialogReference.data.url),
      },
    );
  }

  confirm(): void {
    this.dialogReference.result = true;

    if (this.dontAskAgain) {
      this.externalNavigationConsentService.addTrustedUrl(this.dialogReference.data.url);
    }

    this.dialogReference.close();
  }
}

export interface ExternalNavigationConsentDialogConfiguration {
  url: string;
}
