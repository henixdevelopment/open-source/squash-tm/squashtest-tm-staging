import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'sqtm-core-dialog-footer',
  template: ` <ng-content></ng-content> `,
  styleUrls: ['./dialog-footer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DialogFooterComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
