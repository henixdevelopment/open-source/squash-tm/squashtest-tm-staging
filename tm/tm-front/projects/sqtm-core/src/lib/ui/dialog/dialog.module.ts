import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverlayModule } from '@angular/cdk/overlay';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputModule } from 'ng-zorro-antd/input';
import { TranslateModule } from '@ngx-translate/core';
import { DialogCloseDirective } from './directives/dialog-close.directive';
import { DialogHeaderDirective } from './directives/dialog-header.directive';
import { PortalModule } from '@angular/cdk/portal';
import { DialogComponent } from './components/dialog/dialog.component';
import { CreationDialogComponent } from './components/dialog-templates/creation-dialog/creation-dialog.component';
import { ConfirmDeleteDialogComponent } from './components/dialog-templates/confirm-delete-dialog/confirm-delete-dialog.component';
import { CustomisableDialogComponent } from './components/dialog-templates/customisable-dialog/customisable-dialog.component';
import { DialogBodyComponent } from './components/dialog-templates/dialog-body/dialog-body.component';
import { DialogHeaderComponent } from './components/dialog-templates/dialog-header/dialog-header.component';
import { DialogFooterComponent } from './components/dialog-templates/dialog-footer/dialog-footer.component';
import { CreateEntityDialogComponent } from './components/create-entity-dialog/create-entity-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomFieldModule } from '../custom-field/custom-field.module';
import { CKEditorModule } from 'ckeditor4-angular';
import { WorkspaceCommonModule } from '../workspace-common/workspace-common.module';
import { AlertDialogComponent } from './components/dialog-templates/alert-dialog/alert-dialog.component';
import { ConfirmDialogComponent } from './components/dialog-templates/confirm-dialog/confirm-dialog.component';
import { CreateEntityFormComponent } from './components/create-entity-form/create-entity-form.component';
import { CoverageVerifiedByDialogComponent } from './components/coverage-verified-by-dialog/coverage-verified-by-dialog.component';
import { RouterModule } from '@angular/router';
import { CoverageReportDialogComponent } from './components/coverage-report-dialog/coverage-report-dialog.component';
import { HttpErrorDialogComponent } from './components/dialog-templates/http-error-dialog/http-error-dialog.component';
import { AboutDialogComponent } from './components/about-dialog/about-dialog.component';
import { RequirementVersionLinkDialogComponent } from './components/requirement-version-link-dialog/requirement-version-link-dialog.component';
import { CampaignTreeConfirmDeleteDialogComponent } from './components/campaign-tree-confirm-delete-dialog/campaign-tree-confirm-delete-dialog.component';
import { AlertUserHasSyncsDialogComponent } from './components/alert-user-has-synchro/alert-user-has-syncs-dialog.component';
import { BindReqVersionToSprintDialogComponent } from './components/bind-req-version-to-sprint-dialog/bind-req-version-to-sprint-dialog.component';
import { ExternalNavigationConsentDialogComponent } from './components/external-navigation-consent-dialog/external-navigation-consent-dialog.component';

@NgModule({
  declarations: [
    BindReqVersionToSprintDialogComponent,
    DialogComponent,
    CreationDialogComponent,
    ConfirmDeleteDialogComponent,
    CustomisableDialogComponent,
    DialogBodyComponent,
    DialogHeaderComponent,
    DialogFooterComponent,
    DialogCloseDirective,
    DialogHeaderDirective,
    CreateEntityDialogComponent,
    AlertDialogComponent,
    ConfirmDialogComponent,
    CreateEntityFormComponent,
    CoverageVerifiedByDialogComponent,
    CoverageReportDialogComponent,
    HttpErrorDialogComponent,
    AboutDialogComponent,
    RequirementVersionLinkDialogComponent,
    CampaignTreeConfirmDeleteDialogComponent,
    AlertUserHasSyncsDialogComponent,
    ExternalNavigationConsentDialogComponent,
  ],
  exports: [
    BindReqVersionToSprintDialogComponent,
    DialogComponent,
    CreationDialogComponent,
    ConfirmDeleteDialogComponent,
    CustomisableDialogComponent,
    DialogBodyComponent,
    DialogHeaderComponent,
    DialogFooterComponent,
    DialogCloseDirective,
    DialogHeaderDirective,
    CreateEntityDialogComponent,
    AlertDialogComponent,
    ConfirmDialogComponent,
    CreateEntityFormComponent,
    CoverageVerifiedByDialogComponent,
    CoverageReportDialogComponent,
    AboutDialogComponent,
    RequirementVersionLinkDialogComponent,
    CampaignTreeConfirmDeleteDialogComponent,
    AlertUserHasSyncsDialogComponent,
  ],
  imports: [
    CommonModule,
    OverlayModule,
    NzDividerModule,
    TranslateModule.forChild(),
    NzIconModule,
    NzButtonModule,
    PortalModule,
    NzInputModule,
    ReactiveFormsModule,
    CustomFieldModule,
    CKEditorModule,
    WorkspaceCommonModule,
    RouterModule,
    NzCheckboxModule,
    FormsModule,
  ],
})
export class DialogModule {}
