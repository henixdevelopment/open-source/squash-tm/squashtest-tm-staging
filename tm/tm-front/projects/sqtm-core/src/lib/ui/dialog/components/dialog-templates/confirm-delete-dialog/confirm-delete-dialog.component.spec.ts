import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ConfirmDeleteDialogComponent } from './confirm-delete-dialog.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { DialogReference } from '../../../model/dialog-reference';
import { ConfirmDeleteConfiguration } from './confirm-delete-configuration';
import { EMPTY } from 'rxjs';
import { TestingUtilsModule } from '../../../../testing-utils/testing-utils.module';

describe('DeleteConfirmDialogComponent', () => {
  let component: ConfirmDeleteDialogComponent;
  let fixture: ComponentFixture<ConfirmDeleteDialogComponent>;

  const overlayReference = jasmine.createSpyObj(['attachments']);

  overlayReference.attachments.and.returnValue(EMPTY);

  const dialogReference: DialogReference<ConfirmDeleteConfiguration, boolean> = new DialogReference<
    ConfirmDeleteConfiguration,
    boolean
  >('delete', null, overlayReference, { titleKey: 'titleKey', level: 'DANGER', id: 'delete' });

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, HttpClientTestingModule],
      providers: [
        {
          provide: DialogReference,
          useValue: dialogReference,
        },
      ],
      declarations: [ConfirmDeleteDialogComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmDeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
