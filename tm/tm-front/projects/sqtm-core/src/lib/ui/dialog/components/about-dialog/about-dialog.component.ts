import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { DialogReference } from '../../model/dialog-reference';

@Component({
  selector: 'sqtm-core-about-dialog',
  templateUrl: './about-dialog.component.html',
  styleUrls: ['./about-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AboutDialogComponent implements OnInit {
  squashVersion: string;

  constructor(private dialogReference: DialogReference<string>) {
    this.squashVersion = this.dialogReference.data;
  }

  ngOnInit(): void {}
}
