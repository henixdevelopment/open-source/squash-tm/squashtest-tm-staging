import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HttpErrorDialogComponent } from './http-error-dialog.component';
import { EMPTY } from 'rxjs';
import { DialogReference } from '../../../model/dialog-reference';
import { HttpErrorDialogConfiguration } from './http-error-dialog-configuration';
import { TestingUtilsModule } from '../../../../testing-utils/testing-utils.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { HttpErrorResponse } from '@angular/common/http';

describe('HttpErrorDialogComponent', () => {
  let component: HttpErrorDialogComponent;
  let fixture: ComponentFixture<HttpErrorDialogComponent>;

  const overlayReference = jasmine.createSpyObj(['attachments']);

  overlayReference.attachments.and.returnValue(EMPTY);
  const httpError = new HttpErrorResponse({});
  const dialogReference: DialogReference<HttpErrorDialogConfiguration, boolean> =
    new DialogReference<HttpErrorDialogConfiguration, boolean>('delete', null, overlayReference, {
      id: 'error-dialog',
      error: httpError,
    });

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, HttpClientTestingModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: DialogReference,
          useValue: dialogReference,
        },
      ],
      declarations: [HttpErrorDialogComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HttpErrorDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
