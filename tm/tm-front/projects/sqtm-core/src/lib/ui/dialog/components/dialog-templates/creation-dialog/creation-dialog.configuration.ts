import { Type, ViewContainerRef } from '@angular/core';

export class CreationDialogConfiguration<D extends CreationDialogData> {
  formComponent: Type<any>;
  viewContainerReference?: ViewContainerRef;
  data: D;
}

export class CreationDialogData {
  id: string;
  titleKey: string;
  addAnotherLabelKey?: string;
  hideDescriptionField?: boolean;
}
