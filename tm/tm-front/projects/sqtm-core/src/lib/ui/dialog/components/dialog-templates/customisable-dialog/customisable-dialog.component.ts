import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
} from '@angular/core';
import { DialogFooterComponent } from '../dialog-footer/dialog-footer.component';
import { fromEvent, Subject } from 'rxjs';
import { KeyNames } from '../../../../utils/key-names';
import { DialogReference } from '../../../model/dialog-reference';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'sqtm-core-customisable-dialog',
  templateUrl: './customisable-dialog.component.html',
  styleUrls: ['./customisable-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomisableDialogComponent implements AfterViewInit, OnDestroy {
  @Input()
  dialogId: string;

  @Input()
  titleKey: string;

  @Input()
  allowConfirm: boolean = true;

  @Input()
  confirmLabelKey = 'sqtm-core.generic.label.confirm';

  @ContentChild(DialogFooterComponent)
  projectedFooter: DialogFooterComponent;

  @Output()
  confirm = new EventEmitter<void>();

  private unsub$ = new Subject<void>();

  constructor(private dialogReference: DialogReference) {}

  ngAfterViewInit(): void {
    if (this.projectedFooter == null) {
      fromEvent(window, 'keyup')
        .pipe(takeUntil(this.unsub$))
        .subscribe((event: KeyboardEvent) => {
          if (event.key === KeyNames.ENTER) {
            this.handleConfirm();
          } else if (event.key === KeyNames.ESCAPE) {
            this.dialogReference.result = null;
            this.dialogReference.close();
          }
        });
    }
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  handleConfirm() {
    this.confirm.next();
  }
}
