import {
  ChangeDetectorRef,
  Directive,
  Input,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { take } from 'rxjs/operators';
import { CustomField } from '../../../../model/customfield/customfield.model';
import {
  EntityCreationDialogDefaultValue,
  EntityCreationFormData,
  OptionalTextField,
} from '../create-entity-dialog/entity-creation-dialog-data';
import { TextFieldComponent } from '../../../workspace-common/components/forms/text-field/text-field.component';
import { CustomFieldFormComponent } from '../../../custom-field/custom-field-form/custom-field-form.component';
import { FieldValidationError } from '../../../../model/error/error.model';
import { convertFormControlValues } from '../../../custom-field/custom-field-value.utils';
import { dialogLogger } from '../../dialog.logger';
import { ReferentialDataService } from '../../../../core/referential/services/referential-data.service';
import { EntityFormModel } from '../../../../core/services/entity-creation.service';
import { AbstractFormField } from '../../../workspace-common/components/forms/abstract-form-field';
import { NOT_ONLY_SPACES_REGEX } from '../../../../core/utils/regex.utils';

const logger = dialogLogger.compose('AbstractCreateEntityForm');

@Directive()
export abstract class AbstractCreateEntityForm implements OnInit {
  formGroup: FormGroup;

  customFields: CustomField[] = [];

  @Input()
  data: EntityCreationFormData;

  @ViewChild('nameField')
  nameField: TextFieldComponent;

  @ViewChildren(AbstractFormField)
  allFields: QueryList<AbstractFormField>;

  @ViewChild(CustomFieldFormComponent, { read: CustomFieldFormComponent })
  customFieldForm: CustomFieldFormComponent;

  serverSideValidationErrors: FieldValidationError[] = [];

  get optionalTextFields(): OptionalTextField[] {
    return this.data.optionalTextFields || [];
  }

  protected constructor(
    protected fb: FormBuilder,
    protected translateService: TranslateService,
    protected referentialDataService: ReferentialDataService,
    protected cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.initializeForm();
    this.initializeCustomFieldForm();
  }

  private initializeForm() {
    const defaultValue = this.extractDefaultValueFromConfiguration();
    this.formGroup = this.fb.group({
      name: this.fb.control(defaultValue.name, [
        Validators.required,
        Validators.pattern(NOT_ONLY_SPACES_REGEX),
        Validators.maxLength(255),
      ]),
      description: this.fb.control(defaultValue.description),
    });
    this.initializeOptionalTextField();
    this.initializeAdditionalField();
  }

  private initializeOptionalTextField() {
    this.optionalTextFields.forEach((optionalTextField) => {
      const defaultValue = this.evaluateDefaultValue(optionalTextField.defaultValue);

      this.formGroup.addControl(
        optionalTextField.fieldName,
        this.fb.control(defaultValue, optionalTextField.validators || []),
      );
    });
  }

  private initializeCustomFieldForm() {
    if (this.data.bindableEntity && this.data.projectId) {
      this.findCustomFieldsForProjectAndDomain();
    }
  }

  private findCustomFieldsForProjectAndDomain() {
    const projectId = this.data.projectId;
    const bindableEntity = this.data.bindableEntity;
    this.referentialDataService
      .findCustomFieldByProjectIdAndDomain(projectId, bindableEntity)
      .pipe(take(1))
      .subscribe((customFields) => {
        this.customFields = customFields.filter((customField) => !customField.optional);
        logger.debug(`Loading mandatory cuf for domain  ${this.data.bindableEntity}`, [
          customFields,
        ]);
        this.cdr.detectChanges();
      });
  }

  validateForm(): boolean {
    if (this.formIsValid()) {
      return true;
    } else {
      this.showClientSideErrors();
      return false;
    }
  }

  private formIsValid() {
    return this.formGroup.status === 'VALID';
  }

  private showClientSideErrors() {
    this.allFields.forEach((item) => item.showClientSideError());
    if (this.customFieldForm) {
      this.customFieldForm.showClientSideErrors();
    }
  }

  showServerSideErrors(serverSideErrors: FieldValidationError[]) {
    this.serverSideValidationErrors = serverSideErrors;
    this.cdr.detectChanges();
  }

  resetForm(): void {
    this.formGroup.get('name').reset('');
    this.formGroup.get('description').reset('');
    Object.entries(this.getOptionalTextFieldWithDefaultValues()).forEach(
      ([fieldName, defaultValue]) => {
        this.formGroup.get(fieldName).reset(this.evaluateDefaultValue(defaultValue));
      },
    );

    if (this.customFieldForm != null) {
      this.customFieldForm.resetToDefaultValues();
    }

    // Some entity creation dialogs need additional processing after the dialog was reset (e.g. reference field for
    // iterations) so we need to notify them
    this.resetAdditionalField();
    this.showClientSideErrors();
    this.serverSideValidationErrors = [];
    this.cdr.markForCheck();
    this.nameField.grabFocus();
  }

  // Evaluate a default value based on its type (string or function returning a string)
  private evaluateDefaultValue(defaultValue: any): string {
    if (defaultValue == null) {
      return '';
    }

    switch (typeof defaultValue) {
      case 'string':
        return defaultValue;
      case 'function':
        return defaultValue();
      default:
        throw new Error(`Default value has unhandled default value ${defaultValue}`);
    }
  }

  private getOptionalTextFieldWithDefaultValues() {
    const fields = {};

    this.optionalTextFields.forEach((textField: OptionalTextField) => {
      fields[textField.fieldName] = textField.defaultValue;
    });

    return fields;
  }

  getLabelI18nKey(fieldName: string) {
    return `sqtm-core.entity.generic.${fieldName}.label`;
  }

  buildEntityFormModel(): Partial<EntityFormModel> {
    const customFieldsControls = this.formGroup.controls['customFields'] as FormGroup;
    const rawValueMap = convertFormControlValues(this.customFields, customFieldsControls);
    const formModel: Partial<EntityFormModel> = {
      name: this.formGroup.controls['name'].value,
      description: this.formGroup.controls['description'].value,
      customFields: rawValueMap,
    };

    this.optionalTextFields.forEach((value) => {
      formModel[value.fieldName] = this.formGroup.controls[value.fieldName].value;
    });

    this.addAdditionalField(formModel);

    return formModel;
  }

  private extractDefaultValueFromConfiguration(): EntityCreationDialogDefaultValue {
    const defaultValues: EntityCreationDialogDefaultValue = {
      name: '',
      description: '',
    };
    if (this.data.defaultValues) {
      defaultValues.name = this.evaluateDefaultValue(this.data.defaultValues.name);
      defaultValues.description = this.evaluateDefaultValue(this.data.defaultValues.description);
    }
    return defaultValues;
  }

  // extension point for additional fields
  protected initializeAdditionalField() {
    if (this.data.hasOptionalCopyTestPlanField) {
      this.formGroup.addControl('copyTestPlan', this.fb.control(true));
    }
  }

  // extension point to add additional fields values to EntityFormModel
  protected addAdditionalField(formModel: Partial<EntityFormModel>) {
    if (this.data.hasOptionalCopyTestPlanField) {
      formModel['copyTestPlan'] = this.formGroup.controls['copyTestPlan'].value;
    }
  }

  // extension point to reset additional fields
  protected resetAdditionalField() {
    if (this.data.hasOptionalCopyTestPlanField) {
      this.formGroup.get('copyTestPlan').reset(true);
    }
  }
}
