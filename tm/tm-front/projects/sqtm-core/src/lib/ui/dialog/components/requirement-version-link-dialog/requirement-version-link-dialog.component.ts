import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import {
  RequirementVersionLinkDialogConfiguration,
  RequirementVersionLinkDialogResult,
} from './requirement-version-link-dialog.configuration';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DialogReference } from '../../model/dialog-reference';
import { RequirementVersionLinkType } from '../../../../model/requirement/requirement-version-link-type.model';
import { TranslateService } from '@ngx-translate/core';
import { DisplayOption } from '../../../../model/display-option';

@Component({
  selector: 'sqtm-core-requirement-version-link-dialog',
  templateUrl: './requirement-version-link-dialog.component.html',
  styleUrls: ['./requirement-version-link-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RequirementVersionLinkDialogComponent implements OnInit {
  configuration: RequirementVersionLinkDialogConfiguration;

  formGroup: FormGroup;

  linkOptions: DisplayOption[] = [];

  constructor(
    private dialogReference: DialogReference<
      RequirementVersionLinkDialogConfiguration,
      RequirementVersionLinkDialogResult
    >,
    private translateService: TranslateService,
    private fb: FormBuilder,
  ) {
    this.configuration = dialogReference.data;
  }

  ngOnInit(): void {
    this.initializeFormGroup();
    this.initializeLinkOptions();
  }

  private initializeFormGroup() {
    const linkTypes = this.configuration.requirementVersionLinkTypes;
    const defaultType = linkTypes.filter((type) => type.default)[0];
    const defaultTypeId = `${defaultType.id}_0`;
    this.formGroup = this.fb.group({
      type: this.fb.control(defaultTypeId, [Validators.required]),
    });
  }

  private initializeLinkOptions() {
    this.linkOptions = this.getOptions(this.configuration.requirementVersionLinkTypes);
  }

  private getOptions(linkTypes: RequirementVersionLinkType[]): DisplayOption[] {
    const displayOptions: DisplayOption[] = [];
    for (const type of linkTypes) {
      displayOptions.push({
        id: `${type.id}_0`,
        label: `${this.getRoleLabel(type.role)} - ${this.getRoleLabel(type.role2)}`,
      });
      if (type.role !== type.role2) {
        displayOptions.push({
          id: `${type.id}_1`,
          label: `${this.getRoleLabel(type.role2)} - ${this.getRoleLabel(type.role)}`,
        });
      }
    }

    return displayOptions;
  }

  private getRoleLabel(role: string) {
    const isDefaultSystemRole = role.includes('requirement-version.link.type');

    if (isDefaultSystemRole) {
      return this.translateService.instant('sqtm-core.entity.requirement.' + role);
    } else {
      return role;
    }
  }

  confirmLink() {
    const linkType = this.formGroup.controls['type'].value;
    this.dialogReference.result = this.getLinkTypeAndDirection(linkType);
    this.dialogReference.close();
  }

  private getLinkTypeAndDirection(id: string): RequirementVersionLinkDialogResult {
    const idInfo = id.split('_');
    const direction = idInfo[1] !== '0';
    return { linkTypeId: Number.parseInt(idInfo[0], 10), linkDirection: direction };
  }
}
