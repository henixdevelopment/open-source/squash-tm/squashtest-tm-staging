export class ConfirmConfiguration {
  id: string;
  titleKey: string;
  level: 'WARNING' | 'DANGER' | 'INFO' = 'DANGER';
  messageKey?: string;
  confirmButtonText?: string;
  cancelButtonText?: string;
}

export const defaultConfirmConfiguration: Readonly<ConfirmConfiguration> = {
  id: 'confirm',
  titleKey: 'sqtm-core.generic.label.confirm',
  level: 'WARNING',
  messageKey: 'sqtm-core.dialog.confirm',
  confirmButtonText: 'sqtm-core.generic.label.confirm',
  cancelButtonText: 'sqtm-core.generic.label.cancel',
};
