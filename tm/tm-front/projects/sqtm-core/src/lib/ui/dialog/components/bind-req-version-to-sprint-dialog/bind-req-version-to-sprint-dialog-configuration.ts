export interface BindReqVersionToSprintDialogConfiguration {
  titleKey: string;
  highLevelReqVersions: string[];
  obsoleteReqVersions: string[];
  alreadyLinkedReqVersions: string[];
}
