import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { DialogReference } from '../../model/dialog-reference';
import { CoverageVerifiedByConfiguration } from './coverage-verified-by-configuration';
import { Router } from '@angular/router';

@Component({
  selector: 'sqtm-core-coverage-verified-by-dialog',
  templateUrl: './coverage-verified-by-dialog.component.html',
  styleUrls: ['./coverage-verified-by-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CoverageVerifiedByDialogComponent implements OnInit {
  configuration: CoverageVerifiedByConfiguration;

  constructor(
    public dialogReference: DialogReference<CoverageVerifiedByConfiguration>,
    private router: Router,
  ) {
    this.configuration = this.dialogReference.data;
  }

  ngOnInit(): void {}

  navigateToCalledTestCase(calledTcId) {
    this.dialogReference.close();
    this.router.navigate(['/test-case-workspace/test-case/', calledTcId]);
  }

  navigateToTestStep(stepIndex) {
    this.dialogReference.close();
    this.router.navigate([`detailed-test-step/${this.configuration.testCaseId}/step/`, stepIndex]);
  }
}
