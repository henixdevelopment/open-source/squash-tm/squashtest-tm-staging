import { ChangeDetectionStrategy, Component } from '@angular/core';
import { BindReqVersionToSprintDialogConfiguration } from './bind-req-version-to-sprint-dialog-configuration';
import { DialogReference } from '../../model/dialog-reference';

@Component({
  selector: 'sqtm-core-bind-req-version-to-sprint-dialog',
  templateUrl: './bind-req-version-to-sprint-dialog.component.html',
  styleUrls: ['./bind-req-version-to-sprint-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BindReqVersionToSprintDialogComponent {
  data: BindReqVersionToSprintDialogConfiguration;
  displayHighLevelReqVersionsDetails = false;
  displayObsoleteReqVersionsDetails = false;
  displayAlreadyLinkedReqVersionsDetails = false;

  constructor(
    private dialogReference: DialogReference<BindReqVersionToSprintDialogConfiguration, void>,
  ) {
    this.data = dialogReference.data;
  }

  toggleHighLevelReqVersionsDisplay() {
    this.displayHighLevelReqVersionsDetails = !this.displayHighLevelReqVersionsDetails;
  }

  toggleObsoleteReqVersionsDisplay() {
    this.displayObsoleteReqVersionsDetails = !this.displayObsoleteReqVersionsDetails;
  }

  toggleAlreadyLinkedReqVersionsDisplay() {
    this.displayAlreadyLinkedReqVersionsDetails = !this.displayAlreadyLinkedReqVersionsDetails;
  }

  getDetailDisplayMessage(shouldDisplay: boolean): string {
    return shouldDisplay
      ? 'sqtm-core.generic.label.hide-detail'
      : 'sqtm-core.generic.label.show-detail';
  }
}
