export type ConfirmDeleteLevel = 'WARNING' | 'DANGER' | 'INFO';

export class CampaignTreeConfirmDeleteConfiguration {
  id: string;
  titleKey: string;
  level: ConfirmDeleteLevel = 'DANGER';
  showRemoveFromIterCheckbox = false;
  messageKey?: string;
  simulationReportMessages?: string[];
  suffixMessageKey?: string;
}

export const defaultCampaignTreeDeleteConfirmConfiguration: Readonly<CampaignTreeConfirmDeleteConfiguration> =
  {
    id: 'confirm-delete',
    titleKey: 'sqtm-core.generic.label.confirm-deletion',
    level: 'DANGER',
    messageKey: 'sqtm-core.dialog.delete.definitive',
    showRemoveFromIterCheckbox: false,
  };
