import { Directive, HostListener, Inject, NgZone, Renderer2 } from '@angular/core';
import { Overlay } from '@angular/cdk/overlay';
import { fromEvent } from 'rxjs';
import { DOCUMENT } from '@angular/common';
import { take, takeUntil } from 'rxjs/operators';
import { DialogReference } from '../model/dialog-reference';
import { clamp } from 'lodash';

@Directive({
  selector: '[sqtmCoreDialogHeader]',
})
export class DialogHeaderDirective {
  private dragging = false;

  constructor(
    private dialogReference: DialogReference,
    private overlay: Overlay,
    @Inject(DOCUMENT) private document: Document,
    private zone: NgZone,
    private renderer: Renderer2,
  ) {}

  @HostListener('mousedown', ['$event'])
  onMouseDown(mouseDownEvent: MouseEvent) {
    if (!this.dragging) {
      this.zone.runOutsideAngular(() => {
        this.dragging = true;

        const initialDialogBounds = this.getDialogBounds();

        const mouseUpStream = fromEvent(this.document.documentElement, 'mouseup').pipe(take(1));

        const mouseMoveStream = fromEvent(this.document.documentElement, 'mousemove').pipe(
          takeUntil(mouseUpStream),
        );

        mouseMoveStream.subscribe((mouseMoveEvent: MouseEvent) => {
          this.preventSelection(mouseMoveEvent);
          this.moveDialog(mouseMoveEvent, mouseDownEvent, initialDialogBounds);
        });

        mouseUpStream.subscribe(() => {
          this.dragging = false;
          this.allowSelection();
        });
      });
    }
  }

  private moveDialog(
    mouseMoveEvent: MouseEvent,
    initialMouseDownEvent: MouseEvent,
    initialDialogBounds: DOMRect,
  ) {
    const dialogBounds = this.getDialogBounds();

    const initialMouseX = initialMouseDownEvent.clientX;
    const initialMouseY = initialMouseDownEvent.clientY;

    // Calculate the offset from the initial mouse position
    const offsetX = mouseMoveEvent.clientX - initialMouseX;
    const offsetY = mouseMoveEvent.clientY - initialMouseY;

    // Calculate the new desired position of the dialog
    const draggedTop = initialDialogBounds.top + offsetY;
    const draggedLeft = initialDialogBounds.left + offsetX;

    // Ensure the dialog does not go out of the screen
    const maxTop = window.innerHeight - dialogBounds.height;
    const maxLeft = window.innerWidth - dialogBounds.width;

    const top = clamp(draggedTop, 0, maxTop);
    const left = clamp(draggedLeft, 0, maxLeft);

    const positionStrategy = this.overlay
      .position()
      .global()
      .top(top + 'px')
      .left(left + 'px');

    const overlayRef = this.dialogReference.overlayReference;
    overlayRef.updatePositionStrategy(positionStrategy);
    overlayRef.updatePosition();
  }

  private getDialogBounds(): DOMRect {
    const dialogElement = this.dialogReference.containerReference.location
      .nativeElement as HTMLElement;
    return dialogElement.getBoundingClientRect();
  }

  private preventSelection(mouseEvent: MouseEvent) {
    mouseEvent.preventDefault();
    mouseEvent.stopPropagation();
    this.renderer.setStyle(this.document.body, 'user-select', 'none');
  }

  private allowSelection() {
    this.renderer.setStyle(this.document.body, 'user-select', '');
  }
}
