import { ViewContainerRef } from '@angular/core';
import { RequirementVersionLinkType } from '../../../../model/requirement/requirement-version-link-type.model';
import { BindReqVersionToSprintOperationReport } from '../../../../model/change-coverage-operation-report';
import { BindReqVersionToSprintDialogComponent } from '../bind-req-version-to-sprint-dialog/bind-req-version-to-sprint-dialog.component';
import { BindReqVersionToSprintDialogConfiguration } from '../bind-req-version-to-sprint-dialog/bind-req-version-to-sprint-dialog-configuration';
import { DialogConfiguration } from '../../services/dialog-feature.state';

export interface RequirementVersionLinkDialogConfiguration {
  titleKey: string;
  requirementVersionName: string;
  requirementVersionNodesName: string;
  requirementVersionLinkTypes: RequirementVersionLinkType[];
}

export interface RequirementVersionLinkDialogResult {
  linkTypeId: number;
  linkDirection: boolean;
}

export function createBindReqVersionToSprintOperationDialogConfiguration(
  operationReport: BindReqVersionToSprintOperationReport,
  vcr: ViewContainerRef,
): DialogConfiguration<BindReqVersionToSprintDialogConfiguration> {
  return {
    id: 'bind-req-version-to-sprint',
    component: BindReqVersionToSprintDialogComponent,
    viewContainerReference: vcr,
    data: createBindReqVersionToSprintConfiguration(operationReport),
    width: 650,
  };
}

export function createBindReqVersionToSprintConfiguration(
  operationReport: BindReqVersionToSprintOperationReport,
) {
  return {
    titleKey: 'sqtm-core.campaign-workspace.dialog.message.bind-req-version-to-sprint.title',
    highLevelReqVersions: operationReport.summary.highLevelReqVersionsInSelection,
    obsoleteReqVersions: operationReport.summary.obsoleteReqVersionsInSelection,
    alreadyLinkedReqVersions: operationReport.summary.reqVersionsAlreadyLinkedToSprint,
  };
}

export function shouldShowBindReqVersionToSprintDialog(
  operationReport: BindReqVersionToSprintOperationReport,
) {
  return (
    operationReport.summary.obsoleteReqVersionsInSelection.length > 0 ||
    operationReport.summary.reqVersionsAlreadyLinkedToSprint.length > 0 ||
    operationReport.summary.highLevelReqVersionsInSelection.length > 0
  );
}
