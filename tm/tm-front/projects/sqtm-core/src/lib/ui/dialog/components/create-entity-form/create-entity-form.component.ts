import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AbstractCreateEntityForm } from './abstract-create-entity-form';
import { ReferentialDataService } from '../../../../core/referential/services/referential-data.service';

@Component({
  selector: 'sqtm-core-create-entity-form',
  templateUrl: './create-entity-form.component.html',
  styleUrls: ['./create-entity-form.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateEntityFormComponent extends AbstractCreateEntityForm implements OnInit {
  constructor(
    protected fb: FormBuilder,
    protected translateService: TranslateService,
    protected referentialDataService: ReferentialDataService,
    protected cdr: ChangeDetectorRef,
  ) {
    super(fb, translateService, referentialDataService, cdr);
  }
}
