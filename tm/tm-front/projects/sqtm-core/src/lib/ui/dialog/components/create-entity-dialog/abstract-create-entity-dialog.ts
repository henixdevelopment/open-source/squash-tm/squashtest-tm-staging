import { Directive, OnDestroy, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import {
  EntityCreationService,
  EntityFormModel,
} from '../../../../core/services/entity-creation.service';
import { DialogReference } from '../../model/dialog-reference';
import { DataRow } from '../../../grid/model/data-row.model';
import { EntityCreationDialogData } from './entity-creation-dialog-data';
import { dialogLogger } from '../../dialog.logger';
import { Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { AbstractCreateEntityForm } from '../create-entity-form/abstract-create-entity-form';
import { ActionErrorDisplayService } from '../../../../core/services/errors-handling/action-error-display.service';
import { doesHttpErrorContainsSquashActionError } from '../../../../core/utils/http-error-utils';
import { SessionPingService } from '../../../../core/services/session-ping/session-ping.service';

const logger = dialogLogger.compose('CreateEntityDialogComponent');

@Directive()
export abstract class AbstractCreateEntityDialog implements OnInit, OnDestroy {
  data: EntityCreationDialogData;

  abstract get createEntityForm(): AbstractCreateEntityForm;

  asyncTaskIsRunning$ = new Subject<boolean>();

  protected constructor(
    protected dialogReference: DialogReference<EntityCreationDialogData, DataRow>,
    protected entityCreationService: EntityCreationService,
    protected actionErrorDisplayService: ActionErrorDisplayService,
    private readonly sessionPingService: SessionPingService,
  ) {
    this.data = dialogReference.data;
  }

  ngOnInit() {
    this.sessionPingService.beginLongTermEdition();
  }

  ngOnDestroy() {
    this.sessionPingService.endLongTermEdition();
  }

  addAnother() {
    this.createEntity(true);
  }

  createEntity(addAnother?: boolean) {
    if (this.createEntityForm.validateForm()) {
      const formModel = this.createEntityForm.buildEntityFormModel();
      formModel.parentEntityReference = this.dialogReference.data.parentEntityReference;
      this.persistEntity(formModel, addAnother);
    }
  }

  private persistEntity(formModel: Partial<EntityFormModel>, addAnother?: boolean) {
    logger.debug(`Try to add a new test case in ${formModel.parentEntityReference}. Model is : `, [
      formModel,
    ]);
    this.notifyAsyncTaskStarted();
    this.entityCreationService
      .persist(formModel, this.data.postUrl)
      .pipe(finalize(() => this.notifyAsyncTaskCompleted()))
      .subscribe({
        next: (dataRow: DataRow) => this.handleCreationSuccess(dataRow, addAnother),
        error: (error: HttpErrorResponse) => this.handleCreationFailure(error),
      });
  }

  private notifyAsyncTaskCompleted() {
    this.asyncTaskIsRunning$.next(false);
  }

  private notifyAsyncTaskStarted() {
    this.asyncTaskIsRunning$.next(true);
  }

  private handleCreationFailure(error: HttpErrorResponse) {
    logger.debug(`Server side error: `, [error]);
    if (error.status === 412) {
      const squashError = error.error.squashTMError;
      if (squashError.kind === 'FIELD_VALIDATION_ERROR') {
        this.createEntityForm.showServerSideErrors(squashError.fieldValidationErrors);
      }
      if (doesHttpErrorContainsSquashActionError(error)) {
        this.actionErrorDisplayService.showActionError(error);
      }
    }
  }

  private handleCreationSuccess(dataRow: DataRow, addAnother?: boolean) {
    this.dialogReference.result = dataRow;

    if (addAnother) {
      this.createEntityForm.resetForm();
      this.dialogReference.notifyFormReset();
    } else {
      this.dialogReference.close();
    }
  }
}
