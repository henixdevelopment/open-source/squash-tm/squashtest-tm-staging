import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { EMPTY } from 'rxjs';
import { CKEditorModule } from 'ckeditor4-angular';
import { CreateEntityDialogComponent } from './create-entity-dialog.component';
import { ReferentialDataService } from '../../../../core/referential/services/referential-data.service';
import { TestingUtilsModule } from '../../../testing-utils/testing-utils.module';
import { DialogReference } from '../../model/dialog-reference';
import { OverlayModule } from '@angular/cdk/overlay';
import createSpyObj = jasmine.createSpyObj;

describe('CreateEntityDialogComponent', () => {
  let component: CreateEntityDialogComponent;
  let fixture: ComponentFixture<CreateEntityDialogComponent>;

  const dialogReference = createSpyObj(['close']);
  dialogReference.data = { projectId: 1 };

  const referentialDataService = createSpyObj<ReferentialDataService>([
    'findCustomFieldByProjectIdAndDomain',
  ]);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        TranslateModule.forRoot(),
        HttpClientTestingModule,
        TestingUtilsModule,
        CKEditorModule,
        OverlayModule,
      ],
      providers: [
        {
          provide: DialogReference,
          useValue: dialogReference,
        },
        {
          provide: ReferentialDataService,
          useValue: referentialDataService,
        },
      ],
      declarations: [CreateEntityDialogComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    referentialDataService.findCustomFieldByProjectIdAndDomain.and.returnValue(EMPTY);
    fixture = TestBed.createComponent(CreateEntityDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
