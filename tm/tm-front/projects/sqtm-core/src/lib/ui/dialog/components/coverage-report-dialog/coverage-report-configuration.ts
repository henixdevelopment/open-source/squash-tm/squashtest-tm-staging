import { DialogConfiguration } from '../../services/dialog-feature.state';
import { CoverageReportDialogComponent } from './coverage-report-dialog.component';
import {
  ChangeCoverageOperationReport,
  ChangeLinkedRequirementOperationReport,
  ChangeOperationReport,
  ChangeVerifyingTestCaseOperationReport,
} from '../../../../model/change-coverage-operation-report';

export class CoverageReportConfiguration {
  messageKeys: string[];
  titleKey: string;
}

export function createTestCaseCoverageMessageDialogConfiguration(
  operationReport: ChangeCoverageOperationReport,
): DialogConfiguration<CoverageReportConfiguration> {
  return buildCoverageOperationReportMessageDialog(operationReport, 'test-case');
}

export function createTestStepCoverageMessageDialogConfiguration(
  operationReport: ChangeCoverageOperationReport,
): DialogConfiguration<CoverageReportConfiguration> {
  return buildCoverageOperationReportMessageDialog(operationReport, 'test-step');
}

export function createRequirementVersionVerifyingTestCasesMessageDialogConfiguration(
  operationReport: ChangeVerifyingTestCaseOperationReport,
): DialogConfiguration<CoverageReportConfiguration> {
  return buildVerifyingTestCaseOperationReportMessageDialog(operationReport);
}

export function createRequirementVersionLinksMessageDialogConfiguration(
  operationReport: ChangeLinkedRequirementOperationReport,
): DialogConfiguration<CoverageReportConfiguration> {
  return buildRequirementLinksOperationReportMessageDialog(operationReport);
}

export function shouldShowCoverageMessageDialog(operationReport: ChangeOperationReport): boolean {
  const summary = operationReport.summary;
  return (
    summary.alreadyVerifiedRejections ||
    summary.noVerifiableVersionRejections ||
    summary.notLinkableRejections
  );
}

export function shouldShowRequirementLinksMessageDialog(
  operationReport: ChangeLinkedRequirementOperationReport,
): boolean {
  const summary = operationReport.summary;
  return (
    summary.alreadyLinkedRejections ||
    summary.sameRequirementRejections ||
    summary.notLinkableRejections
  );
}

function buildCoverageOperationReportMessageDialog(
  operationReport: ChangeCoverageOperationReport,
  type: 'test-case' | 'test-step',
): DialogConfiguration<CoverageReportConfiguration> {
  const messageKeys = createCoverageReportMessageList(operationReport, type);
  return {
    id: 'coverage-report',
    component: CoverageReportDialogComponent,
    data: {
      messageKeys,
      titleKey:
        'sqtm-core.test-case-workspace.dialog.title.verified-requirement-version.add-summary',
    },
    width: 600,
  };
}

function buildVerifyingTestCaseOperationReportMessageDialog(
  operationReport: ChangeVerifyingTestCaseOperationReport,
): DialogConfiguration<CoverageReportConfiguration> {
  const messageKeys = createVerifyingTestCaseReportMessageList(operationReport);
  return {
    id: 'verifying-test-case-report',
    component: CoverageReportDialogComponent,
    data: {
      messageKeys,
      titleKey: 'sqtm-core.requirement-workspace.dialog.link-test-case.title',
    },
    width: 600,
  };
}

function buildRequirementLinksOperationReportMessageDialog(
  operationReport: ChangeLinkedRequirementOperationReport,
): DialogConfiguration<CoverageReportConfiguration> {
  const messageKeys = createRequirementLinksReportMessageList(operationReport);
  return {
    id: 'requirement-links-report',
    component: CoverageReportDialogComponent,
    data: {
      messageKeys,
      titleKey: 'sqtm-core.requirement-workspace.dialog.requirement-links.warning.title',
    },
    width: 600,
  };
}

function createCoverageReportMessageList(
  operationReport: ChangeCoverageOperationReport,
  type: 'test-case' | 'test-step',
) {
  const messageKeys = [];
  const summary = operationReport.summary;
  if (summary.alreadyVerifiedRejections) {
    messageKeys.push(
      `sqtm-core.test-case-workspace.dialog.message.verified-requirement-version.${type}.already-verified-rejection`,
    );
  }

  if (summary.noVerifiableVersionRejections) {
    messageKeys.push(
      `sqtm-core.test-case-workspace.dialog.message.verified-requirement-version.${type}.no-verifiable-version-rejection`,
    );
  }

  if (summary.notLinkableRejections) {
    messageKeys.push(
      `sqtm-core.test-case-workspace.dialog.message.verified-requirement-version.${type}.not-linkable-rejection`,
    );
  }

  return messageKeys;
}

function createVerifyingTestCaseReportMessageList(
  operationReport: ChangeVerifyingTestCaseOperationReport,
) {
  const messageKeys = [];
  const summary = operationReport.summary;
  if (summary.alreadyVerifiedRejections) {
    messageKeys.push(
      `sqtm-core.requirement-workspace.dialog.link-test-case.message.already-verified-rejection`,
    );
  }

  if (summary.noVerifiableVersionRejections) {
    messageKeys.push(
      `sqtm-core.requirement-workspace.dialog.link-test-case.message.no-verifiable-version-rejection`,
    );
  }

  if (summary.notLinkableRejections) {
    messageKeys.push(
      `sqtm-core.requirement-workspace.dialog.link-test-case.message.not-linkable-rejection`,
    );
  }

  return messageKeys;
}

function createRequirementLinksReportMessageList(
  operationReport: ChangeLinkedRequirementOperationReport,
) {
  const messageKeys = [];
  const summary = operationReport.summary;
  if (summary.sameRequirementRejections) {
    messageKeys.push(
      `sqtm-core.requirement-workspace.dialog.requirement-links.warning.message.same-requirement-rejection`,
    );
  }

  if (summary.alreadyLinkedRejections) {
    messageKeys.push(
      `sqtm-core.requirement-workspace.dialog.requirement-links.warning.message.already-linked-rejection`,
    );
  }

  if (summary.notLinkableRejections) {
    messageKeys.push(
      `sqtm-core.requirement-workspace.dialog.requirement-links.warning.message.not-linkable-rejection`,
    );
  }

  return messageKeys;
}
