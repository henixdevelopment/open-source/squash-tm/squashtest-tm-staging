import { ChangeDetectionStrategy, Component, HostListener, OnInit } from '@angular/core';
import { DialogReference } from '../../../model/dialog-reference';
import { ConfirmConfiguration } from './confirm-configuration';
import { KeyNames } from '../../../../utils/key-names';

@Component({
  selector: 'sqtm-core-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfirmDialogComponent implements OnInit {
  configuration: ConfirmConfiguration;

  constructor(private dialogReference: DialogReference<ConfirmConfiguration, boolean>) {
    this.configuration = dialogReference.data;
  }

  ngOnInit() {}

  confirm() {
    this.dialogReference.result = true;
    this.dialogReference.close();
  }

  @HostListener('window:keydown', ['$event'])
  handleKeyDown(event: KeyboardEvent) {
    if (event.key === KeyNames.ENTER) {
      this.confirm();
    } else if (event.key === KeyNames.ESCAPE) {
      this.dialogReference.close();
    }
  }
}
