import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AlertDialogComponent } from './alert-dialog.component';
import { EMPTY } from 'rxjs';
import { DialogReference } from '../../../model/dialog-reference';
import { AlertConfiguration } from './alert-configuration';
import { TestingUtilsModule } from '../../../../testing-utils/testing-utils.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('AlertDialogComponent', () => {
  let component: AlertDialogComponent;
  let fixture: ComponentFixture<AlertDialogComponent>;

  const overlayReference = jasmine.createSpyObj(['attachments']);

  overlayReference.attachments.and.returnValue(EMPTY);

  const dialogReference: DialogReference<AlertConfiguration, boolean> = new DialogReference<
    AlertConfiguration,
    boolean
  >('alert', null, overlayReference, { titleKey: 'titleKey', level: 'DANGER', id: 'alert' });

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, HttpClientTestingModule],
      providers: [
        {
          provide: DialogReference,
          useValue: dialogReference,
        },
      ],
      declarations: [AlertDialogComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
