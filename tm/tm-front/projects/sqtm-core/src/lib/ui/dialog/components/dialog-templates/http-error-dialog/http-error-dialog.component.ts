import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { DialogReference } from '../../../model/dialog-reference';
import { HttpErrorDialogConfiguration } from './http-error-dialog-configuration';
import { HttpErrorResponse } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import {
  doesHttpErrorContainsSquashActionError,
  extractSquashActionError,
} from '../../../../../core/utils/http-error-utils';

@Component({
  selector: 'sqtm-core-http-error-dialog',
  templateUrl: './http-error-dialog.component.html',
  styleUrls: ['./http-error-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HttpErrorDialogComponent implements OnInit {
  data: HttpErrorDialogConfiguration;

  constructor(
    private dialogReference: DialogReference<HttpErrorDialogConfiguration, boolean>,
    private translateService: TranslateService,
  ) {
    this.data = this.dialogReference.data;
  }

  ngOnInit(): void {}

  getErrorMessage(errorHttp: HttpErrorResponse) {
    if (errorHttp != null) {
      if (errorHttp.error?.message != null) {
        return errorHttp.error.message;
      }

      if (doesHttpErrorContainsSquashActionError(errorHttp)) {
        const actionKey = extractSquashActionError(errorHttp)?.actionValidationError?.i18nKey;

        if (actionKey) {
          return this.translateService.instant(actionKey);
        }
      }
    }

    return this.translateService.instant('sqtm-core.generic.label.exception.message');
  }
}
