import { InjectionToken } from '@angular/core';
import { OverlayRef } from '@angular/cdk/overlay';

export const OVERLAY_REF = new InjectionToken<OverlayRef>(
  'Token used to inject overlay reference in Portal Component',
);
