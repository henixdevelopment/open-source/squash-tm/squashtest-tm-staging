import { EntityState } from '@ngrx/entity';
import { Injector, Type, ViewContainerRef } from '@angular/core';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { DialogReference } from '../model/dialog-reference';

export interface DialogFeatureState {
  dialogs: DialogState;
}

interface DialogState extends EntityState<DialogReference> {}

export interface DialogConfiguration<D = any> {
  id: string;
  component: Type<any>;
  // optional parameter used to inject custom data into the dialog component
  data?: D;
  // you can pass a ViewContainerRef so the internal dialog component can access to the DI system at another level.
  viewContainerReference?: ViewContainerRef;
  // you can pass an Injector so the internal dialog component can access to the DI system at another level.
  injector?: Injector;
  // All dimension are assumed in pixel when type is number, as specified in CDK OverlayConfig Class
  width?: number | string;
  height?: number | string;
  minWidth?: number | string;
  minHeight?: number | string;
  maxWidth?: number | string;
  maxHeight?: number | string;
  top?: string;
}

export const DEFAULT_DIALOG_CONFIGURATION: Partial<DialogConfiguration> = {
  minWidth: 500,
  minHeight: 200,
};

export interface DialogClosePayload<R> {
  id: string;
  result: R;
}

const featureSelector = createFeatureSelector<DialogState>('dialogs');

export function createDialogSelector(id: string) {
  return createSelector(featureSelector, (dialogState) => dialogState[id]);
}
