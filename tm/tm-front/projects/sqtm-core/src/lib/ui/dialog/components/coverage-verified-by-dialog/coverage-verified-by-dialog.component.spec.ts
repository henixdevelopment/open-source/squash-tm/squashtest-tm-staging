import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CoverageVerifiedByDialogComponent } from './coverage-verified-by-dialog.component';
import { DialogReference } from '../../model/dialog-reference';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import createSpyObj = jasmine.createSpyObj;

describe('CoverageVerifiedByDialogComponent', () => {
  let component: CoverageVerifiedByDialogComponent;
  let fixture: ComponentFixture<CoverageVerifiedByDialogComponent>;

  const dialogReference = createSpyObj(['close']);
  dialogReference.data = {
    coverageStepInfoDto: [],
    calledTestCaseIds: [],
  };
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, TranslateModule.forRoot()],
      declarations: [CoverageVerifiedByDialogComponent],
      providers: [
        {
          provide: DialogReference,
          useValue: dialogReference,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoverageVerifiedByDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
