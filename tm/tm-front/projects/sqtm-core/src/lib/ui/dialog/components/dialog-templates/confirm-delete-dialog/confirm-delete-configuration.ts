export type ConfirmDeleteLevel = 'WARNING' | 'DANGER' | 'INFO';

export class ConfirmDeleteConfiguration {
  id: string;
  titleKey: string;
  level: ConfirmDeleteLevel = 'DANGER';
  messageKey?: string;
  simulationReportMessages?: string[];
  suffixMessageKey?: string;
}

export const defaultDeleteConfirmConfiguration: Readonly<ConfirmDeleteConfiguration> = {
  id: 'confirm-delete',
  titleKey: 'sqtm-core.generic.label.confirm-deletion',
  level: 'DANGER',
  messageKey: 'sqtm-core.dialog.delete.definitive',
};
