import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RequirementVersionLinkDialogComponent } from './requirement-version-link-dialog.component';
import { DialogReference } from '../../model/dialog-reference';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule } from '@angular/forms';
import createSpyObj = jasmine.createSpyObj;

describe('RequirementVersionLinkDialogComponent', () => {
  let component: RequirementVersionLinkDialogComponent;
  let fixture: ComponentFixture<RequirementVersionLinkDialogComponent>;
  const dialogReference = createSpyObj(['close']);
  dialogReference.data = {
    titleKey: '',
    requirementVersionName: '',
    requirementVersionNodesName: '',
    requirementVersionLinkTypes: [{ default: true, role: 'x', role2: 'y' }],
  };
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, TranslateModule.forRoot(), ReactiveFormsModule],
      declarations: [RequirementVersionLinkDialogComponent],
      providers: [{ provide: DialogReference, useValue: dialogReference }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementVersionLinkDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
