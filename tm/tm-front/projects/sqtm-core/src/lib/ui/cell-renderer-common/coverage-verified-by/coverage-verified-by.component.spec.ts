import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CoverageVerifiedByComponent } from './coverage-verified-by.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { GridTestingModule } from '../../grid/grid-testing/grid-testing.module';
import { TestingUtilsModule } from '../../testing-utils/testing-utils.module';
import { DialogService } from '../../dialog/services/dialog.service';

describe('CoverageVerifiedByComponent', () => {
  let component: CoverageVerifiedByComponent;
  let fixture: ComponentFixture<CoverageVerifiedByComponent>;
  const dialogService = jasmine.createSpyObj('dialogService', ['create']);
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, GridTestingModule, TranslateModule.forRoot()],
      providers: [TranslateService, { provide: DialogService, useValue: dialogService }],
      declarations: [CoverageVerifiedByComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoverageVerifiedByComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
