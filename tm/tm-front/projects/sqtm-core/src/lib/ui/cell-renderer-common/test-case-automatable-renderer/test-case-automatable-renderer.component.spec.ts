import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TestCaseAutomatableRendererComponent } from './test-case-automatable-renderer.component';

import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { OverlayModule } from '@angular/cdk/overlay';
import { GridTestingModule } from '../../grid/grid-testing/grid-testing.module';
import { TestingUtilsModule } from '../../testing-utils/testing-utils.module';

describe('TestCaseAutomatableRendererComponent', () => {
  let component: TestCaseAutomatableRendererComponent;
  let fixture: ComponentFixture<TestCaseAutomatableRendererComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [GridTestingModule, TestingUtilsModule, TranslateModule.forRoot(), OverlayModule],
      declarations: [TestCaseAutomatableRendererComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCaseAutomatableRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
