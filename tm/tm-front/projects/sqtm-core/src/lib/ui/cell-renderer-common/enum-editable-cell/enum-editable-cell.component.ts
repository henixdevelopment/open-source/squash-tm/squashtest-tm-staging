import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { Overlay } from '@angular/cdk/overlay';
import { GridService } from '../../grid/services/grid.service';
import { TranslateService } from '@ngx-translate/core';
import { I18nEnum, I18nEnumItem } from '../../../model/level-enums/level-enum';
import { ListPanelItem } from '../../workspace-common/components/forms/list-panel/list-panel.component';
import { I18nEnumOptions, isI18nEnumOptions } from '../../grid/model/column-definition.model';
import { ActionErrorDisplayService } from '../../../core/services/errors-handling/action-error-display.service';
import { AbstractListCellRendererComponent } from '../../grid/components/cell-renderers/abstract-list-cell-renderer/abstract-list-cell-renderer';
import { RestService } from '../../../core/services/rest.service';

@Component({
  selector: 'sqtm-core-enum-editable-cell',
  templateUrl: './enum-editable-cell.component.html',
  styleUrls: ['./enum-editable-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EnumEditableCellComponent extends AbstractListCellRendererComponent implements OnInit {
  @ViewChild('templatePortalContent', { read: TemplateRef })
  templatePortalContent: TemplateRef<any>;

  @ViewChild('containerRef', { read: ElementRef })
  containerRef: ElementRef;

  get options(): I18nEnumOptions {
    const options = this.columnDisplay.options;
    if (!options || !isI18nEnumOptions(options)) {
      throw Error('You must provide I18nEnumOptions');
    } else {
      return options;
    }
  }

  get i18nEnum(): I18nEnum<any> {
    return this.options.i18nEnum;
  }

  get i18nEnumItem(): I18nEnumItem<any> {
    return this.options.i18nEnum[this.enumItemKey];
  }

  get showIcon() {
    return this.isNotValidEnumKey(this.enumItemKey) ? false : this.options.showIcon;
  }

  get showLabel() {
    return this.isNotValidEnumKey(this.enumItemKey) ? true : this.options.showLabel;
  }

  constructor(
    public grid: GridService,
    public crRef: ChangeDetectorRef,
    public restService: RestService,
    public overlay: Overlay,
    public vcr: ViewContainerRef,
    public translateService: TranslateService,
    public errorDisplayService: ActionErrorDisplayService,
  ) {
    super(grid, crRef, overlay, vcr, translateService, restService, errorDisplayService);
  }

  ngOnInit(): void {}

  showTemplate() {
    if (this.isEditable()) {
      this.showList(this.containerRef, this.templatePortalContent);
    }
  }

  public get enumItemKey(): any {
    return this.row.data[this.columnDisplay.id];
  }

  getValues() {
    return Object.values(this.i18nEnum);
  }

  getLabel(key) {
    if (key == null || key === '') {
      return '-';
    } else if (this.isNotValidEnumKey(key)) {
      return key;
    } else {
      return this.translateService.instant(this.i18nEnum[key].i18nKey);
    }
  }

  isNotValidEnumKey(key) {
    return key == null || this.i18nEnum[key] == null;
  }

  getPanelItems(): ListPanelItem[] {
    const levelEnum: I18nEnumItem<any>[] = this.getValues();
    const panelItems: ListPanelItem[] = [];
    levelEnum.forEach((item) => {
      panelItems.push({
        id: item.id,
        label: this.translateService.instant(item.i18nKey),
      });
    });

    return panelItems;
  }

  get textClass() {
    return 'text-align-' + this.columnDisplay.contentPosition;
  }
}
