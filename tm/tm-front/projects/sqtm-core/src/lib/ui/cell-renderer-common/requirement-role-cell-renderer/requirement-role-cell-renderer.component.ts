import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { DataUpdateTextCellRendererComponent } from '../data-update-text-cell-renderer/data-update-text-cell-renderer.component';
import { DialogService } from '../../dialog/services/dialog.service';
import { GridService } from '../../grid/services/grid.service';
import { ColumnDefinitionBuilder } from '../../grid/model/column-definition.builder';
import { RestService } from '../../../core/services/rest.service';
import { GridColumnId } from '../../../shared/constants/grid/grid-column-id';

@Component({
  selector: 'sqtm-core-requirement-role-cell-renderer',
  template: `
    @if (columnDisplay && row) {
      <div class="full-width full-height flex-column">
        <sqtm-core-editable-text-field
          #editableTextField
          class="sqtm-grid-cell-txt-renderer m-auto-5"
          [showPlaceHolder]="false"
          [value]="requirementRole | translate"
          [layout]="'no-buttons'"
          [size]="'small'"
          [editable]="columnDisplay.editable"
          (confirmEvent)="updateValue($event)"
        >
        </sqtm-core-editable-text-field>
      </div>
    }
  `,
  styleUrls: ['./requirement-role-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RequirementRoleCellRendererComponent extends DataUpdateTextCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    public restService: RestService,
    public dialogService: DialogService,
  ) {
    super(grid, cdRef, restService, dialogService);
  }

  get requirementRole(): string {
    const requirementRole: string = this.row.data[this.columnDisplay.id];
    const isDefaultSystemRole = requirementRole.includes('requirement-version.link.type');

    if (isDefaultSystemRole) {
      return 'sqtm-core.entity.requirement.' + requirementRole;
    } else {
      return requirementRole;
    }
  }
}

export function requirementRoleColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(RequirementRoleCellRendererComponent);
}
