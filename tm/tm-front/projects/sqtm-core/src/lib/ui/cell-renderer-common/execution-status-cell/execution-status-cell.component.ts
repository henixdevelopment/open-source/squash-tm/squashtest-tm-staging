import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  compareLevels,
  ExecutionStatus,
  ExecutionStatusKeys,
} from '../../../model/level-enums/level-enum';
import { ColumnDefinitionBuilder } from '../../grid/model/column-definition.builder';
import { GridService } from '../../grid/services/grid.service';
import { AbstractCellRendererComponent } from '../../grid/components/cell-renderers/abstract-cell-renderer/abstract-cell-renderer.component';
import { StaticOrDynamicColumnId } from '../../../shared/constants/grid/grid-column-id';

@Component({
  selector: 'sqtm-core-execution-status-cell',
  template: ` @if (row) {
    <div class="full-width full-height flex-column">
      @if (row.data[columnDisplay.id] && row.data[columnDisplay.id] !== NONE_STATUS) {
        <div
          [attr.data-test-cell-id]="'execution-status-cell'"
          class="status-badge"
          [style.backgroundColor]="color"
          nz-tooltip
          [nzTooltipTitle]="i18nKey | translate | capitalize"
        ></div>
      } @else {
        <div
          class="no-execution"
          nz-tooltip
          [nzTooltipTitle]="'sqtm-core.generic.label.no-execution' | translate"
        >
          -
        </div>
      }
    </div>
  }`,
  styleUrls: ['./execution-status-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExecutionStatusCellComponent extends AbstractCellRendererComponent {
  public readonly NONE_STATUS = 'NONE';

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }

  get statusKey(): ExecutionStatusKeys {
    return this.row.data[this.columnDisplay.id];
  }

  get color(): string {
    return ExecutionStatus[this.statusKey].color;
  }

  get i18nKey(): string {
    return ExecutionStatus[this.statusKey].i18nKey;
  }
}

export function executionStatusColumn(id: StaticOrDynamicColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(ExecutionStatusCellComponent)
    .withHeaderPosition('center')
    .withSortFunction(sortExecutionStatus());
}

export function sortExecutionStatus() {
  return (keyA: any, keyB: any) => doSortExecutionStatus(keyA, keyB);
}

export function doSortExecutionStatus(keyA: any, keyB: any) {
  const noExecutionLevel = 0;
  const itemA = ExecutionStatus[keyA] ? ExecutionStatus[keyA].level : noExecutionLevel;
  const itemB = ExecutionStatus[keyB] ? ExecutionStatus[keyB].level : noExecutionLevel;
  return compareLevels(itemA, itemB);
}
