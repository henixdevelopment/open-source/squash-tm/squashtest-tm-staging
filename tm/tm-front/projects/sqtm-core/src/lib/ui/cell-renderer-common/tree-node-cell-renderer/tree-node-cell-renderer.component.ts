import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  NgZone,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { fromEvent, Subject, timer } from 'rxjs';
import { filter, switchMap, takeUntil, tap, withLatestFrom } from 'rxjs/operators';
import {
  ExecutionStatus,
  ExecutionStatusKeys,
  I18nEnumItem,
  LevelEnumItem,
  RequirementCriticality,
  RequirementCriticalityKeys,
  RequirementStatus,
  RequirementStatusKeys,
  SprintStatus,
  SprintStatusKeys,
  TestCaseImportanceKeys,
  TestCaseStatus,
  TestCaseStatusKeys,
  TestCaseWeight,
} from '../../../model/level-enums/level-enum';
import { GridService } from '../../grid/services/grid.service';
import { AbstractCellRendererComponent } from '../../grid/components/cell-renderers/abstract-cell-renderer/abstract-cell-renderer.component';
import { CampaignPermissions } from '../../../model/permissions/simple-permissions';
import { TestCaseKindKeys } from '../../../model/level-enums/test-case/test-case-kind';
import { isTreeNodeRendererDisplayOptions } from '../../grid/model/column-definition.model';
import { DataRowOpenState, SquashTmDataRowType } from '../../../model/grids/data-row.model';

@Component({
  selector: 'sqtm-core-tree-node-cell-renderer',
  templateUrl: './tree-node-cell-renderer.component.html',
  styleUrls: ['./tree-node-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TreeNodeCellRendererComponent
  extends AbstractCellRendererComponent
  implements AfterViewInit, OnDestroy
{
  // base width of 1 index increment, in pixels.
  readonly INDEX_BASE_WIDTH = 20;

  readonly SQTM_CORE_TREE_ICON_PREFIX = 'sqtm-core-tree';
  readonly SQTM_CORE_INFOLIST_ITEM_ICON_PREFIX = 'sqtm-core-infolist-item';

  private readonly INFOLIST_NOICON = 'noicon';

  private unsub$ = new Subject<void>();

  @ViewChild('openable', { read: ElementRef })
  containerRef: ElementRef;

  private isOver = false;

  get lastLineBackground(): string {
    if (this.depth === 0) {
      return '';
    } else {
      if (this.isLast) {
        return 'url("assets/sqtm-core/img/tree/line-l.svg")';
      } else {
        return 'url("assets/sqtm-core/img/tree/line-t.svg")';
      }
    }
  }

  get testCaseTooltipCoverageMessage(): string {
    return this.hasCoverage
      ? this.translateService.instant('sqtm-core.tree-picker.test-case.tooltip.some-req')
      : this.translateService.instant('sqtm-core.tree-picker.test-case.tooltip.no-req');
  }

  get testCaseTooltipStepMessage(): string {
    return this.hasStep
      ? this.translateService.instant('sqtm-core.tree-picker.test-case.tooltip.some-step')
      : this.translateService.instant('sqtm-core.tree-picker.test-case.tooltip.no-step');
  }

  get mustShowTestCaseTooltipStepMessage(): boolean {
    const testCaseKind: TestCaseKindKeys = this.row.data['TC_KIND'];
    return testCaseKind && (testCaseKind === 'STANDARD' || testCaseKind === 'KEYWORD');
  }

  get libraryIcon() {
    const { simplePermissions, type } = this.row;

    if (simplePermissions.canImport || simplePermissions.canDelete || simplePermissions.canCreate) {
      return `${this.SQTM_CORE_TREE_ICON_PREFIX}:project-crud`;
    }

    if (
      simplePermissions.canWrite ||
      simplePermissions.canAttach ||
      ([
        SquashTmDataRowType.CampaignLibrary,
        SquashTmDataRowType.RequirementLibrary,
        SquashTmDataRowType.TestCaseLibrary,
      ].includes(type) &&
        (simplePermissions as CampaignPermissions).canLink)
    ) {
      return `${this.SQTM_CORE_TREE_ICON_PREFIX}:project-write`;
    }

    if (
      type === SquashTmDataRowType.CampaignLibrary &&
      (simplePermissions as CampaignPermissions).canExecute
    ) {
      return `${this.SQTM_CORE_TREE_ICON_PREFIX}:project-exec`;
    }

    return `${this.SQTM_CORE_TREE_ICON_PREFIX}:project-read`;
  }

  get nodeName() {
    return this.row.data[this.columnDisplay.id];
  }

  get templateName(): string {
    let templateName;
    switch (this.row.type) {
      case SquashTmDataRowType.RequirementLibrary:
      case SquashTmDataRowType.TestCaseLibrary:
      case SquashTmDataRowType.CampaignLibrary:
      case SquashTmDataRowType.CustomReportLibrary:
      case SquashTmDataRowType.ActionWordLibrary:
        templateName = 'library';
        break;
      case SquashTmDataRowType.RequirementFolder:
        templateName = 'requirementFolder';
        break;
      case SquashTmDataRowType.TestCaseFolder:
      case SquashTmDataRowType.Directory:
      case SquashTmDataRowType.TAFolder:
      case SquashTmDataRowType.CampaignFolder:
      case SquashTmDataRowType.CustomReportFolder:
        templateName = 'simpleContainer';
        break;
      case SquashTmDataRowType.TestCase:
        templateName = 'testCase';
        break;
      case SquashTmDataRowType.Requirement:
        templateName = 'requirement';
        break;
      case SquashTmDataRowType.HighLevelRequirement:
        templateName = 'highLevelRequirement';
        break;
      case SquashTmDataRowType.TATest:
        templateName = 'taTest';
        break;
      case SquashTmDataRowType.TAProject:
        templateName = 'taProject';
        break;
      case SquashTmDataRowType.Campaign:
        templateName = 'campaign';
        break;
      case SquashTmDataRowType.Iteration:
        templateName = 'iteration';
        break;
      case SquashTmDataRowType.TestSuite:
        templateName = 'testSuite';
        break;
      case SquashTmDataRowType.Sprint:
        templateName = 'sprint';
        break;
      case SquashTmDataRowType.SprintGroup:
        templateName = 'sprintGroup';
        break;
      case SquashTmDataRowType.ChartDefinition:
      case SquashTmDataRowType.File:
      case SquashTmDataRowType.CustomReportCustomExport:
      case SquashTmDataRowType.CustomReportDashboard:
      case SquashTmDataRowType.ReportDefinition:
        templateName = 'customReportNode';
        break;
      case SquashTmDataRowType.ActionWord:
        templateName = 'actionWord';
        break;
      default:
        throw Error(`Unable to find template for type ${this.row.type}`);
    }
    return templateName;
  }

  get stateIcon() {
    if (this.row.state === DataRowOpenState.closed) {
      return 'plus';
    } else {
      return 'minus';
    }
  }

  get requirementBorderColor(): string {
    const criticality = this.requirementCriticality;
    if (criticality) {
      return criticality.color;
    }
  }

  get testCaseBorderColor(): string {
    const importance = this.testCaseImportance;
    if (importance) {
      return importance.color;
    }
  }

  get testCaseImportance() {
    const key = this.row.data['IMPORTANCE'] as TestCaseImportanceKeys;
    return TestCaseWeight[key];
  }

  get requirementCriticality() {
    const key = this.row.data['CRITICALITY'] as RequirementCriticalityKeys;
    return RequirementCriticality[key];
  }

  get testSuiteStatus() {
    const key = this.row.data['EXECUTION_STATUS'] as ExecutionStatusKeys;
    return ExecutionStatus[key];
  }

  get testSuiteBorderColor(): string {
    const executionStatus = this.testSuiteStatus;
    if (executionStatus) {
      return executionStatus.color;
    }
  }

  get testCaseStatusIcon(): string {
    const testCaseKind: TestCaseKindKeys = this.row.data['TC_KIND'];
    let iconName: string;
    switch (testCaseKind) {
      case 'STANDARD':
      case 'KEYWORD':
        if (this.hasStep) {
          iconName = this.hasCoverage
            ? `${this.SQTM_CORE_TREE_ICON_PREFIX}:test-case-status-coverage`
            : `${this.SQTM_CORE_TREE_ICON_PREFIX}:test-case-status-no-coverage`;
        } else {
          iconName = this.hasCoverage
            ? `${this.SQTM_CORE_TREE_ICON_PREFIX}:test-case-status-no-steps-coverage`
            : `${this.SQTM_CORE_TREE_ICON_PREFIX}:test-case-status-no-step`;
        }
        break;
      case 'EXPLORATORY':
      case 'GHERKIN':
        iconName = this.hasCoverage
          ? `${this.SQTM_CORE_TREE_ICON_PREFIX}:test-case-status-coverage`
          : `${this.SQTM_CORE_TREE_ICON_PREFIX}:test-case-status-no-coverage`;
        break;
      default:
        throw Error(`unknown test case kind ${testCaseKind}`);
    }
    return iconName;
  }

  // as specified whe use the same icons for requirements and test cases...
  get requirementStatusIcon(): string {
    if (!this.hasDescription) {
      return this.hasCoverage
        ? `${this.SQTM_CORE_TREE_ICON_PREFIX}:test-case-status-no-steps-coverage`
        : `${this.SQTM_CORE_TREE_ICON_PREFIX}:test-case-status-no-step`;
    } else {
      return this.hasCoverage
        ? `${this.SQTM_CORE_TREE_ICON_PREFIX}:test-case-status-coverage`
        : `${this.SQTM_CORE_TREE_ICON_PREFIX}:test-case-status-no-coverage`;
    }
  }

  get requirementTooltipDescriptionMessage(): string {
    return this.hasDescription
      ? this.translateService.instant('sqtm-core.tree-picker.requirement.tooltip.description')
      : this.translateService.instant('sqtm-core.tree-picker.requirement.tooltip.no-description');
  }

  get requirementTooltipCoverageMessage(): string {
    return this.hasCoverage
      ? this.translateService.instant('sqtm-core.tree-picker.requirement.tooltip.some-tc')
      : this.translateService.instant('sqtm-core.tree-picker.requirement.tooltip.no-tc');
  }

  get hasDescription(): boolean {
    return this.row.data['HAS_DESCRIPTION'];
  }

  get hasCoverage() {
    return this.coverageCount > 0;
  }

  get isSynchronized(): boolean {
    return this.row.data['IS_SYNCHRONIZED'];
  }

  get hasRemoteSynchronizationId(): boolean {
    return this.row.data['REMOTE_SYNCHRONISATION_ID'];
  }

  get isClosedSprint(): boolean {
    return this.row.data['REMOTE_STATE'] === SprintStatus.CLOSED.id;
  }

  get getLastSynchronizationStatusIconColor(): string {
    const lastSyncStatus = this.row.data['LAST_SYNC_STATUS'];
    const hasOutOfPerimeterOrDeletedRemoteReq =
      this.row.data['HAS_OUT_OF_PERIMETER_OR_DELETED_REMOTE_REQ'];

    if (lastSyncStatus === SynchronizationStatus.FAILURE || this.isDeletedSprint()) {
      return 'remote-sync-failed';
    } else if (
      lastSyncStatus === SynchronizationStatus.SUCCESS &&
      hasOutOfPerimeterOrDeletedRemoteReq
    ) {
      return 'remote-sync-partial';
    } else {
      return 'remote-sync-success';
    }
  }

  protected isDeletedSprint(): boolean {
    return this.row.type === SquashTmDataRowType.Sprint && this.row.data['IS_DELETED_SPRINT'];
  }

  get getRequirementSynchronizedStatusI18nKey(): string {
    const lastSyncStatus: string = this.row.data['LAST_SYNC_STATUS'];
    const hasOutOfPerimeterOrDeletedRemoteReq: boolean =
      this.row.data['HAS_OUT_OF_PERIMETER_OR_DELETED_REMOTE_REQ'];
    let synchronizationStatus: SynchronizationStatus;

    if (lastSyncStatus === SynchronizationStatus.FAILURE || this.isDeletedSprint()) {
      synchronizationStatus = SynchronizationStatus.FAILURE;
    } else if (
      lastSyncStatus === SynchronizationStatus.SUCCESS &&
      hasOutOfPerimeterOrDeletedRemoteReq
    ) {
      synchronizationStatus = SynchronizationStatus.PARTIAL;
    } else {
      synchronizationStatus = SynchronizationStatus.SUCCESS;
    }
    return this.translateService.instant(
      `sqtm-core.entity.requirement.synchronization-status.${synchronizationStatus}`,
    );
  }

  getRemoteReqPerimeterStatusIconColor(): string {
    const remoteReqPerimeterStatus = this.row.data['REMOTE_REQ_PERIMETER_STATUS'];
    switch (remoteReqPerimeterStatus) {
      case RemoteRequirementPerimeterStatus.IN_CURRENT_PERIMETER:
        return 'remote-req-in-perimeter';
      case RemoteRequirementPerimeterStatus.OUT_OF_CURRENT_PERIMETER:
        return 'remote-req-out-of-perimeter';
      case RemoteRequirementPerimeterStatus.NOT_FOUND:
        return 'remote-req-deleted';
      case RemoteRequirementPerimeterStatus.UNKNOWN:
        return '';
      default:
        throw Error(`unknown Remote Requirement Perimeter Status: ${remoteReqPerimeterStatus}`);
    }
  }

  getRemoteReqPerimeterStatusI18nKey(): string {
    const remoteReqPerimeterStatus = this.row.data['REMOTE_REQ_PERIMETER_STATUS'];
    return this.translateService.instant(
      `sqtm-core.entity.requirement.remote-req-perimeter-status.${remoteReqPerimeterStatus}`,
    );
  }

  get isRequirementUnselectable() {
    return !this.row.selectable || Boolean(this.row.data['INDIRECT_MILESTONE_BIND']);
  }

  get testCaseNameColor() {
    const testCaseKind = this.row.data['TC_KIND'] as TestCaseKindKeys;

    switch (testCaseKind) {
      case 'GHERKIN':
        return '#006c96';
      case 'KEYWORD':
        return '#5ab738';
      case 'EXPLORATORY':
        return '#a925af';
      default:
        return 'black';
    }
  }

  private get coverageCount() {
    return this.row.data['COVERAGE_COUNT'];
  }

  get hasStep() {
    return this.stepCount > 0;
  }

  private get stepCount() {
    return this.row.data['STEP_COUNT'];
  }

  get testCaseStatus(): LevelEnumItem<TestCaseStatusKeys> {
    const testCaseStatusKey: TestCaseStatusKeys = this.row.data['TC_STATUS'];
    return TestCaseStatus[testCaseStatusKey];
  }

  get testCaseStatusColor(): string {
    return this.testCaseStatus.color;
  }

  get testCaseNatureLabel(): string {
    const natureType = this.row.data['TC_NATURE_TYPE'];
    const natureLabel = this.row.data['TC_NATURE_LABEL'];
    return this.findInfoListItemLabel(natureType, natureLabel);
  }

  private findInfoListItemLabel(type: 'USR' | 'SYS', labelOrKey) {
    const isSystem = type && type === 'SYS';
    if (isSystem) {
      return this.translateService.instant(`sqtm-core.entity.${labelOrKey}`);
    } else {
      return labelOrKey;
    }
  }

  get requirementStatus(): LevelEnumItem<RequirementStatusKeys> {
    const requirementStatusKey: RequirementStatusKeys = this.row.data['REQUIREMENT_STATUS'];
    return RequirementStatus[requirementStatusKey];
  }

  get requirementStatusColor(): string {
    return this.requirementStatus.color;
  }

  get reqCategoryLabel(): string {
    const reqCategoryType = this.row.data['REQ_CATEGORY_TYPE'];
    const reqCategoryLabel = this.row.data['REQ_CATEGORY_LABEL'];
    return this.findInfoListItemLabel(reqCategoryType, reqCategoryLabel);
  }

  get depthArray(): boolean[] {
    return this.depthMap.slice(0, this.depth);
  }

  get sprintStatus(): I18nEnumItem<SprintStatusKeys> {
    const sprintStatusKey: SprintStatusKeys = this.row.data['STATUS'];
    return SprintStatus[sprintStatusKey];
  }

  get sprintBorderColor(): string {
    const sprintStatus = this.sprintStatus;
    if (sprintStatus) {
      return sprintStatus.color;
    }
  }

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    private translateService: TranslateService,
    private ngZone: NgZone,
  ) {
    super(grid, cdRef);
  }

  ngAfterViewInit(): void {
    this.ngZone.runOutsideAngular(() => {
      if (this.containerRef) {
        fromEvent(this.containerRef.nativeElement, 'mouseenter')
          .pipe(
            takeUntil(this.unsub$),
            tap(() => (this.isOver = true)),
            filter(() => this.row.state === DataRowOpenState.closed),
            withLatestFrom(this.grid.dragging$),
            filter(([_$event, dragging]) => dragging),
            switchMap(() => timer(600)),
            filter(() => this.isOver),
          )
          .subscribe(() => {
            this.ngZone.run(() => this.grid.openRow(this.row.id));
          });

        fromEvent(this.containerRef.nativeElement, 'mouseleave')
          .pipe(takeUntil(this.unsub$))
          .subscribe(() => (this.isOver = false));
      }
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  toggleRow($event: MouseEvent) {
    $event.stopPropagation();
    if (this.row.state === DataRowOpenState.closed) {
      this.grid.openRow(this.row.id);
    } else if (this.row.state === DataRowOpenState.open) {
      this.grid.closeRow(this.row.id);
    }
  }

  getVerticalLineBackground(index: number, ancestorIsLast: boolean): any {
    const style: any = {};
    if (index > 0 && !ancestorIsLast) {
      style['background-image'] = 'url("assets/sqtm-core/img/tree/line.svg")';
    }
    return style;
  }

  mustShowInfoListItemIcon(attributeName: string): boolean {
    const icon = this.row.data[attributeName];
    return Boolean(icon) && icon !== this.INFOLIST_NOICON;
  }

  getShowInfoListItemIcon(attributeName: string): string {
    return `${this.SQTM_CORE_INFOLIST_ITEM_ICON_PREFIX}:${this.row.data[attributeName]}`;
  }

  getCustomReportIcon() {
    const type = this.row.type;
    let iconName;
    switch (type) {
      case SquashTmDataRowType.ChartDefinition:
        iconName = 'chart';
        break;
      case SquashTmDataRowType.CustomReportCustomExport:
        iconName = 'custom_export';
        break;
      case SquashTmDataRowType.CustomReportDashboard:
        iconName = 'dashboard';
        break;
      case SquashTmDataRowType.ReportDefinition:
      case SquashTmDataRowType.File:
        iconName = 'report';
        break;
      default:
        throw Error(`No icon for type ${type}`);
    }
    return `${this.SQTM_CORE_TREE_ICON_PREFIX}:${iconName}`;
  }

  getTreeNodeClasses() {
    const options = this.columnDisplay.options;
    if (options && isTreeNodeRendererDisplayOptions(options)) {
      return options.ellipsisOnLeft ? 'txt-ellipsis-rtl' : 'txt-ellipsis';
    } else {
      return 'txt-ellipsis';
    }
  }
}

enum RemoteRequirementPerimeterStatus {
  UNKNOWN = 'UNKNOWN',
  IN_CURRENT_PERIMETER = 'IN_CURRENT_PERIMETER',
  OUT_OF_CURRENT_PERIMETER = 'OUT_OF_CURRENT_PERIMETER',
  NOT_FOUND = 'NOT_FOUND',
}

enum SynchronizationStatus {
  SUCCESS = 'SUCCESS',
  FAILURE = 'FAILURE',
  PARTIAL = 'PARTIAL',
}
