import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { GridColumnId } from '../../../shared/constants/grid/grid-column-id';
import { AbstractCellRendererComponent } from '../../grid/components/cell-renderers/abstract-cell-renderer/abstract-cell-renderer.component';
import { GridService } from '../../grid/services/grid.service';
import { BindableEntity } from '../../../model/bindable-entity.model';
import { DataRowSortFunction } from '../../grid/model/data-row.model';
import { ColumnDefinitionBuilder } from '../../grid/model/column-definition.builder';

@Component({
  selector: 'sqtm-core-custom-field-bound-entity-cell',
  template: ` @if (columnDisplay && row) {
    <div class="full-width full-height flex-column">
      <span
        class="m-auto-0"
        nz-tooltip
        [sqtmCoreLabelTooltip]="cellText | translate"
        [ellipsis]="true"
      >
        {{ cellText | translate }}
      </span>
    </div>
  }`,
  styleUrls: ['./custom-field-bound-entity-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomFieldBoundEntityCellComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }

  get cellText(): string {
    return 'sqtm-core.entity.custom-field.bound-entity.' + this.row.data[this.columnDisplay.id];
  }
}

// Map each bindable entity to a numeric rank for sorting
const boundEntitiesSortMap = {
  [BindableEntity.REQUIREMENT_FOLDER]: 0,
  [BindableEntity.REQUIREMENT_VERSION]: 1,
  [BindableEntity.TESTCASE_FOLDER]: 2,
  [BindableEntity.TEST_CASE]: 3,
  [BindableEntity.TEST_STEP]: 4,
  [BindableEntity.CAMPAIGN_FOLDER]: 5,
  [BindableEntity.CAMPAIGN]: 6,
  [BindableEntity.ITERATION]: 7,
  [BindableEntity.TEST_SUITE]: 8,
  [BindableEntity.EXECUTION]: 9,
  [BindableEntity.EXECUTION_STEP]: 10,
};

const boundEntitiesComparator: DataRowSortFunction = (first, second) => {
  const firstIndex = boundEntitiesSortMap[first] ?? 0;
  const secondIndex = boundEntitiesSortMap[second] ?? 0;
  return firstIndex - secondIndex;
};

export function customFieldBoundEntityColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(CustomFieldBoundEntityCellComponent)
    .withI18nKey('sqtm-core.entity.custom-field.bound-entity.label')
    .withSortFunction(boundEntitiesComparator);
}
