import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'sqtm-core-settings-icon',
  templateUrl: './settings-icon.component.html',
  styleUrl: './settings-icon.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SettingsIconComponent {
  @Input()
  show = true;

  @Output()
  openSettings = new EventEmitter<void>();

  constructor() {}

  handleClick() {
    this.openSettings.emit();
  }
}
