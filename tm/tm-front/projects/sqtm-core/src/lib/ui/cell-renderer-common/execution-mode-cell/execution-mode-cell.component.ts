import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { AbstractCellRendererComponent } from '../../grid/components/cell-renderers/abstract-cell-renderer/abstract-cell-renderer.component';
import { GridService } from '../../grid/services/grid.service';
import {
  TestCaseExecutionMode,
  TestCaseExecutionModeKeys,
} from '../../../model/level-enums/level-enum';
import { ColumnDefinitionBuilder } from '../../grid/model/column-definition.builder';

@Component({
  selector: 'sqtm-core-execution-mode-cell',
  template: ` @if (row) {
    <div class="full-width full-height flex-column">
      <sqtm-core-execution-mode
        style="margin: auto"
        [executionMode]="getExecutionMode(row.data[columnDisplay.id])"
      >
      </sqtm-core-execution-mode>
    </div>
  }`,
  styleUrls: ['./execution-mode-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExecutionModeCellComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }

  getExecutionMode(key: TestCaseExecutionModeKeys) {
    if (!this.row.data.testCaseDeleted) {
      return TestCaseExecutionMode[key];
    }
  }
}

export function executionModeColumn(columnId: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(columnId)
    .withRenderer(ExecutionModeCellComponent)
    .withHeaderPosition('center');
}
