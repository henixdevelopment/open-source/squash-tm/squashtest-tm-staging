import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { AbstractCellRendererComponent } from '../../grid/components/cell-renderers/abstract-cell-renderer/abstract-cell-renderer.component';
import { GridService } from '../../grid/services/grid.service';
import { isLinkOptions, LinkOptions } from '../../grid/model/column-definition.model';
import { ColumnDefinitionBuilder } from '../../grid/model/column-definition.builder';
import { GridColumnId } from '../../../shared/constants/grid/grid-column-id';

@Component({
  selector: 'sqtm-core-external-link-cell-renderer',
  template: `
    @if (row) {
      <div class="full-width full-height flex-column">
        @if (!getUrl()) {
          <span class="sqtm-grid-cell-txt-renderer m-auto-0">
            {{ row.data[columnDisplay.id] | translate }}
          </span>
        } @else {
          <a
            [href]="getUrl()"
            [target]="'_blank'"
            class="sqtm-grid-cell-txt-renderer m-auto-0"
            nz-tooltip
            [sqtmCoreLabelTooltip]="row.data[columnDisplay.id]"
            [nzTooltipTitle]=""
            [nzTooltipPlacement]="'topLeft'"
            >{{ row.data[columnDisplay.id] }}</a
          >
        }
      </div>
    }
  `,
  styleUrls: ['./external-link-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExternalLinkCellRendererComponent extends AbstractCellRendererComponent {
  constructor(grid: GridService, cdr: ChangeDetectorRef) {
    super(grid, cdr);
  }

  getUrl(): string | string[] {
    const linkOptions = this.columnDisplay.options;
    if (!linkOptions || !isLinkOptions(linkOptions)) {
      throw Error('This cell renderer must be provided with LinkOptions.');
    }

    if (linkOptions.createUrlFunction) {
      return linkOptions.createUrlFunction(this.row, this.columnDisplay);
    }

    if (linkOptions.columnParamId) {
      return this.row.data[linkOptions.columnParamId];
    } else {
      return this.row.data[this.columnDisplay.id];
    }
  }
}

export function externalLinkColumn(
  id: GridColumnId,
  options: LinkOptions,
): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(ExternalLinkCellRendererComponent)
    .withOptions(options);
}

export function basicExternalLinkColumn(id: GridColumnId): ColumnDefinitionBuilder {
  const options: LinkOptions = { kind: 'link' };
  return new ColumnDefinitionBuilder(id)
    .withRenderer(ExternalLinkCellRendererComponent)
    .withOptions(options);
}
