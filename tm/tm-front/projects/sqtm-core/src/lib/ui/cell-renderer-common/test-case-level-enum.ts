import { AbstractCellRendererComponent } from '../grid/components/cell-renderers/abstract-cell-renderer/abstract-cell-renderer.component';
import {
  ChangeDetectorRef,
  Directive,
  ElementRef,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import { GridService } from '../grid/services/grid.service';
import { RestService } from '../../core/services/rest.service';
import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { TemplatePortal } from '@angular/cdk/portal';
import { LevelEnumItem } from '../../model/level-enums/level-enum';
import { ListPanelItem } from '../workspace-common/components/forms/list-panel/list-panel.component';
import { TranslateService } from '@ngx-translate/core';
import { TableValueChange } from '../grid/model/actions/table-value-change';

@Directive()
export class TestCaseLevelEnumComponent extends AbstractCellRendererComponent {
  overlayRef: OverlayRef;

  constructor(
    public grid: GridService,
    private cdr: ChangeDetectorRef,
    public restService: RestService,
    public overlay: Overlay,
    public vcr: ViewContainerRef,
    public translateService: TranslateService,
  ) {
    super(grid, cdr);
  }

  showList(elementRef: ElementRef, templateRef: TemplateRef<any>) {
    const positionStrategy = this.overlay
      .position()
      .flexibleConnectedTo(elementRef)
      .withPositions([
        { originX: 'center', overlayX: 'start', originY: 'bottom', overlayY: 'top', offsetY: 10 },
        { originX: 'center', overlayX: 'start', originY: 'top', overlayY: 'bottom', offsetY: -10 },
      ]);
    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
      width: 200,
    };
    this.overlayRef = this.overlay.create(overlayConfig);
    const templatePortal = new TemplatePortal(templateRef, this.vcr);
    this.overlayRef.attach(templatePortal);
    this.overlayRef.backdropClick().subscribe(() => this.close());
  }

  close() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
    }
  }

  change($event) {
    const tableValueChange: TableValueChange = { columnId: this.columnDisplay.id, value: $event };
    this.grid.updateCellValue(this.row, this.columnDisplay, $event).subscribe(() => {
      this.grid.editRows([this.row.id], [tableValueChange]);
      this.close();
    });
  }

  getPanelItems(levelEnum: LevelEnumItem<any>[]): ListPanelItem[] {
    const panelItems: ListPanelItem[] = [];
    levelEnum.forEach((item) => {
      panelItems.push({
        id: item.id,
        label: this.translateService.instant(item.i18nKey),
      });
    });

    return panelItems;
  }
}
