import { ColumnDisplay } from '../grid/model/column-display.model';
import { DataRow } from '../grid/model/data-row.model';
import { RequirementStatus } from '../../model/level-enums/level-enum';
import { GridColumnId } from '../../shared/constants/grid/grid-column-id';

export function isTestCaseImportanceEditable(columnDisplay: ColumnDisplay, row: DataRow): boolean {
  return isTestCaseEditable(columnDisplay, row) && !row.data[GridColumnId.importanceAuto];
}

export function isTestCaseEditable(columnDisplay: ColumnDisplay, row: DataRow): boolean {
  const noLockedMilestones = row.data[GridColumnId.tcMilestoneLocked] === 0;

  return columnDisplay.editable && row.simplePermissions.canWrite && noLockedMilestones;
}

// status edition is not locked by status...
export function isRequirementStatusEditable(columnDisplay: ColumnDisplay, row: DataRow): boolean {
  return baseRequirementEditable(columnDisplay, row);
}

// all other attributes are locked by status, perm and milestone
export function isRequirementEditable(columnDisplay: ColumnDisplay, row: DataRow): boolean {
  const editable = baseRequirementEditable(columnDisplay, row);
  const statusKey = row.data[GridColumnId.status];
  const statusAllowModifications = RequirementStatus[statusKey].allowModifications;
  return editable && statusAllowModifications;
}

function baseRequirementEditable(columnDisplay: ColumnDisplay, row: DataRow): boolean {
  const milestoneLocked = row.data[GridColumnId.reqMilestoneLocked] || 0;
  return columnDisplay.editable && row.simplePermissions.canWrite && milestoneLocked === 0;
}
