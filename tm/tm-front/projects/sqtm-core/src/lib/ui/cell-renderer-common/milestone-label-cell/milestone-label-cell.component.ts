import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { AbstractCellRendererComponent } from '../../grid/components/cell-renderers/abstract-cell-renderer/abstract-cell-renderer.component';
import { GridService } from '../../grid/services/grid.service';
import { ColumnDefinitionBuilder } from '../../grid/model/column-definition.builder';
import { DatePipe } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { getSupportedBrowserLang } from '../../../core/utils/browser-langage.utils';
import { GridColumnId } from '../../../shared/constants/grid/grid-column-id';

export const MILESTONE_MIN_DATE = 'milestoneMinDate';
export const MILESTONE_MAX_DATE = 'milestoneMaxDate';

@Component({
  selector: 'sqtm-core-coverage-milestone',
  template: ` @if (row) {
    <div class="full-width full-height flex-column">
      <span
        [class.disabled-row]="row.disabled"
        class="txt-ellipsis"
        style="margin: auto 5px;"
        nz-tooltip
        [nzTooltipTitle]="tooltipString"
      >
        {{ displayString }}
      </span>
    </div>
  }`,
  styleUrls: ['./milestone-label-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MilestoneLabelCellComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    private datePipe: DatePipe,
    private translateService: TranslateService,
  ) {
    super(grid, cdRef);
  }

  get tooltipString(): string {
    const startDate = this.formatDate(this.row.data[MILESTONE_MIN_DATE]);
    const endDate = this.formatDate(this.row.data[MILESTONE_MAX_DATE]);

    if (startDate === endDate) {
      return startDate ? `${this.displayString} ${startDate}` : '-';
    }

    return `${this.displayString} ${startDate} - ${endDate}`;
  }

  get displayString(): string {
    const value = this.row.data[this.columnDisplay.id];
    if (value == null || value === '') {
      return '-';
    } else {
      return value;
    }
  }

  private formatDate(date: Date): string {
    return this.datePipe.transform(
      date,
      'shortDate',
      '',
      getSupportedBrowserLang(this.translateService),
    );
  }
}

export function milestoneLabelColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(MilestoneLabelCellComponent)
    .withI18nKey('sqtm-core.entity.milestone.label.plural');
}
