import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
  Fixed,
  IconLinkOptions,
  isIconLinkOptions,
} from '../../grid/model/column-definition.model';
import { AbstractCellRendererComponent } from '../../grid/components/cell-renderers/abstract-cell-renderer/abstract-cell-renderer.component';
import { GridService } from '../../grid/services/grid.service';
import { ColumnDefinitionBuilder } from '../../grid/model/column-definition.builder';
import { GridColumnId } from '../../../shared/constants/grid/grid-column-id';

@Component({
  selector: 'sqtm-core-link-icon-cell',
  template: `
    @if (row) {
      <div class="full-width full-height flex-column">
        <a
          class="current-workspace-main-color vertical-center"
          style="margin:auto;"
          [routerLink]="getUrl()"
          target="_blank"
        >
          <i nz-icon [nzType]="options.iconName" [nzTheme]="'fill'"> </i>
        </a>
      </div>
    }
  `,
  styleUrls: ['./link-icon-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LinkIconCellComponent extends AbstractCellRendererComponent {
  get options(): IconLinkOptions {
    const options = this.columnDisplay.options;

    if (!options || !isIconLinkOptions(options)) {
      throw Error('You must provide IconLinkOptions');
    } else {
      return options;
    }
  }

  constructor(
    public grid: GridService,
    private cdr: ChangeDetectorRef,
  ) {
    super(grid, cdr);
  }

  getUrl() {
    if (this.options.columnParamId) {
      return this.options.baseUrl + '/' + this.row.data[this.options.columnParamId];
    } else {
      return this.options.baseUrl;
    }
  }
}

export function iconLinkColumn(
  id: GridColumnId,
  options: IconLinkOptions,
): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(LinkIconCellComponent)
    .withOptions(options)
    .disableHeader()
    .changeWidthCalculationStrategy(new Fixed(35));
}
