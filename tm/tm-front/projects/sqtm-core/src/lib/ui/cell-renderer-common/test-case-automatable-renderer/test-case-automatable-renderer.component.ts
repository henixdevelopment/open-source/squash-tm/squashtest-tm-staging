import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { Observable, Subject } from 'rxjs';
import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { TemplatePortal } from '@angular/cdk/portal';
import { AbstractCellRendererComponent } from '../../grid/components/cell-renderers/abstract-cell-renderer/abstract-cell-renderer.component';
import { GridService } from '../../grid/services/grid.service';
import { RestService } from '../../../core/services/rest.service';
import { ReferentialDataService } from '../../../core/referential/services/referential-data.service';
import { TableValueChange } from '../../grid/model/actions/table-value-change';
import {
  LevelEnumItem,
  TestCaseAutomatable,
  TestCaseAutomatableKeys,
} from '../../../model/level-enums/level-enum';
import { ProjectData } from '../../../model/project/project-data.model';
import { ListPanelItem } from '../../workspace-common/components/forms/list-panel/list-panel.component';
import { ColumnDefinitionBuilder } from '../../grid/model/column-definition.builder';
import { GridColumnId } from '../../../shared/constants/grid/grid-column-id';
import { TestCaseKind } from '../../../model/level-enums/test-case/test-case-kind';

@Component({
  selector: 'sqtm-core-test-case-automatable-renderer',
  template: ` @if (row) {
    @if (getProject(row.projectId) | async; as projectData) {
      @if (row) {
        <div
          class="{{ initHover(projectData) }} sqtm-grid-cell-txt-renderer full-height flex-column"
        >
          <span #automatableRef (click)="showList(projectData)">{{
            getTranslatedRowValue(row.data[columnDisplay.id], projectData)
          }}</span>
          <ng-template #templatePortalContent>
            <sqtm-core-list-panel
              [selectedItem]="row.data[columnDisplay.id]"
              (itemSelectionChanged)="change($event)"
              [items]="getPanelItems()"
            ></sqtm-core-list-panel>
          </ng-template>
        </div>
      }
    }
  }`,
  styleUrls: ['./test-case-automatable-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestCaseAutomatableRendererComponent
  extends AbstractCellRendererComponent
  implements OnDestroy
{
  overlayRef: OverlayRef;

  unsub$ = new Subject<void>();

  @ViewChild('templatePortalContent', { read: TemplateRef })
  templatePortalContent: TemplateRef<any>;

  @ViewChild('automatableRef', { read: ElementRef })
  automatableRef: ElementRef;

  constructor(
    public grid: GridService,
    public cdr: ChangeDetectorRef,
    public translateService: TranslateService,
    public restService: RestService,
    private referentialDataService: ReferentialDataService,
    private overlay: Overlay,
    private vcr: ViewContainerRef,
  ) {
    super(grid, cdr);
  }

  getProject(id: number): Observable<ProjectData> {
    return this.referentialDataService.connectToProjectData(id);
  }

  change($event) {
    const changedValue: TableValueChange = { columnId: this.columnDisplay.id, value: $event };
    this.grid.updateCellValue(this.row, this.columnDisplay, $event).subscribe(() => {
      this.grid.editRows([this.row.id], [changedValue]);
      this.close();
    });
  }

  getValues() {
    return Object.values(TestCaseAutomatable);
  }

  getLabel(item: LevelEnumItem<TestCaseAutomatableKeys>): string {
    return this.translateService.instant(item.i18nKey);
  }

  getTranslatedRowValue(rowValue: string, projectData: ProjectData) {
    const isExploratoryTestCase = this.row.data[GridColumnId.kind] === TestCaseKind.EXPLORATORY.id;
    if (projectData.allowAutomationWorkflow && rowValue && !isExploratoryTestCase) {
      return this.translateService.instant(TestCaseAutomatable[rowValue].i18nKey);
    } else {
      return '-';
    }
  }

  initHover(projectData: ProjectData) {
    return this.canEdit(projectData) ? '__hover_pointer' : '';
  }

  getPanelItems(): ListPanelItem[] {
    const panelItems: ListPanelItem[] = [];
    const values = this.getValues();
    values.forEach((item) => {
      panelItems.push({
        id: item.id,
        label: this.translateService.instant(item.i18nKey),
      });
    });

    return panelItems;
  }

  showList(projectData: ProjectData) {
    const isExploratory = this.row.data[GridColumnId.kind] === TestCaseKind.EXPLORATORY.id;

    if (this.canEdit(projectData) && !isExploratory) {
      const positionStrategy = this.overlay
        .position()
        .flexibleConnectedTo(this.automatableRef)
        .withPositions([
          { originX: 'center', overlayX: 'start', originY: 'bottom', overlayY: 'top', offsetY: 10 },
          {
            originX: 'center',
            overlayX: 'start',
            originY: 'top',
            overlayY: 'bottom',
            offsetY: -10,
          },
        ]);
      const overlayConfig: OverlayConfig = {
        positionStrategy,
        hasBackdrop: true,
        disposeOnNavigation: true,
        backdropClass: 'transparent-overlay-backdrop',
        width: 200,
      };

      this.overlayRef = this.overlay.create(overlayConfig);
      const templatePortal = new TemplatePortal(this.templatePortalContent, this.vcr);
      this.overlayRef.attach(templatePortal);
      this.overlayRef.backdropClick().subscribe(() => this.close());
    }
  }

  close() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
    }
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private canEdit(projectData: ProjectData): boolean {
    const noLockedMilestones =
      this.row.data[GridColumnId.tcMilestoneLocked] === 0 &&
      this.row.data[GridColumnId.reqMilestoneLocked] === 0;
    return (
      this.row.simplePermissions.canWrite &&
      projectData.allowAutomationWorkflow &&
      noLockedMilestones
    );
  }
}

export function testCaseSearchAutomatable(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(TestCaseAutomatableRendererComponent);
}
