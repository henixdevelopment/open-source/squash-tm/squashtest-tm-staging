import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { DataRow } from '../../grid/model/data-row.model';
import { DataRowOpenState } from '../../../model/grids/data-row.model';
import { GridService } from '../../grid/services/grid.service';
import { AbstractCellRendererComponent } from '../../grid/components/cell-renderers/abstract-cell-renderer/abstract-cell-renderer.component';

@Component({
  selector: 'sqtm-core-open-close-cell-renderer',
  templateUrl: './open-close-cell-renderer.component.html',
  styleUrls: ['./open-close-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OpenCloseCellRendererComponent extends AbstractCellRendererComponent {
  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
  ) {
    super(grid, cdRef);
  }

  iconName(dataRow: DataRow) {
    if (dataRow.state === DataRowOpenState.open) {
      return 'down';
    } else {
      return 'right';
    }
  }

  toggleRow($event, dataRow: DataRow) {
    if (dataRow.state === DataRowOpenState.closed) {
      this.grid.openExternalRow(this.row.id);
    } else if (dataRow.state === DataRowOpenState.open) {
      this.grid.closeRow(this.row.id);
    }
  }
}
