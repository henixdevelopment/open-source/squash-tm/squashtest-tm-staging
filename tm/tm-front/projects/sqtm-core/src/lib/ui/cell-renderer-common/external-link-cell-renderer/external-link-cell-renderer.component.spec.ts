import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalLinkCellRendererComponent } from './external-link-cell-renderer.component';
import { GridTestingModule } from '../../grid/grid-testing/grid-testing.module';
import { TestingUtilsModule } from '../../testing-utils/testing-utils.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ExternalLinkCellRendererComponent', () => {
  let component: ExternalLinkCellRendererComponent;
  let fixture: ComponentFixture<ExternalLinkCellRendererComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TestingUtilsModule, HttpClientTestingModule, GridTestingModule],
      declarations: [ExternalLinkCellRendererComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalLinkCellRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
