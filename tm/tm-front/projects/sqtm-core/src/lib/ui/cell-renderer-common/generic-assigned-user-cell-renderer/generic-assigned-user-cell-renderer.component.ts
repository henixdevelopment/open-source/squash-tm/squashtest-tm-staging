import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Inject,
  InjectionToken,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { ConnectedPosition, Overlay, OverlayRef } from '@angular/cdk/overlay';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { catchError, filter, finalize, take } from 'rxjs/operators';
import { AbstractListCellRendererComponent } from '../../grid/components/cell-renderers/abstract-list-cell-renderer/abstract-list-cell-renderer';
import { ListPanelItem } from '../../workspace-common/components/forms/list-panel/list-panel.component';
import { GridService } from '../../grid/services/grid.service';
import { RestService } from '../../../core/services/rest.service';
import { ActionErrorDisplayService } from '../../../core/services/errors-handling/action-error-display.service';
import { ColumnDefinitionBuilder } from '../../grid/model/column-definition.builder';
import { formatFullUserName, SimpleUser } from '../../../model/user/user.model';
import { TableValueChange } from '../../grid/model/actions/table-value-change';
import { GridColumnId } from '../../../shared/constants/grid/grid-column-id';

@Component({
  selector: 'sqtm-core-generic-assigned-user-cell-renderer',
  templateUrl: './generic-assigned-user-cell-renderer.component.html',
  styleUrls: ['./generic-assigned-user-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GenericAssignedUserCellRendererComponent
  extends AbstractListCellRendererComponent
  implements OnInit
{
  overlayRef: OverlayRef;
  @ViewChild('templatePortalContent', { read: TemplateRef })
  templatePortalContent: TemplateRef<any>;

  @ViewChild('availableUsers', { read: ElementRef })
  availableUsers: ElementRef;

  readonly rowHeight = 29;

  panelItems: ListPanelItem[] = [];

  canEdit$: Observable<boolean>;

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    public readonly translateService: TranslateService,
    public readonly overlay: Overlay,
    public readonly vcr: ViewContainerRef,
    public readonly restService: RestService,
    public readonly actionErrorDisplayService: ActionErrorDisplayService,
    @Inject(ASSIGNED_USER_DELEGATE) public delegate: AssignedUserDelegate,
  ) {
    super(grid, cdRef, overlay, vcr, translateService, restService, actionErrorDisplayService);
    this.canEdit$ = this.delegate.canAssign$;
  }

  get assignedUser(): string {
    return this.row.data[this.columnDisplay.id] || '-';
  }

  ngOnInit(): void {
    this.delegate.assignableUsers$.pipe(take(1)).subscribe((users) => {
      this.panelItems = [];

      this.panelItems.push({
        id: 0,
        label: this.translateService.instant(
          'sqtm-core.campaign-workspace.test-plan.label.user.unassigned',
        ),
      });

      users.forEach((user) =>
        this.panelItems.push({
          id: user.id,
          label: this.getFullUsername(user),
        }),
      );
    });
  }

  showAvailableUsersList() {
    this.canEdit$
      .pipe(
        take(1),
        filter((canEdit) => canEdit),
      )
      .subscribe(() => {
        const positions: ConnectedPosition[] = [
          {
            originX: 'center',
            overlayX: 'center',
            originY: 'bottom',
            overlayY: 'top',
            offsetY: 10,
          },
          {
            originX: 'center',
            overlayX: 'center',
            originY: 'top',
            overlayY: 'bottom',
            offsetY: -10,
          },
        ];
        this.showList(this.availableUsers, this.templatePortalContent, positions);
      });
  }

  getFullUsername(user: SimpleUser): string {
    return formatFullUserName(user) || '-';
  }

  change(userId: number) {
    const userItem = this.panelItems.filter((item) => item.id === userId);
    const newUserName = userId === 0 ? '-' : userItem[0].label;

    this.beginAsyncOperation();

    this.delegate
      .updateAssignedUser(this.row.data[this.delegate.assignableEntityIdColumnId], userId)
      .pipe(
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => {
          this.close();
          this.endAsyncOperation();
        }),
      )
      .subscribe(() => {
        const changes: TableValueChange[] = [
          { columnId: GridColumnId.assigneeFullName, value: newUserName },
          { columnId: GridColumnId.assigneeId, value: userId === 0 ? null : userId },
        ];
        this.grid.editRows([this.row.id], changes);
        this.close();
      });
  }
}

export function assignedUserColumn(id: GridColumnId): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(GenericAssignedUserCellRendererComponent)
    .withHeaderPosition('left');
}

export interface AssignedUserDelegate {
  readonly canAssign$: Observable<boolean>;
  readonly assignableUsers$: Observable<SimpleUser[]>;
  readonly assignableEntityIdColumnId: GridColumnId;

  updateAssignedUser(testPlanItemId: number, userId: number);
}

export const ASSIGNED_USER_DELEGATE = new InjectionToken<AssignedUserDelegate>(
  'ASSIGNED_USER_DELEGATE',
);
