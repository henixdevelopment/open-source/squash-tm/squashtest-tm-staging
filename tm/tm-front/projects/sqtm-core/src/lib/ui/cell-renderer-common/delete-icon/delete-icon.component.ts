import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';

@Component({
  selector: 'sqtm-core-delete-icon',
  templateUrl: './delete-icon.component.html',
  styleUrls: ['./delete-icon.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteIconComponent implements OnInit {
  @Input()
  show = true;

  @Input()
  iconName = 'sqtm-core-generic:delete';

  @Output()
  delete = new EventEmitter<void>();

  constructor() {}

  ngOnInit(): void {}

  handleClick() {
    this.delete.emit();
  }
}
