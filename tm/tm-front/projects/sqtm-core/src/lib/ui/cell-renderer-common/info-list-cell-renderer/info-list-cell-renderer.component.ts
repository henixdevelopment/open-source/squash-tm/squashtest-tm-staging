import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { Overlay } from '@angular/cdk/overlay';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { AbstractListCellRendererComponent } from '../../grid/components/cell-renderers/abstract-list-cell-renderer/abstract-list-cell-renderer';
import { InfoListOptions, isInfoListOptions } from '../../grid/model/column-definition.model';
import { GridService } from '../../grid/services/grid.service';
import { ReferentialDataService } from '../../../core/referential/services/referential-data.service';
import { RestService } from '../../../core/services/rest.service';
import { ProjectData } from '../../../model/project/project-data.model';
import { InfoListItem } from '../../../model/infolist/infolistitem.model';
import { ListPanelItem } from '../../workspace-common/components/forms/list-panel/list-panel.component';
import { InfoList } from '../../../model/infolist/infolist.model';
import { ColumnDefinitionBuilder } from '../../grid/model/column-definition.builder';
import { ActionErrorDisplayService } from '../../../core/services/errors-handling/action-error-display.service';
import { GridColumnId } from '../../../shared/constants/grid/grid-column-id';

@Component({
  selector: 'sqtm-core-info-list-cell-renderer',
  templateUrl: './info-list-cell-renderer.component.html',
  styleUrls: ['./info-list-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InfoListCellRendererComponent
  extends AbstractListCellRendererComponent
  implements OnDestroy
{
  protected edit = false;

  @ViewChild('templatePortalContent', { read: TemplateRef })
  templatePortalContent: TemplateRef<any>;

  @ViewChild('infoListItemRef', { read: ElementRef })
  infoListItemRef: ElementRef;

  asyncOperationRunning = false;

  get options(): InfoListOptions {
    const options = this.columnDisplay.options;
    if (!options || !isInfoListOptions(options)) {
      throw Error('You must provide InfoListOptions');
    } else {
      return options;
    }
  }

  constructor(
    public grid: GridService,
    public cdRef: ChangeDetectorRef,
    public overlay: Overlay,
    public vcr: ViewContainerRef,
    private referentialDataService: ReferentialDataService,
    public translateService: TranslateService,
    public restService: RestService,
    public actionErrorDisplayService: ActionErrorDisplayService,
  ) {
    super(grid, cdRef, overlay, vcr, translateService, restService, actionErrorDisplayService);
  }

  showTemplate() {
    if (this.isEditable()) {
      this.showList(this.infoListItemRef, this.templatePortalContent);
    }
  }

  getProject(id: number): Observable<ProjectData> {
    return this.referentialDataService.connectToProjectData(id);
  }

  getValue(projectData: ProjectData, itemId: number) {
    const infoList = this.getInfoList(projectData);
    return this.getOptionLabel(infoList.items.find((item) => item.id === itemId));
  }

  getOptionLabel(item: InfoListItem): string {
    if (item) {
      if (item.system) {
        return this.translateService.instant(`sqtm-core.entity.${item.label}`);
      } else {
        return item.label;
      }
    } else {
      return '-';
    }
  }

  getListPanelItems(projectData: ProjectData): ListPanelItem[] {
    const infoListItems = this.getInfoList(projectData).items;
    const listPanelItems: ListPanelItem[] = [];
    infoListItems.forEach((item) => {
      listPanelItems.push({
        id: item.id,
        label: item.system
          ? this.translateService.instant(`sqtm-core.entity.${item.label}`)
          : item.label,
      });
    });
    return listPanelItems;
  }

  protected getInfoList(projectData: ProjectData): InfoList {
    return projectData[this.options.infolist];
  }

  get textClass(): string {
    return 'text-align-' + this.columnDisplay.contentPosition;
  }
}

export function infoListColumn(
  id: GridColumnId,
  options: InfoListOptions,
): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withOptions(options)
    .withRenderer(InfoListCellRendererComponent);
}
