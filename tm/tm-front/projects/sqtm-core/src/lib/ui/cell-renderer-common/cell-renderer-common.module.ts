import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GridModule } from '../grid/grid.module';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { WorkspaceCommonModule } from '../workspace-common/workspace-common.module';
import { SqtmDragAndDropModule } from '../drag-and-drop/sqtm-drag-and-drop.module';
import { CoverageVerifiedByComponent } from './coverage-verified-by/coverage-verified-by.component';
import { RequirementUiModule } from '../entities/requirement-ui/requirement-ui.module';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { ExecutionUiModule } from '../entities/execution-ui/execution-ui.module';
import { ExecutionModeCellComponent } from './execution-mode-cell/execution-mode-cell.component';
import { ExecutionStatusCellComponent } from './execution-status-cell/execution-status-cell.component';
import { PortalModule } from '@angular/cdk/portal';
import { FormsModule } from '@angular/forms';
import { DataUpdateTextCellRendererComponent } from './data-update-text-cell-renderer/data-update-text-cell-renderer.component';
import { MilestoneLabelCellComponent } from './milestone-label-cell/milestone-label-cell.component';
import { DialogModule } from '../dialog/dialog.module';
import { DeleteIconComponent } from './delete-icon/delete-icon.component';
import { LinkCellComponent } from './link-cell/link-cell.component';
import { TreeNodeCellRendererComponent } from './tree-node-cell-renderer/tree-node-cell-renderer.component';
import { EnumEditableCellComponent } from './enum-editable-cell/enum-editable-cell.component';
import { LinkIconCellComponent } from './link-icon-cell/link-icon-cell.component';
import { InfoListCellRendererComponent } from './info-list-cell-renderer/info-list-cell-renderer.component';
import { RequirementRoleCellRendererComponent } from './requirement-role-cell-renderer/requirement-role-cell-renderer.component';
import { IssueKeyComponent } from './issue-key/issue-key.component';
import { IssueExecutionComponent } from './issue-execution/issue-execution.component';
import { TestCaseAutomatableRendererComponent } from './test-case-automatable-renderer/test-case-automatable-renderer.component';
import { AutomatedTestTechnologyComponent } from './automated-test-technology/automated-test-technology.component';
import { ExternalLinkCellRendererComponent } from './external-link-cell-renderer/external-link-cell-renderer.component';
import { IssueExecutionsComponent } from './issue-executions/issue-executions.component';
import { IssueRequirementsComponent } from './issue-requirements/issue-requirements.component';
import { CustomFieldBoundEntityCellComponent } from './custom-field-bound-entity-cell/custom-field-bound-entity-cell.component';
import { OpenCloseCellRendererComponent } from './open-close-cell-renderer/open-close-cell-renderer.component';
import { GenericAssignedUserCellRendererComponent } from './generic-assigned-user-cell-renderer/generic-assigned-user-cell-renderer.component';
import { SettingsIconComponent } from './settings-icon/settings-icon.component';

@NgModule({
  declarations: [
    AutomatedTestTechnologyComponent,
    CoverageVerifiedByComponent,
    CustomFieldBoundEntityCellComponent,
    DataUpdateTextCellRendererComponent,
    DeleteIconComponent,
    EnumEditableCellComponent,
    ExecutionModeCellComponent,
    ExecutionStatusCellComponent,
    ExternalLinkCellRendererComponent,
    GenericAssignedUserCellRendererComponent,
    InfoListCellRendererComponent,
    IssueExecutionComponent,
    IssueExecutionsComponent,
    IssueKeyComponent,
    IssueRequirementsComponent,
    LinkCellComponent,
    LinkIconCellComponent,
    MilestoneLabelCellComponent,
    OpenCloseCellRendererComponent,
    RequirementRoleCellRendererComponent,
    TestCaseAutomatableRendererComponent,
    TreeNodeCellRendererComponent,
    SettingsIconComponent,
  ],
  imports: [
    CommonModule,
    DialogModule,
    ExecutionUiModule,
    FormsModule,
    GridModule,
    NzIconModule,
    NzPopoverModule,
    NzSelectModule,
    NzToolTipModule,
    PortalModule,
    RequirementUiModule,
    RouterModule,
    SqtmDragAndDropModule,
    SqtmDragAndDropModule,
    TranslateModule.forChild(),
    WorkspaceCommonModule,
  ],
  exports: [
    CoverageVerifiedByComponent,
    DeleteIconComponent,
    EnumEditableCellComponent,
    InfoListCellRendererComponent,
    IssueExecutionComponent,
    IssueKeyComponent,
    LinkCellComponent,
    LinkIconCellComponent,
    OpenCloseCellRendererComponent,
    RequirementRoleCellRendererComponent,
    SettingsIconComponent,
    TestCaseAutomatableRendererComponent,
    TreeNodeCellRendererComponent,
  ],
})
export class CellRendererCommonModule {}
