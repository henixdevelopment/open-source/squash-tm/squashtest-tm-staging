import { TestBed } from '@angular/core/testing';

import { UiManagerService } from './ui-manager.service';
import { Router } from '@angular/router';

describe('UiManagerService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [{ provide: Router, useValue: {} }],
    }),
  );

  it('should be created', () => {
    const service: UiManagerService = TestBed.inject(UiManagerService);
    expect(service).toBeTruthy();
  });
});
