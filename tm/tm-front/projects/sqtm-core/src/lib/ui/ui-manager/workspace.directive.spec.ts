import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkspaceDirective } from './workspace.directive';
import { themes } from './themes';
import { UiManagerService } from './ui-manager.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  template: ` <div sqtmCoreWorkspace></div> `,
})
class GlobalThemeTestingComponent {}

@Component({
  template: ` <div [sqtmCoreWorkspace]="'requirement-workspace'"></div> `,
})
class GlobalThemeWithNameTestingComponent {}

@Component({
  template: `
    <div [sqtmCoreWorkspace]="'requirement-workspace'">
      <div
        id="inner-themed-div"
        [sqtmCoreWorkspace]="'test-case-workspace'"
        [localTheme]="true"
      ></div>
    </div>
  `,
})
class LocalThemeTestingComponent {}

describe('WorkspaceDirective - Global - Without Name', () => {
  let fixture: ComponentFixture<GlobalThemeTestingComponent>;
  const testTheme = themes[0];
  // ********************* setup **********************************

  beforeEach(() => {
    fixture = TestBed.configureTestingModule({
      declarations: [GlobalThemeTestingComponent, WorkspaceDirective],
      providers: [{ provide: Router, useValue: {} }, UiManagerService],
    }).createComponent(GlobalThemeTestingComponent);

    fixture.detectChanges();
  });

  // ****************** tests *************************************

  /*
   * This test checks that only the global theme css properties are set, in a workspace-less page
   * (ie no value for attribute 'sqtmTheme' is supplied).
   */
  it('should apply the global theme only when no workspace is specified', () => {
    const documentStyle = document.documentElement.style;

    // testing that global css variables are present
    const globalTheme = testTheme['global-theme'];

    for (const key of Object.keys(globalTheme)) {
      const value = globalTheme[key];
      expect(documentStyle.getPropertyValue(`--${key}`)).toEqual(value.toString());
    }

    const homeTheme = testTheme['home-workspace'];
    for (const key of Object.keys(homeTheme)) {
      expect(documentStyle.getPropertyValue(`--${key}`)).toEqual('');
    }
  });
});

describe('WorkspaceDirective - Global - With Name', () => {
  let fixture: ComponentFixture<GlobalThemeWithNameTestingComponent>;
  const testTheme = themes[0];

  // ********************* setup **********************************
  beforeEach(() => {
    fixture = TestBed.configureTestingModule({
      declarations: [GlobalThemeWithNameTestingComponent, WorkspaceDirective],
      providers: [{ provide: Router, useValue: {} }, UiManagerService],
    }).createComponent(GlobalThemeWithNameTestingComponent);

    fixture.detectChanges();
  });

  it('should apply the global theme and workspace theme when a workspace is defined', () => {
    const documentStyle = document.documentElement.style;

    // testing that global css variables are present
    const globalTheme = testTheme['global-theme'];

    for (const key of Object.keys(globalTheme)) {
      const value = globalTheme[key];
      expect(documentStyle.getPropertyValue(`--${key}`)).toEqual(value.toString());
    }

    const homeTheme = testTheme['requirement-workspace'];
    expect(documentStyle.getPropertyValue(`--current-workspace-main-color`)).toBe(
      homeTheme['main-color'],
    );
    expect(documentStyle.getPropertyValue(`--current-workspace-brightest-color`)).toBe(
      homeTheme['brightest-color'],
    );
    expect(documentStyle.getPropertyValue(`--current-workspace-darkest-color`)).toBe(
      homeTheme['darkest-color'],
    );
  });
});

describe('WorkspaceDirective - Local', () => {
  let fixture: ComponentFixture<LocalThemeTestingComponent>;
  const testTheme = themes[0];
  // ********************* setup **********************************

  beforeEach(() => {
    fixture = TestBed.configureTestingModule({
      declarations: [LocalThemeTestingComponent, WorkspaceDirective],
      providers: [{ provide: Router, useValue: {} }, UiManagerService],
    }).createComponent(LocalThemeTestingComponent);

    fixture.detectChanges();
  });

  it('should apply the global theme on document and not be overridden by local theme', () => {
    const documentStyle = document.documentElement.style;

    // testing that global css variables are present
    const globalTheme = testTheme['global-theme'];

    for (const key of Object.keys(globalTheme)) {
      const value = globalTheme[key];
      expect(documentStyle.getPropertyValue(`--${key}`)).toEqual(value.toString());
    }

    const requirementTheme = testTheme['requirement-workspace'];
    expect(documentStyle.getPropertyValue(`--current-workspace-main-color`)).toBe(
      requirementTheme['main-color'],
    );
    expect(documentStyle.getPropertyValue(`--current-workspace-brightest-color`)).toBe(
      requirementTheme['brightest-color'],
    );
    expect(documentStyle.getPropertyValue(`--current-workspace-darkest-color`)).toBe(
      requirementTheme['darkest-color'],
    );
  });

  it('it should apply a theme locally', () => {
    const themedDiv = fixture.nativeElement.querySelector('#inner-themed-div');
    const themedDivStyle = themedDiv.style;
    const testCaseWorkspaceTheme = testTheme['test-case-workspace'];
    expect(themedDivStyle.getPropertyValue(`--current-workspace-main-color`)).toBe(
      testCaseWorkspaceTheme['main-color'],
    );
    expect(themedDivStyle.getPropertyValue(`--current-workspace-brightest-color`)).toBe(
      testCaseWorkspaceTheme['brightest-color'],
    );
    expect(themedDivStyle.getPropertyValue(`--current-workspace-darkest-color`)).toBe(
      testCaseWorkspaceTheme['darkest-color'],
    );
  });
});
