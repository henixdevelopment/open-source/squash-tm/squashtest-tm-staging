import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { FormControl, ValidationErrors, Validators } from '@angular/forms';
import { Subject, timer } from 'rxjs';
import { filter, takeUntil, tap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { RestService } from '../../../../core/services/rest.service';
import { DialogService } from '../../../dialog/services/dialog.service';
import { TestAutomationDialogComponent } from '../dialog/test-automation-dialog/test-automation-dialog.component';
import { TestAutomationServerKind } from '../../../../model/test-automation/test-automation-server.model';

@Component({
  selector: 'sqtm-core-editable-ta-test',
  templateUrl: './editable-ta-test.component.html',
  styleUrls: ['./editable-ta-test.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditableTaTestComponent implements OnInit {
  _value: string;

  pending = false;

  edit = false;

  formControl: FormControl;

  dirty = false;

  externalErrors: string[] = [];

  @Input()
  set value(value: string) {
    this.updateAndClose(value);
  }

  get value() {
    return this._value;
  }

  @Input()
  set editable(editable: boolean) {
    this._editable = editable;
    this.cdRef.detectChanges();
  }

  get editable(): boolean {
    return this._editable;
  }

  @ViewChild('input')
  input: ElementRef;

  @Input()
  testCaseId: number;

  @Input()
  taServerId: number;

  @Input()
  testAutomationServerKind: TestAutomationServerKind;

  @Output()
  confirmEvent = new EventEmitter<string>();

  private _editable = true;

  constructor(
    private restService: RestService,
    private dialogService: DialogService,
    private vcr: ViewContainerRef,
    public cdRef: ChangeDetectorRef,
    private translateService: TranslateService,
  ) {
    this.formControl = new FormControl();
    this.formControl.setValidators(Validators.pattern('^\\/(.+?[^\\\\]/)+.*?(\\\\\\/$|[^\\/]$)'));
  }

  ngOnInit(): void {}

  choose() {
    const dialogReference = this.dialogService.openDialog({
      component: TestAutomationDialogComponent,
      viewContainerReference: this.vcr,
      id: 'test-automation-serveur',
      data: { testCaseId: this.testCaseId },
      height: 800,
      width: 600,
    });

    dialogReference.dialogClosed$.subscribe((rowId: string) => {
      if (rowId) {
        this.formControl.setValue(rowId);
      }
    });
  }

  beginAsync() {
    this.pending = true;
    this.formControl.disable();
  }

  disableEditMode() {
    this.edit = false;
    this.pending = false;
  }

  private removeExternalErrors() {
    this.externalErrors = [];
  }

  enableEditMode() {
    if (this.editable) {
      this.formControl.reset();
      this.formControl.setValue(this.value);
      this.formControl.updateValueAndValidity();
      this.formControl.enable();
      this.dirty = false;
      this.removeExternalErrors();
      if (!this.edit) {
        this.edit = true;
        // kind of pulling for waiting that input is displayed and active, so we can focus into.
        const unsub = new Subject<void>();
        timer(1, 10)
          .pipe(
            takeUntil(unsub),
            filter(() => Boolean(this.input)),
            tap(() => this.input.nativeElement.focus()),
            // auto unsub, so we don't have wild timers that run into app after an edition
            tap(() => {
              unsub.next();
              unsub.complete();
            }),
          )
          .subscribe();
      }
    }
    return false;
  }

  private updateAndClose(value: string) {
    this.endAsync();
    this.disableEditMode();
    this._value = value;
    this.cdRef.detectChanges();
  }

  endAsync() {
    this.pending = false;
    this.formControl.enable();
  }

  confirm() {
    this.dirty = true;
    this.removeExternalErrors();
    this.formControl.setValue(this.formControl.value);
    if (!this.formControl.errors) {
      this.beginAsync();
      this.confirmEvent.emit(this.getInputFieldValue());
    }
  }

  private getInputFieldValue() {
    return this.formControl.value;
  }

  cancel() {
    this.disableEditMode();
  }

  getComponentClasses() {
    const cssClass = [];
    if (this.edit) {
      cssClass.push('edit');
    } else {
      cssClass.push('read');
      if (this.editable) {
        cssClass.push('editable');
      }
    }
    return cssClass;
  }

  getErrors(errors: ValidationErrors): string[] {
    const messages = [];
    if (errors) {
      Object.keys(errors).forEach((errorKey) => {
        messages.push(this.translateService.instant('sqtm-core.validation.errors.' + errorKey));
      });
    }
    return messages;
  }

  hasJenkinsKindServer() {
    return this.testAutomationServerKind === TestAutomationServerKind.jenkins;
  }
}
