import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SingleMilestonePickerComponent } from './component/single-milestone-picker/single-milestone-picker.component';
import { SingleMilestonePickerDialogComponent } from './component/single-milestone-picker-dialog/single-milestone-picker-dialog.component';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputModule } from 'ng-zorro-antd/input';
import { DialogModule } from '../dialog/dialog.module';
import { TranslateModule } from '@ngx-translate/core';
import { GridModule } from '../grid/grid.module';
import { FormsModule } from '@angular/forms';
import { WorkspaceCommonModule } from '../workspace-common/workspace-common.module';
import { MultipleMilestonePickerComponent } from './component/multiple-milestone-picker/multiple-milestone-picker.component';
import { MultipleMilestonePickerDialogComponent } from './component/multiple-milestone-picker-dialog/multiple-milestone-picker-dialog.component';

@NgModule({
  declarations: [
    SingleMilestonePickerComponent,
    SingleMilestonePickerDialogComponent,
    MultipleMilestonePickerComponent,
    MultipleMilestonePickerDialogComponent,
  ],
  exports: [
    SingleMilestonePickerComponent,
    SingleMilestonePickerDialogComponent,
    MultipleMilestonePickerComponent,
    MultipleMilestonePickerDialogComponent,
  ],
  imports: [
    CommonModule,
    NzDividerModule,
    DialogModule,
    TranslateModule.forChild(),
    NzButtonModule,
    GridModule,
    NzInputModule,
    FormsModule,
    NzIconModule,
    WorkspaceCommonModule,
  ],
})
export class MilestoneModule {}
