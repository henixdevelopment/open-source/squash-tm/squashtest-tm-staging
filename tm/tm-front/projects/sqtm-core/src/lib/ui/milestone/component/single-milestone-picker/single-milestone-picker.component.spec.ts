import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SingleMilestonePickerComponent } from './single-milestone-picker.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestingUtilsModule } from '../../../testing-utils/testing-utils.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('MilestonePickerComponent', () => {
  let component: SingleMilestonePickerComponent;
  let fixture: ComponentFixture<SingleMilestonePickerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TestingUtilsModule],
      declarations: [SingleMilestonePickerComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleMilestonePickerComponent);
    component = fixture.componentInstance;
    component.milestones = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
