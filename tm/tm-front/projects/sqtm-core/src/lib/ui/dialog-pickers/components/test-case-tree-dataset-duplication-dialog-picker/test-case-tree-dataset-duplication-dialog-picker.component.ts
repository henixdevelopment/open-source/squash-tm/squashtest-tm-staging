import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AbstractTreeDialogPicker } from '../abstract-tree-dialog-picker';
import { DialogReference } from '../../../dialog/model/dialog-reference';
import { DatasetDuplicationTreePickerDialogConfiguration } from '../../picker.dialog.configuration';
import { DataRow } from '../../../grid/model/data-row.model';

@Component({
  selector: 'sqtm-core-test-case-tree-dataset-duplication-dialog-picker',
  templateUrl: './test-case-tree-dataset-duplication-dialog-picker.component.html',
  styleUrls: ['./test-case-tree-dataset-duplication-dialog-picker.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestCaseTreeDatasetDuplicationDialogPickerComponent extends AbstractTreeDialogPicker<DatasetDuplicationTreePickerDialogConfiguration> {
  constructor(
    dialogReference: DialogReference<DatasetDuplicationTreePickerDialogConfiguration, DataRow[]>,
  ) {
    super(dialogReference);
  }
}
