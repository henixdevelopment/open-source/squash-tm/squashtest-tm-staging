import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AbstractTreeDialogPicker } from '../abstract-tree-dialog-picker';
import { DialogReference } from '../../../dialog/model/dialog-reference';
import { TreePickerDialogConfiguration } from '../../picker.dialog.configuration';
import { DataRow } from '../../../grid/model/data-row.model';

@Component({
  selector: 'sqtm-core-test-case-tree-folder-dialog-picker',
  templateUrl: './test-case-tree-folder-dialog-picker.component.html',
  styleUrl: './test-case-tree-folder-dialog-picker.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestCaseTreeFolderDialogPickerComponent extends AbstractTreeDialogPicker {
  constructor(dialogReference: DialogReference<TreePickerDialogConfiguration, DataRow[]>) {
    super(dialogReference);
  }
}
