import { DialogConfiguration } from '../dialog/services/dialog-feature.state';
import { RequirementTreeDialogPickerComponent } from './components/requirement-tree-dialog-picker/requirement-tree-dialog-picker.component';
import { Identifier } from '../../model/entity.model';
import { TestCaseTreeDialogPickerComponent } from './components/test-case-tree-dialog-picker/test-case-tree-dialog-picker.component';
import { CampaignTreeDialogPickerComponent } from './components/campaign-tree-dialog-picker/campaign-tree-dialog-picker.component';
import { TestCaseTreeDatasetDuplicationDialogPickerComponent } from './components/test-case-tree-dataset-duplication-dialog-picker/test-case-tree-dataset-duplication-dialog-picker.component';
import { TestCaseTreeFolderDialogPickerComponent } from './components/test-case-tree-folder-dialog-picker/test-case-tree-folder-dialog-picker.component';
import { ViewContainerRef } from '@angular/core';

export interface TreePickerDialogConfiguration {
  titleKey: string;
  selectedNodes: Identifier[];
  enabledMultiSelection: boolean;
  requiredRowType?: string;
}

export type TreePickerDialogType =
  | 'requirement-picker-dialog'
  | 'test-case-picker-dialog'
  | 'test-case-dataset-duplication-picker-dialog'
  | 'campaign-picker-dialog'
  | 'test-case-folder-picker-dialog';

export interface DatasetDuplicationTreePickerDialogConfiguration
  extends TreePickerDialogConfiguration {
  testCaseId?: Identifier;
}

function findComponentType(pickerType: TreePickerDialogType) {
  let component;

  switch (pickerType) {
    case 'requirement-picker-dialog':
      component = RequirementTreeDialogPickerComponent;
      break;
    case 'test-case-picker-dialog':
      component = TestCaseTreeDialogPickerComponent;
      break;
    case 'test-case-dataset-duplication-picker-dialog':
      component = TestCaseTreeDatasetDuplicationDialogPickerComponent;
      break;
    case 'campaign-picker-dialog':
      component = CampaignTreeDialogPickerComponent;
      break;
    case 'test-case-folder-picker-dialog':
      component = TestCaseTreeFolderDialogPickerComponent;
      break;
    default:
      throw Error('Unknown TreePickerDialogType: ' + pickerType);
  }
  return component;
}

export function buildTreePickerDialogDefinition(
  pickerType: TreePickerDialogType,
  selectedNodes: Identifier[] = [],
  enabledMultiSelection: boolean = true,
  requiredRowType?: string,
  vcr?: ViewContainerRef,
): DialogConfiguration<TreePickerDialogConfiguration> {
  const configuration: TreePickerDialogConfiguration = {
    titleKey: enabledMultiSelection
      ? 'sqtm-core.dialog.choose-elements'
      : 'sqtm-core.dialog.choose-element',
    selectedNodes,
    enabledMultiSelection,
    requiredRowType,
  };

  const component = findComponentType(pickerType);

  return {
    id: 'tree-picker-dialog',
    component,
    height: 800,
    width: 600,
    data: configuration,
    viewContainerReference: vcr,
  };
}

export function buildDatasetDuplicationTreePickerDialogDefinition(
  pickerType: TreePickerDialogType,
  selectedNodes: Identifier[] = [],
  enabledMultiSelection: boolean = true,
  testCaseId?: Identifier,
  requiredRowType?: string,
): DialogConfiguration<DatasetDuplicationTreePickerDialogConfiguration> {
  const configuration: DatasetDuplicationTreePickerDialogConfiguration = {
    titleKey: 'sqtm-core.dialog.choose-elements',
    selectedNodes,
    enabledMultiSelection,
    testCaseId,
    requiredRowType,
  };

  const component = findComponentType(pickerType);

  return {
    id: 'tree-picker-dialog',
    component,
    height: 800,
    width: 600,
    data: configuration,
  };
}
