import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AbstractTreeDialogPicker } from '../abstract-tree-dialog-picker';
import { DialogReference } from '../../../dialog/model/dialog-reference';
import { TreePickerDialogConfiguration } from '../../picker.dialog.configuration';
import { DataRow } from '../../../grid/model/data-row.model';

@Component({
  selector: 'sqtm-core-campaign-tree-dialog-picker',
  templateUrl: './campaign-tree-dialog-picker.component.html',
  styleUrls: ['./campaign-tree-dialog-picker.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CampaignTreeDialogPickerComponent extends AbstractTreeDialogPicker {
  constructor(dialogReference: DialogReference<TreePickerDialogConfiguration, DataRow[]>) {
    super(dialogReference);
  }
}
