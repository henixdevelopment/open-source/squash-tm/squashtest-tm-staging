import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AttachmentDrawerComponent } from './attachment-drawer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';

describe('AttachmentDrawerComponent', () => {
  let component: AttachmentDrawerComponent;
  let fixture: ComponentFixture<AttachmentDrawerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [BrowserAnimationsModule, NzDrawerModule],
      declarations: [AttachmentDrawerComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachmentDrawerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
