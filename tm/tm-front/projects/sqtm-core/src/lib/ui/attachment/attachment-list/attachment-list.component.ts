import { ChangeDetectionStrategy, Component, ViewContainerRef } from '@angular/core';
import { AttachmentService } from '../../../core/services/attachment.service';
import { AbstractAttachmentList } from './abstract-attachment-list';
import { DialogService } from '../../dialog/services/dialog.service';
@Component({
  selector: 'sqtm-core-attachment-list',
  templateUrl: './attachment-list.component.html',
  styleUrls: ['./attachment-list.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AttachmentListComponent extends AbstractAttachmentList {
  constructor(
    attachmentService: AttachmentService,
    dialogService: DialogService,
    vcr: ViewContainerRef,
  ) {
    super(attachmentService, dialogService, vcr);
  }
}
