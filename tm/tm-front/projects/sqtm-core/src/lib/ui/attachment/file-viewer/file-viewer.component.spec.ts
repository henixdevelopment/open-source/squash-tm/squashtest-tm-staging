import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FileViewerComponent } from './file-viewer.component';
import { DialogReference } from '../../dialog/model/dialog-reference';
import { AttachmentService } from '../../../core/services/attachment.service';
import { TranslateModule } from '@ngx-translate/core';
import { AdminReferentialDataService } from '../../../core/referential/services/admin-referential-data.service';
import { EMPTY, of } from 'rxjs';
import { ReferentialDataService } from '../../../core/referential/services/referential-data.service';

describe('FileViewerComponent', () => {
  let component: FileViewerComponent;
  let fixture: ComponentFixture<FileViewerComponent>;
  const dialogReference = jasmine.createSpyObj(['close']);
  const attachmentServiceMock = jasmine.createSpyObj('attachmentService', [
    'getAttachmentPreviewURL',
    'getAttachmentDownloadURL',
  ]);
  dialogReference['data'] = {
    attachmentList: {
      id: 1,
      attachments: [
        {
          id: 1,
          name: 'mock-attachment',
          size: 10,
          addedOn: Date.now(),
        },
      ],
    },
    index: 0,
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [FileViewerComponent],
      providers: [
        {
          provide: DialogReference,
          useValue: dialogReference,
        },
        {
          provide: AttachmentService,
          useValue: attachmentServiceMock,
        },
        {
          provide: AdminReferentialDataService,
          useValue: {
            unsafeAttachmentPreviewEnabled$: EMPTY,
          },
        },
        {
          provide: ReferentialDataService,
          useValue: {
            unsafeAttachmentPreviewEnabled$: of(true),
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(FileViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
