import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  InjectionToken,
  Input,
  OnDestroy,
  Output,
} from '@angular/core';
import { AttachmentModel } from '../../../model/attachment/attachment.model';
import { AttachmentService } from '../../../core/services/attachment.service';
import { tree } from '../../grid/model/grid-builders';
import { GridDefinition } from '../../grid/model/grid-definition.model';
import { column } from '../../grid/model/common-column-definition.builders';
import { Extendable } from '../../grid/model/column-definition.model';
import { TreeNodeCellRendererComponent } from '../../cell-renderer-common/tree-node-cell-renderer/tree-node-cell-renderer.component';
import { GridService } from '../../grid/services/grid.service';
import { gridServiceFactory } from '../../grid/grid.service.provider';
import { RestService } from '../../../core/services/rest.service';
import { ReferentialDataService } from '../../../core/referential/services/referential-data.service';
import { DataRow } from '../../grid/model/data-row.model';
import { Directory, File } from '../../grid/model/data-row.type';
import { ReadOnlyPermissions } from '../../../model/permissions/simple-permissions';
import { filter } from 'rxjs/operators';
import { GridColumnId } from '../../../shared/constants/grid/grid-column-id';

export const ARCHIVE_FILE_TREE_CONF = new InjectionToken<GridDefinition>(
  'Grid config for archive file tree',
);
export const ARCHIVE_FILE_TREE = new InjectionToken<GridService>(
  'Grid service instance for archive file tree',
);
export function archiveFileTreeConfigFactory(): GridDefinition {
  return tree('archive-file')
    .withColumns([
      column(GridColumnId.name)
        .changeWidthCalculationStrategy(new Extendable(300))
        .withRenderer(TreeNodeCellRendererComponent),
    ])
    .disableMultiSelection()
    .withRowConverter(archiveLiteralConverter)
    .build();
}

@Component({
  selector: 'sqtm-core-archive-tree',
  template: `<sqtm-core-grid></sqtm-core-grid>`,
  styleUrl: './archive-tree.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: ARCHIVE_FILE_TREE_CONF,
      useFactory: archiveFileTreeConfigFactory,
      deps: [],
    },
    {
      provide: ARCHIVE_FILE_TREE,
      useFactory: gridServiceFactory,
      deps: [RestService, ARCHIVE_FILE_TREE_CONF, ReferentialDataService],
    },
    {
      provide: GridService,
      useExisting: ARCHIVE_FILE_TREE,
    },
  ],
})
export class ArchiveTreeComponent implements OnDestroy {
  private _archive: AttachmentModel;

  @Input()
  get archive(): AttachmentModel {
    return this._archive;
  }
  set archive(archive: AttachmentModel) {
    this._archive = archive;
    this.loadArchiveData();
  }

  @Output() displayFile = new EventEmitter<string>();

  @Output() handleError = new EventEmitter<void>();

  constructor(
    private attachmentService: AttachmentService,
    @Inject(ARCHIVE_FILE_TREE) private gridService: GridService,
  ) {
    gridService.selectedRowIds$
      .pipe(filter((selectedRowIds) => selectedRowIds?.length === 1))
      .subscribe((selectedRowIds) => this.displayFile.emit(selectedRowIds[0].toString()));
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }

  private loadArchiveData(): void {
    if (this.archive) {
      this.gridService.beginAsyncOperation();
      this.attachmentService.getArchiveFileNode(this.archive).subscribe({
        next: (response) => {
          this.gridService.loadInitialDataRows(response.dataRows, response.dataRows.length);
          this.gridService.completeAsyncOperation();
        },
        error: (error) => this.handleError.emit(error),
      });
    }
  }
}

export function archiveLiteralConverter(literals: Partial<DataRow>[]): DataRow[] {
  return literals.reduce((dataRows, literal) => {
    let dataRow: DataRow;
    if (literal.data['isDirectory']) {
      dataRow = new Directory();
    } else {
      dataRow = new File();
    }

    dataRow.simplePermissions = new ReadOnlyPermissions();
    Object.assign(dataRow, literal);
    dataRows.push(dataRow);
    return dataRows;
  }, []);
}
