import { AttachmentService } from '../../../core/services/attachment.service';
import { Directive, EventEmitter, Input, Output, ViewContainerRef } from '@angular/core';
import {
  Attachment,
  PersistedAttachment,
  RejectedAttachment,
} from '../../../model/attachment/attachment.model';
import { DialogService } from '../../dialog/services/dialog.service';
import { FileViewerComponent } from '../file-viewer/file-viewer.component';

@Directive()
export abstract class AbstractAttachmentList {
  @Input()
  attachments: Attachment[];

  @Input()
  attachmentListId: number;

  @Input()
  canAttach = true;

  @Input()
  showDeleteButton = true;

  @Output()
  deleteAttachmentEvent = new EventEmitter<PersistedAttachment>();

  @Output()
  confirmDeleteAttachmentEvent = new EventEmitter<PersistedAttachment>();

  @Output()
  cancelDeleteAttachmentEvent = new EventEmitter<PersistedAttachment>();

  @Output()
  removeRejectedAttachmentEvent = new EventEmitter<RejectedAttachment>();

  delete(attachment: PersistedAttachment): void {
    this.deleteAttachmentEvent.emit(attachment);
  }

  confirm(attachment: PersistedAttachment): void {
    this.confirmDeleteAttachmentEvent.emit(attachment);
  }

  cancel(attachment: PersistedAttachment): void {
    this.cancelDeleteAttachmentEvent.emit(attachment);
  }

  removeRejectedAttachment(attachment: RejectedAttachment) {
    this.removeRejectedAttachmentEvent.emit(attachment);
  }

  protected constructor(
    protected attachmentService: AttachmentService,
    private dialogService: DialogService,
    private vcr: ViewContainerRef,
  ) {}

  getAttachmentDownloadURL(attachment: PersistedAttachment) {
    return this.attachmentService.getAttachmentDownloadURL(this.attachmentListId, attachment);
  }

  openFilePreviewDialog(event: any, attachment: Attachment) {
    event.preventDefault();

    const attachmentIndex: number = this.attachments.indexOf(attachment);

    this.dialogService.openDialog({
      id: 'automated-suite-file-viewer',
      component: FileViewerComponent,
      viewContainerReference: this.vcr,
      data: {
        attachmentList: { id: this.attachmentListId, attachments: this.attachments },
        index: attachmentIndex,
      },
      minHeight: '100%',
      minWidth: '100%',
    });
  }
}
