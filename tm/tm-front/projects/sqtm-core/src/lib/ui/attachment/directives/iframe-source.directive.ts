import { Directive, ElementRef, Host, OnInit } from '@angular/core';

@Directive({
  selector: '[sqtmCoreIframeSource]',
})
export class IframeSourceDirective implements OnInit {
  constructor(@Host() private iframe: ElementRef<HTMLIFrameElement>) {}

  ngOnInit() {
    this.iframe.nativeElement.onload = () => {
      const observer = new MutationObserver((mutations) => {
        mutations.forEach((mutation) =>
          mutation.addedNodes.forEach((node) => this.updateBehaviorOnIdLinks(node)),
        );
      });

      const config = { childList: true, subtree: true };
      observer.observe(this.iframe.nativeElement.contentDocument.body, config);
    };
  }

  private updateBehaviorOnIdLinks(node: Node) {
    if (node.nodeType === Node.ELEMENT_NODE) {
      const element = node as HTMLElement;
      const links = element.getElementsByTagName('a');
      if (links.length > 0) {
        for (let i = 0; i < links.length; i++) {
          const link = links[i];
          const href = link.getAttribute('href');
          if (href && href.startsWith('#')) {
            link.setAttribute('href', 'javascript:;');
            link.setAttribute('onclick', `document.location.hash='${href}';`);
          }
        }
      }
    }
  }
}
