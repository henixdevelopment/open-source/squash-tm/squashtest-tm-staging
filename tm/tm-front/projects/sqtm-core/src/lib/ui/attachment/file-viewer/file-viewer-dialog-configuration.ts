import { AttachmentListModel } from '../../../model/attachment/attachment-list.model';
import { FileViewerFilePathType } from '../../../model/attachment/file-viewer.model';

export class FileViewerDialogConfiguration {
  attachmentList?: AttachmentListModel;
  index?: number;
  filePath?: string;
  origin?: FileViewerFilePathType;
}
