import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'sqtm-core-attachment-drawer',
  templateUrl: './attachment-drawer.component.html',
  styleUrls: ['./attachment-drawer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AttachmentDrawerComponent implements OnInit, OnDestroy {
  isVisible = false;

  unsub$ = new Subject<void>();

  @Input()
  canAttach: boolean;

  constructor(private cdRef: ChangeDetectorRef) {}

  ngOnInit() {}

  open(): void {
    this.isVisible = true;
    this.cdRef.detectChanges();
  }

  close(): void {
    this.isVisible = false;
    this.cdRef.detectChanges();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
