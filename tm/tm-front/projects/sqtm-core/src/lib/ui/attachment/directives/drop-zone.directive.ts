import { Directive, EventEmitter, HostListener, Output } from '@angular/core';
import { attachmentLogger } from '../../attachment.logger';

const logger = attachmentLogger.compose('DropZoneDirective');

@Directive({
  selector: '[sqtmCoreAttachmentDropZone]',
})
export class DropZoneDirective {
  @Output()
  filesDragEnterEvent = new EventEmitter<void>();

  @Output()
  filesDragLeaveEvent = new EventEmitter<void>();

  @Output()
  filesDropEvent = new EventEmitter<File[]>();

  // this counter is used to prevent false dragLeave when the user leave the target element to a child of this element.
  // In that case a dragenter event is emitted followed by a dragleave. So using a counter allow us to make difference
  // between move over a child and move out of the container.
  counter = 0;

  // prevent default on dragover to avoid opening of files directly in browser
  @HostListener('dragover', ['$event'])
  onDragOver(dragEvent: DragEvent) {
    dragEvent.preventDefault();
  }

  @HostListener('dragenter', ['$event'])
  onDragEnter(_dragEvent: DragEvent) {
    if (this.counter === 0) {
      this.filesDragEnterEvent.next();
    }
    this.counter++;
  }

  @HostListener('dragleave', ['$event'])
  onDragLeave(_dragEvent: DragEvent) {
    this.counter--;
    if (this.counter === 0) {
      this.filesDragLeaveEvent.next();
    }
  }

  @HostListener('drop', ['$event'])
  onDrop(dragEvent: DragEvent) {
    logger.debug('drop files into DropZoneDirective ', [dragEvent]);
    dragEvent.preventDefault();
    const files = this.extractFiles(dragEvent);
    if (files.length > 0) {
      logger.debug('Found files in event ', [files]);
      this.filesDropEvent.next(files);
      this.counter = 0;
    }
  }

  private extractFiles(dragEvent: DragEvent) {
    const files = [];
    if (dragEvent.dataTransfer && dragEvent.dataTransfer.files) {
      const fileList = dragEvent.dataTransfer.files;
      logger.debug(`Looking for files in fileList with length ${fileList.length}`, [fileList]);
      if (fileList.length > 0) {
        for (let i = 0; i < fileList.length; i++) {
          if (fileList.item && typeof fileList.item === 'function') {
            files.push(fileList.item(i));
          } else {
            files.push(fileList[i]);
          }
        }
      }
    }
    return files;
  }

  constructor() {}
}
