import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AttachmentFileSelectorComponent } from './attachment-file-selector.component';

describe('AttachmentFileSelectorComponent', () => {
  let component: AttachmentFileSelectorComponent;
  let fixture: ComponentFixture<AttachmentFileSelectorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AttachmentFileSelectorComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachmentFileSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
