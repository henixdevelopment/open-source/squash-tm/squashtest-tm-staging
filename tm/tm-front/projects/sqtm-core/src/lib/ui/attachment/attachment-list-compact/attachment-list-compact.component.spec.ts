import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { AttachmentListCompactComponent } from './attachment-list-compact.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TestingUtilsModule } from '../../testing-utils/testing-utils.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentTester } from 'ngx-speculoos';
import {
  PersistedAttachment,
  RejectedAttachment,
  UploadingAttachment,
} from '../../../model/attachment/attachment.model';

const attachment1: PersistedAttachment = {
  id: '1',
  kind: 'persisted-attachment',
  addedOn: new Date(),
  lastModifiedOn: null,
  name: 'attachment1',
  size: 500,
  pendingDelete: true,
};
const attachment2: PersistedAttachment = {
  id: '2',
  kind: 'persisted-attachment',
  addedOn: new Date(),
  lastModifiedOn: null,
  name: 'attachment2',
  size: 500,
  pendingDelete: false,
};
const rejectedAttachment: RejectedAttachment = {
  id: 'def429ab-3db2-4a4a-9742-db878344fd87',
  kind: 'rejected-attachment',
  addedOn: new Date(),
  lastModifiedOn: null,
  name: 'log_jira_sync.txt',
  size: 50000000000,
  errors: ['Too Heavy'],
};
const uploadingAttachment: UploadingAttachment = {
  id: '3a2003a4-95ed-4d21-9063-4e3eef5122a6',
  kind: 'uploading-attachment',
  addedOn: new Date(),
  lastModifiedOn: null,
  name: 'hello.txt',
  size: 1,
  uploadProgress: 50,
};

const attachments = [attachment1, attachment2, rejectedAttachment, uploadingAttachment];

class AttachmentListCompactTester extends ComponentTester<AttachmentListCompactComponent> {
  findRow(attachmentId: string) {
    return this.element(`tr[data-test-attachment-id="${attachmentId}"]`);
  }

  getAttachmentName(attachmentId: string) {
    return this.findRow(attachmentId).element('td.name').textContent;
  }
}

describe('AttachmentListCompactComponent', () => {
  let fixture: ComponentFixture<AttachmentListCompactComponent>;
  let tester: AttachmentListCompactTester;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AttachmentListCompactComponent],
      imports: [HttpClientTestingModule, TestingUtilsModule],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachmentListCompactComponent);
    tester = new AttachmentListCompactTester(fixture);
    tester.componentInstance.attachmentListId = 1;
    tester.componentInstance.attachments = attachments;
    fixture.detectChanges();
  });

  it('should show attachments', () => {
    expect(tester.componentInstance).toBeTruthy();
    expect(tester.findRow(attachment1.id)).toBeTruthy();
    expect(tester.getAttachmentName(attachment1.id)).toEqual('attachment1');
    expect(tester.findRow(attachment2.id)).toBeTruthy();
    expect(tester.getAttachmentName(attachment2.id)).toEqual('attachment2');
    expect(tester.findRow(rejectedAttachment.id)).toBeTruthy();
    expect(tester.getAttachmentName(rejectedAttachment.id)).toEqual('log_jira_sync.txt');
    expect(tester.findRow(uploadingAttachment.id)).toBeTruthy();
    expect(tester.getAttachmentName(uploadingAttachment.id)).toEqual('hello.txt');
  });
});
