import {
  AfterViewChecked,
  ChangeDetectionStrategy,
  Component,
  computed,
  ElementRef,
  Optional,
  Signal,
  signal,
  ViewChild,
  WritableSignal,
} from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { combineLatest, EMPTY, Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import {
  FileViewer,
  FileViewerFilePathType,
  FileViewerType,
} from '../../../model/attachment/file-viewer.model';
import { AttachmentModel } from '../../../model/attachment/attachment.model';
import { DialogReference } from '../../dialog/model/dialog-reference';
import { AttachmentService } from '../../../core/services/attachment.service';
import { AttachmentListModel } from '../../../model/attachment/attachment-list.model';
import { FileViewerDialogConfiguration } from './file-viewer-dialog-configuration';
import { AdminReferentialDataService } from '../../../core/referential/services/admin-referential-data.service';
import { ReferentialDataService } from '../../../core/referential/services/referential-data.service';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sqtm-core-file-viewer',
  templateUrl: './file-viewer.component.html',
  styleUrls: ['./file-viewer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FileViewerComponent implements AfterViewChecked {
  private readonly imageExtensions: string[] = ['png', 'jpeg', 'jpg', 'gif', 'svg', 'bmp'];
  private readonly iframeExtensions: string[] = ['html', 'pdf'];
  private readonly preExtensions: string[] = ['txt', 'xml', 'log', 'json'];
  private readonly archiveExtensions: string[] = ['tar'];
  private readonly unsafeExtensions: string[] = ['svg', 'html'];

  readonly FileViewerType = FileViewerType;

  $fileViewer: WritableSignal<FileViewer> = signal<FileViewer>(null);
  $isLoading: WritableSignal<boolean> = signal(true);
  $url: Signal<SafeResourceUrl>;
  $unsafeAttachmentPreviewEnabled: Signal<boolean> = toSignal(
    combineLatest([
      this.adminReferentialDataService.unsafeAttachmentPreviewEnabled$,
      this.referentialDataService.unsafeAttachmentPreviewEnabled$,
    ]).pipe(map(([adminProperty, property]) => adminProperty || property)),
  );

  attachmentListId: number;
  attachments: AttachmentModel[];

  @ViewChild('iframeElement')
  iframeElement: ElementRef;

  @ViewChild('imageElement')
  imageElement: ElementRef;

  data: FileViewerDialogConfiguration;

  constructor(
    private dialogReference: DialogReference<FileViewerDialogConfiguration>,
    private attachmentService: AttachmentService,
    private domSanitizer: DomSanitizer,
    @Optional() private readonly adminReferentialDataService: AdminReferentialDataService,
    @Optional() private readonly referentialDataService: ReferentialDataService,
  ) {
    this.data = this.dialogReference.data;
    this.initializeFileViewerData(this.data);
    this.$url = computed(() => this.getAttachmentPreviewURL(this.$fileViewer()));
  }

  private initializeFileViewerData(data: any) {
    if (data.attachmentList) {
      const attachmentList: AttachmentListModel = data.attachmentList;

      this.attachments = attachmentList.attachments;
      this.attachmentListId = attachmentList.id;

      const attachment: AttachmentModel = attachmentList.attachments[data.index];
      const viewerType: FileViewerType = this.getFileViewerType(attachment.name);

      this.$fileViewer.set({
        attachment,
        index: data.index,
        viewerType,
        isArchive: viewerType === FileViewerType.ARCHIVE,
      });

      if (viewerType === FileViewerType.PRE) {
        this.getContentOnAttachment(this.$fileViewer());
      }

      if (viewerType === FileViewerType.ARCHIVE) {
        this.$isLoading.set(false);
      }
    }

    if (data.filePath) {
      const fileName = this.getFileNameFromPath();
      const viewerType: FileViewerType = this.getFileViewerType(fileName);

      this.$fileViewer.update((fileViewer) => {
        return {
          ...fileViewer,
          viewerType,
        };
      });

      if (viewerType === FileViewerType.PRE) {
        this.getContentOnFile(data.origin, data.filePath);
      }
    }
  }

  ngAfterViewChecked(): void {
    if (this.iframeElement) {
      const iframe = this.iframeElement.nativeElement;
      iframe.addEventListener('load', this.fileLoaded.bind(this), { once: true });
    }

    if (this.imageElement) {
      const image = this.imageElement.nativeElement;
      image.addEventListener('load', this.fileLoaded.bind(this), { once: true });
    }
  }

  updatePreview(newIndex: number) {
    this.$isLoading.set(true);
    const attachmentLength: number = this.attachments.length;
    let index: number;

    if (newIndex < 0) {
      index = attachmentLength - 1;
    } else if (newIndex === attachmentLength) {
      index = 0;
    } else {
      index = newIndex;
    }

    const viewerType: FileViewerType = this.getFileViewerType(this.attachments[index].name);

    this.$fileViewer.set({
      attachment: this.attachments[index],
      index,
      viewerType,
      isArchive: viewerType === FileViewerType.ARCHIVE,
    });

    if (viewerType === FileViewerType.PRE) {
      this.getContentOnAttachment(this.$fileViewer());
    }

    if (viewerType === FileViewerType.ARCHIVE) {
      this.$isLoading.set(false);
    }
  }

  private getFileExtension(name: string): string {
    const tab: string[] = name.split('.');
    if (tab.length === 0) {
      throw new Error(`File : ${name} does not contain an extension.`);
    }
    return tab[tab.length - 1];
  }

  private fileLoaded(_event: any) {
    this.$isLoading.set(false);
  }

  private getFileViewerType(attachmentName: string) {
    try {
      const fileExtension: string = this.getFileExtension(attachmentName).toLowerCase();

      if (this.isDisabledExtension(fileExtension)) {
        return FileViewerType.FORBIDDEN;
      }

      if (this.imageExtensions.includes(fileExtension)) {
        return FileViewerType.IMG;
      } else if (this.iframeExtensions.includes(fileExtension)) {
        return FileViewerType.IFRAME;
      } else if (this.preExtensions.includes(fileExtension)) {
        return FileViewerType.PRE;
      } else if (this.archiveExtensions.includes(fileExtension)) {
        return FileViewerType.ARCHIVE;
      } else {
        return FileViewerType.UNKNOWN;
      }
    } catch (error) {
      console.warn(error);
    }
  }

  private isDisabledExtension(fileExtension: string): boolean {
    if (this.$unsafeAttachmentPreviewEnabled()) {
      return false;
    }

    return this.unsafeExtensions.includes(fileExtension);
  }

  private getAttachmentPreviewURL(viewer: FileViewer) {
    if (this.data.filePath) {
      return this.domSanitizer.bypassSecurityTrustResourceUrl(
        this.attachmentService.getFilePreviewURL(this.data.origin, this.data.filePath),
      );
    } else {
      return this.domSanitizer.bypassSecurityTrustResourceUrl(
        this.attachmentService.getAttachmentPreviewURL(viewer),
      );
    }
  }

  getAttachmentDownloadURL(attachment: any) {
    if (this.data.filePath) {
      return this.attachmentService.getFileDownloadUrl(
        this.data.origin,
        encodeURIComponent(this.data.filePath),
      );
    } else {
      return this.attachmentService.getAttachmentDownloadURL(this.attachmentListId, attachment);
    }
  }

  handlePreviewError(event) {
    console.warn(event);
    this.$fileViewer.update((fileViewer) => {
      return {
        ...fileViewer,
        viewerType: FileViewerType.ERROR,
        isArchive: false,
      };
    });
  }

  getFileNameFromPath() {
    return this.data.filePath.split('\\').pop().split('/').pop();
  }

  private getContentOnAttachment(viewer: FileViewer) {
    this.getContent(this.attachmentService.getAttachmentPreviewContent(viewer));
  }

  private getContentOnFile(origin: FileViewerFilePathType, filePath: string) {
    this.getContent(this.attachmentService.getFilePreviewContent(origin, filePath));
  }

  private getContent(content$: Observable<string>) {
    content$
      .pipe(
        tap((content: string) => {
          this.$fileViewer.update((fileViewer) => {
            return {
              ...fileViewer,
              content,
            };
          });
          this.$isLoading.set(false);
        }),
        catchError(() => {
          this.handlePreviewError(null);
          return EMPTY;
        }),
      )
      .subscribe();
  }

  displayArchiveFile(path: string) {
    this.$isLoading.set(true);

    const fileName: string = path.substring(path.lastIndexOf('.'));
    const viewerType: FileViewerType = this.getFileViewerType(fileName);

    this.$fileViewer.update((viewer) => {
      return {
        ...viewer,
        activeFile: path,
        viewerType: viewerType !== FileViewerType.ARCHIVE ? viewerType : FileViewerType.UNKNOWN,
      };
    });

    if (viewerType === FileViewerType.PRE) {
      this.getContentOnAttachment(this.$fileViewer());
    }
  }
}
