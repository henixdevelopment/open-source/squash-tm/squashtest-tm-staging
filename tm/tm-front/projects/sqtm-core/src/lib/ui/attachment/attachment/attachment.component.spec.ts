import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AttachmentComponent } from './attachment.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CORE_MODULE_CONFIGURATION } from '../../../core/sqtm-core.tokens';
import { defaultSqtmConfiguration } from '../../../core/sqtm-core.module';
import { SqtmEntityState } from '../../../core/services/entity-view/entity-view.state';
import { of } from 'rxjs';

import { GenericEntityViewService } from '../../../core/services/genric-entity-view/generic-entity-view.service';

describe('AttachmentComponent', () => {
  let component: AttachmentComponent<DummyEntity, 'dummy'>;
  let fixture: ComponentFixture<AttachmentComponent<DummyEntity, 'dummy'>>;

  interface DummyEntity extends SqtmEntityState {
    name: string;
  }

  const componentData$ = of({
    type: 'dummy',
    dummy: {
      attachmentList: {
        id: 1,
        attachments: {
          ids: [],
          entities: {},
        },
      },
    },
  });
  const mockService = { componentData$ };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AttachmentComponent],
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: CORE_MODULE_CONFIGURATION,
          useValue: defaultSqtmConfiguration,
        },
        {
          provide: GenericEntityViewService,
          useValue: mockService,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents()
      .then(() => {
        fixture =
          TestBed.createComponent<AttachmentComponent<DummyEntity, 'dummy'>>(AttachmentComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      });
  }));

  it('should create', waitForAsync(() => {
    expect(component).toBeTruthy();
  }));
});
