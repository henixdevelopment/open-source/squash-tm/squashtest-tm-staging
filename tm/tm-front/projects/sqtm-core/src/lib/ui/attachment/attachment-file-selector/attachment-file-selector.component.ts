import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';

@Component({
  selector: 'sqtm-core-attachment-file-selector',
  templateUrl: './attachment-file-selector.component.html',
  styleUrls: ['./attachment-file-selector.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AttachmentFileSelectorComponent {
  @Input()
  restrictionFiles: string;

  _multiple = true;

  @Input()
  displayButton = false;

  @Input()
  buttonI18nKey = 'sqtm-core.dialog.choose-a-file';

  @Input()
  set multiple(value: boolean) {
    this._multiple = value;
  }

  @ViewChild('file') file: ElementRef;

  @Output()
  uploadFilesEvent: EventEmitter<File[]> = new EventEmitter();

  constructor() {}

  get textClass() {
    return this.displayButton && '__hover_text';
  }

  onClick(): void {
    this.openFileBrowser();
  }

  onClickOnText() {
    if (!this.displayButton) {
      this.onClick();
    }
  }

  openFileBrowser(): void {
    (this.file.nativeElement as HTMLInputElement).click();
  }

  onFileChange(event: Event): void {
    const input = event.target as HTMLInputElement;
    this.uploadFiles(Array.from(input.files));
    input.value = '';
  }

  uploadFiles(files: File[]): void {
    this.uploadFilesEvent.emit(files);
  }
}
