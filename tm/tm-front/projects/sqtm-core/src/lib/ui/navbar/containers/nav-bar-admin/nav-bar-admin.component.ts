import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  QueryList,
  TemplateRef,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { NzSubMenuComponent } from 'ng-zorro-antd/menu';
import { combineLatest, Observable, Subject } from 'rxjs';
import { AuthenticatedUser } from '../../../../model/user/authenticated-user.model';
import { TranslateService } from '@ngx-translate/core';
import { UiManagerService } from '../../../ui-manager/ui-manager.service';
import { AdminReferentialDataService } from '../../../../core/referential/services/admin-referential-data.service';
import { map, take, takeUntil } from 'rxjs/operators';
import { SquashPlatformNavigationService } from '../../../../core/services/navigation/squash-platform-navigation.service';
import {
  isAdminOrManageMilestone,
  isAdminOrManageProject,
} from '../../../../core/referential/state/referential-data.state';

@Component({
  selector: 'sqtm-core-nav-bar-admin',
  templateUrl: './nav-bar-admin.component.html',
  styleUrls: ['./nav-bar-admin.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavBarAdminComponent implements OnInit, OnDestroy {
  @ViewChild('templatePortalContent')
  private userMenuTemplateRef: TemplateRef<any>;

  @ViewChildren(NzSubMenuComponent)
  nzSubMenus: QueryList<NzSubMenuComponent>;

  private unsub$ = new Subject<void>();

  currentUser$: Observable<AuthenticatedUser>;

  showMilestonesMenuItem$: Observable<boolean>;
  toggleButtonTooltipIsVisible: boolean;

  constructor(
    private translateService: TranslateService,
    public uiManager: UiManagerService,
    private adminReferentialDataService: AdminReferentialDataService,
    private navigationService: SquashPlatformNavigationService,
  ) {}

  ngOnInit() {
    this.currentUser$ = this.adminReferentialDataService.authenticatedUser$.pipe(
      takeUntil(this.unsub$),
    );

    this.showMilestonesMenuItem$ = combineLatest([
      this.adminReferentialDataService.milestoneFeatureEnabled$,
      this.currentUser$,
    ]).pipe(
      takeUntil(this.unsub$),
      map(
        ([milestoneFeatureEnabled, currentUser]) =>
          currentUser.admin || (milestoneFeatureEnabled && currentUser.milestoneManager),
      ),
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  toggleCollapsed(): void {
    this.uiManager.toggleNavBar();
    this.toggleButtonTooltipIsVisible = false;
  }

  getWorkspaceName(workspace: string): string {
    const key = `sqtm-core.nav-bar.administration.${workspace}.label`;
    return this.translateService.instant(key);
  }

  exitAdministration() {
    this.uiManager.navBarState$
      .pipe(
        take(1),
        map((state) => {
          if (
            state.urlBeforeAdministration !== '' &&
            !state.urlBeforeAdministration.includes('login')
          ) {
            return state.urlBeforeAdministration;
          } else {
            return 'home-workspace';
          }
        }),
      )
      .subscribe((returnUrl) => {
        this.navigationService.navigateFromMainApplication(returnUrl);
      });
  }

  getWorkspaceLink(workspace: any, loggedAsAdmin: boolean | null) {
    if (workspace === 'system' && !loggedAsAdmin) {
      return ['administration-workspace', workspace, 'synchronizations-supervision'];
    } else if (workspace === 'profiles' && !loggedAsAdmin) {
      return ['administration-workspace', workspace, 'permissions'];
    } else {
      return ['administration-workspace', workspace];
    }
  }

  protected readonly isAdminOrManageProject = isAdminOrManageProject;
  protected readonly isAdminOrManageMilestone = isAdminOrManageMilestone;
}
