import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  OnDestroy,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { fromEvent, Subject } from 'rxjs';
import { delay, filter, takeUntil, tap, throttleTime } from 'rxjs/operators';
import { TemplatePortal } from '@angular/cdk/portal';
import { AboutDialogComponent } from '../../../dialog/components/about-dialog/about-dialog.component';
import { DialogService } from '../../../dialog/services/dialog.service';
import { TranslateService } from '@ngx-translate/core';
import { getSupportedBrowserLang } from '../../../../core/utils/browser-langage.utils';
import {
  DOC_URL_EN,
  DOC_URL_FR,
  NEWSLETTER_URL_EN,
  NEWSLETTER_URL_FR,
} from '../../nav-bar.constants';

@Component({
  selector: 'sqtm-core-horizontal-logout-bar',
  templateUrl: './horizontal-logout-bar.component.html',
  styleUrls: ['./horizontal-logout-bar.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HorizontalLogoutBarComponent implements OnDestroy, AfterViewInit {
  @Input()
  squashVersion: string;

  @ViewChild('help', { read: ElementRef })
  private help: ElementRef;

  @ViewChild('templatePortalContent')
  private helpMenuTemplateRef: TemplateRef<any>;

  private overlayHelpMenuRef: OverlayRef;

  helpMenuOpened = false;

  private helpMouseOverState = false;

  private unsub$ = new Subject<void>();

  constructor(
    private overlay: Overlay,
    private vcRef: ViewContainerRef,
    private dialogService: DialogService,
    private translateService: TranslateService,
  ) {}

  ngAfterViewInit(): void {
    fromEvent(this.help.nativeElement, 'mouseover')
      .pipe(
        takeUntil(this.unsub$),
        throttleTime(50),
        tap(() => (this.helpMouseOverState = true)),
        delay(100),
        filter(() => this.helpMouseOverState && !this.helpMenuOpened),
      )
      .subscribe(() => {
        this.helpMenuOpened = true;
        this.openHelpMenu();
      });

    fromEvent(this.help.nativeElement, 'mouseleave')
      .pipe(
        takeUntil(this.unsub$),
        tap(() => (this.helpMouseOverState = false)),
        delay(100),
        filter(() => !this.helpMouseOverState && this.helpMenuOpened),
      )
      .subscribe(() => {
        this.helpMenuOpened = false;
        this.closeHelpMenu();
      });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  getDocumentationUrl() {
    return this.isFrench() ? DOC_URL_FR : DOC_URL_EN;
  }

  getSquashNewsletterUrl() {
    return this.isFrench() ? NEWSLETTER_URL_FR : NEWSLETTER_URL_EN;
  }

  showAboutDialog() {
    this.dialogService.openDialog({
      id: 'squash-about',
      viewContainerReference: this.vcRef,
      component: AboutDialogComponent,
      data: this.squashVersion,
      width: 600,
    });
  }

  openHelpMenu() {
    const position = this.overlay
      .position()
      .flexibleConnectedTo(this.help)
      .withDefaultOffsetX(5)
      .withDefaultOffsetY(5)
      .withPositions([
        {
          originX: 'start',
          originY: 'bottom',
          overlayX: 'end',
          overlayY: 'top',
        },
      ]);

    this.overlayHelpMenuRef = this.overlay.create({ positionStrategy: position });
    const avatarMenuPortal = new TemplatePortal(this.helpMenuTemplateRef, this.vcRef);
    this.overlayHelpMenuRef.attach(avatarMenuPortal);

    fromEvent(this.overlayHelpMenuRef.overlayElement, 'mouseover')
      .pipe(
        takeUntil(this.unsub$),
        filter(() => !this.helpMouseOverState),
      )
      .subscribe(() => {
        this.helpMouseOverState = true;
      });

    fromEvent(this.overlayHelpMenuRef.overlayElement, 'mouseleave')
      .pipe(
        takeUntil(this.unsub$),
        tap(() => (this.helpMouseOverState = false)),
        delay(200),
        filter(() => !this.helpMouseOverState),
      )
      .subscribe(() => {
        this.helpMenuOpened = false;
        this.closeHelpMenu();
      });
  }

  closeHelpMenu() {
    if (this.overlayHelpMenuRef) {
      this.overlayHelpMenuRef.detach();
      this.overlayHelpMenuRef.dispose();
    }
  }

  private isFrench(): boolean {
    return getSupportedBrowserLang(this.translateService) === 'fr';
  }
}
