export const DOC_URL_FR =
  'https://tm-fr.doc.squashtest.com/latest?utm_source=Appli_squash&utm_medium=link';
export const DOC_URL_EN =
  'https://tm-en.doc.squashtest.com/latest?utm_source=Appli_squash&utm_medium=link';
export const NEWSLETTER_URL_FR =
  'https://squashtest.us13.list-manage.com/subscribe?u=cb5cbcd668e9d36c6f35988fa&id=400a1358c9';
export const NEWSLETTER_URL_EN =
  'https://squashtest.us13.list-manage.com/subscribe?u=cb5cbcd668e9d36c6f35988fa&id=feb76bc687';
export const OPENTESTFACTORY_DOC_URL =
  'https://opentestfactory.org?utm_source=Appli_squash&utm_medium=link';
