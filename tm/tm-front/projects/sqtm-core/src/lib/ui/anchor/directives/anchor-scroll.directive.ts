import {
  AfterViewInit,
  Directive,
  ElementRef,
  Host,
  NgZone,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { fromEvent, Subject } from 'rxjs';
import { AnchorPanel } from './anchor-panel';
import { distinctUntilChanged, filter, map, skip, takeUntil } from 'rxjs/operators';
import { ViewIdDirective } from './view-id.directive';
import { AnchorService } from '../service/anchor.service';

@Directive({
  selector: '[sqtmCoreAnchorScroll]',
})
export class AnchorScrollDirective implements OnInit, AfterViewInit, OnDestroy {
  private unsub$ = new Subject<void>();

  private panels: AnchorPanel[] = [];

  private offsetTop = 10;

  constructor(
    @Host() public host: ElementRef,
    private ngZone: NgZone,
    private viewIdDirective: ViewIdDirective,
    private anchorService: AnchorService,
  ) {}

  ngOnInit(): void {
    this.ngZone.runOutsideAngular(() => {
      fromEvent(this.host.nativeElement, 'scroll')
        .pipe(
          skip(1),
          map(() => this.selectActivePanel()),
          distinctUntilChanged(),
        )
        .subscribe((panelId: string) => {
          this.notifyScrollToPanel(panelId);
        });
    });
  }

  ngAfterViewInit(): void {
    this.anchorService.requestScroll$
      .pipe(
        takeUntil(this.unsub$),
        filter((scrollRequest) => this.viewIdDirective.id === scrollRequest.viewId),
      )
      .subscribe((scrollRequest) => {
        const panel = this.panels.find((p) => p.getId() === scrollRequest.selectedLinkId);
        if (panel) {
          // Why 2 setTimeouts? Because the first one doesn't work when the page is accessed with history.back() or
          // history.forward(). The second one works in most cases but leaves a small delay that is visible to the user.
          // Please let me know if you find a solution to scroll consistently that doesn't feel like a hack. Git has my email.
          setTimeout(() => panel.getElementRef().nativeElement.scrollIntoView());
          setTimeout(() => panel.getElementRef().nativeElement.scrollIntoView(), 100);
        }
      });
  }

  registerPanel(panel: AnchorPanel) {
    this.panels.push(panel);
  }

  selectActivePanel() {
    const nativeElement = this.host.nativeElement;
    const topHost = nativeElement.getBoundingClientRect().top;
    const sections = [];

    this.panels.forEach((panel) => {
      const topElement = panel.getElementRef().nativeElement.getBoundingClientRect().top;
      const offset = topElement - topHost;
      if (offset <= this.offsetTop) {
        sections.push({ panelId: panel.getId(), offset: offset });
      }
    });
    const maxSection = sections.reduce((prev, curr) => (curr.offset > prev.offset ? curr : prev));
    return maxSection.panelId;
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private notifyScrollToPanel(panelId: string) {
    this.ngZone.run(() => {
      this.anchorService.notifyActiveLink(this.viewIdDirective.id, panelId);
    });
  }
}
