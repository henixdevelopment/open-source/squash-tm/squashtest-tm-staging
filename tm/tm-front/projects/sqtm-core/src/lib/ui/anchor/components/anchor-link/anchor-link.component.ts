import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { AnchorService } from '../../service/anchor.service';
import { AnchorGroupComponent } from '../anchor-group/anchor-group.component';
import { filter, map, take, takeUntil } from 'rxjs/operators';
import { AnchorsState } from '../../service/anchors.state';
import { ActivatedRoute, Router } from '@angular/router';
import { anchorLogger } from '../../anchor.logger';
import { ViewIdDirective } from '../../directives/view-id.directive';
import { defer, from, Observable, Subject } from 'rxjs';

const logger = anchorLogger.compose('AnchorLinkComponent');

@Component({
  selector: 'sqtm-core-anchor-link',
  templateUrl: './anchor-link.component.html',
  styleUrls: ['./anchor-link.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AnchorLinkComponent implements OnInit, OnDestroy, AfterViewInit {
  private unsub$ = new Subject<void>();

  @Input()
  iconName: string;

  @Input()
  id: string;

  @Input()
  set visible(isVisible: boolean) {
    this.anchorService.changeLinkVisibility(this.viewIdDirective.id, this.id, isVisible);
    this._visible = isVisible;
  }

  get visible(): boolean {
    return this._visible;
  }

  private _visible = true;

  active$: Observable<boolean>;

  get viewId(): string {
    return this.viewIdDirective.id;
  }

  constructor(
    public cdr: ChangeDetectorRef,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private anchorService: AnchorService,
    private viewIdDirective: ViewIdDirective,
    private anchorGroup: AnchorGroupComponent,
  ) {
    anchorGroup.registerAnchorLink(this);
  }

  ngOnInit() {
    this.anchorService.registerLink(this.viewId, {
      group: this.anchorGroup.id,
      id: this.id,
      active: true,
      visible: this._visible,
    });

    this.active$ = this.anchorService.getActiveLink(this.viewId).pipe(
      takeUntil(this.unsub$),
      map((activeLinkId) => this.visible && activeLinkId === this.id),
    );
  }

  ngAfterViewInit() {
    this.anchorService.state$
      .pipe(
        take(1),
        filter(() => this.visible),
        filter((state: AnchorsState) => {
          const anchorState = state.entities[this.viewId];
          let filterAnchor = false;
          if (anchorState) {
            filterAnchor = this.id === anchorState.selectedLinkId;
          }
          return filterAnchor;
        }),
      )
      .subscribe(() => this.restoreActiveLink());
  }

  makeActive() {
    if (logger.isDebugEnabled()) {
      logger.debug(
        `Activate Link: ViewId:${this.viewIdDirective.id} GroupId:${this.anchorGroup.id} LinkId:${this.id}`,
      );
    }

    this.navigateToLink().subscribe(() =>
      this.anchorService.notifyAndNavigateToActiveLink(this.viewIdDirective.id, this.id),
    );
  }

  restoreActiveLink() {
    if (logger.isDebugEnabled()) {
      logger.debug(
        `Restore Active Link: ViewId:${this.viewIdDirective.id} GroupId:${this.anchorGroup.id} LinkId:${this.id}`,
      );
    }

    this.navigateToLink().subscribe(() =>
      this.anchorService.notifyRestoredLink(this.viewIdDirective.id, this.id),
    );
  }

  navigateToLink(): Observable<boolean> {
    if (!this.anchorGroup?.id) {
      throw new Error(
        'AnchorLinkComponent should have an AnchorGroupComponent with a defined ID as parent',
      );
    }

    return defer(() =>
      from(
        this.router.navigate([this.anchorGroup.id], {
          queryParams: { anchor: this.id },
          queryParamsHandling: 'merge',
          relativeTo: this.activeRoute,
          replaceUrl: true, // We want the URL to change, but we don't want a new entry in the navigation history
        }),
      ),
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
