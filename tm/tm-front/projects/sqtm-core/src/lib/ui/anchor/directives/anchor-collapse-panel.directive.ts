import {
  AfterViewInit,
  ChangeDetectorRef,
  Directive,
  Host,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { NzCollapsePanelComponent } from 'ng-zorro-antd/collapse';
import { AnchorService } from '../service/anchor.service';
import { ViewIdDirective } from './view-id.directive';
import { take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Directive({
  selector: '[sqtmCoreAnchorCollapsePanel]',
})
export class AnchorCollapsePanelDirective implements AfterViewInit, OnInit, OnDestroy {
  private unsub$ = new Subject<void>();

  @Input('sqtmCoreAnchorCollapsePanel')
  id: string;

  constructor(
    @Host() private nzCollapsePanel: NzCollapsePanelComponent,
    private anchorService: AnchorService,
    private viewIdDirective: ViewIdDirective,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit(): void {
    this.anchorService
      .isActive(this.viewIdDirective.id, this.id)
      .pipe(take(1))
      .subscribe((active) => {
        this.nzCollapsePanel.nzActive = active;
        this.cdr.detectChanges();
      });
  }

  ngAfterViewInit(): void {
    this.nzCollapsePanel.nzActiveChange.pipe(takeUntil(this.unsub$)).subscribe((event) => {
      this.anchorService.notifyCollapsePanel(this.viewIdDirective.id, this.id, event);
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
