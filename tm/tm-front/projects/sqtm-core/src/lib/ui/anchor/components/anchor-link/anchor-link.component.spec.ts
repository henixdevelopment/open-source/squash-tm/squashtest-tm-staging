import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AnchorLinkComponent } from './anchor-link.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { ViewIdDirective } from '../../directives/view-id.directive';
import { AnchorService } from '../../service/anchor.service';
import { AnchorGroupComponent } from '../anchor-group/anchor-group.component';
import { TestingUtilsModule } from '../../../testing-utils/testing-utils.module';

describe('AnchorLinkComponent', () => {
  let component: AnchorLinkComponent;
  let fixture: ComponentFixture<AnchorLinkComponent>;
  const anchorGroup = {
    ...jasmine.createSpyObj<AnchorGroupComponent>(['registerAnchorLink']),
    id: 'groupId',
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AnchorLinkComponent],
      imports: [TestingUtilsModule, RouterTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        AnchorService,
        { provide: ViewIdDirective, useValue: {} },
        { provide: AnchorGroupComponent, useValue: anchorGroup },
      ],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(AnchorLinkComponent);
        component = fixture.componentInstance;
        component.id = 'linkId';
        fixture.detectChanges();
      });
  }));

  it('should create', waitForAsync(() => {
    expect(component).toBeTruthy();
  }));
});
