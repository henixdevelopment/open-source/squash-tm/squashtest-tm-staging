import {
  AfterViewInit,
  Directive,
  ElementRef,
  Host,
  NgZone,
  OnDestroy,
  OnInit,
} from '@angular/core';
import * as CssElementQuery from 'css-element-queries';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

const MIN_GHOST_HEIGHT = 20;

@Directive({
  selector: '[sqtmCoreAnchorResize]',
})
export class AnchorResizeDirective implements OnInit, AfterViewInit, OnDestroy {
  private unsub$ = new Subject<void>();

  private ghostDiv: ElementRef;
  private resizeSensor: CssElementQuery.ResizeSensor;
  private resizeSensorSubject: Subject<ResizeEvent> = new Subject();
  private resizeSensor$ = this.resizeSensorSubject.pipe();

  private resizeSensorClosed: CssElementQuery.ResizeSensor;
  private resizeSensorClosedSubject: Subject<ResizeEvent> = new Subject();
  private resizeSensorClosed$ = this.resizeSensorClosedSubject.pipe();
  private lastPanelElement: ElementRef;
  private lastPanelInitialSize: number;

  constructor(
    @Host() private elementRef: ElementRef,
    private ngZone: NgZone,
  ) {}

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    this.resizeSensor$.pipe(takeUntil(this.unsub$)).subscribe(() => {
      this.modifyGhostHeight();
    });

    this.resizeSensorClosed$.pipe(takeUntil(this.unsub$)).subscribe(() => {
      this.modifyGhostHeight();
    });

    this.ngZone.runOutsideAngular(() => {
      this.resizeSensor = new CssElementQuery.ResizeSensor(
        this.elementRef.nativeElement,
        ($event: ResizeEvent) => {
          this.resizeSensorSubject.next($event);
        },
      );
    });
  }

  registerLastElement(element: ElementRef) {
    this.lastPanelElement = element;
    this.ngZone.runOutsideAngular(() => {
      this.resizeSensorClosed = new CssElementQuery.ResizeSensor(
        this.lastPanelElement.nativeElement,
        ($event: ResizeEvent) => {
          this.resizeSensorClosedSubject.next($event);
        },
      );
    });
  }

  registerGhostDiv(element: ElementRef) {
    this.ghostDiv = element;
  }

  modifyGhostHeight() {
    if (
      this.lastPanelElement &&
      this.lastPanelElement.nativeElement.clientHeight !== this.lastPanelInitialSize
    ) {
      const hostHeight = this.elementRef.nativeElement.clientHeight;
      const elementHeight = this.lastPanelElement.nativeElement.clientHeight;
      this.lastPanelInitialSize = elementHeight;
      const ghostHeight = Math.max(MIN_GHOST_HEIGHT, hostHeight - elementHeight);

      if (this.ghostDiv) {
        this.ghostDiv.nativeElement.style.height = ghostHeight + 'px';
      }
    }
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
    if (this.resizeSensorClosed) {
      this.resizeSensorClosed.detach();
    }
    if (this.resizeSensor) {
      this.resizeSensor.detach();
    }
  }
}

interface ResizeEvent {
  height: number;
  width: number;
}
