import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnchorComponent } from './container/anchor.component';
import { AnchorLinkComponent } from './components/anchor-link/anchor-link.component';
import { RouterModule } from '@angular/router';
import { AnchorGroupComponent } from './components/anchor-group/anchor-group.component';
import { DefaultAnchorLinkDirective } from './directives/default-anchor-link.directive';
import { ViewIdDirective } from './directives/view-id.directive';
import { AnchorScrollDirective } from './directives/anchor-scroll.directive';
import { AnchorPanelDirective } from './directives/anchor-panel.directive';
import { AnchorCollapsePanelDirective } from './directives/anchor-collapse-panel.directive';
import { AnchorLastPanelDirective } from './directives/anchor-last-panel.directive';
import { AnchorGhostDivDirective } from './directives/anchor-ghost-div.directive';
import { AnchorResizeDirective } from './directives/anchor-resize.directive';
import { NzIconModule } from 'ng-zorro-antd/icon';

@NgModule({
  declarations: [
    AnchorComponent,
    AnchorLinkComponent,
    AnchorGroupComponent,
    DefaultAnchorLinkDirective,
    ViewIdDirective,
    AnchorScrollDirective,
    AnchorPanelDirective,
    AnchorCollapsePanelDirective,
    AnchorLastPanelDirective,
    AnchorGhostDivDirective,
    AnchorResizeDirective,
  ],
  imports: [CommonModule, RouterModule, NzIconModule],
  exports: [
    AnchorComponent,
    AnchorLinkComponent,
    AnchorGroupComponent,
    DefaultAnchorLinkDirective,
    ViewIdDirective,
    AnchorScrollDirective,
    AnchorPanelDirective,
    AnchorCollapsePanelDirective,
    AnchorLastPanelDirective,
    AnchorGhostDivDirective,
    AnchorResizeDirective,
  ],
})
export class AnchorModule {}
