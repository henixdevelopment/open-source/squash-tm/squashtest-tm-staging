import { AfterViewInit, Directive, ElementRef, Host, Input, OnInit } from '@angular/core';
import { AnchorScrollDirective } from './anchor-scroll.directive';
import { AnchorPanel } from './anchor-panel';

@Directive({
  selector: '[sqtmCoreAnchorPanel]',
})
export class AnchorPanelDirective implements OnInit, AnchorPanel, AfterViewInit {
  @Input('sqtmCoreAnchorPanel')
  id: string;

  constructor(
    private anchorScroll: AnchorScrollDirective,
    @Host() private elementRef: ElementRef,
  ) {}

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    this.anchorScroll.registerPanel(this);
  }

  getElementRef(): ElementRef {
    return this.elementRef;
  }

  getId(): string {
    return this.id;
  }
}
