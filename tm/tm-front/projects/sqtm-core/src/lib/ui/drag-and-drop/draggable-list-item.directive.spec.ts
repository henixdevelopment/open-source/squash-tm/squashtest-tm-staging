import { DraggableListItemDirective } from './draggable-list-item.directive';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { DraggableListDirective } from './draggable-list.directive';
import { OverlayModule } from '@angular/cdk/overlay';

@Component({
  selector: 'sqtm-core-drag-and-drop-handler-testing',
  template: ` <div [sqtmCoreDraggableListItem]="'testing-item'"></div> `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DragAndDropListItemTestingComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}

describe('DraggableListItemDirective', () => {
  let component: DragAndDropListItemTestingComponent;
  let fixture: ComponentFixture<DragAndDropListItemTestingComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [OverlayModule],
      declarations: [DragAndDropListItemTestingComponent, DraggableListItemDirective],
      providers: [{ provide: DraggableListDirective, useValue: {} }],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(DragAndDropListItemTestingComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      });
  }));

  it('should create an instance', () => {
    expect(component).toBeTruthy();
  });
});
