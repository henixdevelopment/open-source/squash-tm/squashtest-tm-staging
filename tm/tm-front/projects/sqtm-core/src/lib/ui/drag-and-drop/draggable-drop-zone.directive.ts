import {
  Directive,
  ElementRef,
  EventEmitter,
  Host,
  Input,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
} from '@angular/core';
import { DragAndDropService } from './drag-and-drop.service';
import { fromEvent, Subject } from 'rxjs';
import { filter, takeUntil, tap, withLatestFrom } from 'rxjs/operators';
import { DragAndDropData } from './drag-and-drop-data.model';
import {
  SqtmDragCancelEvent,
  SqtmDragEnterEvent,
  SqtmDragLeaveEvent,
  SqtmDropEvent,
} from './events';
import { DraggableDropTarget } from './draggable-drop-target';
import { dndLogger } from './sqtm-drag-and-drop.logger';

const logger = dndLogger.compose('DraggableDropZoneDirective');

@Directive({
  selector: '[sqtmCoreDraggableDropZone]',
})
export class DraggableDropZoneDirective implements OnInit, OnDestroy, DraggableDropTarget {
  private unsub$ = new Subject<void>();

  @Input('sqtmCoreDraggableDropZone')
  id: string;

  @Input('showHover')
  showHover = false;

  @Output()
  dragEnter = new EventEmitter<SqtmDragEnterEvent>();

  @Output()
  dragCancel = new EventEmitter<SqtmDragCancelEvent>();

  @Output()
  drop = new EventEmitter<SqtmDropEvent>();

  @Output()
  dragLeave = new EventEmitter<SqtmDragLeaveEvent>();

  constructor(
    @Host() private host: ElementRef,
    private dndService: DragAndDropService,
    private renderer: Renderer2,
  ) {}

  ngOnInit(): void {
    fromEvent(this.host.nativeElement, 'mouseup')
      .pipe(
        takeUntil(this.unsub$),
        withLatestFrom(this.dndService.dragData$),
        filter(([_$event, dragData]) => Boolean(dragData)),
        tap(() => this.handleDragLeaveSideEffect()),
      )
      .subscribe(([$event, dragData]: [MouseEvent, DragAndDropData]) => {
        if (logger.isDebugEnabled()) {
          logger.debug(`Drop event in zone ${this.id}`);
        }
        $event.stopPropagation();
        this.dndService.notifyDrop(this.id);
        this.drop.emit(new SqtmDropEvent(this.id, dragData));
      });

    fromEvent(this.host.nativeElement, 'mouseenter')
      .pipe(
        takeUntil(this.unsub$),
        withLatestFrom(this.dndService.dragData$),
        filter(([, dragData]) => Boolean(dragData)),
        tap(() => this.handleDragEnterSideEffects()),
      )
      .subscribe(([, dragData]: [MouseEvent, DragAndDropData]) => {
        const dragEnterEvent = new SqtmDragEnterEvent(this.id, dragData);
        this.dragEnter.next(dragEnterEvent);
        this.dndService.notifyDragEnter(dragEnterEvent);
      });

    fromEvent(this.host.nativeElement, 'mouseleave')
      .pipe(
        takeUntil(this.unsub$),
        withLatestFrom(this.dndService.dragData$),
        filter(([, dragData]) => Boolean(dragData)),
        tap(() => this.handleDragLeaveSideEffect()),
      )
      .subscribe(([, dragData]: [MouseEvent, DragAndDropData]) => {
        const dragLeaveEvent = new SqtmDragLeaveEvent(this.id, dragData);
        this.dragLeave.next(dragLeaveEvent);
        this.dndService.notifyDragLeave(dragLeaveEvent);
      });

    this.dndService.registerDropZone(this);
  }

  ngOnDestroy(): void {
    this.dndService.notifyListRemoved(this.id);
    this.unsub$.next();
    this.unsub$.complete();
  }

  notifyCancelDrag(dragData: DragAndDropData) {
    this.dragCancel.emit(new SqtmDragCancelEvent(dragData));
  }

  private handleDragEnterSideEffects() {
    if (this.showHover) {
      this.markAsHovered();
    }
  }

  public markAsHovered() {
    this.renderer.addClass(this.host.nativeElement, '__hover_current_ws_border');
  }

  private handleDragLeaveSideEffect() {
    if (this.showHover) {
      this.unmarkAsHovered();
    }
  }

  public unmarkAsHovered() {
    this.renderer.removeClass(this.host.nativeElement, '__hover_current_ws_border');
  }
}
