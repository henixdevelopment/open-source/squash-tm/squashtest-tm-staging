import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { DraggedContentRenderer } from '../dragged-content-renderer';
import { DragAndDropData } from '../drag-and-drop-data.model';
import { DRAG_AND_DROP_DATA } from '../constants';

@Component({
  selector: 'sqtm-core-default-dragged-content',
  templateUrl: './default-dragged-content.component.html',
  styleUrls: ['./default-dragged-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DefaultDraggedContentComponent extends DraggedContentRenderer implements OnInit {
  constructor(@Inject(DRAG_AND_DROP_DATA) dragAnDropData: DragAndDropData) {
    super(dragAnDropData);
  }

  ngOnInit() {}
}
