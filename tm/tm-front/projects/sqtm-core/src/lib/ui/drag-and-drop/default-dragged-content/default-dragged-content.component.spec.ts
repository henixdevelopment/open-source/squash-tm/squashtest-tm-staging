import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DefaultDraggedContentComponent } from './default-dragged-content.component';
import { DRAG_AND_DROP_DATA } from '../constants';

describe('DefaultDraggedContentComponent', () => {
  let component: DefaultDraggedContentComponent;
  let fixture: ComponentFixture<DefaultDraggedContentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DefaultDraggedContentComponent],
      providers: [{ provide: DRAG_AND_DROP_DATA, useValue: {} }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultDraggedContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
