import { Directive, ElementRef, Host, OnDestroy, OnInit } from '@angular/core';
import { DraggableListDirective } from './draggable-list.directive';
import { DragAndDropService } from './drag-and-drop.service';
import { fromEvent, Subject } from 'rxjs';
import { filter, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { DraggableListItemDirective } from './draggable-list-item.directive';
import { createStartDragObservable } from './drag-and-drop.utils';
import { DndPosition } from './events';
import { isLeftClick } from '../../core/utils/mouse-event-utils';

@Directive({
  selector: '[sqtmCoreDraggableItemHandler]',
})
export class DraggableItemHandlerDirective implements OnInit, OnDestroy {
  private unsub$ = new Subject<void>();

  constructor(
    @Host() public host: ElementRef,
    private dndList: DraggableListDirective,
    private dndService: DragAndDropService,
    private dndItem: DraggableListItemDirective,
  ) {}

  ngOnInit(): void {
    this.dndItem.registerHandler();
    fromEvent(this.host.nativeElement, 'mousedown')
      .pipe(
        takeUntil(this.unsub$),
        filter(($event: MouseEvent) => isLeftClick($event)),
        map(($event: MouseEvent) => {
          return { left: $event.clientX, top: $event.clientY };
        }),
        switchMap((position: DndPosition) => createStartDragObservable(this.host, position)),
        tap(() => this.clearNativeSelection()),
      )
      .subscribe((position) => {
        this.dndItem.requireDragStart(position);
      });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  // Avoids weird behaviors when text is highlighted while dragging an item
  private clearNativeSelection(): void {
    document.getSelection().empty();
  }
}
