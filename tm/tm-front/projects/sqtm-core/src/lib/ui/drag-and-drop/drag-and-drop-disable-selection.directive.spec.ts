import { DragAndDropDisableSelectionDirective } from './drag-and-drop-disable-selection.directive';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { OverlayModule } from '@angular/cdk/overlay';

@Component({
  selector: 'sqtm-core-drag-and-drop-disable-testing',
  template: ` <div sqtmCoreDragAndDropDisableSelection></div> `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DragAndDropDisableTestingComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}

describe('DragAndDropDisableSelectionDirective', () => {
  let component: DragAndDropDisableTestingComponent;
  let fixture: ComponentFixture<DragAndDropDisableTestingComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [OverlayModule],
      declarations: [DragAndDropDisableTestingComponent, DragAndDropDisableSelectionDirective],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(DragAndDropDisableTestingComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      });
  }));

  it('should create an instance', () => {
    expect(component).toBeTruthy();
  });
});
