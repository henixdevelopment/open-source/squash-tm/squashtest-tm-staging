import { ComponentFactoryResolver, Inject, Injectable, NgZone } from '@angular/core';
import { BehaviorSubject, fromEvent, Observable, Subject } from 'rxjs';
import { filter, map, take, withLatestFrom } from 'rxjs/operators';
import { DOCUMENT } from '@angular/common';
import { DragAndDropData } from './drag-and-drop-data.model';
import { DraggableListDirective } from './draggable-list.directive';
import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal, PortalInjector } from '@angular/cdk/portal';
import { DndPosition, SqtmDragEnterEvent, SqtmDragLeaveEvent } from './events';
import { DRAG_AND_DROP_DATA } from './constants';
import { DraggableDropTarget } from './draggable-drop-target';
import { dndLogger } from './sqtm-drag-and-drop.logger';
import { DraggableDropZoneDirective } from './draggable-drop-zone.directive';

const logger = dndLogger.compose('DragAndDropService');

/** @dynamic */
@Injectable({
  providedIn: 'root',
})
export class DragAndDropService {
  private lists = new Map<string, DraggableDropTarget>();

  private _dndData = new BehaviorSubject<DragAndDropData>(null);
  private _dragEnter = new Subject<SqtmDragEnterEvent>();
  private _dragLeave = new Subject<SqtmDragLeaveEvent>();
  public dragAndDrop$: Observable<boolean> = this._dndData
    .asObservable()
    .pipe(map((dndData) => Boolean(dndData)));
  public dragData$: Observable<DragAndDropData> = this._dndData.asObservable();

  // Global observable emitting all drag enter events. Take care to filter by id...
  // The preferred way to react to drag event is directly by listening events on the list/drop zone.
  // Use this if ANOTHER component must react to the event
  public dragEnter$: Observable<SqtmDragEnterEvent> = this._dragEnter.asObservable();

  // as dragEnter$ but for dragleave.
  public dragLeave$: Observable<SqtmDragLeaveEvent> = this._dragLeave.asObservable();

  private overlayRef: OverlayRef;

  constructor(
    private overlay: Overlay,
    @Inject(DOCUMENT) private document: Document,
    private ngZone: NgZone,
  ) {
    const overlayConfig = this.createConfig();
    this.overlayRef = this.overlay.create(overlayConfig);

    fromEvent(this.document.body, 'mouseup')
      .pipe(
        withLatestFrom(this.dragAndDrop$),
        filter(([, dnd]) => dnd),
      )
      .subscribe(() => {
        this.cancelDrag();
      });

    fromEvent(this.document.body, 'keydown')
      .pipe(
        withLatestFrom(this.dragAndDrop$),
        filter(([, dnd]) => dnd),
      )
      .subscribe(() => {
        this.cancelDrag();
      });

    this.ngZone.runOutsideAngular(() => {
      fromEvent(this.document.body, 'mousemove')
        .pipe(
          withLatestFrom(this.dragAndDrop$),
          filter(([, dnd]) => dnd),
        )
        .subscribe(([$event, _dnd]: [MouseEvent, boolean]) => {
          this.updateOverlayPosition($event.clientX, $event.clientY);
        });
    });
  }

  private cancelDrag() {
    logger.debug('Cancel Drag Operation');
    this.removeOverlay();
    this.notifyCancelToAllLists();
    this._dndData.next(null);
  }

  notifyDragStart(dragAndDropData: DragAndDropData, position: DndPosition) {
    logger.debug(`DndService start drag ${JSON.stringify(dragAndDropData)}`);
    this.updateOverlayPosition(position.left, position.top);
    // The cast is safe. A dnd can only start from a dnd list not a simple drop zone
    const listDirective = this.lists.get(dragAndDropData.origin) as DraggableListDirective;
    const draggedContentInjector = new PortalInjector(
      listDirective.viewContainerRef.injector,
      new WeakMap<any, any>([[DRAG_AND_DROP_DATA, dragAndDropData]]),
    );

    const component = new ComponentPortal(
      listDirective.draggedContentRenderer,
      listDirective.viewContainerRef,
      draggedContentInjector,
      draggedContentInjector.get(ComponentFactoryResolver),
    );
    this.overlayRef.attach(component);
    this._dndData.next(dragAndDropData);
  }

  notifyDrop(listId: string) {
    this.removeOverlay();
    this.notifyCancelToOtherLists(listId);
    this._dndData.next(null);
  }

  notifyListRemoved(listId: string) {
    this.removeOverlay();
    this.lists.delete(listId);
  }

  notifyDragEnter(event: SqtmDragEnterEvent) {
    this._dragEnter.next(event);
  }

  notifyDragLeave(event: SqtmDragLeaveEvent) {
    this._dragLeave.next(event);
  }

  registerList(list: DraggableListDirective) {
    if (this.lists.has(list.listId)) {
      throw Error(`List ${list.listId} is already registered in DragAndDrop Service`);
    }
    this.lists.set(list.listId, list);
  }

  registerDropZone(zone: DraggableDropZoneDirective) {
    if (this.lists.has(zone.id)) {
      throw Error(`Zone ${zone.id} is already registered in DragAndDrop Service`);
    }
    this.lists.set(zone.id, zone);
  }

  private notifyCancelToOtherLists(dropListId: string) {
    this.dragData$.pipe(take(1)).subscribe((dragData) => {
      this.lists.forEach((list, listId) => {
        if (listId !== dropListId) {
          list.notifyCancelDrag(dragData);
        }
      });
    });
  }

  private notifyCancelToAllLists() {
    this.dragData$.pipe(take(1)).subscribe((dragData) => {
      this.lists.forEach((list) => {
        list.notifyCancelDrag(dragData);
      });
    });
  }

  private createConfig(): OverlayConfig {
    const positionStrategy = this.overlay.position().global();
    return { positionStrategy };
  }

  private removeOverlay() {
    if (this.overlayRef && this.overlayRef.hasAttached()) {
      this.overlayRef.detach();
    }
  }

  private updateOverlayPosition(left: number, top: number) {
    if (this.overlayRef) {
      const positionStrategy = this.overlay
        .position()
        .global()
        .left(`${left + 20}px`)
        .top(`${top + 20}px`);
      this.overlayRef.updatePositionStrategy(positionStrategy);
    }
  }
}
