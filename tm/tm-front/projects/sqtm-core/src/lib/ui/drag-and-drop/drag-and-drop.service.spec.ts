import { TestBed } from '@angular/core/testing';

import { DragAndDropService } from './drag-and-drop.service';
import { OverlayModule } from '@angular/cdk/overlay';

describe('DragAndDropServiceService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [OverlayModule],
    }),
  );

  it('should be created', () => {
    const service: DragAndDropService = TestBed.inject(DragAndDropService);
    expect(service).toBeTruthy();
  });
});
