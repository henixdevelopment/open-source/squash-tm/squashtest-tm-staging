import { DragAndDropData } from './drag-and-drop-data.model';
import { Identifier } from '../../model/entity.model';

export abstract class AbstractSqtmDragAndDropEvent {
  abstract readonly kind: SqtmDragAndDropEventKind;
  constructor(public readonly dragAndDropData: DragAndDropData) {}
}

export type SqtmDragAndDropEvent =
  | SqtmDragAbortStartEvent
  | SqtmDragStartEvent
  | SqtmDragOverEvent
  | SqtmDropEvent
  | SqtmDragEnterEvent
  | SqtmDragLeaveEvent
  | SqtmDragCancelEvent;

export type SqtmDragAndDropEventKind =
  | SqtmDragAbortStartEventKind
  | SqtmDragStartEventKind
  | SqtmDragOverItemEventKind
  | SqtmDropEventKind
  | SqtmDragCancelEventKind
  | SqtmDragLeaveEventKind
  | SqtmDragEnterEventKind;

export type SqtmDragAbortStartEventKind = 'sqtmDragAbortStartEvent';
export type SqtmDragStartEventKind = 'sqtmDragStartEvent';
export type SqtmDragLeaveEventKind = 'sqtmDragLeaveEvent';
export type SqtmDragCancelEventKind = 'sqtmDragCancelEvent';
export type SqtmDropEventKind = 'sqtmDropEvent';
export type SqtmDragOverItemEventKind = 'sqtmDragOverItemEventKind';
export type SqtmDragEnterEventKind = 'sqtmDragEnterEventKind';

export class SqtmDragAbortStartEvent extends AbstractSqtmDragAndDropEvent {
  kind: SqtmDragAbortStartEventKind = 'sqtmDragAbortStartEvent';
}

export class SqtmDragStartEvent extends AbstractSqtmDragAndDropEvent {
  kind: SqtmDragStartEventKind = 'sqtmDragStartEvent';

  constructor(
    public readonly dragAndDropData: DragAndDropData,
    public readonly position: DndPosition,
  ) {
    super(dragAndDropData);
  }
}

export class SqtmDragLeaveEvent extends AbstractSqtmDragAndDropEvent {
  kind: SqtmDragLeaveEventKind = 'sqtmDragLeaveEvent';

  constructor(
    public readonly dropTargetId: string,
    public readonly dragAndDropData: DragAndDropData,
  ) {
    super(dragAndDropData);
  }
}

export class SqtmDropEvent extends AbstractSqtmDragAndDropEvent {
  kind: SqtmDropEventKind = 'sqtmDropEvent';

  constructor(
    public readonly dropTargetId: string,
    public readonly dragAndDropData: DragAndDropData,
  ) {
    super(dragAndDropData);
  }
}

export class SqtmDragCancelEvent extends AbstractSqtmDragAndDropEvent {
  kind: SqtmDragCancelEventKind = 'sqtmDragCancelEvent';

  constructor(public readonly dragAndDropData: DragAndDropData) {
    super(dragAndDropData);
  }
}

export class SqtmDragOverEvent extends AbstractSqtmDragAndDropEvent {
  kind: SqtmDragOverItemEventKind = 'sqtmDragOverItemEventKind';

  constructor(
    public readonly dndTarget: DragAndDropTarget,
    public readonly dragAndDropData: DragAndDropData,
  ) {
    super(dragAndDropData);
  }
}

export class SqtmDragEnterEvent extends AbstractSqtmDragAndDropEvent {
  kind: SqtmDragEnterEventKind = 'sqtmDragEnterEventKind';

  constructor(
    public readonly dropTargetId: string,
    public readonly dragAndDropData: DragAndDropData,
  ) {
    super(dragAndDropData);
  }
}

export class DragAndDropTarget {
  id: Identifier;
  zone: DragAndDropZone;
  // data: any = {};
}

export type DragAndDropZone = 'above' | 'into' | 'below';

export interface DndPosition {
  top: number;
  left: number;
}
