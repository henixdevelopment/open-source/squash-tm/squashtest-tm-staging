import { Directive, Inject, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { DragAndDropService } from './drag-and-drop.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

/** @dynamic */
@Directive({
  selector: '[sqtmCoreDragAndDropDisableSelection]',
})
export class DragAndDropDisableSelectionDirective implements OnInit, OnDestroy {
  private unsub$ = new Subject<void>();

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private renderer: Renderer2,
    private dndService: DragAndDropService,
  ) {}

  ngOnInit(): void {
    this.dndService.dragAndDrop$.pipe(takeUntil(this.unsub$)).subscribe((dnd) => {
      if (dnd) {
        this.disableTextSelection();
      } else {
        this.enableTextSelection();
      }
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  disableTextSelection() {
    this.renderer.addClass(this.document.body, 'sqtm-core-prevent-selection');
  }

  enableTextSelection() {
    this.renderer.removeClass(this.document.body, 'sqtm-core-prevent-selection');
  }
}
