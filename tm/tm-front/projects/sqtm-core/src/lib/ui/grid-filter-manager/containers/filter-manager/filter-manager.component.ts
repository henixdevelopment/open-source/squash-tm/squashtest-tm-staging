import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnDestroy,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { GridService } from '../../../grid/services/grid.service';
import { Observable, Subject } from 'rxjs';
import { ColumnWithFilter } from '../../../grid/model/column-display.model';
import { GridFilter, selectFilterData } from '../../../grid/model/state/filter.state';
import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { SelectFiltersComponent } from '../../../grid/components/select-filters/select-filters.component';
import { take, takeUntil } from 'rxjs/operators';
import { select } from '@ngrx/store';
import { FilterGroup, FilteringChange } from '../../../filters/state/filter.state';
import { Scope } from '../../../../model/filter/filter.model';
import { DEFAULT_EMPTY_GRID_MESSAGE } from '../../../grid/model/state/ui.state';
import { ReferentialDataService } from '../../../../core/referential/services/referential-data.service';

@Component({
  selector: 'sqtm-core-filter-manager',
  templateUrl: 'filter-manager.component.html',
  styleUrls: ['./filter-manager.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterManagerComponent implements OnDestroy {
  @Input()
  showExtendedScopeOptionInFilterPanel: boolean;

  @ViewChild('addFilter', { read: ElementRef })
  addFilterRef: ElementRef;

  private unsub$ = new Subject<void>();
  private overlayRef: OverlayRef;
  openSelectFilterMenu = false;
  filterData$: Observable<{
    filters: GridFilter[];
    scope: Scope;
    filterGroups: FilterGroup[];
    extendedHighLvlReqScope: boolean;
  }>;

  constructor(
    public grid: GridService,
    private overlay: Overlay,
    private vcr: ViewContainerRef,
    private cdRef: ChangeDetectorRef,
    private referentialDataService: ReferentialDataService,
  ) {
    this.filterData$ = this.grid.gridState$.pipe(takeUntil(this.unsub$), select(selectFilterData));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  trackByColumnId(column: ColumnWithFilter) {
    return column.id;
  }

  validateFilters() {
    this.grid.refreshData();
    this.grid.setEmptyGridMessage(DEFAULT_EMPTY_GRID_MESSAGE);
  }

  changeFilterValue(change: FilteringChange, filter: GridFilter) {
    this.referentialDataService.referentialData$.pipe(take(1)).subscribe((refData) => {
      if (!refData.globalConfigurationState.searchActivationFeatureEnabled) {
        this.grid.changeFilterValue({ id: filter.id, ...change });
      } else {
        this.grid.changeFilterValueWithoutRefresh({ id: filter.id, ...change });
      }
    });
  }

  showAvailableFilters() {
    this.openSelectFilterMenu = true;
    this.cdRef.detectChanges();
    const positionStrategy = this.overlay
      .position()
      .flexibleConnectedTo(this.addFilterRef)
      .withPositions([
        {
          originX: 'center',
          overlayX: 'center',
          originY: 'bottom',
          overlayY: 'top',
          offsetY: 10,
        },
        {
          originX: 'center',
          overlayX: 'center',
          originY: 'top',
          overlayY: 'bottom',
          offsetY: -10,
        },
      ]);
    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
    };
    this.overlayRef = this.overlay.create(overlayConfig);
    const componentPortal = new ComponentPortal(SelectFiltersComponent, this.vcr);
    const componentComponentRef = this.overlayRef.attach(componentPortal);
    componentComponentRef.instance.toggleFilter$
      .pipe(takeUntil(this.unsub$))
      .subscribe(() => this.close());
    this.overlayRef.backdropClick().subscribe(() => this.close());
  }

  close() {
    if (this.overlayRef) {
      this.openSelectFilterMenu = false;
      this.overlayRef.dispose();
      this.cdRef.detectChanges();
    }
  }

  inactivateFilter(id: string) {
    this.referentialDataService.referentialData$.pipe(take(1)).subscribe((refData) => {
      if (!refData.globalConfigurationState.searchActivationFeatureEnabled) {
        this.grid.inactivateFilter(id);
      } else {
        this.grid.inactivateFilterWithoutRefresh(id);
      }
    });
  }

  resetFilters() {
    this.referentialDataService.referentialData$.pipe(take(1)).subscribe((refData) => {
      if (!refData.globalConfigurationState.searchActivationFeatureEnabled) {
        this.grid.resetFilters();
      } else {
        this.grid.resetFiltersWithoutRefresh();
      }
    });
  }

  changeScope($event: Scope) {
    this.referentialDataService.referentialData$.pipe(take(1)).subscribe((refData) => {
      if (!refData.globalConfigurationState.searchActivationFeatureEnabled) {
        this.grid.changeScope($event);
      } else {
        this.grid.changeScopeWithoutRefresh($event);
      }
    });
  }

  changeExtendedScope() {
    this.referentialDataService.referentialData$.pipe(take(1)).subscribe((refData) => {
      if (!refData.globalConfigurationState.searchActivationFeatureEnabled) {
        this.grid.changeExtendedScope();
      } else {
        this.grid.changeExtendedScopeWithoutRefresh();
      }
    });
  }
}
