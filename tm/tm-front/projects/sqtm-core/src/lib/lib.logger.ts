import { LoggerFactory } from './logger/logger.factory';

export const sqtmCoreLogger = LoggerFactory.getLogger('sqtm-core');
