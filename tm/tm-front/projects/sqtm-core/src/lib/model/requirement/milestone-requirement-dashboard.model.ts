import { RequirementStatistics } from './requirement-statistics.model';
import { CustomDashboardModel } from '../custom-report/custom-dashboard.model';

export interface MilestoneRequirementDashboard {
  statistics: RequirementStatistics;
  dashboard: CustomDashboardModel;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
}
