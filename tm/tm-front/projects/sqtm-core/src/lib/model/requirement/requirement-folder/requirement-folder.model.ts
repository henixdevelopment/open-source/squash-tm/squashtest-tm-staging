import { RequirementStatistics } from '../requirement-statistics.model';
import { CustomDashboardModel } from '../../custom-report/custom-dashboard.model';
import { EntityModel } from '../../entity/entity.model';

export interface RequirementFolderModel extends EntityModel {
  name: string;
  description: string;
  statistics: RequirementStatistics;
  dashboard: CustomDashboardModel;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
}
