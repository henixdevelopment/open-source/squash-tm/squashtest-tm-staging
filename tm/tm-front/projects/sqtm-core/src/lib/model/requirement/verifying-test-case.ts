import {
  ExecutionStatusKeys,
  TestCaseImportanceKeys,
  TestCaseStatusKeys,
} from '../level-enums/level-enum';

export interface VerifyingTestCase {
  id: number;
  name: string;
  projectName: string;
  reference: string;
  milestoneLabels: string;
  milestoneMinDate: Date;
  milestoneMaxDate: Date;
  status: TestCaseStatusKeys;
  importance: TestCaseImportanceKeys;
  lastExecutionStatus: ExecutionStatusKeys;
  directlyLinked: boolean;
}
