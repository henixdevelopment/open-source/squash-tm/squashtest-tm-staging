import { Identifier } from '../entity.model';

export interface RequirementSearchMilestoneMassEdit {
  milestoneIds: number[];
  checkedIds: number[];
  samePerimeter: boolean;
  requirementVersionIds: Identifier[];
}
