export interface RequirementVersionLink {
  projectName: string;
  reference: string;
  name: string;
  id: number;
  milestoneLabels: string;
  milestoneMinDate: Date;
  milestoneMaxDate: Date;
  versionNumber: number;
  role: string;
}
