export class RequirementImportLog {
  requirementVersionSuccesses: number;
  requirementVersionWarnings: number;
  requirementVersionFailures: number;
  coverageSuccesses: number;
  coverageWarnings: number;
  coverageFailures: number;
  linkedLowLevelReqSuccesses: number;
  linkedLowLevelReqWarnings: number;
  linkedLowLevelReqFailures: number;
  reqlinksSuccesses: number;
  reqlinksWarnings: number;
  reqlinksFailures: number;
  reportUrl: string;
}

export interface RequirementImportFormatFailure {
  missingMandatoryColumns: string[];
  duplicateColumns: string[];
  actionValidationError: any;
}

export class RequirementXlsReport {
  templateOk: RequirementImportLog;
  importFormatFailure: RequirementImportFormatFailure;
}
