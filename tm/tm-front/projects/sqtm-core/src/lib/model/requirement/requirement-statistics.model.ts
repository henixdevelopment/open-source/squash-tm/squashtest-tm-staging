export interface RequirementStatistics {
  boundTestCasesStatistics: RequirementBoundTestCasesStatistics;
  statusesStatistics: RequirementStatusesStatistics;
  criticalityStatistics: RequirementCriticalityStatistics;
  boundDescriptionStatistics: RequirementBoundDescriptionStatistics;
  coverageStatistics: RequirementCoverageStatistics;
  validationStatistics: RequirementValidationStatistics;
  selectedIds: number[];
  generatedOn?: Date;
}

export interface RequirementBoundTestCasesStatistics {
  zeroTestCases: number;
  oneTestCase: number;
  manyTestCases: number;
}

export interface RequirementStatusesStatistics {
  workInProgress: number;
  underReview: number;
  approved: number;
  obsolete: number;
}

export interface RequirementCriticalityStatistics {
  undefined: number;
  minor: number;
  major: number;
  critical: number;
}

export interface RequirementBoundDescriptionStatistics {
  hasDescription: number;
  hasNoDescription: number;
}

export interface RequirementCoverageStatistics {
  undefined: number;
  minor: number;
  major: number;
  critical: number;
  totalUndefined: number;
  totalMinor: number;
  totalMajor: number;
  totalCritical: number;
}

export interface RequirementValidationStatistics {
  conclusiveUndefined: number;
  conclusiveMinor: number;
  conclusiveMajor: number;
  conclusiveCritical: number;

  inconclusiveUndefined: number;
  inconclusiveMajor: number;
  inconclusiveMinor: number;
  inconclusiveCritical: number;

  undefinedUndefined: number;
  undefinedMajor: number;
  undefinedMinor: number;
  undefinedCritical: number;

  legacy: boolean;
}
