export interface PivotFormatImport {
  id: number;
  name: string;
  type: ImportType;
  createdBy: string;
  createdOn: string;
  successfullyImportedOn: string;
  status: PivotFormatImportStatus;
}

export enum PivotFormatImportStatus {
  PENDING = 'PENDING',
  RUNNING = 'RUNNING',
  SUCCESS = 'SUCCESS',
  WARNING = 'WARNING',
  FAILURE = 'FAILURE',
}

export enum ImportType {
  XRAY = 'XRAY',
  STANDARD = 'STANDARD',
}
