/** Maps to GenericProjectCopyParameter server-side */
export interface GenericProjectCopyParameter {
  keepTemplateBinding: boolean;
  copyPermissions: boolean;
  copyCUF: boolean;
  copyBugtrackerBinding: boolean;
  copyAiServerBinding: boolean;
  copyAutomatedProjects: boolean;
  copyInfolists: boolean;
  copyMilestone: boolean;
  copyAllowTcModifFromExec: boolean;
  copyOptionalExecStatuses: boolean;
  copyPluginsActivation: boolean;
}

/** Model used when creating a new project, maps to JsonProjectFromTemplate server-side */
export type JsonProjectFromTemplate = {
  name: string;
  label: string;
  description: string;
  templateId: number;
  fromTemplate: boolean;
  keepPluginsBinding: boolean;
  copyPluginsConfiguration: boolean;
} & GenericProjectCopyParameter; // server-side JsonProjectFromTemplate wraps each parameters getter/setter
