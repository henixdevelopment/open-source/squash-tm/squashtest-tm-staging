import { Option } from '../option.model';
import { ProjectPermissions } from '../permissions/permissions.model';
import { BugTrackerBinding } from '../bugtracker/bug-tracker.binding';
import { MilestoneBinding } from '../milestone/milestone-binding.model';
import { Bindings } from '../bindable-entity.model';
import {
  BddImplementationTechnologyKeys,
  BddScriptLanguageKeys,
  ExecutionStatusKeys,
} from '../level-enums/level-enum';
import { AttachmentListModel } from '../attachment/attachment-list.model';
import { BugTracker } from '../bugtracker/bug-tracker.model';
import { InfoList } from '../infolist/infolist.model';
import { ScmServer } from '../scm-server/scm-server.model';
import { TestAutomationServer } from '../test-automation/test-automation-server.model';
import { TestAutomationProject } from '../test-automation/test-automation-project.model';
import { MilestoneAdminProjectView } from '../milestone/milestone-admin-project-view.model';
import { ProjectPlugin } from '../plugin/project-plugin';
import { ActivatedPluginsByWorkspace } from './project-data.model';
import { NamedReference } from '../named-reference';
import { AdminEntityModel } from '../entity/admin-entity.model';
import { AiServer } from '../artificial-intelligence/ai-server.model';
import { Profile } from '../profile/profile.model';
import { PivotFormatImport } from './pivot-format-import.model';

export class Project implements AdminEntityModel, InfoListInProject {
  id: number;
  uri: string;
  name: string;
  label: string;
  testCaseNatureId: number;
  testCaseTypeId: number;
  requirementCategoryId: number;
  allowAutomationWorkflow: boolean;
  customFieldBindings: Bindings;
  permissions: ProjectPermissions;
  bugTrackerBinding: BugTrackerBinding;
  milestoneBindings: MilestoneBinding[];
  taServerId: number;
  automationWorkflowType: string;
  disabledExecutionStatus: ExecutionStatusKeys[];
  attachmentList: AttachmentListModel;
  createdOn: string;
  createdBy: string;
  lastModifiedOn: string;
  lastModifiedBy: string;
  linkedTemplate: string;
  linkedTemplateId: number;
  template: boolean;
  description: string;
  bddScriptLanguage: string;
  keywords: Option[];
  allowTcModifDuringExec: boolean;
  activatedPlugins: ActivatedPluginsByWorkspace;
  automatedSuitesLifetime: number;
  hasTemplateConfigurablePluginBinding: boolean;
  scmRepositoryId?: number;
  aiServerId: number;
}

export class ProjectView extends Project {
  availableBugtrackers: BugTracker[];
  bugtrackerProjectNames: string[];
  hasData: boolean;
  allowedStatuses: AllowedStatusesModel;
  statusesInUse: StatusesInUseModel;
  useTreeStructureInScmRepo: boolean;
  infoLists: InfoList[];
  templateLinkedToProjects: boolean;
  partyProjectPermissions: PartyProjectPermission[];
  availableScmServers: ScmServer[];
  availableTestAutomationServers: TestAutomationServer[];
  availableAiServers: AiServer[];
  boundMilestonesInformation: MilestoneAdminProjectView[];
  boundTestAutomationProjects: TestAutomationProject[];
  availablePlugins: ProjectPlugin[];
  bddScriptLanguage: BddScriptLanguageKeys;
  bddImplementationTechnology: BddImplementationTechnologyKeys;
  allProjectBoundToTemplate: NamedReference[];
  availableProfiles: Profile[];
  existingImports: PivotFormatImport[];
}

export interface AllowedStatusesModel {
  UNTESTABLE: boolean;
  SETTLED: boolean;
}

export interface StatusesInUseModel {
  UNTESTABLE: boolean;
  SETTLED: boolean;
}

export interface PartyProjectPermission {
  projectId: number;
  partyName: string;
  partyId: number;
  active: boolean;
  permissionGroup: Profile;
  team: boolean;
}

export type InfoListAttribute = 'testCaseNatureId' | 'testCaseTypeId' | 'requirementCategoryId';

export type InfoListInProject = { [K in InfoListAttribute]: number };
