export interface XrayInfoModel {
  entities: {
    [key: string]: XrayEntity;
  };
}

export interface XrayEntity {
  type: string;
  entityCount: number;
  status: string[];
  priorities: string[];
}

export interface XrayImportModel {
  pivotFileName: string;
  pivotLogFileName: string;
  entityCounts: EntityCount[];
}

export interface EntityCount {
  i18nKey: string;
  entityType: string;
  entitySuccesses: number;
  entityWarnings: number;
  entityFailures: number;
}
