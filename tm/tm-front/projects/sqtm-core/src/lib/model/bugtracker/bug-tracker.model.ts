import {
  AuthenticationPolicy,
  AuthenticationProtocol,
} from '../third-party-server/authentication.model';

export interface BugTracker {
  id: number;
  name: string;
  url: string;
  kind: string;
  authPolicy: AuthenticationPolicy;
  authProtocol: AuthenticationProtocol;
  iframeFriendly: boolean;
}

export interface BugTrackerReferentialDto extends BugTracker {
  useProjectPaths: boolean;
  projectHelpMessage: string | null;

  loginFieldKey?: string;
  passwordFieldKey?: string;
  cacheAllowed?: boolean;
  cacheConfigured?: boolean;
}

export type BugTrackerMode = 'Automatic' | 'Manual';
