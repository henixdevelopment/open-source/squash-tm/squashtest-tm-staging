import { Credentials } from '../third-party-server/credentials.model';
import { AuthConfiguration } from '../third-party-server/auth-configuration.model';
import {
  AuthenticationPolicy,
  AuthenticationProtocol,
} from '../third-party-server/authentication.model';

export interface AdminBugTrackerModel {
  id: number;
  name: string;
  url: string;
  kind: string;
  authPolicy: AuthenticationPolicy;
  authProtocol: AuthenticationProtocol;
  iframeFriendly: boolean;
  bugTrackerKinds: string[];
  supportedAuthenticationProtocols: AuthenticationProtocol[];
  authConfiguration?: AuthConfiguration;
  credentials?: Credentials;
  createdBy: string;
  createdOn: string;
  lastModifiedBy?: string;
  lastModifiedOn?: string;
  description?: string;
  allowsReportingCache?: boolean;
  reportingCacheCredentials?: Credentials;
  hasCacheCredentialsError?: boolean;
  cacheInfo?: BugTrackerCacheInfo;
}

export interface BugTrackerCacheInfo {
  entries: BugTrackerCacheInfoEntry[];
}

export interface BugTrackerCacheInfoEntry {
  bugTrackerId: number;
  projectNameOrPath: string;
  lastUpdate: string;
  lastSuccessfulUpdate: string;
  hasError: boolean;
}
