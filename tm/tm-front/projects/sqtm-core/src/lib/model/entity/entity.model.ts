// The entity MODEL represent the shape of the data server side.
// Ex for a TestCase you will have all TestCase Attributes, the Steps, the Coverages...
import { AttachmentListModel } from '../attachment/attachment-list.model';
import { CustomFieldValueModel } from '../customfield/custom-field-value.model';

export interface EntityModel {
  id: number;
  projectId: number;
  attachmentList: AttachmentListModel;
  customFieldValues: CustomFieldValueModel[];
}
