import { AttachmentListModel } from '../attachment/attachment-list.model';

export interface AdminEntityModel {
  id: number;
  attachmentList: AttachmentListModel;
}
