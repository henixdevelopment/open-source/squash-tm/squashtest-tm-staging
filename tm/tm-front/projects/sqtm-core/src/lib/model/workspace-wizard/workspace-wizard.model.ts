import { Permissions } from '../permissions/permissions.model';
import { WorkspaceTypeForPlugins } from '../project/project-data.model';

export interface WorkspaceWizard {
  id: string;
  wizardMenu?: WizardMenu;
  displayWorkspace: WorkspaceTypeForPlugins;
}

export interface WizardMenu {
  tooltip: string;
  url: string;
  label: string;
  accessRule: AccessRule;
}

export interface AccessRule {}

interface NodeSelection extends AccessRule {
  selectionMode: SelectionMode;
  rules: AccessRule[];
}

export enum SelectionMode {
  SINGLE_SELECTION = 'SINGLE_SELECTION',
  MULTIPLE_SELECTION = 'MULTIPLE_SELECTION',
}

export interface SelectedNodePermission extends AccessRule {
  nodeType: TreeNodeType;
  permission: Permissions;
}

export enum TreeNodeType {
  LIBRARY = 'LIBRARY',
  FOLDER = 'FOLDER',
  CAMPAIGN = 'CAMPAIGN',
  TEST_CASE = 'TEST_CASE',
  REQUIREMENT = 'REQUIREMENT',
  ITERATION = 'ITERATION',
}

export function isNodeSelectionAccessRule(accessRule: AccessRule): accessRule is NodeSelection {
  return (
    Object.prototype.hasOwnProperty.call(accessRule, 'selectionMode') &&
    Object.values(SelectionMode).includes(accessRule['selectionMode']) &&
    Object.prototype.hasOwnProperty.call(accessRule, 'rules') &&
    Array.isArray(accessRule['rules'])
  );
}

export function isSelectedNodePermissionAccessRule(
  accessRule: AccessRule,
): accessRule is SelectedNodePermission {
  return (
    Object.prototype.hasOwnProperty.call(accessRule, 'nodeType') &&
    typeof accessRule['nodeType'] === 'string' &&
    Object.prototype.hasOwnProperty.call(accessRule, 'permission') &&
    Object.values(Permissions).includes(accessRule['permission'])
  );
}
