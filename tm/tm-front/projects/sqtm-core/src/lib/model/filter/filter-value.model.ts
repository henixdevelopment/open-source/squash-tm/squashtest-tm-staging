// Model used server side mainly for the research controllers.
// Other backend controllers will have to use the same data shape.
export interface FilterValueModel {
  operation?: string;
  columnPrototype?: string;
  values: string[];
  id: string;
  cufId?: number;
}

export interface DiscreteFilterValue {
  id: string | number;
  label?: string;
  i18nLabelKey?: string;
}

export type FilterValueKind =
  | 'single-string-value'
  | 'single-numeric-value'
  | 'single-date-value'
  | 'multiple-string-value'
  | 'multiple-date-value'
  | 'multiple-discrete-value'
  | 'multiple-numeric-value'
  | 'multiple-boolean-value';

export abstract class FilterValue {
  readonly kind: FilterValueKind;
  value: string | string[] | number | DiscreteFilterValue[] | number[] | boolean[];
}
