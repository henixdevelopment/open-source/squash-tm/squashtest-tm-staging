import { Identifier } from '../entity.model';
import { FilterValue } from './filter-value.model';

export enum FilterOperation {
  EQUALS = 'EQUALS',
  LIKE = 'LIKE',
  IN = 'IN',
  AND = 'AND',
  GREATER = 'GREATER',
  GREATER_EQUAL = 'GREATER_EQUAL',
  LOWER = 'LOWER',
  LOWER_EQUAL = 'LOWER_EQUAL',
  BETWEEN = 'BETWEEN',
  NONE = 'NONE',
  AT_LEAST_ONE = 'AT_LEAST_ONE',
  MATCHES = 'MATCHES',
  MIN = 'MIN',
  MAX = 'MAX',
  AVG = 'AVG',
  SUM = 'SUM',
  COUNT = 'COUNT',
  IS_NULL = 'IS_NULL',
  NOT_NULL = 'NOT_NULL',
  NOT_EQUALS = 'NOT_EQUALS',
  BY_DAY = 'BY_DAY',
  BY_WEEK = 'BY_WEEK',
  BY_MONTH = 'BY_MONTH',
  BY_YEAR = 'BY_YEAR',
  FULLTEXT = 'FULLTEXT',
}

export interface EntityScope {
  id: string;
  label: string;
  projectId: number;
}

export type ScopeKind = 'project' | 'custom';

export interface Scope {
  value: EntityScope[];
  initialValue: EntityScope[];
  initialKind: ScopeKind;
  kind: ScopeKind;
  active: boolean;
}

export type SimpleScope = Pick<Scope, 'kind' | 'value'>;

export interface SimpleFilter {
  id: Identifier;
  // Value can be null
  value?: FilterValue;
  // Current selected operation.
  operation?: FilterOperation;
}
