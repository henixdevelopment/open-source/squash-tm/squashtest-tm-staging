import { CustomDashboardModel } from '../custom-report/custom-dashboard.model';

export interface HomeWorkspaceModel {
  welcomeMessage: string;
  dashboard: CustomDashboardModel;
}
