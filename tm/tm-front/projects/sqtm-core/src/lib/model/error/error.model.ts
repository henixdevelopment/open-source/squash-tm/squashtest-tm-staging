export type ErrorKind = 'ACTION_ERROR' | 'FIELD_VALIDATION_ERROR';

export interface SquashError {
  kind: ErrorKind;
}

export interface SquashActionError extends SquashError {
  kind: 'ACTION_ERROR';
  actionValidationError: ActionValidationError;
}

export interface SquashFieldError extends SquashError {
  kind: 'FIELD_VALIDATION_ERROR';
  fieldValidationErrors: FieldValidationError[];
}

export interface ActionValidationError {
  exception: string;
  i18nKey: string;
  i18nParams: string[];
}

export interface FieldValidationError {
  objectName: string;
  fieldName: string;
  fieldValue: string;
  fieldValues?: string[];
  i18nKey: string;
  messageArgs: string[];
}
