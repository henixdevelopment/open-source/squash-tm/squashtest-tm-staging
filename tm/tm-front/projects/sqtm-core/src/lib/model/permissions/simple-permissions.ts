import { ProjectData } from '../project/project-data.model';
import { AclClass, Permissions, ProjectPermissions } from './permissions.model';

export interface BasePermissions {
  get canRead(): boolean;
  get canWrite(): boolean;
  get canAttach(): boolean;
  get canCreate(): boolean;
  get canDelete(): boolean;
  get canExtendedDelete(): boolean;
  get canImport(): boolean;
  get canExport(): boolean;
}

export type SimplePermissions =
  | ReadOnlyPermissions
  | TestCasePermissions
  | RequirementPermissions
  | CampaignPermissions;

abstract class AbstractPermissions implements BasePermissions {
  public abstract get canRead(): boolean;

  public abstract get canWrite(): boolean;

  public abstract get canAttach(): boolean;

  public abstract get canCreate(): boolean;

  public abstract get canDelete(): boolean;

  public abstract get canExtendedDelete(): boolean;

  public abstract get canImport(): boolean;

  public abstract get canExport(): boolean;

  protected abstract get permissions(): Permissions[];

  protected abstract checkPermission(permission: Permissions);
}

abstract class AbstractProjectPermissions extends AbstractPermissions {
  protected readonly kind: AclClass;
  private readonly _projectPermissions: ProjectPermissions;

  protected constructor(projectData: ProjectData) {
    super();
    this._projectPermissions = projectData.permissions;
  }

  public get canRead(): boolean {
    return this.checkPermission(Permissions.READ);
  }

  public get canWrite(): boolean {
    return this.checkPermission(Permissions.WRITE);
  }

  public get canWriteAsFunctional(): boolean {
    return this.checkPermission(Permissions.WRITE_AS_FUNCTIONAL);
  }

  public get canWriteAsAutomation(): boolean {
    return this.checkPermission(Permissions.WRITE_AS_AUTOMATION);
  }

  public get canAttach(): boolean {
    return this.checkPermission(Permissions.ATTACH);
  }

  public get canCreate(): boolean {
    return this.checkPermission(Permissions.CREATE);
  }

  public get canDelete(): boolean {
    return this.checkPermission(Permissions.DELETE);
  }

  public get canExtendedDelete(): boolean {
    return this.checkPermission(Permissions.EXTENDED_DELETE);
  }

  public get canLink(): boolean {
    return this.checkPermission(Permissions.LINK);
  }

  public get canImport(): boolean {
    return this.checkPermission(Permissions.IMPORT);
  }

  public get canExport(): boolean {
    return this.checkPermission(Permissions.EXPORT);
  }

  protected checkPermission(permission: Permissions) {
    return this.permissions.includes(permission);
  }

  protected get permissions(): Permissions[] {
    return this._projectPermissions[this.kind];
  }
}

export class ReadOnlyPermissions implements BasePermissions {
  public get canRead(): boolean {
    return true;
  }

  public get canWrite(): boolean {
    return false;
  }

  public get canAttach(): boolean {
    return false;
  }

  public get canCreate(): boolean {
    return false;
  }

  public get canDelete(): boolean {
    return false;
  }

  public get canLink(): boolean {
    return false;
  }

  public get canExtendedDelete(): boolean {
    return false;
  }

  public get canExport(): boolean {
    return false;
  }

  public get canImport(): boolean {
    return false;
  }
}

export class TestCasePermissions extends AbstractProjectPermissions {
  protected kind = 'TEST_CASE_LIBRARY' as const;

  constructor(projectData: ProjectData) {
    super(projectData);
  }

  public get canLink(): boolean {
    return this.checkPermission(Permissions.LINK);
  }
}

export class RequirementPermissions extends AbstractProjectPermissions {
  protected kind = 'REQUIREMENT_LIBRARY' as const;

  constructor(projectData: ProjectData) {
    super(projectData);
  }

  public get canLink(): boolean {
    return this.checkPermission(Permissions.LINK);
  }
}

export class CampaignPermissions extends AbstractProjectPermissions {
  protected kind = 'CAMPAIGN_LIBRARY' as const;

  constructor(projectData: ProjectData) {
    super(projectData);
  }

  public get canLink(): boolean {
    return this.checkPermission(Permissions.LINK);
  }

  public get canExecute(): boolean {
    return this.checkPermission(Permissions.EXECUTE);
  }

  public get canDeleteExecution(): boolean {
    return this.checkPermission(Permissions.DELETE_EXECUTION);
  }
}

export class CustomReportPermissions extends AbstractProjectPermissions {
  protected kind = 'CUSTOM_REPORT_LIBRARY' as const;

  constructor(projectData: ProjectData) {
    super(projectData);
  }
}

export class AutomationRequestPermissions extends AbstractProjectPermissions {
  protected kind = 'AUTOMATION_REQUEST_LIBRARY' as const;

  constructor(projectData: ProjectData) {
    super(projectData);
  }

  public get canWriteAsAutomation(): boolean {
    return this.checkPermission(Permissions.WRITE_AS_AUTOMATION);
  }

  public get canWriteAsFunctional(): boolean {
    return this.checkPermission(Permissions.WRITE_AS_FUNCTIONAL);
  }
}

export class ActionWordPermissions extends AbstractProjectPermissions {
  protected kind = 'ACTION_WORD_LIBRARY' as const;

  constructor(projectData: ProjectData) {
    super(projectData);
  }
}
