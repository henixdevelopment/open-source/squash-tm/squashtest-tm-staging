export type AclClass =
  | 'PROJECT'
  | 'PROJECT_TEMPLATE'
  | 'REQUIREMENT_LIBRARY'
  | 'TEST_CASE_LIBRARY'
  | 'CAMPAIGN_LIBRARY'
  | 'CUSTOM_REPORT_LIBRARY'
  | 'AUTOMATION_REQUEST_LIBRARY'
  | 'ACTION_WORD_LIBRARY';

export enum Permissions {
  READ = 'READ',
  WRITE = 'WRITE',
  CREATE = 'CREATE',
  DELETE = 'DELETE',
  ADMIN = 'ADMIN',
  MANAGE_PROJECT = 'MANAGE_PROJECT',
  EXPORT = 'EXPORT',
  EXECUTE = 'EXECUTE',
  LINK = 'LINK',
  IMPORT = 'IMPORT',
  ATTACH = 'ATTACH',
  EXTENDED_DELETE = 'EXTENDED_DELETE',
  READ_UNASSIGNED = 'READ_UNASSIGNED',
  WRITE_AS_FUNCTIONAL = 'WRITE_AS_FUNCTIONAL',
  WRITE_AS_AUTOMATION = 'WRITE_AS_AUTOMATION',
  MANAGE_MILESTONE = 'MANAGE_MILESTONE',
  MANAGE_PROJECT_CLEARANCE = 'MANAGE_PROJECT_CLEARANCE',
  DELETE_EXECUTION = 'DELETE_EXECUTION',
}

export interface ProjectPermissions {
  PROJECT: (
    | Permissions.READ
    | Permissions.WRITE
    | Permissions.DELETE
    | Permissions.ADMIN
    | Permissions.IMPORT
    | Permissions.MANAGE_PROJECT
    | Permissions.MANAGE_MILESTONE
    | Permissions.MANAGE_PROJECT_CLEARANCE
    | Permissions.ATTACH
  )[];
  PROJECT_TEMPLATE: (
    | Permissions.READ
    | Permissions.WRITE
    | Permissions.DELETE
    | Permissions.ADMIN
    | Permissions.IMPORT
    | Permissions.MANAGE_PROJECT
    | Permissions.MANAGE_MILESTONE
    | Permissions.MANAGE_PROJECT_CLEARANCE
    | Permissions.ATTACH
  )[];
  REQUIREMENT_LIBRARY: (
    | Permissions.READ
    | Permissions.WRITE
    | Permissions.CREATE
    | Permissions.DELETE
    | Permissions.EXPORT
    | Permissions.ATTACH
    | Permissions.LINK
    | Permissions.IMPORT
  )[];
  TEST_CASE_LIBRARY: (
    | Permissions.READ
    | Permissions.WRITE
    | Permissions.CREATE
    | Permissions.DELETE
    | Permissions.ATTACH
    | Permissions.EXPORT
    | Permissions.LINK
  )[];
  CAMPAIGN_LIBRARY: (
    | Permissions.READ
    | Permissions.WRITE
    | Permissions.CREATE
    | Permissions.DELETE
    | Permissions.ATTACH
    | Permissions.EXPORT
    | Permissions.EXECUTE
    | Permissions.LINK
    | Permissions.EXTENDED_DELETE
    | Permissions.READ_UNASSIGNED
    | Permissions.DELETE_EXECUTION
  )[];
  CUSTOM_REPORT_LIBRARY: (
    | Permissions.READ
    | Permissions.WRITE
    | Permissions.CREATE
    | Permissions.DELETE
    | Permissions.ATTACH
    | Permissions.EXPORT
  )[];
  AUTOMATION_REQUEST_LIBRARY: (
    | Permissions.READ
    | Permissions.WRITE
    | Permissions.CREATE
    | Permissions.DELETE
    | Permissions.ATTACH
    | Permissions.WRITE_AS_FUNCTIONAL
    | Permissions.WRITE_AS_AUTOMATION
  )[];
  ACTION_WORD_LIBRARY: (
    | Permissions.READ
    | Permissions.WRITE
    | Permissions.CREATE
    | Permissions.DELETE
    | Permissions.ATTACH
  )[];
}

export enum PermissionWorkspaceType {
  PROJECT = 'PROJECT',
  REQUIREMENT_LIBRARY = 'REQUIREMENT_LIBRARY',
  TEST_CASE_LIBRARY = 'TEST_CASE_LIBRARY',
  CAMPAIGN_LIBRARY = 'CAMPAIGN_LIBRARY',
  CUSTOM_REPORT_LIBRARY = 'CUSTOM_REPORT_LIBRARY',
  AUTOMATION_REQUEST_LIBRARY = 'AUTOMATION_REQUEST_LIBRARY',
  ACTION_WORD_LIBRARY = 'ACTION_WORD_LIBRARY',
}

export const permissionWorkspaceEntityReferences = {
  [PermissionWorkspaceType.PROJECT]: `administration`,
  [PermissionWorkspaceType.REQUIREMENT_LIBRARY]: `requirement`,
  [PermissionWorkspaceType.TEST_CASE_LIBRARY]: `test-case`,
  [PermissionWorkspaceType.CAMPAIGN_LIBRARY]: `campaign`,
  [PermissionWorkspaceType.CUSTOM_REPORT_LIBRARY]: `custom-report`,
  [PermissionWorkspaceType.ACTION_WORD_LIBRARY]: `action-word`,
  [PermissionWorkspaceType.AUTOMATION_REQUEST_LIBRARY]: `test-case`,
};

export interface PermissionWithSubPermissions {
  mainPermission: Permissions;
  subPermissions: Permissions[];
}

export interface WorkspacePermissions {
  workspace: PermissionWorkspaceType;
  permissions: PermissionWithSubPermissions[];
}

export const workspacePermissionsList: WorkspacePermissions[] = [
  {
    workspace: PermissionWorkspaceType.PROJECT,
    permissions: [
      {
        mainPermission: Permissions.MANAGE_PROJECT,
        subPermissions: [
          Permissions.MANAGE_PROJECT_CLEARANCE,
          Permissions.MANAGE_MILESTONE,
          Permissions.IMPORT,
        ],
      },
    ],
  },
  {
    workspace: PermissionWorkspaceType.REQUIREMENT_LIBRARY,
    permissions: [
      {
        mainPermission: Permissions.READ,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.CREATE,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.WRITE,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.DELETE,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.LINK,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.EXPORT,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.IMPORT,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.ATTACH,
        subPermissions: [],
      },
    ],
  },
  {
    workspace: PermissionWorkspaceType.TEST_CASE_LIBRARY,
    permissions: [
      {
        mainPermission: Permissions.READ,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.CREATE,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.WRITE,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.DELETE,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.LINK,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.EXPORT,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.IMPORT,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.ATTACH,
        subPermissions: [],
      },
    ],
  },
  {
    workspace: PermissionWorkspaceType.CAMPAIGN_LIBRARY,
    permissions: [
      {
        mainPermission: Permissions.READ,
        subPermissions: [Permissions.READ_UNASSIGNED],
      },
      {
        mainPermission: Permissions.CREATE,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.WRITE,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.DELETE,
        subPermissions: [Permissions.EXTENDED_DELETE],
      },
      {
        mainPermission: Permissions.DELETE_EXECUTION,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.LINK,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.EXECUTE,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.EXPORT,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.ATTACH,
        subPermissions: [],
      },
    ],
  },
  {
    workspace: PermissionWorkspaceType.CUSTOM_REPORT_LIBRARY,
    permissions: [
      {
        mainPermission: Permissions.READ,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.CREATE,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.WRITE,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.DELETE,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.EXPORT,
        subPermissions: [],
      },
    ],
  },
  {
    workspace: PermissionWorkspaceType.AUTOMATION_REQUEST_LIBRARY,
    permissions: [
      {
        mainPermission: Permissions.WRITE_AS_FUNCTIONAL,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.WRITE_AS_AUTOMATION,
        subPermissions: [],
      },
    ],
  },
  {
    workspace: PermissionWorkspaceType.ACTION_WORD_LIBRARY,
    permissions: [
      {
        mainPermission: Permissions.READ,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.CREATE,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.WRITE,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.DELETE,
        subPermissions: [],
      },
      {
        mainPermission: Permissions.ATTACH,
        subPermissions: [],
      },
    ],
  },
];

export const NO_PERMISSIONS: Readonly<ProjectPermissions> = {
  PROJECT: [],
  PROJECT_TEMPLATE: [],
  REQUIREMENT_LIBRARY: [],
  TEST_CASE_LIBRARY: [],
  CAMPAIGN_LIBRARY: [],
  CUSTOM_REPORT_LIBRARY: [],
  AUTOMATION_REQUEST_LIBRARY: [],
  ACTION_WORD_LIBRARY: [],
};

// All permissions by business domain.
// Mainly used to give permissions to admin and avoid the usual isAdmin everywhere that can cause weirds behavior
// as whe always develop in admin... aka an admin shouldn't have rights that doesn't exist for a given entity...
// Even a mighty Administrator cannot READ_UNASSIGNED on a Requirement. Sad panda...
export const ADMIN_PERMISSIONS: Readonly<ProjectPermissions> = {
  PROJECT: [
    Permissions.READ,
    Permissions.WRITE,
    Permissions.DELETE,
    Permissions.ADMIN,
    Permissions.IMPORT,
    Permissions.MANAGE_PROJECT,
    Permissions.MANAGE_MILESTONE,
    Permissions.MANAGE_PROJECT_CLEARANCE,
    Permissions.ATTACH,
  ],
  PROJECT_TEMPLATE: [
    Permissions.READ,
    Permissions.WRITE,
    Permissions.DELETE,
    Permissions.ADMIN,
    Permissions.IMPORT,
    Permissions.MANAGE_PROJECT,
    Permissions.MANAGE_MILESTONE,
    Permissions.MANAGE_PROJECT_CLEARANCE,
    Permissions.ATTACH,
  ],
  REQUIREMENT_LIBRARY: [
    Permissions.READ,
    Permissions.WRITE,
    Permissions.CREATE,
    Permissions.DELETE,
    Permissions.EXPORT,
    Permissions.ATTACH,
    Permissions.LINK,
    Permissions.IMPORT,
  ],
  TEST_CASE_LIBRARY: [
    Permissions.READ,
    Permissions.WRITE,
    Permissions.CREATE,
    Permissions.DELETE,
    Permissions.EXPORT,
    Permissions.ATTACH,
    Permissions.LINK,
  ],
  CAMPAIGN_LIBRARY: [
    Permissions.READ,
    Permissions.WRITE,
    Permissions.CREATE,
    Permissions.DELETE,
    Permissions.ATTACH,
    Permissions.EXPORT,
    Permissions.EXECUTE,
    Permissions.LINK,
    Permissions.EXTENDED_DELETE,
    Permissions.READ_UNASSIGNED,
    Permissions.DELETE_EXECUTION,
  ],
  CUSTOM_REPORT_LIBRARY: [
    Permissions.READ,
    Permissions.WRITE,
    Permissions.CREATE,
    Permissions.DELETE,
    Permissions.ATTACH,
    Permissions.EXPORT,
  ],
  AUTOMATION_REQUEST_LIBRARY: [
    Permissions.READ,
    Permissions.WRITE,
    Permissions.CREATE,
    Permissions.DELETE,
    Permissions.ATTACH,
    Permissions.WRITE_AS_AUTOMATION,
    Permissions.WRITE_AS_FUNCTIONAL,
  ],
  ACTION_WORD_LIBRARY: [
    Permissions.READ,
    Permissions.WRITE,
    Permissions.CREATE,
    Permissions.DELETE,
    Permissions.ATTACH,
  ],
};

export enum AclGroup {
  ADVANCE_TESTER = 'squashtest.acl.group.tm.AdvanceTester',
  AUTOMATED_TEST_WRITER = 'squashtest.acl.group.tm.AutomatedTestWriter',
  PROJECT_MANAGER = 'squashtest.acl.group.tm.ProjectManager',
  PROJECT_VIEWER = 'squashtest.acl.group.tm.ProjectViewer',
  TEST_DESIGNER = 'squashtest.acl.group.tm.TestDesigner',
  TEST_EDITOR = 'squashtest.acl.group.tm.TestEditor',
  TEST_RUNNER = 'squashtest.acl.group.tm.TestRunner',
  VALIDATOR = 'squashtest.acl.group.tm.Validator',
}

export function isDefaultSystemAclGroup(qualifiedName: string) {
  return Object.values(AclGroup).includes(qualifiedName as AclGroup);
}

export function getAclGroupI18nKey(group: AclGroup) {
  return `sqtm-core.entity.user.project-rights.${getProfileNameFromAclGroup(group)}.label`;
}

export function getAclGroupDescriptionI18nKey(group: AclGroup) {
  return `sqtm-core.entity.profile.${getProfileNameFromAclGroup(group)}.description`;
}

export function getProfileNameFromAclGroup(group: AclGroup) {
  const tokens = group.split('.');
  return tokens[tokens.length - 1];
}
