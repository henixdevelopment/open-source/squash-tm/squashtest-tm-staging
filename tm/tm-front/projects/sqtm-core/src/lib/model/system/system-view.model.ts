import { ReportTemplateModel } from './report-template.model';

export interface AdministrationStatistics {
  projectsNumber: number;
  usersNumber: number;
  requirementsNumber: number;
  testCasesNumber: number;
  campaignsNumber: number;
  iterationsNumber: number;
  executionsNumber: number;
  requirementIndexingDate: Date;
  testcaseIndexingDate: Date;
  campaignIndexingDate: Date;
  databaseSize: number;
}

export interface SystemViewModel {
  statistics: AdministrationStatistics;
  appVersion: string;
  plugins: string[];
  whiteList: string;
  uploadSizeLimit: string;
  importSizeLimit: string;
  callbackUrl: string;
  stackTracePanelIsVisible: boolean;
  stackTraceFeatureIsEnabled: boolean;
  autoconnectOnConnection: boolean;
  caseInsensitiveLogin: boolean;
  caseInsensitiveActions: boolean;
  duplicateLogins: string[];
  duplicateActions: string[];
  welcomeMessage: string;
  loginMessage: string;
  bannerMessage: string;
  logFiles: string[];
  licenseInfo: LicenseInfo;
  currentActiveUsersCount: number;
  reportTemplateModels: ReportTemplateModel[];
  synchronisationPlugins: SynchronizationViewPlugin[];
  unsafeAttachmentPreviewEnabled: boolean;
}

export interface SynchronizationViewPlugin {
  id: string;
  name: string;
  hasSupervisionScreen: boolean;
  supervisionScreenData: SupervisionScreenData;
  remoteSynchronisations: RemoteSynchronisation[];
}

export interface SupervisionScreenData {
  hasSyncNameColumn: boolean;
  hasPerimeterColumn: boolean;
  hasRemoteSelectTypeColumn: boolean;
  hasSynchronizedRequirementsRatioColumn: boolean;
  hasSynchronizedSprintTicketsRatioColumn: boolean;
}

export interface RemoteSynchronisation {
  id: number;
  name: string;
  serverId: number;
  serverName: string;
  projectId: number;
  projectName: string;
  perimeter: string;
  remoteSelectType: RemoteSelectType;
  remoteSelectValue: string;
  status: SynchronizationStatus;
  lastSyncDate: string;
  lastSuccessfulSyncDate: string;
  enabled: boolean;
  synchronizedRequirementsCount?: number;
  unprocessedRequirementsCount?: number;
  synchronizedSprintTicketsCount?: number;
  unprocessedSprintTicketsCount?: number;
  syncErrorLogFilePath: string;
}

export enum RemoteSelectType {
  ISSUE = 'ISSUE',
  BOARD = 'BOARD',
  QUERY = 'QUERY',
  FILTER = 'FILTER',
}

export enum SynchronizationStatus {
  FAILURE = 'FAILURE',
  RUNNING = 'RUNNING',
  SUCCESS = 'SUCCESS',
  NEVER_EXECUTED = 'NEVER_EXECUTED',
}

/*
 * <b>WARNING</b>: This enum should not be abused. In most cases you should not refer to plugins
 * in the core code. You should use the plugin API to interact with plugins.
 */
export enum SynchronizationPluginId {
  AUTOM_JIRA = 'henix.plugin.automation.workflow.automjira',
  XSQUASH4JIRA = 'squash.tm.plugin.jirasync',
  XSQUASH4GITLAB = 'squash.tm.plugin.xsquash4gitlab',
}

export interface LicenseInfo {
  expirationDate: Date;
  maxUsers: number;
}

export enum DocXReportId {
  REPORT_BOOKS_EDITABLE_REQ = 'report.books.requirements.requirements.report.label',
  REPORT_BOOKS_EDITABLE_TC = 'report.books.testcases.report.label',
  ITERATION_ASSESSMENT = 'report.iteration.report.label',
  CAMPAIGN_ASSESSMENT = 'report.campaignassessment.report.label',
}
