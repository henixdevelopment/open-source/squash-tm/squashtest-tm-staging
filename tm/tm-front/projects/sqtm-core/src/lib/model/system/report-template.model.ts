import { DocXReportId } from './system-view.model';

export interface ReportTemplateModel {
  reportId: DocXReportId;
  fileName: string;
}
