import { RequirementVersionCoverage } from '../test-case/requirement-version-coverage-model';
import { EntityModel } from '../entity/entity.model';

export interface ExecutionStepActionTestStepPair {
  executionStepId: number;
  testStepId: number;
}

export interface ModifDuringExecModel {
  executionStepActionTestStepPairs: ExecutionStepActionTestStepPair[];
}

export interface ActionStepExecViewModel extends EntityModel {
  id: number;
  actionStepTestCaseId: number;
  actionStepTestCaseName: string;
  actionStepTestCaseReference: string;
  executionTestCaseId: number;
  executionTestCaseName: string;
  executionTestCaseReference: string;
  action: string;
  expectedResult: string;
  coverages: RequirementVersionCoverage[];
}
