export enum InputType {
  PLAIN_TEXT = 'PLAIN_TEXT',
  CHECKBOX = 'CHECKBOX',
  DROPDOWN_LIST = 'DROPDOWN_LIST',
  RICH_TEXT = 'RICH_TEXT',
  DATE_PICKER = 'DATE_PICKER',
  TAG = 'TAG',
  NUMERIC = 'NUMERIC',
}

export function getInputTypeI18n(inputTypeKey: InputType): string {
  return 'sqtm-core.entity.custom-field.' + inputTypeKey;
}
