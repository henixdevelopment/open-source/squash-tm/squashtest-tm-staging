export interface UsersGroup {
  id: number;
  qualifiedName: string;
}

function getI18nKey(userGroup: UsersGroup): string {
  return getI18nKeyFromQualifiedName(userGroup.qualifiedName);
}

function getI18nKeyFromQualifiedName(qualifiedName: string): string {
  switch (qualifiedName) {
    case 'squashtest.authz.group.core.Admin':
      return 'sqtm-core.entity.user.account.group-authority.admin';
    case 'squashtest.authz.group.tm.User':
      return 'sqtm-core.entity.user.account.group-authority.user';
    case 'squashtest.authz.group.tm.TestAutomationServer':
      return 'sqtm-core.entity.user.account.group-authority.automated-server';
    default:
      throw new Error('Unknown users group ' + qualifiedName);
  }
}

export const UsersGroupHelpers = {
  getI18nKeyFromQualifiedName,
  getI18nKey,
};
