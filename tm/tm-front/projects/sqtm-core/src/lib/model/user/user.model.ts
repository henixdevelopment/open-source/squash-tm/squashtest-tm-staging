import { UsersGroup } from './users-group';
import { AuthenticatedUser } from './authenticated-user.model';
import { Profile } from '../profile/profile.model';

export type SimpleUser = Pick<User, 'id' | 'firstName' | 'lastName' | 'login'>;

export interface User {
  id: number;
  login: string;
  firstName: string;
  lastName: string;
  email: string;
  active: boolean;
  createdOn: string;
  createdBy: string;
  lastModifiedBy: string;
  lastModifiedOn: string;
  lastConnectedOn: string;
  usersGroupBinding: number;
  usersGroups: UsersGroup[];
  profiles: Profile[];
  projectPermissions: ProjectPermission[];
  teams: AssociatedTeam[];
  canManageLocalPassword: boolean;
  canDeleteFromFront: boolean;
}

export interface MilestonePossibleOwner {
  id: number;
  login: string;
  firstName: string;
  lastName: string;
}

export interface ProjectPermission {
  projectId: number;
  projectName: string;
  permissionGroup: Profile;
}

export interface ApiToken {
  name: string;
  expiryDate: Date;
  createdOn: Date;
  lastUsage: Date;
  permissions: string;
}

export interface AssociatedTeam {
  partyId: number;
  name: string;
}

export function formatFullUserName(user: User | SimpleUser | AuthenticatedUser): string {
  const login = isAuthenticatedUser(user) ? user.username : user.login;
  const fullName = user.firstName ? `${user.firstName} ${user.lastName}` : user.lastName;
  return `${fullName} (${login})`;
}

function isAuthenticatedUser(user: any): user is AuthenticatedUser {
  return typeof user.username === 'string';
}
