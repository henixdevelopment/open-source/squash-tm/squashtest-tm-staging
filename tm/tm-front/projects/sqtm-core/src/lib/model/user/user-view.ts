export interface UserView {
  id?: number;
  firstName?: string;
  lastName?: string;
  login: string;
}
