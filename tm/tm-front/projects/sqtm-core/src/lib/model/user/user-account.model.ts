import { BugTracker, BugTrackerMode } from '../bugtracker/bug-tracker.model';
import { Credentials } from '../third-party-server/credentials.model';
import { UsersGroup } from './users-group';
import { ProjectPermission } from './user.model';

export interface BugTrackerCredentials {
  bugTracker: BugTracker;
  credentials: Credentials;
}

export interface UserAccount {
  id: number;
  userGroup: UsersGroup;
  email: string;
  firstName: string;
  lastName: string;
  login: string;
  projectPermissions: ProjectPermission[];
  bugTrackerMode: BugTrackerMode;
  bugTrackerCredentials: BugTrackerCredentials[];
  canManageLocalPassword: boolean;
  hasLocalPassword: boolean;
}
