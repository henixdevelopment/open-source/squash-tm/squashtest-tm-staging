export interface AuthenticatedUser {
  userId: number;
  username: string;
  admin: boolean;
  hasAnyReadPermission: boolean;
  projectManager: boolean;
  milestoneManager: boolean;
  clearanceManager: boolean;
  functionalTester: boolean;
  automationProgrammer: boolean;
  firstName: string;
  lastName: string;
  canDeleteFromFront: boolean;
}

export enum UserRoles {
  ROLE_ADMIN = 'ROLE_ADMIN',
  ROLE_TA_API_CLIENT = 'ROLE_TA_API_CLIENT',
}
