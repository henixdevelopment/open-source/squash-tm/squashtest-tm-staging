import { AuthenticationProtocol } from './authentication.model';

export interface BaseAuthConfiguration {
  implementedProtocol: string;
  type: string;
}

export interface OAuth2Configuration extends BaseAuthConfiguration {
  implementedProtocol: 'OAUTH_2';
  type: 'OAUTH_2';
  grantType: GrantType;
  clientId: string;
  clientSecret: string;
  authorizationUrl: string;
  requestTokenUrl: string;
  callbackUrl: string;
  scope: string;
}

export type AuthConfiguration = OAuth2Configuration;

export enum SignatureMethod {
  HMAC_SHA1 = 'HMAC_SHA1',
  RSA_SHA1 = 'RSA_SHA1',
}

export enum GrantType {
  CODE = 'CODE',
  PKCE = 'PKCE',
}

export function isOauth2(
  authConfiguration: AuthConfiguration,
): authConfiguration is OAuth2Configuration {
  return authConfiguration.implementedProtocol === AuthenticationProtocol.OAUTH_2;
}
