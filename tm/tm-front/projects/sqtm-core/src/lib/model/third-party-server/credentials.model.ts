import { AuthenticationProtocol } from './authentication.model';

export interface BasicAuthCredentials {
  implementedProtocol: AuthenticationProtocol.BASIC_AUTH;
  type: AuthenticationProtocol.BASIC_AUTH;
  username: string;
  password?: string;
  registered?: boolean;
}

export interface TokenAuthCredentials {
  implementedProtocol: AuthenticationProtocol.TOKEN_AUTH;
  type: AuthenticationProtocol.TOKEN_AUTH;
  token: string;
  registered?: boolean;
}

export interface OAuth2Credentials {
  implementedProtocol: AuthenticationProtocol.OAUTH_2;
  type: AuthenticationProtocol.OAUTH_2;
  accessToken: string;
  refreshToken?: string;
  registered?: boolean;
}

export type Credentials = BasicAuthCredentials | TokenAuthCredentials | OAuth2Credentials;

export function isBasicAuthCredentials(credentials: any): credentials is BasicAuthCredentials {
  return credentials?.implementedProtocol === AuthenticationProtocol.BASIC_AUTH;
}

export function isTokenAuthCredentials(credentials: any): credentials is TokenAuthCredentials {
  return credentials?.implementedProtocol === AuthenticationProtocol.TOKEN_AUTH;
}

export function isOAuth2Credentials(credentials: any): credentials is OAuth2Credentials {
  return credentials?.implementedProtocol === AuthenticationProtocol.OAUTH_2;
}
