import { AclClass, Permissions } from '../permissions/permissions.model';

export interface Profile {
  id: number;
  qualifiedName: string;
  active: boolean;
  system: boolean;
}

export interface ProfileView extends Profile {
  name: string; // profile name with translated i18n for system profiles
  partyCount: number;
  description: string;
  createdBy: string;
  createdOn: Date;
  lastModifiedBy: string;
  lastModifiedOn: Date;
  permissions: ProfilePermissions[];
  partyProfileAuthorizations: PartyProfileAuthorization[];
}

export interface ProfilesAndPermissions {
  profileId: number;
  profileName: string;
  active: boolean;
  system: boolean;
  permissions: ProfilePermissions[];
}

export interface ProfilePermissions {
  className: string;
  simplifiedClassName: AclClass;
  permissions: Permissions[];
}

export interface ProfilePermissionsPanel {
  code: string;
  id: string;
  active: boolean;
  items?: ProfilePermissionsPanel[];
}

export interface ActivePermission {
  permission: Permissions;
  active: boolean;
}

export type WorkspaceActivePermissions = {
  className: string;
  permissions: ActivePermission[];
};

export interface PermissionsMatrixPanel {
  id: string;
  active: boolean;
  permissions?: PermissionsMatrixPanel[];
}

export type CollapsePanelStatus = 'CLOSE' | 'OPEN' | 'INVERSE';

export const systemProfilesOrder: string[] = [
  'squashtest.acl.group.tm.ProjectManager',
  'squashtest.acl.group.tm.TestEditor',
  'squashtest.acl.group.tm.TestDesigner',
  'squashtest.acl.group.tm.AdvanceTester',
  'squashtest.acl.group.tm.TestRunner',
  'squashtest.acl.group.tm.Validator',
  'squashtest.acl.group.tm.ProjectViewer',
  'squashtest.acl.group.tm.AutomatedTestWriter',
];

export interface PartyProfileAuthorization {
  partyName: string;
  partyId: number;
  active: boolean;
  team: boolean;
}
