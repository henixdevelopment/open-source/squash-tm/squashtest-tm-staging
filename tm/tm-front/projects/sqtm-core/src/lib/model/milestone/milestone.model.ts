import { MilestoneRange, MilestoneStatusKeys } from '../level-enums/level-enum';

export interface Milestone {
  id: number;
  label: string;
  description: string;
  endDate: string;
  status: string;
  range: string;
  ownerFistName: string;
  ownerLastName: string;
  ownerLogin: string;
  createdBy: string;
  createdOn: string;
  lastModifiedBy: string;
  lastModifiedOn: string;
}

export interface MilestoneAdminView extends Milestone {
  canEdit: boolean;
  boundProjectsInformation: ProjectInfoForMilestoneAdminView[];
}

export interface MilestoneView extends Milestone {
  boundToObject: boolean;
}

export interface ProjectInfoForMilestoneAdminView {
  projectId: number;
  projectName: string;
  template: boolean;
  boundToMilestone: boolean;
  milestoneBoundToOneObjectOfProject: boolean;
}

export const BLOCKING_MILESTONE_STATUSES: ReadonlyArray<MilestoneStatusKeys> = [
  'LOCKED',
  'PLANNED',
];

export function milestoneAllowModification(milestone: Milestone): boolean {
  return Boolean(milestone) && !BLOCKING_MILESTONE_STATUSES.includes(milestone.status as any);
}

export function doMilestonesAllowModification(milestones: Milestone[]): boolean {
  if (!milestones) {
    // Falsy array might indicate we don't have any milestone, so there's no milestone preventing modification.
    return true;
  }

  return milestones.every(milestoneAllowModification);
}

// Returns the full user name or a i18n key if owner is an administrator.
export function formatMilestoneOwner(milestone: Milestone): string {
  if (milestone.range === MilestoneRange.GLOBAL.id) {
    return 'sqtm-core.generic.label.administrator';
  }
  const firstName = milestone.ownerFistName;
  const lastName = milestone.ownerLastName;
  const login = milestone.ownerLogin;

  if (firstName && firstName !== '') {
    return `${firstName} ${lastName} (${login})`;
  } else {
    return `${lastName} (${login})`;
  }
}
