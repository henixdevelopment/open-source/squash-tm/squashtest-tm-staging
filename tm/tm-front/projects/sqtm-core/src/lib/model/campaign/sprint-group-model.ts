import { EntityModel } from '../entity/entity.model';

export interface SprintGroupModel extends EntityModel {
  name: string;
  createdOn: string;
  createdBy: string;
  lastModifiedOn: string;
  lastModifiedBy: string;
  description: string;
}
