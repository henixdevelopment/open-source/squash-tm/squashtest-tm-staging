import { SimpleUser } from '../user/user.model';
import { SprintReqVersionModel } from './sprint-req-version-model';
import { SprintStatusKeys } from '../level-enums/level-enum';

export interface SprintReqVersionViewModel extends SprintReqVersionModel {
  assignableUsers: SimpleUser[];
  sprintStatus: SprintStatusKeys;
}

export interface AvailableTestPlanItemModel {
  testCaseId: number;
  testCaseName: string;
  testCaseReference: string;
  datasetId: number;
  datasetName: string;
}
