import { EntityModel } from '../entity/entity.model';
import { ManagementMode, RemotePerimeterStatus } from './sprint-model';
import { SprintReqVersionValidationStatusKeys } from '../level-enums/level-enum';

export interface SprintReqVersionModel extends EntityModel {
  name: string;
  reference: string;
  createdOn: string;
  createdBy: string;
  lastModifiedBy: string;
  lastModifiedOn: string;
  categoryId: number;
  categoryLabel: string;
  criticality: string;
  status: string;
  validationStatus: SprintReqVersionValidationStatusKeys;
  versionId: number;
  requirementId: number;
  requirementVersionProjectId: number;
  testPlanId: number;
  description: string;
  requirementVersionProjectName: string;
  remotePerimeterStatus: RemotePerimeterStatus;
  remoteReqUrl: string;
  remoteReqState: string;
  mode: ManagementMode;
  nbIssues: number;
  nbExecutions: number;
  nbTests: number;
}
