import { CustomDashboardModel } from '../../custom-report/custom-dashboard.model';
import { EntityModel } from '../../entity/entity.model';

export interface CampaignFolderModel extends EntityModel {
  name: string;
  description: string;
  nbIssues: number;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
  dashboard?: CustomDashboardModel;
}
