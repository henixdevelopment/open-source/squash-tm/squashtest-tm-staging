import { EntityModel } from '../entity/entity.model';
import { SprintReqVersionModel } from './sprint-req-version-model';
import { SprintStatusKeys } from '../level-enums/level-enum';
import { SynchronizationPluginId } from '../system/system-view.model';
import { SimpleUser } from '../user/user.model';

export interface SprintModel extends EntityModel {
  name: string;
  reference: string;
  createdOn: string;
  createdBy: string;
  lastModifiedOn: string;
  lastModifiedBy: string;
  startDate?: string;
  endDate?: string;
  description: string;
  nbRequirements: number;
  hasDataSet: boolean;
  sprintReqVersions: SprintReqVersionModel[];
  nbSprintReqVersions: number;
  status: SprintStatusKeys;
  remoteState?: SprintStatusKeys;
  synchronisationKind?: SynchronizationPluginId;
  nbIssues: number;
  nbTestPlanItems: number;
  assignableUsers: SimpleUser[];
}

export enum ManagementMode {
  NATIVE = 'NATIVE',
  SYNCHRONIZED = 'SYNCHRONIZED',
}

export enum RemotePerimeterStatus {
  UNKNOWN = 'UNKNOWN',
  IN_CURRENT_PERIMETER = 'IN_CURRENT_PERIMETER',
  OUT_OF_CURRENT_PERIMETER = 'OUT_OF_CURRENT_PERIMETER',
  NOT_FOUND = 'NOT_FOUND',
}
