import { EntityModel } from '../../entity/entity.model';
import { CustomDashboardModel } from '../../custom-report/custom-dashboard.model';

export interface CampaignLibraryModel extends EntityModel {
  name: string;
  description: string;
  nbIssues: number;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
  dashboard?: CustomDashboardModel;
}
