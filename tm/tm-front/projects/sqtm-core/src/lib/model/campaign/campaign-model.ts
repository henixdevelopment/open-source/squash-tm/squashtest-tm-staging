import { Milestone } from '../milestone/milestone.model';
import { TestPlanStatistics } from './test-plan-statistics';
import { SimpleUser } from '../user/user.model';
import { NamedReference } from '../named-reference';
import {
  ConclusivenessKeys,
  ExecutionStatusKeys,
  TestCaseImportanceKeys,
} from '../level-enums/level-enum';
import { CustomDashboardModel } from '../custom-report/custom-dashboard.model';
import { EntityModel } from '../entity/entity.model';
import { TestSuiteTestInventoryStatistics } from './iteration-model';

export interface CampaignModel extends EntityModel {
  name: string;
  description: string;
  reference: string;
  campaignStatus: string;
  progressStatus: string;
  lastModifiedOn: string;
  lastModifiedBy: string;
  createdOn: string;
  createdBy: string;
  milestones: Milestone[];
  testPlanStatistics: TestPlanStatistics;
  actualStartDate: string;
  actualEndDate: string;
  actualStartAuto: boolean;
  actualEndAuto: boolean;
  scheduledStartDate: string;
  scheduledEndDate: string;
  nbIssues: number;
  hasDatasets: boolean;
  users: SimpleUser[];
  testSuites: NamedReference[];
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
  campaignStatisticsBundle?: StatisticsBundle;
  dashboard?: CustomDashboardModel;
  nbTestPlanItems: number;
}

export interface IterationPlanning {
  id: number;
  name: string;
  scheduledStartDate: Date;
  scheduledEndDate: Date;
}

export interface ScheduledIteration {
  id: number;
  name: string;
  testplanCount: number;
  scheduledStart: string;
  scheduledEnd: string;
  cumulativeTestsByDate: [string, number][];
}

export interface ProgressionStatistics {
  cumulativeExecutionsPerDate: [string, number][];
  scheduledIterations: ScheduledIteration[];
  errors: string[];
}

export type ExecutionStatusCount = Partial<{ [K in ExecutionStatusKeys]: number }>;

export type ConclusivenessStatusCount = { [K in ConclusivenessKeys]: number };

export type TestCaseImportanceCount = { [K in TestCaseImportanceKeys]: number };

export interface TestInventoryStatistics {
  name: string;
  statistics: ExecutionStatusCount;
}

export interface StatisticsBundle {
  progressionStatistics: ProgressionStatistics;
  testCaseStatusStatistics: ExecutionStatusCount;
  testCaseSuccessRateStatistics: {
    conclusiveness: { [K in TestCaseImportanceKeys]: ConclusivenessStatusCount };
  };
  nonExecutedTestCaseImportanceStatistics: TestCaseImportanceCount;
  testInventoryStatistics: TestInventoryStatistics[];
  testsuiteTestInventoryStatisticsList: TestSuiteTestInventoryStatistics[];
}
