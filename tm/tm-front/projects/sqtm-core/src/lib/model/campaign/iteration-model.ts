import { IterationTestPlanItem } from './iteration-test-plan.item';
import { TestPlanStatistics } from './test-plan-statistics';
import { SimpleUser } from '../user/user.model';
import { Milestone } from '../milestone/milestone.model';
import { NamedReference } from '../named-reference';
import { TestCaseImportanceKeys } from '../level-enums/level-enum';
import { ExecutionStatusCount, StatisticsBundle } from './campaign-model';
import { CustomDashboardModel } from '../custom-report/custom-dashboard.model';
import { EntityModel } from '../entity/entity.model';

export interface IterationModel extends EntityModel {
  name: string;
  reference: string;
  description: string;
  uuid: string;
  iterationStatus: string;
  createdOn: string;
  createdBy: string;
  lastModifiedOn: string;
  lastModifiedBy: string;
  testPlanStatistics: TestPlanStatistics;
  actualStartDate: string;
  actualEndDate: string;
  actualStartAuto: boolean;
  actualEndAuto: boolean;
  scheduledStartDate: string;
  scheduledEndDate: string;
  hasDatasets: boolean;
  executionStatusMap: Map<number, string>;
  itpi: IterationTestPlanItem[];
  nbIssues: number;
  users: SimpleUser[];
  milestones: Milestone[];
  testSuites: NamedReference[];
  iterationStatisticsBundle?: StatisticsBundle;
  nbAutomatedSuites: number;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
  dashboard?: CustomDashboardModel;
  nbTestPlanItems: number;
}

export interface TestSuiteTestInventoryStatistics {
  testsuiteName: string;
  scheduledStart: string;
  scheduledEnd: string;
  nbTotal: number;
  nbExecuted: number;
  pcProgress: number;
  nbToExecute: number;
  pcSuccess: number;
  pcFailure: number;
  pcPrevProgress: number;
  nbPrevToExecute: number;
  statusStatistics: ExecutionStatusCount;
  importanceStatistics: { [K in TestCaseImportanceKeys]: number };
}
