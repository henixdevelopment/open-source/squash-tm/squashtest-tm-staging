import { TestCaseImportanceKeys } from '../level-enums/level-enum';

export interface SprintReqVersionTestPlanItem {
  testPlanItemId: number;
  projectId: number;
  projectName: string;
  testCaseReference: string;
  testCaseId: number;
  testCaseName: string;
  importance: TestCaseImportanceKeys;
  executionStatus: string;
  assigneeId: number;
  lastExecutedOn: string;
  datasetId: number;
  datasetName: string;
  inferredExecutionMode: string;
}
