export interface FailureDetail {
  id: number;
  message: string;
}

export interface FailureDetailComponentData {
  failureDetailList: FailureDetail[];
  uiState: {
    loading: boolean;
  };
}
