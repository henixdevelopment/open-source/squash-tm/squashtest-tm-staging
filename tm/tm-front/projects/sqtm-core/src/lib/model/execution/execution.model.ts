import { DenormalizedCustomFieldValueModel } from './denormalized-custom-field-value.model';
import {
  ExecutionStatusKeys,
  SessionNoteKindKeys,
  SprintStatusKeys,
  TestCaseImportanceKeys,
  TestCaseStatusKeys,
} from '../level-enums/level-enum';
import { RequirementVersionCoverage } from '../test-case/requirement-version-coverage-model';
import { Milestone } from '../milestone/milestone.model';
import { TestAutomationServerKind } from '../test-automation/test-automation-server.model';
import { DenormalizedEnvironmentVariable } from '../denormalized-environment/denormalized-environment-variable.model';
import { DenormalizedEnvironmentTag } from '../denormalized-environment/denormalized-environment-tag.model';
import { EntityModel } from '../entity/entity.model';
import { AttachmentListModel } from '../attachment/attachment-list.model';

export interface ExecutionModel extends EntityModel {
  id: number;
  name: string;
  executionOrder: number;
  executionStepViews: ExecutionStepModel[];
  prerequisite: string;
  testCaseId?: number;
  tcNatLabel: string;
  tcNatIconName: string;
  tcTypeLabel: string;
  tcTypeIconName: string;
  tcStatus: TestCaseStatusKeys;
  tcImportance: TestCaseImportanceKeys;
  tcDescription: string;
  comment: string;
  datasetLabel?: string;
  denormalizedCustomFieldValues: DenormalizedCustomFieldValueModel[];
  coverages: RequirementVersionCoverage[];
  executionMode: string;
  lastExecutedOn: string;
  lastExecutedBy: string;
  executionStatus: ExecutionStatusKeys;
  testAutomationServerKind: TestAutomationServerKind;
  automatedExecutionResultUrl: string;
  automatedExecutionResultSummary: string;
  automatedJobUrl: string;
  automatedExecutionDuration: number;
  nbIssues: number;
  iterationId: number;
  kind: ExecutionKind;
  milestones: Milestone[];
  testPlanItemId: number;
  executionsCount: number;
  denormalizedEnvironmentVariables: DenormalizedEnvironmentVariable[];
  denormalizedEnvironmentTags: DenormalizedEnvironmentTag;
  exploratorySessionOverviewInfo: ExploratorySessionOverviewInfo | null;
  exploratoryExecutionRunningState: ExploratoryExecutionRunningState | null;
  sessionNotes: SessionNoteModel[];
  latestExploratoryExecutionEvent: LatestSessionEventModel;
  reviewed: boolean;
  taskDivision: string;
  parentSprintStatus: SprintStatusKeys | null;
  extenderId: number;
}

export interface LatestSessionEventModel {
  timestamp: string;
  timeElapsed: number;
}

export type ExploratoryExecutionRunningState = 'NEVER_STARTED' | 'RUNNING' | 'PAUSED' | 'STOPPED';

export function getTranslationKeyFromRunningState(
  runningState: ExploratoryExecutionRunningState,
): string {
  switch (runningState) {
    case 'RUNNING':
    case 'PAUSED': {
      return 'sqtm-core.campaign-workspace.exploratory-execution.status.running';
    }
    case 'STOPPED': {
      return 'sqtm-core.campaign-workspace.exploratory-execution.status.finished';
    }
    default:
      return 'sqtm-core.campaign-workspace.exploratory-execution.status.to-do';
  }
}

export type ExecutionKind = 'STANDARD' | 'GHERKIN' | 'KEYWORD';

export interface ExecutionStepModel extends EntityModel {
  order: number;
  executionStatus: ExecutionStatusKeys;
  action: string;
  expectedResult: string;
  comment: string;
  denormalizedCustomFieldValues: DenormalizedCustomFieldValueModel[];
  lastExecutedOn: string;
  lastExecutedBy: string;
  testStepId?: number;
}

export interface SessionNoteModel {
  noteId: number;
  kind: SessionNoteKindKeys;
  content: string;
  noteOrder: number;
  createdOn: string;
  createdBy: string;
  lastModifiedOn: string;
  lastModifiedBy: string;
  attachmentList: AttachmentListModel;
}

export interface ExecutionSummaryDto {
  executionId: number;
  executionStatus: ExecutionStatusKeys;
  executedOn: string;
}

export interface ExploratorySessionOverviewInfo {
  sessionOverviewId: number;
  sessionDuration: number;
  charter: string;
}
