import { EntityModel } from '../entity/entity.model';
import { SimpleUser } from '../user/user.model';
import { Milestone } from '../milestone/milestone.model';
import { SprintStatusKeys } from '../level-enums/level-enum';

export interface ExploratorySessionOverviewModel extends EntityModel {
  name: string;
  reference: string;
  charter: string;
  sessionDuration: number;
  executionStatus: string;
  sessionStatus: string;
  lastModifiedOn: string;
  lastModifiedBy: string;
  createdOn: string;
  createdBy: string;
  dueDate: string;
  assignedUser: string;
  assignableUsers: SimpleUser[];
  nbIssues: number;
  nbExecutions: number;
  nbNotes: number;
  inferredSessionReviewStatus: string;
  comments: string;
  milestones: Milestone[];
  sprintStatus: SprintStatusKeys;
  tclnId: number;
}
