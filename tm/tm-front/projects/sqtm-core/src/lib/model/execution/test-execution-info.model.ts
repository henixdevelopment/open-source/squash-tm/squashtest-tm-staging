export interface TestExecutionInfo {
  testId: number;
  testName: string;
  dataset: string;
  statusBySuite: { [key: string]: string };
}

export interface SuiteIdCreatedOn {
  id: string;
  createdOn: string;
  statuses: [];
}
