import { InputType } from '../customfield/input-type.model';

// custom field values from server
export interface DenormalizedCustomFieldValueModel {
  id: number;
  denormalizedFieldHolderId: number;
  value: string;
  label: string;
  fieldType: string;
  inputType: InputType;
}
