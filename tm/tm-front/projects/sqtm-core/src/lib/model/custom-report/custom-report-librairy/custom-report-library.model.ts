import { EntityModel } from '../../entity/entity.model';

export interface CustomReportLibraryModel extends EntityModel {
  name: string;
  description: string;
}
