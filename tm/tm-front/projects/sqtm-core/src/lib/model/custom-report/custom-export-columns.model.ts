const CUSTOM_EXPORT_COLUMNS_CAMPAIGN: ReadonlyArray<string> = [
  'CAMPAIGN_LABEL',
  'CAMPAIGN_ID',
  'CAMPAIGN_REFERENCE',
  'CAMPAIGN_DESCRIPTION',
  'CAMPAIGN_STATE',
  'CAMPAIGN_PROGRESS_STATUS',
  'CAMPAIGN_MILESTONE',
  'CAMPAIGN_SCHEDULED_START',
  'CAMPAIGN_SCHEDULED_END',
  'CAMPAIGN_ACTUAL_START',
  'CAMPAIGN_ACTUAL_END',
];

const CUSTOM_EXPORT_COLUMNS_ITERATION: ReadonlyArray<string> = [
  'ITERATION_LABEL',
  'ITERATION_ID',
  'ITERATION_REFERENCE',
  'ITERATION_DESCRIPTION',
  'ITERATION_STATE',
  'ITERATION_SCHEDULED_START',
  'ITERATION_SCHEDULED_END',
  'ITERATION_ACTUAL_START',
  'ITERATION_ACTUAL_END',
];

const CUSTOM_EXPORT_COLUMNS_TEST_SUITE: ReadonlyArray<string> = [
  'TEST_SUITE_LABEL',
  'TEST_SUITE_ID',
  'TEST_SUITE_DESCRIPTION',
  'TEST_SUITE_EXECUTION_STATUS',
  'TEST_SUITE_PROGRESS_STATUS',
];

const CUSTOM_EXPORT_COLUMNS_TEST_CASE: ReadonlyArray<string> = [
  'TEST_CASE_PROJECT',
  'TEST_CASE_MILESTONE',
  'TEST_CASE_LABEL',
  'TEST_CASE_ID',
  'TEST_CASE_REFERENCE',
  'TEST_CASE_DESCRIPTION',
  'TEST_CASE_STATUS',
  'TEST_CASE_IMPORTANCE',
  'TEST_CASE_NATURE',
  'TEST_CASE_TYPE',
  'TEST_CASE_DATASET',
  'TEST_CASE_PREREQUISITE',
  'TEST_CASE_LINKED_REQUIREMENTS_NUMBER',
  'TEST_CASE_LINKED_REQUIREMENTS_IDS',
];

const CUSTOM_EXPORT_COLUMNS_EXECUTION: ReadonlyArray<string> = [
  'EXECUTION_ID',
  'EXECUTION_EXECUTION_MODE',
  'EXECUTION_STATUS',
  'EXECUTION_SUCCESS_RATE',
  'EXECUTION_USER',
  'EXECUTION_EXECUTION_DATE',
  'EXECUTION_COMMENT',
];

const CUSTOM_EXPORT_COLUMNS_EXECUTION_STEP: ReadonlyArray<string> = [
  'EXECUTION_STEP_STEP_NUMBER',
  'EXECUTION_STEP_ACTION',
  'EXECUTION_STEP_RESULT',
  'EXECUTION_STEP_STATUS',
  'EXECUTION_STEP_USER',
  'EXECUTION_STEP_EXECUTION_DATE',
  'EXECUTION_STEP_COMMENT',
  'EXECUTION_STEP_LINKED_REQUIREMENTS_NUMBER',
  'EXECUTION_STEP_LINKED_REQUIREMENTS_IDS',
];

const CUSTOM_EXPORT_COLUMNS_ISSUE: ReadonlyArray<string> = [
  'ISSUE_EXECUTION_AND_EXECUTION_STEP_ISSUES_NUMBER',
  'ISSUE_EXECUTION_AND_EXECUTION_STEP_ISSUES_IDS',
  'ISSUE_EXECUTION_STEP_ISSUES_NUMBER',
  'ISSUE_EXECUTION_STEP_ISSUES_IDS',
];

export enum CustomExportEntityType {
  CAMPAIGN = 'CAMPAIGN',
  ITERATION = 'ITERATION',
  TEST_SUITE = 'TEST_SUITE',
  TEST_CASE = 'TEST_CASE',
  TEST_STEP = 'TEST_STEP',
  EXECUTION = 'EXECUTION',
  EXECUTION_STEP = 'EXECUTION_STEP',
  ISSUE = 'ISSUE',
}

export type CustomExportColumnsByEntityType = { [key in CustomExportEntityType]: string[] };

export function getCustomExportColumnNamesByEntityType(
  withMilestoneColumns: boolean,
): CustomExportColumnsByEntityType {
  const columns: CustomExportColumnsByEntityType = {
    [CustomExportEntityType.CAMPAIGN]: [...CUSTOM_EXPORT_COLUMNS_CAMPAIGN],
    [CustomExportEntityType.ITERATION]: [...CUSTOM_EXPORT_COLUMNS_ITERATION],
    [CustomExportEntityType.TEST_SUITE]: [...CUSTOM_EXPORT_COLUMNS_TEST_SUITE],
    [CustomExportEntityType.TEST_CASE]: [...CUSTOM_EXPORT_COLUMNS_TEST_CASE],
    [CustomExportEntityType.TEST_STEP]: [], // [SQUASH-3366] adding test steps only for custom fields
    [CustomExportEntityType.EXECUTION]: [...CUSTOM_EXPORT_COLUMNS_EXECUTION],
    [CustomExportEntityType.EXECUTION_STEP]: [...CUSTOM_EXPORT_COLUMNS_EXECUTION_STEP],
    [CustomExportEntityType.ISSUE]: [...CUSTOM_EXPORT_COLUMNS_ISSUE],
  };

  if (!withMilestoneColumns) {
    Object.keys(columns).forEach((key) => {
      columns[key] = columns[key].filter((column) => !isMilestoneColumn(column));
    });
  }

  return columns;
}

const milestoneColumns: ReadonlyArray<string> = ['CAMPAIGN_MILESTONE', 'TEST_CASE_MILESTONE'];

function isMilestoneColumn(columnName: string): boolean {
  return milestoneColumns.includes(columnName);
}

export class CustomExportColumnUtils {
  static getColumnI18nKey(columnName: string): string {
    return `sqtm-core.custom-report-workspace.custom-export.columns.${columnName}`;
  }

  static getEntityI18nKey(entityName: string): string {
    return `sqtm-core.custom-report-workspace.custom-export.entities.${entityName}`;
  }
}
