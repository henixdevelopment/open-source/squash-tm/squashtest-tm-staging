import { EntityModel } from '../../entity/entity.model';

export interface CustomReportFolderModel extends EntityModel {
  name: string;
  description: string;
}
