export class DenormalizedEnvironmentTag {
  id: number;
  value: string;
}
