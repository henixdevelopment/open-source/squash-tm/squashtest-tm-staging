export class DenormalizedEnvironmentVariable {
  id: number;
  namedValue: string;
  evId: number;
}
