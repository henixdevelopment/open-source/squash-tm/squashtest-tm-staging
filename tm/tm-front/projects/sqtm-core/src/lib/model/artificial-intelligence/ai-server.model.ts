import { Credentials } from '../third-party-server/credentials.model';
import {
  AuthenticationPolicy,
  AuthenticationProtocol,
} from '../third-party-server/authentication.model';

export interface AiServer {
  id: number;
  url: string;
  name: string;
  payloadTemplate: string;
  description: string;
  createdBy: string;
  createdOn: string;
  lastModifiedBy: string;
  lastModifiedOn: string;
  jsonPath: string;
}

export interface AdminAiServer extends AiServer {
  authPolicy: AuthenticationPolicy.APP_LEVEL;
  authProtocol: AuthenticationProtocol.TOKEN_AUTH;
  credentials?: Credentials;
}
