import { DisplayOption } from '../display-option';
import { ProjectData } from '../project/project-data.model';

export type I18nEnum<K extends string> = { [id in K]: I18nEnumItem<K> };

export type LevelEnum<K extends string> = { [id in K]: LevelEnumItem<K> };

export interface I18nEnumItem<K> {
  id: K;
  i18nKey: string;
  color?: string;
  icon?: string;
}

export interface LevelEnumItem<K> extends I18nEnumItem<K> {
  level: number;
}

export type MilestoneStatusLevelEnum<K extends string> = {
  [id in K]: MilestoneStatusLevelEnumItem<K>;
};

export interface MilestoneStatusLevelEnumItem<K> extends LevelEnumItem<K> {
  isBindableToProject?: boolean;
  isBindableToObject?: boolean;
  allowObjectCreateAndDelete?: boolean;
  allowObjectModification?: boolean;
}

export type CampaignStatusLevelEnum<K extends string> = {
  [id in K]: CampaignStatusLevelEnumItem<K>;
};

export interface CampaignStatusLevelEnumItem<K> extends LevelEnumItem<K> {
  color: string;
  icon: string;
}

export type TestPlanStatusLevelEnum<K extends string> = {
  [id in K]: LevelEnumItem<K>;
};

export type ExecutionStatusLevelEnum<K extends string> = {
  [id in K]: ExecutionStatusLevelEnumItem<K>;
};

export interface ExecutionStatusLevelEnumItem<K> extends LevelEnumItem<K> {
  color: string;
  textColor: string;
  indexBackgroundColor: string;
  backgroundColor: string;
  terminal: boolean;
  conclusiveness: LevelEnumItem<ConclusivenessKeys>;
  icon: string;
  canonical: boolean;
  canonicalStatus?: ExecutionStatusKeys;
}

export type TestCaseStatusLevelEnum<K extends string> = {
  [id in K]: TestCaseStatusLevelEnumItem<K>;
};

export interface TestCaseStatusLevelEnumItem<K> extends LevelEnumItem<K> {
  color: string;
  chartColor: string;
  icon: string;
}

export type TestCaseExecutionModeLevelEnum<K extends string> = {
  [id in K]: TestCaseExecutionModeLevelEnumItem<K>;
};

export interface TestCaseExecutionModeLevelEnumItem<K> extends LevelEnumItem<K> {
  icon: string;
}

export type TestCaseImportanceLevelEnum<K extends string> = {
  [id in K]: TestCaseImportanceLevelEnumItem<K>;
};

export interface TestCaseImportanceLevelEnumItem<K> extends LevelEnumItem<K> {
  color: string;
  chartColor: string;
  icon: string;
}

export type TestCaseImportanceKeys = 'LOW' | 'MEDIUM' | 'HIGH' | 'VERY_HIGH';
export type TestCaseStatusKeys =
  | 'WORK_IN_PROGRESS'
  | 'UNDER_REVIEW'
  | 'APPROVED'
  | 'OBSOLETE'
  | 'TO_BE_UPDATED';
export type TestCaseNatureKeys =
  | 'Fonctionnel'
  | 'Non fonctionnel'
  | 'Métier'
  | 'Utilisateur'
  | 'Performance'
  | 'Sécurité'
  | 'ATDD';
export type TestCaseTypeTest =
  | 'Non défini'
  | 'Recevabilité'
  | 'Correctif'
  | 'Evolution'
  | 'Non-régression'
  | 'Bout-en-bout'
  | 'Partenaire';
export type RequirementCriticalityKeys = 'CRITICAL' | 'MAJOR' | 'MINOR' | 'UNDEFINED';
export type RequirementStatusKeys = 'WORK_IN_PROGRESS' | 'UNDER_REVIEW' | 'APPROVED' | 'OBSOLETE';
export type MilestoneStatusKeys = 'PLANNED' | 'IN_PROGRESS' | 'FINISHED' | 'LOCKED';
export type SearchableMilestoneStatusKeys = 'IN_PROGRESS' | 'FINISHED' | 'LOCKED';
export type MilestoneRangeKeys = 'GLOBAL' | 'RESTRICTED';
export type CampaignStatusKeys = 'UNDEFINED' | 'PLANNED' | 'IN_PROGRESS' | 'FINISHED' | 'ARCHIVED';
export type TestPlanStatusKeys = 'DONE' | 'READY' | 'RUNNING';
export type SprintStatusKeys = 'UPCOMING' | 'OPEN' | 'CLOSED' | 'DELETED';
export type AutomationRequestStatusKeys =
  | 'TRANSMITTED'
  | 'AUTOMATION_IN_PROGRESS'
  | 'SUSPENDED'
  | 'REJECTED'
  | 'AUTOMATED'
  | 'READY_TO_TRANSMIT'
  | 'WORK_IN_PROGRESS';

export type ToBeValidatedAutomationRequestStatusKeys =
  | 'SUSPENDED'
  | 'REJECTED'
  | 'WORK_IN_PROGRESS';

export type ToBeTreatAutomationRequestStatusKeys = 'TRANSMITTED' | 'AUTOMATION_IN_PROGRESS';

export type TestCaseExecutionModeKeys = 'AUTOMATED' | 'MANUAL' | 'EXPLORATORY' | 'UNDEFINED';

export const allTestCaseExecutionModeKeys: TestCaseExecutionModeKeys[] = [
  'AUTOMATED',
  'MANUAL',
  'EXPLORATORY',
  'UNDEFINED',
];

export type ConclusivenessKeys = 'SUCCESS' | 'FAILURE' | 'NON_CONCLUSIVE';

export type ExecutionStatusKeys =
  | 'READY'
  | 'RUNNING'
  | 'SUCCESS'
  | 'WARNING'
  | 'FAILURE'
  | 'BLOCKED'
  | 'ERROR'
  | 'SKIPPED'
  | 'CANCELLED'
  | 'NOT_RUN'
  | 'UNTESTABLE'
  | 'NOT_FOUND'
  | 'SETTLED';

export type IterationStatusKeys = 'UNDEFINED' | 'PLANNED' | 'IN_PROGRESS' | 'FINISHED' | 'ARCHIVED';

export type TestCaseAutomatableKeys = 'M' | 'Y' | 'N';

export type BddScriptLanguageKeys = 'ENGLISH' | 'FRENCH' | 'GERMAN' | 'SPANISH';

export type BddImplementationTechnologyKeys = 'CUCUMBER_4' | 'CUCUMBER_5_PLUS' | 'ROBOT';

export type WorkspaceKeys = 'HOME' | 'TEST_CASE' | 'REQUIREMENT' | 'CAMPAIGN';

export const TestCaseWeight: TestCaseImportanceLevelEnum<TestCaseImportanceKeys> = {
  VERY_HIGH: {
    id: 'VERY_HIGH',
    level: 1,
    i18nKey: 'sqtm-core.entity.test-case.importance.VERY_HIGH',
    color: '#CE0000',
    chartColor: '#9d0208',
    icon: 'sqtm-core-test-case:double_up',
  },
  HIGH: {
    id: 'HIGH',
    level: 2,
    i18nKey: 'sqtm-core.entity.test-case.importance.HIGH',
    color: '#FF9719',
    chartColor: '#dc2f02',
    icon: 'sqtm-core-test-case:up',
  },
  MEDIUM: {
    id: 'MEDIUM',
    level: 3,
    i18nKey: 'sqtm-core.entity.test-case.importance.MEDIUM',
    color: '#F4CF27',
    chartColor: '#f48c06',
    icon: 'sqtm-core-test-case:down',
  },
  LOW: {
    id: 'LOW',
    level: 4,
    i18nKey: 'sqtm-core.entity.test-case.importance.LOW',
    color: '#0885C6',
    chartColor: '#ffba08',
    icon: 'sqtm-core-test-case:double_down',
  },
};

export function getTestCaseImportanceChartColors(): string[] {
  return Object.values(TestCaseWeight).map((w) => w.chartColor);
}

export const TestCaseStatus: TestCaseStatusLevelEnum<TestCaseStatusKeys> = {
  WORK_IN_PROGRESS: {
    id: 'WORK_IN_PROGRESS',
    level: 1,
    i18nKey: 'sqtm-core.entity.test-case.status.WORK_IN_PROGRESS',
    color: '#F4CF27',
    chartColor: '#83c5be',
    icon: 'sqtm-core-test-case:status',
  },
  UNDER_REVIEW: {
    id: 'UNDER_REVIEW',
    level: 2,
    i18nKey: 'sqtm-core.entity.test-case.status.UNDER_REVIEW',
    color: '#02A7F0',
    chartColor: '#42999b',
    icon: 'sqtm-core-test-case:status',
  },
  APPROVED: {
    id: 'APPROVED',
    level: 3,
    i18nKey: 'sqtm-core.entity.test-case.status.APPROVED',
    color: '#1FBF05',
    chartColor: '#006d77',
    icon: 'sqtm-core-test-case:status',
  },
  OBSOLETE: {
    id: 'OBSOLETE',
    level: 4,
    i18nKey: 'sqtm-core.entity.test-case.status.OBSOLETE',
    color: '#7F7F7F',
    chartColor: '#ecdbac',
    icon: 'sqtm-core-test-case:status',
  },
  TO_BE_UPDATED: {
    id: 'TO_BE_UPDATED',
    level: 5,
    i18nKey: 'sqtm-core.entity.test-case.status.TO_BE_UPDATED',
    color: '#F59A23',
    chartColor: '#052639',
    icon: 'sqtm-core-test-case:status',
  },
};

export function getTestCaseStatusChartColors(): string[] {
  return Object.values(TestCaseStatus).map((w) => w.chartColor);
}

export type RequirementCriticalityLevelEnum<K extends string> = {
  [id in K]: RequirementCriticalityLevelEnumItem<K>;
};

export interface RequirementCriticalityLevelEnumItem<K> extends LevelEnumItem<K> {
  color: string;
  icon: string;
  chartColor: string;
}

export const RequirementCriticality: RequirementCriticalityLevelEnum<RequirementCriticalityKeys> = {
  CRITICAL: {
    id: 'CRITICAL',
    level: 1,
    i18nKey: 'sqtm-core.entity.requirement.criticality.CRITICAL',
    color: '#CE0000',
    chartColor: '#9d0208',
    icon: 'sqtm-core-requirement:double_up',
  },
  MAJOR: {
    id: 'MAJOR',
    level: 2,
    i18nKey: 'sqtm-core.entity.requirement.criticality.MAJOR',
    color: '#FF9719',
    chartColor: '#dc2f02',
    icon: 'sqtm-core-requirement:up',
  },
  MINOR: {
    id: 'MINOR',
    level: 3,
    i18nKey: 'sqtm-core.entity.requirement.criticality.MINOR',
    color: '#0885C6',
    chartColor: '#f48c06',
    icon: 'sqtm-core-requirement:double_down',
  },
  UNDEFINED: {
    id: 'UNDEFINED',
    level: 4,
    i18nKey: 'sqtm-core.entity.requirement.criticality.UNDEFINED',
    color: '#B5ADAD',
    chartColor: '#ffba08',
    icon: 'sqtm-core-requirement:minus',
  },
};

export function getRequirementCriticalityChartColors(): string[] {
  return Object.values(RequirementCriticality).map((w) => w.chartColor);
}

export type RequirementStatusLevelEnum<K extends string> = {
  [id in K]: RequirementStatusLevelEnumItem<K>;
};

export interface RequirementStatusLevelEnumItem<K> extends LevelEnumItem<K> {
  allowModifications: boolean;
  chartColor: string;
}

export const RequirementStatus: RequirementStatusLevelEnum<RequirementStatusKeys> = {
  WORK_IN_PROGRESS: {
    id: 'WORK_IN_PROGRESS',
    level: 1,
    i18nKey: 'sqtm-core.entity.requirement.status.WORK_IN_PROGRESS',
    color: '#F4CF27',
    chartColor: '#83c5be',
    icon: 'sqtm-core-requirement:status',
    allowModifications: true,
  },
  UNDER_REVIEW: {
    id: 'UNDER_REVIEW',
    level: 2,
    i18nKey: 'sqtm-core.entity.requirement.status.UNDER_REVIEW',
    color: '#02A7F0',
    chartColor: '#42999b',
    icon: 'sqtm-core-requirement:status',
    allowModifications: true,
  },
  APPROVED: {
    id: 'APPROVED',
    level: 3,
    i18nKey: 'sqtm-core.entity.requirement.status.APPROVED',
    color: '#1FBF05',
    chartColor: '#006d77',
    icon: 'sqtm-core-requirement:status',
    allowModifications: false,
  },
  OBSOLETE: {
    id: 'OBSOLETE',
    level: 4,
    i18nKey: 'sqtm-core.entity.requirement.status.OBSOLETE',
    color: '#7F7F7F',
    chartColor: '#ecdbac',
    icon: 'sqtm-core-requirement:status',
    allowModifications: false,
  },
};

export const RequirementNature: I18nEnum<string> = {
  STANDARD: {
    id: 'STANDARD',
    i18nKey: 'sqtm-core.entity.requirement.nature.standard',
  },
  HIGH_LEVEL: {
    id: 'HIGH_LEVEL',
    i18nKey: 'sqtm-core.entity.requirement.nature.high-level',
  },
};

export function getRequirementStatusChartColors(): string[] {
  return Object.values(RequirementStatus).map((w) => w.chartColor);
}

export const TestPlanStatus: TestPlanStatusLevelEnum<TestPlanStatusKeys> = {
  READY: {
    id: 'READY',
    level: 1,
    i18nKey: 'sqtm-core.entity.campaign.progress-status.READY',
  },
  RUNNING: {
    id: 'RUNNING',
    level: 2,
    i18nKey: 'sqtm-core.entity.campaign.progress-status.RUNNING',
  },
  DONE: {
    id: 'DONE',
    level: 3,
    i18nKey: 'sqtm-core.entity.campaign.progress-status.DONE',
  },
};

export const CampaignStatus: CampaignStatusLevelEnum<CampaignStatusKeys> = {
  UNDEFINED: {
    id: 'UNDEFINED',
    level: 1,
    i18nKey: 'sqtm-core.entity.campaign.campaignStatus.UNDEFINED',
    color: '#7f7f7f',
    icon: 'sqtm-core-campaign:status',
  },
  PLANNED: {
    id: 'PLANNED',
    level: 2,
    i18nKey: 'sqtm-core.entity.campaign.campaignStatus.PLANNED',
    color: '#f4cf27',
    icon: 'sqtm-core-campaign:status',
  },
  IN_PROGRESS: {
    id: 'IN_PROGRESS',
    level: 3,
    i18nKey: 'sqtm-core.entity.campaign.campaignStatus.IN_PROGRESS',
    color: '#02a7f0',
    icon: 'sqtm-core-campaign:status',
  },
  FINISHED: {
    id: 'FINISHED',
    level: 4,
    i18nKey: 'sqtm-core.entity.campaign.campaignStatus.FINISHED',
    color: '#1fbf05',
    icon: 'sqtm-core-campaign:status',
  },
  ARCHIVED: {
    id: 'ARCHIVED',
    level: 5,
    i18nKey: 'sqtm-core.entity.campaign.campaignStatus.ARCHIVED',
    color: '#a00fad',
    icon: 'sqtm-core-campaign:status',
  },
};

export const MilestoneStatus: MilestoneStatusLevelEnum<MilestoneStatusKeys> = {
  PLANNED: {
    id: 'PLANNED',
    level: 1,
    i18nKey: 'sqtm-core.entity.milestone.status.PLANNED',
    isBindableToProject: true,
    isBindableToObject: false,
    allowObjectCreateAndDelete: false,
    allowObjectModification: false,
  },
  IN_PROGRESS: {
    id: 'IN_PROGRESS',
    level: 2,
    i18nKey: 'sqtm-core.entity.milestone.status.IN_PROGRESS',
    isBindableToProject: true,
    isBindableToObject: true,
    allowObjectCreateAndDelete: true,
    allowObjectModification: true,
  },
  FINISHED: {
    id: 'FINISHED',
    level: 3,
    i18nKey: 'sqtm-core.entity.milestone.status.FINISHED',
    isBindableToProject: true,
    isBindableToObject: true,
    allowObjectCreateAndDelete: true,
    allowObjectModification: true,
  },
  LOCKED: {
    id: 'LOCKED',
    level: 4,
    i18nKey: 'sqtm-core.entity.milestone.status.LOCKED',
    isBindableToProject: false,
    isBindableToObject: false,
    allowObjectCreateAndDelete: false,
    allowObjectModification: false,
  },
};

export const SearchableMilestoneStatus: MilestoneStatusLevelEnum<SearchableMilestoneStatusKeys> = {
  IN_PROGRESS: {
    id: 'IN_PROGRESS',
    level: 2,
    i18nKey: 'sqtm-core.entity.milestone.status.IN_PROGRESS',
    isBindableToProject: true,
    isBindableToObject: true,
    allowObjectCreateAndDelete: true,
    allowObjectModification: true,
  },
  FINISHED: {
    id: 'FINISHED',
    level: 3,
    i18nKey: 'sqtm-core.entity.milestone.status.FINISHED',
    isBindableToProject: true,
    isBindableToObject: true,
    allowObjectCreateAndDelete: true,
    allowObjectModification: true,
  },
  LOCKED: {
    id: 'LOCKED',
    level: 4,
    i18nKey: 'sqtm-core.entity.milestone.status.LOCKED',
    isBindableToProject: false,
    isBindableToObject: false,
    allowObjectCreateAndDelete: false,
    allowObjectModification: false,
  },
};

export const MilestoneRange: LevelEnum<MilestoneRangeKeys> = {
  GLOBAL: {
    id: 'GLOBAL',
    level: 1,
    i18nKey: 'sqtm-core.entity.milestone.range.GLOBAL',
  },
  RESTRICTED: {
    id: 'RESTRICTED',
    level: 2,
    i18nKey: 'sqtm-core.entity.milestone.range.RESTRICTED',
  },
};

export const AutomationRequestStatus: LevelEnum<AutomationRequestStatusKeys> = {
  TRANSMITTED: {
    id: 'TRANSMITTED',
    level: 1,
    i18nKey: 'sqtm-core.entity.automation-request.status.TRANSMITTED',
  },
  AUTOMATION_IN_PROGRESS: {
    id: 'AUTOMATION_IN_PROGRESS',
    level: 2,
    i18nKey: 'sqtm-core.entity.automation-request.status.AUTOMATION_IN_PROGRESS',
  },
  SUSPENDED: {
    id: 'SUSPENDED',
    level: 3,
    i18nKey: 'sqtm-core.entity.automation-request.status.SUSPENDED',
  },
  REJECTED: {
    id: 'REJECTED',
    level: 4,
    i18nKey: 'sqtm-core.entity.automation-request.status.REJECTED',
  },
  AUTOMATED: {
    id: 'AUTOMATED',
    level: 5,
    i18nKey: 'sqtm-core.entity.automation-request.status.AUTOMATED',
  },
  READY_TO_TRANSMIT: {
    id: 'READY_TO_TRANSMIT',
    level: 6,
    i18nKey: 'sqtm-core.entity.automation-request.status.READY_TO_TRANSMIT',
  },
  WORK_IN_PROGRESS: {
    id: 'WORK_IN_PROGRESS',
    level: 7,
    i18nKey: 'sqtm-core.entity.automation-request.status.WORK_IN_PROGRESS',
  },
};

export const ToBeValidatedAutomationRequestStatus: LevelEnum<ToBeValidatedAutomationRequestStatusKeys> =
  {
    SUSPENDED: {
      id: 'SUSPENDED',
      level: 3,
      i18nKey: 'sqtm-core.entity.automation-request.status.SUSPENDED',
    },
    REJECTED: {
      id: 'REJECTED',
      level: 4,
      i18nKey: 'sqtm-core.entity.automation-request.status.REJECTED',
    },
    WORK_IN_PROGRESS: {
      id: 'WORK_IN_PROGRESS',
      level: 7,
      i18nKey: 'sqtm-core.entity.automation-request.status.WORK_IN_PROGRESS',
    },
  };

export const ToBeTreatAutomationRequestStatus: LevelEnum<ToBeTreatAutomationRequestStatusKeys> = {
  TRANSMITTED: {
    id: 'TRANSMITTED',
    level: 1,
    i18nKey: 'sqtm-core.entity.automation-request.status.TRANSMITTED',
  },
  AUTOMATION_IN_PROGRESS: {
    id: 'AUTOMATION_IN_PROGRESS',
    level: 2,
    i18nKey: 'sqtm-core.entity.automation-request.status.AUTOMATION_IN_PROGRESS',
  },
};

export const TestCaseExecutionMode: TestCaseExecutionModeLevelEnum<TestCaseExecutionModeKeys> = {
  AUTOMATED: {
    id: 'AUTOMATED',
    level: 1,
    i18nKey: 'sqtm-core.entity.test-case.execution-mode.AUTOMATED',
    icon: 'sqtm-core-test-case:automation',
  },
  MANUAL: {
    id: 'MANUAL',
    level: 2,
    i18nKey: 'sqtm-core.entity.test-case.execution-mode.MANUAL',
    icon: 'sqtm-core-test-case:manual_mode',
  },
  EXPLORATORY: {
    id: 'EXPLORATORY',
    level: 3,
    i18nKey: 'sqtm-core.entity.test-case.execution-mode.EXPLORATORY',
    icon: 'sqtm-core-test-case:exploratory',
  },
  UNDEFINED: {
    id: 'UNDEFINED',
    level: 4,
    i18nKey: 'sqtm-core.entity.test-case.execution-mode.UNDEFINED',
    icon: '',
  },
};

export const Conclusiveness: LevelEnum<ConclusivenessKeys> = {
  SUCCESS: {
    id: 'SUCCESS',
    level: 1,
    i18nKey: 'sqtm-core.entity.execution.status.SUCCESS',
  },
  FAILURE: {
    id: 'FAILURE',
    level: 2,
    i18nKey: 'sqtm-core.entity.execution.status.FAILURE',
  },
  NON_CONCLUSIVE: {
    id: 'NON_CONCLUSIVE',
    level: 3,
    i18nKey: 'sqtm-core.entity.execution.non-conclusive',
  },
};

export const ExecutionStatus: ExecutionStatusLevelEnum<ExecutionStatusKeys> = {
  READY: {
    id: 'READY',
    level: 1,
    i18nKey: 'sqtm-core.entity.execution.status.READY',
    color: '#a3b2b8',
    indexBackgroundColor: 'rgba(163, 178, 184, 0.2)',
    backgroundColor: 'rgb(163, 178, 184)',
    textColor: 'black',
    terminal: false,
    conclusiveness: Conclusiveness.NON_CONCLUSIVE,
    icon: 'sqtm-core-campaign:exec_status',
    canonical: true,
  },
  RUNNING: {
    id: 'RUNNING',
    level: 2,
    i18nKey: 'sqtm-core.entity.execution.status.RUNNING',
    color: '#0078ae',
    indexBackgroundColor: 'rgba(0, 120, 174, 0.2)',
    backgroundColor: 'rgb(0, 120, 174)',
    textColor: 'white',
    terminal: false,
    conclusiveness: Conclusiveness.NON_CONCLUSIVE,
    icon: 'sqtm-core-campaign:exec_status',
    canonical: true,
  },
  SUCCESS: {
    id: 'SUCCESS',
    level: 3,
    i18nKey: 'sqtm-core.entity.execution.status.SUCCESS',
    color: '#006f57',
    indexBackgroundColor: 'rgba(0, 111, 87, 0.2)',
    backgroundColor: 'rgb(0, 111, 87)',
    textColor: 'white',
    terminal: true,
    conclusiveness: Conclusiveness.SUCCESS,
    icon: 'sqtm-core-campaign:exec_status',
    canonical: true,
  },
  WARNING: {
    id: 'WARNING',
    level: 4,
    i18nKey: 'sqtm-core.entity.execution.status.WARNING',
    color: '#05bf71',
    indexBackgroundColor: 'rgba(5, 191, 113, 0.2)',
    backgroundColor: 'rgb(5, 191, 113)',
    textColor: 'white',
    terminal: true,
    conclusiveness: Conclusiveness.SUCCESS,
    icon: 'sqtm-core-campaign:exec_status',
    canonical: false,
    canonicalStatus: 'SUCCESS',
  },
  SETTLED: {
    id: 'SETTLED',
    level: 5,
    i18nKey: 'sqtm-core.entity.execution.status.SETTLED',
    color: '#05bf71',
    indexBackgroundColor: 'rgba(5, 191, 113, 0.2)',
    backgroundColor: 'rgb(5, 191, 113)',
    textColor: 'white',
    terminal: true,
    conclusiveness: Conclusiveness.SUCCESS,
    icon: 'sqtm-core-campaign:exec_status',
    canonical: true,
  },
  SKIPPED: {
    id: 'SKIPPED',
    level: 6,
    i18nKey: 'sqtm-core.entity.execution.status.SKIPPED',
    color: '#6e3200',
    indexBackgroundColor: 'rgba(110, 50, 0, 0.2)',
    backgroundColor: 'rgb(110, 50, 0)',
    textColor: 'white',
    terminal: true,
    conclusiveness: Conclusiveness.NON_CONCLUSIVE,
    icon: 'sqtm-core-campaign:exec_status',
    canonical: true,
  },
  CANCELLED: {
    id: 'CANCELLED',
    level: 7,
    i18nKey: 'sqtm-core.entity.execution.status.CANCELLED',
    color: '#9b5fcd',
    indexBackgroundColor: 'rgba(155, 95, 205, 0.2)',
    backgroundColor: 'rgb(155, 95, 205)',
    textColor: 'white',
    terminal: true,
    conclusiveness: Conclusiveness.NON_CONCLUSIVE,
    icon: 'sqtm-core-campaign:exec_status',
    canonical: true,
  },
  FAILURE: {
    id: 'FAILURE',
    level: 7,
    i18nKey: 'sqtm-core.entity.execution.status.FAILURE',
    color: '#cb1524',
    indexBackgroundColor: 'rgba(203, 21, 36, 0.2)',
    backgroundColor: 'rgba(203, 21, 36)',
    textColor: 'white',
    terminal: true,
    conclusiveness: Conclusiveness.FAILURE,
    icon: 'sqtm-core-campaign:exec_status',
    canonical: true,
  },
  BLOCKED: {
    id: 'BLOCKED',
    level: 8,
    i18nKey: 'sqtm-core.entity.execution.status.BLOCKED',
    color: '#ffcc00',
    indexBackgroundColor: 'rgba(255, 204, 0, 0.2)',
    backgroundColor: 'rgb(255, 204, 0)',
    textColor: 'white',
    terminal: true,
    conclusiveness: Conclusiveness.NON_CONCLUSIVE,
    icon: 'sqtm-core-campaign:exec_status',
    canonical: true,
  },
  NOT_RUN: {
    id: 'NOT_RUN',
    level: 9,
    i18nKey: 'sqtm-core.entity.execution.status.NOT_RUN',
    color: '#a3b2b8',
    indexBackgroundColor: 'rgba(163, 178, 184, 0.2)',
    backgroundColor: 'rgb(163, 178, 184)',
    textColor: 'white',
    terminal: true,
    conclusiveness: Conclusiveness.NON_CONCLUSIVE,
    icon: 'sqtm-core-campaign:exec_status',
    canonical: false,
    canonicalStatus: 'BLOCKED',
  },
  ERROR: {
    id: 'ERROR',
    level: 10,
    i18nKey: 'sqtm-core.entity.execution.status.ERROR',
    color: '#ffcc00',
    indexBackgroundColor: 'rgba(255, 204, 0, 0.2)',
    backgroundColor: 'rgb(255, 204, 0)',
    textColor: 'white',
    terminal: true,
    conclusiveness: Conclusiveness.NON_CONCLUSIVE,
    icon: 'sqtm-core-campaign:exec_status',
    canonical: false,
    canonicalStatus: 'BLOCKED',
  },
  UNTESTABLE: {
    id: 'UNTESTABLE',
    level: 11,
    i18nKey: 'sqtm-core.entity.execution.status.UNTESTABLE',
    color: '#3d3d3d',
    indexBackgroundColor: 'rgba(61, 61, 61, 0.2)',
    backgroundColor: 'rgb(61, 61, 61)',
    textColor: 'white',
    terminal: true,
    conclusiveness: Conclusiveness.NON_CONCLUSIVE,
    icon: 'sqtm-core-campaign:exec_status',
    canonical: true,
  },
  NOT_FOUND: {
    id: 'NOT_FOUND',
    level: 12,
    i18nKey: 'sqtm-core.entity.execution.status.NOT_FOUND',
    color: '#3d3d3d',
    indexBackgroundColor: 'rgba(61, 61, 61, 0.2)',
    backgroundColor: 'rgb(61, 61, 61)',
    textColor: 'white',
    terminal: true,
    conclusiveness: Conclusiveness.NON_CONCLUSIVE,
    icon: 'sqtm-core-campaign:exec_status',
    canonical: false,
    canonicalStatus: 'UNTESTABLE',
  },
};

export const TerminalExecutionStatus: ExecutionStatusLevelEnumItem<ExecutionStatusKeys>[] =
  Object.entries(ExecutionStatus)
    .map((entry) => entry[1])
    .filter(
      (levelEnumItem: ExecutionStatusLevelEnumItem<ExecutionStatusKeys>) => !levelEnumItem.terminal,
    );

const conclusiveStatuses: ExecutionStatusKeys[] = ['SUCCESS', 'FAILURE', 'SETTLED'];

export const InconclusiveExecutionStatus = Object.values(ExecutionStatus).filter(
  (status) => !conclusiveStatuses.includes(status.id),
);

export const CanonicalExecutionStatus: ExecutionStatusLevelEnumItem<ExecutionStatusKeys>[] =
  Object.values(ExecutionStatus).filter(
    (levelEnumItem: ExecutionStatusLevelEnumItem<ExecutionStatusKeys>) => levelEnumItem.canonical,
  );

// Temporary quick fix: issue #2806
export const nonAutomatedOnlyExecutionStatus: ExecutionStatusKeys[] = [
  'READY',
  'RUNNING',
  'SUCCESS',
  'SETTLED',
  'FAILURE',
  'BLOCKED',
  'UNTESTABLE',
];

export const manualExecutionStatuses: ExecutionStatusKeys[] = [
  'READY',
  'RUNNING',
  'SUCCESS',
  'FAILURE',
  'BLOCKED',
];

export function getFilteredExecutionStatusKeys(projectData: ProjectData): ExecutionStatusKeys[] {
  const filteredExecutionStatusKeys = manualExecutionStatuses;

  if (isExecutionStatusEnabled('SETTLED', projectData)) {
    filteredExecutionStatusKeys.push('SETTLED');
  }

  if (isExecutionStatusEnabled('UNTESTABLE', projectData)) {
    filteredExecutionStatusKeys.push('UNTESTABLE');
  }

  return filteredExecutionStatusKeys;
}

function isExecutionStatusEnabled(
  statusKey: ExecutionStatusKeys,
  projectData: ProjectData,
): boolean {
  return !projectData.disabledExecutionStatus.includes(statusKey);
}

export const TestCaseAutomatable: LevelEnum<TestCaseAutomatableKeys> = {
  M: { id: 'M', level: 1, i18nKey: 'sqtm-core.entity.test-case.automatable.M' },
  Y: { id: 'Y', level: 2, i18nKey: 'sqtm-core.entity.test-case.automatable.Y' },
  N: { id: 'N', level: 3, i18nKey: 'sqtm-core.entity.test-case.automatable.N' },
};

export const BddScriptLanguage: LevelEnum<BddScriptLanguageKeys> = {
  ENGLISH: {
    id: 'ENGLISH',
    level: 1,
    i18nKey: 'sqtm-core.administration-workspace.views.project.scripts-language.ENGLISH',
  },
  SPANISH: {
    id: 'SPANISH',
    level: 2,
    i18nKey: 'sqtm-core.administration-workspace.views.project.scripts-language.SPANISH',
  },
  GERMAN: {
    id: 'GERMAN',
    level: 3,
    i18nKey: 'sqtm-core.administration-workspace.views.project.scripts-language.GERMAN',
  },
  FRENCH: {
    id: 'FRENCH',
    level: 4,
    i18nKey: 'sqtm-core.administration-workspace.views.project.scripts-language.FRENCH',
  },
};

export const BddImplementationTechnology: LevelEnum<BddImplementationTechnologyKeys> = {
  CUCUMBER_5_PLUS: {
    id: 'CUCUMBER_5_PLUS',
    level: 1,
    i18nKey:
      'sqtm-core.administration-workspace.views.project.automation.technology.CUCUMBER_5_PLUS',
  },
  CUCUMBER_4: {
    id: 'CUCUMBER_4',
    level: 2,
    i18nKey: 'sqtm-core.administration-workspace.views.project.automation.technology.CUCUMBER_4',
  },
  ROBOT: {
    id: 'ROBOT',
    level: 3,
    i18nKey: 'sqtm-core.administration-workspace.views.project.automation.technology.ROBOT',
  },
};

export const IterationStatus: LevelEnum<IterationStatusKeys> = {
  UNDEFINED: {
    id: 'UNDEFINED',
    level: 0,
    i18nKey: 'sqtm-core.entity.iteration.status.UNDEFINED',
  },
  PLANNED: {
    id: 'PLANNED',
    level: 1,
    i18nKey: 'sqtm-core.entity.iteration.status.PLANNED',
  },
  IN_PROGRESS: {
    id: 'IN_PROGRESS',
    level: 2,
    i18nKey: 'sqtm-core.entity.iteration.status.IN_PROGRESS',
  },
  FINISHED: {
    id: 'FINISHED',
    level: 3,
    i18nKey: 'sqtm-core.entity.iteration.status.FINISHED',
  },
  ARCHIVED: {
    id: 'ARCHIVED',
    level: 4,
    i18nKey: 'sqtm-core.entity.iteration.status.ARCHIVED',
  },
};

export type BooleanFilterKeys = 'TRUE' | 'FALSE';

export const BooleanI18nEnum: LevelEnum<BooleanFilterKeys> = {
  TRUE: {
    id: 'TRUE',
    level: 0,
    i18nKey: 'sqtm-core.generic.label.yes',
  },
  FALSE: {
    id: 'FALSE',
    level: 1,
    i18nKey: 'sqtm-core.generic.label.no',
  },
};

export const FalseTrueI18nEnum: I18nEnum<string> = {
  true: {
    id: 'true',
    i18nKey: 'sqtm-core.generic.label.true',
  },
  false: {
    id: 'false',
    i18nKey: 'sqtm-core.generic.label.false',
  },
};

export interface SessionNoteKindLevelEnumItem<K> extends LevelEnumItem<K> {
  backgroundColor: string;
  textColor: string;
}

export type SessionNoteKindLevelEnum<K extends string> = {
  [id in K]: SessionNoteKindLevelEnumItem<K>;
};

export type SessionNoteKindKeys = 'COMMENT' | 'SUGGESTION' | 'BUG' | 'QUESTION' | 'POSITIVE';

export const SessionNoteKind: SessionNoteKindLevelEnum<SessionNoteKindKeys> = {
  COMMENT: {
    id: 'COMMENT',
    level: 1,
    textColor: '#000000',
    backgroundColor: '#ffd205',
    i18nKey: 'sqtm-core.entity.execution.note-kind.COMMENT',
  },
  SUGGESTION: {
    id: 'SUGGESTION',
    level: 2,
    textColor: '#ffffff',
    backgroundColor: '#f15f13',
    i18nKey: 'sqtm-core.entity.execution.note-kind.SUGGESTION',
  },
  BUG: {
    id: 'BUG',
    level: 3,
    textColor: '#ffffff',
    backgroundColor: '#C60B0B',
    i18nKey: 'sqtm-core.entity.execution.note-kind.BUG',
  },
  QUESTION: {
    id: 'QUESTION',
    level: 4,
    textColor: '#ffffff',
    backgroundColor: '#246cff',
    i18nKey: 'sqtm-core.entity.execution.note-kind.QUESTION',
  },
  POSITIVE: {
    id: 'POSITIVE',
    level: 5,
    textColor: '#000000',
    backgroundColor: '#a7c950',
    i18nKey: 'sqtm-core.entity.execution.note-kind.POSITIVE',
  },
};

export type SessionOverviewStatusLevelEnum<K extends string> = {
  [id in K]: LevelEnumItem<K>;
};

export type SessionOverviewStatusKeys = 'TO_DO' | 'RUNNING' | 'FINISHED';

export const SessionOverviewStatus: SessionOverviewStatusLevelEnum<SessionOverviewStatusKeys> = {
  TO_DO: {
    id: 'TO_DO',
    level: 1,
    i18nKey: 'sqtm-core.campaign-workspace.exploratory-execution.status.to-do',
  },
  RUNNING: {
    id: 'RUNNING',
    level: 2,
    i18nKey: 'sqtm-core.campaign-workspace.exploratory-execution.status.running',
  },
  FINISHED: {
    id: 'FINISHED',
    level: 3,
    i18nKey: 'sqtm-core.campaign-workspace.exploratory-execution.status.finished',
  },
};

export type ReviewStatusLevelEnum<K extends string> = {
  [id in K]: LevelEnumItem<K>;
};

export type ReviewStatusKeys = 'TO_DO' | 'RUNNING' | 'DONE';

export const ReviewStatus: ReviewStatusLevelEnum<ReviewStatusKeys> = {
  TO_DO: {
    id: 'TO_DO',
    level: 1,
    i18nKey:
      'sqtm-core.campaign-workspace.exploratory-session-overview.capsule.review-status.status.to-do',
  },
  RUNNING: {
    id: 'RUNNING',
    level: 2,
    i18nKey:
      'sqtm-core.campaign-workspace.exploratory-session-overview.capsule.review-status.status.running',
  },
  DONE: {
    id: 'DONE',
    level: 3,
    i18nKey:
      'sqtm-core.campaign-workspace.exploratory-session-overview.capsule.review-status.status.done',
  },
};

export type CampaignExecutionScopeFilterKeys = 'ALL' | 'LAST_EXECUTED';

export const CampaignExecutionScopeFilter: I18nEnum<CampaignExecutionScopeFilterKeys> = {
  ALL: {
    id: 'ALL',
    i18nKey: 'sqtm-core.search.campaign.criteria.last-execution-scope.all',
  },
  LAST_EXECUTED: {
    id: 'LAST_EXECUTED',
    i18nKey: 'sqtm-core.search.campaign.criteria.last-execution-scope.last-executed',
  },
};

export const SprintStatus: I18nEnum<SprintStatusKeys> = {
  UPCOMING: {
    id: 'UPCOMING',
    color: '#F4CF27',
    i18nKey: 'sqtm-core.sprint.status.UPCOMING',
    icon: 'sqtm-core-campaign:status',
  },
  OPEN: {
    id: 'OPEN',
    color: '#02A7F0',
    i18nKey: 'sqtm-core.sprint.status.OPEN',
    icon: 'sqtm-core-campaign:status',
  },
  CLOSED: {
    id: 'CLOSED',
    color: '#a00fad',
    i18nKey: 'sqtm-core.sprint.status.CLOSED',
    icon: 'sqtm-core-campaign:status',
  },
  DELETED: {
    id: 'DELETED',
    color: '#000000',
    i18nKey: 'sqtm-core.sprint.status.DELETED',
    icon: 'sqtm-core-campaign:status',
  },
};

export interface SprintReqVersionValidationStatusLevelEnumItem<K> extends LevelEnumItem<K> {}

export type SprintReqVersionValidationStatusLevelEnum<K extends string> = {
  [id in K]: SprintReqVersionValidationStatusLevelEnumItem<K>;
};

export type SprintReqVersionValidationStatusKeys =
  | 'TO_BE_TESTED'
  | 'IN_PROGRESS'
  | 'VALIDATED'
  | 'TO_BE_CORRECTED';

export const SprintReqVersionValidationStatus: SprintReqVersionValidationStatusLevelEnum<SprintReqVersionValidationStatusKeys> =
  {
    TO_BE_TESTED: {
      id: 'TO_BE_TESTED',
      level: 1,
      color: '#A3B2B8',
      i18nKey: 'sqtm-core.sprint.req-version.validation-status.TO_BE_TESTED',
      icon: 'sqtm-core-campaign:exec_status',
    },
    IN_PROGRESS: {
      id: 'IN_PROGRESS',
      level: 2,
      color: '#0078AE',
      i18nKey: 'sqtm-core.sprint.req-version.validation-status.IN_PROGRESS',
      icon: 'sqtm-core-campaign:exec_status',
    },
    VALIDATED: {
      id: 'VALIDATED',
      level: 3,
      color: '#006F57',
      i18nKey: 'sqtm-core.sprint.req-version.validation-status.VALIDATED',
      icon: 'sqtm-core-campaign:exec_status',
    },
    TO_BE_CORRECTED: {
      id: 'TO_BE_CORRECTED',
      level: 4,
      color: '#CB1524',
      i18nKey: 'sqtm-core.sprint.req-version.validation-status.TO_BE_CORRECTED',
      icon: 'sqtm-core-campaign:exec_status',
    },
  };

export function buildLevelEnumKeySort(levelEnum: LevelEnum<any>) {
  return (keyA: any, keyB: any) => levelEnumSort(keyA, keyB, levelEnum);
}

export function compareLevelEumItems<K>(itemA: LevelEnumItem<K>, itemB: LevelEnumItem<K>) {
  return compareLevels(itemA.level, itemB.level);
}

export function levelEnumSort(keyA: any, keyB: any, levelEnum: LevelEnum<any>) {
  const itemA = levelEnum[keyA]?.level ?? 0;
  const itemB = levelEnum[keyB]?.level ?? 0;
  return compareLevels(itemA, itemB);
}

export function compareLevels(levelA: number, levelB: number) {
  if (levelA === levelB) {
    return 0;
  } else if (levelA > levelB) {
    return 1;
  } else {
    return -1;
  }
}

export function levelEnumToOptions(levelEnum: LevelEnum<any>): DisplayOption[] {
  const items = Object.values(levelEnum);
  return items.map((i) => ({
    label: i.i18nKey,
    id: i.id,
  }));
}

export interface ExecutionFlagLevelEnumItem<K> extends LevelEnumItem<K> {
  backgroundColor: string;
  textColor: string;
}

export type ExecutionFlagLevelEnum<K extends string> = {
  [id in K]: ExecutionFlagLevelEnumItem<K>;
};

export type ExecutionFlagKeys = 'TO_BE_ANALYSED' | 'FLAKY' | 'FIXED';

export const ExecutionFlag: ExecutionFlagLevelEnum<ExecutionFlagKeys> = {
  TO_BE_ANALYSED: {
    id: 'TO_BE_ANALYSED',
    level: 1,
    textColor: '#ffffff',
    backgroundColor: '#f15f13',
    i18nKey: 'sqtm-core.entity.execution-plan.execution-flag.TO_BE_ANALYSED',
    icon: 'sqtm-core-tree:search',
  },
  FLAKY: {
    id: 'FLAKY',
    level: 2,
    textColor: '#ffffff',
    backgroundColor: '#ffd205',
    i18nKey: 'sqtm-core.entity.execution-plan.execution-flag.FLAKY',
    icon: 'sqtm-core-generic:exclamation',
  },
  FIXED: {
    id: 'FIXED',
    level: 3,
    textColor: '#ffffff',
    backgroundColor: '#a7c950',
    i18nKey: 'sqtm-core.entity.execution-plan.execution-flag.FIXED',
    icon: 'sqtm-core-infolist-item:checkmark',
  },
};
