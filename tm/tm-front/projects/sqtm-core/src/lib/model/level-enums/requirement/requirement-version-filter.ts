import { BooleanFilterKeys, I18nEnum } from '../level-enum';

export type RequirementCurrentVersionFilterKeys = 'ALL' | 'LAST_NON_OBSOLETE' | 'CURRENT';
export const RequirementCurrentVersionFilter: I18nEnum<RequirementCurrentVersionFilterKeys> = {
  ALL: { id: 'ALL', i18nKey: 'sqtm-core.search.requirement.criteria.current-version.all' },
  LAST_NON_OBSOLETE: {
    id: 'LAST_NON_OBSOLETE',
    i18nKey: 'sqtm-core.search.requirement.criteria.current-version.last-non-obsolete',
  },
  CURRENT: {
    id: 'CURRENT',
    i18nKey: 'sqtm-core.search.requirement.criteria.current-version.current',
  },
};

export type RequirementVersionHasDescriptionFilterKey = 'HAS_DESCRIPTION' | 'NO_DESCRIPTION';
export const RequirementVersionHasDescriptionFilter: I18nEnum<RequirementVersionHasDescriptionFilterKey> =
  {
    HAS_DESCRIPTION: { id: 'HAS_DESCRIPTION', i18nKey: 'sqtm-core.generic.label.yes' },
    NO_DESCRIPTION: { id: 'NO_DESCRIPTION', i18nKey: 'sqtm-core.generic.label.no' },
  };

export type RequirementVersionHasChildrenFilterKey = 'HAS_CHILDREN' | 'NO_CHILDREN';
export const RequirementVersionHasChildrenFilter: I18nEnum<RequirementVersionHasChildrenFilterKey> =
  {
    HAS_CHILDREN: {
      id: 'HAS_CHILDREN',
      i18nKey: 'sqtm-core.search.requirement.criteria.has-children.one',
    },
    NO_CHILDREN: {
      id: 'NO_CHILDREN',
      i18nKey: 'sqtm-core.search.requirement.criteria.has-children.none',
    },
  };

export type RequirementVersionHasParentFilterKey = 'HAS_PARENT' | 'NO_PARENT';
export const RequirementVersionHasParentFilter: I18nEnum<RequirementVersionHasParentFilterKey> = {
  HAS_PARENT: { id: 'HAS_PARENT', i18nKey: 'sqtm-core.search.requirement.criteria.linked' },
  NO_PARENT: { id: 'NO_PARENT', i18nKey: 'sqtm-core.search.requirement.criteria.not-linked' },
};

enum REQUIREMENT_ENTITY_KIND {
  REQUIREMENT = 'org.squashtest.tm.domain.requirement.Requirement',
  HIGH_LEVEL_REQUIREMENT = 'org.squashtest.tm.domain.requirement.HighLevelRequirement',
}

export type RequirementKindFilterKey =
  | REQUIREMENT_ENTITY_KIND.REQUIREMENT
  | REQUIREMENT_ENTITY_KIND.HIGH_LEVEL_REQUIREMENT;
export const RequirementKindFilter: I18nEnum<RequirementKindFilterKey> = {
  [REQUIREMENT_ENTITY_KIND.REQUIREMENT]: {
    id: REQUIREMENT_ENTITY_KIND.REQUIREMENT,
    i18nKey: 'sqtm-core.entity.requirement.nature.standard',
  },
  [REQUIREMENT_ENTITY_KIND.HIGH_LEVEL_REQUIREMENT]: {
    id: REQUIREMENT_ENTITY_KIND.HIGH_LEVEL_REQUIREMENT,
    i18nKey: 'sqtm-core.entity.requirement.nature.high-level',
  },
};

export const RequirementBoundToHighLvlReqFilter: I18nEnum<BooleanFilterKeys> = {
  TRUE: {
    id: 'TRUE',
    i18nKey: 'sqtm-core.search.requirement.criteria.linked',
  },
  FALSE: {
    id: 'FALSE',
    i18nKey: 'sqtm-core.search.requirement.criteria.not-linked',
  },
};
