import { AttachmentModel } from './attachment.model';

export interface FileViewer {
  attachment: AttachmentModel;
  index: number;
  viewerType: FileViewerType;
  content?: string;
  isArchive?: boolean;
  activeFile?: string;
}

export enum FileViewerType {
  IMG,
  IFRAME,
  PRE,
  ARCHIVE,
  UNKNOWN,
  ERROR,
  FORBIDDEN,
}

export enum FileViewerFilePathType {
  SYNCHRO = 'SYNCHRO',
  IMPORT = 'IMPORT',
}
