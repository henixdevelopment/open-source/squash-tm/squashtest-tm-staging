export type Attachment = PersistedAttachment | UploadingAttachment | RejectedAttachment;
export type AttachmentKind =
  | 'persisted-attachment'
  | 'uploading-attachment'
  | 'rejected-attachment';

export interface AttachmentModel {
  id: number;
  name: string;
  size: number;
  addedOn: Date;
  lastModifiedOn: Date;
}

// Id is type of string because we mix 'real' attachment with 'virtual' attachment to show running attachment uploads or rejected
// It's an optimisation to avoid separate states and list in renderings
export interface AttachmentBase {
  id: string;
  name: string;
  size: number;
  addedOn: Date;
  lastModifiedOn: Date;
  kind: AttachmentKind;
}

export interface PersistedAttachment extends AttachmentBase {
  id: string;
  kind: 'persisted-attachment';
  pendingDelete?: boolean;
}

export interface UploadingAttachment extends AttachmentBase {
  id: string;
  kind: 'uploading-attachment';
  uploadProgress: number;
}

export interface RejectedAttachment extends AttachmentBase {
  id: string;
  kind: 'rejected-attachment';
  errors: string[];
}

export interface UploadImage {
  id: string;
  kind: string;
  uploadProgress: number;
  attachmentListId: number;
  url: string;
}

export interface TargetOnUpload {
  entityId: number;
  attachmentListId: number;
  holderType: string;
}

export enum AttachmentHolderType {
  ACTION_TEST_STEP = 'actionTestStep',
  EXECUTION_STEP = 'executionStep',
  SESSION_NOTE = 'sessionNote',
}
