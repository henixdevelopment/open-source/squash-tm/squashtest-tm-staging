import { InfoListItem } from './infolistitem.model';

export enum SystemInfoListCodeEnum {
  DEF_TC_TYP = 'DEF_TC_TYP',
  DEF_TC_NAT = 'DEF_TC_NAT',
  DEF_REQ_CAT = 'DEF_REQ_CAT',
}

export enum SystemRequirementCodeEnum {
  CAT_FUNCTIONAL = 'CAT_FUNCTIONAL',
  CAT_NON_FUNCTIONAL = 'CAT_NON_FUNCTIONAL',
  CAT_USE_CASE = 'CAT_USE_CASE',
  CAT_BUSINESS = 'CAT_BUSINESS',
  CAT_TEST_REQUIREMENT = 'CAT_TEST_REQUIREMENT',
  CAT_UNDEFINED = 'CAT_UNDEFINED',
  CAT_ERGONOMIC = 'CAT_ERGONOMIC',
  CAT_PERFORMANCE = 'CAT_PERFORMANCE',
  CAT_TECHNICAL = 'CAT_TECHNICAL',
  CAT_USER_STORY = 'CAT_USER_STORY',
  CAT_SECURITY = 'CAT_SECURITY',
}

export class InfoList {
  id: number;
  uri: string;
  code: string;
  label: string;
  description: string;
  items: InfoListItem[];
}

export function isInfoListCodePatternValid(code: string) {
  return !code.match(/\s+/) && code.match(/^[A-Za-z0-9_]*$/);
}

export function isSystemInfoList(infoList: InfoList) {
  const systemInfoListCodes = Object.values(SystemInfoListCodeEnum);
  return (systemInfoListCodes as string[]).includes(infoList.code);
}
