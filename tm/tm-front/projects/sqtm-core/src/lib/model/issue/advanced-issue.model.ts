import { RemoteIssue, RemoteProject } from './remote-issue.model';

// Advanced Issues (e.g. Jira)

export interface AdvancedIssue extends RemoteIssue {
  fieldValues: { [fieldId: string]: FieldValue };
  project: AdvancedProject;
  id: string;
  btName: string;
  currentScheme: string;
}

export interface AdvancedProject extends RemoteProject {
  id: string;
  name: string;
  schemes: FieldsMap;
}

export interface FieldsMap {
  [schemeName: string]: Field[];
}

export interface Field {
  id: string;
  label: string;
  possibleValues: FieldValue[];
  rendering: ValueRendering;
}

export interface FieldGroup extends Field {
  fields: Field[];
}

export interface SelectorField extends Field {
  fields: Field[];
}

export interface FieldValue {
  id: string;
  typename: string;
  scalar: string;
  composite: FieldValue[];
  custom: unknown;
  name: string;
  autocompleteLabel?: string;
}

export interface ValueRendering {
  operations: string[];
  inputType: ValueRenderingInputType;
  required: boolean;
}

export interface ValueRenderingInputType {
  name: string;
  original: string;
  dataType: string;
  fieldSchemeSelector: boolean;
  configuration: any;
}

export function isAdvancedIssue(remoteIssue: RemoteIssue): remoteIssue is AdvancedIssue {
  if (remoteIssue == null || typeof remoteIssue !== 'object') {
    return false;
  }

  return Boolean(remoteIssue['currentScheme']);
}

// Not used in frontend
type RemoteIssueContext = unknown;

export interface AdvancedIssueReportForm extends AdvancedIssue {
  remoteIssueContext: RemoteIssueContext;
  hasCacheError: boolean;
}
