import { Identifier } from '../entity.model';

export interface RemoteIssue {
  id: string;
  summary: string;
  description: string;
  comment: string;
  project: RemoteProject;
  state: RemoteStatus;
  assignee: RemoteUser;
  priority: RemotePriority;
  category: RemoteCategory;
  version: RemoteVersion;
  bugtracker: string;
  hasBlankId: boolean;

  getNewKey(): string;
}

export interface RemoteAttribute {
  id: Identifier;
  name: string;
}

export type RemoteProject = RemoteAttribute;
export type RemoteStatus = RemoteAttribute;
export type RemoteUser = RemoteAttribute;
export type RemotePriority = RemoteAttribute;
export type RemoteCategory = RemoteAttribute;
export type RemoteVersion = RemoteAttribute;
