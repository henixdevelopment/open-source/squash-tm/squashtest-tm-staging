import { FieldValue } from './advanced-issue.model';

export interface ChangeSet {
  changes: Change[];
}

enum ChangeKind {
  REPLACE_VALUE = 'REPLACE_VALUE',
  REPLACE_POSSIBLE_VALUES = 'REPLACE_POSSIBLE_VALUES',
  APPEND_POSSIBLE_VALUES = 'APPEND_POSSIBLE_VALUES',
}

export interface Change {
  kind: ChangeKind;
  fieldId: string;
  newValue?: FieldValue;
  newPossibleValues?: FieldValue[];
}

export function isChangeSet(object: any): object is ChangeSet {
  return object?.class === 'org.squashtest.tm.bugtracker.advanceddomain.ChangeSet';
}

export function isReplaceValueChange(change: Change): boolean {
  return change.kind === ChangeKind.REPLACE_VALUE;
}

export function isReplacePossibleValuesChange(change: Change): boolean {
  return change.kind === ChangeKind.REPLACE_POSSIBLE_VALUES;
}

export function isAppendPossibleValuesChange(change: Change): boolean {
  return change.kind === ChangeKind.APPEND_POSSIBLE_VALUES;
}
