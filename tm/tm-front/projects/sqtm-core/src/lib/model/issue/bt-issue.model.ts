import { Identifier } from '../entity.model';
import {
  RemoteCategory,
  RemoteIssue,
  RemotePriority,
  RemoteProject,
  RemoteStatus,
  RemoteUser,
  RemoteVersion,
} from './remote-issue.model';

// BT Issues (e.g. Mantis)
export interface BTIssue extends RemoteIssue {
  project: BTProject;
  reporter: RemoteUser;
  createdOn: Date;
  status: RemoteStatus;
}

export interface BTProject extends RemoteProject {
  id: Identifier;
  name: string;
  priorities: RemotePriority[];
  versions: RemoteVersion[];
  users: RemoteUser[];
  categories: RemoteCategory[];
  defaultIssuePriority: RemotePriority;
}

export function isBTIssue(remoteIssue: RemoteIssue): remoteIssue is BTIssue {
  if (remoteIssue == null || typeof remoteIssue !== 'object') {
    return false;
  }

  return Object.prototype.hasOwnProperty.call(remoteIssue, 'reporter');
}
