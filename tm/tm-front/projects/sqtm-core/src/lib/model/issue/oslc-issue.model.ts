import { RemoteIssue } from './remote-issue.model';
import { AdvancedProject, FieldValue } from './advanced-issue.model';

// OSLC issues (e.g. RTC bugtracker plugin)

export interface OslcIssue extends RemoteIssue {
  fieldValues: { [fieldId: string]: FieldValue };
  project: AdvancedProject;
  id: string;
  btName: string;
  url: string;
  currentScheme: string;
  selectDialog: string;
  createDialog: string;
}

export function isOslcIssue(remoteIssue: RemoteIssue): remoteIssue is OslcIssue {
  if (remoteIssue == null || typeof remoteIssue !== 'object') {
    return false;
  }

  return (
    Object.prototype.hasOwnProperty.call(remoteIssue, 'createDialog') &&
    Object.prototype.hasOwnProperty.call(remoteIssue, 'selectDialog')
  );
}
