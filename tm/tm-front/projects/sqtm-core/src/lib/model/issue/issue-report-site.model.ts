// Maps to KnownIssuesGridResponseBuilder.KnownIssueReportSite server-side
export interface IssueReportSite {
  executionId: number;
  executionOrder: number; // 0-based order
  executionName: string;
  suiteNames: string[];
}
