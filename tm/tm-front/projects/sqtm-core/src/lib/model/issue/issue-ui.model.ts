import { IssueBindableEntity } from './issue-bindable-entity.model';
import { BugTrackerMode } from '../bugtracker/bug-tracker.model';

// Model used in lieu of @angular/common/http's HttpErrorResponse to avoid including Angular sources in models
export interface HttpErrorModel {
  error: any;
}

export class IssueUIModel {
  bugTrackerStatus: string;
  delete: string;
  entityType: IssueBindableEntity;
  oslc: boolean;
  panelStyle: string;
  projectId: number;
  projectName: string;
  hasError: boolean;
  error: HttpErrorModel;
  modelLoaded: boolean;
  bugTrackerMode: BugTrackerMode;
  activated: boolean;
}
