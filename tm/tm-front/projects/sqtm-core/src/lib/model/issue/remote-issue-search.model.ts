import { Field } from './advanced-issue.model';

// Model received from server to display remote issue search form
export interface RemoteIssueSearchForm {
  fields: Field[];
  focusedField: string;
  fieldsToRetain?: string[];
}

// Model sent to server when looking for a RemoteIssue
export interface RemoteIssueSearchTerms {
  [searchTermId: string]: any;
}
