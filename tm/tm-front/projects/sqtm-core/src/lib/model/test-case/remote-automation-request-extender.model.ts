export class RemoteAutomationRequestExtender {
  id: number;
  serverId: number;
  remoteStatus: string;
  remoteIssueKey: string;
  remoteRequestUrl: string;
  remoteAssignedTo: string;
  sentValueForSync: string;
  synchronizableIssueStatus: string;
  lastSyncDateSquash: Date;
}
