import { AutomationRequest } from './automation-request-model';
import { Parameter } from './parameter.model';
import { Dataset } from './dataset.model';
import { RequirementVersionCoverage } from './requirement-version-coverage-model';
import { DatasetParamValue } from './dataset-param-value';
import { TestStepModel } from './test-step.model';
import { Execution } from './execution.model';
import { Milestone } from '../milestone/milestone.model';
import {
  ExecutionStatusKeys,
  TestCaseImportanceKeys,
  TestCaseStatusKeys,
} from '../level-enums/level-enum';
import { CalledTestCase } from './called-test-case.model';
import { TestAutomation } from '../test-automation/test-automation.model';
import { TestCaseKindKeys } from '../level-enums/test-case/test-case-kind';
import { EntityModel } from '../entity/entity.model';

export const MAX_TEST_CASE_NAME_LENGTH = 255;

export interface TestCaseModel extends EntityModel {
  name: string;
  reference: string;
  importance: TestCaseImportanceKeys;
  description: string;
  status: TestCaseStatusKeys;
  nature: number;
  type: number;
  importanceAuto: boolean;
  automatable: string;
  prerequisite: string;
  testSteps: TestStepModel[];
  milestones: Milestone[];
  automationRequest: AutomationRequest;
  parameters: Parameter[];
  datasets: Dataset[];
  datasetParamValues: DatasetParamValue[];
  coverages: RequirementVersionCoverage[];
  uuid: string;
  kind: TestCaseKindKeys;
  executions: Execution[];
  nbIssues: number;
  calledTestCases: CalledTestCase[];
  lastModifiedOn: string;
  lastModifiedBy: string;
  createdBy: string;
  createdOn: string;
  lastExecutionStatus: ExecutionStatusKeys;
  nbExecutions: number;
  nbSessions: number;
  automatedTest: TestAutomation;
  script: string;
  charter: string;
  sessionDuration: number;
  actionWordLibraryActive: boolean;
  automatedTestTechnology?: number;
  automatedTestReference?: string;
  scmRepositoryId?: number;
  configuredRemoteFinalStatus?: string;
  draftedByAi: boolean;
}
