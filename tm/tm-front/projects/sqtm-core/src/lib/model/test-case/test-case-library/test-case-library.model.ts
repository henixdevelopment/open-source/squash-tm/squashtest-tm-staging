import { TestCaseStatistics } from '../test-case-statistics.model';
import { CustomDashboardModel } from '../../custom-report/custom-dashboard.model';
import { EntityModel } from '../../entity/entity.model';

export interface TestCaseLibraryModel extends EntityModel {
  name: string;
  description: string;
  statistics: TestCaseStatistics;
  dashboard: CustomDashboardModel;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
}
