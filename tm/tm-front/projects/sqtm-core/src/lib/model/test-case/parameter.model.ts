export class Parameter {
  id: number;
  name: string;
  description: string;
  sourceTestCaseId: number;
  sourceTestCaseReference: string;
  sourceTestCaseName: string;
  sourceTestCaseProjectName: string;
  order: number;
}
