import { Identifier } from '../entity.model';

export interface TestCaseSearchMilestoneMassEdit {
  milestoneIds: number[];
  checkedIds: number[];
  samePerimeter: boolean;
  testCaseIds: Identifier[];
}
