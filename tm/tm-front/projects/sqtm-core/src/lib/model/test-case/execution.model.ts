export class Execution {
  id: number;
  projectName: string;
  campaignName: string;
  iterationName: string;
  testSuiteName: string;
  executionOrder: number;
  executionMode: string;
  datasetsName: string;
  executionStatus: string;
  lastExecutedBy: string;
  lastExecutedOn: string;
  nbIssues: number;
}
