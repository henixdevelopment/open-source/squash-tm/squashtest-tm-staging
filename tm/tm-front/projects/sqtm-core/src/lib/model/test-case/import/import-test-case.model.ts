export interface TestCaseImportFormatFailure {
  missingMandatoryColumns: string[];
  duplicateColumns: string[];
  actionValidationError: any;
}

export class TestCaseImportLog {
  testCaseSuccesses: number;
  testCaseWarnings: number;
  testCaseFailures: number;
  testStepSuccesses: number;
  testStepWarnings: number;
  testStepFailures: number;
  parameterSuccesses: number;
  parameterWarnings: number;
  parameterFailures: number;
  datasetSuccesses: number;
  datasetWarnings: number;
  datasetFailures: number;
  coverageSuccesses: number;
  coverageWarnings: number;
  coverageFailures: number;
  reportUrl: string;
}

export class TestCaseXlsReport {
  templateOk: TestCaseImportLog;
  importFormatFailure: TestCaseImportFormatFailure;
}
