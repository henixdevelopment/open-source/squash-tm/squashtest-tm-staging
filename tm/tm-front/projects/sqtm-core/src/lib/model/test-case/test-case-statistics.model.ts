export interface TestCaseStatistics {
  boundRequirementsStatistics: TestCaseBoundRequirementsStatistics;
  importanceStatistics: TestCaseImportanceStatistics;
  statusesStatistics: TestCaseStatusesStatistics;
  sizeStatistics: TestCaseSizeStatistics;
  selectedIds: number[];
  generatedOn?: Date;
}

export interface TestCaseBoundRequirementsStatistics {
  zeroRequirements: number;
  oneRequirement: number;
  manyRequirements: number;
}

export interface TestCaseImportanceStatistics {
  veryHigh: number;
  high: number;
  medium: number;
  low: number;
}

export interface TestCaseStatusesStatistics {
  workInProgress: number;
  underReview: number;
  approved: number;
  obsolete: number;
  toBeUpdated: number;
}

export interface TestCaseSizeStatistics {
  zeroSteps: number;
  between0And10Steps: number;
  between11And20Steps: number;
  above20Steps: number;
}
