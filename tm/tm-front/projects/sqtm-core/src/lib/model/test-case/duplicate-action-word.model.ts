import { Identifier } from '../entity.model';

export class DuplicateActionWord {
  projectName: string;
  actionWordId: Identifier;
}
