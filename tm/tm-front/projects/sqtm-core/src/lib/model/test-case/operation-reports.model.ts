import { TestStepModel } from './test-step.model';
import { TestCaseImportanceKeys } from '../level-enums/level-enum';
import { Parameter } from './parameter.model';
import { Dataset } from './dataset.model';
import { DatasetParamValue } from './dataset-param-value';

export interface TestCaseParameterOperationReport {
  parameters: Parameter[];
  dataSets: Dataset[];
  paramValues: DatasetParamValue[];
}

export interface TestStepActionWordOperationReport {
  action: string;
  styledAction: string;
  actionWordId: number;
  paramOperationReport: TestCaseParameterOperationReport;
}

export class PasteTestStepOperationReport {
  testSteps: TestStepModel[];
  testCaseImportance: TestCaseImportanceKeys;
  operationReport: TestCaseParameterOperationReport;
}

export class AddTestStepOperationReport {
  testStep: TestStepModel;
  operationReport: TestCaseParameterOperationReport;
}

export class DeleteTestStepOperationReport {
  testStepsToDelete: number[];
  operationReport: TestCaseParameterOperationReport;
}
