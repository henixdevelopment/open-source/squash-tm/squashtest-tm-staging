import { ApiDocumentationLink } from './api-documentation-link';
import { AuthenticatedUser } from '../user/authenticated-user.model';
import { CustomField } from '../customfield/customfield.model';
import { TestAutomationServerKind } from '../test-automation/test-automation-server.model';
import { TemplateConfigurablePlugin } from '../plugin/template-configurable-plugin.model';
import { BugTrackerReferentialDto } from '../bugtracker/bug-tracker.model';
import { LicenseInformationModel } from './license-information.model';
import { GlobalConfigurationModel } from './global-configuration.model';
import { SynchronizationPlugin } from '../plugin/synchronization-plugin.model';
import { ScmServerKind } from '../scm-server/scm-server.model';

/**
 * Class representing the AdminReferentialData fetched from server.
 */
export class AdminReferentialDataModel {
  user: AuthenticatedUser;
  globalConfiguration: GlobalConfigurationModel;
  licenseInformation: LicenseInformationModel;
  customFields: CustomField[];
  availableTestAutomationServerKinds: TestAutomationServerKind[];
  availableScmServerKinds: ScmServerKind[];
  canManageLocalPassword: boolean;
  templateConfigurablePlugins: TemplateConfigurablePlugin[];
  bugTrackers: BugTrackerReferentialDto[];
  documentationLinks: ApiDocumentationLink[];
  callbackUrl: string;
  premiumPluginInstalled: boolean;
  ultimateLicenseAvailable: boolean;
  availableReportIds: string[];
  synchronizationPlugins: SynchronizationPlugin[];
  aiServers: [];
  jwtSecretDefined: boolean;
}
