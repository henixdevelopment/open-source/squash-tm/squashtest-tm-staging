export interface LicenseInformationModel {
  activatedUserExcess: string;
  pluginLicenseExpiration: string;
}
