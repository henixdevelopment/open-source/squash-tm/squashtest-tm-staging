import { AdminReferentialDataModel } from './admin-referential-data.model';
import { Project } from '../project/project.model';
import { InfoList } from '../infolist/infolist.model';
import { Milestone } from '../milestone/milestone.model';
import { TestAutomationServer } from '../test-automation/test-automation-server.model';
import { RequirementVersionLinkType } from '../requirement/requirement-version-link-type.model';
import { WorkspaceWizard } from '../workspace-wizard/workspace-wizard.model';
import { AutomatedTestTechnology } from '../automation/automated-test-technology.model';
import { ScmServer } from '../scm-server/scm-server.model';
import { ProjectPermission } from '../user/user.model';

/**
 * Class representing the ReferentialData fetched from server.
 */
export class ReferentialDataModel extends AdminReferentialDataModel {
  isAdmin: boolean;
  projects: Project[];
  infoLists: InfoList[];
  filteredProjectIds: number[];
  projectFilterStatus: boolean;
  milestones: Milestone[];
  automationServers: TestAutomationServer[];
  requirementVersionLinkTypes: RequirementVersionLinkType[];
  workspaceWizards: WorkspaceWizard[];
  automatedTestTechnologies: AutomatedTestTechnology[];
  scmServers: ScmServer[];
  premiumPluginInstalled: boolean;
  ultimateLicenseAvailable: boolean;
  projectPermissions: ProjectPermission[];
}
