export class GlobalConfigurationModel {
  milestoneFeatureEnabled: boolean;
  uploadFileExtensionWhitelist: string[];
  uploadFileSizeLimit: number;
  bannerMessage: string;
  searchActivationFeatureEnabled: boolean;
  unsafeAttachmentPreviewEnabled: boolean;
}
