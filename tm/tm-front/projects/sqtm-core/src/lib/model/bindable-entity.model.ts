import { CustomFieldBinding } from './customfield/custom-field-binding.model';

export enum BindableEntity {
  CAMPAIGN = 'CAMPAIGN',
  CAMPAIGN_FOLDER = 'CAMPAIGN_FOLDER',
  CUSTOM_REPORT_FOLDER = 'CUSTOM_REPORT_FOLDER',
  EXECUTION = 'EXECUTION',
  EXECUTION_STEP = 'EXECUTION_STEP',
  ITERATION = 'ITERATION',
  REQUIREMENT_FOLDER = 'REQUIREMENT_FOLDER',
  REQUIREMENT_VERSION = 'REQUIREMENT_VERSION',
  SPRINT = 'SPRINT',
  SPRINT_GROUP = 'SPRINT_GROUP',
  TESTCASE_FOLDER = 'TESTCASE_FOLDER',
  TEST_CASE = 'TEST_CASE',
  TEST_STEP = 'TEST_STEP',
  TEST_SUITE = 'TEST_SUITE',
}

export type Bindings = { [id in BindableEntity]: CustomFieldBinding[] };
