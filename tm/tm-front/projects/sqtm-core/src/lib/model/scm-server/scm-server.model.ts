import {
  AuthenticationPolicy,
  AuthenticationProtocol,
} from '../third-party-server/authentication.model';
import { Credentials } from '../third-party-server/credentials.model';
import { I18nEnum } from '../level-enums/level-enum';

export interface ScmServer {
  serverId: number;
  name: string;
  url: string;
  kind: ScmServerKind;
  committerMail: string;
  credentialsNotShared: boolean;
  repositories: ScmRepository[];
}

export interface ScmRepository {
  scmRepositoryId: number;
  serverId: number;
  name: string;
  repositoryPath: string;
  workingFolderPath: string;
  workingBranch: string;
}

export interface AdminScmServer extends ScmServer {
  authPolicy: AuthenticationPolicy;
  authProtocol: AuthenticationProtocol;
  supportedAuthenticationProtocols: AuthenticationProtocol[];
  credentials?: Credentials;
  createdBy: string;
  createdOn: string;
  lastModifiedBy?: string;
  lastModifiedOn?: string;
  description?: string;
}

export function buildScmRepositoryUrl(scmServer: ScmServer, scmRepo: ScmRepository): string {
  let url = scmServer.url;
  if (!url.endsWith('/')) {
    url = `${url}/`;
  }

  url = `${url}${scmRepo.name} (${scmRepo.workingBranch})`;
  return url;
}

export enum ScmServerKind {
  git = 'git',
}

export const ScmServerKindI18nEnum: I18nEnum<ScmServerKind> = {
  [ScmServerKind.git]: {
    id: ScmServerKind.git,
    i18nKey: 'sqtm-core.entity.scm-server.kind.git',
  },
};

export function getScmServerKindI18nKey(kind: ScmServerKind): string {
  if (Object.keys(ScmServerKindI18nEnum).includes(kind)) {
    return ScmServerKindI18nEnum[kind].i18nKey;
  }

  return kind;
}
