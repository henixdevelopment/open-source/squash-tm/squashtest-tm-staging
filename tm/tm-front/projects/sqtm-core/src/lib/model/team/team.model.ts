import { ProjectPermission } from '../user/user.model';
import { Profile } from '../profile/profile.model';

export interface Team {
  id: number;
  name: string;
  description: string;
  createdOn: string;
  createdBy: string;
  lastModifiedBy: string;
  lastModifiedOn: string;
  projectPermissions: ProjectPermission[];
  profiles: Profile[];
  members: Member[];
}

export interface Member {
  partyId: number;
  active: boolean;
  firstName: string;
  lastName: string;
  login: string;
  fullName: string;
}
