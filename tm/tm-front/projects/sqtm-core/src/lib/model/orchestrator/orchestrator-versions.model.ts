export interface OrchestratorConfVersions {
  name: string;
  internalComponents: NameAndVersion[];
  baseImages: NameAndVersion[];
  unreachable: boolean;
}

export interface NameAndVersion {
  name: string;
  version: string;
}
