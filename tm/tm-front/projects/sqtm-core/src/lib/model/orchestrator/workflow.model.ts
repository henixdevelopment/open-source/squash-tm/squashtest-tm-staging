import { ExecutionStatusKeys } from '../level-enums/level-enum';

export interface Workflow {
  id: string;
  name: string;
  namespace: string;
  createdOn: string;
  status: ExecutionStatusKeys;
  completedOn: string;
}
