export interface OrchestratorResponse<T> {
  response: T;
  reachable: boolean;
}
