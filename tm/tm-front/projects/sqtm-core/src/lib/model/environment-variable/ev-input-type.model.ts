export enum EvInputType {
  PLAIN_TEXT = 'PLAIN_TEXT',
  INTERPRETED_TEXT = 'INTERPRETED_TEXT',
  DROPDOWN_LIST = 'DROPDOWN_LIST',
}

export function getTextFieldInputTypes(): EvInputType[] {
  return [EvInputType.PLAIN_TEXT, EvInputType.INTERPRETED_TEXT];
}
export function getEvInputTypeI18n(inputTypeKey: EvInputType): string {
  return 'sqtm-core.entity.environment-variable.' + inputTypeKey;
}
export function getEvInputTypeI18nCustomPatternErrorMessage(inputTypeKey: EvInputType): string {
  if (inputTypeKey === EvInputType.INTERPRETED_TEXT) {
    return 'sqtm-core.entity.environment-variable.invalidValuePattern.interpreted';
  } else {
    return 'sqtm-core.entity.environment-variable.invalidValuePattern.simple';
  }
}
