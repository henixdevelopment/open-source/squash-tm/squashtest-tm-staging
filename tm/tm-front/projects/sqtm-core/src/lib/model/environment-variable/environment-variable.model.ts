import { EnvironmentVariableOption } from './environment-variable-option.model';
import { EvInputType } from './ev-input-type.model';

export class EnvironmentVariable {
  id: number;
  name: string;
  inputType: EvInputType;
  boundToServer: boolean;
  options: EnvironmentVariableOption[] = [];
}

export class BoundEnvironmentVariable extends EnvironmentVariable {
  value: string;
}
