export class EnvironmentVariableOption {
  evId: number;
  label: string;
  position: number;
}

export function isOptionPatternValid(option: string) {
  return option.match(/^[\x20-\x7E]+$/);
}
