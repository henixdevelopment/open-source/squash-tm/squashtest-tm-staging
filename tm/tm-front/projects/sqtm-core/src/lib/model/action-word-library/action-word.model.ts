import { EntityModel } from '../entity/entity.model';

export interface ActionWordModel extends EntityModel {
  name: string;
  description: string;
}
