import { EntityModel } from '../entity/entity.model';

export interface ActionWordLibraryModel extends EntityModel {
  name: string;
  description: string;
}
