export interface ProjectPlugin {
  index: number;
  id: string;
  enabled: boolean;
  type: string;
  name: string;
  status: string;
  configUrl: string;
  hasValidConfiguration: boolean;
}

export enum AutomationWorkflowTypes {
  NONE = 'NONE',
  NATIVE = 'NATIVE',
  NATIVE_SIMPLIFIED = 'NATIVE_SIMPLIFIED',
  REMOTE_WORKFLOW = 'REMOTE_WORKFLOW',
}

export enum PluginType {
  AUTOMATION = 'AUTOMATION',
}

export enum PluginId {
  CAMPAIGN_ASSISTANT = 'squash.tm.wizard.campaignassistant',
  RESULT_PUBLISHER = 'squash.tm.plugin.resultpublisher',
  WORKFLOW_AUTOMATION_JIRA = 'henix.plugin.automation.workflow.automjira',
}
