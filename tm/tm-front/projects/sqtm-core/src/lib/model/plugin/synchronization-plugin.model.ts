export interface SynchronizationPlugin {
  id: string;
  name: string;
}
