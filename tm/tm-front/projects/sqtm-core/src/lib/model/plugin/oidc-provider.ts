export enum OidcProvidersWithLogo {
  GITHUB = 'github',
  GITLAB = 'gitlab',
  GOOGLE = 'google',
  MICROSOFT_AZURE = 'microsoft_azure',
  OKTA = 'okta',
}
