import { EntityReference } from '../entity.model';
import { FilterValueModel } from '../filter/filter-value.model';

export interface AutomatedSuitePreview {
  manualServerSelection: boolean;
  specification: AutomatedSuiteCreationSpecification;
  projects: TestAutomationProjectPreview[];
  squashAutomProjects: SquashAutomProjectPreview[];
}

export interface AutomatedSuiteCreationSpecification {
  context: EntityReference;
  testPlanSubsetIds: number[];
  iterationId?: number;
  filterValues?: FilterValueModel[];
  executionConfigurations?: AutomatedSuiteExecutionConfiguration[];
  squashAutomExecutionConfigurations?: SquashAutomExecutionConfiguration[];
}

export interface TestAutomationProjectPreview {
  projectId: number;
  label: string;
  server: string;
  nodes: string[];
  testCount: number;
}

export interface SquashAutomProjectPreview {
  projectId: number;
  projectName: string;
  serverId: number;
  serverName: string;
  testCases: TestCasePreviewInfo[];
}

export interface AutomatedSuiteExecutionConfiguration {
  projectId: number;
  node: string;
}

export interface SquashAutomExecutionConfiguration {
  projectId: number;
  namespaces: string[];
  environmentTags: string[];
  environmentVariables: { [key: string]: EnvironmentVariableValue };
}

export interface TestCasePreviewInfo {
  reference: string;
  name: string;
  dataset: string;
  importance: string;
}

export interface EnvironmentVariableValue {
  value: string;
  verbatim: boolean;
}
