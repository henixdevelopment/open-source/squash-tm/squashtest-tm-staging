import { ExecutionStatusKeys } from '../level-enums/level-enum';

export interface AutomatedSuiteOverview {
  suiteId: string;
  tfExecutions: AutomatedExecutionOverview[];
  automExecutions: AutomatedItemOverview[];
  tfPercentage: number;
  automTerminatedCount: number;
  errorMessage: string;
  workflowsUUIDs: string[];
}

export interface AutomatedExecutionOverview {
  id: number;
  name: string;
  status: ExecutionStatusKeys;
  node: string;
  automatedProject: string;
}

export interface AutomatedItemOverview {
  id: number;
  name: string;
  datasetLabel: string;
  status: ExecutionStatusKeys;
  automatedServerName: string;
}
