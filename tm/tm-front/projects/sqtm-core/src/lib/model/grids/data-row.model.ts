import { Identifier } from '../entity.model';
import { SimplePermissions } from '../permissions/simple-permissions';

export interface DataRowModel<T = any> {
  id: Identifier;
  readonly type?;
  children?: Identifier[];
  data: T;
  simplePermissions?: SimplePermissions;
  projectId: number;
  readonly allowedChildren?: any[];
  allowMoves?: boolean;
  parentRowId?: Identifier;
  state?: DataRowOpenState;
  selectable?: boolean;
}

export enum SquashTmDataRowType {
  Generic = 'Generic',
  // Tree node entities
  ActionWordLibrary = 'ActionWordLibrary',
  ActionWord = 'ActionWord',
  AutomatedSuite = 'AutomatedSuite',
  Campaign = 'Campaign',
  CampaignFolder = 'CampaignFolder',
  CampaignLibrary = 'CampaignLibrary',
  ChartDefinition = 'ChartDefinition',
  CustomReportFolder = 'CustomReportFolder',
  CustomReportLibrary = 'CustomReportLibrary',
  CustomReportDashboard = 'CustomReportDashboard',
  CustomReportCustomExport = 'CustomReportCustomExport',
  Execution = 'Execution',
  HighLevelRequirement = 'HighLevelRequirement',
  Iteration = 'Iteration',
  ReportDefinition = 'ReportDefinition',
  Requirement = 'Requirement',
  RequirementFolder = 'RequirementFolder',
  RequirementLibrary = 'RequirementLibrary',
  Sprint = 'Sprint',
  SprintGroup = 'SprintGroup',
  TestCase = 'TestCase',
  TestCaseFolder = 'TestCaseFolder',
  TestCaseLibrary = 'TestCaseLibrary',
  TestSuite = 'TestSuite',
  // Non tree node entities
  AutomationRequest = 'AutomationRequest',
  CampaignTestPlanItem = 'CampaignTestPlanItem',
  Directory = 'Directory',
  File = 'File',
  IterationTestPlanItem = 'IterationTestPlanItem',
  RequirementVersion = 'RequirementVersion',
  TAFolder = 'TAFolder',
  TAProject = 'TAProject',
  TATest = 'TATest',
  TestPlanItem = 'TestPlanItem',
}

export enum DataRowOpenState {
  open = 'open',
  closed = 'closed',
  leaf = 'leaf',
}
