import { DataRowModel } from './data-row.model';

export interface GridResponse<T = any> {
  count?: number;
  idAttribute?: string;
  dataRows: DataRowModel<T>[];
  activeColumnIds?: string[];
  page?: number;
}
