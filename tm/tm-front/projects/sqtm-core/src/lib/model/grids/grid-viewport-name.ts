export type RenderedGridViewportName = 'leftViewport' | 'mainViewport' | 'rightViewport';
export type GridViewportName = RenderedGridViewportName | 'headerMainViewport';
