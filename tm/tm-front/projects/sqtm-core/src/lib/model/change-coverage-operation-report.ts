import { RequirementVersionCoverage } from './test-case/requirement-version-coverage-model';
import { VerifyingTestCase } from './requirement/verifying-test-case';
import { RequirementVersionLink } from './requirement/requirement-version.link';
import { LinkedLowLevelRequirement } from './requirement/requirement-version.model';
import { RequirementVersionStatsBundle } from './requirement/requirement-version-stats-bundle.model';
import { SprintReqVersionModel } from './campaign/sprint-req-version-model';

export interface ChangeOperationReport {
  summary: SummaryExceptions;
}

export interface ChangeItemTestPlanIdsReport extends ChangeOperationReport {
  itemTestPlanIds: number[];
}

export interface ChangeCoverageOperationReport extends ChangeOperationReport {
  coverages: RequirementVersionCoverage[];
}

export interface ChangeVerifyingTestCaseOperationReport extends ChangeOperationReport {
  verifyingTestCases: VerifyingTestCase[];
  requirementStats: RequirementVersionStatsBundle;
  nbIssues: number;
}

export interface ChangeLinkedRequirementOperationReport {
  summary: RequirementLinksSummaryExceptions;
  requirementVersionLinks: RequirementVersionLink[];
}

export interface BindRequirementToHighLevelRequirementOperationReport {
  linkedLowLevelRequirements: LinkedLowLevelRequirement[];
  verifyingTestCases: VerifyingTestCase[];
  requirementStats: RequirementVersionStatsBundle;
  nbIssues: number;
  summary: BindRequirementToHighLevelRequirementExceptions;
}

class BindRequirementToHighLevelRequirementExceptions {
  requirementWithNotLinkableStatus: string[];
  alreadyLinked: string[];
  alreadyLinkedToAnotherHighLevelRequirement: string[];
  highLevelRequirementsInSelection: string[];
  childRequirementsInSelection: string[];
}

export interface BindReqVersionToSprintOperationReport {
  linkedReqVersions: SprintReqVersionModel[];
  summary: BindReqVersionToSprintExceptions;
}

class BindReqVersionToSprintExceptions {
  reqVersionsAlreadyLinkedToSprint: string[];
  highLevelReqVersionsInSelection: string[];
  obsoleteReqVersionsInSelection: string[];
}

export class SummaryExceptions {
  alreadyVerifiedRejections: any;
  notLinkableRejections: any;
  noVerifiableVersionRejections: any;
}

export class RequirementLinksSummaryExceptions {
  notLinkableRejections: boolean;
  alreadyLinkedRejections: boolean;
  sameRequirementRejections: boolean;
}
