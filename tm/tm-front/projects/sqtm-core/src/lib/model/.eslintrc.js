module.exports = {
  "root": false,
  "extends": [
    "../../../.eslintrc.js"
  ],
  "rules": {
    "no-restricted-imports": ["error", {
      "paths": ["@ngx-translate", "@angular", "sqtm-core"],
      "patterns": [{
        "group": ["@ngx-translate/*", "@angular/*"],
        "message": "sqtm-core models should be self-contained."
      }]
    }],
  }
}
