import { LoggerService } from './logger.service';
import {
  DebugLoggingLevel,
  ErrorLoggingLevel,
  InfoLoggingLevel,
  LoggingConfiguration,
  LoggingLevel,
  TraceLoggingLevel,
  WarningLoggingLevel,
} from './logger.configuration';
import { NamespaceTree } from './namespace-tree';
import Spy = jasmine.Spy;

describe('LoggerService', () => {
  it('should load simple configuration', () => {
    const service = LoggerService.getLoggerService();
    expect(service).toBeTruthy();
    const nsTree: NamespaceTree = service['namespaceTree'];
    const spy: Spy = spyOn(nsTree, 'addPath');

    service.loadConfiguration({
      loggers: {
        default: 'error',
        namespaces: {
          'sqtm-core': 'debug',
          'sqtm-app': 'info',
        },
      },
    });

    expect(spy).toHaveBeenCalledTimes(2);
  });

  describe('Logging level is enabled', () => {
    const configuration: LoggingConfiguration = {
      loggers: {
        default: 'info',
        namespaces: {
          'sqtm-core': 'debug',
          'sqtm-core.core.referential': 'trace',
          'sqtm-core.core.model': 'warn',
          'sqtm-app': 'warn',
        },
      },
    };

    interface DataType {
      namespace: string;
      requiredLevel: LoggingLevel;
      expected: boolean;
    }

    const dataSets: DataType[] = [
      {
        namespace: 'sqtm-app',
        requiredLevel: new DebugLoggingLevel(),
        expected: false,
      },
      {
        namespace: 'sqtm-app',
        requiredLevel: new WarningLoggingLevel(),
        expected: true,
      },
      {
        namespace: 'sqtm-app',
        requiredLevel: new ErrorLoggingLevel(),
        expected: true,
      },
      {
        namespace: 'sqtm-core.core.referential',
        requiredLevel: new TraceLoggingLevel(),
        expected: true,
      },
      {
        namespace: 'sqtm-core.core',
        requiredLevel: new TraceLoggingLevel(),
        expected: false,
      },
      {
        namespace: 'sqtm-core.ui.grid.GridService',
        requiredLevel: new TraceLoggingLevel(),
        expected: false,
      },
      {
        namespace: 'sqtm-core.ui.grid.GridService',
        requiredLevel: new DebugLoggingLevel(),
        expected: true,
      },
      {
        namespace: 'sqtm-plugin-jira',
        requiredLevel: new DebugLoggingLevel(),
        expected: false,
      },
      {
        namespace: 'sqtm-plugin-jira',
        requiredLevel: new InfoLoggingLevel(),
        expected: true,
      },
    ];
    dataSets.forEach((data, index) => runTest(data, index));

    function runTest(data: DataType, index: number) {
      it(`Dataset ${index} - It should say ${data.expected} when asking ${data.requiredLevel.name} for ns ${data.namespace}`, () => {
        const service = LoggerService.getLoggerService();
        service.loadConfiguration(configuration);
        const result = service.isEnabled(data.namespace, data.requiredLevel);
        expect(result).toEqual(data.expected);
      });
    }
  });
});
