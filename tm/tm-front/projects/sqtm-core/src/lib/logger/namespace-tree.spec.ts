import {
  DebugLoggingLevel,
  InfoLoggingLevel,
  InternalLoggingLevelKey,
  NoneLoggingLevel,
  TraceLoggingLevel,
  UnsetLoggingLevel,
  WarningLoggingLevel,
} from './logger.configuration';
import { NamespaceTree, NamespaceTreeElement } from './namespace-tree';

describe('NamespaceTree', () => {
  it('should load simple configuration', () => {
    const nsTree = new NamespaceTree();

    nsTree.addPath('sqtm-core', { level: new DebugLoggingLevel() });
    nsTree.addPath('sqtm-app', { level: new InfoLoggingLevel() });

    expect(nsTree['root'].length === 2);
    const nsSqtmCore = nsTree['root'][0];
    expect(nsSqtmCore.name).toEqual('sqtm-core');
    expect(nsSqtmCore.configuration.level.name).toEqual('debug');
    const nsSqtmApp = nsTree['root'][1];
    expect(nsSqtmApp.name).toEqual('sqtm-app');
    expect(nsSqtmApp.configuration.level.name).toEqual('info');
  });

  it('should load series of path configuration', () => {
    const nsTree: NamespaceTree = new NamespaceTree();
    nsTree.addPath('sqtm-core.core.referential.ReferentialDataService', {
      level: new DebugLoggingLevel(),
    });
    expect(nsTree['root'].length === 1);
    const nsSqtmCore = nsTree['root'][0];
    expect(nsSqtmCore.name).toEqual('sqtm-core');
    expect(nsSqtmCore.configuration.level.name).toEqual('unset');
    expect(nsSqtmCore.children.length).toEqual(1);

    const nsCore = nsSqtmCore.children[0];
    expect(nsCore.name).toEqual('core');
    expect(nsCore.configuration.level.name).toEqual('unset');
    expect(nsCore.children.length).toEqual(1);

    const nsReferential = nsCore.children[0];
    expect(nsReferential.name).toEqual('referential');
    expect(nsReferential.configuration.level.name).toEqual('unset');
    expect(nsReferential.children.length).toEqual(1);

    const nsReferentialDataService = nsReferential.children[0];
    expect(nsReferentialDataService.name).toEqual('ReferentialDataService');
    expect(nsReferentialDataService.configuration.level.name).toEqual('debug');
    expect(nsReferentialDataService.children.length).toEqual(0);

    nsTree.addPath('sqtm-core.core.referential', { level: new InfoLoggingLevel() });
    expect(nsSqtmCore.name).toEqual('sqtm-core');
    expect(nsSqtmCore.configuration.level.name).toEqual('unset');
    expect(nsSqtmCore.children.length).toEqual(1);

    expect(nsCore.name).toEqual('core');
    expect(nsCore.configuration.level.name).toEqual('unset');
    expect(nsCore.children.length).toEqual(1);

    expect(nsReferential.name).toEqual('referential');
    expect(nsReferential.configuration.level.name).toEqual('info');
    expect(nsReferential.children.length).toEqual(1);

    expect(nsReferentialDataService.name).toEqual('ReferentialDataService');
    expect(nsReferentialDataService.configuration.level.name).toEqual('debug');
    expect(nsReferentialDataService.children.length).toEqual(0);

    nsTree.addPath('sqtm-core.core.referential.ReferentialDataService', {
      level: new TraceLoggingLevel(),
    });
    expect(nsReferentialDataService.name).toEqual('ReferentialDataService');
    expect(nsReferentialDataService.configuration.level.name).toEqual('trace');
    expect(nsReferentialDataService.children.length).toEqual(0);

    nsTree.addPath('sqtm-core.core.entity.EntityViewService', { level: new WarningLoggingLevel() });
    expect(nsSqtmCore.name).toEqual('sqtm-core');
    expect(nsSqtmCore.configuration.level.name).toEqual('unset');
    expect(nsSqtmCore.children.length).toEqual(1);

    expect(nsCore.name).toEqual('core');
    expect(nsCore.configuration.level.name).toEqual('unset');
    expect(nsCore.children.length).toEqual(2);

    const nsEntity = nsCore.children[1];
    expect(nsEntity.name).toEqual('entity');
    expect(nsEntity.configuration.level.name).toEqual('unset');
    expect(nsEntity.children.length).toEqual(1);

    const nsEntityViewService = nsEntity.children[0];
    expect(nsEntityViewService.name).toEqual('EntityViewService');
    expect(nsEntityViewService.configuration.level.name).toEqual('warn');
    expect(nsEntityViewService.children.length).toEqual(0);
  });

  it('should find various path', () => {
    const nsReferentialDataService = new NamespaceTreeElement('ReferentialDataService', {
      level: new InfoLoggingLevel(),
    });
    const nsReferential = new NamespaceTreeElement('referential', {
      level: new UnsetLoggingLevel(),
    });
    const nsEntityViewService = new NamespaceTreeElement('EntityViewService', {
      level: new UnsetLoggingLevel(),
    });
    const nsEntity = new NamespaceTreeElement('entity', { level: new UnsetLoggingLevel() });
    const nsCore = new NamespaceTreeElement('core', { level: new UnsetLoggingLevel() });
    const nsSqtmCore = new NamespaceTreeElement('sqtm-core', { level: new UnsetLoggingLevel() });
    const nsSqtmApp = new NamespaceTreeElement('sqtm-app', { level: new UnsetLoggingLevel() });

    nsSqtmCore.children = [nsCore];
    nsCore.children = [nsReferential, nsEntity];
    nsReferential.children = [nsReferentialDataService];
    nsEntity.children = [nsEntityViewService];

    const namespaceTree = new NamespaceTree();
    namespaceTree['root'] = [nsSqtmCore, nsSqtmApp];
    const nopePath = namespaceTree.getPath('nope');
    expect(nopePath.length).toEqual(0);

    const sqtmCorePath = namespaceTree.getPath('sqtm-core');
    expect(sqtmCorePath.length).toEqual(1);
    expect(sqtmCorePath[0]).toBe(nsSqtmCore);

    const referentialDataServicePath = namespaceTree.getPath(
      'sqtm-core.core.referential.ReferentialDataService',
    );
    expect(referentialDataServicePath.length).toEqual(4);
    expect(referentialDataServicePath[0]).toBe(nsSqtmCore);
    expect(referentialDataServicePath[1]).toBe(nsCore);
    expect(referentialDataServicePath[2]).toBe(nsReferential);
    expect(referentialDataServicePath[3]).toBe(nsReferentialDataService);

    const entityViewServicePath = namespaceTree.getPath('sqtm-core.core.entity.EntityViewService');
    expect(entityViewServicePath.length).toEqual(4);
    expect(entityViewServicePath[0]).toBe(nsSqtmCore);
    expect(entityViewServicePath[1]).toBe(nsCore);
    expect(entityViewServicePath[2]).toBe(nsEntity);
    expect(entityViewServicePath[3]).toBe(nsEntityViewService);

    const interceptorsPath = namespaceTree.getPath('sqtm-core.core.interceptors');
    expect(interceptorsPath.length).toEqual(2);
    expect(interceptorsPath[0]).toBe(nsSqtmCore);
    expect(interceptorsPath[1]).toBe(nsCore);

    const incorrectPath = namespaceTree.getPath('sqtm-core.core.entity.ReferentialDataService');
    expect(incorrectPath.length).toEqual(3);
    expect(incorrectPath[0]).toBe(nsSqtmCore);
    expect(incorrectPath[1]).toBe(nsCore);
    expect(incorrectPath[2]).toBe(nsEntity);
  });

  describe('should reduce logging level', () => {
    const nsReferentialDataService = new NamespaceTreeElement('ReferentialDataService', {
      level: new NoneLoggingLevel(),
    });
    const nsReferential = new NamespaceTreeElement('referential', {
      level: new DebugLoggingLevel(),
    });
    const nsEntityViewService = new NamespaceTreeElement('EntityViewService', {
      level: new DebugLoggingLevel(),
    });
    const nsEntity = new NamespaceTreeElement('entity', { level: new UnsetLoggingLevel() });
    const nsCore = new NamespaceTreeElement('core', { level: new UnsetLoggingLevel() });
    const nsSqtmCore = new NamespaceTreeElement('sqtm-core', { level: new UnsetLoggingLevel() });
    const _nsSqtmApp = new NamespaceTreeElement('sqtm-app', { level: new UnsetLoggingLevel() });

    const namespaceTree = new NamespaceTree();

    interface DataType {
      path: NamespaceTreeElement[];
      expectedLoggingLevel: InternalLoggingLevelKey;
    }

    const dataSets: DataType[] = [
      {
        path: [],
        expectedLoggingLevel: 'unset',
      },
      {
        path: [nsSqtmCore, nsCore, nsEntity],
        expectedLoggingLevel: 'unset',
      },
      {
        path: [nsSqtmCore, nsCore, nsEntity, nsEntityViewService],
        expectedLoggingLevel: 'debug',
      },
      {
        path: [nsSqtmCore, nsCore, nsReferential, nsReferentialDataService],
        expectedLoggingLevel: 'none',
      },
    ];
    dataSets.forEach((data, index) => runTest(data, index));

    function runTest(data: DataType, index: number) {
      it(`Dataset ${index} - It should reduce logging level to ${data.expectedLoggingLevel}`, () => {
        const loggingLevel = namespaceTree.reduceLoggingLevel(data.path);
        expect(loggingLevel.name).toEqual(data.expectedLoggingLevel);
      });
    }
  });
});
