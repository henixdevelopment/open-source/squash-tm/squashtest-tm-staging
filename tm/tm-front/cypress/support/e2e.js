import './commands'
import addContext from "mochawesome/addContext";
import '@cypress/code-coverage/support'

function buildCustomLangMock(locale) {
  const url = `${Cypress.env('appBaseUrl')}/custom_translations_${locale}.json`;
  cy.intercept(url, { statusCode: 404, body: null });
}

function buildPingMock() {
  const url = `${Cypress.env('appBaseUrl')}/ping`;
  cy.intercept(url);
}

beforeEach(() => {
  cy.log('Executing global before each...');

  cy.log('Adding global custom lang files mocks');
  buildCustomLangMock('fr');
  buildCustomLangMock('en');

  cy.log('Adding global ping mock');
  buildPingMock();

  cy.log('Executed global before each.');

  // Stub window.open in order to prevent a new tab to open, because Cypress does not handle multiple windows
  // See https://github.com/cypress-io/cypress/issues/15654#issuecomment-813516654
  // and https://glebbahmutov.com/blog/stub-window-open/
  const windowOpenStub = cy.stub().as('open');
  cy.on('window:before:load', (win) => {
    cy.stub(win, 'open').callsFake(windowOpenStub);
  });
})

Cypress.on("test:after:run", (test, runnable) => {
  if (test.state === "failed") {
    const screenshot       =`./assets/${Cypress.spec.name}/${runnable.parent.title} -- ${test.title} (failed).png`;
    addContext({ test }, screenshot);
  }
});

