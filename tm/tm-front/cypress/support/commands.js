// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
import {LoginPage} from '../integration/page-objects/pages/login/login-page';

Cypress.Commands.add('clickVoid', () => {
  cy.get('body').click('bottomRight');
});

Cypress.Commands.add('hideMenu', (menuId) => {
  cy.get('body').then(body => {
    if (body.has('[data-test-menu-id]')) {
      body.find('[data-test-menu-id='+menuId+']').hide();
    }
  });
});

Cypress.Commands.add('clickOnOverlayBackdrop', () => {
  cy.get('.cdk-overlay-backdrop').click({multiple: true, force: true});
});

Cypress.Commands.add('removeNzTooltip', () => {
  cy.get('body').then(body => {
    if (body.has('.ant-tooltip')) {
      body.find('.ant-tooltip').hide();
    }
  })
});

Cypress.Commands.add('logInAs', (login, password) => {
  const loginPage = LoginPage.navigateTo();
  loginPage.assertExists();
  loginPage.login(login, password);
});

Cypress.Commands.add('logOut', () => {
  cy.goHome();
 cy.get('[data-test-navbar-field-id="user-menu"]').click();
 cy.get('[data-test-menu-item-id="logout"]');
});

Cypress.Commands.add('goHome', () => {
  cy.get('.home-workspace-link').click();
});

// Cypress.Commands.add('addAParameterToTestCase', (projectName, testCaseName, paramName) => {
//   const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
//     testCaseWorkspace.tree.findRowIdNoCallBack('NAME', testCaseName).then(testCaseId => {
//       const testCaseViewPage = testCaseWorkspace.tree.selectNode(testCaseId) as TestCaseViewPage;
//       const testCaseViewParametersPage = testCaseViewPage.clickAnchorLink('parameters') as TestCaseViewParametersPage;
//       const createParameterDialogElement = testCaseViewParametersPage.openAddParameterDialog();
//       createParameterDialogElement.fillName(paramName);
//       createParameterDialogElement.addParam();
//       testCaseViewParametersPage.checkExistingParam(paramName);
//     });
// });


//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
