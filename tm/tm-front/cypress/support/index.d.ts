/// <reference types="cypress" />

declare namespace Cypress {
  interface Chainable<Subject> {
    /**
     * Click on body at 0 0 coordinates, that should be an empty point.
     * Use it to close overlays.
     */
    clickVoid();

    /**
     * Click on overlay backdrop.
     * Use it to close overlays.
     */
    clickOnOverlayBackdrop();

    /**
     * Hide Menu using the data-test-menu-id attribute
     * @example
     * cy.hideMenu('create-menu')
     */
    hideMenu(menuId);

    /**
     * Try to remove all nzToolTips in the page, to prevent failure on clicks because a title is opened.
     * Use it if you have flaky tests because a nzToolTip refuse to disappear and thus make click impossible.
     * Try to avoid force on click if possible as it remove some useful checks performed by cypress before
     * interacting with elements.
     */
    removeNzTooltip();

    /**
     * LogInAs a specific user
     * @example
     * cy.logInAs('admin', 'admin')
     */
    logInAs(login, password);

    /**
     * logOut
     * @example
     * cy.logOut()
     */
    logOut();

    /**
     * visit Home Workspace
     * @example
     * cy.goHome
     */
    goHome();

    // /**
    //  * createTestCase with parameters
    //  * @example
    //  * cy.createTestCase('testCase1', 'ref1', 'desc', 'project1')
    //  */
    // createTestCase(name, reference, description, projectName);

    /**
     * add parameter to a specific test case
     * @example
     * cy.addAParameterToTestCase(projectName, testCaseName, paramName)
     */
    addAParameterToTestCase(projectName, testCaseName, paramName);
  }
}
