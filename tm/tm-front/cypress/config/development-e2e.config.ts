import { defineConfig } from 'cypress';

export default defineConfig({
  env: {
    backend: 'e2e',
    apiBaseUrl: '/squash/backend',
    appBaseUrl: '/squash',
    locale: 'FR',
  },
  video: false,
  projectId: 'uc8ymm',
  e2e: {
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents(on, config) {
      return require('../plugins/index.js')(on, config);
    },
    // If you need to make API calls, use the backend port (e.g., 8080).
    // If you don't need to make API calls, use port 4200 to avoid rebuilding the frontend every 2 minutes.
    baseUrl: 'http://localhost:8080/squash',
    specPattern: 'cypress/integration/scenarios/end-to-end/**/*.{js,jsx,ts,tsx}',
    screenshotsFolder: 'cypress/reports/end-to-end/assets',
    retries: {
      runMode: 2,
      openMode: 0,
    },
    reporter: 'cypress-multi-reporters',
    reporterOptions: {
      configFile: 'cypress/config/reporter-config.json',
    },
  },
});
