module.exports = {
  "root": false,
  "extends": [
    "../.eslintrc.js"
  ],
  "ignorePatterns": ["config/*"],
  "rules": {
    "no-restricted-imports": ["error", {
      "patterns": [
        {
          "group": ["*/sqtm-app/*"],
          "message": "sqtm-app cannot be imported into Cypress tests sources."
        },
      ],
    }],
    "import/no-restricted-paths": ["error",
      {
        "zones": [{
          "target": ["./cypress/**"],
          "from": "./projects/sqtm-core/**",
          "except": [
            "**/src/lib/model/**",
            "**/src/lib/shared/constants/grid/**",
          ],
          "message": "Only sqtm-core models and grid constants can be imported in Cypress sources."
        }]
      },
    ],
  },
}
