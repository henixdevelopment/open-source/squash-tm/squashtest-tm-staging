export function isBackEndMocked(): boolean {
  const envMocked = Cypress.env('backend');
  console.log(`envMocked : ${envMocked}`);
  return envMocked === 'mocked';
}

export function isPostgresql(config?: any): boolean {
  const profile = config ? getDatabaseProfile(config) : Cypress.env('profile');
  return profile === 'postgresql';
}

function getDatabaseProfile(config) {
  const profileName = config.env.profile;
  const accepted = ['mariadb', 'postgresql'];

  if (!accepted.includes(profileName)) {
    throw new Error(`Unknown profile ${profileName}. Accepted values are : ${accepted.join(', ')}`);
  }

  return profileName;
}
