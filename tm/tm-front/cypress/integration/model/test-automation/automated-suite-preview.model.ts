import {
  AutomatedSuitePreview,
  TestAutomationProjectPreview,
} from '../../../../projects/sqtm-core/src/lib/model/test-automation/automated-suite-preview.model';

export interface ExpectedAutomatedSuitePreview extends AutomatedSuitePreview {
  projects: ExpectedTestAutomationProjectPreview[];
}

export interface ExpectedTestAutomationProjectPreview extends TestAutomationProjectPreview {
  testList: string[];
}
