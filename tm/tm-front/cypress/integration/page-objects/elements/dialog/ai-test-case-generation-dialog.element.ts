import { BaseDialogElement } from './base-dialog.element';
import { EditableRichTextFieldElement } from '../forms/editable-rich-text-field.element';
import { TestCaseFolderPickerDialogElement } from './test-case-folder-picker-dialog.element';
import { selectByDataTestButtonId } from '../../../utils/basic-selectors';
import { HttpMock, HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { mockGridResponse } from '../../../data-mock/grid.data-mock';
import { DataRowModel } from '../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';

export class AiTestCaseGenerationDialogElement extends BaseDialogElement {
  readonly requirementDescription = new EditableRichTextFieldElement('requirement-description');
  readonly pickerDialog = new TestCaseFolderPickerDialogElement();

  constructor() {
    super('ai-generate-test-cases');
  }

  assertContainsDescription(expectedDescription: string) {
    this.requirementDescription.assertContainsText(expectedDescription);
  }

  pickDestination(mockDataRows: DataRowModel[], destinationRowId: string, mock?: HttpMock<any>) {
    const mockTreePicker = new HttpMockBuilder('test-case-tree')
      .post()
      .responseBody(mockGridResponse('id', mockDataRows))
      .build();

    this.findByElementId('pick-destination').click();
    this.pickerDialog.assertExists();
    mockTreePicker.wait();
    this.pickerDialog.selectDestination(destinationRowId);
    this.pickerDialog.confirm();
    if (mock) {
      mock.wait();
    }
  }

  assertDestinationPathDisplayed(expectedDestinationPath: string) {
    this.findByElementId('destination-path').should('contain', expectedDestinationPath);
  }

  generateTestCases(mockServerResponse: any) {
    const mock = new HttpMockBuilder('ai-test-case-generation/api-call')
      .post()
      .responseBody(mockServerResponse)
      .build();
    this.find(selectByDataTestButtonId('generate-test-cases')).click();
    mock.wait();
  }

  assertContainsTestCases(expectedTestCaseNames: string[]) {
    expectedTestCaseNames.forEach((testCaseName) => this.findByElementId(testCaseName));
  }

  collapseTestCaseAndAssertContains(
    name: string,
    description: string,
    prerequisites: string,
    testSteps: { index: number; action: string; expectedResult: string }[],
  ) {
    this.findByElementId(name)
      .find(selectByDataTestButtonId('collapse-generated-test-case'))
      .click();
    this.findByElementId('name').should('contain', name);
    this.findByElementId('description').should('contain', description);
    this.findByElementId('prerequisites').should('contain', prerequisites);

    testSteps.forEach((step) => {
      this.findByElementId(`step-number-${step.index}`).should('contain', step.index + 1);
      this.findByElementId(`step-action-${step.action}`).should('contain', step.action);
      this.findByElementId(`step-expected-result-${step.expectedResult}`).should(
        'contain',
        step.expectedResult,
      );
    });
  }

  assertErrorMessageWithNoFolderSelected() {
    this.findByElementId('no-destination-selected')
      .should('exist')
      .should('contain', `Pas d'emplacement sélectionné.`);
  }

  assertErrorMessageWithNoTestCaseSelected() {
    this.findByElementId('no-test-case-selected')
      .should('exist')
      .should('contain', `Aucun cas de test sélectionné.`);
  }

  selectTestCase(testCaseName: string) {
    this.find(selectByDataTestButtonId('select-test-case' + testCaseName)).click();
  }

  assertNoErrorMessage() {
    this.findByElementId('no-destination-selected').should('not.exist');
    this.findByElementId('no-test-case-selected').should('not.exist');
  }

  expandGeneratedTestCase() {
    this.find(selectByDataTestButtonId('collapse-generated-test-case')).first().click();
  }
  assertsNameFieldsAreNotEmpty() {
    this.findByElementId('name').should('not.be.empty');
    this.findByElementId('description').should('not.be.empty');
    this.findByElementId('prerequisites').should('not.be.empty');
    this.findByElementId('step-number-0').should('not.be.empty');
  }
}
