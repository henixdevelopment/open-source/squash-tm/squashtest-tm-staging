import { DeleteConfirmDialogElement } from './delete-confirm-dialog.element';

export class SimpleDeleteConfirmDialogElement extends DeleteConfirmDialogElement {
  constructor() {
    super('confirm-delete');
  }

  deleteForFailure(_response: any) {
    throw Error('Use specific dialog for delete success/failure');
  }

  deleteForSuccess(_response: any) {
    throw Error('Use specific dialog for delete success/failure');
  }
}
