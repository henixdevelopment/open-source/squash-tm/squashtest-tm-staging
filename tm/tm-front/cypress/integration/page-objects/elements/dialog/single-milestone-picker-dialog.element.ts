import { GridElement } from '../grid/grid.element';
import { BaseDialogElement } from './base-dialog.element';

export class SingleMilestonePickerDialogElement extends BaseDialogElement {
  public readonly grid: GridElement;

  private readonly labelCellId = 'label';
  private readonly selectCellId = 'single-select-row-column';

  constructor() {
    super('single-milestone-picker-dialog');
    this.grid = new GridElement('single-milestones-picker-grid');
  }

  selectMilestone(id: number | string) {
    if (typeof id === 'string') {
      this.grid
        .findRowId(this.labelCellId, id)
        .then((numId) =>
          this.grid.getCell(numId, this.selectCellId).radioNgZorroRenderer().check(),
        );
    } else {
      this.grid.getCell(id, this.selectCellId).radioNgZorroRenderer().check();
    }
  }

  assertMilestoneIsSelected(id: number | string) {
    if (typeof id === 'string') {
      this.grid
        .findRowId(this.labelCellId, id)
        .then((numId) =>
          this.grid.getCell(numId, this.selectCellId).radioNgZorroRenderer().assertIsCheck(),
        );
    } else {
      this.grid.getCell(id, this.selectCellId).radioNgZorroRenderer().assertIsCheck();
    }
  }
}
