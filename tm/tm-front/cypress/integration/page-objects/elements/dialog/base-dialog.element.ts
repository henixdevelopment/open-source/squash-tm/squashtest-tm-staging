import Chainable = Cypress.Chainable;
import {
  selectByDataTestElementId,
  selectByDataTestDialogButtonId,
  selectByDataTestDialogId,
} from '../../../utils/basic-selectors';
import { BasicElement } from '../abstract-element';

export class BaseDialogElement extends BasicElement {
  constructor(dialogId: string) {
    super(selectByDataTestDialogId(dialogId));
  }

  public close() {
    this.clickOnCloseButton();
    this.assertNotExist();
  }

  public escapeToCloseDialogAndAssertNotExist() {
    cy.get('body').type('{esc}');
    this.assertNotExist();
  }

  public confirm() {
    this.clickOnConfirmButton();
    this.assertNotExist();
  }

  public cancel() {
    this.clickOnCancelButton();
    this.assertNotExist();
  }

  public assertConfirmButtonExists() {
    this.selectButton('confirm').should('exist');
  }

  public assertCancelButtonExists() {
    this.selectButton('cancel').should('exist');
  }

  public assertCloseButtonExists() {
    this.selectButton('close').should('exist');
  }

  public clickOnCloseButton() {
    this.clickOnButton('close');
  }

  public clickOnConfirmButton() {
    this.clickOnButton('confirm');
  }

  public clickOnAddButton() {
    this.clickOnButton('add');
  }

  public clickOnCancelButton(): void {
    this.clickOnButton('cancel');
  }

  public assertTitleHasText(expectedText: string) {
    this.find(selectByDataTestElementId('dialog-title')).should('contain.text', expectedText);
  }

  public assertHasMessage(expectedMessage: string) {
    return this.find('[data-test-dialog-message]').should('contain.text', expectedMessage);
  }

  public checkButtonClass(buttonId: string, classValue: string) {
    cy.get(buttonId).should('have.class', classValue);
  }

  protected selectButton(buttonId: string): Chainable {
    return this.find(selectByDataTestDialogButtonId(buttonId));
  }

  protected clickOnButton(buttonId: string) {
    this.selectButton(buttonId).click();
  }
}
