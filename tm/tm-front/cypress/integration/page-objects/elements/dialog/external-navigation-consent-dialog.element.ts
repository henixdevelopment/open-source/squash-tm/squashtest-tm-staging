import { BaseDialogElement } from './base-dialog.element';

export class ExternalNavigationConsentDialogElement extends BaseDialogElement {
  constructor() {
    super('external-navigation-consent-dialog');
  }

  assertUrlIs(url: string) {
    this.findByElementId('external-url').should('contain.text', url);
  }
}
