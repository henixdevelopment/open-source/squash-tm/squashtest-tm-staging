import { BaseDialogElement } from './base-dialog.element';

export class ConfirmInterProjectPaste extends BaseDialogElement {
  constructor(dialogId: string) {
    super(dialogId);
  }
}
