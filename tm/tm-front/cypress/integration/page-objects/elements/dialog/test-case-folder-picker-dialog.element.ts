import { BaseDialogElement } from './base-dialog.element';
import { TreeElement } from '../grid/grid.element';

export class TestCaseFolderPickerDialogElement extends BaseDialogElement {
  readonly treeElement = TreeElement.createTreeElement('test-case-tree-folder-picker');

  constructor() {
    super('test-case-tree-folder-picker-dialog');
  }

  selectDestination(destinationRowId: string) {
    this.treeElement.getRow(destinationRowId).findRow().click();
  }
}
