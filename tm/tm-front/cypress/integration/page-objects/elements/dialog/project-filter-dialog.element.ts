import { GridElement } from '../grid/grid.element';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { BaseDialogElement } from './base-dialog.element';

export class ProjectFilterDialog extends BaseDialogElement {
  public readonly grid: GridElement;

  constructor() {
    super('project-filter');
    this.grid = new GridElement('project-filter');
  }

  fillSearchInput(value: string) {
    const searchInput = cy.get('nz-input-group input');
    searchInput.should('exist');
    searchInput.clear().type(value);
  }

  confirm() {
    const mockBuilder = new HttpMockBuilder('global-filter/filter').post().build();
    this.clickOnButton('apply');
    mockBuilder.wait();
    this.assertNotExist();
  }

  confirmWithEnterKey() {
    const mockBuilder = new HttpMockBuilder('global-filter/filter').post().build();
    cy.get('body').type('{enter}');
    mockBuilder.wait();
    this.assertNotExist();
  }

  cancelWithEscapeKey() {
    cy.get('body').type('{esc}');
    this.assertNotExist();
  }

  assertMilestoneWarningIsVisible() {
    this.findByElementId('milestone-warning').should('exist');
  }

  assertMilestoneWarningIsNotVisible() {
    this.findByElementId('milestone-warning').should('not.exist');
  }
}
