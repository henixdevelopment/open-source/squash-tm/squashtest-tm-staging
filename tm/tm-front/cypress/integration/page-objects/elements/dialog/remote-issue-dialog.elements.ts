import { BaseDialogElement } from './base-dialog.element';
import { selectByDataIcon } from '../../../utils/basic-selectors';

type fieldName = 'summary';

export class RemoteIssueDialogElements extends BaseDialogElement {
  constructor() {
    super('remote-issue-dialog');
  }

  assertSpinnerIsNotPresent() {
    this.find(selectByDataIcon('loading')).should('not.exist');
  }

  typeFieldRemoteIssue(fieldName: fieldName, content: string) {
    this.findByFieldName(fieldName).click();
    this.findByFieldName(fieldName).find('input').clear();
    this.findByFieldName(fieldName).find('input').type(content);
  }
}
