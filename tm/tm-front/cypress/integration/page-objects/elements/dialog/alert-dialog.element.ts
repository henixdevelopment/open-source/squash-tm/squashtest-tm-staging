import { BaseDialogElement } from './base-dialog.element';
import { selectByDataIcon } from '../../../utils/basic-selectors';

export class AlertDialogElement extends BaseDialogElement {
  public constructor(dialogId = 'alert') {
    super(dialogId);
  }

  assertExclamationCircleIconExists() {
    this.find(selectByDataIcon('exclamation-circle')).should('exist');
  }
}
