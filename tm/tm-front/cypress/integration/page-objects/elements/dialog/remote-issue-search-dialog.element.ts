import { BaseDialogElement } from './base-dialog.element';
import { selectByDataIcon } from '../../../utils/basic-selectors';
import { OptionalSelectField } from '../forms/optional-select-field.element';

type fieldType = 'projectPath' | 'issueIid';
type searchOption = "Titre de l'Issue" | "ID de l'Issue";

export class RemoteIssueSearchDialogElement extends BaseDialogElement {
  constructor() {
    super('remote-issue-dialog');
  }

  assertSpinnerIsNotPresent() {
    this.find(selectByDataIcon('loading')).should('not.exist');
  }

  typeFieldRemoteIssue(field: fieldType, content: string) {
    this.findByFieldId(field).click();
    this.findByFieldId(field).find('input').clear();
    this.findByFieldId(field).find('input').type(content);
  }

  selectSearchByIidOrTitle(option: searchOption) {
    const iidOrTitleSelectField = new OptionalSelectField('iidOrTitle');
    iidOrTitleSelectField.selectValue(option);
  }

  clickOnSearchButton() {
    this.clickOnButton('search-button');
  }
}
