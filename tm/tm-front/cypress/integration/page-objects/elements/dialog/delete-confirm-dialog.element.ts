import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { BaseDialogElement } from './base-dialog.element';
import { selectByDataIcon } from '../../../utils/basic-selectors';
import Chainable = Cypress.Chainable;

export abstract class DeleteConfirmDialogElement extends BaseDialogElement {
  protected constructor(dialogId: string) {
    super(dialogId);
  }

  abstract deleteForSuccess(response?: GridResponse);

  abstract deleteForFailure(response: any);

  assertExclamationCircleIconExists() {
    this.find(selectByDataIcon('exclamation-circle')).should('exist');
  }

  assertDeleteButtonIsRed() {
    this.selectButton('confirm').should('have.class', 'danger-icon');
  }

  checkWarningMessages(expectedMessages: string[]) {
    this.selectDeletionWarning()
      .should('exist')
      .find('li')
      .then((warnings) => {
        cy.wrap(warnings.length).should('equal', expectedMessages.length);
        warnings.each((index, warning) => {
          cy.wrap(warning).should('contain.text', expectedMessages[index]);
        });
      });
  }

  private selectDeletionWarning(): Chainable<any> {
    return this.findByElementId('deletion-warnings');
  }
}
