import { BaseDialogElement } from './base-dialog.element';

export class BugtrackerDialogElement extends BaseDialogElement {
  constructor() {
    super('bugtracker-connection');
  }

  fillPasswordField(password: string) {
    this.findByFieldName('password').click();
    this.findByFieldName('password').type(password);
  }

  clickOnConnectionButton() {
    this.clickOnButton('connection');
  }
}
