import { BaseDialogElement } from './base-dialog.element';

export class ConfirmInterProjectMove extends BaseDialogElement {
  constructor(dialogId: string) {
    super(dialogId);
  }
}
