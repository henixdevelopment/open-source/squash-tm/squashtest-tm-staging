import { BaseDialogElement } from '../dialog/base-dialog.element';

export class EnvironmentsStatusesSummaryDialogElement extends BaseDialogElement {
  constructor() {
    super('environments-statuses-summary');
  }

  assertSummaryGridForProjectIsVisible(projectId: number, projectName: string): void {
    this.findByElementId('summary-grid-project-' + projectId.toString()).should('be.visible');

    this.findByElementId('exec-env-summary-grid-project-title').should(
      'contain.text',
      'Projet "' + projectName,
    );
  }

  assertEnvironmentGridIsVisible(): void {
    this.findByElementId('exec-env-summary-grid-envs-grid').should('be.visible');
  }

  assertNoTokenWarningIsVisible(): void {
    this.findByElementId('exec-env-summary-grid-no-token-message')
      .should('be.visible')
      .should(
        'contain.text',
        " Aucun jeton n'est configuré ni au niveau du projet, ni au niveau du serveur. Veuillez saisir un jeton à l'un de ces deux endroits pour exécuter les tests automatisés et sélectionner un environnement d'exécution. ",
      );
  }

  assertNoEnvironmentsWarningIsVisible(projectName: string, orchestratorName: string): void {
    this.findByElementId('exec-env-summary-grid-no-env-message')
      .should('be.visible')
      .should('contain.text', "Aucun environnement d'exécution n'a été trouvé.")
      .should(
        'contain.text',
        'Veuillez vérifier que des environnements sont disponibles, et que le token renseigné (dans la configuration du projet "' +
          projectName +
          '" ou dans la configuration du serveur "' +
          orchestratorName +
          '") a bien accès aux environnements d\'exécution souhaités.',
      );
  }

  assertErrorMessageIsVisible(errorMessage: string): void {
    this.findByElementId('exec-env-summary-grid-error-message')
      .should('be.visible')
      .should('contain.text', errorMessage);
  }
}
