import {
  selectByDataTestElementId,
  selectByDataTestIconId,
  selectByDataTestToolbarButtonId,
} from '../../../utils/basic-selectors';
import { HttpMockBuilder, HttpResponseStatus } from '../../../utils/mocks/request-mock';
import { EnvironmentSelectionPanelDto } from './environment-selection-panel.element';

export class EnvironmentsStatusesCountElement {
  private readonly rootSelector = 'sqtm-app-environments-statuses-count';

  refreshCountComponent(newResponse: EnvironmentStatusesCountDTO, view?: string): void {
    if (view) {
      new HttpMockBuilder<EnvironmentStatusesCountDTO>(
        `test-automation/${view}/*/automated-execution-environments-statuses-count`,
      )
        .get()
        .responseBody(newResponse)
        .build();
    }
    cy.get(this.rootSelector)
      .find(selectByDataTestToolbarButtonId(selectors.REFRESH_BUTTON))
      .click();
  }

  assertIconColorIs(status: string, color: string): void {
    cy.get(
      selectByDataTestIconId(selectors.STATUS_COUNT_ICON_PREFIX + status.toLowerCase()),
    ).should('have.css', 'color', color);
  }

  assertGlobalStatusIconIs(iconType: string): void {
    cy.get(selectByDataTestIconId('exec-env-count-global-status-icon')).should(
      'have.attr',
      'ng-reflect-nz-type',
      iconType,
    );
  }

  assertCountIsVisible(): void {
    cy.get(selectByDataTestElementId('statuses-count-capsule')).should('be.visible');
  }

  assertCountIs(status: string, count: number) {
    cy.get(
      selectByDataTestElementId(selectors.STATUS_COUNT_BADGE_PREFIX + status.toLowerCase()),
    ).should('have.attr', 'ng-reflect-nz-count', count);
  }

  assertCapsuleIsClickable(): void {
    cy.get(selectByDataTestElementId('statuses-count-capsule')).should(
      'have.class',
      '__hover_pointer',
    );
  }

  assertCapsuleIsNotClickable(): void {
    cy.get(selectByDataTestElementId('statuses-count-capsule')).should(
      'not.have.class',
      '__hover_pointer',
    );
  }

  openExecutionEnvironmentsSummaryDialog(
    response: EnvironmentSelectionPanelDto,
    status: HttpResponseStatus,
  ): void {
    new HttpMockBuilder<EnvironmentSelectionPanelDto>(
      `test-automation/*/automated-execution-environments/all?frontEndErrorIsHandled=true`,
    )
      .get()
      .responseBody(response)
      .status(status)
      .build();
    cy.get(selectByDataTestElementId('statuses-count-capsule')).click();
  }
}

export interface EnvironmentStatusesCountDTO {
  statuses: { [key: string]: number };
  projects: ExecutionEnvironmentCountProjectInfoDTO[];
  nbOfSquashOrchestratorServers: number;
  nbOfUnreachableSquashOrchestrators: number;
  nbOfServersWithMissingToken: number;
}

export interface ExecutionEnvironmentCountProjectInfoDTO {
  projectId: number;
  projectName: string;
  squashOrchestratorId: number;
  squashOrchestratorName: string;
}

const selectors = {
  STATUS_COUNT_ICON_PREFIX: 'exec-env-status-',
  STATUS_COUNT_BADGE_PREFIX: 'exec-env-count-status-',
  REFRESH_BUTTON: 'refresh-button',
};
