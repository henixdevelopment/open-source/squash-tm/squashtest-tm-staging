import { HttpMockBuilder } from '../../../utils/mocks/request-mock';

export class AttachmentCompactListElement {
  checkAttachmentData(
    attachmentIndex: number,
    expectedName: string,
    expectedSize: string,
    expectedDate: string,
    // index of list in page if several compact attachments lists are present
    listIndex?: number,
  ) {
    this.checkAttachmentName(attachmentIndex, expectedName, listIndex);
    this.checkAttachmentSize(attachmentIndex, expectedSize, listIndex);
    this.checkAttachmentAddedOn(attachmentIndex, expectedDate, listIndex);
  }

  checkAttachmentName(attachmentIndex: number, expectedName: string, listIndex?: number) {
    cy.get(
      `
      ${this.getRowSelector(attachmentIndex, listIndex)}
      td.name
      `,
    ).should('have.text', expectedName);
  }

  checkAttachmentSize(attachmentIndex: number, expectedSize: string, listIndex?: number) {
    cy.get(
      `
      ${this.getRowSelector(attachmentIndex, listIndex)}
      td.size
      `,
    ).should('have.text', expectedSize);
  }

  private getRowSelector(attachmentIndex, listIndex?: number) {
    const nthOfType = attachmentIndex ? attachmentIndex + 1 : 1;
    return `
   ${this.getTableSelector(listIndex)}
    tr:nth-of-type(${nthOfType})
    `;
  }

  private getTableSelector(index?: number) {
    const nthOfType = index ? index + 1 : 1;
    return `
    sqtm-core-attachment-list-compact:nth-of-type(${nthOfType})
    table`;
  }

  private getRowDeleteButtonSelector(attachmentIndex: number, listIndex?: number) {
    const nthOfType = attachmentIndex ? attachmentIndex + 2 : 2;
    return `
   ${this.getTableSelector(listIndex)}
    tr:nth-of-type(${nthOfType}).pending-delete
    `;
  }

  checkAttachmentAddedOn(attachmentIndex: number, expectedDate: string, listIndex?: number) {
    cy.get(
      `
      ${this.getRowSelector(attachmentIndex, listIndex)}
      td.added-on
      `,
    ).should('contain.text', expectedDate);
  }

  // listIndex is the index of list in page if several compact attachments lists are present
  deleteAttachment(attachmentIndex: number, listIndex?: number) {
    cy.get(
      `
      ${this.getRowSelector(attachmentIndex, listIndex)}
      td.delete
      span
      `,
    ).click({ force: true });
    this.getConfirmDeleteAttachmentButton(attachmentIndex).should('exist');
    this.getCancelDeleteAttachmentButton(attachmentIndex).should('exist');
  }

  cancelDeleteAttachment(attachmentIndex: number, listIndex?: number) {
    this.getCancelDeleteAttachmentButton(attachmentIndex, listIndex).click({ force: true });
    this.assertDeleteBarButtonNotExist(attachmentIndex, listIndex);
  }

  confirmDeleteAttachment(
    attachmentIndex: number,
    attachmentListIndex?: number,
    attachmentListId?: number,
    attachmentId?: number,
    entityId?: number,
  ) {
    // /backend/attach-list/1/attachments/2?entityId=1&entityType=execution
    const mock = new HttpMockBuilder(
      `attach-list/${attachmentListId || '*'}/attachments/${attachmentId || '*'}?entityId=${entityId || '*'}&entityType=execution&holderType=*`,
    )
      .delete()
      .build();
    this.getConfirmDeleteAttachmentButton(attachmentIndex, attachmentListIndex).click({
      force: true,
    });
    mock.wait();
    this.assertAttachmentNotExist(attachmentIndex, attachmentId);
  }

  private assertDeleteBarButtonNotExist(attachmentIndex: number, listIndex?: number) {
    cy.get(
      `
      ${this.getRowDeleteButtonSelector(attachmentIndex, listIndex)}
    `,
    ).should('not.exist');
  }

  private getCancelDeleteAttachmentButton(attachmentIndex: number, listIndex?: number) {
    return this.getDeleteBarButton(attachmentIndex, listIndex).contains('Annuler');
  }

  private getConfirmDeleteAttachmentButton(attachmentIndex: number, listIndex?: number) {
    return this.getDeleteBarButton(attachmentIndex, listIndex).contains('Supprimer');
  }

  private getDeleteBarButton(attachmentIndex: number, listIndex?: number) {
    return cy.get(`
      ${this.getRowDeleteButtonSelector(attachmentIndex, listIndex)}
      button
    `);
  }

  private assertAttachmentNotExist(attachmentIndex: number, listIndex?: number) {
    cy.get(this.getRowSelector(attachmentIndex, listIndex)).should('not.exist');
  }
}
