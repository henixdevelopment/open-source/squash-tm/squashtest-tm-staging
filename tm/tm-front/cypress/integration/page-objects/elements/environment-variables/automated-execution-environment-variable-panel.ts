import { GridElement } from '../grid/grid.element';
import {
  selectByDataTestButtonId,
  selectByDataTestElementId,
  selectByDataTestDialogButtonId,
  selectByDataTestItemId,
} from '../../../utils/basic-selectors';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { EvInputType } from '../../../../../projects/sqtm-core/src/lib/model/environment-variable/ev-input-type.model';

export class AutomatedExecutionEnvironmentVariablePanel {
  public readonly boundEnvironmentVariablesGrid: GridElement = GridElement.createGridElement(
    'environment-variable-associations',
  );

  selectUpdateEnvironmentVariableOption(option: string) {
    const mock = new HttpMockBuilder(`bound-environment-variables/value`).post().build();
    cy.get(selectByDataTestItemId(`item-${option}`)).click();
    mock.wait();
  }

  updateSimpleEnvironmentVariableValue(
    newValue: string,
    environmentVariableLabel: string,
    cell: string,
    url: string,
  ) {
    this.boundEnvironmentVariablesGrid.findRowId('name', environmentVariableLabel).then((rowId) => {
      this.boundEnvironmentVariablesGrid
        .getRow(rowId)
        .cell(cell)
        .textRenderer()
        .editText(newValue, url);
    });
  }

  showEnvironmentVariableOptionsList(
    environmentVariableId: number,
    environmentVariableLabel: string,
    cell: string,
  ) {
    this.boundEnvironmentVariablesGrid.findRowId('name', environmentVariableLabel).then((rowId) => {
      this.boundEnvironmentVariablesGrid
        .getRow(rowId)
        .cell(cell)
        .findCell()
        .find(`[data-test-item-id="select-option=${environmentVariableId}"]`)
        .click();
    });
  }

  checkCellValue(valueToCheck: string, environmentVariableLabel: string, cell: string) {
    this.boundEnvironmentVariablesGrid
      .findRowId('name', environmentVariableLabel)
      .then((rowId) =>
        this.boundEnvironmentVariablesGrid
          .getRow(rowId)
          .cell(cell)
          .textRenderer()
          .assertContainsText(valueToCheck),
      );
  }

  unbindEnvironmentVariable(environmentVariableLabel: string) {
    this.boundEnvironmentVariablesGrid.findRowId('name', environmentVariableLabel).then((rowId) => {
      this.boundEnvironmentVariablesGrid.getRow(rowId).cell('delete').iconRenderer().click();

      const unbindMock = new HttpMockBuilder(`bound-environment-variables/unbind`).post().build();

      this.clickConfirmDeleteButton();
      unbindMock.wait();
    });
    this.checkNotExistRow(environmentVariableLabel);
  }

  private clickConfirmDeleteButton() {
    cy.get('sqtm-core-confirm-delete-dialog')
      .find('[data-test-dialog-button-id="confirm"]')
      .click()
      // Then
      .get('sqtm-core-confirm-delete-dialog')
      .should('not.exist');
  }

  unbindMultipleEnvironmentVariable(environmentVariableLabels: string[]) {
    cy.get(selectByDataTestButtonId('unbind-environment-variable')).click();
    const unbindMock = new HttpMockBuilder('bound-environment-variables/unbind').post().build();

    this.clickConfirmDeleteButton();
    unbindMock.wait();
    environmentVariableLabels.forEach((environmentVariableLabel) =>
      this.checkNotExistRow(environmentVariableLabel),
    );
  }

  bindEnvironmentVariable() {
    this.getEnvironmentVariablesListToBind();
    this.bindAllEnvironmentVariable();
    this.checkExistRow('environmentVariable3');
    this.checkExistRow('environmentVariable4');
  }

  private getEnvironmentVariablesListToBind() {
    const environmentVariablesMock = new HttpMockBuilder('environment-variables')
      .get()
      .responseBody({
        environmentVariables: [
          {
            id: 10,
            name: 'environmentVariable3',
          },
          {
            id: 9,
            name: 'environmentVariable4',
          },
        ],
      })
      .build();
    cy.get(selectByDataTestButtonId('bind-environment-variable')).click();
    environmentVariablesMock.wait();
  }

  private bindAllEnvironmentVariable() {
    cy.get(selectByDataTestElementId('item')).click({ multiple: true });

    const bindMock = new HttpMockBuilder('bound-environment-variables/bind')
      .post()
      .responseBody(bindResponse)
      .build();
    cy.get(selectByDataTestDialogButtonId('confirm')).click();
    bindMock.wait();
  }

  private checkNotExistRow(environmentVariableLabel: string) {
    this.boundEnvironmentVariablesGrid.getRow(environmentVariableLabel).assertNotExist();
  }

  private checkExistRow(environmentVariableLabel: string) {
    this.boundEnvironmentVariablesGrid.getRow(environmentVariableLabel).assertExists();
  }

  resetValue(id: number, resetValue: string) {
    const resetMock = new HttpMockBuilder('bound-environment-variables/reset')
      .post()
      .responseBody({ defaultValue: resetValue })
      .build();
    cy.get(selectByDataTestButtonId('reset-ev-value-button=' + id)).click();
    resetMock.wait();
  }
}

const bindResponse = {
  boundEnvironmentVariables: [
    {
      id: 10,
      code: 'EV3',
      inputType: EvInputType.DROPDOWN_LIST,
      name: 'environmentVariable3',
      options: [
        {
          evId: 10,
          label: 'OPT1',
          code: 'OPT1',
          position: 0,
        },
      ],
      value: '',
      boundToServer: false,
    },
    {
      id: 9,
      code: 'EV4',
      inputType: EvInputType.PLAIN_TEXT,
      name: 'environmentVariable4',
      options: [],
      value: '',
      boundToServer: false,
    },
    {
      id: 1,
      name: 'environmentVariable1',
      code: 'code1',
      inputType: EvInputType.PLAIN_TEXT,
      boundToServer: false,
      options: [],
      value: 'ev-value',
    },
    {
      id: 2,
      name: 'environmentVariable2',
      code: 'code2',
      inputType: EvInputType.DROPDOWN_LIST,
      boundToServer: false,
      options: [
        {
          evId: 1,
          label: 'option1',
          code: 'code1',
          position: 0,
        },
        {
          evId: 2,
          label: 'option2',
          code: 'code2',
          position: 1,
        },
      ],
      value: '',
    },
  ],
};
