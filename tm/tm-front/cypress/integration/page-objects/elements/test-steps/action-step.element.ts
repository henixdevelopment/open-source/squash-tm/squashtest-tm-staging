import { AbstractStepElement } from './abstract-step.element';
import { selectByDataIcon } from '../../../utils/basic-selectors';
import { EditableRichTextFieldElement } from '../forms/editable-rich-text-field.element';
import { ToolbarMenuButtonElement } from '../workspace-common/toolbar.element';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { DetailedStepViewPage } from '../../pages/test-case-workspace/detailed-step-view/detailed-step-view.page';
import { ElementSelectorFactory } from '../forms/abstract-form-field.element';
import { TestCaseModel } from '../../../../../projects/sqtm-core/src/lib/model/test-case/test-case.model';
import { CallStepElement } from './call-step.element';
import { RichTextFieldElement } from '../forms/RichTextFieldElement';

export type StepSectionType = 'action' | 'result';

export class ActionStepElement extends AbstractStepElement {
  constructor(stepId: string, stepIndex: number) {
    super(stepId, stepIndex);
  }

  checkIsExpended() {
    this.getCollapseArrow().should('exist');
    this.getExpendArrow().should('not.exist');
  }

  checkIsCollapsed() {
    this.getExpendArrow().should('exist');
    this.getCollapseArrow().should('not.exist');
  }

  extendStep() {
    this.getExpendArrow().click();
    this.checkIsExpended();
  }

  getActionField() {
    return new EditableRichTextFieldElement('action');
  }

  getExpectedResultField() {
    return new EditableRichTextFieldElement('result');
  }

  getActionFieldForNewStep() {
    return new RichTextFieldElement('action');
  }

  getExpectedResultFieldForNewStep() {
    return new RichTextFieldElement('expectedResult');
  }

  private getCollapseArrow() {
    return this.findByElementId('collapse-step');
  }

  private getExpendArrow() {
    return this.findByElementId('expend-step');
  }

  checkCalledTestCaseStep(calledTestCaseName: string, stepId: string, index: number) {
    const callStep = new CallStepElement(stepId, index);
    callStep.assertNameTestCallStep(calledTestCaseName);
  }

  modifyFilledStepWithParam(
    value: string,
    stepSectionType: StepSectionType,
    url: string,
    response?: any,
  ) {
    this.find(`div.${stepSectionType}`).click();
    const fieldSelector = this.getFieldSelector(stepSectionType);
    const actionForm: EditableRichTextFieldElement = new EditableRichTextFieldElement(
      fieldSelector,
      url,
    );
    actionForm.enableEditMode();
    actionForm.setValue(value);
    actionForm.confirmValue(response);
    actionForm.checkTextContent(value);
  }

  modifyFilledStepOnErrorWithParam(
    value: string,
    stepSectionType: StepSectionType,
    url: string,
    squashError?: any,
  ) {
    const actionForm: EditableRichTextFieldElement = new EditableRichTextFieldElement(
      this.getFieldSelector(stepSectionType),
      url,
    );
    actionForm.enableEditMode();
    const newValue = value.replace(/(\$)(\{)(\w*\s*\w*)(})/gi, '$1{$2}$3{$4}');
    actionForm.setValue(newValue);
    actionForm.confirmOnError(squashError);
  }

  navigateToStepDetails(model?: TestCaseModel) {
    const actionMenu = new ToolbarMenuButtonElement(
      this.rootSelector,
      'step-action-button',
      'step-action-menu',
    );
    const testCaseId = model ? model.id.toString() : '*';
    const url = `detailed-test-step-view/${testCaseId}?**`;
    const mock = new HttpMockBuilder(url).responseBody(model).build();
    actionMenu.showMenu(true).item('navigate-to-details').click();
    mock.wait();
    return new DetailedStepViewPage(testCaseId);
  }

  private getFieldSelector(stepSectionType: StepSectionType): ElementSelectorFactory {
    return () => this.findByFieldId(stepSectionType);
  }

  private get stepCoverageField() {
    return this.findByFieldId('step-coverage-field');
  }
  assertRequirementLinkedToTestStep() {
    this.stepCoverageField.should('exist');
  }
  removeLinkedRequirement(index: number) {
    this.stepCoverageField
      .get(selectByDataIcon('remove-step-coverage'))
      .eq(index)
      .click({ force: true });
    this.stepCoverageField.should('not.exist');
  }

  dropZoneRequirementCoverage() {
    cy.get(this.rootSelector)
      .trigger('mouseenter', { force: true, buttons: 1 })
      .trigger('mousemove', -20, -20, { force: true })
      .trigger('mouseup', { force: true });
  }
  assertDeleteIconRequirementTestStepLinkNotExist() {
    this.stepCoverageField.get(selectByDataIcon('remove-step-coverage')).should('not.exist');
  }
}
