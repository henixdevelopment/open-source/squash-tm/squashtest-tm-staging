import {
  selectByDataTestButtonId,
  selectByDataTestElementId,
  selectByDataTestToolbarButtonId,
} from '../../../utils/basic-selectors';
import { AbstractElement } from '../abstract-element';
import { ElementSelectorFactory } from '../forms/abstract-form-field.element';
import { MenuElement } from '../../../utils/menu.element';

export abstract class AbstractStepElement extends AbstractElement {
  protected readonly rootSelector: string;

  override readonly rootChainableFactory: ElementSelectorFactory;

  protected constructor(
    protected stepId: string,
    protected stepIndex: number,
  ) {
    super();

    if (this.stepId) {
      this.rootSelector = `sqtm-app-test-step
    div[data-test-element-id="test-step"][data-test-test-step-id="${stepId}"]
    `;
    } else {
      // step index + 1 because we have the prerequisite block just above the steps
      this.rootSelector = `sqtm-app-test-step:nth-of-type(${this.stepIndex + 1})
      div[data-test-element-id="test-step"]
    `;
    }

    this.rootChainableFactory = () => cy.get(this.rootSelector);
  }

  checkIndexLabel() {
    this.getIndexLabel().should('contain.text', (this.stepIndex + 1).toString());
  }

  // select the step in single mode aka deselect all other steps
  singleSelectStep() {
    this.getIndexLabel().click();
  }

  // select toggle aka with ctrl
  toggleStepSelection() {
    cy.get('body').type('{ctrl}', { release: false });
    this.getIndexLabel().click();
    cy.get('body').type('{ctrl}');
  }

  assertIsSelected() {
    this.getIndexLabel().should('have.class', 'selected');
  }

  assertIsNotSelected() {
    this.getIndexLabel().should('not.have.class', 'selected');
  }

  assertActionButtonBarIsVisible() {
    this.getActionButtonBar().should('be.visible');
  }

  private getActionButtonBar() {
    return this.find(selectByDataTestElementId('button-area'));
  }

  private getIndexLabel() {
    return this.find('.index-typography');
  }

  clickButtonAdd() {
    this.find(selectByDataTestButtonId('addStep')).click({ force: true });
  }

  clickStepActionButton() {
    this.getActionButtonBar()
      .find(selectByDataTestToolbarButtonId('step-action-button'))
      .click({ force: true });
  }

  clickActionStepButtonAdd() {
    this.getActionButtonBar()
      .find(selectByDataTestButtonId('addActionStepAfter'))
      .click({ force: true });
  }

  clickActionStepActionButtonAndSelectOptionInMenu(
    option:
      | 'add-requirement-coverage'
      | 'call-test-case'
      | 'navigate-to-details'
      | 'add-attachments',
  ) {
    const stepMenu = new MenuElement('step-action-menu');
    this.clickStepActionButton();
    stepMenu.assertExists();
    stepMenu.item(`${option}`).click();
  }
  assertStepButtonAddDoesNotExist() {
    this.getActionButtonBar()
      .find(selectByDataTestButtonId('addActionStepAfter'))
      .should('not.exist');
  }
  clickActionStepActionButtonAndAssertOptionInMenuIsDisableAndClick(
    option:
      | 'add-requirement-coverage'
      | 'call-test-case'
      | 'navigate-to-details'
      | 'add-attachments',
  ) {
    const stepMenu = new MenuElement('step-action-menu');
    this.clickStepActionButton();
    stepMenu.assertExists();
    stepMenu.item(`${option}`).assertDisabled();
    stepMenu.item(`${option}`).click();
  }
}
