import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { ElementSelectorFactory } from '../forms/abstract-form-field.element';
import { EditableActionTextFieldElement } from '../forms/editable-action-text-field.element';
import { EditableSelectFieldElement } from '../forms/editable-select-field.element';
import { EditableTextAreaFieldElement } from '../forms/editable-text-area-field.element';
import { TestCaseParameterOperationReport } from '../../../../../projects/sqtm-core/src/lib/model/test-case/operation-reports.model';
import {
  selectByDataTestButtonId,
  selectByDataTestElementId,
  selectByDataTestMenuItemId,
  selectByDataTestTestStepId,
} from '../../../utils/basic-selectors';
import { AbstractElement } from '../abstract-element';
import { MenuElement } from '../../../utils/menu.element';
import { RemoveTestStepsDialogElement } from '../../pages/test-case-workspace/dialogs/remove-test-steps-dialog.element';

export class KeywordStepElement extends AbstractElement {
  override readonly rootChainableFactory: ElementSelectorFactory;

  constructor(
    private stepId: number,
    private stepIndex: number,
  ) {
    super();

    let rootSelector: string;

    if (this.stepId) {
      rootSelector =
        `sqtm-app-keyword-step
    ` +
        selectByDataTestElementId('keyword-test-step') +
        selectByDataTestTestStepId(`${stepId}`);
    } else {
      // step index + 1 because we have the prerequisite block just above the steps
      rootSelector =
        `sqtm-app-keyword-step:nth-of-type(${this.stepIndex + 1})
    ` + selectByDataTestElementId('keyword-test-step');
    }

    this.rootChainableFactory = () => cy.get(rootSelector);
  }

  checkIndexLabel() {
    this.getIndexLabel().should('contain.text', (this.stepIndex + 1).toString());
  }

  selectStep() {
    this.getIndexLabel().click();
  }

  getKeywordField(): EditableSelectFieldElement {
    return new EditableSelectFieldElement(
      () => this.findByFieldId('keyword'),
      'test-steps/*/keyword',
    );
  }

  getActionField(): EditableActionTextFieldElement {
    return new EditableActionTextFieldElement(
      () => this.findByFieldId('action'),
      'test-steps/*/action-word',
    );
  }

  deleteStep(response?: TestCaseParameterOperationReport) {
    new HttpMockBuilder('test-cases/*/verified-requirements').responseBody({}).build();
    const mock = new HttpMockBuilder('test-cases/*/steps/*')
      .delete()
      .responseBody(response)
      .build();
    this.getDeleteButton().click({ force: true });
    this.confirmDelete();
    mock.wait();
  }

  checkDeleteButtonNotVisible() {
    this.getDeleteButton().should('not.exist');
  }

  // select toggle aka with ctrl
  toggleStepSelection() {
    cy.get('body').type('{ctrl}', { release: false });
    this.getIndexLabel().click();
    cy.get('body').type('{ctrl}');
  }

  moveStepUp() {
    const mock = new HttpMockBuilder('test-cases/*/steps/move').post().build();
    this.getArrowUp().click();
    mock.wait();
  }

  moveStepDown() {
    const mock = new HttpMockBuilder('test-cases/*/steps/move').post().build();
    this.getArrowDown().click();
    mock.wait();
  }

  getDraggableArea() {
    return this.find('.dnd-handler').invoke('show');
  }

  private getIndexLabel() {
    return this.find('.index-typography');
  }

  private getDeleteButton() {
    return this.find(selectByDataTestElementId('button-area')).find(
      selectByDataTestButtonId('delete'),
    );
  }

  private getMoreButton() {
    this.find('.step-list-element-buttons').invoke('show');
    return this.find(selectByDataTestButtonId('more'));
  }

  private confirmDelete() {
    const dialog = new RemoveTestStepsDialogElement(this.stepId, this.stepIndex);
    dialog.assertExists();
    dialog.confirm();
  }

  private getArrowUp() {
    return this.find('.arrow-sup').invoke('show');
  }

  private getArrowDown() {
    return this.find('.arrow-down').invoke('show');
  }

  private getCopyButton() {
    this.find('.step-list-element-buttons').invoke('show');
    return this.find(selectByDataTestButtonId('copy'));
  }

  private getPasteButton() {
    this.find('.step-list-element-buttons').invoke('show');
    return this.find(selectByDataTestButtonId('paste'));
  }

  /* Sub-sections management */

  getTextAreaField(fieldName: KeywordStepSectionName): EditableTextAreaFieldElement {
    return new EditableTextAreaFieldElement(
      () => this.findByFieldId(fieldName),
      `test-steps/*/${fieldName}`,
    );
  }

  assertSectionExist(sectionName: KeywordStepSectionName) {
    this.getSection(sectionName).should('exist');
  }

  assertSectionNotExist(sectionName: KeywordStepSectionName) {
    this.getSection(sectionName).should('not.exist');
  }

  assertSectionDeleteIconAppears(sectionName: KeywordStepSectionName) {
    this.getSectionDeleteIcon(sectionName).should('be.visible');
  }

  createSection(sectionName: KeywordStepSectionName) {
    this.getMoreButton().click();
    this.getSectionCreateButton(sectionName).click();
  }

  deleteSection(sectionName: KeywordStepSectionName) {
    const mock = new HttpMockBuilder(`test-steps/*/${sectionName}`).post().build();
    this.getSectionDeleteIcon(sectionName).click();
    mock.wait();
  }

  private getSection(sectionName: KeywordStepSectionName) {
    return this.find(`.${sectionName}-section`);
  }

  private getSectionCreateButton(sectionName: KeywordStepSectionName) {
    const menuElement = new MenuElement('menu');
    menuElement.assertExists();
    return menuElement.item(`create-${sectionName}`);
  }

  private getSectionDeleteIcon(sectionName: KeywordStepSectionName) {
    return this.getSection(sectionName).find(selectByDataTestButtonId('delete')).invoke('show');
  }

  assertWarningIsPresentForHavingBothDatatableAndDocstring(): void {
    this.findByElementId('datatable-and-docstring-warning').should('be.visible');
  }

  assertCreateDocstringMenuItemIsDisabled(): void {
    this.assertCreateSectionMenuItemIsDisabled(DOCSTRING);
  }

  assertCreateDatatableMenuItemIsDisabled(): void {
    this.assertCreateSectionMenuItemIsDisabled(DATATABLE);
  }
  assertCreateCommentMenuItemIsDisabled(): void {
    this.assertCreateSectionMenuItemIsDisabled(COMMENT);
  }
  assertCreateSectionMenuItemIsDisabled(sectionName: string): void {
    const menuElement = new MenuElement('menu');
    this.getMoreButton().click();
    menuElement.assertExists();
    menuElement.item(`create-${sectionName}`).assertDisabled();
  }

  copyKeywordStepWithIconInStep() {
    this.getCopyButton().click();
    this.getPasteButton().click();
  }

  selectMoreOption(
    item: 'create-datatable' | 'create-docstring' | 'create-comment' | 'navigate-to-action',
  ) {
    this.getMoreButton().click();
    cy.get(selectByDataTestMenuItemId(item)).click();
  }
  assertCanCopyTestStepButPasteButtonDoesNotExist() {
    this.getCopyButton().should('exist');
    this.getCopyButton().click();
    this.getPasteButton().should('not.exist');
  }
  assertCannotMoveStepDown() {
    this.find('.arrow-down').should('not.exist');
  }
}

export const DATATABLE = 'datatable';
export const DOCSTRING = 'docstring';
export const COMMENT = 'comment';
export type KeywordStepSectionName = 'datatable' | 'docstring' | 'comment';
