import { RichTextFieldElement } from '../forms/RichTextFieldElement';
import { selectByDataTestElementId } from '../../../utils/basic-selectors';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';

export class CreateActionStepFormElement {
  private readonly actionField: RichTextFieldElement;
  private readonly expectedResultField: RichTextFieldElement;

  constructor() {
    this.actionField = new RichTextFieldElement('action');
    this.expectedResultField = new RichTextFieldElement('expectedResult');
  }

  public assertFormIsReady() {
    this.actionField.assertIsReady();
    this.expectedResultField.assertIsReady();
  }

  public fillAction(name: string) {
    this.actionField.fillWithString(name);
  }

  public fillExpectedResult(name: string) {
    this.expectedResultField.fillWithString(name);
  }

  public confirmAddStep(testCaseId?: number, response?: any) {
    const mock = new HttpMockBuilder(`/test-cases/${testCaseId || '*'}/steps/add`)
      .post()
      .responseBody(response)
      .build();
    cy.get(selectByDataTestElementId('confirm-add-test-step')).click();
    mock.wait();
  }

  public cancelAddStep() {
    cy.get(selectByDataTestElementId('cancel-add-test-step')).click();
  }

  public assertCancelAddStepButtonDisabled() {
    cy.get(selectByDataTestElementId('cancel-add-test-step')).should('be.disabled');
  }
}
