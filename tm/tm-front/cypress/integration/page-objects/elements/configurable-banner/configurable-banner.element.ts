import Chainable = Cypress.Chainable;

export class ConfigurableBannerElement {
  private static selector(): Chainable {
    return cy.get('sqtm-core-configurable-banner');
  }

  public static assertExists(): void {
    this.selector().should('exist');
  }

  public static assertIsVisible(): void {
    this.selector().should('be.visible');
  }

  public static assertIsNotVisible(): void {
    this.selector().should('not.be.visible');
  }

  public static assertIsVisibleWithContent(content: string) {
    this.assertIsVisible();
    this.selector().should('contain', content);
  }
}
