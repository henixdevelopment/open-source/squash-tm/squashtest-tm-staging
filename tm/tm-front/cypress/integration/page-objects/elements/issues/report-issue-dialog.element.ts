import { HTTP_RESPONSE_STATUS, HttpMock, HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { TextFieldElement } from '../forms/TextFieldElement';
import { RemoteAutocompleteFieldElement } from '../forms/remote-autocomplete-field.element';
import { TextAreaFieldElement } from '../forms/text-area-field.element';
import { SelectFieldElement } from '../forms/select-field.element';
import { DatePickerElement } from '../forms/date-picker.element';
import { CascadingSelectFieldElement } from '../forms/cascading-select-field.element';
import { CheckboxListFieldElement } from '../forms/checkbox-list-field.element';
import { RemoteAttachmentFieldElement } from '../forms/remote-attachment-field-element';
import { CheckBoxElement } from '../forms/check-box.element';
import { RemoteIssueSearchForm } from '../../../../../projects/sqtm-core/src/lib/model/issue/remote-issue-search.model';
import { RemoteIssue } from '../../../../../projects/sqtm-core/src/lib/model/issue/remote-issue.model';
import { IssueBindableEntity } from '../../../../../projects/sqtm-core/src/lib/model/issue/issue-bindable-entity.model';
import { BaseDialogElement } from '../dialog/base-dialog.element';
import { RemoteSelectorFieldElement } from '../forms/remote-selector-field.element';
import { selectByDataTestDialogButtonId } from '../../../utils/basic-selectors';
import { RemoteComboBoxFieldElement } from '../forms/remote-combo-box-field.element';

export class ReportIssueDialogElement extends BaseDialogElement {
  private readonly remoteProjectNamesMock: HttpMock<any>;
  private readonly reportIssueModelMock: HttpMock<any>;
  private readonly remoteIssueSearchFormMock: HttpMock<RemoteIssueSearchForm>;

  constructor(public readonly mocks: ReportIssueDialogMocks) {
    super('remote-issue-dialog');

    this.remoteProjectNamesMock = new HttpMockBuilder('issues/projects/*/bugtracker')
      .responseBody({ projectNames: mocks.remoteProjectNames })
      .build();

    this.reportIssueModelMock = new HttpMockBuilder(
      'issues/execution-step/*/new-issue?project-name=*',
    )
      .responseBody(mocks.remoteIssue)
      .build();

    this.remoteIssueSearchFormMock = new HttpMockBuilder<RemoteIssueSearchForm>(
      'issues/issue-search-form',
    )
      .post()
      .responseBody(mocks.searchForm)
      .build();
  }

  waitForInitialRequests() {
    if (!this.mocks.attachMode) {
      this.remoteProjectNamesMock.wait();
      this.reportIssueModelMock.wait();
    } else {
      this.remoteProjectNamesMock.wait();
    }
  }

  fillTextField(fieldId: string, content: string): void {
    new TextFieldElement(fieldId).fill(content);
  }

  assertSearchButtonExists(): void {
    this.selectButton('search-button').should('be.visible');
  }

  assertSearchButtonNotExist(): void {
    this.selectButton('search-button').should('be.hidden');
  }

  assertAddAnotherButtonExists() {
    this.selectButton('add-another').should('exist');
  }

  getAutocompleteTextField(fieldId: string): RemoteAutocompleteFieldElement {
    return new RemoteAutocompleteFieldElement(fieldId);
  }

  getSelectorField(fieldId: string): RemoteSelectorFieldElement {
    return new RemoteSelectorFieldElement(fieldId);
  }

  getComboBoxField(fieldId: string): RemoteComboBoxFieldElement {
    return new RemoteComboBoxFieldElement(fieldId);
  }

  getTextAreaField(fieldId: string): TextAreaFieldElement {
    return new TextAreaFieldElement(fieldId);
  }

  getDropdownList(fieldId: string): SelectFieldElement {
    return new SelectFieldElement(() => this.findByFieldName(fieldId));
  }

  getFreeTagList(fieldId: string): SelectFieldElement {
    return new SelectFieldElement(() => this.findByFieldName(fieldId));
  }

  getTagList(fieldId: string): SelectFieldElement {
    return this.getFreeTagList(fieldId);
  }

  getDatePicker(fieldId: string): DatePickerElement {
    return new DatePickerElement(() => this.findByFieldName(fieldId));
  }

  getMultiSelect(fieldId: string): SelectFieldElement {
    return this.getFreeTagList(fieldId);
  }

  getCascadingSelect(fieldId: string): CascadingSelectFieldElement {
    return new CascadingSelectFieldElement(() => this.findByFieldName(fieldId));
  }

  getCheckboxList(fieldId: string): CheckboxListFieldElement {
    return new CheckboxListFieldElement(() => this.findByFieldName(fieldId));
  }

  getRemoteAttachmentField(fieldId: string): RemoteAttachmentFieldElement {
    return new RemoteAttachmentFieldElement(() => this.findByFieldName(fieldId));
  }

  getCheckbox(fieldId: string): CheckBoxElement {
    return new CheckBoxElement(() => this.findByFieldName(fieldId));
  }

  // We assume all searches are text-based
  searchForIssue(searchTerms: { [fieldName: string]: string }, result: RemoteIssue): void {
    const mock = new HttpMockBuilder('issues/search-issue').post().responseBody(result).build();

    Object.entries(searchTerms).forEach(([fieldName, value]) => {
      this.findByElementId(fieldName).type(value);
    });

    this.findByElementId('search-button').click();

    mock.wait();
  }

  confirmWithoutMock(): void {
    this.findByElementId('confirm').click();
  }

  confirmWithErrorResponse(error: any): void {
    const mock = new HttpMockBuilder(this.buildPostUrl())
      .post()
      .status(HTTP_RESPONSE_STATUS.PRECONDITION_FAIL)
      .responseBody(error)
      .build();

    this.confirmWithoutMock();
    mock.wait();
  }

  confirmWithSuccess(): void {
    const submitIssueMock = new HttpMockBuilder(this.buildPostUrl())
      .post()
      .responseBody({ issueId: 'ISSUE-ID', url: 'http://some.url/bt' })
      .build();

    this.confirmWithoutMock();
    submitIssueMock.wait();
  }

  assertHasRequiredError(fieldId: string): void {
    new TextFieldElement(fieldId).assertErrorExist('sqtm-core.validation.errors.required');
  }

  assertHasModalErrorMessage(message: string): void {
    this.findByElementId('modal-error-message').should('contain.text', message);
  }

  private buildPostUrl(): string {
    let url = 'issues/';

    switch (this.mocks.bindableEntity) {
      case 'EXECUTION_TYPE':
        url += 'execution/';
        break;
      case 'EXECUTION_STEP_TYPE':
        url += 'execution-step/';
        break;
      default:
        throw new Error('not implemented');
    }

    url += '*/';

    switch (this.mocks.issueType) {
      case 'standard':
        url += 'new-issue';
        break;
      case 'advanced':
        url += 'new-advanced-issue';
        break;
      case 'oslc':
        url += 'new-oslc-issue';
        break;
      default:
        throw new Error('not implemented');
    }

    return url;
  }

  clickAddAnotherButton() {
    this.find(selectByDataTestDialogButtonId('add-another')).click();
  }

  clickSearchButton() {
    this.find(selectByDataTestDialogButtonId('search-button')).click();
  }
}

export interface ReportIssueDialogMocks {
  remoteProjectNames: string[];
  remoteIssue: RemoteIssue;
  bindableEntity: IssueBindableEntity;
  issueType: 'standard' | 'advanced' | 'oslc';
  attachMode: boolean;
  searchForm: RemoteIssueSearchForm;
}
