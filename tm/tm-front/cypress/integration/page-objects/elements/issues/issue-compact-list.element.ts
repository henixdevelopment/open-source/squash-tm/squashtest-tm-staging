import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { ElementSelectorFactory } from '../forms/abstract-form-field.element';
import {
  selectByDataTestElementId,
  selectByDataTestDialogButtonId,
  selectByDataTestFieldId,
} from '../../../utils/basic-selectors';
import Chainable = Cypress.Chainable;

const dialogComponentSelector = 'sqtm-core-confirm-delete-dialog';

export class IssueCompactListElement {
  constructor(private elementSelectorFactory: ElementSelectorFactory) {}

  checkIssueRemoteIdLabel(issueIndex: number, expectedId: string) {
    this.findByFieldId(issueIndex, 'remoteId').should('contain.text', expectedId);
  }

  checkIssueBtProjectLabel(issueIndex: number, expectedBtProject: string) {
    this.findByFieldId(issueIndex, 'btProject').should('contain.text', expectedBtProject);
  }

  checkIssueSummaryLabel(issueIndex: number, expectedSummary: string) {
    this.findByFieldId(issueIndex, 'summary').should('contain.text', expectedSummary);
  }

  checkIssuePriorityLabel(issueIndex: number, expectedPriority: string) {
    this.findByFieldId(issueIndex, 'priority').should('contain.text', expectedPriority);
  }

  checkIssueStatusLabel(issueIndex: number, expectedStatus: string) {
    this.findByFieldId(issueIndex, 'status').should('contain.text', expectedStatus);
  }

  unbindOneIssue(issueIndex: number) {
    this.findByFieldId(issueIndex, 'delete-issue').click();

    const deleteMock = new HttpMockBuilder('issues/*').delete().build();

    this.clickConfirmDeleteButton();

    deleteMock.wait();
  }

  checkRowCount(expectedRowCount: number) {
    this.elementSelectorFactory()
      .find(selectByDataTestFieldId('remoteId'))
      .should('have.length', expectedRowCount);
  }

  private clickConfirmDeleteButton() {
    cy.get(dialogComponentSelector)
      .find(selectByDataTestDialogButtonId('confirm'))
      .click()
      // Then
      .get(dialogComponentSelector)
      .should('not.exist');
  }

  private findByFieldId(issueIndex: number, fieldId: FieldId): Chainable {
    return this.getIssueDataRow(issueIndex).find(selectByDataTestFieldId(fieldId));
  }

  private getIssueDataRow(issueIndex: number): Chainable {
    return this.elementSelectorFactory().find(selectByDataTestElementId(`issue-${issueIndex}`));
  }
}

type FieldId = 'remoteId' | 'btProject' | 'summary' | 'priority' | 'status' | 'delete-issue';
