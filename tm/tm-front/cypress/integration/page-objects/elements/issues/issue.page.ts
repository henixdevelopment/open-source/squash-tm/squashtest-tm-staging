import { Page } from '../../pages/page';
import { BugTrackerConnectionDialog } from './bugtracker-connection-dialog.element';

export class IssuePage extends Page {
  constructor(selector: string) {
    super(selector);
  }

  openConnectionDialog(): BugTrackerConnectionDialog {
    cy.get(`button[data-test-button-id="login"]`).click();
    return new BugTrackerConnectionDialog();
  }
}
