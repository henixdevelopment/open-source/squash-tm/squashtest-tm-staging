import { HttpMock, HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { BasicElement } from '../abstract-element';
import Chainable = Cypress.Chainable;

export class DateFilterWidgetElement extends BasicElement {
  private get operationSelector(): Chainable {
    return this.find('sqtm-core-operation-selector');
  }

  constructor(public url: string) {
    super('sqtm-core-date-filter');
  }

  assertExists() {
    super.assertExists();
    this.findByElementId('update-button').should('contain.text', 'Mettre à jour');
    this.findByElementId('cancel-button').should('exist');
    this.findByElementId('cancel-button').should('contain.text', 'Annuler');
  }

  assertRangeComponentExist() {
    this.findByElementId('input-range').should('exist');
  }

  assertOperationChosen(operation: string) {
    this.operationSelector.should('contain.text', operation);
  }

  fillTodayDate() {
    const datePicker = this.findByElementId('input-start');
    datePicker.click();
    cy.get('.ant-picker-today-btn').click();
  }

  fillRange(input: string) {
    this.findByElementId('input-range').type(input);
  }

  update(response: GridResponse = { dataRows: [] }) {
    let mock: HttpMock<GridResponse>;

    if (this.url) {
      mock = new HttpMockBuilder<GridResponse>(this.url).post().responseBody(response).build();
    }

    this.findByElementId('update-button').click();

    if (mock) {
      mock.wait();
    }
  }

  cancel() {
    this.findByElementId('cancel-button').click();
  }

  changeOperation(label: string) {
    // open the select menu
    this.operationSelector.find('nz-select').click();

    // click on item
    cy.get('nz-option-item').contains(label).click();
  }
}
