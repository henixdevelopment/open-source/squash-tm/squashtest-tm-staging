import { selectByDataTestElementId } from '../../../utils/basic-selectors';

export class FilterEnumSingleSelectWidgetElement {
  private selectBaseComponent() {
    return `sqtm-core-filter-enum-single-select`;
  }
  assertExists() {
    cy.get(this.selectBaseComponent()).should('exist');
  }

  selectFilter(filterVersion: string) {
    cy.get(this.selectBaseComponent()).contains(filterVersion).click();
  }

  assertFilterElementIsSelected(filter: string) {
    cy.get(selectByDataTestElementId('grid-filter-field-value')).should('contain.text', filter);
  }
}
