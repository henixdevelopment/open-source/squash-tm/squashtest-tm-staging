import Chainable = Cypress.Chainable;
import { HttpMock, HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { selectByDataTestElementId } from '../../../utils/basic-selectors';
import { BasicElement } from '../abstract-element';

export class GroupedMultiListElement extends BasicElement {
  // For end to end test, url is not required
  constructor(public url?: string) {
    super('sqtm-core-grouped-multi-list');
  }

  assertGroupExist(label: string) {
    this.selectGroup(label).should('exist');
  }

  assertGroupNotExist(label: string) {
    this.selectGroup(label).should('not.exist');
  }

  assertGroupPanelExists() {
    this.findByElementId('ungrouped-items').should('exist');
  }

  assertSearchFieldExists() {
    this.findByElementId('search-field').should('exist');
  }

  toggleOneItem(label: string) {
    this.findItem(label).click();
  }

  itemContains(label: string) {
    this.findItem(label).should('contain', label);
  }

  toggleOneItemAsync(label: string, response: GridResponse = { dataRows: [] }) {
    let mock: HttpMock<GridResponse>;
    if (this.url) {
      mock = new HttpMockBuilder<GridResponse>(this.url).post().responseBody(response).build();
    }
    this.findItem(label).click();

    if (mock != null) {
      mock.wait();
    }
  }

  assertCheckBoxExists(label: string) {
    this.findItem(label).find('sqtm-core-simple-checkbox').should('exist');
  }

  assertGroupContain(groupLabel: string, items: string[]) {
    items.forEach((item) => {
      this.selectItemInGroup(groupLabel, item).should('exist');
    });
  }

  checkGroupConformity(groupLabel: string, items: string[]) {
    items.forEach((item) => {
      this.findItem(item).find('.ant-checkbox').should('exist');
      this.selectItemInGroup(groupLabel, item).should('exist');
    });
  }

  assertItemCount(expectedItemCount: number) {
    this.findByElementId('ungrouped-items')
      .find(selectByDataTestElementId('item'))
      .should('have.length', expectedItemCount);
  }

  assertItemExist(itemLabel: string) {
    return this.findItem(itemLabel).should('exist');
  }

  assertItemNotExist(itemLabel: string) {
    return this.findItem(itemLabel).should('not.exist');
  }

  assertItemIsSelected(itemLabel: string) {
    this.findItem(itemLabel).find('.ant-checkbox-checked').should('exist');
  }

  assertItemIsSelectedAndPulledUp(itemLabel: string) {
    this.findItem(itemLabel).find('.ant-checkbox-checked').should('exist');
    this.findByElementId('selected-items')
      .find(selectByDataTestElementId('item'))
      .contains(itemLabel)
      .should('exist');
  }

  assertNoItemIsSelected() {
    this.find('.ant-checkbox-checked').should('not.exist');
  }

  clickOnSearchField() {
    this.findByElementId(`search-field`).click();
  }

  searchValue(str: string) {
    this.findByElementId(`search-field`).find(selectByDataTestElementId('input-filter')).type(str);
  }

  clearSearchValue() {
    this.findByElementId(`search-field`).find(selectByDataTestElementId('input-filter')).clear();
  }

  filterValues(str: string) {
    this.findByElementId('input-filter').type(str);
  }

  clearFilter() {
    this.findByElementId('input-filter').clear();
  }

  close() {
    cy.clickVoid();
    this.assertNotExist();
  }

  private selectGroup(label: string): Chainable {
    return this.findByElementId('group').contains(label);
  }

  private selectItemInGroup(groupLabel: string, itemLabel: string) {
    if (groupLabel === 'ungrouped-items') {
      return this.findByElementId('ungrouped-items')
        .parent()
        .find(selectByDataTestElementId('item'))
        .contains(itemLabel);
    } else {
      return this.findByElementId('group')
        .contains(groupLabel)
        .parent()
        .find(selectByDataTestElementId('item'))
        .contains(itemLabel);
    }
  }

  findItem(label: string): Chainable {
    return this.findByElementId('item').contains('div', label);
  }
}

export class SearchOnTagElement extends GroupedMultiListElement {
  constructor(public url: string) {
    super(url);
  }

  changeOperation(label: string, response: GridResponse = { dataRows: [] }) {
    let mock: HttpMock<GridResponse>;

    if (this.url) {
      mock = new HttpMockBuilder<GridResponse>(this.url).post().responseBody(response).build();
    }

    // open the select menu
    this.findByElementId('operation').find('nz-select').click();

    // click on item
    cy.contains('nz-option-item div', label).click();

    if (mock != null) {
      mock.wait();
    }

    cy.contains('nz-option-item div', label).should('not.exist');
  }

  assertOperationChosen(label: string) {
    this.findByElementId('operation').find('nz-select').should('contain.text', label);
  }
}
