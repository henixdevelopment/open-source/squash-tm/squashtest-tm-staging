import { BasicElement } from '../abstract-element';
import Chainable = Cypress.Chainable;
import { selectByDataTestElementId } from '../../../utils/basic-selectors';

export class TextFilterWidgetElement extends BasicElement {
  override findByElementId(id: string): Chainable {
    return super.findByElementId('edit-filter-text').find(selectByDataTestElementId(id));
  }

  constructor() {
    super('sqtm-core-edit-filter-text');
  }

  assertExists() {
    super.assertExists();
    this.findByElementId('update-button').should('exist');
    this.findByElementId('update-button').should('contain.text', 'Mettre à jour');
    this.findByElementId('cancel-button').should('exist');
    this.findByElementId('cancel-button').should('contain.text', 'Annuler');
  }

  typeAndConfirm(input: string) {
    this.findByElementId('input').type(input);
    this.findByElementId('update-button').click();
  }

  cancel() {
    this.findByElementId('cancel-button').click();
  }

  private get operationSelector(): Chainable {
    return cy.get('sqtm-core-operation-selector');
  }

  changeOperation(label: string) {
    // open the select menu
    this.operationSelector.find('nz-select').click();

    // click on item
    cy.get('nz-option-item').contains(label).click();
  }
}
