import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { BasicElement } from '../abstract-element';

export class NumericSetFilterWidgetElement extends BasicElement {
  constructor(public url: string) {
    super('sqtm-core-numeric-set-filter');
  }

  assertExists() {
    this.assertExistWithOutOperationSelector();
  }

  assertExistWithOutOperationSelector() {
    super.assertExists();
    this.findByElementId('input').should('exist');
    this.findByElementId('update-button').should('contain.text', 'Mettre à jour');
    this.findByElementId('cancel-button').should('exist');
    this.findByElementId('cancel-button').should('contain.text', 'Annuler');
  }

  fillInput(input: string) {
    this.findByElementId('input').clear().type(input);
  }

  update(response: GridResponse = { dataRows: [] }) {
    if (this.url) {
      new HttpMockBuilder<GridResponse>(this.url).post().responseBody(response).build();
    }
    this.findByElementId('update-button').click();
  }

  cancel() {
    this.findByElementId('cancel-button').click();
  }
}
