import Chainable = Cypress.Chainable;
import { selectByDataTestListItemId } from '../../../utils/basic-selectors';

export class ListPanelElement {
  protected htmlTag = 'sqtm-core-list-panel';

  protected selectById(id: string) {
    return `[data-test-element-id="${id}"]`;
  }

  assertExists() {
    cy.get(this.htmlTag).should('exist');
  }

  assertNotExist() {
    cy.get(this.htmlTag).should('not.exist');
  }

  toggleOneItem(label: string) {
    this.findItem(label).click();
  }

  assertItemExist(itemLabel: string) {
    return this.findItem(itemLabel).should('exist');
  }

  assertItemNotExist(itemLabel: string) {
    return this.findItem(itemLabel).should('not.exist');
  }

  filterValues(str: string) {
    cy.get(this.selectById('input-filter')).type(str);
  }

  close() {
    cy.clickVoid();
  }

  findItem(label: string): Chainable {
    return cy.get(this.htmlTag).find(selectByDataTestListItemId('list-item')).contains(label);
  }
}
