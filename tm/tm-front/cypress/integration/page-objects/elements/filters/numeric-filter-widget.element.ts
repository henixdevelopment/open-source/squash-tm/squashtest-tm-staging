import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { BasicElement } from '../abstract-element';
import Chainable = Cypress.Chainable;

export class NumericFilterWidgetElement extends BasicElement {
  constructor(public url: string) {
    super('sqtm-core-numeric-filter');
  }

  private get operationSelector(): Chainable {
    return this.find('sqtm-core-operation-selector');
  }

  assertExists() {
    this.assertExistWithOutOperationSelector();
    this.operationSelector.should('exist');
  }

  assertExistWithOutOperationSelector() {
    super.assertExists();
    this.findByElementId('input-min').should('exist');
    this.findByElementId('update-button').should('contain.text', 'Mettre à jour');
    this.findByElementId('cancel-button').should('exist');
    this.findByElementId('cancel-button').should('contain.text', 'Annuler');
  }

  assertOperationChosen(operation: string) {
    this.operationSelector.should('contain.text', operation);
  }

  fillInputMin(input: string) {
    this.findByElementId('input-min').find('input').clear().type(input);
  }

  fillInputMax(input: string) {
    this.findByElementId('input-max').find('input').clear().type(input);
  }

  update(response: GridResponse = { dataRows: [] }) {
    if (this.url) {
      new HttpMockBuilder<GridResponse>(this.url).post().responseBody(response).build();
    }
    this.findByElementId('update-button').click();
  }

  cancel() {
    this.findByElementId('cancel-button').click();
  }

  changeOperation(label: string) {
    // open the select menu
    this.operationSelector.find('nz-select').click();

    // click on item
    cy.get('nz-option-item').contains(label).click();
  }
}
