import { BasicElement } from '../abstract-element';
import { selectByDataTestElementId } from '../../../utils/basic-selectors';

export class AutomatedExecutionFailureDetailsElement extends BasicElement {
  constructor() {
    super('sqtm-app-orchestrator-execution-failure-details');
  }

  assertSectionIsNotDisplayed() {
    cy.get(selectByDataTestElementId('failure-detail-section')).should('not.exist');
  }

  assertNoFailureDetailLabelIsDisplayed() {
    cy.get(selectByDataTestElementId('empty-failure-detail-section'))
      .should('exist')
      .should('contain.text', 'Aucun élément à afficher');
  }

  assertTitleIsDisplayed() {
    cy.get(selectByDataTestElementId('failure-detail-title')).should(
      'contain.text',
      'Assertions en erreur',
    );
  }

  assertTitleIsNotDisplayed() {
    cy.get(selectByDataTestElementId('failure-detail-title')).should('not.exist');
  }

  assertFailureDetailIsDisplayed(assertionNumber: string, content: string) {
    cy.get(selectByDataTestElementId('failure-detail-' + assertionNumber)).should(
      'contain.text',
      'Assertion ' + assertionNumber,
    );
    cy.get(selectByDataTestElementId('failure-detail-content-' + assertionNumber)).should(
      'contain.text',
      content,
    );
  }
}
