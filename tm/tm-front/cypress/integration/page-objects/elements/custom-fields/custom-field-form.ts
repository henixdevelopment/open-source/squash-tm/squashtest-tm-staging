import { RichTextFieldElement } from '../forms/RichTextFieldElement';
import Chainable = Cypress.Chainable;
import {
  CustomFieldBindingData,
  ProjectData,
} from '../../../../../projects/sqtm-core/src/lib/model/project/project-data.model';
import { BindableEntity } from '../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { InputType } from '../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { CustomField } from '../../../../../projects/sqtm-core/src/lib/model/customfield/customfield.model';

export class CustomFieldForm {
  constructor(
    private project?: ProjectData,
    private domain?: BindableEntity,
  ) {}

  fillCustomField(cufId: number, value: any) {
    this.typeInCustomField(cufId, value);
  }

  clearCustomField(cufId: number) {
    const customField = this.getCustomFields().find((value) => value.id === cufId);
    switch (customField.inputType) {
      case InputType.PLAIN_TEXT:
        this.clearTextField(cufId);
        break;
      case InputType.NUMERIC:
        this.clearNumericField(cufId);
        break;
      case InputType.TAG:
        this.clearTagField(cufId);
        break;
      case InputType.RICH_TEXT:
        this.clearRichField(cufId);
        break;
      default:
        throw new Error(`Non clearable field ${customField.name} of type ${customField.inputType}`);
    }
  }

  toggleTagFieldOptions(cufId: number, ...indexes: number[]) {
    // Show options
    this.getTagField(cufId).click();
    // Get options and click them
    this.getTagFieldOptions()
      .then((children) => {
        indexes.forEach((optionIndex) => children[optionIndex].click());
      })
      .then(() => {
        // Click again to hide overlay menu
        // We need to force because cypress may complain about the element being covered by the dropdown menu...
        this.getTagField(cufId).click({ force: true });
      })
      .then(() => {
        cy.clickVoid();
      });
  }

  assertHasError(cufId: number, expectedError: string) {
    cy.get(`[data-test-element-id="custom-field-form"]`)
      .should('exist')
      .find(`div[data-test-cuf-id-error="${cufId}"]`)
      .should('exist')
      .should('have.attr', 'data-test-error-key', expectedError);
  }

  assertHasNoError(cufId: number) {
    cy.get(
      `
      [data-test-element-id="custom-field-form"]
      [data-test-cuf-id-error="${cufId}"]
    `,
    ).should('not.exist');
  }

  private clearTextField(cufId: number) {
    this.getTextField(cufId).clear();
  }

  private getTextField(cufId: number): Chainable<any> {
    return cy.get(`
    [data-test-element-id="custom-field-form"]
    [data-test-cuf-text-input-id="${cufId}"]
    `);
  }

  private clearNumericField(cufId: number) {
    this.getNumericField(cufId).clear();
  }

  private getNumericField(cufId: number): Chainable<any> {
    return cy.get(`
    [data-test-element-id="custom-field-form"]
    [data-test-cuf-numeric-input-id="${cufId}"]
    input
    `);
  }

  private clearTagField(cufId: number) {
    this.getTagFieldRemoveButtons(cufId).then((elements) => {
      elements.each((index, element) => {
        element.click();
      });
    });
  }

  private getTagField(cufId: number): Chainable<any> {
    return cy.get(`
      [data-test-element-id="custom-field-form"]
      [data-test-cuf-tag-input-id="${cufId}"]
    `);
  }

  private getTagFieldOptions(): Chainable<any> {
    // This is a modal overlay so we should only have one dropdown...
    return cy.get(`
      nz-option-item
    `);
  }

  private getTagFieldRemoveButtons(cufId: number): Chainable<any> {
    // This will fail if the field has already been cleared !
    return cy.get(`
      [data-test-element-id="custom-field-form"]
      [data-test-cuf-tag-input-id="${cufId}"]
      .ant-select-selection-item-remove
    `);
  }

  private clearRichField(cufId: number) {
    this.getRichField(cufId).clear();
  }

  private typeInCustomField(cufId: number, value: any) {
    const customField = this.getCustomFields().find((cuf) => cuf.id === cufId);
    switch (customField.inputType) {
      case InputType.PLAIN_TEXT:
        this.getTextField(cufId).type(value);
        break;
      case InputType.NUMERIC:
        this.getNumericField(cufId).type(value);
        break;
      case InputType.TAG:
        this.toggleTagFieldOptions(cufId, value);
        break;
      case InputType.RICH_TEXT:
        this.getRichField(cufId).fill(value);
        break;
      default:
        throw new Error(`Unhandled type ${customField.inputType} for field ${customField.name}`);
    }
  }

  private getRichField(cufId: number): RichTextFieldElement {
    return new RichTextFieldElement(cufId.toString());
  }

  private getCustomFields(): CustomField[] {
    if (this.domain && this.project) {
      return this.getCustomFieldBindings().map((value) => value.customField);
    }
    return [];
  }

  private getCustomFieldBindings(): CustomFieldBindingData[] {
    if (this.domain && this.project) {
      return this.project.customFieldBinding[this.domain];
    }
    return [];
  }
}
