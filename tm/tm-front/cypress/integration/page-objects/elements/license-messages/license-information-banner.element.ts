import Chainable = Cypress.Chainable;
import { AlertDialogElement } from '../dialog/alert-dialog.element';

export class LicenseInformationBannerElement {
  constructor() {}

  get selector(): Chainable {
    return cy.get('sqtm-core-license-information-banner>div');
  }

  assertDoesNotExist(): void {
    this.selector.should('not.exist');
  }

  assertIsVisible(): void {
    this.selector.should('be.visible');
  }

  openDetailsDialog(): AlertDialogElement {
    this.selector.find('span').click();
    return new AlertDialogElement('license-information-detail');
  }
}
