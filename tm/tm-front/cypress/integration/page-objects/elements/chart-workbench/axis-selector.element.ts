import { selectByDataTestElementId } from '../../../utils/basic-selectors';
import { CustomReportAttributeSelector } from '../grid/grid.element';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { ChartDefinitionModel } from '../../../../../projects/sqtm-core/src/lib/model/custom-report/chart-definition.model';

export class AxisSelectorElement {
  private readonly treeElement = new CustomReportAttributeSelector(
    'overlay-column-prototype-selector',
  );

  constructor(public id = 'axis') {}

  assertExists() {
    this.getBaseDomNode().should('exist');
  }

  private getBaseDomNode() {
    return cy.get(`sqtm-app-axis-selector [data-test-element-id="${this.id}"]`);
  }

  assertNoAttributeSelected() {
    this.getBaseDomNode().should('contain.text', "Pas d'attribut");
  }

  openAttributeSelector(): CustomReportAttributeSelector {
    this.getBaseDomNode().find(selectByDataTestElementId('columnField')).click();
    this.treeElement.assertExists();
    return this.treeElement;
  }

  addAttributeAndRenderPreview(
    id: number,
    projectId: string = '*',
    chartDefinition?: ChartDefinitionModel,
  ): void {
    const httpMock = new HttpMockBuilder(`chart-workbench/preview/${projectId}`)
      .post()
      .responseBody(chartDefinition)
      .build();
    this.treeElement.assertExists();
    this.treeElement.selectRow(id, 'NAME');
    httpMock.wait();
  }

  addAttributeToChart(id: number, filter?: string): void {
    this.treeElement.assertExists();
    if (filter) {
      this.treeElement.filterAttribute(filter);
    }
    this.treeElement.selectRow(id, 'NAME');
  }

  checkOption(option: string) {
    this.getBaseDomNode().find('nz-select-item').should('contain', option);
  }

  selectOption(option: string) {
    this.getBaseDomNode().find('nz-select-item').click();
    cy.get('nz-option-item').contains(option).click();
  }

  inputFilterAndClickUpdateButtonInFilter(filterInput: string) {
    cy.get('sqtm-core-numeric-filter')
      .find(selectByDataTestElementId('input-min'))
      .type(filterInput);
    cy.get('sqtm-core-numeric-filter').find(selectByDataTestElementId('update-button')).click();
  }

  assertAttributeContain(expectedText: string) {
    this.getBaseDomNode().should('contain.text', expectedText);
  }
}
