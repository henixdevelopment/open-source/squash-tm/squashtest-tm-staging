import { ElementSelectorFactory } from './forms/abstract-form-field.element';
import {
  selectByDataTestElementId,
  selectByDataTestFieldId,
  selectByDataTestFieldName,
} from '../../utils/basic-selectors';
import Chainable = Cypress.Chainable;

export abstract class AbstractElement {
  abstract rootChainableFactory: ElementSelectorFactory;

  get rootElement(): Chainable {
    return this.rootChainableFactory();
  }

  find(selector: string): Chainable {
    return this.rootElement.find(selector);
  }

  findByElementId(componentId: string): Chainable {
    return this.find(selectByDataTestElementId(componentId));
  }

  findByFieldName(fieldName: string): Chainable {
    return this.find(selectByDataTestFieldName(fieldName));
  }

  findByFieldId(fieldId: string) {
    return this.find(selectByDataTestFieldId(fieldId));
  }

  assertExists(): void {
    this.rootElement.should('exist');
  }

  assertNotExist(): void {
    this.rootElement.should('not.exist');
  }
}

export class BasicElement extends AbstractElement {
  constructor(protected readonly rootSelector: string) {
    super();
  }

  override rootChainableFactory: ElementSelectorFactory = () => {
    return cy.get(this.rootSelector);
  };
}
