import { EditableRichTextFieldElement } from '../forms/editable-rich-text-field.element';
import { fillCuf } from '../../../scenarios/scenario-parts/custom-field.part';
import { InputType } from '../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';

export class FolderInformationPanelElement {
  constructor(private folderId?: number | string) {}

  get descriptionRichField(): EditableRichTextFieldElement {
    if (this.folderId) {
      const url = `test-case-folder/${this.folderId}/description`;
      return new EditableRichTextFieldElement('description', url);
    } else {
      return new EditableRichTextFieldElement('description');
    }
  }

  clickOnCufCheckbox(cufName: string) {
    fillCuf(cufName, InputType.CHECKBOX);
  }

  addCufTag(cufName: string, cufContent: string) {
    fillCuf(cufName, InputType.TAG, cufContent);
  }
}
