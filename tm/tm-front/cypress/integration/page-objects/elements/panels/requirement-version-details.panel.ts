export class RequirementVersionDetailsPanel {
  private readonly rootSelector = 'sqtm-app-requirement-version-details';

  assertExists() {
    cy.get(this.rootSelector).should('exist');
  }

  checkField(name: string, expectedValue: string) {
    cy.get(this.rootSelector)
      .find('.label')
      .contains(name)
      .next()
      .should('contain.text', expectedValue);
  }

  checkDescription(expected: string) {
    cy.get(this.rootSelector)
      .find('.label')
      .contains('Description')
      .next()
      .should('contain.html', expected);
  }
}
