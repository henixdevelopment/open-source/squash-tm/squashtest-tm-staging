import {
  selectByDataTestAnchorLinkId,
  selectByDataTestAnchorLinkIdCounter,
} from '../../../utils/basic-selectors';

/**
 * The generics allow to make sure `clickLink` won't be called with a wrong `linkId`.
 * Exemple use :
 * ```
 * const anchors = AnchorsElement.withLinkIds('information', 'steps');
 *
 * anchors.clickLink('information');  // OK
 *
 * anchors.clickLink('foobar');       // Error
 * ```
 */
export class AnchorsElement<LINKS extends readonly string[]> {
  static withLinkIds<LINKS extends readonly string[]>(...linkIds: LINKS) {
    return new AnchorsElement(linkIds);
  }

  private constructor(private readonly linkIds: LINKS) {}

  public clickLink(linkId: (typeof this.linkIds)[number]): void {
    this.assertLinkIsDefined(linkId);

    this.selectLinkById(linkId).click();
    this.selectLinkById(linkId).trigger('mouseleave');

    cy.removeNzTooltip();
  }

  assertLinkExists(linkId: (typeof this.linkIds)[number]): void {
    this.assertLinkIsDefined(linkId);

    this.selectLinkById(linkId).should('exist');
  }

  assertLinkIsActive(linkId: (typeof this.linkIds)[number]): void {
    this.assertLinkIsDefined(linkId);

    this.selectLinkById(linkId).should('have.class', 'container-active');
  }

  assertAnchorCountExists(linkId: string) {
    this.selectCounterById(linkId).should('exist');
  }

  assertAnchorCountNotExists(linkId: string) {
    this.selectCounterById(linkId).should('not.exist');
  }

  assertAnchorCounterEquals(linkId: string, expected: number): void {
    this.selectCounterById(linkId).contains(expected);
  }

  private assertLinkIsDefined(linkId: string): void {
    if (!this.linkIds.includes(linkId)) {
      throw new Error('You should register an anchor link before trying to click it.');
    }
  }

  private selectLinkById(linkId: string): Cypress.Chainable<JQuery<HTMLElement>> {
    return cy.get(selectByDataTestAnchorLinkId(linkId));
  }

  private selectCounterById(linkId: string): Cypress.Chainable<JQuery<HTMLElement>> {
    return cy.get(selectByDataTestAnchorLinkIdCounter(linkId));
  }
}
