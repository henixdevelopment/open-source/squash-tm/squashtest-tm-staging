import { selectByDataTestElementId } from '../../../utils/basic-selectors';
import {
  basicReferentialData,
  ReferentialDataProviderBuilder,
} from '../../../utils/referential/referential-data.provider';
import { TestCaseSearchModel } from '../../pages/test-case-workspace/research/test-case-search-model';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { GridElement } from '../grid/grid.element';
import { TestCaseSearchPage } from '../../pages/test-case-workspace/research/test-case-search-page';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { ReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';

// for now we have only pie chart
// please refactor this to an abstract class and subclass for other types when needed
export class ChartElement {
  constructor(private chartId: string) {}

  assertChartExist() {
    const selector = `${selectByDataTestElementId(this.chartId)} .plotly`;
    cy.get(selector).should('exist');
  }

  assertHasTitle(expectedTitle: string) {
    const selector = `${selectByDataTestElementId(this.chartId)} .infolayer .gtitle`;
    cy.get(selector).should('contain.text', expectedTitle);
  }

  private clickOnLegend(chartZoneIndex: number) {
    const selector = `${selectByDataTestElementId(this.chartId)}`;
    cy.get(selector)
      .find('.legend')
      .find('.traces')
      .eq(chartZoneIndex)
      .should('be.visible')
      .click();
  }

  goToResearchPage(
    chartZoneIndex: number,
    referentialData: ReferentialDataModel = basicReferentialData,
    initialNodes: GridResponse = { dataRows: [] },
    testCaseResearchModel: TestCaseSearchModel = {
      usersWhoCreatedTestCases: [],
      usersWhoModifiedTestCases: [],
    },
  ): TestCaseSearchPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const pageDataMock = new HttpMockBuilder<TestCaseSearchModel>('search/test-case')
      .responseBody(testCaseResearchModel)
      .build();
    const grid = GridElement.createGridElement(
      'test-case-search',
      'search/test-case',
      initialNodes,
    );
    // visit page
    this.clickOnLegend(chartZoneIndex);
    // wait for ref data request to fire
    referentialDataProvider.wait();
    pageDataMock.wait();
    // wait for initial tree data request to fire
    return new TestCaseSearchPage(grid);
  }
}
