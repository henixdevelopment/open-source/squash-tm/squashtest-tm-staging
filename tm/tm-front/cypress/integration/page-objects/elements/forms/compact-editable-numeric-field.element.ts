import { AbstractFormFieldElement, ElementSelectorFactory } from './abstract-form-field.element';

export class CompactEditableNumericFieldElement extends AbstractFormFieldElement {
  constructor(selectorOrFieldId: ElementSelectorFactory | string) {
    super(selectorOrFieldId);
  }

  setValue(newValue: number) {
    this.find('span').click();
    this.find('input').clear();
    this.find('input').type(newValue.toString());
  }

  confirm(): void {
    this.find('input').type('{enter}');
  }
}
