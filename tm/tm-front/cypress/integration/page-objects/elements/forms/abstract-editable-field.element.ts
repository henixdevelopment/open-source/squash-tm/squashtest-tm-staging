import { AbstractFormFieldElement, ElementSelectorFactory } from './abstract-form-field.element';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';

export class AbstractEditableFieldElement extends AbstractFormFieldElement {
  constructor(
    selectorOrFieldId: ElementSelectorFactory | string,
    private readonly url: string = '',
  ) {
    super(selectorOrFieldId);
  }

  setAndConfirmValue(newValue: string, response?) {
    this.setValue(newValue);
    this.confirm(response);
  }

  setValue(newValue: string) {
    this.find('span').click();
    this.checkEditMode(true);
    this.find('input').clear();
    this.find('input').type(newValue);
  }

  confirm(response?): void {
    // Click on button
    if (this.url != null) {
      let mock;
      if (response) {
        mock = new HttpMockBuilder(this.url).post().responseBody(response).build();
      } else {
        mock = new HttpMockBuilder(this.url).post().build();
      }
      this.find('button').first().click();
      mock.wait();
    } else {
      this.find('button').first().click();
    }

    this.checkEditMode(false);
  }

  clickOnConfirmButton() {
    this.find('button').first().click();
  }

  cancel(): void {
    this.find('button').eq(1).click();
    this.checkEditMode(false);
  }

  checkContent(value: string): void {
    this.find('span').should('contain.text', value);
  }

  checkEditMode(shouldBeInEditMode: boolean) {
    const chainer = shouldBeInEditMode ? 'exist' : 'not.exist';
    this.find('button').should(chainer);
  }

  checkPlaceholder() {
    this.checkContent('(Cliquer pour éditer...)');
  }

  checkCustomPlaceholder(expectedText: string): void {
    this.checkContent(expectedText);
  }

  assertIsEditable() {
    this.findDivWrapper().should('have.class', 'editable');
  }

  assertIsNotEditable() {
    this.findDivWrapper().should('not.have.class', 'editable');
  }

  assertIsNotClickable(): void {
    this.findDivWrapper().should('not.have.class', '__hover_pointer');
  }

  assertHasError(errorMessage: string) {
    this.find('.sqtm-core-error-message').should('be.visible');
    this.find('.sqtm-core-error-message').should('have.text', errorMessage);
  }

  private findDivWrapper() {
    return this.find('div').should('exist');
  }

  assertIsNotVisible(): void {
    this.rootElement.should('not.be.visible');
  }
}
