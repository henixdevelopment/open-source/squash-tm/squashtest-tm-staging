import Chainable = Cypress.Chainable;
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { AbstractFormFieldElement, ElementSelectorFactory } from './abstract-form-field.element';
import { selectByDataTestButtonId } from '../../../utils/basic-selectors';

export class EditableSelectFieldElement extends AbstractFormFieldElement {
  constructor(
    selectorOrFieldId: ElementSelectorFactory | string,
    private readonly url?: string,
  ) {
    super(selectorOrFieldId);
  }

  setAndConfirmValueNoButton(newValue: string) {
    this.selectValueNoButton(newValue);
    this.checkSelectedOption(newValue);
  }

  setAndConfirmValueWithServerResponseNoButton(newValue: string, serverResponse: any) {
    this.selectValueWithServerResponseNoButton(newValue, serverResponse);
  }

  confirm() {
    this.clickConfirmButton();
  }

  assertIsEditable() {
    this.find('div').should('have.class', 'editable');
  }

  assertIsNotEditable() {
    this.find('div').should('not.have.class', 'editable');
  }

  // Show dropdown and check that the available options names match the specified string array
  checkAllOptions(options: string[]) {
    this.find('span.editable').click();
    cy.get(`nz-option-item`).each(function (value, index) {
      cy.wrap(value).should('contain.text', options[index]);
    });

    cy.clickVoid();
  }

  // Checks that the selected option (in non-edit mode) matches the specified text
  checkSelectedOption(expected: string) {
    this.find('span').should('contain.text', expected);
  }

  // Returns the Chainable for an option of the dropdown menu
  // This won't work if the dropdown isn't shown.
  private getOption(optionName: string): Chainable<any> {
    return cy.contains(`nz-option-item`, optionName);
  }

  // Shows the dropdown and click on an option
  selectValue(newValue: string) {
    this.find('span').click();
    this.getOption(newValue).click();
  }

  selectValueNoButton(newValue: string) {
    this.find('span.editable').click();
    if (this.url != null) {
      const mock = new HttpMockBuilder(this.url).post().build();
      this.getOption(newValue).click({ force: true });
      mock.wait();
    } else {
      this.getOption(newValue).click({ force: true });
    }
  }

  selectValueWithServerResponseNoButton(newValue: string, response?: any) {
    this.find('span.editable').click();
    if (this.url != null) {
      const mock = new HttpMockBuilder(this.url).post().responseBody(response).build();
      this.getOption(newValue).click();
      mock.wait();
    } else {
      this.getOption(newValue).click();
    }
  }

  private clickConfirmButton() {
    this.find(selectByDataTestButtonId('confirm')).click();
  }
}
