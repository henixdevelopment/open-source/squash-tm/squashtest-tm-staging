import { AbstractFormFieldElement, ElementSelectorFactory } from './abstract-form-field.element';

export class GenericTextFieldElement extends AbstractFormFieldElement {
  constructor(selectorOrFieldId: ElementSelectorFactory | string) {
    super(selectorOrFieldId);
  }

  checkContent(expectedContent: string) {
    this.rootElement.should('contain.text', expectedContent);
  }
}
