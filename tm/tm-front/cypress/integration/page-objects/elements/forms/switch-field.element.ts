import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { AbstractFormFieldElement, ElementSelectorFactory } from './abstract-form-field.element';

export class SwitchFieldElement extends AbstractFormFieldElement {
  constructor(
    selectorOrFieldId: ElementSelectorFactory | string,
    private readonly url?: string,
  ) {
    super(selectorOrFieldId);
  }

  toggle() {
    if (this.url) {
      const mock = new HttpMockBuilder(this.url).post().build();
      this.find('button').click();
      mock.wait();
    } else {
      this.find('button').click();
    }
  }

  checkValue(shouldBeChecked: boolean) {
    const chainer = shouldBeChecked ? 'have.class' : 'not.have.class';
    this.find('button').should(chainer, 'ant-switch-checked');
  }

  assertIsVisible() {
    this.rootElement.should('be.visible');
  }
}
