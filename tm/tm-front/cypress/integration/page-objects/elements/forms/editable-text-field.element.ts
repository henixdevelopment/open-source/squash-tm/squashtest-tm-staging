import { AbstractEditableFieldElement } from './abstract-editable-field.element';
import { ElementSelectorFactory } from './abstract-form-field.element';

export class EditableTextFieldElement extends AbstractEditableFieldElement {
  constructor(selectorOrFieldId: ElementSelectorFactory | string, url: string = '') {
    super(selectorOrFieldId, url);
  }

  clearTextField(): void {
    this.find('span').click();
    this.clearInEditMode();
    this.clickOnConfirmButton();
  }

  clearInEditMode(): void {
    this.checkEditMode(true);
    this.find('input').clear();
  }

  fillWithString(value: string) {
    this.rootElement.type(value);
  }
}
