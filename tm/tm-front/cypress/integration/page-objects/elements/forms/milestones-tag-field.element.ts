import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { MultipleMilestonePickerDialogElement } from '../dialog/multiple-milestone-picker-dialog.element';
import { AbstractFormFieldElement, ElementSelectorFactory } from './abstract-form-field.element';
import { selectByDataTestDialogButtonId } from '../../../utils/basic-selectors';

export class MilestonesTagFieldElement extends AbstractFormFieldElement {
  private openDialogSelector = '[data-test-button-id="open-milestone-dialog"]';

  constructor(
    selectorOrFieldId: ElementSelectorFactory | string,
    private readonly url?: string,
  ) {
    super(selectorOrFieldId);
  }

  openMilestoneDialog(): MultipleMilestonePickerDialogElement {
    this.find(this.openDialogSelector).click();
    return new MultipleMilestonePickerDialogElement();
  }

  openMilestoneDialogNotExist() {
    this.find(this.openDialogSelector).should('not.exist');
  }

  clickOnRemoveMilestoneClose(milestoneName): void {
    this.rootElement.contains(`nz-tag`, milestoneName).find('span[nztype="close"]').click();
  }

  removeMilestoneButtonShouldNotExist(milestoneName): void {
    this.rootElement
      .contains(`nz-tag`, milestoneName)
      .find('span[nztype="close"]')
      .should('not.exist');
  }
  confirm() {
    const mock = new HttpMockBuilder(this.url).post().build();
    cy.get(selectByDataTestDialogButtonId('confirm')).click();
    mock.wait();
  }

  cancel() {
    cy.get(selectByDataTestDialogButtonId('cancel')).click();
  }

  // Checks the tags currently in the field
  checkMilestones(...values: string[]) {
    for (const value of values) {
      this.rootElement.contains(`nz-tag`, value).should('exist');
    }

    this.find(`nz-tag`).should('have.length', values.length);
  }

  assertMilestoneIsNotPresent(milestoneName: string) {
    this.rootElement.should('not.contain', milestoneName);
    this.rootElement.find('nz-tag').should('not.exist');
  }
}
