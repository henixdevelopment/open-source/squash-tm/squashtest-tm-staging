export class ActionTextFieldElement {
  constructor(protected readonly fieldName: string) {}

  get selector(): string {
    return `
    sqtm-core-action-text-field
    [data-test-field-name="${this.fieldName}"]`;
  }

  get inputSelector() {
    return `
    ${this.selector}
    input
    `;
  }

  fill(value: string) {
    cy.get(this.inputSelector).clear();

    if (value !== '') {
      cy.get(this.inputSelector).type(value);
    }
  }

  assertExists() {
    cy.get(this.selector).should('exist');
  }

  assertNotExist() {
    cy.get(this.selector).should('not.exist');
  }
}
