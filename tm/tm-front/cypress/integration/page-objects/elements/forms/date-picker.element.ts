import { AbstractFormFieldElement, ElementSelectorFactory } from './abstract-form-field.element';

export class DatePickerElement extends AbstractFormFieldElement {
  constructor(selectorFactory: ElementSelectorFactory) {
    super(selectorFactory);
  }

  selectTodayDate(): void {
    this.click();
    cy.get('.ant-picker-today-btn').click();
  }

  selectNowDateTime(): void {
    this.click();
    cy.get('.ant-picker-now').click();
    cy.get('.ant-picker-ok > .ant-btn').click();
  }
}
