import { AbstractFormFieldElement, ElementSelectorFactory } from './abstract-form-field.element';
import { SelectFieldElement } from './select-field.element';

export class CascadingSelectFieldElement extends AbstractFormFieldElement {
  public readonly firstSelect: SelectFieldElement;
  public readonly secondSelect: SelectFieldElement;

  constructor(selectorFactory: ElementSelectorFactory) {
    super(selectorFactory);

    this.firstSelect = new SelectFieldElement(() => this.findByElementId('first'));
    this.secondSelect = new SelectFieldElement(() => this.findByElementId('second'));
  }
}
