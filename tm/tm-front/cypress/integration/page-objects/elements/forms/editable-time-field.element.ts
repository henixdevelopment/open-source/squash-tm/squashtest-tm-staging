import { ElementSelectorFactory } from './abstract-form-field.element';
import { AbstractEditableFieldElement } from './abstract-editable-field.element';
import { selectByDataTestInputName } from '../../../utils/basic-selectors';
import { ListPanelElement } from '../filters/list-panel.element';

export class EditableTimeFieldElement extends AbstractEditableFieldElement {
  constructor(selectorOrFieldId: ElementSelectorFactory | string, url?: string) {
    super(selectorOrFieldId, url);
  }

  enableEditMode() {
    this.find('>div').click();
  }

  checkTimeContent(expectedHour: string, expectedMinutes: string): void {
    const value = `${expectedHour}:${expectedMinutes}`;
    this.find('span').should('contain.text', value);
  }

  checkHourInputModelValue(expectedContent: string) {
    cy.clickVoid();
    this.find(selectByDataTestInputName('hour-input-number')).should(
      'have.attr',
      'data-test-input-value',
      expectedContent,
    );
  }

  checkMinutesInputModelValue(expectedContent: string) {
    cy.clickVoid();
    this.find(selectByDataTestInputName('minute-input-number')).should(
      'have.attr',
      'data-test-input-value',
      expectedContent,
    );
  }

  selectInList(selectedElement: string) {
    const listPanel = new ListPanelElement();
    listPanel.findItem(selectedElement).click();
  }

  updateTimeWithKeyboardArrows(expectedHour: number, expectedMinutes: number) {
    cy.clickVoid();
    this.pressHourArrowUp(expectedHour);
    cy.get('body').type('{enter}');
    cy.clickVoid();
    this.pressMinutesArrowUp(expectedMinutes);
  }

  private pressHourArrowUp(expectedUpArrows: number) {
    const typeHourUpArrows = this.writeExpectedArrowsUpText(expectedUpArrows);
    this.find(selectByDataTestInputName('hour-input-number')).type(`${typeHourUpArrows}`);
  }

  private pressMinutesArrowUp(expectedUpArrows: number) {
    const typeMinutesUpArrows = this.writeExpectedArrowsUpText(expectedUpArrows);
    this.find(selectByDataTestInputName('minute-input-number')).type(`${typeMinutesUpArrows}`);
  }

  private writeExpectedArrowsUpText(expectedUpArrows: number) {
    let typeHourUpArrows = '';
    for (let i = 1; i <= expectedUpArrows; i++) {
      typeHourUpArrows += '{upArrow}';
    }
    return typeHourUpArrows;
  }
}
