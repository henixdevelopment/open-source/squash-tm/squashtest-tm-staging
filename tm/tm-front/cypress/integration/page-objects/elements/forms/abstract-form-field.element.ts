import Chainable = Cypress.Chainable;
import PositionType = Cypress.PositionType;
import { AbstractElement } from '../abstract-element';
import { selectByDataTestFieldId } from '../../../utils/basic-selectors';

export type ElementSelectorFactory = () => Chainable<JQuery>;

/**
 * Common interface for form fields elements. It mainly encapsulates the HTML element query based on the selection strategy
 * passed to the constructor.
 *
 * Most of the time, you'll want to pass a string to the constructor so that it matches `[attr.data-test-field-id=MY_SELECTOR]`.
 * If you want to use another selector, pass a selector factory function instead.
 *
 * @see CommonSelectors
 */
export class AbstractFormFieldElement extends AbstractElement {
  override readonly rootChainableFactory: ElementSelectorFactory;

  /**
   * @param selectorOrFieldId either an ElementSelectorFactory or a string. In the second case, the selector
   *        factory used is `CommonSelectors.fieldName`
   */
  constructor(selectorOrFieldId: ElementSelectorFactory | string) {
    super();

    this.rootChainableFactory =
      typeof selectorOrFieldId === 'string'
        ? () => cy.get(selectByDataTestFieldId(selectorOrFieldId))
        : selectorOrFieldId;
  }

  assertContainsText(expectedText: string) {
    this.rootElement.should('contain.text', expectedText);
  }

  click(position?: PositionType) {
    this.rootElement.click(position);
  }
}
