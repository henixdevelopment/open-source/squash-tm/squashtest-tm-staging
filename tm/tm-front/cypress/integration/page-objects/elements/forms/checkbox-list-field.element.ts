import { AbstractFormFieldElement, ElementSelectorFactory } from './abstract-form-field.element';
import Chainable = Cypress.Chainable;

export class CheckboxListFieldElement extends AbstractFormFieldElement {
  constructor(selectorFactory: ElementSelectorFactory) {
    super(selectorFactory);
  }

  clickOption(optionText: string): void {
    this.findOptionWithText(optionText).click();
  }

  checkSelectedOptions(expectedCheckedOptions: string[]): void {
    expectedCheckedOptions.forEach((option) => this.findSelectedOption(option).should('exist'));
  }

  private findOptionWithText(text: string): Chainable {
    return this.rootElement.contains('span', text);
  }

  private findSelectedOption(text: string): Chainable {
    return this.rootElement.contains('.ant-checkbox-wrapper-checked', text);
  }
}
