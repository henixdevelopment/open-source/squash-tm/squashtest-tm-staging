import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { AbstractFormFieldElement, ElementSelectorFactory } from './abstract-form-field.element';

export class EditableDateFieldElement extends AbstractFormFieldElement {
  // Generates the date as displayed in French (dd/MM/yyyy)
  static dateToDisplayString(date: Date): string {
    const year = date.getFullYear();
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const day = date.getDate().toString().padStart(2, '0');
    return `${day}/${month}/${year}`;
  }

  constructor(
    selectorOrFieldId: ElementSelectorFactory | string,
    private readonly url?: string,
    public readonly isDateTime?: boolean,
  ) {
    super(selectorOrFieldId);
  }

  setToTodayAndConfirm() {
    this.find('nz-date-picker').click({ force: true });

    if (this.url != null) {
      const mock = new HttpMockBuilder(this.url).post().build();
      this.isDateTime ? this.clickNowButton() : this.clickTodayButton();
      mock.wait();
    } else {
      this.isDateTime ? this.clickNowButton() : this.clickTodayButton();
    }
  }

  // Enable the edit mode
  enableEditMode() {
    this.find('span').click({ force: true });
  }

  checkContent(value: string, valueShouldBePresent: boolean) {
    if (valueShouldBePresent) {
      this.find('span').should('contain.text', value);
    } else {
      this.find('span').should('not.contain.text', value);
    }
  }

  cancel() {
    this.enableEditMode();
    this.find('button').click();
  }
  setDate(value: string) {
    this.enableEditMode();
    this.find('input').clear();
    this.find('input').type(value + '{enter}');
  }

  private clickTodayButton(): void {
    cy.get('.ant-picker-today-btn').should('be.visible').click();
  }

  private clickNowButton(): void {
    cy.get('.ant-picker-now-btn').should('be.visible').click();
    cy.get('.ant-picker-ok').click();
  }

  assertIsEditable() {
    this.find('span').should('have.class', 'editable');
  }

  assertIsNotEditable() {
    this.find('span').should('not.have.class', 'editable');
  }
}
