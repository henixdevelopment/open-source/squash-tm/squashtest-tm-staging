import { TextFieldElement } from './TextFieldElement';

export class ActionAutocompleteFieldElement extends TextFieldElement {
  constructor(protected readonly fieldName: string) {
    super(fieldName);
  }

  get selector(): string {
    return `
    sqtm-core-action-autocomplete-field
    [data-test-field-name="${this.fieldName}"]`;
  }

  getAutocompleteMenu() {
    return cy.get(`div.autocomplete-menu`);
  }

  checkAutocompletionMenuIsVisible() {
    this.getAutocompleteMenu().should('exist');
  }

  getOptions() {
    return this.getAutocompleteMenu().find('sqtm-core-action-autocomplete-field-option');
  }

  checkOptions(options: string[]) {
    this.getOptions().then((autocompleteOptions) => {
      cy.wrap(autocompleteOptions.length).should('equal', options.length);
      autocompleteOptions.each((index, element) => {
        cy.wrap(element).should('contain.text', options[index]);
      });
    });
  }

  chooseAutocompleteOptionWithClick(option: string) {
    this.getOptions().contains(option).click();
  }

  chooseAutocompleteOptionWithKeyboard(index: number) {
    for (let i = 0; i < index + 1; i++) {
      cy.get(this.selector).find('input').type('{downarrow}');
    }
    cy.get(this.selector).find('input').type('{downarrow}');
    cy.get(this.selector).find('input').type('{uparrow}');
    cy.get(this.selector).find('input').type('{enter}');
  }
}
