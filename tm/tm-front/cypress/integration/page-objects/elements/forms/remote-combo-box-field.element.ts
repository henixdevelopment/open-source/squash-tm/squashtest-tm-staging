import { selectByDataTestElementId } from '../../../utils/basic-selectors';
import { AbstractFormFieldElement } from './abstract-form-field.element';

export class RemoteComboBoxFieldElement extends AbstractFormFieldElement {
  constructor(protected readonly fieldId: string) {
    super(fieldId);
  }

  assertContainsText(expectedText: string) {
    this.find(selectByDataTestElementId('combo-box-input')).should('have.value', expectedText);
  }
}
