import Chainable = Cypress.Chainable;
import { ElementSelectorFactory } from './abstract-form-field.element';

export class RichTextFieldElement {
  constructor(private readonly selectorOrFieldName: ElementSelectorFactory | string) {}

  get rootChainable(): Chainable {
    if (typeof this.selectorOrFieldName === 'function') {
      return this.selectorOrFieldName();
    } else {
      return cy.get(`
        sqtm-core-rich-text-field
        [data-test-field-name="${this.selectorOrFieldName}"]
        `);
    }
  }

  assertIsReady(): void {
    this.extractCKEditorId().then((id) => {
      cy.window()
        .its('CKEDITOR')
        .then((CKEDITOR) => cy.wrap(CKEDITOR.instances[id]))
        .its('status')
        .should('equal', 'ready')
        .then(() => cy.log('CKEditor is ready'));
    });
  }

  fill(value: string) {
    this.fillWithString(value);
  }

  fillWithString(value: string) {
    this.fillWithHtml(`<p>${value}</p>`);
  }

  private fillWithHtml(htmlValue: string): void {
    this.extractCKEditorId().then((id) => this.doSetHtmlText(id, htmlValue));
  }

  private doSetHtmlText(ckEditorId: string, htmlValue: string): void {
    cy.window()
      .its('CKEDITOR')
      .then((CKEDITOR) => {
        cy.log('Setting CKEditor content');
        CKEDITOR.instances[ckEditorId].setData(htmlValue);
      });
  }

  private extractCKEditorId(): Chainable<string> {
    return this.rootChainable
      .find('div.cke')
      .should('exist')
      .then((element) => {
        const id = element.attr('id').split('_')[1];
        return cy.wrap(id);
      });
  }

  clear(): void {
    this.assertIsReady();
    this.rootChainable.find('iframe.cke_wysiwyg_frame').then(($iframe) => {
      const body = $iframe.contents().find('body');

      cy.wrap(body[0]).clear().should('have.text', '');
    });
  }

  checkContent(expectedContent: string) {
    this.assertIsReady();
    this.rootChainable.find('iframe.cke_wysiwyg_frame').then(($iframe) => {
      const body = $iframe.contents().find('body');

      cy.wrap(body[0]).should('have.text', expectedContent);
    });
  }

  checkHtmlTextContent(expectedContent: string) {
    this.assertIsReady();
    this.rootChainable.find('iframe.cke_wysiwyg_frame').then(($iframe) => {
      const body = $iframe.contents().find('body');

      cy.wrap(body[0]).should('have.html', expectedContent);
    });
  }
}
