import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { AbstractFormFieldElement, ElementSelectorFactory } from './abstract-form-field.element';

export class EditableTextAreaFieldElement extends AbstractFormFieldElement {
  constructor(
    selectorOrFieldId: ElementSelectorFactory | string,
    private readonly url?: string,
  ) {
    super(selectorOrFieldId);
  }

  checkDisplayContent(value: string): void {
    this.find('.field-value').should('contain.html', value);
  }

  checkEditContent(value: string): void {
    this.find('textarea').should('have.value', value);
  }

  setAndCancel(value: string, oldValue?: string) {
    this.setValue(value);
    this.cancel(oldValue);
  }

  setAndConfirm(value: string) {
    this.setValue(value);
    this.confirm();
  }

  setValue(value: string) {
    this.find('div > div').eq(0).click();
    this.find('textarea').clear();
    this.typeValue(value);
  }

  typeValue(value: string) {
    this.find('textarea').type(value);
  }

  cancel(oldValue?: string) {
    this.find('button').last().click();
    this.checkEditMode(false);
    if (oldValue) {
      this.checkDisplayContent(oldValue);
    }
  }

  checkEditMode(shouldBeInEditMode: boolean) {
    const chainer = shouldBeInEditMode ? 'exist' : 'not.exist';
    this.find('button').should(chainer);
  }

  confirm(): void {
    if (this.url) {
      const mock = new HttpMockBuilder(this.url).post().build();
      this.find('button').first().click();
      mock.wait();
    } else {
      this.find('button').first().click();
    }
    this.checkEditMode(false);
  }

  blur() {
    this.find('textarea').blur();
  }
}
