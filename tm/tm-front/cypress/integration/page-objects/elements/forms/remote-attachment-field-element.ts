import { AbstractFormFieldElement, ElementSelectorFactory } from './abstract-form-field.element';

export class RemoteAttachmentFieldElement extends AbstractFormFieldElement {
  constructor(selectorFactory: ElementSelectorFactory) {
    super(selectorFactory);
  }

  assertExists(): void {
    super.assertExists();
    cy.get('sqtm-app-remote-attachment-field').should('exist');
  }
}
