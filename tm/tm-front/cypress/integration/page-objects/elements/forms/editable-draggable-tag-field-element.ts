import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { AbstractFormFieldElement, ElementSelectorFactory } from './abstract-form-field.element';

export class EditableDraggableTagFieldElement extends AbstractFormFieldElement {
  constructor(
    selectorOrFieldId: ElementSelectorFactory | string,
    private readonly url: string,
  ) {
    super(selectorOrFieldId);
  }

  addTag(newValue: string, response: any) {
    const tagsValuesMock = new HttpMockBuilder(this.url).post().responseBody(response).build();
    this.findByElementId('add-tag-placeholder').click();
    this.findByElementId('add-tag-field').type(newValue).type('{enter}');
    tagsValuesMock.wait();
    this.find('nz-tag').contains(newValue).should('exist');
  }

  deleteTagWithValue(value: string, response: any) {
    const tagsValuesMock = new HttpMockBuilder(this.url).post().responseBody(response).build();
    const tagToDelete = this.find('nz-tag').contains(value);
    tagToDelete.find('svg').click();
    tagToDelete.should('not.exist');
    tagsValuesMock.wait();
  }

  checkIfTagIsClosable(value: string, shouldBeClosable: boolean) {
    const chainer = shouldBeClosable ? 'exist' : 'not.exist';
    this.find('nz-tag').contains(value).find('i').should(chainer);
  }

  checkTags(...tags: string[]): void {
    tags.forEach((tagValue) => this.find('nz-tag').contains(tagValue).should('exist'));
  }
}
