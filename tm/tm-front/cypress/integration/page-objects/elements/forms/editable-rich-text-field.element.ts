import Chainable = Cypress.Chainable;
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { AbstractFormFieldElement, ElementSelectorFactory } from './abstract-form-field.element';
import {
  selectByDataTestButtonId,
  selectByDataTestElementId,
} from '../../../utils/basic-selectors';

export class EditableRichTextFieldElement extends AbstractFormFieldElement {
  constructor(
    selectorOrFieldId: ElementSelectorFactory | string,
    private readonly url?: string,
  ) {
    super(selectorOrFieldId);
  }

  // Enable the edit mode
  enableEditMode() {
    this.find('>div').click();
  }

  // Add some text to the editor (when visible). By default, the content is cleared before.
  setValue(value: string) {
    if (value === '') {
      return;
    }

    this.clear().then(() => this.fillWithString(value));
  }

  // Replace the content of the editor (when visible) with the default one.
  clear(): Chainable<any> {
    this.assertIsReady();
    return this.find('iframe.cke_wysiwyg_frame').then(($iframe) => {
      const body = $iframe.contents().find('body');

      cy.wrap(body[0]).clear().should('have.text', '');
    });
  }

  fillWithString(value: string) {
    this.fillWithHtml(`<p>${value}</p>`);
  }

  fillWithHtml(htmlValue: string) {
    this.assertIsReady();
    this.extractCKEditorId().then((id) => {
      cy.window()
        .its('CKEDITOR')
        .then((CKEDITOR) => {
          CKEDITOR.instances[id].setData(htmlValue);
        });
    });
  }

  assertIsReady() {
    this.extractCKEditorId()
      .then((id) => {
        cy.window()
          .its('CKEDITOR')
          .then((CKEDITOR) => {
            return cy.wrap(CKEDITOR.instances[id]);
          });
      })
      .its('status')
      .should('equal', 'ready');
  }

  private extractCKEditorId(): Chainable<any> {
    return this.find('div.cke')
      .should('exist')
      .then((element) => {
        const id = element.attr('id').split('_')[1];
        return cy.wrap(id);
      });
  }

  // Click the confirm button (when visible). The result check is done on the content of the non-editable
  confirm(expectedContent?: string): void {
    // Click on button
    if (this.url != null) {
      const mock = new HttpMockBuilder(this.url).post().build();
      this.clickButton('confirm');
      mock.wait();
    } else {
      this.clickButton('confirm');
    }

    // Optionally check displayed value
    if (expectedContent != null) {
      this.checkTextContent(expectedContent);
    }
  }

  confirmValue(response?: any) {
    if (this.url != null) {
      const mock = new HttpMockBuilder(this.url).post().responseBody(response).build();
      this.find('button[data-test-button-id="confirm"]').click();
      mock.wait();
    } else {
      this.find('button[data-test-button-id="confirm"]').click();
    }
  }

  confirmOnError(error?: any) {
    if (this.url != null) {
      const mock = new HttpMockBuilder(this.url).post().status(412).responseBody(error).build();
      this.find('button[data-test-button-id="confirm"]').click();
      mock.wait();
    } else {
      this.find('button[data-test-button-id="confirm"]').click();
    }
  }

  cancel(): void {
    this.find('button[data-test-button-id="cancel"]').click();
    this.find('button[data-test-button-id="cancel"]').should('not.exist');
  }

  checkTextContent(value: string) {
    this.doCheckContent(value, true, false);
  }

  checkHtmlContent(value: string) {
    this.doCheckContent(value, true, true);
  }

  private doCheckContent(value: string, checkIfPresent: boolean, asHtml: boolean) {
    const baseChainer = asHtml ? 'contain.html' : 'contain.text';
    if (checkIfPresent) {
      this.find('>div>div').should(baseChainer, value);
    } else {
      this.find('>div>div').should('not.' + baseChainer, value);
    }
  }

  setAndConfirmValue(newValue: string) {
    this.enableEditMode();
    this.setValue(newValue);
    if (this.url) {
      this.confirm(newValue);
    } else {
      this.confirmValue(newValue);
    }
  }

  clickButton(buttonId: string) {
    this.find(selectByDataTestButtonId(buttonId)).click();
  }

  assertIsEditable() {
    this.getDivWrapper().should('have.class', 'editable');
  }

  assertIsNotEditable() {
    this.getDivWrapper().should('not.have.class', 'editable');
  }

  assertIsInEditMode() {
    this.find('ckeditor').should('exist');
  }

  assertIsNotInEditMode() {
    this.find(selectByDataTestElementId('read-mode-container')).should('exist');
  }

  assertPlaceholderIsVisible() {
    this.find('.sqtm-placeholder').should('be.visible');
  }

  private getDivWrapper() {
    return this.find('>div');
  }
}
