import { selectByDataTestButtonId, selectByDataTestFieldId } from '../../../utils/basic-selectors';
import Chainable = Cypress.Chainable;

export class SecretFieldElement {
  selector(): string {
    return `sqtm-app-display-new-api-token-dialog`;
  }

  getSecretValue(): Chainable {
    return cy.get(this.selector()).find(selectByDataTestFieldId('secret-value'));
  }

  getSecretValueForCopy(): Chainable<string> {
    return cy
      .get(this.selector())
      .find(selectByDataTestFieldId('secret-value'))
      .then((elements) => {
        return (elements[0] as HTMLInputElement).value;
      });
  }

  getShowHideButton(): Chainable {
    return cy.get(this.selector()).find(selectByDataTestButtonId('show-hide'));
  }

  getCopyButton(): Chainable {
    return cy.get(this.selector()).find(selectByDataTestButtonId('copy'));
  }

  showSecretField() {
    this.getShowHideButton().click();
  }

  assertSecretIsNotVisible() {
    this.getSecretValue().should('have.value', '••••••');
  }

  assertSecretIsVisible(secretValue: string) {
    this.getSecretValue().should('have.value', secretValue);
  }

  copySecretValue() {
    this.getCopyButton().click();
  }
}
