export class RemoteSelectorFieldElement {
  constructor(protected readonly fieldId: string) {}

  get selector(): string {
    return `
    sqtm-app-remote-selector-field
    [data-test-field-name="${this.fieldId}"]`;
  }

  assertExists(): void {
    cy.get(this.selector).should('exist');
  }

  selectOption(optionLabel: string) {
    cy.get(this.selector).click();
    cy.contains(`nz-option-item`, optionLabel).click();
  }

  checkAllOptions(expectedOptions: string[]) {
    cy.get(this.selector).click();
    expectedOptions.forEach((option) => cy.contains(`nz-option-item`, option));
  }

  assertOptionSelected(expectedOption: string) {
    cy.get(this.selector).should('contain.text', expectedOption);
  }
}
