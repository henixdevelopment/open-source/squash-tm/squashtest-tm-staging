import Chainable = Cypress.Chainable;
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { AbstractFormFieldElement, ElementSelectorFactory } from './abstract-form-field.element';

export class EditableTagFieldElement extends AbstractFormFieldElement {
  constructor(
    selectorOrFieldId: ElementSelectorFactory | string,
    private readonly url?: string,
  ) {
    super(selectorOrFieldId);
  }

  // Show the dropdown, click on some options (selecting them by displayed value) and hide dropdown
  toggleTagFieldOptions(...optionNames: string[]) {
    const mock = this.url == null ? null : new HttpMockBuilder(this.url).post().build();

    // Show options
    this.find('input').click();

    // Get desired options and click on them
    for (const optionName of optionNames) {
      this.getOption(optionName).click();

      if (mock != null) {
        mock.wait();
      }
    }

    this.hideDropdown();
  }

  // Type a new tag in the input element and submit with 'enter' key
  typeNewTag(value: string) {
    this.getTextInput().type(`${value}{enter}`);
    this.hideDropdown();
  }

  // Click on the 'cross' buttons of each selected tags if any. This won't work if
  // the tags have already been cleared or if the field is required.
  clear(): Chainable<any> {
    return this.getTextInput()
      .clear()
      .then(() => this.getTagFieldRemoveButtons())
      .then((elements) => {
        elements.each((index, element) => {
          element.click();
        });
      });
  }

  // Checks the tags currently in the field
  checkTags(...values: string[]) {
    for (const value of values) {
      this.find(`nz-select-item[title="${value}"]`).should('exist');
    }

    this.find(`nz-select-item`).should('have.length', values.length);
  }

  checkTagsWhenFieldIsNotEditable(...values: string[]) {
    for (const value of values) {
      this.find(`nz-tag`).contains(value).should('exist');
    }

    this.find(`nz-tag`).should('have.length', values.length);
  }

  assertIsEditable() {
    this.find('nz-select-search').should('exist');
  }

  assertIsNotEditable() {
    this.find('nz-select-search').should('not.exist');
    this.find(`nz-select-item`).should('not.exist');
  }

  private getOption(optionName: string): Chainable<any> {
    return cy.contains(`nz-option-item`, optionName);
  }

  private getTagFieldRemoveButtons(): Chainable<any> {
    return this.find('.ant-select-selection__choice__remove');
  }

  private getTextInput(): Chainable<any> {
    return this.find('nz-select-search input');
  }

  private hideDropdown() {
    cy.clickVoid();
  }
}
