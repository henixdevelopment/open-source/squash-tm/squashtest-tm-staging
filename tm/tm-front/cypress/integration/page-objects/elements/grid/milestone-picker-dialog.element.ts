import { GridDialogPicker } from './grid-dialog-picker';

export class MilestonePickerDialogElement extends GridDialogPicker {
  constructor() {
    super('single-milestones-picker-grid');
  }

  selectMilestone(id: number) {
    this.grid.getCell(id, 'single-select-row-column').radioNgZorroRenderer().check();
  }
}
