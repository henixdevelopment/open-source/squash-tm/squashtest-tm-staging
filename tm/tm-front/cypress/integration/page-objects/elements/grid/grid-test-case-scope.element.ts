import { TreeElement } from './grid.element';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { selectByDataTestElementId } from '../../../utils/basic-selectors';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class GridTestCaseScopeElement {
  tree: TreeElement;

  constructor(
    nodes: GridResponse = { dataRows: [] },
    private researchGridUrl: string,
  ) {
    const mock = new HttpMockBuilder<GridResponse>('test-case-tree')
      .post()
      .responseBody(nodes)
      .build();
    this.tree = new TreeElement('test-case-tree-picker', mock, 'test-case-tree');
  }

  confirm(rows: GridResponse = { dataRows: [], count: 0 }) {
    const mock = new HttpMockBuilder<GridResponse>(this.researchGridUrl)
      .post()
      .responseBody(rows)
      .build();
    cy.get(selectByDataTestElementId('confirm-scope')).click();
    mock.wait();
    cy.clickVoid();
  }
}
