import { GridElement } from './grid.element';
import {
  selectByDataTestButtonId,
  selectByDataTestElementId,
} from '../../../utils/basic-selectors';

export class GridFooterElement {
  constructor(public readonly parentGrid: GridElement) {}

  checkPaginationDisplay(expectedPaginationDisplay: string) {
    this.parentGrid
      .selectRoot()
      .find(selectByDataTestElementId('pagination-display'))
      .should('contain.text', expectedPaginationDisplay);
  }

  checkPagination(expectedPagination: string) {
    this.parentGrid
      .selectRoot()
      .find(selectByDataTestElementId('pagination'))
      .should('contain.text', expectedPagination);
  }

  checkPaginationDoesntExist() {
    this.parentGrid
      .selectRoot()
      .find(selectByDataTestElementId('pagination-display'))
      .should('not.exist');
  }

  goToFirstPage() {
    this.parentGrid.selectRoot().find(selectByDataTestButtonId('first-page')).click();
  }

  goToNextPage() {
    this.parentGrid.selectRoot().find(selectByDataTestButtonId('next-page')).click();
  }

  assertPaginationButtonsExist() {
    const buttons = ['first-page', 'previous-page', 'next-page', 'last-page'];
    buttons.forEach((button) => {
      this.parentGrid.selectRoot().find(selectByDataTestButtonId(button)).should('exist');
    });
  }
}
