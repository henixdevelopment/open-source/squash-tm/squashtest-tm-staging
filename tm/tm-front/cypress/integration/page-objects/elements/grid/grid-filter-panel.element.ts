import { GroupedMultiListElement } from '../filters/grouped-multi-list.element';
import { TextFilterWidgetElement } from '../filters/text-filter-widget.element';
import { HttpMock, HttpMockBuilder } from '../../../utils/mocks/request-mock';
import {
  selectByDataTestElementId,
  selectByDataTestDialogButtonId,
} from '../../../utils/basic-selectors';
import { NumericFilterWidgetElement } from '../filters/numeric-filter-widget.element';
import { DateFilterWidgetElement } from '../filters/date-filter-widget.element';
import { GridTestCaseScopeElement } from './grid-test-case-scope.element';
import { NumericSetFilterWidgetElement } from '../filters/numeric-set-filter-widget.element';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { BasicElement } from '../abstract-element';

export class GridFilterPanelElement extends BasicElement {
  constructor(
    private gridId: string,
    private gridUrl?: string,
  ) {
    super('sqtm-core-filter-manager');
  }

  assertPerimeterIsActive() {
    this.findByElementId(`grid-scope-field`).should('exist');
    this.findByElementId(`grid-scope-field`).should('contain.text', 'Périmètre');
  }

  assertCriteriaIsActive(label: string) {
    this.findByElementId(`grid-filter-field`).contains(label).should('exist');
  }

  assertCriteriaIsInactive(label: string) {
    this.findByElementId(`grid-filter-field`).contains(label).should('not.exist');
  }

  openProjectScopeSelector(): GroupedMultiListElement {
    this.findByElementId(`grid-scope-field`).click();
    const groupedMultiListElement = new GroupedMultiListElement(this.gridUrl);
    groupedMultiListElement.assertExists();
    return groupedMultiListElement;
  }

  openCustomScopeSelector(nodes: GridResponse = { dataRows: [] }): GridTestCaseScopeElement {
    const testCaseScope = new GridTestCaseScopeElement(nodes, this.gridUrl);
    this.findByElementId(`grid-scope-field`).click();
    testCaseScope.tree.waitInitialDataFetch();
    testCaseScope.tree.assertExists();
    return testCaseScope;
  }

  changeToCustomScope(nodes: GridResponse = { dataRows: [] }): GridTestCaseScopeElement {
    const testCaseScope = new GridTestCaseScopeElement(nodes, this.gridUrl);
    cy.get(selectByDataTestElementId(`custom-scope`)).click();
    testCaseScope.tree.waitInitialDataFetch();
    testCaseScope.tree.assertExists();
    return testCaseScope;
  }

  assertPerimeterHasValue(expectedValue: string) {
    this.findByElementId(`grid-scope-value`).should('contain.text', expectedValue);
  }

  assertCriteriaHasValue(id: string, expectedValue: string) {
    this.findByElementId(`grid-filter-field`)
      .contains(id)
      .parent()
      .find(selectByDataTestElementId('grid-filter-field-value'))
      .should('contain.text', expectedValue);
  }

  assertCriteriaHasOperationAndValue(id: string, expectedOperation: string, expectedValue: string) {
    this.findByElementId(`grid-filter-field`)
      .contains(id)
      .parent()
      .find(selectByDataTestElementId('grid-filter-field-value'))
      .should('contain.text', expectedOperation)
      .should('contain.text', expectedValue);
  }

  openExistingCriteria(label: string) {
    this.findByElementId(`grid-filter-field`).contains(label).click();
  }

  inactivateCriteria(criteriaLabel: string, response: GridResponse = { dataRows: [] }) {
    let mock: HttpMock<GridResponse>;
    if (this.gridUrl) {
      mock = new HttpMockBuilder<GridResponse>(this.gridUrl).post().responseBody(response).build();
    }
    this.findByElementId(`grid-filter-field`)
      .then((elements) => {
        return elements.filter((index, element) => element.textContent.includes(criteriaLabel));
      })
      .find('[data-test-element-id="inactivate"]')
      .click();
    if (mock) {
      mock.wait();
    }
  }

  assertAddCriteriaLinkIsPresent() {
    this.findByElementId(`grid-filter-manager-add-criteria`).should('exist');
    this.findByElementId(`grid-filter-manager-add-criteria`).should(
      'contain.text',
      'Ajouter un critère',
    );
  }

  openCriteriaList(): GroupedMultiListElement {
    this.findByElementId(`grid-filter-manager-add-criteria`).click();
    const groupedMultiListElement = new GroupedMultiListElement();
    groupedMultiListElement.assertExists();
    return groupedMultiListElement;
  }

  selectTextCriteria(filterId: string): TextFilterWidgetElement {
    this.findByElementId(`grid-filter-manager-add-criteria`).click();
    const groupedMultiListElement = new GroupedMultiListElement();
    groupedMultiListElement.assertExists();
    groupedMultiListElement.toggleOneItem(filterId);
    groupedMultiListElement.assertNotExist();
    const textFilterWidgetElement = new TextFilterWidgetElement();
    textFilterWidgetElement.assertExists();
    return textFilterWidgetElement;
  }

  selectNumericCriteria(filterId: string, withOutOperations?: boolean): NumericFilterWidgetElement {
    this.findByElementId(`grid-filter-manager-add-criteria`).click();
    const groupedMultiListElement = new GroupedMultiListElement();
    groupedMultiListElement.assertExists();
    groupedMultiListElement.toggleOneItem(filterId);
    groupedMultiListElement.assertNotExist();
    const numericFilterWidgetElement = new NumericFilterWidgetElement(this.gridUrl);
    if (withOutOperations) {
      numericFilterWidgetElement.assertExistWithOutOperationSelector();
    } else {
      numericFilterWidgetElement.assertExists();
    }
    return numericFilterWidgetElement;
  }

  selectDateCriteria(filterId: string): DateFilterWidgetElement {
    this.findByElementId(`grid-filter-manager-add-criteria`).click();
    const groupedMultiListElement = new GroupedMultiListElement();
    groupedMultiListElement.assertExists();
    groupedMultiListElement.toggleOneItem(filterId);
    groupedMultiListElement.assertNotExist();
    const dateFilterWidgetElement = new DateFilterWidgetElement(this.gridUrl);
    dateFilterWidgetElement.assertExists();
    return dateFilterWidgetElement;
  }

  selectMultiListCriteria(filterId: string): GroupedMultiListElement {
    this.findByElementId(`grid-filter-manager-add-criteria`).click();
    const groupedMultiListElement = new GroupedMultiListElement();
    groupedMultiListElement.assertExists();
    groupedMultiListElement.toggleOneItem(filterId);
    return new GroupedMultiListElement(this.gridUrl);
  }

  selectNumericSetCriteria(filterId: string): NumericSetFilterWidgetElement {
    this.findByElementId(`grid-filter-manager-add-criteria`).click();
    const groupedMultiListElement = new GroupedMultiListElement();
    groupedMultiListElement.assertExists();
    groupedMultiListElement.toggleOneItem(filterId);
    groupedMultiListElement.assertNotExist();
    return new NumericSetFilterWidgetElement(this.gridUrl);
  }

  fillTextCriteria(
    filterLabel: string,
    input: string,
    response: GridResponse = { dataRows: [] },
  ): void {
    this.findByElementId(`grid-filter-manager-add-criteria`).click();
    const groupedMultiListElement = new GroupedMultiListElement();
    groupedMultiListElement.assertExists();
    groupedMultiListElement.toggleOneItem(filterLabel);
    const textFilterWidgetElement = new TextFilterWidgetElement();
    let mock: HttpMock<GridResponse>;
    if (this.gridUrl) {
      mock = new HttpMockBuilder<GridResponse>(this.gridUrl).post().responseBody(response).build();
    }
    textFilterWidgetElement.typeAndConfirm(input);
    if (mock) {
      mock.wait();
    }
    textFilterWidgetElement.assertNotExist();
  }

  confirmNewConf() {
    const getColumnDataProvider = new HttpMockBuilder('grid/update-column-config').post().build();
    cy.get(selectByDataTestDialogButtonId('confirm')).should('exist').click();
    getColumnDataProvider.wait();
  }

  resetConf() {
    const getColumnDataProvider = new HttpMockBuilder('grid/requirement-search/reset-column-config')
      .delete()
      .build();
    cy.get(selectByDataTestElementId('reset-configuration')).should('exist').click();
    getColumnDataProvider.wait();
  }
}
