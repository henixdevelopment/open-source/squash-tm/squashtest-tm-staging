import { GridDialogPicker } from './grid-dialog-picker';

export class ProjectPickerDialogElement extends GridDialogPicker {
  constructor() {
    super('project-picker');
  }

  assertProjectsAreSelected(ids: number[]) {
    this.assertRowsAreSelected(ids);
  }

  toggleProject(id: number) {
    this.toggleRow(id);
  }
}
