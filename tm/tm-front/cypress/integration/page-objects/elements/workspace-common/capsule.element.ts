import Chainable = Cypress.Chainable;

export class CapsuleElement {
  constructor(public readonly capsuleName) {}

  get selector(): Chainable<any> {
    return cy.get(`sqtm-core-capsule-information[data-test-capsule-id=${this.capsuleName}]`);
  }

  click(): void {
    this.selector.should('exist').click();
  }

  assertIsClickable(): void {
    this.selector.find('div').should('have.class', '__hover_pointer');
  }
  assertIsNotClickable(): void {
    this.selector.find('div').should('not.have.class', '__hover_pointer');
  }

  selectOption(optionText: string) {
    this.click();
    this.clickOnDropdownOption(optionText);
    this.assertContainsText(optionText);
  }

  private clickOnDropdownOption(optionText: string) {
    cy.get('sqtm-core-list-panel').contains('span', optionText).click();
  }

  assertContainsText(expectedText: string) {
    this.selector.should('contain.text', expectedText);
  }

  assertExists() {
    this.selector.should('exist');
  }

  assertNotExist() {
    this.selector.should('not.exist');
  }

  assertIconColor(expectedColor: string) {
    this.selector.find('i').should('have.css', 'color', expectedColor);
  }
}
