export class KeyboardShortcuts {
  private readonly CY_CTRL = '{ctrl}';

  performCopy() {
    this.performCtrlHoldShortcut('c');
  }

  performPaste() {
    this.performCtrlHoldShortcut('v');
  }

  performCtrlHoldShortcut(stroke: string) {
    this.performShortcut({ stroke, ctrlKey: true });
  }

  performShortcut(definition: KeyboardShortcutDefinition) {
    if (definition.ctrlKey) {
      this.typeAndHold(this.CY_CTRL);
    }

    this.typeAndRelease(definition.stroke);

    if (definition.ctrlKey) {
      this.typeAndRelease(this.CY_CTRL);
    }
  }

  pressDelete() {
    this.getBody().type('{del}');
  }

  private typeAndHold(stroke: string) {
    this.getBody().type(stroke, { release: false });
  }

  private typeAndRelease(stroke: string) {
    this.getBody().type(stroke);
  }

  private getBody() {
    return cy.get('body');
  }
}

export interface KeyboardShortcutDefinition {
  stroke: string;
  ctrlKey?: boolean;
}
