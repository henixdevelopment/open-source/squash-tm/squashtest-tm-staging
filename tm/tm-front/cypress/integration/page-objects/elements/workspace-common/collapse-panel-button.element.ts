import Chainable = Cypress.Chainable;
import { selectByDataTestIconId } from '../../../utils/basic-selectors';

export class CollapsePanelButtonElement {
  constructor(protected testIconId: string) {}

  get selector(): Chainable<any> {
    return cy.get(selectByDataTestIconId(this.testIconId));
  }

  click() {
    this.selector.click();
  }
}
