import { ToolbarButtonElement, ToolbarElement, ToolbarMenuButtonElement } from './toolbar.element';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { DeleteTreeNodesDialogElement } from '../../pages/test-case-workspace/dialogs/delete-tree-nodes-dialog.element';
import { SingleMilestonePickerDialogElement } from '../dialog/single-milestone-picker-dialog.element';
import { TestCaseMilestoneViewPage } from '../../pages/test-case-workspace/test-case/test-case-milestone-view.page';
import { RequirementMilestoneViewPage } from '../../pages/requirement-workspace/requirement/requirement-milestone-view.page';
import { DeleteCampaignTreeNodesDialog } from '../../pages/campaign-workspace/dialogs/delete-campaign-tree-nodes-dialog.element';
import { CampaignMilestoneViewPage } from '../../pages/campaign-workspace/campaign/campaign-milestone-view.page';
import { KeyboardShortcuts } from './keyboard-shortcuts';
import { Identifier } from '../../../../../projects/sqtm-core/src/lib/model/entity.model';
import { TestCaseStatistics } from '../../../../../projects/sqtm-core/src/lib/model/test-case/test-case-statistics.model';
import { StatisticsBundle } from '../../../../../projects/sqtm-core/src/lib/model/campaign/campaign-model';
import { MilestoneRequirementDashboard } from '../../../../../projects/sqtm-core/src/lib/model/requirement/milestone-requirement-dashboard.model';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { ConfirmInterProjectPaste } from '../dialog/confirm-inter-project-paste';

export class TreeToolbarElement extends ToolbarElement {
  constructor(protected readonly toolbarId: string) {
    super(toolbarId);
  }

  protected copyButton(): ToolbarButtonElement {
    const createButtonElement = this.button('copy-button');
    createButtonElement.assertExists();
    return createButtonElement;
  }

  protected pasteButton(): ToolbarButtonElement {
    const createButtonElement = this.button('paste-button');
    createButtonElement.assertExists();
    return createButtonElement;
  }

  protected deleteButton(): ToolbarButtonElement {
    const buttonElement = this.button('delete-button');
    buttonElement.assertExists();
    return buttonElement;
  }

  getButton(buttonId: string): ToolbarButtonElement {
    const buttonElement = this.button(buttonId);
    buttonElement.assertExists();
    return buttonElement;
  }

  assertCopyButtonIsActive() {
    const copyButtonElement = this.copyButton();
    copyButtonElement.assertIsActive();
  }

  assertCopyButtonIsDisabled() {
    const copyButtonElement = this.copyButton();
    copyButtonElement.assertIsDisabled();
  }

  copy(useShortcut = false) {
    const mock = new HttpMockBuilder(`clipboard/copy`).post().responseBody({}).build();
    if (useShortcut) {
      new KeyboardShortcuts().performCopy();
    } else {
      const copyButtonElement = this.copyButton();
      copyButtonElement.click();
    }
    mock.wait();
  }

  assertPasteButtonIsActive() {
    const pasteButton = this.pasteButton();
    pasteButton.assertIsActive();
  }

  assertPasteButtonIsDisabled() {
    const pasteButton = this.pasteButton();
    pasteButton.assertIsDisabled();
  }

  assertCreateButtonIsDisabled() {
    const createButton = this.createButton();
    createButton.assertIsDisabled();
  }

  paste(
    refreshedNodes: GridResponse = { dataRows: [] },
    treeUrl: string,
    destinationId: Identifier,
    useShortcut = false,
  ) {
    const refreshUrl = `${treeUrl}/refresh`;
    const pasteUrl = `${treeUrl}/${destinationId}/content/paste`;
    const pasteMock = new HttpMockBuilder(pasteUrl).post().build();
    const refreshDestinationMock = new HttpMockBuilder(refreshUrl)
      .post()
      .responseBody(refreshedNodes)
      .build();
    this.performPasteCommand(useShortcut);
    pasteMock.wait();
    refreshDestinationMock.wait();
  }

  pasteInOtherProject(useShortcut = false) {
    const confirmDialogPasteInterProject = new ConfirmInterProjectPaste(
      'confirm-inter-project-paste',
    );
    this.performPasteCommand(useShortcut);
    confirmDialogPasteInterProject.confirm();
  }

  performPasteCommand(useShortcut: boolean) {
    if (useShortcut) {
      new KeyboardShortcuts().performPaste();
    } else {
      const pasteButton = this.pasteButton();
      pasteButton.click();
    }
  }

  assertDeleteButtonIsActive() {
    const deleteButton = this.deleteButton();
    deleteButton.assertIsActive();
  }

  assertDeleteButtonIsDisabled() {
    const deleteButton = this.deleteButton();
    deleteButton.assertIsDisabled();
  }

  assertDeleteButtonIsHidden(): void {
    this.button('delete-button').assertNotExist();
  }

  createButton(): ToolbarMenuButtonElement {
    const createButtonElement = this.buttonMenu('create-button', 'create-menu');
    createButtonElement.assertExists();
    return createButtonElement;
  }

  importExportButton(): ToolbarMenuButtonElement {
    const importExportButtonElement = this.buttonMenu('import-export', 'import-export-menu');
    importExportButtonElement.assertExists();
    return importExportButtonElement;
  }

  initDeletion(
    treeUrl: string,
    deleteNodes: any[],
    warnings: string[] = [],
  ): DeleteTreeNodesDialogElement {
    const deleteButton = this.deleteButton();
    const pathParam = deleteNodes.map((id) => id.toString()).join(',');
    const simulationUrl = `${treeUrl}/deletion-simulation/${pathParam}`;
    const simulationMock = new HttpMockBuilder(simulationUrl)
      .responseBody({ messageCollection: warnings })
      .build();
    deleteButton.click();
    simulationMock.wait();
    return new DeleteTreeNodesDialogElement(treeUrl);
  }

  // deleted node is identifier[]
  initCampaignWorkspaceDeletion(
    deleteNodes: any[],
    warnings: string[] = [],
  ): DeleteCampaignTreeNodesDialog {
    const deleteButton = this.deleteButton();
    const pathParam = deleteNodes.map((id) => id.toString()).join(',');
    const simulationUrl = `campaign-tree/deletion-simulation/${pathParam}`;
    const simulationMock = new HttpMockBuilder(simulationUrl)
      .responseBody({ messageCollection: warnings })
      .build();
    deleteButton.click();
    simulationMock.wait();
    return new DeleteCampaignTreeNodesDialog('campaign-tree');
  }

  sortTreePositional() {
    const sortButton = this.buttonMenu('settings-button', 'sort-menu');
    sortButton.showMenu().item('sort-positional').click();
    sortButton.hideMenu();
  }

  sortTreeAlphabetical() {
    const sortButton = this.buttonMenu('settings-button', 'sort-menu');
    sortButton.showMenu().item('sort-alphabetical').click();
    sortButton.hideMenu();
  }

  collapseAllNodesInTree() {
    const sortButton = this.buttonMenu('settings-button', 'collapse');
    sortButton.showMenu().item('collapse-tree').click();
    sortButton.hideMenu();
  }

  checkCollapseButtonDisabled() {
    const sortButton = this.buttonMenu('settings-button', 'collapse');
    sortButton.showMenu().item('collapse-tree').assertButtonIsDisabled();
  }

  checkCollapseButtonActive() {
    const sortButton = this.buttonMenu('settings-button', 'collapse');
    sortButton.showMenu().item('collapse-tree').assertButtonIsActive();
    sortButton.hideMenu();
  }

  assertMilestoneButtonIsVisible() {
    this.buttonMenu('milestone-button', 'milestone-menu').assertExists();
  }

  showMilestoneSelector(): SingleMilestonePickerDialogElement {
    const milestoneButton = this.buttonMenu('milestone-button', 'milestone-menu');
    milestoneButton.showMenu().item('change-active-milestone').click();
    return new SingleMilestonePickerDialogElement();
  }

  showMilestoneDashboard(statistics: {
    statistics: TestCaseStatistics;
  }): TestCaseMilestoneViewPage {
    const milestoneButton = this.buttonMenu('milestone-button', 'milestone-menu');
    const mock = new HttpMockBuilder('test-case-milestone-dashboard')
      .responseBody(statistics)
      .build();
    milestoneButton.showMenu().item('show-active-milestone-dashboard').click();
    mock.wait();
    const testCaseMilestoneViewPage = new TestCaseMilestoneViewPage();
    testCaseMilestoneViewPage.assertExists();
    return testCaseMilestoneViewPage;
  }

  showCampaignMilestoneDashboard(
    statistics: { statistics: StatisticsBundle },
    lastExecutionScope: boolean = true,
  ): CampaignMilestoneViewPage {
    const milestoneButton = this.buttonMenu('milestone-button', 'milestone-menu');
    const mock = new HttpMockBuilder(
      `campaign-milestone-dashboard?lastExecutionScope=${lastExecutionScope}`,
    )
      .responseBody(statistics)
      .build();
    milestoneButton.showMenu().item('show-active-milestone-dashboard').click();
    mock.wait();
    const campaignMilestoneViewPage = new CampaignMilestoneViewPage();
    campaignMilestoneViewPage.assertExists();
    return campaignMilestoneViewPage;
  }

  showRequirementMilestoneDashboard(
    statistics: MilestoneRequirementDashboard,
  ): RequirementMilestoneViewPage {
    const milestoneButton = this.buttonMenu('milestone-button', 'milestone-menu');
    const mock = new HttpMockBuilder('requirement-milestone-dashboard')
      .responseBody(statistics)
      .build();
    milestoneButton.showMenu().item('show-active-milestone-dashboard').click();
    mock.wait();
    const requirementMilestoneViewPage = new RequirementMilestoneViewPage();
    requirementMilestoneViewPage.assertExists();
    return requirementMilestoneViewPage;
  }
}
