import {
  selectByDataTestElementId,
  selectByDataTestNavbarFieldId,
} from '../../../utils/basic-selectors';
import { AdministrationWorkspacePage } from '../../pages/administration-workspace/administration-workspace.page';
import { GridElement, UserGridElement } from '../grid/grid.element';
import { AdminWorkspaceProjectsPage } from '../../pages/administration-workspace/admin-workspace-projects.page';
import { AdminWorkspaceUsersPage } from '../../pages/administration-workspace/admin-workspace-users.page';
import { AdminWorkspaceCustomFieldsPage } from '../../pages/administration-workspace/admin-workspace-custom-fields.page';
import { AdminWorkspaceMilestonesPage } from '../../pages/administration-workspace/admin-workspace-milestones.page';
import { AdminWorkspaceBugtrackersPage } from '../../pages/administration-workspace/admin-workspace-bugtrackers.page';

export class NavBarAdminElement {
  constructor() {}

  private readonly rootSelector = 'sqtm-core-nav-bar-admin';

  public toggle() {
    cy.get(this.rootSelector)
      .find(selectByDataTestElementId('sqtm-main-nav-bar-toggle-button'))
      .click();
  }

  public static navigateToAdministration<T extends AdministrationWorkspacePage>(
    workspaceName: AdminWorkspaceItem,
  ): T {
    // Determine which page we're about to navigate to
    let page: any;

    switch (workspaceName) {
      case AdminWorkspaceItem.PROJECTS: {
        const grid = GridElement.createGridElement('projects', 'generic-projects');
        page = new AdminWorkspaceProjectsPage(grid);
        break;
      }
      case AdminWorkspaceItem.USERS: {
        const grid = UserGridElement.createUserGridElement('users', 'users');
        page = new AdminWorkspaceUsersPage(grid);
        break;
      }
      case AdminWorkspaceItem.ENTITIES: {
        const grid = GridElement.createGridElement('customFields', 'custom-fields');
        page = new AdminWorkspaceCustomFieldsPage(grid);
        break;
      }
      case AdminWorkspaceItem.MILESTONES: {
        const grid = GridElement.createGridElement('milestones', 'milestones');
        page = new AdminWorkspaceMilestonesPage(grid);
        break;
      }
      case AdminWorkspaceItem.SERVERS: {
        const grid = GridElement.createGridElement('bugtrackers', 'bugtrackers');
        page = new AdminWorkspaceBugtrackersPage(grid);
        break;
      }

      default:
        throw new Error(`Unknown page type for menu item : ${workspaceName}`);
    }
    this.selectMenu(workspaceName);
    page.assertExists();

    return page;
  }
  public static navigateToAdministrationSystem() {
    this.selectMenu(AdminWorkspaceItem.SYSTEM);
  }

  private static selectMenu(workspaceName: AdminWorkspaceItem) {
    cy.get(`[data-test-navbar-field-id=nav-bar-menu-${workspaceName}]`).click();
  }
  assertExists() {
    cy.get(this.rootSelector).should('have.length', 1);
  }

  static close() {
    cy.get(`[data-test-navbar-field-id=exit]`).click();
  }
  assertMenuItemsExists(workspaceName: AdminWorkspaceItem) {
    cy.get(selectByDataTestNavbarFieldId(`nav-bar-menu-${workspaceName}`)).should('have.length', 1);
  }
  assertMenuItemsNotExists(workspaceName: AdminWorkspaceItem) {
    cy.get(selectByDataTestNavbarFieldId(`nav-bar-menu-${workspaceName}`)).should('not.exist');
  }
}

export enum AdminWorkspaceItem {
  USERS = 'users',
  PROJECTS = 'projects',
  MILESTONES = 'milestones',
  ENTITIES = 'entities-customization',
  SERVERS = 'servers',
  SYSTEM = 'system',
}
