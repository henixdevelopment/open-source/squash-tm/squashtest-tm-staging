import { ExecutionRunnerStepPage } from '../../pages/execution/execution-runner-step-page';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { ExecutionRunnerProloguePage } from '../../pages/execution/execution-runner-prologue-page';
import { ExecutionStatusKeys } from '../../../../../projects/sqtm-core/src/lib/model/level-enums/level-enum';
import { TestPlanResumeModel } from '../../../../../projects/sqtm-core/src/lib/model/execution/test-plan-resume.model';

export class ChangeExecutionStatusButton {
  constructor(private executionStatus: ExecutionStatusKeys) {}

  assertExists() {
    this.getButton().should('exist');
  }

  private getButton() {
    return cy.get(`
    sqtm-app-execution-runner-status-button
    [data-test-execution-status="${this.executionStatus}"]
    `);
  }

  changeStatus(executionStepId: number, executionId: number): ExecutionRunnerStepPage {
    const mock = new HttpMockBuilder(
      `execution-step/${executionStepId}/status?executionId=${executionId}`,
    )
      .post()
      .build();
    this.getButton().click();
    mock.wait();
    return new ExecutionRunnerStepPage();
  }

  changeStatusAndForward(
    executionStepId: number,
    executionId: number,
    iterationId = '*',
    testPlanId = '*',
    testPlanResume?: TestPlanResumeModel,
  ): ExecutionRunnerProloguePage {
    const changeStatusMock = new HttpMockBuilder(
      `execution-step/${executionStepId}/status?executionId=${executionId}`,
    )
      .post()
      .build();
    const resumeMock = new HttpMockBuilder(
      `iteration/${iterationId}/test-plan/${testPlanId}/next-execution`,
    )
      .post()
      .responseBody(testPlanResume)
      .build();
    this.getButton().click();
    changeStatusMock.wait();
    resumeMock.wait();
    return new ExecutionRunnerProloguePage();
  }
}
