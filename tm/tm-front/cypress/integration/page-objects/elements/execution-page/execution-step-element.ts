import { selectByDataTestElementId } from '../../../utils/basic-selectors';
import { EditableRichTextFieldElement } from '../forms/editable-rich-text-field.element';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { ExecutionStatusKeys } from '../../../../../projects/sqtm-core/src/lib/model/level-enums/level-enum';
import { IssueCompactListElement } from '../issues/issue-compact-list.element';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { fillCuf } from '../../../scenarios/scenario-parts/custom-field.part';
import { InputType } from '../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { ToolbarButtonElement } from '../workspace-common/toolbar.element';
import { MenuElement } from '../../../utils/menu.element';
import { RemoteIssueDialogElements } from '../dialog/remote-issue-dialog.elements';

export class ExecutionStepElement {
  rootSelector: string;
  private readonly editableCommentField: EditableRichTextFieldElement;
  private readonly issueCompactListElement: IssueCompactListElement;
  private readonly addIssueToolBarButton: ToolbarButtonElement;

  constructor(
    protected stepId: string,
    protected stepIndex: number,
  ) {
    if (this.stepId) {
      this.rootSelector = `sqtm-app-execution-step
    div[data-test-element-id="execution-step"][data-test-execution-step-id="${stepId}"]
    `;
    } else {
      // step index + 1 because we have the prerequisite block just above the steps
      this.rootSelector = `sqtm-app-execution-step:nth-of-type(${this.stepIndex + 1})
      div[data-test-element-id="execution-step"]
    `;
    }

    const commentUrl = `execution-step/${stepId || '*'}/comment`;
    this.editableCommentField = new EditableRichTextFieldElement('comment', commentUrl);
    this.issueCompactListElement = new IssueCompactListElement(() => cy.get(this.rootSelector));
    this.addIssueToolBarButton = new ToolbarButtonElement(this.rootSelector, 'step-action-button');
  }

  checkIndexLabel() {
    this.getIndexLabel().should('contain.text', (this.stepIndex + 1).toString());
  }

  checkStepExecutionStatus(expectedStatus: string) {
    this.getExecutionStatusField().should('contain.text', expectedStatus);
  }

  checkStepHeaderContent(expectedHeader: string) {
    this.getStepHeader().should('contain.text', expectedHeader);
  }

  checkStepAction(expectedAction: string) {
    this.getStepActionField().should('contain.text', expectedAction);
  }

  checkStepExpectedResult(expectedResult: string) {
    this.getStepExpectedResultField().should('contain.text', expectedResult);
  }

  checkDnzCufLabel(dnzCufOrder: string, expectedLabel: string) {
    this.getStepDnzCufLabelByOrder(dnzCufOrder).should('contain.text', expectedLabel);
  }

  checkDnzCufValue(dnzCufOrder: string, expectedValue: string) {
    this.getStepDnzCufValueByOrder(dnzCufOrder).should('contain.text', expectedValue);
  }

  checkStepComment(expectedComment: string) {
    this.getStepCommentField().should('contain.text', expectedComment);
  }

  updateComment(newComment: string) {
    this.editableCommentField.setAndConfirmValue(newComment);
  }

  assertStepExpectedResultExists() {
    this.getStepExpectedResultField().should('exist');
  }

  assertStepExpectedResultNotExists() {
    this.getStepExpectedResultField().should('not.exist');
  }

  assertCommentFieldExists() {
    this.getStepCommentField().should('exist');
  }

  assertCommentFieldNotExists() {
    this.getStepCommentField().should('not.exist');
  }

  checkIsExtended() {
    this.getCollapseArrow().should('exist');
    this.getExpendArrow().should('not.exist');
  }

  checkIsCollapsed() {
    this.getExpendArrow().should('exist');
    this.getCollapseArrow().should('not.exist');
  }

  checkStepColor(expectedColor: string) {
    this.getIndexElement().should('have.css', 'background-color', expectedColor);
  }

  collapseStep() {
    this.getCollapseArrow().click();
    this.checkIsCollapsed();
  }

  extendStep() {
    this.getExpendArrow().click();
    this.checkIsExtended();
  }

  changeStepExecutionStatus(executionStatus: ExecutionStatusKeys, executionId: number) {
    const mock = new HttpMockBuilder(
      `execution-step/${this.stepId}/status?executionId=${executionId}`,
    )
      .post()
      .responseBody({
        lastExecutedBy: 'admin',
        executionStatus: executionStatus,
        lastExecutedOn: new Date(),
      })
      .build();
    this.getStatusButton(executionStatus).click();
    mock.wait();
  }

  checkIssueRemoteIdLabel(issueIndex: number, expectedId: string) {
    this.issueCompactListElement.checkIssueRemoteIdLabel(issueIndex, expectedId);
  }

  checkIssueBtProjectLabel(issueIndex: number, expectedBtProject: string) {
    this.issueCompactListElement.checkIssueBtProjectLabel(issueIndex, expectedBtProject);
  }

  checkIssueSummaryLabel(issueIndex: number, expectedSummary: string) {
    this.issueCompactListElement.checkIssueSummaryLabel(issueIndex, expectedSummary);
  }

  checkIssuePriorityLabel(issueIndex: number, expectedPriority: string) {
    this.issueCompactListElement.checkIssuePriorityLabel(issueIndex, expectedPriority);
  }

  unbindOneIssue(issueIndex: number, issueGridResponseAfterDelete?: GridResponse) {
    const refreshMock = new HttpMockBuilder('issues/execution/*/all-known-issues')
      .responseBody(issueGridResponseAfterDelete)
      .post()
      .build();

    const issueCountMock = new HttpMockBuilder('execution/*/issue-count')
      .get()
      .responseBody({ issueCount: issueGridResponseAfterDelete?.count })
      .build();

    this.issueCompactListElement.unbindOneIssue(issueIndex);

    refreshMock.wait();
    issueCountMock.wait();
  }

  private getCollapseArrow() {
    return cy.get(`
    ${this.rootSelector}
    ${selectByDataTestElementId('collapse-step')}
    `);
  }

  private getExpendArrow() {
    return cy.get(`
    ${this.rootSelector}
    ${selectByDataTestElementId('extend-step')}
    `);
  }

  private getIndexLabel() {
    return cy.get(`
    ${this.rootSelector}
    .index-typography
    `);
  }

  private getExecutionStatusField() {
    return cy.get(`
    ${this.rootSelector}
    .execution-step-info
    .step-execution-status
    `);
  }

  private getStepActionField() {
    return cy
      .get(
        `
    ${this.rootSelector}
    `,
      )
      .find('[data-test-element-id="execution-step-action"]');
  }

  private getStepHeader() {
    return cy.get(`
    ${this.rootSelector}
    .header-action
    `);
  }

  private getStepExpectedResultField() {
    return cy
      .get(
        `
    ${this.rootSelector}
    `,
      )
      .find('[data-test-element-id="execution-step-expected-result"]');
  }

  private getStepDnzCufLabelByOrder(dnzCufOrder: string) {
    return cy
      .get(
        `
    ${this.rootSelector}
    `,
      )
      .find(`[data-test-element-id="execution-dnz-label-${dnzCufOrder}"]`);
  }

  private getStepDnzCufValueByOrder(dnzCufOrder: string) {
    return cy
      .get(
        `
    ${this.rootSelector}
    `,
      )
      .find(`[data-test-element-id="execution-dnz-value-${dnzCufOrder}"]`);
  }

  private getStepCommentField() {
    return cy
      .get(
        `
    ${this.rootSelector}
    `,
      )
      .find(`[data-test-field-id="comment"]`);
  }

  private getStatusButton(executionStatus: ExecutionStatusKeys) {
    return cy
      .get(
        `
    ${this.rootSelector}
    `,
      )
      .find(`[data-test-execution-status="${executionStatus}"]`);
  }

  private getIndexElement() {
    return cy
      .get(
        `
    ${this.rootSelector}
    `,
      )
      .find(selectByDataTestElementId('index-execution-step'));
  }

  private clickAddIssueButton() {
    this.addIssueToolBarButton.clickWithoutSpan();
  }

  getAndFillCuf(cufName: string, input: InputType, value?: string) {
    fillCuf(cufName, input, value);
  }

  createNewIssue(title: string) {
    const issueMenu = new MenuElement('step-action-menu');
    const remoteIssueDialog = new RemoteIssueDialogElements();

    this.clickAddIssueButton();
    issueMenu.item('add-issue').click();
    remoteIssueDialog.assertSpinnerIsNotPresent();
    remoteIssueDialog.typeFieldRemoteIssue('summary', title);
    remoteIssueDialog.clickOnConfirmButton();
  }

  createNewComment() {
    const issueMenu = new MenuElement('step-action-menu');

    this.clickAddIssueButton();
    issueMenu.item('add-comment').click();
  }
}
