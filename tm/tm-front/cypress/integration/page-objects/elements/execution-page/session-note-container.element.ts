import {
  selectByDataTestAttachmentId,
  selectByDataTestButtonId,
  selectByDataTestElementId,
} from '../../../utils/basic-selectors';
import { EditableRichTextFieldElement } from '../forms/editable-rich-text-field.element';
import { RichTextFieldElement } from '../forms/RichTextFieldElement';
import { ListPanelElement } from '../filters/list-panel.element';
import { SessionNoteModel } from '../../../../../projects/sqtm-core/src/lib/model/execution/execution.model';
import { HttpMock, HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { ElementSelectorFactory } from '../forms/abstract-form-field.element';
import { AttachmentCompactListElement } from '../attachments/attachment-compact-list.element';
import { IssueCompactListElement } from '../issues/issue-compact-list.element';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { AbstractElement } from '../abstract-element';
import { SimpleDeleteConfirmDialogElement } from '../dialog/simple-delete-confirm-dialog.element';

export class SessionNoteContainerElement extends AbstractElement {
  readonly existingNoteElement: ExistingNoteElement;
  readonly newNoteElement: NewNoteElement;

  constructor(public readonly rootChainableFactory: ElementSelectorFactory) {
    super();

    this.existingNoteElement = new ExistingNoteElement(() => {
      return this.find('sqtm-app-session-note');
    });

    this.newNoteElement = new NewNoteElement(this.rootChainableFactory);
  }

  beginDrag(): void {
    this.findByElementId('drag-handle')
      .trigger('mousedown', { button: 0, buttons: 1, force: true })
      .trigger('mousemove', -20, -20, { force: true })
      .trigger('mousemove', 50, 50, { force: true });
  }

  dropOver(): void {
    const httpMock = new HttpMockBuilder('execution/*/move-notes').post().build();
    this.rootElement
      .trigger('mousemove', -20, -20, { force: true })
      .trigger('mousemove', 50, 50, { force: true })
      .trigger('mouseup', { button: 0, buttons: 1 });
    httpMock.wait();
  }

  moveUp(): void {
    this.declareMoveNoteMock();
    this.findByElementId('move-up').click({ force: true });
  }

  moveDown(): void {
    this.declareMoveNoteMock();
    this.findByElementId('move-down').click({ force: true });
  }

  private declareMoveNoteMock(): void {
    // We declare a mock but don't wait for it because there may be no backend request done
    new HttpMockBuilder('execution/*/move-notes').post().build();
  }
}

export class ExistingNoteElement extends AbstractElement {
  private readonly contentRichTextField: EditableRichTextFieldElement;
  public readonly issueCompactListElement: IssueCompactListElement;

  readonly kindSelect: SessionNoteKindSelectElement;

  readonly attachmentList = new AttachmentCompactListElement();

  constructor(readonly rootChainableFactory: ElementSelectorFactory) {
    super();

    this.contentRichTextField = new EditableRichTextFieldElement(() =>
      this.findByElementId('content'),
    );
    this.kindSelect = new SessionNoteKindSelectElement(() =>
      this.find('sqtm-app-session-note-kind-select'),
    );
    this.issueCompactListElement = new IssueCompactListElement(rootChainableFactory);
  }

  checkContentText(expectedText: string): void {
    this.contentRichTextField.checkTextContent(expectedText);
  }

  updateContentText(text: string, response?: SessionNoteModel): void {
    const mock = new HttpMockBuilder<SessionNoteModel>('session-note/*/content')
      .post()
      .responseBody(response)
      .build();

    this.contentRichTextField.setAndConfirmValue(text);

    mock.wait();
  }

  clickOnExpandButton(): void {
    this.findByElementId('expand').click();
  }

  clickOnCollapseButton(): void {
    this.findByElementId('collapse').click();
  }

  clickOnAddButton(): void {
    this.find(selectByDataTestButtonId('addNoteAfter')).click();
  }

  clickRemoveButton(): SimpleDeleteConfirmDialogElement {
    this.find(selectByDataTestButtonId('remove-note')).click();
    return new SimpleDeleteConfirmDialogElement();
  }

  assertIsCollapsed(): void {
    this.contentRichTextField.assertNotExist();
  }

  assertIsExpanded(): void {
    this.contentRichTextField.assertExists();
  }

  checkAttachmentCount(expectedCount: number): void {
    this.findByElementId('attachment-counter').should('contain.text', expectedCount.toString());
  }

  checkIssueCount(expectedCount: number): void {
    this.findByElementId('issue-counter').should('contain.text', expectedCount.toString());
  }

  checkIssueData(
    issueIndex: number,
    expectedRemoteId: string,
    expectedBtProject: string,
    expectedSummary: string,
    expectedPriority: string,
    expectedStatus: string,
  ) {
    this.issueCompactListElement.checkIssueRemoteIdLabel(issueIndex, expectedRemoteId);
    this.issueCompactListElement.checkIssueBtProjectLabel(issueIndex, expectedBtProject);
    this.issueCompactListElement.checkIssueSummaryLabel(issueIndex, expectedSummary);
    this.issueCompactListElement.checkIssuePriorityLabel(issueIndex, expectedPriority);
    this.issueCompactListElement.checkIssueStatusLabel(issueIndex, expectedStatus);
  }

  unbindOneIssue(issueIndex: number, issueGridResponseAfterDelete?: GridResponse) {
    const refreshMock = new HttpMockBuilder('issues/execution/*/all-known-issues')
      .responseBody(issueGridResponseAfterDelete)
      .post()
      .build();

    const issueCountMock = new HttpMockBuilder('execution/*/issue-count')
      .get()
      .responseBody({ issueCount: issueGridResponseAfterDelete?.count })
      .build();

    this.issueCompactListElement.unbindOneIssue(issueIndex);

    refreshMock.wait();
    issueCountMock.wait();
  }

  assertAddButtonDoesNotExist() {
    this.find(selectByDataTestButtonId('addNoteAfter')).should('not.exist');
  }

  assertIsNotEditable(attachmentId: number) {
    this.assertAddButtonDoesNotExist();
    this.find(selectByDataTestElementId('note-kind-select')).should(
      'not.have.class',
      'active-cursor',
    );
    this.rootElement.find(selectByDataTestElementId('content')).should('not.be.enabled');
    this.find(selectByDataTestAttachmentId(attachmentId.toString()))
      .should('exist')
      .find(selectByDataTestElementId('delete-attachment'))
      .should('not.exist');
  }
}

export class NewNoteElement extends AbstractElement {
  readonly newNoteRichTextField: RichTextFieldElement;
  readonly kindSelect: SessionNoteKindSelectElement;

  constructor(readonly rootChainableFactory: ElementSelectorFactory) {
    super();

    this.newNoteRichTextField = new RichTextFieldElement(() => {
      return this.rootElement.find(selectByDataTestElementId('content'));
    });

    this.kindSelect = new SessionNoteKindSelectElement(() => {
      return this.find('sqtm-app-session-note-kind-select');
    });
  }

  confirm(createResponse?: SessionNoteModel): void {
    const mock = new HttpMockBuilder('session-note/create?executionId=*')
      .post()
      .responseBody(createResponse)
      .build();

    this.findByElementId('confirm-button').click();

    mock.wait();
  }
}

export type SessionNoteType = 'Commentaire' | 'Suggestion' | 'Anomalie' | 'Question' | 'Positif';

export class SessionNoteKindSelectElement extends AbstractElement {
  readonly modificationUrl = 'session-note/*/kind';

  constructor(public readonly rootChainableFactory: ElementSelectorFactory) {
    super();
  }

  checkKindText(expectedText: string): void {
    this.rootElement.should('contain.text', expectedText);
  }

  selectKind(kindToSelect: SessionNoteType, response?: SessionNoteModel): void {
    let mock: HttpMock<SessionNoteModel>;

    if (response) {
      mock = new HttpMockBuilder<SessionNoteModel>(this.modificationUrl)
        .post()
        .responseBody(response)
        .build();
    }

    this.rootElement.find('div').click();

    new ListPanelElement().toggleOneItem(kindToSelect);

    if (mock) {
      mock.wait();
    }
  }
}
