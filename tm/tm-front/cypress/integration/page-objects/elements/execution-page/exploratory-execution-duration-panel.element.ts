import { AbstractElement } from '../abstract-element';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import Chainable = Cypress.Chainable;
import { BaseDialogElement } from '../dialog/base-dialog.element';

export class ExploratoryExecutionDurationPanelElement extends AbstractElement {
  public readonly rootChainableFactory = () =>
    cy.get('sqtm-app-execution-page-session-duration-panel');

  runExploratoryExecution() {
    const mockStart = new HttpMockBuilder('exploratory-execution/*/run').post().build();
    this.clickOnStartExploratorySession();
    mockStart.wait();
  }

  stopExploratoryExecution() {
    const mockStop = new HttpMockBuilder('exploratory-execution/*/stop').post().build();
    this.clickOnStopExploratorySession();
    const dialog = new BaseDialogElement('end-exploratory-execution');
    dialog.confirm();
    mockStop.wait();
  }

  pauseExploratoryExecution() {
    const mockPause = new HttpMockBuilder('exploratory-execution/*/pause').post().build();
    this.clickOnPauseExploratorySession();
    mockPause.wait();
  }

  private clickOnStartExploratorySession() {
    this.findByElementId('run-exploratory-execution').click();
  }

  private clickOnStopExploratorySession() {
    this.findByElementId('stop-exploratory-execution').click();
  }

  private clickOnPauseExploratorySession() {
    this.findByElementId('pause-exploratory-execution').click();
  }

  assertStopButtonIsEnabled(): void {
    this.findByElementId('stop-exploratory-execution').should('not.have.class', 'disabled');
  }

  assertStopButtonIsDisabled(): void {
    this.findByElementId('stop-exploratory-execution').should('have.class', 'disabled');
  }

  assertRunButtonIsVisible(): void {
    this.findByElementId('run-exploratory-execution').should('be.visible');
  }

  assertPauseButtonIsVisible(): void {
    this.findByElementId('pause-exploratory-execution').should('be.visible');
  }

  assertTimeElapsedIsZero(): void {
    this.findByElementId('session-time-elapsed').should('contain.text', '00:00:00');
  }

  assertTimeElapsedIsNotZero(): void {
    this.findByElementId('session-time-elapsed').should('not.contain.text', '00:00:00');
  }

  assertDefinedDurationContains(expectedDuration: string): void {
    this.findByElementId('defined-duration').should('contain.text', expectedDuration);
  }

  getTimeElapsed(): Chainable<string> {
    return this.findByElementId('session-time-elapsed').then((element) => element.text());
  }

  checkStatusCapsuleValidity(expectedStatus: string) {
    return this.findByElementId('exploratory-execution-status').should(
      'contain.text',
      expectedStatus,
    );
  }
}
