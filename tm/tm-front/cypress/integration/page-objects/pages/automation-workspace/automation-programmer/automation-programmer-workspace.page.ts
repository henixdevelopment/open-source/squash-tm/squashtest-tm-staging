import { WorkspaceWithGridPage } from '../../page';
import { NavBarElement } from '../../../elements/nav-bar/nav-bar.element';
import { GridElement } from '../../../elements/grid/grid.element';

export abstract class AutomationProgrammerWorkspacePage extends WorkspaceWithGridPage {
  public readonly navBar = new NavBarElement();

  protected constructor(
    public readonly grid: GridElement,
    rootSelector: string,
  ) {
    super(grid, rootSelector);
  }

  protected abstract getPageUrl(): string;
}
