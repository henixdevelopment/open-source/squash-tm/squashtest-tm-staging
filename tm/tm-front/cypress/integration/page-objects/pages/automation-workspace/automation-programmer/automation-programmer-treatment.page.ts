import { PageFactory } from '../../page';
import { NavBarElement } from '../../../elements/nav-bar/nav-bar.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { GridElement } from '../../../elements/grid/grid.element';
import { AutomationProgrammerWorkspacePage } from './automation-programmer-workspace.page';
import {
  AutomationWorkspaceDataModel,
  defaultAutomationWorkspaceData,
} from './utils/automation-programmer-utils';
import { ReferentialDataProviderBuilder } from '../../../../utils/referential/referential-data.provider';
import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { selectByDataTestAnchorLinkId } from '../../../../utils/basic-selectors';
import { AutomationProgrammerGlobalPage } from './automation-programmer-global.page';

export class AutomationProgrammerTreatmentPage extends AutomationProgrammerWorkspacePage {
  public readonly navBar = new NavBarElement();

  public constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-automation-workspace-treat');
  }

  public static initTestAtPage: PageFactory<AutomationProgrammerTreatmentPage> = (
    initialNodes: GridResponse = { dataRows: [] },
    initialWorkspaceModel: AutomationWorkspaceDataModel = defaultAutomationWorkspaceData,
    referentialData?: ReferentialDataModel,
  ) => {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const mockBuilder = new HttpMockBuilder('automation-workspace/data')
      .responseBody(initialWorkspaceModel)
      .build();
    const gridElement = GridElement.createGridElement(
      'automation-programmer-treat',
      'automation-workspace/treatment-autom-req',
      initialNodes,
    );
    const page = new AutomationProgrammerTreatmentPage(gridElement);

    // visit page
    cy.visit(`automation-workspace/automation-programmer-workspace/treat`);

    // wait for ref data request to fire
    referentialDataProvider.wait();

    mockBuilder.wait();

    // wait for initial grid data and additional requests to fire
    page.waitInitialDataFetch();
    // Check page initialisation
    page.assertExists();

    return page;
  };

  protected getPageUrl(): string {
    return 'treatment';
  }
  clickButtonGlobal() {
    cy.get(selectByDataTestAnchorLinkId('global')).click();
    return new AutomationProgrammerGlobalPage(
      GridElement.createGridElement(
        'automation-programmer-global',
        'automation-workspace/global-autom-req',
      ),
    );
  }
}
