import { PageFactory } from '../../page';
import { NavBarElement } from '../../../elements/nav-bar/nav-bar.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { GridElement } from '../../../elements/grid/grid.element';
import { AutomationProgrammerWorkspacePage } from './automation-programmer-workspace.page';
import { ReferentialDataProviderBuilder } from '../../../../utils/referential/referential-data.provider';
import {
  AutomationWorkspaceDataModel,
  defaultAutomationWorkspaceData,
} from './utils/automation-programmer-utils';
import { selectByDataTestAnchorLinkId } from '../../../../utils/basic-selectors';
import { ToolbarElement } from '../../../elements/workspace-common/toolbar.element';
import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { AutomationProgrammerTreatmentPage } from './automation-programmer-treatment.page';

export class AutomationProgrammerAssigneePage extends AutomationProgrammerWorkspacePage {
  public readonly navBar = new NavBarElement();

  gridToolBarElement: ToolbarElement;

  public constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-automation-workspace-assignee');
    this.gridToolBarElement = new ToolbarElement('assignee-toolbar');
  }

  public static initTestAtPage: PageFactory<AutomationProgrammerAssigneePage> = (
    initialNodes: GridResponse = { dataRows: [] },
    initialWorkspaceModel: AutomationWorkspaceDataModel = defaultAutomationWorkspaceData,
    referentialData?: ReferentialDataModel,
  ) => {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const mockBuilder = new HttpMockBuilder('automation-workspace/data')
      .responseBody(initialWorkspaceModel)
      .build();
    const gridElement = GridElement.createGridElement(
      'automation-programmer-assigned',
      'automation-workspace/assignee-autom-req',
      initialNodes,
    );
    const page = new AutomationProgrammerAssigneePage(gridElement);

    // visit page
    cy.visit(`automation-workspace/automation-programmer-workspace/assignee`);

    // wait for ref data request to fire
    referentialDataProvider.wait();
    // wait for initial data fetch
    mockBuilder.wait();

    return page;
  };

  getPageUrl(): string {
    return 'assignee';
  }

  assertTitlePage() {
    cy.get('.automation-workspace-secondary-title').contains("M'étant assignés");
  }

  clickButtonTreatment() {
    cy.get(selectByDataTestAnchorLinkId('treat')).click();
    return new AutomationProgrammerTreatmentPage(
      GridElement.createGridElement(
        'automation-programmer-treat',
        'automation-workspace/treatment-autom-req',
      ),
    );
  }
}
