import { Page } from '../../../page';
import { EditableTextFieldElement } from '../../../../elements/forms/editable-text-field.element';

export class AutomationWorkspaceTestCaseViewPage extends Page {
  public readonly nameTextField: EditableTextFieldElement = new EditableTextFieldElement(
    'entity-name',
  );
  constructor() {
    super(
      'sqtm-app-automation-workspace-test-case-view > sqtm-app-test-case-view-detail >  sqtm-core-entity-view-wrapper > div',
    );
  }
}
