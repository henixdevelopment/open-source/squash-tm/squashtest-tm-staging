import { FunctionalTesterWorkspacePage } from './functional-tester-workspace.page';
import { NavBarElement } from '../../../elements/nav-bar/nav-bar.element';
import { GridElement } from '../../../elements/grid/grid.element';
import { PageFactory } from '../../page';
import { ReferentialDataProviderBuilder } from '../../../../utils/referential/referential-data.provider';
import {
  AutomationFunctionalTesterWorkspaceDataModel,
  defaultFunctionalTesterWorkspaceData,
} from './utils/functional-tester-utils';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { ToolbarElement } from '../../../elements/workspace-common/toolbar.element';
import { selectByDataTestAnchorLinkId } from '../../../../utils/basic-selectors';
import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { FunctionalTesterToBeValidatedPage } from './functional-tester-to-be-validated.page';

export class FunctionalTesterReadyToTransmitPage extends FunctionalTesterWorkspacePage {
  public readonly navBar = new NavBarElement();
  gridToolBarElement: ToolbarElement;

  public constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-ready-for-transmission-functional-tester-view');
    this.gridToolBarElement = new ToolbarElement('ready-to-transmit-toolbar');
  }

  public static initTestAtPage: PageFactory<FunctionalTesterReadyToTransmitPage> = (
    initialNodes: GridResponse = { dataRows: [] },
    initialWorkspaceModel: AutomationFunctionalTesterWorkspaceDataModel = defaultFunctionalTesterWorkspaceData,
    referentialData?: ReferentialDataModel,
  ) => {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const mockBuilder = new HttpMockBuilder('automation-tester-workspace/data')
      .responseBody(initialWorkspaceModel)
      .build();

    const gridElement = GridElement.createGridElement(
      'functional-tester-ready-for-transmission',
      'automation-tester-workspace/ready-for-transmission',
      initialNodes,
    );
    const page = new FunctionalTesterReadyToTransmitPage(gridElement);

    // visit page
    cy.visit(`automation-workspace/functional-tester-workspace`);

    // wait for ref data request to fire
    referentialDataProvider.wait();
    mockBuilder.wait();

    // wait for initial grid data and additional requests to fire
    page.waitInitialDataFetch();
    // Check page initialisation
    page.assertExists();

    return page;
  };

  protected getPageUrl(): string {
    return 'ready-for-transmission';
  }

  clickButtonToBeValidated() {
    cy.get(selectByDataTestAnchorLinkId('to-be-validated')).click();
    return new FunctionalTesterToBeValidatedPage(
      GridElement.createGridElement(
        'functional-tester-to-be-validated',
        'automation-tester-workspace/to-be-validated',
      ),
    );
  }
}
