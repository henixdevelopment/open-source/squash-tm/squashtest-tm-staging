import { FunctionalTesterWorkspacePage } from './functional-tester-workspace.page';
import { NavBarElement } from '../../../elements/nav-bar/nav-bar.element';
import { GridElement } from '../../../elements/grid/grid.element';
import { PageFactory } from '../../page';
import { ReferentialDataProviderBuilder } from '../../../../utils/referential/referential-data.provider';
import { ToolbarElement } from '../../../elements/workspace-common/toolbar.element';
import {
  AutomationFunctionalTesterWorkspaceDataModel,
  defaultFunctionalTesterWorkspaceData,
} from './utils/functional-tester-utils';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';

import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class FunctionalTesterGlobalPage extends FunctionalTesterWorkspacePage {
  public readonly navBar = new NavBarElement();
  gridToolBarElement: ToolbarElement;

  public constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-global-functional-tester-view');
    this.gridToolBarElement = new ToolbarElement('global-view-toolbar');
  }

  public static initTestAtPage: PageFactory<FunctionalTesterGlobalPage> = (
    initialNodes: GridResponse = { dataRows: [] },
    initialWorkspaceModel: AutomationFunctionalTesterWorkspaceDataModel = defaultFunctionalTesterWorkspaceData,
    referentialData?: ReferentialDataModel,
  ) => {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const mockBuilder = new HttpMockBuilder('automation-tester-workspace/data')
      .responseBody(initialWorkspaceModel)
      .build();

    const gridElement = GridElement.createGridElement(
      'functional-tester-global-view',
      'automation-tester-workspace/global-view',
      initialNodes,
    );
    const page = new FunctionalTesterGlobalPage(gridElement);

    // visit page
    cy.visit(`automation-workspace/functional-tester-workspace/global`);

    // wait for ref data request to fire
    referentialDataProvider.wait();
    mockBuilder.wait();

    // wait for initial grid data and additional requests to fire
    page.waitInitialDataFetch();
    // Check page initialisation
    page.assertExists();

    return page;
  };

  protected getPageUrl(): string {
    return 'global';
  }
}
