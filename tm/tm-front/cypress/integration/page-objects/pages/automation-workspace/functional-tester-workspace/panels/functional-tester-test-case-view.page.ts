import { Page } from '../../../page';
import { EditableTextFieldElement } from '../../../../elements/forms/editable-text-field.element';
import { TestCaseViewPage } from '../../../test-case-workspace/test-case/test-case-view.page';
import { selectByDataIcon } from '../../../../../utils/basic-selectors';

export class FunctionalTesterTestCaseViewPage extends Page {
  public readonly nameTextField: EditableTextFieldElement = new EditableTextFieldElement(
    'entity-name',
  );
  readonly testCaseViewPage = new TestCaseViewPage(1);

  constructor() {
    super(
      'sqtm-app-functional-tester-test-case-view > sqtm-app-test-case-view-detail >  sqtm-core-entity-view-wrapper > div',
    );
  }
  closeTestCaseView() {
    this.find(selectByDataIcon('close')).click();
  }
}
