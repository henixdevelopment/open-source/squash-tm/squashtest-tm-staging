import Chainable = Cypress.Chainable;
import {
  selectByDataTestElementId,
  selectByDataTestDialogId,
} from '../../../../utils/basic-selectors';
import { TreeElement } from '../../../elements/grid/grid.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { BaseDialogElement } from '../../../elements/dialog/base-dialog.element';
import { DataRowModel } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';

export class DatasetDuplicationDialogElement extends BaseDialogElement {
  public readonly dialogSelector: string = selectByDataTestDialogId(
    'test-case-tree-dataset-duplication-picker-dialog',
  );
  public readonly mock = new HttpMockBuilder<GridResponse>('test-case-tree/dataset-duplication');

  constructor(public readonly eligibleTestCaseTree: TreeElement) {
    super('test-case-tree-dataset-duplication-picker-dialog');
    this.eligibleTestCaseTree = eligibleTestCaseTree;
  }

  get selector(): Chainable {
    return cy.get(this.dialogSelector);
  }

  assertExists() {
    this.selector.should('exist');
  }

  checkTreeContent(duplicationTreeDataRows: DataRowModel[]) {
    const duplicationPicker = this.selector.find(
      selectByDataTestElementId('dataset-duplication-picker'),
    );
    duplicationPicker.should('exist');

    duplicationTreeDataRows.forEach((dataRow) => {
      this.eligibleTestCaseTree.assertNodeTextContains(dataRow.id, dataRow.data.NAME);
    });
  }

  confirm() {
    this.selector.find('[data-test-dialog-button-id="confirm"]').click();
  }

  cancel() {
    this.selector.find('[data-test-dialog-button-id="cancel"]').click();
  }
}
