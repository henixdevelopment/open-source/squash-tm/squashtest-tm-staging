import { CreateEntityDialog } from '../../create-entity-dialog.element';
import { SelectFieldElement } from '../../../elements/forms/select-field.element';
import { ProjectData } from '../../../../../../projects/sqtm-core/src/lib/model/project/project-data.model';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';

export class CreateTestCaseDialog extends CreateEntityDialog {
  testCaseKindField: SelectFieldElement;

  constructor(project?: ProjectData, domain?: BindableEntity) {
    super(
      {
        treePath: 'test-case-tree',
        viewPath: 'test-case-view',
        newEntityPath: 'new-test-case',
      },
      project,
      domain,
    );

    this.testCaseKindField = new SelectFieldElement(() => this.findByFieldName('testCaseKind'));
  }

  changeTestCaseKind(kind: string) {
    this.testCaseKindField.selectValue(kind);
  }
}
