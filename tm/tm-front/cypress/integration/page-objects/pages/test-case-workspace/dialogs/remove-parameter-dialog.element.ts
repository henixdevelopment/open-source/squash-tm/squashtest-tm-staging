import { DeleteConfirmDialogElement } from '../../../elements/dialog/delete-confirm-dialog.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';

export class RemoveParameterDialog extends DeleteConfirmDialogElement {
  constructor() {
    super('delete-param');
  }

  override deleteForFailure(_response: any) {}

  override deleteForSuccess(response?: any) {
    const removeMock = new HttpMockBuilder<any>(`parameters/*`)
      .delete()
      .responseBody(response)
      .build();
    this.clickOnConfirmButton();
    removeMock.wait();
  }
}
