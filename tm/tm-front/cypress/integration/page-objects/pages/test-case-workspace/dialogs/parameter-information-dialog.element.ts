import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';
import { EditableRichTextFieldElement } from '../../../elements/forms/editable-rich-text-field.element';
import { LinkElement } from '../../../elements/link-element';
import { BaseDialogElement } from '../../../elements/dialog/base-dialog.element';

export class ParameterInformationDialog extends BaseDialogElement {
  testCaseSourceLink: LinkElement;

  constructor() {
    super('information-param');
    this.testCaseSourceLink = new LinkElement('testCaseSource');
  }

  getNameField(url?: string): EditableTextFieldElement {
    return new EditableTextFieldElement('name', url);
  }

  getDescriptionField(url?: string): EditableRichTextFieldElement {
    return new EditableRichTextFieldElement('description', url);
  }

  closeDialog() {
    cy.get('button[data-test-dialog-button-id="close"]').click();
  }

  checkData(fieldId: string, value: string) {
    cy.get(`span[data-test-field-id="${fieldId}"]`).should('contain.text', value);
  }
}
