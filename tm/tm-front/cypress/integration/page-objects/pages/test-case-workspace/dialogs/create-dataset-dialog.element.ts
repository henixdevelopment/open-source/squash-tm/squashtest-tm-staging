import { TextFieldElement } from '../../../elements/forms/TextFieldElement';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { TestCaseParameterOperationReport } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/operation-reports.model';

export class CreateDatasetDialogElement {
  nameField: TextFieldElement;

  constructor() {
    this.nameField = new TextFieldElement('name');
  }

  fillName(value: string) {
    this.nameField.fill(value);
  }

  fillParameter(paramId: number, value: string) {
    const textField = new TextFieldElement(`${paramId}`);
    textField.fill(value);
  }

  addDataSet(operation?: TestCaseParameterOperationReport, another?: boolean) {
    const httpMock = new HttpMockBuilder('test-cases/*/datasets/new')
      .post()
      .responseBody(operation)
      .build();

    if (another) {
      this.clickButton('add-another');
    } else {
      this.clickButton('add');
    }
    httpMock.wait();
  }

  clickButton(buttonId: string) {
    cy.get(`button[data-test-dialog-button-id="${buttonId}"]`).click();
  }

  checkDialogButtons() {
    cy.get('button[data-test-dialog-button-id="add-another"]').should('exist');
    cy.get('button[data-test-dialog-button-id="add"]').should('exist');
    cy.get('button[data-test-dialog-button-id="cancel"]').should('exist');
  }
}
