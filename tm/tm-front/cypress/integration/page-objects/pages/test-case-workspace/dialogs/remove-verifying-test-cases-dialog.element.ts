import { DeleteConfirmDialogElement } from '../../../elements/dialog/delete-confirm-dialog.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class RemoveVerifyingTestCasesDialogElement extends DeleteConfirmDialogElement {
  requirementVersionId: number;
  testCases: number[];

  constructor(requirementVersionId: number, testCases: number[]) {
    super('confirm-delete');
    this.requirementVersionId = requirementVersionId;
    this.testCases = testCases;
  }

  override deleteForFailure(_response: any) {}

  override deleteForSuccess(response?: any, refreshNodeResponse?: GridResponse) {
    const removeMock = new HttpMockBuilder<any>(
      `requirement-version/${this.requirementVersionId}/verifying-test-cases/${[this.testCases]}`,
    )
      .delete()
      .responseBody(response)
      .build();
    const refreshNodeMock = new HttpMockBuilder<any>(`requirement-tree/refresh`)
      .post()
      .responseBody(refreshNodeResponse)
      .build();
    this.clickOnConfirmButton();
    removeMock.wait();
    if (response && response.verifyingTestCases.length === 0) {
      refreshNodeMock.wait();
    }
  }
}
