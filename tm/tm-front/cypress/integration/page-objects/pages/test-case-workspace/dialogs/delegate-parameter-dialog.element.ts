import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import Chainable = Cypress.Chainable;

export class DelegateParameterDialogElement {
  shouldExist() {
    cy.get('[data-test-dialog-id="call-step-param"]').should('exist');
  }

  selectRadio(radioId: string) {
    cy.get(`nz-radio-group label[data-test-radio-id="${radioId}"]`).should('exist').click();
  }

  radioIsSelected(radioId: string) {
    cy.get(`nz-radio-group label[data-test-radio-id="${radioId}"]`).should(
      'have.class',
      'ant-radio-wrapper-checked',
    );
  }

  radioNotSelected(radioId: string) {
    cy.get(`nz-radio-group label[data-test-radio-id="${radioId}"]`).should(
      'not.have.class',
      'ant-radio-wrapper-checked',
    );
  }

  getSelectField(): SelectField {
    return new SelectField('selected-dataset');
  }

  confirm(response?: any) {
    const mock = new HttpMockBuilder('test-cases/*/steps/*/parameter-assignation-mode')
      .post()
      .responseBody(response)
      .build();
    cy.get('[data-test-dialog-button-id="confirm"]').click();
    mock.wait();
  }
}

export class SelectField {
  constructor(private fieldId: string) {}

  get selector(): Chainable<any> {
    return cy.get(`nz-select[data-test-field-name=${this.fieldId}]`);
  }

  setValue(newValue?: string) {
    this.selectValue(newValue);
  }

  // Show dropdown and check that the available options names match the specified string array
  checkAllOptions(options: string[]) {
    this.selector.click();
    cy.get(`nz-option-item`).each(function (value, index) {
      cy.wrap(value).should('contain.text', options[index]);
    });
    cy.clickVoid();
    cy.get(`nz-option-item`).should('not.exist');
  }

  // Checks that the selected option matches the specified text
  checkSelectedOption(expected: string) {
    this.selector.should('contain.text', expected);
  }

  assertExists() {
    this.selector.should('exist');
  }

  // Returns the Chainable for an option of the dropdown menu
  // This won't work if the dropdown isn't shown.
  private getOption(optionName: string): Chainable<any> {
    return cy.contains(`nz-option-item`, optionName);
  }

  // Shows the dropdown and click on an option
  private selectValue(newValue: string) {
    this.selector
      .click()
      .then(() => this.getOption(newValue).click())
      .then(() => cy.clickVoid());
  }
}
