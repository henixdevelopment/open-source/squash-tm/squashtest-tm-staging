import { DeleteConfirmDialogElement } from '../../../elements/dialog/delete-confirm-dialog.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { DataRowModel } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';

export class DeleteTreeNodesDialogElement extends DeleteConfirmDialogElement {
  constructor(private treeUrl: string) {
    super('confirm-delete');
  }

  override deleteForFailure(_response: any) {}

  override deleteForSuccess(_response: any) {}

  deleteNodes(deletedNodeIds: number[], parentIds: string[], refreshedNodes: DataRowModel[]) {
    const refreshMock = new HttpMockBuilder(`${this.treeUrl}/refresh`)
      .post()
      .responseBody({ dataRows: refreshedNodes })
      .build();
    const deleteMock = new HttpMockBuilder(`/${this.treeUrl}/${deletedNodeIds.join(',')}`)
      .delete()
      .build();
    this.clickOnConfirmButton();
    deleteMock.wait();
    refreshMock.wait();
  }
}
