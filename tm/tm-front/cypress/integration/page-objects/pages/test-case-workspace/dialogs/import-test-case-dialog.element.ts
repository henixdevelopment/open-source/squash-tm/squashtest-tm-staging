import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { TestCaseXlsReport } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/import/import-test-case.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class ImportTestCaseDialog {
  chooseImportFile(fileName: string, fileType: string) {
    cy.fixture(fileName, 'binary')
      .as('importFile')
      .get('sqtm-core-attachment-file-selector')
      .find('input[type=file]')
      .then(function (el) {
        const inputFile = el[0];
        const file = Cypress.Blob.binaryStringToBlob(this.importFile, fileType);

        const testFile = new File([file], fileName, {
          type: file.type,
        });
        const dataTransfer = new DataTransfer();
        dataTransfer.items.add(testFile);
        (inputFile as HTMLInputElement).files = dataTransfer.files;
        inputFile.dispatchEvent(new Event('change'));
      });
  }

  clickImport() {
    cy.get('[data-test-dialog-button-id="import"]').click();
  }

  checkConfirmationMessage(fileName: string) {
    cy.get('[data-test-message-id="confirmation-message"').should(
      'contain.html',
      `<p>Vos données seront importées depuis le fichier <b>${fileName}</b>.</p>
<p>Cette opération ne peut être annulée. Confirmez-vous l'import ?</p>`,
    );
  }

  confirmXlsImport(gridResponse: GridResponse = {} as GridResponse, response?: TestCaseXlsReport) {
    const mockBuilder = new HttpMockBuilder('test-cases/importer/xls?frontEndErrorIsHandled=true')
      .responseBody(response)
      .post()
      .build();
    const mockTree = new HttpMockBuilder('test-case-tree/refresh')
      .responseBody(gridResponse)
      .post()
      .build();
    cy.get('[data-test-dialog-button-id="confirm"]').click();
    mockBuilder.wait();
    mockTree.wait();
  }

  assertButtonExist(buttonId: string) {
    cy.get(`[data-test-dialog-button-id="${buttonId}"]`).should('exist');
  }

  assertReportExist(reportId: string) {
    cy.get(`[data-test-report-id="${reportId}"]`).should('exist');
  }
}
