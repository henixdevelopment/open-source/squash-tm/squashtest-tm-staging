import { SelectFieldElement } from '../../../elements/forms/select-field.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { Identifier } from '../../../../../../projects/sqtm-core/src/lib/model/entity.model';
import { DataRowModel } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { BaseDialogElement } from '../../../elements/dialog/base-dialog.element';

export class AddTcFromRequirementDialogElement extends BaseDialogElement {
  constructor() {
    super('add-tc-from-req');
  }

  getField(fieldName: string): SelectFieldElement {
    return new SelectFieldElement(() => this.findByFieldName(fieldName));
  }

  confirm(destinationId?: Identifier, tcKind?: string, refreshedNodes?: DataRowModel[]) {
    const httpMock = new HttpMockBuilder(
      'test-case-tree/' + destinationId + '/content/paste-from-requirement/' + tcKind,
    )
      .post()
      .build();
    const refreshUrl = `test-case-tree/refresh`;
    const httpRefreshMock = new HttpMockBuilder(refreshUrl)
      .post()
      .responseBody({ dataRows: refreshedNodes })
      .build();
    this.clickOnConfirmButton();
    httpMock.wait();
    httpRefreshMock.wait();
  }
}
