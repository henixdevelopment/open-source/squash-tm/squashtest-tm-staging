import { TextFieldElement } from '../../../elements/forms/TextFieldElement';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { RichTextFieldElement } from '../../../elements/forms/RichTextFieldElement';
import { TestCaseParameterOperationReport } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/operation-reports.model';
import { mockEmptyAttachmentListModel } from '../../../../data-mock/generic-entity.data-mock';
import { selectByDataTestDialogButtonId } from '../../../../utils/basic-selectors';

export class CreateParameterDialogElement {
  nameField: TextFieldElement;
  descriptionField: RichTextFieldElement;

  constructor() {
    this.nameField = new TextFieldElement('name');
    this.descriptionField = new RichTextFieldElement('description');
  }

  fillName(value: string) {
    this.nameField.fill(value);
  }

  fillDescription(value: string) {
    this.descriptionField.fill(value);
  }

  addParam(operation?: TestCaseParameterOperationReport, another?: boolean) {
    const httpMock = new HttpMockBuilder('test-cases/*/parameters/new')
      .post()
      .responseBody(operation)
      .build();
    const httpMockAttachment = new HttpMockBuilder('attach-list/*')
      .get()
      .responseBody(mockEmptyAttachmentListModel())
      .build();
    if (another) {
      this.clickButton('add-another');
    } else {
      this.clickButton('add');
    }
    httpMock.wait();
    httpMockAttachment.wait();
  }

  createNewParam(name: string) {
    this.fillName(name);
    this.addParam();
  }

  clickButton(buttonId: string) {
    cy.get(selectByDataTestDialogButtonId(buttonId)).click();
  }
}
