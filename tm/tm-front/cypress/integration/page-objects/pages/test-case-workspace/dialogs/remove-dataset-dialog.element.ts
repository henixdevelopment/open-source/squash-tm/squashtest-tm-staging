import { DeleteConfirmDialogElement } from '../../../elements/dialog/delete-confirm-dialog.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';

export class RemoveDatasetDialog extends DeleteConfirmDialogElement {
  constructor() {
    super('confirm-delete');
  }

  override deleteForFailure(_response: any) {}

  override deleteForSuccess(response?: any) {
    const removeMock = new HttpMockBuilder<any>(`datasets/*`)
      .delete()
      .responseBody(response)
      .build();
    this.clickOnConfirmButton();
    removeMock.wait();
  }
}
