import { DeleteConfirmDialogElement } from '../../../elements/dialog/delete-confirm-dialog.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class RemoveTestStepsDialogElement extends DeleteConfirmDialogElement {
  testStepId: number;
  testCase: number;
  constructor(testStepId: number, testCase: number) {
    super('confirm-delete');
    this.testStepId = testStepId;
    this.testCase = testCase;
  }

  override deleteForSuccess(response?: any, refreshNodeResponse?: GridResponse) {
    const removeMock = new HttpMockBuilder<any>(
      `test-case/${this.testCase}/steps/${this.testStepId}`,
    )
      .delete()
      .responseBody(response)
      .build();
    const refreshNodeMock = new HttpMockBuilder<any>(`test-case-tree/refresh`)
      .post()
      .responseBody(refreshNodeResponse)
      .build();
    this.clickOnConfirmButton();
    removeMock.wait();
    if (response && response.testSteps.length === 0) {
      refreshNodeMock.wait();
    }
  }

  override deleteForFailure(_response: any) {}
}
