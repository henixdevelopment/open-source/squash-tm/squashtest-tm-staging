import { AlertDialogElement } from '../../../elements/dialog/alert-dialog.element';

export class ParameterNameAlertDialogElement extends AlertDialogElement {
  constructor() {
    super();
  }

  assertMessage() {
    this.assertHasMessage(
      "Le nom d'au moins un des paramètres est invalide." +
        ' Seuls les lettres majuscules, minuscules, les chiffres et les tirets - et _ sont autorisés dans les noms de paramètre.',
    );
  }
}
