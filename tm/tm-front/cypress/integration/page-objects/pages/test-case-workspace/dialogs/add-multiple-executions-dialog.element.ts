import { BaseDialogElement } from '../../../elements/dialog/base-dialog.element';
import { GroupedMultiListElement } from '../../../elements/filters/grouped-multi-list.element';

export class AddMultipleExecutionsDialogElement extends BaseDialogElement {
  constructor() {
    super('add-multiple-executions-assign-users-dialog');
  }

  selectAssignableUsersAndAddExecutions(selectedUsers: string[]) {
    const multiListElement = new GroupedMultiListElement();
    selectedUsers.forEach((user) => multiListElement.toggleOneItem(user));
  }
}
