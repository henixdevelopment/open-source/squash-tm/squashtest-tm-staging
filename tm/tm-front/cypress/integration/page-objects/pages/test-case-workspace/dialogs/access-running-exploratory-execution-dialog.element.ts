import { BaseDialogElement } from '../../../elements/dialog/base-dialog.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { IssueUIModel } from '../../../../../../projects/sqtm-core/src/lib/model/issue/issue-ui.model';
import { SessionNoteModel } from '../../../../../../projects/sqtm-core/src/lib/model/execution/execution.model';
import { SessionOverviewActivityPanelElement } from '../../session-overview/panels/session-overview-page-activity-panel.element';
import { mockExecutionModel } from '../../../../data-mock/execution.data-mock';
import { ExecutionPage } from '../../execution/execution-page';

export class AccessRunningExploratoryExecutionDialogElement extends BaseDialogElement {
  constructor() {
    super('access-running-execution-dialog');
  }

  consultActivity(sessionNotes: SessionNoteModel[], issueUiModel: IssueUIModel) {
    const sessionNotesMock = new HttpMockBuilder('session-overview/*/session-notes')
      .responseBody(sessionNotes)
      .build();
    const issueModelMock = new HttpMockBuilder('issues/session-overview/*?*')
      .responseBody(issueUiModel)
      .build();

    this.clickOnButton('consult-activity');
    sessionNotesMock.wait();
    issueModelMock.wait();

    return new SessionOverviewActivityPanelElement();
  }

  accessExecution() {
    const mock = new HttpMockBuilder('execution/*').responseBody(mockExecutionModel({})).build();
    this.clickOnButton('access-running-execution');
    mock.wait();

    return new ExecutionPage();
  }
}
