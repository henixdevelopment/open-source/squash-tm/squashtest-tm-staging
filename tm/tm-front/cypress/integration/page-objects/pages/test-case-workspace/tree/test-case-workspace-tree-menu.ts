import { ExportableScriptType, ExportScriptDialog } from '../dialogs/export-script-dialog.element';
import { TestCaseMenuItemIds } from '../test-case-workspace.page';
import { CreateTestCaseDialog } from '../dialogs/create-test-case-dialog.element';
import { CreateTestCaseFolderDialog } from '../dialogs/create-test-case-folder-dialog.element';
import { ImportTestCaseDialog } from '../dialogs/import-test-case-dialog.element';
import { TreeToolbarElement } from '../../../elements/workspace-common/tree-toolbar.element';
import { AddTcFromRequirementDialogElement } from '../dialogs/add-tc-from-requirement-dialog.element';
import { ProjectData } from '../../../../../../projects/sqtm-core/src/lib/model/project/project-data.model';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';

export class TestCaseWorkspaceTreeMenu extends TreeToolbarElement {
  constructor() {
    super('test-case-toolbar');
    // this.createButton();
  }

  openCreateTestCase(projectData?: ProjectData): CreateTestCaseDialog {
    const createButtonElement = this.createButton();
    const createMenuElement = createButtonElement.showMenu();
    const menuItem = createMenuElement.item(TestCaseMenuItemIds.NEW_TEST_CASE);
    menuItem.click();
    return new CreateTestCaseDialog(projectData, BindableEntity.TEST_CASE);
  }

  openImportTestCaseDialog(): ImportTestCaseDialog {
    const importExportButton = this.importExportButton();
    const menu = importExportButton.showMenu();
    const menuItem = menu.item(TestCaseMenuItemIds.IMPORT);
    menuItem.click();
    return new ImportTestCaseDialog();
  }

  openExportScriptDialog(scriptType: ExportableScriptType): ExportScriptDialog {
    const importExportButton = this.importExportButton();
    const menu = importExportButton.showMenu();
    const menuItem = menu.item(`export-${scriptType}-script`);
    menuItem.click();
    return new ExportScriptDialog(scriptType);
  }

  assertCopyFromReqMenuDisabled() {
    const importExportButton = this.importExportButton();
    const menu = importExportButton.showMenu();
    const menuItem = menu.item(TestCaseMenuItemIds.ADD_TC_FROM_REQ);
    menuItem.assertDisabled();
  }

  showCopyFromRequirementDialog() {
    const importExportButton = this.importExportButton();
    const menu = importExportButton.showMenu();
    const menuItem = menu.item(TestCaseMenuItemIds.ADD_TC_FROM_REQ);
    menuItem.assertEnabled();
    menuItem.click();
    return new AddTcFromRequirementDialogElement();
  }

  openCreateFolder(projectData?: ProjectData): CreateTestCaseFolderDialog {
    const createButtonElement = this.createButton();
    const createMenuElement = createButtonElement.showMenu();
    const menuItem = createMenuElement.item(TestCaseMenuItemIds.NEW_FOLDER);
    menuItem.click();
    return new CreateTestCaseFolderDialog(projectData, BindableEntity.TESTCASE_FOLDER);
  }
}
