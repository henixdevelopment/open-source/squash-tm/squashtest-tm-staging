import { Page } from '../../page';
import { ActionStepElement } from '../../../elements/test-steps/action-step.element';
import { CallStepElement } from '../../../elements/test-steps/call-step.element';
import {
  selectByDataIcon,
  selectByDataTestButtonId,
  selectByDataTestElementId,
  selectByDataTestTestStepId,
  selectByDataTestToolbarButtonId,
} from '../../../../utils/basic-selectors';
import { EditableRichTextFieldElement } from '../../../elements/forms/editable-rich-text-field.element';
import { ParameterNameAlertDialogElement } from '../dialogs/parameter-name-alert-dialog-element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import Chainable = Cypress.Chainable;
import { DataRowModel } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { TreeElement } from '../../../elements/grid/grid.element';
import { ToolbarButtonElement } from '../../../elements/workspace-common/toolbar.element';
import { BaseDialogElement } from '../../../elements/dialog/base-dialog.element';
import { RichTextFieldElement } from '../../../elements/forms/RichTextFieldElement';

export class TestStepsViewPage extends Page {
  constructor() {
    super('sqtm-app-test-steps');
  }

  readonly newStepActionField = new RichTextFieldElement('action');
  readonly newStepResultField = new RichTextFieldElement('expectedResult');

  // check the order of steps by ids.
  checkOrder(expectedIds: number[]) {
    this.getSteps().then((stepComponents) => {
      cy.wrap(stepComponents.length).should('equal', expectedIds.length);
      stepComponents.each((index, element) => {
        const stepId = element.getAttribute('data-test-test-step-id');
        cy.wrap(stepId).should('equal', expectedIds[index].toString());
      });
    });
  }

  collapseAll() {
    this.find(selectByDataTestElementId('collapse-all-steps')).click();
  }

  expendAll() {
    this.find(selectByDataTestElementId('expend-all-steps')).click();
  }

  private getSteps() {
    return this.find(selectByDataTestElementId('test-step'));
  }

  getActionStepByIndex(index: number) {
    return new ActionStepElement(null, index);
  }

  getCallStepByIndex(index: number) {
    return new CallStepElement(null, index);
  }

  checkPrerequisiteIsCollapsed() {
    this.getPrerequisiteToggleArrow().should('have.class', 'anticon-caret-right');
  }

  assertIsEmpty() {
    return this.getSteps().should('have.length', 0);
  }

  private getPrerequisiteToggleArrow() {
    return this.find('.prerequisite').find('.anticon');
  }

  private getPrerequisiteField() {
    return this.find('.prerequisite');
  }

  fillPrerequisiteFieldAndConfirm(prerequisite: string) {
    this.getPrerequisiteField()
      .click()
      .then(() => {
        const prerequisiteForm: EditableRichTextFieldElement = new EditableRichTextFieldElement(
          'prerequisite',
        );
        prerequisiteForm.setAndConfirmValue(prerequisite);
        prerequisiteForm.assertContainsText(prerequisite);
      });
  }

  parameterNameAlertDialog(): ParameterNameAlertDialogElement {
    return new ParameterNameAlertDialogElement();
  }

  enterIntoTestCase() {
    this.getDropZone()
      .trigger('mouseenter', { force: true, buttons: 1 })
      .trigger('mousemove', -20, -20, { force: true });
  }

  enterIntoTestStep(index: number) {
    this.getActionStepByIndex(index).dropZoneRequirementCoverage();
  }

  dropCalledTestCaseE2E(testCaseId: number, response?: any) {
    new HttpMockBuilder('test-cases/*/verified-requirements').responseBody({}).build();
    const url = `test-cases/${testCaseId}/steps/call-test-case`;
    const callMock = new HttpMockBuilder(url).post().responseBody(response).build();

    this.enterIntoTestCase();
    this.getDropZone().trigger('mouseup', { force: true });
    callMock.wait();
  }

  dropCalledTestCase(testCaseId: number, response?: any, refreshNodes: DataRowModel[] = []) {
    new HttpMockBuilder('test-cases/*/verified-requirements').responseBody({}).build();
    const url = `test-cases/${testCaseId}/steps/call-test-case`;
    const callMock = new HttpMockBuilder(url).post().responseBody(response).build();

    const refreshUrl = `test-case-tree/refresh`;
    const refreshTreeMock = new HttpMockBuilder<GridResponse>(refreshUrl)
      .post()
      .responseBody({ dataRows: refreshNodes })
      .build();

    this.getDropZone().trigger('mouseup', { force: true });
    callMock.wait();
    refreshTreeMock.wait();
  }

  private getDropZone(): Chainable<JQuery<HTMLDivElement>> {
    return this.find(`div.last-position-drop-zone`);
  }

  copyStepWithTopCopyIcon(idStep: number, numberStep: number) {
    const copyButton = new ToolbarButtonElement('sqtm-app-test-case-view', 'copy-button');
    const pasteButton = new ToolbarButtonElement('sqtm-app-test-case-view', 'paste-button');
    this.getActionStepByIndex(idStep).singleSelectStep();
    copyButton.click();
    pasteButton.click();
    this.getSteps().should('have.length', `${numberStep}`);
  }

  openTestcaseDrawer(indexStep: number, response?: any): TreeElement {
    const testcaseTree = TreeElement.createTreeElement(
      'test-case-tree-picker',
      'test-case-tree',
      response,
    );
    this.getActionStepByIndex(indexStep).clickStepActionButton();
    this.getActionStepByIndex(indexStep).clickActionStepActionButtonAndSelectOptionInMenu(
      'call-test-case',
    );
    testcaseTree.waitInitialDataFetch();
    return testcaseTree;
  }

  closeDrawer(index: number) {
    cy.get(selectByDataTestButtonId('close-drawer')).eq(index).click({ force: true });
  }

  openAddCoverageRequirementToActionStep(indexStep: number, response?: any) {
    const requirementTree = TreeElement.createTreeElement(
      'requirement-tree-picker',
      'requirement-tree',
      response,
    );
    this.getActionStepByIndex(indexStep).clickActionStepActionButtonAndSelectOptionInMenu(
      'add-requirement-coverage',
    );
    requirementTree.waitInitialDataFetch();
    return requirementTree;
  }

  addRequirementCoverageToStep(indexStep: number, projectName: string, requirementName: string) {
    const coverageTree = this.openAddCoverageRequirementToActionStep(indexStep);
    coverageTree.findRowId('NAME', projectName).then((idProject) => {
      coverageTree.openNodeIfClosed(idProject);
    });
    coverageTree.findRowId('NAME', requirementName).then((idRequirement) => {
      coverageTree.beginDragAndDrop(idRequirement);
    });
    this.enterIntoTestStep(indexStep);
    this.closeDrawer(0);
  }
  moveStepUp(idStep: string) {
    this.find(selectByDataTestTestStepId(idStep))
      .find(selectByDataIcon('arrowMoveStepUp'))
      .trigger('mouseover', { force: true })
      .click({ force: true });
  }

  moveStepDown(idStep: string) {
    this.find(selectByDataTestTestStepId(idStep))
      .find(selectByDataIcon('arrowMoveStepDown'))
      .trigger('mouseover', { force: true })
      .click({ force: true });
  }

  assertActionStepIsNotPresent(idStep: string) {
    this.find(selectByDataTestTestStepId(idStep)).should('not.exist');
  }

  deleteStepWithTopIcon(index: number, idStep: string) {
    const deleteDialog = new BaseDialogElement('confirm-delete');
    this.getActionStepByIndex(index).singleSelectStep();
    this.find(selectByDataTestToolbarButtonId('delete-button')).click();
    deleteDialog.assertExists();
    deleteDialog.confirm();
    this.assertActionStepIsNotPresent(idStep);
  }

  assertNewStepEditorsAreReady() {
    this.newStepActionField.assertIsReady();
    this.newStepResultField.assertIsReady();
  }
  clickPrerequisiteAndAssertNotEditable() {
    this.getPrerequisiteToggleArrow()
      .click()
      .then(() => {
        const prerequisiteForm: EditableRichTextFieldElement = new EditableRichTextFieldElement(
          'prerequisite',
        );
        prerequisiteForm.assertIsNotInEditMode();
      });
  }
  assertMoveStepDownNotExist(idStep: string) {
    this.find(selectByDataTestTestStepId(idStep))
      .find(selectByDataIcon('arrowMoveStepDown'))
      .should('not.exist');
  }
  assertCannotDeleteStepWithTopIcon(index: number) {
    this.getActionStepByIndex(index).singleSelectStep();
    this.find(selectByDataTestToolbarButtonId('delete-button')).should('not.exist');
  }
  assertCannotAddTestStep(index: number) {
    this.getActionStepByIndex(index).extendStep();
    this.getActionStepByIndex(index).assertStepButtonAddDoesNotExist();
  }
}
