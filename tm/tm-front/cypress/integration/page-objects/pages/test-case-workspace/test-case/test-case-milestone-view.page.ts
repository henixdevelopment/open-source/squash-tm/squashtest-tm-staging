import { EntityViewPage } from '../../page';
import { TestCaseStatisticPanelElement } from '../panels/test-case-statistic-panel.element';
import { selectByDataTestElementId } from '../../../../utils/basic-selectors';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';

export class TestCaseMilestoneViewPage extends EntityViewPage {
  readonly anchors = AnchorsElement.withLinkIds('information', 'dashboard');

  public constructor() {
    super('sqtm-app-test-case-milestone-view');
  }

  showInformationPanel(): MilestoneInformationPanel {
    this.anchors.clickLink('information');
    return new MilestoneInformationPanel();
  }

  showDashboard() {
    this.anchors.clickLink('dashboard');
    return new TestCaseStatisticPanelElement();
  }

  assertNameEquals(expectedName: string) {
    cy.get(this.rootSelector)
      .find(selectByDataTestElementId('milestone-label'))
      .should('contain.text', expectedName);
  }
}

export class MilestoneInformationPanel {
  assertStatusEquals(expectedStatus: string) {
    cy.get('span.label').contains('Statut').next().should('contain.text', expectedStatus);
  }

  assertEndDateEquals(expectedDate: string) {
    cy.get('span.label').contains('Échéance').next().should('contain.text', expectedDate);
  }
}
