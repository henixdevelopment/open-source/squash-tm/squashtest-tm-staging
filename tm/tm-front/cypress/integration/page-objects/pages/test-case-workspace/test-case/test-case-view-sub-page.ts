import { EntityViewPage } from '../../page';
import { TestCaseViewPage } from './test-case-view.page';

export class TestCaseViewSubPage extends EntityViewPage {
  readonly testCaseViewPage = new TestCaseViewPage(this.testCaseId);

  constructor(
    public testCaseId: number | '*',
    selector = 'sqtm-app-test-case-view-sub-page',
  ) {
    super(selector);
  }
}
