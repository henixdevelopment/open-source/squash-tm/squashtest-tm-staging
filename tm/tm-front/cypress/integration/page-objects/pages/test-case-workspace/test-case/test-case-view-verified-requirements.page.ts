import { TestCaseViewPage } from './test-case-view.page';
import { Page } from '../../page';

export class TestCaseViewVerifiedRequirementsPage extends Page {
  constructor(private parentPage: TestCaseViewPage) {
    super('sqtm-app-coverage-table');
  }

  get testCaseId(): number | string {
    return this.parentPage.testCaseId;
  }
}
