import { EntityViewPage } from '../../page';
import { CapsuleElement } from '../../../elements/workspace-common/capsule.element';
import { selectByDataTestButtonId } from '../../../../utils/basic-selectors';

export class TestCaseViewDetailPage extends EntityViewPage {
  readonly draftedByAiCapsule = new CapsuleElement('drafted-by-ai');

  constructor(public actionWordId?: number | string) {
    super('sqtm-app-test-case-view-detail');
  }
  navigateBack() {
    cy.get(this.rootSelector).find(selectByDataTestButtonId('back')).click();
  }
}
