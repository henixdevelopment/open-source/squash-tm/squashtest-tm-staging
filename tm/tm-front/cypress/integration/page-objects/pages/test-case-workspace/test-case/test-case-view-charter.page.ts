import { Page } from '../../page';
import { EditableRichTextFieldElement } from '../../../elements/forms/editable-rich-text-field.element';
import { EditableTimeFieldElement } from '../../../elements/forms/editable-time-field.element';
import { selectByDataTestButtonId } from '../../../../utils/basic-selectors';

export class TestCaseViewCharterPage extends Page {
  public readonly charterRichField: EditableRichTextFieldElement;

  public readonly durationField: EditableTimeFieldElement;

  constructor() {
    super('sqtm-app-test-case-charter');
    this.charterRichField = new EditableRichTextFieldElement('charter', `test-case/*/charter`);
    this.durationField = new EditableTimeFieldElement('duration', `test-case/*/session-duration`);
  }

  toggleCharter() {
    cy.get(selectByDataTestButtonId('toggle-charter')).click();
  }

  clickNonEditableCharter(): void {
    this.findByElementId('collapsed-charter-value').click();
  }
}
