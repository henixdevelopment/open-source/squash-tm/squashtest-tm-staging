import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { ActionAutocompleteFieldElement } from '../../../elements/forms/action-autocomplete-field.element';
import { ActionTextFieldElement } from '../../../elements/forms/action-text-field.element';
import { SelectFieldElement } from '../../../elements/forms/select-field.element';
import {
  COMMENT,
  DATATABLE,
  DOCSTRING,
  KeywordStepElement,
  KeywordStepSectionName,
} from '../../../elements/test-steps/keyword-step.element';
import { Page } from '../../page';
import { DuplicateActionWordDialogElement } from '../dialogs/duplicate-action-word-dialog.element';
import { GridElement } from '../../../elements/grid/grid.element';
import { AddTestStepOperationReport } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/operation-reports.model';
import { DuplicateActionWord } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/duplicate-action-word.model';
import { EditableTextAreaFieldElement } from '../../../elements/forms/editable-text-area-field.element';
import { selectByDataTestFieldName } from '../../../../utils/basic-selectors';

export class KeywordTestStepsViewPage extends Page {
  keywordSelectElement: SelectFieldElement;
  actionTextElement: ActionTextFieldElement;
  actionAutocompleteElement: ActionAutocompleteFieldElement;
  dataTableField: EditableTextAreaFieldElement;
  commentField: EditableTextAreaFieldElement;
  docStringField: EditableTextAreaFieldElement;

  constructor() {
    super('sqtm-app-keyword-test-steps');
    this.keywordSelectElement = new SelectFieldElement(() =>
      cy.get(selectByDataTestFieldName('keyword')),
    );
    this.actionTextElement = new ActionTextFieldElement('action');
    this.actionAutocompleteElement = new ActionAutocompleteFieldElement('action');
    this.docStringField = new EditableTextAreaFieldElement('docstring');
    this.commentField = new EditableTextAreaFieldElement('comment');
    this.dataTableField = new EditableTextAreaFieldElement('datatable');
  }

  /* Actions */

  chooseKeyword(keyword: string) {
    this.keywordSelectElement.selectValue(keyword);
  }

  fillAction(action: string) {
    this.actionTextElement.fill(action);
  }

  fillAutocompleteAction(action: string, response?: { actionList: string[] }) {
    const mock = new HttpMockBuilder('keyword-test-cases/autocomplete')
      .post()
      .responseBody(response)
      .build();
    this.actionAutocompleteElement.fill(action);
    mock.wait();
  }

  addKeywordStep(isAutocompleteActive: boolean, response?: AddTestStepOperationReport) {
    cy.get(this.rootSelector).then((element) => {
      const mockCheckDuplicate = new HttpMockBuilder('keyword-test-cases/duplicated-action')
        .post()
        .responseBody({})
        .build();

      const mockAddStep = new HttpMockBuilder('test-cases/*/steps/add-keyword-test-step')
        .post()
        .responseBody(response)
        .build();

      const mockTreeRefresh = new HttpMockBuilder('test-case-tree/refresh')
        .post()
        .responseBody({ dataRows: [], count: 0 })
        .build();

      if (element.find(this.actionTextElement.selector).length) {
        cy.get(this.actionTextElement.inputSelector).focus().type('{enter}');
      } else {
        cy.get(this.actionAutocompleteElement.inputSelector).type('{enter}');
      }

      if (isAutocompleteActive) {
        mockCheckDuplicate.wait();
      }

      mockAddStep.wait();
      mockTreeRefresh.wait();
    });
  }

  addKeywordStepWithActionWordProjectChoice(
    projectNameToSelect: string,
    response?: AddTestStepOperationReport,
    duplicateActionWords?: any,
  ) {
    // TODO PCK not sure we need this cy.get as we're not using the result in then()
    cy.get(this.rootSelector).then(() => {
      const mockCheckDuplicate = new HttpMockBuilder('keyword-test-cases/duplicated-action')
        .post()
        .responseBody(duplicateActionWords)
        .build();
      cy.get(this.actionAutocompleteElement.inputSelector).type('{enter}');
      mockCheckDuplicate.wait();

      const mockAddStep = new HttpMockBuilder('test-cases/*/steps/add-keyword-test-step')
        .post()
        .responseBody(response)
        .build();

      const mockTreeRefresh = new HttpMockBuilder('test-case-tree/refresh')
        .post()
        .responseBody({ dataRows: [], count: 0 })
        .build();

      const duplicateActionWordDialog = new DuplicateActionWordDialogElement();
      duplicateActionWordDialog.assertExists();
      duplicateActionWordDialog.checkContent(
        this.convertDuplicateActionWords(duplicateActionWords),
      );
      duplicateActionWordDialog.chooseProject(projectNameToSelect);
      duplicateActionWordDialog.confirm();

      mockAddStep.wait();
      mockTreeRefresh.wait();
    });
  }

  private clickScopeField() {
    this.getScopeField().click();
  }

  private getProjectPicker(): GridElement {
    return new GridElement('project-picker');
  }

  openProjectScopePicker(): GridElement {
    this.clickScopeField();
    return this.getProjectPicker();
  }

  confirmSelectedProject() {
    cy.get(
      `
    [data-test-dialog-button-id="confirm"]
    `,
    ).click();
  }

  private getScopeField() {
    return cy.get(`
    ${this.rootSelector}
    [data-test-element-id="grid-scope-field"]
    `);
  }

  public getScopeFieldValue(result: string) {
    cy.get(
      `
    ${this.rootSelector}
    [data-test-element-id="grid-scope-value"]
    `,
    ).should('contain', result);
  }

  private convertDuplicateActionWords(duplicateActionWords: any): DuplicateActionWord[] {
    return Object.keys(duplicateActionWords).map((key) => ({
      projectName: key,
      actionWordId: duplicateActionWords[key],
    }));
  }

  showScriptPreview(response?: { script: string }) {
    const mock = new HttpMockBuilder('keyword-test-cases/*/generated-script')
      .get()
      .responseBody(response)
      .build();
    this.getScriptPreviewIcon().click();
    mock.wait();
  }

  moveStepWithDragAndDrop(startIndex: number, lastIndex?: boolean, dropIndex?: number) {
    const mock = new HttpMockBuilder('test-cases/*/steps/move').post().build();
    const movedStep = this.getStepByIndex(startIndex);
    movedStep
      .getDraggableArea()
      .trigger('mousedown', { button: 0 })
      .trigger('mousemove', 100, 100, { force: true, buttons: 1 })
      .trigger('mousemove', 100, 200, { force: true, buttons: 1 });

    if (lastIndex) {
      const lastPositionDropZone = cy.get('.last-position-drop-zone');
      lastPositionDropZone
        .trigger('mousemove', 'center', { force: true, buttons: 1 })
        .trigger('mouseup', { force: true });
    } else {
      const dropStep = this.getStepByIndex(dropIndex).rootElement;
      dropStep
        .trigger('mousemove', 'center', { force: true, buttons: 1 })
        .trigger('mouseup', { force: true });
    }
    mock.wait();
  }

  /* Validation */

  // check the order of steps by ids.
  checkStepsOrder(expectedIds: number[]) {
    this.getSteps().then((stepComponents) => {
      cy.wrap(stepComponents.length).should('equal', expectedIds.length);
      stepComponents.each((index, element) => {
        const stepId = element.getAttribute('data-test-test-step-id');
        cy.wrap(stepId).should('equal', expectedIds[index].toString());
      });
    });
  }

  checkKeywordSelectFieldNotVisible() {
    this.keywordSelectElement.rootElement.should('not.exist');
  }

  checkActionTextFieldNotVisible() {
    this.actionTextElement.assertNotExist();
  }

  checkActionTextFieldVisible() {
    this.actionTextElement.assertExists();
  }

  checkAutocompletionMenuIsVisible() {
    this.actionAutocompleteElement.checkAutocompletionMenuIsVisible();
  }

  checkAutocompletionOptions(options: string[]) {
    this.actionAutocompleteElement.checkOptions(options);
  }

  chooseAutocompletionOptionWithClick(option: string) {
    this.actionAutocompleteElement.chooseAutocompleteOptionWithClick(option);
    this.actionAutocompleteElement.checkContent(option);
  }

  chooseAutocompletionOptionAtIndexWithKeyboard(index: number, option: string) {
    this.actionAutocompleteElement.chooseAutocompleteOptionWithKeyboard(index);
    this.actionAutocompleteElement.checkContent(option);
  }

  checkExistenceAndContentOfStepByIndex(
    stepIndex: number,
    expectedKeyword: string,
    expectedContent: string,
    expectedDatatable?: string,
    expectedDocstring?: string,
    expectedComment?: string,
  ) {
    const stepElement = this.getStepByIndex(stepIndex);
    stepElement.assertExists();
    const keywordField = stepElement.getKeywordField();
    keywordField.assertExists();
    keywordField.checkSelectedOption(expectedKeyword);
    const actionField = stepElement.getActionField();
    actionField.assertExists();
    actionField.checkContent(expectedContent);
    this.checkSectionIfNeeded(DATATABLE, stepElement, expectedDatatable);
    this.checkSectionIfNeeded(DOCSTRING, stepElement, expectedDocstring);
    this.checkSectionIfNeeded(COMMENT, stepElement, expectedComment);
  }

  private checkSectionIfNeeded(
    sectionName: KeywordStepSectionName,
    stepElement: KeywordStepElement,
    expectedValue: string,
  ) {
    const textAreaField = stepElement.getTextAreaField(sectionName);
    if (expectedValue !== undefined) {
      if (expectedValue) {
        textAreaField.assertExists();
        textAreaField.checkDisplayContent(expectedValue);
      } else {
        textAreaField.assertNotExist();
      }
    }
  }

  checkPreviewIconIsVisible() {
    this.getScriptPreviewIcon().should('exist');
  }

  checkScriptPreviewPopupContent(content: string) {
    this.getScriptPreviewPopup().should('contain', content);
  }

  /* Getters */

  private getSteps() {
    return cy.get(`
    ${this.rootSelector}
    [data-test-element-id="keyword-test-step"]
    `);
  }

  private getScriptPreviewIcon() {
    return cy.get(`
    ${this.rootSelector}
    [data-test-button-id="preview-script-icon"]
    `);
  }

  private getScriptPreviewPopup() {
    return cy.get(`[data-test-dialog-id="script-preview-dialog"]`);
  }

  getStepByIndex(stepIndex: number): KeywordStepElement {
    return new KeywordStepElement(null, stepIndex);
  }
}
