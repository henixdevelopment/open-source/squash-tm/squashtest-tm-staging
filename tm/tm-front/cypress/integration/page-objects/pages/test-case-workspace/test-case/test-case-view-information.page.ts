import { EditableSelectFieldElement } from '../../../elements/forms/editable-select-field.element';
import { TestCaseViewPage } from './test-case-view.page';
import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';
import { CheckBoxElement } from '../../../elements/forms/check-box.element';
import { EditableRichTextFieldElement } from '../../../elements/forms/editable-rich-text-field.element';
import { MilestonesTagFieldElement } from '../../../elements/forms/milestones-tag-field.element';
import { Page } from '../../page';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class TestCaseViewInformationPage extends Page {
  constructor(private parentPage: TestCaseViewPage) {
    super('sqtm-app-test-case-information-panel');
  }

  get testCaseId(): number | string {
    return this.parentPage.testCaseId;
  }

  get nameTextField(): EditableTextFieldElement {
    const url = `test-case/${this.testCaseId}/name`;
    return new EditableTextFieldElement('entity-name', url);
  }

  get referenceTextField(): EditableTextFieldElement {
    const url = `test-case/${this.testCaseId}/reference`;
    return new EditableTextFieldElement('entity-reference', url);
  }

  get statusSelectField(): EditableSelectFieldElement {
    const url = `test-case/${this.testCaseId}/status`;
    return new EditableSelectFieldElement('test-case-status', url);
  }

  get natureSelectField(): EditableSelectFieldElement {
    const url = `test-case/${this.testCaseId}/nature`;
    return new EditableSelectFieldElement('test-case-nature', url);
  }

  get typeSelectField(): EditableSelectFieldElement {
    const url = `test-case/${this.testCaseId}/type`;
    return new EditableSelectFieldElement('test-case-type', url);
  }

  get importanceSelectField(): EditableSelectFieldElement {
    const url = `test-case/${this.testCaseId}/importance`;
    return new EditableSelectFieldElement('test-case-importance', url);
  }

  get importanceAutoCheckBox(): CheckBoxElement {
    const url = `test-case/${this.testCaseId}/importance-auto`;
    return new CheckBoxElement(() => this.findByFieldId('test-case-importance-auto'), url);
  }

  get descriptionRichField(): EditableRichTextFieldElement {
    const url = `test-case/${this.testCaseId}/description`;
    return new EditableRichTextFieldElement('test-case-description', url);
  }

  get milestonesField(): MilestonesTagFieldElement {
    return new MilestonesTagFieldElement('test-case-milestones', 'test-case/*/milestones/*');
  }

  rename(newValue: string, refreshedRows?: GridResponse) {
    const refreshTreeMock = this.getRefreshTreeMock(refreshedRows);
    this.nameTextField.setAndConfirmValue(newValue);
    refreshTreeMock.wait();
  }

  changeReference(newValue: string, refreshedRows?: GridResponse) {
    const refreshTreeMock = this.getRefreshTreeMock(refreshedRows);
    this.referenceTextField.setAndConfirmValue(newValue);
    refreshTreeMock.wait();
  }

  getRefreshTreeMock(refreshedRows: GridResponse) {
    return new HttpMockBuilder(`test-case-tree/refresh`).post().responseBody(refreshedRows).build();
  }
}
