import { EntityViewPage } from '../../page';
import { TestCaseViewPage } from './test-case-view.page';
import { ReferentialDataProviderBuilder } from '../../../../utils/referential/referential-data.provider';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { NavBarElement } from '../../../elements/nav-bar/nav-bar.element';
import { AlertDialogElement } from '../../../elements/dialog/alert-dialog.element';
import { TestCaseModel } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/test-case.model';
import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';

export class TestCaseModifyDuringExecProloguePage extends EntityViewPage {
  private testCaseViewPage: TestCaseViewPage;

  constructor(public readonly testCaseId: number | '*') {
    super('sqtm-app-test-case-view-modif-during-exec');
    this.testCaseViewPage = new TestCaseViewPage(testCaseId);
  }

  public checkDataFetched() {
    this.testCaseViewPage.checkDataFetched();
  }

  public static initTestAtPage(
    testCaseModel: TestCaseModel,
    executionId = 1,
    referentialData?: ReferentialDataModel,
  ): TestCaseModifyDuringExecProloguePage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const url = `test-case-workspace/test-case/modification-during-exec/${testCaseModel.id}/execution/${executionId}`;
    const testCaseModelUrl = `test-case-view/${testCaseModel.id}?**`;
    const mock = new HttpMockBuilder(testCaseModelUrl).responseBody(testCaseModel).build();
    cy.visit(url);
    referentialDataProvider.wait();
    mock.wait();
    new NavBarElement().toggle();
    return new TestCaseModifyDuringExecProloguePage(testCaseModel.id);
  }

  goBackToExec(): AlertDialogElement {
    this.findByElementId('back-to-execution').click();
    return new AlertDialogElement();
  }
}
