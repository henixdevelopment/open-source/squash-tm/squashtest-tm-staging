import { EntityViewPage } from '../../page';
import { KeywordTestStepsViewPage } from './keyword-test-step-view-page';
import { TestStepsViewPage } from './test-steps-view.page';
import { GridElement, TreeElement } from '../../../elements/grid/grid.element';
import { apiBaseUrl, HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { RemoveCoveragesDialogElement } from '../dialogs/remove-coverages-dialog.element';
import { TestCaseViewAutomationPage } from './test-case-view-automation.page';
import { TestCaseViewInformationPage } from './test-case-view-information.page';
import { TestCaseViewParametersPage } from './test-case-view-parameters.page';
import { ToolbarMenuButtonElement } from '../../../elements/workspace-common/toolbar.element';
import { CreateNewTestCaseVersionDialog } from '../dialogs/create-new-test-case-version-dialog.element';
import { TestCaseViewExecutionsPage } from './test-case-view-executions.page';
import { TestCaseViewIssuesPage } from './test-case-view-issues.page';
import { TestCaseViewCalledTestCasesPage } from './test-case-view-called-test-cases.page';
import { basicReferentialData } from '../../../../utils/referential/referential-data.provider';
import { navigateToRequirementSearchForCoverage } from '../../requirement-workspace/search/search-page-utils';
import { RequirementForCoverageSearchPage } from '../../requirement-workspace/search/requirement-for-coverage-search-page';
import { TestCaseViewScriptPage } from './test-case-view-script.page';
import { CapsuleElement } from '../../../elements/workspace-common/capsule.element';
import { ChangeCoverageOperationReport } from '../../../../../../projects/sqtm-core/src/lib/model/change-coverage-operation-report';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { TestCaseViewCharterPage } from './test-case-view-charter.page';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';
import { fillCuf } from '../../../../scenarios/scenario-parts/custom-field.part';
import { TestCaseViewSessionsPage } from './test-case-view-sessions.page';
import { TestCaseViewVerifiedRequirementsPage } from './test-case-view-verified-requirements.page';
import {
  selectByDataTestElementId,
  selectByDataTestFieldId,
} from '../../../../utils/basic-selectors';
import { CollapsePanelButtonElement } from '../../../elements/workspace-common/collapse-panel-button.element';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import Chainable = Cypress.Chainable;

export class TestCaseViewPage extends EntityViewPage {
  readonly anchors = AnchorsElement.withLinkIds(
    'information',
    'steps',
    'keywordSteps',
    'automation',
    'parameters',
    'verified-requirements',
    'called-test-cases',
    'script',
    'charter',
    'executions',
    'sessions',
    'issues',
  );

  readonly coveragesTable = new GridElement('test-case-view-coverages');
  readonly calledTestCaseTable = new GridElement('test-case-view-called-test-case');
  readonly automationPanel = new TestCaseViewAutomationPage(this);
  readonly informationPanel = new TestCaseViewInformationPage(this);
  readonly scriptPanel = new TestCaseViewScriptPage();
  readonly statusCapsule = new CapsuleElement('status');
  readonly importanceCapsule = new CapsuleElement('importance');
  readonly draftedByAiCapsule = new CapsuleElement('drafted-by-ai');

  constructor(public readonly testCaseId: number | '*') {
    super('sqtm-app-test-case-view');
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/test-case-view/${this.testCaseId}?**`;
    cy.wait(`@${url}`);
  }

  clickStepsAnchorLink(): TestStepsViewPage {
    this.anchors.clickLink('steps');
    return new TestStepsViewPage();
  }

  clickAutomationAnchorLink(): TestCaseViewAutomationPage {
    this.anchors.clickLink('automation');
    return new TestCaseViewAutomationPage(this);
  }

  clickParametersAnchorLink(): TestCaseViewParametersPage {
    this.anchors.clickLink('parameters');
    return new TestCaseViewParametersPage(this);
  }

  clickScriptAnchorLink(): TestCaseViewScriptPage {
    this.anchors.clickLink('script');
    return new TestCaseViewScriptPage();
  }

  clickKeywordStepsAnchorLink(): KeywordTestStepsViewPage {
    this.anchors.clickLink('keywordSteps');
    return new KeywordTestStepsViewPage();
  }

  clickCalledTestCasesAnchorLink(): TestCaseViewCalledTestCasesPage {
    this.anchors.clickLink('called-test-cases');
    return new TestCaseViewCalledTestCasesPage(this);
  }

  clickCharterAnchorLink(): TestCaseViewCharterPage {
    this.anchors.clickLink('charter');
    return new TestCaseViewCharterPage();
  }

  clickVerifiedRequirementsAnchorLink(): TestCaseViewVerifiedRequirementsPage {
    this.anchors.clickLink('verified-requirements');
    return new TestCaseViewVerifiedRequirementsPage(this);
  }

  showExecutions(data: any): TestCaseViewExecutionsPage {
    const testCaseViewExecutionPage = TestCaseViewExecutionsPage.navigateTo(this.testCaseId, data);
    this.anchors.clickLink('executions');
    testCaseViewExecutionPage.grid.waitInitialDataFetch();
    return testCaseViewExecutionPage;
  }

  showSessions(gridResponse?: GridResponse): TestCaseViewSessionsPage {
    const page = new TestCaseViewSessionsPage(gridResponse);
    this.anchors.clickLink('sessions');
    page.assertExists();
    page.grid.waitInitialDataFetch();
    return page;
  }

  showIssuesIfBoundToBugTracker(
    modelResponse?: any,
    gridResponse?: GridResponse,
  ): TestCaseViewIssuesPage {
    const bugTrackerModel = new HttpMockBuilder('issues/test-case/*?frontEndErrorIsHandled=true')
      .responseBody(modelResponse)
      .build();
    const issues = new HttpMockBuilder('issues/test-case/*/known-issues')
      .responseBody(gridResponse)
      .post()
      .build();
    this.anchors.clickLink('issues');
    bugTrackerModel.wait();
    issues.wait();
    cy.removeNzTooltip();
    return new TestCaseViewIssuesPage();
  }

  showIssuesWithoutBindingToBugTracker(response?: any) {
    const httpMock = new HttpMockBuilder('issues/test-case/*?frontEndErrorIsHandled=true')
      .responseBody(response)
      .build();
    this.anchors.clickLink('issues');
    httpMock.wait();
    cy.removeNzTooltip();
    return new TestCaseViewIssuesPage();
  }

  // Basic value check : only checks if a given string is present inside the field. This doesn't work on all fields
  // so a better alternative is to use field elements methods.
  checkData(fieldId: string, value: string) {
    cy.get(selectByDataTestFieldId(fieldId)).find('span').should('contain.text', value);
  }

  showDeleteConfirmCoveragesDialog(
    testCaseId: number,
    requirementVersionIds: number[],
  ): RemoveCoveragesDialogElement {
    const removeCoverageButton = new CollapsePanelButtonElement('remove-coverages');
    removeCoverageButton.click();
    return new RemoveCoveragesDialogElement(testCaseId, requirementVersionIds);
  }

  showDeleteConfirmCoverageDialog(testCaseId: number, rowId: number) {
    const coverageTable = this.coveragesTable;
    const row = coverageTable.getRow(rowId, 'rightViewport');
    const cell = row.cell('delete');
    cell.iconRenderer().click();
    return new RemoveCoveragesDialogElement(testCaseId, [rowId]);
  }

  openRequirementDrawer(response?: any): TreeElement {
    const requirementTree = TreeElement.createTreeElement(
      'requirement-tree-picker',
      'requirement-tree',
      response,
    );
    const coverageButton = new CollapsePanelButtonElement('add-coverages');
    coverageButton.click();
    requirementTree.waitInitialDataFetch();
    return requirementTree;
  }

  navigateToSearchRequirementForCoverage(
    referentialData: ReferentialDataModel = basicReferentialData,
    initialRows: GridResponse = { dataRows: [] },
  ): RequirementForCoverageSearchPage {
    return navigateToRequirementSearchForCoverage(referentialData, initialRows);
  }

  enterIntoTestCase() {
    this.getDropZone().trigger('mouseenter', { force: true, buttons: 1 });
  }

  dropRequirementIntoTestCase(
    testCaseId: number,
    refreshedCoverages?: ChangeCoverageOperationReport,
  ) {
    const url = `test-cases/${testCaseId}/verified-requirements`;
    const mock = new HttpMockBuilder(url).post().responseBody(refreshedCoverages).build();
    this.getDropZone().trigger('mouseup', { force: true });
    mock.wait();
  }

  closeRequirementDrawer() {
    cy.get('.ant-drawer-body .anticon-close').first().click();
  }

  openCreateNewVersion(): CreateNewTestCaseVersionDialog {
    const actionMenu = new ToolbarMenuButtonElement(
      'sqtm-core-entity-view-header',
      'action-menu-button',
      'action-menu',
    );
    actionMenu.assertExists();
    const menuElement = actionMenu.showMenu();
    menuElement.assertExists();
    const item = menuElement.item('create-test-case-version');
    item.assertExists();
    item.assertEnabled();
    item.click();
    return new CreateNewTestCaseVersionDialog();
  }

  private getDropZone(
    TEST_CASE_DROP_ZONE_ID = 'test-case-view-content',
  ): Chainable<JQuery<HTMLDivElement>> {
    return cy.get(selectByDataTestElementId(TEST_CASE_DROP_ZONE_ID));
  }

  assertMilestoneLockedWarningExist() {
    this.getMilestoneWarning().should('exist');
  }

  private getMilestoneWarning() {
    return this.findByElementId('milestone-locked-warning');
  }

  assertMilestoneLockedWarningNotExist() {
    this.getMilestoneWarning().should('not.exist');
  }

  addCufDate(cufName: string, cufValue: string) {
    fillCuf(cufName, InputType.DATE_PICKER, cufValue);
  }

  clickAndChooseOptionCufValueInList(cufName: string, optionValue: string) {
    fillCuf(cufName, InputType.DROPDOWN_LIST, optionValue);
  }

  deleteRequirementCoverageWithTopIcon(nameRequirement: string) {
    const deleteLinkRequirementDialog = new RemoveCoveragesDialogElement(1, [1]);

    this.coveragesTable.findRowId('name', nameRequirement).then((idRequirement) => {
      this.coveragesTable.selectRow(idRequirement, '#', 'leftViewport');
    });
    this.showDeleteConfirmCoveragesDialog(1, [1]);
    deleteLinkRequirementDialog.confirm();
  }

  deleteRequirementCoverageWithEndOfLineIcon(nameRequirement: string) {
    const deleteLinkRequirementDialog = new RemoveCoveragesDialogElement(2, [3]);

    this.coveragesTable.findRowId('name', nameRequirement).then((idRequirement) => {
      this.showDeleteConfirmCoverageDialog(1, idRequirement);
    });
    deleteLinkRequirementDialog.confirm();
  }

  linkRequirementToTestCaseAndAssert(reqName: string) {
    const requirementLinkTree = this.openRequirementDrawer();
    requirementLinkTree.findRowId('NAME', reqName).then((idRequirement) => {
      requirementLinkTree.beginDragAndDrop(idRequirement);
    });
    this.dropRequirementIntoTestCase(4);
    this.closeRequirementDrawer();
    this.coveragesTable.findRowId('name', reqName).then((idRequirement) => {
      this.coveragesTable.assertRowExist(idRequirement);
    });
  }
}
