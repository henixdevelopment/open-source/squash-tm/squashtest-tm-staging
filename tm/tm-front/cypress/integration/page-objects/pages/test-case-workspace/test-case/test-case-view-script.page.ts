import { Page } from '../../page';
import { ToolbarElement } from '../../../elements/workspace-common/toolbar.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { selectByDataTestElementId } from '../../../../utils/basic-selectors';

export class TestCaseViewScriptPage extends Page {
  CONFIRM_BUTTON_ID = 'ace-confirm';
  CANCEL_BUTTON_ID = 'ace-cancel';
  CHECK_BUTTON_ID = 'ace-check';
  INSERT_BUTTON_ID = 'ace-insert';
  HELP_BUTTON_ID = 'ace-help';
  toolbarElement: ToolbarElement;

  constructor() {
    super('sqtm-app-script');
    this.toolbarElement = new ToolbarElement('ace-toolbar');
  }

  showEditButtons() {
    this.findByElementId('ace-editor').click();
    this.toolbarElement.shouldExist();
  }

  showSnippetPanel() {
    this.findByElementId('ace-editor').type('{enter}');
    this.toolbarElement.button(this.INSERT_BUTTON_ID).clickWithoutSpan();
    cy.get('div.ace_autocomplete').should('exist');
    cy.get('div.ace_active-line').click({ force: true });
  }

  insertScript(script: string) {
    this.showEditButtons();
    cy.get('textarea').type(script);
    this.confirmEdit();
    cy.get(selectByDataTestElementId('ace-editor')).contains(script);
  }

  clearScript() {
    this.showEditButtons();
    cy.get('textarea').clear();
    this.confirmEdit();
  }

  confirmEdit() {
    const httpMock = new HttpMockBuilder('test-case/*/scripted').post().build();
    this.toolbarElement.button(this.CONFIRM_BUTTON_ID).clickWithoutSpan();
    httpMock.wait();
  }

  assertAceEditorExist() {
    this.findByElementId('ace-editor').should('exist');
  }

  toggleHelp() {
    cy.get('[data-test-button-id="ace-help"').click();
  }

  assertHelpExist() {
    cy.get('div.ace_editor').should('have.length', 2);
  }
  assertCannotEditScriptField() {
    this.find(selectByDataTestElementId('ace-editor')).click();
    this.find('textarea.ace_text-input').should('have.attr', 'readonly');
  }
}
