import { TestCaseViewPage } from './test-case-view.page';
import { Page } from '../../page';
import { GridElement, TreeElement } from '../../../elements/grid/grid.element';
import { CreateDatasetDialogElement } from '../dialogs/create-dataset-dialog.element';
import { CreateParameterDialogElement } from '../dialogs/create-parameter-dialog.element';
import { RemoveDatasetDialog } from '../dialogs/remove-dataset-dialog.element';
import { MenuElement } from '../../../../utils/menu.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { RemoveParameterDialog } from '../dialogs/remove-parameter-dialog.element';
import { ParameterInformationDialog } from '../dialogs/parameter-information-dialog.element';
import {
  GridCellSelectorBuilder,
  GridHeaderRowElement,
} from '../../../../utils/grid-selectors.builder';
import { DatasetDuplicationDialogElement } from '../dialogs/dataset-duplication-dialog.element';
import { TestCaseParameterOperationReport } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/operation-reports.model';
import {
  selectByDataTestButtonId,
  selectByDataTestCellId,
  selectByDataTestGridId,
  selectByDataTestIconId,
  selectByDataTestLinkId,
  selectByDataTestRowId,
} from '../../../../utils/basic-selectors';

export class TestCaseViewParametersPage extends Page {
  constructor(public parentPage: TestCaseViewPage) {
    super('sqtm-app-datasets-table');
  }

  get testCaseId(): number | string {
    return this.parentPage.testCaseId;
  }

  get parametersTable(): GridElement {
    return new GridElement('test-case-view-datasets');
  }

  openAddDataSetDialog(): CreateDatasetDialogElement {
    const addDataSet = cy.get(selectByDataTestButtonId('add-dataset'));
    addDataSet.should('exist');
    addDataSet.click();
    return new CreateDatasetDialogElement();
  }

  openAddParameterDialog(): CreateParameterDialogElement {
    const addParam = cy.get(selectByDataTestButtonId('add-param'));
    addParam.should('exist');
    addParam.click();
    return new CreateParameterDialogElement();
  }

  openDeleteDataSetDialog(rowId: string): RemoveDatasetDialog {
    cy.get(selectByDataTestGridId('test-case-view-datasets'))
      .find(selectByDataTestRowId(rowId))
      .find(selectByDataTestCellId('delete'))
      .find('sqtm-app-delete-dataset')
      .click();
    return new RemoveDatasetDialog();
  }
  openDuplicateDataSetMenu(rowId: number) {
    this.parametersTable.getRow(rowId, 'rightViewport').cell('copy').iconRenderer().click();
  }

  openDuplicateDataSetDialogAndInitializeTree(
    rowId: number,
    tree: TreeElement,
  ): DatasetDuplicationDialogElement {
    this.openDuplicateDataSetMenu(rowId);
    cy.get(selectByDataTestLinkId('copy-to-another-tc')).click();

    return new DatasetDuplicationDialogElement(tree);
  }

  openDuplicateDataSetDialogAndCopyDataSet(rowId: number, dataSetName: string | RegExp) {
    this.openDuplicateDataSetMenu(rowId);
    cy.get(selectByDataTestLinkId('duplicate-dataset')).click();
    this.parametersTable.findRowId('name', dataSetName, 'leftViewport').then((idDataset) => {
      this.parametersTable
        .getRow(idDataset, 'leftViewport')
        .cell('name')
        .selectRenderer()
        .assertContainTextOrRegExp(dataSetName);
    });
  }
  openDuplicateDataSetDialogAndCannotDuplicateDataSet(rowId: number, dataSetName: string) {
    this.openDuplicateDataSetMenu(rowId);
    cy.get(selectByDataTestLinkId('duplicate-dataset')).click();
    this.parametersTable.assertRowIdNotExist('name', dataSetName, 'leftViewport');
  }
  mockDatasetDuplicationRequest(
    dataTestLinkId: string,
    operation: TestCaseParameterOperationReport,
  ) {
    const httpMock = new HttpMockBuilder('test-cases/*/datasets/new')
      .post()
      .responseBody(operation)
      .build();
    cy.get(selectByDataTestLinkId(dataTestLinkId)).click();
    httpMock.wait();
  }

  openDeleteParamDialog(headerId: string, isUsed?: ParameterUsed) {
    this.parametersTable.getHeaderRow().cell(headerId).findIconInCell('ellipsis').click();
    const menuElement = new MenuElement('param-menu');
    menuElement.assertExists();
    const removeParamItem = menuElement.item('remove-param');
    removeParamItem.assertExists();
    const httpMock = new HttpMockBuilder('parameters/*/used').responseBody(isUsed).build();
    removeParamItem.click();
    httpMock.wait();
  }

  openInformationParamDialog(headerId: string): ParameterInformationDialog {
    cy.get(`[data-test-cell-id="${headerId}"] i[data-test-icon-id="param-action"]`).trigger(
      'mouseenter',
    );
    const menuElement = new MenuElement('param-menu');
    menuElement.assertExists();
    const removeParamItem = menuElement.item('information-param');
    removeParamItem.assertExists();
    removeParamItem.click();
    return new ParameterInformationDialog();
  }

  renameParam(paramName: string, newValue: string) {
    const gridHeaderRowElement = this.parametersTable.getHeaderRow() as GridHeaderRowElement;
    gridHeaderRowElement.findCellId('name', paramName).then((paramId) => {
      const gridCellSelectorBuilder = gridHeaderRowElement.cell(paramId) as GridCellSelectorBuilder;
      gridCellSelectorBuilder.findCellTextSpan().should('contain', paramName);
      gridCellSelectorBuilder.textRenderer().editText(newValue, 'parameters/*/rename');
      gridCellSelectorBuilder.findCellTextSpan().should('contain', newValue);
    });
  }

  checkExistingParam(paramName: string) {
    const gridHeaderRowElement = this.parametersTable.getHeaderRow() as GridHeaderRowElement;
    gridHeaderRowElement.findCellId('name', paramName).then((paramId) => {
      const gridCellSelectorBuilder = gridHeaderRowElement.cell(paramId) as GridCellSelectorBuilder;
      gridCellSelectorBuilder.findCellTextSpan().contains(new RegExp('\\s' + paramName + '\\s'));
    });
  }

  checkExistingDataSet(datasetName: string) {
    const gridElement = this.parametersTable as GridElement;
    gridElement.findRowId('name', datasetName, 'leftViewport').then((dataSetId) => {
      gridElement.selectRow(dataSetId, 'name', 'leftViewport');
    });
  }

  renameDataSet(datasetName: string, newName: string) {
    const gridElement = this.parametersTable as GridElement;
    gridElement.findRowId('name', datasetName, 'leftViewport').then((dataSetId) => {
      const gridCellSelectorBuilder = gridElement.getCell(
        dataSetId,
        'name',
        'leftViewport',
      ) as GridCellSelectorBuilder;
      gridCellSelectorBuilder.findCellTextSpan().should('contain', datasetName);
      gridCellSelectorBuilder.textRenderer().editText(newName, 'datasets/rename');
      gridCellSelectorBuilder.findCellTextSpan().should('contain', newName);
    });
  }

  changeParamValue(datasetName: string, paramName: string, newValue: string) {
    const gridElement = this.parametersTable as GridElement;
    gridElement.findRowId('name', datasetName, 'leftViewport').then((dataSetId) => {
      const gridHeaderRowElement = this.parametersTable.getHeaderRow() as GridHeaderRowElement;
      gridHeaderRowElement.findCellId('name', paramName).then((paramId) => {
        const gridCellSelectorBuilder = gridElement.getCell(
          dataSetId,
          paramId,
        ) as GridCellSelectorBuilder;
        gridCellSelectorBuilder.textRenderer().editText(newValue, 'dataset-param-values');
        gridCellSelectorBuilder.findCellTextSpan().should('contain', newValue);
      });
    });
  }

  get paramRemoveDialog() {
    return new RemoveParameterDialog();
  }
  addDataSetNotExist() {
    cy.get(selectByDataTestButtonId('add-dataset')).should('not.exist');
  }
  assertDuplicateDataSetIsNotClickable(datasetContent: string) {
    this.parametersTable.findRowId('name', datasetContent, 'leftViewport').then((idDataSet) => {
      this.openDuplicateDataSetMenu(idDataSet);
    });
    cy.get(selectByDataTestLinkId('duplicate-dataset')).should(
      'have.class',
      'copy-menu-link m-b-10 disabled ng-star-inserted',
    );
  }
  assertCopyDataSetToOtherTestCaseIsClickable(datasetContent: string) {
    this.parametersTable.findRowId('name', datasetContent, 'leftViewport').then((idDataSet) => {
      this.openDuplicateDataSetMenu(idDataSet);
    });
    cy.get(selectByDataTestLinkId('copy-to-another-tc')).should(
      'have.class',
      'copy-menu-link ng-star-inserted',
    );
  }
  unableToChangeParamValue(datasetName: string, paramName: string) {
    const gridElement = this.parametersTable as GridElement;
    gridElement.findRowId('name', datasetName, 'leftViewport').then((dataSetId) => {
      const gridHeaderRowElement = this.parametersTable.getHeaderRow() as GridHeaderRowElement;
      gridHeaderRowElement.findCellId('name', paramName).then((paramId) => {
        const gridCellSelectorBuilder = gridElement.getCell(
          dataSetId,
          paramId,
        ) as GridCellSelectorBuilder;
        gridCellSelectorBuilder.textRenderer().assertIsNotEditable();
      });
    });
  }
  openMenuParamAndCheckDeleteOptionNotExist(headerId: string) {
    this.find(selectByDataTestCellId(headerId))
      .find(selectByDataTestIconId('param-action'))
      .trigger('mouseenter');
    const menuElement = new MenuElement('param-menu');
    menuElement.assertExists();
    const removeParamItem = menuElement.item('remove-param');
    removeParamItem.assertNotExist();
  }
  iconDeleteDataSetNotExist(rowId: string) {
    this.find(selectByDataTestGridId('test-case-view-datasets'))
      .find(selectByDataTestRowId(rowId))
      .find(selectByDataTestCellId('delete'))
      .should('not.exist');
  }
}

export interface ParameterUsed {
  PARAMETER_USED: boolean;
}
