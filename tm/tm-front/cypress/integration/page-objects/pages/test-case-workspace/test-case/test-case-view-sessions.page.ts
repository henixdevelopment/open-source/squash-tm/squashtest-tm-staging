import { Page } from '../../page';
import { GridElement } from '../../../elements/grid/grid.element';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class TestCaseViewSessionsPage extends Page {
  readonly grid: GridElement;

  constructor(gridResponse?: GridResponse) {
    super('sqtm-app-session-history');
    this.grid = GridElement.createGridElement(
      'test-case-view-sessions',
      'test-case/*/sessions',
      gridResponse,
    );
  }
}
