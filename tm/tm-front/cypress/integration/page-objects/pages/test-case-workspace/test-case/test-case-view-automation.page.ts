import { Page } from '../../page';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { GenericTextFieldElement } from '../../../elements/forms/generic-text-field.element';
import { EditableSelectFieldElement } from '../../../elements/forms/editable-select-field.element';
import { TestCaseViewPage } from './test-case-view.page';
import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';
import { TestCaseAutomatableKeys } from '../../../../../../projects/sqtm-core/src/lib/model/level-enums/level-enum';
import { TestCaseModel } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/test-case.model';
import { AutomationRequest } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/automation-request-model';
import {
  selectByDataTestButtonId,
  selectByDataTestFieldId,
  selectByDataTestIconId,
} from '../../../../utils/basic-selectors';
import Chainable = Cypress.Chainable;

export class TestCaseViewAutomationPage extends Page {
  constructor(private readonly parentPage: TestCaseViewPage) {
    super('sqtm-app-test-case-view-automation-panel');
  }

  setAutomatable(newState: TestCaseAutomatableKeys, response?: AutomationRequest) {
    const mock = new HttpMockBuilder('test-case/*/automatable')
      .post()
      .responseBody(response)
      .build();

    this.getAutomatableOption(newState).click();

    mock.wait();
  }

  chooseScm(nameScm: string) {
    this.selectObjectInFieldList(nameScm, 'scmRepositoryId');
  }

  chooseTechnology(nameTechno: string) {
    this.selectObjectInFieldList(nameTechno, 'automationTechno');
  }
  chooseAutomationStatus(status: string) {
    this.selectObjectInFieldList(status, 'test-case-automation-request-status');
  }

  insertReferenceOfTestAuto(reference: string) {
    const referenceTestAutoField = new EditableTextFieldElement('AutomFieldEditable');
    referenceTestAutoField.setValue(reference);
    referenceTestAutoField.clickOnConfirmButton();
    referenceTestAutoField.assertContainsText(reference);
  }

  get priority(): EditableTextFieldElement {
    return new EditableTextFieldElement(
      'test-case-automation-priority',
      'test-case/*/automation-request/priority',
    );
  }

  get uuid(): GenericTextFieldElement {
    return new GenericTextFieldElement('test-case-automation-uuid');
  }

  get transmittedOn(): GenericTextFieldElement {
    return new GenericTextFieldElement('test-case-automation-transmitted-on');
  }

  get scmRepository(): EditableSelectFieldElement {
    return new EditableSelectFieldElement('scmRepositoryId');
  }
  get requestStatus(): EditableSelectFieldElement {
    return new EditableSelectFieldElement('test-case-automation-request-status');
  }

  get remoteStatus(): GenericTextFieldElement {
    return new GenericTextFieldElement('test-case-automation-remote-status');
  }

  get syncErrorMessage(): GenericTextFieldElement {
    return new GenericTextFieldElement('tc-automation-sync-error-message');
  }

  get remoteAssignedTo(): GenericTextFieldElement {
    return new GenericTextFieldElement('test-case-automation-assigned-to');
  }

  get remoteUrl(): GenericTextFieldElement {
    return new GenericTextFieldElement('test-case-automation-remote-request-url');
  }

  get automated(): GenericTextFieldElement {
    return new GenericTextFieldElement('test-case-automation-automated');
  }
  get automationTechnology(): EditableSelectFieldElement {
    return new EditableSelectFieldElement('automationTechno');
  }

  transmit(response?: TestCaseModel[]): void {
    const mock = response
      ? new HttpMockBuilder<TestCaseModel[]>('automation-requests/*/status')
          .responseBody(response)
          .post()
          .build()
      : null;

    this.parentPage.find(selectByDataTestButtonId('test-case-automation-transmit')).click();

    if (mock !== null) {
      mock.wait();
    }
  }

  private getAutomatableOption(automatable: TestCaseAutomatableKeys): Chainable<any> {
    return cy
      .get(this.rootSelector)
      .find(selectByDataTestFieldId('test-case-automatable'))
      .find('label.ant-radio-wrapper')
      .eq(this.getOptionIndexForAutomatable(automatable));
  }

  private getOptionIndexForAutomatable(expectedState: TestCaseAutomatableKeys) {
    switch (expectedState) {
      case 'M':
        return 0;
      case 'Y':
        return 1;
      case 'N':
        return 2;
    }
  }

  private selectObjectInFieldList(nameObject: string, fieldId: string) {
    const editableSelectField = new EditableSelectFieldElement(fieldId);
    return editableSelectField.setAndConfirmValueNoButton(nameObject);
  }

  checkTransmitButtonIcon(expectedIconName: string) {
    this.parentPage.find(selectByDataTestIconId(expectedIconName)).should('exist');
  }
}
