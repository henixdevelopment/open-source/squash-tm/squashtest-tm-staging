import { EntityViewPage } from '../../page';
import { FolderInformationPanelElement } from '../../../elements/panels/folder-information-panel.element';
import { TestCaseStatisticPanelElement } from '../panels/test-case-statistic-panel.element';
import { apiBaseUrl } from '../../../../utils/mocks/request-mock';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';

export class TestCaseLibraryViewPage extends EntityViewPage {
  readonly anchors = AnchorsElement.withLinkIds('information', 'dashboard');

  constructor(private libraryId: number | '*') {
    super('sqtm-app-test-case-library-view');
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/test-case-library-view/${this.libraryId}?**`;
    cy.wait(`@${url}`);
  }

  showInformationPanel(): FolderInformationPanelElement {
    this.anchors.clickLink('information');
    return new FolderInformationPanelElement(this.libraryId);
  }

  showDashboard() {
    this.anchors.clickLink('dashboard');
    return new TestCaseStatisticPanelElement();
  }
}
