import { Page, PageFactory } from '../page';
import { GridElement, TreeElement } from '../../elements/grid/grid.element';
import { TestCaseWorkspaceTreeMenu } from './tree/test-case-workspace-tree-menu';
import { ReferentialDataProviderBuilder } from '../../../utils/referential/referential-data.provider';
import { NavBarElement } from '../../elements/nav-bar/nav-bar.element';
import { TestCaseLibraryViewPage } from './test-case/test-case-library-view.page';
import { TestCaseViewPage } from './test-case/test-case-view.page';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { TestCaseSearchPage } from './research/test-case-search-page';
import { TestCaseFolderViewPage } from './test-case-folder/test-case-folder-view.page';
import { WorkspaceWithTreePage } from '../workspace-with-tree.page';
import { ReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { ToolbarButtonElement } from '../../elements/workspace-common/toolbar.element';
import { CreateTestCaseDialog } from './dialogs/create-test-case-dialog.element';
import { CreateTestCaseFolderDialog } from './dialogs/create-test-case-folder-dialog.element';
import { MenuElement } from '../../../utils/menu.element';
import { AddTcFromRequirementDialogElement } from './dialogs/add-tc-from-requirement-dialog.element';
import { selectByDataTestToolbarButtonId } from '../../../utils/basic-selectors';
import { SimpleDeleteConfirmDialogElement } from '../../elements/dialog/simple-delete-confirm-dialog.element';

export enum TestCaseMenuItemIds {
  NEW_TEST_CASE = 'new-test-case',
  NEW_FOLDER = 'new-folder',
  IMPORT = 'import-test-case',
  ADD_TC_FROM_REQ = 'add-test-case',
}

export class TestCaseWorkspacePage extends WorkspaceWithTreePage {
  public readonly navBar: NavBarElement = new NavBarElement();
  public readonly treeMenu: TestCaseWorkspaceTreeMenu;

  public constructor(
    public readonly tree: TreeElement,
    rootSelector: string,
  ) {
    super(tree, rootSelector);
    this.treeMenu = new TestCaseWorkspaceTreeMenu();
  }

  public static initTestAtPage: PageFactory<TestCaseWorkspacePage> = (
    initialNodes: GridResponse = { dataRows: [] },
    referentialData?: ReferentialDataModel,
  ) => {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const tree = TreeElement.createTreeElement(
      'test-case-workspace-main-tree',
      'test-case-tree',
      initialNodes,
    );
    // visit page
    cy.visit('test-case-workspace');
    // wait for ref data request to fire
    referentialDataProvider.wait();
    // wait for initial tree data request to fire
    tree.waitInitialDataFetch();
    return new TestCaseWorkspacePage(tree, 'sqtm-app-test-case-workspace');
  };

  selectMultipleTreeNodesByName(
    names: Array<string>,
    nodeType: string,
    isLeaf: boolean,
    contentResponse: any = null,
  ) {
    let contentMock = null;

    // If the node being double clicked is not a leaf, it will open so we need to wait for its content
    if (!isLeaf) {
      const contentUrl = 'test-case-tree/*/content';
      contentMock = new HttpMockBuilder<GridResponse>(contentUrl)
        .responseBody(contentResponse)
        .build();
    }

    const firstName = names[0];
    const remainingNames = names.slice(1);

    this.selectTreeNodeByNameSimpleClick(firstName, nodeType, isLeaf);

    for (const name of remainingNames) {
      cy.get('body')
        .type('{ctrl}', { release: false })
        .get('sqtm-core-tree-node-cell-renderer')
        .find('span')
        .contains(name)
        .click();

      if (contentMock !== null) {
        contentMock.wait();
      }
    }
  }

  selectTreeNodeByNameSimpleClick(
    name: string,
    nodeType: string,
    isLeaf: boolean,
    contentResponse: any = null,
  ): Page {
    let page;

    switch (nodeType) {
      case 'project':
        new HttpMockBuilder(`test-case-library-view/*`).responseBody({}).build();
        page = new TestCaseLibraryViewPage('*');
        break;
      case 'testCase':
        new HttpMockBuilder(`test-case-view/*`).responseBody({}).build();
        page = new TestCaseViewPage('*');
        break;
      case 'folder':
        page = new TestCaseFolderViewPage('*');
        break;
      default:
        throw Error(`Unknown nodeType : ${nodeType}`);
    }

    let contentMock = null;

    // If the node being double clicked is not a leaf, it will open so we need to wait for its content
    if (!isLeaf) {
      const contentUrl = 'test-case-tree/*/content';
      contentMock = new HttpMockBuilder<GridResponse>(contentUrl)
        .responseBody(contentResponse)
        .build();
    }

    cy.removeNzTooltip();
    cy.get('sqtm-core-tree-node-cell-renderer').find('span').contains(name).click();

    if (contentMock !== null) {
      contentMock.wait();
    }
    page.assertExists();

    return page;
  }

  clickSearchButton(): TestCaseSearchPage {
    cy.get(this.rootSelector).find(selectByDataTestToolbarButtonId('research-button')).click();
    const grid = GridElement.createGridElement('test-case-search', 'search/test-case');
    return new TestCaseSearchPage(grid);
  }

  clickDeleteButton(confirm: boolean) {
    const deleteButtonTestToolbar = new ToolbarButtonElement(
      'sqtm-app-test-case-workspace-tree',
      'delete-button',
    );
    deleteButtonTestToolbar.clickWithoutSpan();
    if (confirm) {
      this.confirmDelete();
    } else {
      this.cancelDelete();
    }
  }

  clickOnCreateButton() {
    const createButtonTestToolbar = new ToolbarButtonElement(
      'sqtm-app-test-case-workspace-tree',
      'create-button',
    );
    createButtonTestToolbar.clickWithoutSpan();
  }

  createObjectInTestCaseWorkspace(item: 'new-test-case' | 'new-folder') {
    const createMenu = new MenuElement('create-menu');
    this.clickOnCreateButton();
    createMenu.item(item).click();
    if (item === 'new-test-case') {
      return new CreateTestCaseDialog();
    }
    if (item === 'new-folder') {
      return new CreateTestCaseFolderDialog();
    }
  }

  private confirmDelete() {
    const confirmDeleteDialog = new SimpleDeleteConfirmDialogElement();
    confirmDeleteDialog.assertExists();
    confirmDeleteDialog.confirm();
  }

  private cancelDelete() {
    const cancelDeleteDialog = new SimpleDeleteConfirmDialogElement();
    cancelDeleteDialog.assertExists();
    cancelDeleteDialog.cancel();
  }

  openImportExportMenuAndSelectOption(
    itemId:
      | 'import-test-case'
      | 'export-test-case'
      | 'add-test-case'
      | 'automation'
      | 'export-gherkin-script'
      | 'export-bdd-script',
  ) {
    const importExportMenu = new MenuElement('import-export-menu');
    this.clickOnImportExportButton();
    importExportMenu.item(itemId).click();
  }

  createTestCaseFromImportedRequirement(
    formatValue: 'Classique' | 'Gherkin' | 'BDD' | 'Exploratoire',
  ) {
    const createTestCaseFromRequirementDialog = new AddTcFromRequirementDialogElement();
    this.openImportExportMenuAndSelectOption('add-test-case');
    createTestCaseFromRequirementDialog.getField('format').selectValue(formatValue);
    createTestCaseFromRequirementDialog.clickOnConfirmButton();
  }
  assertCannotCreateObjectInTestCaseWorkspace(item: 'new-test-case' | 'new-folder') {
    const testCaseDialog = new CreateTestCaseDialog();
    const createTestCaseFolderDialog = new CreateTestCaseFolderDialog();
    const createMenu = new MenuElement('create-menu');
    this.clickOnCreateButton();
    createMenu.item(item).assertDisabled();
    createMenu.item(item).click();
    if (item === 'new-test-case') {
      testCaseDialog.assertNotExist();
    }
    if (item === 'new-folder') {
      createTestCaseFolderDialog.assertNotExist();
    }
  }
  openImportExportMenuAndAssertOptionIsDisable(
    itemId:
      | 'import-test-case'
      | 'export-test-case'
      | 'add-test-case'
      | 'automation'
      | 'export-gherkin-script'
      | 'export-bdd-script',
  ) {
    const importExportMenu = new MenuElement('import-export-menu');
    const createTestCaseFromRequirementDialog = new AddTcFromRequirementDialogElement();
    this.clickOnImportExportButton();
    importExportMenu.item(itemId).assertDisabled();
    importExportMenu.item(itemId).click();
    createTestCaseFromRequirementDialog.assertNotExist();
  }
}
