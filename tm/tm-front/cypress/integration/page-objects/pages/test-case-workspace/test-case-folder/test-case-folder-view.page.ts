import { EntityViewPage } from '../../page';
import { FolderInformationPanelElement } from '../../../elements/panels/folder-information-panel.element';
import { TestCaseStatisticPanelElement } from '../panels/test-case-statistic-panel.element';
import { apiBaseUrl } from '../../../../utils/mocks/request-mock';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';
import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';
import { EditableRichTextFieldElement } from '../../../elements/forms/editable-rich-text-field.element';
import { selectByDataTestFieldId } from '../../../../utils/basic-selectors';

export class TestCaseFolderViewPage extends EntityViewPage {
  readonly anchors = AnchorsElement.withLinkIds('information', 'dashboard');
  public readonly nameTextField: EditableTextFieldElement;
  public readonly descriptionField: EditableRichTextFieldElement;

  constructor(private folderId: number | '*') {
    super('sqtm-app-test-case-folder-view');
    this.nameTextField = new EditableTextFieldElement('entity-name');
    this.descriptionField = new EditableRichTextFieldElement(() =>
      cy.get(this.rootSelector).find(selectByDataTestFieldId('description')),
    );
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/test-case-folder-view/${this.folderId}?**`;
    cy.wait(`@${url}`);
  }

  showInformationPanel(): FolderInformationPanelElement {
    this.anchors.clickLink('information');
    return new FolderInformationPanelElement(this.folderId);
  }

  showDashboard() {
    this.anchors.clickLink('dashboard');
    return new TestCaseStatisticPanelElement();
  }
}
