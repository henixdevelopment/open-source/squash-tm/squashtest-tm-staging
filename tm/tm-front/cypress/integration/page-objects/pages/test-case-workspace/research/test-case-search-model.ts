import { UserView } from '../../../../../../projects/sqtm-core/src/lib/model/user/user-view';

export class TestCaseSearchModel {
  usersWhoCreatedTestCases: UserView[];
  usersWhoModifiedTestCases: UserView[];
}
