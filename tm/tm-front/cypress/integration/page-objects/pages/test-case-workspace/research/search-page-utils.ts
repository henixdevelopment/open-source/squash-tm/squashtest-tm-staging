import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { GridElement } from '../../../elements/grid/grid.element';
import {
  basicReferentialData,
  ReferentialDataProviderBuilder,
} from '../../../../utils/referential/referential-data.provider';
import { TestCaseSearchModel } from './test-case-search-model';
import { TestCaseForCoverageSearchPage } from './test-case-for-coverage-search-page';
import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export function buildTestCaseSearchPageMock(
  requirementSearchModel: TestCaseSearchModel = {
    usersWhoCreatedTestCases: [],
    usersWhoModifiedTestCases: [],
  },
) {
  return new HttpMockBuilder<TestCaseSearchModel>('search/test-case')
    .responseBody(requirementSearchModel)
    .build();
}

export function buildTestCaseSearchGrid(initialRows: GridResponse) {
  return GridElement.createGridElement('test-case-search', 'search/test-case', initialRows);
}

export function navigateToTestCaseSearchForCoverage(
  referentialData: ReferentialDataModel = basicReferentialData,
  initialRows: GridResponse = { dataRows: [] },
) {
  const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
  const pageDataMock = buildTestCaseSearchPageMock();
  const grid = buildTestCaseSearchGrid(initialRows);
  // visit page
  cy.get('[data-test-icon-id=search-coverages]').click();
  // wait for ref data request to fire
  referentialDataProvider.wait();
  pageDataMock.wait();
  // wait for initial tree data request to fire
  return new TestCaseForCoverageSearchPage(grid);
}
