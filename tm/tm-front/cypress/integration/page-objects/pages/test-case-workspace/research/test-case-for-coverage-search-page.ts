import { Page } from '../../page';
import {
  basicReferentialData,
  ReferentialDataProviderBuilder,
} from '../../../../utils/referential/referential-data.provider';
import { GridElement } from '../../../elements/grid/grid.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { ToggleIconElement } from '../../../elements/workspace-common/toggle-icon.element';
import { TestCaseSearchModel } from './test-case-search-model';
import { ChangeVerifyingTestCaseOperationReport } from '../../../../../../projects/sqtm-core/src/lib/model/change-coverage-operation-report';
import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class TestCaseForCoverageSearchPage extends Page {
  private linkSelectionButton = new ToggleIconElement('link-selection-button');
  private linkAllButton = new ToggleIconElement('link-all-button');
  private cancelButton = new ToggleIconElement('cancel-button');

  constructor(public grid: GridElement) {
    super('sqtm-app-test-case-for-coverage-search-page');
  }

  public static initTestAtPage(
    requirementVersionId: string,
    referentialData: ReferentialDataModel = basicReferentialData,
    initialRows: GridResponse = { dataRows: [] },
    requirementSearchModel: TestCaseSearchModel = {
      usersWhoCreatedTestCases: [],
      usersWhoModifiedTestCases: [],
    },
    queryString: string = '',
  ): TestCaseForCoverageSearchPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const pageDataMock = new HttpMockBuilder<TestCaseSearchModel>('search/test-case')
      .responseBody(requirementSearchModel)
      .build();
    const grid = GridElement.createGridElement('test-case-search', 'search/test-case', initialRows);
    // visit page
    cy.visit(`search/test-case/coverage/${requirementVersionId}?${queryString}`);
    // wait for ref data request to fire
    referentialDataProvider.wait();
    pageDataMock.wait();
    // wait for initial tree data request to fire
    return new TestCaseForCoverageSearchPage(grid);
  }

  selectRowsWithMatchingCellContent(cellId: string, contents: string[]) {
    this.grid.selectRowsWithMatchingCellContent(cellId, contents);
  }

  selectRowWithMatchingCellContent(cellId: string, content: string) {
    this.grid.selectRowWithMatchingCellContent(cellId, content);
  }

  foldFilterPanel() {
    this.findByElementId('fold-filter-panel-button').click();
  }

  assertLinkSelectionButtonExist() {
    this.linkSelectionButton.assertExists();
  }

  assertLinkSelectionButtonIsActive() {
    this.linkSelectionButton.assertIsActive();
  }

  assertLinkSelectionButtonIsNotActive() {
    this.linkSelectionButton.assertIsNotActive();
  }

  assertLinkAllButtonExist() {
    this.linkAllButton.assertExists();
  }

  assertLinkAllButtonIsActive() {
    this.linkAllButton.assertIsActive();
  }

  assertNavigateBackButtonExist() {
    this.cancelButton.assertExists();
  }

  assertNavigateBackButtonIsActive() {
    this.cancelButton.assertIsActive();
  }

  linkSelection(
    requirementVersionId = '*',
    response: ChangeVerifyingTestCaseOperationReport = {
      verifyingTestCases: [],
      summary: {
        alreadyVerifiedRejections: null,
        noVerifiableVersionRejections: null,
        notLinkableRejections: null,
      },
      requirementStats: null,
      nbIssues: 0,
    },
  ) {
    const mock = this.buildLinkRequestMock(requirementVersionId, response);
    this.linkSelectionButton.click();
    mock.wait();
  }

  linkAll(
    testCaseId = '*',
    response: ChangeVerifyingTestCaseOperationReport = {
      verifyingTestCases: [],
      summary: {
        alreadyVerifiedRejections: null,
        noVerifiableVersionRejections: null,
        notLinkableRejections: null,
      },
      requirementStats: null,
      nbIssues: 0,
    },
  ) {
    const mock = this.buildLinkRequestMock(testCaseId, response);
    this.linkAllButton.click();
    mock.wait();
  }

  private buildLinkRequestMock(
    testCaseId: string,
    response: ChangeVerifyingTestCaseOperationReport,
  ) {
    return new HttpMockBuilder(`/requirement-version/${testCaseId}/verifying-test-cases`)
      .post()
      .responseBody(response)
      .build();
  }
}
