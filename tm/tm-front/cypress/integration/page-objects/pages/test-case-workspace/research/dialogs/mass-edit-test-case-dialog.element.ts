import { selectByDataTestElementId } from '../../../../../utils/basic-selectors';
import { AbstractMassEditRequirementDialog } from '../../../requirement-workspace/search/dialogs/mass-edit-requirement.dialog';

export class MassEditTestCaseDialog extends AbstractMassEditRequirementDialog {
  readonly treeRefreshUrl: string = 'search/test-case';
  readonly parentSelector: string = 'sqtm-app-test-case-multi-edit-dialog';
  readonly httpMockUrl: string = 'search/test-case/mass-update';

  assertAutomatableFieldsAreDisabled() {
    const uneditableFields = ['automatable', 'automatedTestTechnology', 'scmRepository'];
    uneditableFields.forEach((field) => {
      cy.get(this.getOptionalField(field).buildSelector())
        .find(selectByDataTestElementId('activate-checkbox'))
        .click()
        .find('input')
        .should('not.be.checked');
    });
  }
}
