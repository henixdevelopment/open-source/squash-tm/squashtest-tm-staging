import {
  basicReferentialData,
  ReferentialDataProviderBuilder,
} from '../../../../utils/referential/referential-data.provider';
import { GridElement } from '../../../elements/grid/grid.element';
import { MassEditTestCaseDialog } from './dialogs/mass-edit-test-case-dialog.element';
import { NoLineWritingRightsDialogElement } from './dialogs/no-line-writing-rights-dialog.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { TestCaseSearchModel } from './test-case-search-model';
import {
  selectByDataTestElementId,
  selectByDataTestToolbarButtonId,
} from '../../../../utils/basic-selectors';

import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { GroupedMultiListElement } from '../../../elements/filters/grouped-multi-list.element';
import { AbstractSearchPage } from '../../abstract-search-page';
import { NoPermissionToBindMilestoneDialog } from '../../requirement-workspace/dialogs/NoPermissionToBindMilestoneDialog';
import { ToolbarElement } from '../../../elements/workspace-common/toolbar.element';

export class TestCaseSearchPage extends AbstractSearchPage {
  readonly treeRefreshUrl: string = 'search/test-case';
  readonly searchMilestoneUrl: string = 'search/milestones/test-case/*';
  constructor(public grid: GridElement) {
    super('sqtm-app-test-case-simple-search-page');
  }

  public static initTestAtPage(
    referentialData: ReferentialDataModel = basicReferentialData,
    initialNodes: GridResponse = { dataRows: [] },
    testCaseResearchModel: TestCaseSearchModel = {
      usersWhoCreatedTestCases: [],
      usersWhoModifiedTestCases: [],
    },
    queryString: string = '',
  ): TestCaseSearchPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const pageDataMock = new HttpMockBuilder<TestCaseSearchModel>('search/test-case')
      .responseBody(testCaseResearchModel)
      .build();
    const grid = GridElement.createGridElement(
      'test-case-search',
      'search/test-case',
      initialNodes,
    );
    // visit page
    cy.visit(`search/test-case?${queryString}`);
    // wait for ref data request to fire
    referentialDataProvider.wait();
    pageDataMock.wait();
    // wait for initial tree data request to fire
    return new TestCaseSearchPage(grid);
  }

  showMassEditDialog(): MassEditTestCaseDialog {
    const editButton = cy.get(
      `${this.buildToolBarSelector()} ${selectByDataTestToolbarButtonId('mass-edit-button')}`,
    );
    editButton.should('exist');
    editButton.click();
    return new MassEditTestCaseDialog();
  }

  getNoLineWritingRightsDialog(): NoLineWritingRightsDialogElement {
    return new NoLineWritingRightsDialogElement();
  }

  buildToolBarSelector(): string {
    return '[data-test-toolbar-id=test-case-research-toolbar]';
  }

  selectCriteriaByLabel(
    label: string,
    regEx?: boolean,
    secondLabel?: string,
    gridResponse?: GridResponse,
  ) {
    this.grid.declareRefreshData(gridResponse);

    if (regEx) {
      const newLabel: RegExp = new RegExp(label);
      cy.get('sqtm-core-select-filters div[data-test-element-id="item"]')
        .contains('span', newLabel)
        .as('element');
      cy.get('@element')
        .siblings('sqtm-core-simple-checkbox')
        .find('span.ant-checkbox')
        .click()
        .then(() => {
          this.grid.waitForRefresh();
        });
    } else if (secondLabel) {
      const newLabel: RegExp = new RegExp(secondLabel);
      const multiListCriteria = new GroupedMultiListElement();
      cy.get('sqtm-core-select-filters div[data-test-element-id="item"]')
        .contains('span', label)
        .siblings('sqtm-core-simple-checkbox')
        .find('span.ant-checkbox')
        .click();
      cy.get(selectByDataTestElementId('ungrouped-items')).contains('span', newLabel).as('element');
      cy.get('@element').siblings('sqtm-core-simple-checkbox').find('span.ant-checkbox').click();
      multiListCriteria.assertItemIsSelected(secondLabel);
    } else {
      cy.get('sqtm-core-select-filters div[data-test-element-id="item"]')
        .contains('span', label)
        .as('element');

      cy.get('@element')
        .siblings('sqtm-core-simple-checkbox')
        .find('span.ant-checkbox')
        .click()
        .then(() => {
          this.grid.waitForRefresh();
        });
    }
  }

  selectRowsWithMatchingCellContent(cellId: string, contents: string[]) {
    this.grid.selectRowsWithMatchingCellContent(cellId, contents);
  }

  selectRowWithMatchingCellContent(cellId: string, content: string) {
    this.grid.selectRowWithMatchingCellContent(cellId, content);
  }

  clickOnPerimeter() {
    cy.get(selectByDataTestElementId('grid-scope-field')).click();
  }
  getNoPermissionToBindMilestoneDialog(): NoPermissionToBindMilestoneDialog {
    const toolbarElement = new ToolbarElement('test-case-research-toolbar');
    toolbarElement.shouldExist();
    toolbarElement.button('edit-milestones-button').clickWithoutSpan();
    return new NoPermissionToBindMilestoneDialog();
  }
  getNoPermissionToEditMassDialog(): NoPermissionToBindMilestoneDialog {
    const toolbarElement = new ToolbarElement('test-case-research-toolbar');
    toolbarElement.shouldExist();
    toolbarElement.button('mass-edit-button').clickWithoutSpan();
    return new NoLineWritingRightsDialogElement();
  }
  assertCellIsNotSelectable(contentCell: string, cellId: string) {
    this.grid.findRowId('name', contentCell).then((idTestCase) => {
      this.grid.getCell(idTestCase, cellId).treeNodeRenderer().assertIsNotSelectable();
    });
  }
}
