import { Page } from '../../page';
import { GridElement } from '../../../elements/grid/grid.element';
import { ToolbarElement } from '../../../elements/workspace-common/toolbar.element';

export class TestCaseForCampaignSearchPage extends Page {
  readonly toolBarMenu = new ToolbarElement('requirement-search-toolbar');
  readonly linkSelectionButton = this.toolBarMenu.button('link-selection-button');

  constructor(public grid: GridElement) {
    super('sqtm-app-test-case-for-campaign-search-page');
  }
}
