import { EntityViewPage } from '../../page';
import { ReferentialDataProviderBuilder } from '../../../../utils/referential/referential-data.provider';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { NavBarElement } from '../../../elements/nav-bar/nav-bar.element';
import {
  mockActionStepExecViewModel,
  mockModifDuringExecModel,
} from '../../../../data-mock/modif-during-exec.data-mock';
import {
  ActionStepExecViewModel,
  ModifDuringExecModel,
} from '../../../../../../projects/sqtm-core/src/lib/model/modif-during-exec/modif-during-exec.model';
import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';

export class TestStepModifyDuringExecPage extends EntityViewPage {
  constructor() {
    super('sqtm-app-modif-during-exec-step-view');
  }

  public checkDataFetched() {
    throw Error('unsupported operation exception');
  }

  assertTestCaseNameContains(expectedName: string) {
    this.findByElementId('test-case-name').should('contain.text', expectedName);
  }

  assertCalledTestCaseNameContains(expectedName: string) {
    this.findByElementId('called-test-case-name').should('contain.text', expectedName);
  }

  assertCalledTestCaseNameNotVisible() {
    this.findByElementId('called-test-case-name').should('not.exist');
  }

  public static initTestAtPage(
    modifDuringExecModel: Partial<ModifDuringExecModel>,
    actionStepExecViewModel: Partial<ActionStepExecViewModel>,
    executionId = 1,
    executionStepId = 1,
    referentialData?: ReferentialDataModel,
  ): TestStepModifyDuringExecPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const modifDuringExecModelMockUrl = `execution/${executionId}/modification-during-execution`;
    const actionStepExecViewModelMockUrl = `execution/${executionId}/modification-during-execution/action-step/${executionStepId}`;
    const pageUrl = `modif-during-exec/execution/${executionId}/step/${executionStepId}`;
    const modifDuringExecModelMock = new HttpMockBuilder(modifDuringExecModelMockUrl)
      .responseBody(mockModifDuringExecModel(modifDuringExecModel))
      .build();

    const actionStepExecViewModelMock = new HttpMockBuilder(actionStepExecViewModelMockUrl)
      .responseBody(mockActionStepExecViewModel(actionStepExecViewModel))
      .build();

    cy.visit(pageUrl);
    referentialDataProvider.wait();
    modifDuringExecModelMock.wait();
    actionStepExecViewModelMock.wait();
    new NavBarElement().toggle();
    return new TestStepModifyDuringExecPage();
  }
}
