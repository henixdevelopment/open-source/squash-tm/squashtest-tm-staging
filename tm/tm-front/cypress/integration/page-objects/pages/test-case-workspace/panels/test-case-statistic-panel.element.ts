import { ChartElement } from '../../../elements/charts/chart.element';
import { selectByDataTestElementId } from '../../../../utils/basic-selectors';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { TestCaseStatistics } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/test-case-statistics.model';

export class TestCaseStatisticPanelElement {
  private statPanelTag = `sqtm-app-test-case-statistic-panel`;

  private customDashboardTag = `sqtm-app-custom-dashboard`;

  public coverageChart = new ChartElement('coverage-test-case');
  public statusChart = new ChartElement('status-test-case');
  public importanceChart = new ChartElement('importance-test-case');
  public sizeChart = new ChartElement('size-test-case');

  constructor() {}

  private getFavoriteButton() {
    return cy.get(selectByDataTestElementId('favorite-button'));
  }

  private getDashboardPanel() {
    return cy.get('.ant-collapse-content-box');
  }

  private getDefaultButton() {
    return cy.get(selectByDataTestElementId('default-button'));
  }

  private getDashboardBindingElement(bindingNumber: number) {
    return cy.get(selectByDataTestElementId(`custom-dashboard-binding-${bindingNumber}`));
  }

  private getResizableGrid() {
    return cy.get('.gridster-item-resizable-handler');
  }

  private getDragLayer() {
    return cy.get('.draglayer');
  }

  private getLegends() {
    return cy.get('.legendtext');
  }

  private getRefreshButton() {
    return cy.get(selectByDataTestElementId('refresh-button'));
  }

  assertTitleExist(expectedTitle: string) {
    cy.get('.ant-collapse-header').should('contain.text', expectedTitle);
  }

  refreshStatistics(stats?: TestCaseStatistics) {
    const selector = `.ant-collapse-header ${selectByDataTestElementId('refresh-button')}`;
    const mock = new HttpMockBuilder('test-case/statistics').post().responseBody(stats).build();
    cy.get(selector).click();
    mock.wait();
  }

  assertFooterContains(expected: string) {
    const selector = `${this.statPanelTag} ${selectByDataTestElementId('footer')}`;
    cy.get(selector).should('contain.text', expected);
  }

  assertCustomDashboardExist() {
    cy.get(this.customDashboardTag).should('exist');
  }

  assertCustomDashboardNotExist() {
    cy.get(this.customDashboardTag).should('not.exist');
  }

  clickFavoriteButton() {
    this.getFavoriteButton().click();
  }

  clickDefaultButton() {
    this.getDefaultButton().click();
  }

  clickRefresh() {
    this.getRefreshButton().click();
  }

  assertFavoriteButtonExist() {
    this.getFavoriteButton().should('exist');
  }

  assertFavoriteButtonNotExist() {
    this.getFavoriteButton().should('not.exist');
  }

  assertNoDashboardSelectedMessage() {
    this.getDashboardPanel()
      .find('span')
      .should(
        'contain.text',
        "Vous pouvez sélectionner un tableau de bord à afficher depuis l'espace Pilotage, en cliquant sur le bouton [Favori] d'un tableau de bord.",
      );
  }

  assertRefreshDashboardMessage() {
    this.getDashboardPanel()
      .find('span')
      .should(
        'contain.text',
        'Cliquer sur le bouton [Rafraîchir] pour générer le tableau de bord...',
      );
  }

  assertFavoriteDashboardIsEmptyMessage() {
    this.getDashboardPanel()
      .find('span')
      .should(
        'contain.text',
        "Votre tableau de bord favori est vide. Vous pouvez le remplir dans l'espace Pilotage.",
      );
  }

  assertDefaultButtonExist() {
    this.getDefaultButton().should('exist');
  }

  asserChartDashboardBinding(bindingNumbers: number[]) {
    bindingNumbers.forEach((bindingNumber) => {
      this.getDashboardBindingElement(bindingNumber).find('sqtm-app-custom-chart').should('exist');
    });
  }

  asserReportDashboardBinding(bindingNumbers: number[]) {
    bindingNumbers.forEach((bindingNumber) => {
      this.getDashboardBindingElement(bindingNumber)
        .find('sqtm-app-custom-report-report')
        .should('exist');
    });
  }

  assertCannotResizableElement() {
    this.getResizableGrid().should('not.exist');
  }

  assertCannotMoveElement(index: number) {
    this.getDragLayer().eq(index).find('cursor-crosshair').should('not.exist');
  }

  assertChartLegends(index: number, contentText: string) {
    this.getLegends().eq(index).should('contain.text', contentText);
  }
}
