import { EntityViewPage } from '../page';
import { ReferentialDataProviderBuilder } from '../../../utils/referential/referential-data.provider';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { EditableRichTextFieldElement } from '../../elements/forms/editable-rich-text-field.element';
import { GridElement } from '../../elements/grid/grid.element';
import { ExecutionScenarioPanelElement } from './panels/execution-scenario-panel.element';
import {
  selectByDataTestButtonId,
  selectByDataTestToolbarButtonId,
} from '../../../utils/basic-selectors';
import { ExecutionHistoryPanelElement } from './panels/execution-history-panel.element';
import { ExecutionModel } from '../../../../../projects/sqtm-core/src/lib/model/execution/execution.model';
import { InputType } from '../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { ReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { AnchorsElement } from '../../elements/anchor/anchors.element';
import { SessionNoteContainerElement } from '../../elements/execution-page/session-note-container.element';
import { ExploratoryExecutionDurationPanelElement } from '../../elements/execution-page/exploratory-execution-duration-panel.element';
import { fillCuf } from '../../../scenarios/scenario-parts/custom-field.part';
import { BugtrackerDialogElement } from '../../elements/dialog/bugtracker-dialog.element';
import { ToolbarButtonElement } from '../../elements/workspace-common/toolbar.element';
import { MenuElement } from '../../../utils/menu.element';
import { RemoteIssueSearchDialogElement } from '../../elements/dialog/remote-issue-search-dialog.element';
import { RemoteIssueDialogElements } from '../../elements/dialog/remote-issue-dialog.elements';
import { AutomatedExecutionFailureDetailsElement } from '../../elements/automated-execution-failure-details/automated-execution-failure-details.element';
import { FailureDetail } from '../../../../../projects/sqtm-core/src/lib/model/execution/failure-detail.model';

export class ExecutionPage extends EntityViewPage {
  readonly commentField = new EditableRichTextFieldElement(
    'execution-comment',
    'execution/*/comment',
  );
  readonly automatedExecutionResultSummaryField = new EditableRichTextFieldElement(
    'automated-execution-result-summary',
  );
  readonly coverageGrid = new GridElement('execution-view-coverages');
  readonly failureDetailPanel = new AutomatedExecutionFailureDetailsElement();
  readonly issueGrid = new GridElement('execution-view-issues');
  readonly historyGrid = new GridElement('execution-page-history');
  readonly scenarioPanel = new ExecutionScenarioPanelElement();
  readonly historyPanel = new ExecutionHistoryPanelElement();
  readonly durationPanel = new ExploratoryExecutionDurationPanelElement();
  readonly addIssueToolBarButton = new ToolbarButtonElement(this.rootSelector, 'add-issue');
  readonly removeIssueToolBarButton = new ToolbarButtonElement(this.rootSelector, 'remove-issues');
  readonly anchors = AnchorsElement.withLinkIds(
    'information',
    'history',
    'scenario',
    'charter',
    'failure-details',
    'issues',
    'comments',
    'notes',
  );

  constructor() {
    super('sqtm-app-execution-page');
  }

  public static initTestAtPage(
    executionId: number,
    executionModel?: ExecutionModel,
    issuesGridResponse?: GridResponse,
    historyGridResponse?: GridResponse,
    referentialData?: ReferentialDataModel,
    failureDetailList?: FailureDetail[],
  ): ExecutionPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const executionModelMock = new HttpMockBuilder<ExecutionModel>(`execution/${executionId}?*`)
      .responseBody(executionModel)
      .build();
    const getIssuePanelMock = new HttpMockBuilder('issues/execution/*?frontEndErrorIsHandled=true')
      .responseBody(getAuthenticatedIssuePanelResponse())
      .build();

    const getKnownIssuesMock = new HttpMockBuilder('issues/execution/*/known-issues')
      .post()
      .responseBody(issuesGridResponse)
      .build();

    const getFailureDetailsList = new HttpMockBuilder('execution-extender/*/failure-detail-list')
      .get()
      .responseBody(failureDetailList)
      .build();

    if (historyGridResponse) {
      new HttpMockBuilder('iteration/*/test-plan/*/executions')
        .post()
        .responseBody(historyGridResponse)
        .build();
    }

    cy.visit(`execution/${executionId}`);

    referentialDataProvider.wait();
    executionModelMock.wait();
    if (issuesGridResponse) {
      getIssuePanelMock.wait();
      getKnownIssuesMock.wait();
    }
    if (failureDetailList) {
      failureDetailList.forEach(() => {
        new HttpMockBuilder(`issues/failure-detail/*/known-issues`)
          .post()
          .responseBody({
            count: 0,
            idAttribute: null,
            dataRows: [],
            activeColumnIds: null,
            page: 0,
          })
          .build();
      });
      getFailureDetailsList.wait();
    }

    return new ExecutionPage();
  }

  checkExecutionMode(executionMode: string) {
    cy.get('[data-test-element-id="execution-mode"]').should('contain.text', executionMode);
  }

  checkExecutionStatus(executionStatus: string) {
    cy.get('[data-test-element-id="execution-status"]').should('contain.text', executionStatus);
  }

  checkDuration(executionDuration: string) {
    cy.get('[data-test-element-id="execution-duration"]').should('contain.text', executionDuration);
  }

  checkJobUrl(jobUrl: string) {
    cy.get('[data-test-element-id="job-url"]').should('contain.text', jobUrl);
  }

  checkJobResult(jobResult: string) {
    cy.get('[data-test-element-id="job-result"]').should('contain.text', jobResult);
  }

  checkEnvironmentVariables(evNamedValue: string, evId: number) {
    cy.get('[data-test-element-id="denormalized-environment-variable-' + evId + '"]').should(
      'contain.text',
      evNamedValue,
    );
  }

  checkEnvironmentTags(environmentTagsString: string) {
    cy.get('[data-test-element-id="denormalized-environment-tags"]').should(
      'contain.text',
      environmentTagsString,
    );
  }

  assertJobUrlCapsuleNotExists() {
    cy.get('[data-test-element-id="job-url"]').should('not.exist');
  }

  assertDurationCapsuleNotExists() {
    cy.get('[data-test-element-id="execution-duration"]').should('not.exist');
  }

  assertJobResultCapsuleNotExists() {
    cy.get('[data-test-element-id="job-result"]').should('not.exist');
  }

  checkTcId(testCaseId: string) {
    cy.get('[data-test-link-id="test-case-id"]').should('contain.text', testCaseId);
  }

  checkName(executionName: string) {
    cy.get('[data-test-element-id="execution-title"]').should('contain.text', executionName);
  }

  checkStatus(expectedStatus: string) {
    cy.get('sqtm-app-denormalized-status').should('contain.text', expectedStatus);
  }

  checkImportance(expectedImportance: string) {
    cy.get('sqtm-app-denormalized-importance').should('contain.text', expectedImportance);
  }

  checkNature(expectedNature: string) {
    cy.get('span[data-test-element-id="execution-nature"]').should('contain.text', expectedNature);
  }

  checkNatureIcon(iconName: string): void {
    cy.get('span[data-test-element-id="execution-nature"]')
      .siblings('.sqtm-core-infolist-icon')
      .should('have.class', `anticon-sqtm-core-infolist-item:${iconName}`);
  }

  checkType(expectedType: string) {
    cy.get('span[data-test-element-id="execution-type"]').should('contain.text', expectedType);
  }

  checkTypeIcon(iconName: string): void {
    cy.get('span[data-test-element-id="execution-type"]')
      .siblings('.sqtm-core-infolist-icon')
      .should('have.class', `anticon-sqtm-core-infolist-item:${iconName}`);
  }

  checkDataSet(expectedDataSet: string) {
    cy.get('span[data-test-element-id="execution-dataset"]').should(
      'contain.text',
      expectedDataSet,
    );
  }

  checkTcDescription(expectedDescription: string) {
    cy.get('div[data-test-element-id="execution-tc-description"]').should(
      'have.html',
      expectedDescription,
    );
  }

  checkDenormalizedCustomField(
    index: number,
    expectedLabel: string,
    expectedValue: any,
    inputType?: InputType,
  ) {
    cy.get(`label[data-test-element-id="execution-dnz-label-${index}"]`).should(
      'contain.text',
      expectedLabel,
    );
    if (!inputType) {
      cy.get(
        `sqtm-app-denormalized-custom-field[data-test-element-id="execution-dnz-value-${index}"]`,
      ).should('contain.text', expectedValue);
    } else if (inputType === InputType.CHECKBOX) {
      this.checkDenormalizedCheckBoxCustomField(index, expectedValue);
    } else if (inputType === InputType.RICH_TEXT) {
      cy.get(
        `sqtm-app-denormalized-custom-field[data-test-element-id="execution-dnz-value-${index}"]`,
      ).should('contain.html', expectedValue);
    } else if (inputType === InputType.TAG) {
      this.checkDenormalizedTagCustomField(index, expectedValue);
    }
  }

  private checkDenormalizedCheckBoxCustomField(index: number, expectedValue: boolean) {
    const selector = `
    sqtm-app-denormalized-custom-field[data-test-element-id="execution-dnz-value-${index}"]
    .ant-checkbox
    `;
    if (expectedValue) {
      cy.get(selector).should('have.class', 'ant-checkbox-checked');
    } else {
      cy.get(selector).should('not.have.class', 'ant-checkbox-checked');
    }
  }

  private checkDenormalizedTagCustomField(cufIndex: number, expectedValues: string[]) {
    const selector = `
    sqtm-app-denormalized-custom-field[data-test-element-id="execution-dnz-value-${cufIndex}"]
    nz-tag
    `;
    expectedValues.forEach((expectedValue, index) => {
      cy.get(selector).then((elements) => {
        cy.wrap(elements.eq(index)).should('contain.text', expectedValue);
      });
    });
  }

  unbindOneIssue(remoteIssueId: string, issuesGridResponse: GridResponse) {
    this.issueGrid.findRowIdNoWithLink('remoteId', remoteIssueId).then((id) => {
      this.issueGrid.getRow(id, 'rightViewport').cell('delete').iconRenderer().click();

      const deleteMock = new HttpMockBuilder('issues/*').delete().build();

      const issueCountMock = new HttpMockBuilder('execution/*/issue-count')
        .get()
        .responseBody({ issueCount: issuesGridResponse.count })
        .build();

      const getKnownIssuesMock = new HttpMockBuilder('issues/execution/*/known-issues')
        .post()
        .responseBody(issuesGridResponse)
        .build();

      this.clickConfirmDeleteButton();

      deleteMock.wait();
      issueCountMock.wait();
      getKnownIssuesMock.wait();
    });
  }

  unbindMultipleIssues(remoteIssueIds: string[], issuesGridResponse: GridResponse) {
    this.issueGrid.selectRowsWithMatchingCellContent('remoteId', remoteIssueIds);

    const deleteMock = new HttpMockBuilder('issues/*').delete().build();

    const issueCountMock = new HttpMockBuilder('execution/*/issue-count')
      .get()
      .responseBody({ issueCount: issuesGridResponse.count })
      .build();

    const getKnownIssuesMock = new HttpMockBuilder('issues/execution/*/known-issues')
      .post()
      .responseBody(issuesGridResponse)
      .build();

    this.clickOnDeleteButton();
    this.clickConfirmDeleteButton();

    deleteMock.wait();
    issueCountMock.wait();
    getKnownIssuesMock.wait();
  }

  deleteExecution(rowId: number, newHistoryGrid: GridResponse) {
    const row = this.historyGrid.getRow(rowId);
    row.cell('delete').iconRenderer().click();

    const deleteMock = new HttpMockBuilder('iteration/*/test-plan/execution/*').delete().build();

    const getHistoryMock = new HttpMockBuilder('iteration/*/test-plan/*/executions')
      .post()
      .responseBody(newHistoryGrid)
      .build();

    this.clickConfirmDeleteButton();
    deleteMock.wait();
    getHistoryMock.wait();
  }

  deleteMultipleExecutions(rowIds: number[], newHistoryGrid: GridResponse) {
    rowIds.map((id) => {
      const row = this.historyGrid.getRow(id);
      row.cell('#').indexRenderer().clickWithShift();
    });

    const deleteMock = new HttpMockBuilder('iteration/*/test-plan/execution/*').delete().build();

    const getHistoryMock = new HttpMockBuilder('iteration/*/test-plan/*/executions')
      .post()
      .responseBody(newHistoryGrid)
      .build();

    this.clickOnMultipleDeleteButton();
    this.clickConfirmDeleteButton();
    deleteMock.wait();
    getHistoryMock.wait();
  }

  private clickOnDeleteButton() {
    cy.get(selectByDataTestToolbarButtonId('remove-issues')).should('exist').click();
  }

  private clickOnMultipleDeleteButton() {
    cy.get('sqtm-app-execution-page-history')
      .find('[data-test-button-id="mass-delete-button"]')
      .click();
  }

  private clickConfirmDeleteButton() {
    cy.get('sqtm-core-confirm-delete-dialog')
      .find('[data-test-dialog-button-id="confirm"]')
      .click()
      // Then
      .get('sqtm-core-confirm-delete-dialog')
      .should('not.exist');
  }

  private getBackButton() {
    return this.find(selectByDataTestButtonId('back'));
  }

  clickInformationAnchorLink(): this {
    this.anchors.clickLink('information');
    return this;
  }

  clickHistoryAnchorLink(): ExecutionHistoryPanelElement {
    this.anchors.clickLink('history');
    return this.historyPanel;
  }

  clickScenarioAnchorLink(knowIssues?: GridResponse): ExecutionScenarioPanelElement {
    const getBugTrackerAuthenticatedMock = new HttpMockBuilder(
      'issues/execution/*?frontEndErrorIsHandled=true',
    )
      .responseBody(getAuthenticatedIssuePanelResponse())
      .build();

    const getKnownIssuesMock = new HttpMockBuilder('issues/execution/*/all-known-issues')
      .post()
      .responseBody(knowIssues)
      .build();

    this.anchors.clickLink('scenario');

    if (knowIssues) {
      getBugTrackerAuthenticatedMock.wait();
      getKnownIssuesMock.wait();
    }

    return this.scenarioPanel;
  }

  assertFailureDetailSectionDoesNotExist() {
    this.anchors.assertAnchorCountNotExists('failure-details');
  }

  clickFailureDetailsAnchorLink(): AutomatedExecutionFailureDetailsElement {
    this.anchors.clickLink('failure-details');
    return this.failureDetailPanel;
  }

  clickIssueAnchorLink() {
    this.anchors.clickLink('issues');
  }

  clickCommentAnchorLink() {
    this.anchors.clickLink('comments');
  }

  clickCharterAnchorLink() {
    this.anchors.clickLink('charter');
  }

  checkCollapsedCharterText(expectedText: string): void {
    this.findByElementId('collapsed-charter-value').should('contain.text', expectedText);
  }

  checkExtendedCharterText(expectedText: string): void {
    this.findByElementId('extended-charter-value').should('contain.text', expectedText);
  }

  // Index is 1-based to reflect the visual interface
  findNoteContainer(index: number): SessionNoteContainerElement {
    return new SessionNoteContainerElement(() => this.findByElementId('note-container-' + index));
  }

  clickOnCollapseAllNotes(): void {
    this.findByElementId('collapse-all-notes').click();
  }

  extendCharter() {
    this.findByElementId('toggle-charter').click();
  }

  checkTaskDivision(checkMozillaFirefoxCompatibility: string) {
    this.findByElementId('task-division-value').should(
      'contain.text',
      checkMozillaFirefoxCompatibility,
    );
  }

  fillCuf(cufName: string, input: InputType, value: string) {
    fillCuf(cufName, input, value);
  }

  connectToABugTrackerWithToken(token: string) {
    const connectToBugtrackerDialog = new BugtrackerDialogElement();

    this.find(selectByDataTestButtonId('login')).click();
    connectToBugtrackerDialog.fillPasswordField(token);
    connectToBugtrackerDialog.clickOnConnectionButton();
  }

  linkIssueToAnExecution(pathProject: string, idIssue: string) {
    const issueMenu = new MenuElement('issues-menu');
    const remoteIssueSearchDialog = new RemoteIssueSearchDialogElement();

    this.addIssueToolBarButton.click();
    issueMenu.item('attach-issue').click();
    remoteIssueSearchDialog.assertSpinnerIsNotPresent();
    remoteIssueSearchDialog.typeFieldRemoteIssue('projectPath', pathProject);
    remoteIssueSearchDialog.selectSearchByIidOrTitle("ID de l'Issue");
    remoteIssueSearchDialog.typeFieldRemoteIssue('issueIid', idIssue);
    remoteIssueSearchDialog.clickOnSearchButton();
    remoteIssueSearchDialog.clickOnConfirmButton();
  }

  createNewIssueInExecution(title: string) {
    const issueMenu = new MenuElement('issues-menu');
    const remoteIssueDialog = new RemoteIssueDialogElements();

    this.addIssueToolBarButton.click();
    issueMenu.item('create-issue').click();
    remoteIssueDialog.assertSpinnerIsNotPresent();
    remoteIssueDialog.typeFieldRemoteIssue('summary', title);
    remoteIssueDialog.clickOnConfirmButton();
  }

  clickBackButton() {
    this.getBackButton().click();
  }
}

function getAuthenticatedIssuePanelResponse(): any {
  return {
    entityType: 'execution',
    bugTrackerStatus: 'AUTHENTICATED',
    projectName: '["P1"]',
    projectId: 1,
    delete: '',
    oslc: false,
  };
}
