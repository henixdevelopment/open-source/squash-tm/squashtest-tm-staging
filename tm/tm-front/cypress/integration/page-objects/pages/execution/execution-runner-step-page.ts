import { Page } from '../page';
import { ChangeExecutionStatusButton } from '../../elements/execution-runner/change-execution-status-button';
import { EditableRichTextFieldElement } from '../../elements/forms/editable-rich-text-field.element';
import { AttachmentCompactListElement } from '../../elements/attachments/attachment-compact-list.element';
import { StepCounterElement } from '../../elements/execution-runner/step-counter.element';
import { AttachmentUtils } from '../../../utils/attachments/attachment.utils';
import { GridElement } from '../../elements/grid/grid.element';
import {
  ReportIssueDialogElement,
  ReportIssueDialogMocks,
} from '../../elements/issues/report-issue-dialog.element';
import { ToggleIconElement } from '../../elements/workspace-common/toggle-icon.element';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { ExecutionModel } from '../../../../../projects/sqtm-core/src/lib/model/execution/execution.model';
import { UploadSummary } from '../../../../../projects/sqtm-core/src/lib/model/attachment/upload-summary.model';
import { TestPlanResumeModel } from '../../../../../projects/sqtm-core/src/lib/model/execution/test-plan-resume.model';
import { MenuElement, MenuItemElement } from '../../../utils/menu.element';
import { RemoteIssueSearchDialogElement } from '../../elements/dialog/remote-issue-search-dialog.element';
import { ToolbarButtonElement } from '../../elements/workspace-common/toolbar.element';
import { selectByDataIcon, selectByDataTestButtonId } from '../../../utils/basic-selectors';
import { BugtrackerDialogElement } from '../../elements/dialog/bugtracker-dialog.element';
import { RemoteIssueDialogElements } from '../../elements/dialog/remote-issue-dialog.elements';
import { BaseDialogElement } from '../../elements/dialog/base-dialog.element';

export class ExecutionRunnerStepPage extends Page {
  public successButton: ChangeExecutionStatusButton;
  public failureButton: ChangeExecutionStatusButton;
  public blockedButton: ChangeExecutionStatusButton;
  public untestableButton: ChangeExecutionStatusButton;
  public readyButton: ChangeExecutionStatusButton;
  public settledButton: ChangeExecutionStatusButton;
  private editableCommentField: EditableRichTextFieldElement;
  public attachmentCompactList: AttachmentCompactListElement;
  private stepCounter: StepCounterElement;
  public readonly issuesPanelGrid: GridElement;
  private addIssueToolBarButton = new ToolbarButtonElement(this.rootSelector, 'create-button');
  private fastForwardButton = new ToggleIconElement('fast-forward');

  constructor(executionStepId?: number) {
    super('sqtm-app-execution-runner-step');
    this.successButton = new ChangeExecutionStatusButton('SUCCESS');
    this.failureButton = new ChangeExecutionStatusButton('FAILURE');
    this.blockedButton = new ChangeExecutionStatusButton('BLOCKED');
    this.untestableButton = new ChangeExecutionStatusButton('UNTESTABLE');
    this.readyButton = new ChangeExecutionStatusButton('READY');
    this.settledButton = new ChangeExecutionStatusButton('SETTLED');
    this.stepCounter = new StepCounterElement();
    const commentUrl = `execution-step/${executionStepId || '*'}/comment`;
    this.editableCommentField = new EditableRichTextFieldElement('comment', commentUrl);
    this.attachmentCompactList = new AttachmentCompactListElement();
    this.issuesPanelGrid = GridElement.createGridElement('execution-step-issues');
  }

  checkExecutionButtons() {
    this.successButton.assertExists();
    this.failureButton.assertExists();
    this.blockedButton.assertExists();
  }

  checkDocumentTitle(expectedTitle: string) {
    cy.title().should('include', expectedTitle);
  }

  checkExecutionStepper(currentStep: number, maxStep: number) {
    this.stepCounter.checkExecutionStepper(currentStep, maxStep);
    // backward button is always active
    this.stepCounter.assertBackwardButtonIsActive();
    if (currentStep < maxStep) {
      this.stepCounter.assertForwardButtonIsActive();
    } else {
      this.stepCounter.assertForwardButtonIsDisabled();
    }
  }

  checkStepExecutionStatus(expected: string) {
    cy.get(
      `sqtm-app-execution-runner-toolbar
      .execution-status`,
    )
      .should('have.length', 1)
      .should('contain.text', expected);
  }

  navigateForward(): ExecutionRunnerStepPage {
    this.stepCounter.navigateForward();
    return new ExecutionRunnerStepPage();
  }

  navigateBackward(): ExecutionRunnerStepPage {
    this.stepCounter.navigateBackward();
    return new ExecutionRunnerStepPage();
  }

  navigateToArbitraryStep(stepIndex: number): ExecutionRunnerStepPage {
    this.stepCounter.navigateToArbitraryStep(stepIndex);
    if (stepIndex > 0) {
      return new ExecutionRunnerStepPage();
    }
  }

  checkAction(expectedAction: string) {
    this.checkPanelTitle('action-panel', 'Action');
    this.checkPanelRichContent('action-panel', expectedAction);
  }

  checkExpectedResult(expectedResult: string) {
    this.checkPanelTitle('result-panel', 'Résultat attendu');
    this.checkPanelRichContent('result-panel', expectedResult);
  }

  checkComment(comment: string) {
    this.checkPanelTitle('comment-panel', 'Commentaires');
    this.checkCommentRichContent(comment);
  }

  checkModificationDuringExecutionIsVisible() {
    this.findByElementId('modification-during-exec').should('be.visible');
  }

  addAttachments(files: File[], attachmentListId?: number, uploadSummary?: UploadSummary[]) {
    AttachmentUtils.addAttachments(
      'sqtm-app-execution-runner-step>div',
      files,
      attachmentListId,
      uploadSummary,
    );
  }

  private checkPanelRichContent(panelId: string, expectedContent: string) {
    const panelSelector = this.getPanelSelector(panelId);
    cy.get(
      `
    ${panelSelector}
    .collapse-content
    `,
    ).should('contain.html', expectedContent);
  }

  private checkPanelTitle(panelId: string, expectedTitle: string) {
    const panelSelector = this.getPanelSelector(panelId);
    cy.get(
      `
    ${panelSelector}
    .collapse-title
    `,
    ).should('contain.text', expectedTitle);
    return panelSelector;
  }

  private getPanelSelector(panelId: string) {
    return `sqtm-core-compact-collapse-panel[data-test-element-id="${panelId}"]`;
  }

  updateComment(newComment: string) {
    this.editableCommentField.setAndConfirmValue(newComment);
  }

  private checkCommentRichContent(comment: string) {
    this.editableCommentField.checkHtmlContent(comment);
  }

  openReportIssueDialog(mocks: ReportIssueDialogMocks): ReportIssueDialogElement {
    const dialog = new ReportIssueDialogElement(mocks);
    const issueMenu = new MenuElement('issues-menu');
    this.addIssueToolBarButton.clickWithoutSpan();
    issueMenu.item('create-issue').click();
    dialog.waitForInitialRequests();
    return dialog;
  }

  openAttachIssueDialog(mocks: ReportIssueDialogMocks): ReportIssueDialogElement {
    const dialog = new ReportIssueDialogElement(mocks);
    const issueMenu = new MenuElement('issues-menu');
    this.addIssueToolBarButton.clickWithoutSpan();
    issueMenu.item('attach-issue').click();
    dialog.waitForInitialRequests();
    return dialog;
  }

  assertFastForwardButtonIsActive() {
    this.fastForwardButton.assertExists();
    this.fastForwardButton.assertIsActive();
  }

  public fastForward(
    iterationId = '*',
    testPlanId = '*',
    nextExecutionId = '*',
    testPlanResume?: TestPlanResumeModel,
    nextExecutionModel?: ExecutionModel,
  ) {
    const resumeMock = new HttpMockBuilder(
      `iteration/${iterationId}/test-plan/${testPlanId}/next-execution`,
    )
      .post()
      .responseBody(testPlanResume)
      .build();
    const executionMock = new HttpMockBuilder(`execution-runner/${nextExecutionId}`)
      .responseBody(nextExecutionModel)
      .build();
    this.fastForwardButton.click();
    resumeMock.wait();
    executionMock.wait();
  }

  assertFastForwardButtonIsInactive() {
    this.fastForwardButton.assertIsNotActive();
  }

  linkIssueToAnExecution(idIssue: string) {
    const issueMenu = new MenuElement('issues-menu');
    const remoteIssueSearchDialog = new RemoteIssueSearchDialogElement();

    this.addIssueToolBarButton.clickWithoutSpan();
    issueMenu.item('attach-issue').click();
    remoteIssueSearchDialog.selectSearchByIidOrTitle("ID de l'Issue");
    remoteIssueSearchDialog.typeFieldRemoteIssue('issueIid', idIssue);
    remoteIssueSearchDialog.clickOnSearchButton();
    remoteIssueSearchDialog.clickOnConfirmButton();
  }

  private clickOnButtonLogin() {
    return cy.get(this.rootSelector).find(selectByDataTestButtonId('login')).click();
  }

  connectToABugTrackerWithToken(token: string) {
    const connectToBugtrackerDialog = new BugtrackerDialogElement();
    this.clickOnButtonLogin();
    connectToBugtrackerDialog.fillPasswordField(token);
    connectToBugtrackerDialog.clickOnConnectionButton();
  }

  createNewIssueInExecution(title: string) {
    const toolbarButtonElement = new ToolbarButtonElement('', 'create-button');
    const menuItemElement = new MenuItemElement('', 'create-issue');
    const remoteIssueDialog = new RemoteIssueDialogElements();
    toolbarButtonElement.clickWithoutSpan();
    menuItemElement.click();
    remoteIssueDialog.typeFieldRemoteIssue('summary', title);
    remoteIssueDialog.clickOnConfirmButton();
  }

  private menuStatus() {
    cy.get(this.rootSelector).find(selectByDataIcon('ellipsis')).click();
  }

  setExecutionStatus(
    statusStepExecution: 'SUCCESS' | 'FAILURE' | 'BLOCKED' | 'READY' | 'UNTESTABLE' | 'SETTLED',
    executionStepId: number,
    executionId: number,
  ) {
    const confirmDialog = new BaseDialogElement('end-of-execution');
    const executionRunner = new ExecutionRunnerStepPage();
    switch (statusStepExecution) {
      case 'SUCCESS':
        executionRunner.successButton.changeStatus(executionStepId, executionId);
        break;
      case 'FAILURE':
        executionRunner.failureButton.changeStatus(executionStepId, executionId);
        break;
      case 'BLOCKED':
        executionRunner.blockedButton.changeStatus(executionStepId, executionId);
        break;
      case 'READY':
        this.menuStatus();
        executionRunner.readyButton.changeStatus(executionStepId, executionId);
        break;
      case 'UNTESTABLE':
        this.menuStatus();
        executionRunner.untestableButton.changeStatus(executionStepId, executionId);
        break;
      case 'SETTLED':
        this.menuStatus();
        executionRunner.settledButton.changeStatus(executionStepId, executionId);
        break;
      default:
        throw new Error(`Unexpected status: ${statusStepExecution}`);
    }
    confirmDialog.clickOnConfirmButton();
  }
}
