import { Page } from '../../page';
import { GridElement } from '../../../elements/grid/grid.element';
import { MassDeleteExecutionDialogElement } from '../dialog/mass-delete-execution-dialog.element';
import { selectByDataTestButtonId } from '../../../../utils/basic-selectors';

export class ExecutionHistoryPanelElement extends Page {
  readonly grid = new GridElement('execution-page-history');

  constructor() {
    super('sqtm-app-execution-page-history');
  }

  assertPanelTitleContainsText(expectedTitle: string): void {
    this.findByElementId('panel-title').should('contain.text', expectedTitle);
  }

  deleteExecutionWithTopIcon(executionNumber: string) {
    const deleteExecutionDialog = new MassDeleteExecutionDialogElement();

    this.grid.findRowId('#', executionNumber).then((idExecution) => {
      this.grid.selectRow(idExecution, '#');
      cy.get(this.rootSelector).find(selectByDataTestButtonId('mass-delete-button')).click();
      deleteExecutionDialog.clickOnConfirmButton();
    });
  }

  deleteExecutionWithEndOfLineIcon(executionNumber: string) {
    const deleteExecutionDialog = new MassDeleteExecutionDialogElement();

    this.grid.findRowId('#', executionNumber).then((idExecution) => {
      this.grid.getCell(idExecution, 'delete').findIconInCell('sqtm-core-generic:delete').click();
      deleteExecutionDialog.clickOnConfirmButton();
    });
  }

  assertCannotDeleteExecutionWithTopIcon(executionNumber: string) {
    const deleteExecutionDialog = new MassDeleteExecutionDialogElement();

    this.grid.findRowId('#', executionNumber).then((idExecution) => {
      this.grid.selectRow(idExecution, '#');
      cy.get(this.rootSelector)
        .find(selectByDataTestButtonId('mass-delete-button'))
        .find('span')
        .should('have.class', 'label-color');
      cy.get(this.rootSelector).find(selectByDataTestButtonId('mass-delete-button')).click();
      deleteExecutionDialog.assertNotExist();
    });
  }

  assertEndOfLineDeleteButtonNotVisible(executionNumber: string) {
    this.grid.findRowId('#', executionNumber).then((idExecution) => {
      this.grid.getCell(idExecution, 'delete').find('div').should('not.be.visible');
    });
  }
}
