import { Page } from '../../page';
import { selectByDataTestElementId } from '../../../../utils/basic-selectors';
import { ExecutionStepElement } from '../../../elements/execution-page/execution-step-element';

export class ExecutionScenarioPanelElement extends Page {
  constructor() {
    super('sqtm-app-execution-page-scenario-panel');
  }

  collapseAll() {
    cy.get(
      `
      ${this.rootSelector}
       ${selectByDataTestElementId('collapse-all-steps')}
    `,
    ).click();
  }

  expendAll() {
    cy.get(
      `
      ${this.rootSelector}
       ${selectByDataTestElementId('expend-all-steps')}
    `,
    ).click();
  }

  getExecutionStepById(stepId: string) {
    return new ExecutionStepElement(stepId, null);
  }

  getExecutionStepByIndex(index: number) {
    return new ExecutionStepElement(null, index);
  }

  checkPrerequisiteHeaderLabel(expectedLabel: string) {
    cy.get('[data-test-field-id="prerequisite-header"]').should('contain.text', expectedLabel);
  }

  checkPrerequisiteIsExtended() {
    this.getPrerequisiteToggleArrow().should('have.class', 'anticon-caret-down');
  }

  checkPrerequisiteIsCollapsed() {
    this.getPrerequisiteToggleArrow().should('have.class', 'anticon-caret-right');
  }

  assertPrerequisiteFieldNotExists() {
    this.getPrerequisiteToggleArrow().should('not.exist');
  }

  private getPrerequisiteToggleArrow() {
    return cy.get(`
    ${this.rootSelector}
    .prerequisite
    .anticon
    `);
  }
}
