import { BaseDialogElement } from '../../../elements/dialog/base-dialog.element';

export class EndOfExecutionDialogElement extends BaseDialogElement {
  constructor() {
    super('end-of-execution');
  }
}
