import { DeleteConfirmDialogElement } from '../../../elements/dialog/delete-confirm-dialog.element';

export class MassDeleteExecutionDialogElement extends DeleteConfirmDialogElement {
  constructor() {
    super('confirm-delete');
  }

  deleteForFailure(_response: any) {
    throw Error('Use specific dialog for delete success/failure');
  }

  deleteForSuccess(_response: any) {
    throw Error('Use specific dialog for delete success/failure');
  }
}
