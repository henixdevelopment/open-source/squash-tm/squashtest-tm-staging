import { Page } from '../page';
import { HttpMock, HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { GridElement } from '../../elements/grid/grid.element';
import { UserAccount } from '../../../../../projects/sqtm-core/src/lib/model/user/user-account.model';
import { UserAccountApiTokenPanelElement } from './panels/user-account-api-token-panel.element';
import { UserAccountBugtrackerConfigurationPanelElement } from './panels/user-account-bugtracker-configuration-panel.element';
import {
  selectByDataTestButtonId,
  selectByDataTestElementId,
  selectByDataTestFieldId,
} from '../../../utils/basic-selectors';
import { UserAccountInfoPanelElement } from './panels/user-account-info-panel.element';
import { AnchorsElement } from '../../elements/anchor/anchors.element';
import { GenerateApiTokenDialogElement } from './dialogs/generate-api-token-dialog.element';
import { DisplayTokenDialogElement } from './dialogs/display-token-dialog.element';
import { AlertDialogElement } from '../../elements/dialog/alert-dialog.element';
import Chainable = Cypress.Chainable;

export class UserAccountPage extends Page {
  readonly permissionsGrid: GridElement;

  private initialMock: HttpMock<UserAccount>;

  readonly apiTokenPanelElement = new UserAccountApiTokenPanelElement();
  readonly bugtrackerConfigurationPanelElement =
    new UserAccountBugtrackerConfigurationPanelElement();
  readonly infoPanelElement = new UserAccountInfoPanelElement();
  readonly generateApiTokenDialogElement = new GenerateApiTokenDialogElement();
  readonly displayTokenDialogElement = new DisplayTokenDialogElement();

  readonly anchors = AnchorsElement.withLinkIds(
    'information',
    'permissions',
    'bugtrackers',
    'api-token',
  );

  constructor() {
    super('sqtm-app-user-account-page');

    this.permissionsGrid = GridElement.createGridElement('project-permissions');
  }

  declareInitialData(userAccount: UserAccount): void {
    this.initialMock = new HttpMockBuilder<UserAccount>('user-account')
      .responseBody(userAccount)
      .build();
  }

  checkDataFetched(): void {
    this.initialMock.wait();
  }

  assertName(expected: string): void {
    this.getField(this.infoPanelElement, 'name').should('contain.text', expected);
  }

  assertEmail(expected: string): void {
    this.getField(this.infoPanelElement, 'email').should('contain.text', expected);
  }

  assertGroup(expected: string): void {
    this.getField(this.infoPanelElement, 'group').should('contain.text', expected);
  }

  assertBugTrackerLogin(expected: string): void {
    this.getField(this.bugtrackerConfigurationPanelElement, 'credentials-username')
      .find('input')
      .should('have.value', expected);
  }

  assertBugTrackerRevokeButtonVisible(): void {
    this.bugtrackerConfigurationPanelElement
      .find(selectByDataTestElementId('revoke-credentials-button'))
      .should('be.visible');
  }

  assertApiTokenPanelExists(): void {
    this.apiTokenPanelElement.assertExists();
  }

  private getField(panelElement: Page, fieldName: string): Chainable {
    return panelElement.find(selectByDataTestFieldId(fieldName));
  }

  selectEditedBugTracker(bugTrackerName: string): void {
    this.getField(this.bugtrackerConfigurationPanelElement, 'edited-bugtracker')
      .click()
      .get('.ant-select-item-option-content')
      .contains(bugTrackerName)
      .click();
  }

  openGenerateApiTokenDialog(): void {
    this.find(selectByDataTestButtonId('add-api-token')).click();
  }

  assertErrorDialogIfNoJwtSecret() {
    const alertDialog = new AlertDialogElement();
    this.openGenerateApiTokenDialog();
    alertDialog.assertExists();
  }

  clickAddApiToken() {
    this.openGenerateApiTokenDialog();
  }

  fillAddApiTokenForm(name: string, permissionId: number, expiryDate: string) {
    this.generateApiTokenDialogElement.fillName(name);
    this.generateApiTokenDialogElement.selectPermissions(permissionId);
    this.generateApiTokenDialogElement.fillExpiryDate(expiryDate);
  }
}
