import { Page } from '../../page';

export class UserAccountBugtrackerConfigurationPanelElement extends Page {
  constructor() {
    super('sqtm-app-user-account-bugtrackers-configuration-panel');
  }
}
