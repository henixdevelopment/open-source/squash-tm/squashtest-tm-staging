import { Page } from '../../page';
import { GridElement } from '../../../elements/grid/grid.element';
import { HttpMock, HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { mockDataRow, mockGridResponse } from '../../../../data-mock/grid.data-mock';
import { BaseDialogElement } from '../../../elements/dialog/base-dialog.element';

export class UserAccountApiTokenPanelElement extends Page {
  readonly personalApiTokenGrid: GridElement;

  private personalTokenListMock: HttpMock<GridResponse>;

  constructor() {
    super('sqtm-app-user-account-api-token-panel');

    this.personalApiTokenGrid = GridElement.createGridElement('api-token-grid');

    this.personalTokenListMock = new HttpMockBuilder<GridResponse>(
      'user-account/personal-api-tokens',
    )
      .post()
      .responseBody(
        mockGridResponse('tokenId', [
          mockDataRow({
            id: 1,
            data: {
              createdOn: '2023-07-02T08:01:06.420+00:00',
              expiryDate: '2024-07-02',
              lastUsage: null,
              name: 'token pour lire',
              permissions: 'READ',
              tokenId: 1,
            },
          }),
          mockDataRow({
            id: 2,
            data: {
              createdOn: '2024-07-02T08:01:06.420+00:00',
              expiryDate: '2025-07-02',
              lastUsage: '2024-07-03T12:22:06.000+00:00',
              name: 'token pour lire et écrire',
              permissions: 'READ_WRITE',
              tokenId: 2,
            },
          }),
        ]),
      )
      .build();
  }

  checkDataFetched(): void {
    this.personalTokenListMock.wait();
  }

  deleteApiToken(tokenId: number) {
    const confirmDeleteDialog = new BaseDialogElement('confirm-delete');
    this.personalApiTokenGrid.getRow(tokenId).cell('delete').iconRenderer().click();
    confirmDeleteDialog.assertExists();

    const mock = new HttpMockBuilder('api-token/*').delete().build();
    confirmDeleteDialog.confirm();
    mock.wait();
  }
}
