import { Page } from '../../page';

export class UserAccountInfoPanelElement extends Page {
  constructor() {
    super('sqtm-app-user-account-information-panel');
  }
}
