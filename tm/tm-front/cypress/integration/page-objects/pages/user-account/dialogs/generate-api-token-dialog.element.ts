import { CreationDialogElement } from '../../../elements/dialog/creation-dialog.element';
import {
  selectByDataTestButtonId,
  selectByDataTestDialogButtonId,
  selectByDataTestFieldId,
} from '../../../../utils/basic-selectors';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';

export class GenerateApiTokenDialogElement extends CreationDialogElement<any> {
  constructor() {
    super('create-api-token-dialog');
  }

  addWithOptions(): Cypress.Chainable<any> {
    //  NOOP
    return;
  }

  protected getNewEntityUrl(): string {
    //  NOOP
    return;
  }

  fillName(name: string) {
    cy.get(selectByDataTestFieldId('token-name')).click().type(name);
  }

  selectPermissions(permissionId: number) {
    cy.get(selectByDataTestButtonId('radio-button-' + permissionId)).click();
  }

  fillExpiryDate(expiryDate: string) {
    cy.get(selectByDataTestFieldId('token-expiry-date')).find('input').clear().type(expiryDate);
  }

  addToken() {
    const mock = new HttpMockBuilder('api-token/generate-api-token')
      .post()
      .responseBody({
        token: 'YWJjZGVmZ2g=',
      })
      .build();
    cy.get(selectByDataTestDialogButtonId('add')).click();
    mock.wait();
  }

  confirm() {
    this.clickOnAddButton();
  }
}
