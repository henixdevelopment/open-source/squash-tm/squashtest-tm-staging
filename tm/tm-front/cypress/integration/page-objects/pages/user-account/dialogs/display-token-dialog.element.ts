import { BaseDialogElement } from '../../../elements/dialog/base-dialog.element';
import { SecretFieldElement } from '../../../elements/forms/secret-field.element';

export class DisplayTokenDialogElement extends BaseDialogElement {
  readonly secretField = new SecretFieldElement();

  constructor() {
    super('display-new-api-token-dialog');
  }

  showToken(expectedTokenValue: string) {
    this.secretField.assertSecretIsNotVisible();
    this.secretField.showSecretField();
    this.secretField.assertSecretIsVisible(expectedTokenValue);
  }

  assertCanCopyToken() {
    this.secretField.copySecretValue();
  }

  copyTokenValue(): Cypress.Chainable<string> {
    this.secretField.showSecretField();
    return this.secretField.getSecretValueForCopy();
  }
}
