import { Page } from '../../page';
import { ToggleIconElement } from '../../../elements/workspace-common/toggle-icon.element';
import { GridElement } from '../../../elements/grid/grid.element';
import {
  basicReferentialData,
  ReferentialDataProviderBuilder,
} from '../../../../utils/referential/referential-data.provider';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { ChangeItemTestPlanIdsReport } from '../../../../../../projects/sqtm-core/src/lib/model/change-coverage-operation-report';
import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { RequirementSearchModel } from '../../requirement-workspace/search/requirement-search-model';

export class TestCaseThroughRequirementSearchPage extends Page {
  private linkSelectionButton = new ToggleIconElement('link-selection-button');
  private linkAllButton = new ToggleIconElement('link-all-button');
  private cancelButton = new ToggleIconElement('cancel-button');

  constructor(
    public grid: GridElement,
    public entityName: CampaignWorkspaceEntityName,
  ) {
    super(TestCaseThroughRequirementSearchPage.getRootSelector(entityName));
  }

  private static getRootSelector(entityName: CampaignWorkspaceEntityName) {
    switch (entityName) {
      case 'test-suite':
        return 'sqtm-app-test-case-by-req-for-ts-search-page';
      case 'campaign':
        return 'sqtm-app-test-case-by-req-for-campaign-workspace-entities-search-page';
      default:
        return 'sqtm-app-test-case-by-requirement-search-page';
    }
  }

  public static initTestAtPage(
    entityName: CampaignWorkspaceEntityName,
    entityId: string,
    referentialData: ReferentialDataModel = basicReferentialData,
    initialRows: GridResponse = { dataRows: [] },
    requirementSearchModel: RequirementSearchModel = {
      usersWhoCreatedRequirements: [],
      usersWhoModifiedRequirements: [],
    },
    queryString: string = '',
  ): TestCaseThroughRequirementSearchPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const pageDataMock = new HttpMockBuilder<RequirementSearchModel>('search/requirement')
      .responseBody(requirementSearchModel)
      .build();
    const grid = GridElement.createGridElement(
      'tc-by-requirement',
      'search/test-case/by-requirement',
      initialRows,
    );

    cy.visit(`search/test-case/${entityName}/${entityId}/coverage?${queryString}`);

    referentialDataProvider.wait();
    pageDataMock.wait();

    return new TestCaseThroughRequirementSearchPage(grid, entityName);
  }

  selectRowsWithMatchingCellContent(cellId: string, contents: string[]) {
    this.grid.selectRowsWithMatchingCellContent(cellId, contents);
  }

  selectRowWithMatchingCellContent(cellId: string, content: string) {
    this.grid.selectRowWithMatchingCellContent(cellId, content);
  }

  foldFilterPanel() {
    this.findByElementId('fold-filter-panel-button').click();
  }

  assertLinkSelectionButtonExist() {
    this.linkSelectionButton.assertExists();
  }

  assertLinkSelectionButtonIsActive() {
    this.linkSelectionButton.assertIsActive();
  }

  assertLinkSelectionButtonIsNotActive() {
    this.linkSelectionButton.assertIsNotActive();
  }

  assertLinkAllButtonExist() {
    this.linkAllButton.assertExists();
  }

  assertLinkAllButtonIsActive() {
    this.linkAllButton.assertIsActive();
  }

  assertNavigateBackButtonExist() {
    this.cancelButton.assertExists();
  }

  assertNavigateBackButtonIsActive() {
    this.cancelButton.assertIsActive();
  }

  linkSelection(
    entityId = '*',
    response: ChangeItemTestPlanIdsReport = {
      itemTestPlanIds: [],
      summary: {
        noVerifiableVersionRejections: null,
        notLinkableRejections: null,
        alreadyVerifiedRejections: null,
      },
    },
  ) {
    const mock = this.buildLinkRequestMock(this.entityName, entityId, response);
    this.linkSelectionButton.click();
    mock.wait();
  }

  linkAll(
    entityId = '*',
    response: ChangeItemTestPlanIdsReport = {
      itemTestPlanIds: [],
      summary: {
        noVerifiableVersionRejections: null,
        notLinkableRejections: null,
        alreadyVerifiedRejections: null,
      },
    },
  ) {
    const mock = this.buildLinkRequestMock(this.entityName, entityId, response);
    this.linkAllButton.click();
    mock.wait();
  }

  private buildLinkRequestMock(
    entityName: CampaignWorkspaceEntityName,
    entityId: string,
    response: ChangeItemTestPlanIdsReport,
  ) {
    return new HttpMockBuilder(`/${entityName}/${entityId}/test-plan-items`)
      .post()
      .responseBody(response)
      .build();
  }
}

export type CampaignWorkspaceEntityName = 'campaign' | 'iteration' | 'test-suite';
