import { UserView } from '../../../../../../projects/sqtm-core/src/lib/model/user/user-view';

export class ItpiSearchModel {
  usersAssignedTo: UserView[];
  usersExecutedItpi: UserView[];
}
