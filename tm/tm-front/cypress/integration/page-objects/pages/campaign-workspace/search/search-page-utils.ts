import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { GridElement } from '../../../elements/grid/grid.element';
import { ItpiSearchModel } from './itpi-search-model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export function buildItpiSearchPageMock(
  itpiSearchModel: ItpiSearchModel = { usersExecutedItpi: [], usersAssignedTo: [] },
) {
  return new HttpMockBuilder<ItpiSearchModel>('search/campaign')
    .responseBody(itpiSearchModel)
    .build();
}

export function buildItpiSearchGrid(initialRows: GridResponse) {
  return GridElement.createGridElement('itpi-search', 'search/campaign', initialRows);
}
