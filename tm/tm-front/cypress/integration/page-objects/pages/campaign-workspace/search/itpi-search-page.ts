import {
  basicReferentialData,
  ReferentialDataProviderBuilder,
} from '../../../../utils/referential/referential-data.provider';
import { GridElement } from '../../../elements/grid/grid.element';
import { buildItpiSearchGrid, buildItpiSearchPageMock } from './search-page-utils';
import { ItpiSearchModel } from './itpi-search-model';
import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { selectByDataTestElementId } from '../../../../utils/basic-selectors';
import { GroupedMultiListElement } from '../../../elements/filters/grouped-multi-list.element';
import { ToolbarButtonElement } from '../../../elements/workspace-common/toolbar.element';
import { AddTestPlanDialogElement } from '../dialogs/add-test-plan-dialog.element';
import { ExecutionPage } from '../../execution/execution-page';
import { AbstractSearchPage } from '../../abstract-search-page';
import { MultiEditSearchDialog } from '../dialogs/multi-edit-search.dialog';

export class ItpiSearchPage extends AbstractSearchPage {
  readonly treeRefreshUrl: string = 'search/campaign';
  readonly searchMilestoneUrl: string = null;
  readonly massEditButton = new ToolbarButtonElement(this.rootSelector, 'mass-edit-button');
  readonly addButton = new ToolbarButtonElement(this.rootSelector, 'add-button');

  constructor(public grid: GridElement) {
    super('sqtm-app-itpi-search-page');
  }

  public static initTestAtPage(
    referentialData: ReferentialDataModel = basicReferentialData,
    initialRows: GridResponse = { dataRows: [] },
    itpiSearchModel: ItpiSearchModel = {
      usersExecutedItpi: [],
      usersAssignedTo: [],
    },
    queryString: string = '',
  ): ItpiSearchPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const pageDataMock = buildItpiSearchPageMock(itpiSearchModel);
    const grid = buildItpiSearchGrid(initialRows);
    // visit page
    cy.visit(`search/campaign?${queryString}`);
    // wait for ref data request to fire
    referentialDataProvider.wait();
    pageDataMock.wait();
    // wait for initial tree data request to fire
    return new ItpiSearchPage(grid);
  }

  openPerimeterWidget() {
    cy.get(this.rootSelector).find(selectByDataTestElementId('grid-scope-field')).click();
  }

  selectProjectInWidgetPerimeter(projectName: string) {
    const multilistProject = new GroupedMultiListElement();

    this.openPerimeterWidget();
    multilistProject.toggleOneItem(projectName);
    multilistProject.close();
  }

  assertProjectIsSelectedInWidgetPerimeter(projectName: string) {
    const multilistProject = new GroupedMultiListElement();

    this.openPerimeterWidget();
    multilistProject.assertItemIsSelected(projectName);
    multilistProject.close();
  }
  buildToolBarSelector(): string {
    return '[data-test-toolbar-id=campaign-search-toolbar]';
  }

  clickAddButton() {
    this.addButton.click();
    return new AddTestPlanDialogElement();
  }

  clickMassEditButton() {
    this.massEditButton.clickWithoutSpan();
    return new MultiEditSearchDialog();
  }

  openExecutionPageOfAnItpi(nameItem: string): ExecutionPage {
    this.assertExists();
    this.grid.assertSpinnerIsNotPresent();
    this.grid.findRowId('label', nameItem).then((idItem) => {
      this.grid.getCell(idItem, 'execution', 'rightViewport').findIconInCell('play-circle').click();
    });
    cy.get('div').contains('Nouvelle exécution').click();
    return new ExecutionPage();
  }

  assertExecutionStatusColorByLabel(label: string, color: string) {
    this.grid.findRowId('label', label).then((idRow) => {
      this.grid
        .getCell(idRow, 'executionStatus')
        .findCell()
        .find('sqtm-core-execution-status-cell')
        .find('div')
        .find('div')
        .should('have.css', 'background-color', color);
    });
  }
}
