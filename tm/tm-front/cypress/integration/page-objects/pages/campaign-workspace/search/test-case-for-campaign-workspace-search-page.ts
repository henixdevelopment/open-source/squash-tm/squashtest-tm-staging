import { Page } from '../../page';
import { ToggleIconElement } from '../../../elements/workspace-common/toggle-icon.element';
import { GridElement } from '../../../elements/grid/grid.element';
import { CampaignWorkspaceEntityName } from './test-case-through-requirement-search.page';
import {
  basicReferentialData,
  ReferentialDataProviderBuilder,
} from '../../../../utils/referential/referential-data.provider';
import { TestCaseSearchModel } from '../../test-case-workspace/research/test-case-search-model';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { ChangeItemTestPlanIdsReport } from '../../../../../../projects/sqtm-core/src/lib/model/change-coverage-operation-report';
import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class TestCaseForCampaignWorkspaceSearchPage extends Page {
  private linkSelectionButton = new ToggleIconElement('link-selection-button');
  private linkAllButton = new ToggleIconElement('link-all-button');
  private cancelButton = new ToggleIconElement('cancel-button');

  constructor(
    public grid: GridElement,
    public entityName: CampaignWorkspaceEntityName,
  ) {
    super(TestCaseForCampaignWorkspaceSearchPage.getRootSelector(entityName));
  }

  private static getRootSelector(entityName: CampaignWorkspaceEntityName) {
    switch (entityName) {
      case 'campaign':
        return 'sqtm-app-test-case-for-campaign-search-page';
      case 'test-suite':
        return 'sqtm-app-test-case-for-test-suite-search-page';
      default:
        return 'sqtm-app-test-case-by-requirement-search-page';
    }
  }

  public static initTestAtPage(
    entityName: CampaignWorkspaceEntityName,
    entityId: string,
    referentialData: ReferentialDataModel = basicReferentialData,
    initialRows: GridResponse = { dataRows: [] },
    testCaseSearchModel: TestCaseSearchModel = {
      usersWhoCreatedTestCases: [],
      usersWhoModifiedTestCases: [],
    },
  ): TestCaseForCampaignWorkspaceSearchPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const pageDataMock = new HttpMockBuilder<TestCaseSearchModel>('search/test-case')
      .responseBody(testCaseSearchModel)
      .build();
    const grid = GridElement.createGridElement('test-case-search', 'search/test-case', initialRows);

    cy.visit(`search/test-case/${entityName}/${entityId}`);
    referentialDataProvider.wait();
    pageDataMock.wait();

    return new TestCaseForCampaignWorkspaceSearchPage(grid, entityName);
  }

  selectRowsWithMatchingCellContent(cellId: string, contents: string[]) {
    this.grid.selectRowsWithMatchingCellContent(cellId, contents);
  }

  selectRowWithMatchingCellContent(cellId: string, content: string) {
    this.grid.selectRowWithMatchingCellContent(cellId, content);
  }

  foldFilterPanel() {
    this.findByElementId('fold-filter-panel-button').click();
  }

  assertLinkSelectionButtonExist() {
    this.linkSelectionButton.assertExists();
  }

  assertLinkSelectionButtonIsActive() {
    this.linkSelectionButton.assertIsActive();
  }

  assertLinkSelectionButtonIsNotActive() {
    this.linkSelectionButton.assertIsNotActive();
  }

  assertLinkAllButtonExist() {
    this.linkAllButton.assertExists();
  }

  assertLinkAllButtonIsActive() {
    this.linkAllButton.assertIsActive();
  }

  assertNavigateBackButtonExist() {
    this.cancelButton.assertExists();
  }

  assertNavigateBackButtonIsActive() {
    this.cancelButton.assertIsActive();
  }

  linkSelection(
    entityId = '*',
    response: ChangeItemTestPlanIdsReport = {
      itemTestPlanIds: [],
      summary: {
        alreadyVerifiedRejections: null,
        notLinkableRejections: null,
        noVerifiableVersionRejections: null,
      },
    },
  ) {
    const mock = this.buildLinkRequestMock(this.entityName, entityId, response);
    this.linkSelectionButton.click();
    mock.wait();
  }

  linkAll(
    entityId = '*',
    response: ChangeItemTestPlanIdsReport = {
      itemTestPlanIds: [],
      summary: {
        alreadyVerifiedRejections: null,
        notLinkableRejections: null,
        noVerifiableVersionRejections: null,
      },
    },
  ) {
    const mock = this.buildLinkRequestMock(this.entityName, entityId, response);
    this.linkAllButton.click();
    mock.wait();
  }

  private buildLinkRequestMock(
    entityName: CampaignWorkspaceEntityName,
    entityId: string,
    response: ChangeItemTestPlanIdsReport,
  ) {
    return new HttpMockBuilder(`/${entityName}/${entityId}/test-plan-items`)
      .post()
      .responseBody(response)
      .build();
  }
}
