import { EntityViewPage } from '../../page';
import { apiBaseUrl } from '../../../../utils/mocks/request-mock';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';
import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';
import { EditableRichTextFieldElement } from '../../../elements/forms/editable-rich-text-field.element';
import { CampaignWorkspaceDashboardPage } from '../campaign-workspace-dashboard.page';

export class CampaignFolderViewPage extends EntityViewPage {
  readonly anchors = AnchorsElement.withLinkIds('issues', 'dashboard');
  public readonly nameTextField: EditableTextFieldElement = new EditableTextFieldElement(
    'entity-name',
  );
  public readonly descriptionRichField: EditableRichTextFieldElement =
    new EditableRichTextFieldElement('description');

  private getFavoriteButton() {
    return this.findByElementId('favorite-button');
  }

  private getDefaultButton() {
    return this.findByElementId('default-button');
  }

  private getTitleDashboard() {
    return this.find('.ant-collapse-header');
  }

  private getDashboardPanel() {
    return this.find('.ant-collapse-content-box');
  }

  constructor(private campaignFolderId?: number | '*') {
    super('sqtm-app-campaign-folder-view');
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/campaign-folder-view/${this.campaignFolderId}?**`;
    cy.wait(`@${url}`);
  }

  clickDashboardAnchorLink() {
    this.anchors.clickLink('dashboard');
  }

  assertFavoriteButtonExist() {
    this.getFavoriteButton().should('exist');
  }

  assertDefaultButtonExist() {
    this.getDefaultButton().should('exist');
  }

  clickFavoriteButton() {
    this.getFavoriteButton().click();
    return new CampaignWorkspaceDashboardPage('sqtm-app-custom-dashboard');
  }

  assertTitleDashboardExist(expectedTitle: string) {
    this.getTitleDashboard().should('contain.text', expectedTitle);
  }

  assertNoDashboardSelectedMessage() {
    this.getDashboardPanel()
      .find('span')
      .should(
        'contain.text',
        "Vous pouvez sélectionner un tableau de bord à afficher depuis l'espace Pilotage, en cliquant sur le bouton [Favori] d'un tableau de bord.",
      );
  }
}
