import Chainable = Cypress.Chainable;
import { TextFieldElement } from '../../../elements/forms/TextFieldElement';
import { RichTextFieldElement } from '../../../elements/forms/RichTextFieldElement';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { DataRowModel } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';

export class CreateBindTestSuiteDialog {
  public readonly stringSelector: string = '[data-test-dialog-id="new-entity"]';

  public readonly nameField: TextFieldElement;
  public readonly descriptionField: RichTextFieldElement;

  constructor() {
    this.nameField = new TextFieldElement('name');
    this.descriptionField = new RichTextFieldElement('description');
  }

  assertExists() {
    this.selector.should('exist');
  }

  get selector(): Chainable {
    return cy.get(this.stringSelector);
  }

  confirm(addResponse: DataRowModel): void {
    const createMock = new HttpMockBuilder('campaign-tree/new-test-suite')
      .post()
      .responseBody(addResponse)
      .build();
    const bindMock = new HttpMockBuilder('test-suites/test-plan/bind').post().build();
    const refreshMock = new HttpMockBuilder('campaign-tree/refresh').post().build();

    this.clickAddButton();

    createMock.wait();
    bindMock.wait();
    refreshMock.wait();
  }

  clickAddButton(): void {
    cy.get('[data-test-dialog-button-id="add"]').click();
  }
}
