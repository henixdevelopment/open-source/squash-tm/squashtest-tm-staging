import { OptionalSelectField } from '../../../elements/forms/optional-select-field.element';
import {
  selectByDataTestDialogButtonId,
  selectByDataTestDialogId,
} from '../../../../utils/basic-selectors';

export class MultiEditSearchDialog {
  assertNotExist() {
    cy.get(this.buildSelector()).should('not.exist');
  }

  assertExists() {
    cy.get(this.buildSelector()).should('exist');
  }

  getOptionalField(fieldName): OptionalSelectField {
    const optionalField = new OptionalSelectField(
      fieldName,
      'sqtm-app-requirement-multi-edit-search-dialog',
    );
    optionalField.assertExists();
    return optionalField;
  }

  clickButton(buttonId: string) {
    const confirmButton = cy
      .get(this.buildSelector())
      .find(selectByDataTestDialogButtonId(buttonId));
    confirmButton.should('exist');
    confirmButton.click();
  }

  buildSelector(): string {
    return selectByDataTestDialogId('mass-edit');
  }
}
