import { BaseDialogElement } from '../../../elements/dialog/base-dialog.element';
import { GridElement } from '../../../elements/grid/grid.element';
import { GridId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-id';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';

export class SprintReqVersionUpdateTestPlanDialogElement extends BaseDialogElement {
  readonly grid: GridElement;

  constructor() {
    super('sprint-req-version-update-test-plan');

    this.grid = GridElement.createGridElement(GridId.AVAILABLE_TEST_PLAN_ITEMS);
  }

  confirm() {
    const mock = new HttpMockBuilder('sprint-req-version/*/test-plan-items').post().build();
    super.confirm();
    mock.wait();
  }
}
