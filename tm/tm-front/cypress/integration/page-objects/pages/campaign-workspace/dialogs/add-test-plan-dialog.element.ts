import { BaseDialogElement } from '../../../elements/dialog/base-dialog.element';
import { TreeElement } from '../../../elements/grid/grid.element';

export class AddTestPlanDialogElement extends BaseDialogElement {
  public readonly tree = TreeElement.createTreeElement('campaign-tree-picker', 'campaign-tree');

  constructor() {
    super('add-to-test-plan');
  }

  selectIterationToLinkToItpi(nodeNames: string[]) {
    this.tree.selectMultipleNodesByName(nodeNames);
  }

  clickButtonAddIteration() {
    this.clickOnButton('add-iteration');
  }

  clickAddButton() {
    this.clickOnButton('add');
  }
}
