import { BaseDialogElement } from '../../../elements/dialog/base-dialog.element';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { GridElement } from '../../../elements/grid/grid.element';
import {
  selectByDataTestCellId,
  selectByDataTestElementId,
  selectByDataTestLinkId,
} from '../../../../utils/basic-selectors';
import { ExecutionStatus } from '../../../../../../projects/sqtm-core/src/lib/model/level-enums/level-enum';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { AttachmentListModel } from '../../../../../../projects/sqtm-core/src/lib/model/attachment/attachment-list.model';
import { FailureDetail } from '../../../../../../projects/sqtm-core/src/lib/model/execution/failure-detail.model';

export class AutomatedSuiteDetailsDialogElement extends BaseDialogElement {
  constructor(
    dialogId: string,
    public readonly executions: GridElement,
  ) {
    super(dialogId);
  }

  static navigateTo(
    automatedSuiteId: string,
    executions: GridResponse,
  ): AutomatedSuiteDetailsDialogElement {
    automatedSuiteId = automatedSuiteId ?? '*';
    const url = `automated-suite/${automatedSuiteId}/executions`;
    const gridElement = GridElement.createGridElement('automated-suite-execution', url, executions);

    return new AutomatedSuiteDetailsDialogElement('automated-suite-exec-detail', gridElement);
  }

  assertRowHasExecutionStatus(executionId: number, status: string) {
    this.executions
      .getRow(executionId, 'mainViewport')
      .cell('executionStatus')
      .findCell()
      .find(selectByDataTestCellId('execution-status-cell'))
      .should('have.css', 'background-color')
      .then((actualColor) => {
        const expectedColor = ExecutionStatus[status].backgroundColor;
        const normalizeColor = (color) =>
          color.replace(/\s/g, '').replace('rgba', 'rgb').replace(/,1\)$/, ')');
        expect(normalizeColor(actualColor)).to.equal(normalizeColor(expectedColor));
      });
  }

  displayExecutionAttachmentsAndFailureDetail(
    executionId: number,
    extenderId: number = null,
    attachmentBody: AttachmentListModel,
    failureDetailsBody: FailureDetail[] = null,
  ) {
    const attachmentUrl = `execution/${executionId}/attachments`;
    new HttpMockBuilder(attachmentUrl).get().responseBody(attachmentBody).build();

    if (extenderId && failureDetailsBody) {
      const failureDetailListUrl = `execution-extender/${extenderId}/failure-detail-list`;
      new HttpMockBuilder(failureDetailListUrl).get().responseBody(failureDetailsBody).build();
    }

    this.executions.getRow(executionId).cell('open').iconRenderer().click();
  }

  assertAttachmentsListIsDisplayed() {
    cy.get(selectByDataTestElementId('attachment-label')).should('contain.text', 'Pièces jointes');
  }

  assertNoAttachmentLabelIsDisplayed() {
    cy.get(selectByDataTestElementId('execution-attachments')).should(
      'contain.text',
      'Aucun résultat',
    );
  }

  assertAttachmentIsDisplayed(attachmentName: string) {
    cy.get(selectByDataTestLinkId('attachment-name')).should('contain.text', attachmentName);
  }
}
