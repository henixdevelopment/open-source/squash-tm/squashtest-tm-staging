import { DeleteConfirmDialogElement } from '../../../elements/dialog/delete-confirm-dialog.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class MassDeleteCtpiDialogElement extends DeleteConfirmDialogElement {
  campaignId: number | '*';
  ctpiIds: number[];

  constructor(campaignId: number | '*', ctpiIds: number[]) {
    super('confirm-delete');
    this.campaignId = campaignId;
    this.ctpiIds = ctpiIds;
  }

  override deleteForFailure(_response: any) {}

  override deleteForSuccess(response?: GridResponse) {
    const removeMock = new HttpMockBuilder<any>(
      `campaign/${this.campaignId}/test-plan/${[this.ctpiIds]}`,
    )
      .delete()
      .responseBody('')
      .build();
    const mock = new HttpMockBuilder<GridResponse>(`campaign/${this.campaignId}/test-plan`)
      .post()
      .responseBody(response)
      .build();
    this.clickOnConfirmButton();
    removeMock.wait();
    return mock.waitResponseBody();
  }
}
