import { BaseDialogElement } from '../../../elements/dialog/base-dialog.element';
import { GridElement } from '../../../elements/grid/grid.element';

export class AutomatedSuiteExecutionComparisonDialog extends BaseDialogElement {
  public gridElement: GridElement;

  constructor() {
    super('automated-suite-exec-comparator');
    this.gridElement = GridElement.createGridElement('automated-suite-execution-comparator');
  }
}
