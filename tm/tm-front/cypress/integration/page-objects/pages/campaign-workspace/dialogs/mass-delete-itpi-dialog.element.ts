import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { BaseDialogElement } from '../../../elements/dialog/base-dialog.element';

export class MassDeleteItpiDialogElement extends BaseDialogElement {
  iterationId: number | '*';
  itpiIds: number[];

  constructor(iterationId: number | '*', itpiIds: number[]) {
    super('iteration-tpi-mass-delete-dialog');
    this.iterationId = iterationId;
    this.itpiIds = itpiIds;
  }

  deleteForFailure(_response: any) {}

  deleteForSuccess(response?: GridResponse) {
    const removeMock = new HttpMockBuilder<{ nbIssues: number }>(
      `iteration/${this.iterationId}/test-plan/${[this.itpiIds]}`,
    )
      .delete()
      .responseBody({ nbIssues: 0 })
      .build();
    const mock = new HttpMockBuilder<GridResponse>(`iteration/${this.iterationId}/test-plan`)
      .post()
      .responseBody(response)
      .build();
    this.clickOnConfirmButton();
    removeMock.wait();
    return mock.waitResponseBody();
  }
}
