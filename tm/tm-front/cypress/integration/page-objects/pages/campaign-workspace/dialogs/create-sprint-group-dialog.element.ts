import { CreateEntityDialog } from '../../create-entity-dialog.element';
import { ProjectData } from '../../../../../../projects/sqtm-core/src/lib/model/project/project-data.model';

export class CreateSprintGroupDialog extends CreateEntityDialog {
  constructor(project?: ProjectData) {
    super(
      {
        treePath: 'campaign-tree',
        viewPath: 'sprint-group-view',
        newEntityPath: 'new-sprint-group',
      },
      project,
    );
  }
}
