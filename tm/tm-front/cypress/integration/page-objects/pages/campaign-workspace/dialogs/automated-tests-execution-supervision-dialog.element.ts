import { GridRowElement } from '../../../../utils/grid-selectors.builder';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { SelectFieldElement } from '../../../elements/forms/select-field.element';
import { GridElement } from '../../../elements/grid/grid.element';
import {
  SquashAutomProjectPreview,
  TestAutomationProjectPreview,
} from '../../../../../../projects/sqtm-core/src/lib/model/test-automation/automated-suite-preview.model';
import {
  AutomatedExecutionOverview,
  AutomatedItemOverview,
  AutomatedSuiteOverview,
} from '../../../../../../projects/sqtm-core/src/lib/model/test-automation/automated-suite-overview.model';
import { ExecutionStatusKeys } from '../../../../../../projects/sqtm-core/src/lib/model/level-enums/level-enum';
import { ExpectedTestAutomationProjectPreview } from '../../../../model/test-automation/automated-suite-preview.model';
import { BaseDialogElement } from '../../../elements/dialog/base-dialog.element';

export class AutomatedTestsExecutionSupervisionDialogElement extends BaseDialogElement {
  constructor() {
    super('automated-tests-execution-supervision');
  }

  displayEnvironments(tmProjectId: number) {
    this.getSquashAutomSectionWithTmProjectId(tmProjectId)
      .find('sqtm-app-environment-selection-panel')
      .find('i[nztype="caret-right"]')
      .click();
  }

  addEnvironmentTag(tmProjectId: number, newTag: string) {
    const listRefreshMock = new HttpMockBuilder('test-automation/*/available-tags').build();
    this.getSquashAutomSectionWithTmProjectId(tmProjectId)
      .find('sqtm-app-environment-selection-panel')
      .find('sqtm-core-editable-tag-list-field')
      .find('nz-select-search input')
      .click({ force: true })
      .type(newTag)
      .type('{enter}');
    listRefreshMock.wait();
  }

  getSquashAutomSectionWithTmProjectId(tmProjectId: number) {
    return this.findByElementId(`squash-autom-${tmProjectId}`);
  }

  checkAgentPhaseContentForJenkinsProjects(
    expectedAutomationProjects: ExpectedTestAutomationProjectPreview[],
  ) {
    this.getAutomationProjectSections().then((automProjectSections) =>
      cy.wrap(automProjectSections.length).should('equal', expectedAutomationProjects.length),
    );
    expectedAutomationProjects.forEach((automProject) =>
      this.checkAutomationProjectPreviewContent(automProject),
    );
  }

  checkAgentPhaseContentForSquashAutomProjects(
    expectedSquashAutomProjects: SquashAutomProjectPreview[],
  ) {
    this.getSquashAutomProjectSections().then((squashAutomProjectSections) =>
      cy
        .wrap(squashAutomProjectSections.length)
        .should('equal', expectedSquashAutomProjects.length),
    );
    expectedSquashAutomProjects.forEach((squashAutomProject) =>
      this.checkSquashAutomProjectPreviewContent(squashAutomProject),
    );
  }

  changeAgent(automationProjectId: number, option: string) {
    const agentSelectField = new SelectFieldElement(() =>
      this.findByFieldName(automationProjectId.toString()),
    );
    agentSelectField.selectValue(option);
  }

  confirmAgentPhase(response?: AutomatedSuiteOverview) {
    const createSuiteAndExecute = new HttpMockBuilder('automated-suites/create-and-execute')
      .post()
      .responseBody(response)
      .build();
    this.clickOnConfirmButton();
    createSuiteAndExecute.wait();
  }

  checkExecutionPhaseStaticContent(
    checkJenkinsTable: boolean = true,
    checkSquashAutomTable: boolean = true,
  ) {
    if (checkJenkinsTable) {
      this.checkTfTableTitle();
    }
    if (checkSquashAutomTable) {
      this.checkSquashAutomTableTitle();
    }
    this.selectButton('close').should('exist');
  }

  checkExecutionPhaseDynamicContentAtStart(automatedSuiteOverview: AutomatedSuiteOverview) {
    this.checkTfProgressBar(automatedSuiteOverview.tfPercentage);
    this.checkTfGridContent(automatedSuiteOverview.tfExecutions);
    this.checkAutomProgressBar(false);
    this.checkSquashAutomGridContent(automatedSuiteOverview.automExecutions);
  }

  checkExecutionPhaseDynamicContentProceeding(automatedSuiteOverview: AutomatedSuiteOverview) {
    new HttpMockBuilder('automated-suites/*/executions')
      .get()
      .responseBody(automatedSuiteOverview)
      .build()
      .wait();
    this.checkTfProgressBar(automatedSuiteOverview.tfPercentage);
    this.checkTfGridStatuses(automatedSuiteOverview.tfExecutions);
    this.checkAutomProgressBar(true);
    this.checkSquashAutomGridContent(automatedSuiteOverview.automExecutions);
  }

  checkWarningPhaseContent() {
    this.find('i').should('exist').should('have.length', 1);
    this.find('.close-warning-content').should('exist');
  }

  closeBeforeEnd(automatedSuiteOverview: AutomatedSuiteOverview) {
    new HttpMockBuilder('automated-suites/*/executions')
      .get()
      .responseBody(automatedSuiteOverview)
      .build()
      .wait();
    this.clickOnCloseButton();
  }

  private checkTfGridContent(executionOverviews: AutomatedExecutionOverview[]) {
    const gridElement: GridElement = GridElement.createGridElement(
      'tf-automated-test-plan-execution',
    );
    gridElement.assertExists();
    gridElement.assertColumnCount(4);
    gridElement.assertRowCount(executionOverviews.length);
    executionOverviews.forEach((overview: AutomatedExecutionOverview) => {
      gridElement.assertRowExist(overview.id);
      const gridRowElement: GridRowElement = gridElement.getRow(overview.id);
      this.checkCellContainsText(gridRowElement, 'automatedProject', overview.automatedProject);
      this.checkCellContainsText(gridRowElement, 'name', overview.name);
      this.checkCellContainsText(gridRowElement, 'node', overview.node);
      this.checkStatusCell(gridRowElement, overview.status);
    });
  }

  private checkTfGridStatuses(executionOverviews: AutomatedExecutionOverview[]) {
    const gridElement: GridElement = GridElement.createGridElement(
      'tf-automated-test-plan-execution',
    );
    executionOverviews.forEach((overview: AutomatedExecutionOverview) => {
      const gridRowElement: GridRowElement = gridElement.getRow(overview.id);
      this.checkStatusCell(gridRowElement, overview.status);
    });
  }

  private checkSquashAutomGridContent(squashAutomItemOverviews: AutomatedItemOverview[]) {
    const gridElement: GridElement = GridElement.createGridElement(
      'squash-autom-automated-test-plan-execution',
    );
    gridElement.assertExists();
    gridElement.assertColumnCount(4);
    gridElement.assertRowCount(squashAutomItemOverviews.length);
    squashAutomItemOverviews.forEach((itemOverview: AutomatedItemOverview) => {
      gridElement.assertRowExist(itemOverview.id);
      const gridRowElement: GridRowElement = gridElement.getRow(itemOverview.id);
      this.checkCellContainsText(gridRowElement, 'name', itemOverview.name);
      this.checkCellContainsText(gridRowElement, 'datasetLabel', itemOverview.datasetLabel);
      this.checkCellContainsText(
        gridRowElement,
        'automatedServerName',
        itemOverview.automatedServerName,
      );
      this.checkStatusCell(gridRowElement, itemOverview.status);
    });
  }

  checkTfProgressBar(progress: number) {
    if (progress !== 100) {
      this.find('nz-progress span').first().should('contain.text', progress.toString());
    } else {
      this.find('nz-progress svg').first().should('exist');
    }
  }

  checkAutomProgressBar(terminated: boolean) {
    if (terminated) {
      this.find('nz-progress svg').eq(1).should('exist');
    } else {
      this.find('nz-progress span').eq(1).should('contain.text', '%');
    }
  }

  private checkCellContainsText(
    gridRowElement: GridRowElement,
    cellName: string,
    expectedContent: string,
  ) {
    gridRowElement.cell(cellName).findCellTextSpan().should('contain.text', expectedContent);
  }

  private checkStatusCell(gridRowElement: GridRowElement, expectedStatus: ExecutionStatusKeys) {
    gridRowElement
      .cell('status')
      .findCell()
      .find('.status-badge')
      .should('have.css', 'background-color', this.convertStatusColorToRgb(expectedStatus));
  }

  /* Cypress gives background-color as rgb(_, _, _). */
  private convertStatusColorToRgb(status: ExecutionStatusKeys) {
    switch (status) {
      case 'READY':
        return 'rgb(163, 178, 184)';
      case 'RUNNING':
        return 'rgb(0, 120, 174)';
      case 'SUCCESS':
        return 'rgb(0, 111, 87)';
      case 'FAILURE':
        return 'rgb(203, 21, 36)';
      case 'BLOCKED':
        return 'rgb(255, 204, 0)';
      case 'UNTESTABLE':
        return 'rgb(61, 61, 61)';
      default:
        throw new Error('This ExecutionStatus is not supported.');
    }
  }

  private checkTfTableTitle() {
    this.find('sqtm-app-tf-execution-table .section-title')
      .should('exist')
      .should('contain.text', 'Tests exécutés via Squash TF');
  }

  private checkSquashAutomTableTitle() {
    this.find('sqtm-app-squash-autom-execution-table .section-title')
      .should('exist')
      .should('contain.text', 'Tests exécutés via Squash Orchestrator');
  }

  getAutomationProjectSections() {
    return this.find('sqtm-app-automation-project-menu');
  }

  getSquashAutomProjectSections() {
    return this.find('sqtm-app-squash-autom-project-preview');
  }

  checkAutomationProjectPreviewContent(
    expectedAutomationProject: ExpectedTestAutomationProjectPreview,
  ) {
    this.checkJenkinsProjectSectionMessage(expectedAutomationProject);
    this.checkNodeOptions(expectedAutomationProject);
    this.checkTestList(expectedAutomationProject);
  }

  checkSquashAutomProjectPreviewContent(expectedSquashAutomProject: SquashAutomProjectPreview) {
    this.checkSquashAutomSectionMessage(expectedSquashAutomProject);
    this.checkSquashAutomPreviewTestGrid(expectedSquashAutomProject);
  }

  checkJenkinsProjectSectionMessage(expectedAutomationProject: TestAutomationProjectPreview) {
    const automationProjectSection = this.getAutomationProjectSectionWithId(
      expectedAutomationProject.projectId,
    );
    automationProjectSection
      .find('.section-title')
      .should('contain.text', expectedAutomationProject.label)
      .should('contain.text', expectedAutomationProject.server);
  }

  checkSquashAutomSectionMessage(expecteSquashAutomProject: SquashAutomProjectPreview) {
    const squashAutomProjectSection = this.getSquashAutomPreviewSectionWithId(
      expecteSquashAutomProject.projectId,
    );
    squashAutomProjectSection
      .find('.section-title')
      .should('contain.text', expecteSquashAutomProject.projectName)
      .should('contain.text', expecteSquashAutomProject.serverName);
  }

  private checkNodeOptions(expectedAutomationProject: TestAutomationProjectPreview) {
    const agentSelectField = new SelectFieldElement(() =>
      this.findByFieldName(expectedAutomationProject.projectId.toString()),
    );
    agentSelectField.checkSelectedOption('Indifférent');
    agentSelectField.checkAllOptions(expectedAutomationProject.nodes);
  }

  private checkTestList(expectedAutomationProject: ExpectedTestAutomationProjectPreview) {
    const automationProjectId: number = expectedAutomationProject.projectId;
    const testList: string[] = expectedAutomationProject.testList;

    const testListRequest = new HttpMockBuilder(
      'automated-suites/preview/test-list?auto-project-id=*',
    )
      .responseBody(testList)
      .post()
      .build();
    cy.get(`nz-collapse-panel[data-test-element-id="${automationProjectId}"] .label-color`)
      .should('exist')
      .should('contain.text', `Liste des cas de test automatisés (${testList.length} cas de test)`)
      .click();
    testListRequest.wait();
    cy.get(`nz-collapse-panel[data-test-element-id="${automationProjectId}"] ul > li`).should(
      'have.length',
      testList.length,
    );
    testList.forEach((test) =>
      cy
        .contains(`nz-collapse-panel[data-test-element-id="${automationProjectId}"] ul > li`, test)
        .should('exist'),
    );
  }

  getAutomationProjectSectionWithId(automationProjectId: number) {
    return this.findByElementId(automationProjectId.toString());
  }

  getSquashAutomPreviewSectionWithId(projectId: number) {
    return this.findByElementId(`squash-autom-${projectId}`);
  }

  checkSquashAutomPreviewTestGrid(expectedSquashAutomProject: SquashAutomProjectPreview) {
    const gridElement: GridElement = GridElement.createGridElement('squash-autom-test-case-list');
    gridElement.assertExists();
    gridElement.assertColumnCount(5);
    gridElement.assertRowCount(expectedSquashAutomProject.testCases.length);
  }
}
