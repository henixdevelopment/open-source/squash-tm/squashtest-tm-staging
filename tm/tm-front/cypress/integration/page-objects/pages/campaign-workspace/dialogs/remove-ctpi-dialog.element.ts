import { DeleteConfirmDialogElement } from '../../../elements/dialog/delete-confirm-dialog.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class RemoveCtpiDialogElement extends DeleteConfirmDialogElement {
  itemTestPlanId: number | '*';
  campaignId: number | '*';

  constructor(campaignId: number | '*', itemTestPlanId: number | '*') {
    super('confirm-delete');
    this.itemTestPlanId = itemTestPlanId;
    this.campaignId = campaignId;
  }

  override deleteForFailure(_response: any) {}

  override deleteForSuccess(response?: any) {
    const removeMock = new HttpMockBuilder<any>(
      `campaign/${this.campaignId}/test-plan/${[this.itemTestPlanId]}`,
    )
      .delete()
      .responseBody('')
      .build();
    const mock = new HttpMockBuilder<GridResponse>(`campaign/${this.campaignId}/test-plan`)
      .post()
      .responseBody(response)
      .build();
    this.clickOnConfirmButton();
    removeMock.wait();
    return mock.waitResponseBody();
  }
}
