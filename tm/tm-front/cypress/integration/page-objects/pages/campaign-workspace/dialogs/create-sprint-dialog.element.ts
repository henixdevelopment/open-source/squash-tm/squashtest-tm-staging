import { CreateEntityDialog } from '../../create-entity-dialog.element';
import { ProjectData } from '../../../../../../projects/sqtm-core/src/lib/model/project/project-data.model';

export class CreateSprintDialog extends CreateEntityDialog {
  constructor(project?: ProjectData) {
    super(
      {
        treePath: 'campaign-tree',
        viewPath: 'sprint-view',
        newEntityPath: 'new-sprint',
      },
      project,
    );
  }
}
