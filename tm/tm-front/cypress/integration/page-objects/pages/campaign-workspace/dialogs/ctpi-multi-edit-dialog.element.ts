import { SelectFieldElement } from '../../../elements/forms/select-field.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { DataRowModel } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { BaseDialogElement } from '../../../elements/dialog/base-dialog.element';

export class CtpiMultiEditDialogElement extends BaseDialogElement {
  public readonly assigneeSelect: SelectFieldElement = new SelectFieldElement(() =>
    this.findByFieldName('assignee'),
  );

  constructor() {
    super('ctpi-multi-edit');
  }

  confirm(gridRefreshResponse?: DataRowModel[]): void {
    const updateMock = new HttpMockBuilder('campaign/test-plan/*/mass-update').post().build();

    const testPlanMock = new HttpMockBuilder('campaign/*/test-plan')
      .post()
      .responseBody({ dataRows: gridRefreshResponse })
      .build();

    const refreshTreeMock = new HttpMockBuilder('campaign-tree/refresh')
      .post()
      .responseBody({ dataRows: [] })
      .build();

    this.clickOnConfirmButton();

    updateMock.wait();
    testPlanMock.wait();
    refreshTreeMock.wait();
  }

  toggleAssigneeEdition(): void {
    this.clickLabel('Assignation');
  }

  selectAssignee(assignee: string): void {
    this.assigneeSelect.selectValue(assignee);
  }

  clickOnConfirmButton() {
    super.clickOnConfirmButton();
    this.assertNotExist();
  }

  private clickLabel(labelContent: string): void {
    this.find('span').contains(labelContent).click();
  }
}
