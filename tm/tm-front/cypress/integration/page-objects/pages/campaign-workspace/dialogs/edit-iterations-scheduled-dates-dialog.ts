import { DatePickerElement } from '../../../elements/forms/date-picker.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { IterationPlanning } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/campaign-model';
import { BaseDialogElement } from '../../../elements/dialog/base-dialog.element';

export class EditIterationsScheduledDatesDialog extends BaseDialogElement {
  constructor(private campaignId: string | number = '*') {
    super('iteration-planning-dialog');
  }

  assertIsOpened() {
    this.rootElement.should('be.visible');
  }

  setScheduledStartDateToNow(iterationId: number, dateToday: boolean, valueDate?: string) {
    const datePickerElement = new DatePickerElement(() =>
      this.findByElementId(`iteration-${iterationId}-scheduled-start-date`),
    );
    if (dateToday === true) {
      datePickerElement.selectTodayDate();
    } else {
      datePickerElement.click();
      datePickerElement.rootElement.type(valueDate);
    }
  }

  setScheduledEndDateToNow(iterationId: number, dateToday: boolean, valueDate?: string) {
    const datePickerElement = new DatePickerElement(() =>
      this.findByElementId(`iteration-${iterationId}-scheduled-end-date`),
    );
    if (dateToday === true) {
      datePickerElement.selectTodayDate();
    } else {
      datePickerElement.click();
      datePickerElement.rootElement.type(valueDate);
      datePickerElement.rootElement.type('{enter}');
    }
  }

  confirm(serverResponse: IterationPlanning[] = []) {
    const httpMock = new HttpMockBuilder(`campaign/${this.campaignId}/iterations/planning`)
      .post()
      .responseBody(serverResponse)
      .build();
    this.clickOnConfirmButton();
    httpMock.wait();
  }
}
