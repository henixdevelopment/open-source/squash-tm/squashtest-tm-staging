import { GroupedMultiListFieldElement } from '../../../elements/forms/grouped-multi-list-field.element';
import { SelectFieldElement } from '../../../elements/forms/select-field.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { DataRowModel } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { BaseDialogElement } from '../../../elements/dialog/base-dialog.element';

export class ItpiMultiEditDialogElement extends BaseDialogElement {
  public readonly testSuiteMultiList: GroupedMultiListFieldElement;
  public readonly assigneeSelect: SelectFieldElement;
  public readonly statusSelect: SelectFieldElement;

  constructor() {
    super('itpi-multi-edit');

    this.testSuiteMultiList = new GroupedMultiListFieldElement(
      '[data-test-element-id="test-suites"]',
    );
    this.assigneeSelect = new SelectFieldElement(() => this.findByFieldName('assignee'));
    this.statusSelect = new SelectFieldElement(() => this.findByFieldName('status'));
  }

  confirm(gridRefreshResponse?: DataRowModel[]): void {
    const updateMock = new HttpMockBuilder('iteration/test-plan/*/mass-update').post().build();
    const testPlanMock = new HttpMockBuilder('iteration/*/test-plan')
      .post()
      .responseBody({ dataRows: gridRefreshResponse })
      .build();

    const refreshTreeMock = new HttpMockBuilder('campaign-tree/refresh')
      .post()
      .responseBody({ dataRows: [] })
      .build();

    this.clickOnConfirmButton();

    updateMock.wait();
    testPlanMock.wait();
    refreshTreeMock.wait();
  }

  toggleTestSuiteEdition(): void {
    this.clickLabel('Suites de test');
  }

  selectTestSuites(testSuiteNames: string[]): void {
    this.testSuiteMultiList.showMultiList();
    this.testSuiteMultiList.multiList.assertExists();
    testSuiteNames.forEach((s) => this.testSuiteMultiList.multiList.toggleOneItem(s));
    this.testSuiteMultiList.multiList.close();
  }

  toggleAssigneeEdition(): void {
    this.clickLabel('Assignation');
  }

  selectAssignee(assignee: string): void {
    this.assigneeSelect.selectValue(assignee);
  }

  toggleStatusEdition(): void {
    this.clickLabel('Statut');
  }

  selectStatus(status: string): void {
    this.statusSelect.selectValue(status);
  }

  override clickOnConfirmButton() {
    super.clickOnConfirmButton();
    this.assertNotExist();
  }

  private clickLabel(labelContent: string): void {
    this.find('span').contains(labelContent).click();
  }
}
