import { CreateEntityDialog } from '../../create-entity-dialog.element';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { ProjectData } from '../../../../../../projects/sqtm-core/src/lib/model/project/project-data.model';

export class CreateCampaignDialog extends CreateEntityDialog {
  constructor(project?: ProjectData, domain?: BindableEntity) {
    super(
      {
        treePath: 'campaign-tree',
        viewPath: 'campaign-view',
        newEntityPath: 'new-campaign',
      },
      project,
      domain,
    );
  }
}
