import { DeleteConfirmDialogElement } from '../../../elements/dialog/delete-confirm-dialog.element';
import { ExecutionRunnerProloguePage } from '../../execution/execution-runner-prologue-page';

type Entity = 'campaign' | 'iteration' | 'test-suite';

export class ConfirmDeleteAllExecutionElement extends DeleteConfirmDialogElement {
  constructor() {
    super('confirm-delete-all-execution');
  }

  deleteForFailure(_response: any) {
    throw Error('Use specific dialog for delete success/failure');
  }

  deleteForSuccess(_response: any) {
    throw Error('Use specific dialog for delete success/failure');
  }

  clickConfirmButtonAndGoToNewWindowEntityRunner(
    entityType: Entity,
    entityId: number,
    testPlanId: number,
    executionId: number,
  ) {
    cy.window().then(() => {
      this.clickOnConfirmButton();
      cy.visit(
        Cypress.config().baseUrl +
          `/execution-runner/${entityType}/${entityId}/test-plan/${testPlanId}/execution/${executionId}/prologue`,
      );
    });
    return new ExecutionRunnerProloguePage();
  }
}
