import { DeleteConfirmDialogElement } from '../../../elements/dialog/delete-confirm-dialog.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { DataRowModel } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';

export class DeleteCampaignTreeNodesDialog extends DeleteConfirmDialogElement {
  constructor(private treeUrl: string) {
    super('confirm-delete');
  }

  override deleteForFailure(_response: any) {}

  override deleteForSuccess(_response: any) {}

  deleteNodes(
    deletedNodeIds: string[],
    _parentIds: string[],
    refreshedNodes: DataRowModel[],
    removeFromIter: boolean = false,
  ) {
    const refreshMock = new HttpMockBuilder(`${this.treeUrl}/refresh`)
      .post()
      .responseBody({ dataRows: refreshedNodes })
      .build();
    const deleteMock = new HttpMockBuilder(
      `${this.treeUrl}/${deletedNodeIds.join(',')}?remove_from_iter=${removeFromIter}`,
    )
      .delete()
      .build();
    this.clickOnConfirmButton();
    deleteMock.wait();
    refreshMock.wait();
  }
}
