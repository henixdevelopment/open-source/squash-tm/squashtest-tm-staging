import { selectByDataTestMenuId } from '../../../../utils/basic-selectors';

export class ExportSprintIssuesDialogElement {
  assertExists() {
    cy.get(selectByDataTestMenuId('export-menu')).should('exist');
  }
}
