import { DeleteConfirmDialogElement } from '../../../elements/dialog/delete-confirm-dialog.element';
import { HTTP_RESPONSE_STATUS, HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { AlertDialogElement } from '../../../elements/dialog/alert-dialog.element';

export class RemoveSprintReqVersionDialogElement extends DeleteConfirmDialogElement {
  sprintId: number;
  sprintReqVersionIds: number[];

  constructor(sprintId: number, sprintReqVersionIds: number[]) {
    super('confirm-delete');
    this.sprintId = sprintId;
    this.sprintReqVersionIds = sprintReqVersionIds;
  }

  override deleteForFailure(_response: any) {}

  override deleteForSuccess(response?: any) {
    const url = `sprint/${this.sprintId}/requirements/${this.sprintReqVersionIds}`;
    const mock = new HttpMockBuilder(url).delete().responseBody(response).build();
    this.clickOnConfirmButton();
    mock.wait();
  }

  deleteWithClosedSprint(response?: any) {
    const url = `sprint/${this.sprintId}/requirements/${this.sprintReqVersionIds}`;
    const mock = new HttpMockBuilder(url)
      .delete()
      .responseBody(response)
      .status(HTTP_RESPONSE_STATUS.PRECONDITION_FAIL)
      .build();
    this.clickOnConfirmButton();
    mock.wait();
    return new AlertDialogElement('alert');
  }
}
