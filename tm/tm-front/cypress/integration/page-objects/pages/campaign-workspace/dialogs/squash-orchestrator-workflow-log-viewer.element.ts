import { BaseDialogElement } from '../../../elements/dialog/base-dialog.element';

export class SquashOrchestratorWorkflowLogViewerElement extends BaseDialogElement {
  title: string;
  logs: string;
  errorMessage: string;

  constructor(id: string, title: string, logs?: string, errorMessage?: string) {
    super(id);
    this.title = title;
    this.logs = logs;
    this.errorMessage = errorMessage;
  }

  verifyDialogContent() {
    this.assertExists();
    this.assertCloseButtonExists();
    this.assertTitleHasText(this.title);
    if (this.logs != null) {
      this.assertLogsContains(this.logs);
    }
    if (this.errorMessage == null) {
      this.assertErrorMessageIsNotDisplayed();
    }
  }

  private assertErrorMessageIsNotDisplayed() {
    this.findByElementId('workflow-viewer-unreachable-orchestrator').should('not.exist');
  }

  private assertLogsContains(logs: string) {
    this.findByElementId('workflow-logs-console').should('contain.text', logs);
  }
}

export interface WorkflowLogDto {
  logs: string;
  reachable: boolean;
  isStoredInDatabase: boolean;
}

export interface AutomatedSuiteWorkflowDto {
  workflowId: string;
  projectId: number;
}
