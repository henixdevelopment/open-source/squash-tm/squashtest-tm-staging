import { GridElement } from '../../../elements/grid/grid.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { BaseDialogElement } from '../../../elements/dialog/base-dialog.element';
import {
  selectByDataTestButtonId,
  selectByDataTestDialogButtonId,
} from '../../../../utils/basic-selectors';

export class ExecutionHistoryDialog extends BaseDialogElement {
  public gridElement: GridElement;

  constructor(gridResponse?: GridResponse) {
    super('test-plan-execution-history-dialog');
    this.gridElement = GridElement.createGridElement(
      'iteration-test-plan-execution-history',
      `iteration/*/test-plan/*/executions`,
      gridResponse,
    );
  }

  checkFirstIdInExecutionHistoryDialog(executionOrder: number, excutionId: number) {
    cy.get(`[data-test-cell-id='executionOrder']`)
      .find('a:first-child')
      .should('exist')
      .should('have.attr', 'href')
      .and('contain', excutionId);
  }

  deleteSelectedExecution(
    executionId: number,
    pageUrl: 'campaign' | 'iteration' | 'test-suite',
    response?: any,
  ) {
    const selectedRow = this.gridElement.getRow(executionId);
    let urlRefresh: string = null;
    let urlMockDelete: string = null;
    const deleteDialog = new BaseDialogElement('confirm-delete');
    selectedRow.cell('delete').iconRenderer().click();

    switch (pageUrl) {
      case 'campaign': {
        urlRefresh = 'campaign/*/test-plan/*/executions';
        urlMockDelete = 'campaign/*/test-plan/execution/*';
        break;
      }
      case 'iteration': {
        urlRefresh = 'iteration/*/test-plan/*/executions';
        urlMockDelete = 'iteration/*/test-plan/execution/*';
        break;
      }
      case 'test-suite': {
        urlRefresh = 'iteration/*/test-plan/*/executions';
        urlMockDelete = 'test-suite/*/test-plan/execution/*';
        break;
      }
    }
    const mockDeleteRequest = new HttpMockBuilder<{ nbIssues: number }>(urlMockDelete) //
      .delete()
      .responseBody({ nbIssues: 0 })
      .build();
    const mockRefreshResponse = new HttpMockBuilder(urlRefresh)
      .responseBody(response)
      .post()
      .build();
    deleteDialog.confirm();
    mockDeleteRequest.waitResponseBody();
    mockRefreshResponse.waitResponseBody();
  }

  deleteMultiSelectedExecution(executionIds: number[], reponse?: any) {
    cy.get(selectByDataTestButtonId('show-confirm-mass-delete-execution-dialog'))
      .find('span')
      .should('have.class', 'label-color');
    this.gridElement.selectRows(executionIds, '#', 'leftViewport');
    cy.get(selectByDataTestButtonId('show-confirm-mass-delete-execution-dialog'))
      .find('span')
      .should('not.have.class', 'label-color');

    cy.get(selectByDataTestButtonId('show-confirm-mass-delete-execution-dialog'))
      .find('span')
      .click();

    const mockDeleteRequest = new HttpMockBuilder<{ nbIssues: number }>(
      `iteration/*/test-plan/execution/*`,
    )
      .delete()
      .responseBody({ nbIssues: 0 })
      .build();
    const mockRefreshResponse = new HttpMockBuilder(`iteration/*/test-plan/*/executions`)
      .responseBody(reponse)
      .post()
      .build();

    cy.get(selectByDataTestDialogButtonId('confirm')).click();

    mockDeleteRequest.waitResponseBody();
    mockRefreshResponse.waitResponseBody();
  }
}
