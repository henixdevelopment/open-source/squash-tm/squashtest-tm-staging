import { SelectFieldElement } from '../../../elements/forms/select-field.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { DataRowModel } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { BaseDialogElement } from '../../../elements/dialog/base-dialog.element';

export class TestSuiteTpiMultiEditDialogElement extends BaseDialogElement {
  public readonly assigneeSelect: SelectFieldElement;
  public readonly statusSelect: SelectFieldElement;

  constructor() {
    super('itpi-multi-edit');
    this.assigneeSelect = new SelectFieldElement(() => this.findByFieldName('assignee'));
    this.statusSelect = new SelectFieldElement(() => this.findByFieldName('status'));
  }

  toggleAssigneeEdition(): void {
    this.clickLabel('Assignation');
  }

  selectAssignee(assignee: string): void {
    this.assigneeSelect.selectValue(assignee);
  }

  toggleStatusEdition(): void {
    this.clickLabel('Statut');
  }

  selectStatus(status: string): void {
    this.statusSelect.selectValue(status);
  }

  confirm(gridRefreshResponse?: DataRowModel[]): void {
    const updateMock = new HttpMockBuilder('test-suite/test-plan/*/mass-update').post().build();
    const testPlanMock = new HttpMockBuilder('test-suite/*/test-plan')
      .post()
      .responseBody({ dataRows: gridRefreshResponse })
      .build();

    const refreshTreeMock = new HttpMockBuilder('campaign-tree/refresh')
      .post()
      .responseBody({ dataRows: [] })
      .build();

    this.clickOnConfirmButton();

    updateMock.wait();
    testPlanMock.wait();
    refreshTreeMock.wait();
  }

  private clickLabel(labelContent: string): void {
    this.find('span').contains(labelContent).click();
  }

  clickOnConfirmButton() {
    super.clickOnConfirmButton();
    this.assertNotExist();
  }
}
