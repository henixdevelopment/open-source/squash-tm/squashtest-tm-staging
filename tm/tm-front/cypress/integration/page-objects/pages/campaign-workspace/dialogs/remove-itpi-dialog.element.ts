import { DeleteConfirmDialogElement } from '../../../elements/dialog/delete-confirm-dialog.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class RemoveItpiDialogElement extends DeleteConfirmDialogElement {
  itemTestPlanId: number | '*';
  iterationId: number | '*';

  constructor(itemTestPlanId: number | '*', iterationId: number | '*') {
    super('confirm-delete');
    this.itemTestPlanId = itemTestPlanId;
    this.iterationId = iterationId;
  }

  override deleteForFailure(_response: any) {}

  override deleteForSuccess(response?: any) {
    const removeMock = new HttpMockBuilder<{ nbIssues: number }>(
      `iteration/${this.iterationId}/test-plan/${[this.itemTestPlanId]}`,
    )
      .delete()
      .responseBody({ nbIssues: 0 })
      .build();
    const mock = new HttpMockBuilder<GridResponse>(`iteration/${this.iterationId}/test-plan`)
      .post()
      .responseBody(response)
      .build();
    this.clickOnConfirmButton();
    removeMock.waitResponseBody();
    return mock.waitResponseBody();
  }
}
