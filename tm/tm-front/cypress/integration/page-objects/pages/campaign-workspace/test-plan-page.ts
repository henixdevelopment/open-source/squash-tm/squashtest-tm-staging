import { GridElement, TreeElement } from '../../elements/grid/grid.element';
import { TestCaseForCampaignSearchPage } from '../test-case-workspace/research/test-case-for-campaign-search-page';
import { RequirementTestCaseForCampaignSearchPage } from '../requirement-workspace/search/dialogs/requirement-test-case-for-campaign-search-page';
import { ExecutionPage } from '../execution/execution-page';
import { ExecutionRunnerStepPage } from '../execution/execution-runner-step-page';

export interface TestPlanPage {
  testPlan: GridElement;

  moveItemWithoutServerResponse(itemToMove: string, itemToDropOnto: string): void;

  openTestCaseDrawer(): TreeElement;

  dropIntoTestPlan(idEntity): void;

  closeTestCaseDrawer(): void;

  goToSearchTestCase(): TestCaseForCampaignSearchPage;

  goToRequirementLinkedSearch(): RequirementTestCaseForCampaignSearchPage;

  createNewExecutionOfItem(itemName: string): ExecutionPage;

  openStartOverDialog(): void;

  clickButtonResumeExecution(
    idIteration: string,
    idTestPlan: string,
    idExecution: string,
    idStep: string,
  ): ExecutionRunnerStepPage;

  accessToSession(itemName): void;

  openExecutionWithPopUp(item: string, idExecution: string): void;
}
