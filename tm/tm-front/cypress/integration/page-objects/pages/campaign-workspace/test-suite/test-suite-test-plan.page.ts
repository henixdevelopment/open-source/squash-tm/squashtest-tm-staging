import { Page } from '../../page';
import { GridElement, TreeElement } from '../../../elements/grid/grid.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { AutomatedTestsExecutionSupervisionDialogElement } from '../dialogs/automated-tests-execution-supervision-dialog.element';
import { RemoveOrDetachTestSuiteItpiDialogElement } from '../dialogs/remove-or-detach-test-suite-itpi-dialog.element';
import { VerifyItemTestPlanOptions } from '../iteration/iteration-test-plan.page';
import { TestSuiteTpiMultiEditDialogElement } from '../dialogs/test-suite-tpi-multi-edit-dialog.element';
import Chainable = Cypress.Chainable;
import { AutomatedSuitePreview } from '../../../../../../projects/sqtm-core/src/lib/model/test-automation/automated-suite-preview.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import {
  selectByDataTestButtonId,
  selectByDataTestElementId,
} from '../../../../utils/basic-selectors';
import { MenuElement, MenuItemElement } from '../../../../utils/menu.element';
import { TestCaseForCampaignSearchPage } from '../../test-case-workspace/research/test-case-for-campaign-search-page';
import { RequirementTestCaseForCampaignSearchPage } from '../../requirement-workspace/search/dialogs/requirement-test-case-for-campaign-search-page';
import { ExecutionPage } from '../../execution/execution-page';
import { ExecutionRunnerStepPage } from '../../execution/execution-runner-step-page';
import { ExecutionRunnerProloguePage } from '../../execution/execution-runner-prologue-page';
import { SessionOverviewPage } from '../../session-overview/session-overview.page';
import { TestPlanPage } from '../test-plan-page';

export class TestSuiteTestPlanPage extends Page implements TestPlanPage {
  private constructor(
    rootSelector: string,
    public readonly testPlan: GridElement,
  ) {
    super(rootSelector);
  }

  static navigateTo(testPlan: GridResponse): TestSuiteTestPlanPage {
    const url = 'test-suite/*/test-plan';
    const gridElement = GridElement.createGridElement('test-suite-test-plan', url, testPlan);
    return new TestSuiteTestPlanPage('sqtm-app-test-suite-test-plan-execution', gridElement);
  }

  changeDataset(itemId: string) {
    cy.get(`[data-test-cell-id=dataset-cell]`).click();
    const updatedDatasetMock = new HttpMockBuilder('test-plan-item/test-suite/*/dataset')
      .post()
      .build();
    cy.get(`[data-test-item-id=${itemId}]`).click();
    updatedDatasetMock.wait();
  }

  moveItemWithServerResponse(itemToMove: string, itemToDropOnto: string, response: any): void {
    const mock = new HttpMockBuilder('test-suite/*/test-plan/*/position/*')
      .responseBody(response)
      .post()
      .build();

    this.startDrag(itemToMove);
    this.getDraggedContent()
      .should('be.visible')
      .find(selectByDataTestElementId('cannot-drag-icon'))
      .should('not.exist');
    this.internalDropOnto(itemToDropOnto);

    mock.wait();
  }

  private startDrag(itemToDrag: string): void {
    this.testPlan
      .getRow(itemToDrag, 'leftViewport')
      .cell('#')
      .findCell()
      .find('span')
      .trigger('mousedown', { button: 0 })
      .trigger('mousemove', -20, -20, { force: true })
      .trigger('mousemove', 50, 50, { force: true });
  }

  private internalDropOnto(itemToDropOnto: string): void {
    this.testPlan
      .getRow(itemToDropOnto, 'leftViewport')
      .cell('#')
      .findCell()
      .trigger('mousemove', 50, 50, { force: true })
      .trigger('mouseup', { force: true });
  }

  private getDraggedContent() {
    return cy.get('sqtm-app-test-plan-dragged-content');
  }

  assertCannotMoveItem(itemToMove: string): void {
    this.startDrag(itemToMove);
    this.getDraggedContent().find('[data-test-element-id="cannot-drag-icon"]').should('exist');
    this.internalDropOnto(itemToMove);
  }

  showDeleteConfirmDialog(
    testSuiteId: number,
    itemTestPlanId: number,
    refreshedTestPlan: GridResponse,
  ): RemoveOrDetachTestSuiteItpiDialogElement {
    this.testPlan.scrollPosition('right', 'mainViewport');
    cy.get(`[data-test-row-id=${itemTestPlanId}] [data-test-cell-id="delete"] i`).click();
    return new RemoveOrDetachTestSuiteItpiDialogElement(
      testSuiteId,
      [itemTestPlanId],
      refreshedTestPlan,
    );
  }

  showMassDeleteConfirmDialog(
    testSuiteId: number,
    itemTestPlanIds: number[],
    refreshedTestPlan: GridResponse,
  ): RemoveOrDetachTestSuiteItpiDialogElement {
    cy.get('[data-test-button-id="show-confirm-mass-delete-dialog"]').click();
    return new RemoveOrDetachTestSuiteItpiDialogElement(
      testSuiteId,
      itemTestPlanIds,
      refreshedTestPlan,
    );
  }

  openTestCaseDrawer(response?: GridResponse): TreeElement {
    const treeElement = TreeElement.createTreeElement(
      'test-case-tree-picker',
      'test-case-tree',
      response,
    );
    cy.get('[data-test-button-id=show-test-case-picker]').click();
    treeElement.waitInitialDataFetch();
    return treeElement;
  }

  closeTestCaseDrawer() {
    cy.get('.ant-drawer-body .anticon-close').click();
  }

  enterIntoTestPlan() {
    this.getDropZone().trigger('mouseenter', 'left', { force: true, buttons: 1 });
  }

  private getDropZone(): Chainable<JQuery<HTMLDivElement>> {
    return cy.get(`[data-test-element-id=${TEST_SUITE_TEST_PLAN_DROP_ZONE_ID}]`);
  }

  assertColoredBorderIsVisible() {
    this.getDropZone().should('have.class', 'sqtm-core-border-current-workspace-color');
  }

  assertColoredBorderIsNotVisible() {
    this.getDropZone().should('not.have.class', 'sqtm-core-border-current-workspace-color');
  }

  dropIntoTestPlan(
    testSuiteId: number | string,
    createdTestPlanItemIds?: number[],
    refreshedTestPlan?: GridResponse,
  ) {
    const addTcMock = new HttpMockBuilder<{ itemTestPlanIds: number[] }>(
      `test-suite/${testSuiteId}/test-plan-items`,
    )
      .post()
      .responseBody({ itemTestPlanIds: createdTestPlanItemIds })
      .build();
    new HttpMockBuilder('test-suite/*/test-plan').post().responseBody(refreshedTestPlan).build();
    this.testPlan.declareRefreshData(refreshedTestPlan);
    this.getDropZone().trigger('mouseup', { force: true });
    return addTcMock.waitResponseBody().then((responseBody) => {
      if (refreshedTestPlan) {
        this.testPlan.waitForRefresh();
      }
      return cy.wrap(responseBody.itemTestPlanIds);
    });
  }

  verifyTestPlanItem(options: VerifyItemTestPlanOptions) {
    this.testPlan.assertRowExist(options.id);
    const row = this.testPlan.getRow(options.id);
    if (options.name) {
      if (options.showsAsLink) {
        row.cell('testCaseName').linkRenderer().assertContainText(options.name);
      } else {
        row.cell('testCaseName').textRenderer().assertContainsText(options.name);
      }
    }
  }

  foldGrid() {
    cy.get(`[data-test-element-id="fold-tree-button"]`).click();
  }

  applyFastpassOnItpi(itemId: string) {
    cy.get(`[data-test-cell-id=execution-status-cell]`).click();

    const mock = new HttpMockBuilder('test-plan-item/*/execution-status').post().build();
    const refreshMock = new HttpMockBuilder('campaign-tree/refresh').post().build();
    cy.get(`[data-test-item-id=item-${itemId}]`).trigger('click');
    mock.wait();
    refreshMock.wait();
  }

  openMassEditDialog(): TestSuiteTpiMultiEditDialogElement {
    cy.get('[data-test-button-id=mass-edit]').click();
    return new TestSuiteTpiMultiEditDialogElement();
  }

  assertLaunchAutomatedMenuExist() {
    cy.get('[data-test-button-id="launch-automated"]').should('exist').click();
    cy.get('a.launch-automated-link').should('have.length', 2);
  }

  openAutoTestExecDialogViaLaunchAllButton(
    automatedSuitePreview: AutomatedSuitePreview,
  ): AutomatedTestsExecutionSupervisionDialogElement {
    this.clickOnLaunchAutoTestsIcon();
    cy.get('a.launch-automated-link').should('have.length', 2);

    const resolveTaScriptsRequest = new HttpMockBuilder(
      'automation-requests/associate-TA-script?testSuiteId=*',
    )
      .post()
      .build();
    const automatedSuitePreviewRequest = new HttpMockBuilder('automated-suites/preview')
      .post()
      .responseBody(automatedSuitePreview)
      .build();

    cy.get('a.launch-automated-link')
      .contains('Lancer tous les tests automatisés')
      .should('exist')
      .click();

    resolveTaScriptsRequest.wait();
    automatedSuitePreviewRequest.wait();

    const executionDialog = new AutomatedTestsExecutionSupervisionDialogElement();
    executionDialog.assertExists();
    return executionDialog;
  }

  private clickOnLaunchAutoTestsIcon() {
    cy.get('[data-test-button-id="launch-automated"]').should('exist').click();
  }

  resumeExecution(withFilter: boolean = false, testSuiteId: string = '*') {
    const expectedUrl = withFilter
      ? `test-suite/${testSuiteId}/test-plan/resume-filtered-selection`
      : `test-suite/${testSuiteId}/test-plan/resume`;

    const mock = new HttpMockBuilder(expectedUrl).post().build();
    cy.get('[data-test-button-id=launch-execution]').click();
    mock.wait();
  }

  moveItemWithoutServerResponse(itemToMove: string, itemToDropOnto: string) {
    this.startDrag(itemToMove);
    this.getDraggedContent()
      .should('be.visible')
      .find(selectByDataTestElementId('cannot-drag-icon'))
      .should('not.exist');
    this.internalDropOnto(itemToDropOnto);
  }

  private openSearchMenu() {
    cy.get(selectByDataTestButtonId('search')).click();
    return new MenuElement('search-menu');
  }

  goToSearchTestCase() {
    const searchMenu = this.openSearchMenu();
    const grid = new GridElement('test-case-search');

    searchMenu.item('search-testcase').click();
    return new TestCaseForCampaignSearchPage(grid);
  }

  goToRequirementLinkedSearch() {
    const searchMenu = this.openSearchMenu();
    const grid = new GridElement('tc-by-requirement');

    searchMenu.item('search-tc-by-requirement').click();
    return new RequirementTestCaseForCampaignSearchPage(grid);
  }

  private openExecutionMenuInTestPlan(itemName: string): MenuItemElement {
    this.testPlan.findRowId('testCaseName', itemName).then((idItem) => {
      this.testPlan.getCell(idItem, 'startExecution', 'rightViewport').iconRenderer().click();
    });
    return new MenuItemElement('', 'play-exec');
  }

  createNewExecutionOfItem(itemName: string) {
    const menuItem = this.openExecutionMenuInTestPlan(itemName);
    menuItem.selectLinkElement('manual-execution');
    return new ExecutionPage();
  }

  openStartOverDialog() {
    cy.get(this.rootSelector).find(selectByDataTestButtonId('relaunch-execution')).click();
  }

  private clickOnButtonResumeExecution() {
    return cy.get(this.rootSelector).find(selectByDataTestButtonId('resume-execution')).click();
  }

  private clickOnButtonLaunchExecution() {
    return cy.get(this.rootSelector).find(selectByDataTestButtonId('launch-execution')).click();
  }

  clickButtonResumeExecution(
    idTestSuite: string,
    idTestPlan: string,
    idExecution: string,
    idStep: string,
  ) {
    cy.window().then(() => {
      this.clickOnButtonResumeExecution();
      cy.visit(
        `${Cypress.config().baseUrl}/execution-runner/test-suite/${idTestSuite}/test-plan/${idTestPlan}/execution/${idExecution}/step/${idStep}`,
      );
    });
    return new ExecutionRunnerStepPage();
  }

  openExecutionWithPopUp(item: string, idExecution: string) {
    const menuItem = this.openExecutionMenuInTestPlan(item);
    cy.window().then(() => {
      menuItem.selectLinkElement('popup-manual-execution');
      cy.visit(`${Cypress.config().baseUrl}/execution-runner/execution/${idExecution}/prologue`);
    });
    return new ExecutionRunnerProloguePage();
  }

  clickButtonLaunchExecution(
    idIteration: string,
    idTestPlan: string,
    idExecution: string,
    idStep: string,
  ) {
    cy.window().then(() => {
      this.clickOnButtonLaunchExecution();
      cy.visit(
        `${Cypress.config().baseUrl}/execution-runner/test-suite/${idIteration}/test-plan/${idTestPlan}/execution/${idExecution}/step/${idStep}`,
      );
    });
    return new ExecutionRunnerStepPage();
  }

  accessToSession(itemName: string) {
    this.testPlan.findRowId('testCaseName', itemName).then((idItem) => {
      this.testPlan.getCell(idItem, 'startExecution', 'rightViewport').iconRenderer().click();
    });
    return new SessionOverviewPage();
  }

  assertCellContainsText(testCaseName: string, column: string, expectedText: string) {
    this.testPlan.findRowId('testCaseName', testCaseName).then((idTestCase) => {
      this.testPlan.getCell(idTestCase, column).findCell().contains(expectedText);
    });
  }
}

const TEST_SUITE_TEST_PLAN_DROP_ZONE_ID = 'testSuiteTestPlanDropZone';
