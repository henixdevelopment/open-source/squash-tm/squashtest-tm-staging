import { EditableRichTextFieldElement } from '../../../elements/forms/editable-rich-text-field.element';
import { apiBaseUrl, HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';
import { TestSuiteTestPlanPage } from './test-suite-test-plan.page';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';
import { AbstractTestPlanPage } from '../abstract-test-plan.page';
import { EnvironmentsStatusesCountElement } from '../../../elements/automated-execution-environments/environments-statuses-count.element';

export class TestSuiteViewPage extends AbstractTestPlanPage {
  readonly anchors = AnchorsElement.withLinkIds('plan-exec', 'statistics', 'information', 'issues');
  readonly referenceField = new EditableTextFieldElement('entity-reference');
  readonly descriptionRichField = new EditableRichTextFieldElement(
    'test-suite-description',
    'test-suite/*/description',
  );
  readonly environmentsStatusesCountElement = new EnvironmentsStatusesCountElement();

  // For end to end test suiteId is not required
  constructor(public suiteId?: number | string) {
    super('sqtm-app-test-suite-view');
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/test-suite-view/${this.suiteId}?**`;
    cy.wait(`@${url}`);
  }

  checkData(fieldId: string, value: string) {
    cy.get(`[data-test-field-id=${fieldId}] span`).should('contain.text', value);
  }

  rename(newValue: string, refreshedRows?: GridResponse) {
    const refreshTreeMock = this.getRefreshTreeMock(refreshedRows);
    this.nameTextField.setAndConfirmValue(newValue);
    refreshTreeMock.wait();
  }

  get nameTextField(): EditableTextFieldElement {
    const url = `test-suite/${this.suiteId}/name`;
    return new EditableTextFieldElement('entity-name', url);
  }

  private getRefreshTreeMock(refreshedRows: GridResponse) {
    return new HttpMockBuilder(`campaign-tree/refresh`).post().responseBody(refreshedRows).build();
  }

  showTestPlan(testPlan?: GridResponse): TestSuiteTestPlanPage {
    const testSuiteTestPlanPage = TestSuiteTestPlanPage.navigateTo(testPlan);
    this.anchors.clickLink('plan-exec');
    if (this.suiteId) {
      testSuiteTestPlanPage.testPlan.waitInitialDataFetch();
    }
    return testSuiteTestPlanPage;
  }
}
