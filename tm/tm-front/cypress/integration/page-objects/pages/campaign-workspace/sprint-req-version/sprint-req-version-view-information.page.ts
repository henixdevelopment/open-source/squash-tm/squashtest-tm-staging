import { Page } from '../../page';
import { selectByDataTestElementId } from '../../../../utils/basic-selectors';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { EditableSelectFieldElement } from '../../../elements/forms/editable-select-field.element';

export class SprintReqVersionViewInformationPage extends Page {
  validationStatus: EditableSelectFieldElement = new EditableSelectFieldElement(
    'sprint-req-version-validation-status',
  );

  constructor() {
    super('sqtm-app-sprint-req-version-information-panel');
  }

  clickOnExtendSprintReqVersionDescription() {
    this.find(selectByDataTestElementId('extend-sprint-req_version-description')).click();
  }

  checkSprintReqVersionDescriptionNotExtended() {
    this.find(selectByDataTestElementId('extend-sprint-req_version-description')).should('exist');
  }

  updateValidationStatus(newValidationStatus: string) {
    const mock = new HttpMockBuilder('sprint-req-version/*/validation-status').post().build();
    this.validationStatus.selectValueNoButton(newValidationStatus);
    mock.wait();
  }
}
