import { Page } from '../../page';
import { GridElement, TreeElement } from '../../../elements/grid/grid.element';
import { GridId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-id';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import {
  selectByDataTestButtonId,
  selectByDataTestElementId,
  selectByDataTestFieldId,
  selectByDataTestToolbarButtonId,
} from '../../../../utils/basic-selectors';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { SprintReqVersionTestPlanItem } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/sprint-req-version-test-plan-item.model';
import { SprintReqVersionViewInformationPage } from './sprint-req-version-view-information.page';
import { ExportSprintReqVersionIssuesDialogElement } from '../dialogs/export-sprint-req-version-issues-dialog.element';
import { AvailableTestPlanItemModel } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/sprint-req-version-view-model';
import { CapsuleElement } from '../../../elements/workspace-common/capsule.element';
import Chainable = Cypress.Chainable;

export class SprintReqVersionViewPage extends Page {
  readonly validationStatusCapsule = new CapsuleElement('capsule-validation-status');
  readonly testPlanGrid: GridElement;
  readonly informationPanel: SprintReqVersionViewInformationPage =
    new SprintReqVersionViewInformationPage();
  readonly issueGrid = new GridElement(GridId.SPRINT_REQ_VERSION_ISSUE_EXECUTION);

  constructor(testPlanResponse: GridResponse<SprintReqVersionTestPlanItem>) {
    super('sqtm-app-sprint-req-version-view-sub-page');

    this.testPlanGrid = GridElement.createGridElement(
      GridId.SPRINT_REQ_VERSION_TEST_PLAN,
      'test-plan/*',
      testPlanResponse,
    );
  }

  openTestCaseDrawer(response: GridResponse) {
    const tree = TreeElement.createTreeElement('test-case-tree-picker', 'test-case-tree', response);
    this.clickAddToTestPlanButton();
    tree.waitInitialDataFetch();
    cy.clickVoid();
    cy.removeNzTooltip();
    return tree;
  }

  private clickAddToTestPlanButton() {
    this.find(selectByDataTestButtonId('show-test-case-picker')).click();
  }

  dropTestCaseIntoSprint() {
    const url = 'sprint-req-version/*/test-plan-items';
    const mock = new HttpMockBuilder(url).post().build();
    this.getDropZone().trigger('mouseup', 10, 10, { force: true, buttons: 1 });
    mock.wait();
  }

  private getDropZone(): Chainable<JQuery<HTMLDivElement>> {
    return this.findByElementId('sprintReqVersionTestPlanDropZone');
  }

  closeDrawer() {
    cy.get(selectByDataTestButtonId('close-drawer')).click();
  }

  checkData(fieldId: string, value: string) {
    this.find(selectByDataTestFieldId(fieldId)).should('contain.text', value);
  }

  openExportDialog(): ExportSprintReqVersionIssuesDialogElement {
    cy.get(selectByDataTestToolbarButtonId('export-issue-button')).click();
    return new ExportSprintReqVersionIssuesDialogElement();
  }

  clickUpdateTestPlanButton(availableTestPlanItems: AvailableTestPlanItemModel[]) {
    const mock = new HttpMockBuilder('sprint-req-version-view/*/available-test-plan-items')
      .get()
      .responseBody(availableTestPlanItems)
      .build();
    this.find(selectByDataTestButtonId('update-sprint-req-version-test-plan')).click();
    mock.wait();
  }

  assertActionButtonStatus(selector: string, status: string = 'exist' || 'not.exist') {
    this.find(selectByDataTestElementId(selector)).should(status);
  }

  clickActionButton(selector: string) {
    const mock = new HttpMockBuilder('sprint-req-version/*/validation-status').post().build();
    this.find(selectByDataTestElementId(selector)).click();
    mock.wait();
  }
}
