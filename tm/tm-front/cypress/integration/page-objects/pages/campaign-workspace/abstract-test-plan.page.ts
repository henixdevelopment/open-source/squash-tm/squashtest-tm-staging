import { EntityViewPage, Page } from '../page';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export abstract class AbstractTestPlanPage extends EntityViewPage {
  abstract showTestPlan(testPlan?: GridResponse): Page;

  selectValueInCustomFieldList(nameList: string, value: string): void {
    this.getSelectCustomField(nameList).selectValueNoButton(value);
  }

  assertCustomFieldListContainsText(nameList: string, value: string): void {
    this.getSelectCustomField(nameList).assertContainsText(value);
  }
}
