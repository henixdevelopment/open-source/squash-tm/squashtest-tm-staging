import { CampaignMenuItemIds } from './campaign-workspace.page';
import { ToolbarMenuButtonElement } from '../../elements/workspace-common/toolbar.element';
import { CreateCampaignDialog } from './dialogs/create-campaign-dialog.element';
import { CreateIterationDialog } from './dialogs/create-iteration-dialog.element';
import { CreateSuiteDialog } from './dialogs/create-suite-dialog.element';
import { TreeToolbarElement } from '../../elements/workspace-common/tree-toolbar.element';
import { CreateCampaignFolderDialog } from './dialogs/create-campaign-folder-dialog.element';
import {
  selectByDataTestMenuItemId,
  selectByDataTestToolbarButtonId,
} from '../../../utils/basic-selectors';
import { ProjectData } from '../../../../../projects/sqtm-core/src/lib/model/project/project-data.model';
import { BindableEntity } from '../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { CreateSprintDialog } from './dialogs/create-sprint-dialog.element';
import { CreateSprintGroupDialog } from './dialogs/create-sprint-group-dialog.element';

export class CampaignWorkspaceTreeMenu extends TreeToolbarElement {
  constructor() {
    super('campaign-toolbar');
  }

  openCreateCampaign(projectData?: ProjectData): CreateCampaignDialog {
    this.openAddCampaignMenuItem(CampaignMenuItemIds.newCampaign);
    return new CreateCampaignDialog(projectData, BindableEntity.CAMPAIGN);
  }

  openCreateSprint(projectData?: ProjectData): CreateSprintDialog {
    this.openAddCampaignMenuItem(CampaignMenuItemIds.newSprint);
    return new CreateSprintDialog(projectData);
  }

  openCreateSprintGroup(projectData?: ProjectData): CreateSprintGroupDialog {
    this.openAddCampaignMenuItem(CampaignMenuItemIds.newSprintGroup);
    return new CreateSprintGroupDialog(projectData);
  }

  openAddIteration(projectData?: ProjectData): CreateIterationDialog {
    this.openAddCampaignMenuItem(CampaignMenuItemIds.newIteration);
    return new CreateIterationDialog(projectData, BindableEntity.ITERATION);
  }

  openAddTestSuite(projectData?: ProjectData): CreateSuiteDialog {
    this.openAddCampaignMenuItem(CampaignMenuItemIds.newTestSuite);
    return new CreateSuiteDialog(projectData, BindableEntity.TEST_SUITE);
  }

  private openAddCampaignMenuItem(menuItemId: CampaignMenuItemIds) {
    const createButtonElement = this.createButton();
    const createMenuElement = createButtonElement.showMenu();
    const menuItem = createMenuElement.item(menuItemId);
    menuItem.click();
  }

  assertCreateCampaignDisabled() {
    this.assertAddCampaignMenuItemDisabled(CampaignMenuItemIds.newCampaign);
  }

  assertCreateSprintDisabled() {
    this.assertAddCampaignMenuItemDisabled(CampaignMenuItemIds.newSprint);
  }

  assertCreateSprintGroupDisabled() {
    this.assertAddCampaignMenuItemDisabled(CampaignMenuItemIds.newSprintGroup);
  }

  assertCreateFolderDisabled() {
    this.assertAddCampaignMenuItemDisabled(CampaignMenuItemIds.newFolder);
  }

  assertAddIterationDisabled() {
    this.assertAddCampaignMenuItemDisabled(CampaignMenuItemIds.newIteration);
  }

  assertAddSuiteDisabled() {
    this.assertAddCampaignMenuItemDisabled(CampaignMenuItemIds.newTestSuite);
  }

  private assertAddCampaignMenuItemDisabled(menuItemId: CampaignMenuItemIds) {
    const createButtonElement = this.createButton();
    const createMenuElement = createButtonElement.showMenu();
    const menuItem = createMenuElement.item(menuItemId);
    menuItem.assertDisabled();
    createButtonElement.hideMenu();
  }

  createButton(): ToolbarMenuButtonElement {
    const createButtonElement = this.buttonMenu('create-button', 'create-menu');
    createButtonElement.assertExists();
    return createButtonElement;
  }

  openCreateCampaignFolder(): CreateCampaignFolderDialog {
    cy.get(selectByDataTestToolbarButtonId('create-button'))
      .click()
      .get(selectByDataTestMenuItemId('new-folder'))
      .click();
    return new CreateCampaignFolderDialog();
  }

  performAllChecksForCampaignLibrary() {
    const createButtonElement = this.createButton();
    const createMenuElement = createButtonElement.showMenu();
    const folderMenuItem = createMenuElement.item(CampaignMenuItemIds.newFolder);
    const campaignMenuItem = createMenuElement.item(CampaignMenuItemIds.newCampaign);
    const iterationMenuItem = createMenuElement.item(CampaignMenuItemIds.newIteration);
    const testSuiteMenuItem = createMenuElement.item(CampaignMenuItemIds.newTestSuite);
    const sprintMenuItem = createMenuElement.item(CampaignMenuItemIds.newSprint);
    const sprintGroupMenuItem = createMenuElement.item(CampaignMenuItemIds.newSprintGroup);

    folderMenuItem.assertEnabled();
    campaignMenuItem.assertEnabled();
    iterationMenuItem.assertDisabled();
    testSuiteMenuItem.assertDisabled();
    sprintMenuItem.assertEnabled();
    sprintGroupMenuItem.assertEnabled();
    createButtonElement.hideMenu();
    this.assertCopyButtonIsDisabled();
    this.assertDeleteButtonIsDisabled();
  }

  performAllChecksForCampaign() {
    const createButtonElement = this.createButton();
    const createMenuElement = createButtonElement.showMenu();
    const folderMenuItem = createMenuElement.item(CampaignMenuItemIds.newFolder);
    const campaignMenuItem = createMenuElement.item(CampaignMenuItemIds.newCampaign);
    const iterationMenuItem = createMenuElement.item(CampaignMenuItemIds.newIteration);
    const testSuiteMenuItem = createMenuElement.item(CampaignMenuItemIds.newTestSuite);
    const sprintMenuItem = createMenuElement.item(CampaignMenuItemIds.newSprint);
    const sprintGroupMenuItem = createMenuElement.item(CampaignMenuItemIds.newSprintGroup);

    folderMenuItem.assertDisabled();
    campaignMenuItem.assertEnabled();
    iterationMenuItem.assertEnabled();
    testSuiteMenuItem.assertDisabled();
    sprintMenuItem.assertDisabled();
    sprintGroupMenuItem.assertDisabled();
    createButtonElement.hideMenu();
    this.assertCopyButtonIsActive();
    this.assertDeleteButtonIsActive();
  }

  performAllChecksForIteration() {
    const createButtonElement = this.createButton();
    const createMenuElement = createButtonElement.showMenu();
    const folderMenuItem = createMenuElement.item(CampaignMenuItemIds.newFolder);
    const campaignMenuItem = createMenuElement.item(CampaignMenuItemIds.newCampaign);
    const iterationMenuItem = createMenuElement.item(CampaignMenuItemIds.newIteration);
    const testSuiteMenuItem = createMenuElement.item(CampaignMenuItemIds.newTestSuite);
    const sprintMenuItem = createMenuElement.item(CampaignMenuItemIds.newSprint);
    const sprintGroupMenuItem = createMenuElement.item(CampaignMenuItemIds.newSprintGroup);

    folderMenuItem.assertDisabled();
    campaignMenuItem.assertDisabled();
    iterationMenuItem.assertDisabled();
    testSuiteMenuItem.assertEnabled();
    sprintMenuItem.assertDisabled();
    sprintGroupMenuItem.assertDisabled();
    createButtonElement.hideMenu();
    this.assertCopyButtonIsActive();
    this.assertDeleteButtonIsActive();
  }

  assertAllMenuItemsDisabled() {
    const createButtonElement = this.createButton();
    const createMenuElement = createButtonElement.showMenu();
    const folderMenuItem = createMenuElement.item(CampaignMenuItemIds.newFolder);
    const campaignMenuItem = createMenuElement.item(CampaignMenuItemIds.newCampaign);
    const iterationMenuItem = createMenuElement.item(CampaignMenuItemIds.newIteration);
    const testSuiteMenuItem = createMenuElement.item(CampaignMenuItemIds.newTestSuite);
    const sprintMenuItem = createMenuElement.item(CampaignMenuItemIds.newSprint);
    const sprintGroupMenuItem = createMenuElement.item(CampaignMenuItemIds.newSprintGroup);

    folderMenuItem.assertDisabled();
    campaignMenuItem.assertDisabled();
    iterationMenuItem.assertDisabled();
    testSuiteMenuItem.assertDisabled();
    sprintMenuItem.assertDisabled();
    sprintGroupMenuItem.assertDisabled();
    createButtonElement.hideMenu();
  }
}
