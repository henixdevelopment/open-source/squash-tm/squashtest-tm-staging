import {
  ReferentialDataProvider,
  ReferentialDataProviderBuilder,
} from '../../../utils/referential/referential-data.provider';
import { GridElement, TreeElement } from '../../elements/grid/grid.element';
import { PageFactory } from '../page';
import { CampaignWorkspaceTreeMenu } from './campaign-workspace-tree-menu';
import { CreateCampaignDialog } from './dialogs/create-campaign-dialog.element';
import { CreateIterationDialog } from './dialogs/create-iteration-dialog.element';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { WorkspaceWithTreePage } from '../workspace-with-tree.page';
import { NavBarElement } from '../../elements/nav-bar/nav-bar.element';
import {
  getDefaultCampaignFolderStatisticsBundle,
  getDefaultCampaignStatisticsBundle,
  getEmptyCampaignStatisticsBundle,
} from '../../../data-mock/campaign.data-mock';
import {
  getDefaultIterationStatisticsBundle,
  getEmptyIterationStatisticsBundle,
} from '../../../data-mock/iteration.data-mock';
import { ReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { ItpiSearchPage } from './search/itpi-search-page';
import { MenuElement } from '../../../utils/menu.element';
import { ToolbarButtonElement } from '../../elements/workspace-common/toolbar.element';
import { CreateCampaignFolderDialog } from './dialogs/create-campaign-folder-dialog.element';
import { CreateSprintDialog } from './dialogs/create-sprint-dialog.element';
import { DeleteCampaignTreeNodesDialog } from './dialogs/delete-campaign-tree-nodes-dialog.element';

export enum CampaignMenuItemIds {
  newCampaign = 'new-campaign',
  newIteration = 'new-iteration',
  newTestSuite = 'new-suite',
  newFolder = 'new-folder',
  newSprint = 'new-sprint',
  newSprintGroup = 'new-sprint-group',
}

export class CampaignWorkspacePage extends WorkspaceWithTreePage {
  public readonly navBar: NavBarElement = new NavBarElement();
  public readonly treeMenu: CampaignWorkspaceTreeMenu;

  public constructor(public readonly tree: TreeElement) {
    super(tree, 'sqtm-app-campaign-workspace');
    this.treeMenu = new CampaignWorkspaceTreeMenu();

    this.declareStatisticsRoutes();
  }

  public static initTestAtPage: PageFactory<CampaignWorkspacePage> = (
    initialNodes: GridResponse = { dataRows: [] },
    referentialData?: ReferentialDataModel,
  ) => {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const tree = TreeElement.createTreeElement(
      'campaign-workspace-main-tree',
      'campaign-tree',
      initialNodes,
    );
    cy.visit('campaign-workspace');
    referentialDataProvider.wait();
    tree.waitInitialDataFetch();
    return new CampaignWorkspacePage(tree);
  };

  public static navigateToCampaignWorkspace(
    initialNodes?: GridResponse,
    referentialData?: ReferentialDataModel,
  ) {
    const referentialDataProvider: ReferentialDataProvider = new ReferentialDataProviderBuilder(
      referentialData,
    ).build();
    const tree = TreeElement.createTreeElement(
      'campaign-workspace-main-tree',
      'campaign-tree',
      initialNodes,
    );

    cy.visit('campaign-workspace');
    referentialDataProvider.wait();
    tree.waitInitialDataFetch();
    const page = new CampaignWorkspacePage(tree);
    page.assertExists();
    return page;
  }

  mockCampaignStatisticsRequest(statisticsBundle = getDefaultCampaignStatisticsBundle()) {
    return new HttpMockBuilder(`campaign-view/*/statistics`)
      .post()
      .responseBody(statisticsBundle)
      .build();
  }

  mockCustomReportFolderStatisticsRequest(
    statisticsBundle = getDefaultCampaignFolderStatisticsBundle(),
  ) {
    return new HttpMockBuilder('campaign-folder-view/*/statistics')
      .post()
      .responseBody(statisticsBundle)
      .build();
  }

  mockIterationViewStatisticsRequest(statisticsBundle = getDefaultIterationStatisticsBundle()) {
    return new HttpMockBuilder(`iteration-view/*/statistics`)
      .post()
      .responseBody(statisticsBundle)
      .build();
  }

  openCreateCampaign(): CreateCampaignDialog {
    const createButton = new ToolbarButtonElement(this.rootSelector, 'create-button');
    const menuCreationObjectCampaign = new MenuElement('create-menu');
    createButton.assertExists();
    createButton.clickWithoutSpan();
    menuCreationObjectCampaign.item('new-campaign').click();
    return new CreateCampaignDialog();
  }

  openCreateSprint(): CreateSprintDialog {
    const createButton = new ToolbarButtonElement(this.rootSelector, 'create-button');
    const menuCreationObjectCampaign = new MenuElement('create-menu');
    createButton.assertExists();
    createButton.clickWithoutSpan();
    menuCreationObjectCampaign.item('new-sprint').click();
    return new CreateSprintDialog();
  }

  openCreateSprintGroup(): CreateSprintDialog {
    const createButton = new ToolbarButtonElement(this.rootSelector, 'create-button');
    const menuCreationObjectCampaign = new MenuElement('create-menu');
    createButton.assertExists();
    createButton.clickWithoutSpan();
    menuCreationObjectCampaign.item('new-sprint-group').click();
    return new CreateSprintDialog();
  }

  openCreateIteration(): CreateIterationDialog {
    const createButton = new ToolbarButtonElement(this.rootSelector, 'create-button');
    const menuCreationObjectCampaign = new MenuElement('create-menu');
    createButton.assertExists();
    createButton.clickWithoutSpan();
    menuCreationObjectCampaign.item('new-iteration').click();
    return new CreateIterationDialog();
  }

  openCreateTestSuite(): CreateIterationDialog {
    const createButton = new ToolbarButtonElement(this.rootSelector, 'create-button');
    const menuCreationObjectCampaign = new MenuElement('create-menu');
    createButton.assertExists();
    createButton.clickWithoutSpan();
    menuCreationObjectCampaign.item('new-suite').click();
    return new CreateIterationDialog();
  }

  openCreateFolder(): CreateCampaignFolderDialog {
    const createButton = new ToolbarButtonElement(this.rootSelector, 'create-button');
    const menuCreationObjectCampaign = new MenuElement('create-menu');
    createButton.assertExists();
    createButton.clickWithoutSpan();
    menuCreationObjectCampaign.item('new-folder').click();
    return new CreateCampaignFolderDialog();
  }

  goToSearchItpiPage() {
    this.clickSearchButton();
    const gridItpi = new GridElement('itpi-search');
    return new ItpiSearchPage(gridItpi);
  }

  // Prevents missing stub errors. If you need to actually use these routes in your
  // tests, just redeclare them after the CampaignWorkspacePage was instantiated.
  private declareStatisticsRoutes(): void {
    new HttpMockBuilder('iteration-view/*/statistics')
      .responseBody(getEmptyIterationStatisticsBundle())
      .build();

    new HttpMockBuilder('campaign-view/*/statistics')
      .responseBody(getEmptyCampaignStatisticsBundle())
      .build();
  }

  deleteCampaignLibraryNode() {
    const deleteDialog = new DeleteCampaignTreeNodesDialog('confirm-delete');
    deleteDialog.assertExists();
    deleteDialog.clickOnConfirmButton();
    deleteDialog.assertNotExist();
  }

  toggleTreeElement(rowId: string) {
    this.tree.getTreeNodeCell(rowId).toggle();
  }

  selectTreeRowElement(rowId: string) {
    this.tree.getRow(rowId).findRow().click();
  }

  clickTreeDeleteButton() {
    this.treeMenu.button('delete-button').click();
  }

  assertRowNotExist(rowId: string) {
    this.tree.getRow(rowId).assertNotExist();
  }
}
