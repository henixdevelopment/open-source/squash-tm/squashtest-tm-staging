import { EntityViewPage } from '../../page';
import { apiBaseUrl } from '../../../../utils/mocks/request-mock';
import { CampaignStatisticsPanelElement } from '../campaign/campaign-statistics-panel.element';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';

export class CampaignLibraryViewPage extends EntityViewPage {
  readonly anchors = AnchorsElement.withLinkIds('dashboard');

  constructor(private campaignLibraryId: number | string) {
    super('sqtm-app-campaign-library-view');
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/campaign-library-view/${this.campaignLibraryId}?**`;
    cy.wait(`@${url}`);
  }

  showDashboard() {
    this.anchors.clickLink('dashboard');
    return new CampaignStatisticsPanelElement();
  }
}
