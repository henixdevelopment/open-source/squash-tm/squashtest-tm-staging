import { Page } from '../page';
import { selectByDataTestElementId } from '../../../utils/basic-selectors';
import { NgZorroTable } from '../../elements/ngzorro-table/NgZorroTable';
import { ExecutionStatusCount } from '../../../../../projects/sqtm-core/src/lib/model/campaign/campaign-model';
import { TestCaseImportanceKeys } from '../../../../../projects/sqtm-core/src/lib/model/level-enums/level-enum';

export class CampaignWorkspaceDashboardPage extends Page {
  inventoryTable: NgZorroTable = new NgZorroTable('inventory-table');

  constructor(rootSelector: string) {
    super(rootSelector);
  }

  private getFavoriteButton() {
    return this.findByElementId('favorite-button');
  }

  private getDefaultButton() {
    return this.findByElementId('default-button');
  }

  private getDashboardPanel() {
    return this.find('.ant-collapse-content-box');
  }

  private getCollapseHeader() {
    return this.find('.ant-collapse-header');
  }

  private getResizableGrid() {
    return this.find('.gridster-item-resizable-handler');
  }

  private getCrosshairCursor() {
    return this.find('.cursor-crosshair');
  }

  private getLegends() {
    return this.find('.legendtext');
  }

  private getDashboardBinding(bindingNumber: number) {
    return this.findByElementId(`custom-dashboard-binding-${bindingNumber}`);
  }

  protected assertChartIsRendered(selector: string) {
    cy.get(this.rootSelector).find(selector).should('exist').find('.plotly').should('exist');
  }
  assertStatusChartContainsStatus(status: string) {
    cy.get(this.rootSelector)
      .find(selectByDataTestElementId('status-chart'))
      .find('text.legendtext[data-unformatted="' + status + '"]')
      .should('exist');
  }

  assertStatusChartDoesNotContainStatus(status: string) {
    cy.get(this.rootSelector)
      .find(selectByDataTestElementId('status-chart'))
      .find('text.legendtext[data-unformatted="' + status + '"]')
      .should('not.exist');
  }

  assertStatsChartAreRendered() {
    this.assertChartIsRendered(selectByDataTestElementId('status-chart'));
    this.assertChartIsRendered(selectByDataTestElementId('conclusiveness-chart'));
    this.assertChartIsRendered(selectByDataTestElementId('importance-chart'));
  }

  assertErrorMessageOnEmptyTestPlanIsVisible() {
    this.findByElementId('empty-test-plan-error').should('be.visible');
  }

  checkStatusCount(rowIndex: number, expectedCounts: Partial<ExecutionStatusCount>) {
    Object.entries<number>(expectedCounts).forEach(([id, count]) => {
      this.inventoryTable
        .findRow(rowIndex)
        .assertExists()
        .getCell(id)
        .assertContainsText(count.toString());
    });
  }

  checkImportanceCount(
    rowIndex: number,
    expectedCounts: Partial<{ [K in TestCaseImportanceKeys]: number }>,
  ) {
    Object.entries<number>(expectedCounts).forEach(([id, count]) => {
      this.inventoryTable
        .findRow(rowIndex)
        .assertExists()
        .getCell(id)
        .assertContainsText(count.toString());
    });
  }

  assertAdvancementChartIsRendered() {
    this.assertChartIsRendered('sqtm-app-campaign-advancement-chart');

    this.findByElementId('iteration-scheduled-dates-error').should('not.exist');
  }

  assertErrorMessageOnDateIsVisible() {
    cy.get(this.rootSelector)
      .should('exist')
      .find('sqtm-app-campaign-advancement-chart')
      .should('not.exist');

    this.findByElementId('iteration-scheduled-dates-error').should('be.visible');
  }

  assertFavoriteButtonExist() {
    this.getFavoriteButton().should('exist');
  }

  assertDefaultButtonExist() {
    this.getDefaultButton().should('exist');
  }

  clickFavoriteButton() {
    this.getFavoriteButton().click();
  }

  clickDefaultButton() {
    this.getDefaultButton().click();
  }

  assertNoDashboardSelectedMessage() {
    this.getDashboardPanel()
      .find('span')
      .should(
        'contain.text',
        "Vous pouvez sélectionner un tableau de bord à afficher depuis l'espace Pilotage, en cliquant sur le bouton [Favori] d'un tableau de bord.",
      );
  }

  assertFavoriteDashboardIsEmptyMessage() {
    this.getDashboardPanel()
      .find('span')
      .should(
        'contain.text',
        "Votre tableau de bord favori est vide. Vous pouvez le remplir dans l'espace Pilotage.",
      );
  }

  assertTitleExist(expectedTitle: string) {
    this.getCollapseHeader().should('contain.text', expectedTitle);
  }

  assertCustomDashboardBinding(bindingNumbers: number[]) {
    bindingNumbers.forEach((bindingNumber) => {
      this.getDashboardBinding(bindingNumber).should('exist');
    });
  }

  assertCannotResizableElement() {
    this.getResizableGrid().should('not.exist');
  }

  assertCannotMoveElement(index: number) {
    this.getCrosshairCursor().eq(index).should('not.exist');
  }

  assertChartLegends(index: number, contentText: string) {
    this.getLegends().eq(index).should('contain.text', contentText);
  }

  assertChartLegendsNotExist() {
    this.getLegends().should('not.exist');
  }
}
