import { EntityViewPage } from '../../page';
import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';
import { apiBaseUrl } from '../../../../utils/mocks/request-mock';
import { selectByDataTestFieldId } from '../../../../utils/basic-selectors';
import { EditableRichTextFieldElement } from '../../../elements/forms/editable-rich-text-field.element';

export class SprintGroupViewPage extends EntityViewPage {
  readonly nameTextField = new EditableTextFieldElement('entity-name');

  constructor(public sprintGroupId?: number | string) {
    super('sqtm-app-sprint-group-view');
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/sprint-group-view/${this.sprintGroupId}?**`;
    cy.wait(`@${url}`);
  }

  private getCreatedSprintGroup() {
    return this.findByFieldId('sprint-group-created');
  }

  private getLastModifiedSprintGroup() {
    return this.findByFieldId('sprint-group-lastModified');
  }

  checkData(fieldId: string, value: string) {
    this.find(selectByDataTestFieldId(fieldId)).should('contain.text', value);
  }
  get descriptionRichField(): EditableRichTextFieldElement {
    const url = `sprint/${this.sprintGroupId}/description`;
    return new EditableRichTextFieldElement('sprint-group-description', url);
  }

  assertCreatedBy(expectedCreation: string) {
    this.getCreatedSprintGroup().should('contain.text', expectedCreation);
  }

  assertLastModificationBy(lastModifiedBy: string) {
    this.getLastModifiedSprintGroup().should('contain.text', lastModifiedBy);
  }
}
