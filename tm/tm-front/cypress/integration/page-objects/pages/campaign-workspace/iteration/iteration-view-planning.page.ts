import { Page } from '../../page';
import { EditableDateFieldElement } from '../../../elements/forms/editable-date-field.element';

export class IterationViewPlanningPage extends Page {
  readonly scheduledStartDateField = new EditableDateFieldElement('scheduledStartDate');
  readonly actualStartDateField = new EditableDateFieldElement('actualStartDate');
  readonly scheduledEndDateField = new EditableDateFieldElement('scheduledEndDate');
  readonly actualEndDateField = new EditableDateFieldElement('actualEndDate');

  constructor() {
    super('sqtm-app-iteration-view-planning-panel');
  }

  assertExists() {
    super.assertExists();
  }
}
