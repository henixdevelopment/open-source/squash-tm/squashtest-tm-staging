import { HttpMock, HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { ReferentialDataProviderBuilder } from '../../../../utils/referential/referential-data.provider';
import { EnvironmentSelectionPanelDto } from '../../../elements/automated-execution-environments/environment-selection-panel.element';
import { AlertDialogElement } from '../../../elements/dialog/alert-dialog.element';
import { GridElement, TreeElement } from '../../../elements/grid/grid.element';
import { ExecutionPage } from '../../execution/execution-page';
import { Page } from '../../page';
import { AutomatedTestsExecutionSupervisionDialogElement } from '../dialogs/automated-tests-execution-supervision-dialog.element';
import { CreateBindTestSuiteDialog } from '../dialogs/create-bind-test-suite.dialog';
import { ExecutionHistoryDialog } from '../dialogs/itpi-execution-history-dialog.element';
import { ItpiMultiEditDialogElement } from '../dialogs/itpi-multi-edit-dialog.element';
import { MassDeleteItpiDialogElement } from '../dialogs/mass-delete-itpi-dialog.element';
import { RemoveItpiDialogElement } from '../dialogs/remove-itpi-dialog.element';
import { ExecutionModel } from '../../../../../../projects/sqtm-core/src/lib/model/execution/execution.model';
import { AutomatedSuitePreview } from '../../../../../../projects/sqtm-core/src/lib/model/test-automation/automated-suite-preview.model';
import { EvInputType } from '../../../../../../projects/sqtm-core/src/lib/model/environment-variable/ev-input-type.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import {
  selectByDataTestButtonId,
  selectByDataTestCellId,
  selectByDataTestElementId,
  selectByDataTestLinkId,
  selectByDataTestMenuItemId,
} from '../../../../utils/basic-selectors';
import { mockGridResponse } from '../../../../data-mock/grid.data-mock';
import { ExecutionStatus } from '../../../../../../projects/sqtm-core/src/lib/model/level-enums/level-enum';
import { TestCaseForCampaignSearchPage } from '../../test-case-workspace/research/test-case-for-campaign-search-page';
import { MenuElement, MenuItemElement } from '../../../../utils/menu.element';
import { RequirementTestCaseForCampaignSearchPage } from '../../requirement-workspace/search/dialogs/requirement-test-case-for-campaign-search-page';
import { ExecutionRunnerStepPage } from '../../execution/execution-runner-step-page';
import { ExecutionRunnerProloguePage } from '../../execution/execution-runner-prologue-page';
import { SessionOverviewPage } from '../../session-overview/session-overview.page';
import { TestPlanPage } from '../test-plan-page';
import Chainable = Cypress.Chainable;

export class IterationTestPlanPage extends Page implements TestPlanPage {
  private constructor(
    rootSelector: string,
    public readonly testPlan: GridElement,
  ) {
    super(rootSelector);
  }

  static navigateTo(iterationId: number | string, testPlan: GridResponse): IterationTestPlanPage {
    iterationId = iterationId ?? '*';
    const url = `iteration/${iterationId}/test-plan`;
    const gridElement = GridElement.createGridElement('iteration-test-plan', url, testPlan);
    return new IterationTestPlanPage('sqtm-app-iteration-test-plan-execution', gridElement);
  }

  openTestCaseDrawer(response?: GridResponse): TreeElement {
    const treeElement = TreeElement.createTreeElement(
      'test-case-tree-picker',
      'test-case-tree',
      response,
    );
    cy.get('[data-test-button-id=show-test-case-picker]').click();
    treeElement.waitInitialDataFetch();
    return treeElement;
  }

  closeTestCaseDrawer() {
    cy.get('.ant-drawer-body .anticon-close').click();
  }

  showDeleteConfirmDialog(testPlanId: number, iterationId: number): RemoveItpiDialogElement {
    this.testPlan.scrollPosition('right', 'mainViewport');
    cy.get(`[data-test-row-id=${testPlanId}] [data-test-cell-id="delete"] i`).click();
    return new RemoveItpiDialogElement(testPlanId, iterationId);
  }

  showMassDeleteConfirmDialog(iterationId: number, itpiIds: number[]) {
    cy.get('[data-test-button-id=show-confirm-mass-delete-dialog]').click();
    return new MassDeleteItpiDialogElement(iterationId, itpiIds);
  }

  enterIntoTestPlan() {
    this.getDropZone().trigger('mouseenter', 'left', { force: true, buttons: 1 });
  }

  dropIntoTestPlan(
    iterationId: number | string,
    createdTestPlanItemIds?: number[],
    refreshedTestPlan?: GridResponse,
  ): Chainable<number[]> {
    const url = `iteration/${iterationId}/test-plan-items`;
    const mock = new HttpMockBuilder<{ itemTestPlanIds: number[] }>(url)
      .post()
      .responseBody({ itemTestPlanIds: createdTestPlanItemIds })
      .build();
    this.testPlan.declareRefreshData(refreshedTestPlan);
    this.getDropZone().trigger('mouseup', { force: true });
    return mock.waitResponseBody().then((responseBody) => {
      if (refreshedTestPlan) {
        this.testPlan.waitForRefresh();
      }
      return cy.wrap(responseBody.itemTestPlanIds);
    });
  }

  private getDropZone(): Chainable<JQuery<HTMLDivElement>> {
    return cy.get(`[data-test-element-id=${ITERATION_TEST_PLAN_DROP_ZONE_ID}]`);
  }

  verifyTestPlanItem(options: VerifyItemTestPlanOptions) {
    this.testPlan.assertRowExist(options.id);
    const row = this.testPlan.getRow(options.id);
    if (options.name) {
      if (options.showsAsLink) {
        row.cell('testCaseName').linkRenderer().assertContainText(options.name);
      } else {
        row.cell('testCaseName').textRenderer().assertContainsText(options.name);
      }
    }
  }

  assertColoredBorderIsVisible() {
    this.getDropZone().should('have.class', 'sqtm-core-border-current-workspace-color');
  }

  assertColoredBorderIsNotVisible() {
    this.getDropZone().should('not.have.class', 'sqtm-core-border-current-workspace-color');
  }

  clickOnPlayButton(itemTestPlanId: number) {
    this.testPlan.scrollPosition('right', 'mainViewport');
    const row = this.testPlan.getRow(itemTestPlanId, 'rightViewport');
    row.cell('startExecution').iconRenderer().click();
  }

  checkPlayMenu(numberOfOptions: number = 3) {
    cy.get('.launch-execution-link').should('have.length', numberOfOptions);
  }

  checkPlayMenuDontAllowNewExecution(): void {
    cy.get('.launch-execution-link').should('have.length', 3);
    cy.get('.launch-execution-link.disabled').should('have.length', 2);
  }

  applyFastpassOnItpi(itemId: string) {
    cy.get(`[data-test-cell-id=execution-status-cell]`).click();

    const mock = new HttpMockBuilder('test-plan-item/*/execution-status').post().build();
    const refreshMock = new HttpMockBuilder('campaign-tree/refresh').post().build();
    cy.get(`[data-test-item-id=item-${itemId}]`).trigger('click');
    mock.wait();
    refreshMock.wait();
  }

  changeDataset(itemId: string) {
    cy.get(`[data-test-cell-id=dataset-cell]`).click();

    const updatedDatasetMock = new HttpMockBuilder('test-plan-item/iteration/*/dataset')
      .post()
      .build();
    cy.get(`[data-test-item-id=${itemId}]`).click();
    updatedDatasetMock.wait();
  }

  // DEPRECATED: DO NOT USE ANYMORE
  openAnyExecutionHistoryDialog(data?: any): ExecutionHistoryDialog {
    const executionListMock = new HttpMockBuilder('iteration/*/test-plan/*/executions')
      .responseBody(data)
      .post()
      .build();
    cy.get(`[data-test-link-id=show-execution-history]`).click();
    executionListMock.waitResponseBody();
    return new ExecutionHistoryDialog(data);
  }

  openExecutionHistoryDialog(): ExecutionHistoryDialog {
    const executionListMock = new HttpMockBuilder('iteration/*/test-plan/*/executions')
      .responseBody({})
      .post()
      .build();
    cy.get(`[data-test-link-id=show-execution-history]`).click();
    executionListMock.waitResponseBody();
    return new ExecutionHistoryDialog(mockGridResponse('id', []));
  }

  // faking dialog open by clicking on new page execution
  launchExecutionInPageMode(
    iterationId: number,
    testPlanItemId: number,
    newExecutionId?: number,
    referentialData?: ReferentialDataModel,
    executionModel?: ExecutionModel,
  ): ExecutionPage {
    const url = `iteration/${iterationId}/test-plan/${testPlanItemId}/executions/new-manual`;
    const createExecutionMock = new HttpMockBuilder<{ executionId: number }>(url)
      .post()
      .responseBody({ executionId: newExecutionId })
      .build();
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const executionModelMock = new HttpMockBuilder<ExecutionModel>(`execution/*`)
      .responseBody(executionModel)
      .build();
    cy.get('[data-test-link-id=manual-execution]').click();
    createExecutionMock.waitResponseBody();
    referentialDataProvider.wait();
    executionModelMock.wait();
    return new ExecutionPage();
  }

  moveItemWithServerResponse(itemToMove: string, itemToDropOnto: string, response: any): void {
    const mock = new HttpMockBuilder('iteration/*/test-plan/*/position/*')
      .responseBody(response)
      .post()
      .build();

    this.startDrag(itemToMove);
    this.getDraggedContent()
      .should('be.visible')
      .find(selectByDataTestElementId('cannot-drag-icon'))
      .should('not.exist');
    this.internalDropOnto(itemToDropOnto);

    mock.wait();
  }

  assertCannotMoveItem(itemToMove: string): void {
    this.startDrag(itemToMove);
    this.getDraggedContent().find('[data-test-element-id="cannot-drag-icon"]').should('exist');
    this.internalDropOnto(itemToMove);
  }

  private startDrag(itemToDrag: string): void {
    this.testPlan
      .getRow(itemToDrag, 'leftViewport')
      .cell('#')
      .findCell()
      .find('span')
      .trigger('mousedown', { button: 0 })
      .trigger('mousemove', -20, -20, { force: true })
      .trigger('mousemove', 50, 50, { force: true });
  }

  private internalDropOnto(itemToDropOnto: string): void {
    this.testPlan
      .getRow(itemToDropOnto, 'leftViewport')
      .cell('#')
      .findCell()
      .trigger('mousemove', 50, 50, { force: true })
      .trigger('mouseup', { force: true });
  }

  private getDraggedContent() {
    return cy.get('sqtm-app-test-plan-dragged-content');
  }

  foldGrid() {
    cy.get(`[data-test-element-id="fold-tree-button"]`).click();
  }

  openMassEditDialog(): ItpiMultiEditDialogElement {
    cy.get('[data-test-button-id=mass-edit]').click();
    return new ItpiMultiEditDialogElement();
  }

  openCreateTestSuiteDialog(): CreateBindTestSuiteDialog {
    cy.get('[data-test-button-id=create-test-suite]').click();
    return new CreateBindTestSuiteDialog();
  }

  launchExecutionWithError(iterationId = '*'): AlertDialogElement {
    const mock = new HttpMockBuilder(`iteration/${iterationId}/test-plan/resume`)
      .post()
      .status(412)
      .responseBody(this.getEmptyTestPlanError())
      .build();
    cy.get('[data-test-button-id=launch-execution]').click();
    mock.wait();
    return new AlertDialogElement();
  }

  private getEmptyTestPlanError() {
    return {
      squashTMError: {
        kind: 'ACTION_ERROR',
        actionValidationError: {
          exception: 'EmptyIterationTestPlanException',
          i18nKey: 'sqtm-core.error.test-plan.empty',
          i18nParams: [],
        },
      },
    };
  }

  assertLaunchAutomatedMenuExist() {
    cy.get('[data-test-button-id="launch-automated"]').should('exist').click();
    cy.get('a.launch-automated-link').should('have.length', 2);
  }

  assertLaunchAutomatedMenuIsNotVisible() {
    cy.get('[data-test-button-id="launch-automated"]')
      .find('a.launch-automated-link')
      .should('not.exist');
  }

  openAutoTestExecDialogViaLaunchAllButton(
    automatedSuitePreview: AutomatedSuitePreview,
    environmentPanelResponses?: EnvironmentSelectionPanelDto[],
  ): AutomatedTestsExecutionSupervisionDialogElement {
    this.clickOnLaunchAutoTestsIcon();
    cy.get('a.launch-automated-link').should('have.length', 2);

    const resolveTaScriptsRequest = new HttpMockBuilder(
      'automation-requests/associate-TA-script?iterationId=*',
    )
      .post()
      .build();
    const automatedSuitePreviewRequest = new HttpMockBuilder('automated-suites/preview')
      .post()
      .responseBody(automatedSuitePreview)
      .build();

    let loadEnvironmentMocks: HttpMock<EnvironmentSelectionPanelDto>[];
    if (environmentPanelResponses) {
      loadEnvironmentMocks = environmentPanelResponses.map((panel) =>
        new HttpMockBuilder<EnvironmentSelectionPanelDto>(
          `test-automation/${panel.project.projectId}/automated-execution-environments/all?frontEndErrorIsHandled=true`,
        )
          .get()
          .responseBody(panel)
          .build(),
      );
    } else {
      loadEnvironmentMocks = [];
    }

    let environmentVariablePanels: HttpMock<any>[];
    if (automatedSuitePreview.squashAutomProjects.length > 0) {
      environmentVariablePanels = automatedSuitePreview.squashAutomProjects.map((_project) =>
        new HttpMockBuilder('bound-environment-variables/project/*/iteration/*')
          .get()
          .responseBody(this.getAutomatedEnvironmentPanelResponse())
          .build(),
      );
    } else {
      environmentVariablePanels = [];
    }

    cy.get('a.launch-automated-link')
      .contains('Lancer tous les tests automatisés')
      .should('exist')
      .click();

    resolveTaScriptsRequest.wait();
    automatedSuitePreviewRequest.wait();
    loadEnvironmentMocks.forEach((mock) => mock.wait());
    environmentVariablePanels.forEach((evMock) => evMock.wait());

    const executionDialog = new AutomatedTestsExecutionSupervisionDialogElement();
    executionDialog.assertExists();
    return executionDialog;
  }

  private clickOnLaunchAutoTestsIcon() {
    cy.get('[data-test-button-id="launch-automated"]').should('exist').click();
  }

  private getAutomatedEnvironmentPanelResponse() {
    return {
      boundEnvironmentVariables: [
        {
          id: 1,
          name: 'Simple EV1',
          inputType: EvInputType.PLAIN_TEXT,
          boundToServer: true,
          value: 'toto',
          options: [],
          code: 'Simple EV1',
        },
        {
          id: 2,
          name: 'Dropdown EV1',
          code: 'SSEV1',
          inputType: EvInputType.DROPDOWN_LIST,
          boundToServer: true,
          value: 'OPT1',
          options: [
            {
              label: 'OPT1',
              code: 'OPT1',
              evId: 2,
              position: 0,
            },
            {
              label: 'OPT2',
              code: 'OPT2',
              evId: 2,
              position: 1,
            },
          ],
        },
      ],
    };
  }

  resumeExecution(withFilter: boolean = false, iterationId: string = '*') {
    const expectedUrl = withFilter
      ? `iteration/${iterationId}/test-plan/resume-filtered-selection`
      : `iteration/${iterationId}/test-plan/resume`;

    const mock = new HttpMockBuilder(expectedUrl).post().build();
    cy.get('[data-test-button-id=launch-execution]').click();
    mock.wait();
  }

  checkPlayMenuForManualExecutionMode(itemTestPlanId: string) {
    this.openPlayMenu(itemTestPlanId);
    this.checkPlayMenuOptionsForManual();
  }

  checkPlayMenuForAutomatedExecutionMode(itemTestPlanId: string) {
    this.openPlayMenu(itemTestPlanId);
    this.checkPlayMenuOptionsForAutomated();
  }

  moveItemWithoutServerResponse(itemToMove: string, itemToDropOnto: string) {
    this.startDrag(itemToMove);
    this.getDraggedContent()
      .should('be.visible')
      .find(selectByDataTestElementId('cannot-drag-icon'))
      .should('not.exist');
    this.internalDropOnto(itemToDropOnto);
  }

  public openPlayMenu(itemTestPlanId: string) {
    const row = this.testPlan.getRow(itemTestPlanId, 'rightViewport');
    row.cell('startExecution').iconRenderer().click();
    cy.get(selectByDataTestMenuItemId('play-exec'));
  }

  private checkPlayMenuOptionsForManual() {
    cy.get(selectByDataTestLinkId('popup-manual-execution')).contains('Avec la pop-up');
    cy.get(selectByDataTestLinkId('manual-execution')).contains('Nouvelle exécution');
    cy.get(selectByDataTestLinkId('show-execution-history')).contains('Historique des exécutions');
  }

  private checkPlayMenuOptionsForAutomated() {
    cy.get(selectByDataTestLinkId('automated-execution')).contains('Exécution automatique');
    this.checkPlayMenuOptionsForManual();
  }

  checkExecutionsSummary(testPlan: GridResponse) {
    const data = testPlan.dataRows[0].data;

    cy.get(selectByDataTestCellId('execution-status-cell')).should(
      'have.css',
      'background-color',
      ExecutionStatus[data.executionStatus].backgroundColor,
    );

    for (let i = 0; i < data.lastExecutionStatuses.length; i++) {
      cy.get(selectByDataTestElementId(`execution-status-history-${i}`)).should(
        'have.css',
        'background-color',
        ExecutionStatus[data.lastExecutionStatuses[i].executionStatus].backgroundColor,
      );
    }
  }

  private openSearchMenu() {
    cy.get(selectByDataTestButtonId('search')).click();
    return new MenuElement('search-menu');
  }

  goToSearchTestCase() {
    const searchMenu = this.openSearchMenu();
    const grid = new GridElement('test-case-search');

    searchMenu.item('search-testcase').click();
    return new TestCaseForCampaignSearchPage(grid);
  }

  goToRequirementLinkedSearch() {
    const searchMenu = this.openSearchMenu();
    const grid = new GridElement('tc-by-requirement');

    searchMenu.item('search-tc-by-requirement').click();
    return new RequirementTestCaseForCampaignSearchPage(grid);
  }

  private openExecutionMenuInTestPlan(itemName: string): MenuItemElement {
    this.testPlan.findRowId('testCaseName', itemName).then((idItem) => {
      this.testPlan.getCell(idItem, 'startExecution', 'rightViewport').iconRenderer().click();
    });
    return new MenuItemElement('', 'play-exec');
  }

  createNewExecutionOfItem(itemName: string) {
    const menuItem = this.openExecutionMenuInTestPlan(itemName);
    menuItem.selectLinkElement('manual-execution');
    return new ExecutionPage();
  }

  openStartOverDialog() {
    cy.get(this.rootSelector).find(selectByDataTestButtonId('relaunch-execution')).click();
  }

  private clickOnButtonResumeExecution() {
    return cy.get(this.rootSelector).find(selectByDataTestButtonId('resume-execution')).click();
  }

  private clickOnButtonLaunchExecution() {
    return cy.get(this.rootSelector).find(selectByDataTestButtonId('launch-execution')).click();
  }

  clickButtonResumeExecution(
    idIteration: string,
    idTestPlan: string,
    idExecution: string,
    idStep: string,
  ) {
    cy.window().then(() => {
      this.clickOnButtonResumeExecution();
      cy.visit(
        Cypress.config().baseUrl +
          `/execution-runner/iteration/${idIteration}/test-plan/${idTestPlan}/execution/${idExecution}/step/${idStep}`,
      );
    });
    return new ExecutionRunnerStepPage();
  }

  openExecutionWithPopUp(item: string, idExecution: string) {
    const menuItem = this.openExecutionMenuInTestPlan(item);
    cy.window().then(() => {
      menuItem.selectLinkElement('popup-manual-execution');
      cy.visit(Cypress.config().baseUrl + `/execution-runner/execution/${idExecution}/prologue`);
    });
    return new ExecutionRunnerProloguePage();
  }

  accessToSession(itemName: string) {
    this.testPlan.findRowId('testCaseName', itemName).then((idItem) => {
      this.testPlan.getCell(idItem, 'startExecution', 'rightViewport').iconRenderer().click();
    });
    return new SessionOverviewPage();
  }

  clickButtonLaunchExecution(
    idIteration: string,
    idTestPlan: string,
    idExecution: string,
    idStep: string,
  ) {
    cy.window().then(() => {
      this.clickOnButtonLaunchExecution();
      cy.visit(
        `${Cypress.config().baseUrl}/execution-runner/iteration/${idIteration}/test-plan/${idTestPlan}/execution/${idExecution}/step/${idStep}`,
      );
    });
    return new ExecutionRunnerStepPage();
  }
}

export interface VerifyItemTestPlanOptions {
  id: number;
  name?: string;
  showsAsLink: boolean;
}

const ITERATION_TEST_PLAN_DROP_ZONE_ID = 'iterationTestPlanDropZone';
