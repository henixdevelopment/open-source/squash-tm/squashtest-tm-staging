import { GridElement } from '../../../elements/grid/grid.element';
import { IssuePage } from '../../../elements/issues/issue.page';

export class IterationViewIssuesPage extends IssuePage {
  constructor() {
    super('sqtm-app-iteration-issues');
  }

  get issueGrid() {
    return new GridElement('iteration-view-issue');
  }
}
