import { IterationTestPlanPage } from './iteration-test-plan.page';
import { EditableRichTextFieldElement } from '../../../elements/forms/editable-rich-text-field.element';
import { apiBaseUrl, HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { IterationViewInformationPage } from './iteration-view-information.page';
import { IterationViewStatisticsPage } from './iteration-view-statistics.page';
import { IterationViewIssuesPage } from './iteration-view-issues.page';
import { IterationViewDashboardPage } from './iteration-view-dashboard.page';
import { IterationAutomatedSuitePage } from './iteration-automated-suite.page';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';
import { AbstractTestPlanPage } from '../abstract-test-plan.page';
import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';
import { IterationViewPlanningPage } from './iteration-view-planning.page';
import { EnvironmentsStatusesCountElement } from '../../../elements/automated-execution-environments/environments-statuses-count.element';
import { selectByDataTestButtonId } from '../../../../utils/basic-selectors';
import { ItpiMultiEditDialogElement } from '../dialogs/itpi-multi-edit-dialog.element';

export class IterationViewPage extends AbstractTestPlanPage {
  descriptionRichField = new EditableRichTextFieldElement('iteration-description');
  informationPanel = new IterationViewInformationPage(this);
  referenceField = new EditableTextFieldElement('entity-reference');
  nameTextField = new EditableTextFieldElement('entity-name');
  environmentsStatusesCountElement = new EnvironmentsStatusesCountElement();

  readonly anchors = AnchorsElement.withLinkIds(
    'dashboard',
    'plan-exec',
    'statistics',
    'information',
    'automated-suite',
    'issues',
    'planning',
  );

  // For end to end test iterationId is not required
  constructor(public iterationId?: number | string) {
    super('sqtm-app-iteration-view');
  }
  get massEditButton() {
    return this.find(selectByDataTestButtonId('mass-edit'));
  }
  public checkDataFetched() {
    const url = `${apiBaseUrl()}/iteration-view/${this.iterationId}?**`;
    cy.wait(`@${url}`);
  }

  checkData(fieldId: string, value: string) {
    cy.get(`[data-test-field-id=${fieldId}] span`).should('contain.text', value);
  }

  clickDashboardAnchorLink(): IterationViewDashboardPage {
    this.anchors.clickLink('dashboard');
    return new IterationViewDashboardPage(this);
  }

  clickStatisticsAnchorLink(): IterationViewStatisticsPage {
    this.anchors.clickLink('statistics');
    return new IterationViewStatisticsPage(this);
  }

  clickInformationAnchorLink(): IterationViewInformationPage {
    this.anchors.clickLink('information');
    return new IterationViewInformationPage(this);
  }

  showTestPlan(testPlan?: GridResponse): IterationTestPlanPage {
    const iterationTestPlanPage = IterationTestPlanPage.navigateTo(this.iterationId, testPlan);
    this.anchors.clickLink('plan-exec');
    if (this.iterationId) {
      iterationTestPlanPage.testPlan.waitInitialDataFetch();
    }
    return iterationTestPlanPage;
  }

  clickAutomatedSuitePageAnchorLink(data: any): IterationAutomatedSuitePage {
    const iterationAutomatedSuite = IterationAutomatedSuitePage.navigateTo(this.iterationId, data);
    this.anchors.clickLink('automated-suite');
    iterationAutomatedSuite.testPlan.waitInitialDataFetch();
    return iterationAutomatedSuite;
  }

  showIssuesWithoutBindingToBugTracker(response?: any): IterationViewIssuesPage {
    const httpMock = new HttpMockBuilder('issues/iteration/*?frontEndErrorIsHandled=true')
      .responseBody(response)
      .build();
    this.anchors.clickLink('issues');
    httpMock.wait();
    cy.removeNzTooltip();
    return new IterationViewIssuesPage();
  }

  showIssuesIfBoundToBugTracker(
    modelResponse?: any,
    gridResponse?: GridResponse,
  ): IterationViewIssuesPage {
    const bugTrackerModel = new HttpMockBuilder('issues/iteration/*?frontEndErrorIsHandled=true')
      .responseBody(modelResponse)
      .build();
    const issues = new HttpMockBuilder('issues/iteration/*/known-issues')
      .responseBody(gridResponse)
      .post()
      .build();
    this.anchors.clickLink('issues');
    bugTrackerModel.wait();
    issues.wait();
    cy.removeNzTooltip();
    return new IterationViewIssuesPage();
  }

  clickPlanningAnchorLink(): IterationViewPlanningPage {
    this.anchors.clickLink('planning');
    return new IterationViewPlanningPage();
  }

  clickMassEditButton() {
    this.massEditButton.click();
    return new ItpiMultiEditDialogElement();
  }
}
