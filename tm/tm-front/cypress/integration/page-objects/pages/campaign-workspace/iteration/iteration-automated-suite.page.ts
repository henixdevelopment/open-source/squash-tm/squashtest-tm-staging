import { Page } from '../../page';
import { GridElement } from '../../../elements/grid/grid.element';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import {
  selectByDataTestButtonId,
  selectByDataTestDialogButtonId,
  selectByDataTestDialogId,
  selectByDataTestLinkId,
} from '../../../../utils/basic-selectors';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { TestExecutionInfo } from '../../../../../../projects/sqtm-core/src/lib/model/execution/test-execution-info.model';
import { AutomatedSuiteExecutionComparisonDialog } from '../dialogs/automated-suite-execution-comparison-dialog';
import { GridRowElement } from '../../../../utils/grid-selectors.builder';
import { SimpleDeleteConfirmDialogElement } from '../../../elements/dialog/simple-delete-confirm-dialog.element';
import { WorkspaceColor } from '../../../../utils/color.utils';
import { WorkflowLogDto } from '../dialogs/squash-orchestrator-workflow-log-viewer.element';
import { AutomatedSuiteDetailsDialogElement } from '../dialogs/automated-suite-details-dialog.element';

export class IterationAutomatedSuitePage extends Page {
  private constructor(
    rootSelector: string,
    public readonly testPlan: GridElement,
    private iterationId: number | string,
  ) {
    super(rootSelector);
  }

  static navigateTo(
    iterationId: number | string,
    automatedSuites: GridResponse,
  ): IterationAutomatedSuitePage {
    iterationId = iterationId ?? '*';
    const url = `iteration/${iterationId}/automated-suite`;
    const gridElement = GridElement.createGridElement(
      'iteration-automated-suite',
      url,
      automatedSuites,
    );
    return new IterationAutomatedSuitePage(
      'sqtm-app-iteration-automated-suite',
      gridElement,
      iterationId,
    );
  }

  public assertCompareExecutionButtonIsDisabled() {
    cy.get(`${selectByDataTestButtonId('compare-executions')}>span`)
      .should('exist')
      .should('have.class', 'label-color');
  }

  public assertCompareExecutionButtonIsNotVisible() {
    cy.get(selectByDataTestButtonId('compare-executions')).should('not.exist');
  }

  public assertStopWorkflowButtonIsDisabled(row: GridRowElement): void {
    row
      .cell('stopWorkflows')
      .findCell()
      .get(selectByDataTestButtonId('stop-workflow'))
      .should('have.css', 'color', 'rgb(98, 115, 127)');
  }

  clickCompareExecutionButton(response: TestExecutionInfo[]) {
    const mockCompare = new HttpMockBuilder('automated-suites/compare-executions')
      .post()
      .responseBody(response)
      .build();

    cy.get(`${selectByDataTestButtonId('compare-executions')}>span`)
      .should('not.have.class', 'label-color')
      .click();

    mockCompare.wait();

    return new AutomatedSuiteExecutionComparisonDialog();
  }

  confirmStopWorkflow(row: GridRowElement, response: GridResponse) {
    const stopWorkflowMock = new HttpMockBuilder(`automated-suite/*/stop-workflows`)
      .post()
      .responseBody(false)
      .build();

    const refreshGridMock = new HttpMockBuilder(`iteration/*/automated-suite`)
      .post()
      .responseBody(response)
      .build();

    cy.get('sqtm-core-confirm-dialog').find(selectByDataTestDialogButtonId('confirm')).click();

    stopWorkflowMock.wait();
    refreshGridMock.wait();

    row
      .cell('stopWorkflows')
      .findCell()
      .get(selectByDataTestButtonId('stop-workflow'))
      .should('not.have.css', 'color', WorkspaceColor.CAMPAIGN);
  }

  openStopWorkflowConfirmDialog(row: GridRowElement) {
    const iconRenderer = row.cell('stopWorkflows').iconRenderer();
    iconRenderer.assertIsVisible();
    row
      .cell('stopWorkflows')
      .findCell()
      .get(selectByDataTestButtonId('stop-workflow'))
      .should('have.css', 'color', WorkspaceColor.CAMPAIGN);
    iconRenderer.click();
  }

  deleteAutomatedSuite(suiteId: string, response: GridResponse) {
    this.testPlan.getRow(suiteId, 'rightViewport').cell('delete').iconRenderer().click();

    const deleteMock = new HttpMockBuilder('automated-suites/deletion').post().build();
    const refreshGridMock = new HttpMockBuilder(`iteration/*/automated-suite`)
      .post()
      .responseBody(response)
      .build();

    const dialog = new SimpleDeleteConfirmDialogElement();
    dialog.clickOnConfirmButton();
    deleteMock.wait();
    refreshGridMock.wait();
  }

  deleteMassRows(suiteIds: string[]) {
    this.testPlan.selectRows(suiteIds, '#', 'leftViewport');
    cy.get(selectByDataTestButtonId('show-confirm-mass-delete-dialog')).click();

    const deleteMock = new HttpMockBuilder('automated-suites/deletion').post().build();
    const refreshGridMock = new HttpMockBuilder(`iteration/*/automated-suite`)
      .post()
      .responseBody({
        count: 0,
        idAttribute: null,
        dataRows: [],
        activeColumnIds: [],
        page: 0,
      })
      .build();

    const dialog = new SimpleDeleteConfirmDialogElement();
    dialog.clickOnConfirmButton();
    deleteMock.wait();
    refreshGridMock.wait();
  }

  pruneMassRows(suiteIds: string[]) {
    this.testPlan.selectRows(suiteIds, '#', 'leftViewport');
    cy.get(selectByDataTestButtonId('show-confirm-mass-prune-dialog')).click();
    cy.get('a.prune-link').should('have.length', 2);
    cy.get(selectByDataTestLinkId('complete-prune')).should('exist').click({ force: true });

    const mock = new HttpMockBuilder('automated-suites/attachment-prune?complete=true')
      .post()
      .build();

    cy.get(selectByDataTestDialogId('sqtm-app-automated-suite-attachment-prune'))
      .should('exist')
      .get(selectByDataTestDialogButtonId('confirm'))
      .click();

    mock.wait();
  }

  pruneAutomatedSuite(row: GridRowElement) {
    const iconRenderer = row.cell('attachmentPrune').iconRenderer();
    iconRenderer.assertIsVisible();
    iconRenderer.click();
    cy.get('a.prune-link').should('have.length', 2);
    cy.get(selectByDataTestLinkId('complete-prune')).should('exist').click({ force: true });

    const mock = new HttpMockBuilder('automated-suite/*/attachment-prune?complete=true')
      .post()
      .build();

    cy.get(selectByDataTestDialogId('sqtm-app-automated-suite-attachment-prune'))
      .should('exist')
      .get(selectByDataTestDialogButtonId('confirm'))
      .click();

    mock.wait();
  }

  public assertAutomatedSuiteDetailsButtonDoesNotExist() {
    cy.get(selectByDataTestButtonId('automated-suite-details-button')).should('not.exist');
  }

  openAutomatedSuiteDetails(
    row: GridRowElement,
    autoSuiteId: string,
    executions: GridResponse,
  ): AutomatedSuiteDetailsDialogElement {
    const iconRenderer = row.cell('executionDetails').iconRenderer();
    iconRenderer.assertIsVisible();
    const automatedSuiteExecutions = AutomatedSuiteDetailsDialogElement.navigateTo(
      autoSuiteId,
      executions,
    );
    iconRenderer.click();
    automatedSuiteExecutions.executions.waitInitialDataFetch();
    return automatedSuiteExecutions;
  }

  public assertWorkflowsLogButtonDoesNotExist() {
    cy.get(selectByDataTestButtonId('workflow-logs')).should('not.exist');
  }

  public assertWorkflowsLogButtonIsDisabled() {
    cy.get(selectByDataTestButtonId('workflow-logs'))
      .should('exist')
      .should('have.css', 'color', 'rgb(98, 115, 127)');
  }

  public assertWorkflowsLogButtonIsEnabled() {
    cy.get(selectByDataTestButtonId('workflow-logs'))
      .should('exist')
      .should('have.css', 'color', WorkspaceColor.CAMPAIGN);
  }

  openWorkflowLogViewer(row: GridRowElement, response: WorkflowLogDto) {
    const iconRenderer = row.cell('workflowViewer').iconRenderer();
    iconRenderer.assertIsVisible();
    const logViewerMock = new HttpMockBuilder(`workflows/*/*/logs`)
      .get()
      .responseBody(response)
      .status(200)
      .build();
    iconRenderer.click();
    logViewerMock.wait();
  }

  openWorkflowsMenu(row: GridRowElement) {
    const iconRenderer = row.cell('workflowViewer').iconRenderer();
    iconRenderer.assertIsVisible();
    iconRenderer.click();
  }

  openViewerForWorkflow(workflowUuid: string, response: WorkflowLogDto) {
    const logViewerMock = new HttpMockBuilder(`workflows/*/*/logs`)
      .get()
      .responseBody(response)
      .status(200)
      .build();
    cy.get(selectByDataTestLinkId('workflow-log-' + workflowUuid)).click();
    logViewerMock.wait();
  }
}
