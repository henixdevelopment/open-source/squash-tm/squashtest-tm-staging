import { Page } from '../../page';
import { SprintViewPage } from './sprint-view.page';
import { EditableRichTextFieldElement } from '../../../elements/forms/editable-rich-text-field.element';

export class SprintViewInformationPage extends Page {
  constructor(private parentPage: SprintViewPage) {
    super('sqtm-app-sprint-information-panel');
  }

  get sprintId(): number | string {
    return this.parentPage.sprintId;
  }

  get descriptionRichField(): EditableRichTextFieldElement {
    const url = `sprint/${this.sprintId}/description`;
    return new EditableRichTextFieldElement('sprint-description', url);
  }
}
