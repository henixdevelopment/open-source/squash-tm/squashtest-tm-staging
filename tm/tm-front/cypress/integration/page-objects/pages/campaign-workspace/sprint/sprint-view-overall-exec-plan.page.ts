import { GridElement } from '../../../elements/grid/grid.element';
import { Page } from '../../page';

export class SprintViewOverallExecPlanPage extends Page {
  constructor() {
    super('sqtm-app-sprint-view-overall-exec-plan');
  }

  get sprintOverallExecPlanGrid() {
    return new GridElement('sprint-view-overall-exec-plan');
  }
}
