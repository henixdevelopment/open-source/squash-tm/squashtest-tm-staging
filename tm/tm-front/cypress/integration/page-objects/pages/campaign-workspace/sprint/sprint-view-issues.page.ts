import { GridElement } from '../../../elements/grid/grid.element';
import { IssuePage } from '../../../elements/issues/issue.page';
import { selectByDataTestToolbarButtonId } from '../../../../utils/basic-selectors';
import { ExportSprintIssuesDialogElement } from '../dialogs/export-sprint-issues-dialog.element';

export class SprintViewIssuesPage extends IssuePage {
  constructor() {
    super('sqtm-app-sprint-issues-panel');
  }

  get sprintIssuesGrid() {
    return new GridElement('sprint-view-issue');
  }

  openExportDialog(): ExportSprintIssuesDialogElement {
    cy.get(selectByDataTestToolbarButtonId('export-issue-button')).click();
    return new ExportSprintIssuesDialogElement();
  }
}
