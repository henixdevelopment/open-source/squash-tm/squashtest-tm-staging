import { GridElement } from '../../../elements/grid/grid.element';
import { Page } from '../../page';

export class SprintViewReqVersionsLinkedPage extends Page {
  constructor() {
    super('sqtm-app-sprint-req-versions-linked');
  }

  get sprintReqVersionsGrid() {
    return new GridElement('sprint-req-versions-linked');
  }
}
