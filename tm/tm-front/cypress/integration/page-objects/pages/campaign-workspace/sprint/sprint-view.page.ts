import { AnchorsElement } from '../../../elements/anchor/anchors.element';
import { EntityViewPage } from '../../page';
import { apiBaseUrl, HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';
import { SprintViewReqVersionsLinkedPage } from './sprint-view-req-versions-linked.page';
import { GridElement, TreeElement } from '../../../elements/grid/grid.element';
import {
  selectByDataTestButtonId,
  selectByDataTestCapsuleId,
  selectByDataTestElementId,
  selectByDataTestFieldId,
  selectByDataTestIconId,
} from '../../../../utils/basic-selectors';
import { SprintViewInformationPage } from './sprint-view-information.page';
import { BindReqVersionToSprintOperationReport } from '../../../../../../projects/sqtm-core/src/lib/model/change-coverage-operation-report';
import { SprintReqVersionViewPage } from '../sprint-req-version/sprint-req-version-view.page';
import { mockSprintReqVersionModel } from '../../../../data-mock/sprint-req-version.data-mock';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { SprintReqVersionTestPlanItem } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/sprint-req-version-test-plan-item.model';
import { getRgbColorFromHex } from '../../../../utils/color.utils';
import { RemoveSprintReqVersionDialogElement } from '../dialogs/remove-sprint-req-version-dialog.element';
import { SprintViewIssuesPage } from './sprint-view-issues.page';
import { SprintReqVersionViewModel } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/sprint-req-version-view-model';
import { SprintViewOverallExecPlanPage } from './sprint-view-overall-exec-plan.page';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { BaseDialogElement } from '../../../elements/dialog/base-dialog.element';
import { SprintStatus } from '../../../../../../projects/sqtm-core/src/lib/model/level-enums/level-enum';
import Chainable = Cypress.Chainable;
import { EditableTimeFieldElement } from '../../../elements/forms/editable-time-field.element';

export class SprintViewPage extends EntityViewPage {
  readonly nameTextField = new EditableTextFieldElement('entity-name');
  readonly referenceField = new EditableTextFieldElement('entity-reference');
  readonly informationPanel = new SprintViewInformationPage(this);
  readonly sprintReqVersionsTable = new GridElement('sprint-req-versions-linked');
  readonly startStopButton = selectByDataTestButtonId('start-stop-sprint');
  readonly addRequirementsButton = selectByDataTestButtonId('show-requirement-picker');
  readonly searchRequirementsButton = selectByDataTestIconId('search-coverages');
  readonly deleteSprintReqVersionsButton = selectByDataTestIconId('remove-sprint-req-versions');
  readonly differentStatusMessage = selectByDataTestFieldId('different-status-message');

  get treeNodeLeftBorder() {
    return cy.get(selectByDataTestElementId('sprint-node-left-border'));
  }

  get statusCapsule() {
    return cy.get(selectByDataTestCapsuleId('status'));
  }

  get statusCapsuleIcon() {
    return this.statusCapsule.find('i');
  }

  get statusIcon() {
    return cy.get(selectByDataTestFieldId('sprint-squash-status'));
  }

  private getCreatedSprint() {
    return this.findByFieldId('sprint-created');
  }

  private getLastModifiedSprint() {
    return this.findByFieldId('sprint-lastModified');
  }

  private getStartDateSprint() {
    return new EditableTimeFieldElement('sprint-startDate', 'sprint/*/startDate');
  }

  private getEndDateSprint() {
    return new EditableTimeFieldElement('sprint-endDate', 'sprint/*/endDate');
  }

  readonly anchors = AnchorsElement.withLinkIds(
    'information',
    'linked-requirements',
    'issues',
    'overall-execution-plan',
  );

  constructor(public sprintId?: number | string) {
    super('sqtm-app-sprint-view');
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/sprint-view/${this.sprintId}?**`;
    cy.wait(`@${url}`);
  }

  checkData(fieldId: string, value: string) {
    this.find(selectByDataTestFieldId(fieldId)).should('contain.text', value);
  }

  checkStatusCapsuleExists() {
    this.statusCapsule.should('be.visible');
  }

  checkStatusCapsuleContains(expectedText: string) {
    this.statusCapsule.should('contain.text', expectedText);
  }

  checkStatusCapsuleIconExists() {
    this.statusCapsuleIcon.should('exist');
  }

  checkStatusCapsuleIconColorIs(expectedColor: string) {
    const rgbColorStrFormat: string = getRgbColorFromHex(expectedColor);
    this.statusCapsuleIcon.should('have.css', 'color', rgbColorStrFormat);
  }

  checkTooltipContains(element: Chainable<any>, expectedText: string) {
    element.trigger('mouseenter');
    cy.get('.cdk-overlay-pane').should('be.visible');
    cy.get('.cdk-overlay-pane').should('contain.text', expectedText);
  }

  checkAddButtonExists() {
    this.find(this.addRequirementsButton).should('exist');
  }

  checkAddButtonNotExists() {
    this.find(this.addRequirementsButton).should('not.exist');
  }

  checkSearchButtonExists() {
    this.find(this.searchRequirementsButton).should('exist');
  }

  checkDifferentStatusMessageExists() {
    this.find(this.differentStatusMessage).should('exist');
  }

  checkDifferentStatusMessageNotExists() {
    this.find(this.differentStatusMessage).should('not.exist');
  }

  checkSearchButtonNotExists() {
    this.find(this.searchRequirementsButton).should('not.exist');
  }

  checkDeleteSprintReqVersionsButtonExists() {
    this.find(this.deleteSprintReqVersionsButton).should('exist');
  }

  checkDeleteSprintReqVersionsButtonNotExists() {
    this.find(this.deleteSprintReqVersionsButton).should('not.exist');
  }

  checkStartStopSprintButtonExists() {
    this.find(this.startStopButton).should('be.visible');
  }

  checkStartStopSprintButtonContains(expectedText: string) {
    this.find(this.startStopButton).should('contain.text', expectedText);
  }

  clickStartStopButton(initialNodes: GridResponse, newStatus: string) {
    const sprintNode = initialNodes.dataRows[1];
    const gridResponse = {
      dataRows: [
        {
          ...sprintNode,
          data: { ...sprintNode.data, STATUS: newStatus },
        },
      ],
    };

    const refreshTreeMock = this.refreshCampaignTree(gridResponse);
    const mock = new HttpMockBuilder(`sprint/*/status`).post().build();
    const mock2 = new HttpMockBuilder(`sprint-view/*/sprintReqVersions`).get().build();
    this.find(this.startStopButton).click();

    if (newStatus === SprintStatus.CLOSED.id) {
      const dialog = new BaseDialogElement('confirm');
      dialog.confirm();
    }

    refreshTreeMock.wait();
    mock.wait();
    mock2.wait();
  }

  refreshCampaignTree(gridResponse: GridResponse) {
    return new HttpMockBuilder(`campaign-tree/refresh`).post().responseBody(gridResponse).build();
  }

  clickInformationAnchorLink(): SprintViewInformationPage {
    this.anchors.clickLink('information');
    return new SprintViewInformationPage(this);
  }

  clickSprintReqVersionsAnchorLink(): SprintViewReqVersionsLinkedPage {
    this.anchors.clickLink('linked-requirements');
    return new SprintViewReqVersionsLinkedPage();
  }

  clickSprintIssuesAnchorLink(
    authenticatedIssuePanelResponse: any,
    knownIssues?: GridResponse,
  ): SprintViewIssuesPage {
    const getIssuePanelMock = new HttpMockBuilder('issues/sprint/*?frontEndErrorIsHandled=true')
      .responseBody(authenticatedIssuePanelResponse)
      .build();

    const getKnownIssuesMock = new HttpMockBuilder('issues/sprint/*/known-issues')
      .post()
      .responseBody(knownIssues)
      .build();

    this.anchors.clickLink('issues');
    if (knownIssues) {
      getIssuePanelMock.wait();
      getKnownIssuesMock.wait();
    }
    return new SprintViewIssuesPage();
  }

  checkStatusIconExists() {
    this.statusIcon.find('i').should('exist');
  }

  checkStatusIconColorIs(expectedColor: string) {
    const rgbColorStrFormat: string = getRgbColorFromHex(expectedColor);
    return this.statusIcon.find('i').should('have.css', 'color', rgbColorStrFormat);
  }

  showDeleteConfirmationDialog(
    sprintId: number,
    sprintReqVersionIds: number[],
  ): RemoveSprintReqVersionDialogElement {
    this.find(selectByDataTestIconId('remove-sprint-req-versions')).click();
    return new RemoveSprintReqVersionDialogElement(sprintId, sprintReqVersionIds);
  }

  openRequirementDrawer(response?: any): TreeElement {
    const requirementTree = TreeElement.createTreeElement(
      'requirement-tree-picker',
      'requirement-tree',
      response,
    );
    this.clickSprintReqVersionsActionButton();
    requirementTree.waitInitialDataFetch();
    cy.clickVoid();
    cy.removeNzTooltip();
    return requirementTree;
  }

  private getDropZone(): Chainable<JQuery<HTMLDivElement>> {
    return cy.get(selectByDataTestElementId('sprintDropZone'));
  }

  dropRequirementIntoSprint(
    sprintId: number,
    refreshedSprintReqVersions?: BindReqVersionToSprintOperationReport,
  ) {
    const url = `sprint/${sprintId}/requirements`;
    const mock = new HttpMockBuilder(url).post().responseBody(refreshedSprintReqVersions).build();
    this.getDropZone().trigger('mouseup', { force: true, buttons: 1 });
    mock.wait();
  }

  closeDrawer() {
    cy.get('.ant-drawer-body .anticon-close').first().click();
  }

  clickSprintReqVersionsActionButton() {
    cy.get(this.rootSelector)
      .find(selectByDataTestButtonId('show-requirement-picker'))
      .click({ force: true });
  }

  openSprintReqVersionPageByName(
    sprintReqVersionViewModel: SprintReqVersionViewModel,
    testPlan: GridResponse<SprintReqVersionTestPlanItem>,
    issuesGridResponse: GridResponse,
  ): SprintReqVersionViewPage {
    const viewMock = new HttpMockBuilder('sprint-req-version-view/*')
      .get()
      .responseBody(mockSprintReqVersionModel(sprintReqVersionViewModel))
      .build();

    const getIssuePanelMock = new HttpMockBuilder(
      'issues/sprint-req-version/*?frontEndErrorIsHandled=true',
    )
      .responseBody(getAuthenticatedIssuePanelResponse())
      .build();

    const getKnownIssuesMock = new HttpMockBuilder('issues/sprint-req-version/*/known-issues')
      .post()
      .responseBody(issuesGridResponse)
      .build();

    const sprintReqVersionViewPage: SprintReqVersionViewPage = new SprintReqVersionViewPage(
      testPlan,
    );
    const table = this.sprintReqVersionsTable;

    table.findRowId('name', sprintReqVersionViewModel.name).then((rowId) => {
      table
        .getCell(rowId, GridColumnId.testSprintReqVersion, 'rightViewport')
        .find(selectByDataTestElementId('test-button'))
        .click();
    });

    viewMock.wait();
    if (issuesGridResponse) {
      getIssuePanelMock.wait();
      getKnownIssuesMock.wait();
    }
    return sprintReqVersionViewPage;
  }

  clickSprintOverallExecPlanAnchorLink(
    overallExecPlanPanelResponse: any,
  ): SprintViewOverallExecPlanPage {
    const overallExecPlanPanelMock = new HttpMockBuilder('sprint-view/*/overall-exec-plan')
      .post()
      .responseBody(overallExecPlanPanelResponse)
      .build();
    this.anchors.clickLink('overall-execution-plan');
    overallExecPlanPanelMock.wait();

    return new SprintViewOverallExecPlanPage();
  }

  checkTitle(expectedTitle: string) {
    this.find(selectByDataTestFieldId('sprint-req-versions-grid-title')).should(
      'contain.text',
      expectedTitle,
    );
  }

  foldTree() {
    cy.get(`[data-test-element-id="fold-tree-button"]`).click();
  }

  assertCreatedBy(expectedCreation: string) {
    this.getCreatedSprint().should('contain.text', expectedCreation);
  }

  assertLastModificationBy(lastModifiedBy: string) {
    this.getLastModifiedSprint().should('contain.text', lastModifiedBy);
  }

  assertStartDateSprintIsEmpty() {
    this.getStartDateSprint().checkPlaceholder();
  }

  assertEndDateSprintIsEmpty() {
    this.getEndDateSprint().checkPlaceholder();
  }

  editStartDateSprint(startDate: string) {
    this.getStartDateSprint().setValue(startDate + '{enter}');
  }

  editEndDateSprint(endDate: string) {
    this.getEndDateSprint().setValue(endDate + '{enter}');
  }
}

function getAuthenticatedIssuePanelResponse(): any {
  return {
    entityType: 'SPRINT_REQ_VERSION',
    bugTrackerStatus: 'AUTHENTICATED',
    projectName: '["P1"]',
    projectId: 1,
    delete: '',
    oslc: false,
  };
}
