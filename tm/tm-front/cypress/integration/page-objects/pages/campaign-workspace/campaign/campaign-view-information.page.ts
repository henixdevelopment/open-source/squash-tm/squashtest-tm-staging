import { Page } from '../../page';
import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';
import { MilestonesTagFieldElement } from '../../../elements/forms/milestones-tag-field.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { Milestone } from '../../../../../../projects/sqtm-core/src/lib/model/milestone/milestone.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { SelectFieldElement } from '../../../elements/forms/select-field.element';

export class CampaignViewInformationPage extends Page {
  private REFRESH_URL = 'campaign-tree/Campaign-*/refresh';
  readonly milestoneTagFieldElement = new MilestonesTagFieldElement(
    'campaign-milestones',
    'campaign/*/milestones/*',
  );
  readonly campaignStatusField = new SelectFieldElement('campaign-status');
  readonly nameTextField = new EditableTextFieldElement('entity-name', 'campaign/*/name');

  constructor() {
    super('sqtm-app-campaign-information-panel');
  }

  rename(newValue: string, refreshedRow: GridResponse = { dataRows: [] }) {
    const refreshMock = this.getRefreshTreeMock(refreshedRow);
    this.nameTextField.setValue(newValue);
    this.nameTextField.confirm();
    refreshMock.wait();
  }

  private getRefreshTreeMock(refreshedRow: GridResponse) {
    return new HttpMockBuilder(this.REFRESH_URL).post().responseBody(refreshedRow).build();
  }

  bindMilestone(milestoneId: number, bindableMilestones?: Milestone[]) {
    const milestoneTagElement = this.milestoneTagFieldElement;
    const milestonePickerDialog = milestoneTagElement.openMilestoneDialog();
    milestonePickerDialog.selectMilestone(milestoneId);
    const mock = new HttpMockBuilder('campaign/*/milestones/*')
      .post()
      .responseBody(bindableMilestones)
      .build();
    milestonePickerDialog.confirm();
    mock.wait();
  }
}
