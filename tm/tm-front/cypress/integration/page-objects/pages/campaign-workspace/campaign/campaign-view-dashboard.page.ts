import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { EditIterationsScheduledDatesDialog } from '../dialogs/edit-iterations-scheduled-dates-dialog';
import { CampaignWorkspaceDashboardPage } from '../campaign-workspace-dashboard.page';
import { IterationPlanning } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/campaign-model';

export class CampaignViewDashboardPage extends CampaignWorkspaceDashboardPage {
  constructor(private campaignId: string | number = '*') {
    super('sqtm-app-campaign-view-dashboard');
  }

  openIterationScheduledDatesDialog(scheduledIterations: IterationPlanning[] = []) {
    const mock = new HttpMockBuilder(`campaign/${this.campaignId}/iterations`)
      .responseBody(scheduledIterations)
      .build();
    this.findByElementId('iteration-scheduled-dates-button').click();
    mock.wait();
    const dialog = new EditIterationsScheduledDatesDialog(this.campaignId);
    dialog.assertIsOpened();
    return dialog;
  }

  assertInventoryTableExist() {
    cy.get(this.rootSelector).find('sqtm-app-campaign-inventory').should('exist');

    this.inventoryTable.assertExists();
  }

  assertIterationRowHasName(index: number, expectedName: string) {
    this.inventoryTable.findRow(index).assertExists().getCell(0).assertContainsText(expectedName);
  }

  checkTotalCount(rowIndex: number, expected: number) {
    this.inventoryTable
      .findRow(rowIndex)
      .assertExists()
      .getCell('total-count')
      .assertContainsText(expected.toString());
  }

  checkExecutionRate(rowIndex: number, expected: number) {
    this.inventoryTable
      .findRow(rowIndex)
      .assertExists()
      .getCell('execution-rate')
      .assertContainsText(`${expected} %`);
  }

  assertDateButtonIsVisible() {
    this.findByElementId('iteration-scheduled-dates-button').should('exist');
  }
}
