import { CampaignViewPage } from './campaign-view.page';
import { Page } from '../../page';
import { EditableDateFieldElement } from '../../../elements/forms/editable-date-field.element';
import { selectByDataIcon } from '../../../../utils/basic-selectors';
import { EditIterationsScheduledDatesDialog } from '../dialogs/edit-iterations-scheduled-dates-dialog';
import { selectNodeInCampaignWorkspace } from '../../../../scenarios/scenario-parts/campaign.part';
import { CampaignWorkspacePage } from '../campaign-workspace.page';
import { IterationViewPage } from '../iteration/iteration-view.page';

export class CampaignViewPlanningPage extends Page {
  readonly scheduledStartDateField = new EditableDateFieldElement('scheduledStartDate');
  readonly actualStartDateField = new EditableDateFieldElement('actualStartDate');
  readonly scheduledEndDateField = new EditableDateFieldElement('scheduledEndDate');
  readonly actualEndDateField = new EditableDateFieldElement('actualEndDate');
  constructor(private parentPage: CampaignViewPage) {
    super('sqtm-app-campaign-view-planning-panel');
  }

  assertExists() {
    super.assertExists();
  }

  editIterationScheduleAndCheckModification(
    idIteration: number,
    valueStarDate: string,
    valueEndDate: string,
    campaignWorkspace: CampaignWorkspacePage,
    iterationViewPage: IterationViewPage,
    nameNode: string,
    nameIteration: string,
  ) {
    const dialogEditIterationSchedule = new EditIterationsScheduledDatesDialog();
    this.parentPage.find(selectByDataIcon('sqtm-core-campaign:calendar')).eq(1).click();
    dialogEditIterationSchedule.assertIsOpened();
    dialogEditIterationSchedule.setScheduledStartDateToNow(idIteration, false, valueStarDate);
    dialogEditIterationSchedule.setScheduledEndDateToNow(idIteration, false, valueEndDate);
    dialogEditIterationSchedule.clickOnConfirmButton();
    selectNodeInCampaignWorkspace(campaignWorkspace, nameNode, nameIteration);
    iterationViewPage.assertExists();
    const iterationCampaignPlanning = iterationViewPage.clickPlanningAnchorLink();
    iterationCampaignPlanning.scheduledStartDateField.checkContent(valueStarDate, true);
    iterationCampaignPlanning.scheduledEndDateField.checkContent(valueEndDate, true);
  }
}
