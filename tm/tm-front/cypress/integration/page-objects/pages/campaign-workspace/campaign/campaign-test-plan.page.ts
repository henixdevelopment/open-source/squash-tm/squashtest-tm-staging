import { Page } from '../../page';
import { GridElement, TreeElement } from '../../../elements/grid/grid.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { VerifyItemTestPlanOptions } from '../iteration/iteration-test-plan.page';
import { RemoveCtpiDialogElement } from '../dialogs/remove-ctpi-dialog.element';
import { MassDeleteCtpiDialogElement } from '../dialogs/mass-delete-ctpi-dialog.element';
import { CtpiMultiEditDialogElement } from '../dialogs/ctpi-multi-edit-dialog.element';
import Chainable = Cypress.Chainable;
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import {
  selectByDataTestButtonId,
  selectByDataTestCellId,
  selectByDataTestElementId,
  selectByDataTestItemId,
  selectByDataTestRowId,
} from '../../../../utils/basic-selectors';
import { MenuElement, MenuItemElement } from '../../../../utils/menu.element';
import { TestCaseForCampaignSearchPage } from '../../test-case-workspace/research/test-case-for-campaign-search-page';
import { RequirementTestCaseForCampaignSearchPage } from '../../requirement-workspace/search/dialogs/requirement-test-case-for-campaign-search-page';
import { ExecutionPage } from '../../execution/execution-page';
import { TestPlanPage } from '../test-plan-page';
import { ExecutionRunnerStepPage } from '../../execution/execution-runner-step-page';

export class CampaignTestPlanPage extends Page implements TestPlanPage {
  private constructor(
    rootSelector: string,
    public readonly testPlan: GridElement,
  ) {
    super(rootSelector);
  }

  static navigateTo(campaignId: number | string, testPlan: GridResponse): CampaignTestPlanPage {
    campaignId = campaignId ?? '*';
    const url = `campaign/${campaignId}/test-plan`;
    const gridElement = GridElement.createGridElement('campaign-test-plan', url, testPlan);
    return new CampaignTestPlanPage('sqtm-app-campaign-test-plan-execution', gridElement);
  }

  openTestCaseDrawer(response?: GridResponse): TreeElement {
    const treeElement = TreeElement.createTreeElement(
      'test-case-tree-picker',
      'test-case-tree',
      response,
    );
    cy.get(selectByDataTestButtonId('show-test-case-picker')).click();
    treeElement.waitInitialDataFetch();
    return treeElement;
  }

  enterIntoTestPlan() {
    this.getDropZone().trigger('mouseenter', 'left', { force: true, buttons: 1 });
  }

  private getDropZone(): Chainable<JQuery<HTMLDivElement>> {
    return cy.get(selectByDataTestElementId(CAMPAIGN_TEST_PLAN_DROP_ZONE_ID));
  }

  assertColoredBorderIsVisible() {
    this.getDropZone().should('have.class', 'sqtm-core-border-current-workspace-color');
  }

  dropIntoTestPlan(
    campaignId: number | string,
    createdTestPlanItemIds?: number[],
    refreshedTestPlan?: GridResponse,
  ): Chainable<number[]> {
    const url = `campaign/${campaignId}/test-plan-items`;
    const mock = new HttpMockBuilder<{ itemTestPlanIds: number[] }>(url)
      .post()
      .responseBody({ itemTestPlanIds: createdTestPlanItemIds })
      .build();
    this.testPlan.declareRefreshData(refreshedTestPlan);
    this.getDropZone().trigger('mouseup', { force: true });
    return mock.waitResponseBody().then((responseBody) => {
      if (refreshedTestPlan) {
        this.testPlan.waitForRefresh();
      }
      return cy.wrap(responseBody.itemTestPlanIds);
    });
  }

  verifyTestPlanItem(options: VerifyItemTestPlanOptions) {
    this.testPlan.assertRowExist(options.id);
    const row = this.testPlan.getRow(options.id);
    if (options.name) {
      if (options.showsAsLink) {
        row.cell('testCaseName').linkRenderer().assertContainText(options.name);
      } else {
        row.cell('testCaseName').textRenderer().assertContainsText(options.name);
      }
    }
  }

  assertColoredBorderIsNotVisible() {
    this.getDropZone().should('not.have.class', 'sqtm-core-border-current-workspace-color');
  }

  closeTestCaseDrawer() {
    cy.get('.ant-drawer-body .anticon-close').click();
  }

  moveItemWithoutServerResponse(itemToMove: string, itemToDropOnto: string) {
    this.startDrag(itemToMove);
    this.getDraggedContent()
      .should('be.visible')
      .find(selectByDataTestElementId('cannot-drag-icon'))
      .should('not.exist');
    this.internalDropOnto(itemToDropOnto);
  }

  moveItemWithServerResponse(itemToMove: string, itemToDropOnto: string, res: any): void {
    const mock = new HttpMockBuilder('campaign/*/test-plan/*/position/*')
      .responseBody(res)
      .post()
      .build();

    this.startDrag(itemToMove);
    this.getDraggedContent()
      .should('be.visible')
      .find(selectByDataTestElementId('cannot-drag-icon'))
      .should('not.exist');
    this.internalDropOnto(itemToDropOnto);

    mock.wait();
  }

  private startDrag(itemToDrag: string): void {
    this.testPlan
      .getRow(itemToDrag, 'leftViewport')
      .cell('#')
      .findCell()
      .find('span')
      .trigger('mousedown', { button: 0 })
      .trigger('mousemove', -20, -20, { force: true })
      .trigger('mousemove', 50, 50, { force: true });
  }

  private getDraggedContent() {
    return cy.get('sqtm-app-test-plan-dragged-content');
  }

  private internalDropOnto(itemToDropOnto: string): void {
    this.testPlan
      .getRow(itemToDropOnto, 'leftViewport')
      .cell('#')
      .findCell()
      .trigger('mousemove', 50, 50, { force: true })
      .trigger('mouseup', { force: true });
  }

  assertCannotMoveItem(itemToMove: string): void {
    this.startDrag(itemToMove);
    this.getDraggedContent().find(selectByDataTestElementId('cannot-drag-icon')).should('exist');
    this.internalDropOnto(itemToMove);
  }

  showDeleteConfirmDialog(campaignId: number, testPlanId: number) {
    cy.get(selectByDataTestRowId(`${testPlanId}`))
      .find(selectByDataTestCellId('delete'))
      .find('i')
      .click();
    return new RemoveCtpiDialogElement(testPlanId, campaignId);
  }

  showMassDeleteConfirmDialog(campaignId: number, testPlanIds: number[]) {
    cy.get(selectByDataTestButtonId('show-confirm-mass-delete-dialog')).click();
    return new MassDeleteCtpiDialogElement(campaignId, testPlanIds);
  }

  openMassEditDialog(): CtpiMultiEditDialogElement {
    cy.get(selectByDataTestButtonId('mass-edit')).click();
    return new CtpiMultiEditDialogElement();
  }

  changeDataset(itemId: string) {
    cy.get(selectByDataTestCellId('dataset-cell')).click();
    const updatedDatasetMock = new HttpMockBuilder('test-plan-item/campaign/*/dataset')
      .post()
      .build();
    cy.get(selectByDataTestItemId(itemId)).click();
    updatedDatasetMock.wait();
  }

  private openSearchMenu() {
    cy.get(selectByDataTestButtonId('search')).click();
    return new MenuElement('search-menu');
  }

  goToSearchTestCase() {
    const searchMenu = this.openSearchMenu();
    const grid = new GridElement('test-case-search');

    searchMenu.item('search-testcase').click();
    return new TestCaseForCampaignSearchPage(grid);
  }

  goToRequirementLinkedSearch() {
    const searchMenu = this.openSearchMenu();
    const grid = new GridElement('tc-by-requirement');

    searchMenu.item('search-tc-by-requirement').click();
    return new RequirementTestCaseForCampaignSearchPage(grid);
  }

  private openExecutionMenuInTestPlan(itemName: string): MenuItemElement {
    this.testPlan.findRowId('testCaseName', itemName).then((idItem) => {
      this.testPlan.getCell(idItem, 'startExecution', 'rightViewport').iconRenderer().click();
    });
    return new MenuItemElement('', 'play-exec');
  }

  createNewExecutionOfItem(itemName: string) {
    const menuItem = this.openExecutionMenuInTestPlan(itemName);
    menuItem.selectLinkElement('manual-execution');
    return new ExecutionPage();
  }

  openStartOverDialog() {
    cy.get(this.rootSelector).find(selectByDataTestButtonId('relaunch-execution')).click();
  }

  clickButtonResumeExecution(
    _idIteration: string,
    _idTestPlan: string,
    _idExecution: string,
    _idStep: string,
  ): ExecutionRunnerStepPage {
    throw new Error('Method not supported for CampaignTestPlanPage.');
  }

  accessToSession(_itemName: any): void {
    throw new Error('Method not supported for CampaignTestPlanPage.');
  }

  openExecutionWithPopUp(_item: any, _idExecution: any): void {
    throw new Error('Method not supported for CampaignTestPlanPage.');
  }

  assertTestCasePositionInExecutionPlan(nameTestCase: string, position: number): void {
    this.testPlan.findRowId('testCaseName', nameTestCase).then((idTestCase) => {
      this.testPlan.getCell(idTestCase, '#', 'leftViewport').findCellTextSpan().contains(position);
    });
  }
}

const CAMPAIGN_TEST_PLAN_DROP_ZONE_ID = 'campaignTestPlanDropZone';
