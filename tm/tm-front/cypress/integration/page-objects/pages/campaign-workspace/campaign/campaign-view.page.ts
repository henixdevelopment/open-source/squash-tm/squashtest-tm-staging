import { EditableRichTextFieldElement } from '../../../elements/forms/editable-rich-text-field.element';
import { apiBaseUrl, HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { CampaignViewInformationPage } from './campaign-view-information.page';
import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';
import { CampaignViewStatisticsPage } from './campaign-view-statistics.page';
import { CampaignViewIssuesPage } from './campaign-view-issues.page';
import { CampaignTestPlanPage } from './campaign-test-plan.page';
import { CampaignViewDashboardPage } from './campaign-view-dashboard.page';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';
import { AbstractTestPlanPage } from '../abstract-test-plan.page';
import { MilestonesTagFieldElement } from '../../../elements/forms/milestones-tag-field.element';
import { MilestonePickerDialogElement } from '../../../elements/grid/milestone-picker-dialog.element';
import { selectByDataTestFieldId } from '../../../../utils/basic-selectors';
import { CampaignViewPlanningPage } from './campaign-view-planning.page';

export class CampaignViewPage extends AbstractTestPlanPage {
  readonly nameTextField = new EditableTextFieldElement('entity-name');
  readonly descriptionRichField = new EditableRichTextFieldElement('campaign-description');
  readonly referenceField = new EditableTextFieldElement('entity-reference');
  readonly milestoneTagElement = new MilestonesTagFieldElement(
    'campaign-milestones',
    'campaign/*/milestones/*',
  );

  readonly anchors = AnchorsElement.withLinkIds(
    'information',
    'dashboard',
    'statistics',
    'plan-exec',
    'issues',
    'planning',
  );

  // For end to end test campaignId is not required
  constructor(public campaignId?: number | string) {
    super('sqtm-app-campaign-view');
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/campaign-view/${this.campaignId}?**`;
    cy.wait(`@${url}`);
  }

  clickInformationAnchorLink(): CampaignViewInformationPage {
    this.anchors.clickLink('information');
    return new CampaignViewInformationPage();
  }

  clickStatisticsAnchorLink(): CampaignViewStatisticsPage {
    this.anchors.clickLink('statistics');
    return new CampaignViewStatisticsPage(this);
  }

  clickPlanningAnchorLink(): CampaignViewPlanningPage {
    this.anchors.clickLink('planning');
    return new CampaignViewPlanningPage(this);
  }

  showTestPlan(testPlan?: GridResponse): CampaignTestPlanPage {
    const campaignTestPlanPage = CampaignTestPlanPage.navigateTo(this.campaignId, testPlan);
    this.anchors.clickLink('plan-exec');

    if (this.campaignId) {
      campaignTestPlanPage.testPlan.waitInitialDataFetch();
    }

    return campaignTestPlanPage;
  }

  clickDashboardAnchorLink() {
    this.anchors.clickLink('dashboard');
    return new CampaignViewDashboardPage(this.campaignId);
  }

  checkData(fieldId: string, value: string) {
    cy.get(selectByDataTestFieldId(fieldId)).find('span').should('contain.text', value);
  }

  showIssuesWithoutBindingToBugTracker(response?: any): CampaignViewIssuesPage {
    const httpMock = new HttpMockBuilder('issues/campaign/*?frontEndErrorIsHandled=true')
      .responseBody(response)
      .build();
    this.anchors.clickLink('issues');
    httpMock.wait();
    cy.removeNzTooltip();
    return new CampaignViewIssuesPage();
  }

  showIssuesIfBoundToBugTracker(
    modelResponse?: any,
    gridResponse?: GridResponse,
  ): CampaignViewIssuesPage {
    const bugTrackerModel = new HttpMockBuilder('issues/campaign/*?frontEndErrorIsHandled=true')
      .responseBody(modelResponse)
      .build();
    const issues = new HttpMockBuilder('issues/campaign/*/known-issues')
      .responseBody(gridResponse)
      .post()
      .build();
    this.anchors.clickLink('issues');
    bugTrackerModel.wait();
    issues.wait();
    cy.removeNzTooltip();
    return new CampaignViewIssuesPage();
  }

  bindMilestoneToCampaign(milestoneId: number, singleMilestone: boolean) {
    if (singleMilestone === true) {
      const milestonePicker = new MilestonePickerDialogElement();
      this.milestoneTagElement.openMilestoneDialog();
      milestonePicker.selectMilestone(milestoneId);
      milestonePicker.confirm();
    } else {
      const milestoneTagElement = this.milestoneTagElement;
      const milestonePickerDialog = milestoneTagElement.openMilestoneDialog();
      milestonePickerDialog.assertExists();
      milestonePickerDialog.selectMilestone(milestoneId);
      milestonePickerDialog.confirm();
    }
  }
}
