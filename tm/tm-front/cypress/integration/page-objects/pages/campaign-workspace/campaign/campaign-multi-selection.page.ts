import { Page } from '../../page';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';
import { CampaignWorkspaceDashboardPage } from '../campaign-workspace-dashboard.page';

export class CampaignMultiSelectionPage extends Page {
  readonly anchors = AnchorsElement.withLinkIds('dashboard');

  constructor() {
    super('sqtm-app-campaign-workspace-multi-select-view');
  }

  private getFavoriteButton() {
    return this.findByElementId('favorite-button');
  }

  private getTitleDashboard() {
    return this.find('.ant-collapse-header');
  }

  private getDashboardPanel() {
    return this.find('.ant-collapse-content-box');
  }

  clickDashboardAnchorLink() {
    this.anchors.clickLink('dashboard');
  }

  clickFavoriteButton() {
    this.getFavoriteButton().click();
    return new CampaignWorkspaceDashboardPage('sqtm-app-custom-dashboard');
  }

  assertNoDashboardSelectedMessage() {
    this.getDashboardPanel()
      .find('span')
      .should(
        'contain.text',
        "Vous pouvez sélectionner un tableau de bord à afficher depuis l'espace Pilotage, en cliquant sur le bouton [Favori] d'un tableau de bord.",
      );
  }

  assertTitleDashboardExist(expectedTitle: string) {
    this.getTitleDashboard().should('contain.text', expectedTitle);
  }
}
