import { EntityViewPage } from '../../page';
import { CampaignStatisticsPanelElement } from './campaign-statistics-panel.element';
import { selectByDataTestElementId } from '../../../../utils/basic-selectors';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';

export class CampaignMilestoneViewPage extends EntityViewPage {
  anchors = AnchorsElement.withLinkIds('information', 'dashboard');

  public constructor() {
    super('sqtm-app-campaign-milestone-view');
  }

  showDashboard() {
    this.anchors.clickLink('dashboard');
    return new CampaignStatisticsPanelElement();
  }

  assertNameEquals(expectedName: string) {
    cy.get(this.rootSelector)
      .find(selectByDataTestElementId('milestone-label'))
      .should('contain.text', expectedName);
  }
}
