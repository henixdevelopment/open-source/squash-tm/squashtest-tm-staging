import { ChartElement } from '../../../elements/charts/chart.element';
import { selectByDataTestElementId } from '../../../../utils/basic-selectors';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { StatisticsBundle } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/campaign-model';

export class CampaignStatisticsPanelElement {
  public statusChart = new ChartElement('status-chart');
  public conclusivenessChart = new ChartElement('conclusiveness-chart');
  public importanceChart = new ChartElement('importance-chart');
  private customDashboardTag = `sqtm-app-custom-dashboard`;

  constructor() {}

  private getFavoriteButton() {
    return cy.get(selectByDataTestElementId('favorite-button'));
  }

  assertTitleExist(expectedTitle: string) {
    cy.get('.ant-collapse-header').should('contain.text', expectedTitle);
  }

  refreshStatistics(stats?: StatisticsBundle) {
    const selector = `.ant-collapse-header ${selectByDataTestElementId('refresh-button')}`;
    const mock = new HttpMockBuilder('campaign/statistics').post().responseBody(stats).build();
    cy.get(selector).click();
    mock.wait();
  }

  clickFavoriteButton() {
    this.getFavoriteButton().click();
  }

  assertCustomDashboardNotExist() {
    cy.get(this.customDashboardTag).should('not.exist');
  }
}
