import { Page } from '../page';
import { NavBarElement } from '../../elements/nav-bar/nav-bar.element';
import { ReferentialDataProviderBuilder } from '../../../utils/referential/referential-data.provider';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { HomeWorkspaceModel } from '../../../../../projects/sqtm-core/src/lib/model/home/home-workspace.model';
import { ReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';

export class HomeWorkspacePage extends Page {
  public readonly navBar: NavBarElement = new NavBarElement();

  public constructor() {
    super('sqtm-app-home-workspace-page');
  }

  public static initTestAtPage(referentialData?: ReferentialDataModel) {
    return HomeWorkspacePage.initTestAtPageWithModel(null, referentialData);
  }

  public static initTestAtPageWithModel(
    homeWorkspaceModel?: HomeWorkspaceModel,
    referentialData?: ReferentialDataModel,
  ): HomeWorkspacePage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const homePageModelMock = new HttpMockBuilder('home-workspace')
      .responseBody(homeWorkspaceModel)
      .build();
    cy.visit('home-workspace');
    referentialDataProvider.wait();
    homePageModelMock.wait();
    return new HomeWorkspacePage();
  }

  public showDashboard(): void {
    const userPrefsMock = new HttpMockBuilder('user-prefs/update').post().build();
    this.findByElementId('show-dashboard-button').click();
    userPrefsMock.wait();
  }

  public showWelcomeMessage(): void {
    this.findByElementId('show-welcome-message-button').click();
  }

  public assertMessageIsVisible(): void {
    this.findByElementId('welcome-message-container').should('be.visible');
  }

  public assertDashboardIsVisible(): void {
    this.findByElementId('dashboard-container').should('be.visible');
  }

  public assertDefaultMessageIsVisible(): void {
    this.findByElementId('default-welcome-message').should('be.visible');
  }

  assertCustomMessageIsVisible() {
    this.findByElementId('custom-welcome-message').should('be.visible');
  }

  assertCustomMessageContains(expectedMessage: string) {
    this.findByElementId('custom-welcome-message').should('contain.html', expectedMessage);
  }

  assertLicenseMessageDoesNotExist(): void {
    this.findByElementId('license-message-container').should('not.exist');
  }

  assertLicenseMessageIsVisible(): void {
    this.findByElementId('license-message-container').should('be.visible');
  }

  assertNoDashboardSelectedMessage(): void {
    this.findByElementId('dashboard-container')
      .find('span')
      .should(
        'contain.text',
        "Vous pouvez sélectionner un tableau de bord à afficher depuis l'espace Pilotage, en cliquant sur le bouton [Favori] d'un tableau de bord.",
      );
  }
}
