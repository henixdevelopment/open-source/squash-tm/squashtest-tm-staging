import { GridElement } from '../elements/grid/grid.element';
import {
  selectByDataTestCustomFieldName,
  selectByDataTestFieldId,
  selectByDataTestIconId,
  selectByDataTestMenuId,
} from '../../utils/basic-selectors';
import { EditableTextFieldElement } from '../elements/forms/editable-text-field.element';
import { EditableRichTextFieldElement } from '../elements/forms/editable-rich-text-field.element';
import { EditableNumericFieldElement } from '../elements/forms/editable-numeric-field.element';
import { EditableDateFieldElement } from '../elements/forms/editable-date-field.element';
import { EditableTagFieldElement } from '../elements/forms/editable-tag-field.element';
import { EditableSelectFieldElement } from '../elements/forms/editable-select-field.element';
import { CheckBoxElement } from '../elements/forms/check-box.element';
import { AbstractFormFieldElement } from '../elements/forms/abstract-form-field.element';
import { InputType } from '../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { ToolbarButtonElement } from '../elements/workspace-common/toolbar.element';
import { MenuItemElement } from '../../utils/menu.element';
import { BasicElement } from '../elements/abstract-element';

export type PageFactory<T extends Page> = (...args: any[]) => T;

export abstract class Page extends BasicElement {
  protected constructor(rootSelector: string) {
    super(rootSelector);
  }

  pressEscape() {
    cy.get('body').type('{esc}');
  }

  checkDataFetched() {
    // NOOP: this method is overriden in its children's classes
  }

  assertElementsPageAreFullyLoaded() {
    this.rootElement.find('sqtm-core-full-page-loading-icon').should('not.exist');
  }
}

export abstract class WorkspaceWithGridPage extends Page {
  protected constructor(
    public readonly grid: GridElement,
    rootSelector: string,
  ) {
    super(rootSelector);
  }

  waitInitialDataFetch() {
    this.grid.waitInitialDataFetch();
  }

  assertExists() {
    super.assertExists();
    this.grid.assertExists();
  }
}

export abstract class EntityViewPage extends Page {
  protected constructor(rootSelector: string) {
    super(rootSelector);
  }

  assertNameContains(expected: string) {
    this.find(selectByDataTestFieldId('entity-name')).find('span').should('contain.text', expected);
  }

  assertReferenceContains(expected: string) {
    if (expected == null || expected === '') {
      // For an empty reference, the selector won't work as the span isn't rendered.
      return;
    }

    this.findByFieldId('entity-reference').find('span').should('contain.text', expected);
  }

  assetPathContains(expected: string) {
    if (expected == null || expected === '') {
      return;
    }

    this.findByFieldId('entity-path').should('contain.text', expected);
  }

  toggleTree() {
    this.findByElementId('fold-tree-button').click({ force: true });
  }

  getTextCustomField(customFieldName: string): EditableTextFieldElement {
    return this.getCustomField(customFieldName, InputType.PLAIN_TEXT) as EditableTextFieldElement;
  }

  getRichTextCustomField(customFieldName: string): EditableRichTextFieldElement {
    return this.getCustomField(
      customFieldName,
      InputType.RICH_TEXT,
    ) as EditableRichTextFieldElement;
  }

  getNumericCustomField(customFieldName: string): EditableNumericFieldElement {
    return this.getCustomField(customFieldName, InputType.NUMERIC) as EditableNumericFieldElement;
  }

  getDateCustomField(customFieldName: string): EditableDateFieldElement {
    return this.getCustomField(customFieldName, InputType.DATE_PICKER) as EditableDateFieldElement;
  }

  getTagCustomField(customFieldName: string): EditableTagFieldElement {
    return this.getCustomField(customFieldName, InputType.TAG) as EditableTagFieldElement;
  }

  getSelectCustomField(customFieldName: string): EditableSelectFieldElement {
    return this.getCustomField(
      customFieldName,
      InputType.DROPDOWN_LIST,
    ) as EditableSelectFieldElement;
  }

  getCheckboxCustomField(customFieldName: string): CheckBoxElement {
    return this.getCustomField(customFieldName, InputType.CHECKBOX) as CheckBoxElement;
  }

  getCustomField(customFieldName: string, inputType: InputType): AbstractFormFieldElement {
    const singleValueUrl = 'custom-fields/values/*/single-value';
    const multiValueUrl = 'custom-fields/values/*/multi-value';
    const cufSelector = () =>
      this.find(selectByDataTestCustomFieldName(customFieldName)).siblings().first();

    switch (inputType) {
      case InputType.DROPDOWN_LIST:
        return new EditableSelectFieldElement(cufSelector, singleValueUrl);
      case InputType.CHECKBOX:
        return new CheckBoxElement(cufSelector, singleValueUrl);
      case InputType.DATE_PICKER:
        return new EditableDateFieldElement(cufSelector, singleValueUrl);
      case InputType.NUMERIC:
        return new EditableNumericFieldElement(cufSelector, singleValueUrl);
      case InputType.PLAIN_TEXT:
        return new EditableTextFieldElement(cufSelector, singleValueUrl);
      case InputType.RICH_TEXT:
        return new EditableRichTextFieldElement(cufSelector, singleValueUrl);
      case InputType.TAG:
        return new EditableTagFieldElement(cufSelector, multiValueUrl);
    }
  }

  clickActionMenuAndAssertPrintModeExists() {
    const actionMenuButton = new ToolbarButtonElement(this.rootSelector, 'action-menu-button');
    const printMenuItem = new MenuItemElement(
      selectByDataTestMenuId('action-menu'),
      'open-in-print-mode',
    );
    actionMenuButton.clickWithoutSpan();
    printMenuItem.assertExists();
    printMenuItem.assertContains('Imprimer');
  }

  clickActionMenuAndAssertPrintModeNotExists() {
    const actionMenuButton = new ToolbarButtonElement(this.rootSelector, 'action-menu-button');
    const printMenuItem = new MenuItemElement(
      selectByDataTestMenuId('action-menu'),
      'open-in-print-mode',
    );
    actionMenuButton.clickWithoutSpan();
    printMenuItem.assertNotExist();
  }

  assertIconNotExists(icon: string) {
    this.find(selectByDataTestIconId(icon)).should('not.exist');
  }
}
