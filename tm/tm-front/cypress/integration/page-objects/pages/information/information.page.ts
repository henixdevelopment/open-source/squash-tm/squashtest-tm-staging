import Chainable = Cypress.Chainable;
import { AlertDialogElement } from '../../elements/dialog/alert-dialog.element';
import { HomeWorkspacePage } from '../home-workspace/home-workspace.page';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { ReferentialDataProviderBuilder } from '../../../utils/referential/referential-data.provider';

export class InformationPage {
  private alertDialogElement: AlertDialogElement;

  constructor() {
    this.alertDialogElement = new AlertDialogElement('license-information-detail');
  }

  get selector(): Chainable {
    return cy.get('sqtm-app-information-page');
  }

  assertIsVisible(): void {
    this.selector.should('be.visible');
  }

  assertAlertIsVisible(): void {
    this.alertDialogElement.assertExists();
  }

  confirmAlert(): HomeWorkspacePage {
    const refDataProvider = new ReferentialDataProviderBuilder().build();
    const homePageModelMock = new HttpMockBuilder('home-workspace').responseBody({}).build();

    this.alertDialogElement.close();

    refDataProvider.wait();
    homePageModelMock.wait();
    return new HomeWorkspacePage();
  }
}
