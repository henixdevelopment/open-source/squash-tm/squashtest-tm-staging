import { ToolbarElement } from '../elements/workspace-common/toolbar.element';
import { GroupedMultiListElement } from '../elements/filters/grouped-multi-list.element';
import { getColumnDataProviderMock } from './requirement-workspace/search/search-page-utils';
import {
  selectByDataColumnId,
  selectByDataTestButtonId,
  selectByDataTestElementId,
  selectByDataTestToolbarButtonId,
} from '../../utils/basic-selectors';
import { HttpMockBuilder } from '../../utils/mocks/request-mock';
import { NoMilestoneSharedDialog } from './requirement-workspace/dialogs/no-milestone-shared-dialog';
import { NoPermissionToBindMilestoneDialog } from './requirement-workspace/dialogs/NoPermissionToBindMilestoneDialog';
import { MilestoneAlreadyBoundDialog } from './requirement-workspace/dialogs/MilestoneAlreadyBoundDialog';
import { Page } from './page';
import { EditMilestonesDialog } from './test-case-workspace/research/dialogs/edit-milestones-dialog';
import { TextFilterWidgetElement } from '../elements/filters/text-filter-widget.element';

export abstract class AbstractSearchPage extends Page {
  abstract readonly treeRefreshUrl: string;
  abstract readonly searchMilestoneUrl: string;

  showMassBindingMilestoneDialog<T>(milestoneMassEdit?: T): EditMilestonesDialog {
    const mock = new HttpMockBuilder(this.searchMilestoneUrl)
      .responseBody(milestoneMassEdit)
      .build();
    const milestoneButton = cy.get(
      `${this.buildToolBarSelector()} ${selectByDataTestToolbarButtonId('edit-milestones-button')}`,
    );
    milestoneButton.should('exist');
    milestoneButton.click();
    mock.wait();
    return new EditMilestonesDialog();
  }

  protected abstract buildToolBarSelector(): string;

  openColumnList(activeColumnsIds: string[]): GroupedMultiListElement {
    const groupedMultiListElement = new GroupedMultiListElement();
    const getColumnDataProvider = getColumnDataProviderMock(
      activeColumnsIds,
      'grid/get-column-config',
    );
    cy.get(selectByDataTestToolbarButtonId('open-widget-column-button')).should('exist').click();
    getColumnDataProvider.wait();
    groupedMultiListElement.assertExists();
    return groupedMultiListElement;
  }

  checkIfColumnAreDisplayedThenClick(exist: string) {
    cy.get(selectByDataColumnId('milestones')).should(exist);
    cy.get(selectByDataColumnId('coverages')).should(exist);
    cy.get(selectByDataColumnId('attachments')).should(exist);
  }
  clickOnEyeAndWaitResponse() {
    cy.get('i[data-test-toolbar-button-id="show-all-columns-button"]').click();
    const updateMock = new HttpMockBuilder(this.treeRefreshUrl).post().build();
    updateMock.wait();
  }

  foldFilterPanel() {
    this.findByElementId('fold-filter-panel-button').click();
  }

  getNoMilestoneSharedDialog(): NoMilestoneSharedDialog {
    return new NoMilestoneSharedDialog();
  }

  getNoPermissionToBindMilestoneDialog(): NoPermissionToBindMilestoneDialog {
    const toolbarElement = new ToolbarElement('requirement-search-toolbar');
    toolbarElement.button('edit-milestones-button').clickWithoutSpan();
    return new NoPermissionToBindMilestoneDialog();
  }

  getMilestoneAlreadyBoundDialog(): MilestoneAlreadyBoundDialog {
    return new MilestoneAlreadyBoundDialog();
  }
  clickAddCriteria() {
    cy.get(this.rootSelector)
      .find(selectByDataTestElementId('grid-filter-manager-add-criteria'))
      .click();
  }

  checkIfColumnsAreDisplayed(columnsList: string[], exist: string) {
    columnsList.forEach((columnId) => {
      cy.get(selectByDataColumnId(columnId)).should(exist);
    });
  }

  clickSearchButton() {
    cy.get(selectByDataTestButtonId('search-button')).click();
  }
  clickInactivateButton() {
    cy.get(selectByDataTestElementId('inactivate')).click();
  }

  openColumnConfiguration() {
    cy.get(selectByDataTestToolbarButtonId('open-widget-column-button')).click();
  }

  clickOnNewSearch() {
    cy.get(selectByDataTestElementId('grid-filter-manager-reset-filters')).click();
  }

  selectItemToAppearAloneInPageWithNameCriteria(nameItem: string) {
    const editFilterTextElement = new TextFilterWidgetElement();

    this.addCriteria('Nom');
    editFilterTextElement.typeAndConfirm(nameItem);
  }
  addCriteria(criteria: string) {
    const multilistElement = new GroupedMultiListElement();

    this.clickAddCriteria();
    multilistElement.toggleOneItem(criteria);
  }
}
