import { UserView } from '../../../../../../projects/sqtm-core/src/lib/model/user/user-view';

export class RequirementSearchModel {
  usersWhoCreatedRequirements: UserView[];
  usersWhoModifiedRequirements: UserView[];
}
