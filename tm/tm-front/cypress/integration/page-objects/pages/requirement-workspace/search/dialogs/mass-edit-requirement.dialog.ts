import { OptionalSelectField } from '../../../../elements/forms/optional-select-field.element';
import { HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import { GridResponse } from '../../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export abstract class AbstractMassEditRequirementDialog {
  abstract readonly treeRefreshUrl: string;
  abstract readonly parentSelector: string;
  abstract readonly httpMockUrl: string;
  assertNotExist() {
    cy.get(this.buildSelector()).should('not.exist');
  }

  assertExists() {
    cy.get(this.buildSelector()).should('exist');
  }

  getOptionalField(fieldName): OptionalSelectField {
    const optionalField = new OptionalSelectField(fieldName, this.parentSelector);
    optionalField.assertExists();
    return optionalField;
  }

  confirm(gridResponse?: GridResponse) {
    const httpMock = new HttpMockBuilder(this.httpMockUrl).post().build();
    const researchMock = new HttpMockBuilder(this.treeRefreshUrl)
      .post()
      .responseBody(gridResponse)
      .build();
    this.clickButton('confirm');
    httpMock.waitResponseBody().then(() => {
      researchMock.wait();
    });
  }

  cancel() {
    this.clickButton('cancel');
  }

  clickButton(buttonId: string) {
    const confirmButton = cy.get(`button[data-test-dialog-button-id=${buttonId}]`);
    confirmButton.should('exist');
    confirmButton.click();
  }

  assertExistNoWritingRightsMessage() {
    cy.get('span[data-test-dialog-message=no-writing-right]').should(
      'contain.text',
      "Certains éléments ne seront pas modifiés car soit le statut de l'un de leurs" +
        ' jalons associés ne le permet pas, soit vous ne disposez pas de droits suffisants.',
    );
  }

  assertExistDifferentInfoListMessage() {
    cy.get('span[data-test-dialog-message=different-info-lists]').should(
      'contain.text',
      'Les résultats de recherche sélectionnés sont issus de projets ayant des' +
        ' configurations différentes. Certains critères de modification en masse ne seront donc pas disponibles.',
    );
  }

  assertExistCanOnlyEditStatusMessage() {
    cy.get('span[data-test-dialog-message=can-only-edit-status]').should(
      'contain.text',
      'Les résultats de recherche sélectionnés ont des statuts empêchant leur' +
        ' modification. Certains critères de modification en masse ne seront donc pas disponibles.',
    );
  }

  assertExistNoWritingRightsDialog() {
    cy.get('span[data-test-dialog-message=true]').should(
      'contain.text',
      'Aucun des éléments sélectionnés ne peut être édité car : soit le statut' +
        " de l'un de leurs jalons associés ne le permet pas, soit vous ne disposez pas de droits suffisants.",
    );
  }

  buildSelector(): string {
    return '[data-test-dialog-id=mass-edit]';
  }
}

export class MassEditRequirementDialog extends AbstractMassEditRequirementDialog {
  readonly treeRefreshUrl: string = 'search/requirement';
  readonly parentSelector: string = 'sqtm-app-requirement-multi-edit-dialog';
  readonly httpMockUrl: string = 'search/requirement/mass-update/*';
}
