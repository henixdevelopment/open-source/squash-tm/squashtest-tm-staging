import {
  basicReferentialData,
  ReferentialDataProviderBuilder,
} from '../../../../utils/referential/referential-data.provider';
import { GridElement } from '../../../elements/grid/grid.element';
import { RequirementSearchModel } from './requirement-search-model';
import {
  buildRequirementSearchGrid,
  buildRequirementSearchPageMock,
  getColumnDataProviderMock,
} from './search-page-utils';
import { ToolbarElement } from '../../../elements/workspace-common/toolbar.element';
import { AlertDialogElement } from '../../../elements/dialog/alert-dialog.element';
import { MassEditRequirementDialog } from './dialogs/mass-edit-requirement.dialog';
import { NoMilestoneSharedDialog } from '../dialogs/no-milestone-shared-dialog';
import { MilestoneAlreadyBoundDialog } from '../dialogs/MilestoneAlreadyBoundDialog';
import { NoPermissionToBindMilestoneDialog } from '../dialogs/NoPermissionToBindMilestoneDialog';
import {
  selectByDataTestElementId,
  selectByDataTestToolbarButtonId,
} from '../../../../utils/basic-selectors';
import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { CheckBoxElement } from '../../../elements/forms/check-box.element';
import { GroupedMultiListElement } from '../../../elements/filters/grouped-multi-list.element';
import { AbstractSearchPage } from '../../abstract-search-page';

export class RequirementSearchPage extends AbstractSearchPage {
  readonly treeRefreshUrl: string = 'search/requirement';
  readonly searchMilestoneUrl: string = 'search/milestones/requirement/*';

  public readonly checkbox = new CheckBoxElement('filtered-extendedHighLvlReqScope');
  public readonly multilistItem = new GroupedMultiListElement();

  constructor(public grid: GridElement) {
    super('sqtm-app-requirement-search-page');
  }

  public static initTestAtPage(
    referentialData: ReferentialDataModel = basicReferentialData,
    initialRows: GridResponse = { dataRows: [] },
    requirementSearchModel: RequirementSearchModel = {
      usersWhoCreatedRequirements: [],
      usersWhoModifiedRequirements: [],
    },
    queryString: string = '',
  ): RequirementSearchPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const pageDataMock = buildRequirementSearchPageMock(requirementSearchModel);
    const grid = buildRequirementSearchGrid(initialRows);
    // visit page
    cy.visit(`search/requirement?${queryString}`);
    // wait for ref data request to fire
    referentialDataProvider.wait();
    pageDataMock.wait();
    // wait for initial tree data request to fire
    return new RequirementSearchPage(grid);
  }

  public static initTestAtPremiumPage(
    referentialData: ReferentialDataModel = basicReferentialData,
    initialRows: GridResponse = { dataRows: [] },
    requirementSearchModel: RequirementSearchModel = {
      usersWhoCreatedRequirements: [],
      usersWhoModifiedRequirements: [],
    },
    queryString: string = '',
  ): RequirementSearchPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();

    const pageDataMock = buildRequirementSearchPageMock(requirementSearchModel);
    const grid = buildRequirementSearchGrid(initialRows);
    // visit page
    cy.visit(`search/requirement?${queryString}`);
    // wait for ref data request to fire
    referentialDataProvider.wait();
    pageDataMock.wait();
    // wait for initial tree data request to fire
    return new RequirementSearchPage(grid);
  }

  showExportDialog(): AlertDialogElement {
    const toolbarElement = new ToolbarElement('requirement-search-toolbar');
    const exportButton = toolbarElement.button('export-button');
    exportButton.assertIsActive();
    exportButton.click();
    return new AlertDialogElement('requirement-export-dialog');
  }

  showMassEditDialog(): MassEditRequirementDialog {
    const toolbarElement = new ToolbarElement('requirement-search-toolbar');
    toolbarElement.button('mass-edit-button').clickWithoutSpan();
    return new MassEditRequirementDialog();
  }
  showMassEditDisabledAlert(): AlertDialogElement {
    const toolbarElement = new ToolbarElement('requirement-search-toolbar');
    toolbarElement.button('mass-edit-button').clickWithoutSpan();
    return new AlertDialogElement('alert');
  }

  openColumnList(activeColumnsIds: string[]): GroupedMultiListElement {
    const groupedMultiListElement = new GroupedMultiListElement();
    const getColumnDataProvider = getColumnDataProviderMock(
      activeColumnsIds,
      'grid/get-column-config',
    );
    cy.get(selectByDataTestToolbarButtonId('open-widget-column-button')).should('exist').click();
    getColumnDataProvider.wait();
    groupedMultiListElement.assertExists();
    return groupedMultiListElement;
  }

  showNoPermissionsToEditMilestoneDialog(): AlertDialogElement {
    const toolbarElement = new ToolbarElement('requirement-search-toolbar');
    toolbarElement.button('edit-milestones-button').clickWithoutSpan();
    return new AlertDialogElement('alert');
  }

  getNoMilestoneSharedDialog(): NoMilestoneSharedDialog {
    return new NoMilestoneSharedDialog();
  }

  getNoPermissionToBindMilestoneDialog(): NoPermissionToBindMilestoneDialog {
    const toolbarElement = new ToolbarElement('requirement-search-toolbar');
    toolbarElement.button('edit-milestones-button').clickWithoutSpan();
    return new NoPermissionToBindMilestoneDialog();
  }

  getMilestoneAlreadyBoundDialog(): MilestoneAlreadyBoundDialog {
    return new MilestoneAlreadyBoundDialog();
  }

  buildToolBarSelector(): string {
    return '[data-test-toolbar-id=requirement-search-toolbar]';
  }

  addCriteriaCriticityandLevelOfCriticity(criticity: string) {
    cy.get(selectByDataTestElementId('grid-filter-manager-add-criteria')).click();
    this.multilistItem.toggleOneItem(`Criticité`);
    cy.get(selectByDataTestElementId('ungrouped-items')).trigger('mouseover');
    this.multilistItem.toggleOneItem(`${criticity}`);
    cy.clickVoid();
  }
}
