import { Page } from '../../../page';
import { GridElement } from '../../../../elements/grid/grid.element';
import { ToolbarElement } from '../../../../elements/workspace-common/toolbar.element';

export class RequirementTestCaseForCampaignSearchPage extends Page {
  readonly toolBarMenu = new ToolbarElement('requirement-search-toolbar');
  readonly linkAllButton = this.toolBarMenu.button('link-all-button');

  constructor(public grid: GridElement) {
    super('sqtm-app-test-case-by-req-for-campaign-workspace-entities-search-page');
  }
}
