import { TreeElement } from '../../../elements/grid/grid.element';
import { selectByDataTestElementId } from '../../../../utils/basic-selectors';
import Chainable = Cypress.Chainable;

export class ProjectScopePage {
  readonly rootSelector = 'sqtm-core-project-scope';
  public readonly tree: TreeElement;

  constructor() {
    this.tree = new TreeElement('requirement-tree-picker');
  }

  assertExists() {
    cy.get(this.rootSelector).should('exist');
  }

  find(selector: string): Chainable {
    return cy.get(this.rootSelector).find(selector);
  }

  findByComponentId(componentId: string): Chainable {
    return this.find(selectByDataTestElementId(componentId));
  }
}
