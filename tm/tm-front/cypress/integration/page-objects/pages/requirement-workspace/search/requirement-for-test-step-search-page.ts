import { Page } from '../../page';
import {
  basicReferentialData,
  ReferentialDataProviderBuilder,
} from '../../../../utils/referential/referential-data.provider';
import { GridElement } from '../../../elements/grid/grid.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { RequirementSearchModel } from './requirement-search-model';
import { ToggleIconElement } from '../../../elements/workspace-common/toggle-icon.element';
import { ChangeCoverageOperationReport } from '../../../../../../projects/sqtm-core/src/lib/model/change-coverage-operation-report';
import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class RequirementForTestStepSearchPage extends Page {
  private linkSelectionButton = new ToggleIconElement('link-selection-button');
  private linkAllButton = new ToggleIconElement('link-all-button');
  private cancelButton = new ToggleIconElement('cancel-button');

  constructor(public grid: GridElement) {
    super('sqtm-app-requirement-for-coverage-search-page');
  }

  public static initTestAtPage(
    testCaseId: string,
    testStepId: string,
    referentialData: ReferentialDataModel = basicReferentialData,
    initialRows: GridResponse = { dataRows: [] },
    requirementSearchModel: RequirementSearchModel = {
      usersWhoCreatedRequirements: [],
      usersWhoModifiedRequirements: [],
    },
    queryString: string = '',
  ): RequirementForTestStepSearchPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const pageDataMock = new HttpMockBuilder<RequirementSearchModel>('search/requirement')
      .responseBody(requirementSearchModel)
      .build();
    const grid = GridElement.createGridElement(
      'requirement-search',
      'search/requirement',
      initialRows,
    );
    // visit page
    cy.visit(`search/requirement/coverage/${testCaseId}/${testStepId}?${queryString}`);
    // wait for ref data request to fire
    referentialDataProvider.wait();
    pageDataMock.wait();
    // wait for initial tree data request to fire
    return new RequirementForTestStepSearchPage(grid);
  }

  selectRowsWithMatchingCellContent(cellId: string, contents: string[]) {
    this.grid.selectRowsWithMatchingCellContent(cellId, contents);
  }

  selectRowWithMatchingCellContent(cellId: string, content: string) {
    this.grid.selectRowWithMatchingCellContent(cellId, content);
  }

  foldFilterPanel() {
    this.findByElementId('fold-filter-panel-button').click();
  }

  assertLinkSelectionButtonExist() {
    this.linkSelectionButton.assertExists();
  }

  assertLinkSelectionButtonIsActive() {
    this.linkSelectionButton.assertIsActive();
  }

  assertLinkSelectionButtonIsNotActive() {
    this.linkSelectionButton.assertIsNotActive();
  }

  assertLinkAllButtonExist() {
    this.linkAllButton.assertExists();
  }

  assertLinkAllButtonIsActive() {
    this.linkAllButton.assertIsActive();
  }

  assertNavigateBackButtonExist() {
    this.cancelButton.assertExists();
  }

  assertNavigateBackButtonIsActive() {
    this.cancelButton.assertIsActive();
  }

  linkSelection(
    testCaseId = '*',
    testStepId = '*',
    response: ChangeCoverageOperationReport = {
      coverages: [],
      summary: {
        alreadyVerifiedRejections: null,
        noVerifiableVersionRejections: null,
        notLinkableRejections: null,
      },
    },
  ) {
    const mock = this.buildLinkRequestMock(testCaseId, testStepId, response);
    this.linkSelectionButton.click();
    mock.wait();
  }

  linkAll(
    testCaseId = '*',
    testStepId = '*',
    response: ChangeCoverageOperationReport = {
      coverages: [],
      summary: {
        alreadyVerifiedRejections: null,
        noVerifiableVersionRejections: null,
        notLinkableRejections: null,
      },
    },
  ) {
    const mock = this.buildLinkRequestMock(testCaseId, testStepId, response);
    this.linkAllButton.click();
    mock.wait();
  }

  private buildLinkRequestMock(
    testCaseId: string,
    testStepId: string,
    response: ChangeCoverageOperationReport,
  ) {
    return new HttpMockBuilder(`/test-cases/${testCaseId}/steps/${testStepId}/requirement-versions`)
      .post()
      .responseBody(response)
      .build();
  }
}
