import { Page } from '../../page';
import {
  basicReferentialData,
  ReferentialDataProviderBuilder,
} from '../../../../utils/referential/referential-data.provider';
import { GridElement } from '../../../elements/grid/grid.element';
import { HTTP_RESPONSE_STATUS, HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { RequirementSearchModel } from './requirement-search-model';
import { ToggleIconElement } from '../../../elements/workspace-common/toggle-icon.element';
import { BindReqVersionToSprintOperationReport } from '../../../../../../projects/sqtm-core/src/lib/model/change-coverage-operation-report';
import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { AlertDialogElement } from '../../../elements/dialog/alert-dialog.element';

const noExceptionOperationReport: BindReqVersionToSprintOperationReport = {
  linkedReqVersions: [],
  summary: {
    reqVersionsAlreadyLinkedToSprint: [],
    highLevelReqVersionsInSelection: [],
    obsoleteReqVersionsInSelection: [],
  },
};

export class RequirementForSprintSearchPage extends Page {
  private showAllColumnsButton = new ToggleIconElement(
    'show-all-columns-button',
    'data-test-toolbar-button-id',
  );
  private linkSelectionButton = new ToggleIconElement('link-selection-button');
  private linkAllButton = new ToggleIconElement('link-all-button');
  private cancelButton = new ToggleIconElement('cancel-button');

  constructor(public grid: GridElement) {
    super('sqtm-app-requirement-for-sprint-search-page');
  }

  public static initTestAtPage(
    sprintId: string,
    referentialData: ReferentialDataModel = basicReferentialData,
    initialRows: GridResponse = { dataRows: [] },
    requirementSearchModel: RequirementSearchModel = {
      usersWhoCreatedRequirements: [],
      usersWhoModifiedRequirements: [],
    },
    queryString: string = '',
  ): RequirementForSprintSearchPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const pageDataMock = new HttpMockBuilder<RequirementSearchModel>('search/requirement')
      .responseBody(requirementSearchModel)
      .build();
    const grid = GridElement.createGridElement(
      'requirement-search',
      'search/requirement',
      initialRows,
    );
    // visit page
    cy.visit(`search/requirement/sprint/${sprintId}?${queryString}`);
    referentialDataProvider.wait();
    pageDataMock.wait();
    return new RequirementForSprintSearchPage(grid);
  }

  assertShowAllColumnsButtonExist() {
    this.showAllColumnsButton.assertExists();
  }

  assertShowAllColumnsButtonIsActive() {
    this.showAllColumnsButton.assertIsActive();
  }

  assertLinkSelectionButtonExist() {
    this.linkSelectionButton.assertExists();
  }

  assertLinkSelectionButtonIsActive() {
    this.linkSelectionButton.assertIsActive();
  }

  assertLinkSelectionButtonIsNotActive() {
    this.linkSelectionButton.assertIsNotActive();
  }

  assertLinkAllButtonExist() {
    this.linkAllButton.assertExists();
  }

  assertLinkAllButtonIsActive() {
    this.linkAllButton.assertIsActive();
  }

  assertNavigateBackButtonExist() {
    this.cancelButton.assertExists();
  }

  assertNavigateBackButtonIsActive() {
    this.cancelButton.assertIsActive();
  }

  linkSelection(response: BindReqVersionToSprintOperationReport = noExceptionOperationReport) {
    const mock = this.buildLinkRequestMock(response);
    this.linkSelectionButton.click();
    mock.wait();
  }

  linkAll(response: BindReqVersionToSprintOperationReport = noExceptionOperationReport) {
    const mock = this.buildLinkRequestMock(response);
    this.linkAllButton.click();
    mock.wait();
  }

  linkAllWithSprintClosed(response?: any) {
    const mock = new HttpMockBuilder(`/sprint/*/reqversions`)
      .post()
      .status(HTTP_RESPONSE_STATUS.PRECONDITION_FAIL)
      .responseBody(response)
      .build();
    this.linkAllButton.click();
    mock.wait();
    return new AlertDialogElement('alert');
  }

  private buildLinkRequestMock(response: BindReqVersionToSprintOperationReport) {
    return new HttpMockBuilder(`/sprint/*/reqversions`).post().responseBody(response).build();
  }
}
