import { RequirementSearchModel } from './requirement-search-model';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { GridElement } from '../../../elements/grid/grid.element';
import {
  basicReferentialData,
  ReferentialDataProviderBuilder,
} from '../../../../utils/referential/referential-data.provider';
import { RequirementForCoverageSearchPage } from './requirement-for-coverage-search-page';
import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export function buildRequirementSearchPageMock(
  requirementSearchModel: RequirementSearchModel = {
    usersWhoCreatedRequirements: [],
    usersWhoModifiedRequirements: [],
  },
) {
  return new HttpMockBuilder<RequirementSearchModel>('search/requirement')
    .responseBody(requirementSearchModel)
    .build();
}

export function buildRequirementSearchGrid(initialRows: GridResponse) {
  return GridElement.createGridElement('requirement-search', 'search/requirement', initialRows);
}
export function getColumnDataProviderMock(activeColumnsIds: string[], url: string) {
  return new HttpMockBuilder(url).post().responseBody(activeColumnsIds).build();
}

export function navigateToRequirementSearchForCoverage(
  referentialData: ReferentialDataModel = basicReferentialData,
  initialRows: GridResponse = { dataRows: [] },
) {
  const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
  const pageDataMock = buildRequirementSearchPageMock();
  const grid = buildRequirementSearchGrid(initialRows);
  // visit page
  cy.get('[data-test-icon-id=search-coverages]').click();
  // wait for ref data request to fire
  referentialDataProvider.wait();
  pageDataMock.wait();
  // wait for initial tree data request to fire
  return new RequirementForCoverageSearchPage(grid);
}
