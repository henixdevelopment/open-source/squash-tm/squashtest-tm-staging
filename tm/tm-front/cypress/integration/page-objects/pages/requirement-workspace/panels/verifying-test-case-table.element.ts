import { Page } from '../../page';
import {
  selectByDataTestCellId,
  selectByDataTestDialogButtonId,
} from '../../../../utils/basic-selectors';

export class VerifyingTestCaseTableElement extends Page {
  constructor() {
    super('sqtm-app-verifying-test-case-table');
  }

  isNotEmpty() {
    cy.get(this.rootSelector).should('not.contain.text', 'Aucun élément à afficher');
  }
  clickName(title: string) {
    cy.get(this.rootSelector).find(selectByDataTestCellId('name')).contains(title).click();
  }

  confirm() {
    cy.get(this.rootSelector).find(selectByDataTestDialogButtonId('confirm')).click();
  }
}
