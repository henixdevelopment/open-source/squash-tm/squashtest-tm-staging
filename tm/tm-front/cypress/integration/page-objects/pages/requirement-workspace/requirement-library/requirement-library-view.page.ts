import { EntityViewPage } from '../../page';
import { FolderInformationPanelElement } from '../../../elements/panels/folder-information-panel.element';
import { apiBaseUrl } from '../../../../utils/mocks/request-mock';
import { RequirementStatisticPanelElement } from '../panels/requirement-statistic-panel.element';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';

export class RequirementLibraryViewPage extends EntityViewPage {
  readonly anchors = AnchorsElement.withLinkIds('information', 'dashboard');

  constructor(private libraryId: number | '*') {
    super('sqtm-app-requirement-library-view');
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/requirement-library-view/${this.libraryId}?**`;
    cy.wait(`@${url}`);
  }

  showInformationPanel(): FolderInformationPanelElement {
    this.anchors.clickLink('information');
    return new FolderInformationPanelElement(this.libraryId);
  }

  showDashboard() {
    this.anchors.clickLink('dashboard');
    return new RequirementStatisticPanelElement();
  }
}
