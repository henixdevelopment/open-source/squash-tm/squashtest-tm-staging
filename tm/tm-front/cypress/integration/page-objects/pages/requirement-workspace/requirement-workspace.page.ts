import { TreeElement } from '../../elements/grid/grid.element';
import { ReferentialDataProviderBuilder } from '../../../utils/referential/referential-data.provider';
import { NavBarElement } from '../../elements/nav-bar/nav-bar.element';
import { WorkspaceWithTreePage } from '../workspace-with-tree.page';
import { RequirementWorkspaceTreeMenu } from './tree/requirement-workspace-tree-menu';
import { ReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { ToolbarButtonElement } from '../../elements/workspace-common/toolbar.element';
import { CreateRequirementDialogElement } from './dialogs/create-requirement-dialog.element';
import { CreateHighLevelRequirementDialogElement } from './dialogs/create-high-level-requirement-dialog.element';
import { CreateRequirementFolderDialogElement } from './dialogs/create-requirement-folder-dialog.element';
import { MenuElement } from '../../../utils/menu.element';

export class RequirementWorkspacePage extends WorkspaceWithTreePage {
  public readonly navBar: NavBarElement = new NavBarElement();
  public readonly treeMenu: RequirementWorkspaceTreeMenu;

  public constructor(
    public readonly tree: TreeElement,
    rootSelector: string,
  ) {
    super(tree, rootSelector);
    this.treeMenu = new RequirementWorkspaceTreeMenu();
  }

  public static initTestAtPage(
    initialNodes: GridResponse = { dataRows: [] },
    referentialData?: ReferentialDataModel,
  ): RequirementWorkspacePage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const tree = TreeElement.createTreeElement(
      'requirement-workspace-main-tree',
      'requirement-tree',
      initialNodes,
    );
    // visit page
    cy.visit('requirement-workspace');
    // wait for ref data request to fire
    referentialDataProvider.wait();
    // wait for initial tree data request to fire
    tree.waitInitialDataFetch();
    return new RequirementWorkspacePage(tree, 'sqtm-app-requirement-workspace');
  }

  clickOnCreateButton() {
    const createButtonTestToolbar = new ToolbarButtonElement(
      'sqtm-app-requirement-workspace-tree',
      'create-button',
    );
    createButtonTestToolbar.clickWithoutSpan();
  }

  assertCreateButtonIsGreyed() {
    const createButtonTestToolbar = new ToolbarButtonElement(
      'sqtm-app-requirement-workspace-tree',
      'create-button',
    );
    createButtonTestToolbar.assertIsGreyed();
  }
  openCreateDialog(item: 'new-requirement' | 'new-high-level-requirement' | 'new-folder') {
    const createMenu = new MenuElement('create-menu');
    this.clickOnCreateButton();
    createMenu.item(item).click();
    switch (item) {
      case 'new-requirement':
        return new CreateRequirementDialogElement();
      case 'new-high-level-requirement':
        return new CreateHighLevelRequirementDialogElement();
      case 'new-folder':
        return new CreateRequirementFolderDialogElement();
      default:
        throw new Error(`Unknown item type ${item}`);
    }
  }
}
