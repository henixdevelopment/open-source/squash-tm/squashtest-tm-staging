import { EntityViewPage } from '../../page';
import { FolderInformationPanelElement } from '../../../elements/panels/folder-information-panel.element';
import { apiBaseUrl } from '../../../../utils/mocks/request-mock';
import { RequirementStatisticPanelElement } from '../panels/requirement-statistic-panel.element';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';
import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';

export class RequirementFolderViewPage extends EntityViewPage {
  readonly anchors = AnchorsElement.withLinkIds('information', 'dashboard');
  public readonly nameTextField: EditableTextFieldElement;

  constructor(private folderId?: number | '*') {
    super('sqtm-app-requirement-folder-view');
    this.nameTextField = new EditableTextFieldElement('entity-name');
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/requirement-folder-view/${this.folderId}?**`;
    cy.wait(`@${url}`);
  }

  showInformationPanel(): FolderInformationPanelElement {
    this.anchors.clickLink('information');
    return this.folderId
      ? new FolderInformationPanelElement(this.folderId)
      : new FolderInformationPanelElement();
  }

  showDashboard() {
    this.anchors.clickLink('dashboard');
    return new RequirementStatisticPanelElement();
  }
}
