import { DeleteConfirmDialogElement } from '../../../elements/dialog/delete-confirm-dialog.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class UnbindLowLevelRequirementDialogElement extends DeleteConfirmDialogElement {
  highLevelRequirementVersionId: number;
  linkedLowLevelRequirementIds: number[];

  constructor(highLevelRequirementVersionId: number, linkedLowLevelRequirementIds: number[]) {
    super('confirm-delete');
    this.highLevelRequirementVersionId = highLevelRequirementVersionId;
    this.linkedLowLevelRequirementIds = linkedLowLevelRequirementIds;
  }

  override deleteForFailure(_response: any) {}

  override deleteForSuccess(response?: any, refreshNodeResponse?: GridResponse) {
    const removeMock = new HttpMockBuilder<any>(
      `high-level-requirement/${this.highLevelRequirementVersionId}/unbind-requirement-from-high-level-requirement/${[this.linkedLowLevelRequirementIds]}`,
    )
      .delete()
      .responseBody(response)
      .build();
    const refreshNodeMock = new HttpMockBuilder<any>(`requirement-tree/refresh`)
      .post()
      .responseBody(refreshNodeResponse)
      .build();
    this.clickOnConfirmButton();
    removeMock.wait();
    if (response) {
      refreshNodeMock.wait();
    }
  }
}
