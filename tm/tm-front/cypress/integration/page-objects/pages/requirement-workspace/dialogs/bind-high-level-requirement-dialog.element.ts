import { TreeElement } from '../../../elements/grid/grid.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { LinkedHighLevelRequirement } from '../../../../../../projects/sqtm-core/src/lib/model/requirement/requirement-version.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class BindHighLevelRequirementDialogElement {
  dialogId = 'bind-high-level-requirement-dialog';
  requirementTree: TreeElement;

  constructor(treeResponse: GridResponse) {
    console.log('treeResponse :', treeResponse);
    this.requirementTree = TreeElement.createTreeElement(
      'high-level-requirement-tree-picker',
      'requirement-tree',
      treeResponse,
    );
    console.log('requirementTree :', this.requirementTree);
  }

  assertExists() {
    cy.get(this.buildSelector()).should('exist');
  }

  buildSelector(): string {
    return `[data-test-dialog-id=${this.dialogId}]`;
  }

  confirm(response: LinkedHighLevelRequirement) {
    const mockBuilder = new HttpMockBuilder('high-level-requirement/*/link/*')
      .responseBody(response)
      .post()
      .build();
    cy.get('[data-test-dialog-button-id="confirm"]').click();
    mockBuilder.wait();
  }

  cancel() {
    cy.get('[data-test-dialog-button-id="cancel"]').click();
  }
}
