import { DeleteConfirmDialogElement } from '../../../elements/dialog/delete-confirm-dialog.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class RemoveRequirementVersionLinksDialogElement extends DeleteConfirmDialogElement {
  requirementVersionId: number;
  requirementVersionLinkIds: number[];

  constructor(requirementVersionId: number, requirementVersionLinkIds: number[]) {
    super('confirm-delete');
    this.requirementVersionId = requirementVersionId;
    this.requirementVersionLinkIds = requirementVersionLinkIds;
  }

  override deleteForFailure(_response: any) {}

  override deleteForSuccess(response?: any, refreshNodeResponse?: GridResponse) {
    const removeMock = new HttpMockBuilder<any>(
      `requirement-version/${this.requirementVersionId}/linked-requirement-versions/${[this.requirementVersionLinkIds]}`,
    )
      .delete()
      .responseBody(response)
      .build();
    const refreshNodeMock = new HttpMockBuilder<any>(`requirement-tree/refresh`)
      .post()
      .responseBody(refreshNodeResponse)
      .build();
    this.clickOnConfirmButton();
    removeMock.wait();
    if (response && response.requirementVersionLinks.length === 0) {
      refreshNodeMock.wait();
    }
  }
}
