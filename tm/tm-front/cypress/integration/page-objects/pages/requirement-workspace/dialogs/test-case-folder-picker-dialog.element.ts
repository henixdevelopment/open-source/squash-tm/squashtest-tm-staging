import { TreeElement } from '../../../elements/grid/grid.element';
import { Page } from '../../page';
import {
  selectByDataIcon,
  selectByDataTestDialogButtonId,
  selectByDataTestRowId,
} from '../../../../utils/basic-selectors';

export class TestCaseTreeFolderDialogPickerElement extends Page {
  dialogId = 'test-case-tree-folder-picker-dialog';
  requirementTree: TreeElement;

  constructor() {
    super('sqtm-core-test-case-tree-folder-dialog-picker');
  }
  assertExist() {
    cy.get(this.rootSelector).should('exist');
  }

  confirm() {
    cy.get(this.rootSelector).find(selectByDataTestDialogButtonId('confirm')).click();
  }
  openTree() {
    cy.get(this.rootSelector).find(selectByDataIcon('plus')).click();
  }

  selectDestination(destination: string) {
    cy.get(this.rootSelector).find(selectByDataTestRowId(destination)).eq(1).click();
  }
}
