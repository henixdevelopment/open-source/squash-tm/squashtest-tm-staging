import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { RequirementXlsReport } from '../../../../../../projects/sqtm-core/src/lib/model/requirement/import/import-requirement.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class ImportRequirementDialog {
  chooseImportFile(fileName: string, fileType: string) {
    cy.fixture(fileName, 'binary')
      .as('importFile')
      .get('sqtm-core-attachment-file-selector')
      .find('input[type=file]')
      .then(function (el) {
        const inputFile = el[0];
        const file = Cypress.Blob.binaryStringToBlob(this.importFile, fileType);

        const testFile = new File([file], fileName, {
          type: file.type,
        });
        const dataTransfer = new DataTransfer();
        dataTransfer.items.add(testFile);
        (inputFile as HTMLInputElement).files = dataTransfer.files;
        inputFile.dispatchEvent(new Event('change'));
      });
  }

  clickImport() {
    cy.get('[data-test-dialog-button-id="import"]').click();
  }

  clickCancel() {
    cy.get('[data-test-dialog-button-id="cancel"]').click();
  }

  assertExists() {
    cy.get('[data-test-dialog-id="import-dialog"]').should('exist');
  }

  assertNotExist() {
    cy.get('[data-test-dialog-id="import-dialog"]').should('not.exist');
  }

  assertErrorMessageContent(errorMsg: string) {
    cy.get('[data-test-dialog-span-id="wrong-format"]').should('contain.text', errorMsg);
  }

  clickSimulate(response: RequirementXlsReport) {
    const mockBuilder = new HttpMockBuilder('requirement/importer/xls?frontEndErrorIsHandled=true')
      .responseBody(response)
      .post()
      .build();
    cy.get('[data-test-dialog-button-id="simulate"]').click();
    mockBuilder.wait();
  }

  clickSimulateWithoutSendingRequest() {
    cy.get('[data-test-dialog-button-id="simulate"]').click();
  }

  assertSimulationTableContent() {
    cy.get('[data-test-report-id="xls-report-ok"]').contains('td', 1);
  }

  assertBilanLinkName(expectedLinkName: string) {
    cy.get('[data-test-report-id="xls-report-link"]').should('contain.text', expectedLinkName);
  }

  assertBilanLinkRef(expectedLinkRef: string) {
    cy.get('[data-test-report-id="xls-report-link"]')
      .should('have.attr', 'href')
      .and('contain', expectedLinkRef);
  }

  assertConfirmImportTableContent(fileName: string) {
    cy.get('[data-test-message-id="confirmation-message"]').should(
      'contain.html',
      `<p>Vos données seront importées depuis le fichier <b>${fileName}</b>.</p>
<p>Cette opération ne peut être annulée. Confirmez-vous l'import ?</p>`,
    );
  }

  assertReportImportTableContent() {
    cy.get('[data-test-report-id="xls-report-ok"]').contains('td', 1);
  }

  close() {
    cy.get('[data-test-dialog-button-id="close"]').click();
  }

  clickConfirmAfterSimulate(gridResponse: GridResponse = {} as GridResponse) {
    const mockTree = new HttpMockBuilder('requirement-tree/refresh')
      .responseBody(gridResponse)
      .post()
      .build();
    cy.get('[data-test-dialog-button-id="confirm"]').click();
    mockTree.wait();
  }

  clickConfirm(gridResponse: GridResponse, xlsReport: RequirementXlsReport) {
    const mockBuilder = new HttpMockBuilder('requirement/importer/xls?frontEndErrorIsHandled=true')
      .responseBody(xlsReport)
      .post()
      .build();
    const mockTree = new HttpMockBuilder('requirement-tree/refresh')
      .responseBody(gridResponse)
      .post()
      .build();
    cy.get('[data-test-dialog-button-id="confirm"]').click();
    mockBuilder.wait();
    mockTree.wait();
  }

  assertReportExist(reportId: string) {
    cy.get(`[data-test-report-id="${reportId}"]`).should('exist');
  }

  assertButtonExist(buttonId: string) {
    cy.get(`[data-test-dialog-button-id="${buttonId}"]`).should('exist');
  }
}
