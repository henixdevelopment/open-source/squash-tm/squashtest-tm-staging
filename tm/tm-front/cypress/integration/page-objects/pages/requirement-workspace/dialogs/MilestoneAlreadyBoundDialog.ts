import { AlertDialogElement } from '../../../elements/dialog/alert-dialog.element';

export class MilestoneAlreadyBoundDialog extends AlertDialogElement {
  constructor() {
    super();
  }

  assertMessage() {
    this.assertHasMessage(
      "Au moins une exigence sélectionnée n'a pas été modifiée complètement car au moins " +
        'un des jalons sélectionnés est associé à une autre version de cette exigence.',
    );
  }
}
