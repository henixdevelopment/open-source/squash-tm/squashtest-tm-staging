export class ShowDescriptionChangeDialogElement {
  private oldValue = 'alert-requirement-version-description-changes-old-value';
  private oldValueLabel = 'alert-requirement-version-description-changes-old-value-label';
  private newValue = 'alert-requirement-version-description-changes-new-value';
  private newValueLabel = 'alert-requirement-version-description-changes-new-value-label';

  public constructor(protected dialogId: string) {}

  assertExists() {
    cy.get(this.buildSelector()).should('exist');
  }

  assertNotExist() {
    cy.get(this.buildSelector()).should('not.exist');
  }

  buildSelector(): string {
    return `[data-test-dialog-id=${this.dialogId}]`;
  }

  assertDialogOldValue(value) {
    return cy.get(`[data-test-dialog-id=${this.oldValue}]`).should('contain.html', value);
  }

  assertDialogOldValueLabel(value) {
    return cy.get(`[data-test-dialog-id=${this.oldValueLabel}]`).should('contain.text', value);
  }

  assertDialogNewValue(value) {
    return cy.get(`[data-test-dialog-id=${this.newValue}]`).should('contain.html', value);
  }

  assertDialogNewValueLabel(value) {
    return cy.get(`[data-test-dialog-id=${this.newValueLabel}]`).should('contain.text', value);
  }
}
