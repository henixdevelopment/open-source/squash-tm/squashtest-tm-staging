import { Page } from '../../page';
import {
  selectByDataTestButtonId,
  selectByDataTestElementId,
  selectByDataTestDialogButtonId,
} from '../../../../utils/basic-selectors';

export class TestCaseAiGenerationDialogElement extends Page {
  dialogId = 'ai-generate-test-cases';

  constructor() {
    super('sqtm-app-test-case-ai-generation-dialog');
  }

  confirm() {
    cy.get(this.rootSelector).find(selectByDataTestDialogButtonId('confirm')).click();
  }
  pickDestination() {
    cy.get(this.rootSelector).find(selectByDataTestElementId('pick-destination')).click();
  }

  generateTestCase() {
    cy.get(this.rootSelector).find(selectByDataTestButtonId('generate-test-cases')).click();
  }

  selectGeneratedTestCase(title: string) {
    cy.get(this.rootSelector).find(selectByDataTestButtonId(title)).click();
  }
}
