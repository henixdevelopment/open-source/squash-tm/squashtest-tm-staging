import { SelectFieldElement } from '../../../elements/forms/select-field.element';
import { selectByDataTestDialogButtonId } from '../../../../utils/basic-selectors';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { RequirementVersionLink } from '../../../../../../projects/sqtm-core/src/lib/model/requirement/requirement-version.link';
import { ChangeLinkedRequirementOperationReport } from '../../../../../../projects/sqtm-core/src/lib/model/change-coverage-operation-report';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { BaseDialogElement } from '../../../elements/dialog/base-dialog.element';

export class RequirementLinksTypeDialogElement extends BaseDialogElement {
  readonly selectField: SelectFieldElement = new SelectFieldElement(() =>
    this.findByFieldName('type'),
  );

  constructor() {
    super('requirement-version-link');
  }

  assertRequirementNameIs(reqName: string) {
    this.findByElementId('reqVersionName').should('contain.text', reqName);
  }

  assertRelatedVersionNamesIs(relatedVersionNames: string) {
    this.findByElementId('relatedVersionName').should('contain.text', relatedVersionNames);
  }

  confirmLink(
    response?: ChangeLinkedRequirementOperationReport,
    refreshNodeResponse?: GridResponse,
  ) {
    const mock = new HttpMockBuilder('requirement-version/*/linked-requirement-versions')
      .post()
      .responseBody(response)
      .build();
    const refreshNodeMock = new HttpMockBuilder<any>(`requirement-tree/refresh`)
      .post()
      .responseBody(refreshNodeResponse)
      .build();
    cy.get(selectByDataTestDialogButtonId('confirm')).click({ force: true });
    mock.wait();
    refreshNodeMock.wait();
  }

  updateLink(requirementVersionLinks: RequirementVersionLink[]) {
    const mock = new HttpMockBuilder('requirement-version/*/linked-requirement-versions/update')
      .post()
      .responseBody(requirementVersionLinks)
      .build();
    cy.get(selectByDataTestDialogButtonId('confirm')).click();
    mock.wait();
  }
}
