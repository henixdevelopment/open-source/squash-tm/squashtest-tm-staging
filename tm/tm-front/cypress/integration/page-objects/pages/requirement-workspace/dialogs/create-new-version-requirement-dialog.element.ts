import Chainable = Cypress.Chainable;
import { selectByDataTestDialogButtonId } from '../../../../utils/basic-selectors';

export class CreateNewVersionRequirementDialogElement {
  private rootSelector = 'sqtm-app-new-requirement-version-dialog';

  constructor() {}

  assertExist() {
    cy.get(this.rootSelector).should('exist');
  }

  clickConfirmButton() {
    this.getConfirmButton().click();
  }

  private getConfirmButton(): Chainable {
    return cy.get(this.rootSelector).find(selectByDataTestDialogButtonId('confirm'));
  }
}
