export class ExportRequirementDialog {
  assertExists() {
    cy.get('[data-test-dialog-id="export-dialog"]').should('exist');
  }

  clickCancel() {
    cy.get('[data-test-dialog-button-id="cancel"]').click();
  }

  assertNotExist() {
    cy.get('[data-test-dialog-id="import-dialog"]').should('not.exist');
  }
}
