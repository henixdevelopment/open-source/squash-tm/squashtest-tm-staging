import { CreateEntityDialog } from '../../create-entity-dialog.element';
import { SelectFieldElement } from '../../../elements/forms/select-field.element';
import { ProjectData } from '../../../../../../projects/sqtm-core/src/lib/model/project/project-data.model';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';

export class CreateHighLevelRequirementDialogElement extends CreateEntityDialog {
  criticalitySelectBox = new SelectFieldElement(() => this.findByFieldName('criticality'));
  categorySelectBox = new SelectFieldElement(() => this.findByFieldName('category'));

  constructor(project?: ProjectData, domain?: BindableEntity) {
    super(
      {
        treePath: 'requirement-tree',
        viewPath: 'requirement-view/high-level/current-version',
        newEntityPath: 'new-high-level-requirement',
      },
      project,
      domain,
    );
  }
}
