import { EntityViewPage } from '../../page';
import { RequirementVersionViewInformationPage } from './requirement-version-view-information.page';
import { GridElement, TreeElement } from '../../../elements/grid/grid.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { RemoveVerifyingTestCasesDialogElement } from '../../test-case-workspace/dialogs/remove-verifying-test-cases-dialog.element';
import { RemoveRequirementVersionLinksDialogElement } from '../dialogs/remove-requirement-version-links-dialog.element';
import { RequirementLinksTypeDialogElement } from '../dialogs/requirement-links-type-dialog.element';
import { RequirementVersionViewModificationHistoryPage } from './requirement-version-view-modification-history.page';
import { RequirementVersionViewRatePage } from './requirement-version-view-rate.page';
import { basicReferentialData } from '../../../../utils/referential/referential-data.provider';
import { navigateToTestCaseSearchForCoverage } from '../../test-case-workspace/research/search-page-utils';
import { TestCaseForCoverageSearchPage } from '../../test-case-workspace/research/test-case-for-coverage-search-page';
import { RequirementViewIssuesPage } from './requirement-view-issues.page';
import Chainable = Cypress.Chainable;
import { Identifier } from '../../../../../../projects/sqtm-core/src/lib/model/entity.model';
import { ChangeVerifyingTestCaseOperationReport } from '../../../../../../projects/sqtm-core/src/lib/model/change-coverage-operation-report';
import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';

export class RequirementVersionViewPage extends EntityViewPage {
  readonly verifyingTestCaseTable = new GridElement('requirement-version-view-verifying-tc');
  readonly requirementLinkTable = new GridElement('requirement-version-view-link');
  readonly requirementLowLevelLinkTable = new GridElement('requirement-version-linked-low-level');
  readonly anchors = AnchorsElement.withLinkIds(
    'information',
    'history',
    'rate',
    'issues',
    'linked-test-case',
    'linked-requirement',
    'linked-low-level-requirements',
    'requirement-version-links',
  );

  constructor(
    public requirementVersionId?: number | '*',
    selector = 'sqtm-app-requirement-version-view',
  ) {
    super(selector);
  }

  public checkDataFetched() {
    throw Error('RequirementVersion data should be fetch outside of this page');
  }

  clickInformationAnchorLink(): RequirementVersionViewInformationPage {
    this.anchors.clickLink('information');
    return new RequirementVersionViewInformationPage();
  }

  clickModificationHistoryAnchorLink(data: any): RequirementVersionViewModificationHistoryPage {
    const page = RequirementVersionViewModificationHistoryPage.navigateTo(
      this.requirementVersionId,
      data,
    );
    this.anchors.clickLink('history');
    page.grid.waitInitialDataFetch();
    return page;
  }

  clickRateAnchorLink(): RequirementVersionViewRatePage {
    this.anchors.clickLink('rate');
    return new RequirementVersionViewRatePage();
  }

  navigateToSearchTestCaseForCoverage(
    referentialData: ReferentialDataModel = basicReferentialData,
    initialRows: GridResponse = { dataRows: [] },
  ): TestCaseForCoverageSearchPage {
    return navigateToTestCaseSearchForCoverage(referentialData, initialRows);
  }

  openTestcaseDrawer(response?: any): TreeElement {
    const testcaseTree = TreeElement.createTreeElement(
      'test-case-tree-picker',
      'test-case-tree',
      response,
    );
    cy.get('[data-test-icon-id=add-verifying-test-case]').click();
    testcaseTree.waitInitialDataFetch();
    return testcaseTree;
  }

  openRequirementDrawer(response?: any): TreeElement {
    const requirementTree = TreeElement.createTreeElement(
      'requirement-tree-picker',
      'requirement-tree',
      response,
    );
    cy.get('[data-test-icon-id=add-requirement-links]').click();
    requirementTree.waitInitialDataFetch();
    cy.clickVoid();
    cy.removeNzTooltip();
    return requirementTree;
  }

  private getDropZone(): Chainable<JQuery<HTMLDivElement>> {
    return cy.get(`[data-test-element-id=${REQUIREMENT_VERSION_DROP_ZONE_ID}]`);
  }

  dropTestCaseIntoRequirement(
    requirementVersionId: number,
    refreshedCoverages?: ChangeVerifyingTestCaseOperationReport,
    refreshNodeResponse?: GridResponse,
  ) {
    const url = `requirement-version/${requirementVersionId}/verifying-test-cases`;
    const mock = new HttpMockBuilder(url).post().responseBody(refreshedCoverages).build();
    const refreshNodeMock = new HttpMockBuilder<any>(`requirement-tree/refresh`)
      .post()
      .responseBody(refreshNodeResponse)
      .build();
    this.getDropZone().trigger('mouseup', { force: true });
    if (refreshedCoverages) {
      mock.wait();
    }
    if (refreshNodeResponse) {
      refreshNodeMock.wait();
    }
  }

  dropRequirementIntoRequirement(
    isRelatedIdANodeId: boolean,
    versionNames?: { versionName: string },
  ): RequirementLinksTypeDialogElement {
    const url = `requirement-version/*/linked-requirement-versions/*`;
    const mock = new HttpMockBuilder(url).responseBody(versionNames).build();
    this.getDropZone().trigger('mouseup', { force: true });
    mock.wait();
    return new RequirementLinksTypeDialogElement();
  }

  dropTestCaseIntoRequirementSecondLevelPage(
    requirementVersionId: number,
    refreshedCoverages?: ChangeVerifyingTestCaseOperationReport,
  ) {
    const url = `requirement-version/${requirementVersionId}/verifying-test-cases`;
    const mock = new HttpMockBuilder(url).post().responseBody(refreshedCoverages).build();
    this.getDropZone().trigger('mouseup', { force: true });
    mock.wait();
  }

  closeDrawer() {
    cy.get('[data-test-button-id="close-drawer"]').click();
  }

  showEditTypeDialog(rowId: Identifier): RequirementLinksTypeDialogElement {
    const row = this.requirementLinkTable.getRow(rowId, 'rightViewport');
    row.cell('edit').iconRenderer().click();
    return new RequirementLinksTypeDialogElement();
  }

  showDeleteConfirmTestCaseDialog(requirementVersionId: number, rowId: number) {
    const testCaseTable = this.verifyingTestCaseTable;
    const row = testCaseTable.getRow(rowId, 'rightViewport');
    const cell = row.cell('delete');
    cell.iconRenderer().click();
    return new RemoveVerifyingTestCasesDialogElement(requirementVersionId, [rowId]);
  }

  showDeleteConfirmTestCasesDialogWithTopIcon(requirementVersionId: number, testCasesId: number[]) {
    cy.get(`[data-test-icon-id=remove-verifying-test-case]`).click();
    return new RemoveVerifyingTestCasesDialogElement(requirementVersionId, testCasesId);
  }

  showDeleteConfirmRequirementLinkDialog(requirementVersionId: number, rowId: number) {
    const requirementLinkTable = this.requirementLinkTable;
    const row = requirementLinkTable.getRow(rowId, 'rightViewport');
    const cell = row.cell('delete');
    cell.iconRenderer().click();
    return new RemoveRequirementVersionLinksDialogElement(requirementVersionId, [rowId]);
  }

  showDeleteConfirmRequirementLinksDialog(requirementVersionId: number, rowIds: number[]) {
    cy.get(`[data-test-icon-id=remove-requirement-links]`).click();
    return new RemoveRequirementVersionLinksDialogElement(requirementVersionId, rowIds);
  }

  showIssuesWithoutBindingToBugTracker(response?: any) {
    const httpMock = new HttpMockBuilder('issues/requirement-version/*?frontEndErrorIsHandled=true')
      .responseBody(response)
      .build();
    this.anchors.clickLink('issues');
    httpMock.wait();
    cy.removeNzTooltip();
    return new RequirementViewIssuesPage();
  }

  showIssuesIfBoundToBugTracker(
    modelResponse?: any,
    gridResponse?: GridResponse,
  ): RequirementViewIssuesPage {
    const bugTrackerModel = new HttpMockBuilder(
      'issues/requirement-version/*?frontEndErrorIsHandled=true',
    )
      .responseBody(modelResponse)
      .build();
    const issues = new HttpMockBuilder('issues/requirement-version/*/known-issues/all')
      .responseBody(gridResponse)
      .post()
      .build();
    this.anchors.clickLink('issues');
    bugTrackerModel.wait();
    issues.wait();
    cy.removeNzTooltip();
    return new RequirementViewIssuesPage();
  }

  assertIssuesAnchorsIsActive() {
    this.anchors.assertLinkIsActive('linked-test-case');
  }

  assertIconLinkRequirementDoesNotExist() {
    this.assertIconNotExists('add-requirement-links');
  }
}

const REQUIREMENT_VERSION_DROP_ZONE_ID = 'requirement-version-view';
