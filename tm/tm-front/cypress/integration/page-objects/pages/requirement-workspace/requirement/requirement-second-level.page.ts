import { EntityViewPage } from '../../page';
import { CapsuleElement } from '../../../elements/workspace-common/capsule.element';

import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { ReferentialDataProviderBuilder } from '../../../../utils/referential/referential-data.provider';
import { RequirementVersionViewPage } from './requirement-version-view.page';
import { RequirementViewPage } from './requirement-view.page';
import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';

export class RequirementSecondLevelPage extends EntityViewPage {
  public readonly requirementVersionViewPage: RequirementVersionViewPage;
  public readonly requirementViewPage: RequirementViewPage;
  public readonly statusCapsule: CapsuleElement;
  public readonly criticalityCapsule: CapsuleElement;
  public readonly nameTextField: EditableTextFieldElement;
  public readonly referenceTextField: EditableTextFieldElement;
  constructor() {
    super('sqtm-app-requirement-version-level-two');
    this.requirementVersionViewPage = new RequirementVersionViewPage();
    this.statusCapsule = new CapsuleElement('status');
    this.criticalityCapsule = new CapsuleElement('criticality');
    this.nameTextField = new EditableTextFieldElement('entity-name');
    this.referenceTextField = new EditableTextFieldElement('entity-reference');
  }

  public static navigateToRequirementSecondLevelPage(
    requirementVersionId: number,
    referentialData?: ReferentialDataModel,
  ): RequirementSecondLevelPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    cy.visit(`requirement-workspace/requirement-version/detail/${requirementVersionId}/content`);
    referentialDataProvider.wait();
    return new RequirementSecondLevelPage();
  }
}
