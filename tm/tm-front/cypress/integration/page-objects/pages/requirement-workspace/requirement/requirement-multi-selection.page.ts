import { RequirementStatisticPanelElement } from '../panels/requirement-statistic-panel.element';
import { Page } from '../../page';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { RequirementStatistics } from '../../../../../../projects/sqtm-core/src/lib/model/requirement/requirement-statistics.model';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';
import { selectByDataTestElementId } from '../../../../utils/basic-selectors';

export class RequirementMultiSelectionPage extends Page {
  readonly anchors = AnchorsElement.withLinkIds('dashboard');
  public statistics: RequirementStatistics;

  constructor(statistics?: RequirementStatistics) {
    super('sqtm-app-requirement-workspace-multi-select-view');
    this.statistics = statistics;
  }

  refreshStatistics() {
    const response = {
      statistics: this.statistics,
    };
    const httpMock = new HttpMockBuilder('requirement-workspace-multi-view')
      .post()
      .responseBody(response)
      .build();
    cy.get(selectByDataTestElementId(`refresh-button`)).click();
    httpMock.waitResponseBody();
    return new RequirementStatisticPanelElement();
  }

  showDashboard() {
    this.anchors.clickLink('dashboard');
    return new RequirementStatisticPanelElement();
  }
}
