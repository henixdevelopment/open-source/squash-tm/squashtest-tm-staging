import { EntityViewPage } from '../../page';
import { MilestoneInformationPanel } from '../../test-case-workspace/test-case/test-case-milestone-view.page';
import { RequirementStatisticPanelElement } from '../panels/requirement-statistic-panel.element';
import { selectByDataTestElementId } from '../../../../utils/basic-selectors';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';

export class RequirementMilestoneViewPage extends EntityViewPage {
  readonly anchors = AnchorsElement.withLinkIds('information', 'dashboard');

  public constructor() {
    super('sqtm-app-requirement-milestone-view');
  }

  showInformationPanel(): MilestoneInformationPanel {
    this.anchors.clickLink('information');
    return new MilestoneInformationPanel();
  }

  showDashboard() {
    this.anchors.clickLink('dashboard');
    return new RequirementStatisticPanelElement();
  }

  assertNameEquals(expectedName: string) {
    cy.get(this.rootSelector)
      .find(selectByDataTestElementId('milestone-label'))
      .should('contain.text', expectedName);
  }
}
