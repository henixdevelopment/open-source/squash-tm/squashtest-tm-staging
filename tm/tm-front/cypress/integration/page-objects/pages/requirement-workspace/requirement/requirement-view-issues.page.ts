import { GridElement } from '../../../elements/grid/grid.element';
import { IssuePage } from '../../../elements/issues/issue.page';

export class RequirementViewIssuesPage extends IssuePage {
  constructor() {
    super('sqtm-app-requirement-version-issues');
  }

  get issueGrid() {
    return new GridElement('requirement-version-view-issue');
  }
}
