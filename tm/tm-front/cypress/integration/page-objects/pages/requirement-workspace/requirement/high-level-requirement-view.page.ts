import { apiBaseUrl, HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { HighLevelRequirementVersionPage } from './high-level-requirement-version.page';
import { GridElement, TreeElement } from '../../../elements/grid/grid.element';
import { UnbindLowLevelRequirementDialogElement } from '../dialogs/unbind-low-level-requirement-dialog.element';
import Chainable = Cypress.Chainable;
import { CapsuleElement } from '../../../elements/workspace-common/capsule.element';
import { LinkedLowLevelRequirement } from '../../../../../../projects/sqtm-core/src/lib/model/requirement/requirement-version.model';
import { VerifyingTestCase } from '../../../../../../projects/sqtm-core/src/lib/model/requirement/verifying-test-case';
import { RequirementVersionStatsBundle } from '../../../../../../projects/sqtm-core/src/lib/model/requirement/requirement-version-stats-bundle.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { selectByDataTestElementId } from '../../../../utils/basic-selectors';
import { AbstractRequirementViewPage } from './abstract-requirement-view.page';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';

export class HighLevelRequirementViewPage extends AbstractRequirementViewPage {
  private readonly requirementVersionPage: HighLevelRequirementVersionPage;

  public readonly statusCapsule: CapsuleElement;
  public readonly criticalityCapsule: CapsuleElement;
  readonly anchors = AnchorsElement.withLinkIds(
    'requirement-version-links',
    'linked-low-level-requirements',
    'linked-test-case',
  );

  public get currentVersion() {
    return this.requirementVersionPage;
  }

  get linkedLowLevelRequirementTable() {
    return new GridElement('requirement-version-linked-low-level');
  }

  get verifyingTestCaseTable() {
    return new GridElement('requirement-version-view-verifying-tc');
  }

  constructor(
    private requirementId: number | '*',
    private requirementVersionId: number | '*',
  ) {
    super('sqtm-app-high-level-requirement-view');
    this.requirementVersionPage = new HighLevelRequirementVersionPage(requirementVersionId);
    this.statusCapsule = new CapsuleElement('status');
    this.criticalityCapsule = new CapsuleElement('criticality');
  }

  // The requirement page is responsible for fetching the current requirement version data
  public checkDataFetched() {
    const url = `${apiBaseUrl()}/requirement-view/high-level/current-version/${this.requirementId}?**`;
    cy.wait(`@${url}`);
  }

  openBindStandardRequirementDrawer(response?: any): TreeElement {
    const requirementTree = TreeElement.createTreeElement(
      'high-level-requirement-children-tree-picker',
      'requirement-tree',
      response,
    );
    cy.get('[data-test-icon-id=add-linked-low-level-requirements]').click();
    requirementTree.waitInitialDataFetch();
    cy.clickVoid();
    cy.removeNzTooltip();
    return requirementTree;
  }

  dropRequirementIntoLinkedLowLevelReqTable(
    operationReport?: BindRequirementToHighLevelRequirementOperationReport,
    requirementTreeRefresh?: GridResponse,
  ) {
    if (operationReport && requirementTreeRefresh) {
      const url = `high-level-requirement/*/bind-requirement-to-high-level-requirement/*`;
      const bindingMock = new HttpMockBuilder(url).post().responseBody(operationReport).build();

      const refreshTreeMock = new HttpMockBuilder('requirement-tree/refresh')
        .post()
        .responseBody(requirementTreeRefresh)
        .build();

      this.getDropZoneForLowLevelReqTable().trigger('mouseup', { force: true });

      bindingMock.wait();
      refreshTreeMock.wait();
    } else {
      this.getDropZoneForLowLevelReqTable().trigger('mouseup', { force: true });
    }
  }

  private getDropZoneForLowLevelReqTable(): Chainable<JQuery<any>> {
    return cy.get(selectByDataTestElementId(LOW_LEVEL_REQ_TABLE_DROP_ZONE_ID));
  }

  closeDrawer() {
    cy.get('[data-test-button-id="close-drawer"]').click();
  }

  showUnbindLowLevelReqDialog(highLevelRequirementVersionId: number, rowId: number) {
    const linkedLowLevelRequirementTable = this.linkedLowLevelRequirementTable;
    const row = linkedLowLevelRequirementTable.getRow(rowId, 'rightViewport');
    const cell = row.cell('delete');
    cell.iconRenderer().click();
    return new UnbindLowLevelRequirementDialogElement(highLevelRequirementVersionId, [rowId]);
  }

  showUnbindMultipleLowLevelReqDialog(highLevelRequirementVersionId: number, rowIds: number[]) {
    cy.get(`[data-test-icon-id=remove-linked-low-level-requirements]`).click();
    return new UnbindLowLevelRequirementDialogElement(highLevelRequirementVersionId, rowIds);
  }
}

export interface BindRequirementToHighLevelRequirementOperationReport {
  linkedLowLevelRequirements: LinkedLowLevelRequirement[];
  verifyingTestCases: VerifyingTestCase[];
  requirementStats: Partial<RequirementVersionStatsBundle>;
  nbIssues: number;
  summary: BindRequirementToHighLevelRequirementExceptions;
}

class BindRequirementToHighLevelRequirementExceptions {
  requirementWithNotLinkableStatus: string[];
  alreadyLinked: string[];
  alreadyLinkedToAnotherHighLevelRequirement: string[];
  highLevelRequirementsInSelection: string[];
  childRequirementsInSelection: string[];
}

const LOW_LEVEL_REQ_TABLE_DROP_ZONE_ID = 'linked-low-level-requirement-table';
