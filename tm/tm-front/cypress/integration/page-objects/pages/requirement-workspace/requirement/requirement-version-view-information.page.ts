import { Page } from '../../page';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { EditableSelectFieldElement } from '../../../elements/forms/editable-select-field.element';
import { MilestonesTagFieldElement } from '../../../elements/forms/milestones-tag-field.element';
import { EditableRichTextFieldElement } from '../../../elements/forms/editable-rich-text-field.element';
import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';
import { RequirementMultiVersionPage } from './requirement-multi-version.page';
import { GridElement } from '../../../elements/grid/grid.element';
import {
  selectByDataTestButtonId,
  selectByDataTestElementId,
  selectByDataTestFieldId,
} from '../../../../utils/basic-selectors';
import { BindHighLevelRequirementDialogElement } from '../dialogs/bind-high-level-requirement-dialog.element';
import { Milestone } from '../../../../../../projects/sqtm-core/src/lib/model/milestone/milestone.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { deleteCufTag, fillCuf } from '../../../../scenarios/scenario-parts/custom-field.part';
import { GenericTextFieldElement } from '../../../elements/forms/generic-text-field.element';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';

export abstract class AbstractRequirementVersionViewInformationPage extends Page {
  abstract readonly treeRefreshUrl: string;

  public readonly highLevelRequirementName: GenericTextFieldElement;
  public readonly criticalityField: EditableSelectFieldElement;

  constructor() {
    super('sqtm-app-requirement-view-information');
    this.highLevelRequirementName = new GenericTextFieldElement('high-level-requirement-name');
    this.criticalityField = new EditableSelectFieldElement('requirement-version-criticality');
  }

  get nameTextField(): EditableTextFieldElement {
    const url = `${this.getRootModificationUrl()}/name`;
    return new EditableTextFieldElement('entity-name', url);
  }

  get categoryField() {
    return new EditableSelectFieldElement('requirement-version-category');
  }

  get statusField() {
    return new EditableSelectFieldElement('requirement-version-status');
  }

  get milestoneTagElement() {
    return new MilestonesTagFieldElement(
      'requirement-version-milestones',
      'requirement-version/*/milestones/*',
    );
  }

  get descriptionElement() {
    return new EditableRichTextFieldElement('requirement-version-description');
  }

  rename(newValue: string, refreshedRow: GridResponse = { dataRows: [] }) {
    const refreshMock = this.getRefreshTreeMock(refreshedRow);
    this.nameTextField.setValue(newValue);
    this.nameTextField.confirm();
    refreshMock.wait();
  }

  private getRefreshTreeMock(refreshedRow: GridResponse) {
    return new HttpMockBuilder(this.treeRefreshUrl).post().responseBody(refreshedRow).build();
  }

  changeCategory(category: string, refreshedRow: GridResponse = { dataRows: [] }) {
    const categoryField = this.categoryField;
    const mock = new HttpMockBuilder(`${this.getRootModificationUrl()}/category`).post().build();
    const refreshMock = this.getRefreshTreeMock(refreshedRow);
    categoryField.selectValueNoButton(category);
    mock.wait();
    refreshMock.wait();
    categoryField.checkSelectedOption(category);
  }

  changeStatus(status: string, refreshedRow: GridResponse = { dataRows: [] }) {
    const mock = new HttpMockBuilder(`${this.getRootModificationUrl()}/status`).post().build();
    const refreshMock = this.getRefreshTreeMock(refreshedRow);
    const statusField = this.statusField;
    statusField.selectValueNoButton(status);
    mock.wait();
    refreshMock.wait();
    statusField.checkSelectedOption(status);
  }

  changeCriticality(criticality: string, refreshedRow: GridResponse = { dataRows: [] }) {
    const mock = new HttpMockBuilder(`${this.getRootModificationUrl()}/criticality`).post().build();
    const refreshMock = this.getRefreshTreeMock(refreshedRow);
    this.criticalityField.selectValueNoButton(criticality);
    mock.wait();
    refreshMock.wait();
    this.criticalityField.checkSelectedOption(criticality);
  }

  bindMilestone(milestoneId: number | string, bindableMilestones?: Milestone[]) {
    const milestoneTagElement = this.milestoneTagElement;
    const milestonePickerDialog = milestoneTagElement.openMilestoneDialog();
    milestonePickerDialog.assertExists();
    milestonePickerDialog.selectMilestone(milestoneId);
    if (bindableMilestones) {
      const mock = new HttpMockBuilder('requirement-version/*/milestones/*')
        .post()
        .responseBody(bindableMilestones)
        .build();
      milestonePickerDialog.confirm();
      mock.wait();
    } else {
      milestonePickerDialog.confirm();
    }
  }

  changeDescription(description: string, refreshedRow: GridResponse = { dataRows: [] }) {
    const mock = new HttpMockBuilder(`${this.getRootModificationUrl()}/description`).post().build();
    const refreshMock = this.getRefreshTreeMock(refreshedRow);
    const descriptionElement = this.descriptionElement;
    descriptionElement.enableEditMode();
    descriptionElement.setValue(description);
    descriptionElement.confirm();
    mock.wait();
    refreshMock.wait();
  }

  clickOnVersionLink(
    response?: GridResponse,
    referentialData?: ReferentialDataModel,
  ): RequirementMultiVersionPage {
    const mock = new HttpMockBuilder('referential').responseBody(referentialData).build();
    const grid = GridElement.createGridElement(
      'requirement-versions-grid',
      'requirement-view/versions/*',
      response,
    );
    cy.get('[data-test-field-id="requirement-version-number"]').click();
    mock.wait();
    grid.waitInitialDataFetch();
    return new RequirementMultiVersionPage(grid);
  }

  assertIsLowLevelRequirement() {
    return this.assertRequirementNatureContains('Classique');
  }

  private assertRequirementNatureContains(classique: string) {
    return cy
      .get(selectByDataTestElementId('requirement-nature'))
      .should('contain.text', classique);
  }

  assertIsHighLevelRequirement() {
    return this.assertRequirementNatureContains('Haut niveau');
  }

  private getRootModificationUrl() {
    return `requirement-version/*`;
  }

  openBindHighLevelRequirementSelectorDialog(response: GridResponse) {
    cy.get(`[data-test-button-id=bind-high-level-requirement]`).click();
    const dialog = new BindHighLevelRequirementDialogElement(response);
    dialog.requirementTree.waitInitialDataFetch();
    return dialog;
  }

  openChangeHighLevelRequirementSelectorDialog(response: GridResponse) {
    cy.get(`[data-test-button-id=change-high-level-requirement]`).click({ force: true });
    const dialog = new BindHighLevelRequirementDialogElement(response);
    dialog.requirementTree.waitInitialDataFetch();
    return dialog;
  }

  unlinkHighLevelRequirementSelectorDialog() {
    cy.get(`[data-test-button-id=unlink-high-level-requirement]`).click({ force: true });

    const deleteMock = new HttpMockBuilder('high-level-requirement/*/unlink')
      .delete()
      .responseBody(null)
      .build();

    this.clickConfirmDeleteButton();

    deleteMock.wait();
  }

  checkHighLevelRequirementName(highLevelReqName: string) {
    cy.get(`[data-test-field-id=high-level-requirement-name]`).contains(highLevelReqName);
  }

  clickOnHighLevelRequirementName() {
    cy.get(selectByDataTestFieldId('high-level-requirement-name')).click();
  }

  checkHighLevelRequirementNameDoesNotExist() {
    cy.get(`[data-test-field-id=high-level-requirement-name]`).should('not.exist');
  }

  checkPremiumPluginDisclaimerMessageExists() {
    cy.get(`[data-test-field-id=premium-plugin-for-high-level-req-missing-label]`).should('exist');
  }

  checkPremiumPluginDisclaimerMessageDoesNotExist() {
    cy.get(`[data-test-field-id=premium-plugin-for-high-level-req-missing-label]`).should(
      'not.exist',
    );
  }

  assertBindHighLevelRequirementSelectorButtonsDoNotExist() {
    cy.get(`[data-test-button-id=bind-high-level-requirement]`).should('not.exist');
    cy.get(`[data-test-button-id=change-high-level-requirement]`).should('not.exist');
    cy.get(`[data-test-button-id=unlink-high-level-requirement]`).should('not.exist');
  }

  private clickConfirmDeleteButton() {
    cy.get('sqtm-core-confirm-delete-dialog')
      .find('[data-test-dialog-button-id="confirm"]')
      .click()
      // Then
      .get('sqtm-core-confirm-delete-dialog')
      .should('not.exist');
  }

  clickAndChooseOptionCufValueInList(cufName: string, optionValue: string) {
    fillCuf(cufName, InputType.DROPDOWN_LIST, optionValue);
  }

  addCufTag(cufName: string, cufValue: string) {
    fillCuf(cufName, InputType.TAG, cufValue);
  }

  deleteCufTag(cufName: string, cufValue: string) {
    deleteCufTag(cufName, cufValue);
  }

  addCufDate(cufName: string, cufValue: string) {
    fillCuf(cufName, InputType.DATE_PICKER, cufValue);
  }

  assertVersionNumber(idVersion: string) {
    cy.get(selectByDataTestFieldId('requirement-version-number')).should('contain', idVersion);
  }

  assertButtonChangeHighLevelRequirementIsNotPresent() {
    this.find(selectByDataTestButtonId('change-high-level-requirement')).should('not.exist');
  }

  assertButtonUnlinkHighLevelRequirementIsNotPresent() {
    this.find(selectByDataTestButtonId('unlink-high-level-requirement')).should('not.exist');
  }

  assertButtonBindHighLevelRequirementIsNotPresent() {
    this.find(selectByDataTestButtonId('bind-high-level-requirement')).should('not.exist');
  }
}

export class RequirementVersionViewInformationPage extends AbstractRequirementVersionViewInformationPage {
  readonly treeRefreshUrl: string = 'requirement-tree/refresh';
}

export class RequirementVersionDetailViewInformationPage extends AbstractRequirementVersionViewInformationPage {
  readonly treeRefreshUrl: string = 'requirement-view/versions/*';
}
