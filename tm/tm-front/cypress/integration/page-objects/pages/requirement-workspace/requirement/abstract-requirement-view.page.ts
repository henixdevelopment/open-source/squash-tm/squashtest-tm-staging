import { EntityViewPage } from '../../page';
import { ToolbarButtonElement } from '../../../elements/workspace-common/toolbar.element';
import { MenuElement, MenuItemElement } from '../../../../utils/menu.element';
import { selectByDataTestMenuId } from '../../../../utils/basic-selectors';
import { AlertDialogElement } from '../../../elements/dialog/alert-dialog.element';

export class AbstractRequirementViewPage extends EntityViewPage {
  public readonly actionMenuButton = new ToolbarButtonElement(
    this.rootSelector,
    'action-menu-button',
  );
  public readonly actionMenu = new MenuElement('action-menu');
  public readonly generateTestCaseAiMenuItem = new MenuItemElement(
    selectByDataTestMenuId('action-menu'),
    'ai-test-case-generation',
  );

  clickActionMenuAndAssertGenerateTestCase(shouldExist: boolean) {
    this.actionMenuButton.clickWithoutSpan();
    this.actionMenu.assertExists();
    if (shouldExist) {
      this.generateTestCaseAiMenuItem.assertExists();
      this.generateTestCaseAiMenuItem.assertContains('Générer des cas de test');
    } else {
      this.generateTestCaseAiMenuItem.assertNotExist();
    }
  }

  clickGenerateTestCaseButton() {
    this.actionMenuButton.clickWithoutSpan();
    this.generateTestCaseAiMenuItem.click();
  }

  assertDialogNoUltimateLicenseExists(alertDialog: AlertDialogElement) {
    alertDialog.assertExists();
    alertDialog.assertHasMessage(
      `La création de cas de test par IA est une fonctionnalité expérimentale accessible avec la licence Squash Ultimate. Retrouvez plus d'informations sur notre site`,
    );
    alertDialog.clickOnCloseButton();
    alertDialog.assertNotExist();
  }
}
