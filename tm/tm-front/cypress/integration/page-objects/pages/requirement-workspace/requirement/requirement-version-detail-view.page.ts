import { EntityViewPage } from '../../page';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';
import { RequirementVersionViewPage } from './requirement-version-view.page';
import { CapsuleElement } from '../../../elements/workspace-common/capsule.element';
import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';

export class RequirementVersionDetailViewPage extends EntityViewPage {
  readonly anchors = AnchorsElement.withLinkIds('information', 'history', 'rate', 'issues');
  readonly requirementVersionPage: RequirementVersionViewPage;
  public readonly statusCapsule: CapsuleElement;
  public readonly nameTextField: EditableTextFieldElement;
  public readonly referenceTextField: EditableTextFieldElement;
  constructor(
    public requirementVersionId?: number | '*',
    selector = 'sqtm-app-requirement-version-detail-view',
  ) {
    super(selector);
    this.requirementVersionPage = new RequirementVersionViewPage(requirementVersionId);
    this.statusCapsule = new CapsuleElement('status');
    this.nameTextField = new EditableTextFieldElement('entity-name');
    this.referenceTextField = new EditableTextFieldElement('entity-reference');
  }
}
