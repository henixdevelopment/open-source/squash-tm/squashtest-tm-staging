import { WorkspaceWithGridPage } from '../../page';
import { GridElement } from '../../../elements/grid/grid.element';
import { selectByDataTestButtonId } from '../../../../utils/basic-selectors';

export class RequirementMultiVersionPage extends WorkspaceWithGridPage {
  constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-requirement-multi-version-view');
  }

  clickOnButton(buttonId: string) {
    cy.get(selectByDataTestButtonId(buttonId)).click();
  }
}
