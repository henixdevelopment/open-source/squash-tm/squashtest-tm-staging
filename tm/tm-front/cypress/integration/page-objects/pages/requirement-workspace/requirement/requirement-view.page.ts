import { RequirementVersionViewPage } from './requirement-version-view.page';
import { apiBaseUrl } from '../../../../utils/mocks/request-mock';
import { CapsuleElement } from '../../../elements/workspace-common/capsule.element';
import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';
import { AbstractRequirementViewPage } from './abstract-requirement-view.page';

export class RequirementViewPage extends AbstractRequirementViewPage {
  private readonly requirementVersionPage: RequirementVersionViewPage;
  public readonly statusCapsule: CapsuleElement;
  public readonly criticalityCapsule: CapsuleElement;
  public readonly nameTextField: EditableTextFieldElement;
  public readonly referenceTextField: EditableTextFieldElement;

  public get currentVersion() {
    return this.requirementVersionPage;
  }

  constructor(
    private requirementId?: number | '*',
    private requirementVersionId?: number | '*',
  ) {
    super('sqtm-app-requirement-view');
    this.requirementVersionPage = new RequirementVersionViewPage(requirementVersionId);
    this.statusCapsule = new CapsuleElement('status');
    this.criticalityCapsule = new CapsuleElement('criticality');
    this.nameTextField = new EditableTextFieldElement('entity-name');
    this.referenceTextField = new EditableTextFieldElement('entity-reference');
  }

  // The requirement page is responsible for fetching the current requirement version data
  public checkDataFetched() {
    const url = `${apiBaseUrl()}/requirement-view/current-version/${this.requirementId}?**`;
    cy.wait(`@${url}`);
  }
}
