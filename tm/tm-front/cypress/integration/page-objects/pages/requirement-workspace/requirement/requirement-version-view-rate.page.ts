import { Page } from '../../page';
import { selectByDataTestElementId } from '../../../../utils/basic-selectors';

export class RequirementVersionViewRatePage extends Page {
  constructor() {
    super('sqtm-app-requirement-version-view-rate-panel');
  }

  getTotalStatsElement(): RequirementVersionStatsElement {
    return new RequirementVersionStatsElement('total-stats');
  }

  getCurrentVersionStatsElement(): RequirementVersionStatsElement {
    return new RequirementVersionStatsElement('current-version-stats');
  }

  getChildStatsElement(): RequirementVersionStatsElement {
    return new RequirementVersionStatsElement('child-stats');
  }
}

export class RequirementVersionStatsElement {
  constructor(private componentId: string) {}

  getRedactionRateElement() {
    return new RequirementRateElement(this.componentId, 'redaction-rate');
  }

  getValidationRateElement() {
    return new RequirementRateElement(this.componentId, 'validation-rate');
  }

  getVerificationRateElement() {
    return new RequirementRateElement(this.componentId, 'verification-rate');
  }

  assertExists() {
    cy.get(this.buildSelector()).should('exist');
  }

  buildSelector() {
    return `${selectByDataTestElementId(this.componentId)}`;
  }
}

export class RequirementRateElement {
  constructor(
    private requirementStatsElementId: string,
    private componentId: string,
  ) {}

  assertElementHaveTitle(title: string) {
    cy.get(`${this.buildSelector()} ${selectByDataTestElementId('title')}`).should(
      'contain.text',
      title,
    );
  }

  assertPercentageValue(percentage: string) {
    cy.get(`${this.buildSelector()} ${selectByDataTestElementId('percentage')}`)
      .find('span')
      .should('contain.text', `${percentage}`);
  }

  assertRateValue(rate: string) {
    cy.get(`${this.buildSelector()} ${selectByDataTestElementId('rate')}`).should(
      'contain.text',
      rate,
    );
  }

  assertExists() {
    cy.get(this.buildSelector()).should('exist');
  }

  assertHaveValidColor() {
    cy.get(`${this.buildSelector()} ${selectByDataTestElementId('percentage')}`).should(
      'have.class',
      'percent-valid',
    );
  }

  assertHaveWarningColor() {
    cy.get(`${this.buildSelector()} ${selectByDataTestElementId('percentage')}`).should(
      'have.class',
      'percent-warning',
    );
  }

  assertHaveDangerColor() {
    cy.get(`${this.buildSelector()} ${selectByDataTestElementId('percentage')}`).should(
      'have.class',
      'percent-danger',
    );
  }

  assertHaveUndefinedColor() {
    cy.get(`${this.buildSelector()} ${selectByDataTestElementId('percentage')}`).should(
      'have.class',
      'percent-undefined',
    );
  }

  buildSelector() {
    return `${selectByDataTestElementId(this.requirementStatsElementId)} ${selectByDataTestElementId(this.componentId)}`;
  }
}
