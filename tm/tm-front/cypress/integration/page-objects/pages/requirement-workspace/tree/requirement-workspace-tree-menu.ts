import { TreeToolbarElement } from '../../../elements/workspace-common/tree-toolbar.element';
import { CreateRequirementFolderDialogElement } from '../dialogs/create-requirement-folder-dialog.element';
import { CreateRequirementDialogElement } from '../dialogs/create-requirement-dialog.element';
import { ImportRequirementDialog } from '../dialogs/import-requirement-dialog.element';
import { ExportRequirementDialog } from '../dialogs/export-requirement-dialog.element';
import { CreateHighLevelRequirementDialogElement } from '../dialogs/create-high-level-requirement-dialog.element';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { ProjectData } from '../../../../../../projects/sqtm-core/src/lib/model/project/project-data.model';

export enum RequirementCreateMenuItemIds {
  NEW_REQUIREMENT = 'new-requirement',
  NEW_HIGH_LEVEL_REQUIREMENT = 'new-high-level-requirement',
  NEW_FOLDER = 'new-folder',
  IMPORT = 'import-requirement',
  EXPORT = 'export-requirement',
}

export class RequirementWorkspaceTreeMenu extends TreeToolbarElement {
  constructor() {
    super('requirement-toolbar');
  }

  openCreateFolderDialog(projectData?: ProjectData): CreateRequirementFolderDialogElement {
    const menuItemElement = this.findCreateFolderButton();
    menuItemElement.click();
    return new CreateRequirementFolderDialogElement(
      projectData,
      BindableEntity.REQUIREMENT_VERSION,
    );
  }

  assertCreateFolderButtonIsEnabled() {
    const menuItem = this.findCreateFolderButton();
    menuItem.assertEnabled();
  }

  assertCreateFolderButtonIsDisabled() {
    const menuItem = this.findCreateFolderButton();
    menuItem.assertDisabled();
  }

  private findCreateFolderButton() {
    return this.findCreateMenuButton(RequirementCreateMenuItemIds.NEW_FOLDER);
  }

  private findCreateMenuButton(itemId: RequirementCreateMenuItemIds) {
    const createButton = this.createButton();
    const menuElement = createButton.showMenu();
    return menuElement.item(itemId);
  }

  assertCreateRequirementButtonIsEnabled() {
    const menuButton = this.findCreateRequirementButton();
    menuButton.assertEnabled();
  }

  assertCreateHighLevelRequirementButtonIsEnabled() {
    this.findCreateHighLevelRequirementButton().assertEnabled();
  }

  assertCreateHighLevelRequirementButtonIsDisabled() {
    this.findCreateHighLevelRequirementButton().assertDisabled();
  }

  hideCreateMenu() {
    this.createButton().hideMenu();
  }

  openCreateRequirementDialog() {
    const menuButton = this.findCreateRequirementButton();
    menuButton.click();
    return new CreateRequirementDialogElement();
  }

  openCreateHighLevelRequirementDialog() {
    const menuButton = this.findCreateHighLevelRequirementButton();
    menuButton.click();
    return new CreateHighLevelRequirementDialogElement();
  }

  private findCreateRequirementButton() {
    return this.findCreateMenuButton(RequirementCreateMenuItemIds.NEW_REQUIREMENT);
  }

  private findCreateHighLevelRequirementButton() {
    return this.findCreateMenuButton(RequirementCreateMenuItemIds.NEW_HIGH_LEVEL_REQUIREMENT);
  }

  openImportRequirementDialog(): ImportRequirementDialog {
    const importExportButton = this.importExportButton();
    const menu = importExportButton.showMenu();
    const menuItem = menu.item(RequirementCreateMenuItemIds.IMPORT);
    menuItem.click();
    return new ImportRequirementDialog();
  }

  openExportRequirementDialog() {
    const importExportButton = this.importExportButton();
    const menu = importExportButton.showMenu();
    const menuItem = menu.item(RequirementCreateMenuItemIds.EXPORT);
    menuItem.click();
    return new ExportRequirementDialog();
  }
}
