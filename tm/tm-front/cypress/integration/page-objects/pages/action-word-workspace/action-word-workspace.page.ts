import { WorkspaceWithTreePage } from '../workspace-with-tree.page';
import { NavBarElement } from '../../elements/nav-bar/nav-bar.element';
import { TreeElement } from '../../elements/grid/grid.element';
import { ActionWordWorkspacePageTreeMenu } from './tree/action-word-workspace-tree-menu';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { ReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { ReferentialDataProviderBuilder } from '../../../utils/referential/referential-data.provider';

export class ActionWordWorkspacePage extends WorkspaceWithTreePage {
  public readonly navBar: NavBarElement = new NavBarElement();
  public readonly treeMenu: ActionWordWorkspacePageTreeMenu;

  public constructor(
    public readonly tree: TreeElement,
    rootSelector: string,
  ) {
    super(tree, rootSelector);
    this.treeMenu = new ActionWordWorkspacePageTreeMenu();
  }

  public static initTestAtPage(
    initialNodes: GridResponse = { dataRows: [] },
    referentialData?: ReferentialDataModel,
  ): ActionWordWorkspacePage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const tree = TreeElement.createTreeElement(
      'action-word-workspace-main-tree',
      'action-word-tree',
      initialNodes,
    );
    // visit page
    cy.visit('action-word-workspace');
    // wait for ref data request to fire
    referentialDataProvider.wait();
    // wait for initial tree data request to fire
    tree.waitInitialDataFetch();
    return new ActionWordWorkspacePage(tree, 'app-action-word-workspace');
  }
}
