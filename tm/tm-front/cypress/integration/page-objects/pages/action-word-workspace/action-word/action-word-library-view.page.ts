import { EntityViewPage } from '../../page';

export class ActionWordLibraryViewPage extends EntityViewPage {
  constructor(public actionWordId?: number | string) {
    super('app-action-word-library-view');
  }
}
