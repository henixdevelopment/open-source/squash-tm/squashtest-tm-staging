import { Page } from '../../page';
import { EditableRichTextFieldElement } from '../../../elements/forms/editable-rich-text-field.element';
import { GridElement } from '../../../elements/grid/grid.element';

export class ActionWordLevelTwoPage extends Page {
  readonly descriptionTextField: EditableRichTextFieldElement = new EditableRichTextFieldElement(
    'action-word-description',
  );
  readonly paramGrid = new GridElement('action-word-view-parameters');

  constructor(public actionWordId?: number | string) {
    super('app-action-word-level-two');
  }
}
