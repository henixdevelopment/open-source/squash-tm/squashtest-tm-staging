import { EntityViewPage } from '../../page';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';
import { GridElement } from '../../../elements/grid/grid.element';
import { EditableRichTextFieldElement } from '../../../elements/forms/editable-rich-text-field.element';

export class ActionWordViewPage extends EntityViewPage {
  readonly anchors = AnchorsElement.withLinkIds(
    'information',
    'parameters',
    'test-cases',
    'implementation',
  );
  readonly descriptionTextField: EditableRichTextFieldElement = new EditableRichTextFieldElement(
    'action-word-description',
  );
  readonly testCasesGrid = new GridElement('action-word-view-using-test-cases');
  readonly paramGrid = new GridElement('action-word-view-parameters');
  constructor(public actionWordId?: number | string) {
    super('app-action-word-view');
  }

  checkNameContent(expectedContent: string) {
    cy.get('sqtm-core-fold-button').siblings().contains(expectedContent);
  }
}
