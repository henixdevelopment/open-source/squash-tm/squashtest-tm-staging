import { TreeToolbarElement } from '../../../elements/workspace-common/tree-toolbar.element';

export class ActionWordWorkspacePageTreeMenu extends TreeToolbarElement {
  constructor() {
    super('action-word-toolbar');
  }
}
