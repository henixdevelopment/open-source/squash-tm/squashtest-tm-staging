import { Page } from '../../../page';
import { EditableTextFieldElement } from '../../../../elements/forms/editable-text-field.element';
import { CheckBoxElement } from '../../../../elements/forms/check-box.element';
import { EditableSelectFieldElement } from '../../../../elements/forms/editable-select-field.element';
import { EditableDateFieldElement } from '../../../../elements/forms/editable-date-field.element';
import { EditableRichTextFieldElement } from '../../../../elements/forms/editable-rich-text-field.element';
import { EditableNumericFieldElement } from '../../../../elements/forms/editable-numeric-field.element';
import { EditableDraggableTagFieldElement } from '../../../../elements/forms/editable-draggable-tag-field-element';

export class CustomFieldInformationPanelElement extends Page {
  public readonly labelTextField: EditableTextFieldElement;
  public readonly codeTextField: EditableTextFieldElement;
  public readonly optionalCheckbox: CheckBoxElement;

  public readonly textDefaultValue: EditableTextFieldElement;
  public readonly numericDefaultValue: EditableNumericFieldElement;
  public readonly richTextDefaultValue: EditableRichTextFieldElement;
  public readonly tagDefaultValue: EditableDraggableTagFieldElement;
  public readonly dateDefaultValue: EditableDateFieldElement;
  public readonly checkboxDefaultValue: EditableSelectFieldElement;

  constructor() {
    super('sqtm-app-custom-field-information-panel');

    const urlPrefix = 'custom-fields/*/';

    this.labelTextField = new EditableTextFieldElement('custom-field-label', urlPrefix + 'label');
    this.codeTextField = new EditableTextFieldElement('custom-field-code', urlPrefix + 'code');
    this.optionalCheckbox = new CheckBoxElement(
      () => this.findByFieldName('custom-field-optional'),
      urlPrefix + 'optional',
    );

    const defaultValueUrl = urlPrefix + 'default-value';
    const numericDefaultValueUrl = urlPrefix + 'numeric-default-value';
    const largeDefaultValueUrl = urlPrefix + 'large-default-value';

    this.textDefaultValue = new EditableTextFieldElement(
      'custom-field-default-text-value',
      defaultValueUrl,
    );
    this.numericDefaultValue = new EditableNumericFieldElement(
      'custom-field-default-numeric-value',
      numericDefaultValueUrl,
    );
    this.richTextDefaultValue = new EditableRichTextFieldElement(
      'custom-field-default-rich-text-value',
      largeDefaultValueUrl,
    );
    this.tagDefaultValue = new EditableDraggableTagFieldElement(
      'custom-field-default-tag-value',
      defaultValueUrl,
    );
    this.dateDefaultValue = new EditableDateFieldElement(
      'custom-field-default-date-value',
      defaultValueUrl,
    );
    this.checkboxDefaultValue = new EditableSelectFieldElement(
      'custom-field-default-checkbox-value',
      defaultValueUrl,
    );
  }
}
