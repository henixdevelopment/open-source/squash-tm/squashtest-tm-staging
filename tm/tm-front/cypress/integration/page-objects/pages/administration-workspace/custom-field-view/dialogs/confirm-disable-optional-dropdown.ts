import { BaseDialogElement } from '../../../../elements/dialog/base-dialog.element';

export class ConfirmDisableOptionalDropdown extends BaseDialogElement {
  constructor() {
    super('cuf-disable-optional-dialog');
  }

  assertMessage(currentDefaultOptionLabel: string) {
    const message =
      'Ce champ personnalisé va passer de facultatif à obligatoire.' +
      '\nPour respecter cette nouvelle contrainte, les valeurs non renseignées pour ce champ seront valorisées avec ' +
      `la valeur par défaut: "${currentDefaultOptionLabel}".` +
      '\nCette valorisation est automatique et irréversible.';

    this.assertHasMessage(message);
  }
}
