import { TextFieldElement } from '../../../../elements/forms/TextFieldElement';
import { AbstractCreationDialogElement } from '../../../../elements/dialog/creation-dialog.element';
import { HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import { CustomField } from '../../../../../../../projects/sqtm-core/src/lib/model/customfield/customfield.model';

export class AddCustomFieldOptionDialogElement extends AbstractCreationDialogElement {
  public readonly nameField: TextFieldElement;
  public readonly codeField: TextFieldElement;

  constructor() {
    super('add-custom-field-option');
    this.nameField = new TextFieldElement('label');
    this.codeField = new TextFieldElement('code');
  }

  addOption(response: CustomField) {
    const mock = new HttpMockBuilder('custom-fields/*/options/new')
      .post()
      .responseBody(response)
      .build();

    this.clickOnAddButton();
    mock.wait();
  }
}
