import { Page } from '../../page';
import { CustomFieldInformationPanelElement } from './panels/custom-field-information-panel.element';
import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';
import { CustomFieldOptionsPanelElement } from './panels/custom-field-options-panel.element';
import { CustomFieldBoundProjectsPanelElement } from './panels/custom-field-bound-projects-panel.element';

export class CustomFieldViewPage extends Page {
  public readonly informationPanel = new CustomFieldInformationPanelElement();
  public readonly optionsPanel = new CustomFieldOptionsPanelElement();
  public readonly entityNameField: EditableTextFieldElement;
  public readonly boundProjectsPanel = new CustomFieldBoundProjectsPanelElement();

  constructor() {
    super('sqtm-app-custom-field-view');

    this.entityNameField = new EditableTextFieldElement('entity-name', 'custom-fields/*/name');
  }

  waitInitialDataFetch() {}

  foldGrid() {
    cy.get(`[data-test-element-id="fold-tree-button"]`).click();
  }
}
