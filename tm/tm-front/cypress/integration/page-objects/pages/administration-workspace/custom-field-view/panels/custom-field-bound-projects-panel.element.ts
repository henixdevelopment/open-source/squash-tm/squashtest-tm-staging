import { GridElement } from '../../../../elements/grid/grid.element';
import { GridColumnId } from '../../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';

export class CustomFieldBoundProjectsPanelElement {
  public readonly grid: GridElement;

  constructor() {
    this.grid = GridElement.createGridElement('projects-bound-to-cuf');
  }

  assertProjectNameIs(projectName: string) {
    this.grid.findRowId(GridColumnId.projectName, projectName).then((id) => {
      this.grid.getRow(id).assertExists();
    });
  }

  assertEntityTypeIs(entityType: string) {
    this.grid.findRowId(GridColumnId.bindableEntity, entityType).then((id) => {
      this.grid.getRow(id).assertExists();
    });
  }
}
