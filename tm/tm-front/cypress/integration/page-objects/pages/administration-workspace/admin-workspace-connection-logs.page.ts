import { NavBarElement } from '../../elements/nav-bar/nav-bar.element';
import { GridElement } from '../../elements/grid/grid.element';
import { AdministrationWorkspacePage } from './administration-workspace.page';
import { PageFactory } from '../page';
import { AdminReferentialDataProviderBuilder } from '../../../utils/referential/admin-referential-data.provider';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { selectByDataTestToolbarButtonId } from '../../../utils/basic-selectors';
import { ReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class AdminWorkspaceConnectionLogsPage extends AdministrationWorkspacePage {
  public readonly navBar = new NavBarElement();

  constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-main-user-workspace');
  }

  public static initTestAtPageUsersConnexionLogs: PageFactory<AdminWorkspaceConnectionLogsPage> = (
    initialNodes: GridResponse = { dataRows: [] },
    referentialData?: ReferentialDataModel,
  ) => {
    return AdminWorkspaceConnectionLogsPage.initTestAtPage(
      initialNodes,
      'connectionLogs',
      'users/connection-logs',
      'users/history',
      referentialData,
    );
  };

  public static initTestAtPage: PageFactory<AdminWorkspaceConnectionLogsPage> = (
    initialNodes: GridResponse = { dataRows: [] },
    gridId: string,
    gridUrl: string,
    pageUrl: string,
    referentialData?: ReferentialDataModel,
  ) => {
    const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder(
      referentialData,
    ).build();
    const gridElement = GridElement.createGridElement(gridId, gridUrl, initialNodes);
    // visit page
    cy.visit(`administration-workspace/${pageUrl}`);
    // wait for ref data request to fire
    adminReferentialDataProvider.wait();
    // wait for initial tree data request to fire
    gridElement.waitInitialDataFetch();
    return new AdminWorkspaceConnectionLogsPage(gridElement);
  };

  public openExportDialog(): ExportConnectionLogDialog {
    cy.get(selectByDataTestToolbarButtonId('export-button')).click();
    return new ExportConnectionLogDialog();
  }

  protected getPageUrl(): string {
    return 'users/connection-logs';
  }
}

export class ExportConnectionLogDialog {
  assertExists(): void {
    this.selector.should('exist');
  }

  get selector(): Cypress.Chainable {
    return cy.get('[data-test-dialog-id="export-connection-log"]');
  }

  confirm(): void {
    // We trigger an internal server error on purpose so that the browser native file download isn't triggered.
    // We don't want a file to be downloaded each time the test is run !
    const mock = new HttpMockBuilder('users/connection-logs/export').post().status(500).build();

    cy.get('[data-test-dialog-button-id="export"]').click();
    mock.wait();
  }
}
