import { NavBarElement } from '../../elements/nav-bar/nav-bar.element';
import { GridElement } from '../../elements/grid/grid.element';
import { CreateProjectDialog } from './dialogs/create-project-dialog.element';
import { AdministrationWorkspacePage } from './administration-workspace.page';
import { PageFactory } from '../page';
import { ReferentialDataProviderBuilder } from '../../../utils/referential/referential-data.provider';
import { CreateTemplateDialog } from './dialogs/create-template-dialog.element';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { CreateTemplateFromProjectDialog } from './dialogs/create-template-from-project-dialog.element';
import { ProjectViewMockData, ProjectViewPage } from './project-view/project-view.page';
import { AdminReferentialDataProviderBuilder } from '../../../utils/referential/admin-referential-data.provider';
import {
  selectByDataTestMenuItemId,
  selectByDataTestToolbarButtonId,
} from '../../../utils/basic-selectors';
import { ProjectView } from '../../../../../projects/sqtm-core/src/lib/model/project/project.model';
import { ReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class AdminWorkspaceProjectsPage extends AdministrationWorkspacePage {
  public readonly navBar = new NavBarElement();

  constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-project-workspace');
  }

  public static initTestAtPageProjects: PageFactory<AdminWorkspaceProjectsPage> = (
    initialNodes: GridResponse = { dataRows: [] },
    referentialData?: ReferentialDataModel,
  ) => {
    return AdminWorkspaceProjectsPage.initTestAtPage(
      initialNodes,
      'projects',
      'generic-projects',
      'projects',
      referentialData,
    );
  };

  public static initTestAtPage: PageFactory<AdminWorkspaceProjectsPage> = (
    initialNodes: GridResponse = { dataRows: [] },
    gridId: string,
    gridUrl: string,
    pageUrl: string,
    referentialData?: ReferentialDataModel,
  ) => {
    new ReferentialDataProviderBuilder(referentialData).build();
    new AdminReferentialDataProviderBuilder(referentialData).build();
    const gridElement = GridElement.createGridElement(gridId, gridUrl, initialNodes);

    cy.visit(`administration-workspace/${pageUrl}`);
    gridElement.waitInitialDataFetch();
    return new AdminWorkspaceProjectsPage(gridElement);
  };

  selectProjectByName(
    name: string,
    model?: ProjectView,
    projectViewMockData?: ProjectViewMockData,
  ): ProjectViewPage {
    const projectView = new ProjectViewPage(projectViewMockData);

    const mock = new HttpMockBuilder('project-view/*').responseBody(model).build();
    const statusesInUseMock = new HttpMockBuilder('project-view/*/statuses-in-use')
      .responseBody(model.statusesInUse)
      .build();

    this.selectRowWithMatchingCellContent('name', name);

    mock.wait();
    statusesInUseMock.wait();
    projectView.waitInitialDataFetch();
    projectView.assertExists();

    return projectView;
  }

  openCreateProject(templates: { id: number; name: string }[] = []): CreateProjectDialog {
    const templateListMock = new HttpMockBuilder('generic-projects/templates')
      .responseBody({ templates })
      .build();

    cy.get(selectByDataTestToolbarButtonId('create-button'))
      .click()
      .get(selectByDataTestMenuItemId('new-project'))
      .click();

    templateListMock.wait();

    return new CreateProjectDialog();
  }

  openCreateTemplate(): CreateTemplateDialog {
    cy.get(selectByDataTestToolbarButtonId('create-button'))
      .click()
      .get(selectByDataTestMenuItemId('new-template'))
      .click();

    return new CreateTemplateDialog();
  }

  openCreateTemplateFromProject() {
    cy.get(selectByDataTestToolbarButtonId('create-button'))
      .click()
      .get(selectByDataTestMenuItemId('new-template-from-project'))
      .click()
      .should('not.be.visible');

    return new CreateTemplateFromProjectDialog();
  }

  fillSearchInput(value: string, gridResponse?: GridResponse) {
    const mock = new HttpMockBuilder('generic-projects').post().responseBody(gridResponse).build();

    cy.get('[data-test-element-id="project-filter-field"').find('input').type(value);

    mock.wait();
  }

  protected getPageUrl(): string {
    return 'projects';
  }
}
