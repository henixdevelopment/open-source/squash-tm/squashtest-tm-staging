import { NavBarElement } from '../../elements/nav-bar/nav-bar.element';
import { GridElement } from '../../elements/grid/grid.element';
import { AdministrationWorkspacePage } from './administration-workspace.page';
import { PageFactory } from '../page';
import { HttpMock, HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { CreateBugtrackerDialog } from './dialogs/create-bugtracker-dialog.element';
import { AdminReferentialDataProviderBuilder } from '../../../utils/referential/admin-referential-data.provider';
import { BugTrackerViewPage } from './bug-tracker-view/bug-tracker-view.page';
import { AdminBugTrackerModel } from '../../../../../projects/sqtm-core/src/lib/model/bugtracker/admin-bug-tracker.model';
import { ReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class AdminWorkspaceBugtrackersPage extends AdministrationWorkspacePage {
  public readonly navBar = new NavBarElement();

  private readonly bugtrackerKindMock: HttpMock<any>;

  constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-main-server-workspace');

    this.bugtrackerKindMock = new HttpMockBuilder<any>('bugtracker/get-bugtracker-kinds')
      .responseBody({ bugtrackerKinds: ['Mantis'] })
      .build();
  }

  public static initTestAtPageBugtrackers: PageFactory<AdminWorkspaceBugtrackersPage> = (
    initialNodes: GridResponse = { dataRows: [] },
    referentialData?: ReferentialDataModel,
  ) => {
    return AdminWorkspaceBugtrackersPage.initTestAtPage(
      initialNodes,
      'bugtrackers',
      'bugtrackers',
      'servers/bugtrackers',
      referentialData,
    );
  };

  public static initTestAtPage: PageFactory<AdminWorkspaceBugtrackersPage> = (
    initialNodes: GridResponse = { dataRows: [] },
    gridId: string,
    gridUrl: string,
    pageUrl: string,
    referentialData?: ReferentialDataModel,
  ) => {
    const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder(
      referentialData,
    ).build();
    const gridElement = GridElement.createGridElement(gridId, gridUrl, initialNodes);

    const page = new AdminWorkspaceBugtrackersPage(gridElement);

    // visit page
    cy.visit(`administration-workspace/${pageUrl}`);

    page.assertExists();

    // wait for ref data request to fire
    adminReferentialDataProvider.wait();

    // wait for initial data requests to fire
    page.waitInitialDataFetch();

    return page;
  };

  assertExists() {
    super.assertExists();
  }

  waitInitialDataFetch() {
    super.waitInitialDataFetch();
    this.bugtrackerKindMock.wait();
  }

  openCreateBugtracker(): CreateBugtrackerDialog {
    this.clickCreateButton();
    return new CreateBugtrackerDialog();
  }

  protected getPageUrl(): string {
    return 'bugtrackers';
  }

  protected getDeleteUrl(): string {
    return 'bugtracker';
  }

  selectBugTrackerByName(names: string[], viewResponse?: AdminBugTrackerModel): BugTrackerViewPage {
    const view = new BugTrackerViewPage();
    const mock = new HttpMockBuilder('bugtracker-view/*').responseBody(viewResponse).build();

    this.selectRowsWithMatchingCellContent('name', names);

    mock.waitResponseBody().then(() => {
      view.waitInitialDataFetch();
    });

    return view;
  }
}
