import { NavBarElement } from '../../elements/nav-bar/nav-bar.element';
import { GridElement } from '../../elements/grid/grid.element';
import { PageFactory } from '../page';
import { AdministrationWorkspacePage } from './administration-workspace.page';
import { AdminReferentialDataProviderBuilder } from '../../../utils/referential/admin-referential-data.provider';
import { CreateEnvironmentVariableDialog } from './dialogs/create-environment-variable-dialog';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { selectByDataTestElementId } from '../../../utils/basic-selectors';
import { Identifier } from '../../../../../projects/sqtm-core/src/lib/model/entity.model';
import { ReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class AdminWorkspaceEnvironmentVariablePage extends AdministrationWorkspacePage {
  public readonly navBar = new NavBarElement();

  constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-main-custom-workspace');
  }

  public static initTestAtPageEnvironmentVariables: PageFactory<AdminWorkspaceEnvironmentVariablePage> =
    (initialNodes: GridResponse = { dataRows: [] }, referentialData?: ReferentialDataModel) => {
      return AdminWorkspaceEnvironmentVariablePage.initTestAtPage(
        initialNodes,
        'environmentVariables',
        'environment-variables',
        'entities-customization/environment-variables',
        referentialData,
      );
    };

  public static initTestAtPage: PageFactory<AdminWorkspaceEnvironmentVariablePage> = (
    initialNodes: GridResponse = { dataRows: [] },
    gridId: string,
    gridUrl: string,
    pageUrl: string,
    referentialData?: ReferentialDataModel,
  ) => {
    const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder(
      referentialData,
    ).build();
    const gridElement = GridElement.createGridElement(gridId, gridUrl, initialNodes);
    // visit page
    cy.visit(`administration-workspace/${pageUrl}`);
    // wait for ref data request to fire
    adminReferentialDataProvider.wait();
    // wait for initial tree data request to fire
    gridElement.waitInitialDataFetch();
    return new AdminWorkspaceEnvironmentVariablePage(gridElement);
  };

  protected getPageUrl(): string {
    return 'environment-variables';
  }

  protected getDeleteUrl(): string {
    return 'environment-variables';
  }

  openCreateEnvironmentVariable(): CreateEnvironmentVariableDialog {
    this.clickCreateButton();
    return new CreateEnvironmentVariableDialog();
  }

  deleteSingleEnvironmentVariableInGrid(id: Identifier) {
    const iconRenderer = this.grid.getRow(id, 'rightViewport').cell('delete').iconRenderer();
    iconRenderer.assertIsVisible();
    iconRenderer.click();
  }

  fillSearchInput(value: string, gridResponse?: GridResponse) {
    const mock = new HttpMockBuilder('environment-variables')
      .post()
      .responseBody(gridResponse)
      .build();
    cy.get(selectByDataTestElementId('environment-variable-filter-field'))
      .find('input')
      .type(value);
    mock.wait();
  }
}
