import { GridElement } from '../../../../elements/grid/grid.element';
import { HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import {
  BindCustomFieldToProjectDialog,
  CustomFieldsResponse,
} from '../dialogs/bind-custom-field-to-project.dialog';
import { Page } from '../../../page';

export class CustomFieldsPanelElement extends Page {
  public readonly grid: GridElement;

  constructor() {
    super('sqtm-app-project-custom-fields-panel');
    this.grid = GridElement.createGridElement('project-custom-fields');
  }

  waitInitialDataFetch() {}

  unbindOne(name: string, projectLinkedTemplate: boolean) {
    this.grid.findRowId('name', name).then((id) => {
      this.grid.getRow(id).cell('delete').iconRenderer().click();

      if (!projectLinkedTemplate) {
        const deleteMock = new HttpMockBuilder(
          'custom-field-binding/project/unbind-custom-fields/*',
        )
          .delete()
          .build();

        this.clickConfirmDeleteButton();

        deleteMock.wait();
      }
    });
  }

  unbindMultiple(names: string[], projectLinkedTemplate: boolean) {
    this.grid.selectRowsWithMatchingCellContent('name', names);

    const deleteMock = new HttpMockBuilder('custom-field-binding/project/unbind-custom-fields/*')
      .delete()
      .build();

    this.clickOnDeleteButton();

    if (!projectLinkedTemplate) {
      this.clickConfirmDeleteButton();
      deleteMock.wait();
    }
  }

  openCustomFieldBindingDialog(
    customFields?: CustomFieldsResponse,
  ): BindCustomFieldToProjectDialog {
    const dialog = new BindCustomFieldToProjectDialog(this.grid, customFields);

    cy.get('[data-test-button-id="add-custom-field-binding"]').should('exist').click();

    dialog.waitInitialDataFetch();
    dialog.assertExists();

    return dialog;
  }

  clickOnAddCustomFieldBindingButton() {
    cy.get('[data-test-button-id="add-custom-field-binding"]').should('exist').click();
  }

  private clickOnDeleteButton() {
    cy.get('[data-test-button-id="unbind-custom-fields-from-project"]').should('exist').click();
  }

  private clickConfirmDeleteButton() {
    cy.get('sqtm-core-confirm-delete-dialog')
      .find('[data-test-dialog-button-id="confirm"]')
      .click()
      // Then
      .get('sqtm-core-confirm-delete-dialog')
      .should('not.exist');
  }
}
