import { GridElement } from '../../../../elements/grid/grid.element';
import { HttpMock, HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import { TestAutomationProject } from '../../../../../../../projects/sqtm-core/src/lib/model/test-automation/test-automation-project.model';

export class AddJobDialogElement {
  public readonly baseSelector = '[data-test-dialog-id="add-job-dialog"]';
  public readonly grid: GridElement;
  private initialMock: HttpMock<object>;

  constructor(
    private boundJobsGrid: GridElement,
    availableProjects: TestAutomationProject[],
    isAdmin: boolean = true,
  ) {
    this.grid = GridElement.createGridElement('project-add-jobs');

    if (isAdmin) {
      this.initialMock = new HttpMockBuilder('project-view/*/available-ta-projects')
        .responseBody({ taProjects: availableProjects })
        .build();
    } else {
      this.initialMock = new HttpMockBuilder('project-view/*/restricted-ta-projects')
        .responseBody({ taProjects: availableProjects })
        .build();
    }
  }

  waitForInitialDataFetch() {
    this.initialMock.wait();
  }

  confirm() {
    const mock = new HttpMockBuilder('generic-projects/*/test-automation-projects/new')
      .post()
      .build();
    this.clickConfirmButton();
    mock.wait();
  }

  confirmWithServerSideErrors(error: any) {
    const mock = new HttpMockBuilder('generic-projects/*/test-automation-projects/new')
      .post()
      .status(412)
      .responseBody(error)
      .build();

    this.clickConfirmButton();

    mock.wait();
  }

  confirmWithClientSideErrors() {
    this.clickConfirmButton();
  }

  checkConnectionError(shouldBeVisible: boolean) {
    const chainer = shouldBeVisible ? 'be.visible' : 'not.be.visible';
    cy.get('[data-test-error-key="connection-error"]').should(chainer);
  }

  checkConfirmButtonVisibility(shouldBeVisible: boolean) {
    const chainer = shouldBeVisible ? 'exist' : 'not.exist';
    cy.get(this.baseSelector + ' [data-test-dialog-button-id="confirm"]').should(chainer);
  }

  checkRowIsEditable(jobName: string, shouldBeEditable: boolean) {
    const chainer = shouldBeEditable ? 'exist' : 'not.exist';

    this.grid.findRowId('remoteName', jobName).then((id) => {
      this.grid.getRow(id).cell('label').findCell().find('input').should(chainer);
      this.grid.getRow(id).cell('canRunBdd').checkBoxRender().findCheckbox().should(chainer);
    });
  }

  toggleJobSelection(jobName: string) {
    this.grid.findRowId('remoteName', jobName).then((id) => {
      this.grid.getRow(id).cell('select-row-column').checkBoxRender().toggleState();
    });
  }

  clearJobLabelInTM(jobName: string) {
    this.grid.findRowId('remoteName', jobName).then((id) => {
      this.grid.getRow(id).cell('label').findCell().find('input').clear();
    });
  }

  setJobLabelInTM(jobName: string, newLabel: string) {
    this.grid.findRowId('remoteName', jobName).then((id) => {
      this.grid.getRow(id).cell('label').findCell().find('input').clear().type(newLabel);
    });
  }

  toggleJobCanRunBdd(jobName: string) {
    this.grid.findRowId('remoteName', jobName).then((id) => {
      this.grid.getRow(id).cell('canRunBdd').checkBoxRender().toggleState();
    });
  }

  checkRequiredErrorMessage(shouldBeVisible: boolean) {
    const chainer = shouldBeVisible ? 'exist' : 'not.exist';
    cy.get('[data-test-error-key="Libellé dans Squash: Ce champ ne peut pas être vide."]').should(
      chainer,
    );
  }

  checkDuplicateLabelError(shouldBeVisible: boolean) {
    const chainer = shouldBeVisible ? 'exist' : 'not.exist';
    cy.get(
      '[data-test-error-key="Libellé dans Squash: Doit être unique pour ce projet Squash."]',
    ).should(chainer);
  }

  private clickConfirmButton() {
    cy.get(`${this.baseSelector} [data-test-dialog-button-id="confirm"]`).click();
  }
}
