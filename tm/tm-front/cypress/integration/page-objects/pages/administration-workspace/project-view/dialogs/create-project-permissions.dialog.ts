import { HttpMock, HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import { GroupedMultiListElement } from '../../../../elements/filters/grouped-multi-list.element';
import { SelectFieldElement } from '../../../../elements/forms/select-field.element';
import { GridElement } from '../../../../elements/grid/grid.element';
import { PartyProjectPermission } from '../../../../../../../projects/sqtm-core/src/lib/model/project/project.model';
import { BaseDialogElement } from '../../../../elements/dialog/base-dialog.element';

interface Party {
  id: string;
  label: string;
  groupId: string;
}

export interface UnboundPartiesResponse {
  teams: Party[];
  users: Party[];
}

export class CreateProjectPermissionsDialog extends BaseDialogElement {
  public readonly profileSelectField: SelectFieldElement;

  private unboundPartiesMock: HttpMock<UnboundPartiesResponse>;

  constructor(
    private readonly permissionsGrid: GridElement,
    unboundParties: UnboundPartiesResponse,
  ) {
    super('add-permission-dialog');

    this.unboundPartiesMock = new HttpMockBuilder<UnboundPartiesResponse>(
      'generic-projects/*/unbound-parties',
    )
      .responseBody(unboundParties)
      .build();

    this.profileSelectField = new SelectFieldElement(() =>
      this.findByFieldName('permission-profile'),
    );
  }

  waitInitialDataFetch() {
    this.unboundPartiesMock.wait();
  }

  confirm(updatedPermissions?: PartyProjectPermission[]) {
    const mock = new HttpMockBuilder('generic-projects/*/permissions/*/group/*')
      .responseBody({ partyProjectPermissions: updatedPermissions })
      .post()
      .build();

    this.clickOnConfirmButton();

    mock.wait();
  }

  selectParties(...partyNames: string[]) {
    cy.get('[data-test-element-id="grouped-multi-list-display-value"]').click();
    const multiList = new GroupedMultiListElement();
    partyNames.forEach((name) => multiList.toggleOneItem(name));
    cy.clickVoid();
    multiList.assertNotExist();
  }

  partyListContains(...partyNames: string[]) {
    cy.get('[data-test-element-id="grouped-multi-list-display-value"]').click();
    const multiList = new GroupedMultiListElement();
    partyNames.forEach((name) => multiList.itemContains(name));
  }

  escapeToCloseDialogAndAssertNotExist(this: CreateProjectPermissionsDialog) {
    cy.get('body').type('{esc}');
    this.assertNotExist();
  }

  selectProfile(profile: string) {
    this.profileSelectField.selectValue(profile);
  }

  override clickOnConfirmButton() {
    this.clickOnButton('add');
  }
}
