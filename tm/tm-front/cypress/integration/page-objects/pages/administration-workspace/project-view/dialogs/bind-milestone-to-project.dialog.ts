import { HttpMock, HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import { GridElement } from '../../../../elements/grid/grid.element';
import { Milestone } from '../../../../../../../projects/sqtm-core/src/lib/model/milestone/milestone.model';

export interface MilestonesResponse {
  globalMilestones: Milestone[];
  personalMilestones: Milestone[];
  otherMilestones: Milestone[];
}

export class BindMilestoneToProjectDialog {
  private readonly dialogId = 'bind-milestone-to-project';

  private milestonesMock: HttpMock<MilestonesResponse>;

  public readonly globalMilestonesGrid: GridElement;
  public readonly personalMilestonesGrid: GridElement;
  public readonly otherMilestonesGrid: GridElement;

  constructor(availableMilestones: MilestonesResponse) {
    this.milestonesMock = new HttpMockBuilder<MilestonesResponse>(
      'project-view/*/available-milestones',
    )
      .responseBody(availableMilestones)
      .build();
    this.globalMilestonesGrid = GridElement.createGridElement('global-milestone-picker-grid');
    this.personalMilestonesGrid = GridElement.createGridElement('personal-milestone-picker-grid');
    this.otherMilestonesGrid = GridElement.createGridElement('other-milestone-picker-grid');
  }

  waitInitialDataFetch() {
    this.milestonesMock.wait();
  }

  assertExists() {
    cy.get(this.buildSelector()).should('exist');
  }

  selectMilestones(labels: string[], scope: 'global' | 'personal' | 'other') {
    switch (scope) {
      case 'global':
        cy.get('div[role*="tab"]').contains('Portée globale').click();
        this.globalMilestonesGrid.checkBoxInRowsWithMatchingCellContentInPickerGrid(
          'label',
          labels,
        );
        break;
      case 'personal':
        cy.get('div[role*="tab"]').contains('Possédés').click();
        this.personalMilestonesGrid.checkBoxInRowsWithMatchingCellContentInPickerGrid(
          'label',
          labels,
        );
        break;
      case 'other':
        cy.get('div[role*="tab"]').contains('Autres').click();
        this.otherMilestonesGrid.checkBoxInRowsWithMatchingCellContentInPickerGrid('label', labels);
    }
  }

  confirm() {
    const mock = new HttpMockBuilder('milestone-binding/project/*/bind-milestones/*')
      .post()
      .build();

    this.clickOnConfirmButton();

    mock.wait();
  }

  cancel() {
    const buttonSelector = this.buildButtonSelector('cancel');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  buildSelector(): string {
    return `[data-test-dialog-id=${this.dialogId}]`;
  }

  clickOnConfirmButton() {
    const buttonSelector = this.buildButtonSelector('confirm');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  private buildButtonSelector(buttonId: string) {
    return `${this.buildSelector()} [data-test-dialog-button-id=${buttonId}]`;
  }
}
