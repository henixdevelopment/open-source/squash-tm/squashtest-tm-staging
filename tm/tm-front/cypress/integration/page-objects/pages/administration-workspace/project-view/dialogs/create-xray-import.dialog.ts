import { BaseDialogElement } from '../../../../elements/dialog/base-dialog.element';
import { HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import { TextFieldElement } from '../../../../elements/forms/TextFieldElement';
import {
  selectByDataTestLinkId,
  selectByDataTestRowId,
} from '../../../../../utils/basic-selectors';

export class CreateXrayImportDialog extends BaseDialogElement {
  private readonly importNameField: TextFieldElement = new TextFieldElement('importName');

  constructor() {
    super('import-from-xray-format-dialog');
  }

  fillImportName(name: string) {
    this.importNameField.assertExists();
    this.importNameField.fill(name);
  }

  assertImportNameHasError(errorMessage: string) {
    this.importNameField.assertErrorExist(errorMessage);
  }

  assertImportNameHasNoError(errorMessage: string) {
    this.importNameField.assertErrorNotExist(errorMessage);
  }

  assertImportFileHasMissingFileError() {
    this.findByElementId('error-missing-file').should('exist');
  }

  chooseImportFile(fileName: string, type: string) {
    cy.fixture(fileName).then((f) => {
      this.find('input[type=file]').selectFile(
        {
          contents: Cypress.Buffer.from(f),
          fileName: fileName,
          mimeType: type,
        },
        { force: true },
      );
    });
  }

  assertImportFile(fileName: string) {
    this.find('.file-name')
      .should('contain.text', fileName)
      .get('.remove-file')
      .should('be.visible');
  }

  confirmConfiguration() {
    const mock = new HttpMockBuilder('generic-projects/*/info-from-xray-xml').post().build();
    this.clickOnConfirmButton();
    mock.wait();
  }

  public assertPreviousButtonExists() {
    this.selectButton('previous').should('exist');
  }

  public assertImportButtonExists() {
    this.selectButton('import').should('exist');
  }

  assertXrayEntity(entity: string, issueNumber: number) {
    this.findByElementId(`${entity}-name`).should('not.be.empty');
    this.findByElementId(`${entity}-count`).should('contain.text', issueNumber);
  }

  confirmConfirmation() {
    const mock = new HttpMockBuilder('generic-projects/*/import-from-xray-xml').post().build();
    this.clickOnConfirmButton();
    mock.wait();
  }

  assertSquashEntity(entity: string, success: number, warning: number, error: number) {
    this.find(selectByDataTestRowId(entity)).then((el) => {
      cy.wrap(el).find('.txt-success').should('contain.text', success);
      cy.wrap(el).find('.txt-warn').should('contain.text', warning);
      cy.wrap(el).find('.txt-error').should('contain.text', error);
    });
  }

  assertDownloadReport() {
    this.find(selectByDataTestLinkId('xray-report')).should('exist').click();
  }

  doXrayImport() {
    const mockXray = new HttpMockBuilder('generic-projects/*/import-from-xray-xml/*').get().build();
    const mockPivotRequest = new HttpMockBuilder('generic-projects/*/create-import-request')
      .post()
      .build();
    this.clickOnButton('import');
    mockXray.wait();
    mockPivotRequest.wait();
  }
}
