import { selectByDataTestElementId } from '../../../../../utils/basic-selectors';

export class CleanAutomatedSuitesDialogElement {
  public readonly baseSelector =
    '[data-test-dialog-id="clean-project-automated-suite-and-executions"]';

  assertDialogTitleIs(title: string) {
    cy.get(this.baseSelector)
      .find(selectByDataTestElementId('dialog-title'))
      .should('contain.text', title);
  }
}
