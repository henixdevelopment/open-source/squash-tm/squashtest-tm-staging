import { Page } from '../../page';
import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { TextFieldElement } from '../../../elements/forms/TextFieldElement';
import { EditableSelectFieldElement } from '../../../elements/forms/editable-select-field.element';
import { EditableDraggableTagFieldElement } from '../../../elements/forms/editable-draggable-tag-field-element';
import { EditableRichTextFieldElement } from '../../../elements/forms/editable-rich-text-field.element';
import { RemoveProjectDialog } from '../dialogs/remove-project-dialog.element';
import { PermissionsPanelElement } from './panels/project-permissions-panel.element';
import { ProjectAutomationPanelElement } from './panels/project-automation-panel.element';
import { ChangeExecutionStatusInUseDialog } from '../dialogs/change-execution-status-in-use-dialog.element';
import { CustomFieldsPanelElement } from './panels/project-custom-fields-panel.element';
import { InfoListsPanelElement } from './panels/project-info-lists-panel.element';
import { MilestonesPanelElement } from './panels/project-milestones-panel.element';
import {
  selectByDataTestButtonId,
  selectByDataTestElementId,
  selectByDataTestFieldId,
  selectByDataTestMenuItemId,
} from '../../../../utils/basic-selectors';
import { AssociateProjectToTemplateDialog } from './dialogs/associate-project-to-template.dialog';
import { ProjectPluginsPanelElement } from './panels/project-plugins-panel.element';
import { makeProjectViewData } from '../../../../data-mock/administration-views.data-mock';
import { ProjectBoundToTemplatePanelElement } from './panels/project-bound-to-template-panel.element';
import { BugTracker } from '../../../../../../projects/sqtm-core/src/lib/model/bugtracker/bug-tracker.model';
import { ProjectDataInfo } from '../../../../../../projects/sqtm-core/src/lib/model/project/project-data.model';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';
import { ProjectAiServerPanelElement } from './panels/project-ai-server-panel.element';
import { ProjectImportsPanelElement } from './panels/project-imports-panel.element';

export class ProjectViewPage extends Page {
  public readonly descriptionTextField: EditableRichTextFieldElement;
  public readonly labelTextField: EditableTextFieldElement;
  public readonly nameTextField: EditableTextFieldElement;
  public readonly linkedTemplateField: TextFieldElement;

  public readonly bugtrackerSelectField: EditableSelectFieldElement;
  public readonly projectNamesInBugtracker: EditableDraggableTagFieldElement;

  public readonly permissionsPanel: PermissionsPanelElement;
  public readonly customFieldsPanel: CustomFieldsPanelElement;
  public readonly infoListsPanel: InfoListsPanelElement;
  public readonly milestonesPanel: MilestonesPanelElement;
  public readonly automationPanel: ProjectAutomationPanelElement;
  public readonly pluginPanel: ProjectPluginsPanelElement;
  public readonly projectBoundToTemplatePanel: ProjectBoundToTemplatePanelElement;
  public readonly aiServerPanel: ProjectAiServerPanelElement;
  public readonly importsPanel: ProjectImportsPanelElement;

  public readonly anchors = AnchorsElement.withLinkIds(
    'information',
    'bugtracker',
    'permissions',
    'execution',
    'automation',
    'custom-fields',
    'info-lists',
    'milestones',
    'projectAssociatedToTemplate',
    'plugins',
    'imports',
  );

  constructor(mockData: ProjectViewMockData = defaultMockData) {
    super('sqtm-app-project-view > sqtm-core-entity-view-wrapper > div');

    // merge provided options with the default ones
    Object.keys(defaultMockData).forEach((key) => {
      mockData[key] = mockData[key] || defaultMockData[key];
    });

    this.nameTextField = new EditableTextFieldElement('entity-name', 'generic-projects/*/name');
    this.labelTextField = new EditableTextFieldElement('project-label', 'generic-projects/*/label');
    this.descriptionTextField = new EditableRichTextFieldElement('project-description');
    this.linkedTemplateField = new TextFieldElement('linked-template');
    this.bugtrackerSelectField = new EditableSelectFieldElement(
      'bugtracker-select-field',
      'generic-projects/*/bugtracker',
    );
    this.projectNamesInBugtracker = new EditableDraggableTagFieldElement(
      'project-names-tag-field',
      'project-view/*/bugtracker/projectNames',
    );

    this.permissionsPanel = new PermissionsPanelElement();
    this.automationPanel = new ProjectAutomationPanelElement();
    this.customFieldsPanel = new CustomFieldsPanelElement();
    this.infoListsPanel = new InfoListsPanelElement();
    this.milestonesPanel = new MilestonesPanelElement();
    this.pluginPanel = new ProjectPluginsPanelElement();
    this.projectBoundToTemplatePanel = new ProjectBoundToTemplatePanelElement();
    this.aiServerPanel = new ProjectAiServerPanelElement();
    this.importsPanel = new ProjectImportsPanelElement();
  }

  waitInitialDataFetch() {
    this.permissionsPanel.waitInitialDataFetch();
    this.automationPanel.waitInitialDataFetch();
  }

  foldGrid() {
    cy.get(`[data-test-element-id="fold-tree-button"]`).click();
  }

  clickOnDeleteProjectMenuItem(dataInfoResponse: ProjectDataInfo): RemoveProjectDialog {
    const httpMock = new HttpMockBuilder('generic-projects/*/has-data')
      .post()
      .responseBody(dataInfoResponse)
      .build();

    cy.get('[data-test-button-id="menu-button"]')
      .should('exist')
      .click()
      .get('[data-test-menu-item-id="delete-project"]')
      .should('exist')
      .click();

    httpMock.wait();

    return new RemoveProjectDialog();
  }

  transformProjectIntoTemplate() {
    const mock = new HttpMockBuilder('projects/coerce-into-template').post().build();

    const response: ProjectDataInfo = {
      hasData: false,
      i18nMessageKey:
        'sqtm-core.administration-workspace.projects.dialog.message.coerce-into-template',
    };

    const httpMock = new HttpMockBuilder('generic-projects/*/has-data')
      .post()
      .responseBody(response)
      .build();

    cy.get('[data-test-button-id="menu-button"]')
      .should('exist')
      .click()
      .get('[data-test-menu-item-id="transform-into-template"]')
      .should('exist')
      .click()
      .get('[data-test-dialog-button-id="confirm"]')
      .click();

    httpMock.wait();
    mock.wait();
  }

  openAssociateToTemplateDialog(templateNames: string[]): AssociateProjectToTemplateDialog {
    const dialog = new AssociateProjectToTemplateDialog(templateNames);

    cy.get('[data-test-button-id="menu-button"] i')
      .should('exist')
      .trigger('mouseenter')
      .get('[data-test-menu-item-id="associate-template"]')
      .should('exist')
      .click({ force: true })
      .then(() => {
        dialog.waitInitialMocks();
      });

    return dialog;
  }

  disassociateProjectFromTemplate() {
    const mock = new HttpMockBuilder('generic-projects/*/linked-template-id')
      .post()
      .responseBody({ ...makeProjectViewData(), linkedTemplate: null })
      .build();

    cy.get('[data-test-button-id="menu-button"]')
      .should('exist')
      .click()
      .wait(100)
      .get('[data-test-menu-item-id="associate-template"]')
      .should('exist')
      .click()
      .get('[data-test-dialog-button-id="confirm"]')
      .click();

    mock.wait();
  }

  checkData(fieldId: string, value: string) {
    cy.get(selectByDataTestFieldId(fieldId)).find('span').should('contain.text', value);
  }

  delete(confirm: boolean) {
    const removeProjectDialog = this.clickOnDeleteProjectMenuItem({
      hasData: false,
      i18nMessageKey: 'sqtm-core.administration-workspace.projects.dialog.message.delete-project',
    });
    if (confirm) {
      removeProjectDialog.deleteForSuccess();
    }
  }

  changeBugtracker(bugtracker: string) {
    const mockProjectNames = new HttpMockBuilder('project-view/*/bugtracker/projectNames')
      .responseBody({
        projectNames: [],
      })
      .build();

    this.bugtrackerSelectField.setAndConfirmValueNoButton(bugtracker);
    mockProjectNames.wait();
  }

  selectNoBugtracker() {
    const mockProjectNames = new HttpMockBuilder('project-view/*/bugtracker/projectNames')
      .responseBody({
        projectNames: null,
      })
      .build();

    this.bugtrackerSelectField.setAndConfirmValueNoButton('Pas de bugtracker');
    mockProjectNames.wait();
  }

  clickOnSwitchAllowTcModifDuringExec() {
    cy.get('[data-test-switch-id="allowTcModifDuringExec"]').find('button').click();
  }

  clickOnSwitchUntestableStatus() {
    cy.get('[data-test-switch-id="untestableStatus"]').find('button').click();
  }

  clickOnSwitchSettledStatus() {
    cy.get('[data-test-switch-id="settledStatus"]').find('button').click();
  }

  openDialogForStatusesInUseWithinProject(selectedStatus: 'UNTESTABLE' | 'SETTLED') {
    const mockStatuses = new HttpMockBuilder(
      'generic-projects/*/get-enabled-execution-status/' + selectedStatus,
    )
      .responseBody({
        statuses: [
          { id: 'SUCCESS', label: 'execution.execution-status.SUCCESS' },
          { id: 'RUNNING', label: 'execution.execution-status.RUNNING' },
          { id: 'FAILURE', label: 'execution.execution-status.FAILURE' },
          { id: 'READY', label: 'execution.execution-status.READY' },
          { id: 'BLOCKED', label: 'execution.execution-status.BLOCKED' },
        ],
      })
      .build();

    if (selectedStatus === 'UNTESTABLE') {
      this.clickOnSwitchUntestableStatus();
    } else {
      this.clickOnSwitchSettledStatus();
    }

    mockStatuses.wait();

    return new ChangeExecutionStatusInUseDialog();
  }

  assertPermissionCount(expected: number): void {
    this.getAnchorCounter('project-view-permission-count').should('contain.text', expected);
  }

  assertCustomFieldCount(expected: number): void {
    this.getAnchorCounter('project-view-custom-field-count').should('contain.text', expected);
  }

  assertMilestoneCount(expected: number): void {
    this.getAnchorCounter('project-view-milestone-count').should('contain.text', expected);
  }

  private getAnchorCounter(componentId: string): Cypress.Chainable {
    return cy.get(`[data-test-element-id="${componentId}"]`);
  }

  showInformationPanel(): this {
    this.anchors.clickLink('information');
    cy.removeNzTooltip();
    return this;
  }

  showAutomationPanel(): ProjectAutomationPanelElement {
    this.anchors.clickLink('automation');
    cy.removeNzTooltip();
    return this.automationPanel;
  }

  showCustomFieldsPanel(): CustomFieldsPanelElement {
    this.anchors.clickLink('custom-fields');
    cy.removeNzTooltip();
    return this.customFieldsPanel;
  }

  showMilestonesPanel(): MilestonesPanelElement {
    this.anchors.clickLink('milestones');
    cy.removeNzTooltip();
    return this.milestonesPanel;
  }

  assertHasBugTrackerProjectPathLabel(): void {
    cy.get(selectByDataTestElementId('project-name-or-path-label')).should(
      'contain.text',
      'Chemin du projet',
    );
  }

  showPermissionsPanel(): this {
    this.anchors.clickLink('permissions');
    cy.removeNzTooltip();
    return this;
  }

  showImportsPanel(): ProjectImportsPanelElement {
    this.anchors.clickLink('imports');
    cy.removeNzTooltip();
    return this.importsPanel;
  }

  checkAssociatedTemplate(expectedName: string) {
    cy.get(selectByDataTestFieldId('linked-template')).should('contain.text', expectedName);
  }

  checkNoAssociatedTemplate() {
    cy.get(selectByDataTestFieldId('linked-template')).should('not.exist');
  }

  checkCannotTransformIntoTemplate() {
    cy.get(selectByDataTestButtonId('menu-button'))
      .should('exist')
      .click()
      .get(selectByDataTestMenuItemId('transform-into-template'))
      .should('not.exist');
  }

  selectAiServer(aiServerName: string) {
    cy.get(this.rootSelector).find(selectByDataTestFieldId('ai-server-options')).click();
    cy.get('nz-option-item').contains(aiServerName).click();
  }
}

export interface ProjectViewMockData {
  availableBugtrackers?: BugTracker[];
  bugtrackerProjectNames?: string[];
}

const defaultMockData: ProjectViewMockData = {
  availableBugtrackers: [],
  bugtrackerProjectNames: [],
};
