import { Page } from '../../../page';
import { GridElement } from '../../../../elements/grid/grid.element';

export class ProjectPluginsPanelElement extends Page {
  public readonly grid: GridElement;

  constructor() {
    super('sqtm-app-project-plugins');
    this.grid = GridElement.createGridElement('project-plugins');
  }
}
