import { HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import { BaseDialogElement } from '../../../../elements/dialog/base-dialog.element';

export class ConfirmInfoListBindingAlert extends BaseDialogElement {
  constructor() {
    super('confirm-info-list-binding-dialog');
  }

  assertMessage() {
    this.assertHasMessage(`La liste sera modifiée. Cette action ne peut être annulée.
Le champ correspondant sera valorisé par la valeur par défaut de la nouvelle liste sélectionnée.
Confirmez-vous la modification de la liste ?`);
  }

  confirmAttributeModification(modifiedAttributeName: 'type' | 'nature' | 'category') {
    const mock = new HttpMockBuilder('info-list-binding/project/*/' + modifiedAttributeName)
      .post()
      .build();
    this.clickOnConfirmButton();
    mock.wait();
  }
}
