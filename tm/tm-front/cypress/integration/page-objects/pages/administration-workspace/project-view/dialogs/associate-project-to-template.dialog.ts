import { HttpMock, HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import { SelectFieldElement } from '../../../../elements/forms/select-field.element';
import { makeProjectViewData } from '../../../../../data-mock/administration-views.data-mock';
import { BaseDialogElement } from '../../../../elements/dialog/base-dialog.element';

export class AssociateProjectToTemplateDialog extends BaseDialogElement {
  private templatesMock: HttpMock<any>;

  constructor(templateNames: string[]) {
    super('associate-template-dialog');

    const templates = templateNames.map((templateName, index: number) => ({
      id: index + 1,
      name: templateName,
    }));

    this.templatesMock = new HttpMockBuilder('generic-projects/templates')
      .responseBody({ templates })
      .build();
  }

  waitInitialMocks() {
    this.templatesMock.wait();
  }

  selectTemplateAndConfirm(templateName: string) {
    const selectField = new SelectFieldElement(() => this.findByFieldName('template'));
    selectField.setValue(templateName);

    const mock = new HttpMockBuilder('generic-projects/*/linked-template-id')
      .post()
      .responseBody({ ...makeProjectViewData(), linkedTemplate: templateName })
      .build();

    this.clickOnConfirmButton();

    mock.wait();
  }

  assertBindTemplatePluginConfigurationCheckboxExists(pluginId: string): void {
    this.findByElementId(`keepBinding_${pluginId}`).should('exist');
  }
}
