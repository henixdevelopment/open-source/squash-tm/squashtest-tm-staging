import { EditableSelectFieldElement } from '../../../../elements/forms/editable-select-field.element';
import { selectByDataTestFieldId } from '../../../../../utils/basic-selectors';

export class InfoListsPanelElement {
  public readonly categoryListSelectField: EditableSelectFieldElement;
  public readonly natureListSelectField: EditableSelectFieldElement;
  public readonly typeListSelectField: EditableSelectFieldElement;

  constructor() {
    this.categoryListSelectField = new EditableSelectFieldElement(
      'info-list-category-select-field',
    );
    this.natureListSelectField = new EditableSelectFieldElement('info-list-nature-select-field');
    this.typeListSelectField = new EditableSelectFieldElement('info-list-type-select-field');
  }

  waitInitialDataFetch() {}

  selectCategoryList(categoryList: string) {
    this.setAndConfirmValue(categoryList, 'info-list-category-select-field');
  }

  selectNatureList(natureList: string) {
    this.setAndConfirmValue(natureList, 'info-list-nature-select-field');
  }

  selectTypeList(typeList: string) {
    this.setAndConfirmValue(typeList, 'info-list-type-select-field');
  }

  clickOnDesiredSelectField(selectFieldName: string) {
    cy.get(selectByDataTestFieldId(selectFieldName)).find('span.editable').click();
  }

  private setAndConfirmValue(value: string, selectFieldName: string) {
    cy.get(`[data-test-field-id="${selectFieldName}"] span.editable`).click();
    cy.get('.ant-select-item-option-content').contains(value).click();
  }
}
