import { HttpMock, HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import { GenericTextFieldElement } from '../../../../elements/forms/generic-text-field.element';
import { BaseDialogElement } from '../../../../elements/dialog/base-dialog.element';
import { selectByDataTestButtonId } from '../../../../../utils/basic-selectors';

export class TaServerConnectDialogElement extends BaseDialogElement {
  private readonly usernameField = new GenericTextFieldElement(() =>
    this.findByFieldName('username'),
  );

  private readonly passwordField = new GenericTextFieldElement(() =>
    this.findByFieldName('password'),
  );

  private initialMock: HttpMock<object>;

  constructor() {
    super('taserver-connection');
    this.initialMock = new HttpMockBuilder('/project-view/test-automation-server/*/authentication')
      .post()
      .build();
  }

  public fillUsername(username: string) {
    this.usernameField.rootElement.clear();
    if (username !== '') {
      this.usernameField.rootElement.type(username);
    }
  }

  public fillPassword(password: string) {
    this.passwordField.rootElement.clear();
    if (password !== '') {
      this.passwordField.rootElement.type(password);
    }
  }

  public confirm() {
    this.clickOnConfirmButton();
    this.initialMock.wait();
  }

  override clickOnConfirmButton() {
    this.find(selectByDataTestButtonId('connection')).click();
  }
}
