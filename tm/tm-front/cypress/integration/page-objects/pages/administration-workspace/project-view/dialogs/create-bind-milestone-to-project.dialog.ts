import { TextFieldElement } from '../../../../elements/forms/TextFieldElement';
import { EditableDateFieldElement } from '../../../../elements/forms/editable-date-field.element';
import { HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import { MilestoneAdminProjectView } from '../../../../../../../projects/sqtm-core/src/lib/model/milestone/milestone-admin-project-view.model';
import { BaseDialogElement } from '../../../../elements/dialog/base-dialog.element';

export interface MilestoneCreationProjectViewResponse {
  milestone: MilestoneAdminProjectView;
}

export class CreateBindMilestoneToProjectDialog extends BaseDialogElement {
  private readonly nameField: TextFieldElement;
  private readonly deadlineDate: EditableDateFieldElement;

  constructor() {
    super('create-bind-milestone-dialog');
    this.nameField = new TextFieldElement('label');
    this.deadlineDate = new EditableDateFieldElement(() => this.findByFieldName('endDate'));
  }

  fillName(name: string) {
    this.nameField.fill(name);
  }

  setDeadlineDateToToday() {
    this.deadlineDate.setToTodayAndConfirm();
  }

  confirmCreateAndBindToProject(name: string, newMilestone: MilestoneCreationProjectViewResponse) {
    this.fillName(name);
    this.setDeadlineDateToToday();
    const mock = new HttpMockBuilder(
      'milestone-binding/project/*/create-milestone-and-bind-to-project',
    )
      .responseBody(newMilestone)
      .post()
      .build();

    this.selectButton('create-and-bind-to-project').should('exist').click();

    mock.wait();
  }

  confirmCreateAndBindToProjectAndObjects(
    name: string,
    newMilestone?: MilestoneCreationProjectViewResponse,
  ) {
    this.fillName(name);
    this.setDeadlineDateToToday();
    const mock = new HttpMockBuilder(
      'milestone-binding/project/*/create-milestone-and-bind-to-project-and-objects',
    )
      .responseBody(newMilestone)
      .post()
      .build();

    this.selectButton('create-and-bind-to-project-and-objects').should('exist').click();

    mock.wait();
  }
}
