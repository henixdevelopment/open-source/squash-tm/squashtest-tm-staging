import { GridElement } from '../../../../elements/grid/grid.element';
import { Page } from '../../../page';

export class ProjectBoundToTemplatePanelElement extends Page {
  public readonly grid: GridElement;

  constructor() {
    super('sqtm-app-project-associated');
    this.grid = GridElement.createGridElement('project-associated-template');
  }
}
