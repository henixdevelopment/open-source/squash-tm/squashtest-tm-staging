import { Page } from '../../../page';
import { GridElement } from '../../../../elements/grid/grid.element';
import { CreateXrayImportDialog } from '../dialogs/create-xray-import.dialog';
import {
  selectByDataTestButtonId,
  selectByDataTestMenuId,
  selectByDataTestMenuItemId,
} from '../../../../../utils/basic-selectors';
import { HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import { GridRowElement } from '../../../../../utils/grid-selectors.builder';
import { BaseDialogElement } from '../../../../elements/dialog/base-dialog.element';

export class ProjectImportsPanelElement extends Page {
  public readonly grid: GridElement;

  constructor() {
    super('sqtm-app-project-imports');
    this.grid = GridElement.createGridElement('project-imports');
  }

  openXrayImportDialog(): CreateXrayImportDialog {
    const dialog = new CreateXrayImportDialog();
    this.find(selectByDataTestButtonId('add-import')).should('exist').click();
    cy.get(selectByDataTestMenuId('add-import-menu')).should('exist');
    cy.get(selectByDataTestMenuItemId('import-from-xray-format')).should('exist').click();
    return dialog;
  }

  assertImportGridRow(id: number, name: string, type: string) {
    const row: GridRowElement = this.grid.getRow(id);
    row.cell('#').findCellTextSpan().should('contain.text', id);
    row.cell('name').findCellTextSpan().should('contain.text', name);
    row.cell('type').findCellTextSpan().should('contain.text', type);
    row.cell('createdBy').findCellTextSpan().should('not.be.empty');
    row.cell('createdOn').findCellTextSpan().should('not.be.empty');
    row.cell('successfullyImportedOn').findCellTextSpan().should('contain.text', '-');
    row.cell('status').findCell().find('.status.status-indicator.pending').should('exist');
  }

  deleteSingleImportInGrid(importName: string) {
    this.grid.findRowId('name', importName).then((id) => {
      const iconRenderer = this.grid.getRow(id).cell('delete').iconRenderer();
      this.grid.scrollPosition('right', 'mainViewport');
      iconRenderer.assertIsVisible();
      iconRenderer.click();
      const deleteMock = new HttpMockBuilder('generic-projects/*/delete-import-request/*')
        .delete()
        .build();
      this.assertAndDelete();
      deleteMock.wait();
    });
  }

  private assertAndDelete() {
    const deleteDialog = new BaseDialogElement('confirm-delete');
    deleteDialog.assertExists();
    deleteDialog.assertCancelButtonExists();
    deleteDialog.assertConfirmButtonExists();
    deleteDialog.clickOnConfirmButton();
  }
}
