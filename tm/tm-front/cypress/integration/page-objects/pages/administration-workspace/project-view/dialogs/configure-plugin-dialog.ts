import { selectByDataTestDialogButtonId } from '../../../../../utils/basic-selectors';
import { Page } from '../../../page';

export class ConfigurePluginDialog extends Page {
  constructor() {
    super('sqtm-app-configure-plugin-dialog');
  }
  close() {
    cy.get(selectByDataTestDialogButtonId('close')).find('span').click();
  }
}
