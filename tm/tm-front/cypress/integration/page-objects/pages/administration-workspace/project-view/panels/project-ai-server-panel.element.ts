import { Page } from '../../../page';
import { EditableSelectFieldElement } from '../../../../elements/forms/editable-select-field.element';
import { HttpMockBuilder } from '../../../../../utils/mocks/request-mock';

export class ProjectAiServerPanelElement extends Page {
  private selectField: EditableSelectFieldElement;

  constructor() {
    super('sqtm-app-project-ai-server-panel');
    this.selectField = new EditableSelectFieldElement('ai-server-options');
  }

  selectAiServer(aiServerValue: string) {
    this.selectField.assertIsEditable();
    const mock = new HttpMockBuilder('generic-projects/*/ai-server')
      .responseBody({ aiServerId: null })
      .post()
      .build();

    this.selectField.setAndConfirmValueNoButton(aiServerValue);
    mock.wait();
    this.selectField.checkSelectedOption(aiServerValue);
  }
}
