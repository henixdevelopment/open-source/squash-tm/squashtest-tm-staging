import { SelectFieldElement } from '../../../../elements/forms/select-field.element';
import { HttpMock, HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import { GridElement } from '../../../../elements/grid/grid.element';
import { GroupedMultiListElement } from '../../../../elements/filters/grouped-multi-list.element';
import { Bindings } from '../../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { CustomField } from '../../../../../../../projects/sqtm-core/src/lib/model/customfield/customfield.model';
import { BaseDialogElement } from '../../../../elements/dialog/base-dialog.element';
import { selectByDataTestElementId } from '../../../../../utils/basic-selectors';

export interface CustomFieldsResponse {
  customFields: CustomField[];
}

export class BindCustomFieldToProjectDialog extends BaseDialogElement {
  public readonly entitySelectField: SelectFieldElement;

  private customFieldsMock: HttpMock<CustomFieldsResponse>;

  constructor(
    private readonly customFieldsGrid: GridElement,
    customFields: CustomFieldsResponse,
  ) {
    super('project-custom-field-binding');

    this.customFieldsMock = new HttpMockBuilder<CustomFieldsResponse>('custom-fields')
      .responseBody(customFields)
      .build();

    this.entitySelectField = new SelectFieldElement(() =>
      this.findByFieldName('bound-entity-field'),
    );
  }

  waitInitialDataFetch() {
    this.customFieldsMock.wait();
  }

  confirm(updatedCustomFieldsBinding?: Bindings) {
    const mock = new HttpMockBuilder('custom-field-binding/project/*/bind-custom-fields')
      .responseBody(updatedCustomFieldsBinding)
      .post()
      .build();

    this.clickOnAddButton();

    mock.wait();
  }

  selectCuf(...cufNames: string[]) {
    cy.get(selectByDataTestElementId('cuf-field')).click();
    const multiList = new GroupedMultiListElement();
    cufNames.forEach((name) => multiList.toggleOneItem(name));
    cy.clickVoid();
    multiList.assertNotExist();
  }

  selectEntity(entity: string) {
    this.entitySelectField.selectValue(entity);
  }
}
