import { ProjectViewMockData, ProjectViewPage } from './project-view.page';

export class ProjectViewDetailPage extends ProjectViewPage {
  constructor(mockData?: ProjectViewMockData) {
    super(mockData);
  }

  assertExists() {
    cy.get('sqtm-app-project-view-detail').should('have.length', 1);
    super.assertExists();
  }

  clickBackButton(): void {
    cy.get(this.rootSelector).find('[data-test-button-id="back"]').click();
  }
}
