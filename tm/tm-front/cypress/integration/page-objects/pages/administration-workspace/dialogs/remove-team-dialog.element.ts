import { RemoveAdministrationEntityDialog } from '../remove-administration-entity-dialog';
import { Identifier } from '../../../../../../projects/sqtm-core/src/lib/model/entity.model';

export class RemoveTeamDialogElement extends RemoveAdministrationEntityDialog {
  constructor(public readonly teamIds?: Identifier[]) {
    super('teams', 'teams', teamIds);
  }
}
