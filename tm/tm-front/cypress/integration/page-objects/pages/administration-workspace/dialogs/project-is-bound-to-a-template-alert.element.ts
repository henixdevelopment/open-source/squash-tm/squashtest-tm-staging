import { AlertDialogElement } from '../../../elements/dialog/alert-dialog.element';

export class ProjectIsBoundToATemplateAlert extends AlertDialogElement {
  constructor() {
    super();
  }

  assertMessage() {
    this.assertHasMessage(
      'Ce projet est lié à un modèle de projet, par conséquent ce paramètre est verrouillé.',
    );
  }
}
