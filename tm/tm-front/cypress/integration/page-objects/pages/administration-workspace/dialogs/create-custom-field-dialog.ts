import { TextFieldElement } from '../../../elements/forms/TextFieldElement';
import { SelectFieldElement } from '../../../elements/forms/select-field.element';
import { CreateAdministrationEntityDialog } from '../create-administration-entity-dialog';
import { RichTextFieldElement } from '../../../elements/forms/RichTextFieldElement';
import { HttpResponseStatus } from '../../../../utils/mocks/request-mock';
import { CheckBoxElement } from '../../../elements/forms/check-box.element';
import { GridElement } from '../../../elements/grid/grid.element';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class CreateCustomFieldDialog extends CreateAdministrationEntityDialog {
  public readonly nameField: TextFieldElement;
  public readonly labelField: TextFieldElement;
  public readonly codeField: TextFieldElement;
  public readonly inputTypeField: SelectFieldElement;
  private readonly optionalField: CheckBoxElement;

  public readonly defaultTextField: TextFieldElement;
  public readonly defaultRichTextField: RichTextFieldElement;
  public readonly defaultNumericField: TextFieldElement;
  public readonly defaultTagsField: SelectFieldElement;

  public readonly dropdownListOptionNameField: TextFieldElement;
  public readonly dropdownListOptionCodeField: TextFieldElement;
  public readonly dropdownListOptionsGrid: GridElement;

  constructor() {
    super('new-entity', 'custom-fields/new', 'custom-fields');
    this.nameField = new TextFieldElement('name');
    this.labelField = new TextFieldElement('label');
    this.codeField = new TextFieldElement('code');
    this.inputTypeField = new SelectFieldElement(() => this.findByFieldName('inputType'));
    this.defaultTextField = new TextFieldElement('defaultText');
    this.defaultRichTextField = new RichTextFieldElement('defaultRichText');
    this.defaultNumericField = new TextFieldElement('defaultNumeric');
    this.defaultTagsField = new SelectFieldElement(() => this.findByFieldName('defaultTags'));
    this.optionalField = new CheckBoxElement(() => this.findByFieldName('optional'));

    this.dropdownListOptionNameField = new TextFieldElement('dropdownListOptionName');
    this.dropdownListOptionCodeField = new TextFieldElement('dropdownListOptionCode');
    this.dropdownListOptionsGrid = GridElement.createGridElement('dropdown-list-options');
  }

  fillName(name: string) {
    this.nameField.fill(name);
  }

  toggleOptionalCheck() {
    this.optionalField.toggleState();
  }

  fillDropdownListOptionName(dropdownListOptionName: string) {
    this.dropdownListOptionNameField.fill(dropdownListOptionName);
  }

  fillDropdownListOptionCode(dropdownListOptionCode: string) {
    this.dropdownListOptionCodeField.fill(dropdownListOptionCode);
  }

  addDropdownListOption(dropdownListOptionName: string, dropdownListOptionCode: string) {
    this.fillDropdownListOptionName(dropdownListOptionName);
    this.fillDropdownListOptionCode(dropdownListOptionCode);
    cy.get('button[data-test-dialog-button-id="add-option"').click();
  }

  toggleOptionDefaultValue(optionName: string) {
    cy.get(`sqtm-core-grid-cell[data-test-cell-id="optionName"]`)
      .contains('span', optionName)
      .closest('sqtm-core-grid-cell')
      .siblings('sqtm-core-grid-cell[data-test-cell-id="default"]')
      .find('span')
      .first()
      .click();
  }

  checkIfOptionNameAlreadyExistErrorIsDisplayed() {
    const errorMessage = "Le nom de l'option existe déjà.";
    cy.get(
      'span[data-test-error-key="sqtm-core.validation.errors.optionNameAlreadyExists"]',
    ).should('contain.text', errorMessage);
  }

  checkIfOptionCodeAlreadyExistErrorIsDisplayed() {
    const errorMessage = "Le code de l'option existe déjà.";
    cy.get(
      'span[data-test-error-key="sqtm-core.validation.errors.optionCodeAlreadyExists"]',
    ).should('contain.text', errorMessage);
  }

  checkIfOptionDefaultValueRequiredErrorIsDisplayed() {
    const errorMessage =
      ' Une option par défaut doit être définie lorsque le champ personnalisé est obligatoire.';
    cy.get('span[data-test-error-key="default-option-required"]').should(
      'contain.text',
      errorMessage,
    );
  }

  checkIfInvalidCodePatternErrorIsDisplayed() {
    const errorMessage = 'Ne doit contenir que des lettres, nombres ou underscores.';
    cy.get('span[data-test-error-key="sqtm-core.validation.errors.invalidCodePattern"]').should(
      'contain.text',
      errorMessage,
    );
  }

  protected override addForSuccess(
    addAnother: boolean,
    createResponse?: any,
    gridResponse?: GridResponse,
    _createResponseStatus?: HttpResponseStatus,
  ): Cypress.Chainable<any> {
    return super.addForSuccess(addAnother, createResponse, gridResponse, 201);
  }
}
