import { TextFieldElement } from '../../../elements/forms/TextFieldElement';
import { RichTextFieldElement } from '../../../elements/forms/RichTextFieldElement';
import { CreateAdministrationEntityDialog } from '../create-administration-entity-dialog';
import { SelectFieldElement } from '../../../elements/forms/select-field.element';
import { selectByDataTestElementId } from '../../../../utils/basic-selectors';

export class CreateProjectDialog extends CreateAdministrationEntityDialog {
  private readonly nameField: TextFieldElement;
  private readonly labelField: TextFieldElement;
  private readonly descriptionField: RichTextFieldElement;
  private readonly templateField: SelectFieldElement;

  constructor() {
    super('new-entity', 'projects/new', 'generic-projects');

    this.nameField = new TextFieldElement('name');
    this.labelField = new TextFieldElement('label');
    this.descriptionField = new RichTextFieldElement('description');
    this.templateField = new SelectFieldElement(() => this.findByFieldName('template'));
  }

  fillName(name: string) {
    this.nameField.fill(name);
  }

  fillLabel(label: string) {
    this.labelField.fill(label);
  }

  fillDescription(description: string) {
    this.descriptionField.fill(description);
  }

  checkIfFormIsEmpty() {
    this.nameField.checkContent('');
    this.labelField.checkContent('');
    this.descriptionField.checkContent('');
  }

  selectTemplate(templateName: string) {
    this.templateField.selectValue(templateName);
  }

  getKeepTemplatePluginConfigurationBindingCheckbox() {
    return cy.get(selectByDataTestElementId(`keepPluginsBinding`));
  }

  getCopyTemplatePluginConfigurationCheckbox() {
    return cy.get(selectByDataTestElementId(`copyPluginsConfiguration`));
  }

  assertCopyTemplatePluginConfigurationCheckboxIsEnabled(): void {
    cy.get(selectByDataTestElementId(`copyPluginsConfiguration`))
      .find('input')
      .should('not.have.attr', 'disabled');
  }

  assertCopyTemplatePluginConfigurationCheckboxIsDisabled(): void {
    cy.get(selectByDataTestElementId(`copyPluginsConfiguration`))
      .find('input')
      .should('have.attr', 'disabled');
  }

  assertCopyTemplatePluginConfigurationCheckboxIsChecked(): void {
    cy.get(selectByDataTestElementId(`copyPluginsConfiguration`))
      .find('input')
      .should('be.checked');
  }

  assertCopyAiServerEntryExists() {
    cy.get(selectByDataTestElementId('copyAiServerBinding'))
      .should('exist')
      .find('input')
      .should('be.checked');
  }

  assertCopyAiServerEntryNotExist() {
    cy.get(selectByDataTestElementId('copyAiServerBinding')).should('not.exist');
  }
}
