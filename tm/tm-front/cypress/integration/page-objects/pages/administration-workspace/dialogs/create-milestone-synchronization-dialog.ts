import { CreateAdministrationEntityDialog } from '../create-administration-entity-dialog';

export class CreateMilestoneSynchronizationDialog extends CreateAdministrationEntityDialog {
  constructor() {
    super('synchronize-milestones-dialog', 'milestones/new', 'milestones');
  }
}
