import { AlertDialogElement } from '../../../elements/dialog/alert-dialog.element';

export class NoneScmPluginInstalledAlert extends AlertDialogElement {
  constructor() {
    super();
  }

  assertMessage() {
    this.assertHasMessage(
      'Il est impossible de créer un nouveau serveur car aucun connecteur de partage de code' +
        " source n'a été détecté.",
    );
  }
}
