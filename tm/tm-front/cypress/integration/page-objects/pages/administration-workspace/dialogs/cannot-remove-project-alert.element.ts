import { AlertDialogElement } from '../../../elements/dialog/alert-dialog.element';

export class CannotRemoveProjectAlert extends AlertDialogElement {
  constructor() {
    super();
  }
}
