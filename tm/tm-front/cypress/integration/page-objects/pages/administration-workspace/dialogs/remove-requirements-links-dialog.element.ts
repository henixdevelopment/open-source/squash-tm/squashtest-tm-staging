import { RemoveAdministrationEntityDialog } from '../remove-administration-entity-dialog';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { RequirementVersionLinkType } from '../../../../../../projects/sqtm-core/src/lib/model/requirement/requirement-version-link-type.model';

export class RemoveRequirementsLinksDialogElement extends RemoveAdministrationEntityDialog {
  constructor(requirementsLinksIds: number[]) {
    super('requirements-links', 'requirements-links', requirementsLinksIds);
  }

  deleteSuccessfully(reqLinkIds: number[], requirementsLinks: RequirementVersionLinkType[]) {
    const removeMock = new HttpMockBuilder(`requirements-links/${reqLinkIds.toString()}`)
      .delete()
      .build();
    new HttpMockBuilder('requirements-links')
      .responseBody({ requirementLinks: requirementsLinks })
      .post()
      .build();
    this.clickOnConfirmButton();

    removeMock.wait();
  }
}
