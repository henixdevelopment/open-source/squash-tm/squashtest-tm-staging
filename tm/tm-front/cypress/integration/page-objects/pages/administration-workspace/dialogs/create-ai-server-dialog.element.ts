import { CreateAdministrationEntityDialog } from '../create-administration-entity-dialog';
import { TextFieldElement } from '../../../elements/forms/TextFieldElement';
import { RichTextFieldElement } from '../../../elements/forms/RichTextFieldElement';

export class CreateAiServerDialogElement extends CreateAdministrationEntityDialog {
  private readonly nameField: TextFieldElement;
  private readonly urlField: TextFieldElement;
  private readonly descriptionField: RichTextFieldElement;

  constructor() {
    super('new-ai-server', 'ai-servers/new', 'ai-servers');

    this.nameField = new TextFieldElement('name');
    this.urlField = new TextFieldElement('baseUrl');
    this.descriptionField = new RichTextFieldElement('description');
  }

  fillName(name: string) {
    this.nameField.fill(name);
  }

  fillUrl(baseUrl: string) {
    this.urlField.fill(baseUrl);
  }

  fillDescription(description: string) {
    this.descriptionField.fill(description);
  }

  checkIfFormIsEmpty() {
    this.nameField.checkContent('');
    this.urlField.checkContent('');
    this.descriptionField.checkContent('');
  }
}
