import { RemoveAdministrationEntityDialog } from '../remove-administration-entity-dialog';

export class RemoveAiServerDialogElement extends RemoveAdministrationEntityDialog {
  constructor(public readonly serverIds: number[]) {
    super('ai-servers', 'ai-servers', serverIds);
  }

  assertMessageSingular(name: string) {
    const expectedText =
      'Le serveur d\'intelligence artificielle "' +
      name +
      '" sera supprimé, cette action ne peut être annulée.' +
      " Confirmez-vous la suppression du serveur d'intelligence artificielle ?";

    this.assertHasMessage(expectedText);
  }
}
