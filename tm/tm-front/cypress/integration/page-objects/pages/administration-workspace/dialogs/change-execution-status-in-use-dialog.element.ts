import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { SelectFieldElement } from '../../../elements/forms/select-field.element';
import { BaseDialogElement } from '../../../elements/dialog/base-dialog.element';

export class ChangeExecutionStatusInUseDialog extends BaseDialogElement {
  private readonly statusSelectionField: SelectFieldElement;

  constructor() {
    super('change-execution-status-in-use-dialog');
    this.statusSelectionField = new SelectFieldElement(() => this.findByFieldName('status'));
  }

  fillStatusSelectionField(status: string) {
    this.statusSelectionField.selectValue(status);
  }

  editStatusInUse(oldStatus: 'UNTESTABLE' | 'SETTLED', newStatus: string) {
    this.fillStatusSelectionField(newStatus);
    new HttpMockBuilder<any>(
      'generic-projects/4/disable-and-replace-execution-status-within-project/' + oldStatus,
    )
      .post()
      .build();

    this.confirm();
  }
}
