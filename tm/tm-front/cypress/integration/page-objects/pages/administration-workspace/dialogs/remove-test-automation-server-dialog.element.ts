import { RemoveAdministrationEntityDialog } from '../remove-administration-entity-dialog';

export class RemoveTestAutomationServerDialogElement extends RemoveAdministrationEntityDialog {
  constructor(serverIds: number[]) {
    super('test-automation-servers', 'test-automation-servers', serverIds);
  }
}
