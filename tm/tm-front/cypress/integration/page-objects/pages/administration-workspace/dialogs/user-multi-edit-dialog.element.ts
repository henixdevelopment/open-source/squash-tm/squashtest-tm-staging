import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { OptionalSelectField } from '../../../elements/forms/optional-select-field.element';
import { DataRowModel } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';

export class UserMultiEditDialogElement {
  public readonly dialogId = 'user-multi-edit-dialog';

  public readonly stateSelect: OptionalSelectField;
  public readonly canDeleteSelect: OptionalSelectField;

  constructor() {
    this.stateSelect = new OptionalSelectField('user-state');
    this.canDeleteSelect = new OptionalSelectField('can-delete');
  }

  assertNotExist() {
    cy.get(this.buildSelector()).should('not.exist');
  }

  assertExists() {
    cy.get(this.buildSelector()).should('exist');
  }

  confirm(gridRefreshResponse: DataRowModel[]): void {
    const updateMock = new HttpMockBuilder('users/*/mass-update').post().build();

    const refreshTreeMock = new HttpMockBuilder('users')
      .post()
      .responseBody({ dataRows: gridRefreshResponse })
      .build();

    this.clickOnConfirmButton();

    cy.get(this.buildSelector()).should('not.exist');

    updateMock.wait();
    refreshTreeMock.wait();
  }

  public clickOnConfirmButton() {
    const buttonSelector = this.buildButtonSelector('confirm');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  public clickOnCancelButton() {
    const buttonSelector = this.buildButtonSelector('cancel');
    cy.get(buttonSelector).should('exist').click();
  }

  private buildSelector(): string {
    return `[data-test-dialog-id=${this.dialogId}]`;
  }

  private buildButtonSelector(buttonId: string) {
    return `${this.buildSelector()} [data-test-dialog-button-id=${buttonId}]`;
  }
}
