import { TextFieldElement } from '../../../elements/forms/TextFieldElement';
import { CreateAdministrationEntityDialog } from '../create-administration-entity-dialog';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { RequirementVersionLinkType } from '../../../../../../projects/sqtm-core/src/lib/model/requirement/requirement-version-link-type.model';

export class CreateRequirementsLinkDialog extends CreateAdministrationEntityDialog {
  private readonly role1Field: TextFieldElement;
  private readonly role1CodeField: TextFieldElement;
  private readonly role2Field: TextFieldElement;
  private readonly role2CodeField: TextFieldElement;

  constructor() {
    super('new-requirements-link', 'requirements-links/new', 'requirements-links');

    this.role1Field = new TextFieldElement('role1');
    this.role1CodeField = new TextFieldElement('role1Code');
    this.role2Field = new TextFieldElement('role2');
    this.role2CodeField = new TextFieldElement('role2Code');
  }

  fillRole1(role1: string) {
    this.role1Field.fill(role1);
  }

  fillRole1Code(role1Code: string) {
    this.role1CodeField.fill(role1Code);
  }

  fillRole2(role2: string) {
    this.role2Field.fill(role2);
  }

  fillRole2Code(role2Code: string) {
    this.role2CodeField.fill(role2Code);
  }

  addRequirementLink(
    addedRequirementLinkId: number,
    requirementsLinks: RequirementVersionLinkType[],
    addAnother: boolean,
  ) {
    const mockAdd = new HttpMockBuilder('requirements-links/new')
      .responseBody({ id: addedRequirementLinkId })
      .post()
      .build();
    const mockAddedResponse = new HttpMockBuilder('requirements-links')
      .responseBody({ requirementLinks: requirementsLinks })
      .post()
      .build();
    if (addAnother) {
      this.clickOnAddAnotherButton();
    } else {
      this.clickOnAddButton();
    }
    return mockAdd.waitResponseBody().then(() => {
      mockAddedResponse.waitResponseBody();
    });
  }

  checkIfFormIsEmpty() {
    this.role1Field.checkContent('');
    this.role1CodeField.checkContent('');
    this.role2Field.checkContent('');
    this.role2CodeField.checkContent('');
  }

  checkIfExistingTypeCodeIsDisplayed() {
    const errorMessage = 'Ce code est déjà utilisé pour un type de lien existant.';
    cy.get(
      'span[data-test-error-key="sqtm-core.exception.requirements-links.link-type-code-already-exists"]',
    ).should('contain.text', errorMessage);
  }
}
