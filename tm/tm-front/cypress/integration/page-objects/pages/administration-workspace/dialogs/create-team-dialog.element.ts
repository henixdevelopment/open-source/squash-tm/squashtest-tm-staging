import { TextFieldElement } from '../../../elements/forms/TextFieldElement';
import { RichTextFieldElement } from '../../../elements/forms/RichTextFieldElement';
import { CreateAdministrationEntityDialog } from '../create-administration-entity-dialog';
import { I18nError } from '../../../elements/forms/error.message';
import { DATA_TEST_INPUT_NAME, selectByDataTestFieldId } from '../../../../utils/basic-selectors';

export class CreateTeamDialog extends CreateAdministrationEntityDialog {
  private readonly nameField: TextFieldElement;
  private readonly descriptionField: RichTextFieldElement;

  constructor() {
    super('new-team', 'teams/new', 'teams');

    this.nameField = new TextFieldElement('name');
    this.descriptionField = new RichTextFieldElement('description');
  }

  fillName(name: string) {
    this.nameField.fill(name);
  }

  checkNameHasContent(name: string) {
    this.nameField.checkContent(name);
  }

  assertNameHasError(error: I18nError) {
    this.nameField.assertErrorContains(error);
  }

  fillDescription(description: string) {
    this.descriptionField.fill(description);
  }

  checkDescriptionHasContent(name: string) {
    this.descriptionField.checkContent(name);
  }

  fillNameAndCheckContent(name: string) {
    this.fillName(name);
    this.checkNameHasContent(name);
  }

  fillDescriptionAndCheckContent(description: string) {
    this.fillDescription(description);
    this.checkDescriptionHasContent(description);
  }

  fillNameAndDescriptionAndCheckContent(name: string, description: string) {
    this.fillNameAndCheckContent(name);
    this.fillDescriptionAndCheckContent(description);
  }

  checkIfFormIsEmpty() {
    this.nameField.checkContent('');
    this.descriptionField.checkContent('');
  }

  assertNameLabelExists() {
    cy.get(selectByDataTestFieldId('nameLabel')).should('contain.text', 'Nom');
  }

  assertDescriptionLabelExists() {
    cy.get(selectByDataTestFieldId('descriptionLabel')).should('contain.text', 'Description');
  }

  checkConformity() {
    this.assertTitleHasText('Ajouter une équipe');
    this.assertNameLabelExists();
    this.assertDescriptionLabelExists();
    this.checkIfFormIsEmpty();
    this.checkAllButtonsExist();
    this.checkAllButtonLabelsFeminine();
  }

  assertNameFieldIsFocused() {
    cy.focused().should('have.attr', DATA_TEST_INPUT_NAME, 'name');
  }
}
