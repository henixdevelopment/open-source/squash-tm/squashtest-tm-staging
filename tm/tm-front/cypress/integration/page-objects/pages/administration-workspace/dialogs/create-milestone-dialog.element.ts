import { TextFieldElement } from '../../../elements/forms/TextFieldElement';
import { RichTextFieldElement } from '../../../elements/forms/RichTextFieldElement';
import { CreateAdministrationEntityDialog } from '../create-administration-entity-dialog';
import { EditableDateFieldElement } from '../../../elements/forms/editable-date-field.element';
import { SelectFieldElement } from '../../../elements/forms/select-field.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class CreateMilestoneDialog extends CreateAdministrationEntityDialog {
  private readonly nameField: TextFieldElement;
  private readonly descriptionField: RichTextFieldElement;
  private readonly statusField: SelectFieldElement;
  private readonly deadlineDate: EditableDateFieldElement;

  constructor() {
    super('new-entity', 'milestones/new', 'milestones');

    this.nameField = new TextFieldElement('label');
    this.deadlineDate = new EditableDateFieldElement(() => this.findByFieldName('endDate'));
    this.statusField = new SelectFieldElement(() => this.findByFieldName('status'));
    this.descriptionField = new RichTextFieldElement('description');
  }

  fillName(name: string) {
    this.nameField.fill(name);
  }

  fillDescription(description: string) {
    this.descriptionField.fillWithString(description);
  }

  setStatus(status: string) {
    this.statusField.selectValue(status);
  }

  setDeadlineDateToToday() {
    this.deadlineDate.setToTodayAndConfirm();
  }

  checkIfFormIsEmpty() {
    this.nameField.checkContent('');
    this.descriptionField.checkContent('');
  }

  addMilestone(addedMilestoneId: number, gridResponse: GridResponse, addAnother: boolean) {
    const mockAdd = new HttpMockBuilder('milestones/new')
      .responseBody({ id: addedMilestoneId })
      .post()
      .build();
    const mockAddedResponse = new HttpMockBuilder('milestones')
      .responseBody(gridResponse)
      .post()
      .build();

    if (addAnother) {
      this.clickOnAddAnotherButton();
    } else {
      this.clickOnAddButton();
    }

    return mockAdd.waitResponseBody().then(() => {
      mockAddedResponse.waitResponseBody();
    });
  }
}
