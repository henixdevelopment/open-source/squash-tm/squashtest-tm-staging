import { TextFieldElement } from '../../../elements/forms/TextFieldElement';
import { RichTextFieldElement } from '../../../elements/forms/RichTextFieldElement';
import { CreateAdministrationEntityDialog } from '../create-administration-entity-dialog';
import { HttpResponseStatus } from '../../../../utils/mocks/request-mock';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class CreateTemplateFromProjectDialog extends CreateAdministrationEntityDialog {
  private readonly nameField: TextFieldElement;
  private readonly labelField: TextFieldElement;
  private readonly descriptionField: RichTextFieldElement;

  constructor() {
    super('new-entity', 'project-templates/new', 'generic-projects');

    this.nameField = new TextFieldElement('name');
    this.labelField = new TextFieldElement('label');
    this.descriptionField = new RichTextFieldElement('description');
  }

  fillName(name: string) {
    this.nameField.fill(name);
  }

  fillLabel(label: string) {
    this.labelField.fill(label);
  }

  fillDescription(description: string) {
    this.descriptionField.fill(description);
  }

  protected addForSuccess(
    addAnother: boolean,
    createResponse?: any,
    gridResponse?: GridResponse,
    createResponseStatus?: HttpResponseStatus,
  ): Cypress.Chainable<any> {
    return super.addForSuccess(addAnother, createResponse, gridResponse, createResponseStatus);
  }
}
