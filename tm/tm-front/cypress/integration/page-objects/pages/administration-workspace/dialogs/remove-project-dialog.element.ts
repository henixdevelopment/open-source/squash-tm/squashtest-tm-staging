import { RemoveAdministrationEntityDialog } from '../remove-administration-entity-dialog';

export class RemoveProjectDialog extends RemoveAdministrationEntityDialog {
  constructor(public readonly projectId?: number) {
    super('projects', 'generic-projects', projectId ? [projectId] : null);
  }
}
