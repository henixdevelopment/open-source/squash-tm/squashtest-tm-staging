import { CreateAdministrationEntityDialog } from '../create-administration-entity-dialog';
import { TextFieldElement } from '../../../elements/forms/TextFieldElement';
import { SelectFieldElement } from '../../../elements/forms/select-field.element';
import { GridElement } from '../../../elements/grid/grid.element';
import { OPTION_NAME_ALREADY_EXISTS_ERROR } from '../../../elements/forms/error.message';

export class CreateEnvironmentVariableDialog extends CreateAdministrationEntityDialog {
  public readonly nameField: TextFieldElement;
  public readonly inputTypeField: SelectFieldElement;

  public readonly dropdownListOptionNameField: TextFieldElement;
  public readonly dropdownListOptionsGrid: GridElement;

  constructor() {
    super('new-entity', 'environment-variables/new', 'environment-variables');
    this.nameField = new TextFieldElement('name');
    this.inputTypeField = new SelectFieldElement(() => this.findByFieldName('evInputType'));
    this.dropdownListOptionNameField = new TextFieldElement('dropdownListOptionLabel');
    this.dropdownListOptionsGrid = GridElement.createGridElement('environment-variable-options');
  }

  fillName(name: string) {
    this.nameField.fill(name);
  }

  selectTypeField(inputTypeField: string) {
    this.inputTypeField.selectValue(inputTypeField);
  }

  fillDropdownListOptionName(dropdownListOptionName: string) {
    this.dropdownListOptionNameField.fill(dropdownListOptionName);
  }

  addDropdownListOption(dropdownListOptionName: string) {
    this.fillDropdownListOptionName(dropdownListOptionName);
    cy.get('button[data-test-dialog-button-id="add-option"').click();
  }

  checkIfOptionNameAlreadyExistErrorIsDisplayed() {
    this.dropdownListOptionNameField.assertErrorContains(OPTION_NAME_ALREADY_EXISTS_ERROR);
  }
}
