import { TextFieldElement } from '../../../elements/forms/TextFieldElement';
import { SelectFieldElement } from '../../../elements/forms/select-field.element';
import { CreateAdministrationEntityDialog } from '../create-administration-entity-dialog';
import { I18nError } from '../../../elements/forms/error.message';

export class CreateUserDialog extends CreateAdministrationEntityDialog {
  public readonly loginField: TextFieldElement;
  public readonly firstNameField: TextFieldElement;
  public readonly lastNameField: TextFieldElement;
  public readonly emailField: TextFieldElement;
  public readonly groupField: SelectFieldElement;
  public readonly passwordField: TextFieldElement;
  public readonly passwordConfirmationField: TextFieldElement;

  constructor() {
    super('new-user', 'users/new', 'users');
    this.loginField = new TextFieldElement('login');
    this.firstNameField = new TextFieldElement('firstName');
    this.lastNameField = new TextFieldElement('lastName');
    this.emailField = new TextFieldElement('email');
    this.groupField = new SelectFieldElement(() => this.findByFieldName('groupId'));
    this.passwordField = new TextFieldElement('password');
    this.passwordConfirmationField = new TextFieldElement('confirmPassword');
  }

  fillLogin(login: string) {
    this.loginField.fill(login);
  }

  fillFirstName(firstName: string) {
    this.firstNameField.fill(firstName);
  }

  fillName(lastName: string) {
    this.lastNameField.fill(lastName);
  }

  fillEmail(email: string) {
    this.emailField.fill(email);
  }

  selectGroup(group: 'Utilisateur') {
    this.groupField.selectValue(group);
  }

  fillPassword(password: string) {
    this.passwordField.fill(password);
  }

  fillConfirmPassword(password: string) {
    this.passwordConfirmationField.fill(password);
  }

  assertLoginFieldHasError(error: I18nError) {
    this.loginField.assertErrorContains(error);
  }

  assertLastNameFieldHasError(error: I18nError) {
    this.lastNameField.assertErrorContains(error);
  }

  assertPasswordFieldHasError(error: I18nError) {
    this.passwordField.assertErrorContains(error);
  }

  assertPasswordConfirmationFieldHasError(error: I18nError) {
    this.passwordConfirmationField.assertErrorContains(error);
  }

  createUser(
    login: string,
    firstName: string,
    lastName: string,
    email: string,
    group: 'Utilisateur',
    password: string,
    confirmPassword: string,
  ) {
    this.fillUser(login, firstName, lastName, email, group, password, confirmPassword);
    this.addForSuccess(false);
  }

  fillUser(
    login: string,
    firstName: string,
    lastName: string,
    email: string,
    group: 'Utilisateur',
    password: string,
    confirmPassword: string,
  ) {
    this.fillLogin(login);
    this.fillFirstName(firstName);
    this.fillName(lastName);
    this.fillEmail(email);
    this.selectGroup(group);
    this.fillPassword(password);
    this.fillConfirmPassword(confirmPassword);
  }

  checkFormIsEmpty() {
    this.loginField.checkContent('');
    this.firstNameField.checkContent('');
    this.lastNameField.checkContent('');
    this.emailField.checkContent('');
    this.groupField.setValue('Utilisateur');
    this.passwordField.checkContent('');
    this.passwordConfirmationField.checkContent('');
  }

  checkIfNotMatchingErrorMessageIsDisplayed(): void {
    const errorMessage = 'Les mots de passe saisis ne correspondent pas.';
    cy.get('span[data-test-error-key="sqtm-core.validation.errors.passwordsNotMatching"]').should(
      'contain.text',
      errorMessage,
    );
  }

  checkIfInsufficientCharsInPasswordErrorMessageIsDisplayed(): void {
    const errorMessage =
      'Le nombre de caractères pour le mot de passe doit être compris entre 6 et 255.';
    cy.get('span[data-test-error-key="sqtm-core.validation.errors.passwordLength"]').should(
      'contain.text',
      errorMessage,
    );
  }

  assertHasLicenseWarning(): void {
    this.findByElementId('license-user-warning').should('be.visible');
  }
}
