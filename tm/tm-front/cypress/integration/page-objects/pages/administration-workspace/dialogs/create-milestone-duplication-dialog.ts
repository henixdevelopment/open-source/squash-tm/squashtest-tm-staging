import { CreateAdministrationEntityDialog } from '../create-administration-entity-dialog';
import { TextFieldElement } from '../../../elements/forms/TextFieldElement';
import { RichTextFieldElement } from '../../../elements/forms/RichTextFieldElement';
import { SelectFieldElement } from '../../../elements/forms/select-field.element';
import { EditableDateFieldElement } from '../../../elements/forms/editable-date-field.element';

export class CreateMilestoneDuplicationDialog extends CreateAdministrationEntityDialog {
  private readonly nameField: TextFieldElement;
  private readonly descriptionField: RichTextFieldElement;
  private readonly statusField: SelectFieldElement;
  private readonly deadlineDate: EditableDateFieldElement;

  constructor() {
    super('duplicate-milestone-dialog', 'milestones/new', 'milestones');
    this.nameField = new TextFieldElement('label');
    this.deadlineDate = new EditableDateFieldElement(() => this.findByFieldName('endDate'));
    this.statusField = new SelectFieldElement(() => this.findByFieldName('status'));
    this.descriptionField = new RichTextFieldElement('description');
  }

  fillName(name: string) {
    this.nameField.fill(name);
  }

  fillDescription(description: string) {
    this.descriptionField.fillWithString(description);
  }

  setDeadlineDateToToday() {
    this.deadlineDate.setToTodayAndConfirm();
  }
}
