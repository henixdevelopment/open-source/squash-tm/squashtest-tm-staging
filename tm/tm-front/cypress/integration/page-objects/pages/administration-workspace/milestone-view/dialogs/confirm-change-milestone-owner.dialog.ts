import { BaseDialogElement } from '../../../../elements/dialog/base-dialog.element';

export class ConfirmChangeMilestoneOwnerDialog extends BaseDialogElement {
  constructor() {
    super('change-milestone-owner-warning');
  }

  assertMessage() {
    const message =
      'Vous ne serez plus propriétaire du jalon et ne pourrez plus le modifier, souhaitez-vous continuer ?';

    this.assertHasMessage(message);
  }
}
