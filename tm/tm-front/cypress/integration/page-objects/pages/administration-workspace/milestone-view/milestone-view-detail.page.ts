import { MilestoneViewPage } from './milestone-view.page';
import { MilestonePossibleOwner } from '../../../../../../projects/sqtm-core/src/lib/model/user/user.model';

export class MilestoneViewDetailPage extends MilestoneViewPage {
  constructor(possibleOwners?: MilestonePossibleOwner[]) {
    super(possibleOwners);
  }

  assertExists() {
    cy.get('sqtm-app-milestone-view-detail').should('have.length', 1);
    super.assertExists();
  }

  clickBackButton(): void {
    cy.get(this.rootSelector).find('[data-test-button-id="back"]').click();
  }
}
