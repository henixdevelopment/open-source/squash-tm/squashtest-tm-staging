import { GridElement } from '../../../../elements/grid/grid.element';
import { HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import {
  BindableProject,
  BindProjectsToMilestoneDialog,
} from '../dialogs/bind-projects-to-milestone.dialog';
import { selectByDataTestToolbarButtonId } from '../../../../../utils/basic-selectors';
import { ProjectView } from '../../../../../../../projects/sqtm-core/src/lib/model/project/project.model';
import { GridColumnId } from '../../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';

export class MilestoneProjectsPanelElement {
  public readonly grid: GridElement;

  constructor() {
    this.grid = GridElement.createGridElement('milestone-projects');
  }

  waitInitialDataFetch() {}

  unbindOne(projectName: string) {
    this.grid.findRowId(GridColumnId.projectName, projectName).then((id) => {
      this.grid.getRow(id).cell(GridColumnId.delete).iconRenderer().click();

      const deleteMock = new HttpMockBuilder('milestone-binding/*/unbind-projects/*')
        .delete()
        .build();

      this.clickConfirmDeleteButton();

      deleteMock.wait();
    });
  }

  unbindMultiple(projectNames: string[]) {
    this.grid.selectRowsWithMatchingCellContent(GridColumnId.projectName, projectNames);

    const deleteMock = new HttpMockBuilder('milestone-binding/*/unbind-projects/*')
      .delete()
      .build();

    this.clickOnUnbindProjectsButton();
    this.clickConfirmDeleteButton();

    deleteMock.wait();
  }

  clickOnBindProjectsButton(bindableProjects?: BindableProject[]): BindProjectsToMilestoneDialog {
    const dialog = new BindProjectsToMilestoneDialog(this.grid, bindableProjects);

    cy.get('[data-test-button-id="bind-projects"]').should('exist').click();

    dialog.waitInitialDataFetch();
    dialog.assertExists();

    return dialog;
  }

  private clickOnUnbindProjectsButton() {
    cy.get(selectByDataTestToolbarButtonId('unbind-projects')).should('exist').click();
  }

  private clickConfirmDeleteButton() {
    cy.get('sqtm-core-confirm-delete-dialog')
      .find('[data-test-dialog-button-id="confirm"]')
      .click()
      // Then
      .get('sqtm-core-confirm-delete-dialog')
      .should('not.exist');
  }

  showProjectDetail(projectName: string, response: ProjectView): void {
    const mock = new HttpMockBuilder('project-view/*').responseBody(response).build();
    const statusesInUseMock = new HttpMockBuilder('project-view/*/statuses-in-use')
      .responseBody(response.statusesInUse)
      .build();

    this.grid.findRowId(GridColumnId.projectName, projectName).then((id) => {
      this.grid.getRow(id).cell(GridColumnId.projectName).linkRenderer().findCellLink().click();
    });

    mock.wait();
    statusesInUseMock.wait();
  }
  assertUnbindButtonNotExist(projectName: string) {
    this.grid.findRowId(GridColumnId.projectName, projectName).then((id) => {
      this.grid.getRow(id).cell(GridColumnId.delete).iconRenderer().assertNotExist();
    });
  }
}
