import { Page } from '../../page';
import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';
import { MilestoneInformationPanelElement } from './panels/milestone-information-panel.element';
import { HttpMock, HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { MilestoneProjectsPanelElement } from './panels/milestone-projects-panel.element';
import { MilestonePossibleOwner } from '../../../../../../projects/sqtm-core/src/lib/model/user/user.model';
import { selectByDataIcon } from '../../../../utils/basic-selectors';

export class MilestoneViewPage extends Page {
  public readonly informationPanel = new MilestoneInformationPanelElement();
  public readonly projectsPanel = new MilestoneProjectsPanelElement();
  public readonly entityNameField: EditableTextFieldElement;

  private possibleOwnersMock: HttpMock<MilestonePossibleOwner[]>;

  constructor(possibleOwners: MilestonePossibleOwner[] = []) {
    super('sqtm-app-milestone-view');

    this.possibleOwnersMock = new HttpMockBuilder<MilestonePossibleOwner[]>(
      'milestones/possible-owners',
    )
      .responseBody(possibleOwners)
      .build();
    this.entityNameField = new EditableTextFieldElement('entity-name', 'milestones/*/label');
  }

  waitInitialDataFetch() {
    this.possibleOwnersMock.wait();
  }

  foldGrid() {
    cy.get(`[data-test-element-id="fold-tree-button"]`).click();
  }
  assertAddButtonProjectNotExist() {
    this.find(selectByDataIcon('sqtm-core-generic:add')).should('not.exist');
  }
  assertUnlinkButtonProjectNotExist() {
    this.find(selectByDataIcon('sqtm-core-generic:unlink')).should('not.exist');
  }
  closeMilestoneView() {
    this.find(selectByDataIcon('close')).click();
  }
}
