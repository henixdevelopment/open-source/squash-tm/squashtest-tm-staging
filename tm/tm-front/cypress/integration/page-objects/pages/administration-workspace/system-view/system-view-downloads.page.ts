import { SystemViewInformationPage } from './system-view-information.page';
import Chainable = Cypress.Chainable;
import { SystemViewModel } from '../../../../../../projects/sqtm-core/src/lib/model/system/system-view.model';
import { AdminReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/admin-referential-data.model';
import { SystemViewWorkspacePage } from './system-workspace.page';

export class SystemViewDownloadsPage extends SystemViewWorkspacePage {
  constructor() {
    super('sqtm-app-system-view-downloads > div');
  }

  static navigateToPage(
    viewData: SystemViewModel,
    referentialData?: AdminReferentialDataModel,
  ): SystemViewDownloadsPage {
    const page = SystemViewInformationPage.navigateToPage(viewData, referentialData);
    page.clickDownloadsAnchor();
    return new SystemViewDownloadsPage();
  }

  assertLatestLogLinkExists() {
    this.assertLinkExists('latest-log');
  }

  assertLinkExists(linkId: string) {
    this.getLink(linkId).should('exist');
  }

  assertLinkDoesNotExist(linkId: string): void {
    this.getLink(linkId).should('not.exist');
  }

  private getLink(linkId: string): Chainable {
    return cy.get(`${this.rootSelector} a[data-test-link-id="${linkId}"]`);
  }

  assertPreviousLinksAreNotVisible(): void {
    cy.get(`${this.rootSelector} [data-test-element-id="previous-log-files"]`).should('not.exist');
  }
}
