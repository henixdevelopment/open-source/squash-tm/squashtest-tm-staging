import { Page } from '../../page';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';

export abstract class SystemViewWorkspacePage extends Page {
  readonly anchors = AnchorsElement.withLinkIds(
    'information',
    'messages',
    'downloads',
    'settings',
    'report-templates',
    'cleaning',
    'synchronizations-supervision',
  );

  clickMessagesAnchor(): void {
    this.anchors.clickLink('messages');
  }

  clickDownloadsAnchor(): void {
    this.anchors.clickLink('downloads');
  }

  clickSettingsAnchor(): void {
    this.anchors.clickLink('settings');
  }

  clickSynchronizationSupervisionAnchor(): void {
    this.anchors.clickLink('synchronizations-supervision');
  }
}
