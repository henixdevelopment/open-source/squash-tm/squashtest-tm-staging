import { SystemViewModel } from '../../../../../../projects/sqtm-core/src/lib/model/system/system-view.model';
import { AdminReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/admin-referential-data.model';
import { SystemViewWorkspacePage } from './system-workspace.page';
import { SystemViewInformationPage } from './system-view-information.page';
import { GridElement } from '../../../elements/grid/grid.element';
import { GridId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-id';
import { selectByDataTestToolbarButtonId } from '../../../../utils/basic-selectors';

export class SystemViewSynchronizationSupervisionPage extends SystemViewWorkspacePage {
  public readonly xsquash4jiraGrid: GridElement;
  public readonly xsquash4gitlabGrid: GridElement;
  public readonly automJiraGrid: GridElement;

  constructor() {
    super('sqtm-app-synchronizations-supervision > div');

    this.xsquash4jiraGrid = GridElement.createGridElement(
      GridId.SYSTEM_XSQUASH4JIRA_SYNCHRONISATIONS,
    );
    this.xsquash4gitlabGrid = GridElement.createGridElement(
      GridId.SYSTEM_XSQUASH4GITLAB_SYNCHRONISATIONS,
    );
    this.automJiraGrid = GridElement.createGridElement(GridId.SYSTEM_AUTOM_JIRA_SYNCHRONISATIONS);
  }

  static navigateToPage(
    viewData?: SystemViewModel,
    referentialData?: AdminReferentialDataModel,
  ): SystemViewSynchronizationSupervisionPage {
    const page = SystemViewInformationPage.navigateToPage(viewData, referentialData);
    page.clickSynchronizationSupervisionAnchor();
    return new SystemViewSynchronizationSupervisionPage();
  }

  assertNoPluginInstalledMessageExists() {
    cy.get('[data-test-element-id=no-plugin-installed-message]').should('exist');
    cy.get('[data-test-element-id=no-plugin-installed-message]').should(
      'contain',
      "Aucun plugin de synchronisation n'est installé sur l'instance.",
    );
  }
  clickRefreshButton() {
    this.find(selectByDataTestToolbarButtonId('refresh-button')).find('svg').click();
  }
  clickEnableAutomaticRefreshButton() {
    this.find(selectByDataTestToolbarButtonId('automatic-refresh-button')).find('button').click();
  }
}
