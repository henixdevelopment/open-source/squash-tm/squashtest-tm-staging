import { EditableRichTextFieldElement } from '../../../elements/forms/editable-rich-text-field.element';
import { SystemViewInformationPage } from './system-view-information.page';
import { SystemViewModel } from '../../../../../../projects/sqtm-core/src/lib/model/system/system-view.model';
import { AdminReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/admin-referential-data.model';
import { SystemViewWorkspacePage } from './system-workspace.page';

export class SystemViewMessagesPage extends SystemViewWorkspacePage {
  public readonly loginMessageField: EditableRichTextFieldElement;
  public readonly welcomeMessageField: EditableRichTextFieldElement;
  public readonly bannerMessageField: EditableRichTextFieldElement;

  constructor() {
    super('sqtm-app-system-view-messages > div');

    const changeUrlBase = 'system/messages/';

    this.loginMessageField = new EditableRichTextFieldElement(
      'loginMessage',
      changeUrlBase + 'login-message',
    );
    this.welcomeMessageField = new EditableRichTextFieldElement(
      'welcomeMessage',
      changeUrlBase + 'welcome-message',
    );
    this.bannerMessageField = new EditableRichTextFieldElement(
      'bannerMessage',
      changeUrlBase + 'banner-message',
    );
  }

  static navigateToPage(
    viewData: SystemViewModel,
    referentialData?: AdminReferentialDataModel,
  ): SystemViewMessagesPage {
    const page = SystemViewInformationPage.navigateToPage(viewData, referentialData);
    page.clickMessagesAnchor();
    return new SystemViewMessagesPage();
  }
}
