import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';
import { SwitchFieldElement } from '../../../elements/forms/switch-field.element';
import Chainable = Cypress.Chainable;
import { SystemViewModel } from '../../../../../../projects/sqtm-core/src/lib/model/system/system-view.model';
import { AdminReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/admin-referential-data.model';
import { SystemViewWorkspacePage } from './system-workspace.page';
import { SystemViewInformationPage } from './system-view-information.page';
import { selectByDataIcon } from '../../../../utils/basic-selectors';

export class SystemViewSettingsPage extends SystemViewWorkspacePage {
  public readonly whiteListField: EditableTextFieldElement;
  public readonly uploadSizeLimitField: EditableTextFieldElement;
  public readonly importSizeLimitField: EditableTextFieldElement;
  public readonly callbackUrlField: EditableTextFieldElement;
  public readonly stackTraceFeatureSwitch: SwitchFieldElement;
  public readonly caseInsensitiveLoginSwitch: SwitchFieldElement;
  public readonly caseInsensitiveActionsSwitch: SwitchFieldElement;
  public readonly autoconnectOnConnectionSwitch: SwitchFieldElement;
  public readonly searchActivationSwitch: SwitchFieldElement;

  constructor() {
    super('sqtm-app-system-view-settings > div');

    const urlPrefix = 'system/settings/';
    this.whiteListField = new EditableTextFieldElement('whiteList', urlPrefix + 'white-list');
    this.uploadSizeLimitField = new EditableTextFieldElement(
      'uploadSizeLimit',
      urlPrefix + 'upload-size-limit',
    );
    this.importSizeLimitField = new EditableTextFieldElement(
      'importSizeLimit',
      urlPrefix + 'import-size-limit',
    );
    this.callbackUrlField = new EditableTextFieldElement('callbackUrl', urlPrefix + 'callback-url');

    this.stackTraceFeatureSwitch = new SwitchFieldElement(
      'stackTraceFeature',
      'features/stack-trace?enabled=*',
    );

    this.caseInsensitiveLoginSwitch = new SwitchFieldElement(
      'caseInsensitiveLogin',
      'features/case-insensitive-login?enabled=*',
    );

    this.caseInsensitiveActionsSwitch = new SwitchFieldElement(
      'caseInsensitiveActions',
      'features/case-insensitive-actions?enabled=*',
    );

    this.autoconnectOnConnectionSwitch = new SwitchFieldElement(
      'autoconnectOnConnection',
      'features/autoconnect-on-connection?enabled=*',
    );

    this.searchActivationSwitch = new SwitchFieldElement(
      'searchActivation',
      'features/search-activation',
    );
  }

  static navigateToPage(
    viewData?: SystemViewModel,
    referentialData?: AdminReferentialDataModel,
  ): SystemViewSettingsPage {
    const page = SystemViewInformationPage.navigateToPage(viewData, referentialData);
    page.clickSettingsAnchor();
    return new SystemViewSettingsPage();
  }

  checkWhiteList(expected: string): void {
    this.getField('whiteList').should('contain.text', expected);
  }

  checkUploadSizeLimit(expected: string): void {
    this.getField('uploadSizeLimit').should('contain.text', expected);
  }

  checkImportSizeLimit(expected: string): void {
    this.getField('importSizeLimit').should('contain.text', expected);
  }

  checkCallbackUrl(expected: string): void {
    this.getField('callbackUrl').should('contain.text', expected);
  }

  private getField(fieldName: string): Chainable {
    return cy.get(`${this.rootSelector} [data-test-field-id="${fieldName}"]`);
  }

  assertSpinnerIsNotPresent() {
    this.find(selectByDataIcon('loading')).should('not.exist');
  }
}
