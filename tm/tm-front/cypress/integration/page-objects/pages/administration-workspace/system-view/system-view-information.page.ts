import { AdminReferentialDataProviderBuilder } from '../../../../utils/referential/admin-referential-data.provider';
import { HttpMock, HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import {
  AdministrationStatistics,
  SystemViewModel,
} from '../../../../../../projects/sqtm-core/src/lib/model/system/system-view.model';
import { AdminReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/admin-referential-data.model';
import { SystemViewWorkspacePage } from './system-workspace.page';
import Chainable = Cypress.Chainable;

export class SystemViewInformationPage extends SystemViewWorkspacePage {
  private initialDataMock: HttpMock<SystemViewModel>;

  constructor(viewData?: SystemViewModel) {
    super('sqtm-app-system-view-information > div');
    this.initialDataMock = new HttpMockBuilder<SystemViewModel>('system-view')
      .responseBody(viewData)
      .build();
  }

  static navigateToPage(
    viewData: SystemViewModel,
    referentialData?: AdminReferentialDataModel,
  ): SystemViewInformationPage {
    const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder(
      referentialData,
    ).build();
    const page = new SystemViewInformationPage(viewData);
    cy.visit('administration-workspace/system/information');
    adminReferentialDataProvider.wait();
    return page;
  }

  waitInitialDataFetch() {
    this.initialDataMock.wait();
  }

  checkVersion(expected: string): void {
    this.getField('appVersion').should('contain.text', expected);
  }

  checkStatistics(statistics: AdministrationStatistics): void {
    const fieldsToCheck: (keyof AdministrationStatistics)[] = [
      'testCasesNumber',
      'campaignsNumber',
      'iterationsNumber',
      'executionsNumber',
      'requirementsNumber',
      'usersNumber',
      'projectsNumber',
      'databaseSize',
    ];

    fieldsToCheck.forEach((fieldName) =>
      this.getField(fieldName).should('contain.text', statistics[fieldName]),
    );
  }

  private getField(fieldName: string): Chainable {
    return cy.get(`${this.rootSelector} [data-test-field-id="${fieldName}"]`);
  }

  checkPlugins(plugins: string[]): void {
    plugins.forEach((plugin) =>
      cy.get(`${this.rootSelector} sqtm-app-system-plugins-panel`).should('contain.text', plugin),
    );
  }
}
