import { HttpMock, HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import { GroupedMultiListElement } from '../../../../elements/filters/grouped-multi-list.element';
import { SelectFieldElement } from '../../../../elements/forms/select-field.element';
import { GridElement } from '../../../../elements/grid/grid.element';
import { BaseDialogElement } from '../../../../elements/dialog/base-dialog.element';

export interface ProjectWithoutPermission {
  id: string;
  name: string;
}

export class AddTeamAuthorisationsDialog extends BaseDialogElement {
  public readonly profileSelectField: SelectFieldElement;
  private projectsWithoutPermissionMock: HttpMock<ProjectWithoutPermission[]>;

  constructor(
    private readonly permissionsGrid: GridElement,
    projectsWithoutPermission: ProjectWithoutPermission[],
  ) {
    super('add-team-authorisation-dialog');

    this.projectsWithoutPermissionMock = new HttpMockBuilder<ProjectWithoutPermission[]>(
      'team-view/*/projects-without-permission',
    )
      .responseBody(projectsWithoutPermission)
      .build();

    this.profileSelectField = new SelectFieldElement(() =>
      this.findByFieldName('permission-profile'),
    );
  }

  waitInitialDataFetch() {
    this.projectsWithoutPermissionMock.wait();
  }

  confirm(updatedPermissions?: ProjectWithoutPermission[]) {
    const mock = new HttpMockBuilder('teams/*/projects/*/permissions/*')
      .responseBody({ projectPermissions: updatedPermissions })
      .post()
      .build();

    this.clickOnAddButton();

    mock.wait();
  }

  selectProjects(...projectNames: string[]) {
    cy.get('[data-test-element-id="grouped-multi-list-display-value"]').click();
    const multiList = new GroupedMultiListElement();
    projectNames.forEach((name) => multiList.toggleOneItem(name));
    cy.clickVoid();
    multiList.assertNotExist();
  }

  selectProfile(profile: string) {
    this.profileSelectField.selectValue(profile);
  }
}
