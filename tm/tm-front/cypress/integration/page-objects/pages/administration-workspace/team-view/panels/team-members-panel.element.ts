import { GridElement } from '../../../../elements/grid/grid.element';
import { HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import { AddTeamMemberDialog, Member } from '../dialogs/add-team-member.dialog';
import { selectByDataTestToolbarButtonId } from '../../../../../utils/basic-selectors';
import { User } from '../../../../../../../projects/sqtm-core/src/lib/model/user/user.model';

export class TeamMembersPanelElement {
  public readonly grid: GridElement;

  constructor() {
    this.grid = GridElement.createGridElement('team-members');
  }

  waitInitialDataFetch() {}

  deleteOne(userName: string) {
    this.grid.findRowId('fullName', userName).then((id) => {
      this.grid.getRow(id).cell('delete').iconRenderer().click();

      const deleteMock = new HttpMockBuilder('teams/*/members/*').delete().build();

      this.clickConfirmDeleteButton();

      deleteMock.wait();
    });
  }

  deleteMultiple(memberNames: string[]) {
    this.grid.selectRowsWithMatchingCellContent('fullName', memberNames);

    const deleteMock = new HttpMockBuilder('teams/*/members/*').delete().build();

    this.clickOnDeleteButton();
    this.clickConfirmDeleteButton();

    deleteMock.wait();
  }

  clickOnAddTeamMemberButton(members?: Member[]): AddTeamMemberDialog {
    new HttpMockBuilder('**/team-view/*/has-custom-profile').responseBody(false).build();

    const dialog = new AddTeamMemberDialog(this.grid, members);

    cy.get('[data-test-button-id="add-members"]').should('exist').click();

    dialog.waitInitialDataFetch();
    dialog.assertExists();

    return dialog;
  }

  private clickOnDeleteButton() {
    cy.get(selectByDataTestToolbarButtonId('remove-members')).should('exist').click();
  }

  private clickConfirmDeleteButton() {
    cy.get('sqtm-core-confirm-delete-dialog')
      .find('[data-test-dialog-button-id="confirm"]')
      .click()
      // Then
      .get('sqtm-core-confirm-delete-dialog')
      .should('not.exist');
  }

  showUserDetail(userName: string, response: User): void {
    const mock = new HttpMockBuilder('user-view/*?**').responseBody(response).build();

    this.grid.findRowId('fullName', userName).then((id) => {
      this.grid.getRow(id).cell('fullName').linkRenderer().findCellLink().click();
    });

    mock.wait();
  }
}
