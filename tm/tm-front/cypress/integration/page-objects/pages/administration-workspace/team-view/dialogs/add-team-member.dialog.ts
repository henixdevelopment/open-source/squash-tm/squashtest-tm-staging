import { HttpMock, HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import { GroupedMultiListElement } from '../../../../elements/filters/grouped-multi-list.element';
import { GridElement } from '../../../../elements/grid/grid.element';

export interface Member {
  partyId: number;
  firstName: string;
  lastName: string;
  login: string;
  fullName: string;
}

export class AddTeamMemberDialog {
  private readonly dialogId = 'add-team-member-dialog';

  private nonMembers: HttpMock<Member[]>;

  constructor(
    private readonly permissionsGrid: GridElement,
    nonMembers: Member[],
  ) {
    this.nonMembers = new HttpMockBuilder<Member[]>('team-view/*/non-members')
      .responseBody(nonMembers)
      .build();
  }

  waitInitialDataFetch() {
    this.nonMembers.wait();
  }

  assertExists() {
    cy.get(this.buildSelector()).should('exist');
  }

  assertNotExist(): void {
    cy.get(this.buildSelector()).should('not.exist');
  }

  confirm(updatedMembers?: Member[]) {
    const mock = new HttpMockBuilder('teams/*/members/*')
      .responseBody({ members: updatedMembers })
      .post()
      .build();

    this.clickOnConfirmButton();

    mock.wait();
  }

  cancel() {
    const buttonSelector = this.buildButtonSelector('cancel');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  buildSelector(): string {
    return `[data-test-dialog-id=${this.dialogId}]`;
  }

  clickOnConfirmButton() {
    const buttonSelector = this.buildButtonSelector('confirm');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  private buildButtonSelector(buttonId: string) {
    return `${this.buildSelector()} [data-test-dialog-button-id=${buttonId}]`;
  }

  selectMembers(multiList: GroupedMultiListElement, ...teamMembers: string[]) {
    this.clickOnSelectMembers();
    teamMembers.forEach((name) => multiList.toggleOneItem(name));
    cy.clickVoid();
    multiList.assertNotExist();
  }

  assertMemberNotExist(teamMember: string, multiList: GroupedMultiListElement) {
    this.clickOnSelectMembers();
    multiList.assertItemNotExist(teamMember);
  }

  private clickOnSelectMembers() {
    cy.get('[data-test-element-id="grouped-multi-list-display-value"]').click();
  }

  cancelAndAssertNotExist() {
    this.cancel();
    this.assertNotExist();
  }
}
