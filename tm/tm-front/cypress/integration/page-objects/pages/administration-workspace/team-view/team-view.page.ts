import { Page } from '../../page';
import { TeamMembersPanelElement } from './panels/team-members-panel.element';
import { TeamAuthorisationsPanelElement } from './panels/team-authorisations-panel.element';
import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';

export class TeamViewPage extends Page {
  public readonly teamAuthorisationsPanel: TeamAuthorisationsPanelElement;
  public readonly teamMembersPanel: TeamMembersPanelElement;
  public readonly nameTextField: EditableTextFieldElement;

  public readonly anchors = AnchorsElement.withLinkIds('information', 'authorisation', 'members');

  constructor(selector: string = 'sqtm-app-team-view > sqtm-core-entity-view-wrapper > div') {
    super(selector);
    this.teamAuthorisationsPanel = new TeamAuthorisationsPanelElement();
    this.teamMembersPanel = new TeamMembersPanelElement();
    this.nameTextField = new EditableTextFieldElement('entity-name', 'teams/*/name');
  }

  waitInitialDataFetch() {
    this.teamAuthorisationsPanel.waitInitialDataFetch();
    this.teamMembersPanel.waitInitialDataFetch();
  }

  foldGrid() {
    cy.get(`[data-test-element-id="fold-tree-button"]`).click();
  }

  close() {
    cy.get(`[data-test-element-id="close-view-button"]`).click();
  }
}
