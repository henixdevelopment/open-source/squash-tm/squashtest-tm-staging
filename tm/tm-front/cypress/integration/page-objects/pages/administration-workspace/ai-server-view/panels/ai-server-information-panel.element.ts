import { Page } from '../../../page';
import { EditableTextFieldElement } from '../../../../elements/forms/editable-text-field.element';
import { EditableRichTextFieldElement } from '../../../../elements/forms/editable-rich-text-field.element';
import { HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import { AiServer } from '../../../../../../../projects/sqtm-core/src/lib/model/artificial-intelligence/ai-server.model';
import { selectByDataTestFieldId } from '../../../../../utils/basic-selectors';

export class AiServerInformationPanelElement extends Page {
  urlField: EditableTextFieldElement;
  descriptionField: EditableRichTextFieldElement;

  constructor() {
    super('sqtm-app-ai-server-information-panel');
    this.urlField = new EditableTextFieldElement('ai-server-url', 'ai-server/*/url');
    this.descriptionField = new EditableRichTextFieldElement('ai-server-description');
  }

  checkInfo(expectedUrl: string, expectedDescription: string) {
    this.urlField.checkContent(expectedUrl);
    this.descriptionField.checkHtmlContent(expectedDescription);
  }

  updateUrl(newUrl: string) {
    this.urlField.assertExists();
    this.urlField.assertIsEditable();
    this.urlField.setAndConfirmValue(newUrl);
    this.urlField.checkContent(newUrl);
  }

  updateDescription(newDescription: string, response: AiServer) {
    const mock = new HttpMockBuilder('ai-server/*/description')
      .responseBody(response)
      .post()
      .build();
    this.descriptionField.assertIsEditable();
    this.descriptionField.click();
    this.descriptionField.fillWithString(newDescription);
    this.descriptionField.confirm();
    mock.wait();
    this.descriptionField.checkTextContent(newDescription);
  }

  assertModification(shouldHaveBeenModified: boolean) {
    const modificationField = cy.get(selectByDataTestFieldId('ai-server-modified'));
    if (shouldHaveBeenModified) {
      modificationField.should('not.contain.text', 'jamais');
    } else {
      modificationField.should('contain.text', 'jamais');
    }
  }
}
