import { Page } from '../../../page';
import { EditableTextFieldElement } from '../../../../elements/forms/editable-text-field.element';
import { HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import { Credentials } from '../../../../../../../projects/sqtm-core/src/lib/model/third-party-server/credentials.model';
import {
  selectByDataTestElementId,
  selectByDataTestInputName,
} from '../../../../../utils/basic-selectors';

export class AiServerAuthPolicyPanelElement extends Page {
  tokenField = new EditableTextFieldElement('credentials-password');

  constructor() {
    super('sqtm-app-ai-server-auth-policy-panel');
  }

  updateToken(newToken: string, response: Credentials) {
    const mock = new HttpMockBuilder('ai-server/*/credentials')
      .responseBody(response)
      .post()
      .build();
    const confirmButton = cy.get(selectByDataTestElementId('save-credentials-button'));

    this.tokenField.assertExists();
    this.tokenField.click();
    this.tokenField.fillWithString(newToken);
    confirmButton.click();
    mock.wait();
  }

  assertSuccess(expectedMessage: string) {
    this.assertInfoIconIsVisible();
    this.findByElementId('credentials-status-message').should('contain.text', expectedMessage);
  }

  assertNoTokenInfo() {
    this.assertWarningIconIsVisible();
  }

  deleteToken() {
    const mock = new HttpMockBuilder('ai-server/*/credentials').delete().build();
    const deleteTokenButton = cy.get(selectByDataTestElementId('revoke-credentials-button'));
    deleteTokenButton.click();
    mock.wait();
    this.tokenField.assertContainsText('');
  }

  fillTokenField(token: string) {
    this.assertWarningIconIsVisible();
    this.find(selectByDataTestInputName('password')).should('exist').click();
    this.tokenField.fillWithString(token);
    this.findByElementId('save-credentials-button').click();
    this.tokenField.assertIsNotEditable();
    this.assertInfoIconIsVisible();
  }

  private assertWarningIconIsVisible() {
    this.findByElementId('credentials-status-icon:WARNING').should('exist');
  }

  private assertInfoIconIsVisible() {
    this.findByElementId('credentials-status-icon:INFO').should('exist');
  }
}
