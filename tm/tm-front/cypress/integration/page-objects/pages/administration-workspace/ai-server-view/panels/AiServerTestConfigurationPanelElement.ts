import { Page } from '../../../page';
import { EditableTextAreaFieldElement } from '../../../../elements/forms/editable-text-area-field.element';
import {
  selectByDataTestButtonId,
  selectByDataTestDialogId,
  selectByDataTestFieldId,
} from '../../../../../utils/basic-selectors';
import { apiBaseUrl } from '../../../../../utils/mocks/request-mock';

export class AiServerTestConfigurationPanelElement extends Page {
  readonly requirementModelText = new EditableTextAreaFieldElement('ai-server-requirement-model');

  constructor() {
    super('sqtm-app-ai-server-requirement-model-panel');
  }

  assertTestConfigurationButtonDisabled() {
    cy.get(this.rootSelector)
      .find(selectByDataTestButtonId('test-configuration'))
      .should('be.disabled');
  }

  assertTestConfigurationButtonActive() {
    cy.get(this.rootSelector)
      .find(selectByDataTestButtonId('test-configuration'))
      .should('be.enabled');
  }

  callExternalAiServer() {
    cy.url().then((url) => {
      /*
      retrieve the id from the url
       */
      const id = url.split('ai-server/')[1].split('/')[0];
      const backEndPoint = `${apiBaseUrl()}/ai-server/${id}/test-api`;
      cy.intercept('POST', backEndPoint).as('testApiRequest');
      cy.get(this.rootSelector).find(selectByDataTestButtonId('test-configuration')).click();

      cy.wait('@testApiRequest');
      cy.get(selectByDataTestDialogId('ai-generate-test-cases')).should('exist');
    });
  }

  fillEmtpyRequirementField(requirement: string) {
    this.assertTestConfigurationButtonDisabled();
    cy.get(this.rootSelector)
      .find(selectByDataTestFieldId('ai-server-requirement-model'))
      .contains('Cliquer pour éditer...');
    cy.get(this.rootSelector)
      .find(selectByDataTestFieldId('ai-server-requirement-model'))
      .click()
      .type(requirement);
    cy.clickVoid();
    this.assertTestConfigurationButtonActive();
  }
}
