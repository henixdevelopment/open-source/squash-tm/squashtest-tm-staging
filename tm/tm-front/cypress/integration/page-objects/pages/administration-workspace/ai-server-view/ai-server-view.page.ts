import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';
import { Page } from '../../page';
import { AiServerInformationPanelElement } from './panels/ai-server-information-panel.element';
import { AiServerAuthPolicyPanelElement } from './panels/ai-server-auth-policy-panel.element';
import { AiServerConfigurationPanelElement } from './panels/ai-server-configuration-panel.element';
import { AiServerTestConfigurationPanelElement } from './panels/AiServerTestConfigurationPanelElement';
import { selectByDataTestButtonId } from '../../../../utils/basic-selectors';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';

export class AiServerViewPage extends Page {
  readonly entityNameField: EditableTextFieldElement;
  readonly informationPanel = new AiServerInformationPanelElement();
  readonly authenticationPanel = new AiServerAuthPolicyPanelElement();
  readonly configurationPanel = new AiServerConfigurationPanelElement();
  readonly testConfigurationPanel = new AiServerTestConfigurationPanelElement();
  readonly anchors = AnchorsElement.withLinkIds(
    'information',
    'authentication-protocol',
    'authentication-policy',
    'payload',
    'requirement-test',
  );

  constructor() {
    super('sqtm-app-ai-server-view');
    this.entityNameField = new EditableTextFieldElement('entity-name', 'ai-server/*/name');
  }

  waitInitialDataFetch() {
    //
  }
  assertIncompleteAiServerConfiguration() {
    cy.get(this.rootSelector)
      .find('sqtm-app-ai-server-requirement-model-panel')
      .find(selectByDataTestButtonId('test-configuration'))
      .should('not.be.enabled');
  }

  clickAuthPolicyAnchor(): AiServerAuthPolicyPanelElement {
    this.anchors.clickLink('authentication-policy');
    return new AiServerAuthPolicyPanelElement();
  }
}
