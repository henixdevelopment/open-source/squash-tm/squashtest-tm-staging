import { Page } from '../../../page';
import { EditableTextFieldElement } from '../../../../elements/forms/editable-text-field.element';
import { HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import {
  selectByDataTestButtonId,
  selectByDataTestFieldId,
} from '../../../../../utils/basic-selectors';
import { EditableTextAreaFieldElement } from '../../../../elements/forms/editable-text-area-field.element';

export class AiServerConfigurationPanelElement extends Page {
  readonly modelConfigurationField = new EditableTextAreaFieldElement(
    'ai-server-model-configuration',
    'ai-server/*/new-payload',
  );
  readonly jsonField = new EditableTextFieldElement('ai-server-jsonpath');
  readonly mockJsonPath = new HttpMockBuilder('ai-server/*/new-jsonpath').post().build();

  constructor() {
    super('sqtm-app-ai-server-payload-panel');
  }

  updateJsonPath(newJsonPath: string) {
    this.jsonField.fillWithString(newJsonPath);
    const confirmButton = cy.get(selectByDataTestButtonId('textfield-confirm-btn'));
    confirmButton.click();
    this.mockJsonPath.wait();
  }

  deleteJsonPath() {
    this.jsonField.clearInEditMode();
    const confirmButton = cy.get(selectByDataTestButtonId('textfield-confirm-btn'));
    confirmButton.click();
    this.mockJsonPath.wait();
  }

  fillEmtpyPayloadField(payload: string) {
    cy.get(this.rootSelector)
      .find(selectByDataTestFieldId('ai-server-model-configuration'))
      .contains('Cliquer pour éditer...');
    cy.get(this.rootSelector)
      .find(selectByDataTestFieldId('ai-server-model-configuration'))
      .click()
      .type(payload);
    cy.get(this.rootSelector).find(selectByDataTestButtonId('textfield-confirm-btn')).click();
  }
  fillEmtpyJsonPathField(jsonPath: string) {
    cy.get(this.rootSelector)
      .find(selectByDataTestFieldId('ai-server-jsonpath'))
      .contains('Cliquer pour éditer...');
    cy.get(this.rootSelector)
      .find(selectByDataTestFieldId('ai-server-jsonpath'))
      .click()
      .type(jsonPath);
    cy.clickVoid();
  }
}
