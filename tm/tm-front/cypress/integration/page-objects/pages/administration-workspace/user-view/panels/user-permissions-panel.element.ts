import { SwitchFieldElement } from '../../../../elements/forms/switch-field.element';
import { BasicElement } from '../../../../elements/abstract-element';

export class UserPermissionsPanelElement extends BasicElement {
  public readonly canDeleteFromFrontSwitchElement: SwitchFieldElement = new SwitchFieldElement(
    () => this.findByElementId('can-delete-from-front'),
    'users/*/can-delete-from-front',
  );

  constructor() {
    super('sqtm-app-user-permissions-panel');
  }
}
