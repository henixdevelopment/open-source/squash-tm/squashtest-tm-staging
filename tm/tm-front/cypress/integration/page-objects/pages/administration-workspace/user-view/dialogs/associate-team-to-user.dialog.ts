import { HttpMock, HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import { GroupedMultiListElement } from '../../../../elements/filters/grouped-multi-list.element';
import { GridElement } from '../../../../elements/grid/grid.element';
import { BaseDialogElement } from '../../../../elements/dialog/base-dialog.element';

export interface TeamAssociation {
  partyId: string;
  name: string;
}

export class AssociateTeamToUserDialog extends BaseDialogElement {
  private unassociatedTeamMock: HttpMock<TeamAssociation[]>;

  constructor(
    private readonly permissionsGrid: GridElement,
    unassociatedTeams: TeamAssociation[],
  ) {
    super('associate-team-to-user-dialog');
    this.unassociatedTeamMock = new HttpMockBuilder<TeamAssociation[]>(
      'user-view/*/unassociated-teams',
    )
      .responseBody(unassociatedTeams)
      .build();
  }

  waitInitialDataFetch() {
    this.unassociatedTeamMock.wait();
  }

  confirm(updatedTeams?: TeamAssociation[]) {
    const mock = new HttpMockBuilder('users/*/teams/*')
      .responseBody({ teams: updatedTeams })
      .post()
      .build();

    this.clickOnConfirmButton();

    mock.wait();
  }

  selectTeams(...teamNames: string[]) {
    this.clickOnSelectMembers();
    const multiList = new GroupedMultiListElement();
    teamNames.forEach((name) => multiList.toggleOneItem(name));
    cy.clickVoid();
    multiList.assertNotExist();
  }

  clickOnSelectMembers() {
    cy.get('[data-test-element-id="grouped-multi-list-display-value"]').click();
  }

  assertSelectMembersFieldHasText(expectedValue: string) {
    this.findByElementId('grouped-multi-list-display-value').should('contain.text', expectedValue);
  }

  checkSelectMembersFieldContentTruncation() {
    this.findByElementId('grouped-multi-list-display-value').should('have.class', 'txt-ellipsis');
  }

  checkConformity() {
    this.assertTitleHasText('Associer des équipes');
    this.checkFieldHasPlaceHolder();
    this.checkFooterButtons();
  }

  private checkFieldHasPlaceHolder() {
    this.checkTeamFieldContent('Sélectionner des équipes');
  }

  checkTeamFieldContent(expectedContent: string) {
    this.findByFieldName('team-list').should('contain.text', expectedContent);
  }

  private checkFooterButtons() {
    this.selectButton('confirm').should('contain.text', 'Confirmer');
    this.selectButton('cancel').should('contain.text', 'Annuler');
  }
}
