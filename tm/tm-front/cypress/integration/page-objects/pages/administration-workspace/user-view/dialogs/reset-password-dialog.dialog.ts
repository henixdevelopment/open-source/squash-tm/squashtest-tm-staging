import { TextFieldElement } from '../../../../elements/forms/TextFieldElement';
import { BaseDialogElement } from '../../../../elements/dialog/base-dialog.element';

export class ResetPasswordDialog extends BaseDialogElement {
  dialogId = 'reset-password-dialog';

  public readonly password: TextFieldElement;
  public readonly confirmPassword: TextFieldElement;

  constructor() {
    super('reset-password-dialog');
    this.password = new TextFieldElement('password');
    this.confirmPassword = new TextFieldElement('confirmPassword');
  }

  buildSelector(): string {
    return `[data-test-dialog-id=${this.dialogId}]`;
  }

  private buildButtonSelector(buttonId: string) {
    return `${this.buildSelector()} [data-test-dialog-button-id=${buttonId}]`;
  }

  assertExists() {
    cy.get(this.buildSelector()).should('exist');
  }

  assertNotExist() {
    cy.get(this.buildSelector()).should('not.exist');
  }

  cancel() {
    const buttonSelector = this.buildButtonSelector('cancel');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  assertButtonExists(buttonId: string) {
    const buttonSelector = this.buildButtonSelector(buttonId);
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).should('be.visible');
  }

  clickOnConfirmButton() {
    const buttonSelector = this.buildButtonSelector('confirm');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  fillPassword(password: string) {
    this.password.fill(password);
  }

  fillConfirmPassword(confirmPassword: string) {
    this.confirmPassword.fill(confirmPassword);
  }

  checkDialogTitleContent(expectedTitle: string) {
    this.assertTitleHasText(expectedTitle);
  }
}
