import { GridElement } from '../../../../elements/grid/grid.element';

export class UserApiTokenPanelElement {
  readonly apiTokenGrid: GridElement;

  constructor() {
    this.apiTokenGrid = GridElement.createGridElement('user-view-api-token-grid');
  }

  assertExists(): void {
    cy.get('sqtm-app-user-api-token-panel').should('exist');
  }
}
