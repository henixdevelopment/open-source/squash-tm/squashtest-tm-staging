import { GridElement } from '../../../../elements/grid/grid.element';
import { HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import {
  AssociateTeamToUserDialog,
  TeamAssociation,
} from '../dialogs/associate-team-to-user.dialog';
import { selectByDataTestToolbarButtonId } from '../../../../../utils/basic-selectors';
import { Team } from '../../../../../../../projects/sqtm-core/src/lib/model/team/team.model';

export class UserTeamsPanelElement {
  public readonly grid: GridElement;

  constructor() {
    this.grid = GridElement.createGridElement('user-teams');
  }

  waitInitialDataFetch() {}

  deleteOne(teamName: string) {
    this.grid.findRowId('name', teamName).then((id) => {
      this.grid.getRow(id).cell('delete').iconRenderer().click();

      const deleteMock = new HttpMockBuilder('users/*/teams/*').delete().build();

      this.clickConfirmDeleteButton();

      deleteMock.wait();
    });
  }

  deleteMultiple(teamNames: string[]) {
    this.grid.selectRowsWithMatchingCellContent('name', teamNames);

    const deleteMock = new HttpMockBuilder('users/*/teams/*').delete().build();

    this.clickOnDeleteButton();
    this.clickConfirmDeleteButton();

    deleteMock.wait();
  }

  clickOnAssociateTeamToUserButton(
    unassociatedTeams?: TeamAssociation[],
  ): AssociateTeamToUserDialog {
    const dialog = new AssociateTeamToUserDialog(this.grid, unassociatedTeams);

    cy.get('[data-test-button-id="add-team"]').should('exist').click();

    dialog.waitInitialDataFetch();
    dialog.assertExists();

    return dialog;
  }

  private clickOnDeleteButton() {
    cy.get(selectByDataTestToolbarButtonId('remove-teams')).should('exist').click();
  }

  private clickConfirmDeleteButton() {
    cy.get('sqtm-core-confirm-delete-dialog')
      .find('[data-test-dialog-button-id="confirm"]')
      .click()
      // Then
      .get('sqtm-core-confirm-delete-dialog')
      .should('not.exist');
  }

  showTeamDetail(teamName: string, response: Team): void {
    const mock = new HttpMockBuilder('team-view/*').responseBody(response).build();

    this.grid.findRowId('name', teamName).then((id) => {
      this.grid.getRow(id).cell('name').linkRenderer().findCellLink().click();
    });

    mock.wait();
  }

  assertIsVisible() {
    cy.get('sqtm-app-user-teams-panel').should('be.visible');
  }
}
