import { Page } from '../../page';
import { UserAuthorisationsPanelElement } from './panels/user-authorisations-panel.element';
import { UserTeamsPanelElement } from './panels/user-teams-panel.element';
import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';
import { EditableSelectFieldElement } from '../../../elements/forms/editable-select-field.element';
import { ResetPasswordDialog } from './dialogs/reset-password-dialog.dialog';
import { UserPermissionsPanelElement } from './panels/user-permissions-panel.element';
import { SwitchFieldElement } from '../../../elements/forms/switch-field.element';
import { AdminWorkspaceUsersPage } from '../admin-workspace-users.page';
import {
  selectByDataTestButtonId,
  selectByDataTestElementId,
  selectByDataTestFieldId,
  selectByDataTestToolbarButtonId,
} from '../../../../utils/basic-selectors';
import { RemoveProjectDialog } from '../dialogs/remove-project-dialog.element';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';
import { UserApiTokenPanelElement } from './panels/user-api-token-panel.element';
import { GenerateApiTokenDialogElement } from '../../user-account/dialogs/generate-api-token-dialog.element';
import { DisplayTokenDialogElement } from '../../user-account/dialogs/display-token-dialog.element';

export class UserViewPage extends Page {
  readonly authorisationsPanel: UserAuthorisationsPanelElement;
  readonly teamsPanel: UserTeamsPanelElement;
  readonly permissionPanel: UserPermissionsPanelElement;
  readonly apiTokenPanel: UserApiTokenPanelElement;
  readonly loginTextField: EditableTextFieldElement;
  readonly firstNameTextField: EditableTextFieldElement;
  readonly lastNameTextField: EditableTextFieldElement;
  readonly emailTextField: EditableTextFieldElement;
  readonly groupSelectField: EditableSelectFieldElement;
  readonly activeSwitch: SwitchFieldElement;
  readonly generateApiTokenDialogElement: GenerateApiTokenDialogElement;
  readonly displayTokenDialogElement: DisplayTokenDialogElement;

  public readonly anchors = AnchorsElement.withLinkIds(
    'information',
    'authorisations',
    'permissions',
    'teams',
  );

  constructor(selector: string = 'sqtm-app-user-view > sqtm-core-entity-view-wrapper > div') {
    super(selector);
    this.authorisationsPanel = new UserAuthorisationsPanelElement();
    this.teamsPanel = new UserTeamsPanelElement();
    this.permissionPanel = new UserPermissionsPanelElement();
    this.apiTokenPanel = new UserApiTokenPanelElement();

    this.loginTextField = new EditableTextFieldElement('entity-name', 'users/*/login');
    this.firstNameTextField = new EditableTextFieldElement('user-first-name', 'users/*/first-name');
    this.lastNameTextField = new EditableTextFieldElement('user-last-name', 'users/*/last-name');
    this.emailTextField = new EditableTextFieldElement('user-email', 'users/*/email');
    this.groupSelectField = new EditableSelectFieldElement('user-group', 'users/*/change-group/*');
    this.activeSwitch = new SwitchFieldElement('active');

    this.generateApiTokenDialogElement = new GenerateApiTokenDialogElement();
    this.displayTokenDialogElement = new DisplayTokenDialogElement();
  }

  foldGrid() {
    cy.get(`[data-test-element-id="fold-tree-button"]`).click();
  }

  close() {
    cy.get(`[data-test-element-id="close-view-button"]`).click();
    this.assertNotExist();
  }

  openResetPasswordDialog(): ResetPasswordDialog {
    cy.get('.reset-button').click();
    return new ResetPasswordDialog();
  }

  waitInitialDataFetch() {
    this.authorisationsPanel.waitInitialDataFetch();
    this.teamsPanel.waitInitialDataFetch();
  }

  checkInactiveUserLabelClass(expectedClass: 'disabled' | 'inactive') {
    cy.get(this.rootSelector)
      .find(selectByDataTestFieldId('inactive-user-label'))
      .should('have.class', expectedClass);
  }

  checkActiveUserLabelClass(expectedClass: 'disabled' | 'active') {
    cy.get(this.rootSelector)
      .find(selectByDataTestFieldId('active-user-label'))
      .should('have.class', expectedClass);
  }

  activateUserAndCheckStyle() {
    this.activeSwitch.assertIsVisible();
    this.activeSwitch.checkValue(false);
    this.checkActiveUserLabelClass('disabled');
    this.checkInactiveUserLabelClass('inactive');
    this.activeSwitch.toggle();
    this.checkActiveUserLabelClass('active');
    this.checkInactiveUserLabelClass('disabled');
  }

  deactivateUserAndCheckStyle() {
    this.activeSwitch.assertIsVisible();
    this.activeSwitch.checkValue(true);
    this.checkInactiveUserLabelClass('disabled');
    this.checkActiveUserLabelClass('active');
    this.activeSwitch.toggle();
    this.checkInactiveUserLabelClass('inactive');
    this.checkActiveUserLabelClass('disabled');
  }

  checkLastConnectionDateHasValue() {
    cy.get(this.rootSelector)
      .find(selectByDataTestFieldId('user-last-connected-on'))
      .should('not.contain.text', 'jamais');
  }

  assertAnchorTeamCount(expectedTeamCount: number) {
    cy.get(this.rootSelector)
      .find(selectByDataTestElementId('user-view-team-count'))
      .find('span')
      .should('contain.text', expectedTeamCount);
  }
  clickRemoveButton(buttonId: 'remove-teams' | 'remove-authorisations') {
    cy.get(selectByDataTestToolbarButtonId(buttonId)).click();
  }
  checkIfButtonIsActiveWithClass(buttonId: string, value: string) {
    cy.get(selectByDataTestToolbarButtonId(buttonId)).find('span').should('have.class', value);
  }

  openRemoveProjectDialog() {
    this.clickRemoveButton('remove-authorisations');
    return new RemoveProjectDialog();
  }

  clickAddApiToken() {
    cy.get(selectByDataTestButtonId('add-api-token')).click();
  }

  fillAddApiTokenForm(name: string, permissionId: number, expiryDate: string) {
    this.generateApiTokenDialogElement.fillName(name);
    this.generateApiTokenDialogElement.selectPermissions(permissionId);
    this.generateApiTokenDialogElement.fillExpiryDate(expiryDate);
  }
}

export function changeStatusToActiveWithUserViewAndCheck(
  userView: UserViewPage,
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
  userLogin: string,
) {
  userView.activateUserAndCheckStyle();
  adminWorkspaceUsersPage.grid.findRowId('login', userLogin).then((rowID) => {
    adminWorkspaceUsersPage.grid
      .getCell(rowID, 'active')
      .iconRenderer()
      .assertContainsIcon('active');
  });
}

export function changeStatusToInactiveWithUserViewAndCheck(
  userView: UserViewPage,
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
  userLogin: string,
) {
  userView.deactivateUserAndCheckStyle();
  adminWorkspaceUsersPage.grid.findRowId('login', userLogin).then((rowID) => {
    adminWorkspaceUsersPage.grid
      .getCell(rowID, 'active')
      .iconRenderer()
      .assertContainsIcon('inactive');
  });
}
