import { HttpMock, HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import { GroupedMultiListElement } from '../../../../elements/filters/grouped-multi-list.element';
import { SelectFieldElement } from '../../../../elements/forms/select-field.element';
import { GridElement } from '../../../../elements/grid/grid.element';
import {
  selectByDataTestElementId,
  selectByDataTestFieldName,
} from '../../../../../utils/basic-selectors';
import { BaseDialogElement } from '../../../../elements/dialog/base-dialog.element';

export interface ProjectWithoutPermission {
  id: string;
  name: string;
}

export class AddUserAuthorisationsDialog extends BaseDialogElement {
  profileLabels: string[] = [
    'Automaticien',
    'Chef de projet',
    'Designer de tests',
    'Invité',
    'Testeur avancé',
    'Testeur référent',
    'Testeur',
    'Valideur',
  ];

  public readonly profileSelectField: SelectFieldElement;
  dialogId = 'add-user-authorisation-dialog';

  private projectsWithoutPermissionMock: HttpMock<ProjectWithoutPermission[]>;

  constructor(
    private readonly permissionsGrid: GridElement,
    projectsWithoutPermission: ProjectWithoutPermission[],
  ) {
    super('add-user-authorisation-dialog');
    this.projectsWithoutPermissionMock = new HttpMockBuilder<ProjectWithoutPermission[]>(
      'user-view/*/projects-without-permission',
    )
      .responseBody(projectsWithoutPermission)
      .build();

    this.profileSelectField = new SelectFieldElement(() =>
      this.findByFieldName('permission-profile'),
    );
  }

  waitInitialDataFetch() {
    this.projectsWithoutPermissionMock.wait();
  }

  assertExists() {
    cy.get(this.buildSelector()).should('exist');
  }

  confirm(updatedPermissions?: ProjectWithoutPermission[]) {
    const mock = new HttpMockBuilder('users/*/projects/*/permissions/*')
      .responseBody({ projectPermissions: updatedPermissions })
      .post()
      .build();

    this.clickOnAddButton();

    mock.wait();
  }

  cancel() {
    const buttonSelector = this.buildButtonSelector('cancel');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  buildSelector(): string {
    return `[data-test-dialog-id=${this.dialogId}]`;
  }

  private buildButtonSelector(buttonId: string) {
    return `${this.buildSelector()} [data-test-dialog-button-id=${buttonId}]`;
  }

  selectProjects(...projectNames: string[]) {
    cy.get('[data-test-element-id="grouped-multi-list-display-value"]').click();
    const multiList = new GroupedMultiListElement();
    projectNames.forEach((name) => {
      multiList.toggleOneItem(name);
      multiList.assertItemIsSelectedAndPulledUp(name);
    });
    cy.clickVoid();
    multiList.assertNotExist();
  }

  selectProfile(profile: string) {
    this.profileSelectField.selectValue(profile);
  }

  checkConformity() {
    this.assertTitleHasText('Ajouter des habilitations');
    this.checkFieldsHavePlaceHolder();
    this.checkFooterButtons();
  }

  checkFieldsHavePlaceHolder() {
    this.checkProjectFieldContent('Sélectionner des projets');
    this.checkProfileFieldContent('Sélectionner un profil');
  }

  checkProjectFieldContent(expectedContent: string) {
    cy.get(this.buildSelector())
      .find(selectByDataTestFieldName('project-list'))
      .should('contain.text', expectedContent);
  }

  checkProfileFieldContent(expectedContent: string) {
    cy.get(this.buildSelector())
      .find(selectByDataTestFieldName('permission-profile'))
      .should('contain.text', expectedContent);
  }

  checkProjectListContentTruncation() {
    cy.get(this.buildSelector())
      .find(selectByDataTestElementId('grouped-multi-list-display-value'))
      .should('have.class', 'txt-ellipsis');
  }

  checkFooterButtons() {
    cy.get(this.buildButtonSelector('add-another')).should('contain.text', 'Ajouter une autre');
    cy.get(this.buildButtonSelector('add')).should('contain.text', 'Ajouter');
    cy.get(this.buildButtonSelector('cancel')).should('contain.text', 'Annuler');
  }

  clickOnProjectListAndCheckConformity(projectsNames: string[]) {
    cy.get(selectByDataTestFieldName('project-list')).click();
    const multiList = new GroupedMultiListElement();
    multiList.assertExists();
    multiList.assertSearchFieldExists();
    assertAllProjectsAppearAndHaveCheckBox(multiList, projectsNames);
    multiList.close();
  }

  clickOnProfileListAndCheckConformity() {
    const selectFieldElement = new SelectFieldElement(() =>
      this.findByFieldName('permission-profile'),
    );
    selectFieldElement.checkAllOptions(this.profileLabels);
  }

  clickOnAddAnotherButton() {
    this.clickOnButton('add-another');
  }

  clickOnAddButton() {
    this.clickOnButton('add');
  }
}

function assertAllProjectsAppearAndHaveCheckBox(
  multiList: GroupedMultiListElement,
  projectsNames: string[],
) {
  projectsNames.forEach((projectName) => {
    multiList.assertItemExist(projectName);
    multiList.assertCheckBoxExists(projectName);
  });
}
