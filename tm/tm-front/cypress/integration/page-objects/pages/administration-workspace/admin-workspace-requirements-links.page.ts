import { NavBarElement } from '../../elements/nav-bar/nav-bar.element';
import { GridElement } from '../../elements/grid/grid.element';
import { AdministrationWorkspacePage } from './administration-workspace.page';
import { PageFactory } from '../page';
import { CreateRequirementsLinkDialog } from './dialogs/create-requirements-links-dialog.element';
import { AdminReferentialDataProviderBuilder } from '../../../utils/referential/admin-referential-data.provider';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { RequirementVersionLinkType } from '../../../../../projects/sqtm-core/src/lib/model/requirement/requirement-version-link-type.model';
import { ReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';

export class AdminWorkspaceRequirementsLinksPage extends AdministrationWorkspacePage {
  public readonly navBar = new NavBarElement();

  constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-main-custom-workspace');
  }

  public static initTestAtPageRequirementsLinks: PageFactory<AdminWorkspaceRequirementsLinksPage> =
    (
      requirementVersionLinkTypes: RequirementVersionLinkType[],
      referentialData?: ReferentialDataModel,
    ) => {
      return AdminWorkspaceRequirementsLinksPage.initTestAtPage(
        requirementVersionLinkTypes,
        'requirementsLinks',
        'entities-customization/requirements-links',
        referentialData,
      );
    };

  public static initTestAtPage: PageFactory<AdminWorkspaceRequirementsLinksPage> = (
    requirementVersionLinkTypes: RequirementVersionLinkType[],
    gridId: string,
    pageUrl: string,
    referentialData?: ReferentialDataModel,
  ) => {
    const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder(
      referentialData,
    ).build();
    const reqLinksMock = new HttpMockBuilder('requirements-links')
      .responseBody({ requirementLinks: requirementVersionLinkTypes })
      .post()
      .build();
    const gridElement = GridElement.createGridElement(gridId);
    // visit page
    cy.visit(`administration-workspace/${pageUrl}`);
    // wait for ref data request to fire
    adminReferentialDataProvider.wait();
    // wait for requirement links data request to fire
    reqLinksMock.waitResponseBody();
    return new AdminWorkspaceRequirementsLinksPage(gridElement);
  };

  openCreateRequirementsLink(): CreateRequirementsLinkDialog {
    this.clickCreateButton();
    return new CreateRequirementsLinkDialog();
  }

  assertExists() {
    super.assertExists();
  }

  protected getPageUrl(): string {
    return 'requirements-links';
  }

  protected getDeleteUrl(): string {
    return 'requirements-links';
  }
}
