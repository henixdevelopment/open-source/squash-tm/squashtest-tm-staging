import { NavBarElement } from '../../elements/nav-bar/nav-bar.element';
import { GridElement, UserGridElement } from '../../elements/grid/grid.element';
import { AdministrationWorkspacePage } from './administration-workspace.page';
import { PageFactory } from '../page';
import { AdminWorkspaceTeamsPage } from './admin-workspace-teams.page';
import { CreateUserDialog } from './dialogs/create-user-dialog.element';
import { HttpMock, HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { UserViewPage } from './user-view/user-view.page';
import { AdminReferentialDataProviderBuilder } from '../../../utils/referential/admin-referential-data.provider';
import { UserMultiEditDialogElement } from './dialogs/user-multi-edit-dialog.element';
import {
  selectByDataTestButtonId,
  selectByDataTestToolbarButtonId,
} from '../../../utils/basic-selectors';
import { User } from '../../../../../projects/sqtm-core/src/lib/model/user/user.model';
import { AdminReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/admin-referential-data.model';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { AnchorsElement } from '../../elements/anchor/anchors.element';
import { RemoveUserDialogElement } from './dialogs/remove-user-dialog.element';

export class AdminWorkspaceUsersPage extends AdministrationWorkspacePage {
  readonly anchors = AnchorsElement.withLinkIds('team', 'manage');

  public readonly navBar = new NavBarElement();

  private usersGroupsMock: HttpMock<any>;

  constructor(public readonly grid: UserGridElement) {
    super(grid, 'sqtm-app-main-user-workspace');

    this.usersGroupsMock = new HttpMockBuilder('users/get-users-groups')
      .responseBody({
        usersGroups: [
          { id: 1, qualifiedName: 'squashtest.authz.group.core.Admin' },
          { id: 2, qualifiedName: 'squashtest.authz.group.tm.User' },
          { id: 4, qualifiedName: 'squashtest.authz.group.tm.TestAutomationServer' },
        ],
      })
      .build();
  }

  public static initTestAtPage: PageFactory<AdminWorkspaceUsersPage> = (
    initialNodes: GridResponse = { dataRows: [] },
    referentialData?: AdminReferentialDataModel,
  ) => {
    const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder(
      referentialData,
    ).build();

    const userGridElement: UserGridElement = UserGridElement.createUserGridElement(
      'users',
      'users',
      initialNodes,
    );
    const page = new AdminWorkspaceUsersPage(userGridElement);

    // visit page
    cy.visit(`administration-workspace/${page.getPageUrl()}`);

    // Check page initialisation
    page.assertExists();

    // wait for ref data request to fire
    adminReferentialDataProvider.wait();

    // wait for initial grid data and additional requests to fire
    page.waitInitialDataFetch();

    return page;
  };

  public waitInitialDataFetch() {
    super.waitInitialDataFetch();
  }

  public assertExists() {
    super.assertExists();
  }

  goToTeamAnchor(initialGridData?: GridResponse): AdminWorkspaceTeamsPage {
    this.anchors.clickLink('team');
    cy.get('[data-test-grid-id="teams"]').should('exist');
    const gridElement = GridElement.createGridElement('teams', 'teams', initialGridData);
    const page = new AdminWorkspaceTeamsPage(gridElement);
    page.assertExists();
    return page;
  }

  selectUserByLogin(
    name: string,
    viewResponse?: User,
    mockPersonalApiTokens?: HttpMock<GridResponse>,
  ): UserViewPage {
    const userView = new UserViewPage();
    const mock = new HttpMockBuilder('user-view/*').responseBody(viewResponse).build();

    this.selectRowWithMatchingCellContent('login', name);

    if (mockPersonalApiTokens) {
      mockPersonalApiTokens.wait();
    }
    mock.waitResponseBody().then(() => {
      userView.waitInitialDataFetch();
      userView.assertExists();
    });

    return userView;
  }

  openCreateUser(): CreateUserDialog {
    this.clickCreateButton();
    return new CreateUserDialog();
  }

  protected getPageUrl(): string {
    return 'users/manage';
  }

  protected getDeleteUrl(): string {
    return 'users';
  }

  openMultiEditDialog(): UserMultiEditDialogElement {
    this.clickMultiEditButton();
    return new UserMultiEditDialogElement();
  }

  private clickMultiEditButton(): void {
    cy.get(selectByDataTestButtonId('multi-edit')).click();
  }

  openDeleteDialog(): RemoveUserDialogElement {
    this.clickDeleteButton();
    return new RemoveUserDialogElement();
  }
  private clickDeleteButton(): void {
    cy.get(selectByDataTestToolbarButtonId('delete-button')).click();
  }
}
