import { NavBarElement } from '../../elements/nav-bar/nav-bar.element';
import { GridElement } from '../../elements/grid/grid.element';
import { AdministrationWorkspacePage } from './administration-workspace.page';
import { PageFactory } from '../page';
import { CreateMilestoneDialog } from './dialogs/create-milestone-dialog.element';
import { AdminReferentialDataProviderBuilder } from '../../../utils/referential/admin-referential-data.provider';
import { MilestoneViewPage } from './milestone-view/milestone-view.page';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { MilestoneAdminView } from '../../../../../projects/sqtm-core/src/lib/model/milestone/milestone.model';
import { MilestonePossibleOwner } from '../../../../../projects/sqtm-core/src/lib/model/user/user.model';
import { ReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { selectByDataTestToolbarButtonId } from '../../../utils/basic-selectors';
import { CreateMilestoneDuplicationDialog } from './dialogs/create-milestone-duplication-dialog';
import { CreateMilestoneSynchronizationDialog } from './dialogs/create-milestone-synchronization-dialog';

export class AdminWorkspaceMilestonesPage extends AdministrationWorkspacePage {
  public readonly navBar = new NavBarElement();

  constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-milestone-workspace');
  }

  public static initTestAtPageMilestones: PageFactory<AdminWorkspaceMilestonesPage> = (
    milestones: GridResponse,
    referentialData?: ReferentialDataModel,
  ) => {
    return AdminWorkspaceMilestonesPage.initTestAtPage(
      milestones,
      'milestones',
      'milestones',
      referentialData,
    );
  };

  public static initTestAtPage: PageFactory<AdminWorkspaceMilestonesPage> = (
    milestones: GridResponse,
    gridId: string,
    pageUrl: string,
    referentialData?: ReferentialDataModel,
  ) => {
    const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder(
      referentialData,
    ).build();
    const milestonesMock = new HttpMockBuilder('milestones')
      .responseBody(milestones)
      .post()
      .build();
    const gridElement = GridElement.createGridElement(gridId);
    // visit page
    cy.visit(`administration-workspace/${pageUrl}`);
    // wait for ref data request to fire
    adminReferentialDataProvider.wait();
    // wait for initial milestones data request to fire
    milestonesMock.waitResponseBody();
    return new AdminWorkspaceMilestonesPage(gridElement);
  };

  openCreateMilestone(): CreateMilestoneDialog {
    this.clickCreateButton();
    return new CreateMilestoneDialog();
  }

  openDuplicateMilestone() {
    this.find(selectByDataTestToolbarButtonId('duplicate-button')).click();
    return new CreateMilestoneDuplicationDialog();
  }
  openSynchronizeMilestone() {
    this.find(selectByDataTestToolbarButtonId('synchronize-button')).click();
    return new CreateMilestoneSynchronizationDialog();
  }

  protected getPageUrl(): string {
    return 'milestones';
  }

  protected getDeleteUrl(): string {
    return 'milestones';
  }

  selectMilestoneByLabel(
    label: string,
    viewResponse: MilestoneAdminView,
    possibleOwners: MilestonePossibleOwner[],
  ): MilestoneViewPage {
    const view = new MilestoneViewPage(possibleOwners);
    const mock = new HttpMockBuilder('milestone-view/*').responseBody(viewResponse).build();

    this.selectRowWithMatchingCellContent('label', label);

    mock.waitResponseBody().then(() => {
      view.waitInitialDataFetch();
      view.assertExists();
    });

    return view;
  }
}
