import { GridElement } from '../../../../elements/grid/grid.element';
import { HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import { AddInfoListItemDialogElement } from '../dialogs/add-info-list-item-dialog.element';

export class InfoListItemsPanelElement {
  public readonly grid: GridElement;

  constructor() {
    this.grid = GridElement.createGridElement('infoListItems');
  }

  assertDeleteIconIsVisible(itemLabel: string): void {
    this.grid.findRowId('label', itemLabel).then((id) => {
      this.grid.getRow(id).cell('delete').iconRenderer().assertIsVisible();
    });
  }

  assertDeleteIconIsHidden(itemLabel: string): void {
    this.grid
      .findRowId('label', itemLabel)
      .then((id) => this.grid.getRow(id).cell('delete').iconRenderer().assertNotExist());
  }

  setDefaultItem(itemLabel: string): void {
    const mock = new HttpMockBuilder('info-list-items/*/default').post().build();

    this.grid.findRowId('label', itemLabel).then((id) => {
      this.grid.getRow(id).cell('isDefault').radioNgZorroRenderer().findRadio().click();
      mock.wait();
    });
  }

  openAddItemDialog(): AddInfoListItemDialogElement {
    const dialog = new AddInfoListItemDialogElement();
    this.clickAddButton();
    return dialog;
  }

  clickAddButton(): void {
    cy.get('[data-test-button-id="add-item"]').click();
  }
}
