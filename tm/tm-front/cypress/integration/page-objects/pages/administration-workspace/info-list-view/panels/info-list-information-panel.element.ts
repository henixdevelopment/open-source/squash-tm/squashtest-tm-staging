import { Page } from '../../../page';
import { EditableTextFieldElement } from '../../../../elements/forms/editable-text-field.element';
import { EditableRichTextFieldElement } from '../../../../elements/forms/editable-rich-text-field.element';

export class InfoListInformationPanelElement extends Page {
  public readonly codeField: EditableTextFieldElement;
  public readonly descriptionField: EditableRichTextFieldElement;

  constructor() {
    super('sqtm-app-info-list-information-panel');

    const urlPrefix = 'info-lists/*/';

    this.codeField = new EditableTextFieldElement('info-list-code', urlPrefix + 'code');
    this.descriptionField = new EditableRichTextFieldElement(
      'info-list-description',
      urlPrefix + 'description',
    );
  }
}
