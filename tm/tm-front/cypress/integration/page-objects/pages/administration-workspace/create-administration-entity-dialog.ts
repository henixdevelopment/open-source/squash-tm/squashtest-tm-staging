import {
  CreateDialogOptions,
  CreationDialogElement,
} from '../../elements/dialog/creation-dialog.element';
import { HttpMockBuilder, HttpResponseStatus } from '../../../utils/mocks/request-mock';
import Chainable = Cypress.Chainable;
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class CreateAdministrationEntityDialog extends CreationDialogElement<AdministrationCreateDialogOptions> {
  constructor(
    public readonly dialogId: string,
    public readonly newEntityUrl: string,
    public readonly gridUrl: string,
  ) {
    super(dialogId);
  }

  addWithOptions(options: AdministrationCreateDialogOptions): Chainable<any> {
    const chainable = this.addForSuccess(
      options.addAnother,
      options.createResponse,
      options.gridResponse,
      options.createResponseStatus,
    );

    if (options.addAnother) {
      this.assertExists();
    } else {
      this.assertNotExist();
    }

    return chainable;
  }

  protected addForSuccess(
    addAnother: boolean,
    createResponse?: any,
    gridResponse?: GridResponse,
    createResponseStatus?: HttpResponseStatus,
  ): Chainable<any> {
    const mockAdd = new HttpMockBuilder<any>(this.getNewEntityUrl())
      .post()
      .responseBody(createResponse)
      // fixme : this smells fishy
      .status(createResponseStatus || 200 || 201)
      .build();

    const mockGrid = new HttpMockBuilder<GridResponse>(this.gridUrl)
      .post()
      .responseBody(gridResponse)
      .build();
    if (addAnother) {
      this.clickOnAddAnotherButton();
    } else {
      this.clickOnAddButton();
    }

    return mockAdd.waitResponseBody().then(() => {
      mockGrid.waitResponseBody();
    });
  }

  protected getNewEntityUrl(): string {
    return this.newEntityUrl;
  }
}

interface AdministrationCreateDialogOptions extends CreateDialogOptions {
  /**
   * The grid response after the new entity was added.
   * This is the whole visible grid data as the grid is refreshed.
   */
  gridResponse?: GridResponse;

  /**
   * The server's response status after creation.
   */
  createResponseStatus?: HttpResponseStatus;
}
