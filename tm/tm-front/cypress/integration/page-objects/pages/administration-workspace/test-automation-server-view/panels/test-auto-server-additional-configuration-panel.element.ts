import { Page } from '../../../page';
import { ToolbarElement } from '../../../../elements/workspace-common/toolbar.element';
import { HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import {
  selectByDataTestButtonId,
  selectByDataTestElementId,
} from '../../../../../utils/basic-selectors';
import { AlertDialogElement } from '../../../../elements/dialog/alert-dialog.element';

export class TestAutoServerAdditionalConfigurationPanelElement extends Page {
  EDITOR = 'ace-editor';
  TWO_PANELS_EDITOR = 'div.ace_editor';
  CONFIRM_BUTTON_ID = 'ace-confirm';
  HELP_BUTTON_ID = 'ace-help';
  CHECK_BUTTON = 'ace-check';
  TEXT_INPUT = 'div.ace_text-layer';
  toolbarElement: ToolbarElement;

  constructor() {
    super('sqtm-app-orchestrator-additional-configuration-panel');
    this.toolbarElement = new ToolbarElement('ace-toolbar');
  }

  showEditButtons() {
    cy.get(selectByDataTestElementId('ace-editor')).click();
    this.toolbarElement.shouldExist();
  }

  insertScript(script: string) {
    this.showEditButtons();
    cy.get('textarea').type(script);
    this.confirmEdit();
  }

  confirmEdit() {
    const httpMock = new HttpMockBuilder('test-automation-servers/*/additional-configuration')
      .post()
      .build();
    this.toolbarElement.button(this.CONFIRM_BUTTON_ID).clickWithoutSpan();
    httpMock.wait();
  }

  checkSyntaxMessageShouldBe(msg: string, isValid: boolean) {
    this.showEditButtons();
    const httpMock = new HttpMockBuilder('test-automation-servers/*/check-additional-configuration')
      .post()
      .responseBody(isValid)
      .build();
    this.toolbarElement.button(this.CHECK_BUTTON).clickWithoutSpan();
    httpMock.wait();
    const alertDialog = new AlertDialogElement('check-additional-configuration-validity-dialog');
    alertDialog.assertHasMessage(msg);
  }

  assertAceEditorExist() {
    cy.get(selectByDataTestElementId(this.EDITOR)).should('exist');
  }

  assertAceEditorContainsText(text: string) {
    cy.get(this.TEXT_INPUT).should('contain.text', text);
  }

  toggleHelp() {
    cy.get(selectByDataTestButtonId(this.HELP_BUTTON_ID)).click();
  }

  assertHelpExist() {
    cy.get(this.TWO_PANELS_EDITOR).should('have.length', 2);
  }
}
