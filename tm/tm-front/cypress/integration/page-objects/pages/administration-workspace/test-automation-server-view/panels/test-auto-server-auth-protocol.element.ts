import { Page } from '../../../page';
import { EditableSelectFieldElement } from '../../../../elements/forms/editable-select-field.element';

export class TestAutoServerAuthProtocolElement extends Page {
  protocolField: EditableSelectFieldElement;

  constructor() {
    super('sqtm-app-taserver-auth-protocol-panel');

    const urlPrefix = 'test-automation-server/*/';
    this.protocolField = new EditableSelectFieldElement(
      'authProtocol',
      urlPrefix + 'auth-protocol',
    );
  }
}
