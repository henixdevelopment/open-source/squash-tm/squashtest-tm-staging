import { Page } from '../../../page';
import { AutomatedExecutionEnvironmentVariablePanel } from '../../../../elements/environment-variables/automated-execution-environment-variable-panel';
import { EvInputType } from '../../../../../../../projects/sqtm-core/src/lib/model/environment-variable/ev-input-type.model';

export class TestAutoServerEnvironmentVariableElement extends Page {
  readonly environmentVariableSelectionPanel = new AutomatedExecutionEnvironmentVariablePanel();

  constructor() {
    super('sqtm-app-tas-environment-variables-panel');
  }

  assertNotExist() {
    cy.get(this.rootSelector).should('not.exist');
  }
}

export function getBoundEnvironmentVariables() {
  return {
    boundEnvironmentVariables: [
      {
        id: 1,
        name: 'Simple EV1',
        inputType: EvInputType.PLAIN_TEXT,
        boundToServer: true,
        value: 'toto',
        options: [],
      },
      {
        id: 2,
        name: 'Dropdown EV1',
        inputType: EvInputType.DROPDOWN_LIST,
        boundToServer: true,
        value: 'OPT1',
        options: [
          {
            label: 'OPT1',
            evId: 2,
            position: 0,
          },
          {
            label: 'OPT2',
            evId: 2,
            position: 1,
          },
        ],
      },
    ],
  };
}
