import { HttpMock, HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';
import { Page } from '../../page';
import { TestAutoServerAuthPolicyElement } from './panels/test-auto-server-auth-policy.element';
import { TestAutoServerAuthProtocolElement } from './panels/test-auto-server-auth-protocol.element';
import { TestAutoServerEnvironmentElement } from './panels/test-auto-server-environment-panel.element';
import { TestAutoServerInfoPanelElement } from './panels/test-auto-server-info-panel.element';
import { EnvironmentSelectionPanelDto } from '../../../elements/automated-execution-environments/environment-selection-panel.element';
import { TestAutoServerEnvironmentVariableElement } from './panels/test-auto-server-environment-variable-panel.element';
import { EnvironmentVariableOption } from '../../../../../../projects/sqtm-core/src/lib/model/environment-variable/environment-variable-option.model';
import { EvInputType } from '../../../../../../projects/sqtm-core/src/lib/model/environment-variable/ev-input-type.model';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';
import { TestAutoServerAdditionalConfigurationPanelElement } from './panels/test-auto-server-additional-configuration-panel.element';

export class TestAutomationServerViewPage extends Page {
  readonly entityNameField: EditableTextFieldElement;

  readonly informationPanel = new TestAutoServerInfoPanelElement();
  readonly authPolicyPanel = new TestAutoServerAuthPolicyElement();
  readonly authProtocolPanel = new TestAutoServerAuthProtocolElement();
  readonly environmentPanel = new TestAutoServerEnvironmentElement();
  readonly environmentVariablePanel = new TestAutoServerEnvironmentVariableElement();
  readonly additionalConfigurationPanel = new TestAutoServerAdditionalConfigurationPanelElement();

  readonly fetchAvailableEnvironmentsMock: HttpMock<any>;
  readonly fetchBoundEnvironmentVariableMock: HttpMock<any>;

  readonly anchors = AnchorsElement.withLinkIds(
    'environment-variables',
    'additional-configuration',
  );

  constructor(mockData?: EnvironmentSelectionPanelDto) {
    super('sqtm-app-test-automation-server-view');

    this.entityNameField = new EditableTextFieldElement(
      'entity-name',
      'test-automation-servers/*/name',
    );

    if (mockData != null) {
      this.fetchAvailableEnvironmentsMock = new HttpMockBuilder(
        'test-automation-servers/*/automated-execution-environments/all?*',
      )
        .responseBody(mockData)
        .build();
    }

    this.fetchBoundEnvironmentVariableMock = new HttpMockBuilder(
      'bound-environment-variables/test-automation-server/*',
    )
      .responseBody(this.getEnvironmentVariableMockData())
      .get()
      .build();
  }

  waitInitialDataFetch() {}

  getEnvironmentVariableMockData(): any {
    const option1: EnvironmentVariableOption = {
      evId: 2,
      label: 'option1',
      position: 0,
    };

    const option2: EnvironmentVariableOption = {
      evId: 2,
      label: 'option2',
      position: 1,
    };

    return {
      boundEnvironmentVariables: [
        {
          id: 1,
          name: 'environmentVariable1',
          inputType: EvInputType.PLAIN_TEXT,
          boundToServer: false,
          options: [],
          value: 'ev-value',
        },
        {
          id: 2,
          name: 'environmentVariable2',
          inputType: EvInputType.DROPDOWN_LIST,
          boundToServer: false,
          options: [option1, option2],
          value: '',
        },
      ],
    };
  }

  foldGrid() {
    cy.get(`[data-test-element-id="fold-tree-button"]`).click();
  }

  public showEnvironmentVariablePanel() {
    this.anchors.clickLink('environment-variables');
  }

  public showAdditionalConfigurationPanel() {
    this.anchors.clickLink('additional-configuration');
  }
}
