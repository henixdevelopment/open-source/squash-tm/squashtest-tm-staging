import { Page } from '../../../page';
import { EditableTextFieldElement } from '../../../../elements/forms/editable-text-field.element';

export class ScmServerInformationPanelElement extends Page {
  urlField: EditableTextFieldElement;

  constructor() {
    super('sqtm-app-scm-server-information-panel');

    const urlPrefix = 'scm-servers/*/';
    this.urlField = new EditableTextFieldElement('scm-server-url', urlPrefix + 'url');
  }
}
