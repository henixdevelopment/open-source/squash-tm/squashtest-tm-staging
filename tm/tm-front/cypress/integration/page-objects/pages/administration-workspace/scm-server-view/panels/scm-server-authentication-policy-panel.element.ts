import { Page } from '../../../page';
import { TextFieldElement } from '../../../../elements/forms/TextFieldElement';
import { HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import Chainable = Cypress.Chainable;

export class ScmServerAuthenticationPolicyPanelElement extends Page {
  usernameField: TextFieldElement;
  passwordField: TextFieldElement;

  constructor() {
    super('sqtm-app-scm-server-authentication-policy-panel');

    this.usernameField = new TextFieldElement('username');
    this.passwordField = new TextFieldElement('password');
  }

  get sendButton(): Chainable {
    return this.findByElementId('save-credentials-button');
  }

  get statusMessage(): Chainable {
    return this.findByElementId('credentials-status-message');
  }

  assertSendButtonEnabled(): void {
    this.sendButton.should('not.be.disabled');
  }

  assertSendButtonDisabled(): void {
    this.sendButton.should('be.disabled');
  }

  sendCredentialsForm(): void {
    const mock = new HttpMockBuilder('scm-servers/*/credentials').post().build();
    this.sendButton.click();
    mock.wait();
  }

  assertSaveSuccessMessageVisible(): void {
    this.statusMessage.should('contain.text', 'Enregistré');
  }
}
