import { GridElement } from '../../../../elements/grid/grid.element';
import { HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import { AddScmRepositoryDialog } from '../dialogs/add-scm-repository.dialog';
import { selectByDataTestToolbarButtonId } from '../../../../../utils/basic-selectors';
import { BaseDialogElement } from '../../../../elements/dialog/base-dialog.element';

export class ScmServerRepositoriesPanelElement {
  public readonly grid: GridElement;

  constructor() {
    this.grid = GridElement.createGridElement('scm-server-repositories');
  }

  waitInitialDataFetch() {}

  recloneOne(scmServerName: string) {
    this.grid.findRowId('name', scmServerName).then((id) => {
      this.grid.getRow(id, 'rightViewport').cell('recloneScmRepository').iconRenderer().click();

      const dialog = new BaseDialogElement('reclone-repository');
      dialog.assertExists();
      dialog.assertHasMessage(
        `Le dépôt local sera supprimé puis recréé par clonage du dépôt distant. Confirmez-vous ?`,
      );

      const mock = new HttpMockBuilder('scm-repositories/**/recreate-local-repository')
        .post()
        .build();
      dialog.confirm();
      mock.wait();
      dialog.assertNotExist();
    });
  }

  deleteOne(scmServerName: string) {
    const checkIfRepositoryBoundToProject = new HttpMockBuilder(
      'scm-repositories/check-bound-to-project/*',
    )
      .get()
      .build();

    this.grid.findRowId('name', scmServerName).then((id) => {
      this.grid.getRow(id, 'rightViewport').cell('delete').iconRenderer().click();

      checkIfRepositoryBoundToProject.wait();

      const deleteMock = new HttpMockBuilder('scm-repositories/*').delete().build();

      this.clickConfirmDeleteButton();

      deleteMock.wait();
    });
  }

  deleteMultiple(scmServerNames: string[]) {
    this.grid.selectRowsWithMatchingCellContent('name', scmServerNames);

    const checkIfRepositoriesBoundToProject = new HttpMockBuilder(
      'scm-repositories/check-bound-to-project/*',
    )
      .get()
      .build();

    const deleteMock = new HttpMockBuilder('scm-repositories/*').delete().build();

    this.clickOnDeleteButton();
    checkIfRepositoriesBoundToProject.wait();
    this.clickConfirmDeleteButton();
    deleteMock.wait();
  }

  clickOnAddRepositoryButton(): AddScmRepositoryDialog {
    const dialog = new AddScmRepositoryDialog();

    cy.get('[data-test-button-id="add-repository"]').should('exist').click();
    dialog.assertExists();

    return dialog;
  }

  private clickOnDeleteButton() {
    cy.get(selectByDataTestToolbarButtonId('delete-repositories')).should('exist').click();
  }

  private clickConfirmDeleteButton() {
    cy.get('sqtm-core-confirm-delete-dialog')
      .find('[data-test-dialog-button-id="confirm"]')
      .click()
      // Then
      .get('sqtm-core-confirm-delete-dialog')
      .should('not.exist');
  }
}
