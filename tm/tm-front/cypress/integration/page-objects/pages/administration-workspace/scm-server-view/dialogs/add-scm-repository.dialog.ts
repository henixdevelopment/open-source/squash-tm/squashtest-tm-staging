import { HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import { TextFieldElement } from '../../../../elements/forms/TextFieldElement';
import { ScmRepository } from '../../../../../../../projects/sqtm-core/src/lib/model/scm-server/scm-server.model';
import { CheckBoxElement } from '../../../../elements/forms/check-box.element';
import { BaseDialogElement } from '../../../../elements/dialog/base-dialog.element';

export class AddScmRepositoryDialog extends BaseDialogElement {
  private readonly nameField: TextFieldElement;
  private readonly branchField: TextFieldElement;
  private readonly workingFolderPathField: TextFieldElement;
  private readonly cloneRepositoryCheckbox: CheckBoxElement;

  constructor() {
    super('add-scm-repository-dialog');

    this.nameField = new TextFieldElement('repositoryName');
    this.branchField = new TextFieldElement('branch');
    this.workingFolderPathField = new TextFieldElement('workingFolderPath');
    this.cloneRepositoryCheckbox = new CheckBoxElement(() =>
      this.findByFieldName('cloneRepository'),
    );
  }

  fillName(name: string) {
    this.nameField.fill(name);
  }

  fillBranchField(group: string) {
    this.branchField.fill(group);
  }

  fillWorkingFolderPathField(url: string) {
    this.workingFolderPathField.fill(url);
  }

  openCreateScmRepository(clone: boolean, name: string, branch: string, workingFolderPath: string) {
    if (clone) {
      this.cloneRepositoryCheckbox.click();
    }
    this.fillName(name);
    this.fillBranchField(branch);
    this.fillWorkingFolderPathField(workingFolderPath);
    this.clickOnAddButton();
  }

  confirm(updatedRepositories?: ScmRepository[]) {
    const mock = new HttpMockBuilder('scm-repositories/*/new')
      .responseBody({ repositories: updatedRepositories })
      .post()
      .build();

    this.clickOnConfirmButton();

    mock.wait();
  }
}
