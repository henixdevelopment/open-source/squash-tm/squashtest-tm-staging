import { Page } from '../../page';
import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';
import { ScmServerInformationPanelElement } from './panels/scm-server-information-panel.element';
import { ScmServerAuthenticationPolicyPanelElement } from './panels/scm-server-authentication-policy-panel.element';
import { ScmServerCommitPolicyPanelElement } from './panels/scm-server-commit-policy-panel.element';
import { ScmServerRepositoriesPanelElement } from './panels/scm-server-repositories-panel.element';

export class ScmServerViewPage extends Page {
  readonly entityNameField: EditableTextFieldElement;
  readonly informationPanel = new ScmServerInformationPanelElement();
  readonly authPolicyPanel = new ScmServerAuthenticationPolicyPanelElement();
  readonly commitPolicyPanel = new ScmServerCommitPolicyPanelElement();
  readonly repositoriesPanel = new ScmServerRepositoriesPanelElement();

  constructor() {
    super('sqtm-app-bug-tracker-view');

    this.entityNameField = new EditableTextFieldElement('entity-name', 'scm-servers/*/name');
  }

  waitInitialDataFetch() {}

  foldGrid() {
    cy.get(`[data-test-element-id="fold-tree-button"]`).click();
  }
}
