import { Page } from '../../../page';
import { TextFieldElement } from '../../../../elements/forms/TextFieldElement';
import { HttpMockBuilder } from '../../../../../utils/mocks/request-mock';
import Chainable = Cypress.Chainable;

export class BugTrackerAuthenticationPolicyPanelElement extends Page {
  usernameField: TextFieldElement;
  passwordField: TextFieldElement;

  constructor() {
    super('sqtm-app-bug-tracker-authentication-policy-panel');

    this.usernameField = new TextFieldElement('username');
    this.passwordField = new TextFieldElement('password');
  }

  get sendButton(): Chainable {
    return this.findByElementId('save-credentials-button');
  }

  get statusMessage(): Chainable {
    return this.findByElementId('credentials-status-message');
  }

  assertSendButtonEnabled(): void {
    this.sendButton.should('not.be.disabled');
  }

  assertSendButtonDisabled(): void {
    this.sendButton.should('be.disabled');
  }

  sendCredentialsForm(): void {
    const mock = new HttpMockBuilder('bugtracker/*/credentials').post().build();
    this.sendButton.click();
    mock.wait();
  }

  assertSaveSuccessMessageVisible(): void {
    this.statusMessage.should('contain.text', 'Enregistré');
  }

  sendCredentialsFormWithServerError(errorMessage: string): void {
    const response = {
      squashTMError: {
        kind: 'ACTION_ERROR',
        actionValidationError: {
          i18nKey: errorMessage,
        },
      },
    };

    const mock = new HttpMockBuilder('bugtracker/*/credentials')
      .post()
      .status(412)
      .responseBody(response)
      .build();

    this.sendButton.click();
    mock.wait();
  }

  assertServerErrorMessageVisible(errorMessage: string): void {
    this.statusMessage.should('contain.text', errorMessage);
  }

  get getTokenOauth2Button(): Chainable {
    return this.findByElementId('save-oauth2-credentials-button');
  }

  assertGetTokenOauth2ButtonEnabled(): void {
    this.getTokenOauth2Button.should('not.be.disabled');
  }

  assertGetTokenOauth2ButtonDisabled(): void {
    this.getTokenOauth2Button.should('be.disabled');
  }

  assertGetTokenOauth2ButtonVisible(): void {
    this.getTokenOauth2Button.should('be.visible');
  }

  get revokeOauth2Button(): Chainable {
    return this.findByElementId('revoke-credentials-button');
  }

  assertRevokeOauth2ButtonVisible(): void {
    this.revokeOauth2Button.should('be.visible');
  }

  assertRevokeOauth2ButtonDisabled(): void {
    this.revokeOauth2Button.should('be.disabled');
  }

  assertRevokeOauth2ButtonEnabled(): void {
    this.revokeOauth2Button.should('not.be.disabled');
  }
}
