import { Page } from '../../../page';
import { EditableSelectFieldElement } from '../../../../elements/forms/editable-select-field.element';
import { EditableTextFieldElement } from '../../../../elements/forms/editable-text-field.element';
import { EditableRichTextFieldElement } from '../../../../elements/forms/editable-rich-text-field.element';

export class BugTrackerInformationPanelElement extends Page {
  kindField: EditableSelectFieldElement;
  urlField: EditableTextFieldElement;
  descriptionField: EditableRichTextFieldElement;

  constructor() {
    super('sqtm-app-bug-tracker-information-panel');

    const urlPrefix = 'bugtracker/*/';
    this.kindField = new EditableSelectFieldElement('kind', urlPrefix + 'kind');
    this.urlField = new EditableTextFieldElement('url', urlPrefix + 'url');
    this.descriptionField = new EditableRichTextFieldElement('bugtracker-description');
  }
}
