import { Page } from '../../../page';
import { EditableSelectFieldElement } from '../../../../elements/forms/editable-select-field.element';
import { TextFieldElement } from '../../../../elements/forms/TextFieldElement';
import { HttpMockBuilder } from '../../../../../utils/mocks/request-mock';

export class BugTrackerAuthenticationProtocolPanelElement extends Page {
  protocolField: EditableSelectFieldElement;
  consumerKeyField: TextFieldElement;
  clientIdField: TextFieldElement;
  oauth2SecretField: TextFieldElement;

  private oauth2FormSelector = '[data-test-element-id="oauth2-form"]';

  constructor() {
    super('sqtm-app-bug-tracker-authentication-protocol-panel');

    const urlPrefix = 'bugtracker/*/';
    this.protocolField = new EditableSelectFieldElement(
      'authProtocol',
      urlPrefix + 'auth-protocol',
    );
    this.consumerKeyField = new TextFieldElement('consumerKey');
    this.clientIdField = new TextFieldElement('clientId');
    this.oauth2SecretField = new TextFieldElement('clientSecret');
  }

  /*OAuth 2*/
  assertOAuth2FormVisible(): void {
    cy.get(this.oauth2FormSelector).should('be.visible');
  }

  assertOAuth2FormNotVisible(): void {
    cy.get(this.oauth2FormSelector).should('not.exist');
  }

  get saveOauth2ButtonSelector(): string {
    return this.rootSelector + ' [data-test-element-id="save-oauth2-button"]';
  }

  assertSaveOauth2ButtonEnabled(): void {
    cy.get(this.saveOauth2ButtonSelector).should('not.be.disabled');
  }

  assertSaveOauth2ButtonDisabled(): void {
    cy.get(this.saveOauth2ButtonSelector).should('be.disabled');
  }
  sendOAuth2Form(): void {
    const mock = new HttpMockBuilder('bugtracker/*/auth-protocol/configuration').post().build();
    cy.get(this.saveOauth2ButtonSelector).click();
    mock.wait();
  }

  /* Common*/
  get statusMessageSelector(): string {
    return this.rootSelector + ' [data-test-element-id="status-message"]';
  }
  assertStatusMessageNotVisible(): void {
    cy.get(this.statusMessageSelector).should('not.be.visible');
  }

  assertUnsavedChangesMessageVisible(): void {
    cy.get(this.statusMessageSelector).should('have.text', 'Changements non enregistrés.');
  }

  assertSaveSuccessMessageVisible(): void {
    cy.get(this.statusMessageSelector).should('have.text', 'Enregistré !');
  }

  assertServerErrorMessageVisible(): void {
    cy.get(this.statusMessageSelector).should(
      'have.text',
      'Une erreur est survenue lors du traitement.',
    );
  }
}
