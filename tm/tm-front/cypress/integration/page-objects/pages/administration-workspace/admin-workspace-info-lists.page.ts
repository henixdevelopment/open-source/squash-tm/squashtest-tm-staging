import { NavBarElement } from '../../elements/nav-bar/nav-bar.element';
import { GridElement } from '../../elements/grid/grid.element';
import { AdministrationWorkspacePage } from './administration-workspace.page';
import { PageFactory } from '../page';
import { AdminReferentialDataProviderBuilder } from '../../../utils/referential/admin-referential-data.provider';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { InfoListViewPage } from './info-list-view/info-list-view.page';
import { CreateInfoListDialog } from './dialogs/create-info-list-dialog';
import { AdminInfoList } from '../../../../../projects/sqtm-core/src/lib/model/infolist/adminInfoList.model';
import { ReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { DataRowModel } from '../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';

export class AdminWorkspaceInfoListsPage extends AdministrationWorkspacePage {
  public readonly navBar = new NavBarElement();

  constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-main-custom-workspace');
  }

  public static initTestAtPageInfoLists: PageFactory<AdminWorkspaceInfoListsPage> = (
    initialNodes: GridResponse = { dataRows: [] },
    referentialData?: ReferentialDataModel,
  ) => {
    return AdminWorkspaceInfoListsPage.initTestAtPage(
      initialNodes,
      'infoLists',
      'info-lists',
      'entities-customization/info-lists',
      referentialData,
    );
  };

  public static initTestAtPage: PageFactory<AdminWorkspaceInfoListsPage> = (
    initialNodes: GridResponse = { dataRows: [] },
    gridId: string,
    gridUrl: string,
    pageUrl: string,
    referentialData?: ReferentialDataModel,
  ) => {
    const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder(
      referentialData,
    ).build();
    const gridElement = GridElement.createGridElement(gridId, gridUrl, initialNodes);
    // visit page
    cy.visit(`administration-workspace/${pageUrl}`);
    // wait for ref data request to fire
    adminReferentialDataProvider.wait();
    // wait for initial tree data request to fire
    gridElement.waitInitialDataFetch();
    return new AdminWorkspaceInfoListsPage(gridElement);
  };

  openCreateInfoList(): CreateInfoListDialog {
    this.clickCreateButton();
    return new CreateInfoListDialog();
  }

  protected getPageUrl(): string {
    return 'info-lists';
  }

  protected getDeleteUrl(): string {
    return 'info-lists';
  }

  selectInfoListsByLabel(labels: string[], viewResponse?: AdminInfoList): InfoListViewPage {
    const view = new InfoListViewPage();
    const mock = new HttpMockBuilder('info-list-view/*').responseBody(viewResponse).build();

    this.selectRowsWithMatchingCellContent('label', labels);

    mock.waitResponseBody().then(() => {
      view.waitInitialDataFetch();
    });

    return view;
  }

  deleteInfoList(rowId: number, gridResponse: { dataRows: DataRowModel[]; count: number }) {
    const mock = new HttpMockBuilder('info-lists/**').delete().build();
    const mockGrid = new HttpMockBuilder('info-lists').post().responseBody(gridResponse).build();

    this.grid.getRow(rowId, 'rightViewport').cell('delete').iconRenderer().click();
    this.confirmDelete();
    mock.wait();
    mockGrid.wait();
  }
}
