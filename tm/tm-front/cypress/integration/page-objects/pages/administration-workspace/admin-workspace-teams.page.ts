import { NavBarElement } from '../../elements/nav-bar/nav-bar.element';
import { GridElement } from '../../elements/grid/grid.element';
import { AdministrationWorkspacePage } from './administration-workspace.page';
import { PageFactory } from '../page';
import { CreateTeamDialog } from './dialogs/create-team-dialog.element';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { TeamViewPage } from './team-view/team-view.page';
import { AdminReferentialDataProviderBuilder } from '../../../utils/referential/admin-referential-data.provider';
import { Team } from '../../../../../projects/sqtm-core/src/lib/model/team/team.model';
import { ReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { selectByDataTestToolbarButtonId } from '../../../utils/basic-selectors';
import { RemoveTeamDialogElement } from './dialogs/remove-team-dialog.element';

export class AdminWorkspaceTeamsPage extends AdministrationWorkspacePage {
  public readonly navBar = new NavBarElement();

  constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-main-user-workspace');
  }

  public static initTestAtPageTeams: PageFactory<AdminWorkspaceTeamsPage> = (
    initialNodes: GridResponse = { dataRows: [] },
    referentialData?: ReferentialDataModel,
  ) => {
    return AdminWorkspaceTeamsPage.initTestAtPage(
      initialNodes,
      'teams',
      'teams',
      'users/teams',
      referentialData,
    );
  };

  public static initTestAtPage: PageFactory<AdminWorkspaceTeamsPage> = (
    initialNodes: GridResponse = { dataRows: [] },
    gridId: string,
    gridUrl: string,
    pageUrl: string,
    referentialData?: ReferentialDataModel,
  ) => {
    const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder(
      referentialData,
    ).build();
    const gridElement = GridElement.createGridElement(gridId, gridUrl, initialNodes);

    // visit page
    cy.visit(`administration-workspace/${pageUrl}`);

    // wait for ref data request to fire
    adminReferentialDataProvider.wait();

    // wait for initial tree data request to fire
    const page = new AdminWorkspaceTeamsPage(gridElement);

    page.waitInitialDataFetch();

    return page;
  };

  openCreateTeam(): CreateTeamDialog {
    this.clickCreateButton();
    return new CreateTeamDialog();
  }

  selectTeamByName(name: string, viewResponse?: Team): TeamViewPage {
    const teamView = new TeamViewPage();
    const mock = new HttpMockBuilder('team-view/*').responseBody(viewResponse).build();

    this.selectRowWithMatchingCellContent('name', name);

    mock.waitResponseBody().then(() => {
      teamView.waitInitialDataFetch();
      teamView.assertExists();
    });

    return teamView;
  }

  protected getPageUrl(): string {
    return 'teams';
  }

  protected getDeleteUrl(): string {
    return 'teams';
  }

  openDeleteDialog(): RemoveTeamDialogElement {
    this.clickDeleteButton();
    return new RemoveTeamDialogElement();
  }

  private clickDeleteButton(): void {
    cy.get(selectByDataTestToolbarButtonId('delete-button')).click();
  }
}
