import { NavBarElement } from '../../elements/nav-bar/nav-bar.element';
import { GridElement } from '../../elements/grid/grid.element';
import { AdministrationWorkspacePage } from './administration-workspace.page';
import { PageFactory } from '../page';
import { CreateCustomFieldDialog } from './dialogs/create-custom-field-dialog';
import { AdminReferentialDataProviderBuilder } from '../../../utils/referential/admin-referential-data.provider';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { CustomFieldViewPage } from './custom-field-view/custom-field-view.page';
import { CustomField } from '../../../../../projects/sqtm-core/src/lib/model/customfield/customfield.model';
import { ReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { AnchorsElement } from '../../elements/anchor/anchors.element';

export class AdminWorkspaceCustomFieldsPage extends AdministrationWorkspacePage {
  readonly anchors = AnchorsElement.withLinkIds(
    'info-lists',
    'custom-fields',
    'requirements-links',
  );

  public readonly navBar = new NavBarElement();

  constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-main-custom-workspace');
  }

  public static initTestAtPageCustomFields: PageFactory<AdminWorkspaceCustomFieldsPage> = (
    initialNodes: GridResponse = { dataRows: [] },
    referentialData?: ReferentialDataModel,
  ) => {
    return AdminWorkspaceCustomFieldsPage.initTestAtPage(
      initialNodes,
      'customFields',
      'custom-fields',
      'entities-customization/custom-fields',
      referentialData,
    );
  };

  public static initTestAtPage: PageFactory<AdminWorkspaceCustomFieldsPage> = (
    initialNodes: GridResponse = { dataRows: [] },
    gridId: string,
    gridUrl: string,
    pageUrl: string,
    referentialData?: ReferentialDataModel,
  ) => {
    const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder(
      referentialData,
    ).build();
    const gridElement = GridElement.createGridElement(gridId, gridUrl, initialNodes);
    // visit page
    cy.visit(`administration-workspace/${pageUrl}`);
    // wait for ref data request to fire
    adminReferentialDataProvider.wait();
    // wait for initial tree data request to fire
    gridElement.waitInitialDataFetch();
    return new AdminWorkspaceCustomFieldsPage(gridElement);
  };

  protected getPageUrl(): string {
    return 'custom-fields';
  }

  openCreateCustomField(): CreateCustomFieldDialog {
    this.clickCreateButton();
    return new CreateCustomFieldDialog();
  }

  protected getDeleteUrl(): string {
    return 'custom-fields';
  }

  selectCustomFieldByName(name: string, viewResponse?: CustomField): CustomFieldViewPage {
    const view = new CustomFieldViewPage();
    const mock = new HttpMockBuilder('custom-field-view/*').responseBody(viewResponse).build();

    this.selectRowWithMatchingCellContent('name', name);

    mock.waitResponseBody().then(() => {
      view.waitInitialDataFetch();
      view.assertExists();
    });

    return view;
  }
}
