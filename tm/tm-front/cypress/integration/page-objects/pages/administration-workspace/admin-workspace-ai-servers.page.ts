import { AdminReferentialDataProviderBuilder } from '../../../utils/referential/admin-referential-data.provider';
import { GridElement } from '../../elements/grid/grid.element';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { NavBarElement } from '../../elements/nav-bar/nav-bar.element';
import { PageFactory } from '../page';
import { ReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { selectByDataTestToolbarButtonId } from '../../../utils/basic-selectors';
import { AdministrationWorkspacePage } from './administration-workspace.page';
import { CreateAiServerDialogElement } from './dialogs/create-ai-server-dialog.element';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { AdminAiServer } from '../../../../../projects/sqtm-core/src/lib/model/artificial-intelligence/ai-server.model';
import { AiServerViewPage } from './ai-server-view/ai-server-view.page';
import { AnchorsElement } from '../../elements/anchor/anchors.element';

export class AdminWorkspaceAiServersPage extends AdministrationWorkspacePage {
  public readonly navBar = new NavBarElement();
  readonly anchors = AnchorsElement.withLinkIds('ai-server');

  constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-main-server-workspace');
  }

  public static initTestAtPageTestAiServers: PageFactory<AdminWorkspaceAiServersPage> = (
    initialNodes: GridResponse = { dataRows: [] },
    referentialData?: ReferentialDataModel,
  ) => {
    return AdminWorkspaceAiServersPage.initTestAtPage(
      initialNodes,
      'ai-servers',
      'ai-servers',
      'servers/ai-server',
      referentialData,
    );
  };

  public static initTestAtPage: PageFactory<AdminWorkspaceAiServersPage> = (
    initialNodes: GridResponse = { dataRows: [] },
    gridId: string,
    gridUrl: string,
    pageUrl: string,
    description: string,
    referentialData?: ReferentialDataModel,
  ) => {
    const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder(
      referentialData,
    ).build();
    const gridElement = GridElement.createGridElement(gridId, gridUrl, initialNodes);

    cy.visit(`administration-workspace/${pageUrl}`);
    adminReferentialDataProvider.wait();
    gridElement.waitInitialDataFetch();

    return new AdminWorkspaceAiServersPage(gridElement);
  };

  openCreateAiServer(): CreateAiServerDialogElement {
    cy.get(`i${selectByDataTestToolbarButtonId('create-button')}`).click();

    return new CreateAiServerDialogElement();
  }

  protected getPageUrl(): string {
    return 'ai-server';
  }

  protected getDeleteUrl(): string {
    return 'ai-server';
  }

  selectAiServerByName(name: string, viewResponse?: AdminAiServer): AiServerViewPage {
    const view = new AiServerViewPage();
    const mock = new HttpMockBuilder('ai-server/*').responseBody(viewResponse).build();

    this.selectRowWithMatchingCellContent('name', name);
    mock.waitResponseBody().then(() => view.waitInitialDataFetch());

    return view;
  }
}
