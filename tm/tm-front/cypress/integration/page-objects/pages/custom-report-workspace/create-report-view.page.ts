import { Page } from '../page';
import {
  basicReferentialData,
  ReferentialDataProviderBuilder,
} from '../../../utils/referential/referential-data.provider';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { ReportWidget } from './report-widget/report-widget';
import { selectByDataTestElementId } from '../../../utils/basic-selectors';
import { TreeElement } from '../../elements/grid/grid.element';
import { CustomReportWorkspacePage } from './custom-report-workspace.page';
import { mockFieldValidationError } from '../../../data-mock/http-errors.data-mock';
import { TextFieldElement } from '../../elements/forms/TextFieldElement';
import { mockReportDefinitionViewModel } from '../../../data-mock/report-definition.data-mock';
import { RichTextFieldElement } from '../../elements/forms/RichTextFieldElement';
import {
  Report,
  ReportDefinitionViewModel,
  ReportInputType,
  ReportOptionGroup,
  ReportWorkbenchData,
  StandardReportCategory,
} from '../../../../../projects/sqtm-core/src/lib/model/custom-report/report-definition.model';
import { ReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { DataRowModel } from '../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';

export class CreateReportViewPage extends Page {
  public nameField = new TextFieldElement('name');
  public descriptionField = new RichTextFieldElement('description');

  constructor() {
    super('sqtm-app-create-report-view');
  }

  assertPreparationPhaseReportListExist() {
    this.findByElementId('preparation-phase-report-list').should('exist');
  }

  assertPreparationPhaseReportExist(reportId: string, expectedLabel: string) {
    this.assertReportExist('preparation', reportId, expectedLabel);
  }

  assertExecutionPhaseReportExist(reportId: string, expectedLabel: string) {
    this.assertReportExist('execution', reportId, expectedLabel);
  }

  assertVariousReportExist(reportId: string, expectedLabel: string) {
    this.assertReportExist('various', reportId, expectedLabel);
  }

  private assertReportExist(
    phase: 'preparation' | 'execution' | 'various',
    reportId: string,
    expectedLabel: string,
  ) {
    this.findByElementId(`${phase}-phase-report-list`)
      .find(selectByDataTestElementId(reportId))
      .should('exist')
      .should('contain.text', expectedLabel);
  }

  assertExecutionPhaseReportListExist() {
    this.findByElementId('execution-phase-report-list').should('exist');
  }

  selectReport(reportId: string) {
    this.findByElementId(reportId).click();
  }

  assertNoReportIsSelected() {
    this.findByElementId('selected-report-title').should('not.exist');
  }

  assertReportIsSelected(reportId: string) {
    this.getSelectedReportPanel(reportId).should('be.visible');
  }

  private getSelectedReportPanel(reportId: string) {
    return this.findByElementId(reportId);
  }

  assertReportHasTitle(expectedTitle: string) {
    this.findByElementId('selected-report-title').should('contain.text', expectedTitle);
  }

  findCriteriaWidget(fieldId: string, fieldType: ReportInputType) {
    return ReportWidget.create(fieldId, fieldType);
  }

  clickOnDownload() {
    this.findByElementId('download-report').click();
  }

  toggleReportSelector() {
    this.findByElementId('toggle-report-selector').click();
  }

  public static initTestAtPage(
    referentialData: ReferentialDataModel = basicReferentialData,
    reportWorkbenchData: ReportWorkbenchData = basicWorkbenchData,
    nodeReference: string = '*',
  ): CreateReportViewPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const mock = new HttpMockBuilder(`report-workbench/${nodeReference}`)
      .responseBody(reportWorkbenchData)
      .build();
    // visit page
    cy.visit(`custom-report-workspace/create-report-definition/${nodeReference}`);
    // wait for ref data request to fire
    referentialDataProvider.wait();
    mock.wait();
    return new CreateReportViewPage();
  }

  simulateDuplicateNodeFailure(containerId = '*'): void {
    const createReportMock = new HttpMockBuilder(`report-workbench/new/${containerId}`)
      .post()
      .status(412)
      .responseBody(mockFieldValidationError('name', 'sqtm-core.error.generic.duplicate-name'))
      .build();
    this.clickOnSaveButton();
    createReportMock.wait();
  }

  saveReport(
    containerId = '*',
    createdReportId?: number,
    initialNodes: DataRowModel[] = [],
    reportDefModel?: Partial<ReportDefinitionViewModel>,
    referentialData?: ReferentialDataModel,
  ): CustomReportWorkspacePage {
    const createReportMock = new HttpMockBuilder(`report-workbench/new/${containerId}`)
      .post()
      .responseBody({ id: createdReportId })
      .build();
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const tree = TreeElement.createTreeElement(
      'custom-report-workspace-main-tree',
      'custom-report-tree',
      { dataRows: initialNodes },
    );
    const reportDef = new HttpMockBuilder(
      `report-definition-view/${createdReportId ? createdReportId : '*'}?**`,
    )
      .responseBody(mockReportDefinitionViewModel(reportDefModel))
      .build();
    this.clickOnSaveButton();
    createReportMock.wait();
    referentialDataProvider.wait();
    tree.waitInitialDataFetch();
    reportDef.wait();
    return new CustomReportWorkspacePage(tree, 'sqtm-app-custom-report-workspace');
  }

  private clickOnSaveButton() {
    this.findByElementId('save-report').click();
  }

  assertDuplicateNodeNameErrorIsVisible() {
    new TextFieldElement('name').assertErrorExist('sqtm-core.error.generic.duplicate-name');
  }
}

const defaultProjectPickerReportOption = {
  name: null,
  label: 'Sélectionner le périmètre projets',
  value: 'PROJECT_PICKER',
  defaultSelected: true,
  givesAccessTo: 'projectIds',
  contentType: 'PROJECT_PICKER',
  disabledBy: null,
  composite: true,
  helpMessage: null,
};

export const requirementEditableReport: Report = {
  label: 'Cahier des exigences (format éditable)',
  id: 'report.books.requirements.requirements.report.label',
  description: "Permet de générer un rapport contenant une sélection de fiches d'exigences.",
  category: StandardReportCategory.PREPARATION_PHASE,
  views: [],
  isDocxTemplate: false,
  isDirectDownloadableReport: false,
  inputs: [
    {
      name: 'requirementsSelectionMode',
      type: ReportInputType.RADIO_BUTTONS_GROUP,
      label: 'Périmètre du rapport',
      required: true,
      options: [
        {
          name: null,
          label: 'Sélectionner le périmètre projets',
          value: 'PROJECT_PICKER',
          defaultSelected: true,
          givesAccessTo: 'projectIds',
          contentType: 'PROJECT_PICKER',
          disabledBy: null,
          composite: true,
          helpMessage: null,
        },
        {
          name: null,
          label: "Sélectionner un ou plusieurs nœuds dans l'arbre des exigences",
          value: 'TREE_PICKER',
          defaultSelected: false,
          givesAccessTo: 'requirementsIds',
          contentType: 'TREE_PICKER',
          disabledBy: null,
          nodeType: 'REQUIREMENT',
          composite: true,
          helpMessage: null,
        },
        {
          name: null,
          label: 'Sélectionner un jalon',
          value: 'MILESTONE_PICKER',
          defaultSelected: false,
          givesAccessTo: 'milestones',
          contentType: 'MILESTONE_PICKER',
          disabledBy: null,
          composite: true,
          helpMessage: null,
        },
        {
          name: null,
          label: 'Choisir des tags',
          value: 'TAG_PICKER',
          defaultSelected: false,
          givesAccessTo: 'tags',
          contentType: 'TAG_PICKER',
          disabledBy: null,
          bindableEntity: 'REQUIREMENT_VERSION',
          composite: true,
          helpMessage: null,
        },
      ],
    },
    {
      name: 'reportOptions',
      type: ReportInputType.CHECKBOXES_GROUP,
      label: "Option d'impression",
      required: false,
      options: [defaultProjectPickerReportOption],
    },
  ] as ReportOptionGroup[],
};

export const anotherRequirementEditableReport: Report = {
  ...requirementEditableReport,
  id: 'another.report.books.requirements.requirements.report.label',
  label: 'Un autre rapport',
};

export const executionAdvanceReport: Report = {
  label: "Avancement de l'exécution",
  id: 'report.executionprogressfollowup.name',
  description: "Permet de générer un tableau de bord de suivi de l'exécution des tests.",
  category: StandardReportCategory.EXECUTION_PHASE,
  isDirectDownloadableReport: false,
  isDocxTemplate: false,
  views: [],
  inputs: [
    {
      name: 'campaignSelectionMode',
      type: ReportInputType.RADIO_BUTTONS_GROUP,
      label: 'Périmètre du rapport',
      required: false,
    },
  ],
};

export const variousEditableReport = {
  ...requirementEditableReport,
  label: 'Rapport divers',
  id: 'various.report.books.requirements.requirements.report.label',
  description: "Permet de générer un rapport divers contenant une sélection de fiches d'exigences.",
  category: StandardReportCategory.VARIOUS,
};

const basicWorkbenchData: ReportWorkbenchData = {
  availableReports: [
    requirementEditableReport,
    anotherRequirementEditableReport,
    executionAdvanceReport,
    variousEditableReport,
  ],
  projectId: 1,
  reportDefinition: null,
};
