import { EntityViewPage } from '../page';
import { apiBaseUrl } from '../../../utils/mocks/request-mock';
import { selectByDataTestElementId } from '../../../utils/basic-selectors';
import { AnchorsElement } from '../../elements/anchor/anchors.element';
import { ToolbarButtonElement } from '../../elements/workspace-common/toolbar.element';
import { ModifyReportDefinitionViewPage } from './modify-report-definition-view-page';
import { EditableTextFieldElement } from '../../elements/forms/editable-text-field.element';

export class ReportInformationPanelElement {
  constructor(private customReportLibraryNodeId: number | '*') {}

  assertSummaryContains(expectedSummary: string) {
    cy.get(selectByDataTestElementId('report-summary')).should('contain.text', expectedSummary);
  }

  assertAttributesContains(attributeName: string, attributeValue: string) {
    cy.get(selectByDataTestElementId('report-attributes'))
      .find(`[data-test-attribute-name="${attributeName}"]`)
      .should('contain.text', attributeName);
    cy.get(selectByDataTestElementId('report-attributes'))
      .find(`[data-test-attribute-value="${attributeName}"]`)
      .should('contain.text', attributeValue);
  }

  assertDescriptionContains(expectedDescription: string) {
    cy.get(selectByDataTestElementId('report-description')).should(
      'contain.text',
      expectedDescription,
    );
  }

  assertAttributeExistsWithIndex(index: number) {
    cy.get(selectByDataTestElementId('report-attributes'))
      .children()
      .eq(index)
      .should('have.length', 1);
  }
}

export class ReportDefinitionViewPage extends EntityViewPage {
  readonly anchors = AnchorsElement.withLinkIds('information');
  readonly nameTextField: EditableTextFieldElement = new EditableTextFieldElement('entity-name');

  constructor(private customReportLibraryNodeId: number | '*') {
    super('sqtm-app-report-definition-view');
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/report-definition-view/${this.customReportLibraryNodeId}?**`;
    cy.wait(`@${url}`);
  }

  showInformationPanel(): ReportInformationPanelElement {
    this.anchors.clickLink('information');
    return new ReportInformationPanelElement(this.customReportLibraryNodeId);
  }

  clickModifyButtonInReportDefinitionView() {
    const modifyButton = new ToolbarButtonElement(this.rootSelector, 'modify');
    modifyButton.clickWithoutSpan();
    return new ModifyReportDefinitionViewPage();
  }
}
