import { Page } from '../page';
import { TextFieldElement } from '../../elements/forms/TextFieldElement';
import { RichTextFieldElement } from '../../elements/forms/RichTextFieldElement';
import {
  selectByDataTestElementId,
  selectByDataTestDialogButtonId,
} from '../../../utils/basic-selectors';
import { TreeElement } from '../../elements/grid/grid.element';

export class ModifyReportDefinitionViewPage extends Page {
  public readonly textFieldNameElement = new TextFieldElement('name');
  public readonly richTextField: RichTextFieldElement = new RichTextFieldElement('description');
  constructor() {
    super('sqtm-app-modify-report-view');
  }

  clickActionButton(value: 'download-report' | 'save-report' | 'cancel') {
    this.find(selectByDataTestElementId(value)).click();
  }

  clickTestCaseTreePickerInForm() {
    cy.get('sqtm-app-form-test-case-tree-picker').find('a').contains('Choisir').click();
  }

  private clickOnConfirmButtonInTestCaseTreeDialogPicker() {
    cy.get('sqtm-core-test-case-tree-dialog-picker')
      .find(selectByDataTestDialogButtonId('confirm'))
      .click();
  }

  selectNodesInTestCaseTree(nodeNames: string[]) {
    const tree = TreeElement.createTreeElement('test-case-tree-picker', 'test-case-tree');
    tree.selectMultipleNodesByName(nodeNames);
    this.clickOnConfirmButtonInTestCaseTreeDialogPicker();
  }
}
