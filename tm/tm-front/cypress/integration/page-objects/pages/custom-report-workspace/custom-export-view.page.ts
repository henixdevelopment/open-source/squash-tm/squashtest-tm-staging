import { EntityViewPage } from '../page';
import { ToolbarButtonElement } from '../../elements/workspace-common/toolbar.element';
import { ModifyCustomExportViewPage } from './modify-custom-export-view.page';
import { selectByDataTestElementId, selectByDataTestFieldId } from '../../../utils/basic-selectors';
import { EditableTextFieldElement } from '../../elements/forms/editable-text-field.element';

export class CustomExportViewPage extends EntityViewPage {
  public modifyToolBarButton = new ToolbarButtonElement(this.rootSelector, 'modify');
  public readonly nameTextField: EditableTextFieldElement = new EditableTextFieldElement(
    'entity-name',
  );
  constructor(private customReportLibraryNodeId: number | '*') {
    super('sqtm-app-custom-export-view');
  }
  goToModifyCustomExportViewPage() {
    this.modifyToolBarButton.clickWithoutSpan();
    return new ModifyCustomExportViewPage();
  }

  assertPerimeterFieldContains(perimeterName: string) {
    cy.get(this.rootSelector)
      .find(selectByDataTestFieldId('custom-export-perimeter'))
      .contains(perimeterName);
  }

  assertColumnFieldContains(columnLabel: string, index: number) {
    cy.get(this.rootSelector)
      .find(selectByDataTestElementId('columnField'))
      .eq(index)
      .contains(columnLabel);
  }
}
