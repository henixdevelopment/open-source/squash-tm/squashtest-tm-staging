import { TreeElement } from '../../elements/grid/grid.element';
import { ReferentialDataProviderBuilder } from '../../../utils/referential/referential-data.provider';
import { NavBarElement } from '../../elements/nav-bar/nav-bar.element';
import { WorkspaceWithTreePage } from '../workspace-with-tree.page';
import { CustomReportWorkspaceTreeMenu } from './custom-report-workspace-tree-menu';
import { ReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { PageFactory } from '../page';

export class CustomReportWorkspacePage extends WorkspaceWithTreePage {
  public readonly navBar: NavBarElement = new NavBarElement();
  public readonly treeMenu: CustomReportWorkspaceTreeMenu;

  public constructor(
    public readonly tree: TreeElement,
    rootSelector: string,
  ) {
    super(tree, rootSelector);
    this.treeMenu = new CustomReportWorkspaceTreeMenu();
  }

  public static initTestAtPage: PageFactory<CustomReportWorkspacePage> = (
    initialNodes: GridResponse = { dataRows: [] },
    referentialData?: ReferentialDataModel,
  ) => {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const tree = TreeElement.createTreeElement(
      'custom-report-workspace-main-tree',
      'custom-report-tree',
      initialNodes,
    );
    // visit page
    cy.visit('custom-report-workspace');
    // wait for ref data request to fire
    referentialDataProvider.wait();
    // wait for initial tree data request to fire
    tree.waitInitialDataFetch();
    return new CustomReportWorkspacePage(tree, 'sqtm-app-custom-report-workspace');
  };
}
