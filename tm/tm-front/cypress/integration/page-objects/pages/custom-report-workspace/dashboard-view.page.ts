import { EntityViewPage } from '../page';
import { apiBaseUrl, HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { ToolbarElement } from '../../elements/workspace-common/toolbar.element';
import {
  ChartBinding,
  ReportBinding,
} from '../../../../../projects/sqtm-core/src/lib/model/custom-report/custom-dashboard.model';
import { AnchorsElement } from '../../elements/anchor/anchors.element';
import { EditableTextFieldElement } from '../../elements/forms/editable-text-field.element';
import { selectByDataTestElementId } from '../../../utils/basic-selectors';

class ChartInformationPanelElement {
  constructor() {}
}

export class DashboardViewPage extends EntityViewPage {
  readonly anchors = AnchorsElement.withLinkIds('information', 'dashboard');
  public readonly nameTextField: EditableTextFieldElement = new EditableTextFieldElement(
    'entity-name',
  );

  constructor(private customReportLibraryNodeId: number | '*') {
    super('sqtm-app-dashboard-view');
  }

  private getChartByType(type) {
    return this.find(`sqtm-app-custom-report-${type}-chart`);
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/dashboard-view/${this.customReportLibraryNodeId}?**`;
    cy.wait(`@${url}`);
  }

  showInformationPanel(): ChartInformationPanelElement {
    this.anchors.clickLink('information');
    return new ChartInformationPanelElement();
  }

  showFavoriteList(): ToolbarElement {
    return new ToolbarElement('favorite-icon');
  }

  assertBindingIsRendered(bindingId: number) {
    cy.get(this.rootSelector).find(this.getBindingSelector(bindingId)).should('exist');
  }

  assertBindingIsNotRendered(bindingId: number) {
    cy.get(this.rootSelector).find(this.getBindingSelector(bindingId)).should('not.exist');
  }

  private getBindingSelector(bindingId: number | string) {
    return selectByDataTestElementId(`custom-dashboard-binding-${bindingId}`);
  }

  assertChartBindingIsRendered(bindingId: number) {
    this.find(this.getBindingSelector(bindingId)).find('.js-plotly-plot .main-svg').should('exist');
  }

  assertReportBindingIsRendered(bidingId: number, reportDefinitionId: number) {
    cy.get(this.rootSelector)
      .find(this.getBindingSelector(bidingId))
      .find(selectByDataTestElementId(`report-binding-${reportDefinitionId}`))
      .should('exist');
  }

  assertEmptyDropzoneIsVisible() {
    this.findByElementId('empty-dashboard-dropzone').should('be.visible');
  }

  assertEmptyDropzoneIsNotVisible() {
    this.findByElementId('empty-dashboard-dropzone').should('not.exist');
  }

  assertDropzoneIsVisible() {
    this.findByElementId('dashboard-dropzone').should('be.visible');
  }

  dropChartIntoEmptyZone(dashboardId = '*', createdBiding?: ChartBinding) {
    const httpMock = new HttpMockBuilder(`dashboard/${dashboardId}/chart-binding`)
      .post()
      .responseBody(createdBiding)
      .build();
    this.findByElementId('empty-dashboard-dropzone').trigger('mouseup', { force: true });
    httpMock.wait();
  }

  dropChart(dashboardId = '*', createdBinding?: ChartBinding) {
    const httpMock = new HttpMockBuilder(`dashboard/${dashboardId}/chart-binding`)
      .post()
      .responseBody(createdBinding)
      .build();
    this.findByElementId('dashboard-dropzone').trigger('mouseup', { force: true });
    httpMock.wait();
  }

  dropReportIntoEmptyZone(dashboardId = '*', createdBinding?: ReportBinding) {
    const httpMock = new HttpMockBuilder(`dashboard/${dashboardId}/report-binding`)
      .post()
      .responseBody(createdBinding)
      .build();
    this.findByElementId('empty-dashboard-dropzone').trigger('mouseup', { force: true });
    httpMock.wait();
  }

  dropReport(dashboardId = '*', createdBinding?: ReportBinding) {
    const httpMock = new HttpMockBuilder(`dashboard/${dashboardId}/report-binding`)
      .post()
      .responseBody(createdBinding)
      .build();
    this.findByElementId('dashboard-dropzone').trigger('mouseup', { force: true });
    httpMock.wait();
  }

  removeChart(bindingId: number) {
    const httpMock = new HttpMockBuilder(`dashboard/chart-binding/${bindingId ? bindingId : '*'}`)
      .delete()
      .build();
    cy.get(this.rootSelector)
      .find(this.getBindingSelector(bindingId))
      .find(selectByDataTestElementId('custom-dashboard-remove-binding'))
      .click({ force: true });
    httpMock.wait();
  }

  removeReport(bindingId: number) {
    const httpMock = new HttpMockBuilder(`dashboard/report-binding/${bindingId ? bindingId : '*'}`)
      .delete()
      .build();
    cy.get(this.rootSelector)
      .find(this.getBindingSelector(bindingId))
      .find(selectByDataTestElementId('custom-dashboard-remove-binding'))
      .click({ force: true });
    httpMock.wait();
  }

  dropForSwappingChart(bindingId = '*', newChartNodeId = '*', createdBinding?: ChartBinding) {
    const httpMock = new HttpMockBuilder(
      `dashboard/chart-binding/${bindingId}/swap/${newChartNodeId}`,
    )
      .post()
      .responseBody(createdBinding)
      .build();

    cy.get(this.rootSelector)
      .find(this.getBindingSelector(bindingId))
      .find('div')
      .first()
      .trigger('mouseup', { force: true });

    httpMock.wait();
  }

  removeReportIfDashboardHasMultipleElements(bindingId: number) {
    const httpMock = new HttpMockBuilder(`dashboard/report-binding/${bindingId ? bindingId : '*'}`)
      .delete()
      .build();
    cy.get(this.rootSelector)
      .find(this.getBindingSelector(bindingId))
      .first()
      .find(selectByDataTestElementId('custom-dashboard-remove-binding'))
      .click({ force: true });
    httpMock.wait();
  }

  clickToRemoveChart(bindingId: number) {
    cy.get(this.rootSelector)
      .find(this.getBindingSelector(bindingId))
      .find(selectByDataTestElementId('custom-dashboard-remove-binding'))
      .click({ force: true });
  }

  assertCustomReportChartTitle(type, contentText) {
    this.getChartByType(type).find('.gtitle>tspan').should('contain.text', contentText);
  }
}
