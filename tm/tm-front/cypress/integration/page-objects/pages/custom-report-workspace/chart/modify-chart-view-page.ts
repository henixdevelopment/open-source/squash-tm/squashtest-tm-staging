import {
  basicReferentialData,
  ReferentialDataProviderBuilder,
} from '../../../../utils/referential/referential-data.provider';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { AxisSelectorElement } from '../../../elements/chart-workbench/axis-selector.element';
import { SimpleSelectFieldElement } from '../../../elements/workspace-common/simple-select-field.element';
import { CustomReportWorkspacePage } from '../custom-report-workspace.page';
import { TreeElement } from '../../../elements/grid/grid.element';
import {
  ChartDefinitionModel,
  ChartWorkbenchData,
} from '../../../../../../projects/sqtm-core/src/lib/model/custom-report/chart-definition.model';
import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { DataRowModel } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { selectByDataIcon, selectByDataTestElementId } from '../../../../utils/basic-selectors';
import { AbstractBaseChartPage } from './abstract-base-chart.page';

export class ModifyChartViewPage extends AbstractBaseChartPage {
  measureSelector = new AxisSelectorElement('Measure');
  axisSelector = new AxisSelectorElement('Axis');
  seriesSelector = new AxisSelectorElement('Series');
  chartTypeSelector = new SimpleSelectFieldElement('chart-type-selector');
  filterSelector = new AxisSelectorElement('filter');

  constructor() {
    super('sqtm-app-modify-chart-view');
  }

  private getTitleUnderLine() {
    return this.find('.title__underline');
  }

  private getName() {
    return this.findByFieldName('name');
  }

  private getIcon(icon: string, index: number = 0) {
    return this.find(selectByDataIcon(icon)).eq(index);
  }

  public static initTestAtPage(
    chartDefinition: Partial<ChartDefinitionModel>,
    referentialData: ReferentialDataModel = basicReferentialData,
    chartWorkbenchData: ChartWorkbenchData,
    nodeReference: string = '*',
  ): ModifyChartViewPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const mock = new HttpMockBuilder(`chart-workbench/${nodeReference}`)
      .responseBody(chartWorkbenchData)
      .build();

    const chartDefinitionMock = new HttpMockBuilder(`chart-definition-view/${nodeReference}?**`)
      .responseBody(chartDefinition)
      .build();
    const chartPreviewMock = new HttpMockBuilder(
      `chart-workbench/preview/${chartDefinition.projectId}`,
    )
      .post()
      .responseBody(chartDefinition)
      .build();

    // visit page
    cy.visit(`custom-report-workspace/modify-chart-definition/${nodeReference}`);
    // wait for ref data request to fire
    referentialDataProvider.wait();
    mock.wait();
    chartDefinitionMock.wait();
    chartPreviewMock.wait();
    return new ModifyChartViewPage();
  }

  assertChartIsRendered() {
    cy.get('.js-plotly-plot .main-svg').should('exist');
    this.findByElementId('hint-message').should('not.exist');
  }

  saveChartModifications(
    containerId: string = '*',
    initialNodes?: DataRowModel[],
    model?: Partial<ChartDefinitionModel>,
    referentialData?: ReferentialDataModel,
  ): CustomReportWorkspacePage {
    const modifyChartMock = new HttpMockBuilder(`chart-workbench/update/${containerId}`)
      .post()
      .build();
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const tree = TreeElement.createTreeElement(
      'custom-report-workspace-main-tree',
      'custom-report-tree',
      { dataRows: initialNodes },
    );
    const chartDefMock = new HttpMockBuilder(
      `chart-definition-view/${model.customReportLibraryNodeId}?**`,
    )
      .responseBody(model)
      .build();
    this.findByElementId('modify-chart').click();
    modifyChartMock.wait();
    referentialDataProvider.wait();
    tree.waitInitialDataFetch();
    chartDefMock.wait();
    return new CustomReportWorkspacePage(tree, 'sqtm-app-custom-report-workspace');
  }

  clickFilterButton() {
    this.find('sqtm-app-filter-panel')
      .find(selectByDataTestElementId('filter'))
      .find('svg')
      .click();
  }
  clickOnModifyChartButton() {
    this.find(selectByDataTestElementId('modify-chart')).click();
  }

  assertTitleUnderlineContent() {
    this.getTitleUnderLine().find('span').should('contain.text', 'Création du graphique');
  }

  renameChart(newName: string) {
    this.getName().find('input').clear();
    this.getName()
      .find('input')
      .type(newName + '{enter}');
  }

  clickIcon(dataIcon: string, index?: number) {
    this.getIcon(dataIcon, index).click();
  }
}
