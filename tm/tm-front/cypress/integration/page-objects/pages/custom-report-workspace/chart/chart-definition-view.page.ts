import { apiBaseUrl } from '../../../../utils/mocks/request-mock';
import { AnchorsElement } from '../../../elements/anchor/anchors.element';
import { EditableTextFieldElement } from '../../../elements/forms/editable-text-field.element';
import { ToolbarButtonElement } from '../../../elements/workspace-common/toolbar.element';
import { ModifyChartViewPage } from './modify-chart-view-page';
import {
  selectByDataTestElementId,
  selectByDataTestFieldId,
} from '../../../../utils/basic-selectors';
import { AbstractBaseChartPage } from './abstract-base-chart.page';

class ChartInformationPanelElement {
  constructor() {}
}

export class ChartDefinitionViewPage extends AbstractBaseChartPage {
  readonly anchors = AnchorsElement.withLinkIds('information', 'chart');
  public readonly nameTextField: EditableTextFieldElement = new EditableTextFieldElement(
    'entity-name',
  );

  constructor(private customReportLibraryNodeId: number | '*') {
    super('sqtm-app-chart-definition-view');
  }

  private getCreatedChartDefinition() {
    return this.find(selectByDataTestFieldId('chart-definition-created'));
  }

  private getLastModifiedChartDefinition() {
    return this.find(selectByDataTestFieldId('chart-definition-lastModified'));
  }

  private getAxesSelectorFieldId(axeSelector: string) {
    return this.find(selectByDataTestFieldId(`${axeSelector}-information`));
  }

  private getAxesSelectorElementId(axeSelector: string) {
    return this.find(selectByDataTestElementId(`${axeSelector}-information`));
  }

  private getToolbarButton(buttonId: string) {
    return new ToolbarButtonElement(this.rootSelector, buttonId);
  }

  private getInfoFilter(id: number) {
    return this.find(selectByDataTestFieldId(`filter-information-${id}`));
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/chart-definition-view/${this.customReportLibraryNodeId}?**`;
    cy.wait(`@${url}`);
  }

  showInformationPanel(): ChartInformationPanelElement {
    this.anchors.clickLink('information');
    return new ChartInformationPanelElement();
  }

  foldTree() {
    cy.get(`[data-test-element-id="fold-tree-button"]`).click();
  }

  checkPerimeter(expectedPerimeter: string) {
    cy.get(`[data-test-field-id=perimeter-information]`).should('contain.text', expectedPerimeter);
  }

  checkAxisInfo(expectedAxis: string) {
    this.getAxesSelectorFieldId('axis').should('contain.text', expectedAxis);
  }

  checkInfoFilter(idFilter: number, expectedFilter: string) {
    this.getInfoFilter(idFilter).should('contain.text', expectedFilter);
  }

  checkMeasureInfo(expectedMeasure: string) {
    this.getAxesSelectorFieldId('measure').should('contain.text', expectedMeasure);
  }

  assertMeasureInfoNotExists() {
    this.getAxesSelectorFieldId('measure').should('not.exist');
  }

  checkSeriesInfo(expectedSeries: string) {
    this.getAxesSelectorFieldId('series').should('contain.text', expectedSeries);
  }

  assertSeriesInfoNotExists() {
    this.getAxesSelectorFieldId('series').should('not.exist');
  }

  checkFilterInfo(filterIndex: number, expectedFilter: string) {
    cy.get(`[data-test-field-id=filter-information-${filterIndex}]`).should(
      'contain.text',
      expectedFilter,
    );
  }

  assertFiltersInfoNotExists() {
    cy.get(`[data-test-field-id=filters-label]`).should('not.exist');
  }

  clickModifyButton() {
    this.getToolbarButton('modify').clickWithoutSpan();
    return new ModifyChartViewPage();
  }

  assertToolbarButtonExist(buttonId: string) {
    this.getToolbarButton(buttonId).assertExists();
  }

  triggerToolbarButtonTooltip(buttonId: string) {
    this.getToolbarButton(buttonId).triggerHover();
  }

  assertColorToolbarButton(buttonId: string, color: string) {
    this.getToolbarButton(buttonId).assertColor(color);
  }

  assertHasCreationDate() {
    const dateRegex = /\d{2}\/\d{2}\/\d{4} \d{2}:\d{2}/;
    this.getCreatedChartDefinition().contains('span', dateRegex);
  }

  assertCreatedBy(expectedCreation: string) {
    this.getCreatedChartDefinition().contains('span', expectedCreation);
  }

  assertLastModification(lastModifiedBy: string) {
    this.getLastModifiedChartDefinition().should('contain.text', lastModifiedBy);
  }

  assertIconDisplayedInAxesField(axisSelector: string, icon: string) {
    this.getAxesSelectorElementId(axisSelector).find('i').should('have.class', `anticon-${icon}`);
  }

  assertLabelsDisplayedInAxesField(axisSelector: string, label: string) {
    this.getAxesSelectorElementId(axisSelector).find('span').should('contain.text', label);
  }

  assertColorsDisplayedInAxesField(axisSelector: string, expectedColor: string) {
    this.getAxesSelectorElementId(axisSelector)
      .find('div')
      .find('div')
      .first()
      .should('have.css', 'background-color', expectedColor);
  }
}
