import { EntityViewPage } from '../../page';
import { selectByDataTestElementId } from '../../../../utils/basic-selectors';
import { ExecutionStatusColor } from '../../../../utils/color.utils';

export abstract class AbstractBaseChartPage extends EntityViewPage {
  protected getLegends() {
    return this.find('.legendtext');
  }

  protected getChartType(type: string) {
    return this.find(`sqtm-app-custom-report-${type}-chart`);
  }

  protected getYTitleChart() {
    return this.find('.g-ytitle');
  }

  protected getYLabelChart() {
    return this.find('.ytick');
  }

  protected getXLabelChart() {
    return this.find('.xtick');
  }

  protected getX2LabelChart() {
    return this.find('.x2tick');
  }

  protected getXTitleChart() {
    return this.find('.g-xtitle');
  }

  protected getX2TitleChart() {
    return this.find('.g-x2title');
  }

  protected getTitleChart() {
    return this.find('.gtitle');
  }

  protected getChartPoint() {
    return this.find('.point');
  }

  protected getPointHover() {
    return this.find('.hovertext');
  }

  protected getRectBySubplotXY() {
    return this.find('rect[data-subplot="xy"]');
  }

  protected getRectBySubplotX2Y() {
    return this.find('rect[data-subplot="x2y"]');
  }

  private getScope() {
    return this.find(selectByDataTestElementId('scope'));
  }

  private getFilterField(index: number = 0) {
    return this.findByElementId('filterField').eq(index);
  }

  private getButtonByDataTestElementId(elementId: string) {
    return this.find(selectByDataTestElementId(elementId));
  }

  private getChartSlice() {
    return this.find('text.slicetext');
  }

  private getColorSlice() {
    return this.find('.surface');
  }

  assertChartLegends(index: number, contentText: string) {
    this.getLegends().eq(index).should('contain.text', contentText);
  }

  assertChartXAxisTitle(XTitle: string) {
    this.getXTitleChart().should('contain.text', XTitle);
  }

  assertChartX2AxisTitle(XTitle: string) {
    this.getX2TitleChart().should('contain.text', XTitle);
  }

  assertChartYAxisTitle(YTitle: string) {
    this.getYTitleChart().find('text').should('contain.text', YTitle);
  }

  assertChartTitle(title: string) {
    this.getTitleChart().find('tspan').should('contain.text', title);
  }

  assertChartYAxisLabel(index: number, ordinateText: string) {
    this.getYLabelChart().eq(index).find('text').should('contain.text', ordinateText);
  }

  assertChartXAxisLabel(index: number, abscissaText: string) {
    this.getXLabelChart().eq(index).find('text').should('contain.text', abscissaText);
  }

  assertChartX2AxisLabel(index: number, abscissaText: string) {
    this.getX2LabelChart().eq(index).find('text').should('contain.text', abscissaText);
  }

  findPointChart(index: number) {
    return this.getChartPoint().eq(index);
  }

  hoverOverXYSubplot(x: number, y: number) {
    this.getRectBySubplotXY().first().trigger('mousemove', x, y, { force: true });
  }

  hoverOverX2YSubplot(x: number, y: number) {
    this.getRectBySubplotX2Y().first().trigger('mousemove', x, y, { force: true });
  }

  hoverOverXYSubplotBottom() {
    this.getRectBySubplotXY().first().trigger('mousemove', 'bottom');
  }

  assertElementCountOnPointHover(pointSelector: string, value: string) {
    this.getPointHover().find(pointSelector).last().should('contain.text', value);
  }

  assertLegendOnPointHover(pointSelector: string, legend: string) {
    this.getPointHover().find(pointSelector).first().should('contain.text', legend);
  }

  assertChartBarColor(index: number, color: string) {
    this.findPointChart(index).find('path').should('have.css', 'fill', color);
  }

  assertChartTypeExist(type: string) {
    this.getChartType(type).should('exist');
  }

  assertChartTypeNotExist(type: string) {
    this.getChartType(type).should('not.exist');
  }

  assertChartPointColor(index: number, color: string) {
    this.findPointChart(index).should('have.css', 'fill', color);
  }

  assertCriterionChecked(indexScope: number) {
    this.getScope().children().eq(indexScope).should('have.class', 'ant-radio-wrapper-checked');
  }

  selectScopeCriterion(indexScope: number) {
    this.getScope().children().eq(indexScope).click();
  }

  assertBarValueInChart(index: number, value: string) {
    this.findPointChart(index).find('text').should('contain.text', value);
  }

  assertFilterAttribute(index: number, contentText: string, i?: number) {
    this.getFilterField(i).find('.txt-ellipsis').eq(index).should('contain.text', contentText);
  }

  clickFilterField() {
    this.getFilterField().click();
  }

  assertButtonExistById(elementId: string) {
    this.getButtonByDataTestElementId(elementId).should('exist');
  }

  clickButtonById(elementId: string) {
    this.getButtonByDataTestElementId(elementId).click();
  }

  assertChartSlicePercentage(index: number, percentage: string) {
    this.getChartSlice().eq(index).should('contain.text', percentage);
  }

  assertChartSliceFillColor(index: number, color: string | ExecutionStatusColor) {
    this.getColorSlice().eq(index).should('have.css', 'fill', color);
  }
}
