import { EntityViewPage } from '../page';

export class CustomReportLibraryViewPage extends EntityViewPage {
  constructor(private customReportLibraryNodeId: number | '*') {
    super('sqtm-app-custom-report-library-view');
  }
}
