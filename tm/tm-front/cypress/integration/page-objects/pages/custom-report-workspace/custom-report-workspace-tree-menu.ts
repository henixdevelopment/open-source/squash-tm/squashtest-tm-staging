import { TreeToolbarElement } from '../../elements/workspace-common/tree-toolbar.element';
import { CreateEntityDialog } from '../create-entity-dialog.element';
import { CreateChartViewPage } from './chart/create-chart-view.page';
import { CreateReportViewPage } from './create-report-view.page';
import { CreateExportViewPage } from './create-export-view.page';
import { DeleteTreeNodesDialogElement } from '../test-case-workspace/dialogs/delete-tree-nodes-dialog.element';

export enum CustomReportCreateMenuItemIds {
  NEW_FOLDER = 'new-folder',
  NEW_CHART = 'new-chart-definition',
  NEW_REPORT = 'new-report-definition',
  NEW_DASHBOARD = 'new-dashboard',
  NEW_CUSTOM_EXPORT = 'new-custom-export',
}

export class CustomReportWorkspaceTreeMenu extends TreeToolbarElement {
  constructor() {
    super('custom-report-toolbar');
  }

  // Reactivate when doing folders
  // assertCreateFolderButtonIsEnabled() {
  //   const menuItem = this.findCreateFolderButton();
  //   menuItem.assertExists();
  //   menuItem.assertEnabled();
  // }
  //
  // assertCreateFolderButtonIsDisabled() {
  //   const menuItem = this.findCreateFolderButton();
  //   menuItem.assertExists();
  //   menuItem.assertDisabled();
  // }

  // private findCreateFolderButton() {
  //   return this.findCreateMenuButton(CustomReportCreateMenuItemIds.NEW_FOLDER);
  // }

  private findCreateMenuButton(itemId: CustomReportCreateMenuItemIds) {
    const createButton = this.createButton();
    const menuElement = createButton.showMenu();
    return menuElement.item(itemId);
  }

  assertCreateChartButtonIsEnabled() {
    const menuButton = this.findCreateChartButton();
    menuButton.assertExists();
    menuButton.assertEnabled();
  }

  hideCreateMenu() {
    this.createButton().hideMenu();
  }

  assertCreateMenuIsDisabled() {
    this.createButton().assertExists();
    this.createButton().assertIsDisabled();
  }

  private findCreateChartButton() {
    return this.findCreateMenuButton(CustomReportCreateMenuItemIds.NEW_CHART);
  }

  assertCreateDashboardButtonIsEnabled() {
    const menuButton = this.findCreateDashboardButton();
    menuButton.assertExists();
    menuButton.assertEnabled();
  }

  private findCreateDashboardButton() {
    return this.findCreateMenuButton(CustomReportCreateMenuItemIds.NEW_DASHBOARD);
  }

  private findCreateFolderButton() {
    return this.findCreateMenuButton(CustomReportCreateMenuItemIds.NEW_FOLDER);
  }

  private findCreateReportButton() {
    return this.findCreateMenuButton(CustomReportCreateMenuItemIds.NEW_REPORT);
  }

  private findCreateCustomExportButton() {
    return this.findCreateMenuButton(CustomReportCreateMenuItemIds.NEW_CUSTOM_EXPORT);
  }

  public openCreateDashboard(): CreateEntityDialog {
    const menuItemElement = this.findCreateDashboardButton();
    menuItemElement.click();
    return new CreateEntityDialog({
      viewPath: 'dashboard-view',
      newEntityPath: 'new-dashboard',
      treePath: 'custom-report-tree',
    });
  }

  public openCreateFolder(): CreateEntityDialog {
    const menuItemElement = this.findCreateFolderButton();
    menuItemElement.click();
    return new CreateEntityDialog({
      viewPath: 'folder-view',
      newEntityPath: 'Project',
      treePath: 'custom-report-tree',
    });
  }

  public openCreateChart(): CreateChartViewPage {
    const menuItemElement = this.findCreateChartButton();
    menuItemElement.click();
    return new CreateChartViewPage();
  }

  public openCreateReport(): CreateReportViewPage {
    const menuItemElement = this.findCreateReportButton();
    menuItemElement.click();
    return new CreateReportViewPage();
  }

  public openCreateCustomExport(): CreateExportViewPage {
    const menuItemElement = this.findCreateCustomExportButton();
    menuItemElement.click();
    return new CreateExportViewPage();
  }

  clickDeleteMenuButton(treeUrl: string) {
    const deleteButton = this.deleteButton();
    deleteButton.clickWithoutSpan();
    return new DeleteTreeNodesDialogElement(treeUrl);
  }
}
