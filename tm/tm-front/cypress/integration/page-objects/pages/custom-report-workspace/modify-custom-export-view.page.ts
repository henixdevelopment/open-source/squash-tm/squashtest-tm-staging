import { Page } from '../page';
import { TextFieldElement } from '../../elements/forms/TextFieldElement';
import { selectByDataTestElementId } from '../../../utils/basic-selectors';
import { GridElement, TreeElement } from '../../elements/grid/grid.element';
import { Identifier } from '../../../../../projects/sqtm-core/src/lib/model/entity.model';

export class ModifyCustomExportViewPage extends Page {
  public customExportNameField = new TextFieldElement('name');
  public customExportGrid = new GridElement('custom-export-column-selector');

  constructor() {
    super('sqtm-app-custom-export-workbench');
  }

  private clickScopeButton() {
    return cy.get(this.rootSelector).find('sqtm-app-custom-export-scope-selector').click();
  }

  private openTreeScope() {
    const campaignTreeElement = TreeElement.createTreeElement(
      'campaign-tree-picker',
      'campaign-tree',
    );

    this.clickScopeButton();
    campaignTreeElement.waitInitialDataFetch();
    return campaignTreeElement;
  }

  private clickConfirmScopeButton() {
    return cy
      .get('sqtm-app-custom-export-scope-selector-overlay')
      .find(selectByDataTestElementId('confirm'))
      .click();
  }

  private beginDrag(rowId: Identifier): void {
    this.customExportGrid
      .getCell(rowId, 'NAME', 'mainViewport')
      .findCell()
      .find('div[data-test-grid-selectable="true"]')
      .trigger('mousedown', { button: 0, buttons: 1 })
      .trigger('mousemove', -20, -20, { force: true })
      .trigger('mousemove', 50, 50, { force: true });
  }

  private getDropZone() {
    return cy
      .get(this.rootSelector)
      .find(selectByDataTestElementId('custom-export-workbench-attributes-drop-zone'));
  }

  modifyScopeExport(nodeNames: string[]) {
    const tree = this.openTreeScope();

    for (let i = 0; i < nodeNames.length - 1; i++) {
      const openNodeName = nodeNames[i];
      const selectNodeName = nodeNames[i + 1];
      tree.findRowId('NAME', openNodeName).then((idProject) => {
        tree.openNodeIfClosed(idProject);
      });
      tree.findRowId('NAME', selectNodeName).then((idRequirement) => {
        tree.selectRow(idRequirement, 'NAME');
      });
    }
    this.clickConfirmScopeButton();
  }

  addAttributeNewAttributeToExport(nameAttribute: string) {
    this.customExportGrid.findRowId('NAME', nameAttribute).then((idAttribute) => {
      this.customExportGrid.selectRow(idAttribute, 'NAME');
      this.beginDrag(idAttribute);
      this.getDropZone().trigger('mouseup', { force: true });
    });
  }

  clickModifyButton() {
    cy.get(this.rootSelector).find(selectByDataTestElementId('add-custom-export')).click();
  }
}
