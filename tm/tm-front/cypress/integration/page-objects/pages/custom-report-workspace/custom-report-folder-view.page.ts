import { EditableTextFieldElement } from '../../elements/forms/editable-text-field.element';
import { EditableRichTextFieldElement } from '../../elements/forms/editable-rich-text-field.element';
import { EntityViewPage } from '../page';

export class CustomReportFolderViewPage extends EntityViewPage {
  public readonly nameTextField: EditableTextFieldElement = new EditableTextFieldElement(
    'entity-name',
  );
  public readonly descriptionRichField: EditableRichTextFieldElement =
    new EditableRichTextFieldElement('description');

  constructor(private customReportLibraryNodeId: number | '*') {
    super('sqtm-app-custom-report-folder-view');
  }
}
