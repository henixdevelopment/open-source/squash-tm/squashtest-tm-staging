import { TreeElement } from '../elements/grid/grid.element';
import { NavBarElement } from '../elements/nav-bar/nav-bar.element';
import { HttpMockBuilder } from '../../utils/mocks/request-mock';
import { Page } from './page';
import { DataRowModel } from '../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { ToolbarButtonElement } from '../elements/workspace-common/toolbar.element';
import { MenuElement } from '../../utils/menu.element';

export abstract class WorkspaceWithTreePage extends Page {
  protected constructor(
    public readonly tree: TreeElement,
    rootSelector: string,
  ) {
    super(rootSelector);
    cy.get(this.rootSelector).should('have.length', 1);
    this.tree.assertExists();
  }

  public activateMilestoneMode(milestone: string | number, refreshedRows?: DataRowModel[]) {
    const milestoneSelector = new NavBarElement().openMilestoneSelector();
    milestoneSelector.selectMilestone(milestone);
    const mock = new HttpMockBuilder(this.tree.url)
      .post()
      .responseBody({ dataRows: refreshedRows })
      .build();
    milestoneSelector.confirm();
    mock.wait();
  }

  public disableMilestoneMode(refreshedRows?: DataRowModel[]) {
    const mock = new HttpMockBuilder(this.tree.url)
      .post()
      .responseBody({ dataRows: refreshedRows })
      .build();
    new NavBarElement().disableMilestoneMode();
    mock.wait();
  }

  clickOnParameterButton() {
    const parameterButton = new ToolbarButtonElement(this.rootSelector, 'settings-button');
    const settingsMenu = new MenuElement('settings');
    parameterButton.clickWithoutSpan();
    return settingsMenu;
  }

  assertParameterOverlayExists() {
    const settingsMenu = new MenuElement('settings');
    settingsMenu.assertExists();
  }

  closeParameterOverlay() {
    const settingsMenu = new MenuElement('settings');
    settingsMenu.hide();
  }

  clickOnImportExportButton() {
    const importExportButton = new ToolbarButtonElement(this.rootSelector, 'import-export');
    importExportButton.clickWithoutSpan();
  }

  clickSearchButton() {
    const searchButton = new ToolbarButtonElement(this.rootSelector, 'research-button');
    searchButton.clickWithoutSpan();
  }

  activatePositionalOrder() {
    const sortMenu = new MenuElement('sort-menu');
    this.clickOnParameterButton();
    sortMenu.item('sort-positional').click();
  }
}
