import { selectByDataTestElementId } from '../../../../utils/basic-selectors';
import { GroupedMultiListElement } from '../../../elements/filters/grouped-multi-list.element';

export class SessionOverviewActivityPanelElement {
  public readonly selector = 'sqtm-app-session-overview-page-activity';

  assertHasEmptyActivityMessage(): void {
    cy.get(this.selector)
      .find(selectByDataTestElementId('empty-activity-message'))
      .should('be.visible');
  }

  getNote(index: number): NoteElement {
    return new NoteElement(index);
  }

  filterByKind(kind: string): void {
    cy.get(this.selector).find(selectByDataTestElementId('note-kind-filter')).click();
    const multiList = new GroupedMultiListElement();
    multiList.toggleOneItem(kind);
    multiList.close();
  }

  filterByUser(kind: string): void {
    cy.get(this.selector).find(selectByDataTestElementId('user-filter')).click();
    const multiList = new GroupedMultiListElement();
    multiList.toggleOneItem(kind);
    multiList.close();
  }

  clearUserFilter(): void {
    cy.get(this.selector)
      .find(selectByDataTestElementId('user-filter'))
      .find(selectByDataTestElementId('clear-button'))
      .click();
  }

  clearNoteKindFilter(): void {
    cy.get(this.selector)
      .find(selectByDataTestElementId('note-kind-filter'))
      .find(selectByDataTestElementId('clear-button'))
      .click();
  }

  collapseAllNotes(): void {
    cy.get(this.selector).find(selectByDataTestElementId('collapse-all-notes')).click();
  }

  expandAllNotes(): void {
    cy.get(this.selector).find(selectByDataTestElementId('expand-all-notes')).click();
  }

  assertExists() {
    cy.get(this.selector).should('exist');
  }
}

class NoteElement {
  constructor(private readonly index: number) {}

  assertHasContent(expectedContent: string): void {
    this.selectNote().should('contain', expectedContent);
  }

  assertHasKind(expectedKind: string): void {
    this.selectNote().find('.note-kind').should('contain', expectedKind);
  }

  private selectNote(): Cypress.Chainable<JQuery<HTMLElement>> {
    return cy.get('sqtm-app-readonly-session-note').eq(this.index);
  }

  assertHasAttachmentCount(expectedCount: number): void {
    this.selectNote()
      .find(selectByDataTestElementId('attachment-counter'))
      .should('contain', expectedCount);
  }

  assertDoesNotExist(): void {
    this.selectNote().should('not.exist');
  }

  assertIsCollapsed(): void {
    this.selectNote().find(selectByDataTestElementId('expand')).should('be.visible');
  }

  assertIsExpanded(): void {
    this.selectNote().find(selectByDataTestElementId('collapse')).should('be.visible');
  }
}
