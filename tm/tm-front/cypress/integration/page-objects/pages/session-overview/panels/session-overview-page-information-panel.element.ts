import { Page } from '../../page';
import { EditableDateFieldElement } from '../../../elements/forms/editable-date-field.element';
import { EditableTimeFieldElement } from '../../../elements/forms/editable-time-field.element';
import { selectByDataTestElementId } from '../../../../utils/basic-selectors';
import { EditableSelectFieldElement } from '../../../elements/forms/editable-select-field.element';

export class SessionOverviewPageInformationPanelElement extends Page {
  readonly editableDateField = new EditableDateFieldElement(
    'session-overview-due-date',
    'session-overview/*/due-date',
    true,
  );
  readonly durationField = new EditableTimeFieldElement(
    'session-overview-session-duration',
    'session-overview/*/session-duration',
  );
  readonly executionStatusField = new EditableSelectFieldElement(
    'session-execution-status',
    'session-overview/*/execution-status',
  );

  constructor() {
    super('sqtm-app-session-overview-page-information-panel');
  }

  updateDueDate() {
    this.editableDateField.assertIsEditable();
    this.editableDateField.enableEditMode();
    this.editableDateField.setToTodayAndConfirm();
    this.editableDateField.checkContent(new Date().toLocaleDateString(), true);
  }

  updateSessionDuration() {
    this.durationField.enableEditMode();
    this.durationField.updateTimeWithKeyboardArrows(4, 20);
    cy.clickVoid();
    this.durationField.confirm();
    this.durationField.checkTimeContent('04', '20');
  }

  checkSessionStatus(expectedSessionStatus: string) {
    this.find(selectByDataTestElementId('session-status')).should('contain', expectedSessionStatus);
  }

  checkExecutionStatus(expectedExecutionStatus: string) {
    this.executionStatusField.assertContainsText(expectedExecutionStatus);
  }

  checkExecutionStatusEditableField(isEditable: boolean) {
    this.executionStatusField.find('div').should('have.class', isEditable ? 'editable' : 'read');
  }

  updateExecutionStatusAndCheck(newExecutionStatus: string) {
    this.executionStatusField.setAndConfirmValueNoButton(newExecutionStatus);
  }
}
