import { Page } from '../../page';
import { selectByDataTestElementId } from '../../../../utils/basic-selectors';

export class SessionOverviewPageCharterPanelElement extends Page {
  constructor() {
    super('sqtm-app-session-overview-page-charter-panel');
  }

  checkCharter(expectedCharter: string) {
    cy.get(selectByDataTestElementId('session-overview-charter')).should(
      'have.text',
      expectedCharter,
    );
  }
}
