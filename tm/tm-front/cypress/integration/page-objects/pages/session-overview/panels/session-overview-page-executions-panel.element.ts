import { Page } from '../../page';
import { GridElement } from '../../../elements/grid/grid.element';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { SimpleDeleteConfirmDialogElement } from '../../../elements/dialog/simple-delete-confirm-dialog.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

export class SessionOverviewPageExecutionsPanelElement extends Page {
  readonly gridElement = GridElement.createGridElement(
    'exploratory-session-executions',
    'session-overview/*/executions',
  );

  constructor() {
    super('sqtm-app-session-overview-page-executions-panel');
  }

  checkNoteTypes(rowId: string, expectedLetters: string[]) {
    const noteTypesCell = this.gridElement.getRow(rowId).cell(GridColumnId.noteTypes);
    noteTypesCell.assertExists();
    expectedLetters.forEach((expectedColor) => {
      noteTypesCell
        .findIconInCell('note-type-icon')
        .contains(expectedColor)
        .should('not.have.class', 'hidden');
    });
  }

  deleteRow(rowId: string, gridRefreshResponse: GridResponse): void {
    this.gridElement.declareRefreshData(gridRefreshResponse);
    const mock = new HttpMockBuilder('session-overview/*/executions/*').delete().build();
    this.gridElement
      .getCell(rowId, 'delete', 'rightViewport')
      .findIconInCell('sqtm-core-generic:delete')
      .click();
    new SimpleDeleteConfirmDialogElement().confirm();
    mock.wait();
    this.gridElement.waitForRefresh();
  }

  assertHasExecutionsWithUsers(usersExpected: string[]) {
    this.gridElement.assertRowCount(usersExpected.length);
    usersExpected.forEach((user) => {
      this.gridElement.findRowId('assigneeFullName', user).should('exist');
    });
  }

  clickOnAccessExecution(executionRow: string) {
    const mock = new HttpMockBuilder('exploratory-execution/*/is-execution-running').build();
    this.gridElement
      .getCell(executionRow, 'executionId', 'rightViewport')
      .findIconInCell('sqtm-core-campaign:play')
      .click();
    mock.wait();
  }
}
