import { Page } from '../page';
import { ReferentialDataProviderBuilder } from '../../../utils/referential/referential-data.provider';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { ExploratorySessionOverviewModel } from '../../../../../projects/sqtm-core/src/lib/model/execution/exploratory-session-overview.model';
import { mockGridResponse } from '../../../data-mock/grid.data-mock';
import { SessionOverviewPageInformationPanelElement } from './panels/session-overview-page-information-panel.element';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { SessionOverviewPageExecutionsPanelElement } from './panels/session-overview-page-executions-panel.element';
import { SimpleDeleteConfirmDialogElement } from '../../elements/dialog/simple-delete-confirm-dialog.element';
import {
  selectByDataTestButtonId,
  selectByDataTestElementId,
} from '../../../utils/basic-selectors';
import { SessionOverviewPageCharterPanelElement } from './panels/session-overview-page-charter-panel.element';
import { AddMultipleExecutionsDialogElement } from '../test-case-workspace/dialogs/add-multiple-executions-dialog.element';
import { EditableRichTextFieldElement } from '../../elements/forms/editable-rich-text-field.element';
import { AlertDialogElement } from '../../elements/dialog/alert-dialog.element';
import { AnchorsElement } from '../../elements/anchor/anchors.element';
import { SessionNoteModel } from '../../../../../projects/sqtm-core/src/lib/model/execution/execution.model';
import { IssueUIModel } from '../../../../../projects/sqtm-core/src/lib/model/issue/issue-ui.model';
import { SessionOverviewActivityPanelElement } from './panels/session-overview-page-activity-panel.element';
import { AccessRunningExploratoryExecutionDialogElement } from '../test-case-workspace/dialogs/access-running-exploratory-execution-dialog.element';
import { SimpleUser } from '../../../../../projects/sqtm-core/src/lib/model/user/user.model';

export class SessionOverviewPage extends Page {
  readonly anchors = AnchorsElement.withLinkIds(
    'information',
    'executions',
    'comments',
    'charter',
    'issues',
    'activity',
  );

  readonly informationPanel = new SessionOverviewPageInformationPanelElement();
  readonly executionsPanel = new SessionOverviewPageExecutionsPanelElement();
  readonly charterPanel = new SessionOverviewPageCharterPanelElement();

  readonly commentsField = new EditableRichTextFieldElement(
    'session-overview-comments',
    'session-overview/*/comments',
  );

  readonly finishSessionDialog = new AlertDialogElement('end-session-dialog');
  readonly warningAccessExecutionDialog = new AccessRunningExploratoryExecutionDialogElement();

  constructor() {
    super('sqtm-app-session-overview-page');
  }

  public static initTestAtPage(
    overviewId: number,
    overviewModel?: ExploratorySessionOverviewModel,
    gridResponse?: GridResponse,
  ) {
    const referentialDataProvider = new ReferentialDataProviderBuilder().build();
    const overviewModelMock = new HttpMockBuilder(`session-overview/${overviewId}?*`)
      .responseBody(overviewModel)
      .build();
    const executionGridMock = new HttpMockBuilder(`session-overview/${overviewId}/executions`)
      .responseBody(gridResponse ? gridResponse : mockGridResponse(null, []))
      .post()
      .build();

    cy.visit(`session-overview/${overviewId}`);

    referentialDataProvider.wait();
    overviewModelMock.wait();
    executionGridMock.wait();

    return new SessionOverviewPage();
  }

  addExecution(gridRefreshResponse?: GridResponse): void {
    this.executionsPanel.gridElement.declareRefreshData(gridRefreshResponse);
    const mock = new HttpMockBuilder(`session-overview/*/new-execution`).post().build();
    this.clickOnAddExecutionButton();
    mock.wait();
    this.executionsPanel.gridElement.waitForRefresh();
  }

  private clickOnAddExecutionButton(): void {
    this.find(selectByDataTestButtonId('add-execution')).click();
  }

  massDeleteExecutions(gridRefreshResponse: GridResponse): void {
    const mock = new HttpMockBuilder('session-overview/*/executions/*').delete().build();
    this.clickOnMassDeleteExecutionsButton();
    this.executionsPanel.gridElement.declareRefreshData(gridRefreshResponse);
    new SimpleDeleteConfirmDialogElement().confirm();
    mock.wait();
    this.executionsPanel.gridElement.waitForRefresh();
  }

  private clickOnMassDeleteExecutionsButton(): void {
    this.find(selectByDataTestButtonId('delete-executions')).click();
  }

  checkSessionStatusInCapsule(expectedSessionStatus: string) {
    this.find(selectByDataTestElementId('exploratory-session-status')).should(
      'contain',
      expectedSessionStatus,
    );
  }

  startSession() {
    const mock = new HttpMockBuilder(`session-overview/*/start-session`).post().build();
    this.find(selectByDataTestButtonId('start-session-overview')).click();
    mock.wait();
  }

  finishSession() {
    const mock = new HttpMockBuilder(`session-overview/*/end-session`).post().build();
    this.find(selectByDataTestButtonId('end-session-overview')).click();
    mock.wait();
    this.closeFinishSessionDialog();
  }

  checkStartButton(expectedText: string, isActive: boolean) {
    this.find(selectByDataTestButtonId('start-session-overview')).should('contain', expectedText);
    this.find(selectByDataTestButtonId('start-session-overview')).should(
      isActive ? 'not.be.disabled' : 'be.disabled',
    );
  }

  checkEndButton(isActive: boolean) {
    this.find(selectByDataTestButtonId('end-session-overview')).should(
      isActive ? 'not.be.disabled' : 'be.disabled',
    );
  }

  checkExecutionStatusInCapsule(expectedExecutionStatus: string) {
    this.find(selectByDataTestElementId('exploratory-session-execution-status')).should(
      'contain',
      expectedExecutionStatus,
    );
  }

  updateExecutionStatus(newExecutionStatus: string) {
    this.informationPanel.updateExecutionStatusAndCheck(newExecutionStatus);
  }

  checkReviewStatusInCapsule(expectedReviewStatus: string) {
    this.find(selectByDataTestElementId('exploratory-session-review-status')).should(
      'contain',
      expectedReviewStatus,
    );
  }

  openAddMultipleExecutionsDialog(
    assignableUsers: SimpleUser[],
  ): AddMultipleExecutionsDialogElement {
    const mock = new HttpMockBuilder('session-overview/*/unassigned-users')
      .responseBody(assignableUsers)
      .build();
    this.find(selectByDataTestButtonId('add-multiple-executions')).click();
    mock.wait();
    const dialog = new AddMultipleExecutionsDialogElement();
    dialog.assertExists();
    return dialog;
  }

  closeFinishSessionDialog() {
    this.finishSessionDialog.close();
  }

  showActivityPanel(
    sessionNotes: SessionNoteModel[],
    issueModel: IssueUIModel,
  ): SessionOverviewActivityPanelElement {
    const sessionNotesMock = new HttpMockBuilder('session-overview/*/session-notes')
      .responseBody(sessionNotes)
      .build();
    const issueModelMock = new HttpMockBuilder('issues/session-overview/*?*')
      .responseBody(issueModel)
      .build();

    this.anchors.clickLink('activity');

    sessionNotesMock.wait();
    issueModelMock.wait();

    return new SessionOverviewActivityPanelElement();
  }

  checkActivityCount(expectedCount: string) {
    this.find(selectByDataTestElementId('activity-count')).should('contain', expectedCount);
  }
}
