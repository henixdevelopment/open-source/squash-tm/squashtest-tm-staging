import { Page } from '../page';
import { HomeWorkspacePage } from '../home-workspace/home-workspace.page';
import { ReferentialDataProviderBuilder } from '../../../utils/referential/referential-data.provider';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { InformationPage } from '../information/information.page';
import { AdminReferentialDataProviderBuilder } from '../../../utils/referential/admin-referential-data.provider';
import { AdminReferentialDataMockBuilder } from '../../../utils/referential/admin-referential-data-builder';
import { LoginPageModel } from '../../../../../projects/sqtm-core/src/lib/model/login/login-page.model';
import { HomeWorkspaceModel } from '../../../../../projects/sqtm-core/src/lib/model/home/home-workspace.model';
import { ReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { selectByDataTestButtonId } from '../../../utils/basic-selectors';

export class LoginPage extends Page {
  public static navigateTo(loginModel?: LoginPageModel): LoginPage {
    const httpMock = new HttpMockBuilder('login-page').responseBody(loginModel).build();
    cy.visit('login');
    httpMock.wait();
    return new LoginPage();
  }

  protected constructor() {
    super('sqtm-app-login-page');
  }

  login(
    username: string,
    password: string,
    homeWorkspaceModel?: HomeWorkspaceModel,
    referentialData?: ReferentialDataModel,
  ): HomeWorkspacePage {
    this.fillUsername(username);
    this.fillPassword(password);
    const loginRequestMock = new HttpMockBuilder('login')
      .post()
      .responseBody({ authenticated: true })
      .build();
    const homePageModelMock = new HttpMockBuilder('home-workspace')
      .responseBody(homeWorkspaceModel)
      .build();
    // We must create the provider, and thus the route/stub before submitting form or it can cause race condition between cypress and the
    // tested angular app (aka the request can be fired before cypress is able to install his spy or stub)
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    this.submit();
    // Wait for login success which is the first request that occurs
    loginRequestMock.wait();
    // Wait for referential data provider which his the second request to occurs
    referentialDataProvider.wait();
    // Wait for home workspace request which his the third request to occurs
    homePageModelMock.wait();
    return this.createHomeWorkspacePage();
  }

  assertLoginFails(username: string, password: string): void {
    this.fillUsername(username);
    this.fillPassword(password);
    const httpMock = new HttpMockBuilder('login')
      .post()
      .responseBody({ authenticated: false })
      .build();
    this.submit();
    httpMock.wait();
  }

  loginWithLicenseInfo(username: string, password: string): InformationPage {
    this.fillUsername(username);
    this.fillPassword(password);
    const loginRequestMock = new HttpMockBuilder('login')
      .post()
      .responseBody({
        authenticated: true,
        showInformation: true,
      })
      .build();
    const adminReferentialData = new AdminReferentialDataMockBuilder()
      .withLicenseInformation({
        activatedUserExcess: '1-1-false',
        pluginLicenseExpiration: '12',
      })
      .build();

    const referentialDataProvider = new AdminReferentialDataProviderBuilder(
      adminReferentialData,
    ).build();

    this.submit();
    loginRequestMock.wait();
    referentialDataProvider.wait();
    return this.createLicenseInformationPage();
  }

  private createHomeWorkspacePage() {
    const homeWorkspacePage = new HomeWorkspacePage();
    homeWorkspacePage.assertExists();
    return homeWorkspacePage;
  }

  private createLicenseInformationPage(): InformationPage {
    const licenseInfoPage = new InformationPage();
    licenseInfoPage.assertIsVisible();
    return licenseInfoPage;
  }

  private submit() {
    cy.get('#submit-login-form').click();
  }

  private fillPassword(password: string) {
    cy.get('[formcontrolname="password"]').type(password);
  }

  private fillUsername(username: string) {
    cy.get('[formcontrolname="login"]').type(username);
  }

  assertLoginMessageContains(expectedLoginMessage: string) {
    this.findByElementId('login-message').should('contain.html', expectedLoginMessage);
  }

  assertLoginMessageIsNotVisible() {
    this.findByElementId('login-message').should('not.exist');
  }

  assertH2warningIsVisible() {
    this.findByElementId('h2-warning').should('be.visible');
  }
  assertOidcProviderLoginButtonIsVisible(provider: string) {
    cy.get(selectByDataTestButtonId('oidc-submit-login-form-' + provider)).should('be.visible');
  }

  assertLoginFailedWarningIsVisible() {
    this.findByElementId('login-fail-warning').should('be.visible');
  }

  assertOidcProviderLoginButtonDoesNotExist(provider: string) {
    cy.get(selectByDataTestButtonId('oidc-submit-login-form-' + provider)).should('not.exist');
  }
}
