import { DatabaseUtils } from '../../../utils/database.utils';

export class SystemE2eCommands {
  public static setMilestoneActivated() {
    DatabaseUtils.executeQuery(
      `delete from CORE_CONFIG where STR_KEY LIKE 'feature.milestone.enabled';`,
    ).then(() =>
      DatabaseUtils.executeQuery(
        `insert into CORE_CONFIG values ('feature.milestone.enabled','true')`,
      ),
    );
  }
}
