import { PrerequisiteBuilder } from '../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';

export class AdminE2eCommands {
  public static createUserFromLogin(login: string) {
    this.createUsersFromLogin([login]);
  }

  public static createUsersFromLogin(logins: string[]) {
    new PrerequisiteBuilder()
      .withUsers(AdminE2eCommands.createUsersBuildersFromLogin(logins))
      .build();
  }

  private static createUsersBuildersFromLogin(logins: string[]): UserBuilder[] {
    return logins.map((login) => new UserBuilder(login));
  }
}
