import { AttachmentListModel } from '../../../projects/sqtm-core/src/lib/model/attachment/attachment-list.model';
import { combineWithDefaultData } from './data-mocks.utils';
import { AttachmentModel } from '../../../projects/sqtm-core/src/lib/model/attachment/attachment.model';

export function mockEmptyAttachmentListModel(): AttachmentListModel {
  return {
    id: 1,
    attachments: [],
  };
}

export function mockAttachmentListModel(
  id: number,
  attachments: AttachmentModel[],
): AttachmentListModel {
  return { id, attachments };
}

export function mockAttachmentModel(customData?: Partial<AttachmentModel>): AttachmentModel {
  const defaultData: AttachmentModel = {
    id: 1,
    name: 'Attachment.txt',
    size: 1000,
    addedOn: new Date(),
    lastModifiedOn: null,
  };

  return combineWithDefaultData(defaultData, customData);
}
