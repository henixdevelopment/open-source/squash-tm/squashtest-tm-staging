import { combineWithDefaultData } from './data-mocks.utils';
import { ExploratorySessionOverviewModel } from '../../../projects/sqtm-core/src/lib/model/execution/exploratory-session-overview.model';
import { mockEmptyAttachmentListModel } from './generic-entity.data-mock';

export function mockSessionOverview(
  customData: Partial<ExploratorySessionOverviewModel> = {},
): ExploratorySessionOverviewModel {
  return combineWithDefaultData(
    {
      id: 1,
      createdBy: 'cypress',
      createdOn: new Date().toISOString(),
      charter:
        "Les tests exploratoires consistent à effectuer des parcours libres dans l'application.",
      dueDate: null,
      projectId: 1,
      name: 'Exploratory session overview #1',
      lastModifiedOn: null,
      lastModifiedBy: null,
      sessionDuration: null,
      assignedUser: null,
      assignableUsers: [],
      reference: null,
      sessionStatus: 'TO_DO',
      attachmentList: mockEmptyAttachmentListModel(),
      nbIssues: null,
      executionStatus: 'READY',
      customFieldValues: null,
      inferredSessionReviewStatus: 'TO_DO',
      nbExecutions: 0,
      nbNotes: 0,
      comments: '',
      milestones: null,
      sprintStatus: 'OPEN',
      tclnId: 1,
    },
    customData,
  );
}
