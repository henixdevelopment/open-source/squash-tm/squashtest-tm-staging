import { BasePermissions } from '../../../projects/sqtm-core/src/lib/model/permissions/simple-permissions';
import {
  DataRowModel,
  DataRowOpenState,
  SquashTmDataRowType,
} from '../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { GridResponse } from '../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

class AllPassPermissions implements BasePermissions {
  canRead = true;
  canWrite = true;
  canAttach = true;
  canCreate = true;
  canDelete = true;
  canExtendedDelete = true;
  canExport = true;
  canImport = true;
  canLink = true;
  canExecute = true;
  canDeleteExecution = true;
}

type TreeNodeModel = Omit<
  DataRowModel,
  'type' | 'allowedChildren' | 'allowMoves' | 'selectable' | 'simplePermissions'
>;

export function mockTreeNode<T = any>(customData: Partial<TreeNodeModel>): DataRowModel<T> {
  const defaultNode: DataRowModel = {
    id: 1,
    projectId: 1,
    data: {} as T,
    children: [],
    parentRowId: undefined,
    state: DataRowOpenState.closed,
  };

  return { ...defaultNode, ...customData };
}

export function mockDataRow<T = any>(customData: Partial<DataRowModel<T>>): DataRowModel<T> {
  const defaultRow: DataRowModel<T> = {
    id: 1,
    projectId: 1,
    type: SquashTmDataRowType.Generic,
    data: {} as T,
    allowedChildren: [],
    allowMoves: false,
    children: [],
    parentRowId: undefined,
    state: DataRowOpenState.leaf,
    selectable: true,
    simplePermissions: new AllPassPermissions(),
  };

  return { ...defaultRow, ...customData };
}

export function mockGridResponse<T = any>(
  idAttribute: string,
  dataRows: DataRowModel<T>[],
  activeColumnIds?: string[],
): GridResponse<T> {
  return {
    dataRows,
    count: dataRows.length,
    idAttribute,
    activeColumnIds,
  };
}
