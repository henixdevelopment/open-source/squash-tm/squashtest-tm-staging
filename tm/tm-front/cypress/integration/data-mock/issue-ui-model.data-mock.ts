import { IssueUIModel } from '../../../projects/sqtm-core/src/lib/model/issue/issue-ui.model';
import { combineWithDefaultData } from './data-mocks.utils';

export function mockIssueUIModel(customData?: Partial<IssueUIModel>): IssueUIModel {
  const defaultData: IssueUIModel = {
    activated: false,
    bugTrackerMode: undefined,
    bugTrackerStatus: '',
    delete: '',
    entityType: undefined,
    error: undefined,
    hasError: false,
    modelLoaded: false,
    oslc: false,
    panelStyle: '',
    projectId: 0,
    projectName: '',
  };
  return combineWithDefaultData(defaultData, customData);
}
