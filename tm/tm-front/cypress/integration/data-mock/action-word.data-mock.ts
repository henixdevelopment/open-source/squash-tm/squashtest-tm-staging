import { mockEmptyAttachmentListModel } from './generic-entity.data-mock';
import { ActionWordLibraryModel } from '../../../projects/sqtm-core/src/lib/model/action-word-library/action-word-library.model';
import { combineWithDefaultData } from './data-mocks.utils';
import { ActionWordModel } from '../../../projects/sqtm-core/src/lib/model/action-word-library/action-word.model';

export function mockActionWordLibrary(
  customData: Partial<ActionWordLibraryModel> = {},
): ActionWordLibraryModel {
  const defaultData: ActionWordLibraryModel = {
    id: 1,
    projectId: 1,
    attachmentList: mockEmptyAttachmentListModel(),
    customFieldValues: [],
    description: '',
    name: '',
  };

  return combineWithDefaultData(defaultData, customData);
}

export function mockActionWord(customData: Partial<ActionWordModel> = {}): ActionWordModel {
  const defaultData: ActionWordModel = {
    id: 1,
    projectId: 1,
    attachmentList: mockEmptyAttachmentListModel(),
    customFieldValues: [],
    description: '',
    name: '',
  };

  return combineWithDefaultData(defaultData, customData);
}
