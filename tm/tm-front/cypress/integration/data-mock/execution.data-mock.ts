import { combineWithDefaultData } from './data-mocks.utils';
import {
  ExecutionModel,
  ExecutionStepModel,
  SessionNoteModel,
} from '../../../projects/sqtm-core/src/lib/model/execution/execution.model';

export function mockExecutionModel(customData: Partial<ExecutionModel>): ExecutionModel {
  const defaultData: ExecutionModel = {
    id: 1,
    projectId: 1,
    executionOrder: 2,
    name: 'NX - Test Case 1',
    prerequisite: '',
    attachmentList: { id: 1, attachments: [] },
    customFieldValues: [],
    tcImportance: 'LOW',
    tcNatLabel: 'test-case.nature.NAT_BUSINESS_TESTING',
    tcNatIconName: '',
    tcStatus: 'APPROVED',
    tcTypeLabel: 'test-case.type.TYP_COMPLIANCE_TESTING',
    tcTypeIconName: '',
    tcDescription: 'description',
    comment: '',
    denormalizedCustomFieldValues: [],
    executionStepViews: [
      mockExecutionStepModel({
        attachmentList: {
          id: 1,
          attachments: [
            {
              id: 1,
              addedOn: new Date('25 Mar 2020 10:12:57'),
              lastModifiedOn: null,
              name: 'attachment-1',
              size: 56789,
            },
            {
              id: 2,
              addedOn: new Date('17 Mar 2019 20:40:00'),
              lastModifiedOn: null,
              name: 'attachment-2',
              size: 1234567,
            },
          ],
        },
      }),
      mockExecutionStepModel({
        id: 2,
        order: 1,
      }),
      mockExecutionStepModel({
        id: 3,
        order: 2,
      }),
    ],
    coverages: [],
    executionMode: 'MANUAL',
    lastExecutedOn: null,
    lastExecutedBy: 'admin',
    executionStatus: 'READY',
    automatedJobUrl: null,
    testAutomationServerKind: null,
    automatedExecutionResultUrl: null,
    automatedExecutionResultSummary: null,
    automatedExecutionDuration: null,
    nbIssues: 0,
    iterationId: -1,
    kind: 'STANDARD',
    executionsCount: 0,
    milestones: [],
    testPlanItemId: -1,
    denormalizedEnvironmentTags: { id: 1, value: '' },
    denormalizedEnvironmentVariables: [],
    exploratorySessionOverviewInfo: null,
    sessionNotes: null,
    exploratoryExecutionRunningState: null,
    latestExploratoryExecutionEvent: null,
    reviewed: false,
    taskDivision: null,
    parentSprintStatus: null,
    testCaseId: 1,
    extenderId: null,
  };

  return combineWithDefaultData(defaultData, customData);
}

export function mockExecutionStepModel(
  customData: Partial<ExecutionStepModel>,
): ExecutionStepModel {
  return combineWithDefaultData(
    {
      id: 1,
      order: 0,
      executionStatus: 'READY',
      attachmentList: {
        id: 1,
        attachments: [],
      },
      action: '',
      expectedResult: '',
      comment: '',
      customFieldValues: [],
      projectId: 1,
      lastExecutedBy: 'admin',
      lastExecutedOn: null,
      denormalizedCustomFieldValues: [],
    },
    customData,
  );
}

export function mockSessionNoteModel(customData: Partial<SessionNoteModel>): SessionNoteModel {
  return combineWithDefaultData(
    {
      noteId: 1,
      kind: 'COMMENT',
      content: '',
      noteOrder: 0,
      createdBy: 'Cypress',
      createdOn: new Date().toISOString(),
      lastModifiedBy: null,
      lastModifiedOn: new Date().toISOString(),
      attachmentList: { id: 1, attachments: [] },
    },
    customData,
  );
}
