import { combineWithDefaultData } from './data-mocks.utils';
import { Milestone } from '../../../projects/sqtm-core/src/lib/model/milestone/milestone.model';

export function mockMilestoneModel(customData: Partial<Milestone> = {}): Milestone {
  return combineWithDefaultData(
    {
      id: 1,
      endDate: new Date().toISOString(),
      label: '',
      description: '',
      ownerFistName: '',
      ownerLastName: '',
      ownerLogin: '',
      range: 'GLOBAL',
      status: 'IN_PROGRESS',
      createdBy: 'admin',
      createdOn: new Date().toISOString(),
      lastModifiedBy: 'admin',
      lastModifiedOn: new Date().toISOString(),
    },
    customData,
  );
}
