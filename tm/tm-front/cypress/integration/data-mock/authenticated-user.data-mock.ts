import { AuthenticatedUser } from '../../../projects/sqtm-core/src/lib/model/user/authenticated-user.model';

export function mockAuthenticatedUserNotAdmin(): AuthenticatedUser {
  return {
    userId: 1,
    username: 'Clara',
    admin: false,
    hasAnyReadPermission: false,
    projectManager: false,
    milestoneManager: false,
    clearanceManager: false,
    functionalTester: false,
    automationProgrammer: true,
    firstName: 'Clara',
    lastName: 'Luciani',
    canDeleteFromFront: false,
  };
}
