import { mockEmptyAttachmentListModel } from './generic-entity.data-mock';
import { combineWithDefaultData } from './data-mocks.utils';
import { TestCaseModel } from '../../../projects/sqtm-core/src/lib/model/test-case/test-case.model';
import { TestCaseFolderModel } from '../../../projects/sqtm-core/src/lib/model/test-case/test-case-folder/test-case-folder.model';
import { TestCaseLibraryModel } from '../../../projects/sqtm-core/src/lib/model/test-case/test-case-library/test-case-library.model';
import { CallStepModel } from '../../../projects/sqtm-core/src/lib/model/test-case/test-step.model';

export function mockTestCaseModel(customData: Partial<TestCaseModel> = {}): TestCaseModel {
  const defaultData: TestCaseModel = {
    id: 1,
    projectId: 1,
    customFieldValues: [],
    attachmentList: mockEmptyAttachmentListModel(),
    name: 'name',
    reference: 'ref',
    importance: 'LOW',
    description: 'description',
    status: 'WORK_IN_PROGRESS',
    nature: 12,
    type: 20,
    importanceAuto: false,
    automatable: 'M',
    prerequisite: '',
    testSteps: [],
    milestones: [],
    automationRequest: null,
    parameters: [],
    datasets: [],
    datasetParamValues: [],
    coverages: [],
    uuid: '44d63d7e-11dd-44b0-b584-565b6f791fa2',
    kind: 'STANDARD',
    executions: [],
    nbIssues: 0,
    calledTestCases: [],
    lastModifiedOn: null,
    lastModifiedBy: '',
    createdBy: 'cypress',
    createdOn: new Date().toISOString(),
    lastExecutionStatus: 'SUCCESS',
    script: '',
    charter: '',
    sessionDuration: null,
    actionWordLibraryActive: false,
    scmRepositoryId: null,
    automatedTestReference: '',
    automatedTestTechnology: null,
    automatedTest: {
      id: 0,
      name: 'name',
      fullLabel: '',
    },
    nbExecutions: 0,
    nbSessions: 0,
    draftedByAi: false,
  };

  return combineWithDefaultData(defaultData, customData);
}

export function mockTestCaseFolderModel(
  customData: Partial<TestCaseFolderModel> = {},
): TestCaseFolderModel {
  const defaultData: TestCaseFolderModel = {
    id: 1,
    projectId: 1,
    attachmentList: mockEmptyAttachmentListModel(),
    canShowFavoriteDashboard: false,
    customFieldValues: [],
    dashboard: undefined,
    description: '',
    favoriteDashboardId: 1,
    name: '',
    shouldShowFavoriteDashboard: false,
    statistics: undefined,
  };

  return combineWithDefaultData(defaultData, customData);
}

export function mockTestCaseLibraryModel(
  customData: Partial<TestCaseLibraryModel> = {},
): TestCaseLibraryModel {
  const defaultData: TestCaseLibraryModel = {
    id: 1,
    projectId: 1,
    attachmentList: mockEmptyAttachmentListModel(),
    canShowFavoriteDashboard: false,
    customFieldValues: [],
    dashboard: undefined,
    description: '',
    favoriteDashboardId: 0,
    name: '',
    shouldShowFavoriteDashboard: false,
    statistics: {
      boundRequirementsStatistics: {
        manyRequirements: 0,
        oneRequirement: 0,
        zeroRequirements: 0,
      },
      importanceStatistics: {
        high: 0,
        low: 0,
        medium: 0,
        veryHigh: 0,
      },
      sizeStatistics: {
        above20Steps: 0,
        between0And10Steps: 0,
        between11And20Steps: 0,
        zeroSteps: 0,
      },
      statusesStatistics: {
        approved: 0,
        obsolete: 0,
        toBeUpdated: 0,
        underReview: 0,
        workInProgress: 0,
      },
      selectedIds: [],
    },
  };

  return combineWithDefaultData(defaultData, customData);
}

export function mockCallStepModel(customData: Partial<CallStepModel> = {}): CallStepModel {
  const defaultData: CallStepModel = {
    id: 1,
    kind: 'call-step',
    stepOrder: 0,
    projectId: 1,
    calledTcId: -1,
    calledTcName: 'main engines sequence start',
    calledDatasetId: null,
    calledTestCaseSteps: [],
    delegateParam: false,
    calledDatasetName: 'dataset #1',
  };

  return combineWithDefaultData(defaultData, customData);
}
