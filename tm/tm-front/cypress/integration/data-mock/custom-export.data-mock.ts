import { CustomExportModel } from '../../../projects/sqtm-core/src/lib/model/custom-report/custom-export.model';
import { combineWithDefaultData } from './data-mocks.utils';

export function mockCustomExportViewModel(
  customData: Partial<CustomExportModel> = {},
): CustomExportModel {
  return combineWithDefaultData(
    {
      id: 1,
      customReportLibraryNodeId: 1,
      projectId: 1,
      name: '',
      createdOn: '',
      createdBy: '',
      lastModifiedOn: '',
      lastModifiedBy: '',
      scopeNodes: [],
      columns: [],
      customFieldsOnScope: {},
    },
    customData,
  );
}
