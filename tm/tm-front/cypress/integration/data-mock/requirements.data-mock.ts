import { mockEmptyAttachmentListModel } from './generic-entity.data-mock';
import { combineWithDefaultData } from './data-mocks.utils';
import {
  LinkedLowLevelRequirement,
  RequirementVersionModel,
} from '../../../projects/sqtm-core/src/lib/model/requirement/requirement-version.model';
import { RequirementFolderModel } from '../../../projects/sqtm-core/src/lib/model/requirement/requirement-folder/requirement-folder.model';
import { RequirementLibraryModel } from '../../../projects/sqtm-core/src/lib/model/requirement/requirement-library/requirement-library.model';
import { RequirementVersionStatsBundle } from '../../../projects/sqtm-core/src/lib/model/requirement/requirement-version-stats-bundle.model';
import { DataRowModel } from '../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { mockDataRow } from './grid.data-mock';
import { GridColumnId } from '../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';

export function mockRequirementVersionModel(
  customData: Partial<RequirementVersionModel> = {},
): RequirementVersionModel {
  return combineWithDefaultData(
    {
      id: 1,
      projectId: 1,
      attachmentList: mockEmptyAttachmentListModel(),
      bindableMilestones: [],
      category: 1,
      createdBy: '',
      createdOn: undefined,
      criticality: 'CRITICAL',
      customFieldValues: [],
      description: '',
      lastModifiedBy: '',
      lastModifiedOn: undefined,
      milestones: [],
      name: '',
      reference: '',
      requirementId: 0,
      requirementStats: mockRequirementVersionStatsBundle(),
      requirementVersionLinks: [],
      status: 'OBSOLETE',
      verifyingTestCases: [],
      versionNumber: 0,
      highLevelRequirement: false,
      lowLevelRequirements: [],
      linkedHighLevelRequirement: null,
      remoteReqPerimeterStatus: undefined,
      childOfRequirement: false,
      nbIssues: null,
      hasExtender: false,
      remoteReqId: null,
      remoteReqUrl: null,
      remoteSynchronisationKind: null,
      serverId: null,
      serverName: null,
      syncReqIdsForRemoteSyncId: [],
      syncStatus: null,
      aiServerId: null,
    },
    customData,
  );
}

export function mockRequirementVersionDataRow(customData?: Partial<DataRowModel>): DataRowModel {
  return combineWithDefaultData(
    mockDataRow({
      id: '1',
      type: 'Requirement',
      data: {
        [GridColumnId.name]: 'Exigence 1',
        [GridColumnId.id]: 1,
        [GridColumnId.reference]: 'ref1',
        [GridColumnId.projectName]: 'project 1',
        [GridColumnId.attachments]: 2,
        [GridColumnId.status]: 'WORK_IN_PROGRESS',
        [GridColumnId.criticality]: 'CRITICAL',
        [GridColumnId.category]: 1,
        [GridColumnId.createdBy]: 'admin',
        [GridColumnId.lastModifiedBy]: 'hello',
        [GridColumnId.reqMilestoneLocked]: 0,
        [GridColumnId.milestones]: 0,
        [GridColumnId.coverages]: 3,
        [GridColumnId.versionsCount]: 3,
        [GridColumnId.versionNumber]: 2,
      },
    }),
    customData,
  );
}

export function mockRequirementLibraryModel(
  customData: Partial<RequirementLibraryModel> = {},
): RequirementLibraryModel {
  return combineWithDefaultData(
    {
      id: 1,
      projectId: 1,
      name: '',
      attachmentList: mockEmptyAttachmentListModel(),
      canShowFavoriteDashboard: false,
      customFieldValues: [],
      dashboard: undefined,
      description: '',
      favoriteDashboardId: 0,
      shouldShowFavoriteDashboard: false,
      statistics: undefined,
    },
    customData,
  );
}

export function mockRequirementFolderModel(
  customData: Partial<RequirementFolderModel> = {},
): RequirementFolderModel {
  return combineWithDefaultData(
    {
      id: 1,
      projectId: 1,
      name: '',
      attachmentList: mockEmptyAttachmentListModel(),
      canShowFavoriteDashboard: false,
      customFieldValues: [],
      dashboard: undefined,
      description: '',
      favoriteDashboardId: 0,
      shouldShowFavoriteDashboard: false,
      statistics: undefined,
    },
    customData,
  );
}

export function mockLinkedLowLevelRequirementModel(
  customData: Partial<LinkedLowLevelRequirement> = {},
): LinkedLowLevelRequirement {
  return combineWithDefaultData(
    {
      requirementId: null,
      requirementVersionId: null,
      name: '',
      reference: '',
      childOfRequirement: true,
      projectName: '',
      milestoneLabels: '',
      milestoneMinDate: null,
      milestoneMaxDate: null,
      versionNumber: 1,
      criticality: 'MAJOR',
      requirementStatus: 'APPROVED',
    },
    customData,
  );
}

export function mockRequirementVersionStatsBundle(
  customData: Partial<RequirementVersionStatsBundle> = {},
): RequirementVersionStatsBundle {
  const defaultData: RequirementVersionStatsBundle = {
    children: {
      allTestCaseCount: 0,
      executedTestCase: 0,
      plannedTestCase: 0,
      verifiedTestCase: 0,
      redactedTestCase: 0,
      validatedTestCases: 0,
    },
    total: {
      allTestCaseCount: 0,
      executedTestCase: 0,
      plannedTestCase: 0,
      verifiedTestCase: 0,
      redactedTestCase: 0,
      validatedTestCases: 0,
    },
    currentVersion: {
      allTestCaseCount: 0,
      executedTestCase: 0,
      plannedTestCase: 0,
      verifiedTestCase: 0,
      redactedTestCase: 0,
      validatedTestCases: 0,
    },
    haveChildren: false,
    nonObsoleteDescendantsCount: 0,
    coveredDescendantsCount: 0,
  };

  return combineWithDefaultData(defaultData, customData);
}
