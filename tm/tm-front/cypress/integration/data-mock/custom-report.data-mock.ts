import { mockEmptyAttachmentListModel } from './generic-entity.data-mock';
import { combineWithDefaultData } from './data-mocks.utils';
import { CustomReportLibraryModel } from '../../../projects/sqtm-core/src/lib/model/custom-report/custom-report-librairy/custom-report-library.model';
import { CustomReportFolderModel } from '../../../projects/sqtm-core/src/lib/model/custom-report/custom-report-folder/custom-report-folder.model';
import {
  ChartColumnType,
  ChartDataType,
  ChartDefinitionModel,
  ChartOperation,
  ChartScopeType,
  MeasureColumn,
} from '../../../projects/sqtm-core/src/lib/model/custom-report/chart-definition.model';
import { CustomDashboardModel } from '../../../projects/sqtm-core/src/lib/model/custom-report/custom-dashboard.model';
import { EntityType } from '../../../projects/sqtm-core/src/lib/model/entity.model';
import { DataRowOpenState } from '../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { mockTreeNode } from './grid.data-mock';

export function mockCustomReportLibraryModel(
  customData: Partial<CustomReportLibraryModel> = {},
): CustomReportLibraryModel {
  return combineWithDefaultData(
    {
      id: 1,
      projectId: 1,
      name: '',
      attachmentList: mockEmptyAttachmentListModel(),
      customFieldValues: [],
      description: '',
    },
    customData,
  );
}

export function mockCustomReportFolderModel(
  customData: Partial<CustomReportFolderModel> = {},
): CustomReportFolderModel {
  const defaultData: CustomReportFolderModel = {
    id: 1,
    projectId: 1,
    attachmentList: mockEmptyAttachmentListModel(),
    customFieldValues: [],
    name: '',
    description: 'generic description',
  };

  return combineWithDefaultData(defaultData, customData);
}

export function mockChartDefinitionModel(
  customData: Partial<ChartDefinitionModel> = {},
): ChartDefinitionModel {
  const measureColumn: MeasureColumn = {
    operation: ChartOperation.EQUALS,
    column: {
      id: 1,
      label: 'some column',
      dataType: ChartDataType.AUTOMATED_TEST_TECHNOLOGY,
      columnType: ChartColumnType.ATTRIBUTE,
      specializedType: {
        entityType: EntityType.TEST_CASE,
      },
    },
    label: 'some axis',
  };

  const defaultData: ChartDefinitionModel = {
    id: 1,
    name: 'chart',
    createdOn: new Date().toISOString(),
    customReportLibraryNodeId: 1,
    projectId: 1,
    type: null,
    measures: [measureColumn],
    axis: [measureColumn],
    filters: [],
    abscissa: [],
    scopeType: ChartScopeType.DEFAULT,
    scope: [],
    projectScope: ['1'],
    series: {},
    createdBy: 'admin',
    lastModifiedOn: new Date().toISOString(),
    lastModifiedBy: 'admin',
    colours: [],
  };
  return combineWithDefaultData(defaultData, customData);
}

export function mockCustomReportDashboardModel(
  customData: Partial<CustomDashboardModel> = {},
): CustomDashboardModel {
  const defaultData: CustomDashboardModel = {
    id: 1,
    projectId: 1,
    customReportLibraryNodeId: 1,
    createdBy: '',
    name: 'dashboard',
    chartBindings: [],
    reportBindings: [],
    favoriteWorkspaces: [],
  };
  return combineWithDefaultData(defaultData, customData);
}

export function getSimpleCustomReportLibraryChildNodes() {
  return [
    mockTreeNode({
      id: 'CustomReportLibrary-1',
      children: ['CustomReportFolder-2', 'ChartDefinition-3', 'ReportDefinition-4'],
      data: { NAME: 'International Space Station', CHILD_COUNT: '3' },
      state: DataRowOpenState.open,
    }),
    mockTreeNode({
      id: 'CustomReportFolder-2',
      children: [],
      parentRowId: 'CustomReportLibrary-1',
      data: { NAME: 'Structural Requirements Reports' },
      state: DataRowOpenState.closed,
    }),
    mockTreeNode({
      id: 'ChartDefinition-3',
      children: [],
      parentRowId: 'CustomReportLibrary-1',
      state: DataRowOpenState.leaf,
      data: {
        CRLN_ID: 3,
        CHILD_COUNT: 0,
        NAME: 'Financial Breakdown',
      },
    }),
    mockTreeNode({
      id: 'ReportDefinition-4',
      children: [],
      parentRowId: 'CustomReportLibrary-1',
      state: DataRowOpenState.leaf,
      data: {
        CRLN_ID: 4,
        CHILD_COUNT: 0,
        NAME: 'Reg Report',
      },
    }),
  ];
}
