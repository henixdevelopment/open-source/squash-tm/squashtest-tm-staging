import { ProjectPermission } from '../../../projects/sqtm-core/src/lib/model/user/user.model';

export function mockProjectPermissions(
  permissions: { projectId: number; qualifiedName: string }[],
): ProjectPermission[] {
  const projectPermissions: ProjectPermission[] = [];
  permissions.forEach((permission: { projectId: number; qualifiedName: string }) => {
    projectPermissions.push({
      projectId: permission.projectId,
      projectName: null,
      permissionGroup: {
        id: null,
        qualifiedName: permission.qualifiedName,
        active: true,
        system: true,
      },
    });
  });
  return projectPermissions;
}
