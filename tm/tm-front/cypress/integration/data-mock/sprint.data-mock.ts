import { SprintModel } from '../../../projects/sqtm-core/src/lib/model/campaign/sprint-model';
import { SprintStatus } from '../../../projects/sqtm-core/src/lib/model/level-enums/level-enum';
import { combineWithDefaultData } from './data-mocks.utils';
import { mockEmptyAttachmentListModel } from './generic-entity.data-mock';
import { SynchronizationPluginId } from '../../../projects/sqtm-core/src/lib/model/system/system-view.model';

export function mockSprintModel(customData?: Partial<SprintModel>): SprintModel {
  const defaultData: SprintModel = {
    id: 11,
    projectId: 1,
    name: 'Sprint 1',
    reference: 'REF01',
    createdOn: '2024-02-14 15:00',
    createdBy: 'squashUser',
    lastModifiedOn: '2024-02-15 15:00',
    lastModifiedBy: 'squashAdmin',
    startDate: '2024-03-04',
    endDate: '2024-03-15',
    description: 'This is my description',
    nbRequirements: 0,
    attachmentList: mockEmptyAttachmentListModel(),
    customFieldValues: [],
    hasDataSet: false,
    sprintReqVersions: [],
    nbSprintReqVersions: 0,
    status: SprintStatus.UPCOMING.id,
    nbIssues: 0,
    nbTestPlanItems: 0,
    synchronisationKind: null,
    assignableUsers: null,
  };

  return combineWithDefaultData(defaultData, customData);
}

export function mockSprintModelWithRemoteState() {
  return {
    ...mockSprintModel(),
    remoteState: SprintStatus.OPEN.id,
    synchronisationKind: SynchronizationPluginId.XSQUASH4GITLAB,
  };
}
