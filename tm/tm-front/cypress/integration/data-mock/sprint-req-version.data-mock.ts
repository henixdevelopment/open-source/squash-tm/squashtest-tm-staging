import { combineWithDefaultData } from './data-mocks.utils';
import { SprintReqVersionModel } from '../../../projects/sqtm-core/src/lib/model/campaign/sprint-req-version-model';
import { mockEmptyAttachmentListModel } from './generic-entity.data-mock';
import {
  ManagementMode,
  RemotePerimeterStatus,
} from '../../../projects/sqtm-core/src/lib/model/campaign/sprint-model';
import { SprintReqVersionViewModel } from '../../../projects/sqtm-core/src/lib/model/campaign/sprint-req-version-view-model';

const defaultDtoData: SprintReqVersionModel = {
  id: 11,
  versionId: 1,
  projectId: 1,
  requirementId: 1,
  categoryId: 1,
  name: 'Requirement 01',
  reference: 'REF1',
  criticality: 'CRITICAL',
  status: 'WORK_IN_PROGRESS',
  validationStatus: 'TO_BE_TESTED',
  attachmentList: mockEmptyAttachmentListModel(),
  customFieldValues: [],
  requirementVersionProjectId: 1,
  description: 'une description',
  categoryLabel: null,
  requirementVersionProjectName: 'Project1',
  testPlanId: 1,
  remotePerimeterStatus: RemotePerimeterStatus.UNKNOWN,
  mode: ManagementMode.NATIVE,
  nbIssues: 0,
  createdBy: 'admin',
  createdOn: new Date('2024-07-20T00:00:00Z').toISOString(),
  lastModifiedBy: 'admin',
  lastModifiedOn: new Date('2024-07-31T00:00:00Z').toISOString(),
  nbExecutions: 0,
  nbTests: 3,
  remoteReqUrl: '',
  remoteReqState: '',
};

export function mockSprintReqVersionModel(
  customData?: Partial<SprintReqVersionModel>,
): SprintReqVersionModel {
  return combineWithDefaultData(defaultDtoData, customData);
}

export function mockSprintReqVersionViewModel(
  customData?: Partial<SprintReqVersionViewModel>,
): SprintReqVersionViewModel {
  return combineWithDefaultData({
    ...defaultDtoData,
    assignableUsers: [],
    sprintStatus: 'UPCOMING',
    ...customData,
  });
}
