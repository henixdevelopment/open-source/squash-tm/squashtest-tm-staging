import { combineWithDefaultData } from './data-mocks.utils';
import {
  ReportDefinitionParameterTestCase,
  ReportDefinitionViewModel,
  ReportInputType,
} from '../../../projects/sqtm-core/src/lib/model/custom-report/report-definition.model';

export function mockReportDefinitionViewModel(
  customData: Partial<ReportDefinitionViewModel> = {},
): ReportDefinitionViewModel {
  return combineWithDefaultData(
    {
      id: 1,
      name: '',
      summary: '',
      description: '',
      pluginNamespace: '',
      parameters: [],
      missingPlugin: false,
      reportLabel: '',
      attributes: {},
      customReportLibraryNodeId: 1,
      projectId: 1,
    },
    customData,
  );
}

export function mockReportDefinitionParameterTestCase(
  customData: Partial<ReportDefinitionParameterTestCase> = {},
): ReportDefinitionParameterTestCase {
  const defaultData: ReportDefinitionParameterTestCase = {
    projectIds: { type: ReportInputType.PROJECT_PICKER, val: [1] },
    testcasesIds: { type: ReportInputType.TREE_PICKER, val: [] },
    testcasesSelectionMode: {
      type: ReportInputType.RADIO_BUTTONS_GROUP,
      val: ReportInputType.PROJECT_PICKER,
    },
    reportOptions: { type: ReportInputType.CHECKBOXES_GROUP, val: [] },
  };
  return combineWithDefaultData(defaultData, customData);
}
