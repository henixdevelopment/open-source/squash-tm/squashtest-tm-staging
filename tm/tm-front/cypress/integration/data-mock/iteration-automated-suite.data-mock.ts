import { IterationViewPage } from '../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import {
  ALL_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder,
} from '../utils/referential/referential-data-builder';
import { GridResponse } from '../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { mockDataRow, mockGridResponse, mockTreeNode } from './grid.data-mock';
import {
  DataRowModel,
  DataRowOpenState,
} from '../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { CampaignWorkspacePage } from '../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { IterationModel } from '../../../projects/sqtm-core/src/lib/model/campaign/iteration-model';
import {
  getEmptyExecutionEnvironmentsCount,
  getEmptyIterationStatisticsBundle,
  mockIterationModel,
} from './iteration.data-mock';
import { HttpMockBuilder } from '../utils/mocks/request-mock';
import { TestExecutionInfo } from '../../../projects/sqtm-core/src/lib/model/execution/test-execution-info.model';

export function navigateToIteration(
  items: any[] = [],
  isPremium: boolean = false,
): IterationViewPage {
  let refData;

  if (isPremium) {
    refData = new ReferentialDataMockBuilder()
      .withProjects({
        allowAutomationWorkflow: true,
        permissions: ALL_PROJECT_PERMISSIONS,
      })
      .withPremiumLicense()
      .build();
  } else {
    refData = new ReferentialDataMockBuilder()
      .withProjects({
        allowAutomationWorkflow: true,
        permissions: ALL_PROJECT_PERMISSIONS,
      })
      .build();
  }

  const initialNodes: GridResponse = mockGridResponse('id', [
    mockTreeNode({
      id: 'CampaignLibrary-1',
      children: ['Campaign-3'],
      data: { NAME: 'Project1', CHILD_COUNT: 1 },
      state: DataRowOpenState.open,
    }),
    mockTreeNode({
      id: 'Campaign-3',
      children: ['Iteration-1'],
      projectId: 1,
      parentRowId: 'CampaignLibrary-1',
      state: DataRowOpenState.open,
      data: { NAME: 'campaign3', CHILD_COUNT: 1 },
    }),
    mockTreeNode({
      id: 'Iteration-1',
      children: [],
      projectId: 1,
      parentRowId: 'Campaign-3',
      data: { NAME: 'iteration-1', CHILD_COUNT: 0 },
    }),
  ]);
  const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes, refData);

  const model: IterationModel = mockIterationModel({
    id: 1,
    projectId: 1,
    name: 'iteration-1',
    itpi: items,
    iterationStatus: 'PLANNED',
    uuid: 'b368',
    testPlanStatistics: {
      status: 'DONE',
      progression: 100,
      nbTestCases: 3,
      nbDone: 3,
      nbReady: 0,
      nbRunning: 0,
      nbUntestable: 0,
      nbBlocked: 0,
      nbFailure: 0,
      nbSettled: 0,
      nbSuccess: 0,
    },
    hasDatasets: true,
    users: [
      { id: 1, login: 'raowl', firstName: 'Ra', lastName: 'Oul' },
      { id: 2, login: 'jawny', firstName: 'Joe', lastName: 'Ni' },
    ],
    testSuites: [
      { id: 1, name: 'suite01' },
      { id: 2, name: 'suite02' },
    ],
  });
  new HttpMockBuilder(`iteration-view/${model.id}/statistics`)
    .post()
    .responseBody(getEmptyIterationStatisticsBundle())
    .build();

  new HttpMockBuilder(`test-automation/iteration/*/automated-execution-environments-statuses-count`)
    .get()
    .responseBody(getEmptyExecutionEnvironmentsCount())
    .build();

  return campaignWorkspacePage.tree.selectNode<IterationViewPage>('Iteration-1', model);
}

export function mockAutomatedSuiteDataRow(
  suiteId: string,
  createdOn: Date,
  customData?: any,
): DataRowModel {
  return mockDataRow({
    id: suiteId,
    projectId: 1,
    data: {
      suiteId: suiteId,
      createdBy: 'admin',
      createdOn: createdOn,
      lastModifiedOn: null,
      executionStatus: 'SUCCESS',
      hasExecution: false,
      hasResultUrl: false,
      ...customData,
    },
    allowMoves: true,
  });
}
export function mockTestPlan(dataRows: DataRowModel[]): GridResponse {
  return mockGridResponse('suiteId', dataRows);
}

export function mockExecutionComparison(): TestExecutionInfo[] {
  return [
    {
      testId: 1,
      testName: 'First test',
      dataset: null,
      statusBySuite: {
        az47: 'SUCCESS',
        ad45az: 'FAILURE',
      },
    },
    {
      testId: 2,
      testName: 'Second test',
      dataset: null,
      statusBySuite: {
        az47: 'SUCCESS',
        ad45az: 'BLOCKED',
      },
    },
  ];
}

export function mockExecutions(dataRows: DataRowModel[]): GridResponse {
  return mockGridResponse('executionId', dataRows);
}

export function mockExecutionDataRow(
  executionId: number,
  extenderId: number,
  status: string,
  customData?: any,
): DataRowModel {
  return mockDataRow({
    id: executionId,
    projectId: 1,
    data: {
      executionId: executionId,
      extenderId: extenderId,
      executionStatus: status,
      ...customData,
    },
  });
}
