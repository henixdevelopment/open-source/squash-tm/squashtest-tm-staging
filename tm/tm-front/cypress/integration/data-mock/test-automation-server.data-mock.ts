import {
  TestAutomationServer,
  TestAutomationServerKind,
} from '../../../projects/sqtm-core/src/lib/model/test-automation/test-automation-server.model';
import { combineWithDefaultData } from './data-mocks.utils';
import { AuthenticationProtocol } from '../../../projects/sqtm-core/src/lib/model/third-party-server/authentication.model';

export function mockJenkinsTestAutomationServer(
  customData?: Partial<TestAutomationServer>,
): TestAutomationServer {
  return combineWithDefaultData(
    {
      id: 1,
      name: 'Server',
      kind: TestAutomationServerKind.jenkins,
      baseUrl: 'http://localhost:8080',
      description: '',
      createdOn: '2021-09-01T00:00:00.000Z',
      createdBy: 'cypress',
      lastModifiedBy: 'cypress',
      lastModifiedOn: '2021-09-01T00:00:00.000Z',
      authProtocol: AuthenticationProtocol.BASIC_AUTH,
      environmentTags: [],
      availableEnvironments: [],
      availableEnvironmentTags: [],
      manualSlaveSelection: false,
      observerUrl: '',
      eventBusUrl: '',
      killSwitchUrl: '',
      supportsAutomatedExecutionEnvironments: false,
    },
    customData,
  );
}
