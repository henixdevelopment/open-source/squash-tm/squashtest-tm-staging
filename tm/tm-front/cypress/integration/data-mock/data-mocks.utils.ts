export function combineWithDefaultData<T>(defaultData: T, customData?: Partial<T>): T {
  return {
    ...defaultData,
    ...(customData || {}),
  };
}
