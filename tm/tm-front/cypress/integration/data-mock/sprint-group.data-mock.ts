import { combineWithDefaultData } from './data-mocks.utils';
import { SprintGroupModel } from '../../../projects/sqtm-core/src/lib/model/campaign/sprint-group-model';

export function mockSprintGroupModel(customData?: Partial<SprintGroupModel>): SprintGroupModel {
  const defaultData: SprintGroupModel = {
    id: 11,
    projectId: 1,
    name: 'SprintGroup 1',
    createdOn: '2024-02-14 15:00',
    createdBy: 'squashUser',
    lastModifiedOn: '2024-02-15 15:00',
    lastModifiedBy: 'squashAdmin',
    description: 'This is my description',
    attachmentList: {
      id: 1,
      attachments: [],
    },
    customFieldValues: [],
  };

  return combineWithDefaultData(defaultData, customData);
}
