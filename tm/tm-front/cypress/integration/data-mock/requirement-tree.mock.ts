import { DataRowOpenState } from '../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { mockTreeNode } from './grid.data-mock';

export function getSimpleRequirementLibraryChildNodes() {
  return [
    mockTreeNode({
      id: 'RequirementLibrary-1',
      children: ['RequirementFolder-1', 'Requirement-3', 'RequirementFolder-2'],
      data: { NAME: 'International Space Station', CHILD_COUNT: '3' },
      state: DataRowOpenState.open,
      projectId: 1,
    }),
    mockTreeNode({
      id: 'RequirementFolder-1',
      children: [],
      parentRowId: 'RequirementLibrary-1',
      data: { NAME: 'Structural Requirements' },
      state: DataRowOpenState.closed,
      projectId: 1,
    }),
    mockTreeNode({
      id: 'Requirement-3',
      children: [],
      parentRowId: 'RequirementLibrary-1',
      state: DataRowOpenState.leaf,
      projectId: 1,
      data: {
        RLN_ID: 3,
        CHILD_COUNT: 0,
        NAME: "Find lot's of bucks",
        CRITICALITY: 'CRITICAL',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        HAS_DESCRIPTION: true,
        REQ_CATEGORY_ICON: 'briefcase',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
        REQ_CATEGORY_TYPE: 'SYS',
        COVERAGE_COUNT: 0,
        IS_SYNCHRONIZED: false,
      },
    }),
    mockTreeNode({
      id: 'RequirementFolder-2',
      children: [],
      parentRowId: 'RequirementLibrary-1',
      data: { NAME: 'Functional Requirements' },
      state: DataRowOpenState.closed,
      projectId: 1,
    }),
  ];
}
