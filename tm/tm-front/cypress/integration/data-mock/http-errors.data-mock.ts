import {
  FieldValidationError,
  SquashFieldError,
} from '../../../projects/sqtm-core/src/lib/model/error/error.model';

export function mockFieldValidationErrors(fieldValidationErrors: FieldValidationError[]): {
  squashTMError: SquashFieldError;
} {
  return {
    squashTMError: {
      kind: 'FIELD_VALIDATION_ERROR',
      fieldValidationErrors,
    },
  };
}

export function mockFieldValidationError(
  fieldName: string,
  i18nKey: string,
): { squashTMError: SquashFieldError } {
  return mockFieldValidationErrors([{ fieldName, i18nKey } as FieldValidationError]);
}
