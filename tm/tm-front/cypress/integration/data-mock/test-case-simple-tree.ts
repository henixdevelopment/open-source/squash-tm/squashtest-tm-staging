import { GridResponse } from '../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { DataRowOpenState } from '../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { mockTreeNode } from './grid.data-mock';

export const initialTestCaseLibraries: GridResponse = {
  count: 1,
  dataRows: [
    mockTreeNode({
      id: 'TestCaseLibrary-1',
      children: [],
      data: { NAME: 'Project1' },
      state: DataRowOpenState.closed,
    }),
  ],
};

export const projectOneChildren = [
  mockTreeNode({
    id: 'TestCaseLibrary-1',
    children: ['TestCaseFolder-1', 'TestCase-3', 'TestCaseFolder-2'],
    data: { NAME: 'Project1', CHILD_COUNT: 3 },
    projectId: 1,
    state: DataRowOpenState.open,
  }),
  mockTreeNode({
    id: 'TestCaseFolder-1',
    children: [],
    data: { NAME: 'folder1' },
    parentRowId: 'TestCaseLibrary-1',
    projectId: 1,
  }),
  mockTreeNode({
    id: 'TestCase-3',
    children: [],
    data: {
      NAME: 'a nice test',
      CHILD_COUNT: 0,
      TC_STATUS: 'APPROVED',
      TC_KIND: 'STANDARD',
      IMPORTANCE: 'HIGH',
    },
    parentRowId: 'TestCaseLibrary-1',
    projectId: 1,
  }),
  mockTreeNode({
    id: 'TestCaseFolder-2',
    children: [],
    data: { NAME: 'folder2' },
    parentRowId: 'TestCaseLibrary-1',
    projectId: 1,
  }),
];
