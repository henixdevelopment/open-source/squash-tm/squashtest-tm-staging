import { mockEmptyAttachmentListModel } from './generic-entity.data-mock';
import { combineWithDefaultData } from './data-mocks.utils';
import {
  ActionStepExecViewModel,
  ModifDuringExecModel,
} from '../../../projects/sqtm-core/src/lib/model/modif-during-exec/modif-during-exec.model';

export function mockModifDuringExecModel(
  customData: Partial<ModifDuringExecModel> = {},
): ModifDuringExecModel {
  const defaultData: ModifDuringExecModel = {
    executionStepActionTestStepPairs: [],
  };

  return combineWithDefaultData(defaultData, customData);
}

export function mockActionStepExecViewModel(
  customData: Partial<ActionStepExecViewModel> = {},
): ActionStepExecViewModel {
  const defaultData: ActionStepExecViewModel = {
    id: 1,
    projectId: 1,
    attachmentList: mockEmptyAttachmentListModel(),
    customFieldValues: [],
    actionStepTestCaseId: 1,
    actionStepTestCaseName: 'tc-1',
    actionStepTestCaseReference: 'ref',
    executionTestCaseId: 1,
    executionTestCaseName: 'tc-1',
    executionTestCaseReference: 'ref',
    action: 'action',
    expectedResult: 'result',
    coverages: [],
  };

  return combineWithDefaultData(defaultData, customData);
}
