import {
  selectByDataTestLinkId,
  selectByDataTestMenuId,
  selectByDataTestMenuItemId,
} from './basic-selectors';

export class MenuElement {
  constructor(private menuId: string) {}

  assertExists() {
    cy.get(this.buildSelector()).should('exist');
  }

  buildSelector(): string {
    return `
    ${selectByDataTestMenuId(this.menuId)}
    `;
  }

  item(itemId: string): MenuItemElement {
    return new MenuItemElement(this.buildSelector(), itemId);
  }

  hide() {
    cy.get('body')
      .type('{esc}')
      .then(() => {
        cy.get(this.buildSelector()).should('not.exist');
      });
  }
}

export class MenuItemElement {
  constructor(
    private rootSelector: string,
    private itemId: string,
  ) {}

  buildSelector(): string {
    return `
    ${this.rootSelector}
    ${selectByDataTestMenuItemId(this.itemId)}
    `;
  }

  assertExists() {
    cy.get(this.buildSelector()).should('exist');
  }

  assertNotExist() {
    cy.get(this.buildSelector()).should('not.exist');
  }

  assertDisabled() {
    cy.get(this.buildSelector()).should('have.class', 'ant-dropdown-menu-item-disabled');
  }

  assertButtonIsDisabled() {
    cy.get(this.buildSelector()).should('have.attr', 'disabled');
  }

  assertButtonIsActive() {
    cy.get(this.buildSelector()).should('not.have.attr', 'disabled');
  }

  assertEnabled() {
    cy.get(this.buildSelector()).should('not.have.class', 'ant-dropdown-menu-item-disabled');
  }

  assertIsChecked() {
    cy.get(this.buildSelector())
      .find('span.ant-checkbox')
      .should('have.class', 'ant-checkbox-checked');
  }

  assertIsNotChecked() {
    cy.get(this.buildSelector())
      .find('span.ant-checkbox')
      .should('not.have.class', 'ant-checkbox-checked');
  }

  click() {
    cy.get(this.buildSelector()).click();
  }

  assertContains(value: string) {
    cy.get(this.buildSelector()).should('contain', value);
  }

  selectLinkElement(value: string) {
    cy.get(this.buildSelector()).find(selectByDataTestLinkId(value)).click();
  }
}
