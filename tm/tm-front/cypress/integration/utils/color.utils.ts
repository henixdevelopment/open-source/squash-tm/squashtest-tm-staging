export enum WorkspaceColor {
  REQUIREMENT = 'rgb(0, 120, 174)',
  TEST_CASE = 'rgb(3, 127, 76)',
  CAMPAIGN = 'rgb(117, 26, 163)',
}

export enum ExecutionStatusColor {
  READY_OR_NOT_RUN = 'rgb(163, 178, 184)',
  RUNNING = 'rgb(0, 120, 174)',
  SUCCESS = 'rgb(0, 111, 87)',
  FAILURE = 'rgb(203, 21, 36)',
  ERROR_OR_BLOCKED = 'rgb(255, 204, 0)',
  NOT_FOUND_OR_UNTESTABLE = 'rgb(61, 61, 61)',
  SETTLED_OR_WARNING = 'rgb(5, 191, 113)',
}

export enum TestCaseChartColor {
  NEVER_EXECUTED = 'rgb(249, 199, 79)',
  EXECUTED = 'rgb(39, 125, 161)',
}

export function getRgbColorFromHex(color: string): string | null {
  const hexRegex: RegExp = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i;
  const result: RegExpExecArray = hexRegex.exec(color);
  if (!result) {
    return null;
  }
  const [, red, green, blue]: number[] = result.map((str) => parseInt(str, 16));
  return `rgb(${red}, ${green}, ${blue})`;
}
