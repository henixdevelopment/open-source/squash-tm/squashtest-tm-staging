import { restApiBaseUrl } from '../../../mocks/request-mock';
import { makeApiRequest } from '../api.utils';

export function deleteTestCase(testCaseId: number) {
  const deleteTestCase: string = `${restApiBaseUrl()}/test-cases/${testCaseId}`;
  makeApiRequest('DELETE', deleteTestCase);
}
