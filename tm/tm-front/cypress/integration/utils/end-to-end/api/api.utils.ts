// If you need to make API calls, ensure you run the tests on the backend port (e.g., 8080).

import Chainable = Cypress.Chainable;

export function makeApiRequest(method: 'GET' | 'POST' | 'DELETE', url: string): void {
  cy.request({
    method: method,
    url: url,
    headers: {
      Authorization: 'Basic YWRtaW46YWRtaW4=',
    },
  }).then((response): void => {
    expect(response.status).to.eq(200);
  });
}

export function makeApiRequestWithToken(
  method: 'GET' | 'POST',
  token: string,
  url: string,
  body: object,
): Chainable<Cypress.Response<any>> {
  return cy.request({
    method: method,
    url: url,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    body: body,
  });
}
