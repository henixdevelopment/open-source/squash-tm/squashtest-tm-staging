import { restApiBaseUrl } from '../../../mocks/request-mock';
import { makeApiRequest } from '../api.utils';

export function addVersionToRequirement(
  requirementId: number,
  requirementLink: boolean,
  testCaseRequirementLink: boolean,
) {
  const versionRequirement: string = `${restApiBaseUrl()}/requirement-versions/${requirementId}?req_link=${requirementLink}&tc_req_link=${testCaseRequirementLink}`;
  makeApiRequest('POST', versionRequirement);
}
