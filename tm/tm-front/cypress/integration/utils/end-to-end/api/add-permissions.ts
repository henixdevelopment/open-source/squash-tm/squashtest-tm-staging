import { restApiBaseUrl } from '../../mocks/request-mock';
import { makeApiRequest } from './api.utils';

export enum ApiPermissionGroup {
  ADVANCED_TESTER = 'advanced_tester',
  PROJECT_MANAGER = 'project_manager',
  PROJECT_VIEWER = 'project_viewer',
  TEST_DESIGNER = 'test_designer',
  TEST_EDITOR = 'test_editor',
  TEST_RUNNER = 'test_runner',
  VALIDATOR = 'validator',
  AUTOMATED_TEST_WRITER = 'automated_test_writer',
}

export enum SystemProfile {
  TEST_EDITOR = 2,
  PROJECT_VIEWER = 4,
  PROJECT_MANAGER = 5,
  TEST_RUNNER = 6,
  TEST_DESIGNER = 7,
  ADVANCED_TESTER = 8,
  VALIDATOR = 9,
  AUTOMATED_TEST_WRITER = 10,
}

export const FIRST_CUSTOM_PROFILE_ID = 11;

// Handle 403 errors caused by test order when project permissions are added via database queries (builder)
// To address project permission issues, we initiate an API call for resolution
// Note: Using IDs for now is an acceptable solution
export function addPermissionToProject(
  projectId: number,
  permissionGroup: ApiPermissionGroup,
  userIds: number[],
): void {
  const permissionApiEndPoint: string = `${restApiBaseUrl()}/projects/${projectId}/permissions/${permissionGroup}?ids=${userIds.join(',')}`;
  makeApiRequest('POST', permissionApiEndPoint);
}

export function addClearanceToProject(
  projectId: number,
  profileId: number,
  userIds: number[],
): void {
  const permissionApiEndPoint: string = `${restApiBaseUrl()}/projects/${projectId}/clearances/${profileId}/users/${userIds.join(',')}`;
  makeApiRequest('POST', permissionApiEndPoint);
}

export function addPermissionToUser(
  userId: number,
  permissionGroup: ApiPermissionGroup,
  projectIds: number[],
): void {
  const permissionApiEndPoint: string = `${restApiBaseUrl()}/users/${userId}/permissions/${permissionGroup}?ids=${projectIds.join(',')}`;
  makeApiRequest('POST', permissionApiEndPoint);
}

export function addClearanceToUser(userId: number, profileId: number, projectIds: number[]): void {
  const permissionApiEndPoint: string = `${restApiBaseUrl()}/users/${userId}/clearances/${profileId}/projects/${projectIds.join(',')}`;
  makeApiRequest('POST', permissionApiEndPoint);
}
