import { format } from 'date-fns';
import { fr } from 'date-fns/locale';

export const DEFAULT_USER_LOGIN = 'admin';
export const DEFAULT_USER_PASSWORD =
  '{bcrypt}$2a$10$0C8v8E98KGmDGCoOOEGqje5YLzdMtgIhrwwofaJ.AuuZQDGZgIvme';
export const DEFAULT_ENCRYPTED_CREDENTIALS =
  '2K6BsF/Fg68W7+2pYTzyPYU0bFdKGIS3xMWIha2tM5gQ0tqwVQddq0ZsBbQ+7Hr19Noy2vUOe/ZCdpvChd+tH+t1XNru4RczlXsyQva/GpqB/4J3WfI0FDVPy3AYRVwv08KhiCRmI1w+LggWU/gzIv0w8Je8vVMRcBQmCHxM7MULRiGlMnf7yVyylseiXtAgXtJCexiJPuA=';
export const DEFAULT_SCRIPTED_TC_SCRIPT = '# language: fr Fonctionnalité:';
export const AclClass = {
  1: 'org.squashtest.tm.domain.project.Project',
  2: 'org.squashtest.tm.domain.requirement.RequirementLibrary',
  3: 'org.squashtest.tm.domain.testcase.TestCaseLibrary',
  4: 'org.squashtest.tm.domain.campaign.CampaignLibrary',
  5: 'org.squashtest.tm.domain.project.ProjectTemplate',
  6: 'org.squashtest.tm.domain.customreport.CustomReportLibrary',
  7: 'org.squashtest.tm.domain.tf.automationrequest.AutomationRequestLibrary',
  8: 'org.squashtest.tm.domain.actionword.ActionWordLibrary',
};
export type AclGroupName =
  | 'TestEditor'
  | 'ProjectViewer'
  | 'ProjectManager'
  | 'TestRunner'
  | 'TestDesigner'
  | 'AdvanceTester'
  | 'Validator'
  | 'AutomatedTestWriter';
export const AclGroup: { [K in AclGroupName]: number } = {
  TestEditor: 2,
  ProjectViewer: 4,
  ProjectManager: 5,
  TestRunner: 6,
  TestDesigner: 7,
  AdvanceTester: 8,
  Validator: 9,
  AutomatedTestWriter: 10,
};

export function getProjectAclClasses() {
  return Object.entries(AclClass).filter(
    (entry) => entry[1] !== 'org.squashtest.tm.domain.project.ProjectTemplate',
  );
}

export function newDateAsString(): string {
  return formatDate(new Date());
}

export function formatDate(date: Date | string): string {
  const dateToFormat: Date = typeof date === 'string' ? new Date(date) : date;
  return format(dateToFormat, 'yyyy-MM-dd HH:mm:ss', { locale: fr });
}

export function formatString(value: string): string {
  return value ? `'${value}'` : null;
}
