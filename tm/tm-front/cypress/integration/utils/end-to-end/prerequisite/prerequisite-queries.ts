import {
  MilestoneStatusKeys,
  RequirementCriticalityKeys,
  RequirementStatusKeys,
  TestCaseExecutionModeKeys,
  TestCaseImportanceKeys,
  TestCaseStatusKeys,
} from '../../../../../projects/sqtm-core/src/lib/model/level-enums/level-enum';
import {
  AuthenticationPolicy,
  AuthenticationProtocol,
} from '../../../../../projects/sqtm-core/src/lib/model/third-party-server/authentication.model';
import { BindableEntity } from '../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { DatabaseUtils } from '../../database.utils';
import { DEFAULT_USER_LOGIN, newDateAsString } from './prerequisite-helper';
import { CustomFieldType } from './builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import {
  ChartOperation,
  ChartScopeType,
  ChartType,
} from '../../../../../projects/sqtm-core/src/lib/model/custom-report/chart-definition.model';
import { PluginNameSpace } from '../../../../../projects/sqtm-core/src/lib/model/custom-report/report-definition.model';
import { EntityType } from '../../../../../projects/sqtm-core/src/lib/model/entity.model';
import { CustomExportEntityType } from '../../../../../projects/sqtm-core/src/lib/model/custom-report/custom-export-columns.model';
import { CustomColumnEntityType } from './builders/custom-report-prerequisite-builders';
import { isPostgresql } from '../../../../utils/testing-context'; // **************

// **************
// * ATTACHMENT *
// **************
export function insertAttachmentList(): string {
  return `INSERT INTO ATTACHMENT_LIST (ATTACHMENT_LIST_ID)
          SELECT COALESCE(MAX(ATTACHMENT_LIST_ID), 0) + 1
          FROM ATTACHMENT_LIST`;
}

// ************
// * CAMPAIGN *
// ************
export function insertCampaignLibraryContent(index: number): string {
  return `INSERT INTO CAMPAIGN_LIBRARY_CONTENT (LIBRARY_ID, CONTENT_ID, CONTENT_ORDER)
          SELECT MAX(CL_ID),
                 (SELECT MAX(CLN_ID) FROM CAMPAIGN_LIBRARY_NODE),
                 '${index}'
          FROM CAMPAIGN_LIBRARY`;
}

export function insertCampaignLibraryNode(
  name: string,
  description: string,
  createdBy: string,
  createdOn: string,
  lastModifiedByStr: string,
  lastModifiedOnStr: string,
): string {
  return `INSERT INTO CAMPAIGN_LIBRARY_NODE (CLN_ID, NAME, DESCRIPTION, CREATED_BY, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON, PROJECT_ID, ATTACHMENT_LIST_ID)
          SELECT COALESCE(MAX(CLN_ID), 0) + 1,
                 '${name}',
                 '${description}',
                 '${createdBy}',
                 '${createdOn}',
                 ${lastModifiedByStr},
                 ${lastModifiedOnStr},
                 (SELECT MAX(PROJECT_ID) FROM PROJECT),
                 (SELECT MAX(ATTACHMENT_LIST_ID) FROM ATTACHMENT_LIST)
          FROM CAMPAIGN_LIBRARY_NODE`;
}

export function insertCampaignFolder(): string {
  return `INSERT INTO CAMPAIGN_FOLDER (CLN_ID)
          SELECT MAX(CLN_ID) FROM CAMPAIGN_LIBRARY_NODE`;
}

export function insertCampaign(reference: string): string {
  return `INSERT INTO CAMPAIGN (CLN_ID, REFERENCE, ACTUAL_END_AUTO, ACTUAL_START_AUTO)
          SELECT MAX(CLN_ID),
                 '${reference}',
                 false,
                 false
          FROM CAMPAIGN_LIBRARY_NODE`;
}

export function insertSprintGroup(): string {
  return `INSERT INTO SPRINT_GROUP (CLN_ID, REMOTE_SYNCHRONISATION_ID)
          SELECT MAX(CLN_ID),
                 null
          FROM CAMPAIGN_LIBRARY_NODE`;
}

export function insertSprint(reference: string): string {
  return `INSERT INTO SPRINT (CLN_ID, STATUS, REMOTE_SYNCHRONISATION_ID, REMOTE_SPRINT_ID, REMOTE_NAME, REMOTE_STATE, START_DATE, END_DATE, REFERENCE)
          SELECT MAX(CLN_ID),
                 'UPCOMING',
                 null,
                 null,
                 null,
                 null,
                 null,
                 null,
                 '${reference}'
          FROM CAMPAIGN_LIBRARY_NODE`;
}

export function insertClnRelationship(parentName: string, index: number): string {
  return `INSERT INTO CLN_RELATIONSHIP (DESCENDANT_ID, ANCESTOR_ID, CONTENT_ORDER)
          SELECT MAX(CLN_ID),
                 (SELECT cln.CLN_ID
                  FROM CAMPAIGN_LIBRARY_NODE cln
                  WHERE cln.NAME = '${parentName}'),
                 ${index}
          FROM CAMPAIGN_LIBRARY_NODE`;
}

export function insertCampaignTestPlanItem(
  testCaseName: string,
  userLoginStr: string,
  datasetNameStr: string,
): string {
  return `INSERT INTO CAMPAIGN_TEST_PLAN_ITEM (CTPI_ID, CAMPAIGN_ID, TEST_CASE_ID, TEST_PLAN_ORDER, USER_ID, DATASET_ID)
          SELECT COALESCE(MAX(CTPI_ID), 0) + 1,
                 (SELECT MAX(CLN_ID) FROM CAMPAIGN),
                 (SELECT TCLN_ID FROM TEST_CASE_LIBRARY_NODE WHERE NAME = '${testCaseName}'),
                 (SELECT COALESCE(MAX(TEST_PLAN_ORDER) + 1, 0)
                  FROM CAMPAIGN_TEST_PLAN_ITEM
                  WHERE CAMPAIGN_ID = (SELECT MAX(CLN_ID) FROM CAMPAIGN)),
                 (SELECT PARTY_ID FROM CORE_USER WHERE login = ${userLoginStr}),
                 (SELECT d.DATASET_ID
                  FROM DATASET d
                  WHERE d.NAME = ${datasetNameStr}
                    AND d.TEST_CASE_ID = (SELECT TCLN_ID FROM TEST_CASE_LIBRARY_NODE WHERE NAME = '${testCaseName}'))
          FROM CAMPAIGN_TEST_PLAN_ITEM`;
}

export function insertIterationTestPlanItem(
  testCaseName: string,
  createdBy: string,
  createdOn: string,
  lastModifiedByStr: string,
  lastModifiedOnStr: string,
  userLoginStr: string,
  datasetNameStr: string,
  status: string,
): string {
  return `INSERT INTO ITERATION_TEST_PLAN_ITEM (ITEM_TEST_PLAN_ID, CREATED_BY, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON, TCLN_ID, USER_ID, DATASET_ID, EXECUTION_STATUS)
          SELECT COALESCE(MAX(ITEM_TEST_PLAN_ID), 0) + 1,
                 '${createdBy}',
                 '${createdOn}',
                 ${lastModifiedByStr},
                 ${lastModifiedOnStr},
                 (SELECT TCLN_ID FROM TEST_CASE_LIBRARY_NODE WHERE NAME = '${testCaseName}'),
                 (SELECT PARTY_ID FROM CORE_USER WHERE login = ${userLoginStr}),
                 (SELECT d.DATASET_ID
                  FROM DATASET d
                  WHERE d.NAME = ${datasetNameStr}
                    AND d.TEST_CASE_ID = (SELECT TCLN_ID FROM TEST_CASE_LIBRARY_NODE WHERE NAME = '${testCaseName}')),
                 ${status}
          FROM ITERATION_TEST_PLAN_ITEM`;
}

export function insertSprintTestPlan(sprintName: string): string {
  return `INSERT INTO TEST_PLAN (TEST_PLAN_ID, CL_ID)
          SELECT
            COALESCE(MAX(TEST_PLAN_ID), 0) + 1,
            (SELECT CL_ID FROM PROJECT INNER JOIN CAMPAIGN_LIBRARY_NODE ON CAMPAIGN_LIBRARY_NODE.PROJECT_ID = PROJECT.PROJECT_ID WHERE CAMPAIGN_LIBRARY_NODE.NAME = '${sprintName}')
          FROM TEST_PLAN`;
}

export function insertSprintReqVersion(
  sprintName: string,
  reqVersionName: string,
  createdBy: string,
  createdOn: string,
  lastModifiedByStr: string,
  lastModifiedOnStr: string,
): string {
  return `INSERT INTO SPRINT_REQ_VERSION (SPRINT_REQ_VERSION_ID, REQ_VERSION_ID, SPRINT_ID, REFERENCE, NAME, STATUS, CRITICALITY, CATEGORY, DESCRIPTION, MODE, TEST_PLAN_ID, CREATED_BY, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON, VALIDATION_STATUS)
          SELECT
            COALESCE(MAX(SPRINT_REQ_VERSION_ID), 0) + 1,
            (SELECT RES_ID FROM RESOURCE WHERE RESOURCE.NAME = '${reqVersionName}'),
            (SELECT CLN_ID FROM CAMPAIGN_LIBRARY_NODE WHERE CAMPAIGN_LIBRARY_NODE.NAME = '${sprintName}'),
            null,
            '${reqVersionName}',
            '',
            null,
            null,
            '',
            'MANUAL',
            (SELECT MAX(TEST_PLAN_ID) FROM TEST_PLAN),
            '${createdBy}',
            '${createdOn}',
            ${lastModifiedByStr},
            ${lastModifiedOnStr},
            'TO_BE_TESTED'
          FROM SPRINT_REQ_VERSION`;
}

export function insertSprintTestPlanItem(
  testCaseName: string,
  createdBy: string,
  createdOn: string,
  lastModifiedByStr: string,
  lastModifiedOnStr: string,
  userLoginStr: string,
  datasetNameStr: string,
  status: string,
): string {
  return `INSERT INTO TEST_PLAN_ITEM (TEST_PLAN_ITEM_ID, TEST_PLAN_ID, ITEM_ORDER, EXECUTION_STATUS, LAST_EXECUTED_BY, LAST_EXECUTED_ON, TCLN_ID, DATASET_ID, LABEL, ASSIGNEE_ID, CREATED_BY, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON)
          SELECT COALESCE(MAX(TEST_PLAN_ITEM_ID), 0) + 1,
                 (SELECT TEST_PLAN_ID
                  FROM SPRINT_REQ_VERSION
                         INNER JOIN REQUIREMENT_VERSION_COVERAGE ON REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID = SPRINT_REQ_VERSION.REQ_VERSION_ID
                         INNER JOIN TEST_CASE_LIBRARY_NODE ON TEST_CASE_LIBRARY_NODE.TCLN_ID = REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID
                  WHERE TEST_CASE_LIBRARY_NODE.NAME = '${testCaseName}'),
                 (SELECT COALESCE(MAX(ITEM_ORDER), 0) + 1 FROM TEST_PLAN_ITEM
                                                                 INNER JOIN TEST_CASE_LIBRARY_NODE ON TEST_CASE_LIBRARY_NODE.TCLN_ID = TEST_PLAN_ITEM.TCLN_ID
                  WHERE TEST_CASE_LIBRARY_NODE.NAME = '${testCaseName}'),
                 ${status},
                 null,
                 null,
                 (SELECT TCLN_ID FROM TEST_CASE_LIBRARY_NODE WHERE TEST_CASE_LIBRARY_NODE.NAME = '${testCaseName}'),
                 (SELECT DATASET_ID FROM DATASET WHERE DATASET.NAME = ${datasetNameStr}),
                 '',
                 (SELECT PARTY_ID FROM CORE_USER WHERE login = ${userLoginStr}),
                 '${createdBy}',
                 '${createdOn}',
                 ${lastModifiedByStr},
                 ${lastModifiedOnStr}
          FROM TEST_PLAN_ITEM`;
}

export function insertLastExecutedOnAndByInIterationTestPlanItem(
  lastExecutedBy: string,
  lastExecutedOn: string,
) {
  return `UPDATE ITERATION_TEST_PLAN_ITEM
          SET
            LAST_EXECUTED_BY = '${lastExecutedBy}',
            LAST_EXECUTED_ON = '${lastExecutedOn}',
            LAST_MODIFIED_BY =  '${lastExecutedBy}',
            LAST_MODIFIED_ON = '${lastExecutedOn}'
          WHERE ITEM_TEST_PLAN_ID = (SELECT MAX(ITEM_TEST_PLAN_ID) FROM ITERATION_TEST_PLAN_ITEM)`;
}

export function insertItemTestPlanList(): string {
  return `INSERT INTO ITEM_TEST_PLAN_LIST (ITERATION_ID, ITEM_TEST_PLAN_ID, ITEM_TEST_PLAN_ORDER)
          SELECT MAX(ITERATION_ID),
                 (SELECT MAX(ITEM_TEST_PLAN_ID) FROM ITERATION_TEST_PLAN_ITEM),
                 (SELECT COALESCE (MAX(ITEM_TEST_PLAN_ORDER) + 1, 0)
                  FROM ITEM_TEST_PLAN_LIST
                  WHERE ITERATION_ID = (SELECT MAX(ITERATION_ID) FROM ITERATION))
          FROM ITERATION`;
}

export function insertTestSuite(
  name: string,
  description: string,
  createdBy: string,
  createdOn: string,
  lastModifiedByStr: string,
  lastModifiedOnStr: string,
  status: string,
): string {
  return `INSERT INTO TEST_SUITE (ID, NAME, DESCRIPTION, CREATED_BY, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON, EXECUTION_STATUS, UUID, ATTACHMENT_LIST_ID)
          SELECT COALESCE(MAX(ID), 0) + 1,
                 '${name}',
                 '${description}',
                 '${createdBy}',
                 '${createdOn}',
                 ${lastModifiedByStr},
                 ${lastModifiedOnStr},
                 '${status}',
                 ${DatabaseUtils.createUuidFunction()},
                 (SELECT MAX(ATTACHMENT_LIST_ID) FROM ATTACHMENT_LIST)
          FROM TEST_SUITE`;
}

export function insertIterationTestSuite(): string {
  return `INSERT INTO ITERATION_TEST_SUITE (ITERATION_ID, TEST_SUITE_ID, ITERATION_TEST_SUITE_ORDER)
          SELECT MAX(ITERATION_ID),
                 (SELECT MAX(ID) FROM TEST_SUITE),
                 (SELECT COALESCE(MAX(ITERATION_TEST_SUITE_ORDER) + 1, 0)
                  FROM ITERATION_TEST_SUITE
                  WHERE ITERATION_ID = (SELECT MAX(ITERATION_ID) FROM ITERATION)
                    AND TEST_SUITE_ID = (SELECT MAX(TEST_SUITE_ID) FROM ITERATION_TEST_SUITE))
          FROM ITERATION`;
}

export function insertTestSuiteTestPlanItems(testCaseNames: string[]): string {
  return `INSERT INTO TEST_SUITE_TEST_PLAN_ITEM (TPI_ID, SUITE_ID, TEST_PLAN_ORDER)
          SELECT itpi.ITEM_TEST_PLAN_ID,
                 (SELECT MAX(ID) FROM TEST_SUITE),
                 (ROW_NUMBER() OVER (ORDER BY itpi.ITEM_TEST_PLAN_ID) - 1)
          FROM ITERATION_TEST_PLAN_ITEM itpi
                 JOIN TEST_CASE_LIBRARY_NODE tcln ON itpi.TCLN_ID = tcln.TCLN_ID
          WHERE tcln.NAME IN ('${testCaseNames.join("','")}')`;
}

export function insertIteration(
  name: string,
  reference: string,
  description: string,
  createdBy: string,
  createdOn: string,
  lastModifiedByStr: string,
  lastModifiedOnStr: string,
): string {
  return `INSERT INTO ITERATION (ITERATION_ID, NAME, REFERENCE, DESCRIPTION, CREATED_BY, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON, ACTUAL_END_AUTO, ACTUAL_START_AUTO, UUID, ATTACHMENT_LIST_ID)
          SELECT COALESCE(MAX(ITERATION_ID), 0) + 1,
                 '${name}',
                 '${reference}',
                 '${description}',
                 '${createdBy}',
                 '${createdOn}',
                 ${lastModifiedByStr},
                 ${lastModifiedOnStr},
                 false,
                 false,
                 ${DatabaseUtils.createUuidFunction()},
                 (SELECT MAX(ATTACHMENT_LIST_ID) FROM ATTACHMENT_LIST)
          FROM ITERATION`;
}

export function insertCampaignIteration(): string {
  return `INSERT INTO CAMPAIGN_ITERATION (CAMPAIGN_ID, ITERATION_ID, ITERATION_ORDER)
          SELECT MAX(CLN_ID),
                 (SELECT MAX(ITERATION_ID) FROM ITERATION),
                 (SELECT COALESCE(MAX(ITERATION_ORDER) + 1, 0)
                  FROM CAMPAIGN_ITERATION
                  WHERE CAMPAIGN_ID = (SELECT MAX(CLN_ID) FROM CAMPAIGN)
                    AND ITERATION_ID = (SELECT MAX(ITERATION_ID) FROM CAMPAIGN_ITERATION))
          FROM CAMPAIGN`;
}

export function insertItemTestPlanExecution(index: number): string {
  return `INSERT INTO ITEM_TEST_PLAN_EXECUTION (ITEM_TEST_PLAN_ID, EXECUTION_ID, EXECUTION_ORDER)
          SELECT MAX(ITEM_TEST_PLAN_ID),
                 (SELECT MAX(EXECUTION_ID) FROM EXECUTION),
                 ${index}
          FROM ITERATION_TEST_PLAN_ITEM`;
}

export function insertExecution(
  testCaseName: string,
  createdBy: string,
  createdOn: string,
  lastModifiedByStr: string,
  lastModifiedOnStr: string,
  commentStr: string,
  status: string,
): string {
  return `INSERT INTO EXECUTION (EXECUTION_ID, TCLN_ID, CREATED_BY, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON,
                                 DESCRIPTION, NAME, EXECUTION_MODE, EXECUTION_STATUS, LAST_EXECUTED_BY, LAST_EXECUTED_ON,
                                 ATTACHMENT_LIST_ID, ISSUE_LIST_ID, PREREQUISITE, TC_STATUS, IMPORTANCE, REFERENCE, TC_DESCRIPTION,
                                 DATASET_LABEL, TC_NAT_LABEL, TC_NAT_CODE, TC_NAT_ICON_NAME, TC_TYP_LABEL, TC_TYP_CODE, TC_TYP_ICON_NAME)
          SELECT
            (SELECT COALESCE(MAX(EXECUTION_ID), 0) + 1 FROM EXECUTION),
            tcln.TCLN_ID,
            '${createdBy}',
            '${createdOn}',
            ${lastModifiedByStr},
            ${lastModifiedOnStr},
            ${commentStr},
            tcln.NAME,
            'MANUAL',
            ${status},
            null,
            null,
            (SELECT MAX(ATTACHMENT_LIST_ID) FROM ATTACHMENT_LIST),
            (SELECT MAX(ISSUE_LIST_ID) FROM ISSUE_LIST),
            tc.PREREQUISITE,
            tc.TC_STATUS,
            tc.IMPORTANCE,
            tc.REFERENCE,
            tcln.DESCRIPTION,
            null,
            'test-case.nature.NAT_UNDEFINED',
            'NAT_UNDEFINED',
            'indeterminate_checkbox_empty',
            'test-case.type.TYP_UNDEFINED',
            'TYP_UNDEFINED',
            'indeterminate_checkbox_empty'
          FROM TEST_CASE_LIBRARY_NODE tcln
                 JOIN TEST_CASE tc ON tcln.TCLN_ID = tc.TCLN_ID
          WHERE tcln.NAME = '${testCaseName}'`;
}

export function insertExecutionForSprint(
  testCaseName: string,
  index: number,
  createdBy: string,
  createdOn: string,
  lastModifiedByStr: string,
  lastModifiedOnStr: string,
  commentStr: string,
  status: string,
  datasetName?: string,
): string {
  return `INSERT INTO EXECUTION (EXECUTION_ID, TCLN_ID, CREATED_BY, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON,
                                 DESCRIPTION, NAME, EXECUTION_MODE, EXECUTION_STATUS, LAST_EXECUTED_BY, LAST_EXECUTED_ON,
                                 ATTACHMENT_LIST_ID, ISSUE_LIST_ID, PREREQUISITE, TC_STATUS, IMPORTANCE, REFERENCE, TC_DESCRIPTION,
                                 DATASET_LABEL, TC_NAT_LABEL, TC_NAT_CODE, TC_NAT_ICON_NAME, TC_TYP_LABEL, TC_TYP_CODE, TC_TYP_ICON_NAME,
                                 TEST_PLAN_ITEM_ID, EXECUTION_ORDER)
          SELECT
            (SELECT COALESCE(MAX(EXECUTION_ID), 0) + 1 FROM EXECUTION),
            tcln.TCLN_ID,
            '${createdBy}',
            '${createdOn}',
            ${lastModifiedByStr},
            ${lastModifiedOnStr},
            ${commentStr},
            tcln.NAME,
            'MANUAL',
            ${status},
            null,
            null,
            (SELECT MAX(ATTACHMENT_LIST_ID) FROM ATTACHMENT_LIST),
            (SELECT MAX(ISSUE_LIST_ID) FROM ISSUE_LIST),
            tc.PREREQUISITE,
            tc.TC_STATUS,
            tc.IMPORTANCE,
            tc.REFERENCE,
            tcln.DESCRIPTION,
            '${datasetName}',
            'test-case.nature.NAT_UNDEFINED',
            'NAT_UNDEFINED',
            'indeterminate_checkbox_empty',
            'test-case.type.TYP_UNDEFINED',
            'TYP_UNDEFINED',
            'indeterminate_checkbox_empty',
            (SELECT MAX(TEST_PLAN_ITEM_ID) FROM TEST_PLAN_ITEM),
            ${index}
          FROM TEST_CASE_LIBRARY_NODE tcln
                 JOIN TEST_CASE tc ON tcln.TCLN_ID = tc.TCLN_ID
          WHERE tcln.NAME = '${testCaseName}'`;
}

export function insertKeywordExecution(): string {
  return `INSERT INTO KEYWORD_EXECUTION (EXECUTION_ID)
          SELECT MAX(EXECUTION_ID)
          FROM EXECUTION`;
}

export function insertScriptedExecution(): string {
  return `INSERT INTO SCRIPTED_EXECUTION (EXECUTION_ID)
          SELECT MAX(EXECUTION_ID)
          FROM EXECUTION`;
}

export function insertExploratorySessionForSprint(
  testCaseName: string,
  reqVersionName: string,
): string {
  return `INSERT INTO EXPLORATORY_SESSION_OVERVIEW(OVERVIEW_ID, ITEM_TEST_PLAN_ID, NAME, ATTACHMENT_LIST_ID, TEST_PLAN_ITEM_ID)
          SELECT COALESCE (MAX(OVERVIEW_ID), 0) + 1,
                 null,
                 '${testCaseName}',
                 (SELECT MAX(ATTACHMENT_LIST_ID) FROM ATTACHMENT_LIST),
                 (SELECT TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID
                  FROM TEST_PLAN_ITEM
                         INNER JOIN TEST_PLAN ON TEST_PLAN.TEST_PLAN_ID = TEST_PLAN_ITEM.TEST_PLAN_ID
                         INNER JOIN SPRINT_REQ_VERSION ON SPRINT_REQ_VERSION.TEST_PLAN_ID = TEST_PLAN.TEST_PLAN_ID
                         INNER JOIN TEST_CASE_LIBRARY_NODE ON TEST_CASE_LIBRARY_NODE.TCLN_ID = TEST_PLAN_ITEM.TCLN_ID
                  WHERE SPRINT_REQ_VERSION.NAME = '${reqVersionName}'
                    AND TEST_CASE_LIBRARY_NODE.NAME = '${testCaseName}')
          FROM EXPLORATORY_SESSION_OVERVIEW`;
}

export function insertExploratorySessionForIteration(): string {
  return `INSERT INTO EXPLORATORY_SESSION_OVERVIEW(OVERVIEW_ID, ITEM_TEST_PLAN_ID, NAME, ATTACHMENT_LIST_ID)
          SELECT COALESCE (MAX(OVERVIEW_ID), 0) + 1,
          (SELECT MAX(ITEM_TEST_PLAN_ID) FROM ITERATION_TEST_PLAN_ITEM),
          (SELECT NAME FROM TEST_CASE_LIBRARY_NODE WHERE TCLN_ID = (SELECT MAX(TCLN_ID) FROM ITERATION_TEST_PLAN_ITEM)),
          (SELECT MAX(ATTACHMENT_LIST_ID) FROM ATTACHMENT_LIST)
          FROM EXPLORATORY_SESSION_OVERVIEW`;
}

export function insertExploratoryExecution(): string {
  return `INSERT INTO EXPLORATORY_EXECUTION (EXECUTION_ID, ASSIGNEE_ID, TASK_DIVISION, REVIEWED)
          SELECT MAX(EXECUTION_ID),
                 null,
                 null,
                 false FROM EXECUTION`;
}

export function updateExecutionForExploratoryExecution(
  testCaseName: string,
  index: number,
): string {
  return `UPDATE EXECUTION
            SET TEST_PLAN_ITEM_ID = (SELECT EXPLORATORY_SESSION_OVERVIEW.TEST_PLAN_ITEM_ID
                                        FROM EXPLORATORY_SESSION_OVERVIEW
                                        WHERE EXPLORATORY_SESSION_OVERVIEW.NAME = '${testCaseName}'),
            EXECUTION_ORDER = ${index}`;
}

export function insertExecutionStep(createdBy: string, createdOn: string, status: string): string {
  return `INSERT INTO EXECUTION_STEP(EXECUTION_STEP_ID, EXPECTED_RESULT, ACTION, EXECUTION_STATUS, TEST_STEP_ID, CREATED_BY, CREATED_ON, ATTACHMENT_LIST_ID, ISSUE_LIST_ID)
          SELECT COALESCE (MAX(EXECUTION_STEP_ID), 0) + 1,
                 (SELECT (EXPECTED_RESULT) FROM ACTION_TEST_STEP WHERE TEST_STEP_ID = (SELECT MAX(TEST_STEP_ID) FROM ACTION_TEST_STEP)),
                 (SELECT (ACTION) FROM ACTION_TEST_STEP WHERE TEST_STEP_ID = (SELECT MAX(TEST_STEP_ID) FROM ACTION_TEST_STEP)),
                 ${status},
                 (SELECT MAX(TEST_STEP_ID) FROM TEST_STEP),
                 '${createdBy}',
                 '${createdOn}',
                 (SELECT MAX(ATTACHMENT_LIST_ID) FROM ATTACHMENT_LIST),
                 (SELECT MAX(ISSUE_LIST_ID) FROM ISSUE_LIST)
          FROM EXECUTION_STEP`;
}

export function insertExecutionExecutionStep(): string {
  return `INSERT INTO EXECUTION_EXECUTION_STEPS(EXECUTION_ID, EXECUTION_STEP_ID, EXECUTION_STEP_ORDER)
          SELECT
              (SELECT MAX(EXECUTION_ID) FROM EXECUTION),
              (SELECT MAX(EXECUTION_STEP_ID) FROM EXECUTION_STEP),
              COALESCE(MAX(EXECUTION_STEP_ORDER)+1, 0)
          FROM EXECUTION_EXECUTION_STEPS`;
}

export function insertLastExecutedByAndOnInExecutionStep(
  lastExecutedBy: string,
  lastExecutedOn: string,
): string {
  return `UPDATE EXECUTION_STEP
          SET
            LAST_EXECUTED_BY = '${lastExecutedBy}',
            LAST_EXECUTED_ON = '${lastExecutedOn}',
            LAST_MODIFIED_BY =  '${lastExecutedBy}',
            LAST_MODIFIED_ON = '${lastExecutedOn}'
          WHERE EXECUTION_STEP_ID = (SELECT MAX(EXECUTION_STEP_ID) FROM EXECUTION_STEP)`;
}
// **************
// * CORE PARTY *
// **************

export function insertAclResponsibilityScopeEntryForTeam(
  teamName: string,
  aclGroupId: number,
  array: [string, string][],
  classIndex: number,
  projectName: string,
): string {
  return `INSERT INTO ACL_RESPONSIBILITY_SCOPE_ENTRY(ID, PARTY_ID, ACL_GROUP_ID, OBJECT_IDENTITY_ID)
          SELECT COALESCE(MAX(ID), 0) + 1,
                 (SELECT t.PARTY_ID FROM CORE_TEAM t WHERE t.NAME = '${teamName}'),
                 ${aclGroupId},
                 (SELECT ((p.PROJECT_ID - 1) * ${array.length}) + ${classIndex} + 1
                  FROM PROJECT p
                  WHERE p.NAME = '${projectName}'
                 )
          FROM ACL_RESPONSIBILITY_SCOPE_ENTRY`;
}

export function insertCoreTeamMembers(teamName: string, userLogins: string[]): string {
  return `INSERT INTO CORE_TEAM_MEMBER (USER_ID, TEAM_ID)
          SELECT u.PARTY_ID,
                 (SELECT t.PARTY_ID FROM CORE_TEAM t WHERE t.NAME = '${teamName}')
          FROM CORE_USER u WHERE u.LOGIN IN ('${userLogins.join("','")}')`;
}

export function insertCoreParty(): string {
  return `INSERT INTO CORE_PARTY (PARTY_ID)
          SELECT (COALESCE(MAX(PARTY_ID), 0) + 1)
          FROM CORE_PARTY`;
}

export function insertCoreTeam(
  name: string,
  createdBy: string,
  createdOn: string,
  lastModifiedByStr: string,
  lastModifiedOnStr: string,
  description: string,
): string {
  return `INSERT INTO CORE_TEAM (PARTY_ID, NAME, CREATED_BY, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON, DESCRIPTION)
          SELECT MAX(PARTY_ID),
                 '${name}',
                 '${createdBy}',
                 '${createdOn}',
                 ${lastModifiedByStr},
                 ${lastModifiedOnStr},
                 '${description}'
          FROM CORE_PARTY`;
}

export function insertCoreUser(
  login: string,
  firstName: string,
  lastName: string,
  email: string,
  createdBy: string,
  createdOn: string,
  lastModifiedByStr: string,
  lastModifiedOnStr: string,
  active: boolean,
  lastConnectedOnStr: string,
  canDeleteFromFront: boolean,
): string {
  return `INSERT INTO CORE_USER (PARTY_ID, LOGIN, FIRST_NAME, LAST_NAME, EMAIL, CREATED_BY, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON, ACTIVE, LAST_CONNECTED_ON, CAN_DELETE_FROM_FRONT)
          SELECT MAX(PARTY_ID),
                 '${login}',
                 '${firstName}',
                 '${lastName}',
                 '${email}',
                 '${createdBy}',
                 '${createdOn}',
                 ${lastModifiedByStr},
                 ${lastModifiedOnStr},
                 ${active},
                 ${lastConnectedOnStr},
                 ${canDeleteFromFront}
          FROM CORE_PARTY`;
}

export function insertCoreUserMember(groupId: number): string {
  return `INSERT INTO CORE_GROUP_MEMBER (PARTY_ID, GROUP_ID)
          SELECT MAX(PARTY_ID),
                 ${groupId}
          FROM CORE_PARTY`;
}

export function insertAuthUser(login: string, password: string, active: boolean): string {
  return `INSERT INTO AUTH_USER (LOGIN, PASSWORD, ACTIVE)
          VALUES ('${login}', '${password}', ${active})`;
}

// ****************
// * CUSTOM FIELD *
// ****************
export function insertCustomFieldValueFromEntity(
  boundEntity: BindableEntity,
  boundEntityIdColumnName: string,
  boundEntityTableName: string,
): string {
  return `INSERT INTO CUSTOM_FIELD_VALUE (CFB_ID, CFV_ID, BOUND_ENTITY_ID, BOUND_ENTITY_TYPE, FIELD_TYPE, CF_ID)
          SELECT cfb.CFB_ID,
                 (SELECT COALESCE(MAX(CFV_ID), 0) FROM CUSTOM_FIELD_VALUE) + (ROW_NUMBER() OVER (ORDER BY cfb.CFB_ID)),
                 (SELECT MAX(${boundEntityIdColumnName}) FROM ${boundEntityTableName}),
                 '${boundEntity}',
                 CASE WHEN cf.INPUT_TYPE = 'NUMERIC' THEN 'NUM'
                      WHEN cf.INPUT_TYPE = 'TAG' THEN 'TAG'
                      WHEN cf.INPUT_TYPE = 'RICH_TEXT' THEN 'RTF'
                      ELSE 'CF' END,
                 cfb.CF_ID
          FROM CUSTOM_FIELD_BINDING cfb
                 JOIN CUSTOM_FIELD cf ON cfb.CF_ID = cf.CF_ID
          WHERE cfb.bound_entity = '${boundEntity}'
            AND cfb.bound_project_id = (SELECT MAX(PROJECT_ID) FROM PROJECT)`;
}

export function insertCustomFieldBinding(name: string, boundEntity: BindableEntity): string {
  return `INSERT INTO CUSTOM_FIELD_BINDING (CFB_ID, CF_ID, BOUND_ENTITY, BOUND_PROJECT_ID, POSITION)
          SELECT COALESCE(MAX(CFB_ID), 0) + 1,
                 (SELECT CF_ID FROM CUSTOM_FIELD C WHERE C.NAME= '${name}'),
                 '${boundEntity}',
                 (SELECT MAX(PROJECT_ID) FROM PROJECT),
                 (SELECT COALESCE(MAX(POSITION), 0) + 1
                  FROM CUSTOM_FIELD_BINDING
                  WHERE BOUND_PROJECT_ID = (SELECT MAX(PROJECT_ID) FROM PROJECT)
                    AND BOUND_ENTITY = '${boundEntity}'
                 )
          FROM CUSTOM_FIELD_BINDING`;
}

export function insertCustomFieldOption(
  label: string,
  index: number,
  code: string,
  colourString: string,
): string {
  return `INSERT INTO CUSTOM_FIELD_OPTION (CF_ID, LABEL, POSITION, CODE, COLOUR)
          SELECT MAX(CF_ID),
                 '${label}',
                 ${index},
                 '${code}',
                 ${colourString}
          FROM CUSTOM_FIELD`;
}

export function insertCustomField(
  name: string,
  label: string,
  code: string,
  inputType: InputType,
  fieldType: CustomFieldType,
): string {
  return `INSERT INTO CUSTOM_FIELD (CF_ID, FIELD_TYPE, NAME, LABEL, OPTIONAL, DEFAULT_VALUE, INPUT_TYPE, CODE, LARGE_DEFAULT_VALUE)
          SELECT COALESCE(MAX(CF_ID), 0) + 1,
                 '${fieldType}',
                 '${name}',
                 '${label}',
                 true,
                 CASE WHEN '${inputType}' = 'CHECKBOX' THEN 'false'
                      WHEN '${inputType}' IN ('RTF', 'NUM') THEN null ELSE ''
                   END,
                 '${inputType}',
                 '${code}',
                 CASE WHEN '${inputType}' = 'RICH_TEXT' THEN '' ELSE null END
          FROM CUSTOM_FIELD`;
}

export function updateCustomFieldValue(
  boundEntityType: BindableEntity,
  boundEntityIdColumnName: string,
  boundEntityTableName: string,
  cufName: string,
  value: string,
) {
  return `UPDATE CUSTOM_FIELD_VALUE SET VALUE = '${value}' WHERE BOUND_ENTITY_ID = (SELECT MAX(${boundEntityIdColumnName}) FROM ${boundEntityTableName})
                                                             AND BOUND_ENTITY_TYPE = '${boundEntityType}'
                                                             AND CFB_ID = (SELECT CFB_ID FROM CUSTOM_FIELD_BINDING WHERE CF_ID = (SELECT CF_ID FROM CUSTOM_FIELD WHERE NAME='${cufName}'))`;
}
// *************
// * INFO LIST *
// *************
export function insertInfoListItem(
  label: string,
  code: string,
  icon: string,
  color: string,
): string {
  return `INSERT INTO INFO_LIST_ITEM (ITEM_ID, ITEM_TYPE, LIST_ID, ITEM_INDEX, LABEL, CODE, IS_DEFAULT, ICON_NAME, COLOUR)
          SELECT COALESCE(MAX(ITEM_ID), 0) + 1,
                 'USR',
                 (SELECT MAX(INFO_LIST_ID) FROM INFO_LIST),
                 0,
                 '${label}',
                 '${code}',
                 true,
                 '${icon}',
                 '${color}'
          FROM INFO_LIST_ITEM`;
}
export function insertInfoList(
  label: string,
  description: string,
  code: string,
  createdBy: string,
  createdOn: string,
  lastModifiedByStr: string,
  lastModifiedOnStr: string,
) {
  return `INSERT INTO INFO_LIST (INFO_LIST_ID, LABEL, DESCRIPTION, CODE, CREATED_BY, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON)
          SELECT COALESCE(MAX(INFO_LIST_ID), 0) + 1,
                 '${label}',
                 '${description}',
                 '${code}',
                 '${createdBy}',
                 '${createdOn}',
                 ${lastModifiedByStr},
                 ${lastModifiedOnStr}
          FROM INFO_LIST`;
}
// *********
// * ISSUE *
// *********
export function insertIssueList(): string {
  return `INSERT INTO ISSUE_LIST (ISSUE_LIST_ID)
          SELECT COALESCE(MAX(ISSUE_LIST_ID), 0) + 1
          FROM ISSUE_LIST`;
}

export function insertIssue(remoteIssue: string, issueListId: number) {
  return `INSERT INTO ISSUE (ISSUE_ID, REMOTE_ISSUE_ID, ISSUE_LIST_ID, BUGTRACKER_ID)
          SELECT COALESCE(MAX(ISSUE_ID), 0) + 1,
                 '${remoteIssue}',
                 ${issueListId},
                 (SELECT MAX(BUGTRACKER_ID) FROM BUGTRACKER)
          FROM ISSUE`;
}

// *************
// * MILESTONE *
// *************
export function insertMilestone(
  label: string,
  status: MilestoneStatusKeys,
  endDate: string,
  description: string,
  createdBy: string,
  createdOn: string,
  lastModifiedByStr: string,
  lastModifiedOnStr: string,
  userLogin: string,
): string {
  return `INSERT INTO MILESTONE (MILESTONE_ID, LABEL, STATUS, END_DATE, DESCRIPTION, M_RANGE, CREATED_BY, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON, USER_ID)
          SELECT COALESCE(MAX(MILESTONE_ID), 0) + 1,
                 '${label}',
                 '${status}',
                 '${endDate}',
                 '${description}',
                 'GLOBAL',
                 '${createdBy}',
                 '${createdOn}',
                 ${lastModifiedByStr},
                 ${lastModifiedOnStr},
                 (SELECT PARTY_ID FROM CORE_USER WHERE LOGIN = '${userLogin}')
          FROM MILESTONE`;
}

export function insertMilestoneBinding(label: string): string {
  return `INSERT INTO MILESTONE_BINDING (MILESTONE_BINDING_ID, MILESTONE_ID, PROJECT_ID)
          SELECT COALESCE(MAX(MILESTONE_BINDING_ID), 0) + 1,
                 (SELECT MILESTONE_ID FROM MILESTONE WHERE LABEL = '${label}'),
                 (SELECT MAX(PROJECT_ID) FROM PROJECT)
          FROM MILESTONE_BINDING`;
}

export function insertMilestoneBindingPerimeter(label: string): string {
  return `INSERT INTO MILESTONE_BINDING_PERIMETER (MILESTONE_BINDING_PERIMETER_ID, MILESTONE_ID, PROJECT_ID)
          SELECT COALESCE(MAX(MILESTONE_BINDING_PERIMETER_ID), 0) + 1,
                 (SELECT MILESTONE_ID FROM MILESTONE WHERE LABEL = '${label}'),
                 (SELECT MAX(PROJECT_ID) FROM PROJECT)
          FROM MILESTONE_BINDING_PERIMETER`;
}
export function insertMilestoneRequirementVersion(label: string): string {
  return `INSERT INTO MILESTONE_REQ_VERSION (MILESTONE_ID, REQ_VERSION_ID)
          SELECT (SELECT MILESTONE_ID FROM MILESTONE WHERE LABEL = '${label}'),
                 (SELECT MAX(RES_ID) FROM REQUIREMENT_VERSION)
          FROM MILESTONE`;
}
export function insertMilestoneTestCase(label: string): string {
  return `INSERT INTO MILESTONE_TEST_CASE (MILESTONE_ID, TEST_CASE_ID)
          SELECT (SELECT MILESTONE_ID FROM MILESTONE WHERE LABEL = '${label}'),
                 (SELECT MAX(TCLN_ID) FROM TEST_CASE)
          FROM MILESTONE`;
}
export function insertMilestoneCampaign(label: string): string {
  return `INSERT INTO MILESTONE_CAMPAIGN (MILESTONE_ID, CAMPAIGN_ID)
          SELECT (SELECT MILESTONE_ID FROM MILESTONE WHERE LABEL = '${label}'),
                 (SELECT MAX(CLN_ID) FROM CAMPAIGN)`;
}

// ***********
// * PROJECT *
// ***********
export function insertProject(
  name: string,
  description: string,
  label: string,
  createdBy: string,
  createdOn: string,
  lastModifiedByStr: string,
  lastModifiedOnStr: string,
): string {
  return `INSERT INTO PROJECT(PROJECT_ID, NAME, DESCRIPTION, LABEL, CREATED_BY, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON, RL_ID, TCL_ID, CL_ID, CRL_ID, ARL_ID, AWL_ID, ATTACHMENT_LIST_ID)
          SELECT COALESCE(MAX(PROJECT_ID), 0) + 1,
                 '${name}',
                 '${description}',
                 '${label}',
                 '${createdBy}',
                 '${createdOn}',
                 ${lastModifiedByStr},
                 ${lastModifiedOnStr},
                 (SELECT MAX(RL_ID) FROM REQUIREMENT_LIBRARY),
                 (SELECT MAX(TCL_ID) FROM TEST_CASE_LIBRARY),
                 (SELECT MAX(CL_ID) FROM CAMPAIGN_LIBRARY),
                 (SELECT MAX(CRL_ID) FROM CUSTOM_REPORT_LIBRARY),
                 (SELECT MAX(ARL_ID) FROM AUTOMATION_REQUEST_LIBRARY),
                 (SELECT MAX(AWL_ID) FROM ACTION_WORD_LIBRARY),
                 (SELECT MAX(ATTACHMENT_LIST_ID) FROM ATTACHMENT_LIST)
          FROM PROJECT`;
}

export function insertRequirementLibrary(): string {
  return `INSERT INTO REQUIREMENT_LIBRARY (RL_ID, ATTACHMENT_LIST_ID)
          SELECT COALESCE(MAX(RL_ID), 0) + 1,
                 (SELECT MAX(ATTACHMENT_LIST_ID) FROM ATTACHMENT_LIST)
          FROM REQUIREMENT_LIBRARY`;
}

export function insertTestCaseLibrary(): string {
  return `INSERT INTO TEST_CASE_LIBRARY (TCL_ID, ATTACHMENT_LIST_ID)
          SELECT COALESCE(MAX(TCL_ID), 0) + 1,
                 (SELECT MAX(ATTACHMENT_LIST_ID) FROM ATTACHMENT_LIST)
          FROM TEST_CASE_LIBRARY`;
}

export function insertCampaignLibrary(): string {
  return `INSERT INTO CAMPAIGN_LIBRARY (CL_ID, ATTACHMENT_LIST_ID)
          SELECT COALESCE(MAX(CL_ID), 0) + 1,
                 (SELECT MAX(ATTACHMENT_LIST_ID) FROM ATTACHMENT_LIST)
          FROM CAMPAIGN_LIBRARY`;
}

export function insertCustomReportLibrary(): string {
  return `INSERT INTO CUSTOM_REPORT_LIBRARY (CRL_ID, ATTACHMENT_LIST_ID)
          SELECT COALESCE(MAX(CRL_ID), 0) + 1,
                 (SELECT MAX(ATTACHMENT_LIST_ID) FROM ATTACHMENT_LIST)
          FROM CUSTOM_REPORT_LIBRARY`;
}

export function insertCustomReportLibraryNodeForLibrary(projectName: string): string {
  return `INSERT INTO CUSTOM_REPORT_LIBRARY_NODE (CRLN_ID, NAME, ENTITY_TYPE, ENTITY_ID, CRL_ID)
          SELECT COALESCE(MAX(CRLN_ID), 0) + 1,
                 '${projectName}',
                 'LIBRARY',
                 (SELECT MAX(CRL_ID) FROM CUSTOM_REPORT_LIBRARY),
                 (SELECT MAX(CRL_ID) FROM CUSTOM_REPORT_LIBRARY)
          FROM CUSTOM_REPORT_LIBRARY_NODE`;
}

export function insertAutomationRequestLibrary(): string {
  return `INSERT INTO AUTOMATION_REQUEST_LIBRARY(ARL_ID, ATTACHMENT_LIST_ID)
          SELECT COALESCE(MAX(ARL_ID), 0) + 1,
                 (SELECT MAX(ATTACHMENT_LIST_ID) FROM ATTACHMENT_LIST)
          FROM AUTOMATION_REQUEST_LIBRARY`;
}

export function insertActionWordLibrary(): string {
  return `INSERT INTO ACTION_WORD_LIBRARY (AWL_ID, ATTACHMENT_LIST_ID)
          SELECT COALESCE(MAX(AWL_ID), 0) + 1,
                 (SELECT MAX(ATTACHMENT_LIST_ID) FROM ATTACHMENT_LIST)
          FROM ACTION_WORD_LIBRARY`;
}

export function insertActionWordLibraryNodeForLibrary(projectName: string): string {
  return `INSERT INTO ACTION_WORD_LIBRARY_NODE (AWLN_ID, NAME, ENTITY_TYPE, ENTITY_ID, AWL_ID)
          SELECT COALESCE(MAX(AWLN_ID), 0) + 1,
                 '${projectName}',
                 'LIBRARY',
                 (SELECT MAX(AWL_ID) FROM ACTION_WORD_LIBRARY),
                 (SELECT MAX(AWL_ID) FROM ACTION_WORD_LIBRARY)
          FROM ACTION_WORD_LIBRARY_NODE`;
}

export function insertAclObjectIdentity(key: string): string {
  return `INSERT INTO ACL_OBJECT_IDENTITY (ID, IDENTITY, CLASS_ID)
          SELECT COALESCE(MAX(ID), 0) + 1,
                 (SELECT MAX(PROJECT_ID) FROM PROJECT),
                 ${key}
          FROM ACL_OBJECT_IDENTITY`;
}

// ***************
// * REQUIREMENT *
// ***************
export function insertRequirementLibraryNode(
  createdBy: string,
  createdOn: string,
  lastModifiedByStr: string,
  lastModifiedOnStr: string,
): string {
  return `INSERT INTO REQUIREMENT_LIBRARY_NODE (RLN_ID, CREATED_BY, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON, PROJECT_ID)
          SELECT COALESCE(MAX(RLN_ID), 0) + 1,
                 '${createdBy}',
                 '${createdOn}',
                 ${lastModifiedByStr},
                 ${lastModifiedOnStr},
                 (SELECT MAX(PROJECT_ID) FROM PROJECT)
          FROM REQUIREMENT_LIBRARY_NODE`;
}

export function insertResource(
  name: string,
  description: string,
  createdBy: string,
  createdOn: string,
  lastModifiedByStr: string,
  lastModifiedOnStr: string,
): string {
  return `INSERT INTO RESOURCE (RES_ID, NAME, DESCRIPTION, CREATED_BY, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON, ATTACHMENT_LIST_ID)
          SELECT COALESCE(MAX(RES_ID), 0) + 1,
                 '${name}',
                 '${description}',
                 '${createdBy}',
                 '${createdOn}',
                 ${lastModifiedByStr},
                 ${lastModifiedOnStr},
                 (SELECT MAX(ATTACHMENT_LIST_ID) FROM ATTACHMENT_LIST)
          FROM RESOURCE`;
}

export function insertRequirementLibraryContent(index: number): string {
  return `INSERT INTO REQUIREMENT_LIBRARY_CONTENT (LIBRARY_ID, CONTENT_ID, CONTENT_ORDER)
          SELECT MAX(RL_ID),
                 (SELECT MAX(RLN_ID) FROM REQUIREMENT_LIBRARY_NODE),
                 '${index}'
          FROM REQUIREMENT_LIBRARY`;
}

export function insertRequirement(): string {
  return `INSERT INTO REQUIREMENT (RLN_ID, CURRENT_VERSION_ID, MODE, HIGH_LEVEL_REQUIREMENT_ID)
          SELECT MAX(RLN_ID),
                 null,
                 'NATIVE',
                 null
          FROM REQUIREMENT_LIBRARY_NODE`;
}

export function insertRequirementVersion(
  versionNumber: number,
  reference: string,
  criticality: RequirementCriticalityKeys,
  requirementStatus: RequirementStatusKeys,
): string {
  return `INSERT INTO REQUIREMENT_VERSION (RES_ID, REQUIREMENT_ID, REFERENCE, VERSION_NUMBER, CRITICALITY, REQUIREMENT_STATUS, CATEGORY)
          SELECT MAX(RES_ID),
                 (SELECT MAX(RLN_ID) FROM REQUIREMENT),
                 '${reference}',
                 ${versionNumber},
                 '${criticality}',
                 '${requirementStatus}',
                 3
          FROM RESOURCE`;
}

export function insertRlnRelationship(parentName: string, index: number): string {
  return `INSERT INTO RLN_RELATIONSHIP (DESCENDANT_ID, ANCESTOR_ID, CONTENT_ORDER)
          SELECT MAX(RLN_ID),
                 (SELECT CASE WHEN r.RLN_ID IS NULL THEN rf.RLN_ID ELSE r.RLN_ID END
                  FROM RESOURCE res
                         LEFT JOIN REQUIREMENT r ON res.RES_ID = r.CURRENT_VERSION_ID
                         LEFT JOIN REQUIREMENT_FOLDER rf ON res.RES_ID = rf.RES_ID
                  WHERE res.NAME = '${parentName}'),
                 ${index}
          FROM REQUIREMENT_LIBRARY_NODE`;
}

export function insertSimpleResource(): string {
  return `INSERT INTO SIMPLE_RESOURCE (RES_ID)
          SELECT MAX(RES_ID) FROM RESOURCE`;
}

export function insertRequirementFolder(): string {
  return `INSERT INTO REQUIREMENT_FOLDER (RLN_ID, RES_ID)
          SELECT MAX(RLN_ID),
                 (SELECT MAX(RES_ID) FROM SIMPLE_RESOURCE)
          FROM REQUIREMENT_LIBRARY_NODE`;
}

export function insertHighLevelRequirement(): string {
  return `INSERT INTO HIGH_LEVEL_REQUIREMENT (RLN_ID)
          SELECT MAX(RLN_ID) FROM REQUIREMENT`;
}

export function insertRequirementVersionCoverageFromRequirements(
  verifiedReqVersionNames: string[],
  verifyingTestCaseName: string,
): string {
  return `INSERT INTO REQUIREMENT_VERSION_COVERAGE (VERIFIED_REQ_VERSION_ID, REQUIREMENT_VERSION_COVERAGE_ID, VERIFYING_TEST_CASE_ID)
          SELECT RES_ID,
                 (SELECT COALESCE(MAX(REQUIREMENT_VERSION_COVERAGE_ID), 0) FROM REQUIREMENT_VERSION_COVERAGE) + (ROW_NUMBER() OVER (ORDER BY RES_ID)),
                 (SELECT TCLN_ID FROM TEST_CASE_LIBRARY_NODE WHERE NAME = '${verifyingTestCaseName}')
          FROM RESOURCE
          WHERE NAME IN ('${verifiedReqVersionNames.join("','")}')`;
}

export function insertRequirementVersionCoverageFromTestCases(
  verifyingTestCaseNames: string[],
  verifiedReqVersionName: string,
): string {
  return `INSERT INTO REQUIREMENT_VERSION_COVERAGE (VERIFYING_TEST_CASE_ID, REQUIREMENT_VERSION_COVERAGE_ID, VERIFIED_REQ_VERSION_ID)
          SELECT TCLN_ID,
                 (SELECT COALESCE(MAX(REQUIREMENT_VERSION_COVERAGE_ID), 0) FROM REQUIREMENT_VERSION_COVERAGE) + (ROW_NUMBER() OVER (ORDER BY TCLN_ID)),
                 (SELECT RES_ID FROM RESOURCE WHERE NAME = '${verifiedReqVersionName}')
          FROM TEST_CASE_LIBRARY_NODE
          WHERE NAME IN ('${verifyingTestCaseNames.join("','")}')`;
}

export function insertRequirementVersionCoverageFromTestCasesSteps(verifiedTestCaseNames: string) {
  return `INSERT INTO VERIFYING_STEPS (REQUIREMENT_VERSION_COVERAGE_ID, TEST_STEP_ID)
          SELECT
            COALESCE((SELECT MAX(REQUIREMENT_VERSION_COVERAGE_ID) FROM REQUIREMENT_VERSION_COVERAGE), 0),
            COALESCE((SELECT MAX(TEST_STEP_ID) FROM ACTION_TEST_STEP WHERE TEST_STEP_ID IN (SELECT MAX(STEP_ID) FROM TEST_CASE_STEPS WHERE TEST_CASE_ID = (SELECT TCLN_ID FROM TEST_CASE_LIBRARY_NODE WHERE NAME = '${verifiedTestCaseNames}'))))`;
}

export function updateRequirementCurrentVersion(): string {
  return `UPDATE REQUIREMENT
          SET CURRENT_VERSION_ID = (SELECT MAX(RES_ID) FROM REQUIREMENT_VERSION)
          WHERE RLN_ID = (SELECT MAX(RLN_ID) FROM REQUIREMENT)`;
}

export function updateRequirementHighLevelRequirementId(): string {
  return `UPDATE REQUIREMENT
          SET HIGH_LEVEL_REQUIREMENT_ID = (
            SELECT CASE WHEN hl.RLN_ID is null THEN req.HIGH_LEVEL_REQUIREMENT_ID ELSE hl.RLN_ID END
            FROM RLN_RELATIONSHIP rel
                   LEFT JOIN HIGH_LEVEL_REQUIREMENT hl ON rel.ANCESTOR_ID = hl.RLN_ID
                   LEFT JOIN REQUIREMENT req ON rel.ANCESTOR_ID = req.RLN_ID
            WHERE rel.DESCENDANT_ID = (SELECT MAX(RLN_ID) FROM REQUIREMENT)
          )
          WHERE RLN_ID = (SELECT MAX(RLN_ID) FROM REQUIREMENT)`;
}

export function updateRequirementHighLevelRequirementIdFromNames(
  requirementNames: string[],
  highLevelRequirementName: string,
): string {
  return `UPDATE REQUIREMENT
          SET HIGH_LEVEL_REQUIREMENT_ID = (
            SELECT hl.RLN_ID
            FROM HIGH_LEVEL_REQUIREMENT hl
                   JOIN REQUIREMENT req ON hl.RLN_ID = req.RLN_ID
                   JOIN RESOURCE res ON req.CURRENT_VERSION_ID = res.RES_ID
            WHERE res.NAME = '${highLevelRequirementName}'
          )
          WHERE RLN_ID IN (SELECT req2.RLN_ID
                           FROM REQUIREMENT req2
                                  JOIN RESOURCE res2
                                       ON req2.CURRENT_VERSION_ID = res2.RES_ID
                           WHERE res2.NAME IN ('${requirementNames.join("','")}')
          )`;
}
export function insertRequirementVersionDirectLink(
  requirement: string,
  requirementRelated: string,
) {
  return `INSERT INTO REQUIREMENT_VERSION_LINK (LINK_ID, LINK_TYPE_ID, LINK_DIRECTION, REQUIREMENT_VERSION_ID, RELATED_REQUIREMENT_VERSION_ID)
          SELECT COALESCE(MAX(LINK_ID), 0) + 1,
                 1,
                 false,
                 (SELECT RES_ID FROM RESOURCE WHERE NAME = '${requirement}'),
                 (SELECT RES_ID FROM RESOURCE WHERE NAME = '${requirementRelated}')
          FROM REQUIREMENT_VERSION_LINK`;
}
export function insertRequirementVersionIndirectLink(
  requirement: string,
  requirementRelated: string,
) {
  return `INSERT INTO REQUIREMENT_VERSION_LINK (LINK_ID, LINK_TYPE_ID, LINK_DIRECTION, REQUIREMENT_VERSION_ID, RELATED_REQUIREMENT_VERSION_ID)
          SELECT COALESCE(MAX(LINK_ID), 0) + 1,
                 1,
                 true,
                 (SELECT RES_ID FROM RESOURCE WHERE NAME = '${requirementRelated}'),
                 (SELECT RES_ID FROM RESOURCE WHERE NAME = '${requirement}')
          FROM REQUIREMENT_VERSION_LINK`;
}
// **********
// * AUTOMATION_REQUEST *
// **********

export function insertAutomationRequestLibraryContent(entityName: string) {
  return `INSERT INTO AUTOMATION_REQUEST_LIBRARY_CONTENT (LIBRARY_ID, CONTENT_ID)
          SELECT MAX(ARL_ID),
                 (SELECT TCLN_ID FROM TEST_CASE_LIBRARY_NODE WHERE NAME = '${entityName}')
          FROM AUTOMATION_REQUEST_LIBRARY`;
}

export function insertAutomationRequest(status: string, transmittedOn: string) {
  return `INSERT INTO AUTOMATION_REQUEST (AUTOMATION_REQUEST_ID, REQUEST_STATUS, TRANSMITTED_ON, CREATED_BY, TRANSMITTED_BY, PROJECT_ID)
          SELECT COALESCE(MAX(AUTOMATION_REQUEST_ID), 0) + 1,
                 '${status}',
                 '${transmittedOn}',
                 '1',
                 '1',
                 (SELECT MAX(PROJECT_ID) FROM PROJECT)
          FROM AUTOMATION_REQUEST`;
}

// **********
// * SERVER *
// **********
export function insertThirdPartyServer(
  name: string,
  url: string,
  authPolicy: AuthenticationPolicy,
  authProtocol: AuthenticationProtocol,
  description: string,
  createdBy: string,
  createdOn: string,
  lastModifiedByStr: string,
  lastModifiedOnStr: string,
): string {
  return `INSERT INTO THIRD_PARTY_SERVER (SERVER_ID, NAME, URL, AUTH_POLICY, AUTH_PROTOCOL, DESCRIPTION, CREATED_BY, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON)
          SELECT COALESCE(MAX(SERVER_ID), 0) + 1,
                 '${name}',
                 '${url}',
                 '${authPolicy}',
                 '${authProtocol}',
                 '${description}',
                 '${createdBy}',
                 '${createdOn}',
                 ${lastModifiedByStr},
                 ${lastModifiedOnStr}
          FROM THIRD_PARTY_SERVER`;
}

export function insertStoredCredentials(encryptedCredentials: string): string {
  return `INSERT INTO STORED_CREDENTIALS (CREDENTIAL_ID, ENC_VERSION, ENC_CREDENTIALS, AUTHENTICATED_SERVER)
          SELECT COALESCE(MAX(CREDENTIAL_ID), 0) + 1,
                 1,
                 '${encryptedCredentials}',
                 (SELECT MAX(SERVER_ID) FROM THIRD_PARTY_SERVER)
          FROM STORED_CREDENTIALS`;
}

export function insertSCMServer(committerMail: string): string {
  return `INSERT INTO SCM_SERVER (SERVER_ID, KIND, COMMITTER_MAIL)
          SELECT MAX(SERVER_ID),
                 'git',
                 '${committerMail}'
          FROM THIRD_PARTY_SERVER`;
}

export function insertSCMRepository(
  name: string,
  workingBranch: string,
  repositoryPath: string,
  workingFolderPathStr: string,
): string {
  return `INSERT INTO SCM_REPOSITORY (SCM_REPOSITORY_ID, SERVER_ID, NAME, WORKING_BRANCH, REPOSITORY_PATH, WORKING_FOLDER_PATH)
          SELECT COALESCE(MAX(SCM_REPOSITORY_ID), 0) + 1,
                 (SELECT MAX(SERVER_ID) FROM THIRD_PARTY_SERVER),
                 '${name}',
                 '${workingBranch}',
                 '${repositoryPath}',
                 ${workingFolderPathStr}
          FROM SCM_REPOSITORY`;
}

export function insertBugtracker(kind: string): string {
  return `INSERT INTO BUGTRACKER(BUGTRACKER_ID, KIND)
          SELECT COALESCE(MAX(BUGTRACKER_ID), 0) +1,
                 '${kind}'
          FROM BUGTRACKER`;
}

export function updateProjectWithBugtracker(): string {
  return `UPDATE PROJECT
          SET BUGTRACKER_ID = (SELECT MAX(BUGTRACKER_ID) FROM BUGTRACKER)`;
}

export function insertBugtrackerProject(nameProject: string): string {
  return `INSERT INTO BUGTRACKER_PROJECT(BUGTRACKER_PROJECT_ID, PROJECT_ID, BUGTRACKER_PROJECT_NAME, BUGTRACKER_PROJECT_ORDER)
          SELECT COALESCE(MAX(BUGTRACKER_PROJECT_ID), 0) +1,
                 (SELECT MAX(PROJECT_ID) FROM PROJECT),
                 '${nameProject}',
                 COALESCE(MAX(BUGTRACKER_PROJECT_ORDER) +1, 0)
          FROM BUGTRACKER_PROJECT`;
}

export function insertAutomationServer(kind: string): string {
  return `INSERT INTO TEST_AUTOMATION_SERVER(SERVER_ID, KIND, MANUAL_SLAVE_SELECTION)
          SELECT MAX(SERVER_ID),
                 '${kind}',
                 false
          FROM THIRD_PARTY_SERVER
  `;
}

export function bindAutomationServerToProject(nameServer: string, workflowType: string) {
  return `UPDATE PROJECT
          SET TA_SERVER_ID = (SELECT MAX(SERVER_ID) FROM THIRD_PARTY_SERVER WHERE NAME = '${nameServer}'),
              AUTOMATION_WORKFLOW_TYPE = '${workflowType}',
              ALLOW_AUTOMATION_WORKFLOW = true
          WHERE PROJECT_ID = (SELECT MAX(PROJECT_ID) FROM PROJECT)`;
}

export function bindAutomationProject(workflowType: string) {
  return `UPDATE PROJECT
          SET AUTOMATION_WORKFLOW_TYPE = '${workflowType}',
              ALLOW_AUTOMATION_WORKFLOW = true
          WHERE PROJECT_ID = (SELECT MAX(PROJECT_ID) FROM PROJECT)`;
}

export function insertAiServer(jsonPath: string): string {
  return isPostgresql()
    ? `INSERT INTO AI_SERVER(SERVER_ID, PAYLOAD_TEMPLATE, JSON_PATH)
       SELECT MAX(SERVER_ID),
              '{"model": "mistralai/Mixtral-8x7B-Instruct-v0.1","messages": [{"role": "system", "content": "You are an expert travel guide"},{"role": "user", "content": "Tu es un testeur manuel. Tu dois transformer une exigence en un ou plusieurs cas de test, rédigés en français. La description du cas de test doit consister en un résumé de l exigence fournie. Ta réponse doit suivre le modèle suivant : \\"{\\"testCases\\":[{\\"name\\":\\"\\",\\"description\\":\\"\\",\\"prerequisites\\":\\"\\",\\"testSteps\\":[{\\"index\\":0,\\"action\\":\\"\\",\\"expectedResult\\":\\"\\"}]}]}\\". Voilà l exigence à transformer : {{ requirement }} "}]}',
              '${jsonPath}'
       FROM THIRD_PARTY_SERVER`
    : `INSERT INTO AI_SERVER(SERVER_ID, PAYLOAD_TEMPLATE, JSON_PATH)
       SELECT MAX(SERVER_ID),
              '{"model": "mistralai/Mixtral-8x7B-Instruct-v0.1","messages": [{"role": "system", "content": "You are an expert travel guide"},{"role": "user", "content": "Tu es un testeur manuel. Tu dois transformer une exigence en un ou plusieurs cas de test, rédigés en français. La description du cas de test doit consister en un résumé de l exigence fournie. Ta réponse doit suivre le modèle suivant : \\\\"{\\\\"testCases\\\\":[{\\\\"name\\\\":\\\\"\\\\",\\\\"description\\\\":\\\\"\\\\",\\\\"prerequisites\\\\":\\\\"\\\\",\\\\"testSteps\\\\":[{\\\\"index\\\\":0,\\\\"action\\\\":\\\\"\\\\",\\\\"expectedResult\\\\":\\\\"\\\\"}]}]}\\\\". Voilà l exigence à transformer : {{ requirement }} "}]}',
              '${jsonPath}'
       FROM THIRD_PARTY_SERVER`;
}

export function bindAiServerToProject(nameServer: string) {
  return `UPDATE PROJECT
          SET AI_SERVER_ID = (SELECT MAX(SERVER_ID) FROM THIRD_PARTY_SERVER WHERE NAME = '${nameServer}')
          WHERE PROJECT_ID = (SELECT MAX(PROJECT_ID) FROM PROJECT)`;
}

export function bindRequirementCategoriesListToProject(nameList: string) {
  return `UPDATE PROJECT
          SET REQ_CATEGORIES_LIST = COALESCE((SELECT INFO_LIST_ID FROM INFO_LIST WHERE LABEL LIKE '%${nameList}%'),1)
          WHERE PROJECT_ID = (SELECT MAX(PROJECT_ID) FROM PROJECT)`;
}

export function bindCategoryRequirement(label: string, id: number) {
  return `UPDATE REQUIREMENT_VERSION
          SET CATEGORY = (SELECT ITEM_ID FROM INFO_LIST_ITEM WHERE LABEL = '${label}') WHERE RES_ID = '${id}'`;
}

export function bindTestCaseNatureListToProject(nameList: string) {
  return `UPDATE PROJECT
          SET TC_NATURES_LIST = COALESCE((SELECT INFO_LIST_ID FROM INFO_LIST WHERE LABEL LIKE '%${nameList}%'),1)
          WHERE PROJECT_ID = (SELECT MAX(PROJECT_ID) FROM PROJECT)`;
}

export function bindNatureTestCase(label: string, idTC: number) {
  return `UPDATE TEST_CASE
          SET TC_NATURE = (SELECT ITEM_ID FROM INFO_LIST_ITEM WHERE LABEL = '${label}')
          WHERE tcln_id=${idTC}`;
}

export function bindTestCaseTypeListToProject(nameList: string) {
  return `UPDATE PROJECT
          SET TC_TYPES_LIST = COALESCE((SELECT INFO_LIST_ID FROM INFO_LIST WHERE LABEL LIKE '%${nameList}%'),1)
          WHERE PROJECT_ID = (SELECT MAX(PROJECT_ID) FROM PROJECT)`;
}

export function bindTypeTestCase(label: string, idTC: number) {
  return `UPDATE TEST_CASE tc
          SET TC_TYPE = (SELECT ITEM_ID FROM INFO_LIST_ITEM WHERE LABEL = '${label}')
          WHERE tcln_id = ${idTC}`;
}

// *************
// * TEST CASE *
// *************
export function insertTestCaseLibraryContent(index: number): string {
  return `INSERT INTO TEST_CASE_LIBRARY_CONTENT (LIBRARY_ID, CONTENT_ID, CONTENT_ORDER)
          SELECT MAX(TCL_ID),
                 (SELECT MAX(TCLN_ID) FROM TEST_CASE_LIBRARY_NODE),
                 '${index}'
          FROM TEST_CASE_LIBRARY`;
}

export function insertTestCaseLibraryNode(
  name: string,
  description: string,
  createdBy: string,
  createdOn: string,
  lastModifiedByStr: string,
  lastModifiedOnStr: string,
): string {
  return `INSERT INTO TEST_CASE_LIBRARY_NODE (TCLN_ID, CREATED_BY, CREATED_ON, PROJECT_ID, NAME, DESCRIPTION, ATTACHMENT_LIST_ID, LAST_MODIFIED_BY, LAST_MODIFIED_ON)
          SELECT COALESCE(MAX(TCLN_ID), 0) + 1,
                 '${createdBy}',
                 '${createdOn}',
                 (SELECT MAX(PROJECT_ID) FROM PROJECT),
                 '${name}',
                 '${description}',
                 (SELECT MAX(ATTACHMENT_LIST_ID) FROM ATTACHMENT_LIST),
                 ${lastModifiedByStr},
                 ${lastModifiedOnStr}
          FROM TEST_CASE_LIBRARY_NODE`;
}

export function insertTclnRelationship(parentName: string, index: number): string {
  return `INSERT INTO TCLN_RELATIONSHIP (DESCENDANT_ID, ANCESTOR_ID, CONTENT_ORDER)
          SELECT MAX(TCLN_ID),
                 (SELECT tcln.TCLN_ID
                  FROM TEST_CASE_LIBRARY_NODE tcln
                  WHERE tcln.NAME = '${parentName}'),
                 ${index}
          FROM TEST_CASE_LIBRARY_NODE`;
}

export function insertTestCaseFolder(): string {
  return `INSERT INTO TEST_CASE_FOLDER (TCLN_ID)
          SELECT MAX(TCLN_ID)
          FROM TEST_CASE_LIBRARY_NODE`;
}

export function insertTestCase(
  status: TestCaseStatusKeys,
  importance: TestCaseImportanceKeys,
  executionMode: TestCaseExecutionModeKeys,
  prerequisite: string,
  importanceAuto: boolean,
  reference: string,
  automatable: string,
): string {
  return `INSERT INTO TEST_CASE (TCLN_ID, IMPORTANCE, TC_STATUS, EXECUTION_MODE, PREREQUISITE, UUID, IMPORTANCE_AUTO, REFERENCE, AUTOMATABLE)
          SELECT MAX(TCLN_ID),
                 '${importance}',
                 '${status}',
                 '${executionMode}',
                 '${prerequisite}',
                 ${DatabaseUtils.createUuidFunction()},
                 ${importanceAuto},
                 '${reference}',
                 '${automatable}'
          FROM TEST_CASE_LIBRARY_NODE`;
}

export function updateTestCaseImportanceIfImportanceAuto(verifyingTestCaseName: string): string {
  return `UPDATE TEST_CASE
          SET IMPORTANCE = (
            SELECT CASE WHEN GROUP_CONCAT(RES.CRITICALITY) LIKE '%CRITICAL%' THEN 'HIGH'
                        WHEN GROUP_CONCAT(RES.CRITICALITY) LIKE '%MAJOR%' THEN 'MEDIUM'
                        ELSE 'LOW'
                     END
            FROM TEST_CASE_LIBRARY_NODE TCLN
                   LEFT JOIN REQUIREMENT_VERSION_COVERAGE RVC ON TCLN.TCLN_ID = RVC.VERIFYING_TEST_CASE_ID
                   LEFT JOIN REQUIREMENT_VERSION RES ON RVC.VERIFIED_REQ_VERSION_ID = RES.RES_ID
            WHERE TCLN.NAME = '${verifyingTestCaseName}'
            GROUP BY TCLN.TCLN_ID
          )
          WHERE TCLN_ID = (SELECT TCLN_ID FROM TEST_CASE_LIBRARY_NODE WHERE NAME = '${verifyingTestCaseName}')
            AND IMPORTANCE_AUTO IS TRUE`;
}

export function insertParameter(name: string, index: number, description: string): string {
  return `INSERT INTO PARAMETER (PARAM_ID, NAME, TEST_CASE_ID, DESCRIPTION, PARAM_ORDER)
          SELECT COALESCE(MAX(PARAM_ID), 0) + 1,
                 '${name}',
                 (SELECT MAX(TCLN_ID) FROM TEST_CASE_LIBRARY_NODE),
                 '${description}',
                 ${index}
          FROM PARAMETER`;
}

export function insertDataSet(name: string): string {
  return `INSERT INTO DATASET (DATASET_ID, NAME, TEST_CASE_ID)
          SELECT COALESCE(MAX(DATASET_ID), 0) + 1,
                 '${name}',
                 (SELECT MAX(TCLN_ID) FROM TEST_CASE_LIBRARY_NODE)
          FROM DATASET`;
}

export function insertEmptyDataSetParamValues(): string {
  return `INSERT INTO DATASET_PARAM_VALUE (PARAM_ID, DATASET_ID, PARAM_VALUE)
          SELECT PARAM_ID,
                 (SELECT MAX(DATASET_ID) FROM DATASET),
                 ''
          FROM PARAMETER
          WHERE TEST_CASE_ID = (SELECT MAX(TCLN_ID) FROM TEST_CASE_LIBRARY_NODE)`;
}

export function updateDataSetParamValue(paramName: string, paramValue: string): string {
  return `UPDATE DATASET_PARAM_VALUE
          SET PARAM_VALUE = '${paramValue}'
          WHERE DATASET_ID = (SELECT MAX(DATASET_ID) FROM DATASET)
            AND PARAM_ID = (SELECT PARAM_ID FROM PARAMETER WHERE NAME = '${paramName}')`;
}

export function insertScriptedTestCase(script: string): string {
  return `INSERT INTO SCRIPTED_TEST_CASE (TCLN_ID, SCRIPT)
          SELECT MAX(TCLN_ID),
                 '${script}'
          FROM TEST_CASE_LIBRARY_NODE`;
}

export function insertKeywordTestCase(): string {
  return `INSERT INTO KEYWORD_TEST_CASE (TCLN_ID)
          SELECT MAX(TCLN_ID)
          FROM TEST_CASE_LIBRARY_NODE`;
}

export function insertExploratoryTestCase(charter: string, sessionDuration: number): string {
  return `INSERT INTO EXPLORATORY_TEST_CASE (TCLN_ID, CHARTER, SESSION_DURATION)
          SELECT MAX(TCLN_ID),
                 '${charter}',
                 ${sessionDuration}
          FROM TEST_CASE_LIBRARY_NODE`;
}

export function insertTestStep(): string {
  return `INSERT INTO TEST_STEP (TEST_STEP_ID)
          SELECT COALESCE(MAX(TEST_STEP_ID), 0) + 1
          FROM TEST_STEP`;
}

export function insertTestCaseSteps(): string {
  return `INSERT INTO TEST_CASE_STEPS (TEST_CASE_ID, STEP_ID, STEP_ORDER)
          SELECT MAX(TCLN_ID),
                 (SELECT MAX(TEST_STEP_ID) FROM TEST_STEP),
                 (SELECT COALESCE(MAX(STEP_ORDER) + 1, 0) FROM TEST_CASE_STEPS WHERE TEST_CASE_ID = (SELECT MAX(TCLN_ID) FROM TEST_CASE_LIBRARY_NODE))
          FROM TEST_CASE_LIBRARY_NODE`;
}

export function insertCallTestCaseSteps(testName: string): string {
  return `INSERT INTO TEST_CASE_STEPS (TEST_CASE_ID, STEP_ID, STEP_ORDER)
          SELECT (SELECT TCLN_ID FROM TEST_CASE_LIBRARY_NODE WHERE NAME ='${testName}'),
                 (SELECT MAX(TEST_STEP_ID) FROM TEST_STEP),
                 (SELECT COALESCE(MAX(STEP_ORDER) + 1, 0) FROM TEST_CASE_STEPS WHERE TEST_CASE_ID = (SELECT MAX(TCLN_ID) FROM TEST_CASE_LIBRARY_NODE))`;
}
export function insertCallTestStep() {
  return `INSERT INTO CALL_TEST_STEP (TEST_STEP_ID, CALLED_TEST_CASE_ID, CALLED_DATASET,DELEGATE_PARAMETER_VALUES)
          SELECT MAX(TEST_STEP_ID),
                 (SELECT MAX(TCLN_ID) FROM TEST_CASE),
                 null,
                 false
          FROM TEST_STEP`;
}
export function insertActionTestStep(action: string, expectedResult: string): string {
  return `INSERT INTO ACTION_TEST_STEP (TEST_STEP_ID, ACTION, EXPECTED_RESULT, ATTACHMENT_LIST_ID)
          SELECT MAX(TEST_STEP_ID),
                 '${action}',
                 '${expectedResult}',
                 (SELECT MAX(ATTACHMENT_LIST_ID) FROM ATTACHMENT_LIST)
          FROM TEST_STEP`;
}

export function insertKeywordTestStep(
  keyword: string,
  datatable: string,
  docstring: string,
  comment: string,
): string {
  return `INSERT INTO KEYWORD_TEST_STEP (TEST_STEP_ID, ACTION_WORD_ID, KEYWORD, DATATABLE, DOCSTRING, COMMENT)
          SELECT MAX(TEST_STEP_ID),
                 (SELECT MAX(ACTION_WORD_ID) FROM ACTION_WORD),
                 '${keyword}',
                 '${datatable}',
                 '${docstring}',
                 '${comment}'
          FROM TEST_STEP`;
}

export function updateAutomationRequestIdInTest() {
  return `UPDATE TEST_CASE
          SET AUTOMATION_REQUEST_ID = (SELECT MAX(AUTOMATION_REQUEST_ID) FROM AUTOMATION_REQUEST)
          WHERE TCLN_ID = (SELECT MAX(TCLN_ID) FROM TEST_CASE)`;
}

export function insertActionWord(token: string): string {
  return `INSERT INTO ACTION_WORD (ACTION_WORD_ID, DESCRIPTION, CREATED_BY, CREATED_ON, TOKEN, PROJECT_ID)
          SELECT COALESCE(MAX(ACTION_WORD_ID), 0) + 1,
                 null,
                 '${DEFAULT_USER_LOGIN}',
                 '${newDateAsString()}',
                 '${token}',
                 (SELECT MAX(PROJECT_ID) FROM PROJECT)
          FROM ACTION_WORD`;
}

export function insertActionWordFragment(order: number): string {
  return `INSERT INTO ACTION_WORD_FRAGMENT (ACTION_WORD_FRAGMENT_ID, ACTION_WORD_ID, FRAGMENT_ORDER)
          SELECT COALESCE(MAX(ACTION_WORD_FRAGMENT_ID), 0) + 1,
                 (SELECT MAX(ACTION_WORD_ID) FROM ACTION_WORD),
                 '${order}'
          FROM ACTION_WORD_FRAGMENT`;
}

export function insertActionWordLibraryNode(simpleActionWord: string): string {
  return `INSERT INTO ACTION_WORD_LIBRARY_NODE (AWLN_ID, NAME, ENTITY_TYPE, ENTITY_ID, AWL_ID)
          SELECT COALESCE(MAX(AWLN_ID), 0) + 1,
                 '${simpleActionWord}',
                 'ACTION_WORD',
                 (SELECT MAX(ACTION_WORD_ID) FROM ACTION_WORD),
                 (SELECT MAX(AWL_ID) FROM ACTION_WORD_LIBRARY)
          FROM ACTION_WORD_LIBRARY_NODE`;
}

export function insertActionWordText(simpleActionWord: string): string {
  return `INSERT INTO ACTION_WORD_TEXT (ACTION_WORD_FRAGMENT_ID, TEXT)
          SELECT MAX(ACTION_WORD_FRAGMENT_ID),
                 '${simpleActionWord}'
          FROM ACTION_WORD_FRAGMENT`;
}

export function insertActionWordParameter(name: string, value: string): string {
  return `INSERT INTO ACTION_WORD_PARAMETER (ACTION_WORD_FRAGMENT_ID, NAME, DEFAULT_VALUE)
          SELECT MAX(ACTION_WORD_FRAGMENT_ID),
                 '${name}',
                 '${value}'
          FROM ACTION_WORD_FRAGMENT`;
}

export function insertAwlnRelationship(): string {
  return `INSERT INTO AWLN_RELATIONSHIP (ANCESTOR_ID, DESCENDANT_ID, CONTENT_ORDER)
          SELECT MAX(AWL_ID),
                 (SELECT MAX(AWLN_ID) FROM ACTION_WORD_LIBRARY_NODE),
                 (SELECT COALESCE(MAX(CONTENT_ORDER) + 1, 0) FROM AWLN_RELATIONSHIP WHERE ANCESTOR_ID = (SELECT MAX(AWL_ID) FROM ACTION_WORD_LIBRARY_NODE))
          FROM ACTION_WORD_LIBRARY`;
}

export function insertCrlnRelationship(index: number, parentName: string) {
  return `INSERT INTO CRLN_RELATIONSHIP (ANCESTOR_ID, DESCENDANT_ID, CONTENT_ORDER)
          SELECT  (SELECT CRLN_ID
                   FROM CUSTOM_REPORT_LIBRARY_NODE
                   WHERE NAME = '${parentName}'),
                  MAX(CRLN_ID),
                  ${index}
          FROM CUSTOM_REPORT_LIBRARY_NODE`;
}

export function insertFolderInCustomFieldReportLibrary(name: string): string {
  return `INSERT INTO CUSTOM_REPORT_FOLDER (CRF_ID, NAME, DESCRIPTION, PROJECT_ID)
          SELECT
            COALESCE(MAX(CRF_ID), 0) + 1,
            '${name}',
            '',
            (SELECT MAX(PROJECT_ID) FROM PROJECT)
          FROM CUSTOM_REPORT_FOLDER`;
}

export function insertCustomReportLibraryNode(entityName: string, entityType: string): string {
  return `INSERT INTO CUSTOM_REPORT_LIBRARY_NODE (CRLN_ID, NAME, ENTITY_TYPE, ENTITY_ID, CRL_ID)
          SELECT COALESCE(MAX(CRLN_ID), 0) + 1,
                 '${entityName}',
                 '${entityType}',
                 CASE  WHEN '${entityType}' = 'LIBRARY' THEN (SELECT MAX(PROJECT_ID) FROM PROJECT)
                       WHEN '${entityType}' = 'FOLDER' THEN (SELECT COALESCE(MAX(CRF_ID), 0) + 1 FROM CUSTOM_REPORT_FOLDER)
                       WHEN '${entityType}' = 'CHART' THEN (SELECT COALESCE(MAX(CHART_ID), 0) + 1 FROM CHART_DEFINITION)
                       WHEN '${entityType}' = 'DASHBOARD' THEN (SELECT COALESCE(MAX(CRD_ID), 0) + 1 FROM CUSTOM_REPORT_DASHBOARD)
                       WHEN '${entityType}' = 'REPORT' THEN (SELECT COALESCE(MAX(REPORT_ID), 0) + 1 FROM REPORT_DEFINITION)
                       WHEN '${entityType}' = 'CUSTOM_EXPORT' THEN (SELECT COALESCE(MAX(CRCE_ID), 0) + 1 FROM CUSTOM_REPORT_CUSTOM_EXPORT)
                   END,
                 (SELECT MAX(CRL_ID) FROM CUSTOM_REPORT_LIBRARY)
          FROM CUSTOM_REPORT_LIBRARY_NODE`;
}

export function insertChartDefinition(
  name: string,
  createdBy: string,
  createdOn: string,
  chartType: ChartType,
  ScopeType: ChartScopeType,
) {
  return `INSERT INTO CHART_DEFINITION (CHART_ID, NAME, CHART_TYPE, PROJECT_ID, CREATED_BY, CREATED_ON, SCOPE_TYPE)
          SELECT COALESCE(MAX(CHART_ID), 0) + 1,
                 '${name}',
                 '${chartType}',
                 (SELECT MAX(PROJECT_ID) FROM PROJECT),
                 '${createdBy}',
                 '${createdOn}',
                 '${ScopeType}'
          FROM CHART_DEFINITION`;
}

export function insertChartAxisColumn(label: string, axisRank: number = 0): string {
  return `INSERT INTO CHART_AXIS_COLUMN (CHART_COLUMN_ID, LABEL, AXIS_OPERATION, AXIS_RANK, CHART_DEFINITION_ID)
          SELECT (SELECT QUERY_COLUMN_ID FROM QUERY_COLUMN_PROTOTYPE WHERE LABEL = '${label}'),
                 '',
                 'NONE',
                 ${axisRank},
                 MAX(CHART_ID)
          FROM CHART_DEFINITION`;
}

export function insertChartProjectScope(): string {
  return `INSERT INTO CHART_PROJECT_SCOPE (CHART_PROJECT_SCOPE_ID, CHART_ID, PROJECT_SCOPE)
          SELECT COALESCE(MAX(CHART_PROJECT_SCOPE_ID), 0) + 1,
                 (SELECT MAX(CHART_ID) FROM CHART_DEFINITION),
                 '1'
          FROM CHART_PROJECT_SCOPE`;
}

export function insertChartMeasureColumn(label: string, chartOperation: ChartOperation): string {
  return `INSERT INTO CHART_MEASURE_COLUMN (CHART_COLUMN_ID, LABEL, MEASURE_OPERATION, MEASURE_RANK, CHART_DEFINITION_ID)
          SELECT (SELECT QUERY_COLUMN_ID FROM QUERY_COLUMN_PROTOTYPE WHERE LABEL = '${label}'),
                 '',
                 '${chartOperation}',
                 '0',
                 MAX(CHART_ID)
          FROM CHART_DEFINITION`;
}

export function insertChartScope(entityReferenceType: EntityType): string {
  return `INSERT INTO CHART_SCOPE (CHART_ID, ENTITY_REFERENCE_TYPE, ENTITY_REFERENCE_ID)
          SELECT
            MAX(CHART_ID),
            '${entityReferenceType}',
            (SELECT MAX(PROJECT_ID) FROM PROJECT)
          FROM CHART_DEFINITION`;
}

export function insertCustomReportDashboard(
  name: string,
  createdBy: string,
  createdOn: string,
): string {
  return `INSERT INTO CUSTOM_REPORT_DASHBOARD (CRD_ID, NAME, PROJECT_ID, CREATED_BY, CREATED_ON)
          SELECT COALESCE(MAX(CRD_ID), 0) + 1,
                 '${name}',
                 (SELECT MAX(PROJECT_ID) FROM PROJECT),
                 '${createdBy}',
                 '${createdOn}'
          FROM CUSTOM_REPORT_DASHBOARD`;
}

export function insertReportDefinition(
  name: string,
  createdBy: string,
  createdOn: string,
  pluginNameSpace: PluginNameSpace,
  reportDefinitionParameter: string,
) {
  return `INSERT INTO REPORT_DEFINITION (REPORT_ID, NAME, PLUGIN_NAMESPACE, PARAMETERS, PROJECT_ID, CREATED_BY, CREATED_ON)
          SELECT COALESCE(MAX(REPORT_ID), 0) + 1,
                 '${name}',
                 '${pluginNameSpace}',
                 '${reportDefinitionParameter}',
                 (SELECT MAX(PROJECT_ID) FROM PROJECT ),
                 '${createdBy}',
                 '${createdOn}'
          FROM REPORT_DEFINITION`;
}

export function insertCustomExportColumn(label: CustomColumnEntityType): string {
  return `INSERT INTO CUSTOM_EXPORT_COLUMN (CUSTOM_EXPORT_ID, LABEL)
          SELECT MAX(CRCE_ID),
                 '${label}'
          FROM CUSTOM_REPORT_CUSTOM_EXPORT`;
}

export function insertCustomReportCustomExport(
  name: string,
  createdBy: string,
  createdOn: string,
): string {
  return `INSERT INTO CUSTOM_REPORT_CUSTOM_EXPORT (CRCE_ID, NAME, PROJECT_ID, CREATED_BY, CREATED_ON)
          SELECT COALESCE(MAX(CRCE_ID), 0) + 1,
                 '${name}',
                 (SELECT MAX(PROJECT_ID) FROM PROJECT),
                 '${createdBy}',
                 '${createdOn}'
          FROM CUSTOM_REPORT_CUSTOM_EXPORT`;
}

export function insertCustomExportScope(
  entityReferenceType: CustomExportEntityType,
  entityName: string,
): string {
  return `INSERT INTO CUSTOM_EXPORT_SCOPE (CUSTOM_EXPORT_ID, ENTITY_REFERENCE_TYPE, ENTITY_REFERENCE_ID)
          SELECT MAX(CRCE_ID),
                 '${entityReferenceType}',
                 CASE WHEN '${entityReferenceType}' = 'CAMPAIGN' THEN (SELECT CLN_ID FROM CAMPAIGN_LIBRARY_NODE WHERE NAME = '${entityName}')
                      WHEN '${entityReferenceType}' = 'ITERATION' THEN (SELECT ITERATION_ID FROM ITERATION WHERE NAME = '${entityName}')
                      WHEN '${entityReferenceType}' = 'TEST_SUITE' THEN (SELECT ID FROM TEST_SUITE WHERE NAME = '${entityName}')
                      WHEN '${entityReferenceType}' = 'TEST_CASE' THEN (SELECT TCLN_ID FROM TEST_CASE_LIBRARY_NODE WHERE NAME = '${entityName}')
                      WHEN '${entityReferenceType}' = 'TEST_STEP' THEN (SELECT MAX(STEP_ID) FROM TEST_CASE_STEPS)
                      WHEN '${entityReferenceType}' = 'EXECUTION' THEN (SELECT MAX(EXECUTION_ID) FROM EXECUTION)
                      WHEN '${entityReferenceType}' = 'EXECUTION_STEP' THEN (SELECT MAX(EXECUTION_STEP_ID) FROM EXECUTION_STEP)
                      WHEN '${entityReferenceType}' = 'ISSUE' THEN (SELECT ISSUE_ID FROM ISSUE WHERE REMOTE_ISSUE_ID = '${entityName}')
                   END
          FROM CUSTOM_REPORT_CUSTOM_EXPORT`;
}

export function insertCustomReportChartBinding(
  dashboardName: string,
  chartName: string,
  layout: { row: number; col: number; sizeX: number; sizeY: number },
) {
  return `INSERT INTO CUSTOM_REPORT_CHART_BINDING (CRCB_ID, CRD_ID, CHART_ID, ROW, COL, SIZE_X, SIZE_Y)
          SELECT COALESCE(MAX(CRCB_ID), 0) + 1,
                 (SELECT CRD_ID FROM CUSTOM_REPORT_DASHBOARD WHERE NAME = '${dashboardName}'),
                 (SELECT CHART_ID FROM CHART_DEFINITION WHERE NAME = '${chartName}'),
                 ${layout.row},
                 ${layout.col},
                 ${layout.sizeX},
                 ${layout.sizeY}
          FROM CUSTOM_REPORT_CHART_BINDING`;
}

export function insertCustomReportReportBinding(
  dashboardName: string,
  reportName: string,
  layout: { row: number; col: number; sizeX: number; sizeY: number },
) {
  return `INSERT INTO CUSTOM_REPORT_REPORT_BINDING (CRRB_ID,CRD_ID,REPORT_ID,ROW,COL,SIZE_X,SIZE_Y)
          SELECT COALESCE(MAX(CRRB_ID), 0) + 1,
                 (SELECT CRD_ID FROM CUSTOM_REPORT_DASHBOARD WHERE NAME = '${dashboardName}'),
                 (SELECT REPORT_ID FROM REPORT_DEFINITION WHERE NAME = '${reportName}'),
                 ${layout.row},
                 ${layout.col},
                 ${layout.sizeX},
                 ${layout.sizeY}
          FROM CUSTOM_REPORT_REPORT_BINDING`;
}

export function insertChartFilter(label: string, chartOperation: ChartOperation) {
  return `INSERT INTO CHART_FILTER (FILTER_ID, CHART_COLUMN_ID, FILTER_OPERATION, CUF_ID, CHART_DEFINITION_ID)
          SELECT COALESCE(MAX(FILTER_ID), 0) + 1,
                 (SELECT QUERY_COLUMN_ID FROM QUERY_COLUMN_PROTOTYPE WHERE LABEL = '${label}'),
                 '${chartOperation}',
                 null,
                 (SELECT MAX(CHART_ID) FROM CHART_DEFINITION)
          FROM CHART_FILTER`;
}

export function insertChartFilterValues(value: string) {
  return `INSERT INTO CHART_FILTER_VALUES (FILTER_ID, FILTER_VALUE)
          SELECT MAX(FILTER_ID),
                 '${value}'
          FROM CHART_FILTER`;
}

export function insertCustomColumnConfiguration(
  partyId: number,
  gridId: string,
  projectId: number,
): string {
  return `INSERT INTO GRID_COLUMN_DISPLAY_REFERENCE (GCDR_ID, PARTY_ID, GRID_ID, PROJECT_ID)
          SELECT COALESCE(MAX(GCDR_ID), 0) + 1,
                 '${partyId}',
                 '${gridId}',
                 '${projectId}'
          FROM GRID_COLUMN_DISPLAY_REFERENCE`;
}
export function insertCustomColumnReference(activeColumnId: string) {
  return `INSERT INTO GRID_COLUMN_DISPLAY_CONFIGURATION (GCDC_ID, GCDR_ID, ACTIVE_COLUMN_ID)
          SELECT COALESCE(MAX(GCDC_ID), 0) + 1,
                 (SELECT MAX(GCDR_ID) FROM GRID_COLUMN_DISPLAY_REFERENCE),
                 '${activeColumnId}'
          FROM GRID_COLUMN_DISPLAY_CONFIGURATION`;
}

// ***********
// * PROFILE *
// ***********
export function insertProfile(
  qualifiedName: string,
  active: boolean,
  description: string,
  createdBy: string,
  createdOn: string,
  lastModifiedByStr: string,
  lastModifiedOnStr: string,
) {
  return `INSERT INTO ACL_GROUP (ID, QUALIFIED_NAME, ACTIVE, DESCRIPTION, CREATED_BY, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON)
          SELECT COALESCE(MAX(ID), 0) + 1,
                 '${qualifiedName}',
                 ${active},
                 '${description}',
                 '${createdBy}',
                 '${createdOn}',
                 ${lastModifiedByStr},
                 ${lastModifiedOnStr}
          FROM ACL_GROUP`;
}

export function insertProfilePermission(classId: number, permissionMask: number) {
  return `INSERT INTO ACL_GROUP_PERMISSION (ACL_GROUP_ID, PERMISSION_MASK, CLASS_ID, PERMISSION_ORDER, GRANTING)
          VALUES ((SELECT MAX(ID) FROM ACL_GROUP), ${permissionMask}, ${classId}, 0, true)`;
}
