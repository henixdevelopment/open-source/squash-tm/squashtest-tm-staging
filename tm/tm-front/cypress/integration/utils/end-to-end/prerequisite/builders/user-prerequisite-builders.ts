import { AuditBuilder } from './common-builders';
import {
  AclGroup,
  AclGroupName,
  DEFAULT_USER_PASSWORD,
  formatDate,
  formatString,
  getProjectAclClasses,
} from '../prerequisite-helper';
import {
  insertAclResponsibilityScopeEntryForTeam,
  insertAuthUser,
  insertCoreParty,
  insertCoreTeam,
  insertCoreTeamMembers,
  insertCoreUser,
  insertCoreUserMember,
} from '../prerequisite-queries';

export type CoreGroupName = 'User' | 'Admin' | 'TestAutomationServer';

export const CoreGroup: { [K in CoreGroupName]: number } = {
  Admin: 1,
  User: 2,
  TestAutomationServer: 4,
};

export class UserBuilder extends AuditBuilder {
  private readonly login: string;
  private firstName = '';
  private lastName: string;
  private email = '';
  private groupName: CoreGroupName;
  private active = true;
  private lastConnectedOn: string;
  private canDeleteFromFront = true;
  private password = DEFAULT_USER_PASSWORD;

  constructor(login: string) {
    super();
    this.login = login;
  }

  withFirstName(firstName: string) {
    this.firstName = firstName;
    return this;
  }

  withLastName(lastName: string) {
    this.lastName = lastName;
    return this;
  }

  withEmail(email: string) {
    this.email = email;
    return this;
  }

  withActive(active: boolean) {
    this.active = active;
    return this;
  }

  withGroupName(groupName: CoreGroupName) {
    this.groupName = groupName;
    return this;
  }

  withLastConnection(lastConnectedOn: Date | string) {
    this.lastConnectedOn = formatDate(lastConnectedOn);
    return this;
  }

  disableDeleteFromFront() {
    this.canDeleteFromFront = false;
    return this;
  }

  withPassword(password: string) {
    this.password = password;
    return this;
  }

  build(): string[] {
    const groupId: number = this.groupName ? CoreGroup[this.groupName] : CoreGroup.User;
    const lastModifiedByStr: string = formatString(this.getLastModifiedByStr());
    const lastModifiedOnStr: string = formatString(this.getLastModifiedOnStr());
    const lastConnectedOnStr: string = formatString(this.lastConnectedOn);
    const lastName: string = this.lastName || this.login;

    return [
      insertCoreParty(),
      insertCoreUser(
        this.login,
        this.firstName,
        lastName,
        this.email,
        this.getCreatedBy(),
        this.getCreatedOn(),
        lastModifiedByStr,
        lastModifiedOnStr,
        this.active,
        lastConnectedOnStr,
        this.canDeleteFromFront,
      ),
      insertCoreUserMember(groupId),
      insertAuthUser(this.login, this.password, this.active),
    ].flat();
  }
}

export class TeamBuilder extends AuditBuilder {
  private readonly name: string;
  private description = '';
  private userLogins: string[] = [];

  constructor(name: string) {
    super();
    this.name = name;
  }

  withDescription(description: string) {
    this.description = description;
    return this;
  }

  withUserLogins(userLogins: string[]) {
    this.userLogins = userLogins;
    return this;
  }

  build(): string[] {
    return [
      insertCoreParty(),
      insertCoreTeam(
        this.name,
        this.getCreatedBy(),
        this.getCreatedOn(),
        this.getLastModifiedByStr(),
        this.getLastModifiedOnStr(),
        this.description,
      ),
      this.associateUsers(this.name, this.userLogins),
    ].flat();
  }

  private associateUsers(teamName: string, userLogins: string[]): string {
    return this.userLogins ? insertCoreTeamMembers(teamName, userLogins) : '';
  }
}

export class TeamAndProfileOnProjectBuilder {
  private readonly teamName: string;
  private readonly profileOnProject: AclGroupName;

  constructor(teamName: string, profileOnProject: AclGroupName) {
    this.teamName = teamName;
    this.profileOnProject = profileOnProject;
  }

  build(projectName: string): string[] {
    return [this.buildPermissions(projectName)].flat();
  }

  private buildPermissions(projectName: string) {
    return getProjectAclClasses()
      .map((aclClass, classIndex, array) => {
        return insertAclResponsibilityScopeEntryForTeam(
          this.teamName,
          AclGroup[this.profileOnProject],
          array,
          classIndex,
          projectName,
        );
      })
      .flat();
  }
}
