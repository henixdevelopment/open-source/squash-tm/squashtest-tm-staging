import {
  bindCategoryRequirement,
  bindNatureTestCase,
  bindRequirementCategoriesListToProject,
  bindTestCaseNatureListToProject,
  bindTestCaseTypeListToProject,
  bindTypeTestCase,
  insertInfoList,
  insertInfoListItem,
} from '../prerequisite-queries';
import { AuditBuilder } from './common-builders';

export class InfoListBuilder extends AuditBuilder {
  private labelList: string;
  private codeList: string;
  private description = '';
  private infoListItems: InfoListItemBuilder[] = [];
  constructor(labelList: string, codeList: string) {
    super();
    this.labelList = labelList;
    this.codeList = codeList;
  }
  withDescription(description: string) {
    this.description = description;
    return this;
  }
  withItems(infoListItem: InfoListItemBuilder[]) {
    this.infoListItems = infoListItem;
    return this;
  }
  build() {
    return [
      insertInfoList(
        this.labelList,
        this.description,
        this.codeList,
        this.getCreatedBy(),
        this.getCreatedOn(),
        this.getLastModifiedByStr(),
        this.getLastModifiedOnStr(),
      ),
      ...this.infoListItems.map((i) => i.build()),
    ].flat();
  }
}

export class InfoListItemBuilder {
  private codeOption = '';
  private labelOption = '';
  private icon = '';
  private color = null;

  constructor(labelOption: string, codeOption: string) {
    this.labelOption = labelOption;
    this.codeOption = codeOption;
  }
  withIcon(icon: string) {
    this.icon = icon;
    return this;
  }
  withColor(color: string) {
    this.color = color;
    return this;
  }
  build() {
    return [insertInfoListItem(this.labelOption, this.codeOption, this.icon, this.color)].flat();
  }
}

export class RequirementCategoriesListBindingBuilder {
  private nameList: string;
  private label: string;
  private id: number;

  constructor(nameList: string, label: string, id: number) {
    this.nameList = nameList;
    this.label = label;
    this.id = id;
  }

  build() {
    return [
      bindRequirementCategoriesListToProject(this.nameList),
      bindCategoryRequirement(this.label, this.id),
    ].flat();
  }
}

export class TestCaseTypeListBindingBuilder {
  private nameList: string;
  private label: string;
  private idTC: number;

  constructor(nameList: string, label: string, idTC: number) {
    this.nameList = nameList;
    this.label = label;
    this.idTC = idTC;
  }

  build() {
    return [
      bindTestCaseTypeListToProject(this.nameList),
      bindTypeTestCase(this.label, this.idTC),
    ].flat();
  }
}

export class TestCaseNatureListBindingBuilder {
  private nameList: string;
  private label: string;
  private idTC: number;

  constructor(nameList: string, label: string, idTC: number) {
    this.nameList = nameList;
    this.label = label;
    this.idTC = idTC;
  }

  build() {
    return [
      bindTestCaseNatureListToProject(this.nameList),
      bindNatureTestCase(this.label, this.idTC),
    ].flat();
  }
}
