import { NodeBuilder } from './common-builders';
import {
  insertActionWord,
  insertActionWordFragment,
  insertActionWordLibraryNode,
  insertActionWordParameter,
  insertActionWordText,
  insertAwlnRelationship,
} from '../prerequisite-queries';

export class ActionWordLibraryNodeBuilder extends NodeBuilder {
  private token: string;
  private actionWordFragments: ActionWordFragmentBuilder[] = [];
  constructor(name: string, token: string) {
    super(name);
    this.token = token;
  }

  buildRootNode(): string[] {
    return [
      this.actionWordLibraryNodeBaseQueries(),
      ...this.actionWordFragments.map((actionWordFragment, i) => actionWordFragment.build(i)),
    ].flat();
  }

  build(_index: number, _parentIndex: number, _parentName: string): string[] {
    return [
      this.actionWordLibraryNodeBaseQueries(),
      ...this.actionWordFragments.map((actionWordFragment, i) => actionWordFragment.build(i)),
    ].flat();
  }

  private actionWordLibraryNodeBaseQueries(): string[] {
    return [
      insertActionWord(this.token),
      insertActionWordLibraryNode(this.getName()),
      insertAwlnRelationship(),
    ].flat();
  }
  withFragments(actionWordFragments: ActionWordFragmentBuilder[]) {
    this.actionWordFragments = actionWordFragments;
    return this;
  }
}

export interface ActionWordFragmentBuilder {
  build(index: number): string[];
}

export class ActionWordParameterBuilder implements ActionWordFragmentBuilder {
  private readonly paramName: string;
  private readonly paramDefaultValue: string;

  constructor(paramName: string, paramDefaultValue: string) {
    this.paramName = paramName;
    this.paramDefaultValue = paramDefaultValue;
  }

  build(index: number): string[] {
    return [
      insertActionWordFragment(index),
      insertActionWordParameter(this.paramName, this.paramDefaultValue),
    ].flat();
  }
}

export class ActionWordTextBuilder implements ActionWordFragmentBuilder {
  private readonly actionWordText: string;

  constructor(actionWordText: string) {
    this.actionWordText = actionWordText;
  }

  build(index: number): string[] {
    return [insertActionWordFragment(index), insertActionWordText(this.actionWordText)].flat();
  }
}
