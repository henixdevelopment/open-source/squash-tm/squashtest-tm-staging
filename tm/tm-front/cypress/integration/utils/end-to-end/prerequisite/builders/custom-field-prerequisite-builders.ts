import {
  insertAttachmentList,
  insertCustomField,
  insertCustomFieldBinding,
  insertCustomFieldOption,
} from '../prerequisite-queries';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { formatString } from '../prerequisite-helper';

export type CustomFieldType = 'CF' | 'SSF' | 'MSF' | 'RTF' | 'NUM';

export const CustomFieldTypeByInputType: { [K in InputType]: CustomFieldType } = {
  DATE_PICKER: 'CF',
  CHECKBOX: 'CF',
  TAG: 'MSF',
  RICH_TEXT: 'RTF',
  PLAIN_TEXT: 'CF',
  NUMERIC: 'NUM',
  DROPDOWN_LIST: 'SSF',
};

export class CustomFieldBuilder {
  private readonly name: string;
  private readonly inputType: InputType;
  private label: string;
  private code: string;
  private fieldType: CustomFieldType;
  private options: ListOptionCustomFieldBuilder[] = [];

  constructor(name: string, inputType: InputType) {
    this.name = name;
    this.label = name;
    this.code = name;
    this.inputType = inputType;
    this.fieldType = CustomFieldTypeByInputType[inputType];
  }

  withLabel(label: string) {
    this.label = label;
    return this;
  }

  withCode(code: string) {
    this.code = code;
    return this;
  }

  withOptions(options: ListOptionCustomFieldBuilder[]) {
    this.options = options;
    return this;
  }

  build(): string[] {
    return [
      insertCustomField(this.name, this.label, this.code, this.inputType, this.fieldType),
      ...this.options.map((option, i) => option.build(i)),
    ].flat();
  }
}

export class ListOptionCustomFieldBuilder {
  private readonly label: string;
  private readonly code: string;
  private colour = '';

  constructor(label: string, code: string) {
    this.label = label;
    this.code = code;
  }

  withColour(colour: string) {
    this.colour = colour;
    return this;
  }

  build(index: number): string[] {
    const colourString = formatString(this.colour);

    return [insertCustomFieldOption(this.label, index, this.code, colourString)].flat();
  }
}

export class CustomFieldOnProjectBuilder {
  private readonly cufName: string;
  private readonly boundEntity: BindableEntity;

  constructor(cufName: string, boundEntity: BindableEntity) {
    this.cufName = cufName;
    this.boundEntity = boundEntity;
  }

  build(): string[] {
    return [
      insertAttachmentList(),
      insertCustomFieldBinding(this.cufName, this.boundEntity),
    ].flat();
  }
}

export class CustomFieldValue {
  private readonly cufName: string;
  private readonly value: string;

  constructor(cufName: string, value: string) {
    this.cufName = cufName;
    this.value = value;
  }

  public getCufName(): string {
    return this.cufName;
  }

  public getValue(): string {
    return this.value;
  }
}
