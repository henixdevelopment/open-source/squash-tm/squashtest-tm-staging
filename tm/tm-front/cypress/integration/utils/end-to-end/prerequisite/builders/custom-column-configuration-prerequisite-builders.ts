import {
  insertCustomColumnConfiguration,
  insertCustomColumnReference,
} from '../prerequisite-queries';

export class CustomColumnConfigurationBuilder {
  private partyId: number;
  private gridId: string;
  private projectId: number;
  private activeColumnIds: string[];

  withPartyId(partyId: number) {
    this.partyId = partyId;
    return this;
  }

  withGridId(gridId: string) {
    this.gridId = gridId;
    return this;
  }

  withProjectId(projectId: number) {
    this.projectId = projectId;
    return this;
  }

  withActiveColumnIds(activeColumnIds: string[]) {
    this.activeColumnIds = activeColumnIds;
    return this;
  }

  build() {
    return [
      insertCustomColumnConfiguration(this.partyId, this.gridId, this.projectId),
      ...this.activeColumnIds.map((activeColumnId) => {
        return insertCustomColumnReference(activeColumnId);
      }),
    ];
  }
}
