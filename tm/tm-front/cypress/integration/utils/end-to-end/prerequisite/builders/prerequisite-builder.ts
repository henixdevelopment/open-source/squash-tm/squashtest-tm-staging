import { DatabaseUtils } from '../../../database.utils';
import { ProjectBuilder } from './project-prerequisite-builders';
import { CustomFieldBuilder } from './custom-field-prerequisite-builders';
import { ThirdPartyServerBuilder } from './server-prerequisite-builders';
import { MilestoneBuilder } from './milestone-prerequisite-builders';
import { TeamBuilder, UserBuilder } from './user-prerequisite-builders';
import { InfoListBuilder } from './info-list-builder';
import { CustomColumnConfigurationBuilder } from './custom-column-configuration-prerequisite-builders';
import { ProfileBuilder } from './profile-prerequisite-builders';

export type ExecutionStatus = 'READY';

export class PrerequisiteBuilder {
  private projects: ProjectBuilder[] = [];
  private users: UserBuilder[] = [];
  private teams: TeamBuilder[] = [];
  private profiles: ProfileBuilder[] = [];
  private milestones: MilestoneBuilder[] = [];
  private cufs: CustomFieldBuilder[] = [];
  private servers: ThirdPartyServerBuilder[] = [];
  private infoLists: InfoListBuilder[] = [];
  private customColumnConfiguration: CustomColumnConfigurationBuilder[] = [];

  withProjects(projects: ProjectBuilder[]) {
    this.projects = projects;
    return this;
  }

  withUsers(users: UserBuilder[]) {
    this.users = users;
    return this;
  }

  withTeams(teams: TeamBuilder[]) {
    this.teams = teams;
    return this;
  }

  withProfiles(profiles: ProfileBuilder[]) {
    this.profiles = profiles;
    return this;
  }

  withMilestones(milestones: MilestoneBuilder[]) {
    this.milestones = milestones;
    return this;
  }

  withCufs(cufs: CustomFieldBuilder[]) {
    this.cufs = cufs;
    return this;
  }

  withServers(servers: ThirdPartyServerBuilder[]) {
    this.servers = servers;
    return this;
  }
  withInfoLists(infoLists: InfoListBuilder[]) {
    this.infoLists = infoLists;
    return this;
  }

  withCustomColumnConfiguration(customColumnConfiguration: CustomColumnConfigurationBuilder[]) {
    this.customColumnConfiguration = customColumnConfiguration;
    return this;
  }

  build(): void {
    const requests = [
      ...this.profiles.map((profile) => profile.build()),
      ...this.users.map((user) => user.build()),
      ...this.teams.map((t) => t.build()),
      ...this.milestones.map((m) => m.build()),
      ...this.cufs.map((c) => c.build()),
      ...this.servers.map((server) => server.build()),
      ...this.infoLists.map((l) => l.build()),
      ...this.projects.map((p) => p.build()),
      ...this.customColumnConfiguration.map((ccc) => ccc.build()),
    ].flat();
    const flattenRequests = requests.join(';\n');
    console.log(flattenRequests);
    DatabaseUtils.executeQuery(flattenRequests).then(() => {
      DatabaseUtils.resetAllSequences();
    });
  }
}
