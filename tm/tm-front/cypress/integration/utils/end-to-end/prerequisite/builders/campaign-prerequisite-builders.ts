import {
  insertAttachmentList,
  insertCampaign,
  insertCampaignFolder,
  insertCampaignIteration,
  insertCampaignLibraryContent,
  insertCampaignLibraryNode,
  insertCampaignTestPlanItem,
  insertClnRelationship,
  insertCustomFieldValueFromEntity,
  insertExecution,
  insertExecutionExecutionStep,
  insertExecutionForSprint,
  insertExecutionStep,
  insertExploratoryExecution,
  insertExploratorySessionForIteration,
  insertExploratorySessionForSprint,
  insertIssue,
  insertIssueList,
  insertItemTestPlanExecution,
  insertItemTestPlanList,
  insertIteration,
  insertIterationTestPlanItem,
  insertIterationTestSuite,
  insertKeywordExecution,
  insertLastExecutedByAndOnInExecutionStep,
  insertLastExecutedOnAndByInIterationTestPlanItem,
  insertMilestoneCampaign,
  insertScriptedExecution,
  insertSprint,
  insertSprintGroup,
  insertSprintReqVersion,
  insertSprintTestPlan,
  insertSprintTestPlanItem,
  insertTestSuite,
  insertTestSuiteTestPlanItems,
  updateExecutionForExploratoryExecution,
} from '../prerequisite-queries';
import { AuditBuilder, NodeBuilder } from './common-builders';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { formatDate, formatString, newDateAsString } from '../prerequisite-helper';
import { ExecutionStatusKeys } from '../../../../../../projects/sqtm-core/src/lib/model/level-enums/level-enum';

export abstract class CampaignLibraryNodeBuilder extends NodeBuilder {
  constructor(name: string) {
    super(name);
  }

  buildRootNode(index: number): string[] {
    return [this.campaignLibraryNodeBaseQueries(), insertCampaignLibraryContent(index)].flat();
  }

  build(_index: number, _parentIndex: number, _parentName: string): string[] {
    return this.campaignLibraryNodeBaseQueries();
  }

  private campaignLibraryNodeBaseQueries(): string[] {
    return [
      insertAttachmentList(),
      insertCampaignLibraryNode(
        this.getName(),
        this.getDescription(),
        this.getCreatedBy(),
        this.getCreatedOn(),
        this.getLastModifiedByStr(),
        this.getLastModifiedOnStr(),
      ),
    ].flat();
  }
}

export class SprintGroupBuilder extends CampaignLibraryNodeBuilder {
  private campaignLibraryNodes: CampaignLibraryNodeBuilder[] = [];

  withCampaignLibraryNodes(campaignLibraryNodes: CampaignLibraryNodeBuilder[]) {
    this.campaignLibraryNodes = campaignLibraryNodes;
    return this;
  }

  override build(index: number, parentIndex: number, parentName: string): string[] {
    return [
      super.build(index, parentIndex, parentName),
      this.sprintGroupBaseQueries(),
      insertClnRelationship(parentName, index),
      this.sprintGroupLastQueries(index),
    ].flat();
  }

  override buildRootNode(index: number): string[] {
    return [
      super.buildRootNode(index),
      this.sprintGroupBaseQueries(),
      this.sprintGroupLastQueries(index),
    ].flat();
  }

  private sprintGroupBaseQueries(): string[] {
    return [
      insertSprintGroup(),
      insertCustomFieldValueFromEntity(BindableEntity.SPRINT_GROUP, 'CLN_ID', 'SPRINT_GROUP'),
    ].flat();
  }

  private sprintGroupLastQueries(index: number): string[] {
    return [
      ...this.campaignLibraryNodes.map((campaignLibraryNode, i) =>
        campaignLibraryNode.build(i, index, this.getName()),
      ),
    ].flat();
  }
}

export class SprintBuilder extends CampaignLibraryNodeBuilder {
  private milestone: string;
  private sprintReqVersions: SprintReqVersionBuilder[] = [];

  withMilestone(milestone: string) {
    this.milestone = milestone;
    return this;
  }

  withSprintReqVersions(sprintReqVersions: SprintReqVersionBuilder[]) {
    this.sprintReqVersions = sprintReqVersions;
    return this;
  }

  override build(index: number, parentIndex: number, parentName: string): string[] {
    return [
      super.build(index, parentIndex, parentName),
      this.sprintBaseQueries(),
      this.sprintNonRootQueries(index, parentName),
      this.sprintLastQueries(),
    ].flat();
  }

  override buildRootNode(index: number): string[] {
    return [super.buildRootNode(index), this.sprintBaseQueries(), this.sprintLastQueries()].flat();
  }

  private sprintBaseQueries(): string[] {
    return [insertSprint(this.getReference())].flat();
  }

  private sprintNonRootQueries(index: number, parentName: string): string[] {
    return [insertClnRelationship(parentName, index)].flat();
  }

  private sprintLastQueries(): string[] {
    return [
      ...this.sprintReqVersions.map((sprintReqVersion) => sprintReqVersion.build()),
      insertCustomFieldValueFromEntity(BindableEntity.SPRINT, 'CLN_ID', 'SPRINT'),
      this.milestone ? insertMilestoneCampaign(this.milestone) : [],
    ].flat();
  }
}

export class SprintReqVersionBuilder extends AuditBuilder {
  private sprintName: string;
  private reqVersionName: string;
  private testPlanItems: SprintTestPlanItemBuilder[] = [];

  constructor(sprintName: string, reqVersionName: string) {
    super();
    this.sprintName = sprintName;
    this.reqVersionName = reqVersionName;
  }

  withTestPlanItems(testPlanItems: SprintTestPlanItemBuilder[]) {
    this.testPlanItems = testPlanItems;
    return this;
  }

  build(): string[] {
    return [
      insertAttachmentList(),
      insertSprintTestPlan(this.sprintName),
      insertSprintReqVersion(
        this.sprintName,
        this.reqVersionName,
        this.getCreatedBy(),
        this.getCreatedOn(),
        this.getLastModifiedByStr(),
        this.getLastModifiedOnStr(),
      ),
      ...this.testPlanItems.map((testPlanItem) => testPlanItem.build()),
    ].flat();
  }
}

export class CampaignFolderBuilder extends CampaignLibraryNodeBuilder {
  private campaignLibraryNodes: CampaignLibraryNodeBuilder[] = [];

  withCampaignLibraryNodes(campaignLibraryNodes: CampaignLibraryNodeBuilder[]) {
    this.campaignLibraryNodes = campaignLibraryNodes;
    return this;
  }

  override build(index: number, parentIndex: number, parentName: string): string[] {
    return [
      super.build(index, parentIndex, parentName),
      this.campaignFolderBaseQueries(),
      insertClnRelationship(parentName, index),
      this.campaignFolderLastQueries(index),
    ].flat();
  }

  override buildRootNode(index: number): string[] {
    return [
      super.buildRootNode(index),
      this.campaignFolderBaseQueries(),
      this.campaignFolderLastQueries(index),
    ].flat();
  }

  private campaignFolderBaseQueries(): string[] {
    return [
      insertCampaignFolder(),
      insertCustomFieldValueFromEntity(BindableEntity.CAMPAIGN_FOLDER, 'CLN_ID', 'CAMPAIGN_FOLDER'),
    ].flat();
  }

  private campaignFolderLastQueries(index: number): string[] {
    return [
      ...this.campaignLibraryNodes.map((campaignLibraryNode, i) =>
        campaignLibraryNode.build(i, index, this.getName()),
      ),
    ].flat();
  }
}

export class CampaignBuilder extends CampaignLibraryNodeBuilder {
  private iterations: IterationBuilder[] = [];
  private testplanItems: CampaignTestPlanItemBuilder[] = [];
  private milestone: string;
  withIterations(iterations: IterationBuilder[]) {
    this.iterations = iterations;
    return this;
  }

  withTestPlanItems(testplanItems: CampaignTestPlanItemBuilder[]) {
    this.testplanItems = testplanItems;
    return this;
  }

  withMilestone(milestone: string) {
    this.milestone = milestone;
    return this;
  }

  override build(index: number, parentIndex: number, parentName: string): string[] {
    return [
      super.build(index, parentIndex, parentName),
      this.campaignBaseQueries(),
      this.campaignNonRootQueries(index, parentName),
      this.campaignLastQueries(),
    ].flat();
  }

  override buildRootNode(index: number): string[] {
    return [
      super.buildRootNode(index),
      this.campaignBaseQueries(),
      this.campaignLastQueries(),
    ].flat();
  }

  private campaignBaseQueries(): string[] {
    return [insertCampaign(this.getReference())].flat();
  }

  private campaignNonRootQueries(index: number, parentName: string): string[] {
    return [insertClnRelationship(parentName, index)].flat();
  }

  private campaignLastQueries(): string[] {
    return [
      ...this.iterations.map((iteration) => iteration.build()),
      ...this.testplanItems.map((testPlanItem) => testPlanItem.build()),
      this.milestone ? insertMilestoneCampaign(this.milestone) : [],
      insertCustomFieldValueFromEntity(BindableEntity.CAMPAIGN, 'CLN_ID', 'CAMPAIGN_LIBRARY_NODE'),
    ].flat();
  }
}

abstract class TestPlanItemBuilder extends AuditBuilder {
  private readonly testCaseName: string;
  private userLogin: string;
  private datasetName: string;

  protected constructor(testCaseName: string) {
    super();
    this.testCaseName = testCaseName;
  }

  withUserLogin(userLogin: string) {
    this.userLogin = userLogin;
    return this;
  }

  withDatasetName(datasetName: string) {
    this.datasetName = datasetName;
    return this;
  }

  protected getTestCaseName(): string {
    return this.testCaseName;
  }

  protected getUserLoginStr(): string {
    return formatString(this.userLogin);
  }

  protected getDatasetNameStr(): string {
    return formatString(this.datasetName);
  }

  build(): string[] {
    return [];
  }
}

export class CampaignTestPlanItemBuilder extends TestPlanItemBuilder {
  constructor(testCaseName: string) {
    super(testCaseName);
  }

  override build(): string[] {
    return [
      insertCampaignTestPlanItem(
        this.getTestCaseName(),
        this.getUserLoginStr(),
        this.getDatasetNameStr(),
      ),
    ].flat();
  }
}

export class SprintTestPlanItemBuilder extends TestPlanItemBuilder {
  private executions: ExecutionBuilder[] = [];
  private sessions: ExploratorySessionBuilder[] = [];
  readonly status: ExecutionStatusKeys;
  private reqVersionName: string;

  constructor(testCaseName: string, reqVersionName: string, status: ExecutionStatusKeys = 'READY') {
    super(testCaseName);
    this.status = status;
    this.reqVersionName = reqVersionName;
  }

  withExecutions(executions: ExecutionBuilder[]) {
    this.executions = executions;
    return this;
  }

  withSessions(sessions: ExploratorySessionBuilder[]) {
    this.sessions = sessions;
    return this;
  }

  override build(): string[] {
    const status = formatString(this.status);
    return [
      insertSprintTestPlanItem(
        this.getTestCaseName(),
        this.getCreatedBy(),
        this.getCreatedOn(),
        this.getLastModifiedByStr(),
        this.getLastModifiedOnStr(),
        this.getUserLoginStr(),
        this.getDatasetNameStr(),
        status,
      ),
      ...this.executions.map((execution, i) => execution.build(this.getTestCaseName(), i)),
      ...this.sessions.map((session) => session.build()),
    ].flat();
  }
}

export class IterationTestPlanItemBuilder extends TestPlanItemBuilder {
  private executions: ExecutionBuilder[] = [];
  private sessions: ExploratorySessionBuilder[] = [];
  readonly status: ExecutionStatusKeys;

  constructor(testCaseName: string, status: ExecutionStatusKeys = 'READY') {
    super(testCaseName);
    this.status = status;
  }

  withExecutions(executions: ExecutionBuilder[]) {
    this.executions = executions;
    return this;
  }

  withSessions(sessions: ExploratorySessionBuilder[]) {
    this.sessions = sessions;
    return this;
  }

  override build(): string[] {
    const status = formatString(this.status);
    return [
      insertIterationTestPlanItem(
        this.getTestCaseName(),
        this.getCreatedBy(),
        this.getCreatedOn(),
        this.getLastModifiedByStr(),
        this.getLastModifiedOnStr(),
        this.getUserLoginStr(),
        this.getDatasetNameStr(),
        status,
      ),
      insertItemTestPlanList(),
      ...this.executions.map((execution, i) => execution.build(this.getTestCaseName(), i)),
      ...this.sessions.map((session) => session.build()),
    ].flat();
  }
}

export class TestSuiteBuilder extends AuditBuilder {
  private readonly name: string;
  private description = '';
  private testCaseNames: string[] = [];
  private status: ExecutionStatusKeys;

  constructor(name: string, status: ExecutionStatusKeys = 'READY') {
    super();
    this.name = name;
    this.status = status;
  }

  withTestCaseNames(testCaseNames: string[]) {
    this.testCaseNames = testCaseNames;
    return this;
  }

  build(): string[] {
    return [
      insertAttachmentList(),
      insertTestSuite(
        this.name,
        this.description,
        this.getCreatedBy(),
        this.getCreatedOn(),
        this.getLastModifiedByStr(),
        this.getLastModifiedOnStr(),
        this.status,
      ),
      insertIterationTestSuite(),
      this.buildTestPlanItems(this.testCaseNames),
      insertCustomFieldValueFromEntity(
        BindableEntity.TEST_SUITE,
        'CLN_ID',
        'CAMPAIGN_LIBRARY_NODE',
      ),
    ].flat();
  }

  private buildTestPlanItems(testCaseNames: string[]): string {
    return testCaseNames ? insertTestSuiteTestPlanItems(testCaseNames) : '';
  }
}

export class IterationBuilder extends NodeBuilder {
  private testSuites: TestSuiteBuilder[] = [];
  private testplanItems: IterationTestPlanItemBuilder[] = [];

  constructor(name: string) {
    super(name);
  }

  withTestSuites(testSuites: TestSuiteBuilder[]) {
    this.testSuites = testSuites;
    return this;
  }

  withTestPlanItems(testplanItems: IterationTestPlanItemBuilder[]) {
    this.testplanItems = testplanItems;
    return this;
  }

  build(): string[] {
    return [
      insertAttachmentList(),
      insertIteration(
        this.getName(),
        this.getReference(),
        this.getDescription(),
        this.getCreatedBy(),
        this.getCreatedOn(),
        this.getLastModifiedByStr(),
        this.getLastModifiedOnStr(),
      ),
      insertCampaignIteration(),
      ...this.testplanItems.map((iterationTestPlanItem) => iterationTestPlanItem.build()),
      ...this.testSuites.map((testSuite) => testSuite.build()),
      insertCustomFieldValueFromEntity(BindableEntity.ITERATION, 'CLN_ID', 'CAMPAIGN_LIBRARY_NODE'),
    ].flat();
  }
}

abstract class ExecutionBuilder extends AuditBuilder {
  private comment: string;
  private executionSteps: ExecutionStepBuilder[] = [];
  protected status: ExecutionStatusKeys;
  private issues: DeclareIssueBuilder[] = [];
  private isSprintExecution = false;

  protected constructor(status: ExecutionStatusKeys) {
    super();
    this.status = status;
  }

  forSprint() {
    this.isSprintExecution = true;
    return this;
  }

  withComment(comment: string) {
    this.comment = comment;
    return this;
  }

  withDeclareIssue(issues: DeclareIssueBuilder[]) {
    this.issues = issues;
    return this;
  }

  withExecutionStep(executionSteps: ExecutionStepBuilder[]) {
    this.executionSteps = executionSteps;
    return this;
  }

  build(testCaseName: string, index: number, datasetName?: string): string[] {
    const commentStr = formatString(this.comment);
    const status = formatString(this.status);

    if (this.isSprintExecution) {
      return [
        insertAttachmentList(),
        insertIssueList(),
        insertExecutionForSprint(
          testCaseName,
          index,
          this.getCreatedBy(),
          this.getCreatedOn(),
          this.getLastModifiedByStr(),
          this.getLastModifiedOnStr(),
          commentStr,
          status,
          datasetName ?? null,
        ),
        insertCustomFieldValueFromEntity(BindableEntity.EXECUTION, 'EXECUTION_ID', 'EXECUTION'),
        ...this.executionSteps.map((execution) => execution.build()),
        ...this.issues.map((issue) => issue.build()),
      ].flat();
    } else {
      return [
        insertAttachmentList(),
        insertIssueList(),
        insertExecution(
          testCaseName,
          this.getCreatedBy(),
          this.getCreatedOn(),
          this.getLastModifiedByStr(),
          this.getLastModifiedOnStr(),
          commentStr,
          status,
        ),
        insertItemTestPlanExecution(index),
        insertCustomFieldValueFromEntity(BindableEntity.EXECUTION, 'EXECUTION_ID', 'EXECUTION'),
        ...this.executionSteps.map((execution) => execution.build()),
        ...this.issues.map((issue) => issue.build()),
      ].flat();
    }
  }
}

export class ClassicExecutionBuilder extends ExecutionBuilder {
  constructor(status: ExecutionStatusKeys = 'READY') {
    super(status);
  }
  build(testCaseName: string, index: number): string[] {
    return [super.build(testCaseName, index)].flat();
  }
}

export class KeywordExecutionBuilder extends ExecutionBuilder {
  private datasetName: string;

  constructor(status: ExecutionStatusKeys = 'READY') {
    super(status);
  }

  override build(testCaseName: string, index: number): string[] {
    return [super.build(testCaseName, index, this.datasetName), insertKeywordExecution()].flat();
  }

  withDatasetName(datasetName: string) {
    this.datasetName = datasetName;
    return this;
  }
}

export class ScriptedExecutionBuilder extends ExecutionBuilder {
  constructor(status: ExecutionStatusKeys = 'READY') {
    super(status);
  }

  override build(testCaseName: string, index: number): string[] {
    return [super.build(testCaseName, index), insertScriptedExecution()].flat();
  }
}

export class ExploratorySessionBuilder {
  private testCaseName: string;
  private reqVersionName: string;
  private exploratoryExecutions: ExploratoryExecutionBuilder[] = [];
  private isSprintSession: boolean = false;

  forSprint() {
    this.isSprintSession = true;
    return this;
  }

  constructor(testCaseName: string, reqVersionName?: string) {
    this.testCaseName = testCaseName;
    this.reqVersionName = reqVersionName;
    return this;
  }

  withExploratoryExecutions(exploratoryExecutions: ExploratoryExecutionBuilder[]) {
    this.exploratoryExecutions = exploratoryExecutions;
    return this;
  }

  build(): string[] {
    if (this.isSprintSession) {
      return [
        insertAttachmentList(),
        insertExploratorySessionForSprint(this.testCaseName, this.reqVersionName),
        ...this.exploratoryExecutions.map((exploExec, i) => exploExec.build(this.testCaseName, i)),
      ].flat();
    } else {
      return [
        insertAttachmentList(),
        insertExploratorySessionForIteration(),
        ...this.exploratoryExecutions.map((exploExec, i) => exploExec.build(this.testCaseName, i)),
      ].flat();
    }
  }
}

export class ExploratoryExecutionBuilder extends AuditBuilder {
  private comment: string;
  private status: ExecutionStatusKeys;

  constructor() {
    super();
    this.status = 'READY';
  }

  withComment(comment: string) {
    this.comment = comment;
    return this;
  }

  build(testCaseName: string, index: number): string[] {
    const commentStr = formatString(this.comment);
    const status = formatString(this.status);

    return [
      insertAttachmentList(),
      insertExecution(
        testCaseName,
        this.getCreatedBy(),
        this.getCreatedOn(),
        this.getLastModifiedByStr(),
        this.getLastModifiedOnStr(),
        commentStr,
        status,
      ),
      insertExploratoryExecution(),
      updateExecutionForExploratoryExecution(testCaseName, index),
      insertCustomFieldValueFromEntity(BindableEntity.EXECUTION, 'EXECUTION_ID', 'EXECUTION'),
    ].flat();
  }
}

export class ExecutionStepBuilder extends AuditBuilder {
  readonly status: ExecutionStatusKeys;
  private execution: ExecutionStepsExecutionBuilder[] = [];

  constructor(status: ExecutionStatusKeys = 'READY') {
    super();
    this.status = status;
  }

  withExecutionStepExecution(execution: ExecutionStepsExecutionBuilder[]) {
    this.execution = execution;
    return this;
  }

  build(): string[] {
    const status = formatString(this.status);
    return [
      insertExecutionStep(this.getCreatedBy(), this.getCreatedOn(), status),
      insertExecutionExecutionStep(),
      insertCustomFieldValueFromEntity(
        BindableEntity.EXECUTION_STEP,
        'EXECUTION_STEP_ID',
        'EXECUTION_STEP',
      ),
      ...this.execution.map((execution) => execution.build(this.getCreatedOn())),
    ].flat();
  }
}

export class ExecutionStepsExecutionBuilder {
  readonly executedBy: string;
  protected lastExecutionOn = newDateAsString();

  constructor(executedBy: string) {
    this.executedBy = executedBy;
  }

  withLastExecutedOn(lastExecutionOn: Date | string) {
    this.lastExecutionOn = formatDate(lastExecutionOn);
    return this;
  }

  build(executedOn: string): string[] {
    const lastExecutedOn = this.lastExecutionOn ? this.lastExecutionOn : executedOn;
    return [
      insertLastExecutedOnAndByInIterationTestPlanItem(this.executedBy, lastExecutedOn),
      insertLastExecutedByAndOnInExecutionStep(this.executedBy, lastExecutedOn),
    ];
  }
}

export class DeclareIssueBuilder {
  remoteIssue: string;
  issueListId: number;

  constructor(remoteIssue: string, issueListId: number) {
    this.remoteIssue = remoteIssue;
    this.issueListId = issueListId;
  }

  build() {
    return [insertIssue(this.remoteIssue, this.issueListId)].flat();
  }
}
