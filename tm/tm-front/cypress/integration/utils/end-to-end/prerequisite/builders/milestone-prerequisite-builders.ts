import { AuditBuilder } from './common-builders';
import { DEFAULT_USER_LOGIN, formatDate } from '../prerequisite-helper';
import { MilestoneStatusKeys } from '../../../../../../projects/sqtm-core/src/lib/model/level-enums/level-enum';
import {
  insertMilestone,
  insertMilestoneBinding,
  insertMilestoneBindingPerimeter,
} from '../prerequisite-queries';

export class MilestoneBuilder extends AuditBuilder {
  private readonly label: string;
  private readonly endDate: string;
  private userLogin = DEFAULT_USER_LOGIN;
  private status: MilestoneStatusKeys = 'IN_PROGRESS';
  private description = '';

  constructor(label: string, endDate: string | Date) {
    super();
    this.label = label;
    this.endDate = formatDate(endDate);
  }

  withUserLogin(userLogin: string) {
    this.userLogin = userLogin;
    return this;
  }

  withStatus(status: MilestoneStatusKeys) {
    this.status = status;
    return this;
  }

  withDescription(description: string) {
    this.description = description;
    return this;
  }

  build(): string[] {
    return [
      insertMilestone(
        this.label,
        this.status,
        this.endDate,
        this.description,
        this.getCreatedBy(),
        this.getCreatedOn(),
        this.getLastModifiedByStr(),
        this.getLastModifiedOnStr(),
        this.userLogin,
      ),
    ].flat();
  }
}

export class MilestoneOnProjectBuilder {
  private readonly label: string;

  constructor(label: string) {
    this.label = label;
  }

  build(): string[] {
    return [insertMilestoneBinding(this.label), insertMilestoneBindingPerimeter(this.label)].flat();
  }
}
