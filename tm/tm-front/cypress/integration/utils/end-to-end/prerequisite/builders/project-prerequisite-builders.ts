import { AuditBuilder } from './common-builders';
import {
  insertAclObjectIdentity,
  insertActionWordLibrary,
  insertActionWordLibraryNodeForLibrary,
  insertAttachmentList,
  insertAutomationRequestLibrary,
  insertCampaignLibrary,
  insertCustomReportLibrary,
  insertCustomReportLibraryNodeForLibrary,
  insertProject,
  insertRequirementLibrary,
  insertTestCaseLibrary,
} from '../prerequisite-queries';
import { TestCaseLibraryNodeBuilder } from './test-case-prerequisite-builders';
import { CampaignLibraryNodeBuilder } from './campaign-prerequisite-builders';
import { RequirementLibraryNodeBuilder } from './requirement-prerequisite-builders';
import { CustomFieldOnProjectBuilder } from './custom-field-prerequisite-builders';
import { LinkBuilder } from './link-builders';
import { MilestoneOnProjectBuilder } from './milestone-prerequisite-builders';
import { TeamAndProfileOnProjectBuilder } from './user-prerequisite-builders';
import { AclClass } from '../prerequisite-helper';
import {
  AiServerProjectBindingBuilder,
  BugtrackerProjectBindingBuilder,
  TestAutomationProjectBindingBuilder,
} from './server-prerequisite-builders';
import { CustomReportNodeLibraryBuilders } from './custom-report-prerequisite-builders';
import { ActionWordLibraryNodeBuilder } from './action-prerequiste-builders';
import { AutomationWorkflowBuilder } from './automationWorkflow-builders';
import {
  RequirementCategoriesListBindingBuilder,
  TestCaseNatureListBindingBuilder,
  TestCaseTypeListBindingBuilder,
} from './info-list-builder';

export class ProjectBuilder extends AuditBuilder {
  private readonly name: string;
  private description = '';
  private label = '';
  private entityType: string;
  private automationWorkflow: AutomationWorkflowBuilder[] = [];
  private teamsAndProfiles: TeamAndProfileOnProjectBuilder[] = [];
  private campaignLibraryNodes: CampaignLibraryNodeBuilder[] = [];
  private requirementLibraryNodes: RequirementLibraryNodeBuilder[] = [];
  private testCaseLibraryNodes: TestCaseLibraryNodeBuilder[] = [];
  private cufs: CustomFieldOnProjectBuilder[] = [];
  private milestones: MilestoneOnProjectBuilder[] = [];
  private links: LinkBuilder[] = [];
  private bugtrackers: BugtrackerProjectBindingBuilder[] = [];
  private customReportLibraryNodes: CustomReportNodeLibraryBuilders[] = [];
  private automationServers: TestAutomationProjectBindingBuilder[] = [];
  private aiServers: AiServerProjectBindingBuilder[] = [];
  private actionWordLibraryNodes: ActionWordLibraryNodeBuilder[] = [];
  private listReqCategories: RequirementCategoriesListBindingBuilder[] = [];
  private listNatureTC: TestCaseNatureListBindingBuilder[] = [];
  private listTypeTC: TestCaseTypeListBindingBuilder[] = [];

  constructor(name: string) {
    super();
    this.name = name;
  }

  withLabel(label: string) {
    this.label = label;
    return this;
  }

  withDescription(description: string) {
    this.description = description;
    return this;
  }

  withTeamsAndProfiles(teamsAndProfiles: TeamAndProfileOnProjectBuilder[]) {
    this.teamsAndProfiles = teamsAndProfiles;
    return this;
  }

  withCampaignLibraryNodes(campaignLibraryNodes: CampaignLibraryNodeBuilder[]) {
    this.campaignLibraryNodes = campaignLibraryNodes;
    return this;
  }

  withLinks(links: LinkBuilder[]) {
    this.links = links;
    return this;
  }

  withRequirementLibraryNodes(rootRequirementLibraryNodes: RequirementLibraryNodeBuilder[]) {
    this.requirementLibraryNodes = rootRequirementLibraryNodes;
    return this;
  }

  withTestCaseLibraryNodes(rootTestCaseLibraryNodes: TestCaseLibraryNodeBuilder[]) {
    this.testCaseLibraryNodes = rootTestCaseLibraryNodes;
    return this;
  }

  withCufsOnProject(cufs: CustomFieldOnProjectBuilder[]) {
    this.cufs = cufs;
    return this;
  }

  withMilestonesOnProject(milestones: MilestoneOnProjectBuilder[]) {
    this.milestones = milestones;
    return this;
  }

  withBugtrackerOnProject(bugtrackers: BugtrackerProjectBindingBuilder[]) {
    this.bugtrackers = bugtrackers;
    return this;
  }

  withCustomReportLibraryNode(customReportLibraryNodes: CustomReportNodeLibraryBuilders[]) {
    this.customReportLibraryNodes = customReportLibraryNodes;
    return this;
  }

  withAutomationServerOnProject(automationServers: TestAutomationProjectBindingBuilder[]) {
    this.automationServers = automationServers;
    return this;
  }

  withWorkflowAutomationOnProject(automationWorkflow: AutomationWorkflowBuilder[]) {
    this.automationWorkflow = automationWorkflow;
    return this;
  }

  withAiServerOnProject(aiServers: AiServerProjectBindingBuilder[]) {
    this.aiServers = aiServers;
    return this;
  }

  withActionWordLibraryNodes(actionWordLibraryNodesBuilders: ActionWordLibraryNodeBuilder[]) {
    this.actionWordLibraryNodes = actionWordLibraryNodesBuilders;
    return this;
  }

  withRequirementListsOnProject(listReqCategories: RequirementCategoriesListBindingBuilder[]) {
    this.listReqCategories = listReqCategories;
    return this;
  }

  withNatureTestCaseListsOnProject(listNatureTC: TestCaseNatureListBindingBuilder[]) {
    this.listNatureTC = listNatureTC;
    return this;
  }

  withTypeTestCaseListsOnProject(listTypeTC: TestCaseTypeListBindingBuilder[]) {
    this.listTypeTC = listTypeTC;
    return this;
  }

  private insertRequirementLibrary(): string[] {
    return [insertAttachmentList(), insertRequirementLibrary()];
  }

  private insertTestCaseLibrary(): string[] {
    return [insertAttachmentList(), insertTestCaseLibrary()];
  }

  private insertCampaignLibrary(): string[] {
    return [insertAttachmentList(), insertCampaignLibrary()];
  }

  private insertCustomReportLibrary(): string[] {
    return [
      insertAttachmentList(),
      insertCustomReportLibrary(),
      insertCustomReportLibraryNodeForLibrary(this.name),
    ];
  }

  private insertAutomationRequestLibrary(): string[] {
    return [insertAttachmentList(), insertAutomationRequestLibrary()];
  }

  build(): string[] {
    return [
      this.createLibraries(),
      this.createProject(
        this.name,
        this.description,
        this.label,
        this.getCreatedBy(),
        this.getCreatedOn(),
        this.getLastModifiedByStr(),
        this.getLastModifiedOnStr(),
      ),
      this.createAclObjectIdentities(),
      ...this.teamsAndProfiles.map((teamAndProfile) => teamAndProfile.build(this.name)),
      ...this.cufs.map((c) => c.build()),
      ...this.milestones.map((m) => m.build()),
      ...this.requirementLibraryNodes.map((requirementLibraryNode, i) =>
        requirementLibraryNode.buildRootNode(i),
      ),
      ...this.testCaseLibraryNodes.map((testCaseLibraryNode, i) =>
        testCaseLibraryNode.buildRootNode(i),
      ),
      ...this.campaignLibraryNodes.map((campaignLibraryNode, i) =>
        campaignLibraryNode.buildRootNode(i),
      ),
      ...this.customReportLibraryNodes.map((customReportLibraryNode, i) =>
        customReportLibraryNode.buildRootNode(this.name, i, this.entityType),
      ),
      ...this.links.map((link) => link.build()),
      ...this.bugtrackers.map((bugtracker) => bugtracker.build(this.name)),
      ...this.automationServers.map((automationServer) => automationServer.build()),
      ...this.automationWorkflow.map((automationWorkflow) => automationWorkflow.build()),
      ...this.aiServers.map((aiServer) => aiServer.build()),
      ...this.actionWordLibraryNodes.map((actionWord) => actionWord.buildRootNode()),
      ...this.listReqCategories.map((list) => list.build()),
      ...this.listNatureTC.map((list) => list.build()),
      ...this.listTypeTC.map((list) => list.build()),
    ].flat();
  }

  private createLibraries(): string[] {
    return [
      this.insertRequirementLibrary(),
      this.insertTestCaseLibrary(),
      this.insertCampaignLibrary(),
      this.insertCustomReportLibrary(),
      this.insertAutomationRequestLibrary(),
      this.insertActionWordLibrary(),
    ].flat();
  }

  private createProject(
    name: string,
    description: string,
    label: string,
    createdBy: string,
    createdOn: string,
    lastModifiedByStr: string,
    lastModifiedOnStr: string,
  ): string[] {
    return [
      insertAttachmentList(),
      insertProject(
        name,
        description,
        label,
        createdBy,
        createdOn,
        lastModifiedByStr,
        lastModifiedOnStr,
      ),
    ];
  }

  private createAclObjectIdentities() {
    return Object.entries(AclClass)
      .filter((entry) => entry[1] !== 'org.squashtest.tm.domain.project.ProjectTemplate')
      .map(([key]) => {
        return insertAclObjectIdentity(key);
      });
  }

  private insertActionWordLibrary(): string[] {
    return [
      insertAttachmentList(),
      insertActionWordLibrary(),
      insertActionWordLibraryNodeForLibrary(this.name),
    ];
  }
}
