import {
  insertActionTestStep,
  insertActionWord,
  insertActionWordFragment,
  insertActionWordLibraryNode,
  insertActionWordParameter,
  insertActionWordText,
  insertAttachmentList,
  insertAutomationRequest,
  insertAutomationRequestLibraryContent,
  insertAwlnRelationship,
  insertCallTestCaseSteps,
  insertCallTestStep,
  insertCustomFieldValueFromEntity,
  insertDataSet,
  insertEmptyDataSetParamValues,
  insertExploratoryTestCase,
  insertKeywordTestCase,
  insertKeywordTestStep,
  insertMilestoneTestCase,
  insertParameter,
  insertScriptedTestCase,
  insertTclnRelationship,
  insertTestCase,
  insertTestCaseFolder,
  insertTestCaseLibraryContent,
  insertTestCaseLibraryNode,
  insertTestCaseSteps,
  insertTestStep,
  updateAutomationRequestIdInTest,
  updateCustomFieldValue,
  updateDataSetParamValue,
} from '../prerequisite-queries';
import {
  TestCaseAutomatableKeys,
  TestCaseExecutionModeKeys,
  TestCaseImportanceKeys,
  TestCaseStatusKeys,
} from '../../../../../../projects/sqtm-core/src/lib/model/level-enums/level-enum';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { TestCaseCoverageBuilder } from './link-builders';
import { NodeBuilder } from './common-builders';
import { DEFAULT_SCRIPTED_TC_SCRIPT } from '../prerequisite-helper';
import { CustomFieldValue } from './custom-field-prerequisite-builders';

export class TestCaseLibraryNodeBuilder extends NodeBuilder {
  constructor(name: string) {
    super(name);
  }

  buildRootNode(index: number): string[] {
    return [this.testCaseLibraryNodeBaseQueries(), insertTestCaseLibraryContent(index)].flat();
  }

  build(_index: number, _parentIndex: number, _parentName: string): string[] {
    return this.testCaseLibraryNodeBaseQueries();
  }

  private testCaseLibraryNodeBaseQueries(): string[] {
    return [
      insertAttachmentList(),
      insertTestCaseLibraryNode(
        this.getName(),
        this.getDescription(),
        this.getCreatedBy(),
        this.getCreatedOn(),
        this.getLastModifiedByStr(),
        this.getLastModifiedOnStr(),
      ),
    ].flat();
  }
}

export class TestCaseFolderBuilder extends TestCaseLibraryNodeBuilder {
  private testCaseLibraryNodes: TestCaseLibraryNodeBuilder[] = [];

  withTestCaseLibraryNodes(testCaseLibraryNodes: TestCaseLibraryNodeBuilder[]) {
    this.testCaseLibraryNodes = testCaseLibraryNodes;
    return this;
  }

  override build(index: number, parentIndex: number, parentName: string): string[] {
    return [
      super.build(index, parentIndex, parentName),
      this.testCaseFolderBaseQueries(),
      insertTclnRelationship(parentName, index),
      this.testCaseFolderLastQueries(index),
    ].flat();
  }

  override buildRootNode(index: number): string[] {
    return [
      super.buildRootNode(index),
      this.testCaseFolderBaseQueries(),
      this.testCaseFolderLastQueries(index),
    ].flat();
  }

  private testCaseFolderBaseQueries(): string[] {
    return [
      insertTestCaseFolder(),
      insertCustomFieldValueFromEntity(
        BindableEntity.TESTCASE_FOLDER,
        'TCLN_ID',
        'TEST_CASE_FOLDER',
      ),
    ].flat();
  }

  private testCaseFolderLastQueries(index: number): string[] {
    return [
      ...this.testCaseLibraryNodes.map((testCaseLibraryNode, i) =>
        testCaseLibraryNode.build(i, index, this.getName()),
      ),
    ].flat();
  }
}

abstract class AbstractTestCaseBuilder extends TestCaseLibraryNodeBuilder {
  private status: TestCaseStatusKeys = 'WORK_IN_PROGRESS';
  private importance: TestCaseImportanceKeys = 'LOW';
  private executionMode: TestCaseExecutionModeKeys = 'MANUAL';
  private prerequisite = '';
  private importanceAuto = false;
  private requirements: TestCaseCoverageBuilder[] = [];
  private automatable: TestCaseAutomatableKeys = 'M';
  private automatedTransmissionStatus: TestCaseAutomatedTransmissionBuilder[] = [];
  private milestones: string[] = [];
  private cufValues: CustomFieldValue[] = [];

  withStatus(status: TestCaseStatusKeys) {
    this.status = status;
    return this;
  }

  withImportance(importance: TestCaseImportanceKeys) {
    this.importance = importance;
    return this;
  }

  withImportanceAuto() {
    this.importanceAuto = true;
    return this;
  }

  withRequirements(requirements: TestCaseCoverageBuilder[]) {
    this.requirements = requirements;
    return this;
  }

  withAutomatableStatus(automatable: TestCaseAutomatableKeys) {
    this.automatable = automatable;
    return this;
  }

  withAutomationRequest(automatedTransmissionStatus: TestCaseAutomatedTransmissionBuilder[]) {
    this.automatedTransmissionStatus = automatedTransmissionStatus;
    return this;
  }

  setExecutionMode(executionMode: TestCaseExecutionModeKeys) {
    this.executionMode = executionMode;
  }

  setPrerequisite(prerequisite: string) {
    this.prerequisite = prerequisite;
  }

  withMilestones(milestones: string[]) {
    this.milestones = milestones;
    return this;
  }

  withCufValues(cufValues: CustomFieldValue[]) {
    this.cufValues = cufValues;
    return this;
  }

  override build(index: number, parentIndex: number, parentName: string): string[] {
    return [
      super.build(index, parentIndex, parentName),
      this.testCaseBaseQueries(),
      this.testCaseNonRootQueries(index, parentName),
      this.testCaseLastQueries(index),
    ].flat();
  }

  override buildRootNode(index: number): string[] {
    return [
      super.buildRootNode(index),
      this.testCaseBaseQueries(),
      this.testCaseLastQueries(index),
    ].flat();
  }

  private testCaseBaseQueries(): string[] {
    return [
      insertTestCase(
        this.status,
        this.importance,
        this.executionMode,
        this.prerequisite,
        this.importanceAuto,
        this.getReference(),
        this.automatable,
      ),
      ...this.requirements.map((requirement) => requirement.build(this.getName())),
      ...this.automatedTransmissionStatus.map((automatedTransmissionStatus) =>
        automatedTransmissionStatus.build(this.getName(), this.getCreatedOn()),
      ),
      ...this.milestones.map((milestone) => insertMilestoneTestCase(milestone)),
    ].flat();
  }

  private testCaseNonRootQueries(index: number, parentName: string): string[] {
    return [insertTclnRelationship(parentName, index)].flat();
  }

  protected testCaseLastQueries(_index: number): string[] {
    return [
      insertCustomFieldValueFromEntity(
        BindableEntity.TEST_CASE,
        'TCLN_ID',
        'TEST_CASE_LIBRARY_NODE',
      ),
      ...this.cufValues.map((cufValue) =>
        updateCustomFieldValue(
          BindableEntity.TEST_CASE,
          'TCLN_ID',
          'TEST_CASE_LIBRARY_NODE',
          cufValue.getCufName(),
          cufValue.getValue(),
        ),
      ),
    ].flat();
  }
}

abstract class AbstractTestCaseWithDataSetAndParameterBuilder extends AbstractTestCaseBuilder {
  private parameters: ParameterBuilder[] = [];
  private dataSets: DataSetBuilder[] = [];

  withParameters(parameters: ParameterBuilder[]) {
    this.parameters = parameters;
    return this;
  }

  withDataSets(dataSets: DataSetBuilder[]) {
    this.dataSets = dataSets;
    return this;
  }

  buildParametersAndDataSets(): string[] {
    return [
      ...this.parameters.map((parameter, i) => parameter.build(i)),
      ...this.dataSets.map((dataSet) => dataSet.build()),
    ].flat();
  }

  override testCaseLastQueries(index: number): string[] {
    return [super.testCaseLastQueries(index), this.buildParametersAndDataSets()].flat();
  }
}

export class TestCaseBuilder extends AbstractTestCaseWithDataSetAndParameterBuilder {
  private steps: ActionTestStepBuilder[] = [];

  withSteps(steps: ActionTestStepBuilder[]) {
    this.steps = steps;
    return this;
  }

  withPrerequisite(prerequisite: string): void {
    this.setPrerequisite(prerequisite);
  }

  override build(index: number, parentIndex: number, parentName: string): string[] {
    return [super.build(index, parentIndex, parentName)].flat();
  }

  override buildRootNode(index: number): string[] {
    return [super.buildRootNode(index)].flat();
  }

  override testCaseLastQueries(index: number): string[] {
    return [super.testCaseLastQueries(index), ...this.steps.map((step) => step.build())].flat();
  }
}

export class ParameterBuilder {
  private readonly name: string;
  private description = '';

  constructor(name: string) {
    this.name = name;
  }

  withDescription(description: string) {
    this.description = description;
    return this;
  }

  build(index: number): string[] {
    return [insertParameter(this.name, index, this.description)].flat();
  }
}

export class DataSetBuilder {
  private readonly name: string;
  private paramValues: DataSetParameterValueBuilder[] = [];
  private actionWordParam: ActionWordParameterValueBuilder[] = [];

  constructor(name: string) {
    this.name = name;
  }

  withParamValues(paramValues: DataSetParameterValueBuilder[]) {
    this.paramValues = paramValues;
    return this;
  }

  withActionWordParam(actionWordParam: ActionWordParameterValueBuilder[]) {
    this.actionWordParam = actionWordParam;
    return this;
  }

  build(): string[] {
    return [
      insertDataSet(this.name),
      insertEmptyDataSetParamValues(),
      ...this.paramValues.map((paramValue) => paramValue.build()),
      ...this.actionWordParam.map((actionWordParam) => actionWordParam.build()),
    ].flat();
  }
}

export class DataSetParameterValueBuilder {
  private readonly paramName: string;
  private readonly paramValue: string;

  constructor(paramName: string, paramValue: string) {
    this.paramName = paramName;
    this.paramValue = paramValue;
  }

  build(): string[] {
    return [updateDataSetParamValue(this.paramName, this.paramValue)].flat();
  }
}

export class ActionWordParameterValueBuilder {
  private readonly paramName: string;
  private readonly paramValue: string;

  constructor(paramName: string, paramValue: string) {
    this.paramName = paramName;
    this.paramValue = paramValue;
  }

  build(): string[] {
    return [insertActionWordParameter(this.paramName, this.paramValue)].flat();
  }
}

export class ScriptedTestCaseBuilder extends AbstractTestCaseBuilder {
  private script: string = DEFAULT_SCRIPTED_TC_SCRIPT;

  withScript(script: string) {
    this.script = script;
    return this;
  }

  override build(index: number, parentIndex: number, parentName: string): string[] {
    return [super.build(index, parentIndex, parentName)].flat();
  }

  override buildRootNode(index: number): string[] {
    return [super.buildRootNode(index)].flat();
  }

  override testCaseLastQueries(_index: number): string[] {
    return [
      insertScriptedTestCase(this.script),
      insertCustomFieldValueFromEntity(BindableEntity.TEST_CASE, 'TCLN_ID', 'SCRIPTED_TEST_CASE'),
    ].flat();
  }
}

export class KeywordTestCaseBuilder extends AbstractTestCaseWithDataSetAndParameterBuilder {
  private keywordSteps: KeywordStepBuilder[] = [];

  withKeywordSteps(keywordSteps: KeywordStepBuilder[]) {
    this.keywordSteps = keywordSteps;
    return this;
  }

  override build(index: number, parentIndex: number, parentName: string): string[] {
    return [super.build(index, parentIndex, parentName)].flat();
  }

  override buildRootNode(index: number): string[] {
    return [super.buildRootNode(index)].flat();
  }

  override testCaseLastQueries(_index: number): string[] {
    return [
      insertKeywordTestCase(),
      insertCustomFieldValueFromEntity(BindableEntity.TEST_CASE, 'TCLN_ID', 'KEYWORD_TEST_CASE'),
      ...this.keywordSteps.map((step) => step.build()),
      this.buildParametersAndDataSets(),
    ].flat();
  }
}

export class ExploratoryTestCaseBuilder extends AbstractTestCaseBuilder {
  private charter = '';
  private sessionDuration: number;

  withCharter(charter: string) {
    this.charter = charter;
    return this;
  }

  withSessionDuration(sessionDuration: number) {
    this.sessionDuration = sessionDuration;
    return this;
  }

  constructor(name: string) {
    super(name);
    this.setExecutionMode('EXPLORATORY');
  }

  override build(index: number, parentIndex: number, parentName: string): string[] {
    return [super.build(index, parentIndex, parentName)].flat();
  }

  override buildRootNode(index: number): string[] {
    return [super.buildRootNode(index)].flat();
  }

  override testCaseLastQueries(_index: number): string[] {
    const sessionDuration = this.sessionDuration ? this.sessionDuration : null;
    return [
      insertExploratoryTestCase(this.charter, sessionDuration),
      insertCustomFieldValueFromEntity(
        BindableEntity.TEST_CASE,
        'TCLN_ID',
        'EXPLORATORY_TEST_CASE',
      ),
    ].flat();
  }
}

class TestStepBuilder {
  build(): string[] {
    return [insertAttachmentList(), insertTestStep(), insertTestCaseSteps()].flat();
  }
}

export class ActionTestStepBuilder extends TestStepBuilder {
  private readonly action: string;
  private readonly expectedResult: string;
  private steps: CallTestStepsBuilder[] = [];

  constructor(action: string, expectedResult: string) {
    super();
    this.action = action;
    this.expectedResult = expectedResult;
  }
  withCallTestSteps(steps: CallTestStepsBuilder[]) {
    this.steps = steps;
    return this;
  }

  override build(): string[] {
    return [
      super.build(),
      insertActionTestStep(this.action, this.expectedResult),
      insertCustomFieldValueFromEntity(BindableEntity.TEST_STEP, 'TEST_STEP_ID', 'TEST_STEP'),
      ...this.steps.map((step) => step.build()),
    ].flat();
  }
}

export class KeywordStepBuilder extends TestStepBuilder {
  private readonly keyword: string;
  // For now, let's just assume that we will have a simple text action word, which will take some work to get to a more complicated one
  private readonly simpleActionWord: string;
  private datatable = '';
  private docstring = '';
  private comment = '';

  constructor(keyword: string, simpleActionWord: string) {
    super();
    this.keyword = keyword;
    this.simpleActionWord = simpleActionWord;
  }

  withDatatable(datatable: string) {
    this.datatable = datatable;
    return this;
  }

  withDocstring(docstring: string) {
    this.docstring = docstring;
    return this;
  }

  withComment(comment: string) {
    this.comment = comment;
    return this;
  }

  override build(): string[] {
    return [
      super.build(),
      insertActionWord('T-' + this.simpleActionWord + '-'),
      insertKeywordTestStep(this.keyword, this.datatable, this.docstring, this.comment),
      insertActionWordFragment(0),
      insertActionWordLibraryNode(this.simpleActionWord),
      insertActionWordText(this.simpleActionWord),
      insertAwlnRelationship(),
      insertCustomFieldValueFromEntity(BindableEntity.TEST_STEP, 'TEST_STEP_ID', 'TEST_STEP'),
    ].flat();
  }
}

export class TestCaseAutomatedTransmissionBuilder {
  protected status: string;
  constructor(status: string) {
    this.status = status;
  }
  build(name: string, createdOn: string): string[] {
    return [
      insertAutomationRequest(this.status, createdOn),
      insertAutomationRequestLibraryContent(name),
      updateAutomationRequestIdInTest(),
    ].flat();
  }
}
export class CallTestStepsBuilder {
  protected steps: string;
  constructor(steps: string) {
    this.steps = steps;
  }
  build() {
    return [insertTestStep(), insertCallTestStep(), insertCallTestCaseSteps(this.steps)];
  }
}
