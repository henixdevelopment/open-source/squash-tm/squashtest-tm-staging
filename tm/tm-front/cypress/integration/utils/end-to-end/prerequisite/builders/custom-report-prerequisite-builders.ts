import { NodeBuilder } from './common-builders';
import {
  insertChartAxisColumn,
  insertChartDefinition,
  insertChartFilter,
  insertChartFilterValues,
  insertChartMeasureColumn,
  insertChartProjectScope,
  insertChartScope,
  insertCrlnRelationship,
  insertCustomExportColumn,
  insertCustomExportScope,
  insertCustomFieldValueFromEntity,
  insertCustomReportChartBinding,
  insertCustomReportCustomExport,
  insertCustomReportDashboard,
  insertCustomReportLibraryNode,
  insertCustomReportReportBinding,
  insertFolderInCustomFieldReportLibrary,
  insertReportDefinition,
} from '../prerequisite-queries';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import {
  ChartOperation,
  ChartScopeType,
  ChartType,
} from '../../../../../../projects/sqtm-core/src/lib/model/custom-report/chart-definition.model';
import { PluginNameSpace } from '../../../../../../projects/sqtm-core/src/lib/model/custom-report/report-definition.model';
import { EntityType } from '../../../../../../projects/sqtm-core/src/lib/model/entity.model';
import { CustomExportEntityType } from '../../../../../../projects/sqtm-core/src/lib/model/custom-report/custom-export-columns.model';

export enum CustomColumnEntityType {
  CAMPAIGN_LABEL = 'CAMPAIGN_LABEL',
  CAMPAIGN_ID = 'CAMPAIGN_ID',
  CAMPAIGN_REFERENCE = 'CAMPAIGN_REFERENCE',
  CAMPAIGN_DESCRIPTION = 'CAMPAIGN_DESCRIPTION',
  CAMPAIGN_STATE = 'CAMPAIGN_STATE',
  CAMPAIGN_PROGRESS_STATUS = 'CAMPAIGN_PROGRESS_STATUS',
  CAMPAIGN_MILESTONE = 'CAMPAIGN_MILESTONE',
  CAMPAIGN_SCHEDULED_START = 'CAMPAIGN_SCHEDULED_START',
  CAMPAIGN_SCHEDULED_END = 'CAMPAIGN_SCHEDULED_END',
  CAMPAIGN_ACTUAL_START = 'CAMPAIGN_ACTUAL_START',
  CAMPAIGN_ACTUAL_END = 'CAMPAIGN_ACTUAL_END',
  ITERATION_LABEL = 'ITERATION_LABEL',
  ITERATION_ID = 'ITERATION_ID',
  ITERATION_REFERENCE = 'ITERATION_REFERENCE',
  ITERATION_DESCRIPTION = 'ITERATION_DESCRIPTION',
  ITERATION_STATE = 'ITERATION_STATE',
  ITERATION_SCHEDULED_START = 'ITERATION_SCHEDULED_START',
  ITERATION_SCHEDULED_END = 'ITERATION_SCHEDULED_END',
  ITERATION_ACTUAL_START = 'ITERATION_ACTUAL_START',
  ITERATION_ACTUAL_END = 'ITERATION_ACTUAL_END',
  TEST_SUITE_LABEL = 'TEST_SUITE_LABEL',
  TEST_SUITE_ID = 'TEST_SUITE_ID',
  TEST_SUITE_DESCRIPTION = 'TEST_SUITE_DESCRIPTION',
  TEST_SUITE_EXECUTION_STATUS = 'TEST_SUITE_EXECUTION_STATUS',
  TEST_SUITE_PROGRESS_STATUS = 'TEST_SUITE_PROGRESS_STATUS',
  TEST_CASE_PROJECT = 'TEST_CASE_PROJECT',
  TEST_CASE_MILESTONE = 'TEST_CASE_MILESTONE',
  TEST_CASE_LABEL = 'TEST_CASE_LABEL',
  TEST_CASE_ID = 'TEST_CASE_ID',
  TEST_CASE_REFERENCE = 'TEST_CASE_REFERENCE',
  TEST_CASE_DESCRIPTION = 'TEST_CASE_DESCRIPTION',
  TEST_CASE_STATUS = 'TEST_CASE_STATUS',
  TEST_CASE_IMPORTANCE = 'TEST_CASE_IMPORTANCE',
  TEST_CASE_NATURE = 'TEST_CASE_NATURE',
  TEST_CASE_TYPE = 'TEST_CASE_TYPE',
  TEST_CASE_DATASET = 'TEST_CASE_DATASET',
  TEST_CASE_PREREQUISITE = 'TEST_CASE_PREREQUISITE',
  TEST_CASE_LINKED_REQUIREMENTS_NUMBER = 'TEST_CASE_LINKED_REQUIREMENTS_NUMBER',
  TEST_CASE_LINKED_REQUIREMENTS_IDS = 'TEST_CASE_LINKED_REQUIREMENTS_IDS',
  EXECUTION_ID = 'EXECUTION_ID',
  EXECUTION_EXECUTION_MODE = 'EXECUTION_EXECUTION_MODE',
  EXECUTION_STATUS = 'EXECUTION_STATUS',
  EXECUTION_SUCCESS_RATE = 'EXECUTION_SUCCESS_RATE',
  EXECUTION_USER = 'EXECUTION_USER',
  EXECUTION_EXECUTION_DATE = 'EXECUTION_EXECUTION_DATE',
  EXECUTION_COMMENT = 'EXECUTION_COMMENT',
  EXECUTION_STEP_STEP_NUMBER = 'EXECUTION_STEP_STEP_NUMBER',
  EXECUTION_STEP_ACTION = 'EXECUTION_STEP_ACTION',
  EXECUTION_STEP_RESULT = 'EXECUTION_STEP_RESULT',
  EXECUTION_STEP_STATUS = 'EXECUTION_STEP_STATUS',
  EXECUTION_STEP_USER = 'EXECUTION_STEP_USER',
  EXECUTION_STEP_EXECUTION_DATE = 'EXECUTION_STEP_EXECUTION_DATE',
  EXECUTION_STEP_COMMENT = 'EXECUTION_STEP_COMMENT',
  EXECUTION_STEP_LINKED_REQUIREMENTS_NUMBER = 'EXECUTION_STEP_LINKED_REQUIREMENTS_NUMBER',
  EXECUTION_STEP_LINKED_REQUIREMENTS_IDS = 'EXECUTION_STEP_LINKED_REQUIREMENTS_IDS',
  ISSUE_EXECUTION_AND_EXECUTION_STEP_ISSUES_NUMBER = 'ISSUE_EXECUTION_AND_EXECUTION_STEP_ISSUES_NUMBER',
  ISSUE_EXECUTION_AND_EXECUTION_STEP_ISSUES_IDS = 'ISSUE_EXECUTION_AND_EXECUTION_STEP_ISSUES_IDS',
  ISSUE_EXECUTION_STEP_ISSUES_NUMBER = 'ISSUE_EXECUTION_STEP_ISSUES_NUMBER',
  ISSUE_EXECUTION_STEP_ISSUES_IDS = 'ISSUE_EXECUTION_STEP_ISSUES_IDS',
}

export class CustomReportNodeLibraryBuilders extends NodeBuilder {
  constructor(name: string) {
    super(name);
  }

  build(_parentName: string, _index: number, _parentIndex: number, entityType: string): string[] {
    return this.chartLibraryBaseQueries(entityType);
  }

  buildRootNode(nameParent: string, index: number, entityType: string): string[] {
    return [
      this.chartLibraryBaseQueries(entityType),
      insertCrlnRelationship(index, nameParent),
    ].flat();
  }

  private chartLibraryBaseQueries(entityType: string): string[] {
    return [insertCustomReportLibraryNode(this.getName(), entityType)];
  }
}

export class FolderCustomReportLibraryBuilder extends CustomReportNodeLibraryBuilders {
  private customReportLibraryNode: CustomReportNodeLibraryBuilders[] = [];

  constructor(name: string) {
    super(name);
  }

  withCustomReportFolderLibraryNodes(customReportLibraryNode: CustomReportNodeLibraryBuilders[]) {
    this.customReportLibraryNode = customReportLibraryNode;
    return this;
  }

  override build(
    parentName: string,
    index: number,
    parentIndex: number,
    entityType: string,
  ): string[] {
    return [
      super.build(parentName, index, parentIndex, entityType),
      insertCrlnRelationship(index, parentName),
      this.CustomReportLibraryFolder(),
      this.customReportFolderLastQueries(),
    ].flat();
  }

  override buildRootNode(parentName: string, index: number): string[] {
    return [
      super.buildRootNode(parentName, index, 'FOLDER'),
      this.CustomReportLibraryFolder(),
      this.customReportFolderLastQueries(),
    ].flat();
  }

  private CustomReportLibraryFolder(): string[] {
    return [
      insertFolderInCustomFieldReportLibrary(this.getName()),
      insertCustomFieldValueFromEntity(
        BindableEntity.CUSTOM_REPORT_FOLDER,
        'CRF_ID',
        'CUSTOM_REPORT_FOLDER',
      ),
    ].flat();
  }

  private customReportFolderLastQueries(): string[] {
    return [
      ...this.customReportLibraryNode.map((customReportLibraryNode, i) =>
        customReportLibraryNode.buildRootNode(this.getName(), i, this.getEntityType()),
      ),
    ].flat();
  }
}

export class ChartBuilder extends CustomReportNodeLibraryBuilders {
  private labelPrototype: string;
  private chartType: ChartType;
  private chartScopeType: ChartScopeType;
  private chartOperation: ChartOperation;
  private entityReferenceType: EntityType;
  private axisRank: number;
  private filters: ChartFilterBuilder[] = [];

  constructor(
    name: string,
    label: string,
    chartType: ChartType,
    chartScopeType: ChartScopeType,
    chartOperation: ChartOperation,
    entityReferenceType: EntityType,
  ) {
    super(name);
    this.labelPrototype = label;
    this.chartType = chartType;
    this.chartScopeType = chartScopeType;
    this.chartOperation = chartOperation;
    this.entityReferenceType = entityReferenceType;
  }

  withAxisRank(axisRank: number) {
    this.axisRank = axisRank;
    return this;
  }

  withFilters(filters: ChartFilterBuilder[]) {
    this.filters = filters;
    return this;
  }

  override build(
    _parentName: string,
    _index: number,
    _parentIndex: number,
    entityType: string,
  ): string[] {
    return super.build(_parentName, _index, _parentIndex, entityType);
  }

  override buildRootNode(parentName: string, index: number): string[] {
    return [
      super.buildRootNode(parentName, index, 'CHART'),
      this.chartBaseQueries(this.chartType, this.chartScopeType, this.entityReferenceType),
      this.chartLastQueries(this.chartOperation, this.labelPrototype),
      ...this.filters.map((filter) => filter.build()),
    ].flat();
  }

  private chartBaseQueries(
    chartType: ChartType,
    scopeType: ChartScopeType,
    entityReferenceType: EntityType,
  ): string[] {
    return [
      insertChartDefinition(
        this.getName(),
        this.getCreatedBy(),
        this.getCreatedOn(),
        chartType,
        scopeType,
      ),
      insertChartScope(entityReferenceType),
    ];
  }

  private chartLastQueries(chartOperation: ChartOperation, label: string): string[] {
    const result = [
      insertChartProjectScope(),
      insertChartMeasureColumn(label, chartOperation),
      insertChartAxisColumn(label),
    ];
    if (this.axisRank) {
      result.push(insertChartAxisColumn(label, this.axisRank));
    }
    return result;
  }
}

export class DashboardBuilder extends CustomReportNodeLibraryBuilders {
  private elements: CustomReportElementsBinding[] = [];

  constructor(name: string) {
    super(name);
  }

  withLinkElements(elements: CustomReportElementsBinding[]) {
    this.elements = elements;
    return this;
  }

  override build(
    _parentName: string,
    _index: number,
    _parentIndex: number,
    entityType: string,
  ): string[] {
    return super.build(_parentName, _index, _parentIndex, entityType);
  }

  override buildRootNode(parentName: string, index: number): string[] {
    return [
      super.buildRootNode(parentName, index, 'DASHBOARD'),
      insertCustomReportDashboard(this.getName(), this.getCreatedBy(), this.getCreatedOn()),
      ...this.elements.map((element) => element.build(this.getName())),
    ].flat();
  }
}

export class ReportBuilder extends CustomReportNodeLibraryBuilders {
  private bookType: PluginNameSpace;
  private typeDataParameters: string;

  constructor(
    name: string,
    bookType: PluginNameSpace,
    typeDataParameters: string, //Declared of type string because the data are handled as string in the column of the database so you need  to declare a constant using JSON.stringify on the mock data or the interface of DefinitionParameter you want to use in data injection
  ) {
    super(name);
    this.bookType = bookType;
    this.typeDataParameters = typeDataParameters;
  }

  override build(
    _parentName: string,
    _index: number,
    _parentIndex: number,
    entityType: string,
  ): string[] {
    return super.build(_parentName, _index, _parentIndex, entityType);
  }

  override buildRootNode(parentName: string, index: number): string[] {
    return [
      super.buildRootNode(parentName, index, 'REPORT'),
      insertReportDefinition(
        this.getName(),
        this.getCreatedBy(),
        this.getCreatedOn(),
        this.bookType,
        this.typeDataParameters,
      ),
    ].flat();
  }
}

export class CustomExportBuilder extends CustomReportNodeLibraryBuilders {
  private customColumnEntityType: CustomColumnEntityType;
  private entityReferenceType: CustomExportEntityType;
  private entitySelectedName: string;

  constructor(
    name: string,
    customColumnEntityType: CustomColumnEntityType,
    entityReferenceType: CustomExportEntityType,
    entitySelectedName: string,
  ) {
    super(name);
    this.customColumnEntityType = customColumnEntityType;
    this.entityReferenceType = entityReferenceType;
    this.entitySelectedName = entitySelectedName;
  }

  override build(
    _parentName: string,
    _index: number,
    _parentIndex: number,
    entityType: string,
  ): string[] {
    return super.build(_parentName, _index, _parentIndex, entityType);
  }

  override buildRootNode(parentName: string, index: number): string[] {
    return [
      super.buildRootNode(parentName, index, 'CUSTOM_EXPORT'),
      this.customExportBaseQueries(),
      this.customExportLastQueries(
        this.customColumnEntityType,
        this.entityReferenceType,
        this.entitySelectedName,
      ),
    ].flat();
  }

  private customExportBaseQueries(): string[] {
    return [
      insertCustomReportCustomExport(this.getName(), this.getCreatedBy(), this.getCreatedOn()),
    ];
  }

  private customExportLastQueries(
    customColumnEntityType: CustomColumnEntityType,
    entityReferenceType: CustomExportEntityType,
    entitySelectedName: string,
  ): string[] {
    return [
      insertCustomExportColumn(customColumnEntityType),
      insertCustomExportScope(entityReferenceType, entitySelectedName),
    ];
  }
}

export interface CustomReportElementsBinding {
  build(name: string): string[];
}

export class CustomReportChartBinding implements CustomReportElementsBinding {
  private chartName: string;
  private layout: { row: number; col: number; sizeX: number; sizeY: number };

  /*
 row and col specify the position of the element in the grid, so row must be between 1 and 3, and col must be between 1 and 4.
 sizeX and sizeY represent the size of the element in the grid, and sizeX cannot exceed the grid width (max 4) and sizeY cannot exceed the grid height (max 3).
*/
  constructor(
    chartName: string,
    layout: { row: 1 | 2 | 3; col: 1 | 2 | 3 | 4; sizeX: 1 | 2 | 3 | 4; sizeY: 1 | 2 | 3 },
  ) {
    this.chartName = chartName;
    this.layout = layout;
  }

  build(name: string) {
    return [insertCustomReportChartBinding(name, this.chartName, this.layout)].flat();
  }
}

export class CustomReportReportBinding implements CustomReportElementsBinding {
  private reportName: string;
  private layout: { row: number; col: number; sizeX: number; sizeY: number };

  /*
  row and col specify the position of the element in the grid, so row must be between 1 and 3, and col must be between 1 and 4.
  sizeX and sizeY represent the size of the element in the grid, and sizeX cannot exceed the grid width (max 4) and sizeY cannot exceed the grid height (max 3).
 */
  constructor(
    reportName: string,
    layout: { row: 1 | 2 | 3; col: 1 | 2 | 3 | 4; sizeX: 1 | 2 | 3 | 4; sizeY: 1 | 2 | 3 },
  ) {
    this.reportName = reportName;
    this.layout = layout;
  }

  build(name: string) {
    return [insertCustomReportReportBinding(name, this.reportName, this.layout)].flat();
  }
}

export class ChartFilterBuilder {
  private label: string;
  private chartOperation: ChartOperation;
  private value: string;

  constructor(chartOperation: ChartOperation, label: string, value: string) {
    this.label = label;
    this.chartOperation = chartOperation;
    this.value = value;
  }

  build() {
    return [
      insertChartFilter(this.label, this.chartOperation),
      insertChartFilterValues(this.value),
    ].flat();
  }
}
