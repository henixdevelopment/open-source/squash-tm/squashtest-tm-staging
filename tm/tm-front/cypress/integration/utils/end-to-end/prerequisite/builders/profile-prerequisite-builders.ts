import { AuditBuilder } from './common-builders';
import { insertProfile, insertProfilePermission } from '../prerequisite-queries';
import {
  Permissions,
  PermissionWorkspaceType,
} from '../../../../../../projects/sqtm-core/src/lib/model/permissions/permissions.model';

export class ProfileBuilder extends AuditBuilder {
  private qualifiedName: string;
  private active = true;
  private description = '';
  private permissions: ProfileBuilderPermission[] = [];

  constructor(qualifiedName: string, permissions: ProfileBuilderPermission[]) {
    super();
    this.qualifiedName = qualifiedName;
    permissions.push(ProfileBuilderPermission.PROJECT_READ); // needed permission to all users for now
    this.permissions = permissions;
  }

  withDescription(description: string) {
    this.description = description;
    return this;
  }

  disable() {
    this.active = false;
    return this;
  }

  build() {
    return [
      insertProfile(
        this.qualifiedName,
        this.active,
        this.description,
        this.getCreatedBy(),
        this.getCreatedOn(),
        this.getLastModifiedByStr(),
        this.getLastModifiedOnStr(),
      ),
      this.insertProfilePermissions(this.permissions),
    ].flat();
  }

  private insertProfilePermissions(permissions: ProfileBuilderPermission[]): string[] {
    const permissionQueries: string[] = [];
    permissions.forEach((permission) => {
      const workspaceClassId = getPermissionDetails(permission).workspaceClassId;
      const permissionMask = getPermissionDetails(permission).permissionMask;
      permissionQueries.push(insertProfilePermission(workspaceClassId, permissionMask));
    });
    return permissionQueries;
  }
}

export enum ProfileBuilderPermission {
  ACTION_WORD_READ = 'ACTION_WORD_READ',
  ACTION_WORD_CREATE = 'ACTION_WORD_CREATE',
  ACTION_WORD_WRITE = 'ACTION_WORD_WRITE',
  ACTION_WORD_DELETE = 'ACTION_WORD_DELETE',
  CAMPAIGN_READ = 'CAMPAIGN_READ',
  CAMPAIGN_CREATE = 'CAMPAIGN_CREATE',
  CAMPAIGN_WRITE = 'CAMPAIGN_WRITE',
  CAMPAIGN_DELETE = 'CAMPAIGN_DELETE',
  CAMPAIGN_LINK = 'CAMPAIGN_LINK',
  CAMPAIGN_READ_UNASSIGNED = 'CAMPAIGN_READ_UNASSIGNED',
  CAMPAIGN_EXTENDED_DELETE = 'CAMPAIGN_EXTENDED_DELETE',
  CAMPAIGN_EXECUTE = 'CAMPAIGN_EXECUTE',
  CUSTOM_REPORT_READ = 'CUSTOM_REPORT_READ',
  CUSTOM_REPORT_CREATE = 'CUSTOM_REPORT_CREATE',
  CUSTOM_REPORT_WRITE = 'CUSTOM_REPORT_WRITE',
  CUSTOM_REPORT_DELETE = 'CUSTOM_REPORT_DELETE',
  CUSTOM_REPORT_EXPORT = 'CUSTOM_REPORT_EXPORT',
  MANAGE_PROJECT = 'MANAGE_PROJECT',
  MANAGE_MILESTONE = 'MANAGE_MILESTONE',
  PROJECT_IMPORT = 'PROJECT_IMPORT',
  PROJECT_READ = 'PROJECT_READ',
  REQUIREMENT_READ = 'REQUIREMENT_READ',
  REQUIREMENT_CREATE = 'REQUIREMENT_CREATE',
  REQUIREMENT_WRITE = 'REQUIREMENT_WRITE',
  REQUIREMENT_DELETE = 'REQUIREMENT_DELETE',
  REQUIREMENT_LINK = 'REQUIREMENT_LINK',
  REQUIREMENT_EXPORT = 'REQUIREMENT_EXPORT',
  TEST_CASE_READ = 'TEST_CASE_READ',
  TEST_CASE_CREATE = 'TEST_CASE_CREATE',
  TEST_CASE_WRITE = 'TEST_CASE_WRITE',
  TEST_CASE_DELETE = 'TEST_CASE_DELETE',
  TEST_CASE_LINK = 'TEST_CASE_LINK',
  TEST_CASE_EXPORT = 'TEST_CASE_EXPORT',
}

const permissionWorkspaceTypeValues: Record<PermissionWorkspaceType, number> = {
  PROJECT: 1,
  REQUIREMENT_LIBRARY: 2,
  TEST_CASE_LIBRARY: 3,
  CAMPAIGN_LIBRARY: 4,
  CUSTOM_REPORT_LIBRARY: 6,
  AUTOMATION_REQUEST_LIBRARY: 7,
  ACTION_WORD_LIBRARY: 8,
};

const permissionsValues: Record<Permissions, number> = {
  READ: 1,
  WRITE: 2,
  CREATE: 4,
  DELETE: 8,
  ADMIN: 16,
  MANAGE_PROJECT: 32,
  EXPORT: 64,
  EXECUTE: 128,
  LINK: 256,
  IMPORT: 512,
  ATTACH: 1024,
  EXTENDED_DELETE: 2048,
  READ_UNASSIGNED: 4096,
  WRITE_AS_FUNCTIONAL: 8192,
  WRITE_AS_AUTOMATION: 16384,
  MANAGE_MILESTONE: 32768,
  MANAGE_PROJECT_CLEARANCE: 65536,
  DELETE_EXECUTION: 131072,
};

const profileBuilderPermissionMapping: Record<
  ProfileBuilderPermission,
  [PermissionWorkspaceType, Permissions]
> = {
  ACTION_WORD_READ: [PermissionWorkspaceType.ACTION_WORD_LIBRARY, Permissions.READ],
  ACTION_WORD_CREATE: [PermissionWorkspaceType.ACTION_WORD_LIBRARY, Permissions.CREATE],
  ACTION_WORD_WRITE: [PermissionWorkspaceType.ACTION_WORD_LIBRARY, Permissions.WRITE],
  ACTION_WORD_DELETE: [PermissionWorkspaceType.ACTION_WORD_LIBRARY, Permissions.DELETE],
  CAMPAIGN_READ: [PermissionWorkspaceType.CAMPAIGN_LIBRARY, Permissions.READ],
  CAMPAIGN_CREATE: [PermissionWorkspaceType.CAMPAIGN_LIBRARY, Permissions.CREATE],
  CAMPAIGN_WRITE: [PermissionWorkspaceType.CAMPAIGN_LIBRARY, Permissions.WRITE],
  CAMPAIGN_DELETE: [PermissionWorkspaceType.CAMPAIGN_LIBRARY, Permissions.DELETE],
  CAMPAIGN_LINK: [PermissionWorkspaceType.CAMPAIGN_LIBRARY, Permissions.LINK],
  CAMPAIGN_READ_UNASSIGNED: [PermissionWorkspaceType.CAMPAIGN_LIBRARY, Permissions.READ_UNASSIGNED],
  CAMPAIGN_EXTENDED_DELETE: [PermissionWorkspaceType.CAMPAIGN_LIBRARY, Permissions.EXTENDED_DELETE],
  CAMPAIGN_EXECUTE: [PermissionWorkspaceType.CAMPAIGN_LIBRARY, Permissions.EXECUTE],
  CUSTOM_REPORT_READ: [PermissionWorkspaceType.CUSTOM_REPORT_LIBRARY, Permissions.READ],
  CUSTOM_REPORT_CREATE: [PermissionWorkspaceType.CUSTOM_REPORT_LIBRARY, Permissions.CREATE],
  CUSTOM_REPORT_WRITE: [PermissionWorkspaceType.CUSTOM_REPORT_LIBRARY, Permissions.WRITE],
  CUSTOM_REPORT_DELETE: [PermissionWorkspaceType.CUSTOM_REPORT_LIBRARY, Permissions.DELETE],
  CUSTOM_REPORT_EXPORT: [PermissionWorkspaceType.CUSTOM_REPORT_LIBRARY, Permissions.EXPORT],
  MANAGE_PROJECT: [PermissionWorkspaceType.PROJECT, Permissions.MANAGE_PROJECT],
  MANAGE_MILESTONE: [PermissionWorkspaceType.PROJECT, Permissions.MANAGE_MILESTONE],
  PROJECT_READ: [PermissionWorkspaceType.PROJECT, Permissions.READ],
  PROJECT_IMPORT: [PermissionWorkspaceType.PROJECT, Permissions.IMPORT],
  REQUIREMENT_READ: [PermissionWorkspaceType.REQUIREMENT_LIBRARY, Permissions.READ],
  REQUIREMENT_CREATE: [PermissionWorkspaceType.REQUIREMENT_LIBRARY, Permissions.CREATE],
  REQUIREMENT_WRITE: [PermissionWorkspaceType.REQUIREMENT_LIBRARY, Permissions.WRITE],
  REQUIREMENT_DELETE: [PermissionWorkspaceType.REQUIREMENT_LIBRARY, Permissions.DELETE],
  REQUIREMENT_LINK: [PermissionWorkspaceType.REQUIREMENT_LIBRARY, Permissions.LINK],
  REQUIREMENT_EXPORT: [PermissionWorkspaceType.REQUIREMENT_LIBRARY, Permissions.EXPORT],
  TEST_CASE_READ: [PermissionWorkspaceType.TEST_CASE_LIBRARY, Permissions.READ],
  TEST_CASE_CREATE: [PermissionWorkspaceType.TEST_CASE_LIBRARY, Permissions.CREATE],
  TEST_CASE_WRITE: [PermissionWorkspaceType.TEST_CASE_LIBRARY, Permissions.WRITE],
  TEST_CASE_DELETE: [PermissionWorkspaceType.TEST_CASE_LIBRARY, Permissions.DELETE],
  TEST_CASE_LINK: [PermissionWorkspaceType.TEST_CASE_LIBRARY, Permissions.LINK],
  TEST_CASE_EXPORT: [PermissionWorkspaceType.TEST_CASE_LIBRARY, Permissions.EXPORT],
};

function getPermissionDetails(permission: ProfileBuilderPermission): {
  workspaceClassId: number;
  permissionMask: number;
} {
  const [workspaceType, permissionType] = profileBuilderPermissionMapping[permission];
  return {
    workspaceClassId: permissionWorkspaceTypeValues[workspaceType],
    permissionMask: permissionsValues[permissionType],
  };
}

export const milestoneManagerBuilderPermissions = [
  ProfileBuilderPermission.MANAGE_PROJECT,
  ProfileBuilderPermission.MANAGE_MILESTONE,
];
