import {
  insertRequirementVersionCoverageFromRequirements,
  insertRequirementVersionCoverageFromTestCases,
  insertRequirementVersionCoverageFromTestCasesSteps,
  insertRequirementVersionDirectLink,
  insertRequirementVersionIndirectLink,
  updateRequirementHighLevelRequirementIdFromNames,
  updateTestCaseImportanceIfImportanceAuto,
} from '../prerequisite-queries';

export class LinkBuilder {
  build(): string[] {
    return [].flat();
  }
}

export class RequirementsLinkToHighLevelRequirementBuilder extends LinkBuilder {
  private readonly requirementNames: string[];
  private readonly highLevelRequirementName: string;

  constructor(requirementNames: string[], highLevelRequirementName: string) {
    super();
    this.requirementNames = requirementNames;
    this.highLevelRequirementName = highLevelRequirementName;
  }

  build(): string[] {
    return [
      updateRequirementHighLevelRequirementIdFromNames(
        this.requirementNames,
        this.highLevelRequirementName,
      ),
    ].flat();
  }
}

export class TestCaseCoverageBuilder {
  private readonly verifiedReqVersionNames: string[];

  constructor(verifiedReqVersionNames: string[]) {
    this.verifiedReqVersionNames = verifiedReqVersionNames;
  }
  build(verifyingTestCaseName: string): string[] {
    return [
      insertRequirementVersionCoverageFromRequirements(
        this.verifiedReqVersionNames,
        verifyingTestCaseName,
      ),
      updateTestCaseImportanceIfImportanceAuto(verifyingTestCaseName),
    ].flat();
  }
}

export class RequirementVersionsLinkToVerifyingTestCaseBuilder extends LinkBuilder {
  private readonly verifiedReqVersionNames: string[];
  private readonly verifyingTestCaseName: string;
  constructor(verifiedReqVersionNames: string[], verifyingTestCaseName: string) {
    super();
    this.verifiedReqVersionNames = verifiedReqVersionNames;
    this.verifyingTestCaseName = verifyingTestCaseName;
  }

  build(): string[] {
    return [
      insertRequirementVersionCoverageFromRequirements(
        this.verifiedReqVersionNames,
        this.verifyingTestCaseName,
      ),
      updateTestCaseImportanceIfImportanceAuto(this.verifyingTestCaseName),
    ].flat();
  }
}

export class TestCasesLinkToVerifiedRequirementVersionBuilder extends LinkBuilder {
  private readonly verifyingTestCaseNames: string[];
  private readonly verifiedReqVersionName: string;

  constructor(verifyingTestCaseNames: string[], verifiedReqVersionName: string) {
    super();
    this.verifyingTestCaseNames = verifyingTestCaseNames;
    this.verifiedReqVersionName = verifiedReqVersionName;
  }

  build(): string[] {
    return [
      insertRequirementVersionCoverageFromTestCases(
        this.verifyingTestCaseNames,
        this.verifiedReqVersionName,
      ),
    ].flat();
  }
}

export class TestCasesStepsLinkToRequirementVersionBuilder extends LinkBuilder {
  private readonly verifiedReqVersionNames: string[];
  private readonly verifyingTestCaseName: string;

  constructor(verifyingTestCaseName: string, verifiedReqVersionNames: string[]) {
    super();
    this.verifiedReqVersionNames = verifiedReqVersionNames;
    this.verifyingTestCaseName = verifyingTestCaseName;
  }

  build(): string[] {
    return [
      insertRequirementVersionCoverageFromRequirements(
        this.verifiedReqVersionNames,
        this.verifyingTestCaseName,
      ),
      insertRequirementVersionCoverageFromTestCasesSteps(this.verifyingTestCaseName),
    ].flat();
  }
}
export class RequirementsVersionLinkBuilder extends LinkBuilder {
  private readonly requirement: string;
  private readonly requirementRelated: string;

  constructor(requirement: string, requirementRelated: string) {
    super();
    this.requirement = requirement;
    this.requirementRelated = requirementRelated;
  }

  build(): string[] {
    return [
      insertRequirementVersionDirectLink(this.requirement, this.requirementRelated),
      insertRequirementVersionIndirectLink(this.requirement, this.requirementRelated),
    ].flat();
  }
}
