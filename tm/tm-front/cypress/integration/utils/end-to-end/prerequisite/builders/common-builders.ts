import {
  DEFAULT_USER_LOGIN,
  formatDate,
  formatString,
  newDateAsString,
} from '../prerequisite-helper';

export abstract class AuditBuilder {
  private createdBy = DEFAULT_USER_LOGIN;
  private createdOn = newDateAsString();
  private lastModifiedBy: string;
  private lastModifiedOn: string;

  withCreatedBy(createdBy: string) {
    this.createdBy = createdBy;
    return this;
  }

  withCreatedOn(createdOn: Date | string) {
    this.createdOn = formatDate(createdOn);
    return this;
  }

  withLastModifiedBy(lastModifiedBy: string) {
    this.lastModifiedBy = lastModifiedBy;
    return this;
  }

  withLastModifiedOn(lastModifiedOn: Date | string) {
    if (lastModifiedOn) {
      this.lastModifiedOn = formatDate(lastModifiedOn);
    }
    return this;
  }

  protected getCreatedBy(): string {
    return this.createdBy;
  }

  protected getCreatedOn(): string {
    return this.createdOn;
  }

  protected getLastModifiedBy(): string {
    return this.lastModifiedBy;
  }

  protected getLastModifiedOn(): string {
    return this.lastModifiedOn;
  }

  protected getLastModifiedByStr(): string {
    return formatString(this.lastModifiedBy);
  }

  protected getLastModifiedOnStr(): string {
    return formatString(this.lastModifiedOn);
  }
}

export abstract class NodeBuilder extends AuditBuilder {
  private readonly name: string;
  private reference = '';
  private description = '';
  private entityType: string;

  withReference(reference: string) {
    this.reference = reference;
    return this;
  }

  withDescription(description: string) {
    this.description = description;
    return this;
  }

  protected getName(): string {
    return this.name;
  }

  protected getReference(): string {
    return this.reference;
  }

  protected getDescription(): string {
    return this.description;
  }

  protected constructor(name: string) {
    super();
    this.name = name;
  }

  protected getEntityType(): string {
    return this.entityType;
  }
}
