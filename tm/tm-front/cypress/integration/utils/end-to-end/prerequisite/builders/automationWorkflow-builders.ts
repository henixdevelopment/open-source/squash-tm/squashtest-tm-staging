import { AutomationWorkflowTypes } from '../../../../../../projects/sqtm-core/src/lib/model/plugin/project-plugin';
import { bindAutomationProject } from '../prerequisite-queries';

export class AutomationWorkflowBuilder {
  private readonly workflowType: AutomationWorkflowTypes;
  constructor(workflowType: AutomationWorkflowTypes) {
    this.workflowType = workflowType;
  }
  build(): string[] {
    return [bindAutomationProject(this.workflowType)].flat();
  }
}
