import {
  bindAiServerToProject,
  bindAutomationServerToProject,
  insertAiServer,
  insertAutomationServer,
  insertBugtracker,
  updateProjectWithBugtracker,
  insertBugtrackerProject,
  insertSCMRepository,
  insertSCMServer,
  insertStoredCredentials,
  insertThirdPartyServer,
} from '../prerequisite-queries';
import {
  AuthenticationPolicy,
  AuthenticationProtocol,
} from '../../../../../../projects/sqtm-core/src/lib/model/third-party-server/authentication.model';
import { DEFAULT_ENCRYPTED_CREDENTIALS } from '../prerequisite-helper';
import { TestAutomationServerKind } from '../../../../../../projects/sqtm-core/src/lib/model/test-automation/test-automation-server.model';
import { AutomationWorkflowTypes } from '../../../../../../projects/sqtm-core/src/lib/model/plugin/project-plugin';
import { AuditBuilder } from './common-builders';

export abstract class ThirdPartyServerBuilder extends AuditBuilder {
  private readonly name: string;
  private readonly url: string;
  private authPolicy = AuthenticationPolicy.USER;
  private authProtocol = AuthenticationProtocol.BASIC_AUTH;
  private credentials: CredentialsBuilder;
  private description: string = '';

  protected constructor(name: string, url: string) {
    super();
    this.name = name;
    this.url = url;
  }

  withAppLevelAuthPolicy() {
    this.setAppLevelAuthPolicy();
    return this;
  }

  withUserLevelAuthPolicy() {
    this.setUserLevelAuthPolicy();
    return this;
  }

  withAuthProtocol(authProtocol: AuthenticationProtocol) {
    this.authProtocol = authProtocol;
    return this;
  }

  withCredentials(credentials: CredentialsBuilder) {
    this.credentials = credentials;
    return this;
  }

  withDescription(description: string) {
    this.description = description;
    return this;
  }

  setAppLevelAuthPolicy() {
    this.authPolicy = AuthenticationPolicy.APP_LEVEL;
  }

  setUserLevelAuthPolicy() {
    this.authPolicy = AuthenticationPolicy.USER;
  }

  setAuthProtocol(authProtocol: AuthenticationProtocol) {
    this.authProtocol = authProtocol;
  }

  build(): string[] {
    return [
      insertThirdPartyServer(
        this.name,
        this.url,
        this.authPolicy,
        this.authProtocol,
        this.description,
        this.getCreatedBy(),
        this.getCreatedOn(),
        this.getLastModifiedByStr(),
        this.getLastModifiedOnStr(),
      ),
      this.buildCredentials(),
    ].flat();
  }

  private buildCredentials(): string[] {
    return this.credentials ? this.credentials.build() : [];
  }
}

export class CredentialsBuilder {
  private readonly encryptedCredentials: string;

  constructor(encryptedCredentials?: string) {
    this.encryptedCredentials = encryptedCredentials ?? DEFAULT_ENCRYPTED_CREDENTIALS;
  }

  build(): string[] {
    return [insertStoredCredentials(this.encryptedCredentials)].flat();
  }
}

export class SCMServerBuilder extends ThirdPartyServerBuilder {
  private committerMail = '';
  private scmRepositories: SCMRepositoryBuilder[] = [];

  constructor(name: string, url: string) {
    super(name, url);
    this.setAppLevelAuthPolicy();
  }

  withCommitterMail(committerMail: string) {
    this.committerMail = committerMail;
    return this;
  }

  withRepositories(scmRepositories: SCMRepositoryBuilder[]) {
    this.scmRepositories = scmRepositories;
    return this;
  }

  override build(): string[] {
    return [
      super.build(),
      insertSCMServer(this.committerMail),
      ...this.scmRepositories.map((scmRepository) => scmRepository.build()),
    ].flat();
  }
}

export class SCMRepositoryBuilder {
  private readonly name: string;
  private readonly workingBranch: string;
  private repositoryPath = '';
  private workingFolderPath: string;

  constructor(name: string, workingBranch: string) {
    this.name = name;
    this.workingBranch = workingBranch;
  }

  withRepositoryPath(repositoryPath: string) {
    this.repositoryPath = repositoryPath;
    return this;
  }

  withWorkingFolderPath(workingFolderPath: string) {
    this.workingFolderPath = workingFolderPath;
    return this;
  }

  build(): string[] {
    const workingFolderPathStr = this.workingFolderPath ? `'${this.workingFolderPath}'` : null;
    return [
      insertSCMRepository(this.name, this.workingBranch, this.repositoryPath, workingFolderPathStr),
    ].flat();
  }
}

type BugtrackerType = 'mantis' | 'gitlab.bugtracker';

export class BugtrackerServerBuilder extends ThirdPartyServerBuilder {
  private readonly kind: BugtrackerType;

  constructor(name: string, url: string, kind: BugtrackerType) {
    super(name, url);
    this.setAppLevelAuthPolicy();
    this.kind = kind;
  }

  override build(): string[] {
    return [super.build(), insertBugtracker(this.kind)].flat();
  }
}

export class BugtrackerProjectBindingBuilder {
  build(nameProject: string): string[] {
    return [updateProjectWithBugtracker(), insertBugtrackerProject(nameProject)].flat();
  }
}

export class TestAutomationServerBuilder extends ThirdPartyServerBuilder {
  private readonly kind: TestAutomationServerKind;

  constructor(name: string, url: string, kind: TestAutomationServerKind) {
    super(name, url);
    this.setAppLevelAuthPolicy();
    this.kind = kind;
  }

  override build(): string[] {
    return [super.build(), insertAutomationServer(this.kind)].flat();
  }
}

export class TestAutomationProjectBindingBuilder {
  private readonly workflowType: AutomationWorkflowTypes;
  private readonly nameServer: string;

  constructor(nameServer: string, workflowType: AutomationWorkflowTypes) {
    this.workflowType = workflowType;
    this.nameServer = nameServer;
  }
  build(): string[] {
    return [bindAutomationServerToProject(this.nameServer, this.workflowType)].flat();
  }
}

export class AiServerBuilder extends ThirdPartyServerBuilder {
  private readonly jsonPath: string;

  constructor(name: string, url: string, jsonPath: string) {
    super(name, url);
    this.setAppLevelAuthPolicy();
    this.jsonPath = jsonPath;
  }

  override build(): string[] {
    return [
      super.build(),
      insertAiServer(this.jsonPath),
      insertStoredCredentials(
        'bjDC9IFLNBpPXIGxoO3nwsaI/VCleDY2gOLArNxaPZCXwLT04OOgH5Fc/nIa8R4nVVQ6BkH1qKms+mh1UDw0xWL1MYQuyI6EN37zzdqKwEByntWVmfW50DyWdiIj/1MldcslL9yFO2Krc0OUbkKuD070Vnj5o1uDs8He6J9gDSE8Jsanfnwzn5/LQL97avtOMXzYPumn2GZNWg/+K6JuuCShT5JIz1CSUZ0Sq3Tw85qcygrWTrrovH++8O7EKP/LrrP+zEm0BiQ=',
      ),
    ].flat();
  }
}

export class AiServerProjectBindingBuilder {
  private readonly nameServer: string;
  constructor(nameServer: string) {
    this.nameServer = nameServer;
  }
  build(): string[] {
    return [bindAiServerToProject(this.nameServer)].flat();
  }
}
