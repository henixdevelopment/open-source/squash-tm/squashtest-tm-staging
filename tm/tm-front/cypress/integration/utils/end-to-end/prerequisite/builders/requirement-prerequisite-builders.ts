import {
  insertAttachmentList,
  insertCustomFieldValueFromEntity,
  insertHighLevelRequirement,
  insertMilestoneRequirementVersion,
  insertRequirement,
  insertRequirementFolder,
  insertRequirementLibraryContent,
  insertRequirementLibraryNode,
  insertRequirementVersion,
  insertResource,
  insertRlnRelationship,
  insertSimpleResource,
  updateRequirementCurrentVersion,
  updateRequirementHighLevelRequirementId,
} from '../prerequisite-queries';
import {
  RequirementCriticalityKeys,
  RequirementStatusKeys,
} from '../../../../../../projects/sqtm-core/src/lib/model/level-enums/level-enum';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { NodeBuilder } from './common-builders';

export class RequirementLibraryNodeBuilder extends NodeBuilder {
  constructor(name: string) {
    super(name);
  }

  buildRootNode(index: number): string[] {
    return [
      this.requirementLibraryNodeBaseQueries(),
      insertRequirementLibraryContent(index),
    ].flat();
  }

  build(_index: number, _parentIndex: number, _parentName: string): string[] {
    return this.requirementLibraryNodeBaseQueries();
  }

  private requirementLibraryNodeBaseQueries(): string[] {
    return [
      insertAttachmentList(),
      insertRequirementLibraryNode(
        this.getCreatedBy(),
        this.getCreatedOn(),
        this.getLastModifiedByStr(),
        this.getLastModifiedOnStr(),
      ),
    ].flat();
  }
}

export class RequirementFolderBuilder extends RequirementLibraryNodeBuilder {
  private requirementLibraryNodes: RequirementLibraryNodeBuilder[] = [];

  withRequirementLibraryNodes(requirementLibraryNodes: RequirementLibraryNodeBuilder[]) {
    this.requirementLibraryNodes = requirementLibraryNodes;
    return this;
  }

  override build(index: number, parentIndex: number, parentName: string): string[] {
    return [
      super.build(index, parentIndex, parentName),
      this.requirementFolderBaseQueries(),
      insertRlnRelationship(parentName, index),
      this.requirementFolderLastQueries(index),
    ].flat();
  }

  override buildRootNode(index: number): string[] {
    return [
      super.buildRootNode(index),
      this.requirementFolderBaseQueries(),
      this.requirementFolderLastQueries(index),
    ].flat();
  }

  private requirementFolderBaseQueries(): string[] {
    return [
      insertResource(
        this.getName(),
        this.getDescription(),
        this.getCreatedBy(),
        this.getCreatedOn(),
        this.getLastModifiedByStr(),
        this.getLastModifiedOnStr(),
      ),
      insertSimpleResource(),
      insertRequirementFolder(),
      insertCustomFieldValueFromEntity(
        BindableEntity.REQUIREMENT_FOLDER,
        'RLN_ID',
        'REQUIREMENT_FOLDER',
      ),
    ].flat();
  }

  private requirementFolderLastQueries(index: number): string[] {
    return [
      ...this.requirementLibraryNodes.map((requirementLibraryNode, i) =>
        requirementLibraryNode.build(i, index, this.getName()),
      ),
    ].flat();
  }
}

export class RequirementBuilder extends RequirementLibraryNodeBuilder {
  private criticality: RequirementCriticalityKeys = 'MINOR';
  private requirementStatus: RequirementStatusKeys = 'WORK_IN_PROGRESS';
  private requirements: RequirementBuilder[] = [];
  private versions: RequirementVersionBuilder[] = [];
  private milestones: string[] = [];

  withCriticality(criticality: RequirementCriticalityKeys) {
    this.criticality = criticality;
    return this;
  }

  withRequirementStatus(requirementStatus: RequirementStatusKeys) {
    this.requirementStatus = requirementStatus;
    return this;
  }

  withRequirements(requirements: RequirementBuilder[]) {
    this.requirements = requirements;
    return this;
  }

  withVersions(versions: RequirementVersionBuilder[]) {
    this.versions = versions;
    return this;
  }

  withMilestones(milestones: string[]) {
    this.milestones = milestones;
    return this;
  }

  override build(index: number, parentIndex: number, parentName: string): string[] {
    return [
      super.build(index, parentIndex, parentName),
      this.requirementBaseQueries(),
      this.requirementNonRootQueries(index, parentName),
      this.requirementLastQueries(index),
    ].flat();
  }

  override buildRootNode(index: number): string[] {
    return [
      super.buildRootNode(index),
      this.requirementBaseQueries(),
      this.requirementLastQueries(index),
    ].flat();
  }

  protected requirementBaseQueries(): string[] {
    return [
      insertRequirement(),
      new RequirementVersionBuilder(this.getName())
        .withDescription(this.getDescription())
        .withCreatedBy(this.getCreatedBy())
        .withCreatedOn(this.getCreatedOn())
        .withLastModifiedBy(this.getLastModifiedBy())
        .withLastModifiedOn(this.getLastModifiedOn())
        .withReference(this.getReference())
        .withCriticality(this.criticality)
        .withRequirementStatus(this.requirementStatus)
        .withMilestones(this.milestones)
        .build(1),
    ].flat();
  }

  protected requirementNonRootQueries(index: number, parentName: string): string[] {
    return [
      insertRlnRelationship(parentName, index),
      updateRequirementHighLevelRequirementId(),
    ].flat();
  }

  protected requirementLastQueries(index: number): string[] {
    return [
      ...this.versions.map((version, i) => version.build(i + 2)),
      ...this.requirements.map((requirement, i) => requirement.build(i, index, this.getName())),
    ].flat();
  }
}

export class HighLevelRequirementBuilder extends RequirementBuilder {
  override build(index: number, parentIndex: number, parentName: string): string[] {
    return [super.build(index, parentIndex, parentName)].flat();
  }

  override buildRootNode(index: number): string[] {
    return [super.buildRootNode(index)].flat();
  }

  override requirementBaseQueries(): string[] {
    return [super.requirementBaseQueries(), this.highLevelRequirementBaseQueries()].flat();
  }

  override requirementNonRootQueries(index: number, parentName: string): string[] {
    return [super.requirementNonRootQueries(index, parentName)].flat();
  }

  private highLevelRequirementBaseQueries(): string[] {
    return [insertHighLevelRequirement()].flat();
  }
}

export class RequirementVersionBuilder extends NodeBuilder {
  private criticality: RequirementCriticalityKeys = 'MINOR';
  private requirementStatus: RequirementStatusKeys = 'WORK_IN_PROGRESS';
  private milestones: string[] = [];

  constructor(name: string) {
    super(name);
  }

  withCriticality(criticality: RequirementCriticalityKeys) {
    this.criticality = criticality;
    return this;
  }
  withRequirementStatus(requirementStatus: RequirementStatusKeys) {
    this.requirementStatus = requirementStatus;
    return this;
  }
  withMilestones(milestones: string[]) {
    this.milestones = milestones;
    return this;
  }
  build(index: number): string[] {
    return [
      insertResource(
        this.getName(),
        this.getDescription(),
        this.getCreatedBy(),
        this.getCreatedOn(),
        this.getLastModifiedByStr(),
        this.getLastModifiedOnStr(),
      ),
      insertRequirementVersion(
        index,
        this.getReference(),
        this.criticality,
        this.requirementStatus,
      ),
      updateRequirementCurrentVersion(),
      insertCustomFieldValueFromEntity(
        BindableEntity.REQUIREMENT_VERSION,
        'RES_ID',
        'REQUIREMENT_VERSION',
      ),
      ...this.milestones.map((milestone) => insertMilestoneRequirementVersion(milestone)),
    ].flat();
  }
}
