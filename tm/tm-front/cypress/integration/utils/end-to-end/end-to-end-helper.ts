export function createRegExpForCopyNode(sourceNodeName: string, copyNumber: number = 1): RegExp {
  return new RegExp(`${sourceNodeName}-(Copie|Copy)${copyNumber}`);
}

export function createRegExpForDisabledUserFullName(userFullName: string): RegExp {
  return new RegExp(`${userFullName}\\s\\((désactivé|deactivated)\\)`);
}

export function createRegExpForBugtrackerGitlabName(bugtrackerName: string): RegExp {
  return new RegExp(`${bugtrackerName} (connector|Bugtracker)`);
}
