export function selectByDataTestAnchorLinkId(id: string): string {
  return `[data-test-anchor-link-id="${id}"]`;
}

export function selectByDataTestAnchorLinkIdCounter(id: string): string {
  return `[data-test-anchor-link-id="${id}"]>span.anchor-counter`;
}

export function selectByDataTestElementId(id: string): string {
  return `[data-test-element-id="${id}"]`;
}

export function selectByDataTestButtonId(id: string): string {
  return `[data-test-button-id="${id}"]`;
}

export function selectByDataTestDialogId(id: string): string {
  return `[data-test-dialog-id="${id}"]`;
}

export function selectByDataTestDialogButtonId(id: string): string {
  return `[data-test-dialog-button-id="${id}"]`;
}

export function selectByDataTestToolbarButtonId(id: string): string {
  return `[data-test-toolbar-button-id="${id}"]`;
}

export function selectByDataTestMenuItemId(id: string): string {
  return `[data-test-menu-item-id="${id}"]`;
}

export function selectByDataTestMenuId(id: string): string {
  return `[data-test-menu-id="${id}"]`;
}

export function selectByDataTestFieldId(id: string): string {
  return `[data-test-field-id="${id}"]`;
}

export const DATA_TEST_INPUT_NAME = 'data-test-input-name';

export function selectByDataTestInputName(name: string): string {
  return `[${DATA_TEST_INPUT_NAME}="${name}"]`;
}

export function selectByDataTestFieldName(name: string): string {
  return `[data-test-field-name="${name}"]`;
}

export function selectByDataTestItemId(id: string): string {
  return `[data-test-item-id="${id}"]`;
}

export function selectByDataColumnId(id: string): string {
  return `[data-column-id="${id}"]`;
}

export function selectByDataIcon(id: string): string {
  return `[data-icon="${id}"]`;
}

export function selectByDataTestNavbarFieldId(id: string): string {
  return `[data-test-navbar-field-id="${id}"`;
}

export function selectByDataTestCellId(id: string): string {
  return `[data-test-cell-id="${id}"]`;
}

export function selectByDataTestLinkId(id: string): string {
  return `[data-test-link-id="${id}"]`;
}

export function selectByDataTestCapsuleId(id: string): string {
  return `[data-test-capsule-id="${id}"]`;
}

export function selectByDataTestCustomFieldName(name: string): string {
  return `[data-test-customfield-name="${name}"]`;
}

export function selectByDataTestAttachmentId(id: string): string {
  return `[data-test-attachment-id="${id}"]`;
}
export function selectByDataTestRowId(id: string): string {
  return `[data-test-row-id="${id}"]`;
}

export function selectByDataTestGridId(id: string): string {
  return `[data-test-grid-id="${id}"]`;
}

export function selectByDataTestIconId(id: string): string {
  return `[data-test-icon-id="${id}"]`;
}

export function selectByDataTestListItemId(id: string): string {
  return `[data-test-list-item-id=${id}]`;
}

export function selectByDataTestTestStepId(id: string): string {
  return `[data-test-test-step-id="${id}"]`;
}

export function selectByDataTestErrorKey(id: string) {
  return `[data-test-error-key="${id}"]`;
}
