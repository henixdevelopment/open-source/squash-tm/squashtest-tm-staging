import { isPostgresql } from '../../utils/testing-context';
import Chainable = Cypress.Chainable;

export class DatabaseUtils {
  private static resetSequenceForPostgreSQL(
    tableName: string,
    idColumnName: string,
    sequenceName: string,
  ) {
    const request = `SELECT setval('${sequenceName}',
                                   COALESCE((SELECT MAX(${idColumnName}) FROM ${tableName}), 0)+1, false
                           );`;

    console.log(request);

    this.executeQuery(request).then(() => {
      console.log(`Sequence reset with success : ${sequenceName}`);
    });
  }

  private static getTableNameAndPrimaryKeyColumnNameQueryForMariaDB(): string {
    return `SELECT table_name as tablename,
                   column_name as pkcolumnname
            FROM information_schema.columns
            WHERE table_schema = (SELECT DATABASE())
              AND extra = 'auto_increment'
              AND column_key = 'PRI';`;
  }

  private static getTableNameAndPrimaryKeyColumnNameQueryForPostgreSQL(): string {
    return `SELECT table_name as tablename,
                   column_name as pkcolumnname,
                   pg_get_serial_sequence(table_name, column_name) as sequencename
            FROM information_schema.columns
            WHERE table_schema = current_schema()
              AND is_identity = 'YES'
              AND LOWER(table_name) not in ('core_user')
            ORDER BY table_name;`;
  }

  private static resetAutoIncrementForMariaDB(tableName: string, idColumnName: string) {
    const request = `SET @m = (SELECT COALESCE(MAX(${idColumnName}), 0) + 1 FROM ${tableName});
            SET @s = CONCAT('ALTER TABLE ${tableName} AUTO_INCREMENT=', @m);
            PREPARE stmt1 FROM @s;
            EXECUTE stmt1;
            DEALLOCATE PREPARE stmt1;`;
    console.log(request);
    this.executeQuery(request).then(() => {
      console.log(
        `Auto-increment on table ${tableName} and primary key ${idColumnName} reset with success`,
      );
    });
  }

  public static cleanDatabase() {
    cy.task('cleanDatabase').then(() => {
      console.log('Database Cleaned...');
    });
  }

  public static executeQuery(query: string): Chainable {
    return cy.task('executeQuery', query);
  }

  // Execute select query and return rows as result with column names in lowercase
  public static executeSelectQuery(query: string): Chainable {
    return cy.task('executeSelectQuery', query);
  }

  public static resetAllSequences() {
    console.log('Should reset sequences');
    isPostgresql()
      ? this.resetAllSequencesForPostgreSQL()
      : this.resetAllAutoIncrementsForMariaDB();
  }

  private static resetAllSequencesForPostgreSQL() {
    this.executeSelectQuery(this.getTableNameAndPrimaryKeyColumnNameQueryForPostgreSQL()).then(
      (tables) => {
        tables.forEach((table) => {
          this.resetSequenceForPostgreSQL(table.tablename, table.pkcolumnname, table.sequencename);
        });
      },
    );
  }

  private static resetAllAutoIncrementsForMariaDB() {
    this.executeQuery(this.getTableNameAndPrimaryKeyColumnNameQueryForMariaDB()).then((tables) => {
      tables.forEach((table) => {
        this.resetAutoIncrementForMariaDB(table.tablename, table.pkcolumnname);
      });
    });
  }

  public static createUuidFunction(): string {
    return isPostgresql() ? 'uuid_generate_v4()' : 'uuid()';
  }

  // In PostgreSQL, if order is ASC, hyphens should be at the end of the array and conversely for MariaDB
  public static sortHyphenStartOrEndInArrayAsc(array: string[]): string[] {
    return isPostgresql() ? this.sortStartHyphensToEnd(array) : this.sortEndHyphensToStart(array);
  }
  // In PostgreSQL, if order is DESC, hyphens should be at the start of the array and conversely for MariaDB
  public static sortHyphenStartOrEndInArrayDesc(array: string[]): string[] {
    return isPostgresql() ? this.sortEndHyphensToStart(array) : this.sortStartHyphensToEnd(array);
  }

  private static sortStartHyphensToEnd(array: string[]) {
    while (array[0] === '-') {
      array.push(array.splice(0, 1)[0]);
    }
    return array;
  }
  private static sortEndHyphensToStart(array: string[]) {
    while (array.slice(-1)[0] === '-') {
      array.splice(0, 0, array.pop());
    }
    return array;
  }
}
