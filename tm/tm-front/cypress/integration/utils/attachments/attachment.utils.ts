import { HttpMockBuilder } from '../mocks/request-mock';
import { UploadSummary } from '../../../../projects/sqtm-core/src/lib/model/attachment/upload-summary.model';

export class AttachmentUtils {
  static addAttachments(
    dropZoneSelector: string,
    files: File[],
    attachmentListId?: number,
    uploadSummary?: UploadSummary[],
  ) {
    const mock = new HttpMockBuilder(`attach-list/${attachmentListId || '*'}/attachments/upload`)
      .post()
      .responseBody(uploadSummary)
      .build();
    cy.get(dropZoneSelector).trigger('drop', {
      dataTransfer: {
        files,
      },
    });
    mock.wait();
  }
}
