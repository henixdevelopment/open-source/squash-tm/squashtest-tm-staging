export function getRow(id: string) {
  return `[data-test-row-id="${id}"]`;
}

export function getCell(id: string) {
  return `[data-test-cell-id="${id}"]`;
}
