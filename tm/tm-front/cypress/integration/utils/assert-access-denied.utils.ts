import { AdminReferentialDataMockBuilder } from './referential/admin-referential-data-builder';
import { AdminReferentialDataProviderBuilder } from './referential/admin-referential-data.provider';
import { ReferentialDataMockBuilder } from './referential/referential-data-builder';
import { ReferentialDataProviderBuilder } from './referential/referential-data.provider';
import { HttpMockBuilder } from './mocks/request-mock';
import { AuthenticatedUser } from '../../../projects/sqtm-core/src/lib/model/user/authenticated-user.model';

export function assertAccessDenied(url: string) {
  const userData: AuthenticatedUser = {
    admin: false,
    username: 'gerard',
    userId: 99,
    hasAnyReadPermission: true,
    projectManager: false,
    milestoneManager: false,
    clearanceManager: false,
    functionalTester: true,
    automationProgrammer: false,
    firstName: '',
    lastName: '',
    canDeleteFromFront: true,
  };

  const adminRefData = new AdminReferentialDataMockBuilder().withUser(userData).build();
  const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder(
    adminRefData,
  ).build();

  const referentialData = new ReferentialDataMockBuilder().withUser(userData).build();
  const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();

  const welcomeMessage = 'Hello, Squash!';
  const homePageModelMock = new HttpMockBuilder('home-workspace')
    .responseBody({ welcomeMessage })
    .build();

  cy.visit(url);
  adminReferentialDataProvider.wait();
  referentialDataProvider.wait();
  homePageModelMock.wait();

  cy.get('[data-test-element-id="custom-welcome-message"]').should('contain.html', welcomeMessage);
}
