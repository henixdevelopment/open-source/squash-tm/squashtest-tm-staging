import { AuthenticatedUser } from '../../../../projects/sqtm-core/src/lib/model/user/authenticated-user.model';
import { CustomField } from '../../../../projects/sqtm-core/src/lib/model/customfield/customfield.model';
import { TestAutomationServerKind } from '../../../../projects/sqtm-core/src/lib/model/test-automation/test-automation-server.model';
import { TemplateConfigurablePlugin } from '../../../../projects/sqtm-core/src/lib/model/plugin/template-configurable-plugin.model';
import { BugTrackerReferentialDto } from '../../../../projects/sqtm-core/src/lib/model/bugtracker/bug-tracker.model';
import { AdminReferentialDataModel } from '../../../../projects/sqtm-core/src/lib/model/referential-data/admin-referential-data.model';
import { LicenseInformationModel } from '../../../../projects/sqtm-core/src/lib/model/referential-data/license-information.model';
import { ScmServerKind } from '../../../../projects/sqtm-core/src/lib/model/scm-server/scm-server.model';
import { GlobalConfigurationModel } from '../../../../projects/sqtm-core/src/lib/model/referential-data/global-configuration.model';

export class AdminReferentialDataMockBuilder {
  private readonly _data: AdminReferentialDataModel;

  constructor() {
    this._data = getDefaultAdminReferentialData();
  }

  public withUser(user: AuthenticatedUser): this {
    this._data.user = user;
    return this;
  }

  public withLicenseInformation(licenseInformation: LicenseInformationModel): this {
    this._data.licenseInformation = licenseInformation;
    return this;
  }

  public withCustomFields(customFields: CustomField[]): this {
    this._data.customFields = customFields;
    return this;
  }

  public withAvailableTestAutomationServerKinds(kinds: TestAutomationServerKind[]): this {
    this._data.availableTestAutomationServerKinds = kinds;
    return this;
  }

  public withAvailableScmServerKinds(kinds: ScmServerKind[]): this {
    this._data.availableScmServerKinds = kinds;
    return this;
  }

  public withTemplateConfigurablePlugins(
    templateConfigurablePlugins: TemplateConfigurablePlugin[],
  ): this {
    this._data.templateConfigurablePlugins = templateConfigurablePlugins;
    return this;
  }

  withBugTrackers(bugTrackers: BugTrackerReferentialDto[]): any {
    this._data.bugTrackers = bugTrackers;
    return this;
  }

  public withUltimateLicenseAvailable(ultimateLicenseAvailable: boolean): this {
    this._data.ultimateLicenseAvailable = ultimateLicenseAvailable;
    return this;
  }

  withGlobalConfiguration(globalConfiguration: GlobalConfigurationModel): this {
    this._data.globalConfiguration = globalConfiguration;
    return this;
  }

  public build(): AdminReferentialDataModel {
    return {
      ...this._data,
    };
  }
}

export function getDefaultAdminReferentialData() {
  const defaultAdminReferentialData: AdminReferentialDataModel = {
    user: {
      userId: 1,
      username: 'admin',
      firstName: 'admin',
      lastName: 'admin',
      admin: true,
      hasAnyReadPermission: false,
      projectManager: false,
      milestoneManager: false,
      clearanceManager: false,
      functionalTester: false,
      automationProgrammer: false,
      canDeleteFromFront: true,
    },
    globalConfiguration: {
      milestoneFeatureEnabled: true,
      uploadFileExtensionWhitelist: [],
      uploadFileSizeLimit: 0,
      bannerMessage: null,
      searchActivationFeatureEnabled: false,
      unsafeAttachmentPreviewEnabled: false,
    },
    licenseInformation: {
      pluginLicenseExpiration: null,
      activatedUserExcess: null,
    },
    customFields: [],
    availableTestAutomationServerKinds: [],
    availableScmServerKinds: [ScmServerKind.git],
    canManageLocalPassword: true,
    templateConfigurablePlugins: [],
    bugTrackers: [],
    documentationLinks: [],
    callbackUrl: '',
    premiumPluginInstalled: true,
    ultimateLicenseAvailable: false,
    availableReportIds: [],
    synchronizationPlugins: getDefaultSynchronizationPluginsData(),
    aiServers: [],
    jwtSecretDefined: true,
  };
  return defaultAdminReferentialData;
}

export function getDefaultSynchronizationPluginsData() {
  return [
    { id: 'henix.plugin.automation.workflow.automjira', name: 'Workflow Automatisation Jira' },
    { id: 'squash.tm.plugin.jirasync', name: 'Xsquash4Jira' },
    { id: 'squash.tm.plugin.xsquash4gitlab', name: 'xsquash4gitlab' },
  ];
}
