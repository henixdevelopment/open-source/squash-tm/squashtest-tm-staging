import {
  ALL_BINDABLE_ENTITIES,
  ALL_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder,
} from './referential-data-builder';
import { InputType } from '../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';

export const defaultReferentialData = new ReferentialDataMockBuilder()
  .withProjects(
    {
      name: 'Project 1',
      permissions: ALL_PROJECT_PERMISSIONS,
    },
    {
      name: 'Project 2',
      permissions: ALL_PROJECT_PERMISSIONS,
    },
  )
  .withMilestones(
    {
      label: 'Milestone 1',
      status: 'PLANNED',
      boundProjectIndexes: [0],
    },
    {
      label: 'Milestone 2',
      status: 'IN_PROGRESS',
      boundProjectIndexes: [0],
    },
    {
      label: 'Milestone 3',
      status: 'FINISHED',
      boundProjectIndexes: [0],
    },
    {
      label: 'Milestone 4',
      status: 'LOCKED',
      boundProjectIndexes: [1],
    },
  )
  .withCustomFields(
    {
      code: 'CODE',
      defaultValue: 'a little value',
      inputType: InputType.PLAIN_TEXT,
      label: 'FIELD A',
      name: 'fieldA',
      optional: false,
      bindings: [{ bindableEntities: ALL_BINDABLE_ENTITIES, projectIndexes: [0] }],
    },
    {
      code: 'CODE_2',
      defaultValue: 'a little value',
      inputType: InputType.NUMERIC,
      label: 'FIELD B',
      name: 'fieldB',
      optional: false,
      numericDefaultValue: 2,
      bindings: [{ bindableEntities: ALL_BINDABLE_ENTITIES, projectIndexes: [0] }],
    },
    {
      code: 'CODE_3',
      defaultValue: 'o1|o2',
      inputType: InputType.TAG,
      label: 'FIELD C',
      name: 'fieldC',
      optional: false,
      options: [
        {
          label: 'o1',
          code: 'OPTION_1',
        },
        {
          label: 'o2',
          code: 'OPTION_2',
        },
        {
          label: 'o3',
          code: 'OPTION_2',
        },
      ],
      bindings: [{ bindableEntities: ALL_BINDABLE_ENTITIES, projectIndexes: [0] }],
    },
    {
      code: 'CODE_4',
      defaultValue: 'a little value',
      inputType: InputType.PLAIN_TEXT,
      label: 'FIELD D',
      name: 'fieldD',
      optional: true,
      bindings: [{ bindableEntities: ALL_BINDABLE_ENTITIES, projectIndexes: [0] }],
    },
    {
      code: 'CODE_5',
      defaultValue: 'a little value',
      inputType: InputType.RICH_TEXT,
      label: 'FIELD E',
      name: 'fieldE',
      optional: false,
      largeDefaultValue: '<p>a little value</p>',
      bindings: [{ bindableEntities: ALL_BINDABLE_ENTITIES, projectIndexes: [0] }],
    },
    {
      code: 'CODE_6',
      defaultValue: '',
      inputType: InputType.DATE_PICKER,
      label: 'Date',
      name: 'date',
      optional: true,
      largeDefaultValue: '2020-02-14',
      bindings: [{ bindableEntities: ALL_BINDABLE_ENTITIES, projectIndexes: [0] }],
    },
    {
      code: 'CODE_7',
      defaultValue: '',
      inputType: InputType.CHECKBOX,
      label: 'Checkbox',
      name: 'checkbox',
      optional: true,
      largeDefaultValue: 'true',
      bindings: [{ bindableEntities: ALL_BINDABLE_ENTITIES, projectIndexes: [0] }],
    },
    {
      code: 'CODE_7',
      defaultValue: '',
      inputType: InputType.DROPDOWN_LIST,
      label: 'Dropdown',
      name: 'dropdown',
      optional: true,
      largeDefaultValue: 'Option C',
      options: [
        {
          label: 'Option A',
          colour: 'red',
          code: 'A',
        },
        {
          label: 'Option C',
          colour: '',
          code: 'C',
        },
        {
          label: 'Option B',
          colour: '',
          code: 'B',
        },
      ],
      bindings: [{ bindableEntities: ALL_BINDABLE_ENTITIES, projectIndexes: [0] }],
    },
  )
  .build();
