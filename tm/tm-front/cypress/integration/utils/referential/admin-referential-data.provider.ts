import { HTTP_RESPONSE_STATUS, HttpMock, HttpMockBuilder } from '../mocks/request-mock';
import { getDefaultAdminReferentialData } from './admin-referential-data-builder';
import { AdminReferentialDataModel } from '../../../../projects/sqtm-core/src/lib/model/referential-data/admin-referential-data.model';

export class AdminReferentialDataProvider {
  constructor(private httpRequestMock: HttpMock<AdminReferentialDataModel>) {}

  wait(): void {
    cy.wait(this.httpRequestMock.alias)
      .its('response.statusCode')
      .should('eq', HTTP_RESPONSE_STATUS.SUCCESS);
  }
}

export class AdminReferentialDataProviderBuilder {
  constructor(
    private adminReferentialData: AdminReferentialDataModel = getDefaultAdminReferentialData(),
  ) {}

  build(): AdminReferentialDataProvider {
    const httpRequestMock = new HttpMockBuilder<AdminReferentialDataModel>('referential/admin')
      .responseBody(this.adminReferentialData)
      .build();
    return new AdminReferentialDataProvider(httpRequestMock);
  }
}
