import { HTTP_RESPONSE_STATUS, HttpMock, HttpMockBuilder } from '../mocks/request-mock';
import { getSystemInfoLists } from './referential-data-builder';
import { Project } from '../../../../projects/sqtm-core/src/lib/model/project/project.model';
import { BindableEntity } from '../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { Permissions } from '../../../../projects/sqtm-core/src/lib/model/permissions/permissions.model';
import { WorkspaceTypeForPlugins } from '../../../../projects/sqtm-core/src/lib/model/project/project-data.model';
import { ReferentialDataModel } from '../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';

const project1: Project = {
  id: 1,
  name: 'project 1',
  requirementCategoryId: 1,
  testCaseNatureId: 2,
  testCaseTypeId: 3,
  milestoneBindings: [],
  customFieldBindings: {
    [BindableEntity.REQUIREMENT_FOLDER]: [],
    [BindableEntity.REQUIREMENT_VERSION]: [],
    [BindableEntity.TESTCASE_FOLDER]: [],
    [BindableEntity.TEST_CASE]: [],
    [BindableEntity.TEST_STEP]: [],
    [BindableEntity.CAMPAIGN_FOLDER]: [],
    [BindableEntity.CAMPAIGN]: [],
    [BindableEntity.ITERATION]: [],
    [BindableEntity.TEST_SUITE]: [],
    [BindableEntity.EXECUTION]: [],
    [BindableEntity.EXECUTION_STEP]: [],
    [BindableEntity.CUSTOM_REPORT_FOLDER]: [],
    [BindableEntity.SPRINT_GROUP]: [],
    [BindableEntity.SPRINT]: [],
  },
  permissions: {
    PROJECT: [
      Permissions.READ,
      Permissions.WRITE,
      Permissions.DELETE,
      Permissions.ADMIN,
      Permissions.IMPORT,
      Permissions.MANAGE_PROJECT,
      Permissions.MANAGE_MILESTONE,
      Permissions.MANAGE_PROJECT_CLEARANCE,
      Permissions.ATTACH,
    ],
    PROJECT_TEMPLATE: [
      Permissions.READ,
      Permissions.WRITE,
      Permissions.DELETE,
      Permissions.IMPORT,
      Permissions.ADMIN,
      Permissions.MANAGE_PROJECT,
      Permissions.MANAGE_MILESTONE,
      Permissions.MANAGE_PROJECT_CLEARANCE,
      Permissions.ATTACH,
    ],
    REQUIREMENT_LIBRARY: [
      Permissions.READ,
      Permissions.WRITE,
      Permissions.CREATE,
      Permissions.DELETE,
      Permissions.ATTACH,
      Permissions.EXPORT,
    ],
    TEST_CASE_LIBRARY: [
      Permissions.READ,
      Permissions.WRITE,
      Permissions.CREATE,
      Permissions.DELETE,
      Permissions.ATTACH,
    ],
    CAMPAIGN_LIBRARY: [
      Permissions.READ,
      Permissions.WRITE,
      Permissions.CREATE,
      Permissions.DELETE,
      Permissions.ATTACH,
      Permissions.EXPORT,
      Permissions.EXECUTE,
      Permissions.LINK,
      Permissions.EXTENDED_DELETE,
      Permissions.READ_UNASSIGNED,
    ],
    CUSTOM_REPORT_LIBRARY: [
      Permissions.READ,
      Permissions.WRITE,
      Permissions.CREATE,
      Permissions.DELETE,
      Permissions.ATTACH,
      Permissions.EXPORT,
    ],
    AUTOMATION_REQUEST_LIBRARY: [
      Permissions.READ,
      Permissions.WRITE,
      Permissions.CREATE,
      Permissions.DELETE,
      Permissions.ATTACH,
    ],
  },
  disabledExecutionStatus: [],
  keywords: [
    { label: 'Étant donné que', value: 'GIVEN' },
    { label: 'Quand', value: 'WHEN' },
    { label: 'Alors', value: 'THEN' },
    { label: 'Et', value: 'AND' },
    { label: 'Mais', value: 'BUT' },
  ],
  bddScriptLanguage: 'FRENCH',
  allowTcModifDuringExec: true,
  activatedPlugins: {
    [WorkspaceTypeForPlugins.CAMPAIGN_WORKSPACE]: [],
    [WorkspaceTypeForPlugins.TEST_CASE_WORKSPACE]: [],
    [WorkspaceTypeForPlugins.REQUIREMENT_WORKSPACE]: [],
  },
} as Project;

export class ReferentialDataProvider {
  constructor(private httpRequestMock: HttpMock<ReferentialDataModel>) {}

  wait(): void {
    cy.wait(this.httpRequestMock.alias)
      .its('response.statusCode')
      .should('eq', HTTP_RESPONSE_STATUS.SUCCESS);
  }
}

export class ReferentialDataProviderBuilder {
  constructor(private referentialData: ReferentialDataModel = basicReferentialData) {}

  build(): ReferentialDataProvider {
    const httpRequestMock = new HttpMockBuilder<ReferentialDataModel>('referential')
      .responseBody(this.referentialData)
      .build();
    return new ReferentialDataProvider(httpRequestMock);
  }
}

export const basicReferentialData: ReferentialDataModel = {
  isAdmin: null,
  aiServers: [],
  projects: [project1],
  customFields: [],
  filteredProjectIds: [],
  projectFilterStatus: false,
  infoLists: getSystemInfoLists(),
  globalConfiguration: {
    milestoneFeatureEnabled: false,
    uploadFileExtensionWhitelist: ['txt'],
    uploadFileSizeLimit: 50000,
    bannerMessage: null,
    searchActivationFeatureEnabled: false,
    unsafeAttachmentPreviewEnabled: false,
  },
  bugTrackers: [],
  milestones: [],
  automationServers: [],
  user: {
    username: 'admin',
    userId: 1,
    admin: true,
    hasAnyReadPermission: false,
    projectManager: false,
    milestoneManager: false,
    clearanceManager: false,
    functionalTester: false,
    automationProgrammer: false,
    lastName: 'admin',
    firstName: '',
    canDeleteFromFront: true,
  },
  requirementVersionLinkTypes: [],
  workspaceWizards: [],
  licenseInformation: {
    pluginLicenseExpiration: null,
    activatedUserExcess: null,
  },
  automatedTestTechnologies: [],
  availableTestAutomationServerKinds: [],
  availableScmServerKinds: [],
  scmServers: [],
  templateConfigurablePlugins: [],
  premiumPluginInstalled: false,
  ultimateLicenseAvailable: false,
  availableReportIds: [],
  documentationLinks: [],
  callbackUrl: '',
  projectPermissions: [],
  canManageLocalPassword: true,
  synchronizationPlugins: [],
  jwtSecretDefined: true,
};
