import { TestCaseViewPage } from '../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { TestCaseWorkspacePage } from '../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { mockTestCaseModel } from '../../../data-mock/test-case.data-mock';
import { mockGridResponse, mockTreeNode } from '../../../data-mock/grid.data-mock';
import { GridElement, TreeElement } from '../../../page-objects/elements/grid/grid.element';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { ReferentialDataProviderBuilder } from '../../../utils/referential/referential-data.provider';
import { AdminReferentialDataProviderBuilder } from '../../../utils/referential/admin-referential-data.provider';
import { ProjectViewPage } from '../../../page-objects/pages/administration-workspace/project-view/project-view.page';
import { makeProjectViewData } from '../../../data-mock/administration-views.data-mock';
import { TestCaseModel } from '../../../../../projects/sqtm-core/src/lib/model/test-case/test-case.model';

const standardTestCaseModel: TestCaseModel = mockTestCaseModel({
  id: 1,
  projectId: 1,
  name: 'TestCase1',
  kind: 'STANDARD',
});

const gherkinTestCaseModel: TestCaseModel = mockTestCaseModel({
  id: 2,
  projectId: 1,
  name: 'TestCase2',
  kind: 'GHERKIN',
});

const firstTestCaseDataRow = mockTreeNode({
  id: 'TestCase-1',
  data: {
    NAME: 'TestCase1',
    CHILD_COUNT: 0,
    TC_STATUS: 'APPROVED',
    TC_KIND: 'STANDARD',
    IMPORTANCE: 'HIGH',
  },
});

const secondTestCaseDataRow = mockTreeNode({
  id: 'TestCase-2',
  data: {
    NAME: 'TestCase2',
    CHILD_COUNT: 0,
    TC_STATUS: 'APPROVED',
    TC_KIND: 'GHERKIN',
    IMPORTANCE: 'HIGH',
  },
});

const initialNodes = mockGridResponse(null, [
  mockTreeNode({
    id: 'TestCaseLibrary-1',
    children: ['TestCase-1', 'TestCase-2'],
    data: { NAME: 'Project1' },
  }),
  firstTestCaseDataRow,
  secondTestCaseDataRow,
]);

describe('Anchors navigation', function () {
  it('should recall the latest anchor when switching entity', () => {
    const { workspace, page } = navigateToStandardTestCase();
    const infoPanel = page.informationPanel;
    infoPanel.assertExists();
    const executionsPanel = page.showExecutions(mockGridResponse('id', []));
    executionsPanel.assertExists();

    selectGherkinTestCase(workspace);
    const newExecutionGrid = GridElement.createGridElement(
      'test-case-view-execution',
      '',
      mockGridResponse('id', []),
    );
    newExecutionGrid.assertExists();
  });

  it('should switch back to default anchor when the active one cannot be found', () => {
    const { workspace, page } = navigateToStandardTestCase();
    const infoPanel = page.informationPanel;
    infoPanel.assertExists();
    const stepsPanel = page.clickStepsAnchorLink();
    stepsPanel.assertNewStepEditorsAreReady();
    stepsPanel.assertExists();

    const gherkinPage = selectGherkinTestCase(workspace);
    gherkinPage.informationPanel.assertExists();
  });

  it('should allow direct access to a anchor group in Test case workspace', () => {
    const referentialDataProvider = new ReferentialDataProviderBuilder().build();
    const tree = TreeElement.createTreeElement(
      'test-case-workspace-main-tree',
      'test-case-tree',
      initialNodes,
    );
    const pageMock = new HttpMockBuilder(`test-case-view/2?**`)
      .responseBody(gherkinTestCaseModel)
      .build();

    cy.visit('test-case-workspace/test-case/2/script');
    referentialDataProvider.wait();
    tree.waitInitialDataFetch();
    pageMock.wait();

    const page: TestCaseViewPage = new TestCaseViewPage(2);
    page.scriptPanel.assertExists();
  });

  it('should allow direct access to a anchor group in Test case workspace', () => {
    const referentialDataProvider = new ReferentialDataProviderBuilder().build();
    const tree = TreeElement.createTreeElement(
      'test-case-workspace-main-tree',
      'test-case-tree',
      initialNodes,
    );
    const pageMock = new HttpMockBuilder(`test-case-view/2?**`)
      .responseBody(gherkinTestCaseModel)
      .build();

    cy.visit('test-case-workspace/test-case/2/content?anchor=verified-requirements');
    referentialDataProvider.wait();
    tree.waitInitialDataFetch();
    pageMock.wait();

    const page: TestCaseViewPage = new TestCaseViewPage(2);
    page.anchors.assertLinkIsActive('verified-requirements');
  });

  it('should redirect to default anchor link after a direct access to non-existing anchor', () => {
    const referentialDataProvider = new ReferentialDataProviderBuilder().build();
    const tree = TreeElement.createTreeElement(
      'test-case-workspace-main-tree',
      'test-case-tree',
      initialNodes,
    );
    const pageMock = new HttpMockBuilder(`test-case-view/1?**`)
      .responseBody(standardTestCaseModel)
      .build();

    cy.visit('test-case-workspace/test-case/1/content?anchor=foobar');
    referentialDataProvider.wait();
    tree.waitInitialDataFetch();
    pageMock.wait();

    const page: TestCaseViewPage = new TestCaseViewPage(1);
    page.informationPanel.assertExists();
  });

  it('should redirect to default anchor group after a direct access to non-visible anchor', () => {
    const referentialDataProvider = new ReferentialDataProviderBuilder().build();
    const tree = TreeElement.createTreeElement(
      'test-case-workspace-main-tree',
      'test-case-tree',
      initialNodes,
    );
    const pageMock = new HttpMockBuilder(`test-case-view/1?**`)
      .responseBody(standardTestCaseModel)
      .build();

    cy.visit('test-case-workspace/test-case/1/script');
    referentialDataProvider.wait();
    tree.waitInitialDataFetch();
    pageMock.wait();

    const page: TestCaseViewPage = new TestCaseViewPage(1);
    page.anchors.assertLinkIsActive('information');
  });

  it('should allow direct access to a anchor group in Administration - Project workspace', () => {
    new AdminReferentialDataProviderBuilder().build();
    const gridElement = GridElement.createGridElement(
      'projects',
      'generic-projects',
      mockGridResponse('id', []),
    );
    const pageMock = new HttpMockBuilder(`project-view/1?**`)
      .responseBody(makeProjectViewData())
      .build();
    cy.visit(`administration-workspace/projects/1/plugins`);
    gridElement.waitInitialDataFetch();
    pageMock.wait();

    const projectViewPage = new ProjectViewPage();
    projectViewPage.assertExists();
    projectViewPage.pluginPanel.assertExists();
  });

  it('should allow direct access to a anchor link in Administration - Project workspace', () => {
    new AdminReferentialDataProviderBuilder().build();
    const gridElement = GridElement.createGridElement(
      'projects',
      'generic-projects',
      mockGridResponse('id', []),
    );
    const pageMock = new HttpMockBuilder(`project-view/1?**`)
      .responseBody(makeProjectViewData())
      .build();
    cy.visit(`administration-workspace/projects/1/custom?anchor=info-lists`);
    gridElement.waitInitialDataFetch();
    pageMock.wait();

    const projectViewPage = new ProjectViewPage();
    projectViewPage.assertExists();
  });
});

function navigateToStandardTestCase(): {
  workspace: TestCaseWorkspacePage;
  page: TestCaseViewPage;
} {
  const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes);
  return {
    workspace: testCaseWorkspacePage,
    page: testCaseWorkspacePage.tree.selectNode<TestCaseViewPage>(
      'TestCase-1',
      standardTestCaseModel,
    ),
  };
}

function selectGherkinTestCase(workspace: TestCaseWorkspacePage): TestCaseViewPage {
  return workspace.tree.selectNode<TestCaseViewPage>('TestCase-2', gherkinTestCaseModel);
}
