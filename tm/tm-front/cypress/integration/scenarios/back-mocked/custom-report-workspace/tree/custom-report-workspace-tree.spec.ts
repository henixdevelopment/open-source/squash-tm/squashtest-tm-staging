import { CustomReportWorkspacePage } from '../../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { defaultReferentialData } from '../../../../utils/referential/default-referential-data.const';
import { mockCustomReportDashboardModel } from '../../../../data-mock/custom-report.data-mock';
import {
  DataRowModel,
  DataRowOpenState,
} from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

function getCustomReportLibraryChildNodes() {
  return [
    mockTreeNode({
      id: 'CustomReportLibrary-1',
      children: ['CustomReportFolder-2', 'ChartDefinition-3'],
      data: { NAME: 'International Space Station', CHILD_COUNT: '3' },
      state: DataRowOpenState.open,
      projectId: 1,
    }),
    mockTreeNode({
      id: 'CustomReportFolder-2',
      children: [],
      parentRowId: 'CustomReportLibrary-1',
      data: { NAME: 'Structural Requirements Reports' },
      state: DataRowOpenState.closed,
      projectId: 1,
    }),
    mockTreeNode({
      id: 'ChartDefinition-3',
      children: [],
      parentRowId: 'CustomReportLibrary-1',
      state: DataRowOpenState.leaf,
      projectId: 1,
      data: {
        CRLN_ID: 3,
        CHILD_COUNT: 0,
        NAME: 'Financial Report',
      },
    }),
  ];
}

function getCustomReportFolderChildNodes() {
  return [
    mockTreeNode({
      id: 'CustomReportFolder-2',
      children: [
        'ChartDefinition-4',
        'CustomReportDashboard-5',
        'ReportDefinition-6',
        'CustomReportCustomExport-7',
      ],
      parentRowId: 'CustomReportLibrary-1',
      data: { NAME: 'Structural Requirements Reports' },
      state: DataRowOpenState.open,
      projectId: 1,
    }),
    mockTreeNode({
      id: 'ChartDefinition-4',
      children: [],
      parentRowId: 'CustomReportFolder-2',
      state: DataRowOpenState.leaf,
      projectId: 1,
      data: {
        CRLN_ID: 4,
        CHILD_COUNT: 0,
        NAME: 'Structure by age',
      },
    }),
    mockTreeNode({
      id: 'CustomReportDashboard-5',
      children: [],
      parentRowId: 'CustomReportFolder-2',
      state: DataRowOpenState.leaf,
      projectId: 1,
      data: {
        CRLN_ID: 5,
        CHILD_COUNT: 0,
        NAME: 'Structure control dashboard',
      },
    }),
    mockTreeNode({
      id: 'ReportDefinition-6',
      children: [],
      parentRowId: 'CustomReportFolder-2',
      projectId: 1,
      state: DataRowOpenState.leaf,
      data: {
        CRLN_ID: 6,
        CHILD_COUNT: 0,
        NAME: 'Impact report',
      },
    }),
    mockTreeNode({
      id: 'CustomReportCustomExport-7',
      children: [],
      parentRowId: 'CustomReportFolder-2',
      projectId: 1,
      state: DataRowOpenState.leaf,
      data: {
        CRLN_ID: 7,
        CHILD_COUNT: 0,
        NAME: 'Pre launch checklist',
      },
    }),
  ];
}

function extractName(library: DataRowModel) {
  return library.data['NAME'];
}

describe('Custom Report Workspace Tree Display', function () {
  const initialNodes = mockGridResponse('id', [
    mockTreeNode({
      id: 'CustomReportLibrary-1',
      children: [],
      data: { NAME: 'International Space Station', CHILD_COUNT: '3' },
      projectId: 1,
      state: DataRowOpenState.closed,
    }),
    mockTreeNode({
      id: 'CustomReportLibrary-2',
      children: [],
      data: { NAME: 'STS - Shuttle', CHILD_COUNT: '3' },
      state: DataRowOpenState.closed,
      projectId: -2, // Non-existing project: result in read-only permissions
    }),
  ]);

  it('should display a simple tree', () => {
    const firstNode = initialNodes.dataRows[0];
    const secondNode = initialNodes.dataRows[1];
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage(initialNodes);
    const tree = customReportWorkspacePage.tree;
    tree.assertNodeExist(firstNode.id);
    tree.assertNodeTextContains(firstNode.id, firstNode.data['NAME']);
    tree.assertNodeExist(secondNode.id);
    tree.assertNodeTextContains(secondNode.id, secondNode.data['NAME']);
  });

  it('should open and close various nodes', () => {
    const library = initialNodes.dataRows[0];
    const secondLibrary = initialNodes.dataRows[1];
    const customReportLibraryChildNodes = getCustomReportLibraryChildNodes();
    const folder = customReportLibraryChildNodes[1];
    const rootChart = customReportLibraryChildNodes[2];

    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage(initialNodes);
    const tree = customReportWorkspacePage.tree;
    new NavBarElement().toggle();
    tree.assertNodeExist('CustomReportLibrary-1');
    tree.assertNodeTextContains('CustomReportLibrary-1', 'International Space Station');

    // opening and checking library
    tree.openNode(library.id, customReportLibraryChildNodes);
    tree.assertNodeExist(library.id);
    tree.assertNodeTextContains(library.id, extractName(library));
    tree.assertNodeIsOpen(library.id);
    tree.assertNodeExist(rootChart.id);
    tree.assertNodeTextContains(rootChart.id, extractName(rootChart));
    tree.assertNodeExist(folder.id);
    tree.assertNodeTextContains(folder.id, extractName(folder));
    tree.assertNodeIsClosed(folder.id);
    tree.assertNodeOrderByName([
      extractName(library),
      extractName(rootChart),
      extractName(folder),
      extractName(secondLibrary),
    ]);

    // opening and checking folder
    const folderChildNodes = getCustomReportFolderChildNodes();
    const secondChart = folderChildNodes[1];
    const dashboard = folderChildNodes[2];
    const report = folderChildNodes[3];
    const customExport = folderChildNodes[4];
    tree.openNode(folder.id, folderChildNodes);
    tree.assertNodeIsOpen(folder.id);
    tree.assertNodeExist(secondChart.id);
    tree.assertNodeTextContains(secondChart.id, extractName(secondChart));
    tree.assertNodeExist(dashboard.id);
    tree.assertNodeTextContains(dashboard.id, extractName(dashboard));
    tree.assertNodeExist(report.id);
    tree.assertNodeTextContains(report.id, extractName(report));
    tree.assertNodeExist(customExport.id);
    tree.assertNodeTextContains(customExport.id, extractName(customExport));

    // checking full order by name
    tree.assertNodeOrderByName([
      extractName(library),
      extractName(rootChart),
      extractName(folder),
      extractName(report),
      extractName(customExport),
      extractName(secondChart),
      extractName(dashboard),
      extractName(secondLibrary),
    ]);

    tree.closeNode(folder.id);
    tree.assertNodeOrderByName([
      extractName(library),
      extractName(rootChart),
      extractName(folder),
      extractName(secondLibrary),
    ]);
  });

  it('should update creation menu', () => {
    const library = initialNodes.dataRows[0];
    const customReportLibraryChildNodes = getCustomReportLibraryChildNodes();
    const folder = customReportLibraryChildNodes[1];

    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage(initialNodes, {
      ...defaultReferentialData,
    });
    const treeMenu = customReportWorkspacePage.treeMenu;
    const tree = customReportWorkspacePage.tree;
    new NavBarElement().toggle();

    treeMenu.assertCreateButtonIsDisabled();
    tree.openNode(library.id, customReportLibraryChildNodes);
    tree.selectNode(library.id);
    treeMenu.assertCreateChartButtonIsEnabled();
    treeMenu.assertCreateDashboardButtonIsEnabled();
    treeMenu.hideCreateMenu();
    tree.selectNode(folder.id);
    treeMenu.assertCreateChartButtonIsEnabled();
    treeMenu.assertCreateDashboardButtonIsEnabled();
    treeMenu.hideCreateMenu();

    const folderChildNodes = getCustomReportFolderChildNodes();
    const dashboard = folderChildNodes[2];
    tree.openNode(folder.id, folderChildNodes);
    tree.selectNode(dashboard.id, mockCustomReportDashboardModel());
    treeMenu.assertCreateChartButtonIsEnabled();
    treeMenu.assertCreateChartButtonIsEnabled();

    const readOnlyLibrary = initialNodes.dataRows[1];
    tree.selectNode(readOnlyLibrary.id);
    treeMenu.assertCreateMenuIsDisabled();
  });

  it('should create dashboard', () => {
    const library = initialNodes.dataRows[0];
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage(initialNodes, {
      ...defaultReferentialData,
    });
    const treeMenu = customReportWorkspacePage.treeMenu;
    const tree = customReportWorkspacePage.tree;
    const customReportLibraryChildNodes = getCustomReportLibraryChildNodes();
    new NavBarElement().toggle();

    tree.openNode(library.id, customReportLibraryChildNodes);
    tree.selectNode(library.id);
    const createEntityDialog = treeMenu.openCreateDashboard();
    createEntityDialog.fillName('a nice dashboard');
    const createdNode = mockTreeNode({
      id: 'CustomReportDashboard-8',
      children: [],
      parentRowId: 'CustomReportLibrary-1',
      state: DataRowOpenState.leaf,
      projectId: 1,
      data: {
        CRLN_ID: 8,
        CHILD_COUNT: 0,
        NAME: 'a nice dashboard',
      },
    });

    const refreshedNodes = getCustomReportLibraryChildNodes();
    refreshedNodes[0].children = [
      'CustomReportFolder-2',
      'ChartDefinition-3',
      'CustomReportDashboard-8',
    ];
    refreshedNodes.push(createdNode);
    createEntityDialog.addForSuccessOpen(
      'CustomReportLibrary-1',
      8,
      createdNode,
      { dataRows: refreshedNodes },
      null,
    );
  });
});
