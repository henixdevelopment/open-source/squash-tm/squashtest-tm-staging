import { CustomReportWorkspacePage } from '../../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { ReferentialDataMockBuilder } from '../../../../utils/referential/referential-data-builder';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import {
  mockChartDefinitionModel,
  mockCustomReportFolderModel,
  mockCustomReportLibraryModel,
} from '../../../../data-mock/custom-report.data-mock';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

describe('Custom Report Workspace Tree Delete', function () {
  const initialNodes = mockGridResponse('id', [
    mockTreeNode({
      id: 'CustomReportLibrary-1',
      projectId: 1,
      children: [],
      data: { NAME: 'Project1', CHILD_COUNT: '3' },
      state: DataRowOpenState.closed,
    }),
    mockTreeNode({
      id: 'CustomReportLibrary-2',
      projectId: 2,
      children: [],
      data: { NAME: 'Project2', CHILD_COUNT: '1' },
      state: DataRowOpenState.closed,
    }),
  ]);

  const libraryRefreshAtOpen = [
    mockTreeNode({
      id: 'CustomReportLibrary-1',
      projectId: 1,
      children: ['CustomReportFolder-1', 'ChartDefinition-2', 'CustomReportFolder-2'],
      data: { NAME: 'Project1', CHILD_COUNT: '3' },
      state: DataRowOpenState.open,
    }),
    mockTreeNode({
      id: 'CustomReportFolder-1',
      children: [],
      projectId: 1,
      parentRowId: 'CustomReportLibrary-1',
      data: { NAME: 'folder1' },
      state: DataRowOpenState.closed,
    }),
    mockTreeNode({
      id: 'ChartDefinition-2',
      children: [],
      projectId: 1,
      parentRowId: 'CustomReportLibrary-1',
      data: { NAME: 'a nice chart' },
      state: DataRowOpenState.leaf,
    }),
    mockTreeNode({
      id: 'CustomReportFolder-2',
      children: [],
      projectId: 1,
      parentRowId: 'CustomReportLibrary-1',
      data: { NAME: 'folder2' },
      state: DataRowOpenState.leaf,
    }),
  ];

  it('should not be able to delete from tree', () => {
    const referentialData = new ReferentialDataMockBuilder()
      .withUser({
        canDeleteFromFront: false,
      })
      .build();

    const workspacePage = CustomReportWorkspacePage.initTestAtPage(initialNodes, referentialData);
    workspacePage.treeMenu.assertDeleteButtonIsHidden();
  });

  it('should activate or deactivate delete button according to user selection', () => {
    const firstNode = initialNodes.dataRows[0];
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage(initialNodes);
    const tree = customReportWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    customReportWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
    tree.selectNode('CustomReportFolder-1', mockCustomReportFolderModel());
    customReportWorkspacePage.treeMenu.assertDeleteButtonIsActive();
    tree.selectNode('ChartDefinition-2', mockChartDefinitionModel());
    customReportWorkspacePage.treeMenu.assertDeleteButtonIsActive();
    tree.selectNode('CustomReportLibrary-1', mockCustomReportLibraryModel());
    customReportWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
  });

  it('should show server warnings when deleting', () => {
    const firstNode = initialNodes.dataRows[0];
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage(initialNodes);
    const tree = customReportWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    customReportWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
    tree.selectNode('CustomReportFolder-1');
    customReportWorkspacePage.treeMenu.assertDeleteButtonIsActive();
    const confirmDialog = customReportWorkspacePage.treeMenu.initDeletion(
      'custom-report-tree',
      [1],
      ['warning_called'],
    );
    confirmDialog.checkWarningMessages(['warning_called']);
  });

  it('should delete node and remove it from tree', () => {
    const libRefreshed = mockTreeNode({
      id: 'CustomReportLibrary-1',
      projectId: 1,
      children: ['CustomReportFolder-1', 'ChartDefinition-2', 'CustomReportFolder-2'],
      data: { NAME: 'Project1', CHILD_COUNT: '3' },
      state: DataRowOpenState.open,
    });
    const crf1 = mockTreeNode({
      id: 'CustomReportFolder-1',
      children: [],
      data: { NAME: 'folder1' },
      projectId: 1,
      parentRowId: 'CustomReportLibrary-1',
    });
    const c = mockTreeNode({
      id: 'ChartDefinition-2',
      children: [],
      data: { NAME: 'a nice chart' },
      projectId: 1,
      parentRowId: 'CustomReportLibrary-1',
    });
    const crf2 = mockTreeNode({
      id: 'CustomReportFolder-2',
      children: [],
      data: { NAME: 'folder2' },
      projectId: 1,
      parentRowId: 'CustomReportLibrary-1',
    });
    const childNodes = [libRefreshed, crf1, c, crf2];
    const firstNode = initialNodes.dataRows[0];
    const campaignWorkspacePage = CustomReportWorkspacePage.initTestAtPage(initialNodes);
    const tree = campaignWorkspacePage.tree;
    tree.openNode(firstNode.id, childNodes);
    campaignWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
    tree.selectNode('CustomReportFolder-1');
    campaignWorkspacePage.treeMenu.assertDeleteButtonIsActive();
    const confirmDialog = campaignWorkspacePage.treeMenu.initDeletion('custom-report-tree', [1]);
    const libRefreshedAfterDelete = mockTreeNode({
      id: 'CustomReportLibrary-1',
      projectId: 1,
      children: ['ChartDefinition-2', 'CustomReportFolder-2'],
      data: { NAME: 'Project1', CHILD_COUNT: '2' },
      state: DataRowOpenState.open,
    });
    const refreshedContent = [libRefreshedAfterDelete, c, crf2];
    const selectedParentMock = new HttpMockBuilder('/custom-report-library-view/1?**')
      .responseBody({})
      .build();
    confirmDialog.deleteNodes([1], ['CustomReportLibrary-1'], refreshedContent);
    selectedParentMock.wait();
    campaignWorkspacePage.tree.assertNodeNotExist('CustomReportFolder-1');
    campaignWorkspacePage.tree.assertNodeExist('CustomReportFolder-2');
    campaignWorkspacePage.tree.assertNodeExist('ChartDefinition-2');
  });
});
