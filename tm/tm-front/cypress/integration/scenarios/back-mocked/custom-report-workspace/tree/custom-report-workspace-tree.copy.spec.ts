import {
  ALL_PROJECT_PERMISSIONS,
  NO_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder,
} from '../../../../utils/referential/referential-data-builder';
import { CustomReportWorkspacePage } from '../../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

describe('Custom Report Workspace Tree Copy', function () {
  function referentialData() {
    return new ReferentialDataMockBuilder()
      .withProjects(
        {
          name: 'Project 1',
          permissions: ALL_PROJECT_PERMISSIONS,
        },
        {
          name: 'Project 2',
          permissions: NO_PROJECT_PERMISSIONS,
        },
      )
      .build();
  }

  const initialNodes = mockGridResponse('id', [
    mockTreeNode({
      id: 'CustomReportLibrary-1',
      projectId: 1,
      children: [],
      data: { NAME: 'Project1', CHILD_COUNT: '3' },
      state: DataRowOpenState.closed,
    }),
    mockTreeNode({
      id: 'CustomReportLibrary-2',
      projectId: 2,
      children: [],
      data: { NAME: 'Project2', CHILD_COUNT: '1' },
      state: DataRowOpenState.closed,
    }),
  ]);

  const libraryRefreshAtOpen = [
    mockTreeNode({
      id: 'CustomReportLibrary-1',
      projectId: 1,
      children: ['CustomReportFolder-1', 'CustomReportFolder-2', 'ChartDefinition-2'],
      data: { NAME: 'Project1', CHILD_COUNT: '3' },
      state: DataRowOpenState.open,
    }),
    mockTreeNode({
      id: 'CustomReportFolder-1',
      children: ['ChartDefinition-3'],
      projectId: 1,
      parentRowId: 'CustomReportLibrary-1',
      data: { NAME: 'folder1' },
      state: DataRowOpenState.open,
    }),
    mockTreeNode({
      id: 'ChartDefinition-3',
      children: [],
      projectId: 1,
      parentRowId: 'CustomReportFolder-1',
      data: { NAME: 'a nice chart' },
    }),
    mockTreeNode({
      id: 'CustomReportFolder-2',
      children: [],
      projectId: 1,
      parentRowId: 'CustomReportLibrary-1',
      data: { NAME: 'folder2' },
    }),
    mockTreeNode({
      id: 'ChartDefinition-2',
      children: [],
      projectId: 1,
      parentRowId: 'CustomReportLibrary-1',
      data: { NAME: 'a chart' },
    }),
  ];

  function catchIncomingUncaughtErrors() {
    // some tests throw an uncaught error on purpose (when clicking on some ChartDefinitions),
    // it seems these tests can't pass anymore after upgrade to angular 14
    // we have to catch explicitly the incoming error
    cy.on('uncaught:exception', (err) => {
      expect(err.message).to.include('Unable to find scope type for type');
      return false;
    });
  }

  it('should activate or deactivate copy button according to user selection', () => {
    catchIncomingUncaughtErrors();

    const firstNode = initialNodes.dataRows[0];
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage(initialNodes);
    const tree = customReportWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.selectNode(firstNode.id);
    customReportWorkspacePage.treeMenu.assertCopyButtonIsDisabled();
    customReportWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    // the component should never render with empty model, but we don't care, we are just testing the menu
    tree.selectNode('CustomReportFolder-1');
    customReportWorkspacePage.treeMenu.assertCopyButtonIsActive();
    customReportWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    tree.selectNode('ChartDefinition-2');
    customReportWorkspacePage.treeMenu.assertCopyButtonIsActive();
    customReportWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    tree.selectNode('CustomReportLibrary-1');
    customReportWorkspacePage.treeMenu.assertCopyButtonIsDisabled();
    customReportWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
  });

  it('should activate or deactivate paste button according to destination', () => {
    catchIncomingUncaughtErrors();

    const firstNode = initialNodes.dataRows[0];
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage(
      initialNodes,
      referentialData(),
    );
    const tree = customReportWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.selectNode(firstNode.id);
    customReportWorkspacePage.treeMenu.assertCopyButtonIsDisabled();
    customReportWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    // The component should never render with empty model, but we don't care, we are just testing the menu
    tree.selectNode('ChartDefinition-2');
    customReportWorkspacePage.treeMenu.assertCopyButtonIsActive();
    customReportWorkspacePage.treeMenu.copy();
    customReportWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    tree.selectNode('CustomReportFolder-1');
    customReportWorkspacePage.treeMenu.assertPasteButtonIsActive();
    tree.selectNode('CustomReportLibrary-1');
    customReportWorkspacePage.treeMenu.assertPasteButtonIsActive();
    tree.selectNode('ChartDefinition-3');
    customReportWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    // Testing permissions
    tree.selectNode('CustomReportLibrary-2');
    customReportWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
  });

  it('should copy paste a node', () => {
    catchIncomingUncaughtErrors();

    const refreshedNodes = [
      mockTreeNode({
        id: 'CustomReportFolder-2',
        projectId: 1,
        children: ['ChartDefinition-4'],
        parentRowId: 'CustomReportLibrary-1',
        state: DataRowOpenState.open,
        data: { NAME: 'folder2', CHILD_COUNT: 1 },
      }),
      mockTreeNode({
        id: 'ChartDefinition-4',
        projectId: 1,
        children: [],
        parentRowId: 'CustomReportFolder-2',
        data: { NAME: 'a nice chart' },
      }),
    ];
    const firstNode = initialNodes.dataRows[0];
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage(
      initialNodes,
      referentialData(),
    );
    const tree = customReportWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.selectNode('ChartDefinition-3');
    customReportWorkspacePage.treeMenu.copy();
    tree.selectNode('CustomReportFolder-2');
    customReportWorkspacePage.treeMenu.paste(
      { dataRows: refreshedNodes },
      'custom-report-tree',
      'CustomReportFolder-2',
    );
    tree.assertNodeExist('ChartDefinition-4');
    tree.assertNodeIsOpen('CustomReportFolder-2');
  });
});
