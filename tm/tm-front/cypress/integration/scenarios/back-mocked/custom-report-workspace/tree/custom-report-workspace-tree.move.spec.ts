import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { CustomReportWorkspacePage } from '../../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

describe('Custom Report Workspace Tree Move', function () {
  const initialNodes = mockGridResponse('id', [
    mockTreeNode({
      id: 'CustomReportLibrary-1',
      projectId: 1,
      children: [],
      data: { NAME: 'Project1', CHILD_COUNT: '3' },
      state: DataRowOpenState.closed,
    }),
    mockTreeNode({
      id: 'CustomReportLibrary-2',
      projectId: 2,
      children: [],
      data: { NAME: 'Project2', CHILD_COUNT: '0' },
      state: DataRowOpenState.closed,
    }),
  ]);

  const crLib1 = mockTreeNode({
    id: 'CustomReportLibrary-1',
    projectId: 1,
    children: ['CustomReportFolder-1', 'ChartDefinition-3', 'CustomReportFolder-2'],
    data: { NAME: 'Project1', CHILD_COUNT: '3' },
    state: DataRowOpenState.open,
  });

  const crFolder1 = mockTreeNode({
    id: 'CustomReportFolder-1',
    children: [],
    projectId: 1,
    parentRowId: 'CustomReportLibrary-1',
    data: { NAME: 'Folder1' },
    state: DataRowOpenState.closed,
  });

  const c3 = mockTreeNode({
    id: 'ChartDefinition-3',
    children: [],
    parentRowId: 'CustomReportLibrary-1',
    state: DataRowOpenState.leaf,
    projectId: 1,
    data: { NAME: 'A nice chart' },
  });

  const crFolder2 = mockTreeNode({
    id: 'CustomReportFolder-2',
    children: [],
    projectId: 1,
    parentRowId: 'CustomReportLibrary-1',
    data: { NAME: 'Folder2' },
    state: DataRowOpenState.closed,
  });

  const libraryRefreshAtOpen = [crLib1, crFolder1, c3, crFolder2];

  it('should drop into folder', () => {
    const firstNode = initialNodes.dataRows[0];
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage(initialNodes);
    new NavBarElement().toggle();
    const tree = customReportWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.beginDragAndDrop(crFolder1.id);
    tree.assertNodeNotExist(crFolder1.id);
    tree.dragOverCenter(crFolder2.id);
    tree.assertContainerIsDndTarget(crFolder2.id);
    const refreshedRows = [
      { ...crLib1, children: [c3.id, crFolder2.id] },
      { ...crFolder1, parentRowId: crFolder2.id },
      { ...c3 },
      { ...crFolder2, children: [crFolder1.id], state: DataRowOpenState.open },
    ];
    tree.drop(crFolder2.id, 'CustomReportLibrary-1,CustomReportFolder-2', refreshedRows);
    tree.assertRowHasParent(crFolder1.id, crFolder2.id);
  });

  it('should show rows when dragging multiple rows', () => {
    const firstNode = initialNodes.dataRows[0];
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage(initialNodes);
    new NavBarElement().toggle();
    const tree = customReportWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.selectNode(crFolder2.id);
    tree.addNodeToSelection(crFolder1.id, {});
    tree.beginDragAndDrop(crFolder2.id);
    tree.assertNodeNotExist(crFolder2.id);
    tree.assertNodeNotExist(crFolder1.id);
    tree.assertDndPlaceholderContains(crFolder2.id, 'Folder2');
    tree.assertDndPlaceholderContains(crFolder1.id, 'Folder1');
  });
});
