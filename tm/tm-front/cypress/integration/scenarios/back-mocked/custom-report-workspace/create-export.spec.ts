import { NavBarElement } from '../../../page-objects/elements/nav-bar/nav-bar.element';
import { CreateExportViewPage } from '../../../page-objects/pages/custom-report-workspace/create-export-view.page';
import {
  ALL_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder,
} from '../../../utils/referential/referential-data-builder';
import { BindableEntity } from '../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { mockGridResponse, mockTreeNode } from '../../../data-mock/grid.data-mock';
import { InputType } from '../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { CustomExportEntityType } from '../../../../../projects/sqtm-core/src/lib/model/custom-report/custom-export-columns.model';
import {
  CustomExportModel,
  CustomFieldExportColumnData,
} from '../../../../../projects/sqtm-core/src/lib/model/custom-report/custom-export.model';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { DataRowOpenState } from '../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { HttpMock, HttpMockBuilder } from '../../../utils/mocks/request-mock';

function buildAvailableCufColumnsMock(): HttpMock<CustomFieldExportColumnData[]> {
  const cufColumns: CustomFieldExportColumnData[] = [
    {
      cufId: 1,
      label: 'My CUF',
      bindableEntity: BindableEntity.CAMPAIGN,
    },
    {
      cufId: 2,
      label: 'My CUF2',
      bindableEntity: BindableEntity.CAMPAIGN,
    },
  ];

  return new HttpMockBuilder<CustomFieldExportColumnData[]>('custom-exports/available-cuf-columns')
    .post()
    .responseBody(cufColumns)
    .build();
}

describe('Create Custom Export View', function () {
  it('should create and display a custom export', () => {
    const workbenchPage = navigateToCustomExportWorkbench(true);
    new NavBarElement().toggle();
    workbenchPage.assertExists();

    workbenchPage.changeName('New');

    const scopeSelector = workbenchPage.openScopeSelector(mockCampaignGridResponse());
    const availableCufColumnsMock = buildAvailableCufColumnsMock();
    scopeSelector.selectNode('Campaign 1');
    scopeSelector.confirm();
    availableCufColumnsMock.wait();

    workbenchPage.dragAndDropAttribute('Description');
    workbenchPage.dragAndDropAttribute('My CUF2');

    const customExportViewPage = workbenchPage.confirm(
      3,
      mockCustomLibraryGridResponse(),
      mockCustomExportModel(),
    );
    customExportViewPage.assertExists();
  });

  it('should show milestone columns', () => {
    const workbenchPage = navigateToCustomExportWorkbench(true);
    new NavBarElement().toggle();
    workbenchPage.assertExists();
    workbenchPage.assertMilestoneColumnsExist();
  });

  it('should not show milestone columns', () => {
    const workbenchPage = navigateToCustomExportWorkbench(false);
    new NavBarElement().toggle();
    workbenchPage.assertExists();
    workbenchPage.assertMilestoneColumnsNotExist();
  });
});

function navigateToCustomExportWorkbench(enableMilestoneMode: boolean): CreateExportViewPage {
  return CreateExportViewPage.initTestAtPage(
    new ReferentialDataMockBuilder()
      .withProjects({
        name: 'P',
        permissions: ALL_PROJECT_PERMISSIONS,
      })
      .withCustomFields(
        {
          name: 'My CUF',
          code: 'MyCUF',
          inputType: InputType.PLAIN_TEXT,
          label: 'My CUF',
          optional: false,
          bindings: [
            {
              bindableEntities: [BindableEntity.CAMPAIGN],
              projectIndexes: [0],
            },
          ],
        },
        {
          name: 'My CUF2',
          code: 'MyCUF2',
          inputType: InputType.PLAIN_TEXT,
          label: 'My CUF2',
          optional: false,
          bindings: [
            {
              bindableEntities: [BindableEntity.CAMPAIGN],
              projectIndexes: [0],
            },
          ],
        },
      )
      .withGlobalConfiguration({
        milestoneFeatureEnabled: enableMilestoneMode,
        uploadFileExtensionWhitelist: [],
        uploadFileSizeLimit: 0,
        bannerMessage: null,
        searchActivationFeatureEnabled: false,
        unsafeAttachmentPreviewEnabled: false,
      })
      .build(),
  );
}

function mockCampaignGridResponse(): GridResponse {
  return mockGridResponse('id', [
    mockTreeNode({
      id: 'CampaignLibrary-1',
      children: ['CampaignFolder-2'],
      projectId: 1,
      state: DataRowOpenState.open,
      data: { NAME: 'Project1', CHILD_COUNT: 1 },
    }),
    mockTreeNode({
      id: 'CampaignFolder-2',
      children: ['Campaign-3'],
      parentRowId: 'CampaignLibrary-1',
      projectId: 1,
      state: DataRowOpenState.open,
      data: { NAME: 'Folder 1', CHILD_COUNT: 1 },
    }),
    mockTreeNode({
      id: 'Campaign-3',
      children: [],
      projectId: 1,
      parentRowId: 'CampaignFolder-2',
      data: { NAME: 'Campaign 1', CHILD_COUNT: 0 },
    }),
  ]);
}

function mockCustomExportModel(): CustomExportModel {
  return {
    name: 'New',
    columns: [
      {
        columnName: 'CAMPAIGN_DESCRIPTION',
        cufId: null,
        entityType: CustomExportEntityType.CAMPAIGN,
      },
      {
        columnName: 'CAMPAIGN_CUF',
        cufId: 1,
        entityType: CustomExportEntityType.CAMPAIGN,
      },
    ],
    id: 3,
    projectId: 1,
    customReportLibraryNodeId: 3,
    customFieldsOnScope: { 1: 'Camp' },
    scopeNodes: [],
    lastModifiedBy: 'Cypress',
    lastModifiedOn: new Date().toISOString(),
    createdBy: 'Cypress',
    createdOn: new Date().toISOString(),
  };
}

function mockCustomLibraryGridResponse(): GridResponse {
  return mockGridResponse('id', [
    mockTreeNode({
      id: 'CustomReportCustomExport-3',
      children: [],
      state: DataRowOpenState.leaf,
      data: {
        CRLN_ID: 3,
        CHILD_COUNT: 0,
        NAME: 'Financial Breakdown',
      },
    }),
  ]);
}
