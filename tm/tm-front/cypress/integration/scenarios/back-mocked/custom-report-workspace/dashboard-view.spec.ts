import { CustomReportWorkspacePage } from '../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import { DashboardViewPage } from '../../../page-objects/pages/custom-report-workspace/dashboard-view.page';
import { getSimpleChartDefinition } from '../../../data-mock/custom-chart.mocks';
import { NavBarElement } from '../../../page-objects/elements/nav-bar/nav-bar.element';
import { CustomDashboardModel } from '../../../../../projects/sqtm-core/src/lib/model/custom-report/custom-dashboard.model';
import {
  ChartDefinitionModel,
  ChartType,
} from '../../../../../projects/sqtm-core/src/lib/model/custom-report/chart-definition.model';
import { DataRowOpenState } from '../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { mockTreeNode } from '../../../data-mock/grid.data-mock';

function getCreatedBiding() {
  return {
    id: 1,
    col: 1,
    row: 1,
    sizeX: 1,
    sizeY: 1,
    dashboardId: 1,
    reportDefinitionId: 1,
  };
}

describe('Dashboard View', function () {
  beforeEach(() => {
    cy.viewport(1280, 800);
  });

  it('should display dashboard page', () => {
    const dashboardView = navigateToDashboardView();
    dashboardView.assertExists();
    dashboardView.assertNameContains('Financial Breakdown');
    dashboardView.assertBindingIsRendered(1);
    dashboardView.assertEmptyDropzoneIsNotVisible();
  });

  it('should add chart in empty dashboard', () => {
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage({
      dataRows: getCustomReportLibraryChildNodes(),
    });
    const model: Partial<CustomDashboardModel> = getEmptyModel();
    const dashboardView = customReportWorkspacePage.tree.selectNode<DashboardViewPage>(
      'CustomReportDashboard-3',
      model,
    );
    dashboardView.assertExists();
    dashboardView.assertEmptyDropzoneIsVisible();
    customReportWorkspacePage.tree.beginDragAndDrop('ChartDefinition-4');
    dashboardView.dropChartIntoEmptyZone('1', {
      ...getSimpleChartBinding(),
      chartInstance: getSimpleChartDefinition(ChartType.CUMULATIVE) as ChartDefinitionModel,
    });
    dashboardView.assertBindingIsRendered(1);
    dashboardView.assertChartBindingIsRendered(1);
    dashboardView.assertEmptyDropzoneIsNotVisible();
    dashboardView.assertDropzoneIsVisible();
  });

  it('should add and remove chart in partial dashboard', () => {
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage({
      dataRows: getCustomReportLibraryChildNodes(),
    });
    new NavBarElement().toggle();
    const model: Partial<CustomDashboardModel> = getDashboardWithOneChartModel();
    const dashboardView = customReportWorkspacePage.tree.selectNode<DashboardViewPage>(
      'CustomReportDashboard-3',
      model,
    );
    dashboardView.assertExists();
    dashboardView.assertEmptyDropzoneIsNotVisible();
    customReportWorkspacePage.tree.beginDragAndDrop('ChartDefinition-4');
    dashboardView.dropChart('1', {
      ...getSimpleChartBinding(),
      col: 2,
      id: 2,
      chartInstance: getSimpleChartDefinition(ChartType.CUMULATIVE) as ChartDefinitionModel,
    });
    dashboardView.assertBindingIsRendered(1);
    dashboardView.assertBindingIsRendered(2);
    dashboardView.assertChartBindingIsRendered(1);
    dashboardView.assertChartBindingIsRendered(2);
    dashboardView.removeChart(1);
    dashboardView.assertBindingIsNotRendered(1);
    dashboardView.removeChart(2);
    dashboardView.assertBindingIsNotRendered(2);
  });

  it('should add report in empty dashboard', () => {
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage({
      dataRows: getCustomReportLibraryChildNodes(),
    });
    const model: Partial<CustomDashboardModel> = getEmptyModel();
    const dashboardView = customReportWorkspacePage.tree.selectNode<DashboardViewPage>(
      'CustomReportDashboard-3',
      model,
    );
    dashboardView.assertExists();
    dashboardView.assertEmptyDropzoneIsVisible();
    customReportWorkspacePage.tree.beginDragAndDrop('ReportDefinition-5');
    dashboardView.dropReportIntoEmptyZone('1', {
      ...getCreatedBiding(),
      reportInstance: {
        id: 1,
        pluginNamespace: 'report.books.testcases.editable',
        description: 'this report looks pretty',
        name: 'my sweet report',
        parameters: 'parameters',
        summary: 'this is the best report ever',
        projectId: 1,
      },
    });
    dashboardView.assertBindingIsRendered(1);
    dashboardView.assertReportBindingIsRendered(1, 1);
    dashboardView.assertEmptyDropzoneIsNotVisible();
    dashboardView.assertDropzoneIsVisible();
  });

  it('should add and remove report in partial dashboard', () => {
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage({
      dataRows: getCustomReportLibraryChildNodes(),
    });
    const model: Partial<CustomDashboardModel> = getDashboardWithOneChartModel();
    const dashboardView = customReportWorkspacePage.tree.selectNode<DashboardViewPage>(
      'CustomReportDashboard-3',
      model,
    );
    dashboardView.assertExists();
    dashboardView.assertEmptyDropzoneIsNotVisible();
    customReportWorkspacePage.tree.beginDragAndDrop('ReportDefinition-5');
    dashboardView.dropReport('1', {
      ...getCreatedBiding(),
      col: 2,
      id: 2,
      reportInstance: {
        id: 1,
        pluginNamespace: 'report.books.testcases.editable',
        description: 'this report looks pretty',
        name: 'my sweet report',
        parameters: 'parameters',
        summary: 'this is the best report ever',
        projectId: 1,
      },
    });
    dashboardView.assertBindingIsRendered(1);
    dashboardView.assertBindingIsRendered(2);
    dashboardView.assertChartBindingIsRendered(1);
    dashboardView.assertReportBindingIsRendered(2, 1);
    dashboardView.removeReport(2);
    dashboardView.assertBindingIsNotRendered(2);
  });

  it('it should swap chart', () => {
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage({
      dataRows: getCustomReportLibraryChildNodes(),
    });
    const model: Partial<CustomDashboardModel> = getDashboardWithOneChartModel();
    const dashboardView = customReportWorkspacePage.tree.selectNode<DashboardViewPage>(
      'CustomReportDashboard-3',
      model,
    );
    dashboardView.assertExists();
    dashboardView.assertEmptyDropzoneIsNotVisible();
    customReportWorkspacePage.tree.beginDragAndDrop('ChartDefinition-4');
    dashboardView.dropForSwappingChart('1', '4', {
      ...getCreatedBiding(),
      col: 2,
      id: 2,
      chartDefinitionId: 4,
      chartInstance: getSimpleChartDefinition(ChartType.CUMULATIVE) as ChartDefinitionModel,
    });
    dashboardView.assertBindingIsRendered(1);
  });

  it('should choose dashboard has favorite', () => {
    const dashboardView = navigateToDashboardView();
    const favoriteMenu = dashboardView.showFavoriteList();
    favoriteMenu.shouldExist();
  });

  function getCustomReportLibraryChildNodes() {
    return [
      mockTreeNode({
        id: 'CustomReportLibrary-1',
        children: [
          'CustomReportFolder-2',
          'CustomReportDashboard-3',
          'ChartDefinition-4',
          'ReportDefinition-5',
        ],
        data: { NAME: 'International Space Station', CHILD_COUNT: '3' },
        state: DataRowOpenState.open,
      }),
      mockTreeNode({
        id: 'CustomReportFolder-2',
        children: [],
        parentRowId: 'CustomReportLibrary-1',
        data: { NAME: 'Structural Requirements Reports' },
        state: DataRowOpenState.closed,
      }),
      mockTreeNode({
        id: 'CustomReportDashboard-3',
        children: [],
        parentRowId: 'CustomReportLibrary-1',
        state: DataRowOpenState.leaf,
        data: {
          CRLN_ID: 3,
          CHILD_COUNT: 0,
          NAME: 'Financial Breakdown',
        },
      }),
      mockTreeNode({
        id: 'ChartDefinition-4',
        children: [],
        parentRowId: 'CustomReportLibrary-1',
        state: DataRowOpenState.leaf,
        data: {
          CRLN_ID: 4,
          CHILD_COUNT: 0,
          NAME: 'Bill of Material',
        },
      }),
      mockTreeNode({
        id: 'ReportDefinition-5',
        children: [],
        parentRowId: 'CustomReportLibrary-1',
        state: DataRowOpenState.leaf,
        data: {
          CRLN_ID: 5,
          CHILD_COUNT: 0,
          NAME: 'Reg report',
        },
      }),
    ];
  }

  function getEmptyModel() {
    return {
      id: 1,
      customReportLibraryNodeId: 3,
      projectId: 1,
      name: 'Financial Breakdown',
      chartBindings: [],
      reportBindings: [],
      favoriteWorkspaces: [],
    };
  }

  function navigateToDashboardView(): DashboardViewPage {
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage({
      dataRows: getCustomReportLibraryChildNodes(),
    });
    const model: Partial<CustomDashboardModel> = getDashboardWithOneChartModel();
    return customReportWorkspacePage.tree.selectNode<DashboardViewPage>(
      'CustomReportDashboard-3',
      model,
    );
  }

  function getDashboardWithOneChartModel() {
    return {
      id: 1,
      customReportLibraryNodeId: 3,
      projectId: 1,
      name: 'Financial Breakdown',
      chartBindings: [getSimpleChartBinding()],
      reportBindings: [],
      favoriteWorkspaces: [],
    };
  }

  function getSimpleChartBinding() {
    return {
      id: 1,
      col: 1,
      row: 1,
      sizeX: 1,
      sizeY: 1,
      dashboardId: 1,
      chartInstance: getSimpleChartDefinition(ChartType.CUMULATIVE) as ChartDefinitionModel,
      chartDefinitionId: 1,
    };
  }
});
