import { ChartDefinitionViewPage } from '../../../page-objects/pages/custom-report-workspace/chart/chart-definition-view.page';
import { CustomReportWorkspacePage } from '../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import { getSimpleCustomReportLibraryChildNodes } from '../../../data-mock/custom-report.data-mock';
import { NavBarElement } from '../../../page-objects/elements/nav-bar/nav-bar.element';
import { getSimpleChartDefinition, getThreeAxisModel } from '../../../data-mock/custom-chart.mocks';
import {
  ChartColumnType,
  ChartDataType,
  ChartDefinitionModel,
  ChartOperation,
  ChartType,
} from '../../../../../projects/sqtm-core/src/lib/model/custom-report/chart-definition.model';
import { EntityType } from '../../../../../projects/sqtm-core/src/lib/model/entity.model';

describe('Chart Definition View', function () {
  it('should display a pie chart with correct information and just one axis', () => {
    const model = getSimpleChartDefinition(ChartType.PIE);
    const chartDefinitionViewPage = navigateToChartView(model);
    chartDefinitionViewPage.foldTree();
    new NavBarElement().toggle();
    chartDefinitionViewPage.assertExists();
    chartDefinitionViewPage.checkPerimeter('project 1');
    chartDefinitionViewPage.checkAxisInfo("Versions d'exigence | Catégorie - Agrégation");
    chartDefinitionViewPage.assertMeasureInfoNotExists();
    chartDefinitionViewPage.assertSeriesInfoNotExists();
    chartDefinitionViewPage.assertFiltersInfoNotExists();
  });

  it('should display a simple chart with correct information', () => {
    const model = getSimpleChartDefinition(ChartType.BAR);
    const chartDefinitionViewPage = navigateToChartView(model);
    chartDefinitionViewPage.foldTree();
    new NavBarElement().toggle();
    chartDefinitionViewPage.assertExists();
    chartDefinitionViewPage.checkPerimeter('project 1');
    chartDefinitionViewPage.checkAxisInfo("Versions d'exigence | Catégorie - Agrégation");
    chartDefinitionViewPage.checkMeasureInfo(
      "Versions d'exigence | ID de version d'exigence - Comptage",
    );
    chartDefinitionViewPage.assertSeriesInfoNotExists();
    chartDefinitionViewPage.assertFiltersInfoNotExists();
  });

  it('should display a three axes chart with filters and correct information', () => {
    const model = getThreeAxisModel(ChartType.COMPARATIVE);
    model.filters = [
      {
        cufId: null,
        values: ['WORK_IN_PROGRESS'],
        operation: ChartOperation.IN,
        column: {
          id: 1,
          label: 'TEST_CASE_STATUS',
          columnType: ChartColumnType.ATTRIBUTE,
          dataType: ChartDataType.LEVEL_ENUM,
          specializedType: {
            entityType: EntityType.TEST_CASE,
          },
        },
        id: 96,
      },
    ];
    const chartDefinitionViewPage = navigateToChartView(model);
    chartDefinitionViewPage.foldTree();
    new NavBarElement().toggle();
    chartDefinitionViewPage.assertExists();
    chartDefinitionViewPage.checkPerimeter('project 1');
    chartDefinitionViewPage.checkAxisInfo("Versions d'exigence | Statut - Agrégation");
    chartDefinitionViewPage.checkMeasureInfo(
      "Versions d'exigence | ID de version d'exigence - Comptage",
    );
    chartDefinitionViewPage.checkSeriesInfo("Versions d'exigence | Criticité - Agrégation");
    chartDefinitionViewPage.checkFilterInfo(
      0,
      'Cas de test | Statut - Dans : En cours de rédaction',
    );
  });

  function navigateToChartView(model: Partial<ChartDefinitionModel>): ChartDefinitionViewPage {
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage({
      dataRows: getSimpleCustomReportLibraryChildNodes(),
    });
    return customReportWorkspacePage.tree.selectNode<ChartDefinitionViewPage>(
      'ChartDefinition-3',
      model,
    );
  }
});
