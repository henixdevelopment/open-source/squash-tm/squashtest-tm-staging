import { createBasicTreeWithOneSprint } from '../campaign-workspace-mock-data';
import { mockSprintModel } from '../../../../data-mock/sprint.data-mock';
import { SprintModel } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/sprint-model';
import { SprintViewPage } from '../../../../page-objects/pages/campaign-workspace/sprint/sprint-view.page';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { mockDataRow, mockGridResponse } from '../../../../data-mock/grid.data-mock';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { selectByDataTestCellId } from '../../../../utils/basic-selectors';
import { SprintReqVersionViewPage } from '../../../../page-objects/pages/campaign-workspace/sprint-req-version/sprint-req-version-view.page';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';

describe('Sprint overall execution plan', () => {
  it('Should display overall sprint execution plan', () => {
    const mockOverallExecPlanPanelResponse = navigateToSprintViewOverallExecPlanPage();

    const sprintModel = mockSprintModel({ nbTestPlanItems: 1 });
    const sprintViewPage = navigateToSprint(sprintModel);
    const sprintOverallExecPlan = sprintViewPage.clickSprintOverallExecPlanAnchorLink(
      mockOverallExecPlanPanelResponse,
    );
    sprintOverallExecPlan.assertExists();

    const grid = sprintOverallExecPlan.sprintOverallExecPlanGrid;
    grid.assertExists();
    grid.assertRowCount(1);
    sprintViewPage.anchors.assertAnchorCounterEquals('overall-execution-plan', 1);
    grid
      .getRow(1)
      .cell(GridColumnId.executionStatus)
      .findCell()
      .find(selectByDataTestCellId('execution-status-cell'))
      .should('have.css', 'background-color', 'rgb(0, 111, 87)');
    grid.getRow(1).cell(GridColumnId.datasetName).findCell().should('not.be.enabled');

    const sprintReqVersionViewPage = new SprintReqVersionViewPage(mockOverallExecPlanPanelResponse);
    const mock = new HttpMockBuilder(
      'sprint-req-version-view/*?frontEndErrorIsHandled=true',
    ).build();
    grid
      .getRow(1)
      .cell(GridColumnId.sprintReqVersionNameWithRef)
      .linkRenderer()
      .findCellLink()
      .click();
    mock.wait();
    sprintReqVersionViewPage.assertExists();
  });
});

function navigateToSprintViewOverallExecPlanPage(): GridResponse {
  return mockGridResponse('testPlanItemId', [
    mockDataRow({
      data: {
        successRate: 100.0,
        importance: 'LOW',
        executionStatus: 'SUCCESS',
        overviewId: null,
        datasetName: null,
        assigneeId: 2,
        latestExecutionId: 1,
        inferredExecutionMode: 'MANUAL',
        assigneeLogin: 'admin',
        assigneeFullName: 'admin (Squash administrator)',
        lastExecutedOn: '2024-09-13T12:56:45.371+00:00',
        datasetId: null,
        availableDatasets: [],
        testCaseReference: '',
        testCaseName: 'CT1',
        projectName: 'Project1',
        lastExecutionStatuses: [],
        testPlanItemId: 1,
        projectId: 1,
        testCaseId: 1,
        sprintReqVersionNameWithRef: '001 - Exi01',
      },
    }),
  ]);
}

function navigateToSprint(sprintModel: SprintModel): SprintViewPage {
  const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(
    createBasicTreeWithOneSprint(),
  );
  const sprintViewPage: SprintViewPage = campaignWorkspacePage.tree.selectNode(
    'Sprint-1',
    sprintModel,
  );
  sprintViewPage.assertExists();

  return sprintViewPage;
}
