import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { SprintViewPage } from '../../../../page-objects/pages/campaign-workspace/sprint/sprint-view.page';
import { createBasicTreeWithOneSprint } from '../campaign-workspace-mock-data';
import { SprintStatus } from '../../../../../../projects/sqtm-core/src/lib/model/level-enums/level-enum';
import { TreeElement } from '../../../../page-objects/elements/grid/grid.element';
import { mockSprintModelWithRemoteState } from '../../../../data-mock/sprint.data-mock';

const initialNodes: GridResponse = createBasicTreeWithOneSprint();
const sprintReqVersionsLinkedAnchorId: string = 'linked-requirements';
const frenchSprintStatus = {
  UPCOMING: 'À venir',
  OPEN: 'Ouvert',
  CLOSED: 'Fermé',
};
const hexadecimalColors = {
  UPCOMING: '#F4CF27',
  OPEN: '#02A7F0',
  CLOSED: '#a00fad',
};

describe('Sprint View Display', () => {
  it('should display sprint view', () => {
    const { sprintViewPage }: { sprintViewPage: SprintViewPage } = navigateToSprint();
    sprintViewPage.assertExists();
    sprintViewPage.anchors.assertAnchorCountNotExists(sprintReqVersionsLinkedAnchorId);
    sprintViewPage.nameTextField.checkContent('Sprint 1');
    sprintViewPage.referenceField.checkContent('REF01');
    sprintViewPage.checkStatusCapsuleExists();
    sprintViewPage.checkStartStopSprintButtonExists();
  });

  it('should check capsule content and tooltip', () => {
    const { sprintViewPage }: { sprintViewPage: SprintViewPage } = navigateToSprint();
    sprintViewPage.checkStatusCapsuleExists();
    sprintViewPage.checkStatusCapsuleContains(frenchSprintStatus.UPCOMING);
    sprintViewPage.checkStatusCapsuleIconExists();
    sprintViewPage.checkTooltipContains(sprintViewPage.statusCapsule, 'État du sprint');
    sprintViewPage.checkDifferentStatusMessageNotExists();
  });

  it('should display error message for different status of a synchronized sprint', () => {
    const { sprintViewPage }: { sprintViewPage: SprintViewPage } = navigateToSprint(
      mockSprintModelWithRemoteState(),
    );
    sprintViewPage.checkDifferentStatusMessageExists();
  });

  it('should update sprint status when click on start-stop button', () => {
    const {
      campaignWorkspacePage,
      sprintViewPage,
    }: { campaignWorkspacePage: CampaignWorkspacePage; sprintViewPage: SprintViewPage } =
      navigateToSprint();
    const tree: TreeElement = campaignWorkspacePage.tree;
    sprintViewPage.assertExists();
    sprintViewPage.checkStartStopSprintButtonExists();

    sprintViewPage.checkStartStopSprintButtonContains('Démarrer le sprint');
    sprintViewPage.checkStatusCapsuleIconColorIs(hexadecimalColors.UPCOMING);
    tree.assertSprintNodeLeftBorderHasCorrectBackgroundColor(
      'Sprint-1',
      hexadecimalColors.UPCOMING,
    );
    sprintViewPage.checkStatusIconColorIs(hexadecimalColors.UPCOMING);
    sprintViewPage.checkStatusCapsuleContains(frenchSprintStatus.UPCOMING);
    sprintViewPage.checkTooltipContains(
      sprintViewPage.treeNodeLeftBorder,
      frenchSprintStatus.UPCOMING,
    );

    sprintViewPage.clickStartStopButton(initialNodes, SprintStatus.OPEN.id);
    sprintViewPage.checkStartStopSprintButtonContains('Terminer le sprint');
    sprintViewPage.checkStatusCapsuleIconColorIs(hexadecimalColors.OPEN);
    tree.assertSprintNodeLeftBorderHasCorrectBackgroundColor('Sprint-1', hexadecimalColors.OPEN);
    sprintViewPage.checkStatusIconColorIs(hexadecimalColors.OPEN);
    sprintViewPage.checkStatusCapsuleContains(frenchSprintStatus.OPEN);
    sprintViewPage.checkTooltipContains(sprintViewPage.treeNodeLeftBorder, frenchSprintStatus.OPEN);

    sprintViewPage.clickStartStopButton(initialNodes, SprintStatus.CLOSED.id);
    sprintViewPage.checkStartStopSprintButtonContains('Rouvrir le sprint');
    sprintViewPage.checkStatusCapsuleIconColorIs(hexadecimalColors.CLOSED);
    tree.assertSprintNodeLeftBorderHasCorrectBackgroundColor('Sprint-1', hexadecimalColors.CLOSED);
    sprintViewPage.checkStatusIconColorIs(hexadecimalColors.CLOSED);
    sprintViewPage.checkStatusCapsuleContains(frenchSprintStatus.CLOSED);
    sprintViewPage.checkTooltipContains(
      sprintViewPage.treeNodeLeftBorder,
      frenchSprintStatus.CLOSED,
    );
  });

  type WorkspaceAndView = {
    campaignWorkspacePage: CampaignWorkspacePage;
    sprintViewPage: SprintViewPage;
  };

  function navigateToSprint(model?: any): WorkspaceAndView {
    const campaignWorkspacePage: CampaignWorkspacePage =
      CampaignWorkspacePage.initTestAtPage(initialNodes);
    const tree: TreeElement = campaignWorkspacePage.tree;
    tree.assertNodeExist('CampaignLibrary-1');
    tree.assertNodeTextContains('CampaignLibrary-1', 'Project1');
    tree.assertNodeExist('Sprint-1');
    tree.assertNodeTextContains('Sprint-1', 'Sprint 1');

    const sprintViewPage: SprintViewPage = tree.selectNode('Sprint-1', model);
    sprintViewPage.assertExists();

    return { sprintViewPage, campaignWorkspacePage };
  }
});
