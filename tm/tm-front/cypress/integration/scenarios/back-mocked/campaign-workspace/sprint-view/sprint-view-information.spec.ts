import { SprintViewPage } from '../../../../page-objects/pages/campaign-workspace/sprint/sprint-view.page';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { createBasicTreeWithOneSprint } from '../campaign-workspace-mock-data';
import { mockSprintModelWithRemoteState } from '../../../../data-mock/sprint.data-mock';

const initialNodes: GridResponse = createBasicTreeWithOneSprint();
const sprintReqVersionsLinkedAnchorId: string = 'linked-requirements';

describe('Sprint - Information', () => {
  it('should display manual sprint information', () => {
    const sprintViewPage = navigateToSprint();
    const infoPanel = sprintViewPage.informationPanel;
    sprintViewPage.clickInformationAnchorLink();
    sprintViewPage.checkStatusIconExists();
    sprintViewPage.checkData('sprint-squash-status', 'À venir');
    sprintViewPage.checkData('sprint-startDate', '04/03/2024');
    sprintViewPage.checkData('sprint-endDate', '15/03/2024');
    sprintViewPage.checkData('sprint-created', '14/02/2024 15:00 (squashUser)');
    sprintViewPage.checkData('sprint-lastModified', '15/02/2024 15:00 (squashAdmin)');
    infoPanel.descriptionRichField.checkTextContent('This is my description');
    sprintViewPage.anchors.assertAnchorCountNotExists(sprintReqVersionsLinkedAnchorId);
  });

  it('should display sprint with remote state information', () => {
    const sprintViewPage = navigateToSprint(mockSprintModelWithRemoteState());
    sprintViewPage.checkData('sprint-remote-state', 'Ouvert');
  });

  function navigateToSprint(model?: any): SprintViewPage {
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes);

    const tree = campaignWorkspacePage.tree;
    tree.assertNodeExist('CampaignLibrary-1');
    tree.assertNodeTextContains('CampaignLibrary-1', 'Project1');
    tree.assertNodeExist('Sprint-1');
    tree.assertNodeTextContains('Sprint-1', 'Sprint 1');

    const sprintViewPage: SprintViewPage = tree.selectNode('Sprint-1', model);
    sprintViewPage.assertExists();

    return sprintViewPage;
  }
});
