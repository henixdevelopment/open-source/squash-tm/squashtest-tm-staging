import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { createBasicTreeWithOneSprint } from '../campaign-workspace-mock-data';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { GridElement, TreeElement } from '../../../../page-objects/elements/grid/grid.element';
import { SprintViewPage } from '../../../../page-objects/pages/campaign-workspace/sprint/sprint-view.page';
import { mockSprintModel } from '../../../../data-mock/sprint.data-mock';
import { SprintViewIssuesPage } from '../../../../page-objects/pages/campaign-workspace/sprint/sprint-view-issues.page';
import { mockDataRow, mockGridResponse } from '../../../../data-mock/grid.data-mock';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { Identifier } from '../../../../../../projects/sqtm-core/src/lib/model/entity.model';
import { DataRowModel } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { ReferentialDataMockBuilder } from '../../../../utils/referential/referential-data-builder';
import { SprintModel } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/sprint-model';
import { ExportSprintIssuesDialogElement } from '../../../../page-objects/pages/campaign-workspace/dialogs/export-sprint-issues-dialog.element';

const sprintIssuesAnchorId: string = 'issues';

describe('Sprint Issues', () => {
  it('should not display the sprint issues if no bugtracker is bound to the project', () => {
    const sprintViewPage: SprintViewPage = navigateToSprintView(refDataWithoutBugTracker);
    sprintViewPage.assertExists();
    sprintViewPage.anchors.assertAnchorCountNotExists(sprintIssuesAnchorId);
  });

  it('should display all issues reported during a sprint', () => {
    const [sprintViewPage, sprintViewIssuesPage]: [SprintViewPage, SprintViewIssuesPage] =
      navigateToSprintViewIssuesPage(refDataWithBugTracker);
    const sprintIssuesGrid: GridElement = sprintViewIssuesPage.sprintIssuesGrid;
    sprintIssuesGrid.assertRowCount(2);
    sprintViewPage.anchors.assertAnchorCounterEquals(sprintIssuesAnchorId, 2);
  });

  it('should export issues into a csv file', () => {
    const [, sprintViewIssuesPage]: [SprintViewPage, SprintViewIssuesPage] =
      navigateToSprintViewIssuesPage(refDataWithBugTracker);
    const exportDialog: ExportSprintIssuesDialogElement = sprintViewIssuesPage.openExportDialog();
    exportDialog.assertExists();
  });
});

function navigateToSprintViewIssuesPage(
  referentialDataModel: ReferentialDataModel,
): [SprintViewPage, SprintViewIssuesPage] {
  const sprintViewPage: SprintViewPage = navigateToSprintView(referentialDataModel);
  const authenticatedIssuePanelResponse = getAuthenticatedIssuePanelResponse();
  const sprintIssuesPanel: SprintViewIssuesPage = sprintViewPage.clickSprintIssuesAnchorLink(
    authenticatedIssuePanelResponse,
    issuesGridResponse,
  );
  sprintIssuesPanel.assertExists();
  const sprintIssuesGrid: GridElement = sprintIssuesPanel.sprintIssuesGrid;
  sprintIssuesGrid.assertExists();
  sprintViewPage.anchors.assertAnchorCountExists(sprintIssuesAnchorId);
  return [sprintViewPage, sprintIssuesPanel];
}

function navigateToSprintView(referentialDataModel: ReferentialDataModel): SprintViewPage {
  const initialNodes = createBasicTreeWithOneSprint();
  const sprintModel: SprintModel = mockSprintModel({
    nbIssues: 2,
  });
  const campaignWorkspacePage: CampaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(
    initialNodes,
    referentialDataModel,
  );

  const tree: TreeElement = campaignWorkspacePage.tree;
  tree.assertNodeExist('CampaignLibrary-1');
  tree.assertNodeTextContains('CampaignLibrary-1', 'Project1');
  tree.assertNodeExist('Sprint-1');
  tree.assertNodeTextContains('Sprint-1', 'Sprint 1');

  const sprintViewPage: SprintViewPage = tree.selectNode('Sprint-1', sprintModel);
  sprintViewPage.assertExists();
  return sprintViewPage;
}

const issuesGridResponse: GridResponse = mockGridResponse(GridColumnId.remoteId, [
  mockIssueDataRow(1, 1, 1, 'HIGH'),
  mockIssueDataRow(2, 2, 2, 'LOW'),
]);

function mockIssueDataRow(
  issueId: Identifier,
  remoteId: Identifier,
  stepOrder: number,
  priority: string,
): DataRowModel {
  return mockDataRow({
    id: issueId.toString(),
    projectId: 1,
    data: {
      summary: 'summary',
      issueIds: [issueId],
      btProject: 'P1',
      executionSteps: [stepOrder],
      priority: priority,
      url: 'http://192.168.1.50/mantis/view.php?id=' + remoteId,
      remoteId: remoteId.toString(),
      assignee: 'admin',
      status: 'assigned',
    },
    allowMoves: true,
  });
}

const refDataWithoutBugTracker: ReferentialDataModel = new ReferentialDataMockBuilder()
  .withProjects({
    name: 'Project1',
  })
  .withUser({
    username: 'admin',
    userId: 1,
    admin: true,
  })
  .build();

const refDataWithBugTracker: ReferentialDataModel = new ReferentialDataMockBuilder()
  .withUser({
    username: 'admin',
    userId: 1,
    admin: true,
  })
  .withProjects({
    name: 'Project1',
    bugTrackerBinding: { bugTrackerId: 1, projectId: 1 },
  })
  .withBugTrackers({
    name: 'bt',
  })
  .build();

function getAuthenticatedIssuePanelResponse(): any {
  return {
    entityType: 'SPRINT_TYPE',
    bugTrackerStatus: 'AUTHENTICATED',
    projectName: '["P1"]',
    projectId: 1,
    delete: '',
    oslc: false,
  };
}
