import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { BindReqVersionToSprintOperationReport } from '../../../../../../projects/sqtm-core/src/lib/model/change-coverage-operation-report';
import {
  mockSprintModel,
  mockSprintModelWithRemoteState,
} from '../../../../data-mock/sprint.data-mock';
import { SprintViewPage } from '../../../../page-objects/pages/campaign-workspace/sprint/sprint-view.page';
import { mockRequirementTreeResponse } from '../../requirement-workspace/requirement-workspace-mock-data';
import { createBasicTreeWithOneSprint } from '../campaign-workspace-mock-data';
import { mockSprintReqVersionModel } from '../../../../data-mock/sprint-req-version.data-mock';
import { SprintReqVersionModel } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/sprint-req-version-model';
import { SprintViewReqVersionsLinkedPage } from '../../../../page-objects/pages/campaign-workspace/sprint/sprint-view-req-versions-linked.page';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import {
  ManagementMode,
  RemotePerimeterStatus,
  SprintModel,
} from '../../../../../../projects/sqtm-core/src/lib/model/campaign/sprint-model';
import { SprintStatus } from '../../../../../../projects/sqtm-core/src/lib/model/level-enums/level-enum';
import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { ReferentialDataMockBuilder } from '../../../../utils/referential/referential-data-builder';
import { Permissions } from '../../../../../../projects/sqtm-core/src/lib/model/permissions/permissions.model';
import { selectByDataTestElementId } from '../../../../utils/basic-selectors';

let sprintRequirementPanel: SprintViewReqVersionsLinkedPage;
let requirementTreeResponse: GridResponse;
let sprintViewPage: SprintViewPage;
let initialNodes: GridResponse;
let sprintModel: SprintModel;
let sprintRequirementGrid: GridElement;

const sprintReqVersionsLinkedAnchorId: string = 'linked-requirements';

function mockFourSprintReqVersions() {
  return [
    mockSprintReqVersionModel({
      id: 1,
      name: 'name1',
      validationStatus: 'TO_BE_TESTED',
    }),
    mockSprintReqVersionModel({
      id: 2,
      name: 'name2',
      validationStatus: 'TO_BE_CORRECTED',
    }),
    mockSprintReqVersionModel({
      id: 3,
      name: 'name3',
      validationStatus: 'IN_PROGRESS',
    }),
    mockSprintReqVersionModel({
      id: 4,
      name: 'name4',
      validationStatus: 'VALIDATED',
    }),
  ];
}

describe('Sprint Requirement', () => {
  afterEach(function () {
    sprintRequirementPanel = null;
    requirementTreeResponse = null;
    sprintViewPage = null;
    initialNodes = null;
    sprintModel = null;
  });

  it('should display sprint requirement panel and add requirements', () => {
    navigateToSprintViewReqVersionsLinkedPage();

    sprintRequirementGrid.assertRowCount(1);
    sprintViewPage.anchors.assertAnchorCounterEquals(sprintReqVersionsLinkedAnchorId, 1);

    const requirementDrawer = sprintViewPage.openRequirementDrawer(requirementTreeResponse);
    requirementDrawer.beginDragAndDrop('Requirement-347232');

    const sprintReqVersionModel = mockSprintReqVersionModel({
      id: 347232,
      requirementId: 347232,
      projectId: 327,
      criticality: 'MINOR',
      name: '1.03 - Exigence 3',
      reference: '1.03',
      status: 'WORK_IN_PROGRESS',
    });
    const requirementOperationReport: BindReqVersionToSprintOperationReport = {
      linkedReqVersions: [sprintReqVersionModel],
      summary: {
        reqVersionsAlreadyLinkedToSprint: [],
        highLevelReqVersionsInSelection: [],
        obsoleteReqVersionsInSelection: [],
      },
    };
    sprintViewPage.dropRequirementIntoSprint(sprintModel.id, requirementOperationReport);
    sprintViewPage.closeDrawer();
    sprintViewPage.sprintReqVersionsTable.assertRowCount(2);
    sprintViewPage.anchors.assertAnchorCounterEquals(sprintReqVersionsLinkedAnchorId, 2);
  });

  it('should show correct information in a row of a sprintReqVersion', () => {
    navigateToSprintViewReqVersionsLinkedPage();
    sprintRequirementGrid.assertRowCount(1);
    sprintViewPage.anchors.assertAnchorCounterEquals(sprintReqVersionsLinkedAnchorId, 1);

    const row = sprintRequirementGrid.getRow(11);
    row
      .cell(GridColumnId.requirementVersionProjectName)
      .linkRenderer()
      .assertContainText('Project1');
    row.cell(GridColumnId.reference).textRenderer().assertContainsText('REF1');
    row.cell(GridColumnId.name).linkRenderer().assertContainText('Requirement 01');
    row.cell(GridColumnId.categoryLabel).textRenderer().assertContainsText('Fonctionnelle');
    row
      .cell(GridColumnId.criticality)
      .iconRenderer()
      .assertContainsIcon('anticon-sqtm-core-requirement:double_up');
    row
      .cell(GridColumnId.status)
      .iconRenderer()
      .assertContainsIcon('anticon-sqtm-core-requirement:status');
    row.cell(GridColumnId.nbTests).textRenderer().assertContainsText('3');
    sprintRequirementGrid
      .getCell(11, GridColumnId.testSprintReqVersion, 'rightViewport')
      .buttonRenderer()
      .assertContainsText('Tester');
    sprintRequirementGrid
      .getCell(11, GridColumnId.delete, 'rightViewport')
      .iconRenderer()
      .assertContainsIcon('anticon-sqtm-core-generic:unlink');
    row
      .cell(GridColumnId.validationStatus)
      .findCell()
      .find(selectByDataTestElementId('validation-status'))
      .should('have.css', 'background-color', 'rgb(163, 178, 184)');
  });

  it('should filter validation status column', () => {
    navigateToSprintViewReqVersionsLinkedPage(mockFourSprintReqVersions());
    sprintViewPage.foldTree();

    sprintRequirementGrid.scrollPosition('right', 'mainViewport');
    filterValidationStatuses(['Test en cours']);
    sprintRequirementGrid.assertRowCount(1);

    filterValidationStatuses(['À corriger', 'Validé']);
    sprintRequirementGrid.assertRowCount(3);
  });

  function filterValidationStatuses(statusToFilter: string[]) {
    const filterWidget = sprintRequirementGrid
      .getHeaderRow()
      .header(GridColumnId.validationStatus)
      .openMultiSelectFilter();
    filterWidget.assertExists();
    filterWidget.assertItemCount(4);
    statusToFilter.forEach((status) => filterWidget.toggleOneItem(status));
  }

  it('should sort validation status column', () => {
    navigateToSprintViewReqVersionsLinkedPage(mockFourSprintReqVersions());

    sprintRequirementGrid.clickOnSortingGridHeaderNoMock(GridColumnId.validationStatus);
    sprintRequirementGrid.assertGridContentOrder(GridColumnId.name, [
      'name1',
      'name3',
      'name4',
      'name2',
    ]);
  });

  it('should show delete or unlink icon depending on whether there are executions bound to the verifying test cases of the sprintReqVersion', () => {
    const sprintReqVersionWithExecutions = mockSprintReqVersionModel({
      id: 16813,
      name: 'SprintReqVersion with executions',
      nbExecutions: 2,
    });
    const sprintReqVersionWithoutExecutions = mockSprintReqVersionModel({
      id: 16814,
      name: 'SprintReqVersion without executions',
    });

    navigateToSprintViewReqVersionsLinkedPage([
      sprintReqVersionWithExecutions,
      sprintReqVersionWithoutExecutions,
    ]);
    sprintRequirementGrid.assertRowCount(2);
    sprintViewPage.anchors.assertAnchorCounterEquals(sprintReqVersionsLinkedAnchorId, 2);

    sprintRequirementGrid
      .getCell(16813, GridColumnId.delete, 'rightViewport')
      .iconRenderer()
      .assertContainsIcon('anticon-sqtm-core-generic:delete');
    sprintRequirementGrid
      .getCell(16814, GridColumnId.delete, 'rightViewport')
      .iconRenderer()
      .assertContainsIcon('anticon-sqtm-core-generic:unlink');
  });

  it('should show no information and display (deleted) if a requirement has been deleted', () => {
    const partialSprintReqVersionModel: Partial<SprintReqVersionModel> = {
      name: null,
      reference: null,
      categoryId: null,
      criticality: null,
      status: null,
      versionId: null,
      requirementId: null,
      requirementVersionProjectName: null,
    };
    navigateToSprintViewReqVersionsLinkedPage([
      mockSprintReqVersionModel(partialSprintReqVersionModel),
    ]);
    sprintRequirementGrid.assertExists();
    sprintRequirementGrid.getCell(11, 'name').findCellTextSpan().contains('(supprimée)');
  });

  it('should display remote perimeter status icon with correct color on synchronized requirements', () => {
    navigateToSprintViewReqVersionsLinkedPage(getSynchronizedSprintReqVersionDataRows());

    const sprintRequirementGrid = sprintRequirementPanel.sprintReqVersionsGrid;
    sprintRequirementGrid.assertExists();
    sprintRequirementGrid.assertRowCount(4);
    assertPerimeterStatusCellHasCssClass(sprintRequirementGrid, 1, 'remote-in-perimeter');
    assertPerimeterStatusCellHasCssClass(sprintRequirementGrid, 2, 'remote-out-of-perimeter');
    assertPerimeterStatusCellHasCssClass(sprintRequirementGrid, 3, 'remote-deleted');
  });

  it('should not display search and add buttons if a sprint is closed', () => {
    const partialSprintReqVersionModel: Partial<SprintReqVersionModel> = {
      name: null,
      reference: null,
      categoryId: null,
      criticality: null,
      status: null,
      versionId: null,
      requirementId: null,
      requirementVersionProjectName: null,
    };
    navigateToSprintViewReqVersionsLinkedPage([
      mockSprintReqVersionModel(partialSprintReqVersionModel),
    ]);
    sprintViewPage.assertExists();
    sprintViewPage.checkAddButtonExists();
    sprintViewPage.checkSearchButtonExists();
    sprintViewPage.checkDeleteSprintReqVersionsButtonExists();
    sprintRequirementGrid
      .getCell(11, GridColumnId.delete, 'rightViewport')
      .iconRenderer()
      .assertContainsIcon('anticon-sqtm-core-generic:unlink');
    sprintViewPage.checkStartStopSprintButtonExists();
    sprintViewPage.checkStartStopSprintButtonContains('Démarrer le sprint');
    sprintViewPage.clickStartStopButton(initialNodes, SprintStatus.OPEN.id);
    sprintViewPage.checkStartStopSprintButtonContains('Terminer le sprint');
    sprintViewPage.clickStartStopButton(initialNodes, SprintStatus.CLOSED.id);
    sprintViewPage.checkAddButtonNotExists();
    sprintViewPage.checkSearchButtonNotExists();
    sprintViewPage.checkDeleteSprintReqVersionsButtonNotExists();
    sprintRequirementGrid.getCell(11, GridColumnId.delete, 'rightViewport').isNotVisible();
  });
});

it('should delete sprintReqVersions with and without executions from a sprint', () => {
  const sprintReqVersion1 = mockSprintReqVersionModel();
  const sprintReqVersion2 = mockSprintReqVersionModel({
    id: 16813,
    name: 'SprintReqVersion without executions',
    nbExecutions: 0,
  });
  const sprintReqVersion3 = mockSprintReqVersionModel({
    id: 16814,
    name: 'sprintReqVersion with executions',
    nbExecutions: 2,
  });

  navigateToSprintViewReqVersionsLinkedPage([
    sprintReqVersion1,
    sprintReqVersion2,
    sprintReqVersion3,
  ]);
  sprintRequirementGrid.assertRowCount(3);
  sprintRequirementGrid.selectRow(16813, '#', 'leftViewport');
  const firstCoveragesDialogElement = sprintViewPage.showDeleteConfirmationDialog(sprintModel.id, [
    16813,
  ]);
  firstCoveragesDialogElement.assertExists();
  firstCoveragesDialogElement.deleteForSuccess({
    linkedReqVersions: [sprintReqVersion1, sprintReqVersion3],
    summary: null,
  });
  sprintRequirementGrid.selectRow(16814, '#', 'leftViewport');
  const secondCoveragesDialogElement = sprintViewPage.showDeleteConfirmationDialog(sprintModel.id, [
    16814,
  ]);
  secondCoveragesDialogElement.assertExists();
  secondCoveragesDialogElement.deleteForSuccess({
    linkedReqVersions: [sprintReqVersion1],
    summary: null,
  });
  sprintRequirementGrid.assertRowCount(1);
});

it('should allow deletion of a sprintReqVersion to test referees only if no execution is bound to the verifying test cases', () => {
  const referentialDataMock = new ReferentialDataMockBuilder()
    .withProjects({
      name: 'Project 1',
      label: 'Etiquette',
      permissions: {
        TEST_CASE_LIBRARY: [],
        CAMPAIGN_LIBRARY: [Permissions.LINK],
        REQUIREMENT_LIBRARY: [],
        PROJECT: [],
        PROJECT_TEMPLATE: [],
        CUSTOM_REPORT_LIBRARY: [],
        AUTOMATION_REQUEST_LIBRARY: [],
        ACTION_WORD_LIBRARY: [],
      },
    })
    .build();
  const sprintReqVersionWithoutExec = mockSprintReqVersionModel({
    id: 16813,
    name: 'SprintReqVersion without executions',
    nbExecutions: 0,
  });
  const sprintReqVersionWithExec = mockSprintReqVersionModel({
    id: 16814,
    name: 'sprintReqVersion with executions',
    nbExecutions: 2,
  });

  navigateToSprintViewReqVersionsLinkedPage(
    [sprintReqVersionWithoutExec, sprintReqVersionWithExec],
    null,
    referentialDataMock,
  );
  sprintRequirementGrid.assertRowCount(2);
  sprintRequirementGrid.selectRow(16813, '#', 'leftViewport');
  const firstCoveragesDialogElement = sprintViewPage.showDeleteConfirmationDialog(sprintModel.id, [
    16813,
  ]);
  firstCoveragesDialogElement.assertExists();
  firstCoveragesDialogElement.deleteForSuccess({
    linkedReqVersions: [sprintReqVersionWithExec],
    summary: null,
  });
  sprintRequirementGrid
    .findRowId(GridColumnId.name, 'sprintReqVersion with executions')
    .then((idItem) => {
      const iconRenderer = sprintRequirementGrid
        .getCell(idItem, 'delete', 'rightViewport')
        .iconRenderer();
      iconRenderer.assertNotContainsIcon('anticon-sqtm-core-generic:delete');
      iconRenderer.assertNotContainsIcon('anticon-sqtm-core-generic:unlink');
    });
});

it('should show error popup when trying to delete sprintReqVersion from a closed sprint', () => {
  const sprintReqVersion1 = mockSprintReqVersionModel();
  const sprintReqVersion2 = mockSprintReqVersionModel({
    id: 16813,
    name: 'Second sprintReqVersion',
  });

  navigateToSprintViewReqVersionsLinkedPage([sprintReqVersion1, sprintReqVersion2]);
  sprintRequirementGrid.assertRowCount(2);
  sprintRequirementGrid.selectRow(16813, '#', 'leftViewport');
  const coveragesDialogElement = sprintViewPage.showDeleteConfirmationDialog(sprintModel.id, [
    16813,
  ]);
  coveragesDialogElement.assertExists();
  const response = {
    squashTMError: {
      kind: 'ACTION_ERROR',
      actionValidationError: {
        exception: 'SprintNotLinkableException',
        i18nKey: 'sqtm-core.error.sprint.sprint-closed-rejection',
      },
    },
  };
  const alertDialog = coveragesDialogElement.deleteWithClosedSprint(response);
  alertDialog.assertExists();
  alertDialog.assertExclamationCircleIconExists();
  alertDialog.assertHasMessage('Action impossible car le sprint est fermé.');
});

it('should show a special grid for synchronized sprint', () => {
  const synchronizedSprintModel: SprintModel = mockSprintModelWithRemoteState();
  navigateToSprintViewReqVersionsLinkedPage(
    getSynchronizedSprintReqVersionDataRows(),
    synchronizedSprintModel,
  );
  sprintViewPage.checkTitle('Tickets du sprint');
  sprintRequirementGrid.getHeaderRow().findCellId(GridColumnId.reference, 'Clé');
  sprintRequirementGrid
    .getHeaderRow()
    .cell(GridColumnId.requirementVersionProjectName)
    .assertNotExist();
  const row = sprintRequirementGrid.getRow(1);
  row
    .cell(GridColumnId.reference)
    .linkRenderer()
    .findCellLink()
    .should('contain', 'toto#9')
    .should('have.attr', 'href')
    .and('equal', 'https://gitlab.com/toto/-/issues/9');
});

it('should show the ordinary grid if it is a manual sprint', () => {
  navigateToSprintViewReqVersionsLinkedPage();
  sprintViewPage.checkTitle('Exigences du sprint');
  sprintRequirementGrid.getHeaderRow().findCellId(GridColumnId.reference, 'Référence');
  sprintRequirementGrid
    .getHeaderRow()
    .findCellId(GridColumnId.requirementVersionProjectName, 'Projet');
  sprintViewPage.checkAddButtonExists();
  sprintViewPage.checkSearchButtonExists();
});

function navigateToSprintViewReqVersionsLinkedPage(
  sprintReqVersions?: SprintReqVersionModel[],
  sprintModelProvided?: SprintModel,
  referentialDataMock?: ReferentialDataModel,
) {
  const sprintReqVersionModel: SprintReqVersionModel = mockSprintReqVersionModel();
  initialNodes = createBasicTreeWithOneSprint();
  sprintModel = mockSprintModel({
    ...sprintModelProvided,
    sprintReqVersions: sprintReqVersions || [sprintReqVersionModel],
    nbSprintReqVersions: sprintReqVersions?.length || 1,
  });
  requirementTreeResponse = mockRequirementTreeResponse();
  sprintViewPage = navigateToSprint(sprintModel, referentialDataMock);
  sprintRequirementPanel = sprintViewPage.clickSprintReqVersionsAnchorLink();
  sprintRequirementPanel.assertExists();
  sprintRequirementGrid = sprintRequirementPanel.sprintReqVersionsGrid;
  sprintRequirementGrid.assertExists();
  sprintViewPage.anchors.assertAnchorCountExists(sprintReqVersionsLinkedAnchorId);
}

function navigateToSprint(
  sprintModel: SprintModel,
  referentialDataMock?: ReferentialDataModel,
): SprintViewPage {
  const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(
    initialNodes,
    referentialDataMock,
  );

  const tree = campaignWorkspacePage.tree;
  tree.assertNodeExist('CampaignLibrary-1');
  tree.assertNodeTextContains('CampaignLibrary-1', 'Project1');
  tree.assertNodeExist('Sprint-1');
  tree.assertNodeTextContains('Sprint-1', 'Sprint 1');

  const sprintViewPage: SprintViewPage = tree.selectNode('Sprint-1', sprintModel);
  sprintViewPage.assertExists();
  return sprintViewPage;
}

function assertPerimeterStatusCellHasCssClass(grid: GridElement, rowId: number, cssClass: string) {
  const row = grid.getRow(rowId);
  row.cell('remotePerimeterStatus').iconRenderer().assertContainsIcon(cssClass);
}

function getSynchronizedSprintReqVersionDataRows() {
  return [
    mockSprintReqVersionModel({
      id: 1,
      versionId: null,
      projectId: 1,
      requirementId: null,
      categoryId: null,
      name: 'Sprint Req 1',
      reference: 'SR1',
      remotePerimeterStatus: RemotePerimeterStatus.IN_CURRENT_PERIMETER,
      mode: ManagementMode.SYNCHRONIZED,
      status: 'WORK_IN_PROGRESS',
      criticality: 'MINOR',
      remoteReqUrl: 'https://gitlab.com/toto/-/issues/9',
    }),
    mockSprintReqVersionModel({
      id: 2,
      versionId: null,
      projectId: 1,
      requirementId: null,
      categoryId: null,
      name: 'Sprint Req 2',
      reference: 'SR2',
      remotePerimeterStatus: RemotePerimeterStatus.OUT_OF_CURRENT_PERIMETER,
      mode: ManagementMode.SYNCHRONIZED,
      status: 'WORK_IN_PROGRESS',
      criticality: 'MINOR',
    }),
    mockSprintReqVersionModel({
      id: 3,
      versionId: null,
      projectId: 1,
      requirementId: null,
      categoryId: null,
      name: 'Sprint Req 3',
      reference: 'SR3',
      remotePerimeterStatus: RemotePerimeterStatus.NOT_FOUND,
      mode: ManagementMode.SYNCHRONIZED,
      status: 'WORK_IN_PROGRESS',
      criticality: 'MINOR',
    }),
    mockSprintReqVersionModel({
      id: 4,
      versionId: null,
      projectId: 1,
      requirementId: null,
      categoryId: null,
      name: 'Sprint Req 4',
      reference: 'SR4',
      remotePerimeterStatus: RemotePerimeterStatus.UNKNOWN,
      mode: ManagementMode.SYNCHRONIZED,
      status: 'WORK_IN_PROGRESS',
      criticality: 'MINOR',
    }),
  ];
}
