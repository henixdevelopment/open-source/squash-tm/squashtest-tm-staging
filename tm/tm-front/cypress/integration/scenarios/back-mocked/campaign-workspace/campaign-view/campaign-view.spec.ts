import { createEntityReferentialData } from '../../../../utils/referential/create-entity-referential.const';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { CampaignViewPage } from '../../../../page-objects/pages/campaign-workspace/campaign/campaign-view.page';
import { CampaignViewDashboardPage } from '../../../../page-objects/pages/campaign-workspace/campaign/campaign-view-dashboard.page';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import {
  getDefaultCampaignStatisticsBundle,
  getEmptyCampaignStatisticsBundle,
  mockCampaignModel,
} from '../../../../data-mock/campaign.data-mock';
import { addDays } from 'date-fns';

import {
  CampaignModel,
  StatisticsBundle,
} from '../../../../../../projects/sqtm-core/src/lib/model/campaign/campaign-model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { mockTreeNode } from '../../../../data-mock/grid.data-mock';

const initialNodes: GridResponse = {
  count: 3,
  dataRows: [
    mockTreeNode({
      id: 'CampaignLibrary-1',
      children: ['CampaignFolder-2'],
      projectId: 1,
      state: DataRowOpenState.open,
      data: { NAME: 'Project1', CHILD_COUNT: 1 },
    }),
    mockTreeNode({
      id: 'CampaignFolder-2',
      children: ['Campaign-3'],
      parentRowId: 'CampaignLibrary-1',
      projectId: 1,
      state: DataRowOpenState.open,
      data: { NAME: 'Folder 1', CHILD_COUNT: 1 },
    }),
    mockTreeNode({
      id: 'Campaign-3',
      children: [],
      projectId: 1,
      parentRowId: 'CampaignFolder-2',
      state: DataRowOpenState.open,
      data: { NAME: 'Campaign 3', CHILD_COUNT: 0 },
    }),
  ],
};

const campaignModel: CampaignModel = mockCampaignModel({
  projectId: 1,
  id: 3,
  customFieldValues: [],
  attachmentList: {
    id: 1,
    attachments: [],
  },
  reference: 'REF01',
  description: 'A real description.',
  name: 'Campaign 03',
  campaignStatus: 'UNDEFINED',
  progressStatus: 'READY',
  lastModifiedOn: new Date().toISOString(),
  lastModifiedBy: 'toto',
  createdOn: new Date().toISOString(),
  createdBy: 'admin',
  milestones: [],
  testPlanStatistics: {
    nbTestCases: 5,
    progression: 60,
    status: 'READY',
    nbDone: 3,
    nbReady: 1,
    nbRunning: 1,
    nbSuccess: 0,
    nbSettled: 0,
    nbFailure: 1,
    nbBlocked: 1,
    nbUntestable: 1,
  },
  nbIssues: 0,
  hasDatasets: false,
  users: [],
  testSuites: [],
});

describe('Campaign View Display', () => {
  it('should display a campaign', () => {
    const campaignViewPage: CampaignViewPage = initAtCampaignViewPage();
    campaignViewPage.assertExists();
    campaignViewPage.nameTextField.checkContent('Campaign 03');
    campaignViewPage.assertReferenceContains('REF01');
  });

  it('should display and change information of campaign', () => {
    const campaignViewPage: CampaignViewPage = initAtCampaignViewPage();
    const informationPanel = campaignViewPage.clickInformationAnchorLink();
    informationPanel.assertExists();
  });

  it('should display statistics panel', () => {
    const campaignViewPage: CampaignViewPage = initAtCampaignViewPage();
    const statisticsPanel = campaignViewPage.clickStatisticsAnchorLink();
    statisticsPanel.assertExists();
    campaignViewPage.checkData('campaign-progression', '60');
  });

  it('should display dashboard page', () => {
    const campaignViewPage: CampaignViewPage = initAtCampaignViewPage();
    const dashboardPage: CampaignViewDashboardPage = campaignViewPage.clickDashboardAnchorLink();
    dashboardPage.assertExists();
    dashboardPage.assertAdvancementChartIsRendered();
    dashboardPage.assertStatsChartAreRendered();
  });

  it('should display empty chart warning', () => {
    const campaignViewPage: CampaignViewPage = initAtCampaignViewPage(
      getEmptyCampaignStatisticsBundle(),
    );
    const dashboardPage: CampaignViewDashboardPage = campaignViewPage.clickDashboardAnchorLink();
    dashboardPage.assertExists();
    dashboardPage.assertErrorMessageOnDateIsVisible();
    dashboardPage.assertDateButtonIsVisible();
    dashboardPage.assertErrorMessageOnEmptyTestPlanIsVisible();
  });

  it('should display inventory table', () => {
    const campaignViewPage: CampaignViewPage = initAtCampaignViewPage();
    const dashboardPage: CampaignViewDashboardPage = campaignViewPage.clickDashboardAnchorLink();
    dashboardPage.assertExists();
    dashboardPage.assertInventoryTableExist();
    dashboardPage.assertIterationRowHasName(0, 'It 1');
    dashboardPage.checkStatusCount(
      0,
      getDefaultCampaignStatisticsBundle().testInventoryStatistics[0].statistics,
    );
    dashboardPage.checkTotalCount(0, 23);
    dashboardPage.checkStatusCount(
      1,
      getDefaultCampaignStatisticsBundle().testInventoryStatistics[1].statistics,
    );
    dashboardPage.checkTotalCount(1, 15);
    dashboardPage.checkExecutionRate(1, 73); // 11 executed on 15 total
    dashboardPage.checkStatusCount(2, {
      READY: 10,
      FAILURE: 4,
    });
    dashboardPage.checkTotalCount(2, 38);
    dashboardPage.checkExecutionRate(2, 57); // 22 executed on 38 rounded down
  });

  it('should allow iteration scheduled dates edition and refresh stats', () => {
    const defaultCampaignStatisticsBundle = getDefaultCampaignStatisticsBundle();
    const campaignStatisticsBundle: StatisticsBundle = {
      ...defaultCampaignStatisticsBundle,
      progressionStatistics: {
        ...defaultCampaignStatisticsBundle.progressionStatistics,
        errors: ['some.error'],
      },
    };
    const campaignViewPage: CampaignViewPage = initAtCampaignViewPage(campaignStatisticsBundle);
    const dashboardPage: CampaignViewDashboardPage = campaignViewPage.clickDashboardAnchorLink();
    dashboardPage.assertExists();
    dashboardPage.assertErrorMessageOnDateIsVisible();
    const scheduledDatesDialog = dashboardPage.openIterationScheduledDatesDialog([
      {
        id: 1,
        name: 'iteration-1',
        scheduledStartDate: null,
        scheduledEndDate: addDays(new Date(), 2),
      },
      {
        id: 2,
        name: 'iteration-2',
        scheduledStartDate: addDays(new Date(), 10),
        scheduledEndDate: addDays(new Date(), 12),
      },
    ]);
    scheduledDatesDialog.setScheduledStartDateToNow(1, true);
    const updatedCampaignStatisticsBundle: StatisticsBundle = getDefaultCampaignStatisticsBundle();
    const statBundleMock = new HttpMockBuilder('campaign-view/*/statistics')
      .post()
      .responseBody(updatedCampaignStatisticsBundle)
      .build();
    scheduledDatesDialog.confirm([
      {
        id: 1,
        name: 'iteration-1',
        scheduledStartDate: new Date(),
        scheduledEndDate: new Date('2021-02-11T09:27:42.000+0000'),
      },
    ]);
    statBundleMock.wait();
    dashboardPage.assertAdvancementChartIsRendered();
  });
});

function initAtCampaignViewPage(
  campaignStatisticsBundle = getDefaultCampaignStatisticsBundle(),
): CampaignViewPage {
  const firstNode = initialNodes.dataRows[0];
  const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(
    initialNodes,
    createEntityReferentialData,
  );
  const tree = campaignWorkspacePage.tree;
  tree.assertNodeExist(firstNode.id);
  tree.assertNodeTextContains(firstNode.id, firstNode.data[GridColumnId.NAME]);
  const statBundleMock = new HttpMockBuilder('campaign-view/*/statistics')
    .post()
    .responseBody(campaignStatisticsBundle)
    .build();
  const campaignViewPage: CampaignViewPage = tree.selectNode('Campaign-3', campaignModel);
  statBundleMock.wait();
  campaignViewPage.assertExists();
  return campaignViewPage;
}
