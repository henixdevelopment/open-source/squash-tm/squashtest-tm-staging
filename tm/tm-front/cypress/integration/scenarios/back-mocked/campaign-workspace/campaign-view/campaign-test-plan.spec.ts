import { CampaignViewPage } from '../../../../page-objects/pages/campaign-workspace/campaign/campaign-view.page';
import {
  getDefaultCampaignStatisticsBundle,
  mockCampaignModel,
} from '../../../../data-mock/campaign.data-mock';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import {
  ALL_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder,
} from '../../../../utils/referential/referential-data-builder';
import { mockDataRow, mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import {
  initialTestCaseLibraries,
  projectOneChildren,
} from '../../../../data-mock/test-case-simple-tree';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { Identifier } from '../../../../../../projects/sqtm-core/src/lib/model/entity.model';
import { CampaignModel } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/campaign-model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import {
  DataRowModel,
  DataRowOpenState,
} from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';

describe('Campaign View', function () {
  function navigateToCampaign(): CampaignViewPage {
    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [
        mockTreeNode({
          id: 'CampaignLibrary-1',
          children: ['Campaign-1'],
          data: { NAME: 'Project1', CHILD_COUNT: 1 },
          state: DataRowOpenState.open,
        }),
        mockTreeNode({
          id: 'Campaign-1',
          children: [],
          projectId: 1,
          parentRowId: 'CampaignLibrary-1',
          data: { NAME: 'Campaign-1', CHILD_COUNT: 0 },
          state: DataRowOpenState.closed,
        }),
      ],
    };
    const refData = new ReferentialDataMockBuilder()
      .withProjects({
        allowAutomationWorkflow: true,
        permissions: ALL_PROJECT_PERMISSIONS,
      })
      .build();
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes, refData);
    campaignWorkspacePage.tree.openNode('Campaign-1');
    const model: CampaignModel = mockCampaignModel({
      name: 'Campaign-1',
      hasDatasets: true,
    });
    const statBundleMock = new HttpMockBuilder('campaign-view/*/statistics')
      .post()
      .responseBody(getDefaultCampaignStatisticsBundle())
      .build();
    const campaignViewPage = campaignWorkspacePage.tree.selectNode<CampaignViewPage>(
      'Campaign-1',
      model,
    );
    statBundleMock.wait();
    return campaignViewPage;
  }

  function mockTestPlan(dataRows: DataRowModel[]): GridResponse {
    return mockGridResponse('ctpiId', dataRows);
  }

  function mockCTPIDataRow(itemTestPlanId: Identifier, customData?: any): DataRowModel {
    return mockDataRow({
      id: itemTestPlanId.toString(),
      projectId: 1,
      data: {
        ctpiId: itemTestPlanId,
        campaignId: 1,
        testCaseReference: 'REF-0' + itemTestPlanId,
        testCaseName: 'TC-' + itemTestPlanId,
        testCaseId: itemTestPlanId,
        projectId: 1,
        projectName: 'Project1',
        importance: 'LOW',
        datasetName: 'd' + itemTestPlanId,
        [GridColumnId.assigneeFullName]: 'admin',
        ...customData,
      },
      allowMoves: true,
    });
  }

  it('Should display a simple test plan', () => {
    const campaignViewPage = navigateToCampaign();

    const testPlan = mockTestPlan([mockCTPIDataRow('1'), mockCTPIDataRow('2')]);

    const testPlanPage = campaignViewPage.showTestPlan(testPlan);
    testPlanPage.testPlan.assertRowExist('1');
    testPlanPage.testPlan.assertRowExist('2');

    testPlanPage.testPlan.getRow('1').cell('testCaseName').linkRenderer().assertContainText('TC-1');
    testPlanPage.testPlan.getRow('2').cell('testCaseName').linkRenderer().assertContainText('TC-2');
  });

  it('should display and filter a test plan', () => {
    const campaignViewPage = navigateToCampaign();
    new NavBarElement().toggle();
    campaignViewPage.toggleTree();

    const testPlan = mockTestPlan([mockCTPIDataRow('1'), mockCTPIDataRow('2')]);

    const testPlanPage = campaignViewPage.showTestPlan(testPlan);
    testPlanPage.testPlan.assertRowExist('1');
    testPlanPage.testPlan.assertRowExist('2');

    testPlanPage.testPlan.declareRefreshData({ ...testPlan, dataRows: [testPlan.dataRows[0]] });
    testPlanPage.testPlan.filterByTextColumn('testCaseName', 'value', {
      ...testPlan,
      dataRows: [testPlan.dataRows[0]],
    });

    testPlanPage.testPlan.assertRowExist('1');
    testPlanPage.testPlan.assertRowNotExist('2');
  });

  it('should reorder items with drag and drop', () => {
    const campaignViewPage = navigateToCampaign();
    new NavBarElement().toggle();
    campaignViewPage.toggleTree();

    const testPlan = mockTestPlan([
      mockCTPIDataRow(1),
      mockCTPIDataRow(2),
      mockCTPIDataRow(3),
      mockCTPIDataRow(4),
    ]);

    const testPlanPage = campaignViewPage.showTestPlan(testPlan);
    testPlanPage.testPlan.assertRowCount(4);
    testPlanPage.moveItemWithServerResponse('1', '3', {});
  });

  it('should forbid items reordering if grid is filtered', () => {
    const campaignViewPage = navigateToCampaign();
    new NavBarElement().toggle();
    campaignViewPage.toggleTree();

    const testPlan = mockTestPlan([
      mockCTPIDataRow(1),
      mockCTPIDataRow(2),
      mockCTPIDataRow(3),
      mockCTPIDataRow(4),
    ]);

    const testPlanPage = campaignViewPage.showTestPlan(testPlan);
    testPlanPage.testPlan.filterByTextColumn(GridColumnId.projectName, 'P', testPlan);

    testPlanPage.assertCannotMoveItem('1');
  });

  it('should add test cases to campaign', () => {
    const campaignViewPage = navigateToCampaign();
    new NavBarElement().toggle();
    campaignViewPage.toggleTree();

    const testPlan = mockTestPlan([]);

    const testPlanPage = campaignViewPage.showTestPlan(testPlan);

    const testCaseTreePicker = testPlanPage.openTestCaseDrawer(initialTestCaseLibraries);
    testCaseTreePicker.openNode('TestCaseLibrary-1', projectOneChildren);
    testCaseTreePicker.beginDragAndDrop('TestCase-3');

    const refreshedTestPlan = mockGridResponse('ctpiId', [
      mockCTPIDataRow(1, { testCaseName: 'TestCase-3' }),
    ]);

    testPlanPage.enterIntoTestPlan();
    testPlanPage.assertColoredBorderIsVisible();
    testPlanPage.dropIntoTestPlan(1, [1], refreshedTestPlan);
    testPlanPage.verifyTestPlanItem({ id: 1, name: 'TestCase-3', showsAsLink: true });
    testPlanPage.assertColoredBorderIsNotVisible();
    testPlanPage.closeTestCaseDrawer();
  });

  it('should remove test cases from campaign', () => {
    // This shouldn't be strictly necessary but it's here to avoid a virtual scroll issue with the grid (16/04/2020)
    cy.viewport(1200, 700);

    const campaignViewPage = navigateToCampaign();
    new NavBarElement().toggle();
    campaignViewPage.toggleTree();

    const testPlan = mockTestPlan([mockCTPIDataRow(1)]);

    const testPlanPage = campaignViewPage.showTestPlan(testPlan);
    const response = {
      count: 0,
      idAttribute: null,
      dataRows: [],
    };
    const confirmDialogComponent = testPlanPage.showDeleteConfirmDialog(1, 1);

    confirmDialogComponent.assertExists();
    confirmDialogComponent.deleteForSuccess(response);
    confirmDialogComponent.assertNotExist();
  });

  it('should mass remove test cases from campaign', () => {
    cy.viewport(1200, 700);

    const campaignViewPage = navigateToCampaign();
    new NavBarElement().toggle();
    campaignViewPage.toggleTree();

    const testPlan = mockTestPlan([
      mockCTPIDataRow(1),
      mockCTPIDataRow(2),
      mockCTPIDataRow(3),
      mockCTPIDataRow(4),
    ]);

    const testPlanPage = campaignViewPage.showTestPlan(testPlan);
    const response = {
      count: 0,
      idAttribute: null,
      dataRows: [],
    };
    testPlanPage.testPlan.selectRowsWithStickyIndexColumn([1, 2, 3, 4]);
    const confirmDialogComponent = testPlanPage.showMassDeleteConfirmDialog(1, [1, 2, 3, 4]);

    confirmDialogComponent.assertExists();
    confirmDialogComponent.deleteForSuccess(response);
    confirmDialogComponent.assertNotExist();
  });

  it('should update assigned user', () => {
    cy.viewport(1200, 700);
    const campaignViewPage = navigateToCampaign();
    new NavBarElement().toggle();
    campaignViewPage.toggleTree();

    const testPlan = mockTestPlan([
      mockCTPIDataRow(1),
      mockCTPIDataRow(2),
      mockCTPIDataRow(3),
      mockCTPIDataRow(4),
    ]);

    const testPlanPage = campaignViewPage.showTestPlan(testPlan);
    const row = testPlanPage.testPlan.getRow(1);
    const assignedUserCell = row.cell(GridColumnId.assigneeFullName).selectRenderer();
    assignedUserCell.changeValue('item-2', 'test-plan-item/*/assign-user-to-ctpi');
    assignedUserCell.assertContainText('Joe Ni (jawny)');
  });

  it('should mass edit ITPIs', () => {
    cy.viewport(1200, 700);
    const campaignViewPage = navigateToCampaign();
    new NavBarElement().toggle();
    campaignViewPage.toggleTree();

    const dataRows = [
      mockCTPIDataRow(1),
      mockCTPIDataRow(2),
      mockCTPIDataRow(3),
      mockCTPIDataRow(4),
    ];

    const testPlan = mockTestPlan(dataRows);

    const testPlanPage = campaignViewPage.showTestPlan(testPlan);
    testPlanPage.testPlan.selectRowsWithStickyIndexColumn([1, 2]);

    const massEditDialog = testPlanPage.openMassEditDialog();
    massEditDialog.assertExists();

    massEditDialog.toggleAssigneeEdition();
    massEditDialog.selectAssignee('Joe Ni (jawny)');

    massEditDialog.confirm(dataRows);
  });

  it('should update dataset', () => {
    cy.viewport(1200, 700);
    const campaignViewPage = navigateToCampaign();
    new NavBarElement().toggle();
    campaignViewPage.toggleTree();

    const dataRows = [
      mockCTPIDataRow(1, {
        datasetName: 'JDD',
        availableDatasets: [
          { id: 1, name: 'JDD' },
          { id: 2, name: 'Dataset' },
        ],
      }),
    ];

    const testPlan = mockTestPlan(dataRows);
    const testPlanPage = campaignViewPage.showTestPlan(testPlan);

    cy.get(`[data-test-cell-id=dataset-cell]`).should('contain.text', 'JDD');
    testPlanPage.changeDataset('item-2');
    cy.get(`[data-test-cell-id=dataset-cell]`).should('contain.text', 'Dataset');
  });
});
