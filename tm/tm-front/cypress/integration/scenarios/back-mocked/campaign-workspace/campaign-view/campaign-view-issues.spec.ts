import { ReferentialDataMockBuilder } from '../../../../utils/referential/referential-data-builder';
import { CampaignViewPage } from '../../../../page-objects/pages/campaign-workspace/campaign/campaign-view.page';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import {
  getDefaultCampaignStatisticsBundle,
  mockCampaignModel,
} from '../../../../data-mock/campaign.data-mock';
import { AuthenticationProtocol } from '../../../../../../projects/sqtm-core/src/lib/model/third-party-server/authentication.model';
import { CampaignModel } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/campaign-model';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { mockDataRow, mockTreeNode } from '../../../../data-mock/grid.data-mock';

const initialCampaignRow = mockTreeNode({
  id: 'Campaign-3',
  children: [],
  projectId: 1,
  parentRowId: 'CampaignLibrary-1',
  data: {
    NAME: 'Campaign3',
    CHILD_COUNT: 0,
  },
  state: DataRowOpenState.closed,
});

const referentialData = new ReferentialDataMockBuilder()
  .withProjects({
    name: 'Project_Issues',
    bugTrackerBinding: { bugTrackerId: 1, projectId: 3 },
  })
  .withBugTrackers({
    name: 'bugtracker',
    authProtocol: AuthenticationProtocol.BASIC_AUTH,
  })
  .build();

describe('Campaign View - Issues', () => {
  it('should display connection page if user not connected to bugtracker', () => {
    const campaignViewPage = navigateToCampaign();
    const issuePage = campaignViewPage.showIssuesWithoutBindingToBugTracker();
    const connectionDialog = issuePage.openConnectionDialog();
    connectionDialog.fillUserName('admin');
    connectionDialog.fillPassword('admin');
    connectionDialog.connection();
  });

  it('should display table issues', () => {
    const modelResponse = {
      entityType: 'campaign',
      bugTrackerStatus: 'AUTHENTICATED',
      projectName: '["LELprojet","test","LELprojetclassique","LELclassique","Projet_Test_Arnaud"]',
      projectId: 328,
      delete: '',
      oslc: false,
    };
    const campaignViewPage = navigateToCampaign();
    const gridResponse = {
      dataRows: [
        mockDataRow({
          id: 1,
          data: {
            summary: 'Anomalie',
            url: 'http://mybutracker.com',
            remoteId: 'TA-1',
            priority: '1',
            status: 'Error',
            assignee: 'Admin',
            executionId: '2',
            executionOrder: '1',
            executionName: 'Mon cas de test',
            suiteNames: '',
            btProject: 'Projet',
          },
        }),
      ],
    } as GridResponse;
    const issuePage = campaignViewPage.showIssuesIfBoundToBugTracker(modelResponse, gridResponse);
    const grid = issuePage.issueGrid;

    grid.assertRowCount(1);
    grid.getRow(1).cell('summary').textRenderer().assertContainsText('Anomalie');
  });

  function navigateToCampaign(): CampaignViewPage {
    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [
        mockTreeNode({
          id: 'CampaignLibrary-1',
          children: ['Campaign-3'],
          data: { NAME: 'Project_Issues', CHILD_COUNT: 1 },
          state: DataRowOpenState.open,
        }),
        initialCampaignRow,
      ],
    };
    const testCaseWorkspacePage = CampaignWorkspacePage.initTestAtPage(
      initialNodes,
      referentialData,
    );

    const model: CampaignModel = mockCampaignModel({
      id: 3,
      projectId: 1,
      name: 'Campaign3',
      customFieldValues: [],
      attachmentList: {
        id: 1,
        attachments: [],
      },
      reference: '',
      description: '',
      nbIssues: 2,
      lastModifiedOn: new Date('2020-03-09 10:30').toISOString(),
      lastModifiedBy: 'admin',
      createdOn: new Date('2020-03-09 10:30').toISOString(),
      createdBy: 'admin',
      testPlanStatistics: {
        nbUntestable: 0,
        nbSuccess: 0,
        nbSettled: 0,
        nbRunning: 0,
        nbReady: 0,
        nbFailure: 0,
        nbBlocked: 0,
        nbDone: 0,
        nbTestCases: 0,
        progression: 0,
        status: 'READY',
      },
      campaignStatus: 'UNDEFINED',
      progressStatus: 'READY',
      milestones: [],
      hasDatasets: false,
    });
    const statBundleMock = new HttpMockBuilder('campaign-view/*/statistics')
      .post()
      .responseBody(getDefaultCampaignStatisticsBundle())
      .build();
    const campaignViewPage = testCaseWorkspacePage.tree.selectNode<CampaignViewPage>(
      'Campaign-3',
      model,
    );
    statBundleMock.wait();
    return campaignViewPage;
  }
});
