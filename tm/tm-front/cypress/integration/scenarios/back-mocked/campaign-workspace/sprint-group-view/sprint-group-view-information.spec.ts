import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { createBasicTreeWithOneSprintGroup } from '../campaign-workspace-mock-data';
import { SprintGroupViewPage } from '../../../../page-objects/pages/campaign-workspace/sprint-group/sprint-group-view.page';
import { TreeElement } from '../../../../page-objects/elements/grid/grid.element';

const initialNodes: GridResponse = createBasicTreeWithOneSprintGroup();

describe('Sprint group - Information', () => {
  it('should display sprint group information', () => {
    const sprintGroupViewPage: SprintGroupViewPage = navigateToSprintGroup();
    sprintGroupViewPage.checkData('sprint-group-created', '14/02/2024 15:00 (squashUser)');
    sprintGroupViewPage.checkData('sprint-group-lastModified', '15/02/2024 15:00 (squashAdmin)');
    sprintGroupViewPage.descriptionRichField.checkTextContent('This is my description');
  });

  function navigateToSprintGroup(): SprintGroupViewPage {
    const campaignWorkspacePage: CampaignWorkspacePage =
      CampaignWorkspacePage.initTestAtPage(initialNodes);

    const tree: TreeElement = campaignWorkspacePage.tree;
    tree.assertNodeExist('CampaignLibrary-1');
    tree.assertNodeTextContains('CampaignLibrary-1', 'Project1');
    tree.assertNodeExist('SprintGroup-1');
    tree.assertNodeTextContains('SprintGroup-1', 'SprintGroup 1');

    const sprintGroupViewPage: SprintGroupViewPage = tree.selectNode('SprintGroup-1');
    sprintGroupViewPage.assertExists();

    return sprintGroupViewPage;
  }
});
