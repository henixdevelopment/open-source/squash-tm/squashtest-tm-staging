import { SprintGroupViewPage } from '../../../../page-objects/pages/campaign-workspace/sprint-group/sprint-group-view.page';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { createBasicTreeWithOneSprintGroup } from '../campaign-workspace-mock-data';
import { TreeElement } from '../../../../page-objects/elements/grid/grid.element';

const initialNodes: GridResponse = createBasicTreeWithOneSprintGroup();
describe('Sprint View Display', () => {
  it('should display sprint view', () => {
    const sprintGroupViewPage: SprintGroupViewPage = navigateToSprintGroup();
    sprintGroupViewPage.assertExists();
    sprintGroupViewPage.nameTextField.checkContent('SprintGroup 1');
  });

  function navigateToSprintGroup(): SprintGroupViewPage {
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes);

    const tree: TreeElement = campaignWorkspacePage.tree;
    tree.assertNodeExist('CampaignLibrary-1');
    tree.assertNodeTextContains('CampaignLibrary-1', 'Project1');
    tree.assertNodeExist('SprintGroup-1');
    tree.assertNodeTextContains('SprintGroup-1', 'SprintGroup 1');

    const sprintGroupViewPage: SprintGroupViewPage = tree.selectNode('SprintGroup-1');
    sprintGroupViewPage.assertExists();

    return sprintGroupViewPage;
  }
});
