import { IterationViewPage } from '../../../../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { createEntityReferentialData } from '../../../../utils/referential/create-entity-referential.const';
import {
  getEmptyExecutionEnvironmentsCount,
  getEmptyIterationStatisticsBundle,
  mockIterationModel,
} from '../../../../data-mock/iteration.data-mock';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { IterationModel } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/iteration-model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { mockTreeNode } from '../../../../data-mock/grid.data-mock';

describe('Iteration - Information', () => {
  it('should display iteration informations', () => {
    const iterationViewPage = navigateToIteration();
    const infoPanel = iterationViewPage.informationPanel;
    iterationViewPage.clickInformationAnchorLink();

    iterationViewPage.checkData('iteration-id', '1');
    iterationViewPage.checkData('iteration-created', '09/03/2020 10:30 (toto)');
    iterationViewPage.checkData('iteration-lastModified', '09/03/2020 11:30 (gilbert)');
    iterationViewPage.checkData('iteration-status', 'Planifié');
    iterationViewPage.checkData('iteration-progress-state', 'Terminé');
    infoPanel.descriptionRichField.checkTextContent('this is description');
  });

  it('should allow modifications on iteration informations', () => {
    const iterationViewPage = navigateToIteration();
    const infoPanel = iterationViewPage.informationPanel;
    iterationViewPage.clickInformationAnchorLink();

    infoPanel.rename('GREAT ITERATION ! NO CHEATING !', {
      dataRows: [
        {
          ...initialIterationRow,
          data: { ...initialIterationRow.data, NAME: 'GREAT ITERATION ! NO CHEATING !' },
        },
      ],
    });
    infoPanel.statusSelectField.setAndConfirmValueNoButton('Archivé');

    const descriptionElement = infoPanel.descriptionRichField;
    descriptionElement.enableEditMode();
    descriptionElement.setValue('Hello, World!');
    descriptionElement.confirm('Hello, World!');
    descriptionElement.enableEditMode();
    descriptionElement.clear();
    descriptionElement.confirm(`Cliquer pour renseigner une description`);
  });

  function navigateToIteration(items: any[] = []): IterationViewPage {
    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [
        mockTreeNode({
          id: 'CampaignLibrary-1',
          children: ['Campaign-3'],
          data: { NAME: 'Project1', CHILD_COUNT: 1 },
          state: DataRowOpenState.open,
        }),
        mockTreeNode({
          id: 'Campaign-3',
          children: ['Iteration-1'],
          projectId: 1,
          parentRowId: 'CampaignLibrary-1',
          state: DataRowOpenState.open,
          data: { NAME: 'campaign3', CHILD_COUNT: 1, MILESTONE_STATUS: 'IN_PROGRESS' },
        }),
        mockTreeNode({
          id: 'Iteration-1',
          children: [],
          projectId: 1,
          parentRowId: 'Campaign-3',
          data: { NAME: 'iteration-1', CHILD_COUNT: 0, MILESTONE_STATUS: 'IN_PROGRESS' },
        }),
      ],
    };

    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(
      initialNodes,
      createEntityReferentialData,
    );
    const model: IterationModel = mockIterationModel({
      id: 1,
      projectId: 1,
      name: 'iteration-1',
      itpi: items,
      reference: 'REF 1',
      description: 'this is description',
      createdBy: 'toto',
      createdOn: new Date('2020-03-09 10:30').toISOString(),
      lastModifiedBy: 'gilbert',
      lastModifiedOn: new Date('2020-03-09 11:30').toISOString(),
      scheduledStartDate: new Date('2020-04-10 10:30').toISOString(),
      scheduledEndDate: new Date('2020-05-11 10:30').toISOString(),
      actualStartDate: new Date('2020-04-10 10:30').toISOString(),
      actualEndDate: new Date('2020-05-11 10:30').toISOString(),
      actualStartAuto: false,
      actualEndAuto: false,
      iterationStatus: 'PLANNED',
      uuid: 'b368',
    });
    new HttpMockBuilder(`iteration-view/${model.id}/statistics`)
      .post()
      .responseBody(getEmptyIterationStatisticsBundle())
      .build();

    new HttpMockBuilder(
      `test-automation/iteration/*/automated-execution-environments-statuses-count`,
    )
      .get()
      .responseBody(getEmptyExecutionEnvironmentsCount())
      .build();

    return campaignWorkspacePage.tree.selectNode<IterationViewPage>('Iteration-1', model);
  }

  const initialIterationRow = mockTreeNode({
    id: 'Iteration-1',
    children: [],
    projectId: 1,
    parentRowId: 'Campaign-3',
    data: { NAME: 'iteration-1', CHILD_COUNT: 0 },
  });
});
