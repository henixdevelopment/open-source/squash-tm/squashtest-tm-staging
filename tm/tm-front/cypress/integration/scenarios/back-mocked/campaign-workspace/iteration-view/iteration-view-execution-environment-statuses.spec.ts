import {
  getEmptyIterationStatisticsBundle,
  mockIterationModel,
} from '../../../../data-mock/iteration.data-mock';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { IterationViewPage } from '../../../../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import { HTTP_RESPONSE_STATUS, HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import {
  ALL_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder,
} from '../../../../utils/referential/referential-data-builder';
import { IterationModel } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/iteration-model';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { EnvironmentStatusesCountDTO } from '../../../../page-objects/elements/automated-execution-environments/environments-statuses-count.element';
import { EnvironmentsStatusesSummaryDialogElement } from '../../../../page-objects/elements/automated-execution-environments/environments-statuses-summary-dialog.element';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

describe('Iteration View', function () {
  it('Execution Environments count should be visible and global status should be ok', () => {
    const iterationViewPage = navigateToIteration();
    const countElement = iterationViewPage.environmentsStatusesCountElement;

    countElement.assertCountIsVisible();
    countElement.assertCapsuleIsClickable();
    countElement.assertGlobalStatusIconIs('check-circle');
    countElement.assertIconColorIs('IDLE', 'rgb(98, 115, 127)');
    countElement.assertCountIs('IDLE', 12);
    countElement.assertCountIs('BUSY', 3);
    countElement.assertCountIs('PENDING', 0);
    countElement.assertCountIs('UNREACHABLE', 0);
  });

  it('Should refresh execution environments count component with new warning values', () => {
    const iterationViewPage = navigateToIteration();
    const countElement = iterationViewPage.environmentsStatusesCountElement;

    countElement.assertCountIsVisible();
    countElement.assertCapsuleIsClickable();
    countElement.assertCountIs('IDLE', 12);
    countElement.assertCountIs('BUSY', 3);
    countElement.assertCountIs('PENDING', 0);
    countElement.assertCountIs('UNREACHABLE', 0);

    const newCountResponseBody: EnvironmentStatusesCountDTO = {
      statuses: {
        IDLE: 4,
        BUSY: 2,
        PENDING: 6,
        UNREACHABLE: 1,
      },
      projects: [
        {
          projectId: 1,
          projectName: 'project 01',
          squashOrchestratorId: 5,
          squashOrchestratorName: 'the orchestrator 05',
        },
        {
          projectId: 2,
          projectName: 'project 02',
          squashOrchestratorId: 5,
          squashOrchestratorName: 'the orchestrator 05',
        },
        {
          projectId: 3,
          projectName: 'project 03',
          squashOrchestratorId: 7,
          squashOrchestratorName: 'the orchestrator 07',
        },
        {
          projectId: 4,
          projectName: 'project 04',
          squashOrchestratorId: 8,
          squashOrchestratorName: 'the orchestrator 08',
        },
      ],
      nbOfSquashOrchestratorServers: 3,
      nbOfUnreachableSquashOrchestrators: 1,
      nbOfServersWithMissingToken: 1,
    };
    countElement.refreshCountComponent(newCountResponseBody, 'iteration');

    countElement.assertCountIsVisible();
    countElement.assertCapsuleIsClickable();
    countElement.assertGlobalStatusIconIs('warning');
    countElement.assertIconColorIs('IDLE', 'rgb(98, 115, 127)');

    countElement.assertCountIs('IDLE', 4);
    countElement.assertCountIs('BUSY', 2);
    countElement.assertCountIs('PENDING', 6);
    countElement.assertCountIs('UNREACHABLE', 1);
  });

  it('Should refresh execution environments count component with new error values', () => {
    const iterationViewPage = navigateToIteration();
    const countElement = iterationViewPage.environmentsStatusesCountElement;

    countElement.assertCountIsVisible();
    countElement.assertCountIs('IDLE', 12);
    countElement.assertCountIs('BUSY', 3);
    countElement.assertCountIs('PENDING', 0);
    countElement.assertCountIs('UNREACHABLE', 0);

    const newCountResponseBody: EnvironmentStatusesCountDTO = {
      statuses: null,
      projects: [
        {
          projectId: 1,
          projectName: 'project 01',
          squashOrchestratorId: 5,
          squashOrchestratorName: 'the orchestrator 05',
        },
        {
          projectId: 2,
          projectName: 'project 02',
          squashOrchestratorId: 5,
          squashOrchestratorName: 'the orchestrator 05',
        },
        {
          projectId: 3,
          projectName: 'project 03',
          squashOrchestratorId: 7,
          squashOrchestratorName: 'the orchestrator 07',
        },
        {
          projectId: 4,
          projectName: 'project 04',
          squashOrchestratorId: 8,
          squashOrchestratorName: 'the orchestrator 08',
        },
      ],
      nbOfSquashOrchestratorServers: 3,
      nbOfUnreachableSquashOrchestrators: 3,
      nbOfServersWithMissingToken: 0,
    };
    countElement.refreshCountComponent(newCountResponseBody, 'iteration');

    countElement.assertCountIsVisible();
    countElement.assertCapsuleIsNotClickable();
    countElement.assertGlobalStatusIconIs('close');
    countElement.assertIconColorIs('IDLE', 'rgb(255, 205, 200)');
    countElement.assertCountIs('IDLE', 0);
    countElement.assertCountIs('BUSY', 0);
    countElement.assertCountIs('PENDING', 0);
    countElement.assertCountIs('UNREACHABLE', 0);
  });

  it('should open summary dialog with execution environments available', () => {
    const iterationViewPage = navigateToIteration();
    const countElement = iterationViewPage.environmentsStatusesCountElement;

    countElement.assertCountIsVisible();
    countElement.assertCapsuleIsClickable();

    countElement.openExecutionEnvironmentsSummaryDialog(
      {
        environments: {
          environments: [
            {
              name: 'agent-01',
              namespaces: ['ns10'],
              tags: ['robotframework', 'windows', 'robot1', 'cucumber', 'cucumber5'],
              status: 'IDLE',
            },
          ],
        },
        server: {
          testAutomationServerId: 4,
          defaultTags: [],
          hasServerCredentials: false,
        },
        project: {
          projectId: 1,
          hasProjectToken: true,
          projectTags: [],
          areProjectTagsInherited: true,
        },
        errorMessage: null,
      },
      HTTP_RESPONSE_STATUS.SUCCESS,
    );

    const environmentSummaryDialog = new EnvironmentsStatusesSummaryDialogElement();
    environmentSummaryDialog.assertTitleHasText("Environnements d'exécution");
    environmentSummaryDialog.assertCloseButtonExists();
    environmentSummaryDialog.assertSummaryGridForProjectIsVisible(1, 'project 01');
    environmentSummaryDialog.assertEnvironmentGridIsVisible();
  });

  it('should open summary dialog with no environments available', () => {
    const iterationViewPage = navigateToIteration();
    const countElement = iterationViewPage.environmentsStatusesCountElement;

    countElement.assertCountIsVisible();
    countElement.assertCapsuleIsClickable();

    countElement.openExecutionEnvironmentsSummaryDialog(
      {
        environments: {
          environments: [],
        },
        server: {
          testAutomationServerId: 4,
          defaultTags: [],
          hasServerCredentials: true,
        },
        project: {
          projectId: 1,
          hasProjectToken: false,
          projectTags: [],
          areProjectTagsInherited: true,
        },
        errorMessage: null,
      },
      HTTP_RESPONSE_STATUS.SUCCESS,
    );

    const environmentSummaryDialog = new EnvironmentsStatusesSummaryDialogElement();
    environmentSummaryDialog.assertTitleHasText("Environnements d'exécution");
    environmentSummaryDialog.assertCloseButtonExists();
    environmentSummaryDialog.assertSummaryGridForProjectIsVisible(1, 'project 01');
    environmentSummaryDialog.assertNoEnvironmentsWarningIsVisible('project 01', 'the orchestrator');
  });

  it('should open summary dialog with orchestrator communication error message', () => {
    const iterationViewPage = navigateToIteration();
    const countElement = iterationViewPage.environmentsStatusesCountElement;

    countElement.assertCountIsVisible();
    countElement.assertCapsuleIsClickable();

    countElement.openExecutionEnvironmentsSummaryDialog(
      {
        environments: {
          environments: null,
        },
        server: {
          testAutomationServerId: 4,
          defaultTags: [],
          hasServerCredentials: true,
        },
        project: {
          projectId: 1,
          hasProjectToken: false,
          projectTags: [],
          areProjectTagsInherited: true,
        },
        errorMessage:
          "Squash n'a pas pu joindre le service 'receptionist' à l'URL 'http://localhost:7775'. (Code d'erreur: '405')",
      },
      HTTP_RESPONSE_STATUS.SUCCESS,
    );

    const environmentSummaryDialog = new EnvironmentsStatusesSummaryDialogElement();
    environmentSummaryDialog.assertTitleHasText("Environnements d'exécution");
    environmentSummaryDialog.assertCloseButtonExists();
    environmentSummaryDialog.assertSummaryGridForProjectIsVisible(1, 'project 01');
    environmentSummaryDialog.assertErrorMessageIsVisible(
      "Squash n'a pas pu joindre le service 'receptionist' à l'URL 'http://localhost:7775'. (Code d'erreur: '405')",
    );
  });

  it('should open summary dialog with no token warning message', () => {
    const iterationViewPage = navigateToIteration();
    const countElement = iterationViewPage.environmentsStatusesCountElement;

    countElement.assertCountIsVisible();
    countElement.assertCapsuleIsClickable();

    countElement.openExecutionEnvironmentsSummaryDialog(
      {
        environments: {
          environments: null,
        },
        server: {
          testAutomationServerId: 4,
          defaultTags: null,
          hasServerCredentials: false,
        },
        project: {
          projectId: 4,
          hasProjectToken: false,
          projectTags: null,
          areProjectTagsInherited: false,
        },
        errorMessage: null,
      },
      HTTP_RESPONSE_STATUS.SUCCESS,
    );

    const environmentSummaryDialog = new EnvironmentsStatusesSummaryDialogElement();
    environmentSummaryDialog.assertTitleHasText("Environnements d'exécution");
    environmentSummaryDialog.assertCloseButtonExists();
    environmentSummaryDialog.assertSummaryGridForProjectIsVisible(1, 'project 01');
    environmentSummaryDialog.assertNoTokenWarningIsVisible();
  });

  it('should manage all HTTP error codes inside the custom pop-up', () => {
    const iterationViewPage = navigateToIteration();
    const countElement = iterationViewPage.environmentsStatusesCountElement;

    countElement.assertCountIsVisible();
    countElement.assertCapsuleIsClickable();

    countElement.openExecutionEnvironmentsSummaryDialog(
      null,
      HTTP_RESPONSE_STATUS.INTERNAL_SERVER_ERROR,
    );

    const environmentSummaryDialog = new EnvironmentsStatusesSummaryDialogElement();
    environmentSummaryDialog.assertTitleHasText("Environnements d'exécution");
    environmentSummaryDialog.assertCloseButtonExists();
    environmentSummaryDialog.assertSummaryGridForProjectIsVisible(1, 'project 01');
    environmentSummaryDialog.assertErrorMessageIsVisible(
      '/backend/test-automation/1/automated-execution-environments/all?frontEndErrorIsHandled=true: 500 Internal Server Error',
    );
  });

  function navigateToIteration(): IterationViewPage {
    const refData = new ReferentialDataMockBuilder()
      .withProjects({
        allowAutomationWorkflow: true,
        permissions: ALL_PROJECT_PERMISSIONS,
      })
      .build();

    const initialNodes: GridResponse = mockGridResponse('id', [
      mockTreeNode({
        id: 'CampaignLibrary-1',
        children: ['Campaign-3'],
        data: { NAME: 'Project1', CHILD_COUNT: 1 },
        state: DataRowOpenState.open,
      }),
      mockTreeNode({
        id: 'Campaign-3',
        children: ['Iteration-1'],
        projectId: 1,
        parentRowId: 'CampaignLibrary-1',
        state: DataRowOpenState.open,
        data: { NAME: 'campaign3', CHILD_COUNT: 1 },
      }),
      mockTreeNode({
        id: 'Iteration-1',
        children: [],
        projectId: 1,
        parentRowId: 'Campaign-3',
        data: { NAME: 'iteration-1', CHILD_COUNT: 0 },
      }),
    ]);
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes, refData);

    const model: IterationModel = mockIterationModel(buildDefaultIterationCustomData());

    new HttpMockBuilder(`iteration-view/${model.id}/statistics`)
      .post()
      .responseBody(getEmptyIterationStatisticsBundle())
      .build();

    new HttpMockBuilder<EnvironmentStatusesCountDTO>(
      `test-automation/iteration/*/automated-execution-environments-statuses-count`,
    )
      .get()
      .responseBody({
        statuses: {
          IDLE: 12,
          BUSY: 3,
        },
        projects: [
          {
            projectId: 1,
            projectName: 'project 01',
            squashOrchestratorId: 5,
            squashOrchestratorName: 'the orchestrator',
          },
        ],
        nbOfSquashOrchestratorServers: 1,
        nbOfUnreachableSquashOrchestrators: 0,
        nbOfServersWithMissingToken: 0,
      })
      .build();

    const iterationViewPage = campaignWorkspacePage.tree.selectNode<IterationViewPage>(
      'Iteration-1',
      model,
    );

    // Full screen
    new NavBarElement().toggle();
    iterationViewPage.toggleTree();

    return iterationViewPage;
  }
});

function buildDefaultIterationCustomData(): Partial<IterationModel> {
  return {
    id: 1,
    projectId: 1,
    name: 'iteration-1',
    itpi: [],
    iterationStatus: 'PLANNED',
    uuid: 'b368',
    testPlanStatistics: {
      status: 'DONE',
      progression: 100,
      nbTestCases: 3,
      nbDone: 3,
      nbReady: 0,
      nbRunning: 0,
      nbUntestable: 0,
      nbBlocked: 0,
      nbFailure: 0,
      nbSettled: 0,
      nbSuccess: 0,
    },
    hasDatasets: true,
    users: [
      { id: 1, login: 'raowl', firstName: 'Ra', lastName: 'Oul' },
      { id: 2, login: 'jawny', firstName: 'Joe', lastName: 'Ni' },
    ],
    testSuites: [
      { id: 1, name: 'suite01' },
      { id: 2, name: 'suite02' },
    ],
  };
}
