import {
  mockAutomatedSuiteDataRow,
  mockExecutionComparison,
  mockTestPlan,
  navigateToIteration,
} from '../../../../data-mock/iteration-automated-suite.data-mock';

describe('Iteration View', function () {
  it('should display a simple test plan', () => {
    const iterationViewPage = navigateToIteration();
    const today = new Date();
    const testPlan = mockTestPlan([
      mockAutomatedSuiteDataRow('456', today),
      mockAutomatedSuiteDataRow('123', today, { executionStatus: 'RUNNING' }),
      mockAutomatedSuiteDataRow('az47', today, { hasExecution: true, hasResultUrl: true }),
      mockAutomatedSuiteDataRow('sdfds58', today),
    ]);
    const automatedSuitePage = iterationViewPage.clickAutomatedSuitePageAnchorLink(testPlan);
    automatedSuitePage.testPlan.assertRowCount(4);
    automatedSuitePage.testPlan.assertRowExist('456');
    const row2 = automatedSuitePage.testPlan.getRow('123');
    row2.assertExists();
    row2.cell('executionDetails').iconRenderer().assertNotExist();
    row2.cell('executionReport').iconRenderer().assertNotExist();
    const row3 = automatedSuitePage.testPlan.getRow('az47');
    row3.assertExists();
    row3
      .cell('executionDetails')
      .iconRenderer()
      .assertContainsIcon('anticon-sqtm-core-campaign:exec_details');
    row3
      .cell('executionReport')
      .iconRenderer()
      .assertContainsIcon('anticon-sqtm-core-campaign:exec_report');

    automatedSuitePage.testPlan.assertRowExist('sdfds58');
  });

  it('should compare automated suites when premium plugin is installed', () => {
    const iterationViewPage = navigateToIteration([], true);
    const today = new Date();
    const yesterday = new Date(today.getTime() - 24 * 60 * 60 * 1000);
    const testPlan = mockTestPlan([
      mockAutomatedSuiteDataRow('az47', today, { hasExecution: true, hasResultUrl: true }),
      mockAutomatedSuiteDataRow('ad45az', yesterday, { hasExecution: true, hasResultUrl: true }),
    ]);
    const automatedSuitePage = iterationViewPage.clickAutomatedSuitePageAnchorLink(testPlan);
    automatedSuitePage.assertCompareExecutionButtonIsDisabled();
    automatedSuitePage.testPlan.selectRows(['az47', 'ad45az'], '#', 'leftViewport');

    const comparisonDialog =
      automatedSuitePage.clickCompareExecutionButton(mockExecutionComparison());
    comparisonDialog.gridElement.assertColumnCount(4);
    comparisonDialog.gridElement.assertRowCount(2);
  });

  it('should not display compare automated suites button when community', () => {
    const iterationViewPage = navigateToIteration([], false);
    const today = new Date();
    const yesterday = new Date(today.getTime() - 24 * 60 * 60 * 1000);
    const testPlan = mockTestPlan([
      mockAutomatedSuiteDataRow('az47', today, { hasExecution: true, hasResultUrl: true }),
      mockAutomatedSuiteDataRow('ad45az', yesterday, { hasExecution: true, hasResultUrl: true }),
    ]);
    const automatedSuitePage = iterationViewPage.clickAutomatedSuitePageAnchorLink(testPlan);
    automatedSuitePage.assertCompareExecutionButtonIsNotVisible();
  });

  it('should stop workflow launched in AUTOM mode', () => {
    const iterationViewPage = navigateToIteration();
    const today = new Date();
    const workflowId = '20027990-c45a-4355-908f-870348230dd4';
    const testPlan = mockTestPlan([
      mockAutomatedSuiteDataRow('az47', today, {
        executionStatus: 'RUNNING',
        workflows: [
          {
            workflowId: workflowId,
            projectId: 45,
          },
        ],
      }),
    ]);
    const automatedSuitePage = iterationViewPage.clickAutomatedSuitePageAnchorLink(testPlan);

    const refreshResponse = mockTestPlan([
      mockAutomatedSuiteDataRow('az47', today, { executionStatus: 'BLOCKED' }),
    ]);

    const row = automatedSuitePage.testPlan.getRow('az47', 'rightViewport');
    automatedSuitePage.openStopWorkflowConfirmDialog(row);
    automatedSuitePage.confirmStopWorkflow(row, refreshResponse);
  });

  it('should stop be able to stop workflow launched in DEVOPS mode', () => {
    const iterationViewPage = navigateToIteration();
    const today = new Date();
    const testPlan = mockTestPlan([
      mockAutomatedSuiteDataRow('az47', today, {
        executionStatus: 'RUNNING',
      }),
    ]);
    const automatedSuitePage = iterationViewPage.clickAutomatedSuitePageAnchorLink(testPlan);

    const row = automatedSuitePage.testPlan.getRow('az47', 'rightViewport');
    automatedSuitePage.assertStopWorkflowButtonIsDisabled(row);
  });

  it('should delete automated suite', () => {
    const iterationViewPage = navigateToIteration();
    const today = new Date();
    const testPlan = mockTestPlan([
      mockAutomatedSuiteDataRow('az47', today),
      mockAutomatedSuiteDataRow('ad45az', today),
    ]);
    const automatedSuitePage = iterationViewPage.clickAutomatedSuitePageAnchorLink(testPlan);

    automatedSuitePage.testPlan.assertRowCount(2);

    automatedSuitePage.deleteAutomatedSuite(
      'az47',
      mockTestPlan([mockAutomatedSuiteDataRow('ad45az', today)]),
    );

    automatedSuitePage.testPlan.assertRowCount(1);
  });

  it('should delete mass automated suites', () => {
    const iterationViewPage = navigateToIteration();
    const today = new Date();
    const testPlan = mockTestPlan([
      mockAutomatedSuiteDataRow('az47', today),
      mockAutomatedSuiteDataRow('ad45az', today),
    ]);
    const automatedSuitePage = iterationViewPage.clickAutomatedSuitePageAnchorLink(testPlan);

    automatedSuitePage.testPlan.assertRowCount(2);

    automatedSuitePage.deleteMassRows(['az47', 'ad45az']);

    automatedSuitePage.testPlan.assertRowCount(0);
  });

  it('should archive automated suite', () => {
    const iterationViewPage = navigateToIteration();
    const today = new Date();
    const testPlan = mockTestPlan([mockAutomatedSuiteDataRow('az47', today)]);
    const automatedSuitePage = iterationViewPage.clickAutomatedSuitePageAnchorLink(testPlan);
    const row = automatedSuitePage.testPlan.getRow('az47', 'rightViewport');
    automatedSuitePage.pruneAutomatedSuite(row);
  });

  it('should archive mass automated suites', () => {
    const iterationViewPage = navigateToIteration();
    const today = new Date();
    const testPlan = mockTestPlan([
      mockAutomatedSuiteDataRow('az47', today),
      mockAutomatedSuiteDataRow('ad45az', today),
    ]);
    const automatedSuitePage = iterationViewPage.clickAutomatedSuitePageAnchorLink(testPlan);

    automatedSuitePage.testPlan.assertRowCount(2);

    automatedSuitePage.pruneMassRows(['az47', 'ad45az']);
  });
});
