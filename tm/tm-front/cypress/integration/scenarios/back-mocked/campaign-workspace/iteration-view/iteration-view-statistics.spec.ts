import { IterationViewPage } from '../../../../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import {
  getEmptyExecutionEnvironmentsCount,
  getEmptyIterationStatisticsBundle,
  mockIterationModel,
} from '../../../../data-mock/iteration.data-mock';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { IterationModel } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/iteration-model';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

describe('Iteration - Statistics', () => {
  it('should display iteration statistics', () => {
    const iterationViewPage = navigateToIteration();
    const statPanel = iterationViewPage.clickStatisticsAnchorLink();
    statPanel.assertExists();
    iterationViewPage.checkData('campaign-progression', '100');
  });

  function navigateToIteration(): IterationViewPage {
    const initialNodes = mockGridResponse('id', [
      mockTreeNode({
        id: 'CampaignLibrary-1',
        children: ['Campaign-3'],
        data: { NAME: 'Project1', CHILD_COUNT: 1 },
        state: DataRowOpenState.open,
      }),
      mockTreeNode({
        id: 'Campaign-3',
        children: ['Iteration-1'],
        projectId: 1,
        parentRowId: 'CampaignLibrary-1',
        state: DataRowOpenState.open,
        data: { NAME: 'campaign3', CHILD_COUNT: 1 },
      }),
      mockTreeNode({
        id: 'Iteration-1',
        children: [],
        projectId: 1,
        parentRowId: 'Campaign-3',
        data: { NAME: 'iteration-1', CHILD_COUNT: 0 },
      }),
    ]);
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes);

    const model: IterationModel = mockIterationModel({
      testPlanStatistics: {
        status: 'DONE',
        progression: 100,
        nbTestCases: 3,
        nbDone: 3,
        nbReady: 0,
        nbRunning: 0,
        nbUntestable: 0,
        nbBlocked: 0,
        nbFailure: 0,
        nbSettled: 0,
        nbSuccess: 0,
      },
    });

    new HttpMockBuilder(`iteration-view/${model.id}/statistics`)
      .post()
      .responseBody(getEmptyIterationStatisticsBundle())
      .build();

    new HttpMockBuilder(
      `test-automation/iteration/*/automated-execution-environments-statuses-count`,
    )
      .get()
      .responseBody(getEmptyExecutionEnvironmentsCount())
      .build();

    return campaignWorkspacePage.tree.selectNode<IterationViewPage>('Iteration-1', model);
  }
});
