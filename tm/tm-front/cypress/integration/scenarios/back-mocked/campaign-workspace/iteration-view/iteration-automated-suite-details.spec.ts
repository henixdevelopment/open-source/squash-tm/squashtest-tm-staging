import {
  mockAutomatedSuiteDataRow,
  mockExecutionDataRow,
  mockExecutions,
  mockTestPlan,
  navigateToIteration,
} from '../../../../data-mock/iteration-automated-suite.data-mock';
import { AutomatedSuiteDetailsDialogElement } from '../../../../page-objects/pages/campaign-workspace/dialogs/automated-suite-details-dialog.element';
import { AttachmentListModel } from '../../../../../../projects/sqtm-core/src/lib/model/attachment/attachment-list.model';
import { FailureDetail } from '../../../../../../projects/sqtm-core/src/lib/model/execution/failure-detail.model';
import { AutomatedExecutionFailureDetailsElement } from '../../../../page-objects/elements/automated-execution-failure-details/automated-execution-failure-details.element';

describe('Iteration - automated suite details', function () {
  it('should display executions statuses and attachments if community', () => {
    const automatedSuiteDetailsDialog = openAutomatedSuiteDetailsDialog(false);
    automatedSuiteDetailsDialog.assertTitleHasText('Détails des exécutions');
    automatedSuiteDetailsDialog.assertCloseButtonExists();
    automatedSuiteDetailsDialog.assertRowHasExecutionStatus(-6, 'FAILURE');
    automatedSuiteDetailsDialog.assertRowHasExecutionStatus(-9, 'BLOCKED');
    automatedSuiteDetailsDialog.assertRowHasExecutionStatus(-15, 'SUCCESS');

    const attachmentBody: AttachmentListModel = {
      id: -89,
      attachments: [
        {
          id: -45,
          name: 'output.xml',
          size: 458,
          addedOn: null,
          lastModifiedOn: null,
        },
      ],
    };

    automatedSuiteDetailsDialog.displayExecutionAttachmentsAndFailureDetail(
      -6,
      null,
      attachmentBody,
      null,
    );

    automatedSuiteDetailsDialog.assertAttachmentsListIsDisplayed();
    automatedSuiteDetailsDialog.assertAttachmentIsDisplayed('output.xml');
    const executionFailureDetails = new AutomatedExecutionFailureDetailsElement();
    executionFailureDetails.assertSectionIsNotDisplayed();
  });

  it('should not display attachments if no attachment', () => {
    const automatedSuiteDetailsDialog = openAutomatedSuiteDetailsDialog(false);
    const attachmentBody: AttachmentListModel = {
      id: -89,
      attachments: [],
    };

    automatedSuiteDetailsDialog.displayExecutionAttachmentsAndFailureDetail(
      -6,
      null,
      attachmentBody,
      null,
    );

    automatedSuiteDetailsDialog.assertAttachmentsListIsDisplayed();
    automatedSuiteDetailsDialog.assertNoAttachmentLabelIsDisplayed();
  });

  it('should display executions failure details if premium', () => {
    const automatedSuiteDetailsDialog = openAutomatedSuiteDetailsDialog(true);

    const attachmentBody: AttachmentListModel = {
      id: -89,
      attachments: [
        {
          id: -45,
          name: 'output.xml',
          size: 458,
          addedOn: null,
          lastModifiedOn: null,
        },
      ],
    };
    const failureDetailBody: FailureDetail[] = [
      {
        id: -232,
        message: 'The test has failed : 2 != 3',
      },
      {
        id: -458,
        message: 'Could not find method: Unknown method',
      },
    ];

    automatedSuiteDetailsDialog.displayExecutionAttachmentsAndFailureDetail(
      -6,
      -6,
      attachmentBody,
      failureDetailBody,
    );

    const executionFailureDetails = new AutomatedExecutionFailureDetailsElement();

    executionFailureDetails.assertTitleIsDisplayed();
    executionFailureDetails.assertFailureDetailIsDisplayed('2', 'The test has failed : 2 != 3');
    executionFailureDetails.assertFailureDetailIsDisplayed(
      '1',
      'Could not find method: Unknown method',
    );
  });

  it('should not display executions failure details if no failure details', () => {
    const automatedSuiteDetailsDialog = openAutomatedSuiteDetailsDialog(true);

    const attachmentBody: AttachmentListModel = {
      id: -89,
      attachments: [
        {
          id: -45,
          name: 'output.xml',
          size: 458,
          addedOn: null,
          lastModifiedOn: null,
        },
      ],
    };
    const failureDetailBody: FailureDetail[] = [];

    automatedSuiteDetailsDialog.displayExecutionAttachmentsAndFailureDetail(
      -15,
      -12,
      attachmentBody,
      failureDetailBody,
    );

    const executionFailureDetails = new AutomatedExecutionFailureDetailsElement();

    executionFailureDetails.assertSectionIsNotDisplayed();
  });

  function openAutomatedSuiteDetailsDialog(
    isPremium: boolean = false,
  ): AutomatedSuiteDetailsDialogElement {
    const iterationViewPage = navigateToIteration([], isPremium);
    const today = new Date();
    const automatedSuiteId = 'Auto-Suite-1';
    const testPlan = mockTestPlan([
      mockAutomatedSuiteDataRow(automatedSuiteId, today, {
        hasExecution: true,
        hasResultUrl: true,
        executionStatus: 'FAILURE',
      }),
    ]);
    const automatedSuitePage = iterationViewPage.clickAutomatedSuitePageAnchorLink(testPlan);

    const row = automatedSuitePage.testPlan.getRow(automatedSuiteId);

    const dataRows = [
      mockExecutionDataRow(-6, -6, 'FAILURE', {
        environmentVariables: 'PRESTA_BROWSER_TYPE : chrome',
        environmentTags: 'Robot Framework',
        name: 'CART_TC_001 - Add one product to the cart',
        lastExecutedOn: '2024-11-22T07:59:49.994+00:00',
        reference: 'CART_TC_001',
        datasetLabel: null,
        duration: 11851,
      }),
      mockExecutionDataRow(-9, -7, 'BLOCKED', {
        name: 'CART_TC_002 - Add two products to the cart',
        lastExecutedOn: '2024-11-22T07:59:49.994+00:00',
      }),
      mockExecutionDataRow(-15, -12, 'SUCCESS'),
    ];

    const executions = mockExecutions(dataRows);
    return automatedSuitePage.openAutomatedSuiteDetails(row, automatedSuiteId, executions);
  }
});
