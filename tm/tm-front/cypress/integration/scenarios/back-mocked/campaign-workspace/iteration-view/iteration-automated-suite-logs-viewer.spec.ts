import {
  mockAutomatedSuiteDataRow,
  mockTestPlan,
  navigateToIteration,
} from '../../../../data-mock/iteration-automated-suite.data-mock';
import {
  AutomatedSuiteWorkflowDto,
  SquashOrchestratorWorkflowLogViewerElement,
} from '../../../../page-objects/pages/campaign-workspace/dialogs/squash-orchestrator-workflow-log-viewer.element';

describe('Iteration - automated suite details', function () {
  it('should not display workflow logs when premium plugin is not installed', () => {
    const iterationViewPage = navigateToIteration([]);
    const today = new Date();
    const testPlan = mockTestPlan([
      mockAutomatedSuiteDataRow('az47', today, {
        hasExecution: true,
        hasResultUrl: true,
        executionStatus: 'RUNNING',
        workflows: [
          {
            workflowId: 'bbf0e188-3da6-4db0-8743-da208935b3e6',
            projectId: 45,
          },
        ],
      }),
    ]);
    const automatedSuitePage = iterationViewPage.clickAutomatedSuitePageAnchorLink(testPlan);
    automatedSuitePage.assertWorkflowsLogButtonDoesNotExist();
  });

  it('workflow logs viewer button should be disabled if automated suite is not RUNNING', () => {
    const iterationViewPage = navigateToIteration([], true);
    const today = new Date();
    const testPlan = mockTestPlan([
      mockAutomatedSuiteDataRow('az47', today, {
        hasExecution: true,
        hasResultUrl: true,
        executionStatus: 'SUCCESS',
        workflows: [
          {
            workflowId: 'bbf0e188-3da6-4db0-8743-da208935b3e6',
            projectId: 45,
          },
        ],
      }),
    ]);
    const automatedSuitePage = iterationViewPage.clickAutomatedSuitePageAnchorLink(testPlan);
    automatedSuitePage.assertWorkflowsLogButtonIsDisabled();
  });

  it('should display multi workflows menu for automated suite with multiple workflows', () => {
    const workflows = [
      {
        workflowId: 'bbf0e188-3da6-4db0-8743-da208935b3e6',
        projectId: 45,
      },
      {
        workflowId: '70048b5b-1399-42d0-b7e9-75de82d5a79d',
        projectId: 54,
      },
      {
        workflowId: '2ad6b8ef-bce3-498e-bef2-ba9cf083f913',
        projectId: 82,
      },
    ];
    openWorkflowLogDialog(true, true, workflows);

    const workflowLogsDialog = new SquashOrchestratorWorkflowLogViewerElement(
      'workflow-logs-dialog',
      'Suivi du Workflow (En cours)',
      'I am a workflow log',
      null,
    );
    workflowLogsDialog.verifyDialogContent();
    workflowLogsDialog.close();
  });

  it('should display workflow logs when premium plugin is installed', () => {
    const workflows = [
      {
        workflowId: 'bbf0e188-3da6-4db0-8743-da208935b3e6',
        projectId: 45,
      },
    ];
    openWorkflowLogDialog(true, true, workflows);
    const workflowLogsDialog = new SquashOrchestratorWorkflowLogViewerElement(
      'workflow-logs-dialog',
      'Suivi du Workflow (En cours)',
      'I am a workflow log',
      null,
    );
    workflowLogsDialog.verifyDialogContent();
    workflowLogsDialog.close();
  });

  it('should display information dialog if workflow is already done', () => {
    const workflows = [
      {
        workflowId: '2ad6b8ef-bce3-498e-bef2-ba9cf083f913',
        projectId: 45,
      },
    ];
    openWorkflowLogDialog(true, false, workflows);
    const workflowLogsDialog = new SquashOrchestratorWorkflowLogViewerElement(
      'workflow-logs-already-done',
      'Suivi du Workflow',
      null,
      null,
    );
    workflowLogsDialog.verifyDialogContent();
    workflowLogsDialog.assertHasMessage('Le workflow est terminé');
    workflowLogsDialog.close();
  });

  it('should display error dialog if squash orchestrator is unreachable before opening dialog', () => {
    const workflows = [
      {
        workflowId: '70048b5b-1399-42d0-b7e9-75de82d5a79d',
        projectId: 45,
      },
    ];
    openWorkflowLogDialog(false, true, workflows);

    const workflowLogsDialog = new SquashOrchestratorWorkflowLogViewerElement(
      'workflow-logs-error',
      'Suivi du Workflow',
      null,
      null,
    );
    workflowLogsDialog.verifyDialogContent();
    workflowLogsDialog.assertHasMessage(
      `Impossible de récupérer les logs du workflow.\nVeuillez vérifier que Squash Orchestrator est toujours joignable et que le workflow existe.\nVous pouvez également accéder aux logs du workflow directement depuis le serveur Squash Orchestrator en utilisant opentf-ctl`,
    );
    workflowLogsDialog.close();
  });

  function openWorkflowLogDialog(
    orchestratorIsReachable: boolean,
    workflowIsStoredInDatabase: boolean,
    workflows: AutomatedSuiteWorkflowDto[],
  ): void {
    const iterationViewPage = navigateToIteration([], true);
    const today = new Date();
    const testPlan = mockTestPlan([
      mockAutomatedSuiteDataRow('az47', today, {
        hasExecution: true,
        hasResultUrl: true,
        executionStatus: 'RUNNING',
        workflows: workflows,
      }),
    ]);
    const automatedSuitePage = iterationViewPage.clickAutomatedSuitePageAnchorLink(testPlan);
    automatedSuitePage.assertWorkflowsLogButtonIsEnabled();

    const row = automatedSuitePage.testPlan.getRow('az47', 'rightViewport');

    const workflowsUuids: string[] = workflows.map((workflow) => workflow.workflowId);

    if (workflowsUuids.length > 1) {
      automatedSuitePage.openWorkflowsMenu(row);
      automatedSuitePage.openViewerForWorkflow(workflowsUuids[0], {
        logs: 'I am a workflow log',
        reachable: orchestratorIsReachable,
        isStoredInDatabase: workflowIsStoredInDatabase,
      });
    } else {
      automatedSuitePage.openWorkflowLogViewer(row, {
        logs: 'I am a workflow log',
        reachable: orchestratorIsReachable,
        isStoredInDatabase: workflowIsStoredInDatabase,
      });
    }
  }
});
