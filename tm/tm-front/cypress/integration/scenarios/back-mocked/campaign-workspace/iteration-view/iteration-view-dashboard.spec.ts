import { IterationViewPage } from '../../../../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import {
  getDefaultIterationStatisticsBundle,
  getEmptyExecutionEnvironmentsCount,
  getEmptyIterationStatisticsBundle,
  mockIterationModel,
} from '../../../../data-mock/iteration.data-mock';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { IterationModel } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/iteration-model';
import { StatisticsBundle } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/campaign-model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

describe('Iteration - Statistics', () => {
  it('should display iteration dashboard stats charts', () => {
    const iterationViewPage = navigateToIteration();
    const dashboard = iterationViewPage.clickDashboardAnchorLink();
    dashboard.assertExists();
    dashboard.assertStatsChartAreRendered();
    dashboard.assertStatusChartContainsStatus('Succès');
    dashboard.assertStatusChartContainsStatus('Échec');
    dashboard.assertStatusChartContainsStatus('À exécuter');
    dashboard.assertStatusChartContainsStatus('En cours');
    dashboard.assertStatusChartContainsStatus('Annulé');
    dashboard.assertStatusChartContainsStatus('Bloqué');
    dashboard.assertStatusChartDoesNotContainStatus('Ignoré');
    dashboard.assertAdvancementChartIsRendered();
  });

  it('should display empty chart warning', () => {
    const iterationViewPage: IterationViewPage = navigateToIteration(
      getEmptyIterationStatisticsBundle(),
    );
    const dashboard = iterationViewPage.clickDashboardAnchorLink();
    dashboard.assertExists();
    dashboard.assertErrorMessageOnDateIsVisible();
    dashboard.assertErrorMessageOnEmptyTestPlanIsVisible();
  });

  it('should display test suite inventory', () => {
    const iterationViewPage = navigateToIteration();
    const dashboard = iterationViewPage.clickDashboardAnchorLink();
    dashboard.assertExists();
    dashboard.assertInventoryTableExist();
    dashboard.assertTestSuiteRowHasName(0, 'testSuite 1');
    dashboard.assertTestSuiteRowHasName(1, 'Sans suite');
    dashboard.assertTestSuiteRowHasName(2, 'Total');
    dashboard.checkStatusCount(2, {
      READY: 24,
      RUNNING: 3,
      SUCCESS: 5,
      SETTLED: 4,
      FAILURE: 2,
      BLOCKED: 9,
      UNTESTABLE: 2,
    });
    dashboard.checkImportanceCount(2, {
      VERY_HIGH: 6,
      HIGH: 7,
      MEDIUM: 8,
      LOW: 6,
    });
    dashboard.checkCell(2, 'nb-total', '34');
    dashboard.checkCell(2, 'nb-to-execute', '14');
    dashboard.checkCell(2, 'nb-executed', '20');
  });

  function navigateToIteration(
    iterationStatisticsBundle: StatisticsBundle = getDefaultIterationStatisticsBundle(),
  ): IterationViewPage {
    const initialNodes: GridResponse = mockGridResponse('id', [
      mockTreeNode({
        id: 'CampaignLibrary-1',
        children: ['Campaign-3'],
        data: { NAME: 'Project1', CHILD_COUNT: 1 },
        state: DataRowOpenState.open,
      }),
      mockTreeNode({
        id: 'Campaign-3',
        children: ['Iteration-1'],
        projectId: 1,
        parentRowId: 'CampaignLibrary-1',
        state: DataRowOpenState.open,
        data: { NAME: 'campaign3', CHILD_COUNT: 1 },
      }),
      mockTreeNode({
        id: 'Iteration-1',
        children: [],
        projectId: 1,
        parentRowId: 'Campaign-3',
        data: { NAME: 'iteration-1', CHILD_COUNT: 0 },
      }),
    ]);
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes);

    const model: IterationModel = mockIterationModel({
      testPlanStatistics: {
        status: 'DONE',
        progression: 100,
        nbTestCases: 3,
        nbDone: 3,
        nbReady: 0,
        nbRunning: 0,
        nbUntestable: 0,
        nbBlocked: 0,
        nbFailure: 0,
        nbSettled: 0,
        nbSuccess: 0,
      },
    });

    new HttpMockBuilder(`iteration-view/${model.id}/statistics`)
      .post()
      .responseBody(iterationStatisticsBundle)
      .build();

    new HttpMockBuilder(
      `test-automation/iteration/*/automated-execution-environments-statuses-count`,
    )
      .get()
      .responseBody(getEmptyExecutionEnvironmentsCount())
      .build();

    return campaignWorkspacePage.tree.selectNode<IterationViewPage>('Iteration-1', model);
  }
});
