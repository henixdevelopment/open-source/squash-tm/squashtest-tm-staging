import { mockDataRow, mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';
import {
  getEmptyExecutionEnvironmentsCount,
  getEmptyIterationStatisticsBundle,
  mockIterationModel,
} from '../../../../data-mock/iteration.data-mock';
import {
  initialTestCaseLibraries,
  projectOneChildren,
} from '../../../../data-mock/test-case-simple-tree';
import { EnvironmentSelectionPanelDto } from '../../../../page-objects/elements/automated-execution-environments/environment-selection-panel.element';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { IterationViewPage } from '../../../../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import {
  ALL_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder,
} from '../../../../utils/referential/referential-data-builder';
import {
  mockExecutionModel,
  mockExecutionStepModel,
} from '../../../../data-mock/execution.data-mock';
import {
  ExecutionModel,
  ExecutionSummaryDto,
} from '../../../../../../projects/sqtm-core/src/lib/model/execution/execution.model';
import {
  EntityReference,
  EntityType,
  Identifier,
} from '../../../../../../projects/sqtm-core/src/lib/model/entity.model';
import { IterationModel } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/iteration-model';
import { SquashAutomProjectPreview } from '../../../../../../projects/sqtm-core/src/lib/model/test-automation/automated-suite-preview.model';
import { AutomatedSuiteOverview } from '../../../../../../projects/sqtm-core/src/lib/model/test-automation/automated-suite-overview.model';
import { ExecutionStatusKeys } from '../../../../../../projects/sqtm-core/src/lib/model/level-enums/level-enum';
import {
  ExpectedAutomatedSuitePreview,
  ExpectedTestAutomationProjectPreview,
} from '../../../../model/test-automation/automated-suite-preview.model';
import {
  DataRowModel,
  DataRowOpenState,
} from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';

describe('Iteration View', function () {
  it('should display a simple test plan', () => {
    const iterationViewPage = navigateToIteration();

    const testPlan = mockTestPlan([mockITPIDataRow('1'), mockITPIDataRow('2')]);

    const testPlanPage = iterationViewPage.showTestPlan(testPlan);
    testPlanPage.testPlan.assertRowExist('1');
    testPlanPage.testPlan.assertRowExist('2');

    // should display and filter a test plan
    testPlanPage.testPlan.declareRefreshData(mockTestPlan([testPlan.dataRows[0]]));
    testPlanPage.testPlan.filterByTextColumn(
      'testCaseName',
      'value',
      mockTestPlan([testPlan.dataRows[0]]),
    );

    testPlanPage.testPlan.assertRowExist('1');
    testPlanPage.testPlan.assertRowNotExist('2');
  });

  it('should add and remove test cases to iteration', () => {
    const iterationViewPage = navigateToIteration();

    const testPlan = mockTestPlan([]);

    const testPlanPage = iterationViewPage.showTestPlan(testPlan);
    new NavBarElement().toggle();
    iterationViewPage.toggleTree();

    const testCaseTreePicker = testPlanPage.openTestCaseDrawer(initialTestCaseLibraries);
    testCaseTreePicker.openNode('TestCaseLibrary-1', projectOneChildren);
    testCaseTreePicker.beginDragAndDrop('TestCase-3');

    // We add as many rows as we need to delete them later. We don't care about realism :)
    const fullTestPlan = [mockITPIDataRow(1), mockITPIDataRow(2), mockITPIDataRow(3)];

    testPlanPage.enterIntoTestPlan();
    testPlanPage.assertColoredBorderIsVisible();
    testPlanPage.dropIntoTestPlan(1, [1], mockTestPlan(fullTestPlan));
    testPlanPage.verifyTestPlanItem({ id: 1, name: 'Test-Case 1', showsAsLink: true });
    testPlanPage.assertColoredBorderIsNotVisible();
    testPlanPage.closeTestCaseDrawer();

    // should remove a single test case from iteration
    const confirmDialogComponent = testPlanPage.showDeleteConfirmDialog(1, 1);

    confirmDialogComponent.assertExists();
    confirmDialogComponent.deleteForSuccess(mockTestPlan(fullTestPlan.slice(1)));
    confirmDialogComponent.assertNotExist();

    // should mass remove test cases from iteration
    testPlanPage.testPlan.selectRowsWithStickyIndexColumn([2, 3]);
    const confirmMultiDialogComponent = testPlanPage.showMassDeleteConfirmDialog(1, [2, 3]);
    confirmMultiDialogComponent.assertExists();
    confirmMultiDialogComponent.deleteForSuccess(mockTestPlan([]));
    confirmMultiDialogComponent.assertNotExist();
  });

  it('should display last executions statuses', () => {
    const iterationViewPage = navigateToIteration();

    const summaryTab: ExecutionSummaryDto[] = [
      {
        executionId: 1,
        executionStatus: 'NOT_FOUND',
        executedOn: '2021-01-19T15:39:12.000+0000',
      },
      {
        executionId: 2,
        executionStatus: 'WARNING',
        executedOn: '2021-01-19T15:39:12.000+0000',
      },
      {
        executionId: 3,
        executionStatus: 'WARNING',
        executedOn: '2021-01-19T15:39:12.000+0000',
      },
      {
        executionId: 4,
        executionStatus: 'NOT_RUN',
        executedOn: '2021-01-19T15:39:12.000+0000',
      },
      {
        executionId: 5,
        executionStatus: 'SUCCESS',
        executedOn: '2021-01-19T15:39:12.000+0000',
      },
    ];

    const testPlan = mockTestPlan([
      mockITPIDataRow(1, {
        itemTestPlanId: 1,
        testCaseName: 'Test-Case 1',
        projectId: 1,
        iterationId: 1,
        lastExecutedOn: null,
        executionStatus: 'RUNNING',
        [GridColumnId.assigneeFullName]: 'gilbert',
        lastExecutionStatuses: summaryTab,
      }),
    ]);
    const testPlanPage = iterationViewPage.showTestPlan(testPlan);

    testPlanPage.checkExecutionsSummary(testPlan);
  });

  it('should allow to update data from grid', () => {
    const iterationViewPage = navigateToIteration();

    const testPlan = mockTestPlan([
      mockITPIDataRow(1, {
        itemTestPlanId: 1,
        testCaseName: 'Test-Case 1',
        projectId: 1,
        iterationId: 1,
        lastExecutedOn: null,
        executionStatus: 'RUNNING',
        [GridColumnId.assigneeFullName]: 'gilbert',
        hasExecutions: false,
        datasetName: 'JDD',
        availableDatasets: [
          { id: 1, name: 'JDD' },
          { id: 2, name: 'Dataset' },
        ],
      }),
    ]);
    const testPlanPage = iterationViewPage.showTestPlan(testPlan);
    testPlanPage.foldGrid();

    // Apply fast pass
    cy.get(`[data-test-cell-id=execution-status-cell]`).should(
      'have.css',
      'background-color',
      'rgb(0, 120, 174)',
    );
    testPlanPage.applyFastpassOnItpi('SUCCESS');
    cy.get(`[data-test-cell-id=execution-status-cell]`).should(
      'have.css',
      'background-color',
      'rgb(0, 111, 87)',
    );

    // should update dataset
    cy.get(`[data-test-cell-id=dataset-cell]`).should('contain.text', 'JDD');
    testPlanPage.changeDataset('item-2');
    cy.get(`[data-test-cell-id=dataset-cell]`).should('contain.text', 'Dataset');

    // should update assigned user
    const row = testPlanPage.testPlan.getRow(1);
    const assignedUserCell = row.cell(GridColumnId.assigneeFullName).selectRenderer();
    assignedUserCell.changeValue('item-2', 'test-plan-item/*/assign-user');
    assignedUserCell.assertContainText('Joe Ni (jawny)');
  });

  it('should create execution in page mode', () => {
    const iterationViewPage = navigateToIteration();

    const testPlan = mockTestPlan([mockITPIDataRow(1)]);

    const testPlanPage = iterationViewPage.showTestPlan(testPlan);
    testPlanPage.clickOnPlayButton(1);
    testPlanPage.checkPlayMenu();
    const executionModel: ExecutionModel = getExecutionModel();
    const executionPage = testPlanPage.launchExecutionInPageMode(
      1,
      1,
      1,
      undefined,
      executionModel,
    );
    executionPage.checkName('REF4 - Test Case');
  });

  it('should create an item automated execution in dialog', () => {
    const iterationViewPage = navigateToIteration();
    const testPlan = mockTestPlan([
      mockITPIDataRow(1, { [GridColumnId.inferredExecutionMode]: 'AUTOMATED' }),
    ]);
    const testPlanPage = iterationViewPage.showTestPlan(testPlan);
    testPlanPage.foldGrid();
    testPlanPage.clickOnPlayButton(1);
    testPlanPage.checkPlayMenu(4);
    const automatedExecutionDialog = testPlanPage.openAutoTestExecDialogViaLaunchAllButton({
      manualServerSelection: false,
      specification: {
        context: new EntityReference(1, EntityType.ITERATION),
        testPlanSubsetIds: [],
      },
      projects: [],
      squashAutomProjects: [],
    });
    automatedExecutionDialog.assertExists();
  });

  it('should open execution history of an itpi', () => {
    const iterationViewPage = navigateToIteration();

    const testPlan = mockTestPlan([mockITPIDataRow(1)]);
    const testPlanExecution = {
      count: 3,
      idAttribute: null,
      dataRows: [
        {
          index: 1,
          id: '48',
          data: {
            executionOrder: 2,
            executionReference: '',
            successRate: 0,
            importance: 'HIGH',
            executionStatus: 'READY',
            datasetName: 'JDD',
            [GridColumnId.inferredExecutionMode]: 'MANUAL',
            itemTestPlanId: 5,
            issueCount: 0,
            executionId: 48,
            executionName: 'TC2',
            lastExecutedOn: null,
            iterationId: 1,
            projectId: 1,
            [GridColumnId.assigneeFullName]: 'admin',
          },
          projectId: 1,
        },
        {
          index: 2,
          id: '35',
          data: {
            executionOrder: 1,
            executionReference: '',
            successRate: 0,
            importance: 'HIGH',
            executionStatus: 'BLOCKED',
            datasetName: 'JDD',
            [GridColumnId.inferredExecutionMode]: 'MANUAL',
            itemTestPlanId: 5,
            issueCount: 1,
            executionId: 35,
            executionName: 'TC2',
            lastExecutedOn: '2021-01-19T15:39:12.000+0000',
            iterationId: 1,
            projectId: 1,
            [GridColumnId.assigneeFullName]: 'admin',
          },
          projectId: 1,
        },
        {
          index: 3,
          id: '14',
          data: {
            executionOrder: 0,
            executionReference: '',
            successRate: 0,
            importance: 'HIGH',
            executionStatus: 'BLOCKED',
            datasetName: 'JDD',
            [GridColumnId.inferredExecutionMode]: 'MANUAL',
            itemTestPlanId: 5,
            issueCount: 2,
            executionId: 14,
            executionName: 'TC2',
            lastExecutedOn: '2021-01-19T18:19:42.000+0000',
            iterationId: 1,
            projectId: 1,
            [GridColumnId.assigneeFullName]: 'admin',
          },
          projectId: 1,
        },
      ],
    };

    const testPlanExecutionAfterMultiDeletion = {
      count: 1,
      idAttribute: null,
      dataRows: [
        {
          index: 1,
          id: '48',
          data: {
            executionOrder: 2,
            executionReference: '',
            successRate: 0,
            importance: 'HIGH',
            executionStatus: 'READY',
            datasetName: 'JDD',
            [GridColumnId.inferredExecutionMode]: 'MANUAL',
            itemTestPlanId: 5,
            issueCount: 0,
            executionId: 48,
            executionName: 'TC2',
            lastExecutedOn: null,
            iterationId: 1,
            projectId: 1,
            [GridColumnId.assigneeFullName]: 'admin',
          },
          projectId: 1,
        },
      ],
    };

    const testPlanExecutionAfterSingleDeletion = {
      count: 0,
      idAttribute: null,
      dataRows: [],
    };

    const testPlanPage = iterationViewPage.showTestPlan(testPlan);
    testPlanPage.foldGrid();
    testPlanPage.clickOnPlayButton(1);
    testPlanPage.checkPlayMenu();
    const executionDialog = testPlanPage.openAnyExecutionHistoryDialog(testPlanExecution);
    executionDialog.checkFirstIdInExecutionHistoryDialog(2, 48);

    executionDialog.deleteMultiSelectedExecution([35, 14], testPlanExecutionAfterMultiDeletion);
    executionDialog.gridElement.assertRowCount(1);

    executionDialog.deleteSelectedExecution(48, 'iteration', testPlanExecutionAfterSingleDeletion);
    executionDialog.gridElement.assertRowCount(0);
  });

  it('should reorder items with drag and drop', () => {
    const iterationViewPage = navigateToIteration();
    new NavBarElement().toggle();
    iterationViewPage.toggleTree();

    const testPlan = mockGridResponse('itemTestPlanId', [
      mockITPIDataRow(1),
      mockITPIDataRow(2),
      mockITPIDataRow(3),
      mockITPIDataRow(4),
    ]);

    const testPlanPage = iterationViewPage.showTestPlan(testPlan);
    testPlanPage.testPlan.assertRowCount(4);
    testPlanPage.moveItemWithServerResponse('1', '3', {});

    // should forbid items reordering if grid is filtered
    testPlanPage.testPlan.filterByTextColumn(GridColumnId.projectName, 'P', testPlan);
    testPlanPage.assertCannotMoveItem('1');
  });

  it('should mass edit ITPIs', () => {
    const iterationViewPage = navigateToIteration();
    new NavBarElement().toggle();
    iterationViewPage.toggleTree();

    const dataRows = [
      mockITPIDataRow(1),
      mockITPIDataRow(2),
      mockITPIDataRow(3),
      mockITPIDataRow(4),
    ];
    const testPlan = mockGridResponse('itemTestPlanId', dataRows);

    const testPlanPage = iterationViewPage.showTestPlan(testPlan);
    testPlanPage.testPlan.selectRowsWithStickyIndexColumn([1, 2]);

    const massEditDialog = testPlanPage.openMassEditDialog();
    massEditDialog.assertExists();

    massEditDialog.toggleTestSuiteEdition();
    massEditDialog.selectTestSuites(['suite01']);
    massEditDialog.toggleAssigneeEdition();
    massEditDialog.selectAssignee('Joe Ni (jawny)');
    massEditDialog.toggleStatusEdition();
    massEditDialog.selectStatus('En cours');

    massEditDialog.confirm(dataRows);
  });

  it('should create a test suite', () => {
    const iterationViewPage = navigateToIteration();
    new NavBarElement().toggle();
    iterationViewPage.toggleTree();

    const dataRows = [
      mockITPIDataRow(1),
      mockITPIDataRow(2),
      mockITPIDataRow(3),
      mockITPIDataRow(4),
    ];
    const testPlan = mockGridResponse('itemTestPlanId', dataRows);

    const testPlanPage = iterationViewPage.showTestPlan(testPlan);
    testPlanPage.testPlan.selectRowsWithStickyIndexColumn([1]);

    const createTestSuiteDialog = testPlanPage.openCreateTestSuiteDialog();
    createTestSuiteDialog.assertExists();

    createTestSuiteDialog.nameField.fill('TS01');
    createTestSuiteDialog.descriptionField.fill('test suite 01');

    const addedTS = mockDataRow({ data: { ID: 1 } });
    createTestSuiteDialog.confirm(addedTS);
  });

  it('should not allow to create execution if TC was deleted', () => {
    const iterationViewPage = navigateToIteration();
    const deletedTestCaseDataRow = mockITPIDataRow(1, {
      projectName: null,
      testCaseId: null,
      testCaseName: null,
    });
    const testPlan = mockTestPlan([deletedTestCaseDataRow]);
    const testPlanPage = iterationViewPage.showTestPlan(testPlan);

    testPlanPage.testPlan
      .getRow('1')
      .cell('testCaseName')
      .textRenderer()
      .assertContainsText('(supprimé)');

    testPlanPage.clickOnPlayButton(1);

    testPlanPage.checkPlayMenuDontAllowNewExecution();
  });

  it('should refuse to launch execution plan on error', () => {
    const iterationViewPage = navigateToIteration();
    new NavBarElement().toggle();
    iterationViewPage.toggleTree();

    const dataRows = [
      mockITPIDataRow(1),
      mockITPIDataRow(2),
      mockITPIDataRow(3),
      mockITPIDataRow(4),
    ];
    const testPlan = mockGridResponse('itemTestPlanId', dataRows);

    const testPlanPage = iterationViewPage.showTestPlan(testPlan);
    const resumeAlertDialog = testPlanPage.launchExecutionWithError('1');
    resumeAlertDialog.assertExists();
    resumeAlertDialog.assertHasMessage(
      "L'itération ne peut pas être exécutée parce que le plan de test est vide.",
    );
  });

  it('should launch automated tests', () => {
    const iterationViewPage = navigateToIteration();
    const testPlan = mockTestPlan([
      mockITPIDataRow(1, {
        testCaseName: 'TestCase_OK',
        [GridColumnId.inferredExecutionMode]: 'AUTOMATED',
      }),
      mockITPIDataRow(2, {
        testCaseName: 'TestCase_KO',
        [GridColumnId.inferredExecutionMode]: 'MANUAL',
      }),
      mockITPIDataRow(3, {
        testCaseName: 'TestCase_BLOCKED',
        [GridColumnId.inferredExecutionMode]: 'MANUAL',
      }),
      mockITPIDataRow(4, {
        testCaseName: 'TestCase_Connection',
        [GridColumnId.inferredExecutionMode]: 'AUTOMATED',
      }),
      mockITPIDataRow(5, {
        testCaseName: 'Test_SquashAutom_Login',
        [GridColumnId.inferredExecutionMode]: 'AUTOMATED',
      }),
      mockITPIDataRow(6, {
        testCaseName: 'Test_SquashAutom_Welcome',
        [GridColumnId.inferredExecutionMode]: 'AUTOMATED',
      }),
      mockITPIDataRow(7, {
        testCaseName: 'Test_SquashAutom_Welcome',
        [GridColumnId.inferredExecutionMode]: 'AUTOMATED',
      }),
    ]);
    const testPlanPage = iterationViewPage.showTestPlan(testPlan);
    testPlanPage.assertLaunchAutomatedMenuExist();
    const executionDialog = testPlanPage.openAutoTestExecDialogViaLaunchAllButton(
      mockAutomatedSuitePreview(),
      [mockEnvironmentPanelResponse()],
    );
    executionDialog.displayEnvironments(2);

    executionDialog.addEnvironmentTag(2, 'ssh');

    executionDialog.checkAgentPhaseContentForSquashAutomProjects(mockSquashAutomProjects());
    executionDialog.checkAgentPhaseContentForJenkinsProjects(mockAutomationProjects());
    executionDialog.changeAgent(4, 'Jenkins');
    executionDialog.changeAgent(7, 'Agent5');
    executionDialog.confirmAgentPhase(mockAutomatedSuiteOverviewStart());
    executionDialog.checkExecutionPhaseStaticContent();
    executionDialog.checkExecutionPhaseDynamicContentAtStart(mockAutomatedSuiteOverviewStart());
    executionDialog.closeBeforeEnd(mockAutomatedSuiteOverviewStart());
    executionDialog.checkWarningPhaseContent();
    executionDialog.clickOnCancelButton();
    executionDialog.checkExecutionPhaseDynamicContentProceeding(mockAutomatedSuiteOverviewEnd());
    executionDialog.close();
  });

  it('should reach execution phase with only jenkins automated tests', () => {
    const iterationViewPage = navigateToIteration();
    const testPlan = mockTestPlan([
      mockITPIDataRow(1, {
        testCaseName: 'TestCase_1',
        [GridColumnId.inferredExecutionMode]: 'AUTOMATED',
      }),
      mockITPIDataRow(2, {
        testCaseName: 'TestCase_2',
        [GridColumnId.inferredExecutionMode]: 'AUTOMATED',
      }),
    ]);
    const testPlanPage = iterationViewPage.showTestPlan(testPlan);
    const executionDialog = testPlanPage.openAutoTestExecDialogViaLaunchAllButton(
      mockAutomatedSuitePreviewForJenkinsScenario(),
    );
    testPlanPage.assertLaunchAutomatedMenuIsNotVisible();
    executionDialog.checkAgentPhaseContentForJenkinsProjects(
      mockAutomationProjectsForJenkinsScenario(),
    );
    executionDialog.confirmAgentPhase(mockAutomatedSuiteOverviewStartForJenkinsScenario());
    executionDialog.checkExecutionPhaseStaticContent(true, false);
  });

  it('should resume a full or a partial test plan according to applied filters', () => {
    const iterationViewPage = navigateToIteration();
    const testPlan = mockTestPlan([mockITPIDataRow('1'), mockITPIDataRow('2')]);
    const testPlanPage = iterationViewPage.showTestPlan(testPlan);

    testPlanPage.resumeExecution();

    testPlanPage.testPlan.filterByTextColumn('testCaseName', '1', testPlan);
    testPlanPage.resumeExecution(true);
  });

  it('should display correct buttons when clicking on play', () => {
    const iterationViewPage = navigateToIteration();
    const testPlan = mockTestPlan([
      mockITPIDataRow('1', { [GridColumnId.inferredExecutionMode]: 'MANUAL' }),
      mockITPIDataRow('2', { [GridColumnId.inferredExecutionMode]: 'AUTOMATED' }),
    ]);

    const testPlanPage = iterationViewPage.showTestPlan(testPlan);
    testPlanPage.checkPlayMenuForManualExecutionMode('1');
    testPlanPage.checkPlayMenuForAutomatedExecutionMode('2');
  });

  it('should display correct title for history execution popup', () => {
    const iterationViewPage = navigateToIteration();
    const testPlan = mockTestPlan([
      mockITPIDataRow('1', { [GridColumnId.inferredExecutionMode]: 'MANUAL' }),
    ]);

    const testPlanPage = iterationViewPage.showTestPlan(testPlan);
    testPlanPage.openPlayMenu('1');
    const anyOtherExecutionHistoryDialog = testPlanPage.openExecutionHistoryDialog();
    anyOtherExecutionHistoryDialog.assertTitleHasText('Historique des exécutions');
  });

  function navigateToIteration(): IterationViewPage {
    const refData = new ReferentialDataMockBuilder()
      .withProjects({
        allowAutomationWorkflow: true,
        permissions: ALL_PROJECT_PERMISSIONS,
      })
      .build();

    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [
        mockTreeNode({
          id: 'CampaignLibrary-1',
          children: ['Campaign-3'],
          data: { NAME: 'Project1', CHILD_COUNT: 1 },
          state: DataRowOpenState.open,
        }),
        mockTreeNode({
          id: 'Campaign-3',
          children: ['Iteration-1'],
          projectId: 1,
          parentRowId: 'CampaignLibrary-1',
          state: DataRowOpenState.open,
          data: { NAME: 'campaign3', CHILD_COUNT: 1 },
        }),
        mockTreeNode({
          id: 'Iteration-1',
          children: [],
          projectId: 1,
          parentRowId: 'Campaign-3',
          data: { NAME: 'iteration-1', CHILD_COUNT: 0 },
        }),
      ],
    };
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes, refData);

    const model: IterationModel = mockIterationModel(buildDefaultIterationCustomData());
    new HttpMockBuilder(`iteration-view/${model.id}/statistics`)
      .post()
      .responseBody(getEmptyIterationStatisticsBundle())
      .build();

    new HttpMockBuilder(
      `test-automation/iteration/*/automated-execution-environments-statuses-count`,
    )
      .get()
      .responseBody(getEmptyExecutionEnvironmentsCount())
      .build();

    const iterationViewPage = campaignWorkspacePage.tree.selectNode<IterationViewPage>(
      'Iteration-1',
      model,
    );

    // Full screen
    new NavBarElement().toggle();
    iterationViewPage.toggleTree();

    return iterationViewPage;
  }
});

function buildDefaultIterationCustomData(): Partial<IterationModel> {
  return {
    id: 1,
    projectId: 1,
    name: 'iteration-1',
    itpi: [],
    iterationStatus: 'PLANNED',
    uuid: 'b368',
    testPlanStatistics: {
      status: 'DONE',
      progression: 100,
      nbTestCases: 3,
      nbDone: 3,
      nbReady: 0,
      nbRunning: 0,
      nbUntestable: 0,
      nbBlocked: 0,
      nbFailure: 0,
      nbSettled: 0,
      nbSuccess: 0,
    },
    hasDatasets: true,
    users: [
      { id: 1, login: 'raowl', firstName: 'Ra', lastName: 'Oul' },
      { id: 2, login: 'jawny', firstName: 'Joe', lastName: 'Ni' },
    ],
    testSuites: [
      { id: 1, name: 'suite01' },
      { id: 2, name: 'suite02' },
    ],
  };
}

function mockITPIDataRow(itemTestPlanId: Identifier, customData?: any): DataRowModel {
  return mockDataRow({
    id: itemTestPlanId.toString(),
    projectId: 1,
    data: {
      itemTestPlanId,
      testCaseName: 'Test-Case ' + itemTestPlanId,
      testCaseId: 1,
      projectId: 1,
      projectName: 'P1',
      iterationId: 1,
      [GridColumnId.inferredExecutionMode]: 'MANUAL',
      ...customData,
    },
    allowMoves: true,
  });
}

function mockTestPlan(dataRows: DataRowModel[]): GridResponse {
  return mockGridResponse('itemTestPlanId', dataRows);
}

function getExecutionModel(): ExecutionModel {
  return mockExecutionModel({
    id: 1,
    projectId: 1,
    executionOrder: 2,
    datasetLabel: '',
    executionsCount: 0,
    testCaseId: 0,
    testPlanItemId: 0,
    name: 'REF4 - Test Case',
    prerequisite: '',
    attachmentList: { id: 1, attachments: [] },
    customFieldValues: [],
    tcImportance: 'LOW',
    tcNatLabel: 'test-case.nature.NAT_BUSINESS_TESTING',
    tcNatIconName: 'noicon',
    tcStatus: 'APPROVED',
    tcTypeLabel: 'test-case.type.TYP_COMPLIANCE_TESTING',
    tcTypeIconName: 'noicon',
    tcDescription: 'description',
    comment: '',
    denormalizedCustomFieldValues: [],
    executionStepViews: [
      mockExecutionStepModel({
        id: 1,
        order: 0,
      }),
      mockExecutionStepModel({
        id: 2,
        order: 1,
      }),
      mockExecutionStepModel({
        id: 3,
        order: 2,
      }),
    ],
    coverages: [],
    executionMode: 'MANUAL',
    lastExecutedOn: null,
    lastExecutedBy: 'admin',
    executionStatus: 'READY',
    automatedJobUrl: null,
    testAutomationServerKind: null,
    automatedExecutionResultUrl: null,
    automatedExecutionResultSummary: null,
    nbIssues: 0,
    iterationId: -1,
    kind: 'STANDARD',
    milestones: [],
  });
}

function mockAutomatedSuitePreview(): ExpectedAutomatedSuitePreview {
  return {
    manualServerSelection: true,
    specification: {
      context: new EntityReference(1, EntityType.ITERATION),
      testPlanSubsetIds: [],
    },
    projects: [
      {
        projectId: 4,
        label: 'Job4',
        server: 'Jenkins',
        nodes: ['Agent1', 'Agent2', 'Agent3'],
        testCount: 3,
        testList: ['Job4/test_ok.ta', 'Job4/test_ko.ta', 'Job4/test_blocked.ta'],
      },
      {
        projectId: 7,
        label: 'Job7',
        server: 'Jenkins',
        nodes: ['Agent1', 'Agent5'],
        testCount: 1,
        testList: ['Job7/connection.feature'],
      },
    ],
    squashAutomProjects: [
      {
        projectId: 2,
        projectName: 'project 2',
        serverId: 9,
        serverName: 'orchestrator',
        testCases: [
          {
            reference: 'LOGIN_1',
            name: 'Test_SquashAutom_Login',
            dataset: 'dataset_1',
            importance: 'LOW',
          },
          {
            reference: 'WELCOME_3',
            name: 'Test_SquashAutom_Welcome',
            dataset: '-',
            importance: 'LOW',
          },
          {
            reference: 'WELCOME_3',
            name: 'Test_SquashAutom_Welcome',
            dataset: '-',
            importance: 'LOW',
          },
        ],
      },
    ],
  };
}

function mockAutomatedSuitePreviewForJenkinsScenario(): ExpectedAutomatedSuitePreview {
  return {
    manualServerSelection: true,
    specification: {
      context: new EntityReference(1, EntityType.ITERATION),
      testPlanSubsetIds: [],
    },
    projects: [
      {
        projectId: 1,
        label: 'Job1',
        server: 'Jenkins',
        nodes: ['Agent1', 'Agent2'],
        testCount: 2,
        testList: ['TestCase_1', 'TestCase_2'],
      },
    ],
    squashAutomProjects: [],
  };
}

function mockEnvironmentPanelResponse(): EnvironmentSelectionPanelDto {
  return {
    environments: {
      environments: [
        {
          name: 'robot-agent',
          tags: ['robotframework', 'ssh'],
          namespaces: [],
          status: 'IDLE',
        },
      ],
    },
    server: {
      testAutomationServerId: 9,
      defaultTags: [],
      hasServerCredentials: true,
    },
    project: {
      projectId: 2,
      projectTags: ['robotframework'],
      areProjectTagsInherited: false,
      hasProjectToken: false,
    },
    errorMessage: null,
  };
}

function mockAutomationProjects(): ExpectedTestAutomationProjectPreview[] {
  return extractProjectPreviewsFromSuitePreview(mockAutomatedSuitePreview());
}

function mockAutomationProjectsForJenkinsScenario(): ExpectedTestAutomationProjectPreview[] {
  return extractProjectPreviewsFromSuitePreview(mockAutomatedSuitePreviewForJenkinsScenario());
}

function extractProjectPreviewsFromSuitePreview(
  automatedSuitePreview: ExpectedAutomatedSuitePreview,
): ExpectedTestAutomationProjectPreview[] {
  return automatedSuitePreview.projects.map(
    (automationProject: ExpectedTestAutomationProjectPreview) => ({
      ...automationProject,
      nodes: [automationProject.server, ...automationProject.nodes, 'Indifférent'],
    }),
  );
}

function mockAutomatedSuiteOverviewStart(): AutomatedSuiteOverview {
  return {
    suiteId: '4028b88178ef5c290178ef6e8347j0g0',
    tfExecutions: [
      {
        id: 4,
        name: 'TestCase_OK',
        status: 'RUNNING',
        node: 'master',
        automatedProject: 'Job4',
      },
      {
        id: 5,
        name: 'TestCase_KO',
        status: 'READY',
        node: 'master',
        automatedProject: 'Job4',
      },
      {
        id: 6,
        name: 'TestCase_BLOCKED',
        status: 'READY',
        node: 'master',
        automatedProject: 'Job4',
      },
      {
        id: 7,
        name: 'TestCase_Connection',
        status: 'READY',
        node: 'Agent5',
        automatedProject: 'Job7',
      },
    ],
    automExecutions: [
      {
        id: 56,
        name: 'Test_SquashAutom_Login',
        status: 'READY',
        datasetLabel: 'dataset_1',
        automatedServerName: 'Orchestrator',
      },
      {
        id: 57,
        name: 'Test_SquashAutom_Welcome',
        status: 'READY',
        datasetLabel: '-',
        automatedServerName: 'Orchestrator',
      },
      {
        id: 58,
        name: 'Test_SquashAutom_Welcome',
        status: 'READY',
        datasetLabel: '-',
        automatedServerName: 'Orchestrator',
      },
    ],
    tfPercentage: 0,
    automTerminatedCount: 0,
    errorMessage: null,
    workflowsUUIDs: null,
  };
}

function mockAutomatedSuiteOverviewStartForJenkinsScenario(): AutomatedSuiteOverview {
  return {
    suiteId: '4028b88178ef5c290178ef6e8347j0g0',
    tfExecutions: [
      {
        id: 4,
        name: 'TestCase_1',
        status: 'RUNNING',
        node: 'Agent1',
        automatedProject: 'Job1',
      },
      {
        id: 5,
        name: 'TestCase_2',
        status: 'RUNNING',
        node: 'Agent1',
        automatedProject: 'Job1',
      },
    ],
    automExecutions: [],
    tfPercentage: 0,
    automTerminatedCount: 0,
    errorMessage: null,
    workflowsUUIDs: null,
  };
}

function mockAutomatedSuiteOverviewEnd(): AutomatedSuiteOverview {
  const endOverview = mockAutomatedSuiteOverviewStart();
  ['BLOCKED', 'SUCCESS', 'FAILURE', 'UNTESTABLE'].forEach((value: ExecutionStatusKeys, index) => {
    endOverview.tfExecutions[index].status = value;
  });
  endOverview.tfPercentage = 100;
  ['BLOCKED', 'SUCCESS', 'FAILURE'].forEach((value: ExecutionStatusKeys, index) => {
    endOverview.automExecutions[index].status = value;
  });
  endOverview.automTerminatedCount = 3;
  return endOverview;
}

function mockSquashAutomProjects(): SquashAutomProjectPreview[] {
  return mockAutomatedSuitePreview().squashAutomProjects;
}
