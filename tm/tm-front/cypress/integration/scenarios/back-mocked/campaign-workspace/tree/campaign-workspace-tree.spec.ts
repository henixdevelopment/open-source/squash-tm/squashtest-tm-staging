import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { createEntityReferentialData } from '../../../../utils/referential/create-entity-referential.const';
import { CampaignViewPage } from '../../../../page-objects/pages/campaign-workspace/campaign/campaign-view.page';
import { CreateEntityDialog } from '../../../../page-objects/pages/create-entity-dialog.element';
import { IterationViewPage } from '../../../../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import { CreateCampaignDialog } from '../../../../page-objects/pages/campaign-workspace/dialogs/create-campaign-dialog.element';
import { TreeElement } from '../../../../page-objects/elements/grid/grid.element';
import { CreateIterationDialog } from '../../../../page-objects/pages/campaign-workspace/dialogs/create-iteration-dialog.element';
import { CreateSuiteDialog } from '../../../../page-objects/pages/campaign-workspace/dialogs/create-suite-dialog.element';
import { TestSuiteViewPage } from '../../../../page-objects/pages/campaign-workspace/test-suite/test-suite-view.page';
import { CreateCampaignFolderDialog } from '../../../../page-objects/pages/campaign-workspace/dialogs/create-campaign-folder-dialog.element';
import { CampaignFolderViewPage } from '../../../../page-objects/pages/campaign-workspace/campaign-folder/campaign-folder.page';
import {
  getEmptyIterationStatisticsBundle,
  mockIterationModel,
  mockTestSuiteModel,
} from '../../../../data-mock/iteration.data-mock';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import {
  mockCampaignFolderModel,
  mockCampaignLibraryModel,
  mockCampaignModel,
} from '../../../../data-mock/campaign.data-mock';
import { CampaignLibraryModel } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/campaign-library/campaign-library.model';
import { CampaignFolderModel } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/campaign-folder/campaign-folder.model';
import { CampaignModel } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/campaign-model';
import { IterationModel } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/iteration-model';
import { TestSuiteModel } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/test-suite.model';
import { Identifier } from '../../../../../../projects/sqtm-core/src/lib/model/entity.model';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { ProjectData } from '../../../../../../projects/sqtm-core/src/lib/model/project/project-data.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import {
  DataRowModel,
  DataRowOpenState,
} from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { CreateSprintDialog } from '../../../../page-objects/pages/campaign-workspace/dialogs/create-sprint-dialog.element';
import { SprintViewPage } from '../../../../page-objects/pages/campaign-workspace/sprint/sprint-view.page';
import { mockSprintModel } from '../../../../data-mock/sprint.data-mock';
import { SprintModel } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/sprint-model';
import { SprintStatus } from '../../../../../../projects/sqtm-core/src/lib/model/level-enums/level-enum';
import { CreateSprintGroupDialog } from '../../../../page-objects/pages/campaign-workspace/dialogs/create-sprint-group-dialog.element';
import { SprintGroupModel } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/sprint-group-model';
import { mockSprintGroupModel } from '../../../../data-mock/sprint-group.data-mock';
import { SprintGroupViewPage } from '../../../../page-objects/pages/campaign-workspace/sprint-group/sprint-group-view.page';
import { mockDataRow, mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';
import Chainable = Cypress.Chainable;

describe('Campaign Workspace Tree', () => {
  describe('Display Campaign Workspace Tree', function () {
    const initialNodes = mockGridResponse('id', [
      mockTreeNode({
        id: 'CampaignLibrary-1',
        children: [],
        data: { NAME: 'Project1' },
        state: DataRowOpenState.closed,
      }),
    ]);

    it('should display a simple tree', () => {
      const firstNode = initialNodes.dataRows[0];
      const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes);
      const tree = campaignWorkspacePage.tree;
      tree.assertNodeExist(firstNode.id);
      tree.assertNodeTextContains(firstNode.id, firstNode.data.NAME);
    });

    it('should expand a library node', () => {
      const refreshedLibraryAfterOpen = [
        mockTreeNode({
          id: 'CampaignLibrary-1',
          children: ['CampaignFolder-1', 'Campaign-3', 'CampaignFolder-2'],
          state: DataRowOpenState.open,
          data: { NAME: 'Project1', CHILD_COUNT: 3 },
        }),
        mockTreeNode({
          id: 'CampaignFolder-1',
          children: [],
          data: { NAME: 'folder1' },
          parentRowId: 'CampaignLibrary-1',
        }),
        mockTreeNode({
          id: 'Campaign-3',
          children: [],
          data: { NAME: 'campaign3' },
          parentRowId: 'CampaignLibrary-1',
        }),
        mockTreeNode({
          id: 'CampaignFolder-2',
          children: [],
          data: { NAME: 'folder2' },
          parentRowId: 'CampaignLibrary-1',
        }),
      ];
      const firstNode = initialNodes.dataRows[0];
      const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes);
      const tree = campaignWorkspacePage.tree;
      tree.openNode(firstNode.id, refreshedLibraryAfterOpen);
      tree.assertNodeIsOpen(firstNode.id);
      tree.assertNodeExist('CampaignFolder-1');
      tree.assertNodeTextContains('CampaignFolder-1', 'folder1');
      tree.assertNodeExist('Campaign-3');
      tree.assertNodeTextContains('Campaign-3', 'campaign3');
      tree.assertNodeExist('CampaignFolder-2');
      tree.assertNodeTextContains('CampaignFolder-2', 'folder2');
      tree.closeNode(firstNode.id);
      tree.assertNodeIsClosed(firstNode.id);
      tree.assertNodeNotExist('CampaignFolder-1');
      tree.assertNodeNotExist('Campaign-3');
      tree.assertNodeNotExist('CampaignFolder-2');
    });
  });

  describe('Create Campaign', function () {
    const initialNodes = mockGridResponse('id', [
      mockTreeNode({
        id: 'CampaignLibrary-1',
        children: ['CampaignFolder-2'],
        projectId: 1,
        state: DataRowOpenState.open,
        data: { NAME: 'Project1', CHILD_COUNT: 1 },
      }),
      mockTreeNode({
        id: 'CampaignFolder-2',
        children: ['Campaign-3'],
        parentRowId: 'CampaignLibrary-1',
        projectId: 1,
        state: DataRowOpenState.open,
        data: { NAME: 'Folder 1', CHILD_COUNT: 1 },
      }),
      mockTreeNode({
        id: 'Campaign-3',
        children: [],
        projectId: 1,
        parentRowId: 'CampaignFolder-2',
        data: { NAME: 'Campaign 1', CHILD_COUNT: 0 },
      }),
    ]);

    const libraryModel: CampaignLibraryModel = mockCampaignLibraryModel({
      attachmentList: {
        id: 1,
        attachments: [],
      },
      customFieldValues: [],
      description: '',
      id: 1,
      name: 'Project1',
      projectId: 1,
    });

    const folderModel: CampaignFolderModel = mockCampaignFolderModel({
      attachmentList: {
        id: 1,
        attachments: [],
      },
      customFieldValues: [],
      description: '',
      id: 2,
      name: 'Folder 1',
      projectId: 1,
    });

    it('should create a campaign', () => {
      const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(
        initialNodes,
        createEntityReferentialData,
      );
      const tree = campaignWorkspacePage.tree;

      campaignWorkspacePage.treeMenu.assertCreateCampaignDisabled();
      tree.selectNode(initialNodes.dataRows[0].id, libraryModel);

      const createDialog: CreateCampaignDialog =
        campaignWorkspacePage.treeMenu.openCreateCampaign();
      const addedRow = createCampaignMockData(85, 'Campaign 01', 'REF01', 'A real description.', 1);
      addCampaignAndCheckViewPage(createDialog, addedRow, tree, 'CampaignLibrary-1', false, false, [
        addedRow.addedDataRow,
      ]);
    });

    it('should add a campaign to an already open folder', () => {
      const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(
        initialNodes,
        createEntityReferentialData,
      );
      const tree = campaignWorkspacePage.tree;

      tree.assertNodeExist('CampaignLibrary-1');
      tree.assertNodeExist('CampaignFolder-2');
      tree.assertNodeExist('Campaign-3');

      const statFolderBundleMock = campaignWorkspacePage.mockCustomReportFolderStatisticsRequest();
      tree.selectNode('CampaignFolder-2', folderModel);
      statFolderBundleMock.wait();

      const createDialog: CreateCampaignDialog =
        campaignWorkspacePage.treeMenu.openCreateCampaign();

      const addedRow = createCampaignMockData(86, 'Campaign 02', 'REF02', 'A real description.', 1);
      addCampaignAndCheckViewPage(createDialog, addedRow, tree, 'CampaignFolder-2', false, false, [
        addedRow.addedDataRow,
        initialNodes.dataRows[2],
      ]);
    });

    it('should forbid campaign creation if name already exists in container', () => {
      const httpError = {
        squashTMError: {
          kind: 'FIELD_VALIDATION_ERROR',
          fieldValidationErrors: [
            {
              fieldName: 'name',
              i18nKey: 'sqtm-core.error.generic.duplicate-name',
            },
          ],
        },
      };

      const workspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes);
      workspacePage.tree.selectNode(initialNodes.dataRows[0].id, libraryModel);
      const createDialog: CreateCampaignDialog = workspacePage.treeMenu.openCreateCampaign();
      createDialog.fillName('Campaign 01');
      createDialog.addWithServerSideFailure(httpError);
      createDialog.assertDuplicateNameErrorExist();
      createDialog.assertExists();
    });

    it('should forbid campaign creation if mandatory cuf is empty', () => {
      // Navigate to workspace page
      const workspacePage = CampaignWorkspacePage.initTestAtPage(
        initialNodes,
        createEntityReferentialData,
      );

      // Navigate to create dialog
      const createDialog = navigateToAddCampaignDialog(
        workspacePage,
        workspacePage.tree,
        initialNodes.dataRows[0].id,
      );
      createDialog.fillName('Campaign 01');
      createDialog.fillReference('BLCK');
      createDialog.fillDescription('A real description...');

      clearCustomFieldsAndCheckErrors(createDialog, 12, 13, 14, 16);
    });

    it('should remove any error message on CUFs after a Campaign was successfully created', () => {
      // Navigate to workspace page
      const workspacePage = CampaignWorkspacePage.initTestAtPage(
        initialNodes,
        createEntityReferentialData,
      );
      const tree = workspacePage.tree;

      // Navigate to create dialog
      const createDialog = navigateToAddCampaignDialog(
        workspacePage,
        tree,
        initialNodes.dataRows[0].id,
      );

      // Fill form...
      createDialog.fillName('Cmp01');

      // Clear some CUFS and submit to show errors
      clearCustomFieldsAndCheckErrors(createDialog, 12, 13, 14, 16);

      // Correct errors and submit with 'add another'
      createDialog.fillCustomField(12, 'My CUF 12');
      createDialog.fillCustomField(13, '13');
      createDialog.toggleTagFieldOptions(14, 0, 1, 2);
      createDialog.fillCustomField(16, 'rich value');

      const mockData = createCampaignMockData(85, 'Campaign 01', '', '', 1);

      addCampaignAndCheckViewPage(createDialog, mockData, tree, 'CampaignLibrary-1', true, false, [
        mockData.addedDataRow,
      ]).then(() => {
        // Check that errors for CustomField are reset
        createDialog.assertCustomFieldHasNoError(12);
        createDialog.assertCustomFieldHasNoError(13);
      });
    });
  });

  describe('Create Iteration', function () {
    const initialNodes = mockGridResponse('id', [
      mockTreeNode({
        id: 'CampaignLibrary-1',
        children: ['Campaign-85'],
        state: DataRowOpenState.open,
        projectId: 1,
        data: { NAME: 'Project1', CHILD_COUNT: 1 },
      }),
      mockTreeNode({
        id: 'Campaign-85',
        children: ['Iteration-1'],
        projectId: 1,
        state: DataRowOpenState.open,
        parentRowId: 'CampaignLibrary-1',
        data: { NAME: 'Campaign 1', CHILD_COUNT: 1 },
      }),
      mockTreeNode({
        id: 'Iteration-1',
        children: [],
        projectId: 1,
        parentRowId: 'Campaign-85',
        data: { NAME: 'Iteration 1', CHILD_COUNT: 0 },
      }),
    ]);

    const campaignModel: CampaignModel = mockCampaignModel({
      projectId: 1,
      id: 85,
      customFieldValues: [],
      attachmentList: {
        id: 1,
        attachments: [],
      },
      reference: 'REF01',
      description: 'A real description.',
      name: 'Campaign 01',
      campaignStatus: 'UNDEFINED',
      progressStatus: 'READY',
      lastModifiedBy: 'toto',
      createdBy: 'admin',
      milestones: [],
      testPlanStatistics: {
        nbTestCases: 1,
        progression: 2,
        status: 'READY',
        nbDone: 1,
        nbSuccess: 0,
        nbSettled: 0,
        nbFailure: 0,
        nbBlocked: 0,
        nbUntestable: 0,
        nbRunning: 0,
        nbReady: 0,
      },
      nbIssues: 0,
      hasDatasets: false,
    });

    it('should create iteration', () => {
      const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(
        initialNodes,
        createEntityReferentialData,
      );
      const tree = campaignWorkspacePage.tree;

      campaignWorkspacePage.treeMenu.assertAddIterationDisabled();
      const statisticsMock = campaignWorkspacePage.mockCampaignStatisticsRequest();
      tree.selectNode(initialNodes.dataRows[1].id, campaignModel);
      statisticsMock.wait();

      const createDialog: CreateIterationDialog = campaignWorkspacePage.treeMenu.openAddIteration();
      const mockIterationData = createIterationMockData(
        2,
        'Iteration 2',
        '2',
        'Iteration description',
        1,
      );
      addIterationAndCheckViewPage(
        createDialog,
        mockIterationData,
        tree,
        'Campaign-85',
        false,
        false,
        [mockIterationData.addedDataRow],
      );
    });

    it('should forbid iteration creation if name already exists in container', () => {
      const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(
        initialNodes,
        createEntityReferentialData,
      );
      const tree = campaignWorkspacePage.tree;

      // Project 1 : should be allowed to create
      campaignWorkspacePage.treeMenu.assertAddIterationDisabled();
      const statisticsMock = campaignWorkspacePage.mockCampaignStatisticsRequest();
      tree.selectNode(initialNodes.dataRows[1].id, campaignModel);
      statisticsMock.wait();

      const createDialog: CreateEntityDialog = campaignWorkspacePage.treeMenu.openAddIteration();
      createDialog.fillName('Iteration 1');

      const httpError = {
        squashTMError: {
          kind: 'FIELD_VALIDATION_ERROR',
          fieldValidationErrors: [
            {
              fieldName: 'name',
              i18nKey: 'sqtm-core.error.generic.duplicate-name',
            },
          ],
        },
      };

      createDialog.addWithServerSideFailure(httpError);
      createDialog.assertDuplicateNameErrorExist();
      createDialog.assertExists();
    });

    it('should forbid iteration creation if mandatory cuf is empty', () => {
      // Navigate to workspace page
      const workspacePage = CampaignWorkspacePage.initTestAtPage(
        initialNodes,
        createEntityReferentialData,
      );

      const model = createCampaignMockData(1, 'campaign-test', 'ref-1', 'description', 1);
      // Navigate to create dialog
      const createDialog = navigateToAddIterationDialog(
        workspacePage,
        workspacePage.tree,
        initialNodes.dataRows[1].id,
        model.campaignModel,
      );
      createDialog.fillName('Iteration 01');
      createDialog.fillReference('BLCK');
      createDialog.fillDescription('Une description riche...');

      clearCustomFieldsAndCheckErrors(createDialog, 12, 13, 14, 16);
    });

    it('should remove error message on CUFs and set default reference after a Iteration was successfully created', () => {
      // Navigate to workspace page
      const workspacePage = CampaignWorkspacePage.initTestAtPage(
        initialNodes,
        createEntityReferentialData,
      );
      const tree = workspacePage.tree;

      const model = createCampaignMockData(1, 'campaign-test', 'ref-1', 'description', 1);
      // Navigate to create dialog
      const createDialog = navigateToAddIterationDialog(
        workspacePage,
        tree,
        initialNodes.dataRows[1].id,
        model.campaignModel,
      );

      // Fill form...
      createDialog.fillName('IT01');

      // Clear some CUFS and submit to show errors
      clearCustomFieldsAndCheckErrors(createDialog, 12, 13, 14, 16);

      // Correct errors and submit with 'add another'
      createDialog.fillCustomField(12, 'My CUF 12');
      createDialog.fillCustomField(13, '13');
      createDialog.toggleTagFieldOptions(14, 0, 1, 2);
      createDialog.fillCustomField(16, 'rich value');

      const mockData = createIterationMockData(2, 'Iteration 2', '', '', 1);

      addIterationAndCheckViewPage(createDialog, mockData, tree, 'Campaign-85', true, false, [
        mockData.addedDataRow,
      ]).then(() => {
        // Check that errors for CustomField are reset
        createDialog.assertCustomFieldHasNoError(12);
        createDialog.assertCustomFieldHasNoError(13);
        createDialog.checkReferenceContent('3');
      });
    });
  });

  describe('Create Test Suite', function () {
    const initialNodes = mockGridResponse('id', [
      mockTreeNode({
        id: 'CampaignLibrary-1',
        children: ['Campaign-85'],
        state: DataRowOpenState.open,
        projectId: 1,
        data: { NAME: 'Project1', CHILD_COUNT: 1 },
      }),
      mockTreeNode({
        id: 'Campaign-85',
        children: ['Iteration-1'],
        projectId: 1,
        state: DataRowOpenState.open,
        parentRowId: 'CampaignLibrary-1',
        data: { NAME: 'Campaign 1', CHILD_COUNT: 1 },
      }),
      mockTreeNode({
        id: 'Iteration-1',
        children: ['TestSuite-1'],
        projectId: 1,
        state: DataRowOpenState.open,
        parentRowId: 'Campaign-85',
        data: { NAME: 'Iteration 1', CHILD_COUNT: 1 },
      }),
      mockTreeNode({
        id: 'TestSuite-1',
        children: [],
        projectId: 1,
        parentRowId: 'Iteration-1',
        data: { NAME: 'Suite 1', EXECUTION_STATUS: 'READY', CHILD_COUNT: 0 },
      }),
    ]);

    const iterationModel: IterationModel = mockIterationModel({
      reference: 'REF01',
      description: 'A real description.',
      name: 'Iteration 1',
      testPlanStatistics: {
        nbTestCases: 1,
        progression: 2,
        status: 'READY',
        nbDone: 1,
        nbSuccess: 0,
        nbSettled: 0,
        nbFailure: 0,
        nbBlocked: 0,
        nbUntestable: 0,
        nbRunning: 0,
        nbReady: 0,
      },
    });

    it('should create test suite', () => {
      const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(
        initialNodes,
        createEntityReferentialData,
      );
      const tree = campaignWorkspacePage.tree;

      campaignWorkspacePage.treeMenu.assertAddSuiteDisabled();
      campaignWorkspacePage.mockIterationViewStatisticsRequest();
      const statisticsMock = campaignWorkspacePage.mockIterationViewStatisticsRequest();
      tree.selectNode(initialNodes.dataRows[2].id, iterationModel);
      statisticsMock.wait();

      const createDialog: CreateSuiteDialog = campaignWorkspacePage.treeMenu.openAddTestSuite();
      const suiteMockData = createTestSuiteMockData(2, 'Suite 2', '2', 1);
      addSuiteAndCheckViewPage(createDialog, suiteMockData, tree, 'Iteration-1', false, false, [
        suiteMockData.addedDataRow,
      ]);
    });
  });

  describe('Create Folder', function () {
    const initialNodes = mockGridResponse('id', [
      mockTreeNode({
        id: 'CampaignLibrary-1',
        children: ['CampaignFolder-2'],
        projectId: 1,
        state: DataRowOpenState.open,
        data: { NAME: 'Project1', CHILD_COUNT: 1 },
      }),
      mockTreeNode({
        id: 'CampaignFolder-2',
        children: ['Campaign-3'],
        parentRowId: 'CampaignLibrary-1',
        projectId: 1,
        state: DataRowOpenState.open,
        data: { NAME: 'Folder 1', CHILD_COUNT: 1 },
      }),
      mockTreeNode({
        id: 'Campaign-3',
        children: [],
        projectId: 1,
        parentRowId: 'CampaignFolder-2',
        data: { NAME: 'Campaign 1', CHILD_COUNT: 0 },
      }),
    ]);

    const libraryModel: CampaignLibraryModel = mockCampaignLibraryModel({
      attachmentList: {
        id: 1,
        attachments: [],
      },
      customFieldValues: [],
      description: '',
      id: 1,
      name: 'Project1',
      projectId: 1,
    });

    const folderModel: CampaignFolderModel = mockCampaignFolderModel({
      attachmentList: {
        id: 1,
        attachments: [],
      },
      customFieldValues: [],
      description: '',
      id: 2,
      name: 'Folder 1',
      projectId: 1,
    });

    it('should create a folder', () => {
      const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(
        initialNodes,
        createEntityReferentialData,
      );
      const tree = campaignWorkspacePage.tree;

      campaignWorkspacePage.treeMenu.assertCreateFolderDisabled();
      tree.selectNode(initialNodes.dataRows[0].id, libraryModel);

      const createDialog: CreateCampaignFolderDialog =
        campaignWorkspacePage.treeMenu.openCreateCampaignFolder();
      const addedRow = createFolderMockData(85, 'Folder 01', 'A real description.', 1);
      addFolderAndCheckViewPage(createDialog, addedRow, tree, 'CampaignLibrary-1', false, false, [
        addedRow.addedDataRow,
      ]);
    });

    it('should add a folder to an already open folder', () => {
      const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(
        initialNodes,
        createEntityReferentialData,
      );
      const tree = campaignWorkspacePage.tree;

      tree.assertNodeExist('CampaignLibrary-1');
      tree.assertNodeExist('CampaignFolder-2');
      tree.assertNodeExist('Campaign-3');

      const statFolderBundleMock = campaignWorkspacePage.mockCustomReportFolderStatisticsRequest();
      tree.selectNode('CampaignFolder-2', folderModel);
      statFolderBundleMock.wait();

      const createDialog: CreateCampaignDialog =
        campaignWorkspacePage.treeMenu.openCreateCampaignFolder();

      const addedRow = createFolderMockData(86, 'Folder 02', 'A real description.', 1);
      addFolderAndCheckViewPage(createDialog, addedRow, tree, 'CampaignFolder-2', false, false, [
        addedRow.addedDataRow,
        initialNodes.dataRows[2],
      ]);
    });
  });

  describe('Create Sprint', function () {
    const initialNodes: GridResponse = mockGridResponse('id', [
      mockTreeNode({
        id: 'CampaignLibrary-1',
        children: ['Sprint-1', 'SprintGroup-1'],
        projectId: 1,
        state: DataRowOpenState.open,
        data: { NAME: 'Project1', CHILD_COUNT: 2 },
      }),
      mockTreeNode({
        id: 'Sprint-1',
        parentRowId: 'CampaignLibrary-1',
        projectId: 1,
        state: DataRowOpenState.leaf,
        data: { NAME: 'Sprint 1', STATUS: SprintStatus.UPCOMING.id, CHILD_COUNT: 0 },
      }),
      mockTreeNode({
        id: 'SprintGroup-1',
        parentRowId: 'CampaignLibrary-1',
        projectId: 1,
        state: DataRowOpenState.leaf,
        data: { NAME: 'Sprint Group 1', CHILD_COUNT: 0 },
      }),
    ]);

    const libraryModel: CampaignLibraryModel = mockCampaignLibraryModel({
      attachmentList: {
        id: 1,
        attachments: [],
      },
      customFieldValues: [],
      description: '',
      id: 1,
      name: 'Project1',
      projectId: 1,
    });

    it('should create a sprint', () => {
      const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(
        initialNodes,
        createEntityReferentialData,
      );
      const tree = campaignWorkspacePage.tree;

      campaignWorkspacePage.treeMenu.assertCreateSprintDisabled();
      tree.selectNode(initialNodes.dataRows[0].id, libraryModel);

      const createDialog: CreateSprintDialog = campaignWorkspacePage.treeMenu.openCreateSprint();
      const addedRow = createSprintMockData(45, 'Sprint 78', 'REF01', 1);
      addSprintAndCheckViewPage(createDialog, addedRow, tree, 'CampaignLibrary-1', false, false);
    });

    it('should create a sprint under a sprint group', () => {
      const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(
        initialNodes,
        createEntityReferentialData,
      );
      const tree = campaignWorkspacePage.tree;

      campaignWorkspacePage.treeMenu.assertCreateSprintDisabled();
      tree.selectNode(initialNodes.dataRows[2].id, libraryModel);

      const createDialog: CreateSprintGroupDialog =
        campaignWorkspacePage.treeMenu.openCreateSprintGroup();
      const addedRow = createSprintGroupMockData(45, 'Sprint Group 2024', 1);
      addSprintGroupAndCheckViewPage(
        createDialog,
        addedRow,
        tree,
        'CampaignLibrary-1',
        false,
        false,
      );
    });

    it('should forbid sprint creation if name already exists in container', () => {
      const httpError = {
        squashTMError: {
          kind: 'FIELD_VALIDATION_ERROR',
          fieldValidationErrors: [
            {
              fieldName: 'name',
              i18nKey: 'sqtm-core.error.generic.duplicate-name',
            },
          ],
        },
      };

      const workspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes);
      workspacePage.tree.selectNode(initialNodes.dataRows[0].id, libraryModel);
      const createDialog: CreateSprintDialog = workspacePage.treeMenu.openCreateSprint();
      createDialog.fillName('Sprint 1');
      createDialog.addWithServerSideFailure(httpError);
      createDialog.assertDuplicateNameErrorExist();
      createDialog.assertExists();
    });
  });

  describe('Create Sprint Group', function () {
    const initialNodes: GridResponse = mockGridResponse('id', [
      mockTreeNode({
        id: 'CampaignLibrary-1',
        children: ['Sprint-1', 'Campaign-1', 'SprintGroup-1', 'CampaignFolder-1'],
        projectId: 1,
        state: DataRowOpenState.open,
        data: { NAME: 'Project1', CHILD_COUNT: 2 },
      }),
      mockTreeNode({
        id: 'Sprint-1',
        parentRowId: 'CampaignLibrary-1',
        projectId: 1,
        state: DataRowOpenState.leaf,
        data: { NAME: 'Sprint 1', CHILD_COUNT: 0, STATUS: SprintStatus.UPCOMING.id },
      }),
      mockTreeNode({
        id: 'Campaign-1',
        parentRowId: 'CampaignLibrary-1',
        children: ['Iteration-1'],
        projectId: 1,
        state: DataRowOpenState.open,
        data: { NAME: 'Camapign 1', CHILD_COUNT: 1 },
      }),
      mockTreeNode({
        id: 'Iteration-1',
        parentRowId: 'Campaign-1',
        children: ['TestSuite-1'],
        projectId: 1,
        state: DataRowOpenState.open,
        data: { NAME: 'Iteration 1', CHILD_COUNT: 1 },
      }),
      mockTreeNode({
        id: 'TestSuite-1',
        parentRowId: 'Iteration-1',
        projectId: 1,
        data: { NAME: 'Suite 1', EXECUTION_STATUS: 'READY', CHILD_COUNT: 0 },
      }),
      mockTreeNode({
        id: 'SprintGroup-1',
        parentRowId: 'CampaignLibrary-1',
        children: ['CampaignFolder-2'],
        state: DataRowOpenState.open,
        projectId: 1,
        data: { NAME: 'Sprint Group 1', CHILD_COUNT: 1 },
      }),
      mockTreeNode({
        id: 'CampaignFolder-1',
        parentRowId: 'CampaignLibrary-1',
        children: [],
        projectId: 1,
        data: { NAME: 'Folder 1', CHILD_COUNT: 0, HAS_SPRINT_GROUP_PARENT: false },
      }),
      mockTreeNode({
        id: 'CampaignFolder-2',
        parentRowId: 'SprintGroup-1',
        children: [],
        projectId: 1,
        data: { NAME: 'Folder 2', CHILD_COUNT: 0, HAS_SPRINT_GROUP_PARENT: true },
      }),
    ]);

    const libraryModel: CampaignLibraryModel = mockCampaignLibraryModel({
      attachmentList: {
        id: 1,
        attachments: [],
      },
      customFieldValues: [],
      description: '',
      id: 1,
      name: 'Project1',
      projectId: 1,
    });

    it('should create a sprint group', () => {
      const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(
        initialNodes,
        createEntityReferentialData,
      );
      const tree = campaignWorkspacePage.tree;

      campaignWorkspacePage.treeMenu.assertCreateSprintGroupDisabled();
      tree.selectNode(initialNodes.dataRows[0].id, libraryModel);

      const createDialog: CreateSprintGroupDialog =
        campaignWorkspacePage.treeMenu.openCreateSprintGroup();
      const addedRow = createSprintGroupMockData(45, 'Sprint Group 2024', 1);
      addSprintGroupAndCheckViewPage(
        createDialog,
        addedRow,
        tree,
        'CampaignLibrary-1',
        false,
        false,
      );
    });

    it('should forbid sprint group creation under sprint, campaign, iteration or test suite', () => {
      const workspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes);
      // SPRINT
      workspacePage.tree.selectNode(initialNodes.dataRows[1].id, libraryModel);
      workspacePage.treeMenu.assertCreateSprintGroupDisabled();

      // CAMPAIGN
      workspacePage.tree.selectNode(initialNodes.dataRows[2].id, libraryModel);
      workspacePage.treeMenu.assertCreateSprintGroupDisabled();

      // ITERATION
      workspacePage.tree.selectNode(initialNodes.dataRows[3].id, libraryModel);
      workspacePage.treeMenu.assertCreateSprintGroupDisabled();

      // TEST SUITE
      workspacePage.tree.selectNode(initialNodes.dataRows[4].id, libraryModel);
      workspacePage.treeMenu.assertCreateSprintGroupDisabled();
    });

    it('should forbid sprint group creation under folder which has a sprint group parent', () => {
      const workspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes);
      workspacePage.tree.selectNode('CampaignFolder-2', libraryModel);
      workspacePage.treeMenu.assertCreateSprintGroupDisabled();
    });
  });
});

interface CampaignMockData {
  campaignId: number;
  campaignModel: CampaignModel;
  addedDataRow: DataRowModel;
}

interface IterationMockData {
  iterationId: number;
  iterationModel: IterationModel;
  addedDataRow: DataRowModel;
}

interface SuiteMockData {
  suiteId: number;
  suiteModel: TestSuiteModel;
  addedDataRow: DataRowModel;
}

interface FolderMockData {
  folderId: number;
  folderModel: CampaignFolderModel;
  addedDataRow: DataRowModel;
}

interface SprintMockData {
  sprintId: number;
  sprintModel: SprintModel;
  addedDataRow: DataRowModel;
}

interface SprintGroupMockData {
  sprintGroupId: number;
  sprintGroupModel: SprintGroupModel;
  addedDataRow: DataRowModel;
}

function createCampaignMockData(
  campaignId: number,
  name: string,
  reference: string,
  description: string,
  projectId: number,
): CampaignMockData {
  const campaignModel: CampaignModel = mockCampaignModel({
    id: campaignId,
    projectId,
    customFieldValues: [],
    attachmentList: {
      id: 1,
      attachments: [],
    },
    name,
    description,
    reference,
    campaignStatus: 'UNDEFINED',
    progressStatus: 'READY',
    lastModifiedBy: 'toto',
    createdBy: 'admin',
    milestones: [],
    testPlanStatistics: {
      nbTestCases: 1,
      progression: 2,
      status: 'READY',
      nbDone: 1,
      nbBlocked: 0,
      nbFailure: 0,
      nbReady: 0,
      nbRunning: 0,
      nbSettled: 0,
      nbSuccess: 0,
      nbUntestable: 0,
    },
    nbIssues: 0,
    hasDatasets: false,
  });

  const addedDataRow = mockDataRow({
    id: `Campaign-${campaignId}`,
    projectId: projectId,
    children: [],
    state: DataRowOpenState.leaf,
    data: {
      CLN_ID: campaignId,
      NAME: name,
      projectId: projectId,
      REFERENCE: reference,
      STEP_COUNT: 0,
    },
  });

  return {
    campaignId,
    campaignModel,
    addedDataRow,
  };
}

function createIterationMockData(
  iterationId: number,
  name: string,
  reference: string,
  description: string,
  projectId: number,
): IterationMockData {
  const iterationModel: IterationModel = mockIterationModel({
    id: iterationId,
    projectId,
    name,
    description,
    reference,
  });

  const addedDataRow = mockDataRow({
    id: `Iteration-${iterationId}`,
    projectId: projectId,
    children: [],
    state: DataRowOpenState.leaf,
    data: {
      ITERATION_ID: iterationId,
      NAME: name,
      projectId: projectId,
      REFERENCE: reference,
      STEP_COUNT: 0,
    },
  });

  return {
    iterationId,
    iterationModel,
    addedDataRow,
  };
}

function createTestSuiteMockData(
  suiteId: number,
  name: string,
  description: string,
  projectId: number,
): SuiteMockData {
  const suiteModel: TestSuiteModel = mockTestSuiteModel({
    id: suiteId,
    projectId,
    customFieldValues: [],
    attachmentList: {
      id: 1,
      attachments: [],
    },
    name,
    description,
    executionStatus: 'RUNNING',
    createdBy: 'toto',
    lastModifiedBy: 'toto',
    uuid: 'ds',
    testPlanStatistics: {
      nbTestCases: 1,
      progression: 2,
      status: 'READY',
      nbDone: 1,
      nbSuccess: 0,
      nbSettled: 0,
      nbFailure: 0,
      nbBlocked: 0,
      nbUntestable: 0,
      nbRunning: 0,
      nbReady: 0,
    },
    hasDatasets: false,
    users: [],
    executionStatusMap: new Map(),
  });

  const addedDataRow = mockDataRow({
    id: `TestSuite-${suiteId}`,
    projectId: projectId,
    children: [],
    state: DataRowOpenState.leaf,
    data: {
      TEST_SUITE_ID: suiteId,
      NAME: name,
      projectId: projectId,
      EXECUTION_STATUS: 'READY',
      STEP_COUNT: 0,
      CAMPAIGN_ID: 85,
    },
  });

  return {
    suiteId,
    suiteModel,
    addedDataRow,
  };
}

function createFolderMockData(
  folderId: number,
  name: string,
  description: string,
  projectId: number,
): FolderMockData {
  const folderModel: CampaignFolderModel = mockCampaignFolderModel({
    attachmentList: {
      id: 1,
      attachments: [],
    },
    customFieldValues: [],
    description: description,
    name: name,
    id: folderId,
    projectId: projectId,
  });

  const addedDataRow = mockDataRow({
    id: `CampaignFolder-${folderId}`,
    projectId: projectId,
    children: [],
    state: DataRowOpenState.leaf,
    data: {
      CLN_ID: folderId,
      NAME: name,
      projectId: projectId,
      STEP_COUNT: 0,
    },
  });

  return {
    folderId,
    folderModel,
    addedDataRow,
  };
}

function createSprintMockData(
  sprintId: number,
  name: string,
  reference: string,
  projectId: number,
): SprintMockData {
  const sprintModel: SprintModel = mockSprintModel({
    id: sprintId,
    name: name,
    reference: reference,
    projectId: projectId,
  });

  const addedDataRow = mockDataRow({
    id: `Sprint-${sprintId}`,
    projectId: projectId,
    children: [],
    state: DataRowOpenState.leaf,
    data: {
      CLN_ID: sprintId,
      NAME: name,
      projectId: projectId,
      REFERENCE: reference,
      STEP_COUNT: 0,
      STATUS: SprintStatus.UPCOMING.id,
    },
  });

  return {
    sprintId: sprintId,
    sprintModel,
    addedDataRow,
  };
}

function createSprintGroupMockData(
  sprintGroupId: number,
  name: string,
  projectId: number,
): SprintGroupMockData {
  const sprintGroupModel: SprintGroupModel = mockSprintGroupModel({
    id: sprintGroupId,
    name: name,
    projectId: projectId,
  });

  const addedDataRow = mockDataRow({
    id: `SprintGroup-${sprintGroupId}`,
    projectId: projectId,
    children: [],
    state: DataRowOpenState.leaf,
    data: {
      CLN_ID: sprintGroupId,
      NAME: name,
      projectId: projectId,
      STEP_COUNT: 0,
    },
  });

  return {
    sprintGroupId: sprintGroupId,
    sprintGroupModel,
    addedDataRow,
  };
}

function navigateToAddCampaignDialog(
  workspacePage: CampaignWorkspacePage,
  tree: TreeElement,
  rowIdToAddTo: Identifier,
): CreateCampaignDialog {
  const firstModel: CampaignLibraryModel = mockCampaignLibraryModel({
    attachmentList: {
      id: 1,
      attachments: [],
    },
    customFieldValues: [],
    description: '',
    id: 1,
    name: 'Project1',
    projectId: 1,
  });

  workspacePage.treeMenu.assertCreateCampaignDisabled();
  tree.selectNode(rowIdToAddTo, firstModel);

  const projectData = {
    id: 1,
    customFieldBinding: {
      CAMPAIGN: [
        {
          customField: createEntityReferentialData.customFields[0],
          bindableEntity: BindableEntity.CAMPAIGN,
          boundProjectId: 1,
          id: 1,
          position: 0,
          renderingLocations: [],
        },
        {
          customField: createEntityReferentialData.customFields[2],
          bindableEntity: BindableEntity.CAMPAIGN,
          boundProjectId: 1,
          id: 3,
          position: 2,
          renderingLocations: [],
        },
        {
          customField: createEntityReferentialData.customFields[1],
          bindableEntity: BindableEntity.CAMPAIGN,
          boundProjectId: 1,
          id: 2,
          position: 1,
          renderingLocations: [],
        },
        {
          customField: createEntityReferentialData.customFields[3],
          bindableEntity: BindableEntity.CAMPAIGN,
          boundProjectId: 1,
          id: 4,
          position: 3,
          renderingLocations: [],
        },
        {
          customField: createEntityReferentialData.customFields[4],
          bindableEntity: BindableEntity.CAMPAIGN,
          boundProjectId: 1,
          id: 5,
          position: 4,
          renderingLocations: [],
        },
      ],
    },
  } as ProjectData;

  const createDialog: CreateCampaignDialog = workspacePage.treeMenu.openCreateCampaign(projectData);
  createDialog.assertExists();

  return createDialog;
}

function navigateToAddIterationDialog(
  workspacePage: CampaignWorkspacePage,
  tree: TreeElement,
  rowIdToAddTo: Identifier,
  model: CampaignModel,
): CreateIterationDialog {
  workspacePage.treeMenu.assertAddIterationDisabled();
  const statisticsMock = workspacePage.mockCampaignStatisticsRequest();
  tree.selectNode(rowIdToAddTo, model);
  statisticsMock.wait();

  const projectData = {
    id: 1,
    customFieldBinding: {
      ITERATION: [
        {
          customField: createEntityReferentialData.customFields[0],
          bindableEntity: BindableEntity.ITERATION,
          boundProjectId: 1,
          id: 1,
          position: 0,
          renderingLocations: [],
        },
        {
          customField: createEntityReferentialData.customFields[2],
          bindableEntity: BindableEntity.ITERATION,
          boundProjectId: 1,
          id: 3,
          position: 2,
          renderingLocations: [],
        },
        {
          customField: createEntityReferentialData.customFields[1],
          bindableEntity: BindableEntity.ITERATION,
          boundProjectId: 1,
          id: 2,
          position: 1,
          renderingLocations: [],
        },
        {
          customField: createEntityReferentialData.customFields[3],
          bindableEntity: BindableEntity.ITERATION,
          boundProjectId: 1,
          id: 4,
          position: 3,
          renderingLocations: [],
        },
        {
          customField: createEntityReferentialData.customFields[4],
          bindableEntity: BindableEntity.ITERATION,
          boundProjectId: 1,
          id: 5,
          position: 4,
          renderingLocations: [],
        },
      ],
    },
  } as ProjectData;

  const createDialog: CreateIterationDialog = workspacePage.treeMenu.openAddIteration(projectData);
  createDialog.assertExists();

  return createDialog;
}

/* Shortcut to empty a bunch of CUFS, validate the form and check that validation errors are shown */
function clearCustomFieldsAndCheckErrors(createDialog: CreateCampaignDialog, ...cufIds: number[]) {
  cufIds.forEach((id) => createDialog.clearCustomField(id));
  createDialog.addWithClientSideFailure();
  cufIds.forEach((id) => {
    createDialog.assertCustomFieldErrorExist(id, 'sqtm-core.validation.errors.required');
  });
}

function addFolderAndCheckViewPage(
  createDialog: CreateCampaignFolderDialog,
  mockData: FolderMockData,
  tree: TreeElement,
  parentId: string,
  addAnother: boolean,
  parentRowIsClosed: boolean,
  openNodeResponse: DataRowModel[],
): Chainable<any> {
  // We assume the creation dialog is already shown so we can fill the form
  createDialog.fillName(mockData.folderModel.name);
  createDialog.fillDescription(mockData.folderModel.description);

  // Declare route for statistics panel
  new HttpMockBuilder('campaign-folder-view/*/statistics').post().build();

  // Do the button click
  return createDialog
    .addWithOptions({
      addAnother,
      addedId: mockData.folderId,
      children: createOpenNodeResponse(openNodeResponse),
      createResponse: mockData.addedDataRow,
      entityModel: mockData.folderModel,
      parentRowIsClosed,
      parentRowRef: parentId,
    })
    .then(() => {
      // Check the dialog visibility based on which button was pressed
      if (addAnother) {
        createDialog.assertExists();
      } else {
        createDialog.assertNotExist();
      }

      // Check node existence in the tree
      tree.assertNodeExist(mockData.addedDataRow.id);

      // Check the fields in the displayed view page
      const page = new CampaignFolderViewPage(mockData.folderId);
      page.assertExists();
      page.assertNameContains(mockData.folderModel.name);
    });
}

function addCampaignAndCheckViewPage(
  createDialog: CreateCampaignDialog,
  mockData: CampaignMockData,
  tree: TreeElement,
  parentId: string,
  addAnother: boolean,
  parentRowIsClosed: boolean,
  openNodeResponse: DataRowModel[],
): Chainable<any> {
  // We assume the creation dialog is already shown so we can fill the form
  createDialog.fillName(mockData.campaignModel.name);
  createDialog.fillReference(mockData.campaignModel.reference);
  createDialog.fillDescription(mockData.campaignModel.description);

  // declare route for statistics panel
  new HttpMockBuilder(`campaign-view/*/statistics`).post().build();

  // Do the button click
  return createDialog
    .addWithOptions({
      addAnother,
      addedId: mockData.campaignId,
      children: createOpenNodeResponse(openNodeResponse),
      createResponse: mockData.addedDataRow,
      entityModel: mockData.campaignModel,
      parentRowIsClosed,
      parentRowRef: parentId,
    })
    .then(() => {
      // Check the dialog visibility based on which button was pressed
      if (addAnother) {
        createDialog.assertExists();
      } else {
        createDialog.assertNotExist();
      }

      // Check node existence in the tree
      tree.assertNodeExist(mockData.addedDataRow.id);

      // Check the fields in the displayed view page
      const page = new CampaignViewPage(mockData.campaignId);
      page.assertExists();
      page.assertNameContains(mockData.campaignModel.name);
    });
}

function addIterationAndCheckViewPage(
  createDialog: CreateIterationDialog,
  mockData: IterationMockData,
  tree: TreeElement,
  parentId: string,
  addAnother: boolean,
  parentRowIsClosed: boolean,
  openNodeResponse: DataRowModel[],
): Chainable<any> {
  // We assume the creation dialog is already shown so we can fill the form
  createDialog.fillName(mockData.iterationModel.name);
  createDialog.fillReference(mockData.iterationModel.reference);
  createDialog.fillDescription(mockData.iterationModel.description);

  new HttpMockBuilder(`iteration-view/*/statistics`)
    .post()
    .responseBody(getEmptyIterationStatisticsBundle())
    .build();

  const page = new IterationViewPage();

  // Do the button click
  return createDialog
    .addWithOptions({
      addAnother,
      addedId: mockData.iterationId,
      children: createOpenNodeResponse(openNodeResponse),
      createResponse: mockData.addedDataRow,
      entityModel: mockData.iterationModel,
      parentRowIsClosed,
      parentRowRef: parentId,
    })
    .then(() => {
      // Check the dialog visibility based on which button was pressed
      if (addAnother) {
        createDialog.assertExists();
      } else {
        createDialog.assertNotExist();
      }

      // Check node existence in the tree
      tree.assertNodeExist(mockData.addedDataRow.id);

      // Check the fields in the displayed view page
      page.assertExists();
      page.assertNameContains(mockData.iterationModel.name);
    });
}

function addSuiteAndCheckViewPage(
  createDialog: CreateSuiteDialog,
  mockData: SuiteMockData,
  tree: TreeElement,
  parentId: string,
  addAnother: boolean,
  parentRowIsClosed: boolean,
  openNodeResponse: DataRowModel[],
): Chainable<any> {
  // We assume the creation dialog is already shown so we can fill the form
  createDialog.fillName(mockData.suiteModel.name);
  createDialog.fillDescription(mockData.suiteModel.description);

  // Do the button click
  return createDialog
    .addWithOptions({
      addAnother,
      addedId: mockData.suiteId,
      children: createOpenNodeResponse(openNodeResponse),
      createResponse: mockData.addedDataRow,
      entityModel: mockData.suiteModel,
      parentRowIsClosed,
      parentRowRef: parentId,
    })
    .then(() => {
      // Check the dialog visibility based on which button was pressed
      if (addAnother) {
        createDialog.assertExists();
      } else {
        createDialog.assertNotExist();
      }

      // Check node existence in the tree
      tree.assertNodeExist(mockData.addedDataRow.id);

      // Check the fields in the displayed view page
      const page = new TestSuiteViewPage(mockData.suiteId);
      page.assertExists();
      page.assertNameContains(mockData.suiteModel.name);
    });
}

function addSprintAndCheckViewPage(
  createDialog: CreateSprintDialog,
  mockData: SprintMockData,
  tree: TreeElement,
  parentId: string,
  addAnother: boolean,
  parentRowIsClosed: boolean,
): Chainable<any> {
  // We assume the creation dialog is already shown so we can fill the form
  createDialog.fillName(mockData.sprintModel.name);
  createDialog.fillReference(mockData.sprintModel.reference);

  // declare route for statistics panel
  new HttpMockBuilder(`sprint-view/*`).post().build();

  // Do the button click
  return createDialog
    .addWithOptions({
      addAnother,
      addedId: mockData.sprintId,
      // children: createOpenNodeResponse(openNodeResponse),
      createResponse: mockData.addedDataRow,
      entityModel: mockData.sprintModel,
      parentRowIsClosed,
      parentRowRef: parentId,
    })
    .then(() => {
      // Check the dialog visibility based on which button was pressed
      if (addAnother) {
        createDialog.assertExists();
      } else {
        createDialog.assertNotExist();
      }

      // Check node existence in the tree
      tree.assertNodeExist(mockData.addedDataRow.id);

      // Check the fields in the displayed view page
      const page = new SprintViewPage(mockData.sprintId);
      page.assertExists();
      page.assertNameContains(mockData.sprintModel.name);
    });
}

function addSprintGroupAndCheckViewPage(
  createDialog: CreateSprintGroupDialog,
  mockData: SprintGroupMockData,
  tree: TreeElement,
  parentId: string,
  addAnother: boolean,
  parentRowIsClosed: boolean,
): Chainable<any> {
  // We assume the creation dialog is already shown so we can fill the form
  createDialog.fillName(mockData.sprintGroupModel.name);

  // declare route for statistics panel
  new HttpMockBuilder(`sprint-group-view/*`).post().build();

  // Do the button click
  return createDialog
    .addWithOptions({
      addAnother,
      addedId: mockData.sprintGroupId,
      createResponse: mockData.addedDataRow,
      entityModel: mockData.sprintGroupModel,
      parentRowIsClosed,
      parentRowRef: parentId,
    })
    .then(() => {
      // Check the dialog visibility based on which button was pressed
      if (addAnother) {
        createDialog.assertExists();
      } else {
        createDialog.assertNotExist();
      }

      // Check node existence in the tree
      tree.assertNodeExist(mockData.addedDataRow.id);

      // Check the fields in the displayed view page
      const page = new SprintGroupViewPage(mockData.sprintGroupId);
      page.assertExists();
      page.assertNameContains(mockData.sprintGroupModel.name);
    });
}

// Shortcut to create a GridResponse based on a DataRow array
function createOpenNodeResponse(rows: DataRowModel[]): GridResponse {
  return {
    count: rows.length,
    dataRows: rows,
  };
}
