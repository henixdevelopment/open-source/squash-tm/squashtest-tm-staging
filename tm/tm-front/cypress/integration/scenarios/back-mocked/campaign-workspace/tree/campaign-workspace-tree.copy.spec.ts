import {
  ALL_PROJECT_PERMISSIONS,
  NO_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder,
} from '../../../../utils/referential/referential-data-builder';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

describe('Campaign Workspace Tree Copy', function () {
  function referentialData() {
    return new ReferentialDataMockBuilder()
      .withProjects(
        {
          name: 'Project 1',
          permissions: ALL_PROJECT_PERMISSIONS,
        },
        {
          name: 'Project 2',
          permissions: NO_PROJECT_PERMISSIONS,
        },
      )
      .build();
  }

  const initialNodes = mockGridResponse('id', [
    mockTreeNode({
      id: 'CampaignLibrary-1',
      projectId: 1,
      children: ['CampaignFolder-1', 'Campaign-3', 'CampaignFolder-2', 'Campaign-5'],
      data: { NAME: 'Project1', CHILD_COUNT: '4' },
      state: DataRowOpenState.open,
    }),
    mockTreeNode({
      id: 'CampaignLibrary-2',
      projectId: 2,
      children: [],
      data: { NAME: 'Project2', CHILD_COUNT: '1' },
    }),
    mockTreeNode({
      id: 'CampaignFolder-1',
      children: [],
      projectId: 1,
      parentRowId: 'CampaignLibrary-1',
      data: { NAME: 'folder1' },
    }),
    mockTreeNode({
      id: 'Campaign-3',
      children: ['Iteration-1'],
      projectId: 1,
      state: DataRowOpenState.open,
      parentRowId: 'CampaignLibrary-1',
      data: { NAME: 'a nice test' },
    }),
    mockTreeNode({
      id: 'CampaignFolder-2',
      children: [],
      projectId: 1,
      parentRowId: 'CampaignLibrary-1',
      data: { NAME: 'folder2' },
    }),
    mockTreeNode({
      id: 'Iteration-1',
      projectId: 1,
      children: [],
      parentRowId: 'Campaign-3',
      data: { NAME: 'Iteration' },
    }),
    mockTreeNode({
      id: 'Campaign-5',
      children: [],
      projectId: 1,
      parentRowId: 'CampaignLibrary-1',
      data: { NAME: 'a nice test' },
    }),
  ]);

  it('should activate or deactivate copy button according to user selection', () => {
    const firstNode = initialNodes.dataRows[0];
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes);
    const tree = campaignWorkspacePage.tree;
    tree.selectNode(firstNode.id);
    campaignWorkspacePage.treeMenu.assertCopyButtonIsDisabled();
    campaignWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    new HttpMockBuilder('campaign-folder-view/*/statistics').post().responseBody({}).build();
    tree.selectNode('CampaignFolder-1');
    campaignWorkspacePage.treeMenu.assertCopyButtonIsActive();
    campaignWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    campaignWorkspacePage.mockCampaignStatisticsRequest();
    tree.selectNode('Campaign-3');
    campaignWorkspacePage.treeMenu.assertCopyButtonIsActive();
    campaignWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    tree.selectNode('CampaignLibrary-1');
    campaignWorkspacePage.treeMenu.assertCopyButtonIsDisabled();
    campaignWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
  });

  it('should activate or deactivate paste button according to destination', () => {
    const firstNode = initialNodes.dataRows[0];
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(
      initialNodes,
      referentialData(),
    );
    const tree = campaignWorkspacePage.tree;
    tree.selectNode(firstNode.id);
    campaignWorkspacePage.treeMenu.assertCopyButtonIsDisabled();
    campaignWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    campaignWorkspacePage.mockCampaignStatisticsRequest();
    tree.selectNode('Campaign-3');
    campaignWorkspacePage.treeMenu.assertCopyButtonIsActive();
    campaignWorkspacePage.treeMenu.copy();
    campaignWorkspacePage.treeMenu.assertPasteButtonIsDisabled();

    const statFolderBundleMock = campaignWorkspacePage.mockCustomReportFolderStatisticsRequest();
    tree.selectNode('CampaignFolder-1');
    statFolderBundleMock.wait();

    campaignWorkspacePage.treeMenu.assertPasteButtonIsActive();
    tree.selectNode('CampaignLibrary-1');
    campaignWorkspacePage.treeMenu.assertPasteButtonIsActive();
    tree.selectNode('Campaign-3');
    campaignWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    // Testing permissions
    tree.selectNode('CampaignLibrary-2');
    campaignWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
  });

  it('should copy paste a node', () => {
    const refreshedNodes = [
      mockTreeNode({
        id: 'CampaignFolder-1',
        projectId: 1,
        children: ['Campaign-4'],
        parentRowId: 'CampaignLibrary-1',
        state: DataRowOpenState.open,
        data: { NAME: 'folder1', CHILD_COUNT: 1 },
      }),
      mockTreeNode({
        id: 'Campaign-4',
        projectId: 1,
        children: [],
        parentRowId: 'CampaignFolder-1',
        data: { NAME: 'a nice test' },
      }),
    ];
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(
      initialNodes,
      referentialData(),
    );
    const tree = campaignWorkspacePage.tree;
    campaignWorkspacePage.mockCampaignStatisticsRequest();
    tree.selectNode('Campaign-3');
    campaignWorkspacePage.treeMenu.copy();

    const statFolderBundleMock = campaignWorkspacePage.mockCustomReportFolderStatisticsRequest();
    tree.selectNode('CampaignFolder-1');
    statFolderBundleMock.wait();
    campaignWorkspacePage.treeMenu.paste(
      { dataRows: refreshedNodes },
      'campaign-tree',
      'CampaignFolder-1',
    );
    tree.assertNodeExist('Campaign-4');
    tree.assertNodeIsOpen('CampaignFolder-1');
  });

  it('should copy paste an iteration', () => {
    const refreshedNodes = [
      mockTreeNode({
        id: 'Campaign-5',
        projectId: 1,
        children: ['Iteration-4'],
        state: DataRowOpenState.open,
        parentRowId: 'CampaignFolder-1',
        data: { NAME: 'a nice test' },
      }),
      mockTreeNode({
        id: 'Iteration-4',
        projectId: 1,
        children: [],
        parentRowId: 'Campaign-5',
        data: { NAME: 'Itération 5' },
      }),
    ];
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(
      initialNodes,
      referentialData(),
    );
    const tree = campaignWorkspacePage.tree;
    campaignWorkspacePage.mockIterationViewStatisticsRequest();
    tree.selectNode('Iteration-1');
    campaignWorkspacePage.treeMenu.copy();
    campaignWorkspacePage.mockCampaignStatisticsRequest();
    tree.selectNode('Campaign-5');
    campaignWorkspacePage.treeMenu.paste(
      { dataRows: refreshedNodes },
      'campaign-tree',
      'Campaign-5',
    );
    tree.assertNodeExist('Iteration-4');
    tree.assertNodeIsOpen('Campaign-5');
  });

  it('forbid copy if a campaign and an iteration are selected', () => {
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(
      initialNodes,
      referentialData(),
    );
    const tree = campaignWorkspacePage.tree;
    campaignWorkspacePage.mockIterationViewStatisticsRequest();
    tree.selectNode('Iteration-1');
    tree.addNodeToSelection('Campaign-5');
    campaignWorkspacePage.treeMenu.assertCopyButtonIsDisabled();
  });
});
