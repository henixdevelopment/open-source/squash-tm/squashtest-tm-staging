import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { ReferentialDataMockBuilder } from '../../../../utils/referential/referential-data-builder';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { SprintStatus } from '../../../../../../projects/sqtm-core/src/lib/model/level-enums/level-enum';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

describe('Campaign Workspace Tree Delete', function () {
  const initialNodes = mockGridResponse('id', [
    mockTreeNode({
      id: 'CampaignLibrary-1',
      projectId: 1,
      children: ['CampaignFolder-1', 'Campaign-3', 'CampaignFolder-2', 'Sprint-1'],
      data: { NAME: 'Project1', CHILD_COUNT: '4' },
      state: DataRowOpenState.open,
    }),
    mockTreeNode({
      id: 'CampaignLibrary-2',
      projectId: 2,
      children: [],
      data: { NAME: 'Project2', CHILD_COUNT: '1' },
    }),
    mockTreeNode({
      id: 'CampaignFolder-1',
      children: [],
      projectId: 1,
      parentRowId: 'CampaignLibrary-1',
      data: { NAME: 'folder1' },
    }),
    mockTreeNode({
      id: 'Campaign-3',
      children: [],
      projectId: 1,
      parentRowId: 'CampaignLibrary-1',
      data: { NAME: 'a nice test' },
    }),
    mockTreeNode({
      id: 'CampaignFolder-2',
      children: [],
      projectId: 1,
      parentRowId: 'CampaignLibrary-1',
      data: { NAME: 'folder2' },
    }),
    mockTreeNode({
      id: 'Sprint-1',
      projectId: 1,
      parentRowId: 'CampaignLibrary-1',
      data: { NAME: 'Sprint 1', STATUS: SprintStatus.UPCOMING.id },
      state: DataRowOpenState.leaf,
    }),
  ]);

  it('should not be able to delete from tree', () => {
    const referentialData = new ReferentialDataMockBuilder()
      .withUser({
        canDeleteFromFront: false,
      })
      .build();

    const workspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes, referentialData);
    workspacePage.treeMenu.assertDeleteButtonIsHidden();
  });

  it('should activate or deactivate delete button according to user selection', () => {
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes);
    const tree = campaignWorkspacePage.tree;

    campaignWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();

    tree.selectNode('CampaignLibrary-1');
    campaignWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();

    tree.selectNode('Sprint-1');
    campaignWorkspacePage.treeMenu.assertDeleteButtonIsActive();

    const statFolderBundleMock = campaignWorkspacePage.mockCustomReportFolderStatisticsRequest();
    tree.selectNode('CampaignFolder-1');
    statFolderBundleMock.wait();
    campaignWorkspacePage.treeMenu.assertDeleteButtonIsActive();

    const campaignStatisticsMock = campaignWorkspacePage.mockCampaignStatisticsRequest();
    tree.selectNode('Campaign-3');
    campaignStatisticsMock.wait();
    campaignWorkspacePage.treeMenu.assertDeleteButtonIsActive();
  });

  it('should show server warnings when deleting', () => {
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes);
    const tree = campaignWorkspacePage.tree;
    campaignWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
    const statFolderBundleMock = campaignWorkspacePage.mockCustomReportFolderStatisticsRequest();
    tree.selectNode('CampaignFolder-1');
    statFolderBundleMock.wait();
    campaignWorkspacePage.treeMenu.assertDeleteButtonIsActive();
    const confirmDialog = campaignWorkspacePage.treeMenu.initCampaignWorkspaceDeletion(
      ['CampaignFolder-1'],
      ['warning_milestone', 'warning_called'],
    );
    confirmDialog.checkWarningMessages(['warning_milestone', 'warning_called']);
  });

  it('should delete node and remove it from tree', () => {
    const c = mockTreeNode({
      id: 'Campaign-3',
      children: [],
      data: { NAME: 'a nice test' },
      projectId: 1,
      parentRowId: 'CampaignLibrary-1',
    });
    const cf2 = mockTreeNode({
      id: 'CampaignFolder-2',
      children: [],
      data: { NAME: 'folder2' },
      projectId: 1,
      parentRowId: 'CampaignLibrary-1',
    });
    const s1 = mockTreeNode({
      id: 'Sprint-1',
      projectId: 1,
      parentRowId: 'CampaignLibrary-1',
      data: { NAME: 'Sprint 1', STATUS: SprintStatus.UPCOMING.id },
    });
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes);
    const tree = campaignWorkspacePage.tree;
    campaignWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
    const statFolderBundleMock = campaignWorkspacePage.mockCustomReportFolderStatisticsRequest();
    tree.selectNode('CampaignFolder-1');
    statFolderBundleMock.wait();
    campaignWorkspacePage.treeMenu.assertDeleteButtonIsActive();
    const confirmDialog = campaignWorkspacePage.treeMenu.initCampaignWorkspaceDeletion([
      'CampaignFolder-1',
    ]);
    const libRefreshedAfterDelete = mockTreeNode({
      id: 'CampaignLibrary-1',
      projectId: 1,
      children: ['Campaign-3', 'CampaignFolder-2', 'Sprint-1'],
      data: { NAME: 'Project1', CHILD_COUNT: '3' },
      state: DataRowOpenState.open,
    });
    const refreshedContent = [libRefreshedAfterDelete, c, cf2, s1];
    const selectedParentMock = new HttpMockBuilder('/campaign-library-view/1?**')
      .responseBody({})
      .build();
    confirmDialog.deleteNodes(['CampaignFolder-1'], ['CampaignLibrary-1'], refreshedContent);
    selectedParentMock.wait();
    campaignWorkspacePage.tree.assertNodeNotExist('CampaignFolder-1');
    campaignWorkspacePage.tree.assertNodeExist('CampaignFolder-2');
    campaignWorkspacePage.tree.assertNodeExist('Campaign-3');
    campaignWorkspacePage.tree.assertNodeExist('Sprint-1');
  });
});
