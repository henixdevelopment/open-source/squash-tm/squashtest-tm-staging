import {
  ALL_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder,
} from '../../../../utils/referential/referential-data-builder';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

describe('Campaign Workspace Tree Move', function () {
  function referentialData() {
    return new ReferentialDataMockBuilder()
      .withProjects(
        {
          name: 'International Space Station',
          permissions: ALL_PROJECT_PERMISSIONS,
        },
        {
          name: 'STS - Shuttle',
          permissions: ALL_PROJECT_PERMISSIONS,
        },
      )
      .build();
  }

  const initialNodes = mockGridResponse('id', [
    mockTreeNode({
      id: 'CampaignLibrary-1',
      projectId: 1,
      children: [],
      data: { NAME: 'International Space Station', CHILD_COUNT: '3' },
    }),
    mockTreeNode({
      id: 'CampaignLibrary-2',
      projectId: 2,
      children: [],
      data: { NAME: 'STS - Shuttle', CHILD_COUNT: '3' },
    }),
  ]);

  const cmpLib1 = mockTreeNode({
    id: 'CampaignLibrary-1',
    projectId: 1,
    children: ['CampaignFolder-1', 'Campaign-3', 'CampaignFolder-2'],
    data: { NAME: 'International Space Station', CHILD_COUNT: '3' },
    state: DataRowOpenState.open,
  });

  const cmpFolder1 = mockTreeNode({
    id: 'CampaignFolder-1',
    children: [],
    projectId: 1,
    parentRowId: 'CampaignLibrary-1',
    data: { NAME: 'Structural Campaigns' },
    state: DataRowOpenState.closed,
  });

  const cmp3 = mockTreeNode({
    id: 'Campaign-3',
    children: [],
    parentRowId: 'CampaignLibrary-1',
    state: DataRowOpenState.leaf,
    projectId: 1,
    data: {
      CLN_ID: 3,
      CHILD_COUNT: 0,
      NAME: "Find lot's of bucks",
      IS_SYNCHRONIZED: false,
    },
  });

  const cmpFolder2 = mockTreeNode({
    id: 'CampaignFolder-2',
    children: [],
    projectId: 1,
    parentRowId: 'CampaignLibrary-1',
    data: { NAME: 'Functional Campaigns' },
    state: DataRowOpenState.closed,
  });

  const libraryRefreshAtOpen = [cmpLib1, cmpFolder1, cmp3, cmpFolder2];

  it('should init move and show target according to destination', () => {
    const firstNode = initialNodes.dataRows[0];
    const requirementWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes);
    new NavBarElement().toggle();
    requirementWorkspacePage.treeMenu.sortTreePositional();
    const tree = requirementWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.beginDragAndDrop(cmpFolder1.id);
    tree.assertNodeNotExist(cmpFolder1.id);
    tree.dragOverTopPart(cmpFolder2.id);
    tree.assertDragAndDropTargetIsVisible(2);
    tree.dragOverCenter(cmpFolder2.id);
    tree.assertContainerIsDndTarget(cmpFolder2.id);
    tree.dragOverBottomPart(cmpFolder2.id);
    tree.assertDragAndDropTargetIsVisible(3);
    tree.cancelDnd();
    tree.assertNodeExist(cmpFolder1.id);
  });

  it('should drop into folder', () => {
    const firstNode = initialNodes.dataRows[0];
    const requirementWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes);
    new NavBarElement().toggle();
    requirementWorkspacePage.treeMenu.sortTreePositional();
    const tree = requirementWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.beginDragAndDrop(cmpFolder1.id);
    tree.assertNodeNotExist(cmpFolder1.id);
    tree.dragOverCenter(cmpFolder2.id);
    tree.assertContainerIsDndTarget(cmpFolder2.id);
    const refreshedRows = [
      { ...cmpLib1, children: [cmp3.id, cmpFolder2.id] },
      { ...cmpFolder1, parentRowId: cmpFolder2.id },
      { ...cmp3 },
      { ...cmpFolder2, children: [cmpFolder1.id], state: DataRowOpenState.open },
    ];
    tree.drop(cmpFolder2.id, 'CampaignLibrary-1,CampaignFolder-2', refreshedRows);
    tree.assertRowHasParent(cmpFolder1.id, cmpFolder2.id);
  });

  it('should suspend drag and resume when coming back into tree', () => {
    const firstNode = initialNodes.dataRows[0];
    const requirementWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes);
    new NavBarElement().toggle();
    requirementWorkspacePage.treeMenu.sortTreePositional();
    const tree = requirementWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.beginDragAndDrop(cmpFolder1.id);
    tree.assertNodeNotExist(cmpFolder1.id);
    tree.dragOverTopPart(cmpFolder2.id);
    tree.assertDragAndDropTargetIsVisible(2);
    tree.suspendDnd();
    tree.assertDragAndDropTargetIsNotVisible();
    tree.assertNodeExist(cmpFolder1.id);
    tree.dragOverCenter(cmpFolder2.id);
    tree.assertContainerIsDndTarget(cmpFolder2.id);
    tree.assertNodeNotExist(cmpFolder1.id);
    tree.suspendDnd();
    tree.assertNodeExist(cmpFolder1.id);
    tree.assertContainerIsNotDndTarget(cmpFolder2.id);
    tree.cancelDnd();
  });

  it('should show rows when dragging multiple rows', () => {
    const firstNode = initialNodes.dataRows[0];
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes);
    new NavBarElement().toggle();
    campaignWorkspacePage.treeMenu.sortTreePositional();
    const tree = campaignWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    const statisticsMock = campaignWorkspacePage.mockCampaignStatisticsRequest();
    tree.selectNode(cmp3.id);
    statisticsMock.wait();
    tree.addNodeToSelection(cmpFolder1.id, {});
    tree.beginDragAndDrop(cmp3.id);
    tree.assertNodeNotExist(cmp3.id);
    tree.assertNodeNotExist(cmpFolder1.id);
    tree.assertDndPlaceholderContains(cmp3.id, "Find lot's of bucks");
    tree.assertDndPlaceholderContains(cmpFolder1.id, 'Structural Campaigns');
  });

  it('should show warning when dropping into another project', () => {
    const firstNode = initialNodes.dataRows[0];
    const requirementWorkspacePage = CampaignWorkspacePage.initTestAtPage(
      initialNodes,
      referentialData(),
    );
    new NavBarElement().toggle();
    requirementWorkspacePage.treeMenu.sortTreePositional();
    const tree = requirementWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.beginDragAndDrop(cmp3.id);
    tree.dragOverCenter('CampaignLibrary-2');
    const confirmInterProjectMove = tree.dropIntoOtherProject('CampaignLibrary-2');
    confirmInterProjectMove.assertExists();
    confirmInterProjectMove.cancel();
    tree.assertNodeExist(cmp3.id);
    tree.assertRowHasParent(cmp3.id, cmpLib1.id);
  });
});
