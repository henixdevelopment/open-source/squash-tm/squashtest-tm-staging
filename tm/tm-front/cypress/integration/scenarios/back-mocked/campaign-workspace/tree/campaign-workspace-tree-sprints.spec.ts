import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { TreeElement } from '../../../../page-objects/elements/grid/grid.element';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

describe('Campaign Workspace Tree - Sprints', function () {
  const initialNodes = mockGridResponse('id', [
    mockTreeNode({
      id: 'Sprint-1',
      children: [],
      data: {
        CLN_ID: 1,
        NAME: 'Sprint 1',
        LAST_SYNC_STATUS: 'SUCCESS',
        IS_SYNCHRONIZED: true,
        REMOTE_SYNCHRONISATION_ID: 1,
        STATUS: 'CLOSED',
        REMOTE_STATE: 'OPEN',
        IS_DELETED_SPRINT: false,
        HAS_OUT_OF_PERIMETER_OR_DELETED_REMOTE_REQ: false,
      },
      state: DataRowOpenState.open,
    }),
    mockTreeNode({
      id: 'Sprint-2',
      children: [],
      data: {
        CLN_ID: 2,
        NAME: 'Sprint 2',
        LAST_SYNC_STATUS: 'SUCCESS',
        IS_SYNCHRONIZED: true,
        REMOTE_SYNCHRONISATION_ID: 1,
        STATUS: 'UPCOMING',
        REMOTE_STATE: 'UPCOMING',
        IS_DELETED_SPRINT: false,
        HAS_OUT_OF_PERIMETER_OR_DELETED_REMOTE_REQ: true,
      },
      state: DataRowOpenState.open,
    }),
    mockTreeNode({
      id: 'Sprint-3',
      children: [],
      data: {
        CLN_ID: 3,
        NAME: 'Sprint 3',
        LAST_SYNC_STATUS: 'FAILURE',
        IS_SYNCHRONIZED: true,
        REMOTE_SYNCHRONISATION_ID: 1,
        STATUS: 'OPEN',
        REMOTE_STATE: 'OPEN',
        IS_DELETED_SPRINT: false,
        HAS_OUT_OF_PERIMETER_OR_DELETED_REMOTE_REQ: false,
      },
      state: DataRowOpenState.open,
    }),
    mockTreeNode({
      id: 'Sprint-4',
      children: [],
      data: {
        CLN_ID: 4,
        NAME: 'Sprint 4',
        LAST_SYNC_STATUS: 'SUCCESS',
        IS_SYNCHRONIZED: true,
        REMOTE_SYNCHRONISATION_ID: 1,
        STATUS: 'OPEN',
        REMOTE_STATE: 'CLOSED',
        IS_DELETED_SPRINT: false,
        HAS_OUT_OF_PERIMETER_OR_DELETED_REMOTE_REQ: false,
      },
      state: DataRowOpenState.open,
    }),
    mockTreeNode({
      id: 'Sprint-5',
      children: [],
      data: {
        CLN_ID: 5,
        NAME: 'Sprint 5',
        LAST_SYNC_STATUS: 'SUCCESS',
        IS_SYNCHRONIZED: true,
        REMOTE_SYNCHRONISATION_ID: 1,
        STATUS: 'UPCOMING',
        REMOTE_STATE: 'UPCOMING',
        IS_DELETED_SPRINT: true,
        HAS_OUT_OF_PERIMETER_OR_DELETED_REMOTE_REQ: true,
      },
      state: DataRowOpenState.open,
    }),
  ]);

  it('should display sprints synchronization status icon with correct color', () => {
    const campaignWorkspacePage: CampaignWorkspacePage =
      CampaignWorkspacePage.initTestAtPage(initialNodes);
    const tree: TreeElement = campaignWorkspacePage.tree;
    tree.assertSprintSynchronizationStatusIconHasCorrectCssClass('Sprint-1', 'remote-sync-success');
    tree.assertSprintSynchronizationStatusIconHasCorrectCssClass('Sprint-2', 'remote-sync-partial');
    tree.assertSprintSynchronizationStatusIconHasCorrectCssClass('Sprint-3', 'remote-sync-failed');
    tree.assertSprintHasCorrectRemoteSynchronizationStatusIcon(
      'Sprint-4',
      'sqtm-core-generic:locked',
    );
    tree.assertSprintHasCorrectRemoteSynchronizationStatusIcon(
      'Sprint-5',
      'sqtm-core-generic:delete',
    );
  });
});
