import { TestSuiteViewPage } from '../../../../page-objects/pages/campaign-workspace/test-suite/test-suite-view.page';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { createEntityReferentialData } from '../../../../utils/referential/create-entity-referential.const';
import { TestSuiteModel } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/test-suite.model';
import { mockTestSuiteModel } from '../../../../data-mock/iteration.data-mock';
import {
  DataRowModel,
  DataRowOpenState,
} from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { mockDataRow, mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

describe('Test Suite - Information', () => {
  it('should display test-suite informations', () => {
    const testSuiteViewPage = navigateToTestSuite();
    testSuiteViewPage.checkData('test-suite-id', '5');
    testSuiteViewPage.checkData('test-suite-created', '09/03/2020 10:30 (Bébert)');
    testSuiteViewPage.checkData('test-suite-lastModified', '09/03/2020 11:30 (Riton)');
    testSuiteViewPage.checkData('execution-status', 'En cours');
    testSuiteViewPage.checkData('test-suite-progress-state', 'À exécuter');
    testSuiteViewPage.descriptionRichField.checkTextContent('this is a wonderful test suite');
  });

  it('should allow modifications on test-suite informations', () => {
    const testSuiteViewPage = navigateToTestSuite();
    const initialTestSuiteRow: DataRowModel = mockDataRow({
      id: 'TestSuite-5',
      children: [],
      projectId: 1,
      parentRowId: 'Iteration-4',
      state: DataRowOpenState.leaf,
      data: {
        NAME: 'testSuite-5',
        CHILD_COUNT: 0,
        MILESTONE_STATUS: 'IN_PROGRESS',
        EXECUTION_STATUS: 'RUNNING',
      },
    });

    testSuiteViewPage.rename('CHOUBIDOU BIDOU WA', {
      dataRows: [
        {
          ...initialTestSuiteRow,
          data: { ...initialTestSuiteRow.data, NAME: 'CHOUBIDOU BIDOU WA' },
        },
      ],
    });

    const descriptionElement = testSuiteViewPage.descriptionRichField;
    descriptionElement.enableEditMode();
    descriptionElement.setValue('Hello, World!');
    descriptionElement.confirm('Hello, World!');
  });

  function navigateToTestSuite(): TestSuiteViewPage {
    const initialNodes = mockGridResponse('id', [
      mockTreeNode({
        id: 'CampaignLibrary-1',
        children: ['Campaign-3'],
        data: { NAME: 'Project1', CHILD_COUNT: 1 },
        state: DataRowOpenState.open,
      }),
      mockTreeNode({
        id: 'Campaign-3',
        children: ['Iteration-4'],
        projectId: 1,
        parentRowId: 'CampaignLibrary-1',
        state: DataRowOpenState.open,
        data: { NAME: 'campaign3', CHILD_COUNT: 1, MILESTONE_STATUS: 'IN_PROGRESS' },
      }),
      mockTreeNode({
        id: 'Iteration-4',
        children: ['TestSuite-5'],
        projectId: 1,
        parentRowId: 'Campaign-3',
        state: DataRowOpenState.open,
        data: { NAME: 'iteration-4', CHILD_COUNT: 1, MILESTONE_STATUS: 'IN_PROGRESS' },
      }),
      mockTreeNode({
        id: 'TestSuite-5',
        children: [],
        projectId: 1,
        parentRowId: 'Iteration-4',
        state: DataRowOpenState.leaf,
        data: {
          NAME: 'testSuite-5',
          CHILD_COUNT: 0,
          MILESTONE_STATUS: 'IN_PROGRESS',
          EXECUTION_STATUS: 'RUNNING',
        },
      }),
    ]);

    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(
      initialNodes,
      createEntityReferentialData,
    );

    const executionStatusMap = new Map<number, string>();
    executionStatusMap.set(1, 'READY');
    executionStatusMap.set(2, 'SUCCESS');

    const model: TestSuiteModel = mockTestSuiteModel({
      id: 5,
      projectId: 1,
      name: 'testSuite-5',
      description: 'this is a wonderful test suite',
      uuid: '3e448aca-436a-11eb-a12b-5c80b64fb103',
      executionStatus: 'RUNNING',
      createdOn: new Date('2020-03-09 10:30').toISOString(),
      createdBy: 'Bébert',
      lastModifiedOn: new Date('2020-03-09 11:30').toISOString(),
      lastModifiedBy: 'Riton',
      testPlanStatistics: {
        status: 'RUNNING',
        progression: 50,
        nbTestCases: 2,
        nbDone: 1,
        nbReady: 1,
        nbRunning: 0,
        nbUntestable: 0,
        nbBlocked: 0,
        nbFailure: 0,
        nbSettled: 0,
        nbSuccess: 1,
      },
      customFieldValues: [],
      attachmentList: {
        id: 1,
        attachments: [],
      },
      hasDatasets: true,
      users: [
        { id: 1, login: 'raowl', firstName: 'Ra', lastName: 'Oul' },
        { id: 2, login: 'jawny', firstName: 'Joe', lastName: 'Ni' },
      ],
      executionStatusMap: executionStatusMap,
    });

    new HttpMockBuilder(
      `test-automation/test-suite/*/automated-execution-environments-statuses-count`,
    )
      .get()
      .responseBody({
        statuses: null,
        nbOfSquashOrchestratorServers: 0,
        nbOfUnreachableSquashOrchestrators: null,
        nbOfServersWithMissingToken: null,
      })
      .build();

    return campaignWorkspacePage.tree.selectNode<TestSuiteViewPage>('TestSuite-5', model);
  }
});
