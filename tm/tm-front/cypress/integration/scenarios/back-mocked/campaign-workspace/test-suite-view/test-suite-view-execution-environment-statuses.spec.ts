import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { TestSuiteViewPage } from '../../../../page-objects/pages/campaign-workspace/test-suite/test-suite-view.page';
import {
  ALL_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder,
} from '../../../../utils/referential/referential-data-builder';
import { TestSuiteModel } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/test-suite.model';
import { mockTestSuiteModel } from '../../../../data-mock/iteration.data-mock';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { EnvironmentStatusesCountDTO } from '../../../../page-objects/elements/automated-execution-environments/environments-statuses-count.element';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

describe('Test Suite Test Plan View', () => {
  it('Execution Environments count should be visible and global status should be ok', () => {
    const testSuiteViewPage = navigateToTestSuite();
    const countElement = testSuiteViewPage.environmentsStatusesCountElement;
    countElement.assertCountIsVisible();
    countElement.assertCapsuleIsClickable();
    countElement.assertGlobalStatusIconIs('check-circle');
    countElement.assertIconColorIs('IDLE', 'rgb(98, 115, 127)');
    countElement.assertCountIs('IDLE', 12);
    countElement.assertCountIs('BUSY', 3);
    countElement.assertCountIs('PENDING', 0);
    countElement.assertCountIs('UNREACHABLE', 0);
  });

  it('Should refresh execution environments count component with new warning values', () => {
    const testSuiteViewPage = navigateToTestSuite();
    const countElement = testSuiteViewPage.environmentsStatusesCountElement;
    countElement.assertCountIsVisible();
    countElement.assertCapsuleIsClickable();
    countElement.assertCountIs('IDLE', 12);
    countElement.assertCountIs('BUSY', 3);
    countElement.assertCountIs('PENDING', 0);
    countElement.assertCountIs('UNREACHABLE', 0);

    const newCountResponseBody: EnvironmentStatusesCountDTO = {
      statuses: {
        IDLE: 4,
        BUSY: 2,
        PENDING: 6,
        UNREACHABLE: 1,
      },
      projects: [
        {
          projectId: 1,
          projectName: 'project 01',
          squashOrchestratorId: 5,
          squashOrchestratorName: 'the orchestrator 05',
        },
        {
          projectId: 2,
          projectName: 'project 02',
          squashOrchestratorId: 5,
          squashOrchestratorName: 'the orchestrator 05',
        },
        {
          projectId: 3,
          projectName: 'project 03',
          squashOrchestratorId: 7,
          squashOrchestratorName: 'the orchestrator 07',
        },
        {
          projectId: 4,
          projectName: 'project 04',
          squashOrchestratorId: 8,
          squashOrchestratorName: 'the orchestrator 08',
        },
      ],
      nbOfSquashOrchestratorServers: 3,
      nbOfUnreachableSquashOrchestrators: 1,
      nbOfServersWithMissingToken: 1,
    };
    countElement.refreshCountComponent(newCountResponseBody, 'test-suite');

    countElement.assertCountIsVisible();
    countElement.assertCapsuleIsClickable();
    countElement.assertGlobalStatusIconIs('warning');
    countElement.assertIconColorIs('IDLE', 'rgb(98, 115, 127)');

    countElement.assertCountIs('IDLE', 4);
    countElement.assertCountIs('BUSY', 2);
    countElement.assertCountIs('PENDING', 6);
    countElement.assertCountIs('UNREACHABLE', 1);
  });

  it('Should refresh execution environments count component with new error values', () => {
    const testSuiteViewPage = navigateToTestSuite();
    const countElement = testSuiteViewPage.environmentsStatusesCountElement;
    countElement.assertCountIsVisible();
    countElement.assertCapsuleIsClickable();
    countElement.assertCountIs('IDLE', 12);
    countElement.assertCountIs('BUSY', 3);
    countElement.assertCountIs('PENDING', 0);
    countElement.assertCountIs('UNREACHABLE', 0);

    const newCountResponseBody: EnvironmentStatusesCountDTO = {
      statuses: null,
      projects: [
        {
          projectId: 1,
          projectName: 'project 01',
          squashOrchestratorId: 5,
          squashOrchestratorName: 'the orchestrator 05',
        },
        {
          projectId: 2,
          projectName: 'project 02',
          squashOrchestratorId: 5,
          squashOrchestratorName: 'the orchestrator 05',
        },
        {
          projectId: 3,
          projectName: 'project 03',
          squashOrchestratorId: 7,
          squashOrchestratorName: 'the orchestrator 07',
        },
        {
          projectId: 4,
          projectName: 'project 04',
          squashOrchestratorId: 8,
          squashOrchestratorName: 'the orchestrator 08',
        },
      ],
      nbOfSquashOrchestratorServers: 3,
      nbOfUnreachableSquashOrchestrators: 3,
      nbOfServersWithMissingToken: 0,
    };
    countElement.refreshCountComponent(newCountResponseBody, 'test-suite');

    countElement.assertCountIsVisible();
    countElement.assertCapsuleIsNotClickable();
    countElement.assertGlobalStatusIconIs('close');
    countElement.assertIconColorIs('IDLE', 'rgb(255, 205, 200)');
    countElement.assertCountIs('IDLE', 0);
    countElement.assertCountIs('BUSY', 0);
    countElement.assertCountIs('PENDING', 0);
    countElement.assertCountIs('UNREACHABLE', 0);
  });
});

function navigateToTestSuite(): TestSuiteViewPage {
  const refData = new ReferentialDataMockBuilder()
    .withProjects({
      allowAutomationWorkflow: true,
      permissions: ALL_PROJECT_PERMISSIONS,
    })
    .build();
  const initialNodes: GridResponse = mockGridResponse('id', [
    mockTreeNode({
      id: 'CampaignLibrary-1',
      children: ['Campaign-3'],
      data: { NAME: 'Project1', CHILD_COUNT: 1 },
      state: DataRowOpenState.open,
    }),
    mockTreeNode({
      id: 'Campaign-3',
      children: ['Iteration-4'],
      projectId: 1,
      parentRowId: 'CampaignLibrary-1',
      state: DataRowOpenState.open,
      data: { NAME: 'campaign3', CHILD_COUNT: 1, MILESTONE_STATUS: 'IN_PROGRESS' },
    }),
    mockTreeNode({
      id: 'Iteration-4',
      children: ['TestSuite-5'],
      projectId: 1,
      parentRowId: 'Campaign-3',
      state: DataRowOpenState.open,
      data: { NAME: 'iteration-4', CHILD_COUNT: 1, MILESTONE_STATUS: 'IN_PROGRESS' },
    }),
    mockTreeNode({
      id: 'TestSuite-5',
      children: [],
      projectId: 1,
      parentRowId: 'Iteration-4',
      state: DataRowOpenState.leaf,
      data: {
        NAME: 'testSuite-5',
        CHILD_COUNT: 0,
        MILESTONE_STATUS: 'IN_PROGRESS',
        EXECUTION_STATUS: 'RUNNING',
      },
    }),
  ]);
  const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes, refData);

  new HttpMockBuilder<EnvironmentStatusesCountDTO>(
    `test-automation/test-suite/*/automated-execution-environments-statuses-count`,
  )
    .get()
    .responseBody({
      statuses: {
        IDLE: 12,
        BUSY: 3,
      },
      projects: [
        {
          projectId: 1,
          projectName: 'project 01',
          squashOrchestratorId: 5,
          squashOrchestratorName: 'the orchestrator',
        },
      ],
      nbOfSquashOrchestratorServers: 1,
      nbOfUnreachableSquashOrchestrators: 0,
      nbOfServersWithMissingToken: 0,
    })
    .build();

  return campaignWorkspacePage.tree.selectNode<TestSuiteViewPage>(
    'TestSuite-5',
    buildDefaultTestSuiteModel(),
  );
}

function buildDefaultTestSuiteModel(): TestSuiteModel {
  return mockTestSuiteModel({
    id: 5,
    projectId: 1,
    name: 'testSuite-5',
    description: 'this is a wonderful test suite',
    uuid: '3e448aca-436a-11eb-a12b-5c80b64fb103',
    executionStatus: 'RUNNING',
    createdOn: new Date('2020-03-09 10:30').toISOString(),
    createdBy: 'Bébert',
    lastModifiedOn: new Date('2020-03-09 11:30').toISOString(),
    lastModifiedBy: 'Riton',
    testPlanStatistics: {
      status: 'RUNNING',
      progression: 50,
      nbTestCases: 2,
      nbDone: 1,
      nbReady: 1,
      nbRunning: 0,
      nbUntestable: 0,
      nbBlocked: 0,
      nbFailure: 0,
      nbSettled: 0,
      nbSuccess: 1,
    },
    customFieldValues: [],
    attachmentList: {
      id: 1,
      attachments: [],
    },
    hasDatasets: true,
    users: [
      { id: 1, login: 'raowl', firstName: 'Ra', lastName: 'Oul' },
      { id: 2, login: 'jawny', firstName: 'Joe', lastName: 'Ni' },
    ],
    executionStatusMap: new Map<number, string>(),
  });
}
