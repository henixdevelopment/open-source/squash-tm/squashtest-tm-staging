import { createEntityReferentialData } from '../../../../utils/referential/create-entity-referential.const';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { ItpiSearchPage } from '../../../../page-objects/pages/campaign-workspace/search/itpi-search-page';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { mockDataRow } from '../../../../data-mock/grid.data-mock';

describe('ITPI Search Results', function () {
  beforeEach(() => {
    cy.viewport(1200, 720);
  });

  describe('Requirement search table', () => {
    it('should display row in table', () => {
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: [itpiBasic],
      };
      const requirementSearchPage = ItpiSearchPage.initTestAtPage(
        createEntityReferentialData,
        gridResponse,
      );
      const gridElement = requirementSearchPage.grid;
      new NavBarElement().toggle();
      requirementSearchPage.foldFilterPanel();

      gridElement.assertRowExist(1);
      const row = gridElement.getRow(1);
      row.cell(GridColumnId.projectName).textRenderer().assertContainsText('project 1');
      row.cell('reference').textRenderer().assertContainsText('ref1');
      row.cell('label').textRenderer().assertContainsText('Test Case 1');
      row.cell('iterationName').textRenderer().assertContainsText('Iteration 1');
      row.cell('campaignName').textRenderer().assertContainsText('Campagne 1');
    });
  });
});

const itpiBasic = mockDataRow({
  id: '1',
  type: 'Campaign',
  projectId: 1,
  data: {
    label: 'Test Case 1',
    id: 1,
    reference: 'ref1',
    [GridColumnId.projectName]: 'project 1',
    iterationName: 'Iteration 1',
    campaignName: 'Campagne 1',
    importance: 'LOW',
    datasetName: 'JDD 1',
    testSuites: 'Suite 1',
    automatable: 'M',
    [GridColumnId.inferredExecutionMode]: 'UNDEFINED',
    executionStatus: 'SUCCESS',
    lastExecutedBy: 'Admin',
    lastExecutedOn: new Date(),
  },
});
