import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { createEntityReferentialData } from '../../../../utils/referential/create-entity-referential.const';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { TestCaseForCampaignWorkspaceSearchPage } from '../../../../page-objects/pages/campaign-workspace/search/test-case-for-campaign-workspace-search-page';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { mockDataRow } from '../../../../data-mock/grid.data-mock';

describe('Test Case search for test suite test plan', () => {
  beforeEach(() => {
    cy.viewport(1200, 720);
  });

  describe('Test Case search table', () => {
    const testCase = mockDataRow({
      id: '1',
      type: 'TestCase',
      projectId: 1,
      data: {
        name: 'Test 1',
        id: 1,
        reference: 'ref1',
        [GridColumnId.projectName]: 'project 1',
        attachments: 0,
        items: 0,
        steps: 0,
        nature: 14,
        type: 16,
        automatable: 'Y',
        status: 'WORK_IN_PROGRESS',
        importance: 'LOW',
        createdBy: 'admin',
        lastModifiedBy: 'admin',
        tcMilestoneLocked: 0,
        reqMilestoneLocked: 0,
      },
    });

    it('should show links buttons and activate according to user selection', () => {
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: [testCase],
      };
      const testCaseSearchPage = TestCaseForCampaignWorkspaceSearchPage.initTestAtPage(
        'test-suite',
        '1',
        createEntityReferentialData,
        gridResponse,
      );
      const gridElement = testCaseSearchPage.grid;

      new NavBarElement().toggle();
      testCaseSearchPage.foldFilterPanel();
      testCaseSearchPage.assertLinkSelectionButtonExist();
      testCaseSearchPage.assertLinkSelectionButtonIsNotActive();
      testCaseSearchPage.assertLinkAllButtonExist();
      testCaseSearchPage.assertLinkAllButtonIsActive();
      testCaseSearchPage.assertNavigateBackButtonExist();
      testCaseSearchPage.assertNavigateBackButtonIsActive();
      gridElement.selectRow('1', '#', 'leftViewport');
      testCaseSearchPage.assertLinkSelectionButtonIsActive();
      gridElement.toggleRow('1', '#', 'leftViewport');
      testCaseSearchPage.assertLinkSelectionButtonIsNotActive();
    });

    it('should add a test case to test plan', () => {
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: [testCase],
      };
      const testCaseSearchPage = TestCaseForCampaignWorkspaceSearchPage.initTestAtPage(
        'test-suite',
        '1',
        createEntityReferentialData,
        gridResponse,
      );
      const gridElement = testCaseSearchPage.grid;

      new NavBarElement().toggle();
      gridElement.selectRow('1', '#', 'leftViewport');

      const campaignTreeMock = buildCampaignTreeMock();
      testCaseSearchPage.linkSelection();
      campaignTreeMock.wait();
      assertRedirectionToCampaignWorkspaceDone();
    });

    it('should add all test cases to test plan', () => {
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: [testCase],
      };
      const testCaseSearchPage = TestCaseForCampaignWorkspaceSearchPage.initTestAtPage(
        'test-suite',
        '1',
        createEntityReferentialData,
        gridResponse,
      );

      new NavBarElement().toggle();
      const campaignTreeMock = buildCampaignTreeMock();
      testCaseSearchPage.linkAll();
      campaignTreeMock.wait();
      assertRedirectionToCampaignWorkspaceDone();
    });
  });
});

function assertRedirectionToCampaignWorkspaceDone() {
  cy.url().should('equal', `${Cypress.config().baseUrl}/campaign-workspace`);
}

function buildCampaignTreeMock() {
  const campaignTree: GridResponse = { count: 0, dataRows: [] };
  return new HttpMockBuilder(`/campaign-tree`).post().responseBody(campaignTree).build();
}
