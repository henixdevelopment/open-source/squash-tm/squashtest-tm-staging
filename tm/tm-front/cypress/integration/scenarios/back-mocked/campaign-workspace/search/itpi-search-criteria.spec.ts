import {
  ALL_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder,
} from '../../../../utils/referential/referential-data-builder';
import { ItpiSearchPage } from '../../../../page-objects/pages/campaign-workspace/search/itpi-search-page';
import { EditableDateFieldElement } from '../../../../page-objects/elements/forms/editable-date-field.element';
import { UserView } from '../../../../../../projects/sqtm-core/src/lib/model/user/user-view';

const INFORMATION_GROUP_LABEL = 'Information';
const AUTOMATION_GROUP_LABEL = 'Automatisation';
const MILESTONE_GROUP_LABEL = 'Jalons';
const ATTRIBUTE_GROUP_LABEL = 'Attribut';
const EXECUTION_GROUP_LABEL = 'Exécution';

const TEST_CASE_ID_LABEL = 'ID cas de test';
const REFERENCE_LABEL = 'Référence';
const NAME_LABEL = 'Nom';
const AUTOMATABLE_LABEL = "Éligibilité à l'automatisation";
const AUTOMATION_STATUS_LABEL = "Statut d'automatisation";
const IMPORTANCE_LABEL = `Importance`;
const ASSIGNEE_LABEL = `Assignation`;
const EXECUTED_ON_LABEL = `Exécuté le`;
const EXECUTED_BY_LABEL = `Exécuté par`;
const EXECUTION_STATUS_LABEL = `Statut`;
const EXECUTION_MODE_LABEL = `Mode`;
const MILESTONE_NAME = 'Nom du jalon';
const MILESTONE_STATUS = 'Statut du jalon';
const MILESTONE_END_DATE = "Date d'échéance";

const OPERATION_LABELS = {
  EQUALS: 'Égal',
  GREATER: 'Supérieur',
  GREATER_EQUALS: 'Supérieur ou égal',
  BETWEEN: 'Entre',
};

function referentialDataWithMilestones() {
  return new ReferentialDataMockBuilder()
    .withProjects(
      {
        name: 'Project 1',
        permissions: ALL_PROJECT_PERMISSIONS,
      },
      {
        name: 'Project 2',
        permissions: ALL_PROJECT_PERMISSIONS,
      },
      {
        name: 'Project 3',
        permissions: ALL_PROJECT_PERMISSIONS,
      },
    )
    .withMilestones(
      {
        label: 'Milestone 1',
        status: 'IN_PROGRESS',
        endDate: new Date('2020-05-19').toISOString(),
        boundProjectIndexes: [0, 1],
      },
      {
        label: 'Milestone 2',
        status: 'IN_PROGRESS',
        endDate: new Date('2020-05-19').toISOString(),
        boundProjectIndexes: [0, 1],
      },
      {
        label: 'Milestone 3',
        status: 'PLANNED',
        endDate: new Date('2020-05-19').toISOString(),
        boundProjectIndexes: [0],
      },
      {
        label: 'Milestone 4',
        status: 'FINISHED',
        endDate: new Date('2020-05-19').toISOString(),
        boundProjectIndexes: [0],
      },
    )
    .build();
}

describe('ITPI Search', function () {
  beforeEach(() => {
    cy.viewport(1200, 720);
  });

  it('should display itpi search page', () => {
    const itpiSearchPage = ItpiSearchPage.initTestAtPage();
    itpiSearchPage.grid.filterPanel.assertPerimeterIsActive();
    itpiSearchPage.grid.filterPanel.assertPerimeterHasValue('project 1');
  });

  it('should show criteria list', () => {
    const referentialData = referentialDataWithMilestones();
    const itpiSearchPage = ItpiSearchPage.initTestAtPage(referentialData);
    const criteriaList = itpiSearchPage.grid.filterPanel.openCriteriaList();
    criteriaList.assertGroupExist(INFORMATION_GROUP_LABEL);
    criteriaList.assertGroupContain(INFORMATION_GROUP_LABEL, [
      NAME_LABEL,
      REFERENCE_LABEL,
      TEST_CASE_ID_LABEL,
    ]);
    criteriaList.assertGroupExist(AUTOMATION_GROUP_LABEL);
    criteriaList.assertGroupContain(AUTOMATION_GROUP_LABEL, [
      AUTOMATABLE_LABEL,
      AUTOMATION_STATUS_LABEL,
    ]);
    criteriaList.assertGroupExist(ATTRIBUTE_GROUP_LABEL);
    criteriaList.assertGroupContain(ATTRIBUTE_GROUP_LABEL, [IMPORTANCE_LABEL, ASSIGNEE_LABEL]);
    criteriaList.assertGroupExist(MILESTONE_GROUP_LABEL);
    criteriaList.assertGroupContain(MILESTONE_GROUP_LABEL, [
      MILESTONE_NAME,
      MILESTONE_STATUS,
      MILESTONE_END_DATE,
    ]);

    criteriaList.assertGroupExist(EXECUTION_GROUP_LABEL);
    criteriaList.assertGroupContain(EXECUTION_GROUP_LABEL, [
      EXECUTED_BY_LABEL,
      EXECUTED_ON_LABEL,
      EXECUTION_MODE_LABEL,
      EXECUTION_STATUS_LABEL,
    ]);
  });

  it('should add name criteria and research', () => {
    const itpiSearchPage = ItpiSearchPage.initTestAtPage();
    const textFilterWidgetElement = itpiSearchPage.grid.filterPanel.selectTextCriteria(NAME_LABEL);
    const filterPanel = itpiSearchPage.grid.filterPanel;
    textFilterWidgetElement.cancel();
    textFilterWidgetElement.assertNotExist();
    filterPanel.inactivateCriteria(NAME_LABEL);
    filterPanel.fillTextCriteria(NAME_LABEL, 'STS');
    filterPanel.assertCriteriaIsActive(NAME_LABEL);
    filterPanel.assertCriteriaHasValue(NAME_LABEL, 'sts');
    filterPanel.openExistingCriteria(NAME_LABEL);
  });

  it('should show milestone criteria', () => {
    const referentialData = referentialDataWithMilestones();
    const itpiSearchPage = ItpiSearchPage.initTestAtPage(referentialData);
    const multiList = itpiSearchPage.grid.filterPanel.selectMultiListCriteria(MILESTONE_NAME);
    itpiSearchPage.grid.filterPanel.assertCriteriaIsActive(MILESTONE_NAME);
    multiList.assertGroupContain('ungrouped-items', [
      'Milestone 1',
      'Milestone 2',
      'Milestone 3',
      'Milestone 4',
    ]);
    multiList.close();
    const projectScope = itpiSearchPage.grid.filterPanel.openProjectScopeSelector();
    projectScope.toggleOneItemAsync('Project 1');
    projectScope.close();
    itpiSearchPage.grid.filterPanel.openExistingCriteria(MILESTONE_NAME);
    multiList.assertGroupContain('ungrouped-items', ['Milestone 1', 'Milestone 2']);
  });

  it('should show milestone status criteria', () => {
    const referentialData = referentialDataWithMilestones();
    const itpiSearchPage = ItpiSearchPage.initTestAtPage(referentialData);
    const multiList = itpiSearchPage.grid.filterPanel.selectMultiListCriteria(MILESTONE_STATUS);
    multiList.assertGroupContain('ungrouped-items', ['En cours', 'Terminé', 'Verrouillé']);
    multiList.toggleOneItemAsync('Terminé');
    multiList.toggleOneItemAsync('Verrouillé');
    multiList.close();
    itpiSearchPage.grid.filterPanel.assertCriteriaHasValue(MILESTONE_STATUS, 'Terminé, Verrouillé');
  });

  it('should add executedOn criteria and research', () => {
    const referentialData = referentialDataWithMilestones();
    const itpiSearchPage = ItpiSearchPage.initTestAtPage(referentialData);
    const dateCriteria = itpiSearchPage.grid.filterPanel.selectDateCriteria(EXECUTED_ON_LABEL);
    dateCriteria.assertOperationChosen(OPERATION_LABELS.EQUALS);
    dateCriteria.changeOperation(OPERATION_LABELS.GREATER);
    dateCriteria.fillTodayDate();
    dateCriteria.update();
    const localeToday = EditableDateFieldElement.dateToDisplayString(new Date(Date.now()));
    itpiSearchPage.grid.filterPanel.assertCriteriaHasOperationAndValue(
      EXECUTED_ON_LABEL,
      OPERATION_LABELS.GREATER,
      localeToday,
    );
    itpiSearchPage.grid.filterPanel.openExistingCriteria(EXECUTED_ON_LABEL);
    dateCriteria.changeOperation(OPERATION_LABELS.BETWEEN);
    dateCriteria.assertRangeComponentExist();
  });

  it('should add users criteria and research', () => {
    const usersAssignedTo: UserView[] = [
      { login: 'n-armstrong', firstName: 'Neil', lastName: 'Armstrong', id: 1 },
      { login: 'b-aldrin', firstName: 'Buzz', lastName: 'Aldrin', id: 2 },
      { login: 'm-collins', firstName: 'Michael', lastName: 'Collins', id: 3 },
    ];

    const usersExecutedItpi = [
      { login: 'n-armstrong', firstName: 'Neil', lastName: 'Armstrong', id: 1 },
      { login: 'b-aldrin', firstName: 'Buzz', lastName: 'Aldrin', id: 2 },
    ];

    const referentialData = referentialDataWithMilestones();
    const requirementSearchPage = ItpiSearchPage.initTestAtPage(referentialData, undefined, {
      usersAssignedTo,
      usersExecutedItpi,
    });
    const createdByList =
      requirementSearchPage.grid.filterPanel.selectMultiListCriteria(ASSIGNEE_LABEL);
    createdByList.assertGroupContain('ungrouped-items', [
      'Neil Armstrong (n-armstrong)',
      'Buzz Aldrin (b-aldrin)',
      'Michael Collins (m-collins)',
    ]);
    createdByList.toggleOneItemAsync('Neil Armstrong (n-armstrong)');
    createdByList.close();
    requirementSearchPage.grid.filterPanel.assertCriteriaHasValue(ASSIGNEE_LABEL, 'n-armstrong');
    const modifiedByList =
      requirementSearchPage.grid.filterPanel.selectMultiListCriteria(EXECUTED_BY_LABEL);
    modifiedByList.assertGroupContain('ungrouped-items', [
      'Neil Armstrong (n-armstrong)',
      'Buzz Aldrin (b-aldrin)',
    ]);
    modifiedByList.toggleOneItemAsync('Buzz Aldrin (b-aldrin)');
    modifiedByList.close();
    requirementSearchPage.grid.filterPanel.assertCriteriaHasValue(EXECUTED_BY_LABEL, 'b-aldrin');
  });
});
