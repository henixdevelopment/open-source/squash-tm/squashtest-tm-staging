import { getDefaultCampaignStatisticsBundle } from '../../../../data-mock/campaign.data-mock';
import {
  ALL_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder,
} from '../../../../utils/referential/referential-data-builder';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

describe('Campaign MilestoneDashboard', () => {
  const referentialDataMock = new ReferentialDataMockBuilder()
    .withProjects(
      { name: 'Project 1', label: 'Etiquette', permissions: ALL_PROJECT_PERMISSIONS },
      { name: 'Project 2', label: 'Etiquette 2', permissions: ALL_PROJECT_PERMISSIONS },
    )
    .withMilestones(
      {
        label: 'milestone1',
        description: '',
        endDate: new Date().toISOString(),
        status: 'IN_PROGRESS',
        boundProjectIndexes: [0],
        range: 'GLOBAL',
      },
      {
        label: 'milestone2',
        description: '',
        endDate: new Date().toISOString(),
        status: 'FINISHED',
        boundProjectIndexes: [0],
        range: 'GLOBAL',
      },
      {
        label: 'milestone3',
        description: '',
        endDate: new Date().toISOString(),
        status: 'LOCKED',
        boundProjectIndexes: [1],
        range: 'GLOBAL',
      },
    )
    .withUser({
      hasAnyReadPermission: true,
      functionalTester: true,
    })
    .build();

  const campaignLibrary1 = mockTreeNode({
    id: 'CampaignLibrary-1',
    children: ['CampaignFolder-1', 'Campaign-3', 'Campaign-4', 'CampaignFolder-2'],
    projectId: 1,
    data: { NAME: 'Project 1', CHILD_COUNT: '3', MILESTONES: [1, 2] },
    state: DataRowOpenState.open,
  });
  const campaignLibrary2 = mockTreeNode({
    id: 'CampaignLibrary-2',
    children: ['Campaign-5'],
    projectId: 2,
    data: { NAME: 'Project 2', CHILD_COUNT: '0', MILESTONES: [3] },
    state: DataRowOpenState.open,
  });

  const campaignFolder1 = mockTreeNode({
    id: 'CampaignFolder-1',
    children: [],
    projectId: 1,
    parentRowId: 'CampaignLibrary-1',
    data: { NAME: 'folder1', MILESTONES: [1, 2] },
  });
  const campaignFolder2 = mockTreeNode({
    id: 'CampaignFolder-2',
    children: [],
    projectId: 1,
    parentRowId: 'CampaignLibrary-1',
    data: { NAME: 'folder2', MILESTONES: [1, 2] },
  });

  const campaign3 = mockTreeNode({
    id: 'Campaign-3',
    children: ['Iteration-1'],
    projectId: 1,
    parentRowId: 'CampaignLibrary-1',
    data: {
      NAME: 'campaign number 3',
      MILESTONES: [1],
    },
    state: DataRowOpenState.open,
  });
  const campaign4 = mockTreeNode({
    id: 'Campaign-4',
    children: [],
    projectId: 1,
    parentRowId: 'CampaignLibrary-1',
    data: {
      NAME: 'campaign number 4',
      MILESTONES: [2],
    },
  });
  const campaign5 = mockTreeNode({
    id: 'Campaign-5',
    children: ['Iteration-2'],
    projectId: 1,
    parentRowId: 'CampaignLibrary-2',
    data: {
      NAME: 'campaign number 5',
      MILESTONES: [3],
    },
    state: DataRowOpenState.open,
  });

  const iteration1 = mockTreeNode({
    id: 'Iteration-1',
    children: ['TestSuite-1'],
    projectId: 1,
    parentRowId: 'Campaign-3',
    data: {
      NAME: 'iteration 1',
      MILESTONES: [1],
    },
    state: DataRowOpenState.open,
  });
  const iteration2 = mockTreeNode({
    id: 'Iteration-2',
    children: ['TestSuite-2'],
    projectId: 1,
    parentRowId: 'Campaign-5',
    data: {
      NAME: 'iteration 2',
      MILESTONES: [3],
    },
    state: DataRowOpenState.open,
  });

  const allNodes = mockGridResponse('id', [
    campaignLibrary1,
    campaignLibrary2,
    campaignFolder1,
    campaignFolder2,
    campaign3,
    campaign4,
    campaign5,
    iteration1,
    iteration2,
  ]);

  it('should select milestone above tree buttons', () => {
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(
      allNodes,
      referentialDataMock,
    );
    const navbar = campaignWorkspacePage.navBar;

    const milestonePicker = navbar.openMilestoneSelector();
    milestonePicker.selectMilestone(1);
    milestonePicker.confirm();

    campaignWorkspacePage.treeMenu.assertMilestoneButtonIsVisible();
    const campaignMilestoneSelector = campaignWorkspacePage.treeMenu.showMilestoneSelector();
    campaignMilestoneSelector.assertMilestoneIsSelected(1);
    campaignMilestoneSelector.selectMilestone(2);
    campaignMilestoneSelector.confirm();
    campaignWorkspacePage.treeMenu.showMilestoneSelector();
    campaignMilestoneSelector.assertMilestoneIsSelected(2);
    campaignMilestoneSelector.cancel();
    campaignWorkspacePage.navBar.assertMilestoneModeIsActive();
    campaignWorkspacePage.navBar.openMilestoneSelector();
    campaignMilestoneSelector.assertMilestoneIsSelected(2);
  });

  it('should display campaign milestone dashboard', () => {
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(
      allNodes,
      referentialDataMock,
    );
    const navbar = campaignWorkspacePage.navBar;
    const milestonePicker = navbar.openMilestoneSelector();
    milestonePicker.selectMilestone(1);
    milestonePicker.confirm();

    const milestonePage = campaignWorkspacePage.treeMenu.showCampaignMilestoneDashboard({
      statistics: getDefaultCampaignStatisticsBundle(),
    });
    milestonePage.assertNameEquals('milestone1');

    const dashboardPanel = milestonePage.showDashboard();
    dashboardPanel.assertTitleExist('Statistiques des campagnes du jalon');
    dashboardPanel.importanceChart.assertChartExist();
    dashboardPanel.importanceChart.assertHasTitle('Importance des tests jamais exécutés');
    dashboardPanel.conclusivenessChart.assertChartExist();
    dashboardPanel.statusChart.assertChartExist();
    dashboardPanel.statusChart.assertHasTitle('Statut des tests');
  });
});
