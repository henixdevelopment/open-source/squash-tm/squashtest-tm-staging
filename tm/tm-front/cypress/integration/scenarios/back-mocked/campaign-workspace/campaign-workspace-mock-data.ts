import { DataRowOpenState } from '../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { mockGridResponse, mockTreeNode } from '../../../data-mock/grid.data-mock';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { SprintStatus } from '../../../../../projects/sqtm-core/src/lib/model/level-enums/level-enum';

export function createBasicTreeWithOneSprint(): GridResponse {
  return mockGridResponse('CampaignLibrary', [initialCampaignLibraryNode, initialSprintNode]);
}

export function createBasicTreeWithOneSprintGroup(): GridResponse {
  return mockGridResponse('CampaignLibrary', [initialCampaignLibraryNode, initialSprintGroupNode]);
}

const initialCampaignLibraryNode = mockTreeNode({
  id: 'CampaignLibrary-1',
  children: ['Sprint-1', 'SprintGroup-1'],
  state: DataRowOpenState.open,
  data: { NAME: 'Project1', CHILD_COUNT: 1 },
});

const initialSprintNode = mockTreeNode({
  id: 'Sprint-1',
  parentRowId: 'CampaignLibrary-1',
  data: {
    NAME: 'Sprint 1',
    STATUS: SprintStatus.UPCOMING.id,
    CHILD_COUNT: 0,
  },
});

const initialSprintGroupNode = mockTreeNode({
  id: 'SprintGroup-1',
  parentRowId: 'CampaignLibrary-1',
  data: { NAME: 'SprintGroup 1', CHILD_COUNT: 0 },
});
