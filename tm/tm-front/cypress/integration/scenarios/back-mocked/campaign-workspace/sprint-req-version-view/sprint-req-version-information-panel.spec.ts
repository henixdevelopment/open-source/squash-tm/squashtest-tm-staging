import { mockSprintReqVersionViewModel } from '../../../../data-mock/sprint-req-version.data-mock';
import {
  ManagementMode,
  RemotePerimeterStatus,
} from '../../../../../../projects/sqtm-core/src/lib/model/campaign/sprint-model';
import { SprintReqVersionViewPage } from '../../../../page-objects/pages/campaign-workspace/sprint-req-version/sprint-req-version-view.page';
import { SprintReqVersionViewInformationPage } from '../../../../page-objects/pages/campaign-workspace/sprint-req-version/sprint-req-version-view-information.page';
import {
  nativeSprintReqVersionModel,
  navigateToSprintReqVersion,
} from '../../../scenario-parts/sprint-req-version.part';

describe('Sprint requirement version - Information panel display', () => {
  it('should display native sprint req version information in the information panel', () => {
    const sprintReqVersionViewPage: SprintReqVersionViewPage = navigateToSprintReqVersion(
      nativeSprintReqVersionModel,
    );
    sprintReqVersionViewPage.assertExists();
    const infoPanel: SprintReqVersionViewInformationPage =
      sprintReqVersionViewPage.informationPanel;
    sprintReqVersionViewPage.checkData('requirement-version-status', 'En cours de rédaction');
    sprintReqVersionViewPage.checkData('sprint-req-version-id', '1');
    sprintReqVersionViewPage.checkData('requirement-version-criticality', 'Critique');
    sprintReqVersionViewPage.checkData('sprint-req-version-category', 'Fonctionnelle');
    infoPanel.checkSprintReqVersionDescriptionNotExtended();
    infoPanel.clickOnExtendSprintReqVersionDescription();
    sprintReqVersionViewPage.checkData('sprint-req_version-description', 'une description');
  });

  it('should display synchronized sprint req version information in the information panel', () => {
    const sprintReqVersionViewPage: SprintReqVersionViewPage = navigateToSprintReqVersion(
      synchronizedSprintReqVersionModel,
    );
    sprintReqVersionViewPage.assertExists();
    const infoPanel: SprintReqVersionViewInformationPage =
      sprintReqVersionViewPage.informationPanel;
    sprintReqVersionViewPage.checkData('requirement-version-status', 'A faire');
    sprintReqVersionViewPage.checkData('sprint-req-version-id', '518');
    sprintReqVersionViewPage.checkData('requirement-version-criticality', 'Low');
    sprintReqVersionViewPage.checkData('sprint-req-version-category', 'Story');
    sprintReqVersionViewPage.checkData('sprint-req-version-validation-status', 'À tester');
    sprintReqVersionViewPage.checkData('sprint-group-ticket-url', 'projet_test_simu_synchro#1');
    infoPanel.clickOnExtendSprintReqVersionDescription();
    sprintReqVersionViewPage.checkData('sprint-req_version-description', 'une description');
  });

  it('should update sprint req version validation status', () => {
    const sprintReqVersionViewPage: SprintReqVersionViewPage = navigateToSprintReqVersion(
      synchronizedSprintReqVersionModel,
    );
    sprintReqVersionViewPage.informationPanel.updateValidationStatus('Test en cours');
    sprintReqVersionViewPage.checkData('sprint-req-version-validation-status', 'Test en cours');
  });
});

const synchronizedSprintReqVersionModel = mockSprintReqVersionViewModel({
  id: 1,
  versionId: 518,
  projectId: 1,
  requirementId: null,
  categoryId: null,
  categoryLabel: 'Story',
  name: 'Sprint Req 1',
  reference: 'SR1',
  remotePerimeterStatus: RemotePerimeterStatus.IN_CURRENT_PERIMETER,
  mode: ManagementMode.SYNCHRONIZED,
  status: 'A faire',
  criticality: 'Low',
  description: 'une description',
  remoteReqUrl:
    'https://gitlab.com/henixdevelopment/sandbox/toto/projet_test_simu_synchro/-/issues/1',
});
