import {
  nativeSprintReqVersionModel,
  navigateToSprintReqVersion,
} from '../../../scenario-parts/sprint-req-version.part';

describe('Sprint requirement version - view display', () => {
  it('Should display correct info in validation status capsule', () => {
    const sprintReqVersionViewPage = navigateToSprintReqVersion(nativeSprintReqVersionModel);
    sprintReqVersionViewPage.validationStatusCapsule.assertExists();
    sprintReqVersionViewPage.validationStatusCapsule.assertContainsText('À tester');

    sprintReqVersionViewPage.informationPanel.updateValidationStatus('Test en cours');
    sprintReqVersionViewPage.validationStatusCapsule.assertContainsText('Test en cours');
    sprintReqVersionViewPage.validationStatusCapsule.assertIconColor('rgb(0, 120, 174)');
  });

  it('Should update validation status when clicking on action buttons', () => {
    const sprintReqVersionViewPage = navigateToSprintReqVersion(nativeSprintReqVersionModel);

    sprintReqVersionViewPage.validationStatusCapsule.assertContainsText('À tester');
    sprintReqVersionViewPage.clickActionButton('from-to-be-tested-to-in-progress-button');
    sprintReqVersionViewPage.validationStatusCapsule.assertContainsText('Test en cours');
    sprintReqVersionViewPage.informationPanel.validationStatus.assertContainsText('Test en cours');
  });

  it('Should follow correct workflow for action buttons', () => {
    const sprintReqVersionViewPage = navigateToSprintReqVersion(nativeSprintReqVersionModel);
    sprintReqVersionViewPage.validationStatusCapsule.assertContainsText('À tester');

    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-to-be-tested-to-in-progress-button',
      'exist',
    );
    sprintReqVersionViewPage.clickActionButton('from-to-be-tested-to-in-progress-button');
    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-to-be-tested-to-in-progress-button',
      'not.exist',
    );

    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-in-progress-to-to-be-corrected-button',
      'exist',
    );
    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-in-progress-to-validated-button',
      'exist',
    );
    sprintReqVersionViewPage.clickActionButton('from-in-progress-to-validated-button');
    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-in-progress-to-to-be-corrected-button',
      'not.exist',
    );
    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-in-progress-to-validated-button',
      'not.exist',
    );

    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-validated-to-to-be-corrected-button',
      'exist',
    );
    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-validated-to-in-progress-button',
      'exist',
    );
    sprintReqVersionViewPage.clickActionButton('from-validated-to-to-be-corrected-button');
    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-validated-to-to-be-corrected-button',
      'not.exist',
    );
    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-validated-to-in-progress-button',
      'not.exist',
    );

    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-to-be-corrected-to-validated-button',
      'exist',
    );
    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-to-be-corrected-to-in-progress-button',
      'exist',
    );
    sprintReqVersionViewPage.clickActionButton('from-to-be-corrected-to-validated-button');
    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-to-be-corrected-to-validated-button',
      'not.exist',
    );
    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-to-be-corrected-to-in-progress-button',
      'not.exist',
    );

    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-validated-to-to-be-corrected-button',
      'exist',
    );
    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-validated-to-in-progress-button',
      'exist',
    );
    sprintReqVersionViewPage.clickActionButton('from-validated-to-in-progress-button');
    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-validated-to-to-be-corrected-button',
      'not.exist',
    );
    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-validated-to-in-progress-button',
      'not.exist',
    );

    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-in-progress-to-validated-button',
      'exist',
    );
    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-in-progress-to-to-be-corrected-button',
      'exist',
    );
    sprintReqVersionViewPage.clickActionButton('from-in-progress-to-to-be-corrected-button');
    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-in-progress-to-validated-button',
      'not.exist',
    );
    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-in-progress-to-to-be-corrected-button',
      'not.exist',
    );

    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-to-be-corrected-to-validated-button',
      'exist',
    );
    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-to-be-corrected-to-in-progress-button',
      'exist',
    );
    sprintReqVersionViewPage.clickActionButton('from-to-be-corrected-to-in-progress-button');
    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-to-be-corrected-to-validated-button',
      'not.exist',
    );
    sprintReqVersionViewPage.assertActionButtonStatus(
      'from-to-be-corrected-to-in-progress-button',
      'not.exist',
    );
  });
});
