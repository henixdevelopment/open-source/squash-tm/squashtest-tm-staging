import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { SprintViewPage } from '../../../../page-objects/pages/campaign-workspace/sprint/sprint-view.page';
import { mockSprintReqVersionModel } from '../../../../data-mock/sprint-req-version.data-mock';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { SprintReqVersionViewPage } from '../../../../page-objects/pages/campaign-workspace/sprint-req-version/sprint-req-version-view.page';
import { SprintReqVersionTestPlanItem } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/sprint-req-version-test-plan-item.model';
import { mockDataRow, mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';
import {
  DataRowModel,
  DataRowOpenState,
} from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { SprintReqVersionViewModel } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/sprint-req-version-view-model';
import { AlertDialogElement } from '../../../../page-objects/elements/dialog/alert-dialog.element';
import { SprintReqVersionUpdateTestPlanDialogElement } from '../../../../page-objects/pages/campaign-workspace/dialogs/sprint-req-version-update-test-plan-dialog.element';
import {
  getSprintModel,
  initializeCampaignTree,
} from '../../../scenario-parts/sprint-req-version.part';

describe('Sprint requirement-version - Test plan panel display', () => {
  it('should display a sprint req version test plan', () => {
    const page: SprintReqVersionViewPage = navigateToSprintReqVersion();
    page.assertExists();
    page.testPlanGrid.assertRowCount(1);
  });

  it('should add a test case in test plan', () => {
    const page: SprintReqVersionViewPage = navigateToSprintReqVersion();

    const testCaseDrawer = page.openTestCaseDrawer(testCaseTree);
    testCaseDrawer.beginDragAndDrop('TestCase-3');
    page.testPlanGrid.declareRefreshData(testPlanAfterAddingTestCase);
    page.dropTestCaseIntoSprint();

    page.closeDrawer();
    page.testPlanGrid.assertRowCount(2);
  });

  it('should show info message when no test case is available to update test plan', () => {
    const page: SprintReqVersionViewPage = navigateToSprintReqVersion();
    page.clickUpdateTestPlanButton([]);

    const dialog = new AlertDialogElement('synchronize-test-plan-dialog');
    dialog.assertExists();
  });

  it('should update test plan with selected available items', () => {
    const page: SprintReqVersionViewPage = navigateToSprintReqVersion();
    page.clickUpdateTestPlanButton(mockAvailableTestPlanItems());

    const dialog = new SprintReqVersionUpdateTestPlanDialogElement();
    dialog.assertExists();

    dialog.grid.getCell(0, 'select-row-column').checkBoxRender().toggleState();
    dialog.grid.getCell(2, 'select-row-column').checkBoxRender().toggleState();

    page.testPlanGrid.declareRefreshData(
      mockGridResponse(GridColumnId.testPlanItemId, [
        mockTestPlanItemRow(1, 'test case #1'),
        mockTestPlanItemRow(2, 'test case #2'),
        mockTestPlanItemRow(3, 'test case #2'),
      ]),
    );

    dialog.confirm();
  });
});

function navigateToSprintReqVersion(): SprintReqVersionViewPage {
  const tree = initializeCampaignTree();
  const sprintViewPage: SprintViewPage = tree.selectNode(
    'Sprint-1',
    getSprintModel(sprintReqVersionModel),
  );
  sprintViewPage.assertExists();

  const viewModel: SprintReqVersionViewModel = {
    ...getSprintModel(sprintReqVersionModel).sprintReqVersions[0],
    assignableUsers: [],
    sprintStatus: 'UPCOMING',
  };

  sprintViewPage.clickSprintReqVersionsAnchorLink();
  return sprintViewPage.openSprintReqVersionPageByName(viewModel, initialTestPlan, null);
}

const sprintReqVersionModel = mockSprintReqVersionModel({
  name: 'exigence01',
});

function mockTestPlanItemRow(
  testPlanItemId: number,
  testCaseName: string,
): DataRowModel<SprintReqVersionTestPlanItem> {
  return mockDataRow({
    id: testPlanItemId,
    data: {
      testPlanItemId,
      testCaseName,
      assigneeId: 1,
      datasetId: 1,
      projectId: 1,
      datasetName: 'dataset #1',
      executionStatus: 'READY',
      importance: 'MEDIUM',
      inferredExecutionMode: 'manual',
      lastExecutedOn: '',
      projectName: 'project #1',
      testCaseId: 1,
      testCaseReference: 'reference',
    },
  });
}

const initialTestPlan: GridResponse<SprintReqVersionTestPlanItem> = mockGridResponse(
  GridColumnId.testPlanItemId,
  [mockTestPlanItemRow(1, 'test case #1')],
);

const testPlanAfterAddingTestCase: GridResponse<SprintReqVersionTestPlanItem> = mockGridResponse(
  GridColumnId.testPlanItemId,
  [mockTestPlanItemRow(1, 'test case #1'), mockTestPlanItemRow(2, 'test case #2')],
);

const testCaseTree: GridResponse = mockGridResponse('test-case-tree', [
  mockTreeNode({
    id: 'TestCaseLibrary-1',
    children: ['TestCase-3'],
    data: { NAME: 'Project1', CHILD_COUNT: 1 },
    projectId: 1,
    state: DataRowOpenState.open,
  }),
  mockTreeNode({
    id: 'TestCase-3',
    children: [],
    data: {
      NAME: 'test case #2',
      CHILD_COUNT: 0,
      TC_STATUS: 'APPROVED',
      TC_KIND: 'STANDARD',
      IMPORTANCE: 'HIGH',
    },
    parentRowId: 'TestCaseLibrary-1',
    projectId: 1,
  }),
]);

function mockAvailableTestPlanItems() {
  return [
    {
      testCaseId: 1,
      testCaseReference: 'reference',
      testCaseName: 'test case #1',
      datasetId: null,
      datasetName: null,
    },
    {
      testCaseId: 2,
      testCaseReference: 'reference',
      testCaseName: 'test case #2',
      datasetId: 1,
      datasetName: 'dataset #1',
    },
    {
      testCaseId: 2,
      testCaseReference: 'reference',
      testCaseName: 'test case #2',
      datasetId: 2,
      datasetName: 'dataset #2',
    },
  ];
}
