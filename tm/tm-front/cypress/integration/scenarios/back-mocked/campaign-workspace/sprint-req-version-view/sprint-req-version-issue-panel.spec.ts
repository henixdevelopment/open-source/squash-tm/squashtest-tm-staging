import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { SprintViewPage } from '../../../../page-objects/pages/campaign-workspace/sprint/sprint-view.page';
import { createBasicTreeWithOneSprint } from '../campaign-workspace-mock-data';
import { SprintModel } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/sprint-model';
import { mockSprintModel } from '../../../../data-mock/sprint.data-mock';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { SprintReqVersionViewPage } from '../../../../page-objects/pages/campaign-workspace/sprint-req-version/sprint-req-version-view.page';
import { mockDataRow, mockGridResponse } from '../../../../data-mock/grid.data-mock';
import { GridElement, TreeElement } from '../../../../page-objects/elements/grid/grid.element';
import { SprintReqVersionModel } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/sprint-req-version-model';
import { ReferentialDataMockBuilder } from '../../../../utils/referential/referential-data-builder';
import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { DataRowModel } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { Identifier } from '../../../../../../projects/sqtm-core/src/lib/model/entity.model';
import { ExportSprintReqVersionIssuesDialogElement } from '../../../../page-objects/pages/campaign-workspace/dialogs/export-sprint-req-version-issues-dialog.element';
import { SprintReqVersionViewModel } from '../../../../../../projects/sqtm-core/src/lib/model/campaign/sprint-req-version-view-model';
import {
  initialTestPlan,
  nativeSprintReqVersionModel,
} from '../../../scenario-parts/sprint-req-version.part';

describe('Sprint requirement version - Issue panel display', () => {
  it('should not display the issues grid if no bugtracker is bound to the project', () => {
    const sprintReqVersionViewPage: SprintReqVersionViewPage = navigateToSprintReqVersionWithIssues(
      nativeSprintReqVersionModel,
      refDataWithoutBugTracker,
      null,
    );
    sprintReqVersionViewPage.assertExists();
    const issueGrid: GridElement = sprintReqVersionViewPage.issueGrid;
    issueGrid.assertNotExists();
  });

  it('should display issues in the issues grid', () => {
    const sprintReqVersionViewPage: SprintReqVersionViewPage = navigateToSprintReqVersionWithIssues(
      nativeSprintReqVersionModel,
      refDataWithBugTracker,
      issuesGridResponse,
    );
    sprintReqVersionViewPage.assertExists();
    const issueGrid: GridElement = sprintReqVersionViewPage.issueGrid;
    issueGrid.assertExists();
    issueGrid.assertRowCount(2);
  });

  it('should export issues into a csv file', () => {
    const sprintReqVersionViewPage: SprintReqVersionViewPage = navigateToSprintReqVersionWithIssues(
      nativeSprintReqVersionModel,
      refDataWithBugTracker,
      issuesGridResponse,
    );
    const exportDialog: ExportSprintReqVersionIssuesDialogElement =
      sprintReqVersionViewPage.openExportDialog();
    exportDialog.assertExists();
  });
});

function navigateToSprintReqVersionWithIssues(
  sprintReqVersionModel: SprintReqVersionViewModel,
  referentialData: ReferentialDataModel,
  issuesResponse: GridResponse,
): SprintReqVersionViewPage {
  const initialNodes: GridResponse = createBasicTreeWithOneSprint();
  const campaignWorkspacePage: CampaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(
    initialNodes,
    referentialData,
  );

  const tree: TreeElement = campaignWorkspacePage.tree;
  tree.assertNodeExist('CampaignLibrary-1');
  tree.assertNodeTextContains('CampaignLibrary-1', 'Project1');
  tree.assertNodeExist('Sprint-1');
  tree.assertNodeTextContains('Sprint-1', 'Sprint 1');

  const sprintViewPage: SprintViewPage = tree.selectNode(
    'Sprint-1',
    getSprintModel(sprintReqVersionModel),
  );
  sprintViewPage.assertExists();

  sprintViewPage.clickSprintReqVersionsAnchorLink();
  return sprintViewPage.openSprintReqVersionPageByName(
    sprintReqVersionModel,
    initialTestPlan,
    issuesResponse,
  );
}

function getSprintModel(sprintReqVersionModel: SprintReqVersionModel): SprintModel {
  return mockSprintModel({
    sprintReqVersions: [sprintReqVersionModel],
  });
}

const refDataWithoutBugTracker: ReferentialDataModel = new ReferentialDataMockBuilder()
  .withProjects({
    name: 'Project1',
  })
  .withUser({
    username: 'admin',
    userId: 1,
    admin: true,
  })
  .build();

const refDataWithBugTracker: ReferentialDataModel = new ReferentialDataMockBuilder()
  .withUser({
    username: 'admin',
    userId: 1,
    admin: true,
  })
  .withProjects({
    name: 'Project1',
    bugTrackerBinding: { bugTrackerId: 1, projectId: 1 },
  })
  .withBugTrackers({
    name: 'bt',
  })
  .build();

const issuesGridResponse: GridResponse = mockGridResponse(GridColumnId.remoteId, [
  mockIssueDataRow(1, 1, 1, 'HIGH'),
  mockIssueDataRow(2, 2, 2, 'LOW'),
]);

function mockIssueDataRow(
  issueId: Identifier,
  remoteId: Identifier,
  stepOrder: number,
  priority: string,
): DataRowModel {
  return mockDataRow({
    id: issueId.toString(),
    projectId: 1,
    data: {
      summary: 'summary',
      issueIds: [issueId],
      btProject: 'P1',
      executionSteps: [stepOrder],
      priority: priority,
      url: 'http://192.168.1.50/mantis/view.php?id=' + remoteId,
      remoteId: remoteId.toString(),
      assignee: 'admin',
      status: 'assigned',
    },
    allowMoves: true,
  });
}
