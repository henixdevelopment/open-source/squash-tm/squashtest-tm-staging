import { ExecutionRunnerProloguePage } from '../../../page-objects/pages/execution/execution-runner-prologue-page';
import { mockExecutionModel } from '../../../data-mock/execution.data-mock';
import {
  ExecutionModel,
  ExecutionStepModel,
} from '../../../../../projects/sqtm-core/src/lib/model/execution/execution.model';
import { ReferentialDataMockBuilder } from '../../../utils/referential/referential-data-builder';
import { ReferentialDataProviderBuilder } from '../../../utils/referential/referential-data.provider';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { AlertDialogElement } from '../../../page-objects/elements/dialog/alert-dialog.element';

function getExecutionModel(): ExecutionModel {
  return mockExecutionModel({
    id: 1,
    projectId: 1,
    executionOrder: 2,
    name: 'NX - Test Case 1',
    prerequisite: '',
    attachmentList: { id: 1, attachments: [] },
    customFieldValues: [],
    tcImportance: 'LOW',
    tcNatLabel: 'test-case.nature.NAT_BUSINESS_TESTING',
    tcNatIconName: 'noicon',
    tcStatus: 'APPROVED',
    tcTypeLabel: 'test-case.type.TYP_COMPLIANCE_TESTING',
    tcTypeIconName: 'noicon',
    tcDescription: 'description',
    comment: '',
    denormalizedCustomFieldValues: [],
    executionStepViews: [
      {
        id: 1,
        order: 0,
        executionStatus: 'READY',
        attachmentList: {
          id: 1,
          attachments: [],
        },
      } as ExecutionStepModel,
      {
        id: 2,
        order: 1,
        executionStatus: 'READY',
        attachmentList: {
          id: 1,
          attachments: [],
        },
      } as ExecutionStepModel,
      {
        id: 3,
        order: 2,
        executionStatus: 'READY',
        attachmentList: {
          id: 1,
          attachments: [],
        },
      } as ExecutionStepModel,
    ],
    coverages: [],
    executionMode: 'MANUAL',
    lastExecutedOn: null,
    lastExecutedBy: 'admin',
    executionStatus: 'READY',
    automatedJobUrl: null,
    testAutomationServerKind: null,
    automatedExecutionResultUrl: null,
    automatedExecutionResultSummary: null,
    nbIssues: 0,
    iterationId: -1,
    kind: 'STANDARD',
    milestones: [],
    testPlanItemId: null,
    executionsCount: null,
  });
}

describe('Execution runner', function () {
  it('it should navigate from prologue to steps', () => {
    const executionModel: ExecutionModel = getExecutionModel();
    const prologuePage = ExecutionRunnerProloguePage.initTestAtPage(1, executionModel);

    const firstStepPage = prologuePage.startExecution();
    firstStepPage.assertExists();
    firstStepPage.checkExecutionStepper(1, 3);
    firstStepPage.checkDocumentTitle('#3 - NX - Test Case 1 (1/3)');
    const secondStepPage = firstStepPage.navigateForward();
    secondStepPage.checkExecutionStepper(2, 3);
    secondStepPage.checkDocumentTitle('#3 - NX - Test Case 1 (2/3)');
    const thirdStepPage = secondStepPage.navigateForward();
    thirdStepPage.checkExecutionStepper(3, 3);
    secondStepPage.checkDocumentTitle('#3 - NX - Test Case 1 (3/3)');
    thirdStepPage.navigateBackward();
    secondStepPage.checkExecutionStepper(2, 3);
    secondStepPage.navigateBackward();
    firstStepPage.checkExecutionStepper(1, 3);
    firstStepPage.navigateBackward();
    prologuePage.assertExists();
    prologuePage.checkTitle('#3 : NX - Test Case 1');
  });

  it('it should navigate to arbitrary steps', () => {
    const executionModel: ExecutionModel = getExecutionModel();
    const prologuePage = ExecutionRunnerProloguePage.initTestAtPage(1, executionModel);
    prologuePage.assertExists();
    prologuePage.checkTitle('#3 : NX - Test Case 1');
    prologuePage.checkDocumentTitle('Exéc #3 - NX - Test Case 1 (Préambule)');

    const firstStepPage = prologuePage.startExecution();
    firstStepPage.assertExists();
    firstStepPage.checkExecutionStepper(1, 3);
    firstStepPage.checkDocumentTitle('#3 - NX - Test Case 1 (1/3)');
    const thirdStepPage = firstStepPage.navigateToArbitraryStep(3);
    thirdStepPage.checkStepExecutionStatus('À exécuter');
    thirdStepPage.checkExecutionStepper(3, 3);
    thirdStepPage.checkDocumentTitle('#3 - NX - Test Case 1 (3/3)');
    thirdStepPage.navigateToArbitraryStep(1);
    firstStepPage.checkExecutionStepper(1, 3);
    firstStepPage.checkDocumentTitle('#3 - NX - Test Case 1 (1/3)');
    firstStepPage.navigateToArbitraryStep(0);
    prologuePage.assertExists();
    prologuePage.checkTitle('#3 : NX - Test Case 1');
    prologuePage.checkDocumentTitle('Exéc #3 - NX - Test Case 1 (Préambule)');
  });

  it('it should change status and navigate', () => {
    const executionModel: ExecutionModel = getExecutionModel();
    const prologuePage = ExecutionRunnerProloguePage.initTestAtPage(1, executionModel);
    prologuePage.assertExists();
    prologuePage.checkTitle('#3 : NX - Test Case 1');
    const firstStepPage = prologuePage.startExecution();
    firstStepPage.assertExists();
    firstStepPage.checkExecutionStepper(1, 3);
    firstStepPage.checkStepExecutionStatus('À exécuter');
    firstStepPage.checkExecutionButtons();
    const secondStepPage = firstStepPage.successButton.changeStatus(1, 1);
    secondStepPage.checkExecutionStepper(2, 3);
    secondStepPage.checkStepExecutionStatus('À exécuter');
    let thirdStepPage = secondStepPage.failureButton.changeStatus(2, 1);
    thirdStepPage.checkStepExecutionStatus('À exécuter');
    thirdStepPage.checkExecutionStepper(3, 3);
    thirdStepPage = secondStepPage.blockedButton.changeStatus(3, 1);
    thirdStepPage.checkExecutionStepper(3, 3);
    thirdStepPage.checkStepExecutionStatus('Bloqué');
  });

  it('should display alert dialog if error 412 is thrown', () => {
    const referentialData = new ReferentialDataMockBuilder().build();
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();

    const executionRunnerPrologueMock = new HttpMockBuilder('execution-runner/*')
      .responseBody({
        squashTMError: {
          kind: 'ACTION_ERROR',
          actionValidationError: {
            exception: 'MilestoneForbidModificationException',
          },
        },
      })
      .status(412)
      .build();

    cy.visit('execution-runner/execution/1/prologue');
    referentialDataProvider.wait();
    executionRunnerPrologueMock.wait();

    const dialogError = new AlertDialogElement();
    dialogError.assertExists();
  });
});
