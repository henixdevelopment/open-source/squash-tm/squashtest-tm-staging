import { ExecutionPage } from '../../../page-objects/pages/execution/execution-page';
import { EditableDateFieldElement } from '../../../page-objects/elements/forms/editable-date-field.element';
import { NavBarElement } from '../../../page-objects/elements/nav-bar/nav-bar.element';
import { mockExecutionModel } from '../../../data-mock/execution.data-mock';
import { ExecutionModel } from '../../../../../projects/sqtm-core/src/lib/model/execution/execution.model';
import { InputType } from '../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import {
  DataRowModel,
  DataRowOpenState,
} from '../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { mockDataRow, mockGridResponse } from '../../../data-mock/grid.data-mock';
import { GridColumnId } from '../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';

const todayDate = new Date();

describe('Execution page - history', function () {
  const initialNodes = mockGridResponse('executionId', [
    mockExecutionDataRow(3),
    mockExecutionDataRow(2),
    mockExecutionDataRow(1),
  ]);

  const oneDeleteNodes = mockGridResponse('executionId', [
    mockExecutionDataRow(3),
    mockExecutionDataRow(2),
  ]);

  const twoDeletesNodes = mockGridResponse('executionId', [mockExecutionDataRow(3)]);

  it('should display correct information', () => {
    const executionPage = ExecutionPage.initTestAtPage(3, getExecutionModel(), null, initialNodes);
    const historyPanel = executionPage.clickHistoryAnchorLink();
    historyPanel.assertExists();
    const grid = executionPage.historyGrid;
    new NavBarElement().toggle();

    grid.assertRowExist(2);
    const row = grid.getRow(2);
    row
      .cell(GridColumnId.inferredExecutionMode)
      .iconRenderer()
      .assertContainsIcon('anticon-sqtm-core-administration:user');
    row
      .cell('importance')
      .iconRenderer()
      .assertContainsIcon('anticon-sqtm-core-test-case:double_down');
    row.cell('user').textRenderer().assertContainsText('admin');
    row
      .cell('lastExecutedOn')
      .textRenderer()
      .assertContainsText(EditableDateFieldElement.dateToDisplayString(todayDate));

    grid.assertRowExist(3);
    grid
      .getRow(3)
      .cell('delete')
      .iconRenderer()
      .assertContainsIcon('anticon-sqtm-core-generic:delete');
  });

  it('should delete an other execution', () => {
    const executionPage = ExecutionPage.initTestAtPage(3, getExecutionModel(), null, initialNodes);
    const historyPanel = executionPage.clickHistoryAnchorLink();
    historyPanel.assertExists();
    new NavBarElement().toggle();
    const grid = executionPage.historyGrid;
    executionPage.deleteExecution(1, oneDeleteNodes);
    grid.assertRowCount(2);
  });

  it('should delete multiple executions', () => {
    const executionPage = ExecutionPage.initTestAtPage(3, getExecutionModel(), null, initialNodes);
    const historyPanel = executionPage.clickHistoryAnchorLink();
    historyPanel.assertExists();
    new NavBarElement().toggle();
    const grid = executionPage.historyGrid;
    executionPage.deleteMultipleExecutions([1, 2], twoDeletesNodes);
    grid.assertRowCount(1);
  });

  it('should show the correct title for a manual execution', () => {
    const executionPage = ExecutionPage.initTestAtPage(3, getExecutionModel(), null, initialNodes);
    executionPage.anchors.clickLink('history');
    executionPage.historyPanel.assertPanelTitleContainsText('Historique des exécutions');
  });
});

function getExecutionModel(): ExecutionModel {
  return mockExecutionModel({
    id: 3,
    name: 'Exec 3',
    projectId: 1,
    testCaseId: 1,
    executionOrder: 2,
    prerequisite: '',
    tcNatLabel: 'test-case.nature.NAT_UNDEFINED',
    tcNatIconName: 'indeterminate_checkbox_empty',
    tcTypeLabel: 'test-case.type.TYP_UNDEFINED',
    tcTypeIconName: 'indeterminate_checkbox_empty',
    tcStatus: 'APPROVED',
    tcImportance: 'LOW',
    tcDescription: '',
    datasetLabel: null,
    comment: null,
    executionMode: 'MANUAL',
    lastExecutedOn: todayDate.toISOString(),
    lastExecutedBy: 'admin',
    executionStatus: 'SUCCESS',
    executionStepViews: [
      {
        id: 159,
        order: 0,
        executionStatus: 'SUCCESS',
        action: '',
        expectedResult: '',
        comment: '',
        attachmentList: {
          id: 542,
          attachments: [],
        },
        denormalizedCustomFieldValues: [],
        customFieldValues: [],
        lastExecutedOn: todayDate.toISOString(),
        lastExecutedBy: 'admin',
        projectId: 2,
      },
    ],
    denormalizedCustomFieldValues: [
      {
        id: 1,
        label: 'dnz-cuf-1',
        inputType: InputType.PLAIN_TEXT,
        denormalizedFieldHolderId: 1,
        fieldType: 'CF',
        value: 'dnz cuf value 1',
      },
      {
        id: 2,
        label: 'dnz-cuf-2',
        inputType: InputType.DATE_PICKER,
        denormalizedFieldHolderId: 1,
        fieldType: 'CF',
        value: '2020-02-25',
      },
    ],
    customFieldValues: [],
    coverages: [],
    attachmentList: {
      id: 541,
      attachments: [],
    },
    automatedExecutionResultUrl: null,
    automatedExecutionResultSummary: null,
    automatedJobUrl: null,
    nbIssues: 0,
    iterationId: 1,
    milestones: [],
    testPlanItemId: 1,
    executionsCount: 3,
    kind: 'STANDARD',
    testAutomationServerKind: null,
  });
}

function mockExecutionDataRow(id: number): DataRowModel {
  return mockDataRow({
    id,
    children: [],
    state: DataRowOpenState.leaf,
    data: {
      executionReference: '',
      successRate: 100.0,
      importance: 'LOW',
      executionStatus: 'SUCCESS',
      datasetName: null,
      [GridColumnId.inferredExecutionMode]: 'MANUAL',
      itemTestPlanId: 1,
      issueCount: 0,
      executionOrder: 2,
      executionId: id,
      executionName: 'Exec ' + id,
      boundToBlockingMilestone: false,
      lastExecutedOn: todayDate,
      iterationId: 1,
      projectId: 1,
      user: 'admin',
    },
    projectId: 1,
    parentRowId: null,
  });
}
