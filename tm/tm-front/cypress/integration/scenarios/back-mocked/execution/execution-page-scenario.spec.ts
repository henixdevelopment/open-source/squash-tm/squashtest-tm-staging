import { ExecutionPage } from '../../../page-objects/pages/execution/execution-page';
import { mockDataRow, mockGridResponse } from '../../../data-mock/grid.data-mock';
import { ReferentialDataMockBuilder } from '../../../utils/referential/referential-data-builder';
import {
  ExecutionKind,
  ExecutionModel,
} from '../../../../../projects/sqtm-core/src/lib/model/execution/execution.model';
import { InputType } from '../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { Identifier } from '../../../../../projects/sqtm-core/src/lib/model/entity.model';
import { DataRowModel } from '../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

describe('Execution page - scenario', function () {
  it('should collapse/expand all steps', () => {
    const executionPage = ExecutionPage.initTestAtPage(1, getExecutionModel());
    executionPage.clickScenarioAnchorLink();
    executionPage.scenarioPanel.assertExists();
    executionPage.scenarioPanel.expendAll();
    [0, 1].forEach((stepIndex) => {
      const executionStep = executionPage.scenarioPanel.getExecutionStepByIndex(stepIndex);
      executionStep.checkIsExtended();
    });
    executionPage.scenarioPanel.checkPrerequisiteIsExtended();
    cy.clickVoid();
    executionPage.scenarioPanel.collapseAll();
    executionPage.scenarioPanel.checkPrerequisiteIsCollapsed();
    [0, 1].forEach((stepIndex) => {
      const executionStep = executionPage.scenarioPanel.getExecutionStepByIndex(stepIndex);
      executionStep.checkIsCollapsed();
    });
  });

  it('should collapse/expand selected step', () => {
    const executionPage = ExecutionPage.initTestAtPage(1, getExecutionModel());
    executionPage.clickScenarioAnchorLink();
    executionPage.scenarioPanel.assertExists();
    const executionStep = executionPage.scenarioPanel.getExecutionStepById('1');
    executionStep.extendStep();
    executionStep.checkIsExtended();
    executionStep.collapseStep();
    executionStep.checkIsCollapsed();
  });

  it('should display correct index background color', () => {
    const executionPage = ExecutionPage.initTestAtPage(1, getExecutionModel());
    executionPage.clickScenarioAnchorLink();
    const executionStep1 = executionPage.scenarioPanel.getExecutionStepById('1');
    executionStep1.checkStepColor('rgba(0, 111, 87, 0.2)');
    const executionStep2 = executionPage.scenarioPanel.getExecutionStepById('2');
    executionStep2.checkStepColor('rgba(163, 178, 184, 0.2)');
  });

  it('should display correct information in step', () => {
    const refData = mockRefDataWithBugtracker();
    const issueGridResponse = mockIssueGridResponse([
      mockIssueDataRow(1, 12, 0, 'minor'),
      mockIssueDataRow(2, 13, 0, 'major'),
    ]);
    const executionPage = ExecutionPage.initTestAtPage(
      1,
      getExecutionModel(),
      issueGridResponse,
      null,
      refData,
    );
    executionPage.clickScenarioAnchorLink(issueGridResponse);
    executionPage.scenarioPanel.assertExists();
    const executionStep = executionPage.scenarioPanel.getExecutionStepById('1');
    executionStep.extendStep();
    executionStep.checkIsExtended();
    executionStep.checkIndexLabel();
    executionStep.checkStepExecutionStatus('Succès');
    executionStep.checkStepAction('do something');
    executionStep.assertStepExpectedResultExists();
    executionStep.checkStepExpectedResult('it should have done that');
    executionStep.checkDnzCufLabel('0', 'dnz-cuf-1');
    executionStep.checkDnzCufValue('0', 'dnz cuf value 1');
    executionStep.assertCommentFieldExists();
    executionStep.checkStepComment('this is a nice comment');
    executionStep.checkIssueRemoteIdLabel(0, '12');
    executionStep.checkIssueBtProjectLabel(0, 'P1');
    executionStep.checkIssueSummaryLabel(0, 'summary');
    executionStep.checkIssuePriorityLabel(0, 'minor');
  });

  it('should edit comment in step', () => {
    const executionPage = ExecutionPage.initTestAtPage(1, getExecutionModel());
    executionPage.clickScenarioAnchorLink();
    executionPage.scenarioPanel.assertExists();
    const executionStep = executionPage.scenarioPanel.getExecutionStepById('1');
    executionStep.extendStep();
    executionStep.checkIsExtended();
    executionStep.updateComment('this is a new comment');
    executionStep.checkStepComment('this is a new comment');
  });

  it('should change step execution status', () => {
    const executionPage = ExecutionPage.initTestAtPage(1, getExecutionModel());
    executionPage.clickScenarioAnchorLink();
    executionPage.scenarioPanel.assertExists();
    const executionStep = executionPage.scenarioPanel.getExecutionStepById('1');
    executionStep.changeStepExecutionStatus('FAILURE', 1);
    executionStep.checkStepExecutionStatus('Échec');
  });

  it('should not display comment in step if there is no comment', () => {
    const executionPage = ExecutionPage.initTestAtPage(1, getExecutionModel());
    executionPage.clickScenarioAnchorLink();
    executionPage.scenarioPanel.assertExists();
    const executionStep = executionPage.scenarioPanel.getExecutionStepById('2');
    executionStep.extendStep();
    executionStep.checkIsExtended();
    executionStep.assertCommentFieldNotExists();
  });

  it('should not display prerequisite block if this is a keyword execution', () => {
    const executionPage = ExecutionPage.initTestAtPage(1, getExecutionModel('KEYWORD'));
    executionPage.clickScenarioAnchorLink();
    executionPage.scenarioPanel.assertExists();
    executionPage.scenarioPanel.assertPrerequisiteFieldNotExists();
  });

  it('should not display expected result in step if this is a keyword execution', () => {
    const executionPage = ExecutionPage.initTestAtPage(1, getExecutionModel('KEYWORD'));
    executionPage.clickScenarioAnchorLink();
    executionPage.scenarioPanel.assertExists();
    const executionStep = executionPage.scenarioPanel.getExecutionStepById('1');
    executionStep.assertStepExpectedResultNotExists();
  });

  it('should display a script block instead of action block in step if this is a gherkin execution', () => {
    const executionPage = ExecutionPage.initTestAtPage(1, getExecutionModel('GHERKIN'));
    executionPage.clickScenarioAnchorLink();
    executionPage.scenarioPanel.assertExists();
    const executionStep = executionPage.scenarioPanel.getExecutionStepById('1');
    executionStep.checkStepHeaderContent('Script');
  });

  it('should display background block instead of prerequisite if this is a gherkin execution', () => {
    const executionPage = ExecutionPage.initTestAtPage(1, getExecutionModel('GHERKIN'));
    executionPage.clickScenarioAnchorLink();
    executionPage.scenarioPanel.assertExists();
    executionPage.scenarioPanel.checkPrerequisiteHeaderLabel('Contexte');
  });

  it('should not display expected result in step if this is a gherkin execution', () => {
    const executionPage = ExecutionPage.initTestAtPage(1, getExecutionModel('GHERKIN'));
    executionPage.clickScenarioAnchorLink();
    executionPage.scenarioPanel.assertExists();
    const executionStep = executionPage.scenarioPanel.getExecutionStepById('1');
    executionStep.assertStepExpectedResultNotExists();
  });

  it('should unbind issue in step', () => {
    const refData = mockRefDataWithBugtracker();
    const issueGridResponse = mockIssueGridResponse([
      mockIssueDataRow(1, 12, 0, 'minor'),
      mockIssueDataRow(2, 13, 0, 'major'),
    ]);

    const issueGridResponseAfterDelete = mockIssueGridResponse([
      mockIssueDataRow(2, 13, 0, 'major'),
    ]);

    const executionPage = ExecutionPage.initTestAtPage(
      1,
      getExecutionModel(),
      issueGridResponse,
      null,
      refData,
    );
    executionPage.clickScenarioAnchorLink(issueGridResponse);
    executionPage.scenarioPanel.assertExists();
    const executionStep = executionPage.scenarioPanel.getExecutionStepById('1');
    executionStep.extendStep();
    executionStep.checkIsExtended();
    executionStep.unbindOneIssue(0, issueGridResponseAfterDelete);
  });
});

function getExecutionModel(executionKind?: ExecutionKind): ExecutionModel {
  return {
    id: 1,
    projectId: 1,
    executionOrder: 2,
    name: 'NX - Test Case 1',
    prerequisite: 'this is a prerequisite',
    attachmentList: {
      id: 1,
      attachments: [
        {
          id: 1,
          addedOn: new Date('25 Mar 2020 10:12:57'),
          lastModifiedOn: null,
          name: 'attachment-1',
          size: 56789,
        },
        {
          id: 2,
          addedOn: new Date('17 Mar 2019 20:40:00'),
          lastModifiedOn: null,
          name: 'attachment-2',
          size: 1234567,
        },
      ],
    },
    customFieldValues: [],
    tcImportance: 'HIGH',
    tcNatLabel: 'test-case.nature.NAT_BUSINESS_TESTING',
    tcNatIconName: 'indeterminate_checkbox',
    tcStatus: 'WORK_IN_PROGRESS',
    datasetLabel: 'JDD-1',
    tcTypeLabel: 'default',
    tcTypeIconName: 'indeterminate_checkbox',
    tcDescription: 'this is a description',
    comment: 'this is a comment',
    denormalizedCustomFieldValues: [
      {
        id: 1,
        label: 'dnz-cuf-1',
        inputType: InputType.PLAIN_TEXT,
        denormalizedFieldHolderId: 1,
        fieldType: 'CF',
        value: 'dnz cuf value 1',
      },
      {
        id: 2,
        label: 'dnz-cuf-2',
        inputType: InputType.DATE_PICKER,
        denormalizedFieldHolderId: 1,
        fieldType: 'CF',
        value: '2020-02-25',
      },
    ],
    executionStepViews: [
      {
        id: 1,
        order: 0,
        executionStatus: 'SUCCESS',
        action: 'do something',
        expectedResult: 'it should have done that',
        comment: 'this is a nice comment',
        lastExecutedOn: new Date().toISOString(),
        lastExecutedBy: 'admin',
        attachmentList: {
          id: 1,
          attachments: [
            {
              id: 1,
              addedOn: new Date('25 Mar 2020 10:12:57'),
              lastModifiedOn: null,
              name: 'attachment-1',
              size: 56789,
            },
            {
              id: 2,
              addedOn: new Date('17 Mar 2019 20:40:00'),
              lastModifiedOn: null,
              name: 'attachment-2',
              size: 1234567,
            },
          ],
        },
        projectId: 1,
        customFieldValues: [],
        denormalizedCustomFieldValues: [
          {
            id: 1,
            label: 'dnz-cuf-1',
            inputType: InputType.PLAIN_TEXT,
            denormalizedFieldHolderId: 1,
            fieldType: 'CF',
            value: 'dnz cuf value 1',
          },
        ],
      },
      {
        id: 2,
        order: 1,
        executionStatus: 'READY',
        action: 'do something',
        expectedResult: 'it should have done that',
        comment: '',
        lastExecutedOn: new Date().toISOString(),
        lastExecutedBy: 'admin',
        attachmentList: {
          id: 1,
          attachments: [],
        },
        projectId: 1,
        customFieldValues: [],
        denormalizedCustomFieldValues: [],
      },
    ],
    coverages: [
      {
        criticality: 'CRITICAL',
        directlyVerified: true,
        name: 'Requirement 1',
        projectName: 'P1',
        reference: 'This is a reference',
        requirementVersionId: 1,
        status: 'WORK_IN_PROGRESS',
        stepIndex: 0,
        unDirectlyVerified: false,
        verifiedBy: 'admin',
        verifyingCalledTestCaseIds: [12],
        coverageStepInfos: [{ id: 11, index: 0 }],
        verifyingTestCaseId: 3,
        milestoneLabels: '',
        milestoneMaxDate: '',
        milestoneMinDate: '',
      },
      {
        criticality: 'MAJOR',
        directlyVerified: true,
        name: 'Requirement 2',
        projectName: 'P1',
        reference: '',
        requirementVersionId: 2,
        status: 'APPROVED',
        stepIndex: 2,
        unDirectlyVerified: true,
        verifiedBy: 'admin',
        verifyingCalledTestCaseIds: [12],
        coverageStepInfos: [{ id: 11, index: 2 }],
        verifyingTestCaseId: 3,
        milestoneLabels: '',
        milestoneMaxDate: '',
        milestoneMinDate: '',
      },
    ],
    executionMode: 'MANUAL',
    lastExecutedOn: new Date().toISOString(),
    lastExecutedBy: 'admin',
    executionStatus: 'READY',
    automatedJobUrl: null,
    testAutomationServerKind: null,
    automatedExecutionResultUrl: null,
    automatedExecutionResultSummary: null,
    automatedExecutionDuration: null,
    nbIssues: 0,
    iterationId: -1,
    kind: executionKind ? executionKind : 'STANDARD',
    milestones: [],
    executionsCount: null,
    testPlanItemId: null,
    denormalizedEnvironmentVariables: [],
    denormalizedEnvironmentTags: { id: 1, value: '' },
    exploratorySessionOverviewInfo: null,
    sessionNotes: null,
    exploratoryExecutionRunningState: null,
    latestExploratoryExecutionEvent: null,
    reviewed: false,
    taskDivision: null,
    parentSprintStatus: null,
    extenderId: null,
  };
}

function mockRefDataWithBugtracker() {
  return new ReferentialDataMockBuilder()
    .withUser({
      admin: true,
      username: 'admin',
      userId: 1,
    })
    .withProjects({
      bugTrackerBinding: { bugTrackerId: 1, projectId: 1 },
    })
    .withBugTrackers({
      name: 'bt',
    })
    .build();
}

function mockIssueGridResponse(dataRows: DataRowModel[]): GridResponse {
  return mockGridResponse('remoteId', dataRows);
}

function mockIssueDataRow(
  issueId: Identifier,
  remoteId: Identifier,
  stepOrder: number,
  priority: string,
): DataRowModel {
  return mockDataRow({
    id: issueId.toString(),
    projectId: 1,
    data: {
      summary: 'summary',
      issueIds: [issueId],
      btProject: 'P1',
      executionSteps: [stepOrder],
      priority: priority,
      url: 'http://192.168.1.50/mantis/view.php?id=' + remoteId,
      remoteId: remoteId.toString(),
      assignee: 'admin',
      status: 'assigned',
    },
    allowMoves: true,
  });
}
