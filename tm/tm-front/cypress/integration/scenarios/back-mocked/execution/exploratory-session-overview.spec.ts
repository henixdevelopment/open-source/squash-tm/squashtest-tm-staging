import { SessionOverviewPage } from '../../../page-objects/pages/session-overview/session-overview.page';
import { mockSessionOverview } from '../../../data-mock/session-overview.data-mock';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { mockDataRow, mockGridResponse } from '../../../data-mock/grid.data-mock';
import { SessionOverviewPageInformationPanelElement } from '../../../page-objects/pages/session-overview/panels/session-overview-page-information-panel.element';
import { DataRowModel } from '../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { mockIssueUIModel } from '../../../data-mock/issue-ui-model.data-mock';
import { SessionNoteModel } from '../../../../../projects/sqtm-core/src/lib/model/execution/execution.model';
import { mockSessionNoteModel } from '../../../data-mock/execution.data-mock';
import {
  mockAttachmentListModel,
  mockAttachmentModel,
} from '../../../data-mock/generic-entity.data-mock';

describe('Exploratory session page', function () {
  it('should display correct information', () => {
    const sessionOverviewPage = SessionOverviewPage.initTestAtPage(1, mockSessionOverview());
    sessionOverviewPage.assertExists();
    const infoPanel = sessionOverviewPage.informationPanel;
    infoPanel.assertExists();
    infoPanel.updateDueDate();
    infoPanel.updateSessionDuration();
  });

  it('should display correct note types in exploratory executions grid', () => {
    const exploratoryExecutionsGrid: GridResponse = mockGridResponse('executionId', [
      mockDataRow({
        id: '1',
        data: {
          executionId: '1',
          reviewed: false,
          noteTypes: 'BUG,POSITIVE',
        },
        projectId: 1,
      }),
    ]);

    const sessionOverviewPage = SessionOverviewPage.initTestAtPage(
      1,
      mockSessionOverview(),
      exploratoryExecutionsGrid,
    );
    const executionsPanel = sessionOverviewPage.executionsPanel;
    executionsPanel.assertExists();
    executionsPanel.checkNoteTypes('1', ['A', 'P']);
  });

  it('should delete multiple executions', () => {
    const exploratoryExecutionsGrid: GridResponse = mockGridResponse('executionId', [
      mockExploratoryExecutionDataRow('1'),
      mockExploratoryExecutionDataRow('2'),
    ]);

    const sessionOverviewPage = SessionOverviewPage.initTestAtPage(
      1,
      mockSessionOverview(),
      exploratoryExecutionsGrid,
    );
    const executionsPanel = sessionOverviewPage.executionsPanel;
    executionsPanel.assertExists();
    executionsPanel.deleteRow(
      '1',
      mockGridResponse('executionId', [mockExploratoryExecutionDataRow('2')]),
    );

    executionsPanel.gridElement.selectRow('2', '#', 'leftViewport');
    sessionOverviewPage.massDeleteExecutions(mockGridResponse('executionId', []));

    sessionOverviewPage.executionsPanel.gridElement.assertRowCount(0);
  });

  it('should display correct text in charter panel', () => {
    const sessionOverviewPage = SessionOverviewPage.initTestAtPage(1, mockSessionOverview());
    const charterPanel = sessionOverviewPage.charterPanel;
    charterPanel.assertExists();
    charterPanel.checkCharter(
      "Les tests exploratoires consistent à effectuer des parcours libres dans l'application.",
    );
  });

  it('should update session status when starting or finishing a session', () => {
    const sessionOverviewPage = SessionOverviewPage.initTestAtPage(1, mockSessionOverview());
    const infoPanel = sessionOverviewPage.informationPanel;
    infoPanel.assertExists();
    checkSessionStatusAndButtonsAtInitialization(sessionOverviewPage, infoPanel);
    checkSessionStatusAndButtonsAtStarting(sessionOverviewPage, infoPanel);
    checkSessionStatusAndButtonsAtFinishing(sessionOverviewPage, infoPanel);
  });

  it('should update the session execution status when starting or finishing a session', () => {
    const sessionOverviewPage = SessionOverviewPage.initTestAtPage(1, mockSessionOverview());
    const infoPanel = sessionOverviewPage.informationPanel;
    infoPanel.assertExists();
    checkSessionExecutionStatusAtInitialization(infoPanel, sessionOverviewPage);
    startSessionAndCheckExecutionStatus(sessionOverviewPage, infoPanel);
    finishSessionAndCheckExecutionStatus(sessionOverviewPage, infoPanel);
    updateManuallyExecutionStatus(sessionOverviewPage, infoPanel);
  });

  it('should display correct review status for a session overview', () => {
    const executionNotReviewed = mockExploratoryExecutionDataRow('3');
    const sessionOverviewPage = SessionOverviewPage.initTestAtPage(1, mockSessionOverview());
    addExecutionsAndCheckReviewStatus(sessionOverviewPage, [executionNotReviewed], 'Revue à faire');
  });

  it('should display correct review status for a session overview after adding an execution', () => {
    const executionReviewed = mockExploratoryExecutionDataRow('1', true);
    const executionNotReviewed = mockExploratoryExecutionDataRow('2', true);
    const sessionOverviewPage = SessionOverviewPage.initTestAtPage(
      1,
      mockSessionOverview({ nbExecutions: 1, inferredSessionReviewStatus: 'DONE' }),
      mockGridResponse('executionId', [executionReviewed]),
    );
    addExecutionsAndCheckReviewStatus(
      sessionOverviewPage,
      [executionNotReviewed, executionReviewed],
      'Revue en cours',
    );
  });

  it('should update review status to "to do" after deleting an execution', () => {
    const executionReviewed = mockExploratoryExecutionDataRow('1', true);
    const executionNotReviewed = mockExploratoryExecutionDataRow('2', false);
    const sessionOverviewPage = SessionOverviewPage.initTestAtPage(
      1,
      mockSessionOverview({ nbExecutions: 2, inferredSessionReviewStatus: 'RUNNING' }),
      mockGridResponse('executionId', [executionReviewed, executionNotReviewed]),
    );

    sessionOverviewPage.startSession();
    sessionOverviewPage.finishSession();
    sessionOverviewPage.executionsPanel.deleteRow(
      '1',
      mockGridResponse('executionId', [executionNotReviewed]),
    );
    sessionOverviewPage.checkReviewStatusInCapsule('Revue à faire');
  });

  it('should update review status to "done" after deleting an execution', () => {
    const executionReviewed = mockExploratoryExecutionDataRow('1', true);
    const executionNotReviewed = mockExploratoryExecutionDataRow('2', false);
    const sessionOverviewPage = SessionOverviewPage.initTestAtPage(
      1,
      mockSessionOverview({ nbExecutions: 2, inferredSessionReviewStatus: 'RUNNING' }),
      mockGridResponse('executionId', [executionReviewed, executionNotReviewed]),
    );

    sessionOverviewPage.startSession();
    sessionOverviewPage.finishSession();
    sessionOverviewPage.executionsPanel.deleteRow(
      '2',
      mockGridResponse('executionId', [executionReviewed]),
    );
    sessionOverviewPage.checkReviewStatusInCapsule('Revue effectuée');
  });

  it('should add multiple executions with their assignee user at the same time', () => {
    const mockAssignableUsers = [
      { id: 1, login: 'ldefunes', firstName: 'Louis', lastName: 'de Funès' },
      { id: 2, login: 'alovelace', firstName: 'Ada', lastName: 'Lovelace' },
      { id: 3, login: 'jdoe', firstName: 'John', lastName: 'Doe' },
      { id: 4, login: 'psmith', firstName: 'Patti', lastName: 'Smith' },
      { id: 5, login: 'jmorrison', firstName: 'Jim', lastName: 'Morrison' },
    ];
    const sessionOverviewPage = SessionOverviewPage.initTestAtPage(
      1,
      mockSessionOverview({ assignableUsers: mockAssignableUsers }),
    );

    const dialog = sessionOverviewPage.openAddMultipleExecutionsDialog(mockAssignableUsers);

    dialog.selectAssignableUsersAndAddExecutions([
      'Louis de Funès (ldefunes)',
      'Ada Lovelace (alovelace)',
    ]);
    sessionOverviewPage.executionsPanel.gridElement.declareRefreshData(
      mockGridResponse('executionId', [
        mockExploratoryExecutionDataRow('1', false, 'Louis de Funès (ldefunes)'),
        mockExploratoryExecutionDataRow('2', false, 'Ada Lovelace (alovelace)'),
      ]),
    );
    const mock = new HttpMockBuilder('session-overview/*/add-executions-with-users/*')
      .post()
      .build();
    dialog.confirm();
    mock.wait();
    sessionOverviewPage.executionsPanel.assertHasExecutionsWithUsers([
      'Louis de Funès',
      'Ada Lovelace',
    ]);
  });

  it('should display the comments panel', () => {
    const sessionOverviewPage = SessionOverviewPage.initTestAtPage(
      1,
      mockSessionOverview({
        comments: 'Commentaires de la session',
      }),
    );
    sessionOverviewPage.commentsField.assertExists();
    sessionOverviewPage.commentsField.checkTextContent('Commentaires de la session');
  });

  it('should update the comments', () => {
    const sessionOverviewPage = SessionOverviewPage.initTestAtPage(1, mockSessionOverview());
    sessionOverviewPage.commentsField.setAndConfirmValue("Super session, on s'est bien amusé !");
    sessionOverviewPage.commentsField.checkTextContent("Super session, on s'est bien amusé !");
  });

  it('should display the activity panel with no content', () => {
    const sessionOverviewPage = SessionOverviewPage.initTestAtPage(1, mockSessionOverview());
    const sessionNotes = [];
    const issueUiModel = mockIssueUIModel();
    const activityPanel = sessionOverviewPage.showActivityPanel(sessionNotes, issueUiModel);
    activityPanel.assertHasEmptyActivityMessage();
  });

  it('should display the activity panel with session notes', () => {
    const sessionOverviewPage = SessionOverviewPage.initTestAtPage(1, mockSessionOverview());
    const sessionNotes: SessionNoteModel[] = mockActivityNotes();
    const issueUiModel = mockIssueUIModel();
    const activityPanel = sessionOverviewPage.showActivityPanel(sessionNotes, issueUiModel);
    activityPanel.getNote(0).assertHasContent('Note 1');
    activityPanel.getNote(0).assertHasKind('Commentaire');
    activityPanel.getNote(1).assertHasContent('Note 2');
    activityPanel.getNote(1).assertHasKind('Anomalie');
    activityPanel.getNote(1).assertHasAttachmentCount(2);
    sessionOverviewPage.checkActivityCount('2');
  });

  it('should filter activity on kind', () => {
    const sessionOverviewPage = SessionOverviewPage.initTestAtPage(1, mockSessionOverview());
    const sessionNotes: SessionNoteModel[] = mockActivityNotes();
    const issueUiModel = mockIssueUIModel();
    const activityPanel = sessionOverviewPage.showActivityPanel(sessionNotes, issueUiModel);
    activityPanel.filterByKind('Anomalie');
    activityPanel.getNote(0).assertHasKind('Anomalie');
    activityPanel.getNote(1).assertDoesNotExist();

    activityPanel.clearNoteKindFilter();
    activityPanel.filterByKind('Commentaire');
    activityPanel.getNote(0).assertHasKind('Commentaire');
    activityPanel.getNote(1).assertDoesNotExist();
  });

  it('should filter activity on user', () => {
    const sessionOverviewPage = SessionOverviewPage.initTestAtPage(1, mockSessionOverview());
    const sessionNotes: SessionNoteModel[] = mockActivityNotes();
    const issueUiModel = mockIssueUIModel();
    const activityPanel = sessionOverviewPage.showActivityPanel(sessionNotes, issueUiModel);
    activityPanel.filterByUser('Cypress');
    activityPanel.getNote(0).assertHasKind('Anomalie');
    activityPanel.getNote(1).assertDoesNotExist();

    activityPanel.clearUserFilter();
    activityPanel.filterByUser('Jane Doe');
    activityPanel.getNote(0).assertHasKind('Commentaire');
    activityPanel.getNote(1).assertDoesNotExist();
  });

  it('should collapse and expand notes', () => {
    const sessionOverviewPage = SessionOverviewPage.initTestAtPage(1, mockSessionOverview());
    const sessionNotes: SessionNoteModel[] = mockActivityNotes();
    const issueUiModel = mockIssueUIModel();
    const activityPanel = sessionOverviewPage.showActivityPanel(sessionNotes, issueUiModel);

    activityPanel.collapseAllNotes();
    activityPanel.getNote(0).assertIsCollapsed();

    activityPanel.expandAllNotes();
    activityPanel.getNote(0).assertIsExpanded();
  });

  it('should go to activity panel after trying to access a running execution', () => {
    const exploratoryExecutionsGrid: GridResponse = mockGridResponse('executionId', [
      mockDataRow({
        id: '1',
        data: {
          executionId: '1',
          reviewed: false,
          runningState: 'RUNNING',
          noteTypes: 'BUG,POSITIVE',
        },
        projectId: 1,
      }),
    ]);
    const sessionOverviewPage = SessionOverviewPage.initTestAtPage(
      1,
      mockSessionOverview(),
      exploratoryExecutionsGrid,
    );
    sessionOverviewPage.executionsPanel.clickOnAccessExecution('1');
    const warningAccessExecutionDialog = sessionOverviewPage.warningAccessExecutionDialog;

    const sessionNotes: SessionNoteModel[] = mockActivityNotes();
    const issueUiModel = mockIssueUIModel();
    const activityPanel = warningAccessExecutionDialog.consultActivity(sessionNotes, issueUiModel);
    activityPanel.assertExists();
  });

  it('should go to execution after trying to access a running execution', () => {
    const exploratoryExecutionsGrid: GridResponse = mockGridResponse('executionId', [
      mockDataRow({
        id: '1',
        data: {
          executionId: '1',
          reviewed: false,
          runningState: 'RUNNING',
          noteTypes: 'BUG,POSITIVE',
        },
        projectId: 1,
      }),
    ]);
    const sessionOverviewPage = SessionOverviewPage.initTestAtPage(
      1,
      mockSessionOverview(),
      exploratoryExecutionsGrid,
    );
    sessionOverviewPage.executionsPanel.clickOnAccessExecution('1');
    const warningAccessExecutionDialog = sessionOverviewPage.warningAccessExecutionDialog;
    const executionPage = warningAccessExecutionDialog.accessExecution();
    executionPage.assertExists();
  });
});

function mockExploratoryExecutionDataRow(
  id: string,
  isReviewed?: boolean,
  userFullName?: string,
  userId?: number,
) {
  return mockDataRow({
    id,
    data: {
      executionId: id,
      reviewed: isReviewed,
      noteTypes: 'BUG,POSITIVE',
      assigneeFullName: userFullName,
      assigneeId: userId,
    },
    projectId: 1,
  });
}

function mockActivityNotes(): SessionNoteModel[] {
  return [
    mockSessionNoteModel({ content: 'Note 1', lastModifiedBy: 'Jane Doe' }),
    mockSessionNoteModel({
      content: 'Note 2',
      kind: 'BUG',
      attachmentList: mockAttachmentListModel(12, [
        mockAttachmentModel({ id: 1, name: 'attachment1.png', size: 1234 }),
        mockAttachmentModel({ id: 2, name: 'attachment2.png', size: 65430 }),
      ]),
    }),
  ];
}

function checkSessionStatusAndButtonsAtInitialization(
  sessionOverviewPage: SessionOverviewPage,
  infoPanel: SessionOverviewPageInformationPanelElement,
) {
  sessionOverviewPage.checkSessionStatusInCapsule('Planifiée');
  infoPanel.checkSessionStatus('Planifiée');
  sessionOverviewPage.checkStartButton('Démarrer la session', true);
  sessionOverviewPage.checkEndButton(false);
}

function checkSessionStatusAndButtonsAtStarting(
  sessionOverviewPage: SessionOverviewPage,
  infoPanel: SessionOverviewPageInformationPanelElement,
) {
  sessionOverviewPage.startSession();
  sessionOverviewPage.checkSessionStatusInCapsule('En cours');
  sessionOverviewPage.checkStartButton('Reprendre la session', false);
  sessionOverviewPage.checkEndButton(true);
  infoPanel.checkSessionStatus('En cours');
}

function checkSessionStatusAndButtonsAtFinishing(
  sessionOverviewPage: SessionOverviewPage,
  infoPanel: SessionOverviewPageInformationPanelElement,
) {
  sessionOverviewPage.finishSession();
  sessionOverviewPage.checkSessionStatusInCapsule('Terminée');
  sessionOverviewPage.checkStartButton('Reprendre la session', true);
  sessionOverviewPage.checkEndButton(false);
  infoPanel.checkSessionStatus('Terminée');
}

function checkSessionExecutionStatusAtInitialization(
  infoPanel: SessionOverviewPageInformationPanelElement,
  sessionOverviewPage: SessionOverviewPage,
) {
  infoPanel.checkExecutionStatus('À exécuter');
  sessionOverviewPage.checkExecutionStatusInCapsule(' À exécuter ');
}

function startSessionAndCheckExecutionStatus(
  sessionOverviewPage: SessionOverviewPage,
  infoPanel: SessionOverviewPageInformationPanelElement,
) {
  sessionOverviewPage.startSession();
  sessionOverviewPage.checkExecutionStatusInCapsule('En cours');
  infoPanel.checkExecutionStatus('En cours');
  infoPanel.checkExecutionStatusEditableField(false);
}

function finishSessionAndCheckExecutionStatus(
  sessionOverviewPage: SessionOverviewPage,
  infoPanel: SessionOverviewPageInformationPanelElement,
) {
  sessionOverviewPage.finishSession();
  sessionOverviewPage.checkExecutionStatusInCapsule('En cours');
  infoPanel.checkExecutionStatus('En cours');
  infoPanel.checkExecutionStatusEditableField(true);
}

function updateManuallyExecutionStatus(
  sessionOverviewPage: SessionOverviewPage,
  infoPanel: SessionOverviewPageInformationPanelElement,
) {
  sessionOverviewPage.updateExecutionStatus('Succès');
  sessionOverviewPage.checkExecutionStatusInCapsule('Succès');
  infoPanel.checkExecutionStatus('Succès');
  infoPanel.checkExecutionStatusEditableField(true);
}

function addExecutionsAndCheckReviewStatus(
  sessionOverviewPage: SessionOverviewPage,
  executions: DataRowModel[],
  expectedReviewStatus: string,
) {
  sessionOverviewPage.startSession();
  sessionOverviewPage.finishSession();
  sessionOverviewPage.addExecution(mockGridResponse('executionId', executions));
  sessionOverviewPage.checkReviewStatusInCapsule(expectedReviewStatus);
}
