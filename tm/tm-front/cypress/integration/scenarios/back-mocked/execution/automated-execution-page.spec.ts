import { ExecutionPage } from '../../../page-objects/pages/execution/execution-page';
import { mockExecutionModel } from '../../../data-mock/execution.data-mock';
import { TestAutomationServerKind } from '../../../../../projects/sqtm-core/src/lib/model/test-automation/test-automation-server.model';
import {
  ExecutionModel,
  ExecutionStepModel,
} from '../../../../../projects/sqtm-core/src/lib/model/execution/execution.model';
import { ExecutionStatusKeys } from '../../../../../projects/sqtm-core/src/lib/model/level-enums/level-enum';
import { FailureDetail } from '../../../../../projects/sqtm-core/src/lib/model/execution/failure-detail.model';
import {
  ALL_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder,
} from '../../../utils/referential/referential-data-builder';

describe('Jenkins automated execution page', function () {
  it('should display correct capsules information and result summary', () => {
    const executionModel: ExecutionModel = getJenkinsExecutionModel();
    const automatedExecutionPage = ExecutionPage.initTestAtPage(1, executionModel);
    automatedExecutionPage.checkExecutionMode('Automatique');
    automatedExecutionPage.checkExecutionStatus('Succès');
    automatedExecutionPage.checkJobUrl('http://192.168.0.56/jenkins/job/test1');
    automatedExecutionPage.checkJobResult(
      'http://192.168.0.56/jenkins/job/test1/Squash_TA_HTML_Report',
    );
    automatedExecutionPage.automatedExecutionResultSummaryField.checkTextContent(
      'This is a TF execution summary',
    );
    automatedExecutionPage.assertDurationCapsuleNotExists();
  });

  function getJenkinsExecutionModel(): ExecutionModel {
    return mockExecutionModel({
      id: 1,
      projectId: 1,
      executionOrder: 2,
      name: 'NX - Test Case 1',
      prerequisite: '',
      attachmentList: {
        id: 1,
        attachments: [
          {
            id: 1,
            addedOn: new Date('25 Mar 2020 10:12:57'),
            lastModifiedOn: null,
            name: 'attachment-1',
            size: 56789,
          },
          {
            id: 2,
            addedOn: new Date('17 Mar 2019 20:40:00'),
            lastModifiedOn: null,
            name: 'attachment-2',
            size: 1234567,
          },
        ],
      },
      customFieldValues: [],
      tcImportance: 'HIGH',
      tcNatLabel: 'test-case.nature.NAT_BUSINESS_TESTING',
      tcNatIconName: 'indeterminate_checkbox',
      tcStatus: 'WORK_IN_PROGRESS',
      datasetLabel: 'JDD-1',
      tcTypeLabel: 'default',
      tcTypeIconName: 'indeterminate_checkbox',
      tcDescription: 'this is a description',
      comment: 'this is a comment',
      denormalizedCustomFieldValues: [],
      executionStepViews: [
        {
          id: 1,
          order: 0,
          executionStatus: 'READY',
          attachmentList: {
            id: 1,
            attachments: [],
          },
        } as ExecutionStepModel,
        {
          id: 2,
          order: 1,
          executionStatus: 'READY',
          attachmentList: {
            id: 1,
            attachments: [],
          },
        } as ExecutionStepModel,
        {
          id: 3,
          order: 2,
          executionStatus: 'READY',
          attachmentList: {
            id: 1,
            attachments: [],
          },
        } as ExecutionStepModel,
      ],
      coverages: [],
      executionMode: 'AUTOMATED',
      lastExecutedOn: new Date().toISOString(),
      lastExecutedBy: 'admin',
      executionStatus: 'SUCCESS',
      testAutomationServerKind: TestAutomationServerKind.jenkins,
      automatedJobUrl: 'http://192.168.0.56/jenkins/job/test1',
      automatedExecutionResultUrl: 'http://192.168.0.56/jenkins/job/test1/Squash_TA_HTML_Report',
      automatedExecutionResultSummary: 'This is a TF execution summary',
      automatedExecutionDuration: null,
      nbIssues: 0,
      iterationId: -1,
      kind: 'STANDARD',
      milestones: [],
      testPlanItemId: null,
      executionsCount: null,
      denormalizedEnvironmentVariables: null,
      denormalizedEnvironmentTags: null,
    });
  }
});

describe('SquashOrchestrator automated execution page ', function () {
  it('should display correct capsules and should not display job url and job result capsules and result summary', () => {
    const executionModel: ExecutionModel = getSquashOrchestratorExecutionModel('SUCCESS');
    const automatedExecutionPage = ExecutionPage.initTestAtPage(-1, executionModel);
    automatedExecutionPage.checkEnvironmentTags('linux, robotframework');
    automatedExecutionPage.checkEnvironmentVariables('variable test 1 : valeur 1', 1);
    automatedExecutionPage.assertJobUrlCapsuleNotExists();
    automatedExecutionPage.assertJobResultCapsuleNotExists();
    automatedExecutionPage.automatedExecutionResultSummaryField.assertNotExist();
    automatedExecutionPage.checkDuration('4s96ms');
  });

  it('should not display failure details section if premium with SUCCESS execution', () => {
    const executionModel: ExecutionModel = getSquashOrchestratorExecutionModel('SUCCESS');
    const refData = new ReferentialDataMockBuilder()
      .withProjects({
        allowAutomationWorkflow: true,
        permissions: ALL_PROJECT_PERMISSIONS,
      })
      .withPremiumLicense()
      .build();
    const automatedExecutionPage = ExecutionPage.initTestAtPage(
      -1,
      executionModel,
      null,
      null,
      refData,
      null,
    );
    automatedExecutionPage.assertFailureDetailSectionDoesNotExist();
  });

  it('should display empty failure details section if premium with FAILURE execution without failure details', () => {
    const executionModel: ExecutionModel = getSquashOrchestratorExecutionModel('FAILURE');
    const refData = new ReferentialDataMockBuilder()
      .withProjects({
        allowAutomationWorkflow: true,
        permissions: ALL_PROJECT_PERMISSIONS,
      })
      .withPremiumLicense()
      .build();
    const automatedExecutionPage = ExecutionPage.initTestAtPage(
      -1,
      executionModel,
      null,
      null,
      refData,
      null,
    );
    const failureDetailsPanel = automatedExecutionPage.clickFailureDetailsAnchorLink();
    failureDetailsPanel.assertNoFailureDetailLabelIsDisplayed();
  });

  it('should display failure details section if premium', () => {
    const executionModel: ExecutionModel = getSquashOrchestratorExecutionModel('FAILURE');
    const refData = new ReferentialDataMockBuilder()
      .withProjects({
        allowAutomationWorkflow: true,
        permissions: ALL_PROJECT_PERMISSIONS,
      })
      .withPremiumLicense()
      .build();
    const failureDetails: FailureDetail[] = [
      {
        id: -232,
        message: 'The test has failed : 2 != 3',
      },
      {
        id: -458,
        message: 'Could not find method: Unknown method',
      },
    ];
    const automatedExecutionPage = ExecutionPage.initTestAtPage(
      -1,
      executionModel,
      null,
      null,
      refData,
      failureDetails,
    );
    const failureDetailsPanel = automatedExecutionPage.clickFailureDetailsAnchorLink();
    failureDetailsPanel.assertTitleIsNotDisplayed();
    failureDetailsPanel.assertFailureDetailIsDisplayed('2', 'The test has failed : 2 != 3');
    failureDetailsPanel.assertFailureDetailIsDisplayed(
      '1',
      'Could not find method: Unknown method',
    );
  });

  function getSquashOrchestratorExecutionModel(
    executionStatus: ExecutionStatusKeys,
  ): ExecutionModel {
    return mockExecutionModel({
      id: -5,
      extenderId: -4,
      projectId: 1,
      executionOrder: 2,
      name: 'Squash Orchestrator execution',
      prerequisite: '',
      attachmentList: {
        id: 1,
        attachments: [
          {
            id: 1,
            addedOn: new Date('25 Mar 2020 10:12:57'),
            lastModifiedOn: null,
            name: 'attachment-1',
            size: 56789,
          },
          {
            id: 2,
            addedOn: new Date('17 Mar 2019 20:40:00'),
            lastModifiedOn: null,
            name: 'attachment-2',
            size: 1234567,
          },
        ],
      },
      customFieldValues: [],
      tcNatLabel: 'test-case.nature.NAT_UNDEFINED',
      tcNatIconName: 'indeterminate_checkbox_empty',
      tcTypeLabel: 'test-case.type.TYP_UNDEFINED',
      tcTypeIconName: 'indeterminate_checkbox_empty',
      tcStatus: 'WORK_IN_PROGRESS',
      tcImportance: 'LOW',
      datasetLabel: 'JDD-1',
      tcDescription: 'this is a description',
      comment: 'this is a comment',
      denormalizedCustomFieldValues: [],
      executionStepViews: [
        {
          id: 1,
          order: 0,
          executionStatus: 'READY',
          attachmentList: {
            id: 1,
            attachments: [],
          },
        } as ExecutionStepModel,
        {
          id: 2,
          order: 1,
          executionStatus: 'READY',
          attachmentList: {
            id: 1,
            attachments: [],
          },
        } as ExecutionStepModel,
        {
          id: 3,
          order: 2,
          executionStatus: 'READY',
          attachmentList: {
            id: 1,
            attachments: [],
          },
        } as ExecutionStepModel,
      ],
      coverages: [],
      executionMode: 'AUTOMATED',
      lastExecutedOn: new Date().toISOString(),
      lastExecutedBy: 'admin',
      executionStatus: executionStatus,
      testAutomationServerKind: TestAutomationServerKind.squashOrchestrator,
      automatedJobUrl: null,
      automatedExecutionResultUrl: null,
      automatedExecutionResultSummary: '',
      automatedExecutionDuration: 4096,
      nbIssues: 0,
      iterationId: -1,
      kind: 'STANDARD',
      milestones: [],
      testPlanItemId: -1,
      executionsCount: 1,
      denormalizedEnvironmentVariables: [
        {
          id: 1,
          namedValue: 'variable test 1 : valeur 1',
          evId: 1,
        },
        {
          id: 2,
          namedValue: 'variable test 2 : valeur 2',
          evId: 2,
        },
      ],
      denormalizedEnvironmentTags: {
        id: 1,
        value: 'linux, robotframework',
      },
    });
  }
});
