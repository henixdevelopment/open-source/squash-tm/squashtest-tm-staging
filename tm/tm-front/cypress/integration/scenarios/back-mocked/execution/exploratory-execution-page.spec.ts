import { ExecutionModel } from '../../../../../projects/sqtm-core/src/lib/model/execution/execution.model';
import { ExecutionPage } from '../../../page-objects/pages/execution/execution-page';
import { mockExecutionModel, mockSessionNoteModel } from '../../../data-mock/execution.data-mock';
import { InputType } from '../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { mockDataRow, mockGridResponse } from '../../../data-mock/grid.data-mock';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { ReferentialDataMockBuilder } from '../../../utils/referential/referential-data-builder';
import { ADMIN_PERMISSIONS } from '../../../../../projects/sqtm-core/src/lib/model/permissions/permissions.model';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';
import { DataRowModel } from '../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { Identifier } from '../../../../../projects/sqtm-core/src/lib/model/entity.model';
import { ExistingNoteElement } from '../../../page-objects/elements/execution-page/session-note-container.element';
import { NavBarElement } from '../../../page-objects/elements/nav-bar/nav-bar.element';
import { AlertDialogElement } from '../../../page-objects/elements/dialog/alert-dialog.element';

describe('Exploratory execution page', function () {
  // Temporary fix to avoid CKEditor errors
  function catchIncomingUncaughtErrors() {
    cy.on('uncaught:exception', (err) => {
      expect(err.message).to.include("Cannot read properties of null (reading 'unselectable')");
      return false;
    });
  }

  beforeEach(() => {
    catchIncomingUncaughtErrors();
    new HttpMockBuilder('exploratory-execution/*/pause').post().build();
    new HttpMockBuilder('issues/execution/*/all-known-issues').post().build();
  });

  it('it should display correct information', () => {
    const executionPage = whenNavigatingToAnExploratoryExecutionPage();
    thenDisplayedExecutionInformationIsCorrect(executionPage);
  });

  it('should display charter', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    executionPage.checkCollapsedCharterText('This is a test charter !');
    executionPage.extendCharter();
    executionPage.checkExtendedCharterText('This is a test charter !');
  });

  it('should display task division', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    executionPage.checkTaskDivision('Check Mozilla Firefox compatibility');
  });

  it('should display existing notes', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    thenExistingNotesShouldBeDisplayed(executionPage);
  });

  it('should collapse and expand a note', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    executionPage.durationPanel.runExploratoryExecution();
    thenNoteShouldBeCollapsableAndExpandable(executionPage);
  });

  it('should collapse all notes', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    whenCollapsingAllNotes(executionPage);
    thenAllNotesAreKeptCollapsed();
  });

  it('should create a note', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    executionPage.durationPanel.runExploratoryExecution();
    whenAddingNoteAtLastPosition(executionPage);
    thenNoteIsAddedAtLastPosition(executionPage);
  });

  it('should create a note below an existing one', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    executionPage.durationPanel.runExploratoryExecution();
    whenAddingNoteBelowExisting(1, executionPage);
    thenNoteOrderIsCorrectAndCreationFormIsAtBottom(
      ['A comment.', 'New note added on position 2.', 'A question?'],
      executionPage,
    );

    whenAddingNoteBelowExisting(3, executionPage);
    thenNoteOrderIsCorrectAndCreationFormIsAtBottom(
      [
        'A comment.',
        'New note added on position 2.',
        'A question?',
        'New note added on position 4.',
      ],
      executionPage,
    );
  });

  it('should modify an existing note', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    executionPage.durationPanel.runExploratoryExecution();
    const note = whenModifyingExistingNote(executionPage);
    thenNoteShouldDisplayUpdatedState(note);
  });

  it('should display note attachments', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    thenNoteAttachmentsAreDisplayed(executionPage);
  });

  it('should display reported issues', () => {
    const executionPage = givenAnExploratorySessionNotesPageWithDefinedBugtracker();
    thenNoteIssuesShouldBeDisplayed(executionPage);
  });

  it("should unbind a note's linked issue issues", () => {
    const executionPage = givenAnExploratorySessionNotesPageWithDefinedBugtracker();
    executionPage.durationPanel.runExploratoryExecution();
    const note = whenUnlinkingNoteIssue(executionPage);
    thenIssuesAreUpdated(note);
  });

  it('should move a note with drag and drop when the session is running', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    executionPage.durationPanel.runExploratoryExecution();
    cy.wait(1); // Give CKEditor some time to properly load
    whenDraggingAndDroppingNote(executionPage);
    thenNotesOrderIsUpdated(executionPage);
  });

  it('should move a note with drag and drop when the session is stopped', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    whenDraggingAndDroppingNote(executionPage);
    thenNotesOrderIsUpdated(executionPage);
  });

  it('should move a note up with the button when the session is running', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    executionPage.durationPanel.runExploratoryExecution();
    whenMovingNoteUp(2, executionPage);
    thenNotesOrderIsUpdated(executionPage);
  });

  it('should move a note up with the button when the session is stopped', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    whenMovingNoteUp(2, executionPage);
    thenNotesOrderIsUpdated(executionPage);
  });

  it('should move a note down with the button when the session is running', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    executionPage.durationPanel.runExploratoryExecution();
    whenMovingNoteDown(1, executionPage);
    thenNotesOrderIsUpdated(executionPage);
  });

  it('should move a note down with the button when the session is stopped', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    whenMovingNoteDown(1, executionPage);
    thenNotesOrderIsUpdated(executionPage);
  });

  it('should move an existing note under the creation form', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    executionPage.durationPanel.runExploratoryExecution();
    whenMovingNoteDown(2, executionPage);
    thenCreationFormIsAtIndex(2, executionPage);
  });

  it('should move the creation form above an existing note', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    executionPage.durationPanel.runExploratoryExecution();
    whenMovingNoteUp(3, executionPage);
    thenCreationFormIsAtIndex(2, executionPage);
  });

  it('should not be able to modify or add notes if session is stopped', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    executionPage.durationPanel.runExploratoryExecution();
    cy.wait(1); // Give CKEditor some time to properly load
    executionPage.durationPanel.stopExploratoryExecution();
    const noteContainer = executionPage.findNoteContainer(1);
    noteContainer.existingNoteElement.assertIsNotEditable(1);
    executionPage.findNoteContainer(3).newNoteElement.assertNotExist();
  });

  it('should not be able to add notes if session is paused', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    executionPage.durationPanel.runExploratoryExecution();
    cy.wait(1); // Give CKEditor some time to properly load
    executionPage.durationPanel.pauseExploratoryExecution();
    const noteContainer = executionPage.findNoteContainer(1);
    noteContainer.existingNoteElement.assertAddButtonDoesNotExist();
    executionPage.findNoteContainer(3).newNoteElement.assertNotExist();
  });

  it('should disable stop button when session is not running', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    executionPage.durationPanel.assertStopButtonIsDisabled();
  });

  it('should show note creation form when session is running', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    executionPage.durationPanel.runExploratoryExecution();
    thenExistingNotesAndCreationFormShouldBeDisplayed(executionPage);
  });

  it('should show pause button and activate stop button when session is running', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    executionPage.durationPanel.runExploratoryExecution();
    executionPage.durationPanel.assertPauseButtonIsVisible();
    executionPage.durationPanel.assertStopButtonIsEnabled();
  });

  it('should update time elapsed when session is running', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    executionPage.durationPanel.assertTimeElapsedIsZero();
    executionPage.durationPanel.runExploratoryExecution();
    cy.wait(1000);
    executionPage.durationPanel.assertTimeElapsedIsNotZero();
  });

  it('should pause a session', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    executionPage.durationPanel.checkStatusCapsuleValidity('Planifiée');
    executionPage.durationPanel.runExploratoryExecution();
    executionPage.durationPanel.checkStatusCapsuleValidity('En cours');
    cy.wait(1000);
    executionPage.durationPanel.pauseExploratoryExecution();
    executionPage.durationPanel.checkStatusCapsuleValidity('En cours');

    executionPage.durationPanel.getTimeElapsed().then((firstTimeElapsed) => {
      cy.wait(1000);
      executionPage.durationPanel
        .getTimeElapsed()
        .then((secondTimeElapsed) => assert(firstTimeElapsed === secondTimeElapsed));
    });
  });

  it('should stop a session', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    executionPage.durationPanel.runExploratoryExecution();
    cy.wait(1000);
    executionPage.durationPanel.stopExploratoryExecution();

    executionPage.durationPanel.assertRunButtonIsVisible();
    executionPage.durationPanel.checkStatusCapsuleValidity('Terminée');
    const noteContainer = executionPage.findNoteContainer(1);
    noteContainer.existingNoteElement.assertIsNotEditable(1);
    executionPage.findNoteContainer(3).newNoteElement.assertNotExist();
  });

  it('should show defined duration', () => {
    const executionModel = getExecutionModel();
    executionModel.exploratorySessionOverviewInfo.sessionDuration = 150;
    const executionPage = givenAnExploratorySessionNotesPage(executionModel);

    executionPage.durationPanel.assertDefinedDurationContains('02:30:00');
  });

  it('should show alert dialog when navigating if the session is running', () => {
    const executionPage = givenAnExploratorySessionNotesPage();
    executionPage.durationPanel.runExploratoryExecution();
    NavBarElement.navigateToAdministrationSystem();
    const alertDialog = new AlertDialogElement('confirm-pause-exploratory-execution');
    alertDialog.assertExists();
    alertDialog.clickOnCancelButton();
    executionPage.durationPanel.assertPauseButtonIsVisible();
  });

  function whenNavigatingToAnExploratoryExecutionPage(): ExecutionPage {
    const executionPage = ExecutionPage.initTestAtPage(1, getExecutionModel());
    executionPage.assertExists();
    return executionPage;
  }

  function thenDisplayedExecutionInformationIsCorrect(executionPage: ExecutionPage): void {
    executionPage.checkName('#3 : NX - Test Case 1');
    executionPage.checkStatus('Approuvé');
    executionPage.checkImportance('Faible');
    executionPage.checkNature('Métier');
    executionPage.checkType('Recevabilité');
    executionPage.checkTcDescription('description');
    executionPage.checkDenormalizedCustomField(0, 'Text CUF', 'value');
  }

  function givenAnExploratorySessionNotesPage(executionModel = getExecutionModel()): ExecutionPage {
    const executionPage = ExecutionPage.initTestAtPage(1, executionModel);
    executionPage.assertExists();
    executionPage.anchors.clickLink('notes');
    return executionPage;
  }

  function thenExistingNotesShouldBeDisplayed(executionPage: ExecutionPage): void {
    const note1 = executionPage.findNoteContainer(1).existingNoteElement;
    note1.checkContentText('A comment.');
    note1.kindSelect.checkKindText('Commentaire');

    const note2 = executionPage.findNoteContainer(2).existingNoteElement;
    note2.checkContentText('A question?');
    note2.kindSelect.checkKindText('Question');
  }

  function thenExistingNotesAndCreationFormShouldBeDisplayed(executionPage: ExecutionPage): void {
    thenExistingNotesShouldBeDisplayed(executionPage);
    executionPage.findNoteContainer(3).newNoteElement.assertExists();
  }

  function thenNoteShouldBeCollapsableAndExpandable(executionPage: ExecutionPage): void {
    const note = executionPage.findNoteContainer(1).existingNoteElement;
    note.clickOnCollapseButton();
    note.assertIsCollapsed();

    note.clickOnExpandButton();
    note.assertIsExpanded();
  }

  function whenCollapsingAllNotes(executionPage: ExecutionPage): void {
    executionPage.clickOnCollapseAllNotes();
  }

  function thenAllNotesAreKeptCollapsed(): void {
    const executionPage = givenAnExploratorySessionNotesPage();
    executionPage.findNoteContainer(1).existingNoteElement.assertIsCollapsed();
    executionPage.findNoteContainer(2).existingNoteElement.assertIsCollapsed();
  }

  function whenAddingNoteAtLastPosition(executionPage: ExecutionPage): void {
    const note = executionPage.findNoteContainer(3).newNoteElement;
    note.newNoteRichTextField.fill('Some content.');
    note.kindSelect.selectKind('Suggestion');

    note.confirm(
      mockSessionNoteModel({
        noteId: 3,
        noteOrder: 2,
        kind: 'SUGGESTION',
        content: 'Some content.',
        createdOn: '2023-02-21T17:00:00-05:00',
        createdBy: 'cypress-tester',
      }),
    );
  }

  function thenNoteIsAddedAtLastPosition(executionPage: ExecutionPage): void {
    const newlyCreatedNote = executionPage.findNoteContainer(3).existingNoteElement;
    newlyCreatedNote.kindSelect.checkKindText('Suggestion');
    newlyCreatedNote.checkContentText('Some content.');
  }

  function whenAddingNoteBelowExisting(
    indexOfPreviousNote: number,
    executionPage: ExecutionPage,
  ): void {
    const existingNote = executionPage.findNoteContainer(indexOfPreviousNote).existingNoteElement;
    existingNote.clickOnAddButton();

    const newNoteForm = executionPage.findNoteContainer(indexOfPreviousNote + 1).newNoteElement;
    newNoteForm.assertExists();

    newNoteForm.newNoteRichTextField.assertIsReady();
    newNoteForm.newNoteRichTextField.fill('New note added on 2nd position.');
    newNoteForm.kindSelect.selectKind('Suggestion');

    newNoteForm.confirm(
      mockSessionNoteModel({
        noteId: 10 + indexOfPreviousNote,
        noteOrder: indexOfPreviousNote,
        kind: 'SUGGESTION',
        content: `New note added on position ${indexOfPreviousNote + 1}.`,
        createdOn: '2023-02-21T17:00:00-05:00',
        createdBy: 'cypress-tester',
      }),
    );
  }

  function thenNoteOrderIsCorrectAndCreationFormIsAtBottom(
    expectedContents: string[],
    executionPage: ExecutionPage,
  ): void {
    expectedContents.forEach((content, idx) => {
      executionPage.findNoteContainer(idx + 1).existingNoteElement.checkContentText(content);
    });

    executionPage.findNoteContainer(expectedContents.length + 1).newNoteElement.assertExists();
  }

  function thenNoteShouldDisplayUpdatedState(note: ExistingNoteElement): void {
    note.checkContentText('Updated content');
    note.kindSelect.checkKindText('Anomalie');
  }

  function whenModifyingExistingNote(executionPage: ExecutionPage): ExistingNoteElement {
    const note = executionPage.findNoteContainer(1).existingNoteElement;
    let sessionNoteUpdateModel = mockSessionNoteModel({
      content: 'Updated content.',
      createdOn: '2023-02-21T12:00:00-05:00',
      lastModifiedOn: '2023-02-21T17:00:00-05:00',
    });

    note.updateContentText('Updated content.', sessionNoteUpdateModel);

    sessionNoteUpdateModel = {
      ...sessionNoteUpdateModel,
      kind: 'BUG',
    };

    note.kindSelect.selectKind('Anomalie', sessionNoteUpdateModel);
    return note;
  }

  function thenNoteAttachmentsAreDisplayed(executionPage: ExecutionPage): void {
    const note = executionPage.findNoteContainer(1).existingNoteElement;
    note.checkAttachmentCount(2);
    note.attachmentList.checkAttachmentData(
      0,
      'attachment-1',
      '55,46 Ko',
      'Ajouté le 25/03/2023 10:12',
    );

    note.attachmentList.checkAttachmentData(
      1,
      'attachment-2',
      '1,18 Mo',
      'Ajouté le 25/03/2023 10:40',
    );
  }

  function givenAnExploratorySessionNotesPageWithDefinedBugtracker(): ExecutionPage {
    const refData = mockRefDataWithBugtracker();
    const issueGridResponse = mockIssueGridResponse([
      mockIssueDataRow(1, '1', [0]),
      mockIssueDataRow(2, '2', [0]),
    ]);
    const executionPage = ExecutionPage.initTestAtPage(
      1,
      getExecutionModel(),
      issueGridResponse,
      null,
      refData,
    );
    executionPage.issueGrid.assertExists();

    const mockIssues = new HttpMockBuilder('issues/execution/*/all-known-issues')
      .responseBody(issueGridResponse)
      .post()
      .build();
    executionPage.anchors.clickLink('notes');
    mockIssues.wait();
    return executionPage;
  }

  function thenNoteIssuesShouldBeDisplayed(executionPage: ExecutionPage): void {
    const note = executionPage.findNoteContainer(1).existingNoteElement;
    note.checkIssueCount(2);
    note.checkIssueData(0, '1', 'P1', 'summary', 'minor', 'assigned');
  }

  function whenUnlinkingNoteIssue(executionPage: ExecutionPage): ExistingNoteElement {
    const note = executionPage.findNoteContainer(1).existingNoteElement;

    const issueGridResponseAfterDelete = mockIssueGridResponse([mockIssueDataRow(2, '2', [0])]);

    note.unbindOneIssue(0, issueGridResponseAfterDelete);
    return note;
  }

  function thenIssuesAreUpdated(note: ExistingNoteElement): void {
    note.checkIssueCount(1);
    note.issueCompactListElement.checkRowCount(1);
  }

  function whenDraggingAndDroppingNote(executionPage: ExecutionPage): void {
    executionPage.findNoteContainer(2).beginDrag();
    executionPage.findNoteContainer(1).dropOver();
  }

  function thenNotesOrderIsUpdated(executionPage: ExecutionPage): void {
    executionPage.findNoteContainer(1).existingNoteElement.checkContentText('A question?');

    executionPage.findNoteContainer(2).existingNoteElement.checkContentText('A comment');
  }

  function whenMovingNoteUp(noteIndex: number, executionPage: ExecutionPage): void {
    executionPage.findNoteContainer(noteIndex).moveUp();
  }

  function whenMovingNoteDown(noteIndex: number, executionPage: ExecutionPage): void {
    executionPage.findNoteContainer(noteIndex).moveDown();
  }

  function thenCreationFormIsAtIndex(expectedIndex: number, executionPage: ExecutionPage): void {
    executionPage.findNoteContainer(expectedIndex).newNoteElement.assertExists();
  }
});

function getExecutionModel(): ExecutionModel {
  return mockExecutionModel({
    executionMode: 'EXPLORATORY',
    exploratorySessionOverviewInfo: {
      sessionOverviewId: 1,
      charter: `<p>This is a <b>test charter</b> !</p>`,
      sessionDuration: null,
    },
    denormalizedCustomFieldValues: [
      {
        id: 1,
        denormalizedFieldHolderId: 1,
        value: 'value',
        fieldType: 'CF',
        label: 'Text CUF',
        inputType: InputType.PLAIN_TEXT,
      },
    ],
    sessionNotes: [
      {
        noteId: 1,
        kind: 'COMMENT',
        content: 'A <b>comment</b>.',
        noteOrder: 0,
        createdBy: 'Cypress',
        createdOn: '2023-02-21T12:00:00-05:00',
        lastModifiedBy: null,
        lastModifiedOn: null,
        attachmentList: {
          id: 3,
          attachments: [
            {
              id: 1,
              addedOn: new Date('25 Mar 2023 10:12:57'),
              lastModifiedOn: null,
              name: 'attachment-1',
              size: 56789,
            },
            {
              id: 2,
              addedOn: new Date('25 Mar 2023 10:40:00'),
              lastModifiedOn: null,
              name: 'attachment-2',
              size: 1234567,
            },
          ],
        },
      },
      {
        noteId: 2,
        kind: 'QUESTION',
        content: 'A <b>question</b>?',
        noteOrder: 1,
        createdBy: 'Cypress',
        createdOn: '2023-02-21T13:00:00-05:00',
        lastModifiedBy: 'admin',
        lastModifiedOn: '2023-02-21T14:00:00-05:00',
        attachmentList: { id: 2, attachments: [] },
      },
    ],
    exploratoryExecutionRunningState: 'NEVER_STARTED',
    taskDivision: 'Check Mozilla Firefox compatibility',
  });
}

function mockIssueGridResponse(dataRows: DataRowModel[]): GridResponse {
  return mockGridResponse('remoteId', dataRows);
}

function mockRefDataWithBugtracker() {
  return new ReferentialDataMockBuilder()
    .withBugTrackers({
      name: 'bt',
    })
    .withProjects({
      bugTrackerBinding: { bugTrackerId: 1, projectId: 1 },
      permissions: ADMIN_PERMISSIONS,
    })
    .build();
}

function mockIssueDataRow(
  issueId: Identifier,
  remoteId: Identifier,
  noteOrder: number[],
): DataRowModel {
  return mockDataRow({
    id: issueId.toString(),
    projectId: 1,
    data: {
      summary: 'summary',
      btProject: 'P1',
      executionSteps: noteOrder,
      issueIds: [issueId],
      priority: 'minor',
      executionOrder: 2,
      url: 'http://192.168.1.50/mantis/view.php?id=' + remoteId,
      remoteId: remoteId.toString(),
      assignee: 'admin',
      status: 'assigned',
      reportSites: [
        {
          executionId: 1,
          executionOrder: 0,
          executionName: 'NX - Test Case 1',
          suiteNames: [],
        },
      ],
    },
    allowMoves: true,
  });
}
