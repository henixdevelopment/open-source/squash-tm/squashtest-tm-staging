import { HomeWorkspacePage } from '../../../page-objects/pages/home-workspace/home-workspace.page';
import { HomeWorkspaceModel } from '../../../../../projects/sqtm-core/src/lib/model/home/home-workspace.model';
import { ReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { ReferentialDataMockBuilder } from '../../../utils/referential/referential-data-builder';
import { ExternalNavigationConsentDialogElement } from '../../../page-objects/elements/dialog/external-navigation-consent-dialog.element';

describe('External navigation consent dialog', function () {
  it('should not show consent dialog for link with same domain', () => {
    const homeWorkspacePage = HomeWorkspacePage.initTestAtPageWithModel(
      getHomeWorkspaceModel(Cypress.env('appBaseUrl')),
      getReferentialData(),
    );

    homeWorkspacePage.assertExists();
    homeWorkspacePage.find('#testLink').click();

    const dialog = new ExternalNavigationConsentDialogElement();
    dialog.assertNotExist();
  });

  it('should not show consent dialog for relative links', () => {
    const homeWorkspacePage = HomeWorkspacePage.initTestAtPageWithModel(
      getHomeWorkspaceModel('foo'),
      getReferentialData(),
    );

    homeWorkspacePage.assertExists();
    homeWorkspacePage.find('#testLink').click();

    const dialog = new ExternalNavigationConsentDialogElement();
    dialog.assertNotExist();
  });

  it('should show consent dialog for external link', () => {
    const homeWorkspacePage = HomeWorkspacePage.initTestAtPageWithModel(
      getHomeWorkspaceModel('https://example.com'),
      getReferentialData(),
    );

    homeWorkspacePage.assertExists();
    homeWorkspacePage.find('#testLink').click();

    const dialog = new ExternalNavigationConsentDialogElement();
    dialog.assertExists();
    dialog.assertUrlIs('https://example.com');
  });

  it('should show consent dialog for element inside external link', () => {
    const homeWorkspacePage = HomeWorkspacePage.initTestAtPageWithModel(
      getHomeWorkspaceModel('https://example.com'),
      getReferentialData(),
    );

    homeWorkspacePage.assertExists();
    homeWorkspacePage.find('#childElement').click();

    const dialog = new ExternalNavigationConsentDialogElement();
    dialog.assertExists();
    dialog.assertUrlIs('https://example.com');
  });
});

function getHomeWorkspaceModel(href: string): HomeWorkspaceModel {
  return {
    welcomeMessage: `
      <a id="testLink" href=${href} target="_blank">Click <span id="childElement">here</span></a>.
    `,
    dashboard: undefined,
  };
}

function getReferentialData(): ReferentialDataModel {
  return new ReferentialDataMockBuilder().withUser({ userId: 1, admin: true }).build();
}
