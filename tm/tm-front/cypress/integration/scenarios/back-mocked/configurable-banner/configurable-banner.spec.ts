import { HomeWorkspacePage } from '../../../page-objects/pages/home-workspace/home-workspace.page';
import { ReferentialDataMockBuilder } from '../../../utils/referential/referential-data-builder';
import { TestCaseWorkspacePage } from '../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { AdminWorkspaceProjectsPage } from '../../../page-objects/pages/administration-workspace/admin-workspace-projects.page';
import { mockGridResponse } from '../../../data-mock/grid.data-mock';
import { HomeWorkspaceModel } from '../../../../../projects/sqtm-core/src/lib/model/home/home-workspace.model';
import { ReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { AdminReferentialDataMockBuilder } from '../../../utils/referential/admin-referential-data-builder';
import { AdminReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/admin-referential-data.model';
import { GlobalConfigurationModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/global-configuration.model';

describe('License messages', function () {
  const testSets: TestSet[] = [
    {
      location: 'home page',
      accessor: (withBannerMessage) => {
        HomeWorkspacePage.initTestAtPageWithModel(
          getHomeWorkspaceModel(),
          getReferentialData(withBannerMessage),
        );
      },
    },
    {
      location: 'test case workspace',
      accessor: (withBannerMessage) => {
        TestCaseWorkspacePage.initTestAtPage(
          getEmptyGridResponse(),
          getReferentialData(withBannerMessage),
        );
      },
    },
    {
      location: 'admin projects workspace',
      accessor: (withBannerMessage) => {
        AdminWorkspaceProjectsPage.initTestAtPageProjects(
          getEmptyGridResponse(),
          getAdminReferentialData(withBannerMessage),
        );
      },
    },
  ];

  testSets.forEach(({ location, accessor }) => {
    it(`should show ${location} with no banner`, () => {
      accessor(false);
      assertConfigurableBannerIsNotShown();
    });

    it(`should show ${location} with banner`, () => {
      accessor(true);
      assertConfigurableBannerIsShown();
    });
  });
});

function assertConfigurableBannerIsNotShown() {
  cy.get('sqtm-core-configurable-banner').should('not.be.visible');
}

function assertConfigurableBannerIsShown() {
  cy.get('sqtm-core-configurable-banner')
    .should('exist')
    .should('contain.text', 'Hello darkness my old friend');
}

function getHomeWorkspaceModel(): HomeWorkspaceModel {
  return {
    welcomeMessage: 'Welcome to Squash',
    dashboard: undefined,
  };
}

function getReferentialData(withBannerMessage: boolean): ReferentialDataModel {
  return new ReferentialDataMockBuilder()
    .withGlobalConfiguration(
      withBannerMessage ? globalConfigurationWithBanner : globalConfigurationWithoutBanner,
    )
    .withUser({ userId: 1, admin: true })
    .build();
}

function getAdminReferentialData(withBannerMessage: boolean): AdminReferentialDataModel {
  return new AdminReferentialDataMockBuilder()
    .withGlobalConfiguration(
      withBannerMessage ? globalConfigurationWithBanner : globalConfigurationWithoutBanner,
    )
    .build();
}

const globalConfigurationWithoutBanner: GlobalConfigurationModel = {
  bannerMessage: null,
  milestoneFeatureEnabled: false,
  uploadFileExtensionWhitelist: [],
  uploadFileSizeLimit: 0,
  searchActivationFeatureEnabled: false,
  unsafeAttachmentPreviewEnabled: false,
};

const globalConfigurationWithBanner: GlobalConfigurationModel = {
  ...globalConfigurationWithoutBanner,
  bannerMessage: '<p>Hello darkness my old friend</p>',
};

function getEmptyGridResponse(): GridResponse {
  return mockGridResponse('id', []);
}

interface TestSet {
  location: string;
  accessor: (withBannerMessage: boolean) => void;
}
