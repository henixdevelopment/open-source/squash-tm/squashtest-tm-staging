import { HomeWorkspacePage } from '../../../page-objects/pages/home-workspace/home-workspace.page';
import { ReferentialDataMockBuilder } from '../../../utils/referential/referential-data-builder';

describe('NavBar', function () {
  it(`should don't display bugtrackers menu if no bugtrackers in referential data`, () => {
    const homeWorkspacePage = HomeWorkspacePage.initTestAtPage();
    const navBar = homeWorkspacePage.navBar;
    navBar.assertBugTrackerMenuNotExist();
  });

  it(`should display bugtrackers menu`, () => {
    const referentialDataMockBuilder = new ReferentialDataMockBuilder();
    referentialDataMockBuilder
      .withProjects(
        { bugTrackerBinding: { bugTrackerId: 1, projectId: 1 } },
        { bugTrackerBinding: { bugTrackerId: 2, projectId: 2 } },
      )
      .withBugTrackers({ name: 'Jira' }, { name: 'Mantis' })
      .withUser({
        hasAnyReadPermission: true,
        functionalTester: true,
      });
    const homeWorkspacePage = HomeWorkspacePage.initTestAtPage(referentialDataMockBuilder.build());
    const navBar = homeWorkspacePage.navBar;
    navBar.assertBugTrackerMenuExist();
    const bugTrackerMenu = navBar.showSubMenu('bugtrackers', 'bugtrackers-menu');
    bugTrackerMenu.assertExists();
    bugTrackerMenu.item('Jira').assertExists();
    bugTrackerMenu.item('Mantis').assertExists();
    bugTrackerMenu.hide();
  });

  it('should display initials in avatar', () => {
    const referentialDataMockBuilder = new ReferentialDataMockBuilder();
    referentialDataMockBuilder.withUser({
      username: 'admin',
      userId: 1,
      admin: true,
      firstName: 'Squash',
      lastName: 'administrator',
    });

    const homeWorkspace = HomeWorkspacePage.initTestAtPage(referentialDataMockBuilder.build());
    const navBar = homeWorkspace.navBar;
    navBar.assertAvatarHasText('SA');
  });

  it('should display action word workspace', () => {
    const referentialDataMockBuilder = new ReferentialDataMockBuilder();
    referentialDataMockBuilder.withUser({
      username: 'admin',
      userId: 1,
      admin: true,
      firstName: 'Squash',
      lastName: 'administrator',
    });

    let referentialData = referentialDataMockBuilder.build();
    referentialData = {
      ...referentialData,
      ultimateLicenseAvailable: true,
    };
    const homeWorkspace = HomeWorkspacePage.initTestAtPage(referentialData);
    const navBar = homeWorkspace.navBar;
    navBar.assertActionWordWorkspaceLinkExists();
    navBar.assertFakeActionWordNotExist();
  });

  it('should display advertising', () => {
    const referentialDataMockBuilder = new ReferentialDataMockBuilder();
    referentialDataMockBuilder.withUser({
      username: 'admin',
      userId: 1,
      admin: true,
      firstName: 'Squash',
      lastName: 'administrator',
    });
    const homeWorkspace = HomeWorkspacePage.initTestAtPage(referentialDataMockBuilder.build());
    const navBar = homeWorkspace.navBar;
    navBar.assertFakeActionWordExist();
  });

  it('should display workspace buttons as links', () => {
    const referentialDataMockBuilder = new ReferentialDataMockBuilder();
    referentialDataMockBuilder.withUser({
      username: 'admin',
      userId: 1,
      admin: true,
      firstName: 'Squash',
      lastName: 'administrator',
    });
    const homeWorkspace = HomeWorkspacePage.initTestAtPage(referentialDataMockBuilder.build());
    const navBar = homeWorkspace.navBar;
    navBar.assertWorkspaceLinkExist('requirement-link', 'Exigences', '/requirement-workspace');
    navBar.assertWorkspaceLinkExist('test-case-link', 'Cas de test', '/test-case-workspace');
    navBar.assertWorkspaceLinkExist('campaign-link', 'Exécutions', '/campaign-workspace');
    navBar.assertWorkspaceLinkExist('custom-report-link', 'Pilotage', '/custom-report-workspace');
  });

  it('should persist navbar folding in localstorage', () => {
    const referentialDataMockBuilder = new ReferentialDataMockBuilder();
    referentialDataMockBuilder.withUser({
      username: 'admin',
      userId: 1,
      admin: true,
      firstName: 'Squash',
      lastName: 'administrator',
    });
    const homeWorkspace = HomeWorkspacePage.initTestAtPage(referentialDataMockBuilder.build());
    const navBar = homeWorkspace.navBar;
    navBar.toggle();
    navBar.assertNavBarIsFold();
    HomeWorkspacePage.initTestAtPage(referentialDataMockBuilder.build());
    navBar.assertNavBarIsFold();
  });
});
