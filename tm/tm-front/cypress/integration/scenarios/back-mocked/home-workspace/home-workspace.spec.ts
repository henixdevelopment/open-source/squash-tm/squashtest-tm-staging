import { HomeWorkspacePage } from '../../../page-objects/pages/home-workspace/home-workspace.page';

describe('Home Workspace', function () {
  it('should show page and default message', () => {
    const homeWorkspacePage = HomeWorkspacePage.initTestAtPageWithModel();
    homeWorkspacePage.assertExists();
    homeWorkspacePage.assertMessageIsVisible();
    homeWorkspacePage.assertDefaultMessageIsVisible();
  });

  it('should show page and custom message', () => {
    const welcomeMessage = '<p>My nice custom message</p>';
    const homeWorkspacePage = HomeWorkspacePage.initTestAtPageWithModel({
      welcomeMessage,
      dashboard: undefined,
    });
    homeWorkspacePage.assertExists();
    homeWorkspacePage.assertMessageIsVisible();
    homeWorkspacePage.assertCustomMessageIsVisible();
    homeWorkspacePage.assertCustomMessageContains(welcomeMessage);
  });

  it('should toggle message and dashboard', () => {
    const homeWorkspacePage = HomeWorkspacePage.initTestAtPageWithModel();
    homeWorkspacePage.assertExists();
    homeWorkspacePage.assertMessageIsVisible();
    homeWorkspacePage.showDashboard();
    homeWorkspacePage.assertDashboardIsVisible();
    homeWorkspacePage.showWelcomeMessage();
    homeWorkspacePage.assertMessageIsVisible();
  });
});
