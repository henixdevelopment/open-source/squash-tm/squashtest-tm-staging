import { LoginPage } from '../../../page-objects/pages/login/login-page';
import { HomeWorkspacePage } from '../../../page-objects/pages/home-workspace/home-workspace.page';
import { NavBarElement } from '../../../page-objects/elements/nav-bar/nav-bar.element';
import { HttpMockBuilder } from '../../../utils/mocks/request-mock';

describe('Authentication', function () {
  it('should login authorized user', () => {
    const loginPage = LoginPage.navigateTo();
    loginPage.assertExists();
    const homeWorkspacePage = loginPage.login('admin', 'admin');
    homeWorkspacePage.assertExists();
  });

  it('should show error message if login fail', () => {
    const loginPage = LoginPage.navigateTo();
    loginPage.assertExists();
    loginPage.assertLoginFails('noop', 'noop');
    loginPage.assertLoginFailedWarningIsVisible();
  });

  it('should show login message if exist', () => {
    const loginMessage = '<p>Welcome to Squash TM 2.0</p>';
    const loginPage = LoginPage.navigateTo({
      loginMessage: loginMessage,
      isH2: false,
      isOpenIdConnectPluginInstalled: false,
      oAuth2ProviderNames: [],
      squashVersion: '2.0.0.RELEASE',
    });
    loginPage.assertExists();
    loginPage.assertLoginMessageContains(loginMessage);
  });

  it('should hide login message block if no login message exist', () => {
    const loginPage = LoginPage.navigateTo({
      loginMessage: '',
      isH2: false,
      isOpenIdConnectPluginInstalled: false,
      oAuth2ProviderNames: [],
      squashVersion: '2.0.0.RELEASE',
    });
    loginPage.assertExists();
    loginPage.assertLoginMessageIsNotVisible();
  });

  it('should show h2 warning if required', () => {
    const loginPage = LoginPage.navigateTo({
      loginMessage: '',
      isH2: true,
      isOpenIdConnectPluginInstalled: false,
      oAuth2ProviderNames: [],
      squashVersion: '2.0.0.RELEASE',
    });
    loginPage.assertExists();
    loginPage.assertH2warningIsVisible();
  });

  it('should show openId login buttons if multiple providers are configured and openID plugin is present', () => {
    const loginModel = {
      loginMessage: '',
      isH2: true,
      isOpenIdConnectPluginInstalled: true,
      oAuth2ProviderNames: ['google', 'gitlab'],
      squashVersion: '2.0.0.RELEASE',
    };
    const loginPage = LoginPage.navigateTo(loginModel);
    loginPage.assertExists();
    loginPage.assertOidcProviderLoginButtonIsVisible(loginModel.oAuth2ProviderNames[0]);
    loginPage.assertOidcProviderLoginButtonIsVisible(loginModel.oAuth2ProviderNames[1]);
  });

  it('should not show openId login buttons if openID plugin is absent', () => {
    const loginModel = {
      loginMessage: '',
      isH2: true,
      isOpenIdConnectPluginInstalled: false,
      oAuth2ProviderNames: ['google', 'gitlab'],
      squashVersion: '2.0.0.RELEASE',
    };
    const loginPage = LoginPage.navigateTo(loginModel);
    loginPage.assertExists();
    loginPage.assertOidcProviderLoginButtonDoesNotExist(loginModel.oAuth2ProviderNames[0]);
    loginPage.assertOidcProviderLoginButtonDoesNotExist(loginModel.oAuth2ProviderNames[1]);
  });

  it('should logout', () => {
    HomeWorkspacePage.initTestAtPage();
    const mock = new HttpMockBuilder('logout').build();
    NavBarElement.logout();
    mock.wait();
  });
});
