import { AdminWorkspaceEnvironmentVariablePage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-environment-variable.page';
import { RemoveEnvironmentVariableElement } from '../../../../page-objects/pages/administration-workspace/dialogs/remove-environment-variable.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { mockDataRow } from '../../../../data-mock/grid.data-mock';

describe('Administration workspace -Environment Variable', function () {
  const initialNodes: GridResponse = {
    count: 2,
    dataRows: [
      mockDataRow({
        id: '1',
        children: [],
        data: {
          evId: 1,
          name: 'EnvironmentVariable1',
          inputType: 'PLAIN_TEXT',
        },
      }),
      mockDataRow({
        id: '2',
        children: [],
        data: {
          evId: 2,
          name: 'EnvironmentVariable2',
          inputType: 'DROPDOWN_LIST',
        },
      }),
    ],
  };

  it('should display environment variable grid', () => {
    const page =
      AdminWorkspaceEnvironmentVariablePage.initTestAtPageEnvironmentVariables(initialNodes);
    const grid = page.grid;

    grid.assertRowExist(1);
    const row = grid.getRow(1);
    row.cell('name').textRenderer().assertContainsText('EnvironmentVariable1');
    row.cell('inputType').textRenderer().assertContainsText('Texte simple');
  });

  it('should add environment variable', () => {
    const page =
      AdminWorkspaceEnvironmentVariablePage.initTestAtPageEnvironmentVariables(initialNodes);
    const httpMockView = new HttpMockBuilder('environment-variable-view/3?*')
      .get()
      .responseBody({
        evId: 3,
        name: 'EV',
        inputType: 'PLAIN_TEXT',
      })
      .build();

    const dialog = page.openCreateEnvironmentVariable();
    dialog.assertExists();

    dialog.fillName('EV');
    dialog.selectTypeField('Texte simple');
    dialog.addWithOptions({
      addAnother: false,
      createResponse: { id: 3 },
      gridResponse: {
        count: initialNodes.count + 1,
        dataRows: [
          ...initialNodes.dataRows,
          mockDataRow({
            id: 3,
            children: [],
            data: {
              evId: 3,
              name: 'EV',
              inputType: 'PLAIN_TEXT',
            },
          }),
        ],
      },
    });
    httpMockView.wait();
  });

  it('should add environment variable with dropdown list', () => {
    const page =
      AdminWorkspaceEnvironmentVariablePage.initTestAtPageEnvironmentVariables(initialNodes);

    const httpMockView = new HttpMockBuilder('environment-variable-view/4?*')
      .get()
      .responseBody({
        evId: 4,
        name: 'EVlist',
        inputType: 'DROPDOWN_LIST',
      })
      .build();

    const dialog = page.openCreateEnvironmentVariable();
    dialog.assertExists();

    dialog.fillName('EVlist');
    dialog.selectTypeField('Liste déroulante');
    dialog.addDropdownListOption('option1');
    dialog.dropdownListOptionsGrid.assertRowCount(1);
    const row = dialog.dropdownListOptionsGrid.getRow('option1');
    row.cell('label').textRenderer().assertContainsText('option1');
    dialog.addWithOptions({
      addAnother: false,
      createResponse: { id: 4 },
      gridResponse: {
        count: initialNodes.count + 1,
        dataRows: [
          ...initialNodes.dataRows,
          mockDataRow({
            id: 4,
            children: [],
            data: {
              evId: 4,
              name: 'EVlist',
              inputType: 'DROPDOWN_LIST',
            },
          }),
        ],
      },
    });
    httpMockView.wait();
  });

  it('should search on name column', () => {
    const page =
      AdminWorkspaceEnvironmentVariablePage.initTestAtPageEnvironmentVariables(initialNodes);
    const grid = page.grid;
    grid.assertRowCount(2);

    page.fillSearchInput('Variable2', {
      count: 1,
      dataRows: [
        mockDataRow({
          id: '2',
          children: [],
          data: {
            evId: 2,
            name: 'EnvironmentVariable2',
            inputType: 'DROPDOWN_LIST',
          },
        }),
      ],
    });

    grid.assertRowCount(1);
  });

  it('should validate creation form', () => {
    const page =
      AdminWorkspaceEnvironmentVariablePage.initTestAtPageEnvironmentVariables(initialNodes);
    const dialog = page.openCreateEnvironmentVariable();
    dialog.assertExists();

    dialog.fillName('EVerror');
    dialog.selectTypeField('Liste déroulante');

    dialog.addDropdownListOption('name');
    dialog.addDropdownListOption('name');
    dialog.checkIfOptionNameAlreadyExistErrorIsDisplayed();

    dialog.addDropdownListOption('name1');
  });

  it('should delete environment variable', () => {
    const page =
      AdminWorkspaceEnvironmentVariablePage.initTestAtPageEnvironmentVariables(initialNodes);
    page.grid.assertExists();
    page.deleteSingleEnvironmentVariableInGrid(2);

    const deleteDialog = new RemoveEnvironmentVariableElement([2]);
    deleteDialog.deleteForSuccess({
      count: 1,
      dataRows: [initialNodes.dataRows[0]],
    });

    page.grid.assertRowCount(1);
  });

  it('should delete multiple environment variable', () => {
    const page =
      AdminWorkspaceEnvironmentVariablePage.initTestAtPageEnvironmentVariables(initialNodes);

    page.grid.selectRows([1, 2]);
    page.clickOnMultipleDeleteButton(true);
  });
});
