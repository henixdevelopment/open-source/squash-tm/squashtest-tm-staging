import { AdminWorkspaceProjectsPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-projects.page';
import { NavBarAdminElement } from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import {
  makeProjectViewData,
  mockBugTrackerReferentialDto,
} from '../../../../data-mock/administration-views.data-mock';
import { UnboundPartiesResponse } from '../../../../page-objects/pages/administration-workspace/project-view/dialogs/create-project-permissions.dialog';
import { ProjectIsBoundToATemplateAlert } from '../../../../page-objects/pages/administration-workspace/dialogs/project-is-bound-to-a-template-alert.element';
import {
  selectByDataTestButtonId,
  selectByDataTestDialogButtonId,
} from '../../../../utils/basic-selectors';
import { mockFieldValidationError } from '../../../../data-mock/http-errors.data-mock';
import { AdminReferentialDataMockBuilder } from '../../../../utils/referential/admin-referential-data-builder';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import {
  EnvironmentSelectionPanelDto,
  EnvironmentSelectionPanelElement,
} from '../../../../page-objects/elements/automated-execution-environments/environment-selection-panel.element';
import {
  getBoundEnvironmentVariables,
  TestAutoServerEnvironmentVariableElement,
} from '../../../../page-objects/pages/administration-workspace/test-automation-server-view/panels/test-auto-server-environment-variable-panel.element';
import { AclGroup } from '../../../../../../projects/sqtm-core/src/lib/model/permissions/permissions.model';
import { ProjectView } from '../../../../../../projects/sqtm-core/src/lib/model/project/project.model';
import {
  AuthenticationPolicy,
  AuthenticationProtocol,
} from '../../../../../../projects/sqtm-core/src/lib/model/third-party-server/authentication.model';
import { TestAutomationProject } from '../../../../../../projects/sqtm-core/src/lib/model/test-automation/test-automation-project.model';
import { ScmServer } from '../../../../../../projects/sqtm-core/src/lib/model/scm-server/scm-server.model';
import { TestAutomationServer } from '../../../../../../projects/sqtm-core/src/lib/model/test-automation/test-automation-server.model';
import { BugTrackerBinding } from '../../../../../../projects/sqtm-core/src/lib/model/bugtracker/bug-tracker.binding';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { Workflow } from '../../../../../../projects/sqtm-core/src/lib/model/orchestrator/workflow.model';
import { mockDataRow } from '../../../../data-mock/grid.data-mock';

const refData = new AdminReferentialDataMockBuilder()
  .withBugTrackers([
    mockBugTrackerReferentialDto({
      id: -1,
      name: 'BT JIRA',
      useProjectPaths: true,
    }),
  ])
  .build();

const refDataWithUltimate = new AdminReferentialDataMockBuilder()
  .withBugTrackers([
    mockBugTrackerReferentialDto({
      id: -1,
      name: 'BT JIRA',
      useProjectPaths: true,
    }),
  ])
  .withUltimateLicenseAvailable(true)
  .build();

describe('Administration Workspace - Projects - Project bound to Template', function () {
  it('should display bound project to template block with correct information', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[1].data.name,
      getProjectTemplateDto(),
    );

    projectViewPage.anchors.assertLinkExists('projectAssociatedToTemplate');

    projectViewPage.projectBoundToTemplatePanel.assertExists();
    const grid = projectViewPage.projectBoundToTemplatePanel.grid;
    grid.assertExists();
    grid.assertRowCount(3);
    grid.getRow(2).cell('name').linkRenderer().assertContainText('Project2');
  });

  function getProjectTemplateDto(): ProjectView {
    return makeProjectViewData({
      template: true,
      allProjectBoundToTemplate: [
        { id: 2, name: 'Project2' },
        { id: 3, name: 'Project3' },
        { id: 4, name: 'Project4' },
      ],
    });
  }
});

describe('Administration Workspace - Projects - Bugtracker', function () {
  it('it should bind a bugtracker to the project', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(null, null),
    );

    projectViewPage.assertExists();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();
    projectViewPage.automationPanel.fetchEnvironmentVariableMock.wait();

    projectViewPage.projectNamesInBugtracker.assertNotExist();

    projectViewPage.changeBugtracker('BT JIRA');

    projectViewPage.projectNamesInBugtracker.assertExists();
  });

  it('it should show special label when the selected bugtracker uses path to projects', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);

    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto({ projectId: 1, bugTrackerId: -1 }, []),
    );

    projectViewPage.assertHasBugTrackerProjectPathLabel();
  });

  it('it should add a project name to tag field', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto({ projectId: 4, bugTrackerId: -1 }, ['Project1']),
    );

    projectViewPage.assertExists();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.projectNamesInBugtracker.addTag('@_@', { projectNames: ['Project1', '@_@'] });
  });

  it('it should delete a project name from tag field', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto({ projectId: 4, bugTrackerId: -1 }, ['Project1', 'Project2']),
    );

    projectViewPage.assertExists();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.projectNamesInBugtracker.deleteTagWithValue('Project2', {
      projectNames: ['Project1'],
    });
  });

  it('it should forbid to delete a unique tag field', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto({ projectId: 4, bugTrackerId: -1 }, ['Project1']),
    );

    projectViewPage.assertExists();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.projectNamesInBugtracker.checkIfTagIsClosable('Project1', false);
  });

  it('it should remove a bugtracker binding', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto({ projectId: 4, bugTrackerId: -1 }, ['Project1']),
    );

    projectViewPage.assertExists();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.selectNoBugtracker();
    projectViewPage.projectNamesInBugtracker.assertNotExist();
  });

  function getProjectDto(
    bugTrackerBinding: BugTrackerBinding,
    bugtrackerProjectNames: string[],
  ): ProjectView {
    return makeProjectViewData({
      availableBugtrackers: [
        {
          id: -1,
          kind: 'jira.cloud',
          name: 'BT JIRA',
          url: 'https://',
          iframeFriendly: false,
          authPolicy: AuthenticationPolicy.APP_LEVEL,
          authProtocol: AuthenticationProtocol.BASIC_AUTH,
        },
      ],
      bugTrackerBinding: bugTrackerBinding,
      bugtrackerProjectNames: bugtrackerProjectNames,
    });
  }
});

describe('Administration Workspace - Projects - Artificial intelligence servers', function () {
  it('it should bind and unbind an AI server to the project', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(
      initialNodes,
      refDataWithUltimate,
    );
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(),
    );

    projectViewPage.assertExists();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();
    projectViewPage.aiServerPanel.assertExists();
    projectViewPage.aiServerPanel.selectAiServer('Open AI');
    projectViewPage.aiServerPanel.selectAiServer("Pas de serveur d'intelligence artificielle");
  });

  function getProjectDto(): ProjectView {
    return makeProjectViewData({
      availableAiServers: [
        {
          id: 1,
          description: '',
          name: 'Open AI',
          url: 'https://test.com',
          payloadTemplate: '',
          createdBy: 'me',
          createdOn: '2024-02-27',
          lastModifiedBy: null,
          lastModifiedOn: null,
          jsonPath: 'jsonpath',
        },
      ],
    });
  }
});

describe('Administration Workspace - Projects - Permissions', function () {
  it('should show permissions count', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(true),
    );
    projectViewPage.assertPermissionCount(2);
  });

  it('should add project permissions to users', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(false),
    );

    projectViewPage.assertExists();
    projectViewPage.permissionsPanel.grid.assertExists();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    const unboundParties: UnboundPartiesResponse = {
      teams: [],
      users: [
        {
          groupId: 'user',
          id: '-1',
          label: 'John Dane (jdane)',
        },
        {
          groupId: 'user',
          id: '-2',
          label: 'Jane Doe (jdoe)',
        },
      ],
    };

    const createPermissionDialog =
      projectViewPage.permissionsPanel.clickOnAddUserPermissionButton(unboundParties);

    createPermissionDialog.selectParties('Jane Doe (jdoe)', 'John Dane (jdane)');
    createPermissionDialog.selectProfile('Testeur référent');

    createPermissionDialog.confirm([]);
  });

  it('should add project permissions to teams', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(false),
    );

    projectViewPage.assertExists();
    projectViewPage.permissionsPanel.grid.assertExists();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    const unboundParties: UnboundPartiesResponse = {
      teams: [
        {
          groupId: 'team',
          id: '-1',
          label: 'team1',
        },
        {
          groupId: 'team',
          id: '-2',
          label: 'team2',
        },
      ],
      users: [],
    };

    const createPermissionDialog =
      projectViewPage.permissionsPanel.clickOnAddTeamPermissionButton(unboundParties);

    createPermissionDialog.selectParties('team1', 'team2');
    createPermissionDialog.selectProfile('Testeur référent');

    createPermissionDialog.confirm([]);
  });

  it('should remove project permission', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(true),
    );

    projectViewPage.assertExists();
    projectViewPage.permissionsPanel.grid.assertExists();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.permissionsPanel.deleteOne('Jane Doe (jdoe)', []);
  });

  it('should remove multiple project permissions', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(true),
    );

    projectViewPage.assertExists();
    projectViewPage.permissionsPanel.grid.assertExists();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.permissionsPanel.deleteMultiple(['Jane Doe (jdoe)', 'Team A']);
  });

  it('should prevent adding permissions with empty fields', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(false),
    );

    projectViewPage.assertExists();
    projectViewPage.permissionsPanel.grid.assertExists();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    const unboundParties = { teams: [], users: [] };
    const createPermissionDialog =
      projectViewPage.permissionsPanel.clickOnAddUserPermissionButton(unboundParties);

    createPermissionDialog.clickOnConfirmButton();
    cy.get('.sqtm-core-error-message').should('have.length', 2);
  });

  function getProjectDto(withPermissions: boolean): ProjectView {
    return makeProjectViewData({
      partyProjectPermissions: withPermissions
        ? [
            {
              partyId: -1,
              partyName: 'Jane Doe (jdoe)',
              permissionGroup: {
                id: 1,
                qualifiedName: AclGroup.PROJECT_MANAGER,
                active: true,
                system: true,
              },
              team: false,
              projectId: 4,
              active: true,
            },
            {
              partyId: -2,
              partyName: 'Team A',
              permissionGroup: {
                id: 2,
                qualifiedName: AclGroup.TEST_RUNNER,
                active: true,
                system: true,
              },
              team: true,
              projectId: 4,
              active: true,
            },
          ]
        : [],
    });
  }
});

describe('Administration Workspace - Projects - Execution', function () {
  it('should forbid to switch status availability if project is bound to a template', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(true, false, false),
    );

    projectViewPage.assertExists();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.clickOnSwitchAllowTcModifDuringExec();

    const alert = new ProjectIsBoundToATemplateAlert();
    alert.assertMessage();
    alert.close();
  });

  it('should alert that UNTESTABLE status is used within project when trying to switch it off', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(false, false, true),
    );

    projectViewPage.assertExists();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    const dialog = projectViewPage.openDialogForStatusesInUseWithinProject('UNTESTABLE');
    dialog.editStatusInUse('UNTESTABLE', 'Succès');
  });

  it('should alert that SETTLED status is used within project when trying to switch it off', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(false, true, false),
    );

    projectViewPage.assertExists();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    const dialog = projectViewPage.openDialogForStatusesInUseWithinProject('SETTLED');
    dialog.editStatusInUse('SETTLED', 'Succès');
  });

  function getProjectDto(
    isLinkedToTemplate: boolean,
    settledStatusIsUsedWithinProject: boolean,
    untestableStatusIsUsedWithinProject: boolean,
  ): ProjectView {
    return makeProjectViewData({
      linkedTemplateId: isLinkedToTemplate ? 2 : null,
      linkedTemplate: isLinkedToTemplate ? 'Template1' : null,
      allowTcModifDuringExec: false,
      allowedStatuses: { SETTLED: true, UNTESTABLE: true },
      statusesInUse: {
        SETTLED: settledStatusIsUsedWithinProject,
        UNTESTABLE: untestableStatusIsUsedWithinProject,
      },
    });
  }
});

describe('Administration Workspace - Projects - Automation', function () {
  it('should set a SCM repository', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(),
    );

    projectViewPage.assertExists();
    const automationPanel = projectViewPage.showAutomationPanel();

    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    automationPanel.checkScmServerBlockVisibility(false);
    automationPanel.selectAutomationWorkflowType('Squash avancé');

    automationPanel.checkScmServerBlockVisibility(true);
    automationPanel.useTreeStructureField.checkValue(false);
    automationPanel.useTreeStructureField.toggle();
    automationPanel.useTreeStructureField.checkValue(true);

    automationPanel.checkScmRepositoryFieldVisibility(false);
    automationPanel.selectScmServer('server1');
    automationPanel.checkScmRepositoryFieldVisibility(true);

    automationPanel.scmRepositorySelectField.setAndConfirmValueNoButton('repo1');

    automationPanel.selectAutomationWorkflowType('Aucun');
    automationPanel.checkScmServerBlockVisibility(false);
    automationPanel.checkScmRepositoryFieldVisibility(false);
  });

  it('should set an execution server', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(),
    );

    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.automationPanel.checkTaJobsBlockVisibility(false);
    projectViewPage.automationPanel.executionServerField.setAndConfirmValueNoButton('taServer2');
    projectViewPage.automationPanel.checkTaJobsBlockVisibility(true);
  });

  it('should add a job', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(-1),
      {},
    );

    const addJobDialog = projectViewPage.automationPanel.openAddJobDialogAsAdmin([
      { remoteName: 'Job1', label: 'Job1' } as TestAutomationProject,
      { remoteName: 'Job2', label: 'Job2' } as TestAutomationProject,
    ]);

    addJobDialog.checkRowIsEditable('Job2', false);
    addJobDialog.toggleJobSelection('Job2');
    addJobDialog.setJobLabelInTM('Job2', 'blabla');
    addJobDialog.toggleJobCanRunBdd('Job2');
    addJobDialog.confirm();
  });

  it('should add a job as project manager', () => {
    const managerRefData = {
      ...refData,
      user: {
        ...refData.user,
        admin: false,
        hasAnyReadPermission: true,
        projectManager: true,
        milestoneManager: true,
        clearanceManager: true,
        functionalTester: true,
        automationProgrammer: true,
      },
    };
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, managerRefData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(-1),
      {},
    );

    projectViewPage.automationPanel.clickAddJobButton();
    const taServerConnectDialog = projectViewPage.automationPanel.openTaServerConnectDialog();
    taServerConnectDialog.fillUsername('manager');
    taServerConnectDialog.fillPassword('manager');

    const addJobDialog = projectViewPage.automationPanel.openAddJobDialogAsManager([
      { remoteName: 'Job1', label: 'Job1' } as TestAutomationProject,
      { remoteName: 'Job2', label: 'Job2' } as TestAutomationProject,
    ]);

    taServerConnectDialog.confirm();

    addJobDialog.waitForInitialDataFetch();

    addJobDialog.checkRowIsEditable('Job2', false);
    addJobDialog.toggleJobSelection('Job2');
    addJobDialog.setJobLabelInTM('Job2', 'blabla');
    addJobDialog.toggleJobCanRunBdd('Job2');
    addJobDialog.confirm();
  });

  it('should modify a job inside the grid', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const dto = getProjectDto(-1);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name, dto);

    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.automationPanel.changeJobLabel(
      'rem1',
      'new label',
      dto.boundTestAutomationProjects,
    );
    projectViewPage.automationPanel.toggleJobCanRunBdd('rem1', dto.boundTestAutomationProjects);
    projectViewPage.automationPanel.toggleJobCanRunBdd('rem2', dto.boundTestAutomationProjects);

    // Only one job per TM project should be able to run BDD. We don't test this in ITs because
    // this is happening server-side and the front won't do anything fancy (only refreshing the
    // grid with fresh data from the server)
  });

  it('should show error messages', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(-1),
    );
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();
    const automationPanel = projectViewPage.showAutomationPanel();

    // should show an error message if the execution server is unreachable when showing job binding dialog
    const alert = automationPanel.openAddJobDialogWithError();
    cy.get(alert.baseSelector).find(selectByDataTestDialogButtonId('cancel')).click();

    // should show error message in job binding dialog when label is blank
    const addJobDialog = automationPanel.openAddJobDialogAsAdmin([
      { remoteName: 'Job1', label: 'Job1' } as TestAutomationProject,
      { remoteName: 'Job2', label: 'Job2' } as TestAutomationProject,
    ]);

    addJobDialog.toggleJobSelection('Job1');
    addJobDialog.clearJobLabelInTM('Job1');
    addJobDialog.confirmWithClientSideErrors();
    addJobDialog.checkRequiredErrorMessage(true);

    // should show error message in job binding dialog when label are duplicates
    addJobDialog.toggleJobSelection('Job2');
    addJobDialog.setJobLabelInTM('Job1', 'Job1');
    addJobDialog.setJobLabelInTM('Job2', 'Job1');
    addJobDialog.confirmWithClientSideErrors();
    addJobDialog.checkDuplicateLabelError(true);

    // should show server-side error messages
    addJobDialog.toggleJobSelection('Job2');
    const error = mockFieldValidationError('label', 'sqtm-core.exception.duplicate.tmlabel');
    addJobDialog.confirmWithServerSideErrors(error);
    addJobDialog.checkDuplicateLabelError(true);
  });

  it('should delete a job', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectDto = getProjectDto(-1);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      projectDto,
    );

    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    const automationPanel = projectViewPage.showAutomationPanel();
    automationPanel.taServerJobsGrid.assertRowCount(2);
    automationPanel.deleteSingleJob('job1', true, [projectDto.boundTestAutomationProjects[0]]);
    automationPanel.deleteSingleJob('job2', false, []);
  });

  it('should show automated suites and executions block', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(),
    );

    projectViewPage.assertExists();
    const automationPanel = projectViewPage.showAutomationPanel();
    automationPanel.assertAutomatedSuiteAndExecutionCleaningIsVisible();
  });

  it('should update automated suites lifetime', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(),
    );

    projectViewPage.assertExists();
    const automationPanel = projectViewPage.showAutomationPanel();

    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    automationPanel.automatedSuitesLifetimeField.setAndConfirmValue(300);
  });

  it('should open delete automated suites pop-up', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(),
    );

    projectViewPage.assertExists();
    const automationPanel = projectViewPage.showAutomationPanel();

    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    const dialog = automationPanel.openCleanAutomatedSuiteDialog('clean-automated-suite');
    dialog.assertDialogTitleIs('Suppression des suites automatisées et des exécutions');
  });

  it('should open complete prune pop-up', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(),
    );

    projectViewPage.assertExists();
    const automationPanel = projectViewPage.showAutomationPanel();

    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    const dialog = automationPanel.openCleanAutomatedSuiteDialog('complete-prune');
    dialog.assertDialogTitleIs('Nettoyage complet des pièces jointes');
  });

  it('should open partial prune pop-up', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(),
    );

    projectViewPage.assertExists();
    const automationPanel = projectViewPage.showAutomationPanel();

    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    const dialog = automationPanel.openCleanAutomatedSuiteDialog('partial-prune');
    dialog.assertDialogTitleIs(
      'Nettoyage partiel des pièces jointes - exécutions en succès seulement',
    );
  });

  it('should update implementation technology', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(),
    );

    projectViewPage.assertExists();
    const automationPanel = projectViewPage.showAutomationPanel();

    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    automationPanel.bddTechnologyField.setAndConfirmValueNoButton('Robot Framework');
    automationPanel.bddScriptLanguageField.assertIsNotEditable();

    automationPanel.bddTechnologyField.setAndConfirmValueNoButton('Cucumber');
    automationPanel.bddScriptLanguageField.assertIsEditable();
    automationPanel.bddScriptLanguageField.setAndConfirmValueNoButton('Espagnol');
  });

  function getProjectDto(taServerId?: number): ProjectView {
    return makeProjectViewData({
      scmRepositoryId: taServerId ? 1 : null,
      automationWorkflowType: 'NONE',
      taServerId,
      availableScmServers: [
        {
          serverId: -1,
          name: 'server1',
          repositories: [
            {
              scmRepositoryId: 1,
              serverId: 1,
              name: 'repo1',
            },
          ],
        } as ScmServer,
        {
          serverId: -2,
          name: 'server2',
          repositories: [
            {
              scmRepositoryId: 2,
              serverId: 2,
              name: 'repo1',
            },
          ],
        } as ScmServer,
      ],
      availableTestAutomationServers: [
        {
          id: -1,
          name: 'taServer1',
          baseUrl: 'https://url1',
          authProtocol: AuthenticationProtocol.BASIC_AUTH,
        } as TestAutomationServer,
        { id: -2, name: 'taServer2', baseUrl: 'https://url2' } as TestAutomationServer,
      ],
      boundMilestonesInformation: [],
      boundTestAutomationProjects: [
        {
          taProjectId: -1,
          label: 'job1',
          remoteName: 'rem1',
          canRunBdd: false,
          executionEnvironments: '',
          serverId: -1,
          tmProjectId: 4,
        },
        {
          taProjectId: -2,
          label: 'job2',
          remoteName: 'rem2',
          canRunBdd: false,
          executionEnvironments: '',
          serverId: -1,
          tmProjectId: 4,
        },
      ],
      bddScriptLanguage: 'ENGLISH',
      bddImplementationTechnology: 'CUCUMBER_4',
    });
  }
});

describe('Administration Workspace - Project - TAServer environments', function () {
  it('should set an execution server that supports environment tags', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectData(),
    );

    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.automationPanel.assertEnvironmentsBlockIsHidden();

    const envPanelMock = new HttpMockBuilder(
      'generic-projects/*/automated-execution-environments/all?*',
    )
      .responseBody(mergeWithDefaultPanelResponse({}))
      .build();

    const envVariableMock = new HttpMockBuilder('bound-environment-variables/project/*')
      .responseBody({ boundEnvironmentVariables: [] })
      .build();

    const workflowMock = new HttpMockBuilder('orchestrator-operation/*/project/*/workflows')
      .get()
      .responseBody({
        response: [],
        reachable: true,
      })
      .build();

    projectViewPage.automationPanel.executionServerField.setAndConfirmValueNoButton('taServer2');
    envPanelMock.wait();
    envVariableMock.wait();
    workflowMock.wait();
    projectViewPage.automationPanel.assertEnvironmentsBlockIsVisible();
  });

  it('should show inherited tags', () => {
    const envPanel = navigateToEnvironmentSelectionPanel(
      mergeWithDefaultPanelResponse({
        project: { projectTags: ['override'], areProjectTagsInherited: true },
        server: { defaultTags: ['inherited'] },
      }),
    );

    envPanel.assertSelectedTagsContainAll(['inherited']);
  });

  it('should show project overridden tags', () => {
    const envPanel = navigateToEnvironmentSelectionPanel(
      mergeWithDefaultPanelResponse({
        project: { projectTags: ['override'], areProjectTagsInherited: false },
        server: { defaultTags: ['inherited'] },
      }),
    );

    envPanel.assertSelectedTagsContainAll(['override']);
  });

  it('should clear tag overrides', () => {
    const envPanel = navigateToEnvironmentSelectionPanel(
      mergeWithDefaultPanelResponse({
        project: { projectTags: ['override'], areProjectTagsInherited: false },
        server: { defaultTags: ['inherited'] },
      }),
    );

    envPanel.assertSelectedTagsContainAll(['override']);
    envPanel.clearTagOverrides();
    envPanel.assertSelectedTagsContainAll(['inherited']);
  });

  it('should change project token', () => {
    const envPanel = navigateToEnvironmentSelectionPanel(
      mergeWithDefaultPanelResponse({
        project: { hasProjectToken: true },
      }),
    );

    const firstRefreshMock = new HttpMockBuilder(
      'generic-projects/*/automated-execution-environments/all?*',
    )
      .responseBody(
        mergeWithDefaultPanelResponse({
          project: { hasProjectToken: false },
        }),
      )
      .build();

    envPanel.clearTokenOverride();
    firstRefreshMock.wait();
    envPanel.assertResetTokenButtonIsHidden();

    const secondRefreshMock = new HttpMockBuilder(
      'generic-projects/*/automated-execution-environments/all?*',
    )
      .responseBody(
        mergeWithDefaultPanelResponse({
          project: { hasProjectToken: true },
        }),
      )
      .build();

    envPanel.setTokenOverride('my new token');
    secondRefreshMock.wait();
    envPanel.assertResetTokenButtonIsVisible();
  });

  function navigateToEnvironmentSelectionPanel(
    panelResponse: EnvironmentSelectionPanelDto,
  ): EnvironmentSelectionPanelElement {
    initializeEnvironmentPanel(panelResponse);
    return new EnvironmentSelectionPanelElement();
  }

  function mergeWithDefaultPanelResponse(
    partial: NestedPartial<EnvironmentSelectionPanelDto>,
  ): EnvironmentSelectionPanelDto {
    const defaultResponse: EnvironmentSelectionPanelDto = getDefaultPanelResponse();

    return {
      environments: { ...defaultResponse.environments, ...partial.environments },
      server: { ...defaultResponse.server, ...partial.server },
      project: { ...defaultResponse.project, ...partial.project },
      errorMessage: partial.errorMessage,
    };
  }

  // Each nested object is a partial of itself
  type NestedPartial<T> = {
    [key in keyof T]?: Partial<T[key]>;
  };
});

describe('Administration Workspace - Project - TAServer environment variables', function () {
  it('should show default values for environment variables linked to server', () => {
    const automationEnvironmentPanel =
      navigateToEnvironmentVariableProjectPanel(getDefaultPanelResponse());
    const environmentVariablePanel = automationEnvironmentPanel.environmentVariableSelectionPanel;
    environmentVariablePanel.boundEnvironmentVariablesGrid.assertExists();

    environmentVariablePanel.boundEnvironmentVariablesGrid
      .findRowId('name', 'Simple EV1')
      .then((rowId) =>
        environmentVariablePanel.boundEnvironmentVariablesGrid
          .getRow(rowId)
          .cell('value')
          .textRenderer()
          .assertContainsText('toto'),
      );

    environmentVariablePanel.boundEnvironmentVariablesGrid
      .findRowId('name', 'Dropdown EV1')
      .then((rowId) =>
        environmentVariablePanel.boundEnvironmentVariablesGrid
          .getRow(rowId)
          .cell('value')
          .textRenderer()
          .assertContainsText('OPT1'),
      );
  });

  it('should update value', () => {
    const automationEnvironmentPanel =
      navigateToEnvironmentVariableProjectPanel(getDefaultPanelResponse());
    const environmentVariablePanel = automationEnvironmentPanel.environmentVariableSelectionPanel;

    environmentVariablePanel.updateSimpleEnvironmentVariableValue(
      'new value',
      'Simple EV1',
      'value',
      'bound-environment-variables/value',
    );

    environmentVariablePanel.showEnvironmentVariableOptionsList(2, 'Dropdown EV1', 'value');
    environmentVariablePanel.selectUpdateEnvironmentVariableOption('OPT2');

    environmentVariablePanel.checkCellValue('new value', 'Simple EV1', 'value');
    environmentVariablePanel.checkCellValue('OPT2', 'Dropdown EV1', 'value');
  });

  it('should reset to server value', () => {
    const automationEnvironmentPanel =
      navigateToEnvironmentVariableProjectPanel(getDefaultPanelResponse());
    const environmentVariablePanel = automationEnvironmentPanel.environmentVariableSelectionPanel;

    environmentVariablePanel.resetValue(2, 'OPT2');
    environmentVariablePanel.checkCellValue('OPT2', 'Dropdown EV1', 'value');
    environmentVariablePanel.resetValue(1, 'default');
    environmentVariablePanel.checkCellValue('default', 'Simple EV1', 'value');
  });
});

describe('Administration Workspace - Project - Squash autom server workflows', function () {
  it('should set an execution server that display workflows grid', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectData(),
    );
    const automationPanel = projectViewPage.automationPanel;

    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    automationPanel.assertWorkflowsGridIsHidden();

    const envPanelMock = new HttpMockBuilder(
      'generic-projects/*/automated-execution-environments/all?*',
    )
      .responseBody(getDefaultPanelResponse())
      .build();

    const envVariableMock = new HttpMockBuilder('bound-environment-variables/project/*')
      .responseBody({ boundEnvironmentVariables: [] })
      .build();

    const workflowMock = new HttpMockBuilder('orchestrator-operation/*/project/*/workflows')
      .get()
      .responseBody({
        response: workflowResponse,
        reachable: true,
      })
      .build();

    automationPanel.executionServerField.setAndConfirmValueNoButton('taServer2');
    envPanelMock.wait();
    envVariableMock.wait();
    workflowMock.wait();

    automationPanel.assertWorkflowsGridIsVisible();

    const successWorkflow = workflowResponse[0];
    const runningWorkflow = workflowResponse[1];

    automationPanel.workflowsGrid.findRowId('id', successWorkflow.id).then((rowId) => {
      automationPanel.workflowsGrid
        .getRow(rowId)
        .cell('name')
        .textRenderer()
        .assertContainsText(successWorkflow.name);
      automationPanel.workflowsGrid.getRow(rowId).cell('status').textRenderer();
    });

    cy.get(selectByDataTestButtonId('stop-workflow-' + successWorkflow.id))
      .find('span')
      .should('not.have.class', '__hover_pointer');
    cy.get(selectByDataTestButtonId('stop-workflow-' + runningWorkflow.id))
      .find('span')
      .should('have.class', '__hover_pointer');
  });

  const workflowResponse: Workflow[] = [
    {
      id: '00a6934a-86ee-4638-aaab-e2dbdf2e10e3',
      name: 'Workflow1',
      namespace: 'default',
      createdOn: '2021-03-18T14:00:00.000+01:00',
      status: 'SUCCESS',
      completedOn: '2021-03-18T14:00:00.000+01:00',
    },
    {
      id: '00a8764a-86ee-4638-aaab-e2dbdf2t7ui2',
      name: 'Running workflow',
      namespace: 'default',
      createdOn: '2021-03-18T16:00:00.000+01:00',
      status: 'RUNNING',
      completedOn: '2021-03-18T16:00:00.000+01:00',
    },
  ];
});

function navigateToEnvironmentVariableProjectPanel(
  panelResponse: EnvironmentSelectionPanelDto,
): TestAutoServerEnvironmentVariableElement {
  initializeEnvironmentPanel(panelResponse);
  return new TestAutoServerEnvironmentVariableElement();
}

function initializeEnvironmentPanel(panelResponse: EnvironmentSelectionPanelDto) {
  const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);
  const projectViewPage = page.selectProjectByName(
    initialNodes.dataRows[0].data.name,
    getProjectData(),
  );

  new NavBarAdminElement().toggle();
  projectViewPage.foldGrid();

  projectViewPage.automationPanel.assertEnvironmentsBlockIsHidden();
  const envPanelMock = new HttpMockBuilder(
    'generic-projects/*/automated-execution-environments/all?*',
  )
    .responseBody(panelResponse)
    .build();

  const envVariableMock = new HttpMockBuilder('bound-environment-variables/project/*')
    .responseBody(getBoundEnvironmentVariables())
    .build();

  const workflowMock = new HttpMockBuilder('orchestrator-operation/*/project/*/workflows')
    .get()
    .responseBody({
      response: [],
      reachable: true,
    })
    .build();

  projectViewPage.automationPanel.executionServerField.setAndConfirmValueNoButton('taServer2');
  envPanelMock.wait();
  envVariableMock.wait();
  workflowMock.wait();
}

function getDefaultPanelResponse(): EnvironmentSelectionPanelDto {
  return {
    project: {
      areProjectTagsInherited: false,
      hasProjectToken: false,
      projectId: 0,
      projectTags: [],
    },
    server: { defaultTags: [], hasServerCredentials: false, testAutomationServerId: 0 },
    environments: { environments: [] },
    errorMessage: null,
  };
}

function getProjectData(taServerId?: number): ProjectView {
  return makeProjectViewData({
    scmRepositoryId: taServerId ? 1 : null,
    automationWorkflowType: 'NONE',
    taServerId,
    availableScmServers: [
      {
        serverId: -1,
        name: 'server1',
        repositories: [
          {
            scmRepositoryId: 1,
            serverId: 1,
            name: 'repo1',
          },
        ],
      } as ScmServer,
      {
        serverId: -2,
        name: 'server2',
        repositories: [
          {
            scmRepositoryId: 2,
            serverId: 2,
            name: 'repo1',
          },
        ],
      } as ScmServer,
    ],
    availableTestAutomationServers: [
      { id: -1, name: 'taServer1', baseUrl: 'https://url1' } as TestAutomationServer,
      {
        id: -2,
        name: 'taServer2',
        baseUrl: 'https://url2',
        supportsAutomatedExecutionEnvironments: true,
      } as TestAutomationServer,
    ],
    boundMilestonesInformation: [],
    boundTestAutomationProjects: [
      {
        taProjectId: -1,
        label: 'job1',
        remoteName: 'rem1',
        canRunBdd: false,
        executionEnvironments: '',
        serverId: -1,
        tmProjectId: 4,
      },
      {
        taProjectId: -2,
        label: 'job2',
        remoteName: 'rem2',
        canRunBdd: false,
        executionEnvironments: '',
        serverId: -1,
        tmProjectId: 4,
      },
    ],
    bddScriptLanguage: 'ENGLISH',
    bddImplementationTechnology: 'CUCUMBER_4',
    availableProfiles: [
      {
        id: 1,
        qualifiedName: AclGroup.PROJECT_MANAGER,
        active: true,
        system: true,
      },
    ],
  });
}

const todayDate = new Date();

const initialNodes: GridResponse = {
  count: 2,
  dataRows: [
    mockDataRow({
      id: '1',
      children: [],
      data: {
        projectId: 1,
        name: 'Project1',
        label: 'label',
        isTemplate: false,
        createdOn: todayDate,
        createdBy: 'JP01',
        lastModifiedOn: todayDate,
        lastModifiedBy: 'JP01',
        hasPermissions: true,
        bugtrackerName: 'BT JIRA',
        executionServer: 'TA server',
      },
    }),
    mockDataRow({
      id: '2',
      children: [],
      data: {
        projectId: 6,
        name: 'ProjectTemplate',
        label: 'label',
        isTemplate: true,
        createdOn: todayDate,
        createdBy: 'JP01',
        lastModifiedOn: todayDate,
        lastModifiedBy: 'JP01',
        hasPermissions: true,
        bugtrackerName: 'BT JIRA',
        executionServer: 'TA server',
      },
    }),
  ],
};
