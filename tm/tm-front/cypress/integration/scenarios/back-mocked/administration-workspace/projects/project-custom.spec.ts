import { CustomFieldsResponse } from '../../../../page-objects/pages/administration-workspace/project-view/dialogs/bind-custom-field-to-project.dialog';
import { AdminWorkspaceProjectsPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-projects.page';
import { NavBarAdminElement } from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import { ProjectIsBoundToATemplateAlert } from '../../../../page-objects/pages/administration-workspace/dialogs/project-is-bound-to-a-template-alert.element';
import { makeProjectViewData } from '../../../../data-mock/administration-views.data-mock';
import { ConfirmInfoListBindingAlert } from '../../../../page-objects/pages/administration-workspace/project-view/dialogs/confirm-info-list-binding.alert';
import { AdminReferentialDataMockBuilder } from '../../../../utils/referential/admin-referential-data-builder';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { CustomField } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/customfield.model';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { ProjectView } from '../../../../../../projects/sqtm-core/src/lib/model/project/project.model';
import { WorkspaceTypeForPlugins } from '../../../../../../projects/sqtm-core/src/lib/model/project/project-data.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { mockDataRow } from '../../../../data-mock/grid.data-mock';

describe('Administration Workspace - Projects - Custom Fields', function () {
  const todayDate = new Date();

  const initialNodes: GridResponse = {
    count: 1,
    dataRows: [
      mockDataRow({
        id: '1',
        children: [],
        data: {
          projectId: 1,
          name: 'Project1',
          label: 'label',
          isTemplate: false,
          createdOn: todayDate,
          createdBy: 'JP01',
          lastModifiedOn: todayDate,
          lastModifiedBy: 'JP01',
          hasPermissions: true,
          bugtrackerName: 'BT JIRA',
          executionServer: 'TA server',
        },
      }),
    ],
  };

  const customFields: CustomFieldsResponse = {
    customFields: [
      {
        id: 3,
        code: 'Date3',
        inputType: InputType.DATE_PICKER,
        name: 'Date3',
        label: 'Date3',
        defaultValue: '',
        largeDefaultValue: null,
        numericDefaultValue: null,
        optional: true,
        options: [],
        boundProjectsToCuf: [],
      },
      {
        id: 4,
        code: 'Date4',
        inputType: InputType.DATE_PICKER,
        name: 'Date4',
        label: 'Date4',
        defaultValue: '',
        largeDefaultValue: null,
        numericDefaultValue: null,
        optional: true,
        options: [],
        boundProjectsToCuf: [],
      },
    ],
  };

  function mockDateCuf(id: number): CustomField {
    return {
      id,
      code: 'Date' + id,
      inputType: InputType.DATE_PICKER,
      name: 'Date' + id,
      label: 'Date' + id,
      defaultValue: '',
      largeDefaultValue: null,
      numericDefaultValue: null,
      optional: true,
      options: [],
      boundProjectsToCuf: [],
    };
  }

  const baseRefData = new AdminReferentialDataMockBuilder()
    .withCustomFields([mockDateCuf(1), mockDateCuf(2)])
    .build();

  it('should show custom fields count', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, baseRefData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(false),
    );
    projectViewPage.assertCustomFieldCount(2);
  });

  it('should bind a custom field to project', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, baseRefData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(false),
    );
    projectViewPage.assertExists();
    projectViewPage.showCustomFieldsPanel();
    projectViewPage.customFieldsPanel.grid.assertExists();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    const bindCustomFieldToProjectDialog =
      projectViewPage.customFieldsPanel.openCustomFieldBindingDialog(customFields);
    bindCustomFieldToProjectDialog.selectEntity('Exigences');
    bindCustomFieldToProjectDialog.selectCuf('Date3', 'Date4');

    bindCustomFieldToProjectDialog.confirm({
      CAMPAIGN_FOLDER: [],
      TESTCASE_FOLDER: [],
      REQUIREMENT_VERSION: [],
      REQUIREMENT_FOLDER: [
        {
          id: 1,
          customFieldId: 1,
          bindableEntity: BindableEntity.REQUIREMENT_FOLDER,
          renderingLocations: [],
          boundProjectId: 4,
          position: 1,
        },
        {
          id: 2,
          customFieldId: 2,
          bindableEntity: BindableEntity.REQUIREMENT_FOLDER,
          renderingLocations: [],
          boundProjectId: 4,
          position: 2,
        },
        {
          id: 3,
          customFieldId: 3,
          bindableEntity: BindableEntity.REQUIREMENT_FOLDER,
          renderingLocations: [],
          boundProjectId: 4,
          position: 3,
        },
        {
          id: 4,
          customFieldId: 4,
          bindableEntity: BindableEntity.REQUIREMENT_FOLDER,
          renderingLocations: [],
          boundProjectId: 4,
          position: 4,
        },
      ],
      TEST_SUITE: [],
      ITERATION: [],
      TEST_CASE: [],
      TEST_STEP: [],
      CAMPAIGN: [],
      EXECUTION: [],
      CUSTOM_REPORT_FOLDER: [],
      EXECUTION_STEP: [],
      SPRINT: [],
      SPRINT_GROUP: [],
    });
  });

  it('should forbid to bind a custom field to a project linked to a template', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, baseRefData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(true),
    );
    projectViewPage.assertExists();
    projectViewPage.showCustomFieldsPanel();
    projectViewPage.customFieldsPanel.grid.assertExists();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.customFieldsPanel.clickOnAddCustomFieldBindingButton();
    const alert = new ProjectIsBoundToATemplateAlert();
    alert.assertMessage();
    alert.close();
  });

  it('should unbind custom field from project', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, baseRefData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(false),
    );

    projectViewPage.assertExists();
    projectViewPage.showCustomFieldsPanel();
    projectViewPage.customFieldsPanel.grid.assertExists();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.customFieldsPanel.unbindOne('Date1', false);
  });

  it('should unbind multiple custom fields from project', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, baseRefData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(false),
    );

    projectViewPage.assertExists();
    projectViewPage.showCustomFieldsPanel();
    projectViewPage.customFieldsPanel.grid.assertExists();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.customFieldsPanel.unbindMultiple(['Date1', 'Date2'], false);
  });

  it('should forbid to unbind custom field from a project linked to a template', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, baseRefData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(true),
    );

    projectViewPage.assertExists();
    projectViewPage.showCustomFieldsPanel();
    projectViewPage.customFieldsPanel.grid.assertExists();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.customFieldsPanel.unbindOne('Date1', true);
    const alert = new ProjectIsBoundToATemplateAlert();
    alert.assertMessage();
    alert.close();
  });

  it('should forbid to unbind multiple custom fields from a project linked to a template', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, baseRefData);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(true),
    );

    projectViewPage.assertExists();
    projectViewPage.showCustomFieldsPanel();
    projectViewPage.customFieldsPanel.grid.assertExists();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.customFieldsPanel.unbindMultiple(['Date1', 'Date2'], true);
    const alert = new ProjectIsBoundToATemplateAlert();
    alert.assertMessage();
    alert.close();
  });

  function getProjectDto(linkedToTemplate: boolean): ProjectView {
    return makeProjectViewData({
      availableBugtrackers: [],
      bugtrackerProjectNames: [],
      id: 4,
      name: 'Project1',
      label: null,
      testCaseNatureId: 2,
      testCaseTypeId: 3,
      requirementCategoryId: 1,
      allowAutomationWorkflow: true,
      customFieldBindings: {
        CAMPAIGN_FOLDER: [],
        TESTCASE_FOLDER: [],
        REQUIREMENT_VERSION: [],
        REQUIREMENT_FOLDER: [
          {
            id: 1,
            customFieldId: 1,
            bindableEntity: BindableEntity.REQUIREMENT_FOLDER,
            renderingLocations: [],
            boundProjectId: 4,
            position: 1,
          },
          {
            id: 2,
            customFieldId: 2,
            bindableEntity: BindableEntity.REQUIREMENT_FOLDER,
            renderingLocations: [],
            boundProjectId: 4,
            position: 1,
          },
        ],
        TEST_SUITE: [],
        ITERATION: [],
        TEST_CASE: [],
        TEST_STEP: [],
        CAMPAIGN: [],
        EXECUTION: [],
        CUSTOM_REPORT_FOLDER: [],
        EXECUTION_STEP: [],
        SPRINT: [],
        SPRINT_GROUP: [],
      },
      permissions: null,
      bugTrackerBinding: null,
      milestoneBindings: [],
      taServerId: null,
      automationWorkflowType: 'NATIVE',
      disabledExecutionStatus: [],
      attachmentList: {
        id: 519,
        attachments: [
          {
            id: 7,
            name: 'demo_sprint_7.pdf',
            size: 152088,
            addedOn: new Date(),
            lastModifiedOn: null,
          },
        ],
      },
      linkedTemplateId: linkedToTemplate ? 2 : null,
      linkedTemplate: null,
      createdOn: new Date().toISOString(),
      createdBy: 'admin',
      lastModifiedOn: new Date().toISOString(),
      lastModifiedBy: 'admin',
      allowTcModifDuringExec: false,
      allowedStatuses: { SETTLED: false, UNTESTABLE: false },
      statusesInUse: { SETTLED: false, UNTESTABLE: false },
      description: '',
      uri: '',
      useTreeStructureInScmRepo: false,
      infoLists: [
        {
          id: 1,
          uri: null,
          code: 'DEF_REQ_CAT',
          label: 'infolist.category.default',
          description: '',
          items: [],
        },
        {
          id: 2,
          uri: null,
          code: 'DEF_TC_NAT',
          label: 'infolist.nature.default',
          description: '',
          items: [],
        },
        {
          id: 3,
          uri: null,
          code: 'DEF_TC_TYP',
          label: 'infolist.type.default',
          description: '',
          items: [],
        },
        {
          id: 4,
          uri: null,
          code: 'Test1',
          label: 'Test 1 list',
          description: '',
          items: [],
        },
        {
          id: 5,
          uri: null,
          code: 'Test2',
          label: 'Test 2 list',
          description: '',
          items: [],
        },
      ],
      template: false,
      templateLinkedToProjects: false,
      partyProjectPermissions: [],
      availableScmServers: [],
      availableTestAutomationServers: [],
      boundTestAutomationProjects: [],
      boundMilestonesInformation: [],
      activatedPlugins: {
        [WorkspaceTypeForPlugins.CAMPAIGN_WORKSPACE]: [],
        [WorkspaceTypeForPlugins.TEST_CASE_WORKSPACE]: [],
        [WorkspaceTypeForPlugins.REQUIREMENT_WORKSPACE]: [],
      },
    });
  }
});

describe('Administration Workspace - Projects - Info Lists', function () {
  const todayDate = new Date();
  const initialNodes: GridResponse = {
    count: 1,
    dataRows: [
      mockDataRow({
        id: '1',
        children: [],
        data: {
          projectId: 1,
          name: 'Project1',
          label: 'label',
          isTemplate: false,
          createdOn: todayDate,
          createdBy: 'JP01',
          lastModifiedOn: todayDate,
          lastModifiedBy: 'JP01',
          hasPermissions: true,
          bugtrackerName: 'BT JIRA',
          executionServer: 'TA server',
        },
      }),
    ],
  };

  it('should bind a category info list to a project', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(false),
    );
    projectViewPage.assertExists();
    projectViewPage.showCustomFieldsPanel();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();
    projectViewPage.infoListsPanel.selectCategoryList('Test 2 list');
    const confirmDialog = new ConfirmInfoListBindingAlert();
    confirmDialog.assertMessage();
    confirmDialog.confirmAttributeModification('category');
  });

  it('should bind a nature info list to a project', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(false),
    );
    projectViewPage.assertExists();
    projectViewPage.showCustomFieldsPanel();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();
    projectViewPage.infoListsPanel.selectNatureList('Test 1 list');
    const confirmDialog = new ConfirmInfoListBindingAlert();
    confirmDialog.assertMessage();
    confirmDialog.confirmAttributeModification('nature');
  });

  it('should bind a type info list to a project', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(false),
    );
    projectViewPage.assertExists();
    projectViewPage.showCustomFieldsPanel();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();
    projectViewPage.infoListsPanel.selectTypeList('Test 1 list');
    const confirmDialog = new ConfirmInfoListBindingAlert();
    confirmDialog.assertMessage();
    confirmDialog.confirmAttributeModification('type');
  });

  it('should forbid to bind a info list to a project linked to a template', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto(true),
    );
    projectViewPage.assertExists();
    projectViewPage.showCustomFieldsPanel();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.infoListsPanel.clickOnDesiredSelectField('info-list-category-select-field');
    const alert = new ProjectIsBoundToATemplateAlert();
    alert.assertMessage();
    alert.close();
  });

  function getProjectDto(linkedToTemplate: boolean): ProjectView {
    return makeProjectViewData({
      linkedTemplateId: linkedToTemplate ? 2 : null,
      infoLists: [
        {
          id: 1,
          uri: null,
          code: 'DEF_REQ_CAT',
          label: 'infolist.category.default',
          description: '',
          items: [],
        },
        {
          id: 2,
          uri: null,
          code: 'DEF_TC_NAT',
          label: 'infolist.nature.default',
          description: '',
          items: [],
        },
        {
          id: 3,
          uri: null,
          code: 'DEF_TC_TYP',
          label: 'infolist.type.default',
          description: '',
          items: [],
        },
        {
          id: 4,
          uri: null,
          code: 'Test1',
          label: 'Test 1 list',
          description: '',
          items: [],
        },
        {
          id: 5,
          uri: null,
          code: 'Test2',
          label: 'Test 2 list',
          description: '',
          items: [],
        },
      ],
      template: false,
      templateLinkedToProjects: false,
      partyProjectPermissions: [],
      availableScmServers: [],
      availableTestAutomationServers: [],
      boundTestAutomationProjects: [],
      boundMilestonesInformation: [],
    });
  }
});
