import { EditableDateFieldElement } from '../../../../page-objects/elements/forms/editable-date-field.element';
import {
  NO_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder,
} from '../../../../utils/referential/referential-data-builder';
import { ReferentialDataProviderBuilder } from '../../../../utils/referential/referential-data.provider';
import { AdminWorkspaceProjectsPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-projects.page';
import { HttpMock, HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { RemoveProjectDialog } from '../../../../page-objects/pages/administration-workspace/dialogs/remove-project-dialog.element';
import { CannotRemoveProjectAlert } from '../../../../page-objects/pages/administration-workspace/dialogs/cannot-remove-project-alert.element';
import { AdminReferentialDataMockBuilder } from '../../../../utils/referential/admin-referential-data-builder';
import { AdminReferentialDataProviderBuilder } from '../../../../utils/referential/admin-referential-data.provider';
import { assertAccessDenied } from '../../../../utils/assert-access-denied.utils';
import { makeProjectViewData } from '../../../../data-mock/administration-views.data-mock';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import { ProjectViewPage } from '../../../../page-objects/pages/administration-workspace/project-view/project-view.page';
import { Permissions } from '../../../../../../projects/sqtm-core/src/lib/model/permissions/permissions.model';
import {
  Project,
  ProjectView,
} from '../../../../../../projects/sqtm-core/src/lib/model/project/project.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { AdminReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/admin-referential-data.model';
import { CreateAdministrationEntityDialog } from '../../../../page-objects/pages/administration-workspace/create-administration-entity-dialog';
import { mockDataRow } from '../../../../data-mock/grid.data-mock';

describe('Administration Workspace - Projects', function () {
  const todayDate = new Date();

  const initialNodes: GridResponse = {
    count: 2,
    dataRows: [
      mockDataRow({
        id: '1',
        children: [],
        data: {
          projectId: 1,
          name: 'Project1',
          label: 'label',
          isTemplate: false,
          createdOn: todayDate,
          createdBy: 'JP01',
          lastModifiedOn: todayDate,
          lastModifiedBy: 'JP01',
          hasPermissions: true,
          bugtrackerName: 'BT JIRA',
          executionServer: 'TA server',
        },
      }),
      mockDataRow({
        id: '2',
        children: [],
        data: {
          projectId: 2,
          name: 'Template1',
          label: 'label',
          isTemplate: true,
          createdOn: todayDate,
          createdBy: 'admin',
          lastModifiedOn: todayDate,
          lastModifiedBy: 'JP01',
          hasPermissions: true,
          bugtrackerName: 'BT JIRA',
          executionServer: 'TA server',
        },
      }),
    ],
  };

  const addProjectResponse: GridResponse = {
    count: 3,
    dataRows: [
      ...initialNodes.dataRows,
      mockDataRow({
        children: [],
        id: '4',
        data: {
          name: 'Project 45',
          projectId: 4,
        },
      }),
    ],
  };

  const addTemplateResponse: GridResponse = {
    count: 2,
    dataRows: [
      ...initialNodes.dataRows,
      mockDataRow({
        id: '4',
        children: [],
        data: {
          projectId: 4,
          name: 'Template',
          isTemplate: true,
        },
      }),
    ],
  };

  it('should forbid access to non-admin users', () => {
    assertAccessDenied('administration-workspace/projects');
  });

  it('should display managed projects for a project manager', () => {
    const refData = new ReferentialDataMockBuilder()
      .withUser({
        admin: false,
        username: 'gerard',
        userId: 99,
        hasAnyReadPermission: true,
        projectManager: true,
        milestoneManager: true,
        clearanceManager: true,
        functionalTester: true,
        automationProgrammer: true,
      })
      .withProjects(
        {
          name: 'project with permissions',
          permissions: { ...NO_PROJECT_PERMISSIONS, PROJECT: [Permissions.MANAGE_PROJECT] },
        },
        {
          name: 'project without permissions',
          permissions: NO_PROJECT_PERMISSIONS,
        },
      )
      .build();

    const response = getProjectsAsGridResponse(refData.projects[0]);
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(response, refData);
    page.grid.assertRowExist(1);
  });

  it('should forbid access for project manager without managed project', () => {
    const adminRefData = new AdminReferentialDataMockBuilder()
      .withUser({
        admin: false,
        username: 'gerard',
        userId: 99,
        hasAnyReadPermission: false,
        projectManager: false,
        milestoneManager: false,
        clearanceManager: false,
        functionalTester: false,
        automationProgrammer: false,
        firstName: '',
        lastName: '',
        canDeleteFromFront: false,
      })
      .build();

    const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder(
      adminRefData,
    ).build();

    const refData = new ReferentialDataMockBuilder()
      .withUser({
        admin: false,
        username: 'gerard',
        userId: 99,
        hasAnyReadPermission: true,
        projectManager: true,
        milestoneManager: true,
        clearanceManager: true,
        functionalTester: true,
        automationProgrammer: true,
      })
      .withProjects(
        {
          name: 'project without permissions',
          permissions: NO_PROJECT_PERMISSIONS,
        },
        {
          name: 'project without permissions2',
          permissions: NO_PROJECT_PERMISSIONS,
        },
      )
      .build();

    const referentialDataProvider = new ReferentialDataProviderBuilder(refData).build();
    const homeWorkspaceMock = new HttpMockBuilder('/home-workspace').build();
    cy.visit(`administration-workspace/projects`);
    adminReferentialDataProvider.wait();
    referentialDataProvider.wait();
    homeWorkspaceMock.wait();
    cy.location('pathname').should('contain', '/home-workspace');
  });

  it('should display projects grid', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const grid = page.grid;

    grid.assertRowExist(1);
    const row = grid.getRow(1);
    row
      .cell('createdOn')
      .textRenderer()
      .assertContainsText(EditableDateFieldElement.dateToDisplayString(todayDate));
    row.cell('createdBy').textRenderer().assertContainsText('JP01');
    grid.scrollPosition('right', 'mainViewport');
    row
      .cell('lastModifiedOn')
      .textRenderer()
      .assertContainsText(EditableDateFieldElement.dateToDisplayString(todayDate));
    row.cell('lastModifiedBy').textRenderer().assertContainsText('JP01');
    row.cell('hasPermissions').textRenderer().assertContainsText('Oui');
    row.cell('bugtrackerName').textRenderer().assertContainsText('BT JIRA');
    row.cell('executionServer').textRenderer().assertContainsText('TA server');

    grid.assertRowExist(2);
    grid
      .getRow(2)
      .cell('isTemplate')
      .iconRenderer()
      .assertContainsIcon('anticon-sqtm-core-administration:template');
  });

  it('should add project', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const genericProject = getGenericProject(
      4,
      'Project 45',
      'A description',
      'My label',
      1,
      '',
      false,
    );

    const dialog = page.openCreateProject();
    dialog.assertExists();
    dialog.fillName('Project 45');
    dialog.fillDescription('A description');
    dialog.fillLabel('My label');

    addProjectAndMock(genericProject, dialog, addProjectResponse);

    const view = new ProjectViewPage();
    view.assertExists();

    const grid = page.grid;

    grid.assertRowExist(1);
    grid.assertRowExist(4);
  });

  it('should add another project', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const genericProject = getGenericProject(
      4,
      'Project 45',
      'A description',
      'My label',
      1,
      '',
      false,
    );
    const dialog = page.openCreateProject();
    dialog.assertExists();
    dialog.fillName('Project 45');
    dialog.fillDescription('A description');
    dialog.fillLabel('My label');

    addProjectAndMock(genericProject, dialog, addProjectResponse, true);

    dialog.checkIfFormIsEmpty();
    dialog.cancel();
    dialog.assertNotExist();

    const view = new ProjectViewPage();
    view.assertExists();

    page.grid.assertRowExist(4);
  });

  it('should add project from a template', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);

    const genericProject = getGenericProject(
      4,
      'Project 45',
      'A description',
      'My label',
      1,
      'Template1',
      false,
    );

    const dialog = page.openCreateProject([
      {
        id: 2,
        name: 'Template1',
      },
    ]);

    dialog.assertExists();
    dialog.fillName('Project 45');
    dialog.fillDescription('A description');
    dialog.fillLabel('My label');
    dialog.selectTemplate('Template1');
    // if no license ultimate, AI server feature is not displayed
    dialog.assertCopyAiServerEntryNotExist();

    addProjectAndMock(genericProject, dialog, addProjectResponse);

    const view = new ProjectViewPage();
    view.assertExists();

    const grid = page.grid;

    grid.assertRowExist(1);
    grid.assertRowExist(4);
  });

  it('should display entry AI server in template parameters if license ultimate is available', () => {
    const refDataWithUltimate = new AdminReferentialDataMockBuilder()
      .withUltimateLicenseAvailable(true)
      .build();

    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(
      initialNodes,
      refDataWithUltimate,
    );
    const dialog = page.openCreateProject([
      {
        id: 2,
        name: 'Template1',
      },
    ]);

    dialog.selectTemplate('Template1');
    dialog.assertCopyAiServerEntryExists();
  });

  it('should forbid to add a project with an empty name', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const dialog = page.openCreateProject();
    dialog.assertExists();
    dialog.fillDescription('A description');
    dialog.fillLabel('My label');

    dialog.clickOnAddButton();

    dialog.checkIfRequiredErrorMessageIsDisplayed();
  });

  it('should forbid to add a project if name already exists', () => {
    const httpError = {
      squashTMError: {
        kind: 'FIELD_VALIDATION_ERROR',
        fieldValidationErrors: [
          {
            fieldName: 'name',
            i18nKey: 'sqtm-core.error.generic.name-already-in-use',
          },
        ],
      },
    };
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const dialog = page.openCreateProject();
    dialog.assertExists();
    dialog.fillName('Project 45');
    dialog.fillDescription('A description');
    dialog.fillLabel('My label');

    dialog.addWithServerSideFailure(httpError);
    dialog.assertExists();
    dialog.checkIfNameAlreadyInUseErrorMessageIsDisplayed();
  });

  it('should add template', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);

    const genericProject = getGenericProject(
      4,
      'Project 45',
      'A description',
      'My label',
      1,
      'Template1',
      true,
    );

    const dialog = page.openCreateTemplate();
    dialog.assertExists();
    dialog.fillName('Project 45');
    dialog.fillDescription('A description');
    dialog.fillLabel('My label');

    addProjectAndMock(genericProject, dialog, addTemplateResponse);
    const view = new ProjectViewPage();
    view.assertExists();

    const grid = page.grid;

    grid.assertRowExist(1);
    grid.assertRowExist(4);
  });

  it('should add template from project', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);

    const viewMock: HttpMock<any> = new HttpMockBuilder('project-view/*').build();
    page.grid.selectRow(1, '#', 'leftViewport');
    viewMock.wait();

    const dialog = page.openCreateTemplateFromProject();
    dialog.assertExists();
    dialog.fillName('Project 45');
    dialog.fillDescription('A description');
    dialog.fillLabel('My label');

    dialog.addWithOptions({
      addAnother: false,
      createResponse: { id: 4 },
      gridResponse: addTemplateResponse,
    });

    const grid = page.grid;

    grid.assertRowExist(1);
    grid.assertRowExist(4);
  });

  it('should delete a project', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectView = page.selectProjectByName('Project1', makeProjectViewData());

    projectView.clickOnDeleteProjectMenuItem({
      hasData: false,
      i18nMessageKey: 'sqtm-core.administration-workspace.projects.dialog.message.delete-project',
    });

    const removeProjectDialog = new RemoveProjectDialog(4);
    removeProjectDialog.deleteForSuccess({
      dataRows: [],
      count: 0,
    });

    page.grid.assertRowCount(0);
  });

  it('should forbid delete a project with data', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectDto = makeProjectViewData();

    const projectView = page.selectProjectByName('Project1', projectDto);

    projectView.clickOnDeleteProjectMenuItem({
      hasData: true,
      i18nMessageKey:
        'sqtm-core.administration-workspace.projects.dialog.message.cannot-delete-project-with-data',
    });

    const alert = new CannotRemoveProjectAlert();
    alert.assertExists();
    alert.clickOnCloseButton();

    page.grid.assertRowCount(2);
  });

  it('should rename a project', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectView = page.selectProjectByName('Project1', makeProjectViewData());

    projectView.nameTextField.setAndConfirmValue('New name');
  });

  it('should associate a project to a template', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewData = {
      ...makeProjectViewData(),
      attachmentList: { id: 514, attachments: [] },
    };
    const projectView = page.selectProjectByName('Project1', projectViewData);
    projectView.checkAssociatedTemplate('Template1');
    projectView.disassociateProjectFromTemplate();
    projectView.checkAssociatedTemplate('Aucun');

    const dialog = projectView.openAssociateToTemplateDialog(['Template1', 'Template2']);
    dialog.selectTemplateAndConfirm('Template1');
    projectView.checkAssociatedTemplate('Template1');
  });

  it('should associate a project to a template with plugin binding', () => {
    const referentialData: AdminReferentialDataModel = new AdminReferentialDataMockBuilder()
      .withTemplateConfigurablePlugins([
        {
          id: 'my-plugin',
          name: 'My Plugin',
          type: 'my-plugin-type',
        },
        {
          id: 'my-plugin2',
          name: 'My Plugin2',
          type: 'my-plugin-type',
        },
      ])
      .build();

    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, referentialData);
    const projectView = page.selectProjectByName('Project1', makeProjectViewData());
    projectView.checkAssociatedTemplate('Template1');
    projectView.disassociateProjectFromTemplate();
    projectView.checkAssociatedTemplate('Aucun');

    const dialog = projectView.openAssociateToTemplateDialog(['Template1', 'Template2']);
    dialog.assertBindTemplatePluginConfigurationCheckboxExists('my-plugin');
    dialog.assertBindTemplatePluginConfigurationCheckboxExists('my-plugin2');
  });

  it('should transform a project into a template', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectView = page.selectProjectByName('Project1', makeProjectViewData());
    projectView.checkAssociatedTemplate('Template1');
    projectView.transformProjectIntoTemplate();
    projectView.checkNoAssociatedTemplate();
    projectView.checkCannotTransformIntoTemplate();
  });

  it('should search on multi columns', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const grid = page.grid;
    grid.assertRowCount(2);
    page.fillSearchInput('Temp', {
      count: 1,
      dataRows: [
        mockDataRow({
          id: '2',
          children: [],
          data: {
            projectId: 2,
            name: 'Template1',
            label: 'label',
            isTemplate: true,
            createdOn: todayDate,
            createdBy: 'admin',
            lastModifiedOn: todayDate,
            lastModifiedBy: 'JP01',
            hasPermissions: true,
            bugtrackerName: 'BT JIRA',
            executionServer: 'TA server',
          },
        }),
      ],
    });
    grid.assertRowCount(1);
  });

  it('should add project and bind/copy template plugin configuration', () => {
    const referentialData: AdminReferentialDataModel = new AdminReferentialDataMockBuilder()
      .withTemplateConfigurablePlugins([
        {
          id: 'my-plugin',
          name: 'My Plugin',
          type: 'my-plugin-type',
        },
      ])
      .build();

    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, referentialData);

    const genericProject = getGenericProject(
      4,
      'Project 45',
      'A description',
      'My label',
      2,
      'Template',
      true,
    );

    const dialog = page.openCreateProject([
      {
        id: 2,
        name: 'Template',
      },
    ]);
    dialog.assertExists();
    dialog.fillName('Project 45');
    dialog.fillDescription('A description');
    dialog.fillLabel('My label');
    dialog.selectTemplate('Template');

    dialog.assertCopyTemplatePluginConfigurationCheckboxIsDisabled();
    dialog.getKeepTemplatePluginConfigurationBindingCheckbox().click();
    dialog.assertCopyTemplatePluginConfigurationCheckboxIsEnabled();

    dialog.getCopyTemplatePluginConfigurationCheckbox().click();
    dialog.getKeepTemplatePluginConfigurationBindingCheckbox().click();
    dialog.assertCopyTemplatePluginConfigurationCheckboxIsDisabled();
    dialog.assertCopyTemplatePluginConfigurationCheckboxIsChecked();
    addProjectAndMock(genericProject, dialog, addProjectResponse);

    const view = new ProjectViewPage();
    view.assertExists();

    const grid = page.grid;

    grid.assertRowExist(1);
    grid.assertRowExist(4);
  });

  it('should navigate to a contextual view with a given anchor group', () => {
    const { view, workspace } = visitContextualViewWithURL(
      initialNodes,
      makeProjectViewData({ id: 1 }),
    );
    view.assertExists();
    workspace.grid.getRow('1').assertIsSelected();
    view.pluginPanel.assertExists();
  });

  function visitContextualViewWithURL(
    gridResponse: GridResponse,
    viewResponse: any,
  ): { workspace: AdminWorkspaceProjectsPage; view: ProjectViewPage } {
    const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder().build();
    const gridElement = GridElement.createGridElement('projects', 'generic-projects', gridResponse);
    const view = new ProjectViewPage();
    const viewMock = new HttpMockBuilder('project-view/*').responseBody(viewResponse).build();

    // visit page
    cy.visit(`administration-workspace/projects/${viewResponse.id}/plugins`);
    adminReferentialDataProvider.wait();
    gridElement.waitInitialDataFetch();
    viewMock.waitResponseBody().then(() => {
      view.waitInitialDataFetch();
    });

    return {
      workspace: new AdminWorkspaceProjectsPage(gridElement),
      view,
    };
  }

  function getProjectsAsGridResponse(...projects: Project[]): GridResponse {
    return {
      count: projects.length,
      dataRows: projects.map((project) =>
        mockDataRow({
          id: project.id.toString(),
          children: [],
          data: {
            name: project.name,
            label: project.label,
            isTemplate: false,
            createdOn: todayDate,
            createdBy: 'admin',
            lastModifiedOn: todayDate,
            lastModifiedBy: 'admin',
            hasPermissions: true,
            bugtrackerName: 'BT JIRA',
            executionServer: 'TA server',
          },
        }),
      ),
    };
  }

  function addProjectAndMock(
    genericProject: ProjectView,
    dialog: CreateAdministrationEntityDialog,
    response: GridResponse,
    addAnother?: boolean,
  ) {
    const mock = new HttpMockBuilder<any>('project-view/4?frontEndErrorIsHandled=true')
      .get()
      .responseBody(genericProject)
      .build();

    const statusRequestMock = new HttpMockBuilder('project-view/*/statuses-in-use')
      .get()
      .responseBody(genericProject.statusesInUse)
      .build();

    const environmentVariableMock = new HttpMockBuilder('bound-environment-variables/project/*')
      .get()
      .responseBody({ boundEnvironmentVariables: [] })
      .build();

    dialog.addWithOptions({
      addAnother: addAnother ? addAnother : false,
      createResponse: { id: 4 },
      gridResponse: response,
    });

    mock.wait();
    statusRequestMock.wait();
    environmentVariableMock.wait();
  }

  function getGenericProject(
    id: number,
    name: string,
    description: string,
    label: string,
    templateId: number,
    template: string,
    isTemplate: boolean,
  ): ProjectView {
    return {
      id: id,
      uri: '',
      name: name,
      label: label,
      testCaseNatureId: null,
      testCaseTypeId: null,
      requirementCategoryId: null,
      allowAutomationWorkflow: false,
      customFieldBindings: null,
      permissions: null,
      bugTrackerBinding: null,
      milestoneBindings: null,
      taServerId: null,
      automationWorkflowType: 'NONE',
      disabledExecutionStatus: [],
      description: description,
      keywords: [],
      bddScriptLanguage: 'ENGLISH',
      bddImplementationTechnology: 'CUCUMBER_5_PLUS',
      allowTcModifDuringExec: false,
      activatedPlugins: {
        TEST_CASE_WORKSPACE: [],
        CAMPAIGN_WORKSPACE: [],
        REQUIREMENT_WORKSPACE: [],
      },
      automatedSuitesLifetime: null,
      availableBugtrackers: [],
      bugtrackerProjectNames: null,
      allowedStatuses: { SETTLED: false, UNTESTABLE: true },
      statusesInUse: { SETTLED: false, UNTESTABLE: false },
      useTreeStructureInScmRepo: true,
      scmRepositoryId: null,
      infoLists: [],
      templateLinkedToProjects: false,
      partyProjectPermissions: [],
      availableScmServers: [],
      availableTestAutomationServers: [],
      availableAiServers: [],
      aiServerId: null,
      boundMilestonesInformation: [],
      boundTestAutomationProjects: [],
      attachmentList: { id: 7, attachments: [] },
      createdOn: new Date().toISOString(),
      createdBy: 'admin',
      lastModifiedOn: null,
      lastModifiedBy: null,
      linkedTemplate: template,
      linkedTemplateId: templateId,
      template: isTemplate,
      availablePlugins: [],
      allProjectBoundToTemplate: [],
      hasData: false,
      hasTemplateConfigurablePluginBinding: false,
      availableProfiles: [],
      existingImports: [],
    };
  }
});
