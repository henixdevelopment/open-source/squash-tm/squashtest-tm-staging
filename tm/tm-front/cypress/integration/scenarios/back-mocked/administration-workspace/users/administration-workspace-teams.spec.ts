import { EditableDateFieldElement } from '../../../../page-objects/elements/forms/editable-date-field.element';
import { RemoveTeamDialogElement } from '../../../../page-objects/pages/administration-workspace/dialogs/remove-team-dialog.element';
import { AdminWorkspaceTeamsPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-teams.page';
import { assertAccessDenied } from '../../../../utils/assert-access-denied.utils';
import { selectByDataTestToolbarButtonId } from '../../../../utils/basic-selectors';
import { mockFieldValidationError } from '../../../../data-mock/http-errors.data-mock';
import { mockDataRow, mockGridResponse } from '../../../../data-mock/grid.data-mock';
import { NavBarAdminElement } from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import { makeTeamData, makeUserData } from '../../../../data-mock/administration-views.data-mock';
import { UserViewDetailPage } from '../../../../page-objects/pages/administration-workspace/user-view/user-view-detail.page';
import { ProjectWithoutPermission } from '../../../../page-objects/pages/administration-workspace/user-view/dialogs/add-user-authorisations.dialog';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { TeamViewPage } from '../../../../page-objects/pages/administration-workspace/team-view/team-view.page';
import { GroupedMultiListElement } from '../../../../page-objects/elements/filters/grouped-multi-list.element';
import { AclGroup } from '../../../../../../projects/sqtm-core/src/lib/model/permissions/permissions.model';
import { Team } from '../../../../../../projects/sqtm-core/src/lib/model/team/team.model';
import { Member } from '../../../../page-objects/pages/administration-workspace/team-view/dialogs/add-team-member.dialog';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

describe('Administration Workspace - Teams', function () {
  it('should forbid access to non-admin users', () => {
    assertAccessDenied('administration-workspace/projects');
  });

  it('should display teams grid', () => {
    const page = AdminWorkspaceTeamsPage.initTestAtPageTeams(initialNodes);
    const grid = page.grid;

    grid.assertRowExist('1');
    grid.assertRowExist('2');
    const firstRow = grid.getRow('1');
    firstRow.cell('name').textRenderer().assertContainsText('Team Ying');
    const secondRow = grid.getRow('2');
    secondRow.cell('name').textRenderer().assertContainsText('Team Yang');
    firstRow.cell('description').textRenderer().assertContainsText('ma description');
    firstRow.cell('teamMembersCount').textRenderer().assertContainsText('5');
    firstRow
      .cell('createdOn')
      .textRenderer()
      .assertContainsText(EditableDateFieldElement.dateToDisplayString(todayDate));
    firstRow.cell('createdBy').textRenderer().assertContainsText('superAdmin');
    firstRow
      .cell('lastModifiedOn')
      .textRenderer()
      .assertContainsText(EditableDateFieldElement.dateToDisplayString(todayDate));
    firstRow.cell('lastModifiedBy').textRenderer().assertContainsText('superAdmin');
  });

  it('should add team', () => {
    const page = AdminWorkspaceTeamsPage.initTestAtPageTeams(initialNodes);
    const httpMockView = new HttpMockBuilder('team-view/3?*')
      .get()
      .responseBody({
        id: 3,
        name: 'Team1',
        description: 'A description',
        createdOn: new Date(),
        createdBy: 'admin',
        lastModifiedBy: 'admin',
        lastModifiedOn: new Date(),
        projectPermissions: {
          projectId: -1,
          projectName: 'Alpha Project',
          permissionGroup: {
            id: 1,
            qualifiedName: AclGroup.PROJECT_MANAGER,
            active: true,
            system: true,
          },
        },
        members: [],
        profiles: [
          {
            id: 1,
            qualifiedName: AclGroup.PROJECT_MANAGER,
            active: true,
            system: true,
          },
          {
            id: 1,
            qualifiedName: AclGroup.TEST_EDITOR,
            active: true,
            system: true,
          },
        ],
      })
      .build();

    const dialog = page.openCreateTeam();
    dialog.assertExists();
    dialog.fillName('Team1');
    dialog.fillDescription('A description');

    dialog.addWithOptions({
      addAnother: false,
      createResponse: { id: 3 },
      gridResponse: addTeamResponse,
    });

    httpMockView.wait();
    const view = new TeamViewPage();
    view.assertExists();

    const grid = page.grid;

    grid.assertRowExist(1);
    grid.assertRowExist(2);
    grid.assertRowExist(3);

    // Add another
    const dialogAnother = page.openCreateTeam();
    dialogAnother.assertExists();
    dialogAnother.fillName('Team1');
    dialogAnother.fillDescription('A description');

    dialogAnother.addWithOptions({
      addAnother: true,
      createResponse: { id: 3 },
      gridResponse: addTeamResponse,
    });

    dialogAnother.assertExists();
    dialogAnother.checkIfFormIsEmpty();
    dialogAnother.cancel();
    dialogAnother.assertNotExist();
  });

  it('should validate creation form', () => {
    const page = AdminWorkspaceTeamsPage.initTestAtPageTeams(initialNodes);
    const dialog = page.openCreateTeam();
    dialog.assertExists();

    // should forbid to add a team with an empty name
    dialog.fillDescription('d');
    dialog.clickOnAddButton();
    dialog.checkIfRequiredErrorMessageIsDisplayed();

    // should forbid to add a team if name already exists
    dialog.fillName('a');
    const nameHttpError = mockFieldValidationError(
      'name',
      'sqtm-core.error.generic.name-already-in-use',
    );
    dialog.addWithServerSideFailure(nameHttpError);
    dialog.assertExists();
    dialog.checkIfNameAlreadyInUseErrorMessageIsDisplayed();
  });

  it('should allow team removal', () => {
    const page = AdminWorkspaceTeamsPage.initTestAtPageTeams(initialNodes);
    const iconRenderer = page.grid.getRow(2, 'rightViewport').cell('delete').iconRenderer();
    iconRenderer.assertContainsIcon('anticon-sqtm-core-generic:delete');
    iconRenderer.click();

    // Confirm
    const dialog = new RemoveTeamDialogElement([2]);
    dialog.deleteForSuccess(mockGridResponse('partyId', [initialNodes.dataRows[0]]));
  });

  it('should delete multiple teams', () => {
    cy.viewport(1200, 700);
    const page = AdminWorkspaceTeamsPage.initTestAtPageTeams(initialNodes);
    page.grid.selectRows(['1', '2'], '#', 'leftViewport');

    cy.get(selectByDataTestToolbarButtonId('delete-button')).click();

    // Confirm
    const dialog = new RemoveTeamDialogElement([1, 2]);
    dialog.deleteForSuccess(mockGridResponse('partyId', []));
  });
});

const todayDate = new Date();

const initialNodes: GridResponse = {
  count: 2,
  dataRows: [
    mockDataRow({
      id: '1',
      children: [],
      data: {
        partyId: 1,
        name: 'Team Ying',
        description: 'ma description',
        teamMembersCount: '5',
        createdOn: todayDate,
        createdBy: 'superAdmin',
        lastModifiedOn: todayDate,
        lastModifiedBy: 'superAdmin',
      },
    }),
    mockDataRow({
      id: '2',
      children: [],
      data: {
        partyId: 2,
        name: 'Team Yang',
      },
    }),
  ],
};

const addTeamResponse: GridResponse = {
  count: 3,
  dataRows: [
    ...initialNodes.dataRows,
    mockDataRow({
      id: 3,
      children: [],
      data: { name: 'Team1' },
    }),
  ],
};

describe('Administration Workspace - Teams - Members', function () {
  it('should add member to team', () => {
    const page = AdminWorkspaceTeamsPage.initTestAtPageTeams(initialNodes);
    const teamViewPage = page.selectTeamByName(initialNodes.dataRows[0].data.name, getTeamDto());

    teamViewPage.assertExists();
    teamViewPage.teamMembersPanel.grid.assertExists();

    const nonMembers: Member[] = [
      {
        partyId: -3,
        firstName: 'James',
        lastName: 'Bond',
        login: '007',
        fullName: 'James Bond (007)',
      },
      {
        partyId: -4,
        firstName: 'John',
        lastName: 'Snow',
        login: 'knowNothing',
        fullName: 'John Snow (knowNothing)',
      },
    ];

    const addTeamMemberDialog =
      teamViewPage.teamMembersPanel.clickOnAddTeamMemberButton(nonMembers);
    const multiList = new GroupedMultiListElement();
    addTeamMemberDialog.selectMembers(multiList, 'James Bond (007)', 'John Snow (knowNothing)');
    addTeamMemberDialog.confirm([]);
  });

  it('should remove team member', () => {
    const page = AdminWorkspaceTeamsPage.initTestAtPageTeams(initialNodes);
    const teamViewPage = page.selectTeamByName(initialNodes.dataRows[0].data.name, getTeamDto());

    teamViewPage.assertExists();
    teamViewPage.teamMembersPanel.grid.assertExists();
    new NavBarAdminElement().toggle();
    teamViewPage.foldGrid();

    teamViewPage.teamMembersPanel.deleteOne('John Doe (jdoe)');
  });

  it('should remove multiple team members', () => {
    const page = AdminWorkspaceTeamsPage.initTestAtPageTeams(initialNodes);
    const teamViewPage = page.selectTeamByName(initialNodes.dataRows[0].data.name, getTeamDto());

    teamViewPage.assertExists();
    teamViewPage.teamMembersPanel.grid.assertExists();
    new NavBarAdminElement().toggle();
    teamViewPage.foldGrid();

    teamViewPage.teamMembersPanel.deleteMultiple(['John Doe (jdoe)', 'Jane Smith (jsmith)']);
  });

  it('should prevent adding member if none is selected', () => {
    const page = AdminWorkspaceTeamsPage.initTestAtPageTeams(initialNodes);
    const teamViewPage = page.selectTeamByName(initialNodes.dataRows[0].data.name, getTeamDto());

    teamViewPage.assertExists();
    teamViewPage.teamMembersPanel.grid.assertExists();
    new NavBarAdminElement().toggle();
    teamViewPage.foldGrid();

    const nonMembers: Member[] = [];

    const addTeamMemberDialog =
      teamViewPage.teamMembersPanel.clickOnAddTeamMemberButton(nonMembers);

    addTeamMemberDialog.clickOnConfirmButton();
    cy.get('.sqtm-core-error-message').should('have.length', 1);
  });

  it('should navigate to user detail page and back', () => {
    const teamsWorkspace = AdminWorkspaceTeamsPage.initTestAtPageTeams(initialNodes);
    const teamViewPage = teamsWorkspace.selectTeamByName(
      initialNodes.dataRows[0].data.name,
      getTeamDto(),
    );

    teamViewPage.teamMembersPanel.showUserDetail('John Doe (jdoe)', makeUserData());
    const userViewDetail = new UserViewDetailPage();
    userViewDetail.assertExists();

    userViewDetail.clickBackButton();
    teamsWorkspace.assertExists();
  });

  function getTeamDto(): Team {
    return makeTeamData({
      id: 3,
      name: 'Team 3',
      description: 'This is the third team',
      createdOn: new Date().toISOString(),
      createdBy: 'admin',
      lastModifiedBy: 'admin',
      lastModifiedOn: new Date().toISOString(),
      projectPermissions: [
        {
          projectId: -1,
          projectName: 'Alpha Project',
          permissionGroup: {
            id: 1,
            qualifiedName: AclGroup.PROJECT_MANAGER,
            active: true,
            system: true,
          },
        },
        {
          projectId: -2,
          projectName: 'Beta Project',
          permissionGroup: {
            id: 2,
            qualifiedName: AclGroup.TEST_RUNNER,
            active: true,
            system: true,
          },
        },
      ],
      profiles: [
        {
          id: 1,
          qualifiedName: AclGroup.PROJECT_MANAGER,
          active: true,
          system: true,
        },
        {
          id: 1,
          qualifiedName: AclGroup.TEST_EDITOR,
          active: true,
          system: true,
        },
      ],
      members: [
        {
          partyId: -1,
          firstName: 'John',
          lastName: 'Doe',
          login: 'jdoe',
          fullName: 'John Doe (jdoe)',
          active: true,
        },
        {
          partyId: -2,
          firstName: 'Jane',
          lastName: 'Smith',
          login: 'jsmith',
          fullName: 'Jane Smith (jsmith)',
          active: true,
        },
      ],
    });
  }
});

describe('Administration Workspace - Teams - Authorisations', function () {
  it('should add project permissions to team', () => {
    const page = AdminWorkspaceTeamsPage.initTestAtPageTeams(initialNodes);
    const teamViewPage = page.selectTeamByName(initialNodes.dataRows[0].data.name, getTeamDto());

    teamViewPage.assertExists();
    teamViewPage.teamAuthorisationsPanel.grid.assertExists();
    new NavBarAdminElement().toggle();
    teamViewPage.foldGrid();

    const projectsWithoutPermissions: ProjectWithoutPermission[] = [
      {
        id: '-1',
        name: 'Alpha project',
      },
      {
        id: '-2',
        name: 'Beta Project',
      },
    ];

    const addTeamAuthorisationsDialog =
      teamViewPage.teamAuthorisationsPanel.clickOnAddPermissionButton(projectsWithoutPermissions);

    addTeamAuthorisationsDialog.selectProjects('Alpha project', 'Beta Project');
    addTeamAuthorisationsDialog.selectProfile('Testeur référent');

    addTeamAuthorisationsDialog.confirm([]);
  });

  it('should remove team authorisation', () => {
    const page = AdminWorkspaceTeamsPage.initTestAtPageTeams(initialNodes);
    const teamViewPage = page.selectTeamByName(initialNodes.dataRows[0].data.name, getTeamDto());

    teamViewPage.assertExists();
    teamViewPage.teamAuthorisationsPanel.grid.assertExists();
    new NavBarAdminElement().toggle();
    teamViewPage.foldGrid();

    teamViewPage.teamAuthorisationsPanel.deleteOne('Alpha Project');
  });

  it('should remove multiple team authorisations', () => {
    const page = AdminWorkspaceTeamsPage.initTestAtPageTeams(initialNodes);
    const teamViewPage = page.selectTeamByName(initialNodes.dataRows[0].data.name, getTeamDto());

    teamViewPage.assertExists();
    teamViewPage.teamAuthorisationsPanel.grid.assertExists();
    new NavBarAdminElement().toggle();
    teamViewPage.foldGrid();

    teamViewPage.teamAuthorisationsPanel.deleteMultiple(['Alpha Project', 'Beta Project']);
  });

  it('should prevent adding authorisations with empty fields', () => {
    const page = AdminWorkspaceTeamsPage.initTestAtPageTeams(initialNodes);
    const teamViewPage = page.selectTeamByName(initialNodes.dataRows[0].data.name, getTeamDto());

    teamViewPage.assertExists();
    teamViewPage.teamAuthorisationsPanel.grid.assertExists();
    new NavBarAdminElement().toggle();
    teamViewPage.foldGrid();

    const projectsWithoutPermissions: ProjectWithoutPermission[] = [];

    const addTeamAuthorisationsDialog =
      teamViewPage.teamAuthorisationsPanel.clickOnAddPermissionButton(projectsWithoutPermissions);

    addTeamAuthorisationsDialog.clickOnAddButton();
    cy.get('.sqtm-core-error-message').should('have.length', 2);
  });

  function getTeamDto(): Team {
    return {
      id: 3,
      name: 'Team 3',
      description: 'This is the third team',
      createdOn: new Date().toISOString(),
      createdBy: 'admin',
      lastModifiedBy: 'admin',
      lastModifiedOn: new Date().toISOString(),
      projectPermissions: [
        {
          projectId: -1,
          projectName: 'Alpha Project',
          permissionGroup: {
            id: 1,
            qualifiedName: AclGroup.PROJECT_MANAGER,
            active: true,
            system: true,
          },
        },
        {
          projectId: -2,
          projectName: 'Beta Project',
          permissionGroup: {
            id: 2,
            qualifiedName: AclGroup.TEST_RUNNER,
            active: true,
            system: true,
          },
        },
      ],
      profiles: [
        {
          id: 1,
          qualifiedName: AclGroup.PROJECT_MANAGER,
          active: true,
          system: true,
        },
        {
          id: 1,
          qualifiedName: AclGroup.TEST_EDITOR,
          active: true,
          system: true,
        },
      ],
      members: [],
    };
  }
});
