import { EditableDateFieldElement } from '../../../../page-objects/elements/forms/editable-date-field.element';
import { RemoveUserDialogElement } from '../../../../page-objects/pages/administration-workspace/dialogs/remove-user-dialog.element';
import { AdminWorkspaceUsersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import { assertAccessDenied } from '../../../../utils/assert-access-denied.utils';
import {
  selectByDataTestFieldId,
  selectByDataTestToolbarButtonId,
} from '../../../../utils/basic-selectors';
import { mockFieldValidationError } from '../../../../data-mock/http-errors.data-mock';
import { ProjectWithoutPermission } from '../../../../page-objects/pages/administration-workspace/user-view/dialogs/add-user-authorisations.dialog';
import { NavBarAdminElement } from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import { AclGroup } from '../../../../../../projects/sqtm-core/src/lib/model/permissions/permissions.model';
import { TeamAssociation } from '../../../../page-objects/pages/administration-workspace/user-view/dialogs/associate-team-to-user.dialog';
import { makeTeamData, makeUserData } from '../../../../data-mock/administration-views.data-mock';
import { TeamViewDetailPage } from '../../../../page-objects/pages/administration-workspace/team-view/team-view-detail.page';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { UserViewPage } from '../../../../page-objects/pages/administration-workspace/user-view/user-view.page';
import { User } from '../../../../../../projects/sqtm-core/src/lib/model/user/user.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { mockDataRow, mockGridResponse } from '../../../../data-mock/grid.data-mock';

describe('Administration workspace users', function () {
  it('should forbid access to non-admin users', () => {
    assertAccessDenied('administration-workspace/projects');
  });

  it('should display users grid', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const grid = page.grid;

    grid.assertRowExist(1);
    const row = grid.getRow(1);
    row.cell('login').textRenderer().assertContainsText('admin');
    grid
      .getRow(2)
      .cell('active')
      .iconRenderer()
      .assertContainsIcon('anticon-sqtm-core-administration:user');
    row.cell('userGroup').textRenderer().assertContainsText('Administrateur');
    row.cell('firstName').textRenderer().assertContainsText('adminFirstName');
    row.cell('lastName').textRenderer().assertContainsText('adminLastName');
    row.cell('email').textRenderer().assertContainsText('admin@henix.fr');
    row
      .cell('createdOn')
      .textRenderer()
      .assertContainsText(EditableDateFieldElement.dateToDisplayString(todayDate));
    row.cell('createdBy').textRenderer().assertContainsText('superAdmin');
    grid.scrollPosition('right', 'mainViewport');
    row.cell('habilitationCount').textRenderer().assertContainsText('3');
    row.cell('teamCount').textRenderer().assertContainsText('2');
    row
      .cell('lastConnectedOn')
      .textRenderer()
      .assertContainsText(EditableDateFieldElement.dateToDisplayString(todayDate));
  });

  it('should add user', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const httpMockView = new HttpMockBuilder('user-view/5?*')
      .get()
      .responseBody({
        id: 5,
        login: 'jSmith',
        firstName: 'John',
        lastName: 'Smith',
        email: 'jsmith@email.com',
        active: false,
        createdOn: new Date(),
        createdBy: 'admin',
        lastModifiedBy: 'admin',
        lastModifiedOn: new Date(),
        lastConnectedOn: new Date(),
        usersGroupBinding: 2,
        usersGroups: getUserDto().usersGroups,
        projectPermissions: getUserDto().projectPermissions,
        teams: getUserDto().teams,
      })
      .build();

    const dialog = page.openCreateUser();
    dialog.assertExists();
    dialog.fillLogin('jSmith');
    dialog.fillName('Smith');
    dialog.fillFirstName('John');
    dialog.fillEmail('jsmith@email.com');
    dialog.selectGroup('Utilisateur');
    dialog.fillPassword('password');
    dialog.fillConfirmPassword('password');

    dialog.addWithOptions({
      addAnother: false,
      createResponse: { id: 5 },
      gridResponse: addUserResponse,
    });

    httpMockView.wait();
    const view = new UserViewPage();
    view.assertExists();
    view.emailTextField.checkContent('jsmith@email.com');

    const grid = page.grid;

    grid.assertRowExist(1);
    grid.assertRowExist(2);
    grid.assertRowExist(3);
    grid.assertRowExist(4);
    grid.assertRowExist(5);

    // should add another user
    const dialogAnother = page.openCreateUser();
    dialogAnother.assertExists();
    dialogAnother.fillLogin('j');
    dialogAnother.fillName('s');
    dialogAnother.fillFirstName('j');
    dialogAnother.selectGroup('Utilisateur');
    dialogAnother.fillPassword('password');
    dialogAnother.fillConfirmPassword('password');

    dialogAnother.addWithOptions({
      addAnother: true,
      createResponse: { id: 5 },
      gridResponse: addUserResponse,
    });

    dialogAnother.assertExists();
    dialogAnother.checkFormIsEmpty();
    dialogAnother.cancel();
    dialogAnother.assertNotExist();
  });

  it('should validate creation form', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);

    const dialog = page.openCreateUser();
    dialog.assertExists();

    // should forbid to add a user with an empty login
    dialog.fillLogin('');
    dialog.fillName('s');
    dialog.fillFirstName('j');
    dialog.fillEmail('@');
    dialog.selectGroup('Utilisateur');
    dialog.fillPassword('password');
    dialog.fillConfirmPassword('password');
    dialog.clickOnAddButton();
    dialog.checkIfRequiredErrorMessageIsDisplayed();

    // should forbid to add a user if login already exists
    const loginHttpError = mockFieldValidationError(
      'login',
      'sqtm-core.error.generic.name-already-in-use',
    );
    dialog.fillLogin('a');
    dialog.addWithServerSideFailure(loginHttpError);

    // should forbid to add a user with an empty name
    dialog.fillName('');
    dialog.clickOnAddButton();
    dialog.checkIfRequiredErrorMessageIsDisplayed();

    // should forbid to add a user with a password containing less than 6 characters
    dialog.fillName('a');
    dialog.fillPassword('pass');
    dialog.fillConfirmPassword('pass');
    dialog.clickOnAddButton();
    dialog.checkIfInsufficientCharsInPasswordErrorMessageIsDisplayed();

    // should forbid to add a user with an empty password
    dialog.fillPassword('');
    dialog.fillConfirmPassword('');
    dialog.clickOnAddButton();
    dialog.checkIfRequiredErrorMessageIsDisplayed();

    // should forbid to add a user with a not matching password confirmation
    dialog.fillPassword('password');
    dialog.fillConfirmPassword('passwoooord');
    dialog.clickOnAddButton();
    dialog.checkIfNotMatchingErrorMessageIsDisplayed();
  });

  it('should allow user removal', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const grid = page.grid;

    // Show dialog
    grid.scrollPosition('right', 'mainViewport');

    const iconRenderer = grid.getRow(3, 'rightViewport').cell('delete').iconRenderer();
    iconRenderer.assertContainsIcon('anticon-sqtm-core-generic:delete');
    iconRenderer.click();

    // Confirm
    const dialog = new RemoveUserDialogElement([3]);
    dialog.deleteUserForSuccess({
      count: 2,
      dataRows: [initialNodes.dataRows[0], initialNodes.dataRows[1]],
    });
  });

  it('should delete multiple users', () => {
    cy.viewport(1200, 700);
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);

    page.grid.selectRows(['2', '3'], '#', 'leftViewport');

    // Show dialog
    page.grid.scrollPosition('right', 'mainViewport');

    cy.get(selectByDataTestToolbarButtonId('delete-button')).click();

    // Confirm
    const dialog = new RemoveUserDialogElement([2, 3]);
    dialog.deleteForSuccess({
      count: 1,
      dataRows: [initialNodes.dataRows[0]],
    });
  });

  it('should edit multiple users', () => {
    cy.viewport(1200, 700);
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);

    page.grid.selectRows(['2', '3'], '#', 'leftViewport');

    const dialog = page.openMultiEditDialog();
    dialog.assertExists();

    dialog.stateSelect.assertExists();
    dialog.stateSelect.check();
    dialog.stateSelect.selectValue('Désactiver les utilisateurs');

    dialog.canDeleteSelect.assertExists();
    dialog.canDeleteSelect.check();
    dialog.canDeleteSelect.selectValue('Interdire la suppression');

    dialog.confirm(
      initialNodes.dataRows.map((row) => ({ ...row, data: { ...row.data, active: false } })),
    );
  });

  it('should prevent deactivation of current user', () => {
    cy.viewport(1200, 700);
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);

    page.grid.selectRows(['1', '2', '3'], '#', 'leftViewport');

    const dialog = page.openMultiEditDialog();
    dialog.assertExists();

    dialog.stateSelect.assertExists();
    dialog.stateSelect.check();
    dialog.stateSelect.selectValue('Désactiver les utilisateurs');

    dialog.canDeleteSelect.assertExists();
    dialog.canDeleteSelect.check();
    dialog.canDeleteSelect.selectValue('Interdire la suppression');

    dialog.clickOnConfirmButton();
    dialog.assertExists();
  });
});

const todayDate = new Date();

const initialNodes: GridResponse = {
  count: 4,
  dataRows: [
    mockDataRow({
      id: '1',
      children: [],
      data: {
        partyId: 1,
        active: true,
        login: 'admin',
        userGroup: 'squashtest.authz.group.core.Admin',
        firstName: 'adminFirstName',
        lastName: 'adminLastName',
        email: 'admin@henix.fr',
        createdOn: todayDate,
        createdBy: 'superAdmin',
        habilitationCount: '3',
        teamCount: '2',
        lastConnectedOn: todayDate,
      },
    }),
    mockDataRow({
      id: '2',
      children: [],
      data: {
        partyId: 2,
        active: true,
        login: 'jimmy',
        userGroup: 'squashtest.authz.group.tm.User',
        firstName: 'jimmy',
        lastName: 'ymmij',
        email: 'jj@henix.fr',
        createdOn: todayDate,
        createdBy: 'superAdmin',
        habilitationCount: '0',
        teamCount: '1',
      },
    }),
    mockDataRow({
      id: '3',
      children: [],
      data: {
        partyId: 3,
        active: true,
        login: 'bobby',
        userGroup: 'squashtest.authz.group.tm.User',
        firstName: 'bobby',
        lastName: 'xyz',
        email: 'bx@henix.fr',
        createdOn: todayDate,
        createdBy: 'superAdmin',
        habilitationCount: '0',
        teamCount: '1',
      },
    }),
    mockDataRow({
      id: '4',
      children: [],
      data: {
        partyId: 4,
        active: true,
        login: 'ziggy',
        userGroup: 'squashtest.authz.group.tm.TestAutomationServer',
        usersGroupBinding: 4,
        firstName: 'ziggy',
        lastName: 'stardust',
        email: 'ziggy.stardust@henix.fr',
        createdOn: todayDate,
        createdBy: 'superAdmin',
        habilitationCount: '0',
        teamCount: '0',
      },
    }),
  ],
};

const addUserResponse: GridResponse = {
  count: 5,
  dataRows: [
    ...initialNodes.dataRows,
    mockDataRow({
      id: '5',
      children: [],
      data: { name: 'User5' },
    }),
  ],
};

describe('Administration Workspace - Users - Information', function () {
  it('should not be able to update group from Test auto server', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const userViewPage = page.selectUserByLogin(
      initialNodes.dataRows[3].data.login,
      getTestAutoServerUserDto(),
    );
    const infoPanel = userViewPage.find('sqtm-app-user-information-panel');
    infoPanel.find(selectByDataTestFieldId('user-group-test-auto')).should('exist');
    userViewPage.groupSelectField.assertNotExist();
  });

  it('should be able to update group from User', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const userViewPage = page.selectUserByLogin(initialNodes.dataRows[0].data.login, getUserDto());
    const infoPanel = userViewPage.find('sqtm-app-user-information-panel');
    infoPanel.find(selectByDataTestFieldId('user-group-test-auto')).should('not.exist');
    userViewPage.groupSelectField.checkAllOptions(['Administrateur', 'Utilisateur']);
  });
});

describe('Administration Workspace - Users - Authorisations', function () {
  it('should add project permissions to user', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const userViewPage = page.selectUserByLogin(initialNodes.dataRows[0].data.login, getUserDto());

    userViewPage.assertExists();
    userViewPage.authorisationsPanel.grid.assertExists();

    const projectsWithoutPermissions: ProjectWithoutPermission[] = [
      {
        id: '-1',
        name: 'Alpha project',
      },
      {
        id: '-2',
        name: 'Beta Project',
      },
    ];

    const addUserAuthorisationsDialog =
      userViewPage.authorisationsPanel.clickOnAddPermissionButtonAndAssertDialogExists(
        projectsWithoutPermissions,
      );

    addUserAuthorisationsDialog.selectProjects('Alpha project', 'Beta Project');
    addUserAuthorisationsDialog.selectProfile('Testeur référent');

    addUserAuthorisationsDialog.confirm([]);
  });

  it('should remove user authorisation', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const userViewPage = page.selectUserByLogin(initialNodes.dataRows[0].data.login, getUserDto());

    userViewPage.assertExists();
    userViewPage.authorisationsPanel.grid.assertExists();
    new NavBarAdminElement().toggle();
    userViewPage.foldGrid();

    userViewPage.authorisationsPanel.deleteOne('Alpha Project');
  });

  it('should remove multiple authorisations', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const userViewPage = page.selectUserByLogin(initialNodes.dataRows[0].data.login, getUserDto());

    userViewPage.assertExists();
    userViewPage.authorisationsPanel.grid.assertExists();
    new NavBarAdminElement().toggle();
    userViewPage.foldGrid();

    userViewPage.authorisationsPanel.deleteMultiple(['Alpha Project', 'Beta Project']);
  });

  it('should prevent adding authorisations with empty fields', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const userViewPage = page.selectUserByLogin(initialNodes.dataRows[0].data.login, getUserDto());

    userViewPage.assertExists();
    userViewPage.authorisationsPanel.grid.assertExists();
    new NavBarAdminElement().toggle();
    userViewPage.foldGrid();

    const projectsWithoutPermissions: ProjectWithoutPermission[] = [];

    const addUserAuthorisationsDialog =
      userViewPage.authorisationsPanel.clickOnAddPermissionButtonAndAssertDialogExists(
        projectsWithoutPermissions,
      );

    addUserAuthorisationsDialog.clickOnAddButton();
    cy.get('.sqtm-core-error-message').should('have.length', 2);
  });

  it('should toggle permission to delete from front', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const userViewPage = page.selectUserByLogin(initialNodes.dataRows[0].data.login, getUserDto());

    userViewPage.anchors.clickLink('permissions');
    userViewPage.permissionPanel.assertExists();

    userViewPage.permissionPanel.canDeleteFromFrontSwitchElement.checkValue(false);
    userViewPage.permissionPanel.canDeleteFromFrontSwitchElement.toggle();
    userViewPage.permissionPanel.canDeleteFromFrontSwitchElement.checkValue(true);
  });
});

describe('Administration Workspace - Users - Teams', function () {
  it('should associate user to team', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const userViewPage = page.selectUserByLogin(initialNodes.dataRows[0].data.login, getUserDto());

    userViewPage.assertExists();
    userViewPage.teamsPanel.grid.assertExists();

    const unassociatedTeams: TeamAssociation[] = [
      {
        partyId: '-1',
        name: 'Team A',
      },
      {
        partyId: '-2',
        name: 'Team B',
      },
    ];

    const associateUserToTeamDialog =
      userViewPage.teamsPanel.clickOnAssociateTeamToUserButton(unassociatedTeams);

    associateUserToTeamDialog.selectTeams('Team A', 'Team B');

    associateUserToTeamDialog.confirm([]);
  });

  it('should remove team association', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const userViewPage = page.selectUserByLogin(initialNodes.dataRows[0].data.login, getUserDto());

    userViewPage.assertExists();
    userViewPage.teamsPanel.grid.assertExists();

    const navBar = new NavBarAdminElement();
    navBar.toggle();

    userViewPage.teamsPanel.deleteOne('Team A');
  });

  it('should remove multiple team associations', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const userViewPage = page.selectUserByLogin(initialNodes.dataRows[0].data.login, getUserDto());

    userViewPage.assertExists();
    userViewPage.teamsPanel.grid.assertExists();

    const navBar = new NavBarAdminElement();
    navBar.toggle();

    userViewPage.teamsPanel.deleteMultiple(['Team A', 'Team B']);
  });

  it('should prevent adding team if none is selected', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const userViewPage = page.selectUserByLogin(initialNodes.dataRows[0].data.login, getUserDto());

    userViewPage.assertExists();
    userViewPage.teamsPanel.grid.assertExists();

    const unassociatedTeams: TeamAssociation[] = [];

    const associateUserToTeamDialog =
      userViewPage.teamsPanel.clickOnAssociateTeamToUserButton(unassociatedTeams);

    associateUserToTeamDialog.clickOnConfirmButton();
    cy.get('.sqtm-core-error-message').should('have.length', 1);
  });

  it('should navigate to team detail page and back', () => {
    const usersWorkspace = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const userViewPage = usersWorkspace.selectUserByLogin(
      initialNodes.dataRows[0].data.login,
      getUserDto(),
    );

    userViewPage.teamsPanel.showTeamDetail('Team A', makeTeamData());
    const teamViewDetail = new TeamViewDetailPage();
    teamViewDetail.assertExists();

    teamViewDetail.clickBackButton();
    usersWorkspace.assertExists();
  });
});

describe('Administration Workspace - Users - API tokens', function () {
  it('should display API token grid for test automation server user', () => {
    const usersWorkspace = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const mockPersonalApiTokens = new HttpMockBuilder<GridResponse>('user-view/*/api-tokens')
      .post()
      .responseBody(
        mockGridResponse('tokenId', [
          mockDataRow({
            id: 1,
            data: {
              createdOn: '2023-07-02T08:01:06.420+00:00',
              expiryDate: '2024-07-02',
              lastUsage: null,
              name: 'token pour lire',
              permissions: 'READ',
              tokenId: 1,
            },
          }),
        ]),
      )
      .build();

    const userViewPage = usersWorkspace.selectUserByLogin(
      initialNodes.dataRows[3].data.login,
      getTestAutoServerUserDto(),
      mockPersonalApiTokens,
    );
    userViewPage.find('sqtm-app-user-api-token-panel').should('exist');
  });

  it('should not display API token grid for basic user and admin user', () => {
    const usersWorkspace = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const userViewPage = usersWorkspace.selectUserByLogin(
      initialNodes.dataRows[0].data.login,
      getUserDto(),
    );
    userViewPage.find('sqtm-app-user-api-token-panel').should('not.exist');
  });
});

function getUserDto(): User {
  return makeUserData({
    id: 3,
    login: '007',
    firstName: 'James',
    lastName: 'Bond',
    email: '007@secret-agent.co.uk',
    active: true,
    createdOn: new Date().toISOString(),
    createdBy: 'admin',
    lastModifiedBy: 'admin',
    lastModifiedOn: new Date().toISOString(),
    lastConnectedOn: new Date().toISOString(),
    profiles: [
      {
        id: 1,
        qualifiedName: AclGroup.PROJECT_MANAGER,
        active: true,
        system: true,
      },
      {
        id: 1,
        qualifiedName: AclGroup.TEST_EDITOR,
        active: true,
        system: true,
      },
    ],
    usersGroupBinding: 2,
    usersGroups: [
      { id: 1, qualifiedName: 'squashtest.authz.group.core.Admin' },
      { id: 4, qualifiedName: 'squashtest.authz.group.tm.TestAutomationServer' },
      { id: 2, qualifiedName: 'squashtest.authz.group.tm.User' },
    ],
    projectPermissions: [
      {
        projectId: -1,
        projectName: 'Alpha Project',
        permissionGroup: {
          id: 1,
          qualifiedName: AclGroup.PROJECT_MANAGER,
          active: true,
          system: true,
        },
      },
      {
        projectId: -2,
        projectName: 'Beta Project',
        permissionGroup: {
          id: 2,
          qualifiedName: AclGroup.TEST_RUNNER,
          active: true,
          system: true,
        },
      },
    ],
    teams: [
      {
        partyId: -1,
        name: 'Team A',
      },
      {
        partyId: -2,
        name: 'Team B',
      },
    ],
  });
}

function getTestAutoServerUserDto(): User {
  return makeUserData({
    id: 4,
    usersGroupBinding: 4,
  });
}
