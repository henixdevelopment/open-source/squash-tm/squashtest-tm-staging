import { EditableDateFieldElement } from '../../../../page-objects/elements/forms/editable-date-field.element';
import { AdminWorkspaceConnectionLogsPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-connection-logs.page';
import { assertAccessDenied } from '../../../../utils/assert-access-denied.utils';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { mockDataRow } from '../../../../data-mock/grid.data-mock';

describe('Administration Workspace - Connection logs', function () {
  const todayDate = new Date();

  const initialNodes: GridResponse = {
    count: 1,
    dataRows: [
      mockDataRow({
        id: 'ConnectionAttemptLog-1',
        children: [],
        data: {
          login: 'admin',
          connectionDate: todayDate,
          success: true,
        },
      }),
    ],
  };

  it('should forbid access to non-admin users', () => {
    assertAccessDenied('administration-workspace/projects');
  });

  it('should display connection logs grid', () => {
    const page = AdminWorkspaceConnectionLogsPage.initTestAtPageUsersConnexionLogs(initialNodes);
    const grid = page.grid;

    grid.assertRowExist('ConnectionAttemptLog-1');
    const row = grid.getRow('ConnectionAttemptLog-1');
    row.cell('login').textRenderer().assertContainsText('admin');
    row
      .cell('connectionDate')
      .textRenderer()
      .assertContainsText(EditableDateFieldElement.dateToDisplayString(todayDate));
    row.cell('success').textRenderer().assertContainsText('Oui');
  });

  it('should open export dialog', () => {
    const page = AdminWorkspaceConnectionLogsPage.initTestAtPageUsersConnexionLogs(initialNodes);
    const exportDialog = page.openExportDialog();
    exportDialog.assertExists();
    exportDialog.confirm();
  });
});
