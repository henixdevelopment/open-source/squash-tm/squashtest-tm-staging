import { RemoveRequirementsLinksDialogElement } from '../../../../page-objects/pages/administration-workspace/dialogs/remove-requirements-links-dialog.element';
import { RemoveDefaultRequirementsLinksDialogElement } from '../../../../page-objects/pages/administration-workspace/dialogs/remove-default-requirements-links-dialog.element';
import { AdminWorkspaceRequirementsLinksPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-requirements-links.page';
import { selectByDataTestToolbarButtonId } from '../../../../utils/basic-selectors';
import { mockFieldValidationError } from '../../../../data-mock/http-errors.data-mock';
import { RequirementVersionLinkType } from '../../../../../../projects/sqtm-core/src/lib/model/requirement/requirement-version-link-type.model';

describe('Administration workspace - Requirements links', function () {
  it('should display the grid of requirements links', () => {
    const page =
      AdminWorkspaceRequirementsLinksPage.initTestAtPageRequirementsLinks(requirementsLinks);
    const grid = page.grid;

    grid.assertRowExist(1);
    const row = grid.getRow(1);
    row.cell('role').textRenderer().assertContainsText('role1');
    row.cell('role1Code').textRenderer().assertContainsText('role1code');
    row.cell('role2').textRenderer().assertContainsText('role2');
    row.cell('role2Code').textRenderer().assertContainsText('role2code');
    row.cell('default').findCell().find('input[type=radio]:checked').should('exist');
  });

  it('should add a requirements link type', () => {
    const page =
      AdminWorkspaceRequirementsLinksPage.initTestAtPageRequirementsLinks(requirementsLinks);

    const dialog = page.openCreateRequirementsLink();
    dialog.assertExists();
    dialog.fillRole1('test1');
    dialog.fillRole1Code('test1Code');
    dialog.fillRole2('test2');
    dialog.fillRole2Code('test2Code');

    dialog.addRequirementLink(4, addRequirementsLinkResponse, false);

    const grid = page.grid;

    grid.assertRowExist(1);
    grid.assertRowExist(2);
    grid.assertRowExist(3);
    grid.assertRowExist(4);

    // Add another
    const dialogAnother = page.openCreateRequirementsLink();
    dialogAnother.assertExists();
    dialogAnother.fillRole1('test1');
    dialogAnother.fillRole1Code('test1Code');
    dialogAnother.fillRole2('test2');
    dialogAnother.fillRole2Code('test2Code');
    dialogAnother.addRequirementLink(4, addRequirementsLinkResponse, true);
    dialogAnother.assertExists();
    dialogAnother.checkIfFormIsEmpty();
    dialogAnother.cancel();
    dialogAnother.assertNotExist();
  });

  it('should validate creation form', () => {
    const page =
      AdminWorkspaceRequirementsLinksPage.initTestAtPageRequirementsLinks(requirementsLinks);

    const dialog = page.openCreateRequirementsLink();
    dialog.assertExists();

    // should forbid to add a requirements link type with an empty role1
    dialog.fillRole1('');
    dialog.fillRole1Code('test1Code');
    dialog.fillRole2('role2');
    dialog.fillRole2Code('test2Code');
    dialog.clickOnAddButton();
    dialog.checkIfRequiredErrorMessageIsDisplayed();

    // should forbid to add a requirements link type with an empty role1Code
    dialog.fillRole1('test1');
    dialog.fillRole1Code('');
    dialog.clickOnAddButton();
    dialog.checkIfRequiredErrorMessageIsDisplayed();

    // should forbid to add a requirements link type with an empty role2
    dialog.fillRole1Code('test1Code');
    dialog.fillRole2('');
    dialog.clickOnAddButton();
    dialog.checkIfRequiredErrorMessageIsDisplayed();

    // should forbid to add a requirements link type with an empty role2Code
    dialog.fillRole2('test2');
    dialog.fillRole2Code('');
    dialog.clickOnAddButton();
    dialog.checkIfRequiredErrorMessageIsDisplayed();

    // should forbid to add a requirements link type with an existing role1Code
    dialog.fillRole2Code('test2Code');
    const role1CodeHttpError = mockFieldValidationError(
      'role1Code',
      'sqtm-core.exception.requirements-links.link-type-code-already-exists',
    );
    dialog.addWithServerSideFailure(role1CodeHttpError);
    dialog.assertExists();
    dialog.checkIfExistingTypeCodeIsDisplayed();

    // should forbid to add a requirements link type with an existing role2Code
    const role2CodeHttpError = mockFieldValidationError(
      'role2Code',
      'sqtm-core.exception.requirements-links.link-type-code-already-exists',
    );
    dialog.addWithServerSideFailure(role2CodeHttpError);
    dialog.assertExists();
    dialog.checkIfExistingTypeCodeIsDisplayed();
  });

  it('should allow requirements link type removal', () => {
    const page =
      AdminWorkspaceRequirementsLinksPage.initTestAtPageRequirementsLinks(requirementsLinks);

    // Show dialog
    page.grid.getRow(2, 'rightViewport').cell('delete').iconRenderer().click();

    // Confirm
    const dialog = new RemoveRequirementsLinksDialogElement([2]);
    dialog.deleteSuccessfully(
      [2],
      [
        {
          id: 1,
          role: 'role1',
          role1Code: 'role1code',
          role2: 'role2',
          role2Code: 'role2code',
          default: true,
        },
        {
          id: 3,
          role: 'role5',
          role1Code: 'role5code',
          role2: 'role6',
          role2Code: 'role6code',
          default: false,
        },
      ],
    );
  });

  it('should allow requirements link type removal if type is used', () => {
    const page =
      AdminWorkspaceRequirementsLinksPage.initTestAtPageRequirementsLinks(requirementsLinks);

    // Show dialog
    page.grid.getRow(3, 'rightViewport').cell('delete').iconRenderer().click();

    // Confirm
    const dialog = new RemoveRequirementsLinksDialogElement([3]);
    dialog.deleteSuccessfully(
      [3],
      [
        {
          id: 1,
          role: 'role1',
          role1Code: 'role1code',
          role2: 'role2',
          role2Code: 'role2code',
          default: true,
        },
        {
          id: 2,
          role: 'role3',
          role1Code: 'role3code',
          role2: 'role4',
          role2Code: 'role4code',
          default: false,
        },
      ],
    );
  });

  it('should prevent to delete requirements link if it is a default value', () => {
    const page =
      AdminWorkspaceRequirementsLinksPage.initTestAtPageRequirementsLinks(requirementsLinks);

    // Show dialog
    page.grid.getRow(1, 'rightViewport').cell('delete').iconRenderer().click();

    // There should only be a close button
    const alert = new RemoveDefaultRequirementsLinksDialogElement();
    alert.assertMessageSimpleDelete();
    alert.close();
  });

  it('should allow multiple requirements links  removal', () => {
    const page =
      AdminWorkspaceRequirementsLinksPage.initTestAtPageRequirementsLinks(requirementsLinks);

    // Select elements to delete
    page.grid.selectRows([2, 3], '#', 'leftViewport');

    // Show dialog
    cy.get(selectByDataTestToolbarButtonId('delete-button')).click();

    // Confirm
    const dialog = new RemoveRequirementsLinksDialogElement([2, 3]);
    dialog.deleteSuccessfully(
      [2, 3],
      [
        {
          id: 1,
          role: 'role1',
          role1Code: 'role1code',
          role2: 'role2',
          role2Code: 'role2code',
          default: true,
        },
      ],
    );
  });

  it('should forbid multiple requirements links  removal if default value is selected', () => {
    const page =
      AdminWorkspaceRequirementsLinksPage.initTestAtPageRequirementsLinks(requirementsLinks);

    // Select elements to delete
    page.grid.selectRows([1, 2, 3], '#', 'leftViewport');

    // Show dialog
    cy.get(selectByDataTestToolbarButtonId('delete-button')).click();

    // Confirm
    const alert = new RemoveDefaultRequirementsLinksDialogElement();
    alert.assertMessageMultipleDelete();
    alert.close();
  });
});

const requirementsLinks: RequirementVersionLinkType[] = [
  {
    id: 1,
    role: 'role1',
    role1Code: 'role1code',
    role2: 'role2',
    role2Code: 'role2code',
    default: true,
  },
  {
    id: 2,
    role: 'role3',
    role1Code: 'role3code',
    role2: 'role4',
    role2Code: 'role4code',
    default: false,
  },
  {
    id: 3,
    role: 'role5',
    role1Code: 'role5code',
    role2: 'role6',
    role2Code: 'role6code',
    default: false,
  },
];

const addRequirementsLinkResponse: RequirementVersionLinkType[] = [
  ...requirementsLinks,
  {
    id: 4,
    role: 'test1',
    role1Code: 'test1Code',
    role2: 'test2',
    role2Code: 'test2Code',
    default: false,
  },
];
