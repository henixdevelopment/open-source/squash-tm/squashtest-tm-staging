import { RemoveCustomFieldDialogElement } from '../../../../page-objects/pages/administration-workspace/dialogs/remove-custom-field-dialog.element';
import { AdminWorkspaceCustomFieldsPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-custom-fields.page';
import { assertAccessDenied } from '../../../../utils/assert-access-denied.utils';
import { AdminReferentialDataProviderBuilder } from '../../../../utils/referential/admin-referential-data.provider';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import { CustomFieldViewPage } from '../../../../page-objects/pages/administration-workspace/custom-field-view/custom-field-view.page';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { ConfirmDisableOptionalDropdown } from '../../../../page-objects/pages/administration-workspace/custom-field-view/dialogs/confirm-disable-optional-dropdown';
import { NavBarAdminElement } from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import { mockDataRow } from '../../../../data-mock/grid.data-mock';
import { CustomField } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/customfield.model';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';

describe('Administration workspace - Custom fields', function () {
  const initialNodes: GridResponse = {
    count: 2,
    dataRows: [
      mockDataRow({
        id: '1',
        children: [],
        data: {
          cfId: 1,
          name: 'CustomField1',
          label: 'CUF1',
          code: '123',
          inputType: 'PLAIN_TEXT',
          optional: true,
        },
      }),
      mockDataRow({
        id: '2',
        children: [],
        data: {
          cfId: 2,
          name: 'CustomField2',
          label: 'CUF2',
          code: '456',
          inputType: 'RICH_TEXT',
          optional: true,
        },
      }),
    ],
  };

  it('should forbid access to non-admin users', () => {
    assertAccessDenied('administration-workspace/projects');
  });

  it('should display the grid of custom fields', () => {
    const page = AdminWorkspaceCustomFieldsPage.initTestAtPageCustomFields(initialNodes);
    const grid = page.grid;

    grid.assertRowExist(1);
    const row = grid.getRow(1);
    row.cell('name').textRenderer().assertContainsText('CustomField1');
    row.cell('label').textRenderer().assertContainsText('CUF1');
    row.cell('code').textRenderer().assertContainsText('123');
    row.cell('inputType').textRenderer().assertContainsText('Texte simple');
    row.cell('optional').textRenderer().assertContainsText('Oui');
  });

  it('should allow custom field removal', () => {
    const page = AdminWorkspaceCustomFieldsPage.initTestAtPageCustomFields(initialNodes);

    page.grid.assertExists();

    // Show dialog
    const iconRenderer = page.grid.getRow(2, 'rightViewport').cell('delete').iconRenderer();
    iconRenderer.assertIsVisible();
    iconRenderer.click();

    // Confirm
    const dialog = new RemoveCustomFieldDialogElement([2]);
    dialog.deleteForSuccess({
      count: 1,
      dataRows: [initialNodes.dataRows[0]],
    });
  });

  it('should add custom fields', () => {
    const page = AdminWorkspaceCustomFieldsPage.initTestAtPageCustomFields(initialNodes);
    const httpMockView = new HttpMockBuilder('custom-field-view/3?*')
      .get()
      .responseBody({
        id: 3,
        name: 'CF',
        label: 'CF',
        code: 'CF',
        inputType: 'NUMERIC',
        optional: true,
        defaultValue: '123',
      })
      .build();

    const dialog = page.openCreateCustomField();
    dialog.assertExists();

    dialog.nameField.fill('CF');
    dialog.labelField.fill('CF');
    dialog.codeField.fill('CF');
    dialog.inputTypeField.selectValue('Numérique');
    dialog.defaultNumericField.fill('123');
    dialog.addWithOptions({
      addAnother: false,
      createResponse: { id: 3 },
      gridResponse: {
        count: initialNodes.count + 1,
        dataRows: [
          ...initialNodes.dataRows,
          mockDataRow({
            id: 3,
            children: [],
            data: {
              cfId: 3,
              name: 'CF',
              label: 'CF',
              code: 'CF',
              inputType: 'NUMERIC',
              optional: true,
              defaultValue: '123',
            },
          }),
        ],
      },
    });
    httpMockView.wait();
    const view = new CustomFieldViewPage();
    view.assertExists();
    view.entityNameField.checkContent('CF');
  });

  it('should add custom fields with dropdown list option', () => {
    const page = AdminWorkspaceCustomFieldsPage.initTestAtPageCustomFields(initialNodes);

    const dialog = page.openCreateCustomField();
    dialog.assertExists();

    dialog.nameField.fill('CF');
    dialog.labelField.fill('CF');
    dialog.codeField.fill('CF');
    dialog.inputTypeField.selectValue('Liste déroulante');
    cy.clickVoid();
    dialog.addDropdownListOption('option 1', 'code1');
    dialog.dropdownListOptionsGrid.assertRowCount(1);
    dialog.addWithOptions({
      addAnother: false,
      createResponse: { id: 3 },
      gridResponse: {
        count: initialNodes.count + 1,
        dataRows: [
          ...initialNodes.dataRows,
          mockDataRow({
            id: 3,
            children: [],
            data: {
              cfId: 3,
              name: 'CF',
              label: 'CF',
              code: 'CF',
              inputType: 'DROPDOWN_LIST',
              optional: true,
            },
          }),
        ],
      },
    });
  });

  it('should validate creation form', () => {
    const page = AdminWorkspaceCustomFieldsPage.initTestAtPageCustomFields(initialNodes);

    const dialog = page.openCreateCustomField();
    dialog.assertExists();

    dialog.nameField.fill('CF');
    dialog.labelField.fill('CF');
    dialog.codeField.fill('CF');

    // should forbid to add custom fields with dropdown list option if default value is required and not selected in grid
    dialog.inputTypeField.selectValue('Liste déroulante');
    cy.clickVoid();
    dialog.toggleOptionalCheck();
    dialog.addDropdownListOption('option 1', 'code1');
    dialog.dropdownListOptionsGrid.assertRowCount(1);
    dialog.clickOnAddButton();
    dialog.checkIfOptionDefaultValueRequiredErrorIsDisplayed();

    // should forbid to add a dropdown list option with an empty name
    dialog.addDropdownListOption('', 'code1');
    dialog.checkIfRequiredErrorMessageIsDisplayed();

    // should forbid to add a dropdown list option with an empty code
    dialog.addDropdownListOption('option 1', '');
    dialog.checkIfRequiredErrorMessageIsDisplayed();

    // should forbid to add a dropdown list option if name already exists
    dialog.addDropdownListOption('option 1', 'code1');
    dialog.addDropdownListOption('option 1', 'code2');
    dialog.checkIfOptionNameAlreadyExistErrorIsDisplayed();

    // should forbid to add a dropdown list option if code already exists
    dialog.addDropdownListOption('option 2', 'code1');
    dialog.checkIfOptionCodeAlreadyExistErrorIsDisplayed();

    // should forbid to add a dropdown list option if code has an invalid pattern
    dialog.addDropdownListOption('option 2', 'code 1 !');
    dialog.checkIfInvalidCodePatternErrorIsDisplayed();
  });

  it('should toggle option default value and be unique in grid', () => {
    const page = AdminWorkspaceCustomFieldsPage.initTestAtPageCustomFields(initialNodes);

    const dialog = page.openCreateCustomField();
    dialog.assertExists();

    dialog.nameField.fill('CF');
    dialog.labelField.fill('CF');
    dialog.codeField.fill('CF');
    dialog.inputTypeField.selectValue('Liste déroulante');
    cy.clickVoid();
    dialog.addDropdownListOption('option 1', 'code1');
    dialog.addDropdownListOption('option 2', 'code2');
    dialog.toggleOptionDefaultValue('option 1');
    dialog.dropdownListOptionsGrid
      .getRow('option 1')
      .cell('default')
      .checkBoxRender()
      .assertIsCheck();
    dialog.dropdownListOptionsGrid
      .getRow('option 2')
      .cell('default')
      .checkBoxRender()
      .assertIsNotCheck();
    dialog.toggleOptionDefaultValue('option 2');
    dialog.dropdownListOptionsGrid
      .getRow('option 1')
      .cell('default')
      .checkBoxRender()
      .assertIsNotCheck();
    dialog.dropdownListOptionsGrid
      .getRow('option 2')
      .cell('default')
      .checkBoxRender()
      .assertIsCheck();
  });

  it('should navigate to contextual view with URL and select in grid', () => {
    const { view, workspace } = visitContextualViewWithURL(
      initialNodes,
      mockAdminCustomFieldViewState(1),
    );
    view.assertExists();
    workspace.grid.getRow('1').assertIsSelected();
  });
});

function visitContextualViewWithURL(
  gridResponse: GridResponse,
  viewResponse: any,
): { workspace: AdminWorkspaceCustomFieldsPage; view: CustomFieldViewPage } {
  const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder().build();
  const gridElement = GridElement.createGridElement('customFields', 'custom-fields', gridResponse);
  const view = new CustomFieldViewPage();
  const viewMock = new HttpMockBuilder('custom-field-view/*').responseBody(viewResponse).build();

  // visit page
  cy.visit(
    `administration-workspace/entities-customization/custom-fields/${viewResponse.id}/content`,
  );
  adminReferentialDataProvider.wait();
  gridElement.waitInitialDataFetch();
  viewMock.waitResponseBody().then(() => {
    view.waitInitialDataFetch();
  });

  return {
    workspace: new AdminWorkspaceCustomFieldsPage(gridElement),
    view,
  };
}

function mockAdminCustomFieldViewState(id: number): any {
  return {
    id,
    code: 'ddl',
    inputType: 'DROPDOWN_LIST',
    name: 'ddl',
    label: 'ddl',
    defaultValue: null,
    largeDefaultValue: null,
    numericDefaultValue: null,
    optional: true,
    options: [
      { cfId: 1, label: 'c', position: 0, code: 'c', colour: null },
      { cfId: 1, label: 'b', position: 1, code: 'b', colour: null },
    ],
  };
}

describe('Administration Workspace - Custom fields - Information panel', function () {
  it('should set custom field basic attributes', () => {
    const initialNodes = getInitialNodes();
    const page = AdminWorkspaceCustomFieldsPage.initTestAtPageCustomFields(initialNodes);
    const rowIndex = 0;
    const cufPage = showCustomFieldView(rowIndex, page);
    cufPage.assertExists();

    cufPage.entityNameField.setAndConfirmValue('NEW');
    cufPage.entityNameField.checkContent('NEW');
    cufPage.informationPanel.labelTextField.setAndConfirmValue('New value');
    cufPage.informationPanel.labelTextField.checkContent('New value');
    cufPage.informationPanel.codeTextField.setAndConfirmValue('New code');
    cufPage.informationPanel.codeTextField.checkContent('New code');
    cufPage.informationPanel.optionalCheckbox.toggleState();
  });

  it('should set default values', () => {
    const initialNodes = getInitialNodes();
    const page = AdminWorkspaceCustomFieldsPage.initTestAtPageCustomFields(initialNodes);
    const numericCufPage = showCustomFieldView(0, page);
    numericCufPage.informationPanel.numericDefaultValue.setAndConfirmValue(43);
    numericCufPage.informationPanel.numericDefaultValue.checkContent(43);
    const textCufPage = showCustomFieldView(1, page);
    textCufPage.informationPanel.textDefaultValue.setAndConfirmValue('quarante-trois');
    textCufPage.informationPanel.textDefaultValue.checkContent('quarante-trois');
    const richTextCufPage = showCustomFieldView(2, page);
    richTextCufPage.informationPanel.richTextDefaultValue.setAndConfirmValue('quarante-trois');
    richTextCufPage.informationPanel.richTextDefaultValue.checkTextContent('quarante-trois');
    const tagCufPage = showCustomFieldView(3, page);
    tagCufPage.informationPanel.tagDefaultValue.addTag('tag4', {});
    tagCufPage.informationPanel.tagDefaultValue.checkTags('tag1', 'tag2', 'tag3', 'tag4');
    const dateCufPage = showCustomFieldView(4, page);
    dateCufPage.informationPanel.dateDefaultValue.enableEditMode();
    dateCufPage.informationPanel.dateDefaultValue.setToTodayAndConfirm();
    const checkboxCufPage = showCustomFieldView(5, page);
    checkboxCufPage.informationPanel.checkboxDefaultValue.setAndConfirmValueNoButton('Faux');
  });

  function showCustomFieldView(
    rowIndex: number,
    page: AdminWorkspaceCustomFieldsPage,
  ): CustomFieldViewPage {
    const initialNodes = getInitialNodes();
    return page.selectCustomFieldByName(
      initialNodes.dataRows[rowIndex].data.name,
      initialNodes.dataRows[rowIndex].data as CustomField,
    );
  }

  function getInitialNodes(): GridResponse {
    const numeric = getNumericCustomField(1);
    const text = getTextCustomField(2);
    const richText = getRichTextCustomField(3);
    const tags = getTagsCustomField(4);
    const date = getDateCustomField(5);
    const checkbox = getCheckboxCustomField(6);
    const dropdown = getDropdownCustomField(7);

    return {
      count: 7,
      dataRows: [numeric, text, richText, tags, date, checkbox, dropdown].map((cuf) =>
        mockDataRow({
          id: cuf.id.toString(),
          children: [],
          data: cuf,
          allowMoves: true,
          allowedChildren: [],
          type: 'Generic',
        }),
      ),
    };
  }

  function getNumericCustomField(id: number): CustomField {
    return {
      id,
      name: 'CUF' + id,
      label: 'CUF' + id,
      code: 'CUF' + id,
      inputType: InputType.NUMERIC,
      numericDefaultValue: 42,
      optional: false,
      defaultValue: '',
      largeDefaultValue: '',
      options: null,
      boundProjectsToCuf: [],
    };
  }

  function getTextCustomField(id: number): CustomField {
    return {
      id,
      name: 'CUF' + id,
      label: 'CUF' + id,
      code: 'CUF' + id,
      inputType: InputType.PLAIN_TEXT,
      numericDefaultValue: null,
      optional: false,
      defaultValue: 'Hello, World!',
      largeDefaultValue: '',
      options: null,
      boundProjectsToCuf: [],
    };
  }

  function getRichTextCustomField(id: number): CustomField {
    return {
      id,
      name: 'CUF' + id,
      label: 'CUF' + id,
      code: 'CUF' + id,
      inputType: InputType.RICH_TEXT,
      numericDefaultValue: null,
      optional: false,
      defaultValue: '',
      largeDefaultValue: '<p>Hello, <b>World!</b></p>',
      options: null,
      boundProjectsToCuf: [],
    };
  }

  function getDateCustomField(id: number): CustomField {
    return {
      id,
      name: 'CUF' + id,
      label: 'CUF' + id,
      code: 'CUF' + id,
      inputType: InputType.DATE_PICKER,
      numericDefaultValue: null,
      optional: false,
      defaultValue: '2012-12-20',
      largeDefaultValue: '',
      options: null,
      boundProjectsToCuf: [],
    };
  }

  function getCheckboxCustomField(id: number): CustomField {
    return {
      id,
      name: 'CUF' + id,
      label: 'CUF' + id,
      code: 'CUF' + id,
      inputType: InputType.CHECKBOX,
      numericDefaultValue: null,
      optional: false,
      defaultValue: 'true',
      largeDefaultValue: '',
      options: null,
      boundProjectsToCuf: [],
    };
  }

  function getTagsCustomField(id: number): CustomField {
    return {
      id,
      name: 'CUF' + id,
      label: 'CUF' + id,
      code: 'CUF' + id,
      inputType: InputType.TAG,
      numericDefaultValue: null,
      optional: false,
      defaultValue: 'tag1|tag2|tag3',
      largeDefaultValue: '',
      options: null,
      boundProjectsToCuf: [],
    };
  }
});

describe('Administration Workspace - Custom fields - Options panel', function () {
  it('should forbid removing the default option', () => {
    const cufPage = openCustomFieldView();
    const optionsPanel = cufPage.optionsPanel;
    optionsPanel.grid.assertRowCount(3);

    optionsPanel.assertDeleteIconIsHidden('op1');
    optionsPanel.toggleDefaultOption('op1');
    optionsPanel.assertDeleteIconIsVisible('op1');
  });

  it('should reorder options with drag and drop', () => {
    const cufPage = openCustomFieldView();
    const optionsPanel = cufPage.optionsPanel;

    const updatedCuf: CustomField = {
      ...getDropdownCustomField(7),
      options: [
        { label: 'op2', code: 'op2', cfId: 7, colour: null, position: 0 },
        { label: 'op3', code: 'op3', cfId: 7, colour: null, position: 1 },
        { label: 'op1', code: 'op1', cfId: 7, colour: null, position: 2 },
      ],
    };

    optionsPanel.moveOption('op1', 'op3', updatedCuf);
    optionsPanel.grid.assertRowCount(3);
  });

  it('should warn user when changing default option', () => {
    const cufPage = openCustomFieldView();

    cufPage.informationPanel.optionalCheckbox.click();
    const confirmDialog = new ConfirmDisableOptionalDropdown();
    confirmDialog.assertMessage('op1');
  });

  it('should add an option', () => {
    const cufPage = openCustomFieldView();
    const cuf = getDropdownCustomField(7);

    const addDialog = cufPage.optionsPanel.openAddOptionDialog();
    addDialog.nameField.fill('New option');
    addDialog.codeField.fill('NEW');
    addDialog.addOption({
      ...cuf,
      options: [
        ...cuf.options,
        {
          cfId: 7,
          label: 'New option',
          code: 'NEW',
          colour: null,
          position: 4,
        },
      ],
    });

    cufPage.optionsPanel.grid.assertRowCount(4);
  });

  function openCustomFieldView(): CustomFieldViewPage {
    const initialNodes = getInitialNodes();
    const wsPage = AdminWorkspaceCustomFieldsPage.initTestAtPageCustomFields(initialNodes);
    const cufPage = wsPage.selectCustomFieldByName(
      initialNodes.dataRows[0].data.name,
      initialNodes.dataRows[0].data as CustomField,
    );

    cufPage.assertExists();
    new NavBarAdminElement().toggle();
    cufPage.foldGrid();
    return cufPage;
  }

  function getInitialNodes(): GridResponse {
    const dropdown = getDropdownCustomField(7);

    return {
      count: 1,
      dataRows: [
        mockDataRow({
          id: dropdown.id.toString(),
          data: dropdown,
        }),
      ],
    };
  }
});

describe('Administration Workspace - Custom fields - Bound projects panel', function () {
  it('should display projects bound to cuf panel', () => {
    const initialNodes = getInitialNodes();
    const customField = initialNodes.dataRows[0].data as CustomField;
    const wsPage = AdminWorkspaceCustomFieldsPage.initTestAtPageCustomFields(initialNodes);
    const cufPage = wsPage.selectCustomFieldByName(customField.name, customField);
    cufPage.boundProjectsPanel.assertProjectNameIs(
      customField.boundProjectsToCuf[0].projectName.toString(),
    );
    cufPage.boundProjectsPanel.assertEntityTypeIs('Itérations');
  });

  function getInitialNodes(): GridResponse {
    const numeric = getNumericCustomField(1);

    return {
      count: 1,
      dataRows: [numeric].map((cuf) =>
        mockDataRow({
          id: cuf.id.toString(),
          children: [],
          data: cuf,
          allowMoves: true,
          allowedChildren: [],
          type: 'Generic',
        }),
      ),
    };
  }

  function getNumericCustomField(id: number): CustomField {
    return {
      id,
      name: 'CUF' + id,
      label: 'CUF' + id,
      code: 'CUF' + id,
      inputType: InputType.NUMERIC,
      numericDefaultValue: 42,
      optional: false,
      defaultValue: '',
      largeDefaultValue: '',
      options: null,
      boundProjectsToCuf: [
        {
          projectId: 1,
          projectName: 'project1',
          bindableEntity: BindableEntity.ITERATION,
          customFieldBindingId: 1,
        },
      ],
    };
  }
});

function getDropdownCustomField(id: number): CustomField {
  return {
    id,
    name: 'CUF' + id,
    label: 'CUF' + id,
    code: 'CUF' + id,
    inputType: InputType.DROPDOWN_LIST,
    numericDefaultValue: null,
    optional: true,
    defaultValue: 'op1',
    largeDefaultValue: '',
    options: [
      { label: 'op1', code: 'op1', cfId: id, colour: null, position: 0 },
      { label: 'op2', code: 'op2', cfId: id, colour: null, position: 1 },
      { label: 'op3', code: 'op3', cfId: id, colour: null, position: 2 },
    ],
    boundProjectsToCuf: [],
  };
}
