import { RemoveBugtrackerDialogElement } from '../../../../page-objects/pages/administration-workspace/dialogs/remove-bugtracker-dialog.element';
import { CannotRemoveBugtrackerAlert } from '../../../../page-objects/pages/administration-workspace/dialogs/cannot-remove-bugtracker-alert.element';
import { AdminWorkspaceBugtrackersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-bugtrackers.page';
import { CannotRemoveBugtrackersAlert } from '../../../../page-objects/pages/administration-workspace/dialogs/cannot-remove-bugtrackers-alert.element';
import { assertAccessDenied } from '../../../../utils/assert-access-denied.utils';
import { selectByDataTestToolbarButtonId } from '../../../../utils/basic-selectors';
import { mockDataRow, mockGridResponse } from '../../../../data-mock/grid.data-mock';
import { mockFieldValidationError } from '../../../../data-mock/http-errors.data-mock';
import { BugTrackerViewPage } from '../../../../page-objects/pages/administration-workspace/bug-tracker-view/bug-tracker-view.page';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import {
  AuthenticationPolicy,
  AuthenticationProtocol,
} from '../../../../../../projects/sqtm-core/src/lib/model/third-party-server/authentication.model';
import { Credentials } from '../../../../../../projects/sqtm-core/src/lib/model/third-party-server/credentials.model';
import { AdminBugTrackerModel } from '../../../../../../projects/sqtm-core/src/lib/model/bugtracker/admin-bug-tracker.model';
import { DataRowModel } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

describe('Administration Workspace - Bugtrackers', function () {
  it('should forbid access to non-admin users', () => {
    assertAccessDenied('administration-workspace/projects');
  });

  it('should display bugtrackers grid', () => {
    const initialNodes = mockBTGridResponse(getInitialDataRows());
    const page = AdminWorkspaceBugtrackersPage.initTestAtPageBugtrackers(initialNodes);
    const grid = page.grid;

    const firstRow = grid.getRow(1);
    firstRow.cell('name').textRenderer().assertContainsText('BT01');
    firstRow.cell('kind').textRenderer().assertContainsText('jira.rest');
    firstRow.cell('url').linkRenderer().assertContainText('http://localhost:1392');
    firstRow.cell('synchronisationCount').findCell().contains(0);
    grid.getRow(2).assertExists();
    grid.getRow(3).assertExists();
  });

  it('should add bugtrackers', () => {
    const initialNodes = mockBTGridResponse(getInitialDataRows());
    const page = AdminWorkspaceBugtrackersPage.initTestAtPageBugtrackers(initialNodes);
    const httpMockView = new HttpMockBuilder('bugtracker-view/4?*')
      .get()
      .responseBody({
        id: 4,
        name: 'BT05',
        kind: 'Mantis',
        url: 'http://localhost:9090',
        authProtocol: AuthenticationProtocol.BASIC_AUTH,
        authPolicy: AuthenticationPolicy.USER,
        supportedAuthenticationProtocols: [AuthenticationProtocol.BASIC_AUTH],
        iframeFriendly: true,
        bugTrackerKinds: ['jira.cloud', 'jira.server', 'mantis'],
      })
      .build();

    const addDialog = page.openCreateBugtracker();
    addDialog.assertExists();
    addDialog.fillName('BT05');
    addDialog.selectKind('Mantis');
    addDialog.fillUrl('http://localhost:9090');

    addDialog.addWithOptions({
      addAnother: false,
      createResponse: { id: 4 },
      gridResponse: mockBTGridResponse([
        ...getInitialDataRows(),
        mockDataRow({ data: { serverId: 4 } }),
      ]),
    });

    httpMockView.wait();
    const view = new BugTrackerViewPage();
    view.entityNameField.checkContent('BT05');

    page.grid.assertRowExist(4);

    // Add another
    const addAnotherDialog = page.openCreateBugtracker();
    const httpMockViewAnother = new HttpMockBuilder('bugtracker-view/5?*')
      .get()
      .responseBody({
        id: 5,
        name: 'b',
        kind: 'Mantis',
        url: 'h',
        authProtocol: AuthenticationProtocol.BASIC_AUTH,
        authPolicy: AuthenticationPolicy.USER,
        supportedAuthenticationProtocols: [AuthenticationProtocol.BASIC_AUTH],
        iframeFriendly: true,
        bugTrackerKinds: ['jira.cloud', 'jira.server', 'mantis'],
      })
      .build();
    addAnotherDialog.assertExists();
    addAnotherDialog.fillName('b');
    addAnotherDialog.selectKind('Mantis');
    addAnotherDialog.fillUrl('h');

    addAnotherDialog.addWithOptions({
      addAnother: true,
      createResponse: { id: 5 },
      gridResponse: mockBTGridResponse([]), // already tested above
    });
    httpMockViewAnother.wait();

    addAnotherDialog.assertExists();
    addAnotherDialog.checkIfFormIsEmpty();
    addAnotherDialog.cancel();
    addAnotherDialog.assertNotExist();
  });

  it('should validate creation form', () => {
    const initialNodes = mockBTGridResponse(getInitialDataRows());
    const page = AdminWorkspaceBugtrackersPage.initTestAtPageBugtrackers(initialNodes);

    const dialog = page.openCreateBugtracker();
    dialog.assertExists();

    // should forbid to add a bugtracker if name already exists
    const nameHttpError = mockFieldValidationError(
      'name',
      'sqtm-core.error.generic.name-already-in-use',
    );
    dialog.fillName('b');
    dialog.selectKind('Mantis');
    dialog.fillUrl('h');
    dialog.addWithServerSideFailure(nameHttpError);
    dialog.assertExists();
    dialog.checkIfNameAlreadyInUseErrorMessageIsDisplayed();

    // should forbid to add a bugtracker with an empty name
    dialog.clearForm();
    dialog.selectKind('Mantis');
    dialog.fillUrl('u');
    dialog.clickOnAddButton();
    dialog.checkIfRequiredErrorMessageIsDisplayed();

    // should forbid to add a bugtracker with an empty url
    dialog.clearForm();
    dialog.fillName('b');
    dialog.selectKind('Mantis');
    dialog.fillUrl('');
    dialog.clickOnAddButton();
    dialog.checkIfRequiredErrorMessageIsDisplayed();

    // should forbid to add a bugtracker with a malformed url
    const urlHttpError = mockFieldValidationError('url', 'sqtm-core.exception.wrong-url');
    dialog.clearForm();
    dialog.fillName('BT05');
    dialog.selectKind('Mantis');
    dialog.fillUrl('localhost');
    dialog.addWithServerSideFailure(urlHttpError);
    dialog.assertExists();
    dialog.checkIfMalformedUrlErrorMessageIsDisplayed();
  });

  it('should allow removal of bugtracker from the grid', () => {
    const initialNodes = mockBTGridResponse(getInitialDataRows());
    const page = AdminWorkspaceBugtrackersPage.initTestAtPageBugtrackers(initialNodes);

    const firstRow = page.grid.getRow(1, 'rightViewport');
    firstRow.cell('delete').assertExists();
    firstRow.cell('delete').iconRenderer().click();
    const dialog = new RemoveBugtrackerDialogElement([1]);
    dialog.assertMessageSingular();
    dialog.deleteForSuccess({
      count: 2,
      dataRows: [initialNodes.dataRows[1], initialNodes.dataRows[2]],
    });
  });

  it('should forbid removal of bugtracker used by synchronizations', () => {
    const initialNodes = mockBTGridResponse(getInitialDataRows());
    const page = AdminWorkspaceBugtrackersPage.initTestAtPageBugtrackers(initialNodes);

    const firstRow = page.grid.getRow(3, 'rightViewport');
    firstRow.cell('delete').assertExists();
    firstRow.cell('delete').iconRenderer().click();
    const alert = new CannotRemoveBugtrackerAlert();
    alert.assertMessage();
    alert.close();
  });

  it('should allow multiple bugtrackers removal', () => {
    const initialNodes = mockBTGridResponse(getInitialDataRows());
    const page = AdminWorkspaceBugtrackersPage.initTestAtPageBugtrackers(initialNodes);

    // Select elements to delete
    page.grid.selectRows([1, 2], '#', 'leftViewport');

    // Show dialog
    cy.get(selectByDataTestToolbarButtonId('delete-button')).click();

    // Confirm
    const dialog = new RemoveBugtrackerDialogElement([1, 2]);
    dialog.deleteForSuccess({
      count: 1,
      dataRows: [initialNodes.dataRows[2]],
    });
  });

  it('should forbid multiple bugtracker removal used by synchronizations', () => {
    const initialNodes = mockBTGridResponse(getInitialDataRows());
    const page = AdminWorkspaceBugtrackersPage.initTestAtPageBugtrackers(initialNodes);

    // Select elements to delete
    page.grid.selectRows([1, 2, 3], '#', 'leftViewport');

    // Show dialog
    cy.get(selectByDataTestToolbarButtonId('delete-button')).click();

    const alert = new CannotRemoveBugtrackersAlert();
    alert.assertMessage();
    alert.close();
  });

  function getInitialDataRows(): DataRowModel[] {
    return [
      mockDataRow({
        data: {
          serverId: 1,
          name: 'BT01',
          kind: 'jira.rest',
          url: 'http://localhost:1392',
          synchronisationCount: 0,
        },
      }),
      mockDataRow({
        data: {
          serverId: 2,
          name: 'BT02',
          kind: 'jira.rest',
          url: 'http://localhost:1393',
          synchronisationCount: 0,
        },
      }),
      mockDataRow({
        data: {
          serverId: 3,
          name: 'BT03',
          kind: 'jira.cloud',
          url: 'http://plugin01.atlassian.com/',
          synchronisationCount: 3,
        },
      }),
    ];
  }

  function mockBTGridResponse(dataRows: DataRowModel[]): GridResponse {
    return mockGridResponse('serverId', dataRows);
  }
});

describe('Administration Workspace - Bug tracker - Information panel', function () {
  it('should set bugtracker name', () => {
    const page = showBugTrackerView();

    page.entityNameField.setAndConfirmValue('NEW');

    page.entityNameField.checkContent('NEW');
  });

  it('should set bugtracker kind', () => {
    const page = showBugTrackerView();

    const updatedViewResponse = {
      id: 1,
      authProtocol: AuthenticationProtocol.BASIC_AUTH,
      authPolicy: AuthenticationPolicy.USER,
      kind: 'jira.server',
      supportedAuthenticationProtocols: [AuthenticationProtocol.BASIC_AUTH],
      url: 'http://some.url',
      name: 'BT1',
      iframeFriendly: true,
      bugTrackerKinds: ['jira.cloud', 'jira.server', 'mantis'],
    };

    const mock = new HttpMockBuilder('bugtracker-view/*').responseBody(updatedViewResponse).build();

    page.informationPanel.kindField.setAndConfirmValueNoButton('jira.server');

    mock.wait();

    page.informationPanel.kindField.checkSelectedOption('jira.server');
  });

  it('should set bugtracker url', () => {
    const page = showBugTrackerView();

    page.informationPanel.urlField.setAndConfirmValue('http://blabla.blabla');

    page.informationPanel.urlField.checkContent('http://blabla.blabla');
  });

  it('should have description', () => {
    const page = showBugTrackerView();

    page.informationPanel.descriptionField.checkTextContent(
      'Yesterday, I fixed the bug that I caused myself',
    );
  });
});

describe('Administration Workspace - Bug tracker - Auth policy panel', function () {
  it('should see Oauth2 buttons with no credentials', () => {
    const page = showBugTrackerView();
    const mock = mockOAuth2DefaultFormValues();
    page.authProtocolPanel.protocolField.setAndConfirmValueNoButton('OAuth 2');
    mock.wait();
    page.foldAuthenticationProtocolPanel();
    page.authPolicyPanel.assertGetTokenOauth2ButtonVisible();
    page.authPolicyPanel.assertGetTokenOauth2ButtonEnabled();
    page.authPolicyPanel.assertRevokeOauth2ButtonVisible();
    page.authPolicyPanel.assertRevokeOauth2ButtonDisabled();
  });

  it('should see Oauth2 buttons with credentials', () => {
    const credentials: Credentials = {
      accessToken: 'token',
      registered: true,
      refreshToken: 'refresh',
      implementedProtocol: AuthenticationProtocol.OAUTH_2,
      type: AuthenticationProtocol.OAUTH_2,
    };

    const page = showBugTrackerView(credentials);
    const mock = mockOAuth2DefaultFormValues();
    page.authProtocolPanel.protocolField.setAndConfirmValueNoButton('OAuth 2');
    mock.wait();
    page.foldAuthenticationProtocolPanel();
    page.authPolicyPanel.assertGetTokenOauth2ButtonVisible();
    page.authPolicyPanel.assertGetTokenOauth2ButtonDisabled();
    page.authPolicyPanel.assertRevokeOauth2ButtonVisible();
    page.authPolicyPanel.assertRevokeOauth2ButtonEnabled();
  });

  it('should send credential form', () => {
    const page = showBugTrackerView();

    page.authPolicyPanel.assertSendButtonDisabled();
    page.authPolicyPanel.usernameField.fill('username');
    page.authPolicyPanel.passwordField.fill('password');
    page.authPolicyPanel.assertSendButtonEnabled();

    page.authPolicyPanel.sendCredentialsForm();
    page.authPolicyPanel.assertSaveSuccessMessageVisible();
  });

  it('should warn user about server errors', () => {
    const page = showBugTrackerView();

    page.authPolicyPanel.assertSendButtonDisabled();
    page.authPolicyPanel.usernameField.fill('username');
    page.authPolicyPanel.passwordField.fill('password');
    page.authPolicyPanel.assertSendButtonEnabled();

    const errorMessage = '3RR0R';
    page.authPolicyPanel.sendCredentialsFormWithServerError(errorMessage);
    page.authPolicyPanel.assertServerErrorMessageVisible(errorMessage);
  });
});

describe('Administration Workspace - Bug tracker - Auth protocol panel', function () {
  it('should set bugtracker OAuth2 protocol', () => {
    const page = showBugTrackerView();
    const mock = mockOAuth2DefaultFormValues();
    page.authProtocolPanel.protocolField.checkSelectedOption('basic authentication');
    page.authProtocolPanel.assertOAuth2FormNotVisible();
    page.authProtocolPanel.protocolField.setAndConfirmValueNoButton('OAuth 2');
    mock.wait();
    page.authProtocolPanel.assertOAuth2FormVisible();
  });

  it('should send OAuth2 conf form', () => {
    const page = showBugTrackerView();
    const mock = mockOAuth2DefaultFormValues();
    page.authProtocolPanel.protocolField.setAndConfirmValueNoButton('OAuth 2');
    mock.wait();
    page.authProtocolPanel.assertSaveOauth2ButtonDisabled();
    page.authProtocolPanel.assertStatusMessageNotVisible();
    page.authProtocolPanel.clientIdField.fill('some-key');
    page.authProtocolPanel.oauth2SecretField.fill('some-secret');
    page.authProtocolPanel.assertSaveOauth2ButtonEnabled();
    page.authProtocolPanel.assertUnsavedChangesMessageVisible();
    page.authProtocolPanel.sendOAuth2Form();
    page.authProtocolPanel.assertSaveSuccessMessageVisible();
  });
});

function showBugTrackerView(credentials?: Credentials): BugTrackerViewPage {
  const initialNodes = getInitialNodes(credentials);
  const page = AdminWorkspaceBugtrackersPage.initTestAtPageBugtrackers(initialNodes);
  return page.selectBugTrackerByName(
    [initialNodes.dataRows[0].data.name],
    initialNodes.dataRows[0].data as AdminBugTrackerModel,
  );
}

function getInitialNodes(credentials?: Credentials): GridResponse {
  const bugtracker = getInitialModel(credentials);

  return {
    count: 1,
    dataRows: [
      mockDataRow({
        id: bugtracker.id.toString(),
        children: [],
        data: bugtracker,
        allowMoves: true,
        allowedChildren: [],
        type: 'Generic',
      }),
    ],
  };
}

function getInitialModel(credentials?: Credentials): AdminBugTrackerModel {
  return {
    id: 1,
    authProtocol: AuthenticationProtocol.BASIC_AUTH,
    authPolicy: AuthenticationPolicy.USER,
    kind: 'jira.cloud',
    supportedAuthenticationProtocols: [
      AuthenticationProtocol.BASIC_AUTH,
      AuthenticationProtocol.OAUTH_2,
    ],
    url: 'http://some.url',
    name: 'BT1',
    iframeFriendly: true,
    bugTrackerKinds: ['jira.cloud', 'jira.server', 'mantis', 'jira.xsquash'],
    credentials,
    createdBy: 'admin',
    createdOn: '2024-05-22',
    description: 'Yesterday, I fixed the bug that I caused myself',
  };
}

function mockOAuth2DefaultFormValues() {
  const defaultFormValues = {
    authorizationUrl: 'https://plugin01.atlassian.net/rest/oauth2/latest/authorize',
    requestTokenUrl: 'https://plugin01.atlassian.net/rest/oauth2/latest/token',
    scope: 'WRITE',
  };
  return new HttpMockBuilder('bugtracker-view/*/oauth2/default-form-values')
    .responseBody(defaultFormValues)
    .build();
}
