import { assertAccessDenied } from '../../../../utils/assert-access-denied.utils';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { AdminWorkspaceAiServersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-ai-servers.page';
import { RemoveAiServerDialogElement } from '../../../../page-objects/pages/administration-workspace/dialogs/remove-ai-server-dialog.element';
import { AiServerViewPage } from '../../../../page-objects/pages/administration-workspace/ai-server-view/ai-server-view.page';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import { Credentials } from '../../../../../../projects/sqtm-core/src/lib/model/third-party-server/credentials.model';
import {
  AuthenticationPolicy,
  AuthenticationProtocol,
} from '../../../../../../projects/sqtm-core/src/lib/model/third-party-server/authentication.model';
import { AdminAiServer } from '../../../../../../projects/sqtm-core/src/lib/model/artificial-intelligence/ai-server.model';
import { mockDataRow } from '../../../../data-mock/grid.data-mock';

describe('Administration workspace - AI Servers', function () {
  const initialNodes: GridResponse = {
    count: 3,
    dataRows: [
      mockDataRow({
        id: 1,
        children: [],
        data: {
          serverId: 1,
          name: 'Terminator',
          url: 'https://universal.com',
          description: 'Jurassic park is a better movie than Terminator',
          createdBy: 'Steven Spielberg',
          createdOn: new Date().toISOString(),
          lastModifiedBy: null,
          lastModifiedOn: null,
        },
      }),
      mockDataRow({
        id: 2,
        children: [],
        data: {
          serverId: 2,
          name: 'Wall-E',
          url: 'https://disney.com',
          description: 'Eva? EVAAAAAA!!!!',
          createdBy: 'Pixar',
          createdOn: new Date().toISOString(),
          lastModifiedBy: null,
          lastModifiedOn: null,
        },
      }),
      mockDataRow({
        id: 3,
        children: [],
        data: {
          serverId: 3,
          name: 'Star Wars',
          url: 'https://jedi.com',
          description: '-Hello there. -General Kenobi',
          createdBy: 'Georges Lucas',
          createdOn: new Date().toISOString(),
          lastModifiedBy: null,
          lastModifiedOn: null,
        },
      }),
    ],
  };

  it('should forbid access to non-admin users', () => {
    assertAccessDenied('administration-workspace/servers/ai-server');
  });

  it('should display AI servers grid', () => {
    const page = AdminWorkspaceAiServersPage.initTestAtPageTestAiServers(initialNodes);
    const grid = page.grid;

    const firstRow = grid.getRow(1);
    firstRow.cell('name').textRenderer().assertContainsText('Terminator');
    firstRow.cell('url').linkRenderer().assertContainText('https://universal.com');
    firstRow
      .cell('description')
      .textRenderer()
      .assertContainsText('Jurassic park is a better movie than Terminator');

    const secondRow = grid.getRow(2);
    secondRow.cell('name').textRenderer().assertContainsText('Wall-E');
    secondRow.cell('url').linkRenderer().assertContainText('https://disney.com');
    secondRow.cell('description').textRenderer().assertContainsText('Eva? EVAAAAAA!!!!');

    const thirdRow = grid.getRow(3);
    thirdRow.cell('name').textRenderer().assertContainsText('Star Wars');
    thirdRow.cell('url').linkRenderer().assertContainText('https://jedi.com');
    thirdRow.cell('description').textRenderer().assertContainsText('-Hello there. -General Kenobi');
  });

  it('should add several AI servers in a row and check grid content', () => {
    const page = AdminWorkspaceAiServersPage.initTestAtPageTestAiServers(initialNodes);
    const { httpMockView1, httpMockView2 } = buildNewServersMockViews();
    const server1 = {
      name: 'Neo',
      description: 'First movie is always the best',
      url: 'https://matrix.com',
    };
    const server2 = { name: 'No inspiration', description: 'Nothing', url: 'https://no-idea.com' };

    const addDialog = page.openCreateAiServer();
    addDialog.assertExists();
    addDialog.fillName(server1.name);
    addDialog.fillDescription(server1.description);
    addDialog.fillUrl(server1.url);
    addDialog.addWithOptions({
      addAnother: true,
      createResponse: { id: 4 },
      gridResponse: {
        count: 4,
        dataRows: [
          ...initialNodes.dataRows,
          mockNewDataRow(4, server1.name, server1.description, server1.url),
        ],
      },
    });

    httpMockView1.wait();
    const view = new AiServerViewPage();
    view.entityNameField.checkContent(server1.name);
    view.informationPanel.assertExists();
    view.informationPanel.checkInfo(server1.url, server1.description);

    addDialog.assertExists();
    addDialog.checkIfFormIsEmpty();
    addDialog.fillName(server2.name);
    addDialog.fillDescription(server2.description);
    addDialog.fillUrl(server2.url);
    addDialog.addWithOptions({
      addAnother: true,
      createResponse: { id: 5 },
      gridResponse: {
        count: 5,
        dataRows: [
          ...initialNodes.dataRows,
          mockNewDataRow(4, server1.name, server1.description, server1.url),
          mockNewDataRow(5, server2.name, server2.description, server2.url),
        ],
      },
    });

    httpMockView2.wait();
    const view2 = new AiServerViewPage();
    view2.entityNameField.checkContent(server2.name);
    view2.informationPanel.assertExists();
    view2.informationPanel.checkInfo(server2.url, server2.description);

    addDialog.cancel();
    addDialog.assertNotExist();
    checkNewElementsInGrid(page.grid);
  });

  it('should suppress AI servers', () => {
    const page = AdminWorkspaceAiServersPage.initTestAtPageTestAiServers(initialNodes);
    const grid = page.grid;
    const thirdRow = grid.getRow(3, 'rightViewport');
    thirdRow.cell('delete').iconRenderer().click();
    const dialog = new RemoveAiServerDialogElement([3]);
    dialog.assertMessageSingular('Star Wars');

    dialog.deleteForSuccess({
      count: 1,
      dataRows: [initialNodes.dataRows[0], initialNodes.dataRows[1]],
    });

    grid.assertRowExist('1');
    grid.assertRowExist('2');
  });

  function buildNewServersMockViews() {
    const httpMockView1 = new HttpMockBuilder('ai-server/4?*')
      .get()
      .responseBody({
        id: 5,
        url: 'https://matrix.com',
        name: 'Neo',
        description: 'First movie is always the best',
        payloadTemplate: '',
        createdOn: new Date().toISOString(),
        createdBy: 'admin',
      })
      .build();

    const httpMockView2 = new HttpMockBuilder('ai-server/5?*')
      .get()
      .responseBody({
        id: 5,
        url: 'https://no-idea.com',
        name: 'No inspiration',
        description: 'Nothing',
        payloadTemplate: '',
        createdOn: new Date().toISOString(),
        createdBy: 'admin',
      })
      .build();
    return { httpMockView1, httpMockView2 };
  }

  function mockNewDataRow(id: number, name: string, description: string, url: string) {
    return mockDataRow({
      id: id,
      children: [],
      data: {
        name: name,
        description: description,
        url: url,
      },
    });
  }

  function checkNewElementsInGrid(grid: GridElement) {
    grid.assertRowCount(5);

    const fourthRow = grid.getRow(4);
    fourthRow.cell('name').textRenderer().assertContainsText('Neo');
    fourthRow.cell('url').linkRenderer().assertContainText('https://matrix.com');
    fourthRow
      .cell('description')
      .textRenderer()
      .assertContainsText('First movie is always the best');

    const fifthRow = grid.getRow(5);
    fifthRow.cell('name').textRenderer().assertContainsText('No inspiration');
    fifthRow.cell('url').linkRenderer().assertContainText('https://no-idea.com');
    fifthRow.cell('description').textRenderer().assertContainsText('Nothing');
  }
});

describe('Administration workspace - AI Servers - Information panel', function () {
  it('should set AI server name', () => {
    const page = showAiServerView();

    page.informationPanel.assertModification(false);
    page.entityNameField.setAndConfirmValue('NEW');
    page.entityNameField.checkContent('NEW');
    page.informationPanel.assertModification(true);
  });

  it('should update information', () => {
    const page = showAiServerView();

    page.informationPanel.assertModification(false);
    page.informationPanel.updateUrl('https://new-url.com');
    const response = {
      ...getInitialModel(),
      description: 'A new description',
    };
    page.informationPanel.assertModification(true);

    page.informationPanel.updateDescription('A new description', response);
  });
});

describe('Administration workspace - AI Servers - Authentication panel', function () {
  it('should update token', () => {
    const page = showAiServerView();
    page.authenticationPanel.assertNoTokenInfo();

    const newToken = 'abc123';
    const response = {
      implementedProtocol: AuthenticationProtocol.TOKEN_AUTH,
      type: AuthenticationProtocol.TOKEN_AUTH,
      token: newToken,
      registered: true,
    } as Credentials;

    page.informationPanel.assertModification(false);
    page.authenticationPanel.updateToken(newToken, response);
    page.authenticationPanel.assertSuccess('Le jeton a bien été sauvegardé');
    page.informationPanel.assertModification(true);
  });

  it('should remove token', () => {
    const page = showAiServerView({
      implementedProtocol: AuthenticationProtocol.TOKEN_AUTH,
      type: AuthenticationProtocol.TOKEN_AUTH,
      token: 'xyz',
      registered: true,
    });

    page.informationPanel.assertModification(false);
    page.authenticationPanel.deleteToken();
    page.authenticationPanel.assertSuccess('Le jeton a bien été supprimé.');
    page.informationPanel.assertModification(true);
  });
});

describe('Administration workspace - AI Servers - Configuration panel', function () {
  it('should update json path', () => {
    const page = showAiServerView();
    page.configurationPanel.jsonField.assertExists();
    page.configurationPanel.jsonField.checkContent('This does not follow back rules');
    page.configurationPanel.jsonField.click();
    page.configurationPanel.updateJsonPath(', but I can update!');
    page.configurationPanel.jsonField.checkContent(
      'This does not follow back rules, but I can update!',
    );
  });

  it('should delete json path', () => {
    const page = showAiServerView();
    page.configurationPanel.jsonField.click();
    page.configurationPanel.deleteJsonPath();
    page.configurationPanel.jsonField.checkPlaceholder();
  });
});

describe('Administration workspace - AI servers - Configuration test panel', function () {
  it('should not be able to test configuration button if some field is empty', () => {
    const page = showAiServerView();
    page.testConfigurationPanel.assertExists();
    page.testConfigurationPanel.assertTestConfigurationButtonDisabled();
    page.configurationPanel.modelConfigurationField.click();
    page.configurationPanel.modelConfigurationField.setAndConfirm('A model');
    page.testConfigurationPanel.assertTestConfigurationButtonDisabled();

    page.testConfigurationPanel.requirementModelText.click();
    page.testConfigurationPanel.requirementModelText.setValue('A requirement');
    page.testConfigurationPanel.requirementModelText.blur();
    page.testConfigurationPanel.assertTestConfigurationButtonActive();

    page.configurationPanel.jsonField.click();
    page.configurationPanel.deleteJsonPath();
    page.testConfigurationPanel.assertTestConfigurationButtonDisabled();
  });
});

function showAiServerView(credentials?: Credentials): AiServerViewPage {
  const initialNodes = getInitialNodes(credentials);
  const page = AdminWorkspaceAiServersPage.initTestAtPageTestAiServers(initialNodes);
  return page.selectAiServerByName(
    initialNodes.dataRows[0].data.name,
    initialNodes.dataRows[0].data as AdminAiServer,
  );
}

function getInitialNodes(credentials?: Credentials): GridResponse {
  const aiServer = getInitialModel(credentials);
  return {
    count: 1,
    dataRows: [
      mockDataRow({
        id: aiServer.id.toString(),
        children: [],
        data: aiServer,
      }),
    ],
  };
}

function getInitialModel(credentials?: Credentials): AdminAiServer {
  return {
    id: 1,
    url: 'http://some.url',
    name: 'Server1',
    description: null,
    payloadTemplate: null,
    createdOn: '2024-02-28',
    createdBy: 'admin',
    lastModifiedBy: null,
    lastModifiedOn: null,
    credentials,
    authProtocol: AuthenticationProtocol.TOKEN_AUTH,
    authPolicy: AuthenticationPolicy.APP_LEVEL,
    jsonPath: 'This does not follow back rules',
  };
}
