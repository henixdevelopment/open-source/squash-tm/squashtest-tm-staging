import { RemoveTestAutomationServerDialogElement } from '../../../../page-objects/pages/administration-workspace/dialogs/remove-test-automation-server-dialog.element';
import { AdminWorkspaceAutomationServersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-automation-servers.page';
import { mockFieldValidationError } from '../../../../data-mock/http-errors.data-mock';
import { TestAutomationServerViewPage } from '../../../../page-objects/pages/administration-workspace/test-automation-server-view/test-automation-server-view.page';
import { makeTAServerViewData } from '../../../../data-mock/administration-views.data-mock';
import {
  AdminReferentialDataMockBuilder,
  getDefaultAdminReferentialData,
} from '../../../../utils/referential/admin-referential-data-builder';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { EnvironmentSelectionPanelDto } from '../../../../page-objects/elements/automated-execution-environments/environment-selection-panel.element';
import {
  AdminTestAutomationServer,
  AutomatedExecutionEnvironment,
  TestAutomationServer,
  TestAutomationServerKind,
} from '../../../../../../projects/sqtm-core/src/lib/model/test-automation/test-automation-server.model';
import { TokenAuthCredentials } from '../../../../../../projects/sqtm-core/src/lib/model/third-party-server/credentials.model';
import { AuthenticationProtocol } from '../../../../../../projects/sqtm-core/src/lib/model/third-party-server/authentication.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { EditableTextFieldElement } from '../../../../page-objects/elements/forms/editable-text-field.element';
import { mockDataRow } from '../../../../data-mock/grid.data-mock';

describe('Administration workspace - Test Automation Servers', function () {
  function initialisePage(): AdminWorkspaceAutomationServersPage {
    const refData = new AdminReferentialDataMockBuilder()
      .withAvailableTestAutomationServerKinds([
        TestAutomationServerKind.jenkins,
        TestAutomationServerKind.squashOrchestrator,
      ])
      .build();
    return AdminWorkspaceAutomationServersPage.initTestAtPageTestAutomationServers(
      initialNodes,
      refData,
    );
  }

  it('should display  the grid of test automation servers', () => {
    const page = initialisePage();
    const grid = page.grid;

    grid.assertRowExist(1);
    const row = grid.getRow(1);
    row.cell('name').textRenderer().assertContainsText('TestAutomationServer1');
    row.cell('baseUrl').linkRenderer().assertContainText('http:192.168.0.03:9090/testServer1');
  });

  it('should add test automation servers', () => {
    const page = initialisePage();
    const httpMockView = new HttpMockBuilder('test-automation-server-view/4?*')
      .get()
      .responseBody({
        id: 73,
        baseUrl: 'u',
        kind: 'jenkins',
        name: 't',
        description: 'd',
      })
      .build();

    const dialog = page.openCreateTestAutomationServer();
    dialog.assertExists();
    dialog.fillName('t');
    dialog.fillUrl('u');
    dialog.fillDescription('d');
    dialog.addWithOptions({
      addAnother: false,
      createResponse: { id: 4 },
      gridResponse: addTestAutomationServerResponse,
    });

    const grid = page.grid;

    grid.assertRowExist('1');
    grid.assertRowExist('2');
    grid.assertRowExist('3');
    grid.assertRowExist('4');

    httpMockView.wait();
    const view = new TestAutomationServerViewPage();
    view.entityNameField.checkContent('t');

    // should add another test automation server
    const dialogAnother = page.openCreateTestAutomationServer();
    dialogAnother.assertExists();
    dialogAnother.fillName('t');
    dialogAnother.fillUrl('u');
    dialogAnother.fillDescription('d');

    dialogAnother.addWithOptions({
      addAnother: true,
      createResponse: { id: 4 },
      gridResponse: addTestAutomationServerResponse,
    });

    dialogAnother.assertExists();
    dialogAnother.checkIfFormIsEmpty();
    dialogAnother.cancel();
    dialogAnother.assertNotExist();
  });

  it('should validate creation form', () => {
    const page = initialisePage();

    const dialog = page.openCreateTestAutomationServer();
    dialog.assertExists();
    dialog.fillName('t');
    dialog.fillUrl('u');
    dialog.fillDescription('d');

    // should forbid to add a test automation server if name already exists
    const nameHttpError = mockFieldValidationError(
      'name',
      'sqtm-core.error.generic.name-already-in-use',
    );
    dialog.addWithServerSideFailure(nameHttpError);
    dialog.assertExists();
    dialog.checkIfNameAlreadyInUseErrorMessageIsDisplayed();

    // should forbid to add a test automation server with an empty name
    dialog.fillName('');
    dialog.clickOnAddButton();
    dialog.checkIfRequiredErrorMessageIsDisplayed();

    // should forbid to add a test automation server with an empty url
    dialog.fillName('n');
    dialog.fillUrl('');
    dialog.clickOnAddButton();
    dialog.checkIfRequiredErrorMessageIsDisplayed();

    // should forbid to add a test automation server with a malformed url
    const urlHttpError = mockFieldValidationError('baseUrl', 'sqtm-core.exception.wrong-url');
    dialog.fillUrl('http');
    dialog.addWithServerSideFailure(urlHttpError);
    dialog.assertExists();
    dialog.checkIfMalformedUrlErrorMessageIsDisplayed();
  });

  it('should allow test automation removal without bound projects', () => {
    const page = initialisePage();

    // Show dialog
    page.grid.getRow(3, 'rightViewport').cell('delete').iconRenderer().click();

    // Confirm
    const dialog = new RemoveTestAutomationServerDialogElement([3]);
    dialog.deleteForSuccess({
      count: 1,
      dataRows: [initialNodes.dataRows[0]],
    });
  });

  it('should allow test automation removal with bound projects', () => {
    const page = initialisePage();

    // Show dialog
    page.grid.getRow(2, 'rightViewport').cell('delete').iconRenderer().click();

    // Confirm
    const dialog = new RemoveTestAutomationServerDialogElement([2]);
    dialog.deleteForSuccess({
      count: 1,
      dataRows: [initialNodes.dataRows[0]],
    });
  });

  it('should allow multiple test automation servers removal', () => {
    const page = initialisePage();

    // Select elements to delete
    page.grid.selectRows([1, 2, 3], '#', 'leftViewport');

    // Show dialog
    page.clickOnMultipleDeleteButton(true, {
      count: 0,
      dataRows: [],
      idAttribute: '',
    });
  });

  const initialNodes: GridResponse = {
    count: 3,
    dataRows: [
      mockDataRow({
        id: '1',
        children: [],
        data: {
          serverId: 1,
          name: 'TestAutomationServer1',
          baseUrl: 'http:192.168.0.03:9090/testServer1',
          executionCount: 0,
          createdBy: 'cypress',
          createdOn: new Date().toISOString(),
          lastModifiedBy: null,
          lastModifiedOn: null,
          kind: TestAutomationServerKind.jenkins,
        },
      }),
      mockDataRow({
        id: '2',
        children: [],
        data: {
          serverId: 2,
          name: 'TestAutomationServer2',
          baseUrl: 'http:192.168.0.03:9090/testServer2',
          executionCount: 2,
          createdBy: 'cypress',
          createdOn: new Date().toISOString(),
          lastModifiedBy: null,
          lastModifiedOn: null,
          kind: TestAutomationServerKind.jenkins,
        },
      }),
      mockDataRow({
        id: '3',
        children: [],
        data: {
          serverId: 3,
          name: 'TestAutomationServer3',
          baseUrl: 'http:192.168.0.03:9090/testServer3',
          executionCount: 0,
          createdBy: 'cypress',
          createdOn: new Date().toISOString(),
          lastModifiedBy: null,
          lastModifiedOn: null,
          kind: TestAutomationServerKind.jenkins,
        },
      }),
    ],
  };

  const addTestAutomationServerResponse: GridResponse = {
    count: 4,
    dataRows: [
      ...initialNodes.dataRows,
      mockDataRow({
        id: '4',
        children: [],
        data: { name: 'TestAutomationServer4' },
      }),
    ],
  };
});

describe('Administration Workspace - Test automation server - Authentication policy', function () {
  it('should send credential form', () => {
    const page = showTestAutomationServerView();

    page.authPolicyPanel.assertSendButtonDisabled();
    page.authPolicyPanel.usernameField.fill('username');
    page.authPolicyPanel.passwordField.fill('password');
    page.authPolicyPanel.assertSendButtonEnabled();

    page.authPolicyPanel.sendCredentialsForm();
    page.authPolicyPanel.assertSaveSuccessMessageVisible();
  });
});

describe('Administration Workspace - Test automation server - Authentication protocol panel', function () {
  it('should display auth protocol panel', () => {
    const page = showTestAutomationServerView();
    page.authProtocolPanel.protocolField.checkSelectedOption('basic authentication');
  });
});

describe('Administration Workspace - Test automation server - Information panel', function () {
  it('should set test automation server name', () => {
    const page = showTestAutomationServerView();
    page.entityNameField.setAndConfirmValue('NEW');
    page.entityNameField.checkContent('NEW');
  });

  it('should set test automation server url', () => {
    const page = showTestAutomationServerView();
    page.informationPanel.urlField.setAndConfirmValue('http://192.168.0.1:8080/jenkins');
    page.informationPanel.urlField.checkContent('http://192.168.0.1:8080/jenkins');
  });

  it('should set test automation server description', () => {
    const page = showTestAutomationServerView();
    page.informationPanel.descriptionField.setAndConfirmValue('A description');
    page.informationPanel.descriptionField.checkTextContent('A description');
  });

  it('should check test automation server manual slave selection', () => {
    const page = showTestAutomationServerView();
    page.informationPanel.manualSlaveSelection.toggleState();
    page.informationPanel.manualSlaveSelection.checkState(true);
  });
});

function showTestAutomationServerView(
  initialNodes = getInitialNodes(),
  envMockData?: EnvironmentSelectionPanelDto,
): TestAutomationServerViewPage {
  const page =
    AdminWorkspaceAutomationServersPage.initTestAtPageTestAutomationServers(initialNodes);
  const firstRowName = initialNodes.dataRows[0].data.name;

  return page.selectAutomationServerByName(
    [firstRowName],
    initialNodes.dataRows[0].data as AdminTestAutomationServer,
    envMockData,
  );
}

function showTestAutomationServerViewUltimate(
  initialNodes = getInitialNodes(),
  envMockData?: EnvironmentSelectionPanelDto,
): TestAutomationServerViewPage {
  const page = AdminWorkspaceAutomationServersPage.initTestAtPageTestAutomationServers(
    initialNodes,
    { ...getDefaultAdminReferentialData(), ultimateLicenseAvailable: true },
  );
  const firstRowName = initialNodes.dataRows[0].data.name;

  return page.selectAutomationServerByName(
    [firstRowName],
    initialNodes.dataRows[0].data as AdminTestAutomationServer,
    envMockData,
  );
}

function getInitialNodes(automationServer = getInitialModel()): GridResponse {
  return {
    count: 1,
    dataRows: [
      mockDataRow({
        id: automationServer.id.toString(),
        children: [],
        data: automationServer,
        allowMoves: true,
        allowedChildren: [],
        type: 'Generic',
      }),
    ],
  };
}

function getInitialModel(): AdminTestAutomationServer {
  return makeTAServerViewData({
    id: 1,
    name: 'auto1',
    baseUrl: 'http://test',
    createdBy: 'admin',
    createdOn: new Date().toISOString(),
    description: '',
    lastModifiedBy: 'admin',
    lastModifiedOn: new Date().toISOString(),
  });
}

describe('Administration Workspace - Test automation server - Environments panel', function () {
  it('should not show environment panel for a Jenkins server', () => {
    const page = showTestAutomationServerView();
    page.environmentPanel.assertNotExist();
  });

  it('should display environments panel', () => {
    const taServer = makeTAServerWithEnvironments();
    const page = showTestAutomationServerView(
      getInitialNodes(taServer),
      mockEnvDataFromServer(taServer),
    );
    page.fetchAvailableEnvironmentsMock.wait();
    page.fetchBoundEnvironmentVariableMock.wait();

    const environmentPanel = page.environmentPanel.environmentSelectionPanel;

    // Only visible on project's page
    environmentPanel.tokenOverrideField.assertNotExist();

    environmentPanel.assertSelectedTagsContainAll(['robot', 'chrome']);
    environmentPanel.availableEnvironmentsGrid.assertExists();
    environmentPanel.availableEnvironmentsGrid
      .findRowId('name', 'dummy')
      .then((rowId) =>
        environmentPanel.availableEnvironmentsGrid
          .getRow(rowId)
          .cell('joinedTags')
          .textRenderer()
          .assertContainsText('chrome, robot, windows'),
      );
  });

  it('should modify environment tags', () => {
    const taServer = makeTAServerWithEnvironments();
    const page = showTestAutomationServerView(
      getInitialNodes(taServer),
      mockEnvDataFromServer(taServer),
    );
    page.fetchAvailableEnvironmentsMock.wait();
    page.fetchBoundEnvironmentVariableMock.wait();

    const environmentPanel = page.environmentPanel.environmentSelectionPanel;

    environmentPanel.assertSelectedTagsContainAll(['robot', 'chrome']);

    environmentPanel.assertTagsOptionsContainAll([
      'chrome',
      'firefox',
      'linux',
      'robot',
      'windows',
    ]);

    environmentPanel.addTag('linux', ['robot', 'chrome', 'linux']);
    environmentPanel.removeTag('robot', ['chrome', 'linux']);

    environmentPanel.assertSelectedTagsContainAll(['chrome', 'linux']);
  });

  it('should display missing credentials message', () => {
    const taServer = makeTAServerWithEnvironments();
    const page = showTestAutomationServerView(getInitialNodes(taServer), {
      server: { hasServerCredentials: false, defaultTags: null, testAutomationServerId: null },
      environments: { environments: null },
      errorMessage: null,
    });
    page.fetchAvailableEnvironmentsMock.wait();

    page.environmentPanel.environmentSelectionPanel.assertMissingCredentialsMessageExists();
  });

  it('should display environments fetch error message', () => {
    const taServer = makeTAServerWithEnvironments();
    const availableEnvMockBuilder = new HttpMockBuilder(
      'test-automation-servers/*/automated-execution-environments/all?*',
    )
      .status(500)
      .build();

    const page = showTestAutomationServerView(getInitialNodes(taServer), null);
    availableEnvMockBuilder.wait();

    page.environmentPanel.environmentSelectionPanel.assertLoadEnvironmentsErrorMessageExists();
  });

  it('should filter available environments based on selected tags', () => {
    const taServer = makeTAServerWithEnvironments();
    const page = showTestAutomationServerView(
      getInitialNodes(taServer),
      mockEnvDataFromServer(taServer),
    );
    page.fetchAvailableEnvironmentsMock.wait();
    const environmentPanel = page.environmentPanel.environmentSelectionPanel;

    environmentPanel.removeTag('robot', ['chrome']);
    environmentPanel.removeTag('chrome', []);
    environmentPanel.availableEnvironmentsGrid.assertRowCount(2);

    environmentPanel.addTag('linux', ['linux']);
    environmentPanel.availableEnvironmentsGrid.assertRowCount(1);

    environmentPanel.addTag('windows', ['linux', 'windows']);
    environmentPanel.assertNoMatchingEnvironmentMessageExists();
  });
});

describe('Administration Workspace - Test automation server - SquashAUTOM fields', function () {
  it('should not show SquashAUTOM fields for a Jenkins server', () => {
    const page = showTestAutomationServerView();
    page.informationPanel.assertSquashAUTOMFieldsAreHidden();
  });

  it('should show SquashAUTOM fields', () => {
    const taServer = makeTAServerViewData({
      kind: TestAutomationServerKind.squashOrchestrator,
      supportsAutomatedExecutionEnvironments: true,
    });
    const page = showTestAutomationServerView(
      getInitialNodes(taServer),
      mockEnvDataFromServer(taServer),
    );
    page.fetchAvailableEnvironmentsMock.wait();
    page.informationPanel.assertSquashAUTOMFieldsAreVisible();
  });

  it('should modify Observer and EventBus and KillSwitch URL', () => {
    const taServer = makeTAServerViewData({
      kind: TestAutomationServerKind.squashOrchestrator,
      supportsAutomatedExecutionEnvironments: true,
    });
    const page = showTestAutomationServerView(
      getInitialNodes(taServer),
      mockEnvDataFromServer(taServer),
    );
    page.fetchAvailableEnvironmentsMock.wait();

    const observerUrlField = page.informationPanel.observerUrlField;

    observerUrlField.checkCustomPlaceholder(taServer.baseUrl);
    observerUrlField.setAndConfirmValue('http://my.url/');
    observerUrlField.checkContent('http://my.url/');

    const eventBusUrlField = page.informationPanel.eventBusUrlField;

    eventBusUrlField.checkCustomPlaceholder(taServer.baseUrl);
    eventBusUrlField.setAndConfirmValue('http://my.url/');
    eventBusUrlField.checkContent('http://my.url/');

    const killSwitchUrlField: EditableTextFieldElement = page.informationPanel.killSwitchUrlField;
    killSwitchUrlField.checkCustomPlaceholder(taServer.baseUrl);
    killSwitchUrlField.setAndConfirmValue('http://my.url/');
    killSwitchUrlField.checkContent('http://my.url/');
  });
});

describe('Administration Workspace - Test automation server - Environment variable panel', function () {
  it('should not show environment variable panel for a Jenkins server', () => {
    const page = showTestAutomationServerView();
    page.environmentVariablePanel.assertNotExist();
  });

  it('should display environment variable panel', () => {
    const taServer = makeTAServerWithEnvironments();
    const page = showTestAutomationServerView(
      getInitialNodes(taServer),
      mockEnvDataFromServer(taServer),
    );
    const environmentPanel = page.environmentVariablePanel.environmentVariableSelectionPanel;
    page.fetchBoundEnvironmentVariableMock.wait();
    page.showEnvironmentVariablePanel();
    environmentPanel.boundEnvironmentVariablesGrid.assertExists();
    environmentPanel.boundEnvironmentVariablesGrid
      .findRowId('name', 'environmentVariable1')
      .then((rowId) =>
        environmentPanel.boundEnvironmentVariablesGrid
          .getRow(rowId)
          .cell('value')
          .textRenderer()
          .assertContainsText('ev-value'),
      );
  });

  it('should modify environment variable', () => {
    const taServer = makeTAServerWithEnvironments();
    const page = showTestAutomationServerView(
      getInitialNodes(taServer),
      mockEnvDataFromServer(taServer),
    );

    const environmentVariablePanel =
      page.environmentVariablePanel.environmentVariableSelectionPanel;
    page.fetchBoundEnvironmentVariableMock.wait();
    page.showEnvironmentVariablePanel();

    environmentVariablePanel.updateSimpleEnvironmentVariableValue(
      'simple update',
      'environmentVariable1',
      'value',
      'bound-environment-variables/value',
    );

    environmentVariablePanel.showEnvironmentVariableOptionsList(2, 'environmentVariable2', 'value');
    environmentVariablePanel.selectUpdateEnvironmentVariableOption('option1');

    environmentVariablePanel.checkCellValue('simple update', 'environmentVariable1', 'value');
    environmentVariablePanel.checkCellValue('option1', 'environmentVariable2', 'value');
  });

  it('should delete one environment variable binding', () => {
    const taServer = makeTAServerWithEnvironments();
    const page = showTestAutomationServerView(
      getInitialNodes(taServer),
      mockEnvDataFromServer(taServer),
    );

    const environmentVariablePanel =
      page.environmentVariablePanel.environmentVariableSelectionPanel;
    page.fetchBoundEnvironmentVariableMock.wait();
    page.showEnvironmentVariablePanel();

    environmentVariablePanel.unbindEnvironmentVariable('environmentVariable1');
  });

  it('should delete multiple environment variable bindings', () => {
    const taServer = makeTAServerWithEnvironments();
    const page = showTestAutomationServerView(
      getInitialNodes(taServer),
      mockEnvDataFromServer(taServer),
    );

    const environmentVariablePanel =
      page.environmentVariablePanel.environmentVariableSelectionPanel;
    page.fetchBoundEnvironmentVariableMock.wait();
    page.showEnvironmentVariablePanel();

    environmentVariablePanel.boundEnvironmentVariablesGrid.selectRowsWithStickyIndexColumn([
      'environmentVariable1',
      'environmentVariable2',
    ]);
    environmentVariablePanel.unbindMultipleEnvironmentVariable([
      'environmentVariable1',
      'environmentVariable2',
    ]);
  });

  it('should add environment variable bindings', () => {
    const taServer = makeTAServerWithEnvironments();
    const page = showTestAutomationServerView(
      getInitialNodes(taServer),
      mockEnvDataFromServer(taServer),
    );

    const environmentVariablePanel =
      page.environmentVariablePanel.environmentVariableSelectionPanel;
    page.fetchBoundEnvironmentVariableMock.wait();
    page.showEnvironmentVariablePanel();

    environmentVariablePanel.bindEnvironmentVariable();
  });
});

describe('Administration Workspace - Test automation server - Additional Configuration panel', function () {
  it('should not display yaml additional configuration for a Jenkins server', () => {
    const page = showTestAutomationServerView();
    page.additionalConfigurationPanel.assertNotExist();
  });

  it('should display yaml editor when Squash Orchestrator server Ultimate', () => {
    const page = makeSquashAutomTAServerUltimate();
    page.showAdditionalConfigurationPanel();
    page.additionalConfigurationPanel.assertAceEditorExist();
  });

  it('should not display yaml editor when Squash Orchestrator server Not Ultimate', () => {
    const taServer = makeTAServerViewData({
      kind: TestAutomationServerKind.squashOrchestrator,
      supportsAutomatedExecutionEnvironments: true,
    });
    const page = showTestAutomationServerView(
      getInitialNodes(taServer),
      mockEnvDataFromServer(taServer),
    );
    page.showAdditionalConfigurationPanel();
    page.additionalConfigurationPanel.assertNotExist();
  });

  it('should display help', () => {
    const page = makeSquashAutomTAServerUltimate();
    page.showAdditionalConfigurationPanel();
    page.additionalConfigurationPanel.toggleHelp();
    page.additionalConfigurationPanel.assertHelpExist();
  });

  it('should display toolbar', () => {
    const page = makeSquashAutomTAServerUltimate();
    page.showAdditionalConfigurationPanel();
    page.additionalConfigurationPanel.showEditButtons();
  });

  it('should display yaml text script', () => {
    const page = makeSquashAutomTAServerUltimate();
    page.showAdditionalConfigurationPanel();
    page.additionalConfigurationPanel.insertScript('# Exemple de hook');
    page.additionalConfigurationPanel.assertAceEditorContainsText('# Exemple de hook');
  });

  it('should display valid script message in modal - yaml', () => {
    const page = makeSquashAutomTAServerUltimate();
    page.showAdditionalConfigurationPanel();
    page.additionalConfigurationPanel.insertScript('key: value');
    page.additionalConfigurationPanel.checkSyntaxMessageShouldBe(
      'La syntaxe du script est valide.',
      true,
    );
  });

  it('should display invalid script message in modal - yaml', () => {
    const page = makeSquashAutomTAServerUltimate();
    page.showAdditionalConfigurationPanel();
    page.additionalConfigurationPanel.insertScript('key: value: value2');
    page.additionalConfigurationPanel.checkSyntaxMessageShouldBe(
      "La syntaxe du script n'est pas valide. Un fichier YAML ou JSON valide est attendu.",
      false,
    );
  });
});

function mockEnvDataFromServer(taServer: TestAutomationServer): EnvironmentSelectionPanelDto {
  return {
    server: {
      testAutomationServerId: taServer.id,
      defaultTags: taServer.environmentTags,
      hasServerCredentials: true,
    },
    environments: {
      environments: taServer.availableEnvironments,
    },
    errorMessage: null,
  };
}

function makeTAServerWithEnvironments() {
  const dummyEnvironment1: AutomatedExecutionEnvironment = {
    name: 'dummy',
    tags: ['robot', 'firefox', 'linux'],
    namespaces: [],
    status: 'IDLE',
  };

  const dummyEnvironment2: AutomatedExecutionEnvironment = {
    name: 'dummy2',
    tags: ['windows', 'chrome', 'robot'],
    namespaces: [],
    status: 'BUSY',
  };

  const allTags = [...dummyEnvironment1.tags];
  dummyEnvironment2.tags.forEach((tag) => {
    if (!allTags.includes(tag)) {
      allTags.push(tag);
    }
  });

  return makeTAServerViewData({
    kind: TestAutomationServerKind.squashOrchestrator,
    supportsAutomatedExecutionEnvironments: true,
    availableEnvironments: [dummyEnvironment1, dummyEnvironment2],
    availableEnvironmentTags: allTags,
    environmentTags: ['robot', 'chrome'],
    credentials: makeTokenCredentials(),
  });
}

function makeSquashAutomTAServerUltimate(): TestAutomationServerViewPage {
  const taServer = makeTAServerViewData({
    kind: TestAutomationServerKind.squashOrchestrator,
    supportsAutomatedExecutionEnvironments: true,
  });
  return showTestAutomationServerViewUltimate(
    getInitialNodes(taServer),
    mockEnvDataFromServer(taServer),
  );
}

function makeTokenCredentials(): TokenAuthCredentials {
  return {
    type: AuthenticationProtocol.TOKEN_AUTH,
    implementedProtocol: AuthenticationProtocol.TOKEN_AUTH,
    token: 'whatever, dude',
  };
}
