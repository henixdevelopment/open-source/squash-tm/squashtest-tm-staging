import { AdminWorkspaceInfoListsPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-info-lists.page';
import { InfoListViewPage } from '../../../../page-objects/pages/administration-workspace/info-list-view/info-list-view.page';
import { NavBarAdminElement } from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import {
  AdminInfoList,
  AdminInfoListItem,
} from '../../../../../../projects/sqtm-core/src/lib/model/infolist/adminInfoList.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { mockDataRow } from '../../../../data-mock/grid.data-mock';

describe('Administration Workspace - Info lists', function () {
  const initialNodes: GridResponse = {
    count: 2,
    dataRows: [
      mockDataRow({
        id: '1',
        children: [],
        data: {
          infoListId: 1,
          label: 'List 1',
          description: 'description',
          code: 'LIST1',
          defaultValue: 'undefined',
          projectCount: 2,
        },
      }),
      mockDataRow({
        id: '2',
        children: [],
        data: {
          infoListId: 2,
          label: 'List 2',
          description: 'description',
          code: 'LIST2',
          defaultValue: 'undefined',
          projectCount: 0,
        },
      }),
    ],
  };

  it('should display info lists grid', () => {
    const page = AdminWorkspaceInfoListsPage.initTestAtPageInfoLists(initialNodes);
    const grid = page.grid;

    grid.assertRowExist(1);
    const row = grid.getRow(1);
    row.cell('label').textRenderer().assertContainsText('List 1');
    row.cell('description').textRenderer().assertContainsText('description');
    row.cell('code').textRenderer().assertContainsText('LIST1');
    row.cell('defaultValue').textRenderer().assertContainsText('undefined');

    grid.assertRowExist(2);
    const rowList2 = grid.getRow(2);
    rowList2.cell('label').textRenderer().assertContainsText('List 2');
    rowList2.cell('description').textRenderer().assertContainsText('description');
    rowList2.cell('code').textRenderer().assertContainsText('LIST2');
    rowList2.cell('defaultValue').textRenderer().assertContainsText('undefined');
  });

  it('should create info list', () => {
    const page = AdminWorkspaceInfoListsPage.initTestAtPageInfoLists(initialNodes);
    const httpMockView = new HttpMockBuilder('info-list-view/3?*')
      .get()
      .responseBody({
        id: 3,
        label: 'info list 1',
        code: 'IL_01',
        description: 'description',
        defaultValue: 'option 1',
        projectCount: 0,
        items: [{ id: 1, label: 'option 1', code: 'code 1' }],
      })
      .build();
    const dialog = page.openCreateInfoList();
    dialog.assertExists();

    dialog.labelField.fill('info list 1');
    dialog.codeField.fill('IL_01');
    dialog.descriptionField.fill('description');
    cy.clickVoid();
    dialog.addInfoListOption('option 1', 'code1');
    dialog.infoListOptionsGrid.assertRowCount(1);
    dialog.addWithOptions({
      addAnother: false,
      createResponse: { id: 3 },
      gridResponse: {
        count: initialNodes.count + 1,
        dataRows: [
          ...initialNodes.dataRows,
          mockDataRow({
            id: '3',
            children: [],
            data: {
              infoListId: 3,
              label: 'info list 1',
              code: 'IL_01',
              description: 'description',
              defaultValue: 'option 1',
              projectCount: 0,
            },
          }),
        ],
      },
    });
    httpMockView.wait();
    const view = new InfoListViewPage();
    view.assertExists();
    view.entityNameField.checkContent('info list 1');
  });

  it('should validate creation form', () => {
    const page = AdminWorkspaceInfoListsPage.initTestAtPageInfoLists(initialNodes);

    const dialog = page.openCreateInfoList();
    dialog.assertExists();
    dialog.labelField.fill('il');

    // should forbid to create info list with invalid code pattern
    dialog.codeField.fill('IL-01!');
    dialog.addInfoListOption('option 1', 'code1');
    dialog.infoListOptionsGrid.assertRowCount(1);
    dialog.clickOnAddButton();
    dialog.checkIfInvalidCodePatternErrorIsDisplayed();

    // should forbid to create info list without default value
    dialog.codeField.fill('IL_01');
    dialog.removeOptionWithLabel('option 1');
    dialog.clickOnAddButton();
    dialog.checkIfOptionDefaultValueRequiredErrorIsDisplayed();

    // should forbid to create info list if option name already exists
    dialog.addInfoListOption('option 1', 'code1');
    dialog.fillInfoListOptionLabel('option 1');
    dialog.fillInfoListOptionCode('code2');
    dialog.clickOnAddOptionButton();
    dialog.checkIfOptionNameAlreadyExistErrorIsDisplayed();

    // should forbid to create info list if option code already exists
    dialog.fillInfoListOptionLabel('option 2');
    dialog.fillInfoListOptionCode('code1');
    dialog.clickOnAddOptionButton();
    dialog.checkIfOptionCodeAlreadyExistErrorIsDisplayed();

    // should forbid to create info list if option code has invalid pattern
    dialog.fillInfoListOptionLabel('option 1');
    dialog.fillInfoListOptionCode('code1!!!!');
    dialog.clickOnAddOptionButton();
    dialog.checkIfInvalidCodePatternErrorIsDisplayed();
  });

  it('should delete an info list from the grid', () => {
    const page = AdminWorkspaceInfoListsPage.initTestAtPageInfoLists(initialNodes);
    page.deleteInfoList(1, { count: 1, dataRows: [initialNodes.dataRows[1]] });
    page.grid.assertRowCount(1);
  });

  it('should delete multiple rows', () => {
    const page = AdminWorkspaceInfoListsPage.initTestAtPageInfoLists(initialNodes);
    page.selectInfoListsByLabel(['List 1', 'List 2']);
    page.clickOnMultipleDeleteButton(true, {
      count: 0,
      dataRows: [],
    });
    page.grid.assertRowCount(0);
  });
});

describe('Administration Workspace - Info lists - Information panel', function () {
  it('should set infolist name', () => {
    const infoListPage = showInfoListView();
    infoListPage.assertExists();

    infoListPage.entityNameField.setAndConfirmValue('NEW');
    infoListPage.entityNameField.checkContent('NEW');
  });

  it('should set infolist code', () => {
    const infoListPage = showInfoListView();
    infoListPage.assertExists();

    infoListPage.informationPanel.codeField.setAndConfirmValue('NEW');
    infoListPage.informationPanel.codeField.checkContent('NEW');
  });

  it('should set infolist description', () => {
    const infoListPage = showInfoListView();
    infoListPage.assertExists();

    infoListPage.informationPanel.descriptionField.setAndConfirmValue('NEW');
    infoListPage.informationPanel.descriptionField.checkTextContent('NEW');
  });
});

describe('Administration Workspace - Info lists - Items panel', function () {
  it('should show info list items grid', () => {
    const page = showInfoListView();
    page.itemsPanel.grid.assertRowCount(3);
  });

  it('should forbid removing the default item', () => {
    // given
    const page = showInfoListView();
    const itemsPanel = page.itemsPanel;
    const first = 'label1';
    const second = 'label2';

    itemsPanel.assertDeleteIconIsHidden(first);
    itemsPanel.assertDeleteIconIsVisible(second);

    // when
    itemsPanel.setDefaultItem(second);

    // then
    itemsPanel.assertDeleteIconIsVisible(first);
    itemsPanel.assertDeleteIconIsHidden(second);
  });

  it('should add a new item', () => {
    // given
    const initialModel = getInitialInfoListModel();
    const page = showInfoListView();
    const addDialog = page.itemsPanel.openAddItemDialog();

    addDialog.iconField.assertHasNoIcon();
    const desiredIcon = 'sqtm-core-infolist-item\\:handshake';
    addDialog.iconField.selectIcon(desiredIcon);
    addDialog.iconField.assertHasIcon(desiredIcon);

    addDialog.nameField.fill('label4');
    addDialog.codeField.fill('code4');

    addDialog.addItem({
      ...initialModel,
      items: [...initialModel.items, { ...getInfoListItem(4, 3), iconName: 'handshake' }],
    });

    page.itemsPanel.grid.assertRowCount(4);
  });
});

function showInfoListView(): InfoListViewPage {
  const initialNodes = getInitialNodes();
  const page = AdminWorkspaceInfoListsPage.initTestAtPageInfoLists(initialNodes);
  const viewPage = page.selectInfoListsByLabel(
    [initialNodes.dataRows[0].data.label],
    initialNodes.dataRows[0].data as AdminInfoList,
  );

  new NavBarAdminElement().toggle();
  viewPage.foldGrid();

  return viewPage;
}

function getInitialNodes(): GridResponse {
  const infoList = getInitialInfoListModel();

  return {
    count: 1,
    dataRows: [
      mockDataRow({
        id: infoList.id.toString(),
        children: [],
        data: infoList,
        allowMoves: true,
        allowedChildren: [],
        type: 'Generic',
      }),
    ],
  };
}

function getInitialInfoListModel(): AdminInfoList {
  return {
    id: 1,
    code: 'list1',
    label: 'InfoList1',
    createdBy: 'cypress',
    createdOn: new Date().toISOString(),
    description: '',
    items: [
      { ...getInfoListItem(1, 0), isDefault: true },
      getInfoListItem(2, 1),
      getInfoListItem(3, 2),
    ],
    lastModifiedBy: null,
    lastModifiedOn: null,
  };
}

function getInfoListItem(id: number, index: number): AdminInfoListItem {
  return {
    id,
    label: 'label' + id,
    code: 'code' + id,
    system: false,
    itemIndex: index,
    isDefault: false,
    iconName: '',
    colour: '',
    friendlyLabel: '',
    uri: '',
  };
}
