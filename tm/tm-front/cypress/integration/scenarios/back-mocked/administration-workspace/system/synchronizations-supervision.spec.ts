import { SystemViewSynchronizationSupervisionPage } from '../../../../page-objects/pages/administration-workspace/system-view/system-view-synchronization-supervision.page';
import { SystemViewModel } from '../../../../../../projects/sqtm-core/src/lib/model/system/system-view.model';
import { makeSystemViewData } from '../../../../data-mock/administration-views.data-mock';
import {
  AdminReferentialDataMockBuilder,
  getDefaultAdminReferentialData,
} from '../../../../utils/referential/admin-referential-data-builder';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { EditableDateFieldElement } from '../../../../page-objects/elements/forms/editable-date-field.element';

const todayDate = EditableDateFieldElement.dateToDisplayString(new Date());

describe('Administration workspace - Synchronizations supervision', function () {
  it('should display correct information in sync grids', () => {
    const viewData: SystemViewModel = makeSystemViewData();
    const page = SystemViewSynchronizationSupervisionPage.navigateToPage(viewData);

    checkInfoInXsquash4GitLabGrid(page);
    checkInfoInXsquash4JiraGrid(page);
    checkInfoInAutomJiraGrid(page);
  });

  it('should show not display synchronization grid, if plugin is not installed', () => {
    const referentialData = getDefaultAdminReferentialData();
    referentialData.synchronizationPlugins = [
      { id: 'henix.plugin.automation.workflow.automjira', name: 'Workflow Automatisation Jira' },
      { id: 'squash.tm.plugin.jirasync', name: 'Xsquash4Jira' },
    ];

    const viewData: SystemViewModel = makeSystemViewData();

    const page = SystemViewSynchronizationSupervisionPage.navigateToPage(viewData, referentialData);
    page.xsquash4jiraGrid.assertExists();
    page.automJiraGrid.assertExists();
    page.xsquash4gitlabGrid.assertNotExists();
  });

  it('should only display perimeter column on xsquash4gitlab grid', () => {
    const viewData: SystemViewModel = makeSystemViewData();
    const page = SystemViewSynchronizationSupervisionPage.navigateToPage(viewData);
    page.xsquash4gitlabGrid.getHeaderRow().header(GridColumnId.perimeter).assertExists();
    page.automJiraGrid.getHeaderRow().header(GridColumnId.perimeter).assertNotExists();
    page.xsquash4jiraGrid.getHeaderRow().header(GridColumnId.perimeter).assertNotExists();
  });

  it('should not display name column on autom jira grid and display on other grids', () => {
    const viewData: SystemViewModel = makeSystemViewData();
    const page = SystemViewSynchronizationSupervisionPage.navigateToPage(viewData);
    page.automJiraGrid.getHeaderRow().header(GridColumnId.name).assertNotExists();
    page.xsquash4gitlabGrid.getHeaderRow().header(GridColumnId.name).assertExists();
    page.xsquash4jiraGrid.getHeaderRow().header(GridColumnId.name).assertExists();
  });

  it('should only display select type column only on xsquash4jira grid', () => {
    const viewData: SystemViewModel = makeSystemViewData();
    const page = SystemViewSynchronizationSupervisionPage.navigateToPage(viewData);
    page.xsquash4jiraGrid.getHeaderRow().header(GridColumnId.remoteSelectType).assertExists();
    page.automJiraGrid.getHeaderRow().header(GridColumnId.remoteSelectType).assertNotExists();
    page.xsquash4gitlabGrid.getHeaderRow().header(GridColumnId.remoteSelectType).assertNotExists();
  });

  it('should not be possible to click on server name if logged as project manager', () => {
    const referentialData = new AdminReferentialDataMockBuilder()
      .withUser({
        userId: 1,
        username: 'theBoss',
        firstName: 'John',
        lastName: 'Doe',
        admin: false,
        hasAnyReadPermission: true,
        projectManager: true,
        milestoneManager: true,
        clearanceManager: true,
        functionalTester: true,
        automationProgrammer: true,
        canDeleteFromFront: true,
      })
      .build();

    const viewData: SystemViewModel = makeSystemViewData();
    const page = SystemViewSynchronizationSupervisionPage.navigateToPage(viewData, referentialData);
    const row = page.xsquash4jiraGrid.getRow(1);
    assertServerNameIsNotClickable(row);
  });

  it('should display the download log icon if the status is failure and there is log file path in view data', () => {
    const viewData: SystemViewModel = makeSystemViewData();
    const page = SystemViewSynchronizationSupervisionPage.navigateToPage(viewData);

    page.xsquash4gitlabGrid.assertRowExist(1);
    const rowRight = page.xsquash4gitlabGrid.getRow(1, 'rightViewport');
    rowRight
      .cell(GridColumnId.syncErrorLogFilePath)
      .iconRenderer()
      .assertContainsIcon('anticon-sqtm-core-generic:file-alert');
  });

  it('should not display the download log icon if the status is not failure even if there is log file path in view data', () => {
    const viewData: SystemViewModel = makeSystemViewData();
    const page = SystemViewSynchronizationSupervisionPage.navigateToPage(viewData);

    page.xsquash4gitlabGrid.assertRowExist(2);
    const rowRight = page.xsquash4gitlabGrid.getRow(2, 'rightViewport');
    rowRight.cell(GridColumnId.syncErrorLogFilePath).iconRenderer().assertNotExist();
  });

  it('should not display the download log icon if there is no log file path in view data', () => {
    const viewData: SystemViewModel = makeSystemViewData();
    const page = SystemViewSynchronizationSupervisionPage.navigateToPage(viewData);

    page.xsquash4gitlabGrid.assertRowExist(5);
    const rowRight = page.xsquash4gitlabGrid.getRow(5, 'rightViewport');
    rowRight.cell(GridColumnId.syncErrorLogFilePath).iconRenderer().assertNotExist();
  });

  it('should display a message if no synchronization plugin installed', () => {
    const referentialData = getDefaultAdminReferentialData();
    referentialData.synchronizationPlugins = [];

    const viewData: SystemViewModel = makeSystemViewData();
    const page = SystemViewSynchronizationSupervisionPage.navigateToPage(viewData, referentialData);
    page.assertNoPluginInstalledMessageExists();
  });
});

function checkInfoInXsquash4GitLabGrid(page: SystemViewSynchronizationSupervisionPage) {
  page.xsquash4gitlabGrid.assertRowExist(1);
  const row = page.xsquash4gitlabGrid.getRow(1);
  const rowRight = page.xsquash4gitlabGrid.getRow(1, 'rightViewport');
  row.cell(GridColumnId.id).textRenderer().assertContainsText('1');
  row.cell(GridColumnId.name).textRenderer().assertContainsText('Sync1');
  row.cell(GridColumnId.projectName).linkRenderer().assertContainText('project 1');
  row.cell(GridColumnId.serverName).linkRenderer().assertContainText('GitLab Server');
  assertServerNameIsClickable(row);
  row.cell(GridColumnId.perimeter).textRenderer().assertContainsText('henix/recette/squash');
  row.cell(GridColumnId.lastSyncDate).textRenderer().assertContainsText(todayDate);
  row.cell(GridColumnId.lastSuccessfulSyncDate).textRenderer().assertContainsText(todayDate);
  row.cell(GridColumnId.enabled).textRenderer().assertContainsText('Oui');
  row.cell(GridColumnId.synchronizedRequirementsRatio).textRenderer().assertContainsText('25/27');
  row.cell(GridColumnId.synchronizedSprintTicketsRatio).textRenderer().assertContainsText('-');
  rowRight.cell(GridColumnId.status).findCell().find('div').should('have.class', 'failure');
  page.xsquash4gitlabGrid.assertRowExist(3);
  const row3 = page.xsquash4gitlabGrid.getRow(3);
  row3.cell(GridColumnId.synchronizedRequirementsRatio).textRenderer().assertContainsText('0/7');
  row3.cell(GridColumnId.synchronizedSprintTicketsRatio).textRenderer().assertContainsText('8/8');
}

function checkInfoInXsquash4JiraGrid(page: SystemViewSynchronizationSupervisionPage) {
  page.xsquash4jiraGrid.assertRowExist(1);
  const row = page.xsquash4jiraGrid.getRow(1);
  const rowRight = page.xsquash4jiraGrid.getRow(1, 'rightViewport');
  row.cell(GridColumnId.id).textRenderer().assertContainsText('1');
  row.cell(GridColumnId.name).textRenderer().assertContainsText('Jira Sync 1');
  row.cell(GridColumnId.projectName).linkRenderer().assertContainText('example project 1');
  row.cell(GridColumnId.serverName).linkRenderer().assertContainText('Jira Server');
  assertServerNameIsClickable(row);
  row.cell(GridColumnId.remoteSelectType).findCell().find('b').should('contain', 'Query');
  row.cell(GridColumnId.remoteSelectType).textRenderer().assertContainsText('myValue');
  row.cell(GridColumnId.lastSyncDate).textRenderer().assertContainsText(todayDate);
  row.cell(GridColumnId.lastSuccessfulSyncDate).textRenderer().assertContainsText(todayDate);
  row.cell(GridColumnId.enabled).textRenderer().assertContainsText('Non');
  row.cell(GridColumnId.synchronizedRequirementsRatio).textRenderer().assertContainsText('10/10');
  row.cell(GridColumnId.synchronizedSprintTicketsRatio).textRenderer().assertContainsText('5/7');
  rowRight.cell(GridColumnId.status).findCell().find('div').should('have.class', 'failure');
  page.xsquash4jiraGrid.assertRowExist(2);
  const row2 = page.xsquash4jiraGrid.getRow(2);
  row2.cell(GridColumnId.synchronizedRequirementsRatio).textRenderer().assertContainsText('-');
  row2.cell(GridColumnId.synchronizedSprintTicketsRatio).textRenderer().assertContainsText('-');
}

function checkInfoInAutomJiraGrid(page: SystemViewSynchronizationSupervisionPage) {
  page.automJiraGrid.assertRowExist(1);
  const row = page.automJiraGrid.getRow(1);
  const rowRight = page.automJiraGrid.getRow(1, 'rightViewport');
  row.cell(GridColumnId.id).textRenderer().assertContainsText('1');
  row.cell(GridColumnId.projectName).linkRenderer().assertContainText('project 1');
  row.cell(GridColumnId.serverName).linkRenderer().assertContainText('Autom Jira Server');
  assertServerNameIsClickable(row);
  row.cell(GridColumnId.lastSyncDate).textRenderer().assertContainsText(todayDate);
  row.cell(GridColumnId.lastSuccessfulSyncDate).textRenderer().assertContainsText(todayDate);
  row.cell(GridColumnId.enabled).textRenderer().assertContainsText('Oui');
  rowRight.cell(GridColumnId.status).findCell().find('div').should('have.class', 'success');
}

function assertServerNameIsClickable(row) {
  row.cell(GridColumnId.serverName).findCell().find('div').should('contain.html', 'a');
}

function assertServerNameIsNotClickable(row) {
  row.cell(GridColumnId.serverName).findCell().find('div').should('contain.html', 'span');
}
