import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { ReferentialDataMockBuilder } from '../../../../utils/referential/referential-data-builder';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

describe('Requirement export', () => {
  function buildReferentialData() {
    return new ReferentialDataMockBuilder()
      .withUser({
        userId: 1,
        admin: true,
        username: 'admin',
      })
      .withProjects(
        { name: 'Project1', label: 'Project1' },
        { name: 'Project2', label: 'Project2' },
      )
      .build();
  }

  const initialNodes = mockGridResponse('id', [
    mockTreeNode({
      id: 'RequirementLibrary-1',
      projectId: 1,
      children: [],
      data: { NAME: 'Project1', CHILD_COUNT: 2 },
    }),
    mockTreeNode({
      id: 'RequirementLibrary-2',
      projectId: 2,
      children: [],
      data: { NAME: 'Project2', CHILD_COUNT: 1 },
    }),
    mockTreeNode({
      id: 'Requirement-3',
      children: [],
      parentRowId: 'RequirementLibrary-1',
      state: DataRowOpenState.leaf,
      data: {
        RLN_ID: 3,
        CHILD_COUNT: 0,
        NAME: 'M4 - Build Cupola',
        CRITICALITY: 'CRITICAL',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        HAS_DESCRIPTION: true,
        REQ_CATEGORY_ICON: 'briefcase',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
        REQ_CATEGORY_TYPE: 'SYS',
        COVERAGE_COUNT: 0,
        IS_SYNCHRONIZED: false,
      },
    }),
    mockTreeNode({
      id: 'Requirement-4',
      children: [],
      parentRowId: 'RequirementLibrary-1',
      state: DataRowOpenState.leaf,
      data: {
        RLN_ID: 4,
        CHILD_COUNT: 0,
        NAME: 'Repair Cupola',
        CRITICALITY: 'CRITICAL',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        HAS_DESCRIPTION: false,
        REQ_CATEGORY_ICON: 'briefcase',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
        REQ_CATEGORY_TYPE: 'SYS',
        COVERAGE_COUNT: 0,
        IS_SYNCHRONIZED: false,
      },
    }),
    mockTreeNode({
      id: 'Requirement-5',
      children: [],
      parentRowId: 'RequirementLibrary-2',
      state: DataRowOpenState.leaf,
      data: {
        RLN_ID: 5,
        CHILD_COUNT: 0,
        NAME: 'Kill Cupola',
        CRITICALITY: 'CRITICAL',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        HAS_DESCRIPTION: true,
        REQ_CATEGORY_ICON: 'briefcase',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
        REQ_CATEGORY_TYPE: 'SYS',
        COVERAGE_COUNT: 0,
        IS_SYNCHRONIZED: false,
      },
    }),
  ]);

  function navigateToRequirementWorkspace() {
    const referentialDataMock = buildReferentialData();
    return RequirementWorkspacePage.initTestAtPage(initialNodes, referentialDataMock);
  }

  it('Should open export requirement dialog then close it with Cancel button', () => {
    const requirementWorkspacePage: RequirementWorkspacePage = navigateToRequirementWorkspace();
    requirementWorkspacePage.navBar.toggle();
    requirementWorkspacePage.tree.selectNode('RequirementLibrary-1', {});
    const exportDialog = requirementWorkspacePage.treeMenu.openExportRequirementDialog();
    exportDialog.assertExists();
    exportDialog.clickCancel();
    exportDialog.assertNotExist();
  });

  it('Should export a requirement into an Excel file', () => {
    const requirementWorkspacePage: RequirementWorkspacePage = navigateToRequirementWorkspace();
    requirementWorkspacePage.navBar.toggle();
    requirementWorkspacePage.tree.selectNode('RequirementLibrary-1', {});
    const exportDialog = requirementWorkspacePage.treeMenu.openExportRequirementDialog();
    exportDialog.assertExists();

    // We do not click the confirm button during tests because it would cause a file download!
    // exportDialog.clickExport();
    // exportDialog.assertNotExist();
  });
});
