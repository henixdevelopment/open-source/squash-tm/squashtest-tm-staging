import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { ReferentialDataMockBuilder } from '../../../../utils/referential/referential-data-builder';
import {
  DataRowModel,
  DataRowOpenState,
} from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { mockTreeNode } from '../../../../data-mock/grid.data-mock';

const req3 = mockTreeNode({
  id: 'Requirement-3',
  children: [],
  parentRowId: 'RequirementLibrary-1',
  state: DataRowOpenState.leaf,
  data: {
    RLN_ID: 3,
    CHILD_COUNT: 0,
    NAME: "Find lot's of bucks",
    CRITICALITY: 'CRITICAL',
    REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
    HAS_DESCRIPTION: true,
    REQ_CATEGORY_ICON: 'briefcase',
    REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
    REQ_CATEGORY_TYPE: 'SYS',
    COVERAGE_COUNT: 0,
    IS_SYNCHRONIZED: false,
  },
});
const rl1 = mockTreeNode({
  id: 'RequirementLibrary-1',
  children: ['RequirementFolder-1', 'Requirement-3', 'RequirementFolder-2'],
  data: { NAME: 'International Space Station', CHILD_COUNT: '3' },
  state: DataRowOpenState.open,
});
const rl2 = mockTreeNode({
  id: 'RequirementLibrary-2',
  children: [],
  data: { NAME: 'STS - Shuttle', CHILD_COUNT: '3' },
  state: DataRowOpenState.closed,
});
const rf2 = mockTreeNode({
  id: 'RequirementFolder-2',
  children: [],
  parentRowId: 'RequirementLibrary-1',
  data: { NAME: 'Functional Requirements' },
  state: DataRowOpenState.closed,
});
const rf1 = mockTreeNode({
  id: 'RequirementFolder-1',
  parentRowId: 'RequirementLibrary-1',
  data: { NAME: 'Structural Requirements' },
  children: ['Requirement-4', 'Requirement-5'],
  state: DataRowOpenState.open,
});
const req4 = mockTreeNode({
  id: 'Requirement-4',
  children: [],
  parentRowId: 'RequirementFolder-1',
  state: DataRowOpenState.leaf,
  data: {
    RLN_ID: 4,
    CHILD_COUNT: 0,
    NAME: 'Build Soyouz deck',
    CRITICALITY: 'CRITICAL',
    REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
    HAS_DESCRIPTION: true,
    REQ_CATEGORY_ICON: 'briefcase',
    REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
    REQ_CATEGORY_TYPE: 'SYS',
    COVERAGE_COUNT: 0,
    IS_SYNCHRONIZED: false,
  },
});
const req5 = mockTreeNode({
  id: 'Requirement-5',
  children: ['Requirement-6', 'Requirement-7'],
  parentRowId: 'RequirementFolder-1',
  state: DataRowOpenState.open,
  data: {
    RLN_ID: 4,
    CHILD_COUNT: 1,
    NAME: 'Build modules',
    CRITICALITY: 'MAJOR',
    REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
    HAS_DESCRIPTION: true,
    REQ_CATEGORY_ICON: 'briefcase',
    REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
    REQ_CATEGORY_TYPE: 'SYS',
    COVERAGE_COUNT: 0,
    IS_SYNCHRONIZED: false,
  },
});
const req6 = mockTreeNode({
  id: 'Requirement-6',
  children: [],
  parentRowId: 'Requirement-5',
  state: DataRowOpenState.leaf,
  data: {
    RLN_ID: 5,
    CHILD_COUNT: 0,
    NAME: 'Build Central Module',
    CRITICALITY: 'MAJOR',
    REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
    HAS_DESCRIPTION: true,
    REQ_CATEGORY_ICON: 'briefcase',
    REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
    REQ_CATEGORY_TYPE: 'SYS',
    COVERAGE_COUNT: 0,
    IS_SYNCHRONIZED: false,
  },
});
const req7 = mockTreeNode({
  id: 'Requirement-7',
  children: [],
  parentRowId: 'Requirement-5',
  state: DataRowOpenState.leaf,
  data: {
    RLN_ID: 5,
    CHILD_COUNT: 0,
    NAME: 'Build Cupola',
    CRITICALITY: 'CRITICAL',
    REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
    HAS_DESCRIPTION: true,
    REQ_CATEGORY_ICON: 'briefcase',
    REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
    REQ_CATEGORY_TYPE: 'SYS',
    COVERAGE_COUNT: 0,
    IS_SYNCHRONIZED: false,
  },
});

function getNodesInReferentialMode() {
  return [rl1, rl2, req3, rf2, rf1, req4, req5, req6, req7];
}

// nodes with first milestone active
function getNodesInFirstMilestoneMode() {
  return [
    buildNodeWithMilestone(rl1, [1, 2], [1, 2], true, false),
    buildNodeWithMilestone(rl2, [1], [1], true, false),
    buildNodeWithMilestone(req3, [], [], false, false),
    buildNodeWithMilestone(rf2, [1, 2], [1, 2], true, false),
    buildNodeWithMilestone(rf1, [1, 2], [1, 2], true, false),
    buildNodeWithMilestone(req4, [2], [2], false, false),
    buildNodeWithMilestone(req5, [], [], false, true),
    buildNodeWithMilestone(req6, [1], [1], true, false),
    buildNodeWithMilestone(req7, [2], [2], false, false),
  ];
}

function getNodesInSecondMilestoneMode() {
  return [
    buildNodeWithMilestone(rl1, [1, 2], [1, 2], true, false),
    buildNodeWithMilestone(rl2, [1], [1], false, false),
    buildNodeWithMilestone(req3, [], [], false, false),
    buildNodeWithMilestone(rf2, [1, 2], [1, 2], true, false),
    buildNodeWithMilestone(rf1, [1, 2], [1, 2], true, false),
    buildNodeWithMilestone(req4, [2], [2], true, false),
    buildNodeWithMilestone(req5, [], [], false, true),
    buildNodeWithMilestone(req6, [1], [1], false, false),
    buildNodeWithMilestone(req7, [2], [2], true, false),
  ];
}

function buildNodeWithMilestone(
  node: DataRowModel,
  milestones: number[],
  allMilestones: number[],
  directlyBound: boolean,
  indirectlyBound: boolean,
) {
  return {
    ...node,
    data: {
      ...node.data,
      MILESTONES: milestones,
      ALL_MILESTONES: allMilestones,
      DIRECT_MILESTONE_BIND: directlyBound,
      INDIRECT_MILESTONE_BIND: indirectlyBound,
    },
  };
}

function performAllChecksInReferentialDataMode(tree) {
  tree.assertNodeExist(rl1.id);
  tree.assertNodeExist(rl2.id);
  tree.assertNodeIsOpen(rl1.id);
  tree.assertNodeIsClosed(rl2.id);
  tree.assertNodeExist(rf1.id);
  tree.assertNodeExist(rf2.id);
  tree.assertNodeExist(req3.id);
  tree.assertNodeExist(req4.id);
  tree.assertNodeExist(req5.id);
  tree.assertNodeExist(req6.id);
  tree.assertNodeExist(req7.id);
}

describe('Requirement Workspace Tree Display', function () {
  it('should display a tree in milestone mode', () => {
    const referentialData = new ReferentialDataMockBuilder()
      .withProjects(
        { name: 'Project 1', label: 'Etiquette' },
        { name: 'Project 2', label: 'Etiquette 2' },
      )
      .withMilestones(
        {
          label: 'US-Phase',
          description: '',
          endDate: new Date().toISOString(),
          status: 'IN_PROGRESS',
          boundProjectIndexes: [0, 1],
          range: 'GLOBAL',
        },
        {
          label: 'International-Phase',
          description: '',
          endDate: new Date().toISOString(),
          status: 'IN_PROGRESS',
          boundProjectIndexes: [0],
          range: 'GLOBAL',
        },
      )
      .withUser({
        hasAnyReadPermission: true,
        functionalTester: true,
      })
      .build();
    referentialData.globalConfiguration.milestoneFeatureEnabled = true;

    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(
      { dataRows: getNodesInReferentialMode() },
      referentialData,
    );
    const tree = requirementWorkspacePage.tree;
    performAllChecksInReferentialDataMode(tree);
    requirementWorkspacePage.activateMilestoneMode('US-Phase', getNodesInFirstMilestoneMode());
    tree.assertNodeExist(rl1.id);
    tree.assertNodeExist(rl2.id);
    tree.assertNodeIsOpen(rl1.id);
    tree.assertNodeIsClosed(rl2.id);
    tree.assertNodeExist(rf1.id);
    tree.assertNodeExist(rf2.id);
    tree.assertNodeNotExist(req3.id);
    tree.assertNodeNotExist(req4.id);
    tree.assertNodeExist(req5.id);
    tree.assertNodeIsNotSelectable(req5.id);
    tree.assertNodeExist(req6.id);
    tree.assertNodeNotExist(req7.id);
    requirementWorkspacePage.disableMilestoneMode(getNodesInReferentialMode());
    performAllChecksInReferentialDataMode(tree);
    requirementWorkspacePage.activateMilestoneMode(
      'International-Phase',
      getNodesInSecondMilestoneMode(),
    );
    tree.assertNodeExist(rl1.id);
    tree.assertNodeNotExist(rl2.id);
    tree.assertNodeIsOpen(rl1.id);
    tree.assertNodeExist(rf1.id);
    tree.assertNodeExist(rf2.id);
    tree.assertNodeNotExist(req3.id);
    tree.assertNodeExist(req4.id);
    tree.assertNodeExist(req5.id);
    tree.assertNodeIsNotSelectable(req5.id);
    tree.assertNodeNotExist(req6.id);
    tree.assertNodeExist(req7.id);
    requirementWorkspacePage.disableMilestoneMode(getNodesInReferentialMode());
    performAllChecksInReferentialDataMode(tree);
  });
});
