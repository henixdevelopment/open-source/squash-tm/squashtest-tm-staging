import { defaultReferentialData } from '../../../../utils/referential/default-referential-data.const';
import { RequirementFolderViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement-folder/requirement-folder-view.page';
import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { getFavoriteDashboard, getStatistics } from '../requirement-workspace-mock-data';
import { RequirementMultiSelectionPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-multi-selection.page';
import { mockRequirementFolderModel } from '../../../../data-mock/requirements.data-mock';
import { RequirementStatistics } from '../../../../../../projects/sqtm-core/src/lib/model/requirement/requirement-statistics.model';
import { RequirementFolderModel } from '../../../../../../projects/sqtm-core/src/lib/model/requirement/requirement-folder/requirement-folder.model';
import { CustomDashboardModel } from '../../../../../../projects/sqtm-core/src/lib/model/custom-report/custom-dashboard.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

const description = `<p>a nice description</p>
<table><tbody><tr><th>Col1</th><th>Col2</th></tr><tr><td>Col1L1</td><td>Col2L1</td></tr></tbody></table>`;

describe('Requirement Folder View', function () {
  it('should display requirement folder page and information', () => {
    const requirementFolderPage = navigateToRequirementFolder();
    requirementFolderPage.assertExists();
    const informationPanel = requirementFolderPage.showInformationPanel();
    informationPanel.descriptionRichField.checkHtmlContent(description);
  });

  it('should display requirement folder dashboard', () => {
    const requirementFolderViewPage = navigateToRequirementFolder();
    requirementFolderViewPage.assertExists();
    const dashboardPanel = requirementFolderViewPage.showDashboard();
    dashboardPanel.assertTitleExist('Tableau de bord');
    dashboardPanel.orphanRequirementChart.assertChartExist();
    dashboardPanel.orphanRequirementChart.assertHasTitle('Couverture par les cas de test');
    dashboardPanel.statusChart.assertChartExist();
    dashboardPanel.statusChart.assertHasTitle('Statut');
    dashboardPanel.criticalityChart.assertChartExist();
    dashboardPanel.criticalityChart.assertHasTitle('Criticité');
    dashboardPanel.descriptionChart.assertChartExist();
    dashboardPanel.descriptionChart.assertHasTitle('Description');
    dashboardPanel.coverageByCriticalityChart.assertChartExist();
    dashboardPanel.validationByCriticalityChart.assertChartExist();
    dashboardPanel.assertFooterContains('Total des exigences : 10');
  });

  it('should refresh requirement folder dashboard', () => {
    const requirementFolderViewPage = navigateToRequirementFolder();
    requirementFolderViewPage.assertExists();
    const dashboardPanel = requirementFolderViewPage.showDashboard();
    dashboardPanel.assertFooterContains('Total des exigences : 10');
    const updatedStatistics: RequirementStatistics = {
      ...getStatistics(),
      selectedIds: [4, 5, 7, 9, 12, 13, 48, 78],
    };
    dashboardPanel.refreshStatistics(updatedStatistics);
    dashboardPanel.assertFooterContains('Total des exigences : 8');
  });

  it('should display multi selection dashboard', () => {
    // environnement de test
    const requirementMultiSelectionPage = navigateToRequirementMultiSelection();
    requirementMultiSelectionPage.assertExists();
    const statsPanel = requirementMultiSelectionPage.refreshStatistics();
    statsPanel.assertTitleExist('Tableau de bord');
    statsPanel.orphanRequirementChart.assertChartExist();
    statsPanel.orphanRequirementChart.assertHasTitle('Couverture par les cas de test');
    statsPanel.statusChart.assertChartExist();
    statsPanel.statusChart.assertHasTitle('Statut');
    statsPanel.criticalityChart.assertChartExist();
    statsPanel.criticalityChart.assertHasTitle('Criticité');
    statsPanel.descriptionChart.assertChartExist();
    statsPanel.descriptionChart.assertHasTitle('Description');
    statsPanel.coverageByCriticalityChart.assertChartExist();
    statsPanel.validationByCriticalityChart.assertChartExist();
    statsPanel.assertFooterContains('Total des exigences : 10');
  });

  it('should display requirement folder favorite dashboard', () => {
    const requirementLibraryViewPage = navigateToRequirementFolderWithFavoriteDashboard();
    requirementLibraryViewPage.assertExists();
    const dashboardPanel = requirementLibraryViewPage.showDashboard();
    dashboardPanel.assertTitleExist('Favorite Dashboard');
    dashboardPanel.assertCustomDashboardExist();
  });
});

function openRequirementFolderNode(): RequirementWorkspacePage {
  const initialNodes: GridResponse = mockGridResponse('id', [
    mockTreeNode({
      id: 'RequirementLibrary-1',
      children: ['RequirementFolder-3'],
      state: DataRowOpenState.open,
      data: { NAME: 'Project1', CHILD_COUNT: 1 },
    }),
    mockTreeNode({
      id: 'RequirementFolder-3',
      children: [],
      projectId: 1,
      data: { NAME: 'RequirementFolder-3', CHILD_COUNT: 6 },
      parentRowId: 'RequirementLibrary-1',
    }),
  ]);

  return RequirementWorkspacePage.initTestAtPage(initialNodes, defaultReferentialData);
}

function navigateToRequirementFolder(): RequirementFolderViewPage {
  const requirementWorkspacePage = openRequirementFolderNode();
  const model = createRequirementFolderModel(getStatistics(), null);
  return requirementWorkspacePage.tree.selectNode<RequirementFolderViewPage>(
    'RequirementFolder-3',
    model,
  );
}

function navigateToRequirementFolderWithFavoriteDashboard(): RequirementFolderViewPage {
  const requirementWorkspacePage = openRequirementFolderNode();
  const model = createRequirementFolderModel(null, getFavoriteDashboard());
  return requirementWorkspacePage.tree.selectNode<RequirementFolderViewPage>(
    'RequirementFolder-3',
    model,
  );
}

function navigateToRequirementMultiSelection(): RequirementMultiSelectionPage {
  const initialNodes: GridResponse = mockGridResponse('id', [
    mockTreeNode({
      id: 'RequirementLibrary-1',
      children: ['Requirement-3', 'Requirement-4'],
      state: DataRowOpenState.open,
      data: { NAME: 'Project1', CHILD_COUNT: 2 },
    }),
    mockTreeNode({
      id: 'Requirement-3',
      children: [],
      projectId: 1,
      data: {
        NAME: 'Requirement-3',
        CHILD_COUNT: 0,
        CRITICALITY: 'MINOR',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
      },
      parentRowId: 'RequirementLibrary-1',
    }),
    mockTreeNode({
      id: 'Requirement-4',
      children: [],
      projectId: 1,
      data: {
        NAME: 'Requirement-4',
        CHILD_COUNT: 0,
        CRITICALITY: 'MINOR',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
      },
      parentRowId: 'RequirementLibrary-1',
    }),
  ]);
  const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(
    initialNodes,
    defaultReferentialData,
  );

  const model: RequirementFolderModel = mockRequirementFolderModel({
    id: 3,
    projectId: 1,
    name: 'RequirementFolder3',
    customFieldValues: [
      {
        id: 0,
        value: 'a little value',
        cufId: 0,
        fieldType: 'CF',
      },
      {
        id: 1,
        value: '2',
        cufId: 1,
        fieldType: 'NUM',
      },
      {
        id: 2,
        value: 'o1|o2',
        cufId: 2,
        fieldType: 'TAG',
      },
      {
        id: 3,
        value: 'a little value',
        cufId: 3,
        fieldType: 'CF',
      },
      {
        id: 4,
        value: '<p>a little value</p>',
        cufId: 4,
        fieldType: 'RTF',
      },
      {
        id: 5,
        value: '2020-02-14',
        cufId: 5,
        fieldType: 'CF',
      },
      {
        id: 6,
        value: 'true',
        cufId: 6,
        fieldType: 'CF',
      },
      {
        id: 7,
        value: 'Option C',
        cufId: 7,
        fieldType: 'CF',
      },
    ],
    attachmentList: {
      id: 1,
      attachments: [],
    },
    description,
    statistics: getStatistics(),
  });
  requirementWorkspacePage.tree.selectNodes(['Requirement-3', 'Requirement-4'], model);

  return new RequirementMultiSelectionPage(model.statistics);
}

function createRequirementFolderModel(
  statistics: RequirementStatistics,
  dashboard: CustomDashboardModel,
): RequirementFolderModel {
  return {
    canShowFavoriteDashboard: Boolean(dashboard),
    favoriteDashboardId: 1,
    shouldShowFavoriteDashboard: Boolean(dashboard),
    id: 3,
    projectId: 1,
    name: 'RequirementFolder3',
    customFieldValues: [
      {
        id: 0,
        value: 'a little value',
        cufId: 0,
        fieldType: 'CF',
      },
      {
        id: 1,
        value: '2',
        cufId: 1,
        fieldType: 'NUM',
      },
      {
        id: 2,
        value: 'o1|o2',
        cufId: 2,
        fieldType: 'TAG',
      },
      {
        id: 3,
        value: 'a little value',
        cufId: 3,
        fieldType: 'CF',
      },
      {
        id: 4,
        value: '<p>a little value</p>',
        cufId: 4,
        fieldType: 'RTF',
      },
      {
        id: 5,
        value: '2020-02-14',
        cufId: 5,
        fieldType: 'CF',
      },
      {
        id: 6,
        value: 'true',
        cufId: 6,
        fieldType: 'CF',
      },
      {
        id: 7,
        value: 'Option C',
        cufId: 7,
        fieldType: 'CF',
      },
    ],
    attachmentList: {
      id: 1,
      attachments: [],
    },
    description,
    dashboard: dashboard,
    statistics: statistics,
  };
}
