import { AiTestCaseGenerationDialogElement } from '../../../../page-objects/elements/dialog/ai-test-case-generation-dialog.element';
import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import {
  DataRowModel,
  DataRowOpenState,
} from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { defaultReferentialData } from '../../../../utils/referential/default-referential-data.const';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { RequirementVersionModel } from '../../../../../../projects/sqtm-core/src/lib/model/requirement/requirement-version.model';
import { mockRequirementVersionModel } from '../../../../data-mock/requirements.data-mock';
import { RequirementViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';
import { mockProjectPermissions } from '../../../../data-mock/project-permissions.data-mock';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';

describe('Test case generation using artificial intelligence', () => {
  it('Should not be able to update the requirement description', () => {
    const requirementViewPage = initAtRequirementViewPage();
    const generateTestCaseDialog = new AiTestCaseGenerationDialogElement();

    requirementViewPage.clickGenerateTestCaseButton();
    generateTestCaseDialog.requirementDescription.assertExists();
    generateTestCaseDialog.requirementDescription.assertIsNotEditable();
    generateTestCaseDialog.assertContainsDescription(requirementModel.description);
  });

  it('Should pick a project as a destination', () => {
    const requirementViewPage = initAtRequirementViewPage();
    const generateTestCaseDialog = new AiTestCaseGenerationDialogElement();
    requirementViewPage.clickGenerateTestCaseButton();

    const mockDataRows = [
      mockTreeNode({
        id: 'TestCaseLibrary-1',
        children: ['TestCaseFolder-1'],
        data: { NAME: 'Projet 1', CHILD_COUNT: 1 },
        state: DataRowOpenState.closed,
      }),
    ];

    generateTestCaseDialog.pickDestination(mockDataRows, 'TestCaseLibrary-1');
    generateTestCaseDialog.assertDestinationPathDisplayed('Projet 1');
  });

  it('Should pick a folder as a destination', () => {
    const requirementViewPage = initAtRequirementViewPage();
    const generateTestCaseDialog = new AiTestCaseGenerationDialogElement();
    requirementViewPage.clickGenerateTestCaseButton();

    const mockDataRows = [
      mockTreeNode({
        id: 'TestCaseLibrary-1',
        children: ['TestCaseFolder-1'],
        data: { NAME: 'Projet 1', CHILD_COUNT: 1 },
        state: DataRowOpenState.open,
      }),
      mockTreeNode({
        id: 'TestCaseFolder-1',
        children: [],
        data: { NAME: 'Dossier 1', CHILD_COUNT: 0, TCLN_ID: 1 },
        state: DataRowOpenState.leaf,
        parentRowId: 'TestCaseLibrary-1',
      }),
    ];
    const mock = new HttpMockBuilder('ai-test-case-generation/folder-path/*')
      .responseBody({ folderPath: 'Projet 1' })
      .build();
    generateTestCaseDialog.pickDestination(mockDataRows, 'TestCaseFolder-1', mock);
    generateTestCaseDialog.assertDestinationPathDisplayed('Projet 1 > Dossier 1');
  });

  it('Should display generated test cases', () => {
    const requirementViewPage = initAtRequirementViewPage();
    const generateTestCaseDialog = new AiTestCaseGenerationDialogElement();
    requirementViewPage.clickGenerateTestCaseButton();

    generateTestCaseDialog.generateTestCases(mockServerResponse);
    generateTestCaseDialog.assertContainsTestCases([
      `Test de création d'article par un utilisateur authentifié`,
      `Test de modification d'article par un utilisateur autorisé`,
    ]);

    generateTestCaseDialog.collapseTestCaseAndAssertContains(
      `Test de création d'article par un utilisateur authentifié`,
      `Vérifier que le système permet à un utilisateur authentifié de créer un article avec un titre, un contenu et une date de publication.`,
      `Un utilisateur authentifié est connecté au système`,
      [
        {
          index: 0,
          action: `Saisir un titre valide dans le champ titre`,
          expectedResult: `Le titre est saisi correctement`,
        },
        {
          index: 1,
          action: `Saisir un contenu valide dans le champ contenu`,
          expectedResult: `Le contenu est saisi correctement`,
        },
        {
          index: 2,
          action: `Cliquer sur le bouton de création d'article`,
          expectedResult: `L'article est créé avec succès et s'affiche dans la liste des articles de l'utilisateur`,
        },
        {
          index: 3,
          action: `Vérifier que la date de publication est correcte`,
          expectedResult: `La date de publication est correcte`,
        },
      ],
    );
  });

  it('Should display error message if no folder or test case selected', () => {
    const requirementViewPage = initAtRequirementViewPage();
    const generateTestCaseDialog = new AiTestCaseGenerationDialogElement();
    requirementViewPage.clickGenerateTestCaseButton();

    const mockDataRows = [
      mockTreeNode({
        id: 'TestCaseLibrary-1',
        children: ['TestCaseFolder-1'],
        data: { NAME: 'Projet 1', CHILD_COUNT: 1 },
        state: DataRowOpenState.closed,
      }),
    ];

    generateTestCaseDialog.generateTestCases(mockServerResponse);
    generateTestCaseDialog.clickOnConfirmButton();
    generateTestCaseDialog.assertErrorMessageWithNoFolderSelected();
    generateTestCaseDialog.pickDestination(mockDataRows, 'TestCaseLibrary-1');
    generateTestCaseDialog.assertNoErrorMessage();

    generateTestCaseDialog.clickOnConfirmButton();
    generateTestCaseDialog.assertErrorMessageWithNoTestCaseSelected();
    generateTestCaseDialog.selectTestCase(
      `Test de création d'article par un utilisateur authentifié`,
    );
    generateTestCaseDialog.assertNoErrorMessage();
  });
});

function initAtRequirementViewPage(): RequirementViewPage {
  const firstNode = initialNodes.dataRows[0];
  const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes, {
    ...defaultReferentialData,
    projectPermissions: mockProjectPermissions([
      { projectId: 1, qualifiedName: 'squashtest.acl.group.tm.ProjectManager' },
    ]),
    ultimateLicenseAvailable: true,
  });
  new NavBarElement().toggle();
  const tree = requirementWorkspacePage.tree;
  tree.openNode(firstNode.id, getRequirementLibraryChildNodes());
  return tree.selectNode('Requirement-1', { ...requirementModel });
}

const initialNodes: GridResponse = mockGridResponse('id', [
  mockTreeNode({
    id: 'RequirementLibrary-1',
    children: ['Requirement-1'],
    data: {
      NAME: 'zin2 test5',
      CHILD_COUNT: 1,
    },
    state: DataRowOpenState.closed,
  }),
]);

function getRequirementLibraryChildNodes(): DataRowModel[] {
  return [
    mockTreeNode({
      id: 'RequirementLibrary-1',
      children: ['Requirement-1'],
      data: {
        NAME: 'zin2 test5',
        CHILD_COUNT: 1,
      },
      state: DataRowOpenState.open,
    }),
    mockTreeNode({
      id: 'Requirement-1',
      children: [],
      parentRowId: 'RequirementLibrary-1',
      data: {
        NAME: 'Avril 14th',
        CHILD_COUNT: 0,
        CRITICALITY: 'CRITICAL',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
      },
      state: DataRowOpenState.leaf,
    }),
  ];
}

const requirementModel: RequirementVersionModel = mockRequirementVersionModel({
  name: 'Avril 14th',
  status: 'WORK_IN_PROGRESS',
  aiServerId: 1,
  description: 'This is a description',
  canUseAiFeature: true,
});

const mockServerResponse = `{"testCases":[{"name":"Test de création d'article par un utilisateur authentifié","description":"Vérifier que le système permet à un utilisateur authentifié de créer un article avec un titre, un contenu et une date de publication.","prerequisites":"Un utilisateur authentifié est connecté au système","testSteps":[{"index":0,"action":"Accéder à la page de création d'article","expectedResult":"La page de création d'article s'affiche correctement"},{"index":1,"action":"Saisir un titre valide dans le champ titre","expectedResult":"Le titre est saisi correctement"},{"index":2,"action":"Saisir un contenu valide dans le champ contenu","expectedResult":"Le contenu est saisi correctement"},{"index":3,"action":"Cliquer sur le bouton de création d'article","expectedResult":"L'article est créé avec succès et s'affiche dans la liste des articles de l'utilisateur"},{"index":4,"action":"Vérifier que la date de publication est correcte","expectedResult":"La date de publication est correcte"}]},{"name":"Test de modification d'article par un utilisateur autorisé","description":"Vérifier que le système permet à un utilisateur autorisé de modifier un article existant.","prerequisites":"Un utilisateur authentifié est connecté au système et possède un article existant","testSteps":[{"index":0,"action":"Accéder à la page de modification d'article","expectedResult":"La page de modification d'article s'affiche correctement"},{"index":1,"action":"Modifier le titre de l'article","expectedResult":"Le titre est modifié correctement"},{"index":2,"action":"Modifier le contenu de l'article","expectedResult":"Le contenu est modifié correctement"},{"index":3,"action":"Cliquer sur le bouton de sauvegarde","expectedResult":"L'article est modifié avec succès et les modifications sont visibles dans la liste des articles de l'utilisateur"}]}]}`;
