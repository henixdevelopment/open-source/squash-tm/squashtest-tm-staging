import { createEntityReferentialData } from '../../../../utils/referential/create-entity-referential.const';
import { RequirementSearchPage } from '../../../../page-objects/pages/requirement-workspace/search/requirement-search-page';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { NoPermissionToBindMilestoneDialog } from '../../../../page-objects/pages/requirement-workspace/dialogs/NoPermissionToBindMilestoneDialog';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { WorkspaceTypeForPlugins } from '../../../../../../projects/sqtm-core/src/lib/model/project/project-data.model';
import { Project } from '../../../../../../projects/sqtm-core/src/lib/model/project/project.model';
import { Permissions } from '../../../../../../projects/sqtm-core/src/lib/model/permissions/permissions.model';
import { RequirementSearchMilestoneMassEdit } from '../../../../../../projects/sqtm-core/src/lib/model/requirement/requirement-search.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { DataRowModel } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import {
  selectByDataTestDialogButtonId,
  selectByDataTestElementId,
} from '../../../../utils/basic-selectors';
import { basicReferentialData } from '../../../../utils/referential/referential-data.provider';
import { mockDataRow, mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';
import { ReadOnlyPermissions } from '../../../../../../projects/sqtm-core/src/lib/model/permissions/simple-permissions';

describe('Requirement Search Results', function () {
  beforeEach(() => {
    cy.viewport(1200, 720);
  });

  describe('Requirement search table', () => {
    it('should display row in table', () => {
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: [requirementVersionBasic],
      };
      const requirementSearchPage = RequirementSearchPage.initTestAtPage(
        createEntityReferentialData,
        gridResponse,
      );
      const gridElement = requirementSearchPage.grid;
      new NavBarElement().toggle();
      requirementSearchPage.foldFilterPanel();
      requirementSearchPage.clickOnEyeAndWaitResponse();

      gridElement.assertRowExist(1);
      const row = gridElement.getRow(1);
      row.cell(GridColumnId.projectName).textRenderer().assertContainsText('project 1');
      row.cell(GridColumnId.reference).textRenderer().assertContainsText('ref1');
      row.cell(GridColumnId.name).textRenderer().assertContainsText('Exigence 1');
      row.cell(GridColumnId.category).selectRenderer().assertContainText('Fonctionnelle');
      row.cell(GridColumnId.createdBy).textRenderer().assertContainsText('admin');
      row.cell(GridColumnId.lastModifiedBy).textRenderer().assertContainsText('hello');
      row.cell(GridColumnId.lastModifiedBy).textRenderer().assertContainsText('hello');
      row.cell(GridColumnId.milestones).textRenderer().assertContainsText('0');
      row.cell(GridColumnId.coverages).textRenderer().assertContainsText('3');
      row.cell(GridColumnId.attachments).textRenderer().assertContainsText('2');
      row.cell(GridColumnId.versionsCount).textRenderer().assertContainsText('3');
      row.cell(GridColumnId.versionNumber).textRenderer().assertContainsText('2');
    });

    it('should edit cell in table', () => {
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: [requirementVersionBasic],
      };
      const requirementSearchPage = RequirementSearchPage.initTestAtPage(
        createEntityReferentialData,
        gridResponse,
      );
      const gridElement = requirementSearchPage.grid;

      gridElement.assertRowExist(1);
      const row = gridElement.getRow(1);
      const refCell = row.cell('reference').textRenderer();
      refCell.editText('ref02', 'requirement-version/1/reference');
      refCell.assertContainsText('ref02');

      const categoryCell = row.cell('category').selectRenderer();
      categoryCell.changeValue('item-4', 'requirement-version/*/category');
      categoryCell.assertContainText('Métier');

      const httpError = {
        squashTMError: {
          kind: 'FIELD_VALIDATION_ERROR',
          fieldValidationErrors: [
            {
              fieldName: 'name',
              i18nKey: 'sqtm-core.error.generic.duplicate-name',
            },
          ],
        },
      };
      const nameCell = row.cell('name').textRenderer();
      nameCell.editTextError('Requirement-42', 'requirement-version/*/name', httpError);
      nameCell.assertErrorDialogContains('Un élément avec ce nom existe déjà à cet emplacement');
    });

    it('forbid edition for requirement with status approved or obsolete', () => {
      const approvedRequirementVersion: DataRowModel = {
        ...requirementVersionBasic,
        data: {
          ...requirementVersionBasic.data,
          status: 'APPROVED',
        },
      };
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: [approvedRequirementVersion],
      };
      const requirementSearchPage = RequirementSearchPage.initTestAtPage(
        createEntityReferentialData,
        gridResponse,
      );
      const gridElement = requirementSearchPage.grid;

      gridElement.assertRowExist(1);
      const row = gridElement.getRow(1);
      const refCell = row.cell('reference').textRenderer();
      refCell.assertIsNotEditable();
      const nameCell = row.cell('name').textRenderer();
      nameCell.assertIsNotEditable();
    });

    it('should change column displayed when eye clicked', () => {
      const gridResponse: GridResponse = {
        count: 2,
        dataRows: [requirementVersionBasic, requirementVersionUnderReview],
      };
      const requirementSearchPage = RequirementSearchPage.initTestAtPage(
        createEntityReferentialData,
        gridResponse,
      );
      requirementSearchPage.checkIfColumnAreDisplayedThenClick('not.exist');
      requirementSearchPage.clickOnEyeAndWaitResponse();
      requirementSearchPage.checkIfColumnAreDisplayedThenClick('exist');
      requirementSearchPage.clickOnEyeAndWaitResponse();
    });
  });

  describe('Requirement export dialog', () => {
    it('show export dialog', () => {
      const gridResponse: GridResponse = {
        count: 2,
        dataRows: [requirementVersionBasic, requirementVersionUnderReview],
      };
      const requirementSearchPage = RequirementSearchPage.initTestAtPage(
        createEntityReferentialData,
        gridResponse,
      );
      requirementSearchPage.grid.assertRowExist(1);

      const alertDialogElement = requirementSearchPage.showExportDialog();
      alertDialogElement.assertExists();
      alertDialogElement.close();
      alertDialogElement.assertNotExist();
    });
  });

  describe('Requirement mass edit dialog', () => {
    it('should mass edit rows', () => {
      const gridResponse: GridResponse = {
        count: 2,
        dataRows: [requirementVersionBasic, requirementVersionUnderReview],
      };
      const editResponse: GridResponse = {
        count: 2,
        dataRows: [
          requirementVersionBasic,
          mockDataRow({
            id: '2',
            type: 'Requirement',
            projectId: 1,
            data: {
              [GridColumnId.name]: 'Exigence 2',
              [GridColumnId.id]: 1,
              [GridColumnId.reference]: 'ref1',
              [GridColumnId.projectName]: 'project 1',
              [GridColumnId.attachments]: 2,
              [GridColumnId.status]: 'UNDER_REVIEW',
              [GridColumnId.criticality]: 'MINOR',
              [GridColumnId.category]: 1,
              [GridColumnId.createdBy]: 'admin',
              [GridColumnId.lastModifiedBy]: 'hello',
              [GridColumnId.reqMilestoneLocked]: 0,
              [GridColumnId.milestones]: 0,
              [GridColumnId.coverages]: 3,
              [GridColumnId.versionsCount]: 3,
              [GridColumnId.versionNumber]: 2,
              [GridColumnId.requirementId]: 1,
            },
          }),
        ],
      };

      const requirementSearchPage = RequirementSearchPage.initTestAtPage(
        createEntityReferentialData,
        gridResponse,
      );
      const gridElement = requirementSearchPage.grid;
      // gridElement.selectRows([1, 2]);
      gridElement.selectRow(2, '#', 'leftViewport');
      const massEditRequirementDialog = requirementSearchPage.showMassEditDialog();
      massEditRequirementDialog.assertExists();

      const categoryField = massEditRequirementDialog.getOptionalField('category');
      categoryField.check();
      categoryField.selectValue('Fonctionnelle');

      massEditRequirementDialog.confirm(editResponse);

      const row = gridElement.getRow(2);
      const categoryCell = row.cell('category').selectRenderer();
      categoryCell.assertContainText('Fonctionnelle');
    });

    it('should only edit status if obsolete', () => {
      const gridResponse: GridResponse = {
        count: 2,
        dataRows: [requirementVersionBasic, requirementVersionObsolete],
      };

      const requirementSearchPage = RequirementSearchPage.initTestAtPage(
        createEntityReferentialData,
        gridResponse,
      );
      requirementSearchPage.grid.selectRows([1, 3], '#', 'leftViewport');
      const massEditRequirementDialog = requirementSearchPage.showMassEditDialog();
      massEditRequirementDialog.assertExists();
      massEditRequirementDialog.assertExistCanOnlyEditStatusMessage();
    });

    it('should not mass edit if different categories lists', () => {
      const gridResponse: GridResponse = {
        count: 2,
        dataRows: [requirementVersionBasic, requirementVersionOtherProject],
      };

      const differentCategoriesReferentialData: ReferentialDataModel = {
        ...createEntityReferentialData,
        projects: [...createEntityReferentialData.projects, projectWithDifferentCategories],
      };

      const requirementSearchPage = RequirementSearchPage.initTestAtPage(
        differentCategoriesReferentialData,
        gridResponse,
      );
      requirementSearchPage.grid.selectRows([1, 4], '#', 'leftViewport');
      const massEditRequirementDialog = requirementSearchPage.showMassEditDialog();
      massEditRequirementDialog.assertExists();
      massEditRequirementDialog.assertExistDifferentInfoListMessage();
    });

    it('should not display mass edit dialog if no rights on any row', () => {
      const gridResponse: GridResponse = {
        count: 2,
        dataRows: [requirementVersionLockedMilestone, requirementVersionCantWrite],
      };
      const projectsWithDifferentPermissionsReferentialData: ReferentialDataModel = {
        ...createEntityReferentialData,
        projects: [...createEntityReferentialData.projects, projectWithoutWritingPermissions],
      };
      const requirementSearchPage = RequirementSearchPage.initTestAtPage(
        projectsWithDifferentPermissionsReferentialData,
        gridResponse,
      );
      requirementSearchPage.grid.selectRows([5, 6], '#', 'leftViewport');
      const massEditRequirementDialog = requirementSearchPage.showMassEditDialog();
      massEditRequirementDialog.assertNotExist();
      massEditRequirementDialog.assertExistNoWritingRightsDialog();
    });

    it('should mass edit only editable rows', () => {
      const gridResponse: GridResponse = {
        count: 2,
        dataRows: [requirementVersionBasic, requirementVersionLockedMilestone],
      };
      const requirementSearchPage = RequirementSearchPage.initTestAtPage(
        createEntityReferentialData,
        gridResponse,
      );
      requirementSearchPage.grid.selectRows([1, 6], '#', 'leftViewport');
      const massEditRequirementDialog = requirementSearchPage.showMassEditDialog();
      massEditRequirementDialog.assertExists();
      massEditRequirementDialog.assertExistNoWritingRightsMessage();
    });
  });

  describe('Requirement to Milestone mass binding', () => {
    it('should not display dialog if no permissions on project', () => {
      const gridResponse: GridResponse = {
        count: 2,
        dataRows: [requirementVersionLockedMilestone, requirementVersionCantWrite],
      };

      const projectsWithDifferentPermissionsReferentialData: ReferentialDataModel = {
        ...createEntityReferentialData,
        projects: [...createEntityReferentialData.projects, projectWithoutWritingPermissions],
      };
      const requirementSearchPage = RequirementSearchPage.initTestAtPage(
        projectsWithDifferentPermissionsReferentialData,
        gridResponse,
      );
      requirementSearchPage.grid.selectRows([5], '#', 'leftViewport');
      const noPermissionsDialog: NoPermissionToBindMilestoneDialog =
        requirementSearchPage.getNoPermissionToBindMilestoneDialog();
      noPermissionsDialog.assertExists();
    });

    it('should display dialog if permissions OK', () => {
      const gridResponse: GridResponse = {
        count: 2,
        dataRows: [requirementVersionBasic, requirementVersionUnderReview],
      };
      const requirementSearchPage = RequirementSearchPage.initTestAtPage(
        createEntityReferentialData,
        gridResponse,
      );
      requirementSearchPage.grid.selectRows([1, 2], '#', 'leftViewport');
      const massBindingMilestone: RequirementSearchMilestoneMassEdit = {
        milestoneIds: [1, 2],
        checkedIds: [],
        samePerimeter: true,
        requirementVersionIds: [],
      };
      const massBindingMilestoneDialog =
        requirementSearchPage.showMassBindingMilestoneDialog<RequirementSearchMilestoneMassEdit>(
          massBindingMilestone,
        );
      massBindingMilestoneDialog.assertExists();
      // massBindingMilestoneDialog.confirmForReq(gridResponse, false);
    });

    it('should display alert if milestone is already bound to requirement', () => {
      const gridResponse: GridResponse = {
        count: 2,
        dataRows: [requirementVersionBasic, requirementVersionUnderReview],
      };

      const requirementSearchPage = RequirementSearchPage.initTestAtPage(
        createEntityReferentialData,
        gridResponse,
      );
      requirementSearchPage.grid.selectRows([1, 2], '#', 'leftViewport');
      const massBindingMilestone: RequirementSearchMilestoneMassEdit = {
        milestoneIds: [1, 2],
        checkedIds: [1],
        samePerimeter: true,
        requirementVersionIds: [1],
      };
      const massBindingMilestoneDialog =
        requirementSearchPage.showMassBindingMilestoneDialog<RequirementSearchMilestoneMassEdit>(
          massBindingMilestone,
        );
      massBindingMilestoneDialog.assertExists();
      massBindingMilestoneDialog.confirmForReq(gridResponse, true);
      const milestoneAlreadyBoundDialog = requirementSearchPage.getMilestoneAlreadyBoundDialog();
      milestoneAlreadyBoundDialog.assertMessage();
    });

    it('should display message if selection has different perimeter', () => {
      const gridResponse: GridResponse = {
        count: 2,
        dataRows: [requirementVersionBasic, requirementVersionUnderReview],
      };
      const requirementSearchPage = RequirementSearchPage.initTestAtPage(
        createEntityReferentialData,
        gridResponse,
      );
      requirementSearchPage.clickOnEyeAndWaitResponse();
      requirementSearchPage.grid.selectRows([1, 2], '#', 'leftViewport');
      const massBindingMilestone: RequirementSearchMilestoneMassEdit = {
        milestoneIds: [1, 2],
        checkedIds: [],
        samePerimeter: false,
        requirementVersionIds: [],
      };
      const massBindingMilestoneDialog =
        requirementSearchPage.showMassBindingMilestoneDialog<RequirementSearchMilestoneMassEdit>(
          massBindingMilestone,
        );
      massBindingMilestoneDialog.assertExists();
      massBindingMilestoneDialog.assertNoSamePerimeterMessage();
    });

    it('should have another icon with overlay if premium plugin installed', () => {
      const gridResponse = mockGridResponse(
        '2',
        [requirementVersionBasic, requirementVersionUnderReview],
        ['#', 'name', 'createdBy', 'folder', 'detail'],
      );

      const refData = basicReferentialData;
      refData.premiumPluginInstalled = true;

      const requirementSearchPage = RequirementSearchPage.initTestAtPremiumPage(
        refData,
        gridResponse,
      );
      const columnIdsDisplayedFirst = ['name', 'createdBy', '#', 'folder', 'detail'];
      const columnIdsUnDisplayedFirst = [
        'milestones',
        'projectName',
        'requirementId',
        'id',
        'reference',
        'status',
        'criticality',
        'category',
        'lastModifiedBy',
        'versionNumber',
        'versionsCount',
        'attachments',
        'coverages',
      ];
      const columnConfirmed = ['Criticité', 'Référence', 'Catégorie'];
      const columnConfirmedForGrid = ['criticality', 'reference', 'category'];
      const visibleColumnsAfterReset = ['projectName', 'requirementId', 'id', 'versionsCount'];

      requirementSearchPage.checkIfColumnsAreDisplayed(columnIdsDisplayedFirst, 'exist');
      requirementSearchPage.checkIfColumnsAreDisplayed(columnIdsUnDisplayedFirst, 'not.exist');

      const columnList = requirementSearchPage.openColumnList([
        'name',
        'createdBy',
        '#',
        'folder',
        'detail',
      ]);
      cy.get(selectByDataTestDialogButtonId('cancel')).should('exist').click();
      columnList.assertNotExist();

      requirementSearchPage.openColumnList(['name', 'createdBy', '#', 'folder', 'detail']);

      columnConfirmed.forEach((componentId) => {
        cy.get(selectByDataTestElementId('item')).contains(componentId).click();
      });
      requirementSearchPage.grid.filterPanel.confirmNewConf();
      columnList.assertNotExist();

      requirementSearchPage.checkIfColumnsAreDisplayed(columnConfirmedForGrid, 'exist');

      requirementSearchPage.openColumnList(['name', 'createdBy', '#', 'folder', 'detail']);

      requirementSearchPage.grid.filterPanel.resetConf();
      columnList.assertNotExist();
      requirementSearchPage.checkIfColumnsAreDisplayed(columnIdsDisplayedFirst, 'exist');
      requirementSearchPage.checkIfColumnsAreDisplayed(visibleColumnsAfterReset, 'exist');
    });

    it('should display alert if selected requirements projects share no milestones', () => {
      const gridResponse: GridResponse = {
        count: 2,
        dataRows: [requirementVersionBasic, requirementVersionOtherProject],
      };

      const differentCategoriesReferentialData: ReferentialDataModel = {
        ...createEntityReferentialData,
        projects: [...createEntityReferentialData.projects, projectWithDifferentCategories],
      };

      const requirementSearchPage = RequirementSearchPage.initTestAtPage(
        differentCategoriesReferentialData,
        gridResponse,
      );
      requirementSearchPage.grid.selectRows([1, 4], '#', 'leftViewport');
      const massBindingMilestone: RequirementSearchMilestoneMassEdit = {
        milestoneIds: [],
        checkedIds: [],
        samePerimeter: false,
        requirementVersionIds: [],
      };
      const massBindingMilestoneDialog =
        requirementSearchPage.showMassBindingMilestoneDialog<RequirementSearchMilestoneMassEdit>(
          massBindingMilestone,
        );
      massBindingMilestoneDialog.assertNotExist();
      const noMilestoneSharedDialog = requirementSearchPage.getNoMilestoneSharedDialog();
      noMilestoneSharedDialog.assertMessage();
    });
  });
});

const requirementVersionBasic = mockTreeNode({
  id: '1',
  projectId: 1,
  data: {
    [GridColumnId.name]: 'Exigence 1',
    [GridColumnId.id]: 1,
    [GridColumnId.reference]: 'ref1',
    [GridColumnId.projectName]: 'project 1',
    [GridColumnId.attachments]: 2,
    [GridColumnId.status]: 'WORK_IN_PROGRESS',
    [GridColumnId.criticality]: 'CRITICAL',
    [GridColumnId.category]: 1,
    [GridColumnId.createdBy]: 'admin',
    [GridColumnId.lastModifiedBy]: 'hello',
    [GridColumnId.reqMilestoneLocked]: 0,
    [GridColumnId.milestones]: 0,
    [GridColumnId.coverages]: 3,
    [GridColumnId.versionsCount]: 3,
    [GridColumnId.versionNumber]: 2,
    [GridColumnId.requirementId]: 1,
  },
});

const requirementVersionUnderReview: DataRowModel = {
  ...requirementVersionBasic,
  id: '2',
  data: {
    ...requirementVersionBasic.data,
    id: 2,
    reference: 'ref2',
    status: 'UNDER_REVIEW',
    requirementId: '2',
  },
};

const requirementVersionObsolete: DataRowModel = {
  ...requirementVersionBasic,
  id: '3',
  data: {
    ...requirementVersionBasic.data,
    id: 3,
    reference: 'ref3',
    status: 'OBSOLETE',
    requirementId: '3',
  },
};

const requirementVersionOtherProject: DataRowModel = {
  ...requirementVersionBasic,
  id: '4',
  projectId: 3,
  data: {
    ...requirementVersionBasic.data,
    id: 4,
    reference: 'ref4',
    category: 2,
    requirementId: '4',
  },
};

const requirementVersionCantWrite: DataRowModel = {
  ...requirementVersionBasic,
  id: '5',
  projectId: 4,
  data: {
    ...requirementVersionBasic.data,
    id: 5,
    reference: 'ref5',
    requirementId: '5',
  },
  simplePermissions: new ReadOnlyPermissions(),
};

const requirementVersionLockedMilestone: DataRowModel = {
  ...requirementVersionBasic,
  id: '6',
  data: {
    ...requirementVersionBasic.data,
    id: 6,
    reference: 'ref6',
    reqMilestoneLocked: 1,
    requirementId: '6',
  },
};

const projectWithDifferentCategories: Project = {
  id: 3,
  milestoneBindings: [
    {
      id: 4,
      milestoneId: 4,
      projectId: 1,
    },
  ],
  requirementCategoryId: 2,
  testCaseNatureId: 2,
  testCaseTypeId: 3,
  customFieldBindings: {
    [BindableEntity.REQUIREMENT_FOLDER]: [],
    [BindableEntity.REQUIREMENT_VERSION]: [],
    [BindableEntity.TESTCASE_FOLDER]: [],
    [BindableEntity.TEST_CASE]: [
      {
        boundProjectId: 1,
        customFieldId: 12,
        id: 1,
        position: 0,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE,
      },
      {
        boundProjectId: 1,
        customFieldId: 13,
        id: 1,
        position: 2,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE,
      },
      {
        boundProjectId: 1,
        customFieldId: 14,
        id: 1,
        position: 1,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE,
      },
      {
        boundProjectId: 1,
        customFieldId: 15,
        id: 1,
        position: 3,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE,
      },
      {
        boundProjectId: 1,
        customFieldId: 16,
        id: 1,
        position: 4,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE,
      },
      {
        boundProjectId: 1,
        customFieldId: 17,
        id: 1,
        position: 5,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE,
      },
      {
        boundProjectId: 1,
        customFieldId: 18,
        id: 1,
        position: 6,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE,
      },
      {
        boundProjectId: 1,
        customFieldId: 19,
        id: 1,
        position: 7,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE,
      },
    ],
    [BindableEntity.TEST_STEP]: [],
    [BindableEntity.CAMPAIGN_FOLDER]: [],
    [BindableEntity.CAMPAIGN]: [
      {
        boundProjectId: 1,
        customFieldId: 12,
        id: 1,
        position: 0,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN,
      },
      {
        boundProjectId: 1,
        customFieldId: 13,
        id: 1,
        position: 2,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN,
      },
      {
        boundProjectId: 1,
        customFieldId: 14,
        id: 1,
        position: 1,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN,
      },
      {
        boundProjectId: 1,
        customFieldId: 15,
        id: 1,
        position: 3,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN,
      },
      {
        boundProjectId: 1,
        customFieldId: 16,
        id: 1,
        position: 4,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN,
      },
      {
        boundProjectId: 1,
        customFieldId: 17,
        id: 1,
        position: 5,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN,
      },
      {
        boundProjectId: 1,
        customFieldId: 18,
        id: 1,
        position: 6,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN,
      },
      {
        boundProjectId: 1,
        customFieldId: 19,
        id: 1,
        position: 7,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN,
      },
    ],
    [BindableEntity.ITERATION]: [
      {
        boundProjectId: 1,
        customFieldId: 12,
        id: 1,
        position: 0,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION,
      },
      {
        boundProjectId: 1,
        customFieldId: 13,
        id: 1,
        position: 2,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION,
      },
      {
        boundProjectId: 1,
        customFieldId: 14,
        id: 1,
        position: 1,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION,
      },
      {
        boundProjectId: 1,
        customFieldId: 15,
        id: 1,
        position: 3,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION,
      },
      {
        boundProjectId: 1,
        customFieldId: 16,
        id: 1,
        position: 4,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION,
      },
      {
        boundProjectId: 1,
        customFieldId: 17,
        id: 1,
        position: 5,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION,
      },
      {
        boundProjectId: 1,
        customFieldId: 18,
        id: 1,
        position: 6,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION,
      },
      {
        boundProjectId: 1,
        customFieldId: 19,
        id: 1,
        position: 7,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION,
      },
    ],
    [BindableEntity.TEST_SUITE]: [],
    [BindableEntity.EXECUTION]: [],
    [BindableEntity.EXECUTION_STEP]: [],
    [BindableEntity.CUSTOM_REPORT_FOLDER]: [],
    [BindableEntity.SPRINT_GROUP]: [],
    [BindableEntity.SPRINT]: [],
  },
  permissions: {
    REQUIREMENT_LIBRARY: [Permissions.READ, Permissions.CREATE, Permissions.WRITE],
    TEST_CASE_LIBRARY: [Permissions.READ, Permissions.CREATE, Permissions.WRITE],
    CAMPAIGN_LIBRARY: [Permissions.READ, Permissions.CREATE, Permissions.WRITE],
    PROJECT: [],
  },
  allowAutomationWorkflow: true,
  activatedPlugins: {
    [WorkspaceTypeForPlugins.CAMPAIGN_WORKSPACE]: [],
    [WorkspaceTypeForPlugins.TEST_CASE_WORKSPACE]: [],
    [WorkspaceTypeForPlugins.REQUIREMENT_WORKSPACE]: [],
  },
} as Project;

const projectWithoutWritingPermissions: Project = {
  id: 4,
  milestoneBindings: [
    {
      id: 4,
      milestoneId: 4,
      projectId: 1,
    },
  ],
  requirementCategoryId: 2,
  testCaseNatureId: 2,
  testCaseTypeId: 3,
  customFieldBindings: {
    [BindableEntity.REQUIREMENT_FOLDER]: [],
    [BindableEntity.REQUIREMENT_VERSION]: [],
    [BindableEntity.TESTCASE_FOLDER]: [],
    [BindableEntity.TEST_CASE]: [
      {
        boundProjectId: 1,
        customFieldId: 12,
        id: 1,
        position: 0,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE,
      },
      {
        boundProjectId: 1,
        customFieldId: 13,
        id: 1,
        position: 2,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE,
      },
      {
        boundProjectId: 1,
        customFieldId: 14,
        id: 1,
        position: 1,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE,
      },
      {
        boundProjectId: 1,
        customFieldId: 15,
        id: 1,
        position: 3,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE,
      },
      {
        boundProjectId: 1,
        customFieldId: 16,
        id: 1,
        position: 4,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE,
      },
      {
        boundProjectId: 1,
        customFieldId: 17,
        id: 1,
        position: 5,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE,
      },
      {
        boundProjectId: 1,
        customFieldId: 18,
        id: 1,
        position: 6,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE,
      },
      {
        boundProjectId: 1,
        customFieldId: 19,
        id: 1,
        position: 7,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE,
      },
    ],
    [BindableEntity.TEST_STEP]: [],
    [BindableEntity.CAMPAIGN_FOLDER]: [],
    [BindableEntity.CAMPAIGN]: [
      {
        boundProjectId: 1,
        customFieldId: 12,
        id: 1,
        position: 0,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN,
      },
      {
        boundProjectId: 1,
        customFieldId: 13,
        id: 1,
        position: 2,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN,
      },
      {
        boundProjectId: 1,
        customFieldId: 14,
        id: 1,
        position: 1,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN,
      },
      {
        boundProjectId: 1,
        customFieldId: 15,
        id: 1,
        position: 3,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN,
      },
      {
        boundProjectId: 1,
        customFieldId: 16,
        id: 1,
        position: 4,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN,
      },
      {
        boundProjectId: 1,
        customFieldId: 17,
        id: 1,
        position: 5,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN,
      },
      {
        boundProjectId: 1,
        customFieldId: 18,
        id: 1,
        position: 6,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN,
      },
      {
        boundProjectId: 1,
        customFieldId: 19,
        id: 1,
        position: 7,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN,
      },
    ],
    [BindableEntity.ITERATION]: [
      {
        boundProjectId: 1,
        customFieldId: 12,
        id: 1,
        position: 0,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION,
      },
      {
        boundProjectId: 1,
        customFieldId: 13,
        id: 1,
        position: 2,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION,
      },
      {
        boundProjectId: 1,
        customFieldId: 14,
        id: 1,
        position: 1,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION,
      },
      {
        boundProjectId: 1,
        customFieldId: 15,
        id: 1,
        position: 3,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION,
      },
      {
        boundProjectId: 1,
        customFieldId: 16,
        id: 1,
        position: 4,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION,
      },
      {
        boundProjectId: 1,
        customFieldId: 17,
        id: 1,
        position: 5,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION,
      },
      {
        boundProjectId: 1,
        customFieldId: 18,
        id: 1,
        position: 6,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION,
      },
      {
        boundProjectId: 1,
        customFieldId: 19,
        id: 1,
        position: 7,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION,
      },
    ],
    [BindableEntity.TEST_SUITE]: [],
    [BindableEntity.EXECUTION]: [],
    [BindableEntity.EXECUTION_STEP]: [],
    [BindableEntity.CUSTOM_REPORT_FOLDER]: [],
    [BindableEntity.SPRINT]: [],
    [BindableEntity.SPRINT_GROUP]: [],
  },
  permissions: {
    REQUIREMENT_LIBRARY: [Permissions.READ],
    TEST_CASE_LIBRARY: [Permissions.READ],
    CAMPAIGN_LIBRARY: [Permissions.READ],
    PROJECT: [],
  },
  allowAutomationWorkflow: true,
  activatedPlugins: {
    [WorkspaceTypeForPlugins.CAMPAIGN_WORKSPACE]: [],
    [WorkspaceTypeForPlugins.TEST_CASE_WORKSPACE]: [],
    [WorkspaceTypeForPlugins.REQUIREMENT_WORKSPACE]: [],
  },
} as Project;
