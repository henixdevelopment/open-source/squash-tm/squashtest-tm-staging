import { createEntityReferentialData } from '../../../../utils/referential/create-entity-referential.const';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { DataRowModel } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { RequirementForSprintSearchPage } from '../../../../page-objects/pages/requirement-workspace/search/requirement-for-sprint-search-page';
import { beforeEach } from 'mocha';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import { AlertDialogElement } from '../../../../page-objects/elements/dialog/alert-dialog.element';
import { BindReqVersionToSprintOperationReport } from '../../../../../../projects/sqtm-core/src/lib/model/change-coverage-operation-report';
import { mockRequirementVersionDataRow } from '../../../../data-mock/requirements.data-mock';

function assertRedirectionToCampaignWorkspaceDone() {
  cy.url().should('equal', `${Cypress.config().baseUrl}/campaign-workspace`);
}

describe('Requirement for sprint search page', () => {
  let forSprintSearchPage: RequirementForSprintSearchPage;
  let gridElement: GridElement;

  const requirementVersion: DataRowModel = mockRequirementVersionDataRow();

  beforeEach(() => {
    forSprintSearchPage = RequirementForSprintSearchPage.initTestAtPage(
      '1',
      createEntityReferentialData,
      {
        count: 1,
        dataRows: [requirementVersion],
      },
    );
    gridElement = forSprintSearchPage.grid;
  });

  afterEach(() => {
    forSprintSearchPage = null;
    gridElement = null;
  });

  it('should show and activate top buttons when row is selected', () => {
    forSprintSearchPage.assertExists();
    forSprintSearchPage.assertShowAllColumnsButtonExist();
    forSprintSearchPage.assertShowAllColumnsButtonIsActive();
    forSprintSearchPage.assertLinkSelectionButtonExist();
    forSprintSearchPage.assertLinkSelectionButtonIsNotActive();
    forSprintSearchPage.assertLinkAllButtonExist();
    forSprintSearchPage.assertLinkAllButtonIsActive();
    forSprintSearchPage.assertNavigateBackButtonExist();
    forSprintSearchPage.assertNavigateBackButtonIsActive();
    gridElement.selectRow('1', '#', 'leftViewport');
    forSprintSearchPage.assertLinkSelectionButtonIsActive();
    gridElement.toggleRow('1', '#', 'leftViewport');
    forSprintSearchPage.assertLinkSelectionButtonIsNotActive();
  });

  it('should link a requirement', () => {
    gridElement.selectRow('1', '#', 'leftViewport');
    const campaignTreeMock = buildCampaignTreeMock();
    forSprintSearchPage.linkSelection();
    campaignTreeMock.wait();
    assertRedirectionToCampaignWorkspaceDone();
  });

  it('should link all requirement', () => {
    const campaignTreeMock = buildCampaignTreeMock();
    forSprintSearchPage.linkAll();
    campaignTreeMock.wait();
    assertRedirectionToCampaignWorkspaceDone();
  });

  it('should show error popup when trying to link a requirement to a closed sprint', () => {
    const response = {
      squashTMError: {
        kind: 'ACTION_ERROR',
        actionValidationError: {
          exception: 'SprintNotLinkableException',
          i18nKey: 'sqtm-core.error.sprint.sprint-closed-rejection',
        },
      },
    };
    const alertDialog = forSprintSearchPage.linkAllWithSprintClosed(response);
    alertDialog.assertExists();
    alertDialog.assertExclamationCircleIconExists();
    alertDialog.assertHasMessage('Action impossible car le sprint est fermé.');
  });

  it('should not link a requirement and show error dialog with 3 exceptions', () => {
    gridElement.selectRow('1', '#', 'leftViewport');
    const response: BindReqVersionToSprintOperationReport = {
      linkedReqVersions: [],
      summary: {
        reqVersionsAlreadyLinkedToSprint: [requirementVersion.data[GridColumnId.name]],
        highLevelReqVersionsInSelection: [requirementVersion.data[GridColumnId.name]],
        obsoleteReqVersionsInSelection: [requirementVersion.data[GridColumnId.name]],
      },
    };

    buildCampaignTreeMock();

    forSprintSearchPage.linkSelection(response);

    const alertDialogElement = new AlertDialogElement('bind-req-version-to-sprint');
    alertDialogElement.assertExists();
    alertDialogElement.assertHasMessage(
      "Votre sélection comporte des exigences qui n'ont pas pu être associées au sprint. " +
        "Les exigences pouvant l'être ont cependant bien été associées.",
    );
    alertDialogElement.assertHasMessage('Exigences de haut niveau');
    alertDialogElement.assertHasMessage('Exigences obsolètes');
    alertDialogElement.assertHasMessage('Exigences déjà liées à ce sprint');
    alertDialogElement.close();
  });
});

function buildCampaignTreeMock() {
  const campaignTree: GridResponse = { count: 0, dataRows: [] };
  return new HttpMockBuilder(`/campaign-tree`).post().responseBody(campaignTree).build();
}
