import { createEntityReferentialData } from '../../../../utils/referential/create-entity-referential.const';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { RequirementForCoverageSearchPage } from '../../../../page-objects/pages/requirement-workspace/search/requirement-for-coverage-search-page';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { AlertDialogElement } from '../../../../page-objects/elements/dialog/alert-dialog.element';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { mockRequirementVersionDataRow } from '../../../../data-mock/requirements.data-mock';

function assertRedirectionToTestCaseWorkspaceDone() {
  cy.url().should('equal', `${Cypress.config().baseUrl}/test-case-workspace`);
}

describe('Requirement for coverage search page', function () {
  beforeEach(() => {
    cy.viewport(1200, 720);
  });

  describe('Requirement search table', () => {
    const requirementVersion = mockRequirementVersionDataRow();

    it('should show links buttons and activate according to user selection', () => {
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: [requirementVersion],
      };
      const forCoverageSearchPage = RequirementForCoverageSearchPage.initTestAtPage(
        '1',
        createEntityReferentialData,
        gridResponse,
      );
      const gridElement = forCoverageSearchPage.grid;
      new NavBarElement().toggle();
      forCoverageSearchPage.foldFilterPanel();
      forCoverageSearchPage.assertLinkSelectionButtonExist();
      forCoverageSearchPage.assertLinkSelectionButtonIsNotActive();
      forCoverageSearchPage.assertLinkAllButtonExist();
      forCoverageSearchPage.assertLinkAllButtonIsActive();
      forCoverageSearchPage.assertNavigateBackButtonExist();
      forCoverageSearchPage.assertNavigateBackButtonIsActive();
      gridElement.selectRow('1', '#', 'leftViewport');
      forCoverageSearchPage.assertLinkSelectionButtonIsActive();
      gridElement.toggleRow('1', '#', 'leftViewport');
      forCoverageSearchPage.assertLinkSelectionButtonIsNotActive();
    });

    it('should link a requirement', () => {
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: [requirementVersion],
      };
      const forCoverageSearchPage = RequirementForCoverageSearchPage.initTestAtPage(
        '1',
        createEntityReferentialData,
        gridResponse,
      );
      const gridElement = forCoverageSearchPage.grid;
      new NavBarElement().toggle();
      gridElement.selectRow('1', '#', 'leftViewport');
      const testCaseTreeMock = buildTestCaseTreeMock();
      forCoverageSearchPage.linkSelection('1');
      testCaseTreeMock.wait();
      assertRedirectionToTestCaseWorkspaceDone();
    });

    it('should link all requirement', () => {
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: [requirementVersion],
      };
      const forCoverageSearchPage = RequirementForCoverageSearchPage.initTestAtPage(
        '1',
        createEntityReferentialData,
        gridResponse,
      );
      new NavBarElement().toggle();
      const testCaseTreeMock = buildTestCaseTreeMock();
      forCoverageSearchPage.linkAll('1');
      testCaseTreeMock.wait();
      assertRedirectionToTestCaseWorkspaceDone();
    });

    it('should link a requirement and show error', () => {
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: [requirementVersion],
      };
      const forCoverageSearchPage = RequirementForCoverageSearchPage.initTestAtPage(
        '1',
        createEntityReferentialData,
        gridResponse,
      );
      const gridElement = forCoverageSearchPage.grid;
      new NavBarElement().toggle();
      gridElement.selectRow('1', '#', 'leftViewport');
      forCoverageSearchPage.linkSelection('1', {
        coverages: [],
        summary: {
          alreadyVerifiedRejections: true,
          notLinkableRejections: true,
          noVerifiableVersionRejections: true,
        },
      });
      const alertDialogElement = new AlertDialogElement('coverage-report');
      alertDialogElement.assertExists();
      alertDialogElement.assertHasMessage(
        "Au moins une des exigences sélectionnées n'a pas été associée au cas de test parce que celui-ci vérifiait déjà l'exigence.",
      );
      alertDialogElement.assertHasMessage(
        "Au moins une des exigences n'a pu être associée au cas de test car elle n'a que une/des version(s) obsolète(s).",
      );
      alertDialogElement.assertHasMessage(
        "Au moins une des exigences sélectionnées n'a pas pu être associée au cas de test parce que son statut l'interdit.",
      );
      alertDialogElement.close();
    });
  });
});

function buildTestCaseTreeMock() {
  const testCaseTree: GridResponse = { count: 0, dataRows: [] };
  return new HttpMockBuilder(`/test-case-tree`).post().responseBody(testCaseTree).build();
}
