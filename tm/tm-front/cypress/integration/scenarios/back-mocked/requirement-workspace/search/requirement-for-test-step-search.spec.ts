import { createEntityReferentialData } from '../../../../utils/referential/create-entity-referential.const';
import { RequirementForTestStepSearchPage } from '../../../../page-objects/pages/requirement-workspace/search/requirement-for-test-step-search-page';
import { AlertDialogElement } from '../../../../page-objects/elements/dialog/alert-dialog.element';
import { DataRowModel } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { mockRequirementDataRows } from '../requirement-workspace-mock-data';

describe('Requirement for test step search page', function () {
  beforeEach(() => {
    cy.viewport(1200, 720);
  });

  describe('Requirement search table', () => {
    const requirementVersionRows: DataRowModel[] = mockRequirementDataRows();
    let forTestStepSearchPage: RequirementForTestStepSearchPage;

    beforeEach(() => {
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: requirementVersionRows,
      };
      forTestStepSearchPage = RequirementForTestStepSearchPage.initTestAtPage(
        '1',
        '2',
        createEntityReferentialData,
        gridResponse,
      );
    });

    afterEach(() => (forTestStepSearchPage = null));

    it('should show links buttons and activate according to user selection', () => {
      const gridElement = forTestStepSearchPage.grid;
      forTestStepSearchPage.foldFilterPanel();
      forTestStepSearchPage.assertLinkSelectionButtonExist();
      forTestStepSearchPage.assertLinkSelectionButtonIsNotActive();
      forTestStepSearchPage.assertLinkAllButtonExist();
      forTestStepSearchPage.assertLinkAllButtonIsActive();
      forTestStepSearchPage.assertNavigateBackButtonExist();
      forTestStepSearchPage.assertNavigateBackButtonIsActive();
      gridElement.selectRow('1234', '#', 'leftViewport');
      forTestStepSearchPage.assertLinkSelectionButtonIsActive();
      gridElement.toggleRow('1234', '#', 'leftViewport');
      forTestStepSearchPage.assertLinkSelectionButtonIsNotActive();
    });

    it('should link a requirement', () => {
      forTestStepSearchPage.grid.selectRow('1234', '#', 'leftViewport');
      forTestStepSearchPage.linkSelection('1', '2');
    });

    it('should link all requirement', () => {
      forTestStepSearchPage.linkAll('1', '2');
    });

    it('should link a requirement and show error', () => {
      forTestStepSearchPage.grid.selectRow('1234', '#', 'leftViewport');
      forTestStepSearchPage.linkSelection('1', '2', {
        coverages: [],
        summary: {
          alreadyVerifiedRejections: true,
          notLinkableRejections: true,
          noVerifiableVersionRejections: true,
        },
      });
      const alertDialogElement = new AlertDialogElement('coverage-report');
      alertDialogElement.assertExists();
      alertDialogElement.assertHasMessage(
        "Au moins une des exigences sélectionnées n'a pas été associée au cas de test parce que celui-ci vérifiait déjà l'exigence.",
      );
      alertDialogElement.assertHasMessage(
        "Au moins une des exigences n'a pu être associée au cas de test car elle n'a que une/des version(s) obsolète(s).",
      );
      alertDialogElement.assertHasMessage(
        "Au moins une des exigences sélectionnées n'a pas pu être associée au cas de test parce que son statut l'interdit.",
      );
      alertDialogElement.close();
    });
  });
});
