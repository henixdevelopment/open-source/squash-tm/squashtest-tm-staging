import { RequirementStatistics } from '../../../../../projects/sqtm-core/src/lib/model/requirement/requirement-statistics.model';
import { CustomDashboardModel } from '../../../../../projects/sqtm-core/src/lib/model/custom-report/custom-dashboard.model';
import {
  ChartColumnType,
  ChartDataType,
  ChartOperation,
  ChartScopeType,
  ChartType,
} from '../../../../../projects/sqtm-core/src/lib/model/custom-report/chart-definition.model';
import { EntityType } from '../../../../../projects/sqtm-core/src/lib/model/entity.model';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { mockGridResponse, mockTreeNode } from '../../../data-mock/grid.data-mock';
import {
  DataRowModel,
  DataRowOpenState,
} from '../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { GridColumnId } from '../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';

export function getStatistics(): RequirementStatistics {
  return {
    boundTestCasesStatistics: {
      zeroTestCases: 3,
      manyTestCases: 4,
      oneTestCase: 3,
    },
    statusesStatistics: {
      approved: 5,
      workInProgress: 4,
      obsolete: 0,
      underReview: 1,
    },
    criticalityStatistics: {
      critical: 2,
      major: 3,
      minor: 1,
      undefined: 4,
    },
    boundDescriptionStatistics: {
      hasNoDescription: 1,
      hasDescription: 9,
    },
    coverageStatistics: {
      critical: 2,
      totalCritical: 2,
      major: 2,
      totalMajor: 3,
      minor: 0,
      totalMinor: 1,
      undefined: 1,
      totalUndefined: 4,
    },
    validationStatistics: {
      conclusiveUndefined: 2,
      conclusiveMinor: 5,
      conclusiveMajor: 2,
      conclusiveCritical: 8,

      inconclusiveUndefined: 4,
      inconclusiveMajor: 1,
      inconclusiveMinor: 2,
      inconclusiveCritical: 3,

      undefinedUndefined: 3,
      undefinedMajor: 1,
      undefinedMinor: 2,
      undefinedCritical: 0,
      legacy: false,
    },
    generatedOn: new Date(),
    selectedIds: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
  };
}

export function getFavoriteDashboard(): CustomDashboardModel {
  return {
    id: 2,
    projectId: 1,
    customReportLibraryNodeId: 18,
    name: 'Favorite Dashboard',
    createdBy: 'cypress',
    chartBindings: [
      {
        id: 2,
        chartDefinitionId: 2,
        dashboardId: 2,
        chartInstance: {
          id: 2,
          customReportLibraryNodeId: null,
          projectId: 1,
          name: 'New Chart',
          type: ChartType.PIE,
          measures: [
            {
              cufId: null,
              label: '',
              column: {
                id: 1,
                columnType: ChartColumnType.ATTRIBUTE,
                label: 'TEST_CASE_ID',
                specializedType: { entityType: EntityType.TEST_CASE },
                dataType: ChartDataType.NUMERIC,
              },
              operation: ChartOperation.COUNT,
            },
          ],
          axis: [
            {
              cufId: null,
              label: '',
              column: {
                id: 1,
                columnType: ChartColumnType.ATTRIBUTE,
                label: 'TEST_CASE_REFERENCE',
                specializedType: { entityType: EntityType.TEST_CASE },
                dataType: ChartDataType.STRING,
              },
              operation: ChartOperation.NONE,
            },
          ],
          filters: [],
          abscissa: [['']],
          series: { '': [2] },
          projectScope: [],
          scope: [],
          scopeType: ChartScopeType.DEFAULT,
          createdOn: new Date().toISOString(),
          createdBy: 'admin',
          lastModifiedOn: new Date().toISOString(),
          lastModifiedBy: 'admin',
          colours: [],
        },
        row: 1,
        col: 1,
        sizeX: 2,
        sizeY: 2,
      },
    ],
    reportBindings: [],
    favoriteWorkspaces: [],
  };
}

export function mockRequirementTreeResponse(): GridResponse {
  return mockGridResponse('id', [
    mockTreeNode({
      id: 'Requirement-347232',
      children: [],
      state: DataRowOpenState.leaf,
      data: {
        RLN_ID: 347232,
        projectId: 327,
        NAME: '1.03 - Exigence 3',
        HAS_DESCRIPTION: false,
        REFERENCE: '1.03',
        CRITICALITY: 'MINOR',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        REQ_CATEGORY_ICON: 'monitor',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_FUNCTIONAL',
        REQ_CATEGORY_TYPE: 'SYS',
        CHILD_COUNT: 0,
        COVERAGE_COUNT: 1,
        IS_SYNCHRONIZED: false,
      },
      projectId: 327,
      parentRowId: 'RequirementLibrary-327',
    }),
    mockTreeNode({
      id: 'Requirement-347231',
      children: [],
      state: DataRowOpenState.leaf,
      data: {
        RLN_ID: 347231,
        projectId: 327,
        NAME: '1.02 - Exigence 2',
        HAS_DESCRIPTION: false,
        REFERENCE: '1.02',
        CRITICALITY: 'CRITICAL',
        REQUIREMENT_STATUS: 'OBSOLETE',
        REQ_CATEGORY_ICON: 'monitor',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_FUNCTIONAL',
        REQ_CATEGORY_TYPE: 'SYS',
        CHILD_COUNT: 0,
        COVERAGE_COUNT: 4,
        IS_SYNCHRONIZED: false,
      },
      projectId: 327,
      parentRowId: 'RequirementLibrary-327',
    }),
    mockTreeNode({
      id: 'Requirement-347230',
      children: [],
      state: DataRowOpenState.closed,
      data: {
        RLN_ID: 347230,
        projectId: 327,
        NAME: '1.01 - Exigence 1',
        HAS_DESCRIPTION: false,
        REFERENCE: '1.01',
        CRITICALITY: 'MAJOR',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        REQ_CATEGORY_ICON: 'monitor',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_FUNCTIONAL',
        REQ_CATEGORY_TYPE: 'SYS',
        CHILD_COUNT: 10,
        COVERAGE_COUNT: 10,
        IS_SYNCHRONIZED: false,
      },
      projectId: 327,
      parentRowId: 'RequirementLibrary-327',
    }),
    mockTreeNode({
      id: 'RequirementLibrary-327',
      children: ['Requirement-347230', 'Requirement-347231', 'Requirement-347232'],
      state: DataRowOpenState.open,
      data: {
        RL_ID: 327,
        projectId: 327,
        NAME: 'Test projet A',
        CHILD_COUNT: 5,
      },
      projectId: 327,
      parentRowId: null,
    }),
  ]);
}

export function mockRequirementDataRows(): DataRowModel[] {
  return [
    mockTreeNode({
      id: '1234',
      data: {
        [GridColumnId.name]: 'Exigence 1 v1',
        [GridColumnId.id]: 1,
        [GridColumnId.reference]: 'ref1-v1',
        [GridColumnId.projectName]: 'project 1',
        [GridColumnId.attachments]: 2,
        [GridColumnId.status]: 'WORK_IN_PROGRESS',
        [GridColumnId.criticality]: 'CRITICAL',
        [GridColumnId.category]: 1,
        [GridColumnId.createdBy]: 'admin',
        [GridColumnId.lastModifiedBy]: 'hello',
        [GridColumnId.reqMilestoneLocked]: 0,
        [GridColumnId.milestones]: 0,
        [GridColumnId.versionsCount]: 2,
        [GridColumnId.versionNumber]: 1,
      },
    }),
    mockTreeNode({
      id: '2345',
      data: {
        [GridColumnId.name]: 'Exigence 1 v2',
        [GridColumnId.id]: 2,
        [GridColumnId.reference]: 'ref1-v2',
        [GridColumnId.projectName]: 'project 1',
        [GridColumnId.attachments]: 2,
        [GridColumnId.status]: 'WORK_IN_PROGRESS',
        [GridColumnId.criticality]: 'CRITICAL',
        [GridColumnId.category]: 1,
        [GridColumnId.createdBy]: 'admin',
        [GridColumnId.lastModifiedBy]: 'hello',
        [GridColumnId.reqMilestoneLocked]: 0,
        [GridColumnId.milestones]: 0,
        [GridColumnId.versionsCount]: 2,
        [GridColumnId.versionNumber]: 2,
      },
    }),
  ];
}
