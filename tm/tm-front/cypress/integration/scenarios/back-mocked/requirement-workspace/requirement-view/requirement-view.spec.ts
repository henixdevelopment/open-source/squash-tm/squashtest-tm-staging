import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { RequirementViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { defaultReferentialData } from '../../../../utils/referential/default-referential-data.const';
import {
  mockRequirementVersionModel,
  mockRequirementVersionStatsBundle,
} from '../../../../data-mock/requirements.data-mock';
import { mockMilestoneModel } from '../../../../data-mock/milestone.data-mock';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import {
  LinkedHighLevelRequirement,
  RequirementVersionModel,
} from '../../../../../../projects/sqtm-core/src/lib/model/requirement/requirement-version.model';
import {
  DataRowModel,
  DataRowOpenState,
} from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { AlertDialogElement } from '../../../../page-objects/elements/dialog/alert-dialog.element';
import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { AiTestCaseGenerationDialogElement } from '../../../../page-objects/elements/dialog/ai-test-case-generation-dialog.element';
import { mockAuthenticatedUserNotAdmin } from '../../../../data-mock/authenticated-user.data-mock';
import { mockProjectPermissions } from '../../../../data-mock/project-permissions.data-mock';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

function getRequirementLibraryChildNodes() {
  return [
    mockTreeNode({
      id: 'RequirementLibrary-1',
      children: [
        'RequirementFolder-1',
        'Requirement-3',
        'RequirementFolder-2',
        'HighLevelRequirement-1',
        'HighLevelRequirement-2',
        'Requirement-4',
      ],
      data: { NAME: 'International Space Station', CHILD_COUNT: '6' },
      state: DataRowOpenState.open,
      projectId: 1,
    }),
    mockTreeNode({
      id: 'RequirementFolder-1',
      children: [],
      parentRowId: 'RequirementLibrary-1',
      data: { NAME: 'Structural Requirements' },
      state: DataRowOpenState.closed,
      projectId: 1,
    }),
    mockTreeNode({
      id: 'Requirement-3',
      children: [],
      parentRowId: 'RequirementLibrary-1',
      state: DataRowOpenState.leaf,
      data: {
        RLN_ID: 3,
        CHILD_COUNT: 0,
        NAME: 'M4 - Build Cupola',
        CRITICALITY: 'CRITICAL',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        HAS_DESCRIPTION: true,
        REQ_CATEGORY_ICON: 'briefcase',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
        REQ_CATEGORY_TYPE: 'SYS',
        COVERAGE_COUNT: 0,
        IS_SYNCHRONIZED: false,
      },
      projectId: 1,
    }),
    mockTreeNode({
      id: 'Requirement-4',
      children: ['Requirement-5'],
      parentRowId: 'RequirementLibrary-1',
      state: DataRowOpenState.open,
      data: {
        RLN_ID: 4,
        CHILD_COUNT: 1,
        NAME: 'Requirement 4',
        CRITICALITY: 'CRITICAL',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        HAS_DESCRIPTION: true,
        REQ_CATEGORY_ICON: 'briefcase',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
        REQ_CATEGORY_TYPE: 'SYS',
        COVERAGE_COUNT: 0,
        IS_SYNCHRONIZED: false,
      },
      projectId: 1,
    }),
    mockTreeNode({
      id: 'Requirement-5',
      children: [],
      parentRowId: 'Requirement-4',
      state: DataRowOpenState.leaf,
      data: {
        RLN_ID: 5,
        CHILD_COUNT: 0,
        NAME: 'Child Requirement 5',
        CRITICALITY: 'CRITICAL',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        HAS_DESCRIPTION: true,
        REQ_CATEGORY_ICON: 'briefcase',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
        REQ_CATEGORY_TYPE: 'SYS',
        COVERAGE_COUNT: 0,
        IS_SYNCHRONIZED: false,
      },
      projectId: 1,
    }),
    mockTreeNode({
      id: 'RequirementFolder-2',
      children: [],
      parentRowId: 'RequirementLibrary-1',
      data: { NAME: 'Functional Requirements' },
      state: DataRowOpenState.closed,
      projectId: 1,
    }),
    mockTreeNode({
      id: 'HighLevelRequirement-1',
      children: [],
      state: DataRowOpenState.leaf,
      data: {
        RLN_ID: 1,
        projectId: 1,
        NAME: 'hl 001',
        HAS_DESCRIPTION: false,
        REFERENCE: '',
        CRITICALITY: 'MINOR',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        REQ_CATEGORY_ICON: 'indeterminate_checkbox_empty',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_UNDEFINED',
        REQ_CATEGORY_TYPE: 'SYS',
        CHILD_COUNT: 0,
        COVERAGE_COUNT: 0,
        IS_SYNCHRONIZED: false,
        REMOTE_REQ_PERIMETER_STATUS: null,
        REMOTE_SYNCHRONISATION_ID: null,
        BOUND_TO_BLOCKING_MILESTONE: false,
      },
      projectId: 1,
      parentRowId: 'RequirementLibrary-1',
    }),
    mockTreeNode({
      id: 'HighLevelRequirement-2',
      children: [],
      state: DataRowOpenState.leaf,
      data: {
        RLN_ID: 2,
        projectId: 1,
        NAME: 'hl 002',
        HAS_DESCRIPTION: false,
        REFERENCE: '',
        CRITICALITY: 'MINOR',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        REQ_CATEGORY_ICON: 'indeterminate_checkbox_empty',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_UNDEFINED',
        REQ_CATEGORY_TYPE: 'SYS',
        CHILD_COUNT: 0,
        COVERAGE_COUNT: 0,
        IS_SYNCHRONIZED: false,
        REMOTE_REQ_PERIMETER_STATUS: null,
        REMOTE_SYNCHRONISATION_ID: null,
        BOUND_TO_BLOCKING_MILESTONE: false,
      },
      projectId: 1,
      parentRowId: 'RequirementLibrary-1',
    }),
  ];
}

function updateRowData(
  beforeUpdateDataRow: DataRowModel,
  dataProperty: string,
  value: string,
): DataRowModel {
  const data = { ...beforeUpdateDataRow.data };
  data[dataProperty] = value;
  return {
    ...beforeUpdateDataRow,
    data,
  };
}

describe('Requirement View Display', function () {
  const initialNodes = mockGridResponse('id', [
    mockTreeNode({
      id: 'RequirementLibrary-1',
      children: [],
      data: { NAME: 'International Space Station', CHILD_COUNT: '3' },
      projectId: 1,
      state: DataRowOpenState.closed,
    }),
    mockTreeNode({
      id: 'RequirementLibrary-2',
      children: [],
      data: { NAME: 'STS - Shuttle', CHILD_COUNT: '3' },
      projectId: 2,
      state: DataRowOpenState.closed,
    }),
  ]);

  it('should display a requirement', () => {
    const path = 'International Space Station / Functional Requirements';
    const requirementPage = initAtRequirementViewPage(initReq3Model(), null, null, path);
    const currentVersion = requirementPage.currentVersion;
    currentVersion.assertExists();
    currentVersion.assertNameContains('Build Cupola');
    currentVersion.assertReferenceContains('M4');
    currentVersion.assetPathContains(path);

    const informationPage = currentVersion.clickInformationAnchorLink();
    informationPage.assertIsLowLevelRequirement();
  });

  it('should change information of requirement version', () => {
    let initialReqRowForTest = initialReqRow;
    const requirementPage = initAtRequirementViewPage(initReq3Model());
    const currentVersion = requirementPage.currentVersion;
    const informationPage = currentVersion.clickInformationAnchorLink();

    initialReqRowForTest = updateRowData(initialReqRowForTest, 'NAME', 'Rebuild Cupola');
    informationPage.rename('Rebuild Cupola', {
      dataRows: [initialReqRowForTest],
    });
    currentVersion.assertNameContains('Rebuild Cupola');
    informationPage.changeCategory('Métier');
    informationPage.changeCriticality('Mineure');
    informationPage.bindMilestone(1, []);

    const milestoneTagElement = informationPage.milestoneTagElement;
    milestoneTagElement.checkMilestones('Milestone');
    informationPage.changeDescription('Ma nouvelle description');
    informationPage.descriptionElement.checkTextContent('Ma nouvelle description');

    initialReqRowForTest = updateRowData(initialReqRowForTest, 'REQUIREMENT_STATUS', 'APPROVED');
    informationPage.changeStatus('Approuvée', {
      dataRows: [initialReqRowForTest],
    });
  });

  it('should bind to high level requirement', () => {
    const requirementPage = initAtRequirementViewPage(initReq3Model(), {
      premiumPluginInstalled: true,
    });
    const currentVersion = requirementPage.currentVersion;
    const informationPage = currentVersion.clickInformationAnchorLink();
    const dialog = informationPage.openBindHighLevelRequirementSelectorDialog(initialNodes);
    const tree = dialog.requirementTree;
    const firstNode = initialNodes.dataRows[0];

    tree.openNode(firstNode.id, getRequirementLibraryChildNodes());
    tree.pickNode('HighLevelRequirement-1');
    dialog.confirm(linkedHighLevelRequirement1);
    informationPage.checkHighLevelRequirementName('hl 001');
  });

  it('should replace bound high level requirement', () => {
    const requirementPage = initAtRequirementViewPage(initReq3Model(linkedHighLevelRequirement1), {
      premiumPluginInstalled: true,
    });
    const currentVersion = requirementPage.currentVersion;
    const informationPage = currentVersion.clickInformationAnchorLink();
    const dialog = informationPage.openChangeHighLevelRequirementSelectorDialog(initialNodes);
    const tree = dialog.requirementTree;
    const firstNode = initialNodes.dataRows[0];

    tree.openNode(firstNode.id, getRequirementLibraryChildNodes());
    tree.pickNode('HighLevelRequirement-2');
    dialog.confirm(linkedHighLevelRequirement2);
    informationPage.checkHighLevelRequirementName('hl 002');
  });

  it('should display disclaimer for user without premium plugin', () => {
    const requirementPage = initAtRequirementViewPage(initReq3Model());
    const currentVersion = requirementPage.currentVersion;
    const informationPage = currentVersion.clickInformationAnchorLink();

    informationPage.openBindHighLevelRequirementSelectorDialog(initialNodes);
    informationPage.checkPremiumPluginDisclaimerMessageExists();
  });

  it('should not display disclaimer for user with premium plugin', () => {
    const requirementPage = initAtRequirementViewPage(initReq3Model(linkedHighLevelRequirement1), {
      premiumPluginInstalled: true,
    });
    const currentVersion = requirementPage.currentVersion;
    const informationPage = currentVersion.clickInformationAnchorLink();

    informationPage.openChangeHighLevelRequirementSelectorDialog(initialNodes);
    informationPage.checkPremiumPluginDisclaimerMessageDoesNotExist();
  });

  it('should unbind to high level requirement', () => {
    const requirementPage = initAtRequirementViewPage(initReq3Model(linkedHighLevelRequirement1));
    const currentVersion = requirementPage.currentVersion;
    const informationPage = currentVersion.clickInformationAnchorLink();

    informationPage.unlinkHighLevelRequirementSelectorDialog();
    informationPage.checkHighLevelRequirementNameDoesNotExist();
  });

  it('should not display high level requirement selector if requirement is child', () => {
    const requirementPage = initAtRequirementViewPage(initReq5Model());
    const currentVersion = requirementPage.currentVersion;
    const informationPage = currentVersion.clickInformationAnchorLink();

    informationPage.assertBindHighLevelRequirementSelectorButtonsDoNotExist();
  });

  it('should update status from capsule', () => {
    const requirementPage = initAtRequirementViewPage(initReq3Model());
    const capsule = requirementPage.statusCapsule;
    const criticalityCapsule = requirementPage.criticalityCapsule;
    const currentVersion = requirementPage.currentVersion;
    const informationPage = currentVersion.clickInformationAnchorLink();

    const updateMock = new HttpMockBuilder('requirement-version/*/status').post().build();

    const refreshMock = new HttpMockBuilder('requirement-tree/refresh').post().build();

    capsule.selectOption('En cours de rédaction');
    updateMock.wait();
    refreshMock.wait();

    capsule.assertContainsText('En cours de rédaction');
    informationPage.statusField.checkSelectedOption('En cours de rédaction');

    capsule.selectOption('À approuver');
    updateMock.wait();
    refreshMock.wait();

    capsule.assertContainsText('À approuver');
    informationPage.statusField.checkSelectedOption('À approuver');
    criticalityCapsule.assertIsClickable();

    capsule.selectOption('Approuvé');
    updateMock.wait();
    refreshMock.wait();

    capsule.assertContainsText('Approuvé');
    informationPage.statusField.checkSelectedOption('Approuvé');
    criticalityCapsule.assertIsNotClickable();
  });

  it('should update criticality from capsule', () => {
    const requirementPage = initAtRequirementViewPage(initReq3Model());
    const capsule = requirementPage.criticalityCapsule;
    capsule.assertIsClickable();

    const updateMock = new HttpMockBuilder('requirement-version/*/criticality').post().build();

    const refreshMock = new HttpMockBuilder('requirement-tree/refresh').post().build();

    capsule.selectOption('Critique');
    updateMock.wait();
    refreshMock.wait();

    capsule.assertContainsText('Critique');
  });

  it("should always display entry 'Generate test case' in menu if no license ultimate", () => {
    const requirementPage = initAtRequirementViewPage(initReq3Model(), {
      ultimateLicenseAvailable: false,
      user: mockAuthenticatedUserNotAdmin(),
      projectPermissions: mockProjectPermissions([
        { projectId: 1, qualifiedName: 'squashtest.acl.group.tm.ProjectManager' },
      ]),
    });
    requirementPage.clickActionMenuAndAssertGenerateTestCase(true);
  });

  it("should not display entry 'Generate test case' in menu if no create permission on any test case library", () => {
    const requirementPage = initAtRequirementViewPage(initReq3Model(), {
      ultimateLicenseAvailable: false,
      user: mockAuthenticatedUserNotAdmin(),
      projectPermissions: mockProjectPermissions([
        { projectId: 1, qualifiedName: 'squashtest.acl.group.tm.AutomatedTestWriter' },
      ]),
    });
    requirementPage.clickActionMenuAndAssertGenerateTestCase(false);
  });

  it("should display entry 'Generate test case' in menu create permission on at least one test case library", () => {
    const requirementPage = initAtRequirementViewPage(initReq3Model(), {
      ultimateLicenseAvailable: false,
      user: mockAuthenticatedUserNotAdmin(),
      projectPermissions: mockProjectPermissions([
        { projectId: 1, qualifiedName: 'squashtest.acl.group.tm.ProjectManager' },
      ]),
    });
    requirementPage.clickActionMenuAndAssertGenerateTestCase(true);
  });

  it("should not display entry 'Generate test case' in menu if license ultimate but no AI server declared in project", () => {
    const requirementPage = initAtRequirementViewPage(initReq3Model(), {
      ultimateLicenseAvailable: true,
    });
    requirementPage.clickActionMenuAndAssertGenerateTestCase(false);
  });

  it("should not display entry 'Generate test case' in menu if no description in requirement", () => {
    const requirementPage = initAtRequirementViewPage(
      initReq3Model(),
      {
        ultimateLicenseAvailable: true,
      },
      {
        description: null,
      },
    );
    requirementPage.clickActionMenuAndAssertGenerateTestCase(false);
  });

  it("should not display entry 'Generate test case' in menu if description does not contain any letter or digit", () => {
    const requirementPage = initAtRequirementViewPage(initReq3Model(), null, {
      description: '<p></p>""<p></p>',
    });
    requirementPage.clickActionMenuAndAssertGenerateTestCase(false);
  });

  it('should display alert popup if trying to access test case generation with not configured server', () => {
    const requirementPage = initAtRequirementViewPage(
      initReq3Model(),
      {
        ultimateLicenseAvailable: true,
        projectPermissions: mockProjectPermissions([
          { projectId: 1, qualifiedName: 'squashtest.acl.group.tm.ProjectManager' },
        ]),
      },
      { aiServerId: 1, canUseAiFeature: false },
    );
    const alertDialog = new AlertDialogElement('test-case-generation-needs-ai-server-configured');
    requirementPage.clickGenerateTestCaseButton();
    alertDialog.assertExists();
  });

  it('should display alert popup when trying to generate test cases with no ultimate license', () => {
    const requirementPage = initAtRequirementViewPage(initReq3Model(), {
      ultimateLicenseAvailable: false,
      projectPermissions: mockProjectPermissions([
        { projectId: 1, qualifiedName: 'squashtest.acl.group.tm.ProjectManager' },
      ]),
    });
    const alertDialog = new AlertDialogElement('test-case-generation-needs-ultimate');
    requirementPage.clickGenerateTestCaseButton();
    requirementPage.assertDialogNoUltimateLicenseExists(alertDialog);
  });

  it('should display popup to generate test cases if ultimate license is available and AI server declared in project', () => {
    const requirementPage = initAtRequirementViewPage(
      initReq3Model(),
      {
        ultimateLicenseAvailable: true,
        projectPermissions: mockProjectPermissions([
          { projectId: 1, qualifiedName: 'squashtest.acl.group.tm.ProjectManager' },
        ]),
      },
      { aiServerId: 1, canUseAiFeature: true },
    );
    const generateTestCaseDialog = new AiTestCaseGenerationDialogElement();
    requirementPage.clickGenerateTestCaseButton();
    generateTestCaseDialog.assertContainsDescription(initReq3Model().description);
  });

  function initAtRequirementViewPage(
    requirementVersionModel: RequirementVersionModel,
    referentialDataOverrides: Partial<ReferentialDataModel> = {},
    requirementVersionModelOverrides: Partial<RequirementVersionModel> = {},
    path?: string,
  ): RequirementViewPage {
    const firstNode = initialNodes.dataRows[0];
    const referentialData = {
      ...defaultReferentialData,
      ...referentialDataOverrides,
    };
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(
      initialNodes,
      referentialData,
    );
    const tree = requirementWorkspacePage.tree;

    new NavBarElement().toggle();
    tree.assertNodeExist(firstNode.id);
    tree.assertNodeTextContains(firstNode.id, firstNode.data['NAME']);
    tree.openNode(firstNode.id, getRequirementLibraryChildNodes());
    tree.assertNodeIsOpen(firstNode.id);

    const requirementPage: RequirementViewPage = tree.selectNode(
      `Requirement-${requirementVersionModel.id}`,
      { ...requirementVersionModel, ...requirementVersionModelOverrides, path },
    );
    requirementPage.assertExists();

    return requirementPage;
  }
});

const initialReqRow: DataRowModel = mockTreeNode({
  id: 'Requirement-3',
  children: [],
  state: DataRowOpenState.leaf,
  data: {
    RLN_ID: 3,
    NAME: 'M4 - Build Cupola',
    HAS_DESCRIPTION: true,
    REFERENCE: 'M4',
    CRITICALITY: 'CRITICAL',
    REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
    REQ_CATEGORY_ICON: 'briefcase',
    REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
    REQ_CATEGORY_TYPE: 'SYS',
    CHILD_COUNT: 0,
    COVERAGE_COUNT: 0,
    IS_SYNCHRONIZED: false,
  },
});

const linkedHighLevelRequirement1: LinkedHighLevelRequirement = {
  name: 'hl 001',
  reference: null,
  requirementId: 1,
  requirementVersionId: 1,
  path: '',
  projectName: '',
};

const linkedHighLevelRequirement2: LinkedHighLevelRequirement = {
  name: 'hl 002',
  reference: null,
  requirementId: 2,
  requirementVersionId: 1,
  path: '',
  projectName: '',
};

function initReq3Model(
  linkedHighLevelRequirement?: LinkedHighLevelRequirement,
): RequirementVersionModel {
  return mockRequirementVersionModel({
    id: 3,
    projectId: 1,
    name: 'Build Cupola',
    reference: 'M4',
    category: 2,
    createdBy: 'admin',
    createdOn: new Date('2020-09-30 10:30').toISOString(),
    criticality: 'MAJOR',
    requirementId: 3,
    description: 'A description',
    status: 'UNDER_REVIEW',
    versionNumber: 1,
    bindableMilestones: [
      mockMilestoneModel({
        label: 'Milestone',
        endDate: new Date('2020-09-30 10:30').toISOString(),
        createdOn: new Date('2020-09-30 10:30').toISOString(),
        lastModifiedOn: new Date('2020-09-30 10:30').toISOString(),
      }),
    ],
    requirementStats: mockRequirementVersionStatsBundle({
      total: {
        allTestCaseCount: 5,
        executedTestCase: 2,
        plannedTestCase: 2,
        verifiedTestCase: 1,
        redactedTestCase: 2,
        validatedTestCases: 1,
      },
      currentVersion: {
        allTestCaseCount: 5,
        executedTestCase: 2,
        plannedTestCase: 2,
        verifiedTestCase: 1,
        redactedTestCase: 2,
        validatedTestCases: 1,
      },
    }),
    remoteReqPerimeterStatus: null,
    linkedHighLevelRequirement: linkedHighLevelRequirement ? linkedHighLevelRequirement : null,
    childOfRequirement: false,
    aiServerId: null,
  });
}

function initReq5Model(): RequirementVersionModel {
  return mockRequirementVersionModel({
    id: 5,
    projectId: 1,
    name: 'Child Requirement 5',
    reference: '',
    category: 2,
    createdBy: 'admin',
    createdOn: new Date('2020-09-30 10:30').toISOString(),
    criticality: 'MAJOR',
    requirementId: 5,
    status: 'UNDER_REVIEW',
    versionNumber: 1,
    bindableMilestones: null,
    requirementStats: mockRequirementVersionStatsBundle({
      total: {
        allTestCaseCount: 5,
        executedTestCase: 2,
        plannedTestCase: 2,
        verifiedTestCase: 1,
        redactedTestCase: 2,
        validatedTestCases: 1,
      },
      currentVersion: {
        allTestCaseCount: 5,
        executedTestCase: 2,
        plannedTestCase: 2,
        verifiedTestCase: 1,
        redactedTestCase: 2,
        validatedTestCases: 1,
      },
    }),
    remoteReqPerimeterStatus: null,
    linkedHighLevelRequirement: null,
    childOfRequirement: true,
    aiServerId: null,
    canUseAiFeature: true,
  });
}
