import { RequirementVersionViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-view.page';
import { RequirementViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { defaultReferentialData } from '../../../../utils/referential/default-referential-data.const';
import {
  mockRequirementVersionModel,
  mockRequirementVersionStatsBundle,
} from '../../../../data-mock/requirements.data-mock';
import { RequirementVersionModel } from '../../../../../../projects/sqtm-core/src/lib/model/requirement/requirement-version.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { mockRequirementTreeResponse } from '../requirement-workspace-mock-data';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

describe('Requirement View link', () => {
  it('Should display data in table', () => {
    const page = navigateToRequirementView();
    const table = page.requirementLinkTable;
    table.assertColumnCount(9);
    table.assertRowCount(2);

    const row = table.getRow(1);
    row.assertExists();
    row.cell(GridColumnId.projectName).textRenderer().assertContainsText('test-project');
    row.cell(GridColumnId.name).linkRenderer().assertContainText('Repair Cupola');
    row.cell(GridColumnId.reference).textRenderer().assertContainsText('');
    row.cell(GridColumnId.milestoneLabels).assertExists();
    row.cell(GridColumnId.role).textRenderer().assertContainsText('Relatif');
    row.cell(GridColumnId.versionNumber).textRenderer().assertContainsText('2');
  });

  it('Should remove a requirement row in table', () => {
    const page = navigateToRequirementView();
    const table = page.requirementLinkTable;
    table.assertRowCount(2);
    const requirementLinksDialogElement = page.showDeleteConfirmRequirementLinkDialog(3, 1);
    requirementLinksDialogElement.assertExists();
    requirementLinksDialogElement.deleteForSuccess(
      {
        requirementVersionLinks: [
          {
            id: 5,
            projectName: 'test-project',
            name: 'Kill Cupola',
            reference: '',
            milestoneLabels: null,
            milestoneMinDate: null,
            milestoneMaxDate: null,
            role: 'requirement-version.link.type.related',
            versionNumber: 1,
          },
        ],
      },
      { dataRows: [] },
    );
    requirementLinksDialogElement.assertNotExist();
    table.assertRowCount(1);
  });

  it('Should remove many requirement rows in table with multiple suppression button', () => {
    const page = navigateToRequirementView();
    const table = page.requirementLinkTable;
    table.assertRowCount(2);
    let dialogElement = page.showDeleteConfirmRequirementLinksDialog(3, [1, 2]);
    dialogElement.assertNotExist();
    table.selectRows([1, 2], '#', 'leftViewport');
    dialogElement = page.showDeleteConfirmRequirementLinksDialog(3, [1, 2]);
    dialogElement.assertExists();
    dialogElement.deleteForSuccess(
      {
        requirementVersionLinks: [],
      },
      { dataRows: [] },
    );
    dialogElement.assertNotExist();
    table.assertRowCount(0);
  });

  it('should add requirement links by requirement-tree-picker', () => {
    const page = navigateToRequirementView();
    const requirementTreeResponse: GridResponse = mockRequirementTreeResponse();
    const requirementDrawer = page.openRequirementDrawer(requirementTreeResponse);
    requirementDrawer.beginDragAndDrop('Requirement-347232');
    const dialog = page.dropRequirementIntoRequirement(true, {
      versionName: 'Exigence 3',
    });
    dialog.assertExists();
    dialog.assertRequirementNameIs('Build Cupola');
    dialog.assertRelatedVersionNamesIs('Exigence 3');
    dialog.confirmLink(
      {
        summary: {
          alreadyLinkedRejections: false,
          sameRequirementRejections: false,
          notLinkableRejections: false,
        },
        requirementVersionLinks: [
          {
            id: 1,
            projectName: 'test-project',
            name: 'Repair Cupola',
            reference: '',
            milestoneLabels: 'jalon-1',
            milestoneMinDate: new Date('2020-09-30 10:30'),
            milestoneMaxDate: new Date('2020-10-10 10:30'),
            role: 'requirement-version.link.type.related',
            versionNumber: 2,
          },
          {
            id: 2,
            projectName: 'test-project',
            name: 'Kill Cupola',
            reference: '',
            milestoneLabels: null,
            milestoneMinDate: null,
            milestoneMaxDate: null,
            role: 'requirement-version.link.type.related',
            versionNumber: 1,
          },
          {
            id: 347232,
            projectName: 'test-project',
            name: 'Kill Cupola',
            reference: '1.03',
            milestoneLabels: null,
            milestoneMinDate: null,
            milestoneMaxDate: null,
            role: 'requirement-version.link.type.related',
            versionNumber: 1,
          },
        ],
      },
      { dataRows: [] },
    );
    page.closeDrawer();
    page.requirementLinkTable.assertRowCount(3);
  });

  it('should edit link type in table', () => {
    const page = navigateToRequirementView();
    const table = page.requirementLinkTable;
    table.assertRowCount(2);
    const editDialog = page.showEditTypeDialog(1);
    editDialog.selectField.selectValue('Doublon - Doublon');
    editDialog.updateLink([
      {
        id: 1,
        projectName: 'test-project',
        name: 'Repair Cupola',
        reference: '',
        milestoneLabels: 'jalon-1',
        milestoneMinDate: new Date('2020-09-30 10:30'),
        milestoneMaxDate: new Date('2020-10-10 10:30'),
        role: 'requirement-version.link.type.duplicate',
        versionNumber: 2,
      },
      {
        id: 2,
        projectName: 'test-project',
        name: 'Exigence 3',
        reference: '',
        milestoneLabels: null,
        milestoneMinDate: null,
        milestoneMaxDate: null,
        role: 'requirement-version.link.type.related',
        versionNumber: 1,
      },
    ]);
    const row = table.getRow(1);
    row.cell('role').textRenderer().assertContainsText('Doublon');
  });

  const req3Model: RequirementVersionModel = mockRequirementVersionModel({
    id: 3,
    projectId: 1,
    name: 'Build Cupola',
    reference: 'M4',
    attachmentList: { id: 1, attachments: [] },
    customFieldValues: [],
    category: 2,
    createdBy: 'admin',
    createdOn: new Date('2020-09-30 10:30').toISOString(),
    criticality: 'MAJOR',
    description: '',
    lastModifiedBy: '',
    lastModifiedOn: null,
    milestones: [],
    requirementId: 3,
    status: 'UNDER_REVIEW',
    versionNumber: 1,
    bindableMilestones: [],
    verifyingTestCases: [],
    requirementVersionLinks: [
      {
        id: 1,
        projectName: 'test-project',
        name: 'Repair Cupola',
        reference: '',
        milestoneLabels: 'jalon-1',
        milestoneMinDate: new Date('2020-09-30 10:30'),
        milestoneMaxDate: new Date('2020-10-10 10:30'),
        role: 'requirement-version.link.type.related',
        versionNumber: 2,
      },
      {
        id: 2,
        projectName: 'test-project',
        name: 'Exigence 3',
        reference: '',
        milestoneLabels: null,
        milestoneMinDate: null,
        milestoneMaxDate: null,
        role: 'requirement-version.link.type.related',
        versionNumber: 1,
      },
    ],
    requirementStats: mockRequirementVersionStatsBundle({
      total: {
        allTestCaseCount: 5,
        executedTestCase: 2,
        plannedTestCase: 2,
        verifiedTestCase: 1,
        redactedTestCase: 2,
        validatedTestCases: 1,
      },
      currentVersion: {
        allTestCaseCount: 5,
        executedTestCase: 2,
        plannedTestCase: 2,
        verifiedTestCase: 1,
        redactedTestCase: 2,
        validatedTestCases: 1,
      },
    }),
    remoteReqPerimeterStatus: null,
    highLevelRequirement: false,
    childOfRequirement: false,
    linkedHighLevelRequirement: null,
    lowLevelRequirements: [],
  });

  const initialNodes = mockGridResponse('id', [
    mockTreeNode({
      id: 'RequirementLibrary-1',
      children: ['Requirement-3', 'Requirement-4', 'Requirement-5'],
      data: { NAME: 'International Space Station', CHILD_COUNT: 3 },
      state: DataRowOpenState.open,
    }),
    mockTreeNode({
      id: 'Requirement-3',
      children: [],
      parentRowId: 'RequirementLibrary-1',
      state: DataRowOpenState.leaf,
      data: {
        RLN_ID: 3,
        CHILD_COUNT: 0,
        NAME: 'M4 - Build Cupola',
        CRITICALITY: 'CRITICAL',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        HAS_DESCRIPTION: true,
        REQ_CATEGORY_ICON: 'briefcase',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
        REQ_CATEGORY_TYPE: 'SYS',
        COVERAGE_COUNT: 0,
        IS_SYNCHRONIZED: false,
      },
    }),
    mockTreeNode({
      id: 'Requirement-4',
      children: [],
      parentRowId: 'RequirementLibrary-1',
      state: DataRowOpenState.leaf,
      data: {
        RLN_ID: 4,
        CHILD_COUNT: 0,
        NAME: 'Repair Cupola',
        CRITICALITY: 'CRITICAL',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        HAS_DESCRIPTION: false,
        REQ_CATEGORY_ICON: 'briefcase',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
        REQ_CATEGORY_TYPE: 'SYS',
        COVERAGE_COUNT: 0,
        IS_SYNCHRONIZED: false,
      },
    }),
    mockTreeNode({
      id: 'Requirement-5',
      children: [],
      parentRowId: 'RequirementLibrary-1',
      state: DataRowOpenState.leaf,
      data: {
        RLN_ID: 5,
        CHILD_COUNT: 0,
        NAME: 'Kill Cupola',
        CRITICALITY: 'CRITICAL',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        HAS_DESCRIPTION: true,
        REQ_CATEGORY_ICON: 'briefcase',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
        REQ_CATEGORY_TYPE: 'SYS',
        COVERAGE_COUNT: 0,
        IS_SYNCHRONIZED: false,
      },
    }),
  ]);

  function navigateToRequirementView(): RequirementVersionViewPage {
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(
      initialNodes,
      defaultReferentialData,
    );
    requirementWorkspacePage.navBar.toggle();
    const requirementViewPage = requirementWorkspacePage.tree.selectNode(
      'Requirement-3',
      req3Model,
    ) as RequirementViewPage;
    const requirementVersionViewPage = requirementViewPage.currentVersion;
    requirementVersionViewPage.toggleTree();
    return requirementVersionViewPage;
  }
});
