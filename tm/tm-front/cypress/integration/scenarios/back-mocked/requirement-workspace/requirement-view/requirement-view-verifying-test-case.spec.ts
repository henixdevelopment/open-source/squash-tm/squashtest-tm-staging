import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { defaultReferentialData } from '../../../../utils/referential/default-referential-data.const';
import { RequirementViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import { RequirementVersionViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-view.page';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import {
  mockRequirementVersionModel,
  mockRequirementVersionStatsBundle,
} from '../../../../data-mock/requirements.data-mock';
import { VerifyingTestCase } from '../../../../../../projects/sqtm-core/src/lib/model/requirement/verifying-test-case';
import { ChangeVerifyingTestCaseOperationReport } from '../../../../../../projects/sqtm-core/src/lib/model/change-coverage-operation-report';
import { RequirementVersionModel } from '../../../../../../projects/sqtm-core/src/lib/model/requirement/requirement-version.model';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

describe('Requirement View verifying test case', function () {
  it('Should display data in table', () => {
    const page = navigateToRequirementView();
    const table = page.verifyingTestCaseTable;
    table.assertRowCount(2);
    const row = table.getRow(1);
    row.assertExists();
    row.cell(GridColumnId.projectName).textRenderer().assertContainsText('Project 1');
    row.cell(GridColumnId.reference).textRenderer().assertContainsText('REF.001');
    row.cell(GridColumnId.name).linkRenderer().assertContainText('TestCase 1');
    row.cell(GridColumnId.milestoneLabels).assertExists();
    row
      .cell(GridColumnId.importance)
      .iconRenderer()
      .assertContainsIcon('anticon-sqtm-core-test-case:double_down');
    row
      .cell(GridColumnId.status)
      .iconRenderer()
      .assertContainsIcon('anticon-sqtm-core-test-case:status');

    const secondRow = table.getRow(2);
    secondRow.assertExists();
    secondRow.cell(GridColumnId.projectName).textRenderer().assertContainsText('Project 42');
    secondRow.cell(GridColumnId.reference).textRenderer().assertContainsText('');
    secondRow.cell(GridColumnId.name).linkRenderer().assertContainText('Mon Cas de test');
    secondRow.cell(GridColumnId.milestoneLabels).assertExists();
    secondRow
      .cell(GridColumnId.importance)
      .iconRenderer()
      .assertContainsIcon('anticon-sqtm-core-test-case:double_up');
    secondRow
      .cell(GridColumnId.status)
      .iconRenderer()
      .assertContainsIcon('anticon-sqtm-core-test-case:status');
  });

  it('Should add test case to requirement version', () => {
    const page = navigateToRequirementView();
    const testcaseResponse = mockGridResponse('id', [
      mockTreeNode({
        id: 'TestCaseLibrary-1',
        children: ['TestCase-3'],
        data: { NAME: 'Project1', CHILD_COUNT: 1 },
        state: DataRowOpenState.open,
      }),
      mockTreeNode({
        id: 'TestCase-3',
        children: [],
        data: {
          NAME: 'a nice test',
          CHILD_COUNT: 0,
          TC_STATUS: 'APPROVED',
          TC_KIND: 'STANDARD',
          IMPORTANCE: 'HIGH',
        },
        parentRowId: 'TestCaseLibrary-1',
      }),
    ]);

    const testcaseDrawer = page.openTestcaseDrawer(testcaseResponse);
    testcaseDrawer.beginDragAndDrop('TestCase-3');
    const otherTestCases: VerifyingTestCase[] = [
      {
        id: 1,
        name: 'TestCase 1',
        importance: 'LOW',
        milestoneLabels: '',
        milestoneMaxDate: null,
        milestoneMinDate: null,
        projectName: 'Project 1',
        reference: 'REF.001',
        status: 'APPROVED',
        directlyLinked: true,
        lastExecutionStatus: null,
      },
      {
        id: 2,
        name: 'Mon Cas de test',
        importance: 'VERY_HIGH',
        milestoneLabels: 'Jalon',
        milestoneMaxDate: new Date('2020-10-18'),
        milestoneMinDate: new Date('2020-10-12'),
        projectName: 'Project 42',
        reference: '',
        status: 'TO_BE_UPDATED',
        directlyLinked: true,
        lastExecutionStatus: null,
      },
      {
        id: 3,
        name: 'a nice test',
        importance: 'HIGH',
        milestoneLabels: 'Jalon',
        milestoneMaxDate: new Date('2020-10-18'),
        milestoneMinDate: new Date('2020-10-12'),
        projectName: 'Project 42',
        reference: '',
        status: 'APPROVED',
        directlyLinked: true,
        lastExecutionStatus: null,
      },
    ];
    const verifyingTestCaseOperationReport: ChangeVerifyingTestCaseOperationReport = {
      verifyingTestCases: otherTestCases,
      summary: {
        notLinkableRejections: false,
        noVerifiableVersionRejections: false,
        alreadyVerifiedRejections: false,
      },
      nbIssues: 0,
      requirementStats: null,
    };
    page.dropTestCaseIntoRequirement(3, verifyingTestCaseOperationReport, { dataRows: [] });
    page.closeDrawer();
    const table = page.verifyingTestCaseTable;
    const newTestCase = table.getRow(3);
    newTestCase.cell('name').linkRenderer().assertContainText('a nice test');
  });

  it('should remove a testcase row in table', () => {
    const page = navigateToRequirementView();
    const table = page.verifyingTestCaseTable;
    const testcasesDialogElement = page.showDeleteConfirmTestCaseDialog(3, 2);
    testcasesDialogElement.assertExists();
    testcasesDialogElement.deleteForSuccess(
      {
        verifyingTestCases: [
          {
            id: 1,
            name: 'TestCase 1',
            importance: 'LOW',
            milestoneLabels: '',
            milestoneMaxDate: null,
            milestoneMinDate: null,
            projectName: 'Project 1',
            reference: 'REF.001',
            status: 'APPROVED',
            directlyLinked: true,
          },
        ],
      },
      { dataRows: [] },
    );
    testcasesDialogElement.assertNotExist();
    table.assertRowCount(1);
  });

  it('should remove many test case rows in table', () => {
    const page = navigateToRequirementView();
    const table = page.verifyingTestCaseTable;
    let testcasesDialogElement = page.showDeleteConfirmTestCasesDialogWithTopIcon(3, [1, 2]);
    testcasesDialogElement.assertNotExist();
    table.selectRows([1, 2], '#', 'leftViewport');
    testcasesDialogElement = page.showDeleteConfirmTestCasesDialogWithTopIcon(3, [1, 2]);
    testcasesDialogElement.assertExists();
    testcasesDialogElement.deleteForSuccess(
      {
        verifyingTestCases: [],
      },
      { dataRows: [] },
    );
    testcasesDialogElement.assertNotExist();
    table.assertRowCount(0);
  });

  it('should navigate to search test-case for coverages', () => {
    const page = navigateToRequirementView();
    new NavBarElement().toggle();
    page.toggleTree();
    const testCaseForCoverageSearchPage = page.navigateToSearchTestCaseForCoverage();
    testCaseForCoverageSearchPage.assertExists();
    testCaseForCoverageSearchPage.assertLinkSelectionButtonExist();
    testCaseForCoverageSearchPage.assertLinkAllButtonExist();
  });

  const req3Model: RequirementVersionModel = mockRequirementVersionModel({
    id: 3,
    projectId: 1,
    name: 'Build Cupola',
    reference: 'M4',
    attachmentList: { id: 1, attachments: [] },
    customFieldValues: [],
    category: 2,
    createdBy: 'admin',
    createdOn: new Date('2020-09-30 10:30').toISOString(),
    criticality: 'MAJOR',
    description: '',
    lastModifiedBy: '',
    lastModifiedOn: null,
    milestones: [],
    requirementId: 3,
    status: 'UNDER_REVIEW',
    versionNumber: 1,
    bindableMilestones: [
      {
        id: 1,
        endDate: new Date('2020-09-30 10:30').toISOString(),
        label: 'Milestone',
        description: '',
        ownerFistName: '',
        ownerLastName: '',
        ownerLogin: '',
        range: 'GLOBAL',
        status: 'IN_PROGRESS',
        lastModifiedOn: new Date('2020-09-30 10:30').toISOString(),
        lastModifiedBy: 'admin',
        createdOn: new Date('2020-09-30 10:30').toISOString(),
        createdBy: 'admin',
      },
    ],
    verifyingTestCases: [
      {
        id: 1,
        name: 'TestCase 1',
        importance: 'LOW',
        milestoneLabels: '',
        milestoneMaxDate: null,
        milestoneMinDate: null,
        projectName: 'Project 1',
        reference: 'REF.001',
        status: 'APPROVED',
        directlyLinked: true,
        lastExecutionStatus: null,
      },
      {
        id: 2,
        name: 'Mon Cas de test',
        importance: 'VERY_HIGH',
        milestoneLabels: 'Jalon',
        milestoneMaxDate: new Date('2020-10-18'),
        milestoneMinDate: new Date('2020-10-12'),
        projectName: 'Project 42',
        reference: '',
        status: 'TO_BE_UPDATED',
        directlyLinked: true,
        lastExecutionStatus: null,
      },
    ],
    requirementVersionLinks: [],
    requirementStats: mockRequirementVersionStatsBundle({
      total: {
        allTestCaseCount: 5,
        executedTestCase: 2,
        plannedTestCase: 2,
        verifiedTestCase: 1,
        redactedTestCase: 2,
        validatedTestCases: 1,
      },
      currentVersion: {
        allTestCaseCount: 5,
        executedTestCase: 2,
        plannedTestCase: 2,
        verifiedTestCase: 1,
        redactedTestCase: 2,
        validatedTestCases: 1,
      },
    }),
    remoteReqPerimeterStatus: null,
    highLevelRequirement: false,
    linkedHighLevelRequirement: null,
    childOfRequirement: false,
    lowLevelRequirements: [],
  });

  const initialNodes = mockGridResponse('id', [
    mockTreeNode({
      id: 'RequirementLibrary-1',
      children: ['Requirement-3'],
      data: { NAME: 'International Space Station', CHILD_COUNT: 1 },
      state: DataRowOpenState.open,
    }),
    mockTreeNode({
      id: 'Requirement-3',
      children: [],
      parentRowId: 'RequirementLibrary-1',
      state: DataRowOpenState.leaf,
      data: {
        RLN_ID: 3,
        CHILD_COUNT: 0,
        NAME: 'M4 - Build Cupola',
        CRITICALITY: 'CRITICAL',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        HAS_DESCRIPTION: true,
        REQ_CATEGORY_ICON: 'briefcase',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
        REQ_CATEGORY_TYPE: 'SYS',
        COVERAGE_COUNT: 0,
        IS_SYNCHRONIZED: false,
      },
    }),
  ]);

  function navigateToRequirementView(): RequirementVersionViewPage {
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(
      initialNodes,
      defaultReferentialData,
    );
    requirementWorkspacePage.navBar.toggle();
    const requirementViewPage = requirementWorkspacePage.tree.selectNode(
      'Requirement-3',
      req3Model,
    ) as RequirementViewPage;
    const requirementVersionViewPage = requirementViewPage.currentVersion;
    requirementVersionViewPage.toggleTree();
    return requirementVersionViewPage;
  }
});
