import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { defaultReferentialData } from '../../../../utils/referential/default-referential-data.const';
import { RequirementVersionViewInformationPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-view-information.page';
import {
  mockLinkedLowLevelRequirementModel,
  mockRequirementVersionModel,
} from '../../../../data-mock/requirements.data-mock';
import {
  BindRequirementToHighLevelRequirementOperationReport,
  HighLevelRequirementViewPage,
} from '../../../../page-objects/pages/requirement-workspace/requirement/high-level-requirement-view.page';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import {
  LinkedLowLevelRequirement,
  RequirementVersionModel,
} from '../../../../../../projects/sqtm-core/src/lib/model/requirement/requirement-version.model';
import { RequirementVersionStatsBundle } from '../../../../../../projects/sqtm-core/src/lib/model/requirement/requirement-version-stats-bundle.model';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { AlertDialogElement } from '../../../../page-objects/elements/dialog/alert-dialog.element';
import { AiTestCaseGenerationDialogElement } from '../../../../page-objects/elements/dialog/ai-test-case-generation-dialog.element';
import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { mockAuthenticatedUserNotAdmin } from '../../../../data-mock/authenticated-user.data-mock';
import { mockProjectPermissions } from '../../../../data-mock/project-permissions.data-mock';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

function getRequirementLibraryChildNodes() {
  return [
    mockTreeNode({
      id: 'RequirementLibrary-1',
      children: ['RequirementFolder-1', 'HighLevelRequirement-3', 'RequirementFolder-2'],
      data: { NAME: 'International Space Station', CHILD_COUNT: '3' },
      state: DataRowOpenState.open,
    }),
    mockTreeNode({
      id: 'RequirementFolder-1',
      children: [],
      parentRowId: 'RequirementLibrary-1',
      data: { NAME: 'Structural Requirements' },
      state: DataRowOpenState.closed,
    }),
    mockTreeNode({
      id: 'HighLevelRequirement-3',
      children: [],
      parentRowId: 'RequirementLibrary-1',
      state: DataRowOpenState.leaf,
      data: {
        RLN_ID: 3,
        CHILD_COUNT: 0,
        NAME: 'M4 - Build Cupola',
        CRITICALITY: 'CRITICAL',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        HAS_DESCRIPTION: true,
        REQ_CATEGORY_ICON: 'briefcase',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
        REQ_CATEGORY_TYPE: 'SYS',
        COVERAGE_COUNT: 0,
        IS_SYNCHRONIZED: false,
      },
    }),
    mockTreeNode({
      id: 'RequirementFolder-2',
      children: [],
      parentRowId: 'RequirementLibrary-1',
      data: { NAME: 'Functional Requirements' },
      state: DataRowOpenState.closed,
    }),
  ];
}

describe('Requirement View Display', function () {
  const initialNodes = mockGridResponse('id', [
    mockTreeNode({
      id: 'RequirementLibrary-1',
      children: [],
      data: { NAME: 'International Space Station', CHILD_COUNT: '3' },
      state: DataRowOpenState.closed,
    }),
    mockTreeNode({
      id: 'RequirementLibrary-2',
      children: [],
      data: { NAME: 'STS - Shuttle', CHILD_COUNT: '3' },
      state: DataRowOpenState.closed,
    }),
  ]);

  it('should display a high level requirement', () => {
    const requirementPage = initAtRequirementViewPage();
    const currentVersion = requirementPage.currentVersion;
    currentVersion.assertExists();
    currentVersion.assertNameContains('Build Cupola');
    currentVersion.assertReferenceContains('M4');
    const informationPage: RequirementVersionViewInformationPage =
      currentVersion.clickInformationAnchorLink();
    informationPage.assertIsHighLevelRequirement();
    const table = currentVersion.lowLevelRequirementTable;
    table.assertExists();
    table.assertRowCount(3);

    const row = table.getRow(4);
    row.assertExists();
    row.cell(GridColumnId.projectName).textRenderer().assertContainsText('test-project');
    row.cell(GridColumnId.projectName).textRenderer().assertNotDisabled();
    row.cell(GridColumnId.name).linkRenderer().assertContainText('Get some astronauts');
    row.cell(GridColumnId.reference).textRenderer().assertContainsText('ASTR-1');
    row.cell(GridColumnId.milestoneLabels).assertExists();

    const secondRow = table.getRow(6);
    secondRow.assertExists();
    secondRow.cell(GridColumnId.projectName).textRenderer().assertDisabled();
  });

  it('should add requirement to low level requirement table', () => {
    const requirementPage = initAtRequirementViewPage();
    const requirementTreeResponse: GridResponse = mockRequirementTreeResponse();
    const requirementDrawer =
      requirementPage.openBindStandardRequirementDrawer(requirementTreeResponse);
    requirementDrawer.beginDragAndDrop('Requirement-347232');
    const operationReport: BindRequirementToHighLevelRequirementOperationReport =
      mockOperationReport([
        mockLinkedLowLevelRequirementModel({
          requirementId: 4,
          requirementVersionId: 4,
          name: 'Get some astronauts',
          reference: 'ASTR-1',
          projectName: 'test-project',
          versionNumber: 2,
          childOfRequirement: false,
        }),
        mockLinkedLowLevelRequirementModel({
          requirementId: 5,
          requirementVersionId: 5,
          name: 'Get some furniture',
          projectName: 'test-project',
        }),
        mockLinkedLowLevelRequirementModel({
          requirementId: 6,
          requirementVersionId: 6,
          name: 'Child Requirement',
          childOfRequirement: true,
        }),
        mockLinkedLowLevelRequirementModel({
          requirementId: 347232,
          requirementVersionId: 347232,
          name: 'Exigence 3',
          reference: '1.03',
          projectName: 'Projet 1',
        }),
      ]);
    requirementPage.dropRequirementIntoLinkedLowLevelReqTable(
      operationReport,
      requirementTreeResponse,
    );
    requirementPage.closeDrawer();
    requirementPage.linkedLowLevelRequirementTable.assertRowCount(4);
  });

  it('should unbind a requirement in low level requirement table', () => {
    const requirementPage = initAtRequirementViewPage();
    const table = requirementPage.linkedLowLevelRequirementTable;
    table.assertRowCount(3);
    const unbindLowLevelReqDialog = requirementPage.showUnbindLowLevelReqDialog(3, 4);
    unbindLowLevelReqDialog.assertExists();
    unbindLowLevelReqDialog.deleteForSuccess(
      mockOperationReport([
        mockLinkedLowLevelRequirementModel({
          requirementId: 5,
          requirementVersionId: 5,
          name: 'Get some furniture',
          projectName: 'test-project',
        }),
        mockLinkedLowLevelRequirementModel({
          requirementId: 6,
          requirementVersionId: 6,
          name: 'Child Requirement',
          childOfRequirement: true,
        }),
      ]),
      { dataRows: [] },
    );
    unbindLowLevelReqDialog.assertNotExist();
    table.assertRowCount(2);
  });

  it('should unbind multiple requirements in low level requirement table', () => {
    const requirementPage = initAtRequirementViewPage();
    const table = requirementPage.linkedLowLevelRequirementTable;
    table.assertRowCount(3);
    table.selectRows([4, 5], '#', 'leftViewport');
    const unbindLowLevelReqDialog = requirementPage.showUnbindMultipleLowLevelReqDialog(3, [4, 5]);
    unbindLowLevelReqDialog.assertExists();
    unbindLowLevelReqDialog.deleteForSuccess(
      mockOperationReport([
        mockLinkedLowLevelRequirementModel({
          requirementId: 6,
          requirementVersionId: 6,
          name: 'Child Requirement',
          childOfRequirement: true,
        }),
      ]),
      { dataRows: [] },
    );
    unbindLowLevelReqDialog.assertNotExist();
    table.assertRowCount(1);
  });

  it('should not be possible to unbind a child requirement in low level requirement table', () => {
    const requirementPage = initAtRequirementViewPage();
    const table = requirementPage.linkedLowLevelRequirementTable;
    table.assertRowCount(3);
    const childReqRow = table.getRow(6);
    childReqRow.assertExists();
    childReqRow.cell(GridColumnId.projectName).textRenderer().assertDisabled();
    table.selectRows([6], '#', 'leftViewport');
    table.assertRowIsNotSelected(6);
    const childReqRightViewport = table.getRow(6, 'rightViewport');
    childReqRightViewport.cell('delete').iconRenderer().assertNotExist();
  });

  it('should not be possible to unbind a verifying test cases which comes from a low level req in table', () => {
    const requirementPage = initAtRequirementViewPage();
    const table = requirementPage.verifyingTestCaseTable;
    table.assertRowCount(2);
    const indirectlyLinkedVerifyingTC = table.getRow(2);
    indirectlyLinkedVerifyingTC.assertExists();
    indirectlyLinkedVerifyingTC.cell(GridColumnId.projectName).textRenderer().assertDisabled();
    table.selectRows([2], '#', 'leftViewport');
    table.assertRowIsNotSelected(2);
    const verifyingTestCaseRightViewport = table.getRow(2, 'rightViewport');
    verifyingTestCaseRightViewport.cell('delete').iconRenderer().assertNotExist();
  });

  it('should update high level status from capsule', () => {
    const requirementPage = initAtRequirementViewPage();
    const capsule = requirementPage.statusCapsule;
    const criticalityCapsule = requirementPage.criticalityCapsule;
    const currentVersion = requirementPage.currentVersion;
    const informationPage = currentVersion.clickInformationAnchorLink();

    const updateMock = new HttpMockBuilder('requirement-version/*/status').post().build();

    const refreshMock = new HttpMockBuilder('requirement-tree/refresh').post().build();

    capsule.selectOption('En cours de rédaction');
    updateMock.wait();
    refreshMock.wait();

    capsule.assertContainsText('En cours de rédaction');
    informationPage.statusField.checkSelectedOption('En cours de rédaction');

    capsule.selectOption('À approuver');
    updateMock.wait();
    refreshMock.wait();

    capsule.assertContainsText('À approuver');
    informationPage.statusField.checkSelectedOption('À approuver');
    criticalityCapsule.assertIsClickable();

    capsule.selectOption('Approuvé');
    updateMock.wait();
    refreshMock.wait();

    capsule.assertContainsText('Approuvé');
    informationPage.statusField.checkSelectedOption('Approuvé');
    criticalityCapsule.assertIsNotClickable();
  });

  it('should update high level criticality from capsule', () => {
    const requirementPage = initAtRequirementViewPage();
    const capsule = requirementPage.criticalityCapsule;
    capsule.assertIsClickable();

    const updateMock = new HttpMockBuilder('requirement-version/*/criticality').post().build();

    const refreshMock = new HttpMockBuilder('requirement-tree/refresh').post().build();

    capsule.selectOption('Critique');
    updateMock.wait();
    refreshMock.wait();

    capsule.assertContainsText('Critique');
  });

  it("should always display entry 'Generate test case' in menu if no license ultimate", () => {
    const requirementPage = initAtRequirementViewPage({
      ultimateLicenseAvailable: false,
      user: mockAuthenticatedUserNotAdmin(),
      projectPermissions: mockProjectPermissions([
        { projectId: 1, qualifiedName: 'squashtest.acl.group.tm.ProjectManager' },
      ]),
    });
    requirementPage.clickActionMenuAndAssertGenerateTestCase(true);
  });

  it("should not display entry 'Generate test case' in menu if license ultimate but no AI server declared in project", () => {
    const requirementPage = initAtRequirementViewPage({
      ultimateLicenseAvailable: true,
      user: mockAuthenticatedUserNotAdmin(),
      projectPermissions: mockProjectPermissions([
        { projectId: 1, qualifiedName: 'squashtest.acl.group.tm.ProjectManager' },
      ]),
    });
    requirementPage.clickActionMenuAndAssertGenerateTestCase(false);
  });

  it('should display alert popup when trying to generate test cases with no ultimate license', () => {
    const requirementPage = initAtRequirementViewPage({
      ultimateLicenseAvailable: false,
      user: mockAuthenticatedUserNotAdmin(),
      projectPermissions: mockProjectPermissions([
        { projectId: 1, qualifiedName: 'squashtest.acl.group.tm.ProjectManager' },
      ]),
    });
    const alertDialog = new AlertDialogElement('test-case-generation-needs-ultimate');
    requirementPage.clickGenerateTestCaseButton();
    requirementPage.assertDialogNoUltimateLicenseExists(alertDialog);
  });

  it('should display popup to generate test cases if ultimate license is available and AI server declared in project', () => {
    const requirementPage = initAtRequirementViewPage(
      {
        ultimateLicenseAvailable: true,
        projectPermissions: mockProjectPermissions([
          { projectId: 1, qualifiedName: 'squashtest.acl.group.tm.ProjectManager' },
        ]),
      },
      { aiServerId: 1 },
    );
    const generateTestCaseDialog = new AiTestCaseGenerationDialogElement();
    requirementPage.clickGenerateTestCaseButton();
    generateTestCaseDialog.assertContainsDescription(req3Model.description);
  });

  const req3Model: RequirementVersionModel = mockRequirementVersionModel({
    id: 3,
    projectId: 1,
    name: 'Build Cupola',
    reference: 'M4',
    category: 2,
    createdBy: 'admin',
    createdOn: new Date('2020-09-30 10:30').toISOString(),
    criticality: 'MAJOR',
    requirementId: 3,
    description: 'A description',
    status: 'UNDER_REVIEW',
    versionNumber: 1,
    highLevelRequirement: true,
    requirementStats: {
      children: {
        allTestCaseCount: 0,
        executedTestCase: 0,
        plannedTestCase: 0,
        verifiedTestCase: 0,
        redactedTestCase: 0,
        validatedTestCases: 0,
      },
      total: {
        allTestCaseCount: 5,
        executedTestCase: 2,
        plannedTestCase: 2,
        verifiedTestCase: 1,
        redactedTestCase: 2,
        validatedTestCases: 1,
      },
      currentVersion: {
        allTestCaseCount: 5,
        executedTestCase: 2,
        plannedTestCase: 2,
        verifiedTestCase: 1,
        redactedTestCase: 2,
        validatedTestCases: 1,
      },
      nonObsoleteDescendantsCount: 1,
      coveredDescendantsCount: 1,
      haveChildren: false,
    },
    lowLevelRequirements: [
      mockLinkedLowLevelRequirementModel({
        requirementId: 4,
        requirementVersionId: 4,
        name: 'Get some astronauts',
        reference: 'ASTR-1',
        projectName: 'test-project',
        versionNumber: 2,
        childOfRequirement: false,
      }),
      mockLinkedLowLevelRequirementModel({
        requirementId: 5,
        requirementVersionId: 5,
        name: 'Get some furniture',
        childOfRequirement: false,
      }),
      mockLinkedLowLevelRequirementModel({
        requirementId: 6,
        requirementVersionId: 6,
        name: 'Child Requirement',
        childOfRequirement: true,
      }),
    ],
    verifyingTestCases: [
      {
        id: 1,
        name: 'TestCase 1',
        importance: 'LOW',
        milestoneLabels: '',
        milestoneMaxDate: null,
        milestoneMinDate: null,
        projectName: 'Project 1',
        reference: 'REF.001',
        status: 'APPROVED',
        directlyLinked: true,
        lastExecutionStatus: null,
      },
      {
        id: 2,
        name: 'Mon Cas de test',
        importance: 'VERY_HIGH',
        milestoneLabels: 'Jalon',
        milestoneMaxDate: new Date('2020-10-18'),
        milestoneMinDate: new Date('2020-10-12'),
        projectName: 'Project 42',
        reference: '',
        status: 'TO_BE_UPDATED',
        directlyLinked: false,
        lastExecutionStatus: null,
      },
    ],
    nbIssues: 3,
    aiServerId: null,
    canUseAiFeature: true,
  });

  function mockRequirementTreeResponse() {
    return mockGridResponse('id', [
      mockTreeNode({
        id: 'Requirement-347232',
        children: [],
        state: DataRowOpenState.leaf,
        data: {
          RLN_ID: 347232,
          projectId: 327,
          NAME: '1.03 - Exigence 3',
          HAS_DESCRIPTION: false,
          REFERENCE: '1.03',
          CRITICALITY: 'MINOR',
          REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
          REQ_CATEGORY_ICON: 'monitor',
          REQ_CATEGORY_LABEL: 'requirement.category.CAT_FUNCTIONAL',
          REQ_CATEGORY_TYPE: 'SYS',
          CHILD_COUNT: 0,
          COVERAGE_COUNT: 1,
          IS_SYNCHRONIZED: false,
        },
        projectId: 327,
        parentRowId: 'RequirementLibrary-327',
      }),
      mockTreeNode({
        id: 'Requirement-347231',
        children: [],
        state: DataRowOpenState.leaf,
        data: {
          RLN_ID: 347231,
          projectId: 327,
          NAME: '1.02 - Exigence 2',
          HAS_DESCRIPTION: false,
          REFERENCE: '1.02',
          CRITICALITY: 'CRITICAL',
          REQUIREMENT_STATUS: 'OBSOLETE',
          REQ_CATEGORY_ICON: 'monitor',
          REQ_CATEGORY_LABEL: 'requirement.category.CAT_FUNCTIONAL',
          REQ_CATEGORY_TYPE: 'SYS',
          CHILD_COUNT: 0,
          COVERAGE_COUNT: 4,
          IS_SYNCHRONIZED: false,
        },
        projectId: 327,
        parentRowId: 'RequirementLibrary-327',
      }),
      mockTreeNode({
        id: 'Requirement-347230',
        children: [],
        state: DataRowOpenState.closed,
        data: {
          RLN_ID: 347230,
          projectId: 327,
          NAME: '1.01 - Exigence 1',
          HAS_DESCRIPTION: false,
          REFERENCE: '1.01',
          CRITICALITY: 'MAJOR',
          REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
          REQ_CATEGORY_ICON: 'monitor',
          REQ_CATEGORY_LABEL: 'requirement.category.CAT_FUNCTIONAL',
          REQ_CATEGORY_TYPE: 'SYS',
          CHILD_COUNT: 10,
          COVERAGE_COUNT: 10,
          IS_SYNCHRONIZED: false,
        },
        projectId: 327,
        parentRowId: 'RequirementLibrary-327',
      }),
      mockTreeNode({
        id: 'RequirementLibrary-327',
        children: ['Requirement-347230', 'Requirement-347231', 'Requirement-347232'],
        state: DataRowOpenState.open,
        data: {
          RL_ID: 327,
          projectId: 327,
          NAME: 'Test projet A',
          CHILD_COUNT: 5,
        },
        projectId: 327,
        parentRowId: null,
      }),
    ]);
  }

  function mockOperationReport(linkedLowLevelRequirements: LinkedLowLevelRequirement[]) {
    return {
      linkedLowLevelRequirements: linkedLowLevelRequirements,
      nbIssues: 0,
      requirementStats: mockRequirementStats(),
      summary: {
        alreadyLinked: [],
        alreadyLinkedToAnotherHighLevelRequirement: [],
        childRequirementsInSelection: [],
        highLevelRequirementsInSelection: [],
        requirementWithNotLinkableStatus: [],
      },
      verifyingTestCases: [],
    };
  }

  function mockRequirementStats(): Partial<RequirementVersionStatsBundle> {
    return {
      children: {
        allTestCaseCount: 0,
        executedTestCase: 0,
        plannedTestCase: 0,
        verifiedTestCase: 0,
        redactedTestCase: 0,
        validatedTestCases: 0,
      },
      total: {
        allTestCaseCount: 5,
        executedTestCase: 2,
        plannedTestCase: 2,
        verifiedTestCase: 1,
        redactedTestCase: 2,
        validatedTestCases: 1,
      },
      currentVersion: {
        allTestCaseCount: 5,
        executedTestCase: 2,
        plannedTestCase: 2,
        verifiedTestCase: 1,
        redactedTestCase: 2,
        validatedTestCases: 1,
      },
    };
  }

  function initAtRequirementViewPage(
    referentialDataOverrides: Partial<ReferentialDataModel> = {},
    requirementModelOverrides: Partial<RequirementVersionModel> = {},
  ): HighLevelRequirementViewPage {
    const firstNode = initialNodes.dataRows[0];
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes, {
      ...defaultReferentialData,
      ...referentialDataOverrides,
    });
    const tree = requirementWorkspacePage.tree;

    new NavBarElement().toggle();
    tree.assertNodeExist(firstNode.id);
    tree.assertNodeTextContains(firstNode.id, firstNode.data['NAME']);
    tree.openNode(firstNode.id, getRequirementLibraryChildNodes());
    tree.assertNodeIsOpen(firstNode.id);

    const requirementPage: HighLevelRequirementViewPage = tree.selectNode(
      'HighLevelRequirement-3',
      { ...req3Model, ...requirementModelOverrides },
    );
    requirementPage.assertExists();

    return requirementPage;
  }
});
