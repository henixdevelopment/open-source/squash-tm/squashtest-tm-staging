import { ReferentialDataMockBuilder } from '../../../../utils/referential/referential-data-builder';
import { RequirementVersionViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-view.page';
import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { RequirementViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import {
  mockRequirementVersionModel,
  mockRequirementVersionStatsBundle,
} from '../../../../data-mock/requirements.data-mock';
import { AuthenticationProtocol } from '../../../../../../projects/sqtm-core/src/lib/model/third-party-server/authentication.model';
import { RequirementVersionModel } from '../../../../../../projects/sqtm-core/src/lib/model/requirement/requirement-version.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { mockDataRow, mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

const referentialData = new ReferentialDataMockBuilder()
  .withProjects({
    name: 'Project_Issues',
    bugTrackerBinding: { bugTrackerId: 1, projectId: 3 },
  })
  .withBugTrackers({
    name: 'bugtracker',
    authProtocol: AuthenticationProtocol.BASIC_AUTH,
  })
  .build();

describe('Requirement View - Issues', () => {
  it('should display connection page if user not connected to bugtracker', () => {
    const testCaseViewPage = navigateToRequirementVersion();
    const issuePage = testCaseViewPage.showIssuesWithoutBindingToBugTracker();
    const connectionDialog = issuePage.openConnectionDialog();
    connectionDialog.fillUserName('admin');
    connectionDialog.fillPassword('admin');
    connectionDialog.connection();
  });

  it('should display table issues', () => {
    const modelResponse = {
      entityType: 'requirement-version',
      bugTrackerStatus: 'AUTHENTICATED',
      projectName: '["LELprojet","test","LELprojetclassique","LELclassique","Projet_Test_Arnaud"]',
      projectId: 328,
      delete: '',
      oslc: false,
    };
    const requirementVersionViewPage = navigateToRequirementVersion();
    const gridResponse = mockGridResponse(null, [
      mockDataRow({
        id: 1,
        data: {
          remoteId: 'LE-01',
          btProject: 'Project bt',
          summary: 'summary',
          priority: '4',
          status: 'OK',
          assignee: 'Paul',
          verifiedRequirementVersions: [{ id: 1, name: 'Requirement 2' }],
        },
      }),
    ]);
    const issuePage = requirementVersionViewPage.showIssuesIfBoundToBugTracker(
      modelResponse,
      gridResponse,
    );
    const grid = issuePage.issueGrid;
    const row = grid.getRow(1);
    row.cell('remoteId').linkRenderer().assertContainText('LE-01');
    row.cell('btProject').textRenderer().assertContainsText('Project bt');
    row.cell('summary').textRenderer().assertContainsText('summary');
    row.cell('priority').textRenderer().assertContainsText('4');
    row.cell('status').textRenderer().assertContainsText('OK');
    row.cell('assignee').textRenderer().assertContainsText('Paul');
    row.cell('verifiedRequirementVersions').linkRenderer().assertContainText('Requirement 2');
  });

  function navigateToRequirementVersion(): RequirementVersionViewPage {
    const initialNodes: GridResponse = mockGridResponse('id', [
      mockTreeNode({
        id: 'RequirementLibrary-1',
        children: ['Requirement-3'],
        data: { NAME: 'Project_Issues' },
        state: DataRowOpenState.open,
      }),
      mockTreeNode({
        id: 'Requirement-3',
        children: [],
        parentRowId: 'RequirementLibrary-1',
        state: DataRowOpenState.leaf,
        data: {
          RLN_ID: 3,
          CHILD_COUNT: 0,
          NAME: 'M4 - Build Cupola',
          CRITICALITY: 'CRITICAL',
          REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
          HAS_DESCRIPTION: true,
          REQ_CATEGORY_ICON: 'briefcase',
          REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
          REQ_CATEGORY_TYPE: 'SYS',
          COVERAGE_COUNT: 0,
          IS_SYNCHRONIZED: false,
        },
      }),
    ]);
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(
      initialNodes,
      referentialData,
    );
    new NavBarElement().toggle();
    const tree = requirementWorkspacePage.tree;
    const requirementPage: RequirementViewPage = tree.selectNode('Requirement-3', { ...req3Model });
    requirementPage.toggleTree();
    return requirementPage.currentVersion;
  }

  const req3Model: RequirementVersionModel = mockRequirementVersionModel({
    id: 3,
    projectId: 1,
    name: 'Build Cupola',
    reference: 'M4',
    attachmentList: { id: 1, attachments: [] },
    customFieldValues: [],
    category: 2,
    createdBy: 'admin',
    createdOn: new Date('2020-09-30 10:30').toISOString(),
    criticality: 'MAJOR',
    description: '',
    lastModifiedBy: '',
    lastModifiedOn: null,
    milestones: [],
    requirementId: 3,
    status: 'UNDER_REVIEW',
    versionNumber: 1,
    bindableMilestones: [],
    verifyingTestCases: [],
    requirementVersionLinks: [],
    requirementStats: mockRequirementVersionStatsBundle({
      total: {
        allTestCaseCount: 5,
        executedTestCase: 2,
        plannedTestCase: 2,
        verifiedTestCase: 1,
        redactedTestCase: 2,
        validatedTestCases: 1,
      },
      currentVersion: {
        allTestCaseCount: 5,
        executedTestCase: 2,
        plannedTestCase: 2,
        verifiedTestCase: 1,
        redactedTestCase: 2,
        validatedTestCases: 1,
      },
    }),
    remoteReqPerimeterStatus: null,
  });
});
