import { TestCaseViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { createEntityReferentialData } from '../../../../utils/referential/create-entity-referential.const';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { mockCallStepModel, mockTestCaseModel } from '../../../../data-mock/test-case.data-mock';
import { Parameter } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/parameter.model';
import { Dataset } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/dataset.model';
import { DatasetParamValue } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/dataset-param-value';
import {
  ActionStepModel,
  CallStepModel,
  TestStepModel,
} from '../../../../../../projects/sqtm-core/src/lib/model/test-case/test-step.model';
import { TestCaseModel } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/test-case.model';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

describe('Test Case View - Steps', function () {
  it('should display test case steps', () => {
    const page = navigateToTestCase();
    new NavBarElement().toggle();
    page.toggleTree();
    const testStepPage = page.clickStepsAnchorLink();
    testStepPage.checkOrder([2, 4, 1, 3]);

    // checking that prerequisite is collapsed
    testStepPage.checkPrerequisiteIsCollapsed();

    // checking that all actions steps are expended
    [0, 2, 3].forEach((stepIndex) => {
      const actionStepElement = testStepPage.getActionStepByIndex(stepIndex);
      actionStepElement.checkIndexLabel();
      actionStepElement.checkIsCollapsed();
    });

    // checking that all call steps are collapsed
    const callStepElement = testStepPage.getCallStepByIndex(1);
    callStepElement.checkIsCollapsed();
  });

  it('should collapse/expend all test case steps', () => {
    const page = navigateToTestCase();
    new NavBarElement().toggle();
    page.toggleTree();
    const testStepPage = page.clickStepsAnchorLink();
    testStepPage.collapseAll();
    testStepPage.checkPrerequisiteIsCollapsed();

    [0, 2, 3].forEach((stepIndex) => {
      const actionStep = testStepPage.getActionStepByIndex(stepIndex);
      actionStep.checkIsCollapsed();
    });

    const callStepElement = testStepPage.getCallStepByIndex(1);
    callStepElement.checkIsCollapsed();

    testStepPage.expendAll();
    [0, 2, 3].forEach((stepIndex) => {
      const actionStep = testStepPage.getActionStepByIndex(stepIndex);
      actionStep.checkIsExpended();
    });

    callStepElement.checkIsExpended();
  });

  it('should select multiple steps', () => {
    const page = navigateToTestCase();
    new NavBarElement().toggle();
    page.toggleTree();
    const testStepPage = page.clickStepsAnchorLink();
    const step1 = testStepPage.getActionStepByIndex(0);
    const step2 = testStepPage.getCallStepByIndex(1);
    const step3 = testStepPage.getActionStepByIndex(2);
    const step4 = testStepPage.getActionStepByIndex(3);

    step1.singleSelectStep();
    step1.assertIsSelected();
    step2.assertIsNotSelected();
    step3.assertIsNotSelected();
    step4.assertIsNotSelected();

    step2.singleSelectStep();
    step2.assertIsSelected();
    step1.assertIsNotSelected();

    // adding step 4 to selection
    step4.toggleStepSelection();
    step1.assertIsNotSelected();
    step2.assertIsSelected();
    step3.assertIsNotSelected();
    step4.assertIsSelected();

    // adding step 3
    step3.toggleStepSelection();
    step1.assertIsNotSelected();
    step2.assertIsSelected();
    step3.assertIsSelected();
    step4.assertIsSelected();

    // checking that collapse doesn't change selection
    testStepPage.collapseAll();
    step1.assertIsNotSelected();
    step2.assertIsSelected();
    step3.assertIsSelected();
    step4.assertIsSelected();

    // removing step 3
    step3.toggleStepSelection();
    step1.assertIsNotSelected();
    step2.assertIsSelected();
    step3.assertIsNotSelected();
    step4.assertIsSelected();

    // force select step 4 and deselect all
    step4.singleSelectStep();
    step1.assertIsNotSelected();
    step2.assertIsNotSelected();
    step3.assertIsNotSelected();
    step4.assertIsSelected();

    // unselect without ctrl
    step4.singleSelectStep();
    step1.assertIsNotSelected();
    step2.assertIsNotSelected();
    step3.assertIsNotSelected();
    step4.assertIsNotSelected();
  });

  it('should create param in test step', () => {
    const page = navigateToTestCase();
    new NavBarElement().toggle();
    page.toggleTree();
    const testStepPage = page.clickStepsAnchorLink();
    const step1 = testStepPage.getActionStepByIndex(0);
    step1.extendStep();
    const response = {
      parameters: [{ id: 1, name: 'param_42' } as Parameter],
      paramValues: [],
      dataSets: [],
    };
    step1.modifyFilledStepWithParam('Action ${param_42}', 'action', 'test-steps/*', response);
    const parameterPage = page.clickParametersAnchorLink();
    const parameterTable = parameterPage.parametersTable;
    const cell = parameterTable.getHeaderRow().cell('PARAM_COLUMN_1');
    cell.assertExists();
    cell.textRenderer().assertContainsText('param_42');
  });

  it('should forbid update test step if parameter pattern name is wrong', () => {
    const page = navigateToTestCase();
    new NavBarElement().toggle();
    page.toggleTree();
    const testStepPage = page.clickStepsAnchorLink();
    const step1 = testStepPage.getActionStepByIndex(0);
    step1.extendStep();
    const error = {
      squashTMError: {
        actionValidationError: {
          i18nKey: 'sqtm-core.error.test-step.parameter.name',
        },
        kind: 'ACTION_ERROR',
      },
    };
    step1.modifyFilledStepOnErrorWithParam(
      'wrong param ${param 42} of ${23}',
      'action',
      'test-steps/*',
      error,
    );
    const alertDialog = testStepPage.parameterNameAlertDialog();
    alertDialog.assertExists();
    alertDialog.assertMessage();
  });

  it('should show delegate param dialog', () => {
    const page = navigateToTestCase();
    new NavBarElement().toggle();
    page.toggleTree();
    const testStepPage = page.clickStepsAnchorLink();
    const callStep = testStepPage.getCallStepByIndex(1);
    const dataSetLink = callStep.getLinkElement('dataset');
    dataSetLink.containText('Choisir un jeu de données');

    const dataSets = [{ id: 1, name: 'JDD' }];
    const dialog = callStep.showDelegateParamDialog(dataSets);
    dialog.shouldExist();
    dialog.radioIsSelected('available-dataset');
    dialog.radioNotSelected('no-dataset');
    const dataSetField = dialog.getSelectField();
    dataSetField.checkSelectedOption('Aucun');
    dataSetField.checkAllOptions(['Aucun', 'JDD']);
    dataSetField.setValue('JDD');
    dataSetField.checkSelectedOption('JDD');

    const response = {
      parameters: [] as Parameter[],
      dataSets: [] as Dataset[],
      paramValues: [] as DatasetParamValue[],
    };
    dialog.confirm(response);
    dataSetLink.containText('JDD');
  });

  it('should change delegate param', () => {
    const page = navigateToTestCase();
    new NavBarElement().toggle();
    page.toggleTree();
    const testStepPage = page.clickStepsAnchorLink();
    const callStep = testStepPage.getCallStepByIndex(1);
    const dataSetLink = callStep.getLinkElement('dataset');
    dataSetLink.containText('Choisir un jeu de données');
    const dialog = callStep.showDelegateParamDialog([]);
    dialog.shouldExist();
    dialog.selectRadio('no-dataset');
    dialog.confirm({
      parameters: [{ id: 1, name: 'Parameter', sourceTestCaseId: 4 } as Parameter],
      dataSets: [],
      paramValues: [],
    });
    dataSetLink.containText('(paramètres délégués)');
    const parameterPage = page.clickParametersAnchorLink();
    const parameterTable = parameterPage.parametersTable;
    const cell = parameterTable.getHeaderRow().cell('PARAM_COLUMN_1');
    cell.assertExists();
    cell.textRenderer().assertContainsText('Parameter');
  });

  it('should create call test case', () => {
    const workspacePage = initTestCaseWorkspacePage();
    const testCaseModel = createTestCaseModel([]);
    const testCaseViewPage = workspacePage.tree.selectNode<TestCaseViewPage>(
      'TestCase-3',
      testCaseModel,
    );
    const testStepPage = testCaseViewPage.clickStepsAnchorLink();
    testStepPage.assertNewStepEditorsAreReady();

    workspacePage.tree.beginDragAndDrop('TestCase-4');
    testStepPage.enterIntoTestCase();
    const testSteps = {
      testSteps: [
        {
          id: 1,
          stepOrder: 0,
          testCaseId: 3,
          calledTcId: 4,
          calledTcName: 'Launch Atlantis 2',
          calledDatasetId: null,
          calledDatasetName: null,
          delegateParam: false,
          calledTestCaseSteps: [],
          kind: 'call-step',
        },
      ],
      testCaseImportance: 'LOW',
    };
    testStepPage.dropCalledTestCase(3, testSteps);
    const callStep = testStepPage.getCallStepByIndex(0);
    callStep.getLinkElement('calledTestCase').containText('Launch Atlantis 2');
    callStep.getLinkElement('dataset').containText('Choisir un jeu de données');
  });
});

function initTestCaseWorkspacePage(): TestCaseWorkspacePage {
  const initialNodes = mockGridResponse('id', [
    mockTreeNode({
      id: 'TestCaseLibrary-1',
      children: ['TestCase-3', 'TestCase-4'],
      data: { NAME: 'Project1', CHILD_COUNT: 2 },
      state: DataRowOpenState.open,
    }),
    mockTreeNode({
      id: 'TestCase-3',
      children: [],
      projectId: 1,
      parentRowId: 'TestCaseLibrary-1',
      state: DataRowOpenState.leaf,
      data: {
        NAME: 'Launch Atlantis',
        CHILD_COUNT: 0,
        TC_STATUS: 'APPROVED',
        TC_KIND: 'STANDARD',
        IMPORTANCE: 'LOW',
      },
    }),
    mockTreeNode({
      id: 'TestCase-4',
      children: [],
      projectId: 1,
      parentRowId: 'TestCaseLibrary-1',
      state: DataRowOpenState.leaf,
      data: {
        NAME: 'Launch Atlantis 2',
        CHILD_COUNT: 0,
        TC_STATUS: 'APPROVED',
        TC_KIND: 'STANDARD',
        IMPORTANCE: 'LOW',
      },
    }),
  ]);
  return TestCaseWorkspacePage.initTestAtPage(initialNodes, createEntityReferentialData);
}

function navigateToTestCase(): TestCaseViewPage {
  const testCaseWorkspacePage = initTestCaseWorkspacePage();
  const actionStep1: ActionStepModel = {
    id: 2,
    projectId: 1,
    stepOrder: 0,
    customFieldValues: [],
    attachmentList: { id: 2, attachments: [] },
    kind: 'action-step',
    action: 'check main engines glimbal',
    expectedResult: 'main engines ok to handle corrections',
  };

  const callStep1: CallStepModel = mockCallStepModel({
    id: 4,
    stepOrder: 1,
    projectId: 1,
    calledTcId: 12,
    calledTcName: 'main engines sequence start',
    kind: 'call-step',
    calledDatasetId: null,
    calledTestCaseSteps: [],
    delegateParam: false,
  });

  const actionStep2: ActionStepModel = {
    id: 1,
    projectId: 1,
    stepOrder: 2,
    customFieldValues: [],
    attachmentList: { id: 3, attachments: [] },
    kind: 'action-step',
    action: 'solid rocket booster ignition',
    expectedResult: ' big petards are running',
  };

  const actionStep3: ActionStepModel = {
    id: 3,
    projectId: 1,
    stepOrder: 3,
    customFieldValues: [],
    attachmentList: { id: 3, attachments: [] },
    kind: 'action-step',
    action: 'release fixation arms',
    expectedResult: '... and we have lift off. Space shuttle Atlantis is returning to the ISS',
  };

  const testSteps: TestStepModel[] = [actionStep1, callStep1, actionStep2, actionStep3];
  const model = createTestCaseModel(testSteps);
  return testCaseWorkspacePage.tree.selectNode<TestCaseViewPage>('TestCase-3', model);
}

function createTestCaseModel(testSteps: TestStepModel[]): TestCaseModel {
  return mockTestCaseModel({
    id: 3,
    testSteps,
  });
}
