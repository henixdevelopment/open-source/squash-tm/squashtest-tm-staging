import { TestCaseViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { createEntityReferentialData } from '../../../../utils/referential/create-entity-referential.const';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { mockTestCaseModel } from '../../../../data-mock/test-case.data-mock';
import { TestCaseModel } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/test-case.model';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

describe('Test Case View - Sessions', function () {
  it('should display exploratory test case sessions', () => {
    const page = navigateToTestCase();
    new NavBarElement().toggle();
    page.toggleTree();

    const sessions: GridResponse = mockGridResponse('overviewId', [
      mockTreeNode({
        id: '64',
        data: {
          overviewId: 64,
          executionStatus: 'SUCCESS',
          executionCount: 3,
          executionPath: 'P1 > Campagne 1> Itération 1',
          projectId: 1,
          nbIssues: 0,
          lastModifiedOn: null,
        },
        projectId: 1,
      }),
      mockTreeNode({
        id: '65',
        data: {
          overviewId: 65,
          executionStatus: 'FAILURE',
          executionCount: 2,
          executionPath: 'P1 > Campagne 1 >Itération 2 > Suite 1',
          projectId: 1,
          nbIssues: 2,
          lastModifiedOn: null,
        },
        projectId: 1,
      }),
    ]);

    const executionPage = page.showSessions(sessions);
    const grid = executionPage.grid;
    grid.assertRowCount(2);

    const row1 = grid.getRow(64);
    row1
      .cell(GridColumnId.executionPath)
      .textRenderer()
      .assertContainsText('P1 > Campagne 1> Itération 1');
    row1.cell(GridColumnId.executionCount).textRenderer().assertContainsText('3');

    const row2 = grid.getRow(65);
    row2
      .cell(GridColumnId.executionPath)
      .textRenderer()
      .assertContainsText('P1 > Campagne 1 >Itération 2 > Suite 1');
    row2.cell(GridColumnId.executionCount).textRenderer().assertContainsText('2');
  });

  function navigateToTestCase(): TestCaseViewPage {
    const initialNodes = mockGridResponse('id', [
      mockTreeNode({
        id: 'TestCaseLibrary-1',
        children: ['TestCase-3'],
        projectId: 1,
        data: { NAME: 'Project1', CHILD_COUNT: 1 },
        state: DataRowOpenState.open,
      }),
      mockTreeNode({
        id: 'TestCase-3',
        children: [],
        projectId: 1,
        parentRowId: 'TestCaseLibrary-1',
        data: {
          NAME: 'TestCase3',
          CHILD_COUNT: 0,
          TC_STATUS: 'APPROVED',
          TC_KIND: 'EXPLORATORY',
          IMPORTANCE: 'LOW',
        },
      }),
    ]);

    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(
      initialNodes,
      createEntityReferentialData,
    );

    const model: TestCaseModel = mockTestCaseModel({
      id: 3,
      projectId: 1,
      name: 'TestCase3',
      kind: 'EXPLORATORY',
    });

    return testCaseWorkspacePage.tree.selectNode<TestCaseViewPage>('TestCase-3', model);
  }
});
