import { TestCaseViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { defaultReferentialData } from '../../../../utils/referential/default-referential-data.const';
import { mockTestCaseModel } from '../../../../data-mock/test-case.data-mock';
import { TestCaseModel } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/test-case.model';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { TestCaseViewCharterPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-charter.page';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';

const initialTestCaseRow = mockTreeNode({
  id: 'TestCase-3',
  parentRowId: 'TestCaseLibrary-1',
  data: {
    NAME: 'TestCase3',
    CHILD_COUNT: 0,
    TC_STATUS: 'APPROVED',
    TC_KIND: 'EXPLORATORY',
    IMPORTANCE: 'HIGH',
  },
});

describe('Test Case View - Charter', function () {
  it('should display and toggle exploratory test case charter', () => {
    const charterPage = navigateToTestCase();

    // By default, charter panel is extended in non-edit mode
    charterPage.charterRichField.assertExists();
    charterPage.charterRichField.assertIsEditable();
    charterPage.charterRichField.assertIsNotInEditMode();

    // It can be collapsed
    charterPage.toggleCharter();
    charterPage.charterRichField.assertNotExist();

    // It can be extended back
    charterPage.toggleCharter();
    charterPage.charterRichField.assertExists();
    charterPage.charterRichField.assertIsNotInEditMode();

    // And it can be set to edit mode by clicking the charter value preview
    charterPage.toggleCharter();
    charterPage.clickNonEditableCharter();
    charterPage.charterRichField.assertIsInEditMode();
  });

  it('should update exploratory test case charter', () => {
    const charterPage = navigateToTestCase();
    charterPage.charterRichField.setAndConfirmValue('a brand new charter!');
    charterPage.charterRichField.checkTextContent('a brand new charter!');
  });

  it('should update duration with keyboard', () => {
    const charterPage = navigateToTestCase();
    charterPage.durationField.checkPlaceholder();
    charterPage.durationField.assertIsEditable();
    writeTimeWithKeyboardAndCheckContent('02', '15');
  });

  it('should delete character when backspacing', () => {
    const charterPage = navigateToTestCase();
    charterPage.durationField.enableEditMode();
    cy.get('body').type('23').type('{backspace}');
    charterPage.durationField.checkHourInputModelValue('2');
  });

  it('should keep value if trying to enter a wrong character when the field is highlighted', () => {
    const charterPage = navigateToTestCase();
    charterPage.durationField.enableEditMode();
    writeTimeWithKeyboardAndCheckContent('02', '10');
    charterPage.durationField.enableEditMode();
    writeCharactersAndCheckContent('/', '/', '2', '10', charterPage);
  });

  it('should not allow alphabetic characters', () => {
    const charterPage = navigateToTestCase();
    charterPage.durationField.enableEditMode();
    writeCharactersAndCheckContent('4a', '10b', '4', '10', charterPage);
  });

  it('should not allow special characters', () => {
    const charterPage = navigateToTestCase();
    charterPage.durationField.enableEditMode();
    writeCharactersAndCheckContent('/@', '-11', '0', '11', charterPage);
  });

  it('should not allow values superior to 23 and 59', () => {
    const charterPage = navigateToTestCase();
    charterPage.durationField.enableEditMode();
    writeCharactersAndCheckContent('123', '598', '23', '59', charterPage);
  });

  it('should not allow negative numbers', () => {
    const charterPage = navigateToTestCase();
    charterPage.durationField.enableEditMode();
    writeCharactersAndCheckContent('-02', '-25', '2', '25', charterPage);
  });

  it('should not allow decimal numbers', () => {
    const charterPage = navigateToTestCase();
    charterPage.durationField.enableEditMode();
    writeCharactersAndCheckContent('0.2', '2.5', '2', '25', charterPage);
  });

  it('should update duration with keyboard arrows', () => {
    selectTimeWithKeyboardArrowsAndCheckContent(3, 2);
  });

  it('should update duration with mouse', () => {
    const charterPage = navigateToTestCase();
    charterPage.durationField.checkPlaceholder();
    charterPage.durationField.assertIsEditable();
    selectTimeInListAndCheckContent('04', '10');
  });

  it('should not update duration when cancelling', () => {
    selectTimeAndCancelAndCheckContent('02', '20');
  });

  function navigateToTestCase(): TestCaseViewCharterPage {
    const initialNodes = mockGridResponse('id', [
      mockTreeNode({
        id: 'TestCaseLibrary-1',
        data: { NAME: 'Project1', CHILD_COUNT: 1 },
        children: ['TestCase-3'],
        state: DataRowOpenState.open,
      }),
      { ...initialTestCaseRow },
    ]);

    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(
      initialNodes,
      defaultReferentialData,
    );

    const model: TestCaseModel = mockTestCaseModel({
      kind: 'EXPLORATORY',
      charter: 'Default charter value',
    });
    const tcPage = testCaseWorkspacePage.tree.selectNode<TestCaseViewPage>('TestCase-3', model);
    return tcPage.clickCharterAnchorLink();
  }

  function writeTimeWithKeyboardAndCheckContent(hour: string, minutes: string) {
    const charterPage = navigateToTestCase();
    charterPage.durationField.enableEditMode();

    const mockedMinutes = Number(hour) * 60 + Number(minutes);
    const mock = new HttpMockBuilder('test-case/*/session-duration')
      .post()
      .responseBody(mockedMinutes)
      .build();
    cy.get('body').type(hour).type('{enter}').type(minutes).type('{enter}');
    mock.wait();
    charterPage.durationField.checkTimeContent(hour, minutes);
  }

  function writeCharactersAndCheckContent(
    typedHour: string,
    typedMinutes: string,
    expectedHourModelResult: string,
    expectedMinutesModelResult: string,
    charterPage: TestCaseViewCharterPage,
  ) {
    cy.get('body').type(typedHour).type('{enter}').type(typedMinutes);
    charterPage.durationField.checkHourInputModelValue(expectedHourModelResult);
    charterPage.durationField.checkMinutesInputModelValue(expectedMinutesModelResult);
  }

  function selectTimeInListAndCheckContent(selectedHour: string, selectedMinutes: string) {
    const charterPage = navigateToTestCase();
    selectHourAndMinutesInList(charterPage, selectedHour, selectedMinutes);
    charterPage.durationField.confirm();
    charterPage.durationField.checkTimeContent(selectedHour, selectedMinutes);
  }

  function selectTimeAndCancelAndCheckContent(selectedHour: string, selectedMinutes: string) {
    const charterPage = navigateToTestCase();
    selectHourAndMinutesInList(charterPage, selectedHour, selectedMinutes);
    charterPage.durationField.cancel();
    charterPage.durationField.checkContent('');
  }

  function selectHourAndMinutesInList(
    charterPage: TestCaseViewCharterPage,
    selectedHour: string,
    selectedMinutes: string,
  ) {
    charterPage.durationField.enableEditMode();
    charterPage.durationField.selectInList(selectedHour);
    charterPage.durationField.selectInList(selectedMinutes);
  }

  function selectTimeWithKeyboardArrowsAndCheckContent(
    expectedHour: number,
    expectedMinutes: number,
  ) {
    const charterPage = navigateToTestCase();
    charterPage.durationField.enableEditMode();
    charterPage.durationField.updateTimeWithKeyboardArrows(expectedHour, expectedMinutes);
    charterPage.durationField.checkHourInputModelValue(expectedHour.toString());
    charterPage.durationField.checkMinutesInputModelValue(expectedMinutes.toString());
    charterPage.durationField.confirm();
    const hour = expectedHour.toString().length === 1 ? `0${expectedHour}` : `${expectedHour}`;
    const minutes =
      expectedMinutes.toString().length === 1 ? `0${expectedMinutes}` : `${expectedMinutes}`;
    charterPage.durationField.checkTimeContent(hour, minutes);
  }
});
