import { TestCaseViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { ReferentialDataMockBuilder } from '../../../../utils/referential/referential-data-builder';
import { mockTestCaseModel } from '../../../../data-mock/test-case.data-mock';
import { AuthenticationProtocol } from '../../../../../../projects/sqtm-core/src/lib/model/third-party-server/authentication.model';
import { TestCaseModel } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/test-case.model';
import {
  DataRowModel,
  DataRowOpenState,
} from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

const initialTestCaseRow: DataRowModel = mockTreeNode({
  id: 'TestCase-3',
  children: [],
  projectId: 1,
  parentRowId: 'TestCaseLibrary-1',
  data: {
    NAME: 'TestCase3',
    CHILD_COUNT: 0,
    TC_STATUS: 'APPROVED',
    TC_KIND: 'STANDARD',
    IMPORTANCE: 'LOW',
  },
});

const referentialData = new ReferentialDataMockBuilder()
  .withProjects({
    name: 'Project_Issues',
    bugTrackerBinding: { bugTrackerId: 1, projectId: 3 },
  })
  .withBugTrackers({
    name: 'bugtracker',
    authProtocol: AuthenticationProtocol.BASIC_AUTH,
  })
  .build();

describe('Test Case View - Issues', () => {
  it('should display connection page if user not connected to bugtracker', () => {
    const testCaseViewPage = navigateToTestCase();
    const issuePage = testCaseViewPage.showIssuesWithoutBindingToBugTracker();
    const connectionDialog = issuePage.openConnectionDialog();
    connectionDialog.fillUserName('admin');
    connectionDialog.fillPassword('admin');
    connectionDialog.connection();
  });

  it('should display table issues', () => {
    const modelResponse = {
      entityType: 'test-case',
      bugTrackerStatus: 'AUTHENTICATED',
      projectName: '["LELprojet","test","LELprojetclassique","LELclassique","Projet_Test_Arnaud"]',
      projectId: 328,
      delete: '',
      oslc: false,
    };
    const testCaseViewPage = navigateToTestCase();
    const gridResponse = {
      dataRows: [
        {
          data: {},
        },
      ],
    } as GridResponse;

    testCaseViewPage.showIssuesIfBoundToBugTracker(modelResponse, gridResponse);
  });

  function navigateToTestCase(): TestCaseViewPage {
    const initialNodes = mockGridResponse('id', [
      mockTreeNode({
        id: 'TestCaseLibrary-1',
        children: ['TestCase-3'],
        data: { NAME: 'Project_Issues', CHILD_COUNT: 1 },
        state: DataRowOpenState.open,
      }),
      initialTestCaseRow,
    ]);
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(
      initialNodes,
      referentialData,
    );
    const model: TestCaseModel = mockTestCaseModel({
      id: 3,
      projectId: 1,
      name: 'TestCase3',
      customFieldValues: [],
      attachmentList: {
        id: 1,
        attachments: [],
      },
      reference: '',
      description: '',
      uuid: '',
      type: 20,
      testSteps: [],
      status: 'WORK_IN_PROGRESS',
      prerequisite: '',
      parameters: [],
      nbIssues: 2,
      nature: 12,
      milestones: [],
      lastModifiedOn: new Date('2020-03-09 10:30').toISOString(),
      lastModifiedBy: 'admin',
      kind: 'STANDARD',
      importanceAuto: false,
      importance: 'LOW',
      executions: [],
      datasets: [],
      datasetParamValues: [],
      createdOn: new Date('2020-03-09 10:30').toISOString(),
      createdBy: 'admin',
      coverages: [],
      automationRequest: null,
      automatable: 'M',
      calledTestCases: [],
      lastExecutionStatus: 'SUCCESS',
      script: '',
    });
    return testCaseWorkspacePage.tree.selectNode<TestCaseViewPage>('TestCase-3', model);
  }
});
