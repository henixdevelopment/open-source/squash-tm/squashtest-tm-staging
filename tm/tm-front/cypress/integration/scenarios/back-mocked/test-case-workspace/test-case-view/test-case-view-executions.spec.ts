import { TestCaseViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { createEntityReferentialData } from '../../../../utils/referential/create-entity-referential.const';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { mockTestCaseModel } from '../../../../data-mock/test-case.data-mock';
import { TestCaseModel } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/test-case.model';
import {
  DataRowModel,
  DataRowOpenState,
} from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { mockDataRow, mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

const initialTestCaseRow: DataRowModel = mockTreeNode({
  id: 'TestCase-3',
  children: [],
  projectId: 1,
  parentRowId: 'TestCaseLibrary-1',
  data: {
    NAME: 'TestCase3',
    CHILD_COUNT: 0,
    TC_STATUS: 'APPROVED',
    TC_KIND: 'STANDARD',
    IMPORTANCE: 'LOW',
  },
});
describe('Test Case View - Executions', function () {
  it('should display test case execution', () => {
    const page = navigateToTestCase();
    new NavBarElement().toggle();
    page.toggleTree();

    const executions = mockGridResponse('id', [
      mockDataRow({
        id: 1,
        data: {
          id: 1,
          datasetsName: '',
          executionMode: 'MANUAL',
          executionOrder: 0,
          executionStatus: 'SUCCESS',
          lastExecutedBy: 'admin',
          lastExecutedOn: '2020-06-20',
          nbIssues: 0,
          executionPath: 'Project1 > Campaign1 > Iteration1 > TestSuite1',
          execPlanType: 'TEST_SUITE',
        },
      }),
    ]);

    const executionPage = page.showExecutions(executions);
    const grid = executionPage.grid;
    grid.assertRowExist(1);
    const row = grid.getRow(1);
    row.cell(GridColumnId.execPlanType).textRenderer().assertContainsText('Suite de tests');
    row
      .cell(GridColumnId.executionPath)
      .textRenderer()
      .assertContainsText('Project1 > Campaign1 > Iteration1 > TestSuite1');
  });

  function navigateToTestCase(): TestCaseViewPage {
    const initialNodes = mockGridResponse('id', [
      mockTreeNode({
        id: 'TestCaseLibrary-1',
        children: ['TestCase-3'],
        data: { NAME: 'Project1', CHILD_COUNT: 1 },
        state: DataRowOpenState.open,
      }),
      initialTestCaseRow,
    ]);
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(
      initialNodes,
      createEntityReferentialData,
    );
    const model: TestCaseModel = mockTestCaseModel({
      id: 3,
      projectId: 1,
      name: 'TestCase3',
      customFieldValues: [],
      attachmentList: {
        id: 1,
        attachments: [],
      },
      reference: '',
      description: '',
      uuid: '',
      type: 20,
      testSteps: [],
      status: 'WORK_IN_PROGRESS',
      prerequisite: '',
      parameters: [],
      nbIssues: 0,
      nature: 12,
      milestones: [],
      lastModifiedOn: new Date('2020-03-09 10:30').toISOString(),
      lastModifiedBy: 'admin',
      kind: 'STANDARD',
      importanceAuto: false,
      importance: 'LOW',
      executions: [],
      datasets: [],
      datasetParamValues: [],
      createdOn: new Date('2020-03-09 10:30').toISOString(),
      createdBy: 'admin',
      coverages: [],
      automationRequest: null,
      automatable: 'M',
      calledTestCases: [],
      lastExecutionStatus: 'SUCCESS',
      script: '',
    });
    return testCaseWorkspacePage.tree.selectNode<TestCaseViewPage>('TestCase-3', model);
  }
});
