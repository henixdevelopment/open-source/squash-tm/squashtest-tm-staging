import { TestCaseViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { createEntityReferentialData } from '../../../../utils/referential/create-entity-referential.const';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { mockTestCaseModel } from '../../../../data-mock/test-case.data-mock';
import { ChangeCoverageOperationReport } from '../../../../../../projects/sqtm-core/src/lib/model/change-coverage-operation-report';
import { TestCaseModel } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/test-case.model';
import { RequirementVersionCoverage } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/requirement-version-coverage-model';
import {
  DataRowModel,
  DataRowOpenState,
} from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

const initialTestCaseRow: DataRowModel = mockTreeNode({
  id: 'TestCase-3',
  children: [],
  projectId: 1,
  parentRowId: 'TestCaseLibrary-1',
  data: {
    NAME: 'TestCase3',
    CHILD_COUNT: 0,
    TC_STATUS: 'APPROVED',
    TC_KIND: 'STANDARD',
    IMPORTANCE: 'LOW',
  },
});

describe('Test Case View - Coverages', function () {
  it('should display test case coverages', () => {
    const page = navigateToTestCase();
    // folding tree and navbar to avoid nasty scroll bar in grids
    new NavBarElement().toggle();
    page.toggleTree();
    const coverageTables = page.coveragesTable;
    coverageTables.assertExists();
    coverageTables.assertRowExist(1);
    const row = coverageTables.getRow(1);
    row.cell(GridColumnId.projectName).textRenderer().assertContainsText('Project 1');
    row.cell(GridColumnId.reference).textRenderer().assertContainsText('');
    row.cell(GridColumnId.name).linkRenderer().assertContainText('Requirement 1');
    row
      .cell(GridColumnId.criticality)
      .iconRenderer()
      .assertContainsIcon('anticon-sqtm-core-requirement:double_up');
    row
      .cell(GridColumnId.status)
      .iconRenderer()
      .assertContainsIcon('anticon-sqtm-core-requirement:status');
    // coverageTables.scrollPosition('right', 'mainViewport');
    const verifiedByCell = row.cell(GridColumnId.verifiedBy);
    verifiedByCell.assertExists();
    verifiedByCell.textRenderer().assertContainsText('Pas de test #1');

    const secondRow = coverageTables.getRow(2);
    // coverageTables.scrollPosition('left', 'mainViewport');
    secondRow.cell(GridColumnId.projectName).textRenderer().assertContainsText('Project 1');
    secondRow.cell(GridColumnId.reference).textRenderer().assertContainsText('');
    secondRow.cell(GridColumnId.name).linkRenderer().assertContainText('Requirement 2');
    secondRow
      .cell(GridColumnId.criticality)
      .iconRenderer()
      .assertContainsIcon('anticon-sqtm-core-requirement:up');
    secondRow
      .cell(GridColumnId.status)
      .iconRenderer()
      .assertContainsIcon('anticon-sqtm-core-requirement:status');
    // coverageTables.scrollPosition('right', 'mainViewport');
    const secondRowVerifiedByCell = secondRow.cell('verifiedBy');
    secondRowVerifiedByCell.assertExists();
  });

  it('should add coverages', () => {
    const page = navigateToTestCase();
    new NavBarElement().toggle();
    page.toggleTree();
    const requirementResponse = mockGridResponse('id', [
      mockTreeNode({
        id: 'RequirementLibrary-1',
        children: ['Requirement-4', 'Requirement-5'],
        data: { NAME: 'Project1' },
        projectId: 1,
        state: DataRowOpenState.open,
      }),
      mockTreeNode({
        id: 'Requirement-4',
        children: [],
        parentRowId: 'RequirementLibrary-1',
        projectId: 1,
        data: {
          NAME: 'Requirement4',
          CRITICALITY: 'MAJOR',
          REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
          HAS_DESCRIPTION: true,
          REQ_CATEGORY_ICON: 'briefcase',
          REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
          REQ_CATEGORY_TYPE: 'SYS',
          COVERAGE_COUNT: 0,
          IS_SYNCHRONIZED: false,
        },
      }),
      mockTreeNode({
        id: 'Requirement-5',
        children: [],
        parentRowId: 'RequirementLibrary-1',
        projectId: 1,
        data: {
          NAME: 'Requirement5',
          CRITICALITY: 'MAJOR',
          REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
          HAS_DESCRIPTION: true,
          REQ_CATEGORY_ICON: 'briefcase',
          REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
          REQ_CATEGORY_TYPE: 'SYS',
          COVERAGE_COUNT: 0,
          IS_SYNCHRONIZED: false,
        },
      }),
    ]);
    const requirementDrawer = page.openRequirementDrawer(requirementResponse);
    requirementDrawer.beginDragAndDrop('Requirement-4');
    page.enterIntoTestCase();
    const coverageResponse = createCoverages([
      {
        criticality: 'CRITICAL',
        directlyVerified: true,
        name: 'Requirement4',
        projectName: 'Project 1',
        reference: '',
        requirementVersionId: 4,
        status: 'WORK_IN_PROGRESS',
        stepIndex: 0,
        unDirectlyVerified: false,
        verifiedBy: 'admin',
        verifyingCalledTestCaseIds: [12],
        coverageStepInfos: [{ id: 11, index: 0 }],
        verifyingTestCaseId: 3,
        milestoneLabels: '',
        milestoneMaxDate: '',
        milestoneMinDate: '',
      },
    ]);
    const coverageOperationReport: ChangeCoverageOperationReport = {
      coverages: coverageResponse,
      summary: {
        notLinkableRejections: false,
        noVerifiableVersionRejections: false,
        alreadyVerifiedRejections: false,
      },
    };
    page.dropRequirementIntoTestCase(3, coverageOperationReport);
    page.closeRequirementDrawer();
    const coverageTables = page.coveragesTable;
    const newCoverage = coverageTables.getRow(4);
    newCoverage.cell('name').linkRenderer().assertContainText('Requirement4');
  });

  it('should navigate to search requirement for coverages', () => {
    const page = navigateToTestCase();
    new NavBarElement().toggle();
    page.toggleTree();
    const requirementForCoverageSearchPage = page.navigateToSearchRequirementForCoverage();
    requirementForCoverageSearchPage.assertExists();
    requirementForCoverageSearchPage.assertLinkSelectionButtonExist();
    requirementForCoverageSearchPage.assertLinkAllButtonExist();
  });

  it('should remove coverages', () => {
    const page = navigateToTestCase();
    new NavBarElement().toggle();
    page.toggleTree();
    const coverageTable = page.coveragesTable;
    let coveragesDialogElement = page.showDeleteConfirmCoveragesDialog(3, [1]);
    coveragesDialogElement.assertNotExist();
    coverageTable.selectRow(1, '#', 'leftViewport');
    coveragesDialogElement = page.showDeleteConfirmCoveragesDialog(3, [1]);
    coveragesDialogElement.assertExists();
    coveragesDialogElement.deleteForSuccess({
      coverages: [
        {
          criticality: 'MAJOR',
          directlyVerified: true,
          name: 'Requirement 2',
          projectName: 'Project 1',
          reference: '',
          requirementVersionId: 2,
          status: 'APPROVED',
          stepIndex: 2,
          unDirectlyVerified: true,
          verifiedBy: 'admin',
          verifyingCalledTestCaseIds: [12],
          verifyingTestCaseId: 3,
          coverageStepInfos: [{ id: 11, index: 0 }],
        },
      ],
    });
    coveragesDialogElement.assertNotExist();
    coverageTable.assertRowCount(1);
  });

  it('should remove coverage directly in table', () => {
    const page = navigateToTestCase();
    new NavBarElement().toggle();
    page.toggleTree();
    const coverageTable = page.coveragesTable;
    const coveragesDialogElement = page.showDeleteConfirmCoverageDialog(3, 2);
    coveragesDialogElement.assertExists();
    coveragesDialogElement.deleteForSuccess({
      coverages: [
        {
          criticality: 'CRITICAL',
          directlyVerified: true,
          name: 'Requirement 1',
          projectName: 'Project 1',
          reference: '',
          requirementVersionId: 1,
          status: 'WORK_IN_PROGRESS',
          stepIndex: 0,
          stepIndexes: [0],
          unDirectlyVerified: false,
          verifiedBy: 'admin',
          verifyingCalledTestCaseIds: [12],
          verifyingStepIds: [11],
          coverageStepInfos: [{ id: 11, index: 0 }],
          verifyingTestCaseId: 3,
        },
      ],
    });
    coveragesDialogElement.assertNotExist();
    coverageTable.assertRowCount(1);
  });

  function navigateToTestCase(): TestCaseViewPage {
    const initialNodes = mockGridResponse('id', [
      mockTreeNode({
        id: 'TestCaseLibrary-1',
        children: ['TestCase-3'],
        data: { NAME: 'Project1', CHILD_COUNT: 1 },
        state: DataRowOpenState.open,
      }),
      initialTestCaseRow,
    ]);
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(
      initialNodes,
      createEntityReferentialData,
    );
    const model: TestCaseModel = mockTestCaseModel({
      id: 3,
      projectId: 1,
      name: 'TestCase3',
      customFieldValues: [],
      attachmentList: {
        id: 1,
        attachments: [],
      },
      reference: '',
      description: '',
      uuid: '',
      type: 20,
      testSteps: [],
      status: 'WORK_IN_PROGRESS',
      prerequisite: '',
      parameters: [],
      nbIssues: 0,
      nature: 12,
      milestones: [],
      lastModifiedOn: new Date('2020-03-09 10:30').toISOString(),
      lastModifiedBy: 'admin',
      kind: 'STANDARD',
      importanceAuto: false,
      importance: 'LOW',
      executions: [],
      datasets: [],
      datasetParamValues: [],
      createdOn: new Date('2020-03-09 10:30').toISOString(),
      createdBy: 'admin',
      coverages: createCoverages(),
      automationRequest: null,
      automatable: 'M',
      calledTestCases: [],
      lastExecutionStatus: 'SUCCESS',
      script: '',
    });
    return testCaseWorkspacePage.tree.selectNode<TestCaseViewPage>('TestCase-3', model);
  }
});

function createCoverages(
  newCoverages?: RequirementVersionCoverage[],
): RequirementVersionCoverage[] {
  const coverages = [...initialCoverages];
  if (newCoverages) {
    coverages.push(...newCoverages);
  }
  return coverages;
}

const initialCoverages: RequirementVersionCoverage[] = [
  {
    criticality: 'CRITICAL',
    directlyVerified: true,
    name: 'Requirement 1',
    projectName: 'Project 1',
    reference: '',
    requirementVersionId: 1,
    status: 'WORK_IN_PROGRESS',
    stepIndex: 0,
    unDirectlyVerified: false,
    verifiedBy: 'admin',
    verifyingCalledTestCaseIds: [12],
    verifyingTestCaseId: 3,
    coverageStepInfos: [{ id: 11, index: 0 }],
    milestoneLabels: '',
    milestoneMaxDate: '',
    milestoneMinDate: '',
  },
  {
    criticality: 'MAJOR',
    directlyVerified: true,
    name: 'Requirement 2',
    projectName: 'Project 1',
    reference: '',
    requirementVersionId: 2,
    status: 'APPROVED',
    stepIndex: 2,
    unDirectlyVerified: true,
    verifiedBy: 'admin',
    verifyingCalledTestCaseIds: [12],
    verifyingTestCaseId: 3,
    coverageStepInfos: [{ id: 11, index: 0 }],
    milestoneLabels: '',
    milestoneMaxDate: '',
    milestoneMinDate: '',
  },
];
