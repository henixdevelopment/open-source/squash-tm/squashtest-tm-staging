import { TestCaseViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { createEntityReferentialData } from '../../../../utils/referential/create-entity-referential.const';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { mockTestCaseModel } from '../../../../data-mock/test-case.data-mock';
import { TestCaseModel } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/test-case.model';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

describe('Test Case View - Called test cases', function () {
  it('should display table', () => {
    const page = navigateToTestCase();
    const calledTCTable = page.calledTestCaseTable;
    new NavBarElement().toggle();
    page.toggleTree();

    const firstRow = calledTCTable.getRow(6);
    firstRow.cell(GridColumnId.projectName).textRenderer().assertContainsText('Project 1');
    firstRow.cell(GridColumnId.reference).textRenderer().assertContainsText('Ref.006');
    firstRow.cell(GridColumnId.name).linkRenderer().assertContainText('Test Case 6');
    firstRow.cell(GridColumnId.datasetName).textRenderer().assertContainsText('Dataset 1');
    firstRow.cell(GridColumnId.stepOrder).textRenderer().assertContainsText('1');

    const secondRow = calledTCTable.getRow(7);
    secondRow.cell(GridColumnId.projectName).textRenderer().assertContainsText('Custom Project');
    secondRow.cell(GridColumnId.reference).textRenderer().assertContainsText('Ref.007');
    secondRow.cell(GridColumnId.name).linkRenderer().assertContainText('Test Case 7');
    secondRow.cell(GridColumnId.datasetName).textRenderer().assertContainsText('');
    secondRow.cell(GridColumnId.stepOrder).textRenderer().assertContainsText('2');
  });

  function navigateToTestCase(): TestCaseViewPage {
    const initialNodes = mockGridResponse('id', [
      mockTreeNode({
        id: 'TestCaseLibrary-1',
        children: ['TestCase-3'],
        data: { NAME: 'Project1', CHILD_COUNT: 1 },
        state: DataRowOpenState.open,
      }),
      mockTreeNode({
        id: 'TestCase-3',
        children: [],
        projectId: 1,
        data: {
          NAME: 'TestCase3',
          CHILD_COUNT: 0,
          TC_STATUS: 'APPROVED',
          TC_KIND: 'STANDARD',
          IMPORTANCE: 'LOW',
        },
        parentRowId: 'TestCaseLibrary-1',
      }),
    ]);
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(
      initialNodes,
      createEntityReferentialData,
    );

    const model: TestCaseModel = mockTestCaseModel({
      id: 3,
      projectId: 1,
      name: 'TestCase3',
      customFieldValues: [],
      attachmentList: {
        id: 1,
        attachments: [],
      },
      reference: '',
      description: '',
      uuid: '',
      type: 20,
      testSteps: [],
      status: 'WORK_IN_PROGRESS',
      prerequisite: '',
      parameters: [],
      nbIssues: 0,
      nature: 12,
      milestones: [],
      lastModifiedOn: new Date('2020-03-09 10:30').toISOString(),
      lastModifiedBy: 'admin',
      kind: 'STANDARD',
      importanceAuto: false,
      importance: 'LOW',
      executions: [],
      datasets: [],
      datasetParamValues: [],
      createdOn: new Date('2020-03-09 10:30').toISOString(),
      createdBy: 'admin',
      coverages: [],
      automationRequest: null,
      automatable: 'M',
      calledTestCases: [
        {
          id: 6,
          projectName: 'Project 1',
          datasetName: 'Dataset 1',
          name: 'Test Case 6',
          reference: 'Ref.006',
          stepOrder: 0,
        },
        {
          id: 7,
          projectName: 'Custom Project',
          datasetName: '',
          name: 'Test Case 7',
          reference: 'Ref.007',
          stepOrder: 1,
        },
      ],
      lastExecutionStatus: 'SUCCESS',
      script: '',
    });
    return testCaseWorkspacePage.tree.selectNode<TestCaseViewPage>('TestCase-3', model);
  }
});
