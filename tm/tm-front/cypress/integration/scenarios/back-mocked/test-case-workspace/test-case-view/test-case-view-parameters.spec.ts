import { TestCaseViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { createEntityReferentialData } from '../../../../utils/referential/create-entity-referential.const';
import { TestCaseViewParametersPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-parameters.page';
import { mockTestCaseModel } from '../../../../data-mock/test-case.data-mock';
import { TestCaseModel } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/test-case.model';
import { Parameter } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/parameter.model';
import { Dataset } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/dataset.model';
import { DatasetParamValue } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/dataset-param-value';
import { TestCaseParameterOperationReport } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/operation-reports.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import {
  DataRowModel,
  DataRowOpenState,
} from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { TreeElement } from '../../../../page-objects/elements/grid/grid.element';
import { DatasetDuplicationDialogElement } from '../../../../page-objects/pages/test-case-workspace/dialogs/dataset-duplication-dialog.element';
import {
  ALL_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder,
} from '../../../../utils/referential/referential-data-builder';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';
import { ReferentialDataModel } from '../../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';

describe('Test Case View - Parameters', function () {
  it('should display table', () => {
    const testCaseViewPage = navigateToTestCase();
    const table = testCaseViewPage.parametersTable;
    const firstRow = table.getRow(1, 'leftViewport');
    firstRow.cell('name').textRenderer().assertContainsText('Dataset');
  });

  it('should add parameter', () => {
    const testCaseViewPage = navigateToTestCase();
    const dialog = testCaseViewPage.openAddParameterDialog();

    dialog.fillName('Param_2');
    dialog.fillDescription('Description');

    const operation: TestCaseParameterOperationReport = {
      dataSets: [
        { id: 1, name: 'Dataset' },
        { id: 2, name: 'Dataset 2' },
      ],
      paramValues: [
        ...initializeDataSetParamValue(),
        { id: 7, parameterId: 4, datasetId: 1, value: '' },
        { id: 8, parameterId: 4, datasetId: 2, value: '' },
      ],
      parameters: [
        ...initializeParameters(),
        {
          id: 4,
          name: 'Param_2',
          description: 'Description',
          sourceTestCaseId: 3,
          sourceTestCaseName: '',
          sourceTestCaseProjectName: '',
          sourceTestCaseReference: '',
          order: 3,
        },
      ],
    };
    dialog.addParam(operation);
    const table = testCaseViewPage.parametersTable;
    const headerRow = table.getHeaderRow();
    headerRow.cell('PARAM_COLUMN_4').assertExists();
  });

  it('should add datasets', () => {
    const testCaseViewPage = navigateToTestCase();
    const dialog = testCaseViewPage.openAddDataSetDialog();
    dialog.checkDialogButtons();
    dialog.fillName('New dataset');
    dialog.fillParameter(1, 'p245');

    const operation: TestCaseParameterOperationReport = {
      dataSets: [
        { id: 1, name: 'Dataset' },
        { id: 2, name: 'Dataset 2' },
        { id: 3, name: 'New dataset' },
      ],
      paramValues: [
        { id: 1, parameterId: 1, datasetId: 1, value: '' },
        { id: 2, parameterId: 1, datasetId: 2, value: '' },
        { id: 3, parameterId: 1, datasetId: 3, value: 'p245' },
        { id: 4, parameterId: 2, datasetId: 1, value: '' },
        { id: 5, parameterId: 2, datasetId: 2, value: '' },
        { id: 6, parameterId: 2, datasetId: 3, value: '' },
        { id: 7, parameterId: 3, datasetId: 1, value: '' },
        { id: 8, parameterId: 3, datasetId: 2, value: '' },
        { id: 9, parameterId: 3, datasetId: 3, value: '' },
      ],
      parameters: [...initializeParameters()],
    };
    dialog.addDataSet(operation);

    const table = testCaseViewPage.parametersTable;
    table.assertRowCount(3);
    const newRow = table.getRow('3', 'leftViewport');
    newRow.cell('name').textRenderer().assertContainsText('New dataset');
  });

  it('should duplicate a dataset', () => {
    const refData = createEntityReferentialData;
    refData.premiumPluginInstalled = true;

    const testCaseViewPage = navigateToTestCase(refData);
    const grid = testCaseViewPage.parametersTable;
    grid.assertExists();
    grid.assertRowCount(2);
    testCaseViewPage.openDuplicateDataSetMenu(2);
    const operation: TestCaseParameterOperationReport = {
      dataSets: [
        { id: 1, name: 'Dataset' },
        { id: 2, name: 'Dataset 2' },
        { id: 3, name: 'Dataset-Copy1' },
      ],
      paramValues: [
        { id: 1, parameterId: 1, datasetId: 1, value: 'a' },
        { id: 3, parameterId: 2, datasetId: 1, value: 'b' },
        { id: 5, parameterId: 3, datasetId: 1, value: 'c' },
        { id: 1, parameterId: 1, datasetId: 2, value: '' },
        { id: 3, parameterId: 2, datasetId: 2, value: '' },
        { id: 5, parameterId: 3, datasetId: 2, value: '' },
        { id: 7, parameterId: 1, datasetId: 3, value: 'a' },
        { id: 9, parameterId: 2, datasetId: 3, value: 'b' },
        { id: 11, parameterId: 3, datasetId: 3, value: 'c' },
      ],
      parameters: [...initializeParameters()],
    };

    testCaseViewPage.mockDatasetDuplicationRequest('duplicate-dataset', operation);
    grid.assertRowCount(3);
  });

  it('should open dataset duplication dialog and check conformity', () => {
    const referentialDataMockBuilder = new ReferentialDataMockBuilder();
    referentialDataMockBuilder.withProjects(
      {
        name: 'Project1',
        permissions: ALL_PROJECT_PERMISSIONS,
      },
      {
        name: 'Project2',
        permissions: ALL_PROJECT_PERMISSIONS,
      },
    );

    const refData = referentialDataMockBuilder.build();
    refData.premiumPluginInstalled = true;

    const initialDataRowMock: DataRowModel[] = [
      mockTreeNode({
        id: 'TestCaseLibrary-2',
        children: [],
        data: { NAME: 'Project2' },
      }),
    ];

    const testCaseViewPage = navigateToTestCase(refData, initialDataRowMock);
    testCaseViewPage.openDuplicateDataSetMenu(1);

    const duplicationTreeGridResponse: GridResponse = initializeDuplicationDialogTreeResponse();

    const tree = TreeElement.createTreeElement(
      'test-case-tree-dataset-duplication-picker',
      'test-case-tree/dataset-duplication',
      duplicationTreeGridResponse,
    );

    const pickerDialog: DatasetDuplicationDialogElement =
      testCaseViewPage.openDuplicateDataSetDialogAndInitializeTree(1, tree);

    pickerDialog.assertExists();
    pickerDialog.eligibleTestCaseTree.assertRowCount(2);
    pickerDialog.checkTreeContent(duplicationTreeGridResponse.dataRows);
  });

  it('should remove dataset', () => {
    const testCaseViewPage = navigateToTestCase();
    const dialog = testCaseViewPage.openDeleteDataSetDialog('2');
    const operation: TestCaseParameterOperationReport = {
      dataSets: [{ id: 1, name: 'Dataset' }],
      paramValues: [
        { id: 1, parameterId: 1, datasetId: 1, value: '' },
        { id: 3, parameterId: 2, datasetId: 1, value: '' },
        { id: 5, parameterId: 3, datasetId: 1, value: '' },
      ],
      parameters: [...initializeParameters()],
    };
    dialog.deleteForSuccess(operation);

    const table = testCaseViewPage.parametersTable;
    table.assertRowCount(1);
  });

  it('should remove a parameter', () => {
    const testCaseViewPage = navigateToTestCase();
    testCaseViewPage.openDeleteParamDialog('PARAM_COLUMN_1', { PARAMETER_USED: false });

    const removeDialog = testCaseViewPage.paramRemoveDialog;

    const operation: TestCaseParameterOperationReport = {
      dataSets: [
        { id: 1, name: 'Dataset' },
        { id: 2, name: 'Dataset 2' },
      ],
      paramValues: [
        { id: 3, parameterId: 2, datasetId: 1, value: '' },
        { id: 4, parameterId: 2, datasetId: 2, value: '' },
        { id: 5, parameterId: 3, datasetId: 1, value: '' },
        { id: 6, parameterId: 3, datasetId: 2, value: '' },
      ],
      parameters: [
        {
          id: 2,
          name: 'param2',
          description: '',
          sourceTestCaseId: 5,
          sourceTestCaseName: 'TestCase_5',
          sourceTestCaseProjectName: 'Project 42',
          sourceTestCaseReference: '',
          order: 0,
        },
        {
          id: 3,
          name: 'param3',
          description: '',
          sourceTestCaseId: 6,
          sourceTestCaseName: 'TestCase_6',
          sourceTestCaseProjectName: 'Project 42',
          sourceTestCaseReference: '006',
          order: 1,
        },
      ],
    };

    removeDialog.deleteForSuccess(operation);

    const table = testCaseViewPage.parametersTable;
    table.assertRowCount(2);
    table.assertColumnCount(5);
  });

  it('should update paramValue', () => {
    const testCaseViewPage = navigateToTestCase();
    const table = testCaseViewPage.parametersTable;
    const row = table.getRow(1);
    const cell = row.cell('PARAM_COLUMN_1');
    cell.textRenderer().editText('paramValue', 'dataset-param-values');
    cell.assertExists();
    cell.textRenderer().assertContainsText('paramValue');
  });

  it('should update parameter name', () => {
    const testCaseViewPage = navigateToTestCase();
    const table = testCaseViewPage.parametersTable;
    const headerRow = table.getHeaderRow();
    const cell = headerRow.cell('PARAM_COLUMN_1');
    cell.textRenderer().editTextResponse('ParamDeTest', 'parameters/*/rename', {
      testStepList: [],
      prerequisite: '',
    });
    cell.assertExists();
    cell.textRenderer().assertContainsText('ParamDeTest');
  });

  it('should rename parameter by dialog', () => {
    const testCaseViewPage = navigateToTestCase();
    const table = testCaseViewPage.parametersTable;
    const informationDialog = testCaseViewPage.openInformationParamDialog('PARAM_COLUMN_1');
    const nameField = informationDialog.getNameField('parameters/*/rename');
    nameField.checkContent('param1');
    nameField.setAndConfirmValue('param_1', { testStepList: [], prerequisite: '' });
    nameField.checkContent('param_1');
    informationDialog.closeDialog();
    const headerRow = table.getHeaderRow();
    headerRow.cell('PARAM_COLUMN_1').textRenderer().assertContainsText('param_1');
  });

  it('should change description of parameter', () => {
    const testCaseViewPage = navigateToTestCase();
    const informationDialog = testCaseViewPage.openInformationParamDialog('PARAM_COLUMN_1');
    const descriptionField = informationDialog.getDescriptionField('parameters/*/description');
    descriptionField.setAndConfirmValue('description');
    descriptionField.checkTextContent('description');
    informationDialog.closeDialog();
  });

  it('should display test case source link', () => {
    const testCaseViewPage = navigateToTestCase();
    const informationDialog = testCaseViewPage.openInformationParamDialog('PARAM_COLUMN_2');
    informationDialog.testCaseSourceLink.containText('TestCase_5 (Project 42)');
    informationDialog.closeDialog();
  });

  function navigateToTestCase(
    referentialDataMock: ReferentialDataModel = createEntityReferentialData,
    additionalDataRowMock: DataRowModel[] = [],
  ): TestCaseViewParametersPage {
    const initialNodes = mockGridResponse('id', [
      mockTreeNode({
        id: 'TestCaseLibrary-1',
        children: [],
        data: { NAME: 'Project1' },
      }),
      mockTreeNode({
        id: 'TestCaseLibrary-2',
        children: [],
        data: { NAME: 'Project2' },
      }),
    ]);

    initialNodes.dataRows.push(...additionalDataRowMock);
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(
      initialNodes,
      referentialDataMock,
    );
    const libraryRefreshAtOpen = [
      mockTreeNode({
        id: 'TestCaseLibrary-1',
        children: ['TestCase-3'],
        data: { NAME: 'Project1', CHILD_COUNT: 1 },
        state: DataRowOpenState.open,
      }),
      mockTreeNode({
        id: 'TestCase-3',
        children: [],
        projectId: 1,
        data: {
          NAME: 'TestCase3',
          CHILD_COUNT: 0,
          TC_STATUS: 'APPROVED',
          TC_KIND: 'STANDARD',
          IMPORTANCE: 'LOW',
        },
        parentRowId: 'TestCaseLibrary-1',
      }),
    ];

    testCaseWorkspacePage.tree.openNode('TestCaseLibrary-1', libraryRefreshAtOpen);
    const model: TestCaseModel = mockTestCaseModel({
      id: 3,
      projectId: 1,
      name: 'TestCase3',
      customFieldValues: [],
      attachmentList: {
        id: 1,
        attachments: [],
      },
      reference: '',
      description: '',
      uuid: '',
      type: 20,
      testSteps: [],
      status: 'WORK_IN_PROGRESS',
      prerequisite: '',
      parameters: initializeParameters(),
      nbIssues: 0,
      nature: 12,
      milestones: [],
      lastModifiedOn: new Date('2020-03-09 10:30').toISOString(),
      lastModifiedBy: 'admin',
      kind: 'STANDARD',
      importanceAuto: false,
      importance: 'LOW',
      executions: [],
      datasets: initializeDataSets(),
      datasetParamValues: initializeDataSetParamValue(),
      createdOn: new Date('2020-03-09 10:30').toISOString(),
      createdBy: 'admin',
      coverages: [],
      automationRequest: null,
      automatable: 'M',
      calledTestCases: [],
      lastExecutionStatus: 'SUCCESS',
      script: '',
    });
    const testCaseViewPage = testCaseWorkspacePage.tree.selectNode<TestCaseViewPage>(
      'TestCase-3',
      model,
    );
    return testCaseViewPage.clickParametersAnchorLink();
  }

  function initializeParameters(): Parameter[] {
    return [
      {
        id: 1,
        name: 'param1',
        description: '',
        sourceTestCaseId: 3,
        sourceTestCaseName: '',
        sourceTestCaseProjectName: '',
        sourceTestCaseReference: '',
        order: 0,
      },
      {
        id: 2,
        name: 'param2',
        description: '',
        sourceTestCaseId: 5,
        sourceTestCaseName: 'TestCase_5',
        sourceTestCaseProjectName: 'Project 42',
        sourceTestCaseReference: '',
        order: 1,
      },
      {
        id: 3,
        name: 'param3',
        description: '',
        sourceTestCaseId: 6,
        sourceTestCaseName: 'TestCase_6',
        sourceTestCaseProjectName: 'Project 42',
        sourceTestCaseReference: '006',
        order: 2,
      },
    ];
  }

  function initializeDataSets(): Dataset[] {
    return [
      { id: 1, name: 'Dataset' },
      { id: 2, name: 'Dataset 2' },
    ];
  }

  function initializeDataSetParamValue(): DatasetParamValue[] {
    return [
      { id: 1, parameterId: 1, datasetId: 1, value: '' },
      { id: 2, parameterId: 1, datasetId: 2, value: '' },
      { id: 3, parameterId: 2, datasetId: 1, value: '' },
      { id: 4, parameterId: 2, datasetId: 2, value: '' },
      { id: 5, parameterId: 3, datasetId: 1, value: '' },
      { id: 6, parameterId: 3, datasetId: 2, value: '' },
    ];
  }

  function initializeDuplicationDialogTreeResponse(): GridResponse {
    const tcln1 = mockTreeNode({
      id: 'TestCaseLibrary-1',
      projectId: 1,
      children: [],
      state: DataRowOpenState.closed,
      data: {
        TCL_ID: 1,
        projectId: 1,
        NAME: 'Project1',
        CHILD_COUNT: '1',
        BOUND_TO_BLOCKING_MILESTONE: false,
      },
    });

    const tcln2 = mockTreeNode({
      id: 'TestCaseLibrary-2',
      projectId: 2,
      children: [],
      state: DataRowOpenState.closed,
      data: {
        TCL_ID: 2,
        projectId: 2,
        NAME: 'Project2',
        CHILD_COUNT: '0',
        BOUND_TO_BLOCKING_MILESTONE: false,
      },
    });

    return mockGridResponse('id', [tcln1, tcln2]);
  }
});
