import { TestCaseViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { createEntityReferentialData } from '../../../../utils/referential/create-entity-referential.const';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { DetailedStepViewPage } from '../../../../page-objects/pages/test-case-workspace/detailed-step-view/detailed-step-view.page';
import { defaultReferentialData } from '../../../../utils/referential/default-referential-data.const';
import { CreateActionStepFormElement } from '../../../../page-objects/elements/test-steps/create-action-step-form.element';
import { mockTestCaseModel } from '../../../../data-mock/test-case.data-mock';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { RequirementVersionModel } from '../../../../../../projects/sqtm-core/src/lib/model/requirement/requirement-version.model';
import { mockRequirementVersionModel } from '../../../../data-mock/requirements.data-mock';
import { ChangeCoverageOperationReport } from '../../../../../../projects/sqtm-core/src/lib/model/change-coverage-operation-report';
import { RequirementVersionCoverage } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/requirement-version-coverage-model';
import { CustomFieldValueModel } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/custom-field-value.model';
import {
  ActionStepModel,
  CallStepModel,
  TestStepModel,
} from '../../../../../../projects/sqtm-core/src/lib/model/test-case/test-step.model';
import { TestCaseModel } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/test-case.model';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

const requirementVersionDetails: RequirementVersionModel = mockRequirementVersionModel({
  versionNumber: 2,
  status: 'WORK_IN_PROGRESS',
  category: 3,
  criticality: 'CRITICAL',
  description: `<ul><li>Bring O2</li><li>Bring Water</li><li>Bring Food</li></ul>`,
});

function addOneNewRequirement(page: DetailedStepViewPage) {
  const requirementResponse = mockGridResponse('id', [
    mockTreeNode({
      id: 'RequirementLibrary-1',
      children: ['Requirement-2', 'Requirement-3'],
      data: { NAME: 'Project1' },
    }),
    mockTreeNode({
      id: 'Requirement-4',
      children: [],
      data: {
        NAME: 'Correct ISS Orbit',
        CRITICALITY: 'MAJOR',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        HAS_DESCRIPTION: true,
        REQ_CATEGORY_ICON: 'briefcase',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
        REQ_CATEGORY_TYPE: 'SYS',
        COVERAGE_COUNT: 0,
        IS_SYNCHRONIZED: false,
      },
    }),
    mockTreeNode({
      id: 'Requirement-5',
      children: [],
      data: {
        NAME: 'Requirement5',
        CRITICALITY: 'MAJOR',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        HAS_DESCRIPTION: true,
        REQ_CATEGORY_ICON: 'briefcase',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
        REQ_CATEGORY_TYPE: 'SYS',
        COVERAGE_COUNT: 0,
        IS_SYNCHRONIZED: false,
      },
    }),
  ]);
  const requirementDrawer = page.showAddRequirementCoverageDrawer(requirementResponse);
  requirementDrawer.openNode('RequirementLibrary-1');
  requirementDrawer.beginDragAndDrop('Requirement-4');
  page.enterIntoRequirementDropZone();
  const coverageResponse = [...initialCoverages];
  coverageResponse.push(newRequirement);
  const operationReport: ChangeCoverageOperationReport = {
    coverages: coverageResponse,
    summary: {
      notLinkableRejections: false,
      noVerifiableVersionRejections: false,
      alreadyVerifiedRejections: false,
    },
  };
  page.dropRequirement(3, 4, operationReport);
  page.closeRequirementDrawer();
}

describe('Test Step Detailed View', function () {
  it('should navigate to step details', () => {
    const page = navigateToTestCase();
    new NavBarElement().toggle();
    page.toggleTree();
    const testStepPage = page.clickStepsAnchorLink();
    const step4 = testStepPage.getActionStepByIndex(3);
    const detailedStepViewPage = step4.navigateToStepDetails(createTestCaseModelWithSteps());
    detailedStepViewPage.assertExists();
    detailedStepViewPage.checkTestCaseName('STS 135 - Launch Atlantis');
    detailedStepViewPage.returnToPreviousPage();
    page.assertExists();
  });

  it('should show requirement table', () => {
    const page = DetailedStepViewPage.initTestAtPage(
      1,
      createTestCaseModelWithSteps(),
      defaultReferentialData,
    );
    page.assertExists();
    page.checkTestCaseName('STS 135 - Launch Atlantis');
    const coverageTable = page.coverageTable;
    coverageTable.assertExists();
    coverageTable
      .getHeaderRow()
      .cell(GridColumnId.linkedToCurrentStep)
      .textRenderer()
      .assertContainsText('Liée au PT');
    coverageTable
      .getHeaderRow()
      .cell(GridColumnId.projectName)
      .textRenderer()
      .assertContainsText('Projet');
    coverageTable
      .getHeaderRow()
      .cell(GridColumnId.reference)
      .textRenderer()
      .assertContainsText('Référence');
    coverageTable
      .getHeaderRow()
      .cell(GridColumnId.name)
      .textRenderer()
      .assertContainsText('Exigence');
    const firstCoverage = coverageTable.getRow(1);
    firstCoverage.assertExists();
    firstCoverage.cell(GridColumnId.linkedToCurrentStep).checkBoxRender().assertIsCheck();
    firstCoverage.cell(GridColumnId.name).textRenderer().assertContainsText('Supplying ISS');
    firstCoverage.cell(GridColumnId.reference).textRenderer().assertContainsText('L4');
    firstCoverage.cell(GridColumnId.projectName).textRenderer().assertContainsText('ISS');

    const secondCoverage = coverageTable.getRow(2);
    secondCoverage.assertExists();
    secondCoverage.cell(GridColumnId.linkedToCurrentStep).checkBoxRender().assertIsNotCheck();
    secondCoverage.cell(GridColumnId.name).textRenderer().assertContainsText('Last shuttle fly');
    secondCoverage.cell(GridColumnId.projectName).textRenderer().assertContainsText('STS');
  });

  it('should update status from capsule', () => {
    const tcPage = navigateToTestCase();
    const capsule = tcPage.statusCapsule;
    const infoPanel = tcPage.informationPanel;
    capsule.assertIsClickable();
    const updateMock = new HttpMockBuilder('test-case/*/status').post().build();

    const refreshMock = infoPanel.getRefreshTreeMock(null);

    capsule.selectOption('À approuver');
    updateMock.wait();
    refreshMock.wait();

    capsule.assertContainsText('À approuver');
    infoPanel.statusSelectField.assertContainsText('À approuver');
  });

  it('should update importance from capsule', () => {
    const tcPage = navigateToTestCase();
    const capsule = tcPage.importanceCapsule;
    const infoPanel = tcPage.informationPanel;
    capsule.assertIsClickable();
    const refreshMock = infoPanel.getRefreshTreeMock(null);
    infoPanel.importanceSelectField.setAndConfirmValueNoButton('Moyenne');
    refreshMock.wait();
    capsule.assertContainsText('Moyenne');

    const updateMock = new HttpMockBuilder('test-case/*/importance').post().build();
    capsule.selectOption('Haute');
    updateMock.wait();
    refreshMock.wait();
    capsule.assertContainsText('Haute');
    infoPanel.importanceSelectField.assertContainsText('Haute');
    infoPanel.importanceAutoCheckBox.toggleState('{"importance":"HIGH"}');
    capsule.assertIsNotClickable();
  });

  it('should show requirement details', () => {
    const page = DetailedStepViewPage.initTestAtPage(
      1,
      createTestCaseModelWithSteps(),
      defaultReferentialData,
    );
    page.assertExists();
    page.checkTestCaseName('STS 135 - Launch Atlantis');
    const coverageTable = page.coverageTable;
    const detailsPanel = coverageTable.showRequirementVersionDetails(1, requirementVersionDetails);
    detailsPanel.assertExists();
    detailsPanel.checkField('Numéro de version', '2');
    detailsPanel.checkField('Statut', 'En cours de rédaction');
    detailsPanel.checkField('Criticité', 'Critique');
    detailsPanel.checkField('Catégorie', "Cas d'utilisation");
    detailsPanel.checkDescription(requirementVersionDetails.description);
  });

  it('should add requirement coverage', () => {
    const page = DetailedStepViewPage.initTestAtPage(
      1,
      createTestCaseModelWithSteps(),
      defaultReferentialData,
    );
    page.assertExists();
    page.checkTestCaseName('STS 135 - Launch Atlantis');
    const coverageTable = page.coverageTable;
    const detailsPanel = coverageTable.showRequirementVersionDetails(1, requirementVersionDetails);
    detailsPanel.assertExists();
    addOneNewRequirement(page);
    const newCoverage = coverageTable.getRow(4);
    newCoverage.assertExists();
    newCoverage.cell(GridColumnId.linkedToCurrentStep).checkBoxRender().assertIsCheck();
    newCoverage.cell(GridColumnId.name).textRenderer().assertContainsText('Correct ISS Orbit');
    newCoverage.cell(GridColumnId.reference).textRenderer().assertContainsText('C41');
    newCoverage.cell(GridColumnId.projectName).textRenderer().assertContainsText('ISS');
    // checking that previously opened panel is always opened (Had a bug with that...)
    detailsPanel.assertExists();
  });

  it('should navigate to requirement search for coverage', () => {
    const page = DetailedStepViewPage.initTestAtPage(
      1,
      createTestCaseModelWithSteps(),
      defaultReferentialData,
    );
    page.assertExists();
    page.checkTestCaseName('STS 135 - Launch Atlantis');
    page.navigateToRequirementSearchForCoverage();
  });

  it('should delete requirement', () => {
    const page = DetailedStepViewPage.initTestAtPage(
      1,
      createTestCaseModelWithSteps(),
      defaultReferentialData,
    );
    page.assertExists();
    page.checkTestCaseName('STS 135 - Launch Atlantis');
    const coverageTable = page.coverageTable;
    page.assertRequirementVersionIsCoveredByTestCase(1);
    page.assertRequirementVersionIsCoveredByTestCase(2);
    coverageTable.deleteRequirementVersion(2, 3, [initialCoverages[0]]);
    page.assertRequirementVersionIsCoveredByTestCase(1);
    page.assertRequirementVersionIsNotCoveredByTestCase(2);
  });

  it('should delete multiple requirement from test case', () => {
    const page = DetailedStepViewPage.initTestAtPage(
      1,
      createTestCaseModelWithSteps(),
      defaultReferentialData,
    );
    page.assertExists();
    addOneNewRequirement(page);
    page.deleteSelectedCoverageFromTestCase([1, 2], { coverages: [newRequirement] });
    page.assertRequirementVersionIsNotCoveredByTestCase(1);
    page.assertRequirementVersionIsNotCoveredByTestCase(2);
    page.assertRequirementVersionIsCoveredByTestCase(4);
  });

  it('should delete multiple requirement from test step', () => {
    const page = DetailedStepViewPage.initTestAtPage(
      1,
      createTestCaseModelWithSteps(),
      defaultReferentialData,
    );
    page.assertExists();
    addOneNewRequirement(page);
    const coverageResponse = [...initialCoverages];
    const modifiedCoveragesResponse = coverageResponse.map((cov) => ({
      ...cov,
      coverageStepInfos: [],
    }));
    modifiedCoveragesResponse.push(newRequirement);
    page.assertRequirementVersionIsCoveredByCurrentStep(1);
    page.assertRequirementVersionIsCoveredByCurrentStep(4);
    page.deleteSelectedCoverageFromTestStep([1, 2], '4', { coverages: modifiedCoveragesResponse });
    page.assertRequirementVersionIsNotCoveredByCurrentStep(1);
    page.assertRequirementVersionIsCoveredByCurrentStep(4);
  });

  it('should link and unlink one requirement from test step', () => {
    const page = DetailedStepViewPage.initTestAtPage(
      1,
      createTestCaseModelWithSteps(),
      defaultReferentialData,
    );
    page.assertExists();
    page.checkTestCaseName('STS 135 - Launch Atlantis');
    addOneNewRequirement(page);
    const coverageResponse = [...initialCoverages];
    const modifiedCoveragesResponse = coverageResponse.map((cov) => ({
      ...cov,
      coverageStepInfos: [],
    }));
    modifiedCoveragesResponse.push(newRequirement);
    page.assertRequirementVersionIsCoveredByCurrentStep(1);
    page.assertRequirementVersionIsCoveredByCurrentStep(4);
    page.deleteOneCoverageFromTestStep(1, '4', modifiedCoveragesResponse);
    page.assertRequirementVersionIsNotCoveredByCurrentStep(1);
    page.assertRequirementVersionIsCoveredByCurrentStep(4);
    page.linkRequirementVersionToCurrentStep(1, '4');
    page.assertRequirementVersionIsCoveredByCurrentStep(1);
    page.assertRequirementVersionIsCoveredByCurrentStep(4);
  });

  it('should navigate through test steps and show data', () => {
    const page = DetailedStepViewPage.initTestAtPage(
      0,
      createTestCaseModelWithSteps(),
      defaultReferentialData,
    );
    page.assertExists();
    page.stepCounter.assertBackwardButtonIsDisabled();
    page.stepCounter.assertForwardButtonIsActive();
    page.assertActionContains('delegate to shuttle navigation computer');
    page.assertResultContains('Atlantys navigation computer is controlling');
    page.assertRequirementVersionIsNotCoveredByCurrentStep(1);
    page.assertRequirementVersionIsCoveredByCurrentStep(2);
    page.stepCounter.navigateForward();
    page.stepCounter.assertBackwardButtonIsActive();
    page.stepCounter.assertForwardButtonIsActive();
    page.assertActionContains('check main engines glimbal');
    page.assertResultContains('main engines ok to handle corrections');
    page.getTextCustomField('fieldA').checkContent('cuf value');
    page.assertRequirementVersionIsCoveredByCurrentStep(1);
    page.stepCounter.navigateForward();
    page.stepCounter.assertBackwardButtonIsActive();
    page.stepCounter.assertForwardButtonIsActive();
    page.assertActionContains('solid rocket booster ignition');
    page.assertResultContains('big petards are running');
    page.getTextCustomField('fieldA').checkContent('another value');
    page.stepCounter.navigateForward();
    page.stepCounter.assertBackwardButtonIsActive();
    page.stepCounter.assertForwardButtonIsActive();
    page.assertActionContains('release fixation arms');
    page.assertResultContains(
      '... and we have lift off. Space shuttle Atlantis is returning to the ISS',
    );
    page.stepCounter.navigateForward();
    page.stepCounter.assertForwardButtonIsDisabled();
    page.stepCounter.assertBackwardButtonIsActive();
  });

  it('should edit step data', () => {
    const page = DetailedStepViewPage.initTestAtPage(
      1,
      createTestCaseModelWithSteps(),
      defaultReferentialData,
    );
    page.assertExists();
    page.assertActionContains('check main engines glimbal');
    page.assertResultContains('main engines ok to handle corrections');
    page.getTextCustomField('fieldA').checkContent('cuf value');
    page.changeAction('perform main engines glimbal check', 4);
    page.assertActionContains('perform main engines glimbal check');
    page.changeResult('main engines ok to handle path corrections', 4);
    page.assertResultContains('main engines ok to handle path corrections');
    page.getTextCustomField('fieldA').setAndConfirmValue('cuf value modified');
    page.getTextCustomField('fieldA').checkContent('cuf value modified');

    page.addAttachments(
      [
        new File(['last flight'], 'STS_135.txt'),
        new File(['last flight'], 'STS_132.txt'),
        new File(['last flight'], 'STS_134.txt'),
      ],
      4,
      [
        {
          status: '',
          iStatus: 0,
          attachmentDto: {
            name: 'STS_135.txt',
            size: 123456,
            addedOn: new Date('25 Mar 2020 10:12:57'),
            lastModifiedOn: null,
            id: 12,
          },
        },
        {
          status: '',
          iStatus: 0,
          attachmentDto: {
            name: 'STS_132.txt',
            size: 123456,
            addedOn: new Date('25 Mar 2020 10:12:57'),
            lastModifiedOn: null,
            id: 13,
          },
        },
        {
          status: '',
          iStatus: 0,
          attachmentDto: {
            name: 'STS_134.txt',
            size: 123456,
            addedOn: new Date('25 Mar 2020 10:12:57'),
            lastModifiedOn: null,
            id: 14,
          },
        },
      ],
    );

    page.attachmentCompactList.checkAttachmentData(
      0,
      'STS_135.txt',
      '120,56 Ko',
      'Ajouté le 25/03/2020 10:12',
    );
    page.attachmentCompactList.checkAttachmentData(
      1,
      'STS_132.txt',
      '120,56 Ko',
      'Ajouté le 25/03/2020 10:12',
    );
    page.attachmentCompactList.checkAttachmentData(
      2,
      'STS_134.txt',
      '120,56 Ko',
      'Ajouté le 25/03/2020 10:12',
    );
  });

  it('should cancel test step creation', () => {
    const page = DetailedStepViewPage.initTestAtPage(
      2,
      createTestCaseModelWithSteps(),
      defaultReferentialData,
    );
    page.assertExists();
    page.stepCounter.checkExecutionStepper(3, 5);

    const createActionStepForm: CreateActionStepFormElement = page.showCreateActionStepForm();
    page.assertAddStepButtonNotVisible();
    page.stepCounter.checkExecutionStepper(4, 6);
    page.stepCounter.assertBackwardButtonIsDisabled();
    page.stepCounter.assertForwardButtonIsDisabled();

    createActionStepForm.assertFormIsReady();
    createActionStepForm.fillAction('activate water cooling system');
    createActionStepForm.fillExpectedResult('launch pad is flooded');

    createActionStepForm.cancelAddStep();
    page.stepCounter.checkExecutionStepper(3, 5);
    page.stepCounter.assertBackwardButtonIsActive();
    page.stepCounter.assertForwardButtonIsActive();
  });

  it('should create test step', () => {
    const page = DetailedStepViewPage.initTestAtPage(
      2,
      createTestCaseModelWithSteps(),
      defaultReferentialData,
    );
    page.assertExists();
    page.stepCounter.checkExecutionStepper(3, 5);

    const createActionStepForm: CreateActionStepFormElement = page.showCreateActionStepForm();
    page.stepCounter.checkExecutionStepper(4, 6);
    page.stepCounter.assertBackwardButtonIsDisabled();
    page.stepCounter.assertForwardButtonIsDisabled();

    createActionStepForm.assertFormIsReady();
    createActionStepForm.fillAction('activate water cooling system');
    createActionStepForm.fillExpectedResult('launch pad is flooded');

    const response = {
      testStep: {
        id: 6,
        stepOrder: 3,
        testCaseId: 3,
        action: '<p>activate water cooling system</p>',
        expectedResult: '<p>launch pad is flooded</p>',
        verifiedRequirements: null,
        customFieldValues: [...getTestStepsCustomFieldValues()],
        attachmentList: { id: 6, attachments: [] },
        kind: 'action-step',
        attachmentListId: 6,
      },
      operationReport: { parameters: [], dataSets: [], paramValues: [] },
    };
    createActionStepForm.confirmAddStep(3, response);
    page.stepCounter.checkExecutionStepper(4, 6);
    page.stepCounter.assertBackwardButtonIsActive();
    page.stepCounter.assertForwardButtonIsActive();
    page.assertActionContains('activate water cooling system');
    page.assertResultContains('launch pad is flooded');
    page.getTextCustomField('fieldA').checkContent('cuf value');
  });

  it('should delete test steps', () => {
    const page = DetailedStepViewPage.initTestAtPage(
      2,
      createTestCaseModelWithSteps(),
      defaultReferentialData,
    );
    page.assertExists();
    page.stepCounter.checkExecutionStepper(3, 5);
    page.deleteCurrentStep(3, 1);
    page.stepCounter.checkExecutionStepper(2, 4);
    page.assertActionContains('check main engines glimbal');
    page.assertResultContains('main engines ok to handle corrections');
    page.deleteCurrentStep(3, 4);
    page.stepCounter.checkExecutionStepper(1, 3);
    page.assertActionContains('delegate to shuttle navigation computer');
    page.assertResultContains('Atlantys navigation computer is controlling');
    page.deleteCurrentStep(3, 2);
    page.stepCounter.checkExecutionStepper(1, 2);
    page.deleteCurrentStep(3, 3);
    page.stepCounter.checkExecutionStepper(1, 1);
    page.deleteCurrentStep(3, 5);
    page.stepCounter.checkExecutionStepper(1, 1);
    const createActionStepFormElement = new CreateActionStepFormElement();
    createActionStepFormElement.assertFormIsReady();
    createActionStepFormElement.assertCancelAddStepButtonDisabled();
  });
});

const initialCoverages: RequirementVersionCoverage[] = [
  {
    criticality: 'CRITICAL',
    directlyVerified: true,
    name: 'Supplying ISS',
    projectName: 'ISS',
    reference: 'L4',
    requirementVersionId: 1,
    status: 'WORK_IN_PROGRESS',
    stepIndex: 0,
    unDirectlyVerified: false,
    verifiedBy: 'admin',
    verifyingCalledTestCaseIds: [12],
    coverageStepInfos: [{ id: 4, index: 1 }],
    verifyingTestCaseId: 3,
    milestoneMinDate: '',
    milestoneMaxDate: '',
    milestoneLabels: '',
  },
  {
    criticality: 'MAJOR',
    directlyVerified: true,
    name: 'Last shuttle fly',
    projectName: 'STS',
    reference: '',
    requirementVersionId: 2,
    status: 'APPROVED',
    stepIndex: 2,
    unDirectlyVerified: true,
    verifiedBy: 'admin',
    verifyingCalledTestCaseIds: [12],
    coverageStepInfos: [{ id: 2, index: 2 }],
    verifyingTestCaseId: 3,
    milestoneMinDate: '',
    milestoneMaxDate: '',
    milestoneLabels: '',
  },
];

const newRequirement: RequirementVersionCoverage = {
  criticality: 'CRITICAL',
  directlyVerified: true,
  name: 'Correct ISS Orbit',
  projectName: 'ISS',
  reference: 'C41',
  requirementVersionId: 4,
  status: 'WORK_IN_PROGRESS',
  stepIndex: 0,
  unDirectlyVerified: false,
  verifiedBy: 'admin',
  verifyingCalledTestCaseIds: [],
  verifyingTestCaseId: 3,
  coverageStepInfos: [{ id: 4, index: 0 }],
  milestoneMinDate: '',
  milestoneMaxDate: '',
  milestoneLabels: '',
};

function initTestCaseWorkspacePage(): TestCaseWorkspacePage {
  const initialNodes = mockGridResponse('id', [
    mockTreeNode({
      id: 'TestCaseLibrary-1',
      children: ['TestCase-3', 'TestCase-4'],
      data: { NAME: 'Project1', CHILD_COUNT: 2 },
      state: DataRowOpenState.open,
    }),
    mockTreeNode({
      id: 'TestCase-3',
      children: [],
      projectId: 1,
      parentRowId: 'TestCaseLibrary-1',
      state: DataRowOpenState.leaf,
      data: {
        NAME: 'Launch Atlantis',
        CHILD_COUNT: 0,
        TC_STATUS: 'APPROVED',
        TC_KIND: 'STANDARD',
        IMPORTANCE: 'LOW',
      },
    }),
    mockTreeNode({
      id: 'TestCase-4',
      children: [],
      projectId: 1,
      parentRowId: 'TestCaseLibrary-1',
      state: DataRowOpenState.leaf,
      data: {
        NAME: 'Launch Atlantis 2',
        CHILD_COUNT: 0,
        TC_STATUS: 'APPROVED',
        TC_KIND: 'STANDARD',
        IMPORTANCE: 'LOW',
      },
    }),
  ]);
  return TestCaseWorkspacePage.initTestAtPage(initialNodes, createEntityReferentialData);
}

function getTestStepsCustomFieldValues(cufAValue = 'cuf value'): CustomFieldValueModel[] {
  return [
    { cufId: 0, fieldType: 'CF', id: 1, value: cufAValue },
    { cufId: 1, fieldType: 'NUM', id: 2, value: '3' },
    { cufId: 2, fieldType: 'TAG', id: 3, value: 'o1|o2' },
    { cufId: 3, fieldType: 'CF', id: 4, value: 'cuf value' },
    { cufId: 4, fieldType: 'RTF', id: 5, value: '<p>a little rich value</p>' },
    { cufId: 5, fieldType: 'CF', id: 6, value: '2020-02-14' },
    { cufId: 6, fieldType: 'CF', id: 7, value: 'true' },
    { cufId: 7, fieldType: 'CF', id: 8, value: 'Option A' },
  ];
}

function createTestCaseModelWithSteps() {
  const actionStep1: ActionStepModel = {
    id: 2,
    projectId: 1,
    stepOrder: 0,
    customFieldValues: getTestStepsCustomFieldValues(),
    attachmentList: { id: 2, attachments: [] },
    kind: 'action-step',
    action: 'delegate to shuttle navigation computer',
    expectedResult: 'Atlantys navigation computer is controlling',
  };

  const actionStep2: ActionStepModel = {
    id: 4,
    stepOrder: 1,
    projectId: 1,
    customFieldValues: getTestStepsCustomFieldValues(),
    attachmentList: { id: 4, attachments: [] },
    kind: 'action-step',
    action: 'check main engines glimbal',
    expectedResult: 'main engines ok to handle corrections',
  };

  const actionStep3: ActionStepModel = {
    id: 1,
    projectId: 1,
    stepOrder: 2,
    customFieldValues: getTestStepsCustomFieldValues('another value'),
    attachmentList: { id: 3, attachments: [] },
    kind: 'action-step',
    action: 'solid rocket booster ignition',
    expectedResult: 'big petards are running',
  };

  const actionStep4: ActionStepModel = {
    id: 3,
    projectId: 1,
    stepOrder: 3,
    customFieldValues: getTestStepsCustomFieldValues(),
    attachmentList: { id: 3, attachments: [] },
    kind: 'action-step',
    action: 'release fixation arms',
    expectedResult: '... and we have lift off. Space shuttle Atlantis is returning to the ISS',
  };

  const callStep1: CallStepModel = {
    id: 5,
    stepOrder: 4,
    projectId: 1,
    calledTcId: 12,
    calledTcName: 'first flight phase',
    kind: 'call-step',
    calledDatasetId: null,
    calledTestCaseSteps: [],
    delegateParam: false,
    calledDatasetName: '',
  };

  const testSteps: TestStepModel[] = [
    actionStep1,
    actionStep2,
    actionStep3,
    actionStep4,
    callStep1,
  ];
  return createTestCaseModel(testSteps);
}

function navigateToTestCase(): TestCaseViewPage {
  const testCaseWorkspacePage = initTestCaseWorkspacePage();
  const model = createTestCaseModelWithSteps();
  return testCaseWorkspacePage.tree.selectNode<TestCaseViewPage>('TestCase-3', model);
}

function createTestCaseModel(testSteps: TestStepModel[]): TestCaseModel {
  return mockTestCaseModel({
    id: 3,
    projectId: 1,
    name: 'Launch Atlantis',
    customFieldValues: [],
    attachmentList: {
      id: 1,
      attachments: [],
    },
    reference: 'STS 135',
    description: '',
    uuid: '',
    type: 20,
    testSteps,
    status: 'WORK_IN_PROGRESS',
    prerequisite: '',
    parameters: [],
    nbIssues: 0,
    nature: 12,
    milestones: [],
    lastModifiedOn: new Date('2020-03-09 10:30').toISOString(),
    lastModifiedBy: 'admin',
    kind: 'STANDARD',
    importanceAuto: false,
    importance: 'VERY_HIGH',
    executions: [],
    datasets: [],
    datasetParamValues: [],
    createdOn: new Date('2020-03-09 10:30').toISOString(),
    createdBy: 'admin',
    coverages: initialCoverages,
    automationRequest: null,
    automatable: 'M',
    calledTestCases: [],
    lastExecutionStatus: 'SUCCESS',
  });
}
