import { TestCaseStatistics } from '../../../../../projects/sqtm-core/src/lib/model/test-case/test-case-statistics.model';
import { CustomDashboardModel } from '../../../../../projects/sqtm-core/src/lib/model/custom-report/custom-dashboard.model';
import {
  ChartColumnType,
  ChartDataType,
  ChartOperation,
  ChartScopeType,
  ChartType,
} from '../../../../../projects/sqtm-core/src/lib/model/custom-report/chart-definition.model';
import { EntityType } from '../../../../../projects/sqtm-core/src/lib/model/entity.model';

export function getStatistics(): TestCaseStatistics {
  return {
    selectedIds: [4, 5, 7, 9, 12, 78],
    sizeStatistics: {
      zeroSteps: 1,
      between0And10Steps: 2,
      between11And20Steps: 2,
      above20Steps: 1,
    },
    importanceStatistics: { low: 1, medium: 2, high: 1, veryHigh: 2 },
    statusesStatistics: {
      workInProgress: 2,
      underReview: 1,
      approved: 2,
      toBeUpdated: 1,
      obsolete: 0,
    },
    boundRequirementsStatistics: { zeroRequirements: 1, oneRequirement: 4, manyRequirements: 1 },
  };
}

export function getFavoriteDashboard(): CustomDashboardModel {
  return {
    favoriteWorkspaces: [],
    id: 2,
    projectId: 1,
    customReportLibraryNodeId: 18,
    name: 'Favorite Dashboard',
    createdBy: 'cypress',
    chartBindings: [
      {
        id: 2,
        chartDefinitionId: 2,
        dashboardId: 2,
        chartInstance: {
          id: 2,
          customReportLibraryNodeId: null,
          projectId: 1,
          name: 'New Chart',
          type: ChartType.PIE,
          measures: [
            {
              cufId: null,
              label: '',
              column: {
                id: 1,
                columnType: ChartColumnType.ATTRIBUTE,
                label: 'TEST_CASE_ID',
                specializedType: { entityType: EntityType.TEST_CASE },
                dataType: ChartDataType.NUMERIC,
              },
              operation: ChartOperation.COUNT,
            },
          ],
          axis: [
            {
              cufId: null,
              label: '',
              column: {
                id: 1,
                columnType: ChartColumnType.ATTRIBUTE,
                label: 'TEST_CASE_REFERENCE',
                specializedType: { entityType: EntityType.TEST_CASE },
                dataType: ChartDataType.STRING,
              },
              operation: ChartOperation.NONE,
            },
          ],
          filters: [],
          abscissa: [['']],
          series: { '': [2] },
          projectScope: [],
          scope: [],
          scopeType: ChartScopeType.DEFAULT,
          lastModifiedBy: 'admin',
          lastModifiedOn: new Date().toISOString(),
          createdBy: 'admin',
          createdOn: new Date().toISOString(),
          colours: [],
        },
        row: 1,
        col: 1,
        sizeX: 2,
        sizeY: 2,
      },
    ],
    reportBindings: [],
  };
}
