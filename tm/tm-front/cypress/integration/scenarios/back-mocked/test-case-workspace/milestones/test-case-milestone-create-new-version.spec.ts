import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { ReferentialDataMockBuilder } from '../../../../utils/referential/referential-data-builder';
import { TestCaseViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { mockTestCaseModel } from '../../../../data-mock/test-case.data-mock';
import { TestCaseModel } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/test-case.model';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

const description = `<p>a nice description</p>`;

const endDate = new Date(2020, 6, 28).toISOString();

const ORBITAL_RDV = 'orbital rendez-vous';

function buildReferentialData() {
  const referentialDataMock = new ReferentialDataMockBuilder()
    .withProjects({ name: 'Apollo', label: 'Apollo' }, { name: 'Gemini', label: 'Gemini' })
    .withMilestones(
      {
        label: ORBITAL_RDV,
        description: '',
        endDate,
        status: 'IN_PROGRESS',
        boundProjectIndexes: [0, 1],
        range: 'GLOBAL',
        ownerFistName: 'admin',
        ownerLastName: 'admin',
        ownerLogin: 'admin',
      },
      {
        label: 'saturn 5 tests',
        description: '',
        endDate: new Date().toISOString(),
        status: 'IN_PROGRESS',
        boundProjectIndexes: [1],
        range: 'GLOBAL',
        ownerFistName: 'admin',
        ownerLastName: 'admin',
        ownerLogin: 'admin',
      },
    )
    .withUser({
      hasAnyReadPermission: true,
      functionalTester: true,
    })
    .build();
  referentialDataMock.globalConfiguration.milestoneFeatureEnabled = true;
  return referentialDataMock;
}

describe('Test Case Milestone Create New Version', function () {
  it('should create new version in milestone mode', () => {
    const testCaseWorkspacePage: TestCaseWorkspacePage = navigateToTestCaseWorkspace();
    testCaseWorkspacePage.navBar.toggle();
    const milestoneSelector = testCaseWorkspacePage.navBar.openMilestoneSelector();
    milestoneSelector.selectMilestone(ORBITAL_RDV);
    milestoneSelector.confirm();
    const childNodes = [
      mockTreeNode({
        id: 'TestCaseLibrary-1',
        children: ['TestCase-3'],
        data: { NAME: 'Project1', CHILD_COUNT: '1' },
        state: DataRowOpenState.open,
      }),
      mockTreeNode({
        id: 'TestCase-3',
        children: [],
        parentRowId: 'TestCaseLibrary-1',
        data: {
          NAME: 'a nice test',
          TC_KIND: 'STANDARD',
          TC_STATUS: 'APPROVED',
          IMPORTANCE: 'HIGH',
          MILESTONES: [1, 2],
        },
      }),
    ];
    testCaseWorkspacePage.tree.openNode('TestCaseLibrary-1', childNodes);
    const testCaseModel = buildTestCaseModel(3, 'a nice test');
    const page: TestCaseViewPage = testCaseWorkspacePage.tree.selectNode(
      'TestCase-3',
      testCaseModel,
    );
    const dialog = page.openCreateNewVersion();
    dialog.checkCreateTestCaseDialogButtons();
    dialog.checkNameContent(`${testCaseModel.name}-${ORBITAL_RDV}`);
    dialog.checkReferenceContent(testCaseModel.reference);
    dialog.checkDescriptionContent(testCaseModel.description);
    const name = 'One Milestone to bring them all and in darkness bind them';
    dialog.fillName(name);
    dialog.fillReference('');
    const refreshedNodes = [
      mockTreeNode({
        id: 'TestCaseLibrary-1',
        children: ['TestCase-4'],
        data: { NAME: 'Project1', CHILD_COUNT: '1' },
        state: DataRowOpenState.open,
      }),
      mockTreeNode({
        id: 'TestCase-4',
        children: [],
        parentRowId: 'TestCaseLibrary-1',
        data: {
          NAME: name,
          TC_KIND: 'STANDARD',
          TC_STATUS: 'APPROVED',
          IMPORTANCE: 'HIGH',
          MILESTONES: [1, 2],
        },
      }),
    ];
    dialog.confirm(refreshedNodes, buildTestCaseModel(4, name));
    testCaseWorkspacePage.tree.assertNodeNotExist('TestCase-3');
    testCaseWorkspacePage.tree.assertNodeExist('TestCase-4');
    testCaseWorkspacePage.tree.assertNodeIsSelected('TestCase-4');
  });

  function buildTestCaseModel(id: number, name: string): TestCaseModel {
    return mockTestCaseModel({
      id,
      projectId: 1,
      name,
      customFieldValues: [],
      attachmentList: {
        id: 1,
        attachments: [],
      },
      reference: 'REF',
      description,
      uuid: '8c83e7cd-6073-4624-8787-93482055f904',
      type: 20,
      testSteps: [],
      status: 'WORK_IN_PROGRESS',
      prerequisite: '',
      parameters: [],
      nbIssues: 0,
      nature: 12,
      milestones: [],
      lastModifiedOn: new Date('2020-03-09 10:30').toISOString(),
      lastModifiedBy: 'admin',
      kind: 'STANDARD',
      importanceAuto: true,
      importance: 'LOW',
      executions: [],
      datasets: [],
      datasetParamValues: [],
      createdOn: new Date('2020-03-09 10:30').toISOString(),
      createdBy: 'admin',
      coverages: [],
      calledTestCases: [],
      automationRequest: null,
      automatable: 'N',
      lastExecutionStatus: 'SUCCESS',
    });
  }

  function navigateToTestCaseWorkspace(): TestCaseWorkspacePage {
    const referentialDataMock = buildReferentialData();

    const initialNodes = mockGridResponse('id', [
      mockTreeNode({
        id: 'TestCaseLibrary-1',
        children: [],
        data: { NAME: 'Project 1', MILESTONES: [1, 2] },
      }),
    ]);
    return TestCaseWorkspacePage.initTestAtPage(initialNodes, referentialDataMock);
  }
});
