import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {
  ALL_PROJECT_PERMISSIONS,
  NO_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder,
} from '../../../../utils/referential/referential-data-builder';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

describe('TestCase Workspace Tree Copy', function () {
  function referentialData() {
    return new ReferentialDataMockBuilder()
      .withProjects(
        {
          name: 'Project 1',
          permissions: ALL_PROJECT_PERMISSIONS,
        },
        {
          name: 'Project 2',
          permissions: NO_PROJECT_PERMISSIONS,
        },
      )
      .build();
  }

  const initialNodes = mockGridResponse('id', [
    mockTreeNode({
      id: 'TestCaseLibrary-1',
      projectId: 1,
      children: [],
      data: { NAME: 'Project1', CHILD_COUNT: '3' },
    }),
    mockTreeNode({
      id: 'TestCaseLibrary-2',
      projectId: 2,
      children: [],
      data: { NAME: 'Project2', CHILD_COUNT: '1' },
    }),
  ]);

  const libraryRefreshAtOpen = [
    mockTreeNode({
      id: 'TestCaseLibrary-1',
      projectId: 1,
      children: ['TestCaseFolder-1', 'TestCase-3', 'TestCaseFolder-2'],
      data: { NAME: 'Project1', CHILD_COUNT: '3' },
      state: DataRowOpenState.open,
    }),
    mockTreeNode({
      id: 'TestCaseFolder-1',
      children: [],
      projectId: 1,
      parentRowId: 'TestCaseLibrary-1',
      data: { NAME: 'folder1' },
    }),
    mockTreeNode({
      id: 'TestCase-3',
      children: [],
      projectId: 1,
      parentRowId: 'TestCaseLibrary-1',
      data: { NAME: 'a nice test', TC_KIND: 'STANDARD', TC_STATUS: 'APPROVED', IMPORTANCE: 'HIGH' },
    }),
    mockTreeNode({
      id: 'TestCaseFolder-2',
      children: [],
      projectId: 1,
      parentRowId: 'TestCaseLibrary-1',
      data: { NAME: 'folder2' },
    }),
  ];

  it('should activate or deactivate copy button according to user selection', () => {
    const firstNode = initialNodes.dataRows[0];
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes);
    const tree = testCaseWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.selectNode(firstNode.id);
    testCaseWorkspacePage.treeMenu.assertCopyButtonIsDisabled();
    testCaseWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    tree.selectNode('TestCaseFolder-1');
    testCaseWorkspacePage.treeMenu.assertCopyButtonIsActive();
    testCaseWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    tree.selectNode('TestCase-3');
    testCaseWorkspacePage.treeMenu.assertCopyButtonIsActive();
    testCaseWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    tree.selectNode('TestCaseLibrary-1');
    testCaseWorkspacePage.treeMenu.assertCopyButtonIsDisabled();
    testCaseWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
  });

  it('should activate or deactivate paste button according to destination', () => {
    const firstNode = initialNodes.dataRows[0];
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(
      initialNodes,
      referentialData(),
    );
    const tree = testCaseWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.selectNode(firstNode.id);
    testCaseWorkspacePage.treeMenu.assertCopyButtonIsDisabled();
    testCaseWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    tree.selectNode('TestCase-3');
    testCaseWorkspacePage.treeMenu.assertCopyButtonIsActive();
    testCaseWorkspacePage.treeMenu.copy();
    testCaseWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    tree.selectNode('TestCaseFolder-1');
    testCaseWorkspacePage.treeMenu.assertPasteButtonIsActive();
    tree.selectNode('TestCaseLibrary-1');
    testCaseWorkspacePage.treeMenu.assertPasteButtonIsActive();
    tree.selectNode('TestCase-3');
    testCaseWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    // Testing permissions
    tree.selectNode('TestCaseLibrary-2');
    testCaseWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
  });

  function performCopyPaste(useShortcut = false) {
    const refreshedNodes = [
      mockTreeNode({
        id: 'TestCaseFolder-1',
        projectId: 1,
        children: ['TestCase-4'],
        parentRowId: 'TestCaseLibrary-1',
        state: DataRowOpenState.open,
        data: { NAME: 'folder1', CHILD_COUNT: 1 },
      }),
      mockTreeNode({
        id: 'TestCase-4',
        projectId: 1,
        children: [],
        parentRowId: 'TestCaseFolder-1',
        data: {
          NAME: 'a nice test',
          TC_KIND: 'STANDARD',
          TC_STATUS: 'APPROVED',
          IMPORTANCE: 'HIGH',
        },
      }),
    ];
    const firstNode = initialNodes.dataRows[0];
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(
      initialNodes,
      referentialData(),
    );
    const tree = testCaseWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.selectNode('TestCase-3');
    testCaseWorkspacePage.treeMenu.copy(useShortcut);
    tree.selectNode('TestCaseFolder-1');
    testCaseWorkspacePage.treeMenu.paste(
      { dataRows: refreshedNodes },
      'test-case-tree',
      'TestCaseFolder-1',
      useShortcut,
    );
    tree.assertNodeExist('TestCase-4');
    tree.assertNodeIsOpen('TestCaseFolder-1');
  }

  it('should copy paste a node', () => {
    performCopyPaste();
  });

  it('should copy paste a node with keyboard shortcut', () => {
    performCopyPaste(true);
  });
});
