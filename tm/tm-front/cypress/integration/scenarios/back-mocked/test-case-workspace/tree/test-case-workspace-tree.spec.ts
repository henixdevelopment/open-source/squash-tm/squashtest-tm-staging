import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { createEntityReferentialData } from '../../../../utils/referential/create-entity-referential.const';
import { CreateTestCaseDialog } from '../../../../page-objects/pages/test-case-workspace/dialogs/create-test-case-dialog.element';
import { TestCaseViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { TreeElement } from '../../../../page-objects/elements/grid/grid.element';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { CreateTestCaseFolderDialog } from '../../../../page-objects/pages/test-case-workspace/dialogs/create-test-case-folder-dialog.element';
import { TestCaseFolderViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case-folder/test-case-folder-view.page';
import { ReferentialDataMockBuilder } from '../../../../utils/referential/referential-data-builder';
import {
  mockTestCaseFolderModel,
  mockTestCaseLibraryModel,
  mockTestCaseModel,
} from '../../../../data-mock/test-case.data-mock';
import { TestCaseLibraryModel } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/test-case-library/test-case-library.model';
import { ProjectData } from '../../../../../../projects/sqtm-core/src/lib/model/project/project-data.model';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { TestCaseModel } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/test-case.model';
import { TestCaseKindKeys } from '../../../../../../projects/sqtm-core/src/lib/model/level-enums/test-case/test-case-kind';
import { TestCaseFolderModel } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/test-case-folder/test-case-folder.model';
import {
  DataRowModel,
  DataRowOpenState,
} from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { mockTreeNode } from '../../../../data-mock/grid.data-mock';
import Chainable = Cypress.Chainable;

type Identifier = string | number;

function getChildNodes() {
  return [
    mockTreeNode({
      id: 'TestCaseLibrary-1',
      children: ['TestCaseFolder-1', 'TestCase-3', 'TestCaseFolder-2'],
      data: { NAME: 'Project1', CHILD_COUNT: '3' },
      state: DataRowOpenState.open,
    }),
    mockTreeNode({
      id: 'TestCaseFolder-1',
      children: [],
      parentRowId: 'TestCaseLibrary-1',
      data: { NAME: 'folder1' },
    }),
    mockTreeNode({
      id: 'TestCase-3',
      children: [],
      parentRowId: 'TestCaseLibrary-1',
      data: { NAME: 'a nice test', TC_KIND: 'STANDARD', TC_STATUS: 'APPROVED', IMPORTANCE: 'HIGH' },
    }),
    mockTreeNode({
      id: 'TestCaseFolder-2',
      children: [],
      parentRowId: 'TestCaseLibrary-1',
      data: { NAME: 'folder2' },
    }),
  ];
}

describe('TestCase Workspace Tree', () => {
  describe('TestCase Workspace Tree Display', function () {
    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [
        mockTreeNode({
          id: 'TestCaseLibrary-1',
          children: [],
          data: { NAME: 'Project1', CHILD_COUNT: '3' },
        }),
      ],
    };

    it('should display a simple tree', () => {
      const firstNode = initialNodes.dataRows[0];
      const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes);
      const tree = testCaseWorkspacePage.tree;
      tree.assertNodeExist(firstNode.id);
      tree.assertNodeTextContains(firstNode.id, firstNode.data['NAME']);
    });

    it('should expand a library node', () => {
      const childNodes = getChildNodes();
      const firstNode = initialNodes.dataRows[0];
      const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes);
      const tree = testCaseWorkspacePage.tree;
      tree.openNode(firstNode.id, childNodes);
      tree.assertNodeIsOpen(firstNode.id);
      tree.assertNodeExist('TestCaseFolder-1');
      tree.assertNodeTextContains('TestCaseFolder-1', 'folder1');
      tree.assertNodeExist('TestCase-3');
      tree.assertNodeTextContains('TestCase-3', 'a nice test');
      tree.assertNodeExist('TestCaseFolder-2');
      tree.assertNodeTextContains('TestCaseFolder-2', 'folder2');
      tree.closeNode(firstNode.id);
      tree.assertNodeIsClosed(firstNode.id);
      tree.assertNodeNotExist('TestCaseFolder-1');
      tree.assertNodeNotExist('TestCase-3');
      tree.assertNodeNotExist('TestCaseFolder-2');
    });

    it('should do various selections', () => {
      const childNodes = getChildNodes();
      const firstNode = initialNodes.dataRows[0];
      const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes);
      const tree = testCaseWorkspacePage.tree;
      tree.openNode(firstNode.id, childNodes);
      tree.assertNodeIsOpen(firstNode.id);
      tree.selectNode('TestCase-3', mockTestCaseModel());
      tree.assertNodeIsSelected('TestCase-3');
      tree.expendSelectionToNode('TestCaseFolder-2');
      tree.assertNodeIsSelected('TestCaseFolder-1');
      tree.assertNodeIsSelected('TestCaseFolder-2');
      tree.selectNode('TestCaseFolder-2');
      tree.expendSelectionToNode('TestCase-3');
      tree.assertNodeIsSelected('TestCase-3');
      tree.assertNodeIsSelected('TestCaseFolder-1');
      tree.assertNodeIsSelected('TestCaseFolder-2');
      tree.expendSelectionToNode('TestCaseFolder-1');
      tree.assertNodeIsNotSelected('TestCaseFolder-1');
      tree.assertNodeIsNotSelected('TestCase-3');
      tree.assertNodeIsSelected('TestCaseFolder-2');
    });
  });
  describe('TestCase Workspace Tree Milestone Mode', function () {
    it('should filter tree by milestones', () => {
      const referentialDataMock = new ReferentialDataMockBuilder()
        .withProjects(
          { name: 'Project 1', label: 'Etiquette' },
          { name: 'Project 2', label: 'Etiquette 2' },
        )
        .withMilestones(
          {
            label: 'milestone1',
            description: '',
            endDate: new Date().toISOString(),
            status: 'IN_PROGRESS',
            boundProjectIndexes: [1],
            range: 'GLOBAL',
          },
          {
            label: 'ahhh',
            description: '',
            endDate: new Date().toISOString(),
            status: 'IN_PROGRESS',
            boundProjectIndexes: [1],
            range: 'GLOBAL',
          },
        )
        .withUser({
          hasAnyReadPermission: true,
          functionalTester: true,
        })
        .build();
      referentialDataMock.globalConfiguration.milestoneFeatureEnabled = true;

      const tcl2 = mockTreeNode({
        id: 'TestCaseLibrary-2',
        children: [],
        data: { NAME: 'Project 2', CHILD_COUNT: '0', MILESTONES: [] },
      });

      const tcl1 = mockTreeNode({
        id: 'TestCaseLibrary-1',
        children: ['TestCaseFolder-1', 'TestCase-3', 'TestCase-4', 'TestCaseFolder-2'],
        data: { NAME: 'Project 1', CHILD_COUNT: '3', MILESTONES: [1, 2] },
        state: DataRowOpenState.open,
      });

      const tcf1 = mockTreeNode({
        id: 'TestCaseFolder-1',
        children: [],
        parentRowId: 'TestCaseLibrary-1',
        data: { NAME: 'folder1', MILESTONES: [1, 2] },
      });

      const tc3 = mockTreeNode({
        id: 'TestCase-3',
        children: [],
        parentRowId: 'TestCaseLibrary-1',
        data: {
          NAME: 'a nice test',
          TC_KIND: 'STANDARD',
          TC_STATUS: 'APPROVED',
          IMPORTANCE: 'HIGH',
          MILESTONES: [1],
        },
      });
      const tc4 = mockTreeNode({
        id: 'TestCase-4',
        children: [],
        parentRowId: 'TestCaseLibrary-1',
        data: {
          NAME: 'another nice test',
          TC_KIND: 'STANDARD',
          TC_STATUS: 'APPROVED',
          IMPORTANCE: 'HIGH',
          MILESTONES: [2],
        },
      });
      const tcf2 = mockTreeNode({
        id: 'TestCaseFolder-2',
        children: [],
        parentRowId: 'TestCaseLibrary-1',
        data: { NAME: 'folder2', MILESTONES: [1, 2] },
      });

      const allNodes: DataRowModel[] = [tcl1, tcl2, tcf1, tcf2, tc3, tc4];

      const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(
        { dataRows: allNodes },
        referentialDataMock,
      );
      const navBar = testCaseWorkspacePage.navBar;
      const tree = testCaseWorkspacePage.tree;
      tree.assertNodeIsOpen(tcl1.id);
      tree.assertNodeExist('TestCaseLibrary-1');
      tree.assertNodeExist('TestCaseLibrary-2');
      tree.assertNodeExist('TestCaseFolder-1');
      tree.assertNodeExist('TestCaseFolder-2');
      tree.assertNodeExist('TestCase-3');
      tree.assertNodeExist('TestCase-4');
      const milestonePicker = navBar.openMilestoneSelector();
      milestonePicker.selectMilestone('milestone1');
      milestonePicker.confirm();
      tree.assertNodeExist('TestCaseLibrary-1');
      tree.assertNodeExist('TestCaseFolder-1');
      tree.assertNodeExist('TestCaseFolder-2');
      tree.assertNodeExist('TestCase-3');
      tree.assertNodeNotExist('TestCase-4');
      tree.assertNodeNotExist('TestCaseLibrary-2');
      navBar.disableMilestoneMode();
      tree.assertNodeExist('TestCaseLibrary-1');
      tree.assertNodeExist('TestCaseFolder-1');
      tree.assertNodeExist('TestCaseFolder-2');
      tree.assertNodeExist('TestCase-3');
      tree.assertNodeExist('TestCase-4');
      tree.assertNodeExist('TestCaseLibrary-2');
      navBar.openMilestoneSelector();
      milestonePicker.selectMilestone('ahhh');
      milestonePicker.confirm();
      tree.assertNodeExist('TestCaseLibrary-1');
      tree.assertNodeExist('TestCaseFolder-1');
      tree.assertNodeExist('TestCaseFolder-2');
      tree.assertNodeNotExist('TestCase-3');
      tree.assertNodeExist('TestCase-4');
      tree.assertNodeNotExist('TestCaseLibrary-2');
    });
  });
  describe('Create a test case', function () {
    const tcl1 = mockTreeNode({
      id: 'TestCaseLibrary-1',
      children: [],
      projectId: 1,
      data: { NAME: 'Project1' },
    });

    const initialNodes: GridResponse = {
      count: 2,
      dataRows: [
        { ...tcl1 },
        mockTreeNode({
          id: 'TestCaseLibrary-2',
          children: [],
          projectId: 2,
          data: { NAME: 'Project2' },
        }),
      ],
    };

    it('should create new test cases', () => {
      // Navigate to workspace page
      const workspacePage = TestCaseWorkspacePage.initTestAtPage(
        initialNodes,
        createEntityReferentialData,
      );
      const tree = workspacePage.tree;

      // Navigate to create dialog
      const createDialog = navigateToAddTestCaseDialog(
        workspacePage,
        tree,
        initialNodes.dataRows[0].id,
      );

      // We'll start by adding a test case with the "Add another" button
      const mockDataAddAnother = createTestCaseMockData(
        85,
        'Test case 01',
        'BLK',
        'Une description riche...',
        1,
        'TestCaseLibrary-1',
      );

      const updatedLibrary = refreshNodeAfterOpen(tcl1, ['TestCase-85']);

      addTestCaseAndCheckViewPage(
        createDialog,
        mockDataAddAnother,
        tree,
        'TestCaseLibrary-1',
        true,
        true,
        [updatedLibrary, mockDataAddAnother.addedDataRow],
      ).then(() => {
        // Then, we'll try adding another one with the "Add" button
        const mockDataAdd = createTestCaseMockData(
          86,
          'Test case 02',
          'BLK',
          'Une description riche...',
          1,
          'TestCaseLibrary-1',
        );

        updatedLibrary.children = ['TestCase-85', 'TestCase-86'];

        return addTestCaseAndCheckViewPage(
          createDialog,
          mockDataAdd,
          tree,
          'TestCaseLibrary-1',
          false,
          false,
          [updatedLibrary, mockDataAddAnother.addedDataRow, mockDataAdd.addedDataRow],
        );
      });

      // Project 2 : should NOT be allowed to create
      const secondModel: TestCaseLibraryModel = mockTestCaseLibraryModel({
        attachmentList: {
          id: 1,
          attachments: [],
        },
        customFieldValues: [],
        description: '',
        id: 2,
        name: 'Project2',
        projectId: 2,
        statistics: {
          boundRequirementsStatistics: null,
          generatedOn: new Date(),
          importanceStatistics: null,
          selectedIds: [],
          sizeStatistics: null,
          statusesStatistics: null,
        },
      });

      tree.selectNode(initialNodes.dataRows[1].id, secondModel);
      workspacePage.treeMenu.assertCreateButtonIsDisabled();
    });

    it('should forbid test case creation if name already exists in container', () => {
      const firstModel: TestCaseLibraryModel = mockTestCaseLibraryModel({
        attachmentList: {
          id: 1,
          attachments: [],
        },
        customFieldValues: [],
        description: '',
        id: 1,
        name: 'Project1',
        projectId: 1,
        statistics: {
          boundRequirementsStatistics: null,
          generatedOn: new Date(),
          importanceStatistics: null,
          selectedIds: [],
          sizeStatistics: null,
          statusesStatistics: null,
        },
      });

      const httpError = {
        squashTMError: {
          kind: 'FIELD_VALIDATION_ERROR',
          fieldValidationErrors: [
            {
              fieldName: 'name',
              i18nKey: 'sqtm-core.error.generic.duplicate-name',
            },
          ],
        },
      };

      const workspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes);
      workspacePage.tree.selectNode(initialNodes.dataRows[0].id, firstModel);
      const createDialog: CreateTestCaseDialog = workspacePage.treeMenu.openCreateTestCase();
      createDialog.fillName('Test case 01');
      createDialog.addWithServerSideFailure(httpError);
      createDialog.assertDuplicateNameErrorExist();
      createDialog.assertExists();
    });

    it('should forbid test case creation if mandatory cuf is empty', () => {
      // Navigate to workspace page
      const workspacePage = TestCaseWorkspacePage.initTestAtPage(
        initialNodes,
        createEntityReferentialData,
      );

      // Navigate to create dialog
      const createDialog = navigateToAddTestCaseDialog(
        workspacePage,
        workspacePage.tree,
        initialNodes.dataRows[0].id,
      );
      createDialog.fillName('Test case 01');
      createDialog.fillReference('BLCK');
      createDialog.fillDescription('Une description riche...');

      clearCustomFieldsAndCheckErrors(createDialog, 12, 13, 14, 16);
    });

    it('should remove any error message on CUFs after a Test Case was successfully created', () => {
      // Navigate to workspace page
      const workspacePage = TestCaseWorkspacePage.initTestAtPage(
        initialNodes,
        createEntityReferentialData,
      );
      const tree = workspacePage.tree;

      // Navigate to create dialog
      const createDialog = navigateToAddTestCaseDialog(
        workspacePage,
        tree,
        initialNodes.dataRows[0].id,
      );

      // Fill form...
      createDialog.fillName('TC01');

      // Clear some CUFS and submit to show errors
      clearCustomFieldsAndCheckErrors(createDialog, 12, 13, 14, 16);

      // Correct errors and submit with 'add another'
      createDialog.fillCustomField(12, 'My CUF 12');
      createDialog.fillCustomField(13, '13');
      createDialog.toggleTagFieldOptions(14, 0, 1, 2);
      createDialog.fillCustomField(16, 'rich value');

      const mockData = createTestCaseMockData(85, 'TC01', '', '', 1, 'TestCaseLibrary-1');

      const updatedLibrary = refreshNodeAfterOpen(tcl1, ['TestCase-85']);
      addTestCaseAndCheckViewPage(createDialog, mockData, tree, 'TestCaseLibrary-1', true, true, [
        updatedLibrary,
        mockData.addedDataRow,
      ]).then(() => {
        // Check that errors for CustomField are reset
        createDialog.assertCustomFieldHasNoError(12);
        createDialog.assertCustomFieldHasNoError(13);
      });
    });

    it('should create folder', () => {
      const workspacePage = TestCaseWorkspacePage.initTestAtPage(
        initialNodes,
        createEntityReferentialData,
      );
      const creationTestCaseFolderDialog = navigateToAddTestCaseFolderDialog(
        workspacePage,
        workspacePage.tree,
        initialNodes.dataRows[0].id,
      );
      creationTestCaseFolderDialog.assertExists();
      const testCaseFolderMockData = createTestCaseFolderMockData(
        5,
        'Folder',
        'New Folder',
        1,
        'TestCaseLibrary-1',
      );
      const updatedLibrary = refreshNodeAfterOpen(tcl1, ['TestCaseFolder-5']);
      addTestCaseFolderAndCheckViewPage(
        creationTestCaseFolderDialog,
        testCaseFolderMockData,
        workspacePage.tree,
        'TestCaseLibrary-1',
        false,
        true,
        [updatedLibrary, testCaseFolderMockData.addedDataRow],
      );
    });

    it('should create new gherkin test cases', () => {
      // Navigate to workspace page
      const workspacePage = TestCaseWorkspacePage.initTestAtPage(
        initialNodes,
        createEntityReferentialData,
      );
      const tree = workspacePage.tree;

      // Navigate to create dialog
      const createDialog = navigateToAddTestCaseDialog(
        workspacePage,
        tree,
        initialNodes.dataRows[0].id,
      );

      // We'll start by adding a test case with the "Add another" button
      const mockDataAddAnother = createTestCaseMockData(
        85,
        'Test case 01',
        'BLK',
        'Une description riche...',
        1,
        'TestCaseLibrary-1',
        'GHERKIN',
      );

      const updatedLibrary = refreshNodeAfterOpen(tcl1, ['TestCase-85']);

      addTestCaseAndCheckViewPage(
        createDialog,
        mockDataAddAnother,
        tree,
        'TestCaseLibrary-1',
        false,
        true,
        [updatedLibrary, mockDataAddAnother.addedDataRow],
        'Gherkin',
      );
    });

    it('should create new keyword test cases', () => {
      // Navigate to workspace page
      const workspacePage = TestCaseWorkspacePage.initTestAtPage(
        initialNodes,
        createEntityReferentialData,
      );
      const tree = workspacePage.tree;

      // Navigate to create dialog
      const createDialog = navigateToAddTestCaseDialog(
        workspacePage,
        tree,
        initialNodes.dataRows[0].id,
      );

      // We'll start by adding a test case with the "Add another" button
      const mockDataAddAnother = createTestCaseMockData(
        85,
        'Test case 01',
        'BLK',
        'Une description riche...',
        1,
        'TestCaseLibrary-1',
        'KEYWORD',
      );

      const updatedLibrary = refreshNodeAfterOpen(tcl1, ['TestCase-85']);

      addTestCaseAndCheckViewPage(
        createDialog,
        mockDataAddAnother,
        tree,
        'TestCaseLibrary-1',
        false,
        true,
        [updatedLibrary, mockDataAddAnother.addedDataRow],
        'BDD',
      );
    });

    it('should create new exploratory test cases', () => {
      // Navigate to workspace page
      const workspacePage = TestCaseWorkspacePage.initTestAtPage(
        initialNodes,
        createEntityReferentialData,
      );
      const tree = workspacePage.tree;

      // Navigate to create dialog
      const createDialog = navigateToAddTestCaseDialog(
        workspacePage,
        tree,
        initialNodes.dataRows[0].id,
      );

      // We'll start by adding a test case with the "Add another" button
      const mockDataAddAnother = createTestCaseMockData(
        85,
        'Test case 02',
        'r',
        'd',
        1,
        'TestCaseLibrary-1',
        'EXPLORATORY',
      );

      const updatedLibrary = refreshNodeAfterOpen(tcl1, ['TestCase-85']);

      addTestCaseAndCheckViewPage(
        createDialog,
        mockDataAddAnother,
        tree,
        'TestCaseLibrary-1',
        false,
        true,
        [updatedLibrary, mockDataAddAnother.addedDataRow],
        'Exploratoire',
      );
    });
  });

  describe('Persist tree selection', function () {
    const tcf1 = mockTreeNode({
      id: 'TestCaseFolder-1',
      children: ['TestCaseFolder-2'],
      parentRowId: 'TestCaseLibrary-2',
      projectId: 2,
      data: { NAME: 'Folder-1', CHILD_COUNT: 1 },
    });
    const tcf2 = mockTreeNode({
      id: 'TestCaseFolder-2',
      parentRowId: 'TestCaseFolder-1',
      children: [],
      projectId: 2,
      data: { NAME: 'Folder-2', CHILD_COUNT: 1 },
    });
    const tcl1 = mockTreeNode({
      id: 'TestCaseLibrary-1',
      children: [],
      projectId: 1,
      data: { NAME: 'Project1' },
    });
    const tcl2 = mockTreeNode({
      id: 'TestCaseLibrary-2',
      children: ['TestCaseFolder-1'],
      projectId: 2,
      data: { NAME: 'Project2', CHILD_COUNT: 1 },
    });
    const initialNodes: GridResponse = {
      count: 2,
      dataRows: [tcl1, tcl2],
    };
    const initialNodesWithOpened: GridResponse = {
      count: 4,
      dataRows: [
        tcl1,
        { ...tcl2, state: DataRowOpenState.open },
        { ...tcf1, state: DataRowOpenState.open },
        tcf2,
      ],
    };

    it('should persist tree state', () => {
      // Navigate to workspace page
      const workspacePage = TestCaseWorkspacePage.initTestAtPage(
        initialNodes,
        createEntityReferentialData,
      );
      const firstModel: TestCaseLibraryModel = mockTestCaseLibraryModel({
        attachmentList: {
          id: 1,
          attachments: [],
        },
        customFieldValues: [],
        description: '',
        id: 1,
        name: 'Project1',
        projectId: 1,
        statistics: {
          boundRequirementsStatistics: null,
          generatedOn: new Date(),
          importanceStatistics: null,
          selectedIds: [],
          sizeStatistics: null,
          statusesStatistics: null,
        },
      });
      const page = workspacePage.tree.selectNode('TestCaseLibrary-1', firstModel);
      page.assertExists();
      NavBarElement.navigateToCampaignWorkspace();
      NavBarElement.navigateToTestCaseWorkspace(initialNodes);
      page.assertExists();
      workspacePage.tree.openNode('TestCaseLibrary-2', [refreshNodeAfterOpen(tcl2), tcf1]);
      workspacePage.tree.openNode('TestCaseFolder-1', [refreshNodeAfterOpen(tcf1), tcf2]);
      NavBarElement.navigateToCampaignWorkspace();
      NavBarElement.navigateToTestCaseWorkspace(initialNodesWithOpened);
      workspacePage.tree.assertNodeIsOpen('TestCaseLibrary-2');
      workspacePage.tree.assertNodeIsOpen('TestCaseFolder-1');
    });
  });
});

/* Utility functions */

/* Shortcut to empty a bunch of CUFS, validate the form and check that validation errors are shown */
function clearCustomFieldsAndCheckErrors(createDialog, ...cufIds: number[]) {
  cufIds.forEach((id) => createDialog.clearCustomField(id));
  createDialog.addWithClientSideFailure();
  cufIds.forEach((id) => {
    createDialog.assertCustomFieldErrorExist(id, 'sqtm-core.validation.errors.required');
  });
}

/*
 * From the workspace page, select a row in the tree and from the creation menu, select the "Add new test case" command.
 * It will then ensure the dialog is visible and return the dialog element for further testing.
 */
function navigateToAddTestCaseDialog(
  workspacePage: TestCaseWorkspacePage,
  tree: TreeElement,
  rowIdToAddTo: Identifier,
): CreateTestCaseDialog {
  const firstModel: TestCaseLibraryModel = mockTestCaseLibraryModel({
    attachmentList: {
      id: 1,
      attachments: [],
    },
    customFieldValues: [],
    description: '',
    id: 1,
    name: 'Project1',
    projectId: 1,
    statistics: {
      statusesStatistics: null,
      sizeStatistics: null,
      selectedIds: [],
      importanceStatistics: null,
      generatedOn: new Date(),
      boundRequirementsStatistics: null,
    },
  });

  // workspacePage.treeMenu.assertDialogDisabled(TestCaseMenuItemIds.NEW_TEST_CASE);
  tree.selectNode(rowIdToAddTo, firstModel);

  const projectData = {
    id: 1,
    customFieldBinding: {
      TEST_CASE: [
        {
          customField: createEntityReferentialData.customFields[0],
          bindableEntity: BindableEntity.TEST_CASE,
          boundProjectId: 1,
          id: 1,
          position: 0,
          renderingLocations: [],
        },
        {
          customField: createEntityReferentialData.customFields[2],
          bindableEntity: BindableEntity.TEST_CASE,
          boundProjectId: 1,
          id: 3,
          position: 2,
          renderingLocations: [],
        },
        {
          customField: createEntityReferentialData.customFields[1],
          bindableEntity: BindableEntity.TEST_CASE,
          boundProjectId: 1,
          id: 2,
          position: 1,
          renderingLocations: [],
        },
        {
          customField: createEntityReferentialData.customFields[3],
          bindableEntity: BindableEntity.TEST_CASE,
          boundProjectId: 1,
          id: 4,
          position: 3,
          renderingLocations: [],
        },
        {
          customField: createEntityReferentialData.customFields[4],
          bindableEntity: BindableEntity.TEST_CASE,
          boundProjectId: 1,
          id: 5,
          position: 4,
          renderingLocations: [],
        },
      ],
    },
  } as ProjectData;

  const createDialog: CreateTestCaseDialog = workspacePage.treeMenu.openCreateTestCase(projectData);
  createDialog.assertExists();

  return createDialog;
}

/*
 * From the create test case dialog, fill the form and create a new test case, ensuring the provided infos are
 * reported to the corresponding view page.
 */
function addTestCaseAndCheckViewPage(
  createDialog: CreateTestCaseDialog,
  mockData: TestCaseMockData,
  tree: TreeElement,
  parentId: string,
  addAnother: boolean,
  parentRowIsClosed: boolean,
  openNodeResponse: DataRowModel[],
  testCaseKind: string = 'Classique',
): Chainable<any> {
  // We assume the creation dialog is already shown so we can fill the form
  createDialog.fillName(mockData.testCaseModel.name);
  createDialog.fillReference(mockData.testCaseModel.reference);
  createDialog.fillDescription(mockData.testCaseModel.description);
  createDialog.changeTestCaseKind(testCaseKind);
  // Do the button click
  return createDialog
    .addWithOptions({
      addAnother,
      addedId: mockData.testCaseId,
      children: createOpenNodeResponse(openNodeResponse),
      createResponse: mockData.addedDataRow,
      entityModel: mockData.testCaseModel,
      parentRowIsClosed,
      parentRowRef: parentId,
    })
    .then(() => {
      // Check the dialog visibility based on which button was pressed
      if (addAnother) {
        createDialog.assertExists();
      } else {
        createDialog.assertNotExist();
      }

      // Check node existence in the tree
      tree.assertNodeExist(mockData.addedDataRow.id);

      // Check the fields in the displayed view page
      const page = new TestCaseViewPage(mockData.testCaseId);
      page.assertExists();
      page.assertReferenceContains(mockData.testCaseModel.reference);
      page.assertNameContains(mockData.testCaseModel.name);
      page.checkData('test-case-kind', testCaseKind);
    });
}

// Shortcut to create a GridResponse based on a DataRow array
function createOpenNodeResponse(rows: DataRowModel[]): GridResponse {
  return {
    count: rows.length,
    dataRows: rows,
  };
}

/*
 * Small interface only used as a return type for createTestCaseMockData to hold together a TestCaseModel and the
 * corresponding DataRow when creating a new test Case.
 */
interface TestCaseMockData {
  testCaseId: number;
  testCaseModel: TestCaseModel;
  addedDataRow: DataRowModel;
}

/*
 * Utility function to generate a test case model and the matching data row.
 */
function createTestCaseMockData(
  testCaseId: number,
  name: string,
  reference: string,
  description: string,
  projectId: number,
  parentId: string,
  kind: TestCaseKindKeys = 'STANDARD',
): TestCaseMockData {
  const testCaseModel: TestCaseModel = mockTestCaseModel({
    id: testCaseId,
    projectId,
    customFieldValues: [],
    attachmentList: {
      id: 1,
      attachments: [],
    },
    name,
    reference,
    importance: 'LOW',
    description,
    status: 'WORK_IN_PROGRESS',
    nature: 12,
    type: 20,
    importanceAuto: false,
    automatable: 'M',
    prerequisite: '',
    testSteps: [],
    milestones: [],
    automationRequest: null,
    parameters: [],
    datasets: [],
    datasetParamValues: [],
    coverages: [],
    uuid: '44d63d7e-11dd-44b0-b584-565b6f791fa2',
    kind: kind,
    executions: [],
    nbIssues: 0,
    calledTestCases: [],
    lastModifiedOn: null,
    lastModifiedBy: '',
    createdBy: 'cypress',
    lastExecutionStatus: 'SUCCESS',
    script: '',
  });

  const addedDataRow = mockTreeNode({
    id: `TestCase-${testCaseId}`,
    projectId: projectId,
    children: [],
    state: DataRowOpenState.leaf,
    parentRowId: parentId,
    data: {
      TCLN_ID: testCaseId,
      NAME: name,
      projectId: projectId,
      IMPORTANCE: 'LOW',
      REFERENCE: reference,
      TC_STATUS: 'WORK_IN_PROGRESS',
      TC_KIND: kind,
      STEP_COUNT: 0,
    },
  });

  return {
    testCaseId,
    testCaseModel,
    addedDataRow,
  };
}

function navigateToAddTestCaseFolderDialog(
  workspacePage: TestCaseWorkspacePage,
  tree: TreeElement,
  rowIdToAddTo: Identifier,
): CreateTestCaseFolderDialog {
  const firstModel: TestCaseLibraryModel = mockTestCaseLibraryModel({
    attachmentList: {
      id: 1,
      attachments: [],
    },
    customFieldValues: [],
    description: '',
    id: 1,
    name: 'Project1',
    projectId: 1,
    statistics: {
      boundRequirementsStatistics: null,
      generatedOn: new Date(),
      importanceStatistics: null,
      selectedIds: [],
      sizeStatistics: null,
      statusesStatistics: null,
    },
  });

  tree.selectNode(rowIdToAddTo, firstModel);

  const projectData = {
    id: 1,
    customFieldBinding: {
      TESTCASE_FOLDER: [],
    },
  } as ProjectData;

  const createDialog: CreateTestCaseFolderDialog =
    workspacePage.treeMenu.openCreateFolder(projectData);
  createDialog.assertExists();

  return createDialog;
}

interface TestCaseFolderMockData {
  testCaseFolderId: number;
  testCaseFolderModel: TestCaseFolderModel;
  addedDataRow: DataRowModel;
}

function addTestCaseFolderAndCheckViewPage(
  createDialog: CreateTestCaseFolderDialog,
  mockData: TestCaseFolderMockData,
  tree: TreeElement,
  parentId: string,
  addAnother: boolean,
  parentRowIsClosed: boolean,
  openNodeResponse: DataRowModel[],
): Chainable<any> {
  // We assume the creation dialog is already shown so we can fill the form
  createDialog.fillName(mockData.testCaseFolderModel.name);
  createDialog.fillDescription(mockData.testCaseFolderModel.description);

  // Do the button click
  return createDialog
    .addWithOptions({
      addAnother,
      addedId: mockData.testCaseFolderId,
      children: createOpenNodeResponse(openNodeResponse),
      createResponse: mockData.addedDataRow,
      entityModel: mockData.testCaseFolderModel,
      parentRowIsClosed,
      parentRowRef: parentId,
    })
    .then(() => {
      // Check the dialog visibility based on which button was pressed
      if (addAnother) {
        createDialog.assertExists();
      } else {
        createDialog.assertNotExist();
      }

      // Check node existence in the tree
      tree.assertNodeExist(mockData.addedDataRow.id);

      // Check the fields in the displayed view page
      const page = new TestCaseFolderViewPage(mockData.testCaseFolderId);
      page.assertExists();
      // page.checkStatus();
      page.assertNameContains(mockData.testCaseFolderModel.name);
    });
}

function createTestCaseFolderMockData(
  testCaseFolderId: number,
  name: string,
  description: string,
  projectId: number,
  parentId: string,
): TestCaseFolderMockData {
  const testCaseFolderModel: TestCaseFolderModel = mockTestCaseFolderModel({
    id: testCaseFolderId,
    projectId,
    customFieldValues: [],
    attachmentList: {
      id: 1,
      attachments: [],
    },
    name,
    description,
    statistics: {
      boundRequirementsStatistics: null,
      generatedOn: new Date(),
      importanceStatistics: null,
      selectedIds: [],
      sizeStatistics: null,
      statusesStatistics: null,
    },
  });

  const addedDataRow = mockTreeNode({
    id: `TestCaseFolder-${testCaseFolderId}`,
    projectId: projectId,
    children: [],
    state: DataRowOpenState.leaf,
    parentRowId: parentId,
    data: {
      TCLN_ID: testCaseFolderId,
      NAME: name,
      projectId: projectId,
    },
  });

  return {
    testCaseFolderId,
    testCaseFolderModel,
    addedDataRow,
  };
}

function refreshNodeAfterOpen(library: DataRowModel, children: Identifier[] = []): DataRowModel {
  return { ...library, state: DataRowOpenState.open, children: [...library.children, ...children] };
}
