import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';

describe('TestCase Workspace Tree Sort', function () {
  const initialNodes = mockGridResponse('id', [
    mockTreeNode({
      id: 'TestCaseLibrary-1',
      children: [],
      data: { NAME: 'Project1', CHILD_COUNT: '3' },
    }),
  ]);

  it('should sort tree', () => {
    const childNodes = [
      mockTreeNode({
        id: 'TestCaseLibrary-1',
        children: ['TestCaseFolder-1', 'TestCase-3', 'TestCaseFolder-2'],
        data: { NAME: 'Project1', CHILD_COUNT: '3' },
        state: DataRowOpenState.open,
      }),
      mockTreeNode({
        id: 'TestCaseFolder-1',
        children: [],
        parentRowId: 'TestCaseLibrary-1',
        data: { NAME: 'folder1' },
      }),
      mockTreeNode({
        id: 'TestCase-3',
        children: [],
        parentRowId: 'TestCaseLibrary-1',
        data: {
          NAME: 'a nice test',
          TC_KIND: 'STANDARD',
          TC_STATUS: 'APPROVED',
          IMPORTANCE: 'HIGH',
        },
      }),
      mockTreeNode({
        id: 'TestCaseFolder-2',
        children: [],
        parentRowId: 'TestCaseLibrary-1',
        data: { NAME: 'folder2' },
      }),
    ];
    const firstNode = initialNodes.dataRows[0];
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes);
    const tree = testCaseWorkspacePage.tree;
    tree.openNode(firstNode.id, childNodes);
    tree.assertNodeIsOpen(firstNode.id);
    tree.assertNodeOrderByName(['Project1', 'a nice test', 'folder1', 'folder2']);
    testCaseWorkspacePage.treeMenu.sortTreePositional();
    tree.assertNodeOrderByName(['Project1', 'folder1', 'a nice test', 'folder2']);
    testCaseWorkspacePage.treeMenu.sortTreeAlphabetical();
    tree.assertNodeOrderByName(['Project1', 'a nice test', 'folder1', 'folder2']);
  });

  it('should persist sort', () => {
    const childNodes = [
      mockTreeNode({
        id: 'TestCaseLibrary-1',
        children: ['TestCaseFolder-1', 'TestCase-3', 'TestCaseFolder-2'],
        data: { NAME: 'Project1', CHILD_COUNT: '3' },
        state: DataRowOpenState.open,
      }),
      mockTreeNode({
        id: 'TestCaseFolder-1',
        children: [],
        parentRowId: 'TestCaseLibrary-1',
        data: { NAME: 'folder1' },
      }),
      mockTreeNode({
        id: 'TestCase-3',
        children: [],
        parentRowId: 'TestCaseLibrary-1',
        data: {
          NAME: 'a nice test',
          TC_KIND: 'STANDARD',
          TC_STATUS: 'APPROVED',
          IMPORTANCE: 'HIGH',
        },
      }),
      mockTreeNode({
        id: 'TestCaseFolder-2',
        children: [],
        parentRowId: 'TestCaseLibrary-1',
        data: { NAME: 'folder2' },
      }),
    ];
    const firstNode = initialNodes.dataRows[0];
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes);
    const tree = testCaseWorkspacePage.tree;
    tree.openNode(firstNode.id, childNodes);
    tree.assertNodeIsOpen(firstNode.id);
    tree.assertNodeOrderByName(['Project1', 'a nice test', 'folder1', 'folder2']);
    testCaseWorkspacePage.treeMenu.sortTreePositional();
    tree.assertNodeOrderByName(['Project1', 'folder1', 'a nice test', 'folder2']);
    NavBarElement.navigateToCampaignWorkspace(mockGridResponse('id', []));
    NavBarElement.navigateToTestCaseWorkspace(initialNodes);
    tree.openNode(firstNode.id, childNodes);
    tree.assertNodeOrderByName(['Project1', 'folder1', 'a nice test', 'folder2']);
  });
});
