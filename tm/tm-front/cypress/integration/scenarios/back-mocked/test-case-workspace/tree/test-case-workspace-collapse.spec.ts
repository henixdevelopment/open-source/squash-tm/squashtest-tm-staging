import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';

describe('TestCase Workspace Tree Collapse', function () {
  const initialNodes = mockGridResponse('id', [
    mockTreeNode({
      id: 'TestCaseLibrary-1',
      projectId: 1,
      data: { NAME: 'Project1', CHILD_COUNT: '3' },
      children: ['TestCaseFolder-1', 'TestCaseFolder-2'],
      state: DataRowOpenState.closed,
    }),
    mockTreeNode({
      id: 'TestCaseLibrary-2',
      projectId: 2,
      data: { NAME: 'Project2', CHILD_COUNT: '3' },
      children: ['TestCaseFolder-5', 'TestCaseFolder-7'],
      state: DataRowOpenState.closed,
    }),
  ]);

  const firstNodeChildNodes = [
    mockTreeNode({
      id: 'TestCaseLibrary-1',
      children: ['TestCaseFolder-1', 'TestCaseFolder-2'],
      data: { NAME: 'Project1', CHILD_COUNT: '3' },
      state: DataRowOpenState.open,
    }),
    mockTreeNode({
      id: 'TestCaseFolder-1',
      children: ['TestCase-3'],
      parentRowId: 'TestCaseLibrary-1',
      data: { NAME: 'folder1' },
      state: DataRowOpenState.open,
    }),
    mockTreeNode({
      id: 'TestCase-3',
      children: [],
      parentRowId: 'TestCaseFolder-1',
      data: { NAME: 'a nice test', TC_KIND: 'STANDARD', TC_STATUS: 'APPROVED', IMPORTANCE: 'LOW' },
    }),
    mockTreeNode({
      id: 'TestCaseFolder-2',
      children: [],
      parentRowId: 'TestCaseLibrary-1',
      data: { NAME: 'folder2' },
    }),
  ];

  const secondNodeChildNodes = [
    mockTreeNode({
      id: 'TestCaseLibrary-2',
      children: ['TestCaseFolder-5', 'TestCaseFolder-7'],
      data: { NAME: 'Project1', CHILD_COUNT: '3' },
      state: DataRowOpenState.open,
    }),
    mockTreeNode({
      id: 'TestCaseFolder-5',
      children: ['TestCase-8'],
      parentRowId: 'TestCaseLibrary-2',
      data: { NAME: 'folder5' },
      state: DataRowOpenState.open,
    }),
    mockTreeNode({
      id: 'TestCase-8',
      children: [],
      parentRowId: 'TestCaseFolder-5',
      data: {
        NAME: 'a very nice test',
        TC_KIND: 'STANDARD',
        TC_STATUS: 'APPROVED',
        IMPORTANCE: 'LOW',
      },
    }),
    mockTreeNode({
      id: 'TestCaseFolder-7',
      children: [],
      parentRowId: 'TestCaseLibrary-2',
      data: { NAME: 'folder7' },
    }),
  ];

  it('should collapse tree and ensure it is still collapsed after navigation', () => {
    const firstNode = initialNodes.dataRows[0];
    const secondNode = initialNodes.dataRows[1];
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes);
    const tree = testCaseWorkspacePage.tree;
    tree.openNode(firstNode.id, firstNodeChildNodes);
    tree.openNode(secondNode.id, secondNodeChildNodes);

    testCaseWorkspacePage.treeMenu.checkCollapseButtonActive();
    testCaseWorkspacePage.treeMenu.collapseAllNodesInTree();
    tree.assertNodeIsClosed(firstNode.id);
    tree.assertNodeIsClosed(secondNode.id);
    testCaseWorkspacePage.treeMenu.checkCollapseButtonDisabled();

    NavBarElement.navigateToCampaignWorkspace(mockGridResponse('id', []));
    NavBarElement.navigateToTestCaseWorkspace(initialNodes);
    tree.assertNodeIsClosed(firstNode.id);
    tree.assertNodeIsClosed(secondNode.id);
    testCaseWorkspacePage.treeMenu.checkCollapseButtonDisabled();

    tree.openNode(firstNode.id, firstNodeChildNodes);
    testCaseWorkspacePage.treeMenu.checkCollapseButtonActive();
  });
});
