import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {
  ALL_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder,
} from '../../../../utils/referential/referential-data-builder';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { mockTestCaseFolderModel } from '../../../../data-mock/test-case.data-mock';
import { DataRowOpenState } from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

describe('TestCase Workspace Tree Move', function () {
  function referentialData() {
    return new ReferentialDataMockBuilder()
      .withProjects(
        {
          name: 'Project 1',
          permissions: ALL_PROJECT_PERMISSIONS,
        },
        {
          name: 'Project 2',
          permissions: ALL_PROJECT_PERMISSIONS,
        },
      )
      .build();
  }

  const initialNodes = mockGridResponse('id', [
    mockTreeNode({
      id: 'TestCaseLibrary-1',
      projectId: 1,
      children: [],
      data: { NAME: 'Project 1', CHILD_COUNT: '3' },
    }),
    mockTreeNode({
      id: 'TestCaseLibrary-2',
      projectId: 2,
      children: [],
      data: { NAME: 'Project 2', CHILD_COUNT: '0' },
    }),
  ]);

  const tcl1 = mockTreeNode({
    id: 'TestCaseLibrary-1',
    projectId: 1,
    children: ['TestCaseFolder-1', 'TestCase-3', 'TestCaseFolder-2'],
    data: { NAME: 'Project1', CHILD_COUNT: '3' },
    state: DataRowOpenState.open,
  });

  const tcf1 = mockTreeNode({
    id: 'TestCaseFolder-1',
    children: [],
    projectId: 1,
    parentRowId: 'TestCaseLibrary-1',
    data: { NAME: 'Folder One' },
  });

  const tc3 = mockTreeNode({
    id: 'TestCase-3',
    children: [],
    projectId: 1,
    parentRowId: 'TestCaseLibrary-1',
    data: { NAME: 'a nice test', TC_KIND: 'STANDARD', TC_STATUS: 'APPROVED', IMPORTANCE: 'HIGH' },
  });

  const tcf2 = mockTreeNode({
    id: 'TestCaseFolder-2',
    children: [],
    projectId: 1,
    parentRowId: 'TestCaseLibrary-1',
    data: { NAME: 'folder2' },
  });

  const libraryRefreshAtOpen = [tcl1, tcf1, tc3, tcf2];

  it('should init move and show target according to destination', () => {
    const firstNode = initialNodes.dataRows[0];
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes);
    new NavBarElement().toggle();
    testCaseWorkspacePage.treeMenu.sortTreePositional();
    const tree = testCaseWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.beginDragAndDrop('TestCaseFolder-1');
    tree.assertNodeNotExist('TestCaseFolder-1');
    tree.dragOverTopPart('TestCaseFolder-2');
    tree.assertDragAndDropTargetIsVisible(2);
    tree.dragOverCenter('TestCaseFolder-2');
    tree.assertContainerIsDndTarget('TestCaseFolder-2');
    tree.dragOverBottomPart('TestCaseFolder-2');
    tree.assertDragAndDropTargetIsVisible(3);
    tree.cancelDnd();
    tree.assertNodeExist('TestCaseFolder-1');
  });

  it('should drop into folder', () => {
    const firstNode = initialNodes.dataRows[0];
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes);
    new NavBarElement().toggle();
    testCaseWorkspacePage.treeMenu.sortTreePositional();
    const tree = testCaseWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.beginDragAndDrop('TestCaseFolder-1');
    tree.assertNodeNotExist('TestCaseFolder-1');
    tree.dragOverCenter('TestCaseFolder-2');
    tree.assertContainerIsDndTarget('TestCaseFolder-2');
    const refreshedRows = [
      { ...tcl1, children: ['TestCase-3', 'TestCaseFolder-2'] },
      { ...tcf1, parentRowId: 'TestCaseFolder-2' },
      { ...tc3 },
      { ...tcf2, children: ['TestCaseFolder-1'], state: DataRowOpenState.open },
    ];
    tree.drop('TestCaseFolder-2', 'TestCaseLibrary-1,TestCaseFolder-2', refreshedRows);
    tree.assertRowHasParent('TestCaseFolder-1', 'TestCaseFolder-2');
  });

  it('should suspend drag and resume when coming back into tree', () => {
    const firstNode = initialNodes.dataRows[0];
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes);
    new NavBarElement().toggle();
    testCaseWorkspacePage.treeMenu.sortTreePositional();
    const tree = testCaseWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.beginDragAndDrop('TestCaseFolder-1');
    tree.assertNodeNotExist('TestCaseFolder-1');
    tree.dragOverTopPart('TestCaseFolder-2');
    tree.assertDragAndDropTargetIsVisible(2);
    tree.suspendDnd();
    tree.assertDragAndDropTargetIsNotVisible();
    tree.assertNodeExist('TestCaseFolder-1');
    tree.dragOverCenter('TestCaseFolder-2');
    tree.assertContainerIsDndTarget('TestCaseFolder-2');
    tree.assertNodeNotExist('TestCaseFolder-1');
    tree.suspendDnd();
    tree.assertNodeExist('TestCaseFolder-1');
    tree.assertContainerIsNotDndTarget('TestCaseFolder-2');
  });

  it('should show rows when dragging multiple rows', () => {
    const firstNode = initialNodes.dataRows[0];
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes);
    new NavBarElement().toggle();
    testCaseWorkspacePage.treeMenu.sortTreePositional();
    const tree = testCaseWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.selectNode('TestCase-3');
    tree.addNodeToSelection('TestCaseFolder-1', mockTestCaseFolderModel());
    tree.beginDragAndDrop('TestCase-3');
    tree.assertNodeNotExist('TestCase-3');
    tree.assertNodeNotExist('TestCaseFolder-1');
    tree.assertDndPlaceholderContains('TestCase-3', 'a nice test');
    tree.assertDndPlaceholderContains('TestCaseFolder-1', 'Folder One');
  });

  it('should show warning when dropping into another project', () => {
    const firstNode = initialNodes.dataRows[0];
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(
      initialNodes,
      referentialData(),
    );
    new NavBarElement().toggle();
    testCaseWorkspacePage.treeMenu.sortTreePositional();
    const tree = testCaseWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.beginDragAndDrop('TestCase-3');
    tree.dragOverCenter('TestCaseLibrary-2');
    const confirmInterProjectMove = tree.dropIntoOtherProject('TestCaseLibrary-2');
    confirmInterProjectMove.assertExists();
    confirmInterProjectMove.cancel();
    tree.assertNodeExist('TestCase-3');
    tree.assertRowHasParent('TestCase-3', 'TestCaseLibrary-1');
  });
});
