import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { ReferentialDataMockBuilder } from '../../../../utils/referential/referential-data-builder';
import { TestCaseXlsReport } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/import/import-test-case.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { mockDataRow } from '../../../../data-mock/grid.data-mock';

function buildReferentialData() {
  return new ReferentialDataMockBuilder()
    .withUser({
      userId: 1,
      admin: true,
      username: 'admin',
    })
    .withProjects(
      {
        name: 'Apollo',
        label: 'Apollo',
      },
      {
        name: 'Gemini',
        label: 'Gemini',
      },
    )
    .build();
}

describe('Test Case Import', function () {
  it('should import test case with xls file', () => {
    const testCaseWorkspacePage: TestCaseWorkspacePage = navigateToTestCaseWorkspace();
    testCaseWorkspacePage.navBar.toggle();
    const importDialog = testCaseWorkspacePage.treeMenu.openImportTestCaseDialog();
    importDialog.chooseImportFile(
      'test_import.xls',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    );
    importDialog.clickImport();
    importDialog.checkConfirmationMessage('test_import.xls');

    const xlsReport: TestCaseXlsReport = {
      templateOk: {
        reportUrl: 'test-cases/import/test_case_200000.xls',
        coverageFailures: 0,
        coverageSuccesses: 0,
        coverageWarnings: 0,
        datasetFailures: 0,
        datasetSuccesses: 0,
        datasetWarnings: 0,
        parameterFailures: 0,
        parameterSuccesses: 0,
        parameterWarnings: 0,
        testCaseFailures: 0,
        testCaseSuccesses: 1,
        testCaseWarnings: 0,
        testStepFailures: 0,
        testStepSuccesses: 0,
        testStepWarnings: 0,
      },
      importFormatFailure: null,
    };

    importDialog.confirmXlsImport({ dataRows: [] } as GridResponse, xlsReport);
    importDialog.assertReportExist('xls-report-ok');
    importDialog.assertButtonExist('close');
  });

  it('should display Error Import report', () => {
    const testCaseWorkspacePage: TestCaseWorkspacePage = navigateToTestCaseWorkspace();
    testCaseWorkspacePage.navBar.toggle();
    const importDialog = testCaseWorkspacePage.treeMenu.openImportTestCaseDialog();
    importDialog.chooseImportFile(
      'test_import.xls',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    );
    importDialog.clickImport();
    importDialog.checkConfirmationMessage('test_import.xls');

    const xlsReport: TestCaseXlsReport = {
      templateOk: null,
      importFormatFailure: {
        duplicateColumns: ['TEST_CASE_NAME'],
        actionValidationError: null,
        missingMandatoryColumns: ['PROJECT_NAME'],
      },
    };

    importDialog.confirmXlsImport({ dataRows: [] } as GridResponse, xlsReport);
    importDialog.assertReportExist('xls-report-ko');
    importDialog.assertButtonExist('cancel');
  });

  function navigateToTestCaseWorkspace(): TestCaseWorkspacePage {
    const referentialDataMock = buildReferentialData();

    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [
        mockDataRow({
          id: 'TestCaseLibrary-1',
          children: [],
          data: { NAME: 'Project 1', MILESTONES: [1, 2] },
        }),
      ],
    };
    return TestCaseWorkspacePage.initTestAtPage(initialNodes, referentialDataMock);
  }
});
