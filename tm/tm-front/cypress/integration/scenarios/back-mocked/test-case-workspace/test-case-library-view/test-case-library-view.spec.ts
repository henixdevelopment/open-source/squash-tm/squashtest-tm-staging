import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { defaultReferentialData } from '../../../../utils/referential/default-referential-data.const';
import { TestCaseLibraryViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-library-view.page';
import { getFavoriteDashboard, getStatistics } from '../test-case-workspace-mock-data';
import { TestCaseLibraryModel } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/test-case-library/test-case-library.model';
import { TestCaseStatistics } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/test-case-statistics.model';
import { CustomDashboardModel } from '../../../../../../projects/sqtm-core/src/lib/model/custom-report/custom-dashboard.model';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

const description = `<p>a nice description</p>
<table><tbody><tr><th>Col1</th><th>Col2</th></tr><tr><td>Col1L1</td><td>Col2L1</td></tr></tbody></table>`;

describe('Test Case Library View', function () {
  it('should display test case library page and information', () => {
    const tcFolderPage = navigateToTestCaseLibrary();
    tcFolderPage.assertExists();
    const informationPanel = tcFolderPage.showInformationPanel();
    informationPanel.descriptionRichField.checkHtmlContent(description);
  });

  it('should display test case library dashboard', () => {
    const tcFolderPage = navigateToTestCaseLibrary();
    tcFolderPage.assertExists();
    const dashboardPanel = tcFolderPage.showDashboard();
    dashboardPanel.assertTitleExist('Tableau de bord');
    dashboardPanel.coverageChart.assertChartExist();
    dashboardPanel.coverageChart.assertHasTitle('Associations aux exigences');
    dashboardPanel.statusChart.assertChartExist();
    dashboardPanel.statusChart.assertHasTitle('Statut');
    dashboardPanel.importanceChart.assertChartExist();
    dashboardPanel.importanceChart.assertHasTitle('Importance');
    dashboardPanel.sizeChart.assertChartExist();
    dashboardPanel.sizeChart.assertHasTitle('Pas de test');
    dashboardPanel.assertFooterContains('Total des cas de test : 6');
  });

  it('should refresh test case library dashboard', () => {
    const tcLibraryPage = navigateToTestCaseLibrary();
    tcLibraryPage.assertExists();
    const dashboardPanel = tcLibraryPage.showDashboard();
    dashboardPanel.assertFooterContains('Total des cas de test : 6');
    const updatedStatistics: TestCaseStatistics = {
      ...getStatistics(),
      selectedIds: [4, 5, 7, 9, 12, 13, 48, 78],
    };
    dashboardPanel.refreshStatistics(updatedStatistics);
    dashboardPanel.assertFooterContains('Total des cas de test : 8');
  });

  it('should display test case library favorite dashboard', () => {
    const tcLibraryPage = navigateToTestCaseLibraryWithFavoriteDashboard();
    tcLibraryPage.assertExists();
    const dashboardPanel = tcLibraryPage.showDashboard();
    dashboardPanel.assertTitleExist('Favorite Dashboard');
    dashboardPanel.assertCustomDashboardExist();
  });

  function navigateToTestCaseLibrary(): TestCaseLibraryViewPage {
    const initialNodes = mockGridResponse('id', [
      mockTreeNode({
        id: 'TestCaseLibrary-1',
        children: [],
        data: { NAME: 'Project1' },
      }),
    ]);
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(
      initialNodes,
      defaultReferentialData,
    );

    const model: TestCaseLibraryModel = createTestCaseLibraryModel(getStatistics(), null);
    return testCaseWorkspacePage.tree.selectNode<TestCaseLibraryViewPage>(
      'TestCaseLibrary-1',
      model,
    );
  }

  function navigateToTestCaseLibraryWithFavoriteDashboard(): TestCaseLibraryViewPage {
    const initialNodes = mockGridResponse('id', [
      mockTreeNode({
        id: 'TestCaseLibrary-1',
        children: [],
        data: { NAME: 'Project1' },
      }),
    ]);
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(
      initialNodes,
      defaultReferentialData,
    );

    const model: TestCaseLibraryModel = createTestCaseLibraryModel(null, getFavoriteDashboard());
    return testCaseWorkspacePage.tree.selectNode<TestCaseLibraryViewPage>(
      'TestCaseLibrary-1',
      model,
    );
  }

  function createTestCaseLibraryModel(
    statistics: TestCaseStatistics,
    dashboard: CustomDashboardModel,
  ): TestCaseLibraryModel {
    return {
      id: 3,
      projectId: 1,
      name: 'Test Case Library 1',
      customFieldValues: [],
      attachmentList: {
        id: 1,
        attachments: [],
      },
      description,
      statistics: statistics,
      dashboard: dashboard,
      canShowFavoriteDashboard: Boolean(dashboard),
      favoriteDashboardId: 1,
      shouldShowFavoriteDashboard: Boolean(dashboard),
    };
  }
});
