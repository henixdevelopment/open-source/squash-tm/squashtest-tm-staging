import { createEntityReferentialData } from '../../../../utils/referential/create-entity-referential.const';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { HttpMockBuilder } from '../../../../utils/mocks/request-mock';
import { TestCaseForCoverageSearchPage } from '../../../../page-objects/pages/test-case-workspace/research/test-case-for-coverage-search-page';
import { AlertDialogElement } from '../../../../page-objects/elements/dialog/alert-dialog.element';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { mockDataRow } from '../../../../data-mock/grid.data-mock';

function assertRedirectionToRequirementWorkspaceDone() {
  cy.url().should('equal', `${Cypress.config().baseUrl}/requirement-workspace`);
}

describe('Test Case for coverage search page', function () {
  beforeEach(() => {
    cy.viewport(1200, 720);
  });

  describe('Test Case search table', () => {
    const testCase = mockDataRow({
      id: '1',
      type: 'TestCase',
      projectId: 1,
      data: {
        [GridColumnId.name]: 'Test 1',
        [GridColumnId.id]: 1,
        [GridColumnId.reference]: 'ref1',
        [GridColumnId.projectName]: 'project 1',
        [GridColumnId.attachments]: 0,
        [GridColumnId.items]: 0,
        [GridColumnId.steps]: 0,
        [GridColumnId.nature]: 14,
        [GridColumnId.type]: 16,
        [GridColumnId.automatable]: 'Y',
        [GridColumnId.status]: 'WORK_IN_PROGRESS',
        [GridColumnId.importance]: 'LOW',
        [GridColumnId.createdBy]: 'admin',
        [GridColumnId.lastModifiedBy]: 'admin',
        [GridColumnId.tcMilestoneLocked]: 0,
        [GridColumnId.reqMilestoneLocked]: 0,
      },
    });

    it('should show links buttons and activate according to user selection', () => {
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: [testCase],
      };
      const forCoverageSearchPage = TestCaseForCoverageSearchPage.initTestAtPage(
        '1',
        createEntityReferentialData,
        gridResponse,
      );
      const gridElement = forCoverageSearchPage.grid;
      new NavBarElement().toggle();
      forCoverageSearchPage.foldFilterPanel();
      forCoverageSearchPage.assertLinkSelectionButtonExist();
      forCoverageSearchPage.assertLinkSelectionButtonIsNotActive();
      forCoverageSearchPage.assertLinkAllButtonExist();
      forCoverageSearchPage.assertLinkAllButtonIsActive();
      forCoverageSearchPage.assertNavigateBackButtonExist();
      forCoverageSearchPage.assertNavigateBackButtonIsActive();
      gridElement.selectRow('1', '#', 'leftViewport');
      forCoverageSearchPage.assertLinkSelectionButtonIsActive();
      gridElement.toggleRow('1', '#', 'leftViewport');
      forCoverageSearchPage.assertLinkSelectionButtonIsNotActive();
    });

    it('should link a test Case', () => {
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: [testCase],
      };
      const forCoverageSearchPage = TestCaseForCoverageSearchPage.initTestAtPage(
        '1',
        createEntityReferentialData,
        gridResponse,
      );
      const gridElement = forCoverageSearchPage.grid;
      new NavBarElement().toggle();
      gridElement.selectRow('1', '#', 'leftViewport');
      const testCaseTreeMock = buildRequirementTreeMock();
      forCoverageSearchPage.linkSelection('1');
      testCaseTreeMock.wait();
      assertRedirectionToRequirementWorkspaceDone();
    });

    it('should link all test cases', () => {
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: [testCase],
      };
      const forCoverageSearchPage = TestCaseForCoverageSearchPage.initTestAtPage(
        '1',
        createEntityReferentialData,
        gridResponse,
      );
      new NavBarElement().toggle();
      const testCaseTreeMock = buildRequirementTreeMock();
      forCoverageSearchPage.linkAll('1');
      testCaseTreeMock.wait();
      assertRedirectionToRequirementWorkspaceDone();
    });

    it('should link a test case and show error', () => {
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: [testCase],
      };
      const forCoverageSearchPage = TestCaseForCoverageSearchPage.initTestAtPage(
        '1',
        createEntityReferentialData,
        gridResponse,
      );
      const gridElement = forCoverageSearchPage.grid;
      new NavBarElement().toggle();
      gridElement.selectRow('1', '#', 'leftViewport');
      forCoverageSearchPage.linkSelection('1', {
        verifyingTestCases: [],
        summary: {
          alreadyVerifiedRejections: true,
          notLinkableRejections: true,
          noVerifiableVersionRejections: true,
        },
        nbIssues: 0,
        requirementStats: null,
      });
      const alertDialogElement = new AlertDialogElement('coverage-report');
      alertDialogElement.assertExists();
      alertDialogElement.assertHasMessage(
        'Au moins un des cas de test sélectionné est déjà associé à cette version ou à une autre version de cette' +
          " exigence. Ce cas de test n'a pas été associé à cette version.",
      );
      alertDialogElement.assertHasMessage(
        "Au moins un des cas de test sélectionnés n'a pas été ajouté à l'exigence car son statut ne le permet pas.",
      );
      alertDialogElement.close();
    });
  });
});

function buildRequirementTreeMock() {
  const requirementTree: GridResponse = { count: 0, dataRows: [] };
  return new HttpMockBuilder(`/requirement-tree`).post().responseBody(requirementTree).build();
}
