import { TestCaseSearchPage } from '../../../../page-objects/pages/test-case-workspace/research/test-case-search-page';
import {
  ALL_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder,
} from '../../../../utils/referential/referential-data-builder';
import { GroupedMultiListElement } from '../../../../page-objects/elements/filters/grouped-multi-list.element';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import {
  DataRowModel,
  DataRowOpenState,
} from '../../../../../../projects/sqtm-core/src/lib/model/grids/data-row.model';
import { SimpleScope } from '../../../../../../projects/sqtm-core/src/lib/model/filter/filter.model';
import { mockGridResponse, mockTreeNode } from '../../../../data-mock/grid.data-mock';

const CUSTOM_FIELD_GROUP_LABEL = 'Champs personnalisés';

function referentialDataWithCustomInfoLists() {
  return new ReferentialDataMockBuilder()
    .withProjects(
      {
        name: 'Project 1',
        permissions: ALL_PROJECT_PERMISSIONS,
      },
      {
        name: 'Project 2',
        permissions: ALL_PROJECT_PERMISSIONS,
      },
      {
        name: 'Project 3',
        permissions: ALL_PROJECT_PERMISSIONS,
      },
    )
    .withInfoLists(
      {
        label: 'Space Shuttles',
        items: [
          { label: 'Atlantys' },
          { label: 'Endeavour' },
          { label: 'Discovery' },
          { label: 'Columbia' },
          { label: 'Challenger' },
        ],
        boundToProject: [{ projectIndex: 0, role: 'testCaseNature' }],
      },
      {
        label: 'Rockets',
        items: [{ label: 'Saturn 5' }, { label: 'Proton' }, { label: 'Ariane 6' }],
        boundToProject: [{ projectIndex: 1, role: 'testCaseNature' }],
      },
      {
        label: 'Satellites',
        items: [{ label: 'Diapason' }, { label: 'Pollux' }, { label: 'Polaire' }],
        boundToProject: [{ projectIndex: 0, role: 'testCaseType' }],
      },
    )
    .withCustomFields(
      {
        code: 'MISSION_TAG',
        inputType: InputType.TAG,
        label: 'Objectif de mission',
        name: 'Objectif de mission',
        optional: true,
        options: [
          {
            label: 'ISS',
          },
          {
            label: 'Hubble',
          },
          {
            label: 'Scientific experiences',
          },
        ],
        bindings: [{ bindableEntities: [BindableEntity.TEST_CASE], projectIndexes: [0, 2] }],
      },
      {
        code: 'ENGINE_CODE',
        inputType: InputType.PLAIN_TEXT,
        label: 'Moteur du lanceur',
        name: 'Moteur principal du lanceur',
        optional: true,
        bindings: [{ bindableEntities: [BindableEntity.TEST_CASE], projectIndexes: [2] }],
      },
      {
        code: 'Numéro de mission',
        inputType: InputType.NUMERIC,
        label: 'Numéro de mission',
        name: 'Numéro de mission',
        optional: true,
        bindings: [{ bindableEntities: [BindableEntity.TEST_CASE], projectIndexes: [0] }],
      },
    )
    .build();
}

function checkSpaceShuttles(multiListCriteria: GroupedMultiListElement) {
  multiListCriteria.assertGroupExist('Space Shuttles');
  multiListCriteria.assertGroupContain('Space Shuttles', [
    'Atlantys',
    'Endeavour',
    'Discovery',
    'Columbia',
    'Challenger',
  ]);
}

function checkRockets(multiListCriteria: GroupedMultiListElement) {
  multiListCriteria.assertGroupExist('Rockets');
  multiListCriteria.assertGroupContain('Rockets', ['Saturn 5', 'Proton', 'Ariane 6']);
}

function checkSatellites(multiListCriteria: GroupedMultiListElement) {
  multiListCriteria.assertGroupExist('Satellites');
  multiListCriteria.assertGroupContain('Satellites', ['Diapason', 'Pollux', 'Polaire']);
}

function checkDefaultInfoList(multiListCriteria: GroupedMultiListElement) {
  multiListCriteria.assertGroupExist('Liste par défaut');
  multiListCriteria.assertGroupContain('Liste par défaut', [
    'Non définie',
    'Fonctionnel',
    'Métier',
    'Utilisateur',
    'Non fonctionnel',
    'Performance',
    'Sécurité',
    'ATDD',
  ]);
}

function checkDefaultInfoListType(multiListCriteria: GroupedMultiListElement) {
  multiListCriteria.assertGroupExist('Liste par défaut');
  multiListCriteria.assertGroupContain('Liste par défaut', [
    'Non défini',
    'Recevabilité',
    'Correctif',
    'Évolution',
    'Non-régression',
    'Bout-en-bout',
    'Partenaire',
  ]);
}

function checkNotFilteredList(multiListCriteria: GroupedMultiListElement) {
  checkSpaceShuttles(multiListCriteria);
  checkRockets(multiListCriteria);
  checkDefaultInfoList(multiListCriteria);
}

function checkNotFilteredListTypes(multiListCriteria: GroupedMultiListElement) {
  checkDefaultInfoListType(multiListCriteria);
  checkSatellites(multiListCriteria);
}

function openNatureSelector(testCaseSearchPage: TestCaseSearchPage) {
  return testCaseSearchPage.grid.filterPanel.selectMultiListCriteria('Nature');
}

function openTypeSelector(testCaseSearchPage: TestCaseSearchPage) {
  return testCaseSearchPage.grid.filterPanel.selectMultiListCriteria('Type');
}

describe('Test Case Search Scope', function () {
  beforeEach(() => {
    cy.viewport(1200, 720);
  });

  it('should change scope and emit ajax when widget is closed', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(referentialData);
    testCaseSearchPage.grid.filterPanel.assertPerimeterHasValue('Project 1, Project 2, Project 3');
    const multiListCriteria = testCaseSearchPage.grid.filterPanel.openProjectScopeSelector();
    multiListCriteria.toggleOneItem('Project 1');
    multiListCriteria.close();
    testCaseSearchPage.grid.filterPanel.assertPerimeterHasValue('Project 2, Project 3');
    testCaseSearchPage.grid.filterPanel.openProjectScopeSelector();
    multiListCriteria.toggleOneItem('Project 2');
    multiListCriteria.close();
    testCaseSearchPage.grid.filterPanel.assertPerimeterHasValue('Project 3');
    testCaseSearchPage.grid.filterPanel.openProjectScopeSelector();
    multiListCriteria.toggleOneItem('Project 1');
    multiListCriteria.toggleOneItem('Project 2');
    multiListCriteria.close();
    testCaseSearchPage.grid.filterPanel.assertPerimeterHasValue('Project 1, Project 2, Project 3');
  });

  it('should remove filters tied to scope when changing scope', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(referentialData);
    testCaseSearchPage.grid.filterPanel.assertPerimeterHasValue('Project 1, Project 2, Project 3');
    testCaseSearchPage.grid.filterPanel.fillTextCriteria('Nom', 'STS');
    testCaseSearchPage.grid.filterPanel.assertCriteriaIsActive('Nom');
    const natureList = testCaseSearchPage.grid.filterPanel.selectMultiListCriteria('Nature');
    natureList.toggleOneItem('Atlantys');
    natureList.close();
    const scopeSelector = testCaseSearchPage.grid.filterPanel.openProjectScopeSelector();
    scopeSelector.toggleOneItem('Project 1');
    scopeSelector.close();
    testCaseSearchPage.grid.filterPanel.assertCriteriaIsActive('Nom');
    testCaseSearchPage.grid.filterPanel.assertCriteriaIsInactive('Nature');
  });

  it('should change nature options when changing scope', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(referentialData);
    testCaseSearchPage.grid.filterPanel.assertPerimeterHasValue('Project 1, Project 2, Project 3');
    const natureList = openNatureSelector(testCaseSearchPage);
    checkNotFilteredList(natureList);
    natureList.close();
    const scopeSelector = testCaseSearchPage.grid.filterPanel.openProjectScopeSelector();
    scopeSelector.toggleOneItem('Project 3');
    scopeSelector.close();
    openNatureSelector(testCaseSearchPage);
    natureList.assertGroupNotExist('Liste par défaut');
    natureList.assertGroupExist('Space Shuttles');
    natureList.assertGroupExist('Rockets');
    natureList.close();
    testCaseSearchPage.grid.filterPanel.openProjectScopeSelector();
    scopeSelector.toggleOneItem('Project 2');
    scopeSelector.close();
    openNatureSelector(testCaseSearchPage);
    natureList.assertGroupNotExist('Liste par défaut');
    natureList.assertGroupNotExist('Rockets');
    natureList.assertGroupExist('Space Shuttles');
    natureList.close();
    testCaseSearchPage.grid.filterPanel.openProjectScopeSelector();
    scopeSelector.toggleOneItem('Project 3');
    scopeSelector.toggleOneItem('Project 1');
    scopeSelector.close();
    openNatureSelector(testCaseSearchPage);
    natureList.assertGroupNotExist('Space Shuttles');
    natureList.assertGroupNotExist('Rockets');
    natureList.assertGroupExist('Liste par défaut');
  });

  it('should change type options when changing scope', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(referentialData);
    testCaseSearchPage.grid.filterPanel.assertPerimeterHasValue('Project 1, Project 2, Project 3');
    const typeList = openTypeSelector(testCaseSearchPage);
    checkNotFilteredListTypes(typeList);
    typeList.close();
    const scopeSelector = testCaseSearchPage.grid.filterPanel.openProjectScopeSelector();
    scopeSelector.toggleOneItem('Project 3');
    scopeSelector.close();
    openTypeSelector(testCaseSearchPage);
    typeList.assertGroupExist('Liste par défaut');
    typeList.assertGroupExist('Satellites');
    typeList.close();
    testCaseSearchPage.grid.filterPanel.openProjectScopeSelector();
    scopeSelector.toggleOneItem('Project 1');
    scopeSelector.close();
    openTypeSelector(testCaseSearchPage);
    typeList.assertGroupExist('Liste par défaut');
    typeList.assertGroupNotExist('Satellites');
    typeList.close();
  });

  it('should add custom field when changing scope', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(referentialData);
    testCaseSearchPage.grid.filterPanel.assertPerimeterHasValue('Project 1, Project 2, Project 3');
    const scopeSelector = testCaseSearchPage.grid.filterPanel.openProjectScopeSelector();
    scopeSelector.toggleOneItem('Project 3');
    scopeSelector.toggleOneItem('Project 1');
    scopeSelector.close();
    const criteriaList = testCaseSearchPage.grid.filterPanel.openCriteriaList();
    criteriaList.assertGroupNotExist(CUSTOM_FIELD_GROUP_LABEL);
    criteriaList.close();
    testCaseSearchPage.grid.filterPanel.openProjectScopeSelector();
    scopeSelector.toggleOneItem('Project 3');
    scopeSelector.toggleOneItem('Project 2');
    scopeSelector.close();
    testCaseSearchPage.grid.filterPanel.openCriteriaList();
    criteriaList.assertGroupExist(CUSTOM_FIELD_GROUP_LABEL);
  });

  it('should change scope kind', () => {
    const initialNodes = mockGridResponse('id', [
      mockTreeNode({
        id: 'TestCaseLibrary-1',
        children: [],
        data: { NAME: 'Project 1', CHILD_COUNT: '1' },
        projectId: 1,
      }),
      mockTreeNode({
        id: 'TestCaseLibrary-2',
        children: [],
        data: { NAME: 'Project 2', CHILD_COUNT: '3' },
        projectId: 2,
      }),
      mockTreeNode({
        id: 'TestCaseLibrary-3',
        children: [],
        data: { NAME: 'Project 3', CHILD_COUNT: '3' },
        projectId: 3,
      }),
    ]);

    const children: DataRowModel[] = [
      mockTreeNode({
        id: 'TestCaseLibrary-1',
        children: ['TestCaseFolder-1'],
        data: { NAME: 'Project 1', CHILD_COUNT: '1' },
        projectId: 1,
        state: DataRowOpenState.open,
      }),
      mockTreeNode({
        id: 'TestCaseFolder-1',
        children: [],
        data: { NAME: 'Folder 1', CHILD_COUNT: '3' },
        parentRowId: 'TestCaseLibrary-1',
        projectId: 1,
      }),
    ];

    const referentialData = referentialDataWithCustomInfoLists();
    const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(referentialData);
    testCaseSearchPage.grid.filterPanel.assertPerimeterHasValue('Project 1, Project 2, Project 3');
    testCaseSearchPage.grid.filterPanel.openProjectScopeSelector();
    const testCaseScope = testCaseSearchPage.grid.filterPanel.changeToCustomScope(initialNodes);
    testCaseScope.tree.assertNodeExist('TestCaseLibrary-1');
    testCaseScope.tree.assertNodeTextContains('TestCaseLibrary-1', 'Project 1');
    testCaseScope.tree.assertNodeExist('TestCaseLibrary-2');
    testCaseScope.tree.assertNodeTextContains('TestCaseLibrary-2', 'Project 2');
    testCaseScope.tree.assertNodeExist('TestCaseLibrary-3');
    testCaseScope.tree.assertNodeTextContains('TestCaseLibrary-3', 'Project 3');
    testCaseScope.tree.openNode('TestCaseLibrary-1', children);
    testCaseScope.tree.assertNodeExist('TestCaseFolder-1');
    testCaseScope.tree.assertNodeTextContains('TestCaseFolder-1', 'Folder 1');
    testCaseScope.tree.pickNode('TestCaseFolder-1');
    testCaseScope.confirm();
    testCaseSearchPage.grid.filterPanel.assertPerimeterHasValue('Folder 1');
    testCaseSearchPage.grid.filterPanel.openCustomScopeSelector(initialNodes);
    testCaseScope.tree.pickNode('TestCaseLibrary-1');
    testCaseScope.confirm();
    testCaseSearchPage.grid.filterPanel.assertPerimeterHasValue('Project 1');
  });

  it('should set scope from url', () => {
    const initialNodes = mockGridResponse('id', [
      mockTreeNode({
        id: 'TestCaseLibrary-1',
        children: ['TestCaseFolder-1'],
        data: { NAME: 'Project 1', CHILD_COUNT: '1' },
        projectId: 1,
        state: DataRowOpenState.open,
      }),
      mockTreeNode({
        id: 'TestCaseLibrary-2',
        children: [],
        data: { NAME: 'Project 2', CHILD_COUNT: '3' },
        projectId: 2,
      }),
      mockTreeNode({
        id: 'TestCaseLibrary-3',
        children: [],
        data: { NAME: 'Project 3', CHILD_COUNT: '3' },
        projectId: 3,
      }),
      mockTreeNode({
        id: 'TestCaseFolder-1',
        children: [],
        data: { NAME: 'Folder 1', CHILD_COUNT: '3' },
        parentRowId: 'TestCaseLibrary-1',
        projectId: 1,
      }),
    ]);

    const scope: SimpleScope = {
      kind: 'custom',
      value: [
        {
          id: 'TestCaseFolder-1',
          projectId: 1,
          label: 'Folder 1',
        },
      ],
    };

    const queryString = `scope=${JSON.stringify(scope)}`;

    const referentialData = referentialDataWithCustomInfoLists();
    const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(
      referentialData,
      undefined,
      undefined,
      queryString,
    );
    testCaseSearchPage.grid.filterPanel.assertPerimeterHasValue('Folder 1');
    const testCaseScope = testCaseSearchPage.grid.filterPanel.openCustomScopeSelector(initialNodes);
    testCaseScope.tree.assertNodeExist('TestCaseFolder-1');
    testCaseScope.tree.assertNodeIsSelected('TestCaseFolder-1');
    testCaseScope.tree.assertNodeTextContains('TestCaseFolder-1', 'Folder 1');
  });
});
