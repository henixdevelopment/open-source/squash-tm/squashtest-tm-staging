import { TestCaseSearchPage } from '../../../../page-objects/pages/test-case-workspace/research/test-case-search-page';
import { createEntityReferentialData } from '../../../../utils/referential/create-entity-referential.const';
import { TestCaseSearchMilestoneMassEdit } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/test-case-search.model';
import { GridResponse } from '../../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { ListPanelElement } from '../../../../page-objects/elements/filters/list-panel.element';
import { mockDataRow } from '../../../../data-mock/grid.data-mock';

function getInitialNodes() {
  const initialNodes: GridResponse = {
    count: 1,
    dataRows: [
      mockDataRow({
        id: '1',
        type: 'TestCase',
        projectId: 1,
        data: {
          [GridColumnId.name]: 'Test 1',
          [GridColumnId.id]: 1,
          [GridColumnId.reference]: 'ref1',
          [GridColumnId.projectName]: 'project 1',
          [GridColumnId.attachments]: 0,
          [GridColumnId.items]: 0,
          [GridColumnId.steps]: 0,
          [GridColumnId.nature]: 12,
          [GridColumnId.type]: 16,
          [GridColumnId.automatable]: 'Y',
          [GridColumnId.status]: 'WORK_IN_PROGRESS',
          [GridColumnId.importance]: 'LOW',
          [GridColumnId.createdBy]: 'admin',
          [GridColumnId.lastModifiedBy]: 'admin',
          [GridColumnId.tcMilestoneLocked]: 0,
          [GridColumnId.reqMilestoneLocked]: 0,
        },
      }),
      mockDataRow({
        id: '2',
        type: 'TestCase',
        projectId: 1,
        data: {
          [GridColumnId.name]: 'Test 2',
          [GridColumnId.id]: 2,
          [GridColumnId.reference]: 'ref2',
          [GridColumnId.projectName]: 'project 1',
          [GridColumnId.attachments]: 0,
          [GridColumnId.items]: 0,
          [GridColumnId.steps]: 0,
          [GridColumnId.nature]: 12,
          [GridColumnId.type]: 16,
          [GridColumnId.automatable]: 'Y',
          [GridColumnId.status]: 'WORK_IN_PROGRESS',
          [GridColumnId.importance]: 'LOW',
          [GridColumnId.createdBy]: 'admin',
          [GridColumnId.lastModifiedBy]: 'admin',
          [GridColumnId.tcMilestoneLocked]: 1,
          [GridColumnId.reqMilestoneLocked]: 0,
        },
      }),
      mockDataRow({
        id: '3',
        type: 'TestCase',
        projectId: 1,
        data: {
          [GridColumnId.name]: 'Test 3',
          [GridColumnId.id]: 3,
          [GridColumnId.reference]: 'ref3',
          [GridColumnId.projectName]: 'project 1',
          [GridColumnId.attachments]: 0,
          [GridColumnId.items]: 0,
          [GridColumnId.steps]: 0,
          [GridColumnId.nature]: 12,
          [GridColumnId.type]: 16,
          [GridColumnId.automatable]: 'Y',
          [GridColumnId.status]: 'WORK_IN_PROGRESS',
          [GridColumnId.importance]: 'LOW',
          [GridColumnId.createdBy]: 'admin',
          [GridColumnId.lastModifiedBy]: 'admin',
          [GridColumnId.tcMilestoneLocked]: 0,
          [GridColumnId.reqMilestoneLocked]: 0,
          [GridColumnId.kind]: 'EXPLORATORY',
        },
      }),
      mockDataRow({
        id: '4',
        type: 'TestCase',
        projectId: 1,
        data: {
          [GridColumnId.name]: 'Test 4',
          [GridColumnId.id]: 4,
          [GridColumnId.reference]: 'ref4',
          [GridColumnId.projectName]: 'project 1',
          [GridColumnId.attachments]: 0,
          [GridColumnId.items]: 0,
          [GridColumnId.steps]: 0,
          [GridColumnId.nature]: 12,
          [GridColumnId.type]: 16,
          [GridColumnId.automatable]: 'Y',
          [GridColumnId.status]: 'WORK_IN_PROGRESS',
          [GridColumnId.importance]: 'LOW',
          [GridColumnId.createdBy]: 'admin',
          [GridColumnId.lastModifiedBy]: 'admin',
          [GridColumnId.tcMilestoneLocked]: 0,
          [GridColumnId.reqMilestoneLocked]: 0,
          [GridColumnId.kind]: 'EXPLORATORY',
        },
      }),
    ],
  };
  return initialNodes;
}

describe('Test Case Search Results', function () {
  beforeEach(() => {
    cy.viewport(1200, 720);
  });

  describe('Test Case search table', () => {
    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [
        mockDataRow({
          id: '1',
          type: 'TestCase',
          projectId: 1,
          data: {
            [GridColumnId.name]: 'Test 1',
            [GridColumnId.id]: 1,
            [GridColumnId.reference]: 'ref1',
            [GridColumnId.projectName]: 'project 1',
            [GridColumnId.attachments]: 0,
            [GridColumnId.items]: 0,
            [GridColumnId.steps]: 0,
            [GridColumnId.nature]: 12,
            [GridColumnId.type]: 16,
            [GridColumnId.automatable]: 'Y',
            [GridColumnId.status]: 'WORK_IN_PROGRESS',
            [GridColumnId.importance]: 'LOW',
            [GridColumnId.createdBy]: 'admin',
            [GridColumnId.lastModifiedBy]: 'admin',
            [GridColumnId.tcMilestoneLocked]: 0,
            [GridColumnId.reqMilestoneLocked]: 0,
          },
        }),
      ],
    };

    it('should display row in table', () => {
      const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(
        createEntityReferentialData,
        initialNodes,
      );
      const gridElement = testCaseSearchPage.grid;

      gridElement.assertRowExist(1);
      const row = gridElement.getRow(1);
      row.cell('reference').textRenderer().assertContainsText('ref1');
      row.cell('name').textRenderer().assertContainsText('Test 1');
    });

    it('should edit cell in table', () => {
      const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(
        createEntityReferentialData,
        initialNodes,
      );
      const gridElement = testCaseSearchPage.grid;

      gridElement.assertRowExist(1);
      const row = gridElement.getRow(1);
      const refCell = row.cell('reference').textRenderer();
      refCell.editText('ref02', 'test-case/*/reference');
      refCell.assertContainsText('ref02');

      const natureCell = row.cell('nature').selectRenderer();
      natureCell.changeValue('item-16', 'test-case/*/nature');
      natureCell.assertContainText('Non fonctionnel');

      const httpError = {
        squashTMError: {
          kind: 'FIELD_VALIDATION_ERROR',
          fieldValidationErrors: [
            {
              fieldName: 'name',
              i18nKey: 'sqtm-core.error.generic.duplicate-name',
            },
          ],
        },
      };
      const nameCell = row.cell('name').textRenderer();
      nameCell.editTextError('TestCase-42', 'test-case/*/name', httpError);
      nameCell.assertErrorDialogContains('Un élément avec ce nom existe déjà à cet emplacement');
    });
  });

  describe('Test Case search mass edit', () => {
    const initialNodes = getInitialNodes();

    const editResponse: GridResponse = {
      count: 1,
      dataRows: [
        mockDataRow({
          id: '1',
          type: 'TestCase',
          projectId: 1,
          data: {
            [GridColumnId.name]: 'Test 1',
            [GridColumnId.id]: 1,
            [GridColumnId.reference]: 'ref1',
            [GridColumnId.projectName]: 'project 1',
            [GridColumnId.attachments]: 0,
            [GridColumnId.items]: 0,
            [GridColumnId.steps]: 0,
            [GridColumnId.nature]: 14,
            [GridColumnId.type]: 16,
            [GridColumnId.automatable]: 'Y',
            [GridColumnId.status]: 'WORK_IN_PROGRESS',
            [GridColumnId.importance]: 'LOW',
            [GridColumnId.createdBy]: 'admin',
            [GridColumnId.lastModifiedBy]: 'admin',
            [GridColumnId.tcMilestoneLocked]: 0,
            [GridColumnId.reqMilestoneLocked]: 0,
            [GridColumnId.kind]: 'STANDARD',
          },
        }),
        mockDataRow({
          id: '2',
          type: 'TestCase',
          projectId: 1,
          data: {
            [GridColumnId.name]: 'Test 2',
            [GridColumnId.id]: 2,
            [GridColumnId.reference]: 'ref2',
            [GridColumnId.projectName]: 'project 1',
            [GridColumnId.attachments]: 0,
            [GridColumnId.items]: 0,
            [GridColumnId.steps]: 0,
            [GridColumnId.nature]: 12,
            [GridColumnId.type]: 16,
            [GridColumnId.automatable]: 'Y',
            [GridColumnId.status]: 'WORK_IN_PROGRESS',
            [GridColumnId.importance]: 'LOW',
            [GridColumnId.createdBy]: 'admin',
            [GridColumnId.lastModifiedBy]: 'admin',
            [GridColumnId.tcMilestoneLocked]: 1,
            [GridColumnId.reqMilestoneLocked]: 0,
            [GridColumnId.kind]: 'STANDARD',
          },
        }),
      ],
    };

    it('should mass edit rows', () => {
      const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(
        createEntityReferentialData,
        initialNodes,
      );
      const gridElement = testCaseSearchPage.grid;
      gridElement.selectRow(1, '#', 'leftViewport');
      const massEditTestCaseDialog = testCaseSearchPage.showMassEditDialog();
      massEditTestCaseDialog.assertExists();
      const natureField = massEditTestCaseDialog.getOptionalField('nature');
      natureField.check();
      natureField.selectValue('Métier');

      massEditTestCaseDialog.confirm(editResponse);
      const row = gridElement.getRow(1);
      const natureCell = row.cell('nature').selectRenderer();
      natureCell.assertContainText('Métier');
    });

    it('should mass edit only available rows', () => {
      const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(
        createEntityReferentialData,
        initialNodes,
      );
      const gridElement = testCaseSearchPage.grid;
      gridElement.selectRows([1, 2], '#', 'leftViewport');
      const massEditTestCaseDialog = testCaseSearchPage.showMassEditDialog();
      massEditTestCaseDialog.assertExists();
      massEditTestCaseDialog.assertExistNoWritingRightsMessage();
      const natureField = massEditTestCaseDialog.getOptionalField('nature');
      natureField.check();
      natureField.selectValue('Métier');
      massEditTestCaseDialog.confirm(editResponse);
      const row = gridElement.getRow(1);
      const natureCell = row.cell('nature').selectRenderer();
      natureCell.assertContainText('Métier');
    });

    it('should not display multi edit dialog if not writing rights', () => {
      const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(
        createEntityReferentialData,
        initialNodes,
      );
      const gridElement = testCaseSearchPage.grid;
      gridElement.selectRow(2, '#', 'leftViewport');
      const massEditTestCaseDialog = testCaseSearchPage.showMassEditDialog();
      massEditTestCaseDialog.assertNotExist();
      const noLineWritingRightsDialogElement = testCaseSearchPage.getNoLineWritingRightsDialog();
      noLineWritingRightsDialogElement.assertExists();
      noLineWritingRightsDialogElement.assertMessage();
      noLineWritingRightsDialogElement.close();
      noLineWritingRightsDialogElement.assertNotExist();
    });

    it('should not edit automatable, technology and url if test case is exploratory', () => {
      const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(
        createEntityReferentialData,
        initialNodes,
      );
      const gridElement = testCaseSearchPage.grid;

      gridElement.getRow(3).cell('automatable').findCell().find('span').click();
      const listPanel = new ListPanelElement();
      listPanel.assertNotExist();

      gridElement.selectRows([3, 4], '#', 'leftViewport');
      const massEditTestCaseDialog = testCaseSearchPage.showMassEditDialog();
      massEditTestCaseDialog.assertExists();
      massEditTestCaseDialog.assertAutomatableFieldsAreDisabled();
    });
  });

  describe('Test Case search edit milestones', () => {
    const initialNodes = getInitialNodes();

    it('should edit milestones on selected rows', () => {
      const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(
        createEntityReferentialData,
        initialNodes,
      );
      testCaseSearchPage.clickOnEyeAndWaitResponse();
      const gridElement = testCaseSearchPage.grid;
      gridElement.selectRow(1, '#', 'leftViewport');
      const milestoneMassEdit: TestCaseSearchMilestoneMassEdit = {
        milestoneIds: [1, 2],
        checkedIds: [1],
        samePerimeter: true,
        testCaseIds: [],
      };
      const milestoneDialog =
        testCaseSearchPage.showMassBindingMilestoneDialog<TestCaseSearchMilestoneMassEdit>(
          milestoneMassEdit,
        );
      milestoneDialog.assertExists();

      const milestoneGrid = milestoneDialog.getMilestoneGrid();
      milestoneGrid.assertExists();
      milestoneGrid.assertRowExist(1);
      milestoneGrid.assertRowExist(2);
      const row2 = milestoneGrid.getRow(2);
      const checkBoxCell = row2.cell('select-row-column').checkBoxRender();
      checkBoxCell.toggleState();
      checkBoxCell.assertIsCheck();

      milestoneDialog.confirm();
    });

    it('should display no same perimeter message', () => {
      const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(
        createEntityReferentialData,
        initialNodes,
      );
      const gridElement = testCaseSearchPage.grid;
      testCaseSearchPage.clickOnEyeAndWaitResponse();
      gridElement.selectRows([1, 2], '#', 'leftViewport');
      const milestoneMassEdit: TestCaseSearchMilestoneMassEdit = {
        milestoneIds: [1, 2],
        checkedIds: [1],
        samePerimeter: false,
        testCaseIds: [],
      };

      const milestoneDialog =
        testCaseSearchPage.showMassBindingMilestoneDialog<TestCaseSearchMilestoneMassEdit>(
          milestoneMassEdit,
        );
      milestoneDialog.assertExists();

      milestoneDialog.assertNoSamePerimeterMessage();
    });
  });
  it('should change column displayed when eye clicked', () => {
    const testCaseSearchPage = TestCaseSearchPage.initTestAtPage(
      createEntityReferentialData,
      getInitialNodes(),
    );
    testCaseSearchPage.checkIfColumnAreDisplayedThenClick('not.exist');
    testCaseSearchPage.clickOnEyeAndWaitResponse();
    testCaseSearchPage.checkIfColumnAreDisplayedThenClick('exist');
    testCaseSearchPage.clickOnEyeAndWaitResponse();
  });
});
