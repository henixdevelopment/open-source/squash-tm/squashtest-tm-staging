import { ReferentialDataMockBuilder } from '../../../utils/referential/referential-data-builder';
import { HomeWorkspacePage } from '../../../page-objects/pages/home-workspace/home-workspace.page';
import { NavBarElement } from '../../../page-objects/elements/nav-bar/nav-bar.element';

describe('MilestoneMode', function () {
  it('should not display milestone button if milestones are not enabled', function () {
    const homeWorkspacePage = HomeWorkspacePage.initTestAtPage();
    const navBarElement = homeWorkspacePage.navBar;
    navBarElement.assertMilestoneMenuNotExist();
  });

  it('should display milestone button if milestones are activated', function () {
    const referentialDataMock = new ReferentialDataMockBuilder()
      .withProjects(
        { name: 'Project 1', label: 'Etiquette' },
        { name: 'Project 2', label: 'Etiquette 2' },
      )
      .withMilestones({
        label: 'milestone1',
        description: '',
        endDate: new Date().toISOString(),
        status: 'IN_PROGRESS',
        boundProjectIndexes: [1],
        range: 'GLOBAL',
        ownerFistName: 'admin',
        ownerLastName: 'admin',
        ownerLogin: 'admin',
      })
      .withUser({
        hasAnyReadPermission: true,
        functionalTester: true,
      })
      .build();
    referentialDataMock.globalConfiguration.milestoneFeatureEnabled = true;
    const homeWorkspacePage = HomeWorkspacePage.initTestAtPage(referentialDataMock);
    const navBarElement = homeWorkspacePage.navBar;
    navBarElement.assertMilestoneMenuExist();
  });

  it('should select a milestone', function () {
    const referentialDataMock = new ReferentialDataMockBuilder()
      .withProjects(
        { name: 'Project 1', label: 'Etiquette' },
        { name: 'Project 2', label: 'Etiquette 2' },
      )
      .withMilestones(
        {
          label: 'milestone1',
          description: '',
          endDate: new Date().toISOString(),
          status: 'IN_PROGRESS',
          boundProjectIndexes: [1],
          range: 'GLOBAL',
          ownerFistName: 'admin',
          ownerLastName: 'admin',
          ownerLogin: 'admin',
        },
        {
          label: 'ahhh',
          description: '',
          endDate: new Date().toISOString(),
          status: 'IN_PROGRESS',
          boundProjectIndexes: [1],
          range: 'GLOBAL',
          ownerFistName: 'admin',
          ownerLastName: 'admin',
          ownerLogin: 'admin',
        },
      )
      .withUser({
        hasAnyReadPermission: true,
        functionalTester: true,
      })
      .build();
    referentialDataMock.globalConfiguration.milestoneFeatureEnabled = true;
    const homeWorkspacePage = HomeWorkspacePage.initTestAtPage(referentialDataMock);
    const navBarElement = homeWorkspacePage.navBar;
    navBarElement.assertMilestoneMenuExist();
    navBarElement.assertMilestoneModeIsDisabled();
    const milestonePicker = navBarElement.openMilestoneSelector();
    milestonePicker.selectMilestone('ahhh');
    milestonePicker.assertMilestoneIsSelected('ahhh');
    milestonePicker.selectMilestone('milestone1');
    milestonePicker.assertMilestoneIsSelected('milestone1');
    milestonePicker.confirm();
    navBarElement.assertMilestoneModeIsActive();
    navBarElement.openMilestoneSelector();
    milestonePicker.assertMilestoneIsSelected('milestone1');
    cy.reload();
    navBarElement.assertMilestoneModeIsActive();
    navBarElement.openMilestoneSelector();
    milestonePicker.assertMilestoneIsSelected('milestone1');
  });

  it('should show warning in project filter when milestone mode is enabled', function () {
    const referentialDataMock = new ReferentialDataMockBuilder()
      .withProjects(
        { name: 'Project 1', label: 'Etiquette' },
        { name: 'Project 2', label: 'Etiquette 2' },
      )
      .withMilestones(
        {
          label: 'milestone1',
          description: '',
          endDate: new Date().toISOString(),
          status: 'IN_PROGRESS',
          boundProjectIndexes: [1],
          range: 'GLOBAL',
          ownerFistName: 'admin',
          ownerLastName: 'admin',
          ownerLogin: 'admin',
        },
        {
          label: 'ahhh',
          description: '',
          endDate: new Date().toISOString(),
          status: 'IN_PROGRESS',
          boundProjectIndexes: [1],
          range: 'GLOBAL',
          ownerFistName: 'admin',
          ownerLastName: 'admin',
          ownerLogin: 'admin',
        },
      )
      .withUser({
        hasAnyReadPermission: true,
        functionalTester: true,
      })
      .build();
    referentialDataMock.globalConfiguration.milestoneFeatureEnabled = true;
    const homeWorkspacePage = HomeWorkspacePage.initTestAtPage(referentialDataMock);
    const navBarElement = homeWorkspacePage.navBar;
    navBarElement.assertMilestoneMenuExist();
    navBarElement.assertMilestoneModeIsDisabled();
    const projectFilterDialog = NavBarElement.openProjectFilter();
    projectFilterDialog.assertMilestoneWarningIsNotVisible();
    projectFilterDialog.cancel();
    const milestonePicker = navBarElement.openMilestoneSelector();
    milestonePicker.selectMilestone('ahhh');
    milestonePicker.confirm();
    NavBarElement.openProjectFilter();
    projectFilterDialog.assertMilestoneWarningIsVisible();
    projectFilterDialog.cancel();
    navBarElement.disableMilestoneMode();
    NavBarElement.openProjectFilter();
    projectFilterDialog.assertMilestoneWarningIsNotVisible();
  });
});
