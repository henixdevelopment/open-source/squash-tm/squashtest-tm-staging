import { ReferentialDataMockBuilder } from '../../../utils/referential/referential-data-builder';
import { NavBarElement } from '../../../page-objects/elements/nav-bar/nav-bar.element';
import { HomeWorkspacePage } from '../../../page-objects/pages/home-workspace/home-workspace.page';

describe('ProjectFilter', function () {
  it('should display project filter dialog', function () {
    const referentialDataMock = new ReferentialDataMockBuilder()
      .withProjects(
        { name: 'Project 1', label: 'Etiquette' },
        { name: 'Project 2', label: 'Etiquette 2' },
      )
      .withUser({
        hasAnyReadPermission: true,
        functionalTester: true,
      })
      .build();
    referentialDataMock.filteredProjectIds = [1];
    HomeWorkspacePage.initTestAtPage(referentialDataMock);
    const projectFilterDialog = NavBarElement.openProjectFilter();
    const grid = projectFilterDialog.grid;

    grid.assertExists();
    grid.assertRowExist(1);
    grid.assertRowExist(2);
    const selectedRow = grid.getRow(1);
    selectedRow.assertIsSelected();

    projectFilterDialog.cancel();
  });

  it('should change filter project', function () {
    const referentialDataMock = new ReferentialDataMockBuilder()
      .withProjects(
        { name: 'Project 1', label: 'Etiquette' },
        { name: 'Project 2', label: 'Etiquette 2' },
        { name: 'Project 3', label: '' },
        { name: 'Project 4', label: '' },
        { name: 'Test', label: '' },
        { name: 'Test 2', label: '' },
      )
      .withUser({
        hasAnyReadPermission: true,
        functionalTester: true,
      })
      .build();
    referentialDataMock.filteredProjectIds = [1, 3, 4];
    HomeWorkspacePage.initTestAtPage(referentialDataMock);
    const projectFilterDialog = NavBarElement.openProjectFilter();
    const grid = projectFilterDialog.grid;

    grid.assertRowCount(6);
    const row = grid.getRow(2);
    const checkBoxCell = row.cell('select-row-column').checkBoxRender();
    checkBoxCell.toggleState();
    projectFilterDialog.confirm();

    const projectFilter = NavBarElement.openProjectFilter();
    projectFilter.grid.assertRowCount(6);
    projectFilter.grid.getRow(2).assertIsSelected();
  });

  it('should search project on label and name', function () {
    const referentialDataMock = new ReferentialDataMockBuilder()
      .withProjects(
        { name: 'Project 1', label: 'Etiquette' },
        { name: 'Project 2', label: 'Etiquette 2' },
        { name: 'T 3', label: 'Project' },
        { name: 'project 4', label: 'Project' },
        { name: 'Test', label: 'Eti' },
        { name: 'Test 2', label: 'Eti' },
      )
      .withUser({
        hasAnyReadPermission: true,
        functionalTester: true,
      })
      .build();
    HomeWorkspacePage.initTestAtPage(referentialDataMock);
    const projectFilterDialog = NavBarElement.openProjectFilter();
    const grid = projectFilterDialog.grid;

    grid.assertRowCount(6);
    projectFilterDialog.fillSearchInput('Project');
    grid.assertRowCount(4);
    projectFilterDialog.fillSearchInput('Eti');
    grid.assertRowCount(4);
    projectFilterDialog.fillSearchInput('Etiquette');
    grid.assertRowCount(2);
  });

  it('should change filter project by hitting Enter', function () {
    const referentialDataMock = new ReferentialDataMockBuilder()
      .withProjects(
        { name: 'Project 1', label: 'Etiquette' },
        { name: 'Project 2', label: 'Etiquette 2' },
        { name: 'Project 3', label: '' },
        { name: 'Project 4', label: '' },
        { name: 'Test', label: '' },
        { name: 'Test 2', label: '' },
      )
      .withUser({
        hasAnyReadPermission: true,
        functionalTester: true,
      })
      .build();
    referentialDataMock.filteredProjectIds = [1, 3, 4];
    HomeWorkspacePage.initTestAtPage(referentialDataMock);
    const projectFilterDialog = NavBarElement.openProjectFilter();
    const grid = projectFilterDialog.grid;

    grid.assertRowCount(6);
    const row = grid.getRow(2);
    const checkBoxCell = row.cell('select-row-column').checkBoxRender();
    checkBoxCell.toggleState();
    projectFilterDialog.confirmWithEnterKey();

    const projectFilter = NavBarElement.openProjectFilter();
    projectFilter.grid.assertRowCount(6);
    projectFilter.grid.getRow(2).assertIsSelected();
  });

  it('should exit filter project dialog by hitting Escape', function () {
    const referentialDataMock = new ReferentialDataMockBuilder()
      .withProjects(
        { name: 'Project 1', label: 'Etiquette' },
        { name: 'Project 2', label: 'Etiquette 2' },
        { name: 'Project 3', label: '' },
        { name: 'Project 4', label: '' },
        { name: 'Test', label: '' },
        { name: 'Test 2', label: '' },
      )
      .withUser({
        hasAnyReadPermission: true,
        functionalTester: true,
      })
      .build();
    referentialDataMock.filteredProjectIds = [1, 3, 4];
    HomeWorkspacePage.initTestAtPage(referentialDataMock);
    const projectFilterDialog = NavBarElement.openProjectFilter();
    const grid = projectFilterDialog.grid;

    grid.assertRowCount(6);
    const row = grid.getRow(2);
    const checkBoxCell = row.cell('select-row-column').checkBoxRender();
    checkBoxCell.toggleState();
    projectFilterDialog.cancelWithEscapeKey();

    const projectFilter = NavBarElement.openProjectFilter();
    projectFilter.grid.assertRowCount(6);
    projectFilter.grid.getRow(1).assertIsSelected();
    projectFilter.grid.getRow(3).assertIsSelected();
    projectFilter.grid.getRow(4).assertIsSelected();
    projectFilter.grid.getRow(2).assertIsNotSelected();
  });
});
