import { HomeWorkspacePage } from '../../../page-objects/pages/home-workspace/home-workspace.page';
import { NavBarElement } from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {
  AuthenticationPolicy,
  AuthenticationProtocol,
} from '../../../../../projects/sqtm-core/src/lib/model/third-party-server/authentication.model';
import { UserAccount } from '../../../../../projects/sqtm-core/src/lib/model/user/user-account.model';
import { selectByDataTestElementId } from '../../../utils/basic-selectors';

describe('UserAccount', function () {
  it(`should navigate to user account`, () => {
    HomeWorkspacePage.initTestAtPage();
    const userAccountPage = NavBarElement.openUserAccount(getInitialData());

    userAccountPage.assertExists();

    userAccountPage.assertName('admin admin');
    userAccountPage.assertEmail('admin@squash.fr');
    userAccountPage.assertGroup('Administrateur');

    userAccountPage.permissionsGrid.assertRowCount(2);
  });

  it(`should load appropriate credentials form`, () => {
    HomeWorkspacePage.initTestAtPage();
    const userAccountPage = NavBarElement.openUserAccount(getInitialData());

    userAccountPage.anchors.clickLink('bugtrackers');
    userAccountPage.assertBugTrackerLogin('username');
    userAccountPage.selectEditedBugTracker('BT2 (url2)');
    userAccountPage.assertBugTrackerRevokeButtonVisible();
  });

  it(`should generate an API token`, () => {
    HomeWorkspacePage.initTestAtPage();
    const userAccountPage = NavBarElement.openUserAccount(getInitialData());

    userAccountPage.assertApiTokenPanelExists();

    const generateDialog = userAccountPage.generateApiTokenDialogElement;
    userAccountPage.openGenerateApiTokenDialog();
    generateDialog.assertExists();
    generateDialog.fillName('my token');
    generateDialog.addToken();

    const displayGeneratedTokenDialog = userAccountPage.displayTokenDialogElement;
    displayGeneratedTokenDialog.assertExists();
    displayGeneratedTokenDialog.showToken('abcdefgh');
    displayGeneratedTokenDialog.assertCanCopyToken();
  });

  it(`should not be able to generate an API token without jwt secret`, () => {
    HomeWorkspacePage.initTestAtPage();
    const userAccountPage = NavBarElement.openUserAccount(
      {
        ...getInitialData(),
      } as UserAccount,
      false,
    );

    userAccountPage.assertErrorDialogIfNoJwtSecret();
  });

  it(`should display personal token list`, () => {
    HomeWorkspacePage.initTestAtPage();
    const userAccountPage = NavBarElement.openUserAccount(getInitialData());

    userAccountPage.assertApiTokenPanelExists();
    const tokenGrid = userAccountPage.apiTokenPanelElement.personalApiTokenGrid;
    tokenGrid.assertExists();
    tokenGrid.assertRowCount(2);
    tokenGrid.getRow(1).cell('name').findCell().should('contain.text', 'token pour lire');
    tokenGrid.getRow(1).cell('permissions').findCell().should('contain.text', 'Lecture');
    tokenGrid.getRow(1).cell('createdOn').findCell().should('contain.text', '02/07/2023');
    tokenGrid.getRow(1).cell('lastUsage').findCell().should('contain.text', '-');
    tokenGrid.getRow(1).cell('expiryDate').findCell().should('contain.text', '02/07/2024 (expiré)');
    tokenGrid
      .getRow(1)
      .cell('expiryDate')
      .findCell()
      .find(selectByDataTestElementId('date-with-expired-info'))
      .should('have.css', 'color', 'rgb(255, 0, 0)');

    tokenGrid.getRow(2).cell('lastUsage').findCell().should('contain.text', '03/07/2024 14:22');
    tokenGrid.getRow(2).cell('expiryDate').findCell().should('contain.text', '02/07/2025');
    tokenGrid
      .getRow(2)
      .cell('expiryDate')
      .findCell()
      .find(selectByDataTestElementId('date-with-expired-info'))
      .should('have.css', 'color', 'rgb(0, 0, 0)');
  });

  it('should delete a personal API token', () => {
    HomeWorkspacePage.initTestAtPage();
    const userAccountPage = NavBarElement.openUserAccount(getInitialData());

    const apiTokenPanel = userAccountPage.apiTokenPanelElement;
    apiTokenPanel.deleteApiToken(1);
  });
});

function getInitialData(): UserAccount {
  return {
    id: 1,
    login: 'admin',
    email: 'admin@squash.fr',
    firstName: 'admin',
    lastName: 'admin',
    bugTrackerCredentials: [
      {
        bugTracker: {
          id: 1,
          url: 'url',
          name: 'BT1',
          kind: 'jira.cloud',
          authProtocol: AuthenticationProtocol.BASIC_AUTH,
          authPolicy: AuthenticationPolicy.USER,
          iframeFriendly: true,
        },
        credentials: {
          implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
          type: AuthenticationProtocol.BASIC_AUTH,
          username: 'username',
          password: 'password',
        },
      },
      {
        bugTracker: {
          id: 2,
          url: 'url2',
          name: 'BT2',
          kind: 'jira.server',
          authProtocol: AuthenticationProtocol.OAUTH_2,
          authPolicy: AuthenticationPolicy.USER,
          iframeFriendly: true,
        },
        credentials: {
          implementedProtocol: AuthenticationProtocol.OAUTH_2,
          type: AuthenticationProtocol.OAUTH_2,
          accessToken: '',
        },
      },
    ],
    projectPermissions: [
      {
        projectName: 'P1',
        projectId: 1,
        permissionGroup: {
          qualifiedName: 'ProjectManager',
          id: 1,
          active: true,
          system: true,
        },
      },
      {
        projectName: 'P2',
        projectId: 2,
        permissionGroup: {
          qualifiedName: 'AdvanceTester',
          id: 2,
          active: true,
          system: true,
        },
      },
    ],
    bugTrackerMode: 'Automatic',
    userGroup: {
      id: 1,
      qualifiedName: 'squashtest.authz.group.core.Admin',
    },
    canManageLocalPassword: true,
    hasLocalPassword: true,
  };
}
