import { HomeWorkspacePage } from '../../../page-objects/pages/home-workspace/home-workspace.page';
import { ReferentialDataMockBuilder } from '../../../utils/referential/referential-data-builder';
import { TestCaseWorkspacePage } from '../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { LicenseInformationBannerElement } from '../../../page-objects/elements/license-messages/license-information-banner.element';
import { AdminWorkspaceProjectsPage } from '../../../page-objects/pages/administration-workspace/admin-workspace-projects.page';
import { mockGridResponse } from '../../../data-mock/grid.data-mock';
import { AdminReferentialDataMockBuilder } from '../../../utils/referential/admin-referential-data-builder';
import { AdminWorkspaceUsersPage } from '../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import { AlertDialogElement } from '../../../page-objects/elements/dialog/alert-dialog.element';
import { LoginPage } from '../../../page-objects/pages/login/login-page';
import { HomeWorkspaceModel } from '../../../../../projects/sqtm-core/src/lib/model/home/home-workspace.model';
import { ReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/referential-data.model';
import { AdminReferentialDataModel } from '../../../../../projects/sqtm-core/src/lib/model/referential-data/admin-referential-data.model';
import { GridResponse } from '../../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';

describe('License messages', function () {
  it('should show home page with no license message', () => {
    const homeWorkspacePage = HomeWorkspacePage.initTestAtPageWithModel(
      getHomeWorkspaceModel(),
      getReferentialData(false),
    );
    homeWorkspacePage.assertExists();
    homeWorkspacePage.assertLicenseMessageDoesNotExist();
  });

  it('should show home page with license message', () => {
    const homeWorkspacePage = HomeWorkspacePage.initTestAtPageWithModel(
      getHomeWorkspaceModel(),
      getReferentialData(true),
    );
    homeWorkspacePage.assertExists();
    homeWorkspacePage.assertLicenseMessageIsVisible();
  });

  it('should display test case workspace without license banner', () => {
    TestCaseWorkspacePage.initTestAtPage(getEmptyGridResponse(), getReferentialData(false));
    const banner = new LicenseInformationBannerElement();
    banner.assertDoesNotExist();
  });

  it('should display test case workspace with license banner', () => {
    TestCaseWorkspacePage.initTestAtPage(getEmptyGridResponse(), getReferentialData(true));
    const banner = new LicenseInformationBannerElement();
    banner.assertIsVisible();
    const detailsDialog = banner.openDetailsDialog();
    detailsDialog.assertExists();
    detailsDialog.clickOnCloseButton();
  });

  it('should display projects workspace without license banner', () => {
    AdminWorkspaceProjectsPage.initTestAtPageProjects(getEmptyGridResponse());
    const banner = new LicenseInformationBannerElement();
    banner.assertDoesNotExist();
  });

  it('should display projects workspace with license banner', () => {
    AdminWorkspaceProjectsPage.initTestAtPageProjects(
      getEmptyGridResponse(),
      getAdminRefDataWithUserWarning(),
    );
    const banner = new LicenseInformationBannerElement();
    banner.assertIsVisible();
    const detailsDialog = banner.openDetailsDialog();
    detailsDialog.assertExists();
    detailsDialog.clickOnCloseButton();
  });

  it('should show user warning in user creation dialog', () => {
    const usersWorkspace = AdminWorkspaceUsersPage.initTestAtPage(
      getEmptyGridResponse(),
      getAdminRefDataWithUserWarning(),
    );
    const createUserDialog = usersWorkspace.openCreateUser();

    createUserDialog.assertHasLicenseWarning();
  });

  it('should show prevent user creation when license forbids it', () => {
    const usersWorkspace = AdminWorkspaceUsersPage.initTestAtPage(
      getEmptyGridResponse(),
      getAdminRefDataWithUserExcess(),
    );
    const createUserDialog = usersWorkspace.openCreateUser();

    createUserDialog.assertNotExist();
    const alert = new AlertDialogElement('license-information-detail');
    alert.assertExists();
  });

  it('should show license info in alert when admin logs in', () => {
    const loginPage = LoginPage.navigateTo();
    loginPage.assertExists();
    const informationPage = loginPage.loginWithLicenseInfo('admin', 'admin');
    informationPage.assertAlertIsVisible();

    const homeWorkspacePage = informationPage.confirmAlert();
    homeWorkspacePage.assertExists();
  });
});

function getHomeWorkspaceModel(): HomeWorkspaceModel {
  return {
    welcomeMessage: 'Hello darkness my old friend',
    dashboard: undefined,
  };
}

function getReferentialData(withLicenseInfo: boolean): ReferentialDataModel {
  if (withLicenseInfo) {
    return new ReferentialDataMockBuilder()
      .withLicenseInformation({
        activatedUserExcess: '2-1-false',
        pluginLicenseExpiration: '-100',
      })
      .withUser({ userId: 1, admin: true })
      .build();
  } else {
    return new ReferentialDataMockBuilder().withUser({ userId: 1, admin: true }).build();
  }
}

function getAdminRefDataWithUserExcess(): AdminReferentialDataModel {
  return new AdminReferentialDataMockBuilder()
    .withLicenseInformation({
      activatedUserExcess: '2-1-false',
      pluginLicenseExpiration: '-100',
    })
    .build();
}

function getAdminRefDataWithUserWarning(): AdminReferentialDataModel {
  return new AdminReferentialDataMockBuilder()
    .withLicenseInformation({
      activatedUserExcess: '2-1-true',
      pluginLicenseExpiration: '-100',
    })
    .build();
}

function getEmptyGridResponse(): GridResponse {
  return mockGridResponse('id', []);
}
