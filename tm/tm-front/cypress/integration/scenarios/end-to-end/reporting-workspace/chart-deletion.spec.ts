import { beforeEach } from 'mocha';
import { DatabaseUtils } from '../../../utils/database.utils';
import { CustomReportWorkspacePage } from '../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import { PrerequisiteBuilder } from '../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  ChartBuilder,
  CustomReportChartBinding,
  DashboardBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/custom-report-prerequisite-builders';
import {
  ChartOperation,
  ChartScopeType,
  ChartType,
} from '../../../../../projects/sqtm-core/src/lib/model/custom-report/chart-definition.model';
import { EntityType } from '../../../../../projects/sqtm-core/src/lib/model/entity.model';
import {
  RequirementBuilder,
  RequirementFolderBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  TestCaseBuilder,
  TestCaseFolderBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { DashboardViewPage } from '../../../page-objects/pages/custom-report-workspace/dashboard-view.page';
import { TreeElement } from '../../../page-objects/elements/grid/grid.element';
import { CustomReportWorkspaceTreeMenu } from '../../../page-objects/pages/custom-report-workspace/custom-report-workspace-tree-menu';

describe('This test verifies that it is possible to delete one or more custom charts', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
  });

  it('F05-UC024-CT01 - chart-deletion', () => {
    cy.log('Step 1');
    cy.logInAs('admin', 'admin');
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage();
    const customReportTree = customReportWorkspacePage.tree;
    const customReportTreeMenu = customReportWorkspacePage.treeMenu;
    const dashboardViewPage = new DashboardViewPage(1);

    assertCustomReportWorkspaceDisplaysProjectAndCharts(customReportTree);

    cy.log('Step 2');
    assertChartDeletionWithCancelAndConfirm(customReportTree, customReportTreeMenu);

    cy.log('Step 3');
    assertDeleteChartByName(customReportTree, customReportTreeMenu, 'ChartBar');

    cy.log('Step 4');
    assertDeleteChartByName(customReportTree, customReportTreeMenu, 'ChartCumulative1');

    cy.log('Step 5');
    assertDeleteChartByName(customReportTree, customReportTreeMenu, 'ChartTrend');

    cy.log('Step 6');
    assertDeleteChartByName(customReportTree, customReportTreeMenu, 'ChartComparative1');

    cy.log('Step 7');
    assertMultipleDeleteChartsAndVerifyEmptyDashboard(
      customReportTree,
      customReportTreeMenu,
      dashboardViewPage,
    );
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withProjects([
      new ProjectBuilder('Project')
        .withRequirementLibraryNodes([
          new RequirementFolderBuilder('Folder').withRequirementLibraryNodes([
            new RequirementBuilder('Req1').withCriticality('CRITICAL'),
            new RequirementBuilder('Req2').withCriticality('MAJOR'),
            new RequirementBuilder('Req3').withCriticality('MINOR'),
            new RequirementBuilder('Req4').withCriticality('UNDEFINED'),
          ]),
        ])
        .withTestCaseLibraryNodes([
          new TestCaseFolderBuilder('Folder').withTestCaseLibraryNodes([
            new TestCaseBuilder('Test1').withStatus('TO_BE_UPDATED'),
            new TestCaseBuilder('Test2').withStatus('APPROVED'),
            new TestCaseBuilder('Test3').withStatus('OBSOLETE'),
            new TestCaseBuilder('Test4').withStatus('WORK_IN_PROGRESS'),
            new TestCaseBuilder('Test5').withStatus('TO_BE_UPDATED'),
          ]),
        ])
        .withCustomReportLibraryNode([
          new ChartBuilder(
            'ChartPie1',
            'REQUIREMENT_CRITICALITY',
            ChartType.PIE,
            ChartScopeType.DEFAULT,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ),
          new ChartBuilder(
            'ChartPie2',
            'TEST_CASE_STATUS',
            ChartType.PIE,
            ChartScopeType.DEFAULT,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ),
          new ChartBuilder(
            'ChartBar',
            'REQUIREMENT_CRITICALITY',
            ChartType.BAR,
            ChartScopeType.DEFAULT,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ),
          new ChartBuilder(
            'ChartCumulative1',
            'REQUIREMENT_CRITICALITY',
            ChartType.CUMULATIVE,
            ChartScopeType.DEFAULT,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ),
          new ChartBuilder(
            'ChartCumulative2',
            'TEST_CASE_STATUS',
            ChartType.CUMULATIVE,
            ChartScopeType.DEFAULT,
            ChartOperation.MAX,
            EntityType.PROJECT,
          ),
          new ChartBuilder(
            'ChartTrend',
            'REQUIREMENT_CRITICALITY',
            ChartType.TREND,
            ChartScopeType.PROJECTS,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ).withAxisRank(1),
          new ChartBuilder(
            'ChartComparative1',
            'REQUIREMENT_CRITICALITY',
            ChartType.COMPARATIVE,
            ChartScopeType.PROJECTS,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ).withAxisRank(1),
          new ChartBuilder(
            'ChartComparative2',
            'TEST_CASE_STATUS',
            ChartType.COMPARATIVE,
            ChartScopeType.PROJECTS,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ).withAxisRank(1),
          new DashboardBuilder('Dashboard').withLinkElements([
            new CustomReportChartBinding('ChartComparative2', {
              row: 1,
              col: 1,
              sizeX: 1,
              sizeY: 1,
            }),
            new CustomReportChartBinding('ChartBar', { row: 1, col: 2, sizeX: 1, sizeY: 1 }),
            new CustomReportChartBinding('ChartPie1', { row: 1, col: 3, sizeX: 1, sizeY: 1 }),
          ]),
        ]),
    ])
    .build();
}

function assertCustomReportWorkspaceDisplaysProjectAndCharts(customReportTree: TreeElement) {
  customReportTree.findRowId('NAME', 'Project').then((idProject) => {
    customReportTree.assertNodeExist(idProject);
    customReportTree.openNodeIfClosed(idProject);
  });
  const charts = [
    'ChartPie1',
    'ChartPie2',
    'ChartBar',
    'ChartTrend',
    'ChartCumulative1',
    'ChartCumulative2',
    'ChartComparative1',
    'ChartComparative2',
  ];
  charts.forEach((chart) => {
    customReportTree.findRowId('NAME', chart).then((id) => {
      customReportTree.assertNodeExist(id);
    });
  });
}

function assertChartDeletionWithCancelAndConfirm(
  customReportTree: TreeElement,
  customReportTreeMenu: CustomReportWorkspaceTreeMenu,
) {
  customReportTree.findRowId('NAME', 'ChartPie1').then((id) => {
    customReportTree.selectNode(id);
  });
  const deleteDialog = customReportTreeMenu.clickDeleteMenuButton('custom-report-tree');
  deleteDialog.cancel();
  customReportTree.findRowId('NAME', 'ChartPie1').then((id) => {
    customReportTree.assertNodeExist(id);
  });
  customReportTreeMenu.clickDeleteMenuButton('custom-report-tree');
  deleteDialog.confirm();
  customReportTree.findRowId('NAME', 'ChartPie1').then((id) => {
    customReportTree.assertNodeNotExist(id);
  });
}

function assertDeleteChartByName(
  customReportTree: TreeElement,
  customReportTreeMenu: CustomReportWorkspaceTreeMenu,
  chartName: string,
) {
  customReportTree.findRowId('NAME', chartName).then((id) => {
    customReportTree.selectNode(id);
  });
  const deleteDialog = customReportTreeMenu.clickDeleteMenuButton('custom-report-tree');
  deleteDialog.confirm();
  customReportTree.findRowId('NAME', chartName).then((id) => {
    customReportTree.assertNodeNotExist(id);
  });
}

function assertMultipleDeleteChartsAndVerifyEmptyDashboard(
  customReportTree: TreeElement,
  customReportTreeMenu: CustomReportWorkspaceTreeMenu,
  dashboardViewPage: DashboardViewPage,
) {
  customReportTree.findRowId('NAME', 'ChartComparative2').then((id) => {
    customReportTree.findRowId('NAME', 'ChartCumulative2').then((id1) => {
      customReportTree.findRowId('NAME', 'ChartPie2').then((id2) => {
        customReportTree.selectNodes([id, id1, id2]);
      });
    });
  });
  const deleteDialog = customReportTreeMenu.clickDeleteMenuButton('custom-report-tree');
  deleteDialog.confirm();
  customReportTree.findRowId('NAME', 'Dashboard').then((id) => {
    customReportTree.selectNode(id);
  });
  dashboardViewPage.assertExists();
  dashboardViewPage.assertEmptyDropzoneIsVisible();
}
