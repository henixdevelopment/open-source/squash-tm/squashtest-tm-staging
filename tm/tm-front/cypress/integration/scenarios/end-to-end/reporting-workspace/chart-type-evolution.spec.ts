import {
  CampaignBuilder,
  ClassicExecutionBuilder,
  ExecutionStepBuilder,
  ExecutionStepsExecutionBuilder,
} from 'cypress/integration/utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { CustomReportWorkspacePage } from '../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import { DatabaseUtils } from '../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  ActionTestStepBuilder,
  TestCaseBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  IterationBuilder,
  IterationTestPlanItemBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  accessAddChartPageFromCustomReportWorkspace,
  assertAxesField,
  configureAxisAttributeSelection,
  renameAndSaveChart,
  selectTypeChartInCreateChartPage,
  testPointX2YHover,
} from '../../scenario-parts/reporting.part';
import { CreateChartViewPage } from '../../../page-objects/pages/custom-report-workspace/chart/create-chart-view.page';
import { MilestonePickerDialogElement } from '../../../page-objects/elements/grid/milestone-picker-dialog.element';
import { TestCaseChartColor, WorkspaceColor } from '../../../utils/color.utils';
import { ChartDefinitionViewPage } from '../../../page-objects/pages/custom-report-workspace/chart/chart-definition-view.page';

describe('On the custom chart creation page: the chart type field, add the "Evolution" option, in the Axes block: display the corresponding axes, when the axes are correctly populated', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
  });

  it('F05-UC0212.CT11 - chart-type-evolution', () => {
    cy.log('Step 1');
    cy.logInAs('admin', 'admin');
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage();
    const createChart = new CreateChartViewPage();
    const chartDefinitionViewPage = new ChartDefinitionViewPage(1);
    const pickerDialogElement = new MilestonePickerDialogElement();
    accessAddChartPageFromCustomReportWorkspace(customReportWorkspacePage, 'Project');

    cy.log('Step 2');
    selectProjectScopeInPerimeterBlock(createChart, pickerDialogElement);

    cy.log('Step 3');
    selectTypeChartInCreateChartPage(createChart, 'Évolution');

    cy.log('Step 4');
    configureAxisAttributesInCreateChartViewPage(createChart);

    cy.log('Step 5');
    assertChartPreviewAndDataConsistency(createChart);

    cy.log('Step 6');
    renameAndSaveChart(
      createChart,
      chartDefinitionViewPage,
      'Nombre de cas de test exécutés par mois',
    );

    cy.log('Step 7');
    assertEvolutionChartElementsInChartViewPage(chartDefinitionViewPage);

    cy.log('Step 8');
    assertInformationBlockForChartDetails(chartDefinitionViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withProjects([
      new ProjectBuilder('Project')
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Test1').withSteps([new ActionTestStepBuilder('action', 'result')]),
          new TestCaseBuilder('Test2').withSteps([new ActionTestStepBuilder('action', 'result')]),
          new TestCaseBuilder('Test3').withSteps([new ActionTestStepBuilder('action', 'result')]),
          new TestCaseBuilder('Test4').withSteps([new ActionTestStepBuilder('action', 'result')]),
          new TestCaseBuilder('Test5').withSteps([new ActionTestStepBuilder('action', 'result')]),
          new TestCaseBuilder('Test6').withSteps([new ActionTestStepBuilder('action', 'result')]),
        ])
        .withCreatedOn('2024-05-09 15:05:25')
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campagne')
            .withIterations([new IterationBuilder('Iteration')])
            .withTestPlanItems([
              new IterationTestPlanItemBuilder('Test1'),
              new IterationTestPlanItemBuilder('Test2', 'SUCCESS').withExecutions([
                new ClassicExecutionBuilder('SUCCESS').withExecutionStep([
                  new ExecutionStepBuilder('SUCCESS').withExecutionStepExecution([
                    new ExecutionStepsExecutionBuilder('admin').withLastExecutedOn(
                      '2024-05-09 15:05:25',
                    ),
                  ]),
                ]),
              ]),
              new IterationTestPlanItemBuilder('Test3', 'SUCCESS').withExecutions([
                new ClassicExecutionBuilder('SUCCESS').withExecutionStep([
                  new ExecutionStepBuilder('SUCCESS').withExecutionStepExecution([
                    new ExecutionStepsExecutionBuilder('admin').withLastExecutedOn(
                      '2024-04-09 15:05:25',
                    ),
                  ]),
                ]),
              ]),
              new IterationTestPlanItemBuilder('Test4', 'FAILURE').withExecutions([
                new ClassicExecutionBuilder('FAILURE').withExecutionStep([
                  new ExecutionStepBuilder('SUCCESS').withExecutionStepExecution([
                    new ExecutionStepsExecutionBuilder('admin').withLastExecutedOn(
                      '2024-03-09 15:05:25',
                    ),
                  ]),
                ]),
              ]),
              new IterationTestPlanItemBuilder('Test5', 'FAILURE').withExecutions([
                new ClassicExecutionBuilder('FAILURE').withExecutionStep([
                  new ExecutionStepBuilder('SUCCESS').withExecutionStepExecution([
                    new ExecutionStepsExecutionBuilder('admin').withLastExecutedOn(
                      '2024-02-09 15:05:25',
                    ),
                  ]),
                ]),
              ]),
              new IterationTestPlanItemBuilder('Test6', 'SUCCESS').withExecutions([
                new ClassicExecutionBuilder('SUCCESS').withExecutionStep([
                  new ExecutionStepBuilder('SUCCESS').withExecutionStepExecution([
                    new ExecutionStepsExecutionBuilder('admin').withLastExecutedOn(
                      '2024-01-09 15:05:25',
                    ),
                  ]),
                ]),
              ]),
            ]),
        ]),
    ])
    .build();
}

function selectProjectScopeInPerimeterBlock(
  createChart: CreateChartViewPage,
  pickerDialogElement: MilestonePickerDialogElement,
) {
  createChart.selectScopeCriterion(1);
  pickerDialogElement.confirm();
  createChart.assertCriterionChecked(1);
}

function configureAxisAttributesInCreateChartViewPage(createChart: CreateChartViewPage) {
  createChart.measureSelector.assertExists();
  createChart.axisSelector.assertExists();
  configureAxisAttributeSelection(createChart.measureSelector, 54, 'Comptage', 'id');
  configureAxisAttributeSelection(createChart.axisSelector, 57, 'Par jour', 'Date');
  createChart.axisSelector.selectOption('Par mois');
  createChart.axisSelector.checkOption('Par mois');
}

function assertChartPreviewAndDataConsistency(createChart: CreateChartViewPage) {
  createChart.assertChartTypeExist('cumulative');
  createChart.assertChartYAxisTitle("ID d'item de plan d'exécution");
  createChart.assertChartXAxisLabel(0, 'Jamais');
  const x2AxisLabels = ['Fév 2024', 'Mar 2024', 'Avr 2024', 'Mai 2024'];
  x2AxisLabels.forEach((label, index) => {
    createChart.assertChartX2AxisLabel(index, label);
  });
  createChart.assertChartX2AxisTitle('Date de dernière exécution');
  createChart.assertChartBarColor(0, TestCaseChartColor.NEVER_EXECUTED);
  [1, 2, 3, 4, 5].forEach((index) => {
    createChart.assertChartPointColor(index, TestCaseChartColor.EXECUTED);
  });
  createChart.assertChartTitle('Nouveau graphique');

  createChart.hoverOverXYSubplotBottom();
  createChart.assertLegendOnPointHover('text', 'Jamais');
  createChart.assertElementCountOnPointHover('text', '1');
  const hoverPoints = [
    { x: 0, y: 252, text: '1 Jan, 2024', id: '1' },
    { x: 40, y: 200, text: '1 Fév, 2024', id: '2' },
    { x: 80, y: 150, text: '1 Mar, 2024', id: '3' },
    { x: 120, y: 87, text: '1 Avr, 2024', id: '4' },
    { x: 150, y: 20, text: '1 Mai, 2024', id: '5' },
  ];

  hoverPoints.forEach(({ x, y, text, id }) => {
    testPointX2YHover(createChart, x, y, 'text', text, id);
  });
}

function assertEvolutionChartElementsInChartViewPage(
  chartDefinitionViewPage: ChartDefinitionViewPage,
) {
  chartDefinitionViewPage.assertChartTypeExist('cumulative');
  chartDefinitionViewPage.assertChartYAxisTitle("ID d'item de plan d'exécution");
  chartDefinitionViewPage.assertChartXAxisLabel(0, 'Jamais');
  const x2AxisLabels = ['Fév 2024', 'Mar 2024', 'Avr 2024', 'Mai 2024'];
  x2AxisLabels.forEach((label, index) => {
    chartDefinitionViewPage.assertChartX2AxisLabel(index, label);
  });
  chartDefinitionViewPage.assertChartX2AxisTitle('Date de dernière exécution');
  chartDefinitionViewPage.assertChartBarColor(0, TestCaseChartColor.NEVER_EXECUTED);
  [1, 2, 3, 4, 5].forEach((index) => {
    chartDefinitionViewPage.assertChartPointColor(index, TestCaseChartColor.EXECUTED);
  });

  chartDefinitionViewPage.hoverOverXYSubplotBottom();
  chartDefinitionViewPage.assertLegendOnPointHover('tspan', 'Jamais');
  chartDefinitionViewPage.assertElementCountOnPointHover('tspan', '1');
  const hoverPoints = [
    { x: 0, y: 430, text: '1 Jan, 2024', id: '1' },
    { x: 70, y: 330, text: '1 Fév, 2024', id: '2' },
    { x: 140, y: 230, text: '1 Mar, 2024', id: '3' },
    { x: 210, y: 130, text: '1 Avr, 2024', id: '4' },
    { x: 280, y: 30, text: '1 Mai, 2024', id: '5' },
  ];

  hoverPoints.forEach(({ x, y, text, id }) => {
    testPointX2YHover(chartDefinitionViewPage, x, y, 'tspan', text, id);
  });
}

function assertInformationBlockForChartDetails(chartDefinitionViewPage: ChartDefinitionViewPage) {
  chartDefinitionViewPage.showInformationPanel();
  chartDefinitionViewPage.assertHasCreationDate();
  chartDefinitionViewPage.assertCreatedBy('admin');
  chartDefinitionViewPage.assertLastModification('jamais');
  chartDefinitionViewPage.checkPerimeter('Project');
  assertAxesField(
    chartDefinitionViewPage,
    'measure',
    'sqtm-core-custom-report:up',
    'Mesurer',
    WorkspaceColor.CAMPAIGN,
  );
  chartDefinitionViewPage.checkMeasureInfo(
    "Items de plan d'exécution | ID d'item de plan d'exécution - Comptage",
  );
  assertAxesField(
    chartDefinitionViewPage,
    'axis',
    'sqtm-core-custom-report:right',
    'Par',
    WorkspaceColor.CAMPAIGN,
  );
  chartDefinitionViewPage.checkAxisInfo(
    "Items de plan d'exécution | Date de dernière exécution - Par mois",
  );
}
