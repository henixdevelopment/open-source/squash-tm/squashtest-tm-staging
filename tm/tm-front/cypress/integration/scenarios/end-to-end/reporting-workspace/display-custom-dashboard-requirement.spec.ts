import { beforeEach } from 'mocha';
import { DatabaseUtils } from '../../../utils/database.utils';

import { PrerequisiteBuilder } from '../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  RequirementBuilder,
  RequirementFolderBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  TestCaseBuilder,
  TestCaseFolderBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  ChartBuilder,
  CustomReportChartBinding,
  CustomReportReportBinding,
  DashboardBuilder,
  ReportBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/custom-report-prerequisite-builders';
import {
  ChartOperation,
  ChartScopeType,
  ChartType,
} from '../../../../../projects/sqtm-core/src/lib/model/custom-report/chart-definition.model';
import { EntityType } from '../../../../../projects/sqtm-core/src/lib/model/entity.model';
import { UserBuilder } from '../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { addClearanceToUser } from '../../../utils/end-to-end/api/add-permissions';
import { PluginNameSpace } from '../../../../../projects/sqtm-core/src/lib/model/custom-report/report-definition.model';
import { mockReportDefinitionParameterTestCase } from '../../../data-mock/report-definition.data-mock';
import { RequirementWorkspacePage } from '../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { selectNodeInAnotherNode, selectNodeProject } from '../../scenario-parts/requirement.part';
import { RequirementLibraryViewPage } from '../../../page-objects/pages/requirement-workspace/requirement-library/requirement-library-view.page';
import { NavBarElement } from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {
  dragChartToDashboardEmpty,
  selectNodeInCustomReportWorkspace,
} from '../../scenario-parts/reporting.part';
import { DashboardViewPage } from '../../../page-objects/pages/custom-report-workspace/dashboard-view.page';
import { MenuElement } from '../../../utils/menu.element';
import { RequirementFolderViewPage } from '../../../page-objects/pages/requirement-workspace/requirement-folder/requirement-folder-view.page';
import { RequirementMultiSelectionPage } from '../../../page-objects/pages/requirement-workspace/requirement/requirement-multi-selection.page';
import { selectProjectInTestCaseWorkspace } from '../../scenario-parts/testcase.part';
import { TestCaseLibraryViewPage } from '../../../page-objects/pages/test-case-workspace/test-case/test-case-library-view.page';
import { selectNodeProjectInCampaignWorkspace } from '../../scenario-parts/campaign.part';
import { CampaignLibraryViewPage } from '../../../page-objects/pages/campaign-workspace/campaign-library/campaign-library.page';
import { HomeWorkspacePage } from '../../../page-objects/pages/home-workspace/home-workspace.page';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Favorite button functionality in the requirements workspace, when clicking on the [Favorite] button and selecting the "Requirements" option.', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F05-UC035.CT01 - display-custom-dashboard-requirement', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    const dashboardViewPage = new DashboardViewPage(1);
    const requirementLibraryViewPage = new RequirementLibraryViewPage(1);
    const requirementFolderViewPage = new RequirementFolderViewPage();
    const multiSelection = new RequirementMultiSelectionPage();

    assertFavoriteDashboardMessage(requirementWorkspacePage, requirementLibraryViewPage);

    cy.log('Step 2');
    assertDashboardFavoriteSelectionInRequirementWorkspace(dashboardViewPage);

    cy.log('Step 3');
    assertFavoriteDashboardDisplayInProjectRequirementWorkspace(requirementLibraryViewPage);

    cy.log('Step 4');
    assertFavoriteDashboardDisplayInFolderRequirementWorkspace(
      requirementWorkspacePage,
      requirementFolderViewPage,
    );

    cy.log('Step 5');
    assertFavoriteDashboardDisplayInMultiProjectsRequirementWorkspace(
      requirementWorkspacePage,
      requirementLibraryViewPage,
    );

    cy.log('Step 6');
    assertFavoriteDashboardDisplayInMultiSelectionRequirementWorkspace(
      requirementWorkspacePage,
      multiSelection,
    );

    cy.log('Step 7');
    assertDefaultAndFavoriteDashboardDisplay(requirementWorkspacePage, requirementLibraryViewPage);

    cy.log('Step 8');
    uncheckRequirementDashboardAndVerifyInWorkspaceDashboardMessage(
      dashboardViewPage,
      requirementLibraryViewPage,
      requirementFolderViewPage,
      multiSelection,
    );

    cy.log('Step 9');
    emptyAndAddDashboardAndVerifyUpdatesRequirementWorkspace(
      requirementLibraryViewPage,
      dashboardViewPage,
    );

    cy.log('Step 10');
    assertFavoriteDashboardInDifferentWorkspaces();

    cy.log('Step 11');
    assertFavoriteDashboardWithAnotherUser(requirementLibraryViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([
      new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley'),
      new UserBuilder('WayneR').withFirstName('Wayne').withLastName('Rooney'),
    ])
    .withProfiles([
      new ProfileBuilder('User A', [
        ProfileBuilderPermission.REQUIREMENT_READ,
        ProfileBuilderPermission.TEST_CASE_READ,
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CUSTOM_REPORT_READ,
        ProfileBuilderPermission.CUSTOM_REPORT_CREATE,
        ProfileBuilderPermission.CUSTOM_REPORT_WRITE,
        ProfileBuilderPermission.CUSTOM_REPORT_DELETE,
      ]),
      new ProfileBuilder('User B', [
        ProfileBuilderPermission.REQUIREMENT_READ,
        ProfileBuilderPermission.TEST_CASE_READ,
        ProfileBuilderPermission.CAMPAIGN_READ,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('ProjectA')
        .withRequirementLibraryNodes([
          new RequirementFolderBuilder('Folder').withRequirementLibraryNodes([
            new RequirementBuilder('Req1').withCriticality('CRITICAL'),
            new RequirementBuilder('Req2').withCriticality('MAJOR'),
            new RequirementBuilder('Req3').withCriticality('CRITICAL'),
            new RequirementBuilder('Req4').withCriticality('MINOR'),
          ]),
        ])
        .withTestCaseLibraryNodes([
          new TestCaseFolderBuilder('Folder').withTestCaseLibraryNodes([
            new TestCaseBuilder('Test1').withStatus('TO_BE_UPDATED'),
            new TestCaseBuilder('Test2').withStatus('APPROVED'),
            new TestCaseBuilder('Test3').withStatus('OBSOLETE'),
            new TestCaseBuilder('Test4').withStatus('WORK_IN_PROGRESS'),
            new TestCaseBuilder('Test5').withStatus('TO_BE_UPDATED'),
          ]),
        ])
        .withCustomReportLibraryNode([
          new ChartBuilder(
            'ChartPie',
            'REQUIREMENT_CRITICALITY',
            ChartType.PIE,
            ChartScopeType.DEFAULT,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ),
          new ChartBuilder(
            'ChartTrend',
            'REQUIREMENT_CRITICALITY',
            ChartType.TREND,
            ChartScopeType.PROJECTS,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ).withAxisRank(1),
          new ChartBuilder(
            'ChartBar',
            'REQUIREMENT_CRITICALITY',
            ChartType.BAR,
            ChartScopeType.DEFAULT,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ),
          new ChartBuilder(
            'ChartComparative',
            'REQUIREMENT_CRITICALITY',
            ChartType.COMPARATIVE,
            ChartScopeType.PROJECTS,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ).withAxisRank(1),
          new ReportBuilder(
            'Report',
            PluginNameSpace.TEST_CASE_REPORT,
            JSON.stringify(mockReportDefinitionParameterTestCase()),
          ),
          new DashboardBuilder('Dashboard').withLinkElements([
            new CustomReportChartBinding('ChartComparative', {
              row: 1,
              col: 1,
              sizeX: 1,
              sizeY: 1,
            }),
            new CustomReportChartBinding('ChartBar', { row: 2, col: 2, sizeX: 1, sizeY: 1 }),
            new CustomReportChartBinding('ChartPie', { row: 1, col: 3, sizeX: 1, sizeY: 1 }),
            new CustomReportReportBinding('Report', { row: 1, col: 4, sizeX: 1, sizeY: 1 }),
          ]),
        ]),
      new ProjectBuilder('ProjectB').withRequirementLibraryNodes([
        new RequirementBuilder('Req1').withCriticality('MINOR'),
        new RequirementBuilder('Req2').withCriticality('UNDEFINED'),
      ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, 11, [1, 2]);
  addClearanceToUser(3, 12, [1, 2]);
}

function assertFavoriteDashboardMessage(
  requirementWorkspacePage: RequirementWorkspacePage,
  requirementLibraryViewPage: RequirementLibraryViewPage,
) {
  selectNodeProject(requirementWorkspacePage, 'ProjectA');
  requirementLibraryViewPage.assertExists();
  requirementLibraryViewPage.showDashboard();
  const dashboardPanel = requirementLibraryViewPage.showDashboard();
  dashboardPanel.assertFavoriteButtonExist();
  dashboardPanel.clickFavoriteButton();
  dashboardPanel.assertNoDashboardSelectedMessage();
}

function assertDashboardFavoriteSelectionInRequirementWorkspace(
  dashboardViewPage: DashboardViewPage,
) {
  const customReportWorkspace = NavBarElement.navigateToCustomReportWorkspace();
  selectNodeInCustomReportWorkspace(customReportWorkspace, 'ProjectA', 'Dashboard');
  const favoriteToolbar = dashboardViewPage.showFavoriteList();
  favoriteToolbar.click();
  const favoriteMenu = new MenuElement('favoriteMenu');
  favoriteMenu.item('requirements').click();
  cy.clickVoid();
}

function assertFavoriteDashboardDisplayInProjectRequirementWorkspace(
  requirementLibraryViewPage: RequirementLibraryViewPage,
) {
  const requirementWorkspacePage = NavBarElement.navigateToRequirementWorkspace();
  selectNodeProject(requirementWorkspacePage, 'ProjectA');
  requirementLibraryViewPage.assertExists();
  const dashboardPanel = requirementLibraryViewPage.showDashboard();
  dashboardPanel.assertDefaultButtonExist();
  dashboardPanel.assertTitleExist('Dashboard');
  dashboardPanel.assertCustomDashboardExist();
  dashboardPanel.asserChartDashboardBinding([1, 2, 3]);
  dashboardPanel.asserReportDashboardBinding([1]);
  dashboardPanel.assertCannotResizableElement();
  dashboardPanel.assertCannotMoveElement(1);
  dashboardPanel.assertCannotMoveElement(2);
  ['Mineure', 'Majeure', 'Critique'].forEach((legend, index) => {
    dashboardPanel.assertChartLegends(index, legend);
  });
}

function assertFavoriteDashboardDisplayInFolderRequirementWorkspace(
  requirementWorkspacePage: RequirementWorkspacePage,
  requirementFolderViewPage: RequirementFolderViewPage,
) {
  selectNodeInAnotherNode(requirementWorkspacePage, 'ProjectA', 'Folder');
  requirementFolderViewPage.assertExists();
  const folderDashboardPanel = requirementFolderViewPage.showDashboard();
  folderDashboardPanel.assertDefaultButtonExist();
  folderDashboardPanel.assertTitleExist('Dashboard');
  folderDashboardPanel.assertCustomDashboardExist();
  folderDashboardPanel.asserChartDashboardBinding([1, 2, 3]);
  folderDashboardPanel.asserReportDashboardBinding([1]);
  folderDashboardPanel.assertCannotResizableElement();
  folderDashboardPanel.assertCannotMoveElement(1);
  folderDashboardPanel.assertCannotMoveElement(2);
  ['Mineure', 'Majeure', 'Critique'].forEach((legend, index) => {
    folderDashboardPanel.assertChartLegends(index, legend);
  });
}

function assertFavoriteDashboardDisplayInMultiProjectsRequirementWorkspace(
  requirementWorkspacePage: RequirementWorkspacePage,
  requirementLibraryViewPage: RequirementLibraryViewPage,
) {
  requirementWorkspacePage.tree.findRowId('NAME', 'ProjectA').then((id) => {
    requirementWorkspacePage.tree.findRowId('NAME', 'ProjectB').then((id1) => {
      requirementWorkspacePage.tree.selectNodes([id, id1]);
    });
  });
  const dashboardPanel = requirementLibraryViewPage.showDashboard();
  dashboardPanel.assertDefaultButtonExist();
  dashboardPanel.assertRefreshDashboardMessage();
  dashboardPanel.clickRefresh();
  dashboardPanel.assertTitleExist('Dashboard');
  dashboardPanel.assertCustomDashboardExist();
  dashboardPanel.asserChartDashboardBinding([1, 2, 3]);
  dashboardPanel.asserReportDashboardBinding([1]);
  dashboardPanel.assertCannotResizableElement();
  dashboardPanel.assertCannotMoveElement(1);
  dashboardPanel.assertCannotMoveElement(2);
  ['Non définie', 'Mineure', 'Majeure', 'Critique'].forEach((legend, index) => {
    dashboardPanel.assertChartLegends(index, legend);
  });
}

function assertFavoriteDashboardDisplayInMultiSelectionRequirementWorkspace(
  requirementWorkspacePage: RequirementWorkspacePage,
  multiSelection: RequirementMultiSelectionPage,
) {
  requirementWorkspacePage.tree.findRowId('NAME', 'Folder').then((id) => {
    requirementWorkspacePage.tree.openNodeIfClosed(id);
  });
  requirementWorkspacePage.tree.findRowId('NAME', 'Req2').then((id) => {
    requirementWorkspacePage.tree.findRowId('NAME', 'Req3').then((id1) => {
      requirementWorkspacePage.tree.selectNodes([id, id1]);
    });
  });
  multiSelection.assertExists();
  const multiSelectionDashboardPanel = multiSelection.showDashboard();
  multiSelectionDashboardPanel.assertRefreshDashboardMessage();
  multiSelectionDashboardPanel.assertDefaultButtonExist();
  multiSelectionDashboardPanel.clickRefresh();
  multiSelectionDashboardPanel.assertCustomDashboardExist();
  multiSelectionDashboardPanel.asserChartDashboardBinding([1, 2, 3]);
  multiSelectionDashboardPanel.asserReportDashboardBinding([1]);
  multiSelectionDashboardPanel.assertCannotResizableElement();
  multiSelectionDashboardPanel.assertCannotMoveElement(1);
  multiSelectionDashboardPanel.assertCannotMoveElement(2);
  ['Majeure', 'Critique'].forEach((legend, index) => {
    multiSelectionDashboardPanel.assertChartLegends(index, legend);
  });
}

function assertDefaultAndFavoriteDashboardDisplay(
  requirementWorkspacePage: RequirementWorkspacePage,
  requirementLibraryViewPage: RequirementLibraryViewPage,
) {
  selectNodeProject(requirementWorkspacePage, 'ProjectA');
  const dashboardPanel = requirementLibraryViewPage.showDashboard();
  dashboardPanel.assertDefaultButtonExist();
  dashboardPanel.clickDefaultButton();
  dashboardPanel.descriptionChart.assertChartExist();
  dashboardPanel.criticalityChart.assertChartExist();
  dashboardPanel.statusChart.assertChartExist();
  dashboardPanel.coverageByCriticalityChart.assertChartExist();
  dashboardPanel.orphanRequirementChart.assertChartExist();
  dashboardPanel.validationByCriticalityChart.assertChartExist();
  dashboardPanel.clickFavoriteButton();
  dashboardPanel.asserChartDashboardBinding([1, 2, 3]);
  dashboardPanel.asserReportDashboardBinding([1]);
}

function uncheckRequirementDashboardAndVerifyInWorkspaceDashboardMessage(
  dashboardViewPage: DashboardViewPage,
  requirementLibraryViewPage: RequirementLibraryViewPage,
  requirementFolderViewPage: RequirementFolderViewPage,
  multiSelection: RequirementMultiSelectionPage,
) {
  const customReportWorkspace = NavBarElement.navigateToCustomReportWorkspace();
  selectNodeInCustomReportWorkspace(customReportWorkspace, 'ProjectA', 'Dashboard');
  const favoriteToolbar = dashboardViewPage.showFavoriteList();
  favoriteToolbar.click();
  const favoriteMenu = new MenuElement('favoriteMenu');
  favoriteMenu.item('requirements').click();
  cy.clickVoid();
  const requirementWorkspacePage = NavBarElement.navigateToRequirementWorkspace();
  selectNodeProject(requirementWorkspacePage, 'ProjectA');
  requirementLibraryViewPage.assertExists();
  const dashboardPanel = requirementLibraryViewPage.showDashboard();
  dashboardPanel.assertFavoriteButtonNotExist();
  dashboardPanel.assertNoDashboardSelectedMessage();
  selectNodeInAnotherNode(requirementWorkspacePage, 'ProjectA', 'Folder');
  requirementFolderViewPage.assertExists();
  const folderDashboardPanel = requirementFolderViewPage.showDashboard();
  folderDashboardPanel.assertFavoriteButtonNotExist();
  folderDashboardPanel.assertNoDashboardSelectedMessage();
  requirementWorkspacePage.tree.findRowId('NAME', 'Req2').then((id) => {
    requirementWorkspacePage.tree.findRowId('NAME', 'Req3').then((id1) => {
      requirementWorkspacePage.tree.selectNodes([id, id1]);
    });
  });
  multiSelection.assertExists();
  const multiSelectionDashboardPanel = multiSelection.showDashboard();
  multiSelectionDashboardPanel.assertFavoriteButtonNotExist();
  multiSelectionDashboardPanel.assertRefreshDashboardMessage();
  NavBarElement.navigateToCustomReportWorkspace();
  selectNodeInCustomReportWorkspace(customReportWorkspace, 'ProjectA', 'Dashboard');
  favoriteToolbar.click();
  favoriteMenu.item('requirements').click();
  cy.clickVoid();
}

function emptyAndAddDashboardAndVerifyUpdatesRequirementWorkspace(
  requirementLibraryViewPage: RequirementLibraryViewPage,
  dashboardViewPage: DashboardViewPage,
) {
  NavBarElement.logout();
  cy.logInAs('admin', 'admin');
  const customReportWorkspace = NavBarElement.navigateToCustomReportWorkspace();
  customReportWorkspace.tree.findRowId('NAME', 'ProjectA').then((id) => {
    customReportWorkspace.tree.openNodeIfClosed(id);
  });
  customReportWorkspace.tree.findRowId('NAME', 'ChartPie').then((id1) => {
    customReportWorkspace.tree.findRowId('NAME', 'ChartBar').then((id2) => {
      customReportWorkspace.tree.findRowId('NAME', 'ChartComparative').then((id3) => {
        customReportWorkspace.tree.findRowId('NAME', 'Report').then((id4) => {
          customReportWorkspace.tree.selectNodes([id1, id2, id3, id4]);
        });
      });
    });
  });
  const deleteDialog = customReportWorkspace.treeMenu.clickDeleteMenuButton('custom-report-tree');
  deleteDialog.confirm();
  NavBarElement.logout();
  cy.logInAs('RonW', 'admin');
  NavBarElement.navigateToCustomReportWorkspace();
  selectNodeInCustomReportWorkspace(customReportWorkspace, 'ProjectA', 'Dashboard');
  dashboardViewPage.assertEmptyDropzoneIsVisible();
  const requirementWorkspacePage = NavBarElement.navigateToRequirementWorkspace();
  selectNodeProject(requirementWorkspacePage, 'ProjectA');
  requirementLibraryViewPage.assertExists();
  const dashboardPanel = requirementLibraryViewPage.showDashboard();
  dashboardPanel.assertFavoriteDashboardIsEmptyMessage();
  NavBarElement.navigateToCustomReportWorkspace();
  dragChartToDashboardEmpty(customReportWorkspace, dashboardViewPage, 'ChartTrend');
  NavBarElement.navigateToRequirementWorkspace();
  selectNodeProject(requirementWorkspacePage, 'ProjectA');
  dashboardPanel.assertCustomDashboardExist();
}

function assertFavoriteDashboardInDifferentWorkspaces() {
  const testCaseWorkspacePage = NavBarElement.navigateToTestCaseWorkspace();
  selectProjectInTestCaseWorkspace(testCaseWorkspacePage, 'ProjectA');
  const testCaseLibraryViewPage = new TestCaseLibraryViewPage('*');
  testCaseLibraryViewPage.assertExists();
  const testCaseDashboardPanel = testCaseLibraryViewPage.showDashboard();
  testCaseDashboardPanel.clickFavoriteButton();
  testCaseDashboardPanel.assertCustomDashboardNotExist();

  const campaignWorkspacePage = NavBarElement.navigateToCampaignWorkspace();
  selectNodeProjectInCampaignWorkspace(campaignWorkspacePage, 'ProjectA');
  const campaignLibraryViewPage = new CampaignLibraryViewPage('campaign-view-header');
  campaignLibraryViewPage.assertExists();
  const campaignDashboardPanel = campaignLibraryViewPage.showDashboard();
  campaignDashboardPanel.clickFavoriteButton();
  campaignDashboardPanel.assertCustomDashboardNotExist();

  const homeWorkspacePage = HomeWorkspacePage.initTestAtPageWithModel();
  homeWorkspacePage.showDashboard();
  homeWorkspacePage.assertNoDashboardSelectedMessage();
}

function assertFavoriteDashboardWithAnotherUser(
  requirementLibraryViewPage: RequirementLibraryViewPage,
) {
  NavBarElement.logout();
  cy.logInAs('WayneR', 'admin');
  const requirementWorkspacePage = NavBarElement.navigateToRequirementWorkspace();
  selectNodeProject(requirementWorkspacePage, 'ProjectA');
  requirementLibraryViewPage.assertExists();
  const dashboardPanel = requirementLibraryViewPage.showDashboard();
  dashboardPanel.clickFavoriteButton();
  dashboardPanel.assertNoDashboardSelectedMessage();
}
