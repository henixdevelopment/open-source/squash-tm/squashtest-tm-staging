import { DatabaseUtils } from '../../../utils/database.utils';
import { CustomReportWorkspacePage } from '../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import { PrerequisiteBuilder } from '../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { RequirementBuilder } from '../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import { TestCaseBuilder } from '../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { CampaignBuilder } from '../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  ChartBuilder,
  ChartFilterBuilder,
  CustomReportChartBinding,
  DashboardBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/custom-report-prerequisite-builders';
import {
  ChartOperation,
  ChartScopeType,
  ChartType,
} from '../../../../../projects/sqtm-core/src/lib/model/custom-report/chart-definition.model';
import { EntityType } from '../../../../../projects/sqtm-core/src/lib/model/entity.model';
import {
  assertAxesField,
  selectNodeInCustomReportWorkspace,
} from '../../scenario-parts/reporting.part';
import { GroupedMultiListElement } from '../../../page-objects/elements/filters/grouped-multi-list.element';
import { DashboardViewPage } from '../../../page-objects/pages/custom-report-workspace/dashboard-view.page';
import { ModifyChartViewPage } from '../../../page-objects/pages/custom-report-workspace/chart/modify-chart-view-page';
import { RequirementVersionsLinkToVerifyingTestCaseBuilder } from '../../../utils/end-to-end/prerequisite/builders/link-builders';
import { ChartDefinitionViewPage } from '../../../page-objects/pages/custom-report-workspace/chart/chart-definition-view.page';
import { WorkspaceColor } from '../../../utils/color.utils';
import { UserBuilder } from '../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('This test will verify the creation, modification, and compliance checking of various chart types, while ensuring that interaction with the graphical elements is functioning correctly', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F05-UC023.CT02 - chart-modification', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage();
    const chartDefinitionViewPage = new ChartDefinitionViewPage(1);
    const modifyChartViewPage = new ModifyChartViewPage();
    const groupedMultiListElement = new GroupedMultiListElement();
    const dashboardViewPage = new DashboardViewPage(1);

    accessChartPageFromCustomReportWorkspace(customReportWorkspacePage, chartDefinitionViewPage);

    cy.log('Step 2');
    assertModifyButtonPresenceAndHoverEffectByChartName(
      customReportWorkspacePage,
      chartDefinitionViewPage,
    );

    cy.log('Step 3');
    assertChartPieCreationConformity(chartDefinitionViewPage);

    cy.log('Step 4');
    modifyChartPieAndVerifyChangesInDashboardViewPage(
      customReportWorkspacePage,
      chartDefinitionViewPage,
      dashboardViewPage,
      modifyChartViewPage,
      groupedMultiListElement,
    );

    cy.log('Step 5');
    assertModifyButtonPresenceAndHoverEffectByChartName(
      customReportWorkspacePage,
      chartDefinitionViewPage,
      'Chart2',
    );

    cy.log('Step 6');
    assertChartBarCreationConformity(chartDefinitionViewPage);

    cy.log('Step 7');
    modifyChartBarAndVerifyChangesInDashboardViewPage(
      customReportWorkspacePage,
      chartDefinitionViewPage,
      dashboardViewPage,
      modifyChartViewPage,
      groupedMultiListElement,
    );

    cy.log('Step 8');
    assertModifyButtonPresenceAndHoverEffectByChartName(
      customReportWorkspacePage,
      chartDefinitionViewPage,
      'Chart3',
    );

    cy.log('Step 9');
    assertChartCumulativeCreationConformity(chartDefinitionViewPage);

    cy.log('Step 10');
    modifyChartCumulativeAndVerifyChanges(
      customReportWorkspacePage,
      chartDefinitionViewPage,
      modifyChartViewPage,
    );

    cy.log('Step 11');
    assertModifyButtonPresenceAndHoverEffectByChartName(
      customReportWorkspacePage,
      chartDefinitionViewPage,
      'Chart4',
    );

    cy.log('Step 12');
    assertChartTrendCreationConformity(chartDefinitionViewPage);

    cy.log('Step 13');
    modifyChartTrendAndVerifyChanges(
      customReportWorkspacePage,
      chartDefinitionViewPage,
      modifyChartViewPage,
      groupedMultiListElement,
    );

    cy.log('Step 14');
    assertModifyButtonPresenceAndHoverEffectByChartName(
      customReportWorkspacePage,
      chartDefinitionViewPage,
      'Chart5',
    );

    cy.log('Step 15');
    assertChartComparativeCreationConformity(chartDefinitionViewPage);

    cy.log('Step 16');
    modifyChartComparativeAndVerifyChanges(
      customReportWorkspacePage,
      chartDefinitionViewPage,
      modifyChartViewPage,
      groupedMultiListElement,
    );

    cy.log('Step 17');
    assertChartDetailsAndElements(chartDefinitionViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('chart create', [
        ProfileBuilderPermission.CUSTOM_REPORT_READ,
        ProfileBuilderPermission.CUSTOM_REPORT_CREATE,
        ProfileBuilderPermission.CUSTOM_REPORT_WRITE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project1')
        .withRequirementLibraryNodes([
          new RequirementBuilder('Req1')
            .withCriticality('CRITICAL')
            .withRequirementStatus('WORK_IN_PROGRESS'),
          new RequirementBuilder('Req2')
            .withCriticality('MAJOR')
            .withRequirementStatus('UNDER_REVIEW'),
          new RequirementBuilder('Req3').withCriticality('MINOR').withRequirementStatus('APPROVED'),
          new RequirementBuilder('Req4')
            .withCriticality('UNDEFINED')
            .withRequirementStatus('OBSOLETE'),
        ])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('TC1').withStatus('WORK_IN_PROGRESS').withImportance('MEDIUM'),
          new TestCaseBuilder('TC2').withStatus('APPROVED').withImportance('HIGH'),
          new TestCaseBuilder('TC3').withStatus('TO_BE_UPDATED').withImportance('VERY_HIGH'),
          new TestCaseBuilder('TC4').withStatus('APPROVED').withImportance('VERY_HIGH'),
        ])
        .withCampaignLibraryNodes([new CampaignBuilder('Campaign')])
        .withLinks([
          new RequirementVersionsLinkToVerifyingTestCaseBuilder(['Req1'], 'TC1'),
          new RequirementVersionsLinkToVerifyingTestCaseBuilder(['Req2'], 'TC1'),
          new RequirementVersionsLinkToVerifyingTestCaseBuilder(['Req3'], 'TC1'),
          new RequirementVersionsLinkToVerifyingTestCaseBuilder(['Req2'], 'TC2'),
        ])
        .withCustomReportLibraryNode([
          new ChartBuilder(
            'Chart1',
            'REQUIREMENT_CRITICALITY',
            ChartType.PIE,
            ChartScopeType.DEFAULT,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ).withFilters([
            new ChartFilterBuilder(ChartOperation.IN, 'REQUIREMENT_CRITICALITY', 'CRITICAL'),
          ]),
          new ChartBuilder(
            'Chart2',
            'REQUIREMENT_CRITICALITY',
            ChartType.BAR,
            ChartScopeType.DEFAULT,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ),
          new ChartBuilder(
            'Chart3',
            'TEST_CASE_STATUS',
            ChartType.CUMULATIVE,
            ChartScopeType.DEFAULT,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ).withFilters([
            new ChartFilterBuilder(ChartOperation.IN, 'TEST_CASE_IMPORTANCE', 'VERY_HIGH'),
            new ChartFilterBuilder(ChartOperation.IN, 'TEST_CASE_STATUS', 'TO_BE_UPDATED'),
          ]),
          new ChartBuilder(
            'Chart4',
            'TEST_CASE_IMPORTANCE',
            ChartType.TREND,
            ChartScopeType.PROJECTS,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ).withAxisRank(1),
          new ChartBuilder(
            'Chart5',
            'REQUIREMENT_CRITICALITY',
            ChartType.COMPARATIVE,
            ChartScopeType.PROJECTS,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ).withAxisRank(1),
          new DashboardBuilder('TdB1').withLinkElements([
            new CustomReportChartBinding('Chart1', { row: 1, col: 1, sizeX: 1, sizeY: 1 }),
            new CustomReportChartBinding('Chart2', { row: 1, col: 2, sizeX: 1, sizeY: 1 }),
          ]),
        ]),
      new ProjectBuilder('Project2')
        .withRequirementLibraryNodes([
          new RequirementBuilder('Req1').withCriticality('CRITICAL'),
          new RequirementBuilder('Req2').withCriticality('MAJOR'),
          new RequirementBuilder('Req3').withCriticality('MINOR'),
          new RequirementBuilder('Req4').withCriticality('UNDEFINED'),
        ])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Test1'),
          new TestCaseBuilder('Test2'),
          new TestCaseBuilder('Test3'),
        ])
        .withCampaignLibraryNodes([new CampaignBuilder('Campaign')]),
    ])
    .build();
}

function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1, 2]);
}

function accessChartPageFromCustomReportWorkspace(
  customReportWorkspacePage: CustomReportWorkspacePage,
  chartDefinitionViewPage: ChartDefinitionViewPage,
) {
  selectNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project1', 'Chart1');
  chartDefinitionViewPage.assertExists();
  chartDefinitionViewPage.assertChartTypeExist('Pie');
  chartDefinitionViewPage.assertChartTitle('Chart1');
  chartDefinitionViewPage.assertChartLegends(0, 'Critique');
}

function assertModifyButtonPresenceAndHoverEffectByChartName(
  customReportWorkspacePage: CustomReportWorkspacePage,
  chartDefinitionViewPage: ChartDefinitionViewPage,
  nameChart?: string,
) {
  if (nameChart) {
    customReportWorkspacePage.tree.findRowId('NAME', nameChart).then((idRequirement) => {
      customReportWorkspacePage.tree.selectNode(idRequirement);
    });
  }
  chartDefinitionViewPage.assertToolbarButtonExist('modify');
  chartDefinitionViewPage.assertToolbarButtonExist('download');
  chartDefinitionViewPage.triggerToolbarButtonTooltip('modify');
  chartDefinitionViewPage.assertColorToolbarButton('modify', 'rgb(162, 16, 77)');
  cy.get('.ant-tooltip-inner').should('contain.text', 'Modifier');
}

function assertChartPieCreationConformity(chartDefinitionViewPage: ChartDefinitionViewPage) {
  const modifyChartViewPage = chartDefinitionViewPage.clickModifyButton();
  modifyChartViewPage.assertExists();
  modifyChartViewPage.assertTitleUnderlineContent();
  modifyChartViewPage.assertCriterionChecked(0);
  modifyChartViewPage.chartTypeSelector.checkSelectedOption('Répartition');
  modifyChartViewPage.axisSelector.assertAttributeContain('Exigences | Criticité');
  modifyChartViewPage.axisSelector.checkOption('Agrégation');
  modifyChartViewPage.assertFilterAttribute(0, 'Criticité');
  modifyChartViewPage.assertFilterAttribute(4, 'Critique');
  modifyChartViewPage.assertChartTypeExist('pie');
  modifyChartViewPage.assertChartTitle('Chart1');
  modifyChartViewPage.assertChartLegends(0, 'Critique');
  modifyChartViewPage.assertButtonExistById('modify-chart');
  modifyChartViewPage.assertButtonExistById('cancel');
}

function modifyChartPieAndVerifyChangesInDashboardViewPage(
  customReportWorkspacePage: CustomReportWorkspacePage,
  chartDefinitionViewPage: ChartDefinitionViewPage,
  dashboardViewPage: DashboardViewPage,
  modifyChartViewPage: ModifyChartViewPage,
  groupedMultiListElement: GroupedMultiListElement,
) {
  modifyChartViewPage.renameChart('Nombre de cas de tests associés à des exigences');
  modifyChartViewPage.axisSelector.openAttributeSelector();
  modifyChartViewPage.axisSelector.addAttributeToChart(30, "Nombre d'exigences associées");
  modifyChartViewPage.clickIcon('close-circle');
  modifyChartViewPage.clickButtonById('cancel');
  customReportWorkspacePage.tree.findRowId('NAME', 'Chart1').then((idRequirement) => {
    customReportWorkspacePage.tree.assertNodeExist(idRequirement);
  });
  chartDefinitionViewPage.clickModifyButton();
  modifyChartViewPage.renameChart('Nombre de cas de tests associés à des exigences');
  modifyChartViewPage.axisSelector.openAttributeSelector();
  modifyChartViewPage.axisSelector.addAttributeToChart(30, "Nombre d'exigences associées");
  modifyChartViewPage.clickFilterField();
  groupedMultiListElement.toggleOneItem('Majeure');
  groupedMultiListElement.toggleOneItem('Mineure');
  groupedMultiListElement.close();
  modifyChartViewPage.clickButtonById('modify-chart');
  chartDefinitionViewPage.assertChartTitle('Nombre de cas de tests associés à des exigences');
  customReportWorkspacePage.tree.findRowId('NAME', 'TdB1').then((idRequirement) => {
    customReportWorkspacePage.tree.selectNode(idRequirement);
  });
  dashboardViewPage.assertExists();
  dashboardViewPage.assertCustomReportChartTitle(
    'pie',
    'Nombre de cas de tests associés à des exigences',
  );
}

function assertChartBarCreationConformity(chartDefinitionViewPage: ChartDefinitionViewPage) {
  const modifyChartViewPage = chartDefinitionViewPage.clickModifyButton();
  modifyChartViewPage.assertExists();
  modifyChartViewPage.assertTitleUnderlineContent();
  modifyChartViewPage.assertCriterionChecked(0);
  modifyChartViewPage.chartTypeSelector.checkSelectedOption('Histogramme');
  modifyChartViewPage.measureSelector.assertAttributeContain('Exigences | Criticité');
  modifyChartViewPage.measureSelector.checkOption('Comptage');
  modifyChartViewPage.axisSelector.assertAttributeContain('Exigences | Criticité');
  modifyChartViewPage.axisSelector.checkOption('Agrégation');
  modifyChartViewPage.assertChartTypeExist('bar');
  modifyChartViewPage.assertChartTitle('Chart2');
  modifyChartViewPage.assertChartXAxisTitle('Criticité');
  modifyChartViewPage.assertChartYAxisTitle('Criticité');
  modifyChartViewPage.assertChartXAxisLabel(0, 'Critique');
  modifyChartViewPage.assertChartXAxisLabel(1, 'Majeure');
  modifyChartViewPage.assertChartXAxisLabel(2, 'Mineure');
  modifyChartViewPage.assertChartXAxisLabel(3, 'Non définie');
  modifyChartViewPage.assertButtonExistById('modify-chart');
  modifyChartViewPage.assertButtonExistById('cancel');
}

function modifyChartBarAndVerifyChangesInDashboardViewPage(
  customReportWorkspacePage: CustomReportWorkspacePage,
  chartDefinitionViewPage: ChartDefinitionViewPage,
  dashboardViewPage: DashboardViewPage,
  modifyChartViewPage: ModifyChartViewPage,
  groupedMultiListElement: GroupedMultiListElement,
) {
  modifyChartViewPage.renameChart('Nombre des exigences par statut');
  modifyChartViewPage.axisSelector.openAttributeSelector();
  modifyChartViewPage.axisSelector.addAttributeToChart(5);
  modifyChartViewPage.clickButtonById('add-filter-button');
  modifyChartViewPage.filterSelector.addAttributeToChart(4);
  groupedMultiListElement.toggleOneItem('Majeure');
  groupedMultiListElement.toggleOneItem('Critique');
  groupedMultiListElement.toggleOneItem('Mineure');
  groupedMultiListElement.close();
  modifyChartViewPage.clickButtonById('cancel');
  customReportWorkspacePage.tree.findRowId('NAME', 'Chart2').then((idRequirement) => {
    customReportWorkspacePage.tree.assertNodeExist(idRequirement);
  });
  chartDefinitionViewPage.clickModifyButton();
  modifyChartViewPage.renameChart('Nombre des exigences par statut');
  modifyChartViewPage.axisSelector.openAttributeSelector();
  modifyChartViewPage.axisSelector.addAttributeToChart(5);
  modifyChartViewPage.clickButtonById('add-filter-button');
  modifyChartViewPage.filterSelector.addAttributeToChart(4);
  groupedMultiListElement.toggleOneItem('Majeure');
  groupedMultiListElement.toggleOneItem('Critique');
  groupedMultiListElement.toggleOneItem('Mineure');
  groupedMultiListElement.close();
  modifyChartViewPage.clickButtonById('modify-chart');
  chartDefinitionViewPage.assertChartTitle('Nombre des exigences par statut');
  customReportWorkspacePage.tree.findRowId('NAME', 'TdB1').then((idRequirement) => {
    customReportWorkspacePage.tree.selectNode(idRequirement);
  });
  dashboardViewPage.assertExists();
  dashboardViewPage.assertCustomReportChartTitle('bar', 'Nombre des exigences par statut');
}

function assertChartCumulativeCreationConformity(chartDefinitionViewPage: ChartDefinitionViewPage) {
  const modifyChartViewPage = chartDefinitionViewPage.clickModifyButton();
  modifyChartViewPage.assertExists();
  modifyChartViewPage.assertTitleUnderlineContent();
  modifyChartViewPage.assertCriterionChecked(0);
  modifyChartViewPage.chartTypeSelector.checkSelectedOption('Évolution');
  modifyChartViewPage.measureSelector.assertAttributeContain('Cas de test | Statut');
  modifyChartViewPage.measureSelector.checkOption('Comptage');
  modifyChartViewPage.axisSelector.assertAttributeContain('Cas de test | Statut');
  modifyChartViewPage.axisSelector.checkOption('Agrégation');
  modifyChartViewPage.assertFilterAttribute(0, 'Importance', 0);
  modifyChartViewPage.assertFilterAttribute(4, 'Très haute', 0);
  modifyChartViewPage.assertFilterAttribute(0, 'Statut', 1);
  modifyChartViewPage.assertFilterAttribute(4, 'À mettre à jour', 1);
  modifyChartViewPage.assertChartTypeExist('cumulative');
  modifyChartViewPage.assertChartTitle('Chart3');
  modifyChartViewPage.assertChartXAxisTitle('Statut');
  modifyChartViewPage.assertChartYAxisTitle('Statut');
  modifyChartViewPage.assertChartXAxisLabel(0, 'À mettre à jour');
  modifyChartViewPage.assertButtonExistById('modify-chart');
  modifyChartViewPage.assertButtonExistById('cancel');
}

function modifyChartCumulativeAndVerifyChanges(
  customReportWorkspacePage: CustomReportWorkspacePage,
  chartDefinitionViewPage: ChartDefinitionViewPage,
  modifyChartViewPage: ModifyChartViewPage,
) {
  modifyChartViewPage.renameChart('Évolution des cas de test par statut');
  modifyChartViewPage.clickIcon('close-circle');
  modifyChartViewPage.clickIcon('close-circle');
  modifyChartViewPage.clickButtonById('cancel');
  customReportWorkspacePage.tree.findRowId('NAME', 'Chart3').then((idRequirement) => {
    customReportWorkspacePage.tree.assertNodeExist(idRequirement);
  });
  chartDefinitionViewPage.clickModifyButton();
  modifyChartViewPage.renameChart('Évolution des cas des tests par statut');
  modifyChartViewPage.clickIcon('close-circle');
  modifyChartViewPage.clickIcon('close-circle');
  modifyChartViewPage.clickButtonById('modify-chart');
  chartDefinitionViewPage.assertChartTitle('Évolution des cas des tests par statut');
}

function assertChartTrendCreationConformity(chartDefinitionViewPage: ChartDefinitionViewPage) {
  const modifyChartViewPage = chartDefinitionViewPage.clickModifyButton();
  modifyChartViewPage.assertExists();
  modifyChartViewPage.assertTitleUnderlineContent();
  modifyChartViewPage.assertCriterionChecked(1);
  modifyChartViewPage.chartTypeSelector.checkSelectedOption('Tendances');
  modifyChartViewPage.measureSelector.assertAttributeContain('Cas de test | Importance');
  modifyChartViewPage.measureSelector.checkOption('Comptage');
  modifyChartViewPage.axisSelector.assertAttributeContain('Cas de test | Importance');
  modifyChartViewPage.axisSelector.checkOption('Agrégation');
  modifyChartViewPage.seriesSelector.assertAttributeContain('Cas de test | Importance');
  modifyChartViewPage.seriesSelector.checkOption('Agrégation');
  modifyChartViewPage.assertChartTypeExist('Trend');
  modifyChartViewPage.assertChartTitle('Chart4');
  modifyChartViewPage.assertChartXAxisTitle('Importance');
  modifyChartViewPage.assertChartYAxisTitle('Importance');
  modifyChartViewPage.assertChartXAxisLabel(0, 'Très haute');
  modifyChartViewPage.assertChartXAxisLabel(1, 'Haute');
  modifyChartViewPage.assertChartXAxisLabel(2, 'Moyenne');
  modifyChartViewPage.assertButtonExistById('modify-chart');
  modifyChartViewPage.assertButtonExistById('cancel');
}

function modifyChartTrendAndVerifyChanges(
  customReportWorkspacePage: CustomReportWorkspacePage,
  chartDefinitionViewPage: ChartDefinitionViewPage,
  modifyChartViewPage: ModifyChartViewPage,
  groupedMultiListElement: GroupedMultiListElement,
) {
  modifyChartViewPage.renameChart('Tendance des cas de test par statut approuvé');
  modifyChartViewPage.measureSelector.openAttributeSelector();
  modifyChartViewPage.measureSelector.addAttributeToChart(24, 'Statut');
  modifyChartViewPage.seriesSelector.openAttributeSelector();
  modifyChartViewPage.seriesSelector.addAttributeToChart(24, 'Statut');
  modifyChartViewPage.clickButtonById('add-filter-button');
  modifyChartViewPage.filterSelector.addAttributeToChart(24, 'Statut');
  groupedMultiListElement.toggleOneItem('Approuvé');
  groupedMultiListElement.close();
  modifyChartViewPage.clickButtonById('cancel');
  customReportWorkspacePage.tree.findRowId('NAME', 'Chart4').then((idRequirement) => {
    customReportWorkspacePage.tree.assertNodeExist(idRequirement);
  });
  chartDefinitionViewPage.clickModifyButton();
  modifyChartViewPage.renameChart('Tendance des cas de test par statut approuvé');
  modifyChartViewPage.measureSelector.openAttributeSelector();
  modifyChartViewPage.measureSelector.addAttributeToChart(24, 'Statut');
  modifyChartViewPage.seriesSelector.openAttributeSelector();
  modifyChartViewPage.seriesSelector.addAttributeToChart(24, 'Statut');
  modifyChartViewPage.clickButtonById('add-filter-button');
  modifyChartViewPage.filterSelector.addAttributeToChart(24, 'Statut');
  groupedMultiListElement.toggleOneItem('Approuvé');
  groupedMultiListElement.close();
  modifyChartViewPage.clickButtonById('modify-chart');
  chartDefinitionViewPage.assertChartTitle('Tendance des cas de test par statut approuvé');
}

function assertChartComparativeCreationConformity(
  chartDefinitionViewPage: ChartDefinitionViewPage,
) {
  const modifyChartViewPage = chartDefinitionViewPage.clickModifyButton();
  modifyChartViewPage.assertExists();
  modifyChartViewPage.assertTitleUnderlineContent();
  modifyChartViewPage.assertCriterionChecked(1);
  modifyChartViewPage.chartTypeSelector.checkSelectedOption('Comparaison');
  modifyChartViewPage.measureSelector.assertAttributeContain('Exigences | Criticité');
  modifyChartViewPage.measureSelector.checkOption('Comptage');
  modifyChartViewPage.axisSelector.assertAttributeContain('Exigences | Criticité');
  modifyChartViewPage.axisSelector.checkOption('Agrégation');
  modifyChartViewPage.seriesSelector.assertAttributeContain('Exigences | Criticité');
  modifyChartViewPage.seriesSelector.checkOption('Agrégation');
  modifyChartViewPage.assertChartTypeExist('comparative');
  modifyChartViewPage.assertChartXAxisTitle('Criticité');
  modifyChartViewPage.assertChartYAxisTitle('Criticité');
  modifyChartViewPage.assertChartYAxisLabel(0, 'Critique');
  modifyChartViewPage.assertChartYAxisLabel(1, 'Majeure');
  modifyChartViewPage.assertChartYAxisLabel(2, 'Mineure');
  modifyChartViewPage.assertChartYAxisLabel(3, 'Non définie');
  modifyChartViewPage.assertChartTitle('Chart5');
  modifyChartViewPage.assertButtonExistById('modify-chart');
  modifyChartViewPage.assertButtonExistById('cancel');
}

function modifyChartComparativeAndVerifyChanges(
  customReportWorkspacePage: CustomReportWorkspacePage,
  chartDefinitionViewPage: ChartDefinitionViewPage,
  modifyChartViewPage: ModifyChartViewPage,
  groupedMultiListElement: GroupedMultiListElement,
) {
  modifyChartViewPage.clickButtonById('add-filter-button');
  modifyChartViewPage.filterSelector.addAttributeToChart(5);
  groupedMultiListElement.toggleOneItem('Obsolète');
  groupedMultiListElement.close();
  modifyChartViewPage.clickButtonById('cancel');
  customReportWorkspacePage.tree.findRowId('NAME', 'Chart5').then((idRequirement) => {
    customReportWorkspacePage.tree.assertNodeExist(idRequirement);
  });
  chartDefinitionViewPage.clickModifyButton();
  modifyChartViewPage.clickButtonById('add-filter-button');
  modifyChartViewPage.filterSelector.addAttributeToChart(5);
  groupedMultiListElement.toggleOneItem('Obsolète');
  groupedMultiListElement.close();
  modifyChartViewPage.clickButtonById('modify-chart');
  chartDefinitionViewPage.assertChartTitle('Chart5');
}

function assertChartDetailsAndElements(chartDefinitionViewPage: ChartDefinitionViewPage) {
  chartDefinitionViewPage.assertChartTypeExist('comparative');
  chartDefinitionViewPage.assertChartYAxisTitle('Criticité');
  chartDefinitionViewPage.assertChartYAxisLabel(0, 'Non définie');
  chartDefinitionViewPage.assertChartXAxisTitle('Criticité');
  chartDefinitionViewPage.assertChartTitle('Chart5');
  chartDefinitionViewPage.assertChartLegends(0, 'Non définie');
  chartDefinitionViewPage.showInformationPanel();
  chartDefinitionViewPage.assertHasCreationDate();
  chartDefinitionViewPage.assertCreatedBy('admin');
  chartDefinitionViewPage.assertLastModification('jamais');
  chartDefinitionViewPage.checkPerimeter('Project1');
  assertAxesField(
    chartDefinitionViewPage,
    'measure',
    'sqtm-core-custom-report:right',
    'Mesurer',
    WorkspaceColor.REQUIREMENT,
  );
  chartDefinitionViewPage.checkMeasureInfo('Exigences | Criticité - Comptage');
  assertAxesField(
    chartDefinitionViewPage,
    'axis',
    'sqtm-core-custom-report:up',
    'Par',
    WorkspaceColor.REQUIREMENT,
  );
  chartDefinitionViewPage.checkAxisInfo('Exigences | Criticité - Agrégation');
  assertAxesField(
    chartDefinitionViewPage,
    'series',
    'unordered-list',
    'Séries',
    WorkspaceColor.REQUIREMENT,
  );
  chartDefinitionViewPage.checkSeriesInfo('Exigences | Criticité - Agrégation');
  chartDefinitionViewPage.checkFilterInfo(0, 'Exigences | Statut - Dans : Obsolète');
}
