import { DatabaseUtils } from '../../../utils/database.utils';
import { CampaignWorkspacePage } from '../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { CampaignViewPage } from '../../../page-objects/pages/campaign-workspace/campaign/campaign-view.page';
import { DashboardViewPage } from '../../../page-objects/pages/custom-report-workspace/dashboard-view.page';
import { CampaignFolderViewPage } from '../../../page-objects/pages/campaign-workspace/campaign-folder/campaign-folder.page';
import { CampaignMultiSelectionPage } from '../../../page-objects/pages/campaign-workspace/campaign/campaign-multi-selection.page';
import { IterationViewPage } from '../../../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import { PrerequisiteBuilder } from 'cypress/integration/utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  RequirementBuilder,
  RequirementFolderBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  ExploratoryTestCaseBuilder,
  KeywordTestCaseBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CampaignBuilder,
  CampaignFolderBuilder,
  CampaignTestPlanItemBuilder,
  ClassicExecutionBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
  KeywordExecutionBuilder,
  ScriptedExecutionBuilder,
  TestSuiteBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  ChartBuilder,
  CustomReportChartBinding,
  CustomReportReportBinding,
  DashboardBuilder,
  ReportBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/custom-report-prerequisite-builders';
import { mockReportDefinitionParameterTestCase } from '../../../data-mock/report-definition.data-mock';
import { addClearanceToUser } from '../../../utils/end-to-end/api/add-permissions';
import {
  selectNodeInCampaignWorkspace,
  selectNodeInNodeInCampaignWorkspace,
  selectNodeProjectInCampaignWorkspace,
} from '../../scenario-parts/campaign.part';
import { NavBarElement } from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {
  dragChartToDashboardEmpty,
  selectNodeInCustomReportWorkspace,
} from '../../scenario-parts/reporting.part';
import { MenuElement } from '../../../utils/menu.element';
import { selectProjectInTestCaseWorkspace } from '../../scenario-parts/testcase.part';
import { TestCaseLibraryViewPage } from '../../../page-objects/pages/test-case-workspace/test-case/test-case-library-view.page';
import { selectNodeProject } from '../../scenario-parts/actionword.part';
import { RequirementLibraryViewPage } from '../../../page-objects/pages/requirement-workspace/requirement-library/requirement-library-view.page';
import { HomeWorkspacePage } from '../../../page-objects/pages/home-workspace/home-workspace.page';
import {
  ChartOperation,
  ChartScopeType,
  ChartType,
} from '../../../../../projects/sqtm-core/src/lib/model/custom-report/chart-definition.model';
import { EntityType } from '../../../../../projects/sqtm-core/src/lib/model/entity.model';
import { PluginNameSpace } from '../../../../../projects/sqtm-core/src/lib/model/custom-report/report-definition.model';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Favorite button functionality in the campaigns workspace, when clicking on the [Favorite] button and selecting the "Campaigns" option.', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F05-UC035.CT03 - display-custom-dashboard-campaign', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();
    const campaignViewPage = new CampaignViewPage(1);
    const dashboardViewPage = new DashboardViewPage(1);
    const campaignFolderViewPage = new CampaignFolderViewPage(1);
    const campaignMultiSelectionPage = new CampaignMultiSelectionPage();
    const iterationViewPage = new IterationViewPage();

    assertFavoriteDashboardMessageInCampaignWorkspace(campaignWorkspace, campaignViewPage);

    cy.log('Step 2');
    selectCampaignDashboardAsFavorite(dashboardViewPage);

    cy.log('Step 3');
    assertFavoriteDashboardDisplayInCampaignWorkspace(campaignViewPage);

    cy.log('Step 4');
    assertFavoriteDashboardDisplayInFolderCampaignWorkspace(
      campaignWorkspace,
      campaignFolderViewPage,
    );

    cy.log('Step 5');
    assertFavoriteDashboardDisplayInMultiSelectionCampaignWorkspace(
      campaignWorkspace,
      campaignMultiSelectionPage,
    );

    cy.log('Step 6');
    assertFavoriteDashboardDisplayInIterationWorkspace(campaignWorkspace, iterationViewPage);

    cy.log('Step 7');
    assertDefaultAndFavoriteDashboardDisplay(campaignWorkspace, campaignViewPage);

    cy.log('Step 8');
    uncheckCampaignDashboardAndVerifyInWorkspaceDashboardMessage(
      dashboardViewPage,
      campaignViewPage,
      campaignFolderViewPage,
      campaignMultiSelectionPage,
      iterationViewPage,
    );

    cy.log('Step 9');
    emptyAndAddDashboardAndVerifyUpdatesCampaignWorkspace(dashboardViewPage, campaignViewPage);

    cy.log('Step 10');
    assertFavoriteDashboardInDifferentWorkspaces();

    cy.log('Step 11');
    assertFavoriteDashboardWithAnotherUser(campaignViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([
      new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley'),
      new UserBuilder('WayneR').withFirstName('Wayne').withLastName('Rooney'),
    ])
    .withProfiles([
      new ProfileBuilder('User A', [
        ProfileBuilderPermission.REQUIREMENT_READ,
        ProfileBuilderPermission.TEST_CASE_READ,
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CUSTOM_REPORT_READ,
        ProfileBuilderPermission.CUSTOM_REPORT_CREATE,
        ProfileBuilderPermission.CUSTOM_REPORT_WRITE,
        ProfileBuilderPermission.CUSTOM_REPORT_DELETE,
      ]),
      new ProfileBuilder('User B', [
        ProfileBuilderPermission.REQUIREMENT_READ,
        ProfileBuilderPermission.TEST_CASE_READ,
        ProfileBuilderPermission.CAMPAIGN_READ,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('ProjectA')
        .withRequirementLibraryNodes([
          new RequirementFolderBuilder('Folder').withRequirementLibraryNodes([
            new RequirementBuilder('Req1').withCriticality('CRITICAL'),
            new RequirementBuilder('Req2').withCriticality('MAJOR'),
            new RequirementBuilder('Req3').withCriticality('CRITICAL'),
            new RequirementBuilder('Req4').withCriticality('MINOR'),
          ]),
        ])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Classic'),
          new KeywordTestCaseBuilder('BDD'),
          new ScriptedTestCaseBuilder('Gherkin'),
          new ExploratoryTestCaseBuilder('Exploratory'),
        ])
        .withCampaignLibraryNodes([
          new CampaignFolderBuilder('Folder').withReference('F-01'),
          new CampaignBuilder('Campaign1')
            .withReference('CP-01')
            .withTestPlanItems([
              new CampaignTestPlanItemBuilder('Classic'),
              new CampaignTestPlanItemBuilder('BDD'),
              new CampaignTestPlanItemBuilder('Gherkin'),
              new CampaignTestPlanItemBuilder('Exploratory'),
            ])
            .withIterations([
              new IterationBuilder('Iteration')
                .withTestPlanItems([
                  new IterationTestPlanItemBuilder('Gherkin')
                    .withUserLogin('RonW')
                    .withExecutions([new ScriptedExecutionBuilder('FAILURE')]),
                  new IterationTestPlanItemBuilder('BDD')
                    .withUserLogin('WayneR')
                    .withExecutions([new KeywordExecutionBuilder('SUCCESS')]),
                  new IterationTestPlanItemBuilder('Classic')
                    .withUserLogin('WayneR')
                    .withExecutions([new ClassicExecutionBuilder('SUCCESS')]),
                  new IterationTestPlanItemBuilder('Exploratory'),
                ])
                .withTestSuites([
                  new TestSuiteBuilder('Suite').withTestCaseNames([
                    'Classic',
                    'BDD',
                    'Gherkin',
                    'Exploratory',
                  ]),
                ]),
            ]),
          new CampaignBuilder('Campaign2').withReference('CP-02'),
        ])
        .withCustomReportLibraryNode([
          new ChartBuilder(
            'ChartPie',
            'CAMPAIGN_REFERENCE',
            ChartType.PIE,
            ChartScopeType.DEFAULT,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ),
          new ChartBuilder(
            'ChartTrend',
            'CAMPAIGN_REFERENCE',
            ChartType.TREND,
            ChartScopeType.PROJECTS,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ).withAxisRank(1),
          new ChartBuilder(
            'ChartBar',
            'CAMPAIGN_ITERCOUNT',
            ChartType.BAR,
            ChartScopeType.DEFAULT,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ),
          new ChartBuilder(
            'ChartComparative',
            'CAMPAIGN_ID',
            ChartType.COMPARATIVE,
            ChartScopeType.PROJECTS,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ).withAxisRank(1),
          new ReportBuilder(
            'Report',
            PluginNameSpace.TEST_CASE_REPORT,
            JSON.stringify(mockReportDefinitionParameterTestCase()),
          ),
          new DashboardBuilder('Dashboard').withLinkElements([
            new CustomReportReportBinding('Report', {
              row: 1,
              col: 1,
              sizeX: 1,
              sizeY: 1,
            }),
            new CustomReportChartBinding('ChartComparative', {
              row: 1,
              col: 2,
              sizeX: 1,
              sizeY: 1,
            }),
            new CustomReportChartBinding('ChartBar', {
              row: 1,
              col: 3,
              sizeX: 1,
              sizeY: 1,
            }),
            new CustomReportChartBinding('ChartPie', {
              row: 1,
              col: 4,
              sizeX: 1,
              sizeY: 1,
            }),
          ]),
        ]),
      new ProjectBuilder('ProjectB').withRequirementLibraryNodes([
        new RequirementBuilder('Req1').withCriticality('MINOR'),
        new RequirementBuilder('Req2').withCriticality('UNDEFINED'),
      ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, 11, [1, 2]);
  addClearanceToUser(3, 12, [1, 2]);
}

function assertFavoriteDashboardMessageInCampaignWorkspace(
  campaignWorkspace: CampaignWorkspacePage,
  campaignViewPage: CampaignViewPage,
) {
  selectNodeInNodeInCampaignWorkspace(campaignWorkspace, ['ProjectA', 'Campaign']);
  campaignViewPage.assertExists();
  const dashboardCampaignPanel = campaignViewPage.clickDashboardAnchorLink();
  dashboardCampaignPanel.assertExists();
  dashboardCampaignPanel.assertFavoriteButtonExist();
  dashboardCampaignPanel.clickFavoriteButton();
  dashboardCampaignPanel.assertNoDashboardSelectedMessage();
}

function selectCampaignDashboardAsFavorite(dashboardViewPage: DashboardViewPage) {
  const customReportWorkspace = NavBarElement.navigateToCustomReportWorkspace();
  selectNodeInCustomReportWorkspace(customReportWorkspace, 'ProjectA', 'Dashboard');
  const favoriteToolbar = dashboardViewPage.showFavoriteList();
  favoriteToolbar.click();
  const favoriteMenu = new MenuElement('favoriteMenu');
  favoriteMenu.item('campaigns').click();
  cy.clickVoid();
}

function assertFavoriteDashboardDisplayInCampaignWorkspace(campaignViewPage: CampaignViewPage) {
  const campaignWorkspace = NavBarElement.navigateToCampaignWorkspace();
  selectNodeInCampaignWorkspace(campaignWorkspace, 'ProjectA', 'Campaign1');
  const dashboardCampaignPanel = campaignViewPage.clickDashboardAnchorLink();
  campaignViewPage.clickDashboardAnchorLink();
  dashboardCampaignPanel.assertDefaultButtonExist();
  dashboardCampaignPanel.assertTitleExist('Dashboard');
  dashboardCampaignPanel.assertCustomDashboardBinding([1, 2, 3]);
  dashboardCampaignPanel.assertCannotResizableElement();
  dashboardCampaignPanel.assertCannotMoveElement(1);
  dashboardCampaignPanel.assertCannotMoveElement(2);
  dashboardCampaignPanel.assertChartLegends(1, 'CP-01');
}

function assertFavoriteDashboardDisplayInFolderCampaignWorkspace(
  campaignWorkspace: CampaignWorkspacePage,
  campaignFolderViewPage: CampaignFolderViewPage,
) {
  selectNodeInCampaignWorkspace(campaignWorkspace, 'ProjectA', 'Folder');
  campaignFolderViewPage.assertExists();
  campaignFolderViewPage.clickDashboardAnchorLink();
  campaignFolderViewPage.assertFavoriteButtonExist();
  const dashboardFolderPanel = campaignFolderViewPage.clickFavoriteButton();
  campaignFolderViewPage.assertTitleDashboardExist('Dashboard');
  dashboardFolderPanel.assertCustomDashboardBinding([1, 2, 3]);
  dashboardFolderPanel.assertCannotResizableElement();
  dashboardFolderPanel.assertChartLegendsNotExist();
}

function assertFavoriteDashboardDisplayInMultiSelectionCampaignWorkspace(
  campaignWorkspace: CampaignWorkspacePage,
  campaignMultiSelectionPage: CampaignMultiSelectionPage,
) {
  campaignWorkspace.tree.selectNodesByName(['CP-01', 'CP-02', 'Folder']);
  campaignMultiSelectionPage.assertExists();
  campaignMultiSelectionPage.clickDashboardAnchorLink();
  const dashboardPage = campaignMultiSelectionPage.clickFavoriteButton();
  campaignMultiSelectionPage.assertTitleDashboardExist('Dashboard');
  dashboardPage.assertExists();
  dashboardPage.assertCustomDashboardBinding([1, 2, 3]);
  dashboardPage.assertCannotResizableElement();
  dashboardPage.assertCannotMoveElement(1);
  dashboardPage.assertCannotMoveElement(2);
  dashboardPage.assertChartLegends(2, 'CP-01');
  dashboardPage.assertChartLegends(3, 'CP-02');
}

function assertFavoriteDashboardDisplayInIterationWorkspace(
  campaignWorkspace: CampaignWorkspacePage,
  iterationViewPage: IterationViewPage,
) {
  selectNodeInNodeInCampaignWorkspace(campaignWorkspace, ['ProjectA', 'Campaign1', 'Iteration']);
  iterationViewPage.assertExists();
  const iterationViewDashboardPage = iterationViewPage.clickDashboardAnchorLink();
  iterationViewDashboardPage.assertExists();
  iterationViewDashboardPage.assertDefaultButtonExist();
  iterationViewDashboardPage.assertTitleExist('Dashboard');
  iterationViewDashboardPage.assertCustomDashboardBinding([1, 2, 3]);
  iterationViewDashboardPage.assertCannotResizableElement();
  iterationViewDashboardPage.assertCannotMoveElement(1);
  iterationViewDashboardPage.assertCannotMoveElement(2);
  iterationViewDashboardPage.assertChartLegends(1, 'CP-01');
}

function assertDefaultAndFavoriteDashboardDisplay(
  campaignWorkspace: CampaignWorkspacePage,
  campaignViewPage: CampaignViewPage,
) {
  selectNodeProjectInCampaignWorkspace(campaignWorkspace, 'CP-01');
  const dashboardCampaignPanel = campaignViewPage.clickDashboardAnchorLink();
  dashboardCampaignPanel.assertDefaultButtonExist();
  dashboardCampaignPanel.clickDefaultButton();
  dashboardCampaignPanel.assertStatsChartAreRendered();
  dashboardCampaignPanel.clickFavoriteButton();
  dashboardCampaignPanel.assertCustomDashboardBinding([1, 2, 3]);
}

function uncheckCampaignDashboardAndVerifyInWorkspaceDashboardMessage(
  dashboardViewPage: DashboardViewPage,
  campaignViewPage: CampaignViewPage,
  campaignFolderViewPage: CampaignFolderViewPage,
  campaignMultiSelectionPage: CampaignMultiSelectionPage,
  iterationViewPage: IterationViewPage,
) {
  const customReportWorkspace = NavBarElement.navigateToCustomReportWorkspace();
  selectNodeInCustomReportWorkspace(customReportWorkspace, 'ProjectA', 'Dashboard');
  const favoriteToolbar = dashboardViewPage.showFavoriteList();
  favoriteToolbar.click();
  const favoriteMenu = new MenuElement('favoriteMenu');
  favoriteMenu.item('campaigns').click();
  cy.clickVoid();
  const campaignWorkspace = NavBarElement.navigateToCampaignWorkspace();
  selectNodeProjectInCampaignWorkspace(campaignWorkspace, 'Campaign1');
  campaignViewPage.assertExists();
  const dashboardCampaignPanel = campaignViewPage.clickDashboardAnchorLink();
  dashboardCampaignPanel.assertDefaultButtonExist();
  dashboardCampaignPanel.assertNoDashboardSelectedMessage();
  selectNodeProjectInCampaignWorkspace(campaignWorkspace, 'Folder');
  campaignFolderViewPage.assertExists();
  campaignFolderViewPage.clickDashboardAnchorLink();
  campaignFolderViewPage.assertFavoriteButtonExist();
  campaignFolderViewPage.clickFavoriteButton();
  campaignFolderViewPage.assertNoDashboardSelectedMessage();
  campaignWorkspace.tree.selectNodesByName(['CP-01', 'CP-02', 'Folder']);
  campaignMultiSelectionPage.assertExists();
  campaignMultiSelectionPage.clickDashboardAnchorLink();
  campaignMultiSelectionPage.clickFavoriteButton();
  campaignMultiSelectionPage.assertNoDashboardSelectedMessage();
  selectNodeProjectInCampaignWorkspace(campaignWorkspace, 'Iteration');
  iterationViewPage.assertExists();
  const iterationViewDashboardPage = iterationViewPage.clickDashboardAnchorLink();
  iterationViewDashboardPage.assertNoDashboardSelectedMessage();
  NavBarElement.navigateToCustomReportWorkspace();
  selectNodeInCustomReportWorkspace(customReportWorkspace, 'ProjectA', 'Dashboard');
  favoriteToolbar.click();
  favoriteMenu.item('campaigns').click();
  cy.clickVoid();
}

function emptyAndAddDashboardAndVerifyUpdatesCampaignWorkspace(
  dashboardViewPage: DashboardViewPage,
  campaignViewPage: CampaignViewPage,
) {
  dashboardViewPage.assertExists();
  dashboardViewPage.removeReportIfDashboardHasMultipleElements(1);
  dashboardViewPage.clickToRemoveChart(1);
  dashboardViewPage.clickToRemoveChart(2);
  dashboardViewPage.clickToRemoveChart(3);
  dashboardViewPage.assertEmptyDropzoneIsVisible();
  const campaignWorkspace = NavBarElement.navigateToCampaignWorkspace();
  selectNodeProjectInCampaignWorkspace(campaignWorkspace, 'CP-01');
  campaignViewPage.assertExists();
  const dashboardPanel = campaignViewPage.clickDashboardAnchorLink();
  dashboardPanel.assertFavoriteDashboardIsEmptyMessage();
  const customReportWorkspace = NavBarElement.navigateToCustomReportWorkspace();
  selectNodeInCustomReportWorkspace(customReportWorkspace, 'ProjectA', 'Dashboard');
  dragChartToDashboardEmpty(customReportWorkspace, dashboardViewPage, 'ChartTrend');
  NavBarElement.navigateToCampaignWorkspace();
  selectNodeProjectInCampaignWorkspace(campaignWorkspace, 'CP-01');
  dashboardPanel.assertCustomDashboardBinding([4]);
}

function assertFavoriteDashboardInDifferentWorkspaces() {
  const testCaseWorkspacePage = NavBarElement.navigateToTestCaseWorkspace();
  selectProjectInTestCaseWorkspace(testCaseWorkspacePage, 'ProjectA');
  const testCaseLibraryViewPage = new TestCaseLibraryViewPage('*');
  testCaseLibraryViewPage.assertExists();
  const testCaseDashboardPanel = testCaseLibraryViewPage.showDashboard();
  testCaseDashboardPanel.clickFavoriteButton();
  testCaseDashboardPanel.assertCustomDashboardNotExist();

  const requirementWorkspacePage = NavBarElement.navigateToRequirementWorkspace();
  selectNodeProject(requirementWorkspacePage, 'ProjectA');
  const requirementLibraryViewPage = new RequirementLibraryViewPage(1);
  requirementLibraryViewPage.assertExists();
  const dashboardRequirementPanel = requirementLibraryViewPage.showDashboard();
  dashboardRequirementPanel.clickFavoriteButton();
  dashboardRequirementPanel.assertNoDashboardSelectedMessage();

  const homeWorkspacePage = HomeWorkspacePage.initTestAtPageWithModel();
  homeWorkspacePage.showDashboard();
  homeWorkspacePage.assertNoDashboardSelectedMessage();
}

function assertFavoriteDashboardWithAnotherUser(campaignViewPage: CampaignViewPage) {
  NavBarElement.logout();
  cy.logInAs('WayneR', 'admin');
  const campaignWorkspace = NavBarElement.navigateToCampaignWorkspace();
  selectNodeInCampaignWorkspace(campaignWorkspace, 'ProjectA', 'CP-01');
  campaignViewPage.assertExists();
  const dashboardCampaignPanel = campaignViewPage.clickDashboardAnchorLink();
  dashboardCampaignPanel.clickFavoriteButton();
  dashboardCampaignPanel.assertNoDashboardSelectedMessage();
}
