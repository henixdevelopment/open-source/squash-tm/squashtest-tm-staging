import { DatabaseUtils } from '../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  TestCaseBuilder,
  TestCaseFolderBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  ChartBuilder,
  CustomReportChartBinding,
  CustomReportReportBinding,
  DashboardBuilder,
  ReportBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/custom-report-prerequisite-builders';
import {
  ChartOperation,
  ChartScopeType,
  ChartType,
} from '../../../../../projects/sqtm-core/src/lib/model/custom-report/chart-definition.model';
import { EntityType } from '../../../../../projects/sqtm-core/src/lib/model/entity.model';
import { PluginNameSpace } from '../../../../../projects/sqtm-core/src/lib/model/custom-report/report-definition.model';
import { mockReportDefinitionParameterTestCase } from '../../../data-mock/report-definition.data-mock';
import { addClearanceToUser, SystemProfile } from '../../../utils/end-to-end/api/add-permissions';
import { TestCaseWorkspacePage } from '../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {
  selectNodeInTestCaseWorkspace,
  selectProjectInTestCaseWorkspace,
} from '../../scenario-parts/testcase.part';
import { TestCaseLibraryViewPage } from '../../../page-objects/pages/test-case-workspace/test-case/test-case-library-view.page';
import { NavBarElement } from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {
  dragChartToDashboardEmpty,
  selectNodeInCustomReportWorkspace,
} from '../../scenario-parts/reporting.part';
import { MenuElement } from '../../../utils/menu.element';
import { DashboardViewPage } from '../../../page-objects/pages/custom-report-workspace/dashboard-view.page';
import { TestCaseFolderViewPage } from '../../../page-objects/pages/test-case-workspace/test-case-folder/test-case-folder-view.page';
import { TestCaseMultiSelectionPage } from '../../../page-objects/pages/test-case-workspace/test-case/test-case-multi-selection.page';
import { selectNodeProject } from '../../scenario-parts/requirement.part';
import { selectNodeProjectInCampaignWorkspace } from '../../scenario-parts/campaign.part';
import { CampaignLibraryViewPage } from '../../../page-objects/pages/campaign-workspace/campaign-library/campaign-library.page';
import { HomeWorkspacePage } from '../../../page-objects/pages/home-workspace/home-workspace.page';
import { RequirementLibraryViewPage } from '../../../page-objects/pages/requirement-workspace/requirement-library/requirement-library-view.page';

describe('Favorite button functionality in the campaigns workspace, when clicking on the [Favorite] button and selecting the "Campaigns" option.', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F05-UC035.CT03 - display-custom-dashboard-test-case', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testCaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const dashboardViewPage = new DashboardViewPage(1);
    const testCaseFolderViewPage = new TestCaseFolderViewPage(1);
    const testCaseLibraryViewPage = new TestCaseLibraryViewPage('*');
    const multiTestCaseSelection = new TestCaseMultiSelectionPage();

    assertFavoriteDashboardMessageInTestCaseWorkspace(testCaseWorkspace, testCaseLibraryViewPage);

    cy.log('Step 2');
    selectTestCaseDashboardAsFavorite(dashboardViewPage);

    cy.log('Step 3');
    assertFavoriteDashboardDisplayInTestCaseWorkspace(testCaseLibraryViewPage);

    cy.log('Step 4');
    assertFavoriteDashboardDisplayInFolderTestCaseWorkspace(
      testCaseWorkspace,
      testCaseFolderViewPage,
    );

    cy.log('Step 5');
    assertFavoriteDashboardDisplayInMultiProjectsTestCaseWorkspace(
      testCaseWorkspace,
      testCaseLibraryViewPage,
    );

    cy.log('Step 6');
    assertFavoriteDashboardDisplayInMultiSelectionTestCase(
      testCaseWorkspace,
      multiTestCaseSelection,
    );

    cy.log('Step 7');
    assertDefaultAndFavoriteDashboardDisplay(testCaseWorkspace, testCaseLibraryViewPage);

    cy.log('Step 8');
    uncheckTestCaseDashboardAndVerifyInWorkspaceDashboardMessage(
      dashboardViewPage,
      testCaseLibraryViewPage,
      testCaseFolderViewPage,
      multiTestCaseSelection,
    );

    cy.log('Step 9');
    emptyAndAddDashboardAndVerifyUpdatesTestCaseWorkspace(
      testCaseLibraryViewPage,
      dashboardViewPage,
    );

    cy.log('Step 10');
    assertFavoriteDashboardInDifferentWorkspace();

    cy.log('Step 11');
    assertFavoriteDashboardWithAnotherUser(testCaseLibraryViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([
      new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley'),
      new UserBuilder('WayneR').withFirstName('Wayne').withLastName('Rooney'),
    ])
    .withProjects([
      new ProjectBuilder('ProjectA')
        .withTestCaseLibraryNodes([
          new TestCaseFolderBuilder('Folder').withTestCaseLibraryNodes([
            new TestCaseBuilder('Test1')
              .withStatus('WORK_IN_PROGRESS')
              .withImportance('LOW')
              .withReference('TC_01'),
            new TestCaseBuilder('Test2')
              .withStatus('UNDER_REVIEW')
              .withImportance('MEDIUM')
              .withReference('TC_02'),
            new TestCaseBuilder('Test3')
              .withStatus('APPROVED')
              .withImportance('HIGH')
              .withReference('TC_03'),
            new TestCaseBuilder('Test4')
              .withStatus('OBSOLETE')
              .withImportance('VERY_HIGH')
              .withReference('TC_04'),
            new TestCaseBuilder('Test5')
              .withStatus('TO_BE_UPDATED')
              .withImportance('VERY_HIGH')
              .withReference('TC_05'),
          ]),
        ])
        .withCustomReportLibraryNode([
          new ChartBuilder(
            'ChartPie',
            'TEST_CASE_STATUS',
            ChartType.PIE,
            ChartScopeType.DEFAULT,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ),
          new ChartBuilder(
            'ChartTrend',
            'TEST_CASE_REFERENCE',
            ChartType.TREND,
            ChartScopeType.PROJECTS,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ).withAxisRank(1),
          new ChartBuilder(
            'ChartBar',
            'TEST_CASE_IMPORTANCE',
            ChartType.BAR,
            ChartScopeType.DEFAULT,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ),
          new ChartBuilder(
            'ChartComparative',
            'TEST_CASE_ID',
            ChartType.COMPARATIVE,
            ChartScopeType.PROJECTS,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ).withAxisRank(1),
          new ReportBuilder(
            'Report',
            PluginNameSpace.TEST_CASE_REPORT,
            JSON.stringify(mockReportDefinitionParameterTestCase()),
          ),
          new DashboardBuilder('Dashboard').withLinkElements([
            new CustomReportReportBinding('Report', {
              row: 1,
              col: 1,
              sizeX: 1,
              sizeY: 1,
            }),
            new CustomReportChartBinding('ChartPie', {
              row: 1,
              col: 2,
              sizeX: 1,
              sizeY: 1,
            }),
            new CustomReportChartBinding('ChartComparative', {
              row: 1,
              col: 3,
              sizeX: 1,
              sizeY: 1,
            }),
            new CustomReportChartBinding('ChartBar', {
              row: 1,
              col: 4,
              sizeX: 1,
              sizeY: 1,
            }),
          ]),
        ]),
      new ProjectBuilder('ProjectB').withTestCaseLibraryNodes([
        new TestCaseBuilder('Test1')
          .withStatus('WORK_IN_PROGRESS')
          .withImportance('LOW')
          .withReference('TC_01'),
        new TestCaseBuilder('Test2')
          .withStatus('UNDER_REVIEW')
          .withImportance('MEDIUM')
          .withReference('TC_01'),
      ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_MANAGER, [1, 2]);
  addClearanceToUser(3, SystemProfile.VALIDATOR, [1, 2]);
}

function assertFavoriteDashboardMessageInTestCaseWorkspace(
  testCaseWorkspace: TestCaseWorkspacePage,
  testCaseLibraryViewPage: TestCaseLibraryViewPage,
) {
  selectProjectInTestCaseWorkspace(testCaseWorkspace, 'ProjectA');
  testCaseLibraryViewPage.assertExists();
  const dashboardTestCasePanel = testCaseLibraryViewPage.showDashboard();
  dashboardTestCasePanel.assertFavoriteButtonExist();
  dashboardTestCasePanel.clickFavoriteButton();
  dashboardTestCasePanel.assertNoDashboardSelectedMessage();
}

function selectTestCaseDashboardAsFavorite(dashboardViewPage: DashboardViewPage) {
  const customReportWorkspace = NavBarElement.navigateToCustomReportWorkspace();
  selectNodeInCustomReportWorkspace(customReportWorkspace, 'ProjectA', 'Dashboard');
  const favoriteToolbar = dashboardViewPage.showFavoriteList();
  favoriteToolbar.click();
  const favoriteMenu = new MenuElement('favoriteMenu');
  favoriteMenu.item('test-cases').click();
  cy.clickVoid();
}

function assertFavoriteDashboardDisplayInTestCaseWorkspace(
  testCaseLibraryViewPage: TestCaseLibraryViewPage,
) {
  const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
  selectProjectInTestCaseWorkspace(testCaseWorkspace, 'ProjectA');
  testCaseLibraryViewPage.assertExists();
  const dashboardTestCasePanel = testCaseLibraryViewPage.showDashboard();
  dashboardTestCasePanel.assertDefaultButtonExist();
  dashboardTestCasePanel.assertTitleExist('Dashboard');
  dashboardTestCasePanel.assertCustomDashboardExist();
  dashboardTestCasePanel.asserChartDashboardBinding([1, 2, 3]);
  dashboardTestCasePanel.asserReportDashboardBinding([1]);
  dashboardTestCasePanel.assertCannotResizableElement();
  dashboardTestCasePanel.assertCannotMoveElement(1);
  dashboardTestCasePanel.assertCannotMoveElement(2);
  ['En cours de rédac', 'À approuver', 'Approuvé', 'Obsolète', 'À mettre à jour'].forEach(
    (legend, index) => {
      dashboardTestCasePanel.assertChartLegends(index, legend);
    },
  );
}

function assertFavoriteDashboardDisplayInFolderTestCaseWorkspace(
  testCaseWorkspace: TestCaseWorkspacePage,
  testCaseFolderViewPage: TestCaseFolderViewPage,
) {
  selectNodeInTestCaseWorkspace(testCaseWorkspace, 'ProjectA', 'Folder');
  testCaseFolderViewPage.assertExists();
  const folderTestCaseDashboardPanel = testCaseFolderViewPage.showDashboard();
  folderTestCaseDashboardPanel.assertFavoriteButtonExist();
  folderTestCaseDashboardPanel.assertTitleExist('Dashboard');
  folderTestCaseDashboardPanel.assertCustomDashboardExist();
  folderTestCaseDashboardPanel.asserChartDashboardBinding([1, 2, 3]);
  folderTestCaseDashboardPanel.asserReportDashboardBinding([1]);
  folderTestCaseDashboardPanel.assertCannotResizableElement();
  folderTestCaseDashboardPanel.assertCannotMoveElement(1);
  folderTestCaseDashboardPanel.assertCannotMoveElement(2);
  ['En cours de rédac', 'À approuver', 'Approuvé', 'Obsolète', 'À mettre à jour'].forEach(
    (legend, index) => {
      folderTestCaseDashboardPanel.assertChartLegends(index, legend);
    },
  );
}

function assertFavoriteDashboardDisplayInMultiProjectsTestCaseWorkspace(
  testCaseWorkspace: TestCaseWorkspacePage,
  testCaseLibraryViewPage: TestCaseLibraryViewPage,
) {
  testCaseWorkspace.tree.findRowId('NAME', 'ProjectA').then((id) => {
    testCaseWorkspace.tree.findRowId('NAME', 'ProjectB').then((id1) => {
      testCaseWorkspace.tree.selectNodes([id, id1]);
    });
  });
  const dashboardMultiSelectionProject = testCaseLibraryViewPage.showDashboard();
  dashboardMultiSelectionProject.assertFavoriteButtonExist();
  dashboardMultiSelectionProject.assertRefreshDashboardMessage();
  dashboardMultiSelectionProject.clickRefresh();
  dashboardMultiSelectionProject.assertTitleExist('Dashboard');
  dashboardMultiSelectionProject.assertCustomDashboardExist();
  dashboardMultiSelectionProject.asserChartDashboardBinding([1, 2, 3]);
  dashboardMultiSelectionProject.asserReportDashboardBinding([1]);
  dashboardMultiSelectionProject.assertCannotResizableElement();
  dashboardMultiSelectionProject.assertCannotMoveElement(1);
  dashboardMultiSelectionProject.assertCannotMoveElement(2);
  ['En cours de rédac', 'À approuver', 'Approuvé', 'Obsolète', 'À mettre à jour'].forEach(
    (legend, index) => {
      dashboardMultiSelectionProject.assertChartLegends(index, legend);
    },
  );
}

function assertFavoriteDashboardDisplayInMultiSelectionTestCase(
  testCaseWorkspace: TestCaseWorkspacePage,
  multiTestCaseSelection: TestCaseMultiSelectionPage,
) {
  testCaseWorkspace.tree.findRowId('NAME', 'Folder').then((id) => {
    testCaseWorkspace.tree.openNodeIfClosed(id);
  });
  testCaseWorkspace.tree.findRowId('NAME', 'TC_02').then((id) => {
    testCaseWorkspace.tree.findRowId('NAME', 'TC_03').then((id1) => {
      testCaseWorkspace.tree.selectNodes([id, id1]);
    });
  });
  multiTestCaseSelection.assertExists();
  multiTestCaseSelection.clickDashboardAnchorLink();
  const multiTestCaseSelectionDashboard = multiTestCaseSelection.clickFavoriteButton();
  multiTestCaseSelection.assertTitleDashboardExist('Dashboard');
  multiTestCaseSelectionDashboard.assertCustomDashboardExist();
  multiTestCaseSelectionDashboard.asserChartDashboardBinding([1, 2, 3]);
  multiTestCaseSelectionDashboard.asserReportDashboardBinding([1]);
  multiTestCaseSelectionDashboard.assertCannotResizableElement();
  multiTestCaseSelectionDashboard.assertCannotMoveElement(1);
  multiTestCaseSelectionDashboard.assertCannotMoveElement(2);
  ['À approuver', 'Approuvé'].forEach((legend, index) => {
    multiTestCaseSelectionDashboard.assertChartLegends(index, legend);
  });
}

function assertDefaultAndFavoriteDashboardDisplay(
  testCaseWorkspace: TestCaseWorkspacePage,
  testCaseLibraryViewPage: TestCaseLibraryViewPage,
) {
  selectProjectInTestCaseWorkspace(testCaseWorkspace, 'ProjectA');
  const dashboardTestCasePanel = testCaseLibraryViewPage.showDashboard();
  dashboardTestCasePanel.assertDefaultButtonExist();
  dashboardTestCasePanel.clickDefaultButton();
  dashboardTestCasePanel.coverageChart.assertChartExist();
  dashboardTestCasePanel.statusChart.assertChartExist();
  dashboardTestCasePanel.importanceChart.assertChartExist();
  dashboardTestCasePanel.sizeChart.assertChartExist();
  dashboardTestCasePanel.clickFavoriteButton();
  dashboardTestCasePanel.asserChartDashboardBinding([1, 2, 3]);
  dashboardTestCasePanel.asserReportDashboardBinding([1]);
}

function uncheckTestCaseDashboardAndVerifyInWorkspaceDashboardMessage(
  dashboardViewPage: DashboardViewPage,
  testCaseLibraryViewPage: TestCaseLibraryViewPage,
  testCaseFolderViewPage: TestCaseFolderViewPage,
  multiTestCaseSelection: TestCaseMultiSelectionPage,
) {
  const customReportWorkspace = NavBarElement.navigateToCustomReportWorkspace();
  selectNodeInCustomReportWorkspace(customReportWorkspace, 'ProjectA', 'Dashboard');
  const favoriteToolbar = dashboardViewPage.showFavoriteList();
  favoriteToolbar.click();
  const favoriteMenu = new MenuElement('favoriteMenu');
  favoriteMenu.item('test-cases').click();
  cy.clickVoid();
  const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
  selectProjectInTestCaseWorkspace(testCaseWorkspace, 'ProjectA');
  testCaseLibraryViewPage.assertExists();
  const dashboardPanel = testCaseLibraryViewPage.showDashboard();
  dashboardPanel.assertFavoriteButtonNotExist();
  dashboardPanel.assertNoDashboardSelectedMessage();
  selectNodeInTestCaseWorkspace(testCaseWorkspace, 'ProjectA', 'Folder');
  testCaseFolderViewPage.assertExists();
  const folderDashboardPanel = testCaseFolderViewPage.showDashboard();
  folderDashboardPanel.assertNoDashboardSelectedMessage();
  testCaseWorkspace.tree.findRowId('NAME', 'TC_02').then((id) => {
    testCaseWorkspace.tree.findRowId('NAME', 'TC_03').then((id1) => {
      testCaseWorkspace.tree.selectNodes([id, id1]);
    });
  });
  multiTestCaseSelection.assertExists();
  multiTestCaseSelection.clickDashboardAnchorLink();
  multiTestCaseSelection.clickFavoriteButton();
  multiTestCaseSelection.assertNoDashboardSelectedMessage();
  NavBarElement.navigateToCustomReportWorkspace();
  selectNodeInCustomReportWorkspace(customReportWorkspace, 'ProjectA', 'Dashboard');
  favoriteToolbar.click();
  favoriteMenu.item('test-cases').click();
  cy.clickVoid();
}

function emptyAndAddDashboardAndVerifyUpdatesTestCaseWorkspace(
  testCaseLibraryViewPage: TestCaseLibraryViewPage,
  dashboardViewPage: DashboardViewPage,
) {
  NavBarElement.logout();
  cy.logInAs('admin', 'admin');
  const customReportWorkspace = NavBarElement.navigateToCustomReportWorkspace();
  selectNodeInCustomReportWorkspace(customReportWorkspace, 'ProjectA', 'Dashboard');
  dashboardViewPage.assertExists();
  dashboardViewPage.removeReportIfDashboardHasMultipleElements(1);
  dashboardViewPage.clickToRemoveChart(1);
  dashboardViewPage.clickToRemoveChart(2);
  dashboardViewPage.clickToRemoveChart(3);
  dashboardViewPage.assertEmptyDropzoneIsVisible();
  NavBarElement.logout();
  cy.logInAs('RonW', 'admin');
  NavBarElement.navigateToCustomReportWorkspace();
  selectNodeInCustomReportWorkspace(customReportWorkspace, 'ProjectA', 'Dashboard');
  dashboardViewPage.assertEmptyDropzoneIsVisible();
  const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
  selectProjectInTestCaseWorkspace(testCaseWorkspace, 'ProjectA');
  testCaseLibraryViewPage.assertExists();
  const dashboardPanel = testCaseLibraryViewPage.showDashboard();
  dashboardPanel.assertFavoriteDashboardIsEmptyMessage();
  NavBarElement.navigateToCustomReportWorkspace();
  dragChartToDashboardEmpty(customReportWorkspace, dashboardViewPage, 'ChartTrend');
  NavBarElement.navigateToTestCaseWorkspace();
  selectProjectInTestCaseWorkspace(testCaseWorkspace, 'ProjectA');
  dashboardPanel.assertCustomDashboardExist();
}

function assertFavoriteDashboardInDifferentWorkspace() {
  const requirementWorkspacePage = NavBarElement.navigateToRequirementWorkspace();
  selectNodeProject(requirementWorkspacePage, 'ProjectA');
  const requirementLibraryViewPage = new RequirementLibraryViewPage('*');
  requirementLibraryViewPage.assertExists();
  const requirementDashboardPanel = requirementLibraryViewPage.showDashboard();
  requirementDashboardPanel.clickFavoriteButton();
  requirementDashboardPanel.assertNoDashboardSelectedMessage();

  const campaignWorkspacePage = NavBarElement.navigateToCampaignWorkspace();
  selectNodeProjectInCampaignWorkspace(campaignWorkspacePage, 'ProjectA');
  const campaignLibraryViewPage = new CampaignLibraryViewPage('campaign-view-header');
  campaignLibraryViewPage.assertExists();
  const campaignDashboardPanel = campaignLibraryViewPage.showDashboard();
  campaignDashboardPanel.clickFavoriteButton();
  campaignDashboardPanel.assertCustomDashboardNotExist();

  const homeWorkspacePage = HomeWorkspacePage.initTestAtPageWithModel();
  homeWorkspacePage.showDashboard();
  homeWorkspacePage.assertNoDashboardSelectedMessage();
}

function assertFavoriteDashboardWithAnotherUser(testCaseLibraryViewPage: TestCaseLibraryViewPage) {
  NavBarElement.logout();
  cy.logInAs('WayneR', 'admin');
  const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
  selectProjectInTestCaseWorkspace(testCaseWorkspace, 'ProjectA');
  testCaseLibraryViewPage.assertExists();
  const dashboardPanel = testCaseLibraryViewPage.showDashboard();
  dashboardPanel.clickFavoriteButton();
  dashboardPanel.assertNoDashboardSelectedMessage();
}
