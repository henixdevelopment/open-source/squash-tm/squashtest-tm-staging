import { DatabaseUtils } from '../../../utils/database.utils';
import { CustomReportWorkspacePage } from '../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import { PrerequisiteBuilder } from '../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { TestCaseBuilder } from '../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CampaignBuilder,
  ClassicExecutionBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  accessAddChartPageFromCustomReportWorkspace,
  assertAxesField,
  configureAxisAttributeSelection,
  renameAndSaveChart,
} from '../../scenario-parts/reporting.part';
import { FilterEnumSingleSelectWidgetElement } from '../../../page-objects/elements/filters/filter-enum-single-select-widget.element';
import { deleteTestCase } from '../../../utils/end-to-end/api/testcases/delete-testcase';
import { ExecutionStatusColor, WorkspaceColor } from '../../../utils/color.utils';
import { CreateChartViewPage } from '../../../page-objects/pages/custom-report-workspace/chart/create-chart-view.page';
import { ChartDefinitionViewPage } from '../../../page-objects/pages/custom-report-workspace/chart/chart-definition-view.page';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../utils/end-to-end/api/add-permissions';
import { UserBuilder } from '../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';

describe('On the custom chart creation page: the chart type field, add the "Pie" option, in the Axes block: display the corresponding axes, when the axes are correctly populated', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
    deleteAllTestCases();
  });

  it('F05-UC0212.CT02 - chart-creation-list-cuf', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage();
    const createChart = new CreateChartViewPage();
    const chartDefinitionViewPage = new ChartDefinitionViewPage(1);
    const filterEnumSingleSelectWidgetElement = new FilterEnumSingleSelectWidgetElement();

    accessAddChartPageFromCustomReportWorkspace(customReportWorkspacePage, 'Project');

    cy.log('Step 2');
    createChart.assertCriterionChecked(0);

    cy.log('Step 3');
    createChart.chartTypeSelector.checkSelectedOption('Répartition');

    cy.log('Step 4');
    configureAxisAttributesAndAssertChartExist(createChart);

    cy.log('Step 5');
    filterAndSelectNodeAndAssertChartUpdate(createChart, filterEnumSingleSelectWidgetElement);

    cy.log('Step 6');
    changeFilterNodeAndAssertChartExist(createChart, filterEnumSingleSelectWidgetElement);

    cy.log('Step 7');
    assertChartPreviewAndDataConsistency(createChart);

    cy.log('Step 8');
    renameAndSaveChart(
      createChart,
      chartDefinitionViewPage,
      "Cas de test supprimé par statut d'exécution",
    );

    cy.log('Step 9');
    assertPieChartElementsInChartViewPage(chartDefinitionViewPage);

    cy.log('Step 10');
    assertInformationBlockForChartDetails(chartDefinitionViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('chart create', [
        ProfileBuilderPermission.CUSTOM_REPORT_READ,
        ProfileBuilderPermission.CUSTOM_REPORT_CREATE,
        ProfileBuilderPermission.CUSTOM_REPORT_WRITE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project')
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('TC1'),
          new TestCaseBuilder('TC2'),
          new TestCaseBuilder('TC3'),
        ])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaign').withIterations([
            new IterationBuilder('Iteration').withTestPlanItems([
              new IterationTestPlanItemBuilder('TC1', 'READY').withExecutions([
                new ClassicExecutionBuilder('READY'),
              ]),
              new IterationTestPlanItemBuilder('TC2', 'RUNNING').withExecutions([
                new ClassicExecutionBuilder('RUNNING'),
              ]),
              new IterationTestPlanItemBuilder('TC3', 'SUCCESS').withExecutions([
                new ClassicExecutionBuilder('SUCCESS'),
              ]),
            ]),
          ]),
        ]),
    ])
    .build();
}

function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}

function deleteAllTestCases() {
  deleteTestCase(1);
  deleteTestCase(2);
  deleteTestCase(3);
}

function configureAxisAttributesAndAssertChartExist(createChart: CreateChartViewPage) {
  createChart.axisSelector.assertExists();
  configureAxisAttributeSelection(createChart.axisSelector, 56, 'Agrégation', 'Statut');
  createChart.assertChartTypeExist('Pie');
}

function filterAndSelectNodeAndAssertChartUpdate(
  createChart: CreateChartViewPage,
  filterEnumSingleSelectWidgetElement: FilterEnumSingleSelectWidgetElement,
) {
  const filterAttribute = createChart.clickFilterButton();
  filterAttribute.filterAttribute('Cas de test');
  filterAttribute.addNodeToSelection('61');
  filterEnumSingleSelectWidgetElement.assertExists();
  filterEnumSingleSelectWidgetElement.selectFilter('Non');
  createChart.assertFilterAttribute(4, 'Non');
  createChart.assertChartTypeNotExist('Pie');
}

function changeFilterNodeAndAssertChartExist(
  createChart: CreateChartViewPage,
  filterEnumSingleSelectWidgetElement: FilterEnumSingleSelectWidgetElement,
) {
  createChart.clickFilterField();
  filterEnumSingleSelectWidgetElement.selectFilter('Oui');
  createChart.assertFilterAttribute(4, 'Oui');
  createChart.assertChartTypeExist('Pie');
}

function assertChartPreviewAndDataConsistency(createChart: CreateChartViewPage) {
  createChart.assertChartTypeExist('Pie');
  createChart.assertChartTitle('Nouveau graphique');
  //I will use the chartData data at the bottom of the page
  chartData.forEach((data, index) => {
    createChart.assertChartLegends(index, data.legend);
    createChart.assertChartSlicePercentage(index, data.percentage);
    createChart.assertChartSliceFillColor(index, data.fillColor);
  });
}

function assertPieChartElementsInChartViewPage(chartDefinitionViewPage: ChartDefinitionViewPage) {
  chartDefinitionViewPage.assertChartTypeExist('Pie');
  chartDefinitionViewPage.assertChartTitle("Cas de test supprimé par statut d'exécution");
  //I will use the chartData data at the bottom of the page
  chartData.forEach((data, index) => {
    chartDefinitionViewPage.assertChartLegends(index, data.legend);
    chartDefinitionViewPage.assertChartSlicePercentage(index, data.percentage);
    chartDefinitionViewPage.assertChartSliceFillColor(index, data.fillColor);
  });
}

function assertInformationBlockForChartDetails(chartDefinitionViewPage: ChartDefinitionViewPage) {
  chartDefinitionViewPage.showInformationPanel();
  chartDefinitionViewPage.assertHasCreationDate();
  chartDefinitionViewPage.assertCreatedBy('RonW');
  chartDefinitionViewPage.assertLastModification('jamais');
  chartDefinitionViewPage.checkPerimeter('Project');
  assertAxesField(
    chartDefinitionViewPage,
    'axis',
    'sqtm-core-custom-report:pie',
    'Par',
    WorkspaceColor.CAMPAIGN,
  );
  chartDefinitionViewPage.checkAxisInfo(
    "Items de plan d'exécution | Statut d'exécution - Agrégation",
  );
  chartDefinitionViewPage.checkInfoFilter(
    0,
    "Items de plan d'exécution | Cas de test supprimé - Égal : Oui",
  );
}

const chartData = [
  {
    legend: 'À exécuter',
    percentage: '33.3% (1)',
    fillColor: ExecutionStatusColor.READY_OR_NOT_RUN,
  },
  {
    legend: 'En cours',
    percentage: '33.3% (1)',
    fillColor: ExecutionStatusColor.RUNNING,
  },
  {
    legend: 'Succès',
    percentage: '33.3% (1)',
    fillColor: ExecutionStatusColor.SUCCESS,
  },
];
