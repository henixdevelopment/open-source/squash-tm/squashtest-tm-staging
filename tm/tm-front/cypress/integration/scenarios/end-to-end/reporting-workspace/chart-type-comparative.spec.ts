import { DatabaseUtils } from '../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  RequirementBuilder,
  RequirementFolderBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import { TestCaseBuilder } from '../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { TestCasesLinkToVerifiedRequirementVersionBuilder } from '../../../utils/end-to-end/prerequisite/builders/link-builders';
import {
  CampaignBuilder,
  ClassicExecutionBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { CustomReportWorkspacePage } from '../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import { MilestonePickerDialogElement } from '../../../page-objects/elements/grid/milestone-picker-dialog.element';
import {
  accessAddChartPageFromCustomReportWorkspace,
  assertAxesField,
  configureAxisAttributeSelection,
  renameAndSaveChart,
  selectTypeChartInCreateChartPage,
} from '../../scenario-parts/reporting.part';
import { beforeEach } from 'mocha';
import { ExecutionStatusColor, WorkspaceColor } from '../../../utils/color.utils';
import { CreateChartViewPage } from '../../../page-objects/pages/custom-report-workspace/chart/create-chart-view.page';
import { ChartDefinitionViewPage } from '../../../page-objects/pages/custom-report-workspace/chart/chart-definition-view.page';
import { addVersionToRequirement } from '../../../utils/end-to-end/api/requirement-versions/create-requirement-version';

describe('On the custom chart creation page: the chart type field, add the "Comparison" option, in the Axes block: display the corresponding axes, when the axes are correctly populated', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    createVersionToRequirement();
  });

  it('F05-UC0212.CT09 - chart-type-comparative', () => {
    cy.log('Step 1');
    cy.logInAs('admin', 'admin');
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage();
    const createChart = new CreateChartViewPage();
    const chartDefinitionViewPage = new ChartDefinitionViewPage(1);
    const pickerDialogElement = new MilestonePickerDialogElement();

    accessAddChartPageFromCustomReportWorkspace(customReportWorkspacePage, 'Project');

    cy.log('Step 2');
    selectProjectScopeInPerimeterBlock(createChart, pickerDialogElement);

    cy.log('Step 3');
    selectTypeChartInCreateChartPage(createChart, 'Comparaison');

    cy.log('Step 4');
    configureAxisAttributesInCreateChartViewPage(createChart);

    cy.log('Step 5');
    assertChartPreviewAndDataConsistency(createChart);

    cy.log('Step 6');
    renameAndSaveChart(
      createChart,
      chartDefinitionViewPage,
      "Résultat d'exécution par criticité d'exigences",
    );

    cy.log('Step 7');
    assertComparisonChartElementsInChartViewPage(chartDefinitionViewPage);

    cy.log('Step 8');
    assertInformationBlockForChartDetails(chartDefinitionViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withProjects([
      new ProjectBuilder('Project')
        .withRequirementLibraryNodes([
          new RequirementFolderBuilder('Folder').withRequirementLibraryNodes([
            new RequirementBuilder('Req1').withCriticality('CRITICAL'),
            new RequirementBuilder('Req2').withCriticality('MAJOR'),
            new RequirementBuilder('Req3').withCriticality('MINOR'),
            new RequirementBuilder('Req4').withCriticality('UNDEFINED'),
          ]),
        ])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Test1'),
          new TestCaseBuilder('Test2'),
          new TestCaseBuilder('Test3'),
          new TestCaseBuilder('Test4'),
          new TestCaseBuilder('Test5'),
          new TestCaseBuilder('Test6'),
          new TestCaseBuilder('Test7'),
          new TestCaseBuilder('Test10'),
          new TestCaseBuilder('Test11'),
          new TestCaseBuilder('Test12'),
          new TestCaseBuilder('Test13'),
        ])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campagne')
            .withIterations([new IterationBuilder('Iteration')])
            .withTestPlanItems([
              new IterationTestPlanItemBuilder('Test1', 'READY').withExecutions([
                new ClassicExecutionBuilder('READY'),
              ]),
              new IterationTestPlanItemBuilder('Test2', 'RUNNING').withExecutions([
                new ClassicExecutionBuilder('RUNNING'),
              ]),
              new IterationTestPlanItemBuilder('Test3', 'SUCCESS').withExecutions([
                new ClassicExecutionBuilder('SUCCESS'),
              ]),
              new IterationTestPlanItemBuilder('Test4', 'WARNING').withExecutions([
                new ClassicExecutionBuilder('WARNING'),
              ]),
              new IterationTestPlanItemBuilder('Test5', 'FAILURE').withExecutions([
                new ClassicExecutionBuilder('FAILURE'),
              ]),
              new IterationTestPlanItemBuilder('Test6', 'BLOCKED').withExecutions([
                new ClassicExecutionBuilder('BLOCKED'),
              ]),
              new IterationTestPlanItemBuilder('Test7', 'ERROR').withExecutions([
                new ClassicExecutionBuilder('ERROR'),
              ]),
              new IterationTestPlanItemBuilder('Test10', 'NOT_FOUND').withExecutions([
                new ClassicExecutionBuilder('NOT_FOUND'),
              ]),
              new IterationTestPlanItemBuilder('Test11', 'NOT_RUN').withExecutions([
                new ClassicExecutionBuilder('NOT_RUN'),
              ]),
              new IterationTestPlanItemBuilder('Test12', 'UNTESTABLE').withExecutions([
                new ClassicExecutionBuilder('UNTESTABLE'),
              ]),
              new IterationTestPlanItemBuilder('Test13', 'SETTLED').withExecutions([
                new ClassicExecutionBuilder('SETTLED'),
              ]),
            ]),
        ])
        .withLinks([
          new TestCasesLinkToVerifiedRequirementVersionBuilder(['Test1'], 'Req1'),
          new TestCasesLinkToVerifiedRequirementVersionBuilder(['Test5'], 'Req1'),
          new TestCasesLinkToVerifiedRequirementVersionBuilder(['Test11'], 'Req1'),
          new TestCasesLinkToVerifiedRequirementVersionBuilder(['Test12'], 'Req1'),
          new TestCasesLinkToVerifiedRequirementVersionBuilder(['Test2'], 'Req2'),
          new TestCasesLinkToVerifiedRequirementVersionBuilder(['Test6'], 'Req2'),
          new TestCasesLinkToVerifiedRequirementVersionBuilder(['Test13'], 'Req2'),
          new TestCasesLinkToVerifiedRequirementVersionBuilder(['Test3'], 'Req3'),
          new TestCasesLinkToVerifiedRequirementVersionBuilder(['Test7'], 'Req3'),
          new TestCasesLinkToVerifiedRequirementVersionBuilder(['Test4'], 'Req4'),
          new TestCasesLinkToVerifiedRequirementVersionBuilder(['Test10'], 'Req4'),
        ]),
    ])
    .build();
}

function createVersionToRequirement() {
  addVersionToRequirement(2, false, true);
  addVersionToRequirement(2, false, true);
  addVersionToRequirement(3, false, true);
  addVersionToRequirement(3, false, true);
  addVersionToRequirement(4, false, true);
  addVersionToRequirement(4, false, true);
  addVersionToRequirement(5, false, true);
  addVersionToRequirement(5, false, true);
}

function selectProjectScopeInPerimeterBlock(
  createChart: CreateChartViewPage,
  pickerDialogElement: MilestonePickerDialogElement,
) {
  createChart.selectScopeCriterion(1);
  pickerDialogElement.confirm();
  createChart.assertCriterionChecked(1);
}

function configureAxisAttributesInCreateChartViewPage(createChart: CreateChartViewPage) {
  createChart.measureSelector.assertExists();
  createChart.axisSelector.assertExists();
  createChart.seriesSelector.assertExists();
  configureAxisAttributeSelection(createChart.measureSelector, 54, 'Comptage', 'id');
  configureAxisAttributeSelection(createChart.axisSelector, 10, 'Agrégation');
  configureAxisAttributeSelection(createChart.seriesSelector, 56, 'Agrégation', 'statut');
}

function assertChartPreviewAndDataConsistency(createChart: CreateChartViewPage) {
  createChart.assertChartTypeExist('comparative');
  createChart.assertChartYAxisTitle('Criticité');
  const yAxisLabels = ['Critique', 'Majeure', 'Mineure', 'Non définie'];
  yAxisLabels.forEach((label, index) => {
    createChart.assertChartYAxisLabel(index, label);
  });
  createChart.assertChartXAxisTitle("ID d'item de plan d'exécution");
  createChart.assertChartTitle('Nouveau graphique');
  const chartLegends = [
    'Succès (+avertissement)',
    'Succès',
    'Non testable',
    'Non exécuté',
    'Introuvable',
    'Erreur',
    'En cours',
    'Échec',
    'Bloqué',
    'Arbitré',
    'À exécuter',
  ];
  chartLegends.forEach((legend, index) => {
    createChart.assertChartLegends(index, legend);
  });
  //I will use the barDetails data at the bottom of the page
  barDetails.forEach((bar, index) => {
    createChart.assertBarValueInChart(index, bar.value);
    createChart.assertChartBarColor(index, bar.color);
  });
}

function assertComparisonChartElementsInChartViewPage(
  chartDefinitionViewPage: ChartDefinitionViewPage,
) {
  chartDefinitionViewPage.assertChartTypeExist('comparative');
  chartDefinitionViewPage.assertChartYAxisTitle('Criticité');
  const yAxisLabels = ['Critique', 'Majeure', 'Mineure', 'Non définie'];
  yAxisLabels.forEach((label, index) => {
    chartDefinitionViewPage.assertChartYAxisLabel(index, label);
  });
  chartDefinitionViewPage.assertChartXAxisTitle("ID d'item de plan d'exécution");
  chartDefinitionViewPage.assertChartTitle("Résultat d'exécution par criticité d'exigences");
  const chartLegends = [
    'Succès (+avertissement)',
    'Succès',
    'Non testable',
    'Non exécuté',
    'Introuvable',
    'Erreur',
    'En cours',
    'Échec',
    'Bloqué',
    'Arbitré',
    'À exécuter',
  ];
  chartLegends.forEach((legend, index) => {
    chartDefinitionViewPage.assertChartLegends(index, legend);
  });
  //I will use the barDetails data at the bottom of the page
  barDetails.forEach((bar, index) => {
    chartDefinitionViewPage.assertBarValueInChart(index, bar.value);
    chartDefinitionViewPage.assertChartBarColor(index, bar.color);
  });
}

function assertInformationBlockForChartDetails(chartDefinitionViewPage: ChartDefinitionViewPage) {
  chartDefinitionViewPage.showInformationPanel();
  chartDefinitionViewPage.assertHasCreationDate();
  chartDefinitionViewPage.assertCreatedBy('admin');
  chartDefinitionViewPage.assertLastModification('jamais');
  chartDefinitionViewPage.checkPerimeter('Project');
  assertAxesField(
    chartDefinitionViewPage,
    'measure',
    'sqtm-core-custom-report:right',
    'Mesurer',
    WorkspaceColor.CAMPAIGN,
  );
  chartDefinitionViewPage.checkMeasureInfo(
    "Items de plan d'exécution | ID d'item de plan d'exécution - Comptage",
  );
  assertAxesField(
    chartDefinitionViewPage,
    'axis',
    'sqtm-core-custom-report:up',
    'Par',
    WorkspaceColor.REQUIREMENT,
  );
  chartDefinitionViewPage.checkAxisInfo("Versions d'exigence | Criticité - Agrégation");
  assertAxesField(
    chartDefinitionViewPage,
    'series',
    'unordered-list',
    'Séries',
    WorkspaceColor.CAMPAIGN,
  );
  chartDefinitionViewPage.checkSeriesInfo(
    "Items de plan d'exécution | Statut d'exécution - Agrégation",
  );
}
/*
For the same statuses that share the same color, and because an enum can only have a single value when the color is the same,
I tried to group them under a single designation. For example, 'ERROR_OR_BLOCKED,READY_OR_NOT_RUN...'.
 */

const barDetails = [
  { value: '1', color: ExecutionStatusColor.READY_OR_NOT_RUN },
  { value: '1', color: ExecutionStatusColor.SETTLED_OR_WARNING },
  { value: '1', color: ExecutionStatusColor.ERROR_OR_BLOCKED },
  { value: '1', color: ExecutionStatusColor.FAILURE },
  { value: '1', color: ExecutionStatusColor.RUNNING },
  { value: '1', color: ExecutionStatusColor.ERROR_OR_BLOCKED },
  { value: '1', color: ExecutionStatusColor.NOT_FOUND_OR_UNTESTABLE },
  { value: '1', color: ExecutionStatusColor.READY_OR_NOT_RUN },
  { value: '1', color: ExecutionStatusColor.NOT_FOUND_OR_UNTESTABLE },
  { value: '1', color: ExecutionStatusColor.SUCCESS },
  { value: '1', color: ExecutionStatusColor.SETTLED_OR_WARNING },
];
