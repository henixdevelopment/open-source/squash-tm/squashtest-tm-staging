import { beforeEach } from 'mocha';
import { DatabaseUtils } from '../../../utils/database.utils';
import { CustomReportWorkspacePage } from '../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import { PrerequisiteBuilder } from '../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { TestCaseBuilder } from '../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CampaignBuilder,
  ClassicExecutionBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
  DeclareIssueBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  accessAddChartPageFromCustomReportWorkspace,
  assertAxesField,
  configureAxisAttributeSelection,
  renameAndSaveChart,
  selectTypeChartInCreateChartPage,
} from '../../scenario-parts/reporting.part';
import { TreeElement } from '../../../page-objects/elements/grid/grid.element';
import { selectByDataTestElementId } from '../../../utils/basic-selectors';
import {
  BugtrackerProjectBindingBuilder,
  BugtrackerServerBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/server-prerequisite-builders';
import { AuthenticationProtocol } from '../../../../../projects/sqtm-core/src/lib/model/third-party-server/authentication.model';
import { WorkspaceColor } from '../../../utils/color.utils';
import { CreateChartViewPage } from '../../../page-objects/pages/custom-report-workspace/chart/create-chart-view.page';
import { ChartDefinitionViewPage } from '../../../page-objects/pages/custom-report-workspace/chart/chart-definition-view.page';

describe('On the custom chart creation page: the chart type field, add the "Histogram" option, in the Axes block: display the corresponding axes, when the axes are correctly populated', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
  });

  it('F05-UC0212.CT03 - chart-type-histogram', () => {
    cy.log('Step 1');
    cy.logInAs('admin', 'admin');
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage();
    const createChart = new CreateChartViewPage();
    const chartDefinitionViewPage = new ChartDefinitionViewPage(1);

    accessAddChartPageFromCustomReportWorkspace(customReportWorkspacePage, 'Project');

    cy.log('Step 2');
    selectProjectScopeInPerimeterBlock(createChart);

    cy.log('Step 3');
    selectTypeChartInCreateChartPage(createChart, 'Histogramme');

    cy.log('Step 4');
    configureAxisAttributesInCreateChartViewPage(createChart);

    cy.log('Step 5');
    assertChartPreviewAndDataConsistency(createChart);

    cy.log('Step 6');
    renameAndSaveChart(createChart, chartDefinitionViewPage, "Nombre d'anomalies par itération");

    cy.log('Step 7');
    assertHistogramChartElementsInChartViewPage(chartDefinitionViewPage);

    cy.log('Step 8');
    assertInformationBlockForChartDetails(chartDefinitionViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withServers([
      new BugtrackerServerBuilder('Hello', 'https://gitlab.com', 'gitlab.bugtracker')
        .withAuthProtocol(AuthenticationProtocol.TOKEN_AUTH)
        .withUserLevelAuthPolicy(),
    ])
    .withProjects([
      new ProjectBuilder('Project')
        .withBugtrackerOnProject([new BugtrackerProjectBindingBuilder()])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('TC1'),
          new TestCaseBuilder('TC2'),
          new TestCaseBuilder('TC3'),
          new TestCaseBuilder('TC4'),
          new TestCaseBuilder('TC5'),
          new TestCaseBuilder('TC6'),
          new TestCaseBuilder('TC7'),
          new TestCaseBuilder('TC8'),
          new TestCaseBuilder('TC9'),
          new TestCaseBuilder('TC10'),
        ])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaign').withIterations([
            new IterationBuilder('Iteration')
              .withReference('IT-01')
              .withTestPlanItems([
                new IterationTestPlanItemBuilder('TC1', 'READY').withExecutions([
                  new ClassicExecutionBuilder('READY'),
                ]),
                new IterationTestPlanItemBuilder('TC2', 'FAILURE').withExecutions([
                  new ClassicExecutionBuilder('FAILURE').withDeclareIssue([
                    new DeclareIssueBuilder('1', 2),
                  ]),
                ]),
                new IterationTestPlanItemBuilder('TC3', 'FAILURE').withExecutions([
                  new ClassicExecutionBuilder('FAILURE').withDeclareIssue([
                    new DeclareIssueBuilder('3', 3),
                  ]),
                ]),
                new IterationTestPlanItemBuilder('TC4', 'FAILURE').withExecutions([
                  new ClassicExecutionBuilder('FAILURE').withDeclareIssue([
                    new DeclareIssueBuilder('2', 4),
                  ]),
                ]),
              ]),
            new IterationBuilder('Iteration1')
              .withReference('IT-02')
              .withTestPlanItems([
                new IterationTestPlanItemBuilder('TC5', 'FAILURE').withExecutions([
                  new ClassicExecutionBuilder('FAILURE').withDeclareIssue([
                    new DeclareIssueBuilder('1', 5),
                  ]),
                ]),
                new IterationTestPlanItemBuilder('TC6', 'RUNNING').withExecutions([
                  new ClassicExecutionBuilder('RUNNING'),
                ]),
                new IterationTestPlanItemBuilder('TC7', 'FAILURE').withExecutions([
                  new ClassicExecutionBuilder('FAILURE').withDeclareIssue([
                    new DeclareIssueBuilder('2', 7),
                  ]),
                ]),
                new IterationTestPlanItemBuilder('TC8', 'SUCCESS').withExecutions([
                  new ClassicExecutionBuilder('SUCCESS'),
                ]),
              ]),
            new IterationBuilder('Iteration2')
              .withReference('IT-03')
              .withTestPlanItems([
                new IterationTestPlanItemBuilder('TC9', 'FAILURE').withExecutions([
                  new ClassicExecutionBuilder('FAILURE').withDeclareIssue([
                    new DeclareIssueBuilder('1', 9),
                  ]),
                ]),
                new IterationTestPlanItemBuilder('TC10', 'SUCCESS').withExecutions([
                  new ClassicExecutionBuilder('SUCCESS'),
                ]),
              ]),
          ]),
        ]),
    ])
    .build();
}

function selectProjectScopeInPerimeterBlock(createChart: CreateChartViewPage) {
  createChart.selectScopeCriterion(2);
  cy.get('[nztabscrolllist]').children().eq(2).click();
  const campaignTree = TreeElement.createTreeElement(
    'campaign-limited-tree-picker',
    'campaign-tree',
  );
  campaignTree.assertExists();
  campaignTree.findRowId('NAME', 'Project').then((rowId) => {
    campaignTree.getTreeNodeCell(rowId).toggle();
  });
  campaignTree.findRowId('NAME', 'Campaign').then((rowId) => {
    campaignTree.pickNode(rowId);
  });
  cy.get(selectByDataTestElementId('confirm')).click();
}

function configureAxisAttributesInCreateChartViewPage(createChart: CreateChartViewPage) {
  createChart.measureSelector.assertExists();
  createChart.axisSelector.assertExists();
  configureAxisAttributeSelection(createChart.measureSelector, 53, 'Comptage', 'Nombre');
  createChart.measureSelector.selectOption('Somme');
  createChart.measureSelector.checkOption('Somme');
  configureAxisAttributeSelection(createChart.axisSelector, 47, 'Agrégation', 'Référence');
}

function assertChartPreviewAndDataConsistency(createChart: CreateChartViewPage) {
  createChart.assertChartTypeExist('bar');
  createChart.assertChartTitle('Nouveau graphique');
  createChart.assertChartYAxisTitle("Nombre d'anomalies");
  createChart.assertChartXAxisTitle('Référence');
  const xAxisLabels = ['IT-01', 'IT-02', 'IT-03'];
  xAxisLabels.forEach((label, index) => {
    createChart.assertChartXAxisLabel(index, label);
  });

  barsData.forEach((bar, index) => {
    createChart.assertBarValueInChart(index, bar.value);
    createChart.assertChartBarColor(index, bar.color);
  });
}

function assertHistogramChartElementsInChartViewPage(
  chartDefinitionViewPage: ChartDefinitionViewPage,
) {
  chartDefinitionViewPage.assertChartTypeExist('bar');
  chartDefinitionViewPage.assertChartTitle("Nombre d'anomalies par itération");
  chartDefinitionViewPage.assertChartYAxisTitle("Nombre d'anomalies");
  chartDefinitionViewPage.assertChartXAxisTitle('Référence');
  const xAxisLabels = ['IT-01', 'IT-02', 'IT-03'];
  xAxisLabels.forEach((label, index) => {
    chartDefinitionViewPage.assertChartXAxisLabel(index, label);
  });

  barsData.forEach((bar, index) => {
    chartDefinitionViewPage.assertBarValueInChart(index, bar.value);
    chartDefinitionViewPage.assertChartBarColor(index, bar.color);
  });
}

function assertInformationBlockForChartDetails(chartDefinitionViewPage: ChartDefinitionViewPage) {
  chartDefinitionViewPage.showInformationPanel();
  chartDefinitionViewPage.assertHasCreationDate();
  chartDefinitionViewPage.assertCreatedBy('admin');
  chartDefinitionViewPage.assertLastModification('jamais');
  chartDefinitionViewPage.checkPerimeter('Project');
  assertAxesField(
    chartDefinitionViewPage,
    'measure',
    'sqtm-core-custom-report:up',
    'Mesurer',
    WorkspaceColor.CAMPAIGN,
  );
  chartDefinitionViewPage.checkMeasureInfo("Itérations | Nombre d'anomalies - Somme");
  assertAxesField(
    chartDefinitionViewPage,
    'axis',
    'sqtm-core-custom-report:right',
    'Par',
    WorkspaceColor.CAMPAIGN,
  );
  chartDefinitionViewPage.checkAxisInfo('Itérations | Référence - Agrégation');
}

const barsData = [
  { value: '3', color: 'rgb(39, 125, 161)' },
  { value: '2', color: 'rgb(249, 199, 79)' },
  { value: '1', color: 'rgb(249, 132, 74)' },
];
