import { DatabaseUtils } from '../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  ActionTestStepBuilder,
  TestCaseBuilder,
  TestCaseFolderBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CampaignBuilder,
  ClassicExecutionBuilder,
  ExecutionStepBuilder,
  ExecutionStepsExecutionBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { ChartDefinitionViewPage } from '../../../page-objects/pages/custom-report-workspace/chart/chart-definition-view.page';
import { CreateChartViewPage } from '../../../page-objects/pages/custom-report-workspace/chart/create-chart-view.page';
import { CustomReportWorkspacePage } from '../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import {
  accessAddChartPageFromCustomReportWorkspace,
  assertAxesField,
  configureAxisAttributeSelection,
  renameAndSaveChart,
  selectTypeChartInCreateChartPage,
  testPointX2YHover,
  testPointXYHover,
} from '../../scenario-parts/reporting.part';
import { TreeElement } from '../../../page-objects/elements/grid/grid.element';
import { selectByDataTestElementId } from '../../../utils/basic-selectors';
import { TestCaseChartColor, WorkspaceColor } from '../../../utils/color.utils';

describe('On the custom chart creation page: the chart type field, add the "Trend" option, in the Axes block: display the corresponding axes, when the axes are correctly populated', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
  });

  it('F05-UC0212.CT08 - chart-type-trend', () => {
    cy.log('Step 1');
    cy.logInAs('admin', 'admin');
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage();
    const createChart = new CreateChartViewPage();
    const chartDefinitionViewPage = new ChartDefinitionViewPage(1);

    accessAddChartPageFromCustomReportWorkspace(customReportWorkspacePage, 'Project');

    cy.log('Step 2');
    selectProjectScopeInPerimeterBlock(createChart);

    cy.log('Step 3');
    selectTypeChartInCreateChartPage(createChart, 'Tendances');

    cy.log('Step 4');
    configureAxisAttributesInCreateChartViewPage(createChart);

    cy.log('Step 5');
    assertChartPreviewAndDataConsistency(createChart);

    cy.log('Step 6');
    renameAndSaveChart(
      createChart,
      chartDefinitionViewPage,
      'Si les cas de test ont été exécutés ou pas',
    );

    cy.log('Step 7');
    assertTrendChartElementsInChartViewPage(chartDefinitionViewPage);

    cy.log('Step 8');
    assertInformationBlockForChartDetails(chartDefinitionViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withProjects([
      new ProjectBuilder('Project')
        .withTestCaseLibraryNodes([
          new TestCaseFolderBuilder('Folder').withTestCaseLibraryNodes([
            new TestCaseBuilder('Test1'),
            new TestCaseBuilder('Test2').withSteps([new ActionTestStepBuilder('action', 'result')]),
            new TestCaseBuilder('Test3').withSteps([new ActionTestStepBuilder('action', 'result')]),
            new TestCaseBuilder('Test4').withSteps([new ActionTestStepBuilder('action', 'result')]),
            new TestCaseBuilder('Test5').withSteps([new ActionTestStepBuilder('action', 'result')]),
            new TestCaseBuilder('Test6').withSteps([new ActionTestStepBuilder('action', 'result')]),
          ]),
        ])
        .withCreatedOn('2024-05-09 15:05:25')
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campagne')
            .withIterations([new IterationBuilder('Iteration')])
            .withTestPlanItems([
              new IterationTestPlanItemBuilder('Test1'),
              new IterationTestPlanItemBuilder('Test2', 'SUCCESS').withExecutions([
                new ClassicExecutionBuilder('SUCCESS').withExecutionStep([
                  new ExecutionStepBuilder('SUCCESS').withExecutionStepExecution([
                    new ExecutionStepsExecutionBuilder('admin').withLastExecutedOn(
                      '2024-05-09 15:05:25',
                    ),
                  ]),
                ]),
              ]),
              new IterationTestPlanItemBuilder('Test3', 'SUCCESS').withExecutions([
                new ClassicExecutionBuilder('SUCCESS').withExecutionStep([
                  new ExecutionStepBuilder('SUCCESS').withExecutionStepExecution([
                    new ExecutionStepsExecutionBuilder('admin').withLastExecutedOn(
                      '2024-04-09 15:05:25',
                    ),
                  ]),
                ]),
              ]),
              new IterationTestPlanItemBuilder('Test4', 'FAILURE').withExecutions([
                new ClassicExecutionBuilder('FAILURE').withExecutionStep([
                  new ExecutionStepBuilder('SUCCESS').withExecutionStepExecution([
                    new ExecutionStepsExecutionBuilder('admin').withLastExecutedOn(
                      '2024-03-09 15:05:25',
                    ),
                  ]),
                ]),
              ]),
              new IterationTestPlanItemBuilder('Test5', 'FAILURE').withExecutions([
                new ClassicExecutionBuilder('FAILURE').withExecutionStep([
                  new ExecutionStepBuilder('SUCCESS').withExecutionStepExecution([
                    new ExecutionStepsExecutionBuilder('admin').withLastExecutedOn(
                      '2024-02-09 15:05:25',
                    ),
                  ]),
                ]),
              ]),
              new IterationTestPlanItemBuilder('Test6', 'SUCCESS').withExecutions([
                new ClassicExecutionBuilder('SUCCESS').withExecutionStep([
                  new ExecutionStepBuilder('SUCCESS').withExecutionStepExecution([
                    new ExecutionStepsExecutionBuilder('admin').withLastExecutedOn(
                      '2024-01-09 15:05:25',
                    ),
                  ]),
                ]),
              ]),
            ]),
        ]),
    ])
    .build();
}

function selectProjectScopeInPerimeterBlock(createChart: CreateChartViewPage) {
  createChart.selectScopeCriterion(2);
  cy.get('[nztabscrolllist]').children().eq(1).click();
  const testcaseTree = TreeElement.createTreeElement('test-case-tree-picker', 'test-case-tree');
  testcaseTree.assertExists();
  testcaseTree.findRowId('NAME', 'Project').then((rowId) => {
    testcaseTree.getTreeNodeCell(rowId).toggle();
  });
  testcaseTree.findRowId('NAME', 'Folder').then((rowId) => {
    testcaseTree.pickNode(rowId);
  });
  cy.get(selectByDataTestElementId('confirm')).click();
}

function configureAxisAttributesInCreateChartViewPage(createChart: CreateChartViewPage) {
  createChart.measureSelector.assertExists();
  createChart.axisSelector.assertExists();
  createChart.seriesSelector.assertExists();
  configureAxisAttributeSelection(createChart.measureSelector, 54, 'Comptage', 'id');
  configureAxisAttributeSelection(createChart.axisSelector, 57, 'Par jour', 'Date');
  configureAxisAttributeSelection(createChart.seriesSelector, 62, 'Agrégation', 'Exécuté');
}

function assertChartPreviewAndDataConsistency(createChart: CreateChartViewPage) {
  createChart.assertChartTypeExist('trend');
  createChart.assertChartYAxisTitle("ID d'item de plan d'exécution");
  createChart.assertChartXAxisLabel(0, 'jamais');
  const x2AxisLabels = ['Jan 2024', 'Fév 2024', 'Mar 2024', 'Avr 2024', 'Mai 2024'];
  x2AxisLabels.forEach((label, index) => {
    createChart.assertChartX2AxisLabel(index, label);
  });
  createChart.assertChartX2AxisTitle('Date de dernière exécution');
  createChart.assertChartTitle('Nouveau graphique');
  const chartLegends = ['Faux', 'Vrai'];
  chartLegends.forEach((legend, index) => {
    createChart.assertChartLegends(index, legend);
  });
  createChart.assertChartBarColor(0, TestCaseChartColor.NEVER_EXECUTED);
  [7, 8, 9, 10, 11].forEach((index) => {
    createChart.assertChartPointColor(index, TestCaseChartColor.EXECUTED);
  });
  const hoverPointsX2Y = [
    { x: 0, y: 250, text: 'Vrai', date: '9 Jan, 2024' },
    { x: 40, y: 200, text: 'Vrai', date: '9 Fév, 2024' },
    { x: 80, y: 150, text: 'Vrai', date: '9 Mar, 2024' },
    { x: 120, y: 87, text: 'Vrai', date: '9 Avr, 2024' },
    { x: 135, y: 20, text: 'Vrai', date: '9 Mai, 2024' },
  ];
  hoverPointsX2Y.forEach(({ x, y, text, date }) => {
    testPointX2YHover(createChart, x, y, 'text', text, date);
  });

  testPointXYHover(createChart, 5, 250, 'text', 'Faux', 'jamais');
}

function assertTrendChartElementsInChartViewPage(chartDefinitionViewPage: ChartDefinitionViewPage) {
  chartDefinitionViewPage.assertChartTypeExist('trend');
  chartDefinitionViewPage.assertChartYAxisTitle("ID d'item de plan d'exécution");
  chartDefinitionViewPage.assertChartXAxisLabel(0, 'jamais');
  const yAxisLabels = ['Jan 2024', 'Fév 2024', 'Mar 2024', 'Avr 2024', 'Mai 2024'];
  yAxisLabels.forEach((label, index) => {
    chartDefinitionViewPage.assertChartX2AxisLabel(index, label);
  });
  chartDefinitionViewPage.assertChartX2AxisTitle('Date de dernière exécution');
  chartDefinitionViewPage.assertChartTitle('Si les cas de test ont été exécutés ou pas');
  const chartLegends = ['Faux', 'Vrai'];
  chartLegends.forEach((legend, index) => {
    chartDefinitionViewPage.assertChartLegends(index, legend);
  });
  chartDefinitionViewPage.assertChartBarColor(0, TestCaseChartColor.NEVER_EXECUTED);
  [7, 8, 9, 10, 11].forEach((index) => {
    chartDefinitionViewPage.assertChartPointColor(index, TestCaseChartColor.EXECUTED);
  });
  const hoverPointsX2Y = [
    { x: 20, y: 420, text: 'Vrai', date: '9 Jan, 2024' },
    { x: 80, y: 320, text: 'Vrai', date: '9 Fév, 2024' },
    { x: 140, y: 220, text: 'Vrai', date: '9 Mar, 2024' },
    { x: 200, y: 120, text: 'Vrai', date: '9 Avr, 2024' },
    { x: 260, y: 20, text: 'Vrai', date: '9 Mai, 2024' },
  ];
  hoverPointsX2Y.forEach(({ x, y, text, date }) => {
    testPointX2YHover(chartDefinitionViewPage, x, y, 'text', text, date);
  });

  testPointXYHover(chartDefinitionViewPage, 12, 420, 'text', 'Faux', 'jamais');
}

function assertInformationBlockForChartDetails(chartDefinitionViewPage: ChartDefinitionViewPage) {
  chartDefinitionViewPage.showInformationPanel();
  chartDefinitionViewPage.assertHasCreationDate();
  chartDefinitionViewPage.assertCreatedBy('admin');
  chartDefinitionViewPage.assertLastModification('jamais');
  chartDefinitionViewPage.checkPerimeter('Project');
  assertAxesField(
    chartDefinitionViewPage,
    'measure',
    'sqtm-core-custom-report:up',
    'Mesurer',
    WorkspaceColor.CAMPAIGN,
  );
  chartDefinitionViewPage.checkMeasureInfo(
    "Items de plan d'exécution | ID d'item de plan d'exécution - Comptage",
  );
  assertAxesField(
    chartDefinitionViewPage,
    'axis',
    'sqtm-core-custom-report:right',
    'Par',
    WorkspaceColor.CAMPAIGN,
  );
  chartDefinitionViewPage.checkAxisInfo(
    "Items de plan d'exécution | Date de dernière exécution - Par jour",
  );
  assertAxesField(
    chartDefinitionViewPage,
    'series',
    'unordered-list',
    'Séries',
    WorkspaceColor.CAMPAIGN,
  );
  chartDefinitionViewPage.checkSeriesInfo("Items de plan d'exécution | Exécuté - Agrégation");
}
