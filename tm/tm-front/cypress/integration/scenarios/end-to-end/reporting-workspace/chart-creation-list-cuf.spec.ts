import { DatabaseUtils } from '../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
  CustomFieldValue,
  ListOptionCustomFieldBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { ProjectBuilder } from '../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { TestCaseBuilder } from '../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { BindableEntity } from '../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { RequirementBuilder } from '../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  CampaignBuilder,
  ClassicExecutionBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  InfoListBuilder,
  InfoListItemBuilder,
  RequirementCategoriesListBindingBuilder,
  TestCaseNatureListBindingBuilder,
  TestCaseTypeListBindingBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/info-list-builder';
import { CustomReportWorkspacePage } from '../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import {
  accessAddChartPageFromCustomReportWorkspace,
  assertAxesField,
} from '../../scenario-parts/reporting.part';
import { selectByDataTestElementId } from '../../../utils/basic-selectors';
import { GroupedMultiListElement } from '../../../page-objects/elements/filters/grouped-multi-list.element';
import { ProjectPickerDialogElement } from '../../../page-objects/elements/grid/project-picker-dialog.element';
import { WorkspaceColor } from '../../../utils/color.utils';
import { CreateChartViewPage } from '../../../page-objects/pages/custom-report-workspace/chart/create-chart-view.page';
import { ChartDefinitionViewPage } from '../../../page-objects/pages/custom-report-workspace/chart/chart-definition-view.page';

describe(
  'Attribute tree display on custom chart creation page: in the attribute selection tree on the custom chart page, add custom fields (CUFs) for all relevant entities. The CUFs displayed adjust based on the selected scope.' +
    'Impact of scope changes on axes and filters: when the scope changes: CUFs in axes or filters are removed, custom lists remain in axes but are removed from filters.',
  function () {
    beforeEach(function () {
      Cypress.Cookies.debug(true);
      DatabaseUtils.cleanDatabase();
      initTestData();
    });

    it('F05-UC0212.CT11 - chart-creation-list-cuf', () => {
      cy.log('Step 1');
      cy.logInAs('admin', 'admin');
      const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage();
      const createChart = new CreateChartViewPage();
      const projectPickerDialogElement = new ProjectPickerDialogElement();
      const chartDefinitionViewPage = new ChartDefinitionViewPage(1);

      accessAddChartPageFromCustomReportWorkspace(customReportWorkspacePage, 'Project1');

      cy.log('Step 2');
      assertCustomFieldsInAxesSelection(createChart);

      cy.log('Step 3');
      assertCustomFieldsInFiltersSelection(createChart);

      cy.log('step 4');
      assertCustomListsComplianceInAxes(createChart);

      cy.log('Step 5');
      setupChartWithCufAndVerifyAxesFilters(createChart);

      cy.log('Step 6');
      updateScopeAndValidateCufExclusion(createChart, projectPickerDialogElement);

      cy.log('Step 7');
      assertCufDisplayOnEntitiesInSelection(createChart);

      cy.log('Step 8');
      configureChartWithTypeAttribute(createChart);

      cy.log('Step 9');
      updateScopeWithProjectAndVerifyCustomListAttributes(createChart, projectPickerDialogElement);

      cy.log('Step 10');
      createHistogramAndVerifyCufAttributesDisplay(createChart);

      cy.log('Step 11');
      saveChartWithNameAndValidateConsistency(createChart, chartDefinitionViewPage);

      cy.log('Step 12');
      verifyChartConformityAndDetailsInChartDefinitionPage(chartDefinitionViewPage);
    });
  },
);
function initTestData() {
  return new PrerequisiteBuilder()
    .withCufs([
      new CustomFieldBuilder('Text simple', InputType.PLAIN_TEXT),
      new CustomFieldBuilder('Text rich', InputType.RICH_TEXT),
      new CustomFieldBuilder('Checkbox', InputType.CHECKBOX),
      new CustomFieldBuilder('ListCuf1', InputType.DROPDOWN_LIST).withOptions([
        new ListOptionCustomFieldBuilder('opt1', 'code1'),
        new ListOptionCustomFieldBuilder('opt2', 'code2'),
      ]),
      new CustomFieldBuilder('DateCuf', InputType.DATE_PICKER),
      new CustomFieldBuilder('Number', InputType.NUMERIC),
      new CustomFieldBuilder('Tag', InputType.TAG),
      new CustomFieldBuilder('Automatisation', InputType.DROPDOWN_LIST).withOptions([
        new ListOptionCustomFieldBuilder('Robot', 'code1').withColour('#b8188f'),
        new ListOptionCustomFieldBuilder('UFT', 'code2').withColour('#3bc4d1'),
        new ListOptionCustomFieldBuilder('Cucumber', 'code3').withColour('#21d93b'),
        new ListOptionCustomFieldBuilder('Playright', 'code4').withColour('#111aae'),
      ]),
    ])
    .withInfoLists([
      new InfoListBuilder('ListInfo1', 'code1').withItems([
        new InfoListItemBuilder('Choix 1', 'Choix 1').withColor('#b8185f'),
        new InfoListItemBuilder('Choix 2', 'Choix 2').withColor('#05d93b'),
        new InfoListItemBuilder('Choix 3', 'Choix 3').withColor('#9bc4d1'),
      ]),
      new InfoListBuilder('ListInfo2', 'code2').withItems([
        new InfoListItemBuilder('fafa', 'fafa').withColor('#9bc4d1'),
        new InfoListItemBuilder('lala', 'lala').withColor('#b8188f'),
        new InfoListItemBuilder('jaja', 'jaja').withColor('#3bc4d1'),
      ]),
      new InfoListBuilder('ListInfo3', 'code3').withItems([
        new InfoListItemBuilder('E', 'E').withColor('#b8185f'),
        new InfoListItemBuilder('F', 'F').withColor('#81d93b'),
        new InfoListItemBuilder('J', 'J').withColor('#111aae'),
      ]),
      new InfoListBuilder('ListInfo4', 'code4').withItems([
        new InfoListItemBuilder('FF', 'FF').withColor('#b8188f'),
        new InfoListItemBuilder('GG', 'GG').withColor('#81d93b'),
        new InfoListItemBuilder('EE', 'EE').withColor('#9bc4d1'),
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project1')
        .withRequirementLibraryNodes([
          new RequirementBuilder('Req1'),
          new RequirementBuilder('Req2'),
          new RequirementBuilder('Req3'),
        ])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Test1').withCufValues([
            new CustomFieldValue('Automatisation', 'UFT'),
          ]),
          new TestCaseBuilder('Test2').withCufValues([
            new CustomFieldValue('Automatisation', 'Robot'),
          ]),
          new TestCaseBuilder('Test3').withCufValues([
            new CustomFieldValue('Automatisation', 'Cucumber'),
          ]),
        ])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaigne')
            .withIterations([new IterationBuilder('Iteration')])
            .withTestPlanItems([
              new IterationTestPlanItemBuilder('Test1', 'READY').withExecutions([
                new ClassicExecutionBuilder('READY'),
              ]),
              new IterationTestPlanItemBuilder('Test2', 'RUNNING').withExecutions([
                new ClassicExecutionBuilder('RUNNING'),
              ]),
              new IterationTestPlanItemBuilder('Test3', 'SUCCESS').withExecutions([
                new ClassicExecutionBuilder('SUCCESS'),
              ]),
            ]),
        ])
        .withCufsOnProject([
          new CustomFieldOnProjectBuilder('Text simple', BindableEntity.REQUIREMENT_VERSION),
          new CustomFieldOnProjectBuilder('Text rich', BindableEntity.TEST_CASE),
          new CustomFieldOnProjectBuilder('Checkbox', BindableEntity.CAMPAIGN),
          new CustomFieldOnProjectBuilder('ListCuf1', BindableEntity.ITERATION),
          new CustomFieldOnProjectBuilder('DateCuf', BindableEntity.EXECUTION),
          new CustomFieldOnProjectBuilder('Number', BindableEntity.CAMPAIGN),
          new CustomFieldOnProjectBuilder('Tag', BindableEntity.REQUIREMENT_VERSION),
          new CustomFieldOnProjectBuilder('Automatisation', BindableEntity.TEST_CASE),
        ])
        .withRequirementListsOnProject([
          new RequirementCategoriesListBindingBuilder('ListInfo1', 'Choix 1', 1),
          new RequirementCategoriesListBindingBuilder('ListInfo1', 'Choix 2', 2),
          new RequirementCategoriesListBindingBuilder('ListInfo1', 'Choix 3', 3),
        ])
        .withNatureTestCaseListsOnProject([
          new TestCaseNatureListBindingBuilder('ListInfo2', 'fafa', 1),
          new TestCaseNatureListBindingBuilder('ListInfo2', 'lala', 2),
          new TestCaseNatureListBindingBuilder('ListInfo2', 'jaja', 3),
        ])
        .withTypeTestCaseListsOnProject([
          new TestCaseTypeListBindingBuilder('ListInfo3', 'E', 1),
          new TestCaseTypeListBindingBuilder('ListInfo3', 'F', 2),
          new TestCaseTypeListBindingBuilder('ListInfo3', 'J', 3),
        ]),
      new ProjectBuilder('Project2')
        .withRequirementLibraryNodes([
          new RequirementBuilder('Requirement1'),
          new RequirementBuilder('Requirement2'),
          new RequirementBuilder('Requirement3'),
        ])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Tc1'),
          new TestCaseBuilder('Tc2'),
          new TestCaseBuilder('Tc3'),
        ])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Camp')
            .withIterations([new IterationBuilder('Iter')])
            .withTestPlanItems([
              new IterationTestPlanItemBuilder('Tc1', 'SKIPPED').withExecutions([
                new ClassicExecutionBuilder('SKIPPED'),
              ]),
              new IterationTestPlanItemBuilder('Tc2', 'FAILURE').withExecutions([
                new ClassicExecutionBuilder('FAILURE'),
              ]),
              new IterationTestPlanItemBuilder('Tc3', 'SUCCESS').withExecutions([
                new ClassicExecutionBuilder('SUCCESS'),
              ]),
            ]),
        ])
        .withCufsOnProject([
          new CustomFieldOnProjectBuilder('Text simple', BindableEntity.REQUIREMENT_VERSION),
          new CustomFieldOnProjectBuilder('Text simple', BindableEntity.TEST_CASE),
          new CustomFieldOnProjectBuilder('Text simple', BindableEntity.CAMPAIGN),
          new CustomFieldOnProjectBuilder('Text simple', BindableEntity.ITERATION),
          new CustomFieldOnProjectBuilder('Text simple', BindableEntity.EXECUTION),
        ])
        .withTypeTestCaseListsOnProject([
          new TestCaseTypeListBindingBuilder('ListInfo4', 'FF', 4),
          new TestCaseTypeListBindingBuilder('ListInfo4', 'GG', 5),
          new TestCaseTypeListBindingBuilder('ListInfo4', 'EE', 6),
        ]),
    ])
    .build();
}

function assertCustomFieldsInAxesSelection(createChart: CreateChartViewPage) {
  const attributeSelector = createChart.axisSelector.openAttributeSelector();
  assertCustomFieldsInSelection(attributeSelector, [
    'Text simple',
    'Tag',
    'Automatisation',
    'Checkbox',
    'Number',
    'ListCuf1',
    'DateCuf',
  ]);
  assertCustomFieldsInSelection(attributeSelector, ['Text rich'], false);
  cy.clickOnOverlayBackdrop();
}

function assertCustomFieldsInFiltersSelection(createChart: CreateChartViewPage) {
  const filterAttribute = createChart.clickFilterButton();
  assertCustomFieldsInSelection(filterAttribute, [
    'Text simple',
    'Tag',
    'Automatisation',
    'Checkbox',
    'Number',
    'ListCuf1',
    'DateCuf',
  ]);
  assertCustomFieldsInSelection(filterAttribute, ['Text rich'], false);
  cy.clickOnOverlayBackdrop();
}

function assertCustomListsComplianceInAxes(createChart: CreateChartViewPage) {
  const listCufsData = [
    {
      attribute: 'Catégorie',
      node: 6,
      legends: ['Choix 1', 'Choix 2', 'Choix 3'],
      percentages: ['33.3% (1)', '33.3% (1)', '33.3% (1)'],
      colors: ['rgb(184, 24, 95)', 'rgb(5, 217, 59)', 'rgb(155, 196, 209)'],
    },
    {
      attribute: 'Nature',
      node: 22,
      legends: ['fafa', 'jaja', 'lala'],
      percentages: ['33.3% (1)', '33.3% (1)', '33.3% (1)'],
      colors: ['rgb(155, 196, 209)', 'rgb(59, 196, 209)', 'rgb(184, 24, 143)'],
    },
    {
      attribute: 'Type',
      node: 23,
      legends: ['E', 'F', 'J'],
      percentages: ['33.3% (1)', '33.3% (1)', '33.3% (1)'],
      colors: ['rgb(184, 24, 95)', 'rgb(129, 217, 59)', 'rgb(17, 26, 174)'],
    },
  ];

  for (const listCuf of listCufsData) {
    const attributeSelector = createChart.axisSelector.openAttributeSelector();
    attributeSelector.filterAttribute(listCuf.attribute);
    attributeSelector.addNodeToSelection(listCuf.node);
    createChart.assertChartTypeExist('pie');

    listCuf.legends.forEach((legend, index) => {
      createChart.assertChartLegends(index, legend);
      createChart.assertChartSlicePercentage(index, listCuf.percentages[index]);
      createChart.assertChartSliceFillColor(index, listCuf.colors[index]);
    });
  }
}

function setupChartWithCufAndVerifyAxesFilters(createChart: CreateChartViewPage) {
  const attributeSelector = createChart.axisSelector.openAttributeSelector();
  attributeSelector.filterAttribute('Automatisation');
  attributeSelector.addNodeToSelection('103-8');
  createChart.assertChartTypeExist('pie');
  const chartData = [
    {
      legend: 'Cucumber',
      percentage: '33.3% (1)',
      fillColor: 'rgb(33, 217, 59)',
    },
    {
      legend: 'Robot',
      percentage: '33.3% (1)',
      fillColor: 'rgb(184, 24, 143)',
    },
    {
      legend: 'UFT',
      percentage: '33.3% (1)',
      fillColor: 'rgb(59, 196, 209)',
    },
  ];
  chartData.forEach((data, index) => {
    createChart.assertChartLegends(index, data.legend);
    createChart.assertChartSlicePercentage(index, data.percentage);
    createChart.assertChartSliceFillColor(index, data.fillColor);
  });

  const filterButton = createChart.clickFilterButton();
  cy.get(selectByDataTestElementId('input-filter')).type('Automatisation');
  filterButton.addNodeToSelection('103-8');
  const groupedMultiListElement1 = new GroupedMultiListElement();
  groupedMultiListElement1.toggleOneItem('Robot');
  groupedMultiListElement1.close();
  createChart.assertChartSlicePercentage(0, '100% (1)');
  createChart.assertChartSliceFillColor(0, 'rgb(184, 24, 143)');
}

function updateScopeAndValidateCufExclusion(
  createChart: CreateChartViewPage,
  projectPickerDialogElement: ProjectPickerDialogElement,
) {
  createChart.selectScopeCriterion(1);
  projectPickerDialogElement.toggleProject(1);
  projectPickerDialogElement.toggleProject(2);
  projectPickerDialogElement.confirm();
  const attributeSelector = createChart.axisSelector.openAttributeSelector();
  attributeSelector.filterAttribute('Text simple');
  attributeSelector.findRowId('NAME', 'Text simple').should('exist');
  assertEmptyGridForAttributes(attributeSelector, [
    'Tag',
    'Automatisation',
    'Checkbox',
    'Number',
    'ListCuf1',
    'DateCuf',
  ]);
  cy.clickOnOverlayBackdrop();

  const filterAttribute = createChart.clickFilterButton();
  filterAttribute.filterAttribute('Text simple');
  filterAttribute.findRowId('NAME', 'Text simple').should('exist');
  assertEmptyGridForAttributes(filterAttribute, [
    'Tag',
    'Automatisation',
    'Checkbox',
    'Number',
    'ListCuf1',
    'DateCuf',
  ]);
  cy.clickOnOverlayBackdrop();
}

function assertCufDisplayOnEntitiesInSelection(createChart: CreateChartViewPage) {
  const attributeSelector = createChart.axisSelector.openAttributeSelector();
  attributeSelector.filterAttribute('Text simple');
  attributeSelector.closeAllNodes();
  toggleNodeAndAssertParent(attributeSelector, "Versions d'exigence", 'REQUIREMENT_VERSION');
  toggleNodeAndAssertParent(attributeSelector, 'Cas de test', 'TEST_CASE');
  toggleNodeAndAssertParent(attributeSelector, 'Campagnes', 'CAMPAIGN');
  toggleNodeAndAssertParent(attributeSelector, 'Itérations', 'ITERATION');
  toggleNodeAndAssertParent(attributeSelector, 'Exécutions', 'EXECUTION');

  cy.clickOnOverlayBackdrop();

  const filterAttribute = createChart.clickFilterButton();
  filterAttribute.filterAttribute('Text simple');
  filterAttribute.closeAllNodes();
  toggleNodeAndAssertParent(filterAttribute, "Versions d'exigence", 'REQUIREMENT_VERSION');
  toggleNodeAndAssertParent(filterAttribute, 'Cas de test', 'TEST_CASE');
  toggleNodeAndAssertParent(filterAttribute, 'Campagnes', 'CAMPAIGN');
  toggleNodeAndAssertParent(filterAttribute, 'Itérations', 'ITERATION');
  toggleNodeAndAssertParent(filterAttribute, 'Exécutions', 'EXECUTION');

  cy.clickOnOverlayBackdrop();
}

function configureChartWithTypeAttribute(createChart: CreateChartViewPage) {
  const attributeSelector = createChart.axisSelector.openAttributeSelector();
  attributeSelector.filterAttribute('type');
  attributeSelector.addNodeToSelection(23);
  const filterButton = createChart.clickFilterButton();
  cy.get(selectByDataTestElementId('input-filter')).type('type');
  filterButton.addNodeToSelection(23);
  const groupedMultiListElement = new GroupedMultiListElement();
  groupedMultiListElement.toggleOneItem('E');
  groupedMultiListElement.close();
}

function updateScopeWithProjectAndVerifyCustomListAttributes(
  createChart: CreateChartViewPage,
  projectPickerDialogElement: ProjectPickerDialogElement,
) {
  createChart.selectScopeCriterion(1);
  projectPickerDialogElement.toggleProject(2);
  projectPickerDialogElement.toggleProject(1);
  projectPickerDialogElement.confirm();
  createChart.axisSelector.assertAttributeContain('Cas de test | Type');
  createChart.assertFilterAttribute(1, 'Type');
  createChart.assertFilterAttribute(4, 'E');
}

function createHistogramAndVerifyCufAttributesDisplay(createChart: CreateChartViewPage) {
  createChart.chartTypeSelector.selectValue('Histogramme');
  const measureAttribute = createChart.measureSelector.openAttributeSelector();
  measureAttribute.addNodeToSelection(19);
  const attributeSelector = createChart.axisSelector.openAttributeSelector();
  attributeSelector.filterAttribute('Automatisation');
  attributeSelector.addNodeToSelection('103-8');
  createChart.clickIcon('close-circle');
  createChart.assertChartTypeExist('bar');
  createChart.assertChartXAxisLabel(0, 'Cucumber');
  createChart.assertChartXAxisLabel(1, 'Robot');
  createChart.assertChartXAxisLabel(2, 'UFT');
  createChart.assertChartXAxisTitle('Automatisation');
  createChart.assertChartYAxisTitle('ID du cas de test');
}

function saveChartWithNameAndValidateConsistency(
  createChart: CreateChartViewPage,
  chartDefinitionViewPage: ChartDefinitionViewPage,
) {
  createChart.renameChart("Suivi de l'avancement de l'automatisation des tests");
  createChart.clickAddChart();
  // TODO once ticket #3067 is implemented, the following line is te be uncommented
  // createChart.assertErrorMessageIsDisplayed('La taille ne doit pas dépasser 50 caractères.');
  createChart.renameChart("Suivi de l'avancement de l'automatisation");
  createChart.saveChart();
  chartDefinitionViewPage.assertExists();
  chartDefinitionViewPage.assertNameContains("Suivi de l'avancement de l'automatisation");
}

function verifyChartConformityAndDetailsInChartDefinitionPage(
  chartDefinitionViewPage: ChartDefinitionViewPage,
) {
  chartDefinitionViewPage.assertChartTypeExist('bar');
  const barChartData = [
    { color: 'rgb(33, 217, 59)', value: '1' },
    { color: 'rgb(184, 24, 143)', value: '1' },
    { color: 'rgb(59, 196, 209)', value: '1' },
  ];

  barChartData.forEach((data, index) => {
    chartDefinitionViewPage.assertChartBarColor(index, data.color);
    chartDefinitionViewPage.assertBarValueInChart(index, data.value);
  });

  chartDefinitionViewPage.assertChartTitle("Suivi de l'avancement de l'automatisation");
  chartDefinitionViewPage.assertChartXAxisTitle('Automatisation');
  chartDefinitionViewPage.assertChartYAxisTitle('ID du cas de test');

  chartDefinitionViewPage.showInformationPanel();
  chartDefinitionViewPage.assertHasCreationDate();
  chartDefinitionViewPage.assertCreatedBy('admin');
  chartDefinitionViewPage.checkPerimeter('Project');
  assertAxesField(
    chartDefinitionViewPage,
    'measure',
    'sqtm-core-custom-report:up',
    'Mesurer',
    WorkspaceColor.TEST_CASE,
  );
  chartDefinitionViewPage.checkMeasureInfo('Cas de test | ID du cas de test - Comptage');
  assertAxesField(
    chartDefinitionViewPage,
    'axis',
    'sqtm-core-custom-report:right',
    'Par',
    WorkspaceColor.TEST_CASE,
  );
  chartDefinitionViewPage.checkAxisInfo('Cas de test | Automatisation - Agrégation');
}

function assertCustomFieldsInSelection(
  selector: any,
  fields: string[],
  assertExistence: boolean = true,
) {
  fields.forEach((field) => {
    selector.filterAttribute(field);
    if (assertExistence) {
      selector.findRowId('NAME', field).should('exist');
    } else {
      selector.assertRowIdNotExist('NAME', field);
    }
  });
}

function toggleNodeAndAssertParent(selector: any, nodeName: string, expectedParent: string) {
  selector.findRowId('NAME', nodeName).then((id) => {
    selector.getTreeNodeCell(id).toggle();
  });
  selector.findRowId('NAME', 'Text simple').then((id) => {
    selector.assertRowHasParent(id, expectedParent);
  });
  selector.closeAllNodes();
}

function assertEmptyGridForAttributes(selector: any, attributes: string[]) {
  attributes.forEach((attribute) => {
    selector.filterAttribute(attribute);
    selector.assertGridIsEmpty();
  });
}
