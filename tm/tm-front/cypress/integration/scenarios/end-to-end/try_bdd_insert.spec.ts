import { PrerequisiteBuilder } from '../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { DatabaseUtils } from '../../utils/database.utils';
import { isPostgresql } from '../../../utils/testing-context';
import { ProjectBuilder } from '../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  CredentialsBuilder,
  SCMRepositoryBuilder,
  SCMServerBuilder,
} from '../../utils/end-to-end/prerequisite/builders/server-prerequisite-builders';
import {
  ExploratoryTestCaseBuilder,
  TestCaseBuilder,
} from '../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { BindableEntity } from '../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { RequirementBuilder } from '../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import { UserBuilder } from '../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  CampaignBuilder,
  CampaignFolderBuilder,
} from '../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  addPermissionToProject,
  ApiPermissionGroup,
} from '../../utils/end-to-end/api/add-permissions';

describe('Insert data tests', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    cy.task('cleanDatabase');
    new PrerequisiteBuilder()
      .withCufs([
        new CustomFieldBuilder('CF-1', InputType.PLAIN_TEXT).withLabel('Text').withCode('Project'),
        new CustomFieldBuilder('CF-2', InputType.PLAIN_TEXT)
          .withLabel('Text2')
          .withCode('Project2'),
        new CustomFieldBuilder('CF-3', InputType.PLAIN_TEXT)
          .withLabel('Text3')
          .withCode('Project3'),
      ])
      .withServers([
        new SCMServerBuilder('SCM-Server-1', 'http://git-server1')
          .withCommitterMail('git@henix.fr')
          .withCredentials(new CredentialsBuilder())
          .withRepositories([
            new SCMRepositoryBuilder('SCM-Repository-1', 'master')
              .withRepositoryPath('/home/test/git')
              .withWorkingFolderPath('src/test'),
          ]),
      ])
      .withUsers([
        new UserBuilder('User-A')
          .withCreatedOn(new Date('2020-10-31 05:44:45'))
          .withLastConnection('2021-10-31 05:44:45'),
        new UserBuilder('User-B'),
      ])
      .withProjects([
        new ProjectBuilder('Project-1').withCufsOnProject([
          new CustomFieldOnProjectBuilder('CF-1', BindableEntity.TEST_CASE),
          new CustomFieldOnProjectBuilder('CF-3', BindableEntity.REQUIREMENT_VERSION),
          new CustomFieldOnProjectBuilder('CF-2', BindableEntity.TEST_CASE),
        ]),
        new ProjectBuilder('Project-2'),
        new ProjectBuilder('Project-3')
          .withCufsOnProject([
            new CustomFieldOnProjectBuilder('CF-1', BindableEntity.TEST_CASE),
            new CustomFieldOnProjectBuilder('CF-3', BindableEntity.REQUIREMENT_VERSION),
            new CustomFieldOnProjectBuilder('CF-2', BindableEntity.TEST_CASE),
          ])
          .withRequirementLibraryNodes([
            new RequirementBuilder('REQ-1')
              .withLastModifiedBy('User-A')
              .withLastModifiedOn(new Date('2020-10-31 05:44:45')),
          ])
          .withTestCaseLibraryNodes([
            new TestCaseBuilder('TC-1').withDescription('TC-1 description').withStatus('OBSOLETE'),
            new ExploratoryTestCaseBuilder('TC-2')
              .withDescription('TC-2 description')
              .withReference('TC-2 reference'),
          ])
          .withCampaignLibraryNodes([
            new CampaignFolderBuilder('CF-3')
              .withDescription('CF-3 description')
              .withReference('CF-3 reference')
              .withCampaignLibraryNodes([new CampaignBuilder('C-3')]),
            new CampaignBuilder('CF-4'),
          ]),
      ])
      .build();
    initPermissions();
  });
  function initPermissions() {
    addPermissionToProject(1, ApiPermissionGroup.PROJECT_MANAGER, [2]);
    addPermissionToProject(2, ApiPermissionGroup.AUTOMATED_TEST_WRITER, [3]);
    addPermissionToProject(3, ApiPermissionGroup.TEST_DESIGNER, [2]);
  }
  it('should log in with new user', () => {
    cy.logInAs('User-A', 'admin');
  });

  it('should execute select query inside test and assert result', () => {
    DatabaseUtils.executeSelectQuery(`SELECT * FROM CORE_USER WHERE login = 'User-A';`).then(
      (result) => {
        expect(result[0].login).to.equal('User-A');
      },
    );
  });

  it('should sort an array with hyphens at start or end, depending to database profile', () => {
    const arrayStartingWithHyphens = [
      '-',
      '-',
      '31/10/2020 05:44',
      '31/10/2021 02:55',
      '31/10/2022 03:44',
    ];
    const arrayEndingWithHyphens = [
      '31/10/2020 05:44',
      '31/10/2021 02:55',
      '31/10/2022 03:44',
      '-',
      '-',
    ];

    const sortedArrayAscWithHyphensAtStart = DatabaseUtils.sortHyphenStartOrEndInArrayAsc([
      '-',
      '-',
      '31/10/2020 05:44',
      '31/10/2021 02:55',
      '31/10/2022 03:44',
    ]);
    assertSortDependingToDatabase(
      sortedArrayAscWithHyphensAtStart,
      arrayEndingWithHyphens,
      arrayStartingWithHyphens,
    );

    const sortedArrayAscWithHyphensAtEnd = DatabaseUtils.sortHyphenStartOrEndInArrayAsc([
      '31/10/2020 05:44',
      '31/10/2021 02:55',
      '31/10/2022 03:44',
      '-',
      '-',
    ]);
    assertSortDependingToDatabase(
      sortedArrayAscWithHyphensAtEnd,
      arrayEndingWithHyphens,
      arrayStartingWithHyphens,
    );

    const sortedArrayDescWithHyphensAtStart = DatabaseUtils.sortHyphenStartOrEndInArrayDesc([
      '-',
      '-',
      '31/10/2020 05:44',
      '31/10/2021 02:55',
      '31/10/2022 03:44',
    ]);
    assertSortDependingToDatabase(
      sortedArrayDescWithHyphensAtStart,
      arrayStartingWithHyphens,
      arrayEndingWithHyphens,
    );

    const sortedArrayDescWithHyphensAtEnd = DatabaseUtils.sortHyphenStartOrEndInArrayDesc([
      '31/10/2020 05:44',
      '31/10/2021 02:55',
      '31/10/2022 03:44',
      '-',
      '-',
    ]);
    assertSortDependingToDatabase(
      sortedArrayDescWithHyphensAtEnd,
      arrayStartingWithHyphens,
      arrayEndingWithHyphens,
    );
  });

  function assertSortDependingToDatabase(
    sortedArray: string[],
    sortedPostgresqlArray: string[],
    sortedMariadbArray: string[],
  ) {
    isPostgresql()
      ? expect(sortedArray).to.have.ordered.members(sortedPostgresqlArray)
      : expect(sortedArray).to.have.ordered.members(sortedMariadbArray);
  }
});
