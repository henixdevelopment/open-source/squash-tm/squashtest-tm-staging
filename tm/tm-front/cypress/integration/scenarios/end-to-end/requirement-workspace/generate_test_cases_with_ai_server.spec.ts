import { DatabaseUtils } from '../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  AiServerBuilder,
  AiServerProjectBindingBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/server-prerequisite-builders';
import {
  HighLevelRequirementBuilder,
  RequirementBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  TestCaseBuilder,
  TestCaseFolderBuilder,
} from '../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { NavBarElement } from '../../../page-objects/elements/nav-bar/nav-bar.element';
import { selectNodeInAnotherNode } from '../../scenario-parts/requirement.part';
import { TestCaseTreeFolderDialogPickerElement } from '../../../page-objects/pages/requirement-workspace/dialogs/test-case-folder-picker-dialog.element';
import { VerifyingTestCaseTableElement } from '../../../page-objects/pages/requirement-workspace/panels/verifying-test-case-table.element';
import { ToolbarButtonElement } from '../../../page-objects/elements/workspace-common/toolbar.element';
import { MenuElement } from '../../../utils/menu.element';
import { TestCaseAiGenerationDialogElement } from '../../../page-objects/pages/requirement-workspace/dialogs/test-case-ai-generation-dialog.element';
import { RequirementVersionViewPage } from '../../../page-objects/pages/requirement-workspace/requirement/requirement-version-view.page';
import { TestCaseViewDetailPage } from '../../../page-objects/pages/test-case-workspace/test-case/test-case-view-detail.page';
import {
  addPermissionToProject,
  ApiPermissionGroup,
} from '../../../utils/end-to-end/api/add-permissions';

describe('Generate a test case with an ai server', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
    cy.logInAs('admin', 'admin');
  });

  it('F03-UC02.CT24 - should generate a test case with an ai server and save it', () => {
    cy.log('Step 1');
    const requirementWorkspace = NavBarElement.navigateToRequirementWorkspace();
    const toolbarButton = new ToolbarButtonElement(
      'sqtm-app-requirement-version-view',
      'action-menu-button',
    );
    const toolbarButtonHighLevel = new ToolbarButtonElement(
      'sqtm-app-high-level-requirement-view',
      'action-menu-button',
    );
    const actionMenu = new MenuElement('action-menu');
    const testCaseAiGenerationDialogElement = new TestCaseAiGenerationDialogElement();
    const testCaseWorkspacePage = new TestCaseViewDetailPage();
    const requirementVersionViewPage = new RequirementVersionViewPage(null);
    const testCaseTreeFolderDialogPicker = new TestCaseTreeFolderDialogPickerElement();
    selectNodeInAnotherNode(requirementWorkspace, 'A', 'NotHighLevel');
    toolbarButton.click();
    actionMenu.item('ai-test-case-generation').click();
    generateTCWithAi(
      testCaseAiGenerationDialogElement,
      testCaseTreeFolderDialogPicker,
      'TestCaseFolder-1',
      'select-test-caseAjouter un article au panier',
    );

    cy.log('Step 2');
    const verifyingTestCaseTableElement = new VerifyingTestCaseTableElement();
    assertTestCaseIsCreated(
      verifyingTestCaseTableElement,
      requirementVersionViewPage,
      testCaseWorkspacePage,
      'Ajouter un article au panier',
    );
    testCaseWorkspacePage.navigateBack();

    cy.log('Step 3');
    selectNodeInAnotherNode(requirementWorkspace, 'A', 'Obviously high level');
    toolbarButtonHighLevel.click();
    actionMenu.item('ai-test-case-generation').click();
    generateTCWithAi(
      testCaseAiGenerationDialogElement,
      testCaseTreeFolderDialogPicker,
      'TestCaseLibrary-1',
      'select-test-caseSupprimer un article du panier',
    );

    cy.log('Step 4');
    assertTestCaseIsCreated(
      verifyingTestCaseTableElement,
      requirementVersionViewPage,
      testCaseWorkspacePage,
      'Supprimer un article du panier',
    );
  });
});

function initTestData() {
  const requirement =
    'Le site de vente doit permettre d ajouter et de supprimer des articles dans son panier. Il doit également inclure la possibilité de payer par carte bleue ou virement bancaire.';
  return new PrerequisiteBuilder()
    .withUsers([
      new UserBuilder('Iron Man').withFirstName('Tony').withLastName('Stark'),
      new UserBuilder('Hulk').withFirstName('Bruce').withLastName('Banner'),
    ])
    .withServers([
      new AiServerBuilder(
        'toto',
        'https://api.together.xyz/v1/chat/completions',
        'choices[0].message.content',
      ),
    ])
    .withProjects([
      new ProjectBuilder('A')
        .withAiServerOnProject([new AiServerProjectBindingBuilder('toto')])
        .withRequirementLibraryNodes([
          new RequirementBuilder('NotHighLevel')
            .withCriticality('MINOR')
            .withDescription(requirement),
          new HighLevelRequirementBuilder('Obviously high level')
            .withCriticality('MAJOR')
            .withDescription(requirement),
        ]),
      new ProjectBuilder('B').withTestCaseLibraryNodes([
        new TestCaseFolderBuilder('Dossier1').withTestCaseLibraryNodes([
          new TestCaseBuilder('Classic'),
        ]),
      ]),
    ])
    .build();
}
function initPermissions() {
  addPermissionToProject(1, ApiPermissionGroup.PROJECT_MANAGER, [2]);
  addPermissionToProject(2, ApiPermissionGroup.PROJECT_MANAGER, [3]);
}
function generateTCWithAi(
  testCaseAiGenerationDialogElement: TestCaseAiGenerationDialogElement,
  testCaseTreeFolderDialogPicker: TestCaseTreeFolderDialogPickerElement,
  destination: string,
  title: string,
) {
  testCaseAiGenerationDialogElement.pickDestination();
  testCaseTreeFolderDialogPicker.assertExist();
  testCaseTreeFolderDialogPicker.openTree();
  testCaseTreeFolderDialogPicker.selectDestination(destination);
  testCaseTreeFolderDialogPicker.confirm();
  testCaseAiGenerationDialogElement.generateTestCase();
  cy.wait(8000);
  testCaseAiGenerationDialogElement.selectGeneratedTestCase(title);
  testCaseAiGenerationDialogElement.confirm();
}

function assertTestCaseIsCreated(
  verifyingTestCaseTableElement: VerifyingTestCaseTableElement,
  requirementVersionViewPage: RequirementVersionViewPage,
  testCaseWorkspacePage: TestCaseViewDetailPage,
  title: string,
) {
  verifyingTestCaseTableElement.isNotEmpty();
  requirementVersionViewPage.assertIssuesAnchorsIsActive();
  verifyingTestCaseTableElement.clickName(title);
  testCaseWorkspacePage.draftedByAiCapsule.assertExists();
}
