import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { AdminWorkspaceUsersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import { AdminWorkspaceItem } from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import { DatabaseUtils } from '../../../../utils/database.utils';
import { RemoveUserDialogElement } from '../../../../page-objects/pages/administration-workspace/dialogs/remove-user-dialog.element';
import { RemoveTeamDialogElement } from '../../../../page-objects/pages/administration-workspace/dialogs/remove-team-dialog.element';
import { AdminWorkspaceTeamsPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-teams.page';
import {
  TeamBuilder,
  UserBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';

describe('Dissociate teams from table', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    cy.logInAs('admin', 'admin');
  });

  it('F01-UC0102.CT04 - should delete teams', () => {
    cy.log('Step 1');
    const adminWorkspaceUsersPage = NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>(
      AdminWorkspaceItem.USERS,
    );
    const teamPage = adminWorkspaceUsersPage.goToTeamAnchor();
    teamPage.assertExists();
    teamPage.grid.assertRowCount(4);
    teamPage.grid.assertGridColumnContains('name', [
      'Griffondor',
      'Poufsouffle',
      'Serdaigle',
      'Serpentard',
    ]);

    cy.log('Step 2');
    const confirmDeleteDialog = new RemoveTeamDialogElement();
    deleteTeamFromGridWithDeleteDialogConformity(teamPage, confirmDeleteDialog, ['Serpentard']);
    teamPage.grid.assertRowIdNotExist('name', 'Serpentard');

    cy.log('Step 3');
    deleteTeamFromGridWithDeleteDialogConformity(teamPage, confirmDeleteDialog, [
      'Griffondor',
      'Serdaigle',
    ]);
    teamPage.grid.assertRowIdNotExist('name', 'Griffondor');
    teamPage.grid.assertRowIdNotExist('name', 'Serdaigle');

    cy.log('Step 4');
    adminWorkspaceUsersPage.anchors.clickLink('manage');
    adminWorkspaceUsersPage.grid.assertGridColumnContains('login', ['admin', 'Idiot', 'Voyou']);
  });
});

function initTestData() {
  const teamNames: string[] = ['Poufsouffle', 'Griffondor', 'Serpentard', 'Serdaigle'];
  return new PrerequisiteBuilder()
    .withUsers([
      new UserBuilder('Voyou').withFirstName('Harry').withLastName('Potter'),
      new UserBuilder('Idiot').withFirstName('Ron').withLastName('Weasley'),
    ])
    .withTeams(
      teamNames.map((teamName) => new TeamBuilder(teamName).withUserLogins(['Voyou', 'Idiot'])),
    )
    .build();
}
function deleteTeamFromGridWithDeleteDialogConformity(
  teamPage: AdminWorkspaceTeamsPage,
  confirmDeleteDialog: RemoveUserDialogElement,
  teams: string[],
) {
  teamPage.grid.selectRowsWithMatchingCellContent('name', teams);
  teamPage.openDeleteDialog();
  confirmDeleteDialog.assertExists();
  confirmDeleteDialog.assertExclamationCircleIconExists();
  confirmDeleteDialog.assertTitleHasText('Supprimer les équipes');
  confirmDeleteDialog.assertHasMessage(
    'Les équipes sélectionnées seront supprimées, cette action ne peut être annulée.\nConfirmez-vous la suppression des équipes ?',
  );
  confirmDeleteDialog.assertConfirmButtonExists();
  confirmDeleteDialog.assertCancelButtonExists();
  confirmDeleteDialog.cancel();

  teamPage.openDeleteDialog();
  confirmDeleteDialog.assertExists();
  confirmDeleteDialog.clickOnConfirmButton();
  confirmDeleteDialog.assertNotExist();
}
