import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { AdminWorkspaceUsersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { TeamViewPage } from '../../../../page-objects/pages/administration-workspace/team-view/team-view.page';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import {
  AdminWorkspaceItem,
  NavBarAdminElement,
} from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import { ProjectViewPage } from '../../../../page-objects/pages/administration-workspace/project-view/project-view.page';
import { AlertDialogElement } from '../../../../page-objects/elements/dialog/alert-dialog.element';
import { RemoveUserDialogElement } from '../../../../page-objects/pages/administration-workspace/dialogs/remove-user-dialog.element';
import {
  TeamBuilder,
  UserBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { MilestoneBuilder } from '../../../../utils/end-to-end/prerequisite/builders/milestone-prerequisite-builders';
import {
  addPermissionToProject,
  ApiPermissionGroup,
} from '../../../../utils/end-to-end/api/add-permissions';

describe('Delete multiple users', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC0101.CT05 - should delete multiple users with trash icon', () => {
    cy.log('Step 1');
    cy.logInAs('adminTest', 'admin');
    const adminWorkspaceUsersPage = NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>(
      AdminWorkspaceItem.USERS,
    );

    cy.log('Step 2');
    const removeUserDialogElement = new RemoveUserDialogElement();
    deleteUserSimpleWay(adminWorkspaceUsersPage, removeUserDialogElement);

    cy.log('Step 3');
    deleteTwoUsers(adminWorkspaceUsersPage, removeUserDialogElement);

    cy.log('Step 4');
    const alertDialogElement = new AlertDialogElement();
    checkMilestoneOwnerDeletionFailure(
      adminWorkspaceUsersPage,
      removeUserDialogElement,
      alertDialogElement,
    );

    cy.log('Step 5');
    checkTwoUsersDeletionFailure(
      adminWorkspaceUsersPage,
      removeUserDialogElement,
      alertDialogElement,
    );

    cy.log('Step 6');
    const teamView = new TeamViewPage();
    checkTeamMemberTableIsUpToDate(teamView, adminWorkspaceUsersPage);

    const projectViewPage = new ProjectViewPage();
    checkPermissionTableIsUpToDate(projectViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([
      new UserBuilder('User00').withFirstName('chief').withLastName('Boss'),
      new UserBuilder('User01').withFirstName('Gérard').withLastName('Mansoif'),
      new UserBuilder('User02').withFirstName('Anna').withLastName('Bolisant'),
      new UserBuilder('User03').withFirstName('Bibou').withLastName('Fortoso'),
      new UserBuilder('User04').withFirstName('Loulou').withLastName('Hederson'),
      new UserBuilder('adminTest')
        .withFirstName('Adepte')
        .withLastName('Bolisant')
        .withGroupName('Admin'),
    ])
    .withMilestones([
      new MilestoneBuilder('Milestione1', '2019-10-31 14:44:45').withUserLogin('User00'),
    ])
    .withTeams([
      new TeamBuilder('monequipe').withUserLogins(['User01', 'User02', 'User03', 'User04']),
    ])
    .withProjects([new ProjectBuilder('AZERTY')])
    .build();
}
function initPermissions() {
  addPermissionToProject(1, ApiPermissionGroup.PROJECT_MANAGER, [2]);
  addPermissionToProject(1, ApiPermissionGroup.TEST_EDITOR, [3]);
  addPermissionToProject(1, ApiPermissionGroup.TEST_DESIGNER, [4]);
  addPermissionToProject(1, ApiPermissionGroup.AUTOMATED_TEST_WRITER, [5]);
  addPermissionToProject(1, ApiPermissionGroup.ADVANCED_TESTER, [6]);
}
function tryToDeleteUserWithMilestone(
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
  removeUserDialogElement: RemoveUserDialogElement,
  alertDialogElement: AlertDialogElement,
) {
  adminWorkspaceUsersPage.openDeleteDialog();
  removeUserDialogElement.assertExists();
  removeUserDialogElement.clickOnConfirmButton();
  removeUserDialogElement.assertNotExist();
  alertDialogElement.assertExists();
  alertDialogElement.assertTitleHasText('Erreur');
  alertDialogElement.assertHasMessage(
    "L'utilisateur ou l'un des utilisateurs sélectionné est propriétaire d'au moins un jalon. Veuillez réaffecter le/les jalons à d'autres utilisateurs.",
  );
  alertDialogElement.assertExclamationCircleIconExists();
  alertDialogElement.close();
  alertDialogElement.assertNotExist();
}
function deleteUserSimpleWay(
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
  removeUserDialogElement: RemoveUserDialogElement,
) {
  adminWorkspaceUsersPage.grid.selectRowsWithMatchingCellContent('login', ['User02']);
  adminWorkspaceUsersPage.openDeleteDialog();
  removeUserDialogElement.assertExists();
  removeUserDialogElement.assertExclamationCircleIconExists();
  removeUserDialogElement.assertTitleHasText('Supprimer les utilisateurs');
  removeUserDialogElement.assertHasMessage(
    'Les utilisateurs sélectionnés seront supprimés, cette action ne peut être annulée. Confirmez-vous la suppression des utilisateurs ?',
  );
  removeUserDialogElement.assertConfirmButtonExists();
  removeUserDialogElement.assertCancelButtonExists();
  removeUserDialogElement.assertDeleteButtonIsRed();
  removeUserDialogElement.clickOnConfirmButton();
  adminWorkspaceUsersPage.grid.assertRowCount(6);
  adminWorkspaceUsersPage.grid.assertRowIdNotExist('login', 'User02');
}

function deleteTwoUsers(
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
  removeUserDialogElement: RemoveUserDialogElement,
) {
  adminWorkspaceUsersPage.grid.selectRowsWithMatchingCellContent('login', ['User01', 'User04']);
  adminWorkspaceUsersPage.openDeleteDialog();
  removeUserDialogElement.assertExists();
  removeUserDialogElement.cancel();
  removeUserDialogElement.assertNotExist();
  adminWorkspaceUsersPage.openDeleteDialog();
  removeUserDialogElement.assertExists();
  removeUserDialogElement.clickOnConfirmButton();
  removeUserDialogElement.assertNotExist();
  adminWorkspaceUsersPage.grid.assertRowCount(4);
  adminWorkspaceUsersPage.grid.assertRowIdNotExist('login', 'User01');
  adminWorkspaceUsersPage.grid.assertRowIdNotExist('login', 'User04');
}

function checkMilestoneOwnerDeletionFailure(
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
  removeUserDialogElement: RemoveUserDialogElement,
  alertDialogElement: AlertDialogElement,
) {
  adminWorkspaceUsersPage.grid.selectRowsWithMatchingCellContent('login', ['User00']);
  tryToDeleteUserWithMilestone(
    adminWorkspaceUsersPage,
    removeUserDialogElement,
    alertDialogElement,
  );
  adminWorkspaceUsersPage.grid.findRowId('login', 'User00');
}

function checkTwoUsersDeletionFailure(
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
  removeUserDialogElement: RemoveUserDialogElement,
  alertDialogElement: AlertDialogElement,
) {
  adminWorkspaceUsersPage.grid.selectRowsWithMatchingCellContent('login', ['User00', 'User03']);
  tryToDeleteUserWithMilestone(
    adminWorkspaceUsersPage,
    removeUserDialogElement,
    alertDialogElement,
  );
  adminWorkspaceUsersPage.grid.findRowId('login', 'User00');
  adminWorkspaceUsersPage.grid.findRowId('login', 'User03');
}

function checkTeamMemberTableIsUpToDate(
  teamView: TeamViewPage,
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
) {
  const teamPage = adminWorkspaceUsersPage.goToTeamAnchor();
  teamPage.grid.selectRowWithMatchingCellContent('name', 'monequipe');
  teamView.teamMembersPanel.grid.assertRowCount(1);
  teamView.teamMembersPanel.grid.assertRowIdNotExist('fullName', 'User01');
  teamView.teamMembersPanel.grid.assertRowIdNotExist('fullName', 'User02');
  teamView.teamMembersPanel.grid.assertRowIdNotExist('fullName', 'User04');
}
function checkPermissionTableIsUpToDate(projectViewPage: ProjectViewPage) {
  const projectPage = NavBarAdminElement.navigateToAdministration(AdminWorkspaceItem.PROJECTS);
  projectPage.grid.selectRowWithMatchingCellContent('name', 'AZERTY');
  projectViewPage.permissionsPanel.grid.assertRowCount(2);
  projectViewPage.permissionsPanel.grid.assertRowIdNotExist('partyName', 'User01');
  projectViewPage.permissionsPanel.grid.assertRowIdNotExist('partyName', 'User02');
  projectViewPage.permissionsPanel.grid.assertRowIdNotExist('partyName', 'User04');
}
