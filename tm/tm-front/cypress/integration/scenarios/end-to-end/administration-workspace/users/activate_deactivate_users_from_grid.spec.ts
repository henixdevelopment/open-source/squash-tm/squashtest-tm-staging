import { UserMultiEditDialogElement } from '../../../../page-objects/pages/administration-workspace/dialogs/user-multi-edit-dialog.element';
import { LoginPage } from '../../../../page-objects/pages/login/login-page';
import { UserViewPage } from '../../../../page-objects/pages/administration-workspace/user-view/user-view.page';
import { DatabaseUtils } from '../../../../utils/database.utils';
import {
  AdminWorkspaceItem,
  NavBarAdminElement,
} from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import { AlertDialogElement } from '../../../../page-objects/elements/dialog/alert-dialog.element';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { AdminWorkspaceUsersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import { changeStatusToInactiveInGridAndCheck } from '../../../scenario-parts/user-status.part';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { TestCaseBuilder } from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CampaignBuilder,
  CampaignTestPlanItemBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
  TestSuiteBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  TeamBuilder,
  UserBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addPermissionToProject,
  ApiPermissionGroup,
} from '../../../../utils/end-to-end/api/add-permissions';

describe('Activate/deactivate users from grid', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC0101.CT03b - should activate/deactivate several users from grid', () => {
    cy.log('Step 1');
    cy.logInAs('lclure', 'admin');
    const adminWorkspaceUsersPage = NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>(
      AdminWorkspaceItem.USERS,
    );

    cy.log('Step 2');
    checkUserStatusInGridAndSelectUsers(adminWorkspaceUsersPage);
    const multiEditDialog = adminWorkspaceUsersPage.openMultiEditDialog();
    const userView = new UserViewPage();
    activateAndDeactivateUsers(multiEditDialog, adminWorkspaceUsersPage, userView);

    cy.log('Step 3');
    activateAndDeactivateUser(multiEditDialog, adminWorkspaceUsersPage, userView);

    cy.log('Step 4');
    checkCurrentUserCannotBeDeactivated(multiEditDialog, adminWorkspaceUsersPage);

    cy.log('Step 5');
    checkCurrentUserCanBeActivated(multiEditDialog, adminWorkspaceUsersPage, userView);

    cy.log('Step 6');
    checkDeactivatedUserCannotLog();
  });

  function initTestData() {
    return new PrerequisiteBuilder()
      .withUsers([
        new UserBuilder('gmansoif').withFirstName('Gérard').withLastName('Mansoif'),
        new UserBuilder('abolisant').withFirstName('Anna').withLastName('Bolisant'),
        new UserBuilder('ecoptere').withFirstName('Élie').withLastName('Coptère'),
        new UserBuilder('jcelaire').withFirstName('Jacques').withLastName('Célaire'),
        new UserBuilder('bhaba').withFirstName('Barth').withLastName('Haba').withActive(false),
        new UserBuilder('lclure')
          .withFirstName('Lara')
          .withLastName('Clure')
          .withGroupName('Admin'),
      ])
      .withTeams([
        new TeamBuilder('monequipe').withUserLogins(['gmansoif']),
        new TeamBuilder('secondequipe'),
      ])
      .withProjects([
        new ProjectBuilder('AZERTY')
          .withTestCaseLibraryNodes([
            new TestCaseBuilder('TC 001'),
            new TestCaseBuilder('TC 002'),
            new TestCaseBuilder('TC 003'),
          ])
          .withCampaignLibraryNodes([
            new CampaignBuilder('Campaign 001')
              .withTestPlanItems([
                new CampaignTestPlanItemBuilder('TC 001'),
                new CampaignTestPlanItemBuilder('TC 002').withUserLogin('gmansoif'),
                new CampaignTestPlanItemBuilder('TC 003'),
              ])
              .withIterations([
                new IterationBuilder('Iteration 001')
                  .withTestPlanItems([
                    new IterationTestPlanItemBuilder('TC 001'),
                    new IterationTestPlanItemBuilder('TC 002').withUserLogin('gmansoif'),
                    new IterationTestPlanItemBuilder('TC 003'),
                  ])
                  .withTestSuites([
                    new TestSuiteBuilder('Suite 001').withTestCaseNames([
                      'TC 001',
                      'TC 002',
                      'TC 003',
                    ]),
                  ]),
              ]),
          ]),
        new ProjectBuilder('QSDFGH'),
      ])
      .build();
  }
});
function initPermissions() {
  addPermissionToProject(1, ApiPermissionGroup.TEST_EDITOR, [2]);
}
function checkStatus(
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
  login: string,
  active: string,
) {
  adminWorkspaceUsersPage.grid.findRowId('login', login).then((userID) => {
    adminWorkspaceUsersPage.grid
      .getCell(userID, 'active')
      .iconRenderer()
      .assertContainsIcon(active);
  });
}

function checkUserStatusInGridAndSelectUsers(adminWorkspaceUsersPage: AdminWorkspaceUsersPage) {
  checkStatus(adminWorkspaceUsersPage, 'abolisant', 'active');
  checkStatus(adminWorkspaceUsersPage, 'jcelaire', 'active');
  checkStatus(adminWorkspaceUsersPage, 'bhaba', 'inactive');
  adminWorkspaceUsersPage.grid.selectRowsWithMatchingCellContent('login', [
    'abolisant',
    'jcelaire',
    'bhaba',
  ]);
}

function activateAndDeactivateUsers(
  multiEditDialog: UserMultiEditDialogElement,
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
  userView: UserViewPage,
) {
  multiEditDialog.assertExists();
  changeByModifyingTheAttribute(multiEditDialog, 'Désactiver les utilisateurs');
  checkStatus(adminWorkspaceUsersPage, 'abolisant', 'inactive');
  checkStatus(adminWorkspaceUsersPage, 'jcelaire', 'inactive');
  checkStatus(adminWorkspaceUsersPage, 'bhaba', 'inactive');

  selectRowAndCheckActiveSwitch(adminWorkspaceUsersPage, userView, 'abolisant', false);
  selectRowAndCheckActiveSwitch(adminWorkspaceUsersPage, userView, 'jcelaire', false);
  selectRowAndCheckActiveSwitch(adminWorkspaceUsersPage, userView, 'bhaba', false);

  adminWorkspaceUsersPage.grid.selectRowsWithMatchingCellContent('login', [
    'abolisant',
    'bhaba',
    'jcelaire',
  ]);
  adminWorkspaceUsersPage.openMultiEditDialog();
  multiEditDialog.assertExists();
  changeByModifyingTheAttribute(multiEditDialog, 'Activer les utilisateurs');

  checkStatus(adminWorkspaceUsersPage, 'jcelaire', 'active');
  checkStatus(adminWorkspaceUsersPage, 'bhaba', 'active');
  checkStatus(adminWorkspaceUsersPage, 'abolisant', 'active');

  selectRowAndCheckActiveSwitch(adminWorkspaceUsersPage, userView, 'abolisant', true);
  selectRowAndCheckActiveSwitch(adminWorkspaceUsersPage, userView, 'jcelaire', true);
  selectRowAndCheckActiveSwitch(adminWorkspaceUsersPage, userView, 'bhaba', true);
}

function activateAndDeactivateUser(
  multiEditDialog: UserMultiEditDialogElement,
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
  userView: UserViewPage,
) {
  selectRowAndOpenMultiDialog(adminWorkspaceUsersPage, 'ecoptere', multiEditDialog);
  changeByModifyingTheAttribute(multiEditDialog, 'Désactiver les utilisateurs');
  selectRowAndCheckActiveSwitch(adminWorkspaceUsersPage, userView, 'ecoptere', false);
  userView.close();
  userView.assertNotExist();

  selectRowsAndOpenMultiDialog(adminWorkspaceUsersPage, ['ecoptere'], multiEditDialog);
  changeByModifyingTheAttribute(multiEditDialog, 'Activer les utilisateurs');
  selectRowAndCheckActiveSwitch(adminWorkspaceUsersPage, userView, 'ecoptere', true);
}

function checkCurrentUserCannotBeDeactivated(
  multiEditDialog: UserMultiEditDialogElement,
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
) {
  selectRowsAndOpenMultiDialog(adminWorkspaceUsersPage, ['lclure'], multiEditDialog);
  multiEditDialog.stateSelect.assertExists();
  multiEditDialog.stateSelect.check();
  multiEditDialog.stateSelect.selectValue('Désactiver les utilisateurs');
  multiEditDialog.clickOnConfirmButton();
  const alertDialog = new AlertDialogElement();
  alertDialog.assertExists();
  alertDialog.assertTitleHasText('Erreur');
  alertDialog.assertHasMessage("L'un des utilisateurs sélectionnés est l'utilisateur courant.");
  alertDialog.close();
  multiEditDialog.clickOnCancelButton();
}

function checkCurrentUserCanBeActivated(
  multiEditDialog: UserMultiEditDialogElement,
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
  userView: UserViewPage,
) {
  changeStatusToInactiveInGridAndCheck(adminWorkspaceUsersPage, userView, 'ecoptere');
  selectRowsAndOpenMultiDialog(adminWorkspaceUsersPage, ['ecoptere', 'lclure'], multiEditDialog);
  changeByModifyingTheAttribute(multiEditDialog, 'Activer les utilisateurs');
  checkStatus(adminWorkspaceUsersPage, 'lclure', 'active');
  checkStatus(adminWorkspaceUsersPage, 'ecoptere', 'active');
  adminWorkspaceUsersPage.grid.assertSpinnerIsNotPresent();
  changeStatusToInactiveInGridAndCheck(adminWorkspaceUsersPage, userView, 'gmansoif');
}

function checkDeactivatedUserCannotLog() {
  NavBarAdminElement.close();
  NavBarElement.logout();
  const loginPage = LoginPage.navigateTo();
  loginPage.assertExists();
  loginPage.assertLoginFails('gmansoif', 'admin');
  loginPage.assertLoginFailedWarningIsVisible();
}

function changeByModifyingTheAttribute(multiEditDialog: UserMultiEditDialogElement, value: string) {
  multiEditDialog.stateSelect.assertExists();
  multiEditDialog.stateSelect.check();
  multiEditDialog.stateSelect.selectValue(value);
  multiEditDialog.clickOnConfirmButton();
  multiEditDialog.assertNotExist();
}

function selectRowAndOpenMultiDialog(
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
  login: string,
  multiEditDialog: UserMultiEditDialogElement,
) {
  adminWorkspaceUsersPage.grid.selectRowWithMatchingCellContent('login', login);
  adminWorkspaceUsersPage.openMultiEditDialog();
  multiEditDialog.assertExists();
}

function selectRowsAndOpenMultiDialog(
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
  login: string[],
  multiEditDialog: UserMultiEditDialogElement,
) {
  adminWorkspaceUsersPage.grid.selectRowsWithMatchingCellContent('login', login);
  adminWorkspaceUsersPage.openMultiEditDialog();
  multiEditDialog.assertExists();
}

function selectRowAndCheckActiveSwitch(
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
  userView: UserViewPage,
  login: string,
  checkValue: boolean,
) {
  adminWorkspaceUsersPage.grid.assertSpinnerIsNotPresent();
  adminWorkspaceUsersPage.selectRowWithMatchingCellContent('login', login);
  userView.assertExists();
  userView.activeSwitch.checkValue(checkValue);
}
