import { PrerequisiteBuilder } from 'cypress/integration/utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { AdminWorkspaceItem } from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import { AdminWorkspaceUsersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import { UserViewPage } from '../../../../page-objects/pages/administration-workspace/user-view/user-view.page';
import { DatabaseUtils } from '../../../../utils/database.utils';
import { RemoveProjectDialog } from '../../../../page-objects/pages/administration-workspace/dialogs/remove-project-dialog.element';
import { selectByDataTestDialogButtonId } from '../../../../utils/basic-selectors';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { TeamViewPage } from '../../../../page-objects/pages/administration-workspace/team-view/team-view.page';
import { GridFooterElement } from '../../../../page-objects/elements/grid/grid-footer.element';
import { UserTeamsPanelElement } from '../../../../page-objects/pages/administration-workspace/user-view/panels/user-teams-panel.element';
import {
  TeamBuilder,
  UserBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';

describe('Dissociate team from table', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    cy.logInAs('admin', 'admin');
  });

  it('F01-UC0102.CT13 - should dissociate team', () => {
    cy.log('Step 1');
    const adminWorkspaceUsersPage = NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>(
      AdminWorkspaceItem.USERS,
    );
    adminWorkspaceUsersPage.selectUserByLogin('Sauron');

    const userViewPage = new UserViewPage();
    userViewPage.assertExists();

    cy.log('Step 2');
    userViewPage.assertAnchorTeamCount(11);
    userViewPage.anchors.clickLink('teams');

    const teamPanel = userViewPage.teamsPanel;
    teamPanel.assertIsVisible();

    const gridFooter = teamPanel.grid.gridFooter;
    checkPaginationAndOpenPupUp(gridFooter, teamPanel);

    const removeProjectDialog = new RemoveProjectDialog();

    cy.log('Step 3');
    controlPopUpConformity(removeProjectDialog);

    cy.log('Step 4');
    selectIdThenDeleteAssociation(teamPanel, removeProjectDialog, gridFooter);

    cy.log('Step 5');
    const teamPage = adminWorkspaceUsersPage.goToTeamAnchor();
    teamPage.selectRowWithMatchingCellContent('name', 'Theoden');
    userViewPage.anchors.clickLink('authorisations');

    const teamViewAuthorisationsGrid = new TeamViewPage().teamAuthorisationsPanel.grid;
    teamViewAuthorisationsGrid.assertExists();
    teamViewAuthorisationsGrid.assertGridIsEmpty();
  });
});

function initTestData() {
  const teamNames: string[] = [
    'Aragorn',
    'Gimli',
    'Legolas',
    'Arwenn',
    'SamLeGoat',
    'FrodonLaFraude',
    'Merry',
    'Pippin',
    'Gandalf',
    'Boromir',
    'Theoden',
  ];
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('Sauron').withFirstName('Sauron').withLastName('Sauron')])
    .withTeams(teamNames.map((teamName) => new TeamBuilder(teamName).withUserLogins(['Sauron'])))
    .build();
}

function controlPopUpConformity(removeProjectDialog: RemoveProjectDialog) {
  removeProjectDialog.assertExists();
  removeProjectDialog.assertTitleHasText("Supprimer l'association");
  removeProjectDialog.assertHasMessage(
    "L'équipe sélectionnée ne sera plus associée à l'utilisateur. Confirmez-vous la suppression de l'association?",
  );
  removeProjectDialog.assertExclamationCircleIconExists();
  removeProjectDialog.assertCancelButtonExists();
  removeProjectDialog.assertConfirmButtonExists();
  removeProjectDialog.checkButtonClass(selectByDataTestDialogButtonId('confirm'), 'danger-icon');
  removeProjectDialog.cancel();
}

function checkPaginationAndOpenPupUp(
  gridFooter: GridFooterElement,
  teamPanel: UserTeamsPanelElement,
) {
  gridFooter.checkPaginationDisplay('1 - 10 / 11');
  gridFooter.checkPagination('1/2');
  gridFooter.assertPaginationButtonsExist();
  teamPanel.grid.findRowId('name', 'Gandalf').then((userID) => {
    teamPanel.grid.getCell(userID, 'delete').iconRenderer().click();
  });
}

function selectIdThenDeleteAssociation(
  teamPanel: UserTeamsPanelElement,
  removeProjectDialog: RemoveProjectDialog,
  gridFooter: GridFooterElement,
) {
  gridFooter.goToNextPage();
  teamPanel.grid.findRowId('name', 'Theoden').then((userID) => {
    teamPanel.grid.getCell(userID, 'delete').iconRenderer().click();
  });
  removeProjectDialog.assertExists();
  removeProjectDialog.clickOnConfirmButton();
  removeProjectDialog.assertNotExist();
  teamPanel.grid.assertRowCount(10);
  gridFooter.checkPaginationDoesntExist();
}
