import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { AdminWorkspaceUsersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import { AdministrationWorkspacePage } from '../../../../page-objects/pages/administration-workspace/administration-workspace.page';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { TeamViewPage } from '../../../../page-objects/pages/administration-workspace/team-view/team-view.page';
import { LoginPage } from '../../../../page-objects/pages/login/login-page';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import {
  AdminWorkspaceItem,
  NavBarAdminElement,
} from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import { ProjectViewPage } from '../../../../page-objects/pages/administration-workspace/project-view/project-view.page';
import { AlertDialogElement } from '../../../../page-objects/elements/dialog/alert-dialog.element';
import { RemoveUserDialogElement } from '../../../../page-objects/pages/administration-workspace/dialogs/remove-user-dialog.element';
import {
  TeamBuilder,
  UserBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { MilestoneBuilder } from '../../../../utils/end-to-end/prerequisite/builders/milestone-prerequisite-builders';
import {
  addPermissionToProject,
  ApiPermissionGroup,
} from '../../../../utils/end-to-end/api/add-permissions';

describe('Delete simple users', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC0101.CT04 - should delete a user from table', () => {
    cy.log('Step 1');
    cy.logInAs('adminTest', 'admin');
    const adminWorkspaceUsersPage = NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>(
      AdminWorkspaceItem.USERS,
    );

    cy.log('Step 2');
    const confirmDeleteDialog = new RemoveUserDialogElement();
    deleteUserFromGridWithDeleteDialogConformity(adminWorkspaceUsersPage, confirmDeleteDialog);

    cy.log('Step 3');
    const adminWorkspaceProjectsPage = NavBarAdminElement.navigateToAdministration(
      AdminWorkspaceItem.PROJECTS,
    );
    const projectView = new ProjectViewPage();
    const teamView = new TeamViewPage();
    assertUserIsDeleted(adminWorkspaceProjectsPage, projectView, teamView, adminWorkspaceUsersPage);

    cy.log('Step 4');
    const alertDialogElement = new AlertDialogElement();
    assertDeletionIsImpossible(adminWorkspaceUsersPage, alertDialogElement, confirmDeleteDialog);

    cy.log('Step 5');
    checkLoginPossibilityAfterDeletion();
  });

  function initTestData() {
    return new PrerequisiteBuilder()
      .withUsers([
        new UserBuilder('UserA').withFirstName('Gérard').withLastName('Mansoif'),
        new UserBuilder('UserB').withFirstName('Anna').withLastName('Bolisant'),
        new UserBuilder('adminTest')
          .withFirstName('Adepte')
          .withLastName('Bolisant')
          .withGroupName('Admin'),
      ])
      .withMilestones([
        new MilestoneBuilder('Milestione1', '2019-10-31 14:44:45').withUserLogin('UserB'),
      ])
      .withTeams([new TeamBuilder('monequipe').withUserLogins(['UserA'])])
      .withProjects([new ProjectBuilder('AZERTY')])
      .build();
  }
});
function initPermissions() {
  addPermissionToProject(1, ApiPermissionGroup.TEST_EDITOR, [2]);
}
function deleteUserFromGridWithDeleteDialogConformity(
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
  confirmDeleteDialog: RemoveUserDialogElement,
) {
  adminWorkspaceUsersPage.grid.findRowId('login', 'UserA').then((userID) => {
    adminWorkspaceUsersPage.grid.getCell(userID, 'delete', 'rightViewport').iconRenderer().click();
  });

  confirmDeleteDialog.assertExists();
  confirmDeleteDialog.assertExclamationCircleIconExists();
  confirmDeleteDialog.assertTitleHasText("Supprimer l'utilisateur");
  confirmDeleteDialog.assertHasMessage(
    "L'utilisateur sera supprimé, cette action ne peut être annulée. Confirmez-vous la suppression de l'utilisateur ?",
  );
  confirmDeleteDialog.assertConfirmButtonExists();
  confirmDeleteDialog.assertCancelButtonExists();
  confirmDeleteDialog.cancel();

  adminWorkspaceUsersPage.grid.findRowId('login', 'UserA').then((userID) => {
    adminWorkspaceUsersPage.grid.getCell(userID, 'delete', 'rightViewport').iconRenderer().click();
  });
  confirmDeleteDialog.assertExists();
  confirmDeleteDialog.clickOnConfirmButton();
  confirmDeleteDialog.assertNotExist();

  adminWorkspaceUsersPage.grid.assertRowIdNotExist('login', 'UserA');
}

function assertUserIsDeleted(
  adminWorkspaceProjectsPage: AdministrationWorkspacePage,
  projectView: ProjectViewPage,
  teamView: TeamViewPage,
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
) {
  adminWorkspaceProjectsPage.grid.selectRowWithMatchingCellContent('name', 'AZERTY');

  projectView.permissionsPanel.grid.assertRowCount(0);

  NavBarAdminElement.navigateToAdministration(AdminWorkspaceItem.USERS);
  const adminWorkspaceTeamsPage = adminWorkspaceUsersPage.goToTeamAnchor();

  adminWorkspaceTeamsPage.grid.selectRowWithMatchingCellContent('name', 'monequipe');
  teamView.teamMembersPanel.grid.assertRowCount(0);
  NavBarAdminElement.navigateToAdministration(AdminWorkspaceItem.USERS);
}

function assertDeletionIsImpossible(
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
  alertDialogElement: AlertDialogElement,
  confirmDeleteDialog: RemoveUserDialogElement,
) {
  adminWorkspaceUsersPage.grid.findRowId('login', 'UserB').then((userID) => {
    adminWorkspaceUsersPage.grid.getCell(userID, 'delete', 'rightViewport').iconRenderer().click();
  });
  confirmDeleteDialog.clickOnConfirmButton();

  alertDialogElement.assertExclamationCircleIconExists();
  alertDialogElement.assertTitleHasText('Erreur');
  alertDialogElement.assertHasMessage(
    "L'utilisateur ou l'un des utilisateurs sélectionné est propriétaire " +
      "d'au moins un jalon. Veuillez réaffecter le/les jalons à d'autres utilisateurs.",
  );
  alertDialogElement.close();
}

function checkLoginPossibilityAfterDeletion() {
  NavBarAdminElement.close();
  NavBarElement.logout();
  const loginPage = LoginPage.navigateTo();
  loginPage.assertExists();
  loginPage.assertLoginFails('UserA', 'admin');
  loginPage.assertLoginFailedWarningIsVisible();
  cy.logInAs('UserB', 'admin');
}
