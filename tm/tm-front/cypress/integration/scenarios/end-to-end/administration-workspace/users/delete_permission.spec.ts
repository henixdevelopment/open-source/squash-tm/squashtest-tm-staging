import { DatabaseUtils } from '../../../../utils/database.utils';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { AdminWorkspaceUsersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { UserViewPage } from '../../../../page-objects/pages/administration-workspace/user-view/user-view.page';
import { RemoveProjectDialog } from '../../../../page-objects/pages/administration-workspace/dialogs/remove-project-dialog.element';
import {
  AdminWorkspaceItem,
  NavBarAdminElement,
} from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import { ProjectViewPage } from '../../../../page-objects/pages/administration-workspace/project-view/project-view.page';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import { AdministrationWorkspacePage } from '../../../../page-objects/pages/administration-workspace/administration-workspace.page';
import { selectByDataTestDialogButtonId } from '../../../../utils/basic-selectors';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addPermissionToUser,
  ApiPermissionGroup,
} from '../../../../utils/end-to-end/api/add-permissions';

describe('Delete permission for a User', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
    cy.logInAs('admin', 'admin');
  });

  it('F01-UC0102.CT08 - should delete permission (User)', () => {
    cy.log('Step 1');
    const adminWorkspaceUsersPage = NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>(
      AdminWorkspaceItem.USERS,
    );
    adminWorkspaceUsersPage.selectUserByLogin('jlescaut');
    const userViewPage = new UserViewPage();
    userViewPage.assertExists();
    userViewPage.anchors.clickLink('authorisations');

    cy.log('Step 2');
    const authorisationGrid = userViewPage.authorisationsPanel.grid;
    openDeletePopUp(authorisationGrid);

    cy.log('Step 3');
    const removeProjectDialog = new RemoveProjectDialog();
    controlPopUpConformity(removeProjectDialog);
    userViewPage.authorisationsPanel.grid.assertRowCount(1);

    cy.log('Step 4');
    removePermission(authorisationGrid, removeProjectDialog);

    cy.log('Step 5');
    const adminWorkspaceProjectsPage = NavBarAdminElement.navigateToAdministration(
      AdminWorkspaceItem.PROJECTS,
    );
    const projectViewPage = new ProjectViewPage();
    checkRemovalAsAdmin(adminWorkspaceProjectsPage, projectViewPage);
    cy.logInAs('jlescaut', 'admin');
    const workspaceProjectsPage = NavBarElement.navigateToAdministration(
      AdminWorkspaceItem.PROJECTS,
    );
    workspaceProjectsPage.grid.assertRowCount(1);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('jlescaut').withFirstName('Julie').withLastName('Lescaut')])
    .withProjects([
      new ProjectBuilder('A'),
      new ProjectBuilder('1'),
      new ProjectBuilder('2'),
      new ProjectBuilder('3'),
      new ProjectBuilder('4'),
      new ProjectBuilder('5'),
      new ProjectBuilder('6'),
      new ProjectBuilder('7'),
      new ProjectBuilder('8'),
      new ProjectBuilder('9'),
      new ProjectBuilder('10'),
    ])
    .build();
}
function initPermissions() {
  addPermissionToUser(2, ApiPermissionGroup.PROJECT_MANAGER, [1, 11]);
  addPermissionToUser(2, ApiPermissionGroup.ADVANCED_TESTER, [2]);
  addPermissionToUser(2, ApiPermissionGroup.AUTOMATED_TEST_WRITER, [3]);
  addPermissionToUser(2, ApiPermissionGroup.PROJECT_VIEWER, [4]);
  addPermissionToUser(2, ApiPermissionGroup.TEST_DESIGNER, [5]);
  addPermissionToUser(2, ApiPermissionGroup.TEST_EDITOR, [6]);
  addPermissionToUser(2, ApiPermissionGroup.TEST_RUNNER, [7]);
  addPermissionToUser(2, ApiPermissionGroup.VALIDATOR, [8, 9, 10]);
}
function openDeletePopUp(authorisationGrid: GridElement) {
  authorisationGrid.assertExists();
  authorisationGrid.assertRowCount(10);
  authorisationGrid.gridFooter.goToNextPage();
  authorisationGrid.assertRowCount(1);
  authorisationGrid.findRowId(GridColumnId.projectName, 'A').then((userID) => {
    authorisationGrid.getCell(userID, 'delete').iconRenderer().click();
  });
}
function controlPopUpConformity(removeProjectDialog: RemoveProjectDialog) {
  removeProjectDialog.assertExists();
  removeProjectDialog.assertTitleHasText("Supprimer l'habilitation");
  removeProjectDialog.assertHasMessage(
    "L'habilitation sélectionnée ne sera plus associée. Confirmez-vous la suppression de l'habilitation ?",
  );
  removeProjectDialog.assertExclamationCircleIconExists();
  removeProjectDialog.assertCancelButtonExists();
  removeProjectDialog.assertConfirmButtonExists();
  removeProjectDialog.checkButtonClass(selectByDataTestDialogButtonId('confirm'), 'danger-icon');
  removeProjectDialog.cancel();
}

function removePermission(
  authorisationGrid: GridElement,
  removeProjectDialog: RemoveProjectDialog,
) {
  authorisationGrid.findRowId(GridColumnId.projectName, 'A').then((userID) => {
    authorisationGrid.getCell(userID, 'delete').iconRenderer().click();
    removeProjectDialog.assertExists();
    removeProjectDialog.confirm();
    authorisationGrid.assertRowCount(10);
    authorisationGrid.gridFooter.checkPaginationDoesntExist();
  });
}
function checkRemovalAsAdmin(
  adminWorkspaceProjectsPage: AdministrationWorkspacePage,
  projectViewPage: ProjectViewPage,
) {
  adminWorkspaceProjectsPage.grid.selectRowsWithMatchingCellContent('name', ['A']);
  projectViewPage.assertExists();
  projectViewPage.showPermissionsPanel();
  projectViewPage.permissionsPanel.grid.assertExists();
  projectViewPage.permissionsPanel.grid.assertRowCount(0);
  NavBarAdminElement.close();
  NavBarElement.logout();
}
