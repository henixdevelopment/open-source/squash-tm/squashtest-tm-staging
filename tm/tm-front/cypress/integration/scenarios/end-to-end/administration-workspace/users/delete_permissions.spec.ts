import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { AdminWorkspaceUsersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import { UserViewPage } from '../../../../page-objects/pages/administration-workspace/user-view/user-view.page';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import { RemoveProjectDialog } from '../../../../page-objects/pages/administration-workspace/dialogs/remove-project-dialog.element';
import {
  AdminWorkspaceItem,
  NavBarAdminElement,
} from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import { ProjectViewPage } from '../../../../page-objects/pages/administration-workspace/project-view/project-view.page';
import { AdministrationWorkspacePage } from '../../../../page-objects/pages/administration-workspace/administration-workspace.page';
import { selectByDataTestDialogButtonId } from '../../../../utils/basic-selectors';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addPermissionToUser,
  ApiPermissionGroup,
} from '../../../../utils/end-to-end/api/add-permissions';

describe('Delete permission for a User', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
    cy.logInAs('admin', 'admin');
  });

  it('F01-UC0102.CT09 - should delete permissions (User)', () => {
    cy.log('Step 1');
    const adminWorkspaceUsersPage = NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>(
      AdminWorkspaceItem.USERS,
    );
    adminWorkspaceUsersPage.selectUserByLogin('jlescaut');
    const userViewPage = new UserViewPage();
    userViewPage.anchors.clickLink('authorisations');
    userViewPage.assertExists();
    adminWorkspaceUsersPage.grid.checkActiveColumnVisibility();

    cy.log('Step 2');
    const removeProjectDialog = userViewPage.openRemoveProjectDialog();
    const authorisationGrid = userViewPage.authorisationsPanel.grid;
    openDeletePopUp(authorisationGrid, userViewPage);

    cy.log('Step 3');
    controlPopUpConformity(removeProjectDialog);
    userViewPage.authorisationsPanel.grid.assertRowCount(1);

    cy.log('Step 4');
    removePermission(authorisationGrid, removeProjectDialog, userViewPage);

    cy.log('Step 5');
    selectElementsOpenPopUpThenDelete(
      authorisationGrid,
      removeProjectDialog,
      userViewPage,
      ['1', '7', '9'],
      7,
    );

    cy.log('Step 6');
    selectElementsOpenPopUpThenDelete(
      authorisationGrid,
      removeProjectDialog,
      userViewPage,
      ['1', '2', '3', '4', '5', '6', '8'],
      0,
    );
    authorisationGrid.assertGridIsEmpty();

    cy.log('Step 7');
    const adminWorkspaceProjectsPage = NavBarAdminElement.navigateToAdministration(
      AdminWorkspaceItem.PROJECTS,
    );
    const projectViewPage = new ProjectViewPage();
    checkRemovalAsAdmin(adminWorkspaceProjectsPage, projectViewPage);
    cy.logInAs('jlescaut', 'admin');
    NavBarElement.assertAdministrationFieldNotVisible();
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('jlescaut').withFirstName('Julie').withLastName('Lescaut')])
    .withProjects([
      new ProjectBuilder('A'),
      new ProjectBuilder('1'),
      new ProjectBuilder('2'),
      new ProjectBuilder('3'),
      new ProjectBuilder('4'),
      new ProjectBuilder('5'),
      new ProjectBuilder('6'),
      new ProjectBuilder('7'),
      new ProjectBuilder('8'),
      new ProjectBuilder('9'),
      new ProjectBuilder('10'),
    ])
    .build();
}
function initPermissions() {
  addPermissionToUser(2, ApiPermissionGroup.PROJECT_MANAGER, [1, 11]);
  addPermissionToUser(2, ApiPermissionGroup.ADVANCED_TESTER, [2]);
  addPermissionToUser(2, ApiPermissionGroup.AUTOMATED_TEST_WRITER, [3]);
  addPermissionToUser(2, ApiPermissionGroup.PROJECT_VIEWER, [4]);
  addPermissionToUser(2, ApiPermissionGroup.TEST_DESIGNER, [5]);
  addPermissionToUser(2, ApiPermissionGroup.TEST_EDITOR, [6]);
  addPermissionToUser(2, ApiPermissionGroup.TEST_RUNNER, [7]);
  addPermissionToUser(2, ApiPermissionGroup.VALIDATOR, [8, 9, 10]);
}
function openDeletePopUp(authorisationGrid: GridElement, userViewPage: UserViewPage) {
  authorisationGrid.assertExists();
  authorisationGrid.assertRowCount(10);
  authorisationGrid.gridFooter.goToNextPage();
  authorisationGrid.assertRowCount(1);
  userViewPage.checkIfButtonIsActiveWithClass('remove-authorisations', 'label-color');
  authorisationGrid.selectRowsWithMatchingCellContent(GridColumnId.projectName, ['A']);
  userViewPage.checkIfButtonIsActiveWithClass('remove-authorisations', '__hover_pointer');
  userViewPage.clickRemoveButton('remove-authorisations');
}

function controlPopUpConformity(removeProjectDialog: RemoveProjectDialog) {
  removeProjectDialog.assertExists();
  removeProjectDialog.assertTitleHasText('Supprimer les habilitations');
  removeProjectDialog.assertHasMessage(
    'Les habilitations sélectionnées ne seront plus associées. Confirmez-vous la suppression des habilitations?',
  );
  removeProjectDialog.assertExclamationCircleIconExists();
  removeProjectDialog.assertCancelButtonExists();
  removeProjectDialog.assertConfirmButtonExists();
  removeProjectDialog.checkButtonClass(selectByDataTestDialogButtonId('confirm'), 'danger-icon');
  removeProjectDialog.cancel();
}
function removePermission(
  authorisationGrid: GridElement,
  removeProjectDialog: RemoveProjectDialog,
  userViewPage: UserViewPage,
) {
  userViewPage.clickRemoveButton('remove-authorisations');
  removeProjectDialog.assertExists();
  removeProjectDialog.confirm();
  authorisationGrid.assertRowCount(10);
  authorisationGrid.gridFooter.checkPaginationDoesntExist();
}
function checkRemovalAsAdmin(
  adminWorkspaceProjectsPage: AdministrationWorkspacePage,
  projectViewPage: ProjectViewPage,
) {
  projectViewPage.permissionsPanel.grid.assertRowCount(0);
  NavBarAdminElement.close();
  NavBarElement.logout();
}

function selectElementsOpenPopUpThenDelete(
  authorisationGrid: GridElement,
  removeProjectDialog: RemoveProjectDialog,
  userViewPage: UserViewPage,
  elements: string[],
  count: number,
) {
  authorisationGrid.selectRowsWithMatchingCellContent(GridColumnId.projectName, elements);
  userViewPage.clickRemoveButton('remove-authorisations');
  removeProjectDialog.assertExists();
  removeProjectDialog.cancel();
  userViewPage.clickRemoveButton('remove-authorisations');
  removeProjectDialog.assertExists();
  removeProjectDialog.confirm();
  authorisationGrid.assertRowCount(count);
}
