import { DatabaseUtils } from '../../../../utils/database.utils';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { AdminWorkspaceUsersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import {
  AdminWorkspaceItem,
  NavBarAdminElement,
} from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import { UserViewPage } from '../../../../page-objects/pages/administration-workspace/user-view/user-view.page';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { SystemViewSettingsPage } from '../../../../page-objects/pages/administration-workspace/system-view/system-view-settings.page';
import { EditableTextFieldElement } from '../../../../page-objects/elements/forms/editable-text-field.element';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import { SwitchFieldElement } from '../../../../page-objects/elements/forms/switch-field.element';
import { HomeWorkspacePage } from '../../../../page-objects/pages/home-workspace/home-workspace.page';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';

describe('Modify a login', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    cy.logInAs('admin', 'admin');
  });

  it('F01-UC0102.CT03 - should modify a login', () => {
    cy.log('Step 1');
    NavBarElement.navigateToAdministrationSystem();
    const systemViewSettingsPage = new SystemViewSettingsPage();
    systemViewSettingsPage.clickSettingsAnchor();
    const loginSwitch = systemViewSettingsPage.caseInsensitiveLoginSwitch;
    systemViewSettingsPage.assertSpinnerIsNotPresent();
    loginSwitch.assertExists();
    loginSwitch.checkValue(false);
    const adminWorkspaceUsersPage: AdminWorkspaceUsersPage =
      NavBarAdminElement.navigateToAdministration(AdminWorkspaceItem.USERS);
    adminWorkspaceUsersPage.selectUserByLogin('LOGIN_01');
    const userViewPage = new UserViewPage();

    cy.log('Step 2');
    const grid = adminWorkspaceUsersPage.grid;
    const loginField = userViewPage.loginTextField;
    verifyLoginFieldEditability(loginField);

    cy.log('Step 3');
    checkErrorMessagesFromLogin(loginField);

    cy.log('Step 4');
    modifyOrCancelLoginModification(loginField, grid);

    cy.log('Step 5');
    clickInsensitiveThenTest(
      loginSwitch,
      loginField,
      systemViewSettingsPage,
      adminWorkspaceUsersPage,
    );

    cy.log('Step 6');
    setDifferentValuesAndTestThem(loginField);

    cy.log('Step 7');
    NavBarAdminElement.close();
    NavBarElement.logout();
    cy.logInAs(
      'thisisalongandboringemailtounsurelongloginworksitcontainsonlywords@hotmail.com',
      'admin',
    );
    const homeWorkspace = new HomeWorkspacePage();
    homeWorkspace.assertExists();
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([
      new UserBuilder('LOGIN_01').withFirstName('LOGIN_01').withLastName('LOGIN_01'),
      new UserBuilder('Login.02').withFirstName('Login.02').withLastName('Login.02'),
    ])
    .build();
}

function setValueClickButtonCheckResult(
  loginField: EditableTextFieldElement,
  newLogin: string,
  errorMessage?: string,
) {
  loginField.setValue(newLogin);
  loginField.clickOnConfirmButton();
  if (errorMessage != null) {
    loginField.assertHasError(errorMessage);
  } else {
    loginField.checkContent(newLogin);
  }
}

function verifyLoginFieldEditability(loginField: EditableTextFieldElement) {
  loginField.assertContainsText('LOGIN_01');
  loginField.click();
  loginField.checkEditMode(true);
  loginField.cancel();
}

function checkErrorMessagesFromLogin(loginField: EditableTextFieldElement) {
  loginField.clearTextField();
  setValueClickButtonCheckResult(loginField, '{backspace}', 'Ce champ ne peut pas être vide.');
  setValueClickButtonCheckResult(loginField, 'Login.02', 'Ce login est déjà utilisé.');
}

function modifyOrCancelLoginModification(loginField: EditableTextFieldElement, grid: GridElement) {
  loginField.setValue('login.02');
  loginField.checkEditMode(true);
  loginField.cancel();
  grid.findRowId('login', 'Login.02').should('exist');
  setValueClickButtonCheckResult(loginField, 'login.02');
  grid.findRowId('login', 'login.02').should('exist');
  loginField.checkEditMode(false);
  setValueClickButtonCheckResult(loginField, 'LOGIN_01');
}

function clickInsensitiveThenTest(
  loginSwitch: SwitchFieldElement,
  loginField: EditableTextFieldElement,
  systemViewSettingsPage: SystemViewSettingsPage,
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
) {
  NavBarAdminElement.navigateToAdministrationSystem();
  systemViewSettingsPage.clickSettingsAnchor();
  loginSwitch.click();
  loginSwitch.checkValue(true);
  NavBarAdminElement.navigateToAdministration(AdminWorkspaceItem.USERS);
  adminWorkspaceUsersPage.selectUserByLogin('LOGIN_01');
  setValueClickButtonCheckResult(loginField, 'login_01', 'Ce login est déjà utilisé.');
  setValueClickButtonCheckResult(loginField, 'LOGin.02', 'Ce login est déjà utilisé.');
}

function setDifferentValuesAndTestThem(loginField: EditableTextFieldElement) {
  setValueClickButtonCheckResult(
    loginField,
    'a1z2e3r4t5y6u7i8o9p0a1z2e3r4t5y6u7i8o9p0a1z2e3r4t5y6u7i8o9p0a1z2e3r4t5y6u7i8o9p0a1z2e3r4t5y6u7i8o9p0a1z2e3r4t5y6u7i8o9p0a1z2e3r4t5y6u7i8o9p0',
    'La valeur dépasse la taille maximale autorisée.',
  );
  setValueClickButtonCheckResult(loginField, 'z"é"\'(-è_çà)8*+3/^$*ù!:;,?./§%¨£@[#" ');
  setValueClickButtonCheckResult(
    loginField,
    'thisisalongandboringemailtounsurelongloginworksitcontainsonlywords@hotmail.com',
  );
}
