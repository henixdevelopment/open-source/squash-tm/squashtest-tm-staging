import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { AdminWorkspaceUsersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import { UserViewPage } from '../../../../page-objects/pages/administration-workspace/user-view/user-view.page';
import { AssociateTeamToUserDialog } from '../../../../page-objects/pages/administration-workspace/user-view/dialogs/associate-team-to-user.dialog';
import { UserTeamsPanelElement } from '../../../../page-objects/pages/administration-workspace/user-view/panels/user-teams-panel.element';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import { GroupedMultiListElement } from '../../../../page-objects/elements/filters/grouped-multi-list.element';
import { AdminWorkspaceItem } from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import {
  TeamBuilder,
  UserBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';

describe('Associate teams', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
  });

  it('F01-UC0102.CT11 - should associate one or several teams', () => {
    cy.log('Step 1');
    cy.logInAs('admin', 'admin');
    const adminWorkspaceUsersPage = NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>(
      AdminWorkspaceItem.USERS,
    );
    const userView = new UserViewPage();
    const teamPanel = userView.teamsPanel;
    navigateToUserViewAndAssertTeamPanelExists(adminWorkspaceUsersPage, userView);

    cy.log('Step 2');
    const teamGrid = teamPanel.grid;
    const associateTeamToUserDialog = clickOnAssociateButtonAndDialogExists(teamPanel, teamGrid);

    cy.log('Step 3');
    associateTeamToUserDialog.checkConformity();

    cy.log('Step 4');
    const multiList = new GroupedMultiListElement();
    selectOneTeamAndCheckConformity(associateTeamToUserDialog, multiList, teamNames);

    cy.log('Step 5');
    cancelAssociateTeam(associateTeamToUserDialog, teamGrid);

    cy.log('Step 6');
    const teamToSelect = teamNames[1];
    associateUserToTeam(
      teamPanel,
      associateTeamToUserDialog,
      multiList,
      teamNames,
      teamToSelect,
      teamGrid,
      userView,
    );

    cy.log('Step 7');
    associateUserToTeams(
      teamPanel,
      associateTeamToUserDialog,
      multiList,
      teamNames,
      teamToSelect,
      userView,
    );

    cy.log('Step 8');
    checkGridDataAfterAssociation(teamGrid, teamNames);
  });

  const teamNames: string[] = [
    'Team 001',
    'Team 002',
    'Team 003',
    'Team 004',
    'Team 005',
    'Team 006',
    'Team 007',
    'Team 008',
    'Team 009',
    'Team 010',
    'Team 011',
  ];

  function initTestData() {
    return new PrerequisiteBuilder()
      .withUsers([
        new UserBuilder('mdubois')
          .withFirstName('Marie-Anne')
          .withLastName('Dubois')
          .withEmail('mdubois@test.fr'),
      ])
      .withTeams(teamNames.map((team) => new TeamBuilder(team)))
      .build();
  }
});

function navigateToUserViewAndAssertTeamPanelExists(
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
  userView: UserViewPage,
) {
  adminWorkspaceUsersPage.grid.findRowId('login', 'mdubois');
  adminWorkspaceUsersPage.selectRowWithMatchingCellContent('login', 'mdubois');
  userView.assertExists();

  userView.anchors.clickLink('teams');
  userView.teamsPanel.assertIsVisible();
}

function clickOnAssociateButtonAndDialogExists(
  teamPanel: UserTeamsPanelElement,
  teamGrid: GridElement,
): AssociateTeamToUserDialog {
  teamGrid.assertExists();
  teamGrid.assertGridIsEmpty();

  return teamPanel.clickOnAssociateTeamToUserButton();
}

function selectOneTeamAndCheckConformity(
  associateTeamToUserDialog: AssociateTeamToUserDialog,
  multiList: GroupedMultiListElement,
  teamNames: string[],
) {
  associateTeamToUserDialog.clickOnSelectMembers();

  multiList.assertExists();
  multiList.assertSearchFieldExists();
  multiList.assertGroupPanelExists();

  multiList.checkGroupConformity('ungrouped-items', teamNames);
  const teamToSelect = teamNames[0];
  multiList.toggleOneItem(teamToSelect);
  multiList.assertItemIsSelectedAndPulledUp(teamToSelect);
  associateTeamToUserDialog.assertSelectMembersFieldHasText(teamToSelect);
  multiList.close();
}

function cancelAssociateTeam(
  associateTeamToUserDialog: AssociateTeamToUserDialog,
  teamGrid: GridElement,
) {
  associateTeamToUserDialog.cancel();
  teamGrid.assertGridIsEmpty();
}

function associateUserToTeam(
  teamPanel: UserTeamsPanelElement,
  associateTeamToUserDialog: AssociateTeamToUserDialog,
  multiList: GroupedMultiListElement,
  teamNames: string[],
  teamToSelect: string,
  teamGrid: GridElement,
  userView: UserViewPage,
) {
  teamPanel.clickOnAssociateTeamToUserButton();
  associateTeamToUserDialog.assertExists();
  associateTeamToUserDialog.clickOnSelectMembers();
  multiList.assertExists();

  multiList.toggleOneItem(teamToSelect);
  multiList.close();
  associateTeamToUserDialog.assertSelectMembersFieldHasText(teamToSelect);
  associateTeamToUserDialog.confirm();

  teamGrid.assertRowCount(1);
  userView.assertAnchorTeamCount(1);
  teamGrid.findRowId('name', teamToSelect).then((teamID) => {
    teamGrid.assertRowExist(teamID);
  });
}

function associateUserToTeams(
  teamPanel: UserTeamsPanelElement,
  associateTeamToUserDialog: AssociateTeamToUserDialog,
  multiList: GroupedMultiListElement,
  teamNames: string[],
  selectedTeam: string,
  userView: UserViewPage,
) {
  teamPanel.clickOnAssociateTeamToUserButton();
  associateTeamToUserDialog.assertExists();

  associateTeamToUserDialog.clickOnSelectMembers();
  multiList.assertExists();
  multiList.assertSearchFieldExists();
  multiList.clickOnSearchField();
  const teamToSearch = teamNames[3];
  multiList.searchValue(teamToSearch);
  multiList.assertGroupPanelExists();
  multiList.assertItemExist(teamToSearch);
  multiList.assertItemCount(1);
  multiList.toggleOneItem(teamToSearch);
  multiList.assertItemIsSelectedAndPulledUp(teamToSearch);
  multiList.clearSearchValue();
  multiList.assertItemCount(9);

  const teamsAlreadyAssociated = [selectedTeam, teamToSearch];
  teamNames.map((teamName) => {
    if (!teamsAlreadyAssociated.includes(teamName)) {
      multiList.toggleOneItem(teamName);
      multiList.assertItemIsSelectedAndPulledUp(teamName);
    }
  });
  const teamsWithoutAlreadySelectedOne = teamNames.filter((team) => team !== selectedTeam);
  associateTeamToUserDialog.assertSelectMembersFieldHasText(
    teamsWithoutAlreadySelectedOne.join(', '),
  );
  associateTeamToUserDialog.checkSelectMembersFieldContentTruncation();
  multiList.close();

  associateTeamToUserDialog.confirm();

  userView.assertAnchorTeamCount(11);
}

function checkGridDataAfterAssociation(teamGrid: GridElement, teamNames: string[]) {
  const gridFooter = teamGrid.gridFooter;
  gridFooter.checkPaginationDisplay('1 - 10 / 11');
  gridFooter.checkPagination('1/2');
  gridFooter.assertPaginationButtonsExist();
  checkGridConformity(teamNames, teamGrid);

  gridFooter.goToNextPage();
  gridFooter.checkPaginationDisplay('11 - 11 / 11');
  gridFooter.checkPagination('2/2');
  checkGridConformity([teamNames[10]], teamGrid);
}

function checkGridConformity(teamNames: string[], teamGrid: GridElement) {
  teamNames.map((teamName, index) => {
    if (index < 10) {
      teamGrid.findRowId('name', teamName).then((teamID) => {
        teamGrid.assertRowExist(teamID);
      });
    }
  });
}
