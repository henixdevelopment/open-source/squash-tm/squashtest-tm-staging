import { DatabaseUtils } from '../../../../utils/database.utils';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { AdminWorkspaceUsersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import { AdminWorkspaceItem } from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import { AdminWorkspaceTeamsPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-teams.page';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import { CreateTeamDialog } from '../../../../page-objects/pages/administration-workspace/dialogs/create-team-dialog.element';
import {
  EMPTY_FIELD_ERROR,
  MAXLENGTH_FIELD_ERROR,
  NAME_ALREADY_USED_ERROR,
} from '../../../../page-objects/elements/forms/error.message';
import { TeamViewPage } from '../../../../page-objects/pages/administration-workspace/team-view/team-view.page';
import * as dayjs from 'dayjs';

describe('Add teams', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
  });

  it('F01-UC011.CT02 - should add one or several teams', () => {
    cy.log('Step 1');
    cy.logInAs('admin', 'admin');
    const adminWorkspaceUsersPage = NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>(
      AdminWorkspaceItem.USERS,
    );
    const adminWorkspaceTeamsPage = adminWorkspaceUsersPage.goToTeamAnchor();
    const teamGrid = assertTeamGridExistsAndIsEmpty(adminWorkspaceTeamsPage.grid);
    const createTeamDialog = clickOnCreateTeamButtonAndAssertExists(adminWorkspaceTeamsPage);

    cy.log('Step 2');
    createTeamDialog.checkConformity();

    cy.log('Step 3');
    createTeamDialog.assertNameFieldIsFocused();

    cy.log('Step 4');
    cancelTeamCreation(createTeamDialog, teamGrid, adminWorkspaceTeamsPage);

    cy.log('Step 5');
    const teamViewPage = new TeamViewPage();
    createOneTeam(createTeamDialog, teamGrid, adminWorkspaceTeamsPage, teamViewPage);

    cy.log('Step 6');
    checkCreateTeamInfoInTable(teamGrid);

    cy.log('Step 7');
    createMultipleTeamsAndCheck(createTeamDialog, teamGrid, adminWorkspaceTeamsPage, teamViewPage);
  });
});

function assertTeamGridExistsAndIsEmpty(teamGrid: GridElement): GridElement {
  teamGrid.assertExists();
  teamGrid.assertGridIsEmpty();
  return teamGrid;
}

function clickOnCreateTeamButtonAndAssertExists(
  adminWorkspaceTeamsPage: AdminWorkspaceTeamsPage,
): CreateTeamDialog {
  const createTeamDialog = adminWorkspaceTeamsPage.openCreateTeam();
  createTeamDialog.assertExists();
  return createTeamDialog;
}

function cancelTeamCreation(
  createTeamDialog: CreateTeamDialog,
  teamGrid: GridElement,
  adminWorkspaceTeamsPage: AdminWorkspaceTeamsPage,
) {
  cancelWithCancelButton(createTeamDialog, teamGrid);
  cancelWithEscape(createTeamDialog, teamGrid, adminWorkspaceTeamsPage);
}

function cancelWithCancelButton(createTeamDialog: CreateTeamDialog, teamGrid: GridElement) {
  createTeamDialog.fillNameAndDescriptionAndCheckContent('Team 001', "C'est la Team 001");
  createTeamDialog.cancel();
  teamGrid.assertGridIsEmpty();
}

function cancelWithEscape(
  createTeamDialog: CreateTeamDialog,
  teamGrid: GridElement,
  adminWorkspaceTeamsPage: AdminWorkspaceTeamsPage,
) {
  adminWorkspaceTeamsPage.openCreateTeam();
  createTeamDialog.assertExists();
  createTeamDialog.fillNameAndDescriptionAndCheckContent('New Team 001', "C'est la New Team 001");
  createTeamDialog.escapeToCloseDialogAndAssertNotExist();
  teamGrid.assertGridIsEmpty();
}

function createOneTeam(
  createTeamDialog: CreateTeamDialog,
  teamGrid: GridElement,
  adminWorkspaceTeamsPage: AdminWorkspaceTeamsPage,
  teamViewPage: TeamViewPage,
) {
  addFailureWithoutFillName(createTeamDialog, adminWorkspaceTeamsPage);
  addFailureWithTooLongName(createTeamDialog);
  addOneTeam(createTeamDialog, teamGrid, teamViewPage);
}

function addFailureWithoutFillName(
  createTeamDialog: CreateTeamDialog,
  adminWorkspaceTeamsPage: AdminWorkspaceTeamsPage,
) {
  adminWorkspaceTeamsPage.openCreateTeam();
  createTeamDialog.assertExists();
  createTeamDialog.fillNameAndDescriptionAndCheckContent(
    '',
    'Cette description est suffisamment longue pour être tronquée',
  );
  createTeamDialog.addWithClientSideFailure();
  createTeamDialog.assertNameHasError(EMPTY_FIELD_ERROR);
}

function addFailureWithTooLongName(createTeamDialog: CreateTeamDialog) {
  createTeamDialog.assertExists();
  createTeamDialog.fillNameAndCheckContent(
    'Arras est une petite ville du Pas-de-Calais dans le nord de la France',
  );
  createTeamDialog.addWithEnterAndClientSideFailure();
  createTeamDialog.assertNameHasError(MAXLENGTH_FIELD_ERROR);
}

function addOneTeam(
  createTeamDialog: CreateTeamDialog,
  teamGrid: GridElement,
  teamViewPage: TeamViewPage,
) {
  createTeamDialog.assertExists();
  createTeamDialog.fillNameAndCheckContent('Arras');
  createTeamDialog.clickOnAddButton();
  createTeamDialog.assertNotExist();
  teamViewPage.assertExists();
  teamGrid.assertRowCount(1);
}

function checkCreateTeamInfoInTable(teamGrid: GridElement) {
  teamGrid.findRowId('name', 'Arras').then((teamId) => {
    teamGrid
      .getRow(teamId)
      .cell('description')
      .textRenderer()
      .assertContainsText('Cette description est suffisamment longue pour être tronquée');
    teamGrid.getRow(teamId).cell('teamMembersCount').textRenderer().assertContainsText('0');
    DatabaseUtils.executeSelectQuery(`SELECT CREATED_ON FROM CORE_TEAM WHERE NAME = 'Arras';`).then(
      (result) => {
        const adminCreationDate: Date = result[0].created_on;
        const formattedCreationDate: string = dayjs(adminCreationDate).format('DD/MM/YYYY');
        teamGrid
          .getRow(teamId)
          .cell('createdOn')
          .textRenderer()
          .assertContainsText(formattedCreationDate);
      },
    );
    teamGrid.getRow(teamId).cell('createdBy').textRenderer().assertContainsText('admin');
    teamGrid.getRow(teamId).cell('lastModifiedOn').textRenderer().assertContainsText('-');
    teamGrid.getRow(teamId).cell('lastModifiedBy').textRenderer().assertContainsText('-');
  });
}

function createMultipleTeamsAndCheck(
  createTeamDialog: CreateTeamDialog,
  teamGrid: GridElement,
  adminWorkspaceTeamsPage: AdminWorkspaceTeamsPage,
  teamViewPage: TeamViewPage,
) {
  addFailureWithExistingName(createTeamDialog, adminWorkspaceTeamsPage);
  addAnotherTeam(createTeamDialog, teamGrid);
  addThirdTeam(createTeamDialog, teamGrid, teamViewPage);
}

function addFailureWithExistingName(
  createTeamDialog: CreateTeamDialog,
  adminWorkspaceTeamsPage: AdminWorkspaceTeamsPage,
) {
  adminWorkspaceTeamsPage.openCreateTeam();
  createTeamDialog.assertExists();
  createTeamDialog.fillNameAndDescriptionAndCheckContent(
    'Arras',
    "Ceci est la description de l'équipe d'Arras n°2",
  );
  createTeamDialog.addWithClientSideFailure();
  createTeamDialog.assertNameHasError(NAME_ALREADY_USED_ERROR);
}

function addAnotherTeam(createTeamDialog: CreateTeamDialog, teamGrid: GridElement) {
  createTeamDialog.assertExists();
  createTeamDialog.fillNameAndCheckContent('Arras2');
  createTeamDialog.clickOnAddAnotherButton();
  createTeamDialog.assertExists();
  createTeamDialog.checkIfFormIsEmpty();
  teamGrid.assertRowCount(2);
  createTeamDialog.assertNameFieldIsFocused();
}

function addThirdTeam(
  createTeamDialog: CreateTeamDialog,
  teamGrid: GridElement,
  teamViewPage: TeamViewPage,
) {
  createTeamDialog.assertExists();
  createTeamDialog.fillNameAndDescriptionAndCheckContent(
    'Arras3',
    "Ceci est la description de l'équipe d'Arras n°3",
  );
  createTeamDialog.clickOnAddButton();
  createTeamDialog.assertNotExist();
  teamViewPage.assertExists();
  teamGrid.assertRowCount(3);
  teamViewPage.nameTextField.assertContainsText('Arras3');
}
