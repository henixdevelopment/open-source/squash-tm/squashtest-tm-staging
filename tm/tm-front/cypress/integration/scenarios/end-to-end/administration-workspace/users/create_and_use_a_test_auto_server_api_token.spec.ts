import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { AdminWorkspaceUsersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import { UserGridElement } from '../../../../page-objects/elements/grid/grid.element';
import { UserViewPage } from '../../../../page-objects/pages/administration-workspace/user-view/user-view.page';
import { BaseDialogElement } from '../../../../page-objects/elements/dialog/base-dialog.element';
import Chainable = Cypress.Chainable;

describe(`Check that an administrator can create a token on behalf of a test automation server and that this server's user can use it`, function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
  });

  it('F01-UC0102 - create_and_use_test_auto_server_api_token', () => {
    cy.logInAs('RonW', 'admin');
    createAndUseTestAutomationServerApiToken();
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([
      new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley').withGroupName('Admin'),
      new UserBuilder('HermioneG')
        .withFirstName('Hermione')
        .withLastName('Granger')
        .withGroupName('TestAutomationServer'),
    ])
    .build();
}

function createAndUseTestAutomationServerApiToken(): void {
  const adminWorkspaceUsersPage = new AdminWorkspaceUsersPage(
    UserGridElement.createUserGridElement('users'),
  );
  const menu = NavBarElement.showSubMenu('administration', 'administration-menu');
  menu.item('users').click();
  adminWorkspaceUsersPage.assertExists();
  adminWorkspaceUsersPage.grid.findRowId('login', 'HermioneG').then((testAutoServerRowId) => {
    adminWorkspaceUsersPage.grid.getCell(testAutoServerRowId, 'login').findCellTextSpan().click();
  });

  const testAutoServerUserPage = new UserViewPage();
  testAutoServerUserPage.assertExists();
  testAutoServerUserPage.loginTextField.assertContainsText('HermioneG');
  const apiTokenPanel = testAutoServerUserPage.apiTokenPanel;
  apiTokenPanel.assertExists();

  // Create two personal API tokens
  const oneWeekInMilliseconds = 7 * 24 * 60 * 60 * 1000;
  const expiryDateInOneWeek = new Date().getTime() + oneWeekInMilliseconds;
  const expiryDateInTwoWeeks = new Date().getTime() + oneWeekInMilliseconds * 2;

  const firstToken = {
    name: 'read token',
    permissionId: 1,
    expiryDate: formatExpiryDate(expiryDateInTwoWeeks),
  };

  const secondToken = {
    name: 'read-write token',
    permissionId: 2,
    expiryDate: formatExpiryDate(expiryDateInOneWeek),
  };

  createApiToken(testAutoServerUserPage, firstToken).then((token1) => cy.wrap(token1).as('token1'));
  createApiToken(testAutoServerUserPage, secondToken).then((token2) =>
    cy.wrap(token2).as('token2'),
  );

  // Assert data is correct
  testAutoServerUserPage.apiTokenPanel.apiTokenGrid.assertRowCount(2);
  assertInitialRowValues(
    1,
    testAutoServerUserPage,
    firstToken.name,
    'Lecture',
    firstToken.expiryDate,
  );
  assertInitialRowValues(
    2,
    testAutoServerUserPage,
    secondToken.name,
    'Lecture et écriture',
    secondToken.expiryDate,
  );

  // Delete first token in UI with keyboard
  testAutoServerUserPage.apiTokenPanel.apiTokenGrid
    .getRow(1)
    .cell('#')
    .findCell()
    .find('span')
    .click();
  cy.get('body').type('{del}');
  const deleteDialog = new BaseDialogElement('confirm-delete');
  deleteDialog.assertExists();
  deleteDialog.confirm();

  testAutoServerUserPage.apiTokenPanel.apiTokenGrid.assertRowCount(1);
  testAutoServerUserPage.apiTokenPanel.apiTokenGrid.getRow(1).assertNotExist();

  // Delete second token in UI with trash bin
  testAutoServerUserPage.apiTokenPanel.apiTokenGrid.getRow(2).cell('delete').iconRenderer().click();
  deleteDialog.assertExists();
  deleteDialog.confirm();
  testAutoServerUserPage.apiTokenPanel.apiTokenGrid.assertRowCount(0);
}

function formatExpiryDate(expiryDate: number) {
  return new Date(expiryDate).toLocaleString().substring(0, 10);
}

function createApiToken(
  userViewPage: UserViewPage,
  tokenData: { name: string; permissionId: number; expiryDate: string },
): Chainable<string> {
  userViewPage.clickAddApiToken();
  userViewPage.fillAddApiTokenForm(tokenData.name, tokenData.permissionId, tokenData.expiryDate);
  userViewPage.generateApiTokenDialogElement.confirm();
  const tokenChainable = userViewPage.displayTokenDialogElement.copyTokenValue();
  userViewPage.displayTokenDialogElement.close();

  return tokenChainable;
}

function assertInitialRowValues(
  rowId: number,
  userViewPage: UserViewPage,
  expectedName: string,
  expectedPermissions: string,
  expectedExpiryDate: string,
) {
  const apiTokenRow1 = userViewPage.apiTokenPanel.apiTokenGrid.getRow(rowId);
  apiTokenRow1.cell('name').findCell().should('contain.text', expectedName);
  apiTokenRow1.cell('permissions').findCell().should('contain.text', expectedPermissions);
  apiTokenRow1
    .cell('createdOn')
    .findCell()
    .should('contain.text', new Date().toLocaleString().substring(0, 10));
  apiTokenRow1.cell('lastUsage').findCell().should('contain.text', '-');
  apiTokenRow1.cell('createdBy').findCell().should('contain.text', 'RonW');
  apiTokenRow1.cell('expiryDate').findCell().should('contain.text', expectedExpiryDate);
}
