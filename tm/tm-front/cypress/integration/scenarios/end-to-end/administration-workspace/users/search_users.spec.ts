import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { AdminWorkspaceUsersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import { UserViewPage } from '../../../../page-objects/pages/administration-workspace/user-view/user-view.page';
import { selectByDataIcon, selectByDataTestElementId } from '../../../../utils/basic-selectors';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import { AdminWorkspaceItem } from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';

describe('Search for one/multiple users', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
  });

  it('F01-UC0101.CT09 - Search for one/multiple users', () => {
    cy.log('Step 1');
    cy.logInAs('admin', 'admin');
    const adminWorkspaceUsersPage = NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>(
      AdminWorkspaceItem.USERS,
    );

    cy.log('Step 2');
    const userView = new UserViewPage();
    const grid = adminWorkspaceUsersPage.grid;
    checkSearchFieldConformity(grid);

    cy.log('Step 3');
    searchByLogin(grid, adminWorkspaceUsersPage, userView);

    cy.log('Step 4');
    searchByFirstName(grid);

    cy.log('Step 5');
    searchByName(grid);

    cy.log('Step 6');
    searchByEmail(grid);

    cy.log('Step 7');
    searchByCreatedBy(grid);
  });

  function checkSearchFieldConformity(grid: GridElement) {
    cy.get(selectByDataTestElementId('user-filter-field')).should('exist');
    cy.get(selectByDataIcon('sqtm-core-tree:search')).should('exist');

    //  NOT IMPLEMENTED : check on search field style on hover not implemented because Cypress does not currently
    //  offer any means to handle css :hover pseudo classes

    cy.get(selectByDataTestElementId('input-filter')).should('exist').click();
    const wrapperElement = cy.get('.ant-input-affix-wrapper');
    wrapperElement.should('have.class', 'ant-input-affix-wrapper');
    wrapperElement.should('have.class', 'ant-input-affix-wrapper-focused');
    cy.get(selectByDataTestElementId('input-filter')).should('have.class', 'ant-input');

    cy.get(selectByDataTestElementId('user-filter-field'))
      .find('nz-input-group span i')
      .should('have.class', 'anticon-sqtm-core-tree:search');
    searchAndCheckCountAndExpectedResults(grid, ['p'], 2);

    cy.get(selectByDataTestElementId('user-filter-field'))
      .find('nz-input-group span i')
      .should('have.class', 'anticon-close-circle');
    searchAndCheckCountAndExpectedResults(grid, ['i'], 1);

    cy.get(selectByDataIcon('close-circle')).click();
    cy.get(selectByDataTestElementId('user-filter-field')).should('not.contain.text');
    grid.assertRowCount(6);
  }

  function searchByLogin(
    grid: GridElement,
    adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
    userView: UserViewPage,
  ) {
    searchAndCheckCountAndExpectedResults(grid, ['T'], 6);
    searchAndCheckCountAndExpectedResults(grid, ['E', 'S', 'T'], 5, [
      'User 1',
      'Testeur',
      'Justin refo',
      'REF01',
      'admin',
    ]);
    searchAndCheckCountAndExpectedResults(grid, ['E'], 3, ['Testeur', 'Justin refo', 'REF01']);
    openUsersViewAndAssertExists(adminWorkspaceUsersPage, userView, 'Testeur');
    grid.assertExists();
    checkBothGridAndUserViewAreVisible();
    userView.close();
  }

  function openUsersViewAndAssertExists(
    adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
    userView: UserViewPage,
    login: string,
  ) {
    adminWorkspaceUsersPage.selectRowWithMatchingCellContent('login', login);
    userView.assertExists();
  }

  function checkBothGridAndUserViewAreVisible() {
    cy.get('sqtm-app-user-grid')
      .then((grid) => grid.position().left) // get 1st left value
      .then((gridLeftMarginValue) => {
        cy.get('sqtm-app-user-view-with-grid')
          .then((userView) => userView.position().left) // get 2nd left value
          .then((userViewLeftMarginValue) => {
            expect(gridLeftMarginValue).to.be.lessThan(userViewLeftMarginValue);
          });
      });
  }

  function searchByFirstName(grid: GridElement) {
    grid.clearSearchField('input-filter');
    searchAndCheckCountAndExpectedResults(grid, ['b'], 4, ['User 1', 'User 2', 'REF01', 'admin']);
    searchAndCheckCountAndExpectedResults(grid, ['o'], 2, ['User 1', 'REF01']);
    searchAndCheckCountAndExpectedResults(grid, ['{backspace}', 'r'], 1, ['User 2']);
    doMultiCharacterSearchInUserGrid(grid, ['u', 'n', 'i']);
    grid.assertGridIsEmpty();
  }

  function searchByName(grid: GridElement) {
    grid.clearSearchField('input-filter');
    searchAndCheckCountAndExpectedResults(grid, ['c'], 1, ['Justin refo']);
    grid.clearSearchField('input-filter');
    cy.wait(1000);
    searchAndCheckCountAndExpectedResults(grid, ['ç'], 1, ['Testeur']);
  }

  function searchByEmail(grid: GridElement) {
    grid.clearSearchField('input-filter');
    searchAndCheckCountAndExpectedResults(grid, ['r'], 6);
    searchAndCheckCountAndExpectedResults(grid, ['i', 'e', '-', 'a'], 2, ['User 1', 'REF01']);
    searchAndCheckCountAndExpectedResults(grid, ['@'], 1, ['REF01']);
  }

  function searchByCreatedBy(grid: GridElement) {
    grid.clearSearchField('input-filter');
    searchAndCheckCountAndExpectedResults(grid, ['r', 'e', 'f'], 5, [
      'User 2',
      'Testeur',
      'Justin refo',
      'User 1',
      'REF01',
    ]);
    grid.clearSearchField('input-filter');
    searchAndCheckCountAndExpectedResults(grid, [' ', 'r', 'e', 'f'], 2, ['User 1', 'Justin refo']);
  }

  function doMultiCharacterSearchInUserGrid(grid: GridElement, input: string[]) {
    input.forEach((char) => {
      grid.searchInGrid('user-filter-field', char);
    });
  }

  function searchAndCheckCountAndExpectedResults(
    grid: GridElement,
    input: string[],
    expectedCount: number,
    expectedResults?: string[],
  ) {
    doMultiCharacterSearchInUserGrid(grid, input);
    grid.assertRowCount(expectedCount);
    if (expectedResults != null) {
      grid.assertGridColumnContains('login', expectedResults);
    }
  }

  function initTestData() {
    return new PrerequisiteBuilder()
      .withUsers([
        new UserBuilder('User 1')
          .withFirstName('Marie-Anne')
          .withLastName('Dubois')
          .withEmail('mdubois@test.fr')
          .withCreatedBy('admin ref'),
        new UserBuilder('User 2')
          .withFirstName('Bruno')
          .withLastName('Pireffit')
          .withEmail('user@mail.fr')
          .withCreatedBy('admin'),
        new UserBuilder('Testeur')
          .withFirstName('Aurore')
          .withLastName('François')
          .withEmail('aurorefran@mail.fr')
          .withCreatedBy('admin'),
        new UserBuilder('Justin refo')
          .withLastName('Testeur')
          .withEmail('marco@mail.fr')
          .withCreatedBy('admin ref'),
        new UserBuilder('REF01')
          .withFirstName('Testeur')
          .withEmail('userie-a@post.fr')
          .withCreatedBy('Bobtest'),
      ])
      .build();
  }
});
