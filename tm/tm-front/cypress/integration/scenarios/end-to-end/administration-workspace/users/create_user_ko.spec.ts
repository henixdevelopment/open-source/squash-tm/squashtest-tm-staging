import { DatabaseUtils } from '../../../../utils/database.utils';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { AdminWorkspaceUsersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import { CreateUserDialog } from '../../../../page-objects/pages/administration-workspace/dialogs/create-user-dialog.element';
import { AdminE2eCommands } from '../../../../page-objects/scenarios-parts/administration/admin_e2e_commands';
import {
  EMPTY_FIELD_ERROR,
  INSUFFICIENT_CHARS_IN_PASSWORD_ERROR,
  LOGIN_ALREADY_USED_ERROR,
  NOT_MATCHING_PASSWORD_ERROR,
} from '../../../../page-objects/elements/forms/error.message';
import { AdminWorkspaceItem } from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';

describe('Create User KO', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    AdminE2eCommands.createUserFromLogin('Login0');
  });

  it('F01-UC0101.CT02 - should not create one or several users', () => {
    cy.log('Step 1');
    cy.logInAs('admin', 'admin');
    const adminWorkspaceUsersPage = NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>(
      AdminWorkspaceItem.USERS,
    );

    cy.log('Step 2');
    const createUserDialog: CreateUserDialog = adminWorkspaceUsersPage.openCreateUser();
    createUserDialog.assertExists();

    cy.log('Step 3');
    checkEmptyLoginError(createUserDialog);

    cy.log('Step 4');
    checkEmptyLastNameError(createUserDialog);

    cy.log('Step 5');
    checkInsufficientCharsInPasswordError(createUserDialog);

    cy.log('Step 6');
    checkNotMatchingPasswordError(createUserDialog);

    cy.log('Step 7');
    checkAllErrors(createUserDialog);

    cy.log('Step 8');
    checkLoginAlreadyExistsErrorAndCountAllUsers(createUserDialog, adminWorkspaceUsersPage);
  });

  function checkEmptyLoginError(createUserDialog: CreateUserDialog) {
    createUserDialog.fillUser('', '', 'nom1', '', 'Utilisateur', 'azerty3', 'azerty3');
    createUserDialog.addWithClientSideFailure();
    createUserDialog.assertLoginFieldHasError(EMPTY_FIELD_ERROR);
  }

  function checkEmptyLastNameError(createUserDialog: CreateUserDialog) {
    createUserDialog.fillLogin('Login1');
    createUserDialog.fillName('');
    createUserDialog.addWithClientSideFailure();
    createUserDialog.assertLastNameFieldHasError(EMPTY_FIELD_ERROR);
  }

  function checkInsufficientCharsInPasswordError(createUserDialog: CreateUserDialog) {
    createUserDialog.fillPassword('azert');
    createUserDialog.fillConfirmPassword('azert');
    createUserDialog.fillName('Nom0');
    createUserDialog.addWithClientSideFailure();
    createUserDialog.assertPasswordFieldHasError(INSUFFICIENT_CHARS_IN_PASSWORD_ERROR);
  }

  function checkNotMatchingPasswordError(createUserDialog: CreateUserDialog) {
    createUserDialog.fillPassword('azerty');
    createUserDialog.fillConfirmPassword('aze');
    createUserDialog.addWithClientSideFailure();
    createUserDialog.assertPasswordConfirmationFieldHasError(NOT_MATCHING_PASSWORD_ERROR);
  }

  function checkAllErrors(createUserDialog: CreateUserDialog) {
    createUserDialog.fillLogin('');
    createUserDialog.fillName('');
    createUserDialog.fillPassword('azert');
    createUserDialog.addWithClientSideFailure();
    createUserDialog.assertLoginFieldHasError(EMPTY_FIELD_ERROR);
    createUserDialog.assertLastNameFieldHasError(EMPTY_FIELD_ERROR);
    createUserDialog.assertPasswordFieldHasError(INSUFFICIENT_CHARS_IN_PASSWORD_ERROR);
    createUserDialog.assertPasswordConfirmationFieldHasError(NOT_MATCHING_PASSWORD_ERROR);
  }

  function checkLoginAlreadyExistsErrorAndCountAllUsers(
    createUserDialog: CreateUserDialog,
    adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
  ) {
    createUserDialog.fillLogin('Login0');
    createUserDialog.fillName('Nom0');
    createUserDialog.fillPassword('azerty3');
    createUserDialog.fillConfirmPassword('azerty3');
    createUserDialog.addWithClientSideFailure();
    createUserDialog.assertLoginFieldHasError(LOGIN_ALREADY_USED_ERROR);
    createUserDialog.cancel();
    adminWorkspaceUsersPage.grid.assertRowCount(2);
  }
});
