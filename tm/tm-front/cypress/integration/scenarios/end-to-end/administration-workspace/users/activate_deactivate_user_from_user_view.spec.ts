import { DatabaseUtils } from '../../../../utils/database.utils';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { AdminWorkspaceUsersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import {
  AdminWorkspaceItem,
  NavBarAdminElement,
} from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import { ProjectViewPage } from '../../../../page-objects/pages/administration-workspace/project-view/project-view.page';
import { TeamViewPage } from '../../../../page-objects/pages/administration-workspace/team-view/team-view.page';
import { AdministrationWorkspacePage } from '../../../../page-objects/pages/administration-workspace/administration-workspace.page';
import { GroupedMultiListElement } from '../../../../page-objects/elements/filters/grouped-multi-list.element';
import { CampaignViewPage } from '../../../../page-objects/pages/campaign-workspace/campaign/campaign-view.page';
import { IterationViewPage } from '../../../../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import { TestSuiteViewPage } from '../../../../page-objects/pages/campaign-workspace/test-suite/test-suite-view.page';
import { ListPanelElement } from '../../../../page-objects/elements/filters/list-panel.element';
import { LoginPage } from '../../../../page-objects/pages/login/login-page';
import { LOGIN_ALREADY_USED_ERROR } from '../../../../page-objects/elements/forms/error.message';
import { AdminWorkspaceProjectsPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-projects.page';
import {
  changeStatusToActiveWithUserViewAndCheck,
  changeStatusToInactiveWithUserViewAndCheck,
  UserViewPage,
} from '../../../../page-objects/pages/administration-workspace/user-view/user-view.page';
import {
  buildTestPlanProperties,
  buildUserProperties,
} from '../../../scenario-parts/campaign.part';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  TestCaseBuilder,
  ActionTestStepBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CampaignBuilder,
  CampaignTestPlanItemBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
  TestSuiteBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  CampaignObjectProperties,
  checkTestCasesAssignmentBasedOnUserActivation,
} from '../../../scenario-parts/user-status.part';
import {
  TeamBuilder,
  UserBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addPermissionToProject,
  ApiPermissionGroup,
} from '../../../../utils/end-to-end/api/add-permissions';

describe('Activate/deactivate user from user view', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
    cy.logInAs('admin', 'admin');
  });

  it('FC01-UC0102.CT05 - should activate/deactivate a user from the user view', () => {
    cy.log('Step1');
    const adminWorkspaceUsersPage = NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>(
      AdminWorkspaceItem.USERS,
    );
    adminWorkspaceUsersPage.selectUserByLogin('ctouzet');
    const userViewPage = new UserViewPage();
    userViewPage.assertExists();

    cy.log('Step2');
    changeStatusToInactiveWithUserViewAndCheck(userViewPage, adminWorkspaceUsersPage, 'ctouzet');

    cy.log('Step3');
    const adminWorkspaceProjectsPage =
      NavBarAdminElement.navigateToAdministration<AdminWorkspaceProjectsPage>(
        AdminWorkspaceItem.PROJECTS,
      );
    const projectView = new ProjectViewPage();
    checkUserPermissionsAfterDeactivation(
      adminWorkspaceUsersPage,
      adminWorkspaceProjectsPage,
      projectView,
    );

    cy.log('Step4');
    const campaignViewPage = new CampaignViewPage();
    const iterationViewPage = new IterationViewPage();
    const testSuiteViewPage = new TestSuiteViewPage();
    const panelList = new ListPanelElement();
    checkUserIsPresentInTestPlans(
      campaignViewPage,
      panelList,
      iterationViewPage,
      testSuiteViewPage,
      false,
      false,
    );

    cy.log('Step5');
    checkUserInactiveCannotLoginThenLoginAsAdmin(adminWorkspaceUsersPage);

    cy.log('Step6');
    checkCreateUserWithInactiveLoginFails(adminWorkspaceUsersPage);

    cy.log('Step7');
    changeStatusToActiveWithUserViewAndCheck(userViewPage, adminWorkspaceUsersPage, 'ctouzet');

    cy.log('Step8');
    checkUserIsPresentInTestPlans(
      campaignViewPage,
      panelList,
      iterationViewPage,
      testSuiteViewPage,
      true,
      true,
    );
  });

  function initTestData() {
    return new PrerequisiteBuilder()
      .withUsers([
        new UserBuilder('ctouzet')
          .withFirstName('Corinne')
          .withLastName('Touzet')
          .withGroupName('User'),
      ])
      .withTeams([
        new TeamBuilder('team_rocket').withUserLogins(['ctouzet']),
        new TeamBuilder('team_jacket'),
      ])
      .withProjects([
        new ProjectBuilder('Project 001')
          .withTestCaseLibraryNodes([
            new TestCaseBuilder('TC 001').withSteps([
              new ActionTestStepBuilder('Je crie Expecto Patronum.', 'Mon patronus apparaît.'),
            ]),
            new TestCaseBuilder('TC 002'),
          ])
          .withCampaignLibraryNodes([
            new CampaignBuilder('Campaign 001')
              .withTestPlanItems([
                new CampaignTestPlanItemBuilder('TC 001').withUserLogin('ctouzet'),
                new CampaignTestPlanItemBuilder('TC 002'),
              ])
              .withIterations([
                new IterationBuilder('Iteration 001')
                  .withTestPlanItems([
                    new IterationTestPlanItemBuilder('TC 001').withUserLogin('ctouzet'),
                    new IterationTestPlanItemBuilder('TC 002'),
                  ])
                  .withTestSuites([
                    new TestSuiteBuilder('Suite 001').withTestCaseNames(['TC 001', 'TC 002']),
                  ]),
              ]),
          ]),
        new ProjectBuilder('Project 002'),
      ])
      .build();
  }
});
function initPermissions() {
  addPermissionToProject(1, ApiPermissionGroup.PROJECT_MANAGER, [2]);
}
function checkUserPermissionsAfterDeactivation(
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
  adminWorkspaceProjectsPage: AdminWorkspaceProjectsPage,
  projectView: ProjectViewPage,
) {
  checkPresenceAndStyleOnPermissionPanel(adminWorkspaceProjectsPage, projectView);
  checkOnAddPermissionDialogIfUserNotPresentInProject(adminWorkspaceProjectsPage, projectView);

  NavBarAdminElement.navigateToAdministration(AdminWorkspaceItem.USERS);
  const adminWorkspaceTeamsPage = adminWorkspaceUsersPage.goToTeamAnchor();
  const teamView = new TeamViewPage();
  checkPresenceAndStyleOnTeamPanel(adminWorkspaceTeamsPage, teamView);
  checkAbsenceOnAddTeamMemberDialog(adminWorkspaceTeamsPage, teamView);
}

function checkPresenceAndStyleOnPermissionPanel(
  adminWorkspaceProjectsPage: AdministrationWorkspacePage,
  projectView: ProjectViewPage,
) {
  adminWorkspaceProjectsPage.grid.selectRowWithMatchingCellContent('name', 'Project 001');
  projectView.assertExists();

  projectView.permissionsPanel.grid
    .findRowId('partyName', 'Corinne Touzet (ctouzet)')
    .then((partyID) => {
      const permissionGroupCell = projectView.permissionsPanel.grid
        .getCell(partyID, 'permissionGroup')
        .textRenderer();
      permissionGroupCell.assertContainsText('Chef de projet');
      permissionGroupCell.findCellTextHTMLElement('div').should('have.class', 'disabled-row');
    });
}

function checkOnAddPermissionDialogIfUserNotPresentInProject(
  adminWorkspaceProjectsPage: AdministrationWorkspacePage,
  projectView: ProjectViewPage,
) {
  adminWorkspaceProjectsPage.grid.selectRowWithMatchingCellContent('name', 'Project 002');
  const addPermissionDialog = projectView.permissionsPanel.clickOnAddUserPermissionButton();
  addPermissionDialog.assertExists();
  addPermissionDialog.partyListContains('ctouzet');
  addPermissionDialog.escapeToCloseDialogAndAssertNotExist();
}

function checkPresenceAndStyleOnTeamPanel(
  adminWorkspaceTeamsPage: AdministrationWorkspacePage,
  teamView: TeamViewPage,
) {
  adminWorkspaceTeamsPage.grid.selectRowWithMatchingCellContent('name', 'team_rocket');
  teamView.teamMembersPanel.grid.findRowId('fullName', 'ctouzet').then((userID) => {
    const fullNameCell = teamView.teamMembersPanel.grid.getCell(userID, 'fullName');
    fullNameCell.linkRenderer().assertDisabled();
    fullNameCell.textRenderer().findCellTextHTMLElement('a').should('have.class', 'disabled-row');
  });
}

function checkAbsenceOnAddTeamMemberDialog(
  adminWorkspaceTeamsPage: AdministrationWorkspacePage,
  teamView: TeamViewPage,
) {
  const addTeamMemberDialog = teamView.teamMembersPanel.clickOnAddTeamMemberButton();
  const multiList = new GroupedMultiListElement();
  addTeamMemberDialog.assertExists();
  addTeamMemberDialog.assertMemberNotExist('ctouzet', multiList);
  cy.clickOnOverlayBackdrop();
  addTeamMemberDialog.cancelAndAssertNotExist();
}

function checkUserIsPresentInTestPlans(
  campaignViewPage: CampaignViewPage,
  panelList: ListPanelElement,
  iterationViewPage: IterationViewPage,
  testSuiteViewPage: TestSuiteViewPage,
  areNodesOpenInCampaignTree: boolean,
  userActive: boolean,
) {
  NavBarAdminElement.close();
  const campaignWorkspacePage = NavBarElement.navigateToCampaignWorkspace();
  campaignWorkspacePage.assertExists();

  const campaignProperties: CampaignObjectProperties = {
    viewPage: campaignViewPage,
    testPlanProperties: buildTestPlanProperties('Project 001', 'Campaign 001', 'TC 001', 'TC 002'),
  };
  const iterationProperties: CampaignObjectProperties = {
    viewPage: iterationViewPage,
    testPlanProperties: buildTestPlanProperties(
      'Campaign 001',
      'Iteration 001',
      'TC 001',
      'TC 002',
    ),
  };
  const testSuiteProperties: CampaignObjectProperties = {
    viewPage: testSuiteViewPage,
    testPlanProperties: buildTestPlanProperties('Iteration 001', 'Suite 001', 'TC 001', 'TC 002'),
  };
  const userProperties = buildUserProperties(userActive, 'Corinne Touzet', 'ctouzet');

  checkTestCasesAssignmentBasedOnUserActivation(
    {
      campaignWorkspacePage,
      campaignProperties,
      iterationProperties,
      testSuiteProperties,
      panelList,
      userProperties,
    },
    areNodesOpenInCampaignTree,
  );
}

function checkUserInactiveCannotLoginThenLoginAsAdmin(
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
) {
  cy.logOut();
  const loginPage = LoginPage.navigateTo();
  loginPage.assertExists();
  loginPage.assertLoginFails('ctouzet', 'admin');
  cy.logInAs('admin', 'admin');
  NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>(AdminWorkspaceItem.USERS);
  adminWorkspaceUsersPage.selectUserByLogin('ctouzet');
}

function checkCreateUserWithInactiveLoginFails(adminWorkspaceUsersPage: AdminWorkspaceUsersPage) {
  const createUserDialog = adminWorkspaceUsersPage.openCreateUser();
  createUserDialog.fillLogin('ctouzet');
  createUserDialog.fillName('Touzet');
  createUserDialog.fillPassword('mandragore');
  createUserDialog.fillConfirmPassword('mandragore');
  createUserDialog.addWithClientSideFailure();
  createUserDialog.assertLoginFieldHasError(LOGIN_ALREADY_USED_ERROR);
  createUserDialog.cancel();
}
