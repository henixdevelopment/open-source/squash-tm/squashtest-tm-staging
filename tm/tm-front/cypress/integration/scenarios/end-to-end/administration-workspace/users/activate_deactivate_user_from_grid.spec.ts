import { GroupedMultiListElement } from '../../../../page-objects/elements/filters/grouped-multi-list.element';
import { CreateUserDialog } from '../../../../page-objects/pages/administration-workspace/dialogs/create-user-dialog.element';
import { LOGIN_ALREADY_USED_ERROR } from '../../../../page-objects/elements/forms/error.message';
import { UserViewPage } from '../../../../page-objects/pages/administration-workspace/user-view/user-view.page';
import { DatabaseUtils } from '../../../../utils/database.utils';
import { ListPanelElement } from '../../../../page-objects/elements/filters/list-panel.element';
import {
  AdminWorkspaceItem,
  NavBarAdminElement,
} from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import { ProjectViewPage } from '../../../../page-objects/pages/administration-workspace/project-view/project-view.page';
import { IterationViewPage } from '../../../../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import { TestSuiteViewPage } from '../../../../page-objects/pages/campaign-workspace/test-suite/test-suite-view.page';
import { CampaignViewPage } from '../../../../page-objects/pages/campaign-workspace/campaign/campaign-view.page';
import { AdministrationWorkspacePage } from '../../../../page-objects/pages/administration-workspace/administration-workspace.page';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { AdminWorkspaceUsersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import { TeamViewPage } from '../../../../page-objects/pages/administration-workspace/team-view/team-view.page';
import {
  buildTestPlanProperties,
  buildUserProperties,
} from '../../../scenario-parts/campaign.part';
import {
  CampaignObjectProperties,
  changeStatusToInactiveInGridAndCheck,
  checkTestCasesAssignmentBasedOnUserActivation,
} from '../../../scenario-parts/user-status.part';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { TestCaseBuilder } from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CampaignBuilder,
  CampaignTestPlanItemBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
  TestSuiteBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  TeamBuilder,
  UserBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addPermissionToProject,
  ApiPermissionGroup,
} from '../../../../utils/end-to-end/api/add-permissions';

describe('Activate/deactivate user from grid', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC0101.CT03a - should activate/deactivate a user from grid', () => {
    cy.log('Step 1');
    cy.logInAs('lclure', 'admin');
    const adminWorkspaceUsersPage = NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>(
      AdminWorkspaceItem.USERS,
    );

    cy.log('Step 2');
    const userView = new UserViewPage();
    changeStatusToInactiveInGridAndCheck(adminWorkspaceUsersPage, userView, 'gmansoif');

    cy.log('Step 3');
    checkUserIsPresentInProjectAndTeamViews(adminWorkspaceUsersPage);

    cy.log('Step 4');
    const campaignViewPage = new CampaignViewPage();
    const iterationViewPage = new IterationViewPage();
    const testSuiteViewPage = new TestSuiteViewPage();
    const panelList = new ListPanelElement();

    checkUserIsPresentInTestPlans(
      campaignViewPage,
      panelList,
      iterationViewPage,
      testSuiteViewPage,
      false,
      false,
    );

    cy.log('Step 5');
    checkUserAlreadyExists(adminWorkspaceUsersPage);

    cy.log('Step 6');
    changeStatusToActiveInGridAndCheck(adminWorkspaceUsersPage, userView);

    cy.log('Step 7');
    checkUserIsPresentInTestPlans(
      campaignViewPage,
      panelList,
      iterationViewPage,
      testSuiteViewPage,
      true,
      true,
    );
  });

  function initTestData() {
    return new PrerequisiteBuilder()
      .withUsers([
        new UserBuilder('gmansoif').withFirstName('Gérard').withLastName('Mansoif'),
        new UserBuilder('abolisant').withFirstName('Anna').withLastName('Bolisant'),
        new UserBuilder('ecoptere').withFirstName('Élie').withLastName('Coptère'),
        new UserBuilder('jcelaire').withFirstName('Jacques').withLastName('Célaire'),
        new UserBuilder('bhaba').withFirstName('Barth').withLastName('Haba').withActive(false),
        new UserBuilder('lclure')
          .withFirstName('Lara')
          .withLastName('Clure')
          .withGroupName('Admin'),
      ])
      .withTeams([
        new TeamBuilder('monequipe').withUserLogins(['gmansoif']),
        new TeamBuilder('secondequipe'),
      ])
      .withProjects([
        new ProjectBuilder('AZERTY')
          .withTestCaseLibraryNodes([
            new TestCaseBuilder('TC 001'),
            new TestCaseBuilder('TC 002'),
            new TestCaseBuilder('TC 003'),
          ])
          .withCampaignLibraryNodes([
            new CampaignBuilder('Campaign 001')
              .withTestPlanItems([
                new CampaignTestPlanItemBuilder('TC 001'),
                new CampaignTestPlanItemBuilder('TC 002').withUserLogin('gmansoif'),
                new CampaignTestPlanItemBuilder('TC 003'),
              ])
              .withIterations([
                new IterationBuilder('Iteration 001')
                  .withTestPlanItems([
                    new IterationTestPlanItemBuilder('TC 001'),
                    new IterationTestPlanItemBuilder('TC 002').withUserLogin('gmansoif'),
                    new IterationTestPlanItemBuilder('TC 003'),
                  ])
                  .withTestSuites([
                    new TestSuiteBuilder('Suite 001').withTestCaseNames([
                      'TC 001',
                      'TC 002',
                      'TC 003',
                    ]),
                  ]),
              ]),
          ]),
        new ProjectBuilder('QSDFGH'),
      ])
      .build();
  }
});
function initPermissions() {
  addPermissionToProject(1, ApiPermissionGroup.TEST_EDITOR, [2]);
}
function checkUserIsPresentInProjectAndTeamViews(adminWorkspaceUsersPage: AdminWorkspaceUsersPage) {
  const adminWorkspaceProjectsPage = NavBarAdminElement.navigateToAdministration(
    AdminWorkspaceItem.PROJECTS,
  );
  const projectView = new ProjectViewPage();

  checkOnPermissionPanel(adminWorkspaceProjectsPage, projectView);
  checkOnAddPermissionDialog(adminWorkspaceProjectsPage, projectView);

  NavBarAdminElement.navigateToAdministration(AdminWorkspaceItem.USERS);
  const adminWorkspaceTeamsPage = adminWorkspaceUsersPage.goToTeamAnchor();
  const teamView = new TeamViewPage();

  checkOnTeamPanel(adminWorkspaceTeamsPage, teamView);
  checkOnAddTeamDialog(adminWorkspaceTeamsPage, teamView);
}

function checkUserIsPresentInTestPlans(
  campaignViewPage: CampaignViewPage,
  panelList: ListPanelElement,
  iterationViewPage: IterationViewPage,
  testSuiteViewPage: TestSuiteViewPage,
  areNodesOpenInCampaignTree: boolean,
  userActive: boolean,
) {
  NavBarAdminElement.close();
  const campaignWorkspacePage = NavBarElement.navigateToCampaignWorkspace();
  campaignWorkspacePage.assertExists();

  const campaignProperties: CampaignObjectProperties = {
    viewPage: campaignViewPage,
    testPlanProperties: buildTestPlanProperties('AZERTY', 'Campaign 001', 'TC 002', 'TC 001'),
  };
  const iterationProperties: CampaignObjectProperties = {
    viewPage: iterationViewPage,
    testPlanProperties: buildTestPlanProperties(
      'Campaign 001',
      'Iteration 001',
      'TC 002',
      'TC 001',
    ),
  };
  const testSuiteProperties: CampaignObjectProperties = {
    viewPage: testSuiteViewPage,
    testPlanProperties: buildTestPlanProperties('Iteration 001', 'Suite 001', 'TC 002', 'TC 001'),
  };
  const userProperties = buildUserProperties(userActive, 'Gérard Mansoif', 'gmansoif');

  checkTestCasesAssignmentBasedOnUserActivation(
    {
      campaignWorkspacePage,
      campaignProperties,
      iterationProperties,
      testSuiteProperties,
      panelList,
      userProperties,
    },
    areNodesOpenInCampaignTree,
  );
}

function checkUserAlreadyExists(adminWorkspaceUsersPage: AdminWorkspaceUsersPage) {
  NavBarElement.navigateToAdministration('users');
  const createUserDialog: CreateUserDialog = adminWorkspaceUsersPage.openCreateUser();
  checkLoginAlreadyExistsError(createUserDialog);
}

export function changeStatusToActiveInGridAndCheck(
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
  userView: UserViewPage,
) {
  adminWorkspaceUsersPage.grid.findRowId('login', 'gmansoif').then((userID) => {
    adminWorkspaceUsersPage.grid
      .getCell(userID, 'active')
      .iconRenderer()
      .assertContainsIcon('inactive');
    adminWorkspaceUsersPage.grid.activateUserAndWaitResponse(userID);
    adminWorkspaceUsersPage.grid
      .getCell(userID, 'active')
      .iconRenderer()
      .assertContainsIcon('active');
  });

  adminWorkspaceUsersPage.selectRowWithMatchingCellContent('login', 'gmansoif');
  userView.assertExists();
  userView.activeSwitch.checkValue(true);
}

function checkOnPermissionPanel(
  adminWorkspaceProjectsPage: AdministrationWorkspacePage,
  projectView: ProjectViewPage,
) {
  adminWorkspaceProjectsPage.grid.selectRowWithMatchingCellContent('name', 'AZERTY');

  projectView.assertExists();
  projectView.permissionsPanel.grid
    .findRowId('partyName', 'Gérard Mansoif (gmansoif)')
    .then((partyID) => {
      projectView.permissionsPanel.grid
        .getCell(partyID, 'permissionGroup')
        .textRenderer()
        .assertContainsText('Testeur référent');
    });
}

function checkOnAddPermissionDialog(
  adminWorkspaceProjectsPage: AdministrationWorkspacePage,
  projectView: ProjectViewPage,
) {
  adminWorkspaceProjectsPage.grid.selectRowWithMatchingCellContent('name', 'QSDFGH');
  const addPermissionDialog = projectView.permissionsPanel.clickOnAddUserPermissionButton();
  addPermissionDialog.assertExists();
  addPermissionDialog.partyListContains('gmansoif');
  addPermissionDialog.escapeToCloseDialogAndAssertNotExist();
}

function checkOnTeamPanel(
  adminWorkspaceTeamsPage: AdministrationWorkspacePage,
  teamView: TeamViewPage,
) {
  adminWorkspaceTeamsPage.grid.selectRowWithMatchingCellContent('name', 'monequipe');

  teamView.teamMembersPanel.grid.findRowId('fullName', 'gmansoif').then((userID) => {
    teamView.teamMembersPanel.grid.getCell(userID, 'fullName').linkRenderer().assertDisabled();
  });
}

function checkOnAddTeamDialog(
  adminWorkspaceTeamsPage: AdministrationWorkspacePage,
  teamView: TeamViewPage,
) {
  const addTeamMemberDialog = teamView.teamMembersPanel.clickOnAddTeamMemberButton();
  const multiList = new GroupedMultiListElement();
  addTeamMemberDialog.assertExists();
  addTeamMemberDialog.assertMemberNotExist('gmansoif', multiList);
  cy.clickOnOverlayBackdrop();
  addTeamMemberDialog.cancelAndAssertNotExist();
}

function checkLoginAlreadyExistsError(createUserDialog: CreateUserDialog) {
  createUserDialog.assertExists();
  createUserDialog.fillUser('gmansoif', '', 'Mansoif', '', 'Utilisateur', 'azerty', 'azerty');
  createUserDialog.addWithClientSideFailure();
  createUserDialog.assertLoginFieldHasError(LOGIN_ALREADY_USED_ERROR);
  createUserDialog.cancel();
}
