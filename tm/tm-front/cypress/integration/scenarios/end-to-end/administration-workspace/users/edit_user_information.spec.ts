import { DatabaseUtils } from '../../../../utils/database.utils';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { AdminWorkspaceUsersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import { UserViewPage } from '../../../../page-objects/pages/administration-workspace/user-view/user-view.page';
import { selectByDataTestNavbarFieldId } from '../../../../utils/basic-selectors';
import { ResetPasswordDialog } from '../../../../page-objects/pages/administration-workspace/user-view/dialogs/reset-password-dialog.dialog';
import { LoginPage } from '../../../../page-objects/pages/login/login-page';
import { MenuElement } from '../../../../utils/menu.element';
import { UserGridElement } from '../../../../page-objects/elements/grid/grid.element';
import { EditableTextFieldElement } from '../../../../page-objects/elements/forms/editable-text-field.element';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import {
  INSUFFICIENT_CHARS_IN_PASSWORD_ERROR,
  NOT_MATCHING_PASSWORD_ERROR,
} from '../../../../page-objects/elements/forms/error.message';
import { AdminWorkspaceItem } from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addPermissionToProject,
  ApiPermissionGroup,
} from '../../../../utils/end-to-end/api/add-permissions';

describe('Edit user information', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC0102.CT04 - Edit user information', () => {
    cy.log('Step 1');
    cy.logInAs('admin', 'admin');
    const adminWorkspaceUsersPage = NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>(
      AdminWorkspaceItem.USERS,
    );
    const userView = new UserViewPage();
    const grid = adminWorkspaceUsersPage.grid;

    openUsersViewAndAssertExists(adminWorkspaceUsersPage, userView, 'Cat Lady');
    grid.assertExists();

    cy.log('Step 2');
    editPasswordThenCancelChanges(userView);

    cy.log('Step 3');
    editPasswordThenConfirmChanges(adminWorkspaceUsersPage, userView, grid);

    cy.log('Step 4');
    editFirstName(adminWorkspaceUsersPage, userView, grid);

    cy.log('Step 5');
    editLastName(adminWorkspaceUsersPage, userView, grid);

    cy.log('Step 6');
    editEmail(adminWorkspaceUsersPage, userView, grid);

    cy.log('Step 7');
    editGroup(userView, grid);

    cy.log('Step 8');
    verifyUserPermissionsAfterGroupChange();
  });

  function openUsersViewAndAssertExists(
    adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
    userView: UserViewPage,
    login: string,
  ) {
    adminWorkspaceUsersPage.selectRowWithMatchingCellContent('login', login);
    userView.assertExists();
  }

  function editPasswordThenCancelChanges(userView: UserViewPage) {
    // NOT IMPLEMENTED : check on search field style on hover not implemented because Cypress does not currently
    // offer any means to handle css :hover pseudo classes
    const resetPasswordDialog: ResetPasswordDialog = userView.openResetPasswordDialog();
    resetPasswordDialog.assertExists();

    checkResetPasswordDialogConformity(resetPasswordDialog);

    editPasswordThenCancel(resetPasswordDialog, 'Dizzy Leaper');
    userView.close();

    cy.logOut();
    const loginPage = LoginPage.navigateTo();
    loginPage.assertExists();
    loginPage.assertLoginFails('Cat Lady', 'Dizzy Leaper');
    loginPage.assertLoginFailedWarningIsVisible();
  }

  function checkResetPasswordDialogConformity(resetPasswordDialog: ResetPasswordDialog) {
    resetPasswordDialog.checkDialogTitleContent('Réinitialiser le mot de passe');
    resetPasswordDialog.password.assertExists();
    resetPasswordDialog.confirmPassword.assertExists();
    resetPasswordDialog.assertButtonExists('confirm');
    resetPasswordDialog.assertButtonExists('cancel');
  }

  function editPasswordThenCancel(resetPasswordDialog: ResetPasswordDialog, passwordInput: string) {
    resetPasswordDialog.fillPassword(passwordInput);
    resetPasswordDialog.fillConfirmPassword(passwordInput);
    resetPasswordDialog.cancel();
    resetPasswordDialog.assertNotExist();
  }

  function editPasswordThenConfirmChanges(
    adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
    userView: UserViewPage,
    grid: UserGridElement,
  ) {
    cy.logInAs('admin', 'admin');
    navigateToAdministrationUsersAndOpenUserView(adminWorkspaceUsersPage, userView, grid);

    const resetPasswordDialog: ResetPasswordDialog = userView.openResetPasswordDialog();
    resetPasswordDialog.assertExists();

    setAndConfirmPassword(resetPasswordDialog, 'Nutty', 'Nutty');
    resetPasswordDialog.password.assertErrorExist('sqtm-core.validation.errors.passwordLength');
    resetPasswordDialog.password.assertErrorContains(INSUFFICIENT_CHARS_IN_PASSWORD_ERROR);

    setAndConfirmPassword(resetPasswordDialog, 'Scandalous Nuttypants', 'Nuttypants Scandalous');
    resetPasswordDialog.confirmPassword.assertErrorExist(
      'sqtm-core.validation.errors.passwordsNotMatching',
    );
    resetPasswordDialog.confirmPassword.assertErrorContains(NOT_MATCHING_PASSWORD_ERROR);

    resetPasswordDialog.fillPassword('Scandalous Nuttypants');
    resetPasswordDialog.fillConfirmPassword('Scandalous Nuttypants');
    resetPasswordDialog.clickOnConfirmButton();
    resetPasswordDialog.assertNotExist();

    userView.close();
    cy.logOut();

    cy.logInAs('Cat Lady', 'Scandalous Nuttypants');
    cy.get(selectByDataTestNavbarFieldId('user-menu')).should('be.visible');
    cy.logOut();

    cy.logInAs('admin', 'admin');
    NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>(AdminWorkspaceItem.USERS);
    openUsersViewAndAssertExists(adminWorkspaceUsersPage, userView, 'Cat Lady');
    grid.assertExists();

    checkLastConnectionDate(userView, grid);
  }

  function setAndConfirmPassword(
    resetPasswordDialog: ResetPasswordDialog,
    input: string,
    confirmationInput: string,
  ) {
    resetPasswordDialog.fillPassword(input);
    resetPasswordDialog.fillConfirmPassword(confirmationInput);
    resetPasswordDialog.clickOnConfirmButton();
  }

  function checkLastConnectionDate(userView: UserViewPage, grid: UserGridElement) {
    userView.checkLastConnectionDateHasValue();

    grid.findRowId('login', 'Cat Lady').then((id) => {
      grid.getCell(id, 'lastConnectedOn').textRenderer().assertDoesNotContainText('-');
    });
  }

  function editFirstName(
    adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
    userView: UserViewPage,
    grid: UserGridElement,
  ) {
    checkTextFieldEditMode(userView.firstNameTextField);

    clearTextFieldAndCheckPlaceholder(userView.firstNameTextField);

    setTextFieldValueThenCancel(userView.firstNameTextField, 'Decibelle', '');

    checkCellContentInUserGrid(grid, 'firstName', '');

    setAndConfirmTextFieldValueThenCheck(userView.firstNameTextField, 'Sandrine');

    checkCellContentInUserGrid(grid, 'firstName', 'Sandrine');
  }

  function editLastName(
    adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
    userView: UserViewPage,
    grid: UserGridElement,
  ) {
    checkTextFieldEditMode(userView.lastNameTextField);

    setTextFieldValueThenCancel(userView.lastNameTextField, 'Longbottom', 'Cat Lady');

    checkCellContentInUserGrid(grid, 'lastName', 'Cat Lady');

    userView.lastNameTextField.clearTextField();
    userView.lastNameTextField.assertHasError('Ce champ ne peut pas être vide.');

    setAndConfirmTextFieldValueThenCheck(userView.lastNameTextField, 'Roulier');

    checkCellContentInUserGrid(grid, 'lastName', 'Roulier');
  }

  function editEmail(
    adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
    userView: UserViewPage,
    grid: UserGridElement,
  ) {
    checkTextFieldEditMode(userView.emailTextField);

    clearTextFieldAndCheckPlaceholder(userView.emailTextField);

    userView.emailTextField.setAndConfirmValue('Cat Lady@squashmail.fr');
    userView.emailTextField.checkContent('Cat Lady@squashmail.fr');
    setTextFieldValueThenCancel(
      userView.emailTextField,
      'me@catlady.com',
      'Cat Lady@squashmail.fr',
    );

    checkCellContentInUserGrid(grid, 'email', 'Cat Lady@squashmail.fr');

    setAndConfirmTextFieldValueThenCheck(userView.emailTextField, 'mittens@floof.com');

    checkCellContentInUserGrid(grid, 'email', 'mittens@floof.com');
  }

  function editGroup(userView: UserViewPage, grid: UserGridElement) {
    userView.groupSelectField.assertExists();
    userView.groupSelectField.assertIsEditable();
    // NOT IMPLEMENTED : check on search field style on hover not implemented because Cypress does not currently
    // offer any means to handle css :hover pseudo classes
    userView.groupSelectField.checkAllOptions([
      'Administrateur',
      "Serveur d'automatisation de tests",
      'Utilisateur',
    ]);
    userView.groupSelectField.selectValueNoButton('Administrateur');
    userView.groupSelectField.checkSelectedOption('Administrateur');

    checkCellContentInUserGrid(grid, 'userGroup', 'Administrateur');
  }

  function checkCellContentInUserGrid(
    grid: UserGridElement,
    cellId: string,
    expectedCellContent: string,
  ) {
    grid.findRowId('login', 'Cat Lady').then((id) => {
      grid.getCell(id, cellId).textRenderer().assertContainsText(expectedCellContent);
    });
  }

  function verifyUserPermissionsAfterGroupChange() {
    cy.logOut();
    cy.logInAs('Cat Lady', 'Scandalous Nuttypants');
    const administrationSubMenu: MenuElement = NavBarElement.showSubMenu(
      'administration',
      'administration-menu',
    );
    checkAdministrationSubMenuItemIsAvailable(administrationSubMenu, 'users');
    checkAdministrationSubMenuItemIsAvailable(administrationSubMenu, 'projects');
    checkAdministrationSubMenuItemIsAvailable(administrationSubMenu, 'milestones');
    checkAdministrationSubMenuItemIsAvailable(administrationSubMenu, 'entities-customization');
    checkAdministrationSubMenuItemIsAvailable(administrationSubMenu, 'servers');
    checkAdministrationSubMenuItemIsAvailable(administrationSubMenu, 'system');
  }

  function checkAdministrationSubMenuItemIsAvailable(
    administrationSubMenu: MenuElement,
    subMenuItem: string,
  ) {
    administrationSubMenu.item(subMenuItem).assertExists();
    administrationSubMenu.item(subMenuItem).assertEnabled();
    administrationSubMenu.item(subMenuItem).assertButtonIsActive();
  }

  function navigateToAdministrationUsersAndOpenUserView(
    adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
    userView: UserViewPage,
    grid: UserGridElement,
  ) {
    NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>(AdminWorkspaceItem.USERS);
    openUsersViewAndAssertExists(adminWorkspaceUsersPage, userView, 'Cat Lady');
    grid.assertExists();
  }

  function clearTextFieldAndCheckPlaceholder(textfield: EditableTextFieldElement) {
    textfield.clearTextField();
    textfield.checkEditMode(false);
    textfield.checkPlaceholder();
  }

  function setTextFieldValueThenCancel(
    textfield: EditableTextFieldElement,
    input: string,
    previousValue: string,
  ) {
    textfield.setValue(input);
    textfield.cancel();
    textfield.assertContainsText(previousValue);
  }

  function setAndConfirmTextFieldValueThenCheck(
    textfield: EditableTextFieldElement,
    input: string,
  ) {
    textfield.setAndConfirmValue(input);
    textfield.checkEditMode(false);
    textfield.assertContainsText(input);
  }

  function checkTextFieldEditMode(textfield: EditableTextFieldElement) {
    textfield.click();
    textfield.checkEditMode(true);
    cy.get('body').type('{esc}');
  }

  function initTestData() {
    return new PrerequisiteBuilder()
      .withUsers([new UserBuilder('Cat Lady').withActive(true).withGroupName('User')])
      .withProjects([new ProjectBuilder('AZERTY')])
      .build();
  }
});
function initPermissions() {
  addPermissionToProject(1, ApiPermissionGroup.PROJECT_MANAGER, [2]);
}
