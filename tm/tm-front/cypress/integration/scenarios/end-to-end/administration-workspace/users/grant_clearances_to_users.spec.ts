import { DatabaseUtils } from '../../../../utils/database.utils';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { AdminWorkspaceUsersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { UserViewPage } from '../../../../page-objects/pages/administration-workspace/user-view/user-view.page';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import { ListPanelElement } from '../../../../page-objects/elements/filters/list-panel.element';
import { AddUserAuthorisationsDialog } from '../../../../page-objects/pages/administration-workspace/user-view/dialogs/add-user-authorisations.dialog';
import { AdminWorkspaceItem } from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';

describe('Grant clearances to users', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
  });

  it('F01-UC0101.CT03 - should grant clearances to users', () => {
    cy.log('Step 1');
    cy.logInAs('admin', 'admin');
    const adminWorkspaceUsersPage = NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>(
      AdminWorkspaceItem.USERS,
    );
    const userView = new UserViewPage();
    navigateToUserViewAndAssertViewExists(adminWorkspaceUsersPage, userView);

    cy.log('Step 2');
    userView.anchors.clickLink('authorisations');
    userView.authorisationsPanel.assertIsVisible();

    const authorisationsGrid = userView.authorisationsPanel.grid;
    authorisationsGrid.assertGridIsEmpty();

    const clearancesDialog =
      userView.authorisationsPanel.clickOnAddPermissionButtonAndAssertDialogExists();

    cy.log('Step 3');
    clearancesDialog.checkConformity();

    // For step4: no test on the scrollbar because Cypress does not know how to deal with it currently

    cy.log('Step 4');
    checkProjectFieldAndListPanelConformity(clearancesDialog, projectsNames);

    cy.log('Step 5');
    checkProfileFieldAndListPanelConformity(clearancesDialog);

    cy.log('Step 6');
    clearancesDialog.cancel();
    authorisationsGrid.assertGridIsEmpty();

    cy.log('Step 7');
    addSimplePermissionAndCheck(
      userView,
      projectsNames,
      authorisationsGrid,
      adminWorkspaceUsersPage,
    );

    cy.log('Step 8');
    const profilesAndProjects = [
      { project: projectsNames[4], profile: 'Designer de test' },
      { project: projectsNames[5], profile: 'Invité' },
      { project: projectsNames[6], profile: 'Testeur' },
      { project: projectsNames[7], profile: 'Testeur référent' },
      { project: projectsNames[8], profile: 'Testeur avancé' },
    ];
    addPermissionsInARowAndCheck(userView, projectsNames, authorisationsGrid, profilesAndProjects);

    cy.log('Step 9');
    addMultiplePermissionsWithOneProfileAndCheck(userView, projectsNames, adminWorkspaceUsersPage);

    cy.log('Step 10');
    checkPermissionsDataInGrid(authorisationsGrid, userView);
    checkPermissionsGridPagination(userView);

    cy.log('Step 11');
    userView.authorisationsPanel.grid.gridFooter.goToFirstPage();
    checkListPanelProfilesAndChangeAllProfiles(authorisationsGrid, userView);
  });

  const projectsNames: string[] = [
    'Project 001',
    'Project 002',
    'Project 003',
    'Project 004',
    'Project 005',
    'Project 006',
    'Project 007',
    'Project 008',
    'Project 009',
    'Project 010',
    'Project 011 avec vraiment énormément beaucoup de caractères',
  ];

  function initTestData() {
    return new PrerequisiteBuilder()
      .withUsers([new UserBuilder('jlescaut').withFirstName('Julie').withLastName('Lescaut')])
      .withProjects(projectsNames.map((project) => new ProjectBuilder(project)))
      .build();
  }
});

function navigateToUserViewAndAssertViewExists(
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
  userView: UserViewPage,
) {
  adminWorkspaceUsersPage.grid.findRowId(GridColumnId.login, 'jlescaut');
  adminWorkspaceUsersPage.selectRowWithMatchingCellContent(GridColumnId.login, 'jlescaut');
  userView.assertExists();
}

function checkProjectFieldAndListPanelConformity(
  clearancesDialog: AddUserAuthorisationsDialog,
  projectsNames: string[],
) {
  clearancesDialog.clickOnProjectListAndCheckConformity(projectsNames);
  clearancesDialog.selectProjects(projectsNames[0]);
  clearancesDialog.checkProjectFieldContent(projectsNames[0]);
}

function checkProfileFieldAndListPanelConformity(clearancesDialog: AddUserAuthorisationsDialog) {
  clearancesDialog.clickOnProfileListAndCheckConformity();
  clearancesDialog.selectProfile('Automaticien');
  clearancesDialog.checkProfileFieldContent('Automaticien');
}
function addSimplePermissionAndCheck(
  userView: UserViewPage,
  projectsNames: string[],
  authorisationsGrid: GridElement,
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
) {
  const clearancesDialog =
    userView.authorisationsPanel.clickOnAddPermissionButtonAndAssertDialogExists();

  addOnePermission(clearancesDialog, projectsNames[9], 'Automaticien');
  checkRowContainsClearance(authorisationsGrid, '1', projectsNames[9], 'Automaticien');
  checkPermissionsNumberInUsersGrid(adminWorkspaceUsersPage, '1');
}

function addOnePermission(
  clearancesDialog: AddUserAuthorisationsDialog,
  projectName: string,
  profile: string,
) {
  clearancesDialog.selectProjects(projectName);
  clearancesDialog.selectProfile(profile);
  clearancesDialog.clickOnAddButton();
}

function checkPermissionsNumberInUsersGrid(
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
  expectedNumber: string,
) {
  adminWorkspaceUsersPage.grid.findRowId('login', 'jlescaut').then((login) => {
    adminWorkspaceUsersPage.grid
      .getRow(login)
      .cell(GridColumnId.habilitationCount)
      .textRenderer()
      .assertContainsText(expectedNumber);
  });
}

function addPermissionsInARowAndCheck(
  userView: UserViewPage,
  projectsNames: string[],
  authorisationsGrid: GridElement,
  profilesAndProjects: { project: string; profile: string }[],
) {
  const clearancesDialog =
    userView.authorisationsPanel.clickOnAddPermissionButtonAndAssertDialogExists();

  profilesAndProjects.forEach((profileAndProject) => {
    clearancesDialog.selectProfile(profileAndProject.profile);
    clearancesDialog.selectProjects(profileAndProject.project);
    clearancesDialog.clickOnAddAnotherButton();
    clearancesDialog.checkFieldsHavePlaceHolder();
  });

  addOnePermission(clearancesDialog, projectsNames[3], 'Valideur');
  authorisationsGrid.assertRowCount(7);
}

function addMultiplePermissionsWithOneProfileAndCheck(
  userView: UserViewPage,
  projectsNames: string[],
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
) {
  const clearancesDialog =
    userView.authorisationsPanel.clickOnAddPermissionButtonAndAssertDialogExists();

  clearancesDialog.selectProjects(
    projectsNames[2],
    projectsNames[0],
    projectsNames[1],
    projectsNames[10],
  );

  const projectsNamesDisplayedAlphabeticalOrder =
    projectsNames[0] + ', ' + projectsNames[1] + ', ' + projectsNames[2] + ', ' + projectsNames[10];

  clearancesDialog.checkProjectFieldContent(projectsNamesDisplayedAlphabeticalOrder);
  clearancesDialog.checkProjectListContentTruncation();
  cy.clickVoid();
  clearancesDialog.checkProjectFieldContent(projectsNamesDisplayedAlphabeticalOrder);
  clearancesDialog.checkProjectListContentTruncation();
  clearancesDialog.selectProfile('Chef de projet');
  clearancesDialog.checkProfileFieldContent('Chef de projet');
  clearancesDialog.clickOnAddButton();
  clearancesDialog.assertNotExist();

  userView.authorisationsPanel.grid.gridFooter.checkPaginationDisplay('1 - 10 / 11');
  checkPermissionsNumberInUsersGrid(adminWorkspaceUsersPage, '11');
}

function checkPermissionsDataInGrid(authorisationsGrid: GridElement, userView: UserViewPage) {
  checkRowContainsClearance(authorisationsGrid, '1', 'Project 001', 'Chef de projet');
  checkRowContainsClearance(authorisationsGrid, '2', 'Project 002', 'Chef de projet');
  checkRowContainsClearance(authorisationsGrid, '3', 'Project 003', 'Chef de projet');
  checkRowContainsClearance(authorisationsGrid, '4', 'Project 004', 'Valideur');
  checkRowContainsClearance(authorisationsGrid, '5', 'Project 005', 'Designer de test');
  checkRowContainsClearance(authorisationsGrid, '6', 'Project 006', 'Invité');
  checkRowContainsClearance(authorisationsGrid, '7', 'Project 007', 'Testeur');
  checkRowContainsClearance(authorisationsGrid, '8', 'Project 008', 'Testeur référent');
  checkRowContainsClearance(authorisationsGrid, '9', 'Project 009', 'Testeur avancé');
  checkRowContainsClearance(authorisationsGrid, '10', 'Project 010', 'Automaticien');
  userView.authorisationsPanel.grid.gridFooter.goToNextPage();
  checkRowContainsClearance(
    authorisationsGrid,
    '1',
    'Project 011 avec vraiment énormément beaucoup de caractères',
    'Chef de projet',
  );
}

function checkRowContainsClearance(
  authorisationsGrid: GridElement,
  rowIndex: string,
  project: string,
  profile: string,
) {
  authorisationsGrid.findRowId('#', rowIndex, 'leftViewport').then((id) => {
    const row = authorisationsGrid.getRow(id);
    row.cell(GridColumnId.projectName).linkRenderer().assertContainText(project);
    row.cell(GridColumnId.permissionGroup).textRenderer().assertContainsText(profile);
  });
}

function checkPermissionsGridPagination(userView: UserViewPage) {
  userView.authorisationsPanel.grid.gridFooter.checkPaginationDisplay('11 - 11 / 11');
  userView.authorisationsPanel.grid.gridFooter.checkPagination('2/2');
  userView.authorisationsPanel.grid.gridFooter.assertPaginationButtonsExist();
}

function checkListPanelProfilesAndChangeAllProfiles(
  authorisationsGrid: GridElement,
  userView: UserViewPage,
) {
  checkListAndChangeProfile(authorisationsGrid, '1', 'Designer de tests');
  checkListAndChangeProfile(authorisationsGrid, '2', 'Automaticien');
  checkListAndChangeProfile(authorisationsGrid, '3', 'Testeur');
  checkListAndChangeProfile(authorisationsGrid, '4', 'Invité');
  checkListAndChangeProfile(authorisationsGrid, '5', 'Testeur référent');
  checkListAndChangeProfile(authorisationsGrid, '6', 'Testeur avancé');
  checkListAndChangeProfile(authorisationsGrid, '7', 'Valideur');
  checkListAndChangeProfile(authorisationsGrid, '8', 'Chef de projet');
  checkListAndChangeProfile(authorisationsGrid, '9', 'Invité');
  checkListAndChangeProfile(authorisationsGrid, '10', 'Automaticien');
  userView.authorisationsPanel.grid.gridFooter.goToNextPage();
  checkListAndChangeProfile(authorisationsGrid, '11', 'Testeur avancé');
}

function checkListAndChangeProfile(
  authorisationsGrid: GridElement,
  rowIndex: string,
  newProfile: string,
) {
  authorisationsGrid.findRowId('#', rowIndex, 'leftViewport').then((rowId) => {
    const row = authorisationsGrid.getRow(rowId);
    row.cell('permissionGroup').selectRenderer().click();
    const listPanel = new ListPanelElement();
    listPanel.assertExists();

    const profiles = [
      'Automaticien',
      'Chef de projet',
      'Designer de tests',
      'Invité',
      'Testeur',
      'Testeur avancé',
      'Testeur référent',
      'Valideur',
    ];

    profiles.forEach((profile) => {
      listPanel.assertItemExist(profile);
    });

    listPanel.findItem(newProfile).click();
    listPanel.assertNotExist();
    row.cell('permissionGroup').textRenderer().assertContainsText(newProfile);
  });
}
