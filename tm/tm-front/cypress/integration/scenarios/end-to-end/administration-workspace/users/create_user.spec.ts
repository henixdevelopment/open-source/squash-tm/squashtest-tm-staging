import { CreateUserDialog } from '../../../../page-objects/pages/administration-workspace/dialogs/create-user-dialog.element';
import { UserViewPage } from '../../../../page-objects/pages/administration-workspace/user-view/user-view.page';
import { DatabaseUtils } from '../../../../utils/database.utils';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { AdminWorkspaceUsersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import { AdminWorkspaceItem } from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import Chainable = Cypress.Chainable;
import { format } from 'date-fns';
import { fr } from 'date-fns/locale';

describe('Create User', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
  });

  it('F01-UC0101.CT01 - should create one or several users', () => {
    cy.log('Step 1');
    cy.logInAs('admin', 'admin');
    const adminWorkspaceUsersPage = NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>(
      AdminWorkspaceItem.USERS,
    );

    cy.log('Step 2');
    let createUserDialog: CreateUserDialog = adminWorkspaceUsersPage.openCreateUser();

    cy.log('Step 3');
    checkUserDialogConformity(createUserDialog);

    cy.log('Step 4');
    createUserDialog.loginField.assertHasFocus();

    // Step 5 (NOT IMPLEMENTED because TAB input is not handled by Cypress yet)

    cy.log('Step 6');
    fillAllUserFieldsInDialogAndCheckConformity(createUserDialog);
    createUserDialog.cancel();

    createUserDialog = adminWorkspaceUsersPage.openCreateUser();
    fillAllUserFieldsInDialogAndCheckConformity(createUserDialog);
    cy.get(createUserDialog.loginField.inputSelector).type('{esc}');
    createUserDialog.assertNotExist();

    cy.log('Step 7');
    createUserDialog = adminWorkspaceUsersPage.openCreateUser();
    fillAndCreateUser(createUserDialog);
    const userView = new UserViewPage();
    userView.assertExists();
    extractCreatedOn('login1').then((creationDate) => {
      cy.log('Step 8');
      checkContentHasBeenCorrectlyAddedInUserWorkspaceGrid(
        adminWorkspaceUsersPage,
        userView,
        creationDate,
      );

      cy.log('Step 9');
      checkUserViewOpensProperlyAndHasCorrectTitle(adminWorkspaceUsersPage, userView);

      cy.log('Step 10');
      checkUserInformationContent(userView, creationDate);
    });

    cy.log('Step 11');
    logOutAndLogInWithUser('login1', 'azerty2', 'PN');
    logOutAndLogInWithUser('admin', 'admin', 'S');

    cy.log('Step 12');
    extractLastConnectedOn('login1').then((lastConnectionDate) =>
      checkCreatedUserHasCorrectConnectionDateInfo(
        adminWorkspaceUsersPage,
        userView,
        lastConnectionDate,
      ),
    );

    cy.log('Step 13');
    createNewUsersWithAllPossibleWays(adminWorkspaceUsersPage, userView);

    // Step 14 (NOT IMPLEMENTED because the data of created user has already been check in previous and we don't want to
    // do it 3 more times to keep the test duration relatively short)
  });

  function checkUserDialogConformity(createUserDialog: CreateUserDialog) {
    checkAllLabelsExistInUserCreationDialog();
    checkAllFieldsExistInUserCreationDialog(createUserDialog);
    createUserDialog.checkAllButtonsExist();
  }

  function checkAllLabelsExistInUserCreationDialog() {
    cy.get(`[data-test-field-id="loginLabel"]`).should('contain.text', 'Login');
    cy.get(`[data-test-field-id="firstNameLabelLabel"]`).should('contain.text', 'Prénom');
    cy.get(`[data-test-field-id="lastNameLabel"]`).should('contain.text', 'Nom');
    cy.get(`[data-test-field-id="emailLabel"]`).should('contain.text', 'Email');
    cy.get(`[data-test-field-id="groupLabel"]`).should('contain.text', 'Groupe');
    cy.get(`[data-test-field-id="passwordLabel"]`).should('contain.text', 'Mot de passe local');
    cy.get(`[data-test-field-id="confirmPasswordLabel"]`).should('contain.text', 'Confirmation');
  }

  function checkAllFieldsExistInUserCreationDialog(createUserDialog: CreateUserDialog) {
    createUserDialog.loginField.assertExists();
    createUserDialog.firstNameField.assertExists();
    createUserDialog.lastNameField.assertExists();
    createUserDialog.emailField.assertExists();
    createUserDialog.groupField.assertExists();
    createUserDialog.groupField.checkSelectedOption('Utilisateur');
    createUserDialog.passwordField.assertExists();
    createUserDialog.passwordConfirmationField.assertExists();
  }

  function fillAllUserFieldsInDialogAndCheckConformity(createUserDialog: CreateUserDialog) {
    createUserDialog.fillUser(
      'jbond',
      'James',
      'Bond',
      'email@email.com',
      'Utilisateur',
      'password',
      'password',
    );
    createUserDialog.loginField.checkContent('jbond');
    createUserDialog.firstNameField.checkContent('James');
    createUserDialog.lastNameField.checkContent('Bond');
    createUserDialog.emailField.checkContent('email@email.com');
    createUserDialog.groupField.checkSelectedOption('Utilisateur');
    createUserDialog.passwordField.checkContent('password');
    createUserDialog.passwordConfirmationField.checkContent('password');
  }

  function fillAndCreateUser(createUserDialog: CreateUserDialog) {
    createUserDialog.createUser(
      'login1',
      'prénom1',
      'nom1',
      '',
      'Utilisateur',
      'azerty2',
      'azerty2',
    );
  }

  function checkContentHasBeenCorrectlyAddedInUserWorkspaceGrid(
    adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
    userView: UserViewPage,
    creationDate: string,
  ) {
    adminWorkspaceUsersPage.grid.assertRowCount(2);
    adminWorkspaceUsersPage.grid.findRowId('login', 'login1').then((id) => {
      adminWorkspaceUsersPage.grid.assertRowIsSelected(id);
      userView.close();

      adminWorkspaceUsersPage.grid
        .getCell(id, 'active')
        .iconRenderer()
        .assertContainsIcon('anticon-sqtm-core-administration:user');
      adminWorkspaceUsersPage.grid
        .getCell(id, 'active')
        .iconRenderer()
        .assertContainsIcon('active');
      adminWorkspaceUsersPage.grid.getCell(id, 'login').textRenderer().assertContainsText('login1');
      adminWorkspaceUsersPage.grid
        .getCell(id, 'firstName')
        .textRenderer()
        .assertContainsText('prénom1');
      adminWorkspaceUsersPage.grid
        .getCell(id, 'lastName')
        .textRenderer()
        .assertContainsText('nom1');
      adminWorkspaceUsersPage.grid.getCell(id, 'email').textRenderer().assertContainsText('');
      adminWorkspaceUsersPage.grid
        .getCell(id, 'userGroup')
        .textRenderer()
        .assertContainsText('Utilisateur');
      adminWorkspaceUsersPage.grid
        .getCell(id, 'createdBy')
        .textRenderer()
        .assertContainsText('admin');
      adminWorkspaceUsersPage.grid
        .getCell(id, 'createdOn')
        .textRenderer()
        .assertContainsText(creationDate);
      adminWorkspaceUsersPage.grid
        .getCell(id, 'habilitationCount')
        .textRenderer()
        .assertContainsText('0');
      adminWorkspaceUsersPage.grid.getCell(id, 'teamCount').textRenderer().assertContainsText('0');
      adminWorkspaceUsersPage.grid
        .getCell(id, 'lastConnectedOn')
        .textRenderer()
        .assertContainsText('');
    });
  }

  function checkUserViewOpensProperlyAndHasCorrectTitle(
    adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
    userView: UserViewPage,
  ) {
    adminWorkspaceUsersPage.selectRowWithMatchingCellContent('login', 'login1');
    userView.assertExists();
    userView.loginTextField.checkContent('login1');
  }

  function checkUserInformationContent(userView: UserViewPage, creationDate) {
    userView.activeSwitch.checkValue(true);
    userView.firstNameTextField.checkContent('prénom1');
    userView.lastNameTextField.checkContent('nom1');
    userView.emailTextField.checkContent('');
    userView.groupSelectField.checkSelectedOption('Utilisateur');
    cy.get(`[data-test-field-id="user-created"]`).should('contain.text', creationDate);
    cy.get(`[data-test-field-id="user-created"]`).should('contain.text', '(admin)');
    cy.get(`[data-test-field-id="user-last-modified"]`).should('have.text', 'jamais');
    cy.get(`[data-test-field-id="user-last-connected-on"]`).should('have.text', 'jamais');
  }

  function logOutAndLogInWithUser(
    login: string,
    password: string,
    expectedAvatarName: string,
  ): void {
    const navBar: NavBarElement = new NavBarElement();
    cy.logOut();
    cy.logInAs(login, password);
    navBar.assertAvatarHasText(expectedAvatarName);
  }

  function checkCreatedUserHasCorrectConnectionDateInfo(
    adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
    userView: UserViewPage,
    lastLoginDate: string,
  ) {
    NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>(AdminWorkspaceItem.USERS);
    adminWorkspaceUsersPage.grid.findRowId('login', 'login1').then((id) => {
      adminWorkspaceUsersPage.grid
        .getCell(id, 'lastConnectedOn')
        .textRenderer()
        .assertContainsText(lastLoginDate);
    });

    adminWorkspaceUsersPage.selectRowWithMatchingCellContent('login', 'login1');
    userView.assertExists();
    cy.get(`[data-test-field-id="user-last-connected-on"]`).should('not.have.text', 'jamais');
    cy.get(`[data-test-field-id="user-last-connected-on"]`).should('contain.text', lastLoginDate);
    userView.close();
  }

  function createNewUsersWithAllPossibleWays(
    adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
    userView: UserViewPage,
  ) {
    const createUserDialog: CreateUserDialog = adminWorkspaceUsersPage.openCreateUser();
    fillAllUserFieldsInDialogAndCheckConformity(createUserDialog);
    createUserDialog.clickOnAddAnotherButton();

    createUserDialog.assertExists();
    createUserDialog.loginField.assertHasFocus();
    createUserDialog.checkFormIsEmpty();
    createUserDialog.fillUser(
      'jdoe',
      'John',
      'Doe',
      'jdoe@email.com',
      'Utilisateur',
      'password',
      'password',
    );
    cy.get(createUserDialog.loginField.inputSelector).type('{enter}');

    createUserDialog.assertExists();
    createUserDialog.loginField.assertHasFocus();
    createUserDialog.checkFormIsEmpty();
    createUserDialog.fillUser(
      'jsnow',
      'John',
      'Snow',
      'jsnow@you-know-nothing.com',
      'Utilisateur',
      'password',
      'password',
    );
    createUserDialog.clickOnAddButton();

    adminWorkspaceUsersPage.grid.assertRowCount(5);
    userView.assertExists();
    userView.loginTextField.checkContent('jsnow');
  }
});

export function extractLastConnectedOn(userLogin: string): Chainable<string> {
  return DatabaseUtils.executeSelectQuery(
    `SELECT last_connected_on
                         FROM CORE_USER
                         WHERE login = '${userLogin}'`,
  ).then((queryResult) => {
    return format(new Date(queryResult[0].last_connected_on), 'dd/MM/yyyy HH:mm', { locale: fr });
  });
}

export function extractCreatedOn(userLogin: string): Chainable<string> {
  return DatabaseUtils.executeSelectQuery(
    `SELECT created_on
                         FROM CORE_USER
                         WHERE login = '${userLogin}'`,
  ).then((queryResult) => {
    return format(new Date(queryResult[0].created_on), 'dd/MM/yyyy', { locale: fr });
  });
}
