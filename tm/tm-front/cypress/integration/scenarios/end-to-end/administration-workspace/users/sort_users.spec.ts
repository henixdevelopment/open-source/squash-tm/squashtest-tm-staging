import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { AdminWorkspaceUsersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import { TeamViewDetailPage } from '../../../../page-objects/pages/administration-workspace/team-view/team-view-detail.page';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { ProjectViewDetailPage } from '../../../../page-objects/pages/administration-workspace/project-view/project-view-detail.page';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import { UserViewPage } from '../../../../page-objects/pages/administration-workspace/user-view/user-view.page';
import { DatabaseUtils } from '../../../../utils/database.utils';
import { GridHeaderRowElement } from '../../../../utils/grid-selectors.builder';
import * as dayjs from 'dayjs';
import { AdminWorkspaceItem } from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import { GridColumnId } from '../../../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import {
  TeamBuilder,
  UserBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addPermissionToUser,
  ApiPermissionGroup,
} from '../../../../utils/end-to-end/api/add-permissions';

describe('Sort users', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC0101.CT07 - sort users', () => {
    cy.log('Step 1');
    cy.logInAs('admin', 'admin');
    const adminWorkspaceUsersPage = NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>(
      AdminWorkspaceItem.USERS,
    );

    cy.log('Step 2');
    const grid = adminWorkspaceUsersPage.grid;
    grid.assertGridContentOrder('login', ['admin', 'aladdin', 'awall-e', 'leroy', 'zorro']);

    cy.log('Step 3 and 4');
    sortByColumnsAndCheck(grid);

    cy.log('Step 5');
    const userView = new UserViewPage();
    const permissionGrid = userView.authorisationsPanel.grid;
    const projectView = new ProjectViewDetailPage();
    const headerRow = grid.getHeaderRow();
    checkFiltersAreKeptAfterNavigationOnProject(
      grid,
      userView,
      permissionGrid,
      projectView,
      headerRow,
      adminWorkspaceUsersPage,
    );

    cy.log('Step 6');
    const teamGrid = userView.teamsPanel.grid;
    const teamView = new TeamViewDetailPage();
    checkFiltersAreKeptAfterNavigationOnTeam(
      grid,
      userView,
      teamGrid,
      teamView,
      headerRow,
      adminWorkspaceUsersPage,
    );
  });

  function initTestData() {
    return new PrerequisiteBuilder()
      .withUsers([
        new UserBuilder('leroy')
          .withFirstName('leroy')
          .withLastName('Jenkins')
          .withGroupName('Admin')
          .withEmail('leroyjenkins@dada.com')
          .withCreatedOn(new Date('2020-05-09 15:05:25'))
          .withLastConnection(new Date('2020-10-31 05:44:45')),
        new UserBuilder('aladdin')
          .withFirstName('aladdin')
          .withLastName('Leprincedesvoleurs')
          .withActive(false)
          .withCreatedBy('random admin')
          .withCreatedOn('2019-10-31 15:05:25')
          .withEmail('leprince@haha.fr'),
        new UserBuilder('zorro')
          .withFirstName('don diego')
          .withLastName('Delavega')
          .withEmail('elzorro@zzzz.com')
          .withCreatedOn(new Date('2020-09-22 15:05:25'))
          .withLastConnection(new Date('2021-10-31 14:55:45')),
        new UserBuilder('awall-e')
          .withFirstName('arobot')
          .withLastName('Robot')
          .withGroupName('TestAutomationServer')
          .withEmail('irobot@rrr.ver')
          .withCreatedOn('2019-10-31 15:05:25')
          .withLastConnection(new Date('2022-10-31 15:44:45')),
      ])
      .withTeams([new TeamBuilder('monequipe').withUserLogins(['aladdin'])])
      .withProjects([new ProjectBuilder('AZERTY'), new ProjectBuilder('QSDFGH')])
      .build();
  }
  function initPermissions() {
    addPermissionToUser(2, ApiPermissionGroup.PROJECT_MANAGER, [1, 2]);
    addPermissionToUser(4, ApiPermissionGroup.PROJECT_MANAGER, [1]);
  }
  function sortByColumnsAndCheck(grid: GridElement) {
    sortByActiveColumn(grid);
    sortByLoginColumn(grid);
    sortByUserGroupColumn(grid);
    sortByFirstNameColumn(grid);
    sortByLastNameColumn(grid);
    sortByEmailColumn(grid);
    sortByCreatedOnColumn(grid);
    sortByNumberOfPermission(grid);
    sortByNumberOfTeams(grid);
    sortByLastConnection(grid);
    sortByCreatedByAndCheckIndexImmutability(grid);
  }

  function sortByActiveColumn(grid: GridElement) {
    grid.clickOnSortingGridHeader('active');
    checkIfActiveIconMatchesWithIndex(grid, '1', 'inactive');
    checkIfActiveIconMatchesWithIndex(grid, '2', 'active');
    checkIfActiveIconMatchesWithIndex(grid, '3', 'active');
    checkIfActiveIconMatchesWithIndex(grid, '4', 'active');
    checkIfActiveIconMatchesWithIndex(grid, '5', 'active');

    grid.clickOnSortingGridHeader('active');
    checkIfActiveIconMatchesWithIndex(grid, '5', 'inactive');

    grid.clickOnSortingGridHeader('active');
    checkIfActiveIconMatchesWithIndex(grid, '2', 'inactive');
  }

  function checkIfActiveIconMatchesWithIndex(grid: GridElement, index: string, icon: string) {
    grid.findRowId('#', index, 'leftViewport').then((rowId) => {
      grid.getRow(rowId).cell('active').iconRenderer().assertContainsIcon(icon);
    });
  }

  function sortByLoginColumn(grid: GridElement) {
    clickOnHeaderAndCheckSorting(grid, 'login', ['admin', 'aladdin', 'awall-e', 'leroy', 'zorro']);
    clickOnHeaderAndCheckSorting(grid, 'login', ['zorro', 'leroy', 'awall-e', 'aladdin', 'admin']);
    clickOnHeaderAndCheckSorting(grid, 'login', ['admin', 'aladdin', 'awall-e', 'leroy', 'zorro']);
  }
  function sortByUserGroupColumn(grid: GridElement) {
    clickOnHeaderAndCheckSorting(grid, 'userGroup', [
      'Administrateur',
      'Administrateur',
      "Serveur d'automatisation de tests",
      'Utilisateur',
      'Utilisateur',
    ]);
    clickOnHeaderAndCheckSorting(grid, 'userGroup', [
      'Utilisateur',
      'Utilisateur',
      "Serveur d'automatisation de tests",
      'Administrateur',
      'Administrateur',
    ]);
    clickOnHeaderAndCheckSorting(grid, 'userGroup', [
      'Administrateur',
      'Utilisateur',
      "Serveur d'automatisation de tests",
      'Administrateur',
      'Utilisateur',
    ]);
    grid.assertGridContentOrder('login', ['admin', 'aladdin', 'awall-e', 'leroy', 'zorro']);
  }
  function sortByFirstNameColumn(grid: GridElement) {
    clickOnHeaderAndCheckSorting(grid, 'firstName', [
      '-',
      'aladdin',
      'arobot',
      'don diego',
      'leroy',
    ]);
    clickOnHeaderAndCheckSorting(grid, 'firstName', [
      'leroy',
      'don diego',
      'arobot',
      'aladdin',
      '-',
    ]);
    clickOnHeaderAndCheckSorting(grid, 'firstName', [
      '-',
      'aladdin',
      'arobot',
      'leroy',
      'don diego',
    ]);
  }

  function sortByLastNameColumn(grid: GridElement) {
    clickOnHeaderAndCheckSorting(grid, 'lastName', [
      'Delavega',
      'Jenkins',
      'Leprincedesvoleurs',
      'Robot',
      'Squash Administrator',
    ]);
    clickOnHeaderAndCheckSorting(grid, 'lastName', [
      'Squash Administrator',
      'Robot',
      'Leprincedesvoleurs',
      'Jenkins',
      'Delavega',
    ]);
    clickOnHeaderAndCheckSorting(grid, 'lastName', [
      'Squash Administrator',
      'Leprincedesvoleurs',
      'Robot',
      'Jenkins',
      'Delavega',
    ]);
  }
  function sortByEmailColumn(grid: GridElement) {
    clickOnHeaderAndCheckSorting(grid, 'email', [
      'admin@squashtest.org',
      'elzorro@zzzz.com',
      'irobot@rrr.ver',
      'leprince@haha.fr',
      'leroyjenkins@dada.com',
    ]);
    clickOnHeaderAndCheckSorting(grid, 'email', [
      'leroyjenkins@dada.com',
      'leprince@haha.fr',
      'irobot@rrr.ver',
      'elzorro@zzzz.com',
      'admin@squashtest.org',
    ]);
    clickOnHeaderAndCheckSorting(grid, 'email', [
      'admin@squashtest.org',
      'leprince@haha.fr',
      'irobot@rrr.ver',
      'leroyjenkins@dada.com',
      'elzorro@zzzz.com',
    ]);
  }
  function sortByCreatedOnColumn(grid: GridElement) {
    DatabaseUtils.executeSelectQuery(`SELECT * FROM CORE_USER WHERE LOGIN = 'admin';`).then(
      (result) => {
        const adminCreationDate: Date = result[0].created_on;
        const formattedSortedCreationDate: string = dayjs(adminCreationDate).format('DD/MM/YYYY');
        clickOnHeaderAndCheckSorting(grid, 'createdOn', [
          '31/10/2019',
          '31/10/2019',
          '09/05/2020',
          '22/09/2020',
          formattedSortedCreationDate,
        ]);
        clickOnHeaderAndCheckSorting(grid, 'createdOn', [
          formattedSortedCreationDate,
          '22/09/2020',
          '09/05/2020',
          '31/10/2019',
          '31/10/2019',
        ]);
        clickOnHeaderAndCheckSorting(grid, 'createdOn', [
          formattedSortedCreationDate,
          '31/10/2019',
          '31/10/2019',
          '09/05/2020',
          '22/09/2020',
        ]);
        grid.assertGridContentOrder('login', ['admin', 'aladdin', 'awall-e', 'leroy', 'zorro']);
      },
    );
  }

  function sortByNumberOfPermission(grid: GridElement) {
    clickOnHeaderAndCheckSorting(grid, 'habilitationCount', ['0', '0', '0', '1', '2']);
    clickOnHeaderAndCheckSorting(grid, 'habilitationCount', ['2', '1', '0', '0', '0']);
    clickOnHeaderAndCheckSorting(grid, 'habilitationCount', ['0', '0', '0', '2', '1']);
  }
  function sortByNumberOfTeams(grid: GridElement) {
    clickOnHeaderAndCheckSorting(grid, 'teamCount', ['0', '0', '0', '0', '1']);
    clickOnHeaderAndCheckSorting(grid, 'teamCount', ['1', '0', '0', '0', '0']);
    clickOnHeaderAndCheckSorting(grid, 'teamCount', ['0', '1', '0', '0', '0']);
  }
  function sortByLastConnection(grid: GridElement) {
    DatabaseUtils.executeSelectQuery(`SELECT * FROM CORE_USER WHERE LOGIN = 'admin';`).then(
      (result) => {
        let sortedLastConnectionDate: string[];
        const adminLastConnectionDate: Date = result[0].last_connected_on;
        const formattedAdminLastConnectionDate: string =
          dayjs(adminLastConnectionDate).format('DD/MM/YYYY HH:mm');

        sortedLastConnectionDate = DatabaseUtils.sortHyphenStartOrEndInArrayAsc([
          '31/10/2020 05:44',
          '31/10/2021 14:55',
          '31/10/2022 15:44',
          formattedAdminLastConnectionDate,
          '-',
        ]);
        clickOnHeaderAndCheckSorting(grid, 'lastConnectedOn', sortedLastConnectionDate);

        sortedLastConnectionDate = DatabaseUtils.sortHyphenStartOrEndInArrayDesc([
          '-',
          formattedAdminLastConnectionDate,
          '31/10/2022 15:44',
          '31/10/2021 14:55',
          '31/10/2020 05:44',
        ]);
        clickOnHeaderAndCheckSorting(grid, 'lastConnectedOn', sortedLastConnectionDate);

        clickOnHeaderAndCheckSorting(grid, 'lastConnectedOn', [
          formattedAdminLastConnectionDate,
          '-',
          '31/10/2022 15:44',
          '31/10/2020 05:44',
          '31/10/2021 14:55',
        ]);
      },
    );
  }

  function sortByCreatedByAndCheckIndexImmutability(grid: GridElement) {
    grid.assertGridContentOrder('#', ['1', '2', '3', '4', '5'], 'leftViewport');

    clickOnHeaderAndCheckSorting(grid, 'createdBy', [
      'admin',
      'admin',
      'admin',
      'liquibase',
      'random admin',
    ]);
    grid.assertGridContentOrder('#', ['1', '2', '3', '4', '5'], 'leftViewport');

    clickOnHeaderAndCheckSorting(grid, 'createdBy', [
      'random admin',
      'liquibase',
      'admin',
      'admin',
      'admin',
    ]);
    grid.assertGridContentOrder('#', ['1', '2', '3', '4', '5'], 'leftViewport');

    clickOnHeaderAndCheckSorting(grid, 'createdBy', [
      'liquibase',
      'random admin',
      'admin',
      'admin',
      'admin',
    ]);
    grid.assertGridContentOrder('login', ['admin', 'aladdin', 'awall-e', 'leroy', 'zorro']);
    grid.assertGridContentOrder('#', ['1', '2', '3', '4', '5'], 'leftViewport');
  }

  function clickOnHeaderAndCheckSorting(
    grid: GridElement,
    columnName: string,
    orderedNames: string[],
  ) {
    grid.clickOnSortingGridHeader(columnName);
    grid.assertGridContentOrder(columnName, orderedNames);
  }
  function openUsersView(
    adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
    userView: UserViewPage,
    login: string,
  ) {
    adminWorkspaceUsersPage.selectRowWithMatchingCellContent('login', login);
    userView.assertExists();
  }

  function checkFiltersAreKeptAfterNavigationOnProject(
    grid: GridElement,
    userView: UserViewPage,
    permissionGrid: GridElement,
    projectView: ProjectViewDetailPage,
    headerRow: GridHeaderRowElement,
    adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
  ) {
    assertUnsortedGridThenSearch(grid, 'le', 2);

    clickOnHeaderAndCheckSorting(grid, 'lastName', ['Jenkins', 'Leprincedesvoleurs']);
    headerRow.cell('lastName').findIconInCell('arrow-up');

    openDetailedView(
      userView,
      adminWorkspaceUsersPage,
      'leroy',
      'authorisations',
      permissionGrid,
      GridColumnId.projectName,
      'AZERTY',
    );
    projectView.assertExists();

    projectView.clickBackButton();
    userView.assertExists();
    userView.anchors.assertLinkIsActive('authorisations');

    closeAndCheck(grid, headerRow, userView, 'lastName', ['Jenkins', 'Leprincedesvoleurs']);
  }

  function checkFiltersAreKeptAfterNavigationOnTeam(
    grid: GridElement,
    userView: UserViewPage,
    teamGrid: GridElement,
    teamView: TeamViewDetailPage,
    headerRow: GridHeaderRowElement,
    adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
  ) {
    grid.clickOnSortingGridHeader('lastName');
    grid.clickOnSortingGridHeader('lastName');
    grid.assertGridArrowUpOnSortingNotExist(); // due to ISSUE-138, we cannot check arrow down
    grid.clearSearchField('input-filter');
    grid.assertRowCount(5);

    grid.assertGridArrowUpOnSortingNotExist(); // due to ISSUE-138, we cannot check arrow down
    grid.searchInGrid('user-filter-field', 'la');
    grid.assertRowCount(2);
    clickOnHeaderAndCheckSorting(grid, 'firstName', ['aladdin', 'don diego']);
    headerRow.cell('firstName').findIconInCell('arrow-up');

    openDetailedView(
      userView,
      adminWorkspaceUsersPage,
      'aladdin',
      'teams',
      teamGrid,
      'name',
      'monequipe',
    );
    teamView.assertExists();

    teamView.clickBackButton();
    userView.assertExists();
    userView.anchors.assertLinkIsActive('teams');

    closeAndCheck(grid, headerRow, userView, 'firstName', ['aladdin', 'don diego']);
  }

  function assertUnsortedGridThenSearch(grid: GridElement, searchTerm: string, rowCount: number) {
    grid.assertGridArrowOnSortingNotExist();
    grid.searchInGrid('user-filter-field', searchTerm);
    grid.assertRowCount(rowCount);
  }

  function openDetailedView(
    userView: UserViewPage,
    adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
    login: string,
    linkId: any,
    teamGrid: GridElement,
    cellId: string,
    cellContent: string,
  ) {
    openUsersView(adminWorkspaceUsersPage, userView, login);
    userView.anchors.clickLink(linkId);
    cy.get(`div[data-test-cell-id="${cellId}"]`).click();
    teamGrid.assertExists();
    teamGrid.findRowId(cellId, cellContent).then((rowId) => {
      teamGrid.getCell(rowId, cellId).linkRenderer().findCellLink().click();
    });
  }

  function closeAndCheck(
    grid: GridElement,
    headerRow: GridHeaderRowElement,
    userView: UserViewPage,
    cellId: string,
    orderedNames: string[],
  ) {
    userView.close();
    headerRow.cell(cellId).findIconInCell('arrow-up');
    grid.assertRowCount(2);
    grid.assertGridContentOrder(cellId, orderedNames);
  }
});
