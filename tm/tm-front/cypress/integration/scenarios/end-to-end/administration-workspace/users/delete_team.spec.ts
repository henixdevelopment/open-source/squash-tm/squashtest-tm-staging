import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { AdminWorkspaceUsersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import {
  AdminWorkspaceItem,
  NavBarAdminElement,
} from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import { RemoveUserDialogElement } from '../../../../page-objects/pages/administration-workspace/dialogs/remove-user-dialog.element';
import { AdminWorkspaceTeamsPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-teams.page';
import {
  TeamBuilder,
  UserBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';

describe('Dissociate teams from table', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    cy.logInAs('admin', 'admin');
  });

  it('F01-UC0102.CT14 - should dissociate team', () => {
    cy.log('Step 1');
    const adminWorkspaceUsersPage = NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>(
      AdminWorkspaceItem.USERS,
    );
    adminWorkspaceUsersPage.assertExists();

    cy.log('Step 2');
    const teamPage = adminWorkspaceUsersPage.goToTeamAnchor();
    checkConformityOfTeamAnchorGrid(teamPage);

    cy.log('Step 3');
    const confirmDeleteDialog = new RemoveUserDialogElement();
    checkConfirmDialogConformity(confirmDeleteDialog);

    cy.log('Step 4');
    deleteTeam(confirmDeleteDialog, teamPage);

    cy.log('Step 5');
    checkNumberOfTeamInUserGrid(adminWorkspaceUsersPage);
  });
});

function initTestData() {
  const teamNames: string[] = ['Gryffondor', 'Serpentard'];
  return new PrerequisiteBuilder()
    .withUsers([
      new UserBuilder('HarryP').withFirstName('Harry').withLastName('Potter'),
      new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley'),
    ])
    .withTeams(
      teamNames.map((teamName) => new TeamBuilder(teamName).withUserLogins(['HarryP', 'RonW'])),
    )
    .build();
}

function checkConformityOfTeamAnchorGrid(teamPage: AdminWorkspaceTeamsPage) {
  teamPage.assertExists();
  teamPage.grid.assertRowCount(2);
  teamPage.grid.assertGridColumnContains('name', ['Gryffondor', 'Serpentard']);

  teamPage.grid.findRowId('name', 'Gryffondor').then((teamID) => {
    teamPage.grid.getCell(teamID, 'delete', 'rightViewport').iconRenderer().click();
  });
}

function checkConfirmDialogConformity(confirmDeleteDialog: RemoveUserDialogElement) {
  confirmDeleteDialog.assertExists();
  confirmDeleteDialog.assertExclamationCircleIconExists();
  confirmDeleteDialog.assertTitleHasText("Supprimer l'équipe");
  confirmDeleteDialog.assertHasMessage(
    "L'équipe sera supprimée, cette action ne peut être annulée.\nConfirmez-vous la suppression de l'équipe ?",
  );
  confirmDeleteDialog.assertConfirmButtonExists();
  confirmDeleteDialog.assertCancelButtonExists();
}

function deleteTeam(
  confirmDeleteDialog: RemoveUserDialogElement,
  teamPage: AdminWorkspaceTeamsPage,
) {
  confirmDeleteDialog.cancel();
  confirmDeleteDialog.assertNotExist();

  teamPage.grid.findRowId('name', 'Gryffondor').then((teamID) => {
    teamPage.grid.getCell(teamID, 'delete', 'rightViewport').iconRenderer().click();
  });
  confirmDeleteDialog.assertExists();
  confirmDeleteDialog.clickOnConfirmButton();
  teamPage.grid.assertRowIdNotExist('name', 'Gryffondor');
}

function checkNumberOfTeamInUserGrid(adminWorkspaceUsersPage: AdminWorkspaceUsersPage) {
  NavBarAdminElement.navigateToAdministration(AdminWorkspaceItem.USERS);
  adminWorkspaceUsersPage.assertExists();
  adminWorkspaceUsersPage.grid.findRowId('login', 'HarryP').then((userID) => {
    adminWorkspaceUsersPage.grid
      .getCell(userID, 'teamCount')
      .textRenderer()
      .assertContainsText('1');
  });

  adminWorkspaceUsersPage.grid.findRowId('login', 'RonW').then((userID) => {
    adminWorkspaceUsersPage.grid
      .getCell(userID, 'teamCount')
      .textRenderer()
      .assertContainsText('1');
  });
}
