import { DatabaseUtils } from '../../../../utils/database.utils';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { AdminWorkspaceUsersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import { AdminWorkspaceItem } from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import { UserViewPage } from '../../../../page-objects/pages/administration-workspace/user-view/user-view.page';
import { TeamViewPage } from '../../../../page-objects/pages/administration-workspace/team-view/team-view.page';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { selectByDataTestDialogButtonId } from '../../../../utils/basic-selectors';
import { GridFooterElement } from '../../../../page-objects/elements/grid/grid-footer.element';
import { UserTeamsPanelElement } from '../../../../page-objects/pages/administration-workspace/user-view/panels/user-teams-panel.element';
import { RemoveTeamDialogElement } from '../../../../page-objects/pages/administration-workspace/dialogs/remove-team-dialog.element';
import {
  TeamBuilder,
  UserBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';

describe('Dissociate teams from table', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    cy.logInAs('admin', 'admin');
  });

  it('F01-UC0102.CT14 - should dissociate teams', () => {
    cy.log('Step 1');
    const adminWorkspaceUsersPage = NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>(
      AdminWorkspaceItem.USERS,
    );
    adminWorkspaceUsersPage.selectUserByLogin('Sauron');

    const userViewPage = new UserViewPage();
    userViewPage.assertExists();

    cy.log('Step 2');
    userViewPage.assertAnchorTeamCount(11);
    userViewPage.anchors.clickLink('teams');

    const teamPanel = userViewPage.teamsPanel;
    teamPanel.assertIsVisible();

    const gridFooter = teamPanel.grid.gridFooter;
    checkPaginationAndOpenPupUp(gridFooter, teamPanel, userViewPage);

    const removeTeamDialogElement = new RemoveTeamDialogElement();

    cy.log('Step 3');
    controlPopUpConformity(removeTeamDialogElement);

    cy.log('Step 4');
    removePermission(userViewPage, teamPanel, removeTeamDialogElement, gridFooter);

    cy.log('Step 5');
    selectElementsOpenPopUpThenDelete(
      teamPanel,
      removeTeamDialogElement,
      userViewPage,
      ['Arwen', 'Gimli', 'Legolas'],
      7,
    );

    cy.log('Step 6');
    selectElementsOpenPopUpThenDelete(
      teamPanel,
      removeTeamDialogElement,
      userViewPage,
      ['SamLeGoat', 'FrodonLaFraude', 'Merry', 'Pippin', 'Gandalf', 'Boromir', 'Theoden'],
      0,
    );
    teamPanel.grid.assertGridIsEmpty();

    cy.log('Step 7');
    const teamPage = adminWorkspaceUsersPage.goToTeamAnchor();
    teamPage.selectRowWithMatchingCellContent('name', 'FrodonLaFraude');
    const teamViewPage = new TeamViewPage();
    teamViewPage.anchors.clickLink('members');

    const teamViewAuthorisationsGrid = new TeamViewPage().teamMembersPanel.grid;
    teamViewAuthorisationsGrid.assertExists();
    teamViewAuthorisationsGrid.assertGridIsEmpty();
  });
});

function initTestData() {
  const teamNames: string[] = [
    'Aragorn',
    'Gimli',
    'Legolas',
    'Arwenn',
    'SamLeGoat',
    'FrodonLaFraude',
    'Merry',
    'Pippin',
    'Gandalf',
    'Boromir',
    'Theoden',
  ];
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('Sauron').withFirstName('Sauron').withLastName('Sauron')])
    .withTeams(teamNames.map((teamName) => new TeamBuilder(teamName).withUserLogins(['Sauron'])))
    .build();
}

function controlPopUpConformity(removeTeamDialogElement: RemoveTeamDialogElement) {
  removeTeamDialogElement.assertExists();
  removeTeamDialogElement.assertTitleHasText('Supprimer les associations');
  removeTeamDialogElement.assertHasMessage(
    "Les équipes sélectionnées ne seront plus associées à l'utilisateur. Confirmez-vous la suppression des associations?",
  );
  removeTeamDialogElement.assertExclamationCircleIconExists();
  removeTeamDialogElement.assertCancelButtonExists();
  removeTeamDialogElement.assertConfirmButtonExists();
  removeTeamDialogElement.checkButtonClass(
    selectByDataTestDialogButtonId('confirm'),
    'danger-icon',
  );
  removeTeamDialogElement.cancel();
}
function checkPaginationAndOpenPupUp(
  gridFooter: GridFooterElement,
  teamPanel: UserTeamsPanelElement,
  userViewPage: UserViewPage,
) {
  userViewPage.checkIfButtonIsActiveWithClass('remove-teams', 'label-color');
  gridFooter.checkPaginationDisplay('1 - 10 / 11');
  gridFooter.checkPagination('1/2');
  gridFooter.assertPaginationButtonsExist();
  teamPanel.grid.selectRowsWithMatchingCellContent('name', ['Aragorn']);
  userViewPage.checkIfButtonIsActiveWithClass('remove-teams', '__hover_pointer');
  userViewPage.clickRemoveButton('remove-teams');
}

function removePermission(
  userViewPage: UserViewPage,
  teamPanel: UserTeamsPanelElement,
  removeTeamDialogElement: RemoveTeamDialogElement,
  gridFooter: GridFooterElement,
) {
  userViewPage.clickRemoveButton('remove-teams');
  removeTeamDialogElement.assertExists();
  removeTeamDialogElement.confirm();
  teamPanel.grid.assertRowCount(10);
  gridFooter.checkPaginationDoesntExist();
}

function selectElementsOpenPopUpThenDelete(
  teamPanel: UserTeamsPanelElement,
  removeTeamDialogElement: RemoveTeamDialogElement,
  userViewPage: UserViewPage,
  elements: string[],
  count: number,
) {
  teamPanel.grid.selectRowsWithMatchingCellContent('name', elements);
  userViewPage.clickRemoveButton('remove-teams');
  removeTeamDialogElement.assertExists();
  removeTeamDialogElement.cancel();
  userViewPage.clickRemoveButton('remove-teams');
  removeTeamDialogElement.assertExists();
  removeTeamDialogElement.confirm();
  teamPanel.grid.assertRowCount(count);
}
