import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  CampaignBuilder,
  IterationBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { SystemViewMessagesPage } from '../../../../page-objects/pages/administration-workspace/system-view/system-view-messages.page';
import { ConfigurableBannerElement } from '../../../../page-objects/elements/configurable-banner/configurable-banner.element';
import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { AutomationProgrammerGlobalPage } from '../../../../page-objects/pages/automation-workspace/automation-programmer/automation-programmer-global.page';
import { IterationViewPage } from '../../../../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import { RequirementSearchPage } from '../../../../page-objects/pages/requirement-workspace/search/requirement-search-page';

describe('Activate/deactivate user from grid', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
  });

  it('F01-UC063.CT04 - should configure and display banner', () => {
    {
      cy.log('Step 1');
      cy.logInAs('cypress', 'admin');
      const page = navigateToMessagesAdministration();
      setBannerMessage(page, htmlContent);
      ConfigurableBannerElement.assertIsVisibleWithContent(expectedContent);
    }

    {
      cy.log('Step 2');
      navigateToRequirementSearch();
      ConfigurableBannerElement.assertIsVisibleWithContent(expectedContent);
    }

    {
      cy.log('Step 3');
      const page = navigateToMessagesAdministration();
      setBannerMessage(page, htmlContent2);
      ConfigurableBannerElement.assertIsVisibleWithContent(expectedContent2);
      navigateToIterationView();
      ConfigurableBannerElement.assertIsVisibleWithContent(expectedContent2);
    }

    {
      cy.log('Step 4');
      const page = navigateToMessagesAdministration();
      clearBannerMessage(page);
      ConfigurableBannerElement.assertExists();
      ConfigurableBannerElement.assertIsNotVisible();
      navigateToAutomationWorkspace();
      ConfigurableBannerElement.assertIsNotVisible();
    }
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([
      new UserBuilder('cypress')
        .withFirstName('admin')
        .withLastName('admin')
        .withGroupName('Admin'),
    ])
    .withProjects([
      new ProjectBuilder('Project 1').withCampaignLibraryNodes([
        new CampaignBuilder('Campaign 001').withIterations([new IterationBuilder('Iteration 001')]),
      ]),
    ])
    .build();
}

function navigateToMessagesAdministration() {
  NavBarElement.navigateToAdministrationSystem();
  const page = new SystemViewMessagesPage();
  page.clickMessagesAnchor();
  return page;
}

function navigateToRequirementSearch() {
  const reqWorkspace = RequirementWorkspacePage.initTestAtPage();
  reqWorkspace.clickSearchButton();

  const reqSearchPage = new RequirementSearchPage(null);
  reqSearchPage.assertExists();
}

function navigateToIterationView() {
  const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();
  campaignWorkspace.tree.openNodeByName('Project 1');
  campaignWorkspace.tree.openNodeByName('Campaign 001');
  campaignWorkspace.tree.selectNodeByName('Iteration 001');

  const iterationViewPage = new IterationViewPage();
  iterationViewPage.assertExists();
}

function navigateToAutomationWorkspace() {
  const automationWorkspace = AutomationProgrammerGlobalPage.initTestAtPage();
  automationWorkspace.assertExists();
}

function setBannerMessage(page: SystemViewMessagesPage, content: string) {
  page.bannerMessageField.enableEditMode();
  page.bannerMessageField.setValue(content);
  page.bannerMessageField.confirm();
}

function clearBannerMessage(page: SystemViewMessagesPage) {
  page.bannerMessageField.enableEditMode();
  page.bannerMessageField.clear();
  page.bannerMessageField.confirm();
  page.bannerMessageField.assertPlaceholderIsVisible();
}

const htmlContent = `<p style="--ck-color-image-caption-background: #f7f7f7; --ck-color-image-caption-text: #333333; --ck-color-mention-background: #f5e6ea; --ck-color-mention-text: #990030; --ck-color-selector-caption-background: #f7f7f7; --ck-color-selector-caption-text: #333333; --ck-highlight-marker-blue: #72ccfd; --ck-highlight-marker-green: #62f962; --ck-highlight-marker-pink: #fc7899; --ck-highlight-marker-yellow: #fdfd77; --ck-highlight-pen-green: #128a00; --ck-highlight-pen-red: #e71313; --ck-image-style-spacing: 1.5em; --ck-inline-image-style-spacing: calc(var(--ck-image-style-spacing) / 2); --ck-todo-list-checkmark-size: 16px;"><span style="color:#f44336;"><i><strong>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</strong></i></span></p><ul style="--ck-color-image-caption-background: #f7f7f7; --ck-color-image-caption-text: #333333; --ck-color-mention-background: #f5e6ea; --ck-color-mention-text: #990030; --ck-color-selector-caption-background: #f7f7f7; --ck-color-selector-caption-text: #333333; --ck-highlight-marker-blue: #72ccfd; --ck-highlight-marker-green: #62f962; --ck-highlight-marker-pink: #fc7899; --ck-highlight-marker-yellow: #fdfd77; --ck-highlight-pen-green: #128a00; --ck-highlight-pen-red: #e71313; --ck-image-style-spacing: 1.5em; --ck-inline-image-style-spacing: calc(var(--ck-image-style-spacing) / 2); --ck-todo-list-checkmark-size: 16px; list-style-type: disc;"><li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <a href="#">Cliquer ici</a></li><li>Etiam dignissim libero eu elementum varius.</li></ul>`;
const expectedContent = `Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...`;

const htmlContent2 = `<p>Ceci est une bannière informative visant à alerter les utillisateurs des maintenances de l'outil</p>`;
const expectedContent2 = `Ceci est une bannière informative visant à alerter les utillisateurs des maintenances de l'outil`;
