import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  CampaignBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
  TestSuiteBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { RequirementSearchPage } from '../../../../page-objects/pages/requirement-workspace/search/requirement-search-page';
import { RequirementBuilder } from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import { TestCaseBuilder } from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { SystemViewSettingsPage } from '../../../../page-objects/pages/administration-workspace/system-view/system-view-settings.page';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import { selectByDataTestElementId } from '../../../../utils/basic-selectors';
import { CustomColumnConfigurationBuilder } from '../../../../utils/end-to-end/prerequisite/builders/custom-column-configuration-prerequisite-builders';
import { TestCaseSearchPage } from '../../../../page-objects/pages/test-case-workspace/research/test-case-search-page';
import { ItpiSearchPage } from '../../../../page-objects/pages/campaign-workspace/search/itpi-search-page';
import { WorkspaceWithTreePage } from '../../../../page-objects/pages/workspace-with-tree.page';
import { AbstractSearchPage } from '../../../../page-objects/pages/abstract-search-page';
import { NavBarAdminElement } from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';

describe('Desactivate automatic search', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
  });

  it('F01-UC062.CT10 - should desactivate automatic search', () => {
    const requirementPage = new RequirementSearchPage(new GridElement('requirement-search'));
    const requirementGrid = requirementPage.grid;
    const testCasePage = new TestCaseSearchPage(new GridElement('test-case-search'));
    const testCaseGrid = testCasePage.grid;
    const itpiSearchPage = new ItpiSearchPage(new GridElement('itpi-search'));
    const itpiGrid = itpiSearchPage.grid;

    {
      cy.log('Step 1');
      cy.logInAs('admin', 'admin');
      navigateToSystemAdministrationAndDesactivateAutomaticSearch();
    }

    {
      cy.log('Step 2: requirement search');
      const reqWorkspace = NavBarElement.navigateToRequirementWorkspace();
      accessEmptyGridThenDisplayMethod(requirementPage, reqWorkspace, requirementGrid);
      requirementPage.addCriteriaCriticityandLevelOfCriticity('Majeure');
      assertGrisIsEmptyThenNotEmpty(requirementPage, requirementGrid);

      requirementPage.clickInactivateButton();
      verifyEmptyGridMessage(requirementGrid);
      requirementPage.addCriteriaCriticityandLevelOfCriticity('Critique');
      assertNoresultDisplayedForSearchAndColumnConfiguration(requirementPage, requirementGrid);
    }

    {
      cy.log('Step 3: test case search');
      const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
      accessEmptyGridThenDisplayMethod(testCasePage, testCaseWorkspace, testCaseGrid);
      testCasePage.selectItemToAppearAloneInPageWithNameCriteria('Thor');
      assertGrisIsEmptyThenNotEmpty(testCasePage, testCaseGrid);

      testCasePage.clickInactivateButton();
      verifyEmptyGridMessage(testCaseGrid);

      testCasePage.selectItemToAppearAloneInPageWithNameCriteria('ngjhg');
      assertNoresultDisplayedForSearchAndColumnConfiguration(testCasePage, testCaseGrid);
    }

    {
      cy.log('Step 4: itpi search');
      const itpiWorkspace = NavBarElement.navigateToCampaignWorkspace();
      accessEmptyGridThenDisplayMethod(itpiSearchPage, itpiWorkspace, itpiGrid);
      itpiSearchPage.selectProjectInWidgetPerimeter('Avengers');

      assertGrisIsEmptyThenNotEmpty(itpiSearchPage, itpiGrid);
      itpiSearchPage.selectProjectInWidgetPerimeter('X men');
      assertNoresultDisplayedForSearchAndColumnConfiguration(itpiSearchPage, itpiGrid);
    }
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withProjects([
      new ProjectBuilder('Avengers')
        .withRequirementLibraryNodes([
          new RequirementBuilder('Captain america'),
          new RequirementBuilder('Black Widow'),
          new RequirementBuilder('Hulk').withCriticality('MAJOR'),
        ])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Thor'),
          new TestCaseBuilder('Hawkeye'),
          new TestCaseBuilder('Vision'),
        ])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Ant Man').withIterations([
            new IterationBuilder('Avengers: Ultron')
              .withTestPlanItems([new IterationTestPlanItemBuilder('Thor', 'READY')])
              .withTestSuites([
                new TestSuiteBuilder('Ultron', 'READY').withTestCaseNames(['Chris Hemsworth']),
              ]),
          ]),
          new CampaignBuilder('Red sorceress').withIterations([
            new IterationBuilder('Avengers: Infinity war')
              .withTestPlanItems([new IterationTestPlanItemBuilder('Hawkeye', 'READY')])
              .withTestSuites([
                new TestSuiteBuilder('Thanos', 'READY').withTestCaseNames(['Chris Evans']),
              ]),
          ]),
        ]),
      new ProjectBuilder('X men').withCampaignLibraryNodes([
        new CampaignBuilder('Iron Man').withIterations([
          new IterationBuilder('Avengers')
            .withTestPlanItems([new IterationTestPlanItemBuilder('Vision', 'UNTESTABLE')])
            .withTestSuites([
              new TestSuiteBuilder('Hydra', 'UNTESTABLE').withTestCaseNames(['Chris Pratt']),
            ]),
        ]),
      ]),
      new ProjectBuilder('Eternals').withCampaignLibraryNodes([new CampaignBuilder('spoderman')]),
    ])
    .withCustomColumnConfiguration([
      new CustomColumnConfigurationBuilder()
        .withPartyId(1)
        .withGridId('requirement-search')
        .withProjectId(1)
        .withActiveColumnIds(['name', 'coverages']),
      new CustomColumnConfigurationBuilder()
        .withPartyId(1)
        .withGridId('test-case-search')
        .withProjectId(1)
        .withActiveColumnIds(['name', 'coverages']),
      new CustomColumnConfigurationBuilder()
        .withPartyId(1)
        .withGridId('itpi-search')
        .withProjectId(1)
        .withActiveColumnIds(['projectName', 'campaignName', 'iterationName', 'tclnId', 'label']),
    ])
    .build();
}

function navigateToSystemAdministrationAndDesactivateAutomaticSearch() {
  NavBarElement.navigateToAdministrationSystem();
  const page = new SystemViewSettingsPage();
  page.anchors.clickLink('settings');
  const settingsActivationSwitch = page.searchActivationSwitch;
  settingsActivationSwitch.assertExists();
  settingsActivationSwitch.checkValue(false);
  settingsActivationSwitch.toggle();
  settingsActivationSwitch.checkValue(true);
  NavBarAdminElement.close();
}

function accessEmptyGridThenDisplayMethod(
  page: AbstractSearchPage,
  workspace: WorkspaceWithTreePage,
  grid: GridElement,
) {
  workspace.clickSearchButton();
  page.assertExists();
  grid.assertExists();
  grid.verifyEmptyGridMessage(
    'Sélectionnez vos critères de recherche et cliquez sur "Rechercher" pour afficher les résultats.',
  );

  page.clickSearchButton();
  grid.assertRowCount(3);
}

function assertGrisIsEmptyThenNotEmpty(page: AbstractSearchPage, grid: GridElement) {
  verifyEmptyGridMessage(grid);
  page.clickSearchButton();
  grid.assertRowCount(1);
}

function assertNoresultDisplayedForSearchAndColumnConfiguration(
  page: AbstractSearchPage,
  grid: GridElement,
) {
  page.clickSearchButton();
  grid.verifyEmptyGridMessage('Aucun élément à afficher');
  page.clickOnNewSearch();
  verifyEmptyGridMessage(grid);

  page.openColumnConfiguration();
  cy.get(selectByDataTestElementId('reset-configuration')).click();
  verifyEmptyGridMessage(grid);
}

function verifyEmptyGridMessage(grid: GridElement) {
  grid.verifyEmptyGridMessage('Cliquez sur "Rechercher" pour actualiser les résultats.');
}
