import { DatabaseUtils } from '../../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { AdminWorkspaceProjectsPage } from '../../../../../page-objects/pages/administration-workspace/admin-workspace-projects.page';
import { GridElement } from '../../../../../page-objects/elements/grid/grid.element';
import { ProjectViewPage } from '../../../../../page-objects/pages/administration-workspace/project-view/project-view.page';
import { NavBarElement } from '../../../../../page-objects/elements/nav-bar/nav-bar.element';
import { ProjectImportsPanelElement } from '../../../../../page-objects/pages/administration-workspace/project-view/panels/project-imports-panel.element';
import { CreateXrayImportDialog } from '../../../../../page-objects/pages/administration-workspace/project-view/dialogs/create-xray-import.dialog';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from 'cypress/integration/utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../../utils/end-to-end/api/add-permissions';

describe('Verify the import panel in project view', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    cy.logInAs('RonW', 'admin');
  });

  it('F01-UC0212.CT01 - should add an xray import', () => {
    const importName = 'Import XML';

    cy.log('Step 1');
    const adminWorkspaceProjectsPage = new AdminWorkspaceProjectsPage(
      GridElement.createGridElement('projects', 'generic-projects'),
    );
    const projectViewPage = new ProjectViewPage();
    accessProjectPage(adminWorkspaceProjectsPage, projectViewPage);
    projectViewPage.anchors.assertLinkExists('imports');
    const projectImportsPanel: ProjectImportsPanelElement = projectViewPage.showImportsPanel();
    projectImportsPanel.grid.assertExists();

    cy.log('Step 2');
    const xrayImportDialog: CreateXrayImportDialog = projectImportsPanel.openXrayImportDialog();

    cy.log('Step 3');
    xrayImportDialog.assertConfirmButtonExists();
    xrayImportDialog.assertCancelButtonExists();
    xrayImportDialog.clickOnConfirmButton();
    xrayImportDialog.assertImportNameHasError('sqtm-core.validation.errors.required');
    xrayImportDialog.assertImportFileHasMissingFileError();
    xrayImportDialog.fillImportName(importName);
    xrayImportDialog.clickOnConfirmButton();
    xrayImportDialog.assertImportFileHasMissingFileError();
    xrayImportDialog.assertImportNameHasNoError('sqtm-core.validation.errors.required');

    cy.log('Step 4');
    xrayImportDialog.chooseImportFile('xray-import-e2e.xml', 'text/xml');
    xrayImportDialog.assertImportFile('xray-import-e2e.xml');
    xrayImportDialog.confirmConfiguration();

    cy.log('Step 5');
    xrayImportDialog.assertConfirmButtonExists();
    xrayImportDialog.assertCancelButtonExists();
    xrayImportDialog.assertPreviousButtonExists();

    xrayImportDialog.assertXrayEntity('Test', 21);
    xrayImportDialog.assertXrayEntity('Pre-Condition', 3);
    xrayImportDialog.assertXrayEntity('Test Plan', 2);
    xrayImportDialog.assertXrayEntity('Test Execution', 1);
    xrayImportDialog.confirmConfirmation();

    cy.log('Step 6');
    xrayImportDialog.assertImportButtonExists();
    xrayImportDialog.assertCancelButtonExists();
    xrayImportDialog.assertPreviousButtonExists();

    xrayImportDialog.assertSquashEntity('TEST_CASE', 17, 4, 0);
    xrayImportDialog.assertSquashEntity('CALLED_TEST_CASE', 6, 0, 0);
    xrayImportDialog.assertSquashEntity('CAMPAIGN', 0, 2, 0);
    xrayImportDialog.assertSquashEntity('ITERATION', 0, 1, 0);
    xrayImportDialog.assertDownloadReport();
    xrayImportDialog.doXrayImport();
    xrayImportDialog.clickOnCloseButton();

    cy.log('Step 7');
    projectImportsPanel.grid.assertRowCount(1);
    projectImportsPanel.assertImportGridRow(1, importName, 'Xray');
    projectImportsPanel.deleteSingleImportInGrid(importName);
    projectImportsPanel.grid.assertGridIsEmpty();
  });
});

function initTestData() {
  new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('project importer', [
        ProfileBuilderPermission.MANAGE_PROJECT,
        ProfileBuilderPermission.PROJECT_IMPORT,
      ]),
    ])
    .withProjects([new ProjectBuilder('import_xray')])
    .build();
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}

function accessProjectPage(
  adminWorkspaceProjectsPage: AdminWorkspaceProjectsPage,
  projectViewPage: ProjectViewPage,
) {
  const menu = NavBarElement.showSubMenu('administration', 'administration-menu');
  menu.item('projects').assertExists();
  menu.item('projects').click();
  adminWorkspaceProjectsPage.assertExists();
  adminWorkspaceProjectsPage.grid.findRowId('name', 'import_xray').then((id) => {
    adminWorkspaceProjectsPage.grid
      .getCell(id, 'name')
      .textRenderer()
      .assertContainsText('import_xray');
  });
  adminWorkspaceProjectsPage.grid.findRowId('name', 'import_xray').then((id) => {
    adminWorkspaceProjectsPage.grid.getCell(id, 'name').findCellTextSpan().click();
  });
  projectViewPage.assertExists();
}
