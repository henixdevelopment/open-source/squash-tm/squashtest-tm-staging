import { DatabaseUtils } from '../../../../utils/database.utils';
import { AdminWorkspaceAiServersPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-ai-servers.page';
import { AiServerViewPage } from '../../../../page-objects/pages/administration-workspace/ai-server-view/ai-server-view.page';
import { AiTestCaseGenerationDialogElement } from '../../../../page-objects/elements/dialog/ai-test-case-generation-dialog.element';
import {
  AdminWorkspaceItem,
  NavBarAdminElement,
} from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { ProjectViewPage } from '../../../../page-objects/pages/administration-workspace/project-view/project-view.page';
import { CreateAiServerDialogElement } from '../../../../page-objects/pages/administration-workspace/dialogs/create-ai-server-dialog.element';
import { AiServerAuthPolicyPanelElement } from '../../../../page-objects/pages/administration-workspace/ai-server-view/panels/ai-server-auth-policy-panel.element';
import { AiServerConfigurationPanelElement } from '../../../../page-objects/pages/administration-workspace/ai-server-view/panels/ai-server-configuration-panel.element';
import { AiServerTestConfigurationPanelElement } from '../../../../page-objects/pages/administration-workspace/ai-server-view/panels/AiServerTestConfigurationPanelElement';

describe('Create, Test and Bind an artificial intelligence server', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    cy.logInAs('admin', 'admin');
  });

  it('F01-UC053.CT01 - should create, test and bind an ai server', () => {
    const payload =
      '{{}"model": "mistralai/Mistral-7B-Instruct-v0.3", "messages":[{{}"role": "system", "content": "You are an expert manual tester."{}}, {{}"role": "user","content": "Tu es un testeur manuel. Tu dois transformer une exigence en un ou plusieurs cas de test, rédigés en français. La description du cas de test doit consister en un résumé de l\'exigence fournie. Ta réponse doit suivre le modèle suivant mais ne nécessite pas de mettre des antislashs: \\"{{}\\"testCases\\":[{{}\\"name\\":\\"\\",\\"description\\":\\"\\",\\"prerequisites\\":\\"\\",\\"testSteps\\":[{{}\\"index\\":0,\\"action\\":\\"\\",\\"expectedResult\\":\\"\\"{}}]{}}]{}}\\" Voilà l\'exigence à transformer : {{}{{} Requirement {}}{}} "{}}]{}}';
    const requirement =
      "Le site de vente doit permettre d'ajouter et de supprimer des articles dans son panier. Il doit également inclure la possibilité de payer par carte bleue ou virement bancaire.";

    cy.log('Step 1');
    const page = AdminWorkspaceAiServersPage.initTestAtPageTestAiServers(null);
    page.assertExists();
    const addDialog = page.openCreateAiServer();
    createAiServer(addDialog);
    const aiServerViewPage = new AiServerViewPage();
    checkAiServerInformations(aiServerViewPage);

    cy.log('Step 2');
    const authenticationPanel = aiServerViewPage.authenticationPanel;
    saveToken(authenticationPanel, aiServerViewPage);

    cy.log('Step 3');
    const configurationPanel = aiServerViewPage.configurationPanel;
    fillPayloadAndJsonPAth(configurationPanel, payload);

    cy.log('Step 4');
    const testConfigurationPanel = aiServerViewPage.testConfigurationPanel;
    const aiTestCaseGenerationDialogElement = new AiTestCaseGenerationDialogElement();
    fillRequirementAndGenerateCT(testConfigurationPanel, requirement);
    verifyConformityTestCases(aiTestCaseGenerationDialogElement);

    cy.log('Step 5');
    const projectPage = NavBarAdminElement.navigateToAdministration(AdminWorkspaceItem.PROJECTS);
    linkAiServerToProject(projectPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder().withProjects([new ProjectBuilder('AZERTY')]).build();
}
function createAiServer(addDialog: CreateAiServerDialogElement) {
  addDialog.assertExists();
  addDialog.fillName('Jarvis');
  addDialog.fillUrl('https://api.together.xyz/v1/chat/completions');
  addDialog.clickOnAddButton();
}

function checkAiServerInformations(aiServerViewPage: AiServerViewPage) {
  aiServerViewPage.assertExists();
  aiServerViewPage.informationPanel.assertExists();
  aiServerViewPage.entityNameField.checkContent('Jarvis');
  aiServerViewPage.informationPanel.urlField.checkContent(
    'https://api.together.xyz/v1/chat/completions',
  );
  aiServerViewPage.assertIncompleteAiServerConfiguration();
}

function saveToken(
  authenticationPanel: AiServerAuthPolicyPanelElement,
  aiServerViewPage: AiServerViewPage,
) {
  authenticationPanel.assertExists();
  aiServerViewPage.clickAuthPolicyAnchor();
  authenticationPanel.fillTokenField(
    '4b6867bce0179c26c0f0ea881eb179768774934321055a5b5b7e27786a43d546',
  );
}

function verifyConformityTestCases(aiTestCaseGenerationDialogElement) {
  aiTestCaseGenerationDialogElement.assertExists();
  aiTestCaseGenerationDialogElement.expandGeneratedTestCase();
  aiTestCaseGenerationDialogElement.assertsNameFieldsAreNotEmpty();
  aiTestCaseGenerationDialogElement.clickOnCloseButton();
  aiTestCaseGenerationDialogElement.assertNotExist();
}

function linkAiServerToProject(projectPage) {
  const projectViewPage = new ProjectViewPage();
  projectPage.grid.selectRowWithMatchingCellContent('name', 'AZERTY');
  projectViewPage.selectAiServer('Jarvis');
}

function fillRequirementAndGenerateCT(
  testConfigurationPanel: AiServerTestConfigurationPanelElement,
  requirement: string,
) {
  testConfigurationPanel.assertExists();
  testConfigurationPanel.fillEmtpyRequirementField(requirement);
  testConfigurationPanel.callExternalAiServer();
}

function fillPayloadAndJsonPAth(
  configurationPanel: AiServerConfigurationPanelElement,
  payload: string,
) {
  configurationPanel.assertExists();
  configurationPanel.fillEmtpyPayloadField(payload);
  configurationPanel.fillEmtpyJsonPathField('choices[0].message.content');
}
