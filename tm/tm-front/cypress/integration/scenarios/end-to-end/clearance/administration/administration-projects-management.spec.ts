import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  addPermissionToProject,
  ApiPermissionGroup,
} from '../../../../utils/end-to-end/api/add-permissions';
import { BugtrackerServerBuilder } from '../../../../utils/end-to-end/prerequisite/builders/server-prerequisite-builders';
import { AuthenticationProtocol } from '../../../../../../projects/sqtm-core/src/lib/model/third-party-server/authentication.model';
import { CustomFieldBuilder } from '../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import {
  InfoListBuilder,
  InfoListItemBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/info-list-builder';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';

import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import { AdminWorkspaceProjectsPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-projects.page';
import { ProjectViewPage } from '../../../../page-objects/pages/administration-workspace/project-view/project-view.page';
import { ConfirmInfoListBindingAlert } from '../../../../page-objects/pages/administration-workspace/project-view/dialogs/confirm-info-list-binding.alert';
import { ConfigurePluginDialog } from '../../../../page-objects/pages/administration-workspace/project-view/dialogs/configure-plugin-dialog';
import { createRegExpForBugtrackerGitlabName } from '../../../../utils/end-to-end/end-to-end-helper';

describe('Verify that a Project Leader can perform these actions (consult and modify project, set up bugtracker, associate list, and activate plugin) on the Project in the Administration page', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07070.CT03 - administration-projects-management (Project leader)', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const adminWorkspaceProjectsPage = new AdminWorkspaceProjectsPage(
      GridElement.createGridElement('projects', 'generic-projects'),
    );
    const projectViewPage = new ProjectViewPage();
    accessProjectPage(adminWorkspaceProjectsPage, projectViewPage);

    cy.log('Step 2');
    modifyProjectInfoOnProjectViewPage(projectViewPage);

    cy.log('Step 3');
    addBugtrackerOnProjectViewPage(projectViewPage);

    cy.log('Step 4');
    addPermissionUserOnProjectViewPage(projectViewPage);

    cy.log('Step 5');
    modifyExecutionBlockParameters(projectViewPage);

    cy.log('Step 6');
    modifyAutomationBlockParameters(projectViewPage);

    cy.log('Step 7');
    modifyCustomFieldsAndLists(projectViewPage);

    cy.log('Step 8');
    createAssociateAndDissociateMilestone(projectViewPage);

    cy.log('Step 9');
    accessPluginManagement(projectViewPage);
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([
      new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley'),
      new UserBuilder('TonnyP').withFirstName('Tonny').withLastName('Parker'),
    ])
    .withServers([
      new BugtrackerServerBuilder('BugFix', 'https://gitlab.com', 'gitlab.bugtracker')
        .withAuthProtocol(AuthenticationProtocol.TOKEN_AUTH)
        .withUserLevelAuthPolicy(),
    ])
    .withInfoLists([
      new InfoListBuilder('LIST', 'Jaja').withItems([new InfoListItemBuilder('opt1', 'opt2')]),
    ])
    .withCufs([new CustomFieldBuilder('TestCuf', InputType.CHECKBOX)])
    .withProjects([new ProjectBuilder('Project-1'), new ProjectBuilder('Project-2')])
    .build();
}
function initPermissions() {
  addPermissionToProject(1, ApiPermissionGroup.PROJECT_MANAGER, [2]);
}

function accessProjectPage(
  adminWorkspaceProjectsPage: AdminWorkspaceProjectsPage,
  projectViewPage: ProjectViewPage,
) {
  const menu = NavBarElement.showSubMenu('administration', 'administration-menu');
  menu.item('projects').assertExists();
  menu.item('projects').click();
  adminWorkspaceProjectsPage.assertExists();
  adminWorkspaceProjectsPage.grid.findRowId('name', 'Project-1').then((id) => {
    adminWorkspaceProjectsPage.grid
      .getCell(id, 'name')
      .textRenderer()
      .assertContainsText('Project-1');
  });
  adminWorkspaceProjectsPage.grid.assertRowIdNotExist('name', 'Project-2');
  adminWorkspaceProjectsPage.grid.findRowId('name', 'Project-1').then((id) => {
    adminWorkspaceProjectsPage.grid.getCell(id, 'name').findCellTextSpan().click();
  });
  projectViewPage.assertExists();
}

function modifyProjectInfoOnProjectViewPage(projectViewPage: ProjectViewPage) {
  projectViewPage.labelTextField.setAndConfirmValue('Testing');
  projectViewPage.labelTextField.assertContainsText('Testing');
  projectViewPage.descriptionTextField.setAndConfirmValue('Description');
  projectViewPage.descriptionTextField.assertContainsText('Description');
}

function addBugtrackerOnProjectViewPage(projectViewPage: ProjectViewPage) {
  projectViewPage.changeBugtracker('BugFix');
  projectViewPage.bugtrackerSelectField.assertContainsText('BugFix');
}

function addPermissionUserOnProjectViewPage(projectViewPage: ProjectViewPage) {
  const dialog = projectViewPage
    .showPermissionsPanel()
    .permissionsPanel.clickOnAddUserPermissionButton();
  dialog.assertExists();
  dialog.selectParties('Tonny Parker (TonnyP)');
  dialog.selectProfile('Invité');
  dialog.confirm();
  projectViewPage.permissionsPanel.grid
    .findRowId('partyName', 'Tonny Parker (TonnyP)')
    .then((id) => {
      projectViewPage.permissionsPanel.grid
        .getCell(id, 'partyName')
        .textRenderer()
        .assertContainsText('Tonny Parker (TonnyP)');
    });
}

function modifyExecutionBlockParameters(projectViewPage: ProjectViewPage) {
  projectViewPage.clickOnSwitchAllowTcModifDuringExec();
  projectViewPage.clickOnSwitchSettledStatus();
  projectViewPage.clickOnSwitchUntestableStatus();
}

function modifyAutomationBlockParameters(projectViewPage: ProjectViewPage) {
  const automationPanel = projectViewPage.showAutomationPanel();
  automationPanel.bddTechnologyField.selectValueNoButton('Cucumber 4');
  automationPanel.bddTechnologyField.assertContainsText('Cucumber 4');
  automationPanel.bddScriptLanguageField.selectValueNoButton('Espagnol');
  automationPanel.bddScriptLanguageField.assertContainsText('Espagnol');
  automationPanel.selectAutomationWorkflowType('Squash avancé');
  automationPanel.workflowTypeField.assertContainsText('Squash avancé');
  automationPanel.automatedSuitesLifetimeField.setValue(200);
  automationPanel.automatedSuitesLifetimeField.confirm();
  automationPanel.automatedSuitesLifetimeField.assertContainsText('200');
}
function modifyCustomFieldsAndLists(projectViewPage: ProjectViewPage) {
  const dialog = projectViewPage.showCustomFieldsPanel().openCustomFieldBindingDialog();
  dialog.assertExists();
  dialog.selectEntity('Cas de test');
  dialog.selectCuf('TestCuf');
  dialog.confirm();
  projectViewPage.infoListsPanel.selectTypeList('LIST');
  const confirmDialog = new ConfirmInfoListBindingAlert();
  confirmDialog.confirmAttributeModification('type');
  projectViewPage.infoListsPanel.typeListSelectField.assertContainsText('LIST');
}
function createAssociateAndDissociateMilestone(projectViewPage: ProjectViewPage) {
  const dialog = projectViewPage.showMilestonesPanel().openMilestoneCreateAndBindDialog();
  dialog.assertExists();
  dialog.confirmCreateAndBindToProjectAndObjects('Jalon');
  projectViewPage.milestonesPanel.grid.findRowId('label', 'Jalon').then((id) => {
    projectViewPage.milestonesPanel.grid
      .getCell(id, 'range')
      .textRenderer()
      .assertContainsText('Restreinte');
  });
  projectViewPage.milestonesPanel.unbindOne('Jalon');
  projectViewPage.milestonesPanel.grid.assertGridIsEmpty();
}

function accessPluginManagement(projectViewPage: ProjectViewPage) {
  projectViewPage.anchors.clickLink('plugins');
  projectViewPage.pluginPanel.grid
    .findRowId('name', createRegExpForBugtrackerGitlabName('GitLab'))
    .then((id) => {
      projectViewPage.pluginPanel.grid.getCell(id, 'enabled').find('button').click();
    });
  projectViewPage.pluginPanel.grid
    .findRowId('name', createRegExpForBugtrackerGitlabName('GitLab'))
    .then((id) => {
      projectViewPage.pluginPanel.grid.getCell(id, 'configUrl').find('button').click();
    });
  const dialogConfigPluginGitlab = new ConfigurePluginDialog();
  dialogConfigPluginGitlab.assertExists();
  dialogConfigPluginGitlab.close();
  projectViewPage.pluginPanel.grid.findRowId('name', 'Xsquash4Jira').then((id) => {
    projectViewPage.pluginPanel.grid.getCell(id, 'enabled').find('button').click();
  });
  projectViewPage.pluginPanel.grid.findRowId('name', 'Xsquash4Jira').then((id) => {
    projectViewPage.pluginPanel.grid.getCell(id, 'configUrl').find('button').click();
  });
  const dialogConfigPluginXsquash4Jira = new ConfigurePluginDialog();
  dialogConfigPluginXsquash4Jira.assertExists();
  dialogConfigPluginXsquash4Jira.close();
}
