import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  addPermissionToProject,
  ApiPermissionGroup,
} from '../../../../utils/end-to-end/api/add-permissions';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import {
  AdminWorkspaceItem,
  NavBarAdminElement,
} from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import { MenuElement } from '../../../../utils/menu.element';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';

describe('Verify that the following administration menu options do not appear for a user with the Project Manager role', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07070.CT01 - administration-administrationmenu-management (Project leader)', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const menu = NavBarElement.showSubMenu('administration', 'administration-menu');
    verifyAdminMenuItems(menu);

    cy.log('Step 2');
    verifyAdminWorkspaceNavBar(menu);
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProjects([new ProjectBuilder('Project')])
    .build();
}
function initPermissions() {
  addPermissionToProject(1, ApiPermissionGroup.PROJECT_MANAGER, [2]);
}

function verifyAdminMenuItems(menu: MenuElement) {
  menu.item('projects').assertExists();
  menu.item('system').assertExists();
  menu.item('users').assertNotExist();
  menu.item('servers').assertNotExist();
  menu.item('milestones').assertExists();
  menu.item('entities-customization').assertNotExist();
}

function verifyAdminWorkspaceNavBar(menu: MenuElement) {
  menu.item('milestones').click();
  const navBarProjectManager = new NavBarAdminElement();
  navBarProjectManager.assertExists();
  navBarProjectManager.assertMenuItemsExists(AdminWorkspaceItem.PROJECTS);
  navBarProjectManager.assertMenuItemsExists(AdminWorkspaceItem.SYSTEM);
  navBarProjectManager.assertMenuItemsNotExists(AdminWorkspaceItem.SERVERS);
  navBarProjectManager.assertMenuItemsNotExists(AdminWorkspaceItem.ENTITIES);
  navBarProjectManager.assertMenuItemsExists(AdminWorkspaceItem.MILESTONES);
  navBarProjectManager.assertMenuItemsNotExists(AdminWorkspaceItem.USERS);
}
