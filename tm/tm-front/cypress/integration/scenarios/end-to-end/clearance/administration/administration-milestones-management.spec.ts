import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { addClearanceToProject } from '../../../../utils/end-to-end/api/add-permissions';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  MilestoneBuilder,
  MilestoneOnProjectBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/milestone-prerequisite-builders';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { AdminWorkspaceMilestonesPage } from '../../../../page-objects/pages/administration-workspace/admin-workspace-milestones.page';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import { MilestoneViewPage } from '../../../../page-objects/pages/administration-workspace/milestone-view/milestone-view.page';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import {
  milestoneManagerBuilderPermissions,
  ProfileBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Verify that a user with MANAGE_MILESTONE can create, modify, delete, associate, and dissociate milestones in the Administration page', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07070.CT03 - administration-milestones-management (Project leader)', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const adminWorkspaceMilestonesPage = new AdminWorkspaceMilestonesPage(
      GridElement.createGridElement('milestones', 'milestones'),
    );
    const milestoneViewPage = new MilestoneViewPage();
    verifyAccessMilestonesPage(adminWorkspaceMilestonesPage);

    cy.log('Step 2');
    addMilestone(adminWorkspaceMilestonesPage);

    cy.log('Step 3');
    viewAndEditMilestone(milestoneViewPage);

    cy.log('Step 4');
    verifyCanAssociateAndDissociateMilestone(milestoneViewPage);

    cy.log('Step 5');
    verifyNonOwnerCanViewButNotModifyMilestone(adminWorkspaceMilestonesPage, milestoneViewPage);

    cy.log('Step 6');
    verifyUserCanDuplicateAndSynchronizeMilestone(adminWorkspaceMilestonesPage, milestoneViewPage);
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([new ProfileBuilder('milestone manager', milestoneManagerBuilderPermissions)])
    .withMilestones([
      new MilestoneBuilder('Mars', '2025-10-31 14:44:45')
        .withUserLogin('RonW')
        .withStatus('IN_PROGRESS'),
    ])
    .withProjects([
      new ProjectBuilder('Project-1'),
      new ProjectBuilder('Project-2').withMilestonesOnProject([
        new MilestoneOnProjectBuilder('Mars'),
      ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, 11, [2]);
}

function verifyAccessMilestonesPage(adminWorkspaceMilestonesPage: AdminWorkspaceMilestonesPage) {
  const menu = NavBarElement.showSubMenu('administration', 'administration-menu');
  menu.item('milestones').assertExists();
  menu.item('milestones').click();
  adminWorkspaceMilestonesPage.assertExists();
}

function addMilestone(adminWorkspaceMilestonesPage: AdminWorkspaceMilestonesPage) {
  const dialog = adminWorkspaceMilestonesPage.openCreateMilestone();
  dialog.assertExists();
  dialog.fillName('milestone');
  dialog.setStatus('Planifié');
  dialog.setDeadlineDateToToday();
  dialog.clickOnAddButton();
}

function viewAndEditMilestone(milestoneViewPage: MilestoneViewPage) {
  milestoneViewPage.assertExists();
  milestoneViewPage.informationPanel.endDateField.setDate('15/09/2025');
  milestoneViewPage.informationPanel.endDateField.assertContainsText('15/09/2025');
  milestoneViewPage.informationPanel.descriptionField.assertExists();
  milestoneViewPage.informationPanel.descriptionField.click();
  milestoneViewPage.informationPanel.descriptionField.setValue('Description');
  milestoneViewPage.informationPanel.descriptionField.confirm();
  milestoneViewPage.informationPanel.descriptionField.assertContainsText('Description');
  milestoneViewPage.informationPanel.statusField.setAndConfirmValueNoButton('En cours');
  milestoneViewPage.informationPanel.statusField.assertContainsText('En cours');
  milestoneViewPage.informationPanel.rangeField.assertIsNotEditable();
  milestoneViewPage.informationPanel.assertOwnerIsEditable();
}

function verifyCanAssociateAndDissociateMilestone(milestoneViewPage: MilestoneViewPage) {
  milestoneViewPage.projectsPanel.grid.assertRowExist(1);
  milestoneViewPage.projectsPanel.unbindOne('Project-1');
  milestoneViewPage.projectsPanel.grid.assertGridIsEmpty();
  milestoneViewPage.closeMilestoneView();
}

function verifyNonOwnerCanViewButNotModifyMilestone(
  adminWorkspaceMilestonesPage: AdminWorkspaceMilestonesPage,
  milestoneViewPage: MilestoneViewPage,
) {
  adminWorkspaceMilestonesPage.grid.findRowId('label', 'Mars').then((id) => {
    adminWorkspaceMilestonesPage.grid.getCell(id, 'label').findCellTextSpan().click();
  });
  milestoneViewPage.assertExists();
  milestoneViewPage.informationPanel.endDateField.assertIsNotEditable();
  milestoneViewPage.informationPanel.descriptionField.assertIsNotEditable();
  milestoneViewPage.informationPanel.statusField.assertIsNotEditable();
  milestoneViewPage.informationPanel.rangeField.assertIsNotEditable();
  milestoneViewPage.informationPanel.assertOwnerIsNotEditable();
  milestoneViewPage.assertAddButtonProjectNotExist();
  milestoneViewPage.assertUnlinkButtonProjectNotExist();
  milestoneViewPage.projectsPanel.assertUnbindButtonNotExist('Project-2');
}

function verifyUserCanDuplicateAndSynchronizeMilestone(
  adminWorkspaceMilestonesPage: AdminWorkspaceMilestonesPage,
  milestoneViewPage: MilestoneViewPage,
) {
  const dialog = adminWorkspaceMilestonesPage.openDuplicateMilestone();
  dialog.assertExists();
  dialog.fillName('Jalon');
  dialog.setDeadlineDateToToday();
  dialog.clickOnAddButton();
  dialog.assertNotExist();
  milestoneViewPage.assertExists();
  milestoneViewPage.closeMilestoneView();
  milestoneViewPage.assertNotExist();
  adminWorkspaceMilestonesPage.assertExists();
  adminWorkspaceMilestonesPage.grid.findRowId('label', 'Jalon').then((id) => {
    adminWorkspaceMilestonesPage.grid
      .getCell(id, 'label')
      .textRenderer()
      .assertContainsText('Jalon');
  });
  cy.clickVoid();
  adminWorkspaceMilestonesPage.grid.assertRowIsNotSelected(1);
  adminWorkspaceMilestonesPage.grid.assertRowIsNotSelected(2);
  adminWorkspaceMilestonesPage.grid.assertRowIsNotSelected(3);
  adminWorkspaceMilestonesPage.grid.selectRows([1, 2], 'label');
  const dialogSyncro = adminWorkspaceMilestonesPage.openSynchronizeMilestone();
  dialogSyncro.assertExists();
  dialogSyncro.clickOnConfirmButton();
}
