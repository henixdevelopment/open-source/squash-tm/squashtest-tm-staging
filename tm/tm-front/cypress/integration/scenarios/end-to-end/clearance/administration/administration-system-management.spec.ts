import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  addPermissionToProject,
  ApiPermissionGroup,
} from '../../../../utils/end-to-end/api/add-permissions';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { MenuElement } from '../../../../utils/menu.element';
import { SystemViewSynchronizationSupervisionPage } from '../../../../page-objects/pages/administration-workspace/system-view/system-view-synchronization-supervision.page';

describe('Check if the Project Leader has access to synchronization supervision in the administration workspace, system subspace, but does not have access to users, projects, milestones, entities, and servers', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07070.CT04 - administration-system-management (Project leader)', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const menu = NavBarElement.showSubMenu('administration', 'administration-menu');
    const systemViewSynchronizationSupervisionPage = new SystemViewSynchronizationSupervisionPage();
    accessToSystemView(menu, systemViewSynchronizationSupervisionPage);

    cy.log('Step 2');
    enableAutoRefreshAndRefreshSynchronizationList(systemViewSynchronizationSupervisionPage);
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProjects([new ProjectBuilder('Project')])
    .build();
}
function initPermissions() {
  addPermissionToProject(1, ApiPermissionGroup.PROJECT_MANAGER, [2]);
}

function accessToSystemView(
  menu: MenuElement,
  systemViewSynchronizationSupervisionPage: SystemViewSynchronizationSupervisionPage,
) {
  menu.assertExists();
  menu.item('system').click();
  systemViewSynchronizationSupervisionPage.assertExists();
  systemViewSynchronizationSupervisionPage.anchors.assertAnchorCountNotExists('information');
  systemViewSynchronizationSupervisionPage.anchors.assertAnchorCountNotExists('settings');
  systemViewSynchronizationSupervisionPage.anchors.assertAnchorCountNotExists('messages');
  systemViewSynchronizationSupervisionPage.anchors.assertAnchorCountNotExists('report-templates');
  systemViewSynchronizationSupervisionPage.anchors.assertAnchorCountNotExists('cleaning');
  systemViewSynchronizationSupervisionPage.anchors.assertAnchorCountNotExists('downloads');
}

function enableAutoRefreshAndRefreshSynchronizationList(
  systemViewSynchronizationSupervisionPage: SystemViewSynchronizationSupervisionPage,
) {
  systemViewSynchronizationSupervisionPage.clickEnableAutomaticRefreshButton();
  systemViewSynchronizationSupervisionPage.clickRefreshButton();
}
