import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  TestAutomationProjectBindingBuilder,
  TestAutomationServerBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/server-prerequisite-builders';
import { TestAutomationServerKind } from '../../../../../../projects/sqtm-core/src/lib/model/test-automation/test-automation-server.model';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  KeywordTestCaseBuilder,
  TestCaseAutomatedTransmissionBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { AutomationWorkflowTypes } from '../../../../../../projects/sqtm-core/src/lib/model/plugin/project-plugin';
import { AutomationProgrammerAssigneePage } from '../../../../page-objects/pages/automation-workspace/automation-programmer/automation-programmer-assignee.page';
import { AutomationWorkspaceTestCaseViewPage } from '../../../../page-objects/pages/automation-workspace/automation-programmer/panels/automation-workspace-test-case-view.page';
import { ListPanelElement } from '../../../../page-objects/elements/filters/list-panel.element';
import { AutomationProgrammerTreatmentPage } from '../../../../page-objects/pages/automation-workspace/automation-programmer/automation-programmer-treatment.page';
import { AutomationProgrammerGlobalPage } from '../../../../page-objects/pages/automation-workspace/automation-programmer/automation-programmer-global.page';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import {
  clickCellInGrid,
  typeCellInGrid,
  verifyCellInGrid,
} from '../../../scenario-parts/automation.part';
import {
  addPermissionToProject,
  ApiPermissionGroup,
} from '../../../../utils/end-to-end/api/add-permissions';

describe('Verify that a Project Leader can access the Automation Programmer workspace, modify test case status, and update the reference and technology fields', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07035.CT01 - automation-autom-readwrite (Project leader)', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const menuAutomation = NavBarElement.showSubMenu('automation', 'automation-menu');
    menuAutomation.item('programmer').click();
    const automationProgrammerAssigneePage = new AutomationProgrammerAssigneePage(
      GridElement.createGridElement(
        'automation-programmer-assigned',
        'automation-workspace/assignee-autom-req',
      ),
    );
    consultAutomationProgrammerView(automationProgrammerAssigneePage);

    cy.log('Step 2');
    const automationProgrammerTreatmentPage =
      automationProgrammerAssigneePage.clickButtonTreatment();
    viewTestCaseFromAutomationProgrammerGlobalPage(automationProgrammerTreatmentPage);

    cy.log('Step 3');
    const automationProgrammerGlobalPage = automationProgrammerTreatmentPage.clickButtonGlobal();
    assignedTestCase(automationProgrammerGlobalPage);

    cy.log('Step 4');
    modifyTestCaseInformationViaTableInAutomationProgrammerGlobalPage(
      automationProgrammerGlobalPage,
    );

    cy.log('Step 5');
    updateAutomationStatusInAutomationProgrammerGlobalPage(automationProgrammerGlobalPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withServers([
      new TestAutomationServerBuilder(
        'Haha',
        'http://aaa.com',
        TestAutomationServerKind.squashOrchestrator,
      ),
    ])
    .withProjects([
      new ProjectBuilder('AZERTY')
        .withTestCaseLibraryNodes([
          new KeywordTestCaseBuilder('Yo')
            .withAutomatableStatus('Y')
            .withAutomationRequest([new TestCaseAutomatedTransmissionBuilder('TRANSMITTED')]),
        ])
        .withAutomationServerOnProject([
          new TestAutomationProjectBindingBuilder('Haha', AutomationWorkflowTypes.NATIVE),
        ]),
    ])
    .build();
}
function initPermissions() {
  addPermissionToProject(1, ApiPermissionGroup.PROJECT_MANAGER, [2]);
}

function consultAutomationProgrammerView(
  automationProgrammerAssigneePage: AutomationProgrammerAssigneePage,
) {
  automationProgrammerAssigneePage.assertTitlePage();
  const automationProgrammerTreatmentPage = automationProgrammerAssigneePage.clickButtonTreatment();
  automationProgrammerTreatmentPage.assertExists();
}

function viewTestCaseFromAutomationProgrammerGlobalPage(
  automationProgrammerTreatmentPage: AutomationProgrammerTreatmentPage,
) {
  const automationProgrammerGlobalPage = automationProgrammerTreatmentPage.clickButtonGlobal();
  automationProgrammerGlobalPage.assertExists();
  clickCellInGrid(automationProgrammerGlobalPage, 'projectName', 'AZERTY', 'name');
  const automationWorkspaceTestCaseViewPage = new AutomationWorkspaceTestCaseViewPage();
  automationWorkspaceTestCaseViewPage.assertExists();
  automationWorkspaceTestCaseViewPage.nameTextField.assertContainsText('Yo');
}

function assignedTestCase(automationProgrammerGlobalPage: AutomationProgrammerGlobalPage) {
  automationProgrammerGlobalPage.clickButtonAssignTestCase();
  verifyCellInGrid(automationProgrammerGlobalPage, 'projectName', 'AZERTY', 'assignedUser', 'RonW');
}

function modifyTestCaseInformationViaTableInAutomationProgrammerGlobalPage(
  automationProgrammerGlobalPage: AutomationProgrammerGlobalPage,
) {
  clickCellInGrid(
    automationProgrammerGlobalPage,
    'projectName',
    'AZERTY',
    'automatedTestTechnology',
  );
  const listPanel = new ListPanelElement();
  listPanel.findItem('JUnit').click();
  typeCellInGrid(
    automationProgrammerGlobalPage,
    'projectName',
    'AZERTY',
    'automatedTestReference',
    '1234',
  );
  verifyCellInGrid(
    automationProgrammerGlobalPage,
    'projectName',
    'AZERTY',
    'automatedTestTechnology',
    'JUnit',
  );
  verifyCellInGrid(
    automationProgrammerGlobalPage,
    'projectName',
    'AZERTY',
    'automatedTestReference',
    '1234',
  );
}

function updateAutomationStatusInAutomationProgrammerGlobalPage(
  automationProgrammerGlobalPage: AutomationProgrammerGlobalPage,
) {
  clickCellInGrid(automationProgrammerGlobalPage, 'projectName', 'AZERTY', 'name');
  automationProgrammerGlobalPage.clickButtonAutomatedTestCase();
  verifyCellInGrid(
    automationProgrammerGlobalPage,
    'projectName',
    'AZERTY',
    'requestStatus',
    'Automatisé',
  );
}
