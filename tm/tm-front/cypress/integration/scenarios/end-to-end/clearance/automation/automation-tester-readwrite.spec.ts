import { DatabaseUtils } from '../../../../utils/database.utils';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  SCMRepositoryBuilder,
  SCMServerBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/server-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  TestCaseAutomatedTransmissionBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { AutomationWorkflowTypes } from '../../../../../../projects/sqtm-core/src/lib/model/plugin/project-plugin';
import { AutomationWorkflowBuilder } from '../../../../utils/end-to-end/prerequisite/builders/automationWorkflow-builders';
import { FunctionalTesterReadyToTransmitPage } from '../../../../page-objects/pages/automation-workspace/functional-tester-workspace/functional-tester-ready-to-transmit.page';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import { FunctionalTesterTestCaseViewPage } from '../../../../page-objects/pages/automation-workspace/functional-tester-workspace/panels/functional-tester-test-case-view.page';
import { selectByDataIcon, selectByDataTestAnchorLinkId } from '../../../../utils/basic-selectors';
import {
  clickCellInGrid,
  typeCellInGrid,
  verifyCellInGrid,
} from '../../../scenario-parts/automation.part';
import {
  addPermissionToProject,
  ApiPermissionGroup,
} from '../../../../utils/end-to-end/api/add-permissions';

describe('Verify that a Project Leader can access the Automation tester workspace, modify test case status, and update the Scm repository', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07050.CT02 - automation-tester-readwrite (Project leader)', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const functionalTesterReadyToTransmitPage = new FunctionalTesterReadyToTransmitPage(
      GridElement.createGridElement(
        'functional-tester-ready-for-transmission',
        'automation-tester-workspace/ready-for-transmission',
      ),
    );
    const functionalTesterTestCaseViewPage = new FunctionalTesterTestCaseViewPage();
    consultAutomationTesterView(functionalTesterReadyToTransmitPage);

    cy.log('Step 2');
    clickReadyForTransmissionAndSetPriority(functionalTesterReadyToTransmitPage);

    cy.log('Step 3');
    viewTestCaseFromFunctionalTesterGlobalPage(
      functionalTesterReadyToTransmitPage,
      functionalTesterTestCaseViewPage,
    );

    cy.log('Step 4');
    modifyAutomationStatusTestCase(functionalTesterTestCaseViewPage);

    cy.log('Step 5');
    modifyAutomationBlockInformation(
      functionalTesterReadyToTransmitPage,
      functionalTesterTestCaseViewPage,
    );

    cy.log('Step 6');
    transmitTestAndVerifyTransmission(
      functionalTesterReadyToTransmitPage,
      functionalTesterTestCaseViewPage,
    );
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withServers([
      new SCMServerBuilder('edf', 'http://fake.com').withRepositories([
        new SCMRepositoryBuilder('edf', 'main'),
      ]),
    ])
    .withProjects([
      new ProjectBuilder('AZERTY')
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Yo')
            .withAutomatableStatus('Y')
            .withAutomationRequest([new TestCaseAutomatedTransmissionBuilder('READY_TO_TRANSMIT')]),
          new TestCaseBuilder('Lo')
            .withAutomatableStatus('Y')
            .withAutomationRequest([new TestCaseAutomatedTransmissionBuilder('READY_TO_TRANSMIT')]),
        ])
        .withWorkflowAutomationOnProject([
          new AutomationWorkflowBuilder(AutomationWorkflowTypes.NATIVE),
        ]),
    ])
    .build();
}
function initPermissions() {
  addPermissionToProject(1, ApiPermissionGroup.PROJECT_MANAGER, [2]);
}

function consultAutomationTesterView(
  functionalTesterReadyToTransmitPage: FunctionalTesterReadyToTransmitPage,
) {
  const menuAutomation = NavBarElement.showSubMenu('automation', 'automation-menu');
  menuAutomation.item('tester').click();
  functionalTesterReadyToTransmitPage.assertExists();
  const functionalTesterToBeValidatedPage =
    functionalTesterReadyToTransmitPage.clickButtonToBeValidated();
  functionalTesterToBeValidatedPage.assertTitlePage();
  const functionalTesterGlobalPage = functionalTesterToBeValidatedPage.clickButtonGlobal();
  functionalTesterGlobalPage.assertExists();
}

function clickReadyForTransmissionAndSetPriority(
  functionalTesterReadyToTransmitPage: FunctionalTesterReadyToTransmitPage,
) {
  cy.get(selectByDataTestAnchorLinkId('ready-for-transmission')).click();
  functionalTesterReadyToTransmitPage.assertExists();
  typeCellInGrid(functionalTesterReadyToTransmitPage, 'name', 'Yo', 'automationPriority', '5');
  verifyCellInGrid(functionalTesterReadyToTransmitPage, 'name', 'Yo', 'automationPriority', '5');
}

function viewTestCaseFromFunctionalTesterGlobalPage(
  functionalTesterReadyToTransmitPage: FunctionalTesterReadyToTransmitPage,
  functionalTesterTestCaseViewPage: FunctionalTesterTestCaseViewPage,
) {
  clickCellInGrid(functionalTesterReadyToTransmitPage, 'name', 'Yo', 'name');
  functionalTesterTestCaseViewPage.assertExists();
}

function modifyAutomationStatusTestCase(
  functionalTesterTestCaseViewPage: FunctionalTesterTestCaseViewPage,
) {
  functionalTesterTestCaseViewPage.testCaseViewPage.informationPanel.nameTextField.assertContainsText(
    'Yo',
  );
  functionalTesterTestCaseViewPage.testCaseViewPage.automationPanel.chooseAutomationStatus(
    'Suspendu',
  );
  functionalTesterTestCaseViewPage.testCaseViewPage.automationPanel.requestStatus.checkSelectedOption(
    'Suspendu',
  );
}

function modifyAutomationBlockInformation(
  functionalTesterReadyToTransmitPage: FunctionalTesterReadyToTransmitPage,
  functionalTesterTestCaseViewPage: FunctionalTesterTestCaseViewPage,
) {
  functionalTesterTestCaseViewPage.closeTestCaseView();
  clickCellInGrid(functionalTesterReadyToTransmitPage, 'name', 'Lo', 'name');
  functionalTesterTestCaseViewPage.testCaseViewPage.informationPanel.nameTextField.assertContainsText(
    'Lo',
  );
  functionalTesterTestCaseViewPage.testCaseViewPage.automationPanel.chooseScm('edf');
  functionalTesterTestCaseViewPage.testCaseViewPage.automationPanel.scmRepository.checkSelectedOption(
    ' http://fake.com/edf (main) ',
  );
}

function transmitTestAndVerifyTransmission(
  functionalTesterReadyToTransmitPage: FunctionalTesterReadyToTransmitPage,
  functionalTesterTestCaseViewPage: FunctionalTesterTestCaseViewPage,
) {
  functionalTesterTestCaseViewPage.closeTestCaseView();
  clickCellInGrid(functionalTesterReadyToTransmitPage, 'name', 'Lo', 'name');
  functionalTesterReadyToTransmitPage
    .find(selectByDataIcon('sqtm-core-automation:transmit'))
    .click();
  functionalTesterReadyToTransmitPage.grid.assertNotExists();
}
