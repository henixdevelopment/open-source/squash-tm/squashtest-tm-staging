import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { RequirementBuilder } from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  ActionTestStepBuilder,
  ExploratoryTestCaseBuilder,
  KeywordTestCaseBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  RequirementVersionsLinkToVerifyingTestCaseBuilder,
  TestCasesStepsLinkToRequirementVersionBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/link-builders';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { TestCaseViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  linkRequirementToATestCase,
  selectNodeInTestCaseWorkspace,
  selectProjectInTestCaseWorkspace,
  unlinkRequirementFromClassicTestCaseStep,
} from '../../../scenario-parts/testcase.part';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check if a user can associate a requirement to a test case and dissociate a requirement from a test step', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07022.CT01 - testcase-testcaseview-link', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage();
    const scriptedTestCaseViewPage = new TestCaseViewPage(2);
    const scriptedTestCaseCoverageTable = scriptedTestCaseViewPage.coveragesTable;
    const testCaseViewPage = new TestCaseViewPage(1);
    const testCaseCoverageTable = testCaseViewPage.coveragesTable;
    const keywordTestCaseViewPage = new TestCaseViewPage(3);
    const exploratoryTestCaseViewPage = new TestCaseViewPage(4);

    linkRequirementToAScriptedTestCase(
      testCaseWorkspacePage,
      scriptedTestCaseViewPage,
      scriptedTestCaseCoverageTable,
    );

    cy.log('Step 2');
    linkRequirementToAClassicTestCase(
      testCaseWorkspacePage,
      testCaseViewPage,
      testCaseCoverageTable,
    );

    cy.log('Step 3');
    unlinkRequirementFromClassicTestCaseStep(testCaseViewPage, 0, 0);

    cy.log('Step 4');
    unlinkRequirementFromKeywordTestCase(testCaseWorkspacePage, keywordTestCaseViewPage);

    cy.log('Step 5');
    unlinkRequirementFromExploratoryTestCase(testCaseWorkspacePage, exploratoryTestCaseViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('test case linker', [
        ProfileBuilderPermission.TEST_CASE_READ,
        ProfileBuilderPermission.TEST_CASE_LINK,
        ProfileBuilderPermission.REQUIREMENT_READ,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('AZERTY')
        .withRequirementLibraryNodes([
          new RequirementBuilder('Hey'),
          new RequirementBuilder('Hola'),
        ])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Classique').withSteps([
            new ActionTestStepBuilder('Je mange', 'Je bois'),
          ]),
          new ScriptedTestCaseBuilder('Gherkin'),
          new KeywordTestCaseBuilder('BDD'),
          new ExploratoryTestCaseBuilder('Exploratory'),
        ])
        .withLinks([
          new TestCasesStepsLinkToRequirementVersionBuilder('Classique', ['Hola']),
          new RequirementVersionsLinkToVerifyingTestCaseBuilder(['Hey'], 'BDD'),
          new RequirementVersionsLinkToVerifyingTestCaseBuilder(['Hey'], 'Exploratory'),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}
function linkRequirementToAScriptedTestCase(
  testCaseWorkspacePage: TestCaseWorkspacePage,
  scriptedTestCaseViewPage: TestCaseViewPage,
  scriptedTestCaseCoverageTable: GridElement,
) {
  selectNodeInTestCaseWorkspace(testCaseWorkspacePage, 'AZERTY', 'Gherkin');
  scriptedTestCaseCoverageTable.assertGridIsEmpty();
  linkRequirementToATestCase(scriptedTestCaseViewPage, true, 'AZERTY', 'Hola', 2);
}

function linkRequirementToAClassicTestCase(
  testCaseWorkspacePage: TestCaseWorkspacePage,
  testCaseViewPage: TestCaseViewPage,
  testCaseCoverageTable: GridElement,
) {
  selectProjectInTestCaseWorkspace(testCaseWorkspacePage, 'Classique');
  testCaseCoverageTable.findRowId('name', 'Hola').then((idRequirement) => {
    testCaseCoverageTable.assertRowExist(idRequirement);
  });
  linkRequirementToATestCase(testCaseViewPage, false, 'AZERTY', 'Hey', 1);
}

function unlinkRequirementFromKeywordTestCase(
  testCaseWorkspacePage: TestCaseWorkspacePage,
  keywordTestCaseViewPage: TestCaseViewPage,
) {
  selectProjectInTestCaseWorkspace(testCaseWorkspacePage, 'BDD');
  keywordTestCaseViewPage.deleteRequirementCoverageWithEndOfLineIcon('Hey');
  keywordTestCaseViewPage.coveragesTable.assertRowCount(0);
}

function unlinkRequirementFromExploratoryTestCase(
  testCaseWorkspacePage: TestCaseWorkspacePage,
  exploratoryTestCaseViewPage: TestCaseViewPage,
) {
  selectProjectInTestCaseWorkspace(testCaseWorkspacePage, 'Exploratory');
  exploratoryTestCaseViewPage.assertExists();
  exploratoryTestCaseViewPage.deleteRequirementCoverageWithTopIcon('Hey');
  exploratoryTestCaseViewPage.coveragesTable.assertRowCount(0);
}
