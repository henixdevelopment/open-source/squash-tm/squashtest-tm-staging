import { DatabaseUtils } from '../../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { TestCaseWorkspacePage } from '../../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { TestCaseViewSubPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-sub-page';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  MilestoneBuilder,
  MilestoneOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/milestone-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { BindableEntity } from '../../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { ExploratoryTestCaseBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CampaignBuilder,
  CampaignTestPlanItemBuilder,
  ExploratorySessionBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import {
  accessSecondLevelPageTestCaseThroughSearchAndSelectTestCase,
  assertAccessToCharterWhenSessionDurationNotVisible,
  assertCannotModifyCharterContent,
  assertSessionIsPresentInSessionPage,
  cannotAssociateMilestone,
  cannotDissociateMilestone,
  verifyCannotChangeHeaderNameAndStatusInKeywordTestCase,
} from '../../../../scenario-parts/testcase.part';
import { TestCaseViewPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';

describe('Check if a user with Guest cannot perform the following actions on the level 2 page of an Exploratory test case: edit attributes, rename the test case, modify the session duration and the exploratory test charter, edit the milestone.However, they can view the "Sessions" page.', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC072201.CT04 - testcase-testcasedetails-exploratory-readwrite-nok', () => {
    cy.log('step 1');
    cy.logInAs('RonW', 'admin');
    const testCaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const testCaseSubViewPage = new TestCaseViewSubPage('*');
    const testCaseViewPage = testCaseSubViewPage.testCaseViewPage;

    accessSecondLevelPageTestCaseThroughSearchAndSelectTestCase(testCaseWorkspace, 'Exploratory');

    cy.log('Step 2');
    verifyCannotChangeHeaderNameAndStatusInKeywordTestCase(testCaseViewPage);

    cy.log('Step 3');
    assertCannotModifyImportanceOrNumericField(testCaseViewPage, testCaseSubViewPage);

    cy.log('Step 4');
    cannotAssociateMilestone(testCaseViewPage);
    cannotDissociateMilestone(testCaseViewPage, 'Milestone');

    cy.log('Step 5');
    assertAccessToCharterWhenSessionDurationNotVisible(testCaseViewPage);

    cy.log('Step 6');
    assertCannotModifyCharterContent(testCaseViewPage);

    cy.log('Step 7');
    assertSessionIsPresentInSessionPage(testCaseViewPage);
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withMilestones([new MilestoneBuilder('Milestone', '10-12-2025')])
    .withCufs([new CustomFieldBuilder('Number', InputType.NUMERIC)])
    .withProjects([
      new ProjectBuilder('Project')
        .withCufsOnProject([new CustomFieldOnProjectBuilder('Number', BindableEntity.TEST_CASE)])
        .withMilestonesOnProject([new MilestoneOnProjectBuilder('Milestone')])
        .withTestCaseLibraryNodes([
          new ExploratoryTestCaseBuilder('Exploratory').withMilestones(['Milestone']),
        ])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaign')
            .withTestPlanItems([new CampaignTestPlanItemBuilder('Exploratory')])
            .withIterations([
              new IterationBuilder('Iteration').withTestPlanItems([
                new IterationTestPlanItemBuilder('Exploratory').withSessions([
                  new ExploratorySessionBuilder('Exploratory'),
                ]),
              ]),
            ]),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}
function assertCannotModifyImportanceOrNumericField(
  testCaseViewPage: TestCaseViewPage,
  testCaseSubViewPage: TestCaseViewSubPage,
) {
  const numericCuf = testCaseSubViewPage.getNumericCustomField('Number');
  const importanceField = testCaseViewPage.informationPanel.importanceSelectField;
  numericCuf.assertIsNotEditable();
  importanceField.assertIsNotEditable();
}
