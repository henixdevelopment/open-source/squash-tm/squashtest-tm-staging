import { DatabaseUtils } from '../../../../../utils/database.utils';
import { TestCaseWorkspacePage } from '../../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { SimpleDeleteConfirmDialogElement } from '../../../../../page-objects/elements/dialog/simple-delete-confirm-dialog.element';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  ExploratoryTestCaseBuilder,
  KeywordTestCaseBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
  TestCaseFolderBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { TreeElement } from '../../../../../page-objects/elements/grid/grid.element';
import { KeyboardShortcuts } from '../../../../../page-objects/elements/workspace-common/keyboard-shortcuts';
import { ToolbarButtonElement } from '../../../../../page-objects/elements/workspace-common/toolbar.element';

describe('Check that a Guest cannot delete test case inter/intra-project and test case folder inter/intra-project', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07221.CT04 - testcase-delete-createdelete-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testCaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const testCaseWorkspaceTree = testCaseWorkspace.tree;
    const deleteDialog = new SimpleDeleteConfirmDialogElement();

    assertCannotDeleteTestCases(testCaseWorkspaceTree, deleteDialog);

    cy.log('Step 2');
    assertCannotDeleteFolderContainingTestCases(testCaseWorkspaceTree, deleteDialog);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProjects([
      new ProjectBuilder('Project').withTestCaseLibraryNodes([
        new TestCaseBuilder('Classic'),
        new ScriptedTestCaseBuilder('Gherkin'),
        new KeywordTestCaseBuilder('BDD'),
        new ExploratoryTestCaseBuilder('Exploratory'),
        new TestCaseFolderBuilder('XFolder').withTestCaseLibraryNodes([
          new TestCaseBuilder('Classic'),
          new ScriptedTestCaseBuilder('Gherkin'),
          new KeywordTestCaseBuilder('BDD'),
          new ExploratoryTestCaseBuilder('Exploratory'),
        ]),
      ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}
function assertCannotDeleteTestCases(
  testCaseWorkSpaceTree: TreeElement,
  deleteDialog: SimpleDeleteConfirmDialogElement,
) {
  const deleteButtonTestToolbar = new ToolbarButtonElement(
    'sqtm-app-test-case-workspace-tree',
    'delete-button',
  );
  testCaseWorkSpaceTree.findRowId('NAME', 'Project').then((idProject) => {
    testCaseWorkSpaceTree.openNode(idProject);
  });
  testCaseWorkSpaceTree.selectNodesByName(['Classic', 'Gherkin', 'BDD', 'Exploratory']);
  deleteButtonTestToolbar.assertIsDisabled();
  deleteButtonTestToolbar.clickWithoutSpan();
  deleteDialog.assertNotExist();
}

function assertCannotDeleteFolderContainingTestCases(
  testCaseWorkSpaceTree: TreeElement,
  deleteDialog: SimpleDeleteConfirmDialogElement,
) {
  const deleteButtonShortcut = new KeyboardShortcuts();
  testCaseWorkSpaceTree.findRowId('NAME', 'XFolder').then((idFolder) => {
    testCaseWorkSpaceTree.selectNode(idFolder);
  });
  deleteButtonShortcut.pressDelete();
  deleteDialog.assertNotExist();
}
