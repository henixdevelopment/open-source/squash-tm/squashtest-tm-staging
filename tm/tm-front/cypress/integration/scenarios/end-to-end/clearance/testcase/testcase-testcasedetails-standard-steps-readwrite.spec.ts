import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import {
  ActionTestStepBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { RequirementBuilder } from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import { RequirementVersionsLinkToVerifyingTestCaseBuilder } from '../../../../utils/end-to-end/prerequisite/builders/link-builders';
import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { RequirementViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import { TestCaseViewSubPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-sub-page';
import { TestStepsViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-steps-view.page';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import { accessToSecondLevelViewPageOfTestCase } from '../../../scenario-parts/testcase.part';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check if a user can modify a test case step in Second Level View Page', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC070201.CT05 - testcase-testcasedetails-standard-steps-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspace = RequirementWorkspacePage.initTestAtPage();
    const requirementViewPage = new RequirementViewPage();
    const testcaseSecondLevelViewPage = new TestCaseViewSubPage(1);
    const testcaseStepsView = new TestStepsViewPage();

    accessToSecondLevelViewPageOfTestCase(
      requirementWorkspace,
      requirementViewPage,
      testcaseSecondLevelViewPage,
      'AZERTY',
      'DD',
      'Hello',
    );

    cy.log('Step 2');
    modifyPrerequisiteAndAddStep(testcaseSecondLevelViewPage, testcaseStepsView);

    cy.log('Step 3');
    moveOrderOfTestStep(testcaseStepsView);

    cy.log('Step 4');
    deleteSelectedTestStepWithTopIcon(testcaseStepsView);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('test case modifier', [
        ProfileBuilderPermission.TEST_CASE_READ,
        ProfileBuilderPermission.TEST_CASE_WRITE,
        ProfileBuilderPermission.REQUIREMENT_READ,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('AZERTY')
        .withRequirementLibraryNodes([new RequirementBuilder('DD')])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Hello').withSteps([
            new ActionTestStepBuilder('FFF', 'FFF'),
            new ActionTestStepBuilder('DDD', 'DDD'),
            new ActionTestStepBuilder('EEE', 'EEE'),
          ]),
        ])
        .withLinks([new RequirementVersionsLinkToVerifyingTestCaseBuilder(['DD'], 'Hello')]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}

function modifyPrerequisiteAndAddStep(
  testcaseSecondLevelViewPage: TestCaseViewSubPage,
  testcaseStepsView: TestStepsViewPage,
) {
  testcaseSecondLevelViewPage.testCaseViewPage.clickStepsAnchorLink();
  testcaseStepsView.expendAll();
  testcaseStepsView.fillPrerequisiteFieldAndConfirm('Fraise');
  testcaseStepsView.getActionStepByIndex(0).clickActionStepButtonAdd();
  const newActionStep = testcaseStepsView.getActionStepByIndex(1);
  newActionStep.getActionFieldForNewStep().clear();
  newActionStep.getActionFieldForNewStep().fill('Bonjour');
  newActionStep.getExpectedResultFieldForNewStep().clear();
  newActionStep.getExpectedResultFieldForNewStep().fill('Bonjour');
  cy.clickVoid();
  newActionStep.clickButtonAdd();
  newActionStep.assertActionButtonBarIsVisible();
  newActionStep.getActionField().assertContainsText('Bonjour');
  newActionStep.getExpectedResultField().assertContainsText('Bonjour');
  testcaseStepsView.checkOrder([1, 4, 2, 3]);
}

function moveOrderOfTestStep(testcaseStepsView: TestStepsViewPage) {
  testcaseStepsView.moveStepUp('4');
  testcaseStepsView.moveStepDown('1');
  cy.wait(500);
  testcaseStepsView.getActionStepByIndex(0).checkCalledTestCaseStep('Bonjour', '4', 0);
  testcaseStepsView.getActionStepByIndex(2).checkCalledTestCaseStep('FFF', '1', 2);
  testcaseStepsView.checkOrder([4, 2, 1, 3]);
}

function deleteSelectedTestStepWithTopIcon(testcaseStepsView: TestStepsViewPage) {
  testcaseStepsView.deleteStepWithTopIcon(1, '2');
  testcaseStepsView.checkOrder([4, 1, 3]);
}
