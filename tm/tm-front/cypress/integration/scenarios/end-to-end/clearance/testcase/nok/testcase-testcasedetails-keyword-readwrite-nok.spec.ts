import { DatabaseUtils } from '../../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { BindableEntity } from '../../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import {
  DataSetBuilder,
  DataSetParameterValueBuilder,
  KeywordStepBuilder,
  KeywordTestCaseBuilder,
  ParameterBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { NavBarElement } from '../../../../../page-objects/elements/nav-bar/nav-bar.element';
import { ActionWordViewPage } from '../../../../../page-objects/pages/action-word-workspace/action-word/action-word-view.page';
import { TestCaseViewSubPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-sub-page';
import {
  assertCannotAddTechnologyToKeywordTestCase,
  verifyCannotChangeHeaderOnTestCaseViewPage,
} from '../../../../scenario-parts/testcase.part';
import { getToSecondLevelViewPageOfTestCase } from '../../../../scenario-parts/actionword.part';
import { TestCaseViewPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { TestCaseViewParametersPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-parameters.page';

describe('Check if a user with Guest cannot perform the following actions on the level 2 page of a BDD test case: edit attributes, edit the reference of the automated test, edit the name of a test data set (JDD), duplicate a test data set (JDD), edit the description of a parameter, delete a test data set (JDD), edit the name of a parameter, delete a parameter.', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC070201.CT02 - testcase-testcasedetails-keyword-readwrite-nok', () => {
    cy.log('step 1');
    cy.logInAs('RonW', 'admin');
    const actionWordWorkspace = NavBarElement.navigateToActionWordWorkspace();
    const actionWordViewPage = new ActionWordViewPage();
    const keywordTestCaseSecondLevelViewPage = new TestCaseViewSubPage(1);
    const keywordViewPage = keywordTestCaseSecondLevelViewPage.testCaseViewPage;
    getToSecondLevelViewPageOfTestCase(
      actionWordWorkspace,
      actionWordViewPage,
      keywordTestCaseSecondLevelViewPage,
      'Project',
      'I have "number" apples',
    );

    cy.log('Step 2');
    verifyCannotChangeHeaderOnTestCaseViewPage(keywordViewPage);

    cy.log('Step 3');
    assertCannotModifyStatusRichTextOrDescription(
      keywordTestCaseSecondLevelViewPage,
      keywordViewPage,
    );

    cy.log('Step 4');
    assertCannotAddTechnologyToKeywordTestCase(keywordViewPage);

    cy.log('Step 5');
    const dataSetViewPage = keywordViewPage.clickParametersAnchorLink();
    dataSetViewPage.unableToChangeParamValue('Appele', 'number');

    cy.log('Step 6');
    dataSetViewPage.assertDuplicateDataSetIsNotClickable('Appele');

    cy.log('Step 7');
    assertCannotModifyParameterDescription(dataSetViewPage);
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withCufs([new CustomFieldBuilder('Text', InputType.RICH_TEXT)])
    .withProjects([
      new ProjectBuilder('Project')
        .withCufsOnProject([new CustomFieldOnProjectBuilder('Text', BindableEntity.TEST_CASE)])
        .withTestCaseLibraryNodes([
          new KeywordTestCaseBuilder('BDD')
            .withKeywordSteps([new KeywordStepBuilder('GIVEN', 'I have "number" apples ')])
            .withParameters([new ParameterBuilder('number')])
            .withDataSets([
              new DataSetBuilder('Appele').withParamValues([
                new DataSetParameterValueBuilder('number', '5'),
              ]),
            ]),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}
function assertCannotModifyStatusRichTextOrDescription(
  keywordTestCaseSecondLevelViewPage: TestCaseViewSubPage,
  keywordViewPage: TestCaseViewPage,
) {
  const cufTextRich = keywordTestCaseSecondLevelViewPage.getTagCustomField('Text');
  const descriptionRichField = keywordViewPage.informationPanel.descriptionRichField;
  const statusCapsule = keywordViewPage.statusCapsule;
  cufTextRich.assertExists();
  cufTextRich.assertIsNotEditable();
  statusCapsule.assertIsNotClickable();
  descriptionRichField.assertIsNotEditable();
}
function assertCannotModifyParameterDescription(dataSetViewPage: TestCaseViewParametersPage) {
  const parameterInformationDialog = dataSetViewPage.openInformationParamDialog('PARAM_COLUMN_1');
  parameterInformationDialog.assertExists();
  parameterInformationDialog.assertExists();
  parameterInformationDialog.getDescriptionField().assertIsNotEditable();
}
