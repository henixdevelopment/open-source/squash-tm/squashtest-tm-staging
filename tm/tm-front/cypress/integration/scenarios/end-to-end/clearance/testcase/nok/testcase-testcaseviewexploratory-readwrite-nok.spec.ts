import { DatabaseUtils } from '../../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
  ListOptionCustomFieldBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { BindableEntity } from '../../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { ExploratoryTestCaseBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { TestCaseWorkspacePage } from '../../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { TestCaseViewPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import {
  assertAccessToCharterWhenSessionDurationNotVisible,
  assertCanEditTypeFieldAndSelectDropdownOption,
  assertCannotModifyCharterContent,
  selectNodeInTestCaseWorkspace,
  verifyCannotChangeHeaderOnTestCaseViewPage,
} from '../../../../scenario-parts/testcase.part';

describe('Check if a user with Guest cannot perform the following actions on the Test Case Exploration: modify attributes, modify the reference of a test case, modify the charter and duration of the exploratory test', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07220.CT05 - testcase-testcaseviewexploratory-readwrite-nok', () => {
    cy.log('step 1');
    cy.logInAs('RonW', 'admin');
    const testcaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const exploratoryTestCaseViewPage = new TestCaseViewPage(1);
    selectNodeInTestCaseWorkspace(testcaseWorkspace, 'Project', 'Exploratory');
    verifyCannotChangeHeaderOnTestCaseViewPage(exploratoryTestCaseViewPage);

    cy.log('Step 2');
    assertCanEditTypeFieldAndSelectDropdownOption(exploratoryTestCaseViewPage);

    cy.log('Step 3');
    assertAccessToCharterWhenSessionDurationNotVisible(exploratoryTestCaseViewPage);

    cy.log('Step 4');
    assertCannotModifyCharterContent(exploratoryTestCaseViewPage);
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withCufs([
      new CustomFieldBuilder('Progress status', InputType.DROPDOWN_LIST).withOptions([
        new ListOptionCustomFieldBuilder('Backlog', 'Backlog'),
        new ListOptionCustomFieldBuilder('InProgress', 'InProgress'),
        new ListOptionCustomFieldBuilder('Implemented', 'Implemented'),
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project')
        .withCufsOnProject([
          new CustomFieldOnProjectBuilder('Progress status', BindableEntity.TEST_CASE),
        ])
        .withTestCaseLibraryNodes([new ExploratoryTestCaseBuilder('Exploratory')]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}
