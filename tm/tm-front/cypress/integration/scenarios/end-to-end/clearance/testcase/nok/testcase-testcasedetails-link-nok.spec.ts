import { DatabaseUtils } from '../../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { RequirementBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  ActionTestStepBuilder,
  ExploratoryTestCaseBuilder,
  KeywordTestCaseBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { RequirementVersionsLinkToVerifyingTestCaseBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/link-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { accessToSecondLevelViewPageOfTestCase } from '../../../../scenario-parts/testcase.part';
import { RequirementWorkspacePage } from '../../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { RequirementViewPage } from '../../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import { TestCaseViewSubPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-sub-page';
import { TestStepsViewPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-steps-view.page';
import { NavBarElement } from '../../../../../page-objects/elements/nav-bar/nav-bar.element';
import { TestCaseViewPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { TreeElement } from '../../../../../page-objects/elements/grid/grid.element';

describe('Check if a user with Guest cannot associate a requirement to a Exploratory test case, to a test step (classic test case) and disassociate a requirement from a Classic, BDD and Gherkin test case', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07222.CT04 - testcase-testcasedetails-link-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspace = RequirementWorkspacePage.initTestAtPage();
    const requirementViewPage = new RequirementViewPage();
    const testCaseViewSubPage = new TestCaseViewSubPage(1);
    const testCaseViewPage = testCaseViewSubPage.testCaseViewPage;
    const testStepView = new TestStepsViewPage();

    assertCannotLinkRequirementBySecondLevelViewPageInClassicTestStep(
      requirementWorkspace,
      requirementViewPage,
      testCaseViewSubPage,
      testCaseViewPage,
      testStepView,
    );

    cy.log('Step 2');
    assertCannotUnlinkRequirementFromClassicTestStep(testCaseViewPage);

    cy.log('Step 3');
    assertCannotUnlinkRequirementFromSecondLevelViewPageInKeywordTestCase(
      requirementWorkspace,
      requirementViewPage,
      testCaseViewSubPage,
      testCaseViewPage,
    );

    cy.log('Step 4');
    assertCannotLinkRequirementBySecondLevelViewPageInExploratoryTestCase(
      requirementWorkspace,
      requirementViewPage,
      testCaseViewSubPage,
      testCaseViewPage,
    );

    cy.log('Step 5');
    assertCannotUnlinkRequirementFromSecondLevelViewPageInScriptedTestCase(
      requirementWorkspace,
      requirementViewPage,
      testCaseViewSubPage,
      testCaseViewPage,
    );
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProjects([
      new ProjectBuilder('Project')
        .withRequirementLibraryNodes([
          new RequirementBuilder('Req'),
          new RequirementBuilder('Exig'),
        ])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Classic').withSteps([new ActionTestStepBuilder('Action', 'Result')]),
          new ScriptedTestCaseBuilder('Gherkin'),
          new KeywordTestCaseBuilder('BDD'),
          new ExploratoryTestCaseBuilder('Exploratory'),
        ])
        .withLinks([
          new RequirementVersionsLinkToVerifyingTestCaseBuilder(['Req'], 'Classic'),
          new RequirementVersionsLinkToVerifyingTestCaseBuilder(['Req'], 'BDD'),
          new RequirementVersionsLinkToVerifyingTestCaseBuilder(['Req'], 'Gherkin'),
          new RequirementVersionsLinkToVerifyingTestCaseBuilder(['Req'], 'Exploratory'),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}

function assertCannotLinkRequirementBySecondLevelViewPageInClassicTestStep(
  requirementWorkspace: RequirementWorkspacePage,
  requirementViewPage: RequirementViewPage,
  classicTestCaseViewPage: TestCaseViewSubPage,
  testCaseViewPage: TestCaseViewPage,
  testStepView: TestStepsViewPage,
) {
  const requirementTree = TreeElement.createTreeElement('requirement-tree-picker');
  accessToSecondLevelViewPageOfTestCase(
    requirementWorkspace,
    requirementViewPage,
    classicTestCaseViewPage,
    'Project',
    'Req',
    'Classic',
  );
  testCaseViewPage.clickStepsAnchorLink();
  testStepView.getActionStepByIndex(0).extendStep();
  testStepView
    .getActionStepByIndex(0)
    .clickActionStepActionButtonAndAssertOptionInMenuIsDisableAndClick('add-requirement-coverage');
  requirementTree.assertNotExists();
}

function assertCannotUnlinkRequirementFromClassicTestStep(
  classicTestCaseViewPage: TestCaseViewPage,
) {
  const classicTestCaseCoverageTable = classicTestCaseViewPage.coveragesTable;
  classicTestCaseViewPage.clickVerifiedRequirementsAnchorLink();
  classicTestCaseCoverageTable.findRowId('name', 'Req').then((idRequirement) => {
    classicTestCaseCoverageTable
      .getRow(idRequirement, 'rightViewport')
      .cell('delete')
      .assertNotExist();
  });
}

function assertCannotUnlinkRequirementFromSecondLevelViewPageInKeywordTestCase(
  requirementWorkspace: RequirementWorkspacePage,
  requirementViewPage: RequirementViewPage,
  keywordTestCaseViewSubPage: TestCaseViewSubPage,
  keywordTestCaseViewPage: TestCaseViewPage,
) {
  const keywordTestCaseCoverageTable = keywordTestCaseViewPage.coveragesTable;
  NavBarElement.navigateToRequirementWorkspace();
  accessToSecondLevelViewPageOfTestCase(
    requirementWorkspace,
    requirementViewPage,
    keywordTestCaseViewSubPage,
    'Project',
    'Req',
    'BDD',
  );
  keywordTestCaseViewPage.clickVerifiedRequirementsAnchorLink();
  keywordTestCaseCoverageTable.findRowId('name', 'Req').then((idReq) => {
    keywordTestCaseCoverageTable.selectRow(idReq, '#', 'leftViewport');
  });
  keywordTestCaseViewSubPage.assertIconNotExists('remove-coverages');
}

function assertCannotLinkRequirementBySecondLevelViewPageInExploratoryTestCase(
  requirementWorkspace: RequirementWorkspacePage,
  requirementViewPage: RequirementViewPage,
  exploratoryTestCaseViewSubPage: TestCaseViewSubPage,
  exploratoryTestCaseViewPage: TestCaseViewPage,
) {
  NavBarElement.navigateToRequirementWorkspace();
  accessToSecondLevelViewPageOfTestCase(
    requirementWorkspace,
    requirementViewPage,
    exploratoryTestCaseViewSubPage,
    'Project',
    'Req',
    'Exploratory',
  );
  exploratoryTestCaseViewPage.clickVerifiedRequirementsAnchorLink();
  exploratoryTestCaseViewSubPage.assertIconNotExists('add-coverages');
}

function assertCannotUnlinkRequirementFromSecondLevelViewPageInScriptedTestCase(
  requirementWorkspace: RequirementWorkspacePage,
  requirementViewPage: RequirementViewPage,
  gherkinTestCaseViewSubPage: TestCaseViewSubPage,
  gherkinTestCaseViewPage: TestCaseViewPage,
) {
  const gherkinTestCaseCoverageTable = gherkinTestCaseViewPage.coveragesTable;
  NavBarElement.navigateToRequirementWorkspace();
  accessToSecondLevelViewPageOfTestCase(
    requirementWorkspace,
    requirementViewPage,
    gherkinTestCaseViewSubPage,
    'Project',
    'Req',
    'Gherkin',
  );
  gherkinTestCaseViewPage.clickVerifiedRequirementsAnchorLink();
  gherkinTestCaseCoverageTable.findRowId('name', 'Req').then((idRequirement) => {
    gherkinTestCaseCoverageTable
      .getRow(idRequirement, 'rightViewport')
      .cell('delete')
      .assertNotExist();
  });
}
