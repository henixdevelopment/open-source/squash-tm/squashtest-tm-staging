import { DatabaseUtils } from '../../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  MilestoneBuilder,
  MilestoneOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/milestone-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { ScriptedTestCaseBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CampaignBuilder,
  CampaignTestPlanItemBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
  ScriptedExecutionBuilder,
  TestSuiteBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { BindableEntity } from '../../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import {
  assertCanAccessScriptPageButCannotEdit,
  assertCannotAddTechnologyToKeywordTestCase,
  assertCannotAssociateOrDissociateMilestoneInTestCaseViewPage,
  assertCannotChangeImportantCheckboxValueAndEditDescription,
  selectNodeInTestCaseWorkspace,
  verifyCannotChangeHeaderNameAndStatusInKeywordTestCase,
} from '../../../../scenario-parts/testcase.part';
import { TestCaseWorkspacePage } from '../../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { TestCaseViewPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';

describe('Check if Project Leader can read modify and interact with a scripted testcase', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07020.CT04 - testcase-testcaseviewscripted-readwrite-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testCaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const scriptedTestCaseViewPage = new TestCaseViewPage(1);
    selectNodeInTestCaseWorkspace(testCaseWorkspace, 'Project', 'TestA');
    verifyCannotChangeHeaderNameAndStatusInKeywordTestCase(scriptedTestCaseViewPage);

    cy.log('Step 2');
    assertCannotChangeImportantCheckboxValueAndEditDescription(scriptedTestCaseViewPage);

    cy.log('Step 3');
    assertCannotAssociateOrDissociateMilestoneInTestCaseViewPage(
      testCaseWorkspace,
      scriptedTestCaseViewPage,
      'Project',
      'TestB',
      'Milestone',
    );

    cy.log('Step 4');
    assertCannotAddTechnologyToKeywordTestCase(scriptedTestCaseViewPage);

    cy.log('Step 5');
    assertCanAccessScriptPageButCannotEdit(scriptedTestCaseViewPage);

    cy.log('Step 6');
    assertExecutionAnchorDisplaysTableContent(testCaseWorkspace, scriptedTestCaseViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withMilestones([
      new MilestoneBuilder('Milestone', '2025-10-31 14:44:45').withUserLogin('RonW'),
    ])
    .withCufs([new CustomFieldBuilder('Auto', InputType.CHECKBOX)])
    .withProjects([
      new ProjectBuilder('Project')
        .withTestCaseLibraryNodes([
          new ScriptedTestCaseBuilder('TestA'),
          new ScriptedTestCaseBuilder('TestB').withMilestones(['Milestone']),
        ])
        .withMilestonesOnProject([new MilestoneOnProjectBuilder('Milestone')])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaign')
            .withTestPlanItems([new CampaignTestPlanItemBuilder('TestA').withUserLogin('RonW')])
            .withIterations([
              new IterationBuilder('Iteration')
                .withTestPlanItems([
                  new IterationTestPlanItemBuilder('TestA')
                    .withUserLogin('RonW')
                    .withExecutions([new ScriptedExecutionBuilder()]),
                ])
                .withTestSuites([new TestSuiteBuilder('Suite').withTestCaseNames(['TestA'])]),
            ]),
        ])
        .withCufsOnProject([new CustomFieldOnProjectBuilder('Auto', BindableEntity.TEST_CASE)]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}

function assertExecutionAnchorDisplaysTableContent(
  testCaseWorkspace: TestCaseWorkspacePage,
  scriptedTestCaseViewPage: TestCaseViewPage,
) {
  selectNodeInTestCaseWorkspace(testCaseWorkspace, 'Project', 'TestA');
  const versionExecution = scriptedTestCaseViewPage.showExecutions('');
  versionExecution.assertExists();
}
