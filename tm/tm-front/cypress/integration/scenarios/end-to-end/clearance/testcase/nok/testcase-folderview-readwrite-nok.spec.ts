import { DatabaseUtils } from '../../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';

import { TestCaseFolderBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
  ListOptionCustomFieldBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { BindableEntity } from '../../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { TestCaseWorkspacePage } from '../../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { TestCaseFolderViewPage } from '../../../../../page-objects/pages/test-case-workspace/test-case-folder/test-case-folder-view.page';
import {
  cannotEditDescriptionAndChangeTagValue,
  clickSettingsAndSelectSortingOrder,
  selectNodeInTestCaseWorkspace,
  selectProjectInTestCaseWorkspace,
  verifyNameFieldIsNotClickableAndEditable,
} from '../../../../scenario-parts/testcase.part';
import { TestCaseLibraryViewPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-case-library-view.page';

describe('Check if a user with Guest can perform the following actions in the Test Case workspace ( View the folder page, Print the folder) But cannot ( Modify attributes,Rename the folder).', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07220.CT01 - testcase-folderview-readwrite-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testcaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const testCaseFolderViewPage = new TestCaseFolderViewPage(1);
    assetCannotChangeName(testcaseWorkspace, testCaseFolderViewPage);

    cy.log('Step 2');
    selectProjectAndFolderAndCheckIfParameterButtonWork(testcaseWorkspace, testCaseFolderViewPage);

    cy.log('Step 3');
    assertCannotChangeDescriptionAndTagOfTestcaseFolder(testCaseFolderViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withCufs([
      new CustomFieldBuilder('TagCuf', InputType.TAG).withOptions([
        new ListOptionCustomFieldBuilder('opt1', 'code1'),
        new ListOptionCustomFieldBuilder('opt2', 'code12'),
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project')
        .withTestCaseLibraryNodes([
          new TestCaseFolderBuilder('Folder'),
          new TestCaseFolderBuilder('Application'),
        ])
        .withCufsOnProject([
          new CustomFieldOnProjectBuilder('TagCuf', BindableEntity.TESTCASE_FOLDER),
        ]),
    ])
    .build();
}

function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}

function assetCannotChangeName(
  testcaseWorkspace: TestCaseWorkspacePage,
  testCaseFolderViewPage: TestCaseFolderViewPage,
) {
  selectNodeInTestCaseWorkspace(testcaseWorkspace, 'Project', 'Folder');
  verifyNameFieldIsNotClickableAndEditable(testCaseFolderViewPage);
}

function selectProjectAndFolderAndCheckIfParameterButtonWork(
  testcaseWorkspace: TestCaseWorkspacePage,
  testCaseFolderViewPage: TestCaseFolderViewPage,
) {
  clickSettingsAndSelectSortingOrder(testcaseWorkspace, testCaseFolderViewPage);
  selectProjectInTestCaseWorkspace(testcaseWorkspace, 'Project');
  const testCaseLibraryViewPage = new TestCaseLibraryViewPage('*');
  testCaseLibraryViewPage.assertExists();
  selectNodeInTestCaseWorkspace(testcaseWorkspace, 'Project', 'Folder');
  testCaseFolderViewPage.assertExists();
}

function assertCannotChangeDescriptionAndTagOfTestcaseFolder(
  testCaseFolderViewPage: TestCaseFolderViewPage,
) {
  cannotEditDescriptionAndChangeTagValue(testCaseFolderViewPage);
}
