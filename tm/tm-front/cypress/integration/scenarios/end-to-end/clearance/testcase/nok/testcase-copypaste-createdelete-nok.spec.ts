import { DatabaseUtils } from '../../../../../utils/database.utils';
import { TestCaseWorkspacePage } from '../../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { TreeToolbarElement } from '../../../../../page-objects/elements/workspace-common/tree-toolbar.element';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  ExploratoryTestCaseBuilder,
  KeywordTestCaseBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
  TestCaseFolderBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { copyAndPasteTestCaseInOtherProject } from '../../../../scenario-parts/testcase.part';
import { TreeElement } from '../../../../../page-objects/elements/grid/grid.element';

describe('Check if a user with Guest cannot copy/paste test case, test case folder and scripted/keyword test case', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07221.CT02 - testcase-copypaste-createdelete-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testCaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const testCaseWorkspaceTree = testCaseWorkspace.tree;
    const testCaseWorkspaceTreeToolBar = new TreeToolbarElement('test-case-toolbar');
    assertCannotCopyAndPasteTestCasesInSameProject(
      testCaseWorkspaceTree,
      testCaseWorkspaceTreeToolBar,
    );

    cy.log('Step 2');
    assertCannotCopyAndPasteTestCasesInOtherProject(
      testCaseWorkspaceTree,
      testCaseWorkspaceTreeToolBar,
    );
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProjects([
      new ProjectBuilder('ProjectA').withTestCaseLibraryNodes([
        new TestCaseBuilder('Classic'),
        new KeywordTestCaseBuilder('BDD'),
        new ScriptedTestCaseBuilder('Gherkin'),
        new ExploratoryTestCaseBuilder('Exploratory'),
        new TestCaseFolderBuilder('FolderA').withTestCaseLibraryNodes([
          new TestCaseBuilder('Poire'),
          new KeywordTestCaseBuilder('Fraise'),
          new ScriptedTestCaseBuilder('Mangue'),
          new ExploratoryTestCaseBuilder('Vanille'),
        ]),
      ]),
      new ProjectBuilder('ProjectB').withTestCaseLibraryNodes([
        new TestCaseFolderBuilder('FolderB'),
      ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1, 2]);
}

function assertCannotCopyAndPasteTestCasesInSameProject(
  testCaseWorkspaceTree: TreeElement,
  testCaseWorkspaceTreeToolBar: TreeToolbarElement,
) {
  testCaseWorkspaceTree.findRowId('NAME', 'ProjectA').then((idProject) => {
    testCaseWorkspaceTree.openNode(idProject);
  });
  testCaseWorkspaceTree.selectNodesByName(['Classic', 'BDD', 'Gherkin', 'Exploratory', 'FolderA']);
  testCaseWorkspaceTreeToolBar.copy(true);
  testCaseWorkspaceTree.findRowId('NAME', 'ProjectA').then((idProject) => {
    testCaseWorkspaceTree.pickNode(idProject);
  });
  testCaseWorkspaceTreeToolBar.assertPasteButtonIsDisabled();
  testCaseWorkspaceTreeToolBar.performPasteCommand(false);
  testCaseWorkspaceTree.assertRowIdNotExist('NAME', 'Classic-Copie1');
  testCaseWorkspaceTree.assertRowIdNotExist('NAME', 'BDD-Copie1');
  testCaseWorkspaceTree.assertRowIdNotExist('NAME', 'Gherkin-Copie1');
  testCaseWorkspaceTree.assertRowIdNotExist('NAME', 'Exploratory-Copie1');
}

function assertCannotCopyAndPasteTestCasesInOtherProject(
  testCaseWorkspaceTree: TreeElement,
  testCaseWorkspaceTreeToolBar: TreeToolbarElement,
) {
  copyAndPasteTestCaseInOtherProject(
    testCaseWorkspaceTree,
    testCaseWorkspaceTreeToolBar,
    'Classic',
    'ProjectB',
    true,
    false,
  );
  copyAndPasteTestCaseInOtherProject(
    testCaseWorkspaceTree,
    testCaseWorkspaceTreeToolBar,
    'BDD',
    'ProjectB',
    true,
    false,
  );
  copyAndPasteTestCaseInOtherProject(
    testCaseWorkspaceTree,
    testCaseWorkspaceTreeToolBar,
    'Gherkin',
    'ProjectB',
    true,
    false,
  );
  copyAndPasteTestCaseInOtherProject(
    testCaseWorkspaceTree,
    testCaseWorkspaceTreeToolBar,
    'Exploratory',
    'ProjectB',
    true,
    false,
  );
  copyAndPasteTestCaseInOtherProject(
    testCaseWorkspaceTree,
    testCaseWorkspaceTreeToolBar,
    'FolderA',
    'ProjectB',
    true,
    false,
  );
  testCaseWorkspaceTree.findRowId('NAME', 'ProjectA').then((idProject) => {
    testCaseWorkspaceTree.closeNode(idProject);
  });
  testCaseWorkspaceTree.findRowId('NAME', 'ProjectB').then((idProject) => {
    testCaseWorkspaceTree.openNode(idProject);
  });
  testCaseWorkspaceTree.assertRowIdNotExist('NAME', 'Classic');
  testCaseWorkspaceTree.assertRowIdNotExist('NAME', 'BDD');
  testCaseWorkspaceTree.assertRowIdNotExist('NAME', 'Gherkin');
  testCaseWorkspaceTree.assertRowIdNotExist('NAME', 'Exploratory');
  testCaseWorkspaceTree.assertRowIdNotExist('NAME', 'FolderA');
}
