import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { TestCaseBuilder } from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { TestCaseSearchPage } from '../../../../page-objects/pages/test-case-workspace/research/test-case-search-page';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import { GroupedMultiListElement } from '../../../../page-objects/elements/filters/grouped-multi-list.element';
import { selectByDataTestItemId } from '../../../../utils/basic-selectors';
import { EditMilestonesDialog } from '../../../../page-objects/pages/test-case-workspace/research/dialogs/edit-milestones-dialog';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  MilestoneBuilder,
  MilestoneOnProjectBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/milestone-prerequisite-builders';
import {
  addPermissionToProject,
  ApiPermissionGroup,
} from '../../../../utils/end-to-end/api/add-permissions';
import { TestCaseSearchMilestoneMassEdit } from '../../../../../../projects/sqtm-core/src/lib/model/test-case/test-case-search.model';
import { consultSearchPageAndAddCriteria } from '../../../scenario-parts/testcase.part';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check if a user can read the search testcase page and interact with it', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07020.CT09 - testcase-search-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testcaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const grid = new GridElement('test-case-search');
    const searchTestcasePage = new TestCaseSearchPage(grid);
    const multiListProject = new GroupedMultiListElement();
    const multiListCriteria = new GroupedMultiListElement();
    const milestoneDialog = new EditMilestonesDialog();

    consultSearchPageAndAddCriteria(
      testcaseWorkspace,
      searchTestcasePage,
      multiListProject,
      multiListCriteria,
      'AZERTY',
    );

    cy.log('Step 2');
    modificationOfAttributesOfTestcaseInGrid(searchTestcasePage);

    cy.log('Step 3');
    associateMilestoneToTestCaseInGrid(searchTestcasePage, milestoneDialog);

    cy.log('Step 4');
    dissociateMilestoneToTestCaseInGrid(searchTestcasePage, milestoneDialog);

    cy.log('Step 5');
    modifyAttributesInMassEditDialog(searchTestcasePage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('test case modifier', [
        ProfileBuilderPermission.TEST_CASE_READ,
        ProfileBuilderPermission.TEST_CASE_WRITE,
      ]),
    ])
    .withMilestones([new MilestoneBuilder('Eau', '2025-10-31 14:44:45').withUserLogin('RonW')])
    .withProjects([
      new ProjectBuilder('AZERTY')
        .withTestCaseLibraryNodes([new TestCaseBuilder('Pomme')])
        .withMilestonesOnProject([new MilestoneOnProjectBuilder('Eau')]),
    ])
    .build();
}
function initPermissions() {
  addPermissionToProject(1, ApiPermissionGroup.PROJECT_MANAGER, [2]);
}

function modificationOfAttributesOfTestcaseInGrid(searchTestcasePage: TestCaseSearchPage) {
  searchTestcasePage.grid.findRowId('name', 'Pomme').then((idTestCase) => {
    searchTestcasePage.grid.getCell(idTestCase, 'reference').textRenderer().editText('Baba');
    searchTestcasePage.grid.getCell(idTestCase, 'name').textRenderer().editText('Chocolat');
    searchTestcasePage.grid.getCell(idTestCase, 'importance').openCloseRenderer().toggle();
    cy.get(selectByDataTestItemId('item-VERY_HIGH')).click();
    searchTestcasePage.grid
      .getCell(idTestCase, 'importance')
      .findIconInCell('sqtm-core-test-case:double_up')
      .should('be.visible');
    searchTestcasePage.grid.scrollPosition('center', 'mainViewport');
    searchTestcasePage.grid.getCell(idTestCase, 'nature').indexRenderer().toggle();
    cy.get(selectByDataTestItemId('item-13')).should('contain.text', 'Fonctionnel').click();
    searchTestcasePage.grid
      .getCell(idTestCase, 'nature')
      .findCellTextSpan()
      .should('contain.text', 'Fonctionnel');
  });
}

function associateMilestoneToTestCaseInGrid(
  searchTestcasePage: TestCaseSearchPage,
  milestoneDialog: EditMilestonesDialog,
) {
  searchTestcasePage.grid.findRowId('name', 'Chocolat').then((idTestCase) => {
    searchTestcasePage.grid.selectRow(idTestCase, '#', 'leftViewport');
  });
  searchTestcasePage.showMassBindingMilestoneDialog<TestCaseSearchMilestoneMassEdit>();
  milestoneDialog.assertExists();
  milestoneDialog
    .getMilestoneGrid()
    .checkBoxInRowWithMatchingCellContentInPickerGrid('label', 'Eau');
  milestoneDialog.confirm();
}

function dissociateMilestoneToTestCaseInGrid(
  searchTestcasePage: TestCaseSearchPage,
  milestoneDialog: EditMilestonesDialog,
) {
  searchTestcasePage.grid.assertSpinnerIsNotPresent();
  searchTestcasePage.grid.findRowId('name', 'Chocolat').then((idTestCase) => {
    searchTestcasePage.grid.selectRow(idTestCase, '#', 'leftViewport');
  });
  searchTestcasePage.showMassBindingMilestoneDialog<TestCaseSearchMilestoneMassEdit>();
  milestoneDialog.assertExists();
  milestoneDialog
    .getMilestoneGrid()
    .checkBoxInRowWithMatchingCellContentInPickerGrid('label', 'Eau');
  milestoneDialog.confirm();
}

function modifyAttributesInMassEditDialog(searchTestcasePage: TestCaseSearchPage) {
  searchTestcasePage.grid.assertSpinnerIsNotPresent();
  searchTestcasePage.grid.findRowId('name', 'Chocolat').then((idTestCase) => {
    searchTestcasePage.grid.selectRow(idTestCase, '#', 'leftViewport');
  });
  const massEditDialogPage = searchTestcasePage.showMassEditDialog();
  massEditDialogPage.assertExists();
  massEditDialogPage.getOptionalField('status').check();
  massEditDialogPage.getOptionalField('status').selectValue('Approuvé');
  massEditDialogPage.getOptionalField('status').assertContains('Approuvé');
  massEditDialogPage.getOptionalField('nature').check();
  massEditDialogPage.getOptionalField('nature').selectValue('Performance');
  massEditDialogPage.getOptionalField('nature').assertContains('Performance');
  massEditDialogPage.getOptionalField('type').check();
  massEditDialogPage.getOptionalField('type').selectValue('Correctif');
  massEditDialogPage.getOptionalField('type').assertContains('Correctif');
  massEditDialogPage.getOptionalField('automatedTestTechnology').check();
  massEditDialogPage.getOptionalField('automatedTestTechnology').selectValue('Cucumber 5+');
  massEditDialogPage.getOptionalField('automatedTestTechnology').assertContains('Cucumber 5+');
  massEditDialogPage.confirm();
  searchTestcasePage.grid.findRowId('name', 'Chocolat').then((idTestCase) => {
    searchTestcasePage.grid
      .getCell(idTestCase, 'status')
      .findIconInCell('sqtm-core-test-case:status')
      .should('have.css', 'color', 'rgb(31, 191, 5)');
    searchTestcasePage.grid
      .getCell(idTestCase, 'nature')
      .findCell()
      .should('contain', 'Performance');
    searchTestcasePage.grid.getCell(idTestCase, 'type').findCell().should('contain', 'Correctif');
  });
}
