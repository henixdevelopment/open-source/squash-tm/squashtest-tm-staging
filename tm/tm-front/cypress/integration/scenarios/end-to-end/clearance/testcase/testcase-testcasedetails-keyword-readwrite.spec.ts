import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import {
  DataSetBuilder,
  DataSetParameterValueBuilder,
  KeywordStepBuilder,
  KeywordTestCaseBuilder,
  ParameterBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { ActionWordViewPage } from '../../../../page-objects/pages/action-word-workspace/action-word/action-word-view.page';
import { TestCaseViewSubPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-sub-page';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import { getToSecondLevelViewPageOfTestCase } from '../../../scenario-parts/actionword.part';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check if a user can modify a keyword test case in Second Level View Page', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC070201.CT02 - testcase-testcasedetails-keyword-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const actionWordWorkspace = NavBarElement.navigateToActionWordWorkspace();
    const actionWordViewPage = new ActionWordViewPage();
    const keywordTestCaseSecondLevelViewPage = new TestCaseViewSubPage(1);

    getToSecondLevelViewPageOfTestCase(
      actionWordWorkspace,
      actionWordViewPage,
      keywordTestCaseSecondLevelViewPage,
      'AZERTY',
      'ddd',
    );

    cy.log('Step 2');
    changeHeaderInSecondLevelTestCasePage(keywordTestCaseSecondLevelViewPage);

    cy.log('Step 3');
    changeStatusCUFAndDescriptionInViewPage(keywordTestCaseSecondLevelViewPage);

    cy.log('Step 4');
    modifyAutomationField(keywordTestCaseSecondLevelViewPage);

    cy.log('Step 5');
    modifyDatasetName(keywordTestCaseSecondLevelViewPage);

    cy.log('Step 6');
    copyDatasetInTestCase(keywordTestCaseSecondLevelViewPage);

    cy.log('Step 7');
    changeDescriptionOfParam(keywordTestCaseSecondLevelViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('test case modifier', [
        ProfileBuilderPermission.TEST_CASE_READ,
        ProfileBuilderPermission.TEST_CASE_WRITE,
        ProfileBuilderPermission.ACTION_WORD_READ,
      ]),
    ])
    .withCufs([new CustomFieldBuilder('a', InputType.RICH_TEXT)])
    .withProjects([
      new ProjectBuilder('AZERTY')
        .withTestCaseLibraryNodes([
          new KeywordTestCaseBuilder('Lala')
            .withKeywordSteps([new KeywordStepBuilder('GIVEN', 'ddd')])
            .withParameters([new ParameterBuilder('Lol')])
            .withDataSets([
              new DataSetBuilder('Dada').withParamValues([
                new DataSetParameterValueBuilder('Lol', 'Pp'),
              ]),
            ]),
        ])
        .withCufsOnProject([new CustomFieldOnProjectBuilder('a', BindableEntity.TEST_CASE)]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}

function changeHeaderInSecondLevelTestCasePage(
  keywordTestCaseSecondLevelViewPage: TestCaseViewSubPage,
) {
  keywordTestCaseSecondLevelViewPage.testCaseViewPage.informationPanel.referenceTextField.setAndConfirmValue(
    'Blue',
  );
  keywordTestCaseSecondLevelViewPage.testCaseViewPage.informationPanel.referenceTextField.assertContainsText(
    'Blue',
  );
  keywordTestCaseSecondLevelViewPage.testCaseViewPage.importanceCapsule.selectOption('Moyen');
  keywordTestCaseSecondLevelViewPage.testCaseViewPage.importanceCapsule.assertContainsText('Moyen');
}

function changeStatusCUFAndDescriptionInViewPage(
  keywordTestCaseSecondLevelViewPage: TestCaseViewSubPage,
) {
  keywordTestCaseSecondLevelViewPage.testCaseViewPage.informationPanel.statusSelectField.selectValueNoButton(
    'Obsolète',
  );
  keywordTestCaseSecondLevelViewPage.testCaseViewPage.informationPanel.statusSelectField.assertContainsText(
    'Obsolète',
  );
  keywordTestCaseSecondLevelViewPage.getRichTextCustomField('a').setAndConfirmValue('Lalala');
  keywordTestCaseSecondLevelViewPage.getRichTextCustomField('a').assertContainsText('Lalala');
  keywordTestCaseSecondLevelViewPage.testCaseViewPage.informationPanel.descriptionRichField.setAndConfirmValue(
    'Soleil',
  );
  keywordTestCaseSecondLevelViewPage.testCaseViewPage.informationPanel.descriptionRichField.assertContainsText(
    'Soleil',
  );
}

function modifyAutomationField(keywordTestCaseSecondLevelViewPage: TestCaseViewSubPage) {
  keywordTestCaseSecondLevelViewPage.testCaseViewPage.automationPanel.insertReferenceOfTestAuto(
    'Batman',
  );
}

function modifyDatasetName(
  keywordTestCaseSecondLevelViewPage: TestCaseViewSubPage,
  dataSetViewPage = keywordTestCaseSecondLevelViewPage.testCaseViewPage.clickParametersAnchorLink(),
) {
  dataSetViewPage.renameDataSet('Dada', 'Meme');
}

function copyDatasetInTestCase(
  keywordTestCaseSecondLevelViewPage: TestCaseViewSubPage,
  dataSetViewPage = keywordTestCaseSecondLevelViewPage.testCaseViewPage.clickParametersAnchorLink(),
) {
  dataSetViewPage.parametersTable.findRowId('name', 'Meme', 'leftViewport').then((idDataset) => {
    dataSetViewPage.openDuplicateDataSetDialogAndCopyDataSet(idDataset, 'Meme-Copie1');
  });
}

function changeDescriptionOfParam(
  keywordTestCaseSecondLevelViewPage: TestCaseViewSubPage,
  dataSetViewPage = keywordTestCaseSecondLevelViewPage.testCaseViewPage.clickParametersAnchorLink(),
) {
  const informationParamDialog = dataSetViewPage
    .openInformationParamDialog('PARAM_COLUMN_1')
    .getDescriptionField();
  informationParamDialog.setAndConfirmValue('Zeri');
  informationParamDialog.assertContainsText('Zeri');
}
