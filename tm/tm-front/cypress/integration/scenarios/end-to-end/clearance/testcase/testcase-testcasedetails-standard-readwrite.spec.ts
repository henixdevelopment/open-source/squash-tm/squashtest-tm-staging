import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  DataSetBuilder,
  DataSetParameterValueBuilder,
  ParameterBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
  ListOptionCustomFieldBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { RequirementVersionsLinkToVerifyingTestCaseBuilder } from '../../../../utils/end-to-end/prerequisite/builders/link-builders';
import { RequirementBuilder } from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { RequirementViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import { TestCaseViewSubPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-sub-page';
import { MultipleMilestonePickerDialogElement } from '../../../../page-objects/elements/dialog/multiple-milestone-picker-dialog.element';
import { RemoveDatasetDialog } from '../../../../page-objects/pages/test-case-workspace/dialogs/remove-dataset-dialog.element';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  MilestoneBuilder,
  MilestoneOnProjectBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/milestone-prerequisite-builders';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import { accessToTestCaseSecondLevelViewPageFromRequirementPage } from '../../../scenario-parts/testcase.part';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check if a user can modify a test case in Second Level View Page', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC070201.CT01 - testcase-testcasedetails-standard-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspace = RequirementWorkspacePage.initTestAtPage();
    const requirementViewPage = new RequirementViewPage();
    const testCaseViewDetailsPage = new TestCaseViewSubPage(1);
    const milestoneDialog = new MultipleMilestonePickerDialogElement();
    const deleteDatasetDialog = new RemoveDatasetDialog();

    accessToTestCaseSecondLevelViewPageFromRequirementPage(
      requirementViewPage,
      requirementWorkspace,
      testCaseViewDetailsPage,
      'AZERTY',
      'Lala',
      'Pomme',
    );

    cy.log('Step 2');
    changeNameAndStatusInHeader(testCaseViewDetailsPage);

    cy.log('Step 3');
    modifyTypeAndCufInInformationPanel(testCaseViewDetailsPage);

    cy.log('Step 4');
    associateAndDissociateMilestone(testCaseViewDetailsPage, milestoneDialog);

    cy.log('Step 5');
    addAutomatedTechnology(testCaseViewDetailsPage);

    cy.log('Step 6');
    const parameterViewPage = testCaseViewDetailsPage.testCaseViewPage.clickParametersAnchorLink();
    modifyNameParameter(parameterViewPage);

    cy.log('Step 7');
    deleteParameter(parameterViewPage);

    cy.log('Step 8');
    deleteDataSet(parameterViewPage, deleteDatasetDialog);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('test case modifier', [
        ProfileBuilderPermission.TEST_CASE_READ,
        ProfileBuilderPermission.TEST_CASE_WRITE,
        ProfileBuilderPermission.REQUIREMENT_READ,
      ]),
    ])
    .withMilestones([new MilestoneBuilder('Eau', '2025-10-31 14:44:45').withUserLogin('RonW')])
    .withCufs([
      new CustomFieldBuilder('Liste', InputType.DROPDOWN_LIST).withOptions([
        new ListOptionCustomFieldBuilder('D', 'A'),
        new ListOptionCustomFieldBuilder('B', 'C'),
      ]),
    ])
    .withProjects([
      new ProjectBuilder('AZERTY')
        .withRequirementLibraryNodes([new RequirementBuilder('Lala')])
        .withLinks([new RequirementVersionsLinkToVerifyingTestCaseBuilder(['Lala'], 'Pomme')])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Pomme')
            .withParameters([new ParameterBuilder('Gala')])
            .withDataSets([
              new DataSetBuilder('Fruit').withParamValues([
                new DataSetParameterValueBuilder('Gala', 'Rouge'),
              ]),
            ]),
        ])
        .withMilestonesOnProject([new MilestoneOnProjectBuilder('Eau')])
        .withCufsOnProject([new CustomFieldOnProjectBuilder('Liste', BindableEntity.TEST_CASE)]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}

function changeNameAndStatusInHeader(testCaseViewDetailsPage: TestCaseViewSubPage) {
  testCaseViewDetailsPage.testCaseViewPage.informationPanel.nameTextField.clearTextField();
  testCaseViewDetailsPage.testCaseViewPage.informationPanel.nameTextField.setAndConfirmValue('Lol');
  testCaseViewDetailsPage.testCaseViewPage.informationPanel.nameTextField.assertContainsText('Lol');
  testCaseViewDetailsPage.testCaseViewPage.statusCapsule.selectOption('Approuvé');
  testCaseViewDetailsPage.testCaseViewPage.statusCapsule.assertContainsText('Approuvé');
}

function modifyTypeAndCufInInformationPanel(testCaseViewDetailsPage: TestCaseViewSubPage) {
  testCaseViewDetailsPage.testCaseViewPage.informationPanel.typeSelectField.selectValueNoButton(
    'Bout-en-bout',
  );
  testCaseViewDetailsPage.testCaseViewPage.informationPanel.typeSelectField.assertContainsText(
    'Bout-en-bout',
  );
  testCaseViewDetailsPage.getSelectCustomField('Liste').selectValueNoButton('D');
  testCaseViewDetailsPage.getSelectCustomField('Liste').assertContainsText('D');
}

function associateAndDissociateMilestone(
  testCaseViewDetailsPage: TestCaseViewSubPage,
  milestoneDialog: MultipleMilestonePickerDialogElement,
) {
  testCaseViewDetailsPage.testCaseViewPage.informationPanel.milestonesField.openMilestoneDialog();
  milestoneDialog.selectMilestone('Eau');
  milestoneDialog.confirm();
  testCaseViewDetailsPage.testCaseViewPage.informationPanel.milestonesField.assertContainsText(
    'Eau',
  );
  testCaseViewDetailsPage.testCaseViewPage.informationPanel.milestonesField.clickOnRemoveMilestoneClose(
    'Eau',
  );
  testCaseViewDetailsPage.testCaseViewPage.informationPanel.milestonesField.assertContainsText('');
}

function addAutomatedTechnology(testCaseViewDetailPage: TestCaseViewSubPage) {
  testCaseViewDetailPage.testCaseViewPage.automationPanel.chooseTechnology('SoapUI');
}

function modifyNameParameter(parameterViewPage) {
  parameterViewPage.renameParam('Gala', 'Pink_Lady');
}

function deleteParameter(parameterViewPage) {
  parameterViewPage.openDeleteParamDialog('PARAM_COLUMN_1', { PARAMETER_USED: false });
  parameterViewPage.paramRemoveDialog.confirm();
}

function deleteDataSet(parameterViewPage, deleteDatasetDialog: RemoveDatasetDialog) {
  parameterViewPage.openDeleteDataSetDialog(1);
  deleteDatasetDialog.confirm();
  parameterViewPage.parametersTable.assertGridIsEmpty();
}
