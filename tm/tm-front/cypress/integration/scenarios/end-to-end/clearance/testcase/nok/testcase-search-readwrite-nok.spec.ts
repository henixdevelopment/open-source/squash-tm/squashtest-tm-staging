import { DatabaseUtils } from '../../../../../utils/database.utils';
import { TestCaseWorkspacePage } from '../../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { SystemE2eCommands } from '../../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import {
  MilestoneBuilder,
  MilestoneOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/milestone-prerequisite-builders';
import { GridElement } from '../../../../../page-objects/elements/grid/grid.element';
import { TestCaseSearchPage } from '../../../../../page-objects/pages/test-case-workspace/research/test-case-search-page';
import { GroupedMultiListElement } from '../../../../../page-objects/elements/filters/grouped-multi-list.element';
import { TestCaseBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { consultSearchPageAndAddCriteria } from '../../../../scenario-parts/testcase.part';

describe('Check if a user with Guest can view the page and perform a search. But cannot: modify attributes via the search page table, modify attributes via the "Edit Attributes" popup and associate and disassociate a milestone.', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07220.CT09 - testcase-search-readwrite-nok', () => {
    cy.log('step 1');
    cy.logInAs('RonW', 'admin');
    const testcaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const grid = new GridElement('test-case-search');
    const searchTestcasePage = new TestCaseSearchPage(grid);
    const multiListProject = new GroupedMultiListElement();
    const multiListCriteria = new GroupedMultiListElement();
    consultSearchPageAndAddCriteria(
      testcaseWorkspace,
      searchTestcasePage,
      multiListProject,
      multiListCriteria,
      'Project',
    );

    cy.log('Step 2');
    assertCanSearchButCannotEditTestCaseAttributes(searchTestcasePage);

    cy.log('Step 3');
    assertCannotAssociateOrDissociateMilestone(searchTestcasePage);

    cy.log('Step 4');
    assertCannotEditAttributesFields(searchTestcasePage);
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withMilestones([new MilestoneBuilder('Azerty', '2025-10-31 14:44:45').withUserLogin('RonW')])
    .withProjects([
      new ProjectBuilder('Project')
        .withTestCaseLibraryNodes([new TestCaseBuilder('Test')])
        .withMilestonesOnProject([new MilestoneOnProjectBuilder('Azerty')]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}

function assertCanSearchButCannotEditTestCaseAttributes(searchTestcasePage: TestCaseSearchPage) {
  searchTestcasePage.clickSearchButton();
  searchTestcasePage.assertCellIsNotSelectable('Test', 'reference');
  searchTestcasePage.assertCellIsNotSelectable('Test', 'name');
  searchTestcasePage.assertCellIsNotSelectable('Test', 'importance');
  searchTestcasePage.assertCellIsNotSelectable('Test', 'nature');
}
function assertCannotAssociateOrDissociateMilestone(searchTestcasePage: TestCaseSearchPage) {
  searchTestcasePage.grid.findRowId('name', 'Test').then((idTestCase) => {
    searchTestcasePage.grid.assertSpinnerIsNotPresent();
    searchTestcasePage.grid.selectRow(idTestCase, '#', 'leftViewport');
  });
  searchTestcasePage.grid.assertSpinnerIsNotPresent();
  const noPermissionToBindMilestoneDialog =
    searchTestcasePage.getNoPermissionToBindMilestoneDialog();
  noPermissionToBindMilestoneDialog.assertExists();
  noPermissionToBindMilestoneDialog.assertMessage();
  noPermissionToBindMilestoneDialog.close();
}
function assertCannotEditAttributesFields(searchTestcasePage: TestCaseSearchPage) {
  const noPermissionToEditMassDialog = searchTestcasePage.getNoPermissionToEditMassDialog();
  noPermissionToEditMassDialog.assertExists();
  noPermissionToEditMassDialog.assertMessage();
  noPermissionToEditMassDialog.close();
}
