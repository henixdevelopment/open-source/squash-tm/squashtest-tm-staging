import { DatabaseUtils } from '../../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { RequirementWorkspacePage } from '../../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { RequirementViewPage } from '../../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import { TestCaseViewSubPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-sub-page';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { RequirementBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  KeywordStepBuilder,
  KeywordTestCaseBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { RequirementVersionsLinkToVerifyingTestCaseBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/link-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { accessToTestCaseSecondLevelViewPageFromRequirementPage } from '../../../../scenario-parts/testcase.part';

describe('Check if a user with Guest cannot perform the following actions on the BDD test case : modify and move up or move down step', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC072201.CT06 - testcase-testcaseviewdetails-keyword-steps-readwrite-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspace = RequirementWorkspacePage.initTestAtPage();
    const requirementViewPage = new RequirementViewPage();
    const secondLevelTestCaseViewPage = new TestCaseViewSubPage(1);
    accessToTestCaseSecondLevelViewPageFromRequirementPage(
      requirementViewPage,
      requirementWorkspace,
      secondLevelTestCaseViewPage,
      'Project',
      'Req',
      'BDD',
    );
    const keywordStepElement = secondLevelTestCaseViewPage.testCaseViewPage
      .clickKeywordStepsAnchorLink()
      .getStepByIndex(0);

    keywordStepElement.getKeywordField().assertIsNotEditable();

    cy.log('Step 2');
    keywordStepElement.assertCannotMoveStepDown();
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProjects([
      new ProjectBuilder('Project')
        .withRequirementLibraryNodes([new RequirementBuilder('Req')])
        .withTestCaseLibraryNodes([
          new KeywordTestCaseBuilder('BDD').withKeywordSteps([
            new KeywordStepBuilder('GIVEN', 'Condition'),
            new KeywordStepBuilder('WHEN', 'Action'),
            new KeywordStepBuilder('THEN', 'Result'),
          ]),
        ])
        .withLinks([new RequirementVersionsLinkToVerifyingTestCaseBuilder(['Req'], 'BDD')]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}
