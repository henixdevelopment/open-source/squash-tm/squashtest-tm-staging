import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  ExploratoryTestCaseBuilder,
  KeywordTestCaseBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
  TestCaseFolderBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { TreeElement } from '../../../../page-objects/elements/grid/grid.element';
import { TreeToolbarElement } from '../../../../page-objects/elements/workspace-common/tree-toolbar.element';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  assertNodeExistAndParentWithOptionalRegex,
  copyAndPasteTestCaseInOtherProject,
  selectNodeInTestCaseWorkspace,
} from '../../../scenario-parts/testcase.part';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can copy/paste test case, test case folder and scripted/keyword test case', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07021.CT02 - testcase-copypaste-createdelete', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testCaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const testCaseWorkspaceTree = testCaseWorkspace.tree;
    const testCaseWorkspaceTreeToolBar = new TreeToolbarElement('test-case-toolbar');

    copyAndPasteTestCasesInSameProject(testCaseWorkspaceTree, testCaseWorkspaceTreeToolBar);

    cy.log('Step 2');
    copyAndPasteTestCasesInDifferentProject(
      testCaseWorkspace,
      testCaseWorkspaceTree,
      testCaseWorkspaceTreeToolBar,
    );
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('test case copier', [
        ProfileBuilderPermission.TEST_CASE_READ,
        ProfileBuilderPermission.TEST_CASE_CREATE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Athena').withTestCaseLibraryNodes([
        new TestCaseBuilder('Salut'),
        new KeywordTestCaseBuilder('Hola'),
        new ScriptedTestCaseBuilder('Coucou'),
        new ExploratoryTestCaseBuilder('Chocolat'),
        new TestCaseFolderBuilder('Water').withTestCaseLibraryNodes([
          new TestCaseBuilder('Poire'),
          new KeywordTestCaseBuilder('Fraise'),
          new ScriptedTestCaseBuilder('Mangue'),
          new ExploratoryTestCaseBuilder('Vanille'),
        ]),
      ]),
      new ProjectBuilder('Zeus').withTestCaseLibraryNodes([new TestCaseFolderBuilder('Folder')]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1, 2]);
}
function copyAndPasteTestCasesInSameProject(
  testCaseWorkspaceTree: TreeElement,
  testCaseWorkspaceTreeToolBar: TreeToolbarElement,
) {
  testCaseWorkspaceTree.findRowId('NAME', 'Athena').then((idProject) => {
    testCaseWorkspaceTree.openNode(idProject);
  });
  testCaseWorkspaceTree.selectNodesByName(['Chocolat', 'Salut', 'Hola', 'Coucou', 'Water']);
  testCaseWorkspaceTreeToolBar.copy(true);
  testCaseWorkspaceTree.findRowId('NAME', 'Athena').then((idProject) => {
    testCaseWorkspaceTree.pickNode(idProject);
    testCaseWorkspaceTreeToolBar.paste({ dataRows: idProject }, 'test-case-tree', idProject, true);
  });
  testCaseWorkspaceTree.findRowId('NAME', 'Athena').then((idProject) => {
    assertNodeExistAndParentWithOptionalRegex(
      testCaseWorkspaceTree,
      ['Chocolat', 'Salut', 'Hola', 'Coucou', 'Water'],
      idProject,
      true,
    );
  });
}

function copyAndPasteTestCasesInDifferentProject(
  testCaseWorkspace: TestCaseWorkspacePage,
  testCaseWorkspaceTree: TreeElement,
  testCaseWorkspaceTreeToolBar: TreeToolbarElement,
) {
  selectNodeInTestCaseWorkspace(testCaseWorkspace, 'Zeus', 'Folder');
  copyAndPasteTestCaseInOtherProject(
    testCaseWorkspaceTree,
    testCaseWorkspaceTreeToolBar,
    'Salut',
    'Folder',
    true,
    true,
  );
  copyAndPasteTestCaseInOtherProject(
    testCaseWorkspaceTree,
    testCaseWorkspaceTreeToolBar,
    'Hola',
    'Folder',
    true,
    true,
  );
  copyAndPasteTestCaseInOtherProject(
    testCaseWorkspaceTree,
    testCaseWorkspaceTreeToolBar,
    'Coucou',
    'Folder',
    true,
    true,
  );
  copyAndPasteTestCaseInOtherProject(
    testCaseWorkspaceTree,
    testCaseWorkspaceTreeToolBar,
    'Chocolat',
    'Folder',
    true,
    true,
  );
  copyAndPasteTestCaseInOtherProject(
    testCaseWorkspaceTree,
    testCaseWorkspaceTreeToolBar,
    'Water',
    'Folder',
    true,
    true,
  );

  testCaseWorkspaceTree.findRowId('NAME', 'Athena').then((idProject) => {
    testCaseWorkspaceTree.closeNode(idProject);
  });
  testCaseWorkspaceTree.findRowId('NAME', 'Folder').then((idProject) => {
    assertNodeExistAndParentWithOptionalRegex(
      testCaseWorkspaceTree,
      ['Salut', 'Hola', 'Coucou', 'Chocolat', 'Water'],
      idProject,
      false,
    );
  });
}
