import { DatabaseUtils } from '../../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { BindableEntity } from '../../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import {
  ActionTestStepBuilder,
  CallTestStepsBuilder,
  TestCaseBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { TestCaseWorkspacePage } from '../../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { selectNodeInTestCaseWorkspace } from '../../../../scenario-parts/testcase.part';
import { TestCaseViewPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { EditableTextFieldElement } from '../../../../../page-objects/elements/forms/editable-text-field.element';
import { ToolbarButtonElement } from '../../../../../page-objects/elements/workspace-common/toolbar.element';
import { TreeElement } from '../../../../../page-objects/elements/grid/grid.element';
import { TestStepsViewPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-steps-view.page';

describe('Check if a user with Guest cannot perform the following actions: modify the prerequisite, copy/paste and call a test step, modify a simple text custom field on a test step"', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07220.CT06- testcase-testcaseviewstandard-steps-readwrite-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testCaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const testAViewPage = new TestCaseViewPage(1);
    const testBViewPage = new TestCaseViewPage(2);
    const stepsViewPage = new TestStepsViewPage();
    assertCanAccessPrerequisitesButCannotEdit(testCaseWorkspace, testAViewPage, stepsViewPage);

    cy.log('Step 2');
    assertCannotEditTextCUF(testAViewPage, stepsViewPage);

    cy.log('Step 3');
    assertCanCopyTestStepButCannotPaste(stepsViewPage);

    cy.log('Step 4');
    assertCanClickOptionsButCannotCallTestCase(stepsViewPage);

    cy.log('Step 5');
    assertCanAccessCalledTestCase(testCaseWorkspace, testBViewPage);
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withCufs([new CustomFieldBuilder('TextCuf', InputType.PLAIN_TEXT)])
    .withProjects([
      new ProjectBuilder('Project')
        .withCufsOnProject([new CustomFieldOnProjectBuilder('TextCuf', BindableEntity.TEST_STEP)])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('TestA').withSteps([
            new ActionTestStepBuilder('Action_A', 'Result_A'),
          ]),
          new TestCaseBuilder('TestB ').withSteps([
            new ActionTestStepBuilder('Action_B', 'Result_B').withCallTestSteps([
              new CallTestStepsBuilder('TestA'),
            ]),
          ]),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}
function assertCanAccessPrerequisitesButCannotEdit(
  testCaseWorkspace: TestCaseWorkspacePage,
  testAViewPage: TestCaseViewPage,
  stepsViewPage: TestStepsViewPage,
) {
  selectNodeInTestCaseWorkspace(testCaseWorkspace, 'Project', 'TestA');
  testAViewPage.clickStepsAnchorLink();
  stepsViewPage.assertExists();
  stepsViewPage.clickPrerequisiteAndAssertNotEditable();
}

function assertCannotEditTextCUF(
  testAViewPage: TestCaseViewPage,
  stepsViewPage: TestStepsViewPage,
) {
  const stepElement = stepsViewPage.getActionStepByIndex(0);
  stepElement.extendStep();
  const textCuf = testAViewPage.getCustomField(
    'TextCuf',
    InputType.PLAIN_TEXT,
  ) as EditableTextFieldElement;
  textCuf.checkEditMode(false);
}
function assertCanCopyTestStepButCannotPaste(stepsViewPage: TestStepsViewPage) {
  const copyButton = new ToolbarButtonElement('sqtm-app-test-case-view', 'copy-button');
  const pasteButton = new ToolbarButtonElement('sqtm-app-test-case-view', 'paste-button');
  stepsViewPage.getActionStepByIndex(0).singleSelectStep();
  copyButton.assertExists();
  copyButton.click();
  pasteButton.assertNotExist();
}
function assertCanClickOptionsButCannotCallTestCase(stepsViewPage: TestStepsViewPage) {
  const testcaseTree = TreeElement.createTreeElement('test-case-tree-picker', 'test-case-tree');
  stepsViewPage.getActionStepByIndex(0).clickStepActionButton();
  stepsViewPage
    .getActionStepByIndex(0)
    .clickActionStepActionButtonAndSelectOptionInMenu('call-test-case');
  testcaseTree.assertNotExists();
}
function assertCanAccessCalledTestCase(
  testCaseWorkspace: TestCaseWorkspacePage,
  testBViewPage: TestCaseViewPage,
) {
  selectNodeInTestCaseWorkspace(testCaseWorkspace, 'Project', 'TestB');
  const testCaseViewCalledTestCasesPage = testBViewPage.clickCalledTestCasesAnchorLink();
  testCaseViewCalledTestCasesPage.assertExists();
  testCaseViewCalledTestCasesPage.checkExistingCallingTest('TestA');
}
