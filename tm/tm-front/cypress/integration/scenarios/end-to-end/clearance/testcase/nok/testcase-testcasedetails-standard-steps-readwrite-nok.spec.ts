import { DatabaseUtils } from '../../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { RequirementWorkspacePage } from '../../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { RequirementBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  ActionTestStepBuilder,
  TestCaseBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { RequirementVersionsLinkToVerifyingTestCaseBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/link-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { RequirementViewPage } from '../../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import { TestCaseViewSubPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-sub-page';
import { accessToSecondLevelViewPageOfTestCase } from '../../../../scenario-parts/testcase.part';

describe('Check if a user with Guest cannot perform the following actions in Level 2 page of a Classic test case: modify the prerequisite, add, move, and delete a test step', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC072201.CT05 - testcase-testcasedetails-standard-steps-readwrite-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    const requirementViewPage = new RequirementViewPage();
    const testcaseSecondLevelViewPage = new TestCaseViewSubPage(1);
    accessToSecondLevelViewPageOfTestCase(
      requirementWorkspacePage,
      requirementViewPage,
      testcaseSecondLevelViewPage,
      'Project',
      'Requirement',
      'Test',
    );
    const stepsViewPage = testcaseSecondLevelViewPage.testCaseViewPage.clickStepsAnchorLink();
    stepsViewPage.assertExists();
    stepsViewPage.clickPrerequisiteAndAssertNotEditable();

    cy.log('Step 2');
    stepsViewPage.assertCannotAddTestStep(1);

    cy.log('Step 3');
    stepsViewPage.assertMoveStepDownNotExist('1');

    cy.log('Step 4');
    stepsViewPage.assertCannotDeleteStepWithTopIcon(1);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProjects([
      new ProjectBuilder('Project')
        .withRequirementLibraryNodes([new RequirementBuilder('Requirement')])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Test').withSteps([
            new ActionTestStepBuilder('Action 1', 'Result 1'),
            new ActionTestStepBuilder('Action 2', 'Result 2'),
            new ActionTestStepBuilder('Action 3', 'Result 3'),
          ]),
        ])
        .withLinks([
          new RequirementVersionsLinkToVerifyingTestCaseBuilder(['Requirement'], 'Test'),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}
