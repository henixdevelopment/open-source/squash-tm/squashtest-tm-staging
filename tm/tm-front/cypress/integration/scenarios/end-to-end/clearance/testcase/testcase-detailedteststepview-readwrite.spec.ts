import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import {
  ActionTestStepBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { TestCaseViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { DetailedStepViewPage } from '../../../../page-objects/pages/test-case-workspace/detailed-step-view/detailed-step-view.page';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  modifyStepInDetailedStepView,
  seeDetailsStepViewPage,
} from '../../../scenario-parts/testcase.part';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can modify a test step in a test case', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07020.CT07 - testcase-detailedteststepview-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testCaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const testCaseViewPage = new TestCaseViewPage(1);
    const detailedStepViewPage = new DetailedStepViewPage('1');

    seeDetailsStepViewPage(testCaseWorkspace, testCaseViewPage, 'Athena', 'Fanta', 0);

    cy.log('Step 2');
    modifyStepAndToggleCheckboxCuf(detailedStepViewPage);
  });

  function initTestData() {
    return new PrerequisiteBuilder()
      .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
      .withProfiles([
        new ProfileBuilder('test case modifier', [
          ProfileBuilderPermission.TEST_CASE_READ,
          ProfileBuilderPermission.TEST_CASE_WRITE,
        ]),
      ])
      .withCufs([new CustomFieldBuilder('TestCuf', InputType.CHECKBOX)])
      .withProjects([
        new ProjectBuilder('Athena')
          .withCufsOnProject([new CustomFieldOnProjectBuilder('TestCuf', BindableEntity.TEST_STEP)])
          .withTestCaseLibraryNodes([
            new TestCaseBuilder('Fanta').withSteps([
              new ActionTestStepBuilder('Je crie Expecto Patronum.', 'Mon patronus apparaît.'),
            ]),
          ]),
      ])
      .build();
  }
  function initPermissions() {
    addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
  }
  function modifyStepAndToggleCheckboxCuf(detailedStepViewPage: DetailedStepViewPage) {
    const cufCheckBox = detailedStepViewPage.getCheckboxCustomField('TestCuf');
    modifyStepInDetailedStepView(detailedStepViewPage, 'Faim', 'Gâteau');
    detailedStepViewPage.assertActionContains('Faim');
    detailedStepViewPage.assertResultContains('Gâteau');
    cufCheckBox.toggleState(true);
    cufCheckBox.checkState(true);
  }
});
