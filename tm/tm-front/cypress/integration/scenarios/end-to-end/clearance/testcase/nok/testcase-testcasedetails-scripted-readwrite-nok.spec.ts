import { DatabaseUtils } from '../../../../../utils/database.utils';
import { CampaignWorkspacePage } from '../../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { IterationViewPage } from '../../../../../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import { TestCaseViewSubPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-sub-page';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import {
  SCMRepositoryBuilder,
  SCMServerBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/server-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { ScriptedTestCaseBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CampaignBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { BindableEntity } from '../../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import {
  assertCanAccessScriptPageButCannotEdit,
  getToSecondViewPageOfScriptedTestCase,
  verifyCannotChangeHeaderOnTestCaseViewPage,
} from '../../../../scenario-parts/testcase.part';
import { TestCaseViewPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';

describe('Check if a user with Guest can perform the following actions on the level 2 page of a Gherkin test case: print.However, they cannot: edit attributes, modify the source code repository URL and edit a script.', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC072201.CT03 - testcase-testcasedetails-scripted-readwrite-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();
    const iterationViewPage = new IterationViewPage();
    const scriptedTestCaseSecondLevelViewPage = new TestCaseViewSubPage(1);
    const testCasViewPage = scriptedTestCaseSecondLevelViewPage.testCaseViewPage;
    const scmRepositoryField = testCasViewPage.automationPanel.scmRepository;

    cy.log('Step 1');
    getToSecondViewPageOfScriptedTestCase(
      campaignWorkspace,
      iterationViewPage,
      scriptedTestCaseSecondLevelViewPage,
      'Project',
      'Campaign',
      'Iteration',
    );

    cy.log('Step 2');
    scriptedTestCaseSecondLevelViewPage.assertIconNotExists('sqtm-core-generic:more');

    cy.log('Step 3');
    verifyCannotChangeHeaderOnTestCaseViewPage(testCasViewPage);

    cy.log('Step 4');
    assertCannotModifyNatureTagOrDescription(testCasViewPage, scriptedTestCaseSecondLevelViewPage);

    cy.log('Step 5');
    scmRepositoryField.assertIsNotEditable();

    cy.log('Step 6');
    assertCanAccessScriptPageButCannotEdit(testCasViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withCufs([new CustomFieldBuilder('Tag', InputType.TAG)])
    .withServers([
      new SCMServerBuilder('server', 'http://url.com').withRepositories([
        new SCMRepositoryBuilder('server', 'main'),
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project')
        .withTestCaseLibraryNodes([new ScriptedTestCaseBuilder('Gherkin')])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaign').withIterations([
            new IterationBuilder('Iteration').withTestPlanItems([
              new IterationTestPlanItemBuilder('Gherkin'),
            ]),
          ]),
        ])
        .withCufsOnProject([new CustomFieldOnProjectBuilder('Tag', BindableEntity.TEST_CASE)]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}

function assertCannotModifyNatureTagOrDescription(
  testCasViewPage: TestCaseViewPage,
  scriptedTestCaseSecondLevelViewPage: TestCaseViewSubPage,
) {
  testCasViewPage.informationPanel.natureSelectField.assertIsNotEditable();
  testCasViewPage.informationPanel.descriptionRichField.assertIsNotEditable();
  const customField = scriptedTestCaseSecondLevelViewPage.getTagCustomField('Tag');
  customField.assertIsNotEditable();
}
