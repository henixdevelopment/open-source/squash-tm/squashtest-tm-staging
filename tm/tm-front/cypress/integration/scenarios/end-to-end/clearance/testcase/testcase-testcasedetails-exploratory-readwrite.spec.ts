import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { ExploratoryTestCaseBuilder } from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { DatabaseUtils } from '../../../../utils/database.utils';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import {
  MilestoneBuilder,
  MilestoneOnProjectBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/milestone-prerequisite-builders';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {
  accessSecondLevelPageTestCaseThroughSearchAndSelectTestCase,
  assertSessionIsPresentInSessionPage,
  associateAndDissociateMilestoneOnTestCase,
  modifyChart,
  modifyDurationOfSession,
  renameChangeTypeTestAndModifyInformationBloc,
} from '../../../scenario-parts/testcase.part';
import { TestCaseViewSubPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-sub-page';
import { CapsuleElement } from '../../../../page-objects/elements/workspace-common/capsule.element';
import {
  CampaignBuilder,
  CampaignTestPlanItemBuilder,
  ExploratorySessionBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Assert that a user can modify an exploratory test case on second level page', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC070201.CT04 - testcase-testcasedetails-exploratory-readwrite', () => {
    cy.log('step 1');
    cy.logInAs('RonW', 'admin');
    const testCaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const testCaseSubViewPage = new TestCaseViewSubPage('*');

    accessSecondLevelPageTestCaseThroughSearchAndSelectTestCase(testCaseWorkspace, 'Exploratoire');

    cy.log('step 2');
    modifyCapsuleStatus(testCaseSubViewPage.testCaseViewPage.statusCapsule);

    cy.log('step 3');
    renameChangeTypeTestAndModifyInformationBloc(
      testCaseSubViewPage.testCaseViewPage,
      'Hello',
      'Reference',
      'Performance',
      'Non-régression',
      'Moyenne',
    );

    cy.log('step 4');
    associateAndDissociateMilestoneOnTestCase(testCaseSubViewPage.testCaseViewPage, 'Milestone');

    cy.log('step 5');
    modifyDurationOfSession(testCaseSubViewPage.testCaseViewPage, '5', '10');

    cy.log('step 6');
    modifyChart(testCaseSubViewPage.testCaseViewPage, 'Back to 505');

    cy.log('step 7');
    assertSessionIsPresentInSessionPage(testCaseSubViewPage.testCaseViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('test case modifier', [
        ProfileBuilderPermission.TEST_CASE_READ,
        ProfileBuilderPermission.TEST_CASE_WRITE,
      ]),
    ])
    .withMilestones([new MilestoneBuilder('Milestone', '10-12-2025')])
    .withCufs([new CustomFieldBuilder('ListeDeroulante', InputType.NUMERIC)])
    .withProjects([
      new ProjectBuilder('AZERTY')
        .withCufsOnProject([
          new CustomFieldOnProjectBuilder('ListeDeroulante', BindableEntity.TEST_CASE),
        ])
        .withMilestonesOnProject([new MilestoneOnProjectBuilder('Milestone')])
        .withTestCaseLibraryNodes([new ExploratoryTestCaseBuilder('Exploratoire')])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaign')
            .withTestPlanItems([new CampaignTestPlanItemBuilder('Exploratoire')])
            .withIterations([
              new IterationBuilder('Iteration').withTestPlanItems([
                new IterationTestPlanItemBuilder('Exploratoire').withSessions([
                  new ExploratorySessionBuilder('Exploratoire'),
                ]),
              ]),
            ]),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}
function modifyCapsuleStatus(capsuleStatus: CapsuleElement) {
  capsuleStatus.selectOption('Approuvé');
  capsuleStatus.assertContainsText('Approuvé');
}
