import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { RequirementBuilder } from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { TestCaseFolderViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case-folder/test-case-folder-view.page';
import { TestCaseViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { TestCaseViewScriptPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-script.page';
import { KeywordTestStepsViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/keyword-test-step-view-page';
import { TestCaseViewCharterPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-charter.page';
import { TreeToolbarElement } from '../../../../page-objects/elements/workspace-common/tree-toolbar.element';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  createExploratoryTestCaseAndAssertTypeOfTestCases,
  createTestCaseByKind,
  createTestCaseFolder,
  createTestCaseFromRequirementImport,
  selectProjectInTestCaseWorkspace,
} from '../../../scenario-parts/testcase.part';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can create test case, test case folder and scripted/keyword test case', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07021.CT01 - testcase-create-createdelete', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testCaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const testCaseWorkSpaceTree = testCaseWorkspace.tree;
    const testCaseFolderViewPage = new TestCaseFolderViewPage(1);
    const testCaseViewPage = new TestCaseViewPage(2);
    const scriptedTestCaseViewPage = new TestCaseViewScriptPage();
    const keywordTestCase = new KeywordTestStepsViewPage();
    const exploratoryTestCase = new TestCaseViewCharterPage();
    const treeToolbarElement = new TreeToolbarElement('requirement-toolbar');

    createTestCaseFolder(testCaseWorkspace, testCaseFolderViewPage, 'Athena', 'Lala');

    cy.log('Step 2');
    createClassicTestCase(testCaseWorkspace, testCaseViewPage);

    cy.log('Step 3');
    createScriptedTestCase(testCaseWorkspace);

    cy.log('Step 4');
    createTestCaseByKind('Keyword', true, 'BDD');

    cy.log('Step 5');
    createExploratoryTestCaseAndAssertTypeOfTestCases(
      exploratoryTestCase,
      testCaseWorkSpaceTree,
      keywordTestCase,
      scriptedTestCaseViewPage,
      testCaseViewPage,
      'Exploratory',
      'Keyword',
      'Scripted',
    );

    cy.log('Step 6');
    createTestCaseFromRequirementImport(
      testCaseWorkspace,
      testCaseWorkSpaceTree,
      treeToolbarElement,
      'Athena',
      'Exigence',
    );
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('test case creator', [
        ProfileBuilderPermission.TEST_CASE_READ,
        ProfileBuilderPermission.TEST_CASE_CREATE,
        ProfileBuilderPermission.REQUIREMENT_READ,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Athena').withRequirementLibraryNodes([
        new RequirementBuilder('Exigence'),
      ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}

function createClassicTestCase(
  testCaseWorkspace: TestCaseWorkspacePage,
  testCaseViewPage: TestCaseViewPage,
) {
  selectProjectInTestCaseWorkspace(testCaseWorkspace, 'Athena');
  testCaseWorkspace.createObjectInTestCaseWorkspace('new-test-case');
  createTestCaseByKind('Hello', false, 'Classique');
  testCaseViewPage.assertExists();
}

function createScriptedTestCase(testCaseWorkspace: TestCaseWorkspacePage) {
  testCaseWorkspace.createObjectInTestCaseWorkspace('new-test-case');
  createTestCaseByKind('Scripted', true, 'Gherkin');
}
