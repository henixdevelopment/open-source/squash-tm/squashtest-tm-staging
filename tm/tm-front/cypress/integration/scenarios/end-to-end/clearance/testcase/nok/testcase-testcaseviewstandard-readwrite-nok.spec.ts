import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  DataSetBuilder,
  DataSetParameterValueBuilder,
  ParameterBuilder,
  TestCaseBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { BindableEntity } from '../../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { DatabaseUtils } from '../../../../../utils/database.utils';
import {
  SCMRepositoryBuilder,
  SCMServerBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/server-prerequisite-builders';
import { TestCaseWorkspacePage } from '../../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { TestCaseViewPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { EditableRichTextFieldElement } from '../../../../../page-objects/elements/forms/editable-rich-text-field.element';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import {
  assertCannotChangeValueOfParameterInDataset,
  selectNodeInTestCaseWorkspace,
  verifyCannotChangeHeaderOnTestCaseViewPage,
} from '../../../../scenario-parts/testcase.part';
describe('Check if a user with Guest can perform the following actions: view the test case, print the test case.However, should not be able to: modify attributes, add or duplicate a data set (JDD), modify the value of a parameter', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07220.CT02 - testcase-testcaseviewstandard-readwrite-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testCaseWorkSpace = TestCaseWorkspacePage.initTestAtPage();
    const testCaseViewPage = new TestCaseViewPage(1);

    verifyPrintButtonIsNotPresentOnTestCaseViewPage(testCaseWorkSpace, testCaseViewPage);

    cy.log('Step 2');
    verifyCannotChangeHeaderOnTestCaseViewPage(testCaseViewPage);

    cy.log('Step 3');
    verifyCannotChangeStatusCufAndDescriptionInTestCaseViewPage(testCaseViewPage);

    cy.log('Step 4');
    verifyCannotAddDataSetToTestCase(testCaseViewPage);

    cy.log('Step 5');
    verifyCopyPasteOptions(testCaseViewPage);

    cy.log('Step 6');
    assertCannotChangeValueOfParameterInDataset(testCaseViewPage, 'Token', 'GitLab');

    cy.log('Step 7');
    addScmToTestCase(testCaseViewPage);
  });

  function initTestData() {
    return new PrerequisiteBuilder()
      .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
      .withCufs([new CustomFieldBuilder('Java', InputType.RICH_TEXT)])
      .withServers([
        new SCMServerBuilder('Server', 'http://fakeUrl.com').withRepositories([
          new SCMRepositoryBuilder('Repo Server', 'master'),
        ]),
      ])
      .withProjects([
        new ProjectBuilder('Project')
          .withTestCaseLibraryNodes([
            new TestCaseBuilder('Test')
              .withParameters([new ParameterBuilder('GitLab')])
              .withDataSets([
                new DataSetBuilder('Token').withParamValues([
                  new DataSetParameterValueBuilder('GitLab', 'AsxER1245d'),
                ]),
              ]),
          ])
          .withCufsOnProject([new CustomFieldOnProjectBuilder('Java', BindableEntity.TEST_CASE)]),
      ])
      .build();
  }
  function initPermissions() {
    addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
  }
  function verifyPrintButtonIsNotPresentOnTestCaseViewPage(
    testCaseWorkSpace: TestCaseWorkspacePage,
    testCaseViewPage: TestCaseViewPage,
  ) {
    selectNodeInTestCaseWorkspace(testCaseWorkSpace, 'Project', 'Test');
    testCaseViewPage.assertExists();
    testCaseViewPage.assertIconNotExists('sqtm-core-generic:more');
  }

  function verifyCannotChangeStatusCufAndDescriptionInTestCaseViewPage(
    testCaseViewPage: TestCaseViewPage,
  ) {
    testCaseViewPage.informationPanel.statusSelectField.assertIsNotEditable();
    const cufRichText = testCaseViewPage.getCustomField(
      'Java',
      InputType.RICH_TEXT,
    ) as EditableRichTextFieldElement;
    cufRichText.assertIsNotEditable();
    testCaseViewPage.informationPanel.descriptionRichField.assertIsNotEditable();
  }

  function verifyCannotAddDataSetToTestCase(testCaseViewPage: TestCaseViewPage) {
    testCaseViewPage.clickParametersAnchorLink().addDataSetNotExist();
  }

  function verifyCopyPasteOptions(testCaseViewPage: TestCaseViewPage) {
    const testCaseViewParameters = testCaseViewPage.clickParametersAnchorLink();
    testCaseViewParameters.assertCopyDataSetToOtherTestCaseIsClickable('Token');
    testCaseViewParameters.assertDuplicateDataSetIsNotClickable('Token');
  }

  function addScmToTestCase(testCaseViewPage: TestCaseViewPage) {
    const testCaseViewAutomationPage = testCaseViewPage.clickAutomationAnchorLink();
    testCaseViewAutomationPage.scmRepository.assertIsNotEditable();
  }
});
