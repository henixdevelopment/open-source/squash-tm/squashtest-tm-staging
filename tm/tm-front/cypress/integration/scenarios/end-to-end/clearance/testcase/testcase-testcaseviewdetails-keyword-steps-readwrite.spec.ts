import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  KeywordStepBuilder,
  KeywordTestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { RequirementBuilder } from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import { RequirementVersionsLinkToVerifyingTestCaseBuilder } from '../../../../utils/end-to-end/prerequisite/builders/link-builders';
import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { RequirementViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import { TestCaseViewSubPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-sub-page';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import { accessToTestCaseSecondLevelViewPageFromRequirementPage } from '../../../scenario-parts/testcase.part';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can modify and move steps in second view page of a keyword testcase', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC070201.CT06 - testcase-testcaseviewdetails-keyword-steps-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspace = RequirementWorkspacePage.initTestAtPage();
    const requirementViewPage = new RequirementViewPage();
    const secondLevelTestCaseViewPage = new TestCaseViewSubPage(1);

    accessSecondLevelViewPageAndModifyKeyword(
      requirementWorkspace,
      requirementViewPage,
      secondLevelTestCaseViewPage,
    );

    cy.log('Step 2');
    modifyStepsOrder(secondLevelTestCaseViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('test case modifier', [
        ProfileBuilderPermission.TEST_CASE_READ,
        ProfileBuilderPermission.TEST_CASE_WRITE,
        ProfileBuilderPermission.REQUIREMENT_READ,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('AZERTY')
        .withRequirementLibraryNodes([new RequirementBuilder('Hey')])
        .withTestCaseLibraryNodes([
          new KeywordTestCaseBuilder('Pomme').withKeywordSteps([
            new KeywordStepBuilder('GIVEN', 'the cold'),
            new KeywordStepBuilder('WHEN', 'lalala'),
            new KeywordStepBuilder('THEN', 'call santa'),
          ]),
        ])
        .withLinks([new RequirementVersionsLinkToVerifyingTestCaseBuilder(['Hey'], 'Pomme')]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}
function accessSecondLevelViewPageAndModifyKeyword(
  requirementWorkspace: RequirementWorkspacePage,
  requirementViewPage: RequirementViewPage,
  secondLevelTestCaseViewPage: TestCaseViewSubPage,
) {
  accessToTestCaseSecondLevelViewPageFromRequirementPage(
    requirementViewPage,
    requirementWorkspace,
    secondLevelTestCaseViewPage,
    'AZERTY',
    'Hey',
    'Pomme',
  );
  const viewKeywordTestSteps =
    secondLevelTestCaseViewPage.testCaseViewPage.clickKeywordStepsAnchorLink();
  viewKeywordTestSteps.getStepByIndex(0).getKeywordField().selectValueNoButton('But');
  viewKeywordTestSteps.getStepByIndex(0).getKeywordField().assertContainsText('But');
}

function modifyStepsOrder(
  secondLevelTestCaseViewPage: TestCaseViewSubPage,
  viewKeywordTestSteps = secondLevelTestCaseViewPage.testCaseViewPage.clickKeywordStepsAnchorLink(),
) {
  viewKeywordTestSteps.getStepByIndex(2).moveStepUp();
  viewKeywordTestSteps.checkStepsOrder([1, 3, 2]);
}
