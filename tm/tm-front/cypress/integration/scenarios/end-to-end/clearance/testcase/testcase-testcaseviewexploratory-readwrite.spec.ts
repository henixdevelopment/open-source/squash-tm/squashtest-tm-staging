import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
  ListOptionCustomFieldBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { ExploratoryTestCaseBuilder } from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {
  modifyChart,
  modifyDurationOfSession,
  renameChangeTypeTestAndModifyInformationBloc,
  selectNodeInTestCaseWorkspace,
} from '../../../scenario-parts/testcase.part';
import { TestCaseViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { DatabaseUtils } from '../../../../utils/database.utils';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Assert that a user can modify an exploratory test case', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07020.CT05 - testcase-testcaseviewexploratory-readwrite', () => {
    cy.log('step 1');
    cy.logInAs('RonW', 'admin');
    const testcaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const testCasePage = new TestCaseViewPage('*');

    selectNodeInTestCaseWorkspace(testcaseWorkspace, 'AZERTY', 'Exploratoire');
    renameChangeTypeTestAndModifyInformationBloc(
      testCasePage,
      'Hello',
      'Reference',
      'Métier',
      'Correctif',
      'Très haute',
    );

    cy.log('step 2');
    selectValueCufList(testCasePage);

    cy.log('step 3');
    modifyDurationOfSession(testCasePage, '10', '30');

    cy.log('step 4');
    modifyChart(testCasePage, 'Hey');
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('test case modifier', [
        ProfileBuilderPermission.TEST_CASE_READ,
        ProfileBuilderPermission.TEST_CASE_WRITE,
      ]),
    ])
    .withCufs([
      new CustomFieldBuilder('ListeDeroulante', InputType.DROPDOWN_LIST).withOptions([
        new ListOptionCustomFieldBuilder('Lol', 'Lol'),
        new ListOptionCustomFieldBuilder('Plop', 'Plop'),
      ]),
    ])
    .withProjects([
      new ProjectBuilder('AZERTY')
        .withCufsOnProject([
          new CustomFieldOnProjectBuilder('ListeDeroulante', BindableEntity.TEST_CASE),
        ])
        .withTestCaseLibraryNodes([new ExploratoryTestCaseBuilder('Exploratoire')]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}
function selectValueCufList(testCaseViewPage: TestCaseViewPage) {
  testCaseViewPage.getSelectCustomField('ListeDeroulante').selectValueNoButton('Lol');
}
