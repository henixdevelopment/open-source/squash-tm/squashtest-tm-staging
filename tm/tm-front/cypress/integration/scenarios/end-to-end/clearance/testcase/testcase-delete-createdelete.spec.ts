import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  ExploratoryTestCaseBuilder,
  KeywordTestCaseBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
  TestCaseFolderBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import { SimpleDeleteConfirmDialogElement } from '../../../../page-objects/elements/dialog/simple-delete-confirm-dialog.element';
import {
  deleteFolderContainingTestCasesAndAssertDeleted,
  deleteTestCasesAndAssertDeleted,
} from '../../../scenario-parts/testcase.part';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can delete test case inter/intra-project and test case folder inter/intra-project', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07021.CT04 - testcase-delete-createdelete', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testCaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const testCaseWorkspaceTree = testCaseWorkspace.tree;
    const confirmDialog = new SimpleDeleteConfirmDialogElement();

    deleteTestCasesAndAssertDeleted(testCaseWorkspace, testCaseWorkspaceTree, 'Saken', [
      'Classic',
      'Gherkin',
      'BDD',
      'Exploratory',
    ]);

    cy.log('Step 2');
    deleteFolderContainingTestCasesAndAssertDeleted(
      testCaseWorkspaceTree,
      confirmDialog,
      'Dossier1',
    );
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('test case deleter', [
        ProfileBuilderPermission.TEST_CASE_READ,
        ProfileBuilderPermission.TEST_CASE_DELETE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Saken').withTestCaseLibraryNodes([
        new TestCaseBuilder('Classic'),
        new ScriptedTestCaseBuilder('Gherkin'),
        new KeywordTestCaseBuilder('BDD'),
        new ExploratoryTestCaseBuilder('Exploratory'),
        new TestCaseFolderBuilder('Dossier1').withTestCaseLibraryNodes([
          new TestCaseBuilder('Classic'),
          new ScriptedTestCaseBuilder('Gherkin'),
          new KeywordTestCaseBuilder('BDD'),
          new ExploratoryTestCaseBuilder('Exploratory'),
        ]),
      ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}
