import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { RequirementBuilder } from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  ActionTestStepBuilder,
  ExploratoryTestCaseBuilder,
  KeywordTestCaseBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { RequirementVersionsLinkToVerifyingTestCaseBuilder } from '../../../../utils/end-to-end/prerequisite/builders/link-builders';
import { TestStepsViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-steps-view.page';
import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { RequirementViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { TestCaseViewSubPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-sub-page';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import { linkRequirementBySecondLevelViewPageInClassicTestStep } from '../../../scenario-parts/testcase.part';
import { RemoveCoveragesDialogElement } from '../../../../page-objects/pages/test-case-workspace/dialogs/remove-coverages-dialog.element';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe(
  'Check that a user can associate a requirement to a Exploratory test case, to a test step ' +
    'and disassociate a requirement from a Classic, BDD and Gherkin test case',
  function () {
    beforeEach(function () {
      Cypress.Cookies.debug(true);
      DatabaseUtils.cleanDatabase();
      SystemE2eCommands.setMilestoneActivated();
      initTestData();
      initPermissions();
    });

    it('F01-UC07022.CT04 - testcase-testcasedetails-link', () => {
      cy.log('Step 1');
      cy.logInAs('RonW', 'admin');
      const requirementWorkspace = RequirementWorkspacePage.initTestAtPage();
      const requirementViewPage = new RequirementViewPage();
      const testCaseSubViewPage = new TestCaseViewSubPage(1);
      const testStepView = new TestStepsViewPage();
      const requirementTestCaseTable = requirementViewPage.currentVersion.verifyingTestCaseTable;
      const classicTestCaseViewPage = new TestCaseViewSubPage(1);
      const classicTestCaseCoverageTable = classicTestCaseViewPage.testCaseViewPage.coveragesTable;
      const scriptedTestCaseViewPage = new TestCaseViewSubPage(2);
      const scriptedTestCaseCoverageTable =
        scriptedTestCaseViewPage.testCaseViewPage.coveragesTable;
      const keywordTestCaseViewPage = new TestCaseViewSubPage(3);
      const keywordTestCaseCoverageTable = keywordTestCaseViewPage.testCaseViewPage.coveragesTable;
      const exploratoryTestCaseViewPage = new TestCaseViewSubPage(4);
      const exploratoryTestCaseCoverageTable =
        exploratoryTestCaseViewPage.testCaseViewPage.coveragesTable;

      linkRequirementBySecondLevelViewPageInClassicTestStep(
        requirementViewPage,
        requirementWorkspace,
        testCaseSubViewPage,
        testStepView,
        'KARMINE',
        'Football',
        'Classic',
        'Basketball',
      );

      cy.log('Step 2');
      unlinkRequirementFromClassicTestStep(classicTestCaseViewPage, classicTestCaseCoverageTable);

      cy.log('Step 3');
      unlinkRequirementFromSecondLevelViewPageInKeywordTestCase(
        requirementTestCaseTable,
        keywordTestCaseViewPage,
        keywordTestCaseCoverageTable,
      );

      cy.log('Step 4');
      linkRequirementBySecondLevelViewPageInExploratoryTestCaseByDragAndDrop(
        requirementTestCaseTable,
        exploratoryTestCaseViewPage,
        exploratoryTestCaseCoverageTable,
      );

      cy.log('Step 5');
      unlinkRequirementFromSecondLevelViewPageInScriptedTestCase(
        requirementTestCaseTable,
        scriptedTestCaseViewPage,
        scriptedTestCaseCoverageTable,
      );
    });
  },
);

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('test case linker', [
        ProfileBuilderPermission.TEST_CASE_READ,
        ProfileBuilderPermission.TEST_CASE_LINK,
        ProfileBuilderPermission.REQUIREMENT_READ,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('KARMINE')
        .withRequirementLibraryNodes([
          new RequirementBuilder('Football'),
          new RequirementBuilder('Basketball'),
        ])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Classic').withSteps([
            new ActionTestStepBuilder('Peindre le mur en bleu', 'Le mur est bleu'),
          ]),
          new ScriptedTestCaseBuilder('Gherkin'),
          new KeywordTestCaseBuilder('BDD'),
          new ExploratoryTestCaseBuilder('Exploratory'),
        ])
        .withLinks([
          new RequirementVersionsLinkToVerifyingTestCaseBuilder(['Football'], 'Classic'),
          new RequirementVersionsLinkToVerifyingTestCaseBuilder(['Football'], 'BDD'),
          new RequirementVersionsLinkToVerifyingTestCaseBuilder(['Football'], 'Gherkin'),
          new RequirementVersionsLinkToVerifyingTestCaseBuilder(['Football'], 'Exploratory'),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}

function unlinkRequirementFromClassicTestStep(
  classicTestCaseViewPage: TestCaseViewSubPage,
  classicTestCaseCoverageTable: GridElement,
) {
  const deleteLinkRequirementDialog = new RemoveCoveragesDialogElement(2, [3]);
  classicTestCaseViewPage.testCaseViewPage.clickVerifiedRequirementsAnchorLink();
  classicTestCaseCoverageTable.assertRowCount(2);
  classicTestCaseCoverageTable.findRowId('name', 'Football').then((idRequirement) => {
    classicTestCaseViewPage.testCaseViewPage.showDeleteConfirmCoverageDialog(2, idRequirement);
  });
  deleteLinkRequirementDialog.confirm();
  classicTestCaseCoverageTable.assertRowCount(1);
}

function unlinkRequirementFromSecondLevelViewPageInKeywordTestCase(
  requirementTestCaseTable: GridElement,
  keywordTestCaseViewPage: TestCaseViewSubPage,
  keywordTestCaseCoverageTable: GridElement,
) {
  const deleteLinkRequirementDialog = new RemoveCoveragesDialogElement(2, [3]);
  NavBarElement.navigateToRequirementWorkspace();
  requirementTestCaseTable.scrollPosition('center', 'mainViewport');
  requirementTestCaseTable.findRowId('name', 'BDD').then((idKeywordTestCase) => {
    requirementTestCaseTable
      .getCell(idKeywordTestCase, 'name')
      .linkRenderer()
      .findCellLink()
      .click();
  });
  keywordTestCaseCoverageTable.assertRowCount(1);
  keywordTestCaseCoverageTable.findRowId('name', 'Football').then((idRequirement) => {
    keywordTestCaseCoverageTable.selectRow(idRequirement, '#', 'leftViewport');
  });
  keywordTestCaseViewPage.testCaseViewPage.showDeleteConfirmCoveragesDialog(1, [4]);
  deleteLinkRequirementDialog.confirm();
  keywordTestCaseCoverageTable.assertRowCount(0);
}

function linkRequirementBySecondLevelViewPageInExploratoryTestCaseByDragAndDrop(
  requirementTestCaseTable: GridElement,
  exploratoryTestCaseViewPage: TestCaseViewSubPage,
  exploratoryTestCaseCoverageTable: GridElement,
) {
  NavBarElement.navigateToRequirementWorkspace();
  requirementTestCaseTable.scrollPosition('center', 'mainViewport');
  requirementTestCaseTable.findRowId('name', 'Exploratory').then((idKeywordTestCase) => {
    requirementTestCaseTable
      .getCell(idKeywordTestCase, 'name')
      .linkRenderer()
      .findCellLink()
      .click();
  });
  const requirementLinkTree = exploratoryTestCaseViewPage.testCaseViewPage.openRequirementDrawer();
  requirementLinkTree.findRowId('NAME', 'Basketball').then((idRequirement) => {
    requirementLinkTree.beginDragAndDrop(idRequirement);
  });
  exploratoryTestCaseViewPage.testCaseViewPage.dropRequirementIntoTestCase(4);
  exploratoryTestCaseViewPage.testCaseViewPage.closeRequirementDrawer();
  exploratoryTestCaseCoverageTable.findRowId('name', 'Basketball').then((idRequirement) => {
    exploratoryTestCaseCoverageTable.assertRowExist(idRequirement);
  });
}

function unlinkRequirementFromSecondLevelViewPageInScriptedTestCase(
  requirementTestCaseTable: GridElement,
  scriptedTestCaseViewPage: TestCaseViewSubPage,
  scriptedTestCaseCoverageTable: GridElement,
) {
  const deleteLinkRequirementDialog = new RemoveCoveragesDialogElement(4, [1]);
  NavBarElement.navigateToRequirementWorkspace();
  requirementTestCaseTable.scrollPosition('center', 'mainViewport');
  requirementTestCaseTable.findRowId('name', 'Gherkin').then((idKeywordTestCase) => {
    requirementTestCaseTable
      .getCell(idKeywordTestCase, 'name')
      .linkRenderer()
      .findCellLink()
      .click();
  });
  scriptedTestCaseViewPage.testCaseViewPage.clickVerifiedRequirementsAnchorLink();
  scriptedTestCaseCoverageTable.assertRowCount(1);
  scriptedTestCaseCoverageTable.findRowId('name', 'Football').then((idRequirement) => {
    scriptedTestCaseViewPage.testCaseViewPage.showDeleteConfirmCoverageDialog(2, idRequirement);
  });
  deleteLinkRequirementDialog.confirm();
  scriptedTestCaseCoverageTable.assertRowCount(0);
}
