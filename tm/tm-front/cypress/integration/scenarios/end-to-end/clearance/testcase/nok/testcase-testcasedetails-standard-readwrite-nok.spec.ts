import { DatabaseUtils } from '../../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { RequirementWorkspacePage } from '../../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  MilestoneBuilder,
  MilestoneOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/milestone-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
  ListOptionCustomFieldBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { RequirementBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import { RequirementVersionsLinkToVerifyingTestCaseBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/link-builders';
import {
  DataSetBuilder,
  DataSetParameterValueBuilder,
  ParameterBuilder,
  TestCaseBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { BindableEntity } from '../../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { RequirementViewPage } from '../../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import { TestCaseViewSubPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-sub-page';
import {
  accessToTestCaseSecondLevelViewPageFromRequirementPage,
  assertCannotAddTechnologyToKeywordTestCase,
  assertCannotChangeValueOfParameterInDataset,
  verifyCannotChangeHeaderNameAndStatusInKeywordTestCase,
} from '../../../../scenario-parts/testcase.part';
import { EditableSelectFieldElement } from '../../../../../page-objects/elements/forms/editable-select-field.element';
import { TestCaseViewPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';

describe('Check if a user with Guest cannot perform the following actions on the level 2 test case: modify attributes, rename the test case, associate a milestone, change the automated test technology, delete a test data set (JDD), edit a parameter name and delete.', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC070201.CT01 - testcase-testcasedetails-standard-readwrite-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspace = RequirementWorkspacePage.initTestAtPage();
    const requirementViewPage = new RequirementViewPage();
    const testCaseViewDetailsPage = new TestCaseViewSubPage(1);
    const testCaseViewPage = testCaseViewDetailsPage.testCaseViewPage;
    const milestonesTagFieldElement = testCaseViewPage.informationPanel.milestonesField;
    accessToTestCaseSecondLevelViewPageFromRequirementPage(
      requirementViewPage,
      requirementWorkspace,
      testCaseViewDetailsPage,
      'Fruits',
      'Pome fruit',
      'Apple',
    );

    cy.log('Step 2');
    verifyCannotChangeHeaderNameAndStatusInKeywordTestCase(testCaseViewPage);

    cy.log('Step 3');
    assertCanEditTypeFieldAndSelectDropdown(testCaseViewPage, testCaseViewDetailsPage);

    cy.log('Step 4');
    milestonesTagFieldElement.openMilestoneDialogNotExist();

    cy.log('Step 5');
    assertCannotAddTechnologyToKeywordTestCase(testCaseViewPage);

    cy.log('Step 6');
    assertCannotChangeValueOfParameterInDataset(testCaseViewPage, 'JDD1', 'Variety');

    cy.log('Step 7');
    const testCaseViewParameters = testCaseViewPage.clickParametersAnchorLink();
    testCaseViewParameters.openMenuParamAndCheckDeleteOptionNotExist('PARAM_COLUMN_1');

    cy.log('Step 8');
    testCaseViewParameters.iconDeleteDataSetNotExist('1');
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withMilestones([
      new MilestoneBuilder('Milestone', '2025-10-31 14:44:45').withUserLogin('RonW'),
    ])
    .withCufs([
      new CustomFieldBuilder('CufList', InputType.DROPDOWN_LIST).withOptions([
        new ListOptionCustomFieldBuilder('Choix 1', 'code1'),
        new ListOptionCustomFieldBuilder('Choix 2', 'code2'),
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Fruits')
        .withRequirementLibraryNodes([new RequirementBuilder('Pome fruit')])
        .withLinks([new RequirementVersionsLinkToVerifyingTestCaseBuilder(['Pome fruit'], 'Apple')])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Apple')
            .withParameters([new ParameterBuilder('Variety')])
            .withDataSets([
              new DataSetBuilder('JDD1').withParamValues([
                new DataSetParameterValueBuilder('Variety', 'Pink lady'),
              ]),
            ]),
        ])
        .withMilestonesOnProject([new MilestoneOnProjectBuilder('Milestone')])
        .withCufsOnProject([new CustomFieldOnProjectBuilder('CufList', BindableEntity.TEST_CASE)]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}

function assertCanEditTypeFieldAndSelectDropdown(
  testCaseViewPage: TestCaseViewPage,
  testCaseViewDetailsPage: TestCaseViewSubPage,
) {
  const typeSelectField = testCaseViewPage.informationPanel.typeSelectField;
  const dropdownListCuf = testCaseViewDetailsPage.getCustomField(
    'CufList',
    InputType.DROPDOWN_LIST,
  ) as EditableSelectFieldElement;

  typeSelectField.assertIsNotEditable();
  dropdownListCuf.assertIsNotEditable();
}
