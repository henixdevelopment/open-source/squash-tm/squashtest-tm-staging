import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { TestCaseFolderBuilder } from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { TestCaseFolderViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case-folder/test-case-folder-view.page';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
  ListOptionCustomFieldBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { FolderInformationPanelElement } from '../../../../page-objects/elements/panels/folder-information-panel.element';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  changeNameAndDescriptionOfTestcaseFolder,
  selectFolderAndCheckIfParameterButtonWork,
} from '../../../scenario-parts/testcase.part';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe(
  'Check that a user can consult, rename, modify the description and the ' +
    'cuf tag of a testcase folder ',
  function () {
    beforeEach(function () {
      Cypress.Cookies.debug(true);
      DatabaseUtils.cleanDatabase();
      SystemE2eCommands.setMilestoneActivated();
      initTestData();
      initPermissions();
    });

    it('F01-UC07020.CT01 - testcase-folderview-readwrite', () => {
      cy.log('Step 1');
      cy.logInAs('RonW', 'admin');
      const testcaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
      const testCaseFolderViewPage = new TestCaseFolderViewPage(1);
      const informationPanel = new FolderInformationPanelElement();

      selectFolderAndCheckIfParameterButtonWork(
        testcaseWorkspace,
        testCaseFolderViewPage,
        'Athena',
        'Zeus',
      );

      cy.log('Step 2');
      changeNameAndDescriptionOfTestcaseFolder(testCaseFolderViewPage, 'Bleu', 'Hello World');

      cy.log('Step 3');
      selectTagInCuf(informationPanel);
    });

    function initTestData() {
      return new PrerequisiteBuilder()
        .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
        .withProfiles([
          new ProfileBuilder('test case modifier', [
            ProfileBuilderPermission.TEST_CASE_READ,
            ProfileBuilderPermission.TEST_CASE_WRITE,
          ]),
        ])
        .withCufs([
          new CustomFieldBuilder('TagCuf', InputType.TAG).withOptions([
            new ListOptionCustomFieldBuilder('opt1', 'code1'),
            new ListOptionCustomFieldBuilder('opt2', 'code2'),
          ]),
        ])
        .withProjects([
          new ProjectBuilder('Athena')
            .withTestCaseLibraryNodes([new TestCaseFolderBuilder('Zeus')])
            .withCufsOnProject([
              new CustomFieldOnProjectBuilder('TagCuf', BindableEntity.TESTCASE_FOLDER),
            ]),
        ])
        .build();
    }
    function initPermissions() {
      addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
    }
    function selectTagInCuf(informationPanel: FolderInformationPanelElement) {
      informationPanel.addCufTag('TagCuf', 'opt1');
    }
  },
);
