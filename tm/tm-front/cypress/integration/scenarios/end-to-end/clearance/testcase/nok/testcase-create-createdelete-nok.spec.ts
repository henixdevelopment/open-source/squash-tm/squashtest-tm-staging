import { DatabaseUtils } from '../../../../../utils/database.utils';
import { TestCaseWorkspacePage } from '../../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { RequirementBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { NavBarElement } from '../../../../../page-objects/elements/nav-bar/nav-bar.element';
import { selectNodeInAnotherNode } from '../../../../scenario-parts/requirement.part';
import { TreeToolbarElement } from '../../../../../page-objects/elements/workspace-common/tree-toolbar.element';
import { selectProjectInTestCaseWorkspace } from '../../../../scenario-parts/testcase.part';

describe('Check that a Guest cannot create test case, test case folder and test case from requirement import', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07221.CT01 - testcase-create-createdelete-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testCaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const treeToolbarElement = new TreeToolbarElement('requirement-toolbar');

    testCaseWorkspace.assertCannotCreateObjectInTestCaseWorkspace('new-folder');

    cy.log('Step 2');
    testCaseWorkspace.assertCannotCreateObjectInTestCaseWorkspace('new-test-case');

    cy.log('Step 3');
    verifyCannotCreateTestCaseFromRequirementImport(testCaseWorkspace, treeToolbarElement);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProjects([
      new ProjectBuilder('Project').withRequirementLibraryNodes([
        new RequirementBuilder('Requirement'),
      ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}

function verifyCannotCreateTestCaseFromRequirementImport(
  testCaseWorkspace: TestCaseWorkspacePage,
  treeToolbarElement: TreeToolbarElement,
) {
  const requirementWorkspace = NavBarElement.navigateToRequirementWorkspace();
  selectNodeInAnotherNode(requirementWorkspace, 'Project', 'Requirement');
  treeToolbarElement.getButton('copy-button').clickWithoutSpan();
  NavBarElement.navigateToTestCaseWorkspace();
  selectProjectInTestCaseWorkspace(testCaseWorkspace, 'Project');
  testCaseWorkspace.openImportExportMenuAndAssertOptionIsDisable('add-test-case');
}
