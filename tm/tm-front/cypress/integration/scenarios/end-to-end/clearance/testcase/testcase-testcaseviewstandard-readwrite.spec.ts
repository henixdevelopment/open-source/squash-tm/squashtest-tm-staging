import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  DataSetBuilder,
  DataSetParameterValueBuilder,
  ParameterBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import {
  SCMRepositoryBuilder,
  SCMServerBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/server-prerequisite-builders';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { TestCaseViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { EditableRichTextFieldElement } from '../../../../page-objects/elements/forms/editable-rich-text-field.element';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check if a user can do modifications on test case', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07020.CT02 - testcase-testcaseviewstandard-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testCaseWorkSpace = TestCaseWorkspacePage.initTestAtPage();
    const testCaseViewPage = new TestCaseViewPage(1);

    assertPrintButtonExistOnTestCaseViewPage(testCaseWorkSpace, testCaseViewPage);

    cy.log('Step 2');
    modifyHeaderOnTestCaseViewPage(testCaseViewPage);

    cy.log('Step 3');
    modifyStatusCufAndDescriptionInTestCaseViewPage(testCaseViewPage);

    cy.log('Step 4');
    addDataSetToTestCase(testCaseViewPage);

    cy.log('Step 5');
    copyDataSetInTestCase(testCaseViewPage);

    cy.log('Step 6');
    changeValueOfParameterInDataset(testCaseViewPage);

    cy.log('Step 7');
    addScmToTestCase(testCaseViewPage);
  });

  function initTestData() {
    return new PrerequisiteBuilder()
      .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
      .withProfiles([
        new ProfileBuilder('test case modifier', [
          ProfileBuilderPermission.TEST_CASE_READ,
          ProfileBuilderPermission.TEST_CASE_WRITE,
          ProfileBuilderPermission.TEST_CASE_EXPORT,
        ]),
      ])
      .withCufs([new CustomFieldBuilder('Coco', InputType.RICH_TEXT)])
      .withServers([
        new SCMServerBuilder('ADB', 'http://fakeUrl.com').withRepositories([
          new SCMRepositoryBuilder('aaa', 'aaa'),
        ]),
      ])
      .withProjects([
        new ProjectBuilder('AZERTY')
          .withTestCaseLibraryNodes([
            new TestCaseBuilder('Pomme')
              .withParameters([new ParameterBuilder('Coca')])
              .withDataSets([
                new DataSetBuilder('Smite').withParamValues([
                  new DataSetParameterValueBuilder('Coca', 'Water'),
                ]),
              ]),
          ])
          .withCufsOnProject([new CustomFieldOnProjectBuilder('Coco', BindableEntity.TEST_CASE)]),
      ])
      .build();
  }
  function initPermissions() {
    addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
  }
  function assertPrintButtonExistOnTestCaseViewPage(
    testCaseWorkSpace: TestCaseWorkspacePage,
    testCaseViewPage: TestCaseViewPage,
  ) {
    testCaseWorkSpace.assertExists();
    testCaseWorkSpace.tree.findRowId('NAME', 'AZERTY').then((idProject) => {
      testCaseWorkSpace.tree.selectNode(idProject);
      testCaseWorkSpace.tree.openNode(idProject);
    });
    testCaseWorkSpace.tree.findRowId('NAME', 'Pomme').then((idTestCase) => {
      testCaseWorkSpace.tree.selectNode(idTestCase);
    });
    testCaseViewPage.assertExists();
    testCaseViewPage.clickActionMenuAndAssertPrintModeExists();
  }

  function modifyHeaderOnTestCaseViewPage(testCaseViewPage: TestCaseViewPage) {
    testCaseViewPage.informationPanel.referenceTextField.clearTextField();
    testCaseViewPage.informationPanel.referenceTextField.setValue('Oiseau');
    testCaseViewPage.informationPanel.referenceTextField.clickOnConfirmButton();
    testCaseViewPage.informationPanel.referenceTextField.assertContainsText('Oiseau');
    testCaseViewPage.importanceCapsule.selectOption('Très haute');
    testCaseViewPage.importanceCapsule.assertContainsText('Très haute');
  }

  function modifyStatusCufAndDescriptionInTestCaseViewPage(testCaseViewPage: TestCaseViewPage) {
    testCaseViewPage.informationPanel.statusSelectField.selectValueNoButton('Obsolète');
    const cufRichText = testCaseViewPage.getCustomField(
      'Coco',
      InputType.RICH_TEXT,
    ) as EditableRichTextFieldElement;
    cufRichText.click();
    cufRichText.setValue('coca');
    cufRichText.clickButton('confirm');
    cufRichText.assertContainsText('coca');
    cy.clickVoid();
    testCaseViewPage.informationPanel.descriptionRichField.setAndConfirmValue('Fanta');
  }

  function addDataSetToTestCase(testCaseViewPage: TestCaseViewPage) {
    const datasetView = testCaseViewPage.clickParametersAnchorLink().openAddDataSetDialog();
    datasetView.fillName('Lol');
    datasetView.addDataSet();
    testCaseViewPage.clickParametersAnchorLink().checkExistingDataSet('Lol');
    cy.clickVoid();
  }

  function copyDataSetInTestCase(testCaseViewPage: TestCaseViewPage) {
    testCaseViewPage
      .clickParametersAnchorLink()
      .parametersTable.findRowId('name', 'Smite', 'leftViewport')
      .then((idDataSet) => {
        testCaseViewPage
          .clickParametersAnchorLink()
          .openDuplicateDataSetDialogAndCopyDataSet(idDataSet, 'Smite-Copie1');
      });
  }

  function changeValueOfParameterInDataset(testCaseViewPage: TestCaseViewPage) {
    testCaseViewPage.clickParametersAnchorLink().changeParamValue('Smite', 'Coca', 'Orange');
  }

  function addScmToTestCase(testCaseViewPage: TestCaseViewPage) {
    testCaseViewPage.clickAutomationAnchorLink().chooseScm('http://fakeUrl.com');
  }
});
