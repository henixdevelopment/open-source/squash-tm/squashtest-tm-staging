import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import {
  DataSetBuilder,
  DataSetParameterValueBuilder,
  KeywordTestCaseBuilder,
  ParameterBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { BindableEntity } from '../../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { DatabaseUtils } from '../../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { TestCaseWorkspacePage } from '../../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { TestCaseViewPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  MilestoneBuilder,
  MilestoneOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/milestone-prerequisite-builders';
import {
  assertCannotAddTechnologyToKeywordTestCase,
  assertCannotAssociateOrDissociateMilestoneInTestCaseViewPage,
  assertCannotDuplicateDataSet,
  copyDatasetIntoAnotherKeywordTestCase,
  selectNodeInTestCaseWorkspace,
  verifyCannotChangeHeaderNameAndStatusInKeywordTestCase,
  verifyCannotModifyNatureAndDateCufInInformationBlock,
} from '../../../../scenario-parts/testcase.part';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';

describe('Check if a user with Guest cannot perform the following actions on the BDD test case:modify attributes, associate/dissociate a milestone, rename the test case, modify the automated test technology,add a parameter.However, the user can copy a data set (JDD) to another test case.', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07220.CT03 - testcase-testcaseviewkeyword-readwrite-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testcaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const keywordTestcaseViewPage = new TestCaseViewPage(2);
    selectNodeInTestCaseWorkspace(testcaseWorkspace, 'Project', 'TestA');
    verifyCannotChangeHeaderNameAndStatusInKeywordTestCase(keywordTestcaseViewPage);
    cy.log('Step 2');
    verifyCannotModifyNatureAndDateCufInInformationBlock(keywordTestcaseViewPage);

    cy.log('Step 3');
    assertCannotAssociateOrDissociateMilestoneInTestCaseViewPage(
      testcaseWorkspace,
      keywordTestcaseViewPage,
      'Project',
      'TestB',
      'Jalon',
    );

    cy.log('Step 4');
    assertCannotAddTechnologyToKeywordTestCase(keywordTestcaseViewPage);

    cy.log('Step 5');
    verifyCopyDataSetAndNoDuplicateRemoveOrAddParameters(
      testcaseWorkspace,
      keywordTestcaseViewPage,
    );
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withCufs([new CustomFieldBuilder('Date', InputType.DATE_PICKER)])
    .withMilestones([new MilestoneBuilder('Jalon', '2025-10-31 14:44:45').withUserLogin('RonW')])
    .withProjects([
      new ProjectBuilder('Project A')
        .withMilestonesOnProject([new MilestoneOnProjectBuilder('Jalon')])
        .withTestCaseLibraryNodes([
          new KeywordTestCaseBuilder('TestA')
            .withParameters([new ParameterBuilder('Param')])
            .withDataSets([
              new DataSetBuilder('Jeton').withParamValues([
                new DataSetParameterValueBuilder('BT', 'X4Mantis'),
              ]),
            ]),
          new KeywordTestCaseBuilder('TestB').withMilestones(['Jalon']),
        ])
        .withCufsOnProject([new CustomFieldOnProjectBuilder('Date', BindableEntity.TEST_CASE)]),
      new ProjectBuilder('Project B').withTestCaseLibraryNodes([
        new KeywordTestCaseBuilder('TestC'),
      ]),
    ])
    .build();
}

function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
  addClearanceToUser(2, SystemProfile.PROJECT_MANAGER, [2]);
}

function verifyCopyDataSetAndNoDuplicateRemoveOrAddParameters(
  testcaseWorkspace: TestCaseWorkspacePage,
  keywordTestcaseViewPage: TestCaseViewPage,
) {
  const testCaseViewParameters = keywordTestcaseViewPage.clickParametersAnchorLink();
  selectNodeInTestCaseWorkspace(testcaseWorkspace, 'Project', 'TestA');
  copyDatasetIntoAnotherKeywordTestCase(keywordTestcaseViewPage, 'Jeton', 'Project B', 'TestC');
  assertCannotDuplicateDataSet(testCaseViewParameters, 'Jeton', 'Jeton-Copie1');
  testCaseViewParameters.assertDuplicateDataSetIsNotClickable('Jeton');
  keywordTestcaseViewPage.assertIconNotExists('.sqtm-core-delete-icon');
  testCaseViewParameters.addDataSetNotExist();
}
