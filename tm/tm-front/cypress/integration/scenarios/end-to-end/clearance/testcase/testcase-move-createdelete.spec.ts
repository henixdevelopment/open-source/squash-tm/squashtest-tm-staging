import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { CustomFieldBuilder } from '../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import {
  ExploratoryTestCaseBuilder,
  KeywordTestCaseBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
  TestCaseFolderBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { ConfirmInterProjectMove } from '../../../../page-objects/elements/dialog/confirm-inter-project-move';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  assertCanDragAndDropFolderAndTestCases,
  dragAnDropFolderInOtherProject,
  dragAnDropTestCasesInOtherProject,
} from '../../../scenario-parts/testcase.part';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check if a user can move test case and folder in same project and other project', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07021.CT03 - testcase-move-createdelete', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testCaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const testCaseWorkspaceTree = testCaseWorkspace.tree;
    const dialogConfirmProjectMove = new ConfirmInterProjectMove('confirm-inter-project-move');

    assertCanDragAndDropFolderAndTestCases(
      testCaseWorkspaceTree,
      ['Amande', 'Caramel', 'Vanille', 'Fraise', 'Eau'],
      'Eau',
      'Athena',
      'Chocolat',
    );

    cy.log('Step 2');
    dragAnDropFolderInOtherProject(
      testCaseWorkspaceTree,
      dialogConfirmProjectMove,
      'Chocolat',
      'Sun',
    );

    cy.log('Step 3');
    dragAnDropTestCasesInOtherProject(
      testCaseWorkspaceTree,
      dialogConfirmProjectMove,
      ['Caramel', 'Vanille', 'Fraise', 'Eau'],
      'Eau',
      'Athena',
    );
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('test case mover', [
        ProfileBuilderPermission.TEST_CASE_READ,
        ProfileBuilderPermission.TEST_CASE_CREATE,
        ProfileBuilderPermission.TEST_CASE_DELETE,
      ]),
    ])
    .withCufs([new CustomFieldBuilder('TestCuf', InputType.CHECKBOX)])
    .withProjects([
      new ProjectBuilder('Athena').withTestCaseLibraryNodes([
        new TestCaseFolderBuilder('Amande').withTestCaseLibraryNodes([
          new TestCaseBuilder('Classique'),
          new ScriptedTestCaseBuilder('Gherkin'),
          new KeywordTestCaseBuilder('Keyword'),
          new ExploratoryTestCaseBuilder('Exploratory'),
        ]),
        new TestCaseFolderBuilder('Chocolat'),
        new TestCaseBuilder('Vanille'),
        new ScriptedTestCaseBuilder('Caramel'),
        new KeywordTestCaseBuilder('Fraise'),
        new ExploratoryTestCaseBuilder('Eau'),
      ]),
      new ProjectBuilder('Sun'),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1, 2]);
}
