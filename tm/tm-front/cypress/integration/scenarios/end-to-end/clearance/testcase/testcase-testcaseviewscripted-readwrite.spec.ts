import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { ScriptedTestCaseBuilder } from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import {
  CampaignBuilder,
  CampaignTestPlanItemBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
  TestSuiteBuilder,
  ScriptedExecutionBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { TestCaseViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { CheckBoxElement } from '../../../../page-objects/elements/forms/check-box.element';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  MilestoneBuilder,
  MilestoneOnProjectBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/milestone-prerequisite-builders';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check if a user can read modify and interact with a scripted testcase', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07020.CT04 - testcase-testcaseviewscripted-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testCaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const scriptedTestCaseViewPage = new TestCaseViewPage(1);

    scripTestCaseChangeHeader(testCaseWorkspace, scriptedTestCaseViewPage);

    cy.log('Step 2');
    scriptedTestCaseChangeInformationPanel(scriptedTestCaseViewPage);

    cy.log('Step 3');
    scriptedTestCaseMilestone(scriptedTestCaseViewPage);

    cy.log('Step 4');
    scriptedTestCaseInsertReference(scriptedTestCaseViewPage);

    cy.log('Step 5');
    scriptedTestCaseInsertScript(scriptedTestCaseViewPage);

    cy.log('Step 6');
    scriptedTestCaseVerifyExecution(scriptedTestCaseViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('test case modifier', [
        ProfileBuilderPermission.TEST_CASE_READ,
        ProfileBuilderPermission.TEST_CASE_WRITE,
      ]),
    ])
    .withMilestones([new MilestoneBuilder('Eau', '2025-10-31 14:44:45').withUserLogin('RonW')])
    .withCufs([new CustomFieldBuilder('Coco', InputType.CHECKBOX)])
    .withProjects([
      new ProjectBuilder('AZERTY')
        .withTestCaseLibraryNodes([new ScriptedTestCaseBuilder('hi')])
        .withMilestonesOnProject([new MilestoneOnProjectBuilder('Eau')])
        .withCampaignLibraryNodes([
          new CampaignBuilder('e')
            .withTestPlanItems([new CampaignTestPlanItemBuilder('hi').withUserLogin('RonW')])
            .withIterations([
              new IterationBuilder('a')
                .withTestPlanItems([
                  new IterationTestPlanItemBuilder('hi')
                    .withUserLogin('RonW')
                    .withExecutions([new ScriptedExecutionBuilder()]),
                ])
                .withTestSuites([new TestSuiteBuilder('x').withTestCaseNames(['hi'])]),
            ]),
        ])
        .withCufsOnProject([new CustomFieldOnProjectBuilder('Coco', BindableEntity.TEST_CASE)]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}
function scripTestCaseChangeHeader(
  testCaseWorkspace: TestCaseWorkspacePage,
  scriptedTestCaseViewPage: TestCaseViewPage,
) {
  testCaseWorkspace.tree.findRowId('NAME', 'AZERTY').then((idProject) => {
    testCaseWorkspace.tree.selectNode(idProject);
    testCaseWorkspace.tree.openNode(idProject);
  });
  testCaseWorkspace.tree.findRowId('NAME', 'hi').then((idTestCase) => {
    testCaseWorkspace.tree.selectNode(idTestCase);
  });
  scriptedTestCaseViewPage.informationPanel.rename('Volcan');
  scriptedTestCaseViewPage.informationPanel.nameTextField.assertContainsText('Volcan');
  scriptedTestCaseViewPage.statusCapsule.selectOption(' À mettre à jour ');
}

function scriptedTestCaseChangeInformationPanel(scriptedTestCaseViewPage: TestCaseViewPage) {
  scriptedTestCaseViewPage.informationPanel.importanceSelectField.selectValueNoButton('Moyenne');
  scriptedTestCaseViewPage.informationPanel.importanceSelectField.assertContainsText('Moyenne');
  const checkboxCuf = scriptedTestCaseViewPage.getCustomField(
    'Coco',
    InputType.CHECKBOX,
  ) as CheckBoxElement;
  checkboxCuf.click();
  scriptedTestCaseViewPage.informationPanel.descriptionRichField.setAndConfirmValue('noir');
  scriptedTestCaseViewPage.informationPanel.descriptionRichField.assertContainsText('noir');
}

function scriptedTestCaseMilestone(scriptedTestCaseViewPage: TestCaseViewPage) {
  scriptedTestCaseViewPage.informationPanel.milestonesField
    .openMilestoneDialog()
    .selectMilestone('Eau');
  scriptedTestCaseViewPage.informationPanel.milestonesField.confirm();
  scriptedTestCaseViewPage.informationPanel.milestonesField.assertContainsText('Eau');
  scriptedTestCaseViewPage.informationPanel.milestonesField.clickOnRemoveMilestoneClose('Eau');
  scriptedTestCaseViewPage.informationPanel.milestonesField.rootElement.should(
    'not.contain',
    'Eau',
  );
}

function scriptedTestCaseInsertReference(scriptedTestCaseViewPage: TestCaseViewPage) {
  scriptedTestCaseViewPage.automationPanel.insertReferenceOfTestAuto('Hola');
}

function scriptedTestCaseInsertScript(scriptedTestCaseViewPage: TestCaseViewPage) {
  const scriptPage = scriptedTestCaseViewPage.clickScriptAnchorLink();
  scriptPage.insertScript('bleu');
}

function scriptedTestCaseVerifyExecution(scriptedTestCaseViewPage: TestCaseViewPage) {
  const versionExecution = scriptedTestCaseViewPage.showExecutions('');
  versionExecution.grid.findRowId('executionPath', 'AZERTY > e > a > x').then((idExecution) => {
    versionExecution.grid
      .getCell(idExecution, 'executionOrder', 'mainViewport')
      .findCell()
      .should('contain', '#1');
  });
}
