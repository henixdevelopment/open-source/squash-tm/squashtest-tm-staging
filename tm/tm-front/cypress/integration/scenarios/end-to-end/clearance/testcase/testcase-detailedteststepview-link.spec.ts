import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { RequirementBuilder } from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  ActionTestStepBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { DetailedStepViewPage } from '../../../../page-objects/pages/test-case-workspace/detailed-step-view/detailed-step-view.page';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  linkRequirementBySearch,
  linkRequirementToATestStepInDetailStepViewPage,
  unlinkRequirementToATestStepInDetailStepViewPage,
} from '../../../scenario-parts/testcase.part';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can associate and dissociate a requirement to a test step', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07022.CT03 - testcase-detailedteststepview-link', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const detailedStepViewPage = DetailedStepViewPage.navigateToDetailedStepViewPage('1', 1);

    linkRequirementToATestStepInDetailStepViewPage(detailedStepViewPage, 'France', 'Portugal');

    cy.log('Step 2');
    unlinkRequirementToATestStepInDetailStepViewPage(detailedStepViewPage);

    cy.log('Step 3');
    linkRequirementBySearch(detailedStepViewPage, 'Portugal');
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('test case linker', [
        ProfileBuilderPermission.TEST_CASE_READ,
        ProfileBuilderPermission.TEST_CASE_LINK,
        ProfileBuilderPermission.REQUIREMENT_READ,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('France')
        .withRequirementLibraryNodes([new RequirementBuilder('Portugal')])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Espagne').withSteps([
            new ActionTestStepBuilder('Belgique', 'Luxembourg'),
          ]),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}
