import { DatabaseUtils } from '../../../../../utils/database.utils';
import { TestCaseWorkspacePage } from '../../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { ConfirmInterProjectMove } from '../../../../../page-objects/elements/dialog/confirm-inter-project-move';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { CustomFieldBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  ExploratoryTestCaseBuilder,
  KeywordTestCaseBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
  TestCaseFolderBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { TreeElement } from '../../../../../page-objects/elements/grid/grid.element';

describe('Check if Guest cannot move test case and folder in same project and other project', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07221.CT03 - testcase-move-createdelete-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testCaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const testCaseWorkspaceTree = testCaseWorkspace.tree;
    const dialogConfirmProjectMove = new ConfirmInterProjectMove('confirm-inter-project-move');
    assertCannotDragAndDropFolderAndTestCaseInSameProject(testCaseWorkspaceTree);
    cy.clickVoid();

    cy.log('Step 2');
    assertCannotDragAnDropFolderInOtherProject(testCaseWorkspaceTree, dialogConfirmProjectMove);

    cy.log('Step 3');
    assertCannotDragAnDropTestCasesInOtherProject(testCaseWorkspaceTree, dialogConfirmProjectMove);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withCufs([new CustomFieldBuilder('TestCuf', InputType.CHECKBOX)])
    .withProjects([
      new ProjectBuilder('ProjectA').withTestCaseLibraryNodes([
        new TestCaseFolderBuilder('Dossier1').withTestCaseLibraryNodes([
          new TestCaseBuilder('Classique'),
          new ScriptedTestCaseBuilder('Gherkin'),
          new KeywordTestCaseBuilder('Keyword'),
          new ExploratoryTestCaseBuilder('Exploratory'),
        ]),
        new TestCaseFolderBuilder('Dossier2'),
        new TestCaseBuilder('Pistache'),
        new ScriptedTestCaseBuilder('Amande'),
        new KeywordTestCaseBuilder('Noisette'),
        new ExploratoryTestCaseBuilder('Cajou'),
      ]),
      new ProjectBuilder('ProjectB'),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1, 2]);
}
function assertCannotDragAndDropFolderAndTestCaseInSameProject(testCaseWorkspaceTree: TreeElement) {
  testCaseWorkspaceTree.findRowId('NAME', 'ProjectA').then((idProject) => {
    testCaseWorkspaceTree.openNode(idProject);
  });
  testCaseWorkspaceTree.selectNodesByName(['Dossier1', 'Pistache', 'Amande', 'Noisette', 'Cajou']);
  testCaseWorkspaceTree.findRowId('NAME', 'Dossier2').then((idDropFolder) => {
    testCaseWorkspaceTree.beginDragAndDrop(idDropFolder);
  });
  testCaseWorkspaceTree.assertRowsHaveParentByNames(
    ['Dossier1', 'Pistache', 'Amande', 'Noisette', 'Cajou'],
    'ProjectA',
  );
}

function assertCannotDragAnDropFolderInOtherProject(
  testCaseWorkspaceTree: TreeElement,
  dialogConfirmProjectMove: ConfirmInterProjectMove,
) {
  testCaseWorkspaceTree.findRowId('NAME', 'Dossier2').then((idDropFolder) => {
    testCaseWorkspaceTree.beginDragAndDrop(idDropFolder);
    testCaseWorkspaceTree.findRowId('NAME', 'ProjectB').then((idSecondProject) => {
      testCaseWorkspaceTree.selectNode(idSecondProject);
      testCaseWorkspaceTree.dropIntoOtherProject(idSecondProject);
      dialogConfirmProjectMove.assertNotExist();
      testCaseWorkspaceTree.assertRowsHaveParentByNames(['Dossier2'], 'ProjectA');
    });
  });
}

function assertCannotDragAnDropTestCasesInOtherProject(
  testCaseWorkspaceTree: TreeElement,
  dialogConfirmProjectMove: ConfirmInterProjectMove,
) {
  testCaseWorkspaceTree.selectNodesByName(['Pistache', 'Amande', 'Noisette', 'Cajou']);
  testCaseWorkspaceTree.findRowId('NAME', 'Cajou').then((idExploratoryTestCase) => {
    testCaseWorkspaceTree.beginDragAndDrop(idExploratoryTestCase);
  });
  testCaseWorkspaceTree.findRowId('NAME', 'ProjectA').then((idProject) => {
    testCaseWorkspaceTree.selectNode(idProject);
    testCaseWorkspaceTree.dropIntoOtherProject(idProject);
    dialogConfirmProjectMove.assertNotExist();
  });
  testCaseWorkspaceTree.assertRowsHaveParentByNames(
    ['Pistache', 'Amande', 'Noisette', 'Cajou'],
    'ProjectA',
  );
}
