import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import {
  ActionTestStepBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { TestCaseViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { TestStepsViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-steps-view.page';
import { EditableTextFieldElement } from '../../../../page-objects/elements/forms/editable-text-field.element';
import { TestCaseViewCalledTestCasesPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-called-test-cases.page';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can interact and modify a test step in a test case', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07020.CT06 - testcase-testcaseviewstandard-steps-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testCaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const testCaseViewPage = new TestCaseViewPage(2);
    const stepsViewPage = new TestStepsViewPage();
    const TC001ViewPage = new TestCaseViewPage(1);
    const calledTestCaseViewPage = new TestCaseViewCalledTestCasesPage(TC001ViewPage);

    fillPrerequisiteInStepAnchor(testCaseWorkspace, testCaseViewPage, stepsViewPage);

    cy.log('Step 2');
    fillCufInTestStep(testCaseViewPage);

    cy.log('Step 3');
    copyTestStep(stepsViewPage);

    cy.log('Step 4');
    dropTestCaseInStepViewPage(stepsViewPage);

    cy.log('Step 5');
    assertThatTestIsInAnchorCalledTestCaseOfSecondTestCase(
      testCaseWorkspace,
      TC001ViewPage,
      calledTestCaseViewPage,
    );
  });

  function initTestData() {
    return new PrerequisiteBuilder()
      .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
      .withProfiles([
        new ProfileBuilder('test case modifier', [
          ProfileBuilderPermission.TEST_CASE_READ,
          ProfileBuilderPermission.TEST_CASE_WRITE,
        ]),
      ])
      .withCufs([new CustomFieldBuilder('TestCuf', InputType.PLAIN_TEXT)])
      .withProjects([
        new ProjectBuilder('Athena')
          .withCufsOnProject([new CustomFieldOnProjectBuilder('TestCuf', BindableEntity.TEST_STEP)])
          .withTestCaseLibraryNodes([
            new TestCaseBuilder('TC 001').withSteps([
              new ActionTestStepBuilder('Je crie Expecto Patronum.', 'Mon patronus apparaît.'),
            ]),
            new TestCaseBuilder('Fanta').withSteps([new ActionTestStepBuilder('ahah', 'hihihi')]),
          ]),
      ])
      .build();
  }
  function initPermissions() {
    addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
  }
  function fillPrerequisiteInStepAnchor(
    testCaseWorkspace: TestCaseWorkspacePage,
    testCaseViewPage: TestCaseViewPage,
    stepsViewPage: TestStepsViewPage,
  ) {
    testCaseWorkspace.tree.findRowId('NAME', 'Athena').then((idProject) => {
      testCaseWorkspace.tree.selectNode(idProject);
      testCaseWorkspace.tree.openNode(idProject);
    });
    testCaseWorkspace.tree.findRowId('NAME', 'Fanta').then((idTestCase) => {
      testCaseWorkspace.tree.selectNode(idTestCase);
    });
    testCaseViewPage.clickStepsAnchorLink();
    stepsViewPage.assertExists();
    stepsViewPage.expendAll();
    stepsViewPage.fillPrerequisiteFieldAndConfirm('Hola que tal');
  }

  function fillCufInTestStep(testCaseViewPage: TestCaseViewPage) {
    const cufText = testCaseViewPage.getCustomField(
      'TestCuf',
      InputType.PLAIN_TEXT,
    ) as EditableTextFieldElement;
    cufText.setValue('Cacao');
    cufText.clickOnConfirmButton();
    cufText.assertContainsText('Cacao');
  }

  function copyTestStep(stepsViewPage: TestStepsViewPage) {
    stepsViewPage.copyStepWithTopCopyIcon(0, 2);
  }

  function dropTestCaseInStepViewPage(stepsViewPage: TestStepsViewPage) {
    const testCaseTree = stepsViewPage.openTestcaseDrawer(0);
    testCaseTree.findRowId('NAME', 'Athena').then((idProject) => {
      testCaseTree.openNode(idProject);
      testCaseTree.findRowId('NAME', 'TC 001').then((idTestCase) => {
        testCaseTree.beginDragAndDrop(idTestCase);
      });
    });
    stepsViewPage.dropCalledTestCaseE2E(2);
    stepsViewPage.closeDrawer(1);
    stepsViewPage.getActionStepByIndex(2).checkCalledTestCaseStep('TC 001', '4', 2);
  }

  function assertThatTestIsInAnchorCalledTestCaseOfSecondTestCase(
    testCaseWorkspace: TestCaseWorkspacePage,
    TC001ViewPage: TestCaseViewPage,
    calledTestCaseViewPage: TestCaseViewCalledTestCasesPage,
  ) {
    testCaseWorkspace.tree.findRowId('NAME', 'TC 001').then((idTestCase) => {
      testCaseWorkspace.tree.selectNode(idTestCase);
    });
    TC001ViewPage.clickCalledTestCasesAnchorLink();
    calledTestCaseViewPage.assertExists();
    calledTestCaseViewPage.callingTestTable.assertRowCount(1);
    calledTestCaseViewPage.callingTestTable
      .findRowId('projectName', 'Athena')
      .then((idTestCase) => {
        calledTestCaseViewPage.callingTestTable
          .getCell(idTestCase, 'name', 'mainViewport')
          .findCell()
          .should('contain', 'Fanta');
      });
  }
});
