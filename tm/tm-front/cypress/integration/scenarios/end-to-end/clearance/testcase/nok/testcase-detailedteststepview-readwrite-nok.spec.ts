import { DatabaseUtils } from '../../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { BindableEntity } from '../../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import {
  ActionTestStepBuilder,
  TestCaseBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { TestCaseWorkspacePage } from '../../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { selectNodeInTestCaseWorkspace } from '../../../../scenario-parts/testcase.part';
import { TestCaseViewPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { TestStepsViewPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-steps-view.page';
import { CheckBoxElement } from '../../../../../page-objects/elements/forms/check-box.element';
import { DetailedStepViewPage } from '../../../../../page-objects/pages/test-case-workspace/detailed-step-view/detailed-step-view.page';

describe('Check if a user with Guest cannot perform the following actions on the Test Step Details page: modify a test step, modify a custom checkbox field', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07220.CT07 - testcase-detailedteststepview-readwrite-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testCaseWorkSpace = TestCaseWorkspacePage.initTestAtPage();
    const testCaseViewPage = new TestCaseViewPage(1);
    const stepsViewPage = new TestStepsViewPage();
    const detailedStepViewPage = new DetailedStepViewPage();
    assertCanAccessTestStepDetailsPage(
      testCaseWorkSpace,
      testCaseViewPage,
      stepsViewPage,
      detailedStepViewPage,
    );

    cy.log('Step 2');
    assertCannotModifyFieldsOnTestStepDetailsPage(detailedStepViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withCufs([new CustomFieldBuilder('Manuel', InputType.CHECKBOX)])
    .withProjects([
      new ProjectBuilder('Project')
        .withCufsOnProject([new CustomFieldOnProjectBuilder('Manuel', BindableEntity.TEST_STEP)])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Test').withSteps([new ActionTestStepBuilder('Action', 'Result')]),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}

function assertCanAccessTestStepDetailsPage(
  testCaseWorkSpace: TestCaseWorkspacePage,
  testAViewPage: TestCaseViewPage,
  stepsViewPage: TestStepsViewPage,
  detailedStepViewPage: DetailedStepViewPage,
) {
  const actionStepByIndex = stepsViewPage.getActionStepByIndex(0);
  selectNodeInTestCaseWorkspace(testCaseWorkSpace, 'Project', 'Test');
  testAViewPage.clickStepsAnchorLink();
  stepsViewPage.assertExists();
  actionStepByIndex.clickActionStepActionButtonAndSelectOptionInMenu('navigate-to-details');
  detailedStepViewPage.assertExists();
}
function assertCannotModifyFieldsOnTestStepDetailsPage(detailedStepViewPage: DetailedStepViewPage) {
  detailedStepViewPage.assertCannotEditTestStepAction();
  detailedStepViewPage.assertCannotEditTestStepResult();
  const checkboxCuf = detailedStepViewPage.getCustomField(
    'Manuel',
    InputType.CHECKBOX,
  ) as CheckBoxElement;
  checkboxCuf.click();
  checkboxCuf.findAndCheckState('span', false);
}
