import { DatabaseUtils } from '../../../../../utils/database.utils';
import { TestCaseWorkspacePage } from '../../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { TestCaseViewPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { KeywordTestStepsViewPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/keyword-test-step-view-page';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  KeywordStepBuilder,
  KeywordTestCaseBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { selectNodeInTestCaseWorkspace } from '../../../../scenario-parts/testcase.part';
import { KeywordStepElement } from '../../../../../page-objects/elements/test-steps/keyword-step.element';

describe('Check if a user with Guest cannot perform the following actions in BDD test case:Add, copy/paste, and delete a test step. Add a comment, a docstring, and a data table to an action.', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07220.CT08 - testcase-testcaseviewkeyword-steps-readwrite-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testcaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const keywordTestCaseViewPage = new TestCaseViewPage(1);
    const keywordStepViewPage = new KeywordTestStepsViewPage();
    const keywordStepElement = keywordStepViewPage.getStepByIndex(0);

    assertCannotAddTestKeywordStep(testcaseWorkspace, keywordTestCaseViewPage, keywordStepViewPage);
    cy.log('Step 2');
    keywordStepElement.assertCanCopyTestStepButPasteButtonDoesNotExist();

    cy.log('Step 3');
    assertCannotAddDataTableCommentOrDocstring(testcaseWorkspace, keywordStepElement);

    cy.log('Step 4');
    keywordStepElement.checkDeleteButtonNotVisible();
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProjects([
      new ProjectBuilder('Project').withTestCaseLibraryNodes([
        new KeywordTestCaseBuilder('TestA').withKeywordSteps([
          new KeywordStepBuilder('WHEN', 'I have 5 apples'),
        ]),
        new KeywordTestCaseBuilder('TestB').withKeywordSteps([
          new KeywordStepBuilder('WHEN', 'I have 10 apples'),
        ]),
      ]),
    ])
    .build();
}

function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}
function assertCannotAddTestKeywordStep(
  testcaseWorkspace: TestCaseWorkspacePage,
  keywordTestCaseViewPage: TestCaseViewPage,
  keywordStepViewPage: KeywordTestStepsViewPage,
) {
  selectNodeInTestCaseWorkspace(testcaseWorkspace, 'Project', 'TestA');
  keywordTestCaseViewPage.clickKeywordStepsAnchorLink();
  keywordStepViewPage.checkKeywordSelectFieldNotVisible();
  keywordStepViewPage.checkActionTextFieldNotVisible();
}

function assertCannotAddDataTableCommentOrDocstring(
  testcaseWorkspace: TestCaseWorkspacePage,
  keywordStepElement: KeywordStepElement,
) {
  keywordStepElement.assertCreateDatatableMenuItemIsDisabled();
  keywordStepElement.assertCreateCommentMenuItemIsDisabled();
  selectNodeInTestCaseWorkspace(testcaseWorkspace, 'Project', 'TestB');
  keywordStepElement.assertCreateDocstringMenuItemIsDisabled();
}
