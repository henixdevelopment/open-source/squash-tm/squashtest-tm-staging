import { DatabaseUtils } from '../../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { RequirementBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  ActionTestStepBuilder,
  TestCaseBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { TestCasesStepsLinkToRequirementVersionBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/link-builders';
import { DetailedStepViewPage } from '../../../../../page-objects/pages/test-case-workspace/detailed-step-view/detailed-step-view.page';
import { RequirementSearchPage } from '../../../../../page-objects/pages/requirement-workspace/search/requirement-search-page';
import { GridElement, TreeElement } from '../../../../../page-objects/elements/grid/grid.element';

describe('Check if a user with Guest cannot associate and dissociate a requirement to a test step', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07222.CT03 - testcase-detailedteststepview-link-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const detailedStepViewPage = DetailedStepViewPage.navigateToDetailedStepViewPage('1', 1);
    const requirementSearchPage = new RequirementSearchPage(new GridElement('requirement-search'));
    const requirementTree = TreeElement.createTreeElement('requirement-tree-picker');

    detailedStepViewPage.assertAndClickIfAddRequirementButtonDisabled();
    requirementTree.assertNotExists();

    cy.log('Step 2');
    detailedStepViewPage.assertDeleteButtonNotVisibleInCoverageTableTestStep(1);

    cy.log('Step 3');
    detailedStepViewPage.assertAndClickIfSearchButtonDisabled();
    requirementSearchPage.assertNotExist();
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProjects([
      new ProjectBuilder('Project')
        .withRequirementLibraryNodes([new RequirementBuilder('Requirement')])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Test').withSteps([new ActionTestStepBuilder('Action', 'Result')]),
        ])
        .withLinks([new TestCasesStepsLinkToRequirementVersionBuilder('Test', ['Requirement'])]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}
