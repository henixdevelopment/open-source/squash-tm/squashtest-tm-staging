import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { KeywordTestCaseBuilder } from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { KeywordTestStepsViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/keyword-test-step-view-page';
import { TestCaseViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can interact and modify a keyword-test-case step', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07020.CT08 - testcase-testcaseviewkeyword-steps-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage();
    const keywordTestCaseViewPage = new TestCaseViewPage(1);
    const keywordStepViewPage = new KeywordTestStepsViewPage();

    addKeywordTestStep(testCaseWorkspacePage, keywordTestCaseViewPage, keywordStepViewPage);

    cy.log('Step 2');
    copyKeywordStep(keywordStepViewPage);

    cy.log('Step 3');
    addTableDocstringAndComment(keywordStepViewPage);

    cy.log('Step 4');
    deleteKeywordStep(keywordStepViewPage);
  });

  function initTestData() {
    return new PrerequisiteBuilder()
      .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
      .withProfiles([
        new ProfileBuilder('test case modifier', [
          ProfileBuilderPermission.TEST_CASE_READ,
          ProfileBuilderPermission.TEST_CASE_WRITE,
          ProfileBuilderPermission.ACTION_WORD_READ,
          ProfileBuilderPermission.ACTION_WORD_CREATE,
        ]),
      ])
      .withProjects([
        new ProjectBuilder('Athena').withTestCaseLibraryNodes([
          new KeywordTestCaseBuilder('Bonbon'),
        ]),
      ])
      .build();
  }
  function initPermissions() {
    addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
  }
  function addKeywordTestStep(
    testCaseWorkspacePage: TestCaseWorkspacePage,
    keywordTestCaseViewPage: TestCaseViewPage,
    keywordStepViewPage: KeywordTestStepsViewPage,
  ) {
    testCaseWorkspacePage.tree.findRowId('NAME', 'Athena').then((idProject) => {
      testCaseWorkspacePage.tree.selectNode(idProject);
      testCaseWorkspacePage.tree.openNode(idProject);
    });
    testCaseWorkspacePage.tree.findRowId('NAME', 'Bonbon').then((idTestCase) => {
      testCaseWorkspacePage.tree.selectNode(idTestCase);
    });
    keywordTestCaseViewPage.clickKeywordStepsAnchorLink();
    keywordStepViewPage.chooseKeyword('When');
    keywordStepViewPage.actionAutocompleteElement.fill('Hello');
    keywordStepViewPage.addKeywordStep(false);
    keywordStepViewPage.getStepByIndex(0).getActionField().assertContainsText('Hello');
    keywordStepViewPage.getStepByIndex(0).getKeywordField().assertContainsText('When');
  }
});

function copyKeywordStep(keywordStepViewPage: KeywordTestStepsViewPage) {
  keywordStepViewPage.getStepByIndex(0).copyKeywordStepWithIconInStep();
  keywordStepViewPage.getStepByIndex(0).assertExists();
  keywordStepViewPage.getStepByIndex(1).assertExists();
}

function addTableDocstringAndComment(keywordStepViewPage: KeywordTestStepsViewPage) {
  keywordStepViewPage.getStepByIndex(0).selectMoreOption('create-datatable');
  keywordStepViewPage.dataTableField.confirm();
  keywordStepViewPage.dataTableField.assertExists();
  keywordStepViewPage.dataTableField.assertContainsText(
    '| produit | prix |\n' + '| Expresso | 0.40 |',
  );
  keywordStepViewPage.getStepByIndex(0).selectMoreOption('create-comment');
  keywordStepViewPage.commentField.assertExists();
  keywordStepViewPage.commentField.setAndConfirm('dada');
  keywordStepViewPage.commentField.assertContainsText('dada');
  keywordStepViewPage.getStepByIndex(1).selectMoreOption('create-docstring');
  keywordStepViewPage.docStringField.assertExists();
  keywordStepViewPage.docStringField.setAndConfirm('dodo');
  keywordStepViewPage.docStringField.assertContainsText('dodo');
}

function deleteKeywordStep(keywordStepViewPage: KeywordTestStepsViewPage) {
  keywordStepViewPage.getStepByIndex(1).deleteStep();
  keywordStepViewPage.getStepByIndex(0).assertExists();
  keywordStepViewPage.getStepByIndex(1).assertNotExist();
}
