import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import {
  DataSetBuilder,
  DataSetParameterValueBuilder,
  KeywordTestCaseBuilder,
  ParameterBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { TestCaseViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { EditableDateFieldElement } from '../../../../page-objects/elements/forms/editable-date-field.element';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  MilestoneBuilder,
  MilestoneOnProjectBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/milestone-prerequisite-builders';
import {
  addTechnologyToKeywordTestCase,
  associateAndDissociateMilestoneOnTestCase,
  copyDatasetIntoAnotherKeywordTestCase,
  createNewParameter,
  renameChangeTypeTestAndModifyInformationBloc,
  selectNodeInTestCaseWorkspace,
} from '../../../scenario-parts/testcase.part';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can modify on a Keyword testcase', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07020.CT03 - testcase-testcaseviewkeyword-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testcaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const keywordTestcaseViewPage = new TestCaseViewPage(2);

    modifyHeaderNameAndStatusInKeywordTestCase(testcaseWorkspace, keywordTestcaseViewPage);

    cy.log('Step 2');
    modifyDateCufInInformationBlock(keywordTestcaseViewPage);

    cy.log('Step 3');
    associateAndDissociateMilestoneOnTestCase(keywordTestcaseViewPage, 'Nuit');

    cy.log('Step 4');
    addTechnologyToKeywordTestCase(keywordTestcaseViewPage, 'Robot Framework');

    cy.log('Step 5');
    copyDatasetIntoAnotherKeywordTestCase(keywordTestcaseViewPage, 'Smite', 'AZERTY', 'Salut');

    cy.log('Step 6');
    createNewParameterAndAssertExist(keywordTestcaseViewPage);
  });
  function initTestData() {
    return new PrerequisiteBuilder()
      .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
      .withProfiles([
        new ProfileBuilder('test case modifier', [
          ProfileBuilderPermission.TEST_CASE_READ,
          ProfileBuilderPermission.TEST_CASE_WRITE,
        ]),
      ])
      .withCufs([new CustomFieldBuilder('Coco', InputType.DATE_PICKER)])
      .withMilestones([new MilestoneBuilder('Nuit', '2019-10-31 14:44:45').withUserLogin('RonW')])
      .withProjects([
        new ProjectBuilder('AZERTY')
          .withMilestonesOnProject([new MilestoneOnProjectBuilder('Nuit')])
          .withTestCaseLibraryNodes([
            new KeywordTestCaseBuilder('Salut'),
            new KeywordTestCaseBuilder('Pomme')
              .withParameters([new ParameterBuilder('Coca')])
              .withDataSets([
                new DataSetBuilder('Smite').withParamValues([
                  new DataSetParameterValueBuilder('Coca', 'Water'),
                ]),
              ]),
          ])
          .withCufsOnProject([new CustomFieldOnProjectBuilder('Coco', BindableEntity.TEST_CASE)]),
      ])
      .build();
  }
});
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}
function modifyHeaderNameAndStatusInKeywordTestCase(
  testcaseWorkspace: TestCaseWorkspacePage,
  keywordTestcaseViewPage: TestCaseViewPage,
) {
  selectNodeInTestCaseWorkspace(testcaseWorkspace, 'AZERTY', 'Pomme');
  renameChangeTypeTestAndModifyInformationBloc(
    keywordTestcaseViewPage,
    'Poire',
    'Reference',
    'Métier',
    'Non-régression',
    'Moyenne',
  );
  keywordTestcaseViewPage.informationPanel.nameTextField.assertContainsText('Poire');
  keywordTestcaseViewPage.statusCapsule.selectOption('Approuvé');
}

function modifyDateCufInInformationBlock(keywordTestcaseViewPage: TestCaseViewPage) {
  const dateCuf = keywordTestcaseViewPage.getCustomField(
    'Coco',
    InputType.DATE_PICKER,
  ) as EditableDateFieldElement;
  keywordTestcaseViewPage.informationPanel.natureSelectField.assertContainsText('Métier');
  keywordTestcaseViewPage.addCufDate('Coco', '10/10/2090');
  dateCuf.click();
  dateCuf.setToTodayAndConfirm();
}

function createNewParameterAndAssertExist(keywordTestcaseViewPage: TestCaseViewPage) {
  createNewParameter(keywordTestcaseViewPage, 'Tournesol');
  keywordTestcaseViewPage.clickParametersAnchorLink().checkExistingParam('Tournesol');
}
