import { DatabaseUtils } from '../../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { RequirementBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  ActionTestStepBuilder,
  ExploratoryTestCaseBuilder,
  KeywordTestCaseBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  RequirementVersionsLinkToVerifyingTestCaseBuilder,
  TestCasesStepsLinkToRequirementVersionBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/link-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { TestCaseWorkspacePage } from '../../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {
  assertCannotUnlinkRequirementFromClassicTestCaseStep,
  selectNodeInTestCaseWorkspace,
} from '../../../../scenario-parts/testcase.part';
import { TestCaseViewPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';

describe('Check if a user with Guest cannot perform the following actions on the test case consultation page: associate/dissociate a requirement with a Classic, Gherkin, BDD and Exploratory test case', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07222.CT01 - testcase-testcaseview-link-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage();
    const testCaseViewPage = new TestCaseViewPage(2);
    assertCannotLinkRequirementToScriptedTestCase(testCaseWorkspacePage, testCaseViewPage);

    cy.log('Step 2');
    assertCannotLinkRequirementToClassicTestCase(testCaseWorkspacePage, testCaseViewPage);

    cy.log('Step 3');
    assertCannotUnlinkRequirementFromClassicTestCaseStep(testCaseViewPage);

    cy.log('Step 4');
    assertCannotUnlinkRequirementFromKeywordTestCase(testCaseWorkspacePage, testCaseViewPage);

    cy.log('Step 5');
    assertCannotUnlinkRequirementFromExploratoryTestCase(testCaseWorkspacePage, testCaseViewPage);
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProjects([
      new ProjectBuilder('Project')
        .withRequirementLibraryNodes([
          new RequirementBuilder('Req'),
          new RequirementBuilder('Exig'),
        ])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Classic').withSteps([new ActionTestStepBuilder('Action', 'Result')]),
          new ScriptedTestCaseBuilder('Gherkin'),
          new KeywordTestCaseBuilder('BDD'),
          new ExploratoryTestCaseBuilder('Exploratory'),
        ])
        .withLinks([
          new TestCasesStepsLinkToRequirementVersionBuilder('Classic', ['Exig']),
          new RequirementVersionsLinkToVerifyingTestCaseBuilder(['Req'], 'BDD'),
          new RequirementVersionsLinkToVerifyingTestCaseBuilder(['Req'], 'Exploratory'),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}

function assertCannotLinkRequirementToScriptedTestCase(
  testCaseWorkspacePage: TestCaseWorkspacePage,
  scriptedTestCaseViewPage: TestCaseViewPage,
) {
  selectNodeInTestCaseWorkspace(testCaseWorkspacePage, 'Project', 'Gherkin');
  const testCaseViewVerifiedRequirementsPage =
    scriptedTestCaseViewPage.clickVerifiedRequirementsAnchorLink();
  testCaseViewVerifiedRequirementsPage.assertExists();
  scriptedTestCaseViewPage.assertIconNotExists('add-coverages');
}

function assertCannotLinkRequirementToClassicTestCase(
  testCaseWorkspacePage: TestCaseWorkspacePage,
  testCaseViewPage: TestCaseViewPage,
) {
  selectNodeInTestCaseWorkspace(testCaseWorkspacePage, 'Project', 'Classic');
  testCaseViewPage.clickVerifiedRequirementsAnchorLink();
  testCaseViewPage.assertIconNotExists('add-coverages');
}

function assertCannotUnlinkRequirementFromKeywordTestCase(
  testCaseWorkspacePage: TestCaseWorkspacePage,
  keywordTestCaseViewPage: TestCaseViewPage,
) {
  selectNodeInTestCaseWorkspace(testCaseWorkspacePage, 'Project', 'BDD');
  const keywordCoverageTable = keywordTestCaseViewPage.coveragesTable;
  keywordTestCaseViewPage.clickVerifiedRequirementsAnchorLink();
  keywordCoverageTable.findRowId('name', 'Req').then((idReq) => {
    keywordCoverageTable.getRow(idReq, 'rightViewport').cell('delete').assertNotExist();
  });
}

function assertCannotUnlinkRequirementFromExploratoryTestCase(
  testCaseWorkspacePage: TestCaseWorkspacePage,
  exploratoryTestCaseViewPage: TestCaseViewPage,
) {
  selectNodeInTestCaseWorkspace(testCaseWorkspacePage, 'Project', 'Exploratory');
  const exploratoryCoverageTable = exploratoryTestCaseViewPage.coveragesTable;
  exploratoryCoverageTable.findRowId('name', 'Req').then((idReq) => {
    exploratoryCoverageTable.selectRow(idReq, '#', 'leftViewport');
  });
  exploratoryTestCaseViewPage.assertIconNotExists('remove-coverages');
}
