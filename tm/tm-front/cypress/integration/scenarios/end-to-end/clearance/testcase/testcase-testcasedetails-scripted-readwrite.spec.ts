import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { ScriptedTestCaseBuilder } from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import {
  CampaignBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { IterationViewPage } from '../../../../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import { TestCaseViewSubPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-sub-page';
import {
  SCMRepositoryBuilder,
  SCMServerBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/server-prerequisite-builders';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import { getToSecondViewPageOfScriptedTestCase } from '../../../scenario-parts/testcase.part';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check if a user can modify a scripted test case in Second Level View Page', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC070201.CT03 - testcase-testcasedetails-scripted-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();
    const iterationViewPage = new IterationViewPage();
    const scriptedTestCaseSecondLevelViewPage = new TestCaseViewSubPage(1);

    getToSecondViewPageOfScriptedTestCase(
      campaignWorkspace,
      iterationViewPage,
      scriptedTestCaseSecondLevelViewPage,
      'AZERTY',
      'Hello',
      'Lola',
    );

    cy.log('Step 2');
    clickOnPrintIcon(scriptedTestCaseSecondLevelViewPage);

    cy.log('Step 3');
    modifyReferenceAndImportanceInHeader(scriptedTestCaseSecondLevelViewPage);

    cy.log('Step 4');
    modifyNatureTagCufAndDescriptionInInformationPanel(scriptedTestCaseSecondLevelViewPage);

    cy.log('Step 5');
    selectScmServerInScriptedTestCase(scriptedTestCaseSecondLevelViewPage);

    cy.log('Step 6');
    insertNewScriptInScriptedTestCase(scriptedTestCaseSecondLevelViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('test case modifier', [
        ProfileBuilderPermission.TEST_CASE_READ,
        ProfileBuilderPermission.TEST_CASE_WRITE,
        ProfileBuilderPermission.TEST_CASE_EXPORT,
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CAMPAIGN_READ_UNASSIGNED,
      ]),
    ])
    .withCufs([new CustomFieldBuilder('a', InputType.TAG)])
    .withServers([
      new SCMServerBuilder('edf', 'http://fake.com').withRepositories([
        new SCMRepositoryBuilder('edf', 'main'),
      ]),
    ])
    .withProjects([
      new ProjectBuilder('AZERTY')
        .withTestCaseLibraryNodes([new ScriptedTestCaseBuilder('hi')])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Hello').withIterations([
            new IterationBuilder('Lola').withTestPlanItems([
              new IterationTestPlanItemBuilder('hi'),
            ]),
          ]),
        ])
        .withCufsOnProject([new CustomFieldOnProjectBuilder('a', BindableEntity.TEST_CASE)]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}

function clickOnPrintIcon(scriptedTestCaseSecondLevelViewPage: TestCaseViewSubPage) {
  scriptedTestCaseSecondLevelViewPage.clickActionMenuAndAssertPrintModeExists();
}

function modifyReferenceAndImportanceInHeader(
  scriptedTestCaseSecondLevelViewPage: TestCaseViewSubPage,
) {
  scriptedTestCaseSecondLevelViewPage.testCaseViewPage.informationPanel.referenceTextField.setAndConfirmValue(
    'Pizza',
  );
  scriptedTestCaseSecondLevelViewPage.testCaseViewPage.informationPanel.referenceTextField.assertContainsText(
    'Pizza',
  );
  scriptedTestCaseSecondLevelViewPage.testCaseViewPage.importanceCapsule.selectOption('Très haute');
  scriptedTestCaseSecondLevelViewPage.testCaseViewPage.importanceCapsule.assertContainsText(
    'Très haute',
  );
}

function modifyNatureTagCufAndDescriptionInInformationPanel(
  scriptedTestCaseSecondLevelViewPage: TestCaseViewSubPage,
) {
  scriptedTestCaseSecondLevelViewPage.testCaseViewPage.informationPanel.natureSelectField.selectValueNoButton(
    'Sécurité',
  );
  scriptedTestCaseSecondLevelViewPage.testCaseViewPage.informationPanel.natureSelectField.assertContainsText(
    'Sécurité',
  );
  scriptedTestCaseSecondLevelViewPage.getTagCustomField('a').typeNewTag('Lll');
  scriptedTestCaseSecondLevelViewPage.getTagCustomField('a').assertContainsText('Lll');
  scriptedTestCaseSecondLevelViewPage.testCaseViewPage.informationPanel.descriptionRichField.setAndConfirmValue(
    'Serein',
  );
  scriptedTestCaseSecondLevelViewPage.testCaseViewPage.informationPanel.descriptionRichField.assertContainsText(
    'Serein',
  );
}

function selectScmServerInScriptedTestCase(
  scriptedTestCaseSecondLevelViewPage: TestCaseViewSubPage,
) {
  scriptedTestCaseSecondLevelViewPage.testCaseViewPage.automationPanel.chooseScm(
    'http://fake.com/edf (main)',
  );
}

function insertNewScriptInScriptedTestCase(
  scriptedTestCaseSecondLevelViewPage: TestCaseViewSubPage,
) {
  const scriptViewPage =
    scriptedTestCaseSecondLevelViewPage.testCaseViewPage.clickScriptAnchorLink();
  scriptViewPage.insertScript('Hehehe');
  scriptViewPage.clearScript();
  scriptViewPage.insertScript('Ddddd');
}
