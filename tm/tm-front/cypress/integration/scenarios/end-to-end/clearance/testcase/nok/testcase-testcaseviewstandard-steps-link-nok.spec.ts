import { DatabaseUtils } from '../../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { RequirementBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  ActionTestStepBuilder,
  TestCaseBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { TestCasesStepsLinkToRequirementVersionBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/link-builders';
import { TestCaseWorkspacePage } from '../../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {
  assertCannotUnlinkRequirementFromClassicTestCaseStep,
  selectNodeInTestCaseWorkspace,
} from '../../../../scenario-parts/testcase.part';
import { TestCaseViewPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { TestStepsViewPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-steps-view.page';
import { TreeElement } from '../../../../../page-objects/elements/grid/grid.element';

describe('Check if a user with Guest cannot associate and disassociate a requirement to a test step', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07222.CT02 - testcase-testcaseviewstandard-steps-link-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testCaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const testCaseViewPage = new TestCaseViewPage(1);
    const testStepView = new TestStepsViewPage();
    assertCannotAssociateRequirementToTTestCaseStep(
      testCaseWorkspace,
      testCaseViewPage,
      testStepView,
    );

    cy.log('Step 2');
    assertCannotUnlinkRequirementFromClassicTestCaseStep(testCaseViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProjects([
      new ProjectBuilder('Project')
        .withRequirementLibraryNodes([new RequirementBuilder('Requirement')])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Test').withSteps([new ActionTestStepBuilder('Action', 'Result')]),
        ])
        .withLinks([new TestCasesStepsLinkToRequirementVersionBuilder('Test', ['Requirement'])]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}

function assertCannotAssociateRequirementToTTestCaseStep(
  testCaseWorkspace: TestCaseWorkspacePage,
  testCaseViewPage: TestCaseViewPage,
  testStepView: TestStepsViewPage,
) {
  selectNodeInTestCaseWorkspace(testCaseWorkspace, 'Project', 'Test');
  testCaseViewPage.clickStepsAnchorLink();
  testStepView
    .getActionStepByIndex(0)
    .clickActionStepActionButtonAndAssertOptionInMenuIsDisableAndClick('add-requirement-coverage');
  const requirementTree = TreeElement.createTreeElement('requirement-tree-picker');
  requirementTree.assertNotExists();
}
