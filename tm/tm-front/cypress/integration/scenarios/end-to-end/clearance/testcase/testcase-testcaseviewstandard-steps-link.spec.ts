import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { RequirementBuilder } from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  ActionTestStepBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { TestCaseViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { TestStepsViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-steps-view.page';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import { assertCanAssociateRequirementToTestStepWithDragAndDrop } from '../../../scenario-parts/testcase.part';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check if a user can associate and disassociate a requirement to a test step', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07022.CT02 - testcase-testcaseviewstandard-steps-link', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testCaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const testCaseViewPage = new TestCaseViewPage(1);
    const testStepView = new TestStepsViewPage();

    assertCanAssociateRequirementToTestStepWithDragAndDrop(
      testCaseWorkspace,
      testCaseViewPage,
      testStepView,
      'Fraise',
      'Pomme',
      0,
      'Framboise',
    );

    cy.log('Step 2');
    unlinkRequirementFromClassicTestCaseStep(testStepView);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('test case linker', [
        ProfileBuilderPermission.TEST_CASE_READ,
        ProfileBuilderPermission.TEST_CASE_LINK,
        ProfileBuilderPermission.REQUIREMENT_READ,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Fraise')
        .withRequirementLibraryNodes([new RequirementBuilder('Framboise')])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Pomme').withSteps([new ActionTestStepBuilder('Fruit', 'Légume')]),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}

function unlinkRequirementFromClassicTestCaseStep(testStepView: TestStepsViewPage) {
  testStepView.getActionStepByIndex(0).removeLinkedRequirement(0);
}
