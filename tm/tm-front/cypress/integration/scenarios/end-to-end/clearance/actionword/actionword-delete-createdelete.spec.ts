import { DatabaseUtils } from '../../../../utils/database.utils';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  ActionWordLibraryNodeBuilder,
  ActionWordTextBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/action-prerequiste-builders';
import { RemoveDatasetDialog } from '../../../../page-objects/pages/test-case-workspace/dialogs/remove-dataset-dialog.element';
import {
  assertNodeNotExistsInActionWordWorkspaceProject,
  selectNodeInActionWordWorkspace,
} from '../../../scenario-parts/actionword.part';
import { ActionWordWorkspacePage } from '../../../../page-objects/pages/action-word-workspace/action-word-workspace.page';
import { KeyboardShortcuts } from '../../../../page-objects/elements/workspace-common/keyboard-shortcuts';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check if the user can delete an action from library icon and keyboard shortcut ', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07061.CT04 - actionword-delete-createdelete', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const actionWordWorkspacePage = NavBarElement.navigateToActionWordWorkspace();
    deletePrerequisiteActionFromLibrary(actionWordWorkspacePage);

    cy.log('Step 2');
    deleteActionWithKeyboardShortcut(actionWordWorkspacePage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('action word deleter', [
        ProfileBuilderPermission.ACTION_WORD_READ,
        ProfileBuilderPermission.ACTION_WORD_DELETE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project').withActionWordLibraryNodes([
        new ActionWordLibraryNodeBuilder('Action 1', 'T-Action 1-').withFragments([
          new ActionWordTextBuilder('Action 1'),
        ]),
        new ActionWordLibraryNodeBuilder('Action 2', 'T-Action 2-').withFragments([
          new ActionWordTextBuilder('Action 2'),
        ]),
      ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}
function deletePrerequisiteActionFromLibrary(actionWordWorkspacePage: ActionWordWorkspacePage) {
  selectNodeInActionWordWorkspace(actionWordWorkspacePage, 'Project', 'Action 1');
  actionWordWorkspacePage.treeMenu.getButton('delete-button').clickWithoutSpan();
  const confirmDialog = new RemoveDatasetDialog();
  confirmDialog.confirm();
  assertNodeNotExistsInActionWordWorkspaceProject(actionWordWorkspacePage, 'Project', 'Action 1');
}

function deleteActionWithKeyboardShortcut(actionWordWorkspacePage: ActionWordWorkspacePage) {
  selectNodeInActionWordWorkspace(actionWordWorkspacePage, 'Project', 'Action 2');
  const keyboardShortcut = new KeyboardShortcuts();
  keyboardShortcut.pressDelete();
  const confirmDialog = new RemoveDatasetDialog();
  confirmDialog.confirm();
  assertNodeNotExistsInActionWordWorkspaceProject(actionWordWorkspacePage, 'Project', 'Action 2');
}
