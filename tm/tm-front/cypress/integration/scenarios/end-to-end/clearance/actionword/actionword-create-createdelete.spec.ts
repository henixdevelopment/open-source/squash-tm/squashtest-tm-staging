import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { ActionWordWorkspacePage } from '../../../../page-objects/pages/action-word-workspace/action-word-workspace.page';
import { CreateEntityDialog } from '../../../../page-objects/pages/create-entity-dialog.element';
import { ActionWordViewPage } from '../../../../page-objects/pages/action-word-workspace/action-word/action-word-view.page';
import { selectNodeProject } from '../../../scenario-parts/actionword.part';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Verify that a user can create an action in the action word workspace', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07061.CT01 - actionword-create-createdelete', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const actionWordWorkspacePage = NavBarElement.navigateToActionWordWorkspace();
    const actionWordViewPage = new ActionWordViewPage();
    createActionFromActionWordWorkspacePage(actionWordWorkspacePage, actionWordViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('action word creator', [
        ProfileBuilderPermission.ACTION_WORD_READ,
        ProfileBuilderPermission.ACTION_WORD_CREATE,
      ]),
    ])
    .withProjects([new ProjectBuilder('Project')])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}

function createActionFromActionWordWorkspacePage(
  actionWordWorkspacePage: ActionWordWorkspacePage,
  actionWordViewPage: ActionWordViewPage,
) {
  selectNodeProject(actionWordWorkspacePage, 'Project');
  const menuItemElement = actionWordWorkspacePage.treeMenu
    .createButton()
    .showMenu()
    .item('new-action-word');
  menuItemElement.click();
  const entityDialogAction = new CreateEntityDialog({
    viewPath: 'action-word-view',
    newEntityPath: 'new-action-word',
    treePath: 'action-word-tree',
  });
  entityDialogAction.assertExists();
  entityDialogAction.fillName('New action');
  entityDialogAction.fillDescription('New description');
  cy.clickVoid();
  entityDialogAction.clickOnAddButton();
  actionWordViewPage.assertExists();
  actionWordViewPage.checkNameContent('New action');
  actionWordViewPage.descriptionTextField.assertContainsText('New description');
}
