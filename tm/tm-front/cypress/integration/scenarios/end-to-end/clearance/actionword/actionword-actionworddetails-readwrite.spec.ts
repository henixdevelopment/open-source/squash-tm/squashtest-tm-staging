import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  ActionWordLibraryNodeBuilder,
  ActionWordParameterBuilder,
  ActionWordTextBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/action-prerequiste-builders';
import { KeywordTestCaseBuilder } from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { TestCaseWorkspacePage } from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { selectNodeInTestCaseWorkspace } from '../../../scenario-parts/testcase.part';
import { TestCaseViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { KeywordTestStepsViewPage } from '../../../../page-objects/pages/test-case-workspace/test-case/keyword-test-step-view-page';
import { ActionWordLevelTwoPage } from '../../../../page-objects/pages/action-word-workspace/action-word/action-word-level-two.page';
import {
  clearDefaultValueInParamGridRow,
  editParamGridCellTextInActionWordPage,
} from '../../../scenario-parts/actionword.part';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Verify that a user can access the action word workspace, remove a description to an action, add the default value of a parameter', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07060.CT02 - actionword-actionworddetails-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const testCaseWorkspace = TestCaseWorkspacePage.initTestAtPage();
    const testCaseViewPage = new TestCaseViewPage(1);
    const keywordStepViewPage = new KeywordTestStepsViewPage();
    const actionWordLevelTwoPage = new ActionWordLevelTwoPage();
    UseActionSuggestion(testCaseWorkspace, testCaseViewPage, keywordStepViewPage);

    cy.log('Step 2');
    consultLevelTwoActionPage(keywordStepViewPage, actionWordLevelTwoPage);

    cy.log('Step 3');
    modifyActionDescription(actionWordLevelTwoPage);

    cy.log('Step 4');
    AddAndEditDefaultValue(actionWordLevelTwoPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('action word writer', [
        ProfileBuilderPermission.ACTION_WORD_READ,
        ProfileBuilderPermission.ACTION_WORD_WRITE,
        ProfileBuilderPermission.TEST_CASE_READ,
        ProfileBuilderPermission.TEST_CASE_WRITE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project')
        .withTestCaseLibraryNodes([new KeywordTestCaseBuilder('TDD')])
        .withActionWordLibraryNodes([
          new ActionWordLibraryNodeBuilder('ActionSansParam', 'T-ActionSansParam-').withFragments([
            new ActionWordTextBuilder('ActionSansParam'),
          ]),
          new ActionWordLibraryNodeBuilder(
            'Action avec "x" param',
            'TPT-Action avec - param-',
          ).withFragments([
            new ActionWordTextBuilder('Action avec '),
            new ActionWordParameterBuilder('x', ''),
            new ActionWordTextBuilder(' param'),
          ]),
        ]),
    ])
    .build();
}

function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}

function UseActionSuggestion(
  testCaseWorkspace: TestCaseWorkspacePage,
  testCaseViewPage: TestCaseViewPage,
  keywordStepViewPage: KeywordTestStepsViewPage,
) {
  selectNodeInTestCaseWorkspace(testCaseWorkspace, 'Project', 'TDD');
  testCaseViewPage.clickKeywordStepsAnchorLink();
  keywordStepViewPage.fillAutocompleteAction('A');
  keywordStepViewPage.checkAutocompletionMenuIsVisible();
  keywordStepViewPage.chooseAutocompletionOptionWithClick('ActionSansParam');
  keywordStepViewPage.actionAutocompleteElement.clearContent();
  keywordStepViewPage.fillAutocompleteAction('A');
  keywordStepViewPage.checkAutocompletionMenuIsVisible();
  keywordStepViewPage.chooseAutocompletionOptionWithClick('Action avec "x" param');
  keywordStepViewPage.addKeywordStep(false);
}

function consultLevelTwoActionPage(
  keywordStepViewPage: KeywordTestStepsViewPage,
  actionWordLevelTwoPage: ActionWordLevelTwoPage,
) {
  keywordStepViewPage.getStepByIndex(0).selectMoreOption('navigate-to-action');
  actionWordLevelTwoPage.assertExists();
}

function modifyActionDescription(actionWordLevelTwoPage: ActionWordLevelTwoPage) {
  actionWordLevelTwoPage.descriptionTextField.click();
  actionWordLevelTwoPage.descriptionTextField.clear();
  actionWordLevelTwoPage.descriptionTextField.fillWithString('Description');
  actionWordLevelTwoPage.descriptionTextField.confirm();
  actionWordLevelTwoPage.descriptionTextField.assertContainsText('Description');
}

function AddAndEditDefaultValue(actionWordLevelTwoPage: ActionWordLevelTwoPage) {
  editParamGridCellTextInActionWordPage(actionWordLevelTwoPage, 'x', 'defaultValue', 'joujou');
  clearDefaultValueInParamGridRow(actionWordLevelTwoPage, 'x');
  editParamGridCellTextInActionWordPage(actionWordLevelTwoPage, 'x', 'defaultValue', 'foufou');
}
