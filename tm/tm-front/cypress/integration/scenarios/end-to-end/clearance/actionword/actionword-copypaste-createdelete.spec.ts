import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  ActionWordLibraryNodeBuilder,
  ActionWordTextBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/action-prerequiste-builders';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { ActionWordWorkspacePage } from '../../../../page-objects/pages/action-word-workspace/action-word-workspace.page';
import { KeyboardShortcuts } from '../../../../page-objects/elements/workspace-common/keyboard-shortcuts';
import {
  assertNodeExistsInActionWordWorkspaceProject,
  selectNodeInActionWordWorkspace,
  selectNodeProject,
} from '../../../scenario-parts/actionword.part';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Verify that a user can copy/paste an action in another project', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07061.CT02 - actionword-copypaste-createdelete', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const actionWordWorkspacePage = NavBarElement.navigateToActionWordWorkspace();

    copyPasteActionToAnotherProject(actionWordWorkspacePage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('action word copier', [
        ProfileBuilderPermission.ACTION_WORD_READ,
        ProfileBuilderPermission.ACTION_WORD_CREATE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('ProjectA').withActionWordLibraryNodes([
        new ActionWordLibraryNodeBuilder('ActionSansParam', 'T-ActionSansParam-').withFragments([
          new ActionWordTextBuilder('ActionSansParam'),
        ]),
      ]),
      new ProjectBuilder('ProjectB'),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1, 2]);
}
function copyPasteActionToAnotherProject(actionWordWorkspacePage: ActionWordWorkspacePage) {
  selectNodeInActionWordWorkspace(actionWordWorkspacePage, 'ProjectA', 'ActionSansParam');

  const keyboardShortcut = new KeyboardShortcuts();
  keyboardShortcut.performCopy();

  selectNodeProject(actionWordWorkspacePage, 'ProjectB');

  keyboardShortcut.performPaste();

  assertNodeExistsInActionWordWorkspaceProject(
    actionWordWorkspacePage,
    'ProjectB',
    'ActionSansParam',
  );
}
