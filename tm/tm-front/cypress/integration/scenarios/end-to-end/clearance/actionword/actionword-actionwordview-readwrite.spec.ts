import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { ActionWordViewPage } from '../../../../page-objects/pages/action-word-workspace/action-word/action-word-view.page';
import {
  ActionWordLibraryNodeBuilder,
  ActionWordParameterBuilder,
  ActionWordTextBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/action-prerequiste-builders';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { ActionWordWorkspacePage } from '../../../../page-objects/pages/action-word-workspace/action-word-workspace.page';
import {
  clearDefaultValueInParamGridRow,
  editParamGridCellTextInActionWordPage,
  selectNodeInActionWordWorkspace,
} from '../../../scenario-parts/actionword.part';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Verify that a user can access the action word workspace, add a description to an action, remove the default value of a parameter and modify the name of a parameter', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07060.CT01 - actionword-actionwordview-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const actionWordWorkspacePage = NavBarElement.navigateToActionWordWorkspace();
    const actionWordViewPage = new ActionWordViewPage();
    consultActionInActionWordWorkspacePage(actionWordWorkspacePage, actionWordViewPage);

    cy.log('Step 2');
    addActionDescription(actionWordViewPage);

    cy.log('Step 3');
    removeParameterDefaultValue(actionWordViewPage);

    cy.log('Step 4');
    modifyParameterName(actionWordViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('action word modifier', [
        ProfileBuilderPermission.ACTION_WORD_READ,
        ProfileBuilderPermission.ACTION_WORD_WRITE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project').withActionWordLibraryNodes([
        new ActionWordLibraryNodeBuilder(
          'Action avec "x" param',
          'TPT-Action avec - param-',
        ).withFragments([
          new ActionWordTextBuilder('Action avec '),
          new ActionWordParameterBuilder('x', ''),
          new ActionWordTextBuilder(' param'),
        ]),
      ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}
function consultActionInActionWordWorkspacePage(
  actionWordWorkspacePage: ActionWordWorkspacePage,
  actionWordViewPage: ActionWordViewPage,
) {
  selectNodeInActionWordWorkspace(actionWordWorkspacePage, 'Project', 'Action avec "x" param');
  actionWordViewPage.assertExists();
}

function addActionDescription(actionWordViewPage: ActionWordViewPage) {
  actionWordViewPage.descriptionTextField.click();
  actionWordViewPage.descriptionTextField.clear();
  actionWordViewPage.descriptionTextField.fillWithString('Description');
  actionWordViewPage.descriptionTextField.confirm();
  actionWordViewPage.descriptionTextField.assertContainsText('Description');
}

function removeParameterDefaultValue(actionWordViewPage: ActionWordViewPage) {
  clearDefaultValueInParamGridRow(actionWordViewPage, 'x');
}

function modifyParameterName(actionWordViewPage: ActionWordViewPage) {
  editParamGridCellTextInActionWordPage(actionWordViewPage, 'x', 'name', 'nb');
}
