import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  ActionWordLibraryNodeBuilder,
  ActionWordTextBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/action-prerequiste-builders';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { ActionWordWorkspacePage } from '../../../../page-objects/pages/action-word-workspace/action-word-workspace.page';
import {
  assertNodeExistsInActionWordWorkspaceProject,
  selectNodeInActionWordWorkspace,
} from '../../../scenario-parts/actionword.part';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check if the user can move an action from one project to another project', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07061.CT03 - actionword-move-createdelete', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const actionWordWorkspacePage = NavBarElement.navigateToActionWordWorkspace();
    moveActionToAnotherProject(actionWordWorkspacePage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('action word mover', [
        ProfileBuilderPermission.ACTION_WORD_READ,
        ProfileBuilderPermission.ACTION_WORD_CREATE,
        ProfileBuilderPermission.ACTION_WORD_DELETE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('ProjectA').withActionWordLibraryNodes([
        new ActionWordLibraryNodeBuilder('Action Sans Param', 'T-Action Sans Param-').withFragments(
          [new ActionWordTextBuilder('Action Sans Param')],
        ),
      ]),
      new ProjectBuilder('ProjectB'),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1, 2]);
}
function moveActionToAnotherProject(actionWordWorkspacePage: ActionWordWorkspacePage) {
  selectNodeInActionWordWorkspace(actionWordWorkspacePage, 'ProjectA', 'Action Sans Param');
  actionWordWorkspacePage.tree.findRowId('NAME', 'Action Sans Param').then((idDropFolder) => {
    actionWordWorkspacePage.tree.beginDragAndDrop(idDropFolder);
    actionWordWorkspacePage.tree.findRowId('NAME', 'ProjectB').then((idSecondProject) => {
      actionWordWorkspacePage.tree.selectNode(idSecondProject);
      actionWordWorkspacePage.tree.dropIntoOtherProject(idSecondProject);
    });
  });
  assertNodeExistsInActionWordWorkspaceProject(
    actionWordWorkspacePage,
    'ProjectB',
    'Action Sans Param',
  );
}
