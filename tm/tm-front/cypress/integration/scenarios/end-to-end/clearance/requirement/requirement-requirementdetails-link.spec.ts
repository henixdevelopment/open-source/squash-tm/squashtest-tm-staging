import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { RequirementBuilder } from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  KeywordTestCaseBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { RequirementVersionsLinkToVerifyingTestCaseBuilder } from '../../../../utils/end-to-end/prerequisite/builders/link-builders';
import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { RequirementVersionViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-view.page';
import {
  changeLinkTypeOfRequirementInVersionPage,
  linkRequirementToRequirementInSecondLevelPage,
  linkTestCaseToRequirementFromLinkTestCasePageInVersionPage,
  linkTestCaseToRequirementVersion,
  unlinkRequirementInVersionPage,
  unlinkTestCasesFromRequirementVersionPage,
  unlinkTestCasesFromRequirementVersionPageWithTopIcon,
} from '../../../scenario-parts/requirement.part';
import { RequirementSecondLevelPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-second-level.page';
import { RemoveVerifyingTestCasesDialogElement } from '../../../../page-objects/pages/test-case-workspace/dialogs/remove-verifying-test-cases-dialog.element';
import { RemoveRequirementVersionLinksDialogElement } from '../../../../page-objects/pages/requirement-workspace/dialogs/remove-requirement-version-links-dialog.element';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can link requirements, test-cases to a requirement from page of level 2', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07012.CT03 - requirement-requirementdetails-link', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    RequirementWorkspacePage.initTestAtPage();
    RequirementSecondLevelPage.navigateToRequirementSecondLevelPage(1);
    const requirementVersionViewPage = new RequirementVersionViewPage();
    const removeRequirementLink = new RemoveRequirementVersionLinksDialogElement(1, [2]);
    const removeLinkTestCaseDialog = new RemoveVerifyingTestCasesDialogElement(1, [1, 2, 3]);

    linkRequirementToRequirementInSecondLevelPage('wow', 'AZERTY', 'requirement');

    cy.log('Step 2');
    changeLinkTypeOfRequirementInVersionPage(
      requirementVersionViewPage,
      'wow',
      'Doublon - Doublon',
      'Doublon',
    );

    cy.log('Step 3');
    unlinkRequirementInVersionPage(requirementVersionViewPage, removeRequirementLink, 'wow', 1, 0);

    cy.log('Step 4');
    associateTestCaseToRequirementInSecondLevelPage(requirementVersionViewPage);
    associateTestCaseToRequirementFromLinkTestCasePage(requirementVersionViewPage);

    cy.log('Step 5');
    dissociateTestCasesFromRequirementInSecondLevelPage(
      requirementVersionViewPage,
      removeLinkTestCaseDialog,
    );
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('requirement linker', [
        ProfileBuilderPermission.REQUIREMENT_READ,
        ProfileBuilderPermission.REQUIREMENT_LINK,
        ProfileBuilderPermission.TEST_CASE_READ,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('AZERTY')
        .withRequirementLibraryNodes([
          new RequirementBuilder('Lala'),
          new RequirementBuilder('Jaffar'),
          new RequirementBuilder('wow'),
        ])
        .withLinks([new RequirementVersionsLinkToVerifyingTestCaseBuilder(['Lala'], 'Wolf')])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Wolf'),
          new TestCaseBuilder('Miel'),
          new KeywordTestCaseBuilder('Hello'),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}
function associateTestCaseToRequirementInSecondLevelPage(
  requirementVersionViewPage: RequirementVersionViewPage,
) {
  linkTestCaseToRequirementVersion(
    requirementVersionViewPage,
    requirementVersionViewPage.verifyingTestCaseTable,
    'AZERTY',
    'Hello',
    1,
  );
  requirementVersionViewPage.verifyingTestCaseTable
    .findRowId('name', 'Hello')
    .then((idTestCase) => {
      requirementVersionViewPage.verifyingTestCaseTable.assertRowExist(idTestCase);
    });
  requirementVersionViewPage.verifyingTestCaseTable.findRowId('name', 'Wolf').then((idTestCase) => {
    requirementVersionViewPage.verifyingTestCaseTable.assertRowExist(idTestCase);
  });
}

function associateTestCaseToRequirementFromLinkTestCasePage(
  requirementVersionViewPage: RequirementVersionViewPage,
) {
  linkTestCaseToRequirementFromLinkTestCasePageInVersionPage(requirementVersionViewPage, 'Miel');
  RequirementSecondLevelPage.navigateToRequirementSecondLevelPage(1);
  requirementVersionViewPage.verifyingTestCaseTable.assertRowCount(3);
  requirementVersionViewPage.verifyingTestCaseTable.findRowId('name', 'Miel').then((idTestCase) => {
    requirementVersionViewPage.verifyingTestCaseTable.assertRowExist(idTestCase);
  });
}

function dissociateTestCasesFromRequirementInSecondLevelPage(
  requirementVersionViewPage: RequirementVersionViewPage,
  removeTestCaseLink: RemoveVerifyingTestCasesDialogElement,
) {
  unlinkTestCasesFromRequirementVersionPage(
    requirementVersionViewPage,
    removeTestCaseLink,
    requirementVersionViewPage.verifyingTestCaseTable,
    'Hello',
    1,
    2,
  );
  unlinkTestCasesFromRequirementVersionPageWithTopIcon(
    requirementVersionViewPage,
    removeTestCaseLink,
    requirementVersionViewPage.verifyingTestCaseTable,
    'Miel',
    1,
    2,
    1,
  );
  requirementVersionViewPage.verifyingTestCaseTable.findRowId('name', 'Wolf').then((idTestCase) => {
    requirementVersionViewPage.verifyingTestCaseTable.assertRowExist(idTestCase);
  });
}
