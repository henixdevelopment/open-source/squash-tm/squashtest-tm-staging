import { DatabaseUtils } from '../../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { RequirementWorkspacePage } from '../../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import {
  verifyCannotAssociateRequirements,
  verifyCannotAssociateTestCases,
  verifyCannotDisassociateLinkedRequirementInRequirement,
  verifyCannotDisassociateLinkedRequirementsInLowLevelRequirementTableOfAHighLevelRequirement,
  verifyCannotDisassociateTestCase,
  verifyLinkCannotBeModified,
} from '../../../../scenario-parts/requirement.part';
import { HighLevelRequirementViewPage } from '../../../../../page-objects/pages/requirement-workspace/requirement/high-level-requirement-view.page';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  HighLevelRequirementBuilder,
  RequirementBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  RequirementsLinkToHighLevelRequirementBuilder,
  RequirementsVersionLinkBuilder,
  TestCasesLinkToVerifiedRequirementVersionBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/link-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { TestCaseBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';

describe('Check if a user with Guest cannot Associate/Dissociate requirements with each other, Modify the type of association and Associate/Dissociate test cases', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07112.CT02 - requirement-requirementview-hl-link-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    const highLevelRequirementViewPage = new HighLevelRequirementViewPage(1, 1);
    verifyCannotAssociateRequirements(
      requirementWorkspacePage,
      'high-level-requirement',
      'Project',
      'High Req',
    );

    cy.log('Step 2');
    verifyLinkCannotBeModified('high-level-requirement', 'name', 'Exig');

    cy.log('Step 3');
    assertGuestCanNotDissociateLinkedRequirement(highLevelRequirementViewPage);

    cy.log('Step 4');
    verifyCannotAssociateTestCases('high-level-requirement');

    cy.log('Step 5');
    assertGuestCanNotDissociateATestCase(highLevelRequirementViewPage);
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProjects([
      new ProjectBuilder('Project')
        .withRequirementLibraryNodes([
          new HighLevelRequirementBuilder('High Req'),
          new RequirementBuilder('Exig'),
        ])
        .withTestCaseLibraryNodes([new TestCaseBuilder('Test')])
        .withLinks([
          new RequirementsLinkToHighLevelRequirementBuilder(['Exig'], 'High Req'),
          new TestCasesLinkToVerifiedRequirementVersionBuilder(['Test'], 'High Req'),
          new RequirementsVersionLinkBuilder('High Req', 'Exig'),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}

function assertGuestCanNotDissociateLinkedRequirement(
  highLevelRequirementViewPage: HighLevelRequirementViewPage,
) {
  verifyCannotDisassociateLinkedRequirementInRequirement('high-level-requirement', 'Exig');
  verifyCannotDisassociateLinkedRequirementsInLowLevelRequirementTableOfAHighLevelRequirement(
    'Exig',
  );
  highLevelRequirementViewPage.currentVersion.requirementLinkTable
    .findRowId('name', 'Exig')
    .then((requirementId) => {
      highLevelRequirementViewPage.currentVersion.requirementLinkTable
        .getRow(requirementId)
        .assertExists();
    });
  highLevelRequirementViewPage.linkedLowLevelRequirementTable
    .findRowId('name', 'Exig')
    .then((requirementId) => {
      highLevelRequirementViewPage.linkedLowLevelRequirementTable
        .getRow(requirementId)
        .assertExists();
    });
}

function assertGuestCanNotDissociateATestCase(
  highLevelRequirementViewPage: HighLevelRequirementViewPage,
) {
  verifyCannotDisassociateTestCase('high-level-requirement', 'Test');
  highLevelRequirementViewPage.currentVersion.verifyingTestCaseTable
    .findRowId('name', 'Test')
    .then((idTestCase) => {
      highLevelRequirementViewPage.currentVersion.verifyingTestCaseTable
        .getRow(idTestCase)
        .assertExists();
    });
}
