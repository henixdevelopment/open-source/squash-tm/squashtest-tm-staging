import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import {
  HighLevelRequirementBuilder,
  RequirementBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  KeywordTestCaseBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { HighLevelRequirementViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/high-level-requirement-view.page';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import {
  associateTestCaseToRequirementFromLinkTestCasePage,
  changeLinkTypeOfRequirementInVersionPage,
  linkRequirementAndHighLevelRequirementToAnyRequirement,
  linkRequirementFromLowLevelRequirementInHighLevelRequirement,
  linkTestCaseToRequirementVersion,
  unlinkRequirementFromLowLevelRequirementInVersionPage,
  unlinkRequirementInVersionPage,
  unlinkTestCasesFromRequirementVersionPage,
  unlinkTestCasesFromRequirementVersionPageWithTopIcon,
} from '../../../scenario-parts/requirement.part';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { RemoveRequirementVersionLinksDialogElement } from '../../../../page-objects/pages/requirement-workspace/dialogs/remove-requirement-version-links-dialog.element';
import { UnbindLowLevelRequirementDialogElement } from '../../../../page-objects/pages/requirement-workspace/dialogs/unbind-low-level-requirement-dialog.element';
import { RemoveVerifyingTestCasesDialogElement } from '../../../../page-objects/pages/test-case-workspace/dialogs/remove-verifying-test-cases-dialog.element';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { HighLevelRequirementVersionPage } from '../../../../page-objects/pages/requirement-workspace/requirement/high-level-requirement-version.page';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';
describe('Verify that a user can link test-cases, requirements and high level requirements to high level requirement', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07012.CT02 - requirement-requirementview-hl-link', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    const highLevelRequirementViewPage = new HighLevelRequirementViewPage(1, 1);
    const highLevelRequirementVersionPage = highLevelRequirementViewPage.currentVersion;
    const lowLevelRequirementTable =
      highLevelRequirementVersionPage.requirementLinkHighLevelRequirementTable;
    const verifyingTestCaseTable = highLevelRequirementVersionPage.verifyingTestCaseTable;
    const dialogDeleteAssociation = new RemoveRequirementVersionLinksDialogElement(1, [2, 3]);
    const dialogUnbindFromLowLevelRequirementTable = new UnbindLowLevelRequirementDialogElement(1, [
      3,
    ]);
    const removeVerifyingTestCaseDialog = new RemoveVerifyingTestCasesDialogElement(1, [1, 2]);

    linkRequirementAndHighLevelRequirementToAnyRequirement(
      requirementWorkspacePage,
      'Athena',
      'Eau',
      'high-level-requirement',
      'Feu',
      'Air',
    );
    linkRequirementFromLowLevelLinkedRequirementDrawer(
      highLevelRequirementViewPage,
      lowLevelRequirementTable,
    );

    cy.log('Step 2');

    changeLinkTypeOfRequirementInVersionPage(
      highLevelRequirementVersionPage,
      'Air',
      'Doublon - Doublon',
      'Doublon',
    );

    cy.log('Step 3');
    unlinkRequirementFromHighLevelRequirement(
      highLevelRequirementVersionPage,
      dialogDeleteAssociation,
    );
    unlinkRequirementFromLowLevelRequirementInVersionPage(
      highLevelRequirementViewPage,
      dialogUnbindFromLowLevelRequirementTable,
      'Air',
      1,
      0,
    );

    cy.log('Step 4');

    linkTestCaseToRequirementVersion(
      highLevelRequirementVersionPage,
      verifyingTestCaseTable,
      'Athena',
      'Terre',
      1,
    );
    assertTestCaseCanBeLinkedToRequirementFromLinkTestCasePage(verifyingTestCaseTable);

    cy.log('Step 5');
    dissociateTestCasesFromRequirement(
      highLevelRequirementVersionPage,
      removeVerifyingTestCaseDialog,
      verifyingTestCaseTable,
    );
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('requirement linker', [
        ProfileBuilderPermission.REQUIREMENT_READ,
        ProfileBuilderPermission.REQUIREMENT_LINK,
        ProfileBuilderPermission.TEST_CASE_READ,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Athena')
        .withRequirementLibraryNodes([
          new HighLevelRequirementBuilder('Eau'),
          new HighLevelRequirementBuilder('Feu'),
          new RequirementBuilder('Air'),
        ])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Terre'),
          new KeywordTestCaseBuilder('Acier'),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}
function linkRequirementFromLowLevelLinkedRequirementDrawer(
  highLevelRequirementViewPage: HighLevelRequirementViewPage,
  lowLevelRequirementTable: GridElement,
) {
  linkRequirementFromLowLevelRequirementInHighLevelRequirement(
    highLevelRequirementViewPage,
    lowLevelRequirementTable,
    'Athena',
    'Air',
    1,
  );
  highLevelRequirementViewPage.currentVersion.requirementLinkHighLevelRequirementTable
    .findRowId('name', 'Air')
    .then((idRequirement) => {
      highLevelRequirementViewPage.currentVersion.requirementLinkHighLevelRequirementTable.assertRowExist(
        idRequirement,
      );
    });
}

function unlinkRequirementFromHighLevelRequirement(
  highLevelRequirementVersionPage: HighLevelRequirementVersionPage,
  dialogDeleteAssociation: RemoveRequirementVersionLinksDialogElement,
) {
  unlinkRequirementInVersionPage(
    highLevelRequirementVersionPage,
    dialogDeleteAssociation,
    'Air',
    1,
    1,
  );
  highLevelRequirementVersionPage.requirementLinkTable
    .findRowId('name', 'Feu')
    .then((idRequirement) => {
      highLevelRequirementVersionPage.requirementLinkTable
        .getRow(idRequirement)
        .findRow()
        .should('contain.text', 'Feu');
    });
}

function assertTestCaseCanBeLinkedToRequirementFromLinkTestCasePage(
  verifyingTestCaseTable: GridElement,
) {
  associateTestCaseToRequirementFromLinkTestCasePage('high-level-requirement', 'Acier');
  verifyingTestCaseTable.assertRowCount(2);
}

function dissociateTestCasesFromRequirement(
  highLevelRequirementVersionPage: HighLevelRequirementVersionPage,
  removeVerifyingTestCaseDialog: RemoveVerifyingTestCasesDialogElement,
  verifyingTestCaseTable: GridElement,
) {
  unlinkTestCasesFromRequirementVersionPage(
    highLevelRequirementVersionPage,
    removeVerifyingTestCaseDialog,
    verifyingTestCaseTable,
    'Terre',
    1,
    1,
  );
  highLevelRequirementVersionPage.verifyingTestCaseTable
    .findRowId('name', 'Acier')
    .then((idTestCase) => {
      highLevelRequirementVersionPage.verifyingTestCaseTable.assertRowExist(idTestCase);
    });
  unlinkTestCasesFromRequirementVersionPageWithTopIcon(
    highLevelRequirementVersionPage,
    removeVerifyingTestCaseDialog,
    verifyingTestCaseTable,
    'Acier',
    1,
    2,
    0,
  );
}
