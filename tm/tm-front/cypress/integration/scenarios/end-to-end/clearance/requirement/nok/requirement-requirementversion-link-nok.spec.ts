import { DatabaseUtils } from '../../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  HighLevelRequirementBuilder,
  RequirementBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import { TestCaseBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  RequirementsLinkToHighLevelRequirementBuilder,
  RequirementsVersionLinkBuilder,
  TestCasesLinkToVerifiedRequirementVersionBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/link-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { addVersionToRequirement } from '../../../../../utils/end-to-end/api/requirement-versions/create-requirement-version';
import { RequirementWorkspacePage } from '../../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import {
  selectRequirementAndAssertButtonLinkToHighRequirementIsNotPresent,
  verifyButtonsToAddOrRemoveRelationToAHighLevelRequirementAreNotPresentInRequirement,
  verifyCannotAssociateRequirements,
  verifyCannotAssociateTestCases,
  verifyCannotDisassociateLinkedRequirementInRequirement,
  verifyCannotDisassociateTestCase,
  verifyLinkCannotBeModified,
} from '../../../../scenario-parts/requirement.part';
import { RequirementViewPage } from '../../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';

describe('Check if a user with Guest cannot perform the following actions from the version page of a standard requirement: Associate/dissociate requirements, Modify the association type, Associate/dissociate test cases', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
    createVersionToRequirement();
  });

  it('F01-UC07212.CT05 - requirement-requirementversion-link-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    const requirementViewPage = new RequirementViewPage();

    verifyCannotAssociateRequirements(requirementWorkspacePage, 'requirement', 'Project', 'Exig A');

    cy.log('Step 2');
    verifyLinkCannotBeModified('requirement', 'name', 'haut Exig');

    cy.log('Step 3');
    assertCannotDisassociateRequirement(requirementWorkspacePage, requirementViewPage);

    cy.log('Step 4');
    verifyCannotAssociateTestCases('requirement');

    cy.log('Step 5');
    assertCannotDisassociateTestCase(requirementViewPage);
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProjects([
      new ProjectBuilder('Project')
        .withRequirementLibraryNodes([
          new HighLevelRequirementBuilder('haut Exig'),
          new RequirementBuilder('Exig A'),
          new RequirementBuilder('Exig B'),
        ])
        .withTestCaseLibraryNodes([new TestCaseBuilder('Test')])
        .withLinks([
          new RequirementsLinkToHighLevelRequirementBuilder(['Exig A'], 'haut Exig'),
          new TestCasesLinkToVerifiedRequirementVersionBuilder(['Test'], 'Exig B'),
          new RequirementsVersionLinkBuilder('haut Exig', 'Exig A'),
        ]),
    ])
    .build();
}
function createVersionToRequirement() {
  addVersionToRequirement(2, true, false);
  addVersionToRequirement(3, false, true);
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}

function assertCannotDisassociateRequirement(
  requirementWorkspacePage: RequirementWorkspacePage,
  requirementViewPage: RequirementViewPage,
) {
  verifyCannotDisassociateLinkedRequirementInRequirement('requirement', 'haut Exig');
  requirementViewPage.currentVersion.requirementLinkTable
    .findRowId('name', 'haut Exig')
    .then((requirementId) => {
      requirementViewPage.currentVersion.requirementLinkTable.getRow(requirementId).assertExists();
    });
  verifyButtonsToAddOrRemoveRelationToAHighLevelRequirementAreNotPresentInRequirement();
  selectRequirementAndAssertButtonLinkToHighRequirementIsNotPresent(
    requirementWorkspacePage,
    'Project',
    'Exig B',
  );
}
function assertCannotDisassociateTestCase(requirementViewPage: RequirementViewPage) {
  verifyCannotDisassociateTestCase('requirement', 'Test');
  requirementViewPage.currentVersion.verifyingTestCaseTable
    .findRowId('name', 'Test')
    .then((idTestCase) => {
      requirementViewPage.currentVersion.verifyingTestCaseTable.getRow(idTestCase).assertExists();
    });
}
