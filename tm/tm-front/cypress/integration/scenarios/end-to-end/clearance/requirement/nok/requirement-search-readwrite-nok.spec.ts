import { DatabaseUtils } from '../../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  MilestoneBuilder,
  MilestoneOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/milestone-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  HighLevelRequirementBuilder,
  RequirementBuilder,
  RequirementFolderBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { RequirementWorkspacePage } from '../../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { TreeToolbarElement } from '../../../../../page-objects/elements/workspace-common/tree-toolbar.element';
import {
  goToRequirementSearchPage,
  selectObjectAsPerimeterInSearchRequirementPage,
} from '../../../../scenario-parts/requirement.part';
import { RequirementSearchPage } from '../../../../../page-objects/pages/requirement-workspace/search/requirement-search-page';
import { GridElement } from '../../../../../page-objects/elements/grid/grid.element';
import { ProjectScopePage } from '../../../../../page-objects/pages/requirement-workspace/search/ProjectScopePage';
import { FilterEnumSingleSelectWidgetElement } from '../../../../../page-objects/elements/filters/filter-enum-single-select-widget.element';

describe('Check if a user with Guest can view and search for standard and high-level requirements on the Requirements search page, while prohibiting the following actions: modifying attributes, renaming a requirement, and associating/disassociating a milestone', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07210.CT05 - requirement-search-readwrite-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    const treeToolbarElement = new TreeToolbarElement('requirement-toolbar');
    const requirementSearchPage = new RequirementSearchPage(new GridElement('requirement-search'));
    const projectScope = new ProjectScopePage();
    goToRequirementSearchPage(requirementWorkspacePage, treeToolbarElement);
    selectFolderAsPerimeterAndModifySearchCriteria(requirementSearchPage, projectScope);

    cy.log('Step 2');
    canNotModifyFieldAttributesInRequirementPage(requirementSearchPage);

    cy.log('Step 3');
    verifyCannotRenameRequirementFields(requirementSearchPage);

    cy.log('Step 4');
    canNotAssociateDissociateMilestone(requirementSearchPage);
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withMilestones([new MilestoneBuilder('Jalon', '2025-10-31 14:44:45').withUserLogin('RonW')])
    .withProjects([
      new ProjectBuilder('Project')
        .withRequirementLibraryNodes([
          new RequirementFolderBuilder('Folder').withRequirementLibraryNodes([
            new RequirementBuilder('Exi 1').withCriticality('MINOR'),
            new HighLevelRequirementBuilder('Exi 2').withCriticality('MAJOR'),
          ]),
        ])
        .withMilestonesOnProject([new MilestoneOnProjectBuilder('Jalon')]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}
function selectFolderAsPerimeterAndModifySearchCriteria(
  requirementSearchPage: RequirementSearchPage,
  projectScope: ProjectScopePage,
) {
  selectObjectAsPerimeterInSearchRequirementPage(
    requirementSearchPage,
    projectScope,
    'Project',
    'Folder',
  );
  requirementSearchPage.findByElementId('grid-filter-field').click();
  const filterEnumSingleSelectWidgetElement = new FilterEnumSingleSelectWidgetElement();
  filterEnumSingleSelectWidgetElement.assertExists();
  filterEnumSingleSelectWidgetElement.selectFilter(
    'Seulement la dernière version non obsolète de chaque exigence',
  );
  filterEnumSingleSelectWidgetElement.assertFilterElementIsSelected(
    'Seulement la dernière version non obsolète de chaque exigence',
  );
  requirementSearchPage.checkbox.findAndClick('span.ant-checkbox-inner');
  requirementSearchPage.addCriteriaCriticityandLevelOfCriticity('Mineur');
  requirementSearchPage.clickOnNewSearch();
}

function canNotModifyFieldAttributesInRequirementPage(
  requirementSearchPage: RequirementSearchPage,
) {
  requirementSearchPage.grid.findRowId('name', 'Exi 1').then((projectId) => {
    requirementSearchPage.grid
      .getCell(projectId, 'category')
      .treeNodeRenderer()
      .assertIsNotSelectable();
  });
  requirementSearchPage.grid.findRowId('name', 'Exi 1').then((idReq) => {
    requirementSearchPage.grid.getCell(idReq, '#', 'leftViewport').indexRenderer().toggle();
  });
  const alertDialog = requirementSearchPage.showMassEditDisabledAlert();
  alertDialog.assertExists();
  alertDialog.clickOnCloseButton();
}

function verifyCannotRenameRequirementFields(requirementSearchPage: RequirementSearchPage) {
  requirementSearchPage.grid.findRowId('name', 'Exi 1').then((id) => {
    requirementSearchPage.grid.getCell(id, 'name').textRenderer().assertIsNotEditable();
  });
  requirementSearchPage.grid.findRowId('name', 'Exi 1').then((id) => {
    requirementSearchPage.grid.getCell(id, 'reference').treeNodeRenderer().assertIsNotSelected();
  });
}

function canNotAssociateDissociateMilestone(requirementSearchPage: RequirementSearchPage) {
  const dialogAlert = requirementSearchPage.showNoPermissionsToEditMilestoneDialog();
  dialogAlert.assertExists();
  dialogAlert.clickOnCloseButton();
}
