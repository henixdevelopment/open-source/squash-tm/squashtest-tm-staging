import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
  ListOptionCustomFieldBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import {
  HighLevelRequirementBuilder,
  RequirementBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { RequirementViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import { RequirementVersionViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-view.page';
import { RequirementVersionViewInformationPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-view-information.page';
import {
  bindMilestoneToRequirement,
  modifyNameAndReferenceRequirement,
  openStandardChildNodeAndClickOnHighLevelRequirementToAccessSecondLevelPage,
} from '../../../scenario-parts/requirement.part';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  MilestoneBuilder,
  MilestoneOnProjectBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/milestone-prerequisite-builders';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Verify read/write authorization in the requirement view for a high level requirement', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07010.CT03 - requirement-requirementdetails-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    const requirementViewPage = new RequirementViewPage();
    const requirementVersionViewPage = new RequirementVersionViewPage();
    const requirementVersionInformationViewPage = new RequirementVersionViewInformationPage();
    const highLevelRequirementName = requirementVersionInformationViewPage.highLevelRequirementName;
    const nameTextField = requirementViewPage.nameTextField;
    const referenceTextField = requirementViewPage.referenceTextField;

    openStandardChildNodeAndClickOnHighLevelRequirementToAccessSecondLevelPage(
      requirementWorkspacePage,
      requirementWorkspacePage.tree,
      requirementVersionViewPage,
      requirementViewPage,
      highLevelRequirementName,
      'Chronos',
      'Jupiter',
      'Mars',
    );

    cy.log('Step 2');
    requirementVersionViewPage.clickActionMenuAndAssertPrintModeExists();

    cy.log('Step 3');
    modifyCriticality(requirementViewPage, requirementVersionInformationViewPage);

    cy.log('Step 4');
    modifyNameAndReferenceRequirement(nameTextField, referenceTextField, 'Zeus', 'Demeter');

    cy.log('Step 5');
    bindWithMilestone(requirementVersionInformationViewPage);
  });
});

function modifyCriticality(
  requirementViewPage: RequirementViewPage,
  requirementVersionInformationViewPage: RequirementVersionViewInformationPage,
) {
  requirementViewPage.criticalityCapsule.selectOption('Critique');
  requirementVersionInformationViewPage.criticalityField.selectValueNoButton('Majeure');
  requirementVersionInformationViewPage.addCufTag('CufTAG', 'tagValue');
  requirementVersionInformationViewPage.deleteCufTag('CufTAG', 'tagValue');
}

function bindWithMilestone(
  requirementVersionInformationViewPage: RequirementVersionViewInformationPage,
) {
  bindMilestoneToRequirement(requirementVersionInformationViewPage, 'Mars');
  bindMilestoneToRequirement(requirementVersionInformationViewPage, 'Aphrodite');
  requirementVersionInformationViewPage.milestoneTagElement.clickOnRemoveMilestoneClose('Mars');
}

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('requirement modifier', [
        ProfileBuilderPermission.REQUIREMENT_READ,
        ProfileBuilderPermission.REQUIREMENT_WRITE,
        ProfileBuilderPermission.REQUIREMENT_EXPORT,
      ]),
    ])
    .withCufs([
      new CustomFieldBuilder('CufTAG', InputType.TAG).withOptions([
        new ListOptionCustomFieldBuilder('opt1', 'code1'),
        new ListOptionCustomFieldBuilder('opt2', 'code2'),
      ]),
    ])
    .withMilestones([
      new MilestoneBuilder('Mars', '2025-10-31 14:44:45').withUserLogin('RonW'),
      new MilestoneBuilder('Aphrodite', '2027-02-11 16:00:00').withUserLogin('RonW'),
    ])
    .withProjects([
      new ProjectBuilder('Chronos')
        .withRequirementLibraryNodes([
          new HighLevelRequirementBuilder('PLOUP'),
          new HighLevelRequirementBuilder('Jupiter').withRequirements([
            new RequirementBuilder('Mars'),
            new RequirementBuilder('Vulcain'),
          ]),
        ])
        .withCufsOnProject([
          new CustomFieldOnProjectBuilder('CufTAG', BindableEntity.REQUIREMENT_VERSION),
        ])
        .withMilestonesOnProject([
          new MilestoneOnProjectBuilder('Mars'),
          new MilestoneOnProjectBuilder('Aphrodite'),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}
