import { DatabaseUtils } from '../../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import {
  MilestoneBuilder,
  MilestoneOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/milestone-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { RequirementBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import { BindableEntity } from '../../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { RequirementWorkspacePage } from '../../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { RequirementViewPage } from '../../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import { selectNodeInAnotherNode } from '../../../../scenario-parts/requirement.part';
import { CheckBoxElement } from '../../../../../page-objects/elements/forms/check-box.element';

describe('Check if a user with Guest access can view or print a requirement, while prohibiting the following actions: modifying attributes, renaming a requirement, and associating/disassociating a milestone', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07210.CT02 - requirement-requirementview-readwrite-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    const requirementViewPage = new RequirementViewPage();
    verifyRequirementPrintExists(requirementWorkspacePage, requirementViewPage);

    cy.log('Step 2');
    cannotModifyAttributes(requirementViewPage);

    cy.log('Step 3');
    verifyCannotRenameRequirementFields(requirementViewPage);

    cy.log('Step 4');
    assertCannotAssociateDisassociateMilestone(requirementViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withCufs([new CustomFieldBuilder('CufCheck', InputType.CHECKBOX)])
    .withMilestones([new MilestoneBuilder('Jalon2', '2025-10-31 14:44:45').withUserLogin('RonW')])
    .withProjects([
      new ProjectBuilder('AZERTY')
        .withRequirementLibraryNodes([new RequirementBuilder('Exi1').withMilestones(['Jalon2'])])
        .withCufsOnProject([
          new CustomFieldOnProjectBuilder('CufCheck', BindableEntity.REQUIREMENT_VERSION),
        ])
        .withMilestonesOnProject([new MilestoneOnProjectBuilder('Jalon2')]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}
function verifyRequirementPrintExists(
  requirementWorkspacePage: RequirementWorkspacePage,
  requirementViewPage: RequirementViewPage,
) {
  selectNodeInAnotherNode(requirementWorkspacePage, 'AZERTY', 'Exi1');
  requirementViewPage.assertExists();
  requirementViewPage.clickActionMenuAndAssertPrintModeNotExists();
}

function cannotModifyAttributes(requirementViewPage: RequirementViewPage) {
  const requirementInfo = requirementViewPage.currentVersion.clickInformationAnchorLink();
  requirementViewPage.statusCapsule.assertIsNotClickable();
  requirementInfo.statusField.assertIsNotEditable();
  requirementInfo.descriptionElement.assertIsNotEditable();
  const checkbox = requirementViewPage.getCustomField(
    'CufCheck',
    InputType.CHECKBOX,
  ) as CheckBoxElement;
  checkbox.click();
  checkbox.findAndCheckState('span', false);
}
function verifyCannotRenameRequirementFields(requirementViewPage: RequirementViewPage) {
  requirementViewPage.nameTextField.assertIsNotEditable();
  requirementViewPage.referenceTextField.assertIsNotEditable();
}

function assertCannotAssociateDisassociateMilestone(requirementViewPage: RequirementViewPage) {
  const requirementInfo = requirementViewPage.currentVersion.clickInformationAnchorLink();
  requirementInfo.milestoneTagElement.openMilestoneDialogNotExist();
  requirementInfo.milestoneTagElement.removeMilestoneButtonShouldNotExist('Jalon2');
}
