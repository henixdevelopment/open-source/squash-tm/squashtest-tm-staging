import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';

import {
  HighLevelRequirementBuilder,
  RequirementBuilder,
  RequirementFolderBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { dragObjectInToAnotherObjectInRequirementWorkspace } from '../../../scenario-parts/requirement.part';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Verify drag and drop authorization in the requirement tree', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07011.CT03 - requirement-move-createdelete', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();

    dragObjectInToAnotherObjectInRequirementWorkspace(
      requirementWorkspacePage,
      'Athena',
      'Folder',
      'Folder2',
      'Athena',
      false,
    );
    dragObjectInToAnotherObjectInRequirementWorkspace(
      requirementWorkspacePage,
      'Medusa',
      'Folder3',
      'Athena',
      'Medusa',
      true,
    );

    cy.log('Step 2');
    dragObjectInToAnotherObjectInRequirementWorkspace(
      requirementWorkspacePage,
      'Folder2',
      'EHN2',
      'Athena',
      'Folder2',
      false,
    );
    dragObjectInToAnotherObjectInRequirementWorkspace(
      requirementWorkspacePage,
      'Medusa',
      'Exigence3',
      'Exigence4',
      'Medusa',
      false,
    );
    dragObjectInToAnotherObjectInRequirementWorkspace(
      requirementWorkspacePage,
      'Medusa',
      'Exigence3',
      'Athena',
      'Exigence4',
      true,
    );
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('requirement mover', [
        ProfileBuilderPermission.REQUIREMENT_READ,
        ProfileBuilderPermission.REQUIREMENT_CREATE,
        ProfileBuilderPermission.REQUIREMENT_DELETE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Athena').withRequirementLibraryNodes([
        new RequirementFolderBuilder('Folder').withRequirementLibraryNodes([
          new HighLevelRequirementBuilder('EHN').withCriticality('MAJOR'),
        ]),
        new RequirementFolderBuilder('Folder2').withRequirementLibraryNodes([
          new HighLevelRequirementBuilder('EHN2'),
        ]),
        new HighLevelRequirementBuilder('HighLevelRequirement'),
      ]),
      new ProjectBuilder('Medusa').withRequirementLibraryNodes([
        new RequirementFolderBuilder('Folder3').withRequirementLibraryNodes([
          new RequirementBuilder('exiClassique').withCriticality('MAJOR'),
        ]),
        new RequirementBuilder('Exigence3'),
        new RequirementBuilder('Exigence4'),
      ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1, 2]);
}
