import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  RequirementBuilder,
  RequirementVersionBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  KeywordTestCaseBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { RequirementViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import { RequirementVersionDetailViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-detail-view.page';
import { RequirementMultiVersionPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-multi-version.page';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  changeLinkTypeOfRequirementInVersionPage,
  linkRequirementToAnyRequirementInVersionPage,
  linkTestCaseToRequirementFromLinkTestCasePageInVersionPage,
  linkTestCaseToRequirementVersion,
  unlinkRequirementWithTopIconInVersionPage,
  unlinkTestCasesFromRequirementVersionPage,
  unlinkTestCasesFromRequirementVersionPageWithTopIcon,
} from '../../../scenario-parts/requirement.part';
import { RemoveVerifyingTestCasesDialogElement } from '../../../../page-objects/pages/test-case-workspace/dialogs/remove-verifying-test-cases-dialog.element';
import { RemoveRequirementVersionLinksDialogElement } from '../../../../page-objects/pages/requirement-workspace/dialogs/remove-requirement-version-links-dialog.element';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can link requirements, test-cases to a requirement from version page', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07012.CT05 - requirement-requirementversion-link', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    const requirementViewPage = new RequirementViewPage();
    const grid = new GridElement('requirement-versions-grid');
    const requirementMultiVersionPage = new RequirementMultiVersionPage(grid);
    const requirementVersion = new RequirementVersionDetailViewPage(3);
    const requirementVersionPage = requirementVersion.requirementVersionPage;
    const verifyingTestCaseTable = requirementViewPage.currentVersion.verifyingTestCaseTable;
    const removeVerifyingTestCaseDialog = new RemoveVerifyingTestCasesDialogElement(3, [1, 2]);
    const removeRequirementLinkDialog = new RemoveRequirementVersionLinksDialogElement(3, [1]);

    linkRequirementToAnyRequirementInVersionPage(
      requirementWorkspacePage,
      'requirement',
      requirementMultiVersionPage,
      requirementVersion,
      'AZERTY',
      'Zeus',
      'wow',
      1,
    );

    cy.log('Step 2');
    changeLinkTypeOfRequirementInVersionPage(
      requirementVersionPage,
      'wow',
      'Parent - Enfant',
      'Enfant',
    );

    cy.log('Step 3');
    unlinkRequirementWithTopIconInVersionPage(
      requirementVersionPage,
      removeRequirementLinkDialog,
      'wow',
      1,
    );

    cy.log('Step 4');
    linkTestCaseToRequirementVersion(
      requirementVersionPage,
      verifyingTestCaseTable,
      'AZERTY',
      'Hello',
      3,
    );
    verifyingTestCaseTable.assertRowCount(1);
    linkTestCaseToRequirementFromLinkTestCasePageInVersionPage(requirementVersionPage, 'Wolf');
    verifyingTestCaseTable.findRowId('name', 'Wolf').then((idTestCase) => {
      verifyingTestCaseTable.assertRowExist(idTestCase);
      verifyingTestCaseTable.assertRowCount(2);
    });

    cy.log('Step 5');
    unlinkTestCasesFromRequirementVersionPage(
      requirementVersionPage,
      removeVerifyingTestCaseDialog,
      verifyingTestCaseTable,
      'Hello',
      3,
      1,
    );
    unlinkTestCasesFromRequirementVersionPageWithTopIcon(
      requirementVersionPage,
      removeVerifyingTestCaseDialog,
      verifyingTestCaseTable,
      'Wolf',
      3,
      1,
      0,
    );
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('requirement linker', [
        ProfileBuilderPermission.REQUIREMENT_READ,
        ProfileBuilderPermission.REQUIREMENT_LINK,
        ProfileBuilderPermission.TEST_CASE_READ,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('AZERTY')
        .withRequirementLibraryNodes([
          new RequirementBuilder('Lala').withVersions([
            new RequirementVersionBuilder('Hera'),
            new RequirementVersionBuilder('Zeus'),
          ]),
          new RequirementBuilder('Jaffar'),
          new RequirementBuilder('wow'),
        ])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Wolf'),
          new KeywordTestCaseBuilder('Hello'),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}
