import { DatabaseUtils } from '../../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { RequirementFolderBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import { BindableEntity } from '../../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { RequirementWorkspacePage } from '../../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import {
  selectNodeInAnotherNode,
  selectNodeProject,
} from '../../../../scenario-parts/requirement.part';
import { RequirementLibraryViewPage } from '../../../../../page-objects/pages/requirement-workspace/requirement-library/requirement-library-view.page';
import { RequirementFolderViewPage } from '../../../../../page-objects/pages/requirement-workspace/requirement-folder/requirement-folder-view.page';
import { EditableTextFieldElement } from '../../../../../page-objects/elements/forms/editable-text-field.element';

describe('Check if a user with Guest access can view a folder page in the Requirements Workspace but cannot modify attributes or rename the folder', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07110.CT01 - requirement-folderview-readwrite-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    const requirementFolderViewPage = new RequirementFolderViewPage();
    consultLibrarySettingsAndNavigateTree(requirementWorkspacePage, requirementFolderViewPage);

    cy.log('Step 2');
    attributesCannotBeModified(requirementFolderViewPage);

    cy.log('Step 3');
    cannotRenameFolderNameField(requirementFolderViewPage);
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withCufs([new CustomFieldBuilder('Text', InputType.PLAIN_TEXT)])
    .withProjects([
      new ProjectBuilder('Project')
        .withRequirementLibraryNodes([new RequirementFolderBuilder('Folder')])
        .withCufsOnProject([
          new CustomFieldOnProjectBuilder('Text', BindableEntity.REQUIREMENT_FOLDER),
        ]),
    ])
    .build();
}

function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}

function consultLibrarySettingsAndNavigateTree(
  requirementWorkspacePage: RequirementWorkspacePage,
  requirementFolderViewPage: RequirementFolderViewPage,
) {
  const menu = requirementWorkspacePage.treeMenu.buttonMenu('settings-button', 'sort-menu');
  menu.showMenu().item('sort-positional').assertExists();
  menu.showMenu().item('sort-positional').click();
  cy.clickVoid();
  selectNodeProject(requirementWorkspacePage, 'Project');
  const requirementLibraryViewPage = new RequirementLibraryViewPage(1);
  requirementLibraryViewPage.assertExists();
  selectNodeInAnotherNode(requirementWorkspacePage, 'Project', 'Folder');
  requirementFolderViewPage.assertExists();
}

function attributesCannotBeModified(requirementFolderViewPage: RequirementFolderViewPage) {
  const requirementFolderInfo = requirementFolderViewPage.showInformationPanel();
  requirementFolderInfo.descriptionRichField.assertIsNotEditable();
  const textCuf = requirementFolderViewPage.getCustomField(
    'Text',
    InputType.PLAIN_TEXT,
  ) as EditableTextFieldElement;
  textCuf.assertExists();
  textCuf.assertIsNotEditable();
}

function cannotRenameFolderNameField(requirementFolderViewPage: RequirementFolderViewPage) {
  requirementFolderViewPage.nameTextField.assertIsNotEditable();
}
