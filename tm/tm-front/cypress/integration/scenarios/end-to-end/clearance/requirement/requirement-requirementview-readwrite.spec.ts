import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
  ListOptionCustomFieldBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { RequirementBuilder } from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { RequirementViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { RequirementVersionViewInformationPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-view-information.page';
import { TreeElement } from '../../../../page-objects/elements/grid/grid.element';
import {
  assertNameAndReferenceOfRequirementCanBeChanged,
  modifyDescriptionInRequirement,
  verifyMilestoneBindingAndUnbindingToRequirement,
} from '../../../scenario-parts/requirement.part';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  MilestoneBuilder,
  MilestoneOnProjectBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/milestone-prerequisite-builders';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Verify read/write authorization in the requirement view', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07010.CT02 - requirement-requirementview-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    const requirementWorkspaceTree = requirementWorkspacePage.tree;
    const requirementViewPage = new RequirementViewPage();
    verifyRequirementPrintExists(
      requirementWorkspacePage,
      requirementViewPage,
      requirementWorkspaceTree,
    );

    cy.log('Step 2');
    const requirementVersionViewInformationPage =
      requirementViewPage.currentVersion.clickInformationAnchorLink();
    verifyRequirementStatusCanBeModified(
      requirementVersionViewInformationPage,
      requirementViewPage,
    );
    modifyDescriptionInRequirement(requirementVersionViewInformationPage, 'Bonjour');
    verifyCufCanBeModified(requirementVersionViewInformationPage);

    cy.log('Step 3');
    assertNameAndReferenceOfRequirementCanBeChanged(requirementViewPage, 'Zeus', 'Azur');

    cy.log('Step 4');
    verifyMilestoneBindingAndUnbindingToRequirement(
      requirementVersionViewInformationPage,
      'Jalon2',
    );
  });
});

function verifyRequirementPrintExists(
  requirementWorkspacePage: RequirementWorkspacePage,
  requirementView: RequirementViewPage,
  requirementWorkspaceTree: TreeElement,
) {
  requirementWorkspacePage.assertExists();
  requirementWorkspaceTree.findRowId('NAME', 'AZERTY').then((requirementId) => {
    requirementWorkspaceTree.selectNode(requirementId);
    requirementWorkspaceTree.openNodeIfClosed(requirementId);
  });
  requirementWorkspaceTree.findRowId('NAME', 'Exi1').then((requirementId) => {
    requirementWorkspaceTree.selectNode(requirementId);
    requirementView.assertExists();
    requirementView.clickActionMenuAndAssertPrintModeExists();
  });
}

function verifyRequirementStatusCanBeModified(
  informationPage: RequirementVersionViewInformationPage,
  requirementView: RequirementViewPage,
) {
  requirementView.statusCapsule.selectOption('À approuver');
  informationPage.statusField.selectValueNoButton('En cours de rédaction');
}

function verifyCufCanBeModified(informationPage: RequirementVersionViewInformationPage) {
  informationPage.clickAndChooseOptionCufValueInList('CufList1', 'opt1');
}

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('requirement modifier', [
        ProfileBuilderPermission.REQUIREMENT_READ,
        ProfileBuilderPermission.REQUIREMENT_WRITE,
        ProfileBuilderPermission.REQUIREMENT_EXPORT,
      ]),
    ])
    .withCufs([
      new CustomFieldBuilder('CufList1', InputType.DROPDOWN_LIST).withOptions([
        new ListOptionCustomFieldBuilder('opt1', 'code1'),
        new ListOptionCustomFieldBuilder('opt2', 'code2'),
      ]),
    ])
    .withMilestones([new MilestoneBuilder('Jalon2', '2025-10-31 14:44:45').withUserLogin('RonW')])
    .withProjects([
      new ProjectBuilder('AZERTY')
        .withRequirementLibraryNodes([
          new RequirementBuilder('Exi1'),
          new RequirementBuilder('EXI2'),
        ])
        .withCufsOnProject([
          new CustomFieldOnProjectBuilder('CufList1', BindableEntity.REQUIREMENT_VERSION),
        ])
        .withMilestonesOnProject([new MilestoneOnProjectBuilder('Jalon2')]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}
