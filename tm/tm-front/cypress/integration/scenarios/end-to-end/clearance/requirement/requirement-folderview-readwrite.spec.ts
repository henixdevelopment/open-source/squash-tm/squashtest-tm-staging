import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';

import { RequirementFolderViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement-folder/requirement-folder-view.page';
import { RequirementFolderBuilder } from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { EditableTextFieldElement } from '../../../../page-objects/elements/forms/editable-text-field.element';
import { assertRequirementTreeHabilitation } from '../../../scenario-parts/requirement.part';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Verify read/write authorization in the requirement section', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07110.CT01 - requirement-folderview-readwrite.', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    const requirementFolderViewPage = new RequirementFolderViewPage();
    const requirementFolderNameTextField = requirementFolderViewPage.nameTextField;

    assertRequirementTreeHabilitation(
      requirementWorkspacePage,
      requirementWorkspacePage.tree,
      'AZERTY',
      'Exi',
    );

    cy.log('Step 2');
    verifyInformationPanelAuthorization(requirementFolderViewPage);

    cy.log('Step 3');
    verifyRequirementFolderPageAuthorization(requirementFolderNameTextField);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('requirement modifier', [
        ProfileBuilderPermission.REQUIREMENT_READ,
        ProfileBuilderPermission.REQUIREMENT_WRITE,
      ]),
    ])
    .withCufs([new CustomFieldBuilder('CufCheckbox1', InputType.CHECKBOX)])
    .withProjects([
      new ProjectBuilder('AZERTY')
        .withRequirementLibraryNodes([new RequirementFolderBuilder('Exi1')])
        .withCufsOnProject([
          new CustomFieldOnProjectBuilder('CufCheckbox1', BindableEntity.REQUIREMENT_FOLDER),
        ]),
    ])
    .build();
}

function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}

function verifyInformationPanelAuthorization(requirementFolderViewPage: RequirementFolderViewPage) {
  requirementFolderViewPage.assertExists();
  const informationPanel = requirementFolderViewPage.showInformationPanel();
  informationPanel.descriptionRichField.setAndConfirmValue('Hello World');
  informationPanel.descriptionRichField.assertContainsText('Hello World');
  informationPanel.clickOnCufCheckbox('CufCheckbox1');
}

function verifyRequirementFolderPageAuthorization(
  requirementFolderNameTextField: EditableTextFieldElement,
) {
  requirementFolderNameTextField.setValue('Exi1 is back');
  requirementFolderNameTextField.clickOnConfirmButton();
  requirementFolderNameTextField.assertContainsText('Exi1 is back');
}
