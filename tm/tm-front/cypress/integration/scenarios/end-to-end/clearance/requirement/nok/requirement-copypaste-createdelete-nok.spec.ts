import { DatabaseUtils } from '../../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  HighLevelRequirementBuilder,
  RequirementBuilder,
  RequirementFolderBuilder,
  RequirementVersionBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { RequirementWorkspacePage } from '../../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import {
  assertCopyObjectAllowedAndPasteInAnotherProjectDisabledWithShortCut,
  assertCopyObjectAllowedAndPasteProjectDisabled,
  copyPasteInRequirementWorkspace,
} from '../../../../scenario-parts/requirement.part';
import { ConfirmInterProjectPaste } from '../../../../../page-objects/elements/dialog/confirm-inter-project-paste';

describe('Check if a user with Guest access cannot copy or paste elements in the Requirements workspace, but can paste elements in a project where they have project manager permissions', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07110.CT01 - requirement-copyypaste-createdelete-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    assertCopyObjectAllowedAndPasteProjectDisabled(
      requirementWorkspacePage,
      'Project 1',
      'Folder',
      'Project 1',
    );
    assertCopyObjectAllowedAndPasteProjectDisabled(
      requirementWorkspacePage,
      'Project 1',
      'Exi 2',
      'Project 1',
    );

    cy.log('Step 2');
    copyPasteInRequirementWorkspace(
      requirementWorkspacePage,
      'Project 1',
      'Folder',
      'Project 3',
      false,
    );
    const confirmDialog = new ConfirmInterProjectPaste('confirm-inter-project-paste');
    confirmDialog.clickOnConfirmButton();
    assertCopyObjectAllowedAndPasteInAnotherProjectDisabledWithShortCut(
      requirementWorkspacePage,
      'Project 1',
      'Folder',
      'Project 2',
    );
    assertCopyObjectAllowedAndPasteInAnotherProjectDisabledWithShortCut(
      requirementWorkspacePage,
      'Project 1',
      'Haute Exi',
      'Project 2',
    );
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProjects([
      new ProjectBuilder('Project 1').withRequirementLibraryNodes([
        new RequirementFolderBuilder('Folder'),
        new HighLevelRequirementBuilder('Haute Exi'),
        new RequirementBuilder('Exi').withVersions([
          new RequirementVersionBuilder('Exi 1'),
          new RequirementVersionBuilder('Exi 2'),
        ]),
      ]),
      new ProjectBuilder('Project 2'),
      new ProjectBuilder('Project 3'),
    ])
    .build();
}

function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1, 2]);
  addClearanceToUser(2, SystemProfile.PROJECT_MANAGER, [3]);
}
