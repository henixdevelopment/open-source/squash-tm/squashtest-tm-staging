import { DatabaseUtils } from '../../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  HighLevelRequirementBuilder,
  RequirementBuilder,
  RequirementFolderBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { RequirementWorkspacePage } from '../../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import {
  assertNodeExistsInRequirementWorkspaceProject,
  selectNodeInAnotherNode,
} from '../../../../scenario-parts/requirement.part';
import { KeyboardShortcuts } from '../../../../../page-objects/elements/workspace-common/keyboard-shortcuts';
import { RemoveDatasetDialog } from '../../../../../page-objects/pages/test-case-workspace/dialogs/remove-dataset-dialog.element';

describe('Check if a user with Guest cannot delete an item in the tree using the [Delete] icon from the library and [Delete] key on the keyboard', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07211.CT04 - requirement-delete-createdelete-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    const keyboardShortcut = new KeyboardShortcuts();
    const confirmDeleteDialog = new RemoveDatasetDialog();
    checkCannotDeleteFolderWithIconAndKeyboardShortcut(
      requirementWorkspacePage,
      keyboardShortcut,
      confirmDeleteDialog,
    );

    cy.log('Step 2');
    checkCannotDeleteRequirementWithIconAndKeyboardShortcut(
      requirementWorkspacePage,
      keyboardShortcut,
      confirmDeleteDialog,
    );
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProjects([
      new ProjectBuilder('Project').withRequirementLibraryNodes([
        new RequirementFolderBuilder('Folder 1'),
        new RequirementFolderBuilder('Folder 2'),
        new RequirementBuilder('Exg'),
        new HighLevelRequirementBuilder('High Req'),
      ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}
function checkCannotDeleteFolderWithIconAndKeyboardShortcut(
  requirementWorkspacePage: RequirementWorkspacePage,
  keyboardShortcut: KeyboardShortcuts,
  confirmDeleteDialog: RemoveDatasetDialog,
) {
  selectNodeInAnotherNode(requirementWorkspacePage, 'Project', 'Folder 1');
  requirementWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
  selectNodeInAnotherNode(requirementWorkspacePage, 'Project', 'Folder 2');
  keyboardShortcut.pressDelete();
  confirmDeleteDialog.assertNotExist();
  assertNodeExistsInRequirementWorkspaceProject(requirementWorkspacePage, 'Project', 'Folder 2');
}
function checkCannotDeleteRequirementWithIconAndKeyboardShortcut(
  requirementWorkspacePage: RequirementWorkspacePage,
  keyboardShortcut: KeyboardShortcuts,
  confirmDeleteDialog: RemoveDatasetDialog,
) {
  selectNodeInAnotherNode(requirementWorkspacePage, 'Project', 'Exg');
  requirementWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
  selectNodeInAnotherNode(requirementWorkspacePage, 'Project', 'High Req');
  keyboardShortcut.pressDelete();
  confirmDeleteDialog.assertNotExist();
  assertNodeExistsInRequirementWorkspaceProject(requirementWorkspacePage, 'Project', 'High Req');
}
