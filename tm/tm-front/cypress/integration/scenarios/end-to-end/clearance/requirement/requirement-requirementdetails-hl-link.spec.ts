import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  HighLevelRequirementBuilder,
  RequirementBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  KeywordTestCaseBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { DatabaseUtils } from '../../../../utils/database.utils';
import { RequirementVersionsLinkToVerifyingTestCaseBuilder } from '../../../../utils/end-to-end/prerequisite/builders/link-builders';
import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { HighLevelRequirementViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/high-level-requirement-view.page';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  changeLinkTypeOfRequirementInVersionPage,
  linkRequirementFromLowLevelRequirementInHighLevelRequirement,
  linkRequirementToRequirementInSecondLevelPage,
  linkTestCaseToRequirementFromLinkTestCasePageInVersionPage,
  linkTestCaseToRequirementVersion,
  unlinkRequirementFromLowLevelRequirementInVersionPage,
  unlinkRequirementInVersionPage,
  unlinkTestCasesFromRequirementVersionPage,
  unlinkTestCasesFromRequirementVersionPageWithTopIcon,
} from '../../../scenario-parts/requirement.part';
import { RequirementSecondLevelPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-second-level.page';
import { RemoveRequirementVersionLinksDialogElement } from '../../../../page-objects/pages/requirement-workspace/dialogs/remove-requirement-version-links-dialog.element';
import { UnbindLowLevelRequirementDialogElement } from '../../../../page-objects/pages/requirement-workspace/dialogs/unbind-low-level-requirement-dialog.element';
import { RequirementVersionViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-view.page';
import { RemoveVerifyingTestCasesDialogElement } from '../../../../page-objects/pages/test-case-workspace/dialogs/remove-verifying-test-cases-dialog.element';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can link test-cases, requirements and high level requirements to high level requirement in Second Level Page', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07012.CT04 - requirement-requirementdetails-hl-link', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    RequirementWorkspacePage.initTestAtPage();
    RequirementSecondLevelPage.navigateToRequirementSecondLevelPage(1);
    const highLevelRequirementViewPage = new HighLevelRequirementViewPage(2, 1);
    const highLevelRequirementSecondLevelViewPage = highLevelRequirementViewPage.currentVersion;
    const removeRequirementLinkDialog = new RemoveRequirementVersionLinksDialogElement(1, [2]);
    const unbindLowLevelRequirementDialog = new UnbindLowLevelRequirementDialogElement(1, [3]);
    const removeLinkTestCaseDialog = new RemoveVerifyingTestCasesDialogElement(1, [1, 2, 3]);

    linkRequirementToRequirementInSecondLevelPage('Jaffar', 'AZERTY', 'requirement');
    linkRequirementFromLowLevelRequirementInHighLevelRequirement(
      highLevelRequirementViewPage,
      highLevelRequirementViewPage.linkedLowLevelRequirementTable,
      'AZERTY',
      'wow',
      1,
    );

    cy.log('Step 2');
    changeLinkTypeOfRequirementInVersionPage(
      highLevelRequirementSecondLevelViewPage,
      'Jaffar',
      'Enfant - Parent',
      'Parent',
    );

    cy.log('Step 3');
    unlinkRequirementInVersionPage(
      highLevelRequirementSecondLevelViewPage,
      removeRequirementLinkDialog,
      'Jaffar',
      1,
      0,
    );
    unlinkRequirementFromLowLevelRequirementInVersionPage(
      highLevelRequirementViewPage,
      unbindLowLevelRequirementDialog,
      'wow',
      1,
      0,
    );

    cy.log('Step 4');
    associateTestCaseToRequirementInSecondLevelPage(highLevelRequirementSecondLevelViewPage);
    associateTestCaseToRequirementFromLinkTestCasePage(highLevelRequirementSecondLevelViewPage);

    cy.log('Step 5');
    dissociateTestCasesFromRequirementInSecondLevelPage(
      highLevelRequirementSecondLevelViewPage,
      removeLinkTestCaseDialog,
    );
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('requirement linker', [
        ProfileBuilderPermission.REQUIREMENT_READ,
        ProfileBuilderPermission.REQUIREMENT_LINK,
        ProfileBuilderPermission.TEST_CASE_READ,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('AZERTY')
        .withRequirementLibraryNodes([
          new HighLevelRequirementBuilder('Lala'),
          new HighLevelRequirementBuilder('Jaffar'),
          new RequirementBuilder('wow'),
        ])
        .withLinks([new RequirementVersionsLinkToVerifyingTestCaseBuilder(['Lala'], 'Wolf')])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Wolf'),
          new TestCaseBuilder('Miel'),
          new KeywordTestCaseBuilder('Hello'),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}
function associateTestCaseToRequirementInSecondLevelPage(
  highLevelRequirementSecondLevelViewPage: RequirementVersionViewPage,
) {
  linkTestCaseToRequirementVersion(
    highLevelRequirementSecondLevelViewPage,
    highLevelRequirementSecondLevelViewPage.verifyingTestCaseTable,
    'AZERTY',
    'Hello',
    1,
  );
  highLevelRequirementSecondLevelViewPage.verifyingTestCaseTable
    .findRowId('name', 'Hello')
    .then((idTestCase) => {
      highLevelRequirementSecondLevelViewPage.verifyingTestCaseTable.assertRowExist(idTestCase);
    });
  highLevelRequirementSecondLevelViewPage.verifyingTestCaseTable
    .findRowId('name', 'Wolf')
    .then((idTestCase) => {
      highLevelRequirementSecondLevelViewPage.verifyingTestCaseTable.assertRowExist(idTestCase);
    });
}

function associateTestCaseToRequirementFromLinkTestCasePage(
  highLevelRequirementSecondLevelViewPage: RequirementVersionViewPage,
) {
  linkTestCaseToRequirementFromLinkTestCasePageInVersionPage(
    highLevelRequirementSecondLevelViewPage,
    'Miel',
  );
  RequirementSecondLevelPage.navigateToRequirementSecondLevelPage(1);
  highLevelRequirementSecondLevelViewPage.verifyingTestCaseTable.assertRowCount(3);
  highLevelRequirementSecondLevelViewPage.verifyingTestCaseTable
    .findRowId('name', 'Miel')
    .then((idTestCase) => {
      highLevelRequirementSecondLevelViewPage.verifyingTestCaseTable.assertRowExist(idTestCase);
    });
}

function dissociateTestCasesFromRequirementInSecondLevelPage(
  highLevelRequirementSecondLevelPage: RequirementVersionViewPage,
  removeTestCaseLink: RemoveVerifyingTestCasesDialogElement,
) {
  unlinkTestCasesFromRequirementVersionPage(
    highLevelRequirementSecondLevelPage,
    removeTestCaseLink,
    highLevelRequirementSecondLevelPage.verifyingTestCaseTable,
    'Hello',
    1,
    2,
  );
  unlinkTestCasesFromRequirementVersionPageWithTopIcon(
    highLevelRequirementSecondLevelPage,
    removeTestCaseLink,
    highLevelRequirementSecondLevelPage.verifyingTestCaseTable,
    'Miel',
    1,
    2,
    1,
  );
  highLevelRequirementSecondLevelPage.verifyingTestCaseTable
    .findRowId('name', 'Wolf')
    .then((idTestCase) => {
      highLevelRequirementSecondLevelPage.verifyingTestCaseTable.assertRowExist(idTestCase);
    });
}
