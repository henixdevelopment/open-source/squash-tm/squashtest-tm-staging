import { DatabaseUtils } from '../../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  HighLevelRequirementBuilder,
  RequirementBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  RequirementsLinkToHighLevelRequirementBuilder,
  RequirementsVersionLinkBuilder,
  TestCasesLinkToVerifiedRequirementVersionBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/link-builders';
import { TestCaseBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { RequirementWorkspacePage } from '../../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import {
  selectRequirementAndAssertButtonLinkToHighRequirementIsNotPresent,
  verifyButtonsToAddOrRemoveRelationToAHighLevelRequirementAreNotPresentInRequirement,
  verifyCannotAssociateRequirements,
  verifyCannotAssociateTestCases,
  verifyCannotDisassociateLinkedRequirementInRequirement,
  verifyCannotDisassociateTestCase,
  verifyLinkCannotBeModified,
} from '../../../../scenario-parts/requirement.part';
import { RequirementViewPage } from '../../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';

describe('Check that a guest user can not modify a link', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07212.CT01 - requirement-requirementview-link-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    const requirementViewPage = new RequirementViewPage();

    verifyCannotAssociateRequirements(requirementWorkspacePage, 'requirement', 'Project', 'Exig');

    cy.log('Step 2');
    verifyLinkCannotBeModified('high-level-requirement', 'name', 'High Req');

    cy.log('Step 3');
    assertGuestCanNotDissociateLinkedRequirement(requirementViewPage);
    selectRequirementAndAssertButtonLinkToHighRequirementIsNotPresent(
      requirementWorkspacePage,
      'Project',
      'Hello',
    );

    cy.log('Step 4');
    verifyCannotAssociateTestCases('requirement');

    cy.log('Step 5');
    assertGuestCanNotDissociateATestCase(requirementViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProjects([
      new ProjectBuilder('Project')
        .withRequirementLibraryNodes([
          new HighLevelRequirementBuilder('High Req'),
          new RequirementBuilder('Exig'),
          new RequirementBuilder('Hello'),
        ])
        .withTestCaseLibraryNodes([new TestCaseBuilder('Pomme')])
        .withLinks([
          new RequirementsLinkToHighLevelRequirementBuilder(['Exig'], 'High Req'),
          new RequirementsVersionLinkBuilder('High Req', 'Exig'),
          new TestCasesLinkToVerifiedRequirementVersionBuilder(['Pomme'], 'Hello'),
        ]),
    ])
    .build();
}

function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}

function assertGuestCanNotDissociateLinkedRequirement(requirementViewPage: RequirementViewPage) {
  verifyCannotDisassociateLinkedRequirementInRequirement('requirement', 'High Req');
  requirementViewPage.currentVersion.requirementLinkTable
    .findRowId('name', 'High Req')
    .then((requirementId) => {
      requirementViewPage.currentVersion.requirementLinkTable.getRow(requirementId).assertExists();
    });
  verifyButtonsToAddOrRemoveRelationToAHighLevelRequirementAreNotPresentInRequirement();
}

function assertGuestCanNotDissociateATestCase(requirementViewPage: RequirementViewPage) {
  verifyCannotDisassociateTestCase('requirement', 'Pomme');
  requirementViewPage.currentVersion.verifyingTestCaseTable
    .findRowId('name', 'Pomme')
    .then((idTestCase) => {
      requirementViewPage.currentVersion.verifyingTestCaseTable.getRow(idTestCase).assertExists();
    });
}
