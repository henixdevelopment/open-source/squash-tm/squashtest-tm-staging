import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { TreeToolbarElement } from '../../../../page-objects/elements/workspace-common/tree-toolbar.element';
import { RequirementSearchPage } from '../../../../page-objects/pages/requirement-workspace/search/requirement-search-page';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import { ProjectScopePage } from '../../../../page-objects/pages/requirement-workspace/search/ProjectScopePage';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  HighLevelRequirementBuilder,
  RequirementBuilder,
  RequirementFolderBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  bindMilestoneInSearchPage,
  goToRequirementSearchPage,
  massEditInRequirementSearchPage,
  modifyFieldAttributesWithListInSearchRequirementPage,
  modifyFieldAttributeWithTextFieldInSearchRequirementPage,
  selectObjectAsPerimeterInSearchRequirementPage,
} from '../../../scenario-parts/requirement.part';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  MilestoneBuilder,
  MilestoneOnProjectBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/milestone-prerequisite-builders';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Verify read/write authorization in the requirement search page', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07010.CT05 - requirement-search-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    const treeToolbarElement = new TreeToolbarElement('requirement-toolbar');
    const requirementSearchPage = new RequirementSearchPage(new GridElement('requirement-search'));
    const projectScope = new ProjectScopePage();
    goToRequirementSearchPage(requirementWorkspacePage, treeToolbarElement);

    selectFolderAsPerimeterAndModifySearchCriteria(requirementSearchPage, projectScope);

    cy.log('Step2');
    modifyFieldAttributesWithListInSearchRequirementPage(
      requirementSearchPage,
      'Chronos',
      'category',
      'Métier',
    );
    massEditInRequirementSearchPage(requirementSearchPage, 'Chronos', 'status', 'À approuver');

    cy.log('Step3');
    modifyFieldAttributeWithTextFieldInSearchRequirementPage(
      requirementSearchPage,
      'Chronos',
      'name',
      'Sukuna',
    );
    modifyFieldAttributeWithTextFieldInSearchRequirementPage(
      requirementSearchPage,
      'Chronos',
      'reference',
      'Itadori',
    );

    cy.log('Step4');
    bindMilestoneInSearchPage(requirementSearchPage, 'Chronos', 'Nemesis');
    bindMilestoneInSearchPage(requirementSearchPage, 'Chronos', 'Nemesis');
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('requirement modifier', [
        ProfileBuilderPermission.REQUIREMENT_READ,
        ProfileBuilderPermission.REQUIREMENT_WRITE,
      ]),
    ])
    .withMilestones([new MilestoneBuilder('Nemesis', '2025-10-31 14:44:45').withUserLogin('RonW')])
    .withProjects([
      new ProjectBuilder('Chronos')
        .withRequirementLibraryNodes([
          new RequirementFolderBuilder('Folder').withRequirementLibraryNodes([
            new RequirementBuilder('Exi1').withCriticality('MINOR'),
            new HighLevelRequirementBuilder('EHN').withCriticality('MAJOR'),
          ]),
        ])
        .withMilestonesOnProject([new MilestoneOnProjectBuilder('Nemesis')]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}
function selectFolderAsPerimeterAndModifySearchCriteria(
  requirementSearchPage: RequirementSearchPage,
  projectScope: ProjectScopePage,
) {
  selectObjectAsPerimeterInSearchRequirementPage(
    requirementSearchPage,
    projectScope,
    'Chronos',
    'Folder',
  );
  requirementSearchPage.checkbox.findAndClick('span.ant-checkbox-inner');
  requirementSearchPage.addCriteriaCriticityandLevelOfCriticity('Mineur');
  requirementSearchPage.clickSearchButton();
}
