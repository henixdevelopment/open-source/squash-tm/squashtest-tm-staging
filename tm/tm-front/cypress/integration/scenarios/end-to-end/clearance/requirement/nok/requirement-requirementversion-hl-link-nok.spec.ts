import { DatabaseUtils } from '../../../../../utils/database.utils';
import { RequirementWorkspacePage } from '../../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import {
  verifyCannotAssociateRequirements,
  verifyCannotAssociateTestCases,
  verifyCannotDisassociateLinkedRequirementInRequirement,
  verifyCannotDisassociateLinkedRequirementsInLowLevelRequirementTableOfAHighLevelRequirement,
  verifyCannotDisassociateTestCase,
  verifyLinkCannotBeModified,
} from '../../../../scenario-parts/requirement.part';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  HighLevelRequirementBuilder,
  RequirementBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import { TestCaseBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  RequirementsLinkToHighLevelRequirementBuilder,
  RequirementsVersionLinkBuilder,
  TestCasesLinkToVerifiedRequirementVersionBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/link-builders';
import { addVersionToRequirement } from '../../../../../utils/end-to-end/api/requirement-versions/create-requirement-version';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { HighLevelRequirementVersionPage } from '../../../../../page-objects/pages/requirement-workspace/requirement/high-level-requirement-version.page';

describe('Check if a user with Guest cannot Associate/Dissociate requirements with each other, Modify the type of association and Associate/Dissociate test cases', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
    createVersionToRequirement();
  });

  it('F01-UC07112.CT02 - requirement-requirementview-hl-link-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    const highLevelRequirementVersionPage = new HighLevelRequirementVersionPage(1);
    verifyCannotAssociateRequirements(
      requirementWorkspacePage,
      'high-level-requirement',
      'Project',
      'haut Exig',
    );

    cy.log('Step 2');
    verifyLinkCannotBeModified('high-level-requirement', 'name', 'Exig');

    cy.log('Step 3');
    assertGuestCanNotDissociateLinkedRequirement(highLevelRequirementVersionPage);

    cy.log('Step 4');
    verifyCannotAssociateTestCases('high-level-requirement');

    cy.log('Step 5');
    verifyCannotDisassociateTestCase('high-level-requirement', 'Test');
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProjects([
      new ProjectBuilder('Project')
        .withRequirementLibraryNodes([
          new HighLevelRequirementBuilder('haut Exig'),
          new RequirementBuilder('Exig'),
        ])
        .withTestCaseLibraryNodes([new TestCaseBuilder('Test')])
        .withLinks([
          new RequirementsLinkToHighLevelRequirementBuilder(['Exig'], 'haut Exig'),
          new TestCasesLinkToVerifiedRequirementVersionBuilder(['Test'], 'Exig'),
          new RequirementsVersionLinkBuilder('haut Exig', 'Exig'),
        ]),
    ])
    .build();
}
function createVersionToRequirement() {
  addVersionToRequirement(1, true, true);
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}
function assertGuestCanNotDissociateLinkedRequirement(
  highLevelRequirementViewPage: HighLevelRequirementVersionPage,
) {
  verifyCannotDisassociateLinkedRequirementInRequirement('high-level-requirement', 'Exig');
  verifyCannotDisassociateLinkedRequirementsInLowLevelRequirementTableOfAHighLevelRequirement(
    'Exig',
  );
  highLevelRequirementViewPage.requirementLinkTable
    .findRowId('name', 'Exig')
    .then((requirementId) => {
      highLevelRequirementViewPage.requirementLinkTable.getRow(requirementId).assertExists();
    });
}
