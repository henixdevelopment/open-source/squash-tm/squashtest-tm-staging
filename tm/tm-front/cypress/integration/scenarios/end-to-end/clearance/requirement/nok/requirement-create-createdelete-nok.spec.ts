import { DatabaseUtils } from '../../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  HighLevelRequirementBuilder,
  RequirementBuilder,
  RequirementVersionBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { TestCaseBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { RequirementVersionsLinkToVerifyingTestCaseBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/link-builders';
import { RequirementWorkspacePage } from '../../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import {
  selectNodeInAnotherNode,
  selectNodeProject,
} from '../../../../scenario-parts/requirement.part';
import { MenuElement } from '../../../../../utils/menu.element';
import { HighLevelRequirementViewPage } from '../../../../../page-objects/pages/requirement-workspace/requirement/high-level-requirement-view.page';
import { ToolbarButtonElement } from '../../../../../page-objects/elements/workspace-common/toolbar.element';
import { TestCaseWorkspacePage } from '../../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { selectNodeInTestCaseWorkspace } from '../../../../scenario-parts/testcase.part';
import { TestCaseViewPage } from '../../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { RequirementSecondLevelPage } from '../../../../../page-objects/pages/requirement-workspace/requirement/requirement-second-level.page';
import { selectByDataTestToolbarButtonId } from '../../../../../utils/basic-selectors';
import { RequirementViewPage } from '../../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import { NavBarElement } from '../../../../../page-objects/elements/nav-bar/nav-bar.element';

describe('Check if a user with Guest cannot create folders or requirements, nor transform requirements in the Requirements workspace', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07110.CT01 - requirement-create-createdelete-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    const requirementSecondLevelPage = new RequirementSecondLevelPage();
    assertCanNotCreateFolderAndRequirement(requirementWorkspacePage);

    cy.log('Step 2');
    assertCanNotAddNewVersionAndChangeHighRequirementToRequirement(requirementWorkspacePage);

    cy.log('Step 3');
    assertCanNotConvertRequirementToHighRequirementFromRequirementSecondLevelPage(
      requirementSecondLevelPage,
    );

    cy.log('Step 4');
    assertCanNotConvertRequirementToHighRequirementFromRequirementMultiVersionPage();
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProjects([
      new ProjectBuilder('Project')
        .withRequirementLibraryNodes([
          new HighLevelRequirementBuilder('Haute Exig'),
          new RequirementBuilder('Exig').withVersions([
            new RequirementVersionBuilder('Exig 1'),
            new RequirementVersionBuilder('Exig 2'),
          ]),
        ])
        .withTestCaseLibraryNodes([new TestCaseBuilder('Test')])
        .withLinks([new RequirementVersionsLinkToVerifyingTestCaseBuilder(['Exig'], 'Test')]),
    ])
    .build();
}

function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}

function assertCanNotCreateFolderAndRequirement(
  requirementWorkspacePage: RequirementWorkspacePage,
) {
  selectNodeProject(requirementWorkspacePage, 'Project');
  requirementWorkspacePage.assertCreateButtonIsGreyed();
  requirementWorkspacePage.clickOnCreateButton();
  const createMenu = new MenuElement('create-menu');
  createMenu.assertExists();
  createMenu.item('new-requirement').assertDisabled();
  createMenu.item('new-folder').assertDisabled();
}

function assertCanNotAddNewVersionAndChangeHighRequirementToRequirement(
  requirementWorkspacePage: RequirementWorkspacePage,
) {
  selectNodeInAnotherNode(requirementWorkspacePage, 'Project', 'Haute Exig');
  const highLevelRequirementViewPage = new HighLevelRequirementViewPage('*', '*');
  highLevelRequirementViewPage.assertExists();
  const toolbarButton = new ToolbarButtonElement('', 'action-menu-button');
  toolbarButton.click();
  const actionMenu = new MenuElement('action-menu');
  actionMenu.item('create-new-requirement-version').assertDisabled();
  actionMenu.item('convert-to-standard-requirement').assertDisabled();
}

function assertCanNotConvertRequirementToHighRequirementFromRequirementSecondLevelPage(
  requirementSecondLevelPage: RequirementSecondLevelPage,
) {
  const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage();
  selectNodeInTestCaseWorkspace(testCaseWorkspacePage, 'Project', 'Test');
  const testCaseViewPage = new TestCaseViewPage(1);
  testCaseViewPage.clickVerifiedRequirementsAnchorLink();
  testCaseViewPage.coveragesTable.findRowId('projectName', 'Project').then((id) => {
    testCaseViewPage.coveragesTable.getCell(id, 'name').linkRenderer().findCellLink().click();
  });
  requirementSecondLevelPage.assertExists();
  requirementSecondLevelPage.find(selectByDataTestToolbarButtonId('action-menu-button')).click();
  const actionMenu = new MenuElement('action-menu');
  actionMenu.item('convert-to-high-level-requirement').assertDisabled();
}

function assertCanNotConvertRequirementToHighRequirementFromRequirementMultiVersionPage() {
  const requirementWorkspacePage = NavBarElement.navigateToRequirementWorkspace();
  selectNodeInAnotherNode(requirementWorkspacePage, 'Project', 'Exig 2');
  const requirementViewPage = new RequirementViewPage();
  requirementViewPage.assertExists();
  const requirementVersionViewInformationPage =
    requirementViewPage.currentVersion.clickInformationAnchorLink();
  const requirementMultiVersionPage = requirementVersionViewInformationPage.clickOnVersionLink();
  requirementMultiVersionPage.assertExists();
  requirementMultiVersionPage.grid.findRowId('name', '1').then((requirementVersionId) => {
    requirementMultiVersionPage.grid.selectRow(requirementVersionId, '#', 'leftViewport');
  });
  requirementMultiVersionPage.find(selectByDataTestToolbarButtonId('action-menu-button')).click();
  const actionMenu = new MenuElement('action-menu');
  actionMenu.item('convert-to-high-level-requirement').assertDisabled();
}
