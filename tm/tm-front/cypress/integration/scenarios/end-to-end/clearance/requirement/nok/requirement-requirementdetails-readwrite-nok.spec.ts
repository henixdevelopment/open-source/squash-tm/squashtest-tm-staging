import { DatabaseUtils } from '../../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  HighLevelRequirementBuilder,
  RequirementBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import { BindableEntity } from '../../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import {
  MilestoneBuilder,
  MilestoneOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/milestone-prerequisite-builders';
import { RequirementWorkspacePage } from '../../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { RequirementViewPage } from '../../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import { RequirementSecondLevelPage } from '../../../../../page-objects/pages/requirement-workspace/requirement/requirement-second-level.page';
import { RequirementVersionViewInformationPage } from '../../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-view-information.page';
import { SystemE2eCommands } from '../../../../../page-objects/scenarios-parts/system/system_e2e_commands';

describe('Check if a user with Guest access can view and print the level 2 page of a high-level requirement, but cannot (Modify attributes of a high-level requirement, Rename or change the reference, Associate or dissociate milestones', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07210.CT03 - requirement-requirementdetails-readwrite-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    const requirementSecondLevelPage = new RequirementSecondLevelPage();
    const requirementVersionInformationViewPage = new RequirementVersionViewInformationPage();
    verifyUserCanViewHighRequirementLevelTwoPage(
      requirementWorkspacePage,
      requirementSecondLevelPage,
      requirementVersionInformationViewPage,
    );

    cy.log('Step 2');
    requirementSecondLevelPage.assertIconNotExists('sqtm-core-generic:more');

    cy.log('Step 3');
    verifyUserCanNotModifyAttributes(
      requirementSecondLevelPage,
      requirementVersionInformationViewPage,
    );

    cy.log('Step 4');
    verifyUserCanNotRenameOrModifyFields(requirementSecondLevelPage);

    cy.log('Step 5');
    const tagMilestone = requirementVersionInformationViewPage.milestoneTagElement;
    tagMilestone.openMilestoneDialogNotExist();
    tagMilestone.removeMilestoneButtonShouldNotExist('Estivale');
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withCufs([new CustomFieldBuilder('Text', InputType.RICH_TEXT)])
    .withMilestones([
      new MilestoneBuilder('Estivale', '2025-10-31 14:44:45').withUserLogin('RonW'),
      new MilestoneBuilder('Hivernale', '2027-02-11 16:00:00').withUserLogin('RonW'),
    ])
    .withProjects([
      new ProjectBuilder('Project')
        .withRequirementLibraryNodes([
          new HighLevelRequirementBuilder('highReq')
            .withMilestones(['Estivale'])
            .withRequirements([new RequirementBuilder('Exi 1'), new RequirementBuilder('Exi 2')]),
        ])
        .withCufsOnProject([
          new CustomFieldOnProjectBuilder('Text', BindableEntity.REQUIREMENT_VERSION),
        ])
        .withMilestonesOnProject([
          new MilestoneOnProjectBuilder('Estivale'),
          new MilestoneOnProjectBuilder('Hivernale'),
        ]),
    ])
    .build();
}

function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}

function verifyUserCanViewHighRequirementLevelTwoPage(
  requirementWorkspacePage: RequirementWorkspacePage,
  requirementSecondLevelPage: RequirementSecondLevelPage,
  requirementVersionInformationViewPage: RequirementVersionViewInformationPage,
) {
  const requirementViewPage = new RequirementViewPage();
  requirementWorkspacePage.assertExists();
  requirementWorkspacePage.tree.findRowId('NAME', 'Project').then((requirementId) => {
    requirementWorkspacePage.tree.selectRow(requirementId, 'NAME');
    requirementWorkspacePage.tree.openNodeIfClosed(requirementId);
  });
  requirementWorkspacePage.tree.findRowId('NAME', 'highReq').then((requirementId) => {
    requirementWorkspacePage.tree.openNode(requirementId);
    requirementWorkspacePage.tree.selectNodeByName('Exi 1');
  });
  requirementViewPage.assertExists();
  requirementVersionInformationViewPage.clickOnHighLevelRequirementName();
  requirementSecondLevelPage.assertExists();
}

function verifyUserCanNotModifyAttributes(
  requirementSecondLevelPage: RequirementSecondLevelPage,
  requirementVersionInformationViewPage: RequirementVersionViewInformationPage,
) {
  requirementSecondLevelPage.statusCapsule.assertIsNotClickable();
  requirementVersionInformationViewPage.statusField.assertIsNotEditable();
  requirementSecondLevelPage.getRichTextCustomField('Text').assertIsNotEditable();
}

function verifyUserCanNotRenameOrModifyFields(
  requirementSecondLevelPage: RequirementSecondLevelPage,
) {
  requirementSecondLevelPage.nameTextField.assertIsNotEditable();
  requirementSecondLevelPage.referenceTextField.assertIsNotEditable();
}
