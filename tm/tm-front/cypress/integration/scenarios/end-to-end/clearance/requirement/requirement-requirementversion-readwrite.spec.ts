import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import {
  HighLevelRequirementBuilder,
  RequirementBuilder,
  RequirementVersionBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { RequirementViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import { RequirementMultiVersionPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-multi-version.page';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import { RequirementVersionDetailViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-detail-view.page';
import {
  RequirementVersionDetailViewInformationPage,
  RequirementVersionViewInformationPage,
} from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-view-information.page';
import {
  accessToSecondVersionOfTheRequirement,
  assertNameAndReferenceOfRequirementCanBeChanged,
  modifyCapsuleStatusAndFieldCategory,
  selectNodeInAnotherNode,
} from '../../../scenario-parts/requirement.part';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  MilestoneBuilder,
  MilestoneOnProjectBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/milestone-prerequisite-builders';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Verify read/write authorization in the requirement view for a high level requirement', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07010.CT04 - requirement-requirementversion-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    const requirementViewPage = new RequirementViewPage();
    const grid = new GridElement('requirement-versions-grid');
    const requirementMultiVersionPage = new RequirementMultiVersionPage(grid);
    const requirementVersionViewDetails = new RequirementVersionDetailViewPage();
    const requirementVersionDetailViewInformationPage =
      new RequirementVersionDetailViewInformationPage();

    selectNodeInAnotherNode(requirementWorkspacePage, 'Chronos', 'Mêlichios');
    const requirementVersionViewInformationPage =
      requirementViewPage.currentVersion.clickInformationAnchorLink();
    accessToSecondVersionOfTheRequirement(
      requirementVersionViewInformationPage,
      requirementMultiVersionPage,
      requirementVersionViewDetails,
      'Mêlichios',
    );

    cy.log('Step 2');
    requirementVersionViewDetails.clickActionMenuAndAssertPrintModeExists();

    cy.log('Step 3');
    modifyRequirementViewInformationPage(
      requirementViewPage,
      requirementVersionDetailViewInformationPage,
    );
    requirementMultiVersionPage.clickOnButton('back');

    cy.log('Step 4');

    selectNodeInAnotherNode(requirementWorkspacePage, 'Chronos', 'Zeus');
    accessToSecondVersionOfTheRequirement(
      requirementVersionViewInformationPage,
      requirementMultiVersionPage,
      requirementVersionViewDetails,
      'Zeus',
    );
    requirementVersionViewDetails.assertElementsPageAreFullyLoaded();
    assertNameAndReferenceOfRequirementCanBeChanged(requirementViewPage, 'Poseidon', 'Demeter');

    cy.log('Step 5');
    bindWithMilestone(requirementVersionViewInformationPage);
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('requirement modifier', [
        ProfileBuilderPermission.REQUIREMENT_READ,
        ProfileBuilderPermission.REQUIREMENT_WRITE,
        ProfileBuilderPermission.REQUIREMENT_EXPORT,
      ]),
    ])
    .withCufs([new CustomFieldBuilder('CufDate', InputType.DATE_PICKER)])
    .withMilestones([new MilestoneBuilder('Mars', '2025-10-31 14:44:45').withUserLogin('RonW')])
    .withProjects([
      new ProjectBuilder('Chronos')
        .withRequirementLibraryNodes([
          new HighLevelRequirementBuilder('Jupiter').withVersions([
            new RequirementVersionBuilder('Hera'),
            new RequirementVersionBuilder('Zeus'),
          ]),
          new RequirementBuilder('CapriSun').withVersions([
            new RequirementVersionBuilder('Mêlichios'),
          ]),
        ])
        .withCufsOnProject([
          new CustomFieldOnProjectBuilder('CufDate', BindableEntity.REQUIREMENT_VERSION),
        ])
        .withMilestonesOnProject([new MilestoneOnProjectBuilder('Mars')]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}
function modifyRequirementViewInformationPage(
  requirementViewPage: RequirementViewPage,
  requirementVersionViewInformationPage: RequirementVersionDetailViewInformationPage,
) {
  modifyCapsuleStatusAndFieldCategory(
    requirementViewPage,
    requirementVersionViewInformationPage,
    'À approuver',
    'Ergonomique',
  );
  requirementVersionViewInformationPage.addCufDate('CufDate', '17/02/2058');
}

function bindWithMilestone(
  requirementVersionInformationViewPage: RequirementVersionViewInformationPage,
) {
  requirementVersionInformationViewPage.bindMilestone('Mars');
  requirementVersionInformationViewPage.milestoneTagElement.assertContainsText('Mars');
  requirementVersionInformationViewPage.milestoneTagElement.clickOnRemoveMilestoneClose('Mars');
}
