import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { TestCaseBuilder } from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { HighLevelRequirementViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/high-level-requirement-view.page';
import { ToolbarButtonElement } from '../../../../page-objects/elements/workspace-common/toolbar.element';
import { MenuElement } from '../../../../utils/menu.element';
import { RequirementViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import { RequirementVersionViewInformationPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-view-information.page';
import { RequirementMultiVersionPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-multi-version.page';
import { TestCaseViewSubPage } from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-sub-page';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import {
  addNewVersionHighLevelRequirement,
  changeHighLevelRequirementToRequirement,
  changeRequirementToHighLevelRequirement,
  createObjectInRequirementWorkspace,
  linkTestCaseToRequirementVersion,
  selectNodeInAnotherNode,
  selectRequirementInMultiVersionPage,
} from '../../../scenario-parts/requirement.part';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Verify that a user can create all objects in the requirement workspace', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });
  it('  F01-UC07011.CT01 - requirement-create-createdelete', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    const highLevelRequirementViewPage = new HighLevelRequirementViewPage(1, 1);
    const toolbarButton = new ToolbarButtonElement('', 'action-menu-button');
    const actionMenu = new MenuElement('action-menu');
    const requirementViewPage = new RequirementViewPage();
    const requirementVersionViewPage = requirementViewPage.currentVersion;
    const grid = new GridElement('requirement-versions-grid');
    const requirementMultiVersionPage = new RequirementMultiVersionPage(grid);
    const testCaseTable = requirementViewPage.currentVersion.verifyingTestCaseTable;

    createFolderInRequirementWorkspace(requirementWorkspacePage);

    cy.log('Step 2');
    createRequirementInRequirementWorkspace(requirementWorkspacePage);
    createHighLevelRequirementInRequirementWorkspace(requirementWorkspacePage);

    cy.log('Step 3');
    const newVersionHighLevelRequirementInformationPage =
      highLevelRequirementViewPage.currentVersion.clickInformationAnchorLink();
    const requirementVersionViewInformationPage =
      requirementViewPage.currentVersion.clickInformationAnchorLink();

    addNewVersionHighLevelRequirement(
      highLevelRequirementViewPage,
      toolbarButton,
      actionMenu,
      newVersionHighLevelRequirementInformationPage,
      '2',
    );
    changeHighLevelRequirementToRequirement(
      highLevelRequirementViewPage,
      toolbarButton,
      actionMenu,
      requirementVersionViewInformationPage,
    );

    cy.log('Step 4');
    linkTestCaseToRequirementVersion(requirementVersionViewPage, testCaseTable, 'AZERTY', 'a', 4);
    convertToHighLevelRequirementInSecondViewPage(
      requirementViewPage,
      toolbarButton,
      actionMenu,
      requirementVersionViewInformationPage,
    );

    cy.log('Step 5');
    openVersionPageAndChangeRequirementToHighLevelRequirement(
      requirementMultiVersionPage,
      toolbarButton,
      actionMenu,
      requirementVersionViewInformationPage,
      requirementWorkspacePage,
    );
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('requirement creator', [
        ProfileBuilderPermission.REQUIREMENT_READ,
        ProfileBuilderPermission.REQUIREMENT_CREATE,
        ProfileBuilderPermission.REQUIREMENT_LINK,
        ProfileBuilderPermission.TEST_CASE_READ,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('AZERTY').withTestCaseLibraryNodes([new TestCaseBuilder('a')]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}
function createFolderInRequirementWorkspace(requirementWorkspacePage: RequirementWorkspacePage) {
  createObjectInRequirementWorkspace(
    requirementWorkspacePage,
    true,
    'AZERTY',
    'new-folder',
    'Dossier',
  );
  requirementWorkspacePage.tree.findRowId('NAME', 'Dossier').then((folderId) => {
    requirementWorkspacePage.tree.assertRowHasParent(folderId, 'RequirementLibrary-1');
  });
}

function createRequirementInRequirementWorkspace(
  requirementWorkspacePage: RequirementWorkspacePage,
) {
  createObjectInRequirementWorkspace(
    requirementWorkspacePage,
    true,
    'AZERTY',
    'new-requirement',
    'Exigence',
  );
  requirementWorkspacePage.tree.findRowId('NAME', 'Exigence').then((requirementId) => {
    requirementWorkspacePage.tree.assertRowHasParent(requirementId, 'RequirementLibrary-1');
  });
}

function createHighLevelRequirementInRequirementWorkspace(
  requirementWorkspacePage: RequirementWorkspacePage,
) {
  createObjectInRequirementWorkspace(
    requirementWorkspacePage,
    true,
    'AZERTY',
    'new-high-level-requirement',
    'Haut Niveau',
  );
  requirementWorkspacePage.tree.findRowId('NAME', 'Haut Niveau').then((highLevelId) => {
    requirementWorkspacePage.tree.assertRowHasParent(highLevelId, 'RequirementLibrary-1');
  });
}

function convertToHighLevelRequirementInSecondViewPage(
  requirementViewPage: RequirementViewPage,
  toolbarButton: ToolbarButtonElement,
  actionMenu: MenuElement,
  informationPage: RequirementVersionViewInformationPage,
) {
  requirementViewPage.find('sqtm-core-link-cell').click();
  const testCaseViewDetailsPage = new TestCaseViewSubPage(1);
  testCaseViewDetailsPage.find('sqtm-core-link-cell').click();
  changeRequirementToHighLevelRequirement(toolbarButton, actionMenu, informationPage);
}

function openVersionPageAndChangeRequirementToHighLevelRequirement(
  requirementMultiVersionPage: RequirementMultiVersionPage,
  toolbarButton: ToolbarButtonElement,
  actionMenu: MenuElement,
  informationPage: RequirementVersionViewInformationPage,
  requirementWorkspacePage: RequirementWorkspacePage,
) {
  selectNodeInAnotherNode(requirementWorkspacePage, 'AZERTY', 'Exigence');
  selectRequirementInMultiVersionPage(informationPage, requirementMultiVersionPage, 'Exigence');
  changeRequirementToHighLevelRequirement(toolbarButton, actionMenu, informationPage);
}
