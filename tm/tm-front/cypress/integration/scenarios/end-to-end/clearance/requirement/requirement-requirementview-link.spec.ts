import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import {
  KeywordTestCaseBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  HighLevelRequirementBuilder,
  RequirementBuilder,
  RequirementVersionBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { RequirementViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import { RequirementVersionViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-view.page';
import { RemoveRequirementVersionLinksDialogElement } from '../../../../page-objects/pages/requirement-workspace/dialogs/remove-requirement-version-links-dialog.element';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import {
  associateTestCaseToRequirementFromLinkTestCasePage,
  changeLinkTypeOfRequirementInVersionPage,
  linkRequirementAndHighLevelRequirementToAnyRequirement,
  linkTestCaseToRequirementVersion,
  unlinkRequirementInVersionPage,
  unlinkTestCasesFromRequirementVersionPage,
  unlinkTestCasesFromRequirementVersionPageWithTopIcon,
} from '../../../scenario-parts/requirement.part';
import { RemoveVerifyingTestCasesDialogElement } from '../../../../page-objects/pages/test-case-workspace/dialogs/remove-verifying-test-cases-dialog.element';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe(
  'Verify that a user can link requirements in the requirement-workspace, ' +
    'and change the nature of those links',
  function () {
    beforeEach(function () {
      Cypress.Cookies.debug(true);
      DatabaseUtils.cleanDatabase();
      SystemE2eCommands.setMilestoneActivated();
      initTestData();
      initPermissions();
    });

    it(' F01-UC07012.CT01 - requirement-requirementview-link', () => {
      cy.log('Step 1');
      cy.logInAs('RonW', 'admin');
      const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
      const requirementViewPage = new RequirementViewPage();
      const requirementVersionPage = requirementViewPage.currentVersion;
      const verifyingTestCaseTable = requirementVersionPage.verifyingTestCaseTable;
      const dialogDeleteAssociation = new RemoveRequirementVersionLinksDialogElement(1, [2, 3]);
      const removeVerifyingTestCaseDialog = new RemoveVerifyingTestCasesDialogElement(1, [1, 2]);

      linkRequirementAndHighLevelRequirementToAnyRequirement(
        requirementWorkspacePage,
        'AZERTY',
        'Lala',
        'requirement',
        'wow',
        'Frodo',
      );

      cy.log('Step 2');

      changeLinkTypeOfRequirementInVersionPage(
        requirementVersionPage,
        'wow',
        'Parent - Enfant',
        'Enfant',
      );

      cy.log('Step 3');

      unlinkRequirementFromRequirement(requirementVersionPage, dialogDeleteAssociation);

      cy.log('Step 4');

      linkTestCaseToRequirementVersion(
        requirementVersionPage,
        verifyingTestCaseTable,
        'AZERTY',
        'Wolf',
        1,
      );
      assertTestCaseCanBeLinkedToRequirementFromLinkTestCasePage(verifyingTestCaseTable);

      cy.log('Step 5');
      dissociateTestCasesFromRequirement(
        requirementVersionPage,
        removeVerifyingTestCaseDialog,
        verifyingTestCaseTable,
      );
    });
  },
);

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('requirement linker', [
        ProfileBuilderPermission.REQUIREMENT_READ,
        ProfileBuilderPermission.REQUIREMENT_LINK,
        ProfileBuilderPermission.TEST_CASE_READ,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('AZERTY')
        .withRequirementLibraryNodes([
          new RequirementBuilder('Lala'),
          new RequirementBuilder('wow'),
          new HighLevelRequirementBuilder('Jaffar').withVersions([
            new RequirementVersionBuilder('Frodo'),
          ]),
        ])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Wolf'),
          new KeywordTestCaseBuilder('Hello'),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}
function unlinkRequirementFromRequirement(
  requirementVersionPage: RequirementVersionViewPage,
  dialogDeleteAssociation: RemoveRequirementVersionLinksDialogElement,
) {
  unlinkRequirementInVersionPage(requirementVersionPage, dialogDeleteAssociation, 'wow', 1, 1);
  requirementVersionPage.requirementLinkTable.findRowId('name', 'Frodo').then((idRequirement) => {
    requirementVersionPage.requirementLinkTable.assertRowExist(idRequirement);
  });
}

function assertTestCaseCanBeLinkedToRequirementFromLinkTestCasePage(
  verifyingTestCaseTable: GridElement,
) {
  associateTestCaseToRequirementFromLinkTestCasePage('requirement', 'Hello');
  verifyingTestCaseTable.assertRowCount(2);
}

function dissociateTestCasesFromRequirement(
  requirementVersionPage: RequirementVersionViewPage,
  removeVerifyingTestCaseDialog: RemoveVerifyingTestCasesDialogElement,
  verifyingTestCaseTable: GridElement,
) {
  unlinkTestCasesFromRequirementVersionPage(
    requirementVersionPage,
    removeVerifyingTestCaseDialog,
    verifyingTestCaseTable,
    'Wolf',
    1,
    1,
  );
  requirementVersionPage.verifyingTestCaseTable.findRowId('name', 'Hello').then((idTestCase) => {
    requirementVersionPage.verifyingTestCaseTable.assertRowExist(idTestCase);
  });
  unlinkTestCasesFromRequirementVersionPageWithTopIcon(
    requirementVersionPage,
    removeVerifyingTestCaseDialog,
    verifyingTestCaseTable,
    'Hello',
    1,
    2,
    0,
  );
}
