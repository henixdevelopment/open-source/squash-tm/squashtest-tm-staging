import { DatabaseUtils } from '../../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { RequirementWorkspacePage } from '../../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  HighLevelRequirementBuilder,
  RequirementBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { TestCaseBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  RequirementsLinkToHighLevelRequirementBuilder,
  RequirementsVersionLinkBuilder,
  TestCasesLinkToVerifiedRequirementVersionBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/link-builders';
import { RequirementSecondLevelPage } from '../../../../../page-objects/pages/requirement-workspace/requirement/requirement-second-level.page';
import {
  selectNodeInAnotherNode,
  verifyLinkCannotBeModified,
} from '../../../../scenario-parts/requirement.part';
import { HighLevelRequirementViewPage } from '../../../../../page-objects/pages/requirement-workspace/requirement/high-level-requirement-view.page';
import { RequirementVersionViewPage } from '../../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-view.page';
import { KeyboardShortcuts } from '../../../../../page-objects/elements/workspace-common/keyboard-shortcuts';
import { RemoveDatasetDialog } from '../../../../../page-objects/pages/test-case-workspace/dialogs/remove-dataset-dialog.element';
import { GridElement } from '../../../../../page-objects/elements/grid/grid.element';

describe('Check if a user with Guest access can view the level 2 page of a requirement, but cannot use it to create, remove or modify links between requirements or create or remove links between requirements and test cases', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07210.CT03 - requirement-requirementdetails-readwrite-nok', () => {
    cy.log('Step 1');
    cy.logInAs('SamG', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    const requirementSecondLevelPage = new RequirementSecondLevelPage();
    const requirementViewPage = requirementSecondLevelPage.requirementVersionViewPage;

    verifyCannotAssociateRequirementsFromRequirementSecondLevelPage(
      requirementWorkspacePage,
      requirementSecondLevelPage,
    );

    cy.log('Step 2');
    verifyLinkCannotBeModified('requirement', 'name', 'HLReq');

    cy.log('Step 3');
    assertGuestCanNotDissociateLinkedRequirementFromRequirementSecondLevelPage(requirementViewPage);

    cy.log('Step 4');
    assertGuestCanNotAssociateTestCasesFromRequirementSecondLevelPage(requirementViewPage);

    cy.log('Step 5');
    assertGuestCanNotDissociateTestCasesFromRequirementSecondLevelPage(requirementViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('SamG').withFirstName('Samwise').withLastName('Gamgee')])
    .withProjects([
      new ProjectBuilder('TestProject')
        .withRequirementLibraryNodes([
          new HighLevelRequirementBuilder('HLReq'),
          new RequirementBuilder('ReqA'),
          new RequirementBuilder('ReqB'),
        ])
        .withTestCaseLibraryNodes([new TestCaseBuilder('TestCase')])
        .withLinks([
          new RequirementsLinkToHighLevelRequirementBuilder(['ReqA'], 'HLReq'),
          new RequirementsVersionLinkBuilder('HLReq', 'ReqA'),
          new TestCasesLinkToVerifiedRequirementVersionBuilder(['TestCase'], 'ReqB'),
        ]),
    ])
    .build();
}

function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}
function verifyCannotAssociateRequirementsFromRequirementSecondLevelPage(
  requirementWorkspacePage: RequirementWorkspacePage,
  requirementSecondLevelPage: RequirementSecondLevelPage,
) {
  selectNodeInAnotherNode(requirementWorkspacePage, 'TestProject', 'HLReq');
  const highLevelRequirementViewPage = new HighLevelRequirementViewPage('*', '*');
  highLevelRequirementViewPage.linkedLowLevelRequirementTable
    .getCell('2', 'name')
    .find('a')
    .click();
  requirementSecondLevelPage.assertExists();
  requirementSecondLevelPage.assertElementsPageAreFullyLoaded();
  requirementSecondLevelPage.requirementVersionViewPage.assertIconLinkRequirementDoesNotExist();
}
function assertGuestCanNotDissociateLinkedRequirementFromRequirementSecondLevelPage(
  requirementViewPage: RequirementVersionViewPage,
) {
  requirementViewPage.anchors.clickLink('requirement-version-links');
  requirementViewPage.assertIconNotExists('remove-requirement-links');
  assertRowExistsInTable(requirementViewPage.requirementLinkTable, 'name', 'HLReq');
  verifyIconNotPresentInTableRightViewport(
    requirementViewPage.requirementLinkTable,
    'name',
    'HLReq',
    'delete',
  );
  selectRowInTable(requirementViewPage.requirementLinkTable, 'name', 'HLReq');
  const keyboardShortcut = new KeyboardShortcuts();
  const confirmDeleteDialog = new RemoveDatasetDialog();
  keyboardShortcut.pressDelete();
  confirmDeleteDialog.assertNotExist();
  const requirementVersionViewInformationPage = requirementViewPage.clickInformationAnchorLink();
  requirementVersionViewInformationPage.highLevelRequirementName.rootElement.trigger('mouseover');
  requirementVersionViewInformationPage.assertButtonUnlinkHighLevelRequirementIsNotPresent();
  requirementVersionViewInformationPage.assertButtonChangeHighLevelRequirementIsNotPresent();
  RequirementSecondLevelPage.navigateToRequirementSecondLevelPage(3);
  requirementVersionViewInformationPage.assertButtonBindHighLevelRequirementIsNotPresent();
}

function verifyIconNotPresentInTableRightViewport(
  table: GridElement,
  rowName: string,
  itemName: string,
  icon: string,
) {
  table.findRowId(rowName, itemName).then((rowId) => {
    table.getRow(rowId, 'rightViewport').cell(icon).assertNotExist();
  });
}

function selectRowInTable(table: GridElement, cellId: string, content: string) {
  table.findRowId(cellId, content).then((idTestCase) => {
    table.selectRow(idTestCase, '#', 'leftViewport');
  });
}

function assertRowExistsInTable(table: GridElement, cellId: string, content: string) {
  table.findRowId(cellId, content).then((idTestCase) => {
    table.getRow(idTestCase).assertExists();
  });
}

function assertGuestCanNotAssociateTestCasesFromRequirementSecondLevelPage(
  requirementViewPage: RequirementVersionViewPage,
) {
  requirementViewPage.anchors.clickLink('linked-test-case');
  requirementViewPage.assertIconNotExists('add-verifying-test-case');
  requirementViewPage.assertIconNotExists('search-coverages');
}

function assertGuestCanNotDissociateTestCasesFromRequirementSecondLevelPage(
  requirementViewPage: RequirementVersionViewPage,
) {
  requirementViewPage.anchors.clickLink('linked-test-case');
  requirementViewPage.assertIconNotExists('remove-verifying-test-case');
  assertRowExistsInTable(requirementViewPage.verifyingTestCaseTable, 'name', 'TestCase');
  verifyIconNotPresentInTableRightViewport(
    requirementViewPage.verifyingTestCaseTable,
    'name',
    'TestCase',
    'delete',
  );
  selectRowInTable(requirementViewPage.verifyingTestCaseTable, 'name', 'TestCase');
  const keyboardShortcut = new KeyboardShortcuts();
  const confirmDeleteDialog = new RemoveDatasetDialog();
  keyboardShortcut.pressDelete();
  confirmDeleteDialog.assertNotExist();
}
