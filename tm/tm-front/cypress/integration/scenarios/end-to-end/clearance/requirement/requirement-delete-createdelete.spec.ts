import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import {
  HighLevelRequirementBuilder,
  RequirementBuilder,
  RequirementFolderBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import {
  deleteElementInRequirementWorkspaceWithShortCuts,
  deleteElementInRequirementWorkspaceWithToolBarButton,
} from '../../../scenario-parts/requirement.part';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Delete requirement, requirement-folder and high level requirement in requirements workspace', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07011.CT04 - requirement-delete-createdelete', () => {
    cy.log('step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();

    deleteElementInRequirementWorkspaceWithToolBarButton(
      requirementWorkspacePage,
      'Clio',
      'Calmliope',
    );
    deleteElementInRequirementWorkspaceWithShortCuts(requirementWorkspacePage, 'Clio', 'Melpomène');

    cy.log('step 2');
    deleteElementInRequirementWorkspaceWithToolBarButton(requirementWorkspacePage, 'Clio', 'Erato');
    deleteElementInRequirementWorkspaceWithShortCuts(requirementWorkspacePage, 'Clio', 'Mélété');
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('requirement deleter', [
        ProfileBuilderPermission.REQUIREMENT_READ,
        ProfileBuilderPermission.REQUIREMENT_DELETE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Clio').withRequirementLibraryNodes([
        new RequirementFolderBuilder('Melpomène'),
        new RequirementFolderBuilder('Calmliope'),
        new HighLevelRequirementBuilder('Mélété'),
        new RequirementBuilder('Erato'),
      ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}
