import { DatabaseUtils } from '../../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  HighLevelRequirementBuilder,
  RequirementBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import {
  RequirementsLinkToHighLevelRequirementBuilder,
  RequirementsVersionLinkBuilder,
  TestCasesLinkToVerifiedRequirementVersionBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/link-builders';
import { TestCaseBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { RequirementWorkspacePage } from '../../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import {
  assertGuestCanNotDissociateATestCaseFromRequirementSecondLevelVersionViewPage,
  assertGuestCanNotDissociateLinkedRequirementFromRequirementSecondLevelVersionViewPage,
  verifyCannotAssociateRequirementsFromRequirementSecondLevelVersionViewPage,
  verifyCannotAssociateTestCasesFromRequirementSecondLevelVersionViewPage,
  verifyLinkCannotBeModified,
} from '../../../../scenario-parts/requirement.part';
import { RequirementSecondLevelPage } from '../../../../../page-objects/pages/requirement-workspace/requirement/requirement-second-level.page';

describe('Check if a user with Guest cannot perform the following actions from the level 2 page of a high-level requirement: associate/dissociate requirements, modify the association type, associate/dissociate test cases', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });
  it('F01-UC07212.CT04 - requirement-requirementdetails-hl-link-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    const requirementSecondLevelPage = new RequirementSecondLevelPage();
    const requirementVersionViewPage = requirementSecondLevelPage.requirementVersionViewPage;
    verifyCannotAssociateRequirementsFromRequirementSecondLevelVersionViewPage(
      requirementWorkspacePage,
      requirementSecondLevelPage,
    );

    cy.log('Step 2');
    verifyLinkCannotBeModified('requirement', 'name', 'Exig');

    cy.log('Step 3');
    assertGuestCanNotDissociateLinkedRequirementFromRequirementSecondLevelVersionViewPage(
      requirementVersionViewPage,
    );

    cy.log('Step 4');
    verifyCannotAssociateTestCasesFromRequirementSecondLevelVersionViewPage(
      requirementVersionViewPage,
    );

    cy.log('Step 5');
    assertGuestCanNotDissociateATestCaseFromRequirementSecondLevelVersionViewPage(
      requirementVersionViewPage,
    );
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProjects([
      new ProjectBuilder('Project')
        .withRequirementLibraryNodes([
          new HighLevelRequirementBuilder('haut Exig'),
          new RequirementBuilder('Exig'),
        ])
        .withTestCaseLibraryNodes([new TestCaseBuilder('Test')])
        .withLinks([
          new RequirementsLinkToHighLevelRequirementBuilder(['Exig'], 'haut Exig'),
          new TestCasesLinkToVerifiedRequirementVersionBuilder(['Test'], 'Exig'),
          new RequirementsVersionLinkBuilder('haut Exig', 'Exig'),
        ]),
    ])
    .build();
}

function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}
