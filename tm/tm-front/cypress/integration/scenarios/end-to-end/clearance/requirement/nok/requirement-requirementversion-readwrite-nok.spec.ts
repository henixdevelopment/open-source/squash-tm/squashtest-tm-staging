import { DatabaseUtils } from '../../../../../utils/database.utils';
import { RequirementWorkspacePage } from '../../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  HighLevelRequirementBuilder,
  RequirementBuilder,
  RequirementVersionBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import { BindableEntity } from '../../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { SystemE2eCommands } from '../../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import {
  MilestoneBuilder,
  MilestoneOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/milestone-prerequisite-builders';
import { selectNodeInAnotherNode } from '../../../../scenario-parts/requirement.part';
import { RequirementViewPage } from '../../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import { RequirementVersionDetailViewPage } from '../../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-detail-view.page';
import { RequirementMultiVersionPage } from '../../../../../page-objects/pages/requirement-workspace/requirement/requirement-multi-version.page';
import { GridElement } from '../../../../../page-objects/elements/grid/grid.element';
import { HighLevelRequirementViewPage } from '../../../../../page-objects/pages/requirement-workspace/requirement/high-level-requirement-view.page';

describe('Check if a user with Guest access can view a requirement version page but cannot (modify attributes, modify the name and reference, associate or dissociate a milestone)', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07210.CT04 - requirement-requirementversion-readwrite-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    const requirementVersionDetailViewPage = new RequirementVersionDetailViewPage();
    const requirementMultiVersionPage = new RequirementMultiVersionPage(
      new GridElement('requirement-versions-grid'),
    );
    displayRequirementVersionViewPage(
      requirementWorkspacePage,
      requirementVersionDetailViewPage,
      requirementMultiVersionPage,
    );

    cy.log('Step 2');
    verifyAttributeModificationProhibited(requirementVersionDetailViewPage);

    cy.log('Step 3');
    verifyRenameHighRequirementFieldsProhibited(
      requirementWorkspacePage,
      requirementMultiVersionPage,
      requirementVersionDetailViewPage,
    );

    cy.log('Step 4');
    assertCannotAssociateDisassociateMilestoneInRequirementVersionPage(
      requirementVersionDetailViewPage,
    );
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withCufs([new CustomFieldBuilder('Tag', InputType.TAG)])
    .withMilestones([new MilestoneBuilder('Jalon', '2025-10-31 14:44:45').withUserLogin('RonW')])
    .withProjects([
      new ProjectBuilder('Project')
        .withRequirementLibraryNodes([
          new HighLevelRequirementBuilder('Haut').withVersions([
            new RequirementVersionBuilder('Fafa'),
            new RequirementVersionBuilder('Fifa').withMilestones(['Jalon']),
          ]),
          new RequirementBuilder('Exig').withVersions([
            new RequirementVersionBuilder('Fila'),
            new RequirementVersionBuilder('Fiba'),
          ]),
        ])
        .withCufsOnProject([
          new CustomFieldOnProjectBuilder('Tag', BindableEntity.REQUIREMENT_VERSION),
        ])
        .withMilestonesOnProject([new MilestoneOnProjectBuilder('Jalon')]),
    ])
    .build();
}

function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}

function displayRequirementVersionViewPage(
  requirementWorkspacePage: RequirementWorkspacePage,
  requirementVersionDetailViewPage: RequirementVersionDetailViewPage,
  requirementMultiVersionPage: RequirementMultiVersionPage,
) {
  selectNodeInAnotherNode(requirementWorkspacePage, 'Project', 'Fiba');
  const requirementViewPage = new RequirementViewPage();
  requirementViewPage.assertExists();
  requirementViewPage.currentVersion.clickInformationAnchorLink().clickOnVersionLink();
  requirementMultiVersionPage.assertExists();
  requirementMultiVersionPage.grid.findRowId('versionNumber', '1').then((idExecution) => {
    requirementMultiVersionPage.grid.getCell(idExecution, 'name').findCellTextSpan().click();
  });
  requirementVersionDetailViewPage.assertExists();
}

function verifyAttributeModificationProhibited(
  requirementVersionDetailViewPage: RequirementVersionDetailViewPage,
) {
  requirementVersionDetailViewPage.statusCapsule.assertIsNotClickable();
  requirementVersionDetailViewPage.requirementVersionPage
    .clickInformationAnchorLink()
    .categoryField.assertIsNotEditable();
  requirementVersionDetailViewPage.getTagCustomField('Tag').assertIsNotEditable();
}

function verifyRenameHighRequirementFieldsProhibited(
  requirementWorkspacePage: RequirementWorkspacePage,
  requirementMultiVersionPage: RequirementMultiVersionPage,
  requirementVersionDetailViewPage: RequirementVersionDetailViewPage,
) {
  requirementMultiVersionPage.clickOnButton('back');
  selectNodeInAnotherNode(requirementWorkspacePage, 'Project', 'Fifa');
  const highLevelRequirementViewPage = new HighLevelRequirementViewPage('*', '*');
  highLevelRequirementViewPage.assertExists();
  const highRequirementMultiVersionPage = highLevelRequirementViewPage.currentVersion
    .clickInformationAnchorLink()
    .clickOnVersionLink();
  highRequirementMultiVersionPage.assertExists();
  highRequirementMultiVersionPage.grid.findRowId('versionNumber', '3').then((idExecution) => {
    highRequirementMultiVersionPage.grid.getCell(idExecution, 'name').findCellTextSpan().click();
  });
  requirementVersionDetailViewPage.assertExists();
  requirementVersionDetailViewPage.nameTextField.assertIsNotEditable();
  requirementVersionDetailViewPage.referenceTextField.assertIsNotEditable();
}

function assertCannotAssociateDisassociateMilestoneInRequirementVersionPage(
  requirementVersionDetailViewPage: RequirementVersionDetailViewPage,
) {
  const requirementVersionInformationViewPage =
    requirementVersionDetailViewPage.requirementVersionPage.clickInformationAnchorLink();
  requirementVersionInformationViewPage.milestoneTagElement.openMilestoneDialogNotExist();
  requirementVersionInformationViewPage.milestoneTagElement.removeMilestoneButtonShouldNotExist(
    'Jalon',
  );
}
