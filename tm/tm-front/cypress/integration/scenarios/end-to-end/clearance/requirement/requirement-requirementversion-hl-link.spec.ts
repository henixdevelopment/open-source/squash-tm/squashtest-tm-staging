import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import { RequirementMultiVersionPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-multi-version.page';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  HighLevelRequirementBuilder,
  RequirementBuilder,
  RequirementVersionBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  KeywordTestCaseBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { HighLevelRequirementViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/high-level-requirement-view.page';
import { RequirementVersionDetailViewPage } from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-detail-view.page';
import { RemoveVerifyingTestCasesDialogElement } from '../../../../page-objects/pages/test-case-workspace/dialogs/remove-verifying-test-cases-dialog.element';
import { RemoveRequirementVersionLinksDialogElement } from '../../../../page-objects/pages/requirement-workspace/dialogs/remove-requirement-version-links-dialog.element';
import { UnbindLowLevelRequirementDialogElement } from '../../../../page-objects/pages/requirement-workspace/dialogs/unbind-low-level-requirement-dialog.element';
import {
  changeLinkTypeOfRequirementInVersionPage,
  linkRequirementFromLowLevelRequirementInHighLevelRequirement,
  linkRequirementToAnyRequirementInVersionPage,
  linkTestCaseToRequirementFromLinkTestCasePageInVersionPage,
  linkTestCaseToRequirementVersion,
  unlinkRequirementFromLowLevelRequirementInVersionPage,
  unlinkRequirementInVersionPage,
  unlinkTestCasesFromRequirementVersionPage,
  unlinkTestCasesFromRequirementVersionPageWithTopIcon,
} from '../../../scenario-parts/requirement.part';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addClearanceToProject,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can link requirements and test-cases to a high level requirement from version page', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07012.CT06 - requirement-requirementversion-hl-link', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    const highLevelRequirementViewPage = new HighLevelRequirementViewPage(1, 3);
    const grid = new GridElement('requirement-versions-grid');
    const requirementMultiVersionPage = new RequirementMultiVersionPage(grid);
    const highLevelRequirementVersion = new RequirementVersionDetailViewPage(3);
    const highLevelRequirementVersionPage = highLevelRequirementVersion.requirementVersionPage;
    const verifyingTestCaseTable = highLevelRequirementVersionPage.verifyingTestCaseTable;
    const LinkHighLevelToLowLevelRequirementTable =
      highLevelRequirementViewPage.currentVersion.requirementLinkHighLevelRequirementTable;
    const removeVerifyingTestCaseDialog = new RemoveVerifyingTestCasesDialogElement(3, [1, 2]);
    const removeRequirementLinkDialog = new RemoveRequirementVersionLinksDialogElement(3, [1]);
    const unbindLowLevelRequirementDialog = new UnbindLowLevelRequirementDialogElement(3, [4]);

    linkRequirementToAnyRequirementInVersionPage(
      requirementWorkspacePage,
      'requirement',
      requirementMultiVersionPage,
      highLevelRequirementVersion,
      'AZERTY',
      'Zeus',
      'wow',
      1,
    );
    linkRequirementFromLowLevelRequirementInHighLevelRequirement(
      highLevelRequirementViewPage,
      LinkHighLevelToLowLevelRequirementTable,
      'AZERTY',
      'Jaffar',
      1,
    );

    cy.log('Step 2');
    changeLinkTypeOfRequirementInVersionPage(
      highLevelRequirementVersionPage,
      'wow',
      'Parent - Enfant',
      'Enfant',
    );

    cy.log('Step 3');
    unlinkRequirementInVersionPage(
      highLevelRequirementVersionPage,
      removeRequirementLinkDialog,
      'wow',
      1,
      0,
    );
    unlinkRequirementFromLowLevelRequirementInVersionPage(
      highLevelRequirementViewPage,
      unbindLowLevelRequirementDialog,
      'Jaffar',
      1,
      0,
    );

    cy.log('Step 4');
    linkTestCaseToRequirementVersion(
      highLevelRequirementVersionPage,
      verifyingTestCaseTable,
      'AZERTY',
      'Hello',
      3,
    );
    verifyingTestCaseTable.assertRowCount(1);
    linkTestCaseToRequirementFromLinkTestCasePageInVersionPage(
      highLevelRequirementVersionPage,
      'Wolf',
    );
    verifyingTestCaseTable.findRowId('name', 'Wolf').then((idTestCase) => {
      verifyingTestCaseTable.assertRowExist(idTestCase);
      verifyingTestCaseTable.assertRowCount(2);
    });

    cy.log('Step 5');
    unlinkTestCasesFromRequirementVersionPage(
      highLevelRequirementVersionPage,
      removeVerifyingTestCaseDialog,
      verifyingTestCaseTable,
      'Hello',
      3,
      1,
    );
    unlinkTestCasesFromRequirementVersionPageWithTopIcon(
      highLevelRequirementVersionPage,
      removeVerifyingTestCaseDialog,
      verifyingTestCaseTable,
      'Wolf',
      3,
      1,
      0,
    );
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('requirement linker', [
        ProfileBuilderPermission.REQUIREMENT_READ,
        ProfileBuilderPermission.REQUIREMENT_LINK,
        ProfileBuilderPermission.TEST_CASE_READ,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('AZERTY')
        .withRequirementLibraryNodes([
          new HighLevelRequirementBuilder('Lala').withVersions([
            new RequirementVersionBuilder('Hera'),
            new RequirementVersionBuilder('Zeus'),
          ]),
          new RequirementBuilder('Jaffar'),
          new RequirementBuilder('wow'),
        ])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Wolf'),
          new KeywordTestCaseBuilder('Hello'),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToProject(1, FIRST_CUSTOM_PROFILE_ID, [2]);
}
