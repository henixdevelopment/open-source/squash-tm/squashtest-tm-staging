import { DatabaseUtils } from '../../../../../utils/database.utils';
import { RequirementWorkspacePage } from '../../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  HighLevelRequirementBuilder,
  RequirementBuilder,
  RequirementFolderBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { canNotDragAndDropObjectWithinOrBetweenProjects } from '../../../../scenario-parts/requirement.part';

describe('Check if a user with Guest cannot move an element in the tree by drag and drop in the Requirements workspace', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });
  it('F01-UC07211.CT03 - requirement-move-createdelete-nok', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();
    canNotDragAndDropObjectWithinOrBetweenProjects(
      requirementWorkspacePage,
      'Project 1',
      'Folder 1',
      'Project 2',
      false,
    );
    canNotDragAndDropObjectWithinOrBetweenProjects(
      requirementWorkspacePage,
      'Project 2',
      'Dossier',
      'Project 1',
      false,
    );

    cy.log('Step 2');
    canNotDragAndDropObjectWithinOrBetweenProjects(
      requirementWorkspacePage,
      'Folder 1',
      'Haute Exi',
      'Project 1',
      false,
    );
    canNotDragAndDropObjectWithinOrBetweenProjects(
      requirementWorkspacePage,
      'Project 2',
      'Exi 2',
      'Exi 1',
      false,
    );

    cy.log('Step 3');
    canNotDragAndDropObjectWithinOrBetweenProjects(
      requirementWorkspacePage,
      'Project 1',
      'Folder 1',
      'Project 3',
      true,
    );
    canNotDragAndDropObjectWithinOrBetweenProjects(
      requirementWorkspacePage,
      'Project 2',
      'Exi 1',
      'Project 3',
      false,
    );
  });
});
function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProjects([
      new ProjectBuilder('Project 1').withRequirementLibraryNodes([
        new RequirementFolderBuilder('Folder 1').withRequirementLibraryNodes([
          new HighLevelRequirementBuilder('Haute Exi'),
        ]),
        new RequirementFolderBuilder('Folder 2'),
      ]),
      new ProjectBuilder('Project 2').withRequirementLibraryNodes([
        new RequirementFolderBuilder('Dossier'),
        new RequirementBuilder('Exi 1'),
        new RequirementBuilder('Exi 2'),
      ]),
      new ProjectBuilder('Project 3'),
    ])
    .build();
}

function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1, 2]);
  addClearanceToUser(2, SystemProfile.PROJECT_MANAGER, [3]);
}
