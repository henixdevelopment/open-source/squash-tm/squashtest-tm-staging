import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  HighLevelRequirementBuilder,
  RequirementBuilder,
  RequirementFolderBuilder,
  RequirementVersionBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import { RequirementWorkspacePage } from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { ConfirmInterProjectPaste } from '../../../../page-objects/elements/dialog/confirm-inter-project-paste';
import { copyPasteInRequirementWorkspace } from '../../../scenario-parts/requirement.part';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Verify copy/paste with CREATE authorization in the requirement tree', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07011.CT02 - requirement-copypaste-createdelete', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage();

    copyPasteInRequirementWorkspace(requirementWorkspacePage, 'Athena', 'Folder', 'Athena', false);
    copyPasteInRequirementWorkspace(
      requirementWorkspacePage,
      'Athena',
      'DifferentVersion',
      'Athena',
      true,
    );

    cy.log('Step 2');
    const confirmDialog = new ConfirmInterProjectPaste('confirm-inter-project-paste');

    copyPasteFromKeyboardShortcutInDifferentProjects(requirementWorkspacePage, confirmDialog);
    copyPasteFromTreeToolbarInDifferentProjects(requirementWorkspacePage, confirmDialog);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('requirement copier', [
        ProfileBuilderPermission.REQUIREMENT_READ,
        ProfileBuilderPermission.REQUIREMENT_CREATE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Athena').withRequirementLibraryNodes([
        new RequirementFolderBuilder('Folder').withRequirementLibraryNodes([
          new RequirementBuilder('Exi1').withCriticality('MINOR'),
          new HighLevelRequirementBuilder('EHN').withCriticality('MAJOR'),
        ]),
        new RequirementBuilder('Ex1WithMultipleVersion').withVersions([
          new RequirementVersionBuilder('OtherVersion'),
          new RequirementVersionBuilder('DifferentVersion'),
        ]),
        new HighLevelRequirementBuilder('HighLevelRequirement'),
      ]),
      new ProjectBuilder('Medusa'),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1, 2]);
}
function copyPasteFromKeyboardShortcutInDifferentProjects(
  requirementWorkspacePage: RequirementWorkspacePage,
  confirmDialog: ConfirmInterProjectPaste,
) {
  copyPasteInRequirementWorkspace(requirementWorkspacePage, 'Athena', 'Folder', 'Medusa', true);
  confirmDialog.clickOnConfirmButton();
}

function copyPasteFromTreeToolbarInDifferentProjects(
  requirementWorkspacePage: RequirementWorkspacePage,
  confirmDialog: ConfirmInterProjectPaste,
) {
  copyPasteInRequirementWorkspace(
    requirementWorkspacePage,
    'Athena',
    'HighLevelRequirement',
    'Medusa',
    false,
  );
  confirmDialog.clickOnConfirmButton();
}
