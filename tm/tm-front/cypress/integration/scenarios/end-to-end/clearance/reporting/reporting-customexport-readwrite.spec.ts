import { DatabaseUtils } from '../../../../utils/database.utils';
import { CustomReportWorkspacePage } from '../../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import { CustomExportViewPage } from '../../../../page-objects/pages/custom-report-workspace/custom-export-view.page';
import {
  modifyCustomExportNamePerimeterAndAddAnAttribute,
  selectNodeInCustomReportWorkspace,
} from '../../../scenario-parts/reporting.part';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { CampaignBuilder } from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  CustomColumnEntityType,
  CustomExportBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/custom-report-prerequisite-builders';
import { CustomExportEntityType } from '../../../../../../projects/sqtm-core/src/lib/model/custom-report/custom-export-columns.model';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Test asserts that a user can consult and modify a custom export', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07040.CT05 - reporting-customexport-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage();
    const customExportViewPage = new CustomExportViewPage('*');
    verifyCustomExportSelectionAndNameModification(customReportWorkspacePage, customExportViewPage);

    cy.log('Step 2');
    modifyACustomExport(customExportViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('reporting read/write', [
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CUSTOM_REPORT_READ,
        ProfileBuilderPermission.CUSTOM_REPORT_WRITE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project')
        .withCampaignLibraryNodes([new CampaignBuilder('Campaign'), new CampaignBuilder('B')])
        .withCustomReportLibraryNode([
          new CustomExportBuilder(
            'Lol',
            CustomColumnEntityType.TEST_CASE_ID,
            CustomExportEntityType.CAMPAIGN,
            'Campaign',
          ),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}

function verifyCustomExportSelectionAndNameModification(
  customReportWorkspacePage: CustomReportWorkspacePage,
  customExportViewPage: CustomExportViewPage,
) {
  selectNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project', 'Lol');
  customExportViewPage.assertExists();
  customExportViewPage.nameTextField.setValue('Lola');
  customExportViewPage.nameTextField.clickOnConfirmButton();
  customExportViewPage.nameTextField.assertContainsText('Lola');
}
function modifyACustomExport(customExportViewPage: CustomExportViewPage) {
  customExportViewPage.assertExists();
  modifyCustomExportNamePerimeterAndAddAnAttribute(
    customExportViewPage,
    'Hola',
    ['Project', 'B'],
    'ID',
  );
  customExportViewPage.assertNameContains('Hola');
  customExportViewPage.assertPerimeterFieldContains('B');
  customExportViewPage.assertColumnFieldContains(' Campagnes | ID ', 0);
  customExportViewPage.assertColumnFieldContains(' Cas de test | ID ', 1);
}
