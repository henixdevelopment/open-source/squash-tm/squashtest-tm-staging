import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { CampaignBuilder } from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  ChartBuilder,
  CustomColumnEntityType,
  CustomExportBuilder,
  DashboardBuilder,
  FolderCustomReportLibraryBuilder,
  ReportBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/custom-report-prerequisite-builders';
import { PluginNameSpace } from '../../../../../../projects/sqtm-core/src/lib/model/custom-report/report-definition.model';
import {
  ChartOperation,
  ChartScopeType,
  ChartType,
} from '../../../../../../projects/sqtm-core/src/lib/model/custom-report/chart-definition.model';
import { EntityType } from '../../../../../../projects/sqtm-core/src/lib/model/entity.model';
import { CustomExportEntityType } from '../../../../../../projects/sqtm-core/src/lib/model/custom-report/custom-export-columns.model';
import { CustomReportWorkspacePage } from '../../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import { KeyboardShortcuts } from '../../../../page-objects/elements/workspace-common/keyboard-shortcuts';
import {
  assertNodeExistsInCustomWorkspaceProject,
  selectNodeInCustomReportWorkspace,
  selectPickNodeInCustomReportWorkspace,
} from '../../../scenario-parts/reporting.part';
import { mockReportDefinitionParameterTestCase } from '../../../../data-mock/report-definition.data-mock';
import { createRegExpForCopyNode } from '../../../../utils/end-to-end/end-to-end-helper';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can copy/paste folder, dashboard, chart, report, custom export in custom report workspace ', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07040.CT04 - reporting-copypaste-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage();

    copyPasteFolderInCustomReportWorkspace(customReportWorkspacePage);

    cy.log('Step 2');
    copyPasteChartInCustomReportWorkspace(customReportWorkspacePage);

    cy.log('Step 3');
    copyPasteReportInCustomReportWorkspace(customReportWorkspacePage);

    cy.log('Step 4');
    copyPasteDashboardInCustomReportWorkspace(customReportWorkspacePage);

    cy.log('Step 4');
    copyPasteCustomExportInCustomReportWorkspace(customReportWorkspacePage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('reporting copy/paste', [
        ProfileBuilderPermission.CUSTOM_REPORT_READ,
        ProfileBuilderPermission.CUSTOM_REPORT_CREATE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project-A')
        .withCampaignLibraryNodes([new CampaignBuilder('Campaign')])
        .withCustomReportLibraryNode([
          new ReportBuilder(
            'Report',
            PluginNameSpace.TEST_CASE_REPORT,
            JSON.stringify(mockReportDefinitionParameterTestCase()),
          ),
          new DashboardBuilder('Dashboard'),
          new ChartBuilder(
            'Chart',
            'REQUIREMENT_CRITICALITY',
            ChartType.PIE,
            ChartScopeType.DEFAULT,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ),
          new FolderCustomReportLibraryBuilder('Folder'),
          new CustomExportBuilder(
            'Custom Export',
            CustomColumnEntityType.ITERATION_ID,
            CustomExportEntityType.CAMPAIGN,
            'Campaign',
          ),
        ]),
      new ProjectBuilder('Project-B'),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1, 2]);
}
function copyPasteFolderInCustomReportWorkspace(
  customReportWorkspacePage: CustomReportWorkspacePage,
) {
  selectNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project-A', 'Folder');
  customReportWorkspacePage.treeMenu.getButton('copy-button').clickWithoutSpan();
  selectPickNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project-A');
  customReportWorkspacePage.treeMenu.getButton('paste-button').clickWithoutSpan();
  selectPickNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project-B');
  customReportWorkspacePage.treeMenu.getButton('paste-button').clickWithoutSpan();
  assertNodeExistsInCustomWorkspaceProject(
    customReportWorkspacePage,
    'Project-A',
    createRegExpForCopyNode('Folder'),
  );
  assertNodeExistsInCustomWorkspaceProject(customReportWorkspacePage, 'Project-B', 'Folder');
}

function copyPasteChartInCustomReportWorkspace(
  customReportWorkspacePage: CustomReportWorkspacePage,
) {
  selectNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project-A', 'Chart');
  const keyboardShortcut = new KeyboardShortcuts();
  keyboardShortcut.performCopy();
  selectPickNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project-A');
  keyboardShortcut.performPaste();
  selectPickNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project-B');
  keyboardShortcut.performPaste();
  assertNodeExistsInCustomWorkspaceProject(
    customReportWorkspacePage,
    'Project-A',
    createRegExpForCopyNode('Chart'),
  );
  assertNodeExistsInCustomWorkspaceProject(customReportWorkspacePage, 'Project-B', 'Chart');
}

function copyPasteReportInCustomReportWorkspace(
  customReportWorkspacePage: CustomReportWorkspacePage,
) {
  selectNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project-A', 'Report');
  customReportWorkspacePage.treeMenu.getButton('copy-button').clickWithoutSpan();
  selectPickNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project-A');
  customReportWorkspacePage.treeMenu.getButton('paste-button').clickWithoutSpan();
  selectPickNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project-B');
  customReportWorkspacePage.treeMenu.getButton('paste-button').clickWithoutSpan();
  assertNodeExistsInCustomWorkspaceProject(
    customReportWorkspacePage,
    'Project-A',
    createRegExpForCopyNode('Report'),
  );
  assertNodeExistsInCustomWorkspaceProject(customReportWorkspacePage, 'Project-B', 'Report');
}

function copyPasteDashboardInCustomReportWorkspace(
  customReportWorkspacePage: CustomReportWorkspacePage,
) {
  selectNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project-A', 'Dashboard');
  const keyboardShortcut = new KeyboardShortcuts();
  keyboardShortcut.performCopy();
  selectPickNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project-A');
  keyboardShortcut.performPaste();
  selectPickNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project-B');
  keyboardShortcut.performPaste();
  assertNodeExistsInCustomWorkspaceProject(
    customReportWorkspacePage,
    'Project-A',
    createRegExpForCopyNode('Dashboard'),
  );
  assertNodeExistsInCustomWorkspaceProject(customReportWorkspacePage, 'Project-B', 'Dashboard');
}

function copyPasteCustomExportInCustomReportWorkspace(
  customReportWorkspacePage: CustomReportWorkspacePage,
) {
  selectNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project-A', 'Custom Export');
  customReportWorkspacePage.treeMenu.getButton('copy-button').clickWithoutSpan();
  selectPickNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project-A');
  customReportWorkspacePage.treeMenu.getButton('paste-button').clickWithoutSpan();
  selectPickNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project-B');
  customReportWorkspacePage.treeMenu.getButton('paste-button').clickWithoutSpan();
  assertNodeExistsInCustomWorkspaceProject(
    customReportWorkspacePage,
    'Project-A',
    createRegExpForCopyNode('Custom Export'),
  );
  assertNodeExistsInCustomWorkspaceProject(customReportWorkspacePage, 'Project-B', 'Custom Export');
}
