import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { FolderCustomReportLibraryBuilder } from '../../../../utils/end-to-end/prerequisite/builders/custom-report-prerequisite-builders';
import { CustomReportWorkspacePage } from '../../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import { CustomReportFolderViewPage } from '../../../../page-objects/pages/custom-report-workspace/custom-report-folder-view.page';
import {
  editCustomReportFolderDescriptionInWorkspace,
  renameCustomReportFolderInCustomReportWorkspace,
  selectNodeInCustomReportWorkspace,
} from '../../../scenario-parts/reporting.part';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can view and edit a folder in custom report workspace ', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07035.CT01 - reporting-folderview-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage();
    const customReportFolderViewPage = new CustomReportFolderViewPage(1);

    selectFolderAndAssertFolderView(customReportWorkspacePage, customReportFolderViewPage);

    cy.log('Step 2');
    renameCustomReportFolderInCustomReportWorkspace('Dossier');
    editFolderDescriptionInCustomReportWorkspace();
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('reporting read/write', [
        ProfileBuilderPermission.CUSTOM_REPORT_READ,
        ProfileBuilderPermission.CUSTOM_REPORT_WRITE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project').withCustomReportLibraryNode([
        new FolderCustomReportLibraryBuilder('Folder'),
      ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}
function selectFolderAndAssertFolderView(
  customReportWorkspacePage: CustomReportWorkspacePage,
  customReportFolderViewPage: CustomReportFolderViewPage,
) {
  selectNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project', 'Folder');
  customReportFolderViewPage.assertExists();
}

function editFolderDescriptionInCustomReportWorkspace() {
  editCustomReportFolderDescriptionInWorkspace('Description');
}
