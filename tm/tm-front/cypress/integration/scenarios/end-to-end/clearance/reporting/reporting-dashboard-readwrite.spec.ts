import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  ChartBuilder,
  DashboardBuilder,
  ReportBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/custom-report-prerequisite-builders';

import { CustomReportWorkspacePage } from '../../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import { DashboardViewPage } from '../../../../page-objects/pages/custom-report-workspace/dashboard-view.page';
import {
  dragChartToDashboard,
  dragReportToDashboard,
  selectNodeInCustomReportWorkspace,
} from '../../../scenario-parts/reporting.part';
import { PluginNameSpace } from '../../../../../../projects/sqtm-core/src/lib/model/custom-report/report-definition.model';
import {
  ChartOperation,
  ChartScopeType,
  ChartType,
} from '../../../../../../projects/sqtm-core/src/lib/model/custom-report/chart-definition.model';
import { EntityType } from '../../../../../../projects/sqtm-core/src/lib/model/entity.model';
import { TestCaseBuilder } from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { CampaignBuilder } from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { mockReportDefinitionParameterTestCase } from '../../../../data-mock/report-definition.data-mock';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can view and edit a dashboard in custom report workspace ', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07040.CT04 - reporting-dashboard-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage();
    const dashboardViewPage = new DashboardViewPage(1);

    selectDashboardAndModifyNameDashboard(customReportWorkspacePage, dashboardViewPage);

    cy.log('Step 2');
    addEntitiesToDashboard(customReportWorkspacePage, dashboardViewPage);

    cy.log('Step 3');
    deleteEntitiesToDashboard(dashboardViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('reporting read/write/delete', [
        ProfileBuilderPermission.CUSTOM_REPORT_READ,
        ProfileBuilderPermission.CUSTOM_REPORT_WRITE,
        ProfileBuilderPermission.CUSTOM_REPORT_DELETE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project')
        .withTestCaseLibraryNodes([new TestCaseBuilder('Test')])
        .withCampaignLibraryNodes([new CampaignBuilder('Campaign')])
        .withCustomReportLibraryNode([
          new ReportBuilder(
            'Report',
            PluginNameSpace.TEST_CASE_REPORT,
            JSON.stringify(mockReportDefinitionParameterTestCase()),
          ),
          new DashboardBuilder('lala'),
          new ChartBuilder(
            'Chart',
            'REQUIREMENT_CRITICALITY',
            ChartType.PIE,
            ChartScopeType.DEFAULT,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}
function selectDashboardAndModifyNameDashboard(
  customReportWorkspacePage: CustomReportWorkspacePage,
  dashboardViewPage: DashboardViewPage,
) {
  selectNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project', 'lala');
  dashboardViewPage.assertExists();
  dashboardViewPage.nameTextField.setValue('loulou');
  dashboardViewPage.nameTextField.clickOnConfirmButton();
  dashboardViewPage.nameTextField.assertContainsText('loulou');
}

function addEntitiesToDashboard(
  customReportWorkspacePage: CustomReportWorkspacePage,
  dashboardViewPage: DashboardViewPage,
) {
  dashboardViewPage.assertExists();
  dragReportToDashboard(customReportWorkspacePage, dashboardViewPage, 'Report');
  dragChartToDashboard(customReportWorkspacePage, dashboardViewPage, 'Chart');
  dashboardViewPage.assertBindingIsRendered(1);
}

function deleteEntitiesToDashboard(dashboardViewPage: DashboardViewPage) {
  dashboardViewPage.assertExists();
  dashboardViewPage.removeReportIfDashboardHasMultipleElements(1);
  dashboardViewPage.removeChart(1);
  dashboardViewPage.assertEmptyDropzoneIsVisible();
}
