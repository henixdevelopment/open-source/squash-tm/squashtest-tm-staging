import { DatabaseUtils } from '../../../../utils/database.utils';
import { CustomReportWorkspacePage } from '../../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { CampaignBuilder } from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  ChartBuilder,
  CustomColumnEntityType,
  CustomExportBuilder,
  DashboardBuilder,
  FolderCustomReportLibraryBuilder,
  ReportBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/custom-report-prerequisite-builders';
import { PluginNameSpace } from '../../../../../../projects/sqtm-core/src/lib/model/custom-report/report-definition.model';
import {
  ChartOperation,
  ChartScopeType,
  ChartType,
} from '../../../../../../projects/sqtm-core/src/lib/model/custom-report/chart-definition.model';
import { EntityType } from '../../../../../../projects/sqtm-core/src/lib/model/entity.model';
import { CustomExportEntityType } from '../../../../../../projects/sqtm-core/src/lib/model/custom-report/custom-export-columns.model';
import {
  assertNodeNotExistsInCustomWorkspaceProject,
  selectNodeInCustomReportWorkspace,
} from '../../../scenario-parts/reporting.part';
import { RemoveDatasetDialog } from '../../../../page-objects/pages/test-case-workspace/dialogs/remove-dataset-dialog.element';
import { KeyboardShortcuts } from '../../../../page-objects/elements/workspace-common/keyboard-shortcuts';
import { mockReportDefinitionParameterTestCase } from '../../../../data-mock/report-definition.data-mock';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Assert that a user can delete folder, dashboard, chart, report, custom export in the custom report workspace ', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07041.CT04 - reporting-delete-createdelete', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage();
    deleteFolderInCustomReport(customReportWorkspacePage);

    cy.log('Step 2');
    deleteChartInCustomReport(customReportWorkspacePage);

    cy.log('Step 3');
    deleteReportInCustomReport(customReportWorkspacePage);

    cy.log('Step 4');
    deleteDashboardInCustomReport(customReportWorkspacePage);

    cy.log('Step 5');
    deleteCustomExportInCustomReport(customReportWorkspacePage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('reporting read/delete', [
        ProfileBuilderPermission.CUSTOM_REPORT_READ,
        ProfileBuilderPermission.CUSTOM_REPORT_DELETE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project')
        .withCampaignLibraryNodes([new CampaignBuilder('Campaign')])
        .withCustomReportLibraryNode([
          new FolderCustomReportLibraryBuilder('Folder'),
          new ReportBuilder(
            'Report',
            PluginNameSpace.TEST_CASE_REPORT,
            JSON.stringify(mockReportDefinitionParameterTestCase()),
          ),
          new DashboardBuilder('Dashboard'),
          new ChartBuilder(
            'Chart',
            'REQUIREMENT_CRITICALITY',
            ChartType.PIE,
            ChartScopeType.DEFAULT,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ),
          new CustomExportBuilder(
            'Custom Export',
            CustomColumnEntityType.ITERATION_ID,
            CustomExportEntityType.CAMPAIGN,
            'Campaign',
          ),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}
function deleteFolderInCustomReport(customReportWorkspacePage: CustomReportWorkspacePage) {
  selectNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project', 'Folder');
  customReportWorkspacePage.treeMenu.getButton('delete-button').clickWithoutSpan();
  const confirmDialog = new RemoveDatasetDialog();
  confirmDialog.confirm();
  assertNodeNotExistsInCustomWorkspaceProject(customReportWorkspacePage, 'Project', 'Folder');
}
function deleteChartInCustomReport(customReportWorkspacePage: CustomReportWorkspacePage) {
  selectNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project', 'Chart');
  const keyboardShortcut = new KeyboardShortcuts();
  keyboardShortcut.pressDelete();
  const confirmDialog = new RemoveDatasetDialog();
  confirmDialog.confirm();
  assertNodeNotExistsInCustomWorkspaceProject(customReportWorkspacePage, 'Project', 'Chart');
}

function deleteReportInCustomReport(customReportWorkspacePage: CustomReportWorkspacePage) {
  selectNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project', 'Report');
  customReportWorkspacePage.treeMenu.getButton('delete-button').clickWithoutSpan();
  const confirmDialog = new RemoveDatasetDialog();
  confirmDialog.confirm();
  assertNodeNotExistsInCustomWorkspaceProject(customReportWorkspacePage, 'Project', 'Report');
}

function deleteDashboardInCustomReport(customReportWorkspacePage: CustomReportWorkspacePage) {
  selectNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project', 'Dashboard');
  const keyboardShortcut = new KeyboardShortcuts();
  keyboardShortcut.pressDelete();
  const confirmDialog = new RemoveDatasetDialog();
  confirmDialog.confirm();
  assertNodeNotExistsInCustomWorkspaceProject(customReportWorkspacePage, 'Project', 'Dashboard');
}

function deleteCustomExportInCustomReport(customReportWorkspacePage: CustomReportWorkspacePage) {
  selectNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project', 'Custom Export');
  customReportWorkspacePage.treeMenu.getButton('delete-button').clickWithoutSpan();
  const confirmDialog = new RemoveDatasetDialog();
  confirmDialog.confirm();
  assertNodeNotExistsInCustomWorkspaceProject(
    customReportWorkspacePage,
    'Project',
    'Custom Export',
  );
}
