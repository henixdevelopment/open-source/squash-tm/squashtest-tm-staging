import { DatabaseUtils } from '../../../../utils/database.utils';
import { CustomReportWorkspacePage } from '../../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { CampaignBuilder } from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  ChartBuilder,
  CustomColumnEntityType,
  CustomExportBuilder,
  DashboardBuilder,
  FolderCustomReportLibraryBuilder,
  ReportBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/custom-report-prerequisite-builders';
import { PluginNameSpace } from '../../../../../../projects/sqtm-core/src/lib/model/custom-report/report-definition.model';
import {
  ChartOperation,
  ChartScopeType,
  ChartType,
} from '../../../../../../projects/sqtm-core/src/lib/model/custom-report/chart-definition.model';
import { EntityType } from '../../../../../../projects/sqtm-core/src/lib/model/entity.model';
import { CustomExportEntityType } from '../../../../../../projects/sqtm-core/src/lib/model/custom-report/custom-export-columns.model';
import {
  dragEntityIntoAnotherEntityInCustomReportWorkspace,
  selectNodeInCustomReportWorkspace,
} from '../../../scenario-parts/reporting.part';
import { mockReportDefinitionParameterTestCase } from '../../../../data-mock/report-definition.data-mock';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Verify drag and drop authorization for a user in the custom report workspace ', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07041.CT03 - reporting-move-createdelete', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage();
    moveFolderInToAnotherObjectInCustomReportWorkspace(customReportWorkspacePage);
    cy.log('Step 2');
    moveChartInToAnotherObjectInCustomReportWorkspace(customReportWorkspacePage);

    cy.log('Step 3');
    moveReportInToAnotherObjectInCustomReportWorkspace(customReportWorkspacePage);

    cy.log('Step 4');
    moveDashboardInToAnotherObjectInCustomReportWorkspace(customReportWorkspacePage);

    cy.log('Step 5');
    moveCustomExportInToAnotherObjectInCustomReportWorkspace(customReportWorkspacePage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('reporting move', [
        ProfileBuilderPermission.CUSTOM_REPORT_READ,
        ProfileBuilderPermission.CUSTOM_REPORT_CREATE,
        ProfileBuilderPermission.CUSTOM_REPORT_DELETE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project1')
        .withCampaignLibraryNodes([new CampaignBuilder('Campaign')])
        .withCustomReportLibraryNode([
          new FolderCustomReportLibraryBuilder('B'),
          new FolderCustomReportLibraryBuilder('A'),
          new ReportBuilder(
            'Report',
            PluginNameSpace.TEST_CASE_REPORT,
            JSON.stringify(mockReportDefinitionParameterTestCase()),
          ),
          new DashboardBuilder('Dashboard'),
          new ChartBuilder(
            'Chart',
            'REQUIREMENT_CRITICALITY',
            ChartType.PIE,
            ChartScopeType.DEFAULT,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ),
          new CustomExportBuilder(
            'Custom Export',
            CustomColumnEntityType.ITERATION_ID,
            CustomExportEntityType.CAMPAIGN,
            'Campaign',
          ),
        ]),
      new ProjectBuilder('Project2'),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1, 2]);
}
function moveFolderInToAnotherObjectInCustomReportWorkspace(
  customReportWorkspacePage: CustomReportWorkspacePage,
) {
  selectNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project1', 'A');
  dragEntityIntoAnotherEntityInCustomReportWorkspace(
    customReportWorkspacePage,
    'Project1',
    'A',
    'B',
    'Project1',
    false,
  );
  customReportWorkspacePage.tree.assertRowsHaveParentByNames(['A'], 'B');
  dragEntityIntoAnotherEntityInCustomReportWorkspace(
    customReportWorkspacePage,
    'Project1',
    'A',
    'Project2',
    'Project2',
    false,
  );
  customReportWorkspacePage.tree.assertRowsHaveParentByNames(['A'], 'Project2');
}

function moveChartInToAnotherObjectInCustomReportWorkspace(
  customReportWorkspacePage: CustomReportWorkspacePage,
) {
  dragEntityIntoAnotherEntityInCustomReportWorkspace(
    customReportWorkspacePage,
    'Project1',
    'Chart',
    'B',
    'Project1',
    false,
  );
  customReportWorkspacePage.tree.assertRowsHaveParentByNames(['Chart'], 'B');
  dragEntityIntoAnotherEntityInCustomReportWorkspace(
    customReportWorkspacePage,
    'Project1',
    'Chart',
    'Project2',
    'Project2',
    false,
  );
  customReportWorkspacePage.tree.assertRowsHaveParentByNames(['Chart'], 'Project2');
}

function moveReportInToAnotherObjectInCustomReportWorkspace(
  customReportWorkspacePage: CustomReportWorkspacePage,
) {
  dragEntityIntoAnotherEntityInCustomReportWorkspace(
    customReportWorkspacePage,
    'Project1',
    'Report',
    'B',
    'Project1',
    false,
  );
  customReportWorkspacePage.tree.assertRowsHaveParentByNames(['Report'], 'B');
  dragEntityIntoAnotherEntityInCustomReportWorkspace(
    customReportWorkspacePage,
    'Project1',
    'Report',
    'Project2',
    'Project2',
    false,
  );
  customReportWorkspacePage.tree.assertRowsHaveParentByNames(['Report'], 'Project2');
}

function moveDashboardInToAnotherObjectInCustomReportWorkspace(
  customReportWorkspacePage: CustomReportWorkspacePage,
) {
  dragEntityIntoAnotherEntityInCustomReportWorkspace(
    customReportWorkspacePage,
    'Project1',
    'Dashboard',
    'B',
    'Project1',
    false,
  );
  customReportWorkspacePage.tree.assertRowsHaveParentByNames(['Dashboard'], 'B');
  dragEntityIntoAnotherEntityInCustomReportWorkspace(
    customReportWorkspacePage,
    'Project1',
    'Dashboard',
    'Project2',
    'Project2',
    false,
  );
  customReportWorkspacePage.tree.assertRowsHaveParentByNames(['Dashboard'], 'Project2');
}

function moveCustomExportInToAnotherObjectInCustomReportWorkspace(
  customReportWorkspacePage: CustomReportWorkspacePage,
) {
  dragEntityIntoAnotherEntityInCustomReportWorkspace(
    customReportWorkspacePage,
    'Project1',
    'Custom Export',
    'B',
    'Project1',
    false,
  );
  customReportWorkspacePage.tree.assertRowsHaveParentByNames(['Custom Export'], 'B');
  dragEntityIntoAnotherEntityInCustomReportWorkspace(
    customReportWorkspacePage,
    'Project1',
    'Custom Export',
    'Project2',
    'Project2',
    false,
  );
  customReportWorkspacePage.tree.assertRowsHaveParentByNames(['Custom Export'], 'Project2');
}
