import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { ChartBuilder } from '../../../../utils/end-to-end/prerequisite/builders/custom-report-prerequisite-builders';
import {
  ChartOperation,
  ChartScopeType,
  ChartType,
} from '../../../../../../projects/sqtm-core/src/lib/model/custom-report/chart-definition.model';
import { EntityType } from '../../../../../../projects/sqtm-core/src/lib/model/entity.model';
import { TestCaseBuilder } from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { CampaignBuilder } from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { CustomReportWorkspacePage } from '../../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import { ChartDefinitionViewPage } from '../../../../page-objects/pages/custom-report-workspace/chart/chart-definition-view.page';
import {
  modifyChartDefinition,
  selectNodeInCustomReportWorkspace,
} from '../../../scenario-parts/reporting.part';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can view and edit a chart in custom report workspace ', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07040.CT02 - reporting-chart-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');

    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage();
    const chartDefinitionViewPage = new ChartDefinitionViewPage(1);
    selectChartAndModifyNameChart(customReportWorkspacePage, chartDefinitionViewPage);

    cy.log('Step 2');
    modifyChartDefinitionInCustomReportWorkspace(chartDefinitionViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('reporting read/write', [
        ProfileBuilderPermission.CUSTOM_REPORT_READ,
        ProfileBuilderPermission.CUSTOM_REPORT_WRITE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project')
        .withTestCaseLibraryNodes([new TestCaseBuilder('Test')])
        .withCampaignLibraryNodes([new CampaignBuilder('Campaign')])
        .withCustomReportLibraryNode([
          new ChartBuilder(
            'Chart',
            'REQUIREMENT_CRITICALITY',
            ChartType.PIE,
            ChartScopeType.DEFAULT,
            ChartOperation.COUNT,
            EntityType.PROJECT,
          ),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}
function selectChartAndModifyNameChart(
  customReportWorkspacePage: CustomReportWorkspacePage,
  chartDefinitionViewPage: ChartDefinitionViewPage,
) {
  selectNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project', 'Chart');
  chartDefinitionViewPage.assertExists();
  chartDefinitionViewPage.nameTextField.setValue('Graphique');
  chartDefinitionViewPage.nameTextField.clickOnConfirmButton();
  chartDefinitionViewPage.nameTextField.assertContainsText('Graphique');
}

function modifyChartDefinitionInCustomReportWorkspace(
  chartDefinitionViewPage: ChartDefinitionViewPage,
) {
  const modifyChartViewPage = chartDefinitionViewPage.clickModifyButton();
  modifyChartDefinition(modifyChartViewPage, 1, 5, 2);
  chartDefinitionViewPage.checkAxisInfo(' Exigences | Statut - Agrégation ');
  chartDefinitionViewPage.checkFilterInfo(0, ' Exigences | Nombre de versions - Supérieur : 1 ');
}
