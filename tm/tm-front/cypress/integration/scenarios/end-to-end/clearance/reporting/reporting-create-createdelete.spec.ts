import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { CampaignBuilder } from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { CustomReportWorkspacePage } from '../../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import { TreeElement } from '../../../../page-objects/elements/grid/grid.element';
import { CustomReportFolderViewPage } from '../../../../page-objects/pages/custom-report-workspace/custom-report-folder-view.page';
import { ChartDefinitionViewPage } from '../../../../page-objects/pages/custom-report-workspace/chart/chart-definition-view.page';
import { TestCaseBuilder } from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { ReportDefinitionViewPage } from '../../../../page-objects/pages/custom-report-workspace/report-definition-view.page';
import { DashboardViewPage } from '../../../../page-objects/pages/custom-report-workspace/dashboard-view.page';
import { CustomExportViewPage } from '../../../../page-objects/pages/custom-report-workspace/custom-export-view.page';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can create folder, dashboard, chart, report, custom export in custom report workspace ', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07041.CT01- reporting-chart-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage();
    const customReportWorkspaceTree = customReportWorkspacePage.tree;
    const customReportFolderViewPage = new CustomReportFolderViewPage(1);
    const chartDefinitionViewPage = new ChartDefinitionViewPage(1);
    const reportDefinitionViewPage = new ReportDefinitionViewPage(1);
    const dashboardViewPage = new DashboardViewPage(1);
    const customExportViewPage = new CustomExportViewPage(1);

    createFolderInCustomReportWorkspace(
      customReportWorkspacePage,
      customReportWorkspaceTree,
      customReportFolderViewPage,
    );

    cy.log('Step 2');
    createChartInCustomReportWorkspace(
      customReportWorkspacePage,
      customReportWorkspaceTree,
      chartDefinitionViewPage,
    );

    cy.log('Step 3');
    createReportInCustomReportWorkspace(
      customReportWorkspacePage,
      customReportWorkspaceTree,
      reportDefinitionViewPage,
    );

    cy.log('Step 4');
    createDashboardInCustomReportWorkspace(customReportWorkspacePage, dashboardViewPage);

    cy.log('Step 5');
    createCustomExportInCustomReportWorkspace(customReportWorkspacePage, customExportViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('reporting create', [
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CUSTOM_REPORT_READ,
        ProfileBuilderPermission.CUSTOM_REPORT_CREATE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project')
        .withTestCaseLibraryNodes([new TestCaseBuilder('Test')])
        .withCampaignLibraryNodes([new CampaignBuilder('Campaign')]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}
function createFolderInCustomReportWorkspace(
  customReportWorkspacePage: CustomReportWorkspacePage,
  customReportWorkspaceTree: TreeElement,
  customReportFolderViewPage: CustomReportFolderViewPage,
) {
  customReportWorkspaceTree.findRowId('NAME', 'Project').then((idProject) => {
    customReportWorkspaceTree.selectNode(idProject);
  });
  const entityDialogFolder = customReportWorkspacePage.treeMenu.openCreateFolder();
  entityDialogFolder.fillName('Folder');
  entityDialogFolder.clickOnAddButton();
  customReportFolderViewPage.assertExists();
}

function createChartInCustomReportWorkspace(
  customReportWorkspacePage: CustomReportWorkspacePage,
  customReportWorkspaceTree: TreeElement,
  chartDefinitionViewPage: ChartDefinitionViewPage,
) {
  customReportWorkspaceTree.findRowId('NAME', 'Project').then((idProject) => {
    customReportWorkspaceTree.selectNode(idProject);
  });
  const createChart = customReportWorkspacePage.treeMenu.openCreateChart();
  createChart.axisSelector.openAttributeSelector();
  createChart.axisSelector.addAttributeAndRenderPreview(5);
  createChart.saveChart();
  chartDefinitionViewPage.assertExists();
}

function createReportInCustomReportWorkspace(
  customReportWorkspacePage: CustomReportWorkspacePage,
  customReportWorkspaceTree: TreeElement,
  reportDefinitionViewPage: ReportDefinitionViewPage,
) {
  customReportWorkspaceTree.findRowId('NAME', 'Project').then((idProject) => {
    customReportWorkspaceTree.selectNode(idProject);
  });
  const createReport = customReportWorkspacePage.treeMenu.openCreateReport();
  createReport.selectReport('legacy-tcs-book.testcases.report.label');
  createReport.nameField.fill('Report');
  createReport.saveReport();
  reportDefinitionViewPage.assertExists();
}

function createDashboardInCustomReportWorkspace(
  customReportWorkspacePage: CustomReportWorkspacePage,
  dashboardViewPage: DashboardViewPage,
) {
  const createDashboard = customReportWorkspacePage.treeMenu.openCreateDashboard();
  createDashboard.fillName('Dashboard');
  createDashboard.clickOnAddButton();
  dashboardViewPage.assertExists();
}

function createCustomExportInCustomReportWorkspace(
  customReportWorkspacePage: CustomReportWorkspacePage,
  customExportViewPage: CustomExportViewPage,
) {
  const createCustomExport = customReportWorkspacePage.treeMenu.openCreateCustomExport();
  createCustomExport.nameField.fill('Custom Export');
  const campaignTreeElement = TreeElement.createTreeElement(
    'campaign-tree-picker',
    'campaign-tree',
  );
  createCustomExport.clickScopeButton();
  campaignTreeElement.findRowId('NAME', 'Project').then((idProject) => {
    campaignTreeElement.openNodeIfClosed(idProject);
  });
  campaignTreeElement.findRowId('NAME', 'Campaign').then((idRequirement) => {
    campaignTreeElement.selectRow(idRequirement, 'NAME');
  });
  createCustomExport.clickConfirmScopeButton();
  createCustomExport.dragAndDropAttribute('Description');
  createCustomExport.clickAddButton();
  customExportViewPage.assertExists();
}
