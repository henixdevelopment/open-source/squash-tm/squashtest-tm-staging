import { DatabaseUtils } from '../../../../utils/database.utils';
import { CustomReportWorkspacePage } from '../../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import { ReportDefinitionViewPage } from '../../../../page-objects/pages/custom-report-workspace/report-definition-view.page';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { TestCaseBuilder } from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { ReportBuilder } from '../../../../utils/end-to-end/prerequisite/builders/custom-report-prerequisite-builders';
import { PluginNameSpace } from '../../../../../../projects/sqtm-core/src/lib/model/custom-report/report-definition.model';
import {
  modifyReportDefinitionWithValues,
  selectNodeInCustomReportWorkspace,
} from '../../../scenario-parts/reporting.part';
import { mockReportDefinitionParameterTestCase } from '../../../../data-mock/report-definition.data-mock';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a Project Leader can view and edit a report in custom report workspace ', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07040.CT03 - reporting-report-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage();
    const reportDefinitionViewPage = new ReportDefinitionViewPage(1);
    selectReportAndAssertReportDefinitionView(customReportWorkspacePage, reportDefinitionViewPage);

    cy.log('Step 2');
    modifyReportDefinitionInCustomReportWorkspace(reportDefinitionViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('reporting read/write', [
        ProfileBuilderPermission.TEST_CASE_READ,
        ProfileBuilderPermission.CUSTOM_REPORT_READ,
        ProfileBuilderPermission.CUSTOM_REPORT_WRITE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project')
        .withTestCaseLibraryNodes([new TestCaseBuilder('Classique')])
        .withCustomReportLibraryNode([
          new ReportBuilder(
            'Report',
            PluginNameSpace.TEST_CASE_REPORT,
            JSON.stringify(mockReportDefinitionParameterTestCase()),
          ),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}
function selectReportAndAssertReportDefinitionView(
  customReportWorkspacePage: CustomReportWorkspacePage,
  reportDefinitionViewPage: ReportDefinitionViewPage,
) {
  selectNodeInCustomReportWorkspace(customReportWorkspacePage, 'Project', 'Report');
  reportDefinitionViewPage.assertExists();
}

function modifyReportDefinitionInCustomReportWorkspace(
  reportDefinitionViewPage: ReportDefinitionViewPage,
) {
  const modifyReportDefinitionViewPage =
    reportDefinitionViewPage.clickModifyButtonInReportDefinitionView();
  modifyReportDefinitionViewPage.assertExists();
  modifyReportDefinitionWithValues(modifyReportDefinitionViewPage, 'lala', 'Description');
  reportDefinitionViewPage.nameTextField.assertContainsText('lala');
  reportDefinitionViewPage.showInformationPanel().assertDescriptionContains('Description');
  reportDefinitionViewPage.showInformationPanel().assertAttributeExistsWithIndex(1);
  reportDefinitionViewPage.showInformationPanel().assertAttributeExistsWithIndex(3);
}
