import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  ExploratoryTestCaseBuilder,
  KeywordTestCaseBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CampaignBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import {
  modifyAttributesInItemSearchPage,
  searchItemInSearchPageFromCampaignWorkspace,
} from '../../../scenario-parts/campaign.part';
import { ItpiSearchPage } from '../../../../page-objects/pages/campaign-workspace/search/itpi-search-page';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import { ExecutionStatusColor } from '../../../../utils/color.utils';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can use the search functionality in the campaign workspace', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07030.CT06 - campaign-search-readwrite', () => {
    cy.log('step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();
    const gridITPI = new GridElement('itpi-search');
    const itpiSearchPage = new ItpiSearchPage(gridITPI);

    assertItemCanBeSearchedByNameInSearchPage(campaignWorkspace, itpiSearchPage);

    cy.log('step 2');
    assertAttributesCanBeModifyInSearchPage(itpiSearchPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('execution session view', [
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CAMPAIGN_WRITE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Hello')
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Classique'),
          new KeywordTestCaseBuilder('BDD'),
          new ScriptedTestCaseBuilder('Gherkin'),
          new ExploratoryTestCaseBuilder('Exploratoire'),
        ])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaign').withIterations([
            new IterationBuilder('Iteration').withTestPlanItems([
              new IterationTestPlanItemBuilder('Classique'),
              new IterationTestPlanItemBuilder('BDD'),
              new IterationTestPlanItemBuilder('Gherkin'),
              new IterationTestPlanItemBuilder('Exploratoire'),
            ]),
          ]),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}
function assertItemCanBeSearchedByNameInSearchPage(
  campaignWorkspace: CampaignWorkspacePage,
  itpiSearchPage: ItpiSearchPage,
) {
  searchItemInSearchPageFromCampaignWorkspace(campaignWorkspace, 'Hello', true, 'BDD');
  itpiSearchPage.grid.assertRowCount(1);
  itpiSearchPage.grid.findRowId('label', 'BDD').then((idItem) => {
    itpiSearchPage.grid.assertRowExist(idItem);
  });
  itpiSearchPage.clickOnNewSearch();
  itpiSearchPage.grid.assertSpinnerIsNotPresent();
}

function assertAttributesCanBeModifyInSearchPage(itpiSearchPage: ItpiSearchPage) {
  modifyAttributesInItemSearchPage(itpiSearchPage, ['Classique'], 'Non testable');
  itpiSearchPage.grid.assertSpinnerIsNotPresent();
  modifyAttributesInItemSearchPage(itpiSearchPage, ['Gherkin'], 'Arbitré');
  itpiSearchPage.grid.assertSpinnerIsNotPresent();
  itpiSearchPage.grid.findRowId('label', 'Classique').then((idRow) => {
    itpiSearchPage.grid
      .getCell(idRow, 'executionStatus')
      .findCell()
      .find('sqtm-core-execution-status-cell')
      .find('div')
      .find('div')
      .should('have.css', 'background-color', ExecutionStatusColor.NOT_FOUND_OR_UNTESTABLE);
  });
  itpiSearchPage.grid.findRowId('label', 'Gherkin').then((idRow) => {
    itpiSearchPage.grid
      .getCell(idRow, 'executionStatus')
      .findCell()
      .find('sqtm-core-execution-status-cell')
      .find('div')
      .find('div')
      .should('have.css', 'background-color', ExecutionStatusColor.SETTLED_OR_WARNING);
  });
}
