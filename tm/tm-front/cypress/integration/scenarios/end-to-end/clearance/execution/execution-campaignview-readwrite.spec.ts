import { DatabaseUtils } from '../../../../utils/database.utils';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import {
  ExploratoryTestCaseBuilder,
  KeywordTestCaseBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CampaignBuilder,
  CampaignTestPlanItemBuilder,
  IterationBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  assertItemsInTestPlanExist,
  assignUserToItemInTestPlan,
  editFieldInPlanningView,
  editFieldInPlanningViewWithCalendar,
  renameCampaignIterationCampaignFolderAndTestSuiteInCampaignWorkspace,
  reorderItemsInTestPlan,
  selectNodeInCampaignWorkspace,
  toggleAutoCheckboxInPlanningFields,
} from '../../../scenario-parts/campaign.part';
import { CampaignViewPage } from '../../../../page-objects/pages/campaign-workspace/campaign/campaign-view.page';
import { IterationViewPage } from '../../../../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import { CampaignViewPlanningPage } from '../../../../page-objects/pages/campaign-workspace/campaign/campaign-view-planning.page';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  MilestoneBuilder,
  MilestoneOnProjectBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/milestone-prerequisite-builders';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
  SystemProfile,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can interact with a campaign, an iteration and a test plan', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07030.CT02 - campaign-campaignview-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();
    const campaignViewPage = new CampaignViewPage(1);
    const campaignPlanningViewPage = new CampaignViewPlanningPage(campaignViewPage);
    const iterationViewPage = new IterationViewPage();

    selectCampaignAndAssertCampaignView(campaignWorkspace, campaignViewPage);

    cy.log('Step 2');
    modifyCampaignAttributes(campaignViewPage);

    cy.log('Step 3');
    bindAndUnbindMilestone(campaignViewPage);

    cy.log('Step 4');
    changeDateAndToggleCheckboxInPlanningViewOfCampaign();

    cy.log('Step 5');
    changeDateCalendarInIterationInCampaignView(
      campaignPlanningViewPage,
      campaignWorkspace,
      iterationViewPage,
    );

    cy.log('Step 6');
    checkTestPlanExecutionInCampaignView(campaignWorkspace);

    cy.log('Step 7');
    assignUserToItemInTestPlan('campaign', 'BDD', 'item-3', 'Harry Potter');

    cy.log('Step 8');
    modifyOrderItem(campaignViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([
      new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley'),
      new UserBuilder('HarryP').withFirstName('Harry').withLastName('Potter'),
    ])
    .withProfiles([
      new ProfileBuilder('campaign view', [
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CAMPAIGN_READ_UNASSIGNED,
        ProfileBuilderPermission.CAMPAIGN_WRITE,
        ProfileBuilderPermission.CAMPAIGN_LINK,
      ]),
    ])
    .withCufs([new CustomFieldBuilder('Checkbox', InputType.CHECKBOX)])
    .withMilestones([new MilestoneBuilder('Abc', '2025-10-31 14:44:45').withUserLogin('RonW')])
    .withProjects([
      new ProjectBuilder('HelloWordl')
        .withCufsOnProject([new CustomFieldOnProjectBuilder('Checkbox', BindableEntity.CAMPAIGN)])
        .withMilestonesOnProject([new MilestoneOnProjectBuilder('Abc')])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Classique'),
          new KeywordTestCaseBuilder('BDD'),
          new ScriptedTestCaseBuilder('Gherkin'),
          new ExploratoryTestCaseBuilder('Exploratoire'),
        ])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaign')
            .withTestPlanItems([
              new CampaignTestPlanItemBuilder('Classique'),
              new CampaignTestPlanItemBuilder('BDD'),
              new CampaignTestPlanItemBuilder('Gherkin'),
              new CampaignTestPlanItemBuilder('Exploratoire'),
            ])
            .withIterations([new IterationBuilder('Iteration')]),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
  addClearanceToUser(3, SystemProfile.PROJECT_MANAGER, [1]);
}
function selectCampaignAndAssertCampaignView(
  campaignWorkspace: CampaignWorkspacePage,
  campaignViewPage: CampaignViewPage,
) {
  selectNodeInCampaignWorkspace(campaignWorkspace, 'HelloWordl', 'Campaign');
  campaignViewPage.assertExists();
}

function modifyCampaignAttributes(campaignViewPage: CampaignViewPage) {
  renameCampaignIterationCampaignFolderAndTestSuiteInCampaignWorkspace('Campaign', 'Bonjour');
  campaignViewPage.clickInformationAnchorLink().campaignStatusField.selectValue('En cours');
  campaignViewPage.clickInformationAnchorLink().campaignStatusField.assertContainsText('En cours');
  campaignViewPage.getCheckboxCustomField('Checkbox').click();
  campaignViewPage.getCheckboxCustomField('Checkbox').checkState(true);
}

function bindAndUnbindMilestone(campaignViewPage: CampaignViewPage) {
  campaignViewPage.bindMilestoneToCampaign(1, true);
  campaignViewPage.milestoneTagElement.clickOnRemoveMilestoneClose('Abc');
}

function changeDateAndToggleCheckboxInPlanningViewOfCampaign() {
  editFieldInPlanningViewWithCalendar('campaign', 'scheduledStartDate');
  toggleAutoCheckboxInPlanningFields('actualEndAuto', true);
  toggleAutoCheckboxInPlanningFields('actualEndAuto', false);
  editFieldInPlanningView('campaign', 'actualEndDate', '10/12/2025');
}

function changeDateCalendarInIterationInCampaignView(
  campaignPlanningViewPage: CampaignViewPlanningPage,
  campaignWorkspace: CampaignWorkspacePage,
  iterationViewPage: IterationViewPage,
) {
  campaignPlanningViewPage.editIterationScheduleAndCheckModification(
    1,
    '10/12/2025',
    '10/12/2025',
    campaignWorkspace,
    iterationViewPage,
    'Bonjour',
    'Iteration',
  );
}

function checkTestPlanExecutionInCampaignView(campaignWorkspace: CampaignWorkspacePage) {
  selectNodeInCampaignWorkspace(campaignWorkspace, 'HelloWordl', 'Bonjour');
  assertItemsInTestPlanExist('campaign', ['Classique', 'BDD', 'Gherkin', 'Exploratoire'], 4);
}

function modifyOrderItem(campaignViewPage: CampaignViewPage) {
  reorderItemsInTestPlan('campaign', '1', '2');
  const campaignTestPlan = campaignViewPage.showTestPlan();
  campaignTestPlan.assertTestCasePositionInExecutionPlan('BDD', 1);
  campaignTestPlan.assertTestCasePositionInExecutionPlan('Classique', 2);
}
