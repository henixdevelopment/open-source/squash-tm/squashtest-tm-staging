import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  BugtrackerProjectBindingBuilder,
  BugtrackerServerBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/server-prerequisite-builders';
import {
  CampaignBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  ActionTestStepBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import {
  createExecutionWithPopUpInIterationAndTestSuite,
  selectNodeInNodeInCampaignWorkspace,
  showTestPlanInCampaignOrIterationOrTestSuite,
} from '../../../scenario-parts/campaign.part';
import { ExecutionRunnerProloguePage } from '../../../../page-objects/pages/execution/execution-runner-prologue-page';
import { EndOfExecutionDialogElement } from '../../../../page-objects/pages/execution/dialog/end-of-execution-dialog-element';
import { IterationTestPlanPage } from '../../../../page-objects/pages/campaign-workspace/iteration/iteration-test-plan.page';
import { selectByDataTestCellId } from '../../../../utils/basic-selectors';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import { ExecutionStatusColor } from '../../../../utils/color.utils';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can execute a testcase in execution plan and add an attribute', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07035.CT04', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();
    const executionRunnerProloguePage = new ExecutionRunnerProloguePage();
    const endOfExecutionDialog = new EndOfExecutionDialogElement();

    startExecutionWithPopInIterationTestPlanPage(campaignWorkspace, executionRunnerProloguePage);

    cy.log('Step 2');
    assignExecutionStatus(executionRunnerProloguePage, endOfExecutionDialog);
    assertExecutionStatusChangedInIterationTestPlanPage(campaignWorkspace);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('iteration execute', [
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CAMPAIGN_READ_UNASSIGNED,
        ProfileBuilderPermission.CAMPAIGN_EXECUTE,
      ]),
    ])
    .withServers([new BugtrackerServerBuilder('Hello', 'https://gitlab.com', 'gitlab.bugtracker')])
    .withProjects([
      new ProjectBuilder('France')
        .withBugtrackerOnProject([new BugtrackerProjectBindingBuilder()])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaign').withIterations([
            new IterationBuilder('Iteration').withTestPlanItems([
              new IterationTestPlanItemBuilder('Classic'),
            ]),
          ]),
        ])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Classic').withSteps([new ActionTestStepBuilder('a', 'b')]),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}
function startExecutionWithPopInIterationTestPlanPage(
  campaignWorkspace: CampaignWorkspacePage,
  executionRunnerProloguePage: ExecutionRunnerProloguePage,
) {
  selectNodeInNodeInCampaignWorkspace(campaignWorkspace, ['France', 'Campaign', 'Iteration']);
  createExecutionWithPopUpInIterationAndTestSuite('iteration', 'Classic', '1');
  executionRunnerProloguePage.assertExists();
}

function assignExecutionStatus(
  executionRunnerProloguePage: ExecutionRunnerProloguePage,
  endOfExecutionDialog: EndOfExecutionDialogElement,
) {
  const executionRunnerStepPage = executionRunnerProloguePage.startExecution();

  executionRunnerStepPage.failureButton.changeStatus(1, 1);
  endOfExecutionDialog.confirm();
}

function assertExecutionStatusChangedInIterationTestPlanPage(
  campaignWorkspace: CampaignWorkspacePage,
) {
  CampaignWorkspacePage.navigateToCampaignWorkspace();
  selectNodeInNodeInCampaignWorkspace(campaignWorkspace, ['France', 'Campaign', 'Iteration']);
  const iterationTestPlanPage = showTestPlanInCampaignOrIterationOrTestSuite(
    'iteration',
  ) as IterationTestPlanPage;
  iterationTestPlanPage.testPlan.findRowId('testCaseName', 'Classic').then((idItem) => {
    iterationTestPlanPage.testPlan
      .getCell(idItem, 'executionStatus')
      .findCell()
      .find(selectByDataTestCellId('execution-status-cell'))
      .should('have.css', 'background-color', ExecutionStatusColor.FAILURE);
  });
}
