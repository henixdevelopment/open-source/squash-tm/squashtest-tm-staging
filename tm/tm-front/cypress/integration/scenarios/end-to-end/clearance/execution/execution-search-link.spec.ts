import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  ExploratoryTestCaseBuilder,
  KeywordTestCaseBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CampaignBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { addItpiToExecutionPlanOfAnIteration } from '../../../scenario-parts/campaign.part';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { GridElement } from '../../../../page-objects/elements/grid/grid.element';
import { ItpiSearchPage } from '../../../../page-objects/pages/campaign-workspace/search/itpi-search-page';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Assert that a user can add an ITPI to the test plan of a campaign and an iteration from search page', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07032.CT04 - campaign-search-link ', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();
    const gridItpi = new GridElement('itpi-search');
    const searchPage = new ItpiSearchPage(gridItpi);

    linkItpiToAnIterationInSearchPage(campaignWorkspace, searchPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('execution search link', [
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CAMPAIGN_LINK,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Clio')
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Classic'),
          new ExploratoryTestCaseBuilder('Exploratory'),
          new KeywordTestCaseBuilder('BDD'),
          new ScriptedTestCaseBuilder('Gherkin'),
        ])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaign').withIterations([
            new IterationBuilder('Iteration').withTestPlanItems([
              new IterationTestPlanItemBuilder('Classic'),
              new IterationTestPlanItemBuilder('Exploratory'),
              new IterationTestPlanItemBuilder('BDD'),
              new IterationTestPlanItemBuilder('Gherkin'),
            ]),
          ]),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}
function linkItpiToAnIterationInSearchPage(
  campaignWorkspace: CampaignWorkspacePage,
  searchPage: ItpiSearchPage,
) {
  addItpiToExecutionPlanOfAnIteration(
    campaignWorkspace,
    [1, 2, 3, 4],
    ['Clio', 'Campaign', 'Iteration'],
  );
  searchPage.grid.assertRowCount(8);
  searchPage.grid.assertGridContentOrder('label', [
    'Classic',
    'Exploratory',
    'BDD',
    'Gherkin',
    'Classic',
    'Exploratory',
    'BDD',
    'Gherkin',
  ]);
}
