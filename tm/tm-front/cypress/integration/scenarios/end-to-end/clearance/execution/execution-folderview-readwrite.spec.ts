import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { CampaignFolderBuilder } from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { CampaignFolderViewPage } from '../../../../page-objects/pages/campaign-workspace/campaign-folder/campaign-folder.page';
import {
  editDescriptionCampaignWorkspace,
  selectNodeInCampaignWorkspace,
  renameCampaignIterationCampaignFolderAndTestSuiteInCampaignWorkspace,
} from '../../../scenario-parts/campaign.part';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';
describe('Check that a user can consult the file page, rename the folder, and edit description and CUF', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07030.CT01 - campaign-folderview-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();
    const campaignFolderViewPage = new CampaignFolderViewPage(1);

    selectFolderAndAssertFolderView(campaignWorkspace, campaignFolderViewPage);

    cy.log('Step 2');
    renameCampaignIterationCampaignFolderAndTestSuiteInCampaignWorkspace(
      'Campaign-Folder',
      '4xChampion',
    );

    cy.log('Step 3');
    ediFolderDescriptionAndCufTextField(campaignFolderViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('folder view', [
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CAMPAIGN_WRITE,
      ]),
    ])
    .withCufs([new CustomFieldBuilder('Texte simple', InputType.PLAIN_TEXT)])
    .withProjects([
      new ProjectBuilder('EMEA MASTERS')
        .withCampaignLibraryNodes([new CampaignFolderBuilder('3xChampion')])
        .withCufsOnProject([
          new CustomFieldOnProjectBuilder('Texte simple', BindableEntity.CAMPAIGN_FOLDER),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}
function selectFolderAndAssertFolderView(
  campaignWorkspace: CampaignWorkspacePage,
  campaignFolderViewPage: CampaignFolderViewPage,
) {
  selectNodeInCampaignWorkspace(campaignWorkspace, 'EMEA MASTERS', '3xChampion');
  campaignFolderViewPage.assertExists();
}

function ediFolderDescriptionAndCufTextField(campaignFolderViewPage: CampaignFolderViewPage) {
  editDescriptionCampaignWorkspace('Campaign-Folder', 'KarmineCorp');
  campaignFolderViewPage.getTextCustomField('Texte simple').setAndConfirmValue('Saken');
}
