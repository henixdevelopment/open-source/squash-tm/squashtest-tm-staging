import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  CampaignBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  KeywordStepBuilder,
  KeywordTestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  BugtrackerProjectBindingBuilder,
  BugtrackerServerBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/server-prerequisite-builders';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import {
  modifyExecutionTestStepStatusFromSearchPage,
  selectNodeInNodeInCampaignWorkspace,
  showTestPlanInCampaignOrIterationOrTestSuite,
} from '../../../scenario-parts/campaign.part';
import { ExecutionPage } from '../../../../page-objects/pages/execution/execution-page';
import { AuthenticationProtocol } from '../../../../../../projects/sqtm-core/src/lib/model/third-party-server/authentication.model';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { selectByDataTestCellId } from '../../../../utils/basic-selectors';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import { ExecutionStatusColor } from '../../../../utils/color.utils';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can execute a keyword testcase and link issues to an execution', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07035.CT01 - campaign-search-execute', () => {
    cy.log('Step 1 and Step 2');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();
    const executionPage = new ExecutionPage();

    modifyExecutionStatusExecutionFromSearchPage(campaignWorkspace, executionPage);

    cy.log('Step 3');
    // TODO once ticket #3067 is implemented, the following line is te be uncommented
    // linkAndUnlinkIssueToAnExecution(executionPage);
    NavBarElement.navigateToCampaignWorkspace();
    selectNodeInNodeInCampaignWorkspace(campaignWorkspace, ['Project', 'Campaign', 'Iteration']);
    assertStatusInIterationExecutionPlan();
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('execution search execute', [
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CAMPAIGN_READ_UNASSIGNED,
        ProfileBuilderPermission.CAMPAIGN_EXECUTE,
      ]),
    ])
    .withServers([
      new BugtrackerServerBuilder('Hello', 'https://gitlab.com', 'gitlab.bugtracker')
        .withAuthProtocol(AuthenticationProtocol.TOKEN_AUTH)
        .withUserLevelAuthPolicy(),
    ])
    .withProjects([
      new ProjectBuilder('Project')
        .withBugtrackerOnProject([new BugtrackerProjectBindingBuilder()])
        .withTestCaseLibraryNodes([
          new KeywordTestCaseBuilder('BDD').withKeywordSteps([
            new KeywordStepBuilder('GIVEN', 'I sleep'),
          ]),
        ])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaign').withIterations([
            new IterationBuilder('Iteration').withTestPlanItems([
              new IterationTestPlanItemBuilder('BDD'),
            ]),
          ]),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}
function modifyExecutionStatusExecutionFromSearchPage(
  campaignWorkspace: CampaignWorkspacePage,
  executionPage: ExecutionPage,
) {
  modifyExecutionTestStepStatusFromSearchPage(campaignWorkspace, 'BDD', '1', 'SUCCESS', 1);
  executionPage.scenarioPanel.getExecutionStepById('1').checkStepExecutionStatus('Succès');
}

// function linkAndUnlinkIssueToAnExecution(executionPage: ExecutionPage) {
//   const token = Cypress.env('GITLAB_TOKEN');
//
//   addIssueToAnExecution(executionPage, token, 'henixrecette/e2e_test_do_not_delete', '1');
//   executionPage.issueGrid.assertSpinnerIsNotPresent();
//   executionPage.issueGrid.assertRowCount(1);
//   executionPage.issueGrid.findRowId('#', '1', 'leftViewport').then((idIssue) => {
//     executionPage.issueGrid
//       .getCell(idIssue, 'summary')
//       .findCell()
//       .should('contain.text', 'Fake_Issue_For_E2E');
//   });
//   removeIssueFromAnExecutionWithToolBarButton(executionPage, '#1');
//   executionPage.issueGrid.assertGridIsEmpty();
//   executionPage.issueGrid.assertSpinnerIsNotPresent();
// }

function assertStatusInIterationExecutionPlan() {
  const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite('iteration');
  testPlanPage.testPlan.findRowId('testCaseName', 'BDD').then((idTestCase) => {
    testPlanPage.testPlan
      .getCell(idTestCase, 'executionStatus')
      .findCell()
      .find(selectByDataTestCellId('execution-status-cell'))
      .should('have.css', 'background-color', ExecutionStatusColor.SUCCESS);
  });
}
