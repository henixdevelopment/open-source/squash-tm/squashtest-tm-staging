import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  BugtrackerProjectBindingBuilder,
  BugtrackerServerBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/server-prerequisite-builders';
import { AuthenticationProtocol } from '../../../../../../projects/sqtm-core/src/lib/model/third-party-server/authentication.model';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  ActionTestStepBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CampaignBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import {
  createAnIssueFromAnExecutionInExecutionPage,
  createNewExecutionOfTestInTestPlanPageAndModifyStatus,
  goToCommentPanelAndFillAComment,
  addCommentToExecutionRunnerStepPage,
  selectNodeInNodeInCampaignWorkspace,
  showTestPlanInCampaignOrIterationOrTestSuite,
  startOverAnExecution,
} from '../../../scenario-parts/campaign.part';
import { ExecutionPage } from '../../../../page-objects/pages/execution/execution-page';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { ExecutionRunnerStepPage } from '../../../../page-objects/pages/execution/execution-runner-step-page';
import { selectByDataTestCellId } from '../../../../utils/basic-selectors';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import { ExecutionStatusColor } from '../../../../utils/color.utils';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can execute a testcase and create a new issue', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07035.CT02 - campaign-iteration-testplan-execute ', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();
    const executionPage = new ExecutionPage();
    const executionRunnerStepPage = new ExecutionRunnerStepPage();

    executeTestcaseInTestPlanPage(campaignWorkspace, executionPage);

    cy.log('Step 2');
    clickAnchorCommentAndFillAComment(executionPage);

    cy.log('Step 3');
    createAnIssueInExecution(executionPage);
    assertExecutionStatusInIterationTestPlanPage(campaignWorkspace);

    cy.log('Step 4');
    restartExecutionAndAddComment(executionRunnerStepPage);

    // TODO once ticket #3067 is implemented, the following two lines are te be uncommented
    // cy.log('Step 5');
    // linkIssueToExecutionStepInRunnerStepPage(executionRunnerStepPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('iteration execute', [
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CAMPAIGN_READ_UNASSIGNED,
        ProfileBuilderPermission.CAMPAIGN_EXECUTE,
      ]),
    ])
    .withServers([
      new BugtrackerServerBuilder('Hello', 'https://gitlab.com', 'gitlab.bugtracker')
        .withAuthProtocol(AuthenticationProtocol.TOKEN_AUTH)
        .withUserLevelAuthPolicy(),
    ])
    .withProjects([
      new ProjectBuilder('henixrecette/e2e_test_do_not_delete')
        .withBugtrackerOnProject([new BugtrackerProjectBindingBuilder()])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Classique').withSteps([
            new ActionTestStepBuilder('A', 'b'),
            new ActionTestStepBuilder('C', 'D'),
          ]),
        ])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaign').withIterations([
            new IterationBuilder('Iteration').withTestPlanItems([
              new IterationTestPlanItemBuilder('Classique'),
            ]),
          ]),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}
function executeTestcaseInTestPlanPage(
  campaignWorkspace: CampaignWorkspacePage,
  executionPage: ExecutionPage,
) {
  selectNodeInNodeInCampaignWorkspace(campaignWorkspace, [
    'henixrecette/e2e_test_do_not_delete',
    'Campaign',
    'Iteration',
  ]);
  createNewExecutionOfTestInTestPlanPageAndModifyStatus(
    'iteration',
    'Classique',
    '1',
    'FAILURE',
    1,
  );
  executionPage.scenarioPanel.getExecutionStepById('1').checkStepExecutionStatus('Échec');
}

function clickAnchorCommentAndFillAComment(executionPage: ExecutionPage) {
  goToCommentPanelAndFillAComment(executionPage, 'Hello new comment');
  executionPage.commentField.assertContainsText('Hello new comment');
}

function createAnIssueInExecution(executionPage: ExecutionPage) {
  const token = Cypress.env('GITLAB_TOKEN');
  createAnIssueFromAnExecutionInExecutionPage(executionPage, token, 'Coca');
}

function assertExecutionStatusInIterationTestPlanPage(campaignWorkspace: CampaignWorkspacePage) {
  NavBarElement.navigateToCampaignWorkspace();
  selectNodeInNodeInCampaignWorkspace(campaignWorkspace, [
    'henixrecette/e2e_test_do_not_delete',
    'Campaign',
    'Iteration',
  ]);
  const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite('iteration');
  testPlanPage.testPlan.findRowId('testCaseName', 'Classique').then((idTestCase) => {
    testPlanPage.testPlan
      .getCell(idTestCase, 'executionStatus')
      .findCell()
      .find(selectByDataTestCellId('execution-status-cell'))
      .should('have.css', 'background-color', ExecutionStatusColor.FAILURE);
  });
}

function restartExecutionAndAddComment(executionRunnerStepPage: ExecutionRunnerStepPage) {
  startOverAnExecution('iteration', 1, 1, 2);
  addCommentToExecutionRunnerStepPage('Hello new comment');
  executionRunnerStepPage.checkComment('Hello new comment');
}

// function linkIssueToExecutionStepInRunnerStepPage(
//   executionRunnerStepPage: ExecutionRunnerStepPage,
// ) {
//   linkIssueToAnExecutionRunnerStepPage(executionRunnerStepPage, '1');
//   executionRunnerStepPage.issuesPanelGrid.assertSpinnerIsNotPresent();
//   executionRunnerStepPage.issuesPanelGrid.assertRowCount(1);
//   executionRunnerStepPage.issuesPanelGrid.findRowId('#', '1', 'leftViewport').then((idIssue) => {
//     executionRunnerStepPage.issuesPanelGrid
//       .getCell(idIssue, 'summary')
//       .findCell()
//       .should('contain.text', 'Fake_Issue_For_E2E');
//   });
// }
