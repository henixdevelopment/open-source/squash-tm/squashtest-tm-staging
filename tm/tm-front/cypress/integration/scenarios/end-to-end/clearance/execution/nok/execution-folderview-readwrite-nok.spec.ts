import { DatabaseUtils } from 'cypress/integration/utils/database.utils';
import { CampaignWorkspacePage } from '../../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { CampaignFolderViewPage } from '../../../../../page-objects/pages/campaign-workspace/campaign-folder/campaign-folder.page';
import { PrerequisiteBuilder } from 'cypress/integration/utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { InputType } from '../../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { CampaignFolderBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { BindableEntity } from '../../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { selectNodeInCampaignWorkspace } from '../../../../scenario-parts/campaign.part';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../../utils/end-to-end/api/add-permissions';

describe('Check that a user can consult the folder page, but may not rename the folder or edit the description or a CUF', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07230.CT01 - execution-folderview-readwrite-nok', () => {
    cy.log('Step 1');
    cy.logInAs('SamG', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();
    const campaignFolderViewPage = new CampaignFolderViewPage(1);

    selectFolderAndAssertFolderView(campaignWorkspace, campaignFolderViewPage);

    cy.log('Step 2');
    campaignFolderViewPage.nameTextField.assertIsNotEditable();

    cy.log('Step 3');
    folderDescriptionAndCufTextFieldNotEditable(campaignFolderViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withProfiles([new ProfileBuilder('execution read', [ProfileBuilderPermission.CAMPAIGN_READ])])
    .withUsers([new UserBuilder('SamG').withFirstName('Sam').withLastName('Gamgee')])
    .withCufs([new CustomFieldBuilder('Texte simple', InputType.PLAIN_TEXT)])
    .withProjects([
      new ProjectBuilder('Project')
        .withCampaignLibraryNodes([new CampaignFolderBuilder('Campaign Folder')])
        .withCufsOnProject([
          new CustomFieldOnProjectBuilder('Texte simple', BindableEntity.CAMPAIGN_FOLDER),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}
function selectFolderAndAssertFolderView(
  campaignWorkspace: CampaignWorkspacePage,
  campaignFolderViewPage: CampaignFolderViewPage,
) {
  selectNodeInCampaignWorkspace(campaignWorkspace, 'Project', 'Campaign Folder');
  campaignFolderViewPage.assertExists();
}

function folderDescriptionAndCufTextFieldNotEditable(
  campaignFolderViewPage: CampaignFolderViewPage,
) {
  campaignFolderViewPage.descriptionRichField.assertIsNotEditable();
  campaignFolderViewPage.getTextCustomField('Texte simple').assertIsNotEditable();
}
