import { DatabaseUtils } from '../../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { CampaignWorkspacePage } from '../../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import {
  MilestoneBuilder,
  MilestoneOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/milestone-prerequisite-builders';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { BindableEntity } from '../../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import {
  ExploratoryTestCaseBuilder,
  KeywordTestCaseBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CampaignBuilder,
  CampaignTestPlanItemBuilder,
  IterationBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  addPermissionToProject,
  ApiPermissionGroup,
} from '../../../../../utils/end-to-end/api/add-permissions';
import {
  assertItemsInTestPlanExist,
  reorderItemsInTestPlan,
  selectNodeInCampaignWorkspace,
  showTestPlanInCampaignOrIterationOrTestSuite,
} from '../../../../scenario-parts/campaign.part';
import { CampaignViewPage } from '../../../../../page-objects/pages/campaign-workspace/campaign/campaign-view.page';
import { CheckBoxElement } from '../../../../../page-objects/elements/forms/check-box.element';

describe('Check if a user with Guest access cannot perform the following actions on the campaign consultation page: modify attributes, edit the iteration schedule, associate and dissociate a milestone."', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07230.CT02 - campaign-campaignview-readwrite (Guest)', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();
    const campaignViewPage = new CampaignViewPage(1);

    selectNodeInCampaignWorkspace(campaignWorkspace, 'HelloWordl', 'Campaign');
    campaignViewPage.assertExists();

    cy.log('Step 2');
    assertCannotModifyCampaignAttributes(campaignViewPage);

    cy.log('Step 3');
    campaignViewPage.milestoneTagElement.openMilestoneDialogNotExist();
    campaignViewPage.milestoneTagElement.removeMilestoneButtonShouldNotExist('Milestone');

    cy.log('Step 4');
    assertCannotChangeDateAndToggleCheckboxInPlanningViewOfCampaign(campaignViewPage);

    cy.log('Step 5');
    campaignViewPage.assertIconNotExists('sqtm-core-campaign:calendar');

    cy.log('Step 6');
    assertItemsInTestPlanExist('campaign', ['Classique', 'BDD', 'Gherkin', 'Exploratoire'], 4);

    cy.log('Step 7');
    assertCannotAssignUserToItemInTestPlan();

    cy.log('Step 8');
    assertCannotModifyOrderItemTestPlan();
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withCufs([new CustomFieldBuilder('Checkbox', InputType.CHECKBOX)])
    .withMilestones([
      new MilestoneBuilder('Milestone', '2025-10-31 14:44:45').withUserLogin('RonW'),
      new MilestoneBuilder('Jalon', '2025-10-31 14:44:45').withUserLogin('RonW'),
    ])
    .withProjects([
      new ProjectBuilder('HelloWordl')
        .withCufsOnProject([new CustomFieldOnProjectBuilder('Checkbox', BindableEntity.CAMPAIGN)])
        .withMilestonesOnProject([
          new MilestoneOnProjectBuilder('Milestone'),
          new MilestoneOnProjectBuilder('Jalon'),
        ])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Classique'),
          new KeywordTestCaseBuilder('BDD'),
          new ScriptedTestCaseBuilder('Gherkin'),
          new ExploratoryTestCaseBuilder('Exploratoire'),
        ])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaign')
            .withMilestone('Milestone')
            .withTestPlanItems([
              new CampaignTestPlanItemBuilder('Classique'),
              new CampaignTestPlanItemBuilder('BDD'),
              new CampaignTestPlanItemBuilder('Gherkin'),
              new CampaignTestPlanItemBuilder('Exploratoire'),
            ])
            .withIterations([new IterationBuilder('Iteration')]),
        ]),
    ])
    .build();
}
function initPermissions() {
  addPermissionToProject(1, ApiPermissionGroup.PROJECT_VIEWER, [2]);
}

function assertCannotModifyCampaignAttributes(campaignViewPage: CampaignViewPage) {
  const campaignViewInformationPage = campaignViewPage.clickInformationAnchorLink();
  campaignViewInformationPage.assertExists();
  campaignViewInformationPage.nameTextField.assertIsNotEditable();
  campaignViewInformationPage.campaignStatusField.assertNotEditable();
  campaignViewPage.getCheckboxCustomField('Checkbox').click();
  campaignViewPage.getCheckboxCustomField('Checkbox').findAndCheckState('span', false);
}

function assertCannotChangeDateAndToggleCheckboxInPlanningViewOfCampaign(
  campaignViewPage: CampaignViewPage,
) {
  const checkboxAutoEndDate = new CheckBoxElement('actualEndAuto');
  const planningPage = campaignViewPage.clickPlanningAnchorLink();
  planningPage.scheduledStartDateField.assertIsNotEditable();
  checkboxAutoEndDate.click();
  checkboxAutoEndDate.findAndCheckState('span', false);
  planningPage.actualEndDateField.assertIsNotEditable();
}

function assertCannotAssignUserToItemInTestPlan() {
  const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite('campaign');
  testPlanPage.testPlan.findRowId('testCaseName', 'BDD').then((idTestCase) => {
    testPlanPage.testPlan
      .getCell(idTestCase, 'assigneeFullName')
      .selectRenderer()
      .assertNotClickable();
  });
}

function assertCannotModifyOrderItemTestPlan() {
  reorderItemsInTestPlan('campaign', '1', '2');
  const campaignTestPlanPage = showTestPlanInCampaignOrIterationOrTestSuite('campaign');
  campaignTestPlanPage.testPlan.findRowId('testCaseName', 'BDD').then((idTestCase) => {
    campaignTestPlanPage.testPlan
      .getCell(idTestCase, '#', 'leftViewport')
      .findCellTextSpan()
      .contains('2');
  });
  campaignTestPlanPage.testPlan.findRowId('testCaseName', 'Classique').then((idTestCase) => {
    campaignTestPlanPage.testPlan
      .getCell(idTestCase, '#', 'leftViewport')
      .findCellTextSpan()
      .contains('1');
  });
}
