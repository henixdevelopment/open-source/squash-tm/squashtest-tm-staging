import { DatabaseUtils } from '../../../../../utils/database.utils';
import { CampaignWorkspacePage } from '../../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { BindableEntity } from '../../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import {
  DataSetBuilder,
  DataSetParameterValueBuilder,
  ExploratoryTestCaseBuilder,
  KeywordTestCaseBuilder,
  ParameterBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CampaignBuilder,
  ClassicExecutionBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
  TestSuiteBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../../utils/end-to-end/api/add-permissions';
import {
  filterColumnMode,
  selectNodeInNodeInCampaignWorkspace,
  showTestPlanInCampaignOrIterationOrTestSuite,
} from '../../../../scenario-parts/campaign.part';
import { TestSuiteViewPage } from '../../../../../page-objects/pages/campaign-workspace/test-suite/test-suite-view.page';
import { EditableDateFieldElement } from '../../../../../page-objects/elements/forms/editable-date-field.element';
import { TestSuiteTestPlanPage } from '../../../../../page-objects/pages/campaign-workspace/test-suite/test-suite-test-plan.page';
import { selectByDataTestButtonId } from '../../../../../utils/basic-selectors';
import { TestSuiteTpiMultiEditDialogElement } from '../../../../../page-objects/pages/campaign-workspace/dialogs/test-suite-tpi-multi-edit-dialog.element';
import { ListPanelElement } from '../../../../../page-objects/elements/filters/list-panel.element';
import { MenuItemElement } from '../../../../../utils/menu.element';
import { ExecutionHistoryDialog } from '../../../../../page-objects/pages/campaign-workspace/dialogs/itpi-execution-history-dialog.element';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can consult the testsuite page and edit name, description, CUF and execution plan', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07030.CT04 - campaign-testsuiteview-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();
    const testSuiteViewPage = new TestSuiteViewPage();

    selectNodeInNodeInCampaignWorkspace(campaignWorkspace, [
      'Project',
      'Campaign',
      'Iteration',
      'Test-suite',
    ]);
    testSuiteViewPage.assertExists();

    cy.log('Step 2');
    assertCannotEditSuiteAttributes(testSuiteViewPage);

    cy.log('Step 3');
    const testSuiteTestPlanPage = showTestPlanInCampaignOrIterationOrTestSuite(
      'test-suite',
    ) as TestSuiteTestPlanPage;
    testSuiteTestPlanPage.assertExists();

    cy.log('Step 4');
    assertCannotEditMultipleAttributes(testSuiteTestPlanPage);

    cy.log('Step 5');
    assertCannotModifyDataSet(testSuiteTestPlanPage);

    cy.log('Step 6');
    assertCannotDeleteHistoryExecution(testSuiteTestPlanPage);

    cy.log('Step 7');
    assertColumnFilterMode(testSuiteTestPlanPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withCufs([new CustomFieldBuilder('Date', InputType.DATE_PICKER)])
    .withProfiles([new ProfileBuilder('campaign read', [ProfileBuilderPermission.CAMPAIGN_READ])])
    .withProjects([
      new ProjectBuilder('Project')
        .withCufsOnProject([new CustomFieldOnProjectBuilder('Date', BindableEntity.TEST_SUITE)])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Classic')
            .withParameters([new ParameterBuilder('Nom')])
            .withDataSets([
              new DataSetBuilder('Team').withParamValues([
                new DataSetParameterValueBuilder('Name', 'Eagle'),
              ]),
            ]),
          new KeywordTestCaseBuilder('BDD'),
          new ScriptedTestCaseBuilder('Gherkin'),
          new ExploratoryTestCaseBuilder('Exploratory'),
        ])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaign').withIterations([
            new IterationBuilder('Iteration')
              .withTestPlanItems([
                new IterationTestPlanItemBuilder('Gherkin').withUserLogin('RonW'),
                new IterationTestPlanItemBuilder('BDD').withUserLogin('RonW'),
                new IterationTestPlanItemBuilder('Classic')
                  .withUserLogin('RonW')
                  .withExecutions([new ClassicExecutionBuilder()]),
                new IterationTestPlanItemBuilder('Exploratory').withUserLogin('RonW'),
              ])
              .withTestSuites([
                new TestSuiteBuilder('Test-suite').withTestCaseNames([
                  'Classic',
                  'BDD',
                  'Gherkin',
                  'Exploratory',
                ]),
              ]),
          ]),
        ]),
    ])
    .build();
}

function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}

function assertCannotEditSuiteAttributes(testSuiteViewPage: TestSuiteViewPage) {
  testSuiteViewPage.nameTextField.assertIsNotEditable();
  testSuiteViewPage.descriptionRichField.assertIsNotEditable();
  const date = testSuiteViewPage.getCustomField(
    'Date',
    InputType.DATE_PICKER,
  ) as EditableDateFieldElement;
  date.assertIsNotEditable();
}

function assertCannotEditMultipleAttributes(testSuiteTestPlanPage: TestSuiteTestPlanPage) {
  const massEditDialog = new TestSuiteTpiMultiEditDialogElement();
  testSuiteTestPlanPage.testPlan.findRowId('testCaseName', 'Classic').then((idTestCase) => {
    testSuiteTestPlanPage.testPlan.selectRow(idTestCase, '#', 'leftViewport');
  });
  testSuiteTestPlanPage.find(selectByDataTestButtonId('mass-edit')).click({ force: true });
  massEditDialog.assertNotExist();
}

function assertCannotModifyDataSet(testSuiteTestPlanPage: TestSuiteTestPlanPage) {
  const listItem = new ListPanelElement();

  testSuiteTestPlanPage.testPlan.findRowId('testCaseName', 'Classic').then((idTestCase) => {
    testSuiteTestPlanPage.testPlan.scrollPosition('center', 'mainViewport');
    testSuiteTestPlanPage.testPlan.toggleRow(idTestCase, 'dataset-cell');
    listItem.assertNotExist();
  });
}

function assertCannotDeleteHistoryExecution(testSuiteTestPlanPage: TestSuiteTestPlanPage) {
  const menuExecution = new MenuItemElement('', 'play-exec');
  const executionHistoryDialog = new ExecutionHistoryDialog();

  testSuiteTestPlanPage.testPlan.findRowId('testCaseName', 'Classic').then((idTestCase) => {
    testSuiteTestPlanPage.testPlan
      .getCell(idTestCase, 'startExecution', 'rightViewport')
      .find('svg')
      .click({ force: true });
    menuExecution.selectLinkElement('show-execution-history');
  });
  executionHistoryDialog.assertExists();
  executionHistoryDialog.gridElement.findRowId('executionName', 'Classic').then((idExecution) => {
    executionHistoryDialog.gridElement.getRow(idExecution).cell('delete').isNotVisible();
  });
  executionHistoryDialog.close();
}

function assertColumnFilterMode(testSuiteTestPlanPage: TestSuiteTestPlanPage) {
  testSuiteTestPlanPage.testPlan.findRowId('testCaseName', 'Exploratory').then((idTestCase) => {
    filterColumnMode('test-suite', 'Manuel');
    testSuiteTestPlanPage.testPlan.assertRowNotExist(idTestCase);
    testSuiteTestPlanPage.testPlan.assertGridContentOrder('testCaseName', [
      'Gherkin',
      'BDD',
      'Classic',
    ]);
  });
}
