import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  BugtrackerProjectBindingBuilder,
  BugtrackerServerBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/server-prerequisite-builders';
import {
  CampaignBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
  TestSuiteBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  KeywordStepBuilder,
  KeywordTestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import {
  createAnIssueFromExecutionRunnerStepPage,
  launchExecutionInTestPlanPageIterationOrTestSuite,
  selectNodeInNodeInCampaignWorkspace,
  showTestPlanInCampaignOrIterationOrTestSuite,
} from '../../../scenario-parts/campaign.part';
import { AuthenticationProtocol } from '../../../../../../projects/sqtm-core/src/lib/model/third-party-server/authentication.model';
import { selectByDataTestCellId } from '../../../../utils/basic-selectors';
import { TestSuiteTestPlanPage } from '../../../../page-objects/pages/campaign-workspace/test-suite/test-suite-test-plan.page';
import { ExecutionRunnerStepPage } from '../../../../page-objects/pages/execution/execution-runner-step-page';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import { ExecutionStatusColor } from '../../../../utils/color.utils';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can execute a BDD test, declare issues, and assign execution status', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07035.CT05 - campaign-testsuite-testplan2-execute', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();
    const executionRunnerStepPage = new ExecutionRunnerStepPage();

    startSuiteTest(campaignWorkspace);

    cy.log('Step 2');
    generateAnIssueInExecution(executionRunnerStepPage);

    cy.log('Step 3');
    assignExecutionStatus(executionRunnerStepPage, campaignWorkspace);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('test suite execute', [
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CAMPAIGN_READ_UNASSIGNED,
        ProfileBuilderPermission.CAMPAIGN_EXECUTE,
      ]),
    ])
    .withServers([
      new BugtrackerServerBuilder('Hello', 'https://gitlab.com', 'gitlab.bugtracker')
        .withAuthProtocol(AuthenticationProtocol.TOKEN_AUTH)
        .withUserLevelAuthPolicy(),
    ])
    .withProjects([
      new ProjectBuilder('henixrecette/e2e_test_do_not_delete')
        .withBugtrackerOnProject([new BugtrackerProjectBindingBuilder()])
        .withTestCaseLibraryNodes([
          new KeywordTestCaseBuilder('BDD').withKeywordSteps([
            new KeywordStepBuilder('GIVEN', 'I play'),
          ]),
        ])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaign').withIterations([
            new IterationBuilder('Iteration')
              .withTestPlanItems([new IterationTestPlanItemBuilder('BDD')])
              .withTestSuites([new TestSuiteBuilder('Test-Suite').withTestCaseNames(['BDD'])]),
          ]),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}
function startSuiteTest(campaignWorkspace: CampaignWorkspacePage) {
  selectNodeInNodeInCampaignWorkspace(campaignWorkspace, [
    'henixrecette/e2e_test_do_not_delete',
    'Campaign',
    'Iteration',
    'Test-Suite',
  ]);
  launchExecutionInTestPlanPageIterationOrTestSuite('test-suite', '1', '1', '1', '1');
}

function generateAnIssueInExecution(executionRunnerStepPage: ExecutionRunnerStepPage) {
  const token = Cypress.env('GITLAB_TOKEN');
  executionRunnerStepPage.assertExists();
  createAnIssueFromExecutionRunnerStepPage(executionRunnerStepPage, token, 'Coca');
}

function assignExecutionStatus(
  executionRunnerStepPage: ExecutionRunnerStepPage,
  campaignWorkspace: CampaignWorkspacePage,
) {
  executionRunnerStepPage.setExecutionStatus('FAILURE', 1, 1);
  CampaignWorkspacePage.navigateToCampaignWorkspace();
  selectNodeInNodeInCampaignWorkspace(campaignWorkspace, [
    'henixrecette/e2e_test_do_not_delete',
    'Campaign',
    'Iteration',
    'Test-Suite',
  ]);
  const testsuiteViewPlanPage = showTestPlanInCampaignOrIterationOrTestSuite(
    'test-suite',
  ) as TestSuiteTestPlanPage;
  testsuiteViewPlanPage.testPlan.findRowId('testCaseName', 'BDD').then((idItem) => {
    testsuiteViewPlanPage.testPlan
      .getCell(idItem, 'executionStatus')
      .findCell()
      .find(selectByDataTestCellId('execution-status-cell'))
      .should('have.css', 'background-color', ExecutionStatusColor.FAILURE);
  });
}
