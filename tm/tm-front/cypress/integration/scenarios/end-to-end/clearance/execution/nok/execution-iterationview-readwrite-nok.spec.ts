import { DatabaseUtils } from '../../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
  ListOptionCustomFieldBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { BindableEntity } from '../../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import {
  ExploratoryTestCaseBuilder,
  KeywordTestCaseBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CampaignBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../../utils/end-to-end/api/add-permissions';
import { CampaignWorkspacePage } from '../../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { IterationViewPage } from '../../../../../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import {
  filterColumnsAlphabeticalOrder,
  selectNodeInNodeInCampaignWorkspace,
  showTestPlanInCampaignOrIterationOrTestSuite,
} from '../../../../scenario-parts/campaign.part';
import { CheckBoxElement } from '../../../../../page-objects/elements/forms/check-box.element';
import { IterationTestPlanPage } from '../../../../../page-objects/pages/campaign-workspace/iteration/iteration-test-plan.page';
import { CreateSuiteDialog } from '../../../../../page-objects/pages/campaign-workspace/dialogs/create-suite-dialog.element';
import { selectByDataTestButtonId } from '../../../../../utils/basic-selectors';
import { ItpiMultiEditDialogElement } from '../../../../../page-objects/pages/campaign-workspace/dialogs/itpi-multi-edit-dialog.element';
import { ListPanelElement } from '../../../../../page-objects/elements/filters/list-panel.element';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check if a user access cannot perform the following actions on the campaign consultation page: modify attributes, edit the iteration schedule, associate and dissociate a milestone."', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07230.CT02 - campaign-iterationview-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();
    const iterationViewPage = new IterationViewPage();

    selectNodeInNodeInCampaignWorkspace(campaignWorkspace, ['Project', 'Campaign', 'Iteration']);
    iterationViewPage.assertExists();

    cy.log('Step 2');
    assertCannotModifyIterationAttributes(iterationViewPage);

    cy.log('Step 3');
    assertCannotModifyScheduleFields(iterationViewPage);

    cy.log('Step 4');
    const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite(
      'iteration',
    ) as IterationTestPlanPage;
    testPlanPage.testPlan.assertRowCount(4);

    cy.log('Step 5');
    assertCannotAddTestSuiteInExecutionPlan();

    cy.log('Step 6');
    CannotModifyAttributesInMass();

    cy.log('Step 7');
    assertCannotModifyStatusToItemInTestPlan();

    cy.log('Step 8');
    filterColumnsAlphabeticalOrder(testPlanPage, ['BDD', 'Classique', 'Exploratoire', 'Gherkin']);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withCufs([
      new CustomFieldBuilder('CufList1', InputType.DROPDOWN_LIST).withOptions([
        new ListOptionCustomFieldBuilder('opt1', 'code1'),
        new ListOptionCustomFieldBuilder('opt2', 'code2'),
      ]),
    ])
    .withProfiles([new ProfileBuilder('campaign read', [ProfileBuilderPermission.CAMPAIGN_READ])])
    .withProjects([
      new ProjectBuilder('Project')
        .withCufsOnProject([new CustomFieldOnProjectBuilder('CufList1', BindableEntity.ITERATION)])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Classique'),
          new KeywordTestCaseBuilder('BDD'),
          new ScriptedTestCaseBuilder('Gherkin'),
          new ExploratoryTestCaseBuilder('Exploratoire'),
        ])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaign').withIterations([
            new IterationBuilder('Iteration').withTestPlanItems([
              new IterationTestPlanItemBuilder('Classique').withUserLogin('RonW'),
              new IterationTestPlanItemBuilder('BDD').withUserLogin('RonW'),
              new IterationTestPlanItemBuilder('Gherkin').withUserLogin('RonW'),
              new IterationTestPlanItemBuilder('Exploratoire').withUserLogin('RonW'),
            ]),
          ]),
        ]),
    ])
    .build();
}

function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}

function assertCannotModifyIterationAttributes(iterationViewPage: IterationViewPage) {
  iterationViewPage.clickInformationAnchorLink();
  iterationViewPage.referenceField.assertIsNotEditable();
  iterationViewPage.descriptionRichField.assertIsNotEditable();
  iterationViewPage.getSelectCustomField('CufList1').assertIsNotEditable();
}

function assertCannotModifyScheduleFields(iterationViewPage: IterationViewPage) {
  const checkboxAutoEndDate = new CheckBoxElement('actualEndAuto');
  const planningPage = iterationViewPage.clickPlanningAnchorLink();
  planningPage.actualEndDateField.assertIsNotEditable();
  checkboxAutoEndDate.click();
  checkboxAutoEndDate.findAndCheckState('span', false);
  planningPage.scheduledStartDateField.assertIsNotEditable();
}

function assertCannotAddTestSuiteInExecutionPlan() {
  const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite(
    'iteration',
  ) as IterationTestPlanPage;
  testPlanPage.testPlan.findRowId('testCaseName', 'Classique').then((idTestCase) => {
    testPlanPage.testPlan.selectRow(idTestCase, '#', 'leftViewport');
  });
  testPlanPage.openCreateTestSuiteDialog();
  const dialogCreateTestSuite = new CreateSuiteDialog();
  dialogCreateTestSuite.assertNotExist();
}

function CannotModifyAttributesInMass() {
  const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite('iteration');
  const massEditDialog = new ItpiMultiEditDialogElement();
  testPlanPage.testPlan.findRowId('testCaseName', 'Classique').then((idTestCase) => {
    testPlanPage.testPlan.selectRow(idTestCase, '#', 'leftViewport');
    cy.get(selectByDataTestButtonId('mass-edit')).click({ force: true });
    massEditDialog.assertNotExist();
  });
}

function assertCannotModifyStatusToItemInTestPlan() {
  const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite('iteration');
  const listItem = new ListPanelElement();
  testPlanPage.testPlan.findRowId('testCaseName', 'BDD').then((idTestCase) => {
    testPlanPage.testPlan.getCell(idTestCase, 'executionStatus').findCell().click({ force: true });
  });
  listItem.assertNotExist();
}
