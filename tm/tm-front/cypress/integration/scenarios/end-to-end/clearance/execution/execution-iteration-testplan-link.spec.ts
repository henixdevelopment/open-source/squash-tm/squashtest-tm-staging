import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { RequirementBuilder } from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  DataSetBuilder,
  ExploratoryTestCaseBuilder,
  KeywordTestCaseBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CampaignBuilder,
  IterationBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { TestCasesLinkToVerifiedRequirementVersionBuilder } from '../../../../utils/end-to-end/prerequisite/builders/link-builders';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import {
  assignUserToItemInTestPlan,
  linkAllTestCasePresentInSearchRequirementLinked,
  linkTestCaseFromSearchTestCaseInTestPlanPage,
  linkTestCaseToTestPlanInTestPlanPageThroughAssociateButton,
  removeItemInTestPlanPageWithBarButton,
  removeItemInTestPlanPageWithButtonEndOfLine,
  selectJDDInTestPlanPage,
  selectNodeInNodeInCampaignWorkspace,
  showTestPlanInCampaignOrIterationOrTestSuite,
} from '../../../scenario-parts/campaign.part';
import { IterationTestPlanPage } from '../../../../page-objects/pages/campaign-workspace/iteration/iteration-test-plan.page';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
  SystemProfile,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can link test cases to an execution plan in an Iteration', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    cy.clearAllCookies();
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07032.CT02 - campaign-iteration-testplan-link', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage();
    const iterationTestPlanPage = accessTestPlanPageInIteration(campaignWorkspacePage);

    linkTestCaseWithTopIconInTestPlanPage(iterationTestPlanPage);

    cy.log('Step 2');
    linkTestCaseWithSearchInTestPlanPage(iterationTestPlanPage);

    cy.log('Step 3');
    linkAllTestCaseFromSearchTestCase(iterationTestPlanPage);

    cy.log('Step 4');
    linkDataSetInTestPlan(iterationTestPlanPage);

    cy.log('Step 5');
    assignUserToAnItemInTestPlan(iterationTestPlanPage);

    cy.log('Step 6');
    dissociateTestCaseInTestPlan(iterationTestPlanPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([
      new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley'),
      new UserBuilder('HarryP').withFirstName('Harry').withLastName('Potter'),
    ])
    .withProfiles([
      new ProfileBuilder('iteration link', [
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CAMPAIGN_READ_UNASSIGNED,
        ProfileBuilderPermission.CAMPAIGN_LINK,
        ProfileBuilderPermission.TEST_CASE_READ,
        ProfileBuilderPermission.CAMPAIGN_WRITE,
        ProfileBuilderPermission.CAMPAIGN_DELETE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project')
        .withRequirementLibraryNodes([new RequirementBuilder('Requirement')])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Classique'),
          new KeywordTestCaseBuilder('BDD').withDataSets([
            new DataSetBuilder('Data'),
            new DataSetBuilder('Set'),
          ]),
          new ScriptedTestCaseBuilder('Gherkin'),
          new ExploratoryTestCaseBuilder('Exploratoire'),
        ])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaign').withIterations([new IterationBuilder('Iteration')]),
        ])
        .withLinks([
          new TestCasesLinkToVerifiedRequirementVersionBuilder(
            ['BDD', 'Exploratoire'],
            'Requirement',
          ),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
  addClearanceToUser(3, SystemProfile.PROJECT_MANAGER, [1]);
}

function accessTestPlanPageInIteration(
  campaignWorkspace: CampaignWorkspacePage,
): IterationTestPlanPage {
  selectNodeInNodeInCampaignWorkspace(campaignWorkspace, ['Project', 'Campaign', 'Iteration']);
  const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite('iteration');
  return testPlanPage as IterationTestPlanPage;
}

function linkTestCaseWithTopIconInTestPlanPage(iterationTestPlanPage: IterationTestPlanPage) {
  linkTestCaseToTestPlanInTestPlanPageThroughAssociateButton(
    'iteration',
    'Project',
    'Classique',
    1,
  );
  iterationTestPlanPage.testPlan.findRowId('testCaseName', 'Classique').then((idItem) => {
    iterationTestPlanPage.testPlan.assertRowExist(idItem);
  });
}

function linkTestCaseWithSearchInTestPlanPage(iterationTestPlanPage: IterationTestPlanPage) {
  linkTestCaseFromSearchTestCaseInTestPlanPage('iteration', 'Gherkin');
  iterationTestPlanPage.testPlan.assertRowCount(2);
  iterationTestPlanPage.testPlan.findRowId('testCaseName', 'Gherkin').then((idItem) => {
    iterationTestPlanPage.testPlan.assertRowExist(idItem);
  });
}

function linkAllTestCaseFromSearchTestCase(iterationTestPlanPage: IterationTestPlanPage) {
  linkAllTestCasePresentInSearchRequirementLinked('iteration');
  iterationTestPlanPage.testPlan.assertRowCount(5);
  iterationTestPlanPage.testPlan.assertGridColumnContains('testCaseName', [
    'Classique',
    'Gherkin',
    'BDD',
    'BDD',
    'Exploratoire',
  ]);
}

function linkDataSetInTestPlan(iterationTestPlanPage: IterationTestPlanPage) {
  selectJDDInTestPlanPage('iteration', 'BDD', 'Set');
  iterationTestPlanPage.testPlan.findRowId('#', '3', 'leftViewport').then((idItem) => {
    iterationTestPlanPage.testPlan
      .getCell(idItem, 'dataset-cell')
      .textRenderer()
      .assertContainsText('Set');
  });
}

function assignUserToAnItemInTestPlan(iterationTestPlanPage: IterationTestPlanPage) {
  cy.reload();
  assignUserToItemInTestPlan('iteration', 'Gherkin', 'item-3', 'Harry Potter');
  iterationTestPlanPage.testPlan.findRowId('testCaseName', 'Gherkin').then((idItem) => {
    iterationTestPlanPage.testPlan
      .getCell(idItem, 'assigneeFullName')
      .textRenderer()
      .assertContainsText('Harry Potter');
  });
}

function dissociateTestCaseInTestPlan(iterationTestPlanPage: IterationTestPlanPage) {
  removeItemInTestPlanPageWithButtonEndOfLine('iteration', 'Exploratoire');
  iterationTestPlanPage.testPlan.assertRowCount(4);
  iterationTestPlanPage.testPlan.assertGridColumnContains('testCaseName', [
    'Classique',
    'Gherkin',
    'BDD',
    'BDD',
  ]);
  iterationTestPlanPage.testPlan.findRowId('#', '1', 'leftViewport').then((idItem) => {
    iterationTestPlanPage.testPlan.findRowId('#', '2', 'leftViewport').then((idItem1) => {
      iterationTestPlanPage.testPlan.findRowId('#', '3', 'leftViewport').then((idItem2) => {
        iterationTestPlanPage.testPlan.findRowId('#', '4', 'leftViewport').then((idItem3) => {
          removeItemInTestPlanPageWithBarButton('iteration', [idItem1, idItem2, idItem3, idItem]);
        });
      });
    });
  });
  iterationTestPlanPage.testPlan.assertGridIsEmpty();
}
