import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  CampaignBuilder,
  CampaignFolderBuilder,
  IterationBuilder,
  TestSuiteBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import {
  assertThatDragAndDropWorksInPositionalOrder,
  dragEntityIntoAnotherEntityInCampaignWorkspace,
} from '../../../scenario-parts/campaign.part';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a Project Leader can move a campaign folder, a campaign, an iteration and a test suite', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07031.CT03 - campaign-move-createdelete', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();

    dragAndDropFolderIntoFolderOfTheSameProjectAndThenIntoAnotherProject(campaignWorkspace);

    cy.log('Step 2');
    dragAndDropCampaignIntoFolderOfSameProjectAndThenIntoAnotherProject(campaignWorkspace);

    cy.log('Step 3');
    assertThatDragAndDropWorksInPositionalOrderForAnIteration(campaignWorkspace);

    cy.log('Step 4');
    assertThatDragAndDropWorksInPositionalOrderForATestSuite(campaignWorkspace);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('execution move', [
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CAMPAIGN_CREATE,
        ProfileBuilderPermission.CAMPAIGN_DELETE,
        ProfileBuilderPermission.CAMPAIGN_WRITE,
        ProfileBuilderPermission.CAMPAIGN_LINK,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project').withCampaignLibraryNodes([
        new CampaignFolderBuilder('Ha'),
        new CampaignFolderBuilder('Io'),
        new CampaignBuilder('Campaign').withIterations([
          new IterationBuilder('Iteration'),
          new IterationBuilder('E').withTestSuites([
            new TestSuiteBuilder('A'),
            new TestSuiteBuilder('Plop'),
          ]),
        ]),
      ]),
      new ProjectBuilder('LL').withCampaignLibraryNodes([
        new CampaignBuilder('W').withIterations([new IterationBuilder('AAAA')]),
      ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1, 2]);
}
function dragAndDropFolderIntoFolderOfTheSameProjectAndThenIntoAnotherProject(
  campaignWorkspace: CampaignWorkspacePage,
) {
  dragEntityIntoAnotherEntityInCampaignWorkspace(
    campaignWorkspace,
    'Project',
    'Ha',
    'Io',
    'Project',
    false,
  );
  dragEntityIntoAnotherEntityInCampaignWorkspace(
    campaignWorkspace,
    'Project',
    'Ha',
    'LL',
    'Io',
    true,
  );
}

function dragAndDropCampaignIntoFolderOfSameProjectAndThenIntoAnotherProject(
  campaignWorkspace: CampaignWorkspacePage,
) {
  dragEntityIntoAnotherEntityInCampaignWorkspace(
    campaignWorkspace,
    'Project',
    'Campaign',
    'Io',
    'Project',
    false,
  );
  dragEntityIntoAnotherEntityInCampaignWorkspace(
    campaignWorkspace,
    'Io',
    'Campaign',
    'LL',
    'Io',
    true,
  );
}

function assertThatDragAndDropWorksInPositionalOrderForAnIteration(
  campaignWorkspace: CampaignWorkspacePage,
) {
  assertThatDragAndDropWorksInPositionalOrder(
    campaignWorkspace,
    true,
    ['Campaign', 'Iteration'],
    'Iteration',
    'E',
    ['Project', 'Io', 'LL', 'W', 'Ha', 'Campaign', 'E', 'Iteration'],
  );
}

function assertThatDragAndDropWorksInPositionalOrderForATestSuite(
  campaignWorkspace: CampaignWorkspacePage,
) {
  assertThatDragAndDropWorksInPositionalOrder(campaignWorkspace, false, ['E', 'A'], 'A', 'Plop', [
    'Project',
    'Io',
    'LL',
    'W',
    'Ha',
    'Campaign',
    'E',
    'Plop',
    'A',
    'Iteration',
  ]);
}
