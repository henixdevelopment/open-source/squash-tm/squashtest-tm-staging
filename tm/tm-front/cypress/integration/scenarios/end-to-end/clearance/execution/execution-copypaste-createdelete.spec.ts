import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  CampaignBuilder,
  CampaignFolderBuilder,
  IterationBuilder,
  TestSuiteBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import {
  copyAndPasteEntityInCampaignWorkspaceWithShortcuts,
  copyAndPasteEntityInCampaignWorkspaceWithTreeToolBarButton,
  copyIterationToAnotherProjectInCampaignWorkspaceWithToolBar,
  copyTestSuiteToAnotherProjectInCampaignWorkspaceWithShortcuts,
} from '../../../scenario-parts/campaign.part';
import { ConfirmInterProjectPaste } from '../../../../page-objects/elements/dialog/confirm-inter-project-paste';
import { createRegExpForCopyNode } from '../../../../utils/end-to-end/end-to-end-helper';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can copy and paste objects in the Campaign Workspace', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07031.CT02 - campaign-copypaste-createdelete', () => {
    cy.log('step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();
    const confirmDialog = new ConfirmInterProjectPaste('confirm-inter-project-paste');

    copyFolderInSameProject(campaignWorkspace);
    copyFolderInAnotherProject(campaignWorkspace, confirmDialog);

    cy.log('step 2');
    copyCampaignInSameProject(campaignWorkspace);
    copyCampaignInAnotherProject(campaignWorkspace, confirmDialog);

    cy.log('step 3');
    copyIterationInSameProject(campaignWorkspace);
    copyIterationInAnotherProject(campaignWorkspace, confirmDialog);

    cy.log('step 4');
    copyTestSuiteInSameProject(campaignWorkspace);
    copyTestSuiteInAnotherProject(campaignWorkspace, confirmDialog);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('execution copy paste', [
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CAMPAIGN_CREATE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Athena').withCampaignLibraryNodes([
        new CampaignFolderBuilder('Folder'),
        new CampaignBuilder('Campaign').withIterations([
          new IterationBuilder('Iteration').withTestSuites([new TestSuiteBuilder('TestSuite')]),
        ]),
      ]),
      new ProjectBuilder('Medusa'),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1, 2]);
}
function copyFolderInSameProject(campaignWorkspace: CampaignWorkspacePage) {
  copyAndPasteEntityInCampaignWorkspaceWithTreeToolBarButton(
    campaignWorkspace,
    ['Athena', 'Folder'],
    'Athena',
  );
}

function copyFolderInAnotherProject(
  campaignWorkspace: CampaignWorkspacePage,
  confirmDialog: ConfirmInterProjectPaste,
) {
  copyAndPasteEntityInCampaignWorkspaceWithTreeToolBarButton(
    campaignWorkspace,
    ['Athena', 'Folder'],
    'Medusa',
  );
  confirmDialog.clickOnConfirmButton();
}

function copyCampaignInSameProject(campaignWorkspace: CampaignWorkspacePage) {
  copyAndPasteEntityInCampaignWorkspaceWithShortcuts(
    campaignWorkspace,
    ['Athena', 'Campaign'],
    'Athena',
  );
}

function copyCampaignInAnotherProject(
  campaignWorkspace: CampaignWorkspacePage,
  confirmDialog: ConfirmInterProjectPaste,
) {
  copyAndPasteEntityInCampaignWorkspaceWithShortcuts(
    campaignWorkspace,
    ['Athena', 'Campaign'],
    'Medusa',
  );
  confirmDialog.clickOnConfirmButton();
}

function copyIterationInSameProject(campaignWorkspace: CampaignWorkspacePage) {
  const nodeTargetNameRegExp = createRegExpForCopyNode('Campaign');
  copyAndPasteEntityInCampaignWorkspaceWithTreeToolBarButton(
    campaignWorkspace,
    ['Campaign', 'Iteration'],
    nodeTargetNameRegExp,
  );
}

function copyIterationInAnotherProject(
  campaignWorkspace: CampaignWorkspacePage,
  confirmDialog: ConfirmInterProjectPaste,
) {
  copyIterationToAnotherProjectInCampaignWorkspaceWithToolBar(
    campaignWorkspace,
    ['Athena', 'Iteration'],
    'Campaign',
    'Medusa',
  );
  confirmDialog.clickOnConfirmButton();
}

function copyTestSuiteInSameProject(campaignWorkspace: CampaignWorkspacePage) {
  const nodeTargetNameRegExp = createRegExpForCopyNode('Iteration');
  copyAndPasteEntityInCampaignWorkspaceWithTreeToolBarButton(
    campaignWorkspace,
    ['Iteration', 'TestSuite'],
    nodeTargetNameRegExp,
  );
}

function copyTestSuiteInAnotherProject(
  campaignWorkspace: CampaignWorkspacePage,
  confirmDialog: ConfirmInterProjectPaste,
) {
  copyTestSuiteToAnotherProjectInCampaignWorkspaceWithShortcuts(
    campaignWorkspace,
    ['Iteration', 'TestSuite'],
    'Medusa',
    'Campaign',
    'Iteration',
  );
  confirmDialog.clickOnConfirmButton();
}
