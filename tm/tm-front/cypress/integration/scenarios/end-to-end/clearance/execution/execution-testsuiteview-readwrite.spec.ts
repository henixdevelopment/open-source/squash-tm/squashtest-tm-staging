import { DatabaseUtils } from '../../../../utils/database.utils';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import {
  CampaignBuilder,
  CampaignTestPlanItemBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
  ScriptedExecutionBuilder,
  TestSuiteBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  DataSetBuilder,
  DataSetParameterValueBuilder,
  ExploratoryTestCaseBuilder,
  KeywordTestCaseBuilder,
  ParameterBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { TestSuiteViewPage } from '../../../../page-objects/pages/campaign-workspace/test-suite/test-suite-view.page';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  editAttributeInMassTestPlanPage,
  editDescriptionCampaignWorkspace,
  renameCampaignIterationCampaignFolderAndTestSuiteInCampaignWorkspace,
  selectJDDInTestPlanPage,
  selectNodeInNodeInCampaignWorkspace,
  showTestPlanInCampaignOrIterationOrTestSuite,
  deleteExecutionInTestPlanPage,
  filterColumnMode,
} from '../../../scenario-parts/campaign.part';
import { TestSuiteTestPlanPage } from '../../../../page-objects/pages/campaign-workspace/test-suite/test-suite-test-plan.page';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can consult the testsuite page and edit name, description, CUF and execution plan', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07030.CT04 - campaign-testsuiteview-readwrite', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();
    const testSuiteViewPage = new TestSuiteViewPage();

    selectTestSuiteAndAccessTestSuiteView(campaignWorkspace, testSuiteViewPage);

    cy.log('Step 2');
    editNameDescriptionAndCUF(testSuiteViewPage);

    cy.log('Step 3');
    const testSuiteTestPlanPage = showTestPlanInCampaignOrIterationOrTestSuite(
      'test-suite',
    ) as TestSuiteTestPlanPage;

    cy.log('Step 4');
    modifyAttributesInMass(testSuiteTestPlanPage);

    cy.log('Step 5');
    selectJddValueInTestPlanPage(testSuiteTestPlanPage);

    cy.log('Step 6');
    deleteExecutionInTestPlanPage('test-suite', 'Gherkin');

    cy.log('Step 7');
    filterColumns(testSuiteTestPlanPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('execution test suite view', [
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CAMPAIGN_READ_UNASSIGNED,
        ProfileBuilderPermission.CAMPAIGN_WRITE,
        ProfileBuilderPermission.CAMPAIGN_EXECUTE,
        ProfileBuilderPermission.CAMPAIGN_DELETE,
      ]),
    ])
    .withCufs([new CustomFieldBuilder('Coco', InputType.DATE_PICKER)])
    .withProjects([
      new ProjectBuilder('Project')
        .withCufsOnProject([new CustomFieldOnProjectBuilder('Coco', BindableEntity.TEST_SUITE)])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Classique')
            .withParameters([new ParameterBuilder('Nom')])
            .withDataSets([
              new DataSetBuilder('Equipe').withParamValues([
                new DataSetParameterValueBuilder('Nom', 'Karmine Corp'),
              ]),
            ]),
          new KeywordTestCaseBuilder('BDD'),
          new ScriptedTestCaseBuilder('Gherkin'),
          new ExploratoryTestCaseBuilder('Exploratoire'),
        ])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaign')
            .withTestPlanItems([
              new CampaignTestPlanItemBuilder('Classique'),
              new CampaignTestPlanItemBuilder('BDD'),
              new CampaignTestPlanItemBuilder('Gherkin'),
              new CampaignTestPlanItemBuilder('Exploratoire'),
            ])
            .withIterations([
              new IterationBuilder('Iteration')
                .withTestPlanItems([
                  new IterationTestPlanItemBuilder('Gherkin')
                    .withUserLogin('RonW')
                    .withExecutions([new ScriptedExecutionBuilder()]),
                  new IterationTestPlanItemBuilder('BDD'),
                  new IterationTestPlanItemBuilder('Classique'),
                  new IterationTestPlanItemBuilder('Exploratoire'),
                ])
                .withTestSuites([
                  new TestSuiteBuilder('Suite').withTestCaseNames([
                    'Classique',
                    'BDD',
                    'Gherkin',
                    'Exploratoire',
                  ]),
                ]),
            ]),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}
function selectTestSuiteAndAccessTestSuiteView(
  campaignWorkspace: CampaignWorkspacePage,
  testSuiteViewPage: TestSuiteViewPage,
) {
  selectNodeInNodeInCampaignWorkspace(campaignWorkspace, [
    'Project',
    'Campaign',
    'Iteration',
    'Suite',
  ]);
  testSuiteViewPage.assertExists();
}

function editNameDescriptionAndCUF(testSuiteViewPage: TestSuiteViewPage) {
  renameCampaignIterationCampaignFolderAndTestSuiteInCampaignWorkspace('Test-Suite', 'NewName');
  editDescriptionCampaignWorkspace('Test-Suite', 'NewDescription');
  testSuiteViewPage.getDateCustomField('Coco').click();
  testSuiteViewPage.getDateCustomField('Coco').setToTodayAndConfirm();
}

function modifyAttributesInMass(testSuiteTestPlanPage: TestSuiteTestPlanPage) {
  editAttributeInMassTestPlanPage('test-suite', 'Classique', 'RonW', 'En cours');
  testSuiteTestPlanPage.testPlan.findRowId('testCaseName', 'Classique').then((idTestCase) => {
    testSuiteTestPlanPage.testPlan
      .getCell(idTestCase, 'assigneeFullName')
      .findCell()
      .contains('RonW');
  });
}

function selectJddValueInTestPlanPage(testSuiteTestPlanPage: TestSuiteTestPlanPage) {
  selectJDDInTestPlanPage('test-suite', 'Classique', 'Equipe');
  testSuiteTestPlanPage.testPlan.findRowId('testCaseName', 'Classique').then((idTestCase) => {
    testSuiteTestPlanPage.testPlan
      .getCell(idTestCase, 'dataset-cell')
      .findCell()
      .contains('Equipe');
  });
}

function filterColumns(testSuiteTestPlanPage: TestSuiteTestPlanPage) {
  testSuiteTestPlanPage.testPlan.findRowId('testCaseName', 'Exploratoire').then((idTestCase) => {
    filterColumnMode('test-suite', 'Manuel');
    testSuiteTestPlanPage.testPlan.assertRowNotExist(idTestCase);
    testSuiteTestPlanPage.testPlan.assertGridContentOrder('testCaseName', [
      'Gherkin',
      'BDD',
      'Classique',
    ]);
  });
}
