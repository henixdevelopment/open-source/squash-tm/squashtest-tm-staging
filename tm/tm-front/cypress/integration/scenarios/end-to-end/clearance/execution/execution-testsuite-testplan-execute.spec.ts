import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  BugtrackerProjectBindingBuilder,
  BugtrackerServerBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/server-prerequisite-builders';
import { AuthenticationProtocol } from '../../../../../../projects/sqtm-core/src/lib/model/third-party-server/authentication.model';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { ScriptedTestCaseBuilder } from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CampaignBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
  TestSuiteBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { ExecutionPage } from '../../../../page-objects/pages/execution/execution-page';
import {
  addCommentToAnExecutionStep,
  createAnIssueFromAnExecutionStepInExecutionPage,
  createNewExecutionOfTestInTestPlanPageAndModifyStatus,
  goForwardOrBackwardInExecutionRunnerStep,
  removeIssueFromExecutionRunnerStepPageWithEndOfLineButton,
  resumeExecutionInTestPlanPageIterationOrTestSuite,
  selectNodeInCampaignWorkspace,
  selectNodeInNodeInCampaignWorkspace,
  showTestPlanInCampaignOrIterationOrTestSuite,
} from '../../../scenario-parts/campaign.part';
import { NavBarElement } from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import { ExecutionRunnerStepPage } from '../../../../page-objects/pages/execution/execution-runner-step-page';
import { selectByDataTestCellId } from '../../../../utils/basic-selectors';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import { ExecutionStatusColor } from '../../../../utils/color.utils';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can execute a scripted testcase in a testsuite and create a new issue', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07035.CT03 - campaign-testsuite-testplan-execute', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();
    const executionPage = new ExecutionPage();
    const executionRunnerStepPage = new ExecutionRunnerStepPage();

    executeTestcaseInTestPlanPage(campaignWorkspace, executionPage);

    cy.log('Step 2');
    createAnIssueInExecution(executionPage);

    cy.log('Step 3');
    addCommentStepElement(executionPage);
    assertExecutionStatusInIterationTestPlanPage(campaignWorkspace);

    cy.log('Step 4');
    resumeAnExecutionInTestSuite(campaignWorkspace, executionRunnerStepPage);

    cy.log('Step 5');
    unlinkIssueFromRunnerStepPage(executionRunnerStepPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('test suite execute', [
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CAMPAIGN_READ_UNASSIGNED,
        ProfileBuilderPermission.CAMPAIGN_EXECUTE,
      ]),
    ])
    .withServers([
      new BugtrackerServerBuilder('Hello', 'https://gitlab.com', 'gitlab.bugtracker')
        .withAuthProtocol(AuthenticationProtocol.TOKEN_AUTH)
        .withUserLevelAuthPolicy(),
    ])
    .withProjects([
      new ProjectBuilder('henixrecette/e2e_test_do_not_delete')
        .withBugtrackerOnProject([new BugtrackerProjectBindingBuilder()])
        .withTestCaseLibraryNodes([
          new ScriptedTestCaseBuilder('Hello').withScript(
            '# language: fr\n' +
              'Fonctionnalité: CT Gherkin 1\n' +
              '    \n' +
              ' Scénario: pas de test 1\n' +
              '    Soit je rédige un CT\n' +
              '    \n' +
              '   Scénario: pas de test 1\n' +
              '    Soit je rédige un CT',
          ),
        ])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaign').withIterations([
            new IterationBuilder('Iteration')
              .withTestPlanItems([new IterationTestPlanItemBuilder('Hello')])
              .withTestSuites([new TestSuiteBuilder('TestSuite').withTestCaseNames(['Hello'])]),
          ]),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}
function executeTestcaseInTestPlanPage(
  campaignWorkspace: CampaignWorkspacePage,
  executionPage: ExecutionPage,
) {
  selectNodeInNodeInCampaignWorkspace(campaignWorkspace, [
    'henixrecette/e2e_test_do_not_delete',
    'Campaign',
    'Iteration',
    'TestSuite',
  ]);
  createNewExecutionOfTestInTestPlanPageAndModifyStatus('test-suite', 'Hello', '1', 'SUCCESS', 1);
  executionPage.scenarioPanel.getExecutionStepById('1').checkStepExecutionStatus('Succès');
}

function createAnIssueInExecution(executionPage: ExecutionPage) {
  const token = Cypress.env('GITLAB_TOKEN');
  createAnIssueFromAnExecutionStepInExecutionPage(
    executionPage.scenarioPanel,
    'Coca',
    '1',
    true,
    token,
  );
  executionPage.clickIssueAnchorLink();
  executionPage.issueGrid.assertRowCount(1);
  executionPage.clickScenarioAnchorLink();
}

function addCommentStepElement(executionPage: ExecutionPage) {
  addCommentToAnExecutionStep(executionPage.scenarioPanel, '1', 'Hello');
  executionPage.scenarioPanel.getExecutionStepById('1').checkStepComment('Hello');
}

function assertExecutionStatusInIterationTestPlanPage(campaignWorkspace: CampaignWorkspacePage) {
  NavBarElement.navigateToCampaignWorkspace();
  selectNodeInNodeInCampaignWorkspace(campaignWorkspace, [
    'henixrecette/e2e_test_do_not_delete',
    'Campaign',
    'Iteration',
  ]);
  const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite('iteration');
  testPlanPage.testPlan.findRowId('testCaseName', 'Hello').then((idTestCase) => {
    testPlanPage.testPlan
      .getCell(idTestCase, 'executionStatus')
      .findCell()
      .find(selectByDataTestCellId('execution-status-cell'))
      .should('have.css', 'background-color', ExecutionStatusColor.RUNNING);
  });
}

function resumeAnExecutionInTestSuite(
  campaignWorkspace: CampaignWorkspacePage,
  executionRunnerStepPage: ExecutionRunnerStepPage,
) {
  selectNodeInCampaignWorkspace(campaignWorkspace, 'Iteration', 'TestSuite');
  resumeExecutionInTestPlanPageIterationOrTestSuite('test-suite', '1', '1', '1', '2');
  goForwardOrBackwardInExecutionRunnerStep(executionRunnerStepPage, 'Backard');
  executionRunnerStepPage.checkExecutionStepper(1, 2);
}

function unlinkIssueFromRunnerStepPage(executionRunnerStepPage: ExecutionRunnerStepPage) {
  removeIssueFromExecutionRunnerStepPageWithEndOfLineButton(executionRunnerStepPage, 'Coca');
  executionRunnerStepPage.issuesPanelGrid.assertGridIsEmpty();
}
