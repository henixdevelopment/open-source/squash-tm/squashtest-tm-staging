import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  DataSetBuilder,
  ExploratoryTestCaseBuilder,
  KeywordTestCaseBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { RequirementBuilder } from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import { CampaignBuilder } from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { TestCasesLinkToVerifiedRequirementVersionBuilder } from '../../../../utils/end-to-end/prerequisite/builders/link-builders';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { CampaignTestPlanPage } from '../../../../page-objects/pages/campaign-workspace/campaign/campaign-test-plan.page';
import {
  assignUserToItemInTestPlan,
  linkAllTestCasePresentInSearchRequirementLinked,
  linkTestCaseFromSearchTestCaseInTestPlanPage,
  linkTestCaseToTestPlanInTestPlanPageThroughAssociateButton,
  removeItemInTestPlanPageWithBarButton,
  removeItemInTestPlanPageWithButtonEndOfLine,
  selectJDDInTestPlanPage,
  selectNodeInCampaignWorkspace,
  showTestPlanInCampaignOrIterationOrTestSuite,
} from '../../../scenario-parts/campaign.part';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
  SystemProfile,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can link test cases to an execution plan', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    cy.clearAllCookies();
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07032.CT01 - campaign-campaign-testplan-link', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage();
    const campaignTestPlanPage = accessTestPlanPageInCampaign(campaignWorkspacePage);

    linkTestCaseWithTopIconInTestPlanPage(campaignTestPlanPage);

    cy.log('Step 2');
    linkTestCaseFromSearchTestCase(campaignTestPlanPage);

    cy.log('Step 3');
    linkAllTestCaseFromSearchTestCase(campaignTestPlanPage);

    cy.log('Step 4');
    linkDataSetInTestPlan(campaignTestPlanPage);

    cy.log('Step 5');
    assignUserToAnItemInTestPlan(campaignTestPlanPage);

    cy.log('Step 6');
    dissociateTestCaseInTestPlan(campaignTestPlanPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([
      new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley'),
      new UserBuilder('HarryP').withFirstName('Harry').withLastName('Potter'),
    ])
    .withProfiles([
      new ProfileBuilder('execution test suite view', [
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CAMPAIGN_READ_UNASSIGNED,
        ProfileBuilderPermission.CAMPAIGN_WRITE,
        ProfileBuilderPermission.CAMPAIGN_LINK,
        ProfileBuilderPermission.TEST_CASE_READ,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project')
        .withRequirementLibraryNodes([new RequirementBuilder('Requirement')])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Classique').withDataSets([
            new DataSetBuilder('Data'),
            new DataSetBuilder('Set'),
          ]),
          new KeywordTestCaseBuilder('BDD'),
          new ScriptedTestCaseBuilder('Gherkin'),
          new ExploratoryTestCaseBuilder('Exploratoire'),
        ])
        .withCampaignLibraryNodes([new CampaignBuilder('Campaign')])
        .withLinks([
          new TestCasesLinkToVerifiedRequirementVersionBuilder(
            ['Classique', 'Gherkin'],
            'Requirement',
          ),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
  addClearanceToUser(3, SystemProfile.PROJECT_MANAGER, [1]);
}
function accessTestPlanPageInCampaign(
  campaignWorkspace: CampaignWorkspacePage,
): CampaignTestPlanPage {
  selectNodeInCampaignWorkspace(campaignWorkspace, 'Project', 'Campaign');
  const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite('campaign');
  return testPlanPage as CampaignTestPlanPage;
}

function linkTestCaseWithTopIconInTestPlanPage(campaignTestPlanPage: CampaignTestPlanPage) {
  linkTestCaseToTestPlanInTestPlanPageThroughAssociateButton(
    'campaign',
    'Project',
    'Exploratoire',
    1,
  );
  campaignTestPlanPage.testPlan.findRowId('testCaseName', 'Exploratoire').then((idItem) => {
    campaignTestPlanPage.testPlan.assertRowExist(idItem);
  });
}

function linkTestCaseFromSearchTestCase(campaignTestPlanPage: CampaignTestPlanPage) {
  linkTestCaseFromSearchTestCaseInTestPlanPage('campaign', 'BDD');
  campaignTestPlanPage.testPlan.assertRowCount(2);
  campaignTestPlanPage.testPlan.findRowId('testCaseName', 'BDD').then((idTestCase) => {
    campaignTestPlanPage.testPlan.assertRowExist(idTestCase);
  });
}

function linkAllTestCaseFromSearchTestCase(campaignTestPlanPage: CampaignTestPlanPage) {
  linkAllTestCasePresentInSearchRequirementLinked('campaign');
  campaignTestPlanPage.testPlan.assertRowCount(5);
  campaignTestPlanPage.testPlan.assertGridColumnContains('testCaseName', [
    'Exploratoire',
    'BDD',
    'Classique',
    'Classique',
    'Gherkin',
  ]);
}

function linkDataSetInTestPlan(campaignTestPlanPage: CampaignTestPlanPage) {
  selectJDDInTestPlanPage('campaign', 'Classique', 'Set');
  campaignTestPlanPage.testPlan.findRowId('#', '3', 'leftViewport').then((idItem) => {
    campaignTestPlanPage.testPlan
      .getCell(idItem, 'dataset-cell')
      .textRenderer()
      .assertContainsText('Set');
  });
}

function assignUserToAnItemInTestPlan(campaignTestPlanPage: CampaignTestPlanPage) {
  cy.reload();
  assignUserToItemInTestPlan('campaign', 'BDD', 'item-3', 'Harry Potter');
  campaignTestPlanPage.testPlan.findRowId('testCaseName', 'BDD').then((idItem) => {
    campaignTestPlanPage.testPlan
      .getCell(idItem, 'assigneeFullName')
      .textRenderer()
      .assertContainsText('Harry Potter');
  });
}

function dissociateTestCaseInTestPlan(campaignTestPlanPage: CampaignTestPlanPage) {
  removeItemInTestPlanPageWithButtonEndOfLine('campaign', 'Gherkin');
  campaignTestPlanPage.testPlan.assertRowCount(4);
  campaignTestPlanPage.testPlan.assertGridColumnContains('testCaseName', [
    'Exploratoire',
    'BDD',
    'Classique',
    'Classique',
  ]);
  campaignTestPlanPage.testPlan.findRowId('#', '1', 'leftViewport').then((idItem) => {
    campaignTestPlanPage.testPlan.findRowId('#', '2', 'leftViewport').then((idItem1) => {
      campaignTestPlanPage.testPlan.findRowId('#', '3', 'leftViewport').then((idItem2) => {
        campaignTestPlanPage.testPlan.findRowId('#', '4', 'leftViewport').then((idItem3) => {
          removeItemInTestPlanPageWithBarButton('campaign', [idItem1, idItem2, idItem3, idItem]);
        });
      });
    });
  });
  campaignTestPlanPage.testPlan.assertGridIsEmpty();
}
