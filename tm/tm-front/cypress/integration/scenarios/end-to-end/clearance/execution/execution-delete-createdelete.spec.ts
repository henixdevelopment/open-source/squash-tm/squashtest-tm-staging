import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  CampaignBuilder,
  CampaignFolderBuilder,
  IterationBuilder,
  TestSuiteBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import {
  deleteEntityInCampaignWorkspaceWithShortCut,
  deleteEntityInCampaignWorkspaceWithToolBar,
} from '../../../scenario-parts/campaign.part';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Assert that a user can delete a folder and a campaign in Campaign Workspace ', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07031.CT04 - campaign-delete-createdelete', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();

    deleteFolderInCampaignWorkspace(campaignWorkspace);

    cy.log('Step 2');
    deleteCampaignInCampaignWorkspace(campaignWorkspace);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('execution delete', [
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CAMPAIGN_DELETE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Clio').withCampaignLibraryNodes([
        new CampaignFolderBuilder('Folder'),
        new CampaignBuilder('Campaigne').withIterations([
          new IterationBuilder('Iteration').withTestSuites([new TestSuiteBuilder('TestSuite')]),
        ]),
      ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}

function deleteFolderInCampaignWorkspace(campaignWorkspace: CampaignWorkspacePage) {
  deleteEntityInCampaignWorkspaceWithShortCut(campaignWorkspace, 'Clio', 'Folder');
  campaignWorkspace.tree.assertSpinnerIsNotPresent();
  campaignWorkspace.tree.assertNodeOrderByName(['Clio', 'Campaigne']);
}

function deleteCampaignInCampaignWorkspace(campaignWorkspace: CampaignWorkspacePage) {
  campaignWorkspace.tree.findRowId('NAME', 'Campaigne').then((idCampaign) => {
    deleteEntityInCampaignWorkspaceWithToolBar(campaignWorkspace, 'Clio', 'Campaigne');
    campaignWorkspace.tree.assertRowNotExist(idCampaign);
  });
}
