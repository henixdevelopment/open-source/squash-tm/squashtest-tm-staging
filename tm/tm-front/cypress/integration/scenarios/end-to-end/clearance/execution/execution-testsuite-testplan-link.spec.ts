import { DatabaseUtils } from '../../../../utils/database.utils';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { RequirementBuilder } from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  DataSetBuilder,
  ExploratoryTestCaseBuilder,
  KeywordTestCaseBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CampaignBuilder,
  IterationBuilder,
  TestSuiteBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { TestCasesLinkToVerifiedRequirementVersionBuilder } from '../../../../utils/end-to-end/prerequisite/builders/link-builders';
import {
  assignUserToItemInTestPlan,
  linkAllTestCasePresentInSearchRequirementLinked,
  linkTestCaseFromSearchTestCaseInTestPlanPage,
  linkTestCaseToTestPlanInTestPlanPageThroughAssociateButton,
  removeItemInTestPlanPageWithBarButton,
  removeItemInTestPlanPageWithButtonEndOfLine,
  selectJDDInTestPlanPage,
  selectNodeInNodeInCampaignWorkspace,
  showTestPlanInCampaignOrIterationOrTestSuite,
} from '../../../scenario-parts/campaign.part';
import { TestSuiteTestPlanPage } from '../../../../page-objects/pages/campaign-workspace/test-suite/test-suite-test-plan.page';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
  SystemProfile,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can link test cases to an execution plan in an Test-Suite', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    cy.clearAllCookies();
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07032.CT03- campaign-testsuite-testplan-link (Project leader)', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage();
    const testSuiteTestPlanPage = accessTestPlanPageInTestSuite(campaignWorkspacePage);

    linkTestCaseWithTopIconInTestPlanPage(testSuiteTestPlanPage);

    cy.log('Step 2');
    linkTestCaseWithSearchInTestPlanPage(testSuiteTestPlanPage);

    cy.log('Step 3');
    linkAllTestCaseFromSearchTestCase(testSuiteTestPlanPage);

    cy.log('Step 4');
    linkDataSetInTestPlan(testSuiteTestPlanPage);

    cy.log('Step 5');
    assignUserToAnItemInTestPlan(testSuiteTestPlanPage);

    cy.log('Step 6');
    dissociateTestCaseInTestPlan(testSuiteTestPlanPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([
      new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley'),
      new UserBuilder('HarryP').withFirstName('Harry').withLastName('Potter'),
    ])
    .withProfiles([
      new ProfileBuilder('test suite execute', [
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CAMPAIGN_READ_UNASSIGNED,
        ProfileBuilderPermission.CAMPAIGN_LINK,
        ProfileBuilderPermission.CAMPAIGN_WRITE,
        ProfileBuilderPermission.CAMPAIGN_DELETE,
        ProfileBuilderPermission.TEST_CASE_READ,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project')
        .withRequirementLibraryNodes([new RequirementBuilder('Requirement')])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Classique').withDataSets([
            new DataSetBuilder('Data'),
            new DataSetBuilder('Set'),
          ]),
          new KeywordTestCaseBuilder('BDD'),
          new ScriptedTestCaseBuilder('Gherkin'),
          new ExploratoryTestCaseBuilder('Exploratoire'),
        ])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaign').withIterations([
            new IterationBuilder('Iteration').withTestSuites([new TestSuiteBuilder('Test-Suite')]),
          ]),
        ])
        .withLinks([
          new TestCasesLinkToVerifiedRequirementVersionBuilder(
            ['Classique', 'Exploratoire'],
            'Requirement',
          ),
        ]),
    ])
    .build();
}

function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
  addClearanceToUser(3, SystemProfile.PROJECT_MANAGER, [1]);
}

function accessTestPlanPageInTestSuite(
  campaignWorkspace: CampaignWorkspacePage,
): TestSuiteTestPlanPage {
  selectNodeInNodeInCampaignWorkspace(campaignWorkspace, [
    'Project',
    'Campaign',
    'Iteration',
    'Test-Suite',
  ]);
  const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite('test-suite');
  return testPlanPage as TestSuiteTestPlanPage;
}

function linkTestCaseWithTopIconInTestPlanPage(testSuiteTestPlanPage: TestSuiteTestPlanPage) {
  linkTestCaseToTestPlanInTestPlanPageThroughAssociateButton('test-suite', 'Project', 'BDD', 1);
  testSuiteTestPlanPage.testPlan.getCell('testCaseName', 'BDD');
}

function linkTestCaseWithSearchInTestPlanPage(testSuiteTestPlanPage: TestSuiteTestPlanPage) {
  linkTestCaseFromSearchTestCaseInTestPlanPage('test-suite', 'Gherkin');
  testSuiteTestPlanPage.testPlan.assertRowCount(2);
  testSuiteTestPlanPage.testPlan.findRowId('testCaseName', 'Gherkin').then((idItem) => {
    testSuiteTestPlanPage.testPlan.assertRowExist(idItem);
  });
}

function linkAllTestCaseFromSearchTestCase(testSuiteTestPlanPage: TestSuiteTestPlanPage) {
  linkAllTestCasePresentInSearchRequirementLinked('test-suite');
  testSuiteTestPlanPage.testPlan.assertRowCount(5);
  testSuiteTestPlanPage.testPlan.assertGridColumnContains('testCaseName', [
    'Exploratoire',
    'BDD',
    'Classique',
    'Classique',
    'Gherkin',
  ]);
}

function linkDataSetInTestPlan(testSuiteTestPlanPage: TestSuiteTestPlanPage) {
  selectJDDInTestPlanPage('test-suite', 'Classique', 'Set');
  testSuiteTestPlanPage.testPlan.findRowId('#', '3', 'leftViewport').then((idItem) => {
    testSuiteTestPlanPage.testPlan
      .getCell(idItem, 'dataset-cell')
      .textRenderer()
      .assertContainsText('Set');
  });
}

function assignUserToAnItemInTestPlan(testSuiteTestPlanPage: TestSuiteTestPlanPage) {
  cy.reload();
  assignUserToItemInTestPlan('test-suite', 'Exploratoire', 'item-3', 'Harry Potter');
  testSuiteTestPlanPage.testPlan.findRowId('testCaseName', 'Exploratoire').then((idItem) => {
    testSuiteTestPlanPage.testPlan
      .getCell(idItem, 'assigneeFullName')
      .textRenderer()
      .assertContainsText('Harry Potter');
  });
}

function dissociateTestCaseInTestPlan(testSuiteTestPlanPage: TestSuiteTestPlanPage) {
  removeItemInTestPlanPageWithButtonEndOfLine('test-suite', 'Exploratoire', [1, 2, 3, 4, 5]);
  testSuiteTestPlanPage.testPlan.assertRowCount(4);
  testSuiteTestPlanPage.testPlan.assertGridColumnContains('testCaseName', [
    'BDD',
    'Classique',
    'Classique',
    'Gherkin',
  ]);
  testSuiteTestPlanPage.testPlan.findRowId('#', '1', 'leftViewport').then((idItem) => {
    testSuiteTestPlanPage.testPlan.findRowId('#', '2', 'leftViewport').then((idItem1) => {
      testSuiteTestPlanPage.testPlan.findRowId('#', '3', 'leftViewport').then((idItem2) => {
        testSuiteTestPlanPage.testPlan.findRowId('#', '4', 'leftViewport').then((idItem3) => {
          removeItemInTestPlanPageWithBarButton('test-suite', [idItem1, idItem2, idItem3, idItem]);
        });
      });
    });
  });
  testSuiteTestPlanPage.testPlan.assertGridIsEmpty();
}
