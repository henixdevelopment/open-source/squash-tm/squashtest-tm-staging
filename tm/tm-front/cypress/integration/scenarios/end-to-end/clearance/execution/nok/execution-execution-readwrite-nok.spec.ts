import { DatabaseUtils } from '../../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import { CampaignWorkspacePage } from '../../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { ExecutionPage } from '../../../../../page-objects/pages/execution/execution-page';
import {
  assertCannotDeleteExecutionInExecutionPageWithTopIcon,
  assertExecutionPageExistForAnExecution,
  selectNodeInNodeInCampaignWorkspace,
} from '../../../../scenario-parts/campaign.part';
import { PrerequisiteBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { ProjectBuilder } from '../../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { BindableEntity } from '../../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import {
  ActionTestStepBuilder,
  KeywordTestCaseBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CampaignBuilder,
  ClassicExecutionBuilder,
  ExecutionStepBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
  KeywordExecutionBuilder,
  ScriptedExecutionBuilder,
} from '../../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  addClearanceToUser,
  SystemProfile,
} from '../../../../../utils/end-to-end/api/add-permissions';

describe('Assert that a user can consult execution page,however cannot modify cufs of an execution, cannot modify cufs of an execution step and delete an execution ', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07030.CT05 - execution-execution-readwrite-nok', () => {
    cy.log('step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();
    const executionPage = new ExecutionPage();

    selectNodeInNodeInCampaignWorkspace(campaignWorkspace, ['Project', 'Campaign', 'Iteration']);
    assertExecutionPageExistForAnExecution('iteration', 'Classic', '1');

    cy.log('step 2');
    assertCannotModifyCufsInformationBlocAndExecutionStepOfClassicTest(executionPage);

    cy.log('step 3');
    assertCannotDeleteExecutionInExecutionPageWithTopIcon(executionPage, '1');

    cy.log('step 4');
    assertCufTagExecutionCannotBeModified(executionPage);

    cy.log('step 5');
    assertCannotDeleteExecutionInExecutionPageWithIconEndOfLine(executionPage, '1');

    cy.log('step 6');
    assertCufNumericExecutionCannotBeModified(executionPage);

    cy.log('step 7');
    assertCannotDeleteExecutionInExecutionPageWithTopIcon(executionPage, '1');
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withCufs([
      new CustomFieldBuilder('Rich Text', InputType.RICH_TEXT),
      new CustomFieldBuilder('Numeric', InputType.NUMERIC),
      new CustomFieldBuilder('Tag', InputType.TAG),
      new CustomFieldBuilder('Plain Text', InputType.PLAIN_TEXT),
    ])
    .withProjects([
      new ProjectBuilder('Project')
        .withCufsOnProject([
          new CustomFieldOnProjectBuilder('Rich Text', BindableEntity.EXECUTION),
          new CustomFieldOnProjectBuilder('Numeric', BindableEntity.EXECUTION),
          new CustomFieldOnProjectBuilder('Tag', BindableEntity.EXECUTION),
          new CustomFieldOnProjectBuilder('Plain Text', BindableEntity.EXECUTION_STEP),
        ])
        .withTestCaseLibraryNodes([
          new KeywordTestCaseBuilder('BDD'),
          new TestCaseBuilder('Classic').withSteps([new ActionTestStepBuilder('action', 'result')]),
          new ScriptedTestCaseBuilder('Gherkin'),
        ])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaign').withIterations([
            new IterationBuilder('Iteration').withTestPlanItems([
              new IterationTestPlanItemBuilder('Classic').withExecutions([
                new ClassicExecutionBuilder().withExecutionStep([new ExecutionStepBuilder()]),
              ]),
              new IterationTestPlanItemBuilder('BDD').withExecutions([
                new KeywordExecutionBuilder(),
              ]),
              new IterationTestPlanItemBuilder('Gherkin').withExecutions([
                new ScriptedExecutionBuilder(),
              ]),
            ]),
          ]),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, SystemProfile.PROJECT_VIEWER, [1]);
}

function assertCannotModifyCufsInformationBlocAndExecutionStepOfClassicTest(
  executionPage: ExecutionPage,
) {
  executionPage.getRichTextCustomField('Rich Text').assertIsNotEditable();
  const executionScenarioPanelElement = executionPage.clickScenarioAnchorLink();
  executionScenarioPanelElement.getExecutionStepByIndex(0).extendStep();
  executionPage.getRichTextCustomField('Plain Text').assertIsNotEditable();
}

function assertCufTagExecutionCannotBeModified(executionPage: ExecutionPage) {
  executionPage.clickBackButton();
  assertExecutionPageExistForAnExecution('iteration', 'Gherkin', '1');
  executionPage.getTagCustomField('Tag').assertIsNotEditable();
}

function assertCannotDeleteExecutionInExecutionPageWithIconEndOfLine(
  executionPage: ExecutionPage,
  executionNumber: string,
) {
  const historyExecution = executionPage.clickHistoryAnchorLink();
  historyExecution.assertEndOfLineDeleteButtonNotVisible(executionNumber);
}

function assertCufNumericExecutionCannotBeModified(executionPage: ExecutionPage) {
  executionPage.clickBackButton();
  assertExecutionPageExistForAnExecution('iteration', 'BDD', '1');
  executionPage.getTagCustomField('Numeric').assertIsNotEditable();
}
