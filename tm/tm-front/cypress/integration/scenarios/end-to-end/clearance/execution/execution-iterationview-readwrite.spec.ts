import { DatabaseUtils } from '../../../../utils/database.utils';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
  ListOptionCustomFieldBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import {
  DataSetBuilder,
  DataSetParameterValueBuilder,
  ExploratoryTestCaseBuilder,
  KeywordTestCaseBuilder,
  ParameterBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CampaignBuilder,
  CampaignTestPlanItemBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
  KeywordExecutionBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  changeStatusTestCaseInTestCasePlanPage,
  createTestSuiteFromPagePlanInIteration,
  editAttributeInMassTestPlanPage,
  editDescriptionCampaignWorkspace,
  editFieldInPlanningViewWithCalendar,
  filterColumnsAlphabeticalOrder,
  modifyReferenceCampaignIterationCampaignFolderAndTestSuiteInCampaignWorkspace,
  selectNodeInCampaignWorkspace,
  selectNodeInNodeInCampaignWorkspace,
  showTestPlanInCampaignOrIterationOrTestSuite,
  toggleAutoCheckboxInPlanningFields,
} from '../../../scenario-parts/campaign.part';
import { IterationViewPage } from '../../../../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { IterationTestPlanPage } from '../../../../page-objects/pages/campaign-workspace/iteration/iteration-test-plan.page';
import { TestSuiteViewPage } from '../../../../page-objects/pages/campaign-workspace/test-suite/test-suite-view.page';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
  SystemProfile,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can consult the iteration page and edit name, description, CUF and execution plan', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07030.CT04 - campaign-iterationview-readwrite', () => {
    cy.log('step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();
    const iterationViewPage = new IterationViewPage();
    const testSuiteViewPage = new TestSuiteViewPage();

    accessIterationView(campaignWorkspace, iterationViewPage);

    cy.log('step 2');
    editReferenceDescriptionAndCUF(iterationViewPage);

    cy.log('step 3');
    changeDateCalendarInIterationInCampaignView();

    cy.log('step 4');
    const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite(
      'iteration',
    ) as IterationTestPlanPage;
    accessTestPlanPageAndAssertItemsArePresent(testPlanPage);

    cy.log('Step 5');
    createTestSuiteFromIterationPlanPage(testPlanPage, testSuiteViewPage, campaignWorkspace);

    cy.log('Step 6');
    showTestPlanInCampaignOrIterationOrTestSuite('iteration');
    modifyAttributesInMass(testPlanPage);

    cy.log('Step 7');
    changeStatusTestCaseInTestCasePlanPage('iteration', 'Gherkin', 'Échec');

    cy.log('Step 8');
    filterColumnsAlphabeticalOrder(testPlanPage, ['BDD', 'Classique', 'Exploratoire', 'Gherkin']);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([
      new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley'),
      new UserBuilder('HarryP').withFirstName('Harry').withLastName('Potter'),
    ])
    .withCufs([
      new CustomFieldBuilder('Coco', InputType.DROPDOWN_LIST).withOptions([
        new ListOptionCustomFieldBuilder('Coca', 'Coca'),
        new ListOptionCustomFieldBuilder('Fanta', 'Fanta'),
      ]),
    ])
    .withProfiles([
      new ProfileBuilder('iteration view', [
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CAMPAIGN_READ_UNASSIGNED,
        ProfileBuilderPermission.CAMPAIGN_WRITE,
        ProfileBuilderPermission.CAMPAIGN_CREATE,
        ProfileBuilderPermission.CAMPAIGN_LINK,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project')
        .withCufsOnProject([new CustomFieldOnProjectBuilder('Coco', BindableEntity.ITERATION)])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Classique')
            .withParameters([new ParameterBuilder('Nom')])
            .withDataSets([
              new DataSetBuilder('Equipe').withParamValues([
                new DataSetParameterValueBuilder('Nom', 'Karmine Corp'),
              ]),
            ]),
          new KeywordTestCaseBuilder('BDD'),
          new ScriptedTestCaseBuilder('Gherkin'),
          new ExploratoryTestCaseBuilder('Exploratoire'),
        ])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaign')
            .withTestPlanItems([
              new CampaignTestPlanItemBuilder('Classique'),
              new CampaignTestPlanItemBuilder('BDD'),
              new CampaignTestPlanItemBuilder('Gherkin'),
              new CampaignTestPlanItemBuilder('Exploratoire'),
            ])
            .withIterations([
              new IterationBuilder('Iteration').withTestPlanItems([
                new IterationTestPlanItemBuilder('BDD')
                  .withUserLogin('RonW')
                  .withExecutions([new KeywordExecutionBuilder()]),
                new IterationTestPlanItemBuilder('Classique'),
                new IterationTestPlanItemBuilder('Gherkin'),
                new IterationTestPlanItemBuilder('Exploratoire'),
              ]),
            ]),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
  addClearanceToUser(3, SystemProfile.PROJECT_MANAGER, [1]);
}
function accessIterationView(
  campaignWorkspace: CampaignWorkspacePage,
  iterationViewPage: IterationViewPage,
) {
  selectNodeInNodeInCampaignWorkspace(campaignWorkspace, ['Project', 'Campaign', 'Iteration']);
  iterationViewPage.assertExists();
}

function editReferenceDescriptionAndCUF(iterationViewPage: IterationViewPage) {
  modifyReferenceCampaignIterationCampaignFolderAndTestSuiteInCampaignWorkspace(
    'iteration',
    'NewName',
  );
  iterationViewPage.clickInformationAnchorLink();
  editDescriptionCampaignWorkspace('Iteration', 'NewDescription');
  iterationViewPage.getSelectCustomField('Coco').selectValueNoButton('Coca');
}

function changeDateCalendarInIterationInCampaignView() {
  editFieldInPlanningViewWithCalendar('iteration', 'scheduledEndDate');
  toggleAutoCheckboxInPlanningFields('actualStartAuto', true);
  toggleAutoCheckboxInPlanningFields('actualStartAuto', false);
  editFieldInPlanningViewWithCalendar('iteration', 'actualStartDate');
}

function accessTestPlanPageAndAssertItemsArePresent(testPlanPage: IterationTestPlanPage) {
  testPlanPage.testPlan.assertRowCount(4);
}

function createTestSuiteFromIterationPlanPage(
  testPlanPage: IterationTestPlanPage,
  testSuiteViewPage: TestSuiteViewPage,
  campaignWorkSpace: CampaignWorkspacePage,
) {
  createTestSuiteFromPagePlanInIteration(testPlanPage, 'Classique', 'Test-suite');
  selectNodeInCampaignWorkspace(campaignWorkSpace, 'NewName', 'Test-suite');
  testSuiteViewPage.assertExists();
  selectNodeInCampaignWorkspace(campaignWorkSpace, 'Project', 'NewName');
}

function modifyAttributesInMass(testPlanPage: IterationTestPlanPage) {
  editAttributeInMassTestPlanPage('iteration', 'Classique', 'HarryP', 'Succès');
  testPlanPage.testPlan.findRowId('testCaseName', 'Classique').then((idTestCase) => {
    testPlanPage.testPlan.getCell(idTestCase, 'assigneeFullName').findCell().contains('HarryP');
  });
}
