import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  CampaignBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { TestCaseBuilder } from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  assertNodeHasCorrectParent,
  createAnIterationFromSearchPage,
  createObjectInCampaignWorkspace,
} from '../../../scenario-parts/campaign.part';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can create a campaign folder, a campaign, an iteration and a test suite', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07031.CT01 - campaign-create-createdelete', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();

    assertFolderIsCreatedInCampaignWorkspace(campaignWorkspace);

    cy.log('Step 2');
    assertCampaignIsCreatedInCampaignWorkspace(campaignWorkspace);

    cy.log('step 3');
    assertIterationIsCreatedInCampaignWorkspace(campaignWorkspace);

    cy.log('step 4');
    assertTestSuiteIsCreatedInCampaignWorkspace(campaignWorkspace);

    cy.log('step 5');
    createAnIterationFromSearchPage(
      campaignWorkspace,
      'TestCase',
      ['Project', 'Campaign'],
      'Campaign',
      'Résultats',
    );
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('execution create', [
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CAMPAIGN_CREATE,
        ProfileBuilderPermission.CAMPAIGN_LINK,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project')
        .withTestCaseLibraryNodes([new TestCaseBuilder('TestCase')])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaign').withIterations([
            new IterationBuilder('Iteration').withTestPlanItems([
              new IterationTestPlanItemBuilder('TestCase'),
            ]),
          ]),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}
function assertFolderIsCreatedInCampaignWorkspace(campaignWorkspace: CampaignWorkspacePage) {
  createObjectInCampaignWorkspace(campaignWorkspace, true, 'Project', 'Campaign-Folder', 'Folder');
  assertNodeHasCorrectParent(campaignWorkspace, 'Project', 'Folder');
}

function assertCampaignIsCreatedInCampaignWorkspace(campaignWorkspace: CampaignWorkspacePage) {
  createObjectInCampaignWorkspace(campaignWorkspace, true, 'Project', 'Campaign', 'Lala');
  assertNodeHasCorrectParent(campaignWorkspace, 'Project', 'Lala');
}

function assertIterationIsCreatedInCampaignWorkspace(campaignWorkspace: CampaignWorkspacePage) {
  createObjectInCampaignWorkspace(campaignWorkspace, true, 'Lala', 'Iteration', 'Hello');
  assertNodeHasCorrectParent(campaignWorkspace, 'Lala', 'Hello');
}

function assertTestSuiteIsCreatedInCampaignWorkspace(campaignWorkspace: CampaignWorkspacePage) {
  createObjectInCampaignWorkspace(campaignWorkspace, true, 'Hello', 'Test-Suite', 'Froid');
  assertNodeHasCorrectParent(campaignWorkspace, 'Hello', 'Froid');
}
