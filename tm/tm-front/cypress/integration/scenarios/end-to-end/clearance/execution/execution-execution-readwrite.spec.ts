import {
  CustomFieldBuilder,
  CustomFieldOnProjectBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/custom-field-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  ActionTestStepBuilder,
  KeywordTestCaseBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CampaignBuilder,
  CampaignTestPlanItemBuilder,
  ClassicExecutionBuilder,
  ExecutionStepBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
  KeywordExecutionBuilder,
  ScriptedExecutionBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { InputType } from '../../../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { BindableEntity } from '../../../../../../projects/sqtm-core/src/lib/model/bindable-entity.model';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { DatabaseUtils } from '../../../../utils/database.utils';
import { SystemE2eCommands } from '../../../../page-objects/scenarios-parts/system/system_e2e_commands';
import {
  assertExecutionPageExistForAnExecution,
  deleteExecutionInExecutionPageWithIconEndOfLine,
  deleteExecutionInExecutionPageWithTopIcon,
  selectNodeInNodeInCampaignWorkspace,
} from '../../../scenario-parts/campaign.part';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { ExecutionPage } from '../../../../page-objects/pages/execution/execution-page';
import { ExecutionScenarioPanelElement } from '../../../../page-objects/pages/execution/panels/execution-scenario-panel.element';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Assert that a user can consult execution page, modify cufs of an execution, modify cufs of an execution step and delete an execution ', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    initTestData();
    initPermissions();
  });

  it('F01-UC07030.CT05 - campaign-execution-readwrite', () => {
    cy.log('step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();
    const executionPage = new ExecutionPage();
    const executionScenarioPanelElement = new ExecutionScenarioPanelElement();

    selectNodeInNodeInCampaignWorkspace(campaignWorkspace, ['Clio', 'Campagne', 'Iteration']);
    assertExecutionPageExistForAnExecution('iteration', 'Classique', '1');

    cy.log('step 2');
    modifyCufsInformationBlocAndExecutionStepOfClassicTest(
      executionPage,
      executionScenarioPanelElement,
    );

    cy.log('step 3');
    deleteExecutionInExecutionPageWithTopIcon(executionPage, '1');

    cy.log('step 4');
    assertCufScriptedTestExecutionCanBeModified(executionPage);

    cy.log('step 5');
    deleteExecutionInExecutionPageWithIconEndOfLine(executionPage, '1');

    cy.log('step 6');
    assertCufKeywordTestExecutionCanBeModified(executionPage);

    cy.log('step 7');
    deleteExecutionInExecutionPageWithTopIcon(executionPage, '1');
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('execution read/write', [
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CAMPAIGN_READ_UNASSIGNED,
        ProfileBuilderPermission.CAMPAIGN_WRITE,
        ProfileBuilderPermission.CAMPAIGN_DELETE,
        ProfileBuilderPermission.CAMPAIGN_EXECUTE,
      ]),
    ])
    .withCufs([
      new CustomFieldBuilder('Rich Text', InputType.RICH_TEXT),
      new CustomFieldBuilder('Numeric', InputType.NUMERIC),
      new CustomFieldBuilder('Tag', InputType.TAG),
      new CustomFieldBuilder('Plain Text', InputType.PLAIN_TEXT),
    ])
    .withProjects([
      new ProjectBuilder('Clio')
        .withCufsOnProject([
          new CustomFieldOnProjectBuilder('Rich Text', BindableEntity.EXECUTION),
          new CustomFieldOnProjectBuilder('Numeric', BindableEntity.EXECUTION),
          new CustomFieldOnProjectBuilder('Tag', BindableEntity.EXECUTION),
          new CustomFieldOnProjectBuilder('Plain Text', BindableEntity.EXECUTION_STEP),
        ])
        .withTestCaseLibraryNodes([
          new KeywordTestCaseBuilder('BDD'),
          new TestCaseBuilder('Classique').withSteps([new ActionTestStepBuilder('Lala', 'Lili')]),
          new ScriptedTestCaseBuilder('Gherkin'),
        ])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campagne')
            .withTestPlanItems([
              new CampaignTestPlanItemBuilder('BDD'),
              new CampaignTestPlanItemBuilder('Gherkin'),
              new CampaignTestPlanItemBuilder('Classique'),
            ])
            .withIterations([
              new IterationBuilder('Iteration').withTestPlanItems([
                new IterationTestPlanItemBuilder('Classique').withExecutions([
                  new ClassicExecutionBuilder().withExecutionStep([new ExecutionStepBuilder()]),
                ]),
                new IterationTestPlanItemBuilder('BDD').withExecutions([
                  new KeywordExecutionBuilder(),
                ]),
                new IterationTestPlanItemBuilder('Gherkin').withExecutions([
                  new ScriptedExecutionBuilder(),
                ]),
              ]),
            ]),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}
function modifyCufsInformationBlocAndExecutionStepOfClassicTest(
  executionPage: ExecutionPage,
  executionScenarioPanelElement: ExecutionScenarioPanelElement,
) {
  executionPage.fillCuf('Rich Text', InputType.RICH_TEXT, 'Hallo');
  executionPage.clickScenarioAnchorLink();
  executionScenarioPanelElement.getExecutionStepByIndex(0).extendStep();
  executionScenarioPanelElement
    .getExecutionStepByIndex(0)
    .getAndFillCuf('Plain Text', InputType.PLAIN_TEXT, 'Dodo');
}

function assertCufScriptedTestExecutionCanBeModified(executionPage: ExecutionPage) {
  assertExecutionPageExistForAnExecution('iteration', 'Gherkin', '1');
  executionPage.fillCuf('Tag', InputType.TAG, 'Flaubert');
}

function assertCufKeywordTestExecutionCanBeModified(executionPage: ExecutionPage) {
  assertExecutionPageExistForAnExecution('iteration', 'BDD', '1');
  executionPage.fillCuf('Numeric', InputType.NUMERIC, '1');
}
