import { DatabaseUtils } from '../../../../utils/database.utils';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  ActionTestStepBuilder,
  KeywordStepBuilder,
  KeywordTestCaseBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CampaignBuilder,
  ClassicExecutionBuilder,
  ExecutionStepBuilder,
  ExecutionStepsExecutionBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
  KeywordExecutionBuilder,
  ScriptedExecutionBuilder,
  TestSuiteBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import {
  deleteEntityInCampaignWorkspaceWithShortCut,
  deleteEntityInCampaignWorkspaceWithToolBar,
  removeItemInTestPlanPageWithBarButton,
  selectNodeInNodeInCampaignWorkspace,
  showTestPlanInCampaignOrIterationOrTestSuite,
} from '../../../scenario-parts/campaign.part';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Test asserts that a user can delete a campaign, an iteration and a testsuite contening an execution. It also asserts he can delete an executed test', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    cy.clearAllCookies();
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07036.CT04 - campaign-delete-extendeddelete', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();

    deleteItpiFromTestSuite(campaignWorkspace);

    cy.log('Step 2');
    deleteItpiFromIteration(campaignWorkspace);

    cy.log('Step 3');
    deleteCampaignContainingItpi(campaignWorkspace);

    cy.log('Step 4');
    deleteTestSuiteContainingItpi(campaignWorkspace);

    cy.log('Step 5');
    deleteIterationContainingItpi(campaignWorkspace);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('execution extended delete', [
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CAMPAIGN_READ_UNASSIGNED,
        ProfileBuilderPermission.CAMPAIGN_DELETE,
        ProfileBuilderPermission.CAMPAIGN_EXTENDED_DELETE,
        ProfileBuilderPermission.CAMPAIGN_LINK,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project')
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Classique').withSteps([new ActionTestStepBuilder('aaa', 'bbbb')]),
          new KeywordTestCaseBuilder('Mars').withKeywordSteps([
            new KeywordStepBuilder('GIVEN', 'Coco'),
          ]),
          new ScriptedTestCaseBuilder('Jaune').withScript(
            `# language: fr
              Fonctionnalité: CT Gherkin 1

              Scénario: pas de test 1
                Soit je rédige un CT
                Scénario: pas de test 1
                Soit je rédige un CT`,
          ),
        ])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaign').withIterations([
            new IterationBuilder('Iteration')
              .withTestPlanItems([
                new IterationTestPlanItemBuilder('Classique', 'SUCCESS').withExecutions([
                  new ClassicExecutionBuilder('SUCCESS').withExecutionStep([
                    new ExecutionStepBuilder('SUCCESS').withExecutionStepExecution([
                      new ExecutionStepsExecutionBuilder('admin'),
                    ]),
                  ]),
                ]),
              ])
              .withTestSuites([
                new TestSuiteBuilder('Suite', 'SUCCESS').withTestCaseNames(['Classique']),
              ]),
          ]),
          new CampaignBuilder('Soleil').withIterations([
            new IterationBuilder('Lune')
              .withTestPlanItems([
                new IterationTestPlanItemBuilder('Mars', 'FAILURE').withExecutions([
                  new KeywordExecutionBuilder('FAILURE').withExecutionStep([
                    new ExecutionStepBuilder('FAILURE').withExecutionStepExecution([
                      new ExecutionStepsExecutionBuilder('admin'),
                    ]),
                  ]),
                ]),
              ])
              .withTestSuites([
                new TestSuiteBuilder('Venus', 'FAILURE').withTestCaseNames(['Mars']),
              ]),
          ]),
          new CampaignBuilder('Noir').withIterations([
            new IterationBuilder('Bleu')
              .withTestPlanItems([
                new IterationTestPlanItemBuilder('Jaune', 'UNTESTABLE').withExecutions([
                  new ScriptedExecutionBuilder('UNTESTABLE').withExecutionStep([
                    new ExecutionStepBuilder('UNTESTABLE').withExecutionStepExecution([
                      new ExecutionStepsExecutionBuilder('admin'),
                    ]),
                  ]),
                ]),
              ])
              .withTestSuites([
                new TestSuiteBuilder('Rouge', 'UNTESTABLE').withTestCaseNames(['Jaune']),
              ]),
          ]),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}

function deleteItpiFromTestSuite(campaignWorkspacePage: CampaignWorkspacePage) {
  selectNodeInNodeInCampaignWorkspace(campaignWorkspacePage, [
    'Project',
    'Campaign',
    'Iteration',
    'Suite',
  ]);
  const testSuiteTestPlanPage = showTestPlanInCampaignOrIterationOrTestSuite('test-suite');
  removeItemInTestPlanPageWithBarButton('test-suite', [1]);
  testSuiteTestPlanPage.testPlan.assertGridIsEmpty();
}

function deleteItpiFromIteration(campaignWorkspacePage: CampaignWorkspacePage) {
  selectNodeInNodeInCampaignWorkspace(campaignWorkspacePage, ['Project', 'Campaign', 'Iteration']);
  const iterationTestPlanPage = showTestPlanInCampaignOrIterationOrTestSuite('iteration');
  removeItemInTestPlanPageWithBarButton('iteration', [1]);
  iterationTestPlanPage.testPlan.assertGridIsEmpty();
}

function deleteCampaignContainingItpi(campaignWorkspacePage: CampaignWorkspacePage) {
  campaignWorkspacePage.tree.findRowId('NAME', 'Soleil').then((idProject) => {
    deleteEntityInCampaignWorkspaceWithToolBar(campaignWorkspacePage, 'Project', 'Soleil');
    campaignWorkspacePage.tree.assertRowNotExist(idProject);
  });
}

function deleteTestSuiteContainingItpi(campaignWorkspacePage: CampaignWorkspacePage) {
  selectNodeInNodeInCampaignWorkspace(campaignWorkspacePage, ['Project', 'Noir', 'Bleu']);
  deleteEntityInCampaignWorkspaceWithShortCut(campaignWorkspacePage, 'Bleu', 'Rouge');
  campaignWorkspacePage.tree.assertSpinnerIsNotPresent();
  campaignWorkspacePage.tree.assertNodeOrderByName([
    'Project',
    'Campaign',
    'Iteration',
    'Suite',
    'Noir',
    'Bleu',
  ]);
}

function deleteIterationContainingItpi(campaignWorkspacePage: CampaignWorkspacePage) {
  campaignWorkspacePage.tree.findRowId('NAME', 'Noir').then((idProject) => {
    deleteEntityInCampaignWorkspaceWithToolBar(campaignWorkspacePage, 'Project', 'Noir');
    campaignWorkspacePage.tree.assertRowNotExist(idProject);
  });
}
