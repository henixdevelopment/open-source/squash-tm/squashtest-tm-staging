import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import {
  BugtrackerProjectBindingBuilder,
  BugtrackerServerBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/server-prerequisite-builders';
import { AuthenticationProtocol } from '../../../../../../projects/sqtm-core/src/lib/model/third-party-server/authentication.model';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import { ExploratoryTestCaseBuilder } from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import {
  CampaignBuilder,
  ExploratorySessionBuilder,
  IterationBuilder,
  IterationTestPlanItemBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import {
  accessToSessionPageInIterationOrTestSuiteFromCampaignWorkspace,
  addAnExecutionInSessionPageAndAccessThisExecutionPage,
  createAnIssueFromAnExecutionInExecutionPage,
  modifySessionNoteContentAndType,
  removeIssueWithButtonInGridEndOfLine,
  removeSessionNote,
  startSessionAndAddNewNote,
} from '../../../scenario-parts/campaign.part';
import { SessionOverviewPage } from '../../../../page-objects/pages/session-overview/session-overview.page';
import { ExecutionPage } from '../../../../page-objects/pages/execution/execution-page';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';

describe('Check that a user can execute a exploratory test case, add a session note and create a new issue', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F01-UC07035.CT06 - campaign-sessionoverview-execute', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();
    const sessionOverviewPage = new SessionOverviewPage();
    const executionPage = new ExecutionPage();

    accessSessionPageFromIterationTestPlanPage(
      campaignWorkspace,
      sessionOverviewPage,
      executionPage,
    );

    cy.log('Step 2 and 3');
    beginASessionAndAddNewNote(executionPage);

    cy.log('Step 4');
    createAnIssueInExecutionAndUnlinkTheSameIssue(executionPage);

    cy.log('Step 5');
    modifySessionNoteInSession(executionPage);

    cy.log('Step 6');
    removeSessionNoteInSession(executionPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('execution session view', [
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CAMPAIGN_EXECUTE,
        ProfileBuilderPermission.CAMPAIGN_READ_UNASSIGNED,
      ]),
    ])
    .withServers([
      new BugtrackerServerBuilder('Hello', 'https://gitlab.com', 'gitlab.bugtracker')
        .withAuthProtocol(AuthenticationProtocol.TOKEN_AUTH)
        .withUserLevelAuthPolicy(),
    ])
    .withProjects([
      new ProjectBuilder('henixrecette/e2e_test_do_not_delete')
        .withBugtrackerOnProject([new BugtrackerProjectBindingBuilder()])
        .withTestCaseLibraryNodes([new ExploratoryTestCaseBuilder('Exploratory')])
        .withCampaignLibraryNodes([
          new CampaignBuilder('Campaign').withIterations([
            new IterationBuilder('Iteration').withTestPlanItems([
              new IterationTestPlanItemBuilder('Exploratory').withSessions([
                new ExploratorySessionBuilder('Exploratory'),
              ]),
            ]),
          ]),
        ]),
    ])
    .build();
}
function initPermissions() {
  addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
}
function accessSessionPageFromIterationTestPlanPage(
  campaignWorkspace: CampaignWorkspacePage,
  sessionOverviewPage: SessionOverviewPage,
  executionPage: ExecutionPage,
) {
  accessToSessionPageInIterationOrTestSuiteFromCampaignWorkspace(
    campaignWorkspace,
    ['henixrecette/e2e_test_do_not_delete', 'Campaign', 'Iteration'],
    'iteration',
    'Exploratory',
  );
  sessionOverviewPage.assertExists();
  addAnExecutionInSessionPageAndAccessThisExecutionPage(sessionOverviewPage, '1');
  executionPage.assertExists();
}

function beginASessionAndAddNewNote(executionPage: ExecutionPage) {
  startSessionAndAddNewNote(executionPage, 1, 'Hello Coca');
  executionPage.findNoteContainer(1).existingNoteElement.checkContentText('Hello Coca');
}

function createAnIssueInExecutionAndUnlinkTheSameIssue(executionPage: ExecutionPage) {
  const token = Cypress.env('GITLAB_TOKEN');
  createAnIssueFromAnExecutionInExecutionPage(executionPage, token, 'Coca');
  executionPage.issueGrid.assertRowCount(1);
  removeIssueWithButtonInGridEndOfLine(executionPage, 'Coca');
  executionPage.issueGrid.assertGridIsEmpty();
}

function modifySessionNoteInSession(executionPage: ExecutionPage) {
  modifySessionNoteContentAndType(executionPage, 1, 'Fanta', 'Positif');
  executionPage.findNoteContainer(1).existingNoteElement.checkContentText('Fanta');
  executionPage.findNoteContainer(1).existingNoteElement.kindSelect.checkKindText('Positif');
}

function removeSessionNoteInSession(executionPage: ExecutionPage) {
  removeSessionNote(executionPage, 1);
  executionPage.findNoteContainer(1).existingNoteElement.assertNotExist();
}
