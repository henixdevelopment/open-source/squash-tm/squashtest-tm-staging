import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  ExploratoryExecutionBuilder,
  ExploratorySessionBuilder,
  KeywordExecutionBuilder,
  ScriptedExecutionBuilder,
  SprintBuilder,
  SprintGroupBuilder,
  SprintReqVersionBuilder,
  SprintTestPlanItemBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/campaign-prerequisite-builders';
import { RequirementBuilder } from '../../../../utils/end-to-end/prerequisite/builders/requirement-prerequisite-builders';
import {
  ActionTestStepBuilder,
  DataSetBuilder,
  ExploratoryTestCaseBuilder,
  KeywordStepBuilder,
  KeywordTestCaseBuilder,
  ScriptedTestCaseBuilder,
  TestCaseBuilder,
} from '../../../../utils/end-to-end/prerequisite/builders/test-case-prerequisite-builders';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import {
  ProfileBuilder,
  ProfileBuilderPermission,
} from '../../../../utils/end-to-end/prerequisite/builders/profile-prerequisite-builders';
import {
  addClearanceToUser,
  FIRST_CUSTOM_PROFILE_ID,
} from '../../../../utils/end-to-end/api/add-permissions';
import { TestCaseCoverageBuilder } from '../../../../utils/end-to-end/prerequisite/builders/link-builders';

describe('Delete a sprint or a sprint group', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    addClearanceToUser(2, FIRST_CUSTOM_PROFILE_ID, [1]);
  });

  it('Delete a sprint without execution', function () {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();
    campaignWorkspace.toggleTreeElement('CampaignLibrary-1');
    campaignWorkspace.toggleTreeElement('SprintGroup-1');
    campaignWorkspace.selectTreeRowElement('Sprint-2');
    campaignWorkspace.clickTreeDeleteButton();
    campaignWorkspace.deleteCampaignLibraryNode();
    campaignWorkspace.assertRowNotExist('Sprint-2');

    cy.log('Step 2');
    campaignWorkspace.selectTreeRowElement('Sprint-3');
    campaignWorkspace.clickTreeDeleteButton();
    campaignWorkspace.deleteCampaignLibraryNode();
    campaignWorkspace.assertRowNotExist('Sprint-3');

    cy.log('Step 3');
    campaignWorkspace.tree.getRow('SprintGroup-1').findRow().click();
    campaignWorkspace.clickTreeDeleteButton();
    campaignWorkspace.deleteCampaignLibraryNode();
    campaignWorkspace.assertRowNotExist('SprintGroup-1');
    campaignWorkspace.assertRowNotExist('Sprint-4');
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProfiles([
      new ProfileBuilder('Sprint extended delete', [
        ProfileBuilderPermission.CAMPAIGN_READ,
        ProfileBuilderPermission.CAMPAIGN_DELETE,
        ProfileBuilderPermission.CAMPAIGN_EXTENDED_DELETE,
      ]),
    ])
    .withProjects([
      new ProjectBuilder('Project')
        .withRequirementLibraryNodes([
          new RequirementBuilder('Exi1'),
          new RequirementBuilder('Exi2'),
          new RequirementBuilder('Exi3'),
        ])
        .withTestCaseLibraryNodes([
          new TestCaseBuilder('Classic TC')
            .withSteps([new ActionTestStepBuilder('Action', 'Result')])
            .withRequirements([new TestCaseCoverageBuilder(['Exi1'])]),
          new ScriptedTestCaseBuilder('Gherkin TC')
            .withScript(
              `# language: fr
              Fonctionnalité: CT Gherkin 1

              Scénario: pas de test 1
                Soit je rédige un CT
                Scénario: pas de test 1
                Soit je rédige un CT,

              Scénario: Vérifier les produits disponibles.
                 Etant donné que la machine est en marche.
                 Quand je liste les produits disponibles.
                 Alors je constate que tous les produits suivants sont disponibles :
                 | produit		| prix  |
                 | Expresso		| 0.40  |
                 | Lungo			| 0.50  |
                 | Cappuccino	| 0.80  |`,
            )
            .withRequirements([new TestCaseCoverageBuilder(['Exi2'])]),
          new KeywordTestCaseBuilder('Keyword TC')
            .withKeywordSteps([
              new KeywordStepBuilder('GIVEN', 'Condition'),
              new KeywordStepBuilder('WHEN', 'Action'),
              new KeywordStepBuilder('THEN', 'Result'),
            ])
            .withRequirements([new TestCaseCoverageBuilder(['Exi1'])]),
          new KeywordTestCaseBuilder('Keyword TC 2')
            .withKeywordSteps([
              new KeywordStepBuilder('GIVEN', 'This'),
              new KeywordStepBuilder('THEN', 'That'),
            ])
            .withDataSets([new DataSetBuilder('Dataset 1'), new DataSetBuilder('Dataset 2')])
            .withRequirements([new TestCaseCoverageBuilder(['Exi3'])]),
          new ExploratoryTestCaseBuilder('Exploratory TC')
            .withCharter('A charter')
            .withRequirements([new TestCaseCoverageBuilder(['Exi2'])]),
        ])
        .withCampaignLibraryNodes([
          new SprintGroupBuilder('SprintGroup').withCampaignLibraryNodes([
            new SprintBuilder('Sprint A').withSprintReqVersions([
              new SprintReqVersionBuilder('Sprint A', 'Exi1').withTestPlanItems([
                new SprintTestPlanItemBuilder('Classic TC', 'Exi1'),
                new SprintTestPlanItemBuilder('Keyword TC', 'Exi1'),
              ]),
            ]),
            new SprintBuilder('Sprint B').withSprintReqVersions([
              new SprintReqVersionBuilder('Sprint B', 'Exi2').withTestPlanItems([
                new SprintTestPlanItemBuilder('Gherkin TC', 'Exi2').withExecutions([
                  new ScriptedExecutionBuilder().forSprint(),
                  new ScriptedExecutionBuilder().forSprint(),
                ]),
                new SprintTestPlanItemBuilder('Exploratory TC', 'Exi2').withSessions([
                  new ExploratorySessionBuilder('Exploratory TC', 'Exi2')
                    .forSprint()
                    .withExploratoryExecutions([
                      new ExploratoryExecutionBuilder(),
                      new ExploratoryExecutionBuilder(),
                    ]),
                ]),
              ]),
            ]),
            new SprintBuilder('Sprint C').withSprintReqVersions([
              new SprintReqVersionBuilder('Sprint C', 'Exi3').withTestPlanItems([
                new SprintTestPlanItemBuilder('Keyword TC 2', 'Exi3')
                  .withDatasetName('Dataset 1')
                  .withExecutions([
                    new KeywordExecutionBuilder().withDatasetName('Dataset 1').forSprint(),
                  ]),
                new SprintTestPlanItemBuilder('Keyword TC 2', 'Exi3')
                  .withDatasetName('Dataset 2')
                  .withExecutions([
                    new KeywordExecutionBuilder().withDatasetName('Dataset 2').forSprint(),
                  ]),
              ]),
            ]),
          ]),
        ]),
    ])
    .build();
}
