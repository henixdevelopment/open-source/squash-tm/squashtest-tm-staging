import { DatabaseUtils } from '../../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { ProjectBuilder } from '../../../../utils/end-to-end/prerequisite/builders/project-prerequisite-builders';
import {
  addClearanceToProject,
  SystemProfile,
} from '../../../../utils/end-to-end/api/add-permissions';
import { CampaignWorkspacePage } from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { selectNodeProjectInCampaignWorkspace } from '../../../scenario-parts/campaign.part';
import { SprintGroupViewPage } from '../../../../page-objects/pages/campaign-workspace/sprint-group/sprint-group-view.page';
import { SprintViewPage } from '../../../../page-objects/pages/campaign-workspace/sprint/sprint-view.page';

describe('Assert the  b functioning of the manual sprint creation and sprint group creation', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
    initPermissions();
  });

  it('F04-UC091.CT06 - create-sprint-group', () => {
    cy.log('Step 1');
    cy.logInAs('RonW', 'admin');
    const campaignWorkspace = CampaignWorkspacePage.initTestAtPage();
    const sprintGroupViewPage = new SprintGroupViewPage();
    const sprintViewPage = new SprintViewPage();

    createSprintGroup(campaignWorkspace);

    cy.log('Step 2');
    assertSprintGroupInfo(sprintGroupViewPage);

    cy.log('Step 3');
    editSprintGroupInfo(sprintGroupViewPage);

    cy.log('Step 4');
    createSprintInGroupSprint(campaignWorkspace);

    cy.log('Step 5');
    assertSprintInfo(sprintViewPage);

    cy.log('Step 6');
    editSprintInfo(sprintViewPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .withProjects([new ProjectBuilder('Project')])
    .build();
}

function initPermissions() {
  addClearanceToProject(1, SystemProfile.PROJECT_MANAGER, [2]);
}

function createSprintGroup(campaignWorkspace: CampaignWorkspacePage) {
  selectNodeProjectInCampaignWorkspace(campaignWorkspace, 'Project');
  const sprintGroupDialog = campaignWorkspace.openCreateSprintGroup();
  sprintGroupDialog.assertExists();
  sprintGroupDialog.fillName('Groupe de Sprint A');
  sprintGroupDialog.fillDescription('Ceci est un groupe de sprint');
  sprintGroupDialog.assertIsReady();
  sprintGroupDialog.clickOnAddButton();
  sprintGroupDialog.assertNotExist();
}

function assertSprintGroupInfo(sprintGroupViewPage: SprintGroupViewPage) {
  sprintGroupViewPage.assertExists();
  sprintGroupViewPage.assertNameContains('Groupe de Sprint A');
  sprintGroupViewPage.descriptionRichField.checkHtmlContent('Ceci est un groupe de sprint');
  sprintGroupViewPage.assertCreatedBy('RonW');
  sprintGroupViewPage.assertLastModificationBy('jamais');
}

function editSprintGroupInfo(sprintGroupViewPage: SprintGroupViewPage) {
  const titleSprintGroup = sprintGroupViewPage.nameTextField;
  const descriptionSprintGroup = sprintGroupViewPage.descriptionRichField;
  titleSprintGroup.clearTextField();
  titleSprintGroup.fillWithString('Group of Sprint A');
  descriptionSprintGroup.click();
  descriptionSprintGroup.setValue('This is group of sprint');
  descriptionSprintGroup.clickButton('confirm');
  sprintGroupViewPage.assertCreatedBy('RonW');
  sprintGroupViewPage.assertLastModificationBy('RonW');
}

function createSprintInGroupSprint(campaignWorkspace: CampaignWorkspacePage) {
  const sprintDialog = campaignWorkspace.openCreateSprint();
  sprintDialog.assertExists();
  sprintDialog.fillName('Sprint A');
  sprintDialog.fillDescription('Ceci est un  sprint');
  sprintDialog.assertIsReady();
  sprintDialog.clickOnAddButton();
  sprintDialog.assertNotExist();
}

function assertSprintInfo(sprintViewPage: SprintViewPage) {
  sprintViewPage.assertNameContains('Sprint A');
  sprintViewPage.informationPanel.descriptionRichField.checkHtmlContent('Ceci est un sprint');
  sprintViewPage.assertCreatedBy('RonW');
  sprintViewPage.assertLastModificationBy('jamais');
  sprintViewPage.assertStartDateSprintIsEmpty();
  sprintViewPage.assertEndDateSprintIsEmpty();
}

function editSprintInfo(sprintViewPage: SprintViewPage) {
  const titleSprint = sprintViewPage.nameTextField;
  const descriptionSprint = sprintViewPage.informationPanel.descriptionRichField;
  titleSprint.clearTextField();
  titleSprint.fillWithString('Sprint abc');
  descriptionSprint.click();
  descriptionSprint.setValue('Description Sprint');
  descriptionSprint.clickButton('confirm');
  sprintViewPage.editStartDateSprint('01/12/2024');
  sprintViewPage.editEndDateSprint('11/12/2024');
  sprintViewPage.assertCreatedBy('RonW');
  sprintViewPage.assertLastModificationBy('RonW');
}
