import { DatabaseUtils } from '../../../utils/database.utils';
import { PrerequisiteBuilder } from '../../../utils/end-to-end/prerequisite/builders/prerequisite-builder';
import { UserBuilder } from '../../../utils/end-to-end/prerequisite/builders/user-prerequisite-builders';
import { UserAccountPage } from '../../../page-objects/pages/user-account/user-account.page';
import { NavBarElement } from '../../../page-objects/elements/nav-bar/nav-bar.element';
import { restApiBaseUrl } from '../../../utils/mocks/request-mock';
import { makeApiRequestWithToken } from '../../../utils/end-to-end/api/api.utils';
import { BaseDialogElement } from '../../../page-objects/elements/dialog/base-dialog.element';
import { format } from 'date-fns';
import { fr } from 'date-fns/locale';
import Chainable = Cypress.Chainable;

describe('Check that a user can create and use a personal API token', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
    initTestData();
  });

  it('F00.1-UC02 - create_and_use_personal_api_token', () => {
    cy.logInAs('RonW', 'admin');
    const userAccountPage = new UserAccountPage();
    createAndUsePersonalApiTokens(userAccountPage);
  });
});

function initTestData() {
  return new PrerequisiteBuilder()
    .withUsers([new UserBuilder('RonW').withFirstName('Ron').withLastName('Weasley')])
    .build();
}

function createAndUsePersonalApiTokens(userAccountPage: UserAccountPage): void {
  const menuUserAccount = NavBarElement.showSubMenu('user-menu', 'user-menu');
  menuUserAccount.item('user-account').click();
  userAccountPage.assertExists();

  // Create two personal API tokens
  const oneWeekInMilliseconds = 7 * 24 * 60 * 60 * 1000;
  const expiryDateInOneWeek = new Date().getTime() + oneWeekInMilliseconds;
  const expiryDateInTwoWeeks = new Date().getTime() + oneWeekInMilliseconds * 2;

  const firstToken = {
    name: 'read token',
    permissionId: 1,
    expiryDate: formatExpiryDate(expiryDateInTwoWeeks),
  };

  const secondToken = {
    name: 'read-write token',
    permissionId: 2,
    expiryDate: formatExpiryDate(expiryDateInOneWeek),
  };

  createApiToken(userAccountPage, firstToken).then((token1) => cy.wrap(token1).as('token1'));
  createApiToken(userAccountPage, secondToken).then((token2) => cy.wrap(token2).as('token2'));

  // Assert data is correct
  userAccountPage.apiTokenPanelElement.personalApiTokenGrid.assertRowCount(2);
  assertInitialRowValues(1, userAccountPage, firstToken.name, 'Lecture', firstToken.expiryDate);
  assertInitialRowValues(
    2,
    userAccountPage,
    secondToken.name,
    'Lecture et écriture',
    secondToken.expiryDate,
  );

  // Call GET /tokens to get the ids of the two API tokens
  cy.get('@token1').then((token) => {
    makeApiRequestWithToken('GET', token.toString(), `${restApiBaseUrl()}/tokens`, null).then(
      (response) => {
        const idToken1 = response.body._embedded['api-tokens'][0].id;
        const idToken2 = response.body._embedded['api-tokens'][1].id;
        cy.wrap(idToken1).as('idToken1');
        cy.wrap(idToken2).as('idToken2');
      },
    );
  });

  cy.get('@idToken1').then((value) => {
    expect(value).to.equal(1);
  });
  cy.get('@idToken2').then((value) => {
    expect(value).to.equal(2);
  });

  // Assert the column 'lastUsage' has been updated for the first token
  cy.visit('user-account');
  const apiTokenRow1 = userAccountPage.apiTokenPanelElement.personalApiTokenGrid.getRow(1);
  extractLastUsage(1).then((extractedDate) => {
    apiTokenRow1.cell('lastUsage').findCell().should('contain.text', extractedDate);
  });

  // Delete second token in UI
  userAccountPage.apiTokenPanelElement.personalApiTokenGrid
    .getRow(2)
    .cell('delete')
    .iconRenderer()
    .click();
  const deleteDialog = new BaseDialogElement('confirm-delete');
  deleteDialog.confirm();

  // Assert second token does not appear anymore
  userAccountPage.apiTokenPanelElement.personalApiTokenGrid.assertRowCount(1);
  userAccountPage.apiTokenPanelElement.personalApiTokenGrid.getRow(2).assertNotExist();
}

function formatExpiryDate(expiryDate: number) {
  return new Date(expiryDate).toLocaleString().substring(0, 10);
}

function createApiToken(
  userAccountPage: UserAccountPage,
  tokenData: { name: string; permissionId: number; expiryDate: string },
): Chainable<string> {
  userAccountPage.clickAddApiToken();
  userAccountPage.fillAddApiTokenForm(tokenData.name, tokenData.permissionId, tokenData.expiryDate);
  userAccountPage.generateApiTokenDialogElement.confirm();
  const tokenChainable = userAccountPage.displayTokenDialogElement.copyTokenValue();
  userAccountPage.displayTokenDialogElement.close();

  return tokenChainable;
}

function assertInitialRowValues(
  rowId: number,
  userAccountPage: UserAccountPage,
  expectedName: string,
  expectedPermissions: string,
  expectedExpiryDate: string,
) {
  const apiTokenRow1 = userAccountPage.apiTokenPanelElement.personalApiTokenGrid.getRow(rowId);
  apiTokenRow1.cell('name').findCell().should('contain.text', expectedName);
  apiTokenRow1.cell('permissions').findCell().should('contain.text', expectedPermissions);
  apiTokenRow1
    .cell('createdOn')
    .findCell()
    .should('contain.text', new Date().toLocaleString().substring(0, 10));
  apiTokenRow1.cell('lastUsage').findCell().should('contain.text', '-');
  apiTokenRow1.cell('expiryDate').findCell().should('contain.text', expectedExpiryDate);
}

export function extractLastUsage(tokenId: number): Chainable<string> {
  return DatabaseUtils.executeSelectQuery(
    `SELECT last_usage
                         FROM API_TOKEN
                         WHERE token_id = '${tokenId}'`,
  ).then((queryResult) => {
    return format(new Date(queryResult[0].last_usage), 'dd/MM/yyyy HH:mm', { locale: fr });
  });
}
