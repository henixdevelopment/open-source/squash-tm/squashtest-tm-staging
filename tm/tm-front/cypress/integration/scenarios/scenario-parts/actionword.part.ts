import { ActionWordWorkspacePage } from '../../page-objects/pages/action-word-workspace/action-word-workspace.page';
import { ActionWordLevelTwoPage } from '../../page-objects/pages/action-word-workspace/action-word/action-word-level-two.page';
import { ActionWordViewPage } from '../../page-objects/pages/action-word-workspace/action-word/action-word-view.page';
import { TestCaseViewSubPage } from '../../page-objects/pages/test-case-workspace/test-case/test-case-view-sub-page';

export function selectNodeProject(
  actionWordWorkspacePage: ActionWordWorkspacePage,
  nameProject: string,
) {
  actionWordWorkspacePage.assertExists();
  actionWordWorkspacePage.tree.findRowId('NAME', nameProject).then((requirementId) => {
    actionWordWorkspacePage.tree.selectRow(requirementId, 'NAME');
  });
}

export function selectNodeInActionWordWorkspace(
  actionWordWorkspacePage: ActionWordWorkspacePage,
  nameOriginNode: string,
  nameNode: string,
) {
  const actionWordTree = actionWordWorkspacePage.tree;
  actionWordWorkspacePage.assertExists();
  actionWordTree.findRowId('NAME', nameOriginNode).then((idProject) => {
    actionWordTree.openNodeIfClosed(idProject);
  });
  actionWordTree.findRowId('NAME', nameNode).then((idRequirement) => {
    actionWordTree.selectNode(idRequirement);
  });
}

export function assertNodeExistsInActionWordWorkspaceProject(
  actionWordWorkspacePage: ActionWordWorkspacePage,
  nameOriginNode: string,
  nameNode: string,
) {
  const actionWordTree = actionWordWorkspacePage.tree;
  actionWordTree.findRowId('NAME', nameOriginNode).then((idProject) => {
    actionWordTree.openNodeIfClosed(idProject);
  });
  actionWordTree.findRowId('NAME', nameNode).then((idRequirement) => {
    actionWordTree.assertNodeExist(idRequirement);
  });
}
export function assertNodeNotExistsInActionWordWorkspaceProject(
  actionWordWorkspacePage: ActionWordWorkspacePage,
  nameOriginNode: string,
  nameNode: string,
) {
  const actionWordTree = actionWordWorkspacePage.tree;
  actionWordTree.findRowId('NAME', nameOriginNode).then((idProject) => {
    actionWordTree.openNodeIfClosed(idProject);
  });
  actionWordTree.findRowId('NAME', nameNode).then((idRequirement) => {
    actionWordTree.assertNodeNotExist(idRequirement);
  });
}
export function editParamGridCellTextInActionWordPage(
  actionWordPage: ActionWordLevelTwoPage | ActionWordViewPage,
  paramName: string,
  cellId: string,
  newValue: string,
) {
  actionWordPage.paramGrid.findRowId('name', paramName).then((idTestCase) => {
    actionWordPage.paramGrid.getCell(idTestCase, cellId).textRenderer().editText(newValue);
  });
}

export function clearDefaultValueInParamGridRow(
  actionWordPage: ActionWordLevelTwoPage | ActionWordViewPage,
  paramName: string,
) {
  actionWordPage.paramGrid.findRowId('name', paramName).then((idTestCase) => {
    actionWordPage.paramGrid.getCell(idTestCase, 'defaultValue').textRenderer().clearCellContent();
  });
}

export function getToSecondLevelViewPageOfTestCase(
  actionWordWorkspace: ActionWordWorkspacePage,
  actionWordViewPage: ActionWordViewPage,
  keywordTestCaseSecondLevelViewPage: TestCaseViewSubPage,
  nameProject: string,
  nameNode: string,
) {
  selectNodeInActionWordWorkspace(actionWordWorkspace, nameProject, nameNode);
  actionWordViewPage.anchors.clickLink('test-cases');
  actionWordViewPage.testCasesGrid.findRowId('projectName', nameProject).then((idTestCase) => {
    actionWordViewPage.testCasesGrid
      .getCell(idTestCase, 'name')
      .linkRenderer()
      .findCellLink()
      .click();
  });
  keywordTestCaseSecondLevelViewPage.assertExists();
}
