import { RequirementWorkspacePage } from '../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { GridElement, TreeElement } from '../../page-objects/elements/grid/grid.element';
import { RequirementVersionViewPage } from '../../page-objects/pages/requirement-workspace/requirement/requirement-version-view.page';
import { RequirementViewPage } from '../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import { GenericTextFieldElement } from '../../page-objects/elements/forms/generic-text-field.element';
import { EditableTextFieldElement } from '../../page-objects/elements/forms/editable-text-field.element';
import { HighLevelRequirementViewPage } from '../../page-objects/pages/requirement-workspace/requirement/high-level-requirement-view.page';
import { RequirementMultiVersionPage } from '../../page-objects/pages/requirement-workspace/requirement/requirement-multi-version.page';
import { RequirementVersionDetailViewPage } from '../../page-objects/pages/requirement-workspace/requirement/requirement-version-detail-view.page';
import { BaseDialogElement } from '../../page-objects/elements/dialog/base-dialog.element';
import { RequirementLinksTypeDialogElement } from '../../page-objects/pages/requirement-workspace/dialogs/requirement-links-type-dialog.element';
import { RemoveRequirementVersionLinksDialogElement } from '../../page-objects/pages/requirement-workspace/dialogs/remove-requirement-version-links-dialog.element';
import { UnbindLowLevelRequirementDialogElement } from '../../page-objects/pages/requirement-workspace/dialogs/unbind-low-level-requirement-dialog.element';
import { RemoveVerifyingTestCasesDialogElement } from '../../page-objects/pages/test-case-workspace/dialogs/remove-verifying-test-cases-dialog.element';
import {
  RequirementVersionDetailViewInformationPage,
  RequirementVersionViewInformationPage,
} from '../../page-objects/pages/requirement-workspace/requirement/requirement-version-view-information.page';
import { CreateRequirementFolderDialogElement } from '../../page-objects/pages/requirement-workspace/dialogs/create-requirement-folder-dialog.element';
import { CreateRequirementDialogElement } from '../../page-objects/pages/requirement-workspace/dialogs/create-requirement-dialog.element';
import { CreateHighLevelRequirementDialogElement } from '../../page-objects/pages/requirement-workspace/dialogs/create-high-level-requirement-dialog.element';
import { ToolbarButtonElement } from '../../page-objects/elements/workspace-common/toolbar.element';
import { MenuElement } from '../../utils/menu.element';
import { CreateNewVersionRequirementDialogElement } from '../../page-objects/pages/requirement-workspace/dialogs/create-new-version-requirement-dialog.element';
import { CreateEntityDialog } from '../../page-objects/pages/create-entity-dialog.element';
import { TreeToolbarElement } from '../../page-objects/elements/workspace-common/tree-toolbar.element';
import { KeyboardShortcuts } from '../../page-objects/elements/workspace-common/keyboard-shortcuts';
import { RequirementSearchPage } from '../../page-objects/pages/requirement-workspace/search/requirement-search-page';
import { ListPanelElement } from '../../page-objects/elements/filters/list-panel.element';
import { ProjectScopePage } from '../../page-objects/pages/requirement-workspace/search/ProjectScopePage';
import { HighLevelRequirementVersionPage } from '../../page-objects/pages/requirement-workspace/requirement/high-level-requirement-version.page';
import { DeleteTreeNodesDialogElement } from '../../page-objects/pages/test-case-workspace/dialogs/delete-tree-nodes-dialog.element';
import { RequirementSearchMilestoneMassEdit } from '../../../../projects/sqtm-core/src/lib/model/requirement/requirement-search.model';
import { ConfirmInterProjectPaste } from '../../page-objects/elements/dialog/confirm-inter-project-paste';
import { AlertDialogElement } from '../../page-objects/elements/dialog/alert-dialog.element';
import { RemoveDatasetDialog } from '../../page-objects/pages/test-case-workspace/dialogs/remove-dataset-dialog.element';
import { RequirementSecondLevelPage } from '../../page-objects/pages/requirement-workspace/requirement/requirement-second-level.page';

export function selectNodeInAnotherNode(
  requirementWorkspacePage: RequirementWorkspacePage,
  nameProject: string,
  nameNode: string,
) {
  requirementWorkspacePage.assertExists();
  requirementWorkspacePage.tree.findRowId('NAME', nameProject).then((requirementId) => {
    requirementWorkspacePage.tree.selectRow(requirementId, 'NAME');
    requirementWorkspacePage.tree.openNodeIfClosed(requirementId);
  });
  requirementWorkspacePage.tree.findRowId('NAME', nameNode).then((requirementId) => {
    requirementWorkspacePage.tree.selectNode(requirementId);
  });
}

export function selectNodeProject(
  requirementWorkspacePage: RequirementWorkspacePage,
  nameProject: string,
) {
  requirementWorkspacePage.assertExists();
  requirementWorkspacePage.tree.findRowId('NAME', nameProject).then((requirementId) => {
    requirementWorkspacePage.tree.selectRow(requirementId, 'NAME');
  });
}

export function openStandardRequirementWorkspaceElementChildNode(
  requirementWorkspacePage: RequirementWorkspacePage,
  requirementWorkspaceTree: TreeElement,
  nameProject: string,
  nameNode: string,
) {
  requirementWorkspacePage.assertExists();
  requirementWorkspaceTree.findRowId('NAME', nameProject).then((requirementId) => {
    requirementWorkspaceTree.selectRow(requirementId, 'NAME');
    requirementWorkspaceTree.openNodeIfClosed(requirementId);
  });
  requirementWorkspaceTree.findRowId('NAME', nameNode).then((requirementId) => {
    requirementWorkspaceTree.getTreeNodeCell(requirementId).toggle();
  });
}

export function openStandardChildNodeAndClickOnHighLevelRequirementToAccessSecondLevelPage(
  requirementWorkspacePage: RequirementWorkspacePage,
  requirementWorkspaceTree: TreeElement,
  requirementVersionViewPage: RequirementVersionViewPage,
  requirementViewPage: RequirementViewPage,
  highLevelRequirementName: GenericTextFieldElement,
  nameProject: string,
  nameHighLevelRequirement: string,
  nameChildRequirement: string,
) {
  openStandardRequirementWorkspaceElementChildNode(
    requirementWorkspacePage,
    requirementWorkspaceTree,
    nameProject,
    nameHighLevelRequirement,
  );
  requirementWorkspaceTree.findRowId('NAME', nameChildRequirement).then((requirementId) => {
    requirementWorkspaceTree.selectRow(requirementId, 'NAME');
    requirementViewPage.assertExists();
    highLevelRequirementName.assertExists();
    highLevelRequirementName.checkContent(nameHighLevelRequirement);
    highLevelRequirementName.click();
    requirementVersionViewPage.assertExists();
  });
}

export function modifyNameAndReferenceRequirement(
  nameTextField: EditableTextFieldElement,
  referenceTextField: EditableTextFieldElement,
  newNameValue: string,
  newReferenceValue: string,
) {
  nameTextField.setValue(newNameValue);
  nameTextField.clickOnConfirmButton();
  nameTextField.checkContent(newNameValue);
  referenceTextField.setValue(newReferenceValue);
  referenceTextField.clickOnConfirmButton();
  referenceTextField.checkContent(newReferenceValue);
}

export function assertRequirementTreeHabilitation(
  requirementWorkspacePage: RequirementWorkspacePage,
  requirementWorkspaceTree: TreeElement,
  nameProject: string,
  nameRequirement: string,
) {
  requirementWorkspacePage.clickOnParameterButton();
  requirementWorkspacePage.assertParameterOverlayExists();
  requirementWorkspacePage.pressEscape();

  requirementWorkspaceTree.findRowId('NAME', nameProject).then((requirementId) => {
    const requirementLibraryView = requirementWorkspacePage.tree.selectNode(requirementId);
    requirementLibraryView.assertExists();
    requirementWorkspacePage.tree.openNodeIfClosed(requirementId);
  });

  requirementWorkspacePage.tree.findRowId('NAME', nameRequirement).then((requirementId) => {
    requirementWorkspacePage.tree.selectNode(requirementId);
  });
}

function selectBetweenRequirementAndHighLevelRequirementView(
  viewType: 'requirement' | 'high-level-requirement',
): RequirementViewPage | HighLevelRequirementViewPage {
  let viewPage: RequirementViewPage | HighLevelRequirementViewPage = null;

  switch (viewType) {
    case 'requirement':
      viewPage = new RequirementViewPage();
      break;
    case 'high-level-requirement':
      viewPage = new HighLevelRequirementViewPage('*', '*');
      break;
    default:
      throw new Error(`View type "${viewType}" is not handled`);
  }
  return viewPage;
}

export function chooseRequirementOrHighLevelRequirementVersionPage(
  versionType: 'requirement' | 'high-level-requirement',
): RequirementVersionViewPage | HighLevelRequirementVersionPage {
  let versionPage: RequirementVersionViewPage | HighLevelRequirementVersionPage = null;

  switch (versionType) {
    case 'requirement':
      versionPage = new RequirementVersionViewPage();
      break;
    case 'high-level-requirement':
      versionPage = new HighLevelRequirementVersionPage('*');
      break;
    default:
      throw new Error(
        `"${versionType}" is unknown. The method can only take as argument requirement and high-level-requirement`,
      );
  }
  return versionPage;
}

export function linkRequirementToRequirementInClassicViewPage(
  viewType: 'requirement' | 'high-level-requirement',
  projectName: string,
  requirementName: string,
  expectedCount: number,
) {
  const viewPage = selectBetweenRequirementAndHighLevelRequirementView(viewType);
  const requirementDrawerWorkspace = viewPage.currentVersion.openRequirementDrawer();
  const requirementLinkTable = viewPage.currentVersion.requirementLinkTable;
  const linkRequirementDialog = new RequirementLinksTypeDialogElement();

  requirementDrawerWorkspace.findRowId('NAME', projectName).then((requirementId) => {
    requirementDrawerWorkspace.openNodeIfClosed(requirementId);
  });
  requirementDrawerWorkspace.findRowId('NAME', requirementName).then((requirementId) => {
    requirementDrawerWorkspace.beginDragAndDrop(requirementId);
  });
  viewPage.currentVersion.dropRequirementIntoRequirement(false, { versionName: requirementName });
  linkRequirementDialog.confirmLink();
  viewPage.currentVersion.closeDrawer();
  requirementLinkTable.assertRowCount(expectedCount);
  requirementLinkTable.findRowId('name', requirementName).then((idRequirement) => {
    requirementLinkTable.getRow(idRequirement).findRow().should('contain', requirementName);
  });
}

export function linkRequirementToRequirementInSecondLevelPage(
  requirementGettingLinkedName: string,
  projectName: string,
  viewType: 'requirement' | 'high-level-requirement',
) {
  const versionViewPage = chooseRequirementOrHighLevelRequirementVersionPage(viewType);
  const linkRequirementDialog = new BaseDialogElement('requirement-version-link');
  const requirementDrawerWorkspace = versionViewPage.openRequirementDrawer();
  requirementDrawerWorkspace.findRowId('NAME', projectName).then((requirementId) => {
    requirementDrawerWorkspace.openNode(requirementId);
  });
  requirementDrawerWorkspace
    .findRowId('NAME', requirementGettingLinkedName)
    .then((requirementId) => {
      requirementDrawerWorkspace.beginDragAndDrop(requirementId);
    });
  versionViewPage.dropRequirementIntoRequirement(true);
  linkRequirementDialog.clickOnConfirmButton();
  versionViewPage.closeDrawer();
}

export function linkRequirementToAnyRequirementInVersionPage(
  requirementWorkspace: RequirementWorkspacePage,
  viewType: 'requirement' | 'high-level-requirement',
  requirementMultiVersionPage: RequirementMultiVersionPage,
  requirementVersionDetailPage: RequirementVersionDetailViewPage,
  projectName: string,
  requirementToLinkToName: string,
  requirementGettingLinkedName: string,
  expectedCount: number,
) {
  const viewPage = selectBetweenRequirementAndHighLevelRequirementView(viewType);
  const requirementLinkTable =
    requirementVersionDetailPage.requirementVersionPage.requirementLinkTable;

  requirementWorkspace.tree.findRowId('NAME', projectName).then((requirementId) => {
    requirementWorkspace.tree.selectNode(requirementId);
    requirementWorkspace.tree.openNode(requirementId);
  });
  requirementWorkspace.tree.findRowId('NAME', requirementToLinkToName).then((requirementId) => {
    requirementWorkspace.tree.selectNode(requirementId);
  });
  const informationPage = viewPage.currentVersion.clickInformationAnchorLink();
  accessToSecondVersionOfTheRequirement(
    informationPage,
    requirementMultiVersionPage,
    requirementVersionDetailPage,
    requirementToLinkToName,
  );
  linkRequirementToRequirementInSecondLevelPage(
    requirementGettingLinkedName,
    projectName,
    viewType,
  );
  requirementLinkTable.assertRowCount(expectedCount);
  requirementLinkTable.findRowId('name', requirementGettingLinkedName).then((idRequirement) => {
    requirementLinkTable
      .getRow(idRequirement)
      .findRow()
      .should('contain', requirementGettingLinkedName);
  });
}

export function linkRequirementFromLowLevelRequirementInHighLevelRequirement(
  highLevelRequirementViewPage: HighLevelRequirementViewPage,
  linkHighLevelToLowLevelRequirementTable: GridElement,
  projectName: string,
  requirementName: string,
  expectedCount: number,
) {
  const linkedLowLevelRequirementPage =
    highLevelRequirementViewPage.openBindStandardRequirementDrawer();
  linkedLowLevelRequirementPage.findRowId('NAME', projectName).then((requirementId) => {
    linkedLowLevelRequirementPage.openNode(requirementId);
  });
  linkedLowLevelRequirementPage.findRowId('NAME', requirementName).then((requirementId) => {
    linkedLowLevelRequirementPage.beginDragAndDrop(requirementId);
  });
  highLevelRequirementViewPage.dropRequirementIntoLinkedLowLevelReqTable();
  highLevelRequirementViewPage.closeDrawer();
  linkHighLevelToLowLevelRequirementTable.assertRowCount(expectedCount);
  linkHighLevelToLowLevelRequirementTable
    .findRowId('name', requirementName)
    .then((idRequirement) => {
      linkHighLevelToLowLevelRequirementTable
        .getRow(idRequirement)
        .findRow()
        .should('contain', requirementName);
    });
}

export function changeLinkTypeOfRequirementInVersionPage(
  requirementVersionViewPage: RequirementVersionViewPage,
  requirementName: string,
  relationTypeLink:
    | 'Enfant - Parent'
    | 'Relatif - Relatif'
    | 'Parent - Enfant'
    | 'Doublon - Doublon',
  linkType: 'Parent' | 'Relatif' | 'Doublon' | 'Enfant',
) {
  const linkRequirementDialog = new BaseDialogElement('requirement-version-link');
  const dialogConfirmLinkRequirement = new RequirementLinksTypeDialogElement();
  requirementVersionViewPage.requirementLinkTable
    .findRowId('name', requirementName)
    .then((idRequirement) => {
      requirementVersionViewPage.showEditTypeDialog(idRequirement);
      dialogConfirmLinkRequirement.selectField.selectValue(relationTypeLink);
      linkRequirementDialog.clickOnConfirmButton();
      requirementVersionViewPage.requirementLinkTable
        .getRow(idRequirement)
        .cell('role')
        .findCellTextSpan()
        .contains(linkType);
    });
}

export function unlinkRequirementInVersionPage(
  requirementVersionPage: RequirementVersionViewPage,
  removeRequirementLinkDialog: RemoveRequirementVersionLinksDialogElement,
  nameRequirement: string,
  requirementVersionId: number,
  expectedCount: number,
) {
  const requirementLinkTable = requirementVersionPage.requirementLinkTable;
  requirementLinkTable.findRowId('name', nameRequirement).then((idRequirement) => {
    requirementVersionPage.showDeleteConfirmRequirementLinkDialog(
      requirementVersionId,
      idRequirement,
    );
    removeRequirementLinkDialog.clickOnConfirmButton();
    requirementLinkTable.assertRowCount(expectedCount, 'mainViewport');
  });
}

export function unlinkRequirementWithTopIconInVersionPage(
  requirementVersionPage: RequirementVersionViewPage,
  removeRequirementLinkDialog: RemoveRequirementVersionLinksDialogElement,
  nameRequirement: string,
  requirementVersionId: number,
) {
  const requirementLinkTable = requirementVersionPage.requirementLinkTable;
  requirementLinkTable.findRowId('name', nameRequirement).then((idRequirement) => {
    requirementVersionPage.showDeleteConfirmRequirementLinkDialog(
      requirementVersionId,
      idRequirement,
    );
    removeRequirementLinkDialog.clickOnConfirmButton();
  });
}

export function unlinkRequirementFromLowLevelRequirementInVersionPage(
  highLevelRequirementViewPage: HighLevelRequirementViewPage,

  unbindLowLevelRequirementDialog: UnbindLowLevelRequirementDialogElement,
  nameRequirement: string,
  requirementVersionId: number,
  expectedCount: number,
) {
  const linkHighLevelToLowLevelRequirementTable =
    highLevelRequirementViewPage.linkedLowLevelRequirementTable;
  linkHighLevelToLowLevelRequirementTable
    .findRowId('name', nameRequirement)
    .then((requirementId) => {
      linkHighLevelToLowLevelRequirementTable.selectRow(requirementId, '#', 'leftViewport');
      highLevelRequirementViewPage.showUnbindMultipleLowLevelReqDialog(requirementVersionId, [
        requirementId,
      ]);
      unbindLowLevelRequirementDialog.clickOnConfirmButton();
    });
  linkHighLevelToLowLevelRequirementTable.assertRowCount(expectedCount, 'mainViewport');
}

export function linkTestCaseToRequirementVersion(
  highLevelRequirementVersionPage: RequirementVersionViewPage,
  verifyingTestCaseTable: GridElement,
  nameProject: string,
  testcaseName: string,
  requirementVersionId: number,
) {
  const testCaseWorkspace = highLevelRequirementVersionPage.openTestcaseDrawer();
  testCaseWorkspace.findRowId('NAME', nameProject).then((ProjectId) => {
    testCaseWorkspace.openNode(ProjectId);
  });
  testCaseWorkspace.findRowId('NAME', testcaseName).then((testcaseId) => {
    testCaseWorkspace.beginDragAndDrop(testcaseId);
  });
  highLevelRequirementVersionPage.dropTestCaseIntoRequirement(requirementVersionId);
  highLevelRequirementVersionPage.closeDrawer();
  verifyingTestCaseTable.findRowId('name', testcaseName).then((idTestCase) => {
    verifyingTestCaseTable.getRow(idTestCase).findRow().should('contain.text', testcaseName);
  });
}

export function linkTestCaseToRequirementFromLinkTestCasePageInVersionPage(
  highLevelRequirementVersionPage: RequirementVersionViewPage,
  testCaseName: string,
) {
  const coverageRequirementPage =
    highLevelRequirementVersionPage.navigateToSearchTestCaseForCoverage();
  coverageRequirementPage.assertExists();
  coverageRequirementPage.grid.findRowId('name', testCaseName).then((idTestCase) => {
    coverageRequirementPage.grid.assertSpinnerIsNotPresent();
    coverageRequirementPage.grid.selectRow(idTestCase, '#', 'leftViewport');
    coverageRequirementPage.grid.assertSpinnerIsNotPresent();
  });
  coverageRequirementPage.findByElementId('link-selection-button').click();
}

export function unlinkTestCasesFromRequirementVersionPage(
  highLevelRequirementVersionPage: RequirementVersionViewPage,
  removeVerifyingTestCaseDialog: RemoveVerifyingTestCasesDialogElement,
  verifyingTestCaseTable: GridElement,
  nameTestCase: string,
  requirementVersion: number,
  expectedCount: number,
) {
  verifyingTestCaseTable.findRowId('name', nameTestCase).then((idTestCase) => {
    highLevelRequirementVersionPage.showDeleteConfirmTestCaseDialog(requirementVersion, idTestCase);
  });
  removeVerifyingTestCaseDialog.clickOnConfirmButton();
  verifyingTestCaseTable.assertRowCount(expectedCount);
}

export function unlinkTestCasesFromRequirementVersionPageWithTopIcon(
  requirementVersionViewPage: RequirementVersionViewPage,
  removeVerifyingTestCaseDialog: RemoveVerifyingTestCasesDialogElement,
  verifyingTestCaseTable: GridElement,
  nameTestCase: string,
  requirementVersion: number,
  testCaseId: number,
  expectedCount: number,
) {
  verifyingTestCaseTable.findRowId('name', nameTestCase).then((idTestCase) => {
    verifyingTestCaseTable.assertRowExist(idTestCase);
    verifyingTestCaseTable.selectRow(idTestCase, '#', 'leftViewport');
  });
  requirementVersionViewPage.showDeleteConfirmTestCasesDialogWithTopIcon(requirementVersion, [
    testCaseId,
  ]);
  removeVerifyingTestCaseDialog.clickOnConfirmButton();
  verifyingTestCaseTable.assertRowCount(expectedCount);
}

export function assertNameAndReferenceOfRequirementCanBeChanged(
  requirementView: RequirementViewPage,
  newValueName: string,
  newValueReference: string,
) {
  requirementView.nameTextField.setValue(newValueName);
  requirementView.nameTextField.clickOnConfirmButton();
  requirementView.nameTextField.assertContainsText(newValueName);
  requirementView.referenceTextField.setValue(newValueReference);
  requirementView.referenceTextField.clickOnConfirmButton();
  requirementView.referenceTextField.assertContainsText(newValueReference);
}

export function verifyMilestoneBindingAndUnbindingToRequirement(
  requirementVersionViewInformation: RequirementVersionViewInformationPage,
  milestoneName: string,
) {
  requirementVersionViewInformation.bindMilestone(milestoneName);
  requirementVersionViewInformation.milestoneTagElement.assertContainsText(milestoneName);
  requirementVersionViewInformation.milestoneTagElement.clickOnRemoveMilestoneClose(milestoneName);
}

export function modifyDescriptionInRequirement(
  informationPage: RequirementVersionViewInformationPage,
  newValue: string,
) {
  informationPage.descriptionElement.setAndConfirmValue(newValue);
  informationPage.descriptionElement.checkTextContent(newValue);
}

export function bindMilestoneToRequirement(
  requirementVersionInformationViewPage: RequirementVersionViewInformationPage,
  milestoneName: string,
) {
  requirementVersionInformationViewPage.bindMilestone(milestoneName);
  requirementVersionInformationViewPage.milestoneTagElement.assertContainsText(milestoneName);
}

export function goToRequirementSearchPage(
  requirementWorkspacePage: RequirementWorkspacePage,
  treeToolbarElement: TreeToolbarElement,
) {
  requirementWorkspacePage.assertExists();
  treeToolbarElement.getButton('research-button').clickWithoutSpan();
}

export function accessToSecondVersionOfTheRequirement(
  requirementVersionViewInformationPage: RequirementVersionViewInformationPage,
  requirementMultiVersionPage: RequirementMultiVersionPage,
  requirementVersionViewDetails: RequirementVersionDetailViewPage,
  name: string,
) {
  requirementVersionViewInformationPage.clickOnVersionLink();
  requirementMultiVersionPage.assertExists();
  requirementMultiVersionPage.grid.findRowId('name', name).then((requirementVersionId) => {
    requirementMultiVersionPage.grid
      .getCell(requirementVersionId, 'versionNumber')
      .indexRenderer()
      .select();
  });

  requirementVersionViewDetails.assertExists();
}

export function selectRequirementInMultiVersionPage(
  requirementVersionViewInformationPage: RequirementVersionViewInformationPage,
  requirementMultiVersionPage: RequirementMultiVersionPage,
  name: string,
) {
  requirementVersionViewInformationPage.clickOnVersionLink();

  requirementMultiVersionPage.assertExists();
  requirementMultiVersionPage.grid.findRowId('name', name).then((requirementVersionId) => {
    requirementMultiVersionPage.grid.selectRow(requirementVersionId, '#', 'leftViewport');
  });
}

export function modifyCapsuleStatusAndFieldCategory(
  requirementViewPage: RequirementViewPage,
  requirementVersionViewInformationPage: RequirementVersionDetailViewInformationPage,
  option: string,
  category: string,
) {
  requirementViewPage.statusCapsule.selectOption(option);
  requirementVersionViewInformationPage.changeCategory(category);
}

export function createObjectInRequirementWorkspace(
  requirementWorkspacePage: RequirementWorkspacePage,
  isAddingInProject: boolean,
  projectName: string,
  item: 'new-requirement' | 'new-high-level-requirement' | 'new-folder',
  newEntityName: string,
  containerNodeName?: string,
) {
  if (isAddingInProject === true) {
    selectNodeProject(requirementWorkspacePage, projectName);
  } else {
    selectNodeInAnotherNode(requirementWorkspacePage, projectName, containerNodeName);
  }
  requirementWorkspacePage.openCreateDialog(item);

  let dialog: CreateEntityDialog = null;

  switch (item) {
    case 'new-folder':
      dialog = new CreateRequirementFolderDialogElement();
      break;
    case 'new-high-level-requirement':
      dialog = new CreateHighLevelRequirementDialogElement();
      break;
    case 'new-requirement':
      dialog = new CreateRequirementDialogElement();
  }
  dialog.assertExists();
  dialog.assertIsReady();
  dialog.fillName(newEntityName);
  dialog.clickOnAddButton();
  dialog.assertNotExist();
}

export function addNewVersionHighLevelRequirement(
  highLevelRequirementViewPage: HighLevelRequirementViewPage,
  toolbarButton: ToolbarButtonElement,
  actionMenu: MenuElement,
  informationPage: RequirementVersionViewInformationPage,
  idVersion: string,
) {
  const newVersionDialog = new CreateNewVersionRequirementDialogElement();
  highLevelRequirementViewPage.assertExists();
  toolbarButton.click();
  actionMenu.item('create-new-requirement-version').click();
  newVersionDialog.assertExist();
  newVersionDialog.clickConfirmButton();

  informationPage.assertExists();
  informationPage.assertVersionNumber(idVersion);
  informationPage.assertIsHighLevelRequirement();
}

export function changeHighLevelRequirementToRequirement(
  highLevelRequirementViewPage: HighLevelRequirementViewPage,
  toolbarButton: ToolbarButtonElement,
  actionMenu: MenuElement,
  informationPage: RequirementVersionViewInformationPage,
) {
  const confirmDialog = new BaseDialogElement('cuf-disable-optional-dialog');
  toolbarButton.click();
  actionMenu.item('convert-to-standard-requirement').click();
  confirmDialog.assertExists();
  confirmDialog.clickOnConfirmButton();
  informationPage.assertIsLowLevelRequirement();
}

export function changeRequirementToHighLevelRequirement(
  toolbarButton: ToolbarButtonElement,
  actionMenu: MenuElement,
  informationPage: RequirementVersionViewInformationPage,
) {
  const confirmDialog = new BaseDialogElement('cuf-disable-optional-dialog');
  toolbarButton.click();
  actionMenu.item('convert-to-high-level-requirement').click();
  confirmDialog.assertExists();
  confirmDialog.clickOnConfirmButton();
  informationPage.assertIsHighLevelRequirement();
}

export function copyPasteInRequirementWorkspace(
  requirementWorkspacePage: RequirementWorkspacePage,
  nameProject: string,
  nameNodeToCopy: string,
  nameTargetNode: string,
  withShortCut: boolean,
) {
  selectNodeInAnotherNode(requirementWorkspacePage, nameProject, nameNodeToCopy);
  if (withShortCut === true) {
    const keyboardShortcut = new KeyboardShortcuts();
    keyboardShortcut.performCopy();
    requirementWorkspacePage.tree.findRowId('NAME', nameTargetNode).then((requirementId) => {
      requirementWorkspacePage.tree.pickNode(requirementId);
    });
    keyboardShortcut.performPaste();
  } else {
    const treeToolbarElement = new TreeToolbarElement('requirement-toolbar');
    treeToolbarElement.getButton('copy-button').clickWithoutSpan();
    requirementWorkspacePage.tree.findRowId('NAME', nameTargetNode).then((requirementId) => {
      requirementWorkspacePage.tree.pickNode(requirementId);
    });
    treeToolbarElement.getButton('paste-button').clickWithoutSpan();
  }
}

export function selectObjectAsPerimeterInSearchRequirementPage(
  requirementSearchPage: RequirementSearchPage,
  projectScope: ProjectScopePage,
  projectName: string,
  objectName: string,
) {
  requirementSearchPage.assertExists();
  requirementSearchPage.findByElementId('grid-scope-field').click();

  projectScope.assertExists();
  projectScope.findByComponentId('custom-scope').click();
  projectScope.tree.findRowId('NAME', projectName).then((requirementId) => {
    projectScope.tree.getTreeNodeCell(requirementId).toggle();
  });
  projectScope.tree.findRowId('NAME', objectName).then((requirementId) => {
    projectScope.tree.pickNode(requirementId);
    projectScope.findByComponentId('confirm-scope').click();
  });
}

export function modifyFieldAttributesWithListInSearchRequirementPage(
  requirementSearchPage: RequirementSearchPage,
  projectName: string,
  cellId: string,
  itemName: string,
) {
  const itemList = new ListPanelElement();
  requirementSearchPage.grid.findRowId('projectName', projectName).then((projectId) => {
    requirementSearchPage.grid.getCell(projectId, cellId).indexRenderer().select();
  });
  itemList.findItem(itemName).click();
  requirementSearchPage.grid.findRowId('projectName', projectName).then((projectId) => {
    requirementSearchPage.grid
      .getCell(projectId, cellId)
      .textRenderer()
      .assertContainsText(itemName);
  });
}

export function modifyFieldAttributeWithTextFieldInSearchRequirementPage(
  requirementSearchPage: RequirementSearchPage,
  projectName: string,
  cellId: string,
  newValue: string,
) {
  requirementSearchPage.grid.findRowId('projectName', projectName).then((projectId) => {
    requirementSearchPage.grid.getCell(projectId, cellId).textRenderer().editText(newValue);
    requirementSearchPage.grid
      .getCell(projectId, cellId)
      .textRenderer()
      .assertContainsText(newValue);
  });
}

export function massEditInRequirementSearchPage(
  requirementSearchPage: RequirementSearchPage,
  projectName: string,
  fieldId: string,
  value: string,
) {
  requirementSearchPage.grid.selectRowWithMatchingCellContent('projectName', projectName);
  const massEditDialog = requirementSearchPage.showMassEditDialog();
  massEditDialog.getOptionalField(fieldId).check();
  massEditDialog.getOptionalField(fieldId).selectValue(value);
  massEditDialog.getOptionalField(fieldId).assertContains(value);
  massEditDialog.confirm();
}

export function bindMilestoneInSearchPage(
  requirementSearchPage: RequirementSearchPage,
  projectName: string,
  milestoneName: string,
) {
  requirementSearchPage.grid.selectRowWithMatchingCellContent('projectName', projectName);
  const massBindingMilestoneDialog =
    requirementSearchPage.showMassBindingMilestoneDialog<RequirementSearchMilestoneMassEdit>();
  const massBindingMilestoneDialogGrid = massBindingMilestoneDialog.getMilestoneGrid();
  massBindingMilestoneDialogGrid.findRowId('label', milestoneName).then((milestoneId) => {
    massBindingMilestoneDialogGrid
      .getCell(milestoneId, 'select-row-column')
      .checkBoxRender()
      .findCheckbox()
      .click();
  });
  massBindingMilestoneDialog.confirmForReq();
}

export function dragObjectInToAnotherObjectInRequirementWorkspace(
  requirementWorkspacePage: RequirementWorkspacePage,
  originNodeName: string,
  nodeToDrag: string,
  nodeToDrop: string,
  objectContainingDrag: string,
  interProject: boolean,
) {
  selectNodeInAnotherNode(requirementWorkspacePage, originNodeName, nodeToDrag);
  requirementWorkspacePage.tree.findRowId('NAME', nodeToDrag).then((idNodeToDrag) => {
    requirementWorkspacePage.tree.findRowId('NAME', nodeToDrop).then((idNodeToDrop) => {
      requirementWorkspacePage.tree
        .findRowId('NAME', objectContainingDrag)
        .then((idObjectWhereDrag) => {
          requirementWorkspacePage.tree.dragAndDropObjectInObjectInWorkspace(
            idNodeToDrag,
            idObjectWhereDrag,
            idNodeToDrop,
            interProject,
          );
        });
    });
  });
}

export function deleteElementInRequirementWorkspaceWithShortCuts(
  requirementWorkspace: RequirementWorkspacePage,
  nameOriginNode: string,
  nameNodeToDelete: string,
) {
  const shortCutDelete = new KeyboardShortcuts();
  const confirmDialog = new DeleteTreeNodesDialogElement('requirement-tree');
  selectNodeInAnotherNode(requirementWorkspace, nameOriginNode, nameNodeToDelete);
  shortCutDelete.pressDelete();
  confirmDialog.clickOnConfirmButton();
}

export function deleteElementInRequirementWorkspaceWithToolBarButton(
  requirementWorkspace: RequirementWorkspacePage,
  nameOriginNode: string,
  nameNodeToDelete: string,
) {
  const treeToolbarElement = new TreeToolbarElement('requirement-toolbar');
  const confirmDialog = new DeleteTreeNodesDialogElement('requirement-tree');
  selectNodeInAnotherNode(requirementWorkspace, nameOriginNode, nameNodeToDelete);
  treeToolbarElement.getButton('delete-button').clickWithoutSpan();
  confirmDialog.clickOnConfirmButton();
}

export function associateTestCaseToRequirementFromLinkTestCasePage(
  requirementType: 'requirement' | 'high-level-requirement',
  nameTestCase: string,
) {
  const versionViewPage = chooseRequirementOrHighLevelRequirementVersionPage(requirementType);
  const coverageRequirementPage = versionViewPage.navigateToSearchTestCaseForCoverage();
  coverageRequirementPage.assertExists();
  coverageRequirementPage.grid.findRowId('name', nameTestCase).then((idTestCase) => {
    coverageRequirementPage.grid.assertSpinnerIsNotPresent();
    coverageRequirementPage.grid.selectRow(idTestCase, '#', 'leftViewport');
  });
  coverageRequirementPage.findByElementId('link-selection-button').click();
}

export function linkRequirementAndHighLevelRequirementToAnyRequirement(
  requirementWorkspacePage: RequirementWorkspacePage,
  projectName: string,
  requirementName: string,
  viewType: 'requirement' | 'high-level-requirement',
  requirementInLinkTableName: string,
  highLevelRequirementInLinkTableName: string,
) {
  selectNodeInAnotherNode(requirementWorkspacePage, projectName, requirementName);
  const viewPage = selectBetweenRequirementAndHighLevelRequirementView(viewType);
  linkRequirementToRequirementInClassicViewPage(
    viewType,
    projectName,
    requirementInLinkTableName,
    1,
  );
  linkRequirementToRequirementInClassicViewPage(
    viewType,
    projectName,
    highLevelRequirementInLinkTableName,
    2,
  );
  viewPage.currentVersion.requirementLinkTable
    .findRowId('name', requirementInLinkTableName)
    .then((idRequirement) => {
      viewPage.currentVersion.requirementLinkTable.assertRowExist(idRequirement);
    });
  viewPage.currentVersion.requirementLinkTable
    .findRowId('name', highLevelRequirementInLinkTableName)
    .then((idRequirement) => {
      viewPage.currentVersion.requirementLinkTable.assertRowExist(idRequirement);
    });
}

export function assertCopyObjectAllowedAndPasteProjectDisabled(
  requirementWorkspacePage: RequirementWorkspacePage,
  nameProject: string,
  nameNode: string,
  nameTargetNode: string,
) {
  selectNodeInAnotherNode(requirementWorkspacePage, nameProject, nameNode);
  requirementWorkspacePage.treeMenu.assertCopyButtonIsActive();
  requirementWorkspacePage.treeMenu.copy();
  selectNodeProject(requirementWorkspacePage, nameTargetNode);
  requirementWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
}

export function assertCopyObjectAllowedAndPasteInAnotherProjectDisabledWithShortCut(
  requirementWorkspacePage: RequirementWorkspacePage,
  nameProject: string,
  nameNode: string,
  nameTargetNode: string,
) {
  selectNodeInAnotherNode(requirementWorkspacePage, nameProject, nameNode);
  const keyboardShortcut = new KeyboardShortcuts();
  keyboardShortcut.performCopy();
  selectNodeProject(requirementWorkspacePage, nameTargetNode);
  keyboardShortcut.performPaste();
  requirementWorkspacePage.tree.assertNodeNotExist(nameProject);
}
export function canNotDragAndDropObjectWithinOrBetweenProjects(
  requirementWorkspacePage: RequirementWorkspacePage,
  originNodeName: string,
  nodeToDrag: string,
  nodeToDrop: string,
  multiPermissionOfUser: boolean,
) {
  requirementWorkspacePage.tree.findRowId('NAME', originNodeName).then((idNodeOrigin) => {
    requirementWorkspacePage.tree.selectRow(idNodeOrigin, 'NAME');
    requirementWorkspacePage.tree.openNodeIfClosed(idNodeOrigin);
    requirementWorkspacePage.tree.findRowId('NAME', nodeToDrag).then((idNodeDrag) => {
      requirementWorkspacePage.tree.selectNode(idNodeDrag);
      requirementWorkspacePage.tree.beginDragAndDrop(idNodeDrag);
      requirementWorkspacePage.tree.findRowId('NAME', nodeToDrop).then((idNodeDrop) => {
        requirementWorkspacePage.tree.selectNode(idNodeDrop);
        requirementWorkspacePage.tree.dropIntoOtherProject(idNodeDrop);
        if (multiPermissionOfUser) {
          const confirmDialog = new ConfirmInterProjectPaste('confirm-inter-project-move');
          confirmDialog.assertExists();
          confirmDialog.clickOnConfirmButton();
          const alertDialog = new AlertDialogElement('alert');
          alertDialog.close();
        }
        requirementWorkspacePage.tree.assertRowHasParent(idNodeDrag, idNodeOrigin);
      });
    });
  });
}
export function assertNodeExistsInRequirementWorkspaceProject(
  requirementWorkspacePage: RequirementWorkspacePage,
  nameOriginNode: string,
  nameNode: string,
) {
  const requirementTree = requirementWorkspacePage.tree;
  requirementTree.findRowId('NAME', nameOriginNode).then((idProject) => {
    requirementTree.openNodeIfClosed(idProject);
  });
  requirementTree.findRowId('NAME', nameNode).then((idRequirement) => {
    requirementTree.assertNodeExist(idRequirement);
  });
}

export function verifyCannotAssociateRequirements(
  requirementWorkspacePage: RequirementWorkspacePage,
  viewType: 'requirement' | 'high-level-requirement',
  nameParentNode: string,
  nameNode: string,
) {
  selectNodeInAnotherNode(requirementWorkspacePage, nameParentNode, nameNode);
  const viewPage = selectBetweenRequirementAndHighLevelRequirementView(viewType);
  viewPage.assertExists();
  viewPage.currentVersion.assertIconLinkRequirementDoesNotExist();
}

export function verifyIconNotPresentInLinkedRequirementTable(
  viewType: 'requirement' | 'high-level-requirement',
  rowId: string,
  contentCell: string,
  icon: string,
) {
  const viewPage = selectBetweenRequirementAndHighLevelRequirementView(viewType);
  viewPage.currentVersion.requirementLinkTable
    .findRowId(rowId, contentCell)
    .then((requirementId) => {
      viewPage.currentVersion.requirementLinkTable
        .getRow(requirementId, 'rightViewport')
        .cell(icon)
        .assertNotExist();
    });
}

export function verifyLinkCannotBeModified(
  viewType: 'requirement' | 'high-level-requirement',
  rowId: string,
  contentCell: string,
) {
  verifyIconNotPresentInLinkedRequirementTable(viewType, rowId, contentCell, 'edit');
}

export function verifyCannotDisassociateLinkedRequirementsInLowLevelRequirementTableOfAHighLevelRequirement(
  requirementName: string,
) {
  const keyboardShortcut = new KeyboardShortcuts();
  const confirmDeleteDialog = new RemoveDatasetDialog();
  const highLevelRequirementViewPage = new HighLevelRequirementViewPage('*', '*');

  highLevelRequirementViewPage.linkedLowLevelRequirementTable
    .findRowId('name', requirementName)
    .then((requirementId) => {
      highLevelRequirementViewPage.linkedLowLevelRequirementTable
        .getRow(requirementId)
        .cell('delete')
        .assertNotExist();
    });
  highLevelRequirementViewPage.assertIconNotExists('remove-linked-low-level-requirements');
  highLevelRequirementViewPage.linkedLowLevelRequirementTable
    .findRowId('name', requirementName)
    .then((requirementId) => {
      highLevelRequirementViewPage.linkedLowLevelRequirementTable.selectRow(
        requirementId,
        '#',
        'leftViewport',
      );
    });
  keyboardShortcut.pressDelete();
  confirmDeleteDialog.assertNotExist();
}

export function verifyCannotDisassociateLinkedRequirementInRequirement(
  viewType: 'requirement' | 'high-level-requirement',
  requirementName: string,
) {
  const keyboardShortcut = new KeyboardShortcuts();
  const confirmDeleteDialog = new RemoveDatasetDialog();
  const viewPage = selectBetweenRequirementAndHighLevelRequirementView(viewType);

  viewPage.assertIconNotExists('remove-requirement-links');
  verifyIconNotPresentInLinkedRequirementTable('requirement', 'name', requirementName, 'delete');
  viewPage.currentVersion.requirementLinkTable
    .findRowId('name', requirementName)
    .then((requirementId) => {
      viewPage.currentVersion.requirementLinkTable.selectRow(requirementId, '#', 'leftViewport');
    });
  keyboardShortcut.pressDelete();
  confirmDeleteDialog.assertNotExist();
}

export function verifyButtonsToAddOrRemoveRelationToAHighLevelRequirementAreNotPresentInRequirement() {
  const requirementViewPage = new RequirementViewPage('*', '*');
  const informationBlock = requirementViewPage.currentVersion.clickInformationAnchorLink();

  informationBlock.highLevelRequirementName.rootElement.trigger('mouseover');
  informationBlock.assertButtonUnlinkHighLevelRequirementIsNotPresent();
  informationBlock.assertButtonChangeHighLevelRequirementIsNotPresent();
}

export function selectRequirementAndAssertButtonLinkToHighRequirementIsNotPresent(
  requirementWorkspace: RequirementWorkspacePage,
  nameProject: string,
  nameNode: string,
) {
  const requirementViewPage = new RequirementViewPage();

  selectNodeInAnotherNode(requirementWorkspace, nameProject, nameNode);
  requirementViewPage.currentVersion
    .clickInformationAnchorLink()
    .assertButtonBindHighLevelRequirementIsNotPresent();
}

export function verifyCannotAssociateTestCases(viewType: 'requirement' | 'high-level-requirement') {
  const viewPage = selectBetweenRequirementAndHighLevelRequirementView(viewType);
  viewPage.currentVersion.anchors.clickLink('linked-test-case');
  viewPage.assertIconNotExists('add-verifying-test-case');
  viewPage.assertIconNotExists('search-coverages');
}

export function verifyCannotDisassociateTestCase(
  viewType: 'requirement' | 'high-level-requirement',
  testCaseName: string,
) {
  const viewPage = selectBetweenRequirementAndHighLevelRequirementView(viewType);
  const confirmDeleteDialog = new RemoveDatasetDialog();
  const keyboardShortcut = new KeyboardShortcuts();

  viewPage.assertIconNotExists('remove-verifying-test-case');
  viewPage.currentVersion.verifyingTestCaseTable
    .findRowId('name', testCaseName)
    .then((idTestCase) => {
      viewPage.currentVersion.verifyingTestCaseTable
        .getRow(idTestCase)
        .cell('delete')
        .assertNotExist();
    });
  viewPage.currentVersion.verifyingTestCaseTable
    .findRowId('name', testCaseName)
    .then((idTestCase) => {
      viewPage.currentVersion.verifyingTestCaseTable.selectRow(idTestCase, '#', 'leftViewport');
    });
  keyboardShortcut.pressDelete();
  confirmDeleteDialog.assertNotExist();
}
export function verifyCannotAssociateRequirementsFromRequirementSecondLevelVersionViewPage(
  requirementWorkspacePage: RequirementWorkspacePage,
  requirementSecondLevelPage: RequirementSecondLevelPage,
) {
  selectNodeInAnotherNode(requirementWorkspacePage, 'Project', 'Exig');
  const requirementViewPage = new RequirementViewPage('*', '*');
  const informationBlock = requirementViewPage.currentVersion.clickInformationAnchorLink();
  informationBlock.clickOnHighLevelRequirementName();
  requirementSecondLevelPage.assertExists();
  requirementSecondLevelPage.assertElementsPageAreFullyLoaded();
  requirementSecondLevelPage.requirementVersionViewPage.assertIconLinkRequirementDoesNotExist();
}
export function verifyCannotAssociateTestCasesFromRequirementSecondLevelVersionViewPage(
  requirementVersionViewPage: RequirementVersionViewPage,
) {
  requirementVersionViewPage.anchors.clickLink('linked-test-case');
  requirementVersionViewPage.assertIconNotExists('add-verifying-test-case');
  requirementVersionViewPage.assertIconNotExists('search-coverages');
}

export function assertGuestCanNotDissociateLinkedRequirementFromRequirementSecondLevelVersionViewPage(
  requirementVersionViewPage: RequirementVersionViewPage,
) {
  requirementVersionViewPage.anchors.clickLink('requirement-version-links');
  requirementVersionViewPage.assertIconNotExists('remove-requirement-links');
  verifyIconNotPresentInGrid(
    requirementVersionViewPage.requirementLinkTable,
    'name',
    'Exig',
    'delete',
  );
  selectRowInGrid(requirementVersionViewPage.requirementLinkTable, 'name', 'Exig');
  const keyboardShortcut = new KeyboardShortcuts();
  const confirmDeleteDialog = new RemoveDatasetDialog();
  keyboardShortcut.pressDelete();
  confirmDeleteDialog.assertNotExist();
  assertRowExistsInGrid(requirementVersionViewPage.requirementLinkTable, 'name', 'Exig');
  requirementVersionViewPage.anchors.clickLink('linked-low-level-requirements');
  requirementVersionViewPage.assertIconNotExists('remove-linked-low-level-requirements');
  verifyIconNotPresentInGrid(
    requirementVersionViewPage.requirementLowLevelLinkTable,
    'name',
    'Exig',
    'delete',
  );
  selectRowInGrid(requirementVersionViewPage.requirementLowLevelLinkTable, 'name', 'Exig');
  keyboardShortcut.pressDelete();
  confirmDeleteDialog.assertNotExist();
  assertRowExistsInGrid(requirementVersionViewPage.requirementLowLevelLinkTable, 'name', 'Exig');
}

export function assertGuestCanNotDissociateATestCaseFromRequirementSecondLevelVersionViewPage(
  requirementVersionViewPage: RequirementVersionViewPage,
) {
  const confirmDeleteDialog = new RemoveDatasetDialog();
  const keyboardShortcut = new KeyboardShortcuts();

  requirementVersionViewPage.assertIconNotExists('remove-verifying-test-case');
  verifyIconNotPresentInGrid(
    requirementVersionViewPage.verifyingTestCaseTable,
    'name',
    'Test',
    'delete',
  );
  selectRowInGrid(requirementVersionViewPage.verifyingTestCaseTable, 'name', 'Test');
  keyboardShortcut.pressDelete();
  confirmDeleteDialog.assertNotExist();
  assertRowExistsInGrid(requirementVersionViewPage.verifyingTestCaseTable, 'name', 'Test');
}

export function assertRowExistsInGrid(grid: GridElement, cellId: string, content: string) {
  grid.findRowId(cellId, content).then((idTestCase) => {
    grid.getRow(idTestCase).assertExists();
  });
}

export function verifyIconNotPresentInGrid(
  grid: GridElement,
  rowName: string,
  itemName: string,
  icon: string,
) {
  grid.findRowId(rowName, itemName).then((rowId) => {
    grid.getRow(rowId).cell(icon).assertNotExist();
  });
}

export function selectRowInGrid(grid: GridElement, cellId: string, content: string) {
  grid.findRowId(cellId, content).then((idTestCase) => {
    grid.selectRow(idTestCase, '#', 'leftViewport');
  });
}
