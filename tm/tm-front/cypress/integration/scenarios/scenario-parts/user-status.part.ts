import { CampaignWorkspacePage } from '../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { CampaignViewPage } from '../../page-objects/pages/campaign-workspace/campaign/campaign-view.page';
import {
  checkTestCaseContentIfUserNotAssignedAndInactive,
  clickAndCheckTestCaseAssignee,
  TestPlanProperties,
  UserProperties,
} from './campaign.part';
import { IterationViewPage } from '../../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import { TestSuiteViewPage } from '../../page-objects/pages/campaign-workspace/test-suite/test-suite-view.page';
import { ListPanelElement } from '../../page-objects/elements/filters/list-panel.element';
import { AdminWorkspaceUsersPage } from '../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import { UserViewPage } from '../../page-objects/pages/administration-workspace/user-view/user-view.page';

export interface CheckTestCasesAssignmentElementsAndData {
  campaignWorkspacePage: CampaignWorkspacePage;
  campaignProperties: CampaignObjectProperties;
  iterationProperties: CampaignObjectProperties;
  testSuiteProperties: CampaignObjectProperties;
  userProperties: UserProperties;
  panelList: ListPanelElement;
}

export interface CampaignObjectProperties {
  viewPage: CampaignViewPage | IterationViewPage | TestSuiteViewPage;
  testPlanProperties: TestPlanProperties;
}

export function checkTestCasesAssignmentBasedOnUserActivation(
  elementsAndData: CheckTestCasesAssignmentElementsAndData,
  areNodesOpenInCampaignTree: boolean,
) {
  const {
    campaignWorkspacePage,
    campaignProperties,
    iterationProperties,
    testSuiteProperties,
    userProperties,
    panelList,
  } = elementsAndData;

  const properties = [campaignProperties, iterationProperties, testSuiteProperties];

  properties.forEach((property) => {
    if (!areNodesOpenInCampaignTree) {
      campaignWorkspacePage.tree.openNodeByName(property.testPlanProperties.contentOpen);
    }
    campaignWorkspacePage.tree.selectNodeByName(property.testPlanProperties.contentSelect);
    const testPlanPage = property.viewPage.showTestPlan();
    testPlanPage.testPlan.assertSpinnerIsNotPresent();
    clickAndCheckTestCaseAssignee(
      testPlanPage,
      property.testPlanProperties.testCaseAssigned,
      panelList,
      userProperties,
    );
    if (!userProperties.userActive) {
      checkTestCaseContentIfUserNotAssignedAndInactive(
        testPlanPage,
        property.testPlanProperties.testCaseNotAssigned,
        panelList,
        userProperties,
      );
    }
  });
}

export function changeStatusToInactiveInGridAndCheck(
  adminWorkspaceUsersPage: AdminWorkspaceUsersPage,
  userView: UserViewPage,
  login: string,
) {
  adminWorkspaceUsersPage.grid.findRowId('login', login).then((userID) => {
    adminWorkspaceUsersPage.grid
      .getCell(userID, 'active')
      .iconRenderer()
      .assertContainsIcon('active');
    adminWorkspaceUsersPage.grid.deactivateUserAndWaitResponse(userID);
    adminWorkspaceUsersPage.grid
      .getCell(userID, 'active')
      .iconRenderer()
      .assertContainsIcon('inactive');
  });

  adminWorkspaceUsersPage.selectRowWithMatchingCellContent('login', login);
  userView.assertExists();
  userView.activeSwitch.checkValue(false);
}
