import { selectByDataIcon, selectByDataTestCustomFieldName } from '../../utils/basic-selectors';
import { EditableRichTextFieldElement } from '../../page-objects/elements/forms/editable-rich-text-field.element';
import { ElementSelectorFactory } from '../../page-objects/elements/forms/abstract-form-field.element';
import { EditableSelectFieldElement } from '../../page-objects/elements/forms/editable-select-field.element';
import { CheckBoxElement } from '../../page-objects/elements/forms/check-box.element';
import { EditableDateFieldElement } from '../../page-objects/elements/forms/editable-date-field.element';
import { EditableNumericFieldElement } from '../../page-objects/elements/forms/editable-numeric-field.element';
import { EditableTextFieldElement } from '../../page-objects/elements/forms/editable-text-field.element';
import { EditableTagFieldElement } from '../../page-objects/elements/forms/editable-tag-field.element';
import { InputType } from '../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';

const singleCufUrl: string = 'custom-fields/values/*/single-value';

function getCuf(fieldName: string): ElementSelectorFactory {
  return () => cy.get(selectByDataTestCustomFieldName(fieldName)).siblings().first();
}

export function deleteCufTag(cufName: string, optionValue: string) {
  cy.get(selectByDataTestCustomFieldName(cufName))
    .next()
    .get(`nz-select-item[title="${optionValue}"]`)
    .find(selectByDataIcon('close'))
    .click()
    .get(`nz-select-item[title="${optionValue}"]`)
    .should('not.exist');
}

function getRichTextCuf(cufName: string) {
  return new EditableRichTextFieldElement(getCuf(cufName), singleCufUrl);
}

function getDropDownListCuf(cufName: string) {
  return new EditableSelectFieldElement(getCuf(cufName), singleCufUrl);
}

function getCheckboxCuf(cufName: string) {
  return new CheckBoxElement(getCuf(cufName), singleCufUrl);
}

function getDatePickerCuf(cufName: string) {
  return new EditableDateFieldElement(getCuf(cufName), singleCufUrl);
}

function getNumericCuf(cufName: string) {
  return new EditableNumericFieldElement(getCuf(cufName), singleCufUrl);
}

function getPlainTextCuf(cufName: string) {
  return new EditableTextFieldElement(getCuf(cufName), singleCufUrl);
}

function getTagCuf(cufName: string) {
  return new EditableTagFieldElement(getCuf(cufName), 'custom-fields/values/*/multi-value');
}

export function fillCuf(cufName: string, inputType: InputType, value?: string) {
  switch (inputType) {
    case InputType.RICH_TEXT: {
      getRichTextCuf(cufName).setAndConfirmValue(value);
      getRichTextCuf(cufName).assertContainsText(value);
      break;
    }
    case InputType.DATE_PICKER: {
      getDatePickerCuf(cufName).setDate(value);
      getDatePickerCuf(cufName).checkContent(value, true);
      break;
    }
    case InputType.DROPDOWN_LIST: {
      getDropDownListCuf(cufName).selectValueNoButton(value);
      getDropDownListCuf(cufName).assertContainsText(value);
      break;
    }
    case InputType.PLAIN_TEXT: {
      getPlainTextCuf(cufName).setAndConfirmValue(value);
      getPlainTextCuf(cufName).assertContainsText(value);
      break;
    }
    case InputType.CHECKBOX: {
      getCheckboxCuf(cufName).toggleState(true);
      break;
    }
    case InputType.NUMERIC: {
      const numberValue = +value;
      getNumericCuf(cufName).setAndConfirmValue(numberValue);
      getNumericCuf(cufName).assertContainsText(value);
      break;
    }
    case InputType.TAG: {
      getTagCuf(cufName).typeNewTag(value);
      break;
    }
  }
}
