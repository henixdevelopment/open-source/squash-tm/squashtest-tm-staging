import { CustomReportWorkspacePage } from '../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import { CustomReportFolderViewPage } from '../../page-objects/pages/custom-report-workspace/custom-report-folder-view.page';
import { ModifyChartViewPage } from '../../page-objects/pages/custom-report-workspace/chart/modify-chart-view-page';
import { MilestonePickerDialogElement } from '../../page-objects/elements/grid/milestone-picker-dialog.element';
import { DashboardViewPage } from '../../page-objects/pages/custom-report-workspace/dashboard-view.page';
import { ReportWidget } from '../../page-objects/pages/custom-report-workspace/report-widget/report-widget';
import { ModifyReportDefinitionViewPage } from '../../page-objects/pages/custom-report-workspace/modify-report-definition-view-page';
import { CustomExportViewPage } from '../../page-objects/pages/custom-report-workspace/custom-export-view.page';
import { AxisSelectorElement } from '../../page-objects/elements/chart-workbench/axis-selector.element';
import { WorkspaceColor } from '../../utils/color.utils';
import { ChartDefinitionViewPage } from '../../page-objects/pages/custom-report-workspace/chart/chart-definition-view.page';
import { CreateChartViewPage } from '../../page-objects/pages/custom-report-workspace/chart/create-chart-view.page';

export function selectNodeInCustomReportWorkspace(
  customReportWorkspacePage: CustomReportWorkspacePage,
  nameOriginNode: string,
  nameNode: string,
) {
  const customReportTree = customReportWorkspacePage.tree;
  customReportWorkspacePage.assertExists();
  customReportTree.findRowId('NAME', nameOriginNode).then((idProject) => {
    customReportTree.openNodeIfClosed(idProject);
  });
  customReportTree.findRowId('NAME', nameNode).then((idRequirement) => {
    customReportTree.selectNode(idRequirement);
  });
}

export function renameCustomReportFolderInCustomReportWorkspace(newValue: string) {
  const customReportFolderViewPage = new CustomReportFolderViewPage(1);
  customReportFolderViewPage.assertExists();
  customReportFolderViewPage.nameTextField.setValue(newValue);
  customReportFolderViewPage.nameTextField.clickOnConfirmButton();
  customReportFolderViewPage.nameTextField.assertContainsText(newValue);
}

export function editCustomReportFolderDescriptionInWorkspace(newValue: string) {
  const customReportFolderViewPage = new CustomReportFolderViewPage(1);
  customReportFolderViewPage.assertExists();
  const descriptionFolder = customReportFolderViewPage.descriptionRichField;
  descriptionFolder.click();
  descriptionFolder.setValue(newValue);
  descriptionFolder.confirm();
  descriptionFolder.assertContainsText(newValue);
}

export function assertNodeExistsInCustomWorkspaceProject(
  customReportWorkspacePage: CustomReportWorkspacePage,
  nameOriginNode: string,
  nameNode: string | RegExp,
) {
  const customReportTree = customReportWorkspacePage.tree;
  customReportTree.findRowId('NAME', nameOriginNode).then((idProject) => {
    customReportTree.openNodeIfClosed(idProject);
  });
  customReportTree.findRowId('NAME', nameNode).then((idRequirement) => {
    customReportTree.assertNodeExist(idRequirement);
  });
}

export function assertNodeNotExistsInCustomWorkspaceProject(
  customReportWorkspacePage: CustomReportWorkspacePage,
  nameOriginNode: string,
  nameNode: string,
) {
  const customReportTree = customReportWorkspacePage.tree;
  customReportTree.findRowId('NAME', nameOriginNode).then((idProject) => {
    customReportTree.openNodeIfClosed(idProject);
  });
  customReportTree.findRowId('NAME', nameNode).then((idRequirement) => {
    customReportTree.assertNodeNotExist(idRequirement);
  });
}
export function selectPickNodeInCustomReportWorkspace(
  customReportWorkspacePage: CustomReportWorkspacePage,
  nameNode: string,
) {
  const customReportTree = customReportWorkspacePage.tree;
  customReportTree.findRowId('NAME', nameNode).then((idProject) => {
    customReportTree.pickNode(idProject);
  });
}
export function modifyChartDefinition(
  modifyChartViewPage: ModifyChartViewPage,
  indexScope: number,
  indexAttribute: number,
  indexAttributeFilter: number,
) {
  const pickerDialogElement = new MilestonePickerDialogElement();
  modifyChartViewPage.assertExists();
  modifyChartViewPage.selectScopeCriterion(indexScope);
  pickerDialogElement.confirm();
  modifyChartViewPage.axisSelector.openAttributeSelector();
  modifyChartViewPage.axisSelector.addAttributeToChart(indexAttribute);
  modifyChartViewPage.clickFilterButton();
  modifyChartViewPage.filterSelector.addAttributeToChart(indexAttributeFilter);
  modifyChartViewPage.filterSelector.inputFilterAndClickUpdateButtonInFilter('1');
  modifyChartViewPage.clickOnModifyChartButton();
}
export function dragReportToDashboard(
  customReportWorkspacePage: CustomReportWorkspacePage,
  dashboardViewPage: DashboardViewPage,
  reportName: string,
) {
  customReportWorkspacePage.tree.findRowId('NAME', reportName).then((requirementId) => {
    customReportWorkspacePage.tree.beginDragAndDrop(requirementId);
  });
  dashboardViewPage.dropReportIntoEmptyZone();
}

export function dragChartToDashboard(
  customReportWorkspacePage: CustomReportWorkspacePage,
  dashboardViewPage: DashboardViewPage,
  chartName: string,
) {
  customReportWorkspacePage.tree.findRowId('NAME', chartName).then((requirementId) => {
    customReportWorkspacePage.tree.beginDragAndDrop(requirementId);
  });
  dashboardViewPage.dropChart();
}

export function dragChartToDashboardEmpty(
  customReportWorkspacePage: CustomReportWorkspacePage,
  dashboardViewPage: DashboardViewPage,
  chartName: string,
) {
  customReportWorkspacePage.tree.findRowId('NAME', chartName).then((requirementId) => {
    customReportWorkspacePage.tree.beginDragAndDrop(requirementId);
  });
  dashboardViewPage.dropChartIntoEmptyZone();
}

export function modifyReportDefinitionWithValues(
  modifyReportDefinitionViewPage: ModifyReportDefinitionViewPage,
  newName: string,
  description: string,
) {
  const nameField = modifyReportDefinitionViewPage.textFieldNameElement;
  const descriptionField = modifyReportDefinitionViewPage.richTextField;
  nameField.assertExists();
  nameField.fill(newName);
  descriptionField.assertIsReady();
  descriptionField.fill(description);
  const perimeterReportOption = new ReportWidget('testcasesSelectionMode');
  perimeterReportOption.changeSelectOption('testcasesIds');
  modifyReportDefinitionViewPage.clickTestCaseTreePickerInForm();
  modifyReportDefinitionViewPage.selectNodesInTestCaseTree(['Project', 'Classique']);
  const impressionOption = new ReportWidget('reportOptions');
  impressionOption.changeSelectOption('chk-printparameters');
  modifyReportDefinitionViewPage.clickActionButton('save-report');
}

export function dragEntityIntoAnotherEntityInCustomReportWorkspace(
  customReportWorkspacePage: CustomReportWorkspacePage,
  originNodeName: string,
  nodeToDrag: string,
  nodeToDrop: string,
  objectContainingDrag: string,
  interProject: boolean,
) {
  selectNodeInCustomReportWorkspace(customReportWorkspacePage, originNodeName, nodeToDrag);
  customReportWorkspacePage.tree.findRowId('NAME', nodeToDrag).then((idNodeToDrag) => {
    customReportWorkspacePage.tree.findRowId('NAME', nodeToDrop).then((idNodeToDrop) => {
      customReportWorkspacePage.tree
        .findRowId('NAME', objectContainingDrag)
        .then((idObjectWhereDrag) => {
          customReportWorkspacePage.tree.dragAndDropObjectInObjectInWorkspace(
            idNodeToDrag,
            idObjectWhereDrag,
            idNodeToDrop,
            interProject,
          );
        });
    });
  });
}

export function modifyCustomExportNamePerimeterAndAddAnAttribute(
  customExportViewPage: CustomExportViewPage,
  name: string,
  nodesName: string[],
  attributeName: string,
) {
  const modifyCustomExportViewPage = customExportViewPage.goToModifyCustomExportViewPage();
  modifyCustomExportViewPage.assertExists();
  modifyCustomExportViewPage.customExportNameField.clearContent();
  modifyCustomExportViewPage.customExportNameField.fill(name);
  modifyCustomExportViewPage.modifyScopeExport(nodesName);
  modifyCustomExportViewPage.addAttributeNewAttributeToExport(attributeName);
  modifyCustomExportViewPage.clickModifyButton();
}

export function configureAxisAttributeSelection(
  axisSelectorElement: AxisSelectorElement,
  idAttribute: number,
  optionAttribute: string,
  filterAttribute?: string,
) {
  axisSelectorElement.openAttributeSelector();
  axisSelectorElement.addAttributeToChart(idAttribute, filterAttribute);
  axisSelectorElement.checkOption(optionAttribute);
}

export function assertAxesField(
  chartDefinitionViewPage: ChartDefinitionViewPage,
  type: string,
  icon: string,
  label: string,
  color: WorkspaceColor,
) {
  chartDefinitionViewPage.assertIconDisplayedInAxesField(type, icon);
  chartDefinitionViewPage.assertLabelsDisplayedInAxesField(type, label);
  chartDefinitionViewPage.assertColorsDisplayedInAxesField(type, color);
}

export function accessAddChartPageFromCustomReportWorkspace(
  customReportWorkspacePage: CustomReportWorkspacePage,
  nameNode: string,
) {
  selectPickNodeInCustomReportWorkspace(customReportWorkspacePage, nameNode);
  customReportWorkspacePage.treeMenu.assertCreateChartButtonIsEnabled();
  const createChart = customReportWorkspacePage.treeMenu.openCreateChart();
  createChart.assertExists();
}

export function selectTypeChartInCreateChartPage(createChart: CreateChartViewPage, type: string) {
  createChart.chartTypeSelector.selectValue(type);
  createChart.chartTypeSelector.checkSelectedOption(type);
}

export function renameAndSaveChart(
  createChart: CreateChartViewPage,
  chartDefinitionViewPage: ChartDefinitionViewPage,
  titleChart: string,
) {
  createChart.renameChart(titleChart);
  createChart.saveChart();
  chartDefinitionViewPage.assertExists();
  chartDefinitionViewPage.assertNameContains(titleChart);
}

export function testPointX2YHover(
  chartViewPage: ChartDefinitionViewPage | CreateChartViewPage,
  x: number,
  y: number,
  pointSelector: string,
  expectedLegend: string,
  expectedCount: string,
) {
  chartViewPage.hoverOverX2YSubplot(x, y);
  chartViewPage.assertLegendOnPointHover(pointSelector, expectedLegend);
  chartViewPage.assertElementCountOnPointHover(pointSelector, expectedCount);
}

export function testPointXYHover(
  chartViewPage: ChartDefinitionViewPage | CreateChartViewPage,
  x: number,
  y: number,
  pointSelector: string,
  expectedLegend: string,
  expectedCount: string,
) {
  chartViewPage.hoverOverXYSubplot(x, y);
  chartViewPage.assertLegendOnPointHover(pointSelector, expectedLegend);
  chartViewPage.assertElementCountOnPointHover(pointSelector, expectedCount);
}
