import { ListPanelElement } from '../../page-objects/elements/filters/list-panel.element';
import { TestSuiteTestPlanPage } from '../../page-objects/pages/campaign-workspace/test-suite/test-suite-test-plan.page';
import { IterationTestPlanPage } from '../../page-objects/pages/campaign-workspace/iteration/iteration-test-plan.page';
import { GridElement } from '../../page-objects/elements/grid/grid.element';
import { GridColumnId } from '../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { CampaignFolderViewPage } from '../../page-objects/pages/campaign-workspace/campaign-folder/campaign-folder.page';
import { CampaignWorkspacePage } from '../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { CampaignViewPage } from '../../page-objects/pages/campaign-workspace/campaign/campaign-view.page';
import { IterationViewPage } from '../../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import { TestSuiteViewPage } from '../../page-objects/pages/campaign-workspace/test-suite/test-suite-view.page';
import { IterationViewPlanningPage } from '../../page-objects/pages/campaign-workspace/iteration/iteration-view-planning.page';
import { CampaignViewPlanningPage } from '../../page-objects/pages/campaign-workspace/campaign/campaign-view-planning.page';
import { CheckBoxElement } from '../../page-objects/elements/forms/check-box.element';
import { MenuItemElement } from '../../utils/menu.element';
import { ExecutionHistoryDialog } from '../../page-objects/pages/campaign-workspace/dialogs/itpi-execution-history-dialog.element';
import { selectByDataIcon, selectByDataTestButtonId } from '../../utils/basic-selectors';
import { GroupedMultiListElement } from '../../page-objects/elements/filters/grouped-multi-list.element';
import { ItpiMultiEditDialogElement } from '../../page-objects/pages/campaign-workspace/dialogs/itpi-multi-edit-dialog.element';
import { TestSuiteTpiMultiEditDialogElement } from '../../page-objects/pages/campaign-workspace/dialogs/test-suite-tpi-multi-edit-dialog.element';
import { CreateSuiteDialog } from '../../page-objects/pages/campaign-workspace/dialogs/create-suite-dialog.element';
import { ItpiSearchPage } from '../../page-objects/pages/campaign-workspace/search/itpi-search-page';
import { ExecutionPage } from '../../page-objects/pages/execution/execution-page';
import { MultiEditSearchDialog } from '../../page-objects/pages/campaign-workspace/dialogs/multi-edit-search.dialog';
import { TreeToolbarElement } from '../../page-objects/elements/workspace-common/tree-toolbar.element';
import { KeyboardShortcuts } from '../../page-objects/elements/workspace-common/keyboard-shortcuts';
import { AddTestPlanDialogElement } from '../../page-objects/pages/campaign-workspace/dialogs/add-test-plan-dialog.element';
import { RemoveItpiDialogElement } from '../../page-objects/pages/campaign-workspace/dialogs/remove-itpi-dialog.element';
import { DeleteTreeNodesDialogElement } from '../../page-objects/pages/test-case-workspace/dialogs/delete-tree-nodes-dialog.element';
import { MassDeleteCtpiDialogElement } from '../../page-objects/pages/campaign-workspace/dialogs/mass-delete-ctpi-dialog.element';
import { MassDeleteItpiDialogElement } from '../../page-objects/pages/campaign-workspace/dialogs/mass-delete-itpi-dialog.element';
import { RemoveCtpiDialogElement } from '../../page-objects/pages/campaign-workspace/dialogs/remove-ctpi-dialog.element';
import { RemoveOrDetachTestSuiteItpiDialogElement } from '../../page-objects/pages/campaign-workspace/dialogs/remove-or-detach-test-suite-itpi-dialog.element';
import { createRegExpForDisabledUserFullName } from '../../utils/end-to-end/end-to-end-helper';
import { ExecutionStatusKeys } from '../../../../projects/sqtm-core/src/lib/model/level-enums/level-enum';
import { SimpleDeleteConfirmDialogElement } from '../../page-objects/elements/dialog/simple-delete-confirm-dialog.element';
import { ConfirmDeleteAllExecutionElement } from '../../page-objects/pages/campaign-workspace/dialogs/confirm-delete-all-execution.element';
import { ExecutionRunnerStepPage } from '../../page-objects/pages/execution/execution-runner-step-page';
import { ExecutionRunnerProloguePage } from '../../page-objects/pages/execution/execution-runner-prologue-page';
import { ExecutionScenarioPanelElement } from '../../page-objects/pages/execution/panels/execution-scenario-panel.element';
import { SessionOverviewPage } from '../../page-objects/pages/session-overview/session-overview.page';
import { SessionNoteType } from '../../page-objects/elements/execution-page/session-note-container.element';
import { TestPlanPage } from '../../page-objects/pages/campaign-workspace/test-plan-page';
import { NavBarElement } from '../../page-objects/elements/nav-bar/nav-bar.element';

type StatusItemsTest =
  | 'À exécuter'
  | 'En cours'
  | 'Succès'
  | 'Échec'
  | 'Bloqué'
  | 'Arbitré'
  | 'Non testable';
type ViewPage = 'campaign' | 'iteration' | 'test-suite';
type ObjectCampaignWorkspace =
  | 'Campaign'
  | 'Sprint'
  | 'Sprint-Group'
  | 'Campaign-Folder'
  | 'Iteration'
  | 'Test-Suite';
type ColumNames =
  | 'projectName'
  | 'inferredExecutionMode'
  | 'testCaseReference'
  | 'testCaseName'
  | 'importance'
  | 'datasetName'
  | 'executionStatus'
  | 'assigneeFullName'
  | 'lastExecutedOn';

export interface UserProperties {
  userActive: boolean;
  userFullName: string;
  userLogin: string;
}

export function buildUserProperties(
  userActive: boolean,
  userFullName: string,
  userLogin: string,
): UserProperties {
  return { userActive, userFullName, userLogin };
}

export interface TestPlanProperties {
  contentOpen: string;
  contentSelect: string;
  testCaseAssigned: string;
  testCaseNotAssigned: string;
}

export function buildTestPlanProperties(
  contentOpen: string,
  contentSelect: string,
  testCaseAssigned: string,
  testCaseNotAssigned: string,
): TestPlanProperties {
  return { contentOpen, contentSelect, testCaseAssigned, testCaseNotAssigned };
}

export function clickAndCheckTestCaseAssignee(
  testPlanPage: TestPlanPage,
  testCaseName: string,
  panelList: ListPanelElement,
  userProperties: UserProperties,
) {
  openAssigneeListForTestPlanItem(testPlanPage.testPlan, testCaseName);
  checkAssigneeContent(testPlanPage, testCaseName, userProperties);
  panelList.close();
}

export function checkTestCaseContentIfUserNotAssignedAndInactive(
  testPlanPage: TestPlanPage,
  testCaseName: string,
  panelList: ListPanelElement,
  userProperties: UserProperties,
) {
  openAssigneeListForTestPlanItem(testPlanPage.testPlan, testCaseName);
  panelList.assertExists();
  checkItemNotExistAndCloseList(panelList, userProperties);
}

function openAssigneeListForTestPlanItem(testPlan: GridElement, testCaseName: string) {
  testPlan.findRowId('testCaseName', testCaseName).then((rowId) => {
    testPlan.getCell(rowId, GridColumnId.assigneeFullName).selectRenderer().click();
  });
}

function checkAssigneeContent(
  testPlanPage: TestPlanPage,
  testCaseName: string,
  userProperties: UserProperties,
) {
  testPlanPage.testPlan.findRowId('testCaseName', testCaseName).then((rowId) => {
    let userDisplayed: string | RegExp;
    if (!userProperties.userActive) {
      userDisplayed = createRegExpForDisabledUserFullName(userProperties.userFullName);
    } else {
      userDisplayed = userProperties.userFullName + ' (' + userProperties.userLogin + ')';
    }
    testPlanPage.testPlan
      .getCell(rowId, GridColumnId.assigneeFullName)
      .selectRenderer()
      .assertContainTextOrRegExp(userDisplayed);
  });
}

function checkItemNotExistAndCloseList(
  multiList: ListPanelElement,
  userProperties: UserProperties,
) {
  multiList.assertItemNotExist(userProperties.userFullName + ' (' + userProperties.userLogin + ')');
  cy.clickOnOverlayBackdrop();
  multiList.close();
  multiList.assertNotExist();
}

export function selectNodeProjectInCampaignWorkspace(
  campaignWorkspacePage: CampaignWorkspacePage,
  nameProject: string,
) {
  campaignWorkspacePage.assertExists();
  campaignWorkspacePage.tree.findRowId('NAME', nameProject).then((requirementId) => {
    campaignWorkspacePage.tree.selectRow(requirementId, 'NAME');
  });
}

export function selectNodeInCampaignWorkspace(
  campaignWorkspace: CampaignWorkspacePage,
  nameOriginNode: string,
  nameNode: string,
) {
  const campaignTree = campaignWorkspace.tree;
  campaignWorkspace.assertExists();
  campaignTree.findRowId('NAME', nameOriginNode).then((idProject) => {
    campaignTree.openNodeIfClosed(idProject);
  });
  campaignTree.findRowId('NAME', nameNode).then((idRequirement) => {
    campaignTree.selectNode(idRequirement);
  });
}

export function selectNodeInNodeInCampaignWorkspace(
  campaignWorkspace: CampaignWorkspacePage,
  nodeNames: string[],
) {
  for (let i = 0; i < nodeNames.length - 1; i++) {
    const openNodeName = nodeNames[i];
    const selectNodeName = nodeNames[i + 1];
    selectNodeInCampaignWorkspace(campaignWorkspace, openNodeName, selectNodeName);
  }
}

function selectViewPageOfEntityCampaign(objectCampaign: ObjectCampaignWorkspace) {
  let viewObjectPage:
    | CampaignViewPage
    | IterationViewPage
    | TestSuiteViewPage
    | CampaignFolderViewPage = null;

  switch (objectCampaign) {
    case 'Campaign': {
      viewObjectPage = new CampaignViewPage();
      break;
    }
    case 'Campaign-Folder': {
      viewObjectPage = new CampaignFolderViewPage();
      break;
    }
    case 'Iteration': {
      viewObjectPage = new IterationViewPage();
      break;
    }
    case 'Test-Suite': {
      viewObjectPage = new TestSuiteViewPage();
      break;
    }
    default:
      throw new Error(`Object kind "${objectCampaign}" is not handled`);
  }
  return viewObjectPage;
}

export function renameCampaignIterationCampaignFolderAndTestSuiteInCampaignWorkspace(
  objectCampaign: ObjectCampaignWorkspace,
  newValue: string,
) {
  const viewObjectPage = selectViewPageOfEntityCampaign(objectCampaign);
  viewObjectPage.assertExists();
  viewObjectPage.nameTextField.setValue(newValue);
  viewObjectPage.nameTextField.clickOnConfirmButton();
  viewObjectPage.nameTextField.assertContainsText(newValue);
}

export function modifyReferenceCampaignIterationCampaignFolderAndTestSuiteInCampaignWorkspace(
  viewPage: ViewPage,
  newValue: string,
) {
  let viewEntityPage: CampaignViewPage | IterationViewPage | TestSuiteViewPage = null;

  switch (viewPage) {
    case 'campaign': {
      viewEntityPage = new CampaignViewPage();
      break;
    }
    case 'iteration': {
      viewEntityPage = new IterationViewPage();
      break;
    }
    case 'test-suite': {
      viewEntityPage = new TestSuiteViewPage();
      break;
    }
    default:
      throw new Error(`Object kind "${viewPage}" is not handled`);
  }

  viewEntityPage.referenceField.setValue(newValue);
  viewEntityPage.referenceField.clickOnConfirmButton();
  viewEntityPage.referenceField.assertContainsText(newValue);
}

export function editDescriptionCampaignWorkspace(
  objectCampaign: ObjectCampaignWorkspace,
  newValue: string,
) {
  const viewObjectPage = selectViewPageOfEntityCampaign(objectCampaign);
  viewObjectPage.assertExists();
  viewObjectPage.descriptionRichField.click();
  viewObjectPage.descriptionRichField.setValue(newValue);
  viewObjectPage.descriptionRichField.confirm();
  viewObjectPage.descriptionRichField.assertContainsText(newValue);
}

export function createObjectInCampaignWorkspace(
  campaignWorkspacePage: CampaignWorkspacePage,
  isAddingInProject: boolean,
  projectName: string,
  item: ObjectCampaignWorkspace,
  newEntityName: string,
  containerNodeName?: string,
) {
  if (isAddingInProject) {
    selectNodeProjectInCampaignWorkspace(campaignWorkspacePage, projectName);
  } else {
    selectNodeInCampaignWorkspace(campaignWorkspacePage, projectName, containerNodeName);
  }

  switch (item) {
    case 'Campaign': {
      const campaignDialog = campaignWorkspacePage.openCreateCampaign();
      campaignDialog.assertExists();
      campaignDialog.assertIsReady();
      campaignDialog.fillName(newEntityName);
      campaignDialog.clickOnAddButton();
      campaignDialog.assertNotExist();
      break;
    }
    case 'Sprint': {
      const sprintDialog = campaignWorkspacePage.openCreateSprint();
      sprintDialog.assertExists();
      sprintDialog.assertIsReady();
      sprintDialog.fillName(newEntityName);
      sprintDialog.clickOnAddButton();
      sprintDialog.assertNotExist();
      break;
    }
    case 'Sprint-Group': {
      const sprintGroupDialog = campaignWorkspacePage.openCreateSprintGroup();
      sprintGroupDialog.assertExists();
      sprintGroupDialog.assertIsReady();
      sprintGroupDialog.fillName(newEntityName);
      sprintGroupDialog.clickOnAddButton();
      sprintGroupDialog.assertNotExist();
      break;
    }
    case 'Iteration': {
      const iterationDialog = campaignWorkspacePage.openCreateIteration();
      iterationDialog.assertExists();
      iterationDialog.assertIsReady();
      iterationDialog.fillName(newEntityName);
      iterationDialog.clickOnAddButton();
      iterationDialog.assertNotExist();
      break;
    }
    case 'Test-Suite': {
      const testSuiteDialog = campaignWorkspacePage.openCreateTestSuite();
      testSuiteDialog.assertExists();
      testSuiteDialog.assertIsReady();
      testSuiteDialog.fillName(newEntityName);
      testSuiteDialog.clickOnAddButton();
      testSuiteDialog.assertNotExist();
      break;
    }
    case 'Campaign-Folder': {
      const folderDialog = campaignWorkspacePage.openCreateFolder();
      folderDialog.assertExists();
      folderDialog.assertIsReady();
      folderDialog.fillName(newEntityName);
      folderDialog.clickOnAddButton();
      folderDialog.assertNotExist();
      break;
    }
    default:
      throw new Error(`Item kind "${item}" is not handled`);
  }
}

function clickPlanningAnchorLinkInCampaignOrIteration(
  viewPage: 'campaign' | 'iteration',
): CampaignViewPlanningPage | IterationViewPlanningPage {
  let planningPage: CampaignViewPlanningPage | IterationViewPlanningPage = null;

  switch (viewPage) {
    case 'campaign': {
      const campaignViewPage = new CampaignViewPage();
      planningPage = campaignViewPage.clickPlanningAnchorLink();
      break;
    }
    case 'iteration': {
      const iterationViewPage = new IterationViewPage();
      planningPage = iterationViewPage.clickPlanningAnchorLink();
      break;
    }
    default:
      throw new Error(
        `"${viewPage}" is not iteration or campaign. This method only allows those values : campaign and iteration`,
      );
  }
  return planningPage;
}

export function editFieldInPlanningViewWithCalendar(
  viewPage: 'campaign' | 'iteration',
  fieldName: 'scheduledStartDate' | 'actualStartDate' | 'scheduledEndDate' | 'actualEndDate',
) {
  const planningPage = clickPlanningAnchorLinkInCampaignOrIteration(viewPage);

  switch (fieldName) {
    case 'scheduledStartDate':
      planningPage.scheduledStartDateField.enableEditMode();
      planningPage.scheduledStartDateField.setToTodayAndConfirm();
      break;
    case 'actualStartDate':
      planningPage.actualStartDateField.enableEditMode();
      planningPage.actualStartDateField.setToTodayAndConfirm();
      break;
    case 'scheduledEndDate':
      planningPage.scheduledEndDateField.enableEditMode();
      planningPage.scheduledEndDateField.setToTodayAndConfirm();
      break;
    case 'actualEndDate':
      planningPage.actualEndDateField.enableEditMode();
      planningPage.actualEndDateField.setToTodayAndConfirm();
      break;
    default:
      throw new Error(`"${fieldName}" is not a field present in the planning view`);
  }
}

export function editFieldInPlanningView(
  viewPage: 'campaign' | 'iteration',
  fieldName: 'scheduledStartDate' | 'actualStartDate' | 'scheduledEndDate' | 'actualEndDate',
  valueDate: string,
) {
  const planningPage = clickPlanningAnchorLinkInCampaignOrIteration(viewPage);

  switch (fieldName) {
    case 'scheduledStartDate':
      planningPage.scheduledStartDateField.enableEditMode();
      planningPage.scheduledStartDateField.rootElement.type(valueDate);
      planningPage.scheduledStartDateField.rootElement.type('{enter}');
      planningPage.scheduledStartDateField.assertContainsText(valueDate);
      break;
    case 'actualStartDate':
      planningPage.actualStartDateField.enableEditMode();
      planningPage.actualStartDateField.rootElement.type(valueDate);
      planningPage.actualStartDateField.rootElement.type('{enter}');
      planningPage.actualStartDateField.assertContainsText(valueDate);
      break;
    case 'scheduledEndDate':
      planningPage.scheduledEndDateField.enableEditMode();
      planningPage.scheduledEndDateField.rootElement.type(valueDate);
      planningPage.scheduledEndDateField.rootElement.type('{enter}');
      planningPage.scheduledEndDateField.assertContainsText(valueDate);
      break;
    case 'actualEndDate':
      planningPage.actualEndDateField.enableEditMode();
      planningPage.actualEndDateField.rootElement.type(valueDate);
      planningPage.actualEndDateField.rootElement.type('{enter}');
      planningPage.actualEndDateField.assertContainsText(valueDate);
      break;
    default:
      throw new Error(`"${fieldName}" is not a field present in the planning view`);
  }
}

export function showTestPlanInCampaignOrIterationOrTestSuite(viewPage: ViewPage): TestPlanPage {
  let testPlanPage: TestPlanPage = null;

  switch (viewPage) {
    case 'campaign': {
      const campaignViewPage = new CampaignViewPage();
      testPlanPage = campaignViewPage.showTestPlan();
      break;
    }
    case 'iteration': {
      const iterationViewPage = new IterationViewPage();
      testPlanPage = iterationViewPage.showTestPlan();
      break;
    }
    case 'test-suite': {
      const testSuiteViewPage = new TestSuiteViewPage();
      testPlanPage = testSuiteViewPage.showTestPlan();
      break;
    }
    default:
      throw new Error(
        `"${viewPage}" is not iteration or test-suite or campaign. This method only allows those values : campaign, iteration and test-suite`,
      );
  }
  return testPlanPage;
}

export function assertItemsInTestPlanExist(
  viewPage: ViewPage,
  itemNames: string[],
  itemNumber: number,
) {
  const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite(viewPage);
  itemNames.forEach((nameTestCase) => {
    testPlanPage.testPlan.findRowId('testCaseName', nameTestCase).then((idTestCase) => {
      testPlanPage.testPlan.assertRowExist(idTestCase);
    });
  });
  testPlanPage.testPlan.assertRowCount(itemNumber);
}

export function assignUserToItemInTestPlan(
  viewPage: ViewPage,
  nameTestCase: string,
  idItem: string,
  nameUser: string,
) {
  const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite(viewPage);

  testPlanPage.testPlan.findRowId('testCaseName', nameTestCase).then((idTestCase) => {
    testPlanPage.testPlan
      .getCell(idTestCase, 'assigneeFullName')
      .selectRenderer()
      .changeValue(idItem);

    testPlanPage.testPlan.waitForRefresh();

    testPlanPage.testPlan
      .getCell(idTestCase, 'assigneeFullName')
      .selectRenderer()
      .assertContainText(nameUser);
  });
}

export function reorderItemsInTestPlan(
  type: 'campaign' | 'iteration',
  idItemToMove: string,
  idItemToDrop: string,
) {
  const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite(type);
  testPlanPage.moveItemWithoutServerResponse(idItemToMove, idItemToDrop);
}

export function toggleAutoCheckboxInPlanningFields(
  fieldName: 'actualStartAuto' | 'actualEndAuto',
  stateCheckbox: boolean,
) {
  const checkboxAutoEndDate = new CheckBoxElement(fieldName);
  checkboxAutoEndDate.click();
  checkboxAutoEndDate.toggleState(stateCheckbox);
}

export function editAttributeInMassTestPlanPage(
  viewPage: 'iteration' | 'test-suite',
  nameTestCase: string,
  assigneeName: string,
  status: StatusItemsTest,
) {
  let massEditDialog: ItpiMultiEditDialogElement | TestSuiteTpiMultiEditDialogElement = null;
  let testPlanPage: TestSuiteTestPlanPage | IterationTestPlanPage = null;

  switch (viewPage) {
    case 'iteration': {
      massEditDialog = new ItpiMultiEditDialogElement();
      testPlanPage = new IterationViewPage().showTestPlan();
      break;
    }
    case 'test-suite': {
      massEditDialog = new TestSuiteTpiMultiEditDialogElement();
      testPlanPage = new TestSuiteViewPage().showTestPlan();
      break;
    }
    default:
      throw new Error(
        `"${viewPage}" is not iteration or test-suite. This method only allows those values : iteration and test-suite`,
      );
  }
  testPlanPage.testPlan.findRowId('testCaseName', nameTestCase).then((idTestCase) => {
    testPlanPage.testPlan.selectRow(idTestCase, '#', 'leftViewport');
  });
  cy.get(selectByDataTestButtonId('mass-edit')).click({ force: true });
  massEditDialog.assertExists();
  massEditDialog.toggleAssigneeEdition();
  massEditDialog.selectAssignee(assigneeName);
  massEditDialog.toggleStatusEdition();
  massEditDialog.selectStatus(status);
  massEditDialog.confirm([]);
}

export function selectJDDInTestPlanPage(viewPage: ViewPage, testCaseName: string, item: string) {
  const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite(viewPage);
  const listItem = new ListPanelElement();

  testPlanPage.testPlan.findRowId('testCaseName', testCaseName).then((idTestCase) => {
    testPlanPage.testPlan.scrollPosition('center', 'mainViewport');
    testPlanPage.testPlan.toggleRow(idTestCase, 'dataset-cell');
    listItem.findItem(item).click();
  });
}

export function deleteExecutionInTestPlanPage(viewPage: ViewPage, testCaseName: string) {
  const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite(viewPage);
  const menuExecution = new MenuItemElement('', 'play-exec');
  const executionHistoryDialog = new ExecutionHistoryDialog();

  testPlanPage.testPlan.findRowId('testCaseName', testCaseName).then((idTestCase) => {
    testPlanPage.testPlan
      .getCell(idTestCase, 'startExecution', 'rightViewport')
      .iconRenderer()
      .click();
    menuExecution.selectLinkElement('show-execution-history');
  });
  executionHistoryDialog.assertExists();
  executionHistoryDialog.gridElement
    .findRowId('executionName', testCaseName)
    .then((idExecution) => {
      executionHistoryDialog.deleteSelectedExecution(idExecution, viewPage);
    });
  executionHistoryDialog.gridElement.assertGridIsEmpty();
  executionHistoryDialog.close();
}

export function assertExecutionPageExistForAnExecution(
  viewPage: ViewPage,
  testCaseName: string,
  numberExecution: string,
) {
  const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite(viewPage);
  const menuExecution = new MenuItemElement('', 'play-exec');
  const executionHistoryDialog = new ExecutionHistoryDialog();
  const executionPage = new ExecutionPage();

  testPlanPage.testPlan.findRowId('testCaseName', testCaseName).then((idTestCase) => {
    testPlanPage.testPlan
      .getCell(idTestCase, 'startExecution', 'rightViewport')
      .find('sqtm-app-start-execution')
      .click();
    menuExecution.selectLinkElement('show-execution-history');
  });
  executionHistoryDialog.assertExists();
  executionHistoryDialog.gridElement
    .findRowId('executionOrder', numberExecution)
    .then((idExecution) => {
      executionHistoryDialog.gridElement
        .getCell(idExecution, 'executionOrder')
        .linkRenderer()
        .findCellLink()
        .click();
    });
  executionPage.assertExists();
}

export function filterColumn(viewPage: ViewPage, column: ColumNames) {
  const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite(viewPage);
  testPlanPage.testPlan
    .getHeaderRow()
    .cell(column)
    .findCell()
    .find(selectByDataIcon('filter'))
    .click();
}

export function filterColumnMode(
  viewPage: 'campaign' | 'iteration' | 'test-suite',
  mode: 'Automatique' | 'Manuel' | 'Exploratoire' | 'Non défini',
) {
  const listItem = new GroupedMultiListElement();

  filterColumn(viewPage, 'inferredExecutionMode');
  listItem.findItem(mode).click();
}

export function changeStatusTestCaseInTestCasePlanPage(
  viewPage: ViewPage,
  nameTestCase: string,
  status: StatusItemsTest,
) {
  const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite(viewPage);
  const listItem = new ListPanelElement();

  testPlanPage.testPlan.findRowId('testCaseName', nameTestCase).then((idTestCase) => {
    testPlanPage.testPlan.getCell(idTestCase, 'execution-status-cell').findCell().click();
    listItem.findItem(status).click();
  });
}

export function createTestSuiteFromPagePlanInIteration(
  testPlanPage: IterationTestPlanPage,
  nameTestCase: string,
  nameTestSuite: string,
) {
  const dialogCreateTestSuite = new CreateSuiteDialog();

  testPlanPage.testPlan.findRowId('testCaseName', nameTestCase).then((idTestCase) => {
    testPlanPage.testPlan.selectRow(idTestCase, '#', 'leftViewport');
  });
  testPlanPage.openCreateTestSuiteDialog();
  dialogCreateTestSuite.fillName(nameTestSuite);
  dialogCreateTestSuite.clickOnAddButton();
}

export function filterColumnsAlphabeticalOrder(
  testPlanPage: IterationTestPlanPage,
  expectedNames: string[],
) {
  testPlanPage.testPlan
    .getHeaderRow()
    .cell('testCaseName')
    .findCell()
    .contains('Cas de test')
    .click('center');
  testPlanPage.testPlan.assertSpinnerIsNotPresent();
  testPlanPage.testPlan.assertGridContentOrder('testCaseName', expectedNames, 'mainViewport');
}

export function createIterationFromItpiInSearchPage(
  campaignWorkspace: CampaignWorkspacePage,
  nameItpi: string,
  namesOfNodesToIteration: string[],
) {
  const itpiSearchPage = campaignWorkspace.goToSearchItpiPage();
  const itpiAddDialog = new AddTestPlanDialogElement();

  itpiSearchPage.grid.findRowId('label', nameItpi).then((idItpi) => {
    itpiSearchPage.grid.getCell(idItpi, '#', 'leftViewport').selectRenderer().click();
  });
  itpiSearchPage.addButton.click();
  itpiAddDialog.assertExists();
  itpiAddDialog.selectIterationToLinkToItpi(namesOfNodesToIteration);
  itpiAddDialog.clickButtonAddIteration();
}

export function searchItemInSearchPageFromCampaignWorkspace(
  campaignWorkspace: CampaignWorkspacePage,
  projectName: string,
  isProjectSelected: boolean,
  nameItem: string,
) {
  const itpiSearchPage = campaignWorkspace.goToSearchItpiPage();

  itpiSearchPage.assertExists();
  if (isProjectSelected === true) {
    itpiSearchPage.assertProjectIsSelectedInWidgetPerimeter(projectName);
  } else {
    itpiSearchPage.selectProjectInWidgetPerimeter(projectName);
  }
  itpiSearchPage.selectItemToAppearAloneInPageWithNameCriteria(nameItem);
}

export function modifyAttributesInItemSearchPage(
  itpiSearchPage: ItpiSearchPage,
  nameItems: string[],
  status: StatusItemsTest,
) {
  const multiEditDialog = new MultiEditSearchDialog();

  nameItems.forEach((nameItem) => {
    itpiSearchPage.grid.findRowId('label', nameItem).then((idItem) => {
      itpiSearchPage.grid.getCell(idItem, '#', 'leftViewport').selectRenderer().click();
      itpiSearchPage.grid.assertSpinnerIsNotPresent();
      itpiSearchPage.massEditButton.clickWithoutSpan();
      multiEditDialog.getOptionalField('executionStatus').check();
      multiEditDialog.getOptionalField('executionStatus').selectValue(status);
      multiEditDialog.clickButton('confirm');
      multiEditDialog.assertNotExist();
    });
  });
}

export function deleteExecutionInExecutionPageWithTopIcon(
  executionPage: ExecutionPage,
  executionNumber: string,
) {
  const historyExecution = executionPage.clickHistoryAnchorLink();
  historyExecution.deleteExecutionWithTopIcon(executionNumber);
  executionPage.assertExists();
}

export function deleteExecutionInExecutionPageWithIconEndOfLine(
  executionPage: ExecutionPage,
  executionNumber: string,
) {
  const historyExecution = executionPage.clickHistoryAnchorLink();
  historyExecution.deleteExecutionWithEndOfLineIcon(executionNumber);
  executionPage.assertExists();
}

export function copyAndPasteEntityInCampaignWorkspaceWithTreeToolBarButton(
  campaignWorkspace: CampaignWorkspacePage,
  nodeNames: string[],
  nameTargetNode: string | RegExp,
) {
  const treeToolbarElement = new TreeToolbarElement('campaign-toolbar');

  selectNodeInNodeInCampaignWorkspace(campaignWorkspace, nodeNames);
  treeToolbarElement.getButton('copy-button').clickWithoutSpan();
  campaignWorkspace.tree.findRowId('NAME', nameTargetNode).then((entityId) => {
    campaignWorkspace.tree.pickNode(entityId);
  });
  treeToolbarElement.getButton('paste-button').clickWithoutSpan();
}

export function copyAndPasteEntityInCampaignWorkspaceWithShortcuts(
  campaignWorkspace: CampaignWorkspacePage,
  nodeNames: string[],
  nameTargetNode: string,
) {
  const keyboardShortcut = new KeyboardShortcuts();

  selectNodeInNodeInCampaignWorkspace(campaignWorkspace, nodeNames);
  keyboardShortcut.performCopy();
  campaignWorkspace.tree.findRowId('NAME', nameTargetNode).then((entityId) => {
    campaignWorkspace.tree.pickNode(entityId);
  });
  keyboardShortcut.performPaste();
}

export function copyIterationToAnotherProjectInCampaignWorkspaceWithToolBar(
  campaignWorkspace: CampaignWorkspacePage,
  nodeNames: string[],
  nameTargetNode: string,
  nameProject: string,
) {
  const treeToolbarElement = new TreeToolbarElement('campaign-toolbar');

  selectNodeInNodeInCampaignWorkspace(campaignWorkspace, nodeNames);
  treeToolbarElement.getButton('copy-button').clickWithoutSpan();
  campaignWorkspace.tree
    .findRowIdWithProjectName('NAME', nameTargetNode, 'mainViewport', nameProject)
    .then((entityId) => {
      campaignWorkspace.tree.pickNode(entityId);
    });
  treeToolbarElement.getButton('paste-button').clickWithoutSpan();
}

export function copyTestSuiteToAnotherProjectInCampaignWorkspaceWithShortcuts(
  campaignWorkspace: CampaignWorkspacePage,
  nodeNames: string[],
  nameProjectNode: string,
  nameCampaign: string,
  nameTestSuite: string,
) {
  const keyboardShortcut = new KeyboardShortcuts();

  selectNodeInNodeInCampaignWorkspace(campaignWorkspace, nodeNames);
  keyboardShortcut.performCopy();
  campaignWorkspace.tree
    .findRowIdWithProjectName('NAME', nameCampaign, 'mainViewport', nameProjectNode)
    .then((entityId) => {
      campaignWorkspace.tree.openNodeIfClosed(entityId);
      campaignWorkspace.tree.selectChildNode(entityId, nameTestSuite);
    });
  keyboardShortcut.performPaste();
}

export function dragEntityIntoAnotherEntityInCampaignWorkspace(
  campaignWorkspacePage: CampaignWorkspacePage,
  originNodeName: string,
  nodeToDrag: string,
  nodeToDrop: string,
  objectContainingDrag: string,
  interProject: boolean,
) {
  selectNodeInCampaignWorkspace(campaignWorkspacePage, originNodeName, nodeToDrag);
  campaignWorkspacePage.tree.findRowId('NAME', nodeToDrag).then((idNodeToDrag) => {
    campaignWorkspacePage.tree.findRowId('NAME', nodeToDrop).then((idNodeToDrop) => {
      campaignWorkspacePage.tree
        .findRowId('NAME', objectContainingDrag)
        .then((idObjectWhereDrag) => {
          campaignWorkspacePage.tree.dragAndDropObjectInObjectInWorkspace(
            idNodeToDrag,
            idObjectWhereDrag,
            idNodeToDrop,
            interProject,
          );
        });
    });
  });
}

export function dragAndDropEntityInPositionalOrderInCampaignWorkspaceFromTopToBottom(
  campaignWorkspacePage: CampaignWorkspacePage,
  activatePositionalOrder: boolean,
  selectNodes: string[],
  nodeToDrag: string,
  nodeToDrop: string,
) {
  if (activatePositionalOrder) {
    campaignWorkspacePage.activatePositionalOrder();
    campaignWorkspacePage.closeParameterOverlay();
  }
  selectNodeInNodeInCampaignWorkspace(campaignWorkspacePage, selectNodes);
  campaignWorkspacePage.tree.findRowId('NAME', nodeToDrag).then((idNodeToDrag) => {
    campaignWorkspacePage.tree.findRowId('NAME', nodeToDrop).then((idNodeToDrop) => {
      campaignWorkspacePage.tree.beginDragAndDrop(idNodeToDrag);
      campaignWorkspacePage.tree.dragOverBottomPart(idNodeToDrop);
      campaignWorkspacePage.tree.getTreeNodeCell(idNodeToDrop).drop();
    });
  });
}

export function deleteEntityInCampaignWorkspaceWithToolBar(
  campaignWorkspace: CampaignWorkspacePage,
  nodeNameContainingEntityToDelete: string,
  nodeEntityNameToDelete: string,
) {
  const treeToolbarElement = new TreeToolbarElement('campaign-toolbar');
  const confirmDialog = new DeleteTreeNodesDialogElement('campaign-tree');
  selectNodeInCampaignWorkspace(
    campaignWorkspace,
    nodeNameContainingEntityToDelete,
    nodeEntityNameToDelete,
  );
  treeToolbarElement.getButton('delete-button').clickWithoutSpan();
  confirmDialog.clickOnConfirmButton();
}

export function deleteEntityInCampaignWorkspaceWithShortCut(
  campaignWorkspace: CampaignWorkspacePage,
  nodeNameContainingEntityToDelete: string,
  nodeEntityNameToDelete: string,
) {
  const shortCut = new KeyboardShortcuts();
  const confirmDialog = new DeleteTreeNodesDialogElement('campaign-tree');
  selectNodeInCampaignWorkspace(
    campaignWorkspace,
    nodeNameContainingEntityToDelete,
    nodeEntityNameToDelete,
  );
  shortCut.pressDelete();
  confirmDialog.clickOnConfirmButton();
}

export function linkTestCaseToTestPlanInTestPlanPageThroughAssociateButton(
  viewPage: ViewPage,
  projectTestCaseName: string,
  testCaseName: string,
  idEntity: number,
) {
  const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite(viewPage);
  const testTreePicker = testPlanPage.openTestCaseDrawer();
  testTreePicker.findRowId('NAME', projectTestCaseName).then((idProject) => {
    testTreePicker.openNodeIfClosed(idProject);
    testTreePicker.findRowId('NAME', testCaseName).then((idTestCase) => {
      testTreePicker.beginDragAndDrop(idTestCase);
    });
  });
  testPlanPage.dropIntoTestPlan(idEntity);
  testPlanPage.closeTestCaseDrawer();
}

export function linkTestCaseFromSearchTestCaseInTestPlanPage(
  viewPage: ViewPage,
  testCaseName: string,
) {
  const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite(viewPage);
  const searchTestCasePage = testPlanPage.goToSearchTestCase();

  searchTestCasePage.grid.findRowId('name', testCaseName).then((idTestCase) => {
    searchTestCasePage.grid.assertSpinnerIsNotPresent();
    searchTestCasePage.grid.selectRow(idTestCase, '#', 'leftViewport');
  });
  searchTestCasePage.linkSelectionButton.clickWithoutSpan();
}

export function linkAllTestCasePresentInSearchRequirementLinked(viewPage: ViewPage) {
  const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite(viewPage);
  const requirementSearchTestCasePage = testPlanPage.goToRequirementLinkedSearch();

  requirementSearchTestCasePage.grid.assertSpinnerIsNotPresent();
  requirementSearchTestCasePage.linkAllButton.clickWithoutSpan();
}

export function removeItemInTestPlanPageWithButtonEndOfLine(
  viewPage: ViewPage,
  nameTestCase: string,
  idItems?: number[],
) {
  const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite(viewPage);
  testPlanPage.testPlan.findRowId('testCaseName', nameTestCase).then((idItem) => {
    testPlanPage.testPlan.getCell(idItem, 'delete', 'rightViewport').iconRenderer().click();
  });
  if (viewPage === 'campaign') {
    const removeCtpiDialog = new RemoveCtpiDialogElement('*', '*');
    removeCtpiDialog.assertExists();
    removeCtpiDialog.clickOnConfirmButton();
  }
  if (viewPage === 'iteration') {
    const removeItpiDialog = new RemoveItpiDialogElement('*', '*');
    removeItpiDialog.assertExists();
    removeItpiDialog.clickOnConfirmButton();
  }
  if (viewPage === 'test-suite') {
    const removeOrDetachItpiDialog = new RemoveOrDetachTestSuiteItpiDialogElement('*', idItems);
    removeOrDetachItpiDialog.assertExists();
    removeOrDetachItpiDialog.detachFromTestSuite();
  }
}

export function removeItemInTestPlanPageWithBarButton(
  viewPage: ViewPage,
  testCaseIdentifier: number[],
) {
  const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite(viewPage);

  testPlanPage.testPlan.selectRows(testCaseIdentifier, '#', 'leftViewport');
  cy.get(selectByDataTestButtonId('show-confirm-mass-delete-dialog')).click();
  if (viewPage === 'campaign') {
    const removeCtpiDialog = new MassDeleteCtpiDialogElement('*', testCaseIdentifier);
    removeCtpiDialog.clickOnConfirmButton();
  }
  if (viewPage === 'iteration') {
    const removeItpiDialog = new MassDeleteItpiDialogElement('*', testCaseIdentifier);
    removeItpiDialog.clickOnConfirmButton();
  }
  if (viewPage === 'test-suite') {
    const removeOrDetachItpiDialog = new RemoveOrDetachTestSuiteItpiDialogElement(
      '*',
      testCaseIdentifier,
    );
    removeOrDetachItpiDialog.assertExists();
    removeOrDetachItpiDialog.detachFromTestSuite();
  }
}

export function addItpiToExecutionPlanOfAnIteration(
  campaignWorkspace: CampaignWorkspacePage,
  idItpiInSearchPage: number[],
  nameNodes: string[],
) {
  const itpiSearchPage = campaignWorkspace.goToSearchItpiPage();
  const addTestPlanDialog = new AddTestPlanDialogElement();

  itpiSearchPage.grid.assertSpinnerIsNotPresent();
  itpiSearchPage.grid.selectRows(idItpiInSearchPage, '#', 'leftViewport');
  itpiSearchPage.clickAddButton();
  addTestPlanDialog.selectIterationToLinkToItpi(nameNodes);
  addTestPlanDialog.clickAddButton();
}

export function modifyExecutionTestStepStatusFromSearchPage(
  campaignWorkspace: CampaignWorkspacePage,
  itemName: string,
  stepId: string,
  executionStatus: ExecutionStatusKeys,
  executionId: number,
) {
  const itpiSearchPage = campaignWorkspace.goToSearchItpiPage();
  const executionPage = itpiSearchPage.openExecutionPageOfAnItpi(itemName);
  const scenarioPanelElement = executionPage.clickScenarioAnchorLink();

  scenarioPanelElement
    .getExecutionStepById(stepId)
    .changeStepExecutionStatus(executionStatus, executionId);
}

export function addIssueToAnExecution(
  executionPage: ExecutionPage,
  token: string,
  pathProject: string,
  issueId: string,
) {
  executionPage.clickIssueAnchorLink();
  executionPage.issueGrid.assertGridIsEmpty();
  executionPage.connectToABugTrackerWithToken(token);
  executionPage.issueGrid.assertSpinnerIsNotPresent();
  executionPage.linkIssueToAnExecution(pathProject, issueId);
}

export function createAnIssueFromAnExecutionInExecutionPage(
  executionPage: ExecutionPage,
  token: string,
  name: string,
) {
  executionPage.clickIssueAnchorLink();
  executionPage.issueGrid.assertGridIsEmpty();
  executionPage.connectToABugTrackerWithToken(token);
  executionPage.issueGrid.assertSpinnerIsNotPresent();
  executionPage.createNewIssueInExecution(name);
}

export function createAnIssueFromExecutionRunnerStepPage(
  executionRunnerStepPage: ExecutionRunnerStepPage,
  token: string,
  title: string,
) {
  executionRunnerStepPage.connectToABugTrackerWithToken(token);
  executionRunnerStepPage.createNewIssueInExecution(title);
}

export function createAnIssueFromAnExecutionStepInExecutionPage(
  executionScenarioPanelPage: ExecutionScenarioPanelElement,
  title: string,
  stepId: string,
  connectToBugtracker?: boolean,
  token?: string,
) {
  if (connectToBugtracker) {
    const executionPage = new ExecutionPage();
    executionPage.clickIssueAnchorLink();
    executionPage.connectToABugTrackerWithToken(token);
    executionPage.clickScenarioAnchorLink();
  }
  executionScenarioPanelPage.getExecutionStepById(stepId).createNewIssue(title);
}

export function removeIssueFromAnExecutionWithToolBarButton(
  executionPage: ExecutionPage,
  remoteId: string,
) {
  const deleteLinkDialog = new SimpleDeleteConfirmDialogElement();

  executionPage.issueGrid.findRowId('remoteId', remoteId).then((idIssue) => {
    executionPage.issueGrid.selectRow(idIssue, '#', 'leftViewport');
  });
  executionPage.removeIssueToolBarButton.click();
  deleteLinkDialog.clickOnConfirmButton();
}

export function removeIssueWithButtonInGridEndOfLine(
  executionPage: ExecutionPage,
  summaryIssue: string,
) {
  const deleteLinkDialog = new SimpleDeleteConfirmDialogElement();

  executionPage.issueGrid.findRowId('summary', summaryIssue).then((idIssue) => {
    executionPage.issueGrid.getCell(idIssue, 'delete', 'rightViewport').iconRenderer().click();
  });
  deleteLinkDialog.clickOnConfirmButton();
}

export function createNewExecutionOfTestInTestPlanPageAndModifyStatus(
  viewPage: ViewPage,
  itemName: string,
  stepId: string,
  executionStatus: ExecutionStatusKeys,
  executionId: number,
) {
  const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite(viewPage);
  const executionPage = testPlanPage.createNewExecutionOfItem(itemName);
  const scenarioPanelElement = executionPage.clickScenarioAnchorLink();

  scenarioPanelElement
    .getExecutionStepById(stepId)
    .changeStepExecutionStatus(executionStatus, executionId);
}

export function goToCommentPanelAndFillAComment(executionPage: ExecutionPage, comment: string) {
  executionPage.clickCommentAnchorLink();
  executionPage.commentField.setAndConfirmValue(comment);
}

export function startOverAnExecution(
  viewPage: ViewPage,
  entityId: number,
  testPlanId: number,
  executionId: number,
) {
  const testPlanPage = showTestPlanInCampaignOrIterationOrTestSuite(viewPage);
  const confirmDialog = new ConfirmDeleteAllExecutionElement();

  testPlanPage.openStartOverDialog();
  switch (viewPage) {
    case 'campaign':
      {
        confirmDialog.clickConfirmButtonAndGoToNewWindowEntityRunner(
          'campaign',
          entityId,
          testPlanId,
          executionId,
        );
      }
      break;
    case 'iteration':
      {
        confirmDialog.clickConfirmButtonAndGoToNewWindowEntityRunner(
          'iteration',
          entityId,
          testPlanId,
          executionId,
        );
      }
      break;
    case 'test-suite':
      {
        confirmDialog.clickConfirmButtonAndGoToNewWindowEntityRunner(
          'test-suite',
          entityId,
          testPlanId,
          executionId,
        );
      }
      break;
    default:
      throw new Error(
        `"${viewPage}" is not iteration or test-suite or campaign. This method only allows those values : campaign, iteration and test-suite`,
      );
  }
}

export function addCommentToExecutionRunnerStepPage(comment: string) {
  const executionRunnerProloguePage = new ExecutionRunnerProloguePage();
  const stepPageExecution = new ExecutionRunnerStepPage();

  executionRunnerProloguePage.startExecution();
  stepPageExecution.updateComment(comment);
}

export function linkIssueToAnExecutionRunnerStepPage(
  executionRunnerStepPage: ExecutionRunnerStepPage,
  issueId: string,
) {
  executionRunnerStepPage.issuesPanelGrid.assertGridIsEmpty();
  executionRunnerStepPage.issuesPanelGrid.assertSpinnerIsNotPresent();
  executionRunnerStepPage.linkIssueToAnExecution(issueId);
}

export function addCommentToAnExecutionStep(
  executionScenarioPanel: ExecutionScenarioPanelElement,
  stepId: string,
  comment: string,
) {
  executionScenarioPanel.getExecutionStepById(stepId).createNewComment();
  executionScenarioPanel.getExecutionStepById(stepId).updateComment(comment);
}

export function resumeExecutionInTestPlanPageIterationOrTestSuite(
  viewPage: 'iteration' | 'test-suite',
  idEntity: string,
  idTestPlan: string,
  idExecution: string,
  idStep: string,
) {
  let viewPagePlan: IterationTestPlanPage | TestSuiteTestPlanPage = null;

  switch (viewPage) {
    case 'iteration':
      viewPagePlan = showTestPlanInCampaignOrIterationOrTestSuite(
        viewPage,
      ) as IterationTestPlanPage;
      break;
    case 'test-suite':
      viewPagePlan = showTestPlanInCampaignOrIterationOrTestSuite(
        viewPage,
      ) as TestSuiteTestPlanPage;
      break;
    default:
      throw new Error(
        `${viewPagePlan} incorrect. This method only support iteration and test-suite`,
      );
  }
  viewPagePlan.clickButtonResumeExecution(idEntity, idTestPlan, idExecution, idStep);
}

export function launchExecutionInTestPlanPageIterationOrTestSuite(
  viewPage: 'iteration' | 'test-suite',
  idEntity: string,
  idTestPlan: string,
  idExecution: string,
  idStep: string,
) {
  if (viewPage === 'iteration') {
    const viewPagePlan = showTestPlanInCampaignOrIterationOrTestSuite(
      'iteration',
    ) as IterationTestPlanPage;
    viewPagePlan.clickButtonLaunchExecution(idEntity, idTestPlan, idExecution, idStep);
  } else {
    const viewPagePlan = showTestPlanInCampaignOrIterationOrTestSuite(
      'test-suite',
    ) as TestSuiteTestPlanPage;
    viewPagePlan.clickButtonLaunchExecution(idEntity, idTestPlan, idExecution, idStep);
  }
}

export function goForwardOrBackwardInExecutionRunnerStep(
  executionRunnerStepPage: ExecutionRunnerStepPage,
  direction: 'Forward' | 'Backard',
) {
  switch (direction) {
    case 'Forward': {
      executionRunnerStepPage.navigateForward();
      break;
    }
    case 'Backard': {
      executionRunnerStepPage.navigateBackward();
      break;
    }
    default:
      throw new Error(`${direction} incorrect. This method only support Forward and Backward`);
  }
}

export function removeIssueFromExecutionRunnerStepPageWithEndOfLineButton(
  executionRunnerStepPage: ExecutionRunnerStepPage,
  issueName: string,
) {
  const deleteLinkDialog = new SimpleDeleteConfirmDialogElement();

  executionRunnerStepPage.issuesPanelGrid.findRowId('summary', issueName).then((idIssue) => {
    executionRunnerStepPage.issuesPanelGrid.getCell(idIssue, 'delete').iconRenderer().click();
  });
  deleteLinkDialog.clickOnConfirmButton();
}

export function createExecutionWithPopUpInIterationAndTestSuite(
  viewPage: 'iteration' | 'test-suite',
  nameEntity: string,
  idExecution: string,
) {
  let viewPagePlan: TestPlanPage = null;

  switch (viewPage) {
    case 'iteration':
      viewPagePlan = showTestPlanInCampaignOrIterationOrTestSuite(
        viewPage,
      ) as IterationTestPlanPage;
      break;
    case 'test-suite':
      viewPagePlan = showTestPlanInCampaignOrIterationOrTestSuite(
        viewPage,
      ) as TestSuiteTestPlanPage;
      break;
    default:
      throw new Error(
        `${viewPagePlan} incorrect. This method only support iteration and test-suite`,
      );
  }
  viewPagePlan.openExecutionWithPopUp(nameEntity, idExecution);
}

export function accessToSessionPageInIterationOrTestSuiteFromCampaignWorkspace(
  campaignWorkspace: CampaignWorkspacePage,
  nodeNames: string[],
  viewPage: 'iteration' | 'test-suite',
  itemName: string,
) {
  let viewPagePlan: TestPlanPage = null;
  selectNodeInNodeInCampaignWorkspace(campaignWorkspace, nodeNames);

  switch (viewPage) {
    case 'iteration':
      viewPagePlan = showTestPlanInCampaignOrIterationOrTestSuite(
        viewPage,
      ) as IterationTestPlanPage;
      break;
    case 'test-suite':
      viewPagePlan = showTestPlanInCampaignOrIterationOrTestSuite(
        viewPage,
      ) as TestSuiteTestPlanPage;
      break;
    default:
      throw new Error(
        `${viewPagePlan} incorrect. This method only support iteration and test-suite`,
      );
  }
  viewPagePlan.accessToSession(itemName);
}

export function addAnExecutionInSessionPageAndAccessThisExecutionPage(
  sessionOverviewPage: SessionOverviewPage,
  executionNumber: string,
) {
  sessionOverviewPage.addExecution();
  sessionOverviewPage.executionsPanel.gridElement
    .findRowId('#', executionNumber, 'leftViewport')
    .then((idExecution) => {
      sessionOverviewPage.executionsPanel.gridElement
        .getCell(idExecution, 'executionId', 'rightViewport')
        .iconRenderer()
        .click();
    });
}

export function startSessionAndAddNewNote(
  executionPage: ExecutionPage,
  noteNumber: number,
  comment: string,
) {
  executionPage.durationPanel.runExploratoryExecution();
  executionPage.findNoteContainer(noteNumber).newNoteElement.newNoteRichTextField.clear();
  executionPage.findNoteContainer(noteNumber).newNoteElement.newNoteRichTextField.fill(comment);
  executionPage.findNoteContainer(noteNumber).newNoteElement.confirm();
}

export function modifySessionNoteContentAndType(
  executionPage: ExecutionPage,
  indexNote: number,
  newComment: string,
  kindNote: SessionNoteType,
) {
  executionPage.clickCharterAnchorLink();
  executionPage.findNoteContainer(indexNote).existingNoteElement.updateContentText(newComment);
  executionPage.findNoteContainer(indexNote).existingNoteElement.kindSelect.selectKind(kindNote);
}

export function removeSessionNote(executionPage: ExecutionPage, indexNote: number) {
  const deleteNoteDialog = executionPage
    .findNoteContainer(indexNote)
    .existingNoteElement.clickRemoveButton();
  deleteNoteDialog.clickOnConfirmButton();
}

export function assertThatDragAndDropWorksInPositionalOrder(
  campaignWorkspacePage: CampaignWorkspacePage,
  activatePositionalOrder: boolean,
  nodesToSelect: string[],
  sourceNode: string,
  targetNode: string,
  expectedNodeOrder: string[],
) {
  dragAndDropEntityInPositionalOrderInCampaignWorkspaceFromTopToBottom(
    campaignWorkspacePage,
    activatePositionalOrder,
    nodesToSelect,
    sourceNode,
    targetNode,
  );
  campaignWorkspacePage.tree.assertSpinnerIsNotPresent();
  campaignWorkspacePage.tree.assertNodeOrderByName(expectedNodeOrder);
}

export function assertNodeHasCorrectParent(
  campaignWorkspace: CampaignWorkspacePage,
  nameOriginNode: string,
  nameNode: string,
) {
  campaignWorkspace.tree.findRowId('NAME', nameOriginNode).then((idProject) => {
    campaignWorkspace.tree.findRowId('NAME', nameNode).then((idFolder) => {
      campaignWorkspace.tree.assertRowHasParent(idFolder, idProject);
    });
  });
}

export function createAnIterationFromSearchPage(
  campaignWorkspace: CampaignWorkspacePage,
  itpiName: string,
  namesOfNodesToIteration: string[],
  nodeName: string,
  iterationName: string,
) {
  createIterationFromItpiInSearchPage(campaignWorkspace, itpiName, namesOfNodesToIteration);
  NavBarElement.navigateToCampaignWorkspace();
  campaignWorkspace.tree.findRowId('NAME', nodeName).then((idCampaign) => {
    campaignWorkspace.tree.openNodeIfClosed(idCampaign);
    campaignWorkspace.tree.findRowId('NAME', iterationName).then((idIteration) => {
      campaignWorkspace.tree.assertRowExist(idIteration);
    });
  });
}

export function assertCannotDeleteExecutionInExecutionPageWithTopIcon(
  executionPage: ExecutionPage,
  executionNumber: string,
) {
  const historyExecution = executionPage.clickHistoryAnchorLink();
  historyExecution.assertCannotDeleteExecutionWithTopIcon(executionNumber);
}
