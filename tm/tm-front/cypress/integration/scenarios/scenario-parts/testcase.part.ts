import { TestCaseWorkspacePage } from '../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import { TestCaseViewPage } from '../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import { TestCaseViewSubPage } from '../../page-objects/pages/test-case-workspace/test-case/test-case-view-sub-page';
import { TestCaseFolderViewPage } from '../../page-objects/pages/test-case-workspace/test-case-folder/test-case-folder-view.page';
import { TreeElement } from '../../page-objects/elements/grid/grid.element';
import { DatasetDuplicationDialogElement } from '../../page-objects/pages/test-case-workspace/dialogs/dataset-duplication-dialog.element';
import { ConfirmInterProjectPaste } from '../../page-objects/elements/dialog/confirm-inter-project-paste';
import { DetailedStepViewPage } from '../../page-objects/pages/test-case-workspace/detailed-step-view/detailed-step-view.page';
import { InputType } from '../../../../projects/sqtm-core/src/lib/model/customfield/input-type.model';
import { EditableTagFieldElement } from '../../page-objects/elements/forms/editable-tag-field.element';
import { TestCaseViewParametersPage } from '../../page-objects/pages/test-case-workspace/test-case/test-case-view-parameters.page';
import { EditableDateFieldElement } from '../../page-objects/elements/forms/editable-date-field.element';
import { CheckBoxElement } from '../../page-objects/elements/forms/check-box.element';
import { EditableSelectFieldElement } from '../../page-objects/elements/forms/editable-select-field.element';
import { TestCaseSearchPage } from '../../page-objects/pages/test-case-workspace/research/test-case-search-page';
import { GroupedMultiListElement } from '../../page-objects/elements/filters/grouped-multi-list.element';
import { RequirementViewPage } from '../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import { RequirementWorkspacePage } from '../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import { selectNodeInAnotherNode } from './requirement.part';
import { CampaignWorkspacePage } from '../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { IterationViewPage } from '../../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import { TreeToolbarElement } from '../../page-objects/elements/workspace-common/tree-toolbar.element';
import { CreateTestCaseDialog } from '../../page-objects/pages/test-case-workspace/dialogs/create-test-case-dialog.element';
import { TestCaseViewCharterPage } from '../../page-objects/pages/test-case-workspace/test-case/test-case-view-charter.page';
import { KeywordTestStepsViewPage } from '../../page-objects/pages/test-case-workspace/test-case/keyword-test-step-view-page';
import { TestCaseViewScriptPage } from '../../page-objects/pages/test-case-workspace/test-case/test-case-view-script.page';
import { NavBarElement } from '../../page-objects/elements/nav-bar/nav-bar.element';
import { createRegExpForCopyNode } from '../../utils/end-to-end/end-to-end-helper';
import { KeyboardShortcuts } from '../../page-objects/elements/workspace-common/keyboard-shortcuts';
import { SimpleDeleteConfirmDialogElement } from '../../page-objects/elements/dialog/simple-delete-confirm-dialog.element';
import { ConfirmInterProjectMove } from '../../page-objects/elements/dialog/confirm-inter-project-move';
import { TestStepsViewPage } from '../../page-objects/pages/test-case-workspace/test-case/test-steps-view.page';

type TestCaseNature =
  | 'Fonctionnel'
  | 'Non fonctionnel'
  | 'Métier'
  | 'Utilisateur'
  | 'Performance'
  | 'Sécurité'
  | 'ATDD';
type TestCaseTypeTest =
  | 'Non défini'
  | 'Recevabilité'
  | 'Correctif'
  | 'Evolution'
  | 'Non-régression'
  | 'Bout-en-bout'
  | 'Partenaire';
type TestCaseImportance = 'Faible' | 'Moyenne' | 'Haute' | 'Très haute';
type automationTechnologiesName =
  | 'Agilitest'
  | 'Cucumber 4'
  | 'Cucumber 5+'
  | 'Cypress'
  | 'JUnit'
  | 'Katalon'
  | 'Postman'
  | 'Ranorex'
  | 'Robot Framework'
  | 'SKF'
  | 'SoapUi'
  | 'UF';

export function selectNodeInTestCaseWorkspace(
  testCaseWorkspace: TestCaseWorkspacePage,
  nameOriginNode: string,
  nameNode: string,
) {
  const testCaseTree = testCaseWorkspace.tree;
  testCaseTree.findRowId('NAME', nameOriginNode).then((idProject) => {
    testCaseTree.openNodeIfClosed(idProject);
  });
  testCaseTree.findRowId('NAME', nameNode).then((idRequirement) => {
    testCaseTree.selectNode(idRequirement);
  });
}

export function selectProjectInTestCaseWorkspace(
  testCaseWorkspace: TestCaseWorkspacePage,
  nameProject: string,
) {
  const testCaseTree = testCaseWorkspace.tree;
  testCaseTree.findRowId('NAME', nameProject).then((idProject) => {
    testCaseTree.selectRow(idProject, 'NAME');
  });
}

export function renameChangeTypeTestAndModifyInformationBloc(
  testCaseViewPage: TestCaseViewPage,
  newName: string,
  newReference: string,
  nature: TestCaseNature,
  typeTest: TestCaseTypeTest,
  importanceTest: TestCaseImportance,
) {
  const testCaseViewInformationPage = testCaseViewPage.informationPanel;
  testCaseViewInformationPage.nameTextField.setAndConfirmValue(newName);
  testCaseViewInformationPage.nameTextField.assertContainsText(newName);
  testCaseViewInformationPage.referenceTextField.setAndConfirmValue(newReference);
  testCaseViewInformationPage.referenceTextField.assertContainsText(newReference);
  testCaseViewInformationPage.natureSelectField.selectValueNoButton(nature);
  testCaseViewInformationPage.typeSelectField.selectValueNoButton(typeTest);
  testCaseViewInformationPage.importanceSelectField.selectValueNoButton(importanceTest);
}

export function accessSecondLevelPageTestCaseThroughSearchAndSelectTestCase(
  testCaseWorkspace: TestCaseWorkspacePage,
  nameTestCase: string,
) {
  const testCaseSearchPage = testCaseWorkspace.clickSearchButton();
  const testCaseViewSubPage = new TestCaseViewSubPage('*');

  testCaseSearchPage.assertExists();
  testCaseSearchPage.grid.assertSpinnerIsNotPresent();
  testCaseSearchPage.grid.findRowId('name', nameTestCase).then((idTestCase) => {
    testCaseSearchPage.grid
      .getCell(idTestCase, 'detail', 'rightViewport')
      .linkRenderer()
      .findCellLink()
      .should((linkPage) => {
        expect(linkPage.attr('target'), 'target').to.equal('_blank');
        linkPage.attr('target', '_self');
      })
      .click();
    testCaseViewSubPage.assertExists();
  });
}

export function associateAndDissociateMilestoneOnTestCase(
  testCaseViewPage: TestCaseViewPage,
  milestoneName: string,
) {
  const informationPage = testCaseViewPage.informationPanel;
  const milestoneDialog = informationPage.milestonesField.openMilestoneDialog();

  milestoneDialog.selectMilestone(milestoneName);
  milestoneDialog.clickOnConfirmButton();
  informationPage.milestonesField.assertContainsText(milestoneName);
  informationPage.milestonesField.clickOnRemoveMilestoneClose(milestoneName);
  informationPage.milestonesField.assertMilestoneIsNotPresent(milestoneName);
}

export function modifyDurationOfSession(
  testCaseViewPage: TestCaseViewPage,
  durationHour: string,
  durationMinute: string,
) {
  const testCaseViewCharterPage = testCaseViewPage.clickCharterAnchorLink();
  testCaseViewCharterPage.assertExists();
  testCaseViewCharterPage.durationField.enableEditMode();
  testCaseViewCharterPage.durationField.selectInList(durationHour);
  testCaseViewCharterPage.durationField.selectInList(durationMinute);
  testCaseViewCharterPage.durationField.rootElement.type('{enter}');
  testCaseViewCharterPage.durationField.assertContainsText(durationHour);
  testCaseViewCharterPage.durationField.assertContainsText(durationMinute);
}

export function modifyChart(testCaseViewPage: TestCaseViewPage, chartText: string) {
  const testCaseViewCharterPage = testCaseViewPage.clickCharterAnchorLink();
  testCaseViewCharterPage.charterRichField.setAndConfirmValue(chartText);
  testCaseViewCharterPage.charterRichField.assertContainsText(chartText);
}

export function selectFolderAndCheckIfParameterButtonWork(
  testcaseWorkspace: TestCaseWorkspacePage,
  testCaseFolderViewPage: TestCaseFolderViewPage,
  nameOriginNode: string,
  nameNode: string,
) {
  selectNodeInTestCaseWorkspace(testcaseWorkspace, nameOriginNode, nameNode);
  testCaseFolderViewPage.assertExists();
  testcaseWorkspace.clickOnParameterButton();
  testcaseWorkspace.assertParameterOverlayExists();
  testcaseWorkspace.closeParameterOverlay();
}

export function changeNameAndDescriptionOfTestcaseFolder(
  testCaseFolderViewPage: TestCaseFolderViewPage,
  newName: string,
  description: string,
) {
  testCaseFolderViewPage.nameTextField.setValue(newName);
  testCaseFolderViewPage.nameTextField.clickOnConfirmButton();
  testCaseFolderViewPage.nameTextField.assertContainsText(newName);
  testCaseFolderViewPage
    .showInformationPanel()
    .descriptionRichField.setAndConfirmValue(description);
  testCaseFolderViewPage
    .showInformationPanel()
    .descriptionRichField.assertContainsText(description);
}

export function seeDetailsStepViewPage(
  testCaseWorkspace: TestCaseWorkspacePage,
  testCaseViewPage: TestCaseViewPage,
  nameProject: string,
  nameTestCase: string,
  stepIndex: number,
) {
  selectNodeInTestCaseWorkspace(testCaseWorkspace, nameProject, nameTestCase);
  const stepsViewPage = testCaseViewPage.clickStepsAnchorLink();
  stepsViewPage.assertExists();
  const detailedStepViewPage = stepsViewPage
    .getActionStepByIndex(stepIndex)
    .navigateToStepDetails();
  detailedStepViewPage.assertExists();
}

export function modifyStepInDetailedStepView(
  detailedStepViewPage: DetailedStepViewPage,
  action: string,
  result: string,
) {
  detailedStepViewPage.changeAction(action);
  detailedStepViewPage.changeResult(result);
}

export function addTechnologyToKeywordTestCase(
  keywordTestcaseViewPage: TestCaseViewPage,
  nameTechno: automationTechnologiesName,
) {
  keywordTestcaseViewPage.clickAutomationAnchorLink().chooseTechnology(nameTechno);
}

export function copyDatasetIntoAnotherKeywordTestCase(
  keywordTestcaseViewPage: TestCaseViewPage,
  testCaseName: string,
  projectName: string,
  testCaseNameDataset: string,
) {
  const tree = new TreeElement('test-case-tree-dataset-duplication-picker');
  const dataSetDialogElement = new DatasetDuplicationDialogElement(tree);
  const dialogConfirmDuplication = new ConfirmInterProjectPaste('dataset-duplication-recap-dialog');

  keywordTestcaseViewPage
    .clickParametersAnchorLink()
    .parametersTable.findRowId('name', testCaseName, 'leftViewport')
    .then((idDataSet) => {
      keywordTestcaseViewPage
        .clickParametersAnchorLink()
        .openDuplicateDataSetDialogAndInitializeTree(idDataSet, tree);
    });
  dataSetDialogElement.assertExists();
  dataSetDialogElement.eligibleTestCaseTree.findRowId('NAME', projectName).then((idProject) => {
    dataSetDialogElement.eligibleTestCaseTree.getTreeNodeCell(idProject).toggle();
  });
  dataSetDialogElement.eligibleTestCaseTree
    .findRowId('NAME', testCaseNameDataset)
    .then((idTestCase) => {
      dataSetDialogElement.eligibleTestCaseTree.pickNode(idTestCase);
    });
  dataSetDialogElement.confirm();
  dialogConfirmDuplication.assertExists();
  dialogConfirmDuplication.close();
}

export function createNewParameter(keywordTestcaseViewPage: TestCaseViewPage, nameDataset: string) {
  keywordTestcaseViewPage
    .clickParametersAnchorLink()
    .openAddParameterDialog()
    .createNewParam(nameDataset);
}

export function clickSettingsAndSelectSortingOrder(
  testcaseWorkspace: TestCaseWorkspacePage,
  testCaseFolderViewPage: TestCaseFolderViewPage,
) {
  testCaseFolderViewPage.assertExists();
  const settingsMenu = testcaseWorkspace.clickOnParameterButton();
  settingsMenu.assertExists();
  settingsMenu.item('sort-alphabetical').assertExists();
  settingsMenu.item('sort-alphabetical').click();
  settingsMenu.item('sort-positional').assertExists();
  settingsMenu.item('sort-positional').click();
  testcaseWorkspace.closeParameterOverlay();
}

export function verifyNameFieldIsNotClickableAndEditable(
  testCaseFolderViewPage: TestCaseFolderViewPage,
) {
  testCaseFolderViewPage.assertExists();
  testCaseFolderViewPage.nameTextField.assertIsNotClickable();
  testCaseFolderViewPage.nameTextField.assertIsNotEditable();
}

export function cannotEditDescriptionAndChangeTagValue(
  testCaseFolderViewPage: TestCaseFolderViewPage,
) {
  testCaseFolderViewPage.showInformationPanel().descriptionRichField.assertIsNotEditable();
  const tagFieldElement = testCaseFolderViewPage.getCustomField(
    'TagCuf',
    InputType.TAG,
  ) as EditableTagFieldElement;
  tagFieldElement.assertIsNotEditable();
}

export function cannotAssociateMilestone(testcaseViewPage: TestCaseViewPage) {
  const infoPanelMilestone = testcaseViewPage.informationPanel.milestonesField;
  infoPanelMilestone.openMilestoneDialogNotExist();
}

export function cannotDissociateMilestone(
  testcaseViewPage: TestCaseViewPage,
  nomMilestone: string,
) {
  const infoPanelMilestone = testcaseViewPage.informationPanel.milestonesField;
  infoPanelMilestone.removeMilestoneButtonShouldNotExist(nomMilestone);
}

export function assertCannotDuplicateDataSet(
  testCaseViewParameters: TestCaseViewParametersPage,
  contentCell: string,
  dataSetName: string,
) {
  testCaseViewParameters.parametersTable
    .findRowId('name', contentCell, 'leftViewport')
    .then((idDataSet) => {
      testCaseViewParameters.openDuplicateDataSetDialogAndCannotDuplicateDataSet(
        idDataSet,
        dataSetName,
      );
    });
}

export function verifyCannotChangeHeaderNameAndStatusInKeywordTestCase(
  keywordTestcaseViewPage: TestCaseViewPage,
) {
  keywordTestcaseViewPage.informationPanel.nameTextField.assertIsNotEditable();
  keywordTestcaseViewPage.statusCapsule.assertIsNotClickable();
}

export function verifyCannotModifyNatureAndDateCufInInformationBlock(
  keywordTestcaseViewPage: TestCaseViewPage,
) {
  const dateCuf = keywordTestcaseViewPage.getCustomField(
    'Date',
    InputType.DATE_PICKER,
  ) as EditableDateFieldElement;
  const natureSelectField = keywordTestcaseViewPage.informationPanel.natureSelectField;
  natureSelectField.assertIsNotEditable();
  dateCuf.assertIsNotEditable();
}

export function assertCannotAddTechnologyToKeywordTestCase(testcaseViewPage: TestCaseViewPage) {
  testcaseViewPage.clickAutomationAnchorLink().automationTechnology.assertIsNotEditable();
}

export function assertCannotAssociateOrDissociateMilestoneInTestCaseViewPage(
  testcaseWorkspace: TestCaseWorkspacePage,
  testcaseViewPage: TestCaseViewPage,
  nameOriginNode: string,
  nameNode: string,
  nameMilestone: string,
) {
  cannotAssociateMilestone(testcaseViewPage);
  selectNodeInTestCaseWorkspace(testcaseWorkspace, nameOriginNode, nameNode);
  cannotDissociateMilestone(testcaseViewPage, nameMilestone);
}

export function assertCannotChangeImportantCheckboxValueAndEditDescription(
  testcaseViewPage: TestCaseViewPage,
) {
  const importanceCapsule = testcaseViewPage.importanceCapsule;
  const descriptionRichField = testcaseViewPage.informationPanel.descriptionRichField;
  const checkBoxCuf = testcaseViewPage.getCustomField(
    'Auto',
    InputType.CHECKBOX,
  ) as CheckBoxElement;
  importanceCapsule.assertIsNotClickable();
  checkBoxCuf.click();
  checkBoxCuf.findAndCheckState('span', false);
  descriptionRichField.assertIsNotEditable();
}

export function verifyCannotChangeHeaderOnTestCaseViewPage(testCaseViewPage: TestCaseViewPage) {
  const referenceTextField = testCaseViewPage.informationPanel.referenceTextField;
  const importanceCapsule = testCaseViewPage.importanceCapsule;
  referenceTextField.assertIsNotEditable();
  importanceCapsule.assertIsNotClickable();
}

export function assertCanEditTypeFieldAndSelectDropdownOption(testCaseViewPage: TestCaseViewPage) {
  const typeSelectField = testCaseViewPage.informationPanel.typeSelectField;
  const dropdownListCuf = testCaseViewPage.getCustomField(
    'Progress status',
    InputType.DROPDOWN_LIST,
  ) as EditableSelectFieldElement;

  typeSelectField.assertIsNotEditable();
  dropdownListCuf.assertIsNotEditable();
}

export function assertAccessToCharterWhenSessionDurationNotVisible(
  testCaseViewPage: TestCaseViewPage,
) {
  const viewCharterPage = testCaseViewPage.clickCharterAnchorLink();
  viewCharterPage.assertExists();
  const durationField = viewCharterPage.durationField;
  durationField.assertIsNotVisible();
}
export function assertCannotModifyCharterContent(testCaseViewPage: TestCaseViewPage) {
  const viewCharterPage = testCaseViewPage.clickCharterAnchorLink();
  const charterRichField = viewCharterPage.charterRichField;
  charterRichField.click();
  charterRichField.assertIsNotInEditMode();
}

export function consultSearchPageAndAddCriteria(
  testcaseWorkspace: TestCaseWorkspacePage,
  searchTestcasePage: TestCaseSearchPage,
  multiListCriteria: GroupedMultiListElement,
  multiListProject: GroupedMultiListElement,
  itemName: string,
) {
  testcaseWorkspace.assertExists();
  testcaseWorkspace.clickSearchButton();
  searchTestcasePage.assertExists();
  searchTestcasePage.clickOnPerimeter();
  multiListProject.assertExists();
  multiListProject.assertItemIsSelected(itemName);
  cy.clickVoid();
  searchTestcasePage.clickAddCriteria();
  searchTestcasePage.selectCriteriaByLabel('Format', false, 'Classique');
  cy.clickVoid();
  searchTestcasePage.clickOnNewSearch();
  searchTestcasePage.clickAddCriteria();
  multiListCriteria.assertNoItemIsSelected();
  cy.clickVoid();
}

export function accessToTestCaseSecondLevelViewPageFromRequirementPage(
  requirementViewPage: RequirementViewPage,
  requirementWorkspace: RequirementWorkspacePage,
  testCaseViewDetailsPage: TestCaseViewSubPage,
  nameProject: string,
  nameNode: string,
  nameTestCase: string,
) {
  const requirementTestCaseTable = requirementViewPage.currentVersion.verifyingTestCaseTable;
  selectNodeInAnotherNode(requirementWorkspace, nameProject, nameNode);
  requirementTestCaseTable.scrollPosition('right', 'mainViewport');
  requirementTestCaseTable.findRowId('name', nameTestCase).then((idTestCase) => {
    requirementTestCaseTable.getCell(idTestCase, 'name').linkRenderer().findCellLink().click();
  });
  testCaseViewDetailsPage.assertExists();
  testCaseViewDetailsPage.assertNameContains(nameTestCase);
}

export function assertCannotChangeValueOfParameterInDataset(
  testCaseViewPage: TestCaseViewPage,
  datasetName: string,
  paramName: string,
) {
  const testCaseViewParameters = testCaseViewPage.clickParametersAnchorLink();
  testCaseViewParameters.unableToChangeParamValue(datasetName, paramName);
}

export function getToSecondViewPageOfScriptedTestCase(
  campaignWorkspace: CampaignWorkspacePage,
  iterationViewPage: IterationViewPage,
  scriptedTestCaseSecondLevelViewPage: TestCaseViewSubPage,
  nameOriginNode: string,
  nameNode: string,
  nameSubNode: string,
) {
  const campaignTree = campaignWorkspace.tree;
  campaignTree
    .findRowId('NAME', nameOriginNode)
    .then((idProject) => campaignTree.openNode(idProject));
  campaignTree.findRowId('NAME', nameNode).then((idCampaign) => campaignTree.openNode(idCampaign));
  campaignTree.findRowId('NAME', nameSubNode).then((idIteration) => {
    campaignTree.selectNode(idIteration);
  });
  const iterationTestPlanViewPage = iterationViewPage.showTestPlan();
  iterationTestPlanViewPage.testPlan.findRowId('projectName', nameOriginNode).then((idTestPlan) => {
    iterationTestPlanViewPage.testPlan
      .getCell(idTestPlan, 'testCaseName')
      .linkRenderer()
      .findCellLink()
      .click();
  });
  scriptedTestCaseSecondLevelViewPage.assertExists();
}

export function assertCanAccessScriptPageButCannotEdit(scriptedTestCaseViewPage: TestCaseViewPage) {
  const scriptPage = scriptedTestCaseViewPage.clickScriptAnchorLink();
  scriptPage.assertExists();
  scriptPage.assertCannotEditScriptField();
}

export function assertSessionIsPresentInSessionPage(testCaseViewPage: TestCaseViewPage) {
  const sessionPage = testCaseViewPage.showSessions();
  sessionPage.assertExists();
  sessionPage.grid.assertRowCount(1);
}

export function accessToSecondLevelViewPageOfTestCase(
  requirementWorkspace: RequirementWorkspacePage,
  requirementViewPage: RequirementViewPage,
  testcaseSecondLevelViewPage: TestCaseViewSubPage,
  originNodeName: string,
  nodeName: string,
  testCaseName: string,
) {
  selectNodeInAnotherNode(requirementWorkspace, originNodeName, nodeName);
  requirementViewPage.currentVersion.verifyingTestCaseTable
    .findRowId('name', testCaseName)
    .then((idTestCase) => {
      requirementViewPage.currentVersion.verifyingTestCaseTable.scrollPosition(
        'center',
        'mainViewport',
      );
      requirementViewPage.currentVersion.verifyingTestCaseTable
        .getCell(idTestCase, 'name')
        .linkRenderer()
        .findCellLink()
        .click({ force: true });
    });
  testcaseSecondLevelViewPage.assertExists();
}

export function copyAndPasteTestCaseInOtherProject(
  testCaseWorkspaceTree: TreeElement,
  testCaseWorkspaceTreeToolBar: TreeToolbarElement,
  sourceProjectName: string,
  destinationProjectName: string,
  useShortcut: boolean,
  withConfirmDialog: boolean,
) {
  testCaseWorkspaceTree.findRowId('NAME', sourceProjectName).then((idTestCase) => {
    testCaseWorkspaceTree.selectNode(idTestCase);
    testCaseWorkspaceTreeToolBar.copy(useShortcut);
  });
  testCaseWorkspaceTree.findRowId('NAME', destinationProjectName).then((idProject) => {
    testCaseWorkspaceTree.selectNode(idProject);
    testCaseWorkspaceTreeToolBar.performPasteCommand(useShortcut);
    if (withConfirmDialog) {
      const confirmDialogPasteInterProject = new ConfirmInterProjectPaste(
        'confirm-inter-project-paste',
      );
      confirmDialogPasteInterProject.confirm();
    }
  });
}

export function assertCannotUnlinkRequirementFromClassicTestCaseStep(
  testCaseViewPage: TestCaseViewPage,
) {
  const testCaseStepsView = testCaseViewPage.clickStepsAnchorLink();
  const actionStepElement = testCaseStepsView.getActionStepByIndex(0);
  actionStepElement.extendStep();
  actionStepElement.assertDeleteIconRequirementTestStepLinkNotExist();
}

export function assertNodeExistAndParentWithOptionalRegex(
  workspaceTree: TreeElement,
  listNodes: string[],
  parentId: string,
  useRegExp: boolean,
) {
  listNodes.forEach((testCaseName) => {
    const nodeName = useRegExp ? createRegExpForCopyNode(testCaseName) : testCaseName;

    workspaceTree.findRowId('NAME', nodeName).then((idTestCase) => {
      workspaceTree.assertNodeExist(idTestCase);
      workspaceTree.assertRowHasParent(idTestCase, parentId);
    });
  });
}

export function linkRequirementToTestStepAndAssert(
  testCaseViewPage: TestCaseViewPage,
  testStepView: TestStepsViewPage,
  index: number,
  OriginNodeName: string,
  ReqName: string,
) {
  testCaseViewPage.clickStepsAnchorLink();
  testStepView.getActionStepByIndex(index).extendStep();
  testStepView.addRequirementCoverageToStep(index, OriginNodeName, ReqName);
  testStepView.getActionStepByIndex(index).assertRequirementLinkedToTestStep();
}

export function linkRequirementBySecondLevelViewPageInClassicTestStep(
  requirementViewPage: RequirementViewPage,
  requirementWorkspace: RequirementWorkspacePage,
  testCaseSubViewPage: TestCaseViewSubPage,
  testStepView: TestStepsViewPage,
  originNodeName: string,
  nodeName: string,
  testCaseName: string,
  reqName: string,
) {
  accessToTestCaseSecondLevelViewPageFromRequirementPage(
    requirementViewPage,
    requirementWorkspace,
    testCaseSubViewPage,
    originNodeName,
    nodeName,
    testCaseName,
  );
  linkRequirementToTestStepAndAssert(
    testCaseSubViewPage.testCaseViewPage,
    testStepView,
    0,
    originNodeName,
    reqName,
  );
}

export function unlinkRequirementFromSecondLevelViewPageInTestCase(
  requirementViewPage: RequirementViewPage,
  requirementWorkspace: RequirementWorkspacePage,
  testCaseSubViewPage: TestCaseViewSubPage,
  originNodeName: string,
  reqName: string,
  testCaseName: string,
) {
  accessToTestCaseSecondLevelViewPageFromRequirementPage(
    requirementViewPage,
    requirementWorkspace,
    testCaseSubViewPage,
    originNodeName,
    reqName,
    testCaseName,
  );
  testCaseSubViewPage.testCaseViewPage.clickVerifiedRequirementsAnchorLink();
  testCaseSubViewPage.testCaseViewPage.deleteRequirementCoverageWithTopIcon(reqName);
}

export function linkRequirementBySecondLevelViewPageInTestCase(
  requirementViewPage: RequirementViewPage,
  requirementWorkspace: RequirementWorkspacePage,
  testCaseSubViewPage: TestCaseViewSubPage,
  originNodeName: string,
  nodeName: string,
  testCaseName: string,
  reqName: string,
) {
  accessToTestCaseSecondLevelViewPageFromRequirementPage(
    requirementViewPage,
    requirementWorkspace,
    testCaseSubViewPage,
    originNodeName,
    nodeName,
    testCaseName,
  );
  testCaseSubViewPage.testCaseViewPage.linkRequirementToTestCaseAndAssert(reqName);
}

export function dissociateRequirementToTestCase(
  testCaseViewPage: TestCaseViewPage,
  initialRowCount: number,
  finalRowCount: number,
  nameRequirement: string,
) {
  testCaseViewPage.coveragesTable.assertRowCount(initialRowCount);
  testCaseViewPage.deleteRequirementCoverageWithEndOfLineIcon(nameRequirement);
  testCaseViewPage.coveragesTable.assertRowCount(finalRowCount);
}

export function assertCanDragAndDropFolderAndTestCases(
  testCaseWorkspaceTree: TreeElement,
  listNodes: string[],
  referenceDrag: string,
  nameDrag: string,
  nameDrop: string,
) {
  testCaseWorkspaceTree.findRowId('NAME', nameDrag).then((idProject) => {
    testCaseWorkspaceTree.openNode(idProject);
  });
  testCaseWorkspaceTree.selectNodesByName(listNodes);
  testCaseWorkspaceTree.findRowId('NAME', nameDrop).then((idDropFolder) => {
    testCaseWorkspaceTree.findRowId('NAME', referenceDrag).then((idExploratoryTestCase) => {
      testCaseWorkspaceTree.findRowId('NAME', nameDrag).then((idProject) => {
        testCaseWorkspaceTree.dragAndDropObjectInObjectInWorkspace(
          idExploratoryTestCase,
          idProject,
          idDropFolder,
          false,
        );
      });
    });
  });
  testCaseWorkspaceTree.assertRowsHaveParentByNames(listNodes, nameDrop);
}

export function dragAnDropFolderInOtherProject(
  testCaseWorkspaceTree: TreeElement,
  dialogConfirmProjectMove: ConfirmInterProjectMove,
  nameDrag: string,
  nameDrop: string,
) {
  testCaseWorkspaceTree.findRowId('NAME', nameDrag).then((idDropFolder) => {
    testCaseWorkspaceTree.beginDragAndDrop(idDropFolder);
    testCaseWorkspaceTree.findRowId('NAME', nameDrop).then((idSecondProject) => {
      testCaseWorkspaceTree.selectNode(idSecondProject);
      testCaseWorkspaceTree.dropIntoOtherProject(idSecondProject);
      dialogConfirmProjectMove.confirm();
      testCaseWorkspaceTree.assertRowHasParent(idDropFolder, idSecondProject);
    });
  });
}

export function dragAnDropTestCasesInOtherProject(
  testCaseWorkspaceTree: TreeElement,
  dialogConfirmProjectMove: ConfirmInterProjectMove,
  listNodes: string[],
  referenceDrag: string,
  nameDrop: string,
) {
  testCaseWorkspaceTree.selectNodesByName(listNodes);
  testCaseWorkspaceTree.findRowId('NAME', referenceDrag).then((idExploratoryTestCase) => {
    testCaseWorkspaceTree.beginDragAndDrop(idExploratoryTestCase);
  });
  testCaseWorkspaceTree.findRowId('NAME', nameDrop).then((idProject) => {
    testCaseWorkspaceTree.selectNode(idProject);
    testCaseWorkspaceTree.dropIntoOtherProject(idProject);
    dialogConfirmProjectMove.confirm();
  });
  testCaseWorkspaceTree.assertRowsHaveParentByNames(listNodes, nameDrop);
}

export function deleteTestCasesAndAssertDeleted(
  testCaseWorkspace: TestCaseWorkspacePage,
  testCaseWorkSpaceTree: TreeElement,
  nameNode: string,
  selectNodes: string[],
) {
  testCaseWorkSpaceTree.findRowId('NAME', nameNode).then((idProject) => {
    testCaseWorkSpaceTree.openNode(idProject);
  });
  testCaseWorkSpaceTree.selectNodesByName(selectNodes);
  testCaseWorkspace.clickDeleteButton(true);
  selectNodes.forEach((content) => {
    testCaseWorkSpaceTree.assertRowIdNotExist('NAME', content);
  });
}

export function deleteFolderContainingTestCasesAndAssertDeleted(
  testCaseWorkspaceTree: TreeElement,
  confirmDialog: SimpleDeleteConfirmDialogElement,
  nameFolder: string,
) {
  const deleteButtonShortcut = new KeyboardShortcuts();
  testCaseWorkspaceTree.selectNodeByName(nameFolder);
  deleteButtonShortcut.pressDelete();
  confirmDialog.clickOnConfirmButton();
  testCaseWorkspaceTree.assertRowIdNotExist('NAME', nameFolder);
}

export function createTestCaseByKind(nameItem: string, addAnother: boolean, kind: string) {
  const createTestCaseDialog = new CreateTestCaseDialog();
  createTestCaseDialog.assertExists();
  createTestCaseDialog.fillName(nameItem);
  createTestCaseDialog.changeTestCaseKind(kind);
  if (addAnother) {
    createTestCaseDialog.clickOnAddAnotherButton();
  } else {
    createTestCaseDialog.clickOnAddButton();
  }
}

export function createExploratoryTestCaseAndAssertTypeOfTestCases(
  exploratoryTestCase: TestCaseViewCharterPage,
  testCaseWorkSpaceTree: TreeElement,
  keywordTestCase: KeywordTestStepsViewPage,
  scriptedTestCaseViewPage: TestCaseViewScriptPage,
  testCaseViewPage: TestCaseViewPage,
  nameTestCaseExploratory: string,
  nameTestCaseKeyword: string,
  nameTestCaseScripted: string,
) {
  createTestCaseByKind(nameTestCaseExploratory, false, 'Exploratoire');
  testCaseViewPage.clickCharterAnchorLink();
  exploratoryTestCase.assertExists();
  testCaseWorkSpaceTree.findRowId('NAME', nameTestCaseKeyword).then((idTestCase) => {
    testCaseWorkSpaceTree.selectNode(idTestCase);
  });
  testCaseViewPage.clickKeywordStepsAnchorLink();
  keywordTestCase.assertExists();
  testCaseWorkSpaceTree.findRowId('NAME', nameTestCaseScripted).then((idTestCase) => {
    testCaseWorkSpaceTree.selectNode(idTestCase);
  });
  testCaseViewPage.clickScriptAnchorLink();
  scriptedTestCaseViewPage.assertExists();
}

export function createTestCaseFolder(
  testCaseWorkspace: TestCaseWorkspacePage,
  testCaseFolderViewPage: TestCaseFolderViewPage,
  nameProject: string,
  nameFolder: string,
) {
  selectProjectInTestCaseWorkspace(testCaseWorkspace, nameProject);
  const createFolderDialog = testCaseWorkspace.createObjectInTestCaseWorkspace('new-folder');
  createFolderDialog.assertIsReady();
  createFolderDialog.fillName(nameFolder);
  createFolderDialog.clickOnAddButton();
  createFolderDialog.assertNotExist();
  testCaseFolderViewPage.assertExists();
}

export function createTestCaseFromRequirementImport(
  testCaseWorkspace: TestCaseWorkspacePage,
  testCaseWorkSpaceTree: TreeElement,
  treeToolbarElement: TreeToolbarElement,
  nameProject: string,
  nameRequirement: string,
) {
  const requirementWorkspace = NavBarElement.navigateToRequirementWorkspace();
  selectNodeInAnotherNode(requirementWorkspace, nameProject, nameRequirement);
  treeToolbarElement.getButton('copy-button').clickWithoutSpan();
  NavBarElement.navigateToTestCaseWorkspace();
  selectProjectInTestCaseWorkspace(testCaseWorkspace, nameProject);
  testCaseWorkspace.createTestCaseFromImportedRequirement('Classique');
  testCaseWorkSpaceTree.findRowId('NAME', nameRequirement).then((idTestCase) => {
    testCaseWorkSpaceTree.assertNodeExist(idTestCase);
  });
}
export function assertCanAssociateRequirementToTestStepWithDragAndDrop(
  testCaseWorkspace: TestCaseWorkspacePage,
  testCaseViewPage: TestCaseViewPage,
  testStepView: TestStepsViewPage,
  originNodeName: string,
  nodeName: string,
  index: number,
  requirementName: string,
) {
  selectNodeInTestCaseWorkspace(testCaseWorkspace, originNodeName, nodeName);
  testCaseViewPage.clickStepsAnchorLink();
  testStepView.getActionStepByIndex(index).extendStep();
  testStepView.addRequirementCoverageToStep(index, originNodeName, requirementName);
}

export function linkRequirementToATestCase(
  testCaseViewPage: TestCaseViewPage,
  openNode: boolean,
  nameNode: string,
  requirementName: string,
  idTestCase: number,
) {
  const requirementLinkTree = testCaseViewPage.openRequirementDrawer();
  if (openNode) {
    requirementLinkTree.findRowId('NAME', nameNode).then((idProject) => {
      requirementLinkTree.openNode(idProject);
    });
  }
  requirementLinkTree.findRowId('NAME', requirementName).then((idRequirement) => {
    requirementLinkTree.beginDragAndDrop(idRequirement);
  });
  testCaseViewPage.dropRequirementIntoTestCase(idTestCase);
  testCaseViewPage.closeRequirementDrawer();
  testCaseViewPage.coveragesTable.findRowId('name', requirementName).then((idRequirement) => {
    testCaseViewPage.coveragesTable.assertRowExist(idRequirement);
  });
}

export function unlinkRequirementFromClassicTestCaseStep(
  testCaseViewPage: TestCaseViewPage,
  indexStep: number,
  indexRequirement: number,
) {
  const testCaseStepsView = testCaseViewPage.clickStepsAnchorLink();
  testCaseStepsView.getActionStepByIndex(indexStep).extendStep();
  testCaseStepsView.getActionStepByIndex(indexStep).removeLinkedRequirement(indexRequirement);
}

export function linkRequirementToATestStepInDetailStepViewPage(
  detailedStepViewPage: DetailedStepViewPage,
  projectName: string,
  requirementName: string,
) {
  const requirementTree = detailedStepViewPage.showAddRequirementCoverageDrawer();
  requirementTree.findRowId('NAME', projectName).then((idProject) => {
    requirementTree.openNode(idProject);
  });
  requirementTree.findRowId('NAME', requirementName).then((idRequirement) => {
    requirementTree.beginDragAndDrop(idRequirement);
  });
  detailedStepViewPage.dropRequirement(1, 1);
  detailedStepViewPage.closeRequirementDrawer();
  detailedStepViewPage.assertRequirementVersionIsCoveredByCurrentStep(1);
}

export function unlinkRequirementToATestStepInDetailStepViewPage(
  detailedStepViewPage: DetailedStepViewPage,
) {
  detailedStepViewPage.deleteCoverageRequirementInTestStep(1, 1);
  detailedStepViewPage.coverageTable.assertGridIsEmpty();
}

export function linkRequirementBySearch(
  detailedStepViewPage: DetailedStepViewPage,
  requirementName: string,
) {
  const requirementSearchCoveragePage =
    detailedStepViewPage.navigateToRequirementSearchForCoverage();
  requirementSearchCoveragePage.grid.assertSpinnerIsNotPresent();
  requirementSearchCoveragePage.grid.findRowId('name', requirementName).then((idRequirement) => {
    requirementSearchCoveragePage.grid.selectRow(idRequirement, '#', 'leftViewport');
  });
  requirementSearchCoveragePage.clickLinkSelectionButton();
  detailedStepViewPage.coverageTable
    .getRow('1')
    .cell('name')
    .findCellTextSpan()
    .should('contain', requirementName);
}
