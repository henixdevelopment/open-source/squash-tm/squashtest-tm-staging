import { AutomationProgrammerGlobalPage } from '../../page-objects/pages/automation-workspace/automation-programmer/automation-programmer-global.page';

import { FunctionalTesterReadyToTransmitPage } from '../../page-objects/pages/automation-workspace/functional-tester-workspace/functional-tester-ready-to-transmit.page';

export function clickCellInGrid(
  automationGlobalPage: AutomationProgrammerGlobalPage | FunctionalTesterReadyToTransmitPage,
  cellId: string,
  cellContent: string,
  targetCell: string,
) {
  automationGlobalPage.grid.findRowId(cellId, cellContent).then((idExecution) => {
    automationGlobalPage.grid.getCell(idExecution, targetCell).findCellTextSpan().click();
  });
}

export function typeCellInGrid(
  automationGlobalPage: AutomationProgrammerGlobalPage | FunctionalTesterReadyToTransmitPage,
  cellId: string,
  cellContent: string,
  targetCell: string,
  value: string,
) {
  automationGlobalPage.grid.findRowId(cellId, cellContent).then((idExecution) => {
    automationGlobalPage.grid
      .getCell(idExecution, targetCell)
      .findCellTextSpan()
      .type(value + '{enter}');
  });
}
export function verifyCellInGrid(
  automationGlobalPage: AutomationProgrammerGlobalPage | FunctionalTesterReadyToTransmitPage,
  cellId: string,
  cellContent: string,
  targetCell: string,
  expectedCaseValue: string,
) {
  automationGlobalPage.grid.findRowId(cellId, cellContent).then((idExecution) => {
    automationGlobalPage.grid
      .getCell(idExecution, targetCell)
      .findCellTextSpan()
      .should('contain', expectedCaseValue);
  });
}
