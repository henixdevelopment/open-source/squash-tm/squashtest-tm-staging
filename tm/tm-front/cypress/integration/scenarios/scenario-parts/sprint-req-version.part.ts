import { SprintReqVersionViewModel } from '../../../../projects/sqtm-core/src/lib/model/campaign/sprint-req-version-view-model';
import { SprintReqVersionViewPage } from '../../page-objects/pages/campaign-workspace/sprint-req-version/sprint-req-version-view.page';
import { GridResponse } from '../../../../projects/sqtm-core/src/lib/model/grids/grid-response.model';
import { createBasicTreeWithOneSprint } from '../back-mocked/campaign-workspace/campaign-workspace-mock-data';
import { CampaignWorkspacePage } from '../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import { TreeElement } from '../../page-objects/elements/grid/grid.element';
import { SprintViewPage } from '../../page-objects/pages/campaign-workspace/sprint/sprint-view.page';
import { SprintReqVersionModel } from '../../../../projects/sqtm-core/src/lib/model/campaign/sprint-req-version-model';
import { SprintModel } from '../../../../projects/sqtm-core/src/lib/model/campaign/sprint-model';
import { mockSprintModel } from '../../data-mock/sprint.data-mock';
import { SprintReqVersionTestPlanItem } from '../../../../projects/sqtm-core/src/lib/model/campaign/sprint-req-version-test-plan-item.model';
import { mockGridResponse } from '../../data-mock/grid.data-mock';
import { GridColumnId } from '../../../../projects/sqtm-core/src/lib/shared/constants/grid/grid-column-id';
import { mockSprintReqVersionViewModel } from '../../data-mock/sprint-req-version.data-mock';

export function navigateToSprintReqVersion(
  sprintReqVersionViewModel: SprintReqVersionViewModel,
): SprintReqVersionViewPage {
  const tree = initializeCampaignTree();

  const sprintViewPage: SprintViewPage = tree.selectNode(
    'Sprint-1',
    getSprintModel(sprintReqVersionViewModel),
  );
  sprintViewPage.assertExists();

  sprintViewPage.clickSprintReqVersionsAnchorLink();
  return sprintViewPage.openSprintReqVersionPageByName(
    sprintReqVersionViewModel,
    initialTestPlan,
    null,
  );
}

export function initializeCampaignTree() {
  const initialNodes: GridResponse = createBasicTreeWithOneSprint();
  const campaignWorkspacePage: CampaignWorkspacePage =
    CampaignWorkspacePage.initTestAtPage(initialNodes);

  const tree: TreeElement = campaignWorkspacePage.tree;
  tree.assertNodeExist('CampaignLibrary-1');
  tree.assertNodeTextContains('CampaignLibrary-1', 'Project1');
  tree.assertNodeExist('Sprint-1');
  tree.assertNodeTextContains('Sprint-1', 'Sprint 1');
  return tree;
}

export function getSprintModel(sprintReqVersionModel: SprintReqVersionModel): SprintModel {
  return mockSprintModel({
    sprintReqVersions: [sprintReqVersionModel],
  });
}

export const initialTestPlan: GridResponse<SprintReqVersionTestPlanItem> = mockGridResponse(
  GridColumnId.testPlanItemId,
  [],
);

export const nativeSprintReqVersionModel = mockSprintReqVersionViewModel({
  name: 'exigence01',
  validationStatus: 'TO_BE_TESTED',
});
