const mariadb = require('mariadb');
const {getDatabaseInfos} = require("./database-info");

let pool;

function retrievePool(cypressConfig) {
  if(!pool){
    const poolOptions = {
      ...getDatabaseInfos(cypressConfig),
      connectionLimit: 5,
      multipleStatements: true
    };
    pool = mariadb.createPool(poolOptions);
  }
  return pool;
}

function executeQuery(query, cypressConfig) {
  const pool = retrievePool(cypressConfig);
  return pool.query(query);
}

function executeSelectQuery(query, cypressConfig) {
  const pool = retrievePool(cypressConfig);
  return pool.query(query).then(result => convertKeysIntoLowercase(result));
}

// In MariaDB, keys are in uppercase (lowercase for PG), then this method helps to convert keys into lowercase
function convertKeysIntoLowercase(keys) {
  return Object.fromEntries(Object.entries(keys).map(([key, val]) => {
    const values = Object.fromEntries(Object.entries(val).map(([valKey, valVal]) => {
      return [valKey.toLowerCase(), valVal];
    }));
    return [key, values];
  }));
}

module.exports = {
  executeQuery: executeQuery,
  executeSelectQuery: executeSelectQuery
};
