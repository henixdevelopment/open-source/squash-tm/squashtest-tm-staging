const mariadbExecuteQuery = require('./mariadb-execute-query');
const pgExecuteQuery = require('./pg-execute-query');
const {cleanupScript} = require('./cleanup-script');
const {isPostgresql} = require("../utils/testing-context");


module.exports = (on, config) => {
  require('@cypress/code-coverage/task')(on, config);
  on('task', {
    // in cypress task are identified by their names that must coincide with the function name in task object
    cleanDatabase() {
      return isPostgresql(config) ?
        pgExecuteQuery.executeQuery(cleanupScript, config)
        : mariadbExecuteQuery.executeQuery(cleanupScript, config);
    },
    executeQuery(query) {
      return isPostgresql(config) ?
        pgExecuteQuery.executeQuery(query, config) : mariadbExecuteQuery.executeQuery(query, config);
    },
    executeSelectQuery(query) {
      return isPostgresql(config) ?
        pgExecuteQuery.executeSelectQuery(query, config) : mariadbExecuteQuery.executeSelectQuery(query, config);
    }
  });
  on('before:browser:launch', (browser, launchOptions) => {
    if (browser.family === 'chromium' && browser.name !== 'electron') {
      launchOptions.preferences.default.intl = {"accept_languages": 'fr'};
      return launchOptions;
    }
  });
  return config;
};
