const {getDatabaseInfos} = require("./database-info");
const {Client} = require('pg');

async function executeQuery(query, cypressConfig) {
  const client = new Client(getDatabaseInfos(cypressConfig));
  await client.connect();
  return client.query(query).finally(() => {
    client.end();
  });
}

async function executeSelectQuery(query, cypressConfig) {
  const client = new Client(getDatabaseInfos(cypressConfig));
  await client.connect();
  return client.query(query)
    .then(result => result.rows || [])
    .finally(() => {
      client.end();
    });
}

module.exports = {
  executeQuery: executeQuery,
  executeSelectQuery: executeSelectQuery
};
