import { defineConfig } from 'cypress';

export default defineConfig({
  env: {
    backend: 'mocked',
    apiBaseUrl: 'backend',
    appBaseUrl: '',
    locale: 'FR',
  },
  video: false,
  projectId: 'uc8ymm',
  e2e: {
    retries: {
      runMode: 2,
      openMode: 0,
    },
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents(on, config) {
      return require('./cypress/plugins/index.js')(on, config);
    },
    baseUrl: 'http://localhost:3000',
    specPattern: 'cypress/integration/scenarios/back-mocked/**/*.{js,jsx,ts,tsx}',
    reporter: 'cypress-multi-reporters',
    reporterOptions: {
      reporterEnabled: 'mochawesome, mocha-junit-reporter',
      mochaJunitReporterReporterOptions: {
        mochaFile: 'cypress/results/back-mocked/junit/results-[hash].xml',
      },
      reporterOptions: {
        overwrite: false,
        json: true,
        html: false,
      },
    },
  },
});
