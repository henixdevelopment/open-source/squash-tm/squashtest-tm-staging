const fs = require('fs');
const path = require('path');

function generateIconDefinitions() {
  const iconRootFolder = 'projects/sqtm-core/src/assets/sqtm-core/icons/assets/';
  const iconRootFolderPath = path.join(__dirname , iconRootFolder);
  console.log(`Generating icon library from ${iconRootFolderPath}`);
  const iconDefinitions = [];

  fs.readdirSync(iconRootFolderPath).forEach(iconFolder => {
    const iconFolderPath = path.join(iconRootFolderPath, iconFolder);
    fs.readdirSync(iconFolderPath).forEach(icon => {
      const iconName = icon.replace('.svg', '');
      const iconPath = path.join(iconFolderPath, icon);
      const content = fs.readFileSync(iconPath);
      iconDefinitions.push({id: `${iconFolder}:${iconName}`, content})
    })
  });
  return iconDefinitions;
}

function buildGeneratedIconCode(iconDefinitions) {
  let template = '// tslint:disable:max-line-length\n';
  template = template.concat('export const sqtmIconLib = [\n')
  iconDefinitions.forEach(({id, content}) => {
    template = template.concat(`{id: '${id}', content: \`${content}\`},\n`,);
  })
  template = template.concat('];\n')
  return template;
}

function writeGeneratedIconCodeFile(template) {
  let generatedIconFolderPath = 'projects/sqtm-core/src/lib/core/generated-icons/';
  generatedIconFolderPath = path.join(__dirname , generatedIconFolderPath);
  if (!fs.existsSync(generatedIconFolderPath)) {
    fs.mkdirSync(generatedIconFolderPath);
  }
  const generatedIconFilePath = path.join(generatedIconFolderPath, 'icons.ts');
  fs.writeFileSync(generatedIconFilePath, template);
  console.log(`Wrote generated icons def in ${generatedIconFilePath}`);
}

/**
 * This function generate a ts file containing an array of iconDef from the ./projects/sqtm-core/src/assets/sqtm-core/icons/assets folder content.
 * IconDef: {id:string, content:string}
 * id = the icon identifier aka 'sqtm-core-generic:edit'. The prefix is folder name, ths suffix is icon name. As required per ng-zorro icon namespace API
 * content = the content of the svg file.
 */
function generateIcons() {
  const iconDefinitions = generateIconDefinitions();
  console.log(`Found ${iconDefinitions.length} icon svg files.`);
  const template = buildGeneratedIconCode(iconDefinitions);
  writeGeneratedIconCodeFile(template);
}

function generateCode() {
  console.log('Begin code generation phase');
  generateIcons();
  console.log('End code generation phase');
}

generateCode();
