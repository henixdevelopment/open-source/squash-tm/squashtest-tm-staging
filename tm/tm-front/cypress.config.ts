import { defineConfig } from 'cypress';

export default defineConfig({
  env: {
    backend: 'mocked',
    apiBaseUrl: '/squash/backend',
    appBaseUrl: '/squash',
    locale: 'FR',
  },
  video: false,
  projectId: 'uc8ymm',
  e2e: {
    setupNodeEvents(on, config) {
      return require('./cypress/plugins/index.js')(on, config);
    },
    baseUrl: 'http://localhost:4200/squash',
    specPattern: 'cypress/integration/scenarios/back-mocked/**/*.{js,jsx,ts,tsx}',
  },
});
