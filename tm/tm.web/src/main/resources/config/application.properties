# SQUASH TM INTERNAL APPLICATION PROPERTIES
# Please add new propertiess in alphabetical order

# APPLICATION INFORMATION
info.app.name=Squash TM
info.app.version=@project.version@

# LOGGING
#logging.file=${logging.path}/squash-tm.log
logging.file.path=${squash.path.root}/logs
#logging.config=${spring.config.location}/log4j2.yml

# SECURITY
# /!\ The following url will totally ignore the security filter chain
# 2018/07/23 : security.ignored is deprecated, now we handle it by ourselves in WebSecurityConfig
squash.security.ignored=/images/**, /styles/**, /scripts/**, /static/**, /**/favicon.ico, \
  /isSquashAlive, /accessDenied, /localization/filler, \
  /WEB-INF/**/*.jsp
# Issue 6900. Choose between UT-8, ISO-8859-1, etc
squash.security.basic.token-charset = ISO-8859-1

# Preferred url to authenticate the application
squash.security.preferred-auth-url = /login

# Disabling spring boot actuators
management.endpoints.enabled-by-default = false

# EMBEDDED SERVER CONFIGURATION
server.servlet.context-path=/squash
# Using proxy headers
server.forward-headers-strategy=framework

# session timeout in seconds
server.servlet.session.timeout=3600
server.tomcat.accesslog.enabled=true
server.tomcat.basedir=${squash.path.root}/tomcat-work

spring.config.location=../conf
# Too much of this circular dep for cleaning now
# Maybe using constructor injection rather that dirty field injection would have preserved us from that...
spring.main.allow-circular-references=true

# DATASOURCE
# false = don't populate DB
spring.datasource.initialization-mode=never
spring.datasource.hikari.maximumPoolSize = 20

# JACKSON
# Object Mapper configuration
# note : WRITE_DATE_AS_TIMESTAMPS apparently obsolete since Spring Boot 2, leaving it here in case it's still needed
# spring.jackson.serialization.WRITE_DATES_AS_TIMESTAMPS=false
spring.jackson.serialization.WRITE_DATE_KEYS_AS_TIMESTAMPS = false

# INTERNATIONALIZATION
# Note that wilcards are not allowed !
spring.messages.basename=file:${squash.path.languages-path}/core/messages, \
  /WEB-INF/messages/core/messages, \
  /WEB-INF/messages/tm/messages, \
  org/springframework/security/messages

# SPRING RESOURCES HANDLING
# google says static resources should be cached 1 week
spring.web.resources.cache.period=604800

# JCACHE
spring.cache.jcache.config=classpath:ehcache.xml

# SPRING RESOURCE RESOLVERS
spring.resource-resolvers.cache=true

# THYMELEAF
# '.html' suffix is already in view name
spring.thymeleaf.prefix=/WEB-INF/templates/
spring.thymeleaf.suffix=
spring.thymeleaf.view-names=*.html
# This is required so that thymeleaf resolver doesn't pick up a jsp when it's called by content negociation resolver
spring.thymeleaf.excluded-view-names=page/*,fragment/*,redirect:*,forward:*

# SPRING MVC
spring.mvc.view.prefix=/WEB-INF/jsp/
spring.mvc.view.suffix=.jsp
spring.mvc.pathmatch.matching-strategy=ant_path_matcher

# SQUASH TM SPECIFIC PROPERTIES
squash.path.root=..
squash.path.bundles-path=${squash.path.root}/bundles
squash.path.plugins-path=${squash.path.root}/plugins
squash.path.languages-path=${squash.path.root}/conf/lang
squash.path.file.repository=${squash.path.root}/attachments
# This one is just an alias. Do not ever change it.
squash.path.config-path=${spring.config.location}


# STORED CREDENTIAL (set to void here on purpose)
squash.crypto.secret = JeSuisUnMotDePasse

# TM-TA CONFIGURATION
tm.test.automation.pollinterval.millis=3000

# This remove MultipartAutoConfiguration. PLEASE DO NOT CHANGE THIS.
spring.servlet.multipart.enabled=false

# HIBERNATE

spring.jpa.generate-ddl=false
spring.jpa.hibernate.ddl-auto=none

# Hibernate batch disabled for now because it fails when deleting entities that have extenders
# Anyway the code is not ready for batch insertion (overfrequent flushes, incompatible entity identity generator strategies etc)
#spring.jpa.properties.hibernate.jdbc.batch_size = 100
spring.jpa.properties.hibernate.order_inserts   = true
spring.jpa.properties.hibernate.order_updates   = true
spring.jpa.properties.hibernate.jdbc.fetch_size = 400


spring.jpa.hibernate.naming.physical-strategy=org.squashtest.tm.infrastructure.hibernate.UppercaseUnderscorePhysicalNaming
spring.jpa.properties.hibernate.current_session_context_class=org.springframework.orm.hibernate5.SpringSessionContext
spring.jpa.properties.hibernate.session_factory.interceptor=org.squashtest.tm.service.internal.hibernate.AuditLogInterceptor
spring.jpa.properties.hibernate.cache.use_second_level_cache=true
spring.jpa.properties.hibernate.cache.region.factory_class=jcache
spring.jpa.properties.hibernate.javax.cache.provider=org.ehcache.jsr107.EhcacheCachingProvider
spring.jpa.properties.hibernate.cache.use_query_cache=true

#squash.external.synchronisation.delay =
#squash.external.synchronisation.max-items-per-sync =

# LIQUIBASE
# Property responsible to activate liquibase on application startup.
# Prior to 4.0, we had to publish only manual scripts and thus databasechangelog table could be corrupted.
# So enabling this functionality on old instances can cause weird behavior and should be carefully tested
# before enabling on production database.
# !!! ALWAYS DUMP YOUR DATABASE BEFORE ACTIVATING THIS, AND BEFORE EACH SQUASH-TM UPGRADE !!!
# You have been warned...
spring.liquibase.enabled=false
spring.liquibase.change-log=classpath:global.changelog-master.xml
spring.liquibase.labels=!legacy

# API TOKEN AUTH CONFIGURATION
squash.rest-api.disallow-basic-authentication=false

# The secret used to encrypt JWT tokens encoded in base64.
# This secret must have at least 512 bits and be base64 encoded.
# This represents a character string of at least 86 characters.
#squash.rest-api.jwt.secret=

squash.project-imports.folder-path=${squash.path.root}/imports
#squash.project-imports.delay =

#squash.xls-imports.max-concurrent-imports =
#squash.xls-imports.max-test-cases-per-import =
#squash.xls-imports.max-test-steps-per-import =
#squash.xls-imports.max-requirements-per-import =


# REPORT CUSTOM TEMPLATE
squash.report-custom-template.folder-path=${spring.config.location}/report.custom.templates
