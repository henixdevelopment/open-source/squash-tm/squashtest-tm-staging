/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.bugtracker;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.csp.core.bugtracker.spi.DefaultOAuth2FormValues;
import org.squashtest.tm.service.display.bugtracker.BugTrackerDisplayService;
import org.squashtest.tm.service.internal.display.dto.BugTrackerViewDto;
import org.squashtest.tm.service.internal.servers.OAuth2ConsumerService;

@RestController
@RequestMapping("/backend/bugtracker-view")
public class BugTrackerViewController {

    private final BugTrackerDisplayService bugTrackerDisplayService;
    private final OAuth2ConsumerService oAuth2ConsumerService;

    BugTrackerViewController(
            BugTrackerDisplayService bugTrackerDisplayService,
            OAuth2ConsumerService oAuth2ConsumerService) {
        this.bugTrackerDisplayService = bugTrackerDisplayService;
        this.oAuth2ConsumerService = oAuth2ConsumerService;
    }

    @GetMapping("/{bugTrackerId}")
    BugTrackerViewDto getBugTrackerView(@PathVariable Long bugTrackerId) {
        return this.bugTrackerDisplayService.getBugTrackerView(bugTrackerId);
    }

    @GetMapping(value = "/{bugTrackerId}/oauth2/default-form-values")
    public DefaultOAuth2FormValues askOauth2Token(@PathVariable("bugTrackerId") long bugTrackerId) {
        return oAuth2ConsumerService.getDefaultOAuth2FormValue(bugTrackerId);
    }
}
