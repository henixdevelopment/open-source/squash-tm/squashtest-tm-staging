/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.artificialintelligence.testcasegeneration;

import java.util.Collections;
import java.util.Map;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.artificialintelligence.testcasegeneration.AiTestCaseGenerationService;
import org.squashtest.tm.service.internal.dto.GenerateTestCasesDataDto;
import org.squashtest.tm.service.internal.dto.TestCasesCreationFromAiDto;
import org.squashtest.tm.service.internal.library.EntityPathHeaderService;

@RestController
@RequestMapping("backend/ai-test-case-generation")
public class AiTestCaseGenerationController {

    private final AiTestCaseGenerationService aiTestCaseGenerationService;
    private final EntityPathHeaderService entityPathHeaderService;

    public AiTestCaseGenerationController(
            AiTestCaseGenerationService aiTestCaseGenerationService,
            EntityPathHeaderService entityPathHeaderService) {
        this.aiTestCaseGenerationService = aiTestCaseGenerationService;
        this.entityPathHeaderService = entityPathHeaderService;
    }

    @PostMapping(value = "api-call")
    public String generateTestCaseFromRequirementUsingAi(
            @RequestBody GenerateTestCasesDataDto generateTestCasesDataDto) {
        return aiTestCaseGenerationService.generateTestCaseFromRequirementUsingAiUserSide(
                generateTestCasesDataDto.getSourceProjectId(),
                generateTestCasesDataDto.getRequirementDescription());
    }

    @GetMapping(value = "folder-path/{chosenFolderTclnId}")
    public Map<String, String> getChosenFolderFullPath(@PathVariable long chosenFolderTclnId) {
        String path = entityPathHeaderService.buildTCLNPathHeader(chosenFolderTclnId);
        return Collections.singletonMap("folderPath", path);
    }

    @PostMapping(value = "create-test-cases/{requirementVersionId}")
    public void persistTestCasesGeneratedByAi(
            @PathVariable Long requirementVersionId,
            @RequestBody TestCasesCreationFromAiDto testCasesData) {
        Long testCaseLibraryId =
                aiTestCaseGenerationService.findTargetTestCaseLibraryId(testCasesData.getTargetProjectId());
        aiTestCaseGenerationService.persistTestCaseFromAi(
                testCaseLibraryId, requirementVersionId, testCasesData);
    }
}
