/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.referential;

import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.display.referential.ReferentialDataProvider;
import org.squashtest.tm.service.internal.display.referential.AdminReferentialData;
import org.squashtest.tm.service.internal.display.referential.ReferentialData;
import org.squashtest.tm.service.internal.display.referential.SynchronisationPluginDto;
import org.squashtest.tm.service.internal.display.referential.WorkspaceWizardDto;
import org.squashtest.tm.web.backend.manager.synchronisation.SynchronisationPluginManager;
import org.squashtest.tm.web.backend.manager.wizard.WorkspaceWizardManager;
import org.squashtest.tm.web.backend.report.IdentifiedReportDecorator;
import org.squashtest.tm.web.backend.report.ReportsRegistry;

@RestController
@RequestMapping("backend/referential")
public class ReferentialDataController {

    private final ReferentialDataProvider referentialDataProvider;
    private final WorkspaceWizardManager workspaceWizardManager;
    private final SynchronisationPluginManager synchronisationPluginManager;
    private final ReportsRegistry reportsRegistry;

    public ReferentialDataController(
            ReferentialDataProvider referentialDataProvider,
            ReportsRegistry reportsRegistry,
            WorkspaceWizardManager workspaceWizardManager,
            SynchronisationPluginManager synchronisationPluginManager) {
        this.referentialDataProvider = referentialDataProvider;
        this.workspaceWizardManager = workspaceWizardManager;
        this.reportsRegistry = reportsRegistry;
        this.synchronisationPluginManager = synchronisationPluginManager;
    }

    @GetMapping
    public ReferentialData get() {
        ReferentialData referentialData = referentialDataProvider.findReferentialData();
        appendWorkspaceWizards(referentialData);
        appendSynchronisationPlugins(referentialData);
        return referentialData;
    }

    private void appendWorkspaceWizards(ReferentialData referentialData) {
        List<WorkspaceWizardDto> workspaceWizards =
                workspaceWizardManager.findAll().stream()
                        .map(WorkspaceWizardDto::fromWorkspaceWizard)
                        .toList();

        referentialData.setWorkspaceWizards(workspaceWizards);
    }

    private void appendSynchronisationPlugins(ReferentialData referentialData) {
        List<SynchronisationPluginDto> synchronisationPlugins =
                synchronisationPluginManager.findAll().stream()
                        .map(SynchronisationPluginDto::fromSynchronizationPlugin)
                        .toList();

        referentialData.setSynchronizationPlugins(synchronisationPlugins);
    }

    @GetMapping(path = "/admin")
    public AdminReferentialData getAdminReferentialData() {
        AdminReferentialData referentialData = this.referentialDataProvider.findAdminReferentialData();
        setAvailableReports(referentialData);
        appendSynchronisationPlugins(referentialData);
        return referentialData;
    }

    private void setAvailableReports(AdminReferentialData referentialData) {
        List<String> availableTemplatableReportIds =
                this.reportsRegistry.getSortedReports().stream()
                        .filter(IdentifiedReportDecorator::isDocxTemplate)
                        .map(IdentifiedReportDecorator::extractReportDataId)
                        .toList();
        referentialData.setAvailableReportIds(availableTemplatableReportIds);
    }

    private void appendSynchronisationPlugins(AdminReferentialData referentialData) {
        List<SynchronisationPluginDto> synchronisationPlugins =
                synchronisationPluginManager.findAll().stream()
                        .map(SynchronisationPluginDto::fromSynchronizationPlugin)
                        .toList();

        referentialData.setSynchronizationPlugins(synchronisationPlugins);
    }
}
