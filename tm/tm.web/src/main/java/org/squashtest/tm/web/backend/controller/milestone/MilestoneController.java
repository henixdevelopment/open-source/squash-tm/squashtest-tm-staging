/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.milestone;

import static org.squashtest.tm.api.security.acls.Roles.ROLE_ADMIN;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.milestone.MilestoneRange;
import org.squashtest.tm.domain.milestone.MilestoneStatus;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.exception.NameAlreadyInUseException;
import org.squashtest.tm.exception.milestone.MilestoneLabelAlreadyExistsException;
import org.squashtest.tm.exception.milestone.MilestoneRangeNotFoundException;
import org.squashtest.tm.service.display.milestone.MilestoneDisplayService;
import org.squashtest.tm.service.internal.display.dto.MilestoneDto;
import org.squashtest.tm.service.internal.display.dto.MilestonePossibleOwnerDto;
import org.squashtest.tm.service.internal.display.dto.milestone.MilestoneDuplicationModel;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.milestone.MilestoneBindingManagerService;
import org.squashtest.tm.service.milestone.MilestoneManagerService;
import org.squashtest.tm.service.project.ProjectFinder;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.user.UserAccountService;
import org.squashtest.tm.service.user.UserAdministrationService;
import org.squashtest.tm.web.backend.controller.form.model.MilestoneFormModel;

@RestController
@RequestMapping("/backend/milestones")
public class MilestoneController {
    private static final String LABEL = "label";
    private static final String MILESTONE = "Milestone";

    private final MilestoneDisplayService milestoneDisplayService;
    private final MilestoneManagerService milestoneManager;
    private final PermissionEvaluationService permissionEvaluationService;
    private final ProjectFinder projectFinder;
    private final UserAdministrationService userAdministrationService;
    private final UserAccountService userService;
    private final MilestoneBindingManagerService milestoneBindingManager;

    @Inject
    MilestoneController(
            MilestoneDisplayService milestoneDisplayService,
            MilestoneManagerService milestoneManager,
            PermissionEvaluationService permissionEvaluationService,
            ProjectFinder projectFinder,
            UserAdministrationService userAdministrationService,
            UserAccountService userService,
            MilestoneBindingManagerService milestoneBindingManager) {
        this.milestoneDisplayService = milestoneDisplayService;
        this.milestoneManager = milestoneManager;
        this.permissionEvaluationService = permissionEvaluationService;
        this.projectFinder = projectFinder;
        this.userAdministrationService = userAdministrationService;
        this.userService = userService;
        this.milestoneBindingManager = milestoneBindingManager;
    }

    @PostMapping
    public GridResponse getAllMilestones(@RequestBody GridRequest request) {
        return milestoneDisplayService.findAll(request);
    }

    @DeleteMapping("/{milestoneIds}")
    public void deleteMilestone(@PathVariable("milestoneIds") List<Long> milestoneIds) {
        milestoneManager.removeMilestones(milestoneIds);
    }

    @PostMapping("/new")
    public Map<String, Object> addMilestone(
            @Valid @RequestBody MilestoneFormModel milestoneFormModel) {
        Map<String, Object> response = new HashMap<>();
        Milestone milestone = milestoneFormModel.getMilestone();
        setRange(milestone);
        setPerimeter(milestone);

        try {
            milestoneManager.addMilestone(milestone);
            response.put("id", milestone.getId());
        } catch (MilestoneLabelAlreadyExistsException ex) {
            throw new NameAlreadyInUseException(MILESTONE, milestoneFormModel.getLabel(), LABEL, ex);
        }

        return response;
    }

    private void setRange(Milestone milestone) {
        if (permissionEvaluationService.hasRole(ROLE_ADMIN)) {
            milestone.setRange(MilestoneRange.GLOBAL);
        } else {
            milestone.setRange(MilestoneRange.RESTRICTED);
        }
    }

    private void setPerimeter(Milestone milestone) {
        if (!permissionEvaluationService.hasRole(ROLE_ADMIN)) {
            List<GenericProject> projects = projectFinder.findAllICanManage();
            milestone.addProjectsToPerimeter(projects);
        }
    }

    @PostMapping("/{milestoneId}/label")
    public void changeLabel(@PathVariable long milestoneId, @RequestBody MilestonePatch patch) {
        verifyCanEditMilestone(milestoneId);
        try {
            milestoneManager.changeLabel(milestoneId, patch.label());
        } catch (MilestoneLabelAlreadyExistsException ex) {
            throw new NameAlreadyInUseException(MILESTONE, patch.label(), LABEL, ex);
        }
    }

    @PostMapping("/{milestoneId}/status")
    public MilestoneDto changeStatus(
            @PathVariable("milestoneId") long milestoneId, @RequestBody MilestonePatch patch) {
        verifyCanEditMilestone(milestoneId);
        MilestoneStatus status = MilestoneStatus.valueOf(patch.status());
        milestoneManager.changeStatus(milestoneId, status);

        // A single service call would be more adapted here...
        if (status.equals(MilestoneStatus.PLANNED)) {
            milestoneManager.unbindAllObjects(milestoneId);
        }

        return milestoneDisplayService.getMilestoneView(milestoneId);
    }

    @PostMapping("/{milestoneId}/end-date")
    public MilestoneDto changeEndDate(
            @PathVariable("milestoneId") Long milestoneId, @RequestBody MilestonePatch patch) {
        verifyCanEditMilestone(milestoneId);
        milestoneManager.changeEndDate(milestoneId, patch.endDate());
        return milestoneDisplayService.getMilestoneView(milestoneId);
    }

    @PostMapping("/{milestoneId}/range")
    public MilestoneDto changeRange(
            @PathVariable("milestoneId") Long milestoneId, @RequestBody MilestonePatch patch) {
        milestoneManager.verifyCanEditMilestoneRange();
        checkRangeParameter(patch.range());

        if (MilestoneRange.RESTRICTED.name().equals(patch.range())
                && milestoneManager.isBoundToATemplate(milestoneId)) {
            milestoneBindingManager.unbindTemplateFrom(milestoneId);
        }

        milestoneManager.changeOwner(milestoneId, userService.findCurrentUser());
        milestoneManager.changeRange(milestoneId, MilestoneRange.valueOf(patch.range()));
        return milestoneDisplayService.getMilestoneView(milestoneId);
    }

    private void checkRangeParameter(String range) {
        try {
            MilestoneRange.valueOf(range);
        } catch (IllegalArgumentException ex) {
            throw new MilestoneRangeNotFoundException();
        }
    }

    @PostMapping("/{milestoneId}/description")
    public MilestoneDto changeDescription(
            @PathVariable("milestoneId") Long milestoneId, @RequestBody MilestonePatch patch) {
        verifyCanEditMilestone(milestoneId);
        milestoneManager.changeDescription(milestoneId, patch.description());
        return milestoneDisplayService.getMilestoneView(milestoneId);
    }

    @PostMapping("/{milestoneId}/owner")
    public MilestoneDto changeOwner(
            @PathVariable("milestoneId") Long milestoneId, @RequestBody MilestonePatch patch) {
        verifyCanEditMilestone(milestoneId);
        User newOwner = userAdministrationService.findByLogin(patch.ownerLogin());
        milestoneManager.changeOwner(milestoneId, newOwner);
        return milestoneDisplayService.getMilestoneView(milestoneId);
    }

    @GetMapping("/possible-owners")
    public List<MilestonePossibleOwnerDto> findPossibleOwners() {
        return userAdministrationService.findAllAdminOrManager().stream()
                .sorted((o1, o2) -> o1.login().compareToIgnoreCase(o2.login()))
                .toList();
    }

    @PostMapping("/{motherId}/clone")
    public Map<String, Long> cloneMilestone(
            @PathVariable("motherId") long motherId, @RequestBody MilestoneDuplicationModel model) {
        try {
            long cloneMilestoneId = milestoneManager.cloneMilestone(motherId, model);
            return Collections.singletonMap("id", cloneMilestoneId);
        } catch (MilestoneLabelAlreadyExistsException ex) {
            throw new NameAlreadyInUseException(MILESTONE, model.label(), LABEL, ex);
        }
    }

    @PostMapping("/{sourceId}/synchronize/{targetId}")
    public void synchronizeMilestone(
            @PathVariable("sourceId") long sourceId,
            @PathVariable("targetId") long targetId,
            @RequestBody SynchronizationParameters parameters) {
        milestoneManager.synchronize(
                sourceId, targetId, parameters.extendPerimeter(), parameters.union());
    }

    public record MilestonePatch(
            String label,
            String range,
            Date endDate,
            String status,
            String description,
            String ownerLogin) {}

    public record SynchronizationParameters(boolean extendPerimeter, boolean union) {}

    private void verifyCanEditMilestone(Long milestoneId) {
        milestoneManager.verifyCanEditMilestone(milestoneId);
    }
}
