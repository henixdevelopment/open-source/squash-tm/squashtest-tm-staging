/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.execution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.service.bugtracker.knownissues.remote.RemoteKnownIssueFinder;
import org.squashtest.tm.service.campaign.CustomIterationModificationService;
import org.squashtest.tm.service.display.execution.ExecutionDisplayService;
import org.squashtest.tm.service.execution.ExecutionModificationService;
import org.squashtest.tm.service.internal.display.dto.AttachmentListDto;
import org.squashtest.tm.service.internal.display.dto.execution.ActionStepExecView;
import org.squashtest.tm.service.internal.display.dto.execution.ExecutionView;
import org.squashtest.tm.service.internal.display.dto.execution.ModificationDuringExecutionView;
import org.squashtest.tm.web.backend.controller.form.model.CreatedEntityId;

@RestController
@RequestMapping("/backend/execution/{executionId}")
public class ExecutionController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExecutionController.class);

    private final ExecutionDisplayService executionDisplayService;
    private final ExecutionModificationService executionModificationService;
    private final CustomIterationModificationService itpService;
    private final RemoteKnownIssueFinder remoteKnownIssueFinder;

    public ExecutionController(
            ExecutionDisplayService executionDisplayService,
            ExecutionModificationService executionModificationService,
            CustomIterationModificationService itpService,
            RemoteKnownIssueFinder remoteKnownIssueFinder) {
        this.executionDisplayService = executionDisplayService;
        this.executionModificationService = executionModificationService;
        this.itpService = itpService;
        this.remoteKnownIssueFinder = remoteKnownIssueFinder;
    }

    @GetMapping
    public ExecutionView getExecutionView(@PathVariable("executionId") long executionId) {
        return executionDisplayService.findOne(executionId);
    }

    @GetMapping("attachments")
    public AttachmentListDto getReportUrls(@PathVariable("executionId") long executionId) {
        return executionDisplayService.findAttachmentList(executionId);
    }

    @GetMapping("/modification-during-execution/permissions")
    public void checkPermissions(@PathVariable long executionId) {
        executionDisplayService.checkPermissionsForModificationDuringExecutionPrologue(executionId);
    }

    @GetMapping("/modification-during-execution/permissions/{stepId}")
    public void checkPermissions(@PathVariable long executionId, @PathVariable long stepId) {
        executionDisplayService.checkPermissionsForModificationDuringExecution(executionId, stepId);
    }

    @GetMapping("/modification-during-execution")
    public ModificationDuringExecutionView getModificationDuringExecutionView(
            @PathVariable long executionId) {
        return executionDisplayService.findOneForModificationDuringExec(executionId);
    }

    @GetMapping("/modification-during-execution/action-step/{stepId}")
    public ActionStepExecView getActionStepDuringExecutionView(
            @PathVariable long executionId, @PathVariable long stepId) {
        return executionDisplayService.findOneActionStepForModificationDuringExec(executionId, stepId);
    }

    @PostMapping("/modification-during-execution/update-steps")
    public Long updateSteps(@PathVariable("executionId") long executionId) {
        return executionModificationService.updateSteps(executionId);
    }

    @PostMapping("/comment")
    public void updateComment(
            @RequestBody ExecutionPatch executionPatch, @PathVariable long executionId) {
        // the service is named setExecutionDescription due to old bad naming in db
        // refactoring could break some plugins ?
        executionModificationService.setExecutionDescription(executionId, executionPatch.comment);
        LOGGER.trace("Execution {}: updated description to {}", executionId, executionPatch.comment);
    }

    @PostMapping("/update-from-tc")
    public CreatedEntityId updateExecutionFromTc(@PathVariable long executionId) {
        Execution exec = itpService.updateExecutionFromTc(executionId);
        return new CreatedEntityId(exec.getId());
    }

    @GetMapping("issue-count")
    public Map<String, Integer> getIssueCount(@PathVariable long executionId) {
        return Collections.singletonMap(
                "issueCount", remoteKnownIssueFinder.getCountForExecution(executionId));
    }

    static class ExecutionPatch {
        private String comment;

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }
    }

    @PostMapping("move-notes")
    public void changeNotesIndex(
            @PathVariable long executionId, @RequestBody MoveNotesModel moveNotesModel) {
        executionModificationService.changeNotesIndex(
                executionId, moveNotesModel.newIndex, moveNotesModel.movedNoteIds);
    }

    public static class MoveNotesModel {
        private List<Long> movedNoteIds = new ArrayList<>();
        private Integer newIndex;

        public List<Long> getMovedNoteIds() {
            return movedNoteIds;
        }

        public void setMovedNoteIds(List<Long> movedNoteIds) {
            this.movedNoteIds = movedNoteIds;
        }

        public Integer getNewIndex() {
            return newIndex;
        }

        public void setNewIndex(Integer newIndex) {
            this.newIndex = newIndex;
        }
    }
}
