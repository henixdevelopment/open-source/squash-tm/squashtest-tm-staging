/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.form.model;

import java.util.Map;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.squashtest.tm.service.internal.dto.RawValueModel;

/**
 * Validator used for subclasses of EntityFormModel (which all have a name, a reference and can have
 * custom fields). Not to be used for other entities from the administration workspace such as
 * projects, servers,...
 */
public class EntityFormModelValidator implements Validator {

    private static final String MESSAGE_NOT_BLANK = "message.notBlank";

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(EntityFormModel.class);
    }

    @Override
    public void validate(Object target, Errors errors) {

        EntityFormModel model = (EntityFormModel) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", MESSAGE_NOT_BLANK);

        if (model.getReference() != null && model.getReference().length() > 50) {
            errors.rejectValue("reference", "message.lengthMax");
        }

        for (Map.Entry<Long, RawValueModel> entry : model.getCustomFields().entrySet()) {
            RawValueModel value = entry.getValue();
            if (value.isEmpty()) {
                errors.rejectValue("customFields[" + entry.getKey() + "]", MESSAGE_NOT_BLANK);
            }
        }
    }
}
