/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.importer.requirement;

import java.io.File;
import javax.inject.Inject;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.service.requirement.RequirementLibraryNavigationService;
import org.squashtest.tm.web.backend.controller.importer.AbstractImportHelper;
import org.squashtest.tm.web.backend.controller.importer.ImportExcelResponse;

/**
 * @author qtran - created on 01/12/2020
 */
@RestController
@RequestMapping("backend/requirement/importer")
public class RequirementImportController {
    private static final Logger LOGGER = LoggerFactory.getLogger(RequirementImportController.class);

    @Inject private RequirementLibraryNavigationService navigationService;

    @Inject private RequirementImportHelper importHelper;

    /**
     * Will simulate import of requirements in a xls file format.
     *
     * @param uploadedFile : the xls file to import in a {@link MultipartFile} form
     * @return a {@link ModelAndView} containing the summary of the import and the link to a complete
     *     log for any invalid information it contains
     * @see RequirementLibraryNavigationService#simulateImportExcelRequirement(File)
     */
    @PostMapping(value = "/xls", params = "dry-run")
    public ImportExcelResponse dryRunExcelWorkbook(
            @RequestParam("archive") MultipartFile uploadedFile) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("dryRunExcelWorkbook");
        }
        return importHelper.importWorkbook(
                AbstractImportHelper.REQUIREMENT,
                uploadedFile,
                xls -> navigationService.simulateImportExcelRequirement(xls));
    }

    /**
     * Will import requirements in a one xls file format.
     *
     * @see RequirementLibraryNavigationService#importExcelRequirement(File)
     * @param uploadedFile : the xls file to import in a {@link MultipartFile} form
     * @return @return a {@link ModelAndView} containing the summary of the import and the link to a
     *     complete log for any invalid information it contains
     */
    @PostMapping(value = "/xls", params = "!dry-run")
    public ImportExcelResponse importExcelWorkbook(
            @RequestParam("archive") MultipartFile uploadedFile) {
        return importHelper.importWorkbook(
                AbstractImportHelper.REQUIREMENT,
                uploadedFile,
                xls -> navigationService.importExcelRequirement(xls));
    }
}
