/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.bugtracker;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.csp.core.bugtracker.core.BugTrackerNoCredentialsDetailedException;
import org.squashtest.csp.core.bugtracker.core.BugTrackerNoCredentialsException;
import org.squashtest.csp.core.bugtracker.net.AuthenticationException;
import org.squashtest.tm.core.foundation.exception.InvalidUrlException;
import org.squashtest.tm.core.foundation.lang.UrlUtils;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.domain.servers.AuthenticationPolicy;
import org.squashtest.tm.domain.servers.AuthenticationProtocol;
import org.squashtest.tm.exception.WrongUrlException;
import org.squashtest.tm.exception.bugtracker.BadCredentialsException;
import org.squashtest.tm.exception.bugtracker.CannotConnectBugtrackerException;
import org.squashtest.tm.service.bugtracker.BugTrackerManagerService;
import org.squashtest.tm.service.bugtracker.BugTrackerModificationService;
import org.squashtest.tm.service.internal.servers.OAuth2ConsumerService;
import org.squashtest.tm.service.servers.ManageableCredentials;
import org.squashtest.tm.service.servers.ServerAuthConfiguration;
import org.squashtest.tm.web.backend.controller.form.model.BugtrackerFormModel;
import org.squashtest.tm.web.backend.controller.utils.UrlValidator;

@RestController
@RequestMapping("/backend/bugtracker/")
public class BugTrackerModificationController {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(BugTrackerModificationController.class);

    private final BugTrackerManagerService bugTrackerManagerService;
    private final BugTrackerModificationService bugtrackerModificationService;
    private final OAuth2ConsumerService oAuth2ConsumerService;

    BugTrackerModificationController(
            BugTrackerManagerService bugTrackerManagerService,
            BugTrackerModificationService bugtrackerModificationService,
            OAuth2ConsumerService oAuth2ConsumerService) {
        this.bugTrackerManagerService = bugTrackerManagerService;
        this.bugtrackerModificationService = bugtrackerModificationService;
        this.oAuth2ConsumerService = oAuth2ConsumerService;
    }

    @PostMapping("/new")
    public Map<String, Object> addBugTracker(
            @Valid @RequestBody BugtrackerFormModel bugTrackerFormModel) {
        try {
            BugTracker bugTracker = bugTrackerFormModel.getBugtracker();
            UrlUtils.toUrl(bugTrackerFormModel.getUrl());
            bugTrackerManagerService.addBugTracker(bugTracker);
            return Collections.singletonMap("id", bugTracker.getId());
        } catch (InvalidUrlException iue) {
            throw new WrongUrlException("url", iue);
        }
    }

    @GetMapping("/get-bugtracker-kinds")
    public Map<String, Set<String>> getBugTrackerKinds() {
        return Collections.singletonMap(
                "bugtrackerKinds", bugTrackerManagerService.findBugTrackerKinds());
    }

    @DeleteMapping("/{bugtrackerIds}")
    public void deleteBugtrackers(@PathVariable List<Long> bugtrackerIds) {
        LOGGER.debug("ids of bugtracker to delete {}", bugtrackerIds);
        bugTrackerManagerService.deleteBugTrackers(bugtrackerIds);
    }

    @PostMapping(value = "{bugtrackerId}/name")
    public void changeName(
            @PathVariable long bugtrackerId,
            @RequestBody BugTrackerModificationController.BugTrackerPatch patch) {
        bugtrackerModificationService.changeName(bugtrackerId, patch.getName());
    }

    @PostMapping(value = "{bugtrackerId}/url")
    public void changeUrl(
            @PathVariable long bugtrackerId,
            @RequestBody BugTrackerModificationController.BugTrackerPatch patch) {
        UrlValidator.checkURL(patch.getUrl());
        bugtrackerModificationService.changeUrl(bugtrackerId, patch.getUrl());
    }

    @PostMapping(value = "{bugtrackerId}/kind")
    public void changeKind(
            @PathVariable long bugtrackerId,
            @RequestBody BugTrackerModificationController.BugTrackerPatch patch) {
        bugtrackerModificationService.changeKind(bugtrackerId, patch.getKind());
    }

    @PostMapping(value = "{bugtrackerId}/iframe-friendly")
    public void changeIframeFriendly(
            @PathVariable long bugtrackerId,
            @RequestBody BugTrackerModificationController.BugTrackerPatch patch) {
        bugtrackerModificationService.changeIframeFriendly(bugtrackerId, patch.isIframeFriendly());
    }

    @PostMapping(value = "{bugtrackerId}/auth-protocol")
    public void changeAuthProtocol(
            @PathVariable long bugtrackerId,
            @RequestBody BugTrackerModificationController.BugTrackerPatch patch) {
        try {
            AuthenticationProtocol protocol =
                    Enum.valueOf(AuthenticationProtocol.class, patch.getAuthProtocol());
            bugtrackerModificationService.changeAuthenticationProtocol(bugtrackerId, protocol);
        } catch (IllegalArgumentException e) {
            throw new AuthenticationException(
                    "Unknown authentication protocol " + patch.getAuthProtocol(), e);
        }
    }

    @PostMapping(value = "{bugTrackerId}/auth-protocol/configuration")
    public void saveAuthConfiguration(
            @PathVariable long bugTrackerId, @Valid @RequestBody ServerAuthConfiguration configuration) {
        bugtrackerModificationService.storeAuthConfiguration(bugTrackerId, configuration);
    }

    @PostMapping(value = "/{bugTrackerId}/auth-policy")
    public void changeAuthPolicy(
            @PathVariable long bugTrackerId,
            @RequestBody BugTrackerModificationController.BugTrackerPatch patch) {
        try {
            AuthenticationPolicy policy = Enum.valueOf(AuthenticationPolicy.class, patch.getAuthPolicy());
            bugtrackerModificationService.changeAuthenticationPolicy(bugTrackerId, policy);
        } catch (IllegalArgumentException e) {
            throw new AuthenticationException(
                    "Unknown authentication policy " + patch.getAuthPolicy(), e);
        }
    }

    @PostMapping("/{bugTrackerId}/description")
    public void updateDescription(
            @PathVariable long bugTrackerId,
            @RequestBody BugTrackerModificationController.BugTrackerPatch patch) {
        bugtrackerModificationService.updateDescription(bugTrackerId, patch.description);
    }

    public static class BugTrackerPatch {
        private String name;
        private String kind;
        private String url;
        private boolean iframeFriendly;
        private String authProtocol;
        private String authPolicy;
        private String description;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getKind() {
            return kind;
        }

        public void setKind(String kind) {
            this.kind = kind;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public boolean isIframeFriendly() {
            return iframeFriendly;
        }

        public void setIframeFriendly(boolean iframeFriendly) {
            this.iframeFriendly = iframeFriendly;
        }

        public String getAuthProtocol() {
            return authProtocol;
        }

        public void setAuthProtocol(String authProtocol) {
            this.authProtocol = authProtocol;
        }

        public String getAuthPolicy() {
            return authPolicy;
        }

        public void setAuthPolicy(String authPolicy) {
            this.authPolicy = authPolicy;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

    @PostMapping(value = "/{bugTrackerId}/credentials")
    public void storeCredentials(
            @PathVariable long bugTrackerId, @RequestBody ManageableCredentials credentials) {
        try {
            testCredentials(bugTrackerId, credentials);
            bugtrackerModificationService.storeCredentials(bugTrackerId, credentials);
        } catch (BugTrackerNoCredentialsDetailedException | BugTrackerNoCredentialsException e) {
            throw new BadCredentialsException(e);
        } catch (Exception e) {
            throw new CannotConnectBugtrackerException(e);
        }
    }

    private void testCredentials(long bugTrackerId, ManageableCredentials credentials) {
        bugtrackerModificationService.testCredentials(bugTrackerId, credentials);
    }

    @DeleteMapping(value = "/{bugTrackerId}/credentials")
    public void deleteBugtrackerCredentials(@PathVariable long bugTrackerId) {
        bugtrackerModificationService.deleteCredentials(bugTrackerId);
    }

    @PostMapping(value = "/{bugTrackerId}/authentication/oauth2/token")
    public void askOauth2Token(
            @PathVariable("bugTrackerId") long bugTrackerId, @RequestParam("code") String code) {
        oAuth2ConsumerService.getOauth2token(bugTrackerId, code, null);
    }

    @PostMapping(value = "/{bugTrackerId}/cache-credentials")
    public void storeCacheCredentials(
            @PathVariable long bugTrackerId, @RequestBody ManageableCredentials credentials) {
        try {
            testCredentials(bugTrackerId, credentials);
            bugtrackerModificationService.storeCacheCredentials(bugTrackerId, credentials);
        } catch (BugTrackerNoCredentialsDetailedException | BugTrackerNoCredentialsException e) {
            throw new BadCredentialsException(e);
        } catch (Exception e) {
            throw new CannotConnectBugtrackerException(e);
        }
    }

    @DeleteMapping(value = "/{bugTrackerId}/cache-credentials")
    public void deleteBugtrackerCacheCredentials(@PathVariable long bugTrackerId) {
        bugtrackerModificationService.deleteCacheCredentials(bugTrackerId);
    }
}
