/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.customfield;

import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.inject.Inject;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.service.customfield.CustomFieldBindingModificationService;
import org.squashtest.tm.service.display.custom.field.CustomFieldDisplayService;
import org.squashtest.tm.service.internal.display.dto.CufBindingDto;
import org.squashtest.tm.service.internal.dto.CustomFieldBindingModel;

@RestController
@RequestMapping("/backend/custom-field-binding")
public class CustomFieldBindingController {

    private final CustomFieldBindingModificationService customFieldBindingModificationService;

    private final CustomFieldDisplayService customFieldDisplayService;

    @Inject
    public CustomFieldBindingController(
            CustomFieldBindingModificationService customFieldBindingModificationService,
            CustomFieldDisplayService customFieldDisplayService) {
        this.customFieldBindingModificationService = customFieldBindingModificationService;
        this.customFieldDisplayService = customFieldDisplayService;
    }

    @PostMapping(value = "/project/{projectId}/bind-custom-fields")
    public Map<BindableEntity, Set<CufBindingDto>> bindCustomFieldsToProject(
            @PathVariable long projectId,
            @RequestBody Map<String, CustomFieldBindingModel[]> requestBody) {
        customFieldBindingModificationService.createNewBindings(requestBody.get("customFieldBindings"));
        return customFieldDisplayService.findAllCustomFieldBindings(projectId);
    }

    @DeleteMapping(value = "/project/unbind-custom-fields/{customFieldBindingIds}")
    public void unbindCustomFieldsFromProject(@PathVariable List<Long> customFieldBindingIds) {
        customFieldBindingModificationService.removeCustomFieldBindings(customFieldBindingIds);
    }
}
