/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.exceptionresolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.owasp.esapi.errors.IntrusionException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.AbstractHandlerExceptionResolver;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;

@Component
public class HandlerIntrusionExceptionResolver extends AbstractHandlerExceptionResolver {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(HandlerIntrusionExceptionResolver.class);

    @Override
    @ExceptionHandler(value = {IntrusionException.class})
    protected ModelAndView doResolveException(
            HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        if (!(ex instanceof IntrusionException)) {
            return null;
        }

        IntrusionException intrusionException = (IntrusionException) ex;
        LOGGER.error(intrusionException.getLogMessage(), ex);

        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        String message = intrusionException.getUserMessage();

        if (message.isBlank()) {
            message = "An intrusion was detected";
        }

        if (ExceptionResolverUtils.clientAcceptsMIME(request, MimeType.APPLICATION_JSON)) {
            return new ModelAndView(new MappingJackson2JsonView(), "intrusionException", message);
        } else if (ExceptionResolverUtils.clientAcceptsMIME(request, MimeType.TEXT_PLAIN)) {
            return new ModelAndView(
                    new HandlerSimpleExceptionResolver.PlainTextView(), "simpleError", message);
        }

        return null;
    }
}
