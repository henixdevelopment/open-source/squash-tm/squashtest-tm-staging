/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.campaign;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.campaign.CampaignLibraryNavigationService;
import org.squashtest.tm.service.display.campaign.CampaignDisplayService;
import org.squashtest.tm.service.internal.display.dto.LibraryDto;
import org.squashtest.tm.service.statistics.campaign.StatisticsBundle;

@RestController
@RequestMapping("/backend/campaign-library-view")
public class CampaignLibraryViewController {

    private CampaignDisplayService campaignDisplayService;

    private CampaignLibraryNavigationService campaignLibraryNavigationService;

    public CampaignLibraryViewController(
            CampaignDisplayService campaignDisplayService,
            CampaignLibraryNavigationService campaignLibraryNavigationService) {
        this.campaignDisplayService = campaignDisplayService;
        this.campaignLibraryNavigationService = campaignLibraryNavigationService;
    }

    @GetMapping(value = "/{campaignLibraryId}")
    public LibraryDto getCampaignView(@PathVariable long campaignLibraryId) {
        return campaignDisplayService.getCampaignLibraryView(campaignLibraryId);
    }

    @PostMapping(value = "/{campaignLibraryId}/statistics")
    public StatisticsBundle getCampaignLibraryStatistics(
            @PathVariable long campaignLibraryId,
            @RequestBody CampaignViewController.StatisticsScopePatch patch) {
        return campaignLibraryNavigationService.gatherLibraryStatisticsBundle(
                campaignLibraryId, patch.lastExecutionScope());
    }
}
