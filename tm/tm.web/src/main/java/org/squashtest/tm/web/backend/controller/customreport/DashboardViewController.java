/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.customreport;

import java.util.List;
import java.util.Locale;
import javax.inject.Named;
import javax.inject.Provider;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.Workspace;
import org.squashtest.tm.domain.customreport.CustomReportDashboard;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;
import org.squashtest.tm.service.display.customreport.DashboardDisplayService;
import org.squashtest.tm.service.internal.display.dto.customreports.CustomReportDashboardDto;
import org.squashtest.tm.service.internal.dto.json.JsonCustomReportDashboard;
import org.squashtest.tm.web.backend.model.builder.JsonCustomReportDashboardBuilder;
import org.squashtest.tm.web.backend.model.json.JsonDynamicScope;

@RestController
@RequestMapping("/backend/dashboard-view")
public class DashboardViewController {

    private final CustomReportLibraryNodeService customReportLibraryNodeService;

    private final Provider<JsonCustomReportDashboardBuilder> builderProvider;

    private final DashboardDisplayService dashboardDisplayService;

    public DashboardViewController(
            CustomReportLibraryNodeService customReportLibraryNodeService,
            @Named("customReport.dashboardBuilder")
                    Provider<JsonCustomReportDashboardBuilder> builderProvider,
            DashboardDisplayService dashboardDisplayService) {
        this.customReportLibraryNodeService = customReportLibraryNodeService;
        this.builderProvider = builderProvider;
        this.dashboardDisplayService = dashboardDisplayService;
    }

    @GetMapping(value = "/{customReportLibraryNodeId}")
    public JsonCustomReportDashboard getDashboardDetails(
            @PathVariable Long customReportLibraryNodeId, Locale locale) {
        CustomReportDashboard dashboard =
                customReportLibraryNodeService.findCustomReportDashboardById(customReportLibraryNodeId);

        JsonCustomReportDashboard jsonCustomReportDashboard =
                builderProvider.get().build(customReportLibraryNodeId, dashboard, locale);
        jsonCustomReportDashboard.setFavoriteWorkspaces(
                dashboardDisplayService.findFavoriteWorkspaceForDashboardAndCurrentUser(
                        jsonCustomReportDashboard.getCustomReportLibraryNodeId()));
        return jsonCustomReportDashboard;
    }

    @PostMapping(value = "{customReportLibraryNodeId}")
    public JsonCustomReportDashboard getDashboardDetailsWithDynamicScope(
            @PathVariable Long customReportLibraryNodeId,
            Locale locale,
            @RequestBody JsonDynamicScope dynamicScope) {
        List<EntityReference> entityReferences = dynamicScope.convertToEntityReferences();
        CustomReportDashboard dashboard =
                customReportLibraryNodeService.findCustomReportDashboardById(customReportLibraryNodeId);
        Workspace workspace = Workspace.valueOf(dynamicScope.getWorkspaceName());
        CustomReportDashboardDto dto =
                new CustomReportDashboardDto(
                        workspace,
                        entityReferences,
                        dynamicScope.isMilestoneDashboard(),
                        dynamicScope.isExtendedHighLvlReqScope(),
                        dynamicScope.isLastExecutionScope());

        return builderProvider.get().build(customReportLibraryNodeId, dashboard, locale, dto);
    }
}
