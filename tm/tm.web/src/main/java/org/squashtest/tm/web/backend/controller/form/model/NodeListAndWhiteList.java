/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.form.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.service.clipboard.model.ClipboardPayload;

public class NodeListAndWhiteList extends NodeList {

    private List<String> whiteListContent;

    @JsonIgnore
    public ClipboardPayload asClipboardPayload() {
        List<String> whiteListIds = getWhiteListContent();
        List<NodeReference> copiedNodeReference = getNodeReferences();

        ClipboardPayload clipboardPayload = new ClipboardPayload();
        clipboardPayload.setSelectedNode(copiedNodeReference);
        clipboardPayload.setSelectedNodeIds(
                copiedNodeReference.stream().map(NodeReference::getId).toList());
        clipboardPayload.setWhiteListNodeIds(
                whiteListIds.stream().map(NodeReference::fromNodeId).map(NodeReference::getId).toList());

        return clipboardPayload;
    }

    public List<String> getWhiteListContent() {
        return whiteListContent;
    }

    public void setWhiteListContent(List<String> whiteListContent) {
        this.whiteListContent = whiteListContent;
    }
}
