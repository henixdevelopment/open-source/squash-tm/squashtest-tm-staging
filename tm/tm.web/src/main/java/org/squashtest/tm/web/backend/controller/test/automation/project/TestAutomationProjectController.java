/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.test.automation.project;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.testautomation.TestAutomationProject;
import org.squashtest.tm.exception.DomainException;
import org.squashtest.tm.exception.RequiredFieldException;
import org.squashtest.tm.service.internal.display.dto.TestAutomationProjectDto;
import org.squashtest.tm.service.testautomation.TestAutomationProjectManagerService;
import org.squashtest.tm.web.backend.model.testautomation.TAUsageStatus;

@RestController
@RequestMapping("/backend/test-automation-projects")
public class TestAutomationProjectController {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(TestAutomationProjectController.class);

    private static final String PROJECT_ID = "/{projectId}";
    private static final String PROJECT_IDS = "/{projectIds}";
    private static final String TA_PROJECT = "ta-project";
    private static final String TA_PROJECTS = "taProjects";

    @Inject private TestAutomationProjectManagerService service;

    @DeleteMapping(value = PROJECT_ID)
    public void deleteTestAutomationProject(@PathVariable long projectId) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Delete test automation project of id #{}", projectId);
        }
        service.deleteProject(projectId);
    }

    @PostMapping(value = PROJECT_ID)
    public Map<String, List<TestAutomationProjectDto>> editTestAutomationProject(
            @PathVariable long projectId, @RequestBody TestAutomationProjectController.Patch patch) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Edit test automation project of id #{}", projectId);
        }
        try {
            TestAutomationProject testAutomationProject = new TestAutomationProject();
            testAutomationProject.setCanRunGherkin(patch.isCanRunBdd());
            testAutomationProject.setJobName(patch.getRemoteName());
            testAutomationProject.setLabel(patch.getLabel());
            testAutomationProject.setSlaves(patch.getExecutionEnvironment());
            service.editProject(projectId, testAutomationProject);

            final Long tmProjectId = service.findProjectById(projectId).getTmProject().getId();
            return Collections.singletonMap(TA_PROJECTS, service.findAllByTMProject(tmProjectId));
        } catch (DomainException de) {
            de.setObjectName(TA_PROJECT);
            throw de;
        }
    }

    @GetMapping(value = PROJECT_IDS + "/usage-status")
    public Map<String, TAUsageStatus> getTestAutomationUsageStatus(
            @PathVariable List<Long> projectIds) {
        final boolean hasExecutedTests =
                projectIds.stream()
                        .anyMatch(id -> service.haveExecutedTests(Collections.singletonList(id)));
        return Collections.singletonMap("usageStatus", new TAUsageStatus(hasExecutedTests));
    }

    @PostMapping(value = PROJECT_ID + "/label")
    public Map<String, List<TestAutomationProjectDto>> changeLabel(
            @PathVariable long projectId, @RequestBody TestAutomationProjectController.Patch patch) {
        if (patch.getLabel() == null || patch.getLabel().trim().isEmpty()) {
            throw new RequiredFieldException("label");
        }

        try {
            service.changeLabel(projectId, patch.getLabel().trim());
            final Long tmProjectId = service.findProjectById(projectId).getTmProject().getId();
            return Collections.singletonMap(TA_PROJECTS, service.findAllByTMProject(tmProjectId));
        } catch (DomainException de) {
            de.setObjectName(TA_PROJECT);
            throw de;
        }
    }

    @PostMapping(value = PROJECT_ID + "/can-run-bdd")
    public Map<String, List<TestAutomationProjectDto>> changeCanRunBdd(
            @PathVariable long projectId, @RequestBody TestAutomationProjectController.Patch patch) {
        try {
            service.changeCanRunGherkin(projectId, patch.isCanRunBdd());

            final Long tmProjectId = service.findProjectById(projectId).getTmProject().getId();
            return Collections.singletonMap(TA_PROJECTS, service.findAllByTMProject(tmProjectId));
        } catch (DomainException de) {
            de.setObjectName(TA_PROJECT);
            throw de;
        }
    }

    static class Patch {
        String label;
        boolean canRunBdd;
        Long taProjectId;
        String remoteName;
        String executionEnvironment;

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public boolean isCanRunBdd() {
            return canRunBdd;
        }

        public void setCanRunBdd(boolean canRunBdd) {
            this.canRunBdd = canRunBdd;
        }

        public Long getTaProjectId() {
            return taProjectId;
        }

        public void setTaProjectId(Long taProjectId) {
            this.taProjectId = taProjectId;
        }

        public String getExecutionEnvironment() {
            return executionEnvironment;
        }

        public void setExecutionEnvironment(String executionEnvironment) {
            this.executionEnvironment = executionEnvironment;
        }

        public String getRemoteName() {
            return remoteName;
        }

        public void setRemoteName(String remoteName) {
            this.remoteName = remoteName;
        }
    }
}
