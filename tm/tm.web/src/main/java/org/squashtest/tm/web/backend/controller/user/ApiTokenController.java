/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.user;

import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.user.ApiTokenService;

@RestController
@RequestMapping("backend/api-token")
public class ApiTokenController {

    private final ApiTokenService apiTokenService;

    public ApiTokenController(ApiTokenService apiTokenService) {
        this.apiTokenService = apiTokenService;
    }

    @PostMapping("generate-api-token")
    public Map<String, String> generateApiToken(@RequestBody ApiTokenModel apiTokenModel) {
        String userToken =
                apiTokenService
                        .generateApiToken(
                                apiTokenModel.name, apiTokenModel.expiryDate, apiTokenModel.permissions)
                        .generatedJwtToken();
        Map<String, String> response = new HashMap<>();
        response.put("token", Base64.getEncoder().encodeToString(userToken.getBytes()));

        return response;
    }

    @PostMapping("generate-api-token/{userId}")
    public Map<String, String> generateApiTokenForTestAutoServerUser(
            @PathVariable long userId, @RequestBody ApiTokenModel apiTokenModel) {
        String userToken =
                apiTokenService
                        .generateApiTokenForTestAutoServer(
                                userId, apiTokenModel.name, apiTokenModel.expiryDate, apiTokenModel.permissions)
                        .generatedJwtToken();
        Map<String, String> response = new HashMap<>();
        response.put("token", Base64.getEncoder().encodeToString(userToken.getBytes()));

        return response;
    }

    @DeleteMapping("{tokenId}")
    public void deleteSelfApiToken(@PathVariable long tokenId) {
        apiTokenService.deletePersonalApiToken(tokenId);
    }

    public record ApiTokenModel(String name, Date expiryDate, String permissions) {}
}
