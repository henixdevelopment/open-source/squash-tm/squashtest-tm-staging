/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.thirdpartyserver;

import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.domain.servers.AuthenticationStatus;
import org.squashtest.tm.service.bugtracker.BugTrackerFinderService;
import org.squashtest.tm.service.bugtracker.BugTrackersLocalService;
import org.squashtest.tm.service.configuration.ConfigurationService;
import org.squashtest.tm.service.internal.servers.OAuth2ConsumerService;
import org.squashtest.tm.service.servers.ManageableCredentials;
import org.squashtest.tm.service.user.UserAccountService;

// XSS OK - bflessel
@Controller
@RequestMapping("backend/servers")
public class ThirdPartyServersAuthenticationController {
    private static final String REDIRECT = "redirect:";

    @Inject private ConfigurationService configService;

    @Inject private BugTrackersLocalService btService;

    @Inject private OAuth2ConsumerService oAuth2ConsumerService;

    @Inject private UserAccountService userService;

    @Inject private BugTrackerFinderService bugTrackerFinderService;

    /** returns information about whether the user is authenticated or not */
    @GetMapping(value = "/{serverId}/authentication", produces = "application/json")
    @ResponseBody
    public AuthenticationStatus getAuthenticationStatus(@PathVariable("serverId") Long serverId) {

        // for now we just cheat : all servers are bugtracker servers
        return btService.checkAuthenticationStatus(serverId);
    }

    // ********** Basic Authentication ************************

    /**
     * tries to authenticate the current user against the given server. Status 200 means success (user
     * is authenticated), an exception means failure.
     */
    @ResponseBody
    @PostMapping("/{serverId}/authentication")
    public void authenticate(
            @RequestBody ManageableCredentials credentials, @PathVariable("serverId") long serverId) {
        // will throw if there is a problem
        btService.validateManageableCredentials(serverId, credentials, false);

        // Issue 602 : we need to store the credentials on successful validation
        // because the credentials are not always cacheable.
        userService.saveCurrentUserCredentials(serverId, credentials);
    }

    // ********* OAuth2 authentication ************************

    /**
     * get the url with the params for redirect the user to the Authentication page of the
     * Authorization Server
     */
    @GetMapping(value = "/{serverId}/authentication/oauth2")
    public String authenticateOauth2(@PathVariable("serverId") long serverId) {
        BugTracker bugTracker = bugTrackerFinderService.findById(serverId);
        String oauth2AuthenticationUrl = oAuth2ConsumerService.getOauth2AuthenticationUrl(bugTracker);
        return REDIRECT + oauth2AuthenticationUrl;
    }

    @PostMapping(value = "/{serverId}/authentication/oauth2/token")
    @ResponseBody
    public void askOauth2Token(
            @PathVariable("serverId") long serverId, @RequestParam("code") String code) {
        oAuth2ConsumerService.getCurrentUserOauth2token(serverId, code);
    }
}
