/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.project;

import java.util.Collections;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.project.ProjectTemplate;
import org.squashtest.tm.exception.NameAlreadyInUseException;
import org.squashtest.tm.service.project.GenericProjectManagerService;
import org.squashtest.tm.service.project.ProjectManagerService;
import org.squashtest.tm.web.backend.model.json.JsonProjectFromTemplate;

@RestController
@RequestMapping("/backend/projects")
public class ProjectController {

    private final ProjectManagerService projectManager;
    private final GenericProjectManagerService genericProjectManager;

    public ProjectController(
            ProjectManagerService projectManager, GenericProjectManagerService genericProjectManager) {
        this.projectManager = projectManager;
        this.genericProjectManager = genericProjectManager;
    }

    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping(value = "/new")
    public Map<String, Object> createProjectFromTemplate(
            @Valid @RequestBody JsonProjectFromTemplate jsonProjectFromTemplate) {
        try {
            if (jsonProjectFromTemplate.isFromTemplate()) {
                projectManager.addProjectFromTemplate(
                        jsonProjectFromTemplate.getProject(),
                        jsonProjectFromTemplate.getTemplateId(),
                        jsonProjectFromTemplate.getParams());
                return Collections.singletonMap("id", jsonProjectFromTemplate.getProject().getId());
            } else {
                Project project = jsonProjectFromTemplate.getProject();
                genericProjectManager.persist(project);
                genericProjectManager.createAttachmentFromDescription(project);
                return Collections.singletonMap("id", project.getId());
            }
        } catch (NameAlreadyInUseException ex) {
            ex.setObjectName("add-project-from-template");
            throw ex;
        }
    }

    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping(value = "/new-template")
    public Map<String, Object> createNewTemplate(@RequestBody @Valid ProjectTemplate template) {
        try {
            genericProjectManager.persist(template);
            genericProjectManager.createAttachmentFromDescription(template);
            return Collections.singletonMap("id", template.getId());
        } catch (NameAlreadyInUseException ex) {
            ex.setObjectName("add-template");
            throw ex;
        }
    }

    @DeleteMapping(value = "/{projectId}")
    public void deleteProject(@PathVariable long projectId) {
        projectManager.deleteProject(projectId);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "coerce-into-template")
    public void coerceProjectIntoTemplate(@RequestBody Map<String, Long> payload) {
        final long projectId = payload.get("projectId");
        genericProjectManager.coerceProjectIntoTemplate(projectId);
    }
}
