/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.project;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.testautomation.TestAutomationProject;
import org.squashtest.tm.service.display.project.ProjectDisplayService;
import org.squashtest.tm.service.internal.display.dto.BindMilestoneToProjectDialogData;
import org.squashtest.tm.service.internal.display.dto.PivotFormatImportDto;
import org.squashtest.tm.service.internal.display.dto.ProjectViewDto;
import org.squashtest.tm.service.internal.display.dto.TestAutomationProjectDto;
import org.squashtest.tm.service.project.GenericProjectManagerService;
import org.squashtest.tm.service.project.ProjectManagerService;
import org.squashtest.tm.service.servers.ManageableCredentials;
import org.squashtest.tm.service.testautomation.TestAutomationServerCredentialsService;
import org.squashtest.tm.service.user.UserAccountService;
import org.squashtest.tm.web.backend.manager.plugin.ConfigurablePluginManager;

@RestController
@RequestMapping("/backend/project-view")
public class ProjectViewController {
    private final ProjectDisplayService projectViewDisplayService;
    private final GenericProjectManagerService genericProjectManagerService;
    private final ProjectManagerService projectManagerService;
    private final ConfigurablePluginManager pluginManager;
    private final UserAccountService userAccountService;
    private final TestAutomationServerCredentialsService testAutomationServerCredentialsService;

    @Inject
    ProjectViewController(
            ProjectDisplayService projectViewDisplayService,
            GenericProjectManagerService projectManager,
            ProjectManagerService projectManagerService,
            ConfigurablePluginManager pluginManager,
            UserAccountService userAccountService,
            TestAutomationServerCredentialsService testAutomationServerCredentialsService) {
        this.projectViewDisplayService = projectViewDisplayService;
        this.genericProjectManagerService = projectManager;
        this.projectManagerService = projectManagerService;
        this.pluginManager = pluginManager;
        this.userAccountService = userAccountService;
        this.testAutomationServerCredentialsService = testAutomationServerCredentialsService;
    }

    @GetMapping("/{projectId}")
    public ProjectViewDto getProjectView(@PathVariable long projectId) {
        ProjectViewDto dto = projectViewDisplayService.getProjectView(projectId);
        dto.setAvailablePlugins(pluginManager.getAvailablePlugins(projectId));

        if (Boolean.TRUE.equals(dto.getAllowAutomationWorkflow())
                && "REMOTE_WORKFLOW".equals(dto.getAutomationWorkflowType())
                && !projectViewDisplayService.checkRemotePluginIsAvailableInProject(dto)) {

            genericProjectManagerService.changeAutomationWorkflow(dto.getId(), "NONE");
            dto.setAutomationWorkflowType("NONE");
        }
        return dto;
    }

    @GetMapping("/{projectId}/statuses-in-use")
    public Map<String, Boolean> getProjectStatusesInUse(@PathVariable long projectId) {
        return projectViewDisplayService.getProjectStatusesInUse(projectId);
    }

    @GetMapping("/{projectId}/available-milestones")
    public BindMilestoneToProjectDialogData getAvailableMilestones(@PathVariable long projectId) {
        return projectViewDisplayService.findAvailableMilestones(projectId);
    }

    @GetMapping("/{projectId}/available-ta-projects")
    public Map<String, List<TestAutomationProjectDto>> getAvailableTAProjects(
            @PathVariable long projectId) {
        final List<TestAutomationProjectDto> dtos =
                genericProjectManagerService.findAllAvailableTaProjects(projectId).stream()
                        .map(
                                (TestAutomationProject taProject) ->
                                        TestAutomationProjectDto.fromRemoteTestAutomationProject(taProject, projectId))
                        .toList();
        return Collections.singletonMap("taProjects", dtos);
    }

    @PostMapping("/test-automation-server/{serverId}/authentication")
    public void authenticate(
            @RequestBody ManageableCredentials credentials, @PathVariable("serverId") long serverId) {
        testAutomationServerCredentialsService.validateCredentials(serverId, credentials);
        userAccountService.saveCurrentUserCredentials(serverId, credentials);
    }

    @GetMapping("/{projectId}/restricted-ta-projects")
    public Map<String, List<TestAutomationProjectDto>> getAvailableTAProjectsForCurrentUser(
            @PathVariable long projectId) {
        final List<TestAutomationProjectDto> dtos =
                genericProjectManagerService
                        .findAllAvailableTaProjectsWithUserLevelCredentials(projectId)
                        .stream()
                        .map(
                                (TestAutomationProject taProject) ->
                                        TestAutomationProjectDto.fromRemoteTestAutomationProject(taProject, projectId))
                        .toList();
        return Collections.singletonMap("taProjects", dtos);
    }

    @GetMapping("/{projectId}/bugtracker/projectNames")
    public Map<String, Object> getBugtrackerProjectNames(@PathVariable long projectId) {
        Map<String, Object> response = new HashMap<>();
        List<String> bugtrackerProjectNames =
                projectViewDisplayService.fetchBugtrackerProjectNames(projectId);
        if (!bugtrackerProjectNames.isEmpty()) {
            response.put("projectNames", bugtrackerProjectNames);
        }
        return response;
    }

    @PostMapping("/{projectId}/bugtracker/projectNames")
    public Map<String, Object> updateBugtrackerProjectNames(
            @RequestBody List<String> projectBugTrackerNames, @PathVariable long projectId) {
        Map<String, Object> response = new HashMap<>();
        projectManagerService.updateBugTrackerProjectNames(projectId, projectBugTrackerNames);
        response.put("projectNames", projectBugTrackerNames);
        return response;
    }

    @GetMapping("/{projectId}/refresh-project-imports")
    public List<PivotFormatImportDto> refreshProjectImports(@PathVariable long projectId) {
        return projectViewDisplayService.getExistingImports(projectId);
    }
}
