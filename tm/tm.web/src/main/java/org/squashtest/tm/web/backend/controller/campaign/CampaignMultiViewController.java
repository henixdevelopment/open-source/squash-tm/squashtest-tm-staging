/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.campaign;

import java.util.List;
import java.util.Locale;
import javax.inject.Named;
import javax.inject.Provider;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.NodeReferences;
import org.squashtest.tm.domain.Workspace;
import org.squashtest.tm.domain.customreport.CustomReportDashboard;
import org.squashtest.tm.service.campaign.CampaignLibraryNavigationService;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;
import org.squashtest.tm.service.display.campaign.CampaignDisplayService;
import org.squashtest.tm.service.internal.display.dto.campaign.CampaignMultiSelectionDto;
import org.squashtest.tm.service.internal.display.dto.customreports.CustomReportDashboardDto;
import org.squashtest.tm.service.internal.dto.json.JsonCustomReportDashboard;
import org.squashtest.tm.service.statistics.campaign.StatisticsBundle;
import org.squashtest.tm.web.backend.controller.form.model.NodeList;
import org.squashtest.tm.web.backend.model.builder.JsonCustomReportDashboardBuilder;

@RestController
@RequestMapping("/backend/campaign-workspace-multi-view")
public class CampaignMultiViewController {

    @Named("customReport.dashboardBuilder")
    private final Provider<JsonCustomReportDashboardBuilder> builderProvider;

    private final CampaignDisplayService campaignDisplayService;
    private final CustomReportLibraryNodeService customReportLibraryNodeService;

    private final CampaignLibraryNavigationService campaignLibraryNavigationService;

    public CampaignMultiViewController(
            CampaignDisplayService campaignDisplayService,
            CustomReportLibraryNodeService customReportLibraryNodeService,
            Provider<JsonCustomReportDashboardBuilder> builderProvider,
            CampaignLibraryNavigationService campaignLibraryNavigationService) {
        this.campaignDisplayService = campaignDisplayService;
        this.customReportLibraryNodeService = customReportLibraryNodeService;
        this.builderProvider = builderProvider;
        this.campaignLibraryNavigationService = campaignLibraryNavigationService;
    }

    @PostMapping
    public CampaignMultiSelectionDto getCampaignMultiView(
            @RequestBody CampaignMultiViewStatisticsPatch patch, Locale locale) {
        NodeReferences nodeReferences = patch.references.asNodeReferences();
        CampaignMultiSelectionDto multiSelectionDto = new CampaignMultiSelectionDto();
        if (campaignDisplayService.isMultiSelectionScopeValid(nodeReferences)) {
            multiSelectionDto = campaignDisplayService.getCampaignMultiView(nodeReferences);
            multiSelectionDto.setLastExecutionScope(patch.lastExecutionScope);

            List<String> references = patch.references.getReferences();
            List<EntityReference> entityReferences =
                    references.stream().map(EntityReference::fromNodeId).toList();

            if (multiSelectionDto.isShouldShowFavoriteDashboard()
                    && multiSelectionDto.isCanShowFavoriteDashboard()) {
                CustomReportDashboard dashboard =
                        customReportLibraryNodeService.findCustomReportDashboardById(
                                multiSelectionDto.getFavoriteDashboardId());
                CustomReportDashboardDto dto =
                        new CustomReportDashboardDto(
                                Workspace.CAMPAIGN, entityReferences, false, false, patch.lastExecutionScope);
                JsonCustomReportDashboard jsonDashboard =
                        builderProvider
                                .get()
                                .build(multiSelectionDto.getFavoriteDashboardId(), dashboard, locale, dto);
                multiSelectionDto.setDashboard(jsonDashboard);

            } else if (!multiSelectionDto.isShouldShowFavoriteDashboard()) {
                StatisticsBundle bundle =
                        campaignLibraryNavigationService.compileStatisticsFromSelection(
                                nodeReferences, entityReferences, patch.lastExecutionScope);
                multiSelectionDto.setStatistics(bundle);
            }
        }

        return multiSelectionDto;
    }

    public record CampaignMultiViewStatisticsPatch(boolean lastExecutionScope, NodeList references) {}
}
