/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.campaign;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletResponse;
import org.springframework.context.MessageSource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.core.foundation.exception.ActionException;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.domain.NodeWorkspace;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.Sprint;
import org.squashtest.tm.domain.campaign.SprintGroup;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.campaign.export.CampaignExportCSVModel;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.exception.library.RightsUnsuficientsForOperationException;
import org.squashtest.tm.service.campaign.CampaignFinder;
import org.squashtest.tm.service.campaign.CampaignLibraryNavigationService;
import org.squashtest.tm.service.campaign.IterationModificationService;
import org.squashtest.tm.service.clipboard.model.ClipboardPayload;
import org.squashtest.tm.service.deletion.CampaignLibraryNodesToDelete;
import org.squashtest.tm.service.deletion.OperationReport;
import org.squashtest.tm.service.deletion.SuppressionPreviewReport;
import org.squashtest.tm.service.display.workspace.tree.MultipleHierarchyTreeBrowser;
import org.squashtest.tm.service.display.workspace.tree.TreeBrowser;
import org.squashtest.tm.service.display.workspace.tree.TreeNodeCollectorService;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.TreeGridResponse;
import org.squashtest.tm.service.internal.display.grid.TreeRequest;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;
import org.squashtest.tm.web.backend.controller.RequestParams;
import org.squashtest.tm.web.backend.controller.form.model.CampaignFolderFormModel;
import org.squashtest.tm.web.backend.controller.form.model.CampaignFormModel;
import org.squashtest.tm.web.backend.controller.form.model.EntityFormModel;
import org.squashtest.tm.web.backend.controller.form.model.EntityFormModelValidator;
import org.squashtest.tm.web.backend.controller.form.model.IterationFormModel;
import org.squashtest.tm.web.backend.controller.form.model.NodeList;
import org.squashtest.tm.web.backend.controller.form.model.NodeListAndWhiteList;
import org.squashtest.tm.web.backend.controller.form.model.RefreshTreeNodeModel;
import org.squashtest.tm.web.backend.controller.form.model.SprintFormModel;
import org.squashtest.tm.web.backend.controller.form.model.SprintGroupFormModel;
import org.squashtest.tm.web.backend.controller.form.model.TestSuiteFormModel;
import org.squashtest.tm.web.backend.controller.navigation.Messages;

@RestController
@RequestMapping(path = "backend/campaign-tree")
public class CampaignLibraryNavigationController {

    public static final Logger LOGGER =
            LoggerFactory.getLogger(CampaignLibraryNavigationController.class);

    private static final String ADD_CAMPAIGN = "add-campaign";
    private static final String ADD_SPRINT = "add-sprint";
    private static final String ADD_SPRINT_GROUP = "add-sprint-group";
    private static final String ADD_ITERATION = "add-iteration";
    private static final String ADD_CAMPAIGN_FOLDER = "add-campaign-folder";
    private static final String REMOVE_FROM_ITER = "remove_from_iter";
    private static final String ENTITY_TYPE_IS_NOT_HANDLED = "This entity type is not handled ";

    private final TreeBrowser treeBrowser;
    private final CampaignLibraryNavigationService campaignLibraryNavigationService;
    private final TreeNodeCollectorService treeNodeCollectorService;
    private final IterationModificationService iterationModificationService;
    private final MessageSource messageSource;
    private final CampaignFinder campaignFinder;

    public CampaignLibraryNavigationController(
            MultipleHierarchyTreeBrowser treeBrowser,
            CampaignLibraryNavigationService campaignLibraryNavigationService,
            TreeNodeCollectorService treeNodeCollectorService,
            IterationModificationService iterationModificationService,
            MessageSource messageSource,
            CampaignFinder campaignFinder) {
        this.treeBrowser = treeBrowser;
        this.campaignLibraryNavigationService = campaignLibraryNavigationService;
        this.treeNodeCollectorService = treeNodeCollectorService;
        this.iterationModificationService = iterationModificationService;
        this.messageSource = messageSource;
        this.campaignFinder = campaignFinder;
    }

    @PostMapping
    public TreeGridResponse getInitialRows(@RequestBody TreeRequest treeRequest) {
        return treeBrowser.getInitialTree(
                NodeWorkspace.CAMPAIGN,
                NodeReference.fromNodeIds(treeRequest.getOpenedNodes()),
                NodeReference.fromNodeIds(treeRequest.getSelectedNodes()));
    }

    @GetMapping("/{ids}/content")
    public TreeGridResponse getChildren(@PathVariable List<String> ids) {
        Set<NodeReference> nodeReference = NodeReference.fromNodeIds(ids);
        return treeBrowser.findSubHierarchy(nodeReference, new HashSet<>(nodeReference));
    }

    @PostMapping("/refresh")
    public TreeGridResponse refreshNodes(@RequestBody RefreshTreeNodeModel refreshTreeNodeModel) {
        return treeBrowser.findSubHierarchy(
                NodeReference.fromNodeIds(refreshTreeNodeModel.getNodeIds()),
                new HashSet<>(refreshTreeNodeModel.getNodeList().getNodeReferences()));
    }

    @PostMapping("new-campaign")
    public DataRow addNewCampaign(@RequestBody CampaignFormModel campaignFormModel)
            throws BindException {
        validateFormModel(campaignFormModel, ADD_CAMPAIGN);

        Campaign campaign = campaignFormModel.getCampaign();
        campaign.setActualStartAuto(true);
        campaign.setActualEndAuto(true);
        Map<Long, RawValue> customFieldValues = campaignFormModel.getCufs();

        String serializedParentEntityReference = campaignFormModel.getParentEntityReference();
        EntityReference parentEntityReference =
                EntityReference.fromNodeId(serializedParentEntityReference);
        switch (parentEntityReference.getType()) {
            case CAMPAIGN_LIBRARY:
                campaignLibraryNavigationService.addCampaignToCampaignLibrary(
                        parentEntityReference.getId(), campaign, customFieldValues);
                break;
            case CAMPAIGN_FOLDER:
                campaignLibraryNavigationService.addCampaignToCampaignFolder(
                        parentEntityReference.getId(), campaign, customFieldValues);
                break;
            default:
                throw new IllegalArgumentException(
                        ENTITY_TYPE_IS_NOT_HANDLED + serializedParentEntityReference);
        }
        return treeNodeCollectorService.collectNode(NodeType.CAMPAIGN, campaign);
    }

    @PostMapping("new-sprint")
    public DataRow addNewSprint(@RequestBody SprintFormModel sprintFormModel) throws BindException {
        validateFormModel(sprintFormModel, ADD_SPRINT);

        Sprint sprint = sprintFormModel.getSprint();
        Map<Long, RawValue> customFieldValues = sprintFormModel.getCufs();

        String serializedParentEntityReference = sprintFormModel.getParentEntityReference();
        EntityReference parentEntityReference =
                EntityReference.fromNodeId(serializedParentEntityReference);
        switch (parentEntityReference.getType()) {
            case CAMPAIGN_LIBRARY ->
                    campaignLibraryNavigationService.addSprintToCampaignLibrary(
                            parentEntityReference.getId(), sprint, customFieldValues);
            case CAMPAIGN_FOLDER ->
                    campaignLibraryNavigationService.addSprintToCampaignFolder(
                            parentEntityReference.getId(), sprint, customFieldValues);
            case SPRINT_GROUP ->
                    campaignLibraryNavigationService.addSprintToSprintGroup(
                            parentEntityReference.getId(), sprint, customFieldValues);
            default ->
                    throw new IllegalArgumentException(
                            ENTITY_TYPE_IS_NOT_HANDLED + serializedParentEntityReference);
        }
        return treeNodeCollectorService.collectNode(NodeType.SPRINT, sprint);
    }

    @PostMapping("new-sprint-group")
    public DataRow addNewSprintGroup(@RequestBody SprintGroupFormModel sprintGroupFormModel)
            throws BindException {
        validateFormModel(sprintGroupFormModel, ADD_SPRINT_GROUP);

        SprintGroup sprintGroup = sprintGroupFormModel.getSprintGroup();
        Map<Long, RawValue> customFieldValues = sprintGroupFormModel.getCufs();

        String serializedParentEntityReference = sprintGroupFormModel.getParentEntityReference();
        EntityReference parentEntityReference =
                EntityReference.fromNodeId(serializedParentEntityReference);
        switch (parentEntityReference.getType()) {
            case CAMPAIGN_LIBRARY ->
                    campaignLibraryNavigationService.addSprintGroupToCampaignLibrary(
                            parentEntityReference.getId(), sprintGroup, customFieldValues);
            case CAMPAIGN_FOLDER ->
                    campaignLibraryNavigationService.addSprintGroupToCampaignFolder(
                            parentEntityReference.getId(), sprintGroup, customFieldValues);
            default ->
                    throw new IllegalArgumentException(
                            ENTITY_TYPE_IS_NOT_HANDLED + serializedParentEntityReference);
        }
        return treeNodeCollectorService.collectNode(NodeType.SPRINT_GROUP, sprintGroup);
    }

    @PostMapping("campaign/{campaignId}/new-iteration")
    public DataRow addNewIterationToCampaign(
            @PathVariable long campaignId, @RequestBody IterationFormModel iterationForm)
            throws BindException {
        validateFormModel(iterationForm, ADD_ITERATION);

        Iteration newIteration = iterationForm.getIteration();
        newIteration.setActualStartAuto(true);
        newIteration.setActualEndAuto(true);
        Map<Long, RawValue> customFieldValues = iterationForm.getCufs();
        boolean copyTestPlan = iterationForm.isCopyTestPlan();

        iterationModificationService.addIterationToCampaign(
                newIteration, campaignId, copyTestPlan, customFieldValues);

        return treeNodeCollectorService.collectNode(NodeType.ITERATION, newIteration);
    }

    @PostMapping("new-folder")
    public DataRow addNewFolder(@RequestBody CampaignFolderFormModel folderModel)
            throws BindException {

        validateCampaignFolderFormModel(folderModel);

        CampaignFolder campaignFolder = folderModel.getCampaignFolder();

        Map<Long, RawValue> customFieldValues = folderModel.getCufs();

        String serializedParentEntityReference = folderModel.getParentEntityReference();
        EntityReference parentEntityReference =
                EntityReference.fromNodeId(serializedParentEntityReference);
        switch (parentEntityReference.getType()) {
            case CAMPAIGN_LIBRARY:
                campaignLibraryNavigationService.addFolderToLibrary(
                        parentEntityReference.getId(), campaignFolder, customFieldValues);
                break;
            case CAMPAIGN_FOLDER:
                campaignLibraryNavigationService.addFolderToFolder(
                        parentEntityReference.getId(), campaignFolder, customFieldValues);
                break;
            case SPRINT_GROUP:
                campaignLibraryNavigationService.addFolderToSprintGroup(
                        parentEntityReference.getId(), campaignFolder, customFieldValues);
                break;
            default:
                throw new IllegalArgumentException(
                        ENTITY_TYPE_IS_NOT_HANDLED + serializedParentEntityReference);
        }
        return treeNodeCollectorService.collectNode(NodeType.CAMPAIGN_FOLDER, campaignFolder);
    }

    @PostMapping("new-test-suite")
    public DataRow addTestSuite(@RequestBody TestSuiteFormModel suiteFormModel) {

        TestSuite suite = suiteFormModel.getTestSuite();
        Map<Long, RawValue> customFieldValues = suiteFormModel.getCufs();
        String serializedParentEntityReference = suiteFormModel.getParentEntityReference();
        EntityReference parentEntityReference =
                EntityReference.fromNodeId(serializedParentEntityReference);
        campaignLibraryNavigationService.addTestSuiteToIteration(
                parentEntityReference.getId(), suite, customFieldValues);
        return treeNodeCollectorService.collectNode(NodeType.TEST_SUITE, suite);
    }

    @PostMapping("/{destinationId}/content/paste")
    public void pasteNodes(
            @RequestBody() NodeListAndWhiteList nodeListAndWhiteList,
            @PathVariable("destinationId") String destinationId) {
        NodeReference nodeReference = NodeReference.fromNodeId(destinationId);
        List<Long> copiedNodeIds = nodeListAndWhiteList.getIds();
        ClipboardPayload clipboardPayload = nodeListAndWhiteList.asClipboardPayload();

        try {
            switch (nodeReference.getNodeType()) {
                case CAMPAIGN_FOLDER:
                    campaignLibraryNavigationService.copyNodesToFolder(
                            nodeReference.getId(), copiedNodeIds.toArray(new Long[0]), clipboardPayload);
                    break;
                case CAMPAIGN_LIBRARY:
                    campaignLibraryNavigationService.copyNodesToLibrary(
                            nodeReference.getId(), copiedNodeIds.toArray(new Long[0]), clipboardPayload);
                    break;
                case CAMPAIGN:
                    campaignLibraryNavigationService.copyIterationsToCampaign(
                            nodeReference.getId(), copiedNodeIds.toArray(new Long[0]), clipboardPayload);
                    break;
                case SPRINT_GROUP:
                    campaignLibraryNavigationService.copyNodesToSprintGroup(
                            nodeReference.getId(), copiedNodeIds.toArray(new Long[0]), clipboardPayload);
                    break;
                case ITERATION:
                    iterationModificationService.copyPasteTestSuitesToIteration(
                            copiedNodeIds.toArray(new Long[0]), nodeReference.getId(), clipboardPayload);
                    break;
                default:
                    throw new IllegalArgumentException(
                            "copy nodes : specified destination type doesn't exists : "
                                    + nodeReference.getNodeType());
            }
        } catch (AccessDeniedException ade) {
            throw new RightsUnsuficientsForOperationException(ade);
        }
    }

    @PostMapping("/{destinationRef}/content/move")
    public void moveNodes(
            @RequestBody() NodeList nodeList, @PathVariable("destinationRef") String destinationRef) {
        NodeReference nodeReference = NodeReference.fromNodeId(destinationRef);
        Long destinationId = nodeReference.getId();
        NodeType destinationType = nodeReference.getNodeType();
        Long[] movedNodeIds = nodeList.getIds().toArray(new Long[0]);
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(nodeList.getIds());

        try {
            switch (destinationType) {
                case CAMPAIGN_LIBRARY:
                    campaignLibraryNavigationService.moveNodesToLibrary(
                            destinationId, movedNodeIds, clipboardPayload);
                    break;
                case CAMPAIGN_FOLDER:
                    campaignLibraryNavigationService.moveNodesToFolder(
                            destinationId, movedNodeIds, clipboardPayload);
                    break;
                case SPRINT_GROUP:
                    campaignLibraryNavigationService.moveNodesToSprintGroup(
                            destinationId, movedNodeIds, clipboardPayload);
                    break;
                default:
                    throw new IllegalArgumentException(
                            "move nodes : specified destination type doesn't exists : " + destinationType);
            }
        } catch (AccessDeniedException ade) {
            throw new RightsUnsuficientsForOperationException(ade);
        }
    }

    @PostMapping("/{destinationRef}/content/move/{position}")
    public void moveNodesAtPosition(
            @RequestBody() NodeList nodeList,
            @PathVariable("destinationRef") String destinationRef,
            @PathVariable("position") int position) {
        NodeReference nodeReference = NodeReference.fromNodeId(destinationRef);
        Long destinationId = nodeReference.getId();
        NodeType destinationType = nodeReference.getNodeType();
        Long[] movedNodeIds = nodeList.getIds().toArray(new Long[0]);
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(nodeList.getIds());

        try {
            switch (destinationType) {
                case CAMPAIGN_LIBRARY:
                    campaignLibraryNavigationService.moveNodesToLibrary(
                            destinationId, movedNodeIds, position, clipboardPayload);
                    break;
                case CAMPAIGN_FOLDER:
                    campaignLibraryNavigationService.moveNodesToFolder(
                            destinationId, movedNodeIds, position, clipboardPayload);
                    break;
                case CAMPAIGN:
                    campaignLibraryNavigationService.moveIterationsWithinCampaign(
                            destinationId, movedNodeIds, position);
                    break;
                case ITERATION:
                    iterationModificationService.changeTestSuitePosition(
                            destinationId, position, nodeList.getIds());
                    break;
                default:
                    throw new IllegalArgumentException(
                            "move nodes : specified destination type doesn't exists : " + destinationType);
            }
        } catch (AccessDeniedException ade) {
            throw new RightsUnsuficientsForOperationException(ade);
        }
    }

    @GetMapping("/deletion-simulation/{nodeIds}")
    public Messages simulateNodesDeletion(
            @PathVariable(RequestParams.NODE_IDS) List<String> nodeIds, Locale locale) {
        CampaignLibraryNodesToDelete deletionNodes = CampaignLibraryNodesToDelete.from(nodeIds);
        List<Long> campaignLibraryNodeIdsToDelete = deletionNodes.getCampaignLibraryNodeIds();

        Messages messages = new Messages();

        if (!campaignLibraryNodeIdsToDelete.isEmpty()) {
            List<SuppressionPreviewReport> reportList =
                    campaignLibraryNavigationService.simulateDeletion(campaignLibraryNodeIdsToDelete);
            messages.addMessages(reportList, messageSource, locale);
        }

        List<SuppressionPreviewReport> reportIterationList =
                campaignLibraryNavigationService.simulateIterationDeletion(deletionNodes.getIterationIds());
        messages.addMessages(reportIterationList, messageSource, locale);
        List<SuppressionPreviewReport> reportSuiteList =
                campaignLibraryNavigationService.simulateSuiteDeletion(deletionNodes.getSuiteIds());
        messages.addMessages(reportSuiteList, messageSource, locale);
        return messages;
    }

    @DeleteMapping(
            value = "/{nodeIds}",
            params = {REMOVE_FROM_ITER})
    public OperationReport confirmNodeDeletions(
            @PathVariable(RequestParams.NODE_IDS) List<String> nodeIds,
            @RequestParam(REMOVE_FROM_ITER) boolean removeFromIter) {

        CampaignLibraryNodesToDelete deletionNodes = CampaignLibraryNodesToDelete.from(nodeIds);
        return campaignLibraryNavigationService.deleteNodes(deletionNodes, removeFromIter);
    }

    @PostMapping("campaign/{campaignId}/new-iteration-with-items")
    public void addIterationToCampaignWithItems(
            @PathVariable long campaignId, @RequestBody IterationCreation patch) {
        this.iterationModificationService.createIterationWithItemCopies(
                campaignId, patch.name(), patch.description(), patch.itemTestPlanIds());
    }

    public record IterationCreation(String name, String description, List<Long> itemTestPlanIds) {}

    @GetMapping(value = "/export-campaign/{campaignId}", params = "export=csv")
    public FileSystemResource exportCampaign(
            @PathVariable(RequestParams.CAMPAIGN_ID) long campaignId,
            @RequestParam(value = "exportType", defaultValue = "S") String exportType,
            HttpServletResponse response) {

        Campaign campaign = campaignFinder.findById(campaignId);
        CampaignExportCSVModel model =
                campaignLibraryNavigationService.exportCampaignToCSV(campaignId, exportType);

        final String fileName = buildFileName(exportType, campaign);

        // prepare the response
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

        File exported = exportToFile(model);
        return new FileSystemResource(exported);
    }

    private static String buildFileName(String exportType, Campaign campaign) {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String encodedCampaignName = URLEncoder.encode(campaign.getName(), StandardCharsets.UTF_8);
        return String.format(
                "EXPORT_CPG_%s_%s_%s.csv",
                exportType, encodedCampaignName.replace(" ", "_"), sdf.format(new Date()));
    }

    private void validateCampaignFolderFormModel(CampaignFolderFormModel folderModel)
            throws BindException {
        BindingResult validation = new BeanPropertyBindingResult(folderModel, ADD_CAMPAIGN_FOLDER);
        EntityFormModelValidator entityFormModelValidator = new EntityFormModelValidator();
        entityFormModelValidator.validate(folderModel, validation);

        if (validation.hasErrors()) {
            throw new BindException(validation);
        }
    }

    private void validateFormModel(EntityFormModel formModel, String objectName)
            throws BindException {
        BindingResult validation = new BeanPropertyBindingResult(formModel, objectName);
        EntityFormModelValidator validator = new EntityFormModelValidator();
        validator.validate(formModel, validation);

        if (validation.hasErrors()) {
            throw new BindException(validation);
        }
    }

    private File exportToFile(CampaignExportCSVModel model) {
        try {
            File file = File.createTempFile("export-requirement", "tmp");
            file.deleteOnExit();
            return writeModel(model, file);
        } catch (IOException e) {
            throw new ActionException(
                    "campaign export : I/O failure while creating the temporary file", e);
        }
    }

    private File writeModel(CampaignExportCSVModel model, File file) throws FileNotFoundException {
        try (PrintWriter writer = new PrintWriter(file)) {
            // print header
            CampaignExportCSVModel.Row header = model.getHeader();
            writer.write(header.toString() + "\n");

            // print the rest
            Iterator<CampaignExportCSVModel.Row> iterator = model.dataIterator();

            while (iterator.hasNext()) {
                final CampaignExportCSVModel.Row datarow = iterator.next();
                final String cleanRowValue =
                        HTMLCleanupUtils.htmlToTrimmedText(datarow.toString())
                                .replace("\n", "")
                                .replace("\r", "");
                writer.write(cleanRowValue + "\n");
            }

            return file;
        }
    }
}
