/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.security.authentication;

import javax.inject.Inject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.core.Ordered;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.oauth2.client.authentication.OAuth2LoginAuthenticationToken;
import org.squashtest.tm.api.security.authentication.AuthenticationProviderFeatures;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.service.internal.security.AuthenticationProviderContext;
import org.squashtest.tm.service.plugin.PluginFinderService;
import org.squashtest.tm.service.security.AuthenticatedMissingUserCreatorService;
import org.squashtest.tm.service.security.OidcUserCreatorService;
import org.squashtest.tm.web.backend.context.ApplicationComponent;

/**
 * This class checks if an {@link User} matches the authenticated user. If not, creates this user.
 *
 * @author Gregory Fouquet
 */
@ApplicationComponent
public class AuthenticatedMissingUserCreator
        implements ApplicationListener<AuthenticationSuccessEvent>, Ordered {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(AuthenticatedMissingUserCreator.class);

    @Inject private AuthenticationProviderContext authProviderContext;

    @Inject private AuthenticatedMissingUserCreatorService authenticatedMissingUserCreatorService;

    @Autowired(required = false)
    OidcUserCreatorService oidcUserCreatorService;

    @Inject PluginFinderService pluginFinderService;

    public AuthenticatedMissingUserCreator() {
        super();
        LOGGER.info("created");
    }

    // we must ensure that the user exist
    // before any other event listener that rely on its existence
    // kick in
    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE + 1;
    }

    /**
     * @see
     *     org.springframework.context.ApplicationListener#onApplicationEvent(org.springframework.context.ApplicationEvent)
     */
    @Override
    public void onApplicationEvent(AuthenticationSuccessEvent event) {

        AuthenticationProviderFeatures features =
                authProviderContext.getProviderFeatures(event.getAuthentication());
        if (features.shouldCreateMissingUser()) {
            authenticatedMissingUserCreatorService.createMissingUser(event.getAuthentication());
        } else if (event.getAuthentication() instanceof OAuth2LoginAuthenticationToken
                && pluginFinderService.isOpenIdConnectPluginInstalled()) {
            oidcUserCreatorService.createMissingSquashUser(event.getAuthentication());
        }
    }
}
