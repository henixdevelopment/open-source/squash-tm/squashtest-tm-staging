/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.export;

import java.io.File;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.context.MessageSource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.testcase.TestCaseLibraryNavigationService;
import org.squashtest.tm.web.backend.controller.RequestParams;

@RestController
@RequestMapping(value = "backend/test-case/export")
public class TestCaseExportController {

    private static final String CALLS = "calls";
    private static final String TEST_CASE_SEARCH = "test-case-search";

    private final MessageSource messageSource;

    private final TestCaseLibraryNavigationService testCaseLibraryNavigationService;

    public TestCaseExportController(
            MessageSource messageSource,
            TestCaseLibraryNavigationService testCaseLibraryNavigationService) {
        this.messageSource = messageSource;
        this.testCaseLibraryNavigationService = testCaseLibraryNavigationService;
    }

    @GetMapping(
            value = "/content/xls",
            produces = RequestParams.APPLICATION_SLASH_OCTET_STREAM,
            params = {
                RequestParams.FILENAME,
                RequestParams.LIBRARIES,
                RequestParams.NODES,
                CALLS,
                RequestParams.RTEFORMAT
            })
    public FileSystemResource exportAsExcel(
            @RequestParam(RequestParams.FILENAME) String filename,
            @RequestParam(RequestParams.LIBRARIES) List<Long> libraryIds,
            @RequestParam(RequestParams.NODES) List<Long> nodeIds,
            @RequestParam(CALLS) Boolean includeCalledTests,
            @RequestParam(RequestParams.RTEFORMAT) Boolean keepRteFormat,
            HttpServletResponse response) {

        response.setContentType(RequestParams.APPLICATION_SLASH_OCTET_STREAM);
        response.setHeader(
                RequestParams.CONTENT_DISPOSITION, RequestParams.ATTACHMENT_FILENAME + filename + ".xls");

        File export =
                testCaseLibraryNavigationService.exportTestCaseAsExcel(
                        libraryIds, nodeIds, includeCalledTests, keepRteFormat, messageSource);
        return new FileSystemResource(export);
    }

    @GetMapping(
            value = "/content/keyword-scripts",
            produces = RequestParams.APPLICATION_SLASH_OCTET_STREAM,
            params = {RequestParams.FILENAME, RequestParams.LIBRARIES, RequestParams.NODES})
    public FileSystemResource exportKeywordScripts(
            @RequestParam(RequestParams.FILENAME) String filename,
            @RequestParam(RequestParams.LIBRARIES) List<Long> libraryIds,
            @RequestParam(RequestParams.NODES) List<Long> nodeIds,
            HttpServletResponse response) {
        response.setContentType(RequestParams.APPLICATION_SLASH_OCTET_STREAM);
        response.setHeader(
                RequestParams.CONTENT_DISPOSITION, RequestParams.ATTACHMENT_FILENAME + filename + ".zip");
        File file =
                testCaseLibraryNavigationService.exportKeywordTestCaseAsScriptFiles(
                        libraryIds, nodeIds, messageSource);
        return new FileSystemResource(file);
    }

    @GetMapping(
            value = "/content/features",
            produces = RequestParams.APPLICATION_SLASH_OCTET_STREAM,
            params = {RequestParams.FILENAME, RequestParams.LIBRARIES, RequestParams.NODES})
    public FileSystemResource exportGherkinFeatures(
            @RequestParam(RequestParams.FILENAME) String filename,
            @RequestParam(RequestParams.LIBRARIES) List<Long> libraryIds,
            @RequestParam(RequestParams.NODES) List<Long> nodeIds,
            HttpServletResponse response) {
        response.setContentType(RequestParams.APPLICATION_SLASH_OCTET_STREAM);
        response.setHeader(
                RequestParams.CONTENT_DISPOSITION, RequestParams.ATTACHMENT_FILENAME + filename + ".zip");
        File file =
                testCaseLibraryNavigationService.exportGherkinTestCaseAsFeatureFiles(
                        libraryIds, nodeIds, messageSource);
        return new FileSystemResource(file);
    }

    @GetMapping(
            value = "/searchExports",
            produces = RequestParams.APPLICATION_SLASH_OCTET_STREAM,
            params = {RequestParams.FILENAME, RequestParams.NODES, CALLS, RequestParams.RTEFORMAT})
    public FileSystemResource searchExportAsExcel(
            @RequestParam(RequestParams.FILENAME) String filename,
            @RequestParam(RequestParams.NODES) List<Long> nodeIds,
            @RequestParam(CALLS) Boolean includeCalledTests,
            @RequestParam(RequestParams.TYPE) String type,
            @RequestParam(RequestParams.RTEFORMAT) Boolean keepRteFormat,
            @RequestParam(RequestParams.SIMPLIFIED_COLUMN_DISPLAY_GRID_IDS)
                    List<String> simplifiedColumnDisplayGridIds,
            HttpServletResponse response) {

        response.setContentType(RequestParams.APPLICATION_SLASH_OCTET_STREAM);
        response.setHeader(
                RequestParams.CONTENT_DISPOSITION, RequestParams.ATTACHMENT_FILENAME + filename + ".xls");
        boolean simplifiedColumnDisplay = simplifiedColumnDisplayGridIds.contains(TEST_CASE_SEARCH);
        File export =
                testCaseLibraryNavigationService.searchExportTestCaseAsExcel(
                        nodeIds,
                        includeCalledTests,
                        keepRteFormat,
                        messageSource,
                        type,
                        simplifiedColumnDisplay);
        return new FileSystemResource(export);
    }
}
