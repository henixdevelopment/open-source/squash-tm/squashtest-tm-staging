/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.attachment;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import javax.inject.Inject;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.HtmlUtils;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.attachment.Attachment;
import org.squashtest.tm.service.attachment.AttachmentManagerService;
import org.squashtest.tm.service.attachment.UploadedData;
import org.squashtest.tm.service.display.attachment.AttachmentDisplayService;
import org.squashtest.tm.service.internal.display.dto.AttachmentDto;
import org.squashtest.tm.service.internal.display.dto.AttachmentListDto;
import org.squashtest.tm.web.backend.fileupload.UploadContentFilterUtil;
import org.squashtest.tm.web.backend.fileupload.UploadSummary;

@RestController
@RequestMapping("backend/attach-list/{attachListId}")
public class AttachmentViewController {

    private static final String STR_UPLOAD_STATUS_OK = "dialog.attachment.summary.statusok.label";
    private static final String STR_UPLOAD_STATUS_WRONGFILETYPE =
            "dialog.attachment.summary.statuswrongtype.label";

    private static final Logger LOGGER = LoggerFactory.getLogger(AttachmentViewController.class);

    @Inject private AttachmentManagerService attachmentManagerService;

    @Inject private AttachmentDisplayService attachmentDisplayService;

    @Inject private MessageSource messageSource;

    @Inject private UploadContentFilterUtil filterUtil;

    @InitBinder
    public void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
        binder.registerCustomEditor(UploadedData.class, new UploadedDataPropertyEditorSupport());
    }

    @GetMapping()
    public AttachmentListDto getAttachmentList(@PathVariable long attachListId) {
        return attachmentDisplayService.getAttachmentList(attachListId);
    }

    @DeleteMapping(value = "/attachments/{attachmentIds}")
    public void removeAttachment(
            @PathVariable long attachListId,
            @PathVariable("attachmentIds") List<Long> attachmentIds,
            @RequestParam("entityId") long entityId,
            @RequestParam("entityType") String entityType,
            @RequestParam("holderType") String holderTypeName,
            HttpServletResponse response)
            throws IOException {

        final EntityReference entityRef = getEntityReference(entityType, entityId);
        final EntityType holderType = getEntityType(holderTypeName);

        try {
            attachmentManagerService.removeListOfAttachments(
                    attachListId, attachmentIds, entityRef, holderType);
        } catch (AccessDeniedException | IllegalArgumentException e) {
            LOGGER.warn("Error happened during attachment deletion", e);
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    @PostMapping(value = "/attachments/upload", produces = "application/json")
    public List<UploadSummary> uploadAttachmentAsJson(
            @RequestParam("attachment[]") List<UploadedData> attachments,
            @PathVariable long attachListId,
            @RequestParam("entityId") long entityId,
            @RequestParam("entityType") String entityType,
            @RequestParam("holderType") String holderTypeName,
            Locale locale)
            throws IOException {

        List<UploadSummary> summary = new LinkedList<>();

        final EntityReference entityRef = getEntityReference(entityType, entityId);
        final EntityType holderType = getEntityType(holderTypeName);

        for (UploadedData upload : attachments) {

            LOGGER.trace("Adding attachment {}", upload.getName());

            // file type checking
            boolean shouldProceed = filterUtil.isTypeAllowed(upload);

            if (!shouldProceed) {
                AttachmentDto refusedAttachmentDto = new AttachmentDto();
                refusedAttachmentDto.setName(HtmlUtils.htmlEscape(upload.getName()));

                summary.add(
                        new UploadSummary(
                                refusedAttachmentDto,
                                getUploadSummary(STR_UPLOAD_STATUS_WRONGFILETYPE, locale),
                                UploadSummary.INT_UPLOAD_STATUS_WRONGFILETYPE));
            } else {
                AttachmentDto newAttachmentDto =
                        attachmentManagerService.addAttachment(attachListId, upload, entityRef, holderType);

                summary.add(
                        new UploadSummary(
                                newAttachmentDto,
                                getUploadSummary(STR_UPLOAD_STATUS_OK, locale),
                                UploadSummary.INT_UPLOAD_STATUS_OK));
            }
        }
        return summary;
    }

    @PostMapping(value = "/attachment/upload-image", produces = "application/json")
    public List<UploadSummary> uploadImageFile(
            @RequestParam("attachment") UploadedData uploaded,
            @PathVariable long attachListId,
            @RequestParam("entityId") long entityId,
            @RequestParam("entityType") String entityType,
            @RequestParam("holderType") String holderTypeName,
            Locale locale)
            throws IOException {

        final EntityReference entityRef = getEntityReference(entityType, entityId);
        final EntityType holderType = getEntityType(holderTypeName);

        LOGGER.trace("Adding image attachment {}", uploaded.getName());

        // image file type checking
        boolean shouldProceed = filterUtil.isImageType(uploaded);

        if (!shouldProceed) {
            AttachmentDto refusedAttachmentDto = new AttachmentDto();
            refusedAttachmentDto.setName(HtmlUtils.htmlEscape(uploaded.getName()));

            return Collections.singletonList(
                    new UploadSummary(
                            refusedAttachmentDto,
                            getUploadSummary(STR_UPLOAD_STATUS_WRONGFILETYPE, locale),
                            UploadSummary.INT_UPLOAD_STATUS_WRONGFILETYPE));
        } else {
            AttachmentDto newAttachmentDto =
                    attachmentManagerService.addAttachment(attachListId, uploaded, entityRef, holderType);
            return Collections.singletonList(
                    new UploadSummary(
                            newAttachmentDto,
                            getUploadSummary(STR_UPLOAD_STATUS_OK, locale),
                            UploadSummary.INT_UPLOAD_STATUS_OK));
        }
    }

    @GetMapping(value = "/attachments/download/{attachemendId}")
    public void downloadAttachment(
            @PathVariable("attachemendId") long attachmentId, HttpServletResponse response) {

        try {
            Attachment attachment = attachmentManagerService.findAttachment(attachmentId);
            response.setContentType("application/octet-stream");
            response.setHeader(
                    "Content-Disposition",
                    "attachment; filename=\"" + attachment.getName().replace(" ", "_") + "\"");

            ServletOutputStream outStream = response.getOutputStream();

            attachmentManagerService.writeContent(attachmentId, outStream);
        } catch (IOException e) {
            LOGGER.warn("Error happened during attachment download", e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } catch (AccessDeniedException | IllegalArgumentException e) {
            LOGGER.warn("Error happened during attachment download", e);
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    private String getUploadSummary(String key, Locale locale) {
        return messageSource.getMessage(key, null, locale);
    }

    private EntityReference getEntityReference(String entityType, long entityId) {
        String className = StringUtils.capitalize(entityType);
        EntityType type = EntityType.fromSimpleName(className);
        return new EntityReference(type, entityId);
    }

    private EntityType getEntityType(String entityType) {
        String className = StringUtils.capitalize(entityType);
        return EntityType.fromSimpleName(className);
    }
}
