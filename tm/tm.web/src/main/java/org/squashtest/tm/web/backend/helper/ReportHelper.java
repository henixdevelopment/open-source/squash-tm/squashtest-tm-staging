/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.helper;

import static org.apache.commons.lang3.StringUtils.EMPTY;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import javax.inject.Inject;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.squashtest.tm.api.report.Report;
import org.squashtest.tm.api.report.criteria.Criteria;
import org.squashtest.tm.api.report.form.CheckboxInput;
import org.squashtest.tm.api.report.form.CheckboxesGroup;
import org.squashtest.tm.api.report.form.ContainerOption;
import org.squashtest.tm.api.report.form.DateInput;
import org.squashtest.tm.api.report.form.DropdownList;
import org.squashtest.tm.api.report.form.Input;
import org.squashtest.tm.api.report.form.InputType;
import org.squashtest.tm.api.report.form.InputsGroup;
import org.squashtest.tm.api.report.form.OptionInput;
import org.squashtest.tm.api.report.form.RadioButtonsGroup;
import org.squashtest.tm.api.report.form.TemplateDropdownList;
import org.squashtest.tm.api.report.form.TextInput;
import org.squashtest.tm.api.report.form.TreePicker;
import org.squashtest.tm.api.utils.CurrentUserHelper;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.report.ReportDefinition;
import org.squashtest.tm.service.display.campaign.CampaignDisplayService;
import org.squashtest.tm.service.display.campaign.IterationDisplayService;
import org.squashtest.tm.service.milestone.MilestoneManagerService;
import org.squashtest.tm.service.project.CustomProjectModificationService;
import org.squashtest.tm.service.requirement.RequirementVersionManagerService;
import org.squashtest.tm.service.testcase.TestCaseModificationService;
import org.squashtest.tm.web.backend.report.ReportsRegistry;
import org.squashtest.tm.web.backend.report.criteria.ConciseFormToCriteriaConverter;
import org.squashtest.tm.web.backend.report.criteria.EmptyCriteria;
import org.squashtest.tm.web.backend.report.criteria.MultiOptionsCriteria;
import org.squashtest.tm.web.backend.report.criteria.MultiValuesCriteria;
import org.squashtest.tm.web.backend.report.criteria.SimpleCriteria;
import org.squashtest.tm.web.i18n.InternationalizationHelper;

/**
 * @author zyang
 */
@Component
public class ReportHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportHelper.class);

    private static final String REQUIREMENTS_IDS = "requirementsIds";
    private static final String TESTCASES_IDS = "testcasesIds";
    private static final String CAMPAIGN_IDS = "campaignIds";
    private static final String CAMPAIGN_ID = "campaignId";
    private static final String ITERATION_IDS = "iterationIds";
    private static final String ITERATION_ID = "iterationId";
    private static final String PROJECT_IDS = "projectIds";
    private static final String MILESTONES = "milestones";
    private static final String TAGS = "tags";
    private static final String OPTION = "option";

    @Inject private CampaignDisplayService campaignDisplayService;
    @Inject private IterationDisplayService iterationDisplayService;

    @Inject private RequirementVersionManagerService requirementVersionManagerService;

    @Inject private TestCaseModificationService testCaseModificationService;

    @Inject private MilestoneManagerService milestoneManagerService;

    @Inject private ReportsRegistry reportsRegistry;

    @Inject private InternationalizationHelper i18nHelper;

    @Inject private CustomProjectModificationService customProjectModificationService;

    @Inject private CurrentUserHelper currentUserHelper;

    public Map<String, List<String>> getAttributesFromReportDefinition(ReportDefinition def) {

        Report report = reportsRegistry.findReport(def.getPluginNamespace());
        Map<String, Object> form = null;
        try {
            form = JsonHelper.deserialize(def.getParameters());
        } catch (IOException e) {
            LOGGER.error("the report '{}' has corrupted parameters.", def.getName(), e);
        }

        Map<String, Criteria> crit =
                new ConciseFormToCriteriaConverter(
                                report, Collections.singletonList(def.getProject().getId()))
                        .convert(form);
        List<Long> projectIds = currentUserHelper.findReadableProjectIds();
        return getAttributesForReport(report, crit, projectIds);
    }

    public Map<String, List<String>> getAttributesForReport(
            Report report, Map<String, Criteria> criteriaMap, List<Long> projectIds) {

        Map<String, List<String>> attributes = new LinkedHashMap<>();

        Input[] inputs = report.getForm();
        for (Input input : inputs) {
            getAttributesFromInput(attributes, input, criteriaMap, projectIds);
        }

        return attributes;
    }

    @SuppressWarnings({"squid:S00107", "squid:MethodCyclomaticComplexity", "squid:S00122"})
    private void getAttributesFromInput(
            Map<String, List<String>> attributes,
            Input input,
            Map<String, Criteria> criteriaMap,
            List<Long> projectIds) {
        switch (input.getType()) {
            case RADIO_BUTTONS_GROUP ->
                    getAttributesFromRadioButtonsGroup(attributes, input, criteriaMap, projectIds);
            case CHECKBOXES_GROUP -> getAttributesFromChexBoxesGroup(attributes, input, criteriaMap);
            case INPUTS_GROUP -> getAttributesFromInputsGroup(attributes, input, criteriaMap, projectIds);
            case CHECKBOX -> getAttributesFromCheckBox(attributes, input, criteriaMap, projectIds);
            case DATE -> getAttributesFromDate(attributes, input, criteriaMap);
            case TEXT -> getAttributesFromText(attributes, input, criteriaMap);
            case DROPDOWN_LIST -> getAttributesFromDropdownList(attributes, input, criteriaMap);
            case TEMPLATE_DROPDOWN_LIST ->
                    getAttributesFromTemplateDropdownList(attributes, input, criteriaMap);
            case TREE_PICKER -> getAttributesFromTreePicker(attributes, input, criteriaMap, projectIds);
            case PROJECT_PICKER, MILESTONE_PICKER, TAG_PICKER ->
                    getAttributesFromPickers(attributes, input, criteriaMap, projectIds);
            default -> {
                // NOOP
            }
        }
    }

    private void getAttributesFromRadioButtonsGroup(
            Map<String, List<String>> attributes,
            Input input,
            Map<String, Criteria> criteriaMap,
            List<Long> projectIds) {
        RadioButtonsGroup radioButtonsGroup = (RadioButtonsGroup) input;
        final SimpleCriteria<String> sCrit =
                retrieveCriteriaFromMap(
                        radioButtonsGroup.getName(), radioButtonsGroup.getOptions(), criteriaMap);

        if (Objects.nonNull(sCrit)) {
            radioButtonsGroup
                    .getOptions()
                    .forEach(
                            optionInput -> {
                                if (optionInput.getValue().equalsIgnoreCase(sCrit.getValue())) {
                                    getAttributesFromOptionInput(attributes, optionInput, criteriaMap, projectIds);
                                }
                            });
        }
    }

    private SimpleCriteria<String> retrieveCriteriaFromMap(
            String radioButtonsGroupName, List<OptionInput> options, Map<String, Criteria> criteriaMap) {
        SimpleCriteria<String> crit = (SimpleCriteria<String>) criteriaMap.get(radioButtonsGroupName);
        return Objects.nonNull(crit)
                ? crit
                : createDefaultCriteriaFromOptions(radioButtonsGroupName, options);
    }

    private SimpleCriteria<String> createDefaultCriteriaFromOptions(
            String radioButtonsGroupName, List<OptionInput> options) {
        OptionInput defaultOption = findDefaultSelectedOption(options);
        return Objects.nonNull(defaultOption)
                ? new SimpleCriteria<>(
                        radioButtonsGroupName, defaultOption.getValue(), InputType.RADIO_BUTTONS_GROUP)
                : null;
    }

    private OptionInput findDefaultSelectedOption(List<OptionInput> options) {
        return options.stream().filter(OptionInput::isDefaultSelected).findFirst().orElse(null);
    }

    private void getAttributesFromChexBoxesGroup(
            Map<String, List<String>> attributes, Input input, Map<String, Criteria> criteriaMap) {
        CheckboxesGroup checkboxesGroup = (CheckboxesGroup) input;
        MultiOptionsCriteria moCrit = (MultiOptionsCriteria) criteriaMap.get(checkboxesGroup.getName());
        checkboxesGroup
                .getOptions()
                .forEach(
                        optionInput -> {
                            if (Objects.nonNull(moCrit)) {
                                moCrit
                                        .getSelectedOptions()
                                        .forEach(
                                                selectedOption -> {
                                                    if (optionInput.getValue().equalsIgnoreCase(selectedOption.toString())) {
                                                        List<String> checkedAttributes =
                                                                attributes.computeIfAbsent(
                                                                        checkboxesGroup.getLabel(), k -> new ArrayList<>());
                                                        checkedAttributes.add(optionInput.getLabel());
                                                    }
                                                });
                            } else {
                                if (optionInput.isDefaultSelected()) {
                                    List<String> checkedAttributes =
                                            attributes.computeIfAbsent(
                                                    checkboxesGroup.getLabel(), k -> new ArrayList<>());
                                    checkedAttributes.add(optionInput.getLabel());
                                }
                            }
                        });
    }

    private void getAttributesFromInputsGroup(
            Map<String, List<String>> attributes,
            Input input,
            Map<String, Criteria> criteriaMap,
            List<Long> projectIds) {
        InputsGroup inputsGroup = (InputsGroup) input;
        inputsGroup
                .getInputs()
                .forEach(i -> getAttributesFromInput(attributes, i, criteriaMap, projectIds));
    }

    private void getAttributesFromCheckBox(
            Map<String, List<String>> attributes,
            Input input,
            Map<String, Criteria> criteriaMap,
            List<Long> projectIds) {
        CheckboxInput checkboxInput = (CheckboxInput) input;
        SimpleCriteria sCrit = (SimpleCriteria) criteriaMap.get(checkboxInput.getName());
        if ((boolean) sCrit.getValue()) {
            getAttributesFromOptionInput(attributes, checkboxInput, criteriaMap, projectIds);
        }
    }

    private void getAttributesFromDropdownList(
            Map<String, List<String>> attributes, Input input, Map<String, Criteria> criteriaMap) {
        DropdownList dropdownList = (DropdownList) input;
        SimpleCriteria sCrit = (SimpleCriteria) criteriaMap.get(dropdownList.getName());
        dropdownList
                .getOptions()
                .forEach(
                        optionInput -> {
                            if (optionInput.getValue().equalsIgnoreCase((String) sCrit.getValue())) {
                                attributes.put(dropdownList.getLabel(), Arrays.asList(optionInput.getLabel()));
                            }
                        });
    }

    private void getAttributesFromTemplateDropdownList(
            Map<String, List<String>> attributes, Input input, Map<String, Criteria> criteriaMap) {
        TemplateDropdownList templateDropdownList = (TemplateDropdownList) input;
        Criteria criteria = criteriaMap.get(templateDropdownList.getName());
        List<OptionInput> opts = templateDropdownList.getOptions();
        String attributeValue =
                retrieveAttributeValueForTemplateDropdownList(criteria, templateDropdownList, opts);

        attributes.put(templateDropdownList.getLabel(), Collections.singletonList(attributeValue));
    }

    private String retrieveAttributeValueForTemplateDropdownList(
            Criteria criteria, TemplateDropdownList templateDropdownList, List<OptionInput> opts) {
        if (criteria instanceof EmptyCriteria) {
            return templateDropdownList.getFileDoesNotExistMessage();
        } else {
            Optional<OptionInput> defaultOption =
                    opts.stream().filter(OptionInput::isDefaultSelected).findFirst();

            if (defaultOption.isEmpty()) {
                throw new IllegalArgumentException(
                        String.format(
                                "You must define a default value for TemplateDropdownList: %s",
                                templateDropdownList.getLabel()));
            } else {
                return retrieveTemplateDropdownListAttributeValueIfExists(
                        criteria, defaultOption.get(), opts);
            }
        }
    }

    private String retrieveTemplateDropdownListAttributeValueIfExists(
            Criteria criteria, OptionInput defaultOption, List<OptionInput> opts) {
        SimpleCriteria sCrit = (SimpleCriteria) criteria;

        return Objects.isNull(sCrit)
                ? defaultOption.getLabel()
                : doRetrieveTemplateDropdownListAttributeValueIfExists(sCrit, opts);
    }

    private String doRetrieveTemplateDropdownListAttributeValueIfExists(
            SimpleCriteria sCrit, List<OptionInput> opts) {
        Optional<OptionInput> foundOption =
                opts.stream()
                        .filter(opt -> opt.getValue().equalsIgnoreCase((String) sCrit.getValue()))
                        .findFirst();
        if (foundOption.isPresent()) {
            return foundOption.get().getLabel();
        }
        return EMPTY;
    }

    private void getAttributesFromTreePicker(
            Map<String, List<String>> attributes,
            Input input,
            Map<String, Criteria> criteriaMap,
            List<Long> projectIds) {
        TreePicker treePicker = (TreePicker) input;
        MultiValuesCriteria mvCrit = (MultiValuesCriteria) criteriaMap.get(treePicker.getName());
        List<String> targets = new ArrayList(mvCrit.getValue().values());
        getAttributesFromPicker(attributes, targets, mvCrit.getName(), projectIds);
    }

    private void getAttributesFromPickers(
            Map<String, List<String>> attributes,
            Input input,
            Map<String, Criteria> criteriaMap,
            List<Long> projectIds) {
        MultiOptionsCriteria moCrit = (MultiOptionsCriteria) criteriaMap.get(input.getName());
        List<String> targets = new ArrayList<>();
        moCrit.getSelectedOptions().forEach(o -> targets.add(o.toString()));
        getAttributesFromPicker(attributes, targets, moCrit.getName(), projectIds);
    }

    private void getAttributesFromDate(
            Map<String, List<String>> attributes, Input input, Map<String, Criteria> criteriaMap) {
        DateInput dateInput = (DateInput) input;
        Criteria crit = criteriaMap.get(dateInput.getName());
        Locale locale = LocaleContextHolder.getLocale();
        if (crit instanceof SimpleCriteria) {
            SimpleCriteria sCrit = (SimpleCriteria) crit;
            Date date = (Date) sCrit.getValue();
            attributes.put(
                    dateInput.getLabel(), Arrays.asList(i18nHelper.localizeShortDate(date, locale)));
        } else {
            attributes.put(dateInput.getLabel(), Arrays.asList("-"));
        }
    }

    private void getAttributesFromText(
            Map<String, List<String>> attributes, Input input, Map<String, Criteria> criteriaMap) {
        TextInput textInput = (TextInput) input;
        Criteria crit = criteriaMap.get(textInput.getName());
        if (crit instanceof SimpleCriteria) {
            SimpleCriteria sCrit = (SimpleCriteria) crit;
            String text = (String) sCrit.getValue();
            attributes.put(textInput.getLabel(), Arrays.asList(text));
        } else {
            attributes.put(textInput.getLabel(), Arrays.asList("-"));
        }
    }

    private void getAttributesFromOptionInput(
            Map<String, List<String>> attributes,
            OptionInput optionInput,
            Map<String, Criteria> criteriaMap,
            List<Long> projectIds) {
        List<String> options = new ArrayList<>();
        if (optionInput instanceof ContainerOption) {
            ContainerOption containerOption = (ContainerOption) optionInput;
            getAttributesFromInput(attributes, containerOption.getContent(), criteriaMap, projectIds);
        } else {
            if (attributes.get(OPTION) == null) {
                attributes.put(OPTION, Arrays.asList(optionInput.getLabel()));
            } else {
                options.addAll(attributes.get(OPTION));
                options.add(optionInput.getLabel());
                attributes.put(OPTION, options);
            }
        }
    }

    private void getAttributesFromPicker(
            Map<String, List<String>> attributes,
            List<String> targetIds,
            String entity,
            List<Long> projectIds) {
        List<String> names = new ArrayList<>();
        List<Long> ids = new ArrayList<>();
        Locale locale = LocaleContextHolder.getLocale();
        String entityLabelKey = "";

        switch (entity) {
            case CAMPAIGN_ID:
                entityLabelKey = "label.Campaign";
            case CAMPAIGN_IDS:
                targetIds.forEach(id -> ids.add(Long.parseLong(id)));
                handleAttributesForCampaignIds(attributes, names, ids, locale, entityLabelKey, projectIds);
                break;
            case ITERATION_ID:
                entityLabelKey = "label.iteration";
            case ITERATION_IDS:
                targetIds.forEach(id -> ids.add(Long.parseLong(id)));
                handleAttributesForIterationIds(attributes, names, ids, locale, entityLabelKey, projectIds);
                break;
            case REQUIREMENTS_IDS:
                targetIds.forEach(id -> ids.add(Long.parseLong(id)));
                handleAttributesForRequirementIds(attributes, names, ids, locale, projectIds);
                break;
            case TESTCASES_IDS:
                targetIds.forEach(id -> ids.add(Long.parseLong(id)));
                handleAttributesForTestCaseIds(attributes, names, ids, locale, projectIds);
                break;
            case PROJECT_IDS:
                targetIds.forEach(id -> ids.add(Long.parseLong(id)));
                handleAttributesForProjectIds(attributes, names, ids, locale);
                break;
            case MILESTONES:
                targetIds.forEach(id -> ids.add(Long.parseLong(id)));
                handleAttributesForMilestoneIds(attributes, names, ids, locale, projectIds);
                break;
            case TAGS:
                names.addAll(targetIds);
                attributes.put(TAGS, names);
                break;
            default:
                break;
        }
    }

    private void handleAttributesForTestCaseIds(
            Map<String, List<String>> attributes,
            List<String> names,
            List<Long> testCaseIds,
            Locale locale,
            List<Long> projectIds) {
        List<String> testCaseFullNames =
                testCaseModificationService.retrieveFullNameByTestCaseLibraryNodeIds(
                        testCaseIds, projectIds);
        names.addAll(testCaseFullNames);
        attributes.put(i18nHelper.internationalize("label.testCases", locale), names);
    }

    private void handleAttributesForRequirementIds(
            Map<String, List<String>> attributes,
            List<String> names,
            List<Long> requirementIds,
            Locale locale,
            List<Long> projectIds) {
        List<String> requirementFullNames =
                requirementVersionManagerService.retrieveFullNameByRequirementLibraryNodeIds(
                        requirementIds, projectIds);
        names.addAll(requirementFullNames);
        attributes.put(i18nHelper.internationalize("label.requirements", locale), names);
    }

    private void handleAttributesForMilestoneIds(
            Map<String, List<String>> attributes,
            List<String> names,
            List<Long> milestoneIds,
            Locale locale,
            List<Long> projectIds) {
        List<String> milestoneLabels =
                milestoneManagerService.findMilestoneLabelByIds(milestoneIds, projectIds);
        names.addAll(milestoneLabels);
        attributes.put(i18nHelper.internationalize("label.Milestone", locale), names);
    }

    private void handleAttributesForProjectIds(
            Map<String, List<String>> attributes,
            List<String> names,
            List<Long> projectIds,
            Locale locale) {
        List<String> projectNames = customProjectModificationService.findProjectNamesByIds(projectIds);
        names.addAll(projectNames);
        attributes.put(i18nHelper.internationalize("label.projects", locale), names);
    }

    private void handleAttributesForCampaignIds(
            Map<String, List<String>> attributes,
            List<String> names,
            List<Long> campaignIds,
            Locale locale,
            String entityLabelKey,
            List<Long> projectIds) {
        List<String> campaignFullNames =
                campaignDisplayService.retrieveFullNameByCampaignLibraryNodeIds(campaignIds, projectIds);
        names.addAll(campaignFullNames);

        if (entityLabelKey.isEmpty()) {
            entityLabelKey = "label.campaigns";
        }

        attributes.put(i18nHelper.internationalize(entityLabelKey, locale), names);
    }

    private void handleAttributesForIterationIds(
            Map<String, List<String>> attributes,
            List<String> names,
            List<Long> iterationIds,
            Locale locale,
            String entityLabelKey,
            List<Long> projectIds) {
        List<String> iterationFullNames =
                iterationDisplayService.retrieveFullNameByIterationIds(iterationIds, projectIds);
        names.addAll(iterationFullNames);

        if (entityLabelKey.isEmpty()) {
            entityLabelKey = "label.iterations";
        }

        attributes.put(i18nHelper.internationalize(entityLabelKey, locale), names);
    }
}
