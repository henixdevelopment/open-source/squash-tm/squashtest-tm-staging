/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller;

import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.service.feature.experimental.ExperimentalFeatureManager;

/**
 * This controller is responsible to serve initial index.html for angular main application. His main
 * utility compared to serve the angular app as a static resource is to set the servlet context in
 * the index.html file. So despite the fact that angular make a static build, when can dynamically
 * set the context to the servlet context path, thanks to thymeleaf. Note that because Angular build
 * is mainly automated by angular cli, the index.html file is directly provided by tm-front module
 * and not by tm.web module.
 */
@Controller
public class IndexController {
    private static final Logger LOGGER = LoggerFactory.getLogger(IndexController.class);

    private final ExperimentalFeatureManager experimentalFeatureManager;

    public IndexController(ExperimentalFeatureManager experimentalFeatureManager) {
        this.experimentalFeatureManager = experimentalFeatureManager;
    }

    @GetMapping("/index")
    public String index(Model model) {
        final List<String> flags =
                experimentalFeatureManager.getEnabledFeatures().stream().map(Enum::name).toList();
        model.addAttribute("experimentalFeatureFlags", flags);
        LOGGER.debug(
                "Page request forwarded to SquashTM index controller. Single page app will be served with current context path.");
        LOGGER.debug("Experimental feature flags: {}", flags);
        return "index.html";
    }
}
