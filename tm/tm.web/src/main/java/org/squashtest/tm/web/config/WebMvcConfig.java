/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.config;

import java.util.EnumSet;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.lang.NonNull;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewInterceptor;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.resource.ResourceResolver;
import org.springframework.web.servlet.resource.ResourceResolverChain;
import org.springframework.web.servlet.view.InternalResourceView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.squashtest.tm.web.backend.interceptor.ActiveMilestoneInterceptor;
import org.squashtest.tm.web.backend.interceptor.LoggingInterceptor;
import org.squashtest.tm.web.backend.interceptor.SecurityExpressionResolverExposerInterceptor;

/**
 * This class configures Spring MVC.
 *
 * @author Gregory Fouquet
 * @since 1.13.0
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Value("${squash.path.languages-path}")
    private String customI18nFilePath;

    @PersistenceUnit private EntityManagerFactory emf;

    @Inject
    private SecurityExpressionResolverExposerInterceptor securityExpressionResolverExposerInterceptor;

    @Inject private ActiveMilestoneInterceptor milestoneInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // Log4j output enhancement
        LoggingInterceptor loggingInterceptor = new LoggingInterceptor();
        registry.addWebRequestInterceptor(loggingInterceptor);

        // OSIV
        OpenEntityManagerInViewInterceptor osiv = new OpenEntityManagerInViewInterceptor();
        osiv.setEntityManagerFactory(emf);
        registry.addWebRequestInterceptor(osiv);

        registry.addInterceptor(milestoneInterceptor);

        // #sec in thymeleaf
        registry
                .addInterceptor(securityExpressionResolverExposerInterceptor)
                .excludePathPatterns("/", "/login");
    }

    /**
     * Redirecting all declared url of the SinglePageApp to the appropriate controller. It allow users
     * to bookmark deep url and received the SPA : - without any hard redirect that will modify the
     * url in browser. - by-passing spring sec authentication, as we are serving a "nearly" static
     * content.
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        EnumSet.allOf(AngularAppPageUrls.class)
                .forEach(
                        singlePageAppUrl ->
                                registry
                                        .addViewController(singlePageAppUrl.urlPattern)
                                        .setViewName(singlePageAppUrl.viewName));
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler(
                        "/**/*.css",
                        "/**/*.js",
                        "/**/*.ts",
                        "/**/*.json",
                        "/**/*.jsx",
                        "/**/*.png",
                        "/**/*.svg",
                        "/**/*.ttf",
                        "/**/*.woff",
                        "/**/*.woff2",
                        "/**/*.zip",
                        "/**/*.docx")
                .setCachePeriod(614880)
                .addResourceLocations(
                        "/",
                        "classpath:/",
                        "classpath:/META-INF/resources/",
                        "classpath:/static/",
                        "classpath:/public/");

        registry
                .addResourceHandler("/**/*.html", "/**/*.docx")
                .setCachePeriod(0)
                .addResourceLocations(
                        "/",
                        "classpath:/",
                        "classpath:/META-INF/resources/",
                        "classpath:/static/",
                        "classpath:/public/");

        // Prior to 2.0, there is no trailing slash in squash.path.languages-path property, but it's
        // required by spring for proper loading of resources. To avoid migration and breaking server
        // side i18n customisation we add manually the trailing slash here
        registry
                .addResourceHandler("custom_translations_*.json")
                .setCachePeriod(614880)
                .addResourceLocations("file:" + this.customI18nFilePath + "/")
                // To avoid 404 error when no custom translation file is found, we add a custom resolver
                // that will return an empty JSON dictionary
                .resourceChain(true)
                .addResolver(new CustomTranslationResourceResolver());
    }

    @Bean
    public ViewResolver viewResolver() {
        UrlBasedViewResolver viewResolver = new UrlBasedViewResolver();
        viewResolver.setViewClass(InternalResourceView.class);
        return viewResolver;
    }

    private static class FallbackJsonResource extends ByteArrayResource {
        public FallbackJsonResource() {
            super("{}".getBytes());
        }

        @Override
        public long lastModified() {
            // Default implementation throws an exception, so we need to subclass to enable caching
            return -1;
        }

        @Override
        public String getFilename() {
            // JSON extension allows to set the correct content type but the filename is not relevant
            return "fallback-translation-file.json";
        }
    }

    private static class CustomTranslationResourceResolver implements ResourceResolver {
        private static final Resource DEFAULT_RESOURCE = new FallbackJsonResource();

        @Override
        public Resource resolveResource(
                HttpServletRequest request,
                @NonNull String requestPath,
                @NonNull List<? extends Resource> locations,
                ResourceResolverChain chain) {
            final Resource resource = chain.resolveResource(request, requestPath, locations);
            return (resource != null) ? resource : DEFAULT_RESOURCE;
        }

        @Override
        public String resolveUrlPath(
                @NonNull String resourcePath,
                @NonNull List<? extends Resource> locations,
                ResourceResolverChain chain) {
            return chain.resolveUrlPath(resourcePath, locations);
        }
    }
}
