/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.environmentvariable;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.environmentvariable.EVBindableEntity;
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableBindingService;
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableBindingValueService;
import org.squashtest.tm.service.internal.display.dto.BoundEnvironmentVariableDto;

@RestController
@RequestMapping("/backend/bound-environment-variables")
public class BoundEnvironmentVariableController {

    private static final String BOUND_ENVIRONMENT_VARIABLES = "boundEnvironmentVariables";

    private final EnvironmentVariableBindingService environmentVariableBindingService;

    private final EnvironmentVariableBindingValueService environmentVariableValueService;

    public BoundEnvironmentVariableController(
            EnvironmentVariableBindingService environmentVariableBindingService,
            EnvironmentVariableBindingValueService environmentVariableValueService) {
        this.environmentVariableBindingService = environmentVariableBindingService;
        this.environmentVariableValueService = environmentVariableValueService;
    }

    @GetMapping("test-automation-server/{serverId}")
    public Map<String, List<BoundEnvironmentVariableDto>> getBoundEnvironmentVariableFromServer(
            @PathVariable Long serverId) {
        return Collections.singletonMap(
                BOUND_ENVIRONMENT_VARIABLES,
                environmentVariableBindingService.getBoundEnvironmentVariablesByTestAutomationServerId(
                        serverId));
    }

    @GetMapping("project/{projectId}")
    public Map<String, List<BoundEnvironmentVariableDto>> getBoundEnvironmentVariablesFromProject(
            @PathVariable Long projectId) {
        return Collections.singletonMap(
                BOUND_ENVIRONMENT_VARIABLES,
                environmentVariableBindingService.getBoundEnvironmentVariableFromProjectView(projectId));
    }

    @GetMapping("project/{projectId}/iteration/{iterationId}")
    public Map<String, List<BoundEnvironmentVariableDto>>
            getBoundEnvironmentVariablesFromProjectByIteration(
                    @PathVariable Long projectId, @PathVariable Long iterationId) {
        return Collections.singletonMap(
                BOUND_ENVIRONMENT_VARIABLES,
                environmentVariableBindingService.getBoundEnvironmentVariableFromProjectViewByIteration(
                        projectId, iterationId));
    }

    @PostMapping("value")
    public void editEnvironmentVariableValue(@RequestBody EnvironmentVariableValuePatch patch) {
        environmentVariableValueService.editEnvironmentVariableValue(
                patch.getBindingId(), patch.getValue());
    }

    @PostMapping(value = "reset")
    public Map<String, String> resetEnvironmentVariableDefaultValue(
            @RequestBody EnvironmentVariableValuePatch patch) {
        String defaultValue =
                environmentVariableValueService.resetDefaultValue(
                        patch.getBindingId(), patch.getEntityType());
        return Collections.singletonMap("defaultValue", defaultValue);
    }

    @PostMapping("/bind")
    public Map<String, List<BoundEnvironmentVariableDto>> bindEnvironmentVariables(
            @RequestBody EnvironmentVariableBindingPatch bindingPatch) {
        environmentVariableBindingService.createNewBindings(
                bindingPatch.getEntityId(),
                bindingPatch.getEntityType(),
                bindingPatch.getEnvironmentVariableIds());
        return Collections.singletonMap(
                BOUND_ENVIRONMENT_VARIABLES,
                getBoundEnvironmentVariables(bindingPatch.getEntityId(), bindingPatch.getEntityType()));
    }

    private List<BoundEnvironmentVariableDto> getBoundEnvironmentVariables(
            Long entityId, EVBindableEntity entityType) {
        if (EVBindableEntity.PROJECT.equals(entityType)) {
            return environmentVariableBindingService.getBoundEnvironmentVariableFromProjectView(entityId);
        } else {
            return environmentVariableBindingService.getBoundEnvironmentVariablesByEntity(
                    entityId, entityType);
        }
    }

    @PostMapping("/unbind")
    public void unbindEnvironmentVariables(
            @RequestBody EnvironmentVariableBindingPatch bindingPatch) {
        environmentVariableBindingService.unbind(
                bindingPatch.getEntityId(),
                bindingPatch.getEntityType(),
                bindingPatch.getEnvironmentVariableIds());
    }

    public static class EnvironmentVariableValuePatch {
        Long bindingId;
        String value;

        String entityType;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getEntityType() {
            return entityType;
        }

        public void setEntityType(String entityType) {
            this.entityType = entityType;
        }

        public Long getBindingId() {
            return bindingId;
        }

        public void setBindingId(Long bindingId) {
            this.bindingId = bindingId;
        }
    }

    public static class EnvironmentVariableBindingPatch {
        Long bindingId;
        Long entityId;
        EVBindableEntity entityType;
        List<Long> environmentVariableIds;

        public Long getEntityId() {
            return entityId;
        }

        public void setEntityId(Long entityId) {
            this.entityId = entityId;
        }

        public EVBindableEntity getEntityType() {
            return entityType;
        }

        public void setEntityType(EVBindableEntity entityType) {
            this.entityType = entityType;
        }

        public List<Long> getEnvironmentVariableIds() {
            return environmentVariableIds;
        }

        public void setEnvironmentVariableIds(List<Long> environmentVariableIds) {
            this.environmentVariableIds = environmentVariableIds;
        }

        public Long getBindingId() {
            return bindingId;
        }

        public void setBindingId(Long bindingId) {
            this.bindingId = bindingId;
        }
    }
}
