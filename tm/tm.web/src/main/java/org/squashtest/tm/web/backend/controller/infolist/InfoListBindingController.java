/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.infolist;

import java.util.Map;
import javax.inject.Inject;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.infolist.InfoListBindingManagerService;

@RestController
@RequestMapping("/backend/info-list-binding")
public class InfoListBindingController {

    private static final String INFO_LIST_ID = "infoListId";

    private final InfoListBindingManagerService infoListBindingManagerService;

    @Inject
    InfoListBindingController(InfoListBindingManagerService infoListBindingManagerService) {
        this.infoListBindingManagerService = infoListBindingManagerService;
    }

    @PostMapping(value = "/project/{projectId}/category")
    public void bindCategoryToProject(
            @PathVariable Long projectId, @RequestBody Map<String, Long> request) {
        infoListBindingManagerService.bindListToProjectReqCategory(
                request.get(INFO_LIST_ID), projectId);
    }

    @PostMapping(value = "/project/{projectId}/nature")
    public void bindNatureToProject(
            @PathVariable Long projectId, @RequestBody Map<String, Long> request) {
        infoListBindingManagerService.bindListToProjectTcNature(request.get(INFO_LIST_ID), projectId);
    }

    @PostMapping(value = "/project/{projectId}/type")
    public void bindTypeToProject(
            @PathVariable Long projectId, @RequestBody Map<String, Long> request) {
        infoListBindingManagerService.bindListToProjectTcType(request.get(INFO_LIST_ID), projectId);
    }
}
