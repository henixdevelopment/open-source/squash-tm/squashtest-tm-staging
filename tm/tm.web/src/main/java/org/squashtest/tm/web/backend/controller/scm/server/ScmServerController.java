/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.scm.server;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.csp.core.bugtracker.net.AuthenticationException;
import org.squashtest.tm.core.foundation.exception.InvalidUrlException;
import org.squashtest.tm.core.foundation.lang.UrlUtils;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.domain.scm.ScmRepository;
import org.squashtest.tm.domain.scm.ScmServer;
import org.squashtest.tm.domain.servers.AuthenticationProtocol;
import org.squashtest.tm.exception.WrongUrlException;
import org.squashtest.tm.service.display.scm.server.ScmServerDisplayService;
import org.squashtest.tm.service.internal.display.dto.ScmServerDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.scmserver.ScmConnectorRegistry;
import org.squashtest.tm.service.scmserver.ScmRepositoryManagerService;
import org.squashtest.tm.service.scmserver.ScmServerManagerService;
import org.squashtest.tm.service.servers.ManageableCredentials;
import org.squashtest.tm.service.thirdpartyserver.ThirdPartyServerCredentialsService;
import org.squashtest.tm.web.backend.controller.form.model.ScmServerFormModel;
import org.squashtest.tm.web.backend.controller.utils.UrlValidator;

@RestController
@RequestMapping("/backend/scm-servers")
public class ScmServerController {

    private final ScmServerManagerService scmServerManagerService;

    private final ScmServerDisplayService scmServerDisplayService;

    private final ScmConnectorRegistry scmConnectorRegistry;

    private final ScmRepositoryManagerService scmRepositoryManager;

    private final ThirdPartyServerCredentialsService thirdPartyServerCredentialsService;

    @Inject
    ScmServerController(
            ScmServerDisplayService scmServerDisplayService,
            ScmServerManagerService scmServerManagerService,
            ScmConnectorRegistry scmConnectorRegistry,
            ScmRepositoryManagerService scmRepositoryManager,
            ThirdPartyServerCredentialsService thirdPartyServerCredentialsService) {
        this.scmServerDisplayService = scmServerDisplayService;
        this.scmServerManagerService = scmServerManagerService;
        this.scmConnectorRegistry = scmConnectorRegistry;
        this.scmRepositoryManager = scmRepositoryManager;
        this.thirdPartyServerCredentialsService = thirdPartyServerCredentialsService;
    }

    @GetMapping
    public Map<String, List<ScmServerDto>> getAllServersAndRepositories() {
        return Collections.singletonMap(
                "scmServers", scmServerDisplayService.getAllServersAndRepositories());
    }

    @PostMapping
    public GridResponse getAllScmServers(@RequestBody GridRequest request) {
        return scmServerDisplayService.findAll(request);
    }

    @PostMapping(value = "/new")
    public Map<String, Object> addScmServer(
            @Valid @RequestBody ScmServerFormModel scmServerFormModel) {
        Map<String, Object> tempReturn = new HashMap<>();
        try {
            ScmServer scmServer = scmServerFormModel.getScmServer();
            UrlUtils.toUrl(scmServerFormModel.getUrl());
            scmServerManagerService.createNewScmServer(scmServer);
            tempReturn.put("id", scmServer.getId());

        } catch (InvalidUrlException iue) {
            throw new WrongUrlException("url", iue);
        }

        return tempReturn;
    }

    @GetMapping(value = "/get-scm-server-kinds")
    public Map<String, Set<String>> getScmServerKinds() {
        Map<String, Set<String>> response = new HashMap<>();
        response.put("scmServerKinds", scmConnectorRegistry.getRegisteredScmKinds());

        return response;
    }

    @DeleteMapping(value = "/{scmServerIds}")
    public void deleteScmServers(@PathVariable List<Long> scmServerIds) {
        scmServerManagerService.deleteScmServers(scmServerIds);
    }

    @GetMapping(value = "/{scmServerId}/repositories")
    public Map<String, List<NamedReference>> getScmRepositories(@PathVariable long scmServerId) {
        List<ScmRepository> repositories = scmRepositoryManager.findByScmServerOrderByPath(scmServerId);

        List<NamedReference> namedReferences =
                repositories.stream()
                        .map(
                                scmRepository -> new NamedReference(scmRepository.getId(), scmRepository.getName()))
                        .toList();

        return Collections.singletonMap("repositories", namedReferences);
    }

    @PostMapping(value = "/{scmServerId}/name")
    public void updateName(@PathVariable long scmServerId, @RequestBody ScmServerPatch patch) {
        scmServerManagerService.updateName(scmServerId, patch.getName());
    }

    @PostMapping(value = "/{scmServerId}/url")
    public void updateUrl(@PathVariable long scmServerId, @RequestBody ScmServerPatch patch) {
        UrlValidator.checkURL(patch.getUrl());
        scmServerManagerService.updateUrl(scmServerId, patch.getUrl());
    }

    @PostMapping(value = "/{scmServerId}/committer-mail")
    public void updateCommitterMail(
            @PathVariable long scmServerId, @RequestBody ScmServerPatch patch) {
        scmServerManagerService.updateCommitterMail(scmServerId, patch.getCommitterMail());
    }

    @GetMapping("/{scmServerId}/credentials-not-shared")
    public Map<String, Boolean> getCredentialsNotShared(@PathVariable long scmServerId) {
        boolean credentialsNotShared = scmServerManagerService.getCredentialsNotShared(scmServerId);
        return Collections.singletonMap("credentialsNotShared", credentialsNotShared);
    }

    @PostMapping("/{scmServerId}/credentials-not-shared")
    public void updateCredentialsNotShared(
            @PathVariable long scmServerId, @RequestBody ScmServerPatch patch) {
        scmServerManagerService.updateCredentialsNotShared(scmServerId, patch.isCredentialsNotShared());
    }

    @PostMapping(value = "/{scmServerId}/credentials")
    public void storeCredentials(
            @PathVariable long scmServerId, @RequestBody ManageableCredentials credentials) {
        thirdPartyServerCredentialsService.storeCredentials(scmServerId, credentials);
    }

    @PostMapping(value = "{scmServerId}/auth-protocol")
    public void changeAuthProtocol(
            @PathVariable long scmServerId, @RequestBody ScmServerPatch patch) {
        try {
            AuthenticationProtocol protocol =
                    Enum.valueOf(AuthenticationProtocol.class, patch.getAuthProtocol());
            thirdPartyServerCredentialsService.changeAuthenticationProtocol(scmServerId, protocol);
        } catch (IllegalArgumentException e) {
            throw new AuthenticationException(
                    "Unknown authentication protocol " + patch.getAuthProtocol(), e);
        }
    }

    @PostMapping("/{scmServerId}/description")
    public void updateDescription(@PathVariable long scmServerId, @RequestBody ScmServerPatch patch) {
        scmServerManagerService.updateDescription(scmServerId, patch.description);
    }

    public static class ScmServerPatch {
        String name;
        String url;
        String committerMail;
        boolean credentialsNotShared;
        String description;
        private String authProtocol;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getCommitterMail() {
            return committerMail;
        }

        public void setCommitterMail(String committerMail) {
            this.committerMail = committerMail;
        }

        public boolean isCredentialsNotShared() {
            return credentialsNotShared;
        }

        public void setCredentialsNotShared(boolean credentialsNotShared) {
            this.credentialsNotShared = credentialsNotShared;
        }

        public String getAuthProtocol() {
            return authProtocol;
        }

        public void setAuthProtocol(String authProtocol) {
            this.authProtocol = authProtocol;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}
