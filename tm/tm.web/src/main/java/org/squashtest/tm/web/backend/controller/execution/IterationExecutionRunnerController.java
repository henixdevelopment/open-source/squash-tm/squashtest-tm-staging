/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.execution;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.service.campaign.TestPlanExecutionProcessingService;
import org.squashtest.tm.service.internal.display.dto.execution.TestPlanResume;
import org.squashtest.tm.service.internal.display.dto.execution.TestPlanResume.IterationTestPlanResume;
import org.squashtest.tm.web.backend.controller.form.model.IdList;

/**
 * @author jthebault
 */
@RestController
@RequestMapping("/backend/iteration/{iterationId}/test-plan")
public class IterationExecutionRunnerController {

    private final TestPlanExecutionProcessingService<IterationTestPlanResume>
            iterationExecutionProcessingService;

    public IterationExecutionRunnerController(
            TestPlanExecutionProcessingService<IterationTestPlanResume>
                    iterationExecutionProcessingService) {
        this.iterationExecutionProcessingService = iterationExecutionProcessingService;
    }

    /**
     * @param iterationId id of the {@link Iteration} to execute.
     */
    @PostMapping(value = "/resume")
    public IterationTestPlanResume resumeIteration(@PathVariable long iterationId) {
        return iterationExecutionProcessingService.startResume(iterationId);
    }

    @PostMapping(value = "/resume-filtered-selection")
    public TestPlanResume resumeIterationWithFilter(
            @PathVariable long iterationId, @RequestBody FiltersContainer filtersContainer) {
        return iterationExecutionProcessingService.resumeWithFilteredTestPlan(
                iterationId, filtersContainer.getFilterValues());
    }

    @PostMapping(value = "/relaunch")
    public IterationTestPlanResume relaunchIteration(@PathVariable long iterationId) {
        return iterationExecutionProcessingService.relaunch(iterationId);
    }

    @PostMapping(value = "/relaunch-filtered-selection")
    public TestPlanResume relaunchIterationWithFilter(
            @PathVariable long iterationId, @RequestBody FiltersContainer filtersContainer) {
        return iterationExecutionProcessingService.relaunchFilteredTestPlan(
                iterationId, filtersContainer.getFilterValues());
    }

    @RequestMapping(value = "/{testPlanItemId}/next-execution")
    public IterationTestPlanResume moveToNextTestCase(
            @PathVariable("testPlanItemId") long testPlanItemId,
            @PathVariable("iterationId") long iterationId) {
        return iterationExecutionProcessingService.startResumeNextExecution(
                iterationId, testPlanItemId);
    }

    @RequestMapping(value = "/{testPlanItemId}/next-execution-filtered-selection")
    public TestPlanResume moveToNextTestCaseOfFilteredTestPlan(
            @PathVariable("testPlanItemId") long testPlanItemId,
            @PathVariable("iterationId") long iterationId,
            @RequestBody IdList partialTestPlan) {
        return iterationExecutionProcessingService.resumeNextExecutionOfFilteredTestPlan(
                iterationId, testPlanItemId, partialTestPlan.getIds());
    }
}
