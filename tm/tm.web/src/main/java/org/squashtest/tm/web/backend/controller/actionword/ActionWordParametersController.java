/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.actionword;

import javax.inject.Inject;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.HtmlUtils;
import org.squashtest.tm.service.actionword.ActionWordParameterService;

// ULTIMATE
@RestController
@RequestMapping("backend/action-word/{actionWordId}/parameters")
public class ActionWordParametersController {

    @Inject private ActionWordParameterService actionWordParameterService;

    @PostMapping(
            value = "{parameterId}/name",
            params = {"value"})
    public String renameParameter(
            @PathVariable long parameterId, @RequestParam("value") String newName) {
        actionWordParameterService.renameParameter(parameterId, newName);
        return HtmlUtils.htmlEscape(newName);
    }

    @PostMapping(
            value = "{parameterId}/default-value",
            params = {"value"})
    public String updateParameterDefaultValue(
            @PathVariable long parameterId, @RequestParam("value") String newDefaultValue) {
        actionWordParameterService.updateParameterDefaultValue(parameterId, newDefaultValue);
        return HtmlUtils.htmlEscape(newDefaultValue);
    }
}
