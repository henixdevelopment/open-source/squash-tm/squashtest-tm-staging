/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.execution;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.campaign.TestPlanExecutionProcessingService;
import org.squashtest.tm.service.internal.display.dto.execution.TestPlanResume;
import org.squashtest.tm.service.internal.display.dto.execution.TestPlanResume.TestSuiteTestPlanResume;
import org.squashtest.tm.web.backend.controller.form.model.IdList;

/**
 * @author jthebault
 */
@RestController
@RequestMapping("/backend/test-suite/{testSuiteId}/test-plan")
public class TestSuiteExecutionRunnerController {

    private final TestPlanExecutionProcessingService<TestSuiteTestPlanResume>
            testSuiteExecutionProcessingService;

    public TestSuiteExecutionRunnerController(
            TestPlanExecutionProcessingService<TestSuiteTestPlanResume>
                    testSuiteExecutionProcessingService) {
        this.testSuiteExecutionProcessingService = testSuiteExecutionProcessingService;
    }

    @PostMapping(value = "/resume")
    public TestSuiteTestPlanResume resumeTestSuite(@PathVariable long testSuiteId) {
        return testSuiteExecutionProcessingService.startResume(testSuiteId);
    }

    @PostMapping(value = "/resume-filtered-selection")
    public TestPlanResume resumeIterationWithFilter(
            @PathVariable long testSuiteId, @RequestBody FiltersContainer filtersContainer) {
        return testSuiteExecutionProcessingService.resumeWithFilteredTestPlan(
                testSuiteId, filtersContainer.getFilterValues());
    }

    @PostMapping(value = "/relaunch")
    public TestSuiteTestPlanResume relaunchTestSuite(@PathVariable long testSuiteId) {
        return testSuiteExecutionProcessingService.relaunch(testSuiteId);
    }

    @PostMapping(value = "/relaunch-filtered-selection")
    public TestPlanResume relaunchIterationWithFilter(
            @PathVariable long testSuiteId, @RequestBody FiltersContainer filtersContainer) {
        return testSuiteExecutionProcessingService.relaunchFilteredTestPlan(
                testSuiteId, filtersContainer.getFilterValues());
    }

    @RequestMapping(value = "/{testPlanItemId}/next-execution")
    public TestSuiteTestPlanResume moveToNextTestCase(
            @PathVariable("testPlanItemId") long testPlanItemId,
            @PathVariable("testSuiteId") long testSuiteId) {
        return testSuiteExecutionProcessingService.startResumeNextExecution(
                testSuiteId, testPlanItemId);
    }

    @RequestMapping(value = "/{testPlanItemId}/next-execution-filtered-selection")
    public TestPlanResume moveToNextTestCaseOfFilteredTestPlan(
            @PathVariable("testPlanItemId") long testPlanItemId,
            @PathVariable("testSuiteId") long testSuiteId,
            @RequestBody IdList partialTestPlanItemIds) {
        return testSuiteExecutionProcessingService.resumeNextExecutionOfFilteredTestPlan(
                testSuiteId, testPlanItemId, partialTestPlanItemIds.getIds());
    }
}
