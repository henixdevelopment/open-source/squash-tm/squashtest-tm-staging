/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.attachment;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.Objects;
import javax.inject.Inject;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.attachment.Attachment;
import org.squashtest.tm.exception.attachment.AttachmentException;
import org.squashtest.tm.service.attachment.AttachmentManagerService;
import org.squashtest.tm.service.display.attachment.FileViewerService;
import org.squashtest.tm.service.internal.display.attachment.FileViewerExtension;
import org.squashtest.tm.service.internal.display.dto.FileViewerDto;
import org.squashtest.tm.service.internal.display.dto.FileViewerRequest;
import org.squashtest.tm.service.internal.display.grid.TreeGridResponse;
import org.squashtest.tm.service.system.LogFileDownloadService;

@RestController
@RequestMapping("backend/file-viewer")
public class FileViewerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileViewerController.class);

    private static final String ORIGIN = "origin";
    private static final String SOURCE = "source";
    private static final String TARGET = "target";
    private static final String ERROR_MESSAGE = "An error occurred while viewing the attachment : ";

    private static final String ATTACHMENT_FILENAME = "attachment; filename=";
    private static final String CONTENT_DISPOSITION = "Content-Disposition";

    @Inject private AttachmentManagerService attachmentManagerService;

    @Inject private FileViewerService fileViewerService;
    @Inject private LogFileDownloadService logFileDownloadService;

    @GetMapping(value = "/attachment/{attachmentId}")
    public void previewAttachment(
            @PathVariable("attachmentId") Long attachmentId,
            @RequestParam(name = TARGET, required = false) String target,
            HttpServletResponse response) {

        try {
            Attachment attachment = attachmentManagerService.findAttachment(attachmentId);
            FileViewerRequest viewerRequest = FileViewerRequest.fromAttachment(attachment, target);

            if (Objects.nonNull(viewerRequest.getFileType())) {
                FileViewerDto fileViewer = fileViewerService.preview(viewerRequest);
                displayPreview(response, fileViewer, viewerRequest.getFileName());
            } else {
                LOGGER.info(
                        "Unable to view the attachment '{}'. The file extension is not supported. The attachment will be automatically downloaded.",
                        viewerRequest.getFileName());
                downloadAttachment(attachmentId, response, viewerRequest.getFileName());
            }

        } catch (IOException e) {
            LOGGER.warn(ERROR_MESSAGE + e.getMessage(), e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } catch (AccessDeniedException | IllegalArgumentException e) {
            LOGGER.warn(ERROR_MESSAGE + e.getMessage(), e);
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    @GetMapping(value = "/archive/{attachmentId}")
    public TreeGridResponse getArchiveTree(@PathVariable("attachmentId") Long attachmentId) {

        try {
            Attachment attachment = attachmentManagerService.findAttachment(attachmentId);
            FileViewerRequest viewerRequest = FileViewerRequest.fromAttachment(attachment, null);
            if (FileViewerExtension.TAR.equals(viewerRequest.getFileType())) {
                return fileViewerService.getArchiveFileTree(viewerRequest);
            } else {
                throw new IllegalArgumentException(
                        "File id : " + attachmentId + " is not a valid archive file.");
            }
        } catch (IOException e) {
            LOGGER.warn("An error occurred while getting the archive tree", e);
            throw new AttachmentException(e);
        }
    }

    @GetMapping(value = "/archive-source/{attachmentId}/**")
    public void getArchiveContentTarget(
            HttpServletRequest request,
            HttpServletResponse response,
            @PathVariable("attachmentId") Long attachmentId)
            throws IOException {
        Attachment attachment = attachmentManagerService.findAttachment(attachmentId);

        String uri = request.getRequestURI();
        String target = extractTargetFileFromURI(attachmentId, uri);
        FileViewerRequest viewerRequest = FileViewerRequest.fromAttachment(attachment, target);

        if (Objects.nonNull(viewerRequest.getFileType())) {
            FileViewerDto fileViewer = fileViewerService.preview(viewerRequest);
            displayPreview(response, fileViewer, viewerRequest.getFileName());
        } else {
            LOGGER.info(
                    "Unable to get the archive content: {}. The file extension is not supported.",
                    viewerRequest.getFileName());
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    private static String extractTargetFileFromURI(Long attachmentId, String uri) {
        final String targetPrefix = "target:";
        String target =
                uri.substring(uri.indexOf(attachmentId.toString()) + attachmentId.toString().length() + 1);

        if (target.contains(targetPrefix)) {
            target = target.substring(target.indexOf(targetPrefix) + targetPrefix.length());
        }
        return target;
    }

    @GetMapping(
            value = "/preview/file",
            params = {ORIGIN, SOURCE})
    public void previewFile(
            @RequestParam(ORIGIN) String origin,
            @RequestParam(SOURCE) String filePath,
            HttpServletResponse response)
            throws IOException {
        // This endpoint is only called for synchronisation / project import log files. We check that
        // the queried file is
        // inside logs / import folder. If you need this endpoint to support other scenarios, be careful
        // to check
        // the file path to avoid security issues (arbitrary file read).
        // You'll surely want to check user privileges also.
        final File logFile = logFileDownloadService.getLogFileWithPath(filePath, origin);
        FileViewerRequest viewerRequest = FileViewerRequest.fromFile(logFile);

        try {
            if (Objects.nonNull(viewerRequest.getFileType())) {
                FileViewerDto fileViewer = fileViewerService.preview(viewerRequest);
                displayPreview(response, fileViewer, viewerRequest.getFileName());
            }
        } catch (IOException e) {
            LOGGER.warn(ERROR_MESSAGE + e.getMessage(), e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(
            value = "/download/file",
            params = {ORIGIN, SOURCE},
            produces = MediaType.TEXT_PLAIN_VALUE)
    public FileSystemResource downloadFile(
            @RequestParam(ORIGIN) String origin,
            @RequestParam(SOURCE) String filePath,
            HttpServletResponse response)
            throws IOException {
        // This endpoint is only called for synchronisation / project import log files. We check that
        // the queried file is inside logs / import folder. If you need this endpoint to support other
        // scenarios, be careful to check the file path to avoid security issues (arbitrary file read).
        // You'll surely want to check user privileges also.
        final File logfile = logFileDownloadService.getLogFileWithPath(filePath, origin);
        response.setContentType(MediaType.TEXT_PLAIN_VALUE);
        response.setHeader(CONTENT_DISPOSITION, ATTACHMENT_FILENAME + logfile.getName());
        return new FileSystemResource(logfile);
    }

    private void displayPreview(
            HttpServletResponse response, FileViewerDto fileViewer, String formattedName)
            throws IOException {

        response.setHeader(CONTENT_DISPOSITION, "inline; filename=" + formattedName);
        response.setContentType(fileViewer.getType().media);
        response.setCharacterEncoding(Charset.defaultCharset().name());

        if (fileViewer.getType().isOutputStream) {
            OutputStream outputStream = response.getOutputStream();
            outputStream.write((byte[]) fileViewer.getContent());
        } else {
            PrintWriter writer = response.getWriter();
            writer.print(fileViewer.getContent());
            writer.flush();
        }
    }

    private void downloadAttachment(
            long attachmentId, HttpServletResponse response, String formattedName) throws IOException {
        response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
        response.setHeader(CONTENT_DISPOSITION, ATTACHMENT_FILENAME + formattedName);

        ServletOutputStream outStream = response.getOutputStream();
        attachmentManagerService.writeContent(attachmentId, outStream);
    }
}
