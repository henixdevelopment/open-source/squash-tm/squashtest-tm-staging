/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.user;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.synchronisation.RemoteSynchronisation;
import org.squashtest.tm.domain.users.UsersGroup;
import org.squashtest.tm.service.display.user.UserDisplayService;
import org.squashtest.tm.service.internal.display.dto.ProjectPermissionDto;
import org.squashtest.tm.service.internal.display.dto.UserAdminViewTeamDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.project.ProjectsPermissionManagementService;
import org.squashtest.tm.service.user.UserAdministrationService;

@RestController
@RequestMapping("/backend/users")
public class UsersAdministrationController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UsersAdministrationController.class);
    private final UserDisplayService userDisplayService;
    private final UserAdministrationService adminService;
    private final ProjectsPermissionManagementService permissionService;

    @Inject
    public UsersAdministrationController(
            UserDisplayService userDisplayService,
            UserAdministrationService adminService,
            ProjectsPermissionManagementService permissionService) {
        this.userDisplayService = userDisplayService;
        this.adminService = adminService;
        this.permissionService = permissionService;
    }

    @PostMapping
    public GridResponse getAllUsers(@RequestBody GridRequest request) {
        return userDisplayService.findAll(request);
    }

    @PostMapping(value = "/new")
    public Map<String, Long> addUser(@RequestBody @Valid UserForm userForm) {
        if (userForm.getPassword() == null) {
            adminService.createUserWithoutCredentials(userForm.getUser(), userForm.getGroupId());
        } else {
            adminService.addUser(userForm.getUser(), userForm.getGroupId(), userForm.getPassword());
        }

        return Collections.singletonMap("id", userForm.getUser().getId());
    }

    @GetMapping(value = "/get-users-groups")
    public Map<String, List<UsersGroup>> getUsersGroups() {

        Map<String, List<UsersGroup>> response = new HashMap<>();

        response.put("usersGroups", adminService.findAllUsersGroupOrderedByQualifiedName());
        return response;
    }

    @PostMapping(value = "/{userId}/login")
    public void modifyLogin(
            @PathVariable long userId, @RequestBody UsersAdministrationController.UserPatch patch) {
        adminService.modifyUserLogin(userId, patch.getLogin());
    }

    @PostMapping(value = "/{userId}/first-name")
    public void modifyFirstName(
            @PathVariable long userId, @RequestBody UsersAdministrationController.UserPatch patch) {
        adminService.modifyUserFirstName(userId, patch.getFirstName());
    }

    @PostMapping(value = "/{userId}/last-name")
    public void modifyLastName(
            @PathVariable long userId, @RequestBody UsersAdministrationController.UserPatch patch) {
        adminService.modifyUserLastName(userId, patch.getLastName());
    }

    @PostMapping(value = "/{userId}/email")
    public void modifyEmail(
            @PathVariable long userId, @RequestBody UsersAdministrationController.UserPatch patch) {
        adminService.modifyUserEmail(userId, patch.getEmail());
    }

    @DeleteMapping(value = "/{userIds}")
    public void deleteUsers(@PathVariable("userIds") List<Long> userIds) {
        adminService.deleteUsers(userIds);
    }

    @GetMapping(value = "/{userId}/synchronisations")
    public List<RemoteSynchronisation> getSynchronisationsByUser(
            @PathVariable("userId") Long userId) {
        return adminService.getSynchronisationsByUser(userId);
    }

    @PostMapping(value = "/{userIds}/deactivate")
    public void deactivateUsers(@PathVariable("userIds") List<Long> userIds) {
        adminService.deactivateUsers(userIds);
    }

    @PostMapping(value = "/{userIds}/activate")
    public void activateUsers(@PathVariable("userIds") List<Long> userIds) {
        adminService.activateUsers(userIds);
    }

    @PostMapping(value = "/{userId}/change-group/{groupId}")
    public void changeUserGroup(@PathVariable long userId, @PathVariable long groupId) {
        adminService.setUserGroupAuthority(userId, groupId);
    }

    @PostMapping(value = "/{userId}/reset-password")
    public void resetPassword(@PathVariable long userId, @RequestBody UserPatch patch) {
        LOGGER.trace("Reset password for user #{}", userId);
        adminService.resetUserPassword(userId, patch.getPassword());
    }

    @PostMapping("/{userId}/projects/{projectIds}/permissions/{profileId}")
    public Map<String, List<ProjectPermissionDto>> addNewPermissions(
            @PathVariable long userId,
            @PathVariable List<Long> projectIds,
            @PathVariable long profileId) {

        permissionService.addNewPermissionToProject(userId, projectIds, profileId);
        return Collections.singletonMap(
                "projectPermissions", userDisplayService.getProjectPermissions(userId));
    }

    @DeleteMapping(value = "/{userId}/permissions/{projectIds}")
    public void removePermissions(@PathVariable long userId, @PathVariable List<Long> projectIds) {
        permissionService.removeProjectPermission(userId, projectIds);
    }

    @PostMapping(value = "/{userId}/teams/{partyIds}")
    public AssociatedTeamReport associateTeams(
            @PathVariable long userId, @PathVariable List<Long> partyIds) {
        Set<String> nonAssociatedTeamNames = adminService.associateToTeams(userId, partyIds);
        List<UserAdminViewTeamDto> teams = userDisplayService.getAssociatedTeams(userId);

        return new AssociatedTeamReport(teams, nonAssociatedTeamNames);
    }

    public record AssociatedTeamReport(
            List<UserAdminViewTeamDto> teams, Set<String> nonAssociatedTeamNames) {}

    @DeleteMapping(value = "/{userId}/teams/{partyIds}")
    public void disassociateTeams(@PathVariable long userId, @PathVariable List<Long> partyIds) {
        adminService.deassociateTeams(userId, partyIds);
    }

    @PostMapping("/{userId}/can-delete-from-front")
    public void changeCanDeleteFromFront(@PathVariable long userId, @RequestBody UserPatch patch) {
        adminService.changeCanDeleteFromFront(userId, patch.isCanDeleteFromFront());
    }

    static class UserPatch {
        private String login;
        private String firstName;
        private String lastName;
        private String email;
        private String password;
        private boolean canDeleteFromFront;

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public boolean isCanDeleteFromFront() {
            return canDeleteFromFront;
        }

        public void setCanDeleteFromFront(boolean canDeleteFromFront) {
            this.canDeleteFromFront = canDeleteFromFront;
        }
    }

    @PostMapping("/{userIds}/mass-update")
    public void deactivateUsers(
            @PathVariable("userIds") List<Long> userIds, @RequestBody UserMassEditPatch massEditPatch) {
        if (massEditPatch.isChangeState()) {
            if (massEditPatch.isActivated()) {
                adminService.activateUsers(userIds);
            } else {
                adminService.deactivateUsers(userIds);
            }
        }

        if (massEditPatch.isChangeCanDelete()) {
            adminService.changeCanDeleteFromFront(userIds, massEditPatch.isCanDelete());
        }
    }

    public static final class UserMassEditPatch {
        private boolean changeState;
        private boolean activated;

        private boolean changeCanDelete;
        private boolean canDelete;

        public boolean isChangeState() {
            return changeState;
        }

        public void setChangeState(boolean changeState) {
            this.changeState = changeState;
        }

        public boolean isActivated() {
            return activated;
        }

        public void setActivated(boolean activated) {
            this.activated = activated;
        }

        public boolean isChangeCanDelete() {
            return changeCanDelete;
        }

        public void setChangeCanDelete(boolean changeCanDelete) {
            this.changeCanDelete = changeCanDelete;
        }

        public boolean isCanDelete() {
            return canDelete;
        }

        public void setCanDelete(boolean canDelete) {
            this.canDelete = canDelete;
        }
    }
}
