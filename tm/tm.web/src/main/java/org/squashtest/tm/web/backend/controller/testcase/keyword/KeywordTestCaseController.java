/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.testcase.keyword;

import com.google.common.collect.Lists;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.actionword.ActionWordService;
import org.squashtest.tm.service.testcase.bdd.KeywordTestCaseService;
import org.squashtest.tm.web.backend.controller.form.model.ActionList;
import org.squashtest.tm.web.backend.controller.form.model.ScriptPreviewModel;

@RestController
@RequestMapping("backend/keyword-test-cases")
public class KeywordTestCaseController {

    private final ActionWordService actionWordService;
    private final KeywordTestCaseService keywordTestCaseService;

    @Autowired
    public KeywordTestCaseController(
            Optional<ActionWordService> actionWordService,
            KeywordTestCaseService keywordTestCaseService) {
        this.actionWordService = actionWordService.orElse(null);
        this.keywordTestCaseService = keywordTestCaseService;
    }

    @PostMapping("/autocomplete")
    public ActionList findAllMatchingActionWords(@RequestBody ActionWordAutocompleteInput input) {
        return new ActionList(
                Lists.newArrayList(
                        getActionWordService()
                                .findAllMatchingActionWords(
                                        input.getProjectId(), input.getSearchInput(), input.getSelectedProjectsIds())));
    }

    @PostMapping("/duplicated-action")
    public Map<String, Long> findAllDuplicatedActionWithProject(
            @RequestBody ActionWordAutocompleteInput input) {
        return getActionWordService()
                .findAllDuplicatedActionWithProject(input.getProjectId(), input.getSearchInput());
    }

    @RequestMapping("/{testCaseId}/generated-script")
    public ScriptPreviewModel getGeneratedScript(@PathVariable long testCaseId) {
        return new ScriptPreviewModel(
                keywordTestCaseService.writeScriptFromTestCase(testCaseId, false));
    }

    private ActionWordService getActionWordService() {
        if (Objects.isNull(actionWordService)) {
            throw new AccessDeniedException("A dedicated plugin is required to manage action words.");
        }
        return actionWordService;
    }
}
