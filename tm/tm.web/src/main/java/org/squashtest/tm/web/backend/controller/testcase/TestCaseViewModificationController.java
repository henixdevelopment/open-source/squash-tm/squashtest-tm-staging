/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.testcase;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseAutomatable;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.domain.testcase.TestCaseStatus;
import org.squashtest.tm.service.display.testcase.TestCaseDisplayService;
import org.squashtest.tm.service.internal.display.dto.AutomationRequestDto;
import org.squashtest.tm.service.internal.display.testcase.parameter.TestCaseParameterOperationReport;
import org.squashtest.tm.service.testcase.TestCaseModificationService;
import org.squashtest.tm.service.tf.AutomationRequestModificationService;

@RestController
@RequestMapping(value = "backend/test-case/{testCaseId}")
public class TestCaseViewModificationController {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(TestCaseViewModificationController.class);

    private final TestCaseModificationService testCaseModificationService;

    private final AutomationRequestModificationService automationRequestModificationService;

    private final TestCaseDisplayService testCaseDisplayService;

    public TestCaseViewModificationController(
            TestCaseModificationService testCaseModificationService,
            AutomationRequestModificationService automationRequestModificationService,
            TestCaseDisplayService testCaseDisplayService) {
        this.testCaseModificationService = testCaseModificationService;
        this.automationRequestModificationService = automationRequestModificationService;
        this.testCaseDisplayService = testCaseDisplayService;
    }

    @PostMapping(value = "/importance")
    public void changeImportance(@PathVariable long testCaseId, @RequestBody TestCasePatch patch) {
        testCaseModificationService.changeImportance(
                testCaseId, TestCaseImportance.valueOf(patch.getImportance()));
    }

    @PostMapping(value = "/name")
    public void changeName(@PathVariable long testCaseId, @RequestBody TestCasePatch patch) {
        testCaseModificationService.rename(testCaseId, patch.getName());
    }

    @PostMapping(value = "/reference")
    public void changeReference(@PathVariable long testCaseId, @RequestBody TestCasePatch patch) {
        testCaseModificationService.changeReference(testCaseId, patch.getReference());
    }

    @PostMapping(value = "/status")
    public void changeStatus(@PathVariable long testCaseId, @RequestBody TestCasePatch patch) {
        testCaseModificationService.changeStatus(testCaseId, TestCaseStatus.valueOf(patch.getStatus()));
    }

    @PostMapping(value = "/importance-auto")
    public Map<String, String> changeImportanceAuto(
            @PathVariable long testCaseId, @RequestBody TestCasePatch patch) {
        testCaseModificationService.changeImportanceAuto(testCaseId, patch.isImportanceAuto());
        TestCase testCase = testCaseModificationService.findById(testCaseId);
        Map<String, String> testCaseImportance = new HashMap<>();
        testCaseImportance.put("importance", testCase.getImportance().name());
        return testCaseImportance;
    }

    @PostMapping(value = "/nature")
    public void changeNature(@PathVariable long testCaseId, @RequestBody TestCasePatch patch) {
        testCaseModificationService.changeNature(testCaseId, patch.getNature());
    }

    @PostMapping(value = "/type")
    public void changeType(@PathVariable long testCaseId, @RequestBody TestCasePatch patch) {
        testCaseModificationService.changeType(testCaseId, patch.getType());
    }

    @PostMapping(value = "/description")
    public void changeDescription(@RequestBody TestCasePatch patch, @PathVariable long testCaseId) {
        testCaseModificationService.changeDescription(testCaseId, patch.getDescription());
    }

    @PostMapping(value = "/milestones/{milestoneIds}")
    public void bindMilestones(
            @PathVariable long testCaseId, @PathVariable("milestoneIds") List<Long> milestoneIds) {
        testCaseModificationService.bindMilestones(testCaseId, milestoneIds);
    }

    @DeleteMapping(value = "/milestones/{milestoneIds}")
    public void unbindMilestones(
            @PathVariable long testCaseId, @PathVariable List<Long> milestoneIds) {
        testCaseModificationService.unbindMilestones(testCaseId, milestoneIds);
    }

    @PostMapping(value = "/automatable")
    public AutomationRequestDto changeAutomatable(
            @RequestBody TestCasePatch patch, @PathVariable long testCaseId) {
        return testCaseModificationService.changeAutomatable(
                TestCaseAutomatable.valueOf(patch.getAutomatable()), testCaseId);
    }

    @PostMapping(value = "/prerequisite")
    public TestCaseParameterOperationReport changePrerequisite(
            @PathVariable long testCaseId, @RequestBody TestCasePatch patch) {
        testCaseModificationService.changePrerequisite(testCaseId, patch.getPrerequisite());
        LOGGER.trace("test case {}: updated prerequisite to {}", testCaseId, patch.getPrerequisite());
        return testCaseDisplayService.findParametersData(testCaseId);
    }

    @PostMapping(value = "/automation-request/priority")
    public void changeAutomationRequestPriority(
            @RequestBody Map<String, Integer> request, @PathVariable long testCaseId) {
        automationRequestModificationService.changePriority(
                Collections.singletonList(testCaseId), request.get("priority"));
    }

    @PostMapping(value = "/scm-repository-id")
    public void changeScmRepository(
            @RequestBody TestCasePatch testCasePatch, @PathVariable long testCaseId) {
        if (testCasePatch.getScmRepositoryId() == null
                || testCasePatch.getScmRepositoryId().equals(0L)) {
            testCaseModificationService.unbindSourceCodeRepository(testCaseId);
        } else {
            String newScmRepositoryUrl =
                    testCaseModificationService.changeSourceCodeRepository(
                            testCaseId, testCasePatch.getScmRepositoryId());
            LOGGER.trace("Test case {}: updated git repository to {}", testCaseId, newScmRepositoryUrl);
        }
    }

    @PostMapping(value = "/automated-test-technology")
    public void changeAutomatedTestTechnology(
            @RequestBody TestCasePatch testCasePatch, @PathVariable long testCaseId) {
        if (testCasePatch.getAutomatedTestTechnology() != null) {
            testCaseModificationService.changeAutomatedTestTechnology(
                    testCaseId, testCasePatch.getAutomatedTestTechnology());
        } else {
            testCaseModificationService.unbindAutomatedTestTechnology(testCaseId);
        }
    }

    @PostMapping(value = "/automated-test-reference")
    public void changeAutomatedTestReference(
            @RequestBody TestCasePatch testCasePatch, @PathVariable long testCaseId) {
        testCaseModificationService.changeAutomatedTestReference(
                testCaseId, testCasePatch.getAutomatedTestReference());
    }

    static class TestCasePatch {
        private String name;
        private String reference;
        private String description;
        private String prerequisite;
        private String importance;
        private String status;
        private String automatable;
        private Long nature;
        private Long type;
        private boolean importanceAuto;
        private Long scmRepositoryId;
        private Long automatedTestTechnology;
        private String automatedTestReference;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getReference() {
            return reference;
        }

        public void setReference(String reference) {
            this.reference = reference;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImportance() {
            return importance;
        }

        public void setImportance(String importance) {
            this.importance = importance;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Long getNature() {
            return nature;
        }

        public void setNature(Long nature) {
            this.nature = nature;
        }

        public Long getType() {
            return type;
        }

        public void setType(Long type) {
            this.type = type;
        }

        public String getPrerequisite() {
            return prerequisite;
        }

        public void setPrerequisite(String prerequisite) {
            this.prerequisite = prerequisite;
        }

        public boolean isImportanceAuto() {
            return importanceAuto;
        }

        public void setImportanceAuto(boolean importanceAuto) {
            this.importanceAuto = importanceAuto;
        }

        public String getAutomatable() {
            return automatable;
        }

        public void setAutomatable(String automatable) {
            this.automatable = automatable;
        }

        public Long getScmRepositoryId() {
            return scmRepositoryId;
        }

        public void setScmRepositoryId(Long scmRepositoryId) {
            this.scmRepositoryId = scmRepositoryId;
        }

        public Long getAutomatedTestTechnology() {
            return automatedTestTechnology;
        }

        public void setAutomatedTestTechnology(Long automatedTestTechnology) {
            this.automatedTestTechnology = automatedTestTechnology;
        }

        public String getAutomatedTestReference() {
            return automatedTestReference;
        }

        public void setAutomatedTestReference(String automatedTestReference) {
            this.automatedTestReference = automatedTestReference;
        }
    }
}
