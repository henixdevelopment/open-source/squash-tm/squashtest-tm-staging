/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.execution;

import java.util.Date;
import java.util.List;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.display.execution.ExploratorySessionOverviewDisplayService;
import org.squashtest.tm.service.execution.ExploratorySessionOverviewModificationService;
import org.squashtest.tm.service.internal.display.dto.UserView;
import org.squashtest.tm.service.internal.display.dto.execution.ExploratorySessionOverviewView;
import org.squashtest.tm.service.internal.display.dto.execution.SessionNoteDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;

@RestController()
@RequestMapping("/backend/session-overview/")
public class ExploratorySessionOverviewController {

    private final ExploratorySessionOverviewDisplayService exploratorySessionOverviewDisplayService;
    private final ExploratorySessionOverviewModificationService
            exploratorySessionOverviewModificationService;

    public ExploratorySessionOverviewController(
            ExploratorySessionOverviewDisplayService exploratorySessionOverviewDisplayService,
            ExploratorySessionOverviewModificationService exploratorySessionOverviewModificationService) {
        this.exploratorySessionOverviewDisplayService = exploratorySessionOverviewDisplayService;
        this.exploratorySessionOverviewModificationService =
                exploratorySessionOverviewModificationService;
    }

    @GetMapping("{overviewId}")
    public ExploratorySessionOverviewView getView(@PathVariable Long overviewId) {
        return exploratorySessionOverviewDisplayService.findById(overviewId);
    }

    @GetMapping("{overviewId}/unassigned-users")
    public List<UserView> getUnassignedUsers(@PathVariable Long overviewId) {
        return exploratorySessionOverviewDisplayService.getUnassignedUsers(overviewId);
    }

    @PostMapping("{overviewId}/executions")
    public GridResponse getExecutions(
            @PathVariable Long overviewId, @RequestBody GridRequest gridRequest) {
        return exploratorySessionOverviewDisplayService.getExecutionGrid(overviewId, gridRequest);
    }

    @PostMapping("{overviewId}/new-execution")
    public void addNewExecution(@PathVariable Long overviewId) {
        exploratorySessionOverviewModificationService.addNewExecution(overviewId);
    }

    @PostMapping("{overviewId}/due-date")
    public void updateDueDate(
            @PathVariable Long overviewId, @RequestBody SessionOverviewPatch requestBody) {
        exploratorySessionOverviewModificationService.updateDueDate(overviewId, requestBody.dueDate());
    }

    @PostMapping("{overviewId}/execution-status")
    public void updateExecutionStatus(
            @PathVariable Long overviewId, @RequestBody SessionOverviewPatch requestBody) {
        exploratorySessionOverviewModificationService.updateExecutionStatus(
                overviewId, requestBody.executionStatus());
    }

    @PostMapping("{overviewId}/session-duration")
    public void updateSessionDuration(
            @PathVariable Long overviewId, @RequestBody SessionOverviewPatch requestBody) {
        exploratorySessionOverviewModificationService.updateSessionDuration(
                overviewId, requestBody.sessionDuration());
    }

    @DeleteMapping(value = "{overviewId}/executions/{executionIds}")
    public void deleteExecutions(
            @PathVariable Long overviewId, @PathVariable("executionIds") List<Long> executionIds) {
        exploratorySessionOverviewModificationService.deleteExecutions(overviewId, executionIds);
    }

    @PostMapping(value = "{overviewId}/add-executions-with-users/{userIds}")
    public void addExecutionsWithUsers(
            @PathVariable Long overviewId, @PathVariable("userIds") List<Long> userIds) {
        exploratorySessionOverviewModificationService.addExecutionsWithUsers(overviewId, userIds);
    }

    @PostMapping("{overviewId}/start-session")
    public void startSession(@PathVariable Long overviewId) {
        exploratorySessionOverviewModificationService.startSessionAndUpdateExecutionStatus(overviewId);
    }

    @PostMapping("{overviewId}/end-session")
    public void endSession(@PathVariable Long overviewId) {
        exploratorySessionOverviewModificationService.endSession(overviewId);
    }

    @PostMapping("{overviewId}/comments")
    public void updateComment(
            @PathVariable Long overviewId, @RequestBody SessionOverviewPatch requestBody) {
        exploratorySessionOverviewModificationService.updateComments(
                overviewId, requestBody.comments());
    }

    @GetMapping("{overviewId}/session-notes")
    public List<SessionNoteDto> getSessionNotes(@PathVariable Long overviewId) {
        return exploratorySessionOverviewDisplayService.getAllSessionNotes(overviewId);
    }

    record SessionOverviewPatch(
            Date dueDate, String executionStatus, Integer sessionDuration, String comments) {}
}
