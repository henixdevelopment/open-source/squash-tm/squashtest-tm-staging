/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.campaign;

import java.util.List;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.campaign.TestSuiteTestPlanManagerService;

@RestController
@RequestMapping("backend/test-suites/test-plan")
public class TestSuiteTestPlanModificationController {

    TestSuiteTestPlanManagerService testSuiteTestPlanManagerService;

    TestSuiteTestPlanModificationController(
            TestSuiteTestPlanManagerService testSuiteTestPlanManagerService) {
        this.testSuiteTestPlanManagerService = testSuiteTestPlanManagerService;
    }

    @PostMapping(value = "/bind")
    public void bindTestPlans(@RequestBody BindRequest bindRequest) {
        testSuiteTestPlanManagerService.bindTestPlanToMultipleSuites(
                bindRequest.testSuiteIds, bindRequest.itemIds);
    }

    public static class BindRequest {
        List<Long> itemIds;
        List<Long> testSuiteIds;

        public List<Long> getItemIds() {
            return itemIds;
        }

        public void setItemIds(List<Long> itemIds) {
            this.itemIds = itemIds;
        }

        public List<Long> getTestSuiteIds() {
            return testSuiteIds;
        }

        public void setTestSuiteIds(List<Long> testSuiteIds) {
            this.testSuiteIds = testSuiteIds;
        }
    }
}
