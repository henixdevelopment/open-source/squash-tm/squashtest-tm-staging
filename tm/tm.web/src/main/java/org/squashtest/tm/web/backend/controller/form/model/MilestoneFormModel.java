/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.form.model;

import java.util.Date;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.milestone.MilestoneStatus;

public class MilestoneFormModel {

    private String label;
    private String status;
    private String description;
    private Date endDate;

    public Milestone getMilestone() {
        Milestone milestone = new Milestone();
        milestone.setLabel(this.getLabel());
        milestone.setDescription(this.getDescription());
        milestone.setStatus(this.getMilestoneStatus());
        milestone.setEndDate(this.getEndDate());
        return milestone;
    }

    private MilestoneStatus getMilestoneStatus() {
        return switch (status) {
            case "IN_PROGRESS" -> MilestoneStatus.IN_PROGRESS;
            case "FINISHED" -> MilestoneStatus.FINISHED;
            case "LOCKED" -> MilestoneStatus.LOCKED;
            default -> MilestoneStatus.PLANNED;
        };
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
