/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.home;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2ClientProperties;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;
import org.squashtest.tm.service.plugin.PluginFinderService;
import org.squashtest.tm.service.system.SystemAdministrationService;

@RestController
@RequestMapping("backend/login-page")
public class LoginPageController {

    private final SystemAdministrationService systemAdministrationService;
    private final PluginFinderService pluginFinderService;

    @Value("${info.app.version}")
    private String version;

    // Issue 7509, Spring is a myth, please don't replace @Autowired by @Inject,
    // otherwise, the ClassLoader can not find the class used in Trac plugin
    @Autowired private Environment environment;

    @Autowired(required = false)
    private OAuth2ClientProperties oAuth2ClientProperties;

    public LoginPageController(
            SystemAdministrationService systemAdministrationService,
            PluginFinderService pluginFinderService) {
        this.systemAdministrationService = systemAdministrationService;
        this.pluginFinderService = pluginFinderService;
    }

    @GetMapping
    public WelcomePageData loadLoginPage() {
        String loginMessage = systemAdministrationService.findLoginMessage();
        boolean isH2 = isH2();
        boolean isOpenIdConnectPluginInstalled = isOpenIdConnectPluginInstalled();
        Set<String> oAuth2ProviderNames = getConfiguredOAuth2Providers();
        return new WelcomePageData(
                loginMessage, version, isH2, isOpenIdConnectPluginInstalled, oAuth2ProviderNames);
    }

    private boolean isH2() {
        List<String> activeProfiles = Arrays.asList(environment.getActiveProfiles());
        return activeProfiles.contains("h2");
    }

    private boolean isOpenIdConnectPluginInstalled() {
        return pluginFinderService.isOpenIdConnectPluginInstalled();
    }

    private Set<String> getConfiguredOAuth2Providers() {
        if (oAuth2ClientProperties != null
                && !oAuth2ClientProperties.getProvider().keySet().isEmpty()) {
            return formatOAuth2ProviderNames(oAuth2ClientProperties.getProvider().keySet());
        }
        return Collections.emptySet();
    }

    private Set<String> formatOAuth2ProviderNames(Set<String> oAuth2ProviderNames) {
        return oAuth2ProviderNames.stream()
                .map(String::toLowerCase)
                .sorted(String::compareToIgnoreCase)
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    static class WelcomePageData {
        private final String loginMessage;

        private final String squashVersion;

        private final boolean isH2;

        private final boolean isOpenIdConnectPluginInstalled;

        private final Set<String> oAuth2ProviderNames;

        public WelcomePageData(
                String loginMessage,
                String squashVersion,
                boolean isH2,
                boolean isOpenIdConnectPluginInstalled,
                Set<String> oAuth2ProviderNames) {
            this.loginMessage = HTMLCleanupUtils.cleanHtml(loginMessage);
            this.squashVersion = squashVersion;
            this.isH2 = isH2;
            this.isOpenIdConnectPluginInstalled = isOpenIdConnectPluginInstalled;
            this.oAuth2ProviderNames = oAuth2ProviderNames;
        }

        public String getLoginMessage() {
            return loginMessage;
        }

        public String getSquashVersion() {
            return squashVersion;
        }

        public Set<String> getoAuth2ProviderNames() {
            return oAuth2ProviderNames;
        }

        @JsonProperty("isH2")
        public boolean isH2() {
            return isH2;
        }

        @JsonProperty("isOpenIdConnectPluginInstalled")
        public boolean isOpenIdConnectPluginInstalled() {
            return isOpenIdConnectPluginInstalled;
        }
    }
}
