/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.form.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.HashMap;
import java.util.Map;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.service.internal.dto.RawValueModel;

/** Base class for entities attached to a project. These have common attributes. */
public class EntityFormModel {

    private String name;
    private String reference;
    private String description;
    private String parentEntityReference;
    private Long position;
    private RawValueModel.RawValueModelMap customFields = new RawValueModel.RawValueModelMap();

    @JsonIgnore
    public Map<Long, RawValue> getCufs() {
        Map<Long, RawValue> cufs = new HashMap<>(customFields.size());
        for (Map.Entry<Long, RawValueModel> entry : customFields.entrySet()) {
            cufs.put(entry.getKey(), entry.getValue().toRawValue());
        }
        return cufs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getParentEntityReference() {
        return parentEntityReference;
    }

    public void setParentEntityReference(String parentEntityReference) {
        this.parentEntityReference = parentEntityReference;
    }

    public Long getPosition() {
        return position;
    }

    public void setPosition(Long position) {
        this.position = position;
    }

    public RawValueModel.RawValueModelMap getCustomFields() {
        return customFields;
    }

    public void setCustomFields(RawValueModel.RawValueModelMap customFields) {
        this.customFields = customFields;
    }

    public EntityReference getParentNodeReference() {
        return EntityReference.fromNodeId(this.parentEntityReference);
    }
}
