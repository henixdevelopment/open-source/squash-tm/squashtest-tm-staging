/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.sprintreqversion;

import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.display.sprint.SprintDisplayService;
import org.squashtest.tm.service.internal.display.dto.sprint.AvailableTestPlanItemDto;
import org.squashtest.tm.service.internal.display.dto.sprint.SprintReqVersionView;

@RestController
@RequestMapping("/backend/sprint-req-version-view")
public class SprintReqVersionViewController {

    private final SprintDisplayService sprintDisplayService;

    public SprintReqVersionViewController(SprintDisplayService sprintDisplayService) {
        this.sprintDisplayService = sprintDisplayService;
    }

    @GetMapping("{sprintReqVersionId}")
    public SprintReqVersionView getSprintReqVersionView(@PathVariable long sprintReqVersionId) {
        return sprintDisplayService.findSprintReqVersionViewById(sprintReqVersionId);
    }

    @GetMapping("{sprintReqVersionId}/available-test-plan-items")
    public List<AvailableTestPlanItemDto> getAvailableTestPlanItems(
            @PathVariable long sprintReqVersionId) {
        return sprintDisplayService.findAvailableTestPlanItems(sprintReqVersionId);
    }
}
