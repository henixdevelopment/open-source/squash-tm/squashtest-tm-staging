/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.testcase;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.inject.Inject;
import org.springframework.context.MessageSource;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.domain.NodeWorkspace;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.domain.testcase.TestCaseLibrary;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.exception.library.RightsUnsuficientsForOperationException;
import org.squashtest.tm.exception.requirement.MilestoneForbidModificationException;
import org.squashtest.tm.service.clipboard.model.ClipboardPayload;
import org.squashtest.tm.service.display.workspace.tree.DatasetDuplicationTreeBrowser;
import org.squashtest.tm.service.display.workspace.tree.MultipleHierarchyTreeBrowser;
import org.squashtest.tm.service.display.workspace.tree.TreeNodeCollectorService;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.TreeGridResponse;
import org.squashtest.tm.service.internal.display.grid.TreeRequest;
import org.squashtest.tm.service.internal.display.testcase.parameter.TestCaseParameterOperationReport;
import org.squashtest.tm.service.library.LibraryNavigationService;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;
import org.squashtest.tm.service.testcase.DatasetCopierService;
import org.squashtest.tm.service.testcase.TestCaseLibraryNavigationService;
import org.squashtest.tm.service.testcase.TestCaseModificationService;
import org.squashtest.tm.service.testcase.fromreq.ReqToTestCaseConfiguration;
import org.squashtest.tm.web.backend.controller.form.model.EntityFormModelValidator;
import org.squashtest.tm.web.backend.controller.form.model.NodeList;
import org.squashtest.tm.web.backend.controller.form.model.NodeListAndWhiteList;
import org.squashtest.tm.web.backend.controller.form.model.TestCaseFolderFormModel;
import org.squashtest.tm.web.backend.controller.form.model.TestCaseFormModel;
import org.squashtest.tm.web.backend.controller.navigation.AbstractLibraryNavigationController;

@RestController
@RequestMapping(path = "backend/test-case-tree")
public class TestCaseLibraryNavigationController
        extends AbstractLibraryNavigationController<
                TestCaseLibrary, TestCaseFolder, TestCaseLibraryNode> {

    public static final Logger LOGGER =
            LoggerFactory.getLogger(TestCaseLibraryNavigationController.class);

    private static final String ADD_TEST_CASE = "add-test-case";
    private static final String ADD_TEST_CASE_FOLDER = "add-test-case-folder";

    private final ActiveMilestoneHolder activeMilestoneHolder;
    private final TestCaseLibraryNavigationService testCaseLibraryNavigationService;
    private final TestCaseModificationService testCaseModificationService;
    private final TreeNodeCollectorService treeNodeCollectorService;
    private final DatasetDuplicationTreeBrowser datasetDuplicationTreeBrowser;
    private final DatasetCopierService datasetCopierService;

    @Inject
    public TestCaseLibraryNavigationController(
            DatasetDuplicationTreeBrowser datasetDuplicationTreeBrowser,
            MultipleHierarchyTreeBrowser treeBrowser,
            ActiveMilestoneHolder activeMilestoneHolder,
            TestCaseLibraryNavigationService testCaseLibraryNavigationService,
            TestCaseModificationService testCaseModificationService,
            TreeNodeCollectorService treeNodeCollectorService,
            MessageSource messageSource,
            DatasetCopierService datasetCopierService) {
        super(treeBrowser, messageSource);
        this.datasetDuplicationTreeBrowser = datasetDuplicationTreeBrowser;
        this.activeMilestoneHolder = activeMilestoneHolder;
        this.testCaseLibraryNavigationService = testCaseLibraryNavigationService;
        this.testCaseModificationService = testCaseModificationService;
        this.treeNodeCollectorService = treeNodeCollectorService;
        this.datasetCopierService = datasetCopierService;
    }

    @PostMapping("dataset-duplication")
    public TreeGridResponse getTestCasesEligibleForDatasetDuplication(
            @RequestBody TreeRequest treeRequest) {
        return datasetDuplicationTreeBrowser.getInitialTree(
                NodeWorkspace.TEST_CASE,
                NodeReference.fromNodeIds(treeRequest.getOpenedNodes()),
                NodeReference.fromNodeIds(treeRequest.getSelectedNodes()));
    }

    @GetMapping("dataset-duplication/{testCaseId}/{ids}/content")
    public TreeGridResponse getChildren(
            @PathVariable Long testCaseId, @PathVariable List<String> ids) {
        Set<NodeReference> nodeReference = NodeReference.fromNodeIds(ids);
        return datasetDuplicationTreeBrowser.findSubHierarchyFilteredTestCases(
                nodeReference, new HashSet<>(nodeReference), testCaseId);
    }

    @PostMapping("dataset-duplication/test-case/{sourceTestCaseId}/paste")
    public Map<Long, TestCaseParameterOperationReport> pasteDatasets(
            @RequestBody DatasetDuplicationPatch datasetDuplicationPatch,
            @PathVariable("sourceTestCaseId") Long sourceTestCaseId) {
        return this.datasetCopierService.copyDataset(
                datasetDuplicationPatch.getSourceDatasetId(),
                sourceTestCaseId,
                datasetDuplicationPatch.getTargetTestCasesIds());
    }

    @PostMapping(
            value = "new-test-case",
            consumes = "application/json",
            produces = "application/json")
    public DataRow addNewTestCase(@RequestBody TestCaseFormModel testCaseModel) throws BindException {

        validateTestCaseFormModel(testCaseModel);

        TestCase testCase = testCaseModel.getTestCase();
        Map<Long, RawValue> customFieldValues = testCaseModel.getCufs();

        List<Long> milestoneIds = findMilestoneIds();
        String serializedParentEntityReference = testCaseModel.getParentEntityReference();
        EntityReference parentEntityReference =
                EntityReference.fromNodeId(serializedParentEntityReference);
        switch (parentEntityReference.getType()) {
            case TEST_CASE_LIBRARY:
                testCaseLibraryNavigationService.addTestCaseToLibrary(
                        parentEntityReference.getId(), testCase, customFieldValues, null, milestoneIds);
                break;
            case TEST_CASE_FOLDER:
                testCaseLibraryNavigationService.addTestCaseToFolder(
                        parentEntityReference.getId(), testCase, customFieldValues, null, milestoneIds);
                break;
            default:
                throw new IllegalArgumentException(
                        "This entity type is not handled " + serializedParentEntityReference);
        }
        return treeNodeCollectorService.collectNode(NodeType.TEST_CASE, testCase);
    }

    @PostMapping(value = "new-folder", consumes = "application/json", produces = "application/json")
    public DataRow addNewFolder(@RequestBody TestCaseFolderFormModel folderModel)
            throws BindException {

        validateTestCaseFolderFormModel(folderModel);

        TestCaseFolder testCaseFolder = folderModel.getTestCaseFolder();

        Map<Long, RawValue> customFieldValues = folderModel.getCufs();

        String serializedParentEntityReference = folderModel.getParentEntityReference();
        EntityReference parentEntityReference =
                EntityReference.fromNodeId(serializedParentEntityReference);
        switch (parentEntityReference.getType()) {
            case TEST_CASE_LIBRARY:
                testCaseLibraryNavigationService.addFolderToLibrary(
                        parentEntityReference.getId(), testCaseFolder, customFieldValues);
                break;
            case TEST_CASE_FOLDER:
                testCaseLibraryNavigationService.addFolderToFolder(
                        parentEntityReference.getId(), testCaseFolder, customFieldValues);
                break;
            default:
                throw new IllegalArgumentException(
                        "This entity type is not handled " + serializedParentEntityReference);
        }
        return treeNodeCollectorService.collectNode(NodeType.TEST_CASE_FOLDER, testCaseFolder);
    }

    @PostMapping(value = "/{destinationId}/content/paste")
    public void copyNodes(
            @RequestBody() NodeListAndWhiteList nodeListAndWhiteList,
            @PathVariable("destinationId") String destinationId) {
        NodeReference nodeReference = NodeReference.fromNodeId(destinationId);
        List<Long> copiedNodeIds = nodeListAndWhiteList.getIds();
        ClipboardPayload clipboardPayload = nodeListAndWhiteList.asClipboardPayload();

        try {
            switch (nodeReference.getNodeType()) {
                case TEST_CASE_FOLDER:
                    this.testCaseLibraryNavigationService.copyNodesToFolder(
                            nodeReference.getId(), copiedNodeIds.toArray(new Long[0]), clipboardPayload);
                    break;
                case TEST_CASE_LIBRARY:
                    this.testCaseLibraryNavigationService.copyNodesToLibrary(
                            nodeReference.getId(), copiedNodeIds.toArray(new Long[0]), clipboardPayload);
                    break;
                default:
                    throw new IllegalArgumentException(
                            "copy nodes : specified destination type doesn't exists : "
                                    + nodeReference.getNodeType());
            }
        } catch (AccessDeniedException ade) {
            throw new RightsUnsuficientsForOperationException(ade);
        }
    }

    @PostMapping(value = "/{destinationId}/content/paste-from-requirement/{tcKind}")
    public void copyFromRequirementToTestCases(
            @PathVariable("destinationId") String destinationId,
            @PathVariable("tcKind") String tcKind,
            @RequestBody() NodeList nodeList,
            Locale locale) {
        NodeReference nodeReference = NodeReference.fromNodeId(destinationId);
        List<Long> copiedNodeIdsList = nodeList.getIds();

        Long[] copiedNodeIds = copiedNodeIdsList.toArray(new Long[0]);

        ReqToTestCaseConfiguration configuration = new ReqToTestCaseConfiguration(tcKind);

        try {
            switch (nodeReference.getNodeType()) {
                case TEST_CASE_FOLDER:
                    testCaseLibraryNavigationService.copyReqToTestCasesToFolder(
                            nodeReference.getId(), copiedNodeIds, configuration);
                    break;
                case TEST_CASE_LIBRARY:
                    testCaseLibraryNavigationService.copyReqToTestCasesToLibrary(
                            nodeReference.getId(), copiedNodeIds, configuration);
                    break;
                case TEST_CASE:
                    testCaseLibraryNavigationService.copyReqToTestCasesToTestCases(
                            nodeReference.getId(), copiedNodeIds, configuration);
                    break;
                default:
                    throw new IllegalArgumentException(
                            "copy nodes : specified destination type doesn't exists : "
                                    + nodeReference.getNodeType());
            }
        } catch (AccessDeniedException ade) {
            throw new RightsUnsuficientsForOperationException(ade);
        }
    }

    @PostMapping(value = "/{destinationRef}/content/move")
    public void moveNodes(
            @RequestBody() NodeList nodeList, @PathVariable("destinationRef") String destinationRef) {
        NodeReference nodeReference = NodeReference.fromNodeId(destinationRef);
        Long destinationId = nodeReference.getId();
        NodeType destinationType = nodeReference.getNodeType();
        Long[] movedNodeIds = nodeList.getIds().toArray(new Long[0]);
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(nodeList.getIds());

        try {
            switch (destinationType) {
                case TEST_CASE_FOLDER:
                    testCaseLibraryNavigationService.moveNodesToFolder(
                            destinationId, movedNodeIds, clipboardPayload);
                    break;
                case TEST_CASE_LIBRARY:
                    testCaseLibraryNavigationService.moveNodesToLibrary(
                            destinationId, movedNodeIds, clipboardPayload);
                    break;
                default:
                    throw new IllegalArgumentException(
                            "move nodes : specified destination type doesn't exists : " + destinationType);
            }
        } catch (AccessDeniedException ade) {
            throw new RightsUnsuficientsForOperationException(ade);
        }
    }

    @PostMapping(value = "/{destinationRef}/content/move/{position}")
    public void moveNodesAtPosition(
            @RequestBody() NodeList nodeList,
            @PathVariable("destinationRef") String destinationRef,
            @PathVariable("position") int position) {
        NodeReference nodeReference = NodeReference.fromNodeId(destinationRef);
        Long destinationId = nodeReference.getId();
        NodeType destinationType = nodeReference.getNodeType();
        Long[] movedNodeIds = nodeList.getIds().toArray(new Long[0]);
        ClipboardPayload clipboardPayload = ClipboardPayload.withWhiteListIgnored(nodeList.getIds());

        try {
            switch (destinationType) {
                case TEST_CASE_FOLDER:
                    testCaseLibraryNavigationService.moveNodesToFolder(
                            destinationId, movedNodeIds, position, clipboardPayload);
                    break;
                case TEST_CASE_LIBRARY:
                    testCaseLibraryNavigationService.moveNodesToLibrary(
                            destinationId, movedNodeIds, position, clipboardPayload);
                    break;
                default:
                    throw new IllegalArgumentException(
                            "move nodes : specified destination type doesn't exists : " + destinationType);
            }
        } catch (AccessDeniedException ade) {
            throw new RightsUnsuficientsForOperationException(ade);
        }
    }

    @PostMapping(
            value = "{testCaseId}/new-version",
            consumes = "application/json",
            produces = "application/json")
    public NewTestCaseVersion createNewVersion(
            @PathVariable Long testCaseId, @RequestBody TestCaseFormModel testCaseFormModel)
            throws BindException, MilestoneForbidModificationException {
        validateTestCaseFormModel(testCaseFormModel);
        TestCase testCase = testCaseFormModel.getTestCase();
        TestCase newTestCase = testCaseModificationService.addNewTestCaseVersion(testCaseId, testCase);
        return new NewTestCaseVersion(newTestCase.getId());
    }

    @PostMapping(value = "/transmit-eligible-tcs")
    public Map<String, Object> transmitEligibleNodes(
            @RequestBody Map<String, List<Long>> selectedNodes) {
        return testCaseModificationService.transmitEligibleNodes(selectedNodes);
    }

    private void validateTestCaseFormModel(TestCaseFormModel testCaseModel) throws BindException {
        BindingResult validation = new BeanPropertyBindingResult(testCaseModel, ADD_TEST_CASE);
        EntityFormModelValidator entityFormModelValidator = new EntityFormModelValidator();
        entityFormModelValidator.validate(testCaseModel, validation);

        if (validation.hasErrors()) {
            throw new BindException(validation);
        }
    }

    private void validateTestCaseFolderFormModel(TestCaseFolderFormModel folderModel)
            throws BindException {
        BindingResult validation = new BeanPropertyBindingResult(folderModel, ADD_TEST_CASE_FOLDER);
        EntityFormModelValidator entityFormModelValidator = new EntityFormModelValidator();
        entityFormModelValidator.validate(folderModel, validation);

        if (validation.hasErrors()) {
            throw new BindException(validation);
        }
    }

    private List<Long> findMilestoneIds() {
        List<Long> milestoneIds = new ArrayList<>();
        Optional<Milestone> activeMilestone = activeMilestoneHolder.getActiveMilestone();
        activeMilestone.ifPresent(milestone -> milestoneIds.add(milestone.getId()));
        return milestoneIds;
    }

    @Override
    protected LibraryNavigationService<TestCaseLibrary, TestCaseFolder, TestCaseLibraryNode>
            getLibraryNavigationService() {
        return this.testCaseLibraryNavigationService;
    }

    @Override
    protected NodeWorkspace getWorkspace() {
        return NodeWorkspace.TEST_CASE;
    }

    static class NewTestCaseVersion {
        private final Long newVersionId;

        NewTestCaseVersion(Long newVersionId) {
            this.newVersionId = newVersionId;
        }

        public Long getNewVersionId() {
            return newVersionId;
        }
    }

    public static class DatasetDuplicationPatch {

        List<Long> targetTestCasesIds;
        Long sourceDatasetId;

        public List<Long> getTargetTestCasesIds() {
            return targetTestCasesIds;
        }

        public Long getSourceDatasetId() {
            return sourceDatasetId;
        }
    }
}
