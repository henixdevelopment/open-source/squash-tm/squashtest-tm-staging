/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.exceptionresolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.AbstractHandlerExceptionResolver;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;
import org.squashtest.tm.core.foundation.exception.ActionException;
import org.squashtest.tm.web.backend.exceptionresolver.model.ActionValidationErrorModel;
import org.squashtest.tm.web.backend.exceptionresolver.model.SquashActionErrorModel;

@Component
public class HandlerActionExceptionResolver extends AbstractHandlerExceptionResolver {

    public HandlerActionExceptionResolver() {
        super();
    }

    @Override
    protected ModelAndView doResolveException(
            HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {

        if (exceptionIsHandled(ex)) {
            return handleException(response, ex);
        }

        return null;
    }

    private ModelAndView handleException(HttpServletResponse response, Exception ex) {
        ActionException actionEx = (ActionException) ex; // NOSONAR Type was checked earlier

        return formatJsonResponse(response, actionEx);
    }

    private ModelAndView formatJsonResponse(HttpServletResponse response, ActionException actionEx) {
        response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
        String exception = actionEx.getClass().getSimpleName();
        ActionValidationErrorModel error =
                new ActionValidationErrorModel(exception, actionEx.getI18nKey(), actionEx.messageArgs());

        SquashActionErrorModel squashError = new SquashActionErrorModel(error);
        return new ModelAndView(new MappingJackson2JsonView(), "squashTMError", squashError);
    }

    private boolean exceptionIsHandled(Exception ex) {
        return ActionException.class.isAssignableFrom(ex.getClass());
    }
}
