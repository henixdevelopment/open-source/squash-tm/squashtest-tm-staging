/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.config;

import static org.squashtest.tm.service.internal.jwt.JwtTokenServiceImpl.INVALID_TOKEN;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import org.squashtest.tm.api.security.authentication.ApiTokenPermissionsExemptionEndPoint;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.service.internal.security.SquashUserDetailsManager;
import org.squashtest.tm.service.jwt.JwtTokenService;
import org.squashtest.tm.service.user.UserManagerService;

@Component
public class JwtTokenFilter extends OncePerRequestFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(JwtTokenFilter.class);

    @Inject
    @Named("squashtest.core.security.JdbcUserDetailsManager")
    private SquashUserDetailsManager userManager;

    @Inject private JwtTokenService jwtTokenService;

    @Inject private JwtConfig jwtConfig;

    @Inject private UserManagerService userManagerService;

    @Autowired(required = false)
    private List<ApiTokenPermissionsExemptionEndPoint> tokenPermissionsExemptionEndPoints =
            Collections.emptyList();

    @Override
    protected void doFilterInternal(
            HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        try {
            String token = extractTokenFromRequest(request);
            List<String> endPointExemptions =
                    tokenPermissionsExemptionEndPoints.stream()
                            .map(ApiTokenPermissionsExemptionEndPoint::getIgnorePermissionsEndPoints)
                            .flatMap(List::stream)
                            .toList();

            if (token != null
                    && jwtTokenService.validateApiToken(
                            endPointExemptions, token, jwtConfig.getJwtSecret(), request)) {
                String userId = jwtTokenService.getUserIdFromToken(token, jwtConfig.getJwtSecret());
                String login = userManagerService.findLoginByUserId(Long.parseLong(userId));

                UserDetails userDetails = userManager.loadUserByUsername(login);
                UsernamePasswordAuthenticationToken authentication =
                        new UsernamePasswordAuthenticationToken(
                                userDetails, null, userDetails.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        } catch (BadCredentialsException e) {
            LOGGER.error(INVALID_TOKEN, e);
            SecurityContextHolder.clearContext();
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, e.getMessage());
            return;
        }

        filterChain.doFilter(request, response);
    }

    private String extractTokenFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader("Authorization");
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        } else {
            return null;
        }
    }
}
