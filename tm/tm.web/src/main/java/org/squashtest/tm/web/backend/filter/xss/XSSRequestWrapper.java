/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.filter.xss;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import org.jsoup.Jsoup;
import org.jsoup.safety.Safelist;
import org.owasp.encoder.esapi.ESAPIEncoder;
import org.owasp.esapi.errors.IntrusionException;

public class XSSRequestWrapper extends HttpServletRequestWrapper {

    private static final String USER_FACING_MESSAGE = "Intrusion detected";
    private static final String HEADER_REFERER = "referer";

    /**
     * Constructs a request object wrapping the given request.
     *
     * @param request the {@link HttpServletRequest} to be wrapped.
     * @throws IllegalArgumentException if the request is null
     */
    public XSSRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    @Override
    public Map<String, String[]> getParameterMap() {
        Map<String, String[]> parameterMap = super.getParameterMap();
        if (Objects.isNull(parameterMap)) {
            return null;
        }
        parameterMap.values().forEach(this::stripXSS);
        return parameterMap;
    }

    @Override
    public String[] getParameterValues(String name) {
        String[] parameterValues = super.getParameterValues(name);
        if (Objects.isNull(parameterValues)) {
            return null;
        }
        stripXSS(parameterValues);
        return parameterValues;
    }

    @Override
    public String getParameter(String name) {
        String parameterValue = super.getParameter(name);
        doStripXSS(parameterValue, StripOptions.STRICT);
        return parameterValue;
    }

    @Override
    public Enumeration<String> getHeaders(String name) {
        Enumeration<String> headers = super.getHeaders(name);
        while (headers.hasMoreElements()) {
            String header = headers.nextElement();
            String[] tokens = header.split(",");
            for (String token : tokens) {
                stripHeaderXSS(name, token);
            }
        }
        return super.getHeaders(name);
    }

    @Override
    public String getHeader(String name) {
        String header = super.getHeader(name);
        stripHeaderXSS(name, header);
        return header;
    }

    @Override
    public Cookie[] getCookies() {
        Cookie[] cookies = super.getCookies();

        if (cookies == null) {
            return cookies;
        }

        List<Cookie> filteredCookies =
                Arrays.stream(cookies).filter(cookie -> !isThirdPartyCookie(cookie)).toList();

        filteredCookies.forEach(cookie -> doStripXSS(cookie.getValue(), StripOptions.STRICT));

        return filteredCookies.toArray(new Cookie[0]);
    }

    private boolean isThirdPartyCookie(Cookie cookie) {
        String requestDomain = super.getServerName();
        String cookieDomain = cookie.getDomain();

        if (cookieDomain == null) {
            return false;
        }

        return !cookieDomain.equals(requestDomain) && !cookieDomain.endsWith("." + requestDomain);
    }

    private void stripHeaderXSS(String headerName, String value) {
        // Cookies are checked in the getCookies method
        if ("Cookie".equals(headerName)) {
            return;
        }

        // The 'Referer' header can trigger input validation errors (IntrusionException) because its
        // value may contain JSON with escaped characters that are then URL-encoded. This causes the
        // ESAPIEncoder `canonicalize` to detect a mixed encoding in strict mode.
        // As a workaround, we allow mixed encoding for the 'Referer' parameter.
        if (headerName.equals(HEADER_REFERER)) {
            doStripXSS(value, StripOptions.ALLOW_MIXED_ENCODING);
        } else {
            stripXSS(value);
        }
    }

    private void stripXSS(String[] values) {
        Arrays.stream(values).forEach(this::stripXSS);
    }

    private void stripXSS(String value) {
        doStripXSS(value, StripOptions.STRICT);
    }

    private void doStripXSS(String value, StripOptions options) {
        if (value == null) {
            return;
        }

        // Canonicalize the value to remove encoding and potential obfuscation
        final String canonicalValue =
                ESAPIEncoder.getInstance()
                        .canonicalize(value, options.restrictMultiple, options.restrictMixed)
                        .replace("\0", "") // replacing null characters by empty string
                        .trim();

        // Check if the canonical value contains any potential XSS attack
        final boolean valid = Jsoup.isValid(canonicalValue, Safelist.none());

        if (!valid) {
            final String message =
                    "Found a potential XSS attack in value %s. The canonical value is %s"
                            .formatted(value, canonicalValue);
            throw new IntrusionException(USER_FACING_MESSAGE, message);
        }
    }

    private enum StripOptions {
        STRICT(true, true),
        ALLOW_MIXED_ENCODING(true, false);

        final boolean restrictMultiple;
        final boolean restrictMixed;

        StripOptions(boolean restrictMultiple, boolean restrictMixed) {
            this.restrictMultiple = restrictMultiple;
            this.restrictMixed = restrictMixed;
        }
    }
}
