/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.milestone;

import static org.squashtest.tm.api.security.acls.Roles.ROLE_ADMIN;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.milestone.MilestoneRange;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.exception.NameAlreadyInUseException;
import org.squashtest.tm.exception.milestone.MilestoneLabelAlreadyExistsException;
import org.squashtest.tm.service.display.milestone.MilestoneDisplayService;
import org.squashtest.tm.service.internal.display.dto.BindableProjectToMilestoneDto;
import org.squashtest.tm.service.internal.display.dto.MilestoneAdminViewDto;
import org.squashtest.tm.service.internal.display.dto.MilestoneDto;
import org.squashtest.tm.service.internal.display.dto.MilestoneProjectViewDto;
import org.squashtest.tm.service.internal.display.dto.ProjectInfoForMilestoneAdminViewDto;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;
import org.squashtest.tm.service.milestone.CustomMilestoneManager;
import org.squashtest.tm.service.milestone.MilestoneBindingManagerService;
import org.squashtest.tm.service.project.ProjectFinder;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.web.backend.controller.form.model.MilestoneFormModel;

@RestController
@RequestMapping("/backend/milestone-binding")
public class MilestoneBindingController {

    private final CustomMilestoneManager customMilestoneManager;
    private final MilestoneBindingManagerService milestoneBindingManagerService;
    private final PermissionEvaluationService permissionEvaluationService;
    private final ProjectFinder projectFinder;
    private final MilestoneDisplayService milestoneDisplayService;

    @Inject
    MilestoneBindingController(
            CustomMilestoneManager customMilestoneManager,
            MilestoneBindingManagerService milestoneBindingManagerService,
            PermissionEvaluationService permissionEvaluationService,
            ProjectFinder projectFinder,
            MilestoneDisplayService milestoneDisplayService) {
        this.customMilestoneManager = customMilestoneManager;
        this.milestoneBindingManagerService = milestoneBindingManagerService;
        this.permissionEvaluationService = permissionEvaluationService;
        this.projectFinder = projectFinder;
        this.milestoneDisplayService = milestoneDisplayService;
    }

    @PostMapping("/project/{projectId}/bind-milestones/{milestoneIds}")
    public void bindMilestonesToProject(
            @PathVariable long projectId, @PathVariable List<Long> milestoneIds) {
        milestoneBindingManagerService.bindMilestonesToProject(milestoneIds, projectId);
    }

    @DeleteMapping("/project/{projectId}/unbind-milestones/{milestoneIds}")
    public void unbindMilestonesFromProject(
            @PathVariable long projectId, @PathVariable List<Long> milestoneIds) {
        milestoneBindingManagerService.unbindMilestonesFromProject(milestoneIds, projectId);
    }

    @PostMapping("/{milestoneId}/bind-projects/{projectIds}")
    public Map<String, List<ProjectInfoForMilestoneAdminViewDto>> bindProjectsToMilestone(
            @PathVariable long milestoneId, @PathVariable List<Long> projectIds) {
        milestoneBindingManagerService.bindProjectsToMilestone(projectIds, milestoneId);
        MilestoneAdminViewDto milestone = milestoneDisplayService.getMilestoneView(milestoneId);
        return Collections.singletonMap(
                "boundProjectsInformation", milestone.getBoundProjectsInformation());
    }

    @DeleteMapping("/{milestoneId}/unbind-projects/{projectIds}")
    public void unbindProjectsFromMilestone(
            @PathVariable long milestoneId, @PathVariable List<Long> projectIds) {
        milestoneBindingManagerService.unbindProjectsFromMilestone(projectIds, milestoneId);
    }

    @DeleteMapping("/{milestoneId}/unbind-projects-and-keep-in-perimeter/{projectIds}")
    public void unbindProjectFromMilestoneKeepInPerimeter(
            @PathVariable("milestoneId") long milestoneId,
            @PathVariable("projectIds") List<Long> projectIds) {
        milestoneBindingManagerService.unbindProjectsFromMilestoneKeepInPerimeter(
                projectIds, milestoneId);
    }

    @GetMapping("/{milestoneId}/bindable-projects")
    public Map<String, List<BindableProjectToMilestoneDto>> getBindableProjectsForMilestone(
            @PathVariable long milestoneId) {

        List<BindableProjectToMilestoneDto> bindableProjects =
                milestoneBindingManagerService.getAllBindableProjectForMilestone(milestoneId);

        return Collections.singletonMap("bindableProjects", bindableProjects);
    }

    @PostMapping("/project/{projectId}/create-milestone-and-bind-to-project")
    public Map<String, Object> createAndBindMilestoneToProject(
            @PathVariable long projectId, @Valid @RequestBody MilestoneFormModel milestoneFormModel) {
        Map<String, Object> response = new HashMap<>();
        Milestone milestone = createMilestone(milestoneFormModel);
        MilestoneProjectViewDto milestoneProjectViewDto =
                prepareServerResponseAfterCreatingMilestone(milestone);
        milestoneProjectViewDto.setMilestoneBoundToOneObjectOfProject(false);
        response.put("milestone", milestoneProjectViewDto);
        milestoneBindingManagerService.bindNewMilestonesToProject(
                Collections.singletonList(milestone.getId()), projectId);

        return response;
    }

    @PostMapping("/project/{projectId}/create-milestone-and-bind-to-project-and-objects")
    public Map<String, Object> createAndBindMilestoneToProjectAndObjects(
            @PathVariable long projectId, @Valid @RequestBody MilestoneFormModel milestoneFormModel) {
        Map<String, Object> response = new HashMap<>();
        Milestone milestone = createMilestone(milestoneFormModel);
        MilestoneProjectViewDto milestoneProjectViewDto =
                prepareServerResponseAfterCreatingMilestone(milestone);
        milestoneProjectViewDto.setMilestoneBoundToOneObjectOfProject(true);
        response.put("milestone", milestoneProjectViewDto);
        milestoneBindingManagerService.bindNewMilestonesToProjectAndBindObject(
                projectId, Collections.singletonList(milestone.getId()));

        return response;
    }

    private Milestone createMilestone(MilestoneFormModel milestoneFormModel) {
        Milestone milestone = milestoneFormModel.getMilestone();
        setRange(milestone);
        setPerimeter(milestone);
        try {
            customMilestoneManager.addMilestone(milestone);
        } catch (MilestoneLabelAlreadyExistsException ex) {
            throw new NameAlreadyInUseException("Milestone", milestoneFormModel.getLabel(), "label", ex);
        }

        return milestone;
    }

    private MilestoneProjectViewDto prepareServerResponseAfterCreatingMilestone(Milestone milestone) {
        MilestoneProjectViewDto response = new MilestoneProjectViewDto();
        MilestoneDto milestoneDto = new MilestoneDto();
        milestoneDto.setId(milestone.getId());
        milestoneDto.setLabel(milestone.getLabel());
        String sanitizedDescription = HTMLCleanupUtils.htmlToTrimmedText(milestone.getDescription());
        milestoneDto.setDescription(sanitizedDescription);
        milestoneDto.setEndDate(milestone.getEndDate());
        milestoneDto.setOwnerFirstName(milestone.getOwner().getFirstName());
        milestoneDto.setOwnerLastName(milestone.getOwner().getLastName());
        milestoneDto.setOwnerLogin(milestone.getOwner().getLogin());
        milestoneDto.setStatus(milestone.getStatus().name());
        milestoneDto.setRange(milestone.getRange().name());
        response.setMilestone(milestoneDto);
        return response;
    }

    private void setRange(Milestone milestone) {
        if (permissionEvaluationService.hasRole(ROLE_ADMIN)) {
            milestone.setRange(MilestoneRange.GLOBAL);
        } else {
            milestone.setRange(MilestoneRange.RESTRICTED);
        }
    }

    private void setPerimeter(Milestone milestone) {
        if (!permissionEvaluationService.hasRole(ROLE_ADMIN)) {
            List<GenericProject> projects = projectFinder.findAllICanManage();
            milestone.addProjectsToPerimeter(projects);
        }
    }
}
