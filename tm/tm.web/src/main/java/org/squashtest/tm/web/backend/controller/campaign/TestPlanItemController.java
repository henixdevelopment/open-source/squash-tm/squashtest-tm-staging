/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.campaign;

import java.util.List;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.service.campaign.CampaignTestPlanManagerService;
import org.squashtest.tm.service.campaign.IterationTestPlanManagerService;
import org.squashtest.tm.service.campaign.SprintExecutionCreationService;
import org.squashtest.tm.service.campaign.SprintTestPlanItemManagerService;
import org.squashtest.tm.service.display.campaign.TestPlanItemDisplayService;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;

@RestController
@RequestMapping("backend/test-plan-item")
public class TestPlanItemController {

    private final IterationTestPlanManagerService iterationTestPlanManagerService;

    private final CampaignTestPlanManagerService campaignTestPlanManagerService;

    private final SprintTestPlanItemManagerService sprintTestPlanItemManagerService;

    private final SprintExecutionCreationService sprintExecutionCreationService;

    private final TestPlanItemDisplayService testPlanItemDisplayService;

    public TestPlanItemController(
            IterationTestPlanManagerService iterationTestPlanManagerService,
            CampaignTestPlanManagerService campaignTestPlanManagerService,
            SprintTestPlanItemManagerService sprintTestPlanItemManagerService,
            SprintExecutionCreationService sprintExecutionCreationService,
            TestPlanItemDisplayService testPlanItemDisplayService) {
        this.iterationTestPlanManagerService = iterationTestPlanManagerService;
        this.campaignTestPlanManagerService = campaignTestPlanManagerService;
        this.sprintTestPlanItemManagerService = sprintTestPlanItemManagerService;
        this.sprintExecutionCreationService = sprintExecutionCreationService;
        this.testPlanItemDisplayService = testPlanItemDisplayService;
    }

    @PostMapping(value = "iteration/{testPlanItemId}/dataset")
    public Long setDatasetToIterationTestPlanItem(
            @PathVariable long testPlanItemId, @RequestBody DatasetIdRecord patch) {
        iterationTestPlanManagerService.changeDataset(testPlanItemId, patch.datasetId());
        return patch.datasetId();
    }

    @PostMapping(value = "test-suite/{testPlanItemId}/dataset")
    public Long setDatasetToTestSuiteTestPlanItem(
            @PathVariable long testPlanItemId, @RequestBody DatasetIdRecord patch) {
        iterationTestPlanManagerService.changeDataset(testPlanItemId, patch.datasetId());
        return patch.datasetId();
    }

    @PostMapping(value = "campaign/{testPlanItemId}/dataset")
    public Long setDatasetToCampaignTestPlanItem(
            @PathVariable long testPlanItemId, @RequestBody DatasetIdRecord patch) {
        campaignTestPlanManagerService.changeDataset(testPlanItemId, patch.datasetId());
        return patch.datasetId();
    }

    @PostMapping(value = "sprint-req-version/{testPlanItemId}/dataset")
    public Long setDatasetToSprintRequirementTestPlanItem(
            @PathVariable long testPlanItemId, @RequestBody DatasetIdRecord patch) {
        sprintTestPlanItemManagerService.changeDataset(testPlanItemId, patch.datasetId());
        return patch.datasetId();
    }

    @PostMapping(value = "{testPlanItemId}/new-manual-execution")
    public CreateExecutionResponse createExecution(@PathVariable long testPlanItemId) {
        Execution newExecution = sprintExecutionCreationService.createExecution(testPlanItemId);
        return new CreateExecutionResponse(newExecution.getId());
    }

    @PostMapping(value = "{testPlanItemId}/executions")
    public GridResponse getExecutions(
            @PathVariable long testPlanItemId, @RequestBody GridRequest gridRequest) {
        return testPlanItemDisplayService.getTestPlanItemExecutions(testPlanItemId, gridRequest);
    }

    @PostMapping(value = "/{testPlanItemsIds}/assign-user")
    public Long assignUserToIterationTestPlanItem(
            @PathVariable List<Long> testPlanItemsIds, @RequestBody AssigneeRecord patch) {
        iterationTestPlanManagerService.assignUserToTestPlanItems(testPlanItemsIds, patch.assignee);
        return patch.assignee();
    }

    @PostMapping("/{testPlanItemsId}/assign-user-to-sprint-req-version")
    public Long assignUserToSprintReqVersionTestPlanItem(
            @PathVariable Long testPlanItemsId, @RequestBody AssigneeRecord patch) {
        sprintTestPlanItemManagerService.assignUserToTestPlanItem(testPlanItemsId, patch.assignee());
        return patch.assignee();
    }

    public record AssigneeRecord(Long assignee) {}

    @PostMapping(value = "/{testPlanItemsIds}/execution-status")
    public void updateIterationTestPlanItemsStatus(
            @PathVariable List<Long> testPlanItemsIds,
            @RequestBody IterationTestPlanManagerController.IterationTestPlanItemPatch patch) {
        iterationTestPlanManagerService.forceExecutionStatus(
                testPlanItemsIds, patch.getExecutionStatus());
    }

    // TODO: rename endpoint or merge with other
    @PostMapping(value = "/{testPlanItemIds}/status")
    public void updateTestPlanItemsStatus(
            @PathVariable List<Long> testPlanItemIds, @RequestBody ExecutionStatusRecord requestBody) {
        sprintTestPlanItemManagerService.applyFastPass(testPlanItemIds, requestBody.executionStatus());
    }

    @PostMapping(value = "/{testPlanItemsIds}/assign-user-to-ctpi")
    public Long assignUserToCampaignTestPlanItem(
            @PathVariable List<Long> testPlanItemsIds, @RequestBody AssigneeRecord patch) {
        campaignTestPlanManagerService.assignUserToTestPlanItems(testPlanItemsIds, patch.assignee);
        return patch.assignee;
    }

    public record ExecutionStatusRecord(String executionStatus) {}

    public record DatasetIdRecord(Long datasetId) {}

    public record CreateExecutionResponse(Long executionId) {}

    @DeleteMapping("/{testPlanItemId}/execution/{executionIds}")
    public void deleteExecutions(
            @PathVariable() long testPlanItemId, @PathVariable() List<Long> executionIds) {
        sprintTestPlanItemManagerService.deleteExecutionsAndRemoveFromTestPlanItem(
                executionIds, testPlanItemId);
    }
}
