/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.form.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import org.squashtest.tm.domain.infolist.InfoList;
import org.squashtest.tm.domain.infolist.InfoListItem;
import org.squashtest.tm.domain.infolist.UserListItem;

public class InfoListFormModel {

    private String label;
    private String code;
    private String description;
    private List<InfoListItemFormModel> items;

    public InfoList getInfoList() {
        InfoList infoList = new InfoList();

        infoList.setLabel(label);
        infoList.setCode(code);
        infoList.setDescription(description);

        for (InfoListItemFormModel item : items) {
            InfoListItem infoListItem = new UserListItem();

            infoListItem.setCode(item.getCode());
            infoListItem.setColour(item.getColour());
            infoListItem.setIconName(item.getIconName());
            infoListItem.setDefault(item.isDefault());
            infoListItem.setLabel(item.getLabel());

            infoList.addItem(infoListItem);
        }

        return infoList;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<InfoListItemFormModel> getItems() {
        return items;
    }

    public void setItems(List<InfoListItemFormModel> items) {
        this.items = items;
    }

    public static class InfoListItemFormModel {
        String code;
        String colour;
        String iconName;
        boolean isDefault;
        String label;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getIconName() {
            return iconName;
        }

        public void setIconName(String iconName) {
            this.iconName = iconName;
        }

        public String getColour() {
            return colour;
        }

        public void setColour(String colour) {
            this.colour = colour;
        }

        @JsonProperty("isDefault")
        public boolean isDefault() {
            return isDefault;
        }

        public void setDefault(boolean aDefault) {
            isDefault = aDefault;
        }
    }
}
