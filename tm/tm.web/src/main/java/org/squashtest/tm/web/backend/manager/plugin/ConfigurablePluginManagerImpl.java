/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.manager.plugin;

import static org.squashtest.tm.service.security.Authorizations.MANAGE_PROJECT_OR_ROLE_ADMIN;

import com.google.common.collect.Iterables;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.plugin.ConfigurablePlugin;
import org.squashtest.tm.api.plugin.EntityReference;
import org.squashtest.tm.api.plugin.EntityType;
import org.squashtest.tm.api.plugin.PluginValidationException;
import org.squashtest.tm.api.template.TemplateConfigurablePlugin;
import org.squashtest.tm.api.workspace.WorkspaceType;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.library.LibraryNode;
import org.squashtest.tm.domain.library.PluginReferencer;
import org.squashtest.tm.domain.project.GenericLibrary;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.project.LibraryPluginBinding;
import org.squashtest.tm.service.internal.display.dto.ProjectPluginDto;
import org.squashtest.tm.service.project.GenericProjectFinder;
import org.squashtest.tm.service.project.GenericProjectManagerService;
import org.squashtest.tm.service.templateplugin.TemplateConfigurablePluginBindingService;

@Service
@Transactional
public class ConfigurablePluginManagerImpl implements ConfigurablePluginManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurablePluginManagerImpl.class);

    private final GenericProjectFinder projectFinder;
    private final GenericProjectManagerService projectManager;
    private final TemplateConfigurablePluginBindingService templateConfigurablePluginBindingService;

    @Autowired(required = false)
    private final Collection<ConfigurablePlugin> configurablePlugins = Collections.emptyList();

    @Autowired(required = false)
    private final Collection<TemplateConfigurablePlugin> templatePlugins = Collections.emptyList();

    public ConfigurablePluginManagerImpl(
            GenericProjectFinder projectFinder,
            GenericProjectManagerService projectManager,
            TemplateConfigurablePluginBindingService templateConfigurablePluginBindingService) {
        this.projectFinder = projectFinder;
        this.projectManager = projectManager;
        this.templateConfigurablePluginBindingService = templateConfigurablePluginBindingService;
    }

    @Override
    public boolean isPluginBindingValid(String pluginId, long projectId) {
        final ConfigurablePlugin configurablePlugin = findByIdOrThrow(pluginId);
        final EntityReference context = new EntityReference(EntityType.PROJECT, projectId);
        return isPluginValid(configurablePlugin, context);
    }

    @Override
    public List<ProjectPluginDto> getAvailablePlugins(long projectId) {
        final boolean isTemplate = projectManager.isProjectTemplate(projectId);
        return toPluginDtos(projectId, getBindings(projectId), isTemplate);
    }

    private List<ProjectPluginDto> toPluginDtos(
            long projectId, Collection<ConfigurablePluginBinding> pluginBindings, boolean isTemplate) {
        return IntStream.range(0, pluginBindings.size())
                .mapToObj(
                        index ->
                                toPluginDto(projectId, Iterables.get(pluginBindings, index), index + 1, isTemplate))
                .toList();
    }

    private ProjectPluginDto toPluginDto(
            Long projectId,
            ConfigurablePluginBinding pluginBinding,
            int pluginIndex,
            boolean isTemplate) {
        final EntityReference context = new EntityReference(EntityType.PROJECT, projectId);

        final ProjectPluginDto projectPluginDto = new ProjectPluginDto(pluginBinding.getPlugin());
        projectPluginDto.setIndex(pluginIndex);
        projectPluginDto.setEnabled(pluginBinding.isActive());

        final boolean hasValidConfiguration =
                pluginBinding.isHasConfiguration() && isPluginValid(pluginBinding.getPlugin(), context);
        projectPluginDto.setHasValidConfiguration(hasValidConfiguration);

        updateConfigUrl(pluginBinding.getPlugin(), isTemplate, context, projectPluginDto);

        return projectPluginDto;
    }

    private void updateConfigUrl(
            ConfigurablePlugin plugin,
            boolean isTemplate,
            EntityReference context,
            ProjectPluginDto projectPluginDto) {
        if (isTemplate) {
            final Optional<TemplateConfigurablePlugin> templatePlugin =
                    findTemplatePlugin(plugin.getId());
            templatePlugin.ifPresent(
                    templateConfigurablePlugin ->
                            projectPluginDto.setConfigUrl(
                                    templateConfigurablePlugin.getTemplateConfigurationPath(context)));
        } else {
            projectPluginDto.setConfigUrl(plugin.getConfigurationPath(context));
        }
    }

    private Optional<TemplateConfigurablePlugin> findTemplatePlugin(String pluginId) {
        return templatePlugins.stream().filter(tp -> tp.getId().equals(pluginId)).findFirst();
    }

    private boolean isPluginValid(ConfigurablePlugin plugin, EntityReference context) {
        try {
            plugin.validate(context);
            return true;
        } catch (PluginValidationException ex) {
            LOGGER.debug("Plugin validation failed for plugin '{}'", plugin.getName(), ex);
            return false;
        }
    }

    public Collection<ConfigurablePluginBinding> getBindings(long projectId) {
        return configurablePlugins.stream()
                .map(
                        plugin ->
                                new ConfigurablePluginBinding(
                                        plugin,
                                        projectId,
                                        isActive(plugin, projectId),
                                        hasConfiguration(plugin, projectId)))
                .toList();
    }

    @Override
    public Optional<ConfigurablePlugin> findById(String pluginId) {
        return configurablePlugins.stream()
                .filter(plugin -> plugin.getId().equals(pluginId))
                .findFirst();
    }

    private ConfigurablePlugin findByIdOrThrow(String pluginId) {
        return findById(pluginId)
                .orElseThrow(
                        () -> new IllegalArgumentException("Could not find plugin with ID " + pluginId));
    }

    private boolean isActive(ConfigurablePlugin plugin, long projectId) {
        PluginReferencer<?> library = findLibrary(projectId, plugin.getConfiguringWorkspace());
        LibraryPluginBinding binding = library.getPluginBinding(plugin.getId());
        return binding != null && binding.getActive();
    }

    private boolean hasConfiguration(ConfigurablePlugin plugin, long projectId) {
        return projectManager.pluginHasConfigurationOrSynchronisations(plugin, projectId);
    }

    private GenericLibrary<? extends LibraryNode> findLibrary(
            long projectId, WorkspaceType workspace) {
        GenericProject project = projectFinder.findById(projectId);

        return switch (workspace) {
            case TEST_CASE_WORKSPACE -> project.getTestCaseLibrary();
            case REQUIREMENT_WORKSPACE -> project.getRequirementLibrary();
            case CAMPAIGN_WORKSPACE -> project.getCampaignLibrary();
            default ->
                    throw new IllegalArgumentException(
                            "WorkspaceType " + workspace + " is unknown and not covered by this class");
        };
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public void disableConfigurablePlugin(String pluginId, long projectId, Boolean saveConf) {
        final ConfigurablePlugin plugin = findByIdOrThrow(pluginId);
        final List<WorkspaceType> workspaceTypes = plugin.getWorkspaces();

        if (Boolean.TRUE.equals(saveConf)) {
            projectManager.disablePluginAndKeepConfiguration(projectId, workspaceTypes, plugin.getId());
            projectManager.disableSynchronisations(projectId, pluginId);
        } else {
            // TODO: remove ref to plugin
            if (pluginId.contains("workflow.automjira")) {
                projectManager.deleteAllRemoteAutomationRequestExtenders(projectId);
            }

            boolean isProjectBound =
                    templateConfigurablePluginBindingService.isProjectConfigurationBoundToTemplate(projectId);
            boolean isProjectTemplate = projectManager.isProjectTemplate(projectId);

            if (isProjectTemplate) {
                projectManager.disablePluginAndRemoveConfiguration(projectId, workspaceTypes, pluginId);
                projectManager.synchronizeBoundPluginConfigurations(projectId, pluginId);
            } else if (isProjectBound) {
                projectManager.disablePluginAndKeepConfiguration(projectId, workspaceTypes, pluginId);
                projectManager.removeSynchronisations(projectId, pluginId);
            } else {
                projectManager.disablePluginAndRemoveConfiguration(projectId, workspaceTypes, pluginId);
                projectManager.removeSynchronisations(projectId, pluginId);
            }
        }
    }
}
