/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.artificialintelligence.server;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.aiserver.AiServer;
import org.squashtest.tm.service.artificialintelligence.server.AiServerManagerService;
import org.squashtest.tm.service.display.artificialintelligence.server.AiServerDisplayService;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.web.backend.controller.utils.UrlValidator;

@RestController
@RequestMapping("/backend/ai-servers")
public class AiServerDisplayController {

    private final AiServerDisplayService aiServerDisplayService;
    private final AiServerManagerService aiServerManagerService;

    public AiServerDisplayController(
            AiServerDisplayService aiServerDisplayService,
            AiServerManagerService aiServerManagerService) {
        this.aiServerDisplayService = aiServerDisplayService;
        this.aiServerManagerService = aiServerManagerService;
    }

    @PostMapping
    public GridResponse getAllAiServers(@RequestBody GridRequest request) {
        return aiServerDisplayService.findAll(request);
    }

    @PostMapping("/new")
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String, Object> createNewAiServer(@RequestBody AiServerFormModel formModel) {
        UrlValidator.checkURL(formModel.baseUrl());
        AiServer server = new AiServer(formModel.name(), formModel.baseUrl(), formModel.description());
        server.setDescription(formModel.description());
        aiServerManagerService.persist(server);
        return Collections.singletonMap("id", server.getId());
    }

    @DeleteMapping("/{aiServerIds}")
    public void deleteServers(@PathVariable List<Long> aiServerIds) {
        aiServerManagerService.deleteAiServers(aiServerIds);
    }

    record AiServerFormModel(String name, String baseUrl, String description) {}
}
