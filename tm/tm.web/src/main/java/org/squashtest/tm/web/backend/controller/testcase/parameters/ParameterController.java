/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.testcase.parameters;

import java.util.HashMap;
import java.util.Map;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.exception.DuplicateNameException;
import org.squashtest.tm.exception.NameAlreadyInUseException;
import org.squashtest.tm.service.display.testcase.TestCaseDisplayService;
import org.squashtest.tm.service.internal.display.testcase.parameter.TestCaseParameterRenameOperationReport;
import org.squashtest.tm.service.testcase.ParameterFinder;
import org.squashtest.tm.service.testcase.ParameterModificationService;

@RestController
@RequestMapping(value = "backend/parameters")
public class ParameterController {
    private static final String PARAMETER_ID_URL = "/{parameterId}";
    private static final String PARAMETER_USED = "PARAMETER_USED";

    private final ParameterModificationService parameterService;

    private final TestCaseDisplayService testCaseDisplayService;

    private final ParameterFinder parameterFinder;

    public ParameterController(
            ParameterModificationService parameterService,
            TestCaseDisplayService testCaseDisplayService,
            ParameterFinder parameterFinder) {
        this.parameterService = parameterService;
        this.testCaseDisplayService = testCaseDisplayService;
        this.parameterFinder = parameterFinder;
    }

    @GetMapping(value = PARAMETER_ID_URL + "/used")
    public Map<String, Boolean> isUsedParameter(@PathVariable Long parameterId) {
        Map<String, Boolean> used = new HashMap<>();
        used.put(PARAMETER_USED, parameterService.isUsed(parameterId));
        return used;
    }

    @DeleteMapping(value = PARAMETER_ID_URL)
    public void removeParameter(@PathVariable Long parameterId) {
        parameterService.removeById(parameterId);
    }

    @PostMapping(value = PARAMETER_ID_URL + "/rename")
    public TestCaseParameterRenameOperationReport renameParameter(
            @PathVariable Long parameterId, @RequestBody ParameterPatch patch) {
        try {
            parameterService.changeName(parameterId, patch.getName());
            Parameter parameter = parameterFinder.findById(parameterId);
            return new TestCaseParameterRenameOperationReport(
                    testCaseDisplayService.findTestStepsByTestCaseId(parameter.getTestCase().getId()),
                    parameter.getTestCase().getPrerequisite());
        } catch (DuplicateNameException ex) {
            throw new NameAlreadyInUseException("Parameter", patch.getName(), ex);
        }
    }

    @PostMapping(value = PARAMETER_ID_URL + "/description")
    public void changeDescription(@PathVariable Long parameterId, @RequestBody ParameterPatch patch) {
        parameterService.changeDescription(parameterId, patch.getDescription());
    }
}
