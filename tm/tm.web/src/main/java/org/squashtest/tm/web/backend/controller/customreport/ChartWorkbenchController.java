/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.customreport;

import javax.validation.Valid;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.chart.ChartDefinition;
import org.squashtest.tm.domain.chart.ChartInstance;
import org.squashtest.tm.domain.customreport.CustomReportLibraryNode;
import org.squashtest.tm.service.chart.ChartModificationService;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;
import org.squashtest.tm.service.internal.display.dto.WorkbenchData;
import org.squashtest.tm.service.internal.dto.json.JsonChartInstance;
import org.squashtest.tm.web.backend.controller.form.model.CreatedEntityId;

@RestController
@RequestMapping("/backend/chart-workbench")
public class ChartWorkbenchController {

    private final CustomReportLibraryNodeService customReportLibraryNodeService;
    private final ChartModificationService chartService;

    public ChartWorkbenchController(
            CustomReportLibraryNodeService customReportLibraryNodeService,
            ChartModificationService chartService) {
        this.customReportLibraryNodeService = customReportLibraryNodeService;
        this.chartService = chartService;
    }

    @GetMapping("/{customReportLibraryNodeId}")
    public WorkbenchData getWizardData(@PathVariable Long customReportLibraryNodeId) {
        return this.chartService.getWorkbenchData(customReportLibraryNodeId);
    }

    @PostMapping(value = "/preview")
    public JsonChartInstance generate(@RequestBody @Valid ChartDefinition definition) {
        ChartInstance instance = chartService.generateChart(definition, null, null);
        return new JsonChartInstance(instance);
    }

    // as above but require a project id. We need it for squash tm 1.15 because now the default
    // perimeter is the current
    // project of chart, so for the preview, the definition has no project, we need to pass one...
    @PostMapping(value = "/preview/{projectId}")
    public JsonChartInstance generateWithoutProject(
            @PathVariable("projectId") Long projectId, @RequestBody ChartDefinition definition) {
        ChartInstance instance = chartService.generateChart(definition, projectId);
        return new JsonChartInstance(instance);
    }

    @PostMapping("/new/{id}")
    public CreatedEntityId createNewChartDefinition(
            @RequestBody @Valid ChartDefinition definition, @PathVariable("id") long id) {
        CustomReportLibraryNode node = customReportLibraryNodeService.createNewNode(id, definition);
        return new CreatedEntityId(node.getId());
    }

    @PostMapping("/update/{id}")
    public void updateChartDefinition(
            @RequestBody @Valid ChartDefinition definition, @PathVariable("id") long id) {
        chartService.updateDefinition(definition, id);
    }
}
