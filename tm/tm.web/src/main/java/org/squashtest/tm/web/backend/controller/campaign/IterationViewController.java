/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.campaign;

import java.util.Collections;
import java.util.Locale;
import javax.inject.Named;
import javax.inject.Provider;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.Workspace;
import org.squashtest.tm.domain.customreport.CustomReportDashboard;
import org.squashtest.tm.service.campaign.IterationModificationService;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;
import org.squashtest.tm.service.display.campaign.IterationDisplayService;
import org.squashtest.tm.service.internal.display.dto.campaign.IterationDto;
import org.squashtest.tm.service.internal.display.dto.customreports.CustomReportDashboardDto;
import org.squashtest.tm.service.internal.dto.json.JsonCustomReportDashboard;
import org.squashtest.tm.service.statistics.campaign.StatisticsBundle;
import org.squashtest.tm.web.backend.model.builder.JsonCustomReportDashboardBuilder;

@RestController
@RequestMapping("backend/iteration-view")
public class IterationViewController {

    private final IterationDisplayService iterationDisplayService;
    private final IterationModificationService iterationModificationService;
    private final CustomReportLibraryNodeService customReportLibraryNodeService;

    @Named("customReport.dashboardBuilder")
    private final Provider<JsonCustomReportDashboardBuilder> builderProvider;

    public IterationViewController(
            IterationDisplayService iterationDisplayService,
            IterationModificationService iterationModificationService,
            CustomReportLibraryNodeService customReportLibraryNodeService,
            Provider<JsonCustomReportDashboardBuilder> builderProvider) {
        this.iterationDisplayService = iterationDisplayService;
        this.iterationModificationService = iterationModificationService;
        this.customReportLibraryNodeService = customReportLibraryNodeService;
        this.builderProvider = builderProvider;
    }

    @GetMapping(value = "/{iterationId}")
    public IterationDto getIterationView(@PathVariable Long iterationId, Locale locale) {
        IterationDto dto = this.iterationDisplayService.findIterationView(iterationId);
        if (dto.isShouldShowFavoriteDashboard() && dto.isCanShowFavoriteDashboard()) {
            EntityReference library = new EntityReference(EntityType.ITERATION, iterationId);
            CustomReportDashboard dashboard =
                    customReportLibraryNodeService.findCustomReportDashboardById(
                            dto.getFavoriteDashboardId());
            CustomReportDashboardDto dashboardScopeDto =
                    new CustomReportDashboardDto(
                            Workspace.CAMPAIGN, Collections.singletonList(library), false, false, true);
            JsonCustomReportDashboard jsonDashboard =
                    builderProvider
                            .get()
                            .build(dto.getFavoriteDashboardId(), dashboard, locale, dashboardScopeDto);
            dto.setDashboard(jsonDashboard);
        }
        return dto;
    }

    @PostMapping(value = "/{iterationId}/statistics")
    public StatisticsBundle getIterationStatistics(
            @PathVariable long iterationId,
            @RequestBody CampaignViewController.StatisticsScopePatch patch) {
        return iterationModificationService.gatherIterationStatisticsBundle(
                iterationId, patch.lastExecutionScope());
    }
}
