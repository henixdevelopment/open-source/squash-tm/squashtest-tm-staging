/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.customexport;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.customreport.CustomExportColumnLabel;
import org.squashtest.tm.domain.customreport.CustomReportCustomExport;
import org.squashtest.tm.domain.customreport.CustomReportCustomExportColumn;
import org.squashtest.tm.domain.customreport.CustomReportLibraryNode;
import org.squashtest.tm.service.customfield.CustomFieldFinderService;
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService;
import org.squashtest.tm.service.customreport.CustomReportCustomExportCSVService;
import org.squashtest.tm.service.customreport.CustomReportCustomExportModificationService;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;
import org.squashtest.tm.service.internal.display.dto.customreports.CustomExportWorkbenchData;
import org.squashtest.tm.service.internal.display.dto.customreports.CustomFieldCustomExportDto;
import org.squashtest.tm.service.internal.repository.ExecutionStepDao;
import org.squashtest.tm.web.backend.controller.RequestParams;
import org.squashtest.tm.web.i18n.InternationalizationHelper;

@RestController
@RequestMapping("/backend/custom-exports")
public class CustomExportController {

    private final CustomReportCustomExportModificationService customExportModificationService;
    private final CustomReportLibraryNodeService customReportLibraryNodeService;

    // CSV helper dependencies (why not delegate to a service handling its own dependencies though ?)
    private final CustomReportCustomExportCSVService csvExportService;
    private final CustomFieldFinderService cufService;
    private final CustomFieldValueFinderService cufValueService;
    private final ExecutionStepDao executionStepDao;
    private final InternationalizationHelper i18nHelper;

    public CustomExportController(
            CustomReportCustomExportModificationService customExportModificationService,
            CustomReportLibraryNodeService customReportLibraryNodeService,
            CustomReportCustomExportCSVService csvExportService,
            CustomFieldFinderService cufService,
            CustomFieldValueFinderService cufValueService,
            ExecutionStepDao executionStepDao,
            InternationalizationHelper i18nHelper) {
        this.customExportModificationService = customExportModificationService;
        this.customReportLibraryNodeService = customReportLibraryNodeService;
        this.csvExportService = csvExportService;
        this.cufService = cufService;
        this.cufValueService = cufValueService;
        this.executionStepDao = executionStepDao;
        this.i18nHelper = i18nHelper;
    }

    @GetMapping("/workbench/{parentId}")
    public CustomExportWorkbenchData getWorkbenchData(@PathVariable Long parentId) {
        return customExportModificationService.getWorkbenchData(parentId);
    }

    @PostMapping("/new/{parentNodeId}")
    public Map<String, Long> createNewCustomExport(
            @RequestBody NewCustomExport newCustomExport, @PathVariable long parentNodeId) {
        CustomReportLibraryNode newNode =
                customReportLibraryNodeService.createNewNode(parentNodeId, newCustomExport.asEntity());
        return Collections.singletonMap("id", newNode.getId());
    }

    @PostMapping("/update/{nodeId}")
    public Map<String, Long> updateCustomExport(
            @RequestBody NewCustomExport modifiedCustomExport, @PathVariable long nodeId) {
        customExportModificationService.updateCustomExport(nodeId, modifiedCustomExport.asEntity());
        return Collections.singletonMap("id", nodeId);
    }

    @PostMapping("/available-cuf-columns")
    public List<CustomFieldCustomExportDto> getAvailableCustomFieldExportData(
            @RequestBody() List<Long> projectIds) {
        return cufValueService.findAllAvailableCufsForCustomExport(projectIds);
    }

    public static class NewCustomExportColumn {
        private Long cufId;
        private String label;

        public Long getCufId() {
            return cufId;
        }

        public void setCufId(Long cufId) {
            this.cufId = cufId;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }
    }

    public static class NewCustomExport {
        private String name;
        private List<NewCustomExportColumn> columns;
        private String scope;

        public CustomReportCustomExport asEntity() {
            CustomReportCustomExport exportEntity = new CustomReportCustomExport();

            exportEntity.setName(getName());
            exportEntity.setScope(getExportEntitiesScope());
            exportEntity.setColumns(
                    columns.stream()
                            .map(
                                    c -> {
                                        CustomReportCustomExportColumn columnEntity =
                                                new CustomReportCustomExportColumn();
                                        columnEntity.setLabel(CustomExportColumnLabel.valueOf(c.getLabel()));
                                        columnEntity.setCufId(c.getCufId());
                                        return columnEntity;
                                    })
                            .toList());

            return exportEntity;
        }

        private List<EntityReference> getExportEntitiesScope() {
            String[] ids = getScope().split(",");
            List<EntityReference> entities = new ArrayList<>();
            for (String id : ids) {
                entities.add(EntityReference.fromNodeId(id));
            }
            return entities;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<NewCustomExportColumn> getColumns() {
            return columns;
        }

        public void setColumns(List<NewCustomExportColumn> columns) {
            this.columns = columns;
        }

        public String getScope() {
            return scope;
        }

        public void setScope(String scope) {
            this.scope = scope;
        }
    }

    @GetMapping(
            value = "/generate-with-last-exec/{customExportNodeId}",
            produces = RequestParams.APPLICATION_SLASH_OCTET_STREAM)
    public FileSystemResource generateCustomExportWithLastExecution(
            @PathVariable long customExportNodeId, HttpServletResponse response, Locale locale) {
        return getFileSystemResource(customExportNodeId, response, locale, true);
    }

    @GetMapping(
            value = "/generate/{customExportNodeId}",
            produces = RequestParams.APPLICATION_SLASH_OCTET_STREAM)
    public FileSystemResource generateCustomExport(
            @PathVariable long customExportNodeId, HttpServletResponse response, Locale locale) {
        return getFileSystemResource(customExportNodeId, response, locale, false);
    }

    private FileSystemResource getFileSystemResource(
            long customExportNodeId, HttpServletResponse response, Locale locale, boolean printLastExec) {
        CustomReportCustomExport customExport =
                customReportLibraryNodeService.findCustomExportByNodeId(customExportNodeId);

        // prepare the response
        response.setContentType("application/octet-stream");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");

        response.setHeader(
                "Content-Disposition",
                "attachment; filename="
                        + "EXPORT_"
                        + customExport.getName().replace(" ", "_")
                        + "_"
                        + sdf.format(new Date())
                        + ".csv");

        File exported = createCustomExportFile(customExport, locale, printLastExec);
        return new FileSystemResource(exported);
    }

    private File createCustomExportFile(
            CustomReportCustomExport customExport, Locale locale, boolean printLastExec) {
        File file;
        PrintWriter writer = null;
        CustomExportCSVHelper csvHelper =
                new CustomExportCSVHelper(
                        csvExportService, cufService, cufValueService, i18nHelper, locale, executionStepDao);

        try {
            file = File.createTempFile("custom-export", "tmp");
            file.deleteOnExit();
            writer = new PrintWriter(file);
            // print headers
            writer.write(csvHelper.getInternationalizedHeaders(customExport));
            // print the data
            writer.write(csvHelper.getWritableRowsData(customExport, printLastExec));
            writer.close();
            return file;
        } catch (IOException ioEx) {
            throw new RuntimeException(ioEx);
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }
}
