/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.export.grid;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class CsvGridExporter extends AbstractGridExporter {

    public CsvGridExporter(GridExportModel gridExportModel) {
        super(gridExportModel);
    }

    @Override
    public File export() throws IOException {
        File file;
        PrintWriter writer;

        file = File.createTempFile("grid_export_", ".csv");
        file.deleteOnExit();
        writer = new PrintWriter(file);

        printHeaders();
        printRows();

        for (Row row : rows) {
            writer.write(rowToString(row) + "\n");
        }

        writer.close();
        return file;
    }

    private String rowToString(Row row) {
        StringBuilder builder = new StringBuilder();
        final String SEPARATOR = ";";

        Iterator<Cell> headerIterator = row.cellIterator();
        while (headerIterator.hasNext()) {
            Cell cell = headerIterator.next();
            String value = escapeCharIfNeeded(cell.getStringCellValue());
            if (value != null) {
                builder.append("\"").append(value).append("\"").append(SEPARATOR);
            }
        }
        return builder.toString().replaceAll(SEPARATOR + "$", "");
    }

    private String escapeCharIfNeeded(String value) {
        if (value.contains("\"")) {
            return value.replace("\"", "\"\"");
        }
        return value;
    }
}
