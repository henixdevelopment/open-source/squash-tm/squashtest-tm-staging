/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.tf;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.service.testautomation.AutomatedSuiteManagerService;
import org.squashtest.tm.service.testautomation.AutomationDeletionCount;

@RestController
@RequestMapping("/backend/cleaning")
public class AutomatedSuitesCleaningController {

    public static final Logger LOGGER =
            LoggerFactory.getLogger(AutomatedSuitesCleaningController.class);
    private final AutomatedSuiteManagerService automatedSuiteManagerService;

    public AutomatedSuitesCleaningController(
            AutomatedSuiteManagerService automatedSuiteManagerService) {
        this.automatedSuiteManagerService = automatedSuiteManagerService;
    }

    @GetMapping("/count")
    public AutomationDeletionCount getOldAutomatedSuitesAndExecutionsCount() {
        return automatedSuiteManagerService.countOldAutomatedSuitesAndExecutions();
    }

    @GetMapping("/{projectId}/count")
    public AutomationDeletionCount getOldAutomatedSuitesAndExecutionsCountForProject(
            @PathVariable Long projectId) {
        return automatedSuiteManagerService.countOldAutomatedSuitesAndExecutionsForProject(projectId);
    }

    @PostMapping()
    public void cleanAutomatedSuitesAndExecutions() {
        automatedSuiteManagerService.cleanOldSuites();
    }

    @PostMapping("/{projectId}")
    public void cleanAutomatedSuitesAndExecutionsForProject(@PathVariable Long projectId) {
        automatedSuiteManagerService.cleanOldSuitesForProject(projectId);
    }

    @PostMapping(
            value = "/{projectId}/prune",
            params = {"complete"})
    public void pruneAttachments(
            @PathVariable Long projectId, @RequestParam("complete") boolean complete) {
        automatedSuiteManagerService.pruneAttachments(projectId, complete);
    }
}
