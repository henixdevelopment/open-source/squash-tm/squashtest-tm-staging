/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.config;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.squashtest.tm.api.security.authentication.SecurityExemptionEndPoint;
import org.squashtest.tm.web.security.authentication.SinglePageAppAuthenticationFailureHandler;
import org.squashtest.tm.web.security.authentication.SinglePageAppAuthenticationSuccessHandler;

/**
 * This configures Spring Security
 *
 * <p>#configure(AuthenticationManagerBuilder) should not be overriden ! When it is overriden, it
 * supersedes any "global" AuthenticationManager. This means any third party authentication provider
 * will be ignored.
 *
 * @author Gregory Fouquet
 * @since 1.13.0
 */
@Configuration
public class WebSecurityConfig {

    private static final String ALTERNATE_AUTH_PATH = "/auth/**";
    private static final String LOGIN = "/login";
    private static final String LOGOUT = "/logout";
    private static final String ROOT_PATH = "/";
    private static final String CONTROLLERS_ROOT_URL = "/backend";
    private static final String[] ADMIN_ONLY_URLS = {
        CONTROLLERS_ROOT_URL + "/ai-servers/**",
        CONTROLLERS_ROOT_URL + "/bugtracker/**",
        CONTROLLERS_ROOT_URL + "/bugtrackers/**",
        CONTROLLERS_ROOT_URL + "/custom-field-view/**",
        CONTROLLERS_ROOT_URL + "/environment-variable-view/**",
        CONTROLLERS_ROOT_URL + "/info-list-items/**",
        CONTROLLERS_ROOT_URL + "/info-list-view/**",
        CONTROLLERS_ROOT_URL + "/info-lists/**",
        CONTROLLERS_ROOT_URL + "/profile-view/**",
        CONTROLLERS_ROOT_URL + "/project-templates/**",
        CONTROLLERS_ROOT_URL + "/projects/**",
        CONTROLLERS_ROOT_URL + "/requirement-link-type/**",
        CONTROLLERS_ROOT_URL + "/requirements-links/**",
        CONTROLLERS_ROOT_URL + "/scm-server-view/**",
        CONTROLLERS_ROOT_URL + "/scm-servers/**",
        CONTROLLERS_ROOT_URL + "/system/**",
        CONTROLLERS_ROOT_URL + "/team-view/**",
        CONTROLLERS_ROOT_URL + "/teams/**",
        CONTROLLERS_ROOT_URL + "/test-automation-servers/**",
        CONTROLLERS_ROOT_URL + "/user-view/**",
        CONTROLLERS_ROOT_URL + "/users/**",
        CONTROLLERS_ROOT_URL + "/users/**",
    };

    /* *********************************************************
     *
     *  Enpoint-specific security filter chains
     *
     * *********************************************************/

    @Configuration
    public static class SquashTAWebSecurityConfigurationAdapter {

        /** part of the fix for [Issue #6900] */
        @Value("${squash.security.basic.token-charset}")
        private String basicAuthCharset = "ISO-8859-1";

        @Bean
        public AuthenticationManager authenticationManagerBean(
                AuthenticationConfiguration authenticationConfiguration) throws Exception {
            return authenticationConfiguration.getAuthenticationManager();
        }

        @Bean
        @Order(10)
        protected SecurityFilterChain squashTaFilterChain(HttpSecurity http) throws Exception {
            http.csrf(AbstractHttpConfigurer::disable)
                    .antMatcher("/automated-executions/**")
                    .authorizeHttpRequests(authz -> authz.anyRequest().hasRole("TA_API_CLIENT"))
                    .httpBasic()
                    .withObjectPostProcessor(new BasicAuthCharsetConfigurer(basicAuthCharset));

            return http.build();
        }
    }

    @Configuration
    public static class ApiWebSecurityConfigurationAdapter {
        /** part of the fix for [Issue #6900] */
        @Value("${squash.security.basic.token-charset}")
        private String basicAuthCharset = "ISO-8859-1";

        @Bean
        public JwtTokenFilter apiJwtTokenFilter() {
            return new JwtTokenFilter();
        }

        @Bean
        CustomRestApiBasicAuthFilter customRestApiBasicAuthFilter() {
            return new CustomRestApiBasicAuthFilter();
        }

        @Bean
        @Order(20)
        protected SecurityFilterChain apiFilterChain(HttpSecurity http) throws Exception {
            http.csrf(AbstractHttpConfigurer::disable)
                    .antMatcher("/api/**")
                    .authorizeHttpRequests(authz -> authz.anyRequest().authenticated())
                    .httpBasic()
                    .withObjectPostProcessor(new BasicAuthCharsetConfigurer(basicAuthCharset))
                    .realmName("squash-api")
                    .authenticationEntryPoint(
                            new AuthenticationEntryPoint() {

                                @Override
                                public void commence(
                                        HttpServletRequest request,
                                        HttpServletResponse response,
                                        AuthenticationException authException)
                                        throws IOException {
                                    // TODO Auto-generated method stub

                                    response.addHeader("WWW-Authenticate", "Basic realm=\"squah-api\"");
                                    response.addHeader("Content-Type", "application/json");
                                    response.sendError(
                                            HttpServletResponse.SC_UNAUTHORIZED,
                                            authException.getMessage()
                                                    + ". You may authenticate using "
                                                    + "1/ basic authentication or "
                                                    + "2/ fetching a cookie JSESSIONID from /login");
                                }
                            })
                    .and()
                    .logout()
                    .permitAll()
                    .logoutRequestMatcher(new AntPathRequestMatcher(LOGOUT))
                    .invalidateHttpSession(true)
                    .logoutSuccessUrl("/");

            // Request must fail if basic authentication is not allowed on the instance
            http.addFilterBefore(customRestApiBasicAuthFilter(), BasicAuthenticationFilter.class);
            // Add JWT token filter before the UsernamePasswordAuthenticationFilter
            http.addFilterBefore(apiJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);

            return http.build();
        }
    }

    @Configuration
    public static class StandardWebSecurityConfigurerAdapter {

        private static final List<String> DEFAULT_IGNORE_AUTH_URLS =
                Arrays.asList(ROOT_PATH, LOGIN, ALTERNATE_AUTH_PATH, LOGOUT, "/logged-out");

        /** The Collection of collected SecurityExemptionEndPoint from the classpath. */
        @Autowired(required = false)
        private Collection<SecurityExemptionEndPoint> securityExemptionEndPoints =
                Collections.emptyList();

        @Value("${squash.security.filter.debug.enabled:false}")
        private boolean debugSecurityFilter;

        @Value("${squash.security.preferred-auth-url:/login}")
        private String entryPointUrl = LOGIN;

        @Value("${squash.security.ignored:/scripts/**}")
        private String[] secIngored;

        @Bean
        public SessionRegistry sessionRegistry() {
            return new SessionRegistryImpl();
        }

        @Bean
        public HttpSessionEventPublisher httpSessionEventPublisher() {
            return new HttpSessionEventPublisher();
        }

        @Bean
        public WebSecurityCustomizer standardWebSecurityCustomizer() {
            return web -> web.debug(debugSecurityFilter).ignoring().antMatchers(secIngored);
        }

        @Bean
        @Order(30)
        protected SecurityFilterChain standardFilterChain(HttpSecurity http) throws Exception {
            http.headers()
                    .frameOptions()
                    .sameOrigin()
                    .and()
                    .csrf()
                    .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
                    .ignoringAntMatchers(gatherIgnoringCsrfUrlPatterns())
                    .and()
                    .httpBasic()
                    .and()
                    .authorizeRequests()
                    // allow access to main/alternate authentication portals
                    // note : on this domain the requests will always succeed, thus the user will not be
                    // redirected via the main entry point
                    .antMatchers(gatherIgnoringAuthUrlPatterns())
                    .permitAll()
                    // public
                    .antMatchers(
                            CONTROLLERS_ROOT_URL + LOGIN,
                            CONTROLLERS_ROOT_URL + LOGOUT,
                            CONTROLLERS_ROOT_URL + "/login-page",
                            CONTROLLERS_ROOT_URL + "/version",
                            "/index.html",
                            LOGIN,
                            "/favicon.ico")
                    .permitAll()
                    .antMatchers("/plugin/**", "/index")
                    .permitAll()
                    .antMatchers(AngularAppPageUrls.getAllUrlsPatterns())
                    .permitAll()
                    .antMatchers(
                            "/*.js",
                            "/**/*.js",
                            "/**/*.json",
                            "/*.js.map",
                            "/**/*.js.map",
                            "/resources/**",
                            "/*.css",
                            "/*.ts",
                            "/*.ttf",
                            "/assets/**")
                    .permitAll()
                    // admin workspace
                    .antMatchers(ADMIN_ONLY_URLS)
                    .access(HAS_ROLE_ADMIN)
                    .anyRequest()
                    .authenticated()

                    // main entry point for unauthenticated users
                    .and()
                    .exceptionHandling()
                    .authenticationEntryPoint(mainEntryPoint())
                    .and()
                    .formLogin()
                    .loginProcessingUrl(CONTROLLERS_ROOT_URL + LOGIN)
                    .successHandler(singlePageAppAuthenticationSuccessHandler())
                    .failureHandler(singlePageAppAuthenticationFailureHandler())
                    // http sessions management
                    .and()
                    .sessionManagement()
                    .maximumSessions(-1)
                    .sessionRegistry(sessionRegistry());

            return http.build();
        }

        @Bean
        public SinglePageAppAuthenticationSuccessHandler singlePageAppAuthenticationSuccessHandler() {
            return new SinglePageAppAuthenticationSuccessHandler();
        }

        @Bean
        public SinglePageAppAuthenticationFailureHandler singlePageAppAuthenticationFailureHandler() {
            return new SinglePageAppAuthenticationFailureHandler();
        }

        @Bean
        public AuthenticationEntryPoint mainEntryPoint() {
            return new MainEntryPoint(entryPointUrl);
        }

        private String[] gatherIgnoringCsrfUrlPatterns() {
            List<String> result = new ArrayList<>(Collections.singletonList(ALTERNATE_AUTH_PATH));

            for (SecurityExemptionEndPoint endPoint : securityExemptionEndPoints) {
                result.addAll(endPoint.getIgnoreCsrfUrlPatterns());
            }

            return result.toArray(new String[0]);
        }

        /**
         * Allow queries containing certain special characters defined in the url. Like viewing
         * attachments which contains the file names in the url.
         */
        @Bean
        public HttpFirewall customHttpFirewall() {
            StrictHttpFirewall firewall = new StrictHttpFirewall();
            firewall.setAllowSemicolon(true);
            firewall.setAllowUrlEncodedPercent(true);
            return firewall;
        }

        private String[] gatherIgnoringAuthUrlPatterns() {
            List<String> result = new ArrayList<>(DEFAULT_IGNORE_AUTH_URLS);

            for (SecurityExemptionEndPoint endPoint : securityExemptionEndPoints) {
                result.addAll(endPoint.getIgnoreAuthUrlPatterns());
            }

            return result.toArray(new String[0]);
        }
    }

    /**
     * [Issue #6900]
     *
     * <p>The base64-encoded token for basic auth has a charset too. Spring Sec expects it to be UTF-8
     * but many people around expects it to be Latin-1 (iso-8859-1). This configurer allows to
     * configure the desired encoding.
     *
     * @author bsiri
     */
    private static final class BasicAuthCharsetConfigurer
            implements ObjectPostProcessor<BasicAuthenticationFilter> {

        private final String charset;

        public BasicAuthCharsetConfigurer(String charset) {
            super();
            this.charset = charset;
        }

        @Override
        public <O extends BasicAuthenticationFilter> O postProcess(O object) {
            object.setCredentialsCharset(charset);
            return object;
        }
    }
}
