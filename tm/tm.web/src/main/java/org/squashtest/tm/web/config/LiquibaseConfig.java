/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.config;

import static org.squashtest.tm.jooq.domain.Tables.CORE_CONFIG;
import static org.squashtest.tm.jooq.domain.Tables.DATABASECHANGELOG;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.regex.Pattern;
import javax.sql.DataSource;
import liquibase.exception.LiquibaseException;
import liquibase.integration.spring.SpringLiquibase;
import org.apache.commons.lang3.StringUtils;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.service.DatabaseVersionChecker;
import org.squashtest.tm.service.RepositoryConfig;
import org.squashtest.tm.web.exception.DatabaseConnectionException;
import org.squashtest.tm.web.exception.InvalidDbUpdateModeException;
import org.squashtest.tm.web.exception.PromptAttemptsException;
import org.squashtest.tm.web.exception.RequiredManualChangesetMissingException;
import org.squashtest.tm.web.exception.UnsupportedInteractivePromptException;

@Configuration
public class LiquibaseConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(LiquibaseConfig.class);
    private static final String V8_1_0_LAST_REQUIRED_MANUAL_CHANGESET_ID =
            "tm-8.1.0-replace-SSF-by-CF-in-denormalized-field-table";

    // Prompt variables
    private static final Pattern POSITIVE_PATTERN = Pattern.compile("^[yY]$");
    private static final Pattern NEGATIVE_PATTERN = Pattern.compile("^[nN]$");
    private static final int MAX_ATTEMPTS = 5;

    private final Environment env;
    private final DataSource dataSource;
    private String currentDatabaseVersion;

    @Autowired
    public LiquibaseConfig(Environment env, DataSource dataSource) {
        this.env = env;
        this.dataSource = dataSource;
    }

    @Bean
    public SpringLiquibase liquibase() {
        String dbUpdateModePropertyValue = env.getProperty("squash.db.update-mode");
        DbUpdate dbUpdateMode = DbUpdate.fromString(dbUpdateModePropertyValue);

        // Check if the database is up
        checkDatabaseAccess();

        // Create Bean liquibase and set shouldRun to false
        SpringLiquibase liquibase = new LiquibaseBasePerform();
        liquibase.setDataSource(dataSource);
        liquibase.setChangeLog(env.getProperty("spring.liquibase.change-log"));
        liquibase.setShouldRun(false);
        liquibase.setDropFirst(false);

        if (DbUpdate.DISABLED.equals(dbUpdateMode)) {
            return liquibase;
        } else {
            return handleDatabaseUpdate(dbUpdateMode, liquibase);
        }
    }

    private void checkDatabaseAccess() {
        final int MAX_RETRIES = 12;
        final int RETRY_INTERVAL_MS = 5000;
        SQLException sqlException = null;

        for (int attempt = 0; attempt < MAX_RETRIES; attempt++) {
            try (Connection connection = dataSource.getConnection()) {
                LOGGER.debug(
                        "Connection to database successful on attempt {} / ", attempt + 1, MAX_RETRIES);
                return;
            } catch (SQLException e) {
                sqlException = e;
                LOGGER.warn(
                        "Database connection failed on attempt {}. Retrying in {} ms...",
                        attempt + 1,
                        RETRY_INTERVAL_MS);
                try {
                    Thread.sleep(RETRY_INTERVAL_MS);
                } catch (InterruptedException ie) {
                    Thread.currentThread().interrupt();
                }
            }
        }
        LOGGER.error("Database connection failed after {} attempts. Aborting.", MAX_RETRIES);
        throw new DatabaseConnectionException(
                "Error while connecting to database after multiple retries", sqlException);
    }

    private SpringLiquibase handleDatabaseUpdate(DbUpdate dbUpdateMode, SpringLiquibase liquibase) {
        org.jooq.Configuration jooqConfig = getJooqConfig();
        DSLContext dslContext = DSL.using(jooqConfig);
        if (checkDatabaseChangelogTableExists(dslContext)) {
            handleDatabaseNotEmpty(dbUpdateMode, liquibase, dslContext);
        } else {
            // Liquibase will create all Squash tables. We don't want to ignore "Legacy" labels so that
            // Liquibase runs all the changesets.
            liquibase.setShouldRun(true);
        }
        return liquibase;
    }

    private org.jooq.Configuration getJooqConfig() {
        RepositoryConfig repositoryConfig = new RepositoryConfig(env);
        org.jooq.Configuration configuration = repositoryConfig.getJooqConfiguration();
        return configuration.set(dataSource);
    }

    private boolean checkDatabaseChangelogTableExists(DSLContext dslContext) {
        try {
            return dslContext.fetchExists(DATABASECHANGELOG);
        } catch (DataAccessException e) {
            LOGGER.debug("databasechangelog table doesn't exist.", e);
            return false;
        }
    }

    private void handleDatabaseNotEmpty(
            DbUpdate dbUpdateMode, SpringLiquibase liquibase, DSLContext dslContext) {
        if (checkingDatabaseHasLastRequiredManualChangeset(dslContext)) {
            if (DbUpdate.INTERACTIVE.equals(dbUpdateMode) && !databaseIsUpToDate(dslContext)) {
                askUserPermissionForAutoUpdate(liquibase);
            } else {
                liquibase.setLabelFilter(env.getProperty("spring.liquibase.labels"));
                liquibase.setShouldRun(true);
            }
        } else {
            throw new RequiredManualChangesetMissingException(
                    "Last required manual changeset not found. You need to update your database manually to version 8.1 (included)");
        }
    }

    private boolean databaseIsUpToDate(DSLContext dslContext) {
        currentDatabaseVersion = getDatabaseVersion(dslContext);
        return DatabaseVersionChecker.DATABASE_SQUASH_VERSION.equals(currentDatabaseVersion);
    }


    private String getDatabaseVersion(DSLContext dslContext) {
        currentDatabaseVersion = dslContext
            .select(CORE_CONFIG.VALUE)
            .from(CORE_CONFIG)
            .where(CORE_CONFIG.STR_KEY.eq("squashtest.tm.database.version"))
            .fetchOneInto(String.class);
        return currentDatabaseVersion;
    }

    private void askUserPermissionForAutoUpdate(SpringLiquibase liquibase) {
        System.out.printf("%n%n%s%n%s%n%s%n", "=".repeat(20), "Database update", "=".repeat(20));

        String prompt = String.format("""
                ################################################################################
                ################################################################################
                #####                                                                      #####
                #####   /!\\                   ACTION REQUIRED                        /!\\   #####
                #####                                                                      #####
                #####  >> The upgrade of the Squash database from version %s            #####
                #####             to version %s is about to be performed.               #####
                #####  >> Before running the update, make sure you performed a             #####
                #####             database's backup.                                       #####
                #####                                                                      #####
                #####  >> Do you agree to proceed with the database upgrade (Y/N)?         #####
                #####  >> If you answer N, the database won't be upgraded                  #####
                #####             and Squash will stop.                                    #####
                #####                                                                      #####
                ################################################################################
                ################################################################################
            """, currentDatabaseVersion, DatabaseVersionChecker.DATABASE_SQUASH_VERSION);
        System.out.println(prompt);

        int attempts = 0;
        try (Scanner scanner = new Scanner(System.in)) {
            while (attempts < MAX_ATTEMPTS) {
                System.out.printf(
                        "Enter your choice (y/n): [%d/%d attempts]%n", attempts + 1, MAX_ATTEMPTS);
                String userInput = scanner.nextLine().trim();
                if (POSITIVE_PATTERN.matcher(userInput).matches()) {
                    liquibase.setLabelFilter(env.getProperty("spring.liquibase.labels"));
                    liquibase.setShouldRun(true);
                    System.out.println("Database update initiated.");
                    return;
                } else if (NEGATIVE_PATTERN.matcher(userInput).matches()) {
                    System.out.println("Database update canceled.");
                    return;
                } else {
                    System.out.println("Invalid input. Please enter 'y' for yes or 'n' for no.");
                    attempts++;
                }
            }
        } catch (NoSuchElementException e) {
            LOGGER.error(e.getMessage(), e);
            throw new UnsupportedInteractivePromptException(
                    String.format("The database needs to be updated while Squash TM has been configured with the squash.db.update-mode parameter set to \"%s\". But no console is attached to the process, so the manual input required for confirming the database update cannot be provided. Squash TM will be stopped.%nPlease, either update the value of the squash.db.update-mode parameter (in the squash.tm.cfg.properties file) to \"%s\" or \"%s\", or launch Squash TM from the command line. Refer to the Squash documentation for the details of the database automated update. Do not forget to perform a backup of your database beforehand.",
                        DbUpdate.INTERACTIVE, DbUpdate.FORCED, DbUpdate.ONLY));
        }
        throw new PromptAttemptsException("Exceeded maximum number of attempts. Operation aborted.");
    }

    private boolean checkingDatabaseHasLastRequiredManualChangeset(DSLContext dslContext) {
        return dslContext.fetchExists(
                dslContext
                        .select(DATABASECHANGELOG.ID)
                        .from(DATABASECHANGELOG)
                        .where(DATABASECHANGELOG.ID.eq(V8_1_0_LAST_REQUIRED_MANUAL_CHANGESET_ID)));
    }

    public enum DbUpdate {
        INTERACTIVE,
        ONLY,
        FORCED,
        DISABLED;

        public static DbUpdate fromString(String value) {
            if (StringUtils.isBlank(value)) {
                return INTERACTIVE;
            }

            return Arrays.stream(values())
                    .filter(dbUpdate -> dbUpdate.name().equalsIgnoreCase(value))
                    .findFirst()
                    .orElseThrow(
                            () ->
                                    new InvalidDbUpdateModeException(
                                            String.format(
                                                    "Invalid squash.db.update-mode property: \"%s\". Valid values are: interactive, only, forced or disabled.",
                                                    value)));
        }

        @Override
        public String toString() {
            return name().toLowerCase();
        }
    }

    private class LiquibaseBasePerform extends SpringLiquibase {

        @Override
        public void afterPropertiesSet() throws LiquibaseException {
            super.afterPropertiesSet();
            DbUpdate dbUpdateMode = DbUpdate.fromString(env.getProperty("squash.db.update-mode"));
            if (DbUpdate.ONLY.equals(dbUpdateMode)) {
                LOGGER.info("Database update mode set to 'only'. The program will now stop");
                Runtime.getRuntime().halt(0);
            }
        }
    }
}
