/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.exceptionresolver;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.AbstractHandlerExceptionResolver;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;
import org.squashtest.tm.web.backend.exceptionresolver.model.FieldValidationErrorModel;
import org.squashtest.tm.web.backend.exceptionresolver.model.SquashFieldValidationErrorModel;

/** Created by jthebault on 15/10/2020. */
@ControllerAdvice
public class HandlerTransactionSystemExceptionResolver extends AbstractHandlerExceptionResolver {

    @Override
    @ExceptionHandler(value = {TransactionSystemException.class})
    protected ModelAndView doResolveException(
            HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        if (exceptionIsHandled(ex)) {
            response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);

            ConstraintViolationException bex =
                    (ConstraintViolationException) ex.getCause().getCause(); // NOSONAR
            // Type was checked earlier
            List<FieldValidationErrorModel> errors = buildFieldValidationErrors(bex);

            SquashFieldValidationErrorModel squashError = new SquashFieldValidationErrorModel(errors);

            return new ModelAndView(new MappingJackson2JsonView(), "squashTMError", squashError);
        }
        return null;
    }

    private List<FieldValidationErrorModel> buildFieldValidationErrors(
            ConstraintViolationException constraintViolationException) {
        return constraintViolationException.getConstraintViolations().stream()
                .map(
                        constraintViolation ->
                                new FieldValidationErrorModel(
                                        "",
                                        constraintViolation.getPropertyPath().toString(),
                                        constraintViolation.getMessage()))
                .toList();
    }

    private boolean exceptionIsHandled(Exception ex) {
        Throwable rootCause =
                ex.getCause() != null && ex.getCause().getCause() != null ? ex.getCause().getCause() : null;
        if (rootCause == null) {
            return false;
        }

        return rootCause instanceof ConstraintViolationException;
    }
}
