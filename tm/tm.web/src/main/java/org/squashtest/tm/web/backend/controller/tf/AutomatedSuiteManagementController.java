/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.tf;

import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.testautomation.AutomatedSuite;
import org.squashtest.tm.service.internal.dto.executioncomparator.TestExecutionInfo;
import org.squashtest.tm.service.internal.testautomation.testplanretriever.RestTestPlanFinderImpl;
import org.squashtest.tm.service.testautomation.AutomatedSuiteManagerService;
import org.squashtest.tm.service.testautomation.AutomatedSuitePreviewService;
import org.squashtest.tm.service.testautomation.TestExecutionInfoService;
import org.squashtest.tm.service.testautomation.model.AutomatedSuiteCreationSpecification;
import org.squashtest.tm.service.testautomation.model.AutomatedSuitePreview;
import org.squashtest.tm.service.testautomation.supervision.AutomatedExecutionViewUtils;
import org.squashtest.tm.service.testautomation.supervision.model.AutomatedSuiteOverview;

@RestController
@RequestMapping("backend/automated-suites")
public class AutomatedSuiteManagementController {

    private final AutomatedSuiteManagerService automatedSuiteService;
    private final TestExecutionInfoService testExecutionInfoService;
    private final AutomatedSuitePreviewService automatedSuitePreviewService;
    private final RestTestPlanFinderImpl testPlanFinder;

    public AutomatedSuiteManagementController(
            AutomatedSuiteManagerService automatedSuiteService,
            TestExecutionInfoService testExecutionInfoService,
            AutomatedSuitePreviewService automatedSuitePreviewService,
            RestTestPlanFinderImpl testPlanFinder) {
        this.automatedSuiteService = automatedSuiteService;
        this.testExecutionInfoService = testExecutionInfoService;
        this.automatedSuitePreviewService = automatedSuitePreviewService;
        this.testPlanFinder = testPlanFinder;
    }

    @PostMapping(value = "/preview")
    public AutomatedSuitePreview generateSuitePreview(
            @RequestBody AutomatedSuiteCreationSpecification specification) {
        return automatedSuitePreviewService.preview(specification);
    }

    @PostMapping(value = "/preview/test-list", params = "auto-project-id")
    public List<String> findTestListPreview(
            @RequestBody AutomatedSuiteCreationSpecification specification,
            @RequestParam("auto-project-id") Long automatedProjectId) {
        return automatedSuitePreviewService.findTestListPreview(specification, automatedProjectId);
    }

    @PostMapping(value = "/create-and-execute")
    public AutomatedSuiteOverview createAndExecute(
            @RequestBody AutomatedSuiteCreationSpecification specification) {
        List<Long> itemTestPlanIds = testPlanFinder.getItemTestPlanIdsFromSpecification(specification);
        return automatedSuiteService.createAndExecute(specification, itemTestPlanIds);
    }

    @GetMapping(value = "/{suiteId}/executions")
    public AutomatedSuiteOverview updateExecutionInfo(@PathVariable String suiteId) {
        AutomatedSuite suite = automatedSuiteService.findById(suiteId);
        return AutomatedExecutionViewUtils.buildAutomatedSuiteOverview(suite);
    }

    @PostMapping(value = "/compare-executions")
    public List<TestExecutionInfo> compareExecutions(@RequestBody List<String> suiteIds) {
        return testExecutionInfoService.compareExecutionsBySuites(suiteIds);
    }

    @PostMapping(value = "/deletion")
    public void deleteAutomatedSuites(@RequestBody List<String> suiteIds) {
        automatedSuiteService.deleteAutomatedSuites(suiteIds);
    }

    @PostMapping(
            value = "/attachment-prune",
            params = {"complete"})
    public void pruneAutomatedSuites(
            @RequestBody List<String> suiteIds, @RequestParam("complete") boolean complete) {
        automatedSuiteService.pruneAutomatedSuites(suiteIds, complete);
    }
}
