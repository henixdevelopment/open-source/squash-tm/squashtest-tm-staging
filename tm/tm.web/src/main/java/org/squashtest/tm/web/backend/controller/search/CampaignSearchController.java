/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.search;

import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.query.Operation;
import org.squashtest.tm.domain.query.QueryColumnPrototypeReference;
import org.squashtest.tm.service.display.search.CampaignSearchGridDisplayService;
import org.squashtest.tm.service.display.search.CampaignSearchInputData;
import org.squashtest.tm.service.display.search.CampaignSearchInputDataProvider;
import org.squashtest.tm.service.display.search.CampaignSearchService;
import org.squashtest.tm.service.display.search.ResearchResult;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;

@RestController
@RequestMapping("backend/search/campaign")
public class CampaignSearchController {

    private final CampaignSearchInputDataProvider inputDataProvider;
    private final CampaignSearchService campaignSearchService;
    private final CampaignSearchGridDisplayService campaignSearchGridDisplayService;

    public CampaignSearchController(
            CampaignSearchInputDataProvider inputDataProvider,
            CampaignSearchService campaignSearchService,
            CampaignSearchGridDisplayService campaignSearchGridDisplayService) {
        this.inputDataProvider = inputDataProvider;
        this.campaignSearchService = campaignSearchService;
        this.campaignSearchGridDisplayService = campaignSearchGridDisplayService;
    }

    @GetMapping
    public CampaignSearchInputData getSearchInputData() {
        return inputDataProvider.provide();
    }

    @PostMapping
    public GridResponse search(@RequestBody GridRequest gridRequest) {
        List<String> testSuites =
                gridRequest.getScope().stream()
                        .filter(scope -> scope.contains(EntityType.TEST_SUITE.getSimpleClassName()))
                        .toList();
        List<String> scopeWithoutTestSuites =
                gridRequest.getScope().stream()
                        .filter(scope -> !scope.contains(EntityType.TEST_SUITE.getSimpleClassName()))
                        .toList();

        if (!testSuites.isEmpty() && !scopeWithoutTestSuites.isEmpty()) {
            gridRequest.setScope(scopeWithoutTestSuites);
            gridRequest.setExcludedScope(testSuites);
            GridResponse searchWithoutTestSuiteResult = doSearch(gridRequest);

            gridRequest.setScope(new ArrayList<>());
            createFilterValueOnTestSuiteIds(gridRequest, testSuites);
            GridResponse testSuiteSearchResult = doSearch(gridRequest);

            return combineMultipleSearchResultsIntoGridResponse(
                    searchWithoutTestSuiteResult, testSuiteSearchResult);

        } else if (!testSuites.isEmpty()) {
            createFilterValueOnTestSuiteIds(gridRequest, testSuites);
            return doSearch(gridRequest);

        } else {
            return doSearch(gridRequest);
        }
    }

    private GridResponse doSearch(GridRequest gridRequest) {
        ResearchResult researchResult = campaignSearchService.search(gridRequest);
        return campaignSearchGridDisplayService.fetchResearchRows(researchResult, gridRequest);
    }

    private GridResponse combineMultipleSearchResultsIntoGridResponse(
            GridResponse firstSearchResult, GridResponse testSuiteSearchResult) {
        List<DataRow> allSearchDataRows = firstSearchResult.getDataRows();
        allSearchDataRows.addAll(testSuiteSearchResult.getDataRows());
        firstSearchResult.setDataRows(allSearchDataRows);
        firstSearchResult.setCount(firstSearchResult.getCount() + testSuiteSearchResult.getCount());
        return firstSearchResult;
    }

    // The query engine don't accept TestSuite like 'scopable' entity
    // So we filter TestSuite from scope and create filter on it
    private void createFilterValueOnTestSuiteIds(GridRequest gridRequest, List<String> testSuites) {
        List<String> testSuitesIds =
                testSuites.stream()
                        .map(EntityReference::fromNodeId)
                        .map(EntityReference::getId)
                        .map(String::valueOf)
                        .toList();
        GridFilterValue gridFilterValue = new GridFilterValue();
        gridFilterValue.setOperation(Operation.IN.name());
        gridFilterValue.setColumnPrototype(QueryColumnPrototypeReference.ITEM_SUITE_ID);
        gridFilterValue.setValues(testSuitesIds);
        gridRequest.getFilterValues().add(gridFilterValue);
        List<String> scopeWithoutTestSuites =
                gridRequest.getScope().stream()
                        .filter(scope -> !scope.contains(EntityType.TEST_SUITE.getSimpleClassName()))
                        .toList();
        gridRequest.setScope(scopeWithoutTestSuites);
        gridRequest.setExcludedScope(testSuites);
    }
}
