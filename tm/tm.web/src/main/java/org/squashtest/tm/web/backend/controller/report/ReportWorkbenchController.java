/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.report;

import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.customreport.CustomReportLibraryNode;
import org.squashtest.tm.domain.customreport.CustomReportNodeType;
import org.squashtest.tm.domain.report.ReportDefinition;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;
import org.squashtest.tm.service.report.ReportModificationService;
import org.squashtest.tm.web.backend.controller.form.model.CreatedEntityId;
import org.squashtest.tm.web.backend.report.IdentifiedReportDecorator;
import org.squashtest.tm.web.backend.report.ReportsRegistry;

@RestController
@RequestMapping("backend/report-workbench")
public class ReportWorkbenchController {

    private final ReportsRegistry reportsRegistry;
    private final CustomReportLibraryNodeService customReportLibraryNodeService;
    private final ReportModificationService reportModificationService;

    public ReportWorkbenchController(
            ReportsRegistry reportsRegistry,
            CustomReportLibraryNodeService customReportLibraryNodeService,
            ReportModificationService reportModificationService) {
        this.reportsRegistry = reportsRegistry;
        this.customReportLibraryNodeService = customReportLibraryNodeService;
        this.reportModificationService = reportModificationService;
    }

    @GetMapping("/{customReportLibraryNodeId}")
    public ReportWorkbenchData getAvailableReports(@PathVariable Long customReportLibraryNodeId) {
        CustomReportLibraryNode node =
                customReportLibraryNodeService.findCustomReportLibraryNodeById(customReportLibraryNodeId);
        Long projectId = node.getLibrary().getProject().getId();
        if (node.getEntityType().getTypeName().equals(CustomReportNodeType.REPORT_NAME)) {
            ReportDefinition reportDefinition = (ReportDefinition) node.getEntity();
            ReportWorkbenchReportDefinition def = new ReportWorkbenchReportDefinition(reportDefinition);
            return new ReportWorkbenchData(this.reportsRegistry.getSortedReports(), projectId, def);
        } else {
            return new ReportWorkbenchData(this.reportsRegistry.getSortedReports(), projectId);
        }
    }

    static class ReportWorkbenchData {
        private final List<ReportData> availableReports;
        private final Long projectId;
        private final ReportWorkbenchReportDefinition reportDefinition;

        ReportWorkbenchData(List<IdentifiedReportDecorator> availableReports, Long projectId) {
            this(availableReports, projectId, null);
        }

        public ReportWorkbenchData(
                List<IdentifiedReportDecorator> availableReports,
                Long projectId,
                ReportWorkbenchReportDefinition reportDefinition) {
            this.availableReports = availableReports.stream().map(ReportData::new).toList();
            this.reportDefinition = reportDefinition;
            this.projectId = projectId;
        }

        public List<ReportData> getAvailableReports() {
            return availableReports;
        }

        public Long getProjectId() {
            return projectId;
        }

        public ReportWorkbenchReportDefinition getReportDefinition() {
            return reportDefinition;
        }
    }

    static class ReportWorkbenchReportDefinition {
        private final Long id;
        private final String name;
        private final String description;
        private final String summary;
        private final String pluginNamespace;
        private final String parameters;

        public ReportWorkbenchReportDefinition(ReportDefinition definition) {
            this.id = definition.getId();
            this.name = definition.getName();
            this.description = definition.getDescription();
            this.summary = definition.getSummary();
            this.pluginNamespace = definition.getPluginNamespace();
            this.parameters = definition.getParameters();
        }

        public Long getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }

        public String getSummary() {
            return summary;
        }

        public String getPluginNamespace() {
            return pluginNamespace;
        }

        public String getParameters() {
            return parameters;
        }
    }

    @PostMapping(value = "/new/{parentId}")
    public CreatedEntityId saveReport(
            @RequestBody ReportDefinition reportDefinition, @PathVariable("parentId") Long parentId) {
        CustomReportLibraryNode node =
                customReportLibraryNodeService.createNewNode(parentId, reportDefinition);
        return new CreatedEntityId(node.getId());
    }

    @PostMapping(value = "/update/{parentId}")
    public CreatedEntityId updateReportDefinition(
            @RequestBody ReportDefinition definition, @PathVariable("parentId") long parentId) {
        ReportDefinition oldDef = customReportLibraryNodeService.findReportDefinitionByNodeId(parentId);
        definition.setId(oldDef.getId());
        reportModificationService.updateDefinition(definition, oldDef);
        return new CreatedEntityId(parentId);
    }
}
