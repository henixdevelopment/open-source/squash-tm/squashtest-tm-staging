/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.report;

import org.squashtest.tm.api.report.form.CheckboxInput;
import org.squashtest.tm.api.report.form.InputType;
import org.squashtest.tm.api.report.form.NodeType;
import org.squashtest.tm.api.report.form.OptionInput;
import org.squashtest.tm.api.report.form.composite.TagPickerOption;
import org.squashtest.tm.api.report.form.composite.TreePickerOption;
import org.squashtest.tm.domain.customfield.BindableEntity;

public class OptionInputJson implements InputJson {
    private String name;
    private String label;
    private String value = "";
    private boolean defaultSelected = false;
    private String givesAccessTo = "none";
    private boolean isComposite = false;
    private InputType contentType;
    private String disabledBy;

    public OptionInputJson(OptionInput optionInput) {
        this.name = optionInput.getName();
        this.value = optionInput.getValue();
        this.defaultSelected = optionInput.isDefaultSelected();
        this.givesAccessTo = optionInput.getGivesAccessTo();
        this.label = optionInput.getLabel();
        this.disabledBy = optionInput.getDisabledBy();
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public boolean isDefaultSelected() {
        return defaultSelected;
    }

    public String getGivesAccessTo() {
        return givesAccessTo;
    }

    public boolean isComposite() {
        return isComposite;
    }

    public void setComposite(boolean composite) {
        isComposite = composite;
    }

    public String getLabel() {
        return label;
    }

    public InputType getContentType() {
        return contentType;
    }

    public void setContentType(InputType contentType) {
        this.contentType = contentType;
    }

    public static class TagPickerOptionJson extends OptionInputJson {

        private final BindableEntity bindableEntity;

        public TagPickerOptionJson(TagPickerOption tagPickerOption) {
            super(tagPickerOption);
            this.bindableEntity = BindableEntity.valueOf(tagPickerOption.getPickerBoundEntity());
        }

        public BindableEntity getBindableEntity() {
            return bindableEntity;
        }
    }

    public static class TreePickerOptionJson extends OptionInputJson {

        private final NodeType nodeType;

        public TreePickerOptionJson(TreePickerOption treePickerOption) {
            super(treePickerOption);
            this.nodeType = treePickerOption.getPickedNodeType();
        }

        public NodeType getNodeType() {
            return nodeType;
        }
    }

    public static class CheckBoxInputJson extends OptionInputJson {

        private final InputType type;

        private final String helpMessage;

        public CheckBoxInputJson(CheckboxInput checkboxInput) {
            super(checkboxInput);
            this.type = checkboxInput.getType();
            this.helpMessage = checkboxInput.getHelpMessage();
        }

        public InputType getType() {
            return type;
        }

        @Override
        public String getHelpMessage() {
            return helpMessage;
        }
    }

    public String getDisabledBy() {
        return disabledBy;
    }
}
