/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.user;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.csp.core.bugtracker.core.BugTrackerNoCredentialsDetailedException;
import org.squashtest.csp.core.bugtracker.core.BugTrackerNoCredentialsException;
import org.squashtest.csp.core.bugtracker.core.BugTrackerRemoteException;
import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.domain.users.Party;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.exception.bugtracker.BadCredentialsException;
import org.squashtest.tm.exception.bugtracker.CannotConnectBugtrackerException;
import org.squashtest.tm.service.internal.display.dto.BugTrackerCredentialsDto;
import org.squashtest.tm.service.internal.display.dto.BugTrackerDto;
import org.squashtest.tm.service.internal.display.dto.CredentialsDto;
import org.squashtest.tm.service.internal.display.dto.ProjectPermissionDto;
import org.squashtest.tm.service.internal.display.dto.UserAccountDto;
import org.squashtest.tm.service.internal.display.dto.UsersGroupDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.security.AuthenticationProviderContext;
import org.squashtest.tm.service.internal.servers.ManageableBasicAuthCredentials;
import org.squashtest.tm.service.internal.servers.ManageableTokenAuthCredentials;
import org.squashtest.tm.service.internal.servers.OAuth2ConsumerService;
import org.squashtest.tm.service.project.ProjectsPermissionFinder;
import org.squashtest.tm.service.servers.ManageableCredentials;
import org.squashtest.tm.service.servers.Oauth2Tokens;
import org.squashtest.tm.service.servers.StoredCredentialsManager;
import org.squashtest.tm.service.user.PartyPreferenceService;
import org.squashtest.tm.service.user.UserAccountService;

@RestController
@RequestMapping("backend/user-account")
public class UserAccountController {
    private static final String SQUASH_BUGTRACKER_MODE = "squash.bug.tracker.mode";

    private final UserAccountService userAccountService;
    private final ProjectsPermissionFinder permissionFinder;
    private final PartyPreferenceService partyPreferenceService;
    private final StoredCredentialsManager credentialsManager;
    private final AuthenticationProviderContext authenticationProviderContext;
    private final OAuth2ConsumerService oAuth2ConsumerService;

    UserAccountController(
            UserAccountService userAccountService,
            ProjectsPermissionFinder permissionFinder,
            PartyPreferenceService partyPreferenceService,
            StoredCredentialsManager credentialsManager,
            AuthenticationProviderContext authenticationProviderContext,
            OAuth2ConsumerService oAuth2ConsumerService) {
        this.userAccountService = userAccountService;
        this.permissionFinder = permissionFinder;
        this.partyPreferenceService = partyPreferenceService;
        this.credentialsManager = credentialsManager;
        this.authenticationProviderContext = authenticationProviderContext;
        this.oAuth2ConsumerService = oAuth2ConsumerService;
    }

    @GetMapping
    public UserAccountDto getUserAccountDetails() {
        UserAccountDto dto = new UserAccountDto();
        User user = userAccountService.findCurrentUser();

        dto.setId(user.getId());
        dto.setFirstName(user.getFirstName());
        dto.setLastName(user.getLastName());
        dto.setLogin(user.getLogin());
        dto.setUserGroup(UsersGroupDto.from(user.getGroup()));
        dto.setEmail(user.getEmail());

        List<ProjectPermissionDto> projectPermissions =
                permissionFinder.findProjectPermissionByUserLogin(user.getLogin()).stream()
                        .map(ProjectPermissionDto::from)
                        .toList();

        dto.setProjectPermissions(projectPermissions);

        Party party = userAccountService.getParty(user.getId());
        Map<String, String> map = partyPreferenceService.findPreferences(party);
        dto.setBugTrackerMode(map.get(SQUASH_BUGTRACKER_MODE));

        dto.setBugTrackerCredentials(getBugtrackerCredentials());

        dto.setCanManageLocalPassword(authenticationProviderContext.isInternalProviderEnabled());
        dto.setHasLocalPassword(userAccountService.hasCurrentUserPasswordDefined());
        return dto;
    }

    @PostMapping("{partyId}/email")
    void changeEmail(@PathVariable Long partyId, @RequestBody UserAccountChangeRequest requestBody) {
        userAccountService.setCurrentUserEmail(requestBody.getEmail());
    }

    @PostMapping(value = "/password")
    public void changePassword(@Valid @RequestBody PasswordChangeForm form) {
        if (form.isInitializing()) {
            userAccountService.setCurrentUserPassword(form.getNewPassword());
        } else {
            userAccountService.setCurrentUserPassword(form.getOldPassword(), form.getNewPassword());
        }
    }

    @PostMapping(value = "/bug-tracker-mode")
    public void changeUserBugtrackerMode(@RequestBody UserAccountChangeRequest requestBody) {
        partyPreferenceService.addOrUpdatePreferenceForCurrentUser(
                SQUASH_BUGTRACKER_MODE, requestBody.getBugTrackerMode());
    }

    public static class UserAccountChangeRequest {
        private String email;
        private String bugTrackerMode;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getBugTrackerMode() {
            return bugTrackerMode;
        }

        public void setBugTrackerMode(String bugTrackerMode) {
            this.bugTrackerMode = bugTrackerMode;
        }
    }

    private List<BugTrackerCredentialsDto> getBugtrackerCredentials() {
        return userAccountService.findAllUserBugTracker().stream()
                .sorted(Comparator.comparing((BugTracker::getAuthenticationProtocol)))
                .map(
                        bt -> {
                            BugTrackerCredentialsDto dto = new BugTrackerCredentialsDto();
                            BugTrackerDto bugTrackerDto = BugTrackerDto.from(bt);

                            dto.setBugTracker(bugTrackerDto);
                            dto.setCredentials(CredentialsDto.from(getOrCreateCredentials(bt)));
                            return dto;
                        })
                .toList();
    }

    private ManageableCredentials getOrCreateCredentials(BugTracker bugtracker) {
        ManageableCredentials credentials =
                credentialsManager.findCurrentUserCredentials(bugtracker.getId());

        if (credentials == null) {
            credentials =
                    switch (bugtracker.getAuthenticationProtocol()) {
                        case BASIC_AUTH -> new ManageableBasicAuthCredentials("", "");
                        case TOKEN_AUTH -> new ManageableTokenAuthCredentials("");
                        case OAUTH_2 -> new Oauth2Tokens("", "", "", null);
                        default ->
                                throw new IllegalArgumentException(
                                        "AuthenticationProtocol '"
                                                + bugtracker.getAuthenticationProtocol()
                                                + "' not supported");
                    };
        }

        return credentials;
    }

    @PostMapping("bugtracker/{bugTrackerId}/credentials")
    public void saveCurrentUserCredentials(
            @PathVariable long bugTrackerId, @RequestBody ManageableCredentials credentials) {
        try {
            userAccountService.testCurrentUserCredentials(bugTrackerId, credentials);
            userAccountService.saveCurrentUserCredentials(bugTrackerId, credentials);
        } catch (BugTrackerNoCredentialsDetailedException | BugTrackerNoCredentialsException e) {
            throw new BadCredentialsException(e);
        } catch (BugTrackerRemoteException ex) {
            throw new CannotConnectBugtrackerException(ex);
        }
    }

    @PostMapping(value = "bugtracker/{bugTrackerId}/authentication/oauth2/token")
    public void askOauth2Token(
            @PathVariable("bugTrackerId") long bugTrackerId, @RequestParam("code") String code) {
        oAuth2ConsumerService.getCurrentUserOauth2token(bugTrackerId, code);
    }

    @DeleteMapping("bugtracker/{bugTrackerId}/credentials")
    public void deleteUserCredentials(@PathVariable long bugTrackerId) {
        userAccountService.deleteCurrentUserCredentials(bugTrackerId);
    }

    @PostMapping(value = "personal-api-tokens")
    public GridResponse findPersonalApiTokens(@RequestBody GridRequest request) {
        return userAccountService.findPersonalApiTokens(request);
    }
}
