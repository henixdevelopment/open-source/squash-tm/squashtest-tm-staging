/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.config;

import java.util.List;
import javax.inject.Inject;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.grid.GridConfigurationService;
import org.squashtest.tm.service.internal.dto.GridColumnConfigDto;

@RestController
@RequestMapping("/backend/grid")
public class GridConfigurationController {

    @Inject private GridConfigurationService gridConfigurationService;

    @PostMapping("/update-column-config")
    public void updateGridColumnConfig(@RequestBody GridColumnConfigDto request) {
        gridConfigurationService.addOrUpdatePreferenceForCurrentUser(
                request.getGridId(), request.getActiveColumnIds());
    }

    @PostMapping("/update-column-config-with-project")
    public void updateGridColumnConfigWithProject(@RequestBody GridColumnConfigDto request) {
        gridConfigurationService.addOrUpdatePreferenceForCurrentUserWithProjectId(
                request.getGridId(), request.getActiveColumnIds(), request.getProjectId());
    }

    @DeleteMapping("/{gridId}/reset-column-config")
    public void resetGridColumnConfig(@PathVariable String gridId) {
        gridConfigurationService.resetGridColumnConfig(gridId);
    }

    @DeleteMapping("/{gridId}/reset-column-config-with-project/{projectId}")
    public void resetGridColumnConfigWithProjectId(
            @PathVariable String gridId, @PathVariable String projectId) {
        gridConfigurationService.resetGridColumnConfigWithProjectId(gridId, projectId);
    }

    @PostMapping("/get-column-config")
    public List<String> getGridColumnConfig(@RequestBody GridColumnConfigDto request) {
        return gridConfigurationService.findActiveColumnIdsForUser(request.getGridId());
    }

    @PostMapping("/get-column-config-with-project")
    public List<String> getGridColumnConfigWithProject(@RequestBody GridColumnConfigDto request) {
        return gridConfigurationService.findActiveColumnIdsForUserWithProjectId(
                request.getGridId(), request.getProjectId());
    }
}
