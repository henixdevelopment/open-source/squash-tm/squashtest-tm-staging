/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.environmentvariable;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariable;
import org.squashtest.tm.exception.DomainException;
import org.squashtest.tm.service.display.environmentvariable.EnvironmentVariableDisplayService;
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableManagerService;
import org.squashtest.tm.service.internal.display.dto.EnvironmentVariableDto;
import org.squashtest.tm.service.internal.display.dto.EnvironmentVariableOptionDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.web.backend.controller.form.model.EnvironmentVariableFormModel;

@RestController
@RequestMapping("/backend/environment-variables")
public class EnvironmentVariableController {
    private final EnvironmentVariableManagerService environmentVariableManagerService;
    private final EnvironmentVariableDisplayService environmentVariableDisplayService;

    public EnvironmentVariableController(
            EnvironmentVariableManagerService environmentVariableManagerService,
            EnvironmentVariableDisplayService environmentVariableDisplayService) {
        this.environmentVariableManagerService = environmentVariableManagerService;
        this.environmentVariableDisplayService = environmentVariableDisplayService;
    }

    @PostMapping
    public GridResponse getAllEnvironmentVariables(@RequestBody GridRequest request) {
        return environmentVariableDisplayService.findAll(request);
    }

    @GetMapping
    public Map<String, List<EnvironmentVariableDto>> getAllEnvironmentVariableReferences() {
        return Collections.singletonMap(
                "environmentVariables",
                environmentVariableDisplayService.getAllEnvironmentVariableReferences());
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/new")
    public Map<String, Object> createEnvironmentVariable(
            @RequestBody EnvironmentVariableFormModel model) {
        Map<String, Object> response = new HashMap<>();
        EnvironmentVariable environmentVariable = model.getEnvironmentVariable();
        environmentVariableManagerService.persist(environmentVariable);
        response.put("id", environmentVariable.getId());
        return response;
    }

    @DeleteMapping(value = "/{environmentVariableIds}")
    public void deleteEnvironmentVariable(
            @PathVariable("environmentVariableIds") List<Long> environmentVariableIds) {
        environmentVariableManagerService.deleteEnvironmentVariable(environmentVariableIds);
    }

    @PostMapping(value = "/{evId}/name")
    public void changeName(@PathVariable long evId, @RequestBody Map<String, String> request) {
        environmentVariableManagerService.changeName(evId, request.get("name"));
    }

    @PostMapping(value = "/{evId}/options/label")
    public void changeOptionLabel(
            @PathVariable long evId, @RequestBody EnvironmentVariableOptionPatch patch) {
        environmentVariableManagerService.changeOptionLabel(
                evId, patch.getCurrentLabel(), patch.getNewLabel());
    }

    @PostMapping(value = "/{evId}/options/new")
    public Map<String, List<EnvironmentVariableOptionDto>> addNewOption(
            @PathVariable long evId, @Valid @RequestBody String option) {
        Map<String, List<EnvironmentVariableOptionDto>> response = new HashMap<>();
        try {
            environmentVariableManagerService.addOption(evId, option);
        } catch (DomainException e) {
            e.setObjectName("new-environment-variable-option");
            throw e;
        }
        response.put("options", this.environmentVariableDisplayService.getOptionsByEvId(evId));
        return response;
    }

    @PostMapping(value = "/{evId}/options/remove")
    public void removeOptions(
            @PathVariable Long evId, @RequestBody Map<String, List<String>> request) {
        environmentVariableManagerService.removeOptions(evId, request.get("optionLabels"));
    }

    @PostMapping("/{environmentVariableId}/options/positions")
    public EnvironmentVariableDto changeOptionsPositions(
            @PathVariable Long environmentVariableId, @RequestBody ReorderOptionsPatch patch) {
        environmentVariableManagerService.changeOptionsPosition(
                environmentVariableId, patch.getPosition(), patch.getLabels());
        return environmentVariableDisplayService.getEnvironmentVariableView(environmentVariableId);
    }

    static class EnvironmentVariableOptionPatch {

        private String currentLabel;
        private String newLabel;

        public String getCurrentLabel() {
            return currentLabel;
        }

        public void setCurrentLabel(String currentLabel) {
            this.currentLabel = currentLabel;
        }

        public String getNewLabel() {
            return newLabel;
        }

        public void setNewLabel(String newLabel) {
            this.newLabel = newLabel;
        }
    }

    static class ReorderOptionsPatch {

        private List<String> labels;

        private Integer position;

        public List<String> getLabels() {
            return labels;
        }

        public void setLabels(List<String> labels) {
            this.labels = labels;
        }

        public Integer getPosition() {
            return position;
        }

        public void setPosition(Integer position) {
            this.position = position;
        }
    }
}
