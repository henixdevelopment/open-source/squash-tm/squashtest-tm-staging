/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.actionword;

import static org.squashtest.tm.api.security.acls.Roles.ROLE_ADMIN;

import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.bdd.ActionWord;
import org.squashtest.tm.service.internal.display.dto.actionword.ActionWordDto;
import org.squashtest.tm.service.security.PermissionEvaluationService;

@Component
public class ActionWordDtoBuilder {

    private final PermissionEvaluationService permissionService;

    public ActionWordDtoBuilder(PermissionEvaluationService permissionService) {
        this.permissionService = permissionService;
    }

    public ActionWordDto build(ActionWord actionWord, Long actionWordLibraryNodeId) {
        ActionWordDto actionWordDto = new ActionWordDto();
        fillBasicAttributes(actionWord, actionWordDto);
        fillAuditableAttributes(actionWord, actionWordDto);
        fillOtherAttributes(actionWord, actionWordDto);
        actionWordDto.setActionWordLibraryNodeId(actionWordLibraryNodeId);
        return actionWordDto;
    }

    private void fillBasicAttributes(ActionWord actionWord, ActionWordDto actionWordDto) {
        actionWordDto.setId(actionWord.getId());
        actionWordDto.setWord(actionWord.createWord());
        actionWordDto.setDescription(actionWord.getDescription());
        actionWordDto.setProjectName(actionWord.getProject().getName());
    }

    private void fillAuditableAttributes(ActionWord actionWord, ActionWordDto actionWordDto) {
        actionWordDto.setCreatedBy(actionWord.getCreatedBy());
        actionWordDto.setCreatedOn(actionWord.getCreatedOn());
        actionWordDto.setLastModifiedBy(actionWord.getLastModifiedBy());
        actionWordDto.setLastModifiedOn(actionWord.getLastModifiedOn());
    }

    private void fillOtherAttributes(ActionWord actionWord, ActionWordDto actionWordDto) {
        actionWordDto.setWritable(
                permissionService.hasRoleOrPermissionOnObject(ROLE_ADMIN, "WRITE", actionWord));
    }
}
