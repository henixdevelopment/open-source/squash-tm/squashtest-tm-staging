/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.artificialintelligence.server;

import java.util.Date;
import java.util.Map;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.artificialintelligence.server.AiServerManagerService;
import org.squashtest.tm.service.artificialintelligence.testcasegeneration.AiTestCaseGenerationService;
import org.squashtest.tm.service.display.artificialintelligence.server.AiServerDisplayService;
import org.squashtest.tm.service.internal.display.dto.AiServerAdminViewDto;
import org.squashtest.tm.service.servers.ManageableCredentials;
import org.squashtest.tm.service.thirdpartyserver.ThirdPartyServerCredentialsService;
import org.squashtest.tm.web.backend.controller.utils.UrlValidator;

@RestController
@RequestMapping("/backend/ai-server")
public class AiServerViewController {

    private final AiServerDisplayService aiServerDisplayService;
    private final AiServerManagerService aiServerManagerService;
    private final ThirdPartyServerCredentialsService thirdPartyServerCredentialsService;
    private final AiTestCaseGenerationService aiTestCaseGenerationService;

    public AiServerViewController(
            AiServerDisplayService aiServerDisplayService,
            AiServerManagerService aiServerManagerService,
            ThirdPartyServerCredentialsService thirdPartyServerCredentialsService,
            AiTestCaseGenerationService aiTestCaseGenerationService) {
        this.aiServerDisplayService = aiServerDisplayService;
        this.aiServerManagerService = aiServerManagerService;
        this.thirdPartyServerCredentialsService = thirdPartyServerCredentialsService;
        this.aiTestCaseGenerationService = aiTestCaseGenerationService;
    }

    @GetMapping("/{aiServerId}")
    public AiServerAdminViewDto getAiServerView(@PathVariable Long aiServerId) {
        return aiServerDisplayService.getAiServerView(aiServerId);
    }

    @PostMapping("/{aiServerId}/name")
    public void updateName(
            @PathVariable long aiServerId, @RequestBody AiServerRecord aiServerRecord) {
        aiServerManagerService.updateName(aiServerId, aiServerRecord.name);
    }

    @PostMapping("/{aiServerId}/description")
    public void updateDescription(
            @PathVariable long aiServerId, @RequestBody AiServerRecord aiServerRecord) {
        aiServerManagerService.updateDescription(aiServerId, aiServerRecord.description);
    }

    @PostMapping("/{aiServerId}/url")
    public void updateUrl(@PathVariable long aiServerId, @RequestBody AiServerRecord aiServerRecord) {
        UrlValidator.checkURL(aiServerRecord.url);
        aiServerManagerService.updateUrl(aiServerId, aiServerRecord.url);
    }

    @PostMapping("/{aiServerId}/credentials")
    public void storeCredentials(
            @PathVariable long aiServerId, @RequestBody ManageableCredentials credentials) {
        thirdPartyServerCredentialsService.storeCredentials(aiServerId, credentials);
        aiServerManagerService.forceAuditAfterCredentialsUpdate(aiServerId);
    }

    @DeleteMapping("/{aiServerId}/credentials")
    public void deleteCredentials(@PathVariable long aiServerId) {
        thirdPartyServerCredentialsService.deleteCredentials(aiServerId);
    }

    @PostMapping("/{aiServerId}/new-payload")
    public void updatePayloadTemplate(
            @PathVariable long aiServerId, @RequestBody AiServerRecord aiServerRecord) {
        aiServerManagerService.updatePayloadTemplate(aiServerId, aiServerRecord.payload);
    }

    @PostMapping(value = "/{aiServerId}/test-api")
    public String generateTestCaseFromRequirementUsingAi(
            @PathVariable long aiServerId, @RequestBody Map<String, String> payloadRequestBody) {
        return aiTestCaseGenerationService.testAiServerConfiguration(
                aiServerId, payloadRequestBody.get("requirementDescription"));
    }

    @PostMapping("/{aiServerId}/new-jsonpath")
    public void updateJsonPath(
            @PathVariable long aiServerId, @RequestBody AiServerRecord aiServerRecord) {
        aiServerManagerService.updateJsonPath(aiServerId, aiServerRecord.jsonPath);
    }

    record AiServerRecord(
            String name,
            String url,
            String description,
            String createdBy,
            Date createdOn,
            String lastModifiedBy,
            Date lastModifiedOn,
            String payload,
            String requirement,
            String jsonPath) {}
}
