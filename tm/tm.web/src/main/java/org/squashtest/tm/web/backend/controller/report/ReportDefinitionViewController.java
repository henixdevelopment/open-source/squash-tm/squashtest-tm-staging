/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.report;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.api.report.criteria.Criteria;
import org.squashtest.tm.api.utils.CurrentUserHelper;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.audit.AuditableMixin;
import org.squashtest.tm.domain.report.ReportDefinition;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;
import org.squashtest.tm.service.project.ProjectFinder;
import org.squashtest.tm.service.report.ReportModificationService;
import org.squashtest.tm.web.backend.helper.JsonHelper;
import org.squashtest.tm.web.backend.helper.ReportHelper;
import org.squashtest.tm.web.backend.report.IdentifiedReportDecorator;
import org.squashtest.tm.web.backend.report.ReportsRegistry;
import org.squashtest.tm.web.backend.report.criteria.ConciseFormToCriteriaConverter;

@RestController
@RequestMapping("backend/report-definition-view")
public class ReportDefinitionViewController {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(ReportDefinitionViewController.class);

    private final CustomReportLibraryNodeService customReportLibraryNodeService;
    private final ReportModificationService reportModificationService;
    private final ReportsRegistry reportsRegistry;
    private final ReportHelper reportHelper;
    private final ProjectFinder projectFinder;
    private final CurrentUserHelper currentUserHelper;

    public ReportDefinitionViewController(
            CustomReportLibraryNodeService customReportLibraryNodeService,
            ReportModificationService reportModificationService,
            ReportsRegistry reportsRegistry,
            ReportHelper reportHelper,
            ProjectFinder projectFinder,
            CurrentUserHelper currentUserHelper) {
        this.customReportLibraryNodeService = customReportLibraryNodeService;
        this.reportModificationService = reportModificationService;
        this.reportsRegistry = reportsRegistry;
        this.reportHelper = reportHelper;
        this.projectFinder = projectFinder;
        this.currentUserHelper = currentUserHelper;
    }

    @GetMapping(value = "/{customReportLibraryNodeId}")
    public ReportDefinitionViewModel getReportDefinitionViewModel(
            @PathVariable Long customReportLibraryNodeId) {
        ReportDefinition reportDefinition =
                customReportLibraryNodeService.findReportDefinitionByNodeId(customReportLibraryNodeId);
        return getViewModelFromDefinition(reportDefinition, customReportLibraryNodeId);
    }

    @RequestMapping(value = "/report-definition/{reportDefinitionId}")
    public ReportDefinitionViewModel getReportDefinitionViewModelByReportId(
            @PathVariable Long reportDefinitionId) {
        ReportDefinition reportDefinition = reportModificationService.findById(reportDefinitionId);
        return getViewModelFromDefinition(reportDefinition, null);
    }

    private ReportDefinitionViewModel getViewModelFromDefinition(
            ReportDefinition reportDefinition, Long customReportLibraryNodeId) {
        IdentifiedReportDecorator report =
                reportsRegistry.findReport(reportDefinition.getPluginNamespace());
        if (Objects.nonNull(report)) {
            Map<String, Object> form = null;
            try {
                form = JsonHelper.deserialize(reportDefinition.getParameters());
            } catch (IOException e) {
                LOGGER.error("the report '{}' has corrupted parameters.", reportDefinition.getName(), e);
            }
            List<Long> readableProjectIds = currentUserHelper.findReadableProjectIds();
            List<Long> projectIds = projectFinder.findAllProjectIdsOrderedByName(readableProjectIds);
            Map<String, Criteria> crit =
                    new ConciseFormToCriteriaConverter(report, projectIds).convert(form);
            return new ReportDefinitionViewModel(
                    reportDefinition,
                    customReportLibraryNodeId,
                    reportHelper.getAttributesForReport(report, crit, projectIds),
                    report);
        } else {
            return new ReportDefinitionViewModel(reportDefinition, customReportLibraryNodeId);
        }
    }

    static class ReportDefinitionViewModel {
        private final Long id;
        private final Long customReportLibraryNodeId;
        private final Long projectId;
        private final String name;
        private final String description;
        private final String summary;
        private final String pluginNamespace;
        private final String parameters;
        private Date createdOn;
        private String createdBy;
        private Date lastModifiedOn;
        private String lastModifiedBy;
        private Map<String, List<String>> attributes;
        private String reportLabel;
        private ReportData report;
        private boolean missingPlugin = true;

        public ReportDefinitionViewModel(
                ReportDefinition reportDefinition, Long customReportLibraryNodeId) {
            this.id = reportDefinition.getId();
            this.customReportLibraryNodeId = customReportLibraryNodeId;
            this.projectId = reportDefinition.getProject().getId();
            this.name = reportDefinition.getName();
            this.description = reportDefinition.getDescription();
            this.summary = reportDefinition.getSummary();
            this.pluginNamespace = reportDefinition.getPluginNamespace();
            this.parameters = reportDefinition.getParameters();
            this.attributes = new HashMap<>();
            doAuditableAttributes(reportDefinition);
        }

        public ReportDefinitionViewModel(
                ReportDefinition reportDefinition,
                Long customReportLibraryNodeId,
                Map<String, List<String>> reportAttributes,
                IdentifiedReportDecorator report) {
            this(reportDefinition, customReportLibraryNodeId);
            this.reportLabel = report.getLabel();
            this.missingPlugin = false;
            this.attributes = reportAttributes;
            this.report = new ReportData(report);
        }

        private void doAuditableAttributes(ReportDefinition def) {
            AuditableMixin audit = (AuditableMixin) def;
            this.createdBy = audit.getCreatedBy();
            this.lastModifiedBy = audit.getLastModifiedBy();
            this.createdOn = audit.getCreatedOn();
            this.lastModifiedOn = audit.getLastModifiedOn();
        }

        public Long getId() {
            return id;
        }

        public Long getCustomReportLibraryNodeId() {
            return customReportLibraryNodeId;
        }

        public Long getProjectId() {
            return projectId;
        }

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }

        public String getSummary() {
            return summary;
        }

        public String getPluginNamespace() {
            return pluginNamespace;
        }

        public String getParameters() {
            return parameters;
        }

        public Date getCreatedOn() {
            return createdOn;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public Date getLastModifiedOn() {
            return lastModifiedOn;
        }

        public String getLastModifiedBy() {
            return lastModifiedBy;
        }

        public Map<String, List<String>> getAttributes() {
            return attributes;
        }

        public String getReportLabel() {
            return reportLabel;
        }

        public ReportData getReport() {
            return report;
        }

        public boolean isMissingPlugin() {
            return missingPlugin;
        }
    }
}
