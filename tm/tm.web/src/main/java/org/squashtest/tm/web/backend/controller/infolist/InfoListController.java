/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.infolist;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.infolist.InfoList;
import org.squashtest.tm.domain.infolist.UserListItem;
import org.squashtest.tm.service.display.infolist.InfoListDisplayService;
import org.squashtest.tm.service.infolist.InfoListItemManagerService;
import org.squashtest.tm.service.infolist.InfoListManagerService;
import org.squashtest.tm.service.internal.display.dto.InfoListAdminViewDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.web.backend.controller.form.model.InfoListFormModel;

@RestController
@RequestMapping("/backend/info-lists")
public class InfoListController {

    private final InfoListDisplayService infoListDisplayService;
    private final InfoListManagerService infoListManagerService;
    private final InfoListItemManagerService infoListItemManagerService;

    @Inject
    InfoListController(
            InfoListDisplayService infoListDisplayService,
            InfoListManagerService infoListManagerService,
            InfoListItemManagerService infoListItemManagerService) {
        this.infoListDisplayService = infoListDisplayService;
        this.infoListManagerService = infoListManagerService;
        this.infoListItemManagerService = infoListItemManagerService;
    }

    /**
     * Returns a list of custom info-lists (those not created by 'system').
     *
     * @param request the grid request (pagination, sort...)
     * @return a grid response
     */
    @PostMapping
    public GridResponse getAllUserInfoLists(@RequestBody GridRequest request) {
        return infoListDisplayService.findAll(request);
    }

    /**
     * Deletes a custom info-list. Throws an IllegalAccessError if one of the IDs provided corresponds
     * to a system list (none is deleted).
     *
     * @param listIds the lists to delete
     */
    @DeleteMapping(value = "/{listIds}")
    public void deleteInfoLists(@PathVariable List<Long> listIds) {
        infoListManagerService.remove(listIds);
    }

    @PostMapping(value = "/new")
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String, Object> createInfoList(@RequestBody InfoListFormModel infoListFormModel) {
        InfoList infoList = infoListFormModel.getInfoList();
        infoListManagerService.persist(infoList);
        return Collections.singletonMap("id", infoList.getId());
    }

    @PostMapping(value = "/{infoListId}/label")
    public void changeLabel(@PathVariable long infoListId, @RequestBody InfoListPatch patch) {
        infoListManagerService.changeLabel(infoListId, patch.getLabel());
    }

    @PostMapping(value = "/{infoListId}/description")
    public void changeDescription(@PathVariable long infoListId, @RequestBody InfoListPatch patch) {
        infoListManagerService.changeDescription(infoListId, patch.getDescription());
    }

    @PostMapping(value = "/{infoListId}/code")
    public void changeCode(@PathVariable long infoListId, @RequestBody InfoListPatch patch) {
        infoListManagerService.changeCode(infoListId, patch.getCode());
    }

    public static class InfoListPatch {
        private String label;
        private String code;
        private String description;

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

    @PostMapping(value = "/{infoListId}/items/new")
    public InfoListAdminViewDto addItem(
            @PathVariable long infoListId, @RequestBody NewInfoListItem newItem) {
        infoListItemManagerService.checkCodePattern(newItem.getCode());
        infoListItemManagerService.checkIfCodeAlreadyExists(newItem.getCode());

        UserListItem item = new UserListItem();
        item.setCode(newItem.getCode());
        item.setLabel(newItem.getLabel());
        item.setColour(newItem.getColour());
        item.setIconName(newItem.getIconName());
        infoListItemManagerService.addInfoListItem(infoListId, item);

        return infoListDisplayService.getInfoListView(infoListId);
    }

    @GetMapping(value = "/check-if-item-code-already-exists/{code}")
    public void checkIfItemCodeAlreadyExists(@PathVariable String code) {
        infoListItemManagerService.checkIfCodeAlreadyExists(code);
    }

    public static class NewInfoListItem {
        String code;
        String label;
        String iconName;
        String colour;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getIconName() {
            return iconName;
        }

        public void setIconName(String iconName) {
            this.iconName = iconName;
        }

        public String getColour() {
            return colour;
        }

        public void setColour(String colour) {
            this.colour = colour;
        }
    }
}
