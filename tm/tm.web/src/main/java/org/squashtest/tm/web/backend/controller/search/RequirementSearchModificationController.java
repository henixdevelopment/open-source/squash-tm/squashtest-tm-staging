/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.search;

import java.util.List;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.requirement.RequirementBulkUpdate;
import org.squashtest.tm.service.requirement.RequirementVersionManagerService;

@RestController
@RequestMapping("backend/search/requirement")
public class RequirementSearchModificationController {

    private final RequirementVersionManagerService requirementVersionManagerService;

    public RequirementSearchModificationController(
            RequirementVersionManagerService requirementVersionManagerService) {
        this.requirementVersionManagerService = requirementVersionManagerService;
    }

    @PostMapping(value = "/mass-update/{requirementVersionsIds}", consumes = "application/json")
    public void massEditRequirementVersions(
            @PathVariable List<Long> requirementVersionsIds,
            @RequestBody RequirementBulkUpdate bulkUpdate) {
        requirementVersionManagerService.bulkUpdate(requirementVersionsIds, bulkUpdate);
    }
}
