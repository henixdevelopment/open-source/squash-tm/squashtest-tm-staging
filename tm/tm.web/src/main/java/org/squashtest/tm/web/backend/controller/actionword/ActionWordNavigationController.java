/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.actionword;

import static org.squashtest.tm.web.backend.controller.RequestParams.NODE_IDS;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.domain.NodeWorkspace;
import org.squashtest.tm.domain.actionword.ActionWordLibraryNode;
import org.squashtest.tm.exception.library.RightsUnsuficientsForOperationException;
import org.squashtest.tm.service.actionword.ActionWordLibraryNodeService;
import org.squashtest.tm.service.actionword.ActionWordWorkspaceService;
import org.squashtest.tm.service.deletion.Node;
import org.squashtest.tm.service.deletion.NodeMovement;
import org.squashtest.tm.service.deletion.NodeReferenceChanged;
import org.squashtest.tm.service.deletion.NodeRenaming;
import org.squashtest.tm.service.deletion.OperationReport;
import org.squashtest.tm.service.deletion.SuppressionPreviewReport;
import org.squashtest.tm.service.display.workspace.tree.SingleHierarchyTreeBrowser;
import org.squashtest.tm.service.display.workspace.tree.TreeNodeCollectorService;
import org.squashtest.tm.service.internal.display.dto.actionword.LinkedToKeywordStepPreviewReport;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.TreeGridResponse;
import org.squashtest.tm.service.internal.display.grid.TreeRequest;
import org.squashtest.tm.service.license.UltimateLicenseAvailabilityService;
import org.squashtest.tm.web.backend.controller.form.model.EntityFormModelValidator;
import org.squashtest.tm.web.backend.controller.form.model.NodeList;
import org.squashtest.tm.web.backend.controller.form.model.RefreshTreeNodeModel;
import org.squashtest.tm.web.backend.controller.navigation.Messages;

// ULTIMATE
/** This controller is dedicated to the operations in the tree of Action Word Workspace. */
@RestController
@RequestMapping("backend/action-word-tree")
public class ActionWordNavigationController {

    public static final Logger LOGGER = LoggerFactory.getLogger(ActionWordNavigationController.class);
    private static final String ADD_ACTION_WORD = "add-action-word";

    private final SingleHierarchyTreeBrowser treeBrowser;
    private final ActionWordLibraryNodeService actionWordLibraryNodeService;
    private final TreeNodeCollectorService treeNodeCollectorService;
    private final MessageSource messageSource;
    private final ActionWordWorkspaceService actionWordWorkspaceService;
    private final UltimateLicenseAvailabilityService ultimateLicenseService;

    @Autowired
    public ActionWordNavigationController(
            SingleHierarchyTreeBrowser treeBrowser,
            ActionWordLibraryNodeService actionWordLibraryNodeService,
            TreeNodeCollectorService treeNodeCollectorService,
            MessageSource messageSource,
            Optional<ActionWordWorkspaceService> actionWordWorkspaceService,
            UltimateLicenseAvailabilityService ultimateLicenseService) {
        this.treeBrowser = treeBrowser;
        this.actionWordLibraryNodeService = actionWordLibraryNodeService;
        this.treeNodeCollectorService = treeNodeCollectorService;
        this.messageSource = messageSource;
        this.actionWordWorkspaceService = actionWordWorkspaceService.orElse(null);
        this.ultimateLicenseService = ultimateLicenseService;
    }

    @PostMapping
    public TreeGridResponse getInitialRows(@RequestBody TreeRequest treeRequest) {
        ultimateLicenseService.checkIfAvailable();
        return treeBrowser.getInitialTree(
                NodeWorkspace.ACTION_WORD,
                NodeReference.fromNodeIds(treeRequest.getOpenedNodes()),
                NodeReference.fromNodeIds(treeRequest.getSelectedNodes()));
    }

    @GetMapping("/{ids}/content")
    public TreeGridResponse getChildren(@PathVariable List<String> ids) {
        ultimateLicenseService.checkIfAvailable();
        Set<NodeReference> nodeReference = NodeReference.fromNodeIds(ids);
        return treeBrowser.findSubHierarchy(nodeReference, new HashSet<>(nodeReference));
    }

    @PostMapping("/refresh")
    public TreeGridResponse refreshTree(@RequestBody RefreshTreeNodeModel refreshTreeNodeModel) {
        ultimateLicenseService.checkIfAvailable();
        return treeBrowser.findSubHierarchy(
                NodeReference.fromNodeIds(refreshTreeNodeModel.getNodeIds()),
                new HashSet<>(refreshTreeNodeModel.getNodeList().getNodeReferences()));
    }

    @PostMapping("/{ids}/refresh")
    public TreeGridResponse refreshNodes(
            @PathVariable List<String> ids, @RequestBody NodeList openedNodes) {
        ultimateLicenseService.checkIfAvailable();
        return treeBrowser.findSubHierarchy(
                NodeReference.fromNodeIds(ids), new HashSet<>(openedNodes.getNodeReferences()));
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("new-action-word")
    public DataRow addNewActionWord(@RequestBody ActionWordFormModel actionWordFormModel)
            throws BindException {
        ultimateLicenseService.checkIfAvailable();
        validateActionWordFormModel(actionWordFormModel);
        Long parentId = actionWordFormModel.getParentNodeReference().getId();
        ActionWordLibraryNode newNode =
                actionWordLibraryNodeService.createNewNode(parentId, actionWordFormModel.getActionWord());
        return treeNodeCollectorService.collectNode(NodeType.ACTION_WORD, newNode);
    }

    @PostMapping("/{destinationId}/content/paste")
    public void copyNodes(
            @RequestBody() NodeList nodeList, @PathVariable("destinationId") String destinationId) {
        NodeReference nodeReference = NodeReference.fromNodeId(destinationId);
        List<Long> copiedNodeIds = nodeList.getIds();

        try {
            if (Objects.requireNonNull(nodeReference.getNodeType()) == NodeType.ACTION_WORD_LIBRARY) {
                actionWordLibraryNodeService.copyNodes(copiedNodeIds, nodeReference.getId());
            } else {
                throw new IllegalArgumentException(
                        "copy nodes : specified destination type doesn't exists : "
                                + nodeReference.getNodeType());
            }
        } catch (AccessDeniedException ade) {
            throw new RightsUnsuficientsForOperationException(ade);
        }
    }

    @PostMapping("{destinationId}/content/paste-simulation")
    public boolean simulateCopyNodesToDrives(
            @RequestBody() NodeList nodeList, @PathVariable("destinationId") String destinationId) {
        NodeReference nodeReference = NodeReference.fromNodeId(destinationId);
        List<Long> copiedNodeIds = nodeList.getIds();

        return actionWordLibraryNodeService.simulateCopyNodes(copiedNodeIds, nodeReference.getId());
    }

    @PostMapping("/{destinationRef}/content/move")
    public void moveNodes(
            @RequestBody() NodeList nodeList, @PathVariable("destinationRef") String destinationRef) {
        ultimateLicenseService.checkIfAvailable();
        NodeReference nodeReference = NodeReference.fromNodeId(destinationRef);
        Long destinationId = nodeReference.getId();
        NodeType destinationType = nodeReference.getNodeType();
        List<Long> movedNodeIds = nodeList.getIds();

        try {
            if (Objects.requireNonNull(destinationType) == NodeType.ACTION_WORD_LIBRARY) {
                actionWordLibraryNodeService.moveNodes(movedNodeIds, destinationId);
            } else {
                throw new IllegalArgumentException(
                        "move nodes : specified destination type doesn't exists : " + destinationType);
            }
        } catch (AccessDeniedException ade) {
            throw new RightsUnsuficientsForOperationException(ade);
        }
    }

    @GetMapping("/deletion-simulation/{nodeIds}")
    public Messages simulateNodeDeletion(@PathVariable(NODE_IDS) List<Long> nodeIds, Locale locale) {
        List<LinkedToKeywordStepPreviewReport> reports =
                actionWordLibraryNodeService.previewAffectedNodes(nodeIds);
        List<SuppressionPreviewReport> reportList = new ArrayList<>(reports);
        Messages messages = new Messages();
        messages.addMessages(reportList, messageSource, locale);
        return messages;
    }

    @GetMapping("/are-all-associated-to-keyword-steps/{nodeIds}")
    public boolean areAllAssociatedToKeywordTestSteps(@PathVariable(NODE_IDS) Set<Long> nodeIds) {
        return getActionWordWorkspaceService().areAllAssociatedToKeywordTestSteps(nodeIds);
    }

    @DeleteMapping("/{nodeIds}")
    public OperationReport confirmNodeDeletion(@PathVariable(NODE_IDS) List<Long> nodeIds) {
        OperationReport report = actionWordLibraryNodeService.delete(nodeIds);
        logOperations(report);
        return report;
    }

    private void logOperations(OperationReport report) {
        for (Node deletedNode : report.getRemoved()) {
            LOGGER.info("The node #{} was removed", deletedNode.getResid());
        }
        for (NodeMovement movedNode : report.getMoved()) {
            LOGGER.info(
                    "The nodes #{} were moved to node #{}",
                    movedNode.getMoved().stream().map(Node::getResid).toList(),
                    movedNode.getDest().getResid());
        }
        for (NodeRenaming renamedNode : report.getRenamed()) {
            LOGGER.info(
                    "The node #{} was renamed to {}",
                    renamedNode.getNode().getResid(),
                    renamedNode.getName());
        }
        for (NodeReferenceChanged nodeReferenceChanged : report.getReferenceChanges()) {
            LOGGER.info(
                    "The node #{} reference was changed to {}",
                    nodeReferenceChanged.getNode().getResid(),
                    nodeReferenceChanged.getReference());
        }
    }

    private void validateActionWordFormModel(ActionWordFormModel actionWordFormModel)
            throws BindException {
        BindingResult validation = new BeanPropertyBindingResult(actionWordFormModel, ADD_ACTION_WORD);
        EntityFormModelValidator entityFormModelValidator = new EntityFormModelValidator();
        entityFormModelValidator.validate(actionWordFormModel, validation);

        if (validation.hasErrors()) {
            throw new BindException(validation);
        }
    }

    private ActionWordWorkspaceService getActionWordWorkspaceService() {
        if (Objects.isNull(actionWordWorkspaceService)) {
            throw new AccessDeniedException("A dedicated plugin is required to manage action words");
        }
        return actionWordWorkspaceService;
    }
}
