/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.project;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FileUtils;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.squashtest.tm.api.plugin.ConfigurablePlugin;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.projectimporter.PivotFormatImportType;
import org.squashtest.tm.domain.testautomation.TestAutomationProject;
import org.squashtest.tm.service.display.project.ProjectDisplayService;
import org.squashtest.tm.service.internal.display.dto.PartyProjectPermissionDto;
import org.squashtest.tm.service.internal.display.dto.PivotFormatImportDto;
import org.squashtest.tm.service.internal.display.dto.ProjectDataInfo;
import org.squashtest.tm.service.internal.display.dto.ProjectDto;
import org.squashtest.tm.service.internal.display.dto.TestAutomationProjectDto;
import org.squashtest.tm.service.internal.display.dto.party.UnboundPartiesResponse;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.dto.projectimporterxray.model.XrayImportModel;
import org.squashtest.tm.service.internal.dto.projectimporterxray.model.XrayInfoModel;
import org.squashtest.tm.service.project.CustomGenericProjectManager;
import org.squashtest.tm.service.project.GenericProjectManagerService;
import org.squashtest.tm.service.projectimporter.pivotimporter.GlobalProjectPivotImporterService;
import org.squashtest.tm.service.projectimporter.xrayimporter.GlobalProjectXrayImporterService;
import org.squashtest.tm.service.testautomation.TestAutomationProjectManagerService;
import org.squashtest.tm.web.backend.controller.RequestParams;
import org.squashtest.tm.web.backend.manager.plugin.ConfigurablePluginManager;

@RestController
@RequestMapping("/backend/generic-projects")
public class GenericProjectController {
    private static final Logger LOGGER = LoggerFactory.getLogger(GenericProjectController.class);

    private final ProjectDisplayService projectDisplayService;

    private final CustomGenericProjectManager customGenericProjectManager;

    private final GenericProjectManagerService genericProjectManagerService;

    private final TaskExecutor taskExecutor;

    private final TestAutomationProjectManagerService testAutomationProjectService;

    private final ConfigurablePluginManager configurablePluginManager;

    private final GlobalProjectPivotImporterService projectImporterService;

    private final GlobalProjectXrayImporterService projectImporterXrayService;

    @Inject
    public GenericProjectController(
            ProjectDisplayService projectDisplayService,
            GenericProjectManagerService projectManager,
            CustomGenericProjectManager projectManagerService,
            TaskExecutor taskExecutor,
            TestAutomationProjectManagerService testAutomationProjectService,
            ConfigurablePluginManager configurablePluginManager,
            GlobalProjectPivotImporterService projectImporterService,
            GlobalProjectXrayImporterService projectImporterXrayService) {
        this.projectDisplayService = projectDisplayService;
        this.genericProjectManagerService = projectManager;
        this.customGenericProjectManager = projectManagerService;
        this.taskExecutor = taskExecutor;
        this.testAutomationProjectService = testAutomationProjectService;
        this.configurablePluginManager = configurablePluginManager;
        this.projectImporterService = projectImporterService;
        this.projectImporterXrayService = projectImporterXrayService;
    }

    @PostMapping()
    public GridResponse getProjects(@RequestBody GridRequest request) {
        return projectDisplayService.findAll(request);
    }

    @GetMapping(value = "/templates")
    public Map<String, Object> getTemplates() {
        Map<String, Object> response = new HashMap<>();
        response.put("templates", projectDisplayService.getTemplateNamedReferences());
        return response;
    }

    @PostMapping("/{projectId}/permissions/{partyIds}/group/{groupId}")
    public Map<String, List<PartyProjectPermissionDto>> addNewPartyPermissions(
            @PathVariable long projectId, @PathVariable List<Long> partyIds, @PathVariable long groupId) {
        customGenericProjectManager.addNewPermissionToProject(partyIds, projectId, groupId);
        return Collections.singletonMap(
                "partyProjectPermissions", projectDisplayService.getPartyProjectPermissions(projectId));
    }

    @DeleteMapping("/{projectId}/permissions/{partyIds}")
    public void removePartyPermissions(
            @PathVariable long projectId, @PathVariable List<Long> partyIds) {
        customGenericProjectManager.removeProjectPermission(partyIds, projectId);
    }

    @PostMapping(value = "/{projectId}/bugtracker")
    public void changeBugtracker(
            @PathVariable long projectId, @RequestBody Map<String, Long> request) {
        if (request.get("bugtrackerId") != null) {
            genericProjectManagerService.changeBugTracker(projectId, request.get("bugtrackerId"));
        } else {
            genericProjectManagerService.removeBugTracker(projectId);
        }
    }

    @PostMapping(value = "/{projectId}/ai-server")
    public void changeAiServer(@PathVariable long projectId, @RequestBody Map<String, Long> request) {
        if (request.get("aiServerId") != null) {
            genericProjectManagerService.changeAiServer(projectId, request.get("aiServerId"));
        } else {
            genericProjectManagerService.unbindAiServer(projectId);
        }
    }

    @PostMapping(value = "/{projectId}/name")
    public void changeName(
            @PathVariable long projectId, @RequestBody GenericProjectController.ProjectPatch patch) {
        genericProjectManagerService.changeName(projectId, patch.getName());
    }

    @PostMapping(value = "/{projectId}/description")
    public void changeDescription(
            @PathVariable long projectId, @RequestBody GenericProjectController.ProjectPatch patch) {
        genericProjectManagerService.changeDescription(projectId, patch.getDescription());
    }

    @PostMapping(value = "/{projectId}/label")
    public void changeLabel(
            @PathVariable long projectId, @RequestBody GenericProjectController.ProjectPatch patch) {
        genericProjectManagerService.changeLabel(projectId, patch.getLabel());
    }

    @PostMapping(value = "/{projectId}/linked-template-id")
    public ProjectDto associateTemplate(
            @PathVariable long projectId, @RequestBody GenericProjectController.ProjectPatch patch) {
        final Long templateId = patch.getLinkedTemplateId();

        if (templateId == null) {
            genericProjectManagerService.disassociateFromTemplate(projectId);
        } else {
            genericProjectManagerService.associateToTemplate(
                    projectId, templateId, patch.getBoundTemplatePlugins());
        }

        return projectDisplayService.getProjectView(projectId);
    }

    @PostMapping(value = "/{projectId}/automation-workflow-type")
    public void changeAutomationWorkflowType(
            @PathVariable long projectId, @RequestBody GenericProjectController.ProjectPatch patch) {
        genericProjectManagerService.changeAutomationWorkflow(
                projectId, patch.getAutomationWorkflowType());
    }

    @PostMapping(value = "/{projectId}/bdd-implementation-technology")
    public void changeBddImplementationTechnology(
            @PathVariable long projectId, @RequestBody GenericProjectController.ProjectPatch patch) {
        genericProjectManagerService.changeBddImplTechnology(
                projectId, patch.getBddImplementationTechnology());
    }

    @PostMapping(value = "/{projectId}/bdd-script-language")
    public void changeBddScriptLanguage(
            @PathVariable long projectId, @RequestBody GenericProjectController.ProjectPatch patch) {
        genericProjectManagerService.changeBddScriptLanguage(projectId, patch.getBddScriptLanguage());
    }

    @PostMapping(value = "/{projectId}/scm-repository-id")
    public void changeScmServer(
            @PathVariable long projectId, @RequestBody GenericProjectController.ProjectPatch patch) {
        final Long repositoryId = patch.getScmRepositoryId();

        if (repositoryId == null) {
            genericProjectManagerService.unbindScmRepository(projectId);
        } else {
            genericProjectManagerService.bindScmRepository(projectId, repositoryId);
        }
    }

    @PostMapping(value = "/{projectId}/use-tree-structure-in-scm-repo")
    public void changeUseTreeStructureInScmRepo(
            @PathVariable long projectId, @RequestBody GenericProjectController.ProjectPatch patch) {
        genericProjectManagerService.changeUseTreeStructureInScmRepo(
                projectId, patch.isUseTreeStructureInScmRepo());
    }

    @PostMapping(value = "/{projectId}/ta-server-id")
    public void changeTestAutomationServer(
            @PathVariable long projectId, @RequestBody GenericProjectController.ProjectPatch patch) {
        genericProjectManagerService.bindTestAutomationServer(projectId, patch.getTaServerId());
    }

    @PostMapping(value = "/{projectId}/automated-suites-lifetime")
    public void changeAutomatedSuitesLifetime(
            @PathVariable long projectId, @RequestBody GenericProjectController.ProjectPatch patch) {
        genericProjectManagerService.changeAutomatedSuitesLifetime(
                projectId, patch.getAutomatedSuitesLifetime());
    }

    /**
     * Binds test automation projects (a.k.a jobs) to a TM project.
     *
     * @param projectId the TM project ID to bound jobs to
     * @param request the request body MUST contain a "taProject" field which contains an array of
     *     TestAutomationProjects to bind
     * @return the updated list of bound TA project for this TM project
     */
    @PostMapping(value = "/{projectId}/test-automation-projects/new")
    public Map<String, List<TestAutomationProjectDto>> addTestAutomationProject(
            @PathVariable long projectId, @RequestBody Map<String, TestAutomationProjectDto[]> request) {

        List<TestAutomationProject> taProjects =
                Arrays.stream(request.get("taProjects"))
                        .map(
                                projectDto -> {
                                    TestAutomationProject taProject = new TestAutomationProject();
                                    taProject.setJobName(projectDto.getRemoteName());
                                    taProject.setCanRunGherkin(projectDto.isCanRunBdd());
                                    taProject.setLabel(projectDto.getLabel());
                                    return taProject;
                                })
                        .toList();

        genericProjectManagerService.bindTestAutomationProjects(projectId, taProjects);
        return Collections.singletonMap(
                "taProjects", testAutomationProjectService.findAllByTMProject(projectId));
    }

    @RequestMapping(value = "/{projectId}/allow-tc-modif-during-exec")
    public void changeAllowTcModifDuringExec(
            @PathVariable long projectId, @RequestBody GenericProjectController.ProjectPatch patch) {
        genericProjectManagerService.changeAllowTcModifDuringExec(
                projectId, patch.isAllowTcModifDuringExec());
    }

    // This method enables and disables an optional execution status which is not already used within
    // project
    @PostMapping(value = "/{projectId}/change-execution-status/{executionStatus}")
    public void changeExecutionStatusOnProject(
            @PathVariable long projectId,
            @PathVariable String executionStatus,
            @RequestBody Map<String, Boolean> patch) {
        if (Boolean.TRUE.equals(patch.get(executionStatus.toUpperCase()))) {
            genericProjectManagerService.enableExecutionStatus(
                    projectId, ExecutionStatus.valueOf(executionStatus));
        } else {
            genericProjectManagerService.disableExecutionStatus(
                    projectId, ExecutionStatus.valueOf(executionStatus));
        }
    }

    // If the optional status is already used withing project, this method disables this optional
    // status and replaces all its
    // values by a new one selected by user.
    @PostMapping(
            value =
                    "/{projectId}/disable-and-replace-execution-status-within-project/{sourceExecutionStatus}")
    public void disableAndReplaceStatusWithinProject(
            @PathVariable long projectId,
            @PathVariable String sourceExecutionStatus,
            @RequestBody Map<String, String> targetExecutionStatus) {
        genericProjectManagerService.disableExecutionStatus(
                projectId, ExecutionStatus.valueOf(sourceExecutionStatus));

        ExecutionStatus source = ExecutionStatus.valueOf(sourceExecutionStatus);
        ExecutionStatus target =
                ExecutionStatus.valueOf(targetExecutionStatus.get("targetExecutionStatus"));
        Runnable replacer = new AsynchronousReplaceExecutionStatus(projectId, source, target);
        taskExecutor.execute(replacer);
    }

    @GetMapping(value = "/{projectId}/get-enabled-execution-status/{executionStatus}")
    public Map<String, Object> getEnabledExecutionStatus(
            @PathVariable long projectId, @PathVariable String executionStatus) {
        Set<ExecutionStatus> statuses =
                genericProjectManagerService.enabledExecutionStatuses(projectId);
        ExecutionStatus status = ExecutionStatus.valueOf(executionStatus);
        statuses.remove(status);
        List<Map<String, Object>> options = new ArrayList<>();
        for (ExecutionStatus st : statuses) {
            Map<String, Object> statusOption = new HashMap<>();
            statusOption.put("label", st.getI18nKey());
            statusOption.put("id", st.name());
            options.add(statusOption);
        }
        return Collections.singletonMap("statuses", options);
    }

    // ************************* plugins administration ***********************

    @PostMapping(value = "/{projectId}/plugins/{pluginId}")
    public Map<String, Boolean> enablePlugin(
            @PathVariable long projectId, @PathVariable String pluginId) {
        Optional<ConfigurablePlugin> plugin = configurablePluginManager.findById(pluginId);

        if (plugin.isPresent()) {
            genericProjectManagerService.enablePlugin(projectId, plugin.get());
            final boolean hasConfiguration =
                    genericProjectManagerService.pluginHasConfigurationOrSynchronisations(
                            plugin.get(), projectId);
            final boolean isValid = configurablePluginManager.isPluginBindingValid(pluginId, projectId);
            return Collections.singletonMap("hasValidConfiguration", hasConfiguration && isValid);
        } else {
            throw new IllegalArgumentException("Cannot find configurable plugin with ID " + pluginId);
        }
    }

    @DeleteMapping(
            value = "/{projectId}/plugins/{pluginId}",
            params = {"saveConf"})
    @Transactional
    public void disablePlugin(
            @PathVariable long projectId,
            @PathVariable String pluginId,
            @RequestParam("saveConf") Boolean saveConf) {
        Optional<ConfigurablePlugin> plugin = configurablePluginManager.findById(pluginId);

        if (plugin.isPresent()) {
            configurablePluginManager.disableConfigurablePlugin(pluginId, projectId, saveConf);
        } else {
            throw new IllegalArgumentException("Cannot find configurable plugin with ID " + pluginId);
        }
    }

    @PostMapping(value = "/{projectId}/has-data")
    public ProjectDataInfo getProjectDataInfoOnAction(
            @PathVariable Long projectId, @RequestBody Map<String, String> request) {
        ProjectDataInfo.ProjectAction action =
                ProjectDataInfo.ProjectAction.DELETION.name().equals(request.get("action"))
                        ? ProjectDataInfo.ProjectAction.DELETION
                        : ProjectDataInfo.ProjectAction.TRANSFORMATION;

        return projectDisplayService.getProjectDataInfoOnAction(projectId, action);
    }

    @PostMapping(value = "/{projectId}/create-import-request", produces = "application/json")
    @ResponseBody
    public Map<String, List<PivotFormatImportDto>> importProject(
            @PathVariable long projectId,
            @RequestParam(RequestParams.ARCHIVE) MultipartFile archive,
            @RequestParam(RequestParams.IMPORT_NAME) String importName,
            @RequestParam(RequestParams.IMPORT_TYPE) PivotFormatImportType importType) {
        projectImporterService.createImportRequest(projectId, archive, importName, importType);
        return Collections.singletonMap(
                "existingImports", projectDisplayService.getExistingImports(projectId));
    }

    @DeleteMapping(
            value = "/{projectId}/delete-import-request/{importIds}",
            produces = "application/json")
    public void deleteImportProject(
            @PathVariable long projectId, @PathVariable List<Long> importIds) {
        projectImporterService.deleteImportRequests(projectId, importIds);
    }

    @PostMapping(value = "/{projectId}/info-from-xray-xml")
    public XrayInfoModel getInfoFromXrayXml(
            @PathVariable long projectId, @RequestParam("archive") MultipartFile[] archives)
            throws IOException {
        return projectImporterXrayService.generateXrayInfoDtoFromXml(projectId, archives);
    }

    @PostMapping(value = "/{projectId}/import-from-xray-xml")
    public XrayImportModel importProjectXray(
            @PathVariable long projectId, @RequestParam("archive") MultipartFile[] archives)
            throws IOException {
        return projectImporterXrayService.generatePivotFromXml(projectId, archives);
    }

    @GetMapping(value = "/{projectId}/import-from-xray-xml/{filename:.+}")
    public FileSystemResource getPivotFileFromXray(
            @PathVariable long projectId, @PathVariable String filename, HttpServletResponse response) {
        File pivotFile;
        try {
            pivotFile = projectImporterXrayService.verifyXrayFile(projectId, filename);
            FileSystemResource fileSystemResource = new FileSystemResource(pivotFile);
            response.setContentType(RequestParams.APPLICATION_SLASH_OCTET_STREAM);
            response.setHeader(
                    RequestParams.CONTENT_DISPOSITION,
                    RequestParams.ATTACHMENT_FILENAME + pivotFile.getName());
            return fileSystemResource;
        } catch (IllegalAccessException e) {
            LOGGER.error("Error downloading pivot file", e);
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return null;
        } catch (IllegalArgumentException e) {
            LOGGER.error("Error downloading pivot file", e);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }
    }

    @DeleteMapping(
            value = "/{projectId}/import-from-xray-xml",
            params = {"pivotFile", "pivotLog"})
    public void deletePivotFileFromXray(
            @PathVariable Long projectId,
            @RequestParam("pivotFile") String pivotFileName,
            @RequestParam("pivotLog") String pivotLogName,
            HttpServletResponse response) {
        try {
            FileUtils.deleteQuietly(projectImporterXrayService.verifyXrayFile(projectId, pivotFileName));
            FileUtils.deleteQuietly(projectImporterXrayService.verifyXrayFile(projectId, pivotLogName));
        } catch (IllegalAccessException e) {
            LOGGER.error("Error during the cleanup of temporary pivot files", e);
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        } catch (IllegalArgumentException e) {
            LOGGER.error("Error during the cleanup of temporary pivot files", e);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    @GetMapping("/{genericProjectId}/unbound-parties")
    public UnboundPartiesResponse getUnboundParties(@PathVariable long genericProjectId) {
        return genericProjectManagerService.findPartyWithoutPermissionByProject(genericProjectId);
    }

    private class AsynchronousReplaceExecutionStatus implements Runnable {

        private final Long projectId;
        private final ExecutionStatus sourceExecutionStatus;
        private final ExecutionStatus targetExecutionStatus;

        public AsynchronousReplaceExecutionStatus(
                Long projectId,
                ExecutionStatus sourceExecutionStatus,
                ExecutionStatus targetExecutionStatus) {
            super();
            this.projectId = projectId;
            this.sourceExecutionStatus = sourceExecutionStatus;
            this.targetExecutionStatus = targetExecutionStatus;
        }

        @Override
        public void run() {
            genericProjectManagerService.replaceExecutionStepStatus(
                    projectId, sourceExecutionStatus, targetExecutionStatus);
        }
    }

    public static class ProjectPatch {
        private String name;
        private String description;
        private String label;
        private Long linkedTemplateId;
        private String automationWorkflowType;
        private Long scmRepositoryId;
        private boolean useTreeStructureInScmRepo;
        private Long taServerId;
        private boolean allowTcModifDuringExec;
        private Map<String, Boolean> allowedStatuses;
        private String automatedSuitesLifetime;
        private String bddImplementationTechnology;
        private String bddScriptLanguage;
        private List<String> boundTemplatePlugins;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public Long getLinkedTemplateId() {
            return linkedTemplateId;
        }

        public void setLinkedTemplateId(Long linkedTemplateId) {
            this.linkedTemplateId = linkedTemplateId;
        }

        public String getAutomationWorkflowType() {
            return automationWorkflowType;
        }

        public void setAutomationWorkflowType(String automationWorkflowType) {
            this.automationWorkflowType = automationWorkflowType;
        }

        public Long getScmRepositoryId() {
            return scmRepositoryId;
        }

        public void setScmRepositoryId(Long scmRepositoryId) {
            this.scmRepositoryId = scmRepositoryId;
        }

        public boolean isUseTreeStructureInScmRepo() {
            return useTreeStructureInScmRepo;
        }

        public void setUseTreeStructureInScmRepo(boolean useTreeStructureInScmRepo) {
            this.useTreeStructureInScmRepo = useTreeStructureInScmRepo;
        }

        public Long getTaServerId() {
            return taServerId;
        }

        public void setTaServerId(Long taServerId) {
            this.taServerId = taServerId;
        }

        public boolean isAllowTcModifDuringExec() {
            return allowTcModifDuringExec;
        }

        public void setAllowTcModifDuringExec(boolean allowTcModifDuringExec) {
            this.allowTcModifDuringExec = allowTcModifDuringExec;
        }

        public Map<String, Boolean> getAllowedStatuses() {
            return allowedStatuses;
        }

        public void setAllowedStatuses(Map<String, Boolean> allowedStatuses) {
            this.allowedStatuses = allowedStatuses;
        }

        public String getAutomatedSuitesLifetime() {
            return automatedSuitesLifetime;
        }

        public void setAutomatedSuitesLifetime(String automatedSuitesLifetime) {
            this.automatedSuitesLifetime = automatedSuitesLifetime;
        }

        public String getBddImplementationTechnology() {
            return bddImplementationTechnology;
        }

        public void setBddImplementationTechnology(String bddImplementationTechnology) {
            this.bddImplementationTechnology = bddImplementationTechnology;
        }

        public String getBddScriptLanguage() {
            return bddScriptLanguage;
        }

        public void setBddScriptLanguage(String bddScriptLanguage) {
            this.bddScriptLanguage = bddScriptLanguage;
        }

        public List<String> getBoundTemplatePlugins() {
            return boundTemplatePlugins;
        }

        public void setBoundTemplatePlugins(List<String> boundTemplatePlugins) {
            this.boundTemplatePlugins = boundTemplatePlugins;
        }
    }
}
