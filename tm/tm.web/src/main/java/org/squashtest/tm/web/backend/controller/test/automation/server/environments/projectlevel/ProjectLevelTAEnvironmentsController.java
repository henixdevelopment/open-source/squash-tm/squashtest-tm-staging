/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.test.automation.server.environments.projectlevel;

import java.util.Objects;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.display.test.automation.server.TestAutomationServerDisplayService;
import org.squashtest.tm.service.internal.display.dto.automatedexecutionenvironments.EnvironmentSelectionPanelDto;
import org.squashtest.tm.service.project.CustomProjectModificationService;
import org.squashtest.tm.service.project.GenericProjectFinder;
import org.squashtest.tm.service.servers.StoredCredentialsManager;
import org.squashtest.tm.service.testautomation.environment.AutomatedExecutionEnvironmentService;
import org.squashtest.tm.web.backend.controller.form.model.StringList;
import org.squashtest.tm.web.backend.controller.test.automation.server.environments.AbstractTAEnvironmentsController;

/**
 * Controller for operations on automated execution environment at project-level. Restricted to
 * project managers and administrators (see endpoint configuration in {@link
 * org.squashtest.tm.web.config.WebSecurityConfig}).
 */
@RestController
@RequestMapping("/backend/generic-projects/")
public class ProjectLevelTAEnvironmentsController extends AbstractTAEnvironmentsController {

    private final CustomProjectModificationService customProjectModificationService;

    public ProjectLevelTAEnvironmentsController(
            AutomatedExecutionEnvironmentService automatedExecutionEnvironmentService,
            CustomProjectModificationService customProjectModificationService,
            StoredCredentialsManager storedCredentialsManager,
            GenericProjectFinder genericProjectFinder,
            TestAutomationServerDisplayService testAutomationServerDisplayService) {
        super(
                automatedExecutionEnvironmentService,
                storedCredentialsManager,
                genericProjectFinder,
                testAutomationServerDisplayService);
        this.customProjectModificationService = customProjectModificationService;
    }

    @GetMapping(value = "{projectId}/available-tags")
    public StringList getAllAvailableProjectEnvironmentTags(@PathVariable long projectId) {
        final Long testAutomationServerId = getTestAutomationServerId(projectId);
        return getAvailableProjectEnvironmentTags(projectId, testAutomationServerId);
    }

    @GetMapping(value = "{projectId}/automated-execution-environments/all")
    public EnvironmentSelectionPanelDto getProjectEnvironments(@PathVariable Long projectId) {
        final Long testAutomationServerId = getTestAutomationServerId(projectId);
        return getProjectEnvironmentsPanel(projectId, testAutomationServerId);
    }

    @PostMapping(value = "{projectId}/environment-tags")
    public void updateProjectEnvironmentTags(
            @PathVariable Long projectId, @RequestBody StringList tagList) {
        customProjectModificationService.overrideEnvironmentTags(projectId, tagList.getList());
    }

    @DeleteMapping(value = "{projectId}/environment-tags")
    public void clearProjectTagOverrides(@PathVariable Long projectId) {
        customProjectModificationService.clearEnvironmentTagOverrides(projectId);
    }

    @PostMapping("{projectId}/automated-execution-environments/tokens/{serverId}")
    public void changeTAServerTokenOverride(
            @PathVariable Long projectId,
            @PathVariable Long serverId,
            @RequestBody TokenOverrideRequestBody body) {
        customProjectModificationService.setTestAutomationServerTokenOverride(
                projectId, serverId, body.token);
    }

    @DeleteMapping("{projectId}/automated-execution-environments/tokens/{serverId}")
    public void clearTAServerTokenOverride(
            @PathVariable Long projectId, @PathVariable Long serverId) {
        customProjectModificationService.clearTestAutomationServerTokenOverride(projectId, serverId);
    }

    public static class TokenOverrideRequestBody {
        private String token;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
    }

    private Long getTestAutomationServerId(long genericProjectId) {
        return Objects.requireNonNull(
                testAutomationServerDisplayService.findTestAutomationServerIdByGenericProjectId(
                        genericProjectId),
                String.format(
                        "Expect project with ID %d to have a test automation server bound.", genericProjectId));
    }
}
