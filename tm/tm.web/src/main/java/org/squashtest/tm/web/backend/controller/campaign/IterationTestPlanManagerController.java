/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.campaign;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.service.campaign.CreatedTestPlanItems;
import org.squashtest.tm.service.campaign.IterationModificationService;
import org.squashtest.tm.service.campaign.IterationTestPlanManagerService;
import org.squashtest.tm.service.campaign.TestSuiteTestPlanManagerService;
import org.squashtest.tm.service.display.campaign.IterationDisplayService;
import org.squashtest.tm.service.display.campaign.IterationTestPlanDisplayService;
import org.squashtest.tm.service.internal.bugtracker.knownissues.local.IterationKnownIssueFinder;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;

@RestController
@RequestMapping("backend/iteration")
public class IterationTestPlanManagerController {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(IterationTestPlanManagerController.class);

    private final IterationTestPlanManagerService iterationTestPlanManagerService;

    private final IterationModificationService iterationModificationService;

    private final IterationTestPlanDisplayService iterationTestPlanDisplayService;

    private final TestSuiteTestPlanManagerService testSuiteTestPlanManagerService;

    private final IterationDisplayService iterationDisplayService;

    private final IterationKnownIssueFinder iterationKnownIssueFinder;

    public IterationTestPlanManagerController(
            IterationTestPlanManagerService iterationTestPlanManagerService,
            IterationModificationService iterationModificationService,
            IterationTestPlanDisplayService iterationTestPlanDisplayService,
            TestSuiteTestPlanManagerService testSuiteTestPlanManagerService,
            IterationDisplayService iterationDisplayService,
            IterationKnownIssueFinder iterationKnownIssueFinder) {
        this.iterationTestPlanManagerService = iterationTestPlanManagerService;
        this.iterationModificationService = iterationModificationService;
        this.iterationTestPlanDisplayService = iterationTestPlanDisplayService;
        this.testSuiteTestPlanManagerService = testSuiteTestPlanManagerService;
        this.iterationDisplayService = iterationDisplayService;
        this.iterationKnownIssueFinder = iterationKnownIssueFinder;
    }

    @PostMapping(value = "/{iterationId}/test-plan")
    public GridResponse findIterationTestPlan(
            @PathVariable Long iterationId, @RequestBody GridRequest gridRequest) {
        return this.iterationDisplayService.findTestPlan(iterationId, gridRequest);
    }

    @PostMapping(value = "/{iterationId}/test-plan/{itemId}/executions")
    public GridResponse findIterationTestPlanExecutions(
            @PathVariable("iterationId") Long iterationId,
            @PathVariable("itemId") long itemId,
            @RequestBody GridRequest gridRequest) {
        return this.iterationTestPlanDisplayService.getITPIExecutions(iterationId, itemId, gridRequest);
    }

    @PostMapping(value = "/{iterationId}/test-plan-items")
    public CreatedTestPlanItems addTestCasesToIteration(
            @RequestBody TestCaseIdForm testCaseIdForm, @PathVariable long iterationId) {
        return iterationTestPlanManagerService.addTestCasesToIteration(
                testCaseIdForm.getTestCaseIds(), iterationId);
    }

    @DeleteMapping(value = "/{iterationId}/test-plan/{testPlanItemsIds}")
    public DeletedTestPlanItemReport removeTestPlanItemsFromIteration(
            @PathVariable("testPlanItemsIds") List<Long> testPlanItemsIds,
            @PathVariable long iterationId) {
        DeletedTestPlanItemReport report = new DeletedTestPlanItemReport();
        boolean hasTestPlanItemNotDeleted =
                iterationTestPlanManagerService.removeTestPlansFromIteration(testPlanItemsIds, iterationId);
        report.setHasTestPlanItemNotDeleted(hasTestPlanItemNotDeleted);
        report.setNbIssues(iterationKnownIssueFinder.countKnownIssues(iterationId));
        return report;
    }

    @DeleteMapping(value = "/{iterationId}/test-plan/execution/{executionIds}")
    public DeletedExecutionFromTestPlanItemReport removeExecutionsFromTestPlanItem(
            @PathVariable("executionIds") List<Long> executionIds, @PathVariable long iterationId) {
        iterationTestPlanManagerService.removeExecutionsFromTestPlanItem(executionIds, iterationId);
        DeletedExecutionFromTestPlanItemReport report = new DeletedExecutionFromTestPlanItemReport();
        report.setNbIssues(iterationKnownIssueFinder.countKnownIssues(iterationId));
        return report;
    }

    @PostMapping(value = "/{iterationId}/test-plan/{testPlanItemId}/executions/new-manual")
    public Map<String, Object> addManualExecution(
            @PathVariable long testPlanItemId, @PathVariable long iterationId) {
        LOGGER.trace("Add manual execution : creating execution");
        Execution newExecution = iterationModificationService.addManualExecution(testPlanItemId);
        LOGGER.trace("Add manual execution : completed in");
        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put("executionId", newExecution.getId());
        return responseBody;
    }

    @PostMapping(value = "/{iterationId}/test-plan/copy-items")
    public void addIterationTestPlanItemToIteration(
            @RequestBody IterationTestPlanItemPatch iterationTestPlanItemPatch,
            @PathVariable long iterationId) {
        iterationTestPlanManagerService.copyTestPlanItems(
                iterationTestPlanItemPatch.getItemTestPlanIds(), iterationId);
    }

    @PostMapping(value = "/test-plan/{itemIds}/mass-update")
    public void massUpdate(
            @PathVariable("itemIds") List<Long> itemIds, @RequestBody ItpiMassEditPatch patch) {
        if (patch.getExecutionStatus() != null) {
            iterationTestPlanManagerService.forceExecutionStatus(itemIds, patch.getExecutionStatus());
        }

        if (patch.isChangeTestSuites()) {
            testSuiteTestPlanManagerService.unbindAllTestPlansFromTestPlanItem(itemIds);
            testSuiteTestPlanManagerService.bindTestPlanToMultipleSuites(patch.getTestSuites(), itemIds);
        }

        if (patch.isChangeAssignee()) {
            if (patch.getAssignee() == null) {
                iterationTestPlanManagerService.removeTestPlanItemsAssignments(itemIds);
            } else {
                iterationTestPlanManagerService.assignUserToTestPlanItems(itemIds, patch.getAssignee());
            }
        }
    }

    static class IterationTestPlanItemPatch {
        private String executionStatus;
        private Long assignee;
        private Long datasetId;
        private List<Long> testSuites;
        private List<Long> itemTestPlanIds = new ArrayList<>();

        public Long getDatasetId() {
            return datasetId;
        }

        public void setDatasetId(Long datasetId) {
            this.datasetId = datasetId;
        }

        public Long getAssignee() {
            return assignee;
        }

        public void setAssignee(Long assignee) {
            this.assignee = assignee;
        }

        public String getExecutionStatus() {
            return executionStatus;
        }

        public void setExecutionStatus(String executionStatus) {
            this.executionStatus = executionStatus;
        }

        public List<Long> getTestSuites() {
            return testSuites;
        }

        public void setTestSuites(List<Long> testSuites) {
            this.testSuites = testSuites;
        }

        public List<Long> getItemTestPlanIds() {
            return itemTestPlanIds;
        }

        public void setItemTestPlanIds(List<Long> itemTestPlanIds) {
            this.itemTestPlanIds = itemTestPlanIds;
        }
    }

    static class ItpiMassEditPatch extends IterationTestPlanItemPatch {
        private boolean changeAssignee;
        private boolean changeTestSuites;

        public boolean isChangeAssignee() {
            return changeAssignee;
        }

        public void setChangeAssignee(boolean changeAssignee) {
            this.changeAssignee = changeAssignee;
        }

        public boolean isChangeTestSuites() {
            return changeTestSuites;
        }

        public void setChangeTestSuites(boolean changeTestSuites) {
            this.changeTestSuites = changeTestSuites;
        }
    }

    /**
     * Handles internal drag and drop into grid
     *
     * @param iterationId : the iteration owning the moving test plan items
     * @param itemIds the ids of the items we are trying to move
     * @param newIndex the new position of the first of them
     */
    @PostMapping(value = "/{iterationId}/test-plan/{itemIds}/position/{newIndex}")
    public void moveTestPlanItems(
            @PathVariable long iterationId,
            @PathVariable List<Long> itemIds,
            @PathVariable int newIndex) {
        iterationTestPlanManagerService.changeTestPlanPosition(iterationId, newIndex, itemIds);
    }

    static class DeletedTestPlanItemReport {
        private long nbIssues;
        private boolean hasTestPlanItemNotDeleted;

        public long getNbIssues() {
            return nbIssues;
        }

        public void setNbIssues(long nbIssues) {
            this.nbIssues = nbIssues;
        }

        @JsonProperty("hasTestPlanItemNotDeleted")
        public boolean hasTestPlanItemNotDeleted() {
            return hasTestPlanItemNotDeleted;
        }

        public void setHasTestPlanItemNotDeleted(boolean hasTestPlanItemNotDeleted) {
            this.hasTestPlanItemNotDeleted = hasTestPlanItemNotDeleted;
        }
    }

    static class DeletedExecutionFromTestPlanItemReport {
        private long nbIssues;

        public long getNbIssues() {
            return nbIssues;
        }

        public void setNbIssues(long nbIssues) {
            this.nbIssues = nbIssues;
        }
    }
}
