/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.test.automation.server.environments.serverlevel;

import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.service.display.test.automation.server.TestAutomationServerDisplayService;
import org.squashtest.tm.service.internal.display.dto.automatedexecutionenvironments.EnvironmentSelectionPanelDto;
import org.squashtest.tm.service.project.GenericProjectFinder;
import org.squashtest.tm.service.servers.StoredCredentialsManager;
import org.squashtest.tm.service.testautomation.TestAutomationServerManagerService;
import org.squashtest.tm.service.testautomation.environment.AutomatedExecutionEnvironmentService;
import org.squashtest.tm.service.testautomation.model.AutomatedExecutionEnvironment;
import org.squashtest.tm.service.testautomation.spi.InvalidSquashOrchestratorConfigurationException;
import org.squashtest.tm.service.testautomation.spi.OutdatedSquashOrchestratorException;
import org.squashtest.tm.service.testautomation.spi.TestAutomationException;
import org.squashtest.tm.web.backend.controller.form.model.StringList;
import org.squashtest.tm.web.backend.controller.test.automation.server.environments.AbstractTAEnvironmentsController;

/**
 * Controller for operations on automated execution environment at server-level. Restricted to
 * administrators (see endpoint configuration in {@link
 * org.squashtest.tm.web.config.WebSecurityConfig}).
 */
@RestController
@RequestMapping("/backend/test-automation-servers/")
public class ServerLevelTAEnvironmentsController extends AbstractTAEnvironmentsController {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(ServerLevelTAEnvironmentsController.class);

    private final TestAutomationServerManagerService testAutomationServerManagerService;

    public ServerLevelTAEnvironmentsController(
            AutomatedExecutionEnvironmentService automatedExecutionEnvironmentService,
            TestAutomationServerDisplayService testAutomationServerDisplayService,
            TestAutomationServerManagerService testAutomationServerManagerService,
            StoredCredentialsManager storedCredentialsManager,
            GenericProjectFinder genericProjectFinder) {
        super(
                automatedExecutionEnvironmentService,
                storedCredentialsManager,
                genericProjectFinder,
                testAutomationServerDisplayService);
        this.testAutomationServerManagerService = testAutomationServerManagerService;
    }

    @GetMapping(value = "{testAutomationServerId}/automated-execution-environments/all")
    public EnvironmentSelectionPanelDto getServerEnvironmentsPanel(
            @PathVariable Long testAutomationServerId) {
        if (testAutomationServerDisplayService.hasDefinedCredentials(testAutomationServerId)) {
            final List<String> defaultTags =
                    testAutomationServerDisplayService.getDefaultEnvironmentTags(testAutomationServerId);
            List<AutomatedExecutionEnvironment> allAccessibleEnvironments = null;

            try {
                allAccessibleEnvironments =
                        automatedExecutionEnvironmentService.getAllAccessibleEnvironments(
                                testAutomationServerId);
            } catch (InvalidSquashOrchestratorConfigurationException
                    | OutdatedSquashOrchestratorException ex) {
                LOGGER.error(
                        String.format(
                                "Could not fetch available automated execution environments for server %d.",
                                testAutomationServerId),
                        ex);
                return EnvironmentSelectionPanelDto.forServerWithError(
                        testAutomationServerId, defaultTags, ex.getMessage());
            } catch (TestAutomationException ex) {
                if (LOGGER.isTraceEnabled()) {
                    LOGGER.trace(
                            String.format(
                                    "Could not fetch available automated execution environments for server %d.",
                                    testAutomationServerId),
                            ex);
                }
            }
            return EnvironmentSelectionPanelDto.forServer(
                    testAutomationServerId, defaultTags, allAccessibleEnvironments);
        } else {
            return EnvironmentSelectionPanelDto.forServerWithoutCredentials(testAutomationServerId);
        }
    }

    @GetMapping(value = "{testAutomationServerId}/available-tags")
    public StringList getAvailableServerEnvironmentTags(@PathVariable Long testAutomationServerId) {
        return getAvailableServerEnvironmentTags(testAutomationServerId, null);
    }

    @PostMapping(value = "{testAutomationServerId}/environment-tags")
    public void updateServerEnvironmentTags(
            @PathVariable Long testAutomationServerId, @RequestBody StringList tagList) {
        testAutomationServerManagerService.updateEnvironmentTags(
                testAutomationServerId, tagList.getList());
    }
}
