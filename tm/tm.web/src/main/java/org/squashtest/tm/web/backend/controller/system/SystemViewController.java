/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.system;

import static org.squashtest.tm.api.report.DocxTemplaterReport.DOCX_FILE_EXTENSION;
import static org.squashtest.tm.service.configuration.ConfigurationService.Properties.AUTOCONNECT_ON_CONNECTION_ENABLED;
import static org.squashtest.tm.service.configuration.ConfigurationService.Properties.BANNER_MESSAGE;
import static org.squashtest.tm.service.configuration.ConfigurationService.Properties.CASE_INSENSITIVE_ACTIONS_FEATURE_ENABLED;
import static org.squashtest.tm.service.configuration.ConfigurationService.Properties.CASE_INSENSITIVE_LOGIN_FEATURE_ENABLED;
import static org.squashtest.tm.service.configuration.ConfigurationService.Properties.IMPORT_SIZE_LIMIT;
import static org.squashtest.tm.service.configuration.ConfigurationService.Properties.LOGIN_MESSAGE;
import static org.squashtest.tm.service.configuration.ConfigurationService.Properties.SQUASH_CALLBACK_URL;
import static org.squashtest.tm.service.configuration.ConfigurationService.Properties.STACK_TRACE_FEATURE_ENABLED;
import static org.squashtest.tm.service.configuration.ConfigurationService.Properties.UNSAFE_ATTACHMENT_PREVIEW_FEATURE_ENABLED;
import static org.squashtest.tm.service.configuration.ConfigurationService.Properties.UPLOAD_EXTENSIONS_WHITELIST;
import static org.squashtest.tm.service.configuration.ConfigurationService.Properties.UPLOAD_SIZE_LIMIT;
import static org.squashtest.tm.service.configuration.ConfigurationService.Properties.WELCOME_MESSAGE;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import javax.validation.constraints.NotNull;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.api.report.DocxTemplaterReport;
import org.squashtest.tm.api.wizard.SynchronisationPlugin;
import org.squashtest.tm.core.foundation.sanitizehtml.HTMLSanitizeUtils;
import org.squashtest.tm.domain.synchronisation.RemoteSynchronisation;
import org.squashtest.tm.service.actionword.ActionWordLibraryNodeService;
import org.squashtest.tm.service.configuration.ConfigurationService;
import org.squashtest.tm.service.internal.display.dto.RemoteSynchronisationDto;
import org.squashtest.tm.service.internal.display.dto.ReportTemplateModel;
import org.squashtest.tm.service.internal.display.dto.SynchronisationPluginViewDto;
import org.squashtest.tm.service.internal.display.dto.SystemViewDto;
import org.squashtest.tm.service.internal.dto.UserDto;
import org.squashtest.tm.service.project.CustomProjectFinder;
import org.squashtest.tm.service.system.LogFileDownloadService;
import org.squashtest.tm.service.system.SystemAdministrationService;
import org.squashtest.tm.service.user.UserAccountService;
import org.squashtest.tm.service.user.UserManagerService;
import org.squashtest.tm.web.backend.report.IdentifiedReportDecorator;
import org.squashtest.tm.web.backend.report.ReportsRegistry;

@RestController
@RequestMapping("/backend/system-view")
public class SystemViewController {
    private final SystemAdministrationService systemAdministrationService;
    private final ConfigurationService configurationService;
    private final UserManagerService userManagerService;
    private final ActionWordLibraryNodeService actionWordLibraryNodeService;
    private final ReportsRegistry reportsRegistry;
    private final UserAccountService userAccountService;
    private final CustomProjectFinder customProjectFinder;
    private final LogFileDownloadService logFileDownloadService;

    @Value("${info.app.version}")
    private String appVersion;

    @Value("${squashtm.stack.trace.control.panel.visible:false}")
    private Boolean stackTracePanelIsVisible;

    @Autowired(required = false)
    private Collection<SynchronisationPlugin> synchronisationPlugins = Collections.emptyList();

    SystemViewController(
            SystemAdministrationService administrationService,
            ConfigurationService configurationService,
            UserManagerService userManagerService,
            ActionWordLibraryNodeService actionWordLibraryNodeService,
            ReportsRegistry reportsRegistry,
            UserAccountService userAccountService,
            CustomProjectFinder customProjectFinder,
            LogFileDownloadService logFileDownloadService) {
        this.systemAdministrationService = administrationService;
        this.configurationService = configurationService;
        this.userManagerService = userManagerService;
        this.actionWordLibraryNodeService = actionWordLibraryNodeService;
        this.reportsRegistry = reportsRegistry;
        this.userAccountService = userAccountService;
        this.customProjectFinder = customProjectFinder;
        this.logFileDownloadService = logFileDownloadService;
    }

    @GetMapping
    public SystemViewDto getSystemView() {
        SystemViewDto dto = new SystemViewDto();
        UserDto currentUser = userAccountService.findCurrentUserDto();
        if (currentUser.isAdmin()) {
            Map<String, String> coreConfig = configurationService.findAllConfiguration();
            dto.setStatistics(systemAdministrationService.findAdministrationStatistics());
            dto.setAppVersion(appVersion);
            dto.setPlugins(systemAdministrationService.findAllPluginsAtStart());
            dto.setWhiteList(coreConfig.get(UPLOAD_EXTENSIONS_WHITELIST));
            dto.setUploadSizeLimit(coreConfig.get(UPLOAD_SIZE_LIMIT));
            dto.setImportSizeLimit(coreConfig.get(IMPORT_SIZE_LIMIT));
            dto.setCallbackUrl(coreConfig.get(SQUASH_CALLBACK_URL));
            dto.setStackTracePanelIsVisible(stackTracePanelIsVisible);
            dto.setStackTraceFeatureIsEnabled(
                    Boolean.parseBoolean(coreConfig.get(STACK_TRACE_FEATURE_ENABLED)));
            dto.setAutoconnectOnConnection(
                    Boolean.parseBoolean(coreConfig.get(AUTOCONNECT_ON_CONNECTION_ENABLED)));
            dto.setCaseInsensitiveLogin(
                    Boolean.parseBoolean(coreConfig.get(CASE_INSENSITIVE_LOGIN_FEATURE_ENABLED)));
            dto.setDuplicateLogins(userManagerService.findAllDuplicateLogins());
            dto.setCaseInsensitiveActions(
                    Boolean.parseBoolean(coreConfig.get(CASE_INSENSITIVE_ACTIONS_FEATURE_ENABLED)));
            dto.setDuplicateActions(
                    actionWordLibraryNodeService.findAllCaseInsensitiveDuplicateActions());
            dto.setWelcomeMessage(HTMLSanitizeUtils.cleanHtml(coreConfig.get(WELCOME_MESSAGE)));
            dto.setLoginMessage(HTMLSanitizeUtils.cleanHtml(coreConfig.get(LOGIN_MESSAGE)));
            dto.setBannerMessage(HTMLSanitizeUtils.cleanHtml(coreConfig.get(BANNER_MESSAGE)));
            dto.setLogFiles(logFileDownloadService.getAllPreviousLogFileNames());
            dto.setLicenseInfo(systemAdministrationService.getBasicLicenseInfo());
            dto.setReportTemplateModels(getTemplateModels());
            dto.setUnsafeAttachmentPreviewEnabled(
                    Boolean.parseBoolean(coreConfig.get(UNSAFE_ATTACHMENT_PREVIEW_FEATURE_ENABLED)));
        }

        appendSyncPluginsInfo(currentUser, dto);

        return dto;
    }

    private void appendSyncPluginsInfo(UserDto currentUser, SystemViewDto systemViewDto) {
        List<SynchronisationPluginViewDto> syncPluginDtos = new ArrayList<>();

        if (!synchronisationPlugins.isEmpty()) {
            List<Long> manageableProjectIds = customProjectFinder.findAllManageableIds(currentUser);

            if (!currentUser.isAdmin() && manageableProjectIds.isEmpty()) {
                throw new AccessDeniedException("No projects are manageable");
            }

            synchronisationPlugins.forEach(
                    syncPlugin -> {
                        SynchronisationPluginViewDto syncPluginDto = new SynchronisationPluginViewDto();
                        syncPluginDto.setId(syncPlugin.getId());
                        syncPluginDto.setName(syncPlugin.getName());
                        syncPluginDto.setRemoteSynchronisations(
                                findRemoteSynchronisationsForPlugin(manageableProjectIds, syncPlugin));
                        syncPluginDto.setHasSupervisionScreen(syncPlugin.hasSupervisionScreen());
                        syncPluginDto.setSupervisionScreenData(syncPlugin.getSupervisionScreenData());
                        syncPluginDtos.add(syncPluginDto);
                    });
        }

        systemViewDto.setSynchronisationPlugins(syncPluginDtos);
    }

    @GetMapping("/current-active-users-count")
    public Map<String, Integer> getCurrentActiveUsersCount() {
        Map<String, Integer> responseBody = new HashMap<>();
        int currentActiveUsersCount = userManagerService.countAllActiveUsers();
        responseBody.put("currentActiveUsersCount", currentActiveUsersCount);
        return responseBody;
    }

    private List<ReportTemplateModel> getTemplateModels() {
        List<ReportTemplateModel> reportTemplateModels = new ArrayList<>();

        this.reportsRegistry.getSortedReports().stream()
                .filter(IdentifiedReportDecorator::isDocxTemplate)
                .forEach(
                        reportDecorator -> {
                            DocxTemplaterReport docxTemplaterReport =
                                    (DocxTemplaterReport) reportDecorator.getReport();
                            List<ReportTemplateModel> models =
                                    findTemplateModelsFromFolderPath(
                                            reportDecorator.getNamespace(),
                                            docxTemplaterReport.getCustomTemplateFolderPath());
                            reportTemplateModels.addAll(models);
                        });

        return reportTemplateModels;
    }

    private List<ReportTemplateModel> findTemplateModelsFromFolderPath(
            String namespace, @NotNull String folderPath) {
        File[] customTemplates = new File(folderPath).listFiles();
        if (customTemplates != null) {
            return Stream.of(customTemplates)
                    .filter(customTemplate -> !customTemplate.isDirectory())
                    .map(File::getName)
                    .filter(
                            customTemplateName ->
                                    DOCX_FILE_EXTENSION.equals(FilenameUtils.getExtension(customTemplateName)))
                    .map(fileName -> new ReportTemplateModel(namespace, fileName))
                    .toList();
        }
        return new ArrayList<>();
    }

    private List<RemoteSynchronisationDto> findRemoteSynchronisationsForPlugin(
            List<Long> manageableProjectIds, SynchronisationPlugin plugin) {
        List<RemoteSynchronisation> synchronisations =
                systemAdministrationService.findRemoteSynchronisationForPlugin(
                        manageableProjectIds, plugin.getId());
        List<RemoteSynchronisationDto> synchronisationDtos =
                synchronisations.stream().map(this::transformRemoteSynchronisationInDto).toList();

        appendErrorLogFilePath(synchronisationDtos, plugin);
        return synchronisationDtos;
    }

    private void appendErrorLogFilePath(
            List<RemoteSynchronisationDto> synchronisationDtos, SynchronisationPlugin plugin) {
        synchronisationDtos.forEach(
                syncDto -> {
                    String syncErrorLogFilePath = plugin.getSyncErrorLogPath(syncDto.getId());
                    File file = new File(syncErrorLogFilePath);

                    if (file.exists()) {
                        syncDto.setSyncErrorLogFilePath(syncErrorLogFilePath);
                    }
                });
    }

    private RemoteSynchronisationDto transformRemoteSynchronisationInDto(
            RemoteSynchronisation remoteSync) {
        return new RemoteSynchronisationDto(remoteSync);
    }
}
