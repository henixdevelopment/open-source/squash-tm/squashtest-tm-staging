/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.execution;

import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.execution.ExecutionFlag;
import org.squashtest.tm.service.display.execution.AutomatedExecutionDisplayService;
import org.squashtest.tm.service.execution.automatedexecution.AutomatedExecutionFlagService;
import org.squashtest.tm.service.internal.display.dto.FailureDetailDto;

@RestController
@RequestMapping("/backend/execution-extender/{executionExtenderId}")
public class AutomatedExecutionController {

    private final AutomatedExecutionDisplayService automatedExecutionDisplayService;
    private final AutomatedExecutionFlagService automatedExecutionFlagService;

    public AutomatedExecutionController(
            AutomatedExecutionDisplayService automatedExecutionDisplayService,
            AutomatedExecutionFlagService automatedExecutionFlagService) {
        this.automatedExecutionDisplayService = automatedExecutionDisplayService;
        this.automatedExecutionFlagService = automatedExecutionFlagService;
    }

    @GetMapping("failure-detail-list")
    public List<FailureDetailDto> getFailureDetailList(
            @PathVariable("executionExtenderId") long executionExtenderId) {
        return automatedExecutionDisplayService.findFailureDetailList(executionExtenderId);
    }

    @PostMapping("/flag")
    public void updateFlag(
            @PathVariable("executionExtenderId") long executionExtenderId,
            @RequestBody UpdateFlagRequest request) {
        ExecutionFlag flag = request.flag();
        automatedExecutionFlagService.updateFlag(executionExtenderId, flag);
    }

    public record UpdateFlagRequest(ExecutionFlag flag) {}
}
