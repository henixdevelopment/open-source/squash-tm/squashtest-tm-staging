/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.report;

import static org.squashtest.tm.api.report.DocxTemplaterReport.CUSTOM_TEMPLATE_FOLDER_PATH;
import static org.squashtest.tm.api.report.DocxTemplaterReport.DEFAULT_TEMPLATE_FILE_PATH;
import static org.squashtest.tm.api.report.DocxTemplaterReport.TEMPLATE_FILE_NAME;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.jooq.tools.json.ParseException;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.squashtest.tm.api.report.BasicDirectDownloadableReport;
import org.squashtest.tm.api.report.DocxTemplaterReport;
import org.squashtest.tm.api.report.Report;
import org.squashtest.tm.api.report.criteria.Criteria;
import org.squashtest.tm.api.report.spring.view.docxtemplater.DocxTemplaterDocxView;
import org.squashtest.tm.api.utils.CurrentUserHelper;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.service.project.ProjectFinder;
import org.squashtest.tm.service.report.ReportFinderService;
import org.squashtest.tm.service.report.ReportTemplateManagerService;
import org.squashtest.tm.web.backend.helper.JsonHelper;
import org.squashtest.tm.web.backend.report.IdentifiedReportDecorator;
import org.squashtest.tm.web.backend.report.ReportsRegistry;
import org.squashtest.tm.web.backend.report.criteria.ConciseFormToCriteriaConverter;

@RestController
@RequestMapping("backend/reports/{namespace}")
public class ReportGenerationController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReportGenerationController.class);
    private static final String ATTACHMENT_FILENAME = "attachment; filename=";
    private final ReportsRegistry reportsRegistry;
    private final ProjectFinder projectFinder;
    private final ReportFinderService reportFinderService;
    private final ReportTemplateManagerService reportTemplateManagerService;
    private final CurrentUserHelper currentUserHelper;

    public ReportGenerationController(
            ReportsRegistry reportsRegistry,
            ProjectFinder projectFinder,
            ReportFinderService reportFinderService,
            ReportTemplateManagerService reportTemplateManagerService,
            CurrentUserHelper currentUserHelper) {
        this.reportsRegistry = reportsRegistry;
        this.projectFinder = projectFinder;
        this.reportFinderService = reportFinderService;
        this.reportTemplateManagerService = reportTemplateManagerService;
        this.currentUserHelper = currentUserHelper;
    }

    @GetMapping(
            value = "/views/{viewIndex}/formats/{format}",
            params = {"json"})
    public ModelAndView getReportView(
            @PathVariable String namespace,
            @PathVariable int viewIndex,
            @PathVariable String format,
            @RequestParam("json") String parameters)
            throws IOException {
        Report report = reportsRegistry.findReport(namespace);
        Map<String, Criteria> crit = retrieveReportFormCriteriaMap(parameters, report);

        return report.buildModelAndView(viewIndex, format, crit);
    }

    @GetMapping(
            value = "/views/{viewIndex}/data/docx",
            params = {"json"})
    public Map<String, Object> getReportView(
            @PathVariable String namespace,
            @PathVariable int viewIndex,
            @RequestParam("json") String parameters)
            throws IOException {
        Report report = reportsRegistry.findReport(namespace);
        Map<String, Criteria> crit = retrieveReportFormCriteriaMap(parameters, report);
        // the old report API is based on spring view so to avoid a complete redesign of the API
        // we simply extract the model as response body
        return report.buildModelAndView(viewIndex, "docx", crit).getModel();
    }

    @GetMapping(
            value = "/data/direct-downloadable-report",
            params = {"json"})
    public ResponseEntity<InputStreamResource> getDownloadableReport(
            @PathVariable String namespace, @RequestParam("json") String parameters) throws IOException {
        IdentifiedReportDecorator reportDecorator = reportsRegistry.findReport(namespace);
        Map<String, Criteria> crit = retrieveReportFormCriteriaMap(parameters, reportDecorator);

        if (reportDecorator.isDirectDownloadableReport()) {
            try {
                BasicDirectDownloadableReport directDownloadableReport =
                        (BasicDirectDownloadableReport) reportDecorator.getReport();
                File report = directDownloadableReport.generateReport(crit);
                InputStreamResource resource = new InputStreamResource(new FileInputStream(report));

                return ResponseEntity.ok()
                        .header(HttpHeaders.CONTENT_DISPOSITION, ATTACHMENT_FILENAME + report.getName())
                        .contentType(MediaType.APPLICATION_OCTET_STREAM)
                        .contentLength(report.length())
                        .body(resource);
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        throw new IllegalArgumentException();
    }

    @PostMapping(value = "/views/{viewIndex}/docxtemplate")
    public void getTemplate(
            @PathVariable String namespace,
            @PathVariable int viewIndex,
            @RequestBody DocxTemplatePatch patch,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        IdentifiedReportDecorator identifiedReportDecorator = reportsRegistry.findReport(namespace);

        Map<String, Object> templateModel =
                buildTemplateModel(patch, (DocxTemplaterReport) identifiedReportDecorator.getReport());

        identifiedReportDecorator
                .getViews()[viewIndex]
                .getSpringView()
                .render(templateModel, request, response);
    }

    @PostMapping(value = "/template-exists-in-folder")
    public boolean isTemplateFileExistInFolder(
            @PathVariable String namespace, @RequestBody DocxTemplatePatch patch) {
        DocxTemplaterReport docxReport = findDocXTemplaterReportByNamespace(namespace);
        String filePath = docxReport.getCustomTemplateFolderPath().concat(patch.getTemplateFileName());
        return this.reportTemplateManagerService.doesTemplateExistOnServer(filePath);
    }

    @PostMapping(value = "/add-report-template-file")
    public boolean addReportTemplateFile(
            @PathVariable String namespace, @RequestParam("archive") MultipartFile uploadedFile) {
        DocxTemplaterReport docxReport = findDocXTemplaterReportByNamespace(namespace);
        String filePath =
                docxReport
                        .getCustomTemplateFolderPath()
                        .concat(Objects.requireNonNull(uploadedFile.getOriginalFilename()));

        return reportTemplateManagerService.addATemplateToServer(
                filePath, docxReport.getCustomTemplateFolderPath(), uploadedFile);
    }

    @GetMapping(value = "/download-default-template")
    public ResponseEntity<InputStreamResource> downloadDefaultTemplate(@PathVariable String namespace)
            throws IOException {
        DocxTemplaterReport docxReport = findDocXTemplaterReportByNamespace(namespace);

        Resource resource =
                ((DocxTemplaterDocxView) docxReport.getViews()[0].getSpringView())
                        .downloadDefaultTemplate(docxReport.getDefaultTemplateFilePath());
        InputStreamResource res = new InputStreamResource(resource.getInputStream());

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, ATTACHMENT_FILENAME + resource.getFilename())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .contentLength(resource.contentLength())
                .body(res);
    }

    @GetMapping(value = "/download-custom-template/{templateFileName}")
    public void downloadCustomTemplate(
            @PathVariable String namespace,
            @PathVariable String templateFileName,
            HttpServletResponse response)
            throws AccessDeniedException {
        DocxTemplaterReport docxReport = findDocXTemplaterReportByNamespace(namespace);

        checkFilePresence(templateFileName, docxReport);

        String filePath = docxReport.getCustomTemplateFolderPath().concat(templateFileName);
        File tempFile = new File(filePath);
        try {
            InputStream inputStream = new FileInputStream(tempFile);
            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            response.setHeader("Content-Disposition", ATTACHMENT_FILENAME + templateFileName);
            IOUtils.copy(inputStream, response.getOutputStream());
            response.flushBuffer();
            inputStream.close();
        } catch (IOException ioException) {
            LOGGER.debug("file doesn't exist {}", templateFileName, ioException);
        }
    }

    // Check that the file exists in the custom template folder, avoiding directory traversal for
    // security reasons
    private static void checkFilePresence(String templateFileName, DocxTemplaterReport docxReport)
            throws AccessDeniedException {
        File templateFolder = new File(docxReport.getCustomTemplateFolderPath());

        File[] files = templateFolder.listFiles();

        if (files == null) {
            throw new AccessDeniedException("Invalid custom template folder");
        }

        for (File file : files) {
            if (file.getName().equals(templateFileName)) {
                return;
            }
        }

        throw new AccessDeniedException("Invalid custom template file");
    }

    @PostMapping(value = "/delete-custom-template")
    public void deleteCustomTemplate(
            @PathVariable String namespace, @RequestBody DocxTemplatePatch patch) {
        DocxTemplaterReport docxReport = findDocXTemplaterReportByNamespace(namespace);
        final String filePath =
                docxReport.getCustomTemplateFolderPath().concat(patch.getTemplateFileName());

        this.reportTemplateManagerService.deleteTemplateFromServer(filePath);
    }

    @PostMapping(value = "/is-template-used-in-report")
    public boolean isTemplateUsedInReport(
            @PathVariable String namespace, @RequestBody DocxTemplatePatch patch) {
        return reportFinderService.isTemplateUsedInReport(patch.getTemplateFileName(), namespace);
    }

    private Map<String, Object> buildTemplateModel(
            DocxTemplatePatch patch, DocxTemplaterReport report) throws ParseException {
        Map<String, Object> templateModel = new HashMap<>();

        String templateFileName = retrieveTemplateFileName(patch);

        templateModel.put(CUSTOM_TEMPLATE_FOLDER_PATH, report.getCustomTemplateFolderPath());
        templateModel.put(DEFAULT_TEMPLATE_FILE_PATH, report.getDefaultTemplateFilePath());
        templateModel.put(TEMPLATE_FILE_NAME, templateFileName);

        return templateModel;
    }

    private Map<String, Criteria> retrieveReportFormCriteriaMap(String parameters, Report report)
            throws IOException {
        Map<String, Object> form = JsonHelper.deserialize(parameters);
        final List<Long> readableProjectIds = currentUserHelper.findFilteredReadableProjectIds();
        List<Long> projectIds = projectFinder.findAllProjectIdsOrderedByName(readableProjectIds);

        return new ConciseFormToCriteriaConverter(report, projectIds).convert(form);
    }

    private String retrieveTemplateFileName(DocxTemplatePatch patch) throws ParseException {
        String templateFileNameFromModel = patch.getTemplateFileName();

        if (Objects.isNull(patch.getReportDefinitionId())) {
            return templateFileNameFromModel;
        }
        return StringUtils.hasText(templateFileNameFromModel)
                ? templateFileNameFromModel
                : reportFinderService.fetchTemplateFromParametersByReportDefinitionId(
                        patch.getReportDefinitionId());
    }

    private DocxTemplaterReport findDocXTemplaterReportByNamespace(String namespace) {
        IdentifiedReportDecorator identifiedReportDecorator = reportsRegistry.findReport(namespace);
        return (DocxTemplaterReport) identifiedReportDecorator.getReport();
    }

    static class DocxTemplatePatch {
        private Long reportDefinitionId;
        private String templateFileName;

        public Long getReportDefinitionId() {
            return reportDefinitionId;
        }

        public void setReportDefinitionId(Long reportDefinitionId) {
            this.reportDefinitionId = reportDefinitionId;
        }

        public String getTemplateFileName() {
            return templateFileName;
        }

        public void setTemplateFileName(String templateFileName) {
            this.templateFileName = templateFileName;
        }
    }
}
