/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.scm.server;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.core.scm.api.exception.ScmException;
import org.squashtest.tm.exception.scmserver.ScmPluginException;
import org.squashtest.tm.service.display.scm.server.ScmServerDisplayService;
import org.squashtest.tm.service.internal.display.dto.ScmRepositoryDto;
import org.squashtest.tm.service.scmserver.ScmRepositoryManagerService;
import org.squashtest.tm.web.backend.controller.form.model.ScmRepositoryFormModel;

@RestController
@RequestMapping("/backend/scm-repositories")
public class ScmRepositoryManagementController {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(ScmRepositoryManagementController.class);

    ScmRepositoryManagerService scmRepositoryManager;
    ScmServerDisplayService scmServerDisplayService;

    @Inject
    ScmRepositoryManagementController(
            ScmRepositoryManagerService scmRepositoryManager,
            ScmServerDisplayService scmServerDisplayService) {
        this.scmRepositoryManager = scmRepositoryManager;
        this.scmServerDisplayService = scmServerDisplayService;
    }

    @PostMapping(value = "/{scmServerId}/new")
    public Map<String, List<ScmRepositoryDto>> createNewScmRepository(
            @PathVariable long scmServerId, @RequestBody ScmRepositoryFormModel scmRepositoryFormModel)
            throws IOException {
        try {
            scmRepositoryManager.createNewScmRepository(
                    scmServerId,
                    scmRepositoryFormModel.name(),
                    scmRepositoryFormModel.workingBranch(),
                    scmRepositoryFormModel.workingFolderPath(),
                    scmRepositoryFormModel.cloneRepository());

        } catch (ScmException ex) {
            LOGGER.error("Error while creating new SCM repository", ex);
            throw new ScmPluginException(ex.getField(), ex);
        }

        List<ScmRepositoryDto> repositories =
                scmServerDisplayService.getScmServerView(scmServerId).getRepositories();
        return Collections.singletonMap("repositories", repositories);
    }

    @PostMapping(value = "/{scmServerId}/{scmRepositoryId}/branch")
    public Map<String, List<ScmRepositoryDto>> updateBranch(
            @PathVariable long scmServerId,
            @PathVariable long scmRepositoryId,
            @RequestBody ScmRepositoryPatch patch)
            throws IOException {
        try {
            scmRepositoryManager.updateBranch(scmRepositoryId, patch.getBranch());
        } catch (ScmException ex) {
            throw new ScmPluginException(ex.getField(), ex);
        }

        List<ScmRepositoryDto> repositories =
                scmServerDisplayService.getScmServerView(scmServerId).getRepositories();
        return Collections.singletonMap("repositories", repositories);
    }

    @DeleteMapping(value = "/{scmRepositoriesIds}")
    public void deleteScmRepositories(@PathVariable List<Long> scmRepositoriesIds) {
        scmRepositoryManager.deleteScmRepositories(scmRepositoriesIds);
    }

    @GetMapping("check-bound-to-project/{scmRepositoryIds}")
    public boolean isOneRepositoryBoundToProject(@PathVariable Collection<Long> scmRepositoryIds) {
        return scmRepositoryManager.isOneRepositoryBoundToProjectOrTestCase(scmRepositoryIds);
    }

    public static class ScmRepositoryPatch {
        private String branch;

        public String getBranch() {
            return branch;
        }

        public void setBranch(String branch) {
            this.branch = branch;
        }
    }

    @PostMapping(value = "/{scmRepositoryId}/recreate-local-repository")
    public void recloneScmRepository(@PathVariable long scmRepositoryId) {
        scmRepositoryManager.recloneScmRepository(scmRepositoryId);
    }
}
