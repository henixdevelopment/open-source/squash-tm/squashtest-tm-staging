/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.entitypath;

import java.util.Collections;
import java.util.Map;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.internal.library.EntityPathHeaderService;

@RestController
@RequestMapping("/backend/entity-path-header")
public class EntityPathHeaderController {

    private static final String PATH = "path";

    private final EntityPathHeaderService entityPathHeaderService;

    public EntityPathHeaderController(EntityPathHeaderService entityPathHeaderService) {
        this.entityPathHeaderService = entityPathHeaderService;
    }

    @GetMapping(value = "campaign/{campaignId}")
    Map<String, String> getCampaignPathHeader(@PathVariable Long campaignId) {
        return Collections.singletonMap(PATH, entityPathHeaderService.buildCLNPathHeader(campaignId));
    }

    @GetMapping(value = "campaign-folder/{campaignFolderId}")
    Map<String, String> getCampaignFolderPathHeader(@PathVariable Long campaignFolderId) {
        return Collections.singletonMap(
                PATH, entityPathHeaderService.buildCLNPathHeader(campaignFolderId));
    }

    @GetMapping(value = "iteration/{iterationId}")
    Map<String, String> getIterationPathHeader(@PathVariable Long iterationId) {
        return Collections.singletonMap(
                PATH, entityPathHeaderService.buildIterationPathHeader(iterationId));
    }

    @GetMapping(value = "test-case/{testCaseId}")
    Map<String, String> getTestCasePathHeader(@PathVariable Long testCaseId) {
        return Collections.singletonMap(PATH, entityPathHeaderService.buildTCLNPathHeader(testCaseId));
    }

    @GetMapping(value = "test-case-folder/{testCaseFolderId}")
    Map<String, String> getTestCaseFolderPathHeader(@PathVariable Long testCaseFolderId) {
        return Collections.singletonMap(
                PATH, entityPathHeaderService.buildTCLNPathHeader(testCaseFolderId));
    }

    @GetMapping(value = "requirement/{requirementId}")
    Map<String, String> getRequirementPathHeader(@PathVariable Long requirementId) {
        return Collections.singletonMap(
                PATH, entityPathHeaderService.buildRLNPathHeader(requirementId));
    }

    @GetMapping(value = "requirement-folder/{requirementFolderId}")
    Map<String, String> getRequirementFolderPathHeader(@PathVariable Long requirementFolderId) {
        return Collections.singletonMap(
                PATH, entityPathHeaderService.buildRLNPathHeader(requirementFolderId));
    }
}
