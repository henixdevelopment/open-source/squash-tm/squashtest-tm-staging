/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.system;

import java.io.File;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.service.system.LogFileDownloadService;
import org.squashtest.tm.service.system.SystemAdministrationService;

@RestController
@RequestMapping("/backend/system")
public class SystemAdministrationController {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(SystemAdministrationController.class);

    private static final String ATTACHMENT_FILENAME = "attachment; filename=";
    private static final String CONTENT_DISPOSITION = "Content-Disposition";
    private static final String FILENAME = "filename";

    private final SystemAdministrationService systemAdministrationService;
    private final LogFileDownloadService logFileDownloadService;

    SystemAdministrationController(
            SystemAdministrationService systemAdministrationService,
            LogFileDownloadService logFileDownloadService) {
        this.systemAdministrationService = systemAdministrationService;
        this.logFileDownloadService = logFileDownloadService;
    }

    @PostMapping("/settings/white-list")
    void changeWhiteList(@RequestBody SystemSettingsPatch patch) {
        systemAdministrationService.changeWhiteList(patch.whiteList());
    }

    @PostMapping("/settings/upload-size-limit")
    void changeUploadSizeLimit(@RequestBody SystemSettingsPatch patch) {
        systemAdministrationService.changeUploadSizeLimit(patch.uploadSizeLimit());
    }

    @PostMapping("/settings/import-size-limit")
    void changeImportSizeLimit(@RequestBody SystemSettingsPatch patch) {
        systemAdministrationService.changeImportSizeLimit(patch.importSizeLimit());
    }

    @PostMapping("/settings/callback-url")
    void changeCallbackUrl(@RequestBody SystemSettingsPatch patch) {
        systemAdministrationService.changeCallbackUrl(patch.callbackUrl());
    }

    record SystemSettingsPatch(
            String whiteList, String uploadSizeLimit, String importSizeLimit, String callbackUrl) {}

    @PostMapping("/messages/welcome-message")
    void changeWelcomeMessage(@RequestBody SystemMessagesPatch patch) {
        systemAdministrationService.changeWelcomeMessage(patch.welcomeMessage());
    }

    @PostMapping("/messages/login-message")
    void changeLoginMessage(@RequestBody SystemMessagesPatch patch) {
        systemAdministrationService.changeLoginMessage(patch.loginMessage());
    }

    @PostMapping("/messages/banner-message")
    void changeBannerMessage(@RequestBody SystemMessagesPatch patch) {
        systemAdministrationService.changeBannerMessage(patch.bannerMessage());
    }

    record SystemMessagesPatch(String welcomeMessage, String loginMessage, String bannerMessage) {}

    @GetMapping(value = "logs/latest", produces = MediaType.TEXT_PLAIN_VALUE)
    FileSystemResource downloadCurrentLogfile(HttpServletResponse response) {
        final File logfile = logFileDownloadService.getCurrentLogFile();
        final String filename = logfile.getName();
        return getFileSystemResource(response, filename, logfile);
    }

    @GetMapping(
            value = "logs",
            params = {FILENAME},
            produces = MediaType.TEXT_PLAIN_VALUE)
    FileSystemResource downloadLogfile(
            @RequestParam(FILENAME) String filename, HttpServletResponse response) {
        try {
            final File logfile = logFileDownloadService.getPreviousLogFile(filename);
            return getFileSystemResource(response, logfile.getName(), logfile);
        } catch (IOException e) {
            LOGGER.error("Error downloading log file", e);
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return null;
        } catch (IllegalStateException e) {
            LOGGER.error("Error downloading log file", e);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }
    }

    private static FileSystemResource getFileSystemResource(
            HttpServletResponse response, String logfile, File logfile1) {
        response.setContentType(MediaType.TEXT_PLAIN_VALUE);
        response.setHeader(CONTENT_DISPOSITION, ATTACHMENT_FILENAME + logfile);
        return new FileSystemResource(logfile1);
    }
}
