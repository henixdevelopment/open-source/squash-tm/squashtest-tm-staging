/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.tf;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.display.tf.AutomationRequestDisplayService;
import org.squashtest.tm.service.display.tf.AutomationWorkspaceDataProviderService;
import org.squashtest.tm.service.internal.display.dto.tf.AutomationWorkspaceDataDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;

@RestController
@RequestMapping(value = "backend/automation-workspace")
public class AutomationWorkspaceController {

    private final AutomationRequestDisplayService automationRequestDisplayService;
    private final AutomationWorkspaceDataProviderService automationWorkspaceDataProviderService;

    public AutomationWorkspaceController(
            AutomationRequestDisplayService automationRequestDisplayService,
            AutomationWorkspaceDataProviderService automationWorkspaceDataProviderService) {
        this.automationRequestDisplayService = automationRequestDisplayService;
        this.automationWorkspaceDataProviderService = automationWorkspaceDataProviderService;
    }

    @GetMapping(value = "data")
    public AutomationWorkspaceDataDto getWorkspaceRefData() {
        return automationWorkspaceDataProviderService.findData();
    }

    @PostMapping(value = "assignee-autom-req")
    public GridResponse getAutomationRequestForAssignView(@RequestBody GridRequest request) {
        return automationRequestDisplayService.findAutomationRequestAssignedToCurrentUser(request);
    }

    @PostMapping(value = "treatment-autom-req")
    public GridResponse getAutomationRequestForTreatmentView(@RequestBody GridRequest request) {
        return automationRequestDisplayService.findAutomationRequestToTreat(request);
    }

    @PostMapping(value = "global-autom-req")
    public GridResponse getAutomationRequestForGlobalView(@RequestBody GridRequest request) {
        return automationRequestDisplayService.findAutomationRequests(request);
    }
}
