/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.testplan;

import java.util.List;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.campaign.SprintTestPlanItemManagerService;
import org.squashtest.tm.service.display.testplan.SprintTestPlanDisplayService;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;

@RestController
@RequestMapping("/backend")
public class SprintTestPlanController {

    private final SprintTestPlanDisplayService sprintTestPlanDisplayService;
    private final SprintTestPlanItemManagerService sprintTestPlanItemManagerService;

    public SprintTestPlanController(
            SprintTestPlanDisplayService sprintTestPlanDisplayService,
            SprintTestPlanItemManagerService sprintTestPlanItemManagerService) {
        this.sprintTestPlanDisplayService = sprintTestPlanDisplayService;
        this.sprintTestPlanItemManagerService = sprintTestPlanItemManagerService;
    }

    @PostMapping("/test-plan/{testPlanId}")
    GridResponse findSprintTestPlan(
            @PathVariable Long testPlanId, @RequestBody GridRequest gridRequest) {
        return sprintTestPlanDisplayService.findSprintReqVersionTestPlan(testPlanId, gridRequest);
    }

    @DeleteMapping("/sprint-req-version/{sprintReqVersionId}/test-plan-items/{testPlanItemIds}")
    public void deleteItems(
            @PathVariable() Long sprintReqVersionId, @PathVariable() List<Long> testPlanItemIds) {
        sprintTestPlanItemManagerService.deleteSprintReqVersionTestPlanItems(
                sprintReqVersionId, testPlanItemIds);
    }

    @PostMapping(
            "sprint-req-version/{sprintReqVersionId}/test-plan-items/{testPlanItemIds}/position/{position}")
    public void moveSprintReqVersionTestPlanItems(
            @PathVariable long sprintReqVersionId,
            @PathVariable List<Long> testPlanItemIds,
            @PathVariable int position) {
        sprintTestPlanItemManagerService.moveSprintReqVersionTestPlanItems(
                sprintReqVersionId, testPlanItemIds, position);
    }
}
