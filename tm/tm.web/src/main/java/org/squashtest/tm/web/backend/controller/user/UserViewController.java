/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.user;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.users.Team;
import org.squashtest.tm.service.display.user.UserDisplayService;
import org.squashtest.tm.service.internal.display.dto.UserAdminViewDto;
import org.squashtest.tm.service.internal.display.dto.UserAdminViewTeamDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.project.ProjectsPermissionManagementService;
import org.squashtest.tm.service.user.ApiTokenService;
import org.squashtest.tm.service.user.UserAdministrationService;
import org.squashtest.tm.web.backend.controller.project.ProjectModel;

@RestController
@RequestMapping("/backend/user-view")
public class UserViewController {

    private final UserDisplayService userDisplayService;
    private final ProjectsPermissionManagementService permissionService;
    private final UserAdministrationService userAdministrationService;
    private final ApiTokenService apiTokenService;

    @Inject
    UserViewController(
            UserDisplayService userDisplayService,
            ProjectsPermissionManagementService permissionService,
            UserAdministrationService userAdministrationService,
            ApiTokenService apiTokenService) {

        this.userDisplayService = userDisplayService;
        this.permissionService = permissionService;
        this.userAdministrationService = userAdministrationService;
        this.apiTokenService = apiTokenService;
    }

    @GetMapping(value = "/{userId}")
    public UserAdminViewDto getUserView(@PathVariable long userId) {
        return userDisplayService.getUserView(userId);
    }

    @GetMapping(value = "/{userId}/projects-without-permission")
    public List<ProjectModel> getProjectWithoutPermission(@PathVariable long userId) {

        List<GenericProject> projectList =
                permissionService.findProjectWithoutPermissionByParty(
                        userId, Sort.by(Sort.Direction.ASC, "name"));

        List<ProjectModel> projectModelList = new ArrayList<>();
        if (projectList != null) {
            for (GenericProject project : projectList) {
                projectModelList.add(new ProjectModel(project));
            }
        }

        return projectModelList;
    }

    @GetMapping(value = "/{userId}/unassociated-teams")
    public List<UserAdminViewTeamDto> getUnassociatedTeams(@PathVariable long userId) {

        List<Team> teams = userAdministrationService.findAllNonAssociatedTeams(userId);

        List<UserAdminViewTeamDto> unassociatedTeamsList = new ArrayList<>();
        if (teams != null) {
            teams.forEach(
                    team -> {
                        UserAdminViewTeamDto unassociatedTeam = new UserAdminViewTeamDto();
                        unassociatedTeam.setPartyId(team.getId());
                        unassociatedTeam.setName(team.getName());
                        unassociatedTeamsList.add(unassociatedTeam);
                    });
        }

        return unassociatedTeamsList;
    }

    @PostMapping(value = "{userId}/api-tokens")
    public GridResponse findTestAutoServerApiTokens(
            @PathVariable long userId, @RequestBody GridRequest request) {
        return userDisplayService.findTestAutoServerApiTokens(userId, request);
    }

    @DeleteMapping("delete-token/{tokenId}")
    public void deleteTestAutoServerApiToken(@PathVariable long tokenId) {
        apiTokenService.deleteTestAutoServerApiToken(tokenId);
    }
}
