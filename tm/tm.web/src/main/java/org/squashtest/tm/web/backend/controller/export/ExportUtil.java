/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.export;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRParameter;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.squashtest.tm.domain.library.AbstractExportData;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;
import org.squashtest.tm.web.backend.report.service.JasperReportsService;

/**
 * @author qtran - created on 15/12/2020
 */
public final class ExportUtil {
    private static final int EOF = -1;

    private ExportUtil() {
        throw new IllegalStateException("Utility class");
    }

    public static void printExport(
            List<? extends AbstractExportData> dataSource,
            String filename,
            String jasperFile,
            HttpServletResponse response,
            Locale locale,
            String format,
            boolean keepRteFormat,
            Map<String, Object> reportParameters,
            MessageSource messageSource,
            JasperReportsService jrServices) {
        try {

            if (!keepRteFormat) {
                removeRteFormat(dataSource);
            }

            // report generation parameters
            reportParameters.put(JRParameter.REPORT_LOCALE, locale);
            reportParameters.put(
                    JRParameter.REPORT_RESOURCE_BUNDLE,
                    new MessageSourceResourceBundle(messageSource, locale));

            InputStream jsStream =
                    Thread.currentThread().getContextClassLoader().getResourceAsStream(jasperFile);
            InputStream reportStream =
                    jrServices.getReportAsStream(jsStream, format, dataSource, reportParameters);

            // print it.
            ServletOutputStream servletStream = response.getOutputStream();

            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment; filename=" + filename + "." + format);

            flushStreams(reportStream, servletStream);

            reportStream.close();
            servletStream.close();

        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }

    private static void flushStreams(InputStream inStream, ServletOutputStream outStream)
            throws IOException {
        int readByte;

        do {
            readByte = inStream.read();

            if (readByte != EOF) {
                outStream.write(readByte);
            }
        } while (readByte != EOF);
    }

    private static void removeRteFormat(List<? extends AbstractExportData> dataSource) {
        for (AbstractExportData data : dataSource) {
            String htmlDescription = data.getDescription();
            String description = HTMLCleanupUtils.htmlToTrimmedText(htmlDescription);
            data.setDescription(description);
        }
    }
}
