/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.campaign;

import java.util.Collections;
import java.util.Locale;
import javax.inject.Named;
import javax.inject.Provider;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.Workspace;
import org.squashtest.tm.domain.customreport.CustomReportDashboard;
import org.squashtest.tm.domain.users.PartyPreference;
import org.squashtest.tm.domain.users.preferences.CorePartyPreference;
import org.squashtest.tm.service.campaign.CampaignLibraryNavigationService;
import org.squashtest.tm.service.customreport.CustomReportDashboardService;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;
import org.squashtest.tm.service.internal.display.dto.customreports.CustomReportDashboardDto;
import org.squashtest.tm.service.internal.dto.json.JsonCustomReportDashboard;
import org.squashtest.tm.service.statistics.campaign.StatisticsBundle;
import org.squashtest.tm.service.user.PartyPreferenceService;
import org.squashtest.tm.web.backend.model.builder.JsonCustomReportDashboardBuilder;

@RestController
@RequestMapping("/backend/campaign-milestone-dashboard")
public class CampaignMilestoneDashboardController {

    private final CampaignLibraryNavigationService campaignLibraryNavigationService;
    private final CustomReportLibraryNodeService customReportLibraryNodeService;

    @Named("customReport.dashboardBuilder")
    private final Provider<JsonCustomReportDashboardBuilder> builderProvider;

    private final CustomReportDashboardService customReportDashboardService;
    private final PartyPreferenceService partyPreferenceService;

    public CampaignMilestoneDashboardController(
            CampaignLibraryNavigationService campaignLibraryNavigationService,
            CustomReportLibraryNodeService customReportLibraryNodeService,
            Provider<JsonCustomReportDashboardBuilder> builderProvider,
            CustomReportDashboardService customReportDashboardService,
            PartyPreferenceService partyPreferenceService) {
        this.campaignLibraryNavigationService = campaignLibraryNavigationService;
        this.customReportLibraryNodeService = customReportLibraryNodeService;
        this.builderProvider = builderProvider;
        this.customReportDashboardService = customReportDashboardService;
        this.partyPreferenceService = partyPreferenceService;
    }

    @GetMapping
    public CampaignMilestoneDashboard getDashboardByMilestone(
            Locale locale, @RequestParam boolean lastExecutionScope) {
        CampaignMilestoneDashboard campaignMilestoneDashboard = new CampaignMilestoneDashboard();
        boolean canShowDashboardInWorkspace =
                customReportDashboardService.canShowDashboardInWorkspace(Workspace.CAMPAIGN);
        boolean shouldShowFavoriteDashboardInWorkspace =
                customReportDashboardService.shouldShowFavoriteDashboardInWorkspace(Workspace.CAMPAIGN);
        campaignMilestoneDashboard.setCanShowFavoriteDashboard(canShowDashboardInWorkspace);
        campaignMilestoneDashboard.setShouldShowFavoriteDashboard(
                shouldShowFavoriteDashboardInWorkspace);

        if (shouldShowFavoriteDashboardInWorkspace) {
            if (canShowDashboardInWorkspace) {
                PartyPreference preference =
                        partyPreferenceService.findPreferenceForCurrentUser(
                                CorePartyPreference.FAVORITE_DASHBOARD_CAMPAIGN.getPreferenceKey());
                Long dashboardId = Long.valueOf(preference.getPreferenceValue());
                CustomReportDashboard dashboard =
                        customReportLibraryNodeService.findCustomReportDashboardById(dashboardId);
                CustomReportDashboardDto dto =
                        new CustomReportDashboardDto(
                                Workspace.CAMPAIGN, Collections.emptyList(), true, false, lastExecutionScope);
                JsonCustomReportDashboard jsonDashboard =
                        builderProvider.get().build(dashboardId, dashboard, locale, dto);
                campaignMilestoneDashboard.setDashboard(jsonDashboard);
                campaignMilestoneDashboard.setFavoriteDashboardId(dashboardId);
            }
        } else {
            campaignMilestoneDashboard.setStatistics(
                    campaignLibraryNavigationService.gatherCampaignStatisticsBundleByMilestone(
                            lastExecutionScope));
        }

        return campaignMilestoneDashboard;
    }

    static class CampaignMilestoneDashboard {
        private StatisticsBundle statistics;
        private JsonCustomReportDashboard dashboard;
        private boolean shouldShowFavoriteDashboard;
        private boolean canShowFavoriteDashboard;
        private Long favoriteDashboardId;

        public StatisticsBundle getStatistics() {
            return statistics;
        }

        public void setStatistics(StatisticsBundle statistics) {
            this.statistics = statistics;
        }

        public JsonCustomReportDashboard getDashboard() {
            return dashboard;
        }

        public void setDashboard(JsonCustomReportDashboard dashboard) {
            this.dashboard = dashboard;
        }

        public boolean isShouldShowFavoriteDashboard() {
            return shouldShowFavoriteDashboard;
        }

        public void setShouldShowFavoriteDashboard(boolean shouldShowFavoriteDashboard) {
            this.shouldShowFavoriteDashboard = shouldShowFavoriteDashboard;
        }

        public boolean isCanShowFavoriteDashboard() {
            return canShowFavoriteDashboard;
        }

        public void setCanShowFavoriteDashboard(boolean canShowFavoriteDashboard) {
            this.canShowFavoriteDashboard = canShowFavoriteDashboard;
        }

        public Long getFavoriteDashboardId() {
            return favoriteDashboardId;
        }

        public void setFavoriteDashboardId(Long favoriteDashboardId) {
            this.favoriteDashboardId = favoriteDashboardId;
        }
    }
}
