/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.requirement;

import java.util.Collections;
import java.util.Locale;
import javax.inject.Named;
import javax.inject.Provider;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.Workspace;
import org.squashtest.tm.domain.customreport.CustomReportDashboard;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;
import org.squashtest.tm.service.display.requirements.RequirementDisplayService;
import org.squashtest.tm.service.internal.display.dto.customreports.CustomReportDashboardDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementLibraryDto;
import org.squashtest.tm.service.internal.dto.json.JsonCustomReportDashboard;
import org.squashtest.tm.service.requirement.RequirementLibraryNavigationService;
import org.squashtest.tm.web.backend.model.builder.JsonCustomReportDashboardBuilder;

@RestController
@RequestMapping(value = "backend/requirement-library-view")
public class RequirementLibraryViewController {

    private final RequirementLibraryNavigationService requirementLibraryNavigationService;
    private final RequirementDisplayService requirementDisplayService;
    private final CustomReportLibraryNodeService customReportLibraryNodeService;

    @Named("customReport.dashboardBuilder")
    private final Provider<JsonCustomReportDashboardBuilder> builderProvider;

    public RequirementLibraryViewController(
            RequirementDisplayService requirementDisplayService,
            RequirementLibraryNavigationService requirementLibraryNavigationService,
            CustomReportLibraryNodeService customReportLibraryNodeService,
            Provider<JsonCustomReportDashboardBuilder> builderProvider) {
        this.requirementDisplayService = requirementDisplayService;
        this.requirementLibraryNavigationService = requirementLibraryNavigationService;
        this.customReportLibraryNodeService = customReportLibraryNodeService;
        this.builderProvider = builderProvider;
    }

    @GetMapping("/{requirementLibraryId}")
    public RequirementLibraryDto getRequirementLibrary(
            @PathVariable long requirementLibraryId,
            Locale locale,
            @RequestParam boolean extendHighLvlReqScope) {
        RequirementLibraryDto dto = requirementDisplayService.findLibrary(requirementLibraryId);
        if (dto.isShouldShowFavoriteDashboard()) {
            if (dto.isCanShowFavoriteDashboard()) {
                EntityReference library =
                        new EntityReference(EntityType.REQUIREMENT_LIBRARY, requirementLibraryId);
                CustomReportDashboard dashboard =
                        customReportLibraryNodeService.findCustomReportDashboardById(
                                dto.getFavoriteDashboardId());
                CustomReportDashboardDto dashboardDto =
                        new CustomReportDashboardDto(
                                Workspace.REQUIREMENT,
                                Collections.singletonList(library),
                                false,
                                extendHighLvlReqScope,
                                false);
                JsonCustomReportDashboard jsonDashboard =
                        builderProvider
                                .get()
                                .build(dto.getFavoriteDashboardId(), dashboard, locale, dashboardDto);
                dto.setDashboard(jsonDashboard);
            }
        } else {
            dto.setStatistics(
                    requirementLibraryNavigationService.getStatisticsForSelection(
                            Collections.singleton(requirementLibraryId),
                            Collections.emptyList(),
                            extendHighLvlReqScope));
        }

        return dto;
    }
}
