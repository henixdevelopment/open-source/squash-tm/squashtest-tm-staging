/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.actionword;

import static org.squashtest.tm.api.security.acls.Roles.ROLE_ADMIN;
import static org.squashtest.tm.jooq.domain.Tables.ACTION_WORD_LIBRARY;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;

import javax.inject.Inject;
import org.jooq.DSLContext;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.actionword.ActionWordLibrary;
import org.squashtest.tm.service.internal.display.dto.LibraryDto;
import org.squashtest.tm.service.internal.display.dto.actionword.ActionWordLibraryDto;
import org.squashtest.tm.service.internal.repository.display.AttachmentDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;
import org.squashtest.tm.service.security.PermissionEvaluationService;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ActionWordLibraryDtoBuilder {

    @Inject private PermissionEvaluationService permissionEvaluationService;
    @Inject private AttachmentDisplayDao attachmentDisplayDao;
    @Inject private DSLContext dslContext;

    public LibraryDto build(ActionWordLibrary actionWordLibrary) {
        LibraryDto actionWordLibraryDto =
                dslContext
                        .select(
                                ACTION_WORD_LIBRARY.AWL_ID.as(RequestAliasesConstants.ID),
                                ACTION_WORD_LIBRARY.ATTACHMENT_LIST_ID,
                                PROJECT.PROJECT_ID,
                                PROJECT.DESCRIPTION,
                                PROJECT.NAME)
                        .from(ACTION_WORD_LIBRARY)
                        .innerJoin(PROJECT)
                        .on(PROJECT.AWL_ID.eq(ACTION_WORD_LIBRARY.AWL_ID))
                        .where(ACTION_WORD_LIBRARY.AWL_ID.eq(actionWordLibrary.getId()))
                        .fetchOne()
                        .into(LibraryDto.class);
        actionWordLibraryDto.setAttachmentList(
                attachmentDisplayDao.findAttachmentListById(actionWordLibraryDto.getAttachmentListId()));
        return actionWordLibraryDto;
    }

    public void fillBasicAttributes(
            ActionWordLibrary actionWordLibrary, ActionWordLibraryDto actionWordLibraryDto) {
        actionWordLibraryDto.setName(actionWordLibrary.getProject().getName());
        actionWordLibraryDto.setDescription(
                HTMLCleanupUtils.htmlToText(actionWordLibrary.getProject().getDescription()));
    }

    public void fillAttachableAttributes(
            ActionWordLibrary actionWordLibrary, ActionWordLibraryDto actionWordLibraryDto) {
        actionWordLibraryDto.setAttachable(
                permissionEvaluationService.hasRoleOrPermissionOnObject(
                        ROLE_ADMIN, "ATTACH", actionWordLibrary));
        actionWordLibraryDto.setAttachmentList(actionWordLibrary.getAttachmentList());
    }
}
