/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.execution;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.execution.SessionNoteKind;
import org.squashtest.tm.service.execution.ExecutionModificationService;
import org.squashtest.tm.service.internal.display.dto.execution.SessionNoteDto;

@RestController
@RequestMapping("/backend/session-note")
public class SessionNoteController {

    private final ExecutionModificationService executionModificationService;

    public SessionNoteController(ExecutionModificationService executionModificationService) {
        this.executionModificationService = executionModificationService;
    }

    @PostMapping(value = "/create")
    SessionNoteDto createNote(
            @RequestBody SessionNotePatch sessionNotePatch,
            @RequestParam("executionId") Long executionId) {
        SessionNoteKind noteKind = SessionNoteKind.valueOf(sessionNotePatch.getNoteKind());
        String noteContent = sessionNotePatch.getNoteContent();
        Integer noteOrder = sessionNotePatch.getNoteOrder();
        return executionModificationService.createSessionNote(
                executionId, noteKind, noteContent, noteOrder);
    }

    @PostMapping(value = "{sessionNoteId}/kind")
    public SessionNoteDto updateNoteKind(
            @RequestBody SessionNotePatch sessionNotePatch, @PathVariable long sessionNoteId) {
        SessionNoteKind newKind = SessionNoteKind.valueOf(sessionNotePatch.getNoteKind());
        return executionModificationService.updateSessionNoteKind(sessionNoteId, newKind);
    }

    @PostMapping(value = "{sessionNoteId}/content")
    public SessionNoteDto updateNoteContent(
            @RequestBody SessionNotePatch sessionNotePatch, @PathVariable long sessionNoteId) {
        return executionModificationService.updateSessionNoteContent(
                sessionNoteId, sessionNotePatch.getNoteContent());
    }

    @DeleteMapping(value = "{sessionNoteId}")
    public void deleteNote(@PathVariable long sessionNoteId) {
        executionModificationService.deleteSessionNote(sessionNoteId);
    }

    public static class SessionNotePatch {

        private String noteKind;
        private String noteContent;
        private Integer noteOrder;

        public String getNoteKind() {
            return noteKind;
        }

        public void setNoteKind(String noteKind) {
            this.noteKind = noteKind;
        }

        public String getNoteContent() {
            return noteContent;
        }

        public void setNoteContent(String noteContent) {
            this.noteContent = noteContent;
        }

        public Integer getNoteOrder() {
            return noteOrder;
        }

        public void setNoteOrder(Integer noteOrder) {
            this.noteOrder = noteOrder;
        }
    }
}
