/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.test.automation.server.environments.unrestricted;

import java.util.Map;
import java.util.Objects;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.display.test.automation.server.TestAutomationServerDisplayService;
import org.squashtest.tm.service.internal.display.dto.automatedexecutionenvironments.EnvironmentSelectionPanelDto;
import org.squashtest.tm.service.internal.display.dto.automatedexecutionenvironments.ExecutionEnvironmentCountDto;
import org.squashtest.tm.service.project.GenericProjectFinder;
import org.squashtest.tm.service.servers.StoredCredentialsManager;
import org.squashtest.tm.service.testautomation.environment.AutomatedExecutionEnvironmentService;
import org.squashtest.tm.web.backend.controller.form.model.StringList;
import org.squashtest.tm.web.backend.controller.test.automation.server.environments.AbstractTAEnvironmentsController;

/**
 * Controller for operations on automated execution environments that are accessible to all users.
 */
@RestController
@RequestMapping("/backend/test-automation/")
public class UnrestrictedTAEnvironmentsController extends AbstractTAEnvironmentsController {
    private static final String ITERATION = "iteration";
    private static final String TEST_SUITE = "test-suite";

    public UnrestrictedTAEnvironmentsController(
            AutomatedExecutionEnvironmentService automatedExecutionEnvironmentService,
            TestAutomationServerDisplayService testAutomationServerDisplayService,
            GenericProjectFinder genericProjectFinder,
            StoredCredentialsManager storedCredentialsManager) {
        super(
                automatedExecutionEnvironmentService,
                storedCredentialsManager,
                genericProjectFinder,
                testAutomationServerDisplayService);
    }

    @GetMapping(value = "{projectId}/available-tags")
    public StringList getAllAvailableProjectEnvironmentTags(@PathVariable long projectId) {
        Long testAutomationServerId = getTestAutomationServerIdUnsecured(projectId);
        return getAvailableProjectEnvironmentTags(projectId, testAutomationServerId);
    }

    @GetMapping(value = "{projectId}/automated-execution-environments/all")
    public EnvironmentSelectionPanelDto getProjectEnvironments(@PathVariable Long projectId) {
        Long testAutomationServerId = getTestAutomationServerIdUnsecured(projectId);
        return getProjectEnvironmentsPanel(projectId, testAutomationServerId);
    }

    @GetMapping(value = "{entityType}/{entityId}/automated-execution-environments-statuses-count")
    public ExecutionEnvironmentCountDto getAutomatedExecutionEnvironmentsStatusesCountForEntity(
            @PathVariable String entityType, @PathVariable Long entityId) {
        if (!TEST_SUITE.equals(entityType) && !ITERATION.equals(entityType)) {
            throw new IllegalArgumentException(
                    "Invalid entity type, expected 'test-suite' or 'iteration', got " + entityType);
        }
        Map<Long, Long> projectAndServerIds =
                entityType.equals(ITERATION)
                        ? testAutomationServerDisplayService.findProjectAndServerIdsByIterationId(entityId)
                        : testAutomationServerDisplayService.findProjectAndServerIdsByTestSuiteId(entityId);
        return automatedExecutionEnvironmentService.getExecutionEnvironmentCountDto(
                projectAndServerIds);
    }

    private Long getTestAutomationServerIdUnsecured(long genericProjectId) {
        return Objects.requireNonNull(
                testAutomationServerDisplayService.findTestAutomationServerIdByGenericProjectIdUnsecured(
                        genericProjectId),
                String.format(
                        "Expect project with ID %d to have a test automation server bound.", genericProjectId));
    }
}
