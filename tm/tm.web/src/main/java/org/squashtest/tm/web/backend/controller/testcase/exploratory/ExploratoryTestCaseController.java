/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.testcase.exploratory;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.testcase.exploratory.ExploratoryTestCaseService;

@RestController
@RequestMapping("backend/test-case/{testCaseId}")
public class ExploratoryTestCaseController {

    private final ExploratoryTestCaseService exploratoryTestCaseService;

    public ExploratoryTestCaseController(ExploratoryTestCaseService exploratoryTestCaseService) {
        this.exploratoryTestCaseService = exploratoryTestCaseService;
    }

    @PostMapping(path = "/charter")
    public void updateTestCaseCharter(
            @PathVariable long testCaseId,
            @RequestBody ExploratoryTestCasePatch exploratoryTestCasePatch) {
        exploratoryTestCaseService.updateCharter(testCaseId, exploratoryTestCasePatch.getCharter());
    }

    @PostMapping(path = "/session-duration")
    public void updateTestCaseSessionDuration(
            @PathVariable long testCaseId,
            @RequestBody ExploratoryTestCasePatch exploratoryTestCasePatch) {
        exploratoryTestCaseService.updateSessionDuration(
                testCaseId, exploratoryTestCasePatch.getDurationInMinutes());
    }

    public static class ExploratoryTestCasePatch {

        private String charter;
        private int durationInMinutes;

        public String getCharter() {
            return charter;
        }

        public void setCharter(String charter) {
            this.charter = charter;
        }

        public int getDurationInMinutes() {
            return durationInMinutes;
        }

        public void setDurationInMinutes(int durationInMinutes) {
            this.durationInMinutes = durationInMinutes;
        }
    }
}
