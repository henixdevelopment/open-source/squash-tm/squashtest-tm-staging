/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.testcase.steps;

import java.util.Objects;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.bdd.Keyword;
import org.squashtest.tm.service.display.testcase.TestCaseDisplayService;
import org.squashtest.tm.service.internal.display.testcase.parameter.TestCaseParameterOperationReport;
import org.squashtest.tm.service.internal.display.testcase.teststep.TestStepActionWordOperationReport;
import org.squashtest.tm.service.testcase.TestCaseModificationService;
import org.squashtest.tm.service.testcase.TestStepModificationService;

// XSS OK
@RestController
@RequestMapping("backend/test-steps/{testStepId}")
public class TestStepModificationController {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(TestStepModificationController.class);

    private final TestStepModificationService testStepService;

    private final TestCaseModificationService testCaseModificationService;

    private final TestCaseDisplayService testCaseDisplayService;

    public TestStepModificationController(
            TestStepModificationService testStepService,
            TestCaseDisplayService testCaseDisplayService,
            TestCaseModificationService testCaseModificationService) {
        this.testStepService = testStepService;
        this.testCaseDisplayService = testCaseDisplayService;
        this.testCaseModificationService = testCaseModificationService;
    }

    /**
     * update the TestStep infos
     *
     * @param testStepId
     * @param testStepModel
     */
    @PostMapping(headers = {"Content-Type=application/json"})
    public TestCaseParameterOperationReport updateStep(
            @PathVariable Long testStepId, @RequestBody TestStepUpdateFormModel testStepModel) {
        testStepService.updateTestStep(
                testStepId,
                testStepModel.getAction(),
                testStepModel.getExpectedResult(),
                testStepModel.getCufs());
        return testCaseDisplayService.findParametersDataByTestStepId(testStepId);
    }

    @PostMapping(value = "/keyword")
    public void changeStepKeyword(
            @PathVariable long testStepId, @RequestBody KeywordStepUpdateModel testStepModel) {
        Keyword updatedKeyword = Keyword.valueOf(testStepModel.getKeyword());
        testStepService.updateKeywordTestStep(testStepId, updatedKeyword);
        LOGGER.trace("TestCaseModificationController : updated keyword for step {}", testStepId);
    }

    @PostMapping(value = "/action-word")
    public TestStepActionWordOperationReport changeStepActionWord(
            @PathVariable long testStepId, @RequestBody KeywordStepUpdateModel testStepModel) {
        LOGGER.trace("TestCaseModificationController : updated action word for step {}", testStepId);
        if (Objects.isNull(testStepModel.getActionWordId())) {
            return testCaseModificationService.updateKeywordTestStep(
                    testStepId, testStepModel.getAction());
        } else {
            LOGGER.trace(
                    "TestCaseModificationController : updated action word for step {} with action word id {}",
                    testStepId,
                    testStepModel.getActionWordId());
            return testCaseModificationService.updateKeywordTestStep(
                    testStepId, testStepModel.getAction(), testStepModel.getActionWordId());
        }
    }

    @PostMapping(value = "/datatable")
    public void changeDatatable(
            @PathVariable long testStepId, @RequestBody KeywordStepUpdateModel testStepModel) {
        testCaseModificationService.updateKeywordTestStepDatatable(
                testStepId, testStepModel.getDatatable());
        LOGGER.trace("TestCaseModificationController : updated datatable for step {}", testStepId);
    }

    @PostMapping(value = "/docstring")
    public void changeDocstring(
            @PathVariable long testStepId, @RequestBody KeywordStepUpdateModel testStepModel) {
        testCaseModificationService.updateKeywordTestStepDocstring(
                testStepId, testStepModel.getDocstring());
        LOGGER.trace("TestCaseModificationController : updated docstring for step {}", testStepId);
    }

    @PostMapping(value = "/comment")
    public void changeComment(
            @PathVariable long testStepId, @RequestBody KeywordStepUpdateModel testStepModel) {
        testCaseModificationService.updateKeywordTestStepComment(
                testStepId, testStepModel.getComment());
        LOGGER.trace("TestCaseModificationController : updated comment for step {}", testStepId);
    }
}
