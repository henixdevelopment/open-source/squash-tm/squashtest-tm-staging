/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.profile;

import java.util.List;
import javax.inject.Inject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.display.profile.ProfileDisplayService;
import org.squashtest.tm.service.internal.display.dto.PermissionsDto;
import org.squashtest.tm.service.internal.display.dto.ProfileActivePermissionsRecord;
import org.squashtest.tm.service.internal.display.dto.ProfileAdminViewDto;
import org.squashtest.tm.service.profile.ProfileManagerService;

@RestController
@RequestMapping("/backend/profile-view")
public class ProfileViewController {

    private final ProfileDisplayService profileDisplayService;
    private final ProfileManagerService profileManagerService;

    @Inject
    ProfileViewController(
            ProfileDisplayService profileDisplayService, ProfileManagerService profileManagerService) {
        this.profileDisplayService = profileDisplayService;
        this.profileManagerService = profileManagerService;
    }

    @GetMapping("/{profileId}")
    public ProfileAdminViewDto getProfileView(@PathVariable long profileId) {
        return profileDisplayService.getProfileView(profileId);
    }

    @PostMapping("/{profileId}/deactivate")
    public void deactivateProfile(@PathVariable long profileId) {
        profileManagerService.deactivateProfile(profileId);
    }

    @PostMapping("/{profileId}/activate")
    public void activateProfile(@PathVariable long profileId) {
        profileManagerService.activateProfile(profileId);
    }

    @PostMapping("/{profileId}/description")
    public void changeDescription(@PathVariable long profileId, @RequestBody ProfileRecord record) {
        profileManagerService.changeDescription(profileId, record.description);
    }

    @PostMapping("/{profileId}/name")
    public void changeName(@PathVariable long profileId, @RequestBody ProfileRecord record) {
        profileManagerService.changeName(profileId, record.name);
    }

    @PostMapping("/{profileId}/permissions")
    public void changePermissions(
            @PathVariable long profileId, @RequestBody List<ProfileActivePermissionsRecord> records) {
        profileManagerService.changePermissions(profileId, records);
    }

    @GetMapping("/{profileId}/permissions")
    public List<PermissionsDto> getPermissions(@PathVariable long profileId) {
        return profileDisplayService.getPermissions(profileId);
    }

    public record ProfileRecord(String name, String description) {}
}
