/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.jasperreports.processors;

import java.awt.font.TextAttribute;
import java.text.AttributedCharacterIterator.Attribute;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import javax.swing.JEditorPane;
import javax.swing.text.AbstractDocument.LeafElement;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTML.Tag;
import javax.swing.text.html.HTMLDocument.RunElement;
import net.sf.jasperreports.engine.base.JRBasePrintHyperlink;
import net.sf.jasperreports.engine.type.HyperlinkTypeEnum;
import net.sf.jasperreports.engine.util.HtmlEditorKitMarkupProcessor;
import net.sf.jasperreports.engine.util.JRStyledText;
import net.sf.jasperreports.engine.util.JRStyledText.Run;
import net.sf.jasperreports.engine.util.JRStyledTextParser;
import net.sf.jasperreports.engine.util.JRTextAttribute;
import net.sf.jasperreports.engine.util.MarkupProcessor;
import net.sf.jasperreports.engine.util.MarkupProcessorFactory;
import net.sf.jasperreports.engine.util.StyledTextListInfo;
import net.sf.jasperreports.engine.util.StyledTextListItemInfo;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;

/**
 * As a solution to issue https://ci.squashtest.org/mantis/view.php?id=2293 this implementation will
 * handle &lt;strong&gt; and &lt;entityManager&gt; instead of their obsolete versions. This
 * implementation must be supplied in Jasper Report configuration, like
 * net.sf.jasperreports.markup.processor.factory.html=org.squashtest.tm.web.internal.controller.report.CustomHtmlProcessorFactory
 *
 * @author bsiri
 */
public class CustomHtmlProcessorFactory extends HtmlEditorKitMarkupProcessor
        implements MarkupProcessorFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomHtmlProcessorFactory.class);
    private static CustomHtmlProcessorFactory custom_instance;
    private Document document;
    private boolean bodyOccurred = false;
    private Stack<StyledTextListInfo> htmlListStack;
    private boolean insideLi;
    private boolean liStart;

    @Override
    public MarkupProcessor createMarkupProcessor() {
        if (custom_instance == null) {
            custom_instance = new CustomHtmlProcessorFactory();
        }
        return custom_instance;
    }

    // slightly scrapped from JEditorPanelHtmlMarkupProcessor
    @Override
    protected Map<Attribute, Object> getAttributes(AttributeSet attrSet) {

        Map<Attribute, Object> attributes = super.getAttributes(attrSet);

        // checks for attributes WEIGHT and POSTURE. If they were not set, checks whether some HTML.Tag
        // named "strong" of "entityManager" exists in the
        // attribute set.

        if (!attributes.containsKey(TextAttribute.WEIGHT) && hasHtmlTag(attrSet, HTML.Tag.STRONG)) {
            attributes.put(TextAttribute.WEIGHT, TextAttribute.WEIGHT_BOLD);
        }

        if (!attributes.containsKey(TextAttribute.POSTURE) && hasHtmlTag(attrSet, HTML.Tag.EM)) {
            attributes.put(TextAttribute.POSTURE, TextAttribute.POSTURE_OBLIQUE);
        }

        return attributes;
    }

    public boolean hasHtmlTag(AttributeSet attrSet, HTML.Tag tag) {
        Enumeration<?> attrNames = attrSet.getAttributeNames();
        while (attrNames.hasMoreElements()) {
            Object obj = attrNames.nextElement();
            if (tag.equals(obj)) {
                return true;
            }
        }
        return false;
    }

    // NOSONAR:START
    // COPY PASTA FROM JEditorPaneHtmlMarkupProcessor to correct bug 2411
    @Override
    public String convert(String srcText) {
        if (srcText.indexOf(60) < 0 && srcText.indexOf(38) < 0) {
            return srcText;
        } else {
            JRStyledText styledText = new JRStyledText();
            this.htmlListStack = new Stack();
            JEditorPane editorPane = new JEditorPane("text/html", srcText);
            editorPane.setEditable(false);
            this.document = editorPane.getDocument();
            this.bodyOccurred = false;
            Element root = this.document.getDefaultRootElement();
            if (root != null) {
                processElement(styledText, root);
            }

            styledText.setGlobalAttributes(new HashMap());
            return JRStyledTextParser.getInstance().write(styledText);
        }
    }

    // NOSONAR:END
    // END COPY PASTA FROM JEditorPaneHtmlMarkupProcessor to correct bug 2411
    // upgrade jasperreports to 6.20.0 : Not sure bug 2411 is still there, but there is a problem with
    // newlines (\n)
    private void processElement(JRStyledText styledText, Element parentElement) {
        for (int i = 0; i < parentElement.getElementCount(); ++i) {
            Element element = parentElement.getElement(i);
            AttributeSet attrs = element.getAttributes();
            Object elementName = attrs.getAttribute("$ename");
            Object object = elementName != null ? null : attrs.getAttribute(StyleConstants.NameAttribute);
            if (object instanceof Tag htmlTag) {
                if (htmlTag == Tag.BODY) {
                    this.bodyOccurred = true;
                    this.processElement(styledText, element);
                } else if (htmlTag == Tag.BR) {
                    styledText.append("\n");
                    int startIndex = styledText.length();
                    this.resizeRuns(styledText.getRuns(), startIndex, 1);
                    this.processElement(styledText, element);
                    styledText.addRun(new Run(new HashMap(), startIndex, styledText.length()));
                    if (startIndex < styledText.length()) {
                        styledText.append("\n");
                        this.resizeRuns(styledText.getRuns(), startIndex, 1);
                    }
                } else {
                    StyledTextListInfo htmlList;
                    if (htmlTag != Tag.OL && htmlTag != Tag.UL) {
                        if (htmlTag == Tag.LI) {
                            Map<Attribute, Object> styleAttrs = new HashMap<>();
                            boolean ulAdded = false;
                            if (this.htmlListStack.isEmpty()) {
                                htmlList = new StyledTextListInfo(false, null, null, false);
                                this.htmlListStack.push(htmlList);
                                styleAttrs.put(
                                        JRTextAttribute.HTML_LIST,
                                        this.htmlListStack.toArray(new StyledTextListInfo[this.htmlListStack.size()]));
                                styleAttrs.put(
                                        JRTextAttribute.HTML_LIST_ITEM, StyledTextListItemInfo.NO_LIST_ITEM_FILLER);
                                ulAdded = true;
                            } else {
                                htmlList = this.htmlListStack.peek();
                            }

                            htmlList.setItemCount(htmlList.getItemCount() + 1);
                            this.insideLi = true;
                            this.liStart = true;
                            styleAttrs.put(
                                    JRTextAttribute.HTML_LIST_ITEM,
                                    new StyledTextListItemInfo(htmlList.getItemCount() - 1));
                            int startIndex = styledText.length();
                            this.processElement(styledText, element);
                            styledText.addRun(new Run(styleAttrs, startIndex, styledText.length()));
                            this.insideLi = false;
                            this.liStart = false;

                            if (ulAdded) {
                                this.htmlListStack.pop();
                            }
                        } else if (element instanceof LeafElement) {
                            String chunk = null;

                            try {
                                chunk =
                                        this.document.getText(
                                                element.getStartOffset(),
                                                element.getEndOffset() - element.getStartOffset());
                            } catch (BadLocationException var15) {
                                LOGGER.debug("Error converting markup.", var15);
                            }

                            if (chunk
                                    != null) { // problem with newline, all text was on one line, so I've removed the
                                // condition on \n
                                this.liStart = false;
                                int startIndex = styledText.length();
                                styledText.append(chunk);
                                Map<Attribute, Object> styleAttributes =
                                        this.getAttributes(element.getAttributes());
                                if (element instanceof RunElement runElement) {
                                    AttributeSet attrSet = (AttributeSet) runElement.getAttribute(Tag.A);
                                    if (attrSet != null) {
                                        JRBasePrintHyperlink hyperlink = new JRBasePrintHyperlink();
                                        hyperlink.setHyperlinkType(HyperlinkTypeEnum.REFERENCE);
                                        hyperlink.setHyperlinkReference(
                                                (String) attrSet.getAttribute(javax.swing.text.html.HTML.Attribute.HREF));
                                        hyperlink.setLinkTarget(
                                                (String) attrSet.getAttribute(javax.swing.text.html.HTML.Attribute.TARGET));
                                        styleAttributes.put(JRTextAttribute.HYPERLINK, hyperlink);
                                    }
                                }

                                styledText.addRun(new Run(styleAttributes, startIndex, styledText.length()));
                            }
                        } else if (this.bodyOccurred) {
                            this.processElement(styledText, element);
                        }
                    } else {
                        Object type = attrs.getAttribute(javax.swing.text.html.HTML.Attribute.TYPE);
                        Object htmlListObj = attrs.getAttribute(javax.swing.text.html.HTML.Attribute.START);
                        htmlList =
                                new StyledTextListInfo(
                                        htmlTag == Tag.OL,
                                        htmlTag == Tag.OL && type != null ? String.valueOf(type) : null,
                                        htmlTag == Tag.OL && htmlListObj != null
                                                ? Integer.valueOf(htmlListObj.toString())
                                                : null,
                                        this.insideLi);
                        htmlList.setAtLiStart(this.liStart);
                        this.htmlListStack.push(htmlList);
                        this.insideLi = false;
                        Map<Attribute, Object> styleAttrs = new HashMap();
                        styleAttrs.put(
                                JRTextAttribute.HTML_LIST,
                                this.htmlListStack.toArray(new StyledTextListInfo[this.htmlListStack.size()]));
                        styleAttrs.put(
                                JRTextAttribute.HTML_LIST_ITEM, StyledTextListItemInfo.NO_LIST_ITEM_FILLER);
                        int startIndex = styledText.length();
                        this.processElement(styledText, element);
                        styledText.addRun(new Run(styleAttrs, startIndex, styledText.length()));
                    }
                }
            }
        }
        // FIX FOR [Issue 2411]
        List<Run> runs = styledText.getRuns();
        for (Run run : runs) {
            if (run.endIndex > styledText.length()) {
                run.endIndex = styledText.length();
            }
        }
        // END FIX FOR  [Issue 2411]
    }

    private void resizeRuns(List<Run> runs, int startIndex, int count) {
        for (Run value : runs) {
            if (value.startIndex <= startIndex && value.endIndex > startIndex - count) {
                value.endIndex += count;
            }
        }
    }
}
