/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.service.user.UserAdministrationService;

/**
 * Handle redirections when hitting "/" endpoint.
 *
 * <p>It accommodates both internal (default login page) and external authentication (such as SAML)
 * by using either query params or session attributes.
 *
 * <p>From Squash 2.0, authentication success callbacks should redirect to "/" in order to benefit
 * from redirection after auth.
 */
@Controller
public class RootController {
    public static final String REDIRECT_AFTER_AUTH = "redirect-after-auth";

    public static final String INTERNAL_LOGIN_URL = "/login";

    private static final Logger LOGGER = LoggerFactory.getLogger(RootController.class);

    private final UserAdministrationService userAdministrationService;

    @Value("${squash.security.preferred-auth-url:/login}")
    private String preferredAuthUrl = INTERNAL_LOGIN_URL;

    public RootController(UserAdministrationService userAdministrationService) {
        this.userAdministrationService = userAdministrationService;
    }

    @GetMapping("/")
    public String mainEntryPoint(
            HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession httpSession) {
        final boolean authenticated = isAuthenticated();

        if (authenticated) {
            String redirectUrl = restoreRedirectionUrl(request, httpSession);

            if (!userAdministrationService.findPostLoginInformation().isEmpty()) {
                redirectUrl = String.format("/information?%s=%s", REDIRECT_AFTER_AUTH, redirectUrl);
            }

            LOGGER.info(
                    "Received GET request on \"/\". User is authenticated, redirecting to {}", redirectUrl);
            return "redirect:" + redirectUrl;
        } else {
            // Forward the original request parameters as they are used for redirect after successful auth
            // in the case internal login is used
            redirectAttributes.addAllAttributes(request.getParameterMap());

            // For other authentication workflows, we need to store the redirection in the session.
            // The session will be inspected on next request to "/".
            httpSession.setAttribute(
                    REDIRECT_AFTER_AUTH, request.getParameterMap().get(REDIRECT_AFTER_AUTH));

            LOGGER.info(
                    "Received GET request on \"/\". User is not authenticated, redirecting to {}",
                    preferredAuthUrl);
            return "redirect:" + preferredAuthUrl;
        }
    }

    private String restoreRedirectionUrl(HttpServletRequest request, HttpSession httpSession) {
        // Default to /home-workspace or /automation-workspace
        String redirectUrl =
                userAdministrationService.getDefaultAuthenticatedRedirectUrlForUserAuthority();

        final String[] redirectInQueryParams = request.getParameterMap().get(REDIRECT_AFTER_AUTH);

        // If there's no redirection set in query params...
        if (redirectInQueryParams == null || redirectInQueryParams.length == 0) {
            final Object redirectInSession = httpSession.getAttribute(REDIRECT_AFTER_AUTH);

            // ... But there's a redirection stored in session...
            if (redirectInSession instanceof String[] && ((String[]) redirectInSession).length > 0) {
                // ... Remove session attribute and redirect to this page instead
                httpSession.removeAttribute(REDIRECT_AFTER_AUTH);
                redirectUrl = ((String[]) redirectInSession)[0];

                LOGGER.info(
                        "Found a redirect-after-auth attribute is session. Redirect url is now set to {}",
                        redirectUrl);
            }
        }

        return redirectUrl;
    }

    private boolean isAuthenticated() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();
        final boolean isAnonymous = authentication instanceof AnonymousAuthenticationToken;
        return authentication != null && authentication.isAuthenticated() && !isAnonymous;
    }
}
