/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.security.authentication;

import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;

@Component
public class TraceFailedAuthentication
        implements ApplicationListener<AbstractAuthenticationFailureEvent> {
    private static final Logger LOGGER = LoggerFactory.getLogger(TraceFailedAuthentication.class);

    @Override
    public void onApplicationEvent(AbstractAuthenticationFailureEvent event) {
        final Object details = event.getAuthentication().getDetails();

        if (details instanceof WebAuthenticationDetails) {
            final String ipAddress = ((WebAuthenticationDetails) details).getRemoteAddress();
            LOGGER.info("Failed authentication from remote IP {}", ipAddress);
        }
    }
}
