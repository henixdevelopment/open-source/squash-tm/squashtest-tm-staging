/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.importer.testcase;

import java.io.File;
import javax.inject.Inject;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.service.testcase.TestCaseLibraryNavigationService;
import org.squashtest.tm.web.backend.controller.importer.AbstractImportHelper;
import org.squashtest.tm.web.backend.controller.importer.ImportExcelResponse;

/**
 * @author Gregory Fouquet
 */
// XSS OK
@RestController
@RequestMapping("backend/test-cases/importer")
public class TestCaseImportController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestCaseImportController.class);

    @Inject private TestCaseLibraryNavigationService navigationService;

    @Inject private TestCaseImportHelper importHelper;

    /**
     * Will simulate import of test cases in a one xls file format.
     *
     * @see TestCaseLibraryNavigationService#simulateImportExcelTestCase(File)
     * @param uploadedFile : the xls file to import in a {@link MultipartFile} form
     * @param request : the {@link WebRequest}
     * @return a {@link ModelAndView} containing the summary of the import and the link to a complete
     *     log for any invalid information it contains
     */
    @PostMapping(value = "/xls", params = "dry-run")
    public ImportExcelResponse dryRunExcelWorkbook(
            @RequestParam("archive") MultipartFile uploadedFile, WebRequest request) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("dryRunExcelWorkbook");
        }
        return importHelper.importWorkbook(
                AbstractImportHelper.TEST_CASES,
                uploadedFile,
                xls -> navigationService.simulateImportExcelTestCase(xls));
    }

    /**
     * Will import test cases in a one xls file format.
     *
     * @see TestCaseLibraryNavigationService#performImportExcelTestCase(File)
     * @param uploadedFile : the xls file to import in a {@link MultipartFile} form
     * @return @return a {@link ModelAndView} containing the summary of the import and the link to a
     *     complete log for any invalid information it contains
     */
    @PostMapping(value = "/xls", params = "!dry-run")
    public ImportExcelResponse importExcelWorkbook(
            @RequestParam("archive") MultipartFile uploadedFile) {
        return importHelper.importWorkbook(
                AbstractImportHelper.TEST_CASES,
                uploadedFile,
                xls -> navigationService.performImportExcelTestCase(xls));
    }
}
