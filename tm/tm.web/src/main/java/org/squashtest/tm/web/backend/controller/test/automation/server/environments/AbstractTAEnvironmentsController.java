/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.test.automation.server.environments;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.servers.TokenAuthCredentials;
import org.squashtest.tm.service.display.test.automation.server.TestAutomationServerDisplayService;
import org.squashtest.tm.service.internal.display.dto.automatedexecutionenvironments.EnvironmentSelectionPanelDto;
import org.squashtest.tm.service.project.GenericProjectFinder;
import org.squashtest.tm.service.servers.StoredCredentialsManager;
import org.squashtest.tm.service.testautomation.environment.AutomatedExecutionEnvironmentService;
import org.squashtest.tm.service.testautomation.model.AutomatedExecutionEnvironment;
import org.squashtest.tm.service.testautomation.spi.InvalidSquashOrchestratorConfigurationException;
import org.squashtest.tm.service.testautomation.spi.OutdatedSquashOrchestratorException;
import org.squashtest.tm.service.testautomation.spi.TestAutomationException;
import org.squashtest.tm.web.backend.controller.form.model.StringList;

/**
 * Base class for automated execution environments controllers. Each subclass has a different
 * visibility based on their respective endpoint prefix (see configuration in {@link
 * org.squashtest.tm.web.config.WebSecurityConfig}).
 */
public abstract class AbstractTAEnvironmentsController {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(AbstractTAEnvironmentsController.class);
    protected final AutomatedExecutionEnvironmentService automatedExecutionEnvironmentService;
    protected final TestAutomationServerDisplayService testAutomationServerDisplayService;
    private final GenericProjectFinder genericProjectFinder;
    private final StoredCredentialsManager storedCredentialsManager;

    protected AbstractTAEnvironmentsController(
            AutomatedExecutionEnvironmentService automatedExecutionEnvironmentService,
            StoredCredentialsManager storedCredentialsManager,
            GenericProjectFinder genericProjectFinder,
            TestAutomationServerDisplayService testAutomationServerDisplayService) {
        this.automatedExecutionEnvironmentService = automatedExecutionEnvironmentService;
        this.storedCredentialsManager = storedCredentialsManager;
        this.genericProjectFinder = genericProjectFinder;
        this.testAutomationServerDisplayService = testAutomationServerDisplayService;
    }

    protected StringList getAvailableProjectEnvironmentTags(
            long genericProjectId, Long testAutomationServerId) {
        final Optional<TokenAuthCredentials> optionalProjectToken =
                findProjectToken(testAutomationServerId, genericProjectId);

        return getAvailableServerEnvironmentTags(
                testAutomationServerId, optionalProjectToken.orElse(null));
    }

    protected StringList getAvailableServerEnvironmentTags(
            Long testAutomationServerId, TokenAuthCredentials token) {
        final List<AutomatedExecutionEnvironment> environments =
                getAutomatedExecutionEnvironments(testAutomationServerId, token);
        return new StringList(extractAllTags(environments));
    }

    protected List<String> extractAllTags(List<AutomatedExecutionEnvironment> environments) {
        return environments.stream()
                .map(AutomatedExecutionEnvironment::getTags)
                .flatMap(Collection::stream)
                .distinct()
                .sorted()
                .toList();
    }

    public EnvironmentSelectionPanelDto getProjectEnvironmentsPanel(
            long genericProjectId, Long testAutomationServerId) {
        final boolean hasServerToken =
                testAutomationServerDisplayService.hasDefinedCredentials(testAutomationServerId);
        final Optional<TokenAuthCredentials> optionalProjectToken =
                findProjectToken(testAutomationServerId, genericProjectId);
        final boolean hasProjectToken = optionalProjectToken.isPresent();

        if (hasServerToken || hasProjectToken) {
            return getProjectEnvironmentPanelResponse(
                    genericProjectId,
                    testAutomationServerId,
                    optionalProjectToken.orElse(null),
                    hasServerToken,
                    hasProjectToken);
        } else {
            return EnvironmentSelectionPanelDto.forProjectWithoutCredentials(
                    testAutomationServerId, genericProjectId);
        }
    }

    protected List<AutomatedExecutionEnvironment> getAutomatedExecutionEnvironments(
            Long testAutomationServerId, TokenAuthCredentials optionalProjectToken) {
        return optionalProjectToken != null
                ? automatedExecutionEnvironmentService.getAllAccessibleEnvironments(
                        testAutomationServerId, optionalProjectToken)
                : automatedExecutionEnvironmentService.getAllAccessibleEnvironments(testAutomationServerId);
    }

    protected Optional<TokenAuthCredentials> findProjectToken(
            Long testAutomationServerId, long projectId) {
        return Optional.ofNullable(
                        storedCredentialsManager.findProjectCredentials(testAutomationServerId, projectId))
                .filter(TokenAuthCredentials.class::isInstance)
                .map(TokenAuthCredentials.class::cast);
    }

    private EnvironmentSelectionPanelDto getProjectEnvironmentPanelResponse(
            Long genericProjectId,
            Long testAutomationServerId,
            TokenAuthCredentials optionalProjectToken,
            boolean hasServerCredentials,
            boolean hasProjectToken) {
        final List<String> defaultTags =
                testAutomationServerDisplayService.getDefaultEnvironmentTags(testAutomationServerId);

        final List<String> projectTags =
                genericProjectFinder.getProjectEnvironmentTags(genericProjectId);

        final boolean areProjectTagsInherited =
                genericProjectFinder.isInheritsEnvironmentTags(genericProjectId);

        List<AutomatedExecutionEnvironment> allAccessibleEnvironments = null;
        String errorMessage = null;

        try {
            allAccessibleEnvironments =
                    getAutomatedExecutionEnvironments(testAutomationServerId, optionalProjectToken);
        } catch (InvalidSquashOrchestratorConfigurationException
                | OutdatedSquashOrchestratorException ex) {
            LOGGER.trace(
                    "Could not fetch available automated execution environments for project {}.",
                    genericProjectId,
                    ex);
            errorMessage = ex.getMessage();

        } catch (TestAutomationException ex) {
            LOGGER.trace(
                    "Could not fetch available automated execution environments for project {}.",
                    genericProjectId,
                    ex);
        }

        return EnvironmentSelectionPanelDto.forProject(
                testAutomationServerId,
                defaultTags,
                hasServerCredentials,
                allAccessibleEnvironments,
                genericProjectId,
                hasProjectToken,
                projectTags,
                areProjectTagsInherited,
                errorMessage);
    }
}
