/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.campaign;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.service.library.FolderModificationService;

@RestController
@RequestMapping("backend/campaign-folder/{campaignId}")
public class CampaignFolderModificationController {
    private final FolderModificationService<CampaignFolder> folderFolderModificationService;

    public CampaignFolderModificationController(
            FolderModificationService<CampaignFolder> folderFolderModificationService) {
        this.folderFolderModificationService = folderFolderModificationService;
    }

    @PostMapping(value = "/description")
    public void changeDescription(
            @PathVariable Long campaignId, @RequestBody CampaignFolderPatch patch) {
        this.folderFolderModificationService.updateFolderDescription(
                campaignId, patch.getDescription());
    }

    @PostMapping(value = "/name")
    public void changeName(@PathVariable Long campaignId, @RequestBody CampaignFolderPatch patch) {
        this.folderFolderModificationService.renameFolder(campaignId, patch.getName());
    }

    static class CampaignFolderPatch {
        private String name;
        private String description;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}
