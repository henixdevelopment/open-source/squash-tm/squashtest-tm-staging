/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.customreport;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.customreport.CustomReportCustomExportService;
import org.squashtest.tm.service.internal.display.dto.customreports.CustomReportCustomExportDto;

@RestController
@RequestMapping("/backend/custom-report-custom-export-view")
public class CustomReportCustomExportViewController {

    private final CustomReportCustomExportService customExportService;

    public CustomReportCustomExportViewController(
            CustomReportCustomExportService customExportService) {
        this.customExportService = customExportService;
    }

    @GetMapping("/{id}")
    public CustomReportCustomExportDto getCustomExportDetails(@PathVariable Long id) {
        CustomReportCustomExportDto dto = customExportService.findCustomExportDtoByNodeId(id);

        if (dto == null) {
            throw new IllegalArgumentException("Could not find a custom report with ID " + id);
        }

        return dto;
    }
}
