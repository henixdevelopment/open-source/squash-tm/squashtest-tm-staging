/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.execution;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.display.execution.ExecutionDisplayService;
import org.squashtest.tm.service.internal.display.dto.execution.ExecutionView;

@RestController
@RequestMapping("/backend/execution-runner/{executionId}")
public class ExecutionRunnerController {

    private final ExecutionDisplayService executionDisplayService;

    public ExecutionRunnerController(ExecutionDisplayService executionDisplayService) {
        this.executionDisplayService = executionDisplayService;
    }

    @GetMapping()
    public ExecutionView getExecutionViewForPopup(@PathVariable("executionId") long executionId) {
        return executionDisplayService.findOneWithoutBlockingMilestoneOrSprint(executionId);
    }
}
