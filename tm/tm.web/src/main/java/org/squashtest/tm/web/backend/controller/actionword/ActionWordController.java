/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.actionword;

import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.bdd.ActionWord;
import org.squashtest.tm.service.actionword.ActionWordGridService;
import org.squashtest.tm.service.actionword.ActionWordLibraryNodeService;
import org.squashtest.tm.service.actionword.ActionWordParameterService;
import org.squashtest.tm.service.actionword.ActionWordService;
import org.squashtest.tm.service.internal.display.dto.actionword.ActionWordDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;

// ULTIMATE
@RestController
@RequestMapping("backend/action-word-view")
public class ActionWordController {

    private final ActionWordDtoBuilder actionWordDtoBuilder;
    private final ActionWordLibraryNodeService actionWordLibraryNodeService;
    private final ActionWordService actionWordService;
    private final ActionWordGridService actionWordGridService;
    private final ActionWordParameterService actionWordParameterService;

    @Autowired
    public ActionWordController(
            ActionWordDtoBuilder actionWordDtoBuilder,
            ActionWordLibraryNodeService actionWordLibraryNodeService,
            Optional<ActionWordService> actionWordService,
            Optional<ActionWordGridService> actionWordGridService,
            ActionWordParameterService actionWordParameterService) {
        this.actionWordDtoBuilder = actionWordDtoBuilder;
        this.actionWordLibraryNodeService = actionWordLibraryNodeService;
        this.actionWordService = actionWordService.orElse(null);
        this.actionWordGridService = actionWordGridService.orElse(null);
        this.actionWordParameterService = actionWordParameterService;
    }

    @GetMapping("/{nodeId}")
    public ActionWordDto getActionWordView(@PathVariable long nodeId) {
        return actionWordDtoBuilder.build(
                actionWordLibraryNodeService.findActionWordByNodeId(nodeId), nodeId);
    }

    @PostMapping(value = "/{actionWordId}/description")
    public void updateActionWordDescription(
            @PathVariable long actionWordId, @RequestBody ActionWordPatch patch) {
        getActionWordService().changeDescription(actionWordId, patch.description());
    }

    public record ActionWordPatch(String description) {}

    @PostMapping(value = "/{parameterId}/parameter-default-value")
    public void updateActionWordParameterDefaultValue(
            @PathVariable long parameterId, @RequestBody ActionWordParameterPatch patch) {
        actionWordParameterService.updateParameterDefaultValue(parameterId, patch.defaultValue());
    }

    @PostMapping(value = "/{parameterId}/parameter-name")
    public void updateActionWordParameterName(
            @PathVariable long parameterId, @RequestBody ActionWordParameterPatch patch) {
        actionWordParameterService.renameParameter(parameterId, patch.name());
    }

    public record ActionWordParameterPatch(String defaultValue, String name) {}

    @GetMapping("/{nodeId}/title")
    public ActionWordTitleModel getActionWordTitle(@PathVariable long nodeId) {
        ActionWord actionWord = actionWordLibraryNodeService.findActionWordByNodeId(nodeId);
        return new ActionWordTitleModel(actionWord.createWord());
    }

    public record ActionWordTitleModel(String word) {}

    @PostMapping(value = "/{actionWordId}/implementation")
    public GridResponse getImplementation(
            @PathVariable long actionWordId, @RequestBody GridRequest requestParam) {
        return getActionWordGridService()
                .findAllImplementationByActionWordId(actionWordId, requestParam);
    }

    @PostMapping(value = "/{actionWordId}/using-test-cases")
    public GridResponse getUsingTestCases(
            @PathVariable long actionWordId, @RequestBody GridRequest requestParam) {
        return getActionWordGridService()
                .findAllUsingTestCasesByActionWordId(actionWordId, requestParam);
    }

    @PostMapping(value = "/{actionWordId}/parameters")
    public GridResponse getParameters(
            @PathVariable long actionWordId, @RequestBody GridRequest requestParam) {
        return getActionWordGridService().findAllParametersByActionWordId(actionWordId, requestParam);
    }

    private ActionWordGridService getActionWordGridService() {
        if (Objects.isNull(actionWordGridService)) {
            throw new AccessDeniedException("A dedicated plugin is required to manage action words");
        }
        return actionWordGridService;
    }

    private ActionWordService getActionWordService() {
        if (Objects.isNull(actionWordService)) {
            throw new AccessDeniedException("A dedicated plugin is required to manage action words");
        }
        return actionWordService;
    }
}
