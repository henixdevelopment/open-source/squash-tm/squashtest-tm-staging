/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.navigation;

import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeWorkspace;
import org.squashtest.tm.domain.library.Folder;
import org.squashtest.tm.domain.library.Library;
import org.squashtest.tm.domain.library.LibraryNode;
import org.squashtest.tm.service.deletion.Node;
import org.squashtest.tm.service.deletion.NodeMovement;
import org.squashtest.tm.service.deletion.NodeReferenceChanged;
import org.squashtest.tm.service.deletion.NodeRenaming;
import org.squashtest.tm.service.deletion.OperationReport;
import org.squashtest.tm.service.deletion.SuppressionPreviewReport;
import org.squashtest.tm.service.display.workspace.tree.TreeBrowser;
import org.squashtest.tm.service.internal.display.grid.TreeGridResponse;
import org.squashtest.tm.service.internal.display.grid.TreeRequest;
import org.squashtest.tm.service.library.LibraryNavigationService;
import org.squashtest.tm.web.backend.controller.RequestParams;
import org.squashtest.tm.web.backend.controller.form.model.RefreshTreeNodeModel;

public abstract class AbstractLibraryNavigationController<
        LIBRARY extends Library<? extends NODE>,
        FOLDER extends Folder<? extends NODE>,
        NODE extends LibraryNode> {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(AbstractLibraryNavigationController.class);

    protected final TreeBrowser treeBrowser;
    protected final MessageSource messageSource;

    protected AbstractLibraryNavigationController(
            TreeBrowser treeBrowser, MessageSource messageSource) {
        this.treeBrowser = treeBrowser;
        this.messageSource = messageSource;
    }

    @PostMapping()
    public TreeGridResponse getInitialRows(@RequestBody TreeRequest treeRequest) {
        return treeBrowser.getInitialTree(
                getWorkspace(),
                NodeReference.fromNodeIds(treeRequest.getOpenedNodes()),
                NodeReference.fromNodeIds(treeRequest.getSelectedNodes()));
    }

    @GetMapping(value = "/{ids}/content")
    public TreeGridResponse getChildren(@PathVariable List<String> ids) {
        Set<NodeReference> nodeReference = NodeReference.fromNodeIds(ids);
        return treeBrowser.findSubHierarchy(nodeReference, new HashSet<>(nodeReference));
    }

    @PostMapping(value = "/refresh")
    public TreeGridResponse refreshNodes(@RequestBody RefreshTreeNodeModel refreshTreeNodeModel) {
        return treeBrowser.findSubHierarchy(
                NodeReference.fromNodeIds(refreshTreeNodeModel.getNodeIds()),
                new HashSet<>(refreshTreeNodeModel.getNodeList().getNodeReferences()));
    }

    protected abstract LibraryNavigationService<LIBRARY, FOLDER, NODE> getLibraryNavigationService();

    protected abstract NodeWorkspace getWorkspace();

    @GetMapping(value = "/deletion-simulation/{nodeIds}")
    public Messages simulateNodeDeletion(
            @PathVariable(RequestParams.NODE_IDS) List<Long> nodeIds, Locale locale) {
        List<SuppressionPreviewReport> reportList =
                getLibraryNavigationService().simulateDeletion(nodeIds);
        Messages messages = new Messages();
        messages.addMessages(reportList, messageSource, locale);
        return messages;
    }

    @DeleteMapping(value = "/{nodeIds}")
    public OperationReport confirmNodeDeletion(
            @PathVariable(RequestParams.NODE_IDS) List<Long> nodeIds) {
        OperationReport report = getLibraryNavigationService().deleteNodes(nodeIds);
        logOperations(report);
        return report;
    }

    protected void logOperations(OperationReport report) {
        for (Node deletedNode : report.getRemoved()) {
            LOGGER.info("The node #{} was removed", deletedNode.getResid());
        }
        for (NodeMovement movedNode : report.getMoved()) {
            LOGGER.info(
                    "The nodes #{} were moved to node #{}",
                    movedNode.getMoved().stream().map(Node::getResid).toList(),
                    movedNode.getDest().getResid());
        }
        for (NodeRenaming renamedNode : report.getRenamed()) {
            LOGGER.info(
                    "The node #{} was renamed to {}",
                    renamedNode.getNode().getResid(),
                    renamedNode.getName());
        }
        for (NodeReferenceChanged nodeReferenceChanged : report.getReferenceChanges()) {
            LOGGER.info(
                    "The node #{} reference was changed to {}",
                    nodeReferenceChanged.getNode().getResid(),
                    nodeReferenceChanged.getReference());
        }
    }
}
