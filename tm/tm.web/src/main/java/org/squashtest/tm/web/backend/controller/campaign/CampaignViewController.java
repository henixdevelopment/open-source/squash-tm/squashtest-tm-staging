/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.campaign;

import java.util.Collections;
import java.util.Locale;
import javax.inject.Named;
import javax.inject.Provider;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.Workspace;
import org.squashtest.tm.domain.customreport.CustomReportDashboard;
import org.squashtest.tm.service.campaign.CampaignLibraryNavigationService;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;
import org.squashtest.tm.service.display.campaign.CampaignDisplayService;
import org.squashtest.tm.service.internal.display.dto.campaign.CampaignDto;
import org.squashtest.tm.service.internal.display.dto.customreports.CustomReportDashboardDto;
import org.squashtest.tm.service.internal.dto.json.JsonCustomReportDashboard;
import org.squashtest.tm.service.statistics.campaign.StatisticsBundle;
import org.squashtest.tm.web.backend.model.builder.JsonCustomReportDashboardBuilder;

@RestController
@RequestMapping("/backend/campaign-view")
public class CampaignViewController {

    private CampaignDisplayService campaignDisplayService;
    private CampaignLibraryNavigationService campaignLibraryNavigationService;
    private final CustomReportLibraryNodeService customReportLibraryNodeService;

    @Named("customReport.dashboardBuilder")
    private final Provider<JsonCustomReportDashboardBuilder> builderProvider;

    public CampaignViewController(
            CampaignDisplayService campaignDisplayService,
            CampaignLibraryNavigationService campaignLibraryNavigationService,
            CustomReportLibraryNodeService customReportLibraryNodeService,
            Provider<JsonCustomReportDashboardBuilder> builderProvider) {
        this.campaignDisplayService = campaignDisplayService;
        this.campaignLibraryNavigationService = campaignLibraryNavigationService;
        this.customReportLibraryNodeService = customReportLibraryNodeService;
        this.builderProvider = builderProvider;
    }

    @GetMapping(value = "/{campaignId}")
    public CampaignDto getCampaignView(@PathVariable long campaignId, Locale locale) {
        CampaignDto dto = campaignDisplayService.getCampaignView(campaignId);
        if (dto.isShouldShowFavoriteDashboard() && dto.isCanShowFavoriteDashboard()) {
            EntityReference library = new EntityReference(EntityType.CAMPAIGN, campaignId);
            CustomReportDashboard dashboard =
                    customReportLibraryNodeService.findCustomReportDashboardById(
                            dto.getFavoriteDashboardId());
            CustomReportDashboardDto dashboardScopeDto =
                    new CustomReportDashboardDto(
                            Workspace.CAMPAIGN, Collections.singletonList(library), false, false, true);
            JsonCustomReportDashboard jsonDashboard =
                    builderProvider
                            .get()
                            .build(dto.getFavoriteDashboardId(), dashboard, locale, dashboardScopeDto);
            dto.setDashboard(jsonDashboard);
        }
        return dto;
    }

    @PostMapping(value = "/{campaignId}/statistics")
    public StatisticsBundle getCampaignStatistics(
            @PathVariable long campaignId, @RequestBody StatisticsScopePatch patch) {
        return campaignLibraryNavigationService.gatherCampaignStatisticsBundle(
                campaignId, patch.lastExecutionScope);
    }

    public record StatisticsScopePatch(boolean lastExecutionScope) {}
}
