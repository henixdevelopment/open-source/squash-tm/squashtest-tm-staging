/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.testcase;

import com.google.common.collect.Lists;
import java.util.Collections;
import java.util.Locale;
import javax.inject.Named;
import javax.inject.Provider;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.Workspace;
import org.squashtest.tm.domain.customreport.CustomReportDashboard;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;
import org.squashtest.tm.service.display.testcase.TestCaseDisplayService;
import org.squashtest.tm.service.internal.display.dto.customreports.CustomReportDashboardDto;
import org.squashtest.tm.service.internal.display.dto.testcase.TestCaseLibraryDto;
import org.squashtest.tm.service.internal.dto.json.JsonCustomReportDashboard;
import org.squashtest.tm.service.testcase.TestCaseLibraryNavigationService;
import org.squashtest.tm.web.backend.model.builder.JsonCustomReportDashboardBuilder;

@RestController
@RequestMapping("/backend/test-case-library-view")
public class TestCaseLibraryViewController {

    private TestCaseDisplayService testCaseDisplayService;
    private final CustomReportLibraryNodeService customReportLibraryNodeService;

    @Named("customReport.dashboardBuilder")
    private final Provider<JsonCustomReportDashboardBuilder> builderProvider;

    private final TestCaseLibraryNavigationService testCaseLibraryNavigationService;

    public TestCaseLibraryViewController(
            TestCaseDisplayService testCaseDisplayService,
            CustomReportLibraryNodeService customReportLibraryNodeService,
            Provider<JsonCustomReportDashboardBuilder> builderProvider,
            TestCaseLibraryNavigationService testCaseLibraryNavigationService) {
        this.testCaseDisplayService = testCaseDisplayService;
        this.builderProvider = builderProvider;
        this.customReportLibraryNodeService = customReportLibraryNodeService;
        this.testCaseLibraryNavigationService = testCaseLibraryNavigationService;
    }

    @GetMapping(value = "/{testCaseLibraryId}")
    public TestCaseLibraryDto getTestCaseLibraryView(
            @PathVariable long testCaseLibraryId, Locale locale) {
        TestCaseLibraryDto dto = testCaseDisplayService.getTestCaseLibraryView(testCaseLibraryId);

        if (dto.isShouldShowFavoriteDashboard()) {
            if (dto.isCanShowFavoriteDashboard()) {
                EntityReference library =
                        new EntityReference(EntityType.TEST_CASE_LIBRARY, testCaseLibraryId);
                CustomReportDashboard dashboard =
                        customReportLibraryNodeService.findCustomReportDashboardById(
                                dto.getFavoriteDashboardId());
                CustomReportDashboardDto dashboardDto =
                        new CustomReportDashboardDto(Workspace.TEST_CASE, Collections.singletonList(library));
                JsonCustomReportDashboard jsonDashboard =
                        builderProvider
                                .get()
                                .build(dto.getFavoriteDashboardId(), dashboard, locale, dashboardDto);
                dto.setDashboard(jsonDashboard);
            }
        } else {
            dto.setStatistics(
                    testCaseLibraryNavigationService.getStatisticsForSelection(
                            Lists.newArrayList(testCaseLibraryId), Collections.emptyList()));
        }

        return dto;
    }
}
