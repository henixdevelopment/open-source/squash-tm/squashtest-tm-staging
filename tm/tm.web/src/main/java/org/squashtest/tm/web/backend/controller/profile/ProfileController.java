/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.profile;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.map.SingletonMap;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.display.profile.ProfileDisplayService;
import org.squashtest.tm.service.internal.display.dto.NewProfileDto;
import org.squashtest.tm.service.internal.display.dto.ProfileDto;
import org.squashtest.tm.service.internal.display.dto.ProfilePermissionsDto;
import org.squashtest.tm.service.profile.ProfileManagerService;
import org.squashtest.tm.web.backend.controller.form.model.ProfileFormModel;

@RestController
@RequestMapping("/backend/profiles")
public class ProfileController {

    private final ProfileDisplayService profileDisplayService;
    private final ProfileManagerService profileManagerService;

    public ProfileController(
            ProfileDisplayService profileDisplayService, ProfileManagerService profileManagerService) {
        this.profileDisplayService = profileDisplayService;
        this.profileManagerService = profileManagerService;
    }

    @GetMapping
    public SingletonMap<String, List<ProfileDto>> getAllProfiles() {
        List<ProfileDto> profiles = profileDisplayService.findAll();
        return new SingletonMap<>("profiles", profiles);
    }

    @PostMapping("/new")
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String, Object> createNewProfile(@RequestBody ProfileFormModel formModel) {
        NewProfileDto profileDto = formModel.getProfileDto();
        final long profileId = profileManagerService.persist(profileDto);
        return Collections.singletonMap("id", profileId);
    }

    @DeleteMapping("/{profileId}")
    public void deleteProfile(@PathVariable("profileId") long profileId) {
        profileManagerService.deleteProfile(profileId);
    }

    @GetMapping("/all-permissions")
    public List<ProfilePermissionsDto> getProfilesAndPermissions() {
        return profileDisplayService.getProfilesAndPermissions();
    }

    @PostMapping("/transfer-authorizations/{fromProfileId}/{toProfileId}")
    public void transferAuthorizations(
            @PathVariable("fromProfileId") long fromProfileId,
            @PathVariable("toProfileId") long toProfileId) {
        profileManagerService.transferAuthorizations(fromProfileId, toProfileId);
    }
}
