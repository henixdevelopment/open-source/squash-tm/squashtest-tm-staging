/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.sprintreqversion;

import java.util.List;
import java.util.Map;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.campaign.SprintReqVersionValidationStatus;
import org.squashtest.tm.service.campaign.SprintReqVersionModificationService;
import org.squashtest.tm.service.campaign.SprintReqVersionTestPlanManagerService;

@RestController
@RequestMapping("/backend/sprint-req-version")
public class SprintReqVersionModificationController {

    private final SprintReqVersionTestPlanManagerService sprintReqVersionTestPlanManagerService;
    private final SprintReqVersionModificationService sprintReqVersionModificationService;

    public SprintReqVersionModificationController(
            SprintReqVersionTestPlanManagerService sprintReqVersionTestPlanManagerService,
            SprintReqVersionModificationService sprintReqVersionModificationService) {
        this.sprintReqVersionTestPlanManagerService = sprintReqVersionTestPlanManagerService;
        this.sprintReqVersionModificationService = sprintReqVersionModificationService;
    }

    @PostMapping(value = "{sprintReqVersionId}/test-plan-items")
    public void addTestCasesToTestPlan(
            @PathVariable long sprintReqVersionId,
            @RequestBody AddTestCasesToTestPlanRequestBody requestBody) {
        if (requestBody.testCaseIds() != null) {
            sprintReqVersionTestPlanManagerService.addTestCaseLibraryNodesToTestPlan(
                    sprintReqVersionId, requestBody.testCaseIds());
        }

        if (requestBody.datasetIdsByTestCaseId() != null) {
            sprintReqVersionTestPlanManagerService.addTestCasesWithDatasetsToTestPlan(
                    sprintReqVersionId, requestBody.datasetIdsByTestCaseId());
        }
    }

    @PostMapping(value = "{sprintReqVersionId}/validation-status")
    public void changeValidationStatus(
            @PathVariable long sprintReqVersionId, @RequestBody String newValidationStatus) {
        sprintReqVersionModificationService.changeValidationStatus(
                sprintReqVersionId, SprintReqVersionValidationStatus.valueOf(newValidationStatus));
    }

    public record AddTestCasesToTestPlanRequestBody(
            List<Long> testCaseIds, Map<Long, List<Long>> datasetIdsByTestCaseId) {}
}
