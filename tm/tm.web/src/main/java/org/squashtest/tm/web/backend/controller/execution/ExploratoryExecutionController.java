/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.execution;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.security.UserContextHolder;
import org.squashtest.tm.service.execution.ExploratoryExecutionService;

@RestController()
@RequestMapping("/backend/exploratory-execution/{executionId}")
public class ExploratoryExecutionController {

    private final ExploratoryExecutionService exploratoryExecutionService;

    public ExploratoryExecutionController(ExploratoryExecutionService exploratoryExecutionService) {
        this.exploratoryExecutionService = exploratoryExecutionService;
    }

    @PostMapping(value = "run")
    public void runExploratoryExecution(@PathVariable("executionId") Long executionId) {
        exploratoryExecutionService.startOrResumeExploratoryExecution(
                executionId, UserContextHolder.getUsername());
    }

    @PostMapping(value = "pause")
    public void pauseExploratoryExecution(@PathVariable("executionId") Long executionId) {
        exploratoryExecutionService.pauseExploratoryExecution(
                executionId, UserContextHolder.getUsername());
    }

    @PostMapping(value = "stop")
    public void stopExploratoryExecution(@PathVariable("executionId") Long executionId) {
        exploratoryExecutionService.stopExploratoryExecution(
                executionId, UserContextHolder.getUsername());
    }

    @PostMapping(value = "assign-user-to-exploratory-execution")
    public void assignUser(
            @PathVariable("executionId") Long executionId,
            @RequestBody ExploratorySessionPatch exploratorySessionPatch) {
        exploratoryExecutionService.assignUser(executionId, exploratorySessionPatch.userId);
    }

    @PostMapping(value = "update-task-division")
    public void updateTaskDivision(
            @PathVariable("executionId") Long executionId,
            @RequestBody ExploratorySessionPatch exploratorySessionPatch) {
        exploratoryExecutionService.updateTaskDivision(
                executionId, exploratorySessionPatch.taskDivision);
    }

    @PostMapping(value = "update-review-status")
    public void updateReviewStatus(
            @PathVariable("executionId") Long executionId,
            @RequestBody ExploratorySessionPatch exploratorySessionPatch) {
        exploratoryExecutionService.updateReviewStatus(executionId, exploratorySessionPatch.reviewed);
    }

    @GetMapping(value = "is-execution-running")
    public boolean isExecutionRunning(@PathVariable("executionId") Long executionId) {
        return exploratoryExecutionService.isExecutionRunning(executionId);
    }

    public record ExploratorySessionPatch(Long userId, String taskDivision, boolean reviewed) {}
}
