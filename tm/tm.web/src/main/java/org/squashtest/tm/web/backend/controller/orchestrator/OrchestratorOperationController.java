/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.orchestrator;

import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.internal.dto.WorkflowDto;
import org.squashtest.tm.service.orchestrator.OrchestratorOperationService;
import org.squashtest.tm.service.orchestrator.model.OrchestratorConfVersions;
import org.squashtest.tm.service.orchestrator.model.OrchestratorResponse;

@RestController
@RequestMapping("/backend/orchestrator-operation/{testAutomationServerId}/")
public class OrchestratorOperationController {

    private final OrchestratorOperationService orchestratorOperationService;

    public OrchestratorOperationController(
            OrchestratorOperationService orchestratorOperationService) {
        this.orchestratorOperationService = orchestratorOperationService;
    }

    @GetMapping("/orchestrator-configuration")
    public OrchestratorResponse<OrchestratorConfVersions> getOrchestratorConfiguration(
            @PathVariable Long testAutomationServerId) {
        return orchestratorOperationService.getOrchestratorConfVersions(testAutomationServerId);
    }

    @GetMapping("/project/{projectId}/workflows")
    public OrchestratorResponse<List<WorkflowDto>> getProjectWorkflows(
            @PathVariable Long projectId, @PathVariable Long testAutomationServerId) {
        return orchestratorOperationService.getProjectWorkflows(testAutomationServerId, projectId);
    }

    @PostMapping("/project/{projectId}/kill-workflow")
    public OrchestratorResponse<Void> killWorkflow(
            @PathVariable Long projectId,
            @PathVariable Long testAutomationServerId,
            @RequestBody WorkflowPatch patch) {
        return orchestratorOperationService.killWorkflow(
                testAutomationServerId, projectId, patch.workflowId);
    }

    public record WorkflowPatch(String workflowId) {}
}
