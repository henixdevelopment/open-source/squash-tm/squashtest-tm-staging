/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.report;

import org.squashtest.tm.api.report.form.TreePicker;

public class TreePickerJson extends BasicInputJson {

    private String jsTreeNodeType;
    private boolean enableMultiSelection = true;

    TreePickerJson(TreePicker treePicker) {
        super(treePicker);
        this.jsTreeNodeType = treePicker.getJsTreeNodeType();
        if (treePicker.getNodeSelectionLimit() == 1) {
            this.enableMultiSelection = false;
        }
    }

    public String getJsTreeNodeType() {
        return jsTreeNodeType;
    }

    public boolean isEnableMultiSelection() {
        return enableMultiSelection;
    }

    public void setEnableMultiSelection(boolean enableMultiSelection) {
        this.enableMultiSelection = enableMultiSelection;
    }
}
