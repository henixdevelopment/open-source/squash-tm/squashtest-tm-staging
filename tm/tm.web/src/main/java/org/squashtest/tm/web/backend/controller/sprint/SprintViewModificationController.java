/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.sprint;

import java.util.Date;
import java.util.List;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.campaign.SprintStatus;
import org.squashtest.tm.service.campaign.CustomSprintModificationService;
import org.squashtest.tm.service.campaign.SprintManagerService;
import org.squashtest.tm.service.campaign.SprintModificationService;
import org.squashtest.tm.service.display.sprint.SprintDisplayService;
import org.squashtest.tm.service.internal.display.dto.sprint.SprintReqVersionDto;
import org.squashtest.tm.service.internal.dto.SprintReqVersionsBindingExceptionSummary;

@RestController
@RequestMapping("backend/sprint/{sprintId}")
public class SprintViewModificationController {

    private final CustomSprintModificationService customSprintModificationService;
    private final SprintModificationService sprintModificationService;
    private final SprintManagerService sprintManagerService;
    private final SprintDisplayService sprintDisplayService;

    public SprintViewModificationController(
            CustomSprintModificationService customSprintModificationService,
            SprintModificationService sprintModificationService,
            SprintManagerService sprintManagerService,
            SprintDisplayService sprintDisplayService) {
        this.customSprintModificationService = customSprintModificationService;
        this.sprintModificationService = sprintModificationService;
        this.sprintManagerService = sprintManagerService;
        this.sprintDisplayService = sprintDisplayService;
    }

    @PostMapping(value = "/name")
    public void rename(@PathVariable Long sprintId, @RequestBody SprintRecord record) {
        customSprintModificationService.rename(sprintId, record.name);
    }

    @PostMapping(value = "/reference")
    public void changeReference(@PathVariable Long sprintId, @RequestBody SprintRecord record) {
        sprintModificationService.changeReference(sprintId, record.reference);
    }

    @PostMapping(value = "/description")
    public void changeDescription(@PathVariable Long sprintId, @RequestBody SprintRecord record) {
        customSprintModificationService.updateDescription(sprintId, record.description);
    }

    @PostMapping(value = "/start-date")
    public void changeStartDate(@PathVariable Long sprintId, @RequestBody SprintRecord record) {
        customSprintModificationService.updateStartDate(sprintId, record.startDate);
    }

    @PostMapping(value = "/end-date")
    public void changeEndDate(@PathVariable Long sprintId, @RequestBody SprintRecord record) {
        customSprintModificationService.updateEndDate(sprintId, record.endDate);
    }

    @PostMapping(value = "/status")
    public void changeStatus(@PathVariable Long sprintId, @RequestBody String newStatus) {
        customSprintModificationService.updateStatusBySprintId(
                sprintId, SprintStatus.valueOf(newStatus));
    }

    @PostMapping(value = "/denormalize")
    public void denormalizeSprintReqVersions(@PathVariable Long sprintId) {
        customSprintModificationService.denormalizeSprintReqVersions(sprintId);
    }

    @PostMapping(value = "/requirements")
    public SprintReqVersionsBindingOperationReport bindRequirementsByRlnIds(
            @PathVariable Long sprintId, @RequestBody List<Long> rlnIds) {
        SprintReqVersionsBindingExceptionSummary summary =
                new SprintReqVersionsBindingExceptionSummary();
        List<SprintReqVersionDto> sprintReqVersions =
                sprintManagerService.bindRequirementsByRlnIds(sprintId, rlnIds, summary);
        return new SprintReqVersionsBindingOperationReport(sprintReqVersions, summary);
    }

    @PostMapping(value = "/reqversions")
    public SprintReqVersionsBindingOperationReport bindRequirementsByReqVersionIds(
            @PathVariable Long sprintId, @RequestBody List<Long> reqVersionIds) {
        SprintReqVersionsBindingExceptionSummary summary =
                new SprintReqVersionsBindingExceptionSummary();
        List<SprintReqVersionDto> sprintReqVersions =
                sprintManagerService.bindRequirementsByReqVersionIds(sprintId, reqVersionIds, summary);
        return new SprintReqVersionsBindingOperationReport(sprintReqVersions, summary);
    }

    @DeleteMapping("/requirements/{sprintReqVersionIds}")
    public SprintReqVersionsBindingOperationReport removeSprintReqVersions(
            @PathVariable long sprintId, @PathVariable List<Long> sprintReqVersionIds) {
        sprintManagerService.deleteSprintReqVersions(sprintId, sprintReqVersionIds);
        List<SprintReqVersionDto> sprintReqVersionDtos =
                sprintDisplayService.findSprintReqVersionDtosBySprintId(sprintId);
        return new SprintReqVersionsBindingOperationReport(sprintReqVersionDtos, null);
    }

    public record SprintRecord(
            String name, String reference, String description, Date startDate, Date endDate) {}

    public record SprintReqVersionsBindingOperationReport(
            List<SprintReqVersionDto> linkedReqVersions,
            SprintReqVersionsBindingExceptionSummary summary) {}
}
