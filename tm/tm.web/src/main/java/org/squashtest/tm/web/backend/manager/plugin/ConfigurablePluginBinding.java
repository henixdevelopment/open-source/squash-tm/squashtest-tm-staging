/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.manager.plugin;

import org.squashtest.tm.api.plugin.ConfigurablePlugin;

public class ConfigurablePluginBinding {
    private final ConfigurablePlugin plugin;
    private final long projectId;
    private final boolean isActive;
    private final boolean hasConfiguration;

    public ConfigurablePluginBinding(
            ConfigurablePlugin plugin, long projectId, boolean isActive, boolean hasConfiguration) {
        this.plugin = plugin;
        this.projectId = projectId;
        this.isActive = isActive;
        this.hasConfiguration = hasConfiguration;
    }

    public ConfigurablePlugin getPlugin() {
        return plugin;
    }

    public long getProjectId() {
        return projectId;
    }

    public boolean isActive() {
        return isActive;
    }

    public boolean isHasConfiguration() {
        return hasConfiguration;
    }
}
