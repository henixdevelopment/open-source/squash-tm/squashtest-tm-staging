/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.export.grid

import org.squashtest.tm.service.internal.display.grid.DataRow
import spock.lang.Specification

class CsvGridExporterTest extends Specification{

	def "Should export grid"() {
		given :
		def exportModel = buildBasicModel()

		def rowA = new DataRow()
		rowA.addData("name","TC1")
		rowA.addData("crit","Très Haute")

		def rowB = new DataRow()
		rowB.addData("name","TC2")
		rowB.addData("crit","Haute")
		exportModel.exportedRows = List.of(rowA, rowB)

		when:
		def file = new CsvGridExporter(exportModel).export()

		then:
		file.isFile()
		BufferedReader bf = new BufferedReader(new FileReader(file))
		def firstLine = bf.readLine()
		firstLine == '"Nom";"Criticité"'
		def secondLine = bf.readLine()
		secondLine == '"TC1";"Très Haute"'
		def thirdLine = bf.readLine()
		thirdLine == '"TC2";"Haute"'
		bf.close()
		noExceptionThrown()

	}

	def "Should not crash if missing date"() {
		given:
		def exportModel = buildBasicModel()
		def rowA = new DataRow()
		rowA.addData("crit",null)

		exportModel.exportedRows = List.of(rowA)

		when:
		def file = new CsvGridExporter(exportModel).export()

		then:
		file.isFile()
		BufferedReader bf = new BufferedReader(new FileReader(file))
		def firstLine = bf.readLine()
		firstLine == '"Nom";"Criticité"'
		def secondLine = bf.readLine()
		secondLine == '"";""'
		bf.close()
	}

	def buildBasicModel(){
		def exportModel = new GridExportModel()
		exportModel.exportedColumns =
			List.of(
				new GridExportModel.ExportColumnDefinition("name", "Nom"),
				new GridExportModel.ExportColumnDefinition("crit","Criticité")
			)
		exportModel
	}
}
