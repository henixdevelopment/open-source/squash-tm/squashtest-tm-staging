/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.testautomation

import org.squashtest.tm.service.display.test.automation.server.TestAutomationServerDisplayService
import org.squashtest.tm.service.internal.display.dto.automatedexecutionenvironments.ExecutionEnvironmentCountDto
import org.squashtest.tm.service.internal.display.dto.automatedexecutionenvironments.ExecutionEnvironmentCountProjectInfoDto
import org.squashtest.tm.service.project.GenericProjectFinder
import org.squashtest.tm.service.servers.StoredCredentialsManager
import org.squashtest.tm.service.testautomation.environment.AutomatedExecutionEnvironmentService
import org.squashtest.tm.web.backend.controller.test.automation.server.environments.unrestricted.UnrestrictedTAEnvironmentsController
import spock.lang.Specification


class UnrestrictedTAEnvironmentsControllerTest extends Specification {
    UnrestrictedTAEnvironmentsController controller

    AutomatedExecutionEnvironmentService automatedExecutionEnvironmentService = Mock()
    TestAutomationServerDisplayService testAutomationServerDisplayService = Mock()
    GenericProjectFinder genericProjectFinder = Mock()
    StoredCredentialsManager storedCredentialsManager = Mock()

    def setup() {
       controller  = new UnrestrictedTAEnvironmentsController(
           automatedExecutionEnvironmentService,
           testAutomationServerDisplayService,
           genericProjectFinder,
           storedCredentialsManager)
    }

    def "should return execution environment count for iteration"() {
        given:

            def projectsListIteration = [
                new ExecutionEnvironmentCountProjectInfoDto((-34L), "project1", (-5L), "orchestrator1"),
                new ExecutionEnvironmentCountProjectInfoDto((-86L), "project2", (-7L), "orchestrator2")
            ]

            def projectsListTestSuite = [
                new ExecutionEnvironmentCountProjectInfoDto((-12L), "project3", (-3L), "orchestrator3"),
                new ExecutionEnvironmentCountProjectInfoDto((-78L), "project4", (-17L), "orchestrator4")
            ]

            def iterationMap = [(-34L): (-5L), (-86L): (-7L)]
            testAutomationServerDisplayService.findProjectAndServerIdsByIterationId(_) >>  iterationMap

            def testSuiteMap = [(-12L): (-3L), (-78L): (-17L)]
            testAutomationServerDisplayService.findProjectAndServerIdsByTestSuiteId(_) >> testSuiteMap
        and:
            def iterationResultMap = ["BUSY": 1L, "IDLE": 2L, "PENDING" : 0L, "UNREACHABLE" : 4L]
            automatedExecutionEnvironmentService.getExecutionEnvironmentCountDto(iterationMap) >> new ExecutionEnvironmentCountDto(
                iterationResultMap,
                projectsListIteration,
                2,
                0,
                0)

            def testSuiteResultMap = ["BUSY": 0L, "IDLE": 8L, "PENDING" : 1L, "UNREACHABLE" : 2L]
            automatedExecutionEnvironmentService.getExecutionEnvironmentCountDto(testSuiteMap) >> new ExecutionEnvironmentCountDto(
                testSuiteResultMap,
                projectsListTestSuite,
                2,
                1,
                1)

        when:
            def iterationExecEnvCountResult = controller.getAutomatedExecutionEnvironmentsStatusesCountForEntity("iteration", -154L)

        then:
            iterationExecEnvCountResult.statuses() == ["BUSY": 1L, "IDLE": 2L, "PENDING": 0L, "UNREACHABLE": 4L]
            iterationExecEnvCountResult.projects() == [
                new ExecutionEnvironmentCountProjectInfoDto((-34L), "project1", (-5L), "orchestrator1"),
                new ExecutionEnvironmentCountProjectInfoDto((-86L), "project2", (-7L), "orchestrator2")
            ]
            iterationExecEnvCountResult.nbOfSquashOrchestratorServers() == 2
            iterationExecEnvCountResult.nbOfUnreachableSquashOrchestrators() == 0
            iterationExecEnvCountResult.nbOfServersWithMissingToken() == 0
    }

    def "should return execution environment count for test suite"() {
        given:

            def projectsListIteration = [
                new ExecutionEnvironmentCountProjectInfoDto((-34L), "project1", (-5L), "orchestrator1"),
                new ExecutionEnvironmentCountProjectInfoDto((-86L), "project2", (-7L), "orchestrator2")
            ]

            def projectsListTestSuite = [
                new ExecutionEnvironmentCountProjectInfoDto((-12L), "project3", (-3L), "orchestrator3"),
                new ExecutionEnvironmentCountProjectInfoDto((-78L), "project4", (-17L), "orchestrator4")
            ]

            def iterationMap = [(-34L): (-5L), (-86L): (-7L)]
            testAutomationServerDisplayService.findProjectAndServerIdsByIterationId(_) >>  iterationMap

            def testSuiteMap = [(-12L): (-3L), (-78L): (-17L)]
            testAutomationServerDisplayService.findProjectAndServerIdsByTestSuiteId(_) >> testSuiteMap
        and:
            def iterationResultMap = ["BUSY": 1L, "IDLE": 2L, "PENDING" : 0L, "UNREACHABLE" : 4L]
            automatedExecutionEnvironmentService.getExecutionEnvironmentCountDto(iterationMap) >> new ExecutionEnvironmentCountDto(
                iterationResultMap,
                projectsListIteration,
                2,
                0,
                0)

            def testSuiteResultMap = ["BUSY": 0L, "IDLE": 8L, "PENDING" : 1L, "UNREACHABLE" : 2L]
            automatedExecutionEnvironmentService.getExecutionEnvironmentCountDto(testSuiteMap) >> new ExecutionEnvironmentCountDto(
                testSuiteResultMap,
                projectsListTestSuite,
                2,
                1,
                1)

        when:
            def testSuiteExecEnvCountResult = controller.getAutomatedExecutionEnvironmentsStatusesCountForEntity("test-suite", -879L)

        then:
            testSuiteExecEnvCountResult.statuses() == ["BUSY": 0L, "IDLE": 8L, "PENDING": 1L, "UNREACHABLE": 2L]
            testSuiteExecEnvCountResult.projects() == [
                new ExecutionEnvironmentCountProjectInfoDto((-12L), "project3", (-3L), "orchestrator3"),
                new ExecutionEnvironmentCountProjectInfoDto((-78L), "project4", (-17L), "orchestrator4")
            ]
            testSuiteExecEnvCountResult.nbOfSquashOrchestratorServers() == 2
            testSuiteExecEnvCountResult.nbOfUnreachableSquashOrchestrators() == 1
            testSuiteExecEnvCountResult.nbOfServersWithMissingToken() == 1
    }

    def "should throw error if execution environment count is called for neither iteration nor test suite"() {
        when:
            controller.getAutomatedExecutionEnvironmentsStatusesCountForEntity("not-existing", -879L)
        then:
            thrown(IllegalArgumentException)
    }
}

