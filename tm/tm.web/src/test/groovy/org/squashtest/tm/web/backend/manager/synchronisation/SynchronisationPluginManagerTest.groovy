/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.manager.synchronisation

import org.springframework.core.env.Environment
import org.springframework.scheduling.TaskScheduler
import org.squashtest.tm.api.wizard.SynchronisationPlugin
import spock.lang.Specification
import spock.lang.Unroll

class SynchronisationPluginManagerTest extends Specification {

    SynchronisationPluginManager manager = new SynchronisationPluginManagerImpl()

    Environment environment = Mock(Environment)
    TaskScheduler taskScheduler = Mock(TaskScheduler)

    def "setup"() {
        manager.environment = environment
        manager.taskScheduler = taskScheduler
        manager.plugins = [Mock(SynchronisationPlugin), Mock(SynchronisationPlugin)]
    }

    @Unroll
    def "should schedule synchronisations if property value is anything other than false [#value]"() {
        given:
        environment.getProperty("squash.external.synchronisation.enabled") >> value
        environment.getProperty("squash.external.synchronisation.delay") >> 300

        when:
        manager.scheduleSynchronisations()

        then:
        2 * taskScheduler.scheduleWithFixedDelay(_,_)

        where:
        value << [null, "", "null", "0", "true", "TRUE", "tRuE", "fals", "any"]
    }

    @Unroll
    def "should NOT schedule synchronisations if property value is false [#value]"() {
        given:
        environment.getProperty("squash.external.synchronisation.enabled") >> value
        environment.getProperty("squash.external.synchronisation.delay") >> 300

        when:
        manager.scheduleSynchronisations()

        then:
        0 * taskScheduler.scheduleWithFixedDelay(_,_)

        where:
        value << ["false", "FALSE", "fAlSe"]
    }
}
