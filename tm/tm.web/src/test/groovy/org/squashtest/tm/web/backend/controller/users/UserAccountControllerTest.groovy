/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.users

import org.squashtest.tm.domain.bugtracker.BugTracker
import org.squashtest.tm.domain.servers.AuthenticationPolicy
import org.squashtest.tm.domain.servers.AuthenticationProtocol
import org.squashtest.tm.service.internal.display.dto.BugTrackerCredentialsDto
import org.squashtest.tm.service.internal.security.AuthenticationProviderContext
import org.squashtest.tm.service.internal.servers.ManageableBasicAuthCredentials
import org.squashtest.tm.service.internal.servers.OAuth2ConsumerService
import org.squashtest.tm.service.project.ProjectsPermissionFinder
import org.squashtest.tm.service.servers.ManageableCredentials
import org.squashtest.tm.service.servers.Oauth2Tokens
import org.squashtest.tm.service.servers.StoredCredentialsManager
import org.squashtest.tm.service.user.PartyPreferenceService
import org.squashtest.tm.service.user.UserAccountService
import org.squashtest.tm.web.backend.controller.user.UserAccountController
import spock.lang.Specification

class UserAccountControllerTest extends Specification{

    UserAccountService userService = Mock()
    StoredCredentialsManager credManager = Mock()
    ProjectsPermissionFinder permissionFinder = Mock()
    PartyPreferenceService partyPreferenceService = Mock()
    AuthenticationProviderContext authenticationProviderContext = Mock()
    OAuth2ConsumerService oAuth2ConsumerService = Mock()
    UserAccountController controller


    def setup(){
        controller = new UserAccountController(
            userService,
            permissionFinder,
            partyPreferenceService,
            credManager,
            authenticationProviderContext,
            oAuth2ConsumerService
        )
    }


    def "should create a default, empty instance of manageable basic auth credentials if the bugtracker is configured to authenticate with basic auth"(){

        given:
        BugTracker bt = Mock()
        bt.getAuthenticationProtocol() >> AuthenticationProtocol.BASIC_AUTH
        bt.getId() >> 1L

        and:
        credManager.findCurrentUserCredentials(1L) >> null

        when:
        ManageableCredentials result = controller.getOrCreateCredentials(bt)

        then:
        result instanceof ManageableBasicAuthCredentials
        result.username == ""
        result.password == "" as char[]

    }


    def "should create a default, empty instance of manageable OAuth2 tokens if the bugtracker is configured to authenticate with OAuth2"(){

        given:
        BugTracker bt = Mock()
        bt.getAuthenticationProtocol()>> AuthenticationProtocol.OAUTH_2
        bt.getId() >> 1L

        and:
        credManager.findCurrentUserCredentials(1L) >> null

        when:
        ManageableCredentials result = controller.getOrCreateCredentials(bt)

        then:
        result instanceof  Oauth2Tokens
        result.accessToken == ""
        result.refreshToken == ""
        result.tokenType == ""
        result.expiresIn == null


    }



    def "for all bugtrackers accessible to the current user, map them to the appropriate credentials"(){

        given: "the bugtrackers"

        def bt1 = Mock(BugTracker){
            getId() >> 1L
            getAuthenticationProtocol() >> AuthenticationProtocol.BASIC_AUTH
            getAuthenticationPolicy() >> AuthenticationPolicy.USER
        }

        def bt2 = Mock(BugTracker){
            getId() >> 2L
            getAuthenticationProtocol() >> AuthenticationProtocol.OAUTH_2
            getAuthenticationPolicy() >> AuthenticationPolicy.USER
        }

        def bt3 = Mock(BugTracker){
            getId()>> 3L
            getAuthenticationProtocol()>> AuthenticationProtocol.OAUTH_2
            getAuthenticationPolicy() >> AuthenticationPolicy.USER
        }
        userService.findAllUserBugTracker() >> [ bt1, bt2 ]


        and: "the credentials"

        def cred1 = new ManageableBasicAuthCredentials("Bob", "bob")
        def cred3 = new Oauth2Tokens("accessToken", "refreshToken","bearer",711515L)

        when:
        List<BugTrackerCredentialsDto> result = controller.getBugtrackerCredentials()

        then:

        // interactions & behavior
        1 * userService.findAllUserBugTracker() >> [ bt1, bt2, bt3 ]

        1 * credManager.findCurrentUserCredentials(1L) >> cred1
        1 * credManager.findCurrentUserCredentials(3L) >> cred3


        // result
        result[0].bugTracker.id == 1L
        result[0].credentials.username == "Bob"
        result[0].credentials.token == null

        result[1].bugTracker.id == 2L

        result[2].bugTracker.id == 3L
        result[2].credentials.token == "accessToken"
        result[2].credentials.tokenType == "bearer"
    }


    def "for all bugtrackers accessible to the current user, map default credentials when none are defined for those bugtrackers"(){

        given: "the bugtrackers"

        def bt1 = Mock(BugTracker){
            getId()>> 1L
            getAuthenticationProtocol()>> AuthenticationProtocol.BASIC_AUTH
            getAuthenticationPolicy() >> AuthenticationPolicy.USER
        }

        def bt2 = Mock(BugTracker){
            getId()>> 2L
            getAuthenticationProtocol()>> AuthenticationProtocol.OAUTH_2
            getAuthenticationPolicy() >> AuthenticationPolicy.USER
        }

        def bt3 = Mock(BugTracker){
            getId()>> 3L
            getAuthenticationProtocol()>> AuthenticationProtocol.OAUTH_2
            getAuthenticationPolicy() >> AuthenticationPolicy.USER
        }
        userService.findAllUserBugTracker() >> [ bt1, bt2, bt3 ]

        when:
        List<BugTrackerCredentialsDto> result = controller.getBugtrackerCredentials()

        then:
        1 * userService.findAllUserBugTracker() >> [ bt1, bt2, bt3 ]

        // result
        result[0].bugTracker.id == 1L
        result[0].credentials.username == ""
        result[0].credentials.token == null

        result[1].bugTracker.id == 2L
        result[1].credentials.token == ""

        result[2].bugTracker.id == 3L
        result[2].credentials.token == ""




    }

}
