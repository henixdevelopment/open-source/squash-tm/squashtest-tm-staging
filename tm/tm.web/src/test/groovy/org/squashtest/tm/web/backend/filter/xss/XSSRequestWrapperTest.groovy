/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.filter.xss

import org.owasp.esapi.errors.IntrusionException
import spock.lang.Ignore
import spock.lang.Specification
import spock.lang.Unroll

import javax.servlet.http.HttpServletRequest

class XSSRequestWrapperTest extends Specification {

	HttpServletRequest request
	XSSRequestWrapper wrapper

	def setup() {
		request = Mock(HttpServletRequest)
		wrapper = new XSSRequestWrapper(request)
	}

	def "should let null filter param map pass unmodified"() {
		when:
		def map = wrapper.getParameterMap()

		then:
		1 * request.getParameterMap()
		map == null
	}

	def "should let empty filter param map pass unmodified"() {
		given:
		request.getParameterMap() >> [:]

		when:
		def map = wrapper.getParameterMap()

		then:
		map == [:]
	}

	@Unroll
	def "should let valid filter param map pass unmodified"() {
		given:
		def strArray = new String[2]
		strArray[0] = strA
		strArray[1] = strB
		request.getParameterMap() >> ["param1": strArray]

		when:
		def map = wrapper.getParameterMap()

		then:
		map.size() == 1
		map.get("param1").toList() == [strA, strB]

		where:
		strA    | strB
		"hello" | "world"
		"true"  | "false"
		""      | ""
		""      | null
	}

	@Unroll
	def "should strip script from filter param map"() {
		given:
		def strArray = new String[2]
		strArray[0] = strA
		strArray[1] = strB
		request.getParameterMap() >> ["param1": strArray]

		when:
		def map = wrapper.getParameterMap()

		then:
		thrown(IntrusionException.class)

		where:
		strA                     | strB
		"<p>hello</p>"           | "world</div><script>alert('pwned !!!')</script>"
		"%3Cp%3Ehello%3C%2Fp%3E" | "world%3C%2Fdiv%3E%3Cscript%3Ealert%28%27pwned%20%21%21%21%27%29%3C%2Fscript%3E"
	}

	def "should throw exception if multiple encoding is used"() {
		given:
		def strArray = new String[2]
		strArray[0] = "%22%253Cp%253Ehello%253C%252Fp%253E%22"
		strArray[1] = "world"
		request.getParameterMap() >> ["param1": strArray]

		when:
		wrapper.getParameterMap()

		then:
		thrown(IntrusionException.class)
	}

	def "should clean named param but preserve encoding if valid"() {
		given:
		request.getParameter("param1") >> "hello%20world"

		when:
		def value = wrapper.getParameter("param1")

		then:
		value == "hello%20world"
	}
}
