/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.supervision

import org.squashtest.tm.domain.campaign.IterationTestPlanItem
import org.squashtest.tm.domain.execution.Execution
import org.squashtest.tm.domain.execution.ExecutionStatus
import org.squashtest.tm.domain.project.Project
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender
import org.squashtest.tm.domain.testautomation.AutomatedSuite
import org.squashtest.tm.domain.testautomation.AutomatedTest
import org.squashtest.tm.domain.testautomation.TestAutomationProject
import org.squashtest.tm.domain.testautomation.TestAutomationServer
import org.squashtest.tm.domain.testcase.Dataset
import org.squashtest.tm.domain.testcase.TestCase
import org.squashtest.tm.service.testautomation.model.AutomatedSuiteWithSquashAutomAutomatedITPIs
import org.squashtest.tm.service.testautomation.supervision.model.AutomatedSuiteOverview
import org.squashtest.tm.service.testautomation.supervision.model.SquashAutomExecutionView
import org.squashtest.tm.service.testautomation.supervision.model.TfExecutionView
import org.squashtest.tm.web.i18n.InternationalizationHelper
import spock.lang.Specification
import spock.lang.Unroll

/**
 * @author Gregory Fouquet
 *
 */
class AutomatedExecutionViewUtilsTest extends Specification {
    public static final String AUTOMATED_SUITE_ID = "bacba2b0-14b2-4838-b8b3-371811a0302b"
    InternationalizationHelper i18n = Mock()

    @Unroll
    def "percent progression should be #prog"() {
        expect: AutomatedExecutionViewUtils.percentProgression(part, total) == prog
        where:
        part	| total	| prog
        0		| 0		| 100
        1		| 4		| 25
        2		| 4		| 50
        3		| 4		| 75
        4		| 4		| 100
    }

    def "should create populated TfExecutionView"() {
        given:
        AutomatedExecutionExtender autoExec = Mock()
        autoExec.nodeName >> "verser"

        and:
        Execution exec = Mock()
        exec.id >> 50
        exec.name >> "ordeal"
        exec.executionStatus >> ExecutionStatus.SUCCESS
        autoExec.execution >> exec

        and:
        TestAutomationProject tap = Mock()
        tap.label >> "drips drips drips drips"
        autoExec.automatedProject >> tap

        and :
        i18n.internationalize(spock.lang.Specification._, spock.lang.Specification._) >>	"成功"

        when:
        TfExecutionView res = AutomatedExecutionViewUtils.convertExecExtenderToTfExecutionView(autoExec);

        then:
        res.automatedServerName == "drips drips drips drips"
        res.nodeName == "verser"
        res.id == 50
        res.name == "ordeal"
        res.status == ExecutionStatus.SUCCESS

    }

    def "should create populated SquashAutomExecutionView"() {
        given:
        def item = provideItpi(42, "ordeal", datasetInput, "autom server name")

        when:
        SquashAutomExecutionView res = AutomatedExecutionViewUtils.convertItpiToSquashAutomExecutionView(item);

        then:
        res.id == 42
        res.name == "ordeal"
        res.datasetLabel == datasetInput
        res.status == ExecutionStatus.READY
        res.automatedServerName == "autom server name"

        where: datasetInput << [null, "dataset_1"]
    }

    private IterationTestPlanItem provideItpi(long providedId, String providedTcName, String providedDatasetName, String providedServerName) {
        IterationTestPlanItem item = Mock()
        item.id >> providedId

        TestCase tc = Mock()
        tc.name >> providedTcName

        Dataset dataset = Mock()
        dataset.getName() >> providedDatasetName

        TestAutomationServer automServer = Mock()
        automServer.name >> providedServerName

        Project project = Mock()

        project.testAutomationServer >> automServer
        tc.project >> project
        item.referencedTestCase >> tc
        item.referencedDataset >> dataset

        item
    }

    def "should build first automated suite overview with TfExecution"() {
        given:
        AutomatedSuite suite = provideAutomatedSuiteWithTfExecution()

        AutomatedSuiteWithSquashAutomAutomatedITPIs decoratedSuite = Mock(AutomatedSuiteWithSquashAutomAutomatedITPIs)
        decoratedSuite.getSuite() >> suite
        decoratedSuite.getSquashAutomAutomatedItems() >> []

        when:
        AutomatedSuiteOverview overview = AutomatedExecutionViewUtils.buildFirstAutomatedSuiteOverview(decoratedSuite)

        then:
        overview.suiteId == AUTOMATED_SUITE_ID
        overview.tfPercentage == 100
        overview.tfExecutions.size() == 1
        TfExecutionView tfExecutionView = overview.tfExecutions.get(0)
        tfExecutionView.id == 7L
        tfExecutionView.name == "Login page"
        tfExecutionView.status == ExecutionStatus.SUCCESS
        tfExecutionView.nodeName == "node_1"
        tfExecutionView.automatedServerName == "Jenkins server"
    }

    def "should build automated suite overview with TfExecution"() {
        given:
        AutomatedSuite suite = provideAutomatedSuiteWithTfExecution()

        when:
        AutomatedSuiteOverview overview =
            AutomatedExecutionViewUtils.buildAutomatedSuiteOverview(suite)

        then:
        overview.suiteId == AUTOMATED_SUITE_ID
        overview.tfPercentage == 100
        overview.tfExecutions.size() == 1
        TfExecutionView tfExecutionView = overview.tfExecutions.get(0)
        tfExecutionView.id == 7L
        tfExecutionView.name == "Login page"
        tfExecutionView.status == ExecutionStatus.SUCCESS
        tfExecutionView.nodeName == "node_1"
        tfExecutionView.automatedServerName == "Jenkins server"
    }

    private AutomatedSuite provideAutomatedSuiteWithTfExecution() {
        Execution execution = Mock(Execution)
        execution.getId() >> 7L
        execution.getName() >> "Login page"
        execution.getExecutionStatus() >> ExecutionStatus.SUCCESS

        TestAutomationProject automationProject = Mock(TestAutomationProject)
        automationProject.getLabel() >> "Jenkins server"

        AutomatedExecutionExtender tfExtender = Mock()
        tfExtender.getExecution() >> execution
        tfExtender.getAutomatedTest() >> Mock(AutomatedTest)
        tfExtender.getAutomatedProject() >> automationProject
        tfExtender.getNodeName() >> "node_1"

        AutomatedSuite suite = Mock()
        suite.getId() >> AUTOMATED_SUITE_ID
        suite.getExecutionExtenders() >> [tfExtender]

        return suite
    }

    def "should build first automated suite overview with SquashAutomExecution"() {
        given:
        TestAutomationServer server = Mock(TestAutomationServer)
        server.getName() >> "Orchestrator"

        Project project = Mock(Project)
        project.getTestAutomationServer() >> server

        TestCase testCase = Mock(TestCase)
        testCase.getName() >> "Login page"
        testCase.getProject() >> project

        Dataset dataset = Mock(Dataset)
        dataset.getName() >> "dataset_1"

        IterationTestPlanItem itpi = Mock(IterationTestPlanItem)
        itpi.getId() >> 9L
        itpi.getReferencedTestCase() >> testCase
        itpi.getReferencedDataset() >> dataset
        itpi.getExecutionStatus() >> ExecutionStatus.READY

        AutomatedSuite suite = Mock(AutomatedSuite)
        suite.getId() >> AUTOMATED_SUITE_ID
        suite.getExecutionExtenders() >> []

        AutomatedSuiteWithSquashAutomAutomatedITPIs decoratedSuite = Mock(AutomatedSuiteWithSquashAutomAutomatedITPIs)
        decoratedSuite.getSuite() >> suite
        decoratedSuite.getSquashAutomAutomatedItems() >> [itpi]

        when:
        AutomatedSuiteOverview overview = AutomatedExecutionViewUtils.buildFirstAutomatedSuiteOverview(decoratedSuite)

        then:
        overview.suiteId == AUTOMATED_SUITE_ID
        overview.automTerminatedCount == 0
        overview.automExecutions.size() == 1
        SquashAutomExecutionView squashAutomExecutionView = overview.automExecutions.get(0)
        squashAutomExecutionView.id == 9L
        squashAutomExecutionView.name == "Login page"
        squashAutomExecutionView.datasetLabel == "dataset_1"
        squashAutomExecutionView.status == ExecutionStatus.READY
        squashAutomExecutionView.automatedServerName == "Orchestrator"
    }

    def "should build automated suite overview with SquashAutomExecution"() {
        given:
        TestAutomationServer testAutomationServer = Mock(TestAutomationServer)
        testAutomationServer.getName() >> "Orchestrator"

        and:
        Project project = Mock(Project)
        project.getTestAutomationServer() >> testAutomationServer

        and:
        TestCase testCase = Mock(TestCase)
        testCase.getProject() >> project

        and:
        IterationTestPlanItem itpi = Mock(IterationTestPlanItem)
        itpi.getId() >> 9L

        and:
        Execution execution = Mock(Execution)
        execution.getId() >> 7L
        execution.getName() >> "Login page"
        execution.getExecutionStatus() >> ExecutionStatus.RUNNING
        execution.getReferencedTestCase() >> testCase
        execution.getTestPlan() >> itpi
        execution.getDatasetLabel() >> "dataset_1"

        and:
        AutomatedExecutionExtender squashAutomExtender = Mock()
        squashAutomExtender.getExecution() >> execution
        squashAutomExtender.getAutomatedTest() >> null

        and:
        AutomatedSuite suite = Mock()
        suite.getId() >> AUTOMATED_SUITE_ID
        suite.getExecutionExtenders() >> [squashAutomExtender]

        when:
        AutomatedSuiteOverview overview =
            AutomatedExecutionViewUtils.buildAutomatedSuiteOverview(suite)

        then:
        overview.suiteId == AUTOMATED_SUITE_ID
        overview.automTerminatedCount == 0
        overview.automExecutions.size() == 1
        SquashAutomExecutionView squashAutomExecutionView = overview.automExecutions.get(0)
        squashAutomExecutionView.id == 9L
        squashAutomExecutionView.name == "Login page"
        squashAutomExecutionView.datasetLabel == "dataset_1"
        squashAutomExecutionView.status == ExecutionStatus.RUNNING
        squashAutomExecutionView.automatedServerName == "Orchestrator"
    }

}
