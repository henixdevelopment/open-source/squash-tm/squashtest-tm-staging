/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.testcase.parameters

import org.springframework.http.ResponseEntity
import org.springframework.validation.BindException
import org.squashtest.tm.domain.bdd.ActionWordParameter
import org.squashtest.tm.domain.testcase.KeywordTestStep
import org.squashtest.tm.service.display.testcase.TestCaseDisplayService
import org.squashtest.tm.service.internal.display.dto.testcase.AddTestStepOperationReport
import org.squashtest.tm.service.internal.display.dto.testcase.AbstractTestStepDto
import org.squashtest.tm.service.internal.display.testcase.parameter.TestCaseParameterOperationReport
import org.squashtest.tm.service.testcase.CallStepManagerService
import org.squashtest.tm.service.testcase.TestCaseModificationService
import org.squashtest.tm.web.backend.controller.testcase.steps.KeywordTestStepModel
import org.squashtest.tm.web.backend.controller.testcase.steps.TestCaseTestStepsController
import org.squashtest.tm.web.i18n.InternationalizationHelper
import spock.lang.Specification

import javax.servlet.http.HttpServletRequest

class TestCaseTestStepsControllerTest extends Specification {

	CallStepManagerService callStepManager = Mock()
	TestCaseModificationService testCaseModificationService = Mock()
	TestCaseDisplayService testCaseDisplayService = Mock()
	HttpServletRequest request = Mock()
	InternationalizationHelper messageSource = Mock()

	TestCaseTestStepsController controller = new TestCaseTestStepsController(callStepManager, testCaseModificationService, testCaseDisplayService, messageSource)

	def setup() {
		request.getCharacterEncoding() >> "ISO-8859-1"
	}


	def "should add a keyword test step with given keyword and actionWord"() {
		given:
		KeywordTestStepModel testStepModel = new KeywordTestStepModel()
		testStepModel.setKeyword("BUT")
		testStepModel.setActionWord("add a BDD test step")

		and:
		def testStep = Mock(KeywordTestStep)
		testStep.getId() >> 2020
		def testStepDto = Mock(AbstractTestStepDto)
		testStepDto.getId() >> 2020

		and:
		testCaseModificationService.addKeywordTestStep(1L, "BUT", "add a BDD test step") >> testStep
		testCaseModificationService.findTestStep(2020L) >> testStepDto

		when:
		AddTestStepOperationReport operationReport = controller.addKeywordTestStep(testStepModel, 1L)

		then:
		operationReport.testStep.id == 2020
	}

	def "should add a keyword test step with given keyword and actionWord at sepcific index"() {
		given:
		KeywordTestStepModel testStepModel = new KeywordTestStepModel()
		testStepModel.setKeyword("BUT")
		testStepModel.setActionWord("add a BDD test step")
		testStepModel.setIndex(2)

		and:
		def testStep = Mock(KeywordTestStep)
		testStep.getId() >> 2020
		def testStepDto = Mock(AbstractTestStepDto)
		testStepDto.getId() >> 2020

		and:
		testCaseModificationService.addKeywordTestStep(1L, "BUT", "add a BDD test step", 2) >> testStep
		testCaseModificationService.findTestStep(2020L) >> testStepDto
		testCaseDisplayService.findParametersDataByTestStepId(2020L) >> Mock(TestCaseParameterOperationReport)

		when:
		AddTestStepOperationReport operationReport = controller.addKeywordTestStep(testStepModel, 1L)

		then:
		operationReport.testStep.id == 2020
	}

	def "should add a keyword test step with given keyword and parameterized actionWord"() {
		given:
		KeywordTestStepModel testStepModel = new KeywordTestStepModel()
		testStepModel.setKeyword("BUT")
		testStepModel.setActionWord("add a \"BDD\" test \"step\"")

		and:
		def testStep = Mock(KeywordTestStep)
		testStep.getId() >> 2020
		def testStepDto = Mock(AbstractTestStepDto)
		testStepDto.getId() >> 2020

		and:
		testCaseModificationService.addKeywordTestStep(1L, "BUT", "add a \"BDD\" test \"step\"") >> testStep
		testCaseModificationService.findTestStep(2020L) >> testStepDto
		testCaseDisplayService.findParametersDataByTestStepId(2020L) >> Mock(TestCaseParameterOperationReport)

		when:
		AddTestStepOperationReport operationReport = controller.addKeywordTestStep(testStepModel, 1L)

		then:
		operationReport.testStep.id == 2020
	}

	def "should add a keyword test step with given keyword and parameterized actionWord in which value is between <>"() {
		given:
		KeywordTestStepModel testStepModel = new KeywordTestStepModel()
		testStepModel.setKeyword("BUT")
		testStepModel.setActionWord("add a \"BDD\" test <tcParam>")

		and:
		def testStep = Mock(KeywordTestStep)
		testStep.getId() >> 2020
		def testStepDto = Mock(AbstractTestStepDto)
		testStepDto.getId() >> 2020

		and:
		testCaseModificationService.addKeywordTestStep(1L, "BUT", "add a \"BDD\" test <tcParam>") >> testStep
		testCaseModificationService.findTestStep(2020L) >> testStepDto
		testCaseDisplayService.findParametersDataByTestStepId(2020L) >> Mock(TestCaseParameterOperationReport)

		when:
		AddTestStepOperationReport operationReport = controller.addKeywordTestStep(testStepModel, 1L)

		then:
		operationReport.testStep.id == 2020
	}

	def "should add a keyword test step with given keyword and parameterized actionWord in which values are numbers"() {
		given:
		KeywordTestStepModel testStepModel = new KeywordTestStepModel()
		testStepModel.setKeyword("BUT")
		testStepModel.setActionWord("add \"1\" and 2")

		and:
		def testStep = Mock(KeywordTestStep)
		testStep.getId() >> 2020
		def testStepDto = Mock(AbstractTestStepDto)
		testStepDto.getId() >> 2020

		and:
		testCaseModificationService.addKeywordTestStep(1L, "BUT", "add \"1\" and 2") >> testStep
		testCaseModificationService.findTestStep(2020L) >> testStepDto
		testCaseDisplayService.findParametersDataByTestStepId(2020L) >> Mock(TestCaseParameterOperationReport)

		when:
		AddTestStepOperationReport operationReport = controller.addKeywordTestStep(testStepModel, 1L)

		then:
		operationReport.testStep.id == 2020
	}

	def "should throw exception when adding a keyword test step with empty keyword"() {
		given:
		KeywordTestStepModel testStepModel = new KeywordTestStepModel()
		testStepModel.setKeyword("")
		testStepModel.setActionWord("add a BDD test step")

		and:
		def testStep = Mock(KeywordTestStep)
		testStep.getId() >> 2020
		testCaseModificationService.addKeywordTestStep(1L, "", "add a BDD test step") >> testStep

		when:
		controller.addKeywordTestStep(testStepModel, 1L)

		then:
		BindException ex = thrown()
		ex.message == "org.springframework.validation.BeanPropertyBindingResult: 1 errors\n" +
			"Field error in object 'add-keyword-test-step' on field 'keyword': rejected value []; codes [message.notBlank.add-keyword-test-step.keyword,message.notBlank.keyword,message.notBlank.java.lang.String,message.notBlank]; arguments []; default message [null]"
	}

	def "should throw exception when adding a keyword test step with empty Action word"() {
		given:
		KeywordTestStepModel testStepModel = new KeywordTestStepModel()
		testStepModel.setKeyword("AND")
		testStepModel.setActionWord("")

		and:
		def testStep = Mock(KeywordTestStep)
		testStep.getId() >> 2020
		testCaseModificationService.addKeywordTestStep(1L, "AND", "") >> testStep

		when:
		controller.addKeywordTestStep(testStepModel, 1L)

		then:
		BindException ex = thrown()
		ex.message == "org.springframework.validation.BeanPropertyBindingResult: 1 errors\n" +
			"Field error in object 'add-keyword-test-step' on field 'actionWord': rejected value []; codes [message.notBlank.add-keyword-test-step.actionWord,message.notBlank.actionWord,message.notBlank.java.lang.String,message.notBlank]; arguments []; default message [null]"
	}

	def "Should return true if copied steps projects ids' are equal to target project id"() {
		given:
			def copyStepModel = Mock(TestCaseTestStepsController.CopyStepModel) {
				getCopiedStepIds() >> [1L, 2L, 3L]
			}
			Long testCaseId = 1L
		and:
			testCaseModificationService.fetchProjectsIdsByTestStepsIds(copyStepModel.getCopiedStepIds()) >> [1L, 1L, 1L]
			testCaseModificationService.fetchTargetProjectId(testCaseId) >> 1L
			testCaseModificationService.areCopiedTestStepsProjectsIdsEqualToTargetProjectId([1L, 1L, 1L], 1L) >> true
		when:
			def res = controller.compareKeywordProjectsIds(copyStepModel, testCaseId)
		then:
			noExceptionThrown()
			res == ResponseEntity.ok(true)
	}

	def "Should return false if at least one copied step project ids' is not equal to target project id"() {
		given:
			def copyStepModel = Mock(TestCaseTestStepsController.CopyStepModel) {
				getCopiedStepIds() >> [1L, 2L, 3L]
			}
			Long testCaseId = 1L
		and:
			testCaseModificationService.fetchProjectsIdsByTestStepsIds(copyStepModel.getCopiedStepIds()) >> [2L, 1L, 3L]
			testCaseModificationService.fetchTargetProjectId(testCaseId) >> 1L
			testCaseModificationService.areCopiedTestStepsProjectsIdsEqualToTargetProjectId([2L, 1L, 3L], 1L) >> false
		when:
			def res = controller.compareKeywordProjectsIds(copyStepModel, testCaseId)
		then:
			noExceptionThrown()
			res == ResponseEntity.ok(false)
	}

	class MockActionWordParameter extends ActionWordParameter {
		Long setId

		MockActionWordParameter(Long setId) {
			this.setId = setId
		}

		Long getId() {
			return setId
		}

		void setId(Long newId) {
			setId = newId
		}
	}

}
