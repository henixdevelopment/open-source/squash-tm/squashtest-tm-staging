/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.home

import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2ClientProperties
import org.springframework.core.env.Environment
import org.squashtest.tm.service.plugin.PluginFinderService
import org.squashtest.tm.service.system.SystemAdministrationService
import spock.lang.Specification

class LoginPageControllerTest extends Specification {

    SystemAdministrationService systemAdministrationService = Mock()
    Environment environment = Mock()
    OAuth2ClientProperties oAuth2ClientProperties = Mock()
    PluginFinderService pluginFinderService = Mock()

    LoginPageController controller = new LoginPageController(systemAdministrationService, pluginFinderService)

    def setup() {
        controller.environment = environment
        controller.oAuth2ClientProperties = oAuth2ClientProperties
    }

    def "should format provider names to be in lowercase"() {
        given:
        Set<String> lowercaseProviderNames = new HashSet<>(Arrays.asList("gitlab", "google", "microsoft_azure"))
        Map<String, OAuth2ClientProperties.Provider> providers = new HashMap<String, OAuth2ClientProperties.Provider>()
        providers.put("GitLab", Mock(OAuth2ClientProperties.Provider))
        providers.put("GOOGLE", Mock(OAuth2ClientProperties.Provider))
        providers.put("microsoft_Azure", Mock(OAuth2ClientProperties.Provider))

        and:
        systemAdministrationService.findLoginMessage() >> "Welcome message"
        controller.environment.getActiveProfiles() >> Arrays.asList("")
        controller.oAuth2ClientProperties.getProvider() >> providers

        when:
        LoginPageController.WelcomePageData welcomePageData = controller.loadLoginPage()

        then:
        welcomePageData.oAuth2ProviderNames.size() == 3
        welcomePageData.oAuth2ProviderNames == lowercaseProviderNames
    }
}
