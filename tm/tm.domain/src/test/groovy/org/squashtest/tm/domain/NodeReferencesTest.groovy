/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain


import spock.lang.Specification
import spock.lang.Unroll

import static org.squashtest.tm.domain.NodeReferences.fromStrings

/**
 * @author JThebault
 */
class NodeReferencesTest extends Specification {

	@Unroll
	def "should extract library ids"() {
		expect:
		def references = fromStrings(idsAsString)
		references.extractLibraryIds() == expectedIds as Set

		where:
		idsAsString                                               || expectedIds
		["TestCaseLibrary-1"]                                     || [1L]
		["TestCaseLibrary-1", "TestCaseLibrary-1", "TestCaseLibrary-2",
		 "TestCaseLibrary-3", "TestCase-12", "TestCaseFolder-12"] || [1L, 2L, 3L]
	}

	@Unroll
	def "remove library "() {
		expect:
		NodeReferences references = fromStrings(idsAsString)
		references.removeLibraries()
		references.asSet() == exprectedNodes as Set

		where:
		idsAsString                                               || exprectedNodes
		["TestCaseLibrary-1"]                                     || []
		["TestCaseLibrary-1", "TestCaseLibrary-1", "TestCaseLibrary-2",
		 "TestCaseLibrary-3", "TestCase-12", "TestCaseFolder-12"] || [new NodeReference(NodeType.TEST_CASE, 12L), new NodeReference(NodeType.TEST_CASE_FOLDER, 12L)]
	}

}


