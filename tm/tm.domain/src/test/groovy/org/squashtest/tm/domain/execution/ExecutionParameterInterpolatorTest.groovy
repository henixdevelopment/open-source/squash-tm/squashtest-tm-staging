/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.execution

import org.squashtest.tm.domain.testcase.Dataset
import org.squashtest.tm.domain.testcase.DatasetParamValue
import org.squashtest.tm.domain.testcase.Parameter
import org.squashtest.tm.domain.testcase.TestCase
import spock.lang.Specification

class ExecutionParameterInterpolatorTest extends Specification {

	def "should attribute the correct values for delegate parameter with identical name"() {
		given: "A dataset containing two delegate parameters"
		TestCase calledTestCase1 = new TestCaseMock(-1L)
		TestCase calledTestCase2 = new TestCaseMock(-2L)

		Dataset dataset = new Dataset()
		dataset.addParameterValue(new DatasetParamValue(new Parameter("VAR", calledTestCase1), dataset, "Value A"))
		dataset.addParameterValue(new DatasetParamValue(new Parameter("VAR", calledTestCase2), dataset, "Value B"))

		when:
		ExecutionParameterInterpolator.CollectedParameters collectedParameters = ExecutionParameterInterpolator.collectParameters(dataset)

		then:
		expectedResult == ExecutionParameterInterpolator.replaceParams(source, testCaseId, collectedParameters)

		where:
		source 			| testCaseId 	| expectedResult
		null			| -1L			| null
		"no param"	 	| -1L			| "no param"
		"\${unknown}" 	| -1L			| "&lt;no_value&gt;"
		"\${VAR}" 		| -1L			| "Value A"
		"\${VAR}" 		| -2L			| "Value B"
		"\${VAR}" 		| -3L			| "&lt;no_value&gt;"
	}

	def "should perform value interpolation for keywords"() {
		given:
		Dataset dataset = new Dataset()
		dataset.addParameterValue(new DatasetParamValue(new Parameter("VAR", new TestCaseMock(-1L)), dataset, "Value A"))

		when:
		ExecutionParameterInterpolator.CollectedParameters collectedParameters = ExecutionParameterInterpolator.collectParameters(dataset)

		then:
		expectedResult == ExecutionParameterInterpolator.replaceKeywordParams(source, testCaseId, collectedParameters)

		where:
		source 			| testCaseId 	| expectedResult
		null			| -1L			| null
		"no param"	 	| -1L			| "no param"
		"\${VAR}" 		| -1L			| "\${VAR}"
		"<VAR>" 		| -1L			| "Value A"
		"<VAR>" 		| -42L			| "&lt;no_value&gt;"
	}

	def "should still collect with null dataset"() {
		when:
		ExecutionParameterInterpolator.CollectedParameters collected = ExecutionParameterInterpolator.collectParameters(null)

		then:
		noExceptionThrown()
		collected.parameterValuesByTestCase.isEmpty()
	}

	def "should check for null input at interpolation"() {
		when:
		ExecutionParameterInterpolator.replaceParams("source", null, null)

		then:
		thrown(NullPointerException.class)

		where:
		referenceTestCaseId | collectedParameters
		null				| null
		null				| ExecutionParameterInterpolator.CollectedParameters.empty()
		-1L					| null
	}

	class TestCaseMock extends TestCase {

		Long _id

		TestCaseMock(Long id) {
			this._id = id
		}

		@Override
		Long getId() {
			return _id
		}
	}
}
