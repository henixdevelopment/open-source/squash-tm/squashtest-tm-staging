/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.execution

import spock.lang.Specification
import spock.lang.Unroll

import static org.squashtest.tm.domain.execution.ExecutionStatus.BLOCKED
import static org.squashtest.tm.domain.execution.ExecutionStatus.CANCELLED
import static org.squashtest.tm.domain.execution.ExecutionStatus.ERROR
import static org.squashtest.tm.domain.execution.ExecutionStatus.FAILURE
import static org.squashtest.tm.domain.execution.ExecutionStatus.NOT_RUN
import static org.squashtest.tm.domain.execution.ExecutionStatus.READY
import static org.squashtest.tm.domain.execution.ExecutionStatus.RUNNING
import static org.squashtest.tm.domain.execution.ExecutionStatus.SETTLED
import static org.squashtest.tm.domain.execution.ExecutionStatus.SKIPPED
import static org.squashtest.tm.domain.execution.ExecutionStatus.SUCCESS
import static org.squashtest.tm.domain.execution.ExecutionStatus.UNTESTABLE
import static org.squashtest.tm.domain.execution.ExecutionStatus.WARNING
import static org.squashtest.tm.domain.execution.ExecutionStatus.computeNewStatusForAutomatedSuite
import static org.squashtest.tm.domain.execution.ExecutionStatus.values

/**
 * @author Gregory Fouquet
 *
 */
class AutomatedSuiteExecutionStatusTest extends Specification {

	def "a finished report with no cancelled and blocked statuses should produce a BLOCKED status"(){
		given :
		ExecutionStatusReport report = new ExecutionStatusReport()
		values().each { report.set(it, 3) }
        report.set(READY, 0)
        report.set(RUNNING, 0)
        report.set(CANCELLED, 0)

		expect :
        computeNewStatusForAutomatedSuite(report) == BLOCKED
	}

	def "a non-blocked, non-cancelled finished report with error statuses should produce a FAILURE status"(){

		given :
		ExecutionStatusReport report = new ExecutionStatusReport()
		values().each { report.set(it, 3) }
        report.set(READY, 0)
        report.set(RUNNING, 0)
		report.set(BLOCKED, 0)
        report.set(CANCELLED, 0)

		expect :
        FAILURE == computeNewStatusForAutomatedSuite(report)
	}

	def "a non-blocked, non-cancelled  non error, non not_run finished report with failure statuses should produce a FAILURE status"(){
		given :
		ExecutionStatusReport report = new ExecutionStatusReport()
		values().each { report.set(it, 3) }
        report.set(READY, 0)
        report.set(RUNNING, 0)
		report.set(BLOCKED, 0)
        report.set(CANCELLED, 0)
		report.set(ERROR, 0)
		report.set(NOT_RUN, 0)

		expect :
        computeNewStatusForAutomatedSuite(report) == FAILURE
	}

    def "a non-finished report with RUNNING statuses should produce a RUNNING status"(){
        given :
        ExecutionStatusReport report = new ExecutionStatusReport()
        values().each { report.set(it, 3) }
        report.set(READY, 0)

        expect :
        computeNewStatusForAutomatedSuite(report) == RUNNING
    }

    def "a non-finished report with READY statuses should produce a RUNNING status"(){
        given :
        ExecutionStatusReport report = new ExecutionStatusReport()
        values().each { report.set(it, 3) }
        report.set(RUNNING, 0)

        expect :
        computeNewStatusForAutomatedSuite(report) == RUNNING
    }

	def "should compute new status RUNNING"(){
		given :
		ExecutionStatusReport report = new ExecutionStatusReport()
		report.set(SUCCESS, 3)
		report.set(WARNING, 3)
		report.set(RUNNING, 3)
		report.set(READY, 3)
		report.set(SETTLED, 3)
		report.set(SKIPPED, 3)

		expect :
        computeNewStatusForAutomatedSuite(report) == RUNNING
	}

	@Unroll
	def "should compute new status #expected from #statuses"(){
		given :
		ExecutionStatusReport report = new ExecutionStatusReport()
		statuses.each { report.set(it, 3) }

		expect :
        computeNewStatusForAutomatedSuite(report) == expected

		where:
		statuses                                 | expected
		[SUCCESS, BLOCKED, SKIPPED]   		     | BLOCKED
		[SUCCESS, FAILURE, SKIPPED]              | FAILURE
		[SUCCESS, SETTLED, UNTESTABLE, SKIPPED]  | SUCCESS
		[SUCCESS, SETTLED, SKIPPED] 			 | SUCCESS
		[SUCCESS, SKIPPED]          			 | SUCCESS
		[SETTLED, SKIPPED]                       | SETTLED
		[SETTLED, UNTESTABLE, SKIPPED]           | SETTLED
		[UNTESTABLE, SKIPPED]                    | UNTESTABLE
		[SUCCESS, SETTLED, SKIPPED] 	         | SUCCESS
		[RUNNING, SKIPPED]                       | RUNNING
        [RUNNING, SKIPPED, CANCELLED]            | RUNNING
        [READY, CANCELLED]                       | RUNNING
		[SKIPPED]                                | SKIPPED
		[SUCCESS, SETTLED, SKIPPED, CANCELLED] 	 | CANCELLED
		[SKIPPED, CANCELLED]                     | CANCELLED
		[CANCELLED]                              | CANCELLED
	}
}
