/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.helper

import spock.lang.Specification


class EmojiStringHelperTest extends Specification{

	def "should encode only emojis in string list"() {
		given:
		List<String> emojiStrings = Arrays.asList("👋", "hello", "goodbye 😎")

		when:
		List<String> encodedEmojiStringList = EmojiStringHelper.findAndEncodeStringsWithEmoji(emojiStrings)

		then:
		encodedEmojiStringList.size() == 3
		encodedEmojiStringList.stream()
			.filter {it -> it.contains(EmojiStringHelper.EMOJI_PREFIX)}
			.toList()
			.size() == 2

	}

	def "should encode a simple string containing multiple emojis"() {
		given:
		String emojiString = "😴🤐👻"

		when:
		String result = EmojiStringHelper.findAndEncodeSimpleStringWithEmoji(emojiString)

		then:
		result != null
		result.findAll(EmojiStringHelper.BASE64_ENCODED_EMOJI_REGEX).size() == 3
		result.find(EmojiStringHelper.EMOJI_REGEX) == null
	}

	def "should encode a simple string containing text and emojis"() {
		given:
		String emojiString = "🤡 Thank god it's Friday 🥳👽"

		when:
		String result = EmojiStringHelper.findAndEncodeSimpleStringWithEmoji(emojiString)

		then:
		result != null
		result.findAll(EmojiStringHelper.BASE64_ENCODED_EMOJI_REGEX).size() == 3
		result == "icon;base64/[8J+koQ==] Thank god it's Friday icon;base64/[8J+lsw==]icon;base64/[8J+RvQ==]"
	}

	def "should not encode a simple string"() {
		given:
		String simpleString = "random string content"

		when:
		String result = EmojiStringHelper.findAndEncodeSimpleStringWithEmoji(simpleString)

		then:
		result == simpleString
		result.findAll(EmojiStringHelper.BASE64_ENCODED_EMOJI_REGEX).size() == 0
	}

	def "should not throw error if string is null"() {
		given:
		String nullString = null

		when:
		String result = EmojiStringHelper.findAndEncodeSimpleStringWithEmoji(nullString)

		then:
		result == null
		noExceptionThrown()
	}

	def "should decode a list of strings containing encoded emojis"() {
		given:
		List<String> encodedEmojiList = Arrays.asList(
			"icon;base64/[8J+mhA==]",
			"plain text",
			"use icon;base64/[8J+Spw==] to put out the icon;base64/[8J+UpQ==]"
		)

		when:
		List<String> resultList = EmojiStringHelper.findAndDecodeStringsWithEmoji(encodedEmojiList)

		then:
		resultList.size() == 3
		resultList.stream()
			.filter {it -> it.find(EmojiStringHelper.EMOJI_REGEX) != null}
			.toList()
			.size() == 2
		resultList[0] == "🦄"
		resultList[1] == "plain text"
		resultList[2] == "use 💧 to put out the 🔥"
		noExceptionThrown()
	}

	def "should decode a simple string containing one or more emojis"() {
		given:
		String encodedEmojiString = "icon;base64/[8J+mgQ==], icon;base64/[8J+Qrw==] and icon;base64/[8J+Quw==], oh my!"

		when:
		String result = EmojiStringHelper.findAndDecodeSimpleStringWithEmoji(encodedEmojiString)

		then:
		result != null
		result.findAll(EmojiStringHelper.EMOJI_REGEX).size() == 3
		result == "🦁, 🐯 and 🐻, oh my!"
		noExceptionThrown()
	}

	def "should not decode a simple string without emojis"() {
		given:
		String simpleString = "I don't use emojis"

		when:
		String result = EmojiStringHelper.findAndDecodeSimpleStringWithEmoji(simpleString)

		then:
		result == simpleString
		result.findAll(EmojiStringHelper.EMOJI_REGEX).size() == 0
		noExceptionThrown()

	}
}
