/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain

import org.squashtest.tm.domain.execution.ExecutionStatus
import spock.lang.Specification

import java.util.function.Predicate

class LevelUtilsTest extends Specification {

	def "should filter level enum"() {
		expect:
		def set = LevelUtils.filterLevelEnum(ExecutionStatus.class, ExecutionStatus.Conclusiveness.&concludeToSuccess)
		set == [ExecutionStatus.SUCCESS, ExecutionStatus.WARNING, ExecutionStatus.SETTLED] as Set

	}

	def "should exclude from level enum"() {
		expect:
		// had to use classic inner anonymous class form because :
		// our groovy version doesn't support java lambda
		// using groovy closure cause error, maybe because trying to access to a default method with a java7 base class...
		// don't ask me why it choose the bad class... maybe Spock... ?
		def predicate = new Predicate<ExecutionStatus>() {

			@Override
			boolean test(ExecutionStatus executionStatus) {
				return executionStatus.isTerminatedStatus()
			}
		}
		def set = LevelUtils.excludeFromLevelEnum(ExecutionStatus.class, predicate)
		set == [ExecutionStatus.READY, ExecutionStatus.RUNNING] as Set

	}
}
