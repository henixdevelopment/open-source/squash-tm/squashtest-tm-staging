/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.campaign.testplan;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.Nullable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.domain.audit.Auditable;
import org.squashtest.tm.domain.audit.BaseAuditableEntity;
import org.squashtest.tm.domain.campaign.CampaignLibrary;
import org.squashtest.tm.domain.campaign.ExploratorySessionOverview;
import org.squashtest.tm.domain.common.ordered.Ordered;
import org.squashtest.tm.domain.common.ordered.OrderedListHelper;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.testcase.ConsumerForExploratoryTestCaseVisitor;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseExecutionMode;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.security.annotation.AclConstrainedObject;

@Entity
@Auditable
public class TestPlanItem extends BaseAuditableEntity implements Identified, Ordered {

    @Id
    @Column(name = "TEST_PLAN_ITEM_ID")
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "test_plan_item_test_plan_item_id_seq")
    @SequenceGenerator(
            name = "test_plan_item_test_plan_item_id_seq",
            sequenceName = "test_plan_item_test_plan_item_id_seq",
            allocationSize = 1)
    private Long id;

    @Column(name = "ITEM_ORDER")
    private Integer itemOrder;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TEST_PLAN_ID")
    private TestPlan testPlan;

    @Enumerated(EnumType.STRING)
    private ExecutionStatus executionStatus = ExecutionStatus.READY;

    private String label = "";

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ASSIGNEE_ID")
    private User assignee;

    @Column(insertable = false)
    private String lastExecutedBy;

    @Column(insertable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastExecutedOn;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TCLN_ID", referencedColumnName = "TCLN_ID")
    private TestCase referencedTestCase;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DATASET_ID", referencedColumnName = "DATASET_ID")
    private Dataset referencedDataset;

    @OneToMany(mappedBy = "sprintTestPlanItem", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("executionOrder")
    private final List<Execution> executions = new ArrayList<>();

    @OneToOne(
            mappedBy = "testPlanItem",
            cascade = {CascadeType.REMOVE, CascadeType.PERSIST})
    private ExploratorySessionOverview exploratorySessionOverview;

    public TestPlanItem(TestPlan testPlan, TestCase referencedTestCase) {
        this(testPlan, referencedTestCase, null);
    }

    public TestPlanItem(TestPlan testPlan, TestCase referencedTestCase, Dataset referencedDataset) {
        this.testPlan = testPlan;
        this.referencedTestCase = referencedTestCase;
        this.label = referencedTestCase.getName();
        this.referencedDataset = referencedDataset;

        referencedTestCase.accept(
                new ConsumerForExploratoryTestCaseVisitor(
                        exploratoryTestCase ->
                                exploratorySessionOverview =
                                        new ExploratorySessionOverview(exploratoryTestCase, this)));
    }

    protected TestPlanItem() {
        // Hibernate-friendly constructor
    }

    @Override
    public Long getId() {
        return id;
    }

    public TestPlan getTestPlan() {
        return testPlan;
    }

    @AclConstrainedObject
    public CampaignLibrary getCampaignLibrary() {
        return testPlan.getCampaignLibrary();
    }

    public ExecutionStatus getExecutionStatus() {
        return executionStatus;
    }

    public String getLabel() {
        return label;
    }

    public User getAssignee() {
        return assignee;
    }

    public void setAssignee(User assignee) {
        this.assignee = assignee;
    }

    public String getLastExecutedBy() {
        return lastExecutedBy;
    }

    public Date getLastExecutedOn() {
        return lastExecutedOn;
    }

    public TestCase getReferencedTestCase() {
        return referencedTestCase;
    }

    public void setReferencedTestCase(TestCase referencedTestCase) {
        this.referencedTestCase = referencedTestCase;
    }

    public Dataset getReferencedDataset() {
        return referencedDataset;
    }

    public void setReferencedDataset(@Nullable Dataset dataset) {
        if (dataset == null) {
            this.referencedDataset = null;
        } else {
            final long referencedTestCaseId = referencedTestCase.getId();
            final long datasetTestCaseId = dataset.getTestCase().getId();

            if (referencedTestCaseId != datasetTestCaseId) {
                throw new IllegalArgumentException(
                        "Dataset #"
                                + dataset.getId()
                                + " doesn't belong to test case #"
                                + referencedTestCaseId);
            }

            this.referencedDataset = dataset;
        }
    }

    public List<Execution> getExecutions() {
        return executions;
    }

    public void addExecution(Execution executionToAdd) {
        executionToAdd.setSprintTestPlanItem(this);
        new OrderedListHelper<>(executions).addContent(executionToAdd);

        boolean isNotExploratory =
                !TestCaseExecutionMode.EXPLORATORY.equals(executionToAdd.getExecutionMode());

        if (isNotExploratory) {
            updateLatestExecutionFields();
        }
    }

    public void removeExecutionsById(List<Long> executionIds) {
        final boolean deletingLatestExecution = executionIds.contains(getLatestExecution().getId());

        executions.removeIf(execution -> executionIds.contains(execution.getId()));
        new OrderedListHelper<>(executions).reorder();

        if (deletingLatestExecution) {
            updateLatestExecutionFields();
        }
    }

    private void updateLatestExecutionFields() {
        final Execution latestExecution = getLatestExecution();

        if (latestExecution == null) {
            lastExecutedBy = null;
            lastExecutedOn = null;
            executionStatus = ExecutionStatus.READY;
        } else {
            lastExecutedBy = latestExecution.getLastExecutedBy();
            lastExecutedOn = latestExecution.getLastExecutedOn();
            executionStatus = latestExecution.getExecutionStatus();
        }
    }

    public ExploratorySessionOverview getExploratorySessionOverview() {
        return exploratorySessionOverview;
    }

    public void setExploratorySessionOverview(ExploratorySessionOverview exploratorySessionOverview) {
        this.exploratorySessionOverview = exploratorySessionOverview;
    }

    @Override
    public void setOrder(Integer order) {
        this.itemOrder = order;
    }

    public boolean isTestCaseDeleted() {
        return referencedTestCase == null;
    }

    public Project getProject() {
        return this.testPlan.getProject();
    }

    public void autoUpdateExecutionStatus() {
        final Execution latestExecution = getLatestExecution();

        executionStatus =
                latestExecution == null ? ExecutionStatus.READY : latestExecution.getExecutionStatus();
    }

    public void updateLastExecutionAndAssignee(Date lastExecutedOn, User lastExecutedBy) {
        this.lastExecutedBy = lastExecutedBy == null ? null : lastExecutedBy.getLogin();
        this.lastExecutedOn = lastExecutedOn;
        this.assignee = lastExecutedBy;
    }

    public void applyFastPass(ExecutionStatus status, User user) {
        executionStatus = status;
        lastExecutedBy = user.getLogin();
        lastExecutedOn = new Date();
        assignee = user;
    }

    public Execution getLatestExecution() {
        return executions.isEmpty() ? null : executions.get(executions.size() - 1);
    }

    public TestPlanItem createCopy(TestPlan testPlan) {
        final TestPlanItem copy = new TestPlanItem();

        copy.testPlan = testPlan;
        copy.executionStatus = ExecutionStatus.READY;
        copy.label = label;
        copy.assignee = assignee;
        copy.referencedTestCase = referencedTestCase;
        copy.referencedDataset = referencedDataset;
        copy.itemOrder = itemOrder;

        if (exploratorySessionOverview != null) {
            copy.exploratorySessionOverview = exploratorySessionOverview.createCopy();
            copy.exploratorySessionOverview.setTestPlanItem(copy);
        }

        return copy;
    }
}
