/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.gridconfiguration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.squashtest.tm.domain.Identified;

@Entity
@Table(name = "GRID_COLUMN_DISPLAY_CONFIGURATION")
public class GridColumnDisplayConfiguration implements Identified {
    public static final String CUF_COLUMN_PREFIX_FOR_CONFIGURATIONS = "cuf|";

    @Id
    @Column(name = "GCDC_ID")
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "grid_column_display_configuration_gcdc_id_seq")
    @SequenceGenerator(
            name = "grid_column_display_configuration_gcdc_id_seq",
            sequenceName = "grid_column_display_configuration_gcdc_id_seq",
            allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "GCDR_ID", referencedColumnName = "GCDR_ID")
    private GridColumnDisplayReference gridColumnDisplayReference;

    @NotNull
    @Column(name = "ACTIVE_COLUMN_ID")
    private String activeColumnId;

    @Override
    public Long getId() {
        return id;
    }

    public GridColumnDisplayReference getGridColumnDisplayReference() {
        return gridColumnDisplayReference;
    }

    public void setGridColumnDisplayReference(GridColumnDisplayReference gridColumnDisplayReference) {
        this.gridColumnDisplayReference = gridColumnDisplayReference;
    }

    public String getActiveColumnId() {
        return activeColumnId;
    }

    public void setActiveColumnId(String activeColumnId) {
        this.activeColumnId = activeColumnId;
    }

    /*
    / As we were forced to change cuf column name format in front for display purpose (new format is prefix 'cuf|' and cuf code),
    / we add this method to retrieve easily the same cuf column name as formated in front (now AbstractTestCaseSearchComponent) and saved in grid_column_display_configuration table.
    */
    public static String getCufColumnNameFromCufId(Long id) {
        return CUF_COLUMN_PREFIX_FOR_CONFIGURATIONS + id;
    }
}
