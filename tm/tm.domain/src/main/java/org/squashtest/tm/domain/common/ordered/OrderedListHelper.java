/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.common.ordered;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class OrderedListHelper<ENTITY extends Ordered> {

    private final List<ENTITY> orderedList;

    public OrderedListHelper(List<ENTITY> orderedList) {
        this.orderedList = Objects.requireNonNull(orderedList);
    }

    public void addContent(ENTITY entity) {
        this.addContent(entity, null);
    }

    public void addContent(ENTITY entity, Integer position) {
        Objects.requireNonNull(entity);
        boolean valid = new ListIndexValidator<>(orderedList, position).isValid();
        if (valid) {
            orderedList.add(position, entity);
        } else {
            orderedList.add(entity);
        }
        reorder();
    }

    public void addAllContent(List<ENTITY> entity, Integer position) {
        Objects.requireNonNull(entity);
        boolean valid = new ListIndexValidator<>(orderedList, position).isValid();
        if (valid) {
            orderedList.addAll(position, entity);
        } else {
            orderedList.addAll(entity);
        }
        reorder();
    }

    public void moveContent(Integer oldPosition, Integer newPosition) {
        ENTITY entity = orderedList.get(oldPosition);
        moveContent(entity, newPosition);
    }

    public void moveContent(ENTITY entity, Integer newPosition) {
        moveContent(Collections.singletonList(entity), newPosition);
    }

    public void moveContent(List<ENTITY> entities, Integer newPosition) {
        Objects.requireNonNull(entities);
        if (!entities.isEmpty()) {
            doMoveContent(entities, newPosition);
        }
    }

    private void doMoveContent(List<ENTITY> entities, Integer newPosition) {
        checkMovedEntitiesAreInList(entities);

        orderedList.removeAll(entities);
        Integer validPosition = new ListIndexValidator<>(orderedList, newPosition).coerce();
        orderedList.addAll(validPosition, entities);
        reorder();
    }

    private void checkMovedEntitiesAreInList(List<ENTITY> entities) {
        Set<ENTITY> notInList =
                entities.stream().filter(e -> !orderedList.contains(e)).collect(Collectors.toSet());

        if (!notInList.isEmpty()) {
            String msg =
                    String.format("%s cannot be moved, they are not in list %s", notInList, orderedList);
            throw new IllegalArgumentException(msg);
        }
    }

    public void reorder() {
        for (int i = 0; i < orderedList.size(); i++) {
            ENTITY node = orderedList.get(i);
            node.setOrder(i);
        }
    }
}
