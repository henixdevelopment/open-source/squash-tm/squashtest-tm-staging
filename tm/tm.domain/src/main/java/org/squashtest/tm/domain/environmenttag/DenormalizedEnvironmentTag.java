/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.environmenttag;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Size;
import org.squashtest.tm.domain.Sizes;

@Entity
public class DenormalizedEnvironmentTag {

    @Id
    @Column(name = "DET_ID")
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "denormalized_environment_tag_det_id_seq")
    @SequenceGenerator(
            name = "denormalized_environment_tag_det_id_seq",
            sequenceName = "denormalized_environment_tag_det_id_seq",
            allocationSize = 1)
    private Long id;

    @Column(name = "HOLDER_ID")
    private Long holderId;

    @Column(name = "HOLDER_TYPE")
    @Enumerated(EnumType.STRING)
    protected DenormalizedEnvironmentHolderType holderType;

    @Size(max = Sizes.VALUE_MAX)
    private String value;

    public DenormalizedEnvironmentTag(
            Long holderId, DenormalizedEnvironmentHolderType holderType, String value) {
        this.holderId = holderId;
        this.holderType = holderType;
        this.value = value;
    }

    public DenormalizedEnvironmentTag() {}

    public Long getId() {
        return id;
    }

    public Long getHolderId() {
        return holderId;
    }

    public void setHolderId(Long holderId) {
        this.holderId = holderId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public DenormalizedEnvironmentHolderType getHolderType() {
        return holderType;
    }

    public void setHolderType(DenormalizedEnvironmentHolderType holderType) {
        this.holderType = holderType;
    }
}
