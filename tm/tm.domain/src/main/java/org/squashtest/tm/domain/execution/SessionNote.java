/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.execution;

import java.util.Collections;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import org.hibernate.annotations.Type;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.domain.attachment.AttachmentHolder;
import org.squashtest.tm.domain.attachment.AttachmentList;
import org.squashtest.tm.domain.audit.Auditable;
import org.squashtest.tm.domain.audit.BaseAuditableEntity;
import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.domain.bugtracker.IssueDetector;
import org.squashtest.tm.domain.bugtracker.IssueList;
import org.squashtest.tm.domain.campaign.CampaignLibrary;
import org.squashtest.tm.domain.common.ordered.Ordered;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.security.annotation.AclConstrainedObject;

@Entity
@Auditable
public class SessionNote extends BaseAuditableEntity
        implements Identified, AttachmentHolder, IssueDetector, Ordered {

    @Id
    @Column(name = "NOTE_ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "session_note_note_id_seq")
    @SequenceGenerator(
            name = "session_note_note_id_seq",
            sequenceName = "session_note_note_id_seq",
            allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "EXECUTION_ID", nullable = false)
    private ExploratoryExecution execution;

    @Enumerated(EnumType.STRING)
    private SessionNoteKind kind;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String content;

    @OneToOne(
            cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REMOVE},
            fetch = FetchType.LAZY)
    @JoinColumn(name = "ATTACHMENT_LIST_ID", updatable = false)
    private final AttachmentList attachmentList = new AttachmentList();

    @OneToOne(
            cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REMOVE})
    @JoinColumn(name = "ISSUE_LIST_ID")
    private IssueList issueList = new IssueList();

    @Column(name = "NOTE_ORDER")
    private int noteOrder;

    @Override
    public Long getId() {
        return id;
    }

    public ExploratoryExecution getExecution() {
        return execution;
    }

    public void setExecution(ExploratoryExecution execution) {
        this.execution = execution;
    }

    public SessionNoteKind getKind() {
        return kind;
    }

    public void setKind(SessionNoteKind kind) {
        this.kind = kind;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setIssueList(IssueList issueList) {
        this.issueList = issueList;
    }

    public int getNoteOrder() {
        return noteOrder;
    }

    public void setNoteOrder(int noteOrder) {
        this.noteOrder = noteOrder;
    }

    @AclConstrainedObject
    public CampaignLibrary getCampaignLibrary() {
        return execution.getCampaignLibrary();
    }

    @Override
    public AttachmentList getAttachmentList() {
        return attachmentList;
    }

    @Override
    public Long getAttachmentHolderProjectId() {
        return getProject().getId();
    }

    @Override
    public EntityReference toEntityReference() {
        return new EntityReference(EntityType.SESSION_NOTE, getId());
    }

    @Override
    public IssueList getIssueList() {
        return issueList;
    }

    @Override
    public Project getProject() {
        return execution.getProject();
    }

    @Override
    public BugTracker getBugTracker() {
        return getProject().findBugTracker();
    }

    @Override
    public Long getIssueListId() {
        return issueList.getId();
    }

    @Override
    public List<Long> getAllIssueListId() {
        return Collections.singletonList(getIssueListId());
    }

    @Override
    public TestCase getReferencedTestCase() {
        return execution.getReferencedTestCase();
    }

    @Override
    public void detachIssue(Long id) {
        issueList.removeIssue(id);
    }

    @Override
    public void setOrder(Integer newOrder) {
        noteOrder = newOrder;
    }
}
