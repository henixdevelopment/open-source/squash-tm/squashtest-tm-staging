/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.exception.requirement.link;

import org.squashtest.tm.exception.DomainException;

public class CodesAndRolesInconsistentException extends DomainException {

    private static final String I18N_KEY =
            "sqtm-core.exception.requirements-links.code-and-role-are-inconsistent";
    private final String name;
    private final String code;

    public CodesAndRolesInconsistentException(String name, String code, String fieldName) {
        super(name + " does not match the existing role code: " + code, fieldName);
        this.name = name;
        this.code = code;
    }

    @Override
    public String getI18nKey() {
        return I18N_KEY;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    @Override
    public Object[] getI18nParams() {
        return new Object[] {name, code};
    }
}
