/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.environmentvariable;

import org.squashtest.tm.core.foundation.i18n.Internationalizable;

/**
 * The different types of environment variable entries. Each type has a pattern which fixes the
 * possible input values and an i18n key corresponding to the error message of the input pattern.
 */
public enum EVInputType implements Internationalizable {
    DROPDOWN_LIST(
            "^[\\u0020-\\u007E]*", "sqtm-core.entity.environment-variable.invalidValuePattern.simple"),
    PLAIN_TEXT(
            "^[\\u0020-\\u007E]*", "sqtm-core.entity.environment-variable.invalidValuePattern.simple"),
    INTERPRETED_TEXT(
            "^[a-zA-Z0-9_$]*", "sqtm-core.entity.environment-variable.invalidValuePattern.interpreted");

    public final String pattern;
    public final String i18nErrorKey;

    EVInputType(String pattern, String i18nErrorKey) {
        this.pattern = pattern;
        this.i18nErrorKey = i18nErrorKey;
    }

    @Override
    public String getI18nKey() {
        return "environmentVariable.inputType." + name();
    }
}
