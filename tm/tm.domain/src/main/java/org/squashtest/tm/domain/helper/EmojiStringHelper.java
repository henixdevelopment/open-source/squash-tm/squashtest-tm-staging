/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.helper;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;

/*
This helper uses regex patterns to identify strings containing at least one basic emoji and then encode or decode
them to/from Base64. This is a workaround to be able to save emoji strings in the database without modifying it directly.
Added for SQUASH-5957, if it becomes more commonly used, we can consider alternate solutions.
*/
public final class EmojiStringHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(EmojiStringHelper.class);

    // EMOJI_REGEX identifies a number of special characters, emoji and symbols.
    public static final String EMOJI_REGEX =
            "(?:[\\uD83C\\uDF00-\\uD83D\\uDDFF]|[\\uD83E\\uDD00-\\uD83E\\uDDFF]|[\\uD83D\\uDE00-\\uD83D\\uDE4F]|[\\uD83D\\uDE80-\\uD83D\\uDEFF]|[\\u2600-\\u26FF]\\uFE0F?|[\\u2700-\\u27BF]\\uFE0F?|\\u24C2\\uFE0F?|[\\uD83C\\uDDE6-\\uD83C\\uDDFF]{1,2}"
                    + "|[\\uD83C\\uDD70\\uD83C\\uDD71\\uD83C\\uDD7E\\uD83C\\uDD7F\\uD83C\\uDD8E\\uD83C\\uDD91-\\uD83C\\uDD9A]\\uFE0F?|[\\u0023\\u002A\\u0030-\\u0039]\\uFE0F?\\u20E3|[\\u2194-\\u2199\\u21A9-\\u21AA]\\uFE0F?|[\\u2B05-\\u2B07\\u2B1B\\u2B1C\\u2B50\\u2B55]\\uFE0F?|[\\u2934\\u2935]\\uFE0F?|"
                    + "[\\u3030\\u303D]\\uFE0F?|[\\u3297\\u3299]\\uFE0F?|[\\uD83C\\uDE01\\uD83C\\uDE02\\uD83C\\uDE1A\\uD83C\\uDE2F\\uD83C\\uDE32-\\uD83C\\uDE3A\\uD83C\\uDE50\\uD83C\\uDE51]\\uFE0F?|[\\u203C\\u2049]\\uFE0F?|[\\u25AA\\u25AB\\u25B6\\u25C0\\u25FB-\\u25FE]\\uFE0F?|[\\u00A9\\u00AE]\\uFE0F?|"
                    + "[\\u2122\\u2139]\\uFE0F?|\\uD83C\\uDC04\\uFE0F?|\\uD83C\\uDCCF\\uFE0F?|[\\u231A\\u231B\\u2328\\u23CF\\u23E9-\\u23F3\\u23F8-\\u23FA]\\uFE0F?)";

    /*BASE64_ENCODED_EMOJI_REGEX finds the substrings that were previously encoded in Base64 by searching for content
    in brackets with the EMOJI_PREFIX.*/
    public static final String BASE64_ENCODED_EMOJI_REGEX = "icon;base64\\/\\[[^\\]\\[]*]";
    public static final String EMOJI_PREFIX = "icon;base64/[";

    // private constructor for sonar
    private EmojiStringHelper() {}

    public static List<String> findAndEncodeStringsWithEmoji(List<String> emojiStrings) {
        List<String> formattedStrings = new ArrayList<>();

        emojiStrings.forEach(
                string -> {
                    string = findAndEncodeSimpleStringWithEmoji(string);
                    formattedStrings.add(string);
                });

        return formattedStrings;
    }

    /**
     * Uses regex to identify each emoji contained in a string and encode it in Base64
     *
     * @param string string with emoji or special characters
     * @return encoded string
     */
    public static String findAndEncodeSimpleStringWithEmoji(String string) {
        if (string != null) {
            Matcher matcher = Pattern.compile(EMOJI_REGEX).matcher(string);
            while (matcher.find()) {
                string = string.replace(matcher.group(), encodeInBase64(matcher.group()));
            }
        }
        return string;
    }

    public static List<String> findAndDecodeStringsWithEmoji(List<String> strings) {
        List<String> decodedStrings = new ArrayList<>();

        strings.forEach(
                string -> {
                    string = findAndDecodeSimpleStringWithEmoji(string);
                    decodedStrings.add(string);
                });

        return decodedStrings;
    }

    /**
     * Use regex to identify previously encoded special characters and emoji and decode them from
     * base64
     *
     * @param string String to be decoded
     * @return fully decoded string
     */
    public static String findAndDecodeSimpleStringWithEmoji(String string) {
        if (string != null && string.contains(EMOJI_PREFIX)) {
            Matcher matcher = Pattern.compile(BASE64_ENCODED_EMOJI_REGEX).matcher(string);
            while (matcher.find()) {
                // replace encoded substrings with decoded value
                string = string.replace(matcher.group(), decodeFromBase64(matcher.group()));
            }
        }

        return string;
    }

    private static String encodeInBase64(String value) {
        byte[] data = value.getBytes(StandardCharsets.UTF_8);
        return EMOJI_PREFIX + Base64.getEncoder().encodeToString(data) + "]";
    }

    private static String decodeFromBase64(String value) {
        value = value.replace(EMOJI_PREFIX, "").replace("]", "");
        try {
            byte[] data = Base64.getDecoder().decode(value);
            return new String(data, StandardCharsets.UTF_8);
        } catch (IllegalArgumentException e) {
            LOGGER.error("\"{}\" is not a valid Base64 scheme and cannot be decoded.", value, e);
            return value;
        }
    }
}
