/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.campaign;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.Persister;
import org.springframework.context.MessageSource;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.domain.audit.Auditable;
import org.squashtest.tm.domain.audit.BaseAuditableEntity;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.library.HasExecutionStatus;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.domain.testautomation.FailureDetail;
import org.squashtest.tm.domain.testcase.ConsumerForExploratoryTestCaseVisitor;
import org.squashtest.tm.domain.testcase.CreateExecutionFromTestCaseVisitor;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseExecutionMode;
import org.squashtest.tm.domain.testcase.TestCaseKind;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.exception.NotAutomatedException;
import org.squashtest.tm.exception.execution.TestPlanItemNotExecutableException;
import org.squashtest.tm.infrastructure.hibernate.IterationTestPlanItemPersister;
import org.squashtest.tm.security.annotation.AclConstrainedObject;
import org.squashtest.tm.security.annotation.InheritsAcls;

@NamedQuery(
        name = "IterationTestPlanItem.findAllByIdsOrderedByIterationTestPlan",
        query =
                "select tp.id from Iteration i join i.testPlans tp where tp.id in :testPlanIds order by index(tp)")
@NamedQuery(
        name = "IterationTestPlanItem.findAllByIdsOrderedBySuiteTestPlan",
        query =
                "select tp.id from TestSuite ts join ts.testPlan tp where ts.id = :suiteId and tp.id in :testPlanIds order by index(tp)")
@Entity
@Auditable
@InheritsAcls(constrainedClass = Iteration.class, collectionName = "testPlans")
@Persister(impl = IterationTestPlanItemPersister.class)
public class IterationTestPlanItem extends BaseAuditableEntity
        implements HasExecutionStatus, Identified {

    private static final Set<ExecutionStatus> LEGAL_EXEC_STATUS;
    public static final String SIMPLE_CLASS_NAME =
            "org.squashtest.tm.domain.campaign.IterationTestPlanItem";

    static {
        Set<ExecutionStatus> set = new HashSet<>();
        set.add(ExecutionStatus.SUCCESS);
        set.add(ExecutionStatus.BLOCKED);
        set.add(ExecutionStatus.FAILURE);
        set.add(ExecutionStatus.RUNNING);
        set.add(ExecutionStatus.READY);
        set.add(ExecutionStatus.UNTESTABLE);
        set.add(ExecutionStatus.SETTLED);
        set.add(ExecutionStatus.SKIPPED);
        set.add(ExecutionStatus.CANCELLED);
        LEGAL_EXEC_STATUS = Collections.unmodifiableSet(set);
    }

    @Id
    @Column(name = "ITEM_TEST_PLAN_ID")
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "item_test_plan_item_test_plan_id_seq")
    @SequenceGenerator(
            name = "item_test_plan_item_test_plan_id_seq",
            sequenceName = "item_test_plan_item_test_plan_id_seq",
            allocationSize = 1)
    private Long id;

    @Enumerated(EnumType.STRING)
    private ExecutionStatus executionStatus = ExecutionStatus.READY;

    private String label = "";

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID")
    private User user;

    @Column(insertable = false)
    private String lastExecutedBy;

    @Column(insertable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastExecutedOn;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TCLN_ID", referencedColumnName = "TCLN_ID")
    private TestCase referencedTestCase;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DATASET_ID", referencedColumnName = "DATASET_ID")
    private Dataset referencedDataset;

    @OrderColumn(name = "EXECUTION_ORDER")
    @JoinTable(
            name = "ITEM_TEST_PLAN_EXECUTION",
            joinColumns = @JoinColumn(name = "ITEM_TEST_PLAN_ID"),
            inverseJoinColumns = @JoinColumn(name = "EXECUTION_ID"))
    @OneToMany(
            cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH})
    private final List<Execution> executions = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinTable(
            name = "ITEM_TEST_PLAN_LIST",
            joinColumns = @JoinColumn(name = "ITEM_TEST_PLAN_ID", insertable = false, updatable = false),
            inverseJoinColumns =
                    @JoinColumn(name = "ITERATION_ID", insertable = false, updatable = false))
    private Iteration iteration;

    @OneToOne(
            mappedBy = "iterationTestPlanItem",
            cascade = {CascadeType.REMOVE, CascadeType.PERSIST})
    private ExploratorySessionOverview exploratorySessionOverview;

    @ManyToMany(
            cascade = {CascadeType.PERSIST, CascadeType.MERGE},
            mappedBy = "testPlan")
    private List<TestSuite> testSuites = new ArrayList<>();

    @OneToMany(
            mappedBy = "itpi",
            cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private List<FailureDetail> failureDetailList = new ArrayList<>();

    public IterationTestPlanItem() {
        super();
    }

    public IterationTestPlanItem(TestCase testCase, Dataset dataset, User assignee) {
        referencedTestCase = testCase;
        label = testCase.getName();
        referencedDataset = dataset;
        user = assignee;

        testCase.accept(
                new ConsumerForExploratoryTestCaseVisitor(
                        exploratoryTestCase ->
                                exploratorySessionOverview =
                                        new ExploratorySessionOverview(exploratoryTestCase, this)));
    }

    public IterationTestPlanItem(TestCase testCase, Dataset dataset) {
        this(testCase, dataset, null);
    }

    public IterationTestPlanItem(TestCase testCase) {
        this(testCase, null, null);
    }

    public Iteration getIteration() {
        return iteration;
    }

    @Override
    public ExecutionStatus getExecutionStatus() {
        return executionStatus;
    }

    @Override
    public Set<ExecutionStatus> getLegalStatusSet() {
        return LEGAL_EXEC_STATUS;
    }

    public void setExecutionStatus(ExecutionStatus executionStatus) {
        this.executionStatus = executionStatus;
    }

    /**
     * the IterationTestPlanItem will fetch the ExecutionStatus of the last "live" Execution in his
     * execution list
     */
    public void updateExecutionStatus() {
        if (executions.isEmpty()) {
            executionStatus = ExecutionStatus.READY;
        } else {
            Execution execution = getLatestExecution();
            executionStatus = execution.getExecutionStatus();
        }
    }

    public TestCase getReferencedTestCase() {
        return referencedTestCase;
    }

    public void setReferencedTestCase(TestCase referencedTestCase) {
        this.referencedTestCase = referencedTestCase;
    }

    @Override
    public Long getId() {
        return id;
    }

    public List<Execution> getExecutions() {
        return executions;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public String getLastExecutedBy() {
        return lastExecutedBy;
    }

    public void setLastExecutedBy(String lastExecutedBy) {
        this.lastExecutedBy = lastExecutedBy;
    }

    public Date getLastExecutedOn() {
        return lastExecutedOn;
    }

    /** that method will also forward the information to the iteration for update of autoset dates. */
    public void setLastExecutedOn(Date lastExecutedOn) {
        this.lastExecutedOn = lastExecutedOn;

        if (getIteration() != null) {
            getIteration().updateAutoDates(lastExecutedOn);
        }
    }

    public void addExecution(@NotNull Execution execution) {
        executions.add(execution);
        execution.notifyAddedTo(this);

        boolean isNotExploratory =
                !TestCaseExecutionMode.EXPLORATORY.equals(execution.getExecutionMode());
        if (isNotExploratory) {
            updateExecutionStatus();
        }

        // this means that getLastExecutedBy and getLastExecutedOn should be
        // reset and propagated to the Iteration this
        // object
        // is bound to.
        this.lastExecutedBy = null;
        this.lastExecutedOn = null;
        resetIterationDates();
    }

    public void notifyAutomatedExecutionAddition(Execution execution) {
        boolean isNotExploratory =
                !TestCaseExecutionMode.EXPLORATORY.equals(execution.getExecutionMode());
        if (isNotExploratory) {

            executionStatus = execution.getExecutionStatus();
        }

        this.lastExecutedBy = null;
        this.lastExecutedOn = null;
        resetIterationDates();
    }

    /**
     * Creates an execution of this item and returns it.
     *
     * <h3>WARNING</h3>
     *
     * <p>Will not check cyclic calls between the referenced test cases anymore (eg A calls B calls C
     * calls A). You have been warned
     *
     * @return the new execution
     */
    public Execution createExecution(MessageSource messageSource, Locale locale)
            throws TestPlanItemNotExecutableException {
        checkExecutable();
        CreateExecutionFromTestCaseVisitor createExecutionVisitor =
                new CreateExecutionFromTestCaseVisitor(referencedDataset, messageSource, locale);
        referencedTestCase.accept(createExecutionVisitor);
        return createExecutionVisitor.getCreatedExecution();
    }

    public Execution createAutomatedExecution() throws TestPlanItemNotExecutableException {

        if (!isAutomated()) {
            throw new NotAutomatedException();
        }

        Execution execution = createExecution(null, null);

        AutomatedExecutionExtender extender = new AutomatedExecutionExtender();
        extender.setAutomatedTest(referencedTestCase.getAutomatedTest());
        extender.setExecution(execution);
        execution.setAutomatedExecutionExtender(extender);

        return execution;
    }

    private void checkExecutable() throws TestPlanItemNotExecutableException {
        if (!isExecutableThroughIteration()) {
            throw new TestPlanItemNotExecutableException("Test case referenced by this item was deleted");
        }
    }

    public boolean isAutomated() {
        if (referencedTestCase == null) {
            return false;
        }
        return referencedTestCase.isAutomated()
                && referencedTestCase.getProject() != null
                && referencedTestCase.getProject().isTestAutomationEnabled();
    }

    public boolean isGherkin() {
        if (referencedTestCase == null) {
            return false;
        }
        return TestCaseKind.GHERKIN.equals(referencedTestCase.getKind());
    }

    public boolean isExploratory() {
        if (referencedTestCase == null) {
            return false;
        }
        return TestCaseKind.EXPLORATORY.equals(referencedTestCase.getKind());
    }

    private void resetIterationDates() {
        Iteration it = getIteration();
        if (it != null) {
            it.updateAutoDates(null);
        }
    }

    public void removeExecution(Execution execution) {
        boolean wasLastExecution = this.getLatestExecution().equals(execution);
        ListIterator<Execution> iterator = executions.listIterator();

        while (iterator.hasNext()) {
            Execution exec = iterator.next();
            if (exec.getId().equals(execution.getId())) {
                iterator.remove();
                break;
            }
        }
        if (wasLastExecution) {
            updateExecutionStatus();
            if (this.getLatestExecution() != null) {
                this.lastExecutedOn = this.getLatestExecution().getLastExecutedOn();
                this.lastExecutedBy = this.getLatestExecution().getLastExecutedBy();
            } else {
                this.lastExecutedOn = null;
                this.lastExecutedBy = null;
            }
            Iteration iter = this.getIteration();

            if (iter != null) {
                iter.updateAutoDatesAfterExecutionDetach(this);
            }
        }
    }

    /**
     * Factory method. Creates a copy of this object according to copy / paste rules.
     *
     * @return the copy, never <code>null</code>
     */
    public IterationTestPlanItem createCopy() {
        IterationTestPlanItem copy = new IterationTestPlanItem();

        copy.setExecutionStatus(ExecutionStatus.READY);
        copy.setLabel(this.label);
        copy.setReferencedTestCase(this.referencedTestCase);
        copy.setUser(this.user);
        copy.referencedDataset = this.referencedDataset;

        if (this.exploratorySessionOverview != null) {
            copy.exploratorySessionOverview = this.exploratorySessionOverview.createCopy();
            copy.exploratorySessionOverview.setIterationTestPlanItem(copy);
        }

        return copy;
    }

    @AclConstrainedObject
    public CampaignLibrary getCampaignLibrary() {
        return iteration.getCampaignLibrary();
    }

    public Project getProject() {
        if (iteration != null) {
            return iteration.getProject();
        } else if (!testSuites.isEmpty()) {
            return testSuites.get(0).getProject();
        }
        return null;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isExecutableThroughIteration() {
        // XX check if tester is assigned
        return !isTestCaseDeleted();
    }

    /**
     * @return true if (the item last execution with unexecuted step) or (item has no execution and is
     *     linked to a testCase).
     */
    public boolean isExecutableThroughTestSuite() {
        // XXX check if tester is assigned
        if (executions.isEmpty()) {
            return !this.isTestCaseDeleted();
        } else {
            return isLatestExecutionStillRunning();
        }
    }

    private boolean isLatestExecutionStillRunning() {
        return getLatestExecution().hasUnexecutedSteps();
    }

    /**
     * One should use {@link #isExecutableThroughIteration()} in favor of this method.
     *
     * @return
     */
    public boolean isTestCaseDeleted() {
        return getReferencedTestCase() == null;
    }

    /**
     * Checks id equality in case the comparison fails because in some cases, hibernate proxies make
     * the comparison fail.
     */
    private boolean isSameIteration(Iteration thisIteration, Iteration thatIteration) {

        boolean result;

        if (thisIteration.equals(thatIteration)) {
            result = true;
        } else {
            result = false;
            if (thisIteration.getId() != null && thatIteration.getId() != null) {
                result = thisIteration.getId().equals(thatIteration.getId());
            }
        }

        return result;
    }

    private void checkIsSameIterationTestSuite(Iteration suiteIteration, String suiteName) {
        if (!isSameIteration(this.iteration, suiteIteration)) {
            throw new IllegalArgumentException(
                    String.format(
                            "Item[%s] dont belong to Iteration[%s], it cannot be bound to TestSuite['%s']",
                            id, suiteIteration.getId(), suiteName));
        }
    }

    /**
     * Only if the test suite is in a transient state and the binding test suite to iteration is
     * skipped
     */
    public void addTestSuite(@NotNull TestSuite suite, Iteration suiteIteration) {
        checkIsSameIterationTestSuite(suiteIteration, suite.getName());
        this.testSuites.add(suite);
    }

    public void addTestSuite(@NotNull TestSuite suite) {
        checkIsSameIterationTestSuite(suite.getIteration(), suite.getName());
        this.testSuites.add(suite);
        suite.bindTestPlanItem(this);
    }

    public List<TestSuite> getTestSuites() {
        return this.testSuites;
    }

    public List<FailureDetail> getFailureDetailList() {
        return failureDetailList;
    }

    public String getTestSuiteNames() {

        StringBuilder builder = new StringBuilder();

        for (TestSuite suite : testSuites) {
            builder.append(suite.getName()).append(", ");
        }

        return builder.toString().replaceFirst(", $", ""); // this eliminates the last comma
    }

    public void setTestSuites(List<TestSuite> testSuites) {
        this.testSuites = testSuites;
    }

    /* package */ void setIteration(Iteration iteration) {
        this.iteration = iteration;
    }

    /**
     * @return the last {@linkplain Execution} or null if there is none
     */
    public Execution getLatestExecution() {
        if (!executions.isEmpty()) {
            return executions.get(executions.size() - 1);
        }
        return null;
    }

    public TestCaseExecutionMode getExecutionMode() {
        Execution latest = getLatestExecution();

        return latest == null ? TestCaseExecutionMode.UNDEFINED : latest.getExecutionMode();
    }

    public Dataset getReferencedDataset() {
        return referencedDataset;
    }

    public void setReferencedDataset(Dataset referencedDataset) {
        this.referencedDataset = referencedDataset;
    }

    /**
     * Return true if the item is assigned to the given user.
     *
     * @param userLogin : the login of the concerned user (may not be null)
     * @return true if the assigned user is not <code>null</code> and matches the given login.
     */
    public boolean isAssignedToUser(@NotNull String userLogin) {
        return this.user != null && this.user.getLogin().equals(userLogin);
    }

    public void addExecutionAtPos(Execution execution, int order) {
        executions.add(order, execution);
        execution.notifyAddedTo(this);
    }

    public Campaign getCampaign() {
        return getIteration().getCampaign();
    }

    public Set<Milestone> getMilestones() {
        return getIteration().getCampaign().getMilestones();
    }

    public ExploratorySessionOverview getExploratorySessionOverview() {
        return exploratorySessionOverview;
    }

    public void setExploratorySessionOverview(ExploratorySessionOverview exploratorySessionOverview) {
        this.exploratorySessionOverview = exploratorySessionOverview;
    }
}
