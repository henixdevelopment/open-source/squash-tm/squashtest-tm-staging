/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.projectimporter;

import static org.squashtest.tm.domain.projectimporter.PivotFormatImportStatus.PENDING;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.users.User;

@Entity
@Table(name = "PIVOT_FORMAT_IMPORT")
public class PivotFormatImport {

    @Id
    @Column(name = "PFI_ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "pivot_format_import_pfi_id_seq")
    @SequenceGenerator(
            name = "pivot_format_import_pfi_id_seq",
            sequenceName = "pivot_format_import_pfi_id_seq",
            allocationSize = 1)
    private long id;

    @Column(name = "NAME")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CREATED_BY")
    private User createdBy;

    @Column(updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "FILE_PATH")
    private String filePath;

    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private PivotFormatImportStatus status = PENDING;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROJECT_ID")
    private Project project;

    @Column(name = "IMPORT_TYPE")
    @Enumerated(EnumType.STRING)
    private PivotFormatImportType type;

    @Column(name = "SUCCESSFULLY_IMPORTED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date successfullyImportedOn;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public PivotFormatImportStatus getStatus() {
        return status;
    }

    public void setStatus(PivotFormatImportStatus status) {
        this.status = status;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Date getSuccessfullyImportedOn() {
        return successfullyImportedOn;
    }

    public void setSuccessfullyImportedOn(Date successfullyImportedOn) {
        this.successfullyImportedOn = successfullyImportedOn;
    }

    public PivotFormatImportType getType() {
        return type;
    }

    public void setType(PivotFormatImportType source) {
        this.type = source;
    }
}
