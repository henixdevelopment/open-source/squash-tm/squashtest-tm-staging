/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.bugtracker;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.squashtest.tm.domain.Sizes;
import org.squashtest.tm.domain.common.ordered.Ordered;
import org.squashtest.tm.domain.project.GenericProject;

@Entity
public class BugtrackerProject implements Ordered {

    @Id
    @Column(name = "BUGTRACKER_PROJECT_ID")
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "bugtracker_project_bugtracker_project_id_seq")
    @SequenceGenerator(
            name = "bugtracker_project_bugtracker_project_id_seq",
            sequenceName = "bugtracker_project_bugtracker_project_id_seq",
            allocationSize = 1)
    private Long id;

    @NotNull
    @Size(max = Sizes.NAME_MAX)
    private String bugtrackerProjectName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROJECT_ID", nullable = false)
    private GenericProject project;

    @Column(name = "BUGTRACKER_PROJECT_ORDER")
    private int order;

    public BugtrackerProject(String bugtrackerProjectName, GenericProject project) {
        this.bugtrackerProjectName = bugtrackerProjectName;
        this.project = project;
    }

    public BugtrackerProject() {}

    public BugtrackerProject(String bugtrackerProjectName, GenericProject project, int order) {
        this.bugtrackerProjectName = bugtrackerProjectName;
        this.project = project;
        this.order = order;
    }

    public Long getId() {
        return id;
    }

    public String getBugtrackerProjectName() {
        return bugtrackerProjectName;
    }

    public void setBugtrackerProjectName(String bugtrackerProjectName) {
        this.bugtrackerProjectName = bugtrackerProjectName;
    }

    public GenericProject getProject() {
        return project;
    }

    public void setProject(GenericProject project) {
        this.project = project;
    }

    public int getOrder() {
        return order;
    }

    @Override
    public void setOrder(Integer newOrder) {
        this.order = newOrder;
    }
}
