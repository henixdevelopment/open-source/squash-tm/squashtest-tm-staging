/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.acl;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Type;
import org.squashtest.tm.core.foundation.sanitizehtml.HTMLSanitizeUtils;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.domain.Sizes;
import org.squashtest.tm.domain.audit.Auditable;
import org.squashtest.tm.domain.audit.BaseAuditableEntity;

@Entity
@Table(name = "ACL_GROUP")
@Auditable
public class AclGroup extends BaseAuditableEntity implements Identified {

    private static final String SYSTEM_PROFILE_START = "squashtest.acl.group.tm.";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "acl_group_id_seq")
    @SequenceGenerator(
            name = "acl_group_id_seq",
            sequenceName = "acl_group_id_seq",
            allocationSize = 1)
    private long id;

    @Size(max = Sizes.ACL_GROUP_QUALIFIED_NAME_MAX)
    @NotNull
    private String qualifiedName;

    private boolean active = true;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String description = "";

    private transient String simpleName;

    public AclGroup(long id, String qualifiedName) {
        super();
        this.id = id;
        this.qualifiedName = qualifiedName;
        if (qualifiedName != null) {
            calculateSimpleName();
        }
    }

    public AclGroup(String qualifiedName, String description) {
        this(0, qualifiedName);
        this.description = description;
    }

    public AclGroup() {}

    public String getQualifiedName() {
        return qualifiedName;
    }

    public void setQualifiedName(String qualifiedName) {
        checkAndForbidOperationForDefaultSystemProfile();
        this.qualifiedName = qualifiedName;
    }

    @Override
    public Long getId() {
        return id;
    }

    public String getSimpleName() {
        if (simpleName == null && qualifiedName != null) {
            calculateSimpleName();
        }

        return simpleName;
    }

    private void calculateSimpleName() {
        this.simpleName = qualifiedName.substring(qualifiedName.lastIndexOf('.') + 1);
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getDescription() {
        return HTMLSanitizeUtils.cleanHtml(description);
    }

    public void setDescription(@NotNull String description) {
        checkAndForbidOperationForDefaultSystemProfile();
        this.description = HTMLSanitizeUtils.cleanHtml(description);
    }

    public boolean isSystem() {
        return isSystem(qualifiedName);
    }

    public static boolean isSystem(String qualifiedName) {
        return qualifiedName.startsWith(SYSTEM_PROFILE_START);
    }

    public void checkAndForbidOperationForDefaultSystemProfile() {
        checkAndForbidOperationForDefaultSystemProfile(qualifiedName);
    }

    public static void checkAndForbidOperationForDefaultSystemProfile(String qualifiedName) {
        if (isSystem(qualifiedName)) {
            throw new UnsupportedOperationException(
                    "This operation is not allowed for the default system profile.");
        }
    }
}
