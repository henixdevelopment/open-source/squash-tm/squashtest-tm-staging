/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.environmentvariable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.domain.Sizes;
import org.squashtest.tm.exception.environmentvariable.ValueDoesNotMatchesPatternException;

@Entity
@DiscriminatorColumn(name = "FIELD_TYPE", discriminatorType = DiscriminatorType.STRING)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("EV")
public class EnvironmentVariable implements Identified {

    public static final String NAME_REGEXP = "^[A-Za-z0-9_]*$";

    @Id
    @Column(name = "EV_ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "environment_variable_ev_id_seq")
    @SequenceGenerator(
            name = "environment_variable_ev_id_seq",
            sequenceName = "environment_variable_ev_id_seq",
            allocationSize = 1)
    protected Long id;

    @NotBlank
    @Size(max = Sizes.NAME_MAX)
    @Pattern(regexp = NAME_REGEXP, message = "{org.squashtest.tm.validation.constraint.onlyStdChars}")
    protected String name;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(updatable = false)
    protected EVInputType inputType = EVInputType.PLAIN_TEXT;

    /** For ORM purposes. */
    public EnvironmentVariable() {}

    public EnvironmentVariable(String name, EVInputType inputType) {
        this.name = name;
        this.inputType = inputType;
    }

    public EnvironmentVariable(EVInputType inputType) {
        this.inputType = inputType;
    }

    public void checkValueMatchPattern(String value) {
        if (!value.matches(inputType.pattern)) {
            throw new ValueDoesNotMatchesPatternException(value, inputType, "value");
        }
    }

    @Override
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.trim();
    }

    public EVInputType getInputType() {
        return inputType;
    }

    public void setInputType(EVInputType inputType) {
        this.inputType = inputType;
    }
}
