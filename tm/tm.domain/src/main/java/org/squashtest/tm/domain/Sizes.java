/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * This file is part of the Squash TM management services for SaaS / Squash On Demand
 * (saas.management.fragment) project. Copyright (C) 2015 - 2016 Henix, henix.fr - All Rights
 * Reserved
 *
 * <p>Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and
 * confidential
 *
 * <p>(C)Henix. Tous droits réservés.
 *
 * <p>Avertissement : ce programme est protégé par la loi relative au droit d'auteur et par les
 * conventions internationales. Toute reproduction ou distribution partielle ou totale du logiciel,
 * par quelque moyen que ce soit, est strictement interdite.
 */
package org.squashtest.tm.domain;

/**
 * Defines global size constraints
 *
 * @author Gregory Fouquet
 * @since 1.14.0 07/07/16
 */
public final class Sizes {
    public static final int ACL_GROUP_QUALIFIED_NAME_MAX = 50;
    public static final int AUTHOR_MAX = 255;
    public static final int ACTION_PROVIDER_KEY_MAX = 50;
    public static final int AUTOMATED_TEST_REF_MAX = 512;
    public static final int CODE_MAX = 30;
    public static final int CODE_MIN = 1;
    public static final int COLOUR_MAX = 7;
    public static final int CREATED_BY_MAX = 100;
    public static final int EXECUTION_NAME_MAX = 308;
    public static final int ICON_NAME_MAX = 100;
    public static final int KIND_30 = 30;
    public static final int KIND_50 = 50;
    public static final int LABEL_30 = 30;
    public static final int LABEL_50 = 50;
    public static final int LABEL_100 = 100;
    public static final int LABEL_MAX = 255;
    public static final int LOGIN_MAX = 100;
    public static final int MAIL_MAX = 255;
    public static final int NAME_50 = 50;

    public static final int NAME_MAX = 255;
    public static final int NAME_MIN = 1;
    public static final int PARAM_VALUE_MAX = 1024;
    public static final int PASSWORD_MAX = 256;
    public static final int PASSWORD_MIN = 6;
    public static final int PLUGIN_NAMESPACE_MAX = 255;
    public static final int PROPERTY_MAX = 100;
    public static final int REFERENCE_MAX = 50;
    public static final int ROLE_MAX = 50;
    public static final int SELECT_TYPE_MAX = 50;
    public static final int SELECT_VALUE_MAX = 1000;
    public static final int STATUS_MAX = 50;
    public static final int SUMMARY_MAX = 255;
    public static final int TASK_DIVISION_MAX = 255;
    public static final int TEXT_MAX = 255;
    public static final int URL_255 = 255;
    public static final int URL_300 = 300;
    public static final int VALUE_MAX = 255;

    private Sizes() {
        super();
    }
}
