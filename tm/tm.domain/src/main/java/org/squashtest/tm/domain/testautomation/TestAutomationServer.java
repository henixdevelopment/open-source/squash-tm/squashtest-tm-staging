/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.testautomation;

import static javax.persistence.EnumType.STRING;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Cacheable;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.domain.Sizes;
import org.squashtest.tm.domain.environmenttag.AutomationEnvironmentTag;
import org.squashtest.tm.domain.servers.ThirdPartyServer;

/**
 * An AutomatedTestServer represents both a repository of automated tests, and an automated test
 * execution platform.
 *
 * @author bsiri
 */
@NamedQuery(
        name = "TestAutomationServer.findByUrlAndLogin",
        query = "from TestAutomationServer where baseURL = :url and login = :login")
@NamedQuery(
        name = "testAutomationServer.findAllHostedProjects",
        query = "select p from TestAutomationProject p join p.server s where s.id = :serverId")
@NamedQuery(
        name = "testAutomationServer.hasBoundProjects",
        query = "select count(*) from TestAutomationProject where server.id = :serverId")
@NamedQuery(
        name = "testAutomationServer.dereferenceProjects",
        query =
                "update GenericProject set testAutomationServer = null where testAutomationServer.id = :serverId")
@NamedQuery(
        name = "testAutomationServer.deleteServer",
        query = "delete from TestAutomationServer serv where serv.id = :serverId")
@Entity
@PrimaryKeyJoinColumn(name = "SERVER_ID")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TestAutomationServer extends ThirdPartyServer implements Identified {

    private static final TestAutomationServerKind DEFAULT_KIND = TestAutomationServerKind.jenkins;

    /**
     * The kind of the remote TA server. It'll help selecting the correct connector. Default is {@link
     * #DEFAULT_KIND}
     */
    @Column
    @NotNull
    @Enumerated(STRING)
    private TestAutomationServerKind kind = DEFAULT_KIND;

    @Column(name = "MANUAL_SLAVE_SELECTION")
    private boolean manualSlaveSelection = false;

    @ElementCollection
    @CollectionTable(
            name = "AUTOMATION_ENVIRONMENT_TAG",
            joinColumns = @JoinColumn(name = "ENTITY_ID"))
    @Where(clause = "ENTITY_TYPE = 'TEST_AUTOMATION_SERVER'")
    private List<AutomationEnvironmentTag> environmentTags = new ArrayList<>();

    /**
     * From 4.0.0, SquashAUTOM servers can have a distinct URL to communicate with the Observer
     * service.
     */
    @org.hibernate.validator.constraints.URL
    @Size(max = Sizes.URL_255)
    @Column(name = "OBSERVER_URL")
    private String observerUrl;

    /**
     * From 5.0.0, SquashAUTOM servers can have a distinct URL to communicate with the EventBus
     * service.
     */
    @org.hibernate.validator.constraints.URL
    @Size(max = Sizes.URL_255)
    @Column(name = "EVENT_BUS_URL")
    private String eventBusUrl;

    @org.hibernate.validator.constraints.URL
    @Size(max = Sizes.URL_255)
    @Column(name = "KILLSWITCH_URL")
    private String killswitchUrl;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String additionalConfiguration = "";

    public TestAutomationServer() {
        super();
    }

    public TestAutomationServer(TestAutomationServerKind kind) {
        this.kind = kind;
    }

    public TestAutomationServerKind getKind() {
        return kind;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TestAutomationServer{");
        sb.append("id=").append(getId());
        sb.append(", name='").append(getName()).append('\'');
        sb.append(", url='").append(getUrl()).append('\'');
        sb.append(", kind='").append(kind).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public boolean isManualSlaveSelection() {
        return manualSlaveSelection;
    }

    public void setManualSlaveSelection(boolean manualSlaveSelection) {
        this.manualSlaveSelection = manualSlaveSelection;
    }

    public TestAutomationServer createCopy() {
        TestAutomationServer testAutomationServerCopy = new TestAutomationServer(this.getKind());
        testAutomationServerCopy.setName(this.getName());
        testAutomationServerCopy.setUrl(this.getUrl());
        testAutomationServerCopy.setDescription(this.getDescription());
        testAutomationServerCopy.setManualSlaveSelection(this.isManualSlaveSelection());
        return testAutomationServerCopy;
    }

    public List<AutomationEnvironmentTag> getEnvironmentTags() {
        return environmentTags;
    }

    public void setEnvironmentTags(List<AutomationEnvironmentTag> environmentTags) {
        this.environmentTags = environmentTags;
    }

    public String getObserverUrl() {
        return observerUrl;
    }

    public void setObserverUrl(String observerUrl) {
        this.observerUrl = observerUrl;
    }

    public String getEventBusUrl() {
        return eventBusUrl;
    }

    public void setEventBusUrl(String eventBusUrl) {
        this.eventBusUrl = eventBusUrl;
    }

    public String getAdditionalConfiguration() {
        return additionalConfiguration;
    }

    public void setAdditionalConfiguration(String additionalConfiguration) {
        this.additionalConfiguration = additionalConfiguration;
    }

    public String getKillswitchUrl() {
        return killswitchUrl;
    }

    public void setKillswitchUrl(String killSwitchUrl) {
        this.killswitchUrl = killSwitchUrl;
    }
}
