/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.environmentvariable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.persistence.CollectionTable;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OrderBy;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import org.springframework.security.acls.model.NotFoundException;
import org.squashtest.tm.domain.common.ordered.OrderedListHelper;
import org.squashtest.tm.domain.helper.EntityOptionHelper;

@Entity
@DiscriminatorColumn(name = "FIELD_TYPE", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("SSEV")
public class SingleSelectEnvironmentVariable extends EnvironmentVariable {

    @ElementCollection
    @CollectionTable(name = "ENVIRONMENT_VARIABLE_OPTION", joinColumns = @JoinColumn(name = "EV_ID"))
    @OrderBy("position asc")
    @Valid
    private List<EnvironmentVariableOption> options = new ArrayList<>();

    public SingleSelectEnvironmentVariable() {
        super(EVInputType.DROPDOWN_LIST);
    }

    public List<EnvironmentVariableOption> getOptions() {
        return options;
    }

    public void setOptions(List<EnvironmentVariableOption> options) {
        this.options = options;
    }

    public void addOption(EnvironmentVariableOption option) {
        checkLabelAvailability(option.getLabel());
        checkValueMatchPattern(option.getLabel());
        new OrderedListHelper<>(getOptions()).addContent(option, option.getPosition());
    }

    private void addOption(int index, String label) {
        EnvironmentVariableOption option = new EnvironmentVariableOption(label, index);
        addOption(option);
    }

    private void checkLabelAvailability(String label) {
        List<String> optionLabels = options.stream().map(EnvironmentVariableOption::getLabel).toList();
        EntityOptionHelper.checkLabelAvailability(label, optionLabels, "environment-variable");
    }

    public void changeOptionLabel(String optionLabel, String newLabel) {
        checkLabelAvailability(newLabel);
        checkValueMatchPattern(newLabel);
        int index = findIndexOfLabel(optionLabel);
        removeOption(optionLabel);
        addOption(index, newLabel);
    }

    private int findIndexOfLabel(String previousLabel) {
        for (EnvironmentVariableOption option : options) {
            if (previousLabel.equals(option.getLabel())) {
                return options.indexOf(option);
            }
        }
        throw new NotFoundException(previousLabel);
    }

    public void removeOptionAndReorderList(@NotBlank String label) {
        removeOption(label);
        new OrderedListHelper<>(getOptions()).reorder();
    }

    private void removeOption(@NotBlank String label) {
        Iterator<EnvironmentVariableOption> iterator = options.iterator();
        while (iterator.hasNext()) {
            if (label.equals(iterator.next().getLabel())) {
                iterator.remove();
                return;
            }
        }
    }

    public void moveOptions(int newIndex, List<String> labels) {
        List<EnvironmentVariableOption> selectedOptions =
                options.stream().filter(option -> labels.contains(option.getLabel())).toList();
        new OrderedListHelper<>(getOptions()).moveContent(selectedOptions, newIndex);
    }
}
