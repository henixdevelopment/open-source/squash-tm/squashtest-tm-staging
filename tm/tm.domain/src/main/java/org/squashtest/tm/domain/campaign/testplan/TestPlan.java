/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.campaign.testplan;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.domain.campaign.CampaignLibrary;
import org.squashtest.tm.domain.common.ordered.OrderedListHelper;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.security.annotation.AclConstrainedObject;

@Entity
public class TestPlan implements Identified {
    @Id
    @Column(name = "TEST_PLAN_ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "test_plan_test_plan_id_seq")
    @SequenceGenerator(
            name = "test_plan_test_plan_id_seq",
            sequenceName = "test_plan_test_plan_id_seq",
            allocationSize = 1)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CL_ID")
    private CampaignLibrary campaignLibrary;

    @OneToMany(mappedBy = "testPlan", cascade = CascadeType.ALL)
    @OrderBy("itemOrder")
    private List<TestPlanItem> testPlanItems = new ArrayList<>();

    public TestPlan(CampaignLibrary campaignLibrary) {
        this.campaignLibrary = campaignLibrary;
    }

    protected TestPlan() {
        // Hibernate-friendly constructor
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setCampaignLibrary(CampaignLibrary campaignLibrary) {
        this.campaignLibrary = campaignLibrary;
    }

    public void createAndAddTestPlanItems(List<TestCase> testCasesToAdd) {
        final List<TestPlanItem> createdItems = new ArrayList<>();

        for (TestCase testCase : testCasesToAdd) {
            final Set<Dataset> datasets = testCase.getDatasets();

            if (datasets == null || datasets.isEmpty()) {
                createdItems.add(new TestPlanItem(this, testCase));
            } else {
                final List<TestPlanItem> items =
                        testCase.getDatasets().stream()
                                .map(dataSet -> new TestPlanItem(this, testCase, dataSet))
                                .toList();
                createdItems.addAll(items);
            }
        }

        new OrderedListHelper<>(testPlanItems).addAllContent(createdItems, null);
    }

    public void createAndAddTestPlanItems(Map<TestCase, List<Dataset>> datasetsByTestCase) {
        final List<TestPlanItem> createdItems = new ArrayList<>();

        for (Map.Entry<TestCase, List<Dataset>> entry : datasetsByTestCase.entrySet()) {
            final TestCase testCase = entry.getKey();
            final List<Dataset> datasets = entry.getValue();

            if (datasets == null || datasets.isEmpty()) {
                createdItems.add(new TestPlanItem(this, testCase));
            } else {
                final List<TestPlanItem> items =
                        datasets.stream().map(dataSet -> new TestPlanItem(this, testCase, dataSet)).toList();
                createdItems.addAll(items);
            }
        }

        new OrderedListHelper<>(testPlanItems).addAllContent(createdItems, null);
    }

    public void removeTestPlanItem(TestPlanItem itemToRemove) {
        testPlanItems.remove(itemToRemove);
        new OrderedListHelper<>(testPlanItems).reorder();
    }

    public List<TestPlanItem> getTestPlanItems() {
        return Collections.unmodifiableList(testPlanItems);
    }

    public Project getProject() {
        // Cast should be safe as we cannot have test plans in project templates
        return (Project) campaignLibrary.getProject();
    }

    @AclConstrainedObject
    public CampaignLibrary getCampaignLibrary() {
        return campaignLibrary;
    }

    public void moveItems(List<TestPlanItem> items, int position) {
        new OrderedListHelper<>(testPlanItems).moveContent(items, position);
    }

    public void copyItems(TestPlan source) {
        final List<TestPlanItem> itemsToCopy =
                source.getTestPlanItems().stream().map(item -> item.createCopy(this)).toList();

        new OrderedListHelper<>(testPlanItems).addAllContent(itemsToCopy, null);
    }
}
