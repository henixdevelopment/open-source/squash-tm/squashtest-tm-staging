/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.library;

import java.util.ArrayList;
import java.util.List;
import org.squashtest.tm.domain.attachment.Attachment;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.exception.DuplicateNameException;

/**
 * This class is meant to be used as a delegate when one implements a {@link
 * org.squashtest.tm.domain.campaign.SprintGroup}.
 *
 * @param <NODE> type of LibraryNode
 */
public class SprintGroupSupport<
        NODE extends LibraryNode, SPRINT_GROUP extends SprintGroupContainer<NODE>> {
    /** The sprint group which delegates operations to this object. */
    private final SPRINT_GROUP sprintGroup;

    public SprintGroupSupport(SPRINT_GROUP sprintGroupContainer) {
        super();
        this.sprintGroup = sprintGroupContainer;
    }

    /**
     * If the node to be added to the sprint group is a sprint, the method does not check if its name
     * has already been given to another sprint in the sprint group. In a sprint group 2 sprints can
     * have the same name.
     *
     * @param node
     */
    public void addContent(NODE node) {
        if (!node.allowIdenticalName()) {
            checkContentNameAvailable(node);
        }
        sprintGroup.getContent().add(node);
        node.notifyAssociatedWithProject(sprintGroup.getProject());
    }

    /**
     * If the node to be added to the sprint group is a sprint, the method does not check if its name
     * has already been given to another sprint in the sprint group. In a sprint group 2 sprints can
     * have the same name.
     *
     * @param node
     * @param position
     */
    public void addContent(NODE node, int position) {
        if (!node.allowIdenticalName()) {
            checkContentNameAvailable(node);
        }
        if (position >= sprintGroup.getContent().size() || position < 0) {
            sprintGroup.addContent(node);
        } else {
            sprintGroup.getContent().add(position, node);
        }
        node.notifyAssociatedWithProject(sprintGroup.getProject());
    }

    private void checkContentNameAvailable(NODE candidateContent) throws DuplicateNameException {
        if (!this.sprintGroup.allowContentWithIdenticalName()
                && !isContentNameAvailable(candidateContent.getName())) {
            throw new DuplicateNameException(candidateContent.getName(), candidateContent.getName());
        }
    }

    /**
     * Tells if the given name is already attributed to any of {@link #sprintGroup}'s content.
     *
     * @param name
     * @return
     */
    public boolean isContentNameAvailable(String name) {
        for (NODE folderContent : sprintGroup.getContent()) {
            if (folderContent.getName().equals(name)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Notifies that the project was set to something. Notifies each of {@link #sprintGroup}'s content
     * it is now associated with a new project.
     *
     * <p>We dont want to expose a "setProject" method in folders, so the folder is reponsible for
     * setting the project association, then it can extend the operation by calling this method.
     *
     * @param formerProject former value of {@link #sprintGroup}'s associated project
     * @param currentProject new value of {@link #sprintGroup}'s associated project
     */
    public void notifyAssociatedProjectWasSet(Project formerProject, Project currentProject) {
        if (notSameProject(formerProject, currentProject)) {
            for (NODE node : sprintGroup.getContent()) {
                node.notifyAssociatedWithProject(currentProject);
            }
        }
    }

    private boolean notSameProject(Project thisProject, Project thatProject) {
        if (thisProject == null && thatProject == null) {
            return false;
        } else if (thisProject == null) {
            return true;
        } else {
            return !thisProject.equals(thatProject);
        }
    }

    public boolean hasContent() {
        return !sprintGroup.getContent().isEmpty();
    }

    public SPRINT_GROUP createCopy(SPRINT_GROUP newFolder) {
        newFolder.setName(sprintGroup.getName());
        newFolder.setDescription(sprintGroup.getDescription());
        newFolder.notifyAssociatedWithProject(sprintGroup.getProject());
        addCopiesOfAttachments(sprintGroup, newFolder);
        return newFolder;
    }

    private void addCopiesOfAttachments(
            SprintGroupContainer<NODE> source, SprintGroupContainer<NODE> destination) {
        for (Attachment tcAttach : source.getAttachmentList().getAllAttachments()) {
            Attachment atCopy = tcAttach.shallowCopy();
            destination.getAttachmentList().addAttachment(atCopy);
        }
    }

    public List<String> getContentNames() {
        List<String> contentNames = new ArrayList<>(sprintGroup.getContent().size());
        for (NODE node : sprintGroup.getContent()) {
            contentNames.add(node.getName());
        }
        return contentNames;
    }
}
