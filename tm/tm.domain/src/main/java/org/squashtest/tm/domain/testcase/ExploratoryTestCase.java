/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.testcase;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.PrimaryKeyJoinColumn;
import org.hibernate.annotations.Type;

@Entity
@PrimaryKeyJoinColumn(name = "TCLN_ID")
public class ExploratoryTestCase extends TestCase {

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String charter = "";

    /** The test session duration in minutes. */
    @Column private Integer sessionDuration;

    public ExploratoryTestCase() {
        super();
        setExecutionMode(TestCaseExecutionMode.EXPLORATORY);
    }

    @Override
    public void accept(TestCaseVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public ExploratoryTestCase createCopy() {
        ExploratoryTestCase copy = new ExploratoryTestCase();
        populateCopiedTestCaseAttributes(copy);
        copy.setCharter(this.getCharter());
        copy.setSessionDuration(this.getSessionDuration());
        return copy;
    }

    public String getCharter() {
        return charter;
    }

    public void setCharter(String charter) {
        this.charter = charter;
    }

    public Integer getSessionDuration() {
        return sessionDuration;
    }

    public void setSessionDuration(Integer sessionDuration) {
        this.sessionDuration = sessionDuration;
    }
}
