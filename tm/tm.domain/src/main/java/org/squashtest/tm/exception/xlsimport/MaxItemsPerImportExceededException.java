/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.exception.xlsimport;

import java.io.Serial;
import org.squashtest.tm.core.foundation.exception.ActionException;
import org.squashtest.tm.domain.XlsEntityKind;

public class MaxItemsPerImportExceededException extends ActionException {
    @Serial private static final long serialVersionUID = 4967853448753696589L;

    private final int currentItemsCount;
    private final int maxItemsPerImport;
    private final String i18nKey;

    public MaxItemsPerImportExceededException(
            XlsEntityKind entityKind, int currentItemsCount, int maxItemsPerImport, String message) {
        super(message);
        this.currentItemsCount = currentItemsCount;
        this.maxItemsPerImport = maxItemsPerImport;
        this.i18nKey = getI18nKeyForEntityKind(entityKind);
    }

    private String getI18nKeyForEntityKind(XlsEntityKind entityKind) {
        return switch (entityKind) {
            case TEST_CASES -> "sqtm-core.exception.xls-imports.max-test-cases-per-import-exceeded";
            case TEST_STEPS -> "sqtm-core.exception.xls-imports.max-test-steps-per-import-exceeded";
            case REQUIREMENTS -> "sqtm-core.exception.xls-imports.max-requirements-per-import-exceeded";
        };
    }

    @Override
    public String getI18nKey() {
        return i18nKey;
    }

    @Override
    public Object[] messageArgs() {
        return new Object[] {currentItemsCount, maxItemsPerImport};
    }
}
