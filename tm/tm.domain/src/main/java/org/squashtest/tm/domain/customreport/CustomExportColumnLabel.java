/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.customreport;

import static org.jooq.impl.DSL.concat;
import static org.jooq.impl.DSL.count;
import static org.jooq.impl.DSL.countDistinct;
import static org.jooq.impl.DSL.groupConcatDistinct;
import static org.jooq.impl.DSL.nullif;
import static org.jooq.impl.DSL.select;
import static org.jooq.impl.DSL.val;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_FOLDER;
import static org.squashtest.tm.jooq.domain.Tables.DATASET;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_EXECUTION_STEPS;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_ISSUES_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_STEP;
import static org.squashtest.tm.jooq.domain.Tables.INFO_LIST_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.ISSUE;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_COVERAGE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.VERIFYING_STEPS;
import static org.squashtest.tm.jooq.domain.tables.CampaignLibraryNode.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.tables.Iteration.ITERATION;
import static org.squashtest.tm.jooq.domain.tables.TestSuite.TEST_SUITE;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.jooq.Field;
import org.jooq.TableField;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.squashtest.tm.core.foundation.i18n.Internationalizable;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.jooq.domain.Tables;

public enum CustomExportColumnLabel implements Internationalizable {

    // --- CAMPAIGN ---
    CAMPAIGN_LABEL(
            I18nKeys.I18N_KEY_LABEL,
            CAMPAIGN_LIBRARY_NODE.NAME,
            CAMPAIGN_LIBRARY_NODE.CLN_ID,
            EntityType.CAMPAIGN),

    CAMPAIGN_ID(I18nKeys.I18N_KEY_ID, CAMPAIGN.CLN_ID, CAMPAIGN.CLN_ID, EntityType.CAMPAIGN),

    CAMPAIGN_REFERENCE(
            I18nKeys.I18N_KEY_REFERENCE, CAMPAIGN.REFERENCE, CAMPAIGN.CLN_ID, EntityType.CAMPAIGN),

    CAMPAIGN_DESCRIPTION(
            I18nKeys.I18N_KEY_DESCRIPTION,
            CAMPAIGN_LIBRARY_NODE.DESCRIPTION,
            CAMPAIGN_LIBRARY_NODE.CLN_ID,
            EntityType.CAMPAIGN),

    CAMPAIGN_STATE(
            I18nKeys.I18N_KEY_STATE, CAMPAIGN.CAMPAIGN_STATUS, CAMPAIGN.CLN_ID, EntityType.CAMPAIGN),

    CAMPAIGN_PROGRESS_STATUS(I18nKeys.I18N_KEY_PROGRESS_STATUS, null, null, EntityType.CAMPAIGN),

    CAMPAIGN_MILESTONE(
            I18nKeys.I18N_KEY_MILESTONE,
            MILESTONE.as("camp_milestone").LABEL,
            MILESTONE.as("camp_milestone").MILESTONE_ID,
            EntityType.CAMPAIGN),

    CAMPAIGN_SCHEDULED_START(
            I18nKeys.I18N_KEY_SCHEDULED_START,
            CAMPAIGN.SCHEDULED_START_DATE,
            CAMPAIGN.CLN_ID,
            EntityType.CAMPAIGN),

    CAMPAIGN_SCHEDULED_END(
            I18nKeys.I18N_KEY_SCHEDULED_END,
            CAMPAIGN.SCHEDULED_END_DATE,
            CAMPAIGN.CLN_ID,
            EntityType.CAMPAIGN),

    CAMPAIGN_ACTUAL_START(
            I18nKeys.I18N_KEY_ACTUAL_START,
            CAMPAIGN.ACTUAL_START_DATE,
            CAMPAIGN.CLN_ID,
            EntityType.CAMPAIGN),

    CAMPAIGN_ACTUAL_END(
            I18nKeys.I18N_KEY_ACTUAL_END, CAMPAIGN.ACTUAL_END_DATE, CAMPAIGN.CLN_ID, EntityType.CAMPAIGN),

    CAMPAIGN_CUF(null, null, null, EntityType.CAMPAIGN),

    // --- ITERATION ---
    ITERATION_LABEL(
            I18nKeys.I18N_KEY_LABEL, ITERATION.NAME, ITERATION.ITERATION_ID, EntityType.ITERATION),

    ITERATION_ID(
            I18nKeys.I18N_KEY_ID, ITERATION.ITERATION_ID, ITERATION.ITERATION_ID, EntityType.ITERATION),

    ITERATION_REFERENCE(
            I18nKeys.I18N_KEY_REFERENCE,
            ITERATION.REFERENCE,
            ITERATION.ITERATION_ID,
            EntityType.ITERATION),

    ITERATION_DESCRIPTION(
            I18nKeys.I18N_KEY_DESCRIPTION,
            ITERATION.DESCRIPTION,
            ITERATION.ITERATION_ID,
            EntityType.ITERATION),

    ITERATION_STATE(
            I18nKeys.I18N_KEY_STATE,
            ITERATION.ITERATION_STATUS,
            ITERATION.ITERATION_ID,
            EntityType.ITERATION),

    ITERATION_SCHEDULED_START(
            I18nKeys.I18N_KEY_SCHEDULED_START,
            ITERATION.SCHEDULED_START_DATE,
            ITERATION.ITERATION_ID,
            EntityType.ITERATION),

    ITERATION_SCHEDULED_END(
            I18nKeys.I18N_KEY_SCHEDULED_END,
            ITERATION.SCHEDULED_END_DATE,
            ITERATION.ITERATION_ID,
            EntityType.ITERATION),

    ITERATION_ACTUAL_START(
            I18nKeys.I18N_KEY_ACTUAL_START,
            ITERATION.ACTUAL_START_DATE,
            ITERATION.ITERATION_ID,
            EntityType.ITERATION),

    ITERATION_ACTUAL_END(
            I18nKeys.I18N_KEY_ACTUAL_END,
            ITERATION.ACTUAL_END_DATE,
            ITERATION.ITERATION_ID,
            EntityType.ITERATION),

    ITERATION_CUF(null, null, null, EntityType.ITERATION),

    // -- TEST SUITE ---
    TEST_SUITE_LABEL(I18nKeys.I18N_KEY_LABEL, TEST_SUITE.NAME, TEST_SUITE.ID, EntityType.TEST_SUITE),

    TEST_SUITE_ID(I18nKeys.I18N_KEY_ID, TEST_SUITE.ID, TEST_SUITE.ID, EntityType.TEST_SUITE),

    TEST_SUITE_DESCRIPTION(
            I18nKeys.I18N_KEY_DESCRIPTION, TEST_SUITE.DESCRIPTION, TEST_SUITE.ID, EntityType.TEST_SUITE),

    TEST_SUITE_EXECUTION_STATUS(
            "chart.column.EXECUTION_STATUS",
            TEST_SUITE.EXECUTION_STATUS,
            TEST_SUITE.ID,
            EntityType.TEST_SUITE),

    TEST_SUITE_PROGRESS_STATUS(
            "test-suite.progress_status.label",
            Fields.TEST_SUITE_PROGRESS_STATUS,
            null,
            EntityType.TEST_SUITE),

    TEST_SUITE_CUF(null, null, TEST_SUITE.ID, EntityType.TEST_SUITE),

    // --- TEST CASE ---
    TEST_CASE_PROJECT("label.project", PROJECT.NAME, PROJECT.PROJECT_ID, EntityType.TEST_CASE),

    TEST_CASE_MILESTONE(
            I18nKeys.I18N_KEY_MILESTONES, Fields.TEST_CASE_MILESTONE, null, EntityType.TEST_CASE),

    TEST_CASE_LABEL(
            I18nKeys.I18N_KEY_LABEL,
            TEST_CASE_LIBRARY_NODE.NAME,
            TEST_CASE_LIBRARY_NODE.TCLN_ID,
            EntityType.TEST_CASE),

    TEST_CASE_ID(I18nKeys.I18N_KEY_ID, TEST_CASE.TCLN_ID, TEST_CASE.TCLN_ID, EntityType.TEST_CASE),

    TEST_CASE_REFERENCE(
            I18nKeys.I18N_KEY_REFERENCE, TEST_CASE.REFERENCE, TEST_CASE.TCLN_ID, EntityType.TEST_CASE),

    TEST_CASE_DESCRIPTION(
            I18nKeys.I18N_KEY_DESCRIPTION,
            TEST_CASE_LIBRARY_NODE.DESCRIPTION,
            TEST_CASE_LIBRARY_NODE.TCLN_ID,
            EntityType.TEST_CASE),

    TEST_CASE_STATUS(
            I18nKeys.I18N_KEY_STATUS, TEST_CASE.TC_STATUS, TEST_CASE.TCLN_ID, EntityType.TEST_CASE),

    TEST_CASE_IMPORTANCE(
            "label.Importance", TEST_CASE.IMPORTANCE, TEST_CASE.TCLN_ID, EntityType.TEST_CASE),

    TEST_CASE_NATURE(
            "chart.column.TEST_CASE_NATURE",
            INFO_LIST_ITEM.as("type_nature").LABEL,
            INFO_LIST_ITEM.as("type_nature").ITEM_ID,
            EntityType.TEST_CASE),

    TEST_CASE_TYPE(
            "label.Type",
            INFO_LIST_ITEM.as("type_list").LABEL,
            INFO_LIST_ITEM.as("type_list").ITEM_ID,
            EntityType.TEST_CASE),

    TEST_CASE_DATASET("label.dataset", DATASET.NAME, DATASET.DATASET_ID, EntityType.TEST_CASE),

    TEST_CASE_PREREQUISITE(
            "generics.prerequisite.title",
            TEST_CASE.PREREQUISITE,
            TEST_CASE.TCLN_ID,
            EntityType.TEST_CASE),

    TEST_CASE_LINKED_REQUIREMENTS_NUMBER(
            I18nKeys.I18N_KEY_LINKED_REQUIREMENTS_NUMBER,
            Fields.TEST_CASE_LINKED_REQUIREMENTS_NUMBER,
            null,
            EntityType.TEST_CASE),

    TEST_CASE_LINKED_REQUIREMENTS_IDS(
            I18nKeys.I18N_KEY_CUSTOM_EXPORT_COLUMN_LINKED_REQUIREMENTS_IDS,
            Fields.TEST_CASE_LINKED_REQUIREMENTS_IDS,
            null,
            EntityType.TEST_CASE),

    TEST_CASE_CUF(null, null, TEST_CASE.TCLN_ID, EntityType.TEST_CASE),

    // --- EXECUTION ---
    EXECUTION_ID(
            I18nKeys.I18N_KEY_ID, EXECUTION.EXECUTION_ID, EXECUTION.EXECUTION_ID, EntityType.EXECUTION),

    EXECUTION_EXECUTION_MODE(
            "label.ExecutionMode",
            EXECUTION.EXECUTION_MODE,
            EXECUTION.EXECUTION_ID,
            EntityType.EXECUTION),

    EXECUTION_STATUS(
            I18nKeys.I18N_KEY_STATUS,
            EXECUTION.EXECUTION_STATUS,
            EXECUTION.EXECUTION_ID,
            EntityType.EXECUTION),

    EXECUTION_SUCCESS_RATE(
            "shortLabel.SuccessRate", Fields.EXECUTION_SUCCESS_RATE, null, EntityType.EXECUTION),

    EXECUTION_USER(
            I18nKeys.I18N_KEY_USER,
            EXECUTION.LAST_EXECUTED_BY,
            EXECUTION.EXECUTION_ID,
            EntityType.EXECUTION),

    EXECUTION_EXECUTION_DATE(
            "iteration.executions.table.column-header.execution-date.label",
            EXECUTION.LAST_EXECUTED_ON,
            EXECUTION.EXECUTION_ID,
            EntityType.EXECUTION),

    EXECUTION_COMMENT(
            "executions.steps.table.column-header.comment.label",
            EXECUTION.DESCRIPTION,
            EXECUTION.EXECUTION_ID,
            EntityType.EXECUTION),

    EXECUTION_CUF(null, null, null, EntityType.EXECUTION),

    // --- EXECUTION STEP ---
    EXECUTION_STEP_STEP_NUMBER(
            "custom-export.column.EXECUTION_STEP.EXECUTION_STEP_NUMBER",
            EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ORDER.add(1),
            EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ORDER,
            EntityType.EXECUTION_STEP),

    EXECUTION_STEP_ACTION(
            "label.action",
            EXECUTION_STEP.ACTION,
            EXECUTION_STEP.EXECUTION_STEP_ID,
            EntityType.EXECUTION_STEP),

    EXECUTION_STEP_RESULT(
            "custom-export.column.EXECUTION_STEP.RESULT",
            EXECUTION_STEP.EXPECTED_RESULT,
            EXECUTION_STEP.EXECUTION_STEP_ID,
            EntityType.EXECUTION_STEP),

    EXECUTION_STEP_STATUS(
            I18nKeys.I18N_KEY_STATUS,
            EXECUTION_STEP.EXECUTION_STATUS,
            EXECUTION_STEP.EXECUTION_STEP_ID,
            EntityType.EXECUTION_STEP),

    EXECUTION_STEP_USER(
            I18nKeys.I18N_KEY_USER,
            EXECUTION_STEP.LAST_EXECUTED_BY,
            EXECUTION_STEP.EXECUTION_STEP_ID,
            EntityType.EXECUTION_STEP),

    EXECUTION_STEP_EXECUTION_DATE(
            "iteration.executions.table.column-header.execution-date.label",
            EXECUTION_STEP.LAST_EXECUTED_ON,
            EXECUTION_STEP.EXECUTION_STEP_ID,
            EntityType.EXECUTION_STEP),

    EXECUTION_STEP_COMMENT(
            "executions.steps.table.column-header.comment.label",
            EXECUTION_STEP.COMMENT,
            EXECUTION_STEP.EXECUTION_STEP_ID,
            EntityType.EXECUTION_STEP),

    EXECUTION_STEP_LINKED_REQUIREMENTS_NUMBER(
            I18nKeys.I18N_KEY_LINKED_REQUIREMENTS_NUMBER,
            Fields.EXECUTION_STEP_LINKED_REQUIREMENTS_NUMBER,
            null,
            EntityType.EXECUTION_STEP),

    EXECUTION_STEP_LINKED_REQUIREMENTS_IDS(
            "custom-export.column.EXECUTION_STEP_LINKED_REQUIREMENTS_IDS",
            Fields.EXECUTION_STEP_LINKED_REQUIREMENTS_IDS,
            null,
            EntityType.EXECUTION_STEP),

    EXECUTION_STEP_CUF(null, null, null, EntityType.EXECUTION_STEP),

    TEST_STEP_CUF(null, null, null, EntityType.TEST_STEP),

    // --- ISSUE ---
    ISSUE_EXECUTION_AND_EXECUTION_STEP_ISSUES_NUMBER(
            "custom-export.wizard.attributes.ISSUE.ALL_LINKED_ISSUES_COUNT",
            Fields.ISSUE_EXECUTION_AND_EXECUTION_STEP_ISSUES_NUMBER,
            null,
            EntityType.ISSUE),

    ISSUE_EXECUTION_AND_EXECUTION_STEP_ISSUES_IDS(
            "label.Execution", Fields.EXECUTION_AND_EXECUTION_STEP_ISSUES_KEYS, null, EntityType.ISSUE),

    ISSUE_EXECUTION_STEP_ISSUES_NUMBER(
            "custom-export.wizard.attributes.ISSUE.STEP_LINKED_ISSUES_COUNT",
            Fields.ISSUE_EXECUTION_STEP_ISSUES_NUMBER,
            null,
            EntityType.ISSUE),

    ISSUE_EXECUTION_STEP_ISSUES_IDS(
            "chart.entityType.EXECUTION_STEP",
            Fields.ISSUE_EXECUTION_STEP_ISSUES_IDS,
            null,
            EntityType.ISSUE);

    private static final Map<EntityType, TableField<?, Long>> TREE_SELECTABLE_ENTITY_TYPE_MAP;
    private static final Set<CustomExportColumnLabel> CUSTOMIZABLE_TEXT_FIELDS_SET;
    private static final Map<EntityType, TableField<?, Long>> ENTITY_TYPE_TO_ID_TABLE_FIELD_MAP;
    private static final Set<CustomExportColumnLabel> RICH_TEXT_FIELDS_SET;

    private static final String SUITE_ITPI_DONE = "suite_itpi_done";
    private static final String SUITE_ITPI_TOTAL = "suite_itpi_total";
    private static final String EXEC_STEP_DONE = "exec_step_done";
    private static final String EXEC_STEP_TOTAL = "exec_step_total";

    /** i18n key of the column label. */
    private String i18nKey;

    /** Jooq table field corresponding to this column label. */
    private Field jooqTableField;

    /** Jooq table field of the table primary key column corresponding to this column. */
    private Field jooqTablePkField;

    /** The EntityType corresponding to the column. */
    private EntityType entityType;

    CustomExportColumnLabel(
            String i18nKey, Field jooqTableField, Field jooqTablePkField, EntityType entityType) {
        this.i18nKey = i18nKey;
        this.jooqTableField = jooqTableField;
        this.jooqTablePkField = jooqTablePkField;
        this.entityType = entityType;
    }

    @Override
    public String getI18nKey() {
        return i18nKey;
    }

    public Field getJooqTableField() {
        return jooqTableField;
    }

    public Field getJooqTablePkField() {
        return jooqTablePkField;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public String getShortenedEntityType() {
        return ShortenedNames.getShortenedEntityType(entityType);
    }

    public static Set<CustomExportColumnLabel> getRichTextFieldsSet() {
        return RICH_TEXT_FIELDS_SET;
    }

    public static Map<EntityType, TableField<?, Long>> getEntityTypeToIdTableFieldMap() {
        return ENTITY_TYPE_TO_ID_TABLE_FIELD_MAP;
    }

    public static Map<EntityType, TableField<?, Long>> getSelectableEntityTypeToIdTableFieldMap() {
        return TREE_SELECTABLE_ENTITY_TYPE_MAP;
    }

    public static Set<CustomExportColumnLabel> getCustomizableTextFieldsSet() {
        return CUSTOMIZABLE_TEXT_FIELDS_SET;
    }

    // Initialize unmodifiable CUSTOMIZABLE_TEXT_FIELDS_SET
    static {
        Set<CustomExportColumnLabel> initialSet = new HashSet<>();
        initialSet.addAll(
                Arrays.asList(
                        CAMPAIGN_LABEL,
                        CAMPAIGN_REFERENCE,
                        CAMPAIGN_DESCRIPTION,
                        CAMPAIGN_MILESTONE,
                        CAMPAIGN_CUF,
                        ITERATION_LABEL,
                        ITERATION_REFERENCE,
                        ITERATION_DESCRIPTION,
                        ITERATION_CUF,
                        TEST_SUITE_LABEL,
                        TEST_SUITE_DESCRIPTION,
                        TEST_SUITE_CUF,
                        TEST_CASE_PROJECT,
                        TEST_CASE_MILESTONE,
                        TEST_CASE_LABEL,
                        TEST_CASE_REFERENCE,
                        TEST_CASE_DESCRIPTION,
                        TEST_CASE_NATURE,
                        TEST_CASE_TYPE,
                        TEST_CASE_DATASET,
                        TEST_CASE_PREREQUISITE,
                        TEST_CASE_CUF,
                        EXECUTION_USER,
                        EXECUTION_COMMENT,
                        EXECUTION_CUF,
                        EXECUTION_STEP_ACTION,
                        EXECUTION_STEP_RESULT,
                        EXECUTION_STEP_USER,
                        EXECUTION_STEP_COMMENT,
                        EXECUTION_STEP_CUF,
                        TEST_STEP_CUF));
        CUSTOMIZABLE_TEXT_FIELDS_SET = Collections.unmodifiableSet(initialSet);
    }

    // Initialize unmodifiable ENTITY_TYPE_TO_ID_TABLE_FIELD_MAP
    static {
        Map<EntityType, TableField<?, Long>> initialMap = new HashMap<>(7);
        initialMap.put(EntityType.CAMPAIGN, CAMPAIGN.CLN_ID);
        initialMap.put(EntityType.ITERATION, Tables.ITERATION.ITERATION_ID);
        initialMap.put(EntityType.TEST_SUITE, Tables.TEST_SUITE.ID);
        initialMap.put(EntityType.TEST_CASE, TEST_CASE.TCLN_ID);
        initialMap.put(EntityType.EXECUTION, EXECUTION.EXECUTION_ID);
        initialMap.put(EntityType.EXECUTION_STEP, EXECUTION_STEP.EXECUTION_STEP_ID);
        initialMap.put(EntityType.TEST_STEP, EXECUTION_STEP.EXECUTION_STEP_ID);
        ENTITY_TYPE_TO_ID_TABLE_FIELD_MAP = Collections.unmodifiableMap(initialMap);
    }

    static {
        Map<EntityType, TableField<?, Long>> initialMap = new HashMap<>(4);
        initialMap.put(EntityType.CAMPAIGN_FOLDER, CAMPAIGN_FOLDER.CLN_ID);
        initialMap.put(EntityType.CAMPAIGN, CAMPAIGN.CLN_ID);
        initialMap.put(EntityType.ITERATION, ITERATION.ITERATION_ID);
        initialMap.put(EntityType.TEST_SUITE, TEST_SUITE.ID);
        TREE_SELECTABLE_ENTITY_TYPE_MAP = Collections.unmodifiableMap(initialMap);
    }

    // Initialize unmodifiable RICH_TEXT_FIELDS_SET
    static {
        Set<CustomExportColumnLabel> initialSet = new HashSet<>(9);
        initialSet.add(CAMPAIGN_DESCRIPTION);
        initialSet.add(ITERATION_DESCRIPTION);
        initialSet.add(TEST_SUITE_DESCRIPTION);
        initialSet.add(TEST_CASE_DESCRIPTION);
        initialSet.add(TEST_CASE_PREREQUISITE);
        initialSet.add(EXECUTION_COMMENT);
        initialSet.add(EXECUTION_STEP_COMMENT);
        initialSet.add(EXECUTION_STEP_ACTION);
        initialSet.add(EXECUTION_STEP_RESULT);
        RICH_TEXT_FIELDS_SET = Collections.unmodifiableSet(initialSet);
    }

    private static final class I18nKeys {
        private static final String I18N_KEY_ACTUAL_END = "dialog.label.campaign.actual_end.label";
        private static final String I18N_KEY_ACTUAL_START = "dialog.label.campaign.actual_start.label";
        private static final String I18N_KEY_LINKED_REQUIREMENTS_NUMBER =
                "custom-export.column.LINKED_REQUIREMENTS_COUNT";
        private static final String I18N_KEY_CUSTOM_EXPORT_COLUMN_LINKED_REQUIREMENTS_IDS =
                "custom-export.column.LINKED_REQUIREMENTS_IDS";
        private static final String I18N_KEY_DESCRIPTION = "label.Description";
        private static final String I18N_KEY_ID = "label.id";
        private static final String I18N_KEY_LABEL = "label.Label";
        private static final String I18N_KEY_MILESTONE = "label.Milestone";
        private static final String I18N_KEY_MILESTONES = "label.Milestones";
        private static final String I18N_KEY_PROGRESS_STATUS = "campaign.progress_status.label";
        private static final String I18N_KEY_REFERENCE = "label.Reference";
        private static final String I18N_KEY_SCHEDULED_END =
                "dialog.label.campaign.scheduled_end.label";
        private static final String I18N_KEY_SCHEDULED_START =
                "dialog.label.campaign.scheduled_start.label";
        private static final String I18N_KEY_STATE = "label.State";
        private static final String I18N_KEY_STATUS = "label.Status";
        private static final String I18N_KEY_USER = "label.User";
    }

    private static final class Fields {
        private static final Field FIELD_TEST_SUITE_ITPI_DONE_COUNT =
                DSL.select(count(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID).cast(SQLDataType.DOUBLE))
                        .from(TEST_SUITE.as(SUITE_ITPI_DONE))
                        .leftJoin(TEST_SUITE_TEST_PLAN_ITEM)
                        .on(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.eq(TEST_SUITE.as(SUITE_ITPI_DONE).ID))
                        .leftJoin(ITERATION_TEST_PLAN_ITEM)
                        .on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID))
                        .where(TEST_SUITE.as(SUITE_ITPI_DONE).ID.eq(TEST_SUITE.ID))
                        .and(
                                ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS.in(
                                        "SETTLED", "UNTESTABLE", "BLOCKED", "FAILURE", "SUCCESS"))
                        .asField();

        private static final Field FIELD_TEST_SUITE_ITPI_TOTAL_COUNT =
                DSL.select(count(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID).cast(SQLDataType.DOUBLE))
                        .from(TEST_SUITE.as(SUITE_ITPI_TOTAL))
                        .leftJoin(TEST_SUITE_TEST_PLAN_ITEM)
                        .on(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.eq(TEST_SUITE.as(SUITE_ITPI_TOTAL).ID))
                        .leftJoin(ITERATION_TEST_PLAN_ITEM)
                        .on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID))
                        .where(TEST_SUITE.as(SUITE_ITPI_TOTAL).ID.eq(TEST_SUITE.ID))
                        .asField();
        private static final Field TEST_SUITE_PROGRESS_STATUS =
                concat(
                        DSL.round(
                                        FIELD_TEST_SUITE_ITPI_DONE_COUNT
                                                .div(nullif(FIELD_TEST_SUITE_ITPI_TOTAL_COUNT, 0))
                                                .mul(100L),
                                        2)
                                .cast(SQLDataType.VARCHAR(5)),
                        val(" "),
                        val("%"));

        private static final Field FIELD_EXECUTION_STEP_SUCCESS_COUNT =
                DSL.select(count(EXECUTION_STEP.EXECUTION_STEP_ID).cast(SQLDataType.DOUBLE))
                        .from(EXECUTION.as(EXEC_STEP_DONE))
                        .leftJoin(EXECUTION_EXECUTION_STEPS)
                        .on(
                                EXECUTION_EXECUTION_STEPS.EXECUTION_ID.eq(
                                        EXECUTION.as(EXEC_STEP_DONE).EXECUTION_ID))
                        .leftJoin(EXECUTION_STEP)
                        .on(EXECUTION_STEP.EXECUTION_STEP_ID.eq(EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ID))
                        .where(EXECUTION.as(EXEC_STEP_DONE).EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                        .and(EXECUTION_STEP.EXECUTION_STATUS.eq("SUCCESS"))
                        .asField();

        private static final Field FIELD_EXECUTION_STEP_TOTAL_COUNT =
                DSL.select(count(EXECUTION_STEP.EXECUTION_STEP_ID).cast(SQLDataType.DOUBLE))
                        .from(EXECUTION.as(EXEC_STEP_TOTAL))
                        .leftJoin(EXECUTION_EXECUTION_STEPS)
                        .on(
                                EXECUTION_EXECUTION_STEPS.EXECUTION_ID.eq(
                                        EXECUTION.as(EXEC_STEP_TOTAL).EXECUTION_ID))
                        .leftJoin(EXECUTION_STEP)
                        .on(EXECUTION_STEP.EXECUTION_STEP_ID.eq(EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ID))
                        .where(EXECUTION.as(EXEC_STEP_TOTAL).EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                        .asField();

        private static final Field EXECUTION_SUCCESS_RATE =
                concat(
                        DSL.round(
                                        FIELD_EXECUTION_STEP_SUCCESS_COUNT
                                                .div(nullif(FIELD_EXECUTION_STEP_TOTAL_COUNT, 0))
                                                .mul(100L),
                                        2)
                                .cast(SQLDataType.VARCHAR(5)),
                        val(" "),
                        val("%"));

        private static final Field EXECUTION_AND_EXECUTION_STEP_ISSUES_KEYS =
                DSL.select(
                                groupConcatDistinct(ISSUE.REMOTE_ISSUE_ID)
                                        .separator(", ")
                                        .as("exec_and_es_issue_ids"))
                        .from(ISSUE)
                        .innerJoin(EXECUTION_ISSUES_CLOSURE)
                        .on(EXECUTION_ISSUES_CLOSURE.ISSUE_ID.eq(ISSUE.ISSUE_ID))
                        .where(EXECUTION_ISSUES_CLOSURE.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                        .asField();

        private static final Field ISSUE_EXECUTION_AND_EXECUTION_STEP_ISSUES_NUMBER =
                select(countDistinct(EXECUTION_ISSUES_CLOSURE.ISSUE_ID))
                        .from(EXECUTION_ISSUES_CLOSURE)
                        .where(EXECUTION_ISSUES_CLOSURE.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                        .asField("exec_and_es_issue_number");

        private static final Field ISSUE_EXECUTION_STEP_ISSUES_NUMBER =
                select(countDistinct(ISSUE.ISSUE_ID))
                        .from(ISSUE)
                        .where(ISSUE.ISSUE_LIST_ID.eq(EXECUTION_STEP.ISSUE_LIST_ID))
                        .asField("es_issue_number");

        private static final Field ISSUE_EXECUTION_STEP_ISSUES_IDS =
                select(groupConcatDistinct(ISSUE.REMOTE_ISSUE_ID).separator(", "))
                        .from(ISSUE)
                        .where(ISSUE.ISSUE_LIST_ID.eq(EXECUTION_STEP.ISSUE_LIST_ID))
                        .asField("es_issue_ids");

        private static final Field EXECUTION_STEP_LINKED_REQUIREMENTS_NUMBER =
                select(countDistinct(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID))
                        .from(REQUIREMENT_VERSION_COVERAGE)
                        .innerJoin(VERIFYING_STEPS)
                        .on(
                                REQUIREMENT_VERSION_COVERAGE.REQUIREMENT_VERSION_COVERAGE_ID.eq(
                                        VERIFYING_STEPS.REQUIREMENT_VERSION_COVERAGE_ID))
                        .where(VERIFYING_STEPS.TEST_STEP_ID.eq(EXECUTION_STEP.TEST_STEP_ID))
                        .asField("es_rvc_number");

        private static final Field EXECUTION_STEP_LINKED_REQUIREMENTS_IDS =
                select(
                                groupConcatDistinct(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID)
                                        .separator(", "))
                        .from(REQUIREMENT_VERSION_COVERAGE)
                        .innerJoin(VERIFYING_STEPS)
                        .on(
                                REQUIREMENT_VERSION_COVERAGE.REQUIREMENT_VERSION_COVERAGE_ID.eq(
                                        VERIFYING_STEPS.REQUIREMENT_VERSION_COVERAGE_ID))
                        .where(VERIFYING_STEPS.TEST_STEP_ID.eq(EXECUTION_STEP.TEST_STEP_ID))
                        .asField("es_rvc_ids");

        private static final Field TEST_CASE_LINKED_REQUIREMENTS_NUMBER =
                select(countDistinct(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID))
                        .from(REQUIREMENT_VERSION_COVERAGE)
                        .where(
                                REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID.eq(
                                        ITERATION_TEST_PLAN_ITEM.TCLN_ID))
                        .asField("tc_rvc_number");

        private static final Field TEST_CASE_LINKED_REQUIREMENTS_IDS =
                select(
                                groupConcatDistinct(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID)
                                        .separator(", "))
                        .from(REQUIREMENT_VERSION_COVERAGE)
                        .where(
                                REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID.eq(
                                        ITERATION_TEST_PLAN_ITEM.TCLN_ID))
                        .asField("tc_rvc_ids");

        private static final Field TEST_CASE_MILESTONE =
                select(groupConcatDistinct(MILESTONE.LABEL).separator(", "))
                        .from(MILESTONE)
                        .innerJoin(MILESTONE_TEST_CASE)
                        .on(MILESTONE.MILESTONE_ID.eq(MILESTONE_TEST_CASE.MILESTONE_ID))
                        .where(MILESTONE_TEST_CASE.TEST_CASE_ID.eq(ITERATION_TEST_PLAN_ITEM.TCLN_ID))
                        .asField("tc_milestone_labels");
    }

    private static final class ShortenedNames {

        private static final Map<EntityType, String> ENTITY_TYPE_TO_SHORTEN_ENTITY_NAME_MAP;

        // Initialize unmodifiable ENTITY_TYPE_TO_SHORTEN_ENTITY_NAME_MAP
        static {
            Map<EntityType, String> initialMap = new HashMap<>(7);
            initialMap.put(EntityType.CAMPAIGN, "CPG");
            initialMap.put(EntityType.ITERATION, "IT");
            initialMap.put(EntityType.TEST_SUITE, "SUI");
            initialMap.put(EntityType.TEST_CASE, "TC");
            initialMap.put(EntityType.EXECUTION, "EXEC");
            initialMap.put(EntityType.EXECUTION_STEP, "EXEC_STEP");
            initialMap.put(EntityType.ISSUE, "BUG");
            initialMap.put(EntityType.TEST_STEP, "TEST_STEP");
            ENTITY_TYPE_TO_SHORTEN_ENTITY_NAME_MAP = Collections.unmodifiableMap(initialMap);
        }

        private static String getShortenedEntityType(EntityType entityType) {
            return ENTITY_TYPE_TO_SHORTEN_ENTITY_NAME_MAP.get(entityType);
        }
    }
}
