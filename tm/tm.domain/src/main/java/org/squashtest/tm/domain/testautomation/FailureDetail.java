/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.testautomation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.domain.bugtracker.IssueDetector;
import org.squashtest.tm.domain.bugtracker.IssueList;
import org.squashtest.tm.domain.campaign.CampaignLibrary;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.security.annotation.AclConstrainedObject;

@Entity
public class FailureDetail implements Identified, IssueDetector {

    @Id
    @Column(name = "FAILURE_DETAIL_ID")
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "failure_detail_failure_detail_id_seq")
    @SequenceGenerator(
            name = "failure_detail_failure_detail_id_seq",
            sequenceName = "failure_detail_failure_detail_id_seq",
            allocationSize = 1)
    private Long id;

    @Column(name = "MESSAGE")
    private String message;

    @JoinColumn(name = "CREATED_BY")
    private String createdBy;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ITEM_TEST_PLAN_ID", nullable = false)
    private IterationTestPlanItem itpi;

    @ManyToMany(cascade = {CascadeType.PERSIST})
    @JoinTable(
            name = "AUTOMATED_EXECUTION_FAILURE_DETAIL",
            inverseJoinColumns =
                    @JoinColumn(name = "EXECUTION_EXTENDER_ID", referencedColumnName = "EXTENDER_ID"),
            joinColumns =
                    @JoinColumn(name = "FAILURE_DETAIL_ID", referencedColumnName = "FAILURE_DETAIL_ID"))
    private List<AutomatedExecutionExtender> automatedExecutions = new ArrayList<>();

    @OneToOne(
            cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REMOVE})
    @JoinColumn(name = "ISSUE_LIST_ID")
    private IssueList issueList = new IssueList();

    public FailureDetail() {}

    public FailureDetail(
            String message, String createdBy, Date createdOn, IterationTestPlanItem itpi) {
        this.message = message;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.itpi = itpi;
    }

    @Override
    public Long getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public IterationTestPlanItem getItpi() {
        return itpi;
    }

    public List<AutomatedExecutionExtender> getAutomatedExecutions() {
        return automatedExecutions;
    }

    public void addExecutionExtender(AutomatedExecutionExtender extender) {
        if (!this.automatedExecutions.contains(extender)) {
            this.automatedExecutions.add(extender);
        }
    }

    public List<Execution> getExecutions() {
        return this.getAutomatedExecutions().stream()
                .map(AutomatedExecutionExtender::getExecution)
                .toList();
    }

    @Override
    public IssueList getIssueList() {
        return issueList;
    }

    @Override
    public Project getProject() {
        return itpi.getProject();
    }

    @Override
    public BugTracker getBugTracker() {
        return itpi.getProject().getBugTracker();
    }

    @Override
    public Long getIssueListId() {
        return issueList.getId();
    }

    @Override
    public List<Long> getAllIssueListId() {
        return Collections.singletonList(getIssueListId());
    }

    @Override
    public TestCase getReferencedTestCase() {
        return itpi.getReferencedTestCase();
    }

    @Override
    public void detachIssue(Long id) {
        issueList.removeIssue(id);
    }

    @AclConstrainedObject
    public CampaignLibrary getCampaignLibrary() {
        return itpi.getCampaignLibrary();
    }
}
