/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.squashtest.tm.core.foundation.lang.Assert;

public class NodeReference {
    /** Type of the referenced node. */
    private NodeType nodeType;

    /** identifier of the referenced entity. */
    private Long id;

    public NodeReference(NodeType nodeType, Long entityId) {
        super();
        this.nodeType = nodeType;
        this.id = entityId;
        Assert.parameterNotNull(nodeType, "nodeType");
        Assert.parameterNotNull(entityId, "entityId");
    }

    /**
     * @return the type
     */
    public NodeType getNodeType() {
        return nodeType;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "[" + nodeType.toString() + ":" + id + "]";
    }

    public String toNodeId() {
        return nodeType.getTypeName() + "-" + id;
    }

    public static List<String> toNodeIds(Collection<NodeReference> nodeReferences) {
        return nodeReferences.stream().map(NodeReference::toNodeId).toList();
    }

    public static Set<NodeReference> fromNodeIds(Collection<String> ids) {
        return ids.stream().map(NodeReference::fromNodeId).collect(Collectors.toSet());
    }

    public static NodeReference fromNodeId(String asString) throws IllegalArgumentException {
        if (StringUtils.isBlank(asString)) {
            throw new IllegalArgumentException("Entity reference id as String should not be blank");
        }

        Pattern p = Pattern.compile("(.+)-(\\d+)");
        Matcher m = p.matcher(asString);

        if (m.matches()) {
            String type = m.group(1).replace("-", "");
            Long id = Long.valueOf(m.group(2));

            NodeType etype = NodeType.fromTypeName(type);
            return new NodeReference(etype, id);
        } else {
            throw new IllegalArgumentException(
                    "The string "
                            + asString
                            + " has not the correct format. Provide something like NodeType-NodeId. ex: TestCaseLibrary-1");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        NodeReference that = (NodeReference) o;
        return nodeType == that.nodeType && id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nodeType, id);
    }

    public EntityReference toEntityReference() {
        return new EntityReference(nodeType.toEntityType(), this.id);
    }
}
