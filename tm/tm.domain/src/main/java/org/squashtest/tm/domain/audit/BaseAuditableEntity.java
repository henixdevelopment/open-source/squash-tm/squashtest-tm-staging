/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.audit;

import java.util.Date;
import javax.persistence.Embedded;
import javax.persistence.MappedSuperclass;
import org.squashtest.tm.security.UserContextHolder;

/**
 * Base class for auditable Entity Before v4.0.0 we had an aspect that added a mixin automagically
 * However, upgrading Java and AJC made the Aspect crash because AJC thinks that our generics are
 * incorrect. Even if javac works like a charm, and runtime testing on java11 also works. So maybe
 * our code is legal and AJC compiler bugged... I don't know...
 *
 * <p>I decided to adopt a KISS approach and removed the Aspect responsible for class modifications
 * because : - In Domain objects I prefer explicit approach rather than implicit, even with a few
 * code duplication. - The Mixin require some brutal cast to work. Casting to a class that is not
 * part of hierarchy is just weird... - The cost of maintaining this weaved aspect on complex
 * generics is far too high for avoiding just a bunch of getter/setter - The duplication of
 * getter/setter code can be mitigated by using inheritance and delegation
 */
@MappedSuperclass
public class BaseAuditableEntity implements AuditableMixin {

    @Embedded private AuditableSupport audit = new AuditableSupport();

    public BaseAuditableEntity() {
        super();
    }

    public void updateLastModificationWithCurrentUser() {
        setLastModifiedOn(new Date());
        setLastModifiedBy(UserContextHolder.getUsername());
    }

    @Override
    public Date getCreatedOn() {
        return audit.getCreatedOn();
    }

    @Override
    public String getCreatedBy() {
        return audit.getCreatedBy();
    }

    @Override
    public Date getLastModifiedOn() {
        return audit.getLastModifiedOn();
    }

    @Override
    public String getLastModifiedBy() {
        return audit.getLastModifiedBy();
    }

    @Override
    public boolean isSkipModifyAudit() {
        return audit.isSkipModifyAudit();
    }

    @Override
    public void setCreatedBy(String createdBy) {
        audit.setCreatedBy(createdBy);
    }

    @Override
    public void setCreatedOn(Date createdOn) {
        audit.setCreatedOn(createdOn);
    }

    @Override
    public void setLastModifiedBy(String lastModifiedBy) {
        audit.setLastModifiedBy(lastModifiedBy);
    }

    @Override
    public void setLastModifiedOn(Date lastModifiedOn) {
        audit.setLastModifiedOn(lastModifiedOn);
    }

    @Override
    public void setSkipModifyAudit(boolean skipModifyAudit) {
        audit.setSkipModifyAudit(skipModifyAudit);
    }
}
