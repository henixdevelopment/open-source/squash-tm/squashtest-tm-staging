/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.gridconfiguration;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.domain.users.User;

@Entity
@Table(name = "GRID_COLUMN_DISPLAY_REFERENCE")
public class GridColumnDisplayReference implements Identified {
    @Id
    @Column(name = "GCDR_ID")
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "grid_column_display_reference_gcdr_id_seq")
    @SequenceGenerator(
            name = "grid_column_display_reference_gcdr_id_seq",
            sequenceName = "grid_column_display_reference_gcdr_id_seq",
            allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "PARTY_ID", nullable = false)
    private User user;

    @NotNull
    @Column(name = "GRID_ID")
    private String gridId;

    @Column(name = "PROJECT_ID")
    private Long projectId;

    @OneToMany(mappedBy = "gridColumnDisplayReference", cascade = CascadeType.REMOVE)
    private List<GridColumnDisplayConfiguration> configurations;

    @Override
    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setGridId(String gridId) {
        this.gridId = gridId;
    }

    public String getGridId() {
        return gridId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public List<GridColumnDisplayConfiguration> getConfigurations() {
        return configurations;
    }

    public void setConfigurations(List<GridColumnDisplayConfiguration> configurations) {
        this.configurations = configurations;
    }
}
