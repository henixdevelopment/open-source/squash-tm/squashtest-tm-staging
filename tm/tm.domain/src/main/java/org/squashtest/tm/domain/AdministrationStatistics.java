/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain;

import java.math.BigInteger;

/**
 * Bean that stores the number of some entities in the database. <br>
 * Contains the following properties :
 *
 * <ul>
 *   <li>projectsNumber
 *   <li>usersNumber
 *   <li>requirementsNumber
 *   <li>testCasesNumber
 *   <li>campaignsNumber
 *   <li>iterationsNumber
 *   <li>executionsNumber
 * </ul>
 *
 * @author mpagnon
 *     <p>*
 */
public class AdministrationStatistics {
    private long projectsNumber;
    private long usersNumber;
    private long requirementsNumber;
    private long testCasesNumber;
    private long campaignsNumber;
    private long iterationsNumber;
    private long executionsNumber;
    private BigInteger databaseSize;

    /**
     * Will read the <code>Object[]</code> param and fill it's properties with the following
     * "index/property" mapping :
     *
     * <ul>
     *   <li>0/projectsNumber
     *   <li>1/usersNumber
     *   <li>2/requirementsNumber
     *   <li>3/testCasesNumber
     *   <li>4/campaignsNumber
     *   <li>5/iterationsNumber
     *   <li>6/executionsNumber
     * </ul>
     *
     * @param result
     */
    public AdministrationStatistics(Object[] resultParam, BigInteger databaseSize) {
        Object[] result = resultParam.clone();
        this.projectsNumber = (Long) result[0];
        this.usersNumber = (Long) result[1];
        this.requirementsNumber = (Long) result[2];
        this.testCasesNumber = (Long) result[3];
        this.campaignsNumber = (Long) result[4];
        this.iterationsNumber = (Long) result[5];
        this.executionsNumber = (Long) result[6];
        this.databaseSize = databaseSize;
    }

    public long getProjectsNumber() {
        return projectsNumber;
    }

    public void setProjectsNumber(long projectsNumber) {
        this.projectsNumber = projectsNumber;
    }

    public long getUsersNumber() {
        return usersNumber;
    }

    public void setUsersNumber(long usersNumber) {
        this.usersNumber = usersNumber;
    }

    public long getRequirementsNumber() {
        return requirementsNumber;
    }

    public void setRequirementsNumber(long requirementsNumber) {
        this.requirementsNumber = requirementsNumber;
    }

    public long getTestCasesNumber() {
        return testCasesNumber;
    }

    public void setTestCasesNumber(long testCasesNumber) {
        this.testCasesNumber = testCasesNumber;
    }

    public long getCampaignsNumber() {
        return campaignsNumber;
    }

    public void setCampaignsNumber(long campaignsNumber) {
        this.campaignsNumber = campaignsNumber;
    }

    public long getIterationsNumber() {
        return iterationsNumber;
    }

    public void setIterationsNumber(long iterationsNumber) {
        this.iterationsNumber = iterationsNumber;
    }

    public long getExecutionsNumber() {
        return executionsNumber;
    }

    public void setExecutionsNumber(long executionsNumber) {
        this.executionsNumber = executionsNumber;
    }

    public BigInteger getDatabaseSize() {
        return databaseSize;
    }

    public void setDatabaseSize(BigInteger databaseSize) {
        this.databaseSize = databaseSize;
    }
}
