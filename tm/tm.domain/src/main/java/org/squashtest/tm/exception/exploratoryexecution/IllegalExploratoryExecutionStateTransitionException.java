/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.exception.exploratoryexecution;

import org.squashtest.tm.core.foundation.exception.ActionException;
import org.squashtest.tm.domain.execution.ExploratoryExecutionEventType;
import org.squashtest.tm.domain.execution.ExploratoryExecutionRunningState;

public class IllegalExploratoryExecutionStateTransitionException extends ActionException {
    public final ExploratoryExecutionRunningState currentState;
    public final ExploratoryExecutionEventType eventType;

    public IllegalExploratoryExecutionStateTransitionException(
            ExploratoryExecutionRunningState currentState, ExploratoryExecutionEventType eventType) {
        super(
                String.format(
                        "Current state %s is incompatible with received event %s.",
                        currentState.name(), eventType.name()));
        this.currentState = currentState;
        this.eventType = eventType;
    }

    public static class AlreadyStopped extends IllegalExploratoryExecutionStateTransitionException {
        public AlreadyStopped() {
            super(ExploratoryExecutionRunningState.STOPPED, ExploratoryExecutionEventType.STOP);
        }

        @Override
        public String getI18nKey() {
            return "sqtm-core.campaign-workspace.exploratory-execution.running-error.already-stopped";
        }
    }

    public static class AlreadyPaused extends IllegalExploratoryExecutionStateTransitionException {
        public AlreadyPaused() {
            super(ExploratoryExecutionRunningState.PAUSED, ExploratoryExecutionEventType.PAUSE);
        }

        @Override
        public String getI18nKey() {
            return "sqtm-core.campaign-workspace.exploratory-execution.running-error.already-paused";
        }
    }

    public static class AlreadyRunning extends IllegalExploratoryExecutionStateTransitionException {
        public AlreadyRunning(ExploratoryExecutionEventType eventType) {
            super(ExploratoryExecutionRunningState.RUNNING, eventType);
        }

        @Override
        public String getI18nKey() {
            return "sqtm-core.campaign-workspace.exploratory-execution.running-error.already-running";
        }
    }
}
