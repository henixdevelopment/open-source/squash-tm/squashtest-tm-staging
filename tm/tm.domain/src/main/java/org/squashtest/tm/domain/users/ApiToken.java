/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.users;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.domain.Sizes;

@Entity
@Table(name = "API_TOKEN")
public class ApiToken implements Identified {

    @Id
    @Column(name = "TOKEN_ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "api_token_token_id_seq")
    @SequenceGenerator(
            name = "api_token_token_id_seq",
            sequenceName = "api_token_token_id_seq",
            allocationSize = 1)
    private Long id;

    @NotNull
    @Pattern(regexp = "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}")
    private String uuid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @NotNull private String name;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @NotNull
    @Size(max = Sizes.CREATED_BY_MAX)
    private String createdBy;

    @NotNull
    @Pattern(
            regexp = "\\d{4}-\\d{2}-\\d{2}",
            message = "Expiry date must be in the format yyyy-mm-dd")
    private String expiryDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUsage;

    @NotNull private String permissions;

    public ApiToken() {}

    public ApiToken(
            String uuid,
            User user,
            String name,
            Date createdOn,
            String createdBy,
            String expiryDate,
            String permissions) {
        this.uuid = uuid;
        this.user = user;
        this.name = name;
        this.createdOn = createdOn;
        this.createdBy = createdBy;
        this.expiryDate = expiryDate;
        this.permissions = permissions;
    }

    public String getUuid() {
        return uuid;
    }

    public User getUser() {
        return user;
    }

    public String getName() {
        return name;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public Date getLastUsage() {
        return lastUsage;
    }

    public void setLastUsage(Date lastUsage) {
        this.lastUsage = lastUsage;
    }

    public String getPermissions() {
        return permissions;
    }

    @Override
    public Long getId() {
        return id;
    }
}
