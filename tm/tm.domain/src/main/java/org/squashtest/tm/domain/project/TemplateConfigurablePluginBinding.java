/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.project;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.squashtest.tm.api.template.TemplateConfigurablePlugin;

@Entity
@Table(name = "TEMPLATE_CONFIGURABLE_PLUGIN_BINDING")
public class TemplateConfigurablePluginBinding {

    @Id
    @Column(name = "BINDING_ID")
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "template_configurable_plugin_binding_binding_id_seq")
    @SequenceGenerator(
            name = "template_configurable_plugin_binding_binding_id_seq",
            sequenceName = "template_configurable_plugin_binding_binding_id_seq",
            allocationSize = 1)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROJECT_TEMPLATE_ID", referencedColumnName = "PROJECT_ID")
    private ProjectTemplate projectTemplate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROJECT_ID", referencedColumnName = "PROJECT_ID")
    private Project project;

    @Column private String pluginId;

    /**
     * Empty constructor is only made public for reflection concerns (JPA). Devs should rather use the
     * factory method {@link TemplateConfigurablePluginBinding#createBinding(ProjectTemplate, Project,
     * TemplateConfigurablePlugin)}
     */
    public TemplateConfigurablePluginBinding() {}

    private TemplateConfigurablePluginBinding(
            ProjectTemplate template, Project project, String pluginId) {
        this();
        this.projectTemplate = template;
        this.project = project;
        this.pluginId = pluginId;
    }

    public Long getId() {
        return id;
    }

    void setId(Long id) {
        this.id = id;
    }

    public ProjectTemplate getProjectTemplate() {
        return projectTemplate;
    }

    public Project getProject() {
        return project;
    }

    void setProject(Project attachedProject) {
        this.project = attachedProject;
    }

    public String getPluginId() {
        return pluginId;
    }

    void setPluginId(String pluginId) {
        this.pluginId = pluginId;
    }

    /**
     * Factory for a new TemplateConfigurablePluginBinding.
     *
     * @param template source template
     * @param project target project
     * @param plugin template configurable plugin
     * @return a new binding
     */
    public static TemplateConfigurablePluginBinding createBinding(
            ProjectTemplate template, Project project, TemplateConfigurablePlugin plugin) {
        if (template == null || project == null || plugin == null) {
            throw new IllegalArgumentException();
        }

        return new TemplateConfigurablePluginBinding(template, project, plugin.getId());
    }
}
