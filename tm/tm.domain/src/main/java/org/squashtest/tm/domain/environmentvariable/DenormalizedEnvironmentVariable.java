/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.environmentvariable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.squashtest.tm.domain.Sizes;
import org.squashtest.tm.domain.environmenttag.DenormalizedEnvironmentHolderType;

@Entity
public class DenormalizedEnvironmentVariable {

    @Id
    @Column(name = "DEV_ID")
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "denormalized_environment_variable_dev_id_seq")
    @SequenceGenerator(
            name = "denormalized_environment_variable_dev_id_seq",
            sequenceName = "denormalized_environment_variable_dev_id_seq",
            allocationSize = 1)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EV_ID", nullable = true)
    private EnvironmentVariable environmentVariable;

    @NotNull
    @Column(name = "HOLDER_ID")
    private Long holderId;

    @Column(name = "HOLDER_TYPE")
    @Enumerated(EnumType.STRING)
    protected DenormalizedEnvironmentHolderType holderType;

    @NotBlank
    @Size(min = Sizes.NAME_MIN, max = Sizes.NAME_MAX)
    private String name;

    @Size(max = Sizes.VALUE_MAX)
    private String value;

    private String type;

    public DenormalizedEnvironmentVariable() {}

    public DenormalizedEnvironmentVariable(
            EnvironmentVariable environmentVariable,
            Long holderId,
            DenormalizedEnvironmentHolderType holderType,
            String name,
            String value,
            String type) {
        this.environmentVariable = environmentVariable;
        this.holderId = holderId;
        this.holderType = holderType;
        this.name = name;
        this.value = value;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public EnvironmentVariable getEnvironmentVariable() {
        return environmentVariable;
    }

    public void setEnvironmentVariable(EnvironmentVariable environmentVariable) {
        this.environmentVariable = environmentVariable;
    }

    public Long getHolderId() {
        return holderId;
    }

    public void setHolderId(Long holderId) {
        this.holderId = holderId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public DenormalizedEnvironmentHolderType getHolderType() {
        return holderType;
    }

    public void setHolderType(DenormalizedEnvironmentHolderType holderType) {
        this.holderType = holderType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
