/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain;

import static org.squashtest.tm.domain.NodeType.ACTION_WORD_LIBRARY;
import static org.squashtest.tm.domain.NodeType.CAMPAIGN_LIBRARY;
import static org.squashtest.tm.domain.NodeType.CUSTOM_REPORT_LIBRARY;
import static org.squashtest.tm.domain.NodeType.REQUIREMENT_LIBRARY;
import static org.squashtest.tm.domain.NodeType.TEST_CASE_LIBRARY;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;

import org.jooq.TableField;
import org.squashtest.tm.domain.actionword.ActionWordLibrary;
import org.squashtest.tm.domain.campaign.CampaignLibrary;
import org.squashtest.tm.domain.customreport.CustomReportLibrary;
import org.squashtest.tm.domain.requirement.RequirementLibrary;
import org.squashtest.tm.domain.testcase.TestCaseLibrary;
import org.squashtest.tm.jooq.domain.tables.records.ProjectRecord;

public enum NodeWorkspace {
    REQUIREMENT(PROJECT.RL_ID, REQUIREMENT_LIBRARY, RequirementLibrary.class.getName()),
    TEST_CASE(PROJECT.TCL_ID, TEST_CASE_LIBRARY, TestCaseLibrary.class.getName()),
    CAMPAIGN(PROJECT.CL_ID, CAMPAIGN_LIBRARY, CampaignLibrary.class.getName()),
    CUSTOM_REPORT(PROJECT.CRL_ID, CUSTOM_REPORT_LIBRARY, CustomReportLibrary.class.getName()),
    ACTION_WORD(PROJECT.AWL_ID, ACTION_WORD_LIBRARY, ActionWordLibrary.class.getName());

    private final TableField<ProjectRecord, Long> columnRef;
    private final NodeType libraryType;
    private final String libraryClassName;

    NodeWorkspace(
            TableField<ProjectRecord, Long> columnRef, NodeType libraryType, String libraryClassName) {
        this.columnRef = columnRef;
        this.libraryType = libraryType;
        this.libraryClassName = libraryClassName;
    }

    public TableField<ProjectRecord, Long> getColumnRef() {
        return columnRef;
    }

    public NodeType getLibraryType() {
        return libraryType;
    }

    public String getLibraryClassName() {
        return libraryClassName;
    }
}
