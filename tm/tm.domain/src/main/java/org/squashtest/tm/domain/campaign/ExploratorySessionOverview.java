/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.campaign;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Type;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.domain.Sizes;
import org.squashtest.tm.domain.attachment.Attachment;
import org.squashtest.tm.domain.attachment.AttachmentHolder;
import org.squashtest.tm.domain.attachment.AttachmentList;
import org.squashtest.tm.domain.campaign.testplan.TestPlanItem;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.testcase.ExploratoryTestCase;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.security.annotation.AclConstrainedObject;

@Entity
public class ExploratorySessionOverview implements Identified, AttachmentHolder {

    @Id
    @Column(name = "OVERVIEW_ID")
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "exploratory_session_overview_overview_id_seq")
    @SequenceGenerator(
            name = "exploratory_session_overview_overview_id_seq",
            sequenceName = "exploratory_session_overview_overview_id_seq",
            allocationSize = 1)
    private Long id;

    // Used in the context of iterations. Mutually exclusive with testPlanItem
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ITEM_TEST_PLAN_ID", referencedColumnName = "ITEM_TEST_PLAN_ID")
    private IterationTestPlanItem iterationTestPlanItem;

    // Used in the context of sprints. Mutually exclusive with iterationTestPlanItem
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TEST_PLAN_ITEM_ID", referencedColumnName = "TEST_PLAN_ITEM_ID")
    private TestPlanItem testPlanItem;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String charter;

    @Column private Integer sessionDuration;

    @NotBlank
    @Size(max = Sizes.LABEL_MAX)
    private String name;

    @NotNull
    @Size(max = Sizes.REFERENCE_MAX)
    private String reference = "";

    @Temporal(TemporalType.TIMESTAMP)
    private Date dueDate;

    @NotNull
    @Size(max = Sizes.STATUS_MAX)
    private String sessionStatus = "TO_DO";

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String comments;

    @OneToOne(
            cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REMOVE},
            fetch = FetchType.LAZY)
    @JoinColumn(name = "ATTACHMENT_LIST_ID", updatable = false)
    private final AttachmentList attachmentList = new AttachmentList();

    public ExploratorySessionOverview() {
        super();
    }

    public ExploratorySessionOverview(
            ExploratoryTestCase testCase, IterationTestPlanItem iterationTestPlanItem) {
        charter = testCase.getCharter();
        sessionDuration = testCase.getSessionDuration();
        name = testCase.getName();
        reference = testCase.getReference();

        setIterationTestPlanItem(iterationTestPlanItem);
        iterationTestPlanItem.setExploratorySessionOverview(this);
    }

    public ExploratorySessionOverview(ExploratoryTestCase testCase, TestPlanItem testPlanItem) {
        charter = testCase.getCharter();
        sessionDuration = testCase.getSessionDuration();
        name = testCase.getName();
        reference = testCase.getReference();

        setTestPlanItem(testPlanItem);
        testPlanItem.setExploratorySessionOverview(this);
    }

    @Override
    public Long getId() {
        return id;
    }

    public IterationTestPlanItem getIterationTestPlanItem() {
        return iterationTestPlanItem;
    }

    public void setIterationTestPlanItem(IterationTestPlanItem iterationTestPlanItem) {
        this.iterationTestPlanItem = iterationTestPlanItem;
    }

    public TestPlanItem getTestPlanItem() {
        return testPlanItem;
    }

    public void setTestPlanItem(TestPlanItem testPlanItem) {
        this.testPlanItem = testPlanItem;
    }

    public String getCharter() {
        return charter;
    }

    public void setCharter(String charter) {
        this.charter = charter;
    }

    public Integer getSessionDuration() {
        return sessionDuration;
    }

    public void updateSessionDuration(Integer sessionDuration) {
        if (sessionDuration == 0) {
            sessionDuration = null;
        }

        this.sessionDuration = sessionDuration;
        updateOwnerLastModification();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void updateDueDate(Date dueDate) {
        this.dueDate = dueDate;
        updateOwnerLastModification();
    }

    public String getSessionStatus() {
        return sessionStatus;
    }

    public void startSession(User user) {
        updateSessionStatus(ExploratorySessionOverviewStatus.RUNNING);

        if (iterationTestPlanItem != null) {
            iterationTestPlanItem.setExecutionStatus(ExecutionStatus.RUNNING);
        } else {
            testPlanItem.applyFastPass(ExecutionStatus.RUNNING, user);
        }
    }

    public void endSession() {
        updateSessionStatus(ExploratorySessionOverviewStatus.FINISHED);
    }

    private void updateSessionStatus(ExploratorySessionOverviewStatus sessionStatus) {
        this.sessionStatus = sessionStatus.name();
        updateOwnerLastModification();
    }

    public String getComments() {
        return comments;
    }

    public void updateComments(String comments) {
        this.comments = comments;
        updateOwnerLastModification();
    }

    @Override
    public AttachmentList getAttachmentList() {
        return attachmentList;
    }

    @Override
    public Long getAttachmentHolderProjectId() {
        return iterationTestPlanItem.getProject().getId();
    }

    public ExploratorySessionOverview createCopy() {
        ExploratorySessionOverview copy = new ExploratorySessionOverview();
        copy.setCharter(charter);
        copy.sessionDuration = sessionDuration;
        copy.setName(name);
        copy.setReference(reference);
        copy.dueDate = dueDate;
        copy.sessionStatus = sessionStatus;
        copy.comments = comments;

        for (Attachment tcAttach : this.getAttachmentList().getAllAttachments()) {
            Attachment clone = tcAttach.shallowCopy();
            copy.getAttachmentList().addAttachment(clone);
        }

        return copy;
    }

    @Override
    public EntityReference toEntityReference() {
        return new EntityReference(EntityType.EXPLORATORY_SESSION_OVERVIEW, this.id);
    }

    @AclConstrainedObject
    public CampaignLibrary getCampaignLibrary() {
        if (iterationTestPlanItem != null) {
            return iterationTestPlanItem.getCampaignLibrary();
        } else {
            return testPlanItem.getCampaignLibrary();
        }
    }

    private void updateOwnerLastModification() {
        if (iterationTestPlanItem != null) {
            iterationTestPlanItem.updateLastModificationWithCurrentUser();
        } else {
            testPlanItem.updateLastModificationWithCurrentUser();
        }
    }
}
