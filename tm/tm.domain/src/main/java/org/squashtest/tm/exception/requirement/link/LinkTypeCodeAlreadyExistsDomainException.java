/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.exception.requirement.link;

import org.squashtest.tm.exception.DomainException;

public class LinkTypeCodeAlreadyExistsDomainException extends DomainException {
    private final String code;
    private final String messageKey;

    public LinkTypeCodeAlreadyExistsDomainException(
            String code, String fieldName, String messageKey) {
        super("The code: " + code + " is already used by an existing Link Type.", fieldName);
        this.code = code;
        this.messageKey = messageKey;
    }

    @Override
    public String getI18nKey() {
        return messageKey;
    }

    public String getCode() {
        return code;
    }

    @Override
    public Object[] getI18nParams() {
        return new Object[] {code};
    }
}
