/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.execution;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.squashtest.tm.domain.Sizes;
import org.squashtest.tm.domain.common.ordered.OrderedListHelper;
import org.squashtest.tm.domain.testcase.ExploratoryTestCase;
import org.squashtest.tm.domain.testcase.TestCaseExecutionMode;
import org.squashtest.tm.domain.users.User;

@Entity
@PrimaryKeyJoinColumn(name = "EXECUTION_ID")
public class ExploratoryExecution extends Execution {

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ASSIGNEE_ID")
    private User assigneeUser;

    @NotNull @Column private boolean reviewed = false;

    @Column
    @Size(max = Sizes.TASK_DIVISION_MAX)
    private String taskDivision;

    @NotNull
    @OneToMany(
            mappedBy = "execution",
            cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @OrderBy("noteOrder")
    private List<SessionNote> sessionNotes = new ArrayList<>();

    @NotNull
    @OneToMany(
            mappedBy = "exploratoryExecution",
            fetch = FetchType.LAZY,
            cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private List<ExploratoryExecutionEvent> events = new ArrayList<>();

    public ExploratoryExecution() {
        executionMode = TestCaseExecutionMode.EXPLORATORY;
    }

    public ExploratoryExecution(ExploratoryTestCase exploratoryTestCase) {
        super(exploratoryTestCase);
        executionMode = TestCaseExecutionMode.EXPLORATORY;
    }

    @Override
    public void accept(ExecutionVisitor executionVisitor) {
        executionVisitor.visit(this);
    }

    public User getAssigneeUser() {
        return assigneeUser;
    }

    public void setAssigneeUser(User assigneeUser) {
        this.assigneeUser = assigneeUser;
    }

    public boolean isReviewed() {
        return reviewed;
    }

    public void setReviewed(boolean reviewed) {
        this.reviewed = reviewed;
    }

    public String getTaskDivision() {
        return taskDivision;
    }

    public void setTaskDivision(String taskDivision) {
        this.taskDivision = taskDivision;
    }

    public List<SessionNote> getSessionNotes() {
        return sessionNotes;
    }

    public void setSessionNotes(List<SessionNote> sessionNotes) {
        this.sessionNotes = sessionNotes;
    }

    public List<ExploratoryExecutionEvent> getEvents() {
        return events;
    }

    public void setEvents(List<ExploratoryExecutionEvent> sessionEvents) {
        this.events = sessionEvents;
    }

    public void addSessionNote(SessionNote noteToAdd, Integer position) {
        new OrderedListHelper<>(sessionNotes).addContent(noteToAdd, position);
    }

    public void removeSessionNote(SessionNote noteToRemove) {
        sessionNotes.remove(noteToRemove);
        new OrderedListHelper<>(sessionNotes).reorder();
    }

    public void moveSessionNotes(Integer newIndex, List<SessionNote> movedNotes) {
        if (!sessionNotes.isEmpty() && !movedNotes.isEmpty()) {
            new OrderedListHelper<>(sessionNotes).moveContent(movedNotes, newIndex);
        }
    }
}
