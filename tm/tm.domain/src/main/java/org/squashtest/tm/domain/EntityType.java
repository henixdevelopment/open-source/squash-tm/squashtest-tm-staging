/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain;

import java.util.EnumSet;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import org.squashtest.tm.domain.actionword.ActionWordLibrary;
import org.squashtest.tm.domain.attachment.Attachment;
import org.squashtest.tm.domain.attachment.AttachmentList;
import org.squashtest.tm.domain.bdd.ActionWord;
import org.squashtest.tm.domain.bugtracker.Issue;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.CampaignLibrary;
import org.squashtest.tm.domain.campaign.ExploratorySessionOverview;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.Sprint;
import org.squashtest.tm.domain.campaign.SprintGroup;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.chart.ChartDefinition;
import org.squashtest.tm.domain.customreport.CustomReportCustomExport;
import org.squashtest.tm.domain.customreport.CustomReportDashboard;
import org.squashtest.tm.domain.customreport.CustomReportFolder;
import org.squashtest.tm.domain.customreport.CustomReportLibrary;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.execution.SessionNote;
import org.squashtest.tm.domain.infolist.InfoListItem;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.project.ProjectTemplate;
import org.squashtest.tm.domain.report.ReportDefinition;
import org.squashtest.tm.domain.requirement.HighLevelRequirement;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.requirement.RequirementLibrary;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.domain.scm.ScmRepository;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.domain.testautomation.AutomatedSuite;
import org.squashtest.tm.domain.testautomation.AutomatedTest;
import org.squashtest.tm.domain.testcase.ActionTestStep;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.domain.testcase.TestCaseLibrary;
import org.squashtest.tm.domain.testcase.TestStep;
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequest;
import org.squashtest.tm.domain.users.User;

public enum EntityType {

    // @formatter:off
    ACTION_TEST_STEP(ActionTestStep.class),
    ACTION_WORD(ActionWord.class),
    ACTION_WORD_LIBRARY(ActionWordLibrary.class),
    ATTACHMENT(Attachment.class),
    ATTACHMENT_LIST(AttachmentList.class),
    AUTOMATED_EXECUTION_EXTENDER(AutomatedExecutionExtender.class),
    AUTOMATED_SUITE(AutomatedSuite.class),
    AUTOMATED_TEST(AutomatedTest.class),
    AUTOMATION_REQUEST(AutomationRequest.class),
    CAMPAIGN(Campaign.class),
    CAMPAIGN_FOLDER(CampaignFolder.class),
    CAMPAIGN_LIBRARY(CampaignLibrary.class),
    CHART_DEFINITION(ChartDefinition.class),
    CUSTOM_REPORT_CUSTOM_EXPORT(CustomReportCustomExport.class),
    CUSTOM_REPORT_DASHBOARD(CustomReportDashboard.class),
    CUSTOM_REPORT_FOLDER(CustomReportFolder.class),
    CUSTOM_REPORT_LIBRARY(CustomReportLibrary.class),
    DATASET(Dataset.class),
    EXECUTION(Execution.class),
    EXECUTION_STEP(ExecutionStep.class),
    EXPLORATORY_SESSION_OVERVIEW(ExploratorySessionOverview.class),
    HIGH_LEVEL_REQUIREMENT(HighLevelRequirement.class),
    INFO_LIST_ITEM(InfoListItem.class),
    ISSUE(Issue.class),
    ITEM_TEST_PLAN(IterationTestPlanItem.class),
    ITERATION(Iteration.class),
    MILESTONE(Milestone.class),
    PARAMETER(Parameter.class),
    PROJECT(Project.class),
    PROJECT_TEMPLATE(ProjectTemplate.class),
    REPORT_DEFINITION(ReportDefinition.class),
    REQUIREMENT(Requirement.class),
    REQUIREMENT_FOLDER(RequirementFolder.class),
    REQUIREMENT_LIBRARY(RequirementLibrary.class),
    REQUIREMENT_VERSION(RequirementVersion.class),
    SCM_REPOSITORY(ScmRepository.class),
    SESSION_NOTE(SessionNote.class),
    SPRINT(Sprint.class),
    SPRINT_GROUP(SprintGroup.class),
    TEST_CASE(TestCase.class),
    TEST_CASE_FOLDER(TestCaseFolder.class),
    TEST_CASE_LIBRARY(TestCaseLibrary.class),
    TEST_CASE_STEP(TestStep.class),
    TEST_STEP(TestStep.class),
    TEST_SUITE(TestSuite.class),
    USER(User.class);

    // @formatter:on
    private Class clazz;

    EntityType(Class clazz) {
        this.clazz = clazz;
    }

    public Class getEntityClass() {
        return clazz;
    }

    public String getSimpleClassName() {
        return clazz.getSimpleName();
    }

    public static EntityType fromSimpleName(String simpleClassName) {
        if (StringUtils.isBlank(simpleClassName)) {
            throw new IllegalArgumentException("Simple Name can't be empty");
        }

        EnumSet<EntityType> entityTypes = EnumSet.allOf(EntityType.class);

        Optional<EntityType> type =
                entityTypes.stream()
                        .filter(entityType -> entityType.getSimpleClassName().equals(simpleClassName))
                        .findFirst();

        if (type.isPresent()) {
            return type.get();
        }

        throw new IllegalArgumentException("No Entity Type for simple class name : " + simpleClassName);
    }
}
