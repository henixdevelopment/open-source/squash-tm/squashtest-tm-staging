/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.execution;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.squashtest.tm.domain.Sizes;

@Entity
public class ExploratoryExecutionEvent {

    @Id
    @Column(name = "EVENT_ID")
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "exploratory_execution_event_event_id_seq")
    @SequenceGenerator(
            name = "exploratory_execution_event_event_id_seq",
            sequenceName = "exploratory_execution_event_event_id_seq",
            allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "EXECUTION_ID")
    @NotNull
    private ExploratoryExecution exploratoryExecution;

    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    @Column(name = "EVENT_DATE")
    private Date date;

    @NotNull
    @Size(max = Sizes.AUTHOR_MAX)
    private String author;

    @Enumerated(EnumType.STRING)
    @Column(name = "EVENT_TYPE")
    private ExploratoryExecutionEventType eventType;

    public ExploratoryExecutionEvent() {}

    public ExploratoryExecutionEvent(
            ExploratoryExecution exploratoryExecution,
            Date date,
            String author,
            ExploratoryExecutionEventType eventType) {
        this.exploratoryExecution = exploratoryExecution;
        this.date = date;
        this.author = author;
        this.eventType = eventType;
    }

    public Long getId() {
        return id;
    }

    public ExploratoryExecution getExploratoryExecution() {
        return exploratoryExecution;
    }

    public void setExploratoryExecution(ExploratoryExecution exploratoryExecution) {
        this.exploratoryExecution = exploratoryExecution;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public ExploratoryExecutionEventType getEventType() {
        return eventType;
    }

    public void setEventType(ExploratoryExecutionEventType eventType) {
        this.eventType = eventType;
    }
}
