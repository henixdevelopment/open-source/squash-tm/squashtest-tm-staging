/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.campaign;

import static javax.persistence.FetchType.LAZY;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.Sizes;
import org.squashtest.tm.domain.attachment.Attachment;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.BoundEntity;
import org.squashtest.tm.domain.library.NodeVisitor;
import org.squashtest.tm.domain.synchronisation.RemoteSynchronisation;
import org.squashtest.tm.exception.campaign.SprintNotLinkableException;

@Entity
@PrimaryKeyJoinColumn(name = "CLN_ID")
public class Sprint extends CampaignLibraryNode implements BoundEntity {

    public static final String SIMPLE_CLASS_NAME = "org.squashtest.tm.domain.campaign.Sprint";

    @OneToOne(fetch = LAZY)
    @JoinColumn(name = "REMOTE_SYNCHRONISATION_ID")
    private RemoteSynchronisation remoteSynchronisation;

    @Column(name = "REMOTE_SPRINT_ID")
    private Long remoteSprintId;

    @Column(name = "REMOTE_NAME")
    private String remoteName;

    @Column(name = "REMOTE_STATE")
    private String remoteState;

    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    @Size(max = Sizes.REFERENCE_MAX)
    @Column(name = "REFERENCE")
    private String reference;

    @NotNull
    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private SprintStatus status = SprintStatus.UPCOMING;

    @OrderBy
    @OneToMany(mappedBy = "sprint", cascade = CascadeType.ALL, orphanRemoval = true, fetch = LAZY)
    private List<SprintReqVersion> sprintReqVersions = new ArrayList<>();

    public Sprint() {
        super();
    }

    @Override
    public void accept(CampaignLibraryNodeVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public void accept(NodeVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public EntityReference toEntityReference() {
        return new EntityReference(EntityType.SPRINT, this.getId());
    }

    @Override
    public Sprint createCopy() {
        final Sprint copy = new Sprint();
        // Some attributes (remoteName, remoteState, remoteSprintId, remoteSynchronisation)
        // are not copied because they are related to synchronisation,
        // and they should not be taken into consideration for a copy/paste
        copy.notifyAssociatedWithProject(getProject());
        copy.setName(getName());
        copy.setDescription(getDescription());
        copy.setReference(getReference());
        copy.setStatus(SprintStatus.UPCOMING);
        copy.setStartDate(getStartDate());
        copy.setEndDate(getEndDate());

        for (Attachment attach : this.getAttachmentList().getAllAttachments()) {
            Attachment copyAttach = attach.shallowCopy();
            copy.getAttachmentList().addAttachment(copyAttach);
        }

        copy.setSprintReqVersions(
                sprintReqVersions.stream()
                        .map(sprintReqVersion -> sprintReqVersion.createCopy(copy))
                        .toList());
        return copy;
    }

    @Override
    public Long getBoundEntityId() {
        return getId();
    }

    @Override
    public BindableEntity getBoundEntityType() {
        return BindableEntity.SPRINT;
    }

    public RemoteSynchronisation getRemoteSynchronisation() {
        return remoteSynchronisation;
    }

    public void setRemoteSynchronisation(RemoteSynchronisation remoteSynchronisation) {
        this.remoteSynchronisation = remoteSynchronisation;
    }

    public String getReference() {
        return reference;
    }

    public String getRemoteName() {
        return remoteName;
    }

    public void setRemoteName(String remoteName) {
        this.remoteName = remoteName;
    }

    public String getRemoteState() {
        return remoteState;
    }

    public void setRemoteState(String remoteState) {
        this.remoteState = remoteState;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Long getRemoteSprintId() {
        return remoteSprintId;
    }

    public void setRemoteSprintId(Long remoteSprintId) {
        this.remoteSprintId = remoteSprintId;
    }

    public SprintStatus getStatus() {
        return status;
    }

    public void setStatus(SprintStatus status) {
        this.status = status;
    }

    public List<SprintReqVersion> getSprintReqVersions() {
        return sprintReqVersions;
    }

    public void setSprintReqVersions(List<SprintReqVersion> sprintReqVersions) {
        this.sprintReqVersions = sprintReqVersions;
    }

    /**
     * @throws SprintNotLinkableException
     */
    public void checkLinkable() {
        if (status == SprintStatus.CLOSED) {
            throw new SprintNotLinkableException();
        }
    }

    @Override
    public boolean allowIdenticalName() {
        return true;
    }

    public void migrateTestPlans() {
        sprintReqVersions.forEach(SprintReqVersion::migrateTestPlan);
    }

    public boolean isSynchronized() {
        return Objects.nonNull(this.remoteSynchronisation);
    }
}
