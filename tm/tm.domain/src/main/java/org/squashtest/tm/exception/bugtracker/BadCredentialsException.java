/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.exception.bugtracker;

import org.apache.commons.lang3.StringUtils;
import org.squashtest.tm.core.foundation.exception.ActionException;

public class BadCredentialsException extends ActionException {

    private static final long serialVersionUID = 7629886493066796741L;
    private static final String I18N_KEY =
            "sqtm-core.administration-workspace.bugtrackers.authentication-policy.credentials.bad-credentials";

    public BadCredentialsException() {
        super();
    }

    public BadCredentialsException(Exception cause) {
        super(cause);
    }

    @Override
    public String getI18nKey() {
        final String causeMessage = getCause() != null ? getCause().getMessage() : null;

        if (StringUtils.isNotBlank(causeMessage)) {
            return causeMessage;
        }

        return I18N_KEY;
    }
}
