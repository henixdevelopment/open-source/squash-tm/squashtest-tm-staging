/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.campaign;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Transient;
import org.squashtest.tm.core.foundation.exception.NullArgumentException;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.library.NodeContainerVisitor;
import org.squashtest.tm.domain.library.NodeVisitor;
import org.squashtest.tm.domain.library.SprintGroupContainer;
import org.squashtest.tm.domain.library.SprintGroupSupport;
import org.squashtest.tm.exception.DuplicateNameException;

@Entity
@PrimaryKeyJoinColumn(name = "CLN_ID")
public class SprintGroup extends CampaignLibraryNode
        implements SprintGroupContainer<CampaignLibraryNode> {
    private static final Logger LOGGER = LoggerFactory.getLogger(SprintGroup.class);
    public static final String SIMPLE_CASS_NAME = "org.squashtest.tm.domain.campaign.SprintGroup";

    @Column(name = "REMOTE_SYNCHRONISATION_ID")
    private Long remoteSynchronisationId;

    @Transient
    private final SprintGroupSupport<CampaignLibraryNode, SprintGroup> sprintGroupSupport =
            new SprintGroupSupport<>(this);

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @OrderColumn(name = "CONTENT_ORDER")
    @JoinTable(
            name = "CLN_RELATIONSHIP",
            joinColumns = @JoinColumn(name = "ANCESTOR_ID"),
            inverseJoinColumns = @JoinColumn(name = "DESCENDANT_ID"))
    private List<CampaignLibraryNode> content = new ArrayList<>();

    public SprintGroup() {
        super();
    }

    @Override
    public void addContent(CampaignLibraryNode node)
            throws DuplicateNameException, NullArgumentException {
        sprintGroupSupport.addContent(node);
    }

    @Override
    public void addContent(CampaignLibraryNode node, int position)
            throws DuplicateNameException, NullArgumentException {
        sprintGroupSupport.addContent(node, position);
        // the following enforces that hibernate reinsert the data with their index,
        // and makes sure it works along the triggers.
        content = new ArrayList<>(content);
    }

    @Override
    public boolean isContentNameAvailable(String name) {
        return sprintGroupSupport.isContentNameAvailable(name);
    }

    @Override
    public List<CampaignLibraryNode> getContent() {
        return content;
    }

    @Override
    public Collection<CampaignLibraryNode> getOrderedContent() {
        return content;
    }

    @Override
    public boolean hasContent() {
        return sprintGroupSupport.hasContent();
    }

    @Override
    public void removeContent(CampaignLibraryNode contentToRemove) throws NullArgumentException {
        content.remove(contentToRemove);
        content = new ArrayList<>(content);
        LOGGER.info(content.toString());
    }

    @Override
    public List<String> getContentNames() {
        return sprintGroupSupport.getContentNames();
    }

    @Override
    public void accept(NodeContainerVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public void accept(CampaignLibraryNodeVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public void accept(NodeVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public EntityReference toEntityReference() {
        return new EntityReference(EntityType.SPRINT_GROUP, this.getId());
    }

    @Override
    public SprintGroup createCopy() {
        return sprintGroupSupport.createCopy(new SprintGroup());
    }

    @Override
    public Long getBoundEntityId() {
        return getId();
    }

    @Override
    public BindableEntity getBoundEntityType() {
        return BindableEntity.SPRINT_GROUP;
    }

    public Long getRemoteSynchronisationId() {
        return remoteSynchronisationId;
    }

    public void setRemoteSynchronisationId(Long remoteSynchronisationId) {
        this.remoteSynchronisationId = remoteSynchronisationId;
    }

    public boolean isSynchronized() {
        return Objects.nonNull(this.remoteSynchronisationId);
    }
}
