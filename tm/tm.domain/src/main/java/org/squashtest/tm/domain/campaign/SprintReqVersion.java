/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.campaign;

import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Type;
import org.squashtest.tm.core.foundation.sanitizehtml.HTMLSanitizeUtils;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.domain.Sizes;
import org.squashtest.tm.domain.audit.Auditable;
import org.squashtest.tm.domain.audit.BaseAuditableEntity;
import org.squashtest.tm.domain.campaign.testplan.TestPlan;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.requirement.ManagementMode;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.security.annotation.InheritsAcls;

@Entity
@Table(name = "SPRINT_REQ_VERSION")
@InheritsAcls(constrainedClass = Sprint.class, collectionName = "sprintReqVersions")
@Auditable
public class SprintReqVersion extends BaseAuditableEntity implements Identified {

    @Id
    @Column(name = "SPRINT_REQ_VERSION_ID")
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "sprint_req_version_sprint_req_version_id_seq")
    @SequenceGenerator(
            name = "sprint_req_version_sprint_req_version_id_seq",
            sequenceName = "sprint_req_version_sprint_req_version_id_seq",
            allocationSize = 1)
    private Long id;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "REQ_VERSION_ID", referencedColumnName = "RES_ID")
    private RequirementVersion requirementVersion;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "SPRINT_ID", referencedColumnName = "CLN_ID")
    private Sprint sprint;

    @Column(name = "REFERENCE")
    @Size(max = Sizes.STATUS_MAX)
    private String reference;

    @Column(name = "NAME")
    @Size(max = Sizes.NAME_MAX)
    private String name;

    @Column(name = "STATUS")
    @Size(max = Sizes.STATUS_MAX)
    private String status;

    @Column(name = "VALIDATION_STATUS")
    @NotNull
    @Enumerated(EnumType.STRING)
    private SprintReqVersionValidationStatus validationStatus =
            SprintReqVersionValidationStatus.TO_BE_TESTED;

    @Column(name = "CRITICALITY")
    @Size(max = Sizes.STATUS_MAX)
    private String criticality;

    @Column(name = "CATEGORY")
    @Size(max = Sizes.STATUS_MAX)
    private String category;

    @Column(name = "DESCRIPTION")
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String description;

    @Column
    @Enumerated(EnumType.STRING)
    private ManagementMode mode = ManagementMode.NATIVE;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TEST_PLAN_ID", referencedColumnName = "TEST_PLAN_ID")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private TestPlan testPlan;

    public SprintReqVersion() {
        // Hibernate-friendly constructor
    }

    // used by synchronisation plugins
    public SprintReqVersion(Sprint sprint) {
        this.sprint = sprint;
        this.testPlan = new TestPlan(sprint.getCampaignLibrary());
    }

    public SprintReqVersion(RequirementVersion requirementVersion, Sprint sprint) {
        this.requirementVersion = requirementVersion;
        this.sprint = sprint;
        this.testPlan = new TestPlan(sprint.getProject().getCampaignLibrary());

        // Populate test plan with test cases verifying the requirement version
        testPlan.createAndAddTestPlanItems(requirementVersion.getOrderedVerifyingTestCases());
    }

    @Override
    public Long getId() {
        return id;
    }

    public Project getProject() {
        if (sprint != null) {
            return sprint.getProject();
        } else {
            return null;
        }
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RequirementVersion getRequirementVersion() {
        return requirementVersion;
    }

    public void setRequirementVersion(RequirementVersion requirementVersion) {
        this.requirementVersion = requirementVersion;
    }

    public Sprint getSprint() {
        return sprint;
    }

    public void setSprint(Sprint sprint) {
        this.sprint = sprint;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public SprintReqVersionValidationStatus getValidationStatus() {
        return validationStatus;
    }

    public void setValidationStatus(SprintReqVersionValidationStatus validationStatus) {
        this.validationStatus = validationStatus;
    }

    public String getCriticality() {
        return criticality;
    }

    public void setCriticality(String criticality) {
        this.criticality = criticality;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = HTMLSanitizeUtils.cleanHtml(description);
    }

    public ManagementMode getMode() {
        return mode;
    }

    public void setMode(ManagementMode mode) {
        this.mode = mode;
    }

    public TestPlan getTestPlan() {
        return testPlan;
    }

    public SprintReqVersion createCopy(Sprint parentSprint) {
        final SprintReqVersion copy = new SprintReqVersion(parentSprint);
        copy.setRequirementVersion(requirementVersion);
        // Following attributes are only copied if no requirementVersion exists in Squash.
        // It is thus treated like a denormalized SprintReqVersion.
        // Otherwise, necessary information is found in requirementVersion.
        if (Objects.isNull(requirementVersion)) {
            copy.setReference(reference);
            copy.setName(name);
            copy.setCriticality(criticality);
            copy.setCategory(category);
            copy.setStatus(status);
        }
        copy.setSprint(parentSprint);
        copy.setDescription(description);
        copy.setMode(ManagementMode.NATIVE);
        copy.testPlan.copyItems(testPlan);
        return copy;
    }

    public void migrateTestPlan() {
        testPlan.setCampaignLibrary(sprint.getCampaignLibrary());
    }
}
