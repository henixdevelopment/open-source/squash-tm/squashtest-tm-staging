/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.campaign;

import static org.squashtest.tm.domain.requirement.RemoteRequirementPerimeterStatus.UNKNOWN;

import java.net.URL;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.squashtest.tm.domain.requirement.RemoteRequirementPerimeterStatus;
import org.squashtest.tm.domain.synchronisation.RemoteSynchronisation;

@Entity
public class SprintRequirementSyncExtender {

    @Id
    @Column(name = "SPRINT_REQ_SYNC_ID")
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "sprint_requirement_sync_extender_sprint_req_sync_id_seq")
    @SequenceGenerator(
            name = "sprint_requirement_sync_extender_sprint_req_sync_id_seq",
            sequenceName = "sprint_requirement_sync_extender_sprint_req_sync_id_seq",
            allocationSize = 1)
    private Long id;

    @OneToOne
    @JoinColumn(name = "SPRINT_REQ_VERSION_ID", referencedColumnName = "SPRINT_REQ_VERSION_ID")
    private SprintReqVersion sprintReqVersion;

    @Column private String remoteReqId;

    @Column private String remoteProjectId;

    @Column(name = "REMOTE_REQ_URL")
    private URL remoteReqUrl;

    @Column(name = "REMOTE_LAST_UPDATED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date remoteLastUpdated;

    @ManyToOne
    @JoinColumn(name = "REMOTE_SYNCHRONISATION_ID")
    private RemoteSynchronisation remoteSynchronisation;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "REMOTE_PERIMETER_STATUS")
    private RemoteRequirementPerimeterStatus remotePerimeterStatus = UNKNOWN;

    @Column
    @Size(max = 20)
    private String remoteReqState;

    public SprintReqVersion getSprintReqVersion() {
        return sprintReqVersion;
    }

    public void setSprintReqVersion(SprintReqVersion sprintReqVersion) {
        this.sprintReqVersion = sprintReqVersion;
    }

    public String getRemoteReqId() {
        return remoteReqId;
    }

    public void setRemoteReqId(String remoteReqId) {
        this.remoteReqId = remoteReqId;
    }

    public String getRemoteProjectId() {
        return remoteProjectId;
    }

    public void setRemoteProjectId(String remoteProjectId) {
        this.remoteProjectId = remoteProjectId;
    }

    public URL getRemoteReqUrl() {
        return remoteReqUrl;
    }

    public void setRemoteReqUrl(URL remoteUrl) {
        this.remoteReqUrl = remoteUrl;
    }

    public Date getRemoteLastUpdated() {
        return remoteLastUpdated;
    }

    public void setRemoteLastUpdated(Date remoteLastUpdated) {
        this.remoteLastUpdated = remoteLastUpdated;
    }

    public RemoteSynchronisation getRemoteSynchronisation() {
        return remoteSynchronisation;
    }

    public void setRemoteSynchronisation(RemoteSynchronisation remoteSynchronisation) {
        this.remoteSynchronisation = remoteSynchronisation;
    }

    public RemoteRequirementPerimeterStatus getRemotePerimeterStatus() {
        return remotePerimeterStatus;
    }

    public void setRemotePerimeterStatus(RemoteRequirementPerimeterStatus remotePerimeterStatus) {
        this.remotePerimeterStatus = remotePerimeterStatus;
    }

    public String getRemoteReqState() {
        return remoteReqState;
    }

    public void setRemoteReqState(String remoteReqState) {
        this.remoteReqState = remoteReqState;
    }
}
