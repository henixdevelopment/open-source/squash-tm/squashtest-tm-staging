/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.user;

import java.util.Date;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.squashtest.tm.domain.users.ApiToken;
import org.squashtest.tm.service.internal.dto.ApiTokenDto;

public interface ApiTokenService {

    // User self-management
    ApiTokenDto generateApiToken(String name, Date expiryDate, String permissions);

    Page<ApiToken> findAllByUserId(long userId, Pageable pageable);

    void deletePersonalApiToken(long tokenId);

    void selfDestroyApiToken(String token);

    // Administrator management for Test automation server users
    ApiTokenDto generateApiTokenForTestAutoServer(
            long userId, String name, Date expiryDate, String permissions);

    Page<ApiToken> findAllTestAutoServerApiTokens(long userId, Pageable pageable);

    void deleteTestAutoServerApiToken(long tokenId);
}
