/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.tf;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequestStatus;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.service.display.tf.AutomationWorkspaceDataProviderService;
import org.squashtest.tm.service.internal.display.dto.UserView;
import org.squashtest.tm.service.internal.display.dto.tf.AutomationTesterWorkspaceDataDto;
import org.squashtest.tm.service.internal.display.dto.tf.AutomationWorkspaceDataDto;
import org.squashtest.tm.service.internal.dto.UserDto;
import org.squashtest.tm.service.internal.repository.UserDao;
import org.squashtest.tm.service.internal.repository.display.AutomationRequestDisplayDao;
import org.squashtest.tm.service.internal.repository.display.AutomationTesterRequestDisplayDao;
import org.squashtest.tm.service.project.ProjectFinder;
import org.squashtest.tm.service.user.UserAccountService;

@Service
@Transactional(readOnly = true)
public class AutomationWorkspaceDataProviderServiceImpl
        implements AutomationWorkspaceDataProviderService {

    private final ProjectFinder projectFinder;

    private final UserAccountService userAccountService;

    private final UserDao userDao;

    private final AutomationRequestDisplayDao automationRequestDisplayDao;
    private final AutomationTesterRequestDisplayDao automationTesterRequestDisplayDao;

    public AutomationWorkspaceDataProviderServiceImpl(
            ProjectFinder projectFinder,
            UserAccountService userAccountService,
            UserDao userDao,
            AutomationRequestDisplayDao automationRequestDisplayDao,
            AutomationTesterRequestDisplayDao automationTesterRequestDisplayDao) {
        this.projectFinder = projectFinder;
        this.userAccountService = userAccountService;
        this.userDao = userDao;
        this.automationRequestDisplayDao = automationRequestDisplayDao;
        this.automationTesterRequestDisplayDao = automationTesterRequestDisplayDao;
    }

    @Override
    public AutomationWorkspaceDataDto findData() {
        AutomationWorkspaceDataDto dto = new AutomationWorkspaceDataDto();
        UserDto currentUser = userAccountService.findCurrentUserDto();
        List<Long> readableProjectIds = projectFinder.findAllReadableIdsForAutomationWriter();
        dto.setUsersWhoModifiedTestCasesAssignView(
                findModifiedUserViewsAssignView(readableProjectIds, currentUser.getUserId()));
        dto.setUsersWhoModifiedTestCasesTreatmentView(
                findModifiedUserViewsTreatmentView(readableProjectIds));
        dto.setUsersWhoModifiedTestCasesGlobalView(findModifiedUserViewsGlobalView(readableProjectIds));
        dto.setUsersAssignedTo(findAssignedUserViewsGlobalView(readableProjectIds));
        dto.setNbAssignedAutomReq(
                automationRequestDisplayDao.countByAssignedTo(readableProjectIds, currentUser.getUserId()));
        dto.setNbAutomReqToTreat(automationRequestDisplayDao.countTransmitted(readableProjectIds));
        dto.setNbTotal(automationRequestDisplayDao.countAll(readableProjectIds));
        return dto;
    }

    @Override
    public AutomationTesterWorkspaceDataDto findFunctionalTesterData() {
        AutomationTesterWorkspaceDataDto dto = new AutomationTesterWorkspaceDataDto();
        List<Long> readableProjectIds = projectFinder.findAllReadableIds();
        dto.setNbGlobalAutomReq(
                automationTesterRequestDisplayDao.countGlobalAutomationRequests(readableProjectIds));
        dto.setNbReadyForTransmissionAutomReq(
                automationTesterRequestDisplayDao.countReadyForTransmissionAutomRequests(
                        readableProjectIds));
        dto.setNbToBeValidatedAutomReq(
                automationTesterRequestDisplayDao.countToBeValidatedAutomRequests(readableProjectIds));
        dto.setUsersWhoModifiedTestCasesGlobalView(
                findModifiedUserViewsFunctionalTesterGlobalView(readableProjectIds));
        dto.setUsersWhoModifiedTestCasesReadyView(
                findModifiedUserViewsFunctionalTesterReadyView(readableProjectIds));
        dto.setUsersWhoModifiedTestCasesValidateView(
                findModifiedUserViewsFunctionalTesterValidateView(readableProjectIds));
        return dto;
    }

    private List<UserView> findModifiedUserViewsFunctionalTesterGlobalView(
            List<Long> readableProjectIds) {
        List<String> status =
                Arrays.stream(AutomationRequestStatus.values()).map(AutomationRequestStatus::name).toList();
        List<String> usersWhoModifiedTestCases =
                automationTesterRequestDisplayDao.getLastModifyingUserLoginsForTesterView(
                        readableProjectIds, status);
        return findUserViewsByLogin(usersWhoModifiedTestCases);
    }

    private List<UserView> findModifiedUserViewsFunctionalTesterReadyView(
            List<Long> readableProjectIds) {
        List<String> status = Arrays.asList(AutomationRequestStatus.READY_TO_TRANSMIT.name());
        List<String> usersWhoModifiedTestCases =
                automationTesterRequestDisplayDao.getLastModifyingUserLoginsForTesterView(
                        readableProjectIds, status);
        return findUserViewsByLogin(usersWhoModifiedTestCases);
    }

    private List<UserView> findModifiedUserViewsFunctionalTesterValidateView(
            List<Long> readableProjectIds) {
        List<String> status =
                Arrays.asList(
                        AutomationRequestStatus.WORK_IN_PROGRESS.name(),
                        AutomationRequestStatus.REJECTED.name(),
                        AutomationRequestStatus.SUSPENDED.name());
        List<String> usersWhoModifiedTestCases =
                automationTesterRequestDisplayDao.getLastModifyingUserLoginsForTesterView(
                        readableProjectIds, status);
        return findUserViewsByLogin(usersWhoModifiedTestCases);
    }

    private List<UserView> findAssignedUserViewsGlobalView(List<Long> readableProjectIds) {
        List<String> usersWhoModifiedTestCases =
                automationRequestDisplayDao.getAssignedUserForAutomationRequests(readableProjectIds);
        return findUserViewsByLogin(usersWhoModifiedTestCases);
    }

    private List<UserView> findModifiedUserViewsAssignView(
            List<Long> readableProjectIds, Long userId) {
        List<String> status =
                Arrays.stream(AutomationRequestStatus.values()).map(AutomationRequestStatus::name).toList();
        List<String> usersWhoModifiedTestCases =
                automationRequestDisplayDao.getTcLastModifiedByForCurrentUser(
                        readableProjectIds, status, userId);
        return findUserViewsByLogin(usersWhoModifiedTestCases);
    }

    private List<UserView> findModifiedUserViewsTreatmentView(List<Long> readableProjectIds) {
        List<String> status =
                Arrays.asList(
                        AutomationRequestStatus.TRANSMITTED.name(),
                        AutomationRequestStatus.AUTOMATION_IN_PROGRESS.name());
        List<String> usersWhoModifiedTestCases =
                automationRequestDisplayDao.getTcLastModifiedByToAutomationRequestNotAssigned(
                        readableProjectIds, status);
        return findUserViewsByLogin(usersWhoModifiedTestCases);
    }

    private List<UserView> findModifiedUserViewsGlobalView(List<Long> readableProjectIds) {
        List<String> status =
                Arrays.stream(AutomationRequestStatus.values()).map(AutomationRequestStatus::name).toList();
        List<String> usersWhoModifiedTestCases =
                automationRequestDisplayDao.getTcLastModifiedByForAutomationRequests(
                        readableProjectIds, status);
        return findUserViewsByLogin(usersWhoModifiedTestCases);
    }

    private List<UserView> findUserViewsByLogin(List<String> usersWhoCreatedTestCases) {
        List<User> users = userDao.findUsersByLoginList(usersWhoCreatedTestCases);
        Map<String, UserView> userViews =
                users.stream()
                        .map(
                                user -> {
                                    UserView userView = new UserView();
                                    userView.setId(user.getId());
                                    userView.setFirstName(user.getFirstName());
                                    userView.setLastName(user.getLastName());
                                    userView.setLogin(user.getLogin());
                                    return userView;
                                })
                        .collect(Collectors.toMap(UserView::getLogin, Function.identity()));

        return usersWhoCreatedTestCases.stream()
                .map(login -> userViews.getOrDefault(login, new UserView(login)))
                .toList();
    }
}
