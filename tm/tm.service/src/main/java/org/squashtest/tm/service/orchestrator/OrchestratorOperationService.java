/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.orchestrator;

import java.util.List;
import java.util.function.BiFunction;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.service.internal.dto.WorkflowDto;
import org.squashtest.tm.service.orchestrator.model.OrchestratorConfVersions;
import org.squashtest.tm.service.orchestrator.model.OrchestratorResponse;
import org.squashtest.tm.service.testautomation.spi.TestAutomationConnector;

public interface OrchestratorOperationService {

    OrchestratorResponse<OrchestratorConfVersions> getOrchestratorConfVersions(
            Long testAutomationServerId);

    OrchestratorResponse<List<WorkflowDto>> getProjectWorkflows(
            Long testAutomationServerId, Long projectId);

    OrchestratorResponse<Void> killWorkflow(
            Long testAutomationServerId, Long projectId, String workflowId);

    <T> OrchestratorResponse<T> performOrchestratorOperation(
            Long testAutomationServerId,
            BiFunction<TestAutomationConnector, TestAutomationServer, OrchestratorResponse<T>> operation);
}
