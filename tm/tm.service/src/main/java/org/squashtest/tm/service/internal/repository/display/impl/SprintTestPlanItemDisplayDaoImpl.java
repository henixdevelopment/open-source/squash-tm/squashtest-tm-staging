/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_SESSION_OVERVIEW;
import static org.squashtest.tm.jooq.domain.Tables.TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.tables.TestPlan.TEST_PLAN;

import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.repository.display.SprintTestPlanItemDisplayDao;

@Repository
public class SprintTestPlanItemDisplayDaoImpl implements SprintTestPlanItemDisplayDao {
    private final DSLContext dslContext;

    public SprintTestPlanItemDisplayDaoImpl(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    @Override
    public Long findCampaignLibraryIdByTestPlanItemId(Long testPlanItemId) {
        return dslContext
                .select(TEST_PLAN.CL_ID)
                .from(TEST_PLAN)
                .leftJoin(TEST_PLAN_ITEM)
                .on(TEST_PLAN.TEST_PLAN_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ID))
                .where(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID.eq(testPlanItemId))
                .fetchOneInto(Long.class);
    }

    @Override
    public Long findCampaignLibraryIdByExploratorySessionOverviewId(Long overviewId) {
        return dslContext
                .select(TEST_PLAN.CL_ID)
                .from(TEST_PLAN)
                .leftJoin(TEST_PLAN_ITEM)
                .on(TEST_PLAN.TEST_PLAN_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ID))
                .leftJoin(EXPLORATORY_SESSION_OVERVIEW)
                .on(EXPLORATORY_SESSION_OVERVIEW.TEST_PLAN_ITEM_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID))
                .where(EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.eq(overviewId))
                .fetchOneInto(Long.class);
    }
}
