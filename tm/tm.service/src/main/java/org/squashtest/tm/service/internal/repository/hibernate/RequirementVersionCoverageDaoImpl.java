/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.jooq.impl.DSL.partitionBy;
import static org.jooq.impl.DSL.rowNumber;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_COVERAGE;
import static org.squashtest.tm.jooq.domain.Tables.VERIFYING_STEPS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.COVERAGE_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.REQUIREMENT_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ROW_NUMBER;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_CASE_ID;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.hibernate.type.LongType;
import org.jooq.DSLContext;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.core.foundation.collection.DefaultPagingAndSorting;
import org.squashtest.tm.core.foundation.collection.PagingAndSorting;
import org.squashtest.tm.core.foundation.collection.SortOrder;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.requirement.RequirementStatus;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.domain.testcase.RequirementVersionCoverage;
import org.squashtest.tm.exception.requirement.RequirementVersionNotLinkableException;
import org.squashtest.tm.service.internal.foundation.collection.PagingUtils;
import org.squashtest.tm.service.internal.foundation.collection.SortingUtils;
import org.squashtest.tm.service.internal.repository.CustomRequirementVersionCoverageDao;

public class RequirementVersionCoverageDaoImpl
        extends HibernateEntityDao<RequirementVersionCoverage>
        implements CustomRequirementVersionCoverageDao {

    @Inject private DSLContext dslContext;

    @SuppressWarnings("unchecked")
    @Override
    public List<RequirementVersionCoverage> findAllByTestCaseId(
            long testCaseId, PagingAndSorting pas) {

        // we have to fetch our query and modify the hql a bit, hence the weird operation below
        Query namedquery =
                currentSession().getNamedQuery("RequirementVersionCoverage.findAllByTestCaseId");
        String hql = namedquery.getQueryString();
        hql = SortingUtils.addOrder(hql, pas);

        Query q = currentSession().createQuery(hql);
        if (!pas.shouldDisplayAll()) {
            PagingUtils.addPaging(q, pas);
        }

        q.setParameter("testCaseId", testCaseId);

        List<Object[]> raw = q.list();

        // now we have to collect from the result set the only thing
        // we want : the coverages
        List<RequirementVersionCoverage> res = new ArrayList<>(raw.size());
        for (Object[] tuple : raw) {
            res.add((RequirementVersionCoverage) tuple[0]);
        }

        return res;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<RequirementVersion> findDistinctRequirementVersionsByTestCases(
            Collection<Long> testCaseIds, PagingAndSorting pagingAndSorting) {

        if (testCaseIds.isEmpty()) {
            return Collections.emptyList();
        }

        // we have to fetch our query and modify the hql a bit, hence the weird operation below
        Query namedquery =
                currentSession()
                        .getNamedQuery("RequirementVersion.findDistinctRequirementVersionsByTestCases");
        String hql = namedquery.getQueryString();
        hql = SortingUtils.addOrder(hql, pagingAndSorting);

        Query q = currentSession().createQuery(hql);
        if (!pagingAndSorting.shouldDisplayAll()) {
            PagingUtils.addPaging(q, pagingAndSorting);
        }

        q.setParameterList("testCaseIds", testCaseIds, LongType.INSTANCE);

        List<Object[]> raw = q.list();

        // now we have to collect from the result set the only thing
        // we want : the RequirementVersions
        List<RequirementVersion> res = new ArrayList<>(raw.size());
        for (Object[] tuple : raw) {
            res.add((RequirementVersion) tuple[0]);
        }
        if ("endDate".equals(pagingAndSorting.getSortedAttribute())) {
            Collections.sort(
                    res,
                    new Comparator<RequirementVersion>() {
                        @Override
                        public int compare(RequirementVersion req1, RequirementVersion req2) {
                            return compareReqMilestoneDate(req1, req2);
                        }
                    });

            if (pagingAndSorting.getSortOrder() == SortOrder.ASCENDING) {
                Collections.reverse(res);
            }
        }
        return res;
    }

    private int compareReqMilestoneDate(RequirementVersion req1, RequirementVersion req2) {

        boolean isEmpty1 = req1.getMilestones().isEmpty();
        boolean isEmpty2 = req2.getMilestones().isEmpty();

        if (isEmpty1 && isEmpty2) {
            return 0;
        } else if (isEmpty1) {
            return 1;
        } else if (isEmpty2) {
            return -1;
        } else {
            return getMinDate(req1).before(getMinDate(req2))
                    ? getMinDate(req1).after(getMinDate(req2)) ? 0 : 1
                    : -1;
        }
    }

    private Date getMinDate(RequirementVersion req) {
        return Collections.min(
                        req.getMilestones(),
                        new Comparator<Milestone>() {
                            @Override
                            public int compare(Milestone m1, Milestone m2) {
                                return m1.getEndDate().before(m2.getEndDate()) ? -1 : 1;
                            }
                        })
                .getEndDate();
    }

    @Override
    @Transactional(readOnly = true)
    public List<RequirementVersion> findDistinctRequirementVersionsByTestCases(
            Collection<Long> testCaseIds) {
        PagingAndSorting pas = new DefaultPagingAndSorting("RequirementVersion.name", true);
        return findDistinctRequirementVersionsByTestCases(testCaseIds, pas);
    }

    /*
     * Deletion of data from the VERIFYING_STEPS table not taken into account by Hibernate, deletion in native sql
     *
     * (non-Javadoc)
     * @see org.squashtest.tm.service.internal.repository.CustomRequirementVersionCoverageDao#delete(org.squashtest.tm.domain.testcase.RequirementVersionCoverage)
     */
    @Override
    public void delete(List<Long> requirementVersionCoverageIds) {
        removeTestSTepByCoverageIds(requirementVersionCoverageIds);

        Query queryRemoveRequirementVersionCoverage =
                currentSession().getNamedQuery("requirementDeletionDao.deleteVersionCoverages");
        queryRemoveRequirementVersionCoverage.setParameterList("covIds", requirementVersionCoverageIds);
        queryRemoveRequirementVersionCoverage.executeUpdate();
    }

    @Override
    public void removeTestSTepByCoverageIds(List<Long> requirementVersionCoverageIds) {
        Session s = currentSession();

        String sql = NativeQueries.REQUIREMENT_SQL_REMOVE_TEST_STEPS_BY_COVERAGE_IDS;
        NativeQuery q = s.createNativeQuery(sql);
        q.setParameterList("covIds", requirementVersionCoverageIds);
        q.executeUpdate();
        s.flush();
    }

    @Override
    public List<Long> byTestCaseAndRequirementVersions(
            Collection<Long> reqVerCovIds, long testCaseId) {
        return dslContext
                .select(REQUIREMENT_VERSION_COVERAGE.REQUIREMENT_VERSION_COVERAGE_ID)
                .from(REQUIREMENT_VERSION_COVERAGE)
                .where(
                        REQUIREMENT_VERSION_COVERAGE
                                .VERIFYING_TEST_CASE_ID
                                .eq(testCaseId)
                                .and(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.in(reqVerCovIds)))
                .fetchInto(Long.class);
    }

    @Override
    public List<Long> byRequirementVersionAndTestCases(
            Collection<Long> testCaseIds, long resourceId) {
        return dslContext
                .select(REQUIREMENT_VERSION_COVERAGE.REQUIREMENT_VERSION_COVERAGE_ID)
                .from(REQUIREMENT_VERSION_COVERAGE)
                .join(REQUIREMENT_VERSION)
                .on(REQUIREMENT_VERSION.RES_ID.eq(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID))
                .where(
                        REQUIREMENT_VERSION_COVERAGE
                                .VERIFYING_TEST_CASE_ID
                                .in(testCaseIds)
                                .and(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.eq(resourceId)))
                .fetchInto(Long.class);
    }

    @Override
    public void checkIfReqVersionIsLinkable(long reqVersionId) {
        String result =
                dslContext
                        .select(REQUIREMENT_VERSION.REQUIREMENT_STATUS)
                        .from(REQUIREMENT_VERSION)
                        .where(REQUIREMENT_VERSION.RES_ID.eq(reqVersionId))
                        .fetchOneInto(String.class);

        if (result != null) {
            RequirementStatus status = RequirementStatus.valueOf(result);

            if (!status.isRequirementLinkable()) {
                throw new RequirementVersionNotLinkableException(reqVersionId);
            }
        }
    }

    @Override
    public List<Long> byRequirementVersionsAndTestStep(Collection<Long> rvIds, long stepId) {
        return dslContext
                .select(REQUIREMENT_VERSION_COVERAGE.REQUIREMENT_VERSION_COVERAGE_ID)
                .from(REQUIREMENT_VERSION_COVERAGE)
                .join(REQUIREMENT_VERSION)
                .on(REQUIREMENT_VERSION.RES_ID.eq(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID))
                .join(VERIFYING_STEPS)
                .on(
                        VERIFYING_STEPS.REQUIREMENT_VERSION_COVERAGE_ID.eq(
                                REQUIREMENT_VERSION_COVERAGE.REQUIREMENT_VERSION_COVERAGE_ID))
                .where(VERIFYING_STEPS.TEST_STEP_ID.eq(stepId).and(REQUIREMENT_VERSION.RES_ID.in(rvIds)))
                .fetchInto(Long.class);
    }

    @Override
    public Map<Long, List<Long>> findVerifiedTestCaseIdsByVersionIds(Set<Long> versionIds) {
        return dslContext
                .select(
                        REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID,
                        REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID)
                .from(REQUIREMENT_VERSION_COVERAGE)
                .where(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.in(versionIds))
                .fetchGroups(
                        REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID,
                        REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID);
    }

    @Override
    public Map<Long, Map<Long, Long>> findExistingCoveragesByRequirementIds(
            Collection<Long> requirementIds) {
        var sub =
                dslContext
                        .select(
                                REQUIREMENT_VERSION.REQUIREMENT_ID,
                                REQUIREMENT_VERSION.VERSION_NUMBER,
                                REQUIREMENT_VERSION_COVERAGE.REQUIREMENT_VERSION_COVERAGE_ID.as(COVERAGE_ID),
                                REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID.as(TEST_CASE_ID),
                                rowNumber()
                                        .over(
                                                partitionBy(REQUIREMENT_VERSION.REQUIREMENT_ID)
                                                        .orderBy(REQUIREMENT_VERSION.VERSION_NUMBER.desc()))
                                        .as(ROW_NUMBER))
                        .from(REQUIREMENT_VERSION)
                        .innerJoin(REQUIREMENT_VERSION_COVERAGE)
                        .on(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.eq(REQUIREMENT_VERSION.RES_ID))
                        .where(REQUIREMENT_VERSION.REQUIREMENT_ID.in(requirementIds))
                        .asTable();

        return dslContext
                .select(sub.field(REQUIREMENT_ID), sub.field(COVERAGE_ID), sub.field(TEST_CASE_ID))
                .from(sub)
                .where(sub.field(ROW_NUMBER, Integer.class).eq(1))
                .fetchStream()
                .collect(
                        Collectors.groupingBy(
                                r -> r.get(sub.field(REQUIREMENT_ID, Long.class)),
                                Collectors.toMap(
                                        r -> r.get(sub.field(TEST_CASE_ID, Long.class)),
                                        r -> r.get(sub.field(COVERAGE_ID, Long.class)))));
    }
}
