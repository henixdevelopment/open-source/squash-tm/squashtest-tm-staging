/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.requirement;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.squashtest.tm.core.foundation.annotation.CleanHtml;
import org.squashtest.tm.domain.requirement.RemoteRequirementPerimeterStatus;
import org.squashtest.tm.service.internal.display.dto.AttachmentListDto;
import org.squashtest.tm.service.internal.display.dto.CustomFieldValueDto;
import org.squashtest.tm.service.internal.display.dto.MilestoneDto;

public abstract class AbstractRequirementVersionDto {
    private Long id;
    private Long projectId;
    private String name;
    private String reference;
    private int versionNumber;
    private Long category;
    private String criticality;
    private String status;
    private String createdBy;
    private Date createdOn;
    private String lastModifiedBy;
    private Date lastModifiedOn;
    private String description;
    private Long attachmentListId;
    private Long requirementId;
    private boolean hasExtender;
    private AttachmentListDto attachmentList;
    private List<CustomFieldValueDto> customFieldValues = new ArrayList<>();
    private List<MilestoneDto> milestones = new ArrayList<>();
    private List<MilestoneDto> bindableMilestones = new ArrayList<>();
    private List<VerifyingTestCaseDto> verifyingTestCases = new ArrayList<>();
    private RequirementVersionBundleStatsDto requirementStats;
    private Integer nbIssues;
    private String remoteReqUrl;
    private String remoteReqId;
    private String syncStatus;
    private RemoteRequirementPerimeterStatus remoteReqPerimeterStatus;
    private List<Long> syncReqIdsForRemoteSyncId;
    private Long serverId;
    private String serverName;
    private String remoteSynchronisationKind;
    private String path;
    private Long aiServerId;

    /** True if both payloadTemplate and jsonPath are not null */
    private Boolean canUseAiFeature;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Long getAttachmentListId() {
        return attachmentListId;
    }

    public void setAttachmentListId(Long attachmentListId) {
        this.attachmentListId = attachmentListId;
    }

    public AttachmentListDto getAttachmentList() {
        return attachmentList;
    }

    public void setAttachmentList(AttachmentListDto attachmentList) {
        this.attachmentList = attachmentList;
    }

    public List<CustomFieldValueDto> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<CustomFieldValueDto> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public int getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(int versionNumber) {
        this.versionNumber = versionNumber;
    }

    public Long getCategory() {
        return category;
    }

    public void setCategory(Long category) {
        this.category = category;
    }

    public String getCriticality() {
        return criticality;
    }

    public void setCriticality(String criticality) {
        this.criticality = criticality;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedOn() {
        return lastModifiedOn;
    }

    public void setLastModifiedOn(Date lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    public List<MilestoneDto> getMilestones() {
        return milestones;
    }

    public void setMilestones(List<MilestoneDto> milestones) {
        this.milestones = milestones;
    }

    @CleanHtml
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getRequirementId() {
        return requirementId;
    }

    public void setRequirementId(Long requirementId) {
        this.requirementId = requirementId;
    }

    public boolean isHasExtender() {
        return hasExtender;
    }

    public void setHasExtender(boolean hasExtender) {
        this.hasExtender = hasExtender;
    }

    public List<MilestoneDto> getBindableMilestones() {
        return bindableMilestones;
    }

    public void setBindableMilestones(List<MilestoneDto> bindableMilestones) {
        this.bindableMilestones = bindableMilestones;
    }

    public List<VerifyingTestCaseDto> getVerifyingTestCases() {
        return verifyingTestCases;
    }

    public void setVerifyingTestCases(List<VerifyingTestCaseDto> verifyingTestCases) {
        this.verifyingTestCases = verifyingTestCases;
    }

    public RequirementVersionBundleStatsDto getRequirementStats() {
        return requirementStats;
    }

    public void setRequirementStats(RequirementVersionBundleStatsDto requirementStats) {
        this.requirementStats = requirementStats;
    }

    public Integer getNbIssues() {
        return nbIssues;
    }

    public void setNbIssues(Integer nbIssues) {
        this.nbIssues = nbIssues;
    }

    public String getRemoteReqUrl() {
        return remoteReqUrl;
    }

    public void setRemoteReqUrl(String remoteReqUrl) {
        this.remoteReqUrl = remoteReqUrl;
    }

    public String getRemoteReqId() {
        return remoteReqId;
    }

    public void setRemoteReqId(String remoteReqId) {
        this.remoteReqId = remoteReqId;
    }

    public String getSyncStatus() {
        return syncStatus;
    }

    public void setSyncStatus(String syncStatus) {
        this.syncStatus = syncStatus;
    }

    public abstract boolean isHighLevelRequirement();

    public RemoteRequirementPerimeterStatus getRemoteReqPerimeterStatus() {
        return remoteReqPerimeterStatus;
    }

    public void setRemoteReqPerimeterStatus(
            RemoteRequirementPerimeterStatus remoteReqPerimeterStatus) {
        this.remoteReqPerimeterStatus = remoteReqPerimeterStatus;
    }

    public List<Long> getSyncReqIdsForRemoteSyncId() {
        return syncReqIdsForRemoteSyncId;
    }

    public void setSyncReqIdsForRemoteSyncId(List<Long> syncReqIdsForRemoteSyncId) {
        this.syncReqIdsForRemoteSyncId = syncReqIdsForRemoteSyncId;
    }

    public Long getServerId() {
        return serverId;
    }

    public void setServerId(Long serverId) {
        this.serverId = serverId;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getRemoteSynchronisationKind() {
        return remoteSynchronisationKind;
    }

    public void setRemoteSynchronisationKind(String remoteSynchronisationKind) {
        this.remoteSynchronisationKind = remoteSynchronisationKind;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getAiServerId() {
        return aiServerId;
    }

    public void setAiServerId(Long aiServerId) {
        this.aiServerId = aiServerId;
    }

    public Boolean isCanUseAiFeature() {
        return canUseAiFeature;
    }

    public void setCanUseAiFeature(Boolean canUseAiFeature) {
        this.canUseAiFeature = canUseAiFeature;
    }
}
