/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.artificialintelligence.server;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

import javax.inject.Inject;
import org.jooq.DSLContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.service.annotation.IsUltimateLicenseAvailable;
import org.squashtest.tm.service.display.artificialintelligence.server.AiServerDisplayService;
import org.squashtest.tm.service.internal.display.dto.AiServerAdminViewDto;
import org.squashtest.tm.service.internal.display.dto.CredentialsDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.administration.AiServerGrid;
import org.squashtest.tm.service.internal.repository.display.AiServerDisplayDao;
import org.squashtest.tm.service.servers.ManageableCredentials;
import org.squashtest.tm.service.servers.StoredCredentialsManager;

@Service
@Transactional
public class AiServerDisplayServiceImpl implements AiServerDisplayService {

    private final DSLContext dsl;
    private final AiServerDisplayDao aiServerDisplayDao;
    private final StoredCredentialsManager credentialsManager;

    @Inject
    AiServerDisplayServiceImpl(
            DSLContext dsl,
            AiServerDisplayDao aiServerDisplayDao,
            StoredCredentialsManager credentialsManager) {
        this.dsl = dsl;
        this.aiServerDisplayDao = aiServerDisplayDao;
        this.credentialsManager = credentialsManager;
    }

    @PreAuthorize(HAS_ROLE_ADMIN)
    @Override
    @IsUltimateLicenseAvailable
    public GridResponse findAll(GridRequest gridRequest) {
        GridResponse response = new AiServerGrid().getRows(gridRequest, dsl);
        response.sanitizeField("description");
        return response;
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    @IsUltimateLicenseAvailable
    public AiServerAdminViewDto getAiServerView(Long aiServerId) {
        AiServerAdminViewDto aiServerAdminViewDto = aiServerDisplayDao.findAiServerById(aiServerId);

        ManageableCredentials credentials = credentialsManager.findAppLevelCredentials(aiServerId);
        aiServerAdminViewDto.setCredentials(CredentialsDto.from(credentials));

        return aiServerAdminViewDto;
    }
}
