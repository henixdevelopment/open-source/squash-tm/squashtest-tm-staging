/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporter;

import java.util.ArrayList;
import java.util.List;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.library.NewFolderDto;

public class FolderToImport {

    private String internalId;
    private NewFolderDto folder;
    EntityType parentType;
    private String parentId;
    private List<AttachmentToImport> attachments = new ArrayList<>();

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public NewFolderDto getFolder() {
        return folder;
    }

    public void setFolder(NewFolderDto folder) {
        this.folder = folder;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public EntityType getParentType() {
        return parentType;
    }

    public void setParentType(EntityType parentType) {
        this.parentType = parentType;
    }

    public List<AttachmentToImport> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<AttachmentToImport> attachments) {
        this.attachments = attachments;
    }
}
