/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.grid;

import static org.squashtest.tm.domain.gridconfiguration.GridColumnDisplayConfiguration.CUF_COLUMN_PREFIX_FOR_CONFIGURATIONS;
import static org.squashtest.tm.jooq.domain.Tables.GRID_COLUMN_DISPLAY_CONFIGURATION;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.inject.Inject;
import org.apache.poi.ss.usermodel.Row;
import org.jooq.DSLContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.core.foundation.lang.DateUtils;
import org.squashtest.tm.domain.customfield.CustomField;
import org.squashtest.tm.domain.gridconfiguration.GridColumnDisplayConfiguration;
import org.squashtest.tm.domain.gridconfiguration.GridColumnDisplayReference;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.service.grid.ColumnIds;
import org.squashtest.tm.service.grid.GridConfigurationService;
import org.squashtest.tm.service.internal.batchexport.models.ExportModel;
import org.squashtest.tm.service.internal.repository.CustomFieldDao;
import org.squashtest.tm.service.internal.repository.GridColumnDisplayConfigurationDao;
import org.squashtest.tm.service.internal.repository.GridColumnDisplayReferenceDao;
import org.squashtest.tm.service.user.UserAccountService;

@Service("squashtest.tm.service.GridConfigurationService")
@Transactional
public class GridConfigurationServiceImpl implements GridConfigurationService {

    private static final List<String> MANDATORY_COLUMNS =
            Arrays.asList(
                    "#",
                    "folder",
                    "detail",
                    "delete",
                    "startExecution",
                    "execution",
                    "testCaseBoundToItem",
                    "workflows",
                    "attachmentPrune");
    private static final String REQUIREMENT_SEARCH = "requirement-search";
    private static final String TEST_CASE_SEARCH = "test-case-search";
    private static final String TC_BY_REQUIREMENT = "tc-by-requirement";
    private static final String ITPI_SEARCH = "itpi-search";
    private static final String ITERATION_TEST_PLAN = "iteration-test-plan";
    private static final String TEST_SUITE_TEST_PLAN = "test-suite-test-plan";
    private static final String CAMPAIGN_TEST_PLAN = "campaign-test-plan";
    private static final String ITERATION_AUTOMATED_SUITE = "iteration-automated-suite";
    private static final String TEST_SUITE_AUTOMATED_SUITE = "test-suite-automated-suite";

    @Inject private UserAccountService userAccountService;

    @Inject private GridColumnDisplayReferenceDao gridColumnDisplayReferenceDao;
    @Inject private GridColumnDisplayConfigurationDao gridColumnDisplayConfigurationDao;

    @Inject CustomFieldDao customFieldDao;

    @Inject private DSLContext dsl;

    /**
     * Adds or updates the grid column configuration set by the current user for the grid id passed as
     * an argument.
     *
     * @param gridId
     * @param columnIds : a list of strings referring to the column ids the user wants to be set in
     *     the grid configuration.
     */
    @Override
    public void addOrUpdatePreferenceForCurrentUser(String gridId, List<String> columnIds) {
        User user = userAccountService.findCurrentUser();
        gridId = setGridConfigId(gridId);
        addOrUpdateGridColumnConfig(user, gridId, columnIds);
    }

    @Override
    public void addOrUpdateGridColumnConfig(User user, String gridId, List<String> columnIds) {
        switch (gridId) {
            case REQUIREMENT_SEARCH:
                columnIds.add(ColumnIds.NAME.getColumnId());
                columnIds.add(ColumnIds.REQ_VERSION_BOUND_TO_ITEM.getColumnId());
                break;
            case TEST_CASE_SEARCH:
                columnIds.add(ColumnIds.NAME.getColumnId());
                break;
            case ITPI_SEARCH:
                columnIds.add(ColumnIds.LABEL.getColumnId());
                columnIds.add(ColumnIds.EXECUTION_STATUS.getColumnId());
                break;
            default:
                columnIds.add("");
                break;
        }

        GridColumnDisplayReference reference =
                gridColumnDisplayReferenceDao.findByUserAndGridId(user, gridId);
        if (reference == null) {
            reference = new GridColumnDisplayReference();
            reference.setUser(user);
            reference.setGridId(gridId);
            reference = gridColumnDisplayReferenceDao.save(reference);
            createNewConfigurations(reference, columnIds);
        } else {
            Long referenceId = reference.getId();
            gridColumnDisplayConfigurationDao.deleteByGridColumnDisplayReferenceId(referenceId);
            createNewConfigurations(reference, columnIds);
        }
    }

    /**
     * Deletes the grid configuration set by the current user for the grid whose id is passed as an
     * argument.
     *
     * @param gridId
     */
    @Override
    public void resetGridColumnConfig(String gridId) {
        User user = userAccountService.findCurrentUser();
        gridId = setGridConfigId(gridId);
        Long referenceId = gridColumnDisplayReferenceDao.findByUserAndGridId(user, gridId).getId();
        gridColumnDisplayConfigurationDao.deleteByGridColumnDisplayReferenceId(referenceId);
        deleteExistingReference(user.getId(), referenceId);
    }

    /**
     * Looks for the column ids contained in the configuration set by the current user for the grid id
     * passed as an arguement.
     *
     * @param gridId
     * @return an empty list of strings if no configuration has been set, otherwise the list of active
     *     column ids.
     */
    @Override
    public List<String> findActiveColumnIdsForUser(String gridId) {
        User user = userAccountService.findCurrentUser();
        gridId = setGridConfigId(gridId);
        GridColumnDisplayReference gridConfig =
                gridColumnDisplayReferenceDao.findByUserAndGridId(user, gridId);
        if (gridConfig != null) {
            List<String> activeColumnIds =
                    findActiveColumnIdsByGridColumnDisplayReferenceId(gridConfig.getId());
            activeColumnIds.addAll(MANDATORY_COLUMNS);
            return activeColumnIds;
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public void addOrUpdatePreferenceForCurrentUserWithProjectId(
            String gridId, List<String> columnIds, Long projectId) {
        User user = userAccountService.findCurrentUser();
        addOrUpdateGridColumnConfigWithProjectId(user, gridId, columnIds, projectId);
    }

    @Override
    public List<String> findActiveColumnIdsForUserWithProjectId(String gridId, Long projectId) {
        User user = userAccountService.findCurrentUser();
        GridColumnDisplayReference gridConfig =
                gridColumnDisplayReferenceDao.findByUserAndGridIdAndProjectId(user, gridId, projectId);
        if (gridConfig != null) {
            List<String> activeColumnIds =
                    findActiveColumnIdsByGridColumnDisplayReferenceId(gridConfig.getId());
            activeColumnIds.addAll(MANDATORY_COLUMNS);
            return activeColumnIds;
        } else {
            return new ArrayList<>();
        }
    }

    /**
     * Deletes all grid configurations for a given project.
     *
     * @param projectId : the project id for which to delete all linked grid configurations.
     */
    @Override
    public void deleteColumnConfigForProject(Long projectId) {
        gridColumnDisplayReferenceDao.deleteByProjectId(projectId);
    }

    @Override
    public void resetGridColumnConfigWithProjectId(String gridId, String projectId) {
        User user = userAccountService.findCurrentUser();
        Long referenceId =
                gridColumnDisplayReferenceDao
                        .findByUserAndGridIdAndProjectId(user, gridId, Long.valueOf(projectId))
                        .getId();
        gridColumnDisplayConfigurationDao.deleteByGridColumnDisplayReferenceId(referenceId);
        deleteExistingReference(user.getId(), referenceId);
    }

    private void addOrUpdateGridColumnConfigWithProjectId(
            User user, String gridId, List<String> columnIds, Long projectId) {
        switch (gridId) {
            case ITERATION_TEST_PLAN, TEST_SUITE_TEST_PLAN:
                columnIds.add(ColumnIds.TEST_CASE_NAME.getColumnId());
                columnIds.add(ColumnIds.LAST_EXECUTED_ON.getColumnId());
                columnIds.add(ColumnIds.EXECUTION_STATUS.getColumnId());
                break;
            case CAMPAIGN_TEST_PLAN:
                columnIds.add(ColumnIds.TEST_CASE_NAME.getColumnId());
                break;
            case ITERATION_AUTOMATED_SUITE, TEST_SUITE_AUTOMATED_SUITE:
                columnIds.add(ColumnIds.CREATED_ON.getColumnId());
                columnIds.add(ColumnIds.EXECUTION_STATUS.getColumnId());
                columnIds.add(ColumnIds.EXECUTION_DETAILS.getColumnId());
                columnIds.add(ColumnIds.EXECUTION_REPORTS.getColumnId());
                break;
            default:
                break;
        }

        GridColumnDisplayReference reference =
                gridColumnDisplayReferenceDao.findByUserAndGridIdAndProjectId(user, gridId, projectId);

        if (reference == null) {
            reference = new GridColumnDisplayReference();
            reference.setUser(user);
            reference.setGridId(gridId);
            reference.setProjectId(projectId);
            reference = gridColumnDisplayReferenceDao.save(reference);

            createNewConfigurations(reference, columnIds);
        } else {
            Long referenceId = reference.getId();
            gridColumnDisplayConfigurationDao.deleteByGridColumnDisplayReferenceId(referenceId);
            createNewConfigurations(reference, columnIds);
        }
    }

    /**
     * Retrieves a list of strings representing the columns' ids the user wants to be displayed for a
     * particular grid.
     *
     * @param referenceId : a Long extracted from the grid_column_display_configuration, referring to
     *     a combination of user-grid
     * @return a list of active_column_ids
     */
    public List<String> findActiveColumnIdsByGridColumnDisplayReferenceId(Long referenceId) {
        return dsl.select(GRID_COLUMN_DISPLAY_CONFIGURATION.ACTIVE_COLUMN_ID)
                .from(GRID_COLUMN_DISPLAY_CONFIGURATION)
                .where(GRID_COLUMN_DISPLAY_CONFIGURATION.GCDR_ID.eq(referenceId))
                .fetch(GRID_COLUMN_DISPLAY_CONFIGURATION.ACTIVE_COLUMN_ID, String.class);
    }

    private void deleteExistingReference(Long userId, Long id) {
        GridColumnDisplayReference gridColumnDisplayReference =
                gridColumnDisplayReferenceDao.findByUserIdAndId(userId, id);
        gridColumnDisplayReferenceDao.delete(gridColumnDisplayReference);
    }

    private void createNewConfigurations(
            GridColumnDisplayReference reference, List<String> columnIds) {
        for (String columnId : columnIds) {
            GridColumnDisplayConfiguration config = new GridColumnDisplayConfiguration();
            config.setGridColumnDisplayReference(reference);
            config.setActiveColumnId(columnId);
            gridColumnDisplayConfigurationDao.save(config);
        }
    }

    /**
     * The tc-by-requirement grid and the test-case-search grid share the same configuration. There is
     * only one configuration set for both grids. The grid_id in the grid_colum_display_reference is
     * 'test-case-search'.
     *
     * @param gridId
     * @return a String : "test-case-search" if "tc-by-requirement" is the input argument else gridId
     */
    private String setGridConfigId(String gridId) {
        if (gridId.equals(TC_BY_REQUIREMENT)) {
            return TEST_CASE_SEARCH;
        }
        return gridId;
    }

    @Override
    public Long getCufIdFromCufColumnName(String columnName) {
        return Long.valueOf(columnName.replace(CUF_COLUMN_PREFIX_FOR_CONFIGURATIONS, ""));
    }

    @Override
    public String reformatSimpleDateForExport(Date date) {
        return date == null ? "" : DateUtils.formatIso8601Date(date);
    }

    @Override
    public String reformatMultipleDateForExport(String concatenatedDates) {
        List<String> formattedDates = new ArrayList<>();
        if (org.apache.commons.lang3.StringUtils.isNotBlank(concatenatedDates)) {
            String[] dateStrings = concatenatedDates.split("\\|");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            for (String dateString : dateStrings) {
                try {
                    Date date = dateFormat.parse(dateString);
                    formattedDates.add(reformatSimpleDateForExport(date));
                } catch (Exception e) {
                    throw new IllegalArgumentException("Date has wrong format: " + dateString, e);
                }
            }
        }
        return String.join(", ", formattedDates);
    }

    @Override
    public String reformatLabelForExport(String concatenatedLabel) {
        if (org.apache.commons.lang3.StringUtils.isNotBlank(concatenatedLabel)) {
            return concatenatedLabel.replace("|", ", ");
        } else {
            return "";
        }
    }

    @Override
    public String nullSafeValue(ExportModel.CustomField customField) {
        String value = customField.getValue();
        return value == null ? "" : value;
    }

    @Override
    public ExportModel.CustomField findCufById(
            List<ExportModel.CustomField> cufs, Long cufId, Integer index) {
        Optional<ExportModel.CustomField> optionalCuf =
                cufs.stream().filter(cuf -> cuf.getCufId().equals(cufId)).findAny();

        return optionalCuf.orElse(null);
    }

    @Override
    public void registerCuf(
            Row headerRow, int cIdx, String cufColumnName, Map<Long, Integer> cufColumnsByCode) {
        Long cufId = getCufIdFromCufColumnName(cufColumnName);
        Optional<CustomField> cuf = customFieldDao.findById(cufId);

        if (cuf.isPresent()) {
            CustomField customField = cuf.get();
            headerRow.createCell(cIdx).setCellValue(customField.getLabel());
            cufColumnsByCode.put(customField.getId(), cIdx);
        }
    }
}
