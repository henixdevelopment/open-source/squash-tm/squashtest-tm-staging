/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.squashtest.tm.api.plugin.UsedInPlugin;
import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.domain.requirement.RequirementSyncExtender;

/**
 * @author aguilhem
 */
public interface CustomRequirementSyncExtenderDao {

    /**
     * Retrieve remote issue key of {@link
     * org.squashtest.tm.domain.requirement.RequirementSyncExtender} synchronized via the {@link
     * BugTracker} with the given serverUrl and verified by{@link
     * org.squashtest.tm.domain.testcase.TestCase} with the given testCaseId
     *
     * @param serverUrl
     * @param testCaseId
     * @return
     */
    List<String> findAllRemoteReqIdVerifiedByATestCaseByServerUrl(String serverUrl, Long testCaseId);

    @UsedInPlugin("Xsquash4Jira & Xsquash4GitLab")
    Map<String, RequirementSyncExtender> findByRemoteKeysAndSyncId(
            Collection<String> remoteIds, Long remoteSyncId);

    @UsedInPlugin("RedmineSync & JiraSync")
    Map<String, RequirementSyncExtender> findByRemoteKeysAndProjectId(
            Collection<String> remoteIds, Long projectId);

    @UsedInPlugin("Xsquash4Jira")
    Long findRequirementIdByIssueKeyAndRemoteSyncId(String issueKey, Long remoteSyncId);

    @UsedInPlugin("Xsquash4Jira & Xsquash4GitLab")
    void resetRemoteLastUpdated(Long syncId);
}
