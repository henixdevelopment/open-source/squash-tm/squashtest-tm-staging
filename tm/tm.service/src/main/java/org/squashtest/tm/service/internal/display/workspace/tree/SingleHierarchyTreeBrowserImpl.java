/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.workspace.tree;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.service.display.workspace.tree.SingleHierarchyTreeBrowser;
import org.squashtest.tm.service.display.workspace.tree.TreeNodeCollectorService;
import org.squashtest.tm.service.internal.repository.display.ProjectFilterDisplayDao;
import org.squashtest.tm.service.internal.repository.display.SingleHierarchyTreeBrowserDao;
import org.squashtest.tm.service.internal.repository.display.TreeBrowserDao;
import org.squashtest.tm.service.project.CustomProjectFinder;
import org.squashtest.tm.service.user.UserAccountService;

@Service
@Transactional(readOnly = true)
public class SingleHierarchyTreeBrowserImpl extends AbstractTreeBrowserImpl
        implements SingleHierarchyTreeBrowser {

    private final SingleHierarchyTreeBrowserDao singleHierarchyTreeBrowserDao;

    public SingleHierarchyTreeBrowserImpl(
            TreeNodeCollectorService treeNodeCollectorService,
            UserAccountService userAccountService,
            CustomProjectFinder projectFinder,
            ProjectFilterDisplayDao projectFilterDao,
            SingleHierarchyTreeBrowserDao singleHierarchyTreeBrowserDao) {
        super(treeNodeCollectorService, userAccountService, projectFinder, projectFilterDao);
        this.singleHierarchyTreeBrowserDao = singleHierarchyTreeBrowserDao;
    }

    @Override
    protected TreeBrowserDao getTreeBrowserDao() {
        return singleHierarchyTreeBrowserDao;
    }
}
