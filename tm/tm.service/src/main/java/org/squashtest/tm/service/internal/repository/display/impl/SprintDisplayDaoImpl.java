/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_EXECUTION_STEPS;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_SESSION_OVERVIEW;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REMOTE_SYNCHRONISATION;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.TEST_PLAN_ITEM;

import java.util.List;
import java.util.Map;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.domain.campaign.SprintStatus;
import org.squashtest.tm.service.internal.display.dto.sprint.SprintDto;
import org.squashtest.tm.service.internal.repository.display.SprintDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

@Repository
public class SprintDisplayDaoImpl implements SprintDisplayDao {
    private final DSLContext dsl;

    public SprintDisplayDaoImpl(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public SprintDto getSprintDtoById(long sprintId) {
        return dsl.select(
                        CAMPAIGN_LIBRARY_NODE.CLN_ID.as(RequestAliasesConstants.ID),
                        CAMPAIGN_LIBRARY_NODE.ATTACHMENT_LIST_ID,
                        CAMPAIGN_LIBRARY_NODE.DESCRIPTION,
                        CAMPAIGN_LIBRARY_NODE.NAME,
                        CAMPAIGN_LIBRARY_NODE.PROJECT_ID,
                        CAMPAIGN_LIBRARY_NODE.CREATED_BY,
                        CAMPAIGN_LIBRARY_NODE.CREATED_ON,
                        CAMPAIGN_LIBRARY_NODE.LAST_MODIFIED_BY,
                        CAMPAIGN_LIBRARY_NODE.LAST_MODIFIED_ON,
                        REMOTE_SYNCHRONISATION.KIND.as("synchronisationKind"),
                        PROJECT.CL_ID.as(RequestAliasesConstants.CAMPAIGN_LIBRARY_ID),
                        SPRINT.REFERENCE,
                        SPRINT.STATUS,
                        SPRINT.REMOTE_STATE,
                        SPRINT.START_DATE,
                        SPRINT.END_DATE)
                .from(CAMPAIGN_LIBRARY_NODE)
                .innerJoin(PROJECT)
                .on(PROJECT.PROJECT_ID.eq(CAMPAIGN_LIBRARY_NODE.PROJECT_ID))
                .innerJoin(SPRINT)
                .on(SPRINT.CLN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .leftJoin(REMOTE_SYNCHRONISATION)
                .on(REMOTE_SYNCHRONISATION.REMOTE_SYNCHRONISATION_ID.eq(SPRINT.REMOTE_SYNCHRONISATION_ID))
                .where(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(sprintId))
                .fetchOneInto(SprintDto.class);
    }

    @Override
    public SprintStatus getSprintStatusByTestPlanItemId(long testPlanItemId) {
        return dsl.select(SPRINT.STATUS)
                .from(SPRINT)
                .innerJoin(SPRINT_REQ_VERSION)
                .on(SPRINT.CLN_ID.eq(SPRINT_REQ_VERSION.SPRINT_ID))
                .innerJoin(TEST_PLAN_ITEM)
                .on(SPRINT_REQ_VERSION.TEST_PLAN_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ID))
                .where(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID.eq(testPlanItemId))
                .fetchOneInto(SprintStatus.class);
    }

    @Override
    public SprintStatus getSprintStatusByExecutionId(long executionId) {
        return dsl.select(SPRINT.STATUS)
                .from(SPRINT)
                .innerJoin(SPRINT_REQ_VERSION)
                .on(SPRINT.CLN_ID.eq(SPRINT_REQ_VERSION.SPRINT_ID))
                .innerJoin(TEST_PLAN_ITEM)
                .on(SPRINT_REQ_VERSION.TEST_PLAN_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ID))
                .innerJoin(EXECUTION)
                .on(EXECUTION.TEST_PLAN_ITEM_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID))
                .where(EXECUTION.EXECUTION_ID.eq(executionId))
                .fetchOneInto(SprintStatus.class);
    }

    @Override
    public SprintStatus getSprintStatusByExecutionStepId(Long executionStepId) {
        return dsl.select(SPRINT.STATUS)
                .from(SPRINT)
                .innerJoin(SPRINT_REQ_VERSION)
                .on(SPRINT.CLN_ID.eq(SPRINT_REQ_VERSION.SPRINT_ID))
                .innerJoin(TEST_PLAN_ITEM)
                .on(SPRINT_REQ_VERSION.TEST_PLAN_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ID))
                .innerJoin(EXECUTION)
                .on(EXECUTION.TEST_PLAN_ITEM_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID))
                .innerJoin(EXECUTION_EXECUTION_STEPS)
                .on(EXECUTION.EXECUTION_ID.eq(EXECUTION_EXECUTION_STEPS.EXECUTION_ID))
                .where(EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ID.eq(executionStepId))
                .fetchOneInto(SprintStatus.class);
    }

    @Override
    public Map<Long, List<Long>> findExecutionIdsBySprintIds(List<Long> sprintIds) {
        return dsl.select(SPRINT_REQ_VERSION.SPRINT_ID, EXECUTION.EXECUTION_ID)
                .from(EXECUTION)
                .leftJoin(TEST_PLAN_ITEM)
                .on(EXECUTION.TEST_PLAN_ITEM_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID))
                .leftJoin(SPRINT_REQ_VERSION)
                .on(TEST_PLAN_ITEM.TEST_PLAN_ID.eq(SPRINT_REQ_VERSION.TEST_PLAN_ID))
                .where(SPRINT_REQ_VERSION.SPRINT_ID.in(sprintIds))
                .fetchGroups(SPRINT_REQ_VERSION.SPRINT_ID, EXECUTION.EXECUTION_ID);
    }

    @Override
    public List<NamedReference> findNamedReferences(List<Long> clnIds) {
        return dsl.select(
                        CAMPAIGN_LIBRARY_NODE.CLN_ID.as(RequestAliasesConstants.ID), CAMPAIGN_LIBRARY_NODE.NAME)
                .from(CAMPAIGN_LIBRARY_NODE)
                .join(SPRINT)
                .on(SPRINT.CLN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                .where(CAMPAIGN_LIBRARY_NODE.CLN_ID.in(clnIds))
                .fetchInto(NamedReference.class);
    }

    @Override
    public int countTestPlanItems(long sprintId, String userToRestrictTo) {
        Integer count =
                dsl.selectCount()
                        .from(TEST_PLAN_ITEM)
                        .innerJoin(SPRINT_REQ_VERSION)
                        .on(SPRINT_REQ_VERSION.TEST_PLAN_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ID))
                        .innerJoin(SPRINT)
                        .on(SPRINT.CLN_ID.eq(SPRINT_REQ_VERSION.SPRINT_ID))
                        .leftJoin(CORE_USER)
                        .on(CORE_USER.PARTY_ID.eq(TEST_PLAN_ITEM.ASSIGNEE_ID))
                        .leftJoin(EXPLORATORY_SESSION_OVERVIEW)
                        .on(EXPLORATORY_SESSION_OVERVIEW.TEST_PLAN_ITEM_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID))
                        .where(deduceCondition(userToRestrictTo, sprintId))
                        .fetchOneInto(int.class);

        return count != null ? count : 0;
    }

    @Override
    public SprintStatus getSprintStatusBySprintReqVersionId(long sprintReqVersionId) {
        return dsl.select(SPRINT.STATUS)
                .from(SPRINT)
                .innerJoin(SPRINT_REQ_VERSION)
                .on(SPRINT.CLN_ID.eq(SPRINT_REQ_VERSION.SPRINT_ID))
                .where(SPRINT_REQ_VERSION.SPRINT_REQ_VERSION_ID.eq(sprintReqVersionId))
                .fetchOneInto(SprintStatus.class);
    }

    @Override
    public SprintStatus getSprintStatusBySessionOverviewId(long overviewId) {
        return dsl.select(SPRINT.STATUS)
                .from(SPRINT)
                .innerJoin(SPRINT_REQ_VERSION)
                .on(SPRINT_REQ_VERSION.SPRINT_ID.eq(SPRINT.CLN_ID))
                .innerJoin(TEST_PLAN_ITEM)
                .on(TEST_PLAN_ITEM.TEST_PLAN_ID.eq(SPRINT_REQ_VERSION.TEST_PLAN_ID))
                .innerJoin(EXPLORATORY_SESSION_OVERVIEW)
                .on(EXPLORATORY_SESSION_OVERVIEW.TEST_PLAN_ITEM_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID))
                .where(EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.eq(overviewId))
                .fetchOneInto(SprintStatus.class);
    }

    private Condition deduceCondition(String userToRestrictTo, long sprintId) {
        Condition condition = SPRINT.CLN_ID.eq(sprintId);
        if (userToRestrictTo == null) {
            return condition;
        } else {
            return condition.and(
                    CORE_USER
                            .LOGIN
                            .eq(userToRestrictTo)
                            .or(EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.isNotNull()));
        }
    }
}
