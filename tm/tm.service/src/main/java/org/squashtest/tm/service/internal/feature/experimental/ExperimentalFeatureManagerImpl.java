/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.feature.experimental;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.springframework.stereotype.Service;
import org.squashtest.tm.core.foundation.exception.FeatureFlagNotEnabledException;
import org.squashtest.tm.core.foundation.feature.ExperimentalFeature;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.service.feature.experimental.ExperimentalFeatureManager;

@Service
public class ExperimentalFeatureManagerImpl implements ExperimentalFeatureManager {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(ExperimentalFeatureManagerImpl.class);

    private final ExperimentalFeatureConfig configuration;

    public ExperimentalFeatureManagerImpl(ExperimentalFeatureConfig configuration) {
        this.configuration = configuration;
    }

    @Override
    public List<ExperimentalFeature> getEnabledFeatures() {
        return configuration.getExperimental().entrySet().stream()
                .filter(Map.Entry::getValue)
                .map(Map.Entry::getKey)
                .map(this::toExperimentalFeature)
                .filter(Objects::nonNull)
                .toList();
    }

    @Override
    public boolean isEnabled(ExperimentalFeature featureFlag) {
        return getEnabledFeatures().contains(featureFlag);
    }

    @Override
    public void checkIfEnabled(ExperimentalFeature featureFlag) {
        if (!isEnabled(featureFlag)) {
            throw new FeatureFlagNotEnabledException();
        }
    }

    private ExperimentalFeature toExperimentalFeature(String candidate) {
        try {
            // Replace hyphens with underscores and convert to uppercase
            return ExperimentalFeature.valueOf(candidate.toUpperCase().replace("-", "_"));
        } catch (IllegalArgumentException e) {
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("Unknown experimental feature: {}", candidate);
            }

            return null;
        }
    }
}
