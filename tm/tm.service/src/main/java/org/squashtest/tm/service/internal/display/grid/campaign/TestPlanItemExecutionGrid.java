/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.campaign;

import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_ISSUES_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.KEYWORD_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.SCRIPTED_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_PLAN;
import static org.squashtest.tm.jooq.domain.Tables.TEST_PLAN_ITEM;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.DATASET_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_ORDER;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_REFERENCE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.INFERRED_EXECUTION_MODE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ISSUE_COUNT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ITEM_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.KIND;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PROJECT_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TC_KIND;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_PLAN_ITEM_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.USER;

import java.util.Arrays;
import java.util.List;
import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.SelectHavingStep;
import org.jooq.SortField;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.domain.testcase.TestCaseKind;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;
import org.squashtest.tm.service.internal.display.grid.columns.LevelEnumColumn;

public class TestPlanItemExecutionGrid extends AbstractGrid {
    private final Long testPlanItemId;

    public TestPlanItemExecutionGrid(Long testPlanItemId) {
        this.testPlanItemId = testPlanItemId;
    }

    // TODO count blocking milestone when linked to campaign/iteration/test-suite
    @Override
    protected List<GridColumn> getColumns() {
        return Arrays.asList(
                new GridColumn(EXECUTION.EXECUTION_ID),
                new GridColumn(EXECUTION.EXECUTION_ORDER.as(EXECUTION_ORDER)),
                new GridColumn(PROJECT.PROJECT_ID.as(PROJECT_ID)),
                new GridColumn(EXECUTION.EXECUTION_MODE.as(INFERRED_EXECUTION_MODE)),
                new GridColumn(EXECUTION.REFERENCE.as(EXECUTION_REFERENCE)),
                new GridColumn(EXECUTION.NAME.as(EXECUTION_NAME)),
                new LevelEnumColumn(TestCaseImportance.class, EXECUTION.IMPORTANCE),
                new GridColumn(EXECUTION.DATASET_LABEL.as(DATASET_NAME)),
                new LevelEnumColumn(ExecutionStatus.class, EXECUTION.EXECUTION_STATUS),
                new GridColumn(EXECUTION.LAST_EXECUTED_BY.as(USER)),
                new GridColumn(EXECUTION.LAST_EXECUTED_ON),
                new GridColumn(countIssue().as(ISSUE_COUNT)),
                new GridColumn(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID.as(TEST_PLAN_ITEM_ID)),
                new GridColumn(getTestCaseKind().field(TC_KIND)));
    }

    /**
     * Count issues in Issue list linked to current execution
     *
     * @return number of issues
     */
    private Field<Integer> countIssue() {
        return DSL.selectCount()
                .from(EXECUTION_ISSUES_CLOSURE)
                .where(EXECUTION_ISSUES_CLOSURE.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                .asField(ISSUE_COUNT);
    }

    @Override
    protected Table<?> getTable() {
        final SelectHavingStep<?> testCaseKind = getTestCaseKind();

        return EXECUTION
                .innerJoin(TEST_PLAN_ITEM)
                .on(EXECUTION.TEST_PLAN_ITEM_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID))
                .innerJoin(TEST_PLAN)
                .on(TEST_PLAN_ITEM.TEST_PLAN_ID.eq(TEST_PLAN.TEST_PLAN_ID))
                .innerJoin(CAMPAIGN_LIBRARY)
                .on(TEST_PLAN.CL_ID.eq(CAMPAIGN_LIBRARY.CL_ID))
                .innerJoin(PROJECT)
                .on(PROJECT.CL_ID.eq(CAMPAIGN_LIBRARY.CL_ID))
                .where(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID.eq(testPlanItemId))
                .leftJoin(testCaseKind)
                .on(EXECUTION.EXECUTION_ID.eq(testCaseKind.field(ITEM_ID, Long.class)));
    }

    @Override
    protected Field<?> getIdentifier() {
        return EXECUTION.EXECUTION_ID;
    }

    @Override
    protected Field<?> getProjectIdentifier() {
        return PROJECT.PROJECT_ID;
    }

    @Override
    protected SortField<?> getDefaultOrder() {
        return EXECUTION.EXECUTION_ORDER.desc();
    }

    private SelectHavingStep<Record2<Long, String>> getTestCaseKind() {
        return DSL.select(
                        EXECUTION.EXECUTION_ID.as(ITEM_ID),
                        DSL.when(SCRIPTED_TEST_CASE.TCLN_ID.isNotNull(), TestCaseKind.GHERKIN.toString())
                                .when(KEYWORD_TEST_CASE.TCLN_ID.isNotNull(), TestCaseKind.KEYWORD.toString())
                                .when(
                                        EXPLORATORY_TEST_CASE.TCLN_ID.isNotNull(), TestCaseKind.EXPLORATORY.toString())
                                .otherwise(TestCaseKind.STANDARD.toString())
                                .as(KIND))
                .from(EXECUTION)
                .innerJoin(TEST_CASE)
                .on(EXECUTION.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .leftJoin(SCRIPTED_TEST_CASE)
                .on(TEST_CASE.TCLN_ID.eq(SCRIPTED_TEST_CASE.TCLN_ID))
                .leftJoin(KEYWORD_TEST_CASE)
                .on(TEST_CASE.TCLN_ID.eq(KEYWORD_TEST_CASE.TCLN_ID))
                .leftJoin(EXPLORATORY_TEST_CASE)
                .on(TEST_CASE.TCLN_ID.eq(EXPLORATORY_TEST_CASE.TCLN_ID))
                .groupBy(
                        EXECUTION.EXECUTION_ID,
                        SCRIPTED_TEST_CASE.TCLN_ID,
                        KEYWORD_TEST_CASE.TCLN_ID,
                        EXPLORATORY_TEST_CASE.TCLN_ID);
    }
}
