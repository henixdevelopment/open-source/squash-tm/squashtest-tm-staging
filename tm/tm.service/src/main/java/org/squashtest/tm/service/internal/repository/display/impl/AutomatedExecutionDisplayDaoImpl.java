/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_EXECUTION_FAILURE_DETAIL;
import static org.squashtest.tm.jooq.domain.Tables.FAILURE_DETAIL;

import java.util.List;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.display.dto.FailureDetailDto;
import org.squashtest.tm.service.internal.repository.display.AutomatedExecutionDisplayDao;

@Repository
public class AutomatedExecutionDisplayDaoImpl implements AutomatedExecutionDisplayDao {

    private final DSLContext dsl;

    public AutomatedExecutionDisplayDaoImpl(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public List<FailureDetailDto> findAllFailureDetailsByAutomatedExecutionExtenderId(
            Long executionExtenderId) {
        return dsl.select(FAILURE_DETAIL.FAILURE_DETAIL_ID, FAILURE_DETAIL.MESSAGE)
                .from(FAILURE_DETAIL)
                .innerJoin(AUTOMATED_EXECUTION_FAILURE_DETAIL)
                .on(
                        AUTOMATED_EXECUTION_FAILURE_DETAIL.FAILURE_DETAIL_ID.eq(
                                FAILURE_DETAIL.FAILURE_DETAIL_ID))
                .where(AUTOMATED_EXECUTION_FAILURE_DETAIL.EXECUTION_EXTENDER_ID.eq(executionExtenderId))
                .fetchInto(FailureDetailDto.class);
    }
}
