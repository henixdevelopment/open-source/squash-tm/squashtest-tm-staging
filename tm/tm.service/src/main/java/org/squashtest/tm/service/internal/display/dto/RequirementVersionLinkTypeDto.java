/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import javax.persistence.Column;

public class RequirementVersionLinkTypeDto {

    @Column(name = "TYPE_ID")
    private Long id;

    @Column(name = "ROLE_1")
    private String role;

    @Column(name = "ROLE_1_CODE")
    private String role1Code;

    @Column(name = "ROLE_2")
    private String role2;

    @Column(name = "ROLE_2_CODE")
    private String role2Code;

    @Column(name = "IS_DEFAULT")
    private boolean isDefault;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole1Code() {
        return role1Code;
    }

    public void setRole1Code(String role1Code) {
        this.role1Code = role1Code;
    }

    public String getRole2() {
        return role2;
    }

    public void setRole2(String role2) {
        this.role2 = role2;
    }

    public String getRole2Code() {
        return role2Code;
    }

    public void setRole2Code(String role2Code) {
        this.role2Code = role2Code;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }
}
