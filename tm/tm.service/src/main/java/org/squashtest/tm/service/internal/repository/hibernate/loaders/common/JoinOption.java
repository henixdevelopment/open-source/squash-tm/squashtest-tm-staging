/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate.loaders.common;

import java.util.Set;
import javax.persistence.TypedQuery;
import org.squashtest.tm.service.internal.repository.ParameterNames;

public class JoinOption<KEY, MAIN_AGGREGATE> extends AbstractHqlLoader<KEY, MAIN_AGGREGATE> {

    public JoinOption(HintOptions hintOptions, TypedQuery<MAIN_AGGREGATE> mainQuery) {
        super(hintOptions, mainQuery);
    }

    public JoinOption(TypedQuery<MAIN_AGGREGATE> mainQuery) {
        this(new HintOptions(), mainQuery);
    }

    public void fetch(Set<KEY> ids) {
        build().setParameter(ParameterNames.IDS, ids).getResultList();
    }

    public void addHints(Set<HintOption> hintOptions) {
        this.hintOptions.addAll(hintOptions);
    }
}
