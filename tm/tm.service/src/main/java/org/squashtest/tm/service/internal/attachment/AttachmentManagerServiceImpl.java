/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.attachment;

import static org.squashtest.tm.api.security.acls.Permissions.ATTACH;
import static org.squashtest.tm.api.security.acls.Permissions.MANAGE_PROJECT;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.AUTOMATED_EXECUTION_EXTENDER;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_EXECUTION_BY_ID;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.sql.rowset.serial.SerialBlob;
import org.apache.commons.io.IOUtils;
import org.hibernate.JDBCException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.attachment.Attachment;
import org.squashtest.tm.domain.attachment.AttachmentContent;
import org.squashtest.tm.domain.attachment.AttachmentHolder;
import org.squashtest.tm.domain.attachment.AttachmentList;
import org.squashtest.tm.domain.attachment.ExternalContentCoordinates;
import org.squashtest.tm.domain.campaign.SprintStatus;
import org.squashtest.tm.domain.execution.ConsumerForExploratoryExecution;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.execution.ExploratoryExecutionRunningState;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.testcase.TestCaseExecutionMode;
import org.squashtest.tm.exception.attachment.AttachmentException;
import org.squashtest.tm.exception.campaign.SprintClosedException;
import org.squashtest.tm.service.annotation.CheckBlockingMilestone;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.attachment.AttachmentManagerService;
import org.squashtest.tm.service.attachment.RawAttachment;
import org.squashtest.tm.service.attachment.UploadedData;
import org.squashtest.tm.service.audit.AuditModificationService;
import org.squashtest.tm.service.execution.ExploratoryExecutionService;
import org.squashtest.tm.service.internal.display.dto.AttachmentDto;
import org.squashtest.tm.service.internal.dto.UserDto;
import org.squashtest.tm.service.internal.repository.AttachmentContentDao;
import org.squashtest.tm.service.internal.repository.AttachmentDao;
import org.squashtest.tm.service.internal.repository.AttachmentListDao;
import org.squashtest.tm.service.internal.repository.MilestoneDao;
import org.squashtest.tm.service.internal.repository.ParameterNames;
import org.squashtest.tm.service.internal.repository.display.SprintDisplayDao;
import org.squashtest.tm.service.internal.repository.hibernate.loaders.EntityGraphQueryBuilder;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.user.UserAccountService;

/*
 *
 *
 * we can't secure the operations on attachments yet, because they delegate the security permissions
 * to the owning entity. The problem being that the ownership of an AttachmentList is one way : from the
 * entity to the list.
 *
 * Therefore we can't perform a permission check using the AttachmentList id alone, one would need to fetch
 * the owning entity back first. That would require additional work Dao-side.
 *
 * See task #102 on ci.squashtest.org/mantis
 *
 */
@Service("squashtest.tm.service.AttachmentManagerService")
@Transactional
public class AttachmentManagerServiceImpl implements AttachmentManagerService {
    private static final int EOF = -1;

    @PersistenceContext private EntityManager em;

    @Inject private AttachmentDao attachmentDao;

    @Inject private AttachmentListDao attachmentListDao;

    @Inject private AttachmentContentDao attachmentContentDao;

    @Inject private AttachmentRepository attachmentRepository;

    @Inject private AuditModificationService auditModificationService;

    @Inject private PermissionEvaluationService permissionEvaluationService;

    @Inject private UserAccountService userAccountService;

    @Inject private ExploratoryExecutionService exploratoryExecutionService;

    @Inject private MilestoneDao milestoneDao;

    @Inject private SprintDisplayDao sprintDisplayDao;

    @Override
    public AttachmentDto addAttachment(
            long attachmentListId,
            RawAttachment rawAttachment,
            EntityReference permissionOwner,
            EntityType holderType)
            throws IOException {
        milestoneDao.checkBlockingMilestonesOnAttachmentList(holderType, attachmentListId);
        checkExploratorySessionSpecificCase(permissionOwner);
        checkCanAttach(permissionOwner.getId(), permissionOwner.getType().getEntityClass().getName());
        checkParentSprintStatus(permissionOwner);
        return addAttachment(attachmentListId, rawAttachment, holderType);
    }

    private AttachmentDto addAttachmentWithoutPermissionCheck(
            long attachmentListId, RawAttachment rawAttachment, EntityReference entityReference)
            throws IOException {
        return addAttachment(attachmentListId, rawAttachment, entityReference.getType());
    }

    @Override
    public AttachmentDto addAttachment(
            long attachmentListId, RawAttachment rawAttachment, EntityType holderType)
            throws IOException {
        AttachmentList attachmentList = attachmentListDao.getOne(attachmentListId);

        AttachmentDto result = addAttachment(attachmentList, rawAttachment);

        auditModificationService.updateRelatedToAttachmentAuditableEntity(
                attachmentList.getId(), holderType);

        return result;
    }

    @Override
    public AttachmentDto addAttachment(AttachmentList attachmentList, RawAttachment rawAttachment)
            throws IOException {
        AttachmentContent content =
                getAttachmentRepository().createContent(rawAttachment, attachmentList.getId());

        Attachment attachment = new Attachment();

        attachmentList.addAttachment(attachment);
        attachment.setContent(content);
        attachment.setAddedOn(new Date());
        attachment.setName(rawAttachment.getName());
        attachment.setSize(rawAttachment.getSizeInBytes());
        attachmentDao.save(attachment);

        return new AttachmentDto(attachment);
    }

    @Override
    public AttachmentDto updateAttachmentContent(Attachment attachment, InputStream newContent)
            throws IOException {

        try {
            AttachmentContent content = attachment.getContent();
            byte[] byteArray = newContent.readAllBytes();
            Blob newBlob = new SerialBlob(byteArray);
            content.setContent(newBlob);
            attachment.setContent(content);
            attachment.setLastModifiedOn(new Date());
            attachmentDao.save(attachment);
            return new AttachmentDto(attachment);
        } catch (SQLException e) {
            throw new JDBCException("Cannot read blob property as a stream", e);
        }
    }

    private void checkExploratorySessionSpecificCase(EntityReference permissionOwner) {
        if (EntityType.EXECUTION.equals(permissionOwner.getType())) {
            Execution execution =
                    new EntityGraphQueryBuilder<>(em, Execution.class, FIND_EXECUTION_BY_ID)
                            .addSubGraph(AUTOMATED_EXECUTION_EXTENDER)
                            .execute(Map.of(ParameterNames.ID, permissionOwner.getId()));
            boolean isExploratorySession =
                    TestCaseExecutionMode.EXPLORATORY.equals(execution.getExecutionMode());
            if (isExploratorySession) {
                checkExploratorySessionRunningState(execution);
            }
        }
    }

    private void checkParentSprintStatus(EntityReference permissionOwner) {
        if (EntityType.EXECUTION.equals(permissionOwner.getType())) {
            SprintStatus sprintStatus =
                    sprintDisplayDao.getSprintStatusByExecutionId(permissionOwner.getId());
            checkSprintStatus(sprintStatus);
        } else if (EntityType.EXPLORATORY_SESSION_OVERVIEW.equals(permissionOwner.getType())) {
            SprintStatus sprintStatus =
                    sprintDisplayDao.getSprintStatusBySessionOverviewId(permissionOwner.getId());
            checkSprintStatus(sprintStatus);
        } else if (EntityType.SPRINT.equals(permissionOwner.getType())) {
            SprintStatus sprintStatus =
                    sprintDisplayDao.getSprintDtoById(permissionOwner.getId()).getStatus();
            checkSprintStatus(sprintStatus);
        }
    }

    private void checkSprintStatus(SprintStatus sprintStatus) {
        if (SprintStatus.CLOSED.equals(sprintStatus)) {
            throw new SprintClosedException();
        }
    }

    private AttachmentRepository getAttachmentRepository() {
        return attachmentRepository;
    }

    @Override
    public Attachment findAttachment(Long attachmentId) throws AccessDeniedException {
        checkUserPermissionsOnProject(attachmentId);
        return findById(attachmentId);
    }

    private List<Attachment> findAttachmentsForDeletion(List<Long> attachmentIds) {
        return attachmentDao.findAttachmentsForDeletion(attachmentIds);
    }

    @Override
    public void checkReadPermissionOnProjectByAttachmentListId(Long attachmentListId) {
        UserDto user = userAccountService.findCurrentUserDto();
        if (!user.isAdmin()) {
            AttachmentHolder holder = attachmentListDao.findAttachmentHolder(attachmentListId);
            Long projectId = holder.getAttachmentHolderProjectId();
            PermissionsUtils.checkPermission(
                    permissionEvaluationService,
                    Collections.singletonList(projectId),
                    Permissions.READ.name(),
                    Project.class.getName());
        }
    }

    private Attachment findById(Long attachmentId) {
        return attachmentDao.getReferenceById(attachmentId);
    }

    private void checkUserPermissionsOnProject(Long attachmentId) throws AccessDeniedException {
        Long attachmentListId = attachmentListDao.findAttachmentListIdByAttachmentId(attachmentId);

        if (Objects.isNull(attachmentListId)) {
            final String accessDenied =
                    "Unauthorized access or non-existent attachment: " + attachmentId.toString();
            throw new IllegalArgumentException(accessDenied);
        }

        checkReadPermissionOnProjectByAttachmentListId(attachmentListId);
    }

    @Override
    public void removeListOfAttachments(
            @Id long attachmentListId,
            List<Long> attachmentIds,
            EntityReference permissionOwner,
            EntityType holderType)
            throws AccessDeniedException {
        milestoneDao.checkBlockingMilestonesOnAttachmentList(holderType, attachmentListId);
        checkExploratorySessionSpecificCase(permissionOwner);
        checkCanAttach(permissionOwner.getId(), permissionOwner.getType().getEntityClass().getName());
        checkParentSprintStatus(permissionOwner);

        List<ExternalContentCoordinates> externalContentCoordinates = new ArrayList<>();

        List<Attachment> attachments = findAttachmentsForDeletion(attachmentIds);
        for (Attachment attachment : attachments) {
            Long attachmentContentId = attachment.getContent().getId();
            externalContentCoordinates.add(
                    new ExternalContentCoordinates(attachmentListId, attachmentContentId));
        }

        attachmentDao.deleteAllByIdInBatch(attachmentIds);
        deleteContents(externalContentCoordinates);

        auditModificationService.updateRelatedToAttachmentAuditableEntity(attachmentListId, holderType);
    }

    private void checkExploratorySessionRunningState(Execution execution) {
        execution.accept(
                new ConsumerForExploratoryExecution(
                        exploratoryExecution -> {
                            ExploratoryExecutionRunningState currentState =
                                    exploratoryExecutionService.findExploratoryExecutionRunningState(
                                            exploratoryExecution.getId());
                            exploratoryExecutionService.checkSessionIsNotStopped(currentState);
                        }));
    }

    @Override
    @CheckBlockingMilestone(entityType = Attachment.class)
    public void renameAttachment(@Id long attachmentId, String newName) {
        Attachment attachment = attachmentDao.getReferenceById(attachmentId);
        AttachmentHolder holder =
                attachmentListDao.findAttachmentHolder(attachment.getAttachmentList().getId());
        EntityReference entityRef = holder.toEntityReference();
        checkCanAttach(entityRef.getId(), entityRef.getType().getEntityClass().getName());
        attachment.setShortName(newName);
    }

    @Override
    public Page<Attachment> findPagedAttachments(AttachmentHolder attached, Pageable pas) {
        return findPagedAttachments(attached.getAttachmentList().getId(), pas);
    }

    private Page<Attachment> findPagedAttachments(long attachmentListId, Pageable pageable) {
        return attachmentDao.findAllAttachmentsPagined(attachmentListId, pageable);
    }

    /**
     * @see org.squashtest.tm.service.attachment.AttachmentManagerService#writeContent(long,
     *     OutputStream)
     */
    @Override
    public void writeContent(long attachmentId, OutputStream outStream) throws IOException {
        InputStream is = getAttachmentRepository().getContentStream(attachmentId);

        int readByte;

        do {
            readByte = is.read();

            if (readByte != EOF) {
                outStream.write(readByte);
            }
        } while (readByte != EOF);

        is.close();
    }

    @Override
    public void copyContent(Attachment attachment) {
        getAttachmentRepository().copyContent(attachment);
    }

    @Override
    public void copyContentsOnExternalRepository(AttachmentHolder attachmentHolder) {
        em.flush();
        AttachmentList attachmentList = attachmentHolder.getAttachmentList();
        for (Attachment attachment : attachmentList.getAllAttachments()) {
            copyContent(attachment);
        }
    }

    @Override
    public void batchCopyContentsOnExternalRepository(List<AttachmentHolder> attachmentHolders) {
        em.flush();
        for (AttachmentHolder attachmentHolder : attachmentHolders) {
            AttachmentList attachmentList = attachmentHolder.getAttachmentList();
            for (Attachment attachment : attachmentList.getAllAttachments()) {
                copyContent(attachment);
            }
        }
    }

    private void removeContentFromFileSystem(long attachmentListId, long attachmentContentId) {
        attachmentRepository.removeContent(attachmentListId, attachmentContentId);
    }

    @Override
    public List<ExternalContentCoordinates> getListIDbyContentIdForAttachmentLists(
            List<Long> attachmentsList) {
        return attachmentContentDao.getListPairContentIDListIDFromAttachmentLists(attachmentsList);
    }

    @Override
    public void deleteContents(List<ExternalContentCoordinates> contentIdListIdList) {
        List<Long> contentIds = new ArrayList<>();

        for (ExternalContentCoordinates coord : contentIdListIdList) {
            contentIds.add(coord.getContentId());
        }

        // remove Db Orphans
        removeOrphanAttachmentContents(contentIds);

        // remove from FileSystem
        if (attachmentRepository
                .getClass()
                .getSimpleName()
                .contains("FileSystemAttachmentRepository")) {
            for (ExternalContentCoordinates externalCoord : contentIdListIdList) {
                removeContentFromFileSystem(
                        externalCoord.getAttachmentListId(), externalCoord.getContentId());
            }
        }
    }

    private void removeOrphanAttachmentContents(List<Long> contentIds) {
        if (!contentIds.isEmpty()) {
            Set<Long> notOrphans = attachmentContentDao.findNotOrphanAttachmentContent(contentIds);
            contentIds.removeAll(notOrphans);
            if (!contentIds.isEmpty()) {
                attachmentContentDao.deleteByIds(contentIds);
            }
        }
    }

    @Override
    public void removeAttachmentsAndLists(List<Long> attachmentListIds) {
        if (!attachmentListIds.isEmpty()) {
            Set<Long> attachmentIds = attachmentDao.findAllAttachmentsFromLists(attachmentListIds);
            if (!attachmentIds.isEmpty()) {
                attachmentDao.removeAllAttachments(attachmentIds);
            }
            attachmentDao.removeAllAttachmentsLists(attachmentListIds);
        }
    }

    @Override
    public List<Long> getAttachmentsListsFromRequirementFolders(
            List<Long> requirementLibraryNodeIds) {
        return attachmentDao.findAttachmentsListsFromRequirementFolder(requirementLibraryNodeIds);
    }

    @Override
    public List<ExternalContentCoordinates> getListPairContentIDListIDForExecutionSteps(
            Collection<ExecutionStep> executionSteps) {
        List<Long> executionStepsIds = new ArrayList<>();
        for (ExecutionStep executionStep : executionSteps) {
            executionStepsIds.add(executionStep.getId());
        }
        return attachmentDao.getListPairContentIDListIDForExecutionSteps(executionStepsIds);
    }

    @Override
    public List<ExternalContentCoordinates> getListPairContentIDListIDForExecutionIds(
            List<Long> executionStepsIds) {
        return attachmentDao.getListPairContentIDListIDForExecutions(executionStepsIds);
    }

    @Override
    public List<ExternalContentCoordinates> getListPairContentIDListIDForAutomatedSuiteIds(
            List<String> automatedSuiteIds) {
        return attachmentDao.getListPairContentIDListIDForAutomatedSuiteIds(automatedSuiteIds);
    }

    @Override
    public String handleRichTextAttachments(String html, AttachmentList attachmentList) {
        Document document = Jsoup.parse(html);
        Elements elements = document.select("img");
        if (elements.isEmpty()) {
            return html;
        }

        elements.forEach(
                element -> {
                    String imageSrc = element.attr("src");
                    if (imageIsBase64(imageSrc)) {
                        String imageName = getImageName(element, imageSrc);
                        addAttachmentFromBase64(attachmentList, element, imageSrc, imageName);
                    }
                });

        return document.body().html();
    }

    @Override
    public String copyAttachmentsFromRichText(
            String html,
            Long attachmentListId,
            Long copiedAttachmentListId,
            EntityReference entityReference) {
        Document document = Jsoup.parse(html);
        Elements elements = document.select("img");

        if (elements.isEmpty()) {
            return html;
        }

        elements.forEach(
                element -> {
                    String imageSrc = element.attr("src");

                    if (imageIsAttachment(imageSrc, copiedAttachmentListId)) {
                        processCopyAttachmentImage(element, imageSrc, attachmentListId, entityReference);
                    }
                });

        return document.body().html();
    }

    @Override
    public String updateRichTextUrlsOnEntityCopy(
            AttachmentList sourceAttachmentList, AttachmentList copyAttachmentList, String richText) {
        Long sourceAttachmentListId = sourceAttachmentList.getId();
        String attachmentListUrl =
                String.format("/backend/attach-list/%d/attachments/download/", sourceAttachmentListId);
        if (richText != null && richText.contains(attachmentListUrl)) {
            String currentRichText = richText;
            StringBuilder richTextBuilder = new StringBuilder(richText);
            sourceAttachmentList.getAllAttachments().stream()
                    .filter(
                            sourceAttachment ->
                                    isSourcePresent(sourceAttachment, sourceAttachmentListId, currentRichText))
                    .forEach(
                            sourceAttachment ->
                                    copyAttachmentList.getAllAttachments().stream()
                                            .filter(
                                                    copyAttachment ->
                                                            isTheCopiedAttachment(
                                                                    sourceAttachment.getId(), copyAttachment.getId()))
                                            .forEach(
                                                    copyAttachment ->
                                                            replaceSourceUrl(
                                                                    sourceAttachment,
                                                                    copyAttachment,
                                                                    attachmentListUrl,
                                                                    copyAttachmentList,
                                                                    richTextBuilder)));
            richText = richTextBuilder.toString();
        }
        return richText;
    }

    @Override
    public void removeAttachmentsAndContents(
            List<Long> attachmentListIds, List<ExternalContentCoordinates> externalContentCoordinates) {
        Set<Long> attachmentIds = attachmentDao.findAllAttachmentsFromLists(attachmentListIds);
        if (!attachmentIds.isEmpty()) {
            attachmentDao.removeAllAttachments(attachmentIds);
        }

        deleteContents(externalContentCoordinates);
    }

    private void replaceSourceUrl(
            Attachment sourceAttachment,
            Attachment copyAttachment,
            String baseSourceUrl,
            AttachmentList copyAttachmentList,
            StringBuilder richTextBuilder) {
        String sourceUrl = baseSourceUrl + sourceAttachment.getId();
        String copyURL =
                String.format(
                        "/backend/attach-list/%d/attachments/download/%d",
                        copyAttachmentList.getId(), copyAttachment.getId());
        int startIndex = richTextBuilder.indexOf(sourceUrl);
        if (startIndex != -1) {
            richTextBuilder.replace(startIndex, startIndex + sourceUrl.length(), copyURL);
        }
    }

    private static boolean isSourcePresent(
            Attachment sourceAttachment, Long sourceAttachmentListId, String richText) {
        String originalURL =
                String.format(
                        "/backend/attach-list/%d/attachments/download/%d",
                        sourceAttachmentListId, sourceAttachment.getId());
        return richText.contains(originalURL);
    }

    private boolean isTheCopiedAttachment(Long sourceAttachmentId, Long copyAttachmentId) {
        try {
            return IOUtils.contentEquals(
                    attachmentRepository.getContentStream(sourceAttachmentId),
                    attachmentRepository.getContentStream(copyAttachmentId));
        } catch (IOException e) {
            throw new AttachmentException(e);
        }
    }

    private void processCopyAttachmentImage(
            Element element, String imageSrc, Long attachmentListId, EntityReference entityReference) {
        Long attId = extractAttachmentIdFromSrc(imageSrc);
        Attachment attachment = attachmentDao.getReferenceById(attId);

        try {
            InputStream stream = attachment.getContent().getStream();
            UploadedData uploadedData =
                    new UploadedData(stream, attachment.getName(), stream.available());
            AttachmentDto attachmentDto =
                    addAttachmentWithoutPermissionCheck(attachmentListId, uploadedData, entityReference);
            element.attr("src", buildFileUploadUrl(attachmentListId, attachmentDto.getId()));
        } catch (IOException e) {
            throw new AttachmentException(e);
        }
    }

    private Long extractAttachmentIdFromSrc(String imageSrc) {
        String idString = imageSrc.substring(imageSrc.lastIndexOf("/") + 1);
        return Long.parseLong(idString);
    }

    private boolean imageIsAttachment(String imageSrc, Long copiedAttachmentListId) {
        return imageSrc.contains(
                "/backend/attach-list/" + copiedAttachmentListId + "/attachments/download/");
    }

    private String getImageName(Element element, String imageBase64) {
        String imageName = element.attr("alt");
        if (imageName.isEmpty()) {
            return generateDefaultImageName(imageBase64);
        }
        return imageName;
    }

    private String generateDefaultImageName(String imageBase64) {
        String defaultImageName = "image.";

        String[] parts = imageBase64.split(",");
        String mediaType = parts[0];

        String extension = mediaType.substring("data:image/".length());
        if (extension.contains(";")) {
            extension = extension.substring(0, extension.indexOf(";"));
        }

        return defaultImageName + extension;
    }

    private void addAttachmentFromBase64(
            AttachmentList attachmentList, Element element, String imageSrc, String imageName) {
        String base64Image = imageSrc.split(",")[1];
        byte[] imageBytes = Base64.getDecoder().decode(base64Image);
        InputStream targetStream = new ByteArrayInputStream(imageBytes);

        try {
            UploadedData uploadedData =
                    new UploadedData(targetStream, imageName, targetStream.available());
            AttachmentDto attachmentDto = addAttachment(attachmentList, uploadedData);
            element.attr("src", buildFileUploadUrl(attachmentList.getId(), attachmentDto.getId()));
        } catch (IOException e) {
            throw new AttachmentException(e);
        }
    }

    private String buildFileUploadUrl(Long attachmentListId, Long attachmentId) {
        HttpServletRequest request =
                ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        StringBuilder builder = new StringBuilder();
        builder
                .append(request.getScheme())
                .append("://")
                .append(request.getServerName())
                .append(":")
                .append(request.getServerPort());
        builder.append(request.getContextPath());
        builder.append("/backend/attach-list/");
        builder.append(attachmentListId);
        builder.append("/attachments/download/");
        builder.append(attachmentId);

        return builder.toString();
    }

    private boolean imageIsBase64(String imageSrc) {
        String regex = "^data:image\\/(png|jpeg|jpg|gif|svg|bmp);base64,([A-Za-z0-9+/]+={0,2})$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(imageSrc);

        return matcher.matches();
    }

    private void checkCanAttach(long entityId, String entityClassName) {
        String[] roles = {Roles.ROLE_ADMIN, Roles.ROLE_TA_API_CLIENT};
        String permissionName =
                Project.class.getName().equals(entityClassName) ? MANAGE_PROJECT.name() : ATTACH.name();
        boolean permission =
                permissionEvaluationService.hasRoleOrPermissionOnObject(
                        roles, permissionName, entityId, entityClassName);
        if (!permission) {
            throw new AccessDeniedException("Access is denied");
        }
    }
}
