/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class RequirementVersionCoverageDto {

    private Long requirementVersionId;
    private Long requirementId;
    private Long projectId;
    private String projectName;
    private String reference;
    private String name;
    private String criticality;
    private String status;
    private Long verifyingTestCaseId;
    private Set<Long> verifyingCalledTestCaseIds = new HashSet<>();
    private Date milestoneMinDate;
    private Date milestoneMaxDate;
    private String milestoneLabels;
    private List<CoverageStepInfoDto> coverageStepInfos = new ArrayList<>();
    private String path;

    public Long getRequirementVersionId() {
        return requirementVersionId;
    }

    public void setRequirementVersionId(Long requirementVersionId) {
        this.requirementVersionId = requirementVersionId;
    }

    public Long getRequirementId() {
        return requirementId;
    }

    public void setRequirementId(Long requirementId) {
        this.requirementId = requirementId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCriticality() {
        return criticality;
    }

    public void setCriticality(String criticality) {
        this.criticality = criticality;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getVerifyingTestCaseId() {
        return verifyingTestCaseId;
    }

    public void setVerifyingTestCaseId(Long verifyingTestCaseId) {
        this.verifyingTestCaseId = verifyingTestCaseId;
    }

    public boolean isDirectlyVerified() {
        return Objects.nonNull(this.verifyingTestCaseId);
    }

    public boolean isUnDirectlyVerified() {
        return !this.verifyingCalledTestCaseIds.isEmpty();
    }

    public Set<Long> getVerifyingCalledTestCaseIds() {
        return verifyingCalledTestCaseIds;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public void addCalledVerifyingTestCase(Long verifyingTestCaseId) {
        this.verifyingCalledTestCaseIds.add(verifyingTestCaseId);
    }

    public Date getMilestoneMinDate() {
        return milestoneMinDate;
    }

    public void setMilestoneMinDate(Date milestoneMinDate) {
        this.milestoneMinDate = milestoneMinDate;
    }

    public Date getMilestoneMaxDate() {
        return milestoneMaxDate;
    }

    public void setMilestoneMaxDate(Date milestoneMaxDate) {
        this.milestoneMaxDate = milestoneMaxDate;
    }

    public String getMilestoneLabels() {
        return milestoneLabels;
    }

    public void setMilestoneLabels(String milestoneLabels) {
        this.milestoneLabels = milestoneLabels;
    }

    public List<CoverageStepInfoDto> getCoverageStepInfos() {
        return coverageStepInfos;
    }

    public void addCoverageStepInfo(CoverageStepInfoDto coverageStepInfoDto) {
        this.coverageStepInfos.add(coverageStepInfoDto);
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
