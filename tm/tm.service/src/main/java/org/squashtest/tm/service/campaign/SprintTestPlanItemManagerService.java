/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.campaign;

import java.util.List;
import org.squashtest.tm.domain.users.User;

public interface SprintTestPlanItemManagerService {
    void changeDataset(long testPlanItemId, Long datasetId);

    void deleteExecutionsAndRemoveFromTestPlanItem(List<Long> executionIds, long testPlanItemId);

    void applyFastPass(List<Long> testPlanItemsIds, String executionStatus);

    List<User> findAssignableUsersByCampaignLibraryId(Long campaignLibraryId);

    List<User> findAssignableUsersByTestPlanItemId(Long testPlanItemId);

    List<User> findAssignableUsersByExploratorySessionOverviewId(Long overviewId);

    void assignUserToTestPlanItem(Long testPlanItemId, long userId);

    void deleteSprintReqVersionTestPlanItems(Long sprintReqVersionId, List<Long> testPlanItemIds);

    void moveSprintReqVersionTestPlanItems(
            long sprintReqVersionId, List<Long> testPlanItemIds, int position);
}
