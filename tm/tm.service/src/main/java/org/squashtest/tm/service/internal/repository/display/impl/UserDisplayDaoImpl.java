/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.squashtest.tm.jooq.domain.Tables.ACL_CLASS;
import static org.squashtest.tm.jooq.domain.Tables.ACL_GROUP_PERMISSION;
import static org.squashtest.tm.jooq.domain.Tables.ACL_OBJECT_IDENTITY;
import static org.squashtest.tm.jooq.domain.Tables.ACL_RESPONSIBILITY_SCOPE_ENTRY;
import static org.squashtest.tm.jooq.domain.Tables.CORE_GROUP_MEMBER;
import static org.squashtest.tm.jooq.domain.Tables.CORE_PARTY;
import static org.squashtest.tm.jooq.domain.Tables.CORE_TEAM;
import static org.squashtest.tm.jooq.domain.Tables.CORE_TEAM_MEMBER;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;

import java.util.List;
import java.util.stream.Stream;
import javax.inject.Inject;
import org.jooq.DSLContext;
import org.jooq.SelectJoinStep;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.service.internal.display.dto.UserAdminViewDto;
import org.squashtest.tm.service.internal.display.dto.UserAdminViewTeamDto;
import org.squashtest.tm.service.internal.repository.display.UserDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

@Repository
public class UserDisplayDaoImpl implements UserDisplayDao {

    @Inject private DSLContext dsl;

    @Override
    public UserAdminViewDto getUserById(Long userId) {
        return getBaseRequest()
                .where(CORE_USER.PARTY_ID.eq(userId))
                .fetchOne()
                .into(UserAdminViewDto.class);
    }

    @Override
    public List<UserAdminViewTeamDto> getTeamsByUser(Long userId) {
        return dsl.select(CORE_TEAM.PARTY_ID, CORE_TEAM.NAME)
                .from(CORE_TEAM)
                .leftJoin(CORE_TEAM_MEMBER)
                .on(CORE_TEAM.PARTY_ID.eq(CORE_TEAM_MEMBER.TEAM_ID))
                .where(CORE_TEAM_MEMBER.USER_ID.eq(userId))
                .fetchInto(UserAdminViewTeamDto.class);
    }

    @Override
    public List<Long> findPartyIdsCanAccessProject(List<Long> projectIds) {
        return dsl.select(CORE_PARTY.PARTY_ID)
                .from(CORE_PARTY)
                .join(ACL_RESPONSIBILITY_SCOPE_ENTRY)
                .on(ACL_RESPONSIBILITY_SCOPE_ENTRY.PARTY_ID.eq(CORE_PARTY.PARTY_ID))
                .join(ACL_OBJECT_IDENTITY)
                .on(ACL_OBJECT_IDENTITY.ID.eq(ACL_RESPONSIBILITY_SCOPE_ENTRY.OBJECT_IDENTITY_ID))
                .join(ACL_GROUP_PERMISSION)
                .on(ACL_RESPONSIBILITY_SCOPE_ENTRY.ACL_GROUP_ID.eq(ACL_GROUP_PERMISSION.ACL_GROUP_ID))
                .join(ACL_CLASS)
                .on(
                        ACL_GROUP_PERMISSION
                                .CLASS_ID
                                .eq(ACL_CLASS.ID)
                                .and(ACL_CLASS.CLASSNAME.eq("org.squashtest.tm.domain.project.Project")))
                .where(ACL_OBJECT_IDENTITY.IDENTITY.in(projectIds))
                .groupBy(CORE_PARTY.PARTY_ID)
                .fetch(CORE_PARTY.PARTY_ID, Long.class);
    }

    @Override
    public List<NamedReference> findUserLoginsByPartyIds(List<Long> partyIds) {
        List<NamedReference> usersSolo =
                dsl.select(CORE_USER.PARTY_ID.as(RequestAliasesConstants.ID), CORE_USER.LOGIN.as("NAME"))
                        .from(CORE_USER)
                        .join(CORE_PARTY)
                        .on(CORE_PARTY.PARTY_ID.eq(CORE_USER.PARTY_ID))
                        .where(CORE_PARTY.PARTY_ID.in(partyIds))
                        .groupBy(CORE_USER.PARTY_ID)
                        .fetchInto(NamedReference.class);

        List<NamedReference> usersInTeam =
                dsl.select(CORE_USER.PARTY_ID.as(RequestAliasesConstants.ID), CORE_USER.LOGIN.as("NAME"))
                        .from(CORE_USER)
                        .join(CORE_TEAM_MEMBER)
                        .on(CORE_TEAM_MEMBER.USER_ID.eq(CORE_USER.PARTY_ID))
                        .join(CORE_PARTY)
                        .on(CORE_TEAM_MEMBER.TEAM_ID.eq(CORE_PARTY.PARTY_ID))
                        .where(CORE_PARTY.PARTY_ID.in(partyIds))
                        .groupBy(CORE_USER.PARTY_ID)
                        .fetchInto(NamedReference.class);

        return Stream.concat(usersSolo.stream(), usersInTeam.stream()).distinct().toList();
    }

    private SelectJoinStep<?> getBaseRequest() {
        return dsl.select(
                        CORE_USER.PARTY_ID.as(RequestAliasesConstants.ID),
                        CORE_USER.LOGIN,
                        CORE_USER.FIRST_NAME,
                        CORE_USER.LAST_NAME,
                        CORE_USER.EMAIL,
                        CORE_USER.ACTIVE,
                        CORE_USER.CREATED_BY,
                        CORE_USER.CREATED_ON,
                        CORE_USER.LAST_MODIFIED_BY,
                        CORE_USER.LAST_MODIFIED_ON,
                        CORE_USER.LAST_CONNECTED_ON,
                        CORE_GROUP_MEMBER.GROUP_ID.as("USERS_GROUP_BINDING"),
                        CORE_USER.CAN_DELETE_FROM_FRONT)
                .from(CORE_USER)
                .leftJoin(CORE_GROUP_MEMBER)
                .on(CORE_GROUP_MEMBER.PARTY_ID.eq(CORE_USER.PARTY_ID));
    }
}
