/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_EXECUTION_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_ENVIRONMENT_VARIABLE;
import static org.squashtest.tm.jooq.domain.Tables.ENVIRONMENT_VARIABLE;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;

import java.util.List;
import javax.inject.Inject;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.environmentvariable.EVInputType;
import org.squashtest.tm.service.internal.dto.DenormalizedEnvironmentVariableDto;
import org.squashtest.tm.service.internal.repository.CustomDenormalizedEnvironmentVariableDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

@Repository
public class CustomDenormalizedEnvironmentVariableDaoImpl
        implements CustomDenormalizedEnvironmentVariableDao {

    private static final String INTERPRETED_TEXT = EVInputType.INTERPRETED_TEXT.name();
    @Inject DSLContext dslContext;

    @Override
    public List<DenormalizedEnvironmentVariableDto> getAllByExecutionId(
            Long executionId, String interpretedLabel) {

        Field<String> valueField =
                DSL.when(
                                DENORMALIZED_ENVIRONMENT_VARIABLE.TYPE.eq(INTERPRETED_TEXT),
                                DSL.concat(DENORMALIZED_ENVIRONMENT_VARIABLE.VALUE, interpretedLabel))
                        .otherwise(DENORMALIZED_ENVIRONMENT_VARIABLE.VALUE);

        return dslContext
                .select(
                        DENORMALIZED_ENVIRONMENT_VARIABLE.DEV_ID.as(RequestAliasesConstants.ID),
                        DENORMALIZED_ENVIRONMENT_VARIABLE.EV_ID,
                        DSL.concat(DENORMALIZED_ENVIRONMENT_VARIABLE.NAME, DSL.value(": "), valueField)
                                .as(RequestAliasesConstants.NAMED_VALUE))
                .from(DENORMALIZED_ENVIRONMENT_VARIABLE)
                .leftJoin(AUTOMATED_EXECUTION_EXTENDER)
                .on(
                        DENORMALIZED_ENVIRONMENT_VARIABLE.HOLDER_ID.eq(
                                AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID))
                .leftJoin(EXECUTION)
                .on(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                .leftJoin(ENVIRONMENT_VARIABLE)
                .on(ENVIRONMENT_VARIABLE.EV_ID.eq(DENORMALIZED_ENVIRONMENT_VARIABLE.EV_ID))
                .where(EXECUTION.EXECUTION_ID.eq(executionId))
                .fetchInto(DenormalizedEnvironmentVariableDto.class);
    }
}
