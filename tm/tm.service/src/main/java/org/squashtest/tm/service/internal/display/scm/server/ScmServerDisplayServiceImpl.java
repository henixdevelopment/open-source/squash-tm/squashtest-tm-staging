/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.scm.server;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

import java.util.Arrays;
import java.util.List;
import javax.inject.Inject;
import org.jooq.DSLContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.scm.ScmRepository;
import org.squashtest.tm.domain.scm.ScmServer;
import org.squashtest.tm.service.display.scm.server.ScmServerDisplayService;
import org.squashtest.tm.service.internal.display.dto.CredentialsDto;
import org.squashtest.tm.service.internal.display.dto.ScmRepositoryDto;
import org.squashtest.tm.service.internal.display.dto.ScmServerAdminViewDto;
import org.squashtest.tm.service.internal.display.dto.ScmServerDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.administration.ScmServerGrid;
import org.squashtest.tm.service.internal.repository.ScmServerDao;
import org.squashtest.tm.service.internal.repository.display.ScmServerDisplayDao;
import org.squashtest.tm.service.scmserver.ScmRepositoryManagerService;
import org.squashtest.tm.service.scmserver.ScmServerCredentialsService;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.servers.ManageableCredentials;
import org.squashtest.tm.service.servers.StoredCredentialsManager;

@Service
@Transactional
public class ScmServerDisplayServiceImpl implements ScmServerDisplayService {
    private final DSLContext dsl;
    private final ScmServerDao scmServerDao;
    private final ScmServerDisplayDao scmServerDisplayDao;
    private final ScmRepositoryManagerService scmRepositoryManager;
    private final StoredCredentialsManager credentialsManager;
    private final ScmServerCredentialsService scmServerCredentialsService;
    private final PermissionEvaluationService permissionEvaluationService;

    @Inject
    ScmServerDisplayServiceImpl(
            DSLContext dsl,
            ScmServerDao scmServerDao,
            ScmRepositoryManagerService scmRepositoryManager,
            ScmServerDisplayDao scmServerDisplayDao,
            StoredCredentialsManager credentialsManager,
            ScmServerCredentialsService scmServerCredentialsService,
            PermissionEvaluationService permissionEvaluationService) {
        this.dsl = dsl;
        this.scmServerDao = scmServerDao;
        this.scmRepositoryManager = scmRepositoryManager;
        this.scmServerDisplayDao = scmServerDisplayDao;
        this.credentialsManager = credentialsManager;
        this.scmServerCredentialsService = scmServerCredentialsService;
        this.permissionEvaluationService = permissionEvaluationService;
    }

    @PreAuthorize(HAS_ROLE_ADMIN)
    @Override
    public GridResponse findAll(GridRequest gridRequest) {
        return new ScmServerGrid().getRows(gridRequest, dsl);
    }

    @Override
    public List<ScmServerDto> getAllServersAndRepositories() {
        permissionEvaluationService.checkAtLeastOneProjectManagementPermissionOrAdmin();
        List<ScmServer> allServers = scmServerDao.findAllByOrderByNameAsc();

        return allServers.stream()
                .map(
                        scmServer ->
                                new ScmServerDto(
                                        scmServer,
                                        scmRepositoryManager.findClonedByScmServerOrderByName(scmServer.getId())))
                .toList();
    }

    @PreAuthorize(HAS_ROLE_ADMIN)
    @Override
    public ScmServerAdminViewDto getScmServerView(long scmServerId) {
        ScmServerAdminViewDto scmServer = scmServerDisplayDao.getScmServerViewById(scmServerId);

        ManageableCredentials credentials = credentialsManager.findAppLevelCredentials(scmServerId);
        scmServer.setCredentials(CredentialsDto.from(credentials));

        ScmServer actualScmServer = scmServerDao.getReferenceById(scmServerId);
        List<String> supportedProtocols =
                Arrays.stream(scmServerCredentialsService.getSupportedProtocols(actualScmServer))
                        .map(Enum::name)
                        .toList();
        scmServer.setSupportedAuthenticationProtocols(supportedProtocols);

        appendRepositories(scmServerId, scmServer);

        return scmServer;
    }

    private void appendRepositories(long scmServerId, ScmServerAdminViewDto scmServer) {
        List<ScmRepository> repositories = scmRepositoryManager.findByScmServerOrderByPath(scmServerId);
        List<ScmRepositoryDto> repositoriesDto =
                repositories.stream().map(ScmRepositoryDto::new).toList();
        scmServer.setRepositories(repositoriesDto);
    }
}
