/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.statistics.campaign;

import java.util.List;
import java.util.Map;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.service.statistics.iteration.TestSuiteTestInventoryStatistics;

public class StatisticsBundle {

    private Map<ExecutionStatus, Integer> testCaseStatusStatistics;

    private Map<TestCaseImportance, Integer> nonExecutedTestCaseImportanceStatistics;

    private CampaignTestCaseSuccessRateStatistics testCaseSuccessRateStatistics;

    private ProgressionStatistics progressionStatistics;

    // is null if scope is iteration
    private List<TestInventoryStatistics> testInventoryStatistics;

    // is null for all except iteration scope
    private List<TestSuiteTestInventoryStatistics> testsuiteTestInventoryStatisticsList;

    private List<Long> selectedIds;

    public Map<ExecutionStatus, Integer> getTestCaseStatusStatistics() {
        return testCaseStatusStatistics;
    }

    public void setTestCaseStatusStatistics(Map<ExecutionStatus, Integer> testCaseStatusStatistics) {
        this.testCaseStatusStatistics = testCaseStatusStatistics;
    }

    public Map<TestCaseImportance, Integer> getNonExecutedTestCaseImportanceStatistics() {
        return nonExecutedTestCaseImportanceStatistics;
    }

    public void setNonExecutedTestCaseImportanceStatistics(
            Map<TestCaseImportance, Integer> nonExecutedTestCaseImportanceStatistics) {
        this.nonExecutedTestCaseImportanceStatistics = nonExecutedTestCaseImportanceStatistics;
    }

    public CampaignTestCaseSuccessRateStatistics getTestCaseSuccessRateStatistics() {
        return testCaseSuccessRateStatistics;
    }

    public void setTestCaseSuccessRateStatistics(
            CampaignTestCaseSuccessRateStatistics testCaseSuccessRateStatistics) {
        this.testCaseSuccessRateStatistics = testCaseSuccessRateStatistics;
    }

    public ProgressionStatistics getProgressionStatistics() {
        return progressionStatistics;
    }

    public void setProgressionStatistics(ProgressionStatistics progressionStatistics) {
        this.progressionStatistics = progressionStatistics;
    }

    public List<TestInventoryStatistics> getTestInventoryStatistics() {
        return testInventoryStatistics;
    }

    public void setTestInventoryStatistics(List<TestInventoryStatistics> testInventoryStatistics) {
        this.testInventoryStatistics = testInventoryStatistics;
    }

    public List<TestSuiteTestInventoryStatistics> getTestsuiteTestInventoryStatisticsList() {
        return testsuiteTestInventoryStatisticsList;
    }

    public void setTestsuiteTestInventoryStatisticsList(
            List<TestSuiteTestInventoryStatistics> testsuiteTestInventoryStatisticsList) {
        this.testsuiteTestInventoryStatisticsList = testsuiteTestInventoryStatisticsList;
    }

    public List<Long> getSelectedIds() {
        return selectedIds;
    }

    public void setSelectedIds(List<Long> selectedIds) {
        this.selectedIds = selectedIds;
    }
}
