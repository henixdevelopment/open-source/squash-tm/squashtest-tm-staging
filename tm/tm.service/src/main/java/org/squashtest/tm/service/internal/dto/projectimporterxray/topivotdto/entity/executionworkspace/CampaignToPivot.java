/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity.executionworkspace;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.campaign.CampaignStatus;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportField;
import org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.mapping.XrayStatus;

public class CampaignToPivot extends AbstractGenericExecutionWorkspace {

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @JsonProperty(JsonImportField.STATUS)
    private CampaignStatus status;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @JsonProperty(JsonImportField.PARENT_TYPE)
    private EntityType parentType;

    @Override
    public BindableEntity getBindableEntity() {
        return BindableEntity.CAMPAIGN;
    }

    public CampaignStatus getStatus() {
        return status;
    }

    public void setStatus(XrayStatus status) {
        this.status = status.getCampaignStatus();
    }

    public EntityType getParentType() {
        return parentType;
    }

    public void setParentType(EntityType parentType) {
        this.parentType = parentType;
    }
}
