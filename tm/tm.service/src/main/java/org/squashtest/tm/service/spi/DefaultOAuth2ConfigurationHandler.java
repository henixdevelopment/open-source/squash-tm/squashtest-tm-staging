/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.spi;

import org.squashtest.csp.core.bugtracker.spi.DefaultOAuth2FormValues;
import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.domain.servers.OAuth2Credentials;

public class DefaultOAuth2ConfigurationHandler implements OAuth2ConfigurationHandler {

    private static final String CLIENT_ID_PARAM = "client_id=";
    private static final String REDIRECT_URI_PARAM = "&redirect_uri=";
    private static final String CLIENT_SECRET_PARAM = "&client_secret=";

    @Override
    public String getOauth2AuthenticationUrl(BugTracker bugTracker, GenericServerOAuth2Conf conf) {
        StringBuilder sb = new StringBuilder();
        sb.append(conf.getAuthorizationUrl());
        sb.append("?");
        sb.append(CLIENT_ID_PARAM);
        sb.append(conf.getClientId());
        sb.append(REDIRECT_URI_PARAM);
        sb.append(conf.getCallbackUrl());
        sb.append("&response_type=");
        sb.append(conf.getGrantType());
        appendScope(bugTracker.getUrl(), sb, conf);

        return sb.toString();
    }

    @Override
    public void appendScope(String bugTrackerUrl, StringBuilder sb, GenericServerOAuth2Conf conf) {
        sb.append("&scope=");
        sb.append(conf.getScope());
    }

    @Override
    public String getOauth2RequestTokenUrl(Long serverId, String code, GenericServerOAuth2Conf conf) {
        StringBuilder sb = new StringBuilder();
        sb.append(conf.getRequestTokenUrl());
        sb.append("?");
        sb.append(CLIENT_ID_PARAM);
        sb.append(conf.getClientId());
        sb.append(CLIENT_SECRET_PARAM);
        sb.append(conf.getClientSecret());
        sb.append("&code=");
        sb.append(code);
        sb.append("&grant_type=authorization_code");
        sb.append(REDIRECT_URI_PARAM);
        sb.append(conf.getCallbackUrl());
        return sb.toString();
    }

    @Override
    public String getRefreshTokenUrl(GenericServerOAuth2Conf conf, OAuth2Credentials creds) {
        StringBuilder sb = new StringBuilder();
        sb.append(conf.getRequestTokenUrl());
        sb.append("?");
        sb.append(CLIENT_ID_PARAM);
        sb.append(conf.getClientId());
        sb.append(CLIENT_SECRET_PARAM);
        sb.append(conf.getClientSecret());
        sb.append("&refresh_token=");
        sb.append(creds.getRefreshToken());
        sb.append("&grant_type=refresh_token");
        return sb.toString();
    }

    @Override
    public DefaultOAuth2FormValues getDefaultValueForOauth2Form(String bugTrackerUrl) {
        return new DefaultOAuth2FormValues(
                bugTrackerUrl + "/rest/oauth2/latest/authorize",
                bugTrackerUrl + "/rest/oauth2/latest/token",
                "");
    }
}
