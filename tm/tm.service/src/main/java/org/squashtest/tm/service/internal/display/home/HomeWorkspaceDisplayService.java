/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.home;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.Workspace;
import org.squashtest.tm.domain.users.PartyPreference;
import org.squashtest.tm.domain.users.preferences.CorePartyPreference;
import org.squashtest.tm.service.customreport.CustomReportDashboardService;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;
import org.squashtest.tm.service.system.SystemAdministrationService;
import org.squashtest.tm.service.user.PartyPreferenceService;

@Service
@Transactional(readOnly = true)
public class HomeWorkspaceDisplayService {

    private final SystemAdministrationService systemAdministrationService;

    private final PartyPreferenceService partyPreferenceService;

    private final CustomReportDashboardService customReportDashboardService;

    public HomeWorkspaceDisplayService(
            SystemAdministrationService systemAdministrationService,
            PartyPreferenceService partyPreferenceService,
            CustomReportDashboardService customReportDashboardService) {
        this.systemAdministrationService = systemAdministrationService;
        this.partyPreferenceService = partyPreferenceService;
        this.customReportDashboardService = customReportDashboardService;
    }

    public HomeWorkspaceData getWorkspacePayload() {
        HomeWorkspaceData homeWorkspaceData = new HomeWorkspaceData();
        String welcomeMessage = systemAdministrationService.findWelcomeMessage();
        homeWorkspaceData.setWelcomeMessage(HTMLCleanupUtils.cleanHtml(welcomeMessage));

        if (customReportDashboardService.shouldShowFavoriteDashboardInWorkspace(Workspace.HOME)) {
            homeWorkspaceData.setShowDashboard(true);
        }
        if (customReportDashboardService.canShowDashboardInWorkspace(Workspace.HOME)) {
            PartyPreference preference =
                    partyPreferenceService.findPreferenceForCurrentUser(
                            CorePartyPreference.FAVORITE_DASHBOARD_HOME.getPreferenceKey());
            if (preference != null) {
                Long dashboardId = Long.valueOf(preference.getPreferenceValue());
                homeWorkspaceData.setFavoriteDashboardId(dashboardId);
            }
        }

        return homeWorkspaceData;
    }
}
