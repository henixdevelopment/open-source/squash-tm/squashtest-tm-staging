/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.projectimporter.pivotimporter.parsers;

import com.fasterxml.jackson.core.JsonParser;
import java.util.Objects;
import org.springframework.stereotype.Service;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.infolist.SystemListItem;
import org.squashtest.tm.domain.projectimporter.PivotFormatImport;
import org.squashtest.tm.domain.requirement.NewRequirementVersionDto;
import org.squashtest.tm.domain.requirement.RequirementCriticality;
import org.squashtest.tm.domain.requirement.RequirementStatus;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportField;
import org.squashtest.tm.service.internal.dto.projectimporter.PivotImportMetadata;
import org.squashtest.tm.service.internal.dto.projectimporter.RequirementToImport;
import org.squashtest.tm.service.internal.projectimporter.pivotimporter.PivotFormatLoggerHelper;
import org.squashtest.tm.service.projectimporter.pivotimporter.PivotJsonParsingHelper;
import org.squashtest.tm.service.projectimporter.pivotimporter.parsers.RequirementWorkspaceParser;

@Service("RequirementWorkspaceParser")
public class RequirementWorkspaceParserImpl implements RequirementWorkspaceParser {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(RequirementWorkspaceParserImpl.class);
    private static final String REQ_CAT_PREFIX = "CAT_";

    @Override
    public RequirementToImport parseRequirement(
            JsonParser jsonParser,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport) {
        RequirementToImport requirementToImport = new RequirementToImport();
        NewRequirementVersionDto requirementDto = new NewRequirementVersionDto();

        try {
            while (PivotJsonParsingHelper.isNotTheEndOfParsedObject(jsonParser)) {
                String fieldName = jsonParser.getCurrentName();
                jsonParser.nextToken();

                switch (fieldName) {
                    case JsonImportField.ID -> requirementToImport.setInternalId(jsonParser.getText());
                    case JsonImportField.NAME -> requirementDto.setName(jsonParser.getText());
                    case JsonImportField.DESCRIPTION -> requirementDto.setDescription(jsonParser.getText());
                    case JsonImportField.REFERENCE -> requirementDto.setReference(jsonParser.getText());
                    case JsonImportField.STATUS ->
                            requirementToImport.setStatus(RequirementStatus.valueOf(jsonParser.getText()));
                    case JsonImportField.CRITICALITY ->
                            requirementDto.setCriticality(RequirementCriticality.valueOf(jsonParser.getText()));
                    case JsonImportField.CATEGORY -> handleCategory(requirementDto, jsonParser.getText());
                    case JsonImportField.CUSTOM_FIELDS ->
                            requirementDto.setCustomFields(
                                    PivotJsonParsingHelper.getCustomFieldValues(
                                            jsonParser, pivotImportMetadata.getCustomFieldIdsMap()));
                    case JsonImportField.PARENT_TYPE ->
                            requirementToImport.setParentType(EntityType.valueOf(jsonParser.getText()));
                    case JsonImportField.PARENT_ID -> requirementToImport.setParentId(jsonParser.getText());
                    case JsonImportField.ATTACHMENTS ->
                            requirementToImport.setAttachments(
                                    AttachmentParserHelper.parseAttachments(jsonParser));
                    default -> {
                        // continue parsing
                    }
                }
                PivotFormatLoggerHelper.logParsingSuccessForEntity(
                        LOGGER,
                        PivotFormatLoggerHelper.REQUIREMENT,
                        requirementDto.getName(),
                        requirementToImport.getInternalId(),
                        pivotFormatImport);
            }
        } catch (Exception e) {
            PivotFormatLoggerHelper.handleParsingErrorForEntity(
                    LOGGER,
                    PivotFormatLoggerHelper.REQUIREMENT,
                    requirementToImport.getInternalId(),
                    pivotFormatImport,
                    e);
        }

        updateNullValueWithDefaultValues(requirementToImport, requirementDto);
        requirementToImport.setRequirement(requirementDto);
        return requirementToImport;
    }

    private void handleCategory(NewRequirementVersionDto requirementVersionDto, String category) {
        if (Objects.nonNull(category)) {
            requirementVersionDto.setCategory(REQ_CAT_PREFIX + category);
        } else {
            requirementVersionDto.setCategory(SystemListItem.SYSTEM_REQ_CATEGORY);
        }
    }

    private void updateNullValueWithDefaultValues(
            RequirementToImport requirementToImport, NewRequirementVersionDto requirementDto) {
        if (Objects.isNull(requirementDto.getReference())) {
            requirementDto.setReference("");
        }

        if (Objects.isNull(requirementDto.getCategory())) {
            requirementDto.setCategory(SystemListItem.SYSTEM_REQ_CATEGORY);
        }

        if (Objects.isNull(requirementDto.getCriticality())) {
            requirementDto.setCriticality(RequirementCriticality.UNDEFINED);
        }

        if (Objects.isNull(requirementToImport.getStatus())) {
            requirementToImport.setStatus(RequirementStatus.WORK_IN_PROGRESS);
        }
    }
}
