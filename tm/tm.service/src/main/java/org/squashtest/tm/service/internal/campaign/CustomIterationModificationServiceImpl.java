/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.campaign;

import static org.squashtest.tm.service.security.Authorizations.CREATE_CAMPAIGN_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.CREATE_ITERATION_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.EXECUTE_ITPI_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.LINK_ITERATION_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_ITERATION_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_TC_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.WRITE_ITERATION_OR_ROLE_ADMIN;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import javax.inject.Inject;
import javax.inject.Provider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignLibraryNode;
import org.squashtest.tm.domain.campaign.CampaignTestPlanItem;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.execution.ExecutionVisitor;
import org.squashtest.tm.domain.execution.ExploratoryExecution;
import org.squashtest.tm.domain.execution.KeywordExecution;
import org.squashtest.tm.domain.execution.ScriptedExecution;
import org.squashtest.tm.domain.testcase.ConsumerForScriptedTestCaseVisitor;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.exception.DuplicateNameException;
import org.squashtest.tm.exception.execution.ExecutionHasNoStepsException;
import org.squashtest.tm.exception.execution.ExecutionWasDeleted;
import org.squashtest.tm.exception.execution.TestPlanItemNotExecutableException;
import org.squashtest.tm.exception.execution.TestSuiteTestPlanHasDeletedTestCaseException;
import org.squashtest.tm.service.annotation.BatchPreventConcurrent;
import org.squashtest.tm.service.annotation.CheckBlockingMilestone;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.annotation.Ids;
import org.squashtest.tm.service.annotation.PreventConcurrent;
import org.squashtest.tm.service.annotation.PreventConcurrents;
import org.squashtest.tm.service.attachment.AttachmentManagerService;
import org.squashtest.tm.service.campaign.CustomCampaignModificationService;
import org.squashtest.tm.service.campaign.CustomIterationModificationService;
import org.squashtest.tm.service.campaign.CustomTestSuiteModificationService;
import org.squashtest.tm.service.campaign.IterationStatisticsService;
import org.squashtest.tm.service.campaign.IterationTestPlanManagerService;
import org.squashtest.tm.service.clipboard.model.ClipboardPayload;
import org.squashtest.tm.service.copier.StrategyCopierService;
import org.squashtest.tm.service.execution.ExecutionModificationService;
import org.squashtest.tm.service.internal.campaign.coercers.TestSuiteToIterationCoercerForArray;
import org.squashtest.tm.service.internal.campaign.scripted.ScriptedTestCaseExecutionHelper;
import org.squashtest.tm.service.internal.customfield.PrivateCustomFieldValueService;
import org.squashtest.tm.service.internal.denormalizedfield.PrivateDenormalizedFieldValueService;
import org.squashtest.tm.service.internal.library.PasteStrategy;
import org.squashtest.tm.service.internal.library.TreeNodeCopier;
import org.squashtest.tm.service.internal.repository.CampaignDao;
import org.squashtest.tm.service.internal.repository.ExecutionDao;
import org.squashtest.tm.service.internal.repository.IterationDao;
import org.squashtest.tm.service.internal.repository.IterationTestPlanDao;
import org.squashtest.tm.service.internal.repository.TestSuiteDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.statistics.campaign.StatisticsBundle;
import org.squashtest.tm.service.testcase.TestCaseCyclicCallChecker;

@Service("CustomIterationModificationService")
@Transactional
public class CustomIterationModificationServiceImpl
        implements CustomIterationModificationService, IterationTestPlanManager {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(CustomIterationModificationServiceImpl.class);
    private static final String ITERATION_ID = "iterationId";

    @Inject private MessageSource messageSource;

    @Inject private CampaignDao campaignDao;

    @Inject private CustomCampaignModificationService campaignModificationService;

    @Inject private IterationDao iterationDao;

    @Inject private TestSuiteDao suiteDao;

    @Inject private IterationTestPlanDao testPlanDao;

    @Inject private ExecutionDao executionDao;

    @Inject private TestCaseCyclicCallChecker testCaseCyclicCallChecker;

    @Inject private PrivateCustomFieldValueService customFieldValueService;

    @Inject private PrivateDenormalizedFieldValueService denormalizedFieldValueService;

    @Inject private IterationStatisticsService statisticsService;

    @Inject private ExecutionModificationService executionModificationService;

    @Inject
    @Qualifier("squashtest.tm.service.internal.PasteToIterationStrategy")
    private Provider<PasteStrategy<Iteration, TestSuite>> pasteToIterationStrategyProvider;

    @Inject private Provider<TreeNodeCopier> treeNodeCopierFactory;

    @Inject private CustomTestSuiteModificationService customTestSuiteModificationService;

    @Inject private ScriptedTestCaseExecutionHelper scriptedTestCaseExecutionHelper;

    @Inject private AttachmentManagerService attachmentManagerService;

    @Inject private IterationTestPlanManagerService iterationTestPlanManagerService;

    @Inject protected PermissionEvaluationService permissionService;

    @Inject private TestSuiteDao testSuiteDao;

    @Inject private StrategyCopierService strategyCopierService;

    @Override
    @PreventConcurrent(entityType = CampaignLibraryNode.class)
    @PreAuthorize(CREATE_CAMPAIGN_OR_ROLE_ADMIN)
    @CheckBlockingMilestone(entityType = Campaign.class)
    public Iteration addIterationToCampaign(
            Iteration iteration,
            @Id long campaignId,
            boolean copyTestPlan,
            Map<Long, RawValue> customFieldValues) {
        return addIterationToCampaignUnsecured(iteration, campaignId, copyTestPlan, customFieldValues);
    }

    public Iteration addIterationToCampaignUnsecured(
            Iteration iteration,
            @Id long campaignId,
            boolean copyTestPlan,
            Map<Long, RawValue> customFieldValues) {

        Campaign campaign = campaignDao.findById(campaignId);

        if (copyTestPlan) {
            populateTestPlan(iteration, campaign.getTestPlan());
        }

        iterationDao.persistIterationAndTestPlan(iteration);
        campaign.addContent(iteration);
        customFieldValueService.createAllCustomFieldValues(
                iteration, iteration.getProject(), customFieldValues);
        createIterationAttachments(iteration);
        return iteration;
    }

    @Override
    @PreventConcurrent(entityType = CampaignLibraryNode.class)
    @PreAuthorize(CREATE_CAMPAIGN_OR_ROLE_ADMIN)
    @CheckBlockingMilestone(entityType = Campaign.class)
    public void createIterationWithItemCopies(
            @Id long campaignId, String name, String description, List<Long> itemTestPlanIds) {
        final Iteration iteration = new Iteration();
        iteration.setName(name);
        iteration.setDescription(description);

        addIterationToCampaignUnsecured(iteration, campaignId, false, Collections.emptyMap());
        iterationTestPlanManagerService.copyTestPlanItems(itemTestPlanIds, iteration.getId());
    }

    private void createIterationAttachments(Iteration iteration) {
        String description = iteration.getDescription();

        if (description == null || description.isEmpty()) {
            return;
        }

        String html =
                attachmentManagerService.handleRichTextAttachments(
                        description, iteration.getAttachmentList());

        if (!description.equals(html)) {
            iteration.setDescription(html);
        }
    }

    /**
     * populates an iteration's test plan from a campaign's test plan.
     *
     * @param iteration
     * @param campaignTestPlan
     */
    private void populateTestPlan(Iteration iteration, List<CampaignTestPlanItem> campaignTestPlan) {
        for (CampaignTestPlanItem campaignItem : campaignTestPlan) {

            TestCase testcase = campaignItem.getReferencedTestCase();
            Dataset dataset = campaignItem.getReferencedDataset();
            User assignee = campaignItem.getUser();

            IterationTestPlanItem item = new IterationTestPlanItem(testcase, dataset, assignee);
            iteration.addTestPlan(item);
        }
    }

    @Override
    @PostAuthorize("hasPermission(returnObject, 'READ') " + OR_HAS_ROLE_ADMIN)
    @Transactional(readOnly = true)
    public Iteration findById(long iterationId) {
        return iterationDao.findById(iterationId);
    }

    @Override
    @PreAuthorize(WRITE_ITERATION_OR_ROLE_ADMIN)
    @CheckBlockingMilestone(entityType = Iteration.class)
    public void rename(@Id long iterationId, String newName) {
        Iteration iteration = iterationDao.findById(iterationId);

        List<Iteration> list = iteration.getCampaign().getIterations();

        String trimedName = newName.trim();

        if (!campaignModificationService.checkIterationNameAvailable(trimedName, list)) {
            throw new DuplicateNameException(
                    "Cannot rename iteration "
                            + iteration.getName()
                            + " : new name "
                            + trimedName
                            + " already exists in iteration "
                            + campaignModificationService);
        }
        iteration.setName(trimedName);
    }

    /**
     * @see CustomIterationModificationService#addExecution(long)
     */
    @Override
    @PreAuthorize(EXECUTE_ITPI_OR_ROLE_ADMIN)
    @PreventConcurrent(entityType = IterationTestPlanItem.class)
    public Execution addManualExecution(@Id long testPlanItemId) {
        return addManualExecutionUnsecured(testPlanItemId);
    }

    @Override
    public Execution addManualExecutionUnsecured(@Id long testPlanItemId) {
        IterationTestPlanItem item = testPlanDao.findByIdWithTestCase(testPlanItemId);
        if (item.isTestCaseDeleted()) {
            throw new TestSuiteTestPlanHasDeletedTestCaseException();
        }

        if (item.isExploratory()) {
            return addExecution(item);
        }

        if (!item.isGherkin() && item.getReferencedTestCase().getSteps().isEmpty()) {
            throw new ExecutionHasNoStepsException();
        }

        Locale locale =
                testPlanDao
                        .findProjectBddScriptLanguageByIterationTestPlanItemId(testPlanItemId)
                        .getLocale();
        // Scripted test cases do not have test steps: we need to create the execution to see if there
        // are any execution steps.
        // If an exception occurs, the execution is not persisted.
        Execution execution = addExecution(item, messageSource, locale);
        if (execution.getSteps().isEmpty()) {
            throw new ExecutionHasNoStepsException();
        }

        return execution;
    }

    /**
     * @see CustomIterationModificationService#addExecution(long)
     */
    @Override
    @PreAuthorize(EXECUTE_ITPI_OR_ROLE_ADMIN)
    @PreventConcurrent(entityType = IterationTestPlanItem.class)
    @CheckBlockingMilestone(entityType = IterationTestPlanItem.class)
    public Execution addExecution(@Id long testPlanItemId) {
        IterationTestPlanItem item = testPlanDao.findByIdWithTestCase(testPlanItemId);
        return addExecution(item);
    }

    @Override
    @PreAuthorize(CREATE_ITERATION_OR_ROLE_ADMIN)
    @PreventConcurrent(entityType = Iteration.class)
    @CheckBlockingMilestone(entityType = Iteration.class)
    public void addTestSuite(@Id long iterationId, TestSuite suite) {
        Iteration iteration = iterationDao.findById(iterationId);
        addTestSuite(iteration, suite);
    }

    @Override
    public void addTestSuite(Iteration iteration, TestSuite suite) {
        suiteDao.save(suite);
        iteration.addContent(suite);
        customFieldValueService.createAllCustomFieldValues(suite, suite.getProject());
    }

    @Override
    @Transactional(readOnly = true)
    public List<NamedReference> findAllTestSuitesAsNamedReferences(long iterationId) {
        return iterationDao.findAllTestSuitesAsNamedReferences(iterationId);
    }

    @Override
    @PreAuthorize(LINK_ITERATION_OR_ROLE_ADMIN)
    @PreventConcurrent(entityType = Iteration.class)
    @CheckBlockingMilestone(entityType = Iteration.class)
    public void changeTestSuitePosition(@Id long iterationId, int newIndex, List<Long> itemIds) {
        Iteration iteration = iterationDao.findById(iterationId);
        List<TestSuite> items = suiteDao.findAllById(itemIds);
        iteration.moveTestSuites(newIndex, items);
    }

    @Override
    @PreAuthorize(CREATE_ITERATION_OR_ROLE_ADMIN)
    @PreventConcurrents(
            simplesLocks = {@PreventConcurrent(entityType = Iteration.class, paramName = ITERATION_ID)},
            batchsLocks = {
                @BatchPreventConcurrent(
                        entityType = Iteration.class,
                        paramName = "testSuiteIds",
                        coercer = TestSuiteToIterationCoercerForArray.class)
            })
    public void copyPasteTestSuitesToIteration(
            @Ids("testSuiteIds") Long[] testSuiteIds,
            @Id(ITERATION_ID) long iterationId,
            ClipboardPayload clipboardPayload) {
        NodeType nodePaste = strategyCopierService.verifyPermissionAndGetNodePaste(clipboardPayload);
        strategyCopierService.copyNodeToIteration(iterationId, clipboardPayload, nodePaste);
    }

    @Override
    public Execution addExecution(IterationTestPlanItem item)
            throws TestPlanItemNotExecutableException {

        Execution execution = createExec(item, null, null);
        item.addExecution(execution);
        customTestSuiteModificationService.updateExecutionStatus(item.getTestSuites());

        operationsAfterAddingExec(execution);
        return execution;
    }

    private Execution addExecution(
            IterationTestPlanItem item, MessageSource messageSource, Locale locale)
            throws TestPlanItemNotExecutableException {
        Execution execution = createExec(item, messageSource, locale);
        item.addExecution(execution);
        customTestSuiteModificationService.updateExecutionStatus(item.getTestSuites());

        operationsAfterAddingExec(execution);
        return execution;
    }

    private Execution createExec(
            IterationTestPlanItem item, MessageSource messageSource, Locale locale) {
        TestCase testCase = item.getReferencedTestCase();
        testCaseCyclicCallChecker.checkNoCyclicCall(testCase);

        // if passes, let's move to the next step
        Execution execution;
        if (messageSource != null && locale != null) {
            execution = item.createExecution(messageSource, locale);
        } else {
            execution = item.createExecution(null, null);
        }

        // if we don't persist before we add, add will trigger an update of item.testPlan which fail
        // because execution
        // has no id yet. this is caused by weird mapping (https://hibernate.onjira.com/browse/HHH-5732)
        executionDao.save(execution);
        // we can now copy attachment contents of test case and test step,
        // which is a NOOP in database attachment mode (blob copy handled by Hibernate)
        // but will actually do the blob copy in file system attachment mode
        attachmentManagerService.copyContentsOnExternalRepository(execution);
        for (ExecutionStep executionStep : execution.getSteps()) {
            attachmentManagerService.copyContentsOnExternalRepository(executionStep);
        }
        return execution;
    }

    private void operationsAfterAddingExec(Execution execution) {
        ExecutionVisitor executionVisitor =
                new ExecutionVisitor() {
                    @Override
                    public void visit(Execution execution) {
                        performOperationsAfterAddingStandardExecution(execution);
                    }

                    @Override
                    public void visit(ScriptedExecution scriptedExecution) {
                        createCustomAndDenormalizedFieldsForExecution(scriptedExecution);
                        createExecutionStepsForScriptedTestCase(scriptedExecution);
                    }

                    @Override
                    public void visit(KeywordExecution keywordExecution) {
                        createCustomAndDenormalizedFieldsForExecution(keywordExecution);
                    }

                    @Override
                    public void visit(ExploratoryExecution exploratoryExecution) {
                        createCustomAndDenormalizedFieldsForExecution(exploratoryExecution);
                    }
                };
        execution.accept(executionVisitor);
    }

    private void performOperationsAfterAddingStandardExecution(Execution execution) {
        createCustomFieldsForExecutionAndExecutionSteps(execution);
        createDenormalizedFieldsForExecutionAndExecutionSteps(execution);
        createExecutionAttachments(execution);
    }

    private void createExecutionAttachments(Execution execution) {
        if (execution.getId() == null) {
            return;
        }

        String description = execution.getDescription();

        if (description == null || description.isEmpty()) {
            return;
        }

        String html =
                attachmentManagerService.handleRichTextAttachments(
                        description, execution.getAttachmentList());

        if (!description.equals(html)) {
            execution.setDescription(html);
        }
    }

    // This method is responsible for create execution steps by parsing the script
    // For a standard test case we do that job directly in model but for scripted test case we can't
    // the model mustn't have a parser as dependency, and we don't want to hack the original tests
    // case by detaching him from hibernate session and add virtual steps
    private void createExecutionStepsForScriptedTestCase(ScriptedExecution scriptedExecution) {

        ConsumerForScriptedTestCaseVisitor testCaseVisitor =
                new ConsumerForScriptedTestCaseVisitor(
                        scriptedTestCase ->
                                scriptedTestCaseExecutionHelper.createExecutionStepsForScriptedTestCase(
                                        scriptedExecution));

        scriptedExecution.getReferencedTestCase().accept(testCaseVisitor);
    }

    private void createCustomFieldsForExecutionAndExecutionSteps(Execution execution) {
        customFieldValueService.createAllCustomFieldValues(execution, execution.getProject());
        customFieldValueService.createAllCustomFieldValues(
                execution.getSteps(), execution.getProject());
    }

    // SQUASH-597 : no cuf in keyword step, only in execution
    private void createCustomAndDenormalizedFieldsForExecution(Execution execution) {
        customFieldValueService.createAllCustomFieldValues(execution, execution.getProject());
        createDenormalizedFieldsForExecution(execution);
    }

    private void createDenormalizedFieldsForExecutionAndExecutionSteps(Execution execution) {
        createDenormalizedFieldsForExecution(execution);
        denormalizedFieldValueService.createAllDenormalizedFieldValuesForSteps(execution);
    }

    private void createDenormalizedFieldsForExecution(Execution execution) {
        LOGGER.debug("Create denormalized fields for Execution {}", execution.getId());

        TestCase sourceTC = execution.getReferencedTestCase();
        denormalizedFieldValueService.createAllDenormalizedFieldValues(sourceTC, execution);
    }

    /* ************************* private stuffs ************************* */

    @Override
    @PreAuthorize(READ_TC_OR_ROLE_ADMIN)
    public List<Iteration> findIterationContainingTestCase(long testCaseId) {
        return iterationDao.findAllIterationContainingTestCase(testCaseId);
    }

    @Override
    @PreAuthorize(READ_ITERATION_OR_ROLE_ADMIN)
    public StatisticsBundle gatherIterationStatisticsBundle(
            long iterationId, boolean isLastExecutionScope) {
        return statisticsService.gatherIterationStatisticsBundle(
                Arrays.asList(iterationId), isLastExecutionScope);
    }

    @Override
    public Execution updateExecutionFromTc(long executionId) {

        Optional<Execution> optExec = executionDao.findById(executionId);
        if (optExec.isEmpty()) {
            throw new ExecutionWasDeleted();
        }
        Execution exec = optExec.get();
        if (exec.getReferencedTestCase() != null && exec.getReferencedTestCase().getSteps().isEmpty()) {
            throw new ExecutionHasNoStepsException();
        }

        int order = exec.getExecutionOrder();
        IterationTestPlanItem itpi = exec.getTestPlan();
        executionModificationService.deleteExecution(exec);

        Execution execution = createExec(itpi, null, null);
        itpi.addExecutionAtPos(execution, order);
        operationsAfterAddingExec(execution);
        return execution;
    }
}
