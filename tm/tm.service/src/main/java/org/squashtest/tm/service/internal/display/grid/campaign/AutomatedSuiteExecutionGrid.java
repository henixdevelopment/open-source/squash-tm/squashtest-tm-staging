/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.campaign;

import static org.jooq.impl.DSL.concat;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_EXECUTION_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_ENVIRONMENT_TAG;
import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_ENVIRONMENT_VARIABLE;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.tables.AutomatedSuite.AUTOMATED_SUITE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ENVIRONMENT_TAGS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ENVIRONMENT_VARIABLES;

import java.util.Arrays;
import java.util.List;
import org.jooq.AggregateFunction;
import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.SelectHavingStep;
import org.jooq.SortField;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.squashtest.tm.domain.environmenttag.DenormalizedEnvironmentHolderType;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;

public class AutomatedSuiteExecutionGrid extends AbstractGrid {
    private static final String ALIAS_EXTENDER_ID = "ALIAS_EXTENDER_ID";
    private static final String EXTENDER_ID = "EXTENDER_ID";
    private static final String AUTOMATED_EXECUTION_EXTENDER_TYPE =
            DenormalizedEnvironmentHolderType.AUTOMATED_EXECUTION_EXTENDER.name();
    private static final String ENVIRONMENT_VARIABLE = "ENVIRONMENT_VARIABLE";
    private static final String ENVIRONMENT_TAG = "ENVIRONMENT_TAG";
    private static final String TEST_TECHNOLOGY = "TEST_TECHNOLOGY";
    private final String automatedSuiteId;

    public AutomatedSuiteExecutionGrid(String automatedSuiteId) {
        this.automatedSuiteId = automatedSuiteId;
    }

    @Override
    protected List<GridColumn> getColumns() {
        return Arrays.asList(
                new GridColumn(EXECUTION.EXECUTION_STATUS),
                new GridColumn(EXECUTION.NAME),
                new GridColumn(EXECUTION.REFERENCE),
                new GridColumn(EXECUTION.DATASET_LABEL),
                new GridColumn(EXECUTION.LAST_EXECUTED_ON),
                new GridColumn(EXECUTION.EXECUTION_ID),
                new GridColumn(AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID),
                new GridColumn(getDenormalizedEnvironmentVariables().field(ENVIRONMENT_VARIABLES)),
                new GridColumn(getDenormalizedEnvironmentTags().field(ENVIRONMENT_TAGS)),
                new GridColumn(AUTOMATED_EXECUTION_EXTENDER.DURATION),
                new GridColumn(AUTOMATED_EXECUTION_EXTENDER.FLAG));
    }

    @Override
    protected Table<?> getTable() {
        SelectHavingStep<?> environmentVariables = getDenormalizedEnvironmentVariables();
        SelectHavingStep<?> environmentTags = getDenormalizedEnvironmentTags();

        return AUTOMATED_SUITE
                .innerJoin(AUTOMATED_EXECUTION_EXTENDER)
                .on(AUTOMATED_SUITE.SUITE_ID.eq(AUTOMATED_EXECUTION_EXTENDER.SUITE_ID))
                .innerJoin(EXECUTION)
                .on(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                .leftJoin(environmentVariables)
                .on(
                        AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID.eq(
                                environmentVariables.field(ALIAS_EXTENDER_ID, Long.class)))
                .leftJoin(environmentTags)
                .on(
                        AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID.eq(
                                environmentTags.field(ALIAS_EXTENDER_ID, Long.class)))
                .leftJoin(ITEM_TEST_PLAN_EXECUTION)
                .on(EXECUTION.EXECUTION_ID.eq(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID))
                .leftJoin(ITEM_TEST_PLAN_LIST)
                .on(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                .leftJoin(TEST_SUITE_TEST_PLAN_ITEM)
                .on(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID));
    }

    private SelectHavingStep<?> getDenormalizedEnvironmentVariables() {
        Table<?> result =
                DSL.select(
                                AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID.as(EXTENDER_ID),
                                concat(
                                                DENORMALIZED_ENVIRONMENT_VARIABLE.NAME,
                                                DSL.val(" : "),
                                                DENORMALIZED_ENVIRONMENT_VARIABLE.VALUE)
                                        .as(ENVIRONMENT_VARIABLE))
                        .from(AUTOMATED_EXECUTION_EXTENDER)
                        .innerJoin(DENORMALIZED_ENVIRONMENT_VARIABLE)
                        .on(
                                AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID.eq(
                                        DENORMALIZED_ENVIRONMENT_VARIABLE.HOLDER_ID))
                        .where(
                                DENORMALIZED_ENVIRONMENT_VARIABLE.HOLDER_TYPE.eq(AUTOMATED_EXECUTION_EXTENDER_TYPE))
                        .asTable();

        return DSL.select(
                        result.field(EXTENDER_ID).as(ALIAS_EXTENDER_ID),
                        DSL.groupConcat(result.field(ENVIRONMENT_VARIABLE))
                                .separator(", ")
                                .as(ENVIRONMENT_VARIABLES))
                .from(result)
                .groupBy(result.field(EXTENDER_ID));
    }

    private SelectHavingStep<?> getDenormalizedEnvironmentTags() {
        Table<?> result =
                DSL.select(
                                AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID.as(EXTENDER_ID),
                                DENORMALIZED_ENVIRONMENT_TAG.VALUE.as(ENVIRONMENT_TAG),
                                AUTOMATED_EXECUTION_EXTENDER.TEST_TECHNOLOGY.as(TEST_TECHNOLOGY))
                        .from(AUTOMATED_EXECUTION_EXTENDER)
                        .leftJoin(DENORMALIZED_ENVIRONMENT_TAG)
                        .on(
                                AUTOMATED_EXECUTION_EXTENDER
                                        .EXTENDER_ID
                                        .eq(DENORMALIZED_ENVIRONMENT_TAG.HOLDER_ID)
                                        .and(
                                                DENORMALIZED_ENVIRONMENT_TAG.HOLDER_TYPE.eq(
                                                        AUTOMATED_EXECUTION_EXTENDER_TYPE)))
                        .asTable();

        Field<String> testTechnologyField = result.field(TEST_TECHNOLOGY).coerce(String.class);
        Condition containTags = DSL.count(result.field(ENVIRONMENT_TAG)).greaterThan(0);
        AggregateFunction<String> concatTags =
                DSL.groupConcat(result.field(ENVIRONMENT_TAG)).separator(", ");

        Field<String> withDenormalizedTags =
                DSL.when(
                                DSL.length(testTechnologyField).greaterThan(0),
                                concatTags.concat(DSL.val(", "), testTechnologyField))
                        .otherwise(concatTags);

        return DSL.select(
                        result.field(EXTENDER_ID).as(ALIAS_EXTENDER_ID),
                        DSL.when(containTags, withDenormalizedTags)
                                .otherwise(testTechnologyField)
                                .as(ENVIRONMENT_TAGS))
                .from(result)
                .where(
                        DSL.length(testTechnologyField)
                                .greaterThan(0)
                                .or(result.field(ENVIRONMENT_TAG).isNotNull()))
                .groupBy(result.field(EXTENDER_ID), result.field(TEST_TECHNOLOGY));
    }

    @Override
    protected Field<?> getIdentifier() {
        return EXECUTION.EXECUTION_ID;
    }

    @Override
    protected SortField<?> getDefaultOrder() {
        return DSL.when(
                        AUTOMATED_SUITE.ITERATION_ID.isNotNull(), ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ORDER)
                .otherwise(TEST_SUITE_TEST_PLAN_ITEM.TEST_PLAN_ORDER)
                .asc();
    }

    @Override
    protected Field<?> getProjectIdentifier() {
        return null;
    }

    @Override
    protected Condition craftInvariantFilter() {
        return AUTOMATED_SUITE.SUITE_ID.eq(this.automatedSuiteId);
    }
}
