/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion.jdbc;

import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_FOLDER;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT_GROUP;

import java.util.Collection;
import java.util.Set;
import java.util.UUID;
import javax.persistence.EntityManager;
import org.jooq.DSLContext;
import org.springframework.stereotype.Component;
import org.squashtest.tm.service.internal.attachment.AttachmentRepository;

@Component
public class JdbcCampaignDeletionHandlerFactory {

    private final DSLContext dslContext;
    private final AttachmentRepository attachmentRepository;
    private final JdbcBatchReorderHelper reorderHelper;
    private final JdbcIterationDeletionHandlerFactory iterationDeletionHandlerFactory;
    private final JdbcSprintDeletionHandlerFactory sprintDeletionHandlerFactory;
    private final EntityManager entityManager;

    public JdbcCampaignDeletionHandlerFactory(
            DSLContext dslContext,
            EntityManager entityManager,
            AttachmentRepository attachmentRepository,
            JdbcBatchReorderHelper reorderHelper,
            JdbcIterationDeletionHandlerFactory iterationDeletionHandlerFactory,
            JdbcSprintDeletionHandlerFactory sprintDeletionHandlerFactory) {
        this.dslContext = dslContext;
        this.attachmentRepository = attachmentRepository;
        this.reorderHelper = reorderHelper;
        this.iterationDeletionHandlerFactory = iterationDeletionHandlerFactory;
        this.sprintDeletionHandlerFactory = sprintDeletionHandlerFactory;
        this.entityManager = entityManager;
    }

    public JdbcCampaignDeletionHandler build(Collection<Long> nodeIds) {
        Set<Long> folderIds = findFolderIds(nodeIds);
        Set<Long> campaignIds = findCampaignIds(nodeIds);
        String operationId = UUID.randomUUID().toString();
        Set<Long> iterationIds = findIterationIds(campaignIds);
        JdbcIterationDeletionHandler iterationDeletionHandler =
                iterationDeletionHandlerFactory.build(iterationIds, operationId);
        Set<Long> sprintGroupIds = findSprintGroupIds(nodeIds);
        Set<Long> sprintIds = findSprintIds(nodeIds);
        JdbcSprintDeletionHandler sprintDeletionHandler =
                sprintDeletionHandlerFactory.build(sprintIds, operationId);

        return new JdbcCampaignDeletionHandler(
                folderIds,
                campaignIds,
                sprintGroupIds,
                sprintIds,
                dslContext,
                entityManager,
                attachmentRepository,
                reorderHelper,
                iterationDeletionHandler,
                sprintDeletionHandler,
                operationId);
    }

    private Set<Long> findFolderIds(Collection<Long> nodeIds) {
        return dslContext
                .select(CAMPAIGN_FOLDER.CLN_ID)
                .from(CAMPAIGN_FOLDER)
                .where(CAMPAIGN_FOLDER.CLN_ID.in(nodeIds))
                .fetchSet(CAMPAIGN_FOLDER.CLN_ID);
    }

    private Set<Long> findCampaignIds(Collection<Long> nodeIds) {
        return dslContext
                .select(CAMPAIGN.CLN_ID)
                .from(CAMPAIGN)
                .where(CAMPAIGN.CLN_ID.in(nodeIds))
                .fetchSet(CAMPAIGN.CLN_ID);
    }

    private Set<Long> findIterationIds(Collection<Long> campaignIds) {
        return dslContext
                .select(CAMPAIGN_ITERATION.ITERATION_ID)
                .from(CAMPAIGN_ITERATION)
                .where(CAMPAIGN_ITERATION.CAMPAIGN_ID.in(campaignIds))
                .fetchSet(CAMPAIGN_ITERATION.ITERATION_ID);
    }

    private Set<Long> findSprintGroupIds(Collection<Long> nodeIds) {
        return dslContext
                .select(SPRINT_GROUP.CLN_ID)
                .from(SPRINT_GROUP)
                .where(SPRINT_GROUP.CLN_ID.in(nodeIds))
                .fetchSet(SPRINT_GROUP.CLN_ID);
    }

    private Set<Long> findSprintIds(Collection<Long> nodeIds) {
        return dslContext
                .select(SPRINT.CLN_ID)
                .from(SPRINT)
                .where(SPRINT.CLN_ID.in(nodeIds))
                .fetchSet(SPRINT.CLN_ID);
    }
}
