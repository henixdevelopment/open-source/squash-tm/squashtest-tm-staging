/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.supervision;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import org.squashtest.tm.core.foundation.lang.MathsUtils;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.domain.testautomation.AutomatedSuite;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.service.testautomation.model.AutomatedSuiteWithSquashAutomAutomatedITPIs;
import org.squashtest.tm.service.testautomation.supervision.model.AutomatedSuiteOverview;
import org.squashtest.tm.service.testautomation.supervision.model.ExecutionsOverview;
import org.squashtest.tm.service.testautomation.supervision.model.SquashAutomExecutionView;
import org.squashtest.tm.service.testautomation.supervision.model.TfExecutionView;

public final class AutomatedExecutionViewUtils {

    private AutomatedExecutionViewUtils() {
        super();
    }

    public static AutomatedSuiteOverview buildFirstAutomatedSuiteOverview(
            AutomatedSuiteWithSquashAutomAutomatedITPIs suite) {
        Collection<AutomatedExecutionExtender> executions = suite.getSuite().getExecutionExtenders();

        Collection<AutomatedExecutionExtender> tfExecutions =
                executions.stream().filter(exec -> Objects.nonNull(exec.getAutomatedTest())).toList();
        ExecutionsOverview<TfExecutionView> tfOverview = generateTfOverview(tfExecutions);

        Collection<IterationTestPlanItem> items = suite.getSquashAutomAutomatedItems();
        ExecutionsOverview<SquashAutomExecutionView> automOverview =
                generateSquashAutomOverviewFromItpis(items);

        return new AutomatedSuiteOverview(
                suite.getSuite().getId(),
                tfOverview.progression(),
                tfOverview.executionViews(),
                automOverview.progression(),
                automOverview.executionViews());
    }

    public static AutomatedSuiteOverview buildAutomatedSuiteOverview(AutomatedSuite suite) {
        Collection<AutomatedExecutionExtender> executions = suite.getExecutionExtenders();

        Collection<AutomatedExecutionExtender> tfExecutions =
                executions.stream().filter(exec -> Objects.nonNull(exec.getAutomatedTest())).toList();
        ExecutionsOverview<TfExecutionView> tfOverview = generateTfOverview(tfExecutions);

        Collection<AutomatedExecutionExtender> automExecutions =
                executions.stream().filter(exec -> Objects.isNull(exec.getAutomatedTest())).toList();
        ExecutionsOverview<SquashAutomExecutionView> automOverview =
                generateSquashAutomOverview(automExecutions);

        return new AutomatedSuiteOverview(
                suite.getId(),
                tfOverview.progression(),
                tfOverview.executionViews(),
                automOverview.progression(),
                automOverview.executionViews());
    }

    private static ExecutionsOverview<TfExecutionView> generateTfOverview(
            Collection<AutomatedExecutionExtender> execExtenders) {
        List<TfExecutionView> executionsViews = new ArrayList<>(execExtenders.size());
        int totalExecutions = execExtenders.size();
        int totalTerminated = 0;
        for (AutomatedExecutionExtender execExtender : execExtenders) {
            Execution execution = execExtender.getExecution();
            if (execution.getExecutionStatus().isTerminatedStatus()) {
                totalTerminated++;
            }
            TfExecutionView execView = convertExecExtenderToTfExecutionView(execExtender);
            executionsViews.add(execView);
        }
        int progression = percentProgression(totalTerminated, totalExecutions);
        return new ExecutionsOverview<>(progression, executionsViews);
    }

    private static ExecutionsOverview<SquashAutomExecutionView> generateSquashAutomOverview(
            Collection<AutomatedExecutionExtender> execExtenders) {
        List<SquashAutomExecutionView> executionsViews = new ArrayList<>(execExtenders.size());
        int totalTerminated = 0;
        for (AutomatedExecutionExtender execExtender : execExtenders) {
            Execution execution = execExtender.getExecution();
            if (execution.getExecutionStatus().isTerminatedStatus()) {
                totalTerminated++;
            }
            SquashAutomExecutionView execView =
                    convertExecExtenderToSquashAutomExecutionView(execExtender);
            executionsViews.add(execView);
        }
        return new ExecutionsOverview<>(totalTerminated, executionsViews);
    }

    private static ExecutionsOverview<SquashAutomExecutionView> generateSquashAutomOverviewFromItpis(
            Collection<IterationTestPlanItem> iterationTestPlanItems) {
        List<SquashAutomExecutionView> executionViews = new ArrayList<>(iterationTestPlanItems.size());
        int totalTerminated = 0;
        for (IterationTestPlanItem itpi : iterationTestPlanItems) {
            if (itpi.getExecutionStatus().isTerminatedStatus()) {
                totalTerminated++;
            }
            SquashAutomExecutionView execView = convertItpiToSquashAutomExecutionView(itpi);
            executionViews.add(execView);
        }
        return new ExecutionsOverview<>(totalTerminated, executionViews);
    }

    private static int percentProgression(int totalTerminated, int totalExec) {
        if (totalExec == 0) {
            return 100;
        }
        return MathsUtils.percent(totalTerminated, totalExec);
    }

    public static TfExecutionView convertExecExtenderToTfExecutionView(
            AutomatedExecutionExtender autoExec) {
        return new TfExecutionView(
                autoExec.getExecution().getId(),
                autoExec.getExecution().getName(),
                autoExec.getExecution().getExecutionStatus(),
                autoExec.getNodeName(),
                autoExec.getAutomatedProject().getLabel());
    }

    public static SquashAutomExecutionView convertExecExtenderToSquashAutomExecutionView(
            AutomatedExecutionExtender autoExec) {
        Execution execution = autoExec.getExecution();
        return new SquashAutomExecutionView(
                execution.getTestPlan().getId(),
                execution.getName(),
                execution.getDatasetLabel(),
                execution.getExecutionStatus(),
                execution.getReferencedTestCase().getProject().getTestAutomationServer().getName());
    }

    public static SquashAutomExecutionView convertItpiToSquashAutomExecutionView(
            IterationTestPlanItem itpi) {
        TestCase referencedTestCase = itpi.getReferencedTestCase();
        return new SquashAutomExecutionView(
                itpi.getId(),
                referencedTestCase.getName(),
                Objects.nonNull(itpi.getReferencedDataset()) ? itpi.getReferencedDataset().getName() : null,
                ExecutionStatus.READY,
                referencedTestCase.getProject().getTestAutomationServer().getName());
    }
}
