/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;
import static org.jooq.impl.DSL.select;
import static org.jooq.impl.DSL.when;
import static org.squashtest.tm.jooq.domain.Tables.HIGH_LEVEL_REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_FOLDER;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_SYNC_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.RLN_RELATIONSHIP_CLOSURE;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_REQUIREMENT_CRITICALITY_BY_TESTCASE_IDS;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_REQUIREMENT_FOR_REQUIREMENT_ADDITION;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_REQUIREMENT_FOR_VERSION_ADDITION;
import static org.squashtest.tm.service.internal.repository.ParameterNames.ID;
import static org.squashtest.tm.service.internal.repository.ParameterNames.IDS;

import com.querydsl.core.group.GroupBy;
import com.querydsl.jpa.hibernate.HibernateQuery;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.inject.Inject;
import org.hibernate.Session;
import org.hibernate.jpa.QueryHints;
import org.hibernate.query.Query;
import org.hibernate.type.LongType;
import org.jooq.CommonTableExpression;
import org.jooq.DSLContext;
import org.jooq.Record2;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.IdentifiedUtil;
import org.squashtest.tm.domain.requirement.ExportRequirementData;
import org.squashtest.tm.domain.requirement.ManagementMode;
import org.squashtest.tm.domain.requirement.QRequirement;
import org.squashtest.tm.domain.requirement.QRequirementSyncExtender;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementCriticality;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.service.internal.library.HibernatePathService;
import org.squashtest.tm.service.internal.repository.EntityGraphName;
import org.squashtest.tm.service.internal.repository.JpaQueryString;
import org.squashtest.tm.service.internal.repository.RequirementDao;
import org.squashtest.tm.service.internal.repository.hibernate.loaders.EntityGraphQueryBuilder;

@Repository
public class HibernateRequirementDao extends HibernateEntityDao<Requirement>
        implements RequirementDao {

    private static final String UNCHECKED = "unchecked";

    private static final String REQUIREMENT_IDS = "requirementIds";
    private static final String NODE_IDS = "nodeIds";

    @Inject DSLContext dsl;

    private static final class SetRequirementsIdsParameterCallback
            implements SetQueryParametersCallback {
        private Collection<Long> requirementIds;

        private SetRequirementsIdsParameterCallback(Collection<Long> requirementIds) {
            this.requirementIds = requirementIds;
        }

        @Override
        public void setQueryParameters(Query query) {
            query.setParameterList(REQUIREMENT_IDS, requirementIds);
        }
    }

    @Override
    public List<Long> fetchHighLevelRequirementVersionIds(List<Long> requirementVersionIds) {
        return dsl.select(REQUIREMENT_VERSION.RES_ID)
                .from(REQUIREMENT_VERSION)
                .join(HIGH_LEVEL_REQUIREMENT)
                .on(REQUIREMENT_VERSION.REQUIREMENT_ID.eq(HIGH_LEVEL_REQUIREMENT.RLN_ID))
                .where(REQUIREMENT_VERSION.RES_ID.in(requirementVersionIds))
                .fetchInto(Long.class);
    }

    @Override
    public List<Requirement> findChildrenRequirements(long requirementId) {
        SetQueryParametersCallback setId = new SetIdParameter("requirementId", requirementId);
        return executeListNamedQuery("requirement.findChildrenRequirements", setId);
    }

    @Override
    public List<Long> findByRequirementVersion(Collection<Long> versionIds) {
        if (!versionIds.isEmpty()) {
            Query q = currentSession().getNamedQuery("requirement.findByRequirementVersion");
            q.setParameterList("versionIds", versionIds, LongType.INSTANCE);
            return q.list();
        } else {
            return new ArrayList<>();
        }
    }

    /* ----------------------------------------------------EXPORT METHODS----------------------------------------- */

    @Override
    public List<ExportRequirementData> findRequirementToExportFromNodes(List<Long> params) {
        if (!params.isEmpty()) {
            return doFindRequirementToExportFromNodes(params);
        } else {
            return Collections.emptyList();
        }
    }

    private List<ExportRequirementData> doFindRequirementToExportFromNodes(List<Long> params) {
        // find root leafs
        List<Requirement> rootReqs = findRootContentRequirement(params);
        List<Long> rootReqsIds = IdentifiedUtil.extractIds(rootReqs);
        // find all leafs contained in param ids and contained by folders or requirements in param ids
        Set<Long> nonRootReqNodesIds = new HashSet<>();
        List<Long> listReqNodeId = findDescendantRequirementIds(params);
        nonRootReqNodesIds.addAll(listReqNodeId);
        List<Long> listParentRequirementIds = findRequirementParents(params);
        nonRootReqNodesIds.addAll(listParentRequirementIds);
        nonRootReqNodesIds.addAll(params);
        nonRootReqNodesIds.removeAll(rootReqsIds);

        // Case 1. Only root leafs are found
        if (nonRootReqNodesIds.isEmpty()) {
            return formatExportResult(rootReqs, new ArrayList<Object[]>(0));
        }

        // Case 2. More than root leafs are found
        List<Requirement> nonRootReqs = findAllByIds(nonRootReqNodesIds);
        Collection<Object[]> listObject = addPathInfos(nonRootReqs);

        return formatExportResult(rootReqs, listObject);
    }

    @SuppressWarnings(UNCHECKED)
    private List<Long> findRequirementParents(List<Long> params) {
        Query query = currentSession().getNamedQuery("requirement.findRequirementParentIds");
        query.setParameterList(NODE_IDS, params);
        return query.list();
    }

    @SuppressWarnings(UNCHECKED)
    @Override
    public List<Long> findDescendantRequirementIds(Collection<Long> params) {
        Query query = currentSession().getNamedQuery("requirement.findRequirementDescendantIds");
        query.setParameterList(NODE_IDS, params);
        return query.list();
    }

    /**
     * Returns a list of objects that holds infos for requirements and their position in requirement
     * hierarchy
     *
     * @param requirements
     * @return list of object with, for all objects obj[0] = requirement , obj[1] folder path , obj[2]
     *     requirement path
     */
    private Collection<Object[]> addPathInfos(final List<Requirement> requirements) {
        if (!requirements.isEmpty()) {

            Map<Long, Object[]> exportInfosById = new HashMap<>(requirements.size());
            for (Requirement requirement : requirements) {
                Object[] exportInfo = new Object[3];
                exportInfo[0] = requirement;
                exportInfo[1] = "";
                exportInfo[2] = "";
                exportInfosById.put(requirement.getId(), exportInfo);
            }
            SetQueryParametersCallback newCallBack1 =
                    new SetRequirementsIdsParameterCallback(exportInfosById.keySet());
            Session session = currentSession();

            Query q = session.getNamedQuery("requirement.findReqPaths");
            newCallBack1.setQueryParameters(q);
            List<Object[]> idAndReqPaths = q.list();
            addPathInfosToExportInfos(exportInfosById, idAndReqPaths, 2);
            q = session.getNamedQuery("requirement.findFolderPaths");
            newCallBack1.setQueryParameters(q);
            List<Object[]> folderPaths = q.list();
            addPathInfosToExportInfos(exportInfosById, folderPaths, 1);

            return exportInfosById.values();

        } else {
            return Collections.emptyList();
        }
    }

    public void addPathInfosToExportInfos(
            Map<Long, Object[]> reqAndFolderPathAndReqPathById,
            List<Object[]> idAndPaths,
            int pathInfoIndex) {
        for (Object[] idAndPath : idAndPaths) {
            Long reqId = (Long) idAndPath[0];
            String reqPath = (String) idAndPath[1];
            String escapedPath = HibernatePathService.escapePath(reqPath);
            Object[] reqAndFolderPathAndReqPath = reqAndFolderPathAndReqPathById.get(reqId);
            reqAndFolderPathAndReqPath[pathInfoIndex] = escapedPath;
        }
    }

    private List<ExportRequirementData> formatExportResult(
            Collection<Requirement> rootReq, Collection<Object[]> nonRootReq) {
        List<ExportRequirementData> exportList = new ArrayList<>();

        for (Requirement requirement : rootReq) {
            ExportRequirementData erd = new ExportRequirementData(requirement, "", "");
            exportList.add(erd);
        }

        for (Object[] exportReqInfos : nonRootReq) {
            Requirement requirement = (Requirement) exportReqInfos[0];
            String folderPath = (String) exportReqInfos[1];
            String requirementPath = (String) exportReqInfos[2];
            ExportRequirementData erd =
                    new ExportRequirementData(requirement, folderPath, requirementPath);
            exportList.add(erd);
        }
        Collections.sort(exportList, new ExportRequirementDataComparator());
        return exportList;
    }

    private static final class ExportRequirementDataComparator
            implements Comparator<ExportRequirementData> {

        @Override
        public int compare(ExportRequirementData o1, ExportRequirementData o2) {
            int folderCompare = o1.getFolderName().compareTo(o2.getFolderName());
            if (folderCompare != 0) {
                return folderCompare;
            } else {
                return o1.getRequirementParentPath().compareTo(o2.getRequirementParentPath());
            }
        }
    }

    private List<Requirement> findRootContentRequirement(final List<Long> params) {
        if (!params.isEmpty()) {
            SetQueryParametersCallback newCallBack1 = new SetParamIdsParametersCallback(params);
            return executeListNamedQuery("requirement.findRootContentRequirement", newCallBack1);
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public List<ExportRequirementData> findRequirementToExportFromLibrary(
            final List<Long> libraryIds) {

        if (!libraryIds.isEmpty()) {
            SetLibraryIdsCallback newCallBack1 = new SetLibraryIdsCallback(libraryIds);
            List<Long> result = executeListNamedQuery("requirement.findAllRootContent", newCallBack1);

            return findRequirementToExportFromNodes(result);
        } else {
            return Collections.emptyList();
        }
    }

    /* ----------------------------------------------------/EXPORT METHODS----------------------------------------- */

    @SuppressWarnings(UNCHECKED)
    @Override
    public List<RequirementCriticality> findDistinctRequirementsCriticalitiesVerifiedByTestCases(
            Set<Long> testCasesIds) {
        if (!testCasesIds.isEmpty()) {
            Query query =
                    currentSession()
                            .getNamedQuery(
                                    "requirementVersion.findDistinctRequirementsCriticalitiesVerifiedByTestCases");
            query.setParameterList("testCasesIds", testCasesIds);
            return query.list();
        } else {
            return Collections.emptyList();
        }
    }

    @SuppressWarnings(UNCHECKED)
    @Override
    public List<RequirementCriticality> findDistinctRequirementsCriticalities(
            List<Long> requirementVersionsIds) {
        if (!requirementVersionsIds.isEmpty()) {
            Query query =
                    currentSession()
                            .getNamedQuery("requirementVersion.findDistinctRequirementsCriticalities");
            query.setParameterList("requirementsIds", requirementVersionsIds);
            return query.list();
        } else {
            return Collections.emptyList();
        }
    }

    @SuppressWarnings(UNCHECKED)
    @Override
    public List<RequirementVersion> findVersions(Long requirementId) {
        Query query = currentSession().getNamedQuery("requirement.findVersions");
        query.setParameter("requirementId", requirementId);
        return query.list();
    }

    @SuppressWarnings(UNCHECKED)
    @Override
    public List<RequirementVersion> findVersionsForAll(List<Long> requirementIds) {
        if (!requirementIds.isEmpty()) {
            Query query = currentSession().getNamedQuery("requirement.findVersionsForAll");
            query.setParameterList(REQUIREMENT_IDS, requirementIds, LongType.INSTANCE);
            return query.list();
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public Requirement findByContent(final Requirement child) {
        SetQueryParametersCallback callback = new SetNodeContentParameter(child);

        return executeEntityNamedQuery("requirement.findByContent", callback);
    }

    @Override
    public List<Object[]> findAllParentsOf(List<Long> requirementIds) {
        if (!requirementIds.isEmpty()) {
            List<Object[]> allpairs = new ArrayList<>(requirementIds.size());

            List<Object[]> libraryReqs =
                    executeListNamedQuery(
                            "requirement.findAllLibraryParents",
                            new SetRequirementsIdsParameterCallback(requirementIds));
            List<Object[]> folderReqs =
                    executeListNamedQuery(
                            "requirement.findAllFolderParents",
                            new SetRequirementsIdsParameterCallback(requirementIds));
            List<Object[]> reqReqs =
                    executeListNamedQuery(
                            "requirement.findAllRequirementParents",
                            new SetRequirementsIdsParameterCallback(requirementIds));

            allpairs.addAll(libraryReqs);
            allpairs.addAll(folderReqs);
            allpairs.addAll(reqReqs);

            return allpairs;
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public List<Long> findNonBoundRequirement(Collection<Long> nodeIds, Long milestoneId) {
        if (!nodeIds.isEmpty()) {
            Query q = currentSession().getNamedQuery("requirement.findNonBoundRequirement");
            q.setParameterList(NODE_IDS, nodeIds, LongType.INSTANCE);
            q.setParameter("milestoneId", milestoneId);
            return q.list();
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Long> filterRequirementHavingManyVersions(Collection<Long> requirementIds) {
        if (!requirementIds.isEmpty()) {
            Query q = currentSession().getNamedQuery("requirement.findRequirementHavingManyVersions");
            q.setParameterList(REQUIREMENT_IDS, requirementIds, LongType.INSTANCE);
            return q.list();
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Long> findAllRequirementsIdsByNodes(Collection<Long> nodeIds) {
        if (!nodeIds.isEmpty()) {
            Query q = currentSession().getNamedQuery("requirement.findAllRequirementIdsByNodesId");
            q.setParameterList(NODE_IDS, nodeIds, LongType.INSTANCE);
            return q.list();
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Long> findIdsVersionsForAll(List<Long> requirementIds) {
        if (!requirementIds.isEmpty()) {
            Query q = currentSession().getNamedQuery("requirement.findVersionsIdsForAll");
            q.setParameterList(REQUIREMENT_IDS, requirementIds, LongType.INSTANCE);
            return q.list();
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public Long findNodeIdByRemoteKey(String remoteKey, String projectName) {
        Query q =
                currentSession()
                        .getNamedQuery("requirement.findNodeIdByRemoteKey")
                        .setParameter("key", remoteKey)
                        .setParameter("projectName", projectName);
        return (Long) q.uniqueResult();
    }

    @Override
    public Long findNodeIdByRemoteKeyAndRemoteSyncId(String remoteKey, Long remoteSyncId) {
        Query q =
                currentSession()
                        .getNamedQuery("requirement.findNodeIdByRemoteKeyAndSynchronisationId")
                        .setParameter("key", remoteKey)
                        .setParameter("remoteSynchronisationId", remoteSyncId);
        return (Long) q.uniqueResult();
    }

    @Override
    public Map<String, Long> findNodeIdsByRemoteKeys(
            Collection<String> remoteKeys, String projectName) {

        if (remoteKeys.isEmpty()) {
            return Collections.emptyMap();
        }

        QRequirement req = QRequirement.requirement;
        QRequirementSyncExtender sync = QRequirementSyncExtender.requirementSyncExtender;

        HibernateQuery<Map<String, Long>> query = new HibernateQuery<>(currentSession());

        return query
                .select(req.id)
                .from(req)
                .innerJoin(req.syncExtender, sync)
                .where(sync.remoteReqId.in(remoteKeys))
                .where(req.project.name.eq(projectName))
                .transform(GroupBy.groupBy(sync.remoteReqId).as(req.id));
    }

    @Override
    public List<Long> findAllRequirementIdsFromMilestones(Collection<Long> milestoneIds) {
        if (!milestoneIds.isEmpty()) {
            Query q = currentSession().getNamedQuery("requirement.findAllRequirementIdsFromMilestones");
            q.setParameterList("milestoneIds", milestoneIds, LongType.INSTANCE);
            return q.list();
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Long> findAllRequirementIdsFromMilestonesAndSelectedReqIds(
            Collection<Long> milestoneIds, Collection<Long> requirementIds) {
        if (!milestoneIds.isEmpty()) {
            Query q =
                    currentSession()
                            .getNamedQuery(
                                    "requirement.findAllRequirementVersionIdsFromMilestonesAndSelectedReqIds");
            q.setParameterList("milestoneIds", milestoneIds, LongType.INSTANCE);
            q.setParameterList(REQUIREMENT_IDS, requirementIds, LongType.INSTANCE);
            return q.list();
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public void setNativeManagementModeForSynchronizedRequirements(
            List<Long> remoteSynchronisationIds) {
        Collection<Long> reqIds = findSynchronizedRequirementIds(remoteSynchronisationIds);

        if (!reqIds.isEmpty()) {
            dsl.update(REQUIREMENT)
                    .set(REQUIREMENT.MODE, ManagementMode.NATIVE.name())
                    .where(REQUIREMENT.RLN_ID.in(reqIds))
                    .execute();
        }
    }

    private Set<Long> findSynchronizedRequirementIds(List<Long> remoteSynchronisationIds) {
        return dsl.select(REQUIREMENT.RLN_ID)
                .from(REQUIREMENT)
                .innerJoin(REQUIREMENT_SYNC_EXTENDER)
                .on(REQUIREMENT.RLN_ID.eq(REQUIREMENT_SYNC_EXTENDER.REQUIREMENT_ID))
                .where(REQUIREMENT_SYNC_EXTENDER.REMOTE_SYNCHRONISATION_ID.in(remoteSynchronisationIds))
                .fetchSet(REQUIREMENT.RLN_ID);
    }

    @Override
    public boolean isHighLevelRequirementVersion(Long requirementVersionId) {
        Long result =
                dsl.select(HIGH_LEVEL_REQUIREMENT.RLN_ID)
                        .from(REQUIREMENT_VERSION)
                        .leftJoin(HIGH_LEVEL_REQUIREMENT)
                        .on(REQUIREMENT_VERSION.REQUIREMENT_ID.eq(HIGH_LEVEL_REQUIREMENT.RLN_ID))
                        .where(REQUIREMENT_VERSION.RES_ID.eq(requirementVersionId))
                        .fetchOneInto(Long.class);

        return result != null;
    }

    @Override
    public Long findRequirementIdFromVersionId(Long requirementVersionId) {
        return dsl.selectDistinct(REQUIREMENT_VERSION.REQUIREMENT_ID)
                .from(REQUIREMENT_VERSION)
                .where(REQUIREMENT_VERSION.RES_ID.eq(requirementVersionId))
                .fetchOneInto(Long.class);
    }

    @Override
    public Long findRequirementCurrentVersionIdFromRequirementId(Long highLevelRequirementId) {
        return dsl.select(REQUIREMENT.CURRENT_VERSION_ID)
                .from(REQUIREMENT)
                .where(REQUIREMENT.RLN_ID.eq(highLevelRequirementId))
                .fetchOneInto(Long.class);
    }

    @Override
    public Set<Long> findRequirementCurrentVersionIdsFromRequirementIds(
            Collection<Long> requirementIds) {
        return dsl.select(REQUIREMENT.CURRENT_VERSION_ID)
                .from(REQUIREMENT)
                .where(REQUIREMENT.RLN_ID.in(requirementIds))
                .fetchSet(REQUIREMENT.CURRENT_VERSION_ID);
    }

    @Override
    public boolean checkIfRequirementIsChild(Long requirementId) {
        return dsl.fetchExists(
                dsl.select(REQUIREMENT.RLN_ID)
                        .from(RLN_RELATIONSHIP_CLOSURE)
                        .innerJoin(REQUIREMENT)
                        .on(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.eq(REQUIREMENT.RLN_ID))
                        .where(RLN_RELATIONSHIP_CLOSURE.DEPTH.eq((short) 1))
                        .and(RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID.eq(requirementId)));
    }

    @Override
    public Long findRequirementAncestorId(Long requirementId) {
        return dsl.select(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID)
                .from(RLN_RELATIONSHIP_CLOSURE)
                .where(RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID.eq(requirementId))
                .and(RLN_RELATIONSHIP_CLOSURE.DEPTH.eq((short) 1))
                .fetchOneInto(Long.class);
    }

    @Override
    public List<Long> findAllChildrenIdsFromRequirementId(Long requirementId) {
        return dsl.select(RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID)
                .from(RLN_RELATIONSHIP_CLOSURE)
                .where(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.eq(requirementId))
                .and(RLN_RELATIONSHIP_CLOSURE.DEPTH.ne((short) 0))
                .fetchInto(Long.class);
    }

    @Override
    public Requirement findByIdWithChildren(Long id) {

        return new EntityGraphQueryBuilder<>(
                        entityManager, Requirement.class, JpaQueryString.FIND_REQUIREMENT_BY_ID)
                .addAttributeNodes(EntityGraphName.CHILDREN)
                .addSubGraph(EntityGraphName.CHILDREN, EntityGraphName.SYNC_EXTENDER)
                .execute(Collections.singletonMap(ID, id));
    }

    @Override
    public List<Requirement> findRequirementsNodeRootAndInFolderByNodeIds(List<Long> libraryNodeIds) {
        final String cteName = "CTE";
        final String rlnId = "RLN_ID";
        final String isFolder = "IS_FOLDER";

        CommonTableExpression<Record2<Long, Boolean>> cte =
                name(cteName)
                        .fields(rlnId, isFolder)
                        .as(
                                select(
                                                REQUIREMENT_LIBRARY_NODE.RLN_ID,
                                                when(REQUIREMENT_FOLDER.RLN_ID.isNotNull(), Boolean.TRUE)
                                                        .otherwise(Boolean.FALSE))
                                        .from(REQUIREMENT_LIBRARY_NODE)
                                        .leftJoin(REQUIREMENT_FOLDER)
                                        .on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(REQUIREMENT_FOLDER.RLN_ID))
                                        .where(REQUIREMENT_LIBRARY_NODE.RLN_ID.in(libraryNodeIds)));

        List<Long> requirementIds =
                dsl.with(cte)
                        .select(REQUIREMENT.RLN_ID)
                        .from(REQUIREMENT)
                        .innerJoin(RLN_RELATIONSHIP_CLOSURE)
                        .on(REQUIREMENT.RLN_ID.eq(RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID))
                        .where(
                                RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.in(
                                        select(field(rlnId, Long.class))
                                                .from(name(cteName))
                                                .where(field(isFolder, Boolean.class).isTrue())))
                        .union(
                                select(field(rlnId, Long.class))
                                        .from(name(cteName))
                                        .where(field(isFolder, Boolean.class).isFalse()))
                        .fetchInto(Long.class);

        if (requirementIds.isEmpty()) {
            return Collections.emptyList();
        }

        return findAllByIds(requirementIds);
    }

    @Override
    public Map<Long, RequirementCriticality> findRequirementsCriticalityVerifiedByTestCases(
            Collection<Long> testCaseIds) {
        if (testCaseIds.isEmpty()) {
            return Collections.emptyMap();
        }

        try (Stream<Object[]> resultStream = getRequirementCriticalityByTCIdsStream(testCaseIds)) {
            return resultStream.collect(
                    Collectors.toMap(r -> (Long) r[0], r -> (RequirementCriticality) r[1]));
        }
    }

    private Stream<Object[]> getRequirementCriticalityByTCIdsStream(Collection<Long> testCaseIds) {
        return entityManager
                .createQuery(FIND_REQUIREMENT_CRITICALITY_BY_TESTCASE_IDS, Object[].class)
                .setParameter(IDS, testCaseIds)
                .getResultStream();
    }

    @Override
    public List<Requirement> loadForNodeAddition(Collection<Long> requirementIds) {
        return entityManager
                .createQuery(FIND_REQUIREMENT_FOR_REQUIREMENT_ADDITION, Requirement.class)
                .setParameter(IDS, requirementIds)
                .setHint(QueryHints.HINT_PASS_DISTINCT_THROUGH, false)
                .getResultList();
    }

    @Override
    public List<Requirement> loadForVersionsAddition(Collection<Long> requirementIds) {
        return entityManager
                .createQuery(FIND_REQUIREMENT_FOR_VERSION_ADDITION, Requirement.class)
                .setParameter(IDS, requirementIds)
                .setHint(QueryHints.HINT_PASS_DISTINCT_THROUGH, false)
                .getResultList();
    }
}
