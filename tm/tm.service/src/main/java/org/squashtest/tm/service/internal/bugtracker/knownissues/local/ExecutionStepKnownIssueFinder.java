/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.bugtracker.knownissues.local;

import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_EXECUTION_STEPS;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_STEP;
import static org.squashtest.tm.jooq.domain.Tables.ISSUE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;

import org.jooq.DSLContext;
import org.jooq.Record6;
import org.jooq.SelectHavingStep;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public class ExecutionStepKnownIssueFinder extends ExecutionKnownIssueFinder {

    public ExecutionStepKnownIssueFinder(DSLContext dsl) {
        super(dsl);
    }

    @Override
    protected SelectHavingStep<Record6<Long, Long, String, String, String, String>> selectKnownIssues(
            long executionStepId) {
        return dsl.select(
                        PROJECT.PROJECT_ID,
                        ISSUE.BUGTRACKER_ID,
                        ISSUE.REMOTE_ISSUE_ID,
                        DSL.groupConcatDistinct(EXECUTION.EXECUTION_ID),
                        DSL.groupConcatDistinct(DSL.ifnull(EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ORDER, -1)),
                        DSL.groupConcatDistinct(ISSUE.ISSUE_ID))
                .from(
                        BaseLocalKnownIssueFinder.getIssueToBugtrackerBindingJoin()
                                .innerJoin(EXECUTION_STEP)
                                .on(EXECUTION_STEP.ISSUE_LIST_ID.eq(ISSUE.ISSUE_LIST_ID))
                                .innerJoin(EXECUTION_EXECUTION_STEPS)
                                .on(
                                        EXECUTION_EXECUTION_STEPS
                                                .EXECUTION_ID
                                                .eq(EXECUTION.EXECUTION_ID)
                                                .and(
                                                        EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ID.eq(
                                                                EXECUTION_STEP.EXECUTION_STEP_ID))))
                .where(EXECUTION_STEP.EXECUTION_STEP_ID.eq(executionStepId))
                // Only shows if source project's current bugtracker is the bugtracker the issue was
                // reported with
                .and(ISSUE.BUGTRACKER_ID.eq(PROJECT.BUGTRACKER_ID))
                .groupBy(PROJECT.PROJECT_ID, ISSUE.BUGTRACKER_ID, ISSUE.REMOTE_ISSUE_ID);
    }
}
