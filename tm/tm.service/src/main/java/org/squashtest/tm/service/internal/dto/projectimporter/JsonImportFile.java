/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporter;

public enum JsonImportFile {
    PROJECT("project.json"),
    CUSTOM_FIELDS("custom_fields.json"),
    REQUIREMENT_FOLDERS("requirement_folders.json"),
    REQUIREMENTS("requirements.json"),
    TEST_CASE_FOLDERS("test_case_folders.json"),
    TEST_CASES("test_cases.json"),
    CALLED_TEST_CASES("called_test_cases.json"),
    CAMPAIGN_FOLDERS("campaign_folders.json"),
    CAMPAIGNS("campaigns.json"),
    ITERATIONS("iterations.json"),
    TEST_SUITES("test_suites.json"),
    EXECUTIONS("executions.json");

    private final String fileName;

    JsonImportFile(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }
}
