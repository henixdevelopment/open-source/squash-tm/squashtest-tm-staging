/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * A wrapper for the ExecutionStatus containing a list of attachments.
 *
 * @author akatz
 */
public class AutomatedExecutionState {

    @JsonProperty("testExecutionStatus")
    private TfTestExecutionStatus tfTestExecutionStatus;

    @JsonProperty("testStepExecutionStatuses")
    private Map<Integer, TfTestExecutionStatus> testStepExecutionStatuses;

    @JsonProperty("environmentTags")
    private List<String> environmentTags;

    @JsonProperty("environmentVariables")
    private Map<String, Object> environmentVariables;

    @JsonProperty("attachments")
    private List<Attachment> attachments;

    @JsonProperty("testTechnology")
    private String testTechnology;

    @JsonProperty("isLastUpdate")
    private boolean isLastUpdate;

    public AutomatedExecutionState() {}

    public AutomatedExecutionState(
            TfTestExecutionStatus tfTestExecutionStatus,
            Map<Integer, TfTestExecutionStatus> testStepExecutionStatuses,
            List<String> environmentTags,
            Map<String, Object> environmentVariables,
            List<Attachment> attachments,
            String testTechnology,
            boolean isLastUpdate) {
        this.tfTestExecutionStatus = tfTestExecutionStatus;
        this.testStepExecutionStatuses = testStepExecutionStatuses;
        this.environmentTags =
                Objects.nonNull(environmentTags) ? environmentTags : Collections.emptyList();
        this.environmentVariables =
                Objects.nonNull(environmentVariables) ? environmentVariables : Collections.emptyMap();
        this.attachments = attachments;
        this.testTechnology = testTechnology;
        this.isLastUpdate = isLastUpdate;
    }

    public TfTestExecutionStatus getTfTestExecutionStatus() {
        return tfTestExecutionStatus;
    }

    public Map<Integer, TfTestExecutionStatus> getTestStepExecutionStatuses() {
        return testStepExecutionStatuses;
    }

    public List<Attachment> getAttachments() {
        return attachments;
    }

    public Map<String, Object> getEnvironmentVariables() {
        return environmentVariables;
    }

    public List<String> getEnvironmentTags() {
        return environmentTags;
    }

    public String getTestTechnology() {
        return testTechnology;
    }

    public boolean isLastUpdate() {
        return isLastUpdate;
    }
}
