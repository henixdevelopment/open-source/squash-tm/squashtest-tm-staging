/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.campaign;

import java.util.Date;

public class IterationTestPlanItemDto {

    private Long id;
    private String label;
    private Long testCaseId;
    private Long datasetId;
    private Date lastExecutedOn;
    private Integer itemTestPlanOrder;

    public IterationTestPlanItemDto() {}

    public IterationTestPlanItemDto(Long id, Long testCaseId, Long datasetId, Date lastExecutedOn) {
        this.id = id;
        this.testCaseId = testCaseId;
        this.datasetId = datasetId;
        this.lastExecutedOn = lastExecutedOn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Long getTestCaseId() {
        return testCaseId;
    }

    public void setTestCaseId(Long testCaseId) {
        this.testCaseId = testCaseId;
    }

    public Long getDatasetId() {
        return datasetId;
    }

    public void setDatasetId(Long datasetId) {
        this.datasetId = datasetId;
    }

    public Date getLastExecutedOn() {
        return lastExecutedOn;
    }

    public void setLastExecutedOn(Date lastExecutedOn) {
        this.lastExecutedOn = lastExecutedOn;
    }

    public Integer getItemTestPlanOrder() {
        return itemTestPlanOrder;
    }

    public void setItemTestPlanOrder(Integer itemTestPlanOrder) {
        this.itemTestPlanOrder = itemTestPlanOrder;
    }
}
