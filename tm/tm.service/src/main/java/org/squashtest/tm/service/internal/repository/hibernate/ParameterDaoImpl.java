/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;
import static org.jooq.impl.DSL.select;
import static org.squashtest.tm.jooq.domain.Tables.CALL_TEST_STEP;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_STEPS;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_PARAMETERS_BY_TESTCASE_IDS;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_PARAMETERS_BY_TESTCASE_IDS_ORDER_BY_PARAM_NAME;
import static org.squashtest.tm.service.internal.repository.ParameterNames.CALLED_ID;
import static org.squashtest.tm.service.internal.repository.ParameterNames.CTE;
import static org.squashtest.tm.service.internal.repository.ParameterNames.IDS;
import static org.squashtest.tm.service.internal.repository.ParameterNames.SOURCE_ID;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.service.internal.repository.CustomParameterDao;
import org.squashtest.tm.service.internal.repository.hibernate.loaders.EntityGraphQueryBuilder;

public class ParameterDaoImpl implements CustomParameterDao {

    @PersistenceContext private EntityManager em;
    @Inject private DSLContext dslContext;

    @Override
    public List<Parameter> findAllParametersByTestCase(Long testcaseId) {

        List<Parameter> allParameters = new ArrayList<>();

        Set<Long> exploredTc = new HashSet<>();
        List<Long> srcTc = new LinkedList<>();
        List<Long> destTc;

        Query next = em.createNamedQuery("parameter.findTestCasesThatDelegatesParameters");

        srcTc.add(testcaseId);

        while (!srcTc.isEmpty()) {

            allParameters.addAll(findTestCaseParameters(srcTc));

            next.setParameter("srcIds", srcTc);
            destTc = next.getResultList();

            exploredTc.addAll(srcTc);
            srcTc = destTc;
            srcTc.removeAll(exploredTc);
        }

        return allParameters;
    }

    // note that this is the same queery than ParameterDao#findOwnParametersByTestCases
    // duplicate here because of the inheritance between the interfaces is what it is
    private List<Parameter> findTestCaseParameters(List<Long> testcaseIds) {

        Query query = em.createNamedQuery("Parameter.findOwnParametersByTestCases");
        query.setParameter("testCaseIds", testcaseIds);
        return query.getResultList();
    }

    @Override
    public Map<Long, List<Parameter>> findOwnParametersByTestCaseIds(Collection<Long> testCaseIds) {
        try (Stream<Object[]> resultStream = getParametersByTCIdsStream(testCaseIds)) {
            return resultStream.collect(
                    Collectors.groupingBy(
                            result -> (Long) result[0],
                            Collectors.mapping(result -> (Parameter) result[1], Collectors.toList())));
        }
    }

    @Override
    public Map<Long, List<String>> findOwnParametersNamesByTestCaseIds(Collection<Long> testCaseIds) {
        var query = em.createQuery("""
                  select testCase.id, parameter.name from Parameter parameter
                  join parameter.testCase testCase
                  where testCase.id in :ids
                  order by parameter.order""", Object[].class)
            .setParameter(IDS, testCaseIds);

        try (Stream<Object[]> stream = query.getResultStream()) {
            return stream.collect(
                Collectors.groupingBy(
                    result -> (Long) result[0],
                    Collectors.mapping(result -> (String) result[1], Collectors.toList())));
        }
    }

    private Stream<Object[]> getParametersByTCIdsStream(Collection<Long> testCaseIds) {
        return em.createQuery(FIND_PARAMETERS_BY_TESTCASE_IDS_ORDER_BY_PARAM_NAME, Object[].class)
                .setParameter(IDS, testCaseIds)
                .getResultStream();
    }

    @Override
    public Map<Long, List<Parameter>> findAllParametersByTestCaseIds(
            Collection<Long> sourceTestCaseIds) {
        Map<Long, List<Long>> calledTestCaseIdsByCaller =
                findTestCasesThatDelegatesParameters(sourceTestCaseIds);

        Set<Long> allTestCaseIds =
                Stream.concat(
                                sourceTestCaseIds.stream(),
                                calledTestCaseIdsByCaller.values().stream().flatMap(List::stream))
                        .collect(Collectors.toSet());

        Map<Long, List<Parameter>> parametersByTestCaseId = findParametersByTestCaseIds(allTestCaseIds);

        return sourceTestCaseIds.stream()
                .map(
                        testCaseId -> {
                            List<Parameter> parameters =
                                    new ArrayList<>(parametersByTestCaseId.getOrDefault(testCaseId, List.of()));

                            calledTestCaseIdsByCaller.getOrDefault(testCaseId, Collections.emptyList()).stream()
                                    .map(calledId -> parametersByTestCaseId.getOrDefault(calledId, List.of()))
                                    .forEach(parameters::addAll);

                            return parameters.isEmpty() ? null : Map.entry(testCaseId, parameters);
                        })
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    @Override
    public Map<Long, List<Parameter>> findParametersByTestCaseIds(Collection<Long> testCaseIds) {
        List<Parameter> parameters =
                new EntityGraphQueryBuilder<>(em, Parameter.class, FIND_PARAMETERS_BY_TESTCASE_IDS)
                        .executeDistinctList(Map.of(IDS, testCaseIds));

        Map<Long, List<Parameter>> result = new HashMap<>();

        for (Parameter parameter : parameters) {
            result
                    .computeIfAbsent(parameter.getTestCase().getId(), k -> new ArrayList<>())
                    .add(parameter);
        }

        return result;
    }

    private Map<Long, List<Long>> findTestCasesThatDelegatesParameters(Collection<Long> testCaseIds) {
        var cte =
                DSL.name(CTE)
                        .fields(SOURCE_ID, CALLED_ID)
                        .as(
                                select(TEST_CASE_STEPS.TEST_CASE_ID, CALL_TEST_STEP.CALLED_TEST_CASE_ID)
                                        .from(TEST_CASE_STEPS)
                                        .innerJoin(CALL_TEST_STEP)
                                        .on(TEST_CASE_STEPS.STEP_ID.eq(CALL_TEST_STEP.TEST_STEP_ID))
                                        .where(
                                                TEST_CASE_STEPS
                                                        .TEST_CASE_ID
                                                        .in(testCaseIds)
                                                        .and(CALL_TEST_STEP.DELEGATE_PARAMETER_VALUES.eq(Boolean.TRUE)))
                                        .union(
                                                select(
                                                                field(name(CTE, SOURCE_ID), Long.class),
                                                                CALL_TEST_STEP.CALLED_TEST_CASE_ID)
                                                        .from(name(CTE))
                                                        .innerJoin(TEST_CASE_STEPS)
                                                        .on(field(name(CTE, CALLED_ID)).eq(TEST_CASE_STEPS.TEST_CASE_ID))
                                                        .innerJoin(CALL_TEST_STEP)
                                                        .on(
                                                                TEST_CASE_STEPS
                                                                        .STEP_ID
                                                                        .eq(CALL_TEST_STEP.TEST_STEP_ID)
                                                                        .and(
                                                                                CALL_TEST_STEP.DELEGATE_PARAMETER_VALUES.eq(
                                                                                        Boolean.TRUE)))));

        return dslContext
                .withRecursive(cte)
                .selectFrom(cte)
                .fetchGroups(field(SOURCE_ID, Long.class), field(CALLED_ID, Long.class));
    }
}
