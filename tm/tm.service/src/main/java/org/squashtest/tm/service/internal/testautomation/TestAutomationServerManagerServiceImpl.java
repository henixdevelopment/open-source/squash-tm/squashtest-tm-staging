/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation;

import static org.squashtest.tm.domain.environmenttag.AutomationEnvironmentTagHolder.TEST_AUTOMATION_SERVER;
import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import java.net.URL;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.environmenttag.AutomationEnvironmentTag;
import org.squashtest.tm.domain.environmentvariable.EVBindableEntity;
import org.squashtest.tm.domain.servers.AuthenticationPolicy;
import org.squashtest.tm.domain.servers.AuthenticationProtocol;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.exception.NameAlreadyInUseException;
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableBindingService;
import org.squashtest.tm.service.internal.repository.TestAutomationProjectDao;
import org.squashtest.tm.service.internal.repository.TestAutomationServerDao;
import org.squashtest.tm.service.servers.StoredCredentialsManager;
import org.squashtest.tm.service.testautomation.TestAutomationServerManagerService;

@Transactional
@Service("squashtest.tm.service.TestAutomationServerManagementService")
public class TestAutomationServerManagerServiceImpl implements TestAutomationServerManagerService {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(TestAutomationServerManagerServiceImpl.class);
    private final TestAutomationServerDao serverDao;
    private final TestAutomationProjectDao projectDao;
    private final StoredCredentialsManager storedCredentialsManager;
    private final EnvironmentVariableBindingService environmentVariableBindingService;

    public TestAutomationServerManagerServiceImpl(
            TestAutomationServerDao serverDao,
            TestAutomationProjectDao projectDao,
            StoredCredentialsManager storedCredentialsManager,
            EnvironmentVariableBindingService environmentVariableBindingService) {
        this.serverDao = serverDao;
        this.projectDao = projectDao;
        this.storedCredentialsManager = storedCredentialsManager;
        this.environmentVariableBindingService = environmentVariableBindingService;
    }

    @Override
    public TestAutomationServer findById(long serverId) {
        return serverDao.getReferenceById(serverId);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    /**
     * @see TestAutomationServerManagerService#persist(TestAutomationServer)
     */
    public void persist(TestAutomationServer server) {

        // check 1 : is there another server with that name already ?
        TestAutomationServer nameInUse = serverDao.findByName(server.getName());
        if (nameInUse != null) {
            throw new NameAlreadyInUseException(
                    TestAutomationServer.class.getSimpleName(), server.getName());
        }
        // authentication policy : for now TestAutomationServer only supports APP_LEVEL
        server.setAuthenticationPolicy(AuthenticationPolicy.APP_LEVEL);

        // authentication protocol: set according to server kind
        switch (server.getKind()) {
            case squashOrchestrator:
                server.setAuthenticationProtocol(AuthenticationProtocol.TOKEN_AUTH);
                break;
            case jenkins:
            default:
                server.setAuthenticationProtocol(AuthenticationProtocol.BASIC_AUTH);
        }
        // else we can persist it.
        serverDao.save(server);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void deleteServers(List<Long> serverIds) {
        storedCredentialsManager.deleteAllServerCredentials(serverIds);

        for (Long id : serverIds) {
            environmentVariableBindingService.unbindAllByEntityIdAndType(
                    id, EVBindableEntity.TEST_AUTOMATION_SERVER);
            projectDao.deleteAllHostedProjects(id);
            serverDao.deleteServer(id);
        }
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public Page<TestAutomationServer> findSortedTestAutomationServers(Pageable pageable) {
        return serverDao.findAll(pageable);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void changeURL(long serverId, URL url) {
        TestAutomationServer server = serverDao.getReferenceById(serverId);
        server.setUrl(url.toExternalForm());
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void changeName(long serverId, String newName) {
        TestAutomationServer server = serverDao.getReferenceById(serverId);
        if (newName.equals(server.getName())) {
            return;
        }
        TestAutomationServer alreadyExists = serverDao.findByName(newName);
        if (alreadyExists == null) {
            server.setName(newName);
        } else {
            throw new NameAlreadyInUseException(TestAutomationServer.class.getSimpleName(), newName);
        }
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void changeDescription(long serverId, String description) {
        TestAutomationServer server = serverDao.getReferenceById(serverId);
        server.setDescription(description);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void changeManualSlaveSelection(long serverId, boolean manualSlaveSelection) {
        TestAutomationServer server = serverDao.getReferenceById(serverId);
        server.setManualSlaveSelection(manualSlaveSelection);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void updateEnvironmentTags(long serverId, List<String> tags) {
        Optional<TestAutomationServer> maybeServer = serverDao.findById(serverId);
        if (maybeServer.isPresent()) {
            TestAutomationServer server = maybeServer.get();
            server.getEnvironmentTags().clear();
            if (Objects.nonNull(tags) && !tags.isEmpty()) {
                server.setEnvironmentTags(
                        tags.stream()
                                .map(tag -> new AutomationEnvironmentTag(tag, TEST_AUTOMATION_SERVER))
                                .toList());
            }
        }
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void clearEnvironmentTags(long serverId) {
        updateEnvironmentTags(serverId, Collections.emptyList());
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void changeObserverURL(long serverId, URL url) {
        TestAutomationServer server = serverDao.findById(serverId).orElseThrow();
        server.setObserverUrl(url.toExternalForm());
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void clearObserverURL(long serverId) {
        TestAutomationServer server = serverDao.findById(serverId).orElseThrow();
        server.setObserverUrl(null);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void changeEventBusURL(long serverId, URL url) {
        TestAutomationServer server = serverDao.findById(serverId).orElseThrow();
        server.setEventBusUrl(url.toExternalForm());
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void clearEventBusURL(long serverId) {
        TestAutomationServer server = serverDao.findById(serverId).orElseThrow();
        server.setEventBusUrl(null);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void changeAdditionalConfiguration(long serverId, String additionalConfiguration) {
        TestAutomationServer server = serverDao.findById(serverId).orElseThrow();
        server.setAdditionalConfiguration(additionalConfiguration);
    }

    @Override
    public boolean isAdditionalConfigurationValid(String additionalConfiguration) {
        YAMLMapper yamlMapper = new YAMLMapper();
        try {
            yamlMapper.readTree(additionalConfiguration);
            return true;
        } catch (Exception e) {
            LOGGER.info("Invalid yaml syntax.", e);
            return false;
        }
    }

    @Override
    public void forceAuditAfterCredentialsUpdated(long serverId) {
        TestAutomationServer server = serverDao.findById(serverId).orElseThrow();
        server.setLastModifiedOn(new Date());
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void changeKillSwitchURL(long serverId, URL url) {
        TestAutomationServer server = serverDao.findById(serverId).orElseThrow();
        server.setKillswitchUrl(url.toExternalForm());
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void clearKillSwitchURL(long serverId) {
        TestAutomationServer server = serverDao.findById(serverId).orElseThrow();
        server.setKillswitchUrl(null);
    }
}
