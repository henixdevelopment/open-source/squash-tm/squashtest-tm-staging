/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.execution;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.service.internal.dto.ExecutionSummaryDto;

public interface ExecutionFinder {
    Execution findById(long id);

    List<ExecutionStep> findExecutionSteps(long executionId);

    ExecutionStep findExecutionStepById(long id);

    List<Execution> findAllById(List<Long> executionIds);

    /**
     * Fetch the last 5 execution statuses ( ) of a test plan by the item test plan id. Warning : This
     * method does not take into account the last execution status because it is displayed outside the
     * history.
     *
     * @param itemId The item test plan id
     */
    Map<Long, List<ExecutionSummaryDto>> findIterationTestPlanItemLastExecStatuses(
            Collection<Long> itemId);

    AutomatedExecutionExtender findExecutionExtenderByExtenderId(long extenderId);
}
