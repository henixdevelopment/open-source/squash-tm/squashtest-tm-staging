/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.campaign;

import java.util.Collection;
import java.util.List;
import org.squashtest.tm.domain.campaign.Sprint;
import org.squashtest.tm.domain.campaign.SprintStatus;
import org.squashtest.tm.service.internal.display.dto.sprint.SprintReqVersionDto;
import org.squashtest.tm.service.internal.dto.SprintReqVersionsBindingExceptionSummary;

public interface SprintManagerService {

    List<SprintReqVersionDto> bindRequirementsByRlnIds(
            Long sprintId, List<Long> rlnIds, SprintReqVersionsBindingExceptionSummary summary);

    List<SprintReqVersionDto> bindRequirementsByReqVersionIds(
            Long sprintId, List<Long> reqVersionIds, SprintReqVersionsBindingExceptionSummary summary);

    void deleteSprintReqVersions(final long sprintId, final Collection<Long> sprintReqVersionIds);

    Sprint findById(long id);

    SprintStatus getSprintStatusByExecutionId(long testPlanItemId);

    SprintStatus getSprintStatusBySprintReqVersionId(long sprintReqVersionId);

    void deleteSprintSynchronisation(long sprintId);
}
