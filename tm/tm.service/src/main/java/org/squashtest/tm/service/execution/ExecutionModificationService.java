/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.execution;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.squashtest.tm.core.foundation.collection.PagedCollectionHolder;
import org.squashtest.tm.core.foundation.collection.Paging;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.execution.SessionNoteKind;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.deletion.SuppressionPreviewReport;
import org.squashtest.tm.service.internal.display.dto.execution.SessionNoteDto;

public interface ExecutionModificationService extends ExecutionFinder {

    Execution findAndInitExecution(Long executionId);

    // XXX : should be 'setExecutionComment' because that is how it appears in the interface
    void setExecutionDescription(Long executionId, String description);

    void setExecutionStatus(Long executionId, ExecutionStatus status);

    /*********************************** Steps methods *****************************************/

    PagedCollectionHolder<List<ExecutionStep>> findExecutionSteps(long executionId, Paging filter);

    void setExecutionStepComment(Long executionStepId, String comment);

    /**
     * that method should investigate the consequences of the deletion of the given executions, and
     * return a report about what will happen.
     *
     * @param execId execution id to be deleted
     * @return simulate Execution Deletion result
     */
    List<SuppressionPreviewReport> simulateExecutionDeletion(Long execId);

    /**
     * Deletes an execution. For sprint req version test plans, you should use {@link
     * org.squashtest.tm.service.campaign.SprintTestPlanItemManagerService#deleteExecutionsAndRemoveFromTestPlanItem(List,
     * long)} instead.
     */
    void deleteExecution(Execution execution);

    /**
     * @param executionId the execution to be updated
     * @return the index of the first modified (not deleted) step in the new execution or -1
     */
    long updateSteps(@Id long executionId);

    Page<Execution> findExecutionByTestCaseId(Long testCaseId, Pageable pageable);

    SessionNoteDto createSessionNote(
            long executionId, SessionNoteKind noteKind, String noteContent, Integer noteOrder);

    SessionNoteDto updateSessionNoteKind(long noteId, SessionNoteKind kind);

    SessionNoteDto updateSessionNoteContent(long noteId, String content);

    void deleteSessionNote(long sessionNoteId);

    void changeNotesIndex(@Id long executionId, Integer newIndex, List<Long> movedNoteIds);
}
