/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.requirement.dto;

import static org.squashtest.tm.core.foundation.lang.PathUtils.asSquashPath;
import static org.squashtest.tm.service.internal.batchimport.Existence.EXISTS;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.jooq.Record3;
import org.squashtest.tm.service.internal.batchimport.TargetStatus;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.HighLevelRequirementTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.RequirementTarget;
import org.squashtest.tm.service.internal.batchimport.requirement.tree.ImportedRequirementNode;
import org.squashtest.tm.service.internal.repository.RequirementImportDao;

public class ImportRequirementFinder {

    private final RequirementImportDao dao;

    // Key is the path
    private final Map<String, List<RequirementNode>> requirements = new HashMap<>();

    private final Map<String, List<RequirementNode>> synchronizedRequirements = new HashMap<>();

    private final Map<String, List<Long>> folders = new HashMap<>();

    private final Map<String, List<RequirementTarget>> targetsPath;

    public ImportRequirementFinder(RequirementImportDao dao, Set<RequirementTarget> targets) {
        this.dao = dao;
        this.targetsPath = targets.stream().collect(Collectors.groupingBy(RequirementTarget::getPath));
    }

    public ImportedRequirementNode searchRequirementNode(RequirementTarget target) {
        RequirementNode node;

        if (target.isSynchronized()) {
            node = getSynchronizedRequirementNode(target);
        } else {
            node = getBasicRequirementNode(target.getPath());
        }

        if (node != null) {
            return node.toImportedRequirementNode(target);
        }

        return null;
    }

    private RequirementNode getSynchronizedRequirementNode(RequirementTarget target) {
        List<RequirementNode> synchronizedRequirement =
                synchronizedRequirements.getOrDefault(
                        asSquashPath(target.getPath()), Collections.emptyList());

        for (RequirementNode requirement : synchronizedRequirement) {
            if (target.getRemoteKey().equals(requirement.remoteKey())
                    && Objects.equals(requirement.remoteSyncId(), target.getRemoteSynchronisationId())) {
                return requirement;
            }
        }

        return null;
    }

    private RequirementNode getBasicRequirementNode(String path) {
        return requirements.getOrDefault(asSquashPath(path), Collections.emptyList()).stream()
                .findFirst()
                .orElse(null);
    }

    public ImportedRequirementNode searchSynchronizedRequirementParent(
            String parentPath, Long remoteSyncId) {
        List<RequirementNode> nodes =
                synchronizedRequirements.getOrDefault(asSquashPath(parentPath), Collections.emptyList());

        for (RequirementNode requirement : nodes) {
            if (requirement.isSynchronized()
                    && Objects.equals(requirement.remoteSyncId(), remoteSyncId)) {
                return requirement.toImportedRequirementNode(parentPath);
            }
        }

        return null;
    }

    public ImportedRequirementNode searchBasicRequirementParent(String parentPath) {
        List<RequirementNode> nodes =
                requirements.getOrDefault(asSquashPath(parentPath), Collections.emptyList());

        for (RequirementNode requirement : nodes) {
            if (!requirement.isSynchronized()) {
                return requirement.toImportedRequirementNode(parentPath);
            }
        }

        return null;
    }

    public ImportedRequirementNode searchFolderParent(RequirementTarget target) {
        List<Long> ids = folders.getOrDefault(asSquashPath(target.getPath()), Collections.emptyList());

        for (Long id : ids) {
            return ImportedRequirementNode.createRequirementFolderNode(
                    target, new TargetStatus(EXISTS, id));
        }

        return null;
    }

    public RequirementTarget searchParentTarget(String parentPath, ImportedRequirementNode child) {
        return targetsPath.getOrDefault(parentPath, Collections.emptyList()).stream()
                .filter(
                        target ->
                                (target.isSynchronized() == child.isSynchronized())
                                        && Objects.equals(
                                                target.getRemoteSynchronisationId(), child.getRemoteSynchronisationId()))
                .findFirst()
                .orElse(null);
    }

    private boolean targetExist(RequirementTarget target) {
        if (target.isSynchronized()) {
            return getSynchronizedRequirementNode(target) != null;
        }

        return getBasicRequirementNode(target.getPath()) != null;
    }

    public void fetch(Set<RequirementTarget> targets, String project) {
        Set<String> remoteKeys = new HashSet<>();
        Set<Long> remoteSynchronisationIds = new HashSet<>();

        Set<String> keys = new HashSet<>();
        Set<String> nodeNames = new HashSet<>();

        for (RequirementTarget target : targets) {
            if (target.isSynchronized()) {
                if (target.getRemoteSynchronisationId() != null) {
                    remoteKeys.add(target.getRemoteKey());
                    remoteSynchronisationIds.add(target.getRemoteSynchronisationId());
                } else {
                    keys.add(target.getRemoteKey());
                }
            } else {
                nodeNames.add(target.getName());
            }
        }

        fetchRemoteSynchronizedRequirements(remoteSynchronisationIds, remoteKeys);
        fetchSynchronizedRequirements(keys, project);
        fetchBasicRequirements(nodeNames, project);

        fetchParentNodes(targets, project);
    }

    private void fetchRemoteSynchronizedRequirements(
            Set<Long> remoteSynchronisationIds, Set<String> remoteKeys) {
        if (remoteKeys.isEmpty()) {
            return;
        }

        try (var resultStream = dao.findRemoteSyncRequirements(remoteKeys, remoteSynchronisationIds)) {
            resultStream.forEach(
                    record -> {
                        String path = record.value1();
                        Long id = record.value2();
                        boolean isHighLevel = record.value3();
                        String remoteKey = record.value4();
                        Long remoteSyncId = record.value5();

                        addSynchronizedRequirement(path, id, isHighLevel, remoteKey, remoteSyncId);
                    });
        }
    }

    // Fetches synchronized requirements by their remote keys.
    private void fetchSynchronizedRequirements(Set<String> keys, String project) {
        if (keys.isEmpty()) {
            return;
        }

        try (var resultStream = dao.findSyncRequirements(keys, project)) {
            resultStream.forEach(
                    record -> {
                        String path = record.value1();
                        Long id = record.value2();
                        boolean isHighLevel = record.value3();
                        String remoteKey = record.value4();

                        addSynchronizedRequirement(path, id, isHighLevel, remoteKey, null);
                    });
        }
    }

    private void addSynchronizedRequirement(
            String path, Long id, boolean isHighLevel, String remoteKey, Long remoteSyncId) {
        synchronizedRequirements
                .computeIfAbsent(path, k -> new ArrayList<>())
                .add(new RequirementNode(id, isHighLevel, remoteKey, remoteSyncId));
    }

    private void fetchBasicRequirements(Set<String> nodeNames, String project) {
        if (nodeNames.isEmpty()) {
            return;
        }

        try (Stream<Record3<String, Long, Boolean>> resultStream =
                dao.findBasicRequirementsByNamesAndProject(nodeNames, project)) {
            resultStream.forEach(
                    record -> {
                        String path = record.value1();
                        Long id = record.value2();
                        boolean isHighLevel = record.value3();

                        addRequirements(path, id, isHighLevel, null, null);
                    });
        }
    }

    private void fetchParentNodes(Set<RequirementTarget> targets, String project) {
        Set<String> missingNodes =
                targets.stream()
                        .filter(target -> !targetExist(target))
                        .flatMap(target -> target.collectParentNames().stream())
                        .collect(Collectors.toSet());

        if (missingNodes.isEmpty()) {
            return;
        }

        try (var resultStream = dao.findNodesByNamesAndProject(missingNodes, project)) {
            resultStream.forEach(
                    record -> {
                        String path = record.value1();
                        Long nodeId = record.value2();
                        boolean isRequirement = record.value3();

                        if (isRequirement) {
                            boolean isHighLevel = record.value4();
                            String remoteKey = record.value5();
                            Long remoteSyncId = record.value6();

                            addRequirements(path, nodeId, isHighLevel, remoteKey, remoteSyncId);
                        } else {
                            addFolder(path, nodeId);
                        }
                    });
        }
    }

    private void addFolder(String path, Long id) {
        folders.computeIfAbsent(path, k -> new ArrayList<>()).add(id);
    }

    private void addRequirements(
            String path, Long nodeId, boolean isHighLevel, String remoteKey, Long remoteSyncId) {
        if (remoteKey != null) {
            synchronizedRequirements
                    .computeIfAbsent(path, k -> new ArrayList<>())
                    .add(new RequirementNode(nodeId, isHighLevel, remoteKey, remoteSyncId));
        } else {
            requirements
                    .computeIfAbsent(path, k -> new ArrayList<>())
                    .add(new RequirementNode(nodeId, isHighLevel, null, null));
        }
    }

    public void fetch(Set<RequirementTarget> targets, Set<String> projects) {
        Set<String> names =
                targets.stream().map(RequirementTarget::getName).collect(Collectors.toSet());

        try (var stream = dao.findRequirementsByNamesAndProjects(names, projects)) {
            stream.forEach(
                    record -> {
                        String path = record.value1();
                        Long id = record.value2();
                        boolean isHighLevel = record.value3();
                        String remoteKey = record.value4();
                        Long remoteSyncId = record.value5();

                        addRequirements(path, id, isHighLevel, remoteKey, remoteSyncId);
                    });
        }
    }

    public record RequirementNode(Long id, boolean isHighLevel, String remoteKey, Long remoteSyncId) {
        boolean isSynchronized() {
            return remoteKey != null;
        }

        ImportedRequirementNode toImportedRequirementNode(String path) {
            RequirementTarget target =
                    isHighLevel ? new HighLevelRequirementTarget(path) : new RequirementTarget(path);

            target.setId(id);

            if (remoteKey != null) {
                target.setRemoteKey(remoteKey);
                target.setRemoteSynchronisationId(remoteSyncId);
            }

            return ImportedRequirementNode.createRequirementNode(target, new TargetStatus(EXISTS, id));
        }

        ImportedRequirementNode toImportedRequirementNode(RequirementTarget target) {
            target.setId(id);
            return ImportedRequirementNode.createRequirementNode(target, new TargetStatus(EXISTS, id));
        }
    }
}
