/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display;

import java.util.List;
import org.squashtest.tm.service.internal.display.dto.sprintgroup.SprintGroupDto;

public interface SprintGroupDisplayDao {

    SprintGroupDto getSprintGroupDtoById(long sprintGroupId);

    /**
     * Find the campaign IDs that match the given campaign library node IDs or that are contained in
     * the given campaign library node IDs.
     *
     * @param campaignLibraryNodeIds the campaign library node IDs
     */
    List<Long> findContainedSprintGroupIds(List<Long> campaignLibraryNodeIds);

    /**
     * Find the parent sprint group ID of the given node ID (or null if the node is not a sprint group
     * descendant).
     *
     * @param nodeId - the child campaign library node ID
     */
    Long findParentSprintGroupId(long nodeId);
}
