/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.servers;

import org.squashtest.csp.core.bugtracker.spi.DefaultOAuth2FormValues;
import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.domain.servers.OAuth2Credentials;

public interface OAuth2ConsumerService {

    String getOauth2AuthenticationUrl(BugTracker bugTracker);

    void getOauth2token(Long serverId, String code, String username);

    OAuth2Credentials refreshOauth2Token(Long serverId, OAuth2Credentials creds, String username);

    DefaultOAuth2FormValues getDefaultOAuth2FormValue(Long serverId);

    void getCurrentUserOauth2token(long bugTrackerId, String code);
}
