/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.jooq.impl.DSL.max;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_COVERAGE;

import javax.inject.Inject;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.repository.CustomReqVersionDao;

@Repository
public class CustomReqVersionDaoImpl implements CustomReqVersionDao {

    @Inject private DSLContext dsl;

    @Override
    public Long findAllByRequirementIdAndVerifyingTestCaseId(long requirementId, long testCaseId) {
        return dsl.select(max(REQUIREMENT_VERSION.RES_ID))
                .from(REQUIREMENT_VERSION)
                .innerJoin(REQUIREMENT_VERSION_COVERAGE)
                .on(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.eq(REQUIREMENT_VERSION.RES_ID))
                .where(REQUIREMENT_VERSION.REQUIREMENT_ID.eq(requirementId))
                .and(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID.eq(testCaseId))
                .fetchOneInto(Long.class);
    }
}
