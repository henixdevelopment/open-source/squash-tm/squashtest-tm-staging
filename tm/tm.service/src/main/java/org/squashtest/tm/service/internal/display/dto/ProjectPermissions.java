/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import java.util.ArrayList;
import java.util.List;
import org.squashtest.tm.api.security.acls.Permissions;

/**
 * @deprecated this looks unused. There's a front-end representation with the same name but the
 *     backend is dealing with raw representation (see {@link
 *     org.squashtest.tm.service.internal.repository.display.AclDisplayDao#findPermissions(List,
 *     List)} and {@link ProjectDto#getPermissions()}). Maybe we should use this class there instead
 *     of raw maps.
 * @see org.squashtest.tm.domain.project.ProjectPermission for the demain object
 * @see ProjectPermissionDto
 * @see PartyProjectPermissionDto
 */
@Deprecated(since = "6.0.0", forRemoval = true)
public class ProjectPermissions {

    private List<Permissions> project = new ArrayList<>();
    private List<Permissions> requirement = new ArrayList<>();
    private List<Permissions> testCase = new ArrayList<>();
    private List<Permissions> campaign = new ArrayList<>();
    private List<Permissions> customReport = new ArrayList<>();
    private List<Permissions> automationRequest = new ArrayList<>();

    public List<Permissions> getProject() {
        return project;
    }

    public void addProjectPermission(Permissions permission) {
        project.add(permission);
    }

    public List<Permissions> getRequirement() {
        return requirement;
    }

    public void addRequirementPermission(Permissions permission) {
        requirement.add(permission);
    }

    public List<Permissions> getTestCase() {
        return testCase;
    }

    public void addTestCasePermission(Permissions permission) {
        testCase.add(permission);
    }

    public List<Permissions> getCampaign() {
        return campaign;
    }

    public void addCampaignPermission(Permissions permission) {
        campaign.add(permission);
    }

    public List<Permissions> getCustomReport() {
        return customReport;
    }

    public void addCustomReportPermission(Permissions permission) {
        customReport.add(permission);
    }

    public List<Permissions> getAutomationRequest() {
        return automationRequest;
    }

    public void addAutomationRequestPermission(Permissions permission) {
        automationRequest.add(permission);
    }
}
