/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.squashtest.tm.domain.project.ProjectTemplate.TEMPLATE_TYPE;
import static org.squashtest.tm.jooq.domain.Tables.BUGTRACKER_PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.jooq.domain.Tables.LIBRARY_PLUGIN_BINDING;
import static org.squashtest.tm.jooq.domain.Tables.PIVOT_FORMAT_IMPORT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_AUTOMATION_PROJECT;
import static org.squashtest.tm.jooq.domain.tables.CampaignLibrary.CAMPAIGN_LIBRARY;
import static org.squashtest.tm.jooq.domain.tables.DisabledExecutionStatus.DISABLED_EXECUTION_STATUS;
import static org.squashtest.tm.jooq.domain.tables.Project.PROJECT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CREATED_BY;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TYPE;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.jooq.CommonTableExpression;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.Result;
import org.jooq.SelectOnConditionStep;
import org.jooq.SelectSelectStep;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.api.workspace.WorkspaceType;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.domain.bdd.BddScriptLanguage;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.service.internal.display.dto.PivotFormatImportDto;
import org.squashtest.tm.service.internal.display.dto.ProjectDto;
import org.squashtest.tm.service.internal.display.dto.ProjectViewDto;
import org.squashtest.tm.service.internal.repository.display.ProjectDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

@Repository
public class ProjectDisplayDaoImpl implements ProjectDisplayDao {

    private static final String PROJECT_TEMPLATE_NAME_FIELD = "PROJECT_TEMPLATE.NAME";
    private static final String PROJECT_TEMPLATE_PROJECT_ID_FIELD = "PROJECT_TEMPLATE.PROJECT_ID";

    private final DSLContext dsl;

    public ProjectDisplayDaoImpl(DSLContext dsl) {
        this.dsl = dsl;
    }

    public ProjectViewDto getProjectOrTemplateById(Long projectId) {
        return getBaseRequest(dsl.select(getBaseRequestSelectField()))
                .where(PROJECT.PROJECT_ID.eq(projectId))
                .groupBy(
                        PROJECT.PROJECT_ID,
                        DSL.field(PROJECT_TEMPLATE_NAME_FIELD),
                        DSL.field(PROJECT_TEMPLATE_PROJECT_ID_FIELD))
                .fetchOne()
                .into(ProjectViewDto.class);
    }

    @Override
    public List<ProjectDto> getActiveProjectsByIds(List<Long> projectIds) {
        return getBaseRequest(dsl.select(getBaseRequestSelectField()))
                .where(PROJECT.PROJECT_ID.in(projectIds))
                .and(PROJECT.PROJECT_TYPE.eq(Project.PROJECT_TYPE))
                .groupBy(
                        PROJECT.PROJECT_ID,
                        DSL.field(PROJECT_TEMPLATE_NAME_FIELD),
                        DSL.field(PROJECT_TEMPLATE_PROJECT_ID_FIELD))
                .fetchInto(ProjectDto.class);
    }

    @Override
    public List<ProjectDto> getActiveProjectsByIds(CommonTableExpression<Record1<Long>> projectCte) {
        return getBaseRequest(dsl.with(projectCte).select(getBaseRequestSelectField()))
                .join(projectCte)
                .on(
                        PROJECT.PROJECT_ID.eq(
                                projectCte.field(RequestAliasesConstants.PROJECT_ID_CTE, Long.class)))
                .where(PROJECT.PROJECT_TYPE.eq(Project.PROJECT_TYPE))
                .groupBy(
                        PROJECT.PROJECT_ID,
                        DSL.field(PROJECT_TEMPLATE_NAME_FIELD),
                        DSL.field(PROJECT_TEMPLATE_PROJECT_ID_FIELD))
                .fetchInto(ProjectDto.class);
    }

    @Override
    public Map<Long, List<String>> getDisabledExecutionStatus(Set<Long> projectIds) {
        return dsl.select(PROJECT.PROJECT_ID, DISABLED_EXECUTION_STATUS.EXECUTION_STATUS)
                .from(PROJECT)
                .innerJoin(CAMPAIGN_LIBRARY)
                .on(PROJECT.CL_ID.eq(CAMPAIGN_LIBRARY.CL_ID))
                .innerJoin(DISABLED_EXECUTION_STATUS)
                .on(CAMPAIGN_LIBRARY.CL_ID.eq(DISABLED_EXECUTION_STATUS.CL_ID))
                .where(PROJECT.PROJECT_ID.in(projectIds))
                .fetchGroups(PROJECT.PROJECT_ID, DISABLED_EXECUTION_STATUS.EXECUTION_STATUS);
    }

    @Override
    public Map<Long, List<String>> getDisabledExecutionStatus(
            CommonTableExpression<Record1<Long>> projectIdsCte) {
        return dsl.with(projectIdsCte)
                .select(PROJECT.PROJECT_ID, DISABLED_EXECUTION_STATUS.EXECUTION_STATUS)
                .from(PROJECT)
                .join(projectIdsCte)
                .on(
                        PROJECT.PROJECT_ID.eq(
                                projectIdsCte.field(RequestAliasesConstants.PROJECT_ID_CTE, Long.class)))
                .join(CAMPAIGN_LIBRARY)
                .on(PROJECT.CL_ID.eq(CAMPAIGN_LIBRARY.CL_ID))
                .join(DISABLED_EXECUTION_STATUS)
                .on(CAMPAIGN_LIBRARY.CL_ID.eq(DISABLED_EXECUTION_STATUS.CL_ID))
                .fetchGroups(PROJECT.PROJECT_ID, DISABLED_EXECUTION_STATUS.EXECUTION_STATUS);
    }

    @Override
    public List<Long> getProjectsLinkedToTemplate(Long templateId) {
        return dsl.select(PROJECT.PROJECT_ID)
                .from(PROJECT)
                .where(PROJECT.TEMPLATE_ID.eq(templateId))
                .fetch()
                .into(Long.class);
    }

    @Override
    public void appendActivatedPlugins(List<ProjectDto> projects) {
        if (Objects.nonNull(projects) && !projects.isEmpty()) {
            Map<Long, ProjectDto> projectsMap =
                    projects.stream().collect(Collectors.toMap(ProjectDto::getId, Function.identity()));
            Set<Long> projectIds = projects.stream().map(ProjectDto::getId).collect(Collectors.toSet());
            dsl.select(
                            LIBRARY_PLUGIN_BINDING.PLUGIN_ID,
                            LIBRARY_PLUGIN_BINDING.LIBRARY_TYPE,
                            PROJECT.PROJECT_ID)
                    .from(LIBRARY_PLUGIN_BINDING)
                    .leftJoin(PROJECT)
                    .on(
                            PROJECT
                                    .CL_ID
                                    .eq(LIBRARY_PLUGIN_BINDING.LIBRARY_ID)
                                    .and(LIBRARY_PLUGIN_BINDING.LIBRARY_TYPE.eq("C")))
                    .or(
                            PROJECT
                                    .TCL_ID
                                    .eq(LIBRARY_PLUGIN_BINDING.LIBRARY_ID)
                                    .and(LIBRARY_PLUGIN_BINDING.LIBRARY_TYPE.eq("T")))
                    .or(
                            PROJECT
                                    .RL_ID
                                    .eq(LIBRARY_PLUGIN_BINDING.LIBRARY_ID)
                                    .and(LIBRARY_PLUGIN_BINDING.LIBRARY_TYPE.eq("R")))
                    .where(PROJECT.PROJECT_ID.in(projectIds))
                    .and(LIBRARY_PLUGIN_BINDING.ACTIVE.eq(true))
                    .orderBy(PROJECT.PROJECT_ID)
                    .fetch()
                    .forEach(
                            record -> {
                                ProjectDto projectDto = projectsMap.get(record.get(PROJECT.PROJECT_ID));
                                WorkspaceType workspaceType =
                                        getWorkspaceType(record.get(LIBRARY_PLUGIN_BINDING.LIBRARY_TYPE));
                                projectDto.addActivatedPlugin(
                                        (record.get(LIBRARY_PLUGIN_BINDING.PLUGIN_ID)), workspaceType);
                            });
        }
    }

    @Override
    public Long getTaServerIdByProjectId(Long projectId) {
        return dsl.select(PROJECT.TA_SERVER_ID)
                .from(PROJECT)
                .where(PROJECT.PROJECT_ID.eq(projectId))
                .fetchOne()
                .into(Long.class);
    }

    @Override
    public List<NamedReference> findProjectsByTemplateId(Long templateId) {
        return dsl.select(PROJECT.PROJECT_ID, PROJECT.NAME)
                .from(PROJECT)
                .where(PROJECT.TEMPLATE_ID.eq(templateId))
                .fetch()
                .into(NamedReference.class);
    }

    @Override
    public List<NamedReference> findAllReferences() {
        return dsl.select(PROJECT.PROJECT_ID, PROJECT.NAME)
                .from(PROJECT)
                .where(PROJECT.PROJECT_TYPE.eq(TEMPLATE_TYPE))
                .orderBy(PROJECT.NAME)
                .fetchInto(NamedReference.class);
    }

    private WorkspaceType getWorkspaceType(String type) {
        return switch (type) {
            case ("R") -> WorkspaceType.REQUIREMENT_WORKSPACE;
            case ("C") -> WorkspaceType.CAMPAIGN_WORKSPACE;
            case ("T") -> WorkspaceType.TEST_CASE_WORKSPACE;
            default -> throw new IllegalArgumentException("Unhandled workspace type: " + type);
        };
    }

    private List<Field<?>> getBaseRequestSelectField() {
        return List.of(
                PROJECT.PROJECT_ID.as(RequestAliasesConstants.ID),
                PROJECT.NAME,
                PROJECT.LABEL,
                PROJECT.TC_NATURES_LIST.as(RequestAliasesConstants.TEST_CASE_NATURE_ID),
                PROJECT.TC_TYPES_LIST.as(RequestAliasesConstants.TEST_CASE_TYPE_ID),
                PROJECT.REQ_CATEGORIES_LIST.as(RequestAliasesConstants.REQUIREMENT_CATEGORY_ID),
                PROJECT.ALLOW_AUTOMATION_WORKFLOW,
                PROJECT.TA_SERVER_ID,
                PROJECT.AUTOMATION_WORKFLOW_TYPE,
                PROJECT.ATTACHMENT_LIST_ID,
                PROJECT.CREATED_ON,
                PROJECT.CREATED_BY,
                PROJECT.LAST_MODIFIED_ON,
                PROJECT.LAST_MODIFIED_BY,
                PROJECT.DESCRIPTION,
                DSL.field(PROJECT_TEMPLATE_NAME_FIELD).as("LINKED_TEMPLATE"),
                DSL.field(PROJECT_TEMPLATE_PROJECT_ID_FIELD).as("LINKED_TEMPLATE_ID"),
                DSL.field(PROJECT.PROJECT_TYPE.eq("T")).as("TEMPLATE"),
                PROJECT.SCM_REPOSITORY_ID,
                PROJECT.USE_TREE_STRUCTURE_IN_SCM_REPO,
                DSL.countDistinct(TEST_AUTOMATION_PROJECT.TA_PROJECT_ID).as("TA_PROJECT_COUNT"),
                PROJECT.BDD_SCRIPT_LANGUAGE,
                PROJECT.BDD_IMPLEMENTATION_TECHNOLOGY,
                PROJECT.ALLOW_TC_MODIF_DURING_EXEC,
                PROJECT.AUTOMATED_SUITES_LIFETIME,
                PROJECT.AI_SERVER_ID,
                PROJECT.BUGTRACKER_ID);
    }

    private SelectOnConditionStep<?> getBaseRequest(SelectSelectStep<?> selectFields) {
        return selectFields
                .from(PROJECT)
                .leftJoin(PROJECT.as("PROJECT_TEMPLATE"))
                .on(DSL.field(PROJECT_TEMPLATE_PROJECT_ID_FIELD).eq(PROJECT.TEMPLATE_ID))
                .leftJoin(TEST_AUTOMATION_PROJECT)
                .on(TEST_AUTOMATION_PROJECT.TM_PROJECT_ID.eq(PROJECT.PROJECT_ID));
    }

    @Override
    public BddScriptLanguage findBddScriptLanguageByProjectId(long projectId) {
        final String bddScriptLanguage =
                dsl.select(PROJECT.BDD_SCRIPT_LANGUAGE)
                        .from(PROJECT)
                        .where(PROJECT.PROJECT_ID.eq(projectId))
                        .fetchOneInto(String.class);

        return BddScriptLanguage.valueOf(bddScriptLanguage);
    }

    @Override
    public Map<Long, Boolean> allowAutomationWorkflowByProjectId(List<Long> projectIds) {
        Result<Record2<Long, Boolean>> result =
                dsl.select(PROJECT.PROJECT_ID, PROJECT.ALLOW_AUTOMATION_WORKFLOW)
                        .from(PROJECT)
                        .where(PROJECT.PROJECT_ID.in(projectIds))
                        .fetch();

        return result.stream()
                .collect(
                        Collectors.toMap(
                                record -> record.get(PROJECT.PROJECT_ID),
                                record -> record.get(PROJECT.ALLOW_AUTOMATION_WORKFLOW)));
    }

    @Override
    public List<String> fetchBugtrackerProjectNames(long projectId) {
        return dsl.select(BUGTRACKER_PROJECT.BUGTRACKER_PROJECT_NAME)
                .from(BUGTRACKER_PROJECT)
                .join(PROJECT)
                .on(BUGTRACKER_PROJECT.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .where(BUGTRACKER_PROJECT.PROJECT_ID.eq(projectId))
                .and(PROJECT.BUGTRACKER_ID.isNotNull())
                .orderBy(BUGTRACKER_PROJECT.BUGTRACKER_PROJECT_ORDER)
                .fetchInto(String.class);
    }

    @Override
    public List<PivotFormatImportDto> getExistingImports(Long projectId) {
        return dsl.select(
                        PIVOT_FORMAT_IMPORT.PFI_ID.as(ID),
                        PIVOT_FORMAT_IMPORT.NAME,
                        PIVOT_FORMAT_IMPORT.STATUS,
                        PIVOT_FORMAT_IMPORT.IMPORT_TYPE.as(TYPE),
                        PIVOT_FORMAT_IMPORT.CREATED_ON,
                        PIVOT_FORMAT_IMPORT.SUCCESSFULLY_IMPORTED_ON,
                        CORE_USER.LOGIN.as(CREATED_BY))
                .from(PIVOT_FORMAT_IMPORT)
                .leftJoin(CORE_USER)
                .on(PIVOT_FORMAT_IMPORT.CREATED_BY.eq(CORE_USER.PARTY_ID))
                .where(PIVOT_FORMAT_IMPORT.PROJECT_ID.eq(projectId))
                .fetchInto(PivotFormatImportDto.class);
    }
}
