/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.campaign;

import static org.squashtest.tm.service.security.Authorizations.WRITE_SPRINT_REQ_VERSION_OR_ADMIN;

import javax.persistence.EntityManager;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.campaign.SprintReqVersion;
import org.squashtest.tm.domain.campaign.SprintReqVersionValidationStatus;
import org.squashtest.tm.domain.campaign.SprintStatus;
import org.squashtest.tm.exception.campaign.SprintClosedException;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.campaign.SprintManagerService;
import org.squashtest.tm.service.campaign.SprintReqVersionModificationService;

@Service
@Transactional
public class SprintReqVersionModificationServiceImpl
        implements SprintReqVersionModificationService {

    private final SprintManagerService sprintManagerService;
    private final EntityManager entityManager;

    public SprintReqVersionModificationServiceImpl(
            SprintManagerService sprintManagerService, EntityManager entityManager) {
        this.sprintManagerService = sprintManagerService;
        this.entityManager = entityManager;
    }

    @Override
    @PreAuthorize(WRITE_SPRINT_REQ_VERSION_OR_ADMIN)
    public void changeValidationStatus(
            @Id long sprintReqVersionId, SprintReqVersionValidationStatus newValidationStatus) {
        SprintStatus sprintStatus =
                sprintManagerService.getSprintStatusBySprintReqVersionId(sprintReqVersionId);

        if (SprintStatus.CLOSED.equals(sprintStatus)) {
            throw new SprintClosedException();
        } else {
            entityManager
                    .find(SprintReqVersion.class, sprintReqVersionId)
                    .setValidationStatus(newValidationStatus);
        }
    }
}
