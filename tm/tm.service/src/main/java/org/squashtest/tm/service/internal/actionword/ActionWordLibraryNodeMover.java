/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.actionword;

import java.util.List;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.actionword.ActionWordLibraryNode;
import org.squashtest.tm.domain.actionword.ActionWordTreeLibraryNode;
import org.squashtest.tm.service.internal.repository.ActionWordLibraryNodeDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;

@Component
public class ActionWordLibraryNodeMover {

    private final PermissionEvaluationService permissionEvaluationService;
    private final ActionWordLibraryNodeDao actionWordLibraryNodeDao;

    public ActionWordLibraryNodeMover(
            PermissionEvaluationService permissionEvaluationService,
            ActionWordLibraryNodeDao actionWordLibraryNodeDao) {
        this.permissionEvaluationService = permissionEvaluationService;
        this.actionWordLibraryNodeDao = actionWordLibraryNodeDao;
    }

    public void moveNodes(
            List<ActionWordLibraryNode> nodes, ActionWordLibraryNode target, boolean caseInsensitivity) {
        if (userCanMoveNodesToTarget(target)) {
            moveFirstLayerNodes(nodes, target, caseInsensitivity);
        }
    }

    private void moveFirstLayerNodes(
            List<ActionWordLibraryNode> nodes, ActionWordLibraryNode target, boolean caseInsensitivity) {
        List<Long> allCanBeCopiedNodeIds =
                actionWordLibraryNodeDao.findAllCanBeCopiedNodesInTarget(nodes, target, caseInsensitivity);
        for (ActionWordLibraryNode node : nodes) {
            if (targetIsNotOriginalParent(target, node)
                    && targetIsNotSelf(target, node)
                    && allCanBeCopiedNodeIds.contains(node.getId())) {
                moveOneFirstLayerNode(target, node, caseInsensitivity);
            }
        }
    }

    private void moveOneFirstLayerNode(
            ActionWordLibraryNode target, ActionWordLibraryNode node, boolean caseInsensitivity) {
        if (userCanDeleteMovedNode(node)) {
            ActionWordTreeLibraryNode parent = node.getParent();
            parent.removeChild(node);
            changeNodeLibrary(node, target);
            target.addChild(node, caseInsensitivity);
        }
    }

    private void changeNodeLibrary(ActionWordLibraryNode node, ActionWordLibraryNode target) {
        node.setLibrary(target.getLibrary());
        node.getEntity().setProject(target.getLibrary().getProject());
    }

    private boolean userCanDeleteMovedNode(ActionWordLibraryNode node) {
        return permissionEvaluationService.hasRoleOrPermissionOnObject("ROLE_ADMIN", "DELETE", node);
    }

    private boolean targetIsNotSelf(ActionWordLibraryNode target, ActionWordLibraryNode node) {
        return !target.equals(node);
    }

    private boolean targetIsNotOriginalParent(
            ActionWordLibraryNode target, ActionWordLibraryNode node) {
        return !node.getParent().equals(target);
    }

    private boolean userCanMoveNodesToTarget(ActionWordLibraryNode target) {
        return permissionEvaluationService.hasRoleOrPermissionOnObject("ROLE_ADMIN", "CREATE", target);
    }
}
