/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.spi;

import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.squashtest.tm.core.foundation.lang.Couple;
import org.squashtest.tm.domain.servers.AuthenticationProtocol;
import org.squashtest.tm.domain.servers.Credentials;
import org.squashtest.tm.domain.servers.TokenAuthCredentials;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.domain.testautomation.AutomatedSuiteWorkflow;
import org.squashtest.tm.domain.testautomation.AutomatedTest;
import org.squashtest.tm.domain.testautomation.TestAutomationProject;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.domain.testautomation.TestAutomationServerKind;
import org.squashtest.tm.service.internal.dto.WorkflowDto;
import org.squashtest.tm.service.internal.testautomation.model.IterationTestPlanItemWithCustomFields;
import org.squashtest.tm.service.orchestrator.model.OrchestratorConfVersions;
import org.squashtest.tm.service.orchestrator.model.OrchestratorResponse;
import org.squashtest.tm.service.testautomation.model.AutomatedExecutionEnvironment;
import org.squashtest.tm.service.testautomation.model.SquashAutomExecutionConfiguration;

public interface TestAutomationConnector {

    /**
     * An enum indicating which kind of connector it is
     *
     * @return
     */
    TestAutomationServerKind getConnectorKind();

    /**
     * Checks that the given server configuration with given credentials actually works.
     *
     * @param server
     * @param login
     * @param password
     * @return true if the credentials work, false otherwise
     */
    boolean checkCredentials(TestAutomationServer server, String login, String password)
            throws TestAutomationException;

    /**
     * Checks that the given server configuration with given credentials actually works.
     *
     * @param server
     * @param credentials
     * @return true if the credentials work, false otherwise
     */
    boolean checkCredentials(TestAutomationServer server, Credentials credentials)
            throws TestAutomationException;

    /**
     * Given a server and credentials, returns the collection of {@link TestAutomationProject} that it
     * hosts.
     *
     * @param server
     * @param credentials
     * @return a Collection that may never be null if success
     * @throws ServerConnectionFailed if could not connect to the server
     * @throws AccessDenied if the server was reached but the used user could log in
     * @throws UnreadableResponseException if the server replied something that is not suitable for a
     *     response or otherwise replied not nicely
     * @throws NotFoundException if the server could not find its projects @Throws BadConfiguration if
     *     something went wrong due to the configuration
     * @throws TestAutomationException for anything that doesn't fit the exceptions above.
     */
    Collection<TestAutomationProject> listProjectsOnServer(
            TestAutomationServer server, Credentials credentials) throws TestAutomationException;

    /**
     * Given a project (that contains everything you need to connect it), returns the collection of
     * {@link AutomatedTest} that it contains
     *
     * @param project
     * @return a Collection possibly empty but never null of TestAutomationTest if success
     * @throws ServerConnectionFailed if could not connect to the server
     * @throws AccessDenied if the server was reached but the used user could log in
     * @throws UnreadableResponseException if the server replied something that is not suitable for a
     *     response or otherwise was rude to you
     * @throws NotFoundException if the tests in that project cannot be found @Throws BadConfiguration
     *     if something went wrong due to the configuration
     * @throws TestAutomationException for anything that doesn't fit the exceptions above.
     */
    Collection<AutomatedTest> listTestsInProject(TestAutomationProject project, String username)
            throws TestAutomationException;

    /**
     * Given a bunch of tests, must tell the remote server to execute them. These particular
     * executions of those tests are grouped and must be identifiable by a reference.
     *
     * <p>That method must return immediately after initiating the test start sequence, it must not
     * wait for their completion. However it may possibly start a background task to oversee the
     * remote executions from here.
     *
     * @param tests the tests that must be executed
     * @param externalId a reference that index the resulting executions of those tests
     * @throws ServerConnectionFailed if could not connect to the server
     * @throws AccessDenied if the server was reached but the used user could log in
     * @throws UnreadableResponseException if the server replied something that is not suitable for a
     *     response or otherwise thrown garbages at you
     * @throws NotFoundException if the tests in that project cannot be found @Throws BadConfiguration
     *     if something went wrong due to the configuration
     * @throws TestAutomationException for anything that doesn't fit the exceptions above.
     */
    void executeParameterizedTests(
            Collection<Couple<AutomatedExecutionExtender, Map<String, Object>>> tests, String externalId);

    List<AutomatedSuiteWorkflow> executeParameterizedTestsBasedOnITPICollection(
            Collection<IterationTestPlanItemWithCustomFields> tests,
            String externalId,
            Collection<SquashAutomExecutionConfiguration> squashAutomExecutionCbonfigurations);

    /**
     * Will build and return the URL to access to the given test automation project's.
     *
     * @param testAutomationProject : the {@link TestAutomationProject} we want the URL of
     * @return : the URL for the given {@link TestAutomationProject}
     */
    URL findTestAutomationProjectURL(TestAutomationProject testAutomationProject);

    /**
     * Will say, depending on the tests ecosystems if the execution order of the given test list is
     * guaranteed.
     *
     * @param tests
     * @return true if the test list execution order is guaranteed.
     */
    boolean testListIsOrderGuaranteed(Collection<AutomatedTest> tests);

    /**
     * Tells whether this connector supports the given {@link AuthenticationProtocol}.
     *
     * @param protocol The authentication protocol.
     * @return True if the given protocol is supported. False otherwise.
     */
    default boolean supports(AuthenticationProtocol mode) {
        return Arrays.asList(getSupportedProtocols()).contains(mode);
    }

    /**
     * Get an Array of the AuthenticationProtocols supported by this TestAutomationConnector.
     *
     * @return An Array containing all the supported AuthenticationProtocols of this
     *     TestAutomationServer
     */
    AuthenticationProtocol[] getSupportedProtocols();

    /**
     * Indicates if this connector supports automated execution environments (i.e. SquashAUTOM plugin)
     */
    default boolean supportsAutomatedExecutionEnvironments() {
        return false;
    }

    /**
     * Get all accessible environments (i.e. channels) from the given server.
     *
     * <p>If {@link TestAutomationConnector#supportsAutomatedExecutionEnvironments()} returns false,
     * you don't have to override the default implementation.
     *
     * @return a list of all environments
     */
    default List<AutomatedExecutionEnvironment> getAllAccessibleEnvironments(
            TestAutomationServer server) {
        throw new UnsupportedOperationException();
    }

    /**
     * Same as above but using a custom access token for the test automation server.
     *
     * <p>If {@link TestAutomationConnector#supportsAutomatedExecutionEnvironments()} returns false,
     * you don't have to override the default implementation.
     *
     * @return a list of all environments visible with the provided token
     */
    default List<AutomatedExecutionEnvironment> getAllAccessibleEnvironments(
            TestAutomationServer testAutomationServer, Credentials credentials) {
        throw new UnsupportedOperationException();
    }

    default OrchestratorResponse<OrchestratorConfVersions> getOrchestratorConfVersions(
            TestAutomationServer server) {
        throw new UnsupportedOperationException();
    }

    default OrchestratorResponse<List<WorkflowDto>> getProjectWorkflows(
            TestAutomationServer server, Long projectId, Credentials credentials) {
        throw new UnsupportedOperationException();
    }

    default OrchestratorResponse<Void> killWorkflow(
            TestAutomationServer server,
            Long projectId,
            TokenAuthCredentials credentials,
            String workflowId) {
        throw new UnsupportedOperationException();
    }

    default OrchestratorResponse<String> getWorkflowLogs(
            TestAutomationServer server, TokenAuthCredentials credentials, String workflowId) {
        throw new UnsupportedOperationException();
    }
}
