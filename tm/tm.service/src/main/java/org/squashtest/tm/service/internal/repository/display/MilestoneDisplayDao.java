/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display;

import com.google.common.collect.Multimap;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.jooq.CommonTableExpression;
import org.jooq.Record1;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.service.internal.display.dto.MilestoneAdminViewDto;
import org.squashtest.tm.service.internal.display.dto.MilestoneDto;
import org.squashtest.tm.service.internal.display.dto.ProjectDto;
import org.squashtest.tm.service.internal.display.dto.ProjectViewDto;
import org.squashtest.tm.service.internal.repository.display.impl.MilestoneDisplayDaoImpl;

public interface MilestoneDisplayDao {

    List<MilestoneDto> getMilestonesByTestCaseId(Long id);

    List<MilestoneDto> findAll();

    List<MilestoneDto> findByIds(Set<Long> milestoneIds);

    void appendMilestoneBinding(
            List<ProjectDto> projects, CommonTableExpression<Record1<Long>> projectIdsCte);

    void appendBoundMilestoneInformation(List<ProjectViewDto> projects);

    void appendBoundProjectsToMilestoneInformation(MilestoneAdminViewDto milestone);

    Multimap<Long, Long> findMilestonesByTestCaseId(Set<Long> testCaseIds);

    MilestoneDisplayDaoImpl.RequirementVersionMilestones findMilestonesByRequirementVersionId(
            Set<Long> requirementVersionIds);

    Multimap<Long, MilestoneDto> findMilestonesByCampaignId(Set<Long> campaignIds);

    Multimap<Long, Long> findMilestonesByProjectId(Set<Long> projectIds);

    List<MilestoneDto> getMilestonesByRequirementVersionId(Long currentVersionId);

    List<MilestoneDto> getMilestonesDtoAssociableToRequirementVersion(long versionId);

    List<MilestoneDto> getMilestonesByCampaignId(Long campaignId);

    List<MilestoneDto> getMilestonesByIterationId(Long iterationId);

    List<MilestoneDto> getMilestonesBySuiteId(Long suiteId);

    List<MilestoneDto> getMilestonesByExecutionId(Long executionId);

    List<MilestoneDto> getMilestonesBySessionOverviewId(long id);

    Map<Long, List<MilestoneDto>> getMilestonesByBoundEntity(
            List<Long> ids, NodeType handledEntityType);

    boolean areAllMilestoneGlobalOrInOneManageableProjectPerimeterOrOwner(
            Collection<Long> milestoneIds, Collection<Long> manageableProjectIds, long userId);

    void checkIfAllMilestoneAreGlobalOrInOneManageableProjectPerimeterOrOwner(
            Collection<Long> milestoneIds, Collection<Long> manageableProjectIds, long userId);
}
