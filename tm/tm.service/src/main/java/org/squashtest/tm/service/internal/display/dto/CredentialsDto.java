/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import com.google.common.base.Strings;
import org.squashtest.tm.domain.servers.AuthenticationProtocol;
import org.squashtest.tm.domain.servers.BasicAuthenticationCredentials;
import org.squashtest.tm.domain.servers.OAuth2Credentials;
import org.squashtest.tm.domain.servers.TokenAuthCredentials;
import org.squashtest.tm.service.servers.ManageableCredentials;
import org.squashtest.tm.service.servers.Oauth2Tokens;

/**
 * Used when sending stored third party server credentials from the server to the client. Sensible
 * data (password, token secret, ...) is removed.
 */
public class CredentialsDto {
    private AuthenticationProtocol implementedProtocol;
    private String username;
    private String token;
    private String tokenType;
    private AuthenticationProtocol type;
    private boolean registered;

    public static CredentialsDto from(ManageableCredentials credentials) {
        if (credentials == null) {
            return null;
        }

        return switch (credentials.getImplementedProtocol()) {
            case BASIC_AUTH -> fromBasicAuth((BasicAuthenticationCredentials) credentials);
            case TOKEN_AUTH -> fromTokenAuth((TokenAuthCredentials) credentials);
            case OAUTH_2 -> getOauth2CredentialsDto(credentials);
            default ->
                    throw new IllegalArgumentException(
                            "Invalid authentication protocol " + credentials.getImplementedProtocol());
        };
    }

    private static CredentialsDto fromTokenAuth(TokenAuthCredentials credentials) {
        CredentialsDto dto = new CredentialsDto();
        dto.implementedProtocol = credentials.getImplementedProtocol();
        dto.registered = !Strings.isNullOrEmpty(credentials.getToken());
        dto.type = credentials.getImplementedProtocol();
        return dto;
    }

    private static CredentialsDto fromBasicAuth(BasicAuthenticationCredentials credentials) {
        CredentialsDto dto = new CredentialsDto();
        dto.implementedProtocol = credentials.getImplementedProtocol();
        dto.registered = !Strings.isNullOrEmpty(credentials.getUsername());
        dto.username = credentials.getUsername();
        dto.type = credentials.getImplementedProtocol();
        return dto;
    }

    private static CredentialsDto getOauth2CredentialsDto(ManageableCredentials credentials) {
        if (credentials instanceof OAuth2Credentials) {
            return fromOAuth2((OAuth2Credentials) credentials);
        } else if (credentials instanceof Oauth2Tokens) {
            return fromUserOAuth2Token((Oauth2Tokens) credentials);
        } else {
            throw new IllegalArgumentException(
                    "Invalid type for OAuth2 credentials " + credentials.getClass().getName());
        }
    }

    public static CredentialsDto fromOAuth2(OAuth2Credentials credentials) {
        CredentialsDto dto = new CredentialsDto();
        dto.implementedProtocol = credentials.getImplementedProtocol();
        dto.token = credentials.getAccessToken();
        dto.type = credentials.getImplementedProtocol();
        dto.tokenType = credentials.getTokenType();
        return dto;
    }

    public static CredentialsDto fromUserOAuth2Token(Oauth2Tokens credentials) {
        CredentialsDto dto = new CredentialsDto();
        dto.implementedProtocol = credentials.getImplementedProtocol();
        dto.registered = !Strings.isNullOrEmpty(credentials.getAccessToken());
        dto.token = credentials.getAccessToken();
        dto.type = credentials.getImplementedProtocol();
        dto.tokenType = credentials.getTokenType();
        return dto;
    }

    public AuthenticationProtocol getImplementedProtocol() {
        return implementedProtocol;
    }

    public void setImplementedProtocol(AuthenticationProtocol implementedProtocol) {
        this.implementedProtocol = implementedProtocol;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isRegistered() {
        return registered;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }

    public AuthenticationProtocol getType() {
        return type;
    }

    public void setType(AuthenticationProtocol type) {
        this.type = type;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }
}
