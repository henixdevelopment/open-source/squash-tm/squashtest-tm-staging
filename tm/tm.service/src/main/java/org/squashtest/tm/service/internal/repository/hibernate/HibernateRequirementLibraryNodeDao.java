/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.jooq.impl.DSL.concat;
import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;
import static org.jooq.impl.DSL.select;
import static org.jooq.impl.DSL.val;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY_CONTENT;
import static org.squashtest.tm.jooq.domain.Tables.RESOURCE;
import static org.squashtest.tm.jooq.domain.Tables.RLN_RELATIONSHIP;
import static org.squashtest.tm.jooq.domain.Tables.RLN_RESOURCE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;
import javax.inject.Inject;
import org.hibernate.query.NativeQuery;
import org.hibernate.type.LongType;
import org.jooq.CommonTableExpression;
import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.Record3;
import org.jooq.SelectSeekStep1;
import org.jooq.impl.SQLDataType;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.core.foundation.lang.PathUtils;
import org.squashtest.tm.domain.requirement.RequirementLibraryNode;
import org.squashtest.tm.jooq.domain.tables.Resource;
import org.squashtest.tm.jooq.domain.tables.RlnRelationship;
import org.squashtest.tm.service.internal.repository.ParameterNames;
import org.squashtest.tm.service.internal.repository.RequirementLibraryNodeDao;

@SuppressWarnings("rawtypes")
@Repository("squashtest.tm.repository.RequirementLibraryNodeDao")
public class HibernateRequirementLibraryNodeDao extends HibernateEntityDao<RequirementLibraryNode>
        implements RequirementLibraryNodeDao {

    private static final String ID = "ID";
    private static final String PATH = "PATH";
    private static final String POSITION = "POSITION";
    private static final String PATH_CTE = "pathCte";

    @Inject DSLContext dslContext;

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getParentsName(long entityId) {
        NativeQuery query =
                currentSession().createNativeQuery(NativeQueries.RLN_FIND_SORTED_PARENT_NAMES);
        query.setParameter(ParameterNames.NODE_ID, entityId, LongType.INSTANCE);
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Long> getParentsIds(long entityId) {
        NativeQuery query =
                currentSession().createNativeQuery(NativeQueries.RLN_FIND_SORTED_PARENT_IDS);
        query.setResultTransformer(new SqLIdResultTransformer());
        query.setParameter(ParameterNames.NODE_ID, entityId, LongType.INSTANCE);
        return query.list();
    }

    @Override
    public List<Long> findNodeIdsByPath(List<String> paths) {
        List<Long> result = new ArrayList<>();

        String fullPath = paths.get(paths.size() - 1);
        String projectName = PathUtils.extractUnescapedProjectName(fullPath);
        List<String> splits = Arrays.asList(PathUtils.splitPath(fullPath));
        List<String> effectiveSplits = PathUtils.unescapeSlashes(splits);

        if (effectiveSplits.size() < 2) {
            return Collections.singletonList(null);
        }

        if (effectiveSplits.size() == 2) {
            String rootName = effectiveSplits.get(1);
            return Collections.singletonList(getRootNodeId(rootName, projectName));
        }

        List<String> pathsWithoutProject = getPathsWithoutProject(paths);

        List<Child> children =
                getChildren(pathsWithoutProject.get(pathsWithoutProject.size() - 1), projectName);

        for (String rlnPath : pathsWithoutProject) {
            result.add(
                    children.stream()
                            .filter(c -> c.path().equals(rlnPath))
                            .findFirst()
                            .map(Child::id)
                            .orElse(null));
        }

        return result;
    }

    @Override
    public Long findNodeIdByPath(String path) {
        String projectName = PathUtils.extractUnescapedProjectName(path);
        List<String> splits = Arrays.asList(PathUtils.splitPath(path));
        List<String> effectiveSplits = PathUtils.unescapeSlashes(splits);

        if (effectiveSplits.size() < 2) {
            return null;
        }

        if (effectiveSplits.size() == 2) {
            String rootName = effectiveSplits.get(1);
            return getRootNodeId(rootName, projectName);
        }

        String rlnPath = PathUtils.getPathWithoutProject(effectiveSplits);

        List<Child> children = getChildren(rlnPath, projectName);

        if (children.isEmpty()) {
            return null;
        }

        return children.stream()
                .filter(c -> c.path().equals(rlnPath))
                .findFirst()
                .map(Child::id)
                .orElse(null);
    }

    private List<String> getPathsWithoutProject(List<String> paths) {
        List<String> result = new ArrayList<>();
        for (String path : paths.subList(1, paths.size())) {
            List<String> splits = Arrays.asList(PathUtils.splitPath(path));
            List<String> effectiveSplits = PathUtils.unescapeSlashes(splits);
            result.add(PathUtils.getPathWithoutProject(effectiveSplits));
        }
        return result;
    }

    private List<Child> getChildren(String path, String projectName) {
        CommonTableExpression<Record3<Long, String, Integer>> cte =
                name(PATH_CTE)
                        .fields(ID, PATH, POSITION)
                        .as(
                                dslContext
                                        .select(
                                                RLN_RESOURCE.RLN_ID,
                                                RESOURCE.NAME.cast(SQLDataType.VARCHAR(10000)),
                                                REQUIREMENT_LIBRARY_CONTENT.CONTENT_ORDER)
                                        .from(PROJECT)
                                        .join(REQUIREMENT_LIBRARY_CONTENT)
                                        .on(PROJECT.RL_ID.eq(REQUIREMENT_LIBRARY_CONTENT.LIBRARY_ID))
                                        .join(RLN_RESOURCE)
                                        .on(REQUIREMENT_LIBRARY_CONTENT.CONTENT_ID.eq(RLN_RESOURCE.RLN_ID))
                                        .join(RESOURCE)
                                        .on(RLN_RESOURCE.RES_ID.eq(RESOURCE.RES_ID))
                                        .where(PROJECT.NAME.eq(projectName))
                                        .union(
                                                select(
                                                                RLN_RELATIONSHIP.DESCENDANT_ID,
                                                                field(name(PATH_CTE, PATH))
                                                                        .concat("/")
                                                                        .concat(RESOURCE.NAME)
                                                                        .cast(SQLDataType.VARCHAR(10000)),
                                                                RLN_RELATIONSHIP.CONTENT_ORDER)
                                                        .from(name(PATH_CTE))
                                                        .join(RLN_RELATIONSHIP)
                                                        .on(field(name(PATH_CTE, ID)).eq(RLN_RELATIONSHIP.ANCESTOR_ID))
                                                        .join(RLN_RESOURCE)
                                                        .on(RLN_RELATIONSHIP.DESCENDANT_ID.eq(RLN_RESOURCE.RLN_ID))
                                                        .join(RESOURCE)
                                                        .on(RLN_RESOURCE.RES_ID.eq(RESOURCE.RES_ID))
                                                        .where(
                                                                val(path)
                                                                        .like(
                                                                                concat(
                                                                                        field(name(PATH_CTE, PATH)),
                                                                                        val("/"),
                                                                                        RESOURCE.NAME,
                                                                                        val("%"))))));

        return dslContext
                .withRecursive(cte)
                .selectFrom(cte)
                .orderBy(cte.field(POSITION))
                .fetchInto(Child.class);
    }

    private @Nullable Long getRootNodeId(String rootName, String projectName) {
        return selectRootNodes(rootName, projectName).limit(1).fetchOneInto(Long.class);
    }

    private SelectSeekStep1<Record1<Long>, Integer> selectRootNodes(
            String rootName, String projectName) {
        return dslContext
                .select(REQUIREMENT_LIBRARY_CONTENT.CONTENT_ID)
                .from(PROJECT)
                .innerJoin(REQUIREMENT_LIBRARY_CONTENT)
                .on(PROJECT.RL_ID.eq(REQUIREMENT_LIBRARY_CONTENT.LIBRARY_ID))
                .innerJoin(RLN_RESOURCE)
                .on(REQUIREMENT_LIBRARY_CONTENT.CONTENT_ID.eq(RLN_RESOURCE.RLN_ID))
                .innerJoin(RESOURCE)
                .on(RLN_RESOURCE.RES_ID.eq(RESOURCE.RES_ID))
                .where(PROJECT.NAME.eq(projectName).and(RESOURCE.NAME.eq(rootName)))
                .orderBy(REQUIREMENT_LIBRARY_CONTENT.CONTENT_ORDER.asc());
    }

    private record Child(Long id, String path, Integer position) {}

    @Override
    public Map<Long, List<String>> findContentNames(Collection<Long> folderIds) {
        return dslContext
                .select(RlnRelationship.RLN_RELATIONSHIP.ANCESTOR_ID, Resource.RESOURCE.NAME)
                .from(RlnRelationship.RLN_RELATIONSHIP)
                .innerJoin(RLN_RESOURCE)
                .on(RlnRelationship.RLN_RELATIONSHIP.DESCENDANT_ID.eq(RLN_RESOURCE.RLN_ID))
                .innerJoin(Resource.RESOURCE)
                .on(RLN_RESOURCE.RES_ID.eq(Resource.RESOURCE.RES_ID))
                .where(RlnRelationship.RLN_RELATIONSHIP.ANCESTOR_ID.in(folderIds))
                .fetchGroups(RlnRelationship.RLN_RELATIONSHIP.ANCESTOR_ID, Resource.RESOURCE.NAME);
    }
}
