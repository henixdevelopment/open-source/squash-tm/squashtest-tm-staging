/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.requirement.RequirementCriticality;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.service.internal.repository.RequirementDao;
import org.squashtest.tm.service.internal.repository.RequirementVersionDao;
import org.squashtest.tm.service.internal.repository.TestCaseDao;
import org.squashtest.tm.service.internal.repository.loaders.testcase.TestCaseLoader;
import org.squashtest.tm.service.testcase.TestCaseImportanceManagerService;

@Service
@Transactional
public class TestCaseImportanceManagerServiceImpl implements TestCaseImportanceManagerService {

    @Inject private RequirementDao requirementDao;

    @Inject private RequirementVersionDao requirementVersionDao;

    @Inject private TestCaseDao testCaseDao;

    @Inject private TestCaseCallTreeFinder callTreeFinder;

    @Inject private TestCaseLoader testCaseLoader;

    /**
     * @param testCaseId
     * @return distinct criticalities found for all verified requirementVersions (including through
     *     call steps)
     */
    private List<RequirementCriticality> findAllDistinctRequirementsCriticalityByTestCaseId(
            long testCaseId) {
        Set<Long> calleesIds = callTreeFinder.getTestCaseCallTree(testCaseId);
        calleesIds.add(testCaseId);
        return requirementDao.findDistinctRequirementsCriticalitiesVerifiedByTestCases(calleesIds);
    }

    /**
     * will deduce the importance of the given test case with the list of it's associated
     * requirementVersions taking into account the requirementVersions associated through call steps.
     *
     * <p><i>NB: this can't be done in the setter of "importanceAuto" because of the call-step
     * associated requirementVersions that is an info handled by the "service" package . </i>
     *
     * @param testCaseId
     * @return the test case autoCalculated importance
     */
    private TestCaseImportance deduceImportanceAuto(long testCaseId) {
        List<RequirementCriticality> rCriticalities =
                findAllDistinctRequirementsCriticalityByTestCaseId(testCaseId);
        return TestCaseImportance.deduceTestCaseImportance(rCriticalities);
    }

    /**
     * @see
     *     org.squashtest.tm.service.testcase.TestCaseImportanceManagerService#changeImportanceIfIsAuto(long)
     */
    @Override
    public void changeImportanceIfIsAuto(long testCaseId) {
        TestCase testCase = testCaseLoader.load(testCaseId);
        if (Boolean.TRUE.equals(testCase.isImportanceAuto())) {
            TestCaseImportance importance = deduceImportanceAuto(testCaseId);
            testCase.setImportance(importance);
        }
    }

    /**
     * @see
     *     org.squashtest.tm.service.testcase.TestCaseImportanceManagerService#changeImportanceIfRelationsAddedToReq(List,
     *     RequirementVersion)
     */
    @Override
    public void changeImportanceIfRelationsAddedToReq(
            List<TestCase> testCases, RequirementVersion requirementVersion) {
        RequirementCriticality requirementCriticality = requirementVersion.getCriticality();
        for (TestCase testCase : testCases) {
            changeImportanceIfRelationAdded(testCase, requirementCriticality);
        }
    }

    /**
     * @see
     *     org.squashtest.tm.service.testcase.TestCaseImportanceManagerService#changeImportanceIfRelationsAddedToTestCase(List,
     *     TestCase)
     */
    @Override
    public void changeImportanceIfRelationsAddedToTestCase(
            List<RequirementVersion> requirementVersions, TestCase testCase) {
        if (!requirementVersions.isEmpty()) {
            List<RequirementCriticality> requirementCriticalities =
                    extractCriticalities(requirementVersions);
            RequirementCriticality strongestRequirementCriticality =
                    RequirementCriticality.findStrongestCriticality(requirementCriticalities);
            changeImportanceIfRelationAdded(testCase, strongestRequirementCriticality);
        }
    }

    private List<RequirementCriticality> extractCriticalities(
            List<RequirementVersion> requirementVersions) {
        List<RequirementCriticality> requirementCriticalities =
                new ArrayList<>(requirementVersions.size());
        for (RequirementVersion requirementVersion : requirementVersions) {
            requirementCriticalities.add(requirementVersion.getCriticality());
        }
        return requirementCriticalities;
    }

    private void changeImportanceIfRelationAdded(
            TestCase testCase, RequirementCriticality requirementCriticality) {

        if (Boolean.TRUE.equals(testCase.isImportanceAuto())) {
            TestCaseImportance importance = testCase.getImportance();
            TestCaseImportance newImportance =
                    importance.deduceNewImporanceWhenAddCriticality(requirementCriticality);
            if (newImportance != importance) {
                testCase.setImportance(newImportance);
                List<TestCase> callingTestCases =
                        testCaseDao.findAllCallingTestCases(testCase.getId(), null);
                for (TestCase callingTestCase : callingTestCases) {
                    changeImportanceIfRelationAdded(callingTestCase, requirementCriticality);
                }
            }
        } else {
            List<TestCase> callingTestCases = testCaseDao.findAllCallingTestCases(testCase.getId(), null);
            for (TestCase callingTestCase : callingTestCases) {
                changeImportanceIfRelationAdded(callingTestCase, requirementCriticality);
            }
        }
    }

    /**
     * @see
     *     org.squashtest.tm.service.testcase.TestCaseImportanceManagerService#changeImportanceIfRelationsRemovedFromReq(List,
     *     long)
     */
    @Override
    public void changeImportanceIfRelationsRemovedFromReq(
            List<Long> testCasesIds, long requirementVersionId) {
        RequirementVersion requirementVersion =
                requirementVersionDao.getReferenceById(requirementVersionId);
        RequirementCriticality requirementCriticality = requirementVersion.getCriticality();
        TestCaseImportance reqCritImportance =
                TestCaseImportance.deduceTestCaseImportance(Arrays.asList(requirementCriticality));
        List<TestCase> testCases = extractTestCases(testCasesIds);
        for (TestCase testCase : testCases) {
            changeImportanceIfRelationRemoved(reqCritImportance, testCase);
        }
    }

    /**
     * @see
     *     org.squashtest.tm.service.testcase.TestCaseImportanceManagerService#changeImportanceIfRelationRemoved(TestCaseImportance,
     *     TestCase)
     */
    @Override
    public void changeImportanceIfRelationRemoved(
            TestCaseImportance maxReqCritImportance, TestCase testCase) {
        if (Boolean.TRUE.equals(testCase.isImportanceAuto())) {
            TestCaseImportance actualImportance = testCase.getImportance();
            if (maxReqCritImportance.getLevel() <= actualImportance.getLevel()) {
                TestCaseImportance newImportance = deduceImportanceAuto(testCase.getId());
                if (newImportance != actualImportance) {
                    changeImportanceIfRelationRemovedForAll(testCase, newImportance, maxReqCritImportance);
                }
            }
        } else {
            List<TestCase> callingTestCases = testCaseDao.findAllCallingTestCases(testCase.getId(), null);
            for (TestCase callingTestCase : callingTestCases) {
                changeImportanceIfRelationRemoved(maxReqCritImportance, callingTestCase);
            }
        }
    }

    private void changeImportanceIfRelationRemovedForAll(
            TestCase testCase,
            TestCaseImportance newImportance,
            TestCaseImportance maxReqCritImportance) {
        testCase.setImportance(newImportance);
        List<TestCase> callingTestCases = testCaseDao.findAllCallingTestCases(testCase.getId(), null);
        for (TestCase callingTestCase : callingTestCases) {
            changeImportanceIfRelationRemoved(maxReqCritImportance, callingTestCase);
        }
    }

    private List<TestCase> extractTestCases(List<Long> testCasesIds) {
        return testCaseLoader.load(testCasesIds);
    }

    /**
     * @see
     *     org.squashtest.tm.service.testcase.TestCaseImportanceManagerService#changeImportanceIfRelationsRemovedFromTestCase(List,
     *     long)
     */
    @Override
    public void changeImportanceIfRelationsRemovedFromTestCase(
            List<Long> requirementsVersionIds, long testCaseId) {
        if (!requirementsVersionIds.isEmpty()) {
            TestCase testCase = testCaseLoader.load(testCaseId);
            List<RequirementCriticality> reqCriticalities =
                    requirementDao.findDistinctRequirementsCriticalities(requirementsVersionIds);
            TestCaseImportance maxReqCritImportance =
                    TestCaseImportance.deduceTestCaseImportance(reqCriticalities);
            changeImportanceIfRelationRemoved(maxReqCritImportance, testCase);
        }
    }

    /**
     * @see
     *     org.squashtest.tm.service.testcase.TestCaseImportanceManagerService#changeImportanceIfRequirementCriticalityChanged(long,
     *     RequirementCriticality)
     */
    @Override
    public void changeImportanceIfRequirementCriticalityChanged(
            long requirementVersionId, RequirementCriticality oldRequirementCriticality) {
        RequirementVersion requirementVersion =
                requirementVersionDao.getReferenceById(requirementVersionId);
        List<TestCase> testCases =
                testCaseDao.findUnsortedAllByVerifiedRequirementVersion(requirementVersionId);
        for (TestCase testCase : testCases) {
            changeImportanceIfRequirementCriticalityChanged(
                    oldRequirementCriticality, requirementVersion, testCase);
        }
    }

    private void changeImportanceIfRequirementCriticalityChanged(
            RequirementCriticality oldRequirementCriticality,
            RequirementVersion requirementVersion,
            TestCase testCase) {
        // if test-case is auto
        if (Boolean.TRUE.equals(testCase.isImportanceAuto())) {
            TestCaseImportance importanceAuto = testCase.getImportance();
            // if change of criticality can change importanceAuto
            boolean importanceAutoCanChange =
                    importanceAuto.changeOfCriticalityCanChangeImportanceAuto(
                            oldRequirementCriticality, requirementVersion.getCriticality());
            if (importanceAutoCanChange) {
                // -if it changes
                TestCaseImportance newImportanceAuto = deduceImportanceAuto(testCase.getId());

                if (importanceAuto != newImportanceAuto) {
                    // -- => change importance
                    testCase.setImportance(newImportanceAuto);
                    // -- look for any calling test case and call the method on
                    // them
                    changeImportanceIfRequirementCriticalityChangedForAll(
                            testCase, oldRequirementCriticality, requirementVersion);
                }
            }
        } else {
            // call the method in callers
            List<TestCase> callingTestCases = testCaseDao.findAllCallingTestCases(testCase.getId(), null);
            for (TestCase callingTestCase : callingTestCases) {
                changeImportanceIfRequirementCriticalityChanged(
                        oldRequirementCriticality, requirementVersion, callingTestCase);
            }
        }
    }

    private void changeImportanceIfRequirementCriticalityChangedForAll(
            TestCase testCase,
            RequirementCriticality oldRequirementCriticality,
            RequirementVersion requirementVersion) {
        List<TestCase> callingTestCases = testCaseDao.findAllCallingTestCases(testCase.getId(), null);
        for (TestCase callingTestCase : callingTestCases) {
            changeImportanceIfRequirementCriticalityChanged(
                    oldRequirementCriticality, requirementVersion, callingTestCase);
        }
    }

    /**
     * @see
     *     org.squashtest.tm.service.testcase.TestCaseImportanceManagerService#changeImportanceIfCallStepAddedToTestCases(TestCase,
     *     TestCase)
     */
    @Override
    public void changeImportanceIfCallStepAddedToTestCases(
            TestCase calledTestCase, TestCase parentTestCase) {
        List<RequirementCriticality> rCriticalities =
                findAllDistinctRequirementsCriticalityByTestCaseId(calledTestCase.getId());
        if (!rCriticalities.isEmpty()) {
            RequirementCriticality strongestRequirementCriticality =
                    RequirementCriticality.findStrongestCriticality(rCriticalities);
            changeImportanceIfRelationAdded(parentTestCase, strongestRequirementCriticality);
        }
    }

    @Override
    public void changeImportanceIfCallStepAddedToTestCases(
            Map<TestCase, Set<Long>> calledIdsByCaller) {
        Map<Long, Set<RequirementCriticality>> calledCriticalityListByCallerId =
                findAllReqCriticalityByTestCaseCallerId(calledIdsByCaller);

        if (calledCriticalityListByCallerId.isEmpty()) {
            return;
        }

        Map<TestCase, RequirementCriticality> strongestCriticalityByCaller =
                calledIdsByCaller.keySet().stream()
                        .map(caller -> Map.entry(caller, calledCriticalityListByCallerId.get(caller.getId())))
                        .filter(entry -> !entry.getValue().isEmpty())
                        .collect(
                                Collectors.toMap(
                                        Map.Entry::getKey,
                                        entry ->
                                                RequirementCriticality.findStrongestCriticality(
                                                        List.copyOf(entry.getValue()))));

        List<Long> callerIds =
                strongestCriticalityByCaller.keySet().stream().map(TestCaseLibraryNode::getId).toList();

        Map<Long, List<TestCase>> callingTestCaseByCallerId =
                testCaseDao.findRecursiveCallerTestCasesByCalledId(callerIds);

        for (var entry : strongestCriticalityByCaller.entrySet()) {
            TestCase callerTestCase = entry.getKey();
            RequirementCriticality criticality = entry.getValue();

            if (Boolean.TRUE.equals(callerTestCase.isImportanceAuto())) {
                updateTestCaseImportance(callerTestCase, criticality);
            }
            callingTestCaseByCallerId
                    .getOrDefault(callerTestCase.getId(), Collections.emptyList())
                    .stream()
                    .filter(calling -> Boolean.TRUE.equals(calling.isImportanceAuto()))
                    .forEach(callingTestCase -> updateTestCaseImportance(callingTestCase, criticality));
        }
    }

    private void updateTestCaseImportance(TestCase testCase, RequirementCriticality criticality) {
        TestCaseImportance currentImportance = testCase.getImportance();

        TestCaseImportance newImportance =
                currentImportance.deduceNewImporanceWhenAddCriticality(criticality);

        if (!newImportance.equals(currentImportance)) {
            testCase.setImportance(newImportance);
        }
    }

    private Map<Long, Set<RequirementCriticality>> findAllReqCriticalityByTestCaseCallerId(
            Map<TestCase, Set<Long>> calledIdsByParent) {
        Set<Long> allCalledIds =
                calledIdsByParent.values().stream().flatMap(Collection::stream).collect(Collectors.toSet());

        Map<Long, List<Long>> calledListByCallerId =
                testCaseDao.findRecursiveCalledIdsByCallerId(allCalledIds);

        allCalledIds.forEach(
                calledId ->
                        calledListByCallerId.computeIfAbsent(calledId, k -> new ArrayList<>()).add(calledId));

        Map<Long, RequirementCriticality> criticalityByCalledId =
                requirementDao.findRequirementsCriticalityVerifiedByTestCases(
                        calledListByCallerId.values().stream()
                                .flatMap(List::stream)
                                .collect(Collectors.toSet()));

        Map<Long, Set<RequirementCriticality>> calledCriticalityListByCallerId = new HashMap<>();

        for (var entry : calledIdsByParent.entrySet()) {
            entry.getValue().stream()
                    .map(calledListByCallerId::get)
                    .flatMap(Collection::stream)
                    .filter(criticalityByCalledId::containsKey)
                    .forEach(
                            id ->
                                    calledCriticalityListByCallerId
                                            .computeIfAbsent(entry.getKey().getId(), k -> new HashSet<>())
                                            .add(criticalityByCalledId.get(id)));
        }

        if (calledCriticalityListByCallerId.isEmpty()) {
            return Collections.emptyMap();
        }

        return calledCriticalityListByCallerId;
    }

    /**
     * @see
     *     org.squashtest.tm.service.testcase.TestCaseImportanceManagerService#changeImportanceIfCallStepRemoved(TestCase,
     *     TestCase)
     */
    @Override
    public void changeImportanceIfCallStepRemoved(TestCase calledTestCase, TestCase parentTestCase) {
        List<RequirementCriticality> rCriticalities =
                findAllDistinctRequirementsCriticalityByTestCaseId(calledTestCase.getId());
        if (!rCriticalities.isEmpty()) {
            TestCaseImportance maxReqCritImportance =
                    TestCaseImportance.deduceTestCaseImportance(rCriticalities);
            changeImportanceIfRelationRemoved(maxReqCritImportance, parentTestCase);
        }
    }
}
