/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.squashtest.tm.jooq.domain.Tables.CALL_TEST_STEP;
import static org.squashtest.tm.jooq.domain.Tables.PARAMETER;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_STEPS;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.display.dto.testcase.ParameterDto;
import org.squashtest.tm.service.internal.repository.display.ParameterDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

@Repository
public class ParameterDisplayDaoImpl implements ParameterDisplayDao {
    @Inject private DSLContext dsl;

    @Override
    public List<ParameterDto> findAllByTestCaseId(Long testCaseId) {

        List<ParameterDto> allParameters = new ArrayList<>();

        Set<Long> exploredTc = new HashSet<>();
        List<Long> srcTc = new LinkedList<>();
        List<Long> destTc;

        srcTc.add(testCaseId);

        while (!srcTc.isEmpty()) {

            allParameters.addAll(findTestCaseParameters(srcTc));

            destTc = findTestCasesThatDelegatesParameters(srcTc);

            exploredTc.addAll(srcTc);
            srcTc = destTc;
            srcTc.removeAll(exploredTc);
        }
        allParameters.sort(Comparator.comparing(ParameterDto::getId));
        return allParameters;
    }

    private List<Long> findTestCasesThatDelegatesParameters(List<Long> sourceIds) {
        return dsl.select(CALL_TEST_STEP.CALLED_TEST_CASE_ID)
                .from(TEST_CASE_STEPS)
                .innerJoin(CALL_TEST_STEP)
                .on(CALL_TEST_STEP.TEST_STEP_ID.eq(TEST_CASE_STEPS.STEP_ID))
                .where(
                        TEST_CASE_STEPS
                                .TEST_CASE_ID
                                .in(sourceIds)
                                .and(CALL_TEST_STEP.DELEGATE_PARAMETER_VALUES.isTrue()))
                .fetchInto(Long.class);
    }

    private List<ParameterDto> findTestCaseParameters(List<Long> testCaseIds) {
        return dsl.select(
                        PARAMETER.PARAM_ID.as(RequestAliasesConstants.ID),
                        PARAMETER.NAME,
                        PARAMETER.DESCRIPTION,
                        TEST_CASE_LIBRARY_NODE.TCLN_ID.as("SOURCE_TEST_CASE_ID"),
                        TEST_CASE_LIBRARY_NODE.NAME.as("SOURCE_TEST_CASE_NAME"),
                        TEST_CASE.REFERENCE.as("SOURCE_TEST_CASE_REFERENCE"),
                        PROJECT.NAME.as("SOURCE_TEST_CASE_PROJECT_NAME"),
                        PARAMETER.PARAM_ORDER.as("ORDER"))
                .from(PARAMETER)
                .innerJoin(TEST_CASE_LIBRARY_NODE)
                .on(PARAMETER.TEST_CASE_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .innerJoin(PROJECT)
                .on(PROJECT.PROJECT_ID.eq(TEST_CASE_LIBRARY_NODE.PROJECT_ID))
                .innerJoin(TEST_CASE)
                .on(TEST_CASE.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .where(PARAMETER.TEST_CASE_ID.in(testCaseIds))
                .fetch()
                .into(ParameterDto.class);
    }
}
