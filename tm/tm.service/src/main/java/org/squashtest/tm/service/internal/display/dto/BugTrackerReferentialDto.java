/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import org.squashtest.tm.core.foundation.annotation.CleanHtml;

public class BugTrackerReferentialDto extends BugTrackerDto {

    private boolean useProjectPaths;
    private String projectHelpMessage;
    private String loginFieldKey;
    private String passwordFieldKey;
    private boolean cacheAllowed;
    private boolean cacheConfigured;

    protected BugTrackerReferentialDto() {}

    public static BugTrackerReferentialDto from(BugTrackerDto bugTrackerDto) {
        BugTrackerReferentialDto refDto = new BugTrackerReferentialDto();

        refDto.setId(bugTrackerDto.getId());
        refDto.setName(bugTrackerDto.getName());
        refDto.setUrl(bugTrackerDto.getUrl());
        refDto.setKind(bugTrackerDto.getKind());
        refDto.setAuthPolicy(bugTrackerDto.getAuthPolicy());
        refDto.setAuthProtocol(bugTrackerDto.getAuthProtocol());
        refDto.setIframeFriendly(bugTrackerDto.isIframeFriendly());

        return refDto;
    }

    public boolean isUseProjectPaths() {
        return useProjectPaths;
    }

    public void setUseProjectPaths(boolean useProjectPaths) {
        this.useProjectPaths = useProjectPaths;
    }

    @CleanHtml
    public String getProjectHelpMessage() {
        return projectHelpMessage;
    }

    public void setProjectHelpMessage(String projectHelpMessage) {
        this.projectHelpMessage = projectHelpMessage;
    }

    public String getLoginFieldKey() {
        return loginFieldKey;
    }

    public void setLoginFieldKey(String loginFieldKey) {
        this.loginFieldKey = loginFieldKey;
    }

    public String getPasswordFieldKey() {
        return passwordFieldKey;
    }

    public void setPasswordFieldKey(String passwordFieldKey) {
        this.passwordFieldKey = passwordFieldKey;
    }

    public boolean isCacheAllowed() {
        return cacheAllowed;
    }

    public void setCacheAllowed(boolean cacheAllowed) {
        this.cacheAllowed = cacheAllowed;
    }

    public boolean isCacheConfigured() {
        return cacheConfigured;
    }

    public void setCacheConfigured(boolean cacheConfigured) {
        this.cacheConfigured = cacheConfigured;
    }
}
