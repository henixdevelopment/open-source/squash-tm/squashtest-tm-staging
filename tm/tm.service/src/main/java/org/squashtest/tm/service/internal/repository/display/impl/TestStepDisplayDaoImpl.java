/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static java.util.Collections.singletonList;
import static org.jooq.impl.DSL.field;
import static org.squashtest.tm.jooq.domain.Tables.ACTION_WORD;
import static org.squashtest.tm.jooq.domain.Tables.ACTION_WORD_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.CALL_TEST_STEP;
import static org.squashtest.tm.jooq.domain.Tables.DATASET;
import static org.squashtest.tm.jooq.domain.Tables.KEYWORD_TEST_STEP;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_STEPS;
import static org.squashtest.tm.jooq.domain.tables.ActionTestStep.ACTION_TEST_STEP;
import static org.squashtest.tm.service.internal.display.dto.testcase.ActionTestStepDto.ACTION_STEP;
import static org.squashtest.tm.service.internal.display.dto.testcase.CalledTestStepDto.CALL_STEP;
import static org.squashtest.tm.service.internal.display.dto.testcase.KeywordTestStepDto.KEYWORD_STEP;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ACTION_WORD_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.AWLN_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.COMMENT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.DATATABLE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.DOCSTRING;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.KEYWORD;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PROJECT_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_STEP_ID;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Record8;
import org.jooq.RecordMapper;
import org.jooq.SelectOnConditionStep;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.actionword.ActionWordNodeType;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.jooq.domain.tables.TestCase;
import org.squashtest.tm.jooq.domain.tables.TestCaseLibraryNode;
import org.squashtest.tm.service.internal.display.dto.AttachmentListDto;
import org.squashtest.tm.service.internal.display.dto.AttachmentListHolderDto;
import org.squashtest.tm.service.internal.display.dto.CufHolderDto;
import org.squashtest.tm.service.internal.display.dto.CustomFieldValueDto;
import org.squashtest.tm.service.internal.display.dto.execution.ActionStepExecView;
import org.squashtest.tm.service.internal.display.dto.testcase.AbstractTestStepDto;
import org.squashtest.tm.service.internal.display.dto.testcase.ActionTestStepDto;
import org.squashtest.tm.service.internal.display.dto.testcase.ActionWordFragmentValueDto;
import org.squashtest.tm.service.internal.display.dto.testcase.CalledTestStepDto;
import org.squashtest.tm.service.internal.display.dto.testcase.KeywordTestStepDto;
import org.squashtest.tm.service.internal.repository.display.ActionWordDisplayDao;
import org.squashtest.tm.service.internal.repository.display.AttachmentDisplayDao;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TestStepDisplayDao;

@Repository
public class TestStepDisplayDaoImpl implements TestStepDisplayDao {

    public static final String ACTION_STEP_TEST_CASE_ID = "ACTION_STEP_TEST_CASE_ID";
    public static final String ACTION_STEP_TEST_CASE_NAME = "ACTION_STEP_TEST_CASE_NAME";
    public static final String ACTION_STEP_TEST_CASE_REFERENCE = "ACTION_STEP_TEST_CASE_REFERENCE";
    private static final TestCase CALLED_TEST_CASE = TEST_CASE.as("CALLED_TEST_CASE");
    private static final TestCaseLibraryNode CALLED_TEST_CASE_LIBRARY_NODE =
            TEST_CASE_LIBRARY_NODE.as("CALLED_TEST_CASE_LIBRARY_NODE");

    private DSLContext dsl;

    private AttachmentDisplayDao attachmentDisplayDao;

    private CustomFieldValueDisplayDao customFieldValueDisplayDao;

    private ActionWordDisplayDao actionWordDisplayDao;

    public TestStepDisplayDaoImpl(
            DSLContext dsl,
            AttachmentDisplayDao attachmentDisplayDao,
            CustomFieldValueDisplayDao customFieldValueDisplayDao,
            ActionWordDisplayDao actionWordDisplayDao) {
        this.dsl = dsl;
        this.attachmentDisplayDao = attachmentDisplayDao;
        this.customFieldValueDisplayDao = customFieldValueDisplayDao;
        this.actionWordDisplayDao = actionWordDisplayDao;
    }

    @Override
    public List<AbstractTestStepDto> getTestStepsByTestCase(Long testCaseId) {
        List<AbstractTestStepDto> testSteps = fetchTestStepsByTestCase(testCaseId);
        return appendDependantData(testSteps);
    }

    @Override
    public ActionStepExecView findOneActionStepExecView(long executionId, long stepId) {
        ActionStepExecView view = fetchActionStepExecView(stepId);
        this.appendCustomFieldValues(singletonList(view));
        this.appendAttachments(singletonList(view));
        return view;
    }

    private ActionStepExecView fetchActionStepExecView(long stepId) {
        return dsl.select(
                        ACTION_TEST_STEP.TEST_STEP_ID.as(ID),
                        ACTION_TEST_STEP.ACTION,
                        ACTION_TEST_STEP.EXPECTED_RESULT,
                        ACTION_TEST_STEP.ATTACHMENT_LIST_ID,
                        TEST_CASE_LIBRARY_NODE.TCLN_ID.as(ACTION_STEP_TEST_CASE_ID),
                        TEST_CASE_LIBRARY_NODE.NAME.as(ACTION_STEP_TEST_CASE_NAME),
                        TEST_CASE_LIBRARY_NODE.PROJECT_ID,
                        TEST_CASE.REFERENCE.as(ACTION_STEP_TEST_CASE_REFERENCE))
                .from(ACTION_TEST_STEP)
                .innerJoin(TEST_CASE_STEPS)
                .on(TEST_CASE_STEPS.STEP_ID.eq(ACTION_TEST_STEP.TEST_STEP_ID))
                .innerJoin(TEST_CASE_LIBRARY_NODE)
                .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE_STEPS.TEST_CASE_ID))
                .innerJoin(TEST_CASE)
                .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .where(ACTION_TEST_STEP.TEST_STEP_ID.eq(stepId))
                .fetchOneInto(ActionStepExecView.class);
    }

    @Override
    public List<AbstractTestStepDto> getTestSteps(Set<Long> testStepIds) {
        List<AbstractTestStepDto> testSteps = this.fetchTestSteps(testStepIds);
        return appendDependantData(testSteps);
    }

    // method which append all other stuff needed to render properly the test steps views
    private List<AbstractTestStepDto> appendDependantData(List<AbstractTestStepDto> testSteps) {
        Map<String, List<AbstractTestStepDto>> stepsByKind =
                testSteps.stream().collect(Collectors.groupingBy(AbstractTestStepDto::getKind));

        // append attachments lists
        List<ActionTestStepDto> actionSteps =
                stepsByKind.getOrDefault(ACTION_STEP, new ArrayList<>()).stream()
                        .map(ActionTestStepDto.class::cast)
                        .toList();

        if (!actionSteps.isEmpty()) {
            appendAttachments(actionSteps);

            // append cuf values for action steps
            appendCustomFieldValues(actionSteps);
        }
        List<CalledTestStepDto> callSteps =
                stepsByKind.getOrDefault(CALL_STEP, new ArrayList<>()).stream()
                        .map(CalledTestStepDto.class::cast)
                        .toList();
        if (!callSteps.isEmpty()) {
            this.appendCalledSteps(callSteps);
        }
        List<KeywordTestStepDto> keywordSteps =
                stepsByKind.getOrDefault(KEYWORD_STEP, new ArrayList<>()).stream()
                        .map(KeywordTestStepDto.class::cast)
                        .toList();
        if (!keywordSteps.isEmpty()) {
            appendAction(keywordSteps);
        }
        return testSteps;
    }

    private void appendAction(List<KeywordTestStepDto> keywordSteps) {
        List<Long> keywordStepIds = keywordSteps.stream().map(KeywordTestStepDto::getId).toList();
        List<ActionWordFragmentValueDto> fragmentDtoList =
                actionWordDisplayDao.findActionWordFragmentValues(keywordStepIds);
        keywordSteps.forEach(
                keywordStep -> {
                    List<ActionWordFragmentValueDto> fragmentList =
                            fragmentDtoList.stream()
                                    .filter(fragmentDto -> keywordStep.getId().equals(fragmentDto.getKeywordStepId()))
                                    .toList();
                    keywordStep.setAction(buildActionForStep(fragmentList));
                    keywordStep.setStyledAction(buildStyledActionForStep(fragmentList));
                });
    }

    private String buildStyledActionForStep(List<ActionWordFragmentValueDto> fragmentList) {
        return fragmentList.stream()
                .map(ActionWordFragmentValueDto::getStyledAction)
                .collect(Collectors.joining());
    }

    private String buildActionForStep(List<ActionWordFragmentValueDto> fragmentList) {
        return fragmentList.stream()
                .map(ActionWordFragmentValueDto::getUnstyledAction)
                .collect(Collectors.joining());
    }

    private void appendCalledSteps(List<CalledTestStepDto> callSteps) {
        Set<Long> calledTestCaseId =
                callSteps.stream().map(CalledTestStepDto::getCalledTcId).collect(Collectors.toSet());
        Multimap<Long, AbstractTestStepDto> stepMap =
                this.fetchTestSteps(TEST_CASE_STEPS.TEST_CASE_ID.in(calledTestCaseId));
        callSteps.forEach(
                callStep -> callStep.addCalledTestCaseSteps(stepMap.get(callStep.getCalledTcId())));
    }

    private void appendCustomFieldValues(List<? extends CufHolderDto> cufHolders) {
        List<Long> stepsIds = cufHolders.stream().map(CufHolderDto::getId).toList();

        ListMultimap<Long, CustomFieldValueDto> customFieldValues =
                this.customFieldValueDisplayDao.findCustomFieldValues(BindableEntity.TEST_STEP, stepsIds);
        cufHolders.forEach(
                actionStep -> {
                    List<CustomFieldValueDto> customFieldValue = customFieldValues.get(actionStep.getId());
                    actionStep.setCustomFieldValues(customFieldValue);
                });
    }

    private void appendAttachments(List<? extends AttachmentListHolderDto> actionSteps) {
        Set<Long> attachmentListIds =
                actionSteps.stream()
                        .map(AttachmentListHolderDto::getAttachmentList)
                        .map(AttachmentListDto::getId)
                        .collect(Collectors.toSet());

        Map<Long, AttachmentListDto> attachmentsLists =
                this.attachmentDisplayDao.findAttachmentListByIds(attachmentListIds).stream()
                        .collect(Collectors.toMap(AttachmentListDto::getId, Function.identity()));

        actionSteps.forEach(
                actionTestStepDto -> {
                    Long id = actionTestStepDto.getAttachmentListId();
                    AttachmentListDto attachmentList = attachmentsLists.get(id);
                    actionTestStepDto.addAttachments(attachmentList.getAttachments());
                });
    }

    private List<AbstractTestStepDto> fetchTestSteps(Set<Long> testStepIds) {
        if (testStepIds.isEmpty()) {
            return new ArrayList<>();
        }
        Multimap<Long, AbstractTestStepDto> dtoMap =
                this.fetchTestSteps(TEST_CASE_STEPS.STEP_ID.in(testStepIds));
        return new ArrayList<>(dtoMap.values());
    }

    private List<AbstractTestStepDto> fetchTestStepsByTestCase(Long testCaseId) {
        Multimap<Long, AbstractTestStepDto> dtoMap =
                this.fetchTestSteps(TEST_CASE_STEPS.TEST_CASE_ID.in(singletonList(testCaseId)));
        return new ArrayList<>(dtoMap.get(testCaseId));
    }

    private Multimap<Long, AbstractTestStepDto> fetchTestSteps(Condition condition) {
        Table<Record8<Long, Long, String, String, String, String, Long, Long>> keyWordTestStepTable =
                DSL.select(
                                KEYWORD_TEST_STEP.TEST_STEP_ID,
                                KEYWORD_TEST_STEP.ACTION_WORD_ID,
                                KEYWORD_TEST_STEP.KEYWORD,
                                KEYWORD_TEST_STEP.DATATABLE,
                                KEYWORD_TEST_STEP.DOCSTRING,
                                KEYWORD_TEST_STEP.COMMENT,
                                ACTION_WORD.PROJECT_ID,
                                ACTION_WORD_LIBRARY_NODE.AWLN_ID)
                        .from(TEST_CASE_STEPS)
                        .innerJoin(KEYWORD_TEST_STEP)
                        .on(TEST_CASE_STEPS.STEP_ID.eq(KEYWORD_TEST_STEP.TEST_STEP_ID))
                        .innerJoin(ACTION_WORD)
                        .on(KEYWORD_TEST_STEP.ACTION_WORD_ID.eq(ACTION_WORD.ACTION_WORD_ID))
                        .innerJoin(ACTION_WORD_LIBRARY_NODE)
                        .on(ACTION_WORD.ACTION_WORD_ID.eq(ACTION_WORD_LIBRARY_NODE.ENTITY_ID))
                        .where(ACTION_WORD_LIBRARY_NODE.ENTITY_TYPE.eq(ActionWordNodeType.ACTION_WORD_NAME))
                        .asTable();

        SelectOnConditionStep<? extends Record> baseSelect =
                dsl.select(
                                TEST_CASE_STEPS.STEP_ID,
                                TEST_CASE_STEPS.STEP_ORDER,
                                TEST_CASE_STEPS.TEST_CASE_ID,
                                ACTION_TEST_STEP.ACTION,
                                ACTION_TEST_STEP.EXPECTED_RESULT,
                                ACTION_TEST_STEP.ATTACHMENT_LIST_ID,
                                CALL_TEST_STEP.CALLED_TEST_CASE_ID,
                                CALL_TEST_STEP.CALLED_DATASET,
                                CALL_TEST_STEP.DELEGATE_PARAMETER_VALUES,
                                CALLED_TEST_CASE_LIBRARY_NODE.NAME,
                                DATASET.NAME,
                                keyWordTestStepTable.field(TEST_STEP_ID),
                                keyWordTestStepTable.field(ACTION_WORD_ID),
                                keyWordTestStepTable.field(KEYWORD),
                                keyWordTestStepTable.field(DATATABLE),
                                keyWordTestStepTable.field(DOCSTRING),
                                keyWordTestStepTable.field(COMMENT),
                                keyWordTestStepTable.field(PROJECT_ID),
                                keyWordTestStepTable.field(AWLN_ID))
                        .from(TEST_CASE_STEPS)
                        .leftJoin(ACTION_TEST_STEP)
                        .on(TEST_CASE_STEPS.STEP_ID.eq(ACTION_TEST_STEP.TEST_STEP_ID))
                        .leftJoin(CALL_TEST_STEP)
                        .on(TEST_CASE_STEPS.STEP_ID.eq(CALL_TEST_STEP.TEST_STEP_ID))
                        .leftJoin(CALLED_TEST_CASE)
                        .on(CALL_TEST_STEP.CALLED_TEST_CASE_ID.eq(CALLED_TEST_CASE.TCLN_ID))
                        .leftJoin(DATASET)
                        .on(CALL_TEST_STEP.CALLED_DATASET.eq(DATASET.DATASET_ID))
                        .leftJoin(CALLED_TEST_CASE_LIBRARY_NODE)
                        .on(CALLED_TEST_CASE.TCLN_ID.eq(CALLED_TEST_CASE_LIBRARY_NODE.TCLN_ID))
                        .leftJoin(keyWordTestStepTable)
                        .on(TEST_CASE_STEPS.STEP_ID.eq(keyWordTestStepTable.field(TEST_STEP_ID, Long.class)));

        List<AbstractTestStepDto> testStepDtos =
                baseSelect
                        .where(condition)
                        .orderBy(TEST_CASE_STEPS.STEP_ORDER)
                        .fetch(
                                (RecordMapper<Record, AbstractTestStepDto>)
                                        record -> {
                                            Long calledTestCaseId = record.get(CALL_TEST_STEP.CALLED_TEST_CASE_ID);
                                            Long keywordTestStepId = record.get(field(TEST_STEP_ID, Long.class));
                                            if (Objects.nonNull(calledTestCaseId)) {
                                                return createCallStep(record, calledTestCaseId);
                                            } else if (Objects.nonNull(keywordTestStepId)) {
                                                return createKeywordStep(record);
                                            } else {
                                                return createActionStep(record);
                                            }
                                        });

        ArrayListMultimap<Long, AbstractTestStepDto> dtoMap = ArrayListMultimap.create();
        testStepDtos.forEach(testStepDto -> dtoMap.put(testStepDto.getTestCaseId(), testStepDto));
        return dtoMap;
    }

    private AbstractTestStepDto createActionStep(Record record) {
        ActionTestStepDto actionTestStepDto = new ActionTestStepDto();
        setBaseAttributes(record, actionTestStepDto);
        actionTestStepDto.setAction(record.get(ACTION_TEST_STEP.ACTION));
        actionTestStepDto.setExpectedResult(record.get(ACTION_TEST_STEP.EXPECTED_RESULT));
        actionTestStepDto.setAttachmentListId(record.get(ACTION_TEST_STEP.ATTACHMENT_LIST_ID));
        return actionTestStepDto;
    }

    private CalledTestStepDto createCallStep(Record record, Long calledTestCaseId) {
        CalledTestStepDto calledTestStepDto = new CalledTestStepDto();
        setBaseAttributes(record, calledTestStepDto);
        calledTestStepDto.setCalledTcName(record.get(CALLED_TEST_CASE_LIBRARY_NODE.NAME));
        calledTestStepDto.setCalledTcId(calledTestCaseId);
        calledTestStepDto.setCalledDatasetId(record.get(CALL_TEST_STEP.CALLED_DATASET));
        calledTestStepDto.setDelegateParam(record.get(CALL_TEST_STEP.DELEGATE_PARAMETER_VALUES));
        calledTestStepDto.setCalledDatasetName(record.get(DATASET.NAME));
        return calledTestStepDto;
    }

    private AbstractTestStepDto createKeywordStep(Record record) {
        KeywordTestStepDto keywordTestStepDto = new KeywordTestStepDto();
        setBaseAttributes(record, keywordTestStepDto);
        keywordTestStepDto.setActionWordId(record.get(field(ACTION_WORD_ID, Long.class)));
        keywordTestStepDto.setKeyword(record.get(field(KEYWORD, String.class)));
        keywordTestStepDto.setDatatable(record.get(field(DATATABLE, String.class)));
        keywordTestStepDto.setDocstring(record.get(field(DOCSTRING, String.class)));
        keywordTestStepDto.setComment(record.get(field(COMMENT, String.class)));
        keywordTestStepDto.setActionWordProjectId(record.get(field(PROJECT_ID, Long.class)));
        keywordTestStepDto.setActionWordLibraryNodeId(record.get(field(AWLN_ID, Long.class)));
        return keywordTestStepDto;
    }

    private void setBaseAttributes(Record record, AbstractTestStepDto stepDto) {
        stepDto.setId(record.get(TEST_CASE_STEPS.STEP_ID));
        stepDto.setStepOrder(record.get(TEST_CASE_STEPS.STEP_ORDER));
        stepDto.setTestCaseId(record.get(TEST_CASE_STEPS.TEST_CASE_ID));
    }
}
