/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.requirement;

import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_AUDIT_EVENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LARGE_PROPERTY_CHANGE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_PROPERTY_CHANGE;
import static org.squashtest.tm.jooq.domain.Tables.SYNC_REQUIREMENT_CREATION;
import static org.squashtest.tm.jooq.domain.Tables.SYNC_REQUIREMENT_UPDATE;

import java.util.Arrays;
import java.util.List;
import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

/**
 * @author qtran - created on 03/11/2020
 */
public class RequirementVersionModificationHistoryGrid extends AbstractGrid {
    private final Long requirementVersionId;

    public RequirementVersionModificationHistoryGrid(Long requirementVersionId) {
        this.requirementVersionId = requirementVersionId;
    }

    @Override
    protected List<GridColumn> getColumns() {
        return Arrays.asList(
                new GridColumn(REQUIREMENT_AUDIT_EVENT.EVENT_ID),
                new GridColumn(REQUIREMENT_AUDIT_EVENT.EVENT_DATE.as(RequestAliasesConstants.DATE)),
                new GridColumn(REQUIREMENT_AUDIT_EVENT.AUTHOR.as(RequestAliasesConstants.USER)),
                new GridColumn(getEventTarget().as(RequestAliasesConstants.EVENT)),
                new GridColumn(getOldValue().as(RequestAliasesConstants.OLD_VALUE)),
                new GridColumn(getNewValue().as(RequestAliasesConstants.NEW_VALUE)),
                new GridColumn(
                        SYNC_REQUIREMENT_CREATION.SOURCE.as(RequestAliasesConstants.SYNC_REQ_CREATION_SOURCE)),
                new GridColumn(
                        SYNC_REQUIREMENT_UPDATE.SOURCE.as(RequestAliasesConstants.SYNC_REQ_UPDATE_SOURCE)));
    }

    private Field<?> getNewValue() {
        return DSL.ifnull(
                REQUIREMENT_PROPERTY_CHANGE.NEW_VALUE, REQUIREMENT_LARGE_PROPERTY_CHANGE.NEW_VALUE);
    }

    private Field<?> getOldValue() {
        return DSL.ifnull(
                REQUIREMENT_PROPERTY_CHANGE.OLD_VALUE, REQUIREMENT_LARGE_PROPERTY_CHANGE.OLD_VALUE);
    }

    private Field<?> getEventTarget() {
        return DSL.ifnull(
                REQUIREMENT_PROPERTY_CHANGE.PROPERTY_NAME, REQUIREMENT_LARGE_PROPERTY_CHANGE.PROPERTY_NAME);
    }

    @Override
    protected Table<?> getTable() {
        return REQUIREMENT_AUDIT_EVENT
                .leftJoin(REQUIREMENT_PROPERTY_CHANGE)
                .on(REQUIREMENT_PROPERTY_CHANGE.EVENT_ID.eq(REQUIREMENT_AUDIT_EVENT.EVENT_ID))
                .leftJoin(REQUIREMENT_LARGE_PROPERTY_CHANGE)
                .on(REQUIREMENT_LARGE_PROPERTY_CHANGE.EVENT_ID.eq(REQUIREMENT_AUDIT_EVENT.EVENT_ID))
                .leftJoin(SYNC_REQUIREMENT_CREATION)
                .on(SYNC_REQUIREMENT_CREATION.EVENT_ID.eq(REQUIREMENT_AUDIT_EVENT.EVENT_ID))
                .leftJoin(SYNC_REQUIREMENT_UPDATE)
                .on(SYNC_REQUIREMENT_UPDATE.EVENT_ID.eq(REQUIREMENT_AUDIT_EVENT.EVENT_ID));
    }

    @Override
    protected Field<?> getIdentifier() {
        return REQUIREMENT_AUDIT_EVENT.EVENT_ID;
    }

    @Override
    protected Field<?> getProjectIdentifier() {
        return null;
    }

    @Override
    protected Condition craftInvariantFilter() {
        return REQUIREMENT_AUDIT_EVENT.REQ_VERSION_ID.eq(requirementVersionId);
    }
}
