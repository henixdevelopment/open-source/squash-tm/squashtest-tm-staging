/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.customreport;

import static org.squashtest.tm.domain.EntityType.CAMPAIGN;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.audit.AuditableMixin;
import org.squashtest.tm.domain.campaign.CampaignLibraryNode;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.CustomFieldBinding;
import org.squashtest.tm.domain.customreport.CustomReportCustomExport;
import org.squashtest.tm.domain.customreport.CustomReportLibraryNode;
import org.squashtest.tm.domain.customreport.CustomReportNodeType;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.service.campaign.CustomCampaignModificationService;
import org.squashtest.tm.service.campaign.IterationFinder;
import org.squashtest.tm.service.campaign.TestSuiteFinder;
import org.squashtest.tm.service.customfield.CustomFieldBindingFinderService;
import org.squashtest.tm.service.customreport.CustomReportCustomExportService;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;
import org.squashtest.tm.service.internal.display.dto.customreports.CustomExportScopeNode;
import org.squashtest.tm.service.internal.display.dto.customreports.CustomReportCustomExportDto;
import org.squashtest.tm.service.internal.dto.CustomFieldBindingModel;
import org.squashtest.tm.service.internal.dto.CustomFieldJsonConverter;
import org.squashtest.tm.service.internal.repository.CampaignFolderDao;

@Service
@Transactional
public class CustomReportCustomExportServiceImpl implements CustomReportCustomExportService {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(CustomReportCustomExportServiceImpl.class);
    @Inject private CustomCampaignModificationService customCampaignModificationService;
    @Inject private IterationFinder iterationFinder;
    @Inject private TestSuiteFinder testSuiteFinder;
    @Inject private CustomFieldBindingFinderService cufBindingService;
    @Inject private CustomFieldJsonConverter customFieldConverter;
    @Inject private CustomReportLibraryNodeService customReportLibraryNodeService;

    @Inject private CampaignFolderDao campaignFolderDao;

    @Override
    public CustomReportCustomExportDto findCustomExportDtoByNodeId(Long customReportLibraryNodeId) {
        CustomReportLibraryNode crln =
                customReportLibraryNodeService.findCustomReportLibraryNodeById(customReportLibraryNodeId);

        if (crln.getEntityType().getTypeName().equals(CustomReportNodeType.CUSTOM_EXPORT_NAME)) {
            CustomReportCustomExport customExport = (CustomReportCustomExport) crln.getEntity();
            return getDtoFromEntity(customExport, customReportLibraryNodeId);
        }

        return null;
    }

    @Override
    public Map<String, List<CustomFieldBindingModel>> getCustomFieldsData(
            Long mainProjectId,
            List<IterationTestPlanItem> itpis,
            Map<String, List<CustomFieldBindingModel>> map) {

        // Get the ids of the projects of the test case linked to the given campaign (excluding the main
        // project)
        List<Long> projectIds =
                itpis.stream()
                        .map(
                                itpi -> {
                                    // for deleted TestCases
                                    TestCase testCase = itpi.getReferencedTestCase();
                                    if (testCase != null) {
                                        return testCase.getProject().getId();
                                    } else {
                                        return null;
                                    }
                                })
                        .distinct()
                        .filter(projectId -> projectId != null && !projectId.equals(mainProjectId))
                        .toList();

        for (Long projectId : projectIds) {
            List<CustomFieldBinding> cufs =
                    cufBindingService.findCustomFieldsForProjectAndEntity(
                            projectId, BindableEntity.TEST_CASE);
            for (CustomFieldBinding binding : cufs) {
                map.get(BindableEntity.TEST_CASE.toString()).add(customFieldConverter.toJson(binding));
            }
        }
        return map;
    }

    private CustomReportCustomExportDto getDtoFromEntity(
            CustomReportCustomExport entity, Long libraryNodeId) {
        List<EntityReference> scopeEntityRefList = entity.getScope();

        CustomReportCustomExportDto dto = new CustomReportCustomExportDto();
        dto.setId(entity.getId());
        dto.setCustomReportLibraryNodeId(libraryNodeId);
        dto.setProjectId(entity.getProject().getId());
        dto.setName(entity.getName());

        setScopeNodesToDto(scopeEntityRefList, dto);
        setCustomFieldsOnScope(dto);

        dto.setColumns(
                entity.getColumns().stream()
                        .map(CustomReportCustomExportDto.CustomReportCustomExportColumnDto::from)
                        .toList());
        doAuditableAttributes(dto, entity);
        return dto;
    }

    private void setScopeNodesToDto(
            List<EntityReference> scopeEntityRefList, CustomReportCustomExportDto dto) {
        List<CustomExportScopeNode> scopeNodes = new ArrayList<>();
        scopeEntityRefList.forEach(entityRef -> appendScopeEntityValuesToLists(entityRef, scopeNodes));
        dto.setScopeNodes(scopeNodes);
    }

    private void setCustomFieldsOnScope(CustomReportCustomExportDto dto) {
        List<Long> scopeProjectIds =
                dto.getScopeNodes().stream().map(CustomExportScopeNode::getProjectId).toList();
        Map<Long, String> cufLabelByIdMap =
                cufBindingService.findCustomFieldLabelsByIdsFromProjectIds(scopeProjectIds);
        dto.setCustomFieldsOnScope(cufLabelByIdMap);
    }

    private void doAuditableAttributes(
            CustomReportCustomExportDto dto, CustomReportCustomExport entity) {
        AuditableMixin audit = (AuditableMixin) entity;
        dto.setCreatedBy(audit.getCreatedBy());
        dto.setCreatedOn(audit.getCreatedOn());
        dto.setLastModifiedBy(audit.getLastModifiedBy());
        dto.setLastModifiedOn(audit.getLastModifiedOn());
    }

    private void appendScopeEntityValuesToLists(
            EntityReference entityReference, List<CustomExportScopeNode> scopeNodes) {
        String entityNodeId = entityReference.toNodeId();
        switch (entityReference.getType()) {
            case CAMPAIGN_FOLDER:
            case CAMPAIGN:
                addLibraryNode(entityReference, scopeNodes, entityNodeId);
                break;
            case ITERATION:
                addIterationNode(entityReference, scopeNodes, entityNodeId);
                break;
            case TEST_SUITE:
                addTestSuiteNode(entityReference, scopeNodes, entityNodeId);
                break;
            default:
                throw new IllegalArgumentException(
                        "Entity of type " + entityReference.getType().name() + " is not supported");
        }
    }

    private void addLibraryNode(
            EntityReference entityReference,
            List<CustomExportScopeNode> scopeNodes,
            String entityNodeId) {
        CampaignLibraryNode libraryNode = getCampaignLibraryId(entityReference);
        if (Objects.nonNull(libraryNode)) {
            scopeNodes.add(
                    new CustomExportScopeNode(
                            entityNodeId, libraryNode.getName(), libraryNode.getProject().getId()));
        } else {
            logDeletedEntity(entityReference);
        }
    }

    private CampaignLibraryNode getCampaignLibraryId(EntityReference entityReference) {
        return CAMPAIGN.equals(entityReference.getType())
                ? customCampaignModificationService.findCampaigWithExistenceCheck(entityReference.getId())
                : campaignFolderDao.findById(entityReference.getId());
    }

    private void addTestSuiteNode(
            EntityReference entityReference,
            List<CustomExportScopeNode> scopeNodes,
            String entityNodeId) {
        try {
            TestSuite testSuite = testSuiteFinder.findById(entityReference.getId());
            if (Objects.nonNull(testSuite)) {
                scopeNodes.add(
                        new CustomExportScopeNode(
                                entityNodeId, testSuite.getName(), testSuite.getProject().getId()));
            }
        } catch (EntityNotFoundException e) {
            LOGGER.info("Test suite with id {} seems to have been deleted.", entityReference.getId(), e);
        }
    }

    private void addIterationNode(
            EntityReference entityReference,
            List<CustomExportScopeNode> scopeNodes,
            String entityNodeId) {
        Iteration iteration = iterationFinder.findById(entityReference.getId());
        if (Objects.nonNull(iteration)) {
            scopeNodes.add(
                    new CustomExportScopeNode(
                            entityNodeId, iteration.getName(), iteration.getProject().getId()));
        } else {
            logDeletedEntity(entityReference);
        }
    }

    private void logDeletedEntity(EntityReference entityReference) {
        LOGGER.info(
                "Entity of type {} with id {} seems to have been deleted.",
                entityReference.getType().name(),
                entityReference.getId());
    }
}
