/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.milestone;

import static org.squashtest.tm.domain.milestone.MilestoneStatus.getAllStatusAllowingObjectBind;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.squashtest.tm.core.foundation.annotation.CleanHtml;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.milestone.MilestoneStatus;

public record MilestoneDuplicationModel(
        String label,
        MilestoneStatus status,
        Date endDate,
        @CleanHtml String description,
        boolean bindToRequirements,
        boolean bindToTestCases) {

    public static Milestone toMilestone(MilestoneDuplicationModel model) {
        List<MilestoneStatus> allowedStatuses = getAllStatusAllowingObjectBind();

        if (allowedStatuses.contains(model.status())) {
            Milestone milestone = new Milestone();
            milestone.setLabel(Objects.requireNonNull(model.label()));
            milestone.setStatus(model.status());
            milestone.setEndDate(model.endDate());
            milestone.setDescription(model.description());

            return milestone;
        }

        throw new IllegalArgumentException(
                String.format(
                        "%s %s %s",
                        "The status: ",
                        model.status(),
                        " is not a valid status for milestone creation from milestone duplication operation."));
    }
}
