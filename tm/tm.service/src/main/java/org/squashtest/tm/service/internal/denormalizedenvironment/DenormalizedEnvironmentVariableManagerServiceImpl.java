/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.denormalizedenvironment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import org.squashtest.tm.domain.environmenttag.DenormalizedEnvironmentHolderType;
import org.squashtest.tm.domain.environmentvariable.DenormalizedEnvironmentVariable;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariable;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.service.denormalizedenvironment.DenormalizedEnvironmentVariableManagerService;
import org.squashtest.tm.service.internal.repository.DenormalizedEnvironmentVariableDao;
import org.squashtest.tm.service.internal.repository.EnvironmentVariableDao;

@Service("squashtest.tm.service.DenormalizedEnvironmentVariableManagerService")
@Transactional
public class DenormalizedEnvironmentVariableManagerServiceImpl
        implements DenormalizedEnvironmentVariableManagerService {

    private final DenormalizedEnvironmentVariableDao denormalizedEnvironmentVariableDao;
    private final EnvironmentVariableDao environmentVariableDao;

    public DenormalizedEnvironmentVariableManagerServiceImpl(
            DenormalizedEnvironmentVariableDao denormalizedEnvironmentVariableDao,
            EnvironmentVariableDao environmentVariableDao) {
        this.denormalizedEnvironmentVariableDao = denormalizedEnvironmentVariableDao;
        this.environmentVariableDao = environmentVariableDao;
    }

    @Override
    public void createAllDenormalizedEnvironmentVariablesForAutomatedExecution(
            Map<String, String> environmentVariablesMap, AutomatedExecutionExtender extender) {

        List<EnvironmentVariable> environmentVariableList =
                environmentVariableDao.findAllByName(environmentVariablesMap.keySet());
        List<DenormalizedEnvironmentVariable> denormalizedEnvironmentVariables =
                convertEnvironmentVariablesToDenormalized(
                        environmentVariableList, environmentVariablesMap, extender.getId());
        denormalizedEnvironmentVariableDao.saveAll(denormalizedEnvironmentVariables);
    }

    @Override
    public void handleEnvironmentVariableDeletion(EnvironmentVariable environmentVariable) {
        List<DenormalizedEnvironmentVariable> denormalizedEnvironmentVariables =
                denormalizedEnvironmentVariableDao.findAllByEnvironmentVariable(environmentVariable);
        if (!denormalizedEnvironmentVariables.isEmpty()) {
            denormalizedEnvironmentVariables.forEach(
                    denormalizedEnvironmentVariable ->
                            denormalizedEnvironmentVariable.setEnvironmentVariable(null));
        }
    }

    @Override
    public void removeDenormalizedEnvironmentVariablesOnExecutionDelete(
            AutomatedExecutionExtender automatedExecutionExtender) {
        DenormalizedEnvironmentHolderType holderType =
                DenormalizedEnvironmentHolderType.AUTOMATED_EXECUTION_EXTENDER;
        List<DenormalizedEnvironmentVariable> environmentVariables =
                denormalizedEnvironmentVariableDao.findAllByHolderIdAndHolderType(
                        automatedExecutionExtender.getId(), holderType);

        if (!environmentVariables.isEmpty()) {
            denormalizedEnvironmentVariableDao.deleteAll(environmentVariables);
        }
    }

    private List<DenormalizedEnvironmentVariable> convertEnvironmentVariablesToDenormalized(
            List<EnvironmentVariable> environmentVariables,
            Map<String, String> environmentVariablesMap,
            Long extenderId) {
        List<DenormalizedEnvironmentVariable> denormalizedEnvironmentVariables = new ArrayList<>();
        environmentVariables.forEach(
                variable -> {
                    DenormalizedEnvironmentVariable denormalizedEnvironmentVariable =
                            new DenormalizedEnvironmentVariable(
                                    variable,
                                    extenderId,
                                    DenormalizedEnvironmentHolderType.AUTOMATED_EXECUTION_EXTENDER,
                                    variable.getName(),
                                    environmentVariablesMap.get(variable.getName()),
                                    variable.getInputType().name());

                    denormalizedEnvironmentVariables.add(denormalizedEnvironmentVariable);
                });
        return denormalizedEnvironmentVariables;
    }
}
