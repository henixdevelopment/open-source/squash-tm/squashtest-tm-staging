/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity.executionworkspace;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportField;
import org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity.AbstractPivotEntity;

public abstract class AbstractGenericExecutionWorkspace extends AbstractPivotEntity {
    @JsonProperty(JsonImportField.NAME)
    private String name;

    @JsonProperty(JsonImportField.DESCRIPTION)
    private String description;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(JsonImportField.REFERENCE)
    private String reference;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(JsonImportField.SCHEDULED_START_DATE)
    private String scheduledStartDate;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(JsonImportField.SCHEDULED_END_DATE)
    private String scheduledEndDate;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(JsonImportField.ACTUAL_START_DATE)
    private String actualStartDate;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(JsonImportField.ACTUAL_END_DATE)
    private String actualEndDate;

    @JsonProperty(JsonImportField.TEST_PLAN_TEST_CASE_IDS)
    private List<String> testPlanTestCaseIds = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getScheduledStartDate() {
        return scheduledStartDate;
    }

    public void setScheduledStartDate(String scheduledStartDate) {
        this.scheduledStartDate = scheduledStartDate;
    }

    public String getScheduledEndDate() {
        return scheduledEndDate;
    }

    public void setScheduledEndDate(String scheduledEndDate) {
        this.scheduledEndDate = scheduledEndDate;
    }

    public String getActualStartDate() {
        return actualStartDate;
    }

    public void setActualStartDate(String actualStartDate) {
        this.actualStartDate = actualStartDate;
    }

    public String getActualEndDate() {
        return actualEndDate;
    }

    public void setActualEndDate(String actualEndDate) {
        this.actualEndDate = actualEndDate;
    }

    public List<String> getTestPlanTestCaseIds() {
        return testPlanTestCaseIds;
    }

    public void setTestPlanTestCaseIds(List<String> testPlanTestCaseIds) {
        this.testPlanTestCaseIds = testPlanTestCaseIds;
    }
}
