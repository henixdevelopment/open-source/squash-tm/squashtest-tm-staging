/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporter.campaignworkspace;

import java.util.Objects;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationStatus;

public class IterationToImport extends AbstractCampaignWorkspaceEntityWithTimePeriodInfo {

    IterationStatus status;

    public IterationStatus getStatus() {
        return status;
    }

    public void setStatus(IterationStatus status) {
        this.status = status;
    }

    public Iteration toIteration() {
        Iteration iteration = new Iteration();
        iteration.setName(name);
        iteration.setDescription(description);

        if (Objects.nonNull(reference)) {
            iteration.setReference(reference);
        }

        iteration.setScheduledStartDate(scheduledTimePeriod.getScheduledStartDate());
        iteration.setScheduledEndDate(scheduledTimePeriod.getScheduledEndDate());

        if (!actualTimePeriod.isActualStartAuto()) {
            iteration.setActualStartDate(actualTimePeriod.getActualStartDate());
        }

        if (!actualTimePeriod.isActualEndAuto()) {
            iteration.setActualEndDate(actualTimePeriod.getActualEndDate());
        }

        iteration.setActualStartAuto(actualTimePeriod.isActualStartAuto());
        iteration.setActualEndAuto(actualTimePeriod.isActualEndAuto());

        if (Objects.nonNull(status)) {
            iteration.setStatus(status);
        }

        return iteration;
    }
}
