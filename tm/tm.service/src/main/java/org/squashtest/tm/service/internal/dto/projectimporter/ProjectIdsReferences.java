/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporter;

import org.squashtest.tm.domain.project.GenericProject;

public class ProjectIdsReferences {
    private Long id;
    private Long testCaseLibraryId;
    private Long requirementLibraryId;

    private Long campaignLibraryId;

    public ProjectIdsReferences(GenericProject project) {
        this.id = project.getId();
        this.testCaseLibraryId = project.getTestCaseLibrary().getId();
        this.requirementLibraryId = project.getRequirementLibrary().getId();
        this.campaignLibraryId = project.getCampaignLibrary().getId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTestCaseLibraryId() {
        return testCaseLibraryId;
    }

    public void setTestCaseLibraryId(Long testCaseLibraryId) {
        this.testCaseLibraryId = testCaseLibraryId;
    }

    public Long getRequirementLibraryId() {
        return requirementLibraryId;
    }

    public void setRequirementLibraryId(Long requirementLibraryId) {
        this.requirementLibraryId = requirementLibraryId;
    }

    public Long getCampaignLibraryId() {
        return campaignLibraryId;
    }

    public void setCampaignLibraryId(Long campaignLibraryId) {
        this.campaignLibraryId = campaignLibraryId;
    }
}
