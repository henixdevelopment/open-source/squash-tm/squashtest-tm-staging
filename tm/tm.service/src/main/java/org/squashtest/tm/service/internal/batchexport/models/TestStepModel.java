/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchexport.models;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import org.squashtest.tm.service.internal.batchexport.models.ExportModel.CustomField;

public final class TestStepModel {
    public static final Comparator<TestStepModel> COMPARATOR =
            new Comparator<TestStepModel>() {
                @Override
                public int compare(TestStepModel o1, TestStepModel o2) {
                    int comp1 = o1.getTcOwnerPath().compareTo(o2.getTcOwnerPath());
                    if (comp1 == 0) {
                        return o1.getOrder() - o2.getOrder();
                    } else {
                        return comp1;
                    }
                }
            };

    private String tcOwnerPath;
    private long tcOwnerId;
    private long id;
    private int order;
    private Integer isCallStep;
    private String action;
    private String result;
    private Long nbReq;
    private Long nbAttach;
    private String calledDsName;
    private boolean delegateParameters;
    private List<CustomField> cufs = new LinkedList<>();

    // about delegateParameters : when fetching an action step we want to select the literal
    // 'false'. However Hibernate
    // can't do that, hence it is here shipped as int before casting to Boolean.
    public TestStepModel(
            long tcOwnerId,
            long id,
            int order,
            Integer isCallStep,
            String action,
            String result,
            Long nbReq,
            Long nbAttach,
            String calledDsName,
            Integer delegateParameters) {

        super();
        this.tcOwnerId = tcOwnerId;
        this.id = id;
        this.order = order;
        this.isCallStep = isCallStep;
        this.action = action;
        this.result = result;
        this.nbReq = nbReq;
        this.nbAttach = nbAttach;
        this.calledDsName = calledDsName; // special call steps
        this.delegateParameters = delegateParameters == 1; // special call steps
    }

    public String getTcOwnerPath() {
        return tcOwnerPath;
    }

    public void setTcOwnerPath(String tcOwnerPath) {
        this.tcOwnerPath = tcOwnerPath;
    }

    public long getTcOwnerId() {
        return tcOwnerId;
    }

    public void setTcOwnerId(long tcOwnerId) {
        this.tcOwnerId = tcOwnerId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public Integer getIsCallStep() {
        return isCallStep;
    }

    public void setIsCallStep(Integer isCallStep) {
        this.isCallStep = isCallStep;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Long getNbReq() {
        return nbReq;
    }

    public void setNbReq(Long nbReq) {
        this.nbReq = nbReq;
    }

    public Long getNbAttach() {
        return nbAttach;
    }

    public void setNbAttach(Long nbAttach) {
        this.nbAttach = nbAttach;
    }

    public void addCuf(CustomField cuf) {
        cufs.add(cuf);
    }

    public List<CustomField> getCufs() {
        return cufs;
    }

    public String getDsName() {
        String name;
        if (delegateParameters) {
            name = "INHERIT";
        } else {
            // inexact (we should explicitly treat the no-dataset scenario,
            // but conveniently sufficient here
            name = calledDsName;
        }
        return name;
    }
}
