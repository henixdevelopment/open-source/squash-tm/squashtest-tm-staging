/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testcase;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.domain.testcase.TestStep;
import org.squashtest.tm.service.internal.display.dto.testcase.AbstractTestStepDto;

/**
 * @author Gregory
 */
public interface CustomTestCaseFinder {

    TestCase findTestCaseWithSteps(long testCaseId);

    List<TestStep> findStepsByTestCaseId(long testCaseId);

    /**
     * Will find the ids and importance of the given test cases with importanceAuto = true
     *
     * @param testCaseIds : the id of the testCases
     * @return a map with :
     *     <ul>
     *       <li>key : the id of the test case
     *       <li>value : it's {@link TestCaseImportance}
     *     </ul>
     */
    Map<Long, TestCaseImportance> findImpTCWithImpAuto(Collection<Long> testCaseIds);

    /**
     * willgo through the list of test-case to update and retain the ones that are calling the updated
     * test case.
     *
     * @param updatedId : the id of the test case we want the calling test cases of
     * @param callingCandidates : the ids of potential calling test cases we are interested in
     * @return the set of test case ids that are calling(direclty or indireclty) the updated one and
     *     whose ids are in the callingCadidates list.
     */
    Set<Long> findCallingTCids(long updatedId, Collection<Long> callingCandidates);

    /**
     * returns direct milestone membership, plus indirect milestones due to verified requirements
     *
     * @param testCaseId
     * @return
     */
    Collection<Milestone> findAllMilestones(long testCaseId);

    AbstractTestStepDto findTestStep(Long testStepId);

    /**
     * Method for retrieving test case library node full names from test case library node ids
     *
     * @param testCaseLibraryNodeIds list of test case library node ids
     * @return ordered list of test case full names (ref - name) or folder names
     */
    List<String> retrieveFullNameByTestCaseLibraryNodeIds(
            List<Long> testCaseLibraryNodeIds, List<Long> projectIds);
}
