/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation.httpclient;

import java.io.IOException;
import org.apache.http.Header;
import org.apache.http.HeaderIterator;
import org.apache.http.ProtocolVersion;
import org.apache.http.RequestLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.params.HttpParams;

public class HttpPostRequest implements Request {

    private final HttpPost delegate;

    public HttpPostRequest(HttpPost delegate) {
        this.delegate = delegate;
    }

    @Override
    public Response execute() throws IOException {
        final CloseableHttpResponse response = HttpClients.createSystem().execute(delegate);
        return new Response(response);
    }

    @Override
    public RequestLine getRequestLine() {
        return delegate.getRequestLine();
    }

    @Override
    public ProtocolVersion getProtocolVersion() {
        return delegate.getProtocolVersion();
    }

    @Override
    public boolean containsHeader(String string) {
        return delegate.containsHeader(string);
    }

    @Override
    public Header[] getHeaders(String string) {
        return delegate.getHeaders(string);
    }

    @Override
    public Header getFirstHeader(String string) {
        return delegate.getFirstHeader(string);
    }

    @Override
    public Header getLastHeader(String string) {
        return delegate.getLastHeader(string);
    }

    @Override
    public Header[] getAllHeaders() {
        return delegate.getAllHeaders();
    }

    @Override
    public void addHeader(Header header) {
        delegate.addHeader(header);
    }

    @Override
    public void addHeader(String string, String string1) {
        delegate.addHeader(string, string1);
    }

    @Override
    public void setHeader(Header header) {
        delegate.setHeader(header);
    }

    @Override
    public void setHeader(String string, String string1) {
        delegate.setHeader(string, string1);
    }

    @Override
    public void setHeaders(Header[] headers) {
        delegate.setHeaders(headers);
    }

    @Override
    public void removeHeader(Header header) {
        delegate.removeHeader(header);
    }

    @Override
    public void removeHeaders(String string) {
        delegate.removeHeaders(string);
    }

    @Override
    public HeaderIterator headerIterator() {
        return delegate.headerIterator();
    }

    @Override
    public HeaderIterator headerIterator(String string) {
        return delegate.headerIterator(string);
    }

    @Override
    public HttpParams getParams() {
        return delegate.getParams();
    }

    @Override
    public void setParams(HttpParams hp) {
        delegate.setParams(hp);
    }
}
