/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.security.acls.domain.objectidentity.retrievers;

import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY_NODE;

import java.util.Optional;
import org.jooq.DSLContext;
import org.jooq.Record1;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementLibrary;

@Component
public class RequirementOptimizedObjectIdentityRetriever
        extends AbstractOptimizedObjectIdentityRetriever {

    public RequirementOptimizedObjectIdentityRetriever(DSLContext jooq) {
        super(jooq);
    }

    @Override
    public String handledClass() {
        return Requirement.CLASS_NAME;
    }

    @Override
    String getLibraryClassName() {
        return RequirementLibrary.CLASS_NAME;
    }

    @Override
    Optional<Record1<Long>> getLibraryIdRecord(Long entityId) {
        return jooq.select(PROJECT.RL_ID)
                .from(PROJECT)
                .innerJoin(REQUIREMENT_LIBRARY_NODE)
                .on(REQUIREMENT_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .innerJoin(REQUIREMENT)
                .on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(REQUIREMENT.RLN_ID))
                .where(REQUIREMENT.RLN_ID.eq(entityId))
                .fetchOptional();
    }
}
