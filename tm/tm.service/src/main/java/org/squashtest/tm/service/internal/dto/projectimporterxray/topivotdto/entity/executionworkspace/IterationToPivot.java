/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity.executionworkspace;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.squashtest.tm.domain.campaign.IterationStatus;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportField;
import org.squashtest.tm.service.internal.dto.projectimporterxray.XrayField;
import org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.mapping.XrayStatus;

public class IterationToPivot extends AbstractGenericExecutionWorkspace {
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @JsonProperty(JsonImportField.STATUS)
    private IterationStatus status;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(JsonImportField.ACTUAL_START_DATE_AUTO)
    private boolean actualStartDateAuto;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(JsonImportField.ACTUAL_END_DATE_AUTO)
    private boolean actualEndDateAuto;

    @Override
    public BindableEntity getBindableEntity() {
        return BindableEntity.ITERATION;
    }

    public void setPivotId(Integer id) {
        this.pivotId = String.format("%s%s", XrayField.BASE_PIVOT_ID_ITERATION, id);
    }

    public IterationStatus getStatus() {
        return status;
    }

    public void setStatus(XrayStatus status) {
        this.status = status.getIterationStatus();
    }

    public boolean isActualStartDateAuto() {
        return actualStartDateAuto;
    }

    public void setActualStartDateAuto(boolean actualStartDateAuto) {
        this.actualStartDateAuto = actualStartDateAuto;
    }

    public boolean isActualEndDateAuto() {
        return actualEndDateAuto;
    }

    public void setActualEndDateAuto(boolean actualEndDateAuto) {
        this.actualEndDateAuto = actualEndDateAuto;
    }
}
