/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.campaign;

import java.util.Collection;
import java.util.List;
import org.squashtest.tm.api.plugin.UsedInPlugin;
import org.squashtest.tm.core.dynamicmanager.annotation.QueryParam;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.milestone.Milestone;

public interface CustomCampaignModificationService {

    void rename(long campaignId, String newName);

    boolean checkIterationNameAvailable(String name, List<Iteration> iterations);

    @UsedInPlugin("campaignassistant")
    Integer countIterations(@QueryParam("campaignId") Long campaignId);

    /* ********************** milestones section ******************* */

    /**
     * Bind a milestone to a campaign. Any previous milestone will be unbound because a campaign can
     * be bound to only one milestone.
     *
     * @param campaignId
     * @param milestoneId
     */
    void bindMilestone(long campaignId, long milestoneId);

    void unbindMilestones(long campaignId, Collection<Long> milestoneIds);

    void unbindSingleMilestone(long campaignId, Long milestoneId);

    Collection<Milestone> findAllMilestones(long campaignId);

    /**
     * This method retrieves a {@link Campaign} with the given id.
     *
     * @param campaignId The id of the campaign to retrieve
     * @return The campaign with the given id or null if it does not exist
     */
    Campaign findCampaigWithExistenceCheck(long campaignId);
}
