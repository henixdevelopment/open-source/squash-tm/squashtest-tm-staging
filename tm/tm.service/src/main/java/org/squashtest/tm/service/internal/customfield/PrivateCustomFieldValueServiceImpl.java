/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.customfield;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.map.MultiValueMap;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.IdentifiedUtil;
import org.squashtest.tm.domain.campaign.SprintStatus;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.BoundEntity;
import org.squashtest.tm.domain.customfield.CustomField;
import org.squashtest.tm.domain.customfield.CustomFieldBinding;
import org.squashtest.tm.domain.customfield.CustomFieldValue;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.customfield.RenderingLocation;
import org.squashtest.tm.domain.customfield.RichTextValue;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.exception.campaign.SprintClosedException;
import org.squashtest.tm.service.annotation.CachableType;
import org.squashtest.tm.service.annotation.CacheResult;
import org.squashtest.tm.service.attachment.AttachmentManagerService;
import org.squashtest.tm.service.audit.AuditModificationService;
import org.squashtest.tm.service.internal.copier.BoundEntityCopy;
import org.squashtest.tm.service.internal.display.dto.customreports.CustomFieldCustomExportDto;
import org.squashtest.tm.service.internal.repository.BoundEntityDao;
import org.squashtest.tm.service.internal.repository.CustomFieldBindingDao;
import org.squashtest.tm.service.internal.repository.CustomFieldValueDao;
import org.squashtest.tm.service.internal.repository.CustomFieldValueDao.CustomFieldValuesPair;
import org.squashtest.tm.service.internal.repository.display.SprintDisplayDao;
import org.squashtest.tm.service.internal.repository.hibernate.customfieldvaluesfactory.AbstractCustomFieldValuesFactory;
import org.squashtest.tm.service.security.PermissionEvaluationService;

@Service("squashtest.tm.service.CustomFieldValueManagerService")
@Transactional
public class PrivateCustomFieldValueServiceImpl implements PrivateCustomFieldValueService {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(PrivateCustomFieldValueServiceImpl.class);

    @Inject AttachmentManagerService attachmentManagerService;

    @Inject
    @Named("defaultEditionStatusStrategy")
    private ValueEditionStatusStrategy defaultEditionStatusStrategy;

    @Inject
    @Named("requirementBoundEditionStatusStrategy")
    private ValueEditionStatusStrategy requirementBoundEditionStatusStrategy;

    @Inject private CustomFieldValueDao customFieldValueDao;

    @Inject private CustomFieldBindingDao customFieldBindingDao;

    @Inject private BoundEntityDao boundEntityDao;

    @Inject private PermissionEvaluationService permissionService;

    @Inject private AuditModificationService auditModificationService;

    @Inject private CustomFieldValuesFactoryProvider factoryProvider;

    @Inject private SprintDisplayDao sprintDisplayDao;

    public void setPermissionService(PermissionEvaluationService permissionService) {
        this.permissionService = permissionService;
    }

    @Override
    @Transactional(readOnly = true)
    public boolean hasCustomFields(BoundEntity boundEntity) {
        return boundEntityDao.hasCustomField(
                boundEntity.getBoundEntityId(), boundEntity.getBoundEntityType());
    }

    @Override
    @Transactional(readOnly = true)
    public boolean hasCustomFields(Long boundEntityId, BindableEntity bindableEntity) {
        return boundEntityDao.hasCustomField(boundEntityId, bindableEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CustomFieldValue> findAllCustomFieldValues(BoundEntity boundEntity) {
        if (!permissionService.canRead(boundEntity)
                && !permissionService.hasRole(Roles.ROLE_TA_API_CLIENT)) {
            throw new AccessDeniedException("Access is denied");
        }
        return customFieldValueDao.findAllCustomValues(
                boundEntity.getBoundEntityId(), boundEntity.getBoundEntityType());
    }

    @Override
    @Transactional(readOnly = true)
    public List<CustomFieldValue> findAllCustomFieldValues(
            long boundEntityId, BindableEntity bindableEntity) {
        BoundEntity boundEntity = boundEntityDao.findBoundEntity(boundEntityId, bindableEntity);
        if (!permissionService.canRead(boundEntity)) {
            throw new AccessDeniedException("Access is denied");
        }
        return findAllCustomFieldValues(boundEntity);
    }

    @Override
    // well I'll skip the security check for this one because we don't really want to kill the db
    public List<CustomFieldValue> findAllCustomFieldValues(
            Collection<? extends BoundEntity> boundEntities) {

        // first, because the entities might be of different kind we must segregate them.
        Map<BindableEntity, List<Long>> compositeIds = breakEntitiesIntoCompositeIds(boundEntities);

        // second, one can now call the db and consolidate the result.
        List<CustomFieldValue> result = new ArrayList<>();

        for (Entry<BindableEntity, List<Long>> entry : compositeIds.entrySet()) {
            result.addAll(
                    customFieldValueDao.batchedFindAllCustomValuesFor(entry.getValue(), entry.getKey()));
        }

        return result;
    }

    // same : no sec, a gesture of mercy for the database
    @Override
    public List<CustomFieldValue> findAllCustomFieldValues(
            Collection<? extends BoundEntity> boundEntities,
            Collection<CustomField> restrictedToThoseCustomfields) {

        // first, because the entities might be of different kind we must segregate them.
        Map<BindableEntity, List<Long>> compositeIds = breakEntitiesIntoCompositeIds(boundEntities);

        // second, one can now call the db and consolidate the result.
        List<CustomFieldValue> result = new ArrayList<>();

        for (Entry<BindableEntity, List<Long>> entry : compositeIds.entrySet()) {
            result.addAll(
                    customFieldValueDao.batchedRestrictedFindAllCustomValuesFor(
                            entry.getValue(), entry.getKey(), restrictedToThoseCustomfields));
        }
        return result;
    }

    @Override
    public List<CustomFieldValue> findAllCustomFieldValuesOfCustomField(long cufId) {
        return customFieldValueDao.findAllCustomValuesOfCustomField(cufId);
    }

    @Override
    public void cascadeCustomFieldValuesCreation(CustomFieldBinding binding) {
        BindableEntity boundEntity = binding.getBoundEntity();
        AbstractCustomFieldValuesFactory factory = factoryProvider.getFactory(boundEntity);
        factory.insertValues(binding);
    }

    public void cascadeCustomFieldValuesCreationNotCreatedFolderYet(
            CustomFieldBinding binding, BoundEntity entity) {
        CustomFieldValue value = binding.createNewValue();
        handleRichTextAttachments(entity, value);
        value.setBoundEntity(entity);
        customFieldValueDao.save(value);
    }

    @Override
    public <T extends BoundEntity> void batchFolderCustomFieldValuesCreation(
            List<T> entities, BindableEntity bindableEntity, Long projectId) {
        List<CustomFieldBinding> bindings =
                customFieldBindingDao.findAllForProjectAndEntity(projectId, bindableEntity);

        List<CustomFieldValue> newValues =
                entities.stream()
                        .flatMap(
                                entity ->
                                        bindings.stream()
                                                .map(
                                                        binding -> {
                                                            CustomFieldValue value = binding.createNewValue();
                                                            handleRichTextAttachments(entity, value);
                                                            value.setBoundEntity(entity);
                                                            return value;
                                                        }))
                        .toList();

        customFieldValueDao.saveAll(newValues);
    }

    @Override
    public void cascadeCustomFieldValuesDeletion(CustomFieldBinding binding) {
        customFieldValueDao.deleteAllForBinding(binding.getId());
    }

    @Override
    public void cascadeCustomFieldValuesDeletion(List<Long> customFieldBindingIds) {
        customFieldValueDao.deleteAllFromBindings(customFieldBindingIds);
    }

    @Override
    public void createAllCustomFieldValues(BoundEntity entity, Project project) {
        createAllCustomFieldValues(entity, project, Collections.emptyMap());
    }

    @Override
    public void createAllCustomFieldValues(
            BoundEntity entity, Project project, Map<Long, RawValue> initialCustomFieldValues) {
        LOGGER.debug(
                "creating customfield values for entity {}#{}",
                entity.getBoundEntityType(),
                entity.getBoundEntityId());

        if (project == null) {
            project = entity.getProject();
        }

        List<CustomFieldBinding> bindings = optimizedFindCustomField(entity, project);

        if (LOGGER.isTraceEnabled()) {
            List<String> codes = bindings.stream().map(b -> b.getCustomField().getCode()).toList();
            LOGGER.trace("creating values for customfields : {}", codes);
        }

        /* **************************************************************************************************
         * [Issue 3808]
         *
         * It seems that after #2061 (revision 9540a9a08c49) a defensive block of code was added in order to
         * prevent the creation of a custom field if it exists already for the target entity.
         *
         * I don't know really why it was needed but it killed performances, so I'm rewriting it
         * and hope it makes it faster. Best should be to get rid of it completely.
         ************************************************************************************************* */
        List<CustomFieldBinding> whatIsAlreadyBound =
                customFieldBindingDao.findEffectiveBindingsForEntity(
                        entity.getBoundEntityId(), entity.getBoundEntityType());

        bindings.removeAll(whatIsAlreadyBound);

        /* **** /[Issue 3808]  ************/

        for (CustomFieldBinding binding : bindings) {
            CustomFieldValue customFieldValue = binding.createNewValue();
            Long customFieldId = customFieldValue.getCustomField().getId();

            if (initialCustomFieldValues.containsKey(customFieldId)) {
                RawValue newValue = initialCustomFieldValues.get(customFieldId);
                newValue.setValueFor(customFieldValue);
            }

            handleRichTextAttachments(entity, customFieldValue);

            customFieldValue.setBoundEntity(entity);
            customFieldValueDao.save(customFieldValue);
        }
    }

    private void handleRichTextAttachments(BoundEntity entity, CustomFieldValue customFieldValue) {
        if (customFieldValue instanceof RichTextValue) {
            String richText = customFieldValue.getValue();
            String html =
                    attachmentManagerService.handleRichTextAttachments(richText, entity.getAttachmentList());
            if (!richText.equals(html)) {
                customFieldValue.setValue(html);
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void createAllCustomFieldValues(Collection<? extends BoundEntity> entities, Project p) {
        if (entities.isEmpty()) {
            return;
        }

        BoundEntity firstEntity = entities.iterator().next();

        Project project = p;
        if (p == null) {
            project = firstEntity.getProject();
        }

        List<CustomFieldBinding> bindings = optimizedFindCustomField(firstEntity, project);

        /* **************************************************************************************************
         * [Issue 3808]
         *
         * It seems that after #2061 (revision 9540a9a08c49) a defensive block of code was added in order to
         * prevent the creation of a custom field if it exists already for the target entity.
         *
         * I don't know really why it was needed but it killed performances, so I'm rewriting it
         * and hope it makes it faster. Best should be to get rid of it completely. Its inefficient and ugly.
         ************************************************************************************************* */

        MultiValueMap bindingPerEntities = findEffectiveBindings(entities);

        /* **** /[Issue 3808]  ************/

        // main loop
        for (BoundEntity entity : entities) {

            Collection<CustomFieldBinding> toBeBound = bindings;

            Collection<CustomFieldBinding> effectiveBindings =
                    bindingPerEntities.getCollection(entity.getBoundEntityId());

            if (effectiveBindings != null) {
                toBeBound = CollectionUtils.subtract(bindings, effectiveBindings);
            }

            for (CustomFieldBinding toBind : toBeBound) {
                CustomFieldValue value = toBind.createNewValue();
                handleRichTextAttachments(entity, value);
                value.setBoundEntity(entity);
                customFieldValueDao.save(value);
            }
        }
    }

    @Override
    public void deleteAllCustomFieldValues(BoundEntity entity) {
        customFieldValueDao.deleteAllForEntity(entity.getBoundEntityId(), entity.getBoundEntityType());
    }

    @Override
    public void deleteAllCustomFieldValues(BindableEntity entityType, List<Long> entityIds) {
        customFieldValueDao.deleteAllForEntities(entityType, entityIds);
    }

    @Override
    public void copyCustomFieldValues(BoundEntity source, BoundEntity recipient) {
        List<CustomFieldValue> sourceValues =
                customFieldValueDao.findAllCustomValues(
                        source.getBoundEntityId(), source.getBoundEntityType());

        for (CustomFieldValue value : sourceValues) {
            CustomFieldValue copy = value.copy();
            copy.setBoundEntity(recipient);
            customFieldValueDao.save(copy);
        }
    }

    /**
     * @see
     *     org.squashtest.tm.service.customfield.CustomFieldValueFinderService#areValuesEditable(long,
     *     org.squashtest.tm.domain.customfield.BindableEntity)
     */
    @Override
    public boolean areValuesEditable(long boundEntityId, BindableEntity bindableEntity) {
        return editableStrategy(bindableEntity).isEditable(boundEntityId, bindableEntity);
    }

    @Override
    public List<CustomFieldValue> findAllForEntityAndRenderingLocation(
            BoundEntity boundEntity, RenderingLocation renderingLocation) {
        return customFieldValueDao.findAllForEntityAndRenderingLocation(
                boundEntity.getBoundEntityId(), boundEntity.getBoundEntityType(), renderingLocation);
    }

    /**
     * @see PrivateCustomFieldValueService#copyCustomFieldValues(Map, BindableEntity)
     */
    @Override
    public void copyCustomFieldValues(
            Map<Long, BoundEntity> copiedEntityBySourceId, BindableEntity bindableEntityType) {
        if (copiedEntityBySourceId.isEmpty()) {
            return;
        }

        Set<Long> sourceEntitiesIds = copiedEntityBySourceId.keySet();
        List<CustomFieldValue> sourceValues =
                customFieldValueDao.batchedFindAllCustomValuesFor(sourceEntitiesIds, bindableEntityType);

        for (CustomFieldValue cufSource : sourceValues) {
            BoundEntity targetCopy = copiedEntityBySourceId.get(cufSource.getBoundEntityId());
            CustomFieldValue copy = cufSource.copy();
            copy.setBoundEntity(targetCopy);
            customFieldValueDao.save(copy);
        }
    }

    @Override
    public void copyCustomFieldValues(List<BoundEntityCopy> boundEntityCopies) {
        if (boundEntityCopies.isEmpty()) {
            return;
        }

        Map<BindableEntity, List<BoundEntityCopy>> copiesByEntity =
                boundEntityCopies.stream()
                        .collect(
                                Collectors.groupingBy(
                                        boundEntityCopy -> boundEntityCopy.copy().getBoundEntityType()));

        for (var entry : copiesByEntity.entrySet()) {
            Map<Long, BoundEntity> copiedEntityBySourceId =
                    entry.getValue().stream()
                            .collect(Collectors.toMap(BoundEntityCopy::originId, BoundEntityCopy::copy));

            copyCustomFieldValues(copiedEntityBySourceId, entry.getKey());
        }
    }

    @Override
    public void copyCustomFieldValuesOnProjectChanged(
            List<BoundEntityCopy> boundEntityCopies, Long projectId) {
        if (boundEntityCopies.isEmpty()) {
            return;
        }

        Map<BindableEntity, List<BoundEntityCopy>> copiesByEntity =
                boundEntityCopies.stream()
                        .collect(
                                Collectors.groupingBy(
                                        boundEntityCopy -> boundEntityCopy.copy().getBoundEntityType()));

        for (var entry : copiesByEntity.entrySet()) {
            Map<Long, BoundEntity> copiedEntityBySource =
                    entry.getValue().stream()
                            .collect(Collectors.toMap(BoundEntityCopy::originId, BoundEntityCopy::copy));

            copyCustomFieldValuesOnProjectChanged(entry.getKey(), copiedEntityBySource, projectId);
        }
    }

    private void copyCustomFieldValuesOnProjectChanged(
            BindableEntity bindableEntity, Map<Long, BoundEntity> copyBySourceId, Long projectId) {
        Map<Long, List<CustomFieldValue>> sourceValues =
                customFieldValueDao
                        .batchedFindAllCustomValuesFor(copyBySourceId.keySet(), bindableEntity)
                        .stream()
                        .collect(Collectors.groupingBy(cfv -> cfv.getCustomField().getId()));

        List<CustomFieldBinding> projectBindings = optimizedFindCustomField(bindableEntity, projectId);

        for (CustomFieldBinding binding : projectBindings) {
            List<CustomFieldValue> values =
                    sourceValues.getOrDefault(binding.getCustomField().getId(), Collections.emptyList());

            if (values.isEmpty()) {
                for (BoundEntity entity : copyBySourceId.values()) {
                    CustomFieldValue updatedCUFValue = binding.createNewValue();
                    handleRichTextAttachments(entity, updatedCUFValue);
                    updatedCUFValue.setBoundEntity(entity);
                    customFieldValueDao.save(updatedCUFValue);
                }
            } else {
                for (CustomFieldValue cufSource : values) {
                    BoundEntity targetCopy = copyBySourceId.get(cufSource.getBoundEntityId());
                    CustomFieldValue copy = cufSource.copy();
                    copy.setBinding(binding);
                    copy.setBoundEntity(targetCopy);
                    customFieldValueDao.save(copy);
                }
            }
        }
    }

    @Override
    public void copyCustomFieldValuesContent(BoundEntity source, BoundEntity recipient) {
        List<CustomFieldValuesPair> pairs =
                customFieldValueDao.findPairedCustomFieldValues(
                        source.getBoundEntityType(), source.getBoundEntityId(), recipient.getBoundEntityId());

        for (CustomFieldValuesPair pair : pairs) {
            pair.copyContent();
        }
    }

    @Override
    public void changeValue(long customFieldValueId, RawValue newValue) {

        CustomFieldValue changedValue = customFieldValueDao.getReferenceById(customFieldValueId);

        BoundEntity boundEntity = boundEntityDao.findBoundEntity(changedValue);

        checkParentSprintStatus(boundEntity);

        if (!permissionService.hasMoreThanRead(boundEntity)) {
            throw new AccessDeniedException("access is denied");
        }

        newValue.setValueFor(changedValue);

        auditModificationService.updateRelatedToCustomFieldAuditableEntity(boundEntity);
    }

    private void checkParentSprintStatus(BoundEntity boundEntity) {
        if (BindableEntity.EXECUTION.equals(boundEntity.getBoundEntityType())) {
            SprintStatus sprintStatus =
                    sprintDisplayDao.getSprintStatusByExecutionId(boundEntity.getBoundEntityId());
            if (SprintStatus.CLOSED.equals(sprintStatus)) {
                throw new SprintClosedException();
            }
        } else if (BindableEntity.EXECUTION_STEP.equals(boundEntity.getBoundEntityType())) {
            SprintStatus sprintStatus =
                    sprintDisplayDao.getSprintStatusByExecutionStepId(boundEntity.getBoundEntityId());
            if (SprintStatus.CLOSED.equals(sprintStatus)) {
                throw new SprintClosedException();
            }
        }
    }

    // This method is just here to use the @CacheResult annotation
    @CacheResult(type = CachableType.CUSTOM_FIELD)
    private List<CustomFieldBinding> optimizedFindCustomField(BoundEntity entity) {
        return customFieldBindingDao.findAllForProjectAndEntity(
                entity.getProject().getId(), entity.getBoundEntityType());
    }

    // This method is just here to use the @CacheResult annotation
    @CacheResult(type = CachableType.CUSTOM_FIELD)
    private List<CustomFieldBinding> optimizedFindCustomField(BoundEntity entity, Project project) {
        return customFieldBindingDao.findAllForProjectAndEntity(
                project.getId(), entity.getBoundEntityType());
    }

    private List<CustomFieldBinding> optimizedFindCustomField(
            BindableEntity bindableEntity, Long projectId) {
        return customFieldBindingDao.findAllForProjectAndEntity(projectId, bindableEntity);
    }

    @Override
    // basically it's a copypasta of createAllCustomFieldValues, with some extra code in it.
    public void migrateCustomFieldValues(BoundEntity entity) {

        List<CustomFieldValue> valuesToUpdate =
                customFieldValueDao.findAllCustomValues(
                        entity.getBoundEntityId(), entity.getBoundEntityType());
        // not sure that project can be null...
        // a cuf holder without project would be a non sens...
        if (entity.getProject() != null) {
            // [SQUASH-3235] If cufs already exist after a reqToTestCase copy we must delete before
            // performing migration
            // to avoid database unique constraint violation
            List<Long> valueIds = IdentifiedUtil.extractIds(valuesToUpdate);
            customFieldValueDao.deleteAllById(valueIds);

            List<CustomFieldBinding> projectBindings = optimizedFindCustomField(entity);

            for (CustomFieldBinding binding : projectBindings) {
                CustomFieldValue updatedCUFValue = binding.createNewValue();

                findUpdatedCufValue(valuesToUpdate, updatedCUFValue);

                updatedCUFValue.setBoundEntity(entity);
                customFieldValueDao.save(updatedCUFValue);
            }
        }
    }

    private void findUpdatedCufValue(
            List<CustomFieldValue> valuesToUpdate, CustomFieldValue updatedCUFValue) {
        for (CustomFieldValue formerCUFValue : valuesToUpdate) {
            if (formerCUFValue.representsSameCustomField(updatedCUFValue)) {
                // here we use a RawValue as a container that hides us the arity of the value (single or
                // multi-valued)
                RawValue rawValue = formerCUFValue.asRawValue();
                rawValue.setValueFor(updatedCUFValue);
                break;
            }
        }
    }

    @Override
    public void migrateCustomFieldValues(Collection<BoundEntity> entities) {
        for (BoundEntity entity : entities) {
            migrateCustomFieldValues(entity);
        }
    }

    @Override
    public Map<EntityReference, Map<Long, Object>> getCufValueMapByEntityRef(
            List<EntityReference> scopeEntities, Map<EntityType, List<Long>> cufIdsMapByEntityType) {
        return customFieldValueDao.getCufValuesMapByEntityReference(
                scopeEntities, cufIdsMapByEntityType);
    }

    @Override
    public List<CustomFieldCustomExportDto> findAllAvailableCufsForCustomExport(
            List<Long> projectIds) {
        return customFieldValueDao.findAllAvailableForCustomExportByProjectIds(projectIds);
    }

    @Override
    public Map<Long, List<CustomFieldValue>> getCufValuesMapByBoundEntity(
            List<? extends BoundEntity> boundEntities) {
        if (boundEntities.isEmpty()) {
            return Collections.emptyMap();
        }

        Map<BindableEntity, List<Long>> compositeIds = breakEntitiesIntoCompositeIds(boundEntities);

        if (compositeIds.size() != 1) {
            throw new IllegalArgumentException("All entities must be of the same type");
        }

        Entry<BindableEntity, List<Long>> entry = compositeIds.entrySet().iterator().next();

        return customFieldValueDao.getCufValuesMapByBoundEntity(entry.getKey(), entry.getValue());
    }

    // *********************** private convenience methods ********************

    private Map<BindableEntity, List<Long>> breakEntitiesIntoCompositeIds(
            Collection<? extends BoundEntity> boundEntities) {

        Map<BindableEntity, List<Long>> segregatedEntities = new EnumMap<>(BindableEntity.class);

        for (BoundEntity entity : boundEntities) {
            List<Long> idList = segregatedEntities.get(entity.getBoundEntityType());

            if (idList == null) {
                idList = new ArrayList<>();
                segregatedEntities.put(entity.getBoundEntityType(), idList);
            }
            idList.add(entity.getBoundEntityId());
        }
        return segregatedEntities;
    }

    // will break if the collection is empty so use it responsible
    private MultiValueMap findEffectiveBindings(Collection<? extends BoundEntity> entities) {
        Map<BindableEntity, List<Long>> compositeIds = breakEntitiesIntoCompositeIds(entities);
        Entry<BindableEntity, List<Long>> firstEntry = compositeIds.entrySet().iterator().next();

        List<Long> entityIds = firstEntry.getValue();
        BindableEntity type = firstEntry.getKey();

        List<Object[]> whatIsAlreadyBound =
                customFieldBindingDao.findEffectiveBindingsForEntities(entityIds, type);

        MultiValueMap bindingsPerEntity = new MultiValueMap();
        for (Object[] tuple : whatIsAlreadyBound) {
            Long entityId = (Long) tuple[0];
            CustomFieldBinding binding = (CustomFieldBinding) tuple[1];
            bindingsPerEntity.put(entityId, binding);
        }

        return bindingsPerEntity;
    }

    /**
     * @param bindableEntity
     * @return
     */
    private ValueEditionStatusStrategy editableStrategy(BindableEntity bindableEntity) {
        if (bindableEntity.equals(BindableEntity.REQUIREMENT_VERSION)) {
            return requirementBoundEditionStatusStrategy;
        } else {
            return defaultEditionStatusStrategy;
        }
    }

    @Override
    public void initCustomFieldValues(
            BoundEntity entity, Map<Long, RawValue> initialCustomFieldValues) {
        if (initialCustomFieldValues.isEmpty()) {
            return;
        }

        List<CustomFieldValue> persistentValues = findAllCustomFieldValues(entity);

        for (CustomFieldValue value : persistentValues) {
            Long customFieldId = value.getCufId();
            if (initialCustomFieldValues.containsKey(customFieldId)) {
                RawValue newValue = initialCustomFieldValues.get(customFieldId);
                newValue.setValueFor(value);
            }
        }
    }

    @Override
    public <T extends BoundEntity> void initBatchCustomFieldValues(
            Map<T, Map<Long, RawValue>> customFieldsByEntity) {
        if (customFieldsByEntity.isEmpty()) {
            return;
        }

        Map<Long, List<CustomFieldValue>> customFieldValuesByEntityId =
                getCufValuesMapByBoundEntity(new ArrayList<>(customFieldsByEntity.keySet()));

        if (customFieldValuesByEntityId.isEmpty()) {
            return;
        }

        for (var entry : customFieldsByEntity.entrySet()) {
            Long entityId = entry.getKey().getBoundEntityId();

            Map<Long, RawValue> initialCustomFieldValues = entry.getValue();
            List<CustomFieldValue> customFieldValues = customFieldValuesByEntityId.get(entityId);

            if (initialCustomFieldValues == null || customFieldValues == null) {
                continue;
            }

            for (CustomFieldValue customFieldValue : customFieldValues) {
                Long customFieldId = customFieldValue.getCufId();
                RawValue newValue = initialCustomFieldValues.get(customFieldId);

                if (newValue != null) {
                    newValue.setValueFor(customFieldValue);
                }
            }
        }
    }
}
