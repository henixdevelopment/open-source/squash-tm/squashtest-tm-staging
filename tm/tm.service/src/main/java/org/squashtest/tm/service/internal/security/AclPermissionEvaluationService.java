/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.security;

import static org.squashtest.tm.jooq.domain.Tables.ACL_CLASS;
import static org.squashtest.tm.jooq.domain.Tables.ACL_GROUP_PERMISSION;
import static org.squashtest.tm.jooq.domain.Tables.ACL_RESPONSIBILITY_SCOPE_ENTRY;
import static org.squashtest.tm.jooq.domain.Tables.CORE_TEAM_MEMBER;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import org.jooq.DSLContext;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.acls.domain.PermissionFactory;
import org.springframework.security.acls.model.Permission;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.CustomPermission;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.service.internal.dto.AccessPermission;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.UserContextService;

/**
 * TODO this service can be queried many times by a controller outside of a tx, which means lots of
 * shord lived tx and measurable performance effects. We should add some sort of cache over this
 * service which would short-circuit the tx.
 */
@Service("squashtest.core.security.PermissionEvaluationService")
@Transactional(readOnly = true)
public class AclPermissionEvaluationService implements PermissionEvaluationService {
    // TODO this should be externalized and may be based on the hack from hasMoreThanRead
    private static final String[] RIGHTS = {
        "READ",
        "WRITE",
        "CREATE",
        "DELETE",
        "ADMINISTRATION",
        "MANAGE_PROJECT",
        "EXPORT",
        "EXECUTE",
        "LINK",
        "IMPORT",
        "ATTACH",
        "EXTENDED_DELETE",
        "READ_UNASSIGNED"
    };

    private static final String ROLE_ADMIN = "ROLE_ADMIN";

    @Inject private UserContextService userContextService;

    @Inject @Lazy private AffirmativeBasedCompositePermissionEvaluator permissionEvaluator;

    @Inject private PermissionFactory permissionFactory;

    @Inject private DSLContext dslContext;

    /*
     * Not exposed so that interface remains spring-sec agnostic.
     */
    private boolean hasRoleOrPermissionOnObject(String role, Permission permission, Object object) {
        if (userContextService.hasRole(role)) {
            return true;
        }

        return permissionEvaluator.hasPermission(
                userContextService.getAuthentication(), object, permission);
    }

    @Override
    public boolean hasRoleOrPermissionOnObject(String role, String permissionName, Object object) {
        return hasRoleOrPermissionOnObject(
                role, permissionFactory.buildFromName(permissionName), object);
    }

    /**
     * @see PermissionEvaluationService#hasPermissionOnObject(String, Object)
     */
    @Override
    public boolean hasPermissionOnObject(String permission, Object entity) {
        return permissionEvaluator.hasPermission(
                userContextService.getAuthentication(),
                entity,
                permissionFactory.buildFromName(permission));
    }

    @Override
    public void checkAtLeastOneProjectManagementPermissionOrAdmin() {
        if (!userContextService.hasRole(ROLE_ADMIN)) {
            List<Long> partyIds = fetchPartyIdsFromLogin(userContextService.getUsername());
            checkAtLeastOneProjectManagementPermissionOrAdmin(partyIds);
        }
    }

    @Override
    public void checkAtLeastOneProjectImportPermissionOrAdmin() {
        if (!userContextService.hasRole(ROLE_ADMIN)) {
            List<Long> partyIds = fetchPartyIdsFromLogin(userContextService.getUsername());
            checkHasAtLeastOneManagementPermission(partyIds, Permissions.IMPORT);
        }
    }

    @Override
    public void checkAtLeastOneProjectManagementPermissionOrAdmin(List<Long> partyIds) {
        if (!userContextService.hasRole(ROLE_ADMIN)) {
            checkHasAtLeastOneManagementPermission(partyIds, Permissions.MANAGE_PROJECT);
        }
    }

    @Override
    public void checkAtLeastOneMilestoneManagementPermissionOrAdmin() {
        if (!userContextService.hasRole(ROLE_ADMIN)) {
            List<Long> partyIds = fetchPartyIdsFromLogin(userContextService.getUsername());
            checkAtLeastOneMilestoneManagementPermissionOrAdmin(partyIds);
        }
    }

    @Override
    public void checkAtLeastOneMilestoneManagementPermissionOrAdmin(List<Long> partyIds) {
        if (!userContextService.hasRole(ROLE_ADMIN)) {
            checkHasAtLeastOneManagementPermission(partyIds, Permissions.MANAGE_MILESTONE);
        }
    }

    @Override
    public void checkAtLeastOneClearanceManagementPermissionOrAdmin() {
        if (!userContextService.hasRole(ROLE_ADMIN)) {
            List<Long> partyIds = fetchPartyIdsFromLogin(userContextService.getUsername());
            checkAtLeastOneClearanceManagementPermissionOrAdmin(partyIds);
        }
    }

    @Override
    public void checkAtLeastOneClearanceManagementPermissionOrAdmin(List<Long> partyIds) {
        if (!userContextService.hasRole(ROLE_ADMIN)) {
            checkHasAtLeastOneManagementPermission(partyIds, Permissions.MANAGE_PROJECT_CLEARANCE);
        }
    }

    @Override
    public Set<AccessPermission> findAccessPermissions(List<Long> partyIds) {
        Set<Integer> accessPermissionMasks =
                Arrays.stream(AccessPermission.values())
                        .map(accessPermission -> accessPermission.getPermissionWithMask().getMask())
                        .collect(Collectors.toUnmodifiableSet());

        final Map<Long, List<Integer>> result =
                dslContext
                        .select(ACL_GROUP_PERMISSION.ACL_GROUP_ID, ACL_GROUP_PERMISSION.PERMISSION_MASK)
                        .from(ACL_GROUP_PERMISSION)
                        .join(ACL_RESPONSIBILITY_SCOPE_ENTRY)
                        .on(ACL_RESPONSIBILITY_SCOPE_ENTRY.ACL_GROUP_ID.eq(ACL_GROUP_PERMISSION.ACL_GROUP_ID))
                        .where(ACL_RESPONSIBILITY_SCOPE_ENTRY.PARTY_ID.in(partyIds))
                        .and(ACL_GROUP_PERMISSION.PERMISSION_MASK.in(accessPermissionMasks))
                        .groupBy(ACL_GROUP_PERMISSION.ACL_GROUP_ID, ACL_GROUP_PERMISSION.PERMISSION_MASK)
                        .fetchGroups(ACL_GROUP_PERMISSION.ACL_GROUP_ID, ACL_GROUP_PERMISSION.PERMISSION_MASK);

        if (result.isEmpty()) {
            return Collections.emptySet();
        }

        final boolean isOnlyAutomationProgrammer =
                result.values().stream()
                        .allMatch(
                                list ->
                                        list.contains(Permissions.WRITE_AS_AUTOMATION.getMask())
                                                && !list.contains(Permissions.WRITE_AS_FUNCTIONAL.getMask()));

        // special case of the user which is only an automation programmer on the instance
        if (isOnlyAutomationProgrammer) {
            return Collections.singleton(AccessPermission.WRITE_AS_AUTOMATION);
        }

        return result.values().stream()
                .flatMap(List::stream)
                .map(AccessPermission::findByMask)
                .collect(Collectors.toSet());
    }

    @Override
    public boolean hasRoleOrPermissionOnObject(
            String role, String permissionName, Long entityId, String entityClassName) {

        if (userContextService.hasRole(role)) {
            return true;
        }

        return hasPermissionOnObject(permissionName, entityId, entityClassName);
    }

    @Override
    public boolean hasRoleOrPermissionOnObject(
            String[] roles, String permissionName, Long entityId, String entityClassName) {
        for (String role : roles) {
            if (userContextService.hasRole(role)) {
                return true;
            }
        }

        return hasPermissionOnObject(permissionName, entityId, entityClassName);
    }

    @Override
    public boolean canRead(Object object) {
        return hasRoleOrPermissionOnObject(ROLE_ADMIN, "READ", object);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public boolean hasRole(String role) {
        return userContextService.hasRole(role);
    }

    @Override
    public boolean hasMoreThanRead(Object object) {
        boolean hasMore = false;
        if (userContextService.hasRole(ROLE_ADMIN)) {
            hasMore = true;
        } else {
            Authentication authentication = userContextService.getAuthentication();
            Field[] fields = CustomPermission.class.getFields();
            // TODO below is a hacky enum of all rights, should be externalized.
            hasMore = findPermission(fields, object, authentication);
        }
        return hasMore;
    }

    boolean findPermission(Field[] fields, Object object, Authentication authentication) {
        for (Field field : fields) {
            try {
                if (!"READ".equals(field.getName())
                        && permissionEvaluator.hasPermission(authentication, object, field.getName())) {
                    return true;
                }
            } catch (IllegalArgumentException iaexecption) {
                List<String> knownMessages =
                        Arrays.asList(
                                "Unknown permission 'RESERVED_ON'",
                                "Unknown permission 'RESERVED_OFF'",
                                "Unknown permission 'THIRTY_TWO_RESERVED_OFF'");
                if (!knownMessages.contains(iaexecption.getMessage())) {
                    throw iaexecption;
                }
            }
        }
        return false;
    }

    @Override
    public boolean hasPermissionOnObject(
            String permissionName, Long entityId, String entityClassName) {
        Authentication authentication = userContextService.getAuthentication();
        Permission permission = permissionFactory.buildFromName(permissionName);

        return permissionEvaluator.hasPermission(authentication, entityId, entityClassName, permission);
    }

    @Override
    public Map<String, Boolean> hasRoleOrPermissionsOnObject(
            String role, String[] permissions, Object entity) {
        boolean hasRole = this.hasRole(role);
        Map<String, Boolean> permByName = new HashMap<>(permissions.length);

        for (String perm : permissions) {
            permByName.put(perm, hasRole || this.hasPermissionOnObject(perm, entity));
        }

        return permByName;
    }

    @Override
    public Collection<String> permissionsOn(@NotNull String className, long id) {
        List<String> perms = new ArrayList<>();

        if (this.hasRole(ROLE_ADMIN)) {
            return Arrays.asList(RIGHTS);
        }

        for (String right : RIGHTS) {
            if (this.hasPermissionOnObject(right, id, className)) {
                perms.add(right);
            }
        }

        return perms;
    }

    private void checkHasAtLeastOneManagementPermission(List<Long> partyIds, Permissions permission) {
        boolean hasManagementPermission =
                dslContext.fetchExists(
                        dslContext
                                .select(ACL_RESPONSIBILITY_SCOPE_ENTRY.ID)
                                .from(ACL_RESPONSIBILITY_SCOPE_ENTRY)
                                .join(ACL_GROUP_PERMISSION)
                                .on(
                                        ACL_RESPONSIBILITY_SCOPE_ENTRY.ACL_GROUP_ID.eq(
                                                ACL_GROUP_PERMISSION.ACL_GROUP_ID))
                                .join(ACL_CLASS)
                                .on(ACL_GROUP_PERMISSION.CLASS_ID.eq(ACL_CLASS.ID))
                                .where(ACL_RESPONSIBILITY_SCOPE_ENTRY.PARTY_ID.in(partyIds))
                                .and(ACL_GROUP_PERMISSION.PERMISSION_MASK.eq(permission.getMask()))
                                .and(ACL_CLASS.CLASSNAME.eq(Project.class.getName())));

        if (!hasManagementPermission) {
            throw new AccessDeniedException("Only users with management rights can perform this action");
        }
    }

    private List<Long> fetchPartyIdsFromLogin(String login) {
        return dslContext
                .select(CORE_USER.PARTY_ID)
                .from(CORE_USER)
                .where(CORE_USER.LOGIN.eq(login))
                .union(
                        dslContext
                                .select(CORE_TEAM_MEMBER.TEAM_ID)
                                .from(CORE_TEAM_MEMBER)
                                .join(CORE_USER)
                                .on(CORE_TEAM_MEMBER.USER_ID.eq(CORE_USER.PARTY_ID))
                                .where(CORE_USER.LOGIN.eq(login)))
                .fetchInto(Long.class);
    }
}
