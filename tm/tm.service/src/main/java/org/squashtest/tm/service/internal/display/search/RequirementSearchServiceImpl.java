/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search;

import static org.squashtest.tm.domain.query.QueryColumnPrototypeReference.REQUIREMENT_ID;
import static org.squashtest.tm.domain.query.QueryColumnPrototypeReference.REQUIREMENT_VERSION_ID;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;

import com.google.common.collect.ImmutableList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.jooq.domain.tables.records.ProjectRecord;
import org.squashtest.tm.service.display.search.RequirementSearchService;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.search.filter.FilterHandlers;
import org.squashtest.tm.service.internal.display.search.filter.FilterValueHandlers;
import org.squashtest.tm.service.internal.repository.ColumnPrototypeDao;
import org.squashtest.tm.service.project.ProjectFinder;
import org.squashtest.tm.service.query.QueryProcessingService;

@Service
@Transactional(readOnly = true)
public class RequirementSearchServiceImpl extends AbstractSearchService
        implements RequirementSearchService {

    private final HighLevelRequirementScopeExtender highLevelRequirementPerimeterExtender;

    public RequirementSearchServiceImpl(
            QueryProcessingService queryService,
            ColumnPrototypeDao columnPrototypeDao,
            EntityManager entityManager,
            ProjectFinder projectFinder,
            FilterHandlers filterHandlers,
            FilterValueHandlers gridFilterValueHandlers,
            DSLContext dslContext,
            HighLevelRequirementScopeExtender highLevelRequirementPerimeterExtender) {
        super(
                queryService,
                columnPrototypeDao,
                entityManager,
                projectFinder,
                filterHandlers,
                gridFilterValueHandlers,
                dslContext);
        this.highLevelRequirementPerimeterExtender = highLevelRequirementPerimeterExtender;
    }

    @Override
    protected List<String> getIdentifierColumnName() {
        return ImmutableList.of(REQUIREMENT_VERSION_ID, REQUIREMENT_ID);
    }

    @Override
    protected String getDefaultSortColumnName() {
        return REQUIREMENT_VERSION_ID;
    }

    protected EntityType getLibraryEntityType() {
        return EntityType.REQUIREMENT_LIBRARY;
    }

    protected TableField<ProjectRecord, Long> getLibraryIdField() {
        return PROJECT.RL_ID;
    }

    @Override
    protected List<EntityReference> extendScope(
            List<EntityReference> initialScope, boolean extendedScope) {
        return extendedScope
                ? highLevelRequirementPerimeterExtender.extendScope(initialScope)
                : initialScope;
    }

    @Override
    public Map<EntityType, List<Long>> getEntityReferenceFromScope(GridRequest request) {
        return computeScope(request).stream()
                .collect(
                        Collectors.groupingBy(
                                EntityReference::getType,
                                Collectors.mapping(EntityReference::getId, Collectors.toList())));
    }
}
