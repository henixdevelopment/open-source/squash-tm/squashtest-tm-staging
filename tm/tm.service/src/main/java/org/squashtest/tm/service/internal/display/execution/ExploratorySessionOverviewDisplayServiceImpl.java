/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.execution;

import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_SESSION_OVERVIEW;

import java.util.List;
import javax.persistence.EntityManager;
import org.jooq.DSLContext;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.campaign.ExploratorySessionOverview;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.exception.UnknownEntityException;
import org.squashtest.tm.service.campaign.IterationTestPlanManagerService;
import org.squashtest.tm.service.campaign.SprintTestPlanItemManagerService;
import org.squashtest.tm.service.display.execution.ExploratorySessionOverviewDisplayService;
import org.squashtest.tm.service.internal.bugtracker.knownissues.local.ExploratorySessionOverviewKnownIssueFinder;
import org.squashtest.tm.service.internal.display.dto.UserView;
import org.squashtest.tm.service.internal.display.dto.execution.ExploratorySessionOverviewView;
import org.squashtest.tm.service.internal.display.dto.execution.SessionNoteDto;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.campaign.ExploratorySessionExecutionGrid;
import org.squashtest.tm.service.internal.display.grid.campaign.SprintTestPlanExploratorySessionExecutionGrid;
import org.squashtest.tm.service.internal.display.grid.campaign.TestPlanGridHelpers;
import org.squashtest.tm.service.internal.library.EntityPathHeaderService;
import org.squashtest.tm.service.internal.repository.display.AttachmentDisplayDao;
import org.squashtest.tm.service.internal.repository.display.ExploratorySessionOverviewDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.SessionNoteDisplayDao;
import org.squashtest.tm.service.internal.repository.display.SprintDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TestPlanExploratorySessionOverviewDisplayDao;

@Service
@Transactional(readOnly = true)
public class ExploratorySessionOverviewDisplayServiceImpl
        implements ExploratorySessionOverviewDisplayService {

    private final ExploratorySessionOverviewDisplayDao exploratorySessionOverviewDisplayDao;
    private final TestPlanExploratorySessionOverviewDisplayDao
            testPlanExploratorySessionOverviewDisplayDao;
    private final ExploratorySessionOverviewKnownIssueFinder
            exploratorySessionOverviewKnownIssueFinder;
    private final EntityPathHeaderService entityPathHeaderService;
    private final EntityManager entityManager;
    private final IterationTestPlanManagerService iterationTestPlanManagerService;
    private final AttachmentDisplayDao attachmentDisplayDao;
    private final SessionNoteDisplayDao sessionNoteDisplayDao;
    private final MilestoneDisplayDao milestoneDisplayDao;
    private final DSLContext dslContext;
    private final MessageSource messageSource;
    private final SprintTestPlanItemManagerService sprintTestPlanItemManagerService;
    private final SprintDisplayDao sprintDisplayDao;

    public ExploratorySessionOverviewDisplayServiceImpl(
            ExploratorySessionOverviewDisplayDao exploratorySessionOverviewDisplayDao,
            TestPlanExploratorySessionOverviewDisplayDao testPlanExploratorySessionOverviewDisplayDao,
            ExploratorySessionOverviewKnownIssueFinder exploratorySessionOverviewKnownIssueFinder,
            EntityPathHeaderService entityPathHeaderService,
            EntityManager entityManager,
            IterationTestPlanManagerService iterationTestPlanManagerService,
            AttachmentDisplayDao attachmentDisplayDao,
            SessionNoteDisplayDao sessionNoteDisplayDao,
            MilestoneDisplayDao milestoneDisplayDao,
            DSLContext dslContext,
            MessageSource messageSource,
            SprintTestPlanItemManagerService sprintTestPlanItemManagerService,
            SprintDisplayDao sprintDisplayDao) {
        this.exploratorySessionOverviewDisplayDao = exploratorySessionOverviewDisplayDao;
        this.testPlanExploratorySessionOverviewDisplayDao =
                testPlanExploratorySessionOverviewDisplayDao;
        this.exploratorySessionOverviewKnownIssueFinder = exploratorySessionOverviewKnownIssueFinder;
        this.entityPathHeaderService = entityPathHeaderService;
        this.entityManager = entityManager;
        this.iterationTestPlanManagerService = iterationTestPlanManagerService;
        this.attachmentDisplayDao = attachmentDisplayDao;
        this.sessionNoteDisplayDao = sessionNoteDisplayDao;
        this.milestoneDisplayDao = milestoneDisplayDao;
        this.dslContext = dslContext;
        this.messageSource = messageSource;
        this.sprintTestPlanItemManagerService = sprintTestPlanItemManagerService;
        this.sprintDisplayDao = sprintDisplayDao;
    }

    @Override
    public ExploratorySessionOverviewView findById(long overviewId) {
        final Long iterationId =
                exploratorySessionOverviewDisplayDao.findIterationIdByOverviewId(overviewId);

        if (iterationId != null) {
            return findByIdWithIteration(overviewId);
        } else {
            return findByIdWithTestPlan(overviewId);
        }
    }

    private ExploratorySessionOverviewView findByIdWithIteration(long overviewId) {
        ExploratorySessionOverviewView view = exploratorySessionOverviewDisplayDao.findById(overviewId);
        view.setPath(entityPathHeaderService.buildExploratorySessionOverviewPathHeader(overviewId));
        List<UserView> assignableUsers =
                UserView.fromEntities(
                        iterationTestPlanManagerService.findAssignableUsersForTestPlan(view.getIterationId()));
        view.setAssignableUsers(assignableUsers);
        view.setInferredSessionReviewStatus(
                exploratorySessionOverviewDisplayDao.inferReviewStatus(overviewId));
        view.setNbIssues(exploratorySessionOverviewKnownIssueFinder.countKnownIssues(overviewId));
        view.setNbExecutions(getNbExecutions(overviewId));
        view.setNbNotes(getAllSessionNotes(overviewId).size());
        view.setAttachmentList(attachmentDisplayDao.findAttachmentListById(view.getAttachmentListId()));
        view.setMilestones(milestoneDisplayDao.getMilestonesBySessionOverviewId(view.getId()));
        return view;
    }

    private ExploratorySessionOverviewView findByIdWithTestPlan(long overviewId) {
        final ExploratorySessionOverviewView view =
                testPlanExploratorySessionOverviewDisplayDao.findById(overviewId);
        view.setPath(entityPathHeaderService.buildExploratorySessionOverviewPathHeader(overviewId));
        final List<UserView> assignableUsers =
                UserView.fromEntities(
                        sprintTestPlanItemManagerService.findAssignableUsersByTestPlanItemId(
                                view.getTestPlanItemId()));
        view.setAssignableUsers(assignableUsers);
        view.setInferredSessionReviewStatus(
                testPlanExploratorySessionOverviewDisplayDao.inferReviewStatus(overviewId));
        view.setNbIssues(exploratorySessionOverviewKnownIssueFinder.countKnownIssues(overviewId));
        view.setNbExecutions(getNbExecutions(overviewId));
        view.setAttachmentList(attachmentDisplayDao.findAttachmentListById(view.getAttachmentListId()));
        view.setSprintStatus(sprintDisplayDao.getSprintStatusBySessionOverviewId(overviewId));
        return view;
    }

    private Integer getNbExecutions(long overviewId) {
        Integer executionCount = exploratorySessionOverviewDisplayDao.countExecutions(overviewId);
        return executionCount == null ? 0 : executionCount;
    }

    @Override
    public GridResponse getExecutionGrid(Long overviewId, GridRequest gridRequest) {
        final Long iterationTestPlanItemId =
                dslContext
                        .select(EXPLORATORY_SESSION_OVERVIEW.ITEM_TEST_PLAN_ID)
                        .from(EXPLORATORY_SESSION_OVERVIEW)
                        .where(EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.eq(overviewId))
                        .fetchOneInto(Long.class);

        GridResponse gridResponse;

        if (iterationTestPlanItemId != null) {
            final var grid = new ExploratorySessionExecutionGrid(overviewId);
            gridResponse = grid.getRows(gridRequest, dslContext);
        } else {
            final var grid = new SprintTestPlanExploratorySessionExecutionGrid(overviewId);
            gridResponse = grid.getRows(gridRequest, dslContext);
        }

        for (DataRow row : gridResponse.getDataRows()) {
            TestPlanGridHelpers.formatDeactivatedUserNameInRowData(row, messageSource);
        }

        return gridResponse;
    }

    @Override
    public Project findProjectBySessionOverviewId(Long overviewId) {
        Long projectId =
                exploratorySessionOverviewDisplayDao.findProjectIdBySessionOverviewId(overviewId);
        if (projectId == null) {
            throw new UnknownEntityException(overviewId, ExploratorySessionOverview.class);
        }

        return entityManager.find(Project.class, projectId);
    }

    @Override
    public List<SessionNoteDto> getAllSessionNotes(Long overviewId) {
        return sessionNoteDisplayDao.fetchSessionNotesByOverviewId(overviewId);
    }

    @Override
    public List<UserView> getUnassignedUsers(Long overviewId) {
        final List<Long> alreadyAssigned =
                exploratorySessionOverviewDisplayDao.findAlreadyAssignedUserIds(overviewId);
        final Long iterationId =
                exploratorySessionOverviewDisplayDao.findIterationIdByOverviewId(overviewId);
        final List<User> assignableUsers =
                iterationId != null
                        ? iterationTestPlanManagerService.findAssignableUsersForTestPlan(iterationId)
                        : sprintTestPlanItemManagerService.findAssignableUsersByExploratorySessionOverviewId(
                                overviewId);
        return UserView.fromEntities(assignableUsers).stream()
                .filter(user -> !alreadyAssigned.contains(user.getId()))
                .toList();
    }
}
