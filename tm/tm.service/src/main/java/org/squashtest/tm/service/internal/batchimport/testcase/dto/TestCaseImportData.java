/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.testcase.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequest;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.TestCaseTarget;

public class TestCaseImportData {

    private TestCase testCase;

    private final TestCaseTarget target;

    private Integer position;

    private Map<Long, RawValue> customFields = new HashMap<>();

    private List<Long> milestoneIds = new ArrayList<>();

    private AutomationRequest automationRequest;

    public TestCaseImportData(TestCase testCase, TestCaseTarget target) {
        this.testCase = testCase;
        this.target = target;
        this.position = target.getOrder();
    }

    public void setCustomFields(Map<Long, RawValue> customFields) {
        this.customFields = customFields;
    }

    public void setMilestoneIds(List<Long> milestoneIds) {
        this.milestoneIds = milestoneIds;
    }

    public List<Long> getMilestoneIds() {
        return milestoneIds;
    }

    public Map<Long, RawValue> getCustomFields() {
        return customFields;
    }

    public Integer getPosition() {
        return position;
    }

    public TestCase getTestCase() {
        return testCase;
    }

    public void setTestCase(TestCase testCase) {
        this.testCase = testCase;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public void setAutomationRequest(AutomationRequest automationRequest) {
        this.automationRequest = automationRequest;
    }

    public AutomationRequest getAutomationRequest() {
        return automationRequest;
    }

    public TestCaseTarget getTarget() {
        return target;
    }
}
