/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchexport.models;

import static org.squashtest.tm.domain.requirement.RequirementNature.HIGH_LEVEL;
import static org.squashtest.tm.domain.requirement.RequirementNature.STANDARD;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import org.squashtest.tm.domain.requirement.RequirementCriticality;
import org.squashtest.tm.domain.requirement.RequirementNature;
import org.squashtest.tm.domain.requirement.RequirementStatus;
import org.squashtest.tm.service.internal.batchexport.models.ExportModel.CustomField;
import org.squashtest.tm.service.internal.batchexport.models.RequirementExportModel.RequirementPathSortable;

public final class RequirementModelFromJooq implements RequirementPathSortable {

    private Long id;
    private Long requirementId;
    private Long projectId;
    private String projectName;
    private String path;
    private Integer requirementVersionNumber;
    private String reference;
    private String name;
    private Boolean isHighLevel;
    private RequirementCriticality criticality;
    private RequirementStatus status;
    private String description;
    private Long coverages;
    private Long versionsCount;
    private Long attachments;
    private Date createdOn;
    private String createdBy;
    private Date lastModifiedOn;
    private String lastModifiedBy;
    private String milestoneLabels;
    private List<CustomField> cufs = new LinkedList<>();
    private Long milestones;
    private Long childOfRequirement;
    private Long linkedStandardRequirement;
    private Boolean hasParent;
    private Boolean hasLinkType;
    private Boolean requirementBoundToHighLevelRequirement;
    private String milestoneEndDate;
    private String milestoneStatus;
    private String category;

    public RequirementModelFromJooq() {
        super();
    }

    public Long getId() {
        return id;
    }

    public Long getRequirementId() {
        return requirementId;
    }

    public Long getProjectId() {
        return projectId;
    }

    @Override
    public String getProjectName() {
        return projectName;
    }

    @Override
    public String getPath() {
        return path;
    }

    public String getReference() {
        return reference;
    }

    @Override
    public int getRequirementVersionNumber() {
        return requirementVersionNumber;
    }

    public String getName() {
        return name;
    }

    public boolean isHighLevel() {
        return isHighLevel;
    }

    public RequirementNature getNature() {
        return Boolean.TRUE.equals(isHighLevel) ? HIGH_LEVEL : STANDARD;
    }

    public RequirementCriticality getCriticality() {
        return criticality;
    }

    public RequirementStatus getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }

    public Long getCoverages() {
        return coverages;
    }

    public Long getAttachments() {
        return attachments;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Date getLastModifiedOn() {
        return lastModifiedOn;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public String getMilestoneLabels() {
        return milestoneLabels;
    }

    public List<CustomField> getCufs() {
        return cufs;
    }

    public void setCufs(List<CustomField> cufs) {
        this.cufs = cufs;
    }

    public Long getMilestones() {
        return milestones;
    }

    public Long getVersionsCount() {
        return versionsCount;
    }

    public String getMilestoneEndDate() {
        return milestoneEndDate;
    }

    public String getMilestoneStatus() {
        return milestoneStatus;
    }

    public Boolean getHasParent() {
        return hasParent;
    }

    public Boolean getHasLinkType() {
        return hasLinkType;
    }

    public Boolean getRequirementBoundToHighLevelRequirement() {
        return requirementBoundToHighLevelRequirement;
    }

    public Long getChildOfRequirement() {
        return childOfRequirement;
    }

    public Long getLinkedStandardRequirement() {
        return linkedStandardRequirement;
    }

    public String getCategory() {
        return category;
    }
}
