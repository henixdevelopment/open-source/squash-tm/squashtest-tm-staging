/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.squashtest.tm.domain.campaign.Campaign;

public interface CampaignDao extends EntityDao<Campaign> {
    /**
     * For a given collection of campaign ids, will return only those that belong to a milestone
     * (given its id). If milestoneId is null, the initial list will be returned.
     *
     * @param campaignIds
     * @param milestoneId
     * @return
     */
    List<Long> filterByMilestone(Collection<Long> campaignIds, Long milestoneId);

    List<Long> findAllIdsByMilestone(Long milestoneId);

    /** Returns how many iterations this campaign have */
    int countIterations(long campaignId);

    List<Long> findCampaignIdsHavingMultipleMilestones(List<Long> nodeIds);

    List<Long> findNonBoundCampaign(Collection<Long> nodeIds, Long milestoneId);

    Map<Long, String> findAllCampaignIdsAndComputePathAsNameByLibraries(Collection<Long> readLibIds);

    Map<Long, String> findAllCampaignIdsAndComputePathAsNameByNodeIds(Collection<Long> readNodeIds);

    Map<Long, String> findAllCampaignIdsAndNameByLibraryIds(List<Long> asList);

    Map<Long, String> findAllCampaignIdsAndNameByNodeIds(List<Long> nodeIds);
}
