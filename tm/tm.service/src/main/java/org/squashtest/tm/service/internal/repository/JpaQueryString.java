/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository;

public final class JpaQueryString {
    // ---------- Get entity ----------

    public static final String FIND_AUTOMATED_SUITE_BY_ID_WITH_EXTENDERS = "select a from AutomatedSuite a left join fetch a.executionExtenders where a.id = :id";
    public static final String FIND_PROJECT_CUFS_BY_PROJECT_IDS_AND_ENTITIES = """
        select binding.boundProject.name, binding.boundEntity, cf
        from CustomFieldBinding binding
        join binding.customField cf
        where binding.boundProject.id in :projectIds and binding.boundEntity in :entities""";
    public static final String FIND_CUF_VALUES_BY_ENTITY_AND_IDS = "SELECT c FROM CustomFieldValue c WHERE c.binding.boundEntity = :entity AND c.boundEntityId in :entityIds";
    public static final String FIND_CAMPAIGN_LIBRARY_CONTENT_BY_ID = "select cl from CampaignLibrary cl left join fetch cl.rootContent where cl.id = :id";
    public static final String FIND_CAMPAIGN_FOLDER_CONTENT_BY_ID = "select cf from CampaignFolder cf left join fetch cf.content where cf.id = :id";
    public static final String FIND_CAMPAIGNS_BY_IDS = "select c from Campaign c where c.id in :ids";
    public static final String FIND_DATASETS_BY_TESTCASE_IDS = "select d from Dataset d where d.testCase.id in :ids";
    public static final String FIND_DATASETS_NAMES_BY_TESTCASE_IDS = "select tc.id, d.name from Dataset d join d.testCase tc where tc.id in :ids";
    public static final String FIND_EXECUTION_BY_ID = "SELECT e FROM Execution e WHERE e.id = :id";
    public static final String FIND_EXECUTION_STEPS_BY_EXECUTION_IDS = "select step from ExecutionStep step where step.execution.id in (:ids)";
    public static final String FIND_ITERATION_BY_ID = "select i from Iteration i where i.id = :id";
    public static final String FIND_PARAMETERS_BY_TESTCASE_IDS = "select parameter from Parameter parameter where parameter.testCase.id in (:ids)";
    public static final String FIND_PROJECT_BY_ID = "SELECT p FROM GenericProject p WHERE p.id = :projectId";
    public static final String FIND_REQUIREMENT_BY_ID = "SELECT r FROM Requirement r WHERE r.id = :id";
    public static final String FIND_REQUIREMENT_FOLDER_BY_ID = "SELECT rf FROM RequirementFolder rf  WHERE rf.id = :id";
    public static final String FIND_REQUIREMENT_SYNC_EXTENDERS_BY_KEYS_AND_PROJECT_ID = "select rse from RequirementSyncExtender rse where rse.remoteReqId in :remoteIds and rse.requirement.project.id = :projectId";
    public static final String FIND_REQUIREMENT_SYNC_EXTENDERS_BY_KEYS_AND_SYNC_ID = "select rse from RequirementSyncExtender rse where rse.remoteReqId in :remoteIds and rse.remoteSynchronisation.id = :remoteSynchronisationId";
    public static final String FIND_TESTCASES_NAME_BY_IDS = "select tc.id, tc.name from TestCase tc where tc.id in :ids";
    public static final String FIND_SPRINT_GROUP_FOR_CONTENT_ADDITION = "select sg from SprintGroup sg left join fetch sg.content where sg.id = :id";
    public static final String FIND_REQUIREMENT_FOR_REQUIREMENT_ADDITION = """
                select req from Requirement req
                join fetch req.project project
                join fetch project.requirementCategories
                left join fetch req.syncExtender
                left join fetch req.children children
                left join fetch children.syncExtender
                where req.id in :ids""";
    public static final String FIND_REQUIREMENT_LIBRARY_FOR_NODE_ADDITION = """
                    select library from RequirementLibrary library
                    join fetch library.project project
                    join fetch project.requirementCategories
                    left join fetch library.rootContent content
                    left join fetch content.syncExtender
                    left join fetch content.requirementFolderSyncExtender
                    where library.id = :id""";
    public static final String FIND_REQUIREMENT_FOR_VERSION_ADDITION = "select req from Requirement req join fetch req.resource where req.id in :ids";
    public static final String FIND_REQ_BY_REMOTE_SYNC_AND_REMOTE_KEY = """
        select reqVersion from RequirementVersion reqVersion
        inner join reqVersion.requirement.syncExtender extender
        where extender.remoteSynchronisation.id = :remoteSyncId
        and extender.remoteReqId = :issueKey""";
    // ---------- Get distinct entity ----------
    public static final String FIND_DISTINCT_CAMPAIGN_FOLDERS_CONTENT_BY_ID = "select distinct cf from CampaignFolder cf left join fetch cf.content where cf.id in :id";
    public static final String FIND_DISTINCT_CAMPAIGN_FOLDERS_ATTACHMENT_CONTENT_BY_ID = "select distinct cf from CampaignFolder cf left join fetch cf.attachmentList al left join fetch al.attachments a left join fetch a.content c where cf.id in :ids";
    public static final String FIND_DISTINCT_DATASETS_BY_IDS = "SELECT DISTINCT ds FROM Dataset ds WHERE ds.id IN :ids";
    public static final String FIND_DISTINCT_ITERATIONS_BY_IDS = "SELECT DISTINCT it FROM Iteration it WHERE it.id IN :ids";
    public static final String FIND_DISTINCT_TESTSUITE_BY_IDS = "SELECT DISTINCT ts FROM TestSuite ts WHERE ts.id IN :ids";
    public static final String FIND_DISTINCT_ITPIS_BY_IDS = "SELECT DISTINCT itpi FROM IterationTestPlanItem itpi WHERE itpi.id IN :ids ORDER BY itpi.id";
    public static final String FIND_DISTINCT_SPRINT_GROUP_CONTENT_BY_ID = "select distinct sg from SprintGroup sg left join fetch sg.content where sg.id in :ids";
    public static final String FIND_DISTINCT_SPRINT_GROUP_ATTACHMENT_BY_ID = "select distinct sg from SprintGroup sg left join fetch sg.attachmentList al left join fetch al.attachments a left join fetch a.content c where sg.id in :ids";
    public static final String FIND_DISTINCT_SPRINT_ATTACHMENT_PROJECT_BY_IDS = " SELECT distinct s FROM Sprint s left join fetch s.project p left join fetch s.attachmentList al left join fetch al.attachments a left join fetch a.content c WHERE s.id IN (:ids)";
    public static final String FIND_DISTINCT_SPRINT_WITH_SRV_BY_IDS = "SELECT distinct s FROM Sprint s join fetch s.sprintReqVersions srv where s in :sprints";
    public static final String FIND_DISTINCT_TEST_PLAN_OF_SRV_ATTACHMENT_BY_IDS = "select distinct tp from Sprint s join s.sprintReqVersions srv join srv.testPlan tp left join fetch tp.testPlanItems tpi left join fetch tpi.exploratorySessionOverview eso left join fetch eso.attachmentList al left join fetch al.attachments a left join fetch a.content c where s in :sprints";
    public static final String FIND_DISTINCT_TEST_CASES_BY_IDS = "SELECT DISTINCT tc FROM TestCase tc WHERE tc.id IN :ids";
    public static final String FIND_DISTINCT_AUTOMATED_EXECUTION_EXTENDERS_BY_IDS = "select distinct ext from AutomatedExecutionExtender ext where ext.automatedSuite.id = :id order by ext.id asc";
    public static final String FIND_DISTINCT_SPRINTS_BY_IDS = "SELECT DISTINCT s FROM Sprint s WHERE s.id IN :ids";
    public static final String FIND_DISTINCT_TEST_PLAN_BY_IDS = "SELECT DISTINCT tp FROM TestPlan tp WHERE tp.id IN :ids";

    // ---------- Get child entity dto ----------
    public static final String FIND_CHILD_ENTITY_DTO_CAMPAIGNS_BY_IDS = "select i, c.id from Campaign c join c.iterations i where c.id in :ids";
    public static final String FIND_CHILD_ENTITY_DTO_CAMPAIGN_FOLDERS_BY_IDS = "select c, cf.id from CampaignFolder cf join cf.content c where cf.id in :ids";
    public static final String FIND_CHILD_ENTITY_DTO_SPRINT_GROUPS_BY_IDS = "select c, sg.id from SprintGroup sg join sg.content c where sg.id in :ids";


    // ---------- SELECT ----------
    public static final String FIND_PARAMETERS_BY_TESTCASE_IDS_ORDER_BY_PARAM_NAME = """
        select testCase.id, parameter, parameter.name
        from Parameter parameter
        join parameter.testCase testCase
        where testCase.id in :ids
        order by parameter.name""";
    public static final String FIND_PROJECT_ID_BY_ITERATION_ID = "select it.campaign.project.id from Iteration it where it.id = :id";
    public static final String FIND_TEST_PLAN_IDS_BY_ITERATION_ID = "select tp.id from Iteration it join it.testPlans tp where it.id = :id";
    public static final String FIND_STEPS_DETAILS_BY_TEST_CASE_IDS = """
        select tc.id,
        case when st.class = ActionTestStep then 'ACTION' else 'CALL' end as steptype,
        case when st.class = CallTestStep then st.calledTestCase.id else null end as calledTC,
        case when st.class=CallTestStep then st.delegateParameterValues else false end as delegates
        from TestCase tc join tc.steps st where tc.id in :ids order by index(st)""";
    public static final String FIND_REQ_IDS_BY_REMOTE_KEYS_AND_SYNC_IDS = """
        select sync.id, req.id, syncExt.remoteReqId
        from Requirement req
        inner join req.syncExtender syncExt
        inner join syncExt.remoteSynchronisation sync
        where syncExt.remoteReqId in :keys and sync.id in :synchronisationIds""";
    public static final String FIND_REQUIREMENT_CRITICALITY_BY_TESTCASE_IDS = """
        select tc.id, r.criticality
        from TestCase tc
        join tc.requirementVersionCoverages rvc
        join rvc.verifiedRequirementVersion r
        where tc.id in (:ids)""";
    public static final String FIND_REQUIREMENT_LIBRARY_CONTENT_NAMES = """
        select content.mainResource.name
        from RequirementLibrary library
        join library.rootContent content
        where library.id = :id""";
    public static final String IS_HIGH_LEVEL_REQUIREMENT = """
        select case when (req is not null) then true else false end
        from HighLevelRequirement req
        where req.id = :id
        """;

    // ---------- Get locked milestones query ----------

    public static final String REQ_VERSION_LOCKED_MILESTONES_QUERY = """
        select requirementMilestone.id
        from RequirementVersion rv
        inner join rv.attachmentList al
        inner join rv.milestones requirementMilestone
        where al.id = :attachmentListId
        and requirementMilestone.status in (:statuses)""";

    public static final String TEST_CASE_LOCKED_MILESTONES_QUERY = """
        select directTestCaseMilestone.id
        from TestCase tc
        inner join tc.attachmentList al
        inner join tc.milestones directTestCaseMilestone
        where al.id = :attachmentListId
        and directTestCaseMilestone.status in (:statuses)""";

    public static final String ACTION_TEST_STEP_LOCKED_MILESTONES_QUERY = """
        select directTestCaseMilestone.id
        from ActionTestStep step
        inner join step.attachmentList al
        inner join step.testCase tc
        inner join tc.milestones directTestCaseMilestone
        where al.id = :attachmentListId
        and directTestCaseMilestone.status in (:statuses)""";

    public static final String CAMPAIGN_LOCKED_MILESTONES_QUERY = """
        select campaignMilestone.id
        from Campaign c
        inner join c.attachmentList al
        inner join c.milestones campaignMilestone
        where al.id = :attachmentListId
        and campaignMilestone.status in (:statuses)""";

    public static final String ITERATION_LOCKED_MILESTONES_QUERY = """
        select campaignMilestone.id
        from Iteration it
        inner join it.attachmentList al
        inner join it.campaign c
        inner join c.milestones campaignMilestone
        where al.id = :attachmentListId
        and campaignMilestone.status in (:statuses)""";

    public static final String EXECUTION_LOCKED_MILESTONES_QUERY = """
        select campaignMilestone.id
        from Execution exec
        inner join exec.attachmentList al
        inner join exec.testPlan itpi
        inner join itpi.iteration it
        inner join it.campaign c
        inner join c.milestones campaignMilestone
        where al.id = :attachmentListId
        and campaignMilestone.status in (:statuses)""";

    public static final String EXECUTION_STEP_LOCKED_MILESTONES_QUERY = """
        select campaignMilestone.id
        from ExecutionStep step
        inner join step.attachmentList al
        inner join step.execution exec
        inner join exec.testPlan itpi
        inner join itpi.iteration it
        inner join it.campaign c
        inner join c.milestones campaignMilestone
        where al.id = :attachmentListId
        and campaignMilestone.status in (:statuses)""";

    private JpaQueryString() {}

}
