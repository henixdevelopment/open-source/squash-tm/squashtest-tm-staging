/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.testplanretriever;

import static java.util.Collections.emptyList;

import java.util.List;
import java.util.Map;
import org.squashtest.tm.domain.customfield.CustomFieldValue;

public class AllCustomFieldValuesForExec extends CustomFieldValuesForExec {

    private final Map<Long, List<CustomFieldValue>> iterationCfv;
    private final Map<Long, List<CustomFieldValue>> campaignCfv;
    private final Map<Long, List<CustomFieldValue>> testSuiteCfv;

    public AllCustomFieldValuesForExec(
            Map<Long, List<CustomFieldValue>> testCaseCfv,
            Map<Long, List<CustomFieldValue>> iterationCfv,
            Map<Long, List<CustomFieldValue>> campaignCfv,
            Map<Long, List<CustomFieldValue>> testSuiteCfv) {
        super(testCaseCfv);
        this.iterationCfv = iterationCfv;
        this.campaignCfv = campaignCfv;
        this.testSuiteCfv = testSuiteCfv;
    }

    public List<CustomFieldValue> getValueForIteration(Long testCaseId) {
        return this.iterationCfv.getOrDefault(testCaseId, emptyList());
    }

    public List<CustomFieldValue> getValueForCampaign(Long testCaseId) {
        return this.campaignCfv.getOrDefault(testCaseId, emptyList());
    }

    public List<CustomFieldValue> getValueForTestSuite(Long testCaseId) {
        return this.testSuiteCfv.getOrDefault(testCaseId, emptyList());
    }
}
