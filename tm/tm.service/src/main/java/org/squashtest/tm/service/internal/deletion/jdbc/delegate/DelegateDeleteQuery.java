/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion.jdbc.delegate;

import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.SQLDialect;
import org.jooq.SelectConditionStep;
import org.jooq.TableField;

public interface DelegateDeleteQuery {
    void delete(TableField<?, Long> linkedColumn);

    SelectConditionStep<Record1<Long>> selectEntityIds();

    static DelegateDeleteQuery get(
            TableField<?, Long> originalField, DSLContext dslContext, String operationId) {
        SQLDialect family = dslContext.configuration().dialect().family();
        return switch (family) {
            case MARIADB -> new JoinDelegateDeleteQuery(originalField, dslContext, operationId);
            case H2, POSTGRES ->
                    new SubRequestDelegateDeleteQuery(originalField, dslContext, operationId);
            default -> throw new IllegalArgumentException("Dialog not handled " + family);
        };
    }
}
