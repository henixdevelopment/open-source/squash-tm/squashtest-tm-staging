/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.project;

import java.util.Collection;
import java.util.List;
import org.jooq.CommonTableExpression;
import org.jooq.Record1;
import org.jooq.TableField;
import org.squashtest.tm.api.plugin.UsedInPlugin;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.jooq.domain.tables.records.ProjectRecord;
import org.squashtest.tm.service.internal.dto.UserDto;
import org.squashtest.tm.service.internal.dto.json.JsonProject;

/**
 * @author mpagnon
 */
public interface CustomProjectFinder {

    List<GenericProject> findAllICanManage();

    List<String> findProjectNamesByIds(List<Long> projectIds);

    /**
     * Get all the ids of projects user can read.
     *
     * @return The list of projects ids
     * @param userDto the current user dto
     */
    List<Long> findAllReadableIds(UserDto userDto);

    CommonTableExpression<Record1<Long>> findAllReadableIdsCte(UserDto userDto);

    boolean checkHasAtLeastOneReadableId(CommonTableExpression<Record1<Long>> projectIdsCte);

    List<Long> findAllProjectIdsByEligibleTCPermission(UserDto userDto, int permission);

    /**
     * Get all the ids of projects current user can read.
     *
     * @return The list of projects ids
     */
    List<Long> findAllReadableIds();

    @UsedInPlugin("rest-api")
    List<Long> findReadableProjectIdsOnRequirementLibrary();

    @UsedInPlugin("rest-api")
    List<Long> findReadableProjectIdsOnTestCaseLibrary();

    @UsedInPlugin("rest-api")
    List<Long> findReadableProjectIdsOnCampaignLibrary();

    @UsedInPlugin("rest-api")
    List<Long> findReadableProjectIdsOnActionWordLibrary();

    List<Long> findAllReadableIdsByLibraryClassName(
            UserDto userDto, String libraryClassname, TableField<ProjectRecord, Long> libraryColumnField);

    List<Long> findAllProjectIdsByEligibleTCPermission(int permission);

    /**
     * Get all the ids of projects user can export.
     *
     * @return The list of projects ids
     * @param userDto the current user dto
     * @param libraryClassname the full name of the library class which export is requested
     */
    List<Long> findAllExportableIdsOnGivenLibrary(
            UserDto userDto, String libraryClassname, TableField<ProjectRecord, Long> libraryColumnField);

    List<Long> findAllProjectIdsOrderedByName(List<Long> projectIds);

    /**
     * Return the fact that current user can read at least one project directly or through teams
     *
     * @return can current user read one project ?
     */
    boolean canReadAtLeastOneProject();

    List<Project> findAllOrderedByName();

    Collection<JsonProject> findAllProjects(List<Long> readableProjectIds, UserDto currentUser);

    List<Long> findAllReadableIdsForAutomationWriter();

    Integer countProjectsAllowAutomationWorkflow();

    List<Long> findAllManageableIds();

    List<Long> findAllManageableIds(UserDto userDto);

    List<Long> findAllMilestoneManageableIds(UserDto userDto);

    @UsedInPlugin("RedmineSync")
    Long findByRequirementLibraryId(long requirementLibraryId);
}
