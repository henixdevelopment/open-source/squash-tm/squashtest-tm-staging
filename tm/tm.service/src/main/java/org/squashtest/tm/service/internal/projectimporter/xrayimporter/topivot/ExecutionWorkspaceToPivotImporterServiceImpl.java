/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.projectimporter.xrayimporter.topivot;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.springframework.stereotype.Service;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportField;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportFile;
import org.squashtest.tm.service.internal.dto.projectimporterxray.XrayField;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.dto.ItemXrayDto;
import org.squashtest.tm.service.internal.dto.projectimporterxray.model.XrayImportModel;
import org.squashtest.tm.service.internal.dto.projectimporterxray.model.XrayItemStatus;
import org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity.executionworkspace.AbstractGenericExecutionWorkspace;
import org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity.executionworkspace.CampaignToPivot;
import org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity.executionworkspace.IterationToPivot;
import org.squashtest.tm.service.internal.projectimporter.xrayimporter.exception.ImportXrayException;
import org.squashtest.tm.service.projectimporter.xrayimporter.SquashRules;
import org.squashtest.tm.service.projectimporter.xrayimporter.XrayTablesDao;
import org.squashtest.tm.service.projectimporter.xrayimporter.topivot.AbstractImporterXrayHelper;
import org.squashtest.tm.service.projectimporter.xrayimporter.topivot.ExecutionWorkspaceToPivotImporterService;

@Service("executionWorkspaceToPivotImporterService")
public class ExecutionWorkspaceToPivotImporterServiceImpl extends AbstractImporterXrayHelper
        implements ExecutionWorkspaceToPivotImporterService {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(ExecutionWorkspaceToPivotImporterServiceImpl.class);

    private final XrayTablesDao xrayTablesDao;
    private final SquashRules squashRules;

    public ExecutionWorkspaceToPivotImporterServiceImpl(
            XrayTablesDao xrayTablesDao, SquashRules squashRules) {
        this.xrayTablesDao = xrayTablesDao;
        this.squashRules = squashRules;
    }

    @Override
    public void writeCampaign(
            JsonFactory jsonFactory,
            PrintWriter logWriter,
            ArchiveOutputStream<ZipArchiveEntry> archive,
            XrayImportModel xrayImportModel)
            throws IOException {
        logWriter.write(String.format("%sCampaign%s", System.lineSeparator(), System.lineSeparator()));
        boolean isEmptyEntity =
                xrayTablesDao.isEmptyItemTable(
                        XrayField.Type.TEST_PLAN,
                        XrayField.Type.TEST_EXECUTION,
                        XrayField.Type.SUB_TEST_EXECUTION);
        writeJson(
                jsonFactory,
                jsonGenerator -> handleCampaign(jsonGenerator, logWriter, xrayImportModel),
                JsonImportFile.CAMPAIGNS.getFileName(),
                archive,
                isEmptyEntity);
    }

    @Override
    public void writeIteration(
            JsonFactory jsonFactory,
            PrintWriter logWriter,
            ArchiveOutputStream<ZipArchiveEntry> archive,
            XrayImportModel xrayImportModel)
            throws IOException {
        logWriter.write(String.format("%sIteration%s", System.lineSeparator(), System.lineSeparator()));
        boolean isEmptyEntity =
                xrayTablesDao.isEmptyItemTable(
                        XrayField.Type.TEST_EXECUTION, XrayField.Type.SUB_TEST_EXECUTION);
        writeJson(
                jsonFactory,
                jsonGenerator -> handleIteration(jsonGenerator, logWriter, xrayImportModel),
                JsonImportFile.ITERATIONS.getFileName(),
                archive,
                isEmptyEntity);
    }

    // Campaign
    private void handleCampaign(
            JsonGenerator jsonGenerator, PrintWriter logWriter, XrayImportModel xrayImportModel)
            throws IOException {
        squashRules.handleDuplicateName(xrayTablesDao.selectDuplicateName(XrayField.Type.TEST_PLAN));
        squashRules.handleDuplicateName(
                xrayTablesDao.selectDuplicateNameOrphanTestExecution(
                        XrayField.Type.TEST_EXECUTION, XrayField.Type.SUB_TEST_EXECUTION));

        jsonGenerator.writeFieldName(JsonImportField.CAMPAIGNS);
        jsonGenerator.writeStartArray();
        XrayImportModel.EntityCount entityCount =
                new XrayImportModel.EntityCount(XrayImportModel.EntityType.CAMPAIGN);
        xrayTablesDao.selectTestPlan(
                consumerThrowingIOException(
                        itemXrayDto -> generateCampaign(jsonGenerator, logWriter, itemXrayDto, entityCount)),
                XrayField.Type.TEST_PLAN);
        xrayTablesDao.selectOrphanTestExecution(
                consumerThrowingIOException(
                        itemXrayDto -> generateCampaign(jsonGenerator, logWriter, itemXrayDto, entityCount)),
                XrayField.Type.TEST_EXECUTION,
                XrayField.Type.SUB_TEST_EXECUTION);
        xrayImportModel.getEntityCounts().add(entityCount);
        jsonGenerator.writeEndArray();
    }

    private void generateCampaign(
            JsonGenerator jsonGenerator,
            PrintWriter logWriter,
            ItemXrayDto itemXrayDto,
            XrayImportModel.EntityCount entityCount)
            throws IOException {
        try {
            CampaignToPivot campaignToPivot = populateCampaign(itemXrayDto);
            jsonGenerator.writeObject(campaignToPivot);
            squashRules.markWithSuccessIfStatusIsNull(itemXrayDto);
        } catch (ImportXrayException importXrayException) {
            LOGGER.error("Error during the generation pivot format: ", importXrayException);
            itemXrayDto.setItemStatus(XrayItemStatus.FAILURE);
        }
        entityCount.countEntity(itemXrayDto);
        writeLog(logWriter, itemXrayDto);
    }

    private CampaignToPivot populateCampaign(ItemXrayDto itemXrayDto) {
        squashRules.validateMandatoryCommonField(itemXrayDto);
        CampaignToPivot campaignToPivot = new CampaignToPivot();
        campaignToPivot.setPivotId(itemXrayDto.getPivotId());
        campaignToPivot.setName(itemXrayDto.getSummary());
        campaignToPivot.setReference(itemXrayDto.getKey());
        campaignToPivot.setDescription(
                generateDescription(itemXrayDto, this::addLinkPriorityStatusLabelToDescription));
        campaignToPivot.setStatus(squashRules.checkStatus(itemXrayDto, itemXrayDto.getStatus()));
        campaignToPivot.setParentType(EntityType.CAMPAIGN_LIBRARY);
        handleTestPlanTestCase(campaignToPivot, itemXrayDto);
        handleScheduleDate(campaignToPivot, itemXrayDto);
        return campaignToPivot;
    }

    private void handleScheduleDate(
            AbstractGenericExecutionWorkspace genericExecutionWorkspace, ItemXrayDto itemXrayDto) {
        String beginDate =
                itemXrayDto.getDateCufValue(XrayField.BEGIN_DATE, XrayField.CustomFieldKey.DATE_TIME);
        String endDate =
                itemXrayDto.getDateCufValue(XrayField.END_DATE, XrayField.CustomFieldKey.DATE_TIME);
        parseXrayDateToIsoDateIfNotNull(
                itemXrayDto,
                XrayField.BEGIN_DATE,
                beginDate,
                genericExecutionWorkspace::setScheduledStartDate);
        parseXrayDateToIsoDateIfNotNull(
                itemXrayDto, XrayField.END_DATE, endDate, genericExecutionWorkspace::setScheduledEndDate);
    }

    private void parseXrayDateToIsoDateIfNotNull(
            ItemXrayDto itemXrayDto, String dateType, String xrayDate, Consumer<String> consumeDate) {
        if (Objects.nonNull(xrayDate)) {
            try {
                DateTimeFormatter dateTimeFormatter =
                        new DateTimeFormatterBuilder()
                                .parseCaseInsensitive()
                                .appendPattern("EEE, ")
                                .appendValue(
                                        ChronoField.DAY_OF_MONTH, 1, 2, java.time.format.SignStyle.NOT_NEGATIVE)
                                .appendPattern(" MMM yyyy HH:mm:ss Z")
                                .toFormatter(Locale.ENGLISH);
                LocalDateTime localDateTime =
                        ZonedDateTime.parse(xrayDate, dateTimeFormatter).toLocalDateTime();
                consumeDate.accept(localDateTime.format(DateTimeFormatter.ISO_DATE));
            } catch (DateTimeParseException dateTimeParseException) {
                LOGGER.error(
                        "Error during the parsing of the date in xray import: ", dateTimeParseException);
                squashRules.markWithWarning(
                        itemXrayDto,
                        String.format(
                                "The date '%s' cannot be parsed with the pattern '%s'",
                                xrayDate, XrayField.DATE_FORMAT));
            }
        } else {
            squashRules.markWithWarning(
                    itemXrayDto,
                    String.format(
                            "The %s field is missing. The associated scheduling date has been ignored.",
                            dateType));
        }
    }

    private void handleTestPlanTestCase(
            AbstractGenericExecutionWorkspace genericExecutionWorkspace, ItemXrayDto itemXrayDto) {
        String missingTest =
                itemXrayDto
                        .getFilterCufValues(
                                XrayField.CustomFieldKey.TEST_PLAN_ASSOCIATED_TESTS,
                                XrayField.CustomFieldKey.EXECUTION_ASSOCIATED_TESTS)
                        .stream()
                        .filter(key -> !itemXrayDto.getAssociatedIssuesWithXrayKey().containsKey(key))
                        .collect(Collectors.joining(", "));

        if (missingTest.isEmpty()) {
            genericExecutionWorkspace
                    .getTestPlanTestCaseIds()
                    .addAll(itemXrayDto.getAssociatedIssuesWithXrayKey().values());
        } else {
            squashRules.markWithWarning(
                    itemXrayDto,
                    String.format(
                            "The following test are missing: %s. They have been ignored.", missingTest));
        }
    }

    private void handleIteration(
            JsonGenerator jsonGenerator, PrintWriter logWriter, XrayImportModel xrayImportModel)
            throws IOException {
        squashRules.handleDuplicateNameWithParentType(
                xrayTablesDao.selectDuplicateNameGroupBy(
                        XrayField.CustomFieldKey.EXECUTION_ASSOCIATED_TEST_PLAN,
                        XrayField.Type.TEST_EXECUTION,
                        XrayField.Type.SUB_TEST_EXECUTION));
        XrayImportModel.EntityCount entityCount =
                new XrayImportModel.EntityCount(XrayImportModel.EntityType.ITERATION);
        jsonGenerator.writeFieldName(JsonImportField.ITERATIONS);
        jsonGenerator.writeStartArray();
        xrayTablesDao.selectTestExecutionForIteration(
                consumerThrowingRecordAndId(
                        (itemXrayDto, id) ->
                                generateIteration(jsonGenerator, logWriter, itemXrayDto, entityCount, id)),
                XrayField.Type.TEST_EXECUTION,
                XrayField.Type.SUB_TEST_EXECUTION);
        xrayImportModel.getEntityCounts().add(entityCount);
        jsonGenerator.writeEndArray();
    }

    private void generateIteration(
            JsonGenerator jsonGenerator,
            PrintWriter logWriter,
            ItemXrayDto itemXrayDto,
            XrayImportModel.EntityCount entityCount,
            AtomicInteger id)
            throws IOException {
        try {
            IterationToPivot iterationToPivot = populateIteration(itemXrayDto, id);
            jsonGenerator.writeObject(iterationToPivot);
            squashRules.markWithSuccessIfStatusIsNull(itemXrayDto);
        } catch (ImportXrayException importXrayException) {
            LOGGER.error("Error during the generation pivot format: ", importXrayException);
            itemXrayDto.setItemStatus(XrayItemStatus.FAILURE);
        }
        entityCount.countEntity(itemXrayDto);
        writeLog(logWriter, itemXrayDto);
    }

    private IterationToPivot populateIteration(ItemXrayDto itemXrayDto, AtomicInteger id) {
        squashRules.validateMandatoryCommonField(itemXrayDto);
        IterationToPivot iterationToPivot = new IterationToPivot();
        iterationToPivot.setPivotId(id.incrementAndGet());
        iterationToPivot.setName(itemXrayDto.getSummary());
        iterationToPivot.setReference(itemXrayDto.getKey());
        iterationToPivot.setDescription(
                generateDescription(itemXrayDto, this::addLinkPriorityStatusLabelToDescription));
        iterationToPivot.setStatus(squashRules.checkStatus(itemXrayDto, itemXrayDto.getStatus()));
        handleTestPlanTestCase(iterationToPivot, itemXrayDto);
        handleScheduleDate(iterationToPivot, itemXrayDto);
        addIterationParentId(itemXrayDto, iterationToPivot);
        return iterationToPivot;
    }

    private void addIterationParentId(ItemXrayDto itemXrayDto, IterationToPivot iterationToPivot) {
        if (!itemXrayDto.getTestPlanPivotIds().isEmpty()) {
            if (itemXrayDto.getTestPlanPivotIds().size() > 1) {
                squashRules.markWithWarning(
                        itemXrayDto,
                        "Multiple Test Plan associations found. Only the first one has been taken into account.");
            }
            iterationToPivot.setParentId(itemXrayDto.getTestPlanPivotIds().get(0));
        } else {
            if (Objects.nonNull(itemXrayDto.getPivotId())) {
                iterationToPivot.setParentId(itemXrayDto.getPivotId());
            } else {
                squashRules.markWithErrorAndThrow(
                        itemXrayDto,
                        "The issue is linked to a Test Plan, but the Test Plan is not found in the export.");
            }
        }
    }
}
