/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import org.apache.commons.collections.ListUtils;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.jpa.QueryHints;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.hibernate.type.LongType;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.impl.DSL;
import static org.jooq.impl.DSL.cast;
import static org.jooq.impl.DSL.concat;
import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;
import static org.jooq.impl.DSL.select;
import static org.jooq.impl.DSL.val;
import static org.jooq.impl.DSL.value;
import static org.jooq.impl.DSL.when;
import static org.jooq.impl.DSL.withRecursive;
import org.jooq.impl.SQLDataType;
import org.jooq.tools.StringUtils;
import org.squashtest.tm.core.foundation.collection.DefaultSorting;
import org.squashtest.tm.core.foundation.collection.PagingAndSorting;
import org.squashtest.tm.core.foundation.collection.SortOrder;
import org.squashtest.tm.core.foundation.collection.Sorting;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.domain.NamedReferencePair;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.infolist.InfoListItem;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.milestone.MilestoneStatus;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.TestCase;
import static org.squashtest.tm.domain.testcase.TestCase.FULL_NAME_SEPARATOR;
import org.squashtest.tm.domain.testcase.TestCaseAutomatable;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.domain.testcase.TestCaseStatus;
import org.squashtest.tm.domain.testcase.TestStep;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATION_REQUEST;
import static org.squashtest.tm.jooq.domain.Tables.CALL_TEST_STEP;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REMOTE_AUTOMATION_REQUEST_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_COVERAGE;
import static org.squashtest.tm.jooq.domain.Tables.SCRIPTED_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_STEPS;
import org.squashtest.tm.jooq.domain.tables.TestCaseLibraryNode;
import org.squashtest.tm.service.internal.foundation.collection.PagingUtils;
import org.squashtest.tm.service.internal.foundation.collection.SortingUtils;
import org.squashtest.tm.service.internal.repository.CustomTestCaseDao;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_DISTINCT_TEST_CASES_BY_IDS;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_TESTCASES_NAME_BY_IDS;
import static org.squashtest.tm.service.internal.repository.ParameterNames.CALLED_ID;
import static org.squashtest.tm.service.internal.repository.ParameterNames.CALLED_TABLE;
import static org.squashtest.tm.service.internal.repository.ParameterNames.CALLER_ID;
import static org.squashtest.tm.service.internal.repository.ParameterNames.CALLING_TABLE;
import static org.squashtest.tm.service.internal.repository.ParameterNames.IDS;
import static org.squashtest.tm.service.internal.repository.ParameterNames.SOURCE_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CALLED;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CALLED_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CALLED_NODE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CALLER;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CALLER_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CALLER_NODE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CALL_TREE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CT_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.DIRECTION;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LINKED_CT_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LINKED_TEST_CASE_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ROAD;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_CASE_ID;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * DAO for org.squashtest.tm.domain.testcase.TestCase
 *
 * @author bsiri
 */
public class TestCaseDaoImpl extends HibernateEntityDao<TestCase> implements CustomTestCaseDao {

    /** "Standard" name for a query parameter representing a test case id. */
    private static final String TEST_CASE_ID_PARAM_NAME = "testCaseId";

    private static final String TEST_CASE_IDS_PARAM_NAME = "testCaseIds";
    private static final String TEST_CASES_IDS = "testCasesIds";
    private static final String UNCHECKED = "unchecked";
    private static final String PROJECT_ID = "projectId";

    private static final String FIND_ALL_DESCENDANT_TESTCASE_QUERY = """
        select tc.tcln_id
        from TCLN_RELATIONSHIP_CLOSURE tclnrc
        inner join TEST_CASE tc on tclnrc.DESCENDANT_ID = tc.tcln_id
        where tclnrc.ANCESTOR_ID in (:nodeIds)
    """;

    private static final String FIND_ALL_CALLING_TEST_CASE_MAIN_HQL = """
        select distinct TestCase
        from TestCase as TestCase
        left join TestCase.project as Project
        join TestCase.steps as Steps
        where Steps.calledTestCase.id = :
    """ + TEST_CASE_ID_PARAM_NAME;

    private static final String FIND_ALL_ASSOCIATED_TO_TA_SCRIPT = """
        select tc.id
        from TestCase tc
        left join tc.automationRequest req
        where tc.automatedTest is not null
        and req.testCase is null
        and tc.class = TestCase
        and tc.project.id = :
    """ + PROJECT_ID;

    private static final String FIND_TESTCASE_AND_DATASETS = """
        select tc, ds
        from TestCase tc
        left join tc.datasets ds
        where tc.id in :testCaseIds and (ds is null or ds.id in :datasetIds)
    """;

    private static final String UPDATE_NATURE_IN_LIST =
        "UPDATE TestCase tc SET tc.nature = :nature where tc.id in (:testCaseIds)";

    private static final String UPDATE_TYPE_IN_LIST =
        "UPDATE TestCase tc SET tc.type = :type where tc.id in (:testCaseIds)";

    private static final String UPDATE_STATUS_IN_LIST =
        "UPDATE TestCase tc SET tc.status = :status where tc.id in (:testCaseIds)";

    private static final String UPDATE_AUTOMATABLE_IN_LIST = """
        UPDATE TestCase tc
        SET tc.automatable = :automatable
        where tc.id in (:testCaseIds)
        and exists (
            select p.id
            from Project p
            where tc.project.id = p.id
            and p.allowAutomationWorkflow
            )
    """;

    private static List<DefaultSorting> defaultVerifiedTcSorting;

    static {
        defaultVerifiedTcSorting = new LinkedList<>();
        defaultVerifiedTcSorting.add(new DefaultSorting("TestCase.reference"));
        defaultVerifiedTcSorting.add(new DefaultSorting("TestCase.name"));
        ListUtils.unmodifiableList(defaultVerifiedTcSorting);
    }

    @Inject private DSLContext dsl;

    @Override
    public void safePersist(TestCase testCase) {

        if (testCase.getSteps().isEmpty()) {
            super.persist(testCase);
        } else {
            persistTestCaseAndSteps(testCase);
        }
    }

    @Override
    public void persistTestCaseAndSteps(TestCase testCase) {
        persistEntity(testCase);
    }

    @Override
    // FIXME Uh, should be init'd by a query !
    public TestCase findAndInit(Long testCaseId) {
        Session session = currentSession();
        TestCase tc = (TestCase) session.get(TestCase.class, testCaseId);
        if (tc == null) {
            return null;
        }
        Hibernate.initialize(tc.getSteps());

        return tc;
    }

    /*
     * Implementation note : the query :
     *
     *  select steps from TestStep steps left join fetch steps.attachmentList al left join fetch al.attachments where steps.testCase.id = :tcId order by index(steps)
     *
     *  or any other variant would not work : we need the left join, the fetch joins and order by index etc. The only way to make it work is to fetch the test case with all the required
     *  features then return the hibernate collection of steps...
     *
     *
     * (non-Javadoc)
     * @see org.squashtest.tm.service.internal.repository.CustomTestCaseDao#findTestSteps(long)
     */
    @Override
    public List<TestStep> findTestSteps(long testCaseId) {
        TestCase tc =
                (TestCase)
                        currentSession()
                                .getNamedQuery("TestCase.findInitialized")
                                .setParameter("tcId", testCaseId)
                                .uniqueResult();
        if (tc == null) {
            return Collections.emptyList();
        } else {
            return new ArrayList<>(tc.getSteps());
        }
    }

    private SetQueryParametersCallback idParameter(final long testCaseId) {
        return new SetIdParameter(TEST_CASE_ID_PARAM_NAME, testCaseId);
    }

    private static final class SetIdsParameter implements SetQueryParametersCallback {
        private Collection<Long> testCasesIds;

        private SetIdsParameter(Collection<Long> testCasesIds) {
            this.testCasesIds = testCasesIds;
        }

        @Override
        public void setQueryParameters(Query query) {
            query.setParameterList(TEST_CASES_IDS, testCasesIds);
        }
    }

    @SuppressWarnings(UNCHECKED)
    @Override
    public List<Long> findTestCasesHavingCaller(Collection<Long> testCasesIds) {
        Query query = currentSession().getNamedQuery("testCase.findTestCasesHavingCaller");
        query.setParameterList(TEST_CASES_IDS, testCasesIds);
        return query.list();
    }

    @SuppressWarnings(UNCHECKED)
    @Override
    public List<Long> findAllTestCasesIdsCalledByTestCases(Collection<Long> testCasesIds) {
        Query query = currentSession().getNamedQuery("testCase.findAllTestCasesIdsCalledByTestCases");
        query.setParameterList(TEST_CASES_IDS, testCasesIds);
        return query.list();
    }

    @SuppressWarnings(UNCHECKED)
    @Override
    public List<Long> findAllTestCasesIdsCallingTestCases(List<Long> testCasesIds) {
        if (testCasesIds.isEmpty()) {
            return Collections.emptyList();
        }
        Query query = currentSession().getNamedQuery("testCase.findAllTestCasesIdsCallingTestCases");
        query.setParameterList(TEST_CASES_IDS, testCasesIds);
        return query.list();
    }

    @Override
    @SuppressWarnings(UNCHECKED)
    public List<TestCase> findAllCallingTestCases(
            final long testCaseId, final PagingAndSorting sorting) {
        String orderBy = "";

        if (sorting != null) {
            orderBy =
                    " order by " + sorting.getSortedAttribute() + ' ' + sorting.getSortOrder().getCode();
        }

        Query query = currentSession().createQuery(FIND_ALL_CALLING_TEST_CASE_MAIN_HQL + orderBy);
        query.setParameter(TEST_CASE_ID_PARAM_NAME, testCaseId);

        if (sorting != null) {
            query.setMaxResults(sorting.getPageSize());
            query.setFirstResult(sorting.getFirstItemIndex());
        }
        return query.list();
    }

    @SuppressWarnings(UNCHECKED)
    private List<NamedReference> findTestCaseDetails(Collection<Long> ids) {
        if (ids.isEmpty()) {
            return Collections.emptyList();
        }
        Query q = currentSession().getNamedQuery("testCase.findTestCaseDetails");
        q.setParameterList(TEST_CASE_IDS_PARAM_NAME, ids, LongType.INSTANCE);
        return q.list();
    }

    @Override
    /*
     * implementation note : the following query could not use a right outer join. So we'll do the job manually. Hence
     * the weird things done below.
     */
    public List<NamedReferencePair> findTestCaseCallsUpstream(final Collection<Long> testCaseIds) {

        // get the node pairs when a caller/called pair was found.
        List<NamedReferencePair> result =
                findTestCaseCallsDetails(testCaseIds, "testCase.findTestCasesHavingCallerDetails");

        // now we must also add dummy Object[] for the test case ids that hadn't any caller
        Collection<Long> remainingIds = new HashSet<>(testCaseIds);
        for (NamedReferencePair pair : result) {
            remainingIds.remove(pair.getCalled().getId());
        }

        List<NamedReference> noncalledReferences = findTestCaseDetails(remainingIds);

        for (NamedReference ref : noncalledReferences) {
            result.add(new NamedReferencePair(null, null, ref.getId(), ref.getName()));
        }

        return result;
    }

    @Override
    public List<NamedReferencePair> findTestCaseCallsDownstream(final Collection<Long> testCaseIds) {

        // get the node pairs when a caller/called pair was found.
        List<NamedReferencePair> result =
                findTestCaseCallsDetails(testCaseIds, "testCase.findTestCasesHavingCallStepsDetails");

        // now we must also add dummy Object[] for the test case ids that hadn't any caller
        Collection<Long> remainingIds = new HashSet<>(testCaseIds);
        for (NamedReferencePair pair : result) {
            remainingIds.remove(pair.getCaller().getId());
        }

        List<NamedReference> noncalledReferences = findTestCaseDetails(remainingIds);

        for (NamedReference ref : noncalledReferences) {
            result.add(new NamedReferencePair(ref.getId(), ref.getName(), null, null));
        }

        return result;
    }

    private List<NamedReferencePair> findTestCaseCallsDetails(
            final Collection<Long> testCaseIds, String mainQuery) {
        if (testCaseIds.isEmpty()) {
            return Collections.emptyList();
        }

        // the easy part : fetch the informations for those who are called
        SetQueryParametersCallback queryCallback =
                new SetQueryParametersCallback() {
                    @Override
                    public void setQueryParameters(Query query) {
                        query.setParameterList(TEST_CASE_IDS_PARAM_NAME, testCaseIds, new LongType());
                        query.setReadOnly(true);
                    }
                };

        return executeListNamedQuery(mainQuery, queryCallback);
    }

    @SuppressWarnings(UNCHECKED)
    @Override
    public List<Long> findCalledTestCaseOfCallSteps(List<Long> testStepsIds) {
        Query query = currentSession().getNamedQuery("testCase.findCalledTestCaseOfCallSteps");
        query.setParameterList("testStepsIds", testStepsIds);
        return query.list();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.squashtest.csp.tm.internal.repository.TestCaseDao#findAllVerifyingRequirementVersion(long,
     * org.squashtest.tm.core.foundation.collection.PagingAndSorting)
     */

    /*
     * Issue #1629
     *
     * Observed problem : test cases sorted by references are indeed sorted by reference, but no more by name. Actual
     * problem : We always want them to be sorted by reference and name, even when we want primarily sort them by
     * project or execution type or else. Solution : The resultset will be sorted on all the attributes (ascending), and
     * the Sorting specified by the user will have an higher priority.
     *
     * See #createEffectiveSorting(Sorting sorting), just below
     */
    @SuppressWarnings(UNCHECKED)
    @Override
    public List<TestCase> findAllByVerifiedRequirementVersion(
            long verifiedId, PagingAndSorting sorting) {

        // create the sorting, see comments above
        List<Sorting> effectiveSortings = createEffectiveSorting(sorting);

        // we have to fetch our query and modify the hql a bit, hence the weird operation below
        Query namedquery = currentSession().getNamedQuery("testCase.findVerifyingTestCases");
        String hql = namedquery.getQueryString();
        hql = SortingUtils.addOrders(hql, effectiveSortings);

        Query q = currentSession().createQuery(hql);
        if (!sorting.shouldDisplayAll()) {
            PagingUtils.addPaging(q, sorting);
        }

        q.setParameter("versionId", verifiedId);

        List<Object[]> raw = q.list();

        // now we have to collect from the result set the only thing
        // we want : the test cases
        List<TestCase> res = new ArrayList<>(raw.size());
        for (Object[] tuple : raw) {
            res.add((TestCase) tuple[0]);
        }

        if ("endDate".equals(sorting.getSortedAttribute())) {
            Collections.sort(
                    res,
                    new Comparator<TestCase>() {
                        @Override
                        public int compare(TestCase tc1, TestCase tc2) {
                            return compareTcMilestoneDate(tc1, tc2);
                        }
                    });

            if (sorting.getSortOrder() == SortOrder.ASCENDING) {
                Collections.reverse(res);
            }
        }

        return res;
    }

    @Override
    public List<Long> findAllTestCaseAssociatedToTAScriptByProject(Long projectId) {

        Query query = currentSession().createQuery(FIND_ALL_ASSOCIATED_TO_TA_SCRIPT);
        query.setParameter(PROJECT_ID, projectId);
        return query.getResultList();
    }

    @Override
    public Integer countScriptedTestCaseAssociatedToTAScriptByProject(Long projectId) {
        return dsl.selectCount()
                .from(SCRIPTED_TEST_CASE)
                .innerJoin(TEST_CASE)
                .on(TEST_CASE.TCLN_ID.eq(SCRIPTED_TEST_CASE.TCLN_ID))
                .innerJoin(TEST_CASE_LIBRARY_NODE)
                .on(TEST_CASE.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .innerJoin(PROJECT)
                .on(PROJECT.PROJECT_ID.eq(TEST_CASE_LIBRARY_NODE.PROJECT_ID))
                .where(PROJECT.PROJECT_ID.eq(projectId))
                .and(TEST_CASE.TA_TEST.isNotNull())
                .fetchOne()
                .value1();
    }

    @Override
    public List<TestCase> findTestCaseByAutomationRequestIds(List<Long> requestIds) {
        Query query =
                (Query) entityManager.createNamedQuery("testCase.findTestCaseByAutomationRequestIds");
        query.setParameter("requestIds", requestIds);
        return query.getResultList();
    }

    @Override
    public TestCase findTestCaseByUuid(String uuid) {
        javax.persistence.Query query = entityManager.createNamedQuery("testCase.findTestCaseByUuid");
        query.setParameter("uuid", uuid);
        try {
            return (TestCase) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public List<String> retrieveFullNameByTestCaseLibraryNodeIds(
            List<Long> testCaseLibrayNodeIds, List<Long> projectIds) {
        Field<String> fullName =
                when(
                                TEST_CASE.REFERENCE.isNull().or(TEST_CASE.REFERENCE.eq(StringUtils.EMPTY)),
                                TEST_CASE_LIBRARY_NODE.NAME)
                        .otherwise(
                                concat(TEST_CASE.REFERENCE, val(FULL_NAME_SEPARATOR), TEST_CASE_LIBRARY_NODE.NAME));

        return dsl.select(fullName)
                .from(TEST_CASE_LIBRARY_NODE)
                .leftJoin(TEST_CASE)
                .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .where(TEST_CASE_LIBRARY_NODE.TCLN_ID.in(testCaseLibrayNodeIds))
                .and(TEST_CASE_LIBRARY_NODE.PROJECT_ID.in(projectIds))
                .orderBy(fullName)
                .fetch(fullName);
    }

    @Override
    public List<TestCase> findAllByIdsWithProject(List<Long> testCaseIds) {
        javax.persistence.Query query =
                entityManager.createNamedQuery("testCase.findAllByIdsWithProject");
        query.setParameter("tcIds", testCaseIds);
        return query.getResultList();
    }

    @Override
    public void updateNature(List<Long> testCaseIds, InfoListItem nature) {
        javax.persistence.Query query = entityManager.createQuery(UPDATE_NATURE_IN_LIST);
        query.setParameter("nature", nature);
        query.setParameter(TEST_CASE_IDS_PARAM_NAME, testCaseIds);
        query.executeUpdate();
    }

    @Override
    public void updateType(List<Long> testCaseIds, InfoListItem type) {
        javax.persistence.Query query = entityManager.createQuery(UPDATE_TYPE_IN_LIST);
        query.setParameter("type", type);
        query.setParameter(TEST_CASE_IDS_PARAM_NAME, testCaseIds);
        query.executeUpdate();
    }

    @Override
    public void updateStatus(List<Long> testCaseIds, TestCaseStatus status) {
        javax.persistence.Query query = entityManager.createQuery(UPDATE_STATUS_IN_LIST);
        query.setParameter("status", status);
        query.setParameter(TEST_CASE_IDS_PARAM_NAME, testCaseIds);
        query.executeUpdate();
    }

    @Override
    public void updateAutomatable(List<Long> testCaseIds, TestCaseAutomatable automatable) {
        javax.persistence.Query query = entityManager.createQuery(UPDATE_AUTOMATABLE_IN_LIST);
        query.setParameter("automatable", automatable);
        query.setParameter(TEST_CASE_IDS_PARAM_NAME, testCaseIds);
        query.executeUpdate();
    }

    @Override
    public Set<Long> findAllTestCaseIdsCoveringRequirementVersions(
            Collection<Long> requirementVersionIds) {
        return new HashSet<>(
                dsl.selectDistinct(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID)
                        .from(REQUIREMENT_VERSION_COVERAGE)
                        .where(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.in(requirementVersionIds))
                        .fetch()
                        .getValues(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID, Long.class));
    }

    private int compareTcMilestoneDate(TestCase tc1, TestCase tc2) {

        boolean isEmpty1 = tc1.getMilestones().isEmpty();
        boolean isEmpty2 = tc2.getMilestones().isEmpty();

        if (isEmpty1 && isEmpty2) {
            return 0;
        } else if (isEmpty1) {
            return 1;
        } else if (isEmpty2) {
            return -1;
        } else {
            return getMinDate(tc1).before(getMinDate(tc2))
                    ? getMinDate(tc1).after(getMinDate(tc2)) ? 0 : 1
                    : -1;
        }
    }

    private Date getMinDate(TestCase tc) {
        return Collections.min(
                        tc.getMilestones(),
                        new Comparator<Milestone>() {
                            @Override
                            public int compare(Milestone m1, Milestone m2) {
                                return m1.getEndDate().before(m2.getEndDate()) ? -1 : 1;
                            }
                        })
                .getEndDate();
    }

    /**
     * @param userSorting
     * @return
     */
    /*
     * Issue #1629
     *
     * Observed problem : test cases sorted by references are indeed sorted by reference, but no more by name. Actual
     * problem : We always want them to be sorted by reference and name, even when we want primarily sort them by
     * project or execution type or else. Solution : The resultset will be sorted on all the attributes (ascending), and
     * the Sorting specified by the user will have an higher priority.
     *
     * See #createEffectiveSorting(Sorting sorting), just below
     */

    private List<Sorting> createEffectiveSorting(Sorting userSorting) {

        LinkedList<Sorting> sortings = new LinkedList<>(defaultVerifiedTcSorting);

        // from that list we filter out the redundant element, considering the argument.
        // note that the sorting order is irrelevant here.
        ListIterator<Sorting> iterator = sortings.listIterator();
        while (iterator.hasNext()) {
            Sorting defaultSorting = iterator.next();
            if (defaultSorting.getSortedAttribute().equals(userSorting.getSortedAttribute())) {
                iterator.remove();
                break;
            }
        }

        // now we can set the Sorting specified by the user in first position
        sortings.addFirst(userSorting);

        return sortings;
    }

    /**
     * @see
     *     org.squashtest.tm.service.internal.repository.TestCaseDao#countByVerifiedRequirementVersion(long)
     */
    @Override
    public long countByVerifiedRequirementVersion(final long verifiedId) {
        return (Long)
                executeEntityNamedQuery(
                        "testCase.countByVerifiedRequirementVersion", new SetVerifiedIdParameter(verifiedId));
    }

    private static final class SetVerifiedIdParameter implements SetQueryParametersCallback {
        private long verifiedId;

        private SetVerifiedIdParameter(long verifiedId) {
            this.verifiedId = verifiedId;
        }

        @Override
        public void setQueryParameters(Query query) {
            query.setLong("verifiedId", verifiedId);
        }
    }

    @SuppressWarnings(UNCHECKED)
    @Override
    public List<TestCase> findUnsortedAllByVerifiedRequirementVersion(long requirementVersionId) {
        Query query =
                currentSession().getNamedQuery("testCase.findUnsortedAllByVerifiedRequirementVersion");
        query.setParameter("requirementVersionId", requirementVersionId);
        return query.list();
    }

    @Override
    public List<Execution> findAllExecutionByTestCase(Long tcId) {
        SetQueryParametersCallback callback = idParameter(tcId);
        return executeListNamedQuery("testCase.findAllExecutions", callback);
    }

    /* ----------------------------------------------------EXPORT METHODS----------------------------------------- */

    @SuppressWarnings(UNCHECKED)
    @Override
    public List<Long> findAllTestCaseIdsByNodeIds(Collection<Long> nodeIds) {
        if (nodeIds.isEmpty()) {
            return Collections.emptyList();
        }

        NativeQuery query = currentSession().createNativeQuery(FIND_ALL_DESCENDANT_TESTCASE_QUERY);
        query.setParameterList("nodeIds", nodeIds, LongType.INSTANCE);
        query.setResultTransformer(new SqLIdResultTransformer());

        return query.list();
    }

    /* ----------------------------------------------------/EXPORT METHODS----------------------------------------- */

    @Override
    public List<TestCase> findAllLinkedToIteration(List<Long> nodeIds) {
        return executeListNamedQuery("testCase.findAllLinkedToIteration", new SetIdsParameter(nodeIds));
    }

    @Override
    public Map<Long, TestCaseImportance> findAllTestCaseImportanceWithImportanceAuto(
            Collection<Long> testCaseIds) {
        Map<Long, TestCaseImportance> resultMap = new HashMap<>();
        if (testCaseIds.isEmpty()) {
            return resultMap;
        }
        List<Object[]> resultList =
                executeListNamedQuery("testCase.findAllTCImpWithImpAuto", new SetIdsParameter(testCaseIds));
        for (Object[] resultEntry : resultList) {
            Long id = (Long) resultEntry[0];
            TestCaseImportance imp = (TestCaseImportance) resultEntry[1];
            resultMap.put(id, imp);
        }
        return resultMap;
    }

    @Override
    public List<Long> findAllEligibleTestCaseIds(List<Long> testCaseIds) {
        return dsl.selectDistinct(TEST_CASE.TCLN_ID)
                .from(TEST_CASE)
                .innerJoin(TEST_CASE_LIBRARY_NODE)
                .on(TEST_CASE.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .innerJoin(PROJECT)
                .on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .where(TEST_CASE.TCLN_ID.in(testCaseIds))
                .and(PROJECT.ALLOW_AUTOMATION_WORKFLOW.isTrue())
                .and(TEST_CASE.AUTOMATABLE.eq(TestCaseAutomatable.Y.name()))
                .fetch(TEST_CASE.TCLN_ID);
    }

    @Override
    public Map<Long, String> findAllAutomatableTestCasesByProjectId(long projectId) {
        return dsl.select(TEST_CASE.TCLN_ID, REMOTE_AUTOMATION_REQUEST_EXTENDER.REMOTE_STATUS)
                .from(TEST_CASE)
                .innerJoin(AUTOMATION_REQUEST)
                .on(TEST_CASE.AUTOMATION_REQUEST_ID.eq(AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID))
                .innerJoin(PROJECT)
                .on(AUTOMATION_REQUEST.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .innerJoin(REMOTE_AUTOMATION_REQUEST_EXTENDER)
                .on(
                        AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID.eq(
                                REMOTE_AUTOMATION_REQUEST_EXTENDER.AUTOMATION_REQUEST_ID))
                .where(AUTOMATION_REQUEST.PROJECT_ID.eq(projectId))
                .and(PROJECT.ALLOW_AUTOMATION_WORKFLOW.isTrue())
                .and(TEST_CASE.AUTOMATABLE.eq(TestCaseAutomatable.Y.name()))
                .fetchMap(TEST_CASE.TCLN_ID, REMOTE_AUTOMATION_REQUEST_EXTENDER.REMOTE_STATUS);
    }

    @Override
    public List<Long> findAllTCIdsEligibleForCopyByProjectIds(
            List<Long> targetTestCasesIds, List<Long> editableTCProjectIds) {
        return dsl.selectDistinct(TEST_CASE.TCLN_ID)
                .from(TEST_CASE)
                .innerJoin(TEST_CASE_LIBRARY_NODE)
                .on(TEST_CASE.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .innerJoin(PROJECT)
                .on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .where(TEST_CASE.TCLN_ID.in(targetTestCasesIds))
                .and(PROJECT.PROJECT_ID.in(editableTCProjectIds))
                .fetch(TEST_CASE.TCLN_ID);
    }

    @Override
    public List<Long> findAllTCIdsForActiveMilestoneInList(
            Long activeMilestoneId, List<Long> testCaseIds) {
        return dsl.selectDistinct(MILESTONE_TEST_CASE.TEST_CASE_ID)
                .from(MILESTONE_TEST_CASE)
                .where(MILESTONE_TEST_CASE.MILESTONE_ID.eq(activeMilestoneId))
                .and(MILESTONE_TEST_CASE.TEST_CASE_ID.in(testCaseIds))
                .fetch(MILESTONE_TEST_CASE.TEST_CASE_ID);
    }

    @Override
    public Map<Long, String> findTestCaseExecutionModesByTestCaseIds(List<Long> testCaseIds) {
        return dsl.select(TEST_CASE.TCLN_ID, TEST_CASE.EXECUTION_MODE)
                .from(TEST_CASE)
                .where(TEST_CASE.TCLN_ID.in(testCaseIds))
                .fetchMap(TEST_CASE.TCLN_ID, TEST_CASE.EXECUTION_MODE);
    }

    @Override
    public List<Long> filterTestCaseIdsWithLockedMilestone(List<Long> testCaseIds) {
        return dsl.select(MILESTONE_TEST_CASE.TEST_CASE_ID)
                .from(MILESTONE_TEST_CASE)
                .innerJoin(MILESTONE)
                .on(MILESTONE_TEST_CASE.MILESTONE_ID.eq(MILESTONE.MILESTONE_ID))
                .where(
                        MILESTONE_TEST_CASE
                                .TEST_CASE_ID
                                .in(testCaseIds)
                                .and(MILESTONE.STATUS.eq(MilestoneStatus.LOCKED.name())))
                .union(
                        select(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID)
                                .from(REQUIREMENT_VERSION_COVERAGE)
                                .innerJoin(MILESTONE_REQ_VERSION)
                                .on(
                                        REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.eq(
                                                MILESTONE_REQ_VERSION.REQ_VERSION_ID))
                                .innerJoin(MILESTONE)
                                .on(MILESTONE_REQ_VERSION.MILESTONE_ID.eq(MILESTONE.MILESTONE_ID))
                                .where(
                                        REQUIREMENT_VERSION_COVERAGE
                                                .VERIFYING_TEST_CASE_ID
                                                .in(testCaseIds)
                                                .and(MILESTONE.STATUS.eq(MilestoneStatus.LOCKED.name()))))
                .fetchInto(Long.class);
    }

    @Override
    public Map<TestCase, List<Dataset>> findTestCaseAndDatasets(Map<Long, List<Long>> datasetIdsByTestCaseId) {
        List<Long> testCaseIds = new ArrayList<>(datasetIdsByTestCaseId.keySet());
        List<Long> datasetIds = datasetIdsByTestCaseId.values()
            .stream()
            .flatMap(Collection::stream).distinct().toList();

        try (Stream<Object[]> resultStream = getTestCaseAndDatasetsStream(testCaseIds, datasetIds)) {

            return resultStream.collect(Collectors.groupingBy(
                objects -> (TestCase) objects[0],
                Collectors.mapping(objects -> (Dataset) objects[1], Collectors.toList())));
        }
    }

    private Stream<Object[]> getTestCaseAndDatasetsStream(List<Long> testCaseIds, List<Long> datasetIds) {
        return entityManager.createQuery(FIND_TESTCASE_AND_DATASETS, Object[].class)
            .setParameter(TEST_CASE_IDS_PARAM_NAME, testCaseIds)
            .setParameter("datasetIds", datasetIds)
            .getResultStream();
    }

    @Override
    public List<String> filterExistingTestCaseUuids(Collection<String> uuids) {
        return dsl.select(TEST_CASE.UUID)
            .from(TEST_CASE)
            .where(TEST_CASE.UUID.in(uuids))
            .fetchInto(String.class);
    }

    @Override
    public Map<Long, List<Long>> findRecursiveCalledIdsByCallerId(Collection<Long> callerIds) {
        var cte = name(CALLED_TABLE).fields(SOURCE_ID, CALLED_ID).as(
            select(
                TEST_CASE_STEPS.TEST_CASE_ID,
                CALL_TEST_STEP.CALLED_TEST_CASE_ID)
                .from(TEST_CASE_STEPS)
                .innerJoin(CALL_TEST_STEP).on(TEST_CASE_STEPS.STEP_ID.eq(CALL_TEST_STEP.TEST_STEP_ID))
                .where(TEST_CASE_STEPS.TEST_CASE_ID.in(callerIds))
                .union(
                    select(
                        field(name(CALLED_TABLE, SOURCE_ID), Long.class),
                        CALL_TEST_STEP.CALLED_TEST_CASE_ID
                    )
                        .from(name(CALLED_TABLE))
                        .innerJoin(TEST_CASE_STEPS).on(field(name(CALLED_TABLE, SOURCE_ID)).eq(TEST_CASE_STEPS.TEST_CASE_ID))
                        .innerJoin(CALL_TEST_STEP).on(TEST_CASE_STEPS.STEP_ID.eq(CALL_TEST_STEP.TEST_STEP_ID))
                ));

        return dsl.withRecursive(cte)
            .selectFrom(cte)
            .fetchGroups(
                field(SOURCE_ID, Long.class),
                field(CALLED_ID, Long.class)
            );
    }

    @Override
    public Map<Long, List<TestCase>> findRecursiveCallerTestCasesByCalledId(Collection<Long> calledIds) {
        Map<Long, List<Long>> callerIdsByCalledId = findRecursiveCallerByCalledId(calledIds);

        Set<Long> allCallerIds = callerIdsByCalledId.values().stream()
            .flatMap(List::stream)
            .collect(Collectors.toSet());

        if (allCallerIds.isEmpty()) {
            return Collections.emptyMap();
        }

        Map<Long, TestCase> callerTestCaseById = entityManager.createQuery(FIND_DISTINCT_TEST_CASES_BY_IDS, TestCase.class)
            .setParameter(IDS, allCallerIds)
            .setHint(QueryHints.HINT_PASS_DISTINCT_THROUGH, false)
            .getResultList()
            .stream()
            .collect(Collectors.toMap(
                org.squashtest.tm.domain.testcase.TestCaseLibraryNode::getId,
                Function.identity()
            ));

        return callerIdsByCalledId.entrySet().stream()
            .flatMap(entry -> entry.getValue().stream()
                .map(callerId -> Map.entry(entry.getKey(), callerTestCaseById.get(callerId)))
                .filter(e -> e.getValue() != null))
            .collect(Collectors.groupingBy(Map.Entry::getKey,
                Collectors.mapping(Map.Entry::getValue, Collectors.toList())));
    }

    private Map<Long, List<Long>> findRecursiveCallerByCalledId(Collection<Long> calledTestCaseIds) {
        var cte = name(CALLING_TABLE).fields(SOURCE_ID, CALLER_ID).as(
            select(
                CALL_TEST_STEP.CALLED_TEST_CASE_ID,
                TEST_CASE_STEPS.TEST_CASE_ID)
                .from(CALL_TEST_STEP)
                .innerJoin(TEST_CASE_STEPS).on(CALL_TEST_STEP.TEST_STEP_ID.eq(TEST_CASE_STEPS.STEP_ID))
                .where(CALL_TEST_STEP.CALLED_TEST_CASE_ID.in(calledTestCaseIds))
                .union(
                    select(
                        field(name(CALLING_TABLE, SOURCE_ID), Long.class),
                        TEST_CASE_STEPS.TEST_CASE_ID
                    )
                        .from(name(CALLING_TABLE))
                        .innerJoin(CALL_TEST_STEP).on(field(name(CALLING_TABLE, CALLER_ID)).eq(CALL_TEST_STEP.CALLED_TEST_CASE_ID))
                        .innerJoin(TEST_CASE_STEPS).on(CALL_TEST_STEP.TEST_STEP_ID.eq(TEST_CASE_STEPS.STEP_ID))
                ));

        return dsl.withRecursive(cte)
            .selectFrom(cte)
            .fetchGroups(
                field(SOURCE_ID, Long.class),
                field(CALLER_ID, Long.class)
            );
    }

    @Override
    public List<NamedReferencePair> findRecursiveTestCasesCalls(Collection<Long> sourceIds) {
        TestCaseLibraryNode calledTestCase = TEST_CASE_LIBRARY_NODE.as(CALLED_NODE);
        TestCaseLibraryNode callerTestCase = TEST_CASE_LIBRARY_NODE.as(CALLER_NODE);

        var testCaseCalls = name("testcase_calls").fields(CT_ID, LINKED_CT_ID, DIRECTION).as(
            select(TEST_CASE_STEPS.TEST_CASE_ID, CALL_TEST_STEP.CALLED_TEST_CASE_ID, value(Boolean.TRUE))
                .from(TEST_CASE_STEPS)
                .innerJoin(CALL_TEST_STEP).on(TEST_CASE_STEPS.STEP_ID.eq(CALL_TEST_STEP.TEST_STEP_ID))
                .union(
                    select(CALL_TEST_STEP.CALLED_TEST_CASE_ID, TEST_CASE_STEPS.TEST_CASE_ID, value(Boolean.FALSE))
                        .from(CALL_TEST_STEP)
                        .innerJoin(TEST_CASE_STEPS).on(CALL_TEST_STEP.TEST_STEP_ID.eq(TEST_CASE_STEPS.STEP_ID))
                )
        );

        var cte = name(CALL_TREE).fields(TEST_CASE_ID, LINKED_TEST_CASE_ID, ROAD, DIRECTION).as(
            select(
                testCaseCalls.field(CT_ID, Long.class),
                testCaseCalls.field(LINKED_CT_ID, Long.class),
                cast(
                    concat(
                        DSL.value(" "),
                        testCaseCalls.field(CT_ID, Long.class),
                        DSL.value(" "),
                        testCaseCalls.field(LINKED_CT_ID, Long.class)
                    ), SQLDataType.VARCHAR(1000)),
                testCaseCalls.field(DIRECTION, Boolean.class)
            )
                .from(testCaseCalls)
                .where(testCaseCalls.field(CT_ID).in(sourceIds))
                .union(
                    select(
                        field(name(CALL_TREE, LINKED_TEST_CASE_ID), Long.class),
                        testCaseCalls.field(LINKED_CT_ID, Long.class),
                        cast(
                            concat(
                                field(name(CALL_TREE, ROAD)),
                                value(" "),
                                testCaseCalls.field(LINKED_CT_ID)
                            ), SQLDataType.VARCHAR(1000)),
                        testCaseCalls.field(DIRECTION, Boolean.class)
                    )
                        .from(name(CALL_TREE))
                        .innerJoin(testCaseCalls).on(
                            field(name(CALL_TREE, LINKED_TEST_CASE_ID)).eq(testCaseCalls.field(CT_ID))
                                .and(field(name(CALL_TREE, ROAD)).notLike(concat(
                                    value("% "),
                                    testCaseCalls.field(LINKED_CT_ID),
                                    value(" %")
                                )))
                        )
                )
        );

        var callTreeTable = withRecursive(cte, testCaseCalls)
            .select(
                when(field(DIRECTION, Boolean.class).eq(Boolean.TRUE), field(TEST_CASE_ID, Long.class))
                    .otherwise(field(LINKED_TEST_CASE_ID, Long.class))
                    .as(CALLER),
                when(field(DIRECTION, Boolean.class).eq(Boolean.TRUE), field(LINKED_TEST_CASE_ID, Long.class))
                    .otherwise(field(TEST_CASE_ID, Long.class))
                    .as(CALLED)
            ).from(cte)
            .groupBy(field(CALLER), field(CALLED));

        return dsl.select(
            callTreeTable.field(CALLER, Long.class),
            callerTestCase.NAME.as(CALLER_NAME),
            callTreeTable.field(CALLED, Long.class),
            calledTestCase.NAME.as(CALLED_NAME)
        ).from(callTreeTable)
            .innerJoin(callerTestCase).on(callTreeTable.field(CALLER, Long.class).eq(callerTestCase.TCLN_ID))
            .innerJoin(calledTestCase).on(callTreeTable.field(CALLED, Long.class).eq(calledTestCase.TCLN_ID))
            .fetchStream()
            .map(r -> new NamedReferencePair(
                r.get(CALLER, Long.class),
                r.get(CALLER_NAME, String.class),
                r.get(CALLED, Long.class),
                r.get(CALLED_NAME, String.class)
            )).toList();
    }

    @Override
    public Map<Long, String> findNameByTestCaseId(Collection<Long> testCaseIds) {
        try (Stream<Object[]> resultStream = getTestCasesNameByIdsStream(testCaseIds)) {
            return resultStream.collect(Collectors.toMap(
                r -> (Long) r[0],
                r -> (String) r[1]
            ));
        }
    }

    private Stream<Object[]> getTestCasesNameByIdsStream(Collection<Long> testCaseIds) {
        return entityManager
            .createQuery(FIND_TESTCASES_NAME_BY_IDS, Object[].class)
            .setParameter(IDS, testCaseIds)
            .getResultStream();
    }
}
