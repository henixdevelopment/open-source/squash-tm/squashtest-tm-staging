/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporter.testcaseworkspace;

import org.squashtest.tm.domain.testcase.ParameterAssignationMode;

public class CalledTestCaseStepToImport {

    private String internalId;
    private String callerId;
    private String calledTestCaseInternalId;
    private Integer index;
    ParameterAssignationMode parameterAssignationMode;
    private String internalDatasetId;

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getCallerId() {
        return callerId;
    }

    public void setCallerId(String callerId) {
        this.callerId = callerId;
    }

    public String getCalledTestCaseInternalId() {
        return calledTestCaseInternalId;
    }

    public void setCalledTestCaseInternalId(String calledTestCaseInternalId) {
        this.calledTestCaseInternalId = calledTestCaseInternalId;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public ParameterAssignationMode getParameterAssignationMode() {
        return parameterAssignationMode;
    }

    public void setParameterAssignationMode(ParameterAssignationMode parameterAssignationMode) {
        this.parameterAssignationMode = parameterAssignationMode;
    }

    public String getInternalDatasetId() {
        return internalDatasetId;
    }

    public void setInternalDatasetId(String internalDatasetId) {
        this.internalDatasetId = internalDatasetId;
    }
}
