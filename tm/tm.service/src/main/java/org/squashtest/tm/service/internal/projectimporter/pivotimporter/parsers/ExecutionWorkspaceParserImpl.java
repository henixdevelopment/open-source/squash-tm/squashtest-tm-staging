/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.projectimporter.pivotimporter.parsers;

import com.fasterxml.jackson.core.JsonParser;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Service;
import org.squashtest.tm.core.foundation.lang.DateUtils;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.campaign.CampaignStatus;
import org.squashtest.tm.domain.campaign.IterationStatus;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.projectimporter.PivotFormatImport;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportField;
import org.squashtest.tm.service.internal.dto.projectimporter.PivotImportMetadata;
import org.squashtest.tm.service.internal.dto.projectimporter.campaignworkspace.CampaignToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.campaignworkspace.ExecutionStepInfo;
import org.squashtest.tm.service.internal.dto.projectimporter.campaignworkspace.ExecutionToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.campaignworkspace.IterationToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.campaignworkspace.TestSuiteToImport;
import org.squashtest.tm.service.internal.projectimporter.pivotimporter.PivotFormatLoggerHelper;
import org.squashtest.tm.service.projectimporter.pivotimporter.PivotJsonParsingHelper;
import org.squashtest.tm.service.projectimporter.pivotimporter.parsers.ExecutionWorkspaceParser;

@Service("ExecutionWorkspaceParser")
public class ExecutionWorkspaceParserImpl implements ExecutionWorkspaceParser {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExecutionWorkspaceParserImpl.class);

    @Override
    public CampaignToImport parseCampaign(
            JsonParser jsonParser,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport) {
        CampaignToImport campaignToImport = new CampaignToImport();

        try {
            while (PivotJsonParsingHelper.isNotTheEndOfParsedObject(jsonParser)) {
                String fieldName = jsonParser.getCurrentName();
                jsonParser.nextToken();

                switch (fieldName) {
                    case JsonImportField.ID -> campaignToImport.setInternalId(jsonParser.getText());
                    case JsonImportField.NAME -> campaignToImport.setName(jsonParser.getText());
                    case JsonImportField.DESCRIPTION -> campaignToImport.setDescription(jsonParser.getText());
                    case JsonImportField.REFERENCE -> campaignToImport.setReference(jsonParser.getText());
                    case JsonImportField.STATUS ->
                            campaignToImport.setStatus(CampaignStatus.valueOf(jsonParser.getText()));
                    case JsonImportField.SCHEDULED_START_DATE ->
                            campaignToImport.setScheduledStartDate(parseIso8601Date(jsonParser.getText()));
                    case JsonImportField.SCHEDULED_END_DATE ->
                            campaignToImport.setScheduledEndDate(parseIso8601Date(jsonParser.getText()));
                    case JsonImportField.ACTUAL_START_DATE ->
                            campaignToImport.setActualStartDate(parseIso8601Date(jsonParser.getText()));
                    case JsonImportField.ACTUAL_END_DATE ->
                            campaignToImport.setActualEndDate(parseIso8601Date(jsonParser.getText()));
                    case JsonImportField.ACTUAL_START_DATE_AUTO ->
                            campaignToImport.setActualStartAuto(jsonParser.getBooleanValue());
                    case JsonImportField.ACTUAL_END_DATE_AUTO ->
                            campaignToImport.setActualEndAuto(jsonParser.getBooleanValue());
                    case JsonImportField.CUSTOM_FIELDS ->
                            campaignToImport.setCustomFields(
                                    PivotJsonParsingHelper.getCustomFieldValues(
                                            jsonParser, pivotImportMetadata.getCustomFieldIdsMap()));
                    case JsonImportField.TEST_PLAN_TEST_CASE_IDS ->
                            campaignToImport.setTestPlanTestCaseIds(
                                    PivotJsonParsingHelper.getArrayStringValues(jsonParser));
                    case JsonImportField.PARENT_TYPE ->
                            campaignToImport.setParentType(EntityType.valueOf(jsonParser.getText()));
                    case JsonImportField.PARENT_ID -> campaignToImport.setParentId(jsonParser.getText());
                    case JsonImportField.ATTACHMENTS ->
                            campaignToImport.setAttachments(AttachmentParserHelper.parseAttachments(jsonParser));
                    default -> {
                        // continue parsing
                    }
                }
            }
            PivotFormatLoggerHelper.logParsingSuccessForEntity(
                    LOGGER,
                    PivotFormatLoggerHelper.CAMPAIGN,
                    campaignToImport.getName(),
                    campaignToImport.getInternalId(),
                    pivotFormatImport);
        } catch (Exception e) {
            PivotFormatLoggerHelper.handleParsingErrorForEntity(
                    LOGGER,
                    PivotFormatLoggerHelper.CAMPAIGN,
                    campaignToImport.getInternalId(),
                    pivotFormatImport,
                    e);
        }

        return campaignToImport;
    }

    @Override
    public IterationToImport parseIteration(
            JsonParser jsonParser,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport) {
        IterationToImport iterationToImport = new IterationToImport();

        try {

            while (PivotJsonParsingHelper.isNotTheEndOfParsedObject(jsonParser)) {
                String fieldName = jsonParser.getCurrentName();
                jsonParser.nextToken();

                switch (fieldName) {
                    case JsonImportField.ID -> iterationToImport.setInternalId(jsonParser.getText());
                    case JsonImportField.NAME -> iterationToImport.setName(jsonParser.getText());
                    case JsonImportField.DESCRIPTION ->
                            iterationToImport.setDescription(jsonParser.getText());
                    case JsonImportField.REFERENCE -> iterationToImport.setReference(jsonParser.getText());
                    case JsonImportField.STATUS ->
                            iterationToImport.setStatus(IterationStatus.valueOf(jsonParser.getText()));
                    case JsonImportField.SCHEDULED_START_DATE ->
                            iterationToImport.setScheduledStartDate(parseIso8601Date(jsonParser.getText()));
                    case JsonImportField.SCHEDULED_END_DATE ->
                            iterationToImport.setScheduledEndDate(parseIso8601Date(jsonParser.getText()));
                    case JsonImportField.ACTUAL_START_DATE ->
                            iterationToImport.setActualStartDate(parseIso8601Date(jsonParser.getText()));
                    case JsonImportField.ACTUAL_END_DATE ->
                            iterationToImport.setActualEndDate(parseIso8601Date(jsonParser.getText()));
                    case JsonImportField.ACTUAL_START_DATE_AUTO ->
                            iterationToImport.setActualStartAuto(jsonParser.getBooleanValue());
                    case JsonImportField.ACTUAL_END_DATE_AUTO ->
                            iterationToImport.setActualEndAuto(jsonParser.getBooleanValue());
                    case JsonImportField.CUSTOM_FIELDS ->
                            iterationToImport.setCustomFields(
                                    PivotJsonParsingHelper.getCustomFieldValues(
                                            jsonParser, pivotImportMetadata.getCustomFieldIdsMap()));
                    case JsonImportField.TEST_PLAN_TEST_CASE_IDS ->
                            iterationToImport.setTestPlanTestCaseIds(
                                    PivotJsonParsingHelper.getArrayStringValues(jsonParser));
                    case JsonImportField.PARENT_ID -> iterationToImport.setParentId(jsonParser.getText());
                    case JsonImportField.ATTACHMENTS ->
                            iterationToImport.setAttachments(AttachmentParserHelper.parseAttachments(jsonParser));
                    default -> {
                        // continue parsing
                    }
                }
            }
            PivotFormatLoggerHelper.logParsingSuccessForEntity(
                    LOGGER,
                    PivotFormatLoggerHelper.ITERATION,
                    iterationToImport.getName(),
                    iterationToImport.getInternalId(),
                    pivotFormatImport);
        } catch (Exception e) {
            PivotFormatLoggerHelper.handleParsingErrorForEntity(
                    LOGGER,
                    PivotFormatLoggerHelper.ITERATION,
                    iterationToImport.getInternalId(),
                    pivotFormatImport,
                    e);
        }
        return iterationToImport;
    }

    @Override
    public TestSuiteToImport parseTestSuite(
            JsonParser jsonParser,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport) {
        TestSuiteToImport testSuiteToImport = new TestSuiteToImport();

        try {

            while (PivotJsonParsingHelper.isNotTheEndOfParsedObject(jsonParser)) {
                String fieldName = jsonParser.getCurrentName();
                jsonParser.nextToken();

                switch (fieldName) {
                    case JsonImportField.ID -> testSuiteToImport.setInternalId(jsonParser.getText());
                    case JsonImportField.NAME -> testSuiteToImport.setName(jsonParser.getText());
                    case JsonImportField.DESCRIPTION ->
                            testSuiteToImport.setDescription(jsonParser.getText());
                    case JsonImportField.STATUS ->
                            testSuiteToImport.setExecutionStatus(ExecutionStatus.valueOf(jsonParser.getText()));
                    case JsonImportField.CUSTOM_FIELDS ->
                            testSuiteToImport.setCustomFields(
                                    PivotJsonParsingHelper.getCustomFieldValues(
                                            jsonParser, pivotImportMetadata.getCustomFieldIdsMap()));
                    case JsonImportField.TEST_PLAN_TEST_CASE_IDS ->
                            testSuiteToImport.setTestPlanTestCaseIds(
                                    PivotJsonParsingHelper.getArrayStringValues(jsonParser));
                    case JsonImportField.PARENT_ID -> testSuiteToImport.setParentId(jsonParser.getText());
                    case JsonImportField.ATTACHMENTS ->
                            testSuiteToImport.setAttachments(AttachmentParserHelper.parseAttachments(jsonParser));
                    default -> {
                        // continue parsing
                    }
                }
            }
            PivotFormatLoggerHelper.logParsingSuccessForEntity(
                    LOGGER,
                    PivotFormatLoggerHelper.TEST_SUITE,
                    testSuiteToImport.getName(),
                    testSuiteToImport.getInternalId(),
                    pivotFormatImport);
        } catch (Exception e) {
            PivotFormatLoggerHelper.handleParsingErrorForEntity(
                    LOGGER,
                    PivotFormatLoggerHelper.TEST_SUITE,
                    testSuiteToImport.getInternalId(),
                    pivotFormatImport,
                    e);
        }

        return testSuiteToImport;
    }

    @Override
    public ExecutionToImport parseExecution(
            JsonParser jsonParser,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport) {
        ExecutionToImport executionToImport = new ExecutionToImport();

        try {

            while (PivotJsonParsingHelper.isNotTheEndOfParsedObject(jsonParser)) {
                String fieldName = jsonParser.getCurrentName();
                jsonParser.nextToken();

                switch (fieldName) {
                    case JsonImportField.ID -> executionToImport.setInternalId(jsonParser.getText());
                    case JsonImportField.COMMENT -> executionToImport.setComment(jsonParser.getText());
                    case JsonImportField.STATUS ->
                            executionToImport.setExecutionStatus(ExecutionStatus.valueOf(jsonParser.getText()));
                    case JsonImportField.CUSTOM_FIELDS ->
                            executionToImport.setCustomFields(
                                    PivotJsonParsingHelper.getCustomFieldValues(
                                            jsonParser, pivotImportMetadata.getCustomFieldIdsMap()));
                    case JsonImportField.LAST_EXECUTED_BY ->
                            executionToImport.setLastExecutedBy(jsonParser.getText());
                    case JsonImportField.LAST_EXECUTED_ON ->
                            executionToImport.setLastExecutedOn(parseIso8601DateTime(jsonParser.getText()));
                    case JsonImportField.EXECUTION_STEPS ->
                            handleExecutionSteps(jsonParser, executionToImport, pivotImportMetadata);
                    case JsonImportField.TEST_CASE_ID ->
                            executionToImport.setTestCaseId(jsonParser.getText());
                    case JsonImportField.DATASET_ID -> executionToImport.setDatasetId(jsonParser.getText());
                    case JsonImportField.PARENT_ID -> executionToImport.setParentId(jsonParser.getText());
                    case JsonImportField.PARENT_TYPE ->
                            executionToImport.setParentType(EntityType.valueOf(jsonParser.getText()));
                    case JsonImportField.ATTACHMENTS ->
                            executionToImport.setAttachments(AttachmentParserHelper.parseAttachments(jsonParser));
                    default -> {
                        // continue parsing
                    }
                }
            }
            PivotFormatLoggerHelper.logParsingSuccessForUnnamedEntity(
                    LOGGER,
                    PivotFormatLoggerHelper.EXECUTION,
                    executionToImport.getInternalId(),
                    pivotFormatImport);
        } catch (Exception e) {
            PivotFormatLoggerHelper.handleParsingErrorForEntity(
                    LOGGER,
                    PivotFormatLoggerHelper.EXECUTION,
                    executionToImport.getInternalId(),
                    pivotFormatImport,
                    e);
        }
        return executionToImport;
    }

    private void handleExecutionSteps(
            JsonParser jsonParser, ExecutionToImport execution, PivotImportMetadata pivotImportMetadata)
            throws IOException, ParseException {
        List<ExecutionStepInfo> executionStepsInfo = new ArrayList<>();
        while (PivotJsonParsingHelper.isNotTheEndOfParsedArray(jsonParser)) {
            if (PivotJsonParsingHelper.isStartingToParseNewObject(jsonParser)) {
                parseExecutionStep(jsonParser, executionStepsInfo, pivotImportMetadata);
            }
        }
        execution.setExecutionSteps(executionStepsInfo);
    }

    private void parseExecutionStep(
            JsonParser jsonParser,
            List<ExecutionStepInfo> executionStepsInfo,
            PivotImportMetadata pivotImportMetadata)
            throws IOException, ParseException {
        ExecutionStepInfo executionStepInfo = new ExecutionStepInfo();
        while (PivotJsonParsingHelper.isNotTheEndOfParsedObject(jsonParser)) {
            String fieldName = jsonParser.getCurrentName();
            jsonParser.nextToken();
            switch (fieldName) {
                case JsonImportField.TEST_STEP_ID -> executionStepInfo.setTestStepId(jsonParser.getText());
                case JsonImportField.STATUS ->
                        executionStepInfo.setStatus(ExecutionStatus.valueOf(jsonParser.getText()));
                case JsonImportField.COMMENT -> executionStepInfo.setComment(jsonParser.getText());
                case JsonImportField.CUSTOM_FIELDS ->
                        executionStepInfo.setCustomFields(
                                PivotJsonParsingHelper.getCustomFieldValues(
                                        jsonParser, pivotImportMetadata.getCustomFieldIdsMap()));
                case JsonImportField.LAST_EXECUTED_BY ->
                        executionStepInfo.setLastExecutedBy(jsonParser.getText());
                case JsonImportField.LAST_EXECUTED_ON ->
                        executionStepInfo.setLastExecutedOn(parseIso8601DateTime(jsonParser.getText()));
                case JsonImportField.ATTACHMENTS ->
                        executionStepInfo.setAttachments(AttachmentParserHelper.parseAttachments(jsonParser));
                default -> {
                    // continue parsing
                }
            }
        }
        executionStepsInfo.add(executionStepInfo);
    }

    private Date parseIso8601Date(String date) throws ParseException {
        try {
            return DateUtils.parseIso8601Date(date);
        } catch (ParseException e) {
            LOGGER.error("Error while trying to parse date: {}", date);
            throw e;
        }
    }

    private Date parseIso8601DateTime(String date) throws ParseException {
        try {
            return DateUtils.parseIso8601DateTime(date);
        } catch (ParseException e) {
            LOGGER.error("Error while trying to parse date: {}", date);
            throw e;
        }
    }
}
