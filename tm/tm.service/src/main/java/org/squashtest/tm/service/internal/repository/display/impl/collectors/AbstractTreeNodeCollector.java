/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl.collectors;

import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.BOUND_TO_BLOCKING_MILESTONE;

import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.jooq.DSLContext;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.milestone.MilestoneStatus;
import org.squashtest.tm.service.internal.display.dto.CustomFieldValueDto;
import org.squashtest.tm.service.internal.display.dto.MilestoneDto;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TreeNodeCollector;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;

public abstract class AbstractTreeNodeCollector implements TreeNodeCollector {

    protected static final String PROJECT_ID_ALIAS = "projectId";
    protected static final String CHILD_COUNT_ALIAS = "CHILD_COUNT";
    protected static final String CUF_COLUMN_PREFIX = "CUF_COLUMN_";
    protected static final String NAME_ALIAS = "NAME";
    protected static final String MILESTONES_ALIAS = "MILESTONES";
    protected static final String DIRECT_BIND_MILESTONES_ALIAS = "DIRECT_MILESTONE_BIND";
    protected static final String INDIRECT_BIND_MILESTONES_ALIAS = "INDIRECT_MILESTONE_BIND";
    protected static final String ALL_MILESTONES_ALIAS = "ALL_MILESTONES";
    protected static final String CAMPAIGN_ID_ALIAS = "CAMPAIGN_ID";

    protected DSLContext dsl;

    protected CustomFieldValueDisplayDao customFieldValueDisplayDao;

    protected final ActiveMilestoneHolder activeMilestoneHolder;

    protected final MilestoneDisplayDao milestoneDisplayDao;

    protected AbstractTreeNodeCollector(
            DSLContext dsl,
            CustomFieldValueDisplayDao customFieldValueDisplayDao,
            ActiveMilestoneHolder activeMilestoneHolder,
            MilestoneDisplayDao milestoneDisplayDao) {
        this.dsl = dsl;
        this.customFieldValueDisplayDao = customFieldValueDisplayDao;
        this.activeMilestoneHolder = activeMilestoneHolder;
        this.milestoneDisplayDao = milestoneDisplayDao;
    }

    @Override
    public Map<Long, DataRow> collect(List<Long> ids) {
        if (Objects.isNull(ids) || ids.isEmpty()) {
            return new HashMap<>();
        }
        Map<Long, DataRow> dataRows = doCollect(ids);
        this.appendCustomFieldValues(ids, dataRows);
        this.appendIsBoundToBlockingMilestone(ids, dataRows);
        return dataRows;
    }

    private void appendIsBoundToBlockingMilestone(List<Long> ids, Map<Long, DataRow> dataRows) {
        Map<Long, List<MilestoneDto>> milestonesByBoundEntity =
                this.milestoneDisplayDao.getMilestonesByBoundEntity(ids, getHandledEntityType());
        dataRows.forEach(
                (id, dataRow) -> {
                    List<String> milestoneBlockingStatusNames =
                            MilestoneStatus.MILESTONE_BLOCKING_STATUSES.stream().map(Enum::name).toList();
                    List<MilestoneDto> milestones =
                            milestonesByBoundEntity.getOrDefault(id, Collections.emptyList());

                    boolean isBoundToBlockingMilestone =
                            milestones.stream()
                                    .anyMatch(
                                            milestone -> milestoneBlockingStatusNames.contains(milestone.getStatus()));
                    dataRow.addData(BOUND_TO_BLOCKING_MILESTONE, isBoundToBlockingMilestone);
                });
    }

    protected abstract Map<Long, DataRow> doCollect(List<Long> ids);

    protected void appendCustomFieldValues(List<Long> ids, Map<Long, DataRow> dataRows) {
        if (getHandledEntityType().isCufHolder()) {
            ListMultimap<Long, CustomFieldValueDto> customFieldValuesById =
                    customFieldValueDisplayDao.findCustomFieldValues(
                            getHandledEntityType().getBindableEntity(), ids);
            dataRows.forEach(
                    (id, dataRow) -> {
                        List<CustomFieldValueDto> customFieldValues = customFieldValuesById.get(id);
                        for (CustomFieldValueDto customFieldValue : customFieldValues) {
                            dataRow
                                    .getData()
                                    .put(
                                            CUF_COLUMN_PREFIX + customFieldValue.getCufId(), customFieldValue.getValue());
                        }
                    });
        }
    }

    protected void appendMilestonesByProject(Map<Long, DataRow> libraryRows) {
        Optional<Milestone> activeMilestone = this.activeMilestoneHolder.getActiveMilestone();
        if (activeMilestone.isPresent()) {
            Set<Long> projectIds =
                    libraryRows.values().stream().map(DataRow::getProjectId).collect(Collectors.toSet());
            Multimap<Long, Long> milestonesByProjectId =
                    this.milestoneDisplayDao.findMilestonesByProjectId(projectIds);
            libraryRows.forEach(
                    (aLong, dataRow) ->
                            dataRow.addData(MILESTONES_ALIAS, milestonesByProjectId.get(dataRow.getProjectId())));
        }
    }

    protected boolean mustAppendMilestone() {
        Optional<Milestone> activeMilestone = this.activeMilestoneHolder.getActiveMilestone();
        return activeMilestone.isPresent();
    }

    protected void appendMilestonesInheritedFromCampaign(Map<Long, DataRow> campaignDescendants) {
        if (mustAppendMilestone()) {
            doAppendMilestonesFromCampaigns(campaignDescendants);
        }
    }

    protected void doAppendMilestonesFromCampaigns(Map<Long, DataRow> campaignDescendants) {
        Map<Long, Long> campaignsIdByDescendantId = createCampaignByDescendantMap(campaignDescendants);
        Multimap<Long, MilestoneDto> milestonesByCampaignId =
                milestoneDisplayDao.findMilestonesByCampaignId(
                        new HashSet<>(campaignsIdByDescendantId.values()));
        campaignDescendants.forEach(
                (aLong, dataRow) -> {
                    Long campaignId = campaignsIdByDescendantId.get(aLong);
                    Collection<MilestoneDto> milestones = milestonesByCampaignId.get(campaignId);
                    Collection<Long> milestoneIds = milestones.stream().map(MilestoneDto::getId).toList();
                    Collection<String> milestoneStatuses =
                            milestones.stream().map(MilestoneDto::getStatus).toList();
                    dataRow.addData(MILESTONES_ALIAS, milestoneIds);
                    dataRow.addData("MILESTONE_STATUS", milestoneStatuses);
                });
    }

    protected Map<Long, Long> createCampaignByDescendantMap(Map<Long, DataRow> campaignDescendants) {
        return campaignDescendants.entrySet().stream()
                .collect(
                        Collectors.toMap(
                                Map.Entry::getKey,
                                entry -> (Long) entry.getValue().getData().get(CAMPAIGN_ID_ALIAS)));
    }
}
