/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.projectimporter.pivotimporter.parsers;

import com.fasterxml.jackson.core.JsonParser;
import org.squashtest.tm.domain.projectimporter.PivotFormatImport;
import org.squashtest.tm.service.internal.dto.projectimporter.PivotImportMetadata;
import org.squashtest.tm.service.internal.dto.projectimporter.campaignworkspace.CampaignToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.campaignworkspace.ExecutionToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.campaignworkspace.IterationToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.campaignworkspace.TestSuiteToImport;

public interface ExecutionWorkspaceParser {

    CampaignToImport parseCampaign(
            JsonParser jsonParser,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport);

    IterationToImport parseIteration(
            JsonParser jsonParser,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport);

    TestSuiteToImport parseTestSuite(
            JsonParser jsonParser,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport);

    ExecutionToImport parseExecution(
            JsonParser jsonParser,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport);
}
