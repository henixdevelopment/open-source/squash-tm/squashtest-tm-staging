/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementStatus;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.service.internal.repository.CustomRequirementVersionDao;
import org.squashtest.tm.service.internal.repository.JpaQueryString;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;

import static org.squashtest.tm.service.internal.repository.ParameterNames.VERSION_ID;

/**
 * @author Gregory Fouquet
 */
@Repository("squashtest.tm.repository.RequirementVersionDaoImpl")
@Primary
public class RequirementVersionDaoImpl implements CustomRequirementVersionDao {


    @PersistenceContext private EntityManager em;

    @Inject private ActiveMilestoneHolder activeMilestoneHolder;

    private Session currentSession() {
        return em.unwrap(Session.class);
    }

    @Override
    public Requirement findRequirementById(long requirementId) {
        return (Requirement) currentSession().load(Requirement.class, requirementId);
    }

    @Override
    public RequirementVersion findByRequirementIdAndMilestone(long requirementId) {

        Optional<Milestone> active = activeMilestoneHolder.getActiveMilestone();

        if (active.isPresent()) {
            Query q =
                    currentSession().getNamedQuery("requirementVersion.findVersionByRequirementAndMilestone");
            q.setParameter("requirementId", requirementId);
            q.setParameter("milestoneId", active.get().getId());
            return (RequirementVersion) q.uniqueResult();
        } else {
            Query q = currentSession().getNamedQuery("requirementVersion.findLatestRequirementVersion");
            q.setParameter("requirementId", requirementId);
            return (RequirementVersion) q.uniqueResult();
        }
    }

    @Override
    public Map<Long, RequirementStatus> findRequirementStatusesByVersionIds(
            Collection<Long> versionIds) {
        return em.createQuery(
                        "select rv.id, rv.requirement.resource.status from RequirementVersion rv where rv.id in :versionIds",
                        Object[].class)
                .setParameter("versionIds", versionIds)
                .getResultStream()
                .collect(Collectors.toMap(r -> (Long) r[0], r -> (RequirementStatus) r[1]));
    }

    @Override
    public List<RequirementVersion> loadForCoverageAddition(Collection<Long> versionIds) {
        return em.createQuery(
                        "select rv from RequirementVersion rv join fetch rv.requirement left join fetch rv.requirementVersionCoverages where rv.id in :versionIds",
                        RequirementVersion.class)
                .setParameter("versionIds", versionIds)
                .getResultList();
    }

    @Override
    public RequirementVersion loadForCoverageAddition(Long versionId) {
        return em.createQuery("""
                        select rv from RequirementVersion rv
                        join fetch rv.requirement
                        left join fetch rv.requirementVersionCoverages
                        where rv.id = :versionId""", RequirementVersion.class)
                .setParameter(VERSION_ID, versionId)
                .getSingleResult();
    }

    @Override
    public RequirementVersion findByRemoteSyncIdAndIssueKey(Long remoteSyncId, String issueKey) {
        return em.createQuery(JpaQueryString.FIND_REQ_BY_REMOTE_SYNC_AND_REMOTE_KEY, RequirementVersion.class)
                .setParameter("remoteSyncId", remoteSyncId)
                .setParameter("issueKey", issueKey)
                .getResultStream()
                .findFirst()
                .orElse(null);
    }
}
