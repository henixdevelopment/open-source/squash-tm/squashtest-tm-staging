/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase;

import static org.squashtest.tm.service.security.Authorizations.WRITE_TC_OR_ROLE_ADMIN;

import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.core.foundation.lang.Wrapped;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.testcase.ActionTestStep;
import org.squashtest.tm.domain.testcase.ExploratoryTestCase;
import org.squashtest.tm.domain.testcase.IsScriptedTestCaseVisitor;
import org.squashtest.tm.domain.testcase.KeywordTestCase;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.domain.testcase.ScriptedTestCase;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.domain.testcase.TestCaseVisitor;
import org.squashtest.tm.domain.testcase.TestStep;
import org.squashtest.tm.service.annotation.CheckBlockingMilestone;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.annotation.Ids;
import org.squashtest.tm.service.attachment.AttachmentManagerService;
import org.squashtest.tm.service.internal.batchimport.Batch;
import org.squashtest.tm.service.internal.batchimport.instruction.ParameterInstruction;
import org.squashtest.tm.service.internal.repository.ParameterDao;
import org.squashtest.tm.service.internal.repository.TestStepDao;
import org.squashtest.tm.service.internal.repository.loaders.testcase.TestCaseLoader;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.security.SecurityCheckableObject;
import org.squashtest.tm.service.testcase.DatasetModificationService;
import org.squashtest.tm.service.testcase.ParameterModificationService;

@Transactional
@Service("squashtest.tm.service.ParameterModificationService")
public class ParameterModificationServiceImpl implements ParameterModificationService {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(ParameterModificationServiceImpl.class);

    @Inject private ParameterDao parameterDao;

    @Inject private TestStepDao testStepDao;

    @Inject private TestCaseLoader testCaseLoader;

    @Inject private DatasetModificationService datasetModificationService;

    @Inject private PermissionEvaluationService permissionEvaluationService;

    @Inject private AttachmentManagerService attachmentManagerService;

    @PersistenceContext EntityManager entityManager;

    /** Returns the parameters that belongs to this test case only. */
    @Override
    @Transactional(readOnly = true)
    public List<Parameter> findOwnParameters(long testCaseId) {
        return parameterDao.findOwnParametersByTestCaseOrderedByName(testCaseId);
    }

    /** Returns the unordered parameters that belongs to this test case only. */
    @Override
    @Transactional(readOnly = true)
    public List<Parameter> findOwnOrderedParameters(long testCaseId) {
        return parameterDao.findOwnOrderedParametersByTestCase(testCaseId);
    }

    /**
     * Returns a list of parameters that either belongs to this test case, either belongs to test
     * cases being called by a call step that uses the parameter delegation mode.
     */
    @Override
    public List<Parameter> findAllParameters(long testCaseId) {

        Map<Long, List<Parameter>> parametersByTestCaseId =
                parameterDao.findAllParametersByTestCaseIds(Collections.singleton(testCaseId));

        return parametersByTestCaseId.getOrDefault(testCaseId, Collections.emptyList());
    }

    /**
     * @see ParameterModificationService#addNewParameterToTestCase(Parameter, long)
     */
    @PreAuthorize(WRITE_TC_OR_ROLE_ADMIN)
    @Override
    @CheckBlockingMilestone(entityType = TestCase.class)
    public Parameter addNewParameterToTestCase(Parameter parameter, @Id long testCaseId) {
        TestCase testCase =
                testCaseLoader.load(testCaseId, EnumSet.of(TestCaseLoader.Options.FETCH_PARAMETERS));
        return addNewParameterToTestCaseUnsecured(parameter, testCase);
    }

    @Override
    public Parameter addNewParameterToTestCaseUnsecured(Parameter parameter, TestCase testCase) {
        IsScriptedTestCaseVisitor testCaseVisitor = new IsScriptedTestCaseVisitor();
        if (testCaseVisitor.isScripted()) {
            throw new IllegalArgumentException("Cannot add parameters in a scripted test case.");
        }
        if (testCase != null) {
            testCase.accept(testCaseVisitor);
            createAttachmentIfNeeded(parameter, testCase);
            parameter.setTestCase(testCase);
            datasetModificationService.cascadeDatasetsUpdate(testCase.getId());
        } else {
            LOGGER.warn("Cannot add parameters because the test case is null.");
        }

        return parameter;
    }

    private void addNewParameterToTestCase(Parameter parameter, TestCase testCase) {
        createAttachmentIfNeeded(parameter, testCase);
        parameter.setTestCase(testCase);
        datasetModificationService.cascadeDatasetsUpdate(testCase.getId());
    }

    private void createAttachmentIfNeeded(Parameter parameter, TestCase testCase) {
        String description = parameter.getDescription();

        if (description == null || description.isEmpty()) {
            return;
        }

        String newDescription =
                attachmentManagerService.handleRichTextAttachments(
                        description, testCase.getAttachmentList());

        if (!newDescription.equals(description)) {
            parameter.setDescription(newDescription);
        }
    }

    /**
     * @see ParameterModificationService#addNewParameterToTestCase(Parameter, long)
     */
    @Override
    @CheckBlockingMilestone(entityType = Parameter.class)
    public void changeName(@Id long parameterId, String newName) {
        Parameter parameter = parameterDao.loadWithTestCase(parameterId);
        TestCase testCase = parameter.getTestCase();
        PermissionsUtils.checkPermission(
                permissionEvaluationService, new SecurityCheckableObject(testCase, "WRITE"));
        parameter.setName(newName);
    }

    /**
     * @see ParameterModificationService#changeDescription(long, String)
     */
    @Override
    @CheckBlockingMilestone(entityType = Parameter.class)
    public void changeDescription(@Id long parameterId, String newDescription) {

        Parameter parameter = parameterDao.getReferenceById(parameterId);
        parameter.setDescription(newDescription);
    }

    /**
     * @see ParameterModificationService#remove(Parameter)
     */
    @Override
    public void remove(Parameter parameter) {
        TestCase testCase = parameter.getTestCase();
        PermissionsUtils.checkPermission(
                permissionEvaluationService, new SecurityCheckableObject(testCase, "WRITE"));
        parameterDao.delete(parameter);
        testCase.reorderParametersAfterRemoval(parameter);
    }

    /**
     * @see ParameterModificationService#removeAllByTestCaseIds(List)
     */
    @Override
    public void removeAllByTestCaseIds(@Ids List<Long> testCaseIds) {
        // note : hibernate bulk delete don't care of cascade delete so we have to remove the values by
        // ourselves
        this.parameterDao.removeAllValuesByTestCaseIds(testCaseIds);
        this.parameterDao.removeAllByTestCaseIds(testCaseIds);
    }

    /**
     * @see ParameterModificationService#removeById(long)
     */
    @Override
    @CheckBlockingMilestone(entityType = Parameter.class)
    public void removeById(@Id long parameterId) {
        Optional<Parameter> optParameter = parameterDao.findById(parameterId);
        if (optParameter.isPresent()) {
            Parameter param = optParameter.get();
            remove(param);
        }
    }

    /**
     * @see ParameterModificationService#createParamsForStep(long)
     */
    @Override
    @CheckBlockingMilestone(entityType = TestStep.class)
    public void createParamsForStep(@Id long stepId) {
        TestStep step = testStepDao.findById(stepId);
        createParamsForStep(step);
    }

    /**
     * @see ParameterModificationService#createParamsForStep(TestStep)
     */
    @Override
    public void createParamsForStep(TestStep step) {
        LOGGER.debug("creating parameters for step #{}", step.getId());
        Set<String> parameterNames =
                new ParameterNamesFinder().findParametersNamesInActionAndExpectedResult(step);

        LOGGER.trace("discovered parameters in step : {} ", parameterNames);

        for (String name : parameterNames) {
            createParameterIfNotExists(name, step.getTestCase());
        }
    }

    /**
     * Will first check for a parameter of the given name in the test case. If there is none, will
     * create one. When a parameter is created, the datasets of the test case and it's calling test
     * cases will be updated in consequence.
     *
     * @param name : the name of the potential new Parameter
     * @param testCase : the testCase to add the potential new parameter to
     */
    private void createParameterIfNotExists(String name, TestCase testCase) {
        if (testCase != null) {
            Parameter parameter = testCase.findParameterByName(name);
            if (parameter == null) {
                LOGGER.debug("declaring new parameter '{}' in test case #{}", name, testCase.getId());
                parameter = new Parameter(name);
                addNewParameterToTestCase(parameter, testCase);
            }
        }
    }

    @Override
    public void createParamsForSteps(
            Map<Long, TestCase> testCaseById, List<Batch<ActionTestStep>> batchTestSteps) {
        for (Batch<ActionTestStep> batch : batchTestSteps) {
            TestCase testCase = testCaseById.get(batch.getTargetId());

            Set<String> parameterNames =
                    batch.getEntities().stream()
                            .flatMap(
                                    step ->
                                            new ParameterNamesFinder()
                                                    .findParametersNamesInActionAndExpectedResult(step).stream())
                            .collect(Collectors.toSet());

            for (String name : parameterNames) {
                Parameter parameter = testCase.findParameterByName(name);
                if (parameter == null) {
                    parameter = new Parameter(name);
                    parameter.setTestCase(testCase);
                }
            }
        }

        datasetModificationService.cascadeDatasetsUpdate(testCaseById.keySet());
    }

    /**
     * @see ParameterModificationService#isUsed(long)
     */
    @Override
    public boolean isUsed(long parameterId) {
        Parameter parameter = parameterDao.loadWithTestCase(parameterId);
        long testCaseId = parameter.getTestCase().getId();
        Wrapped<Boolean> isUsed = new Wrapped<>();
        TestCaseVisitor visitor =
                new TestCaseVisitor() {
                    @Override
                    public void visit(TestCase testCase) {
                        isUsed.setValue(
                                testStepDao.stringIsFoundInStepsOfTestCase(
                                        parameter.getParamStringAsUsedInStep(), testCaseId));
                    }

                    @Override
                    public void visit(KeywordTestCase keywordTestCase) {
                        isUsed.setValue(
                                testStepDao.stringIsFoundInStepsOfKeywordTestCase(
                                        parameter.getParamStringAsUsedInStep(), testCaseId));
                    }

                    @Override
                    public void visit(ScriptedTestCase scriptedTestCase) {
                        throw new IllegalArgumentException("Scripted Test Case doesn't have any parameter.");
                    }

                    @Override
                    public void visit(ExploratoryTestCase exploratoryTestCase) {
                        throw new IllegalArgumentException("Exploratory Test Case doesn't have any parameter.");
                    }
                };

        parameter.getTestCase().accept(visitor);
        return isUsed.getValue();
    }

    /**
     * @see ParameterModificationService#findById(long)
     */
    @Override
    public Parameter findById(long parameterId) {
        return parameterDao.getReferenceById(parameterId);
    }

    @Override
    public List<Parameter> findByIds(List<Long> parameterIds) {
        return parameterDao.findAllById(parameterIds);
    }

    /**
     * @see ParameterModificationService#createParamsForTestCaseSteps(TestCase)
     */
    @Override
    public void createParamsForTestCaseSteps(TestCase testCase) {
        for (TestStep step : testCase.getActionSteps()) {
            createParamsForStep(step);
        }
    }

    @Override
    public Map<Long, List<Parameter>> findOwnParametersByTestCaseIds(Collection<Long> testCaseIds) {
        return parameterDao.findOwnParametersByTestCaseIds(testCaseIds);
    }

    @Override
    public void batchParameterAddition(List<Batch<Parameter>> parameterBatches) {
        List<Long> testCaseIds = parameterBatches.stream().map(Batch::getTargetId).toList();

        Map<Long, TestCase> testCases =
                testCaseLoader
                        .load(testCaseIds, EnumSet.of(TestCaseLoader.Options.FETCH_PARAMETERS))
                        .stream()
                        .collect(Collectors.toMap(TestCaseLibraryNode::getId, Function.identity()));

        for (Batch<Parameter> batch : parameterBatches) {
            TestCase testCase = testCases.get(batch.getTargetId());
            IsScriptedTestCaseVisitor testCaseVisitor = new IsScriptedTestCaseVisitor();
            if (testCaseVisitor.isScripted()) {
                throw new IllegalArgumentException("Cannot add parameters in a scripted test case.");
            }
            testCase.accept(testCaseVisitor);
            for (Parameter parameter : batch.getEntities()) {
                parameter.setTestCase(testCase);
            }
        }

        datasetModificationService.cascadeDatasetsUpdate(testCaseIds);

        entityManager.flush();
        entityManager.clear();
    }

    @Override
    public void updateImportParameters(List<Batch<ParameterInstruction>> batchList) {
        List<Long> testCaseIds = batchList.stream().map(Batch::getTargetId).toList();

        Map<Long, List<Parameter>> parametersByTestCaseId =
                parameterDao.findParametersByTestCaseIds(testCaseIds);

        for (Batch<ParameterInstruction> batch : batchList) {
            Map<String, Parameter> parameterByName =
                    parametersByTestCaseId.getOrDefault(batch.getTargetId(), Collections.emptyList()).stream()
                            .collect(Collectors.toMap(Parameter::getName, param -> param));

            for (ParameterInstruction instruction : batch.getEntities()) {
                String description = instruction.getParameter().getDescription();

                if (description == null) {
                    continue;
                }

                Parameter parameter = parameterByName.get(instruction.getTarget().getName());

                if (parameter == null) {
                    throw new NoSuchElementException(
                            "parameter " + instruction.getTarget() + " could not be found");
                }

                parameter.setDescription(description);
            }
        }

        entityManager.flush();
        entityManager.clear();
    }
}
