/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.execution;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.service.display.execution.AutomatedExecutionDisplayService;
import org.squashtest.tm.service.internal.display.dto.FailureDetailDto;
import org.squashtest.tm.service.internal.repository.display.AutomatedExecutionDisplayDao;
import org.squashtest.tm.service.plugin.PluginFinderService;

@Service
@Transactional(readOnly = true)
public class AutomatedExecutionDisplayServiceImpl implements AutomatedExecutionDisplayService {

    private final AutomatedExecutionDisplayDao automatedExecutionDisplayDao;

    private final PluginFinderService pluginFinderService;

    public AutomatedExecutionDisplayServiceImpl(
            AutomatedExecutionDisplayDao automatedExecutionDisplayDao,
            PluginFinderService pluginFinderService) {
        this.automatedExecutionDisplayDao = automatedExecutionDisplayDao;
        this.pluginFinderService = pluginFinderService;
    }

    @Override
    public List<FailureDetailDto> findFailureDetailList(Long executionExtenderId) {
        if (pluginFinderService.isPremiumPluginInstalled()) {
            return automatedExecutionDisplayDao.findAllFailureDetailsByAutomatedExecutionExtenderId(
                    executionExtenderId);
        }
        return new ArrayList<>();
    }
}
