/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportField;

public abstract class AbstractPivotEntity {
    @JsonProperty(JsonImportField.ID)
    protected String pivotId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(JsonImportField.PARENT_ID)
    protected String parentId;

    @JsonProperty(JsonImportField.CUSTOM_FIELDS)
    protected List<String> customFields = new ArrayList<>(); // Not supported yet

    @JsonIgnore protected BindableEntity bindableEntity;

    public String getPivotId() {
        return pivotId;
    }

    public void setPivotId(String pivotId) {
        this.pivotId = pivotId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public List<String> getCustomFields() {
        return customFields;
    }

    public void setCustomFields(List<String> customFields) {
        this.customFields = customFields;
    }

    public BindableEntity getBindableEntity() {
        return bindableEntity;
    }
}
