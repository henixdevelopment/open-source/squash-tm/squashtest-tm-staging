/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.jooq.domain.Tables.SESSION_NOTE;

import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.execution.ExploratoryExecution;
import org.squashtest.tm.domain.execution.SessionNote;
import org.squashtest.tm.domain.execution.SessionNoteKind;
import org.squashtest.tm.service.internal.repository.SessionNoteDao;

@Repository
public class SessionNoteDaoImpl extends HibernateEntityDao<SessionNote> implements SessionNoteDao {

    private final DSLContext dslContext;

    public SessionNoteDaoImpl(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    @Override
    public Long createSessionNote(
            long executionId, SessionNoteKind noteKind, String noteContent, Integer noteOrder) {
        ExploratoryExecution execution = entityManager.find(ExploratoryExecution.class, executionId);
        SessionNote sessionNote = new SessionNote();
        sessionNote.setKind(noteKind);
        sessionNote.setContent(noteContent);
        sessionNote.setExecution(execution);
        execution.addSessionNote(sessionNote, noteOrder);

        flush();
        return sessionNote.getId();
    }

    @Override
    public void updateSessionNoteKind(long noteId, SessionNoteKind kind) {
        SessionNote sessionNote = findById(noteId);
        sessionNote.setKind(kind);
    }

    @Override
    public void updateSessionNoteContent(long noteId, String content) {
        SessionNote sessionNote = findById(noteId);
        sessionNote.setContent(content);
    }

    @Override
    public void deleteSessionNote(long noteId) {
        SessionNote sessionNote = findById(noteId);
        sessionNote.getExecution().removeSessionNote(sessionNote);
        entityManager.remove(sessionNote);
    }

    @Override
    public Long findExploratoryExecutionId(long noteId) {
        return dslContext
                .select(SESSION_NOTE.EXECUTION_ID)
                .from(SESSION_NOTE)
                .where(SESSION_NOTE.NOTE_ID.eq(noteId))
                .fetchOneInto(Long.class);
    }
}
