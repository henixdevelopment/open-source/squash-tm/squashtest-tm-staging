/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.execution.automatedexecution;

import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.bugtracker.Issue;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.domain.testautomation.FailureDetail;
import org.squashtest.tm.exception.UnknownEntityException;
import org.squashtest.tm.security.UserContextHolder;
import org.squashtest.tm.service.execution.automatedexecution.AutomatedExecutionFailureDetailService;
import org.squashtest.tm.service.internal.repository.ExecutionDao;
import org.squashtest.tm.service.internal.repository.FailureDetailDao;
import org.squashtest.tm.service.internal.repository.IssueDao;
import org.squashtest.tm.service.internal.repository.ProjectDao;
import org.squashtest.tm.service.internal.repository.display.FailureDetailDisplayDao;

@Service
@Transactional
public class AutomatedExecutionFailureDetailServiceImpl
        implements AutomatedExecutionFailureDetailService {

    private final FailureDetailDao failureDetailDao;

    private final FailureDetailDisplayDao failureDetailDisplayDao;

    private final IssueDao issueDao;

    private final ProjectDao projectDao;

    private final ExecutionDao executionDao;

    public AutomatedExecutionFailureDetailServiceImpl(
            FailureDetailDao failureDetailDao,
            FailureDetailDisplayDao failureDetailDisplayDao,
            IssueDao issueDao,
            ProjectDao projectDao,
            ExecutionDao executionDao) {
        this.failureDetailDao = failureDetailDao;
        this.failureDetailDisplayDao = failureDetailDisplayDao;
        this.issueDao = issueDao;
        this.projectDao = projectDao;
        this.executionDao = executionDao;
    }

    @Override
    public void upsertFailureDetail(
            String failureDetailMessage,
            List<FailureDetail> existingFailureDetailList,
            AutomatedExecutionExtender extender,
            IterationTestPlanItem itpi) {
        FailureDetail matchingFailureDetail =
                existingFailureDetailList.stream()
                        .filter(fd -> fd.getMessage().equals(failureDetailMessage))
                        .findFirst()
                        .orElse(null);

        if (matchingFailureDetail != null) {
            matchingFailureDetail.addExecutionExtender(extender);
            FailureDetail fd = failureDetailDao.save(matchingFailureDetail);
            addFailureDetailIssuesToNewExecution(fd, extender.getExecution());
        } else {
            String userLogin = UserContextHolder.getUsername();
            FailureDetail failureDetail =
                    new FailureDetail(failureDetailMessage, userLogin, new Date(), itpi);
            failureDetail.addExecutionExtender(extender);
            failureDetailDao.save(failureDetail);
        }
    }

    private void addFailureDetailIssuesToNewExecution(
            FailureDetail failureDetail, Execution execution) {
        if (failureDetail.getIssueList().getAllIssues() == null
                | failureDetail.getIssueList().getAllIssues().isEmpty()) {
            return;
        }
        List<Issue> newExecIssues =
                failureDetail.getIssueList().getAllIssues().stream()
                        .map(fdIssue -> fdIssue.duplicateFailureDetailExecutionForExecution(execution))
                        .toList();
        issueDao.saveAll(newExecIssues);
    }

    @Override
    public Project findProjectByFailureDetailId(Long failureDetailId) {
        Long projectId = failureDetailDisplayDao.findProjectIdByFailureDetailId(failureDetailId);
        if (projectId == null) {
            throw new UnknownEntityException(failureDetailId, FailureDetail.class);
        }
        return projectDao.findById(projectId).orElseThrow();
    }

    @Override
    public FailureDetail findFailureDetailById(Long failureDetailId) {
        return failureDetailDao.findById(failureDetailId).orElse(null);
    }

    @Override
    public List<Execution> findExecutionsByFailureDetailAndDateAfter(
            Long failureDetailId, Date createdOnDate) {
        return executionDao.findExecutionsByFailureDetailAndDateAfter(failureDetailId, createdOnDate);
    }
}
