/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchexport.models;

import java.util.Comparator;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.squashtest.tm.domain.requirement.RequirementVersionLinkType;

public final class RequirementLinkModel {

    public static final Comparator<RequirementLinkModel> REQ_LINK_COMPARATOR =
            new Comparator<RequirementLinkModel>() {

                @Override
                public int compare(RequirementLinkModel o1, RequirementLinkModel o2) {

                    int comp = o1.getReqPath().compareTo(o2.getReqPath());
                    if (comp != 0) {
                        return comp;
                    }

                    comp = o1.getReqVersion() - o2.getReqVersion();
                    if (comp != 0) {
                        return comp;
                    }

                    comp = o1.getRelReqPath().compareTo(o2.getRelReqPath());
                    if (comp != 0) {
                        return comp;
                    }

                    comp = o1.getRelReqVersion() - o2.getRelReqVersion();
                    return comp;
                }
            };

    // those attributes will be set by Hibernate (they are part of the constructor)

    /**
     * Id of the requirement for the requirement version under consideration. Not exported in the
     * final excel file, but useful to resolve the requirement path later on.
     */
    private Long reqId;

    /**
     * Id of the requirement for the related requirement version. Not exported in the final excel
     * file, but useful to resolve the requirement path later on.
     */
    private Long relReqId;

    /** The version number of the requirement version under consideration */
    private int reqVersion;

    /** The version number of the related requirement. */
    private int relReqVersion;

    /**
     * The role of the related requirement version. Corresponds to {@link
     * RequirementVersionLinkType#getRole2Code()}.
     */
    private String relatedReqRole;

    /**
     * The path of the requirement of the requirement version under consideration. Will not be
     * extracted directly from the database, must be set manually.
     */
    private String reqPath;

    /**
     * The path of the requirement of the related requirement version. Will not be extracted directly
     * from the database, must be set manually.
     */
    private String relReqPath;

    /**
     * Constuctor used by Hibernate
     *
     * @param reqId
     * @param relReqId
     * @param reqVersion
     * @param relReqVersion
     * @param relatedReqRole
     */
    public RequirementLinkModel(
            Long reqId, Long relReqId, int reqVersion, int relReqVersion, String relatedReqRole) {
        super();
        this.reqId = reqId;
        this.relReqId = relReqId;
        this.reqVersion = reqVersion;
        this.relReqVersion = relReqVersion;
        this.relatedReqRole = relatedReqRole;
    }

    /**
     * factory method for test purposes
     *
     * @return
     */
    public static RequirementLinkModel create(
            int reqVersion, int relReqVersion, String relatedReqRole, String reqPath, String relReqPath) {

        RequirementLinkModel model =
                new RequirementLinkModel(null, null, reqVersion, relReqVersion, relatedReqRole);

        model.setReqPath(reqPath);
        model.setRelReqPath(relReqPath);

        return model;
    }

    public String getReqPath() {
        return reqPath;
    }

    public void setReqPath(String reqPath) {
        this.reqPath = reqPath;
    }

    public String getRelReqPath() {
        return relReqPath;
    }

    public void setRelReqPath(String relReqPath) {
        this.relReqPath = relReqPath;
    }

    public Long getReqId() {
        return reqId;
    }

    public Long getRelReqId() {
        return relReqId;
    }

    public int getReqVersion() {
        return reqVersion;
    }

    public int getRelReqVersion() {
        return relReqVersion;
    }

    public String getRelatedReqRole() {
        return relatedReqRole;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("req-id", relReqId)
                .append("req-path", reqPath)
                .append("req-version", reqVersion)
                .append("rel-req-id", relReqId)
                .append("rel-req-path", relReqPath)
                .append("rel-req-version", relReqVersion)
                .append("rel-req-role", relatedReqRole)
                .toString();
    }
}
