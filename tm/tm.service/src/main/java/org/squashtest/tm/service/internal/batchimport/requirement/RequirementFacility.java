/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.requirement;

import static org.squashtest.tm.service.internal.repository.hibernate.utils.HibernateConfig.BATCH_10;
import static org.squashtest.tm.service.internal.repository.hibernate.utils.HibernateConfig.BATCH_30;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.squashtest.tm.core.foundation.lang.PathUtils;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.audit.AuditableMixin;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.infolist.InfoListItem;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementCriticality;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.requirement.RequirementLibraryNode;
import org.squashtest.tm.domain.requirement.RequirementLibraryNodeVisitor;
import org.squashtest.tm.domain.requirement.RequirementStatus;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.service.clipboard.model.ClipboardPayload;
import org.squashtest.tm.service.infolist.InfoListItemFinderService;
import org.squashtest.tm.service.internal.batchimport.AbstractEntityFacilitySupport;
import org.squashtest.tm.service.internal.batchimport.Batch;
import org.squashtest.tm.service.internal.batchimport.Batches;
import org.squashtest.tm.service.internal.batchimport.FacilityImpl;
import org.squashtest.tm.service.internal.batchimport.FacilityImplHelper;
import org.squashtest.tm.service.internal.batchimport.LogTrain;
import org.squashtest.tm.service.internal.batchimport.TargetStatus;
import org.squashtest.tm.service.internal.batchimport.ValidationFacility;
import org.squashtest.tm.service.internal.batchimport.column.testcase.CoverageInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.LinkedLowLevelRequirementInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.RequirementLinkInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.RequirementVersionInstruction;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.LinkedLowLevelRequirementTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.RequirementLinkTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.RequirementTarget;
import org.squashtest.tm.service.internal.batchimport.instruction.targets.RequirementVersionTarget;
import org.squashtest.tm.service.internal.batchimport.requirement.dto.CoverageImportDto;
import org.squashtest.tm.service.internal.batchimport.requirement.dto.ExistLinkImportDto;
import org.squashtest.tm.service.internal.batchimport.requirement.dto.LinkImportDto;
import org.squashtest.tm.service.internal.batchimport.requirement.dto.RequirementFolderImportDto;
import org.squashtest.tm.service.internal.batchimport.requirement.dto.RequirementImportDto;
import org.squashtest.tm.service.internal.batchimport.requirement.dto.RequirementVersionImportDto;
import org.squashtest.tm.service.internal.batchimport.requirement.tree.ImportedRequirementNode;
import org.squashtest.tm.service.internal.library.LibraryUtils;
import org.squashtest.tm.service.internal.repository.hibernate.HibernateRequirementLibraryNodeDao;
import org.squashtest.tm.service.milestone.MilestoneMembershipManager;
import org.squashtest.tm.service.requirement.HighLevelRequirementService;
import org.squashtest.tm.service.requirement.LinkedRequirementVersionManagerService;
import org.squashtest.tm.service.requirement.RequirementLibraryNavigationService;
import org.squashtest.tm.service.requirement.RequirementVersionManagerService;

/**
 * @author Gregory Fouquet
 * @since 1.14.0 04/05/16
 */
@Component
@Scope("prototype")
public class RequirementFacility extends AbstractEntityFacilitySupport {

    private static final Logger LOGGER = LoggerFactory.getLogger(FacilityImpl.class);

    private final HibernateRequirementLibraryNodeDao rlnDao;
    private final InfoListItemFinderService listItemFinderService;
    private final RequirementVersionManagerService requirementVersionManagerService;
    private final RequirementLibraryNavigationService reqLibNavigationService;
    private final LinkedRequirementVersionManagerService reqlinkService;
    private final HighLevelRequirementService highLevelRequirementService;
    private final RequirementImportService requirementImportService;

    private final FacilityImplHelper helper = new FacilityImplHelper(this);

    RequirementFacility(
            HibernateRequirementLibraryNodeDao rlnDao,
            InfoListItemFinderService listItemFinderService,
            RequirementVersionManagerService requirementVersionManagerService,
            RequirementLibraryNavigationService reqLibNavigationService,
            LinkedRequirementVersionManagerService reqlinkService,
            HighLevelRequirementService highLevelRequirementService,
            RequirementImportService requirementImportService) {
        this.rlnDao = rlnDao;
        this.listItemFinderService = listItemFinderService;
        this.requirementVersionManagerService = requirementVersionManagerService;
        this.reqLibNavigationService = reqLibNavigationService;
        this.reqlinkService = reqlinkService;
        this.highLevelRequirementService = highLevelRequirementService;
        this.requirementImportService = requirementImportService;
    }

    public void createRequirementVersions(
            List<RequirementVersionInstruction> requirementsCreation,
            List<RequirementVersionInstruction> versionsAddition,
            Project project) {
        createRequirements(requirementsCreation, project);
        addNewVersions(versionsAddition, project);
    }

    private void addNewVersions(List<RequirementVersionInstruction> instructions, Project project) {
        if (instructions.isEmpty()) {
            return;
        }

        Batches<RequirementVersionImportDto> batches = new Batches<>();

        for (RequirementVersionInstruction instruction : instructions) {
            Long reqId = model.getRequirementId(instruction.getTarget());
            batches.addBatch(reqId, asRequirementVersionDto(instruction));
        }

        for (List<Batch<RequirementVersionImportDto>> batchList : batches.partition(BATCH_30)) {
            List<Long> requirementIds = batchList.stream().map(Batch::getTargetId).toList();

            requirementImportService.addVersions(requirementIds, project, batchList);
        }

        instructions.forEach(this::validatingRequirementVersionCreation);
    }

    private void createRequirements(
            List<RequirementVersionInstruction> instructions, Project project) {
        if (instructions.isEmpty()) {
            return;
        }

        Map<ImportedRequirementNode, Set<ImportedRequirementNode>> missingNodes =
                model.getMissingNodes();

        if (missingNodes.isEmpty()) {
            return;
        }

        Map<RequirementTarget, List<RequirementVersionImportDto>> requirementTargetVersions =
                instructions.stream()
                        .collect(
                                Collectors.groupingBy(
                                        i -> i.getTarget().getRequirement(),
                                        Collectors.mapping(
                                                this::asRequirementVersionDto,
                                                Collectors.collectingAndThen(
                                                        Collectors.toList(),
                                                        list -> {
                                                            list.sort(
                                                                    Comparator.comparing(RequirementVersionImportDto::getVersion));
                                                            filterMilestones(list);
                                                            return list;
                                                        }))));

        createRequirements(missingNodes, requirementTargetVersions, project);
        createFolders(missingNodes, requirementTargetVersions, project);

        instructions.forEach(this::validatingRequirementVersionCreation);
    }

    private void validatingRequirementVersionCreation(RequirementVersionInstruction instruction) {
        RequirementVersionTarget target = instruction.getTarget();
        RequirementVersion version = instruction.getRequirementVersion();
        RequirementTarget requirementTarget = target.getRequirement();

        if (requirementTarget.getId() == null) {
            model.setExists(requirementTarget, version.getRequirement().getId());
        }

        model.setExists(target, version.getId());
    }

    private void filterMilestones(List<RequirementVersionImportDto> list) {
        Set<Long> milestoneIds = new HashSet<>();
        for (RequirementVersionImportDto dto : list) {
            Iterator<Long> iterator = dto.milestoneIds().iterator();
            while (iterator.hasNext()) {
                Long milestone = iterator.next();
                if (milestoneIds.contains(milestone)) {
                    iterator.remove();
                } else {
                    milestoneIds.add(milestone);
                }
            }
        }
    }

    private void createFolders(
            Map<ImportedRequirementNode, Set<ImportedRequirementNode>> missingNodes,
            Map<RequirementTarget, List<RequirementVersionImportDto>> versionDtoByRequirementTarget,
            Project project) {
        Batch<RequirementFolderImportDto> libraryChild =
                new Batch<>(project.getRequirementLibrary().getId());
        Batches<RequirementFolderImportDto> folderChild = new Batches<>();

        missingNodes.forEach(
                (parentNode, childrenNode) -> {
                    for (ImportedRequirementNode node : childrenNode) {
                        if (node.isRequirement()) {
                            continue;
                        }

                        RequirementFolderImportDto folderDto =
                                RequirementFolderImportDto.fromRequirementNode(
                                        node, versionDtoByRequirementTarget, project);

                        Long parentId = parentNode.getId();

                        if (parentNode.isLibrary()) {
                            libraryChild.addEntity(folderDto);
                        } else if (parentNode.isRequirementFolder()) {
                            folderChild.addBatch(parentId, folderDto);
                        }
                    }
                });

        createLibraryFolders(libraryChild, project);
        createSubFolders(folderChild, project);
    }

    private void createSubFolders(Batches<RequirementFolderImportDto> batches, Project project) {
        if (batches.isEmpty()) {
            return;
        }

        fixBatchFoldersConflictNames(batches);

        for (List<Batch<RequirementFolderImportDto>> batchList : batches.partition(BATCH_30)) {
            List<Long> folderIds = batchList.stream().map(Batch::getTargetId).toList();

            requirementImportService.addFoldersToFolders(folderIds, project, batchList);
        }
    }

    private void createLibraryFolders(Batch<RequirementFolderImportDto> batch, Project project) {
        if (batch.isEmpty()) {
            return;
        }

        List<RequirementFolderImportDto> folderDtoList = batch.getEntities();

        Long libraryId = batch.getTargetId();

        List<String> contentNames = reqLibNavigationService.findContentNamesByLibraryId(libraryId);

        fixFolderConflictNames(contentNames, folderDtoList);

        requirementImportService.addFoldersToLibrary(libraryId, project, batch.getEntities());
    }

    private void createRequirements(
            Map<ImportedRequirementNode, Set<ImportedRequirementNode>> missingNodes,
            Map<RequirementTarget, List<RequirementVersionImportDto>> requirementTargetVersions,
            Project project) {

        Batch<RequirementImportDto> libraryChild = new Batch<>(project.getRequirementLibrary().getId());
        Batches<RequirementImportDto> folderChild = new Batches<>();
        Batches<RequirementImportDto> requirementChild = new Batches<>();

        missingNodes.forEach(
                (parentNode, childrenNode) -> {
                    for (ImportedRequirementNode node : childrenNode) {
                        if (!node.isRequirement()) {
                            continue;
                        }

                        RequirementImportDto requirementDto =
                                RequirementImportDto.fromRequirementNode(node, requirementTargetVersions, project);

                        Long parentId = parentNode.getId();

                        if (parentNode.isLibrary()) {
                            libraryChild.addEntity(requirementDto);
                        } else if (parentNode.isRequirementFolder()) {
                            folderChild.addBatch(parentId, requirementDto);
                        } else if (parentNode.isRequirement()) {
                            requirementChild.addBatch(parentId, requirementDto);
                        }
                    }
                });

        createLibraryRequirements(project, libraryChild);
        createFolderRequirements(project, folderChild);
        createSubRequirements(project, requirementChild);
    }

    private void createSubRequirements(Project project, Batches<RequirementImportDto> batches) {
        if (batches.isEmpty()) {
            return;
        }

        fixBatchConflictNames(batches);

        for (List<Batch<RequirementImportDto>> batchList : batches.partition(BATCH_30)) {
            List<Long> requirementIds = batchList.stream().map(Batch::getTargetId).toList();

            requirementImportService.addRequirementsToRequirements(requirementIds, project, batchList);
        }
    }

    private void createFolderRequirements(Project project, Batches<RequirementImportDto> batches) {
        if (batches.isEmpty()) {
            return;
        }

        fixBatchConflictNames(batches);

        for (List<Batch<RequirementImportDto>> batchList : batches.partition(BATCH_30)) {
            List<Long> folderIds = batchList.stream().map(Batch::getTargetId).toList();

            requirementImportService.addRequirementsToFolders(folderIds, project, batchList);
        }
    }

    private void createLibraryRequirements(Project project, Batch<RequirementImportDto> batch) {
        if (batch.isEmpty()) {
            return;
        }

        List<RequirementImportDto> requirementDtoList = batch.getEntities();

        Long libraryId = batch.getTargetId();

        List<String> contentNames = reqLibNavigationService.findContentNamesByLibraryId(libraryId);

        fixConflictNames(contentNames, requirementDtoList);

        requirementImportService.addRequirementsToLibrary(libraryId, project, batch.getEntities());
    }

    private void fixBatchConflictNames(Batches<RequirementImportDto> batches) {
        Map<Long, List<String>> folderContentNames =
                reqLibNavigationService.findContentNamesByNodeIds(batches.getTargetIds());

        folderContentNames.forEach(
                (folderId, contentNames) -> {
                    List<RequirementImportDto> importDtoList = batches.getEntitiesByTargetId(folderId);
                    fixConflictNames(contentNames, importDtoList);
                });
    }

    private static void fixConflictNames(
            Collection<String> contentNames, List<RequirementImportDto> requirementsDto) {
        if (requirementsDto == null || requirementsDto.isEmpty()) {
            return;
        }

        requirementsDto.forEach(
                dto -> LibraryUtils.fixConflictNames(contentNames, dto.getRequirement()));
    }

    private void fixBatchFoldersConflictNames(Batches<RequirementFolderImportDto> batches) {
        Map<Long, List<String>> folderContentNames =
                reqLibNavigationService.findContentNamesByNodeIds(batches.getTargetIds());

        folderContentNames.forEach(
                (folderId, contentNames) -> {
                    List<RequirementFolderImportDto> foldersDto = batches.getEntitiesByTargetId(folderId);
                    fixFolderConflictNames(contentNames, foldersDto);
                });
    }

    private static void fixFolderConflictNames(
            Collection<String> contentNames, List<RequirementFolderImportDto> foldersDto) {
        if (foldersDto == null || foldersDto.isEmpty()) {
            return;
        }

        foldersDto.forEach(dto -> LibraryUtils.fixConflictNames(contentNames, dto.getFolder()));
    }

    private RequirementVersionImportDto asRequirementVersionDto(
            RequirementVersionInstruction instruction) {
        RequirementVersion requirementVersion = instruction.getRequirementVersion();
        RequirementVersionTarget target = instruction.getTarget();

        requirementVersion.setVersionNumber(target.getVersion());

        Map<String, String> cufValues = instruction.getCustomFields();

        helper.fillNullWithDefaults(requirementVersion);
        helper.truncate(requirementVersion, cufValues);

        Map<Long, RawValue> acceptableCufs = toAcceptableCufs(instruction.getCustomFields());

        List<Long> milestoneIds = boundMilestonesIds(instruction);

        return new RequirementVersionImportDto(
                requirementVersion,
                acceptableCufs,
                milestoneIds,
                target.getRequirement().isHighLevel(),
                target.getImportedRequirementStatus());
    }

    /**
     * This method ensure that multiple milestone binding to several {@link RequirementVersion} of the
     * same {@link Requirement} is forbidden. The method in service can't prevent this for import as
     * we are in a unique transaction for all import lines. So the n-n relationship between milestones
     * and requirementVersion isn't fixed until transaction is closed and {@link
     * MilestoneMembershipManager#bindRequirementVersionToMilestones(long, Collection)} will let
     * horrible things appends if this list isn't up to date
     */
    private void bindRequirementVersionToMilestones(
            RequirementVersion requirementVersionPersisted, List<Long> boundMilestonesIds) {
        List<RequirementVersion> allVersion =
                requirementVersionPersisted.getRequirement().getRequirementVersions();
        Set<Milestone> milestoneBinded = new HashSet<>();
        Set<Long> milestoneBindedId = new HashSet<>();
        Set<Long> checkedMilestones = new HashSet<>();

        for (RequirementVersion requirementVersion : allVersion) {
            milestoneBinded.addAll(requirementVersion.getMilestones());
        }

        for (Milestone milestone : milestoneBinded) {
            milestoneBindedId.add(milestone.getId());
        }

        for (Long id : boundMilestonesIds) {
            if (!milestoneBindedId.contains(id)) {
                checkedMilestones.add(id);
            }
        }

        if (!checkedMilestones.isEmpty()) {
            requirementVersionPersisted.getMilestones().clear();
            requirementVersionManagerService.bindMilestones(
                    requirementVersionPersisted.getId(), checkedMilestones);
        }
    }

    private void doUpdateRequirementCoreAttributes(
            RequirementVersion reqVersion, RequirementVersion orig) {
        doUpdateRequirementReference(reqVersion, orig);
        doUpdateRequirementDescription(reqVersion, orig);
        doUpdateRequirementCriticality(reqVersion, orig);
        doUpdateRequirementCategory(reqVersion, orig);
    }

    private void doUpdateStatus(RequirementStatus newStatus, RequirementVersion orig) {
        if (orig.getStatus().isRequirementModifiable()
                && newStatus != null
                && newStatus != orig.getStatus()) {
            orig.updateStatusWithoutCheck(newStatus);
        }
    }

    private void doUpdateRequirementReference(
            RequirementVersion reqVersion, RequirementVersion orig) {
        String newReference = reqVersion.getReference();
        if (!StringUtils.isBlank(newReference) && !newReference.equals(orig.getReference())) {
            requirementVersionManagerService.changeReference(orig.getId(), newReference);
        }
    }

    private void doUpdateRequirementDescription(
            RequirementVersion reqVersion, RequirementVersion orig) {
        String newDescription = reqVersion.getDescription();
        if (!StringUtils.isBlank(newDescription) && !newDescription.equals(orig.getDescription())) {
            requirementVersionManagerService.changeDescription(orig.getId(), newDescription);
        }
    }

    private void doUpdateRequirementCriticality(
            RequirementVersion reqVersion, RequirementVersion orig) {
        RequirementCriticality newCriticality = reqVersion.getCriticality();
        if (newCriticality != null && newCriticality != orig.getCriticality()) {
            requirementVersionManagerService.changeCriticality(orig.getId(), newCriticality);
        }
    }

    private void doUpdateRequirementCategory(RequirementVersion reqVersion, RequirementVersion orig) {
        Long idOrig = orig.getId();

        InfoListItem oldCategory = orig.getCategory();
        InfoListItem newCategory = reqVersion.getCategory();

        if (newCategory != null && !oldCategory.references(newCategory)) {
            requirementVersionManagerService.changeCategory(idOrig, newCategory.getCode());
        }
    }

    private void doUpdateRequirementModificationMetadata(
            AuditableMixin requirementVersion, AuditableMixin persistedVersion) {
        persistedVersion.setLastModifiedBy(requirementVersion.getCreatedBy());
        persistedVersion.setLastModifiedOn(requirementVersion.getCreatedOn());
    }

    public void updateRequirementVersions(
            List<RequirementVersionInstruction> instructions, Project project) {
        LOGGER.info("Updating Requirement Versions on project '{}'", project.getName());
        instructions.forEach(this::updateRequirementVersionRoutine);
    }

    private void updateRequirementVersionRoutine(RequirementVersionInstruction instruction) {

        RequirementVersion reqVersion = instruction.getRequirementVersion();
        Map<String, String> cufValues = instruction.getCustomFields();
        RequirementVersionTarget target = instruction.getTarget();

        if (target.getImportedRequirementStatus().isRequirementModifiable()
                || (!target.getImportedRequirementStatus().isRequirementModifiable()
                        && reqVersion.getStatus().isRequirementModifiable())) {
            helper.fillNullWithDefaults(reqVersion);
            helper.truncate(reqVersion, cufValues);
            fixCategory(target, reqVersion);
            RequirementVersion newVersion = doUpdateRequirementVersion(instruction, cufValues);

            // update the instruction with persisted one, needed for postProcess.
            instruction.setRequirementVersion(newVersion);

            // update model
            model.bindMilestonesToRequirementVersion(target, instruction.getMilestones());

            LOGGER.debug(EXCEL_ERR_PREFIX + "Updated Requirement Version \t'" + target + "'");
        } else {
            Requirement req = reqLibNavigationService.findRequirement(target.getRequirement().getId());
            doUpdateRequirementNature(req, target.getRequirement());
            instruction.setRequirementVersion(req.findRequirementVersion(target.getVersion()));
        }
    }

    public LogTrain deleteRequirementLink(RequirementLinkInstruction instr) {
        LogTrain train = validator.deleteRequirementLink(instr);
        if (!train.hasCriticalErrors()) {
            long sourceId = findVersionIdByTarget(instr.getTarget().getSourceVersion());
            long destId = findVersionIdByTarget(instr.getTarget().getDestVersion());
            reqlinkService.removeLinkedRequirementVersionsFromRequirementVersion(
                    sourceId, List.of(destId));
        }
        return train;
    }

    private long findVersionIdByTarget(RequirementVersionTarget versTarget) {
        Long reqId = model.getRequirementId(versTarget);
        return requirementVersionManagerService.findReqVersionIdByRequirementAndVersionNumber(
                reqId, versTarget.getVersion());
    }

    private void fixCategory(RequirementVersionTarget target, RequirementVersion requirementVersion) {
        TargetStatus projectStatus = model.getProjectStatus(target.getProject());

        InfoListItem category = requirementVersion.getCategory();
        // if category is null or inconsistent for project, setting to default project category
        if (category == null
                || !listItemFinderService.isCategoryConsistent(projectStatus.getId(), category.getCode())) {
            requirementVersion.setCategory(
                    listItemFinderService.findDefaultRequirementCategory(projectStatus.getId()));
        }
    }

    private RequirementVersion doUpdateRequirementVersion(
            RequirementVersionInstruction instruction, Map<String, String> cufValues) {

        RequirementVersionTarget target = instruction.getTarget();
        RequirementVersion reqVersion = instruction.getRequirementVersion();

        Requirement req = reqLibNavigationService.findRequirement(target.getRequirement().getId());

        RequirementVersion orig = req.findRequirementVersion(target.getVersion());

        doUpdateRequirementCoreAttributes(reqVersion, orig);

        // Feat 5169, unbind all milestones if cell is empty in import file.
        // Else, bind milestones if possible
        if (CollectionUtils.isEmpty(instruction.getMilestones())) {
            orig.getMilestones().clear();
        } else {
            updateRequirementVersionToMilestones(
                    target.isRejectedMilestone(), orig, boundMilestonesIds(instruction));
        }
        doUpdateCustomFields(cufValues, orig);
        doUpdateRequirementModificationMetadata(reqVersion, orig);
        moveRequirement(target.getRequirement(), req);
        doUpdateRequirementNature(req, target.getRequirement());

        if (orig.getStatus().isRequirementModifiable()) {
            doUpdateStatus(target.getImportedRequirementStatus(), orig);
            renameRequirementVersion(instruction.getTarget().getUnconsistentName(), orig);
        }

        // we return the persisted RequirementVersion for post process
        return orig;
    }

    private void renameRequirementVersion(String unconsistentName, RequirementVersion orig) {
        if (unconsistentName != null && !unconsistentName.isBlank()) {
            String newName = PathUtils.unescapePathPartSlashes(unconsistentName);
            orig.setName(newName);
        }
    }

    private void updateRequirementVersionToMilestones(
            boolean corruptedMilestones,
            RequirementVersion requirementVersionPersisted,
            List<Long> boundMilestonesIds) {
        if (!corruptedMilestones) {
            bindRequirementVersionToMilestones(requirementVersionPersisted, boundMilestonesIds);
        }
    }

    @SuppressWarnings("rawtypes")
    private void moveRequirement(RequirementTarget target, final Requirement req) {
        final Integer newPosition = target.getOrder();

        if (newPosition == null || newPosition <= 0) {
            return;
        }

        final Long[] sourceIds = new Long[] {req.getId()};
        final ClipboardPayload clipboardPayload =
                ClipboardPayload.withWhiteListIgnored(Collections.singletonList(req.getId()));

        if (target.isRootRequirement()) {
            reqLibNavigationService.moveNodesToLibrary(
                    req.getLibrary().getId(), sourceIds, newPosition, clipboardPayload);
        } else {
            final List<Long> ids = rlnDao.getParentsIds(req.getId());
            final Long firstParentId = ids.get(ids.size() - 2);
            final RequirementLibraryNode parent =
                    reqLibNavigationService.findRequirementLibraryNodeById(firstParentId);
            final Long parentId = parent.getId();

            parent.accept(
                    new RequirementLibraryNodeVisitor() {
                        @Override
                        public void visit(Requirement requirement) {
                            reqLibNavigationService.moveNodesToRequirement(
                                    parentId, sourceIds, newPosition, clipboardPayload);
                        }

                        @Override
                        public void visit(RequirementFolder folder) {
                            reqLibNavigationService.moveNodesToFolder(
                                    parentId, sourceIds, newPosition, clipboardPayload);
                        }
                    });
        }
    }

    private void doUpdateRequirementNature(Requirement req, RequirementTarget target) {
        if (!req.isHighLevel() && target.isHighLevel()) {
            highLevelRequirementService.convertIntoHighLevelRequirement(req.getId());
        } else if (req.isHighLevel() && !target.isHighLevel()) {
            highLevelRequirementService.convertIntoStandardRequirement(req.getId());
        }
    }

    public LogTrain deleteRequirementVersion(RequirementVersionInstruction instr) {
        throw new NotImplementedException(
                "implement me - must return a Failure : Not implemented in the log train instead of throwing this exception");
    }

    public LogTrain createLinkedLowLevelRequirement(LinkedLowLevelRequirementInstruction instr) {
        LogTrain train = validator.createLinkedLowLevelRequirement(instr);

        if (!train.hasCriticalErrors()) {
            LinkedLowLevelRequirementTarget target = instr.getTarget();
            Long highLevelReqId = reqLibNavigationService.findNodeIdByPath(target.getHighLevelReqPath());
            Long standardReqId = reqLibNavigationService.findNodeIdByPath(target.getStandardReqPath());

            highLevelRequirementService.linkToHighLevelRequirement(highLevelReqId, standardReqId);
        }

        return train;
    }

    public void setValidator(ValidationFacility validationFacility) {
        this.validator = validationFacility;
    }

    public void createCoverages(List<CoverageInstruction> instructions) {
        Batches<CoverageImportDto> batches = new Batches<>();

        instructions.stream()
                .collect(
                        Collectors.toMap(
                                i -> List.of(i.getTestCaseId(), i.getRequirementId()),
                                i -> i,
                                (i1, i2) ->
                                        i1.getTarget().getReqVersion() > i2.getTarget().getReqVersion() ? i1 : i2))
                .values()
                .forEach(
                        instruction -> {
                            Long versionId =
                                    model.getRequirementVersionId(instruction.getTarget().getRequirementVersion());
                            Long testCaseId = model.getTestCaseId(instruction.getTarget().getTestCase());

                            CoverageImportDto dto =
                                    new CoverageImportDto(
                                            versionId, instruction.getCoverage(), instruction.getExistingCoverageId());

                            batches.addBatch(testCaseId, dto);
                        });

        for (List<Batch<CoverageImportDto>> batchList : batches.partition(BATCH_10)) {
            List<Long> testCaseIds = batchList.stream().map(Batch::getTargetId).toList();
            requirementImportService.createCoverages(testCaseIds, batchList);
        }
    }

    public void createOrUpdateLinks(List<RequirementLinkInstruction> instructions) {
        Batches<LinkImportDto> createInstructions = new Batches<>();
        List<ExistLinkImportDto> updateInstructions = new ArrayList<>();

        for (RequirementLinkInstruction instruction : instructions) {
            RequirementLinkTarget target = instruction.getTarget();

            if (instruction.linkExists()) {
                var dto =
                        new ExistLinkImportDto(
                                target.getExistingOutboundLinkId(),
                                target.getExistingInboundLinkId(),
                                instruction.getRelationRole());
                updateInstructions.add(dto);
            } else {
                Long sourceId = target.getSourceVersion().getId();
                var dto =
                        new LinkImportDto(
                                sourceId, target.getDestVersion().getId(), instruction.getRelationRole());
                createInstructions.addBatch(sourceId, dto);
            }
        }

        for (List<Batch<LinkImportDto>> batches : createInstructions.partition(BATCH_10)) {
            requirementImportService.createLinks(batches);
        }

        for (List<ExistLinkImportDto> batches : Lists.partition(updateInstructions, BATCH_10)) {
            requirementImportService.updateLinks(batches);
        }
    }
}
