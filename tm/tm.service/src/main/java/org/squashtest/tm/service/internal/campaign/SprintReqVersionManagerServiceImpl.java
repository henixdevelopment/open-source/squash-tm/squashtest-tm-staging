/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.campaign;

import static org.squashtest.tm.service.security.Authorizations.READ_SPRINT_REQ_VERSION_OR_ADMIN;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.campaign.SprintReqVersion;
import org.squashtest.tm.service.campaign.SprintReqVersionManagerService;
import org.squashtest.tm.service.internal.repository.SprintReqVersionDao;

@Service("SprintReqVersionManagerService")
@Transactional
public class SprintReqVersionManagerServiceImpl implements SprintReqVersionManagerService {

    @Inject private SprintReqVersionDao sprintReqVersionDao;

    @Override
    @Transactional(readOnly = true)
    @PreAuthorize(READ_SPRINT_REQ_VERSION_OR_ADMIN)
    public SprintReqVersion findById(long sprintReqVersionId) {
        return sprintReqVersionDao
                .findById(sprintReqVersionId)
                .orElseThrow(
                        () ->
                                new EntityNotFoundException(
                                        "SprintReqVersion with id " + sprintReqVersionId + " not found"));
    }
}
