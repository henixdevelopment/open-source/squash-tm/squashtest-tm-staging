/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_COVERAGE;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.SelectHavingStep;
import org.jooq.SelectOrderByStep;
import org.jooq.impl.DSL;
import org.squashtest.tm.domain.milestone.MilestoneStatus;
import org.squashtest.tm.jooq.domain.tables.Requirement;
import org.squashtest.tm.jooq.domain.tables.RequirementVersion;
import org.squashtest.tm.jooq.domain.tables.RequirementVersionCoverage;

public final class DisplayDaoUtils {

    private DisplayDaoUtils() {
        throw new IllegalStateException("Utility class");
    }

    static SelectHavingStep<Record1<Long>> getMilestoneLocked() {
        return DSL.select(MILESTONE.MILESTONE_ID)
                .from(MILESTONE)
                .where(MILESTONE.STATUS.eq(MilestoneStatus.LOCKED.name()))
                .groupBy(MILESTONE.MILESTONE_ID);
    }

    static SelectOrderByStep<Record2<Long, Long>> getCoverage() {
        Requirement linkedLowLevelReq = REQUIREMENT.as("LINKED_LOW_LEVEL_REQ");
        RequirementVersion coverageInnerRequirementVersion =
                REQUIREMENT_VERSION.as("COVERAGE_INNER_REQUIREMENT_VERSION");
        RequirementVersionCoverage innerRequirementVersionCoverage =
                REQUIREMENT_VERSION_COVERAGE.as("INNER_REQUIREMENT_VERSION_COVERAGE");
        Field<Long> coverageInnerRequirementVersionId =
                coverageInnerRequirementVersion.RES_ID.as("COVERAGE_INNER_REQUIREMENT_VERSION_ID");

        return DSL.select(
                        coverageInnerRequirementVersionId,
                        innerRequirementVersionCoverage.VERIFYING_TEST_CASE_ID)
                .from(coverageInnerRequirementVersion)
                .join(innerRequirementVersionCoverage)
                .on(
                        coverageInnerRequirementVersion.RES_ID.eq(
                                innerRequirementVersionCoverage.VERIFIED_REQ_VERSION_ID))
                .union(
                        DSL.select(
                                        coverageInnerRequirementVersionId,
                                        innerRequirementVersionCoverage.VERIFYING_TEST_CASE_ID)
                                .from(coverageInnerRequirementVersion)
                                .join(linkedLowLevelReq)
                                .on(
                                        coverageInnerRequirementVersion.REQUIREMENT_ID.eq(
                                                linkedLowLevelReq.HIGH_LEVEL_REQUIREMENT_ID))
                                .join(innerRequirementVersionCoverage)
                                .on(
                                        linkedLowLevelReq.CURRENT_VERSION_ID.eq(
                                                innerRequirementVersionCoverage.VERIFIED_REQ_VERSION_ID)));
    }
}
