/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.dto;

import java.util.ArrayList;
import java.util.List;
import org.squashtest.tm.service.internal.dto.projectimporterxray.model.XrayItemStatus;

public abstract class AbstractGenericItem {
    private String title;
    private String key;
    private XrayItemStatus itemStatus;
    private String messageOnSuccess;
    private String messageOnError;
    private final List<String> messagesOnWarn = new ArrayList<>();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public XrayItemStatus getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(XrayItemStatus itemStatus) {
        this.itemStatus = itemStatus;
    }

    public String getMessageOnSuccess() {
        return messageOnSuccess;
    }

    public void setMessageOnSuccess(String messageOnSuccess) {
        this.messageOnSuccess = messageOnSuccess;
    }

    public String getMessageOnError() {
        return messageOnError;
    }

    public void setMessageOnError(String messageOnError) {
        this.messageOnError = messageOnError;
    }

    public List<String> getMessagesOnWarn() {
        return messagesOnWarn;
    }
}
