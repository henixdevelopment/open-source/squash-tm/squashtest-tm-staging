/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.testcase;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CalledTestStepDto extends AbstractTestStepDto {

    public static final String CALL_STEP = "call-step";

    private Long calledTcId;
    private String calledTcName;
    private Long calledDatasetId;
    private String calledDatasetName;
    private Boolean delegateParam;
    private List<AbstractTestStepDto> calledTestCaseSteps = new ArrayList<>();

    @Override
    public String getKind() {
        return CALL_STEP;
    }

    public Long getCalledTcId() {
        return calledTcId;
    }

    public void setCalledTcId(Long calledTcId) {
        this.calledTcId = calledTcId;
    }

    public String getCalledTcName() {
        return calledTcName;
    }

    public void setCalledTcName(String calledTcName) {
        this.calledTcName = calledTcName;
    }

    public Long getCalledDatasetId() {
        return calledDatasetId;
    }

    public void setCalledDatasetId(Long calledDatasetId) {
        this.calledDatasetId = calledDatasetId;
    }

    public Boolean getDelegateParam() {
        return delegateParam;
    }

    public void setDelegateParam(Boolean delegateParam) {
        this.delegateParam = delegateParam;
    }

    public List<AbstractTestStepDto> getCalledTestCaseSteps() {
        return calledTestCaseSteps;
    }

    public void addCalledTestCaseSteps(Collection<AbstractTestStepDto> testSteps) {
        this.calledTestCaseSteps.addAll(testSteps);
    }

    public String getCalledDatasetName() {
        return calledDatasetName;
    }

    public void setCalledDatasetName(String calledDatasetName) {
        this.calledDatasetName = calledDatasetName;
    }
}
