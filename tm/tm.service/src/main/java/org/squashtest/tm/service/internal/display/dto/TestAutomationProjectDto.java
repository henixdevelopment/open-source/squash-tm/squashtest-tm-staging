/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import org.squashtest.tm.domain.testautomation.TestAutomationProject;

public final class TestAutomationProjectDto {

    private Long taProjectId;
    private Long tmProjectId;
    private String remoteName;
    private String label;
    private Long serverId;
    private String executionEnvironments;
    private boolean canRunBdd;

    private TestAutomationProjectDto() {}

    public static TestAutomationProjectDto fromPersistedTestAutomationProject(
            TestAutomationProject taProject) {
        final TestAutomationProjectDto dto = fromTestAutomationProject(taProject);
        dto.setTmProjectId(taProject.getTmProject().getId());
        dto.setServerId(taProject.getServer().getId());
        return dto;
    }

    public static TestAutomationProjectDto fromRemoteTestAutomationProject(
            TestAutomationProject taProject, Long tmProjectId) {
        final TestAutomationProjectDto dto = fromTestAutomationProject(taProject);
        dto.setTmProjectId(tmProjectId);
        return dto;
    }

    private static TestAutomationProjectDto fromTestAutomationProject(
            TestAutomationProject taProject) {
        final TestAutomationProjectDto dto = new TestAutomationProjectDto();
        dto.setTaProjectId(taProject.getId());
        dto.setRemoteName(taProject.getJobName());
        dto.setLabel(taProject.getLabel());
        dto.setExecutionEnvironments(taProject.getSlaves());
        dto.setCanRunBdd(taProject.isCanRunGherkin());
        return dto;
    }

    public Long getTaProjectId() {
        return taProjectId;
    }

    void setTaProjectId(Long taProjectId) {
        this.taProjectId = taProjectId;
    }

    public Long getTmProjectId() {
        return tmProjectId;
    }

    void setTmProjectId(Long tmProjectId) {
        this.tmProjectId = tmProjectId;
    }

    public String getRemoteName() {
        return remoteName;
    }

    void setRemoteName(String remoteName) {
        this.remoteName = remoteName;
    }

    public String getLabel() {
        return label;
    }

    void setLabel(String label) {
        this.label = label;
    }

    public Long getServerId() {
        return serverId;
    }

    void setServerId(Long serverId) {
        this.serverId = serverId;
    }

    public String getExecutionEnvironments() {
        return executionEnvironments;
    }

    void setExecutionEnvironments(String executionEnvironments) {
        this.executionEnvironments = executionEnvironments;
    }

    public boolean isCanRunBdd() {
        return canRunBdd;
    }

    void setCanRunBdd(boolean canRunBdd) {
        this.canRunBdd = canRunBdd;
    }
}
