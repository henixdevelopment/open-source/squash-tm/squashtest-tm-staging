/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.requirement;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.squashtest.tm.domain.requirement.RequirementVersionLinkType;
import org.squashtest.tm.exception.RequiredFieldException;
import org.squashtest.tm.exception.requirement.link.CodesAndRolesInconsistentException;
import org.squashtest.tm.exception.requirement.link.LinkTypeCodeAlreadyExistsDomainException;
import org.squashtest.tm.exception.requirement.link.LinkTypeIsDefaultTypeException;
import org.squashtest.tm.service.internal.repository.RequirementVersionLinkDao;
import org.squashtest.tm.service.internal.repository.RequirementVersionLinkTypeDao;
import org.squashtest.tm.service.requirement.RequirementVersionLinkTypeManagerService;
import org.squashtest.tm.service.requirement.RequirementVersionLinkTypeRecord;

/** Created by jlor on 14/06/2017. */
@Transactional
@Service("squashtest.tm.service.RequirementVersionLinkTypeManagerService")
public class RequirementVersionLinkTypeManagerServiceImpl
        implements RequirementVersionLinkTypeManagerService {

    private static final String ROLE_1_CODE = "role1Code";
    private static final String ROLE_2_CODE = "role2Code";
    private static final String ROLE_1 = "role1";

    private static final String MESSAGE_KEY_WITHOUT_CODE =
            "sqtm-core.exception.requirements-links.link-type-code-already-exists";
    private static final String MESSAGE_KEY_WITH_CODE =
            "sqtm-core.exception.requirements-links.link-type-code-already-exists-with-code";

    @Inject private RequirementVersionLinkTypeDao linkTypeDao;

    @Inject private RequirementVersionLinkDao reqLinkDao;

    @PersistenceContext private EntityManager entityManager;

    @Override
    public void addLinkType(RequirementVersionLinkType newLinkType) {

        // We removed the LinkTypeCodeAlreadyExistsException because we needed a domain exception to
        // handle the error
        // with angular client
        if (linkTypeDao.doesCodeAlreadyExist(newLinkType.getRole1Code())) {
            throw new LinkTypeCodeAlreadyExistsDomainException(
                    "code", ROLE_1_CODE, MESSAGE_KEY_WITHOUT_CODE);
        } else if (linkTypeDao.doesCodeAlreadyExist(newLinkType.getRole2Code())) {
            throw new LinkTypeCodeAlreadyExistsDomainException(
                    "code", ROLE_2_CODE, MESSAGE_KEY_WITHOUT_CODE);
        }

        if (newLinkType.getRole1Code().equals(newLinkType.getRole2Code())
                && !newLinkType.getRole1().equals(newLinkType.getRole2())) {
            throw new CodesAndRolesInconsistentException(
                    newLinkType.getRole1(), newLinkType.getRole1Code(), ROLE_1);
        }

        linkTypeDao.save(newLinkType);
    }

    @Override
    public boolean doesLinkTypeCodeAlreadyExist(String code) {
        return linkTypeDao.doesCodeAlreadyExist(code);
    }

    @Override
    public boolean doesLinkTypeCodeAlreadyExist(String code, Long linkTypeId) {
        return linkTypeDao.doesCodeAlreadyExist(code, linkTypeId);
    }

    @Override
    public void changeDefault(Long linkTypeId) {
        RequirementVersionLinkType newDefaultReqLinkType = linkTypeDao.getReferenceById(linkTypeId);
        List<RequirementVersionLinkType> allReqLinkTypes =
                linkTypeDao.getAllRequirementVersionLinkTypes();
        for (RequirementVersionLinkType linkType : allReqLinkTypes) {
            linkType.setDefault(false);
        }
        newDefaultReqLinkType.setDefault(true);
    }

    @Override
    public void changeRole1(Long linkTypeId, String newRole1) {

        RequirementVersionLinkType linkType = linkTypeDao.getReferenceById(linkTypeId);

        if (linkType.getRole1Code().equals(linkType.getRole2Code())) {
            if (areCodesAndRolesInconsistent(
                    newRole1, linkType.getRole1Code(), linkType.getRole2(), linkType.getRole2Code())) {
                throw new CodesAndRolesInconsistentException(newRole1, linkType.getRole1Code(), ROLE_1);
            }
        } else {
            checkFieldIsNotBlank(newRole1, ROLE_1);
        }

        linkType.setRole1(newRole1);
    }

    @Override
    public void changeRole2(Long linkTypeId, String newRole2) {

        RequirementVersionLinkType linkType = linkTypeDao.getReferenceById(linkTypeId);

        if (linkType.getRole1Code().equals(linkType.getRole2Code())) {
            if (areCodesAndRolesInconsistent(
                    linkType.getRole1(), linkType.getRole1Code(), newRole2, linkType.getRole2Code())) {
                throw new CodesAndRolesInconsistentException(newRole2, linkType.getRole2Code(), "role2");
            }
        } else {
            checkFieldIsNotBlank(newRole2, "role2");
        }

        linkType.setRole2(newRole2);
    }

    @Override
    public void changeCode1(Long linkTypeId, String newCode1) {

        RequirementVersionLinkType linkType = linkTypeDao.getReferenceById(linkTypeId);

        if (newCode1.equals(linkType.getRole2Code())) {
            if (areCodesAndRolesInconsistent(
                    linkType.getRole1(), newCode1, linkType.getRole2(), linkType.getRole2Code())) {
                throw new CodesAndRolesInconsistentException(linkType.getRole1(), newCode1, ROLE_1_CODE);
            }
        } else {
            checkCodeFieldErrors(newCode1, ROLE_1_CODE);
        }

        linkType.setRole1Code(newCode1);
    }

    @Override
    public void changeCode2(Long linkTypeId, String newCode2) {

        RequirementVersionLinkType linkType = linkTypeDao.getReferenceById(linkTypeId);

        if (newCode2.equals(linkType.getRole1Code())) {
            if (areCodesAndRolesInconsistent(
                    linkType.getRole1(), linkType.getRole1Code(), linkType.getRole2(), newCode2)) {
                throw new CodesAndRolesInconsistentException(linkType.getRole2(), newCode2, ROLE_2_CODE);
            }
        } else {
            checkCodeFieldErrors(newCode2, ROLE_2_CODE);
        }

        linkType.setRole2Code(newCode2);
    }

    @Override
    public boolean isLinkTypeDefault(Long linkTypeId) {
        return linkTypeDao.isLinkTypeDefault(linkTypeId);
    }

    @Override
    public boolean isLinkTypeUsed(Long linkTypeId) {
        return linkTypeDao.isLinkTypeUsed(linkTypeId);
    }

    @Override
    public void deleteLinkType(Long linkTypeId) {
        RequirementVersionLinkType linkTypeToDelete = linkTypeDao.getReferenceById(linkTypeId);
        if (linkTypeDao.isLinkTypeDefault(linkTypeId)) {
            throw new LinkTypeIsDefaultTypeException();
        } else {
            RequirementVersionLinkType defaultLinkType =
                    linkTypeDao.getDefaultRequirementVersionLinkType();
            reqLinkDao.setLinksTypeToDefault(linkTypeToDelete, defaultLinkType);
            linkTypeDao.delete(linkTypeToDelete);
        }
    }

    @Override
    public boolean doesContainDefault(List<Long> linkTypesIdsToCheck) {
        Iterable<RequirementVersionLinkType> linkTypesToCheck =
                linkTypeDao.findAllById(linkTypesIdsToCheck);
        for (RequirementVersionLinkType linkType : linkTypesToCheck) {
            if (linkType.isDefault()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void deleteLinkTypes(List<Long> linkTypeIdsToDelete) {
        for (Long id : linkTypeIdsToDelete) {
            deleteLinkType(id);
        }
    }

    @Override
    public void editLinkType(
            long linkTypeId, RequirementVersionLinkTypeRecord requirementVersionLinkTypeRecord) {
        checkCanEditLinkType(linkTypeId, requirementVersionLinkTypeRecord);

        RequirementVersionLinkType requirementVersionLinkType =
                entityManager.find(RequirementVersionLinkType.class, linkTypeId);

        requirementVersionLinkType.setRole1(requirementVersionLinkTypeRecord.role1());
        requirementVersionLinkType.setRole1Code(requirementVersionLinkTypeRecord.role1Code());
        requirementVersionLinkType.setRole2(requirementVersionLinkTypeRecord.role2());
        requirementVersionLinkType.setRole2Code(requirementVersionLinkTypeRecord.role2Code());

        linkTypeDao.save(requirementVersionLinkType);
    }

    private void checkCanEditLinkType(
            long linkTypeId, RequirementVersionLinkTypeRecord linkTypeRecord) {
        if (linkTypeDao.doesCodeAlreadyExist(linkTypeRecord.role1Code(), linkTypeId)) {
            throw new LinkTypeCodeAlreadyExistsDomainException(
                    linkTypeRecord.role1Code(), ROLE_1_CODE, MESSAGE_KEY_WITH_CODE);
        } else if (linkTypeDao.doesCodeAlreadyExist(linkTypeRecord.role2Code(), linkTypeId)) {
            throw new LinkTypeCodeAlreadyExistsDomainException(
                    linkTypeRecord.role2Code(), ROLE_2_CODE, MESSAGE_KEY_WITH_CODE);
        }

        if (areCodesAndRolesInconsistent(
                linkTypeRecord.role1(),
                linkTypeRecord.role1Code(),
                linkTypeRecord.role2(),
                linkTypeRecord.role2Code())) {
            throw new CodesAndRolesInconsistentException(
                    linkTypeRecord.role1(), linkTypeRecord.role1Code(), ROLE_1);
        }
    }

    private boolean areCodesAndRolesInconsistent(
            String role1, String role1Code, String role2, String role2Code) {
        return role1Code.equals(role2Code) && !role1.equals(role2);
    }

    private void checkCodeFieldErrors(String code, String fieldName) {
        checkFieldIsNotBlank(code, fieldName);

        if (linkTypeDao.doesCodeAlreadyExist(code)) {
            throw new LinkTypeCodeAlreadyExistsDomainException(
                    "code", fieldName, MESSAGE_KEY_WITHOUT_CODE);
        }
    }

    private void checkFieldIsNotBlank(String fieldValue, String fieldName) {
        if (!StringUtils.hasText(fieldValue)) {
            throw new RequiredFieldException(fieldName);
        }
    }
}
