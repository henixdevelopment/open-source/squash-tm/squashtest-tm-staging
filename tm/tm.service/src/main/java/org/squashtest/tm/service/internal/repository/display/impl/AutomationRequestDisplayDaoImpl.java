/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.squashtest.tm.jooq.domain.Tables.AUTOMATION_REQUEST;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;

import java.util.Arrays;
import java.util.List;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.SelectConditionStep;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.project.AutomationWorkflowType;
import org.squashtest.tm.domain.testcase.TestCaseAutomatable;
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequestStatus;
import org.squashtest.tm.service.internal.display.dto.AutomationRequestDto;
import org.squashtest.tm.service.internal.repository.display.AutomationRequestDisplayDao;
import org.squashtest.tm.service.internal.repository.display.RemoteAutomationRequestExtenderDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

@Repository
public class AutomationRequestDisplayDaoImpl implements AutomationRequestDisplayDao {

    private DSLContext dsl;

    private RemoteAutomationRequestExtenderDisplayDao extenderDao;

    public AutomationRequestDisplayDaoImpl(
            DSLContext dsl, RemoteAutomationRequestExtenderDisplayDao extenderDao) {
        this.extenderDao = extenderDao;
        this.dsl = dsl;
    }

    @Override
    public AutomationRequestDto findByTestCaseId(Long testCaseId) {
        Record record =
                dsl.select(
                                AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID.as(RequestAliasesConstants.ID),
                                AUTOMATION_REQUEST.AUTOMATION_PRIORITY.as(RequestAliasesConstants.PRIORITY),
                                AUTOMATION_REQUEST.REQUEST_STATUS,
                                AUTOMATION_REQUEST.TRANSMITTED_ON)
                        .from(AUTOMATION_REQUEST)
                        .innerJoin(TEST_CASE)
                        .on(AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID.eq(TEST_CASE.AUTOMATION_REQUEST_ID))
                        .where(TEST_CASE.TCLN_ID.eq(testCaseId))
                        .fetchOne();
        AutomationRequestDto automationRequestDto = null;
        if (record != null) {
            automationRequestDto = record.into(AutomationRequestDto.class);
            extenderDao.addRemoteExtender(automationRequestDto);
        }
        return automationRequestDto;
    }

    @Override
    public int countByAssignedTo(List<Long> readableProjectIds, Long userId) {
        Integer count =
                baseRequest(readableProjectIds)
                        .and(AUTOMATION_REQUEST.ASSIGNED_TO.eq(userId))
                        .fetchOneInto(int.class);

        return count != null ? count : 0;
    }

    @Override
    public int countTransmitted(List<Long> readableProjectIds) {
        Integer count =
                baseRequest(readableProjectIds)
                        .and(
                                AUTOMATION_REQUEST.REQUEST_STATUS.in(
                                        Arrays.asList(
                                                AutomationRequestStatus.AUTOMATION_IN_PROGRESS.name(),
                                                AutomationRequestStatus.TRANSMITTED.name())))
                        .and(AUTOMATION_REQUEST.ASSIGNED_TO.isNull())
                        .fetchOneInto(int.class);

        return count != null ? count : 0;
    }

    @Override
    public int countAll(List<Long> readableProjectIds) {
        Integer count = baseRequest(readableProjectIds).fetchOneInto(int.class);

        return count != null ? count : 0;
    }

    @Override
    public List<String> getTcLastModifiedByForCurrentUser(
            List<Long> projectIds, List<String> requestStatus, Long userId) {

        Condition condition = DSL.trueCondition();
        if (userId != null) {
            condition = AUTOMATION_REQUEST.ASSIGNED_TO.eq(userId);
        }

        return getTCLastModifiedByBaseRequest(projectIds, requestStatus)
                .and(condition)
                .orderBy(TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY)
                .fetch(TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY);
    }

    @Override
    public List<String> getTcLastModifiedByToAutomationRequestNotAssigned(
            List<Long> projectIds, List<String> requestStatus) {
        return getTCLastModifiedByBaseRequest(projectIds, requestStatus)
                .and(AUTOMATION_REQUEST.ASSIGNED_TO.isNull())
                .orderBy(TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY)
                .fetch(TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY);
    }

    @Override
    public List<String> getTcLastModifiedByForAutomationRequests(
            List<Long> projectIds, List<String> requestStatus) {
        return getTCLastModifiedByBaseRequest(projectIds, requestStatus)
                .orderBy(TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY)
                .fetch(TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY);
    }

    @Override
    public List<String> getAssignedUserForAutomationRequests(List<Long> projectIds) {
        return dsl.selectDistinct(CORE_USER.LOGIN)
                .from(TEST_CASE_LIBRARY_NODE)
                .innerJoin(TEST_CASE)
                .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .innerJoin(PROJECT)
                .on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .innerJoin(AUTOMATION_REQUEST)
                .on(TEST_CASE.AUTOMATION_REQUEST_ID.eq(AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID))
                .innerJoin(CORE_USER)
                .on(AUTOMATION_REQUEST.ASSIGNED_TO.eq(CORE_USER.PARTY_ID))
                .where(TEST_CASE.AUTOMATABLE.eq(TestCaseAutomatable.Y.name()))
                .and(PROJECT.ALLOW_AUTOMATION_WORKFLOW.isTrue())
                .and(PROJECT.PROJECT_ID.in(projectIds))
                .orderBy(CORE_USER.LOGIN)
                .fetch(CORE_USER.LOGIN);
    }

    private SelectConditionStep<?> getTCLastModifiedByBaseRequest(
            List<Long> projectIds, List<String> requestStatus) {
        return dsl.selectDistinct(TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY)
                .from(TEST_CASE_LIBRARY_NODE)
                .innerJoin(TEST_CASE)
                .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .innerJoin(PROJECT)
                .on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .innerJoin(AUTOMATION_REQUEST)
                .on(TEST_CASE.AUTOMATION_REQUEST_ID.eq(AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID))
                .where(AUTOMATION_REQUEST.REQUEST_STATUS.in(requestStatus))
                .and(TEST_CASE.AUTOMATABLE.eq(TestCaseAutomatable.Y.name()))
                .and(PROJECT.ALLOW_AUTOMATION_WORKFLOW.isTrue())
                .and(PROJECT.PROJECT_ID.in(projectIds));
    }

    private SelectConditionStep<?> baseRequest(List<Long> projectIds) {
        return dsl.selectCount()
                .from(AUTOMATION_REQUEST)
                .innerJoin(TEST_CASE)
                .on(AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID.eq(TEST_CASE.AUTOMATION_REQUEST_ID))
                .innerJoin(TEST_CASE_LIBRARY_NODE)
                .on(TEST_CASE.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .innerJoin(PROJECT)
                .on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .where(PROJECT.PROJECT_ID.in(projectIds))
                .and(PROJECT.ALLOW_AUTOMATION_WORKFLOW.isTrue())
                .and(PROJECT.AUTOMATION_WORKFLOW_TYPE.eq(AutomationWorkflowType.NATIVE.name()))
                .and(TEST_CASE.AUTOMATABLE.eq(TestCaseAutomatable.Y.name()));
    }
}
