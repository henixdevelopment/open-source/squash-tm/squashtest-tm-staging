/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.projectimporter.xrayimporter;

import static org.jooq.impl.DSL.count;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.ObjLongConsumer;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.InsertValuesStepN;
import org.jooq.Query;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.Record3;
import org.jooq.Record8;
import org.jooq.ResultQuery;
import org.jooq.SelectConditionStep;
import org.jooq.SelectLimitPercentStep;
import org.jooq.SelectOnConditionStep;
import org.jooq.Table;
import org.jooq.UpdateConditionStep;
import org.jooq.impl.DSL;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;
import org.squashtest.tm.service.internal.dto.projectimporterxray.XrayField;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.dto.CalledTestXrayDto;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.dto.CustomFieldXrayDto;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.dto.ItemXrayDto;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.tables.CustomFieldTable;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.tables.CustomFieldTableRecord;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.tables.ItemTable;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.tables.ItemTableRecord;
import org.squashtest.tm.service.projectimporter.xrayimporter.XrayTablesDao;

@Component
@RequestScope
@Lazy
public class XrayTablesDaoImpl implements XrayTablesDao {
    private static final int FETCH_SIZE = 10;

    private final DSLContext dsl;
    private ItemTable itemTable;
    private CustomFieldTable customFieldTable;

    public XrayTablesDaoImpl(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public void createTables(ItemTable itemTable, CustomFieldTable customFieldTable) {
        this.customFieldTable = customFieldTable;
        this.itemTable = itemTable;
        createTmpTable(customFieldTable, customFieldTable.ID);
        createTmpTable(itemTable, itemTable.ID);
    }

    private void createTmpTable(Table<?> table, Field<?> primaryKey) {
        dsl.createTemporaryTableIfNotExists(table)
                .columns(table.fields())
                .constraints(DSL.primaryKey(primaryKey))
                .execute();
    }

    @Override
    public void createItems(List<ItemXrayDto> itemXrayDtos) {
        InsertValuesStepN<ItemTableRecord> insert =
                dsl.insertInto(itemTable).columns(itemTable.getFields());
        InsertValuesStepN<CustomFieldTableRecord> insertCuf =
                dsl.insertInto(customFieldTable).columns(customFieldTable.getFields());
        for (ItemXrayDto itemXrayDto : itemXrayDtos) {
            insert = insert.values(itemTable.getValues(itemXrayDto));
            for (CustomFieldXrayDto customFieldXrayDto : itemXrayDto.getCustomFields()) {
                insertCuf = insertCuf.values(customFieldTable.getValues(customFieldXrayDto));
            }
        }
        insertCuf.execute();
        insert.execute();
    }

    @Override
    public void dropTablesIfExist() {
        dsl.dropTableIfExists(customFieldTable).execute();
        dsl.dropTableIfExists(itemTable).execute();
    }

    @Override
    public void executeBatchQueries(List<Query> queries) {
        if (!queries.isEmpty()) {
            dsl.batch(queries).execute();
        }
    }

    // Update name of the item if it is a duplicate
    @Override
    public void addQueryUpdateItemName(Long itemId, String newName, List<Query> queries) {
        UpdateConditionStep<ItemTableRecord> updateQuery =
                dsl.update(itemTable).set(itemTable.SUMMARY, newName).where(itemTable.ID.eq(itemId));
        queries.add(updateQuery);
    }

    @Override
    public void updateCustomFieldPivotId(Long customFieldId, String pivotIdValue) {
        dsl.update(customFieldTable)
                .set(customFieldTable.PIVOT_ID, pivotIdValue)
                .where(customFieldTable.ID.eq(customFieldId))
                .execute();
    }

    @Override
    public boolean isEmptyTestCaseFolder(XrayField.Type type) {
        return isEmptyRequest(selectTypeRepositoryFolder(type));
    }

    @Override
    public boolean isEmptyItemTable(XrayField.Type... typeValue) {
        return isEmptyRequest(selectItemTable(typeValue));
    }

    @Override
    public boolean isEmptyCalledTestCase() {
        SelectLimitPercentStep<? extends Record> selectCalledTestCases =
                dsl.select(itemTable.ID)
                        .from(itemTable)
                        .join(customFieldTable)
                        .on(customFieldTable.ITEM_ID.eq(itemTable.ID))
                        .where(customFieldTable.STEP_CALLED_TEST_KEY.isNotNull())
                        .limit(1);
        return isEmptyRequest(selectCalledTestCases.fetchStream());
    }

    private <T extends Record> boolean isEmptyRequest(Stream<T> stream) {
        try (stream) {
            return stream.limit(1).toList().isEmpty();
        }
    }

    @Override
    public Stream<Record1<Long>> selectItemTable(XrayField.Type... typeValue) {
        List<String> typeValueName = getTypeNames(typeValue);
        return dsl.select(itemTable.ID)
                .from(itemTable)
                .where(itemTable.TYPE.in(typeValueName))
                .fetchSize(FETCH_SIZE)
                .fetchStream();
    }

    @Override
    public void selectTest(Consumer<ItemXrayDto> forEachItemDto, XrayField.Type... typeValue) {
        List<String> typeValueName = getTypeNames(typeValue);
        CustomFieldTable cufPreconditionTable = customFieldTable.as("cufPreconditionTable");
        CustomFieldTable cufCalledParameterTable = customFieldTable.as("cufCalledParameterTable");
        Field<Boolean> isCalledTest = cufCalledParameterTable.STEP_CALLED_TEST_KEY.isNotNull();

        ResultQuery<Record> queryResult =
                dsl.select(itemTable.fields())
                        .select(customFieldTable.fields())
                        .select(cufPreconditionTable.VALUE)
                        .select(isCalledTest)
                        .from(itemTable)
                        .leftJoin(customFieldTable)
                        .on(itemTable.ID.eq(customFieldTable.ITEM_ID))
                        .leftJoin(addPrecondition(cufPreconditionTable))
                        .on(cufPreconditionTable.ID.eq(customFieldTable.ID))
                        .leftJoin(addCalledParameter(cufCalledParameterTable))
                        .on(cufCalledParameterTable.STEP_CALLED_TEST_KEY.eq(itemTable.KEY))
                        .where(itemTable.TYPE.in(typeValueName))
                        .orderBy(itemTable.ID)
                        .fetchSize(FETCH_SIZE);

        commonConsumeItemIterator(
                queryResult,
                forEachItemDto,
                (testRecord, itemXrayDto) -> {
                    addElementToItemDtoDataMap(
                            itemXrayDto.getAssociatedIssuesWithXrayKey(),
                            testRecord.get(customFieldTable.VALUE),
                            testRecord.get(cufPreconditionTable.VALUE));
                    addIsCalledParameter(itemXrayDto, testRecord.get(isCalledTest));
                });
    }

    private Table<Record2<Long, String>> addPrecondition(CustomFieldTable cufPrecondition) {
        CustomFieldTable cufAssociatedPrecoTable = customFieldTable.as("cufAssociatedPrecoTable");
        ItemTable itemAssociatedCufPrecoTable = itemTable.as("itemAssociatedCufPrecoTable");
        CustomFieldTable cufAssociatedItemAssociatedPrecoTable =
                customFieldTable.as("cufAssociatedItemAssociatedPrecoTable");
        return dsl.select(
                        cufAssociatedPrecoTable.ID.as(cufPrecondition.ID),
                        cufAssociatedItemAssociatedPrecoTable.VALUE.as(cufPrecondition.VALUE))
                .from(cufAssociatedPrecoTable)
                .join(itemAssociatedCufPrecoTable)
                .on(itemAssociatedCufPrecoTable.KEY.eq(cufAssociatedPrecoTable.VALUE))
                .join(cufAssociatedItemAssociatedPrecoTable)
                .on(cufAssociatedItemAssociatedPrecoTable.ITEM_ID.eq(itemAssociatedCufPrecoTable.ID))
                .where(
                        cufAssociatedPrecoTable.KEY.contains(
                                XrayField.CustomFieldKey.TEST_ASSOCIATED_PRECONDITION.getKey()))
                .and(
                        cufAssociatedItemAssociatedPrecoTable.KEY.contains(
                                XrayField.CustomFieldKey.PRECONDITION_VALUE.getKey()))
                .asTable(cufPrecondition);
    }

    public Table<Record1<String>> addCalledParameter(CustomFieldTable cufCalledParameter) {
        return dsl.select(cufCalledParameter.STEP_CALLED_TEST_KEY)
                .from(cufCalledParameter)
                .where(cufCalledParameter.STEP_CALLED_TEST_PARAMETERS.isNotNull())
                .groupBy(cufCalledParameter.STEP_CALLED_TEST_KEY)
                .asTable(cufCalledParameter);
    }

    @Override
    public Stream<Record1<String>> selectTypeRepositoryFolder(XrayField.Type typeValue) {
        return dsl.select(customFieldTable.VALUE)
                .from(itemTable)
                .join(customFieldTable)
                .on(customFieldTable.ITEM_ID.eq(itemTable.ID))
                .where(itemTable.TYPE.eq(typeValue.getName()))
                .and(customFieldTable.KEY.contains(XrayField.CustomFieldKey.TEST_REPOSITORY_PATH.getKey()))
                .and(customFieldTable.VALUE.isNotNull())
                .fetchSize(FETCH_SIZE)
                .fetchStream();
    }

    @Override
    public void selectCalledTestCasesToAssignDataset(
            String testCaseXrayKey, ObjLongConsumer<String> consumStepCalledTestParameters) {
        ResultQuery<Record2<String, Long>> resultQuery =
                dsl.select(customFieldTable.STEP_CALLED_TEST_PARAMETERS, customFieldTable.ID)
                        .from(customFieldTable)
                        .where(customFieldTable.STEP_CALLED_TEST_KEY.eq(testCaseXrayKey))
                        .and(customFieldTable.STEP_CALLED_TEST_PARAMETERS.isNotNull())
                        .fetchSize(FETCH_SIZE);
        try (Stream<Record2<String, Long>> stream = resultQuery.fetchStream()) {
            stream.forEach(
                    parametersRecord ->
                            consumStepCalledTestParameters.accept(
                                    parametersRecord.value1(), parametersRecord.value2()));
        }
    }

    @Override
    public void selectCalledTestCases(Consumer<List<CalledTestXrayDto>> calledTestXrayDtosConsumer) {
        ItemTable it = itemTable.as("it1");
        ResultQuery<Record8<String, String, String, Integer, String, String, String, String>> result =
                dsl.select(
                                itemTable.PIVOT_ID,
                                itemTable.KEY,
                                itemTable.TITLE,
                                customFieldTable.STEP_INDEX,
                                customFieldTable.STEP_CALLED_TEST_KEY,
                                customFieldTable.STEP_CALLED_TEST_PARAMETERS,
                                customFieldTable.PIVOT_ID,
                                it.PIVOT_ID.as("called_id"))
                        .from(itemTable)
                        .join(customFieldTable)
                        .on(customFieldTable.ITEM_ID.eq(itemTable.ID))
                        .leftJoin(it)
                        .on(it.KEY.eq(customFieldTable.STEP_CALLED_TEST_KEY))
                        .where(customFieldTable.STEP_CALLED_TEST_KEY.isNotNull())
                        .orderBy(itemTable.PIVOT_ID, customFieldTable.STEP_INDEX)
                        .fetchSize(FETCH_SIZE);
        try (Stream<CalledTestXrayDto> stream = result.fetchStreamInto(CalledTestXrayDto.class)) {
            consumeCalledTestCaseGroupByPivotId(stream.iterator(), calledTestXrayDtosConsumer);
        }
    }

    private void consumeCalledTestCaseGroupByPivotId(
            Iterator<CalledTestXrayDto> iterable,
            Consumer<List<CalledTestXrayDto>> calledTestXrayDtosConsumer) {
        List<CalledTestXrayDto> currentGroup = new ArrayList<>();
        String currentPivotId = null;

        while (iterable.hasNext()) {
            CalledTestXrayDto calledTestXrayDto = iterable.next();
            if (currentPivotId == null || !currentPivotId.equals(calledTestXrayDto.getPivotId())) {
                if (!currentGroup.isEmpty()) {
                    calledTestXrayDtosConsumer.accept(currentGroup);
                    currentGroup.clear();
                }
                currentPivotId = calledTestXrayDto.getPivotId();
            }
            currentGroup.add(calledTestXrayDto);
        }
        if (!currentGroup.isEmpty()) {
            calledTestXrayDtosConsumer.accept(currentGroup);
            currentGroup.clear();
        }
    }

    @Override
    public void selectTestPlan(Consumer<ItemXrayDto> forEachItemDto, XrayField.Type... typeValue) {
        List<String> typeValueName = getTypeNames(typeValue);
        CustomFieldTable testAssociatedTestPlanTable =
                customFieldTable.as("testAssociatedTestPlanTable");
        ResultQuery<Record> queryResult =
                dsl.select(itemTable.fields())
                        .select(customFieldTable.fields())
                        .select(testAssociatedTestPlanTable.PIVOT_ID)
                        .from(itemTable)
                        .leftJoin(customFieldTable)
                        .on(itemTable.ID.eq(customFieldTable.ITEM_ID))
                        .leftJoin(
                                addTestPlanOrExecutionAssociatedTest(
                                        testAssociatedTestPlanTable,
                                        XrayField.CustomFieldKey.TEST_PLAN_ASSOCIATED_TESTS))
                        .on(testAssociatedTestPlanTable.ID.eq(customFieldTable.ID))
                        .where(itemTable.TYPE.in(typeValueName))
                        .orderBy(itemTable.ID)
                        .fetchSize(FETCH_SIZE);

        commonConsumeItemIterator(
                queryResult,
                forEachItemDto,
                (testRecord, itemXrayDto) ->
                        addElementToItemDtoDataMap(
                                itemXrayDto.getAssociatedIssuesWithXrayKey(),
                                testRecord.get(customFieldTable.VALUE),
                                testRecord.get(testAssociatedTestPlanTable.PIVOT_ID)));
    }

    private Table<Record2<Long, String>> addTestPlanOrExecutionAssociatedTest(
            CustomFieldTable tableAssociatedTest, XrayField.CustomFieldKey associatedTestKey) {
        CustomFieldTable cufTestPlanAssociatedTestTable =
                customFieldTable.as("cufTestPlanAssociatedTestTable");
        ItemTable itemTestAssociatedTestPlanTable = itemTable.as("itemTestAssociatedTestPlanTable");
        return dsl.select(
                        cufTestPlanAssociatedTestTable.ID.as(tableAssociatedTest.ID),
                        itemTestAssociatedTestPlanTable.PIVOT_ID.as(tableAssociatedTest.PIVOT_ID))
                .from(cufTestPlanAssociatedTestTable)
                .join(itemTestAssociatedTestPlanTable)
                .on(itemTestAssociatedTestPlanTable.KEY.eq(cufTestPlanAssociatedTestTable.VALUE))
                .where(cufTestPlanAssociatedTestTable.KEY.contains(associatedTestKey.getKey()))
                .asTable(tableAssociatedTest);
    }

    @Override
    public void selectOrphanTestExecution(
            Consumer<ItemXrayDto> forEachItemDto, XrayField.Type... typeValue) {
        List<String> typeValueName = getTypeNames(typeValue);
        CustomFieldTable testAssociatedOrphanExecutionTable =
                customFieldTable.as("testAssociatedOrphanExecutionTable");

        ResultQuery<Record> queryResult =
                dsl.select(itemTable.fields())
                        .select(customFieldTable.fields())
                        .select(testAssociatedOrphanExecutionTable.PIVOT_ID)
                        .from(itemTable)
                        .leftJoin(customFieldTable)
                        .on(itemTable.ID.eq(customFieldTable.ITEM_ID))
                        .leftJoin(
                                addTestPlanOrExecutionAssociatedTest(
                                        testAssociatedOrphanExecutionTable,
                                        XrayField.CustomFieldKey.EXECUTION_ASSOCIATED_TESTS))
                        .on(testAssociatedOrphanExecutionTable.ID.eq(customFieldTable.ID))
                        .where(itemTable.TYPE.in(typeValueName))
                        .andNotExists(selectOneForExecutionWithTestPlan())
                        .orderBy(itemTable.ID)
                        .fetchSize(FETCH_SIZE);

        commonConsumeItemIterator(
                queryResult,
                forEachItemDto,
                (testRecord, itemXrayDto) ->
                        addElementToItemDtoDataMap(
                                itemXrayDto.getAssociatedIssuesWithXrayKey(),
                                testRecord.get(customFieldTable.VALUE),
                                testRecord.get(testAssociatedOrphanExecutionTable.PIVOT_ID)));
    }

    private SelectConditionStep<Record1<Integer>> selectOneForExecutionWithTestPlan() {
        CustomFieldTable cufAssociatedTestPlanTable = customFieldTable.as("cufAssociatedTestPlanTable");

        return DSL.selectOne()
                .from(cufAssociatedTestPlanTable)
                .where(cufAssociatedTestPlanTable.ITEM_ID.eq(itemTable.ID))
                .and(
                        cufAssociatedTestPlanTable.KEY.contains(
                                XrayField.CustomFieldKey.EXECUTION_ASSOCIATED_TEST_PLAN.getKey()));
    }

    @Override
    public void selectTestExecutionForIteration(
            Consumer<ItemXrayDto> forEachItemDto, XrayField.Type... typeValue) {
        List<String> typeValueName = getTypeNames(typeValue);
        CustomFieldTable testAssociatedExecutionTable =
                customFieldTable.as("testAssociatedExecutionTable");
        CustomFieldTable testPlanAssociatedExecutionTable =
                customFieldTable.as("testPlanAssociatedExecutionTable");

        ResultQuery<Record> queryResult =
                dsl.select(itemTable.fields())
                        .select(customFieldTable.fields())
                        .select(testAssociatedExecutionTable.PIVOT_ID)
                        .select(testPlanAssociatedExecutionTable.PIVOT_ID)
                        .from(itemTable)
                        .leftJoin(customFieldTable)
                        .on(itemTable.ID.eq(customFieldTable.ITEM_ID))
                        .leftJoin(
                                addTestPlanOrExecutionAssociatedTest(
                                        testAssociatedExecutionTable,
                                        XrayField.CustomFieldKey.EXECUTION_ASSOCIATED_TESTS))
                        .on(testAssociatedExecutionTable.ID.eq(customFieldTable.ID))
                        .leftJoin(addExecutionAssociatedTestPlan(testPlanAssociatedExecutionTable))
                        .on(testPlanAssociatedExecutionTable.ID.eq(customFieldTable.ID))
                        .where(itemTable.TYPE.in(typeValueName))
                        .orderBy(itemTable.ID)
                        .fetchSize(FETCH_SIZE);
        commonConsumeItemIterator(
                queryResult,
                forEachItemDto,
                (testRecord, itemXrayDto) -> {
                    addElementToItemDtoDataMap(
                            itemXrayDto.getAssociatedIssuesWithXrayKey(),
                            testRecord.get(customFieldTable.VALUE),
                            testRecord.get(testAssociatedExecutionTable.PIVOT_ID));
                    addElementToItemDtoDataList(
                            itemXrayDto.getTestPlanPivotIds(),
                            testRecord.get(testPlanAssociatedExecutionTable.PIVOT_ID));
                });
    }

    private Table<Record2<Long, String>> addExecutionAssociatedTestPlan(
            CustomFieldTable tableAssociatedTestPlan) {
        CustomFieldTable cufExecutionAssociatedTestPlanTable =
                customFieldTable.as("cufExecutionAssociatedTestPlanTable");
        ItemTable itemTestPlanAssociatedExecutionTable =
                itemTable.as("itemTestPlanAssociatedExecutionTable");

        return dsl.select(
                        cufExecutionAssociatedTestPlanTable.ID.as(tableAssociatedTestPlan.ID),
                        itemTestPlanAssociatedExecutionTable.PIVOT_ID.as(tableAssociatedTestPlan.PIVOT_ID))
                .from(cufExecutionAssociatedTestPlanTable)
                .join(itemTestPlanAssociatedExecutionTable)
                .on(itemTestPlanAssociatedExecutionTable.KEY.eq(cufExecutionAssociatedTestPlanTable.VALUE))
                .where(
                        cufExecutionAssociatedTestPlanTable.KEY.contains(
                                XrayField.CustomFieldKey.EXECUTION_ASSOCIATED_TEST_PLAN.getKey()))
                .and(cufExecutionAssociatedTestPlanTable.VALUE.isNotNull())
                .asTable(tableAssociatedTestPlan);
    }

    private void commonConsumeItemIterator(
            ResultQuery<Record> queryResult,
            Consumer<ItemXrayDto> forEachItemDto,
            BiConsumer<Record, ItemXrayDto> additionalFields) {
        try (Stream<Record> resultRecord = queryResult.fetchStream()) {
            Iterator<Record> testIterator = resultRecord.iterator();
            consumeItemIterator(testIterator, forEachItemDto, additionalFields);
        }
    }

    private void consumeItemIterator(
            Iterator<Record> iterator,
            Consumer<ItemXrayDto> consumItemDto,
            BiConsumer<Record, ItemXrayDto> additionalFields) {
        Long currentIdItem = null;
        ItemXrayDto itemDto = null;

        while (iterator.hasNext()) {
            Record itemRecord = iterator.next();
            Long idItem = itemRecord.get(itemTable.ID);
            if (Objects.isNull(currentIdItem)) {
                itemDto = createItemDto(itemRecord);
                additionalFields.accept(itemRecord, itemDto);
                currentIdItem = idItem;
            } else if (currentIdItem.equals(idItem)) {
                addCustomFieldDto(itemDto, itemRecord.into(customFieldTable.fields()));
                additionalFields.accept(itemRecord, itemDto);
            } else {
                consumItemDto.accept(itemDto);
                itemDto = createItemDto(itemRecord);
                additionalFields.accept(itemRecord, itemDto);
                currentIdItem = idItem;
            }
            if (!iterator.hasNext()) {
                consumItemDto.accept(itemDto);
            }
        }
    }

    private ItemXrayDto createItemDto(Record itemRecord) {
        ItemXrayDto itemDto = itemRecord.into(itemTable.fields()).into(ItemXrayDto.class);
        addCustomFieldDto(itemDto, itemRecord.into(customFieldTable.fields()));
        return itemDto;
    }

    private void addCustomFieldDto(ItemXrayDto itemDto, Record customFieldRecord) {
        if (Objects.nonNull(customFieldRecord.get(customFieldTable.ID))) {
            CustomFieldXrayDto customFieldDto = customFieldRecord.into(CustomFieldXrayDto.class);
            itemDto.getCustomFields().add(customFieldDto);
        }
    }

    private <T> void addElementToItemDtoDataList(List<T> itemDtoDataList, T element) {
        if (Objects.nonNull(element)) {
            itemDtoDataList.add(element);
        }
    }

    private <T> void addElementToItemDtoDataMap(Map<T, T> itemDtoDataList, T key, T element) {
        if (Objects.nonNull(element)) {
            itemDtoDataList.put(key, element);
        }
    }

    private void addIsCalledParameter(ItemXrayDto itemDto, Boolean isCalledParameter) {
        if (Objects.nonNull(isCalledParameter)) {
            itemDto.setCalledParameter(isCalledParameter);
        }
    }

    @Override
    public Stream<Record3<Long, String, String>> selectDuplicateNameGroupBy(
            XrayField.CustomFieldKey customFieldKey, XrayField.Type... itemTypes) {
        List<String> typeValueName = Arrays.stream(itemTypes).map(XrayField.Type::getName).toList();
        Table<?> duplicateTable =
                dsl.select(itemTable.SUMMARY, customFieldTable.VALUE)
                        .from(itemTable)
                        .leftJoin(
                                dsl.select(customFieldTable.ITEM_ID, customFieldTable.VALUE)
                                        .from(customFieldTable)
                                        .where(customFieldTable.KEY.containsIgnoreCase(customFieldKey.getKey()))
                                        .asTable(customFieldTable))
                        .on(itemTable.ID.eq(customFieldTable.ITEM_ID))
                        .where(itemTable.TYPE.in(typeValueName))
                        .groupBy(itemTable.SUMMARY, customFieldTable.VALUE)
                        .having(count().gt(1))
                        .asTable("duplicateTable");

        return dsl.select(itemTable.ID, itemTable.SUMMARY, customFieldTable.VALUE)
                .from(itemTable)
                .leftJoin(
                        dsl.select(customFieldTable.ITEM_ID, customFieldTable.VALUE)
                                .from(customFieldTable)
                                .where(customFieldTable.KEY.containsIgnoreCase(customFieldKey.getKey()))
                                .asTable(customFieldTable))
                .on(itemTable.ID.eq(customFieldTable.ITEM_ID))
                .join(duplicateTable)
                .on(
                        itemTable.SUMMARY.eq(duplicateTable.field(itemTable.SUMMARY)),
                        customFieldTable
                                .VALUE
                                .eq(duplicateTable.field(customFieldTable.VALUE))
                                .or(
                                        customFieldTable
                                                .VALUE
                                                .isNull()
                                                .and(duplicateTable.field(customFieldTable.VALUE).isNull())))
                .orderBy(itemTable.SUMMARY)
                .fetchSize(FETCH_SIZE)
                .fetchStream();
    }

    @Override
    public Stream<Record2<Long, String>> selectDuplicateName(XrayField.Type... typeValue) {
        return handleCommonDuplicateName(query -> query, typeValue);
    }

    @Override
    public Stream<Record2<Long, String>> selectDuplicateNameOrphanTestExecution(
            XrayField.Type... typeValue) {
        return handleCommonDuplicateName(
                query ->
                        query.andNotExists(
                                DSL.selectOne()
                                        .from(customFieldTable)
                                        .where(customFieldTable.ITEM_ID.eq(itemTable.ID))
                                        .and(
                                                customFieldTable.KEY.containsIgnoreCase(
                                                        XrayField.CustomFieldKey.EXECUTION_ASSOCIATED_TEST_PLAN.getKey()))),
                typeValue);
    }

    private Stream<Record2<Long, String>> handleCommonDuplicateName(
            UnaryOperator<SelectOnConditionStep<Record2<Long, String>>> addAdditionalCondition,
            XrayField.Type... typeValue) {
        ItemTable itemGroup = itemTable.as("itemGroup");
        List<String> typeValueName = getTypeNames(typeValue);

        Table<Record1<String>> itemGroupTable =
                dsl.select(itemGroup.SUMMARY)
                        .from(itemGroup)
                        .where(itemGroup.TYPE.in(typeValueName))
                        .groupBy(itemGroup.SUMMARY)
                        .having(count().gt(1))
                        .asTable(itemGroup);

        SelectOnConditionStep<Record2<Long, String>> selectWithJoin =
                dsl.select(itemTable.ID, itemTable.SUMMARY)
                        .from(itemTable)
                        .join(itemGroupTable)
                        .on(itemTable.SUMMARY.eq(itemGroup.SUMMARY));
        SelectOnConditionStep<Record2<Long, String>> selectWithConditions =
                addAdditionalCondition.apply(selectWithJoin);
        return selectWithConditions.orderBy(itemTable.SUMMARY).fetchSize(FETCH_SIZE).fetchStream();
    }

    private List<String> getTypeNames(XrayField.Type[] types) {
        return Arrays.stream(types).map(XrayField.Type::getName).toList();
    }

    @Override
    public ItemTable getItemTable() {
        return itemTable;
    }

    @Override
    public CustomFieldTable getCustomFieldTable() {
        return customFieldTable;
    }
}
