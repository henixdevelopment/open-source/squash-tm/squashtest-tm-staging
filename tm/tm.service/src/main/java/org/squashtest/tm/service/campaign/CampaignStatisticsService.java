/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.campaign;

import java.util.List;
import java.util.Map;
import org.squashtest.tm.service.statistics.campaign.StatisticsBundle;

public interface CampaignStatisticsService {

    /* *********************************** all-in-one methods ************************************ */

    /**
     * Given a list of campaign ids, gathers all of the following in one package.
     *
     * @param campaignIds
     * @return StatisticsBundle
     */
    StatisticsBundle gatherCampaignStatisticsBundle(
            List<Long> campaignIds, boolean lastExecutionScope);

    /**
     * For activeMilestone, gathers all of the following in one package for a milestone.
     *
     * @return
     */
    // functional FIXME : ask a spec writer why statistics vary so subtly depending on
    // whether we ask a dashboard for a bunch of campaigns belonging to the same milestone,
    // or a bunch of campaigns belonging to the same folder.
    StatisticsBundle gatherMilestoneStatisticsBundle(boolean lastExecutionScope);

    StatisticsBundle gatherMultiStatisticsBundle(
            Map<Long, String> campaignNameMap, boolean lastExecutionScope);
}
