/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.bdd;

import static org.squashtest.tm.domain.bdd.ActionWord.ACTION_WORD_UNDERSCORE;
import static org.squashtest.tm.domain.bdd.util.ActionWordUtil.wrapWithDoubleQuotes;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;
import org.squashtest.tm.domain.bdd.util.ActionWordUtil;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.DatasetParamValue;
import org.squashtest.tm.domain.testcase.KeywordTestCase;
import org.squashtest.tm.domain.testcase.KeywordTestStep;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestStep;

public class CucumberScriptWriter implements BddScriptWriter {

    private static final String TAB_CHAR = "\t";
    private static final String DOUBLE_TAB_CHAR = "\t\t";
    private static final String TRIPLE_TAB_CHAR = "\t\t\t";
    private static final String NEW_LINE_CHAR = "\n";
    private static final String SPACE_CHAR = " ";
    private static final String VERTICAL_BAR = "|";
    private static final String ACROBAT_CHAR = "@";
    private static final String SCRIPT_LANGUAGE_LABEL = "# language: "; // SQUASH-1184
    private static final String COMMENT_MARKER = "#";
    private static final String DOCSTRING_MARKER = "\"\"\"";

    private boolean hasTCParamInTestCase = false;

    @Override
    public String writeBddScript(
            KeywordTestCase testCase, MessageSource messageSource, boolean escapeArrows) {
        StringBuilder builder = new StringBuilder();
        List<TestStep> testSteps = testCase.getSteps();
        String testCaseName = testCase.getName();
        Locale locale = testCase.getProject().getBddScriptLanguage().getLocale();

        if (!testSteps.isEmpty()) {
            addAllStepsScriptWithoutScenarioToBuilder(
                    builder, testSteps, locale, messageSource, escapeArrows);
            addScenarioAndDatasetToBuilder(
                    builder, testCaseName, testCase.getDatasets(), locale, messageSource);
            hasTCParamInTestCase = false;
        }
        addLanguageAndFeatureToBuilder(
                builder, testCaseName, locale.toLanguageTag(), locale, messageSource);
        return builder.toString();
    }

    private void addAllStepsScriptWithoutScenarioToBuilder(
            StringBuilder builder,
            List<TestStep> testSteps,
            Locale locale,
            MessageSource messageSource,
            boolean escapeArrows) {
        for (TestStep step : testSteps) {
            KeywordTestStep keywordStep = (KeywordTestStep) step;
            boolean wrapParameterValuesWithDoubleQuotes =
                    shouldParameterValuesBeWrappedWithDoubleQuotesByTechnology(keywordStep.getTestCase());

            String stepScript =
                    writeBddStepScript(
                            keywordStep,
                            messageSource,
                            locale,
                            escapeArrows,
                            wrapParameterValuesWithDoubleQuotes);
            raiseHasTCParamFlag(keywordStep.hasTCParam());
            builder.append(DOUBLE_TAB_CHAR).append(stepScript).append(NEW_LINE_CHAR);
        }
        builder.deleteCharAt(builder.length() - 1);
    }

    /**
     * Determines whether all parameter values should be wrapped in double quotes for a test case.
     * Checks against test case automated test technology if there is one, otherwise we look at the
     * Bdd implementation technology set at the project level.
     *
     * @param tc is a TestCase linked to a KeywordTestStep
     * @return boolean
     */
    private boolean shouldParameterValuesBeWrappedWithDoubleQuotesByTechnology(TestCase tc) {
        if (tc == null) {
            return false;
        }
        if (tc.getAutomatedTestTechnology() != null) {
            return "Cucumber 4".equals(tc.getAutomatedTestTechnology().getName())
                    || "Cucumber 5+".equals(tc.getAutomatedTestTechnology().getName());

        } else if (tc.getProject().getBddImplementationTechnology() != null) {
            return "CUCUMBER_4".equals(tc.getProject().getBddImplementationTechnology().name())
                    || "CUCUMBER_5_PLUS".equals(tc.getProject().getBddImplementationTechnology().name());
        }
        return false;
    }

    private void raiseHasTCParamFlag(boolean hasTCParam) {
        if (!hasTCParamInTestCase) {
            hasTCParamInTestCase = hasTCParam;
        }
    }

    private void addScenarioAndDatasetToBuilder(
            StringBuilder builder,
            String testCaseName,
            Set<Dataset> datasetSet,
            Locale locale,
            MessageSource messageSource) {
        if (!datasetSet.isEmpty() && hasTCParamInTestCase) {
            addScenarioToBuilder(
                    builder,
                    testCaseName,
                    messageSource.getMessage("testcase.bdd.script.label.scenario-outline", null, locale));
            generateAllDatasetAndExamplesScript(builder, datasetSet, locale, messageSource);
        } else {
            addScenarioToBuilder(
                    builder,
                    testCaseName,
                    messageSource.getMessage("testcase.bdd.script.label.scenario", null, locale));
        }
    }

    private void addScenarioToBuilder(StringBuilder builder, String testCaseName, String scenario) {
        StringBuilder preBuilder = new StringBuilder();
        preBuilder
                .append(NEW_LINE_CHAR)
                .append(NEW_LINE_CHAR)
                .append(TAB_CHAR)
                .append(scenario)
                .append(testCaseName)
                .append(NEW_LINE_CHAR);
        builder.insert(0, preBuilder);
    }

    private void generateAllDatasetAndExamplesScript(
            StringBuilder builder, Set<Dataset> datasetSet, Locale locale, MessageSource messageSource) {
        Map<String, String> parameterNameToTypeMap = computeParameterToTypeMap(datasetSet);
        for (Dataset dataset : datasetSet) {
            String datasetScript =
                    generateDatasetAndExampleScript(dataset, parameterNameToTypeMap, locale, messageSource);
            builder.append(NEW_LINE_CHAR).append(NEW_LINE_CHAR).append(datasetScript);
        }
    }

    /**
     * For a Set of Datasets, determine the type of each Parameter implied and maps its name with its
     * type. A Parameter is considered a 'number' if all its DatasetParameterValues are numbers, it is
     * considered a 'string' otherwise.
     *
     * @param dataSets the Set of Datasets implied
     * @return A Map of all parameters name mapped to their type
     */
    private Map<String, String> computeParameterToTypeMap(Set<Dataset> dataSets) {
        Map<String, String> parameterTypeMap = new HashMap<>();
        for (Dataset dataset : dataSets) {
            Set<DatasetParamValue> paramValues = dataset.getParameterValues();
            for (DatasetParamValue paramValue : paramValues) {
                String parameterName = paramValue.getParameter().getName();
                if (!ActionWordUtil.isNumber(paramValue.getParamValue())) {
                    parameterTypeMap.put(parameterName, "string");
                } else if (ActionWordUtil.isNumber(paramValue.getParamValue())
                        && !parameterTypeMap.containsKey(parameterName)) {
                    parameterTypeMap.put(parameterName, "number");
                }
            }
        }
        return parameterTypeMap;
    }

    private String generateDatasetAndExampleScript(
            Dataset dataset,
            Map<String, String> parameterNameToTypeMap,
            Locale locale,
            MessageSource messageSource) {
        String datasetTagLine = generateDatasetTagLine(dataset);
        String exampleLine =
                DOUBLE_TAB_CHAR
                        + messageSource.getMessage("testcase.bdd.script.label.examples", null, locale)
                        + NEW_LINE_CHAR;
        String paramNameAndValueLines =
                generateDatasetParamNamesAndValues(dataset, parameterNameToTypeMap);
        return datasetTagLine + exampleLine + paramNameAndValueLines;
    }

    private String generateDatasetTagLine(Dataset dataset) {
        String originalStr = dataset.getName();
        String trimmedAndRemovedExtraSpacesStr =
                ActionWordUtil.replaceExtraSpacesInText(originalStr.trim());
        String replacedSpacesStr =
                trimmedAndRemovedExtraSpacesStr.replace(SPACE_CHAR, ACTION_WORD_UNDERSCORE);
        return DOUBLE_TAB_CHAR + ACROBAT_CHAR + replacedSpacesStr + NEW_LINE_CHAR;
    }

    private String generateDatasetParamNamesAndValues(
            Dataset dataset, Map<String, String> parameterNameToTypeMap) {
        Set<DatasetParamValue> datasetParamValues = dataset.getParameterValues();
        Map<String, String> paramNameValueMap =
                datasetParamValues.stream()
                        .collect(
                                Collectors.toMap(
                                        datasetParamValue -> datasetParamValue.getParameter().getName(),
                                        DatasetParamValue::getParamValue));

        TreeMap<String, String> sortedMap = new TreeMap<>(paramNameValueMap);
        return generateSortedDatasetParamNamesAndValues(sortedMap, parameterNameToTypeMap);
    }

    private String generateSortedDatasetParamNamesAndValues(
            TreeMap<String, String> paramNames, Map<String, String> parameterNameToTypeMap) {
        StringBuilder lineBuilder1 = new StringBuilder();
        lineBuilder1.append(DOUBLE_TAB_CHAR);
        StringBuilder lineBuilder2 = new StringBuilder();
        lineBuilder2.append(DOUBLE_TAB_CHAR);

        paramNames.forEach(
                (paramName, paramValue) ->
                        addParamNameAndValueIntoTwoBuilders(
                                lineBuilder1, lineBuilder2, paramName, paramValue, parameterNameToTypeMap));

        lineBuilder1.append(VERTICAL_BAR).append(NEW_LINE_CHAR);
        lineBuilder2.append(VERTICAL_BAR);
        return lineBuilder1.append(lineBuilder2).toString();
    }

    private void addParamNameAndValueIntoTwoBuilders(
            StringBuilder lineBuilder1,
            StringBuilder lineBuilder2,
            String paramName,
            String paramValue,
            Map<String, String> parameterNameToTypeMap) {
        addInfoIntoBuilder(lineBuilder1, paramName);
        String trimmedParamValue = paramValue.trim();
        if ("string".equals(parameterNameToTypeMap.get(paramName))) {
            String updatedParamValue = wrapWithDoubleQuotes(trimmedParamValue);
            addInfoIntoBuilder(lineBuilder2, updatedParamValue);
        } else {
            addInfoIntoBuilder(lineBuilder2, trimmedParamValue);
        }
    }

    private void addInfoIntoBuilder(StringBuilder lineBuilder, String info) {
        lineBuilder.append(VERTICAL_BAR).append(SPACE_CHAR).append(info).append(SPACE_CHAR);
    }

    private void addLanguageAndFeatureToBuilder(
            StringBuilder builder,
            String testCaseName,
            String language,
            Locale locale,
            MessageSource messageSource) {
        StringBuilder subBuilder = new StringBuilder();
        subBuilder
                .append(SCRIPT_LANGUAGE_LABEL)
                .append(language)
                .append(NEW_LINE_CHAR)
                .append(messageSource.getMessage("testcase.bdd.script.label.feature", null, locale))
                .append(testCaseName);
        builder.insert(0, subBuilder);
    }

    public String writeBddStepScript(
            KeywordTestStep testStep,
            MessageSource messageSource,
            Locale locale,
            boolean escapeArrows,
            boolean wrapParameterValuesWithDoubleQuotes) {
        StringBuilder builder = new StringBuilder();
        String internationalizedKeywordScript =
                messageSource.getMessage(testStep.getKeyword().i18nKeywordNameKey(), null, locale);
        String actionWordScript =
                testStep.writeTestStepActionWordScript(escapeArrows, wrapParameterValuesWithDoubleQuotes);
        String dataTable = testStep.getDatatable();
        String docstring = testStep.getDocstring();
        String comment = testStep.getComment();
        builder.append(internationalizedKeywordScript).append(SPACE_CHAR).append(actionWordScript);
        if (!StringUtils.isBlank(dataTable)) {
            builder
                    .append(NEW_LINE_CHAR)
                    .append(TRIPLE_TAB_CHAR)
                    .append(dataTable.replace(NEW_LINE_CHAR, NEW_LINE_CHAR + TRIPLE_TAB_CHAR));
        }
        if (!StringUtils.isBlank(docstring)) {
            builder
                    .append(NEW_LINE_CHAR)
                    .append(TRIPLE_TAB_CHAR)
                    .append(DOCSTRING_MARKER)
                    .append(NEW_LINE_CHAR)
                    .append(TRIPLE_TAB_CHAR)
                    .append(docstring.replace(NEW_LINE_CHAR, NEW_LINE_CHAR + TRIPLE_TAB_CHAR))
                    .append(NEW_LINE_CHAR)
                    .append(TRIPLE_TAB_CHAR)
                    .append(DOCSTRING_MARKER);
        }
        if (!StringUtils.isBlank(comment)) {
            builder
                    .append(NEW_LINE_CHAR)
                    .append(TRIPLE_TAB_CHAR)
                    .append(COMMENT_MARKER)
                    .append(comment.replace(NEW_LINE_CHAR, NEW_LINE_CHAR + TRIPLE_TAB_CHAR + COMMENT_MARKER));
        }
        return builder.toString();
    }
}
