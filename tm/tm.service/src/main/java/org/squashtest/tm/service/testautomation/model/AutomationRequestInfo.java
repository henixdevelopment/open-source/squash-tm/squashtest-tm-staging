/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.model;

import java.util.Date;
import org.squashtest.tm.domain.project.AutomationWorkflowType;
import org.squashtest.tm.domain.testcase.TestCaseKind;

public class AutomationRequestInfo {

    private Long id;
    private AutomationWorkflowType workflowType;
    private Long scmRepositoryId;
    private TestCaseKind testCaseKind;
    private Date transmissionDate;

    public AutomationRequestInfo(
            Long id,
            AutomationWorkflowType workflowType,
            Long scmRepositoryId,
            TestCaseKind testCaseKind,
            Date transmissionDate) {
        this.id = id;
        this.workflowType = workflowType;
        this.scmRepositoryId = scmRepositoryId;
        this.testCaseKind = testCaseKind;
        this.transmissionDate = transmissionDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AutomationWorkflowType getWorkflowType() {
        return workflowType;
    }

    public void setWorkflowType(AutomationWorkflowType workflowType) {
        this.workflowType = workflowType;
    }

    public Long getScmRepositoryId() {
        return scmRepositoryId;
    }

    public void setScmRepositoryId(Long scmRepositoryId) {
        this.scmRepositoryId = scmRepositoryId;
    }

    public TestCaseKind getTestCaseKind() {
        return testCaseKind;
    }

    public void setTestCaseKind(TestCaseKind testCaseKind) {
        this.testCaseKind = testCaseKind;
    }

    public Date getTransmissionDate() {
        return transmissionDate;
    }

    public void setTransmissionDate(Date transmissionDate) {
        this.transmissionDate = transmissionDate;
    }

    public boolean canBeTransmitted() {
        return hasScriptedOrKeywordTestCaseWithScmAndNativeSimplifiedWorkflow()
                || !isNativeSimplified();
    }

    public boolean hasScriptedOrKeywordTestCaseWithScmAndNativeSimplifiedWorkflow() {
        return hasScriptedOrKeywordTestcase() && scmRepositoryId != null && isNativeSimplified();
    }

    public boolean isNativeSimplified() {
        return AutomationWorkflowType.NATIVE_SIMPLIFIED.equals(workflowType);
    }

    public boolean hasScriptedOrKeywordTestcase() {
        return TestCaseKind.KEYWORD.equals(testCaseKind) || TestCaseKind.GHERKIN.equals(testCaseKind);
    }
}
