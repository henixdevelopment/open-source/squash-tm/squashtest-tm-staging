/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ProjectViewDto extends ProjectDto {

    private List<BugTrackerDto> availableBugtrackers;
    private List<String> bugtrackerProjectNames;
    private AttachmentListDto attachmentList;
    private boolean templateLinkedToProjects;
    private Map<String, Boolean> allowedStatuses;
    private Map<String, Boolean> statusesInUse;
    private List<InfoListDto> infoLists;
    private List<PartyProjectPermissionDto> partyProjectPermissions;
    private List<ScmServerDto> availableScmServers;
    private List<TestAutomationServerDto> availableTestAutomationServers;
    private List<AiServerDto> availableAiServers;
    private List<TestAutomationProjectDto> boundTestAutomationProjects;
    private List<MilestoneProjectViewDto> boundMilestonesInformation = new ArrayList<>();
    private List<ProjectPluginDto> availablePlugins;
    private List<ProfileDto> availableProfiles;
    private boolean hasTemplateConfigurablePluginBinding;

    private List<PivotFormatImportDto> existingImports;

    public List<BugTrackerDto> getAvailableBugtrackers() {
        return availableBugtrackers;
    }

    public void setAvailableBugtrackers(List<BugTrackerDto> availableBugtrackers) {
        this.availableBugtrackers = availableBugtrackers;
    }

    public List<String> getBugtrackerProjectNames() {
        return bugtrackerProjectNames;
    }

    public void setBugtrackerProjectNames(List<String> bugtrackerProjectNames) {
        this.bugtrackerProjectNames = bugtrackerProjectNames;
    }

    public AttachmentListDto getAttachmentList() {
        return attachmentList;
    }

    public void setAttachmentList(AttachmentListDto attachmentList) {
        this.attachmentList = attachmentList;
    }

    public boolean isTemplateLinkedToProjects() {
        return templateLinkedToProjects;
    }

    public void setTemplateLinkedToProjects(boolean templateLinkedToProjects) {
        this.templateLinkedToProjects = templateLinkedToProjects;
    }

    public Map<String, Boolean> getAllowedStatuses() {
        return allowedStatuses;
    }

    public void setAllowedStatuses(Map<String, Boolean> allowedStatuses) {
        this.allowedStatuses = allowedStatuses;
    }

    public Map<String, Boolean> getStatusesInUse() {
        return statusesInUse;
    }

    public void setStatusesInUse(Map<String, Boolean> statusesInUse) {
        this.statusesInUse = statusesInUse;
    }

    public List<InfoListDto> getInfoLists() {
        return infoLists;
    }

    public void setInfoLists(List<InfoListDto> infoLists) {
        this.infoLists = infoLists;
    }

    public List<PartyProjectPermissionDto> getPartyProjectPermissions() {
        return partyProjectPermissions;
    }

    public void setPartyProjectPermissions(List<PartyProjectPermissionDto> partyProjectPermissions) {
        this.partyProjectPermissions = partyProjectPermissions;
    }

    public List<ScmServerDto> getAvailableScmServers() {
        return availableScmServers;
    }

    public void setAvailableScmServers(List<ScmServerDto> availableScmServers) {
        this.availableScmServers = availableScmServers;
    }

    public List<TestAutomationServerDto> getAvailableTestAutomationServers() {
        return availableTestAutomationServers;
    }

    public void setAvailableTestAutomationServers(
            List<TestAutomationServerDto> availableTestAutomationServers) {
        this.availableTestAutomationServers = availableTestAutomationServers;
    }

    public List<AiServerDto> getAvailableAiServers() {
        return availableAiServers;
    }

    public void setAvailableAiServers(List<AiServerDto> availableAiServers) {
        this.availableAiServers = availableAiServers;
    }

    public List<TestAutomationProjectDto> getBoundTestAutomationProjects() {
        return boundTestAutomationProjects;
    }

    public void setBoundTestAutomationProjects(
            List<TestAutomationProjectDto> boundTestAutomationProjects) {
        this.boundTestAutomationProjects = boundTestAutomationProjects;
    }

    public List<MilestoneProjectViewDto> getBoundMilestonesInformation() {
        return boundMilestonesInformation;
    }

    public void setBoundMilestonesInformation(
            List<MilestoneProjectViewDto> boundMilestonesInformation) {
        this.boundMilestonesInformation = boundMilestonesInformation;
    }

    public List<ProjectPluginDto> getAvailablePlugins() {
        return availablePlugins;
    }

    public void setAvailablePlugins(List<ProjectPluginDto> availablePlugins) {
        this.availablePlugins = availablePlugins;
    }

    public List<ProfileDto> getAvailableProfiles() {
        return availableProfiles;
    }

    public void setAvailableProfiles(List<ProfileDto> availableProfiles) {
        this.availableProfiles = availableProfiles;
    }

    public boolean isHasTemplateConfigurablePluginBinding() {
        return hasTemplateConfigurablePluginBinding;
    }

    public void setHasTemplateConfigurablePluginBinding(
            boolean hasTemplateConfigurablePluginBinding) {
        this.hasTemplateConfigurablePluginBinding = hasTemplateConfigurablePluginBinding;
    }

    public List<PivotFormatImportDto> getExistingImports() {
        return existingImports;
    }

    public void setExistingImports(List<PivotFormatImportDto> existingImports) {
        this.existingImports = existingImports;
    }
}
