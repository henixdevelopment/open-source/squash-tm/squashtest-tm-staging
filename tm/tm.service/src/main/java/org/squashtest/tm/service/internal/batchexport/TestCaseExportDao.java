/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchexport;

import static org.jooq.impl.DSL.count;
import static org.jooq.impl.DSL.countDistinct;
import static org.jooq.impl.DSL.groupConcat;
import static org.jooq.impl.DSL.groupConcatDistinct;
import static org.jooq.impl.DSL.when;
import static org.squashtest.tm.jooq.domain.Tables.ATTACHMENT;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_TEST_TECHNOLOGY;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATION_REQUEST;
import static org.squashtest.tm.jooq.domain.Tables.CALL_TEST_STEP;
import static org.squashtest.tm.jooq.domain.Tables.DATASET;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_ISSUES_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.INFO_LIST_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.KEYWORD_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.PARAMETER;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_COVERAGE;
import static org.squashtest.tm.jooq.domain.Tables.SCRIPTED_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TCLN_RELATIONSHIP;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_FOLDER;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_CONTENT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_STEPS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ATTACHMENT_COUNT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CALLER_COUNT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CALL_STEP_COUNT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CONTENT_ORDER;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.DATASET_COUNT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_COUNT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ISSUE_COUNT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ITERATION_COUNT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.MILESTONES_ALIAS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.MILESTONE_COUNT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.MILESTONE_END_DATE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.MILESTONE_STATUS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PARAMETER_COUNT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.REQ_VERSION_COVERAGE_COUNT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TC_KIND;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_STEP_COUNT;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.SelectSelectStep;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.infolist.ListItemReference;
import org.squashtest.tm.domain.testcase.TestCaseAutomatable;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.domain.testcase.TestCaseKind;
import org.squashtest.tm.domain.testcase.TestCaseStatus;
import org.squashtest.tm.jooq.domain.tables.InfoListItem;
import org.squashtest.tm.service.internal.batchexport.models.CoverageModel;
import org.squashtest.tm.service.internal.batchexport.models.DatasetModel;
import org.squashtest.tm.service.internal.batchexport.models.ExportModel;
import org.squashtest.tm.service.internal.batchexport.models.ExportModel.CustomField;
import org.squashtest.tm.service.internal.batchexport.models.ParameterModel;
import org.squashtest.tm.service.internal.batchexport.models.TestCaseModel;
import org.squashtest.tm.service.internal.batchexport.models.TestStepModel;
import org.squashtest.tm.service.internal.library.PathService;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

@Repository
public class TestCaseExportDao {

    private static final String TEST_CASE_IDS = "testCaseIds";
    private static final InfoListItem TEST_CASE_NATURE = INFO_LIST_ITEM.as("nature");
    private static final InfoListItem TEST_CASE_TYPE = INFO_LIST_ITEM.as("type");

    @PersistenceContext private EntityManager em;

    @Inject private DSLContext dsl;

    @Inject private PathService pathService;

    public TestCaseExportDao() {
        super();
    }

    public ExportModel findModel(List<Long> tclnIds) {

        ExportModel model = new ExportModel();

        List<TestCaseModel> tclnModels = findTestCaseModels(tclnIds);
        List<TestStepModel> stepModels = findStepsModel(tclnIds);
        List<ParameterModel> paramModels = findParametersModel(tclnIds);
        List<DatasetModel> datasetModels = findDatasetsModel(tclnIds);
        List<CoverageModel> coverageModels = findTestCaseCoverageModel(tclnIds);

        ExportDaoUtils.setPathForCoverage(em, pathService, coverageModels);

        model.setCoverages(coverageModels);
        model.setTestCases(tclnModels);
        model.setTestSteps(stepModels);
        model.setParameters(paramModels);
        model.setDatasets(datasetModels);

        return model;
    }

    public ExportModel findSimpleModel(List<Long> tclnIds) {

        ExportModel model = new ExportModel();

        List<TestCaseModel> tclnModels = findTestCaseModels(tclnIds);

        model.setTestCases(tclnModels);

        return model;
    }

    private List<CoverageModel> findTestCaseCoverageModel(List<Long> tcIds) {
        return ExportDaoUtils.loadModels(
                em, "testCase.excelExportCoverage", tcIds, TEST_CASE_IDS, CoverageModel.class);
    }

    private List<TestCaseModel> loadTestCaseModelsFromFolderWithJOOQ(List<Long> tclnIds) {
        List<TestCaseModel> result = new ArrayList<>();
        final Field<Integer> contentOrderField = TCLN_RELATIONSHIP.CONTENT_ORDER;

        getTestCaseModelSelect(contentOrderField)
                .from(TEST_CASE)
                .join(TEST_CASE_LIBRARY_NODE)
                .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .join(PROJECT)
                .on(PROJECT.PROJECT_ID.eq(TEST_CASE_LIBRARY_NODE.PROJECT_ID))
                .join(TEST_CASE_NATURE)
                .on(TEST_CASE_NATURE.ITEM_ID.eq(TEST_CASE.TC_NATURE))
                .join(TEST_CASE_TYPE)
                .on(TEST_CASE_TYPE.ITEM_ID.eq(TEST_CASE.TC_TYPE))
                .join(TCLN_RELATIONSHIP)
                .on(TCLN_RELATIONSHIP.DESCENDANT_ID.eq(TEST_CASE.TCLN_ID))
                .join(TEST_CASE_FOLDER)
                .on(TEST_CASE_FOLDER.TCLN_ID.eq(TCLN_RELATIONSHIP.ANCESTOR_ID))
                .leftJoin(SCRIPTED_TEST_CASE)
                .on(SCRIPTED_TEST_CASE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .leftJoin(KEYWORD_TEST_CASE)
                .on(KEYWORD_TEST_CASE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .leftJoin(EXPLORATORY_TEST_CASE)
                .on(EXPLORATORY_TEST_CASE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .leftJoin(AUTOMATION_REQUEST)
                .on(TEST_CASE.AUTOMATION_REQUEST_ID.eq(AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID))
                .leftJoin(AUTOMATED_TEST_TECHNOLOGY)
                .on(TEST_CASE.AUTOMATED_TEST_TECHNOLOGY.eq(AUTOMATED_TEST_TECHNOLOGY.AT_TECHNOLOGY_ID))
                .where(TEST_CASE.TCLN_ID.in(tclnIds))
                .fetch()
                .forEach(
                        record -> {
                            TestCaseModel model = createTestCaseModelFromQueryResult(record);
                            result.add(model);
                        });

        return result;
    }

    private List<TestCaseModel> loadTestCaseModelsFromLibraryWithJOOQ(List<Long> tclnIds) {

        List<TestCaseModel> result = new ArrayList<>();
        final Field<Integer> contentOrderField = TEST_CASE_LIBRARY_CONTENT.CONTENT_ORDER;

        getTestCaseModelSelect(contentOrderField)
                .from(TEST_CASE)
                .join(TEST_CASE_LIBRARY_NODE)
                .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .join(PROJECT)
                .on(PROJECT.PROJECT_ID.eq(TEST_CASE_LIBRARY_NODE.PROJECT_ID))
                .join(TEST_CASE_NATURE)
                .on(TEST_CASE_NATURE.ITEM_ID.eq(TEST_CASE.TC_NATURE))
                .join(TEST_CASE_TYPE)
                .on(TEST_CASE_TYPE.ITEM_ID.eq(TEST_CASE.TC_TYPE))
                .join(TEST_CASE_LIBRARY_CONTENT)
                .on(TEST_CASE_LIBRARY_CONTENT.CONTENT_ID.eq(TEST_CASE.TCLN_ID))
                .leftJoin(SCRIPTED_TEST_CASE)
                .on(SCRIPTED_TEST_CASE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .leftJoin(KEYWORD_TEST_CASE)
                .on(KEYWORD_TEST_CASE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .leftJoin(EXPLORATORY_TEST_CASE)
                .on(EXPLORATORY_TEST_CASE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .leftJoin(AUTOMATION_REQUEST)
                .on(TEST_CASE.AUTOMATION_REQUEST_ID.eq(AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID))
                .leftJoin(AUTOMATED_TEST_TECHNOLOGY)
                .on(TEST_CASE.AUTOMATED_TEST_TECHNOLOGY.eq(AUTOMATED_TEST_TECHNOLOGY.AT_TECHNOLOGY_ID))
                .where(TEST_CASE.TCLN_ID.in(tclnIds))
                .fetch()
                .forEach(
                        record -> {
                            TestCaseModel model = createTestCaseModelFromQueryResult(record);
                            result.add(model);
                        });

        return result;
    }

    private SelectSelectStep<Record> getTestCaseModelSelect(Field<Integer> contentOrder) {
        return dsl.select(
                TEST_CASE.TCLN_ID,
                TEST_CASE.UUID,
                TEST_CASE.REFERENCE,
                TEST_CASE.IMPORTANCE,
                TEST_CASE.IMPORTANCE_AUTO,
                TEST_CASE.AUTOMATABLE,
                TEST_CASE.TC_STATUS,
                TEST_CASE.PREREQUISITE,
                TEST_CASE_LIBRARY_NODE.DESCRIPTION,
                TEST_CASE_LIBRARY_NODE.NAME,
                TEST_CASE_LIBRARY_NODE.CREATED_BY,
                TEST_CASE_LIBRARY_NODE.CREATED_ON,
                TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY,
                TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_ON,
                PROJECT.PROJECT_ID,
                PROJECT.NAME,
                PROJECT.ALLOW_AUTOMATION_WORKFLOW,
                TEST_CASE_NATURE.LABEL,
                TEST_CASE_NATURE.CODE,
                TEST_CASE_TYPE.LABEL,
                TEST_CASE_TYPE.CODE,
                SCRIPTED_TEST_CASE.TCLN_ID,
                SCRIPTED_TEST_CASE.SCRIPT,
                KEYWORD_TEST_CASE.TCLN_ID,
                getMilestonesField().as(MILESTONES_ALIAS),
                getAttachmentCountField().as(ATTACHMENT_COUNT),
                getReqVersionCoverageCountField().as(REQ_VERSION_COVERAGE_COUNT),
                getIterationCountField().as(ITERATION_COUNT),
                getCallerCountField().as(CALLER_COUNT),
                contentOrder.as(CONTENT_ORDER),
                getDatasetCountField().as(DATASET_COUNT),
                getMilestoneCountField().as(MILESTONE_COUNT),
                getTestStepCountField().as(TEST_STEP_COUNT),
                getParameterCount().as(PARAMETER_COUNT),
                getExecutionCountField().as(EXECUTION_COUNT),
                getCallStepCountField().as(CALL_STEP_COUNT),
                getTcKindField().as(TC_KIND),
                AUTOMATION_REQUEST.AUTOMATION_PRIORITY,
                AUTOMATION_REQUEST.TRANSMITTED_ON,
                AUTOMATION_REQUEST.REQUEST_STATUS,
                TEST_CASE.TA_TEST,
                AUTOMATED_TEST_TECHNOLOGY.NAME,
                TEST_CASE.SCM_REPOSITORY_ID,
                TEST_CASE.AUTOMATED_TEST_REFERENCE,
                getMilestoneStatusField().as(MILESTONE_STATUS),
                getMilestoneEndDateField().as(MILESTONE_END_DATE),
                getIssueCountField().as(ISSUE_COUNT),
                TEST_CASE.DRAFTED_BY_AI);
    }

    private Field<String> getMilestonesField() {
        return DSL.select(groupConcatDistinct(MILESTONE.LABEL).separator("|"))
                .from(MILESTONE)
                .join(MILESTONE_TEST_CASE)
                .on(MILESTONE.MILESTONE_ID.eq(MILESTONE_TEST_CASE.MILESTONE_ID))
                .where(MILESTONE_TEST_CASE.TEST_CASE_ID.eq(TEST_CASE.TCLN_ID))
                .asField();
    }

    private Field<Integer> getAttachmentCountField() {
        return DSL.select(count(ATTACHMENT.ATTACHMENT_ID))
                .from(ATTACHMENT)
                .where(ATTACHMENT.ATTACHMENT_LIST_ID.eq(TEST_CASE_LIBRARY_NODE.ATTACHMENT_LIST_ID))
                .asField();
    }

    private Field<Integer> getReqVersionCoverageCountField() {
        return DSL.select(count(REQUIREMENT_VERSION_COVERAGE.REQUIREMENT_VERSION_COVERAGE_ID))
                .from(REQUIREMENT_VERSION_COVERAGE)
                .where(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID.eq(TEST_CASE.TCLN_ID))
                .asField();
    }

    private Field<Integer> getIterationCountField() {
        return DSL.select(count(ITEM_TEST_PLAN_LIST.ITERATION_ID))
                .from(ITEM_TEST_PLAN_LIST)
                .join(ITERATION_TEST_PLAN_ITEM)
                .on(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.eq(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
                .where(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .asField();
    }

    private Field<Integer> getCallerCountField() {
        return DSL.select(countDistinct(TEST_CASE_STEPS.TEST_CASE_ID))
                .from(TEST_CASE_STEPS)
                .where(TEST_CASE_STEPS.TEST_CASE_ID.eq(TEST_CASE.TCLN_ID))
                .asField();
    }

    private Field<Integer> getDatasetCountField() {
        return DSL.select(count(DATASET.DATASET_ID))
                .from(DATASET)
                .where(DATASET.TEST_CASE_ID.eq(TEST_CASE.TCLN_ID))
                .asField();
    }

    private Field<Integer> getMilestoneCountField() {
        return DSL.select(count(MILESTONE_TEST_CASE.MILESTONE_ID))
                .from(MILESTONE_TEST_CASE)
                .where(MILESTONE_TEST_CASE.TEST_CASE_ID.eq(TEST_CASE.TCLN_ID))
                .asField();
    }

    private Field<Integer> getTestStepCountField() {
        return DSL.select(count(TEST_CASE_STEPS.STEP_ID))
                .from(TEST_CASE_STEPS)
                .where(TEST_CASE_STEPS.TEST_CASE_ID.eq(TEST_CASE.TCLN_ID))
                .asField();
    }

    private Field<Integer> getParameterCount() {
        return DSL.select(count(PARAMETER.PARAM_ID))
                .from(PARAMETER)
                .where(PARAMETER.TEST_CASE_ID.eq(TEST_CASE.TCLN_ID))
                .asField();
    }

    private Field<Integer> getExecutionCountField() {
        return DSL.select(count(EXECUTION.EXECUTION_ID))
                .from(EXECUTION)
                .where(EXECUTION.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .asField();
    }

    private Field<Integer> getCallStepCountField() {
        return DSL.select(countDistinct(CALL_TEST_STEP.TEST_STEP_ID))
                .from(CALL_TEST_STEP)
                .where(CALL_TEST_STEP.CALLED_TEST_CASE_ID.eq(TEST_CASE.TCLN_ID))
                .asField();
    }

    private Field<String> getTcKindField() {
        return (when(SCRIPTED_TEST_CASE.TCLN_ID.isNotNull(), TestCaseKind.GHERKIN.name())
                .when(KEYWORD_TEST_CASE.TCLN_ID.isNotNull(), TestCaseKind.KEYWORD.name())
                .when(EXPLORATORY_TEST_CASE.TCLN_ID.isNotNull(), TestCaseKind.EXPLORATORY.name())
                .otherwise(TestCaseKind.STANDARD.name()));
    }

    private Field<Integer> getMilestoneStatusField() {
        return DSL.select(groupConcat(MILESTONE.STATUS).orderBy(MILESTONE.LABEL).separator("|"))
                .from(MILESTONE)
                .join(MILESTONE_TEST_CASE)
                .on(MILESTONE.MILESTONE_ID.eq(MILESTONE_TEST_CASE.MILESTONE_ID))
                .where(MILESTONE_TEST_CASE.TEST_CASE_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .asField();
    }

    private Field<Object> getMilestoneEndDateField() {
        return DSL.select(groupConcat(MILESTONE.END_DATE).orderBy(MILESTONE.LABEL).separator("|"))
                .from(MILESTONE)
                .join(MILESTONE_TEST_CASE)
                .on(MILESTONE.MILESTONE_ID.eq(MILESTONE_TEST_CASE.MILESTONE_ID))
                .where(MILESTONE_TEST_CASE.TEST_CASE_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .asField();
    }

    private Field<Integer> getIssueCountField() {
        return DSL.select(countDistinct(EXECUTION_ISSUES_CLOSURE.ISSUE_ID))
                .from(EXECUTION_ISSUES_CLOSURE)
                .join(EXECUTION)
                .on(EXECUTION_ISSUES_CLOSURE.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                .where(EXECUTION.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                .asField();
    }

    private TestCaseModel createTestCaseModelFromQueryResult(Record record) {
        return new TestCaseModel(
                record.get(PROJECT.PROJECT_ID),
                record.get(PROJECT.ALLOW_AUTOMATION_WORKFLOW),
                record.get(PROJECT.NAME),
                ((Integer) Objects.requireNonNull(record.get(CONTENT_ORDER)) + 1),
                record.get(TEST_CASE.TCLN_ID),
                record.get(TEST_CASE.UUID),
                record.get(TEST_CASE.REFERENCE),
                record.get(TEST_CASE_LIBRARY_NODE.NAME),
                (String) record.get(MILESTONES_ALIAS),
                record.get(TEST_CASE.IMPORTANCE_AUTO),
                TestCaseImportance.valueOf(record.get(TEST_CASE.IMPORTANCE)),
                new ListItemReference(
                        record.get(TEST_CASE_NATURE.CODE), record.get(TEST_CASE_NATURE.LABEL)),
                new ListItemReference(record.get(TEST_CASE_TYPE.CODE), record.get(TEST_CASE_TYPE.LABEL)),
                TestCaseStatus.valueOf(record.get(TEST_CASE.TC_STATUS)),
                TestCaseAutomatable.valueOf(record.get(TEST_CASE.AUTOMATABLE)),
                record.get(TEST_CASE_LIBRARY_NODE.DESCRIPTION),
                record.get(TEST_CASE.PREREQUISITE),
                ((Integer) Objects.requireNonNull(record.get(REQ_VERSION_COVERAGE_COUNT))).longValue(),
                ((Integer) Objects.requireNonNull(record.get(CALLER_COUNT))).longValue(),
                Long.valueOf(Objects.requireNonNull(record.get(ATTACHMENT_COUNT)).toString()),
                ((Integer) Objects.requireNonNull(record.get(ITERATION_COUNT))).longValue(),
                record.get(TEST_CASE_LIBRARY_NODE.CREATED_ON),
                record.get(TEST_CASE_LIBRARY_NODE.CREATED_BY),
                record.get(TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_ON),
                record.get(TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY),
                TestCaseKind.valueOf((String) record.get(TC_KIND)),
                record.get(SCRIPTED_TEST_CASE.SCRIPT),
                ((Integer) Objects.requireNonNull(record.get(DATASET_COUNT))).longValue(),
                ((Integer) Objects.requireNonNull(record.get(MILESTONE_COUNT))).longValue(),
                ((Integer) Objects.requireNonNull(record.get(TEST_STEP_COUNT))).longValue(),
                ((Integer) Objects.requireNonNull(record.get(PARAMETER_COUNT))).longValue(),
                ((Integer) Objects.requireNonNull(record.get(EXECUTION_COUNT))).longValue(),
                record.get(AUTOMATION_REQUEST.AUTOMATION_PRIORITY),
                record.get(AUTOMATION_REQUEST.TRANSMITTED_ON),
                record.get(AUTOMATION_REQUEST.REQUEST_STATUS),
                record.get(TEST_CASE.TA_TEST),
                record.get(AUTOMATED_TEST_TECHNOLOGY.NAME),
                record.get(TEST_CASE.SCM_REPOSITORY_ID),
                record.get(TEST_CASE.AUTOMATED_TEST_REFERENCE),
                (String) record.get(RequestAliasesConstants.MILESTONE_STATUS),
                (String) record.get(RequestAliasesConstants.MILESTONE_END_DATE),
                ((Integer) Objects.requireNonNull(record.get(CALL_STEP_COUNT))).longValue(),
                ((Integer) Objects.requireNonNull(record.get(ISSUE_COUNT))).longValue(),
                record.get(TEST_CASE.DRAFTED_BY_AI));
    }

    private List<TestCaseModel> findTestCaseModels(List<Long> tclnIds) {

        List<TestCaseModel> models = new ArrayList<>(tclnIds.size());
        List<TestCaseModel> buffer;

        // get the models
        buffer = loadTestCaseModelsFromFolderWithJOOQ(tclnIds);
        models.addAll(buffer);

        buffer = loadTestCaseModelsFromLibraryWithJOOQ(tclnIds);
        models.addAll(buffer);

        // get the cufs
        List<CustomField> cufModels =
                ExportDaoUtils.loadModels(
                        em, "testCase.excelExportCUF", tclnIds, TEST_CASE_IDS, CustomField.class);

        // add them to the test case models
        for (TestCaseModel model : models) {
            Long id = model.getId();
            ListIterator<CustomField> cufIter = cufModels.listIterator();

            while (cufIter.hasNext()) {
                CustomField cuf = cufIter.next();
                if (id.equals(cuf.getOwnerId())) {
                    model.addCuf(cuf);
                    cufIter.remove();
                }
            }
        }

        // end
        return models;
    }

    private List<TestStepModel> findStepsModel(List<Long> tcIds) {

        List<TestStepModel> models = new ArrayList<>(tcIds.size());
        List<TestStepModel> buffer;

        buffer =
                ExportDaoUtils.loadModels(
                        em, "testStep.excelExportActionSteps", tcIds, TEST_CASE_IDS, TestStepModel.class);
        models.addAll(buffer);

        buffer =
                ExportDaoUtils.loadModels(
                        em, "testStep.excelExportCallSteps", tcIds, TEST_CASE_IDS, TestStepModel.class);
        models.addAll(buffer);

        // get the cufs
        List<CustomField> cufModels =
                ExportDaoUtils.loadModels(
                        em, "testStep.excelExportCUF", tcIds, TEST_CASE_IDS, CustomField.class);

        // add them to the test case models
        for (TestStepModel model : models) {
            Long id = model.getId();
            ListIterator<CustomField> cufIter = cufModels.listIterator();

            while (cufIter.hasNext()) {
                CustomField cuf = cufIter.next();
                if (id.equals(cuf.getOwnerId())) {
                    model.addCuf(cuf);
                    cufIter.remove();
                }
            }
        }

        // done
        return models;
    }

    private List<ParameterModel> findParametersModel(List<Long> tcIds) {
        return ExportDaoUtils.loadModels(
                em, "parameter.excelExport", tcIds, TEST_CASE_IDS, ParameterModel.class);
    }

    private List<DatasetModel> findDatasetsModel(List<Long> tcIds) {

        return ExportDaoUtils.loadModels(
                em, "dataset.excelExport", tcIds, TEST_CASE_IDS, DatasetModel.class);
    }
}
