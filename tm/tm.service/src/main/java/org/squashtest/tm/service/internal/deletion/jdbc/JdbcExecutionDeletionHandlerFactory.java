/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion.jdbc;

import java.util.Collection;
import java.util.UUID;
import javax.persistence.EntityManager;
import org.jooq.DSLContext;
import org.springframework.stereotype.Component;
import org.squashtest.tm.service.internal.attachment.AttachmentRepository;

@Component
public class JdbcExecutionDeletionHandlerFactory {

    private final DSLContext dslContext;
    private final EntityManager entityManager;
    private final AttachmentRepository attachmentRepository;
    private final JdbcBatchReorderHelper reorderHelper;

    public JdbcExecutionDeletionHandlerFactory(
            DSLContext dslContext,
            EntityManager entityManager,
            AttachmentRepository attachmentRepository,
            JdbcBatchReorderHelper reorderHelper) {
        this.dslContext = dslContext;
        this.attachmentRepository = attachmentRepository;
        this.reorderHelper = reorderHelper;
        this.entityManager = entityManager;
    }

    public JdbcExecutionDeletionHandler build(Collection<Long> executionIds) {
        return build(executionIds, UUID.randomUUID().toString());
    }

    public JdbcExecutionDeletionHandler build(Collection<Long> executionIds, String operationId) {
        return new JdbcExecutionDeletionHandler(
                executionIds, dslContext, attachmentRepository, reorderHelper, operationId, entityManager);
    }
}
