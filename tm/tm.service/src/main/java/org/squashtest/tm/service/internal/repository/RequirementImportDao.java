/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import org.jooq.Record3;
import org.jooq.Record4;
import org.jooq.Record5;
import org.jooq.Record6;
import org.squashtest.tm.domain.requirement.RequirementStatus;
import org.squashtest.tm.service.internal.dto.ReqVersionMilestone;

/**
 * This interface provides methods to retrieve the necessary data for the import process from the
 * database, while minimizing the number of database calls. Noticed for stream methods, the data is
 * retrieved lazily Do not change the order of the records. The ImportRequirementFinder class
 * retrieves the values in this order.
 */
public interface RequirementImportDao {

    // ------------ Requirement ------------
    Stream<Record5<String, Long, Boolean, String, Long>> findRemoteSyncRequirements(
            Collection<String> remoteKeys, Collection<Long> remoteSyncId);

    Stream<Record4<String, Long, Boolean, String>> findSyncRequirements(
            Collection<String> remoteKeys, String project);

    Stream<Record6<String, Long, Boolean, Boolean, String, Long>> findNodesByNamesAndProject(
            Collection<String> nodeNames, String projectName);

    Stream<Record5<String, Long, Boolean, String, Long>> findRequirementsByNamesAndProjects(
            Collection<String> nodeNames, Collection<String> projects);

    Stream<Record3<String, Long, Boolean>> findBasicRequirementsByNamesAndProject(
            Collection<String> nodeNames, String projects);

    // ------------ Coverage ------------
    Map<Long, List<Long>> findVerifiedTestCaseIdsByVersionIds(Set<Long> versionIds);

    Map<Long, Map<Long, Long>> findExistingCoveragesByRequirementIds(Collection<Long> requirementIds);

    Map<Long, RequirementStatus> findRequirementStatusesByVersionIds(Collection<Long> versionIds);

    Map<Long, Map<Long, Long>> findExistingLinksByVersionIds(Collection<Long> versionIds);

    Map<Long, Map<Integer, ReqVersionMilestone>> findReqVersionAndMilestonesByReqId(
            Collection<Long> requirementIds);

    String getDefaultRequirementVersionLinkRole();
}
