/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.campaign;

import static org.squashtest.tm.domain.execution.ExecutionStatus.getCanonicalStatusSet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.service.campaign.CampaignStatisticsService;
import org.squashtest.tm.service.campaign.IterationTestPlanManagerService;
import org.squashtest.tm.service.internal.repository.CampaignDao;
import org.squashtest.tm.service.internal.repository.CustomItpiLastExecutionFilterDao;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.statistics.CountOnEnum;
import org.squashtest.tm.service.statistics.campaign.CampaignTestCaseSuccessRateStatistics;
import org.squashtest.tm.service.statistics.campaign.ProgressionStatistics;
import org.squashtest.tm.service.statistics.campaign.ScheduledIteration;
import org.squashtest.tm.service.statistics.campaign.StatisticsBundle;
import org.squashtest.tm.service.statistics.campaign.TestInventoryStatistics;

@Transactional(readOnly = true)
@Service("CampaignStatisticsService")
public class CampaignStatisticsServiceImpl implements CampaignStatisticsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CampaignStatisticsServiceImpl.class);

    private static final String CAMPAIGN_IDS = "campaignIds";

    private static final String CAMPAIGN_ID = "campaignId";

    private static final String ITPI_IDS = "itpiIds";

    @PersistenceContext private EntityManager em;

    @Inject private CampaignDao campaignDao;
    @Inject private ActiveMilestoneHolder activeMilestoneHolder;
    @Inject private IterationTestPlanManagerService itpManagerService;
    @Inject private CustomItpiLastExecutionFilterDao itpiLastExecutionFilterDao;
    @Inject private PermissionEvaluationService permissionService;

    // ************************************ all-in-one methods ******************************

    @Override
    public StatisticsBundle gatherCampaignStatisticsBundle(
            List<Long> campaignIds, boolean lastExecutionScope) {

        boolean isSolitaryCampaign = campaignIds.size() == 1;

        StatisticsBundle bundle =
                lastExecutionScope
                        ? initializeStatisticsBundleForLastExecutionScope(campaignIds)
                        : initializeStatisticsBundle(campaignIds);

        // specific methods
        List<TestInventoryStatistics> inventory =
                isSolitaryCampaign
                        ? gatherSingleCampaignTestInventoryStatistics(campaignIds.get(0))
                        : gatherManyCampaignTestInventoryStatistics(campaignIds);

        ProgressionStatistics progression =
                isSolitaryCampaign
                        ? gatherSingleCampaignProgressionStatistics(campaignIds.get(0))
                        : new ProgressionStatistics();

        // stuff it all
        bundle.setTestInventoryStatistics(inventory);
        bundle.setProgressionStatistics(progression);
        bundle.setSelectedIds(campaignIds);

        // return
        return bundle;
    }

    @Override
    public StatisticsBundle gatherMilestoneStatisticsBundle(boolean lastExecutionScope) {

        List<Long> campaignIds = filterCampaignIdsByActiveMilestone(new ArrayList<>());

        PermissionsUtils.checkPermission(
                permissionService, campaignIds, Permissions.READ.name(), Campaign.class.getName());

        StatisticsBundle bundle =
                lastExecutionScope
                        ? initializeStatisticsBundleForLastExecutionScope(campaignIds)
                        : initializeStatisticsBundle(campaignIds);

        // specific methods
        List<TestInventoryStatistics> inventory = gatherMilestoneTestInventoryStatistics();
        ProgressionStatistics progression =
                new ProgressionStatistics(); // not used in the by-milestone dashboard

        // stuff it all
        bundle.setTestInventoryStatistics(inventory);
        bundle.setProgressionStatistics(progression);

        // return
        return bundle;
    }

    /**
     * For a list of campaigns gathers the statistics into a bundle to generate dashboard graphs.
     *
     * @param campaignIds selected campaigns and child campaigns of selected given campaign libraries
     *     or folders
     * @return StatisticsBundle
     */
    /* Campaign library and folder dashboards are computed in the same way, so a common method is sufficient.
    Unfortunately, there are small differences in perspective when gathering by campaign, iteration or milestone,
    which means that we still have to rely on different methods instead of using this one.*/
    @Override
    public StatisticsBundle gatherMultiStatisticsBundle(
            Map<Long, String> campaignNameMap, boolean lastExecutionScope) {

        List<Long> campaignIds = new ArrayList<>(campaignNameMap.keySet());
        campaignIds = filterCampaignIdsByActiveMilestone(campaignIds);

        // hydrate object with generic methods
        StatisticsBundle bundle =
                lastExecutionScope
                        ? initializeStatisticsBundleForLastExecutionScope(campaignIds)
                        : initializeStatisticsBundle(campaignIds);

        // specific methods
        List<TestInventoryStatistics> inventory =
                gatherMultiCampaignTestInventoryStatistics(campaignNameMap, campaignIds);
        ProgressionStatistics progression =
                new ProgressionStatistics(); // not used in the by-milestone dashboard

        // stuff it all
        bundle.setTestInventoryStatistics(inventory);
        bundle.setProgressionStatistics(progression);
        bundle.setSelectedIds(campaignIds);
        // return
        return bundle;
    }

    // Instantiate the StatisticsBundle and pass it the generic data
    // common to all statistic gathering methods (campaign, folder, library, milestone..)
    // to limit duplicated lines
    private StatisticsBundle initializeStatisticsBundle(List<Long> campaignIds) {
        StatisticsBundle bundle = new StatisticsBundle();

        // common methods
        Map<ExecutionStatus, Integer> testcaseStatuses = gatherTestCaseStatusStatistics(campaignIds);
        Map<TestCaseImportance, Integer> testcaseImportance =
                gatherNonExecutedTestCaseImportanceStatistics(campaignIds);
        CampaignTestCaseSuccessRateStatistics testcaseSuccessRate =
                gatherTestCaseSuccessRateStatistics(campaignIds);

        bundle.setTestCaseStatusStatistics(testcaseStatuses);
        bundle.setNonExecutedTestCaseImportanceStatistics(testcaseImportance);
        bundle.setTestCaseSuccessRateStatistics(testcaseSuccessRate);

        return bundle;
    }

    private StatisticsBundle initializeStatisticsBundleForLastExecutionScope(List<Long> campaignIds) {
        StatisticsBundle bundle = new StatisticsBundle();

        List<Long> itpiIdsInScope =
                itpiLastExecutionFilterDao.gatherLatestItpiIdsForTCInScopeForCampaign(campaignIds);

        // common methods
        Map<ExecutionStatus, Integer> testcaseStatuses =
                gatherTestCaseStatusStatisticsForLastExecScope(itpiIdsInScope);
        Map<TestCaseImportance, Integer> testcaseImportance =
                gatherNonExecutedTestCaseImportanceStatisticsForLastExecScope(itpiIdsInScope);
        CampaignTestCaseSuccessRateStatistics testcaseSuccessRate =
                gatherTestCaseSuccessRateStatisticsForLastExecScope(itpiIdsInScope);

        bundle.setTestCaseStatusStatistics(testcaseStatuses);
        bundle.setNonExecutedTestCaseImportanceStatistics(testcaseImportance);
        bundle.setTestCaseSuccessRateStatistics(testcaseSuccessRate);

        return bundle;
    }

    private List<Long> filterCampaignIdsByActiveMilestone(List<Long> campaignIds) {
        Optional<Milestone> activeMilestone = activeMilestoneHolder.getActiveMilestone();

        if (activeMilestone.isPresent() && !campaignIds.isEmpty()) {
            return campaignDao.filterByMilestone(campaignIds, activeMilestone.get().getId());
        } else if (activeMilestone.isPresent()) {
            return campaignDao.findAllIdsByMilestone(activeMilestone.get().getId());
        }
        return campaignIds;
    }

    /* *********************************** common statistics methods ************************************ */

    /**
     * Given a list of campaign id, gathers and returns the number of test cases grouped by execution
     * status.
     *
     * @param campaignIds
     * @return
     */
    private Map<ExecutionStatus, Integer> gatherTestCaseStatusStatistics(List<Long> campaignIds) {
        List<Object[]> tuples =
                fetchCommonTuples("CampaignStatistics.globaltestinventory", CAMPAIGN_IDS, campaignIds);
        return CountOnEnum.fromTuples(tuples, ExecutionStatus.class)
                .getStatistics(ExecutionStatus::getCanonicalStatus, getCanonicalStatusSet());
    }

    private LinkedHashMap<ExecutionStatus, Integer> gatherTestCaseStatusStatisticsForLastExecScope(
            List<Long> itpiIds) {
        List<Object[]> tuples =
                fetchCommonTuples(
                        "CampaignStatistics.globaltestinventorybylastexecution", ITPI_IDS, itpiIds);
        return CountOnEnum.fromTuples(tuples, ExecutionStatus.class)
                .getStatistics(ExecutionStatus::getCanonicalStatus, getCanonicalStatusSet());
    }

    /**
     * Given a list of campaign id, gathers and returns the number of passed and failed test cases
     * grouped by weight.
     *
     * @param campaignIds
     * @return
     */
    private CampaignTestCaseSuccessRateStatistics gatherTestCaseSuccessRateStatistics(
            List<Long> campaignIds) {
        List<Object[]> tuples =
                fetchCommonTuples("CampaignStatistics.successRate", CAMPAIGN_IDS, campaignIds);
        return CampaignTestCaseSuccessRateStatistics.fromTuples(tuples);
    }

    private CampaignTestCaseSuccessRateStatistics gatherTestCaseSuccessRateStatisticsForLastExecScope(
            List<Long> itpiIds) {
        List<Object[]> tuples =
                fetchCommonTuples("CampaignStatistics.successRateByLastExecution", ITPI_IDS, itpiIds);
        return CampaignTestCaseSuccessRateStatistics.fromTuples(tuples);
    }

    /**
     * Given a list of campaign id, gathers and returns the number of non-executed test cases grouped
     * by weight.
     *
     * @param campaignIds
     */
    private Map<TestCaseImportance, Integer> gatherNonExecutedTestCaseImportanceStatistics(
            List<Long> campaignIds) {
        List<Object[]> tuples =
                fetchCommonTuples(
                        "CampaignStatistics.nonexecutedTestcaseImportance", CAMPAIGN_IDS, campaignIds);
        return CountOnEnum.fromTuples(tuples, TestCaseImportance.class).getStatistics();
    }

    private Map<TestCaseImportance, Integer>
            gatherNonExecutedTestCaseImportanceStatisticsForLastExecScope(List<Long> itpiIds) {
        List<Object[]> tuples =
                fetchCommonTuples(
                        "CampaignStatistics.nonexecutedTestcaseImportanceByLastExecution", ITPI_IDS, itpiIds);
        return CountOnEnum.fromTuples(tuples, TestCaseImportance.class).getStatistics();
    }

    /* ************************************* statistics specific to multiple campaigns************************************** */

    public List<TestInventoryStatistics> gatherManyCampaignTestInventoryStatistics(
            List<Long> campaignIds) {

        Query query = em.createNamedQuery("CampaignStatistics.testinventorybycampaigns");
        query.setParameter(CAMPAIGN_IDS, campaignIds);
        List<Object[]> tuples = query.getResultList();
        return processTestInventory(Collections.emptyMap(), tuples);
    }

    /* ************************************* statistics specific to one lone campaign************************************** */

    /**
     * Given a campaign id, gathers and returns how many tests and at which status are planned one or
     * more campaigns. Only tests part of an iteration count. Those statistics are grouped and sorted
     * by Iteration.
     *
     * @param campaignId
     */
    public List<TestInventoryStatistics> gatherSingleCampaignTestInventoryStatistics(
            long campaignId) {

        Query query = em.createNamedQuery("CampaignStatistics.testinventorybycampaign");
        query.setParameter(CAMPAIGN_ID, campaignId);
        List<Object[]> tuples = query.getResultList();
        return processTestInventory(Collections.emptyMap(), tuples);
    }

    /**
     * Given a campaignId, gathers and return the theoretical and actual cumulative test count by
     * iterations. The theoretical cumulative test count by iterations means how many tests should
     * have been executed per day on the basis of the scheduled start and end of an iteration. The
     * actual cumulative test count means how many tests have been executed so far, each days, during
     * the same period.
     *
     * <p>This assumes that the scheduled start and end dates of each iterations are square : they
     * must all be defined, and must not overlap. In case of errors appropriate messages will be
     * filled instead and data won't be returned.
     *
     * @param campaignId
     * @return
     */
    public ProgressionStatistics gatherSingleCampaignProgressionStatistics(long campaignId) {

        ProgressionStatistics progression = new ProgressionStatistics();

        Query query = em.createNamedQuery("CampaignStatistics.findScheduledIterations");
        query.setParameter("id", campaignId);
        List<ScheduledIteration> scheduledIterations = query.getResultList();

        Query requery = em.createNamedQuery("CampaignStatistics.findExecutionsHistory");
        requery.setParameter("id", campaignId);
        requery.setParameter("nonterminalStatuses", ExecutionStatus.getNonTerminatedStatusSet());
        List<Date> executionHistory = requery.getResultList();

        try {

            // scheduled iterations
            progression.setScheduledIterations(scheduledIterations); // we want them in any case
            // Issue 7635, XSS when generating campaing dashboard
            progression
                    .getScheduledIterations()
                    .forEach(scheduledIteration -> scheduledIteration.setName(scheduledIteration.getName()));
            ScheduledIteration.checkIterationsDatesIntegrity(scheduledIterations);

            progression.computeSchedule();

            // actual executions
            progression.computeCumulativeTestPerDate(executionHistory);

        } catch (IllegalArgumentException ex) {
            LOGGER.info(
                    "CampaignStatistics : could not generate campaign progression statistics for campaign {}:"
                            + " some iterations scheduled dates are wrong",
                    campaignId);
            progression.addi18nErrorMessage(ex.getMessage());
        }

        return progression;
    }

    /* ************************ statistics specific to all campaigns of one milestone******************************** */

    /**
     * Given a milestone id (and so campaign ids), gathers and returns how many tests and at which
     * status are planned in this campaign. Only tests part of an iteration count. Those statistics
     * are grouped and sorted by Iteration.
     *
     * <p>Note : this method differs slightly from #gatherCampaignTestInventoryStatistics() because
     * the name of each entry is different. </>
     *
     * @param milestoneId
     * @return
     */
    public List<TestInventoryStatistics> gatherMilestoneTestInventoryStatistics() {
        Query query = em.createNamedQuery("CampaignStatistics.testinventorybymilestone");
        Optional<Milestone> activeMilestone = activeMilestoneHolder.getActiveMilestone();
        List<Object[]> tuples = Collections.emptyList();
        if (activeMilestone.isPresent()) {
            Long milestoneId = activeMilestone.get().getId();
            query.setParameter("id", milestoneId);
            tuples = query.getResultList();
        }

        return processTestInventory(Collections.emptyMap(), tuples);
    }

    /* ************************************ statistics specific to campaign folders ****************************** */

    /**
     * Given a list of campaigns, gathers and returns how many tests and at which status are planned
     * in this campaign. Only tests part of an iteration count. Those statistics are grouped and
     * sorted by Campaign.
     *
     * @param campaignNameMap a key value map of campaignId and computed path as campaign name
     * @param campaignIds list of campaign ids
     * @return
     */
    public List<TestInventoryStatistics> gatherMultiCampaignTestInventoryStatistics(
            Map<Long, String> campaignNameMap, List<Long> campaignIds) {

        List<Object[]> tuples = Collections.emptyList();
        if (!campaignIds.isEmpty()) {
            Query query = em.createNamedQuery("MultiCampaignStatistics.testinventory");
            query.setParameter(CAMPAIGN_IDS, campaignIds);
            tuples = query.getResultList();
        }
        return processTestInventory(campaignNameMap, tuples);
    }

    /* *********************** processing code ***************************** */

    /**
     * Execute a named query that accepts as argument a parameter of type List&lt;Long&gt; named
     * "campaignIds", and that returns a list of tuples (namely Object[])
     *
     * @return
     */
    private List<Object[]> fetchCommonTuples(
            String queryName, String idParameterName, List<Long> ids) {

        List<Object[]> res = Collections.emptyList();

        if (!ids.isEmpty()) {
            Query query = em.createNamedQuery(queryName);
            query.setParameter(idParameterName, ids);
            res = query.getResultList();
        }

        return res;
    }

    private List<TestInventoryStatistics> processTestInventory(
            Map<Long, String> campaignNameMap, List<Object[]> res) {
        /*
         * Process. Beware that the logic is a bit awkward here. Indeed we first insert new
         * TestInventoryStatistics in the result list, then we populate them.
         */
        TestInventoryStatistics newStatistics = new TestInventoryStatistics();
        Long currentId = null;

        List<TestInventoryStatistics> result = new LinkedList<>();

        for (Object[] tuple : res) {
            Long id = (Long) tuple[0];

            if (!id.equals(currentId)) {
                newStatistics = new TestInventoryStatistics();
                String name = campaignNameMap.get(id) != null ? campaignNameMap.get(id) : (String) tuple[1];
                newStatistics.setName(name);
                result.add(newStatistics);
                currentId = id;
            }

            ExecutionStatus status = (ExecutionStatus) tuple[2];
            Long howmany = (Long) tuple[3];

            if (status == null) {
                continue; // status == null iif the test plan is empty
            }
            newStatistics.setNumber(howmany.intValue(), status);
        }

        result.sort((a, b) -> a.getName().compareToIgnoreCase(b.getName()));

        return result;
    }
}
