/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.system;

import java.util.List;
import org.squashtest.tm.api.plugin.UsedInPlugin;
import org.squashtest.tm.domain.AdministrationStatistics;
import org.squashtest.tm.domain.synchronisation.RemoteSynchronisation;

/**
 * Provides a high-level control over global features and settings. Whereas ConfigurationService
 * have quite low-level APIs, this service provides stronger typecheck and also checks business
 * rules such as user permissions.
 */
public interface SystemAdministrationService {

    /**
     * will ask database how much there is of some entities and return it in a {@link
     * AdministrationStatistics} bean.
     *
     * @return {@link AdministrationStatistics} as result of counts in database.
     */
    AdministrationStatistics findAdministrationStatistics();

    String findWelcomeMessage();

    /**
     * Change the welcome message displayed on the login page.
     *
     * @param welcomeMessage - the new welcome message
     * @return the sanitized welcome message (return is used in admin REST API plugin)
     */
    String changeWelcomeMessage(String welcomeMessage);

    String findLoginMessage();

    /**
     * Change the login message displayed on the login page.
     *
     * @param loginMessage - the new login message
     * @return the sanitized login message (return is used in admin REST API plugin)
     */
    String changeLoginMessage(String loginMessage);

    String findBannerMessage();

    /**
     * Change the banner message displayed on all pages
     *
     * @param bannerMessage - the new banner message
     * @return the sanitized banner message (return is used in admin REST API plugin)
     */
    String changeBannerMessage(String bannerMessage);

    @UsedInPlugin("rest-api-admin")
    String findCallbackUrl();

    void changeCallbackUrl(String callbackUrl);

    @UsedInPlugin("rest-api-admin")
    String findWhiteList();

    void changeWhiteList(String whiteList);

    // TODO: use numeric type instead ?
    @UsedInPlugin("rest-api-admin")
    String findUploadSizeLimit();

    void changeUploadSizeLimit(String sizeLimit);

    // TODO: use numeric type instead ?
    @UsedInPlugin("rest-api-admin")
    String findImportSizeLimit();

    void changeImportSizeLimit(String sizeLimit);

    List<String> findAllPluginsFilesOnInstance();

    BasicLicenseInfo getBasicLicenseInfo();

    List<String> findAllPluginsAtStart();

    List<RemoteSynchronisation> findRemoteSynchronisationForPlugin(
            List<Long> manageableProjectIds, String pluginId);
}
