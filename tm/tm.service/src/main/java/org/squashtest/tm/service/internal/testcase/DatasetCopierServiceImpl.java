/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.DatasetParamValue;
import org.squashtest.tm.domain.testcase.IsScriptedTestCaseVisitor;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.exception.requirement.MilestoneForbidModificationException;
import org.squashtest.tm.service.display.testcase.TestCaseDisplayService;
import org.squashtest.tm.service.internal.display.testcase.parameter.TestCaseParameterOperationReport;
import org.squashtest.tm.service.internal.repository.DatasetDao;
import org.squashtest.tm.service.internal.repository.MilestoneDao;
import org.squashtest.tm.service.internal.repository.ParameterDao;
import org.squashtest.tm.service.internal.repository.loaders.testcase.TestCaseLoader;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.testcase.DatasetCopierService;

@Transactional
@Service
public class DatasetCopierServiceImpl implements DatasetCopierService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatasetCopierServiceImpl.class);

    private final PermissionEvaluationService permissionService;
    private final DatasetDao datasetDao;
    private final TestCaseLoader testCaseLoader;
    private final MessageSource messageSource;
    private final MilestoneDao milestoneDao;
    private final TestCaseDisplayService testCaseDisplayService;
    private final ParameterDao parameterDao;

    public DatasetCopierServiceImpl(
            PermissionEvaluationService permissionService,
            DatasetDao datasetDao,
            TestCaseLoader testCaseLoader,
            MessageSource messageSource,
            MilestoneDao milestoneDao,
            TestCaseDisplayService testCaseDisplayService,
            ParameterDao parameterDao) {
        this.permissionService = permissionService;
        this.datasetDao = datasetDao;
        this.testCaseLoader = testCaseLoader;
        this.messageSource = messageSource;
        this.milestoneDao = milestoneDao;
        this.testCaseDisplayService = testCaseDisplayService;
        this.parameterDao = parameterDao;
    }

    @Override
    public Map<Long, TestCaseParameterOperationReport> copyDataset(
            long sourceDatasetId, long sourceTestCaseId, List<Long> targetTestCasesIds) {
        List<Long> duplicationSuccessTCIds = new ArrayList<>();
        List<Long> filteredTargetTestCasesIds = checkAndFilterIdsByPermission(targetTestCasesIds);

        Map<Long, TestCase> testCases = loadTestCases(sourceTestCaseId, targetTestCasesIds);
        Source source = loadSource(testCases.get(sourceTestCaseId), sourceDatasetId);
        List<Long> blocked = milestoneDao.findTestCaseIdsBoundToBlockingMilestone(testCases.keySet());

        Map<Long, List<Parameter>> targetParameters =
                parameterDao.findAllParametersByTestCaseIds(filteredTargetTestCasesIds);

        for (Long targetTestCaseId : filteredTargetTestCasesIds) {
            try {
                TestCase targetTestCase = getTargetTestCase(targetTestCaseId, testCases, blocked);

                List<Parameter> parameters =
                        new ArrayList<>(
                                targetParameters.getOrDefault(targetTestCaseId, Collections.emptyList()));
                pasteParameters(source, targetTestCase, parameters);
                pasteDataset(source, targetTestCase, parameters);
                duplicationSuccessTCIds.add(targetTestCaseId);
            } catch (Exception e) {
                LOGGER.warn(
                        "The dataset {} could not be copied into the test case {}",
                        sourceDatasetId,
                        targetTestCaseId,
                        e);
            }
        }

        return compileDatasetCopyReport(duplicationSuccessTCIds, sourceTestCaseId);
    }

    private Source loadSource(TestCase source, long sourceDatasetId) {
        Dataset sourceDataset = datasetDao.loadWithParams(sourceDatasetId);

        return new Source(source, sourceDataset);
    }

    private Map<Long, TestCase> loadTestCases(Long sourceTestCaseId, List<Long> targetTestCasesIds) {
        final List<Long> all = new ArrayList<>(targetTestCasesIds);
        all.add(sourceTestCaseId);

        return testCaseLoader
                .load(
                        all,
                        EnumSet.of(
                                TestCaseLoader.Options.FETCH_DATASETS, TestCaseLoader.Options.FETCH_PARAMETERS))
                .stream()
                .collect(Collectors.toMap(TestCase::getId, Function.identity()));
    }

    private static TestCase getTargetTestCase(
            Long targetTestCaseId, Map<Long, TestCase> testCases, List<Long> blocked) {
        TestCase testCase = testCases.get(targetTestCaseId);

        if (testCase == null) {
            throw new IllegalArgumentException("Cannot find test case with id " + targetTestCaseId);
        }

        IsScriptedTestCaseVisitor testCaseVisitor = new IsScriptedTestCaseVisitor();
        if (testCaseVisitor.isScripted()) {
            throw new IllegalArgumentException("Cannot add dataset in a scripted test case.");
        }

        if (blocked.contains(targetTestCaseId)) {
            throw new MilestoneForbidModificationException(
                    "Cannot add dataset in a test case bound to a blocking milestone.");
        }

        return testCase;
    }

    private void pasteParameters(
            Source source, TestCase targetTestCase, List<Parameter> targetParameters) {
        // I keep the current behavior, which does not take into account the called parameters. Bug ?
        List<String> targetParameterNames =
                targetTestCase.getParameters().stream().map(Parameter::getName).toList();

        checkTargetTestCaseHasValidParameters(source.getParametersNames(), targetParameterNames);

        if (!targetParameterNames.isEmpty()) {
            return;
        }

        for (Parameter parameter : source.getParameters()) {
            Parameter duplicatedParameter = new Parameter();
            duplicatedParameter.setName(parameter.getName());
            duplicatedParameter.setDescription(parameter.getDescription());
            duplicatedParameter.setTestCase(targetTestCase);
            targetParameters.add(duplicatedParameter);
        }
    }

    private void pasteDataset(
            Source source, TestCase targetTestCase, List<Parameter> targetParameters) {

        Dataset dataset = new Dataset();

        String name = resolveDatasetName(targetTestCase, source.getDatasetName());
        dataset.setName(name);

        dataset.setTestCase(targetTestCase);
        targetTestCase.addDataset(dataset);

        if (source.hasNoParameters()) {
            return;
        }

        for (Parameter parameter : targetParameters) {

            DatasetParamValue targetDatasetParamValue = new DatasetParamValue(parameter, dataset);
            dataset.addParameterValue(targetDatasetParamValue);

            // Because called parameters are not taken into account, we can directly paste the values.
            if (!parameter.getTestCase().getId().equals(targetTestCase.getId())) {
                continue;
            }

            String parameterValue = source.getDatasetParamValue(parameter);

            if (parameterValue != null) {
                targetDatasetParamValue.setParamValue(parameterValue);
            }
        }
    }

    private List<Long> checkAndFilterIdsByPermission(List<Long> targetTestCasesIds) {
        return targetTestCasesIds.stream()
                .filter(
                        id ->
                                permissionService.hasRoleOrPermissionOnObject(
                                        Roles.ROLE_ADMIN, Permissions.WRITE.name(), id, TestCase.class.getName()))
                .toList();
    }

    private void checkTargetTestCaseHasValidParameters(
            List<String> sourceParameters, List<String> targetParameters) {
        boolean areMatching =
                ParameterComparator.checkHasMatchingParameters(sourceParameters, targetParameters);
        if (!areMatching) {
            throw new IllegalArgumentException(
                    "Illegal dataset duplication: target parameters are different from source parameters.");
        }
    }

    // The report is only of interest to us if the dataset is copied within the test case it
    // originates from.
    // In reality, only the number of copied test cases matters to us,
    // along with a DTO of the dataset added if we are dealing with the same test case.
    // I’m keeping the map because the frontend would also need to be modified.
    private Map<Long, TestCaseParameterOperationReport> compileDatasetCopyReport(
            List<Long> duplicationSuccessTCIds, Long sourceTestCaseId) {
        Map<Long, TestCaseParameterOperationReport> datasetCopyReport = new HashMap<>();

        duplicationSuccessTCIds.forEach(
                tcId -> {
                    if (tcId.equals(sourceTestCaseId)) {
                        TestCaseParameterOperationReport testCaseParameterOperationReport =
                                testCaseDisplayService.findParametersData(tcId);
                        datasetCopyReport.put(tcId, testCaseParameterOperationReport);
                    } else {
                        datasetCopyReport.put(tcId, new TestCaseParameterOperationReport());
                    }
                });

        return datasetCopyReport;
    }

    private String resolveDatasetName(TestCase targetTestCase, String sourceName) {
        List<String> datasetNames =
                targetTestCase.getDatasets().stream().map(Dataset::getName).toList();

        boolean isNameAlreadyExists = datasetNames.contains(sourceName);

        if (!isNameAlreadyExists) {
            return sourceName;
        }

        String sourceDatasetName = sourceName;

        int i = 0;
        String copySuffix =
                messageSource.getMessage("label.CopySuffix", null, LocaleContextHolder.getLocale());

        while (isNameAlreadyExists) {
            i++;
            String testedName = sourceDatasetName + copySuffix + i;
            isNameAlreadyExists = datasetNames.contains(testedName);
        }

        return i == 0 ? sourceDatasetName : sourceDatasetName + copySuffix + i;
    }

    private static final class Source {
        private final Set<Parameter> parameters;
        private final List<String> parametersNames;
        private final Dataset sourceDataset;
        private final Map<String, String> parameterValueByParameterName;

        public Source(TestCase testCase, Dataset sourceDataset) {
            this.parameters = testCase.getParameters();
            this.parametersNames = parameters.stream().map(Parameter::getName).toList();
            this.sourceDataset = sourceDataset;
            this.parameterValueByParameterName = getParamValuesMap(sourceDataset);
        }

        private Map<String, String> getParamValuesMap(Dataset dataset) {
            return dataset.getParameterValues().stream()
                    .collect(
                            Collectors.toMap(
                                    value -> value.getParameter().getName(), DatasetParamValue::getParamValue));
        }

        List<String> getParametersNames() {
            return parametersNames;
        }

        String getDatasetParamValue(Parameter parameter) {
            return parameterValueByParameterName.get(parameter.getName());
        }

        String getDatasetName() {
            return sourceDataset.getName();
        }

        boolean hasNoParameters() {
            return parameters.isEmpty();
        }

        Set<Parameter> getParameters() {
            return parameters;
        }
    }
}
