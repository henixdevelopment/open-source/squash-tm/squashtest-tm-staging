/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation.model.messages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.service.internal.testautomation.model.messages.exceptions.ArrayExpectedInAdditionalConfigurationException;
import org.squashtest.tm.service.internal.testautomation.model.messages.exceptions.EmptySectionInAdditionalConfigurationException;
import org.squashtest.tm.service.internal.testautomation.model.messages.exceptions.InvalidYamlSyntaxInAdditionalConfigurationException;
import org.squashtest.tm.service.internal.testautomation.model.messages.exceptions.ObjectExpectedInAdditionalConfigurationException;
import org.squashtest.tm.service.internal.testautomation.model.messages.exceptions.UnknownKeyInAdditionalConfigurationException;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.error.MarkedYAMLException;

public class AdditionalConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdditionalConfiguration.class);
    private Map<String, Object> defaults = new HashMap<>();
    private List<Object> hooks = new ArrayList<>();
    private Map<String, Object> metadata = new HashMap<>();
    private Map<String, Object> resources = new HashMap<>();
    private Map<String, Object> variables = new HashMap<>();
    private Map<String, Object> strategy = new HashMap<>();

    public AdditionalConfiguration(String additionalConfiguration) {
        Yaml yaml = new Yaml();
        try {
            Map<String, Object> additionalConfigurationMap = yaml.load(additionalConfiguration);
            ifContainsAnUnknownKeyThrowException(additionalConfigurationMap);
            setAttributes(additionalConfigurationMap);
        } catch (ClassCastException | MarkedYAMLException ex) {
            LOGGER.error(ex.getMessage(), ex);
            InvalidYamlSyntaxInAdditionalConfigurationException invalidYamlException =
                    new InvalidYamlSyntaxInAdditionalConfigurationException();
            invalidYamlException.addArg(additionalConfiguration);
            throw invalidYamlException;
        }
    }

    private void ifContainsAnUnknownKeyThrowException(
            Map<String, Object> additionalConfigurationMap) {
        List<String> acceptedKeys = new ArrayList<>();
        for (AdditionalConfigurationAcceptedKeys key : AdditionalConfigurationAcceptedKeys.values()) {
            acceptedKeys.add(key.getKeyName());
        }

        String unknownKey =
                additionalConfigurationMap.keySet().stream()
                        .filter(key -> !acceptedKeys.contains(key))
                        .findFirst()
                        .orElse(null);
        if (unknownKey != null) {
            UnknownKeyInAdditionalConfigurationException unknownKeyException =
                    new UnknownKeyInAdditionalConfigurationException();
            unknownKeyException.addArg(unknownKey);
            throw unknownKeyException;
        }
    }

    private void setAttributes(Map<String, Object> additionalConfigurationMap) {

        String defaultsKey = AdditionalConfigurationAcceptedKeys.DEFAULTS.getKeyName();
        String hooksKey = AdditionalConfigurationAcceptedKeys.HOOKS.getKeyName();
        String metadataKey = AdditionalConfigurationAcceptedKeys.METADATA.getKeyName();
        String resourcesKey = AdditionalConfigurationAcceptedKeys.RESOURCES.getKeyName();
        String variablesKey = AdditionalConfigurationAcceptedKeys.VARIABLES.getKeyName();
        String strategyKey = AdditionalConfigurationAcceptedKeys.STRATEGY.getKeyName();

        if (additionalConfigurationMap.containsKey(defaultsKey)) {
            this.setDefaults(buildMap(defaultsKey, additionalConfigurationMap.get(defaultsKey)));
        }
        if (additionalConfigurationMap.containsKey(hooksKey)) {
            this.setHooks(buildList(hooksKey, additionalConfigurationMap.get(hooksKey)));
        }
        if (additionalConfigurationMap.containsKey(metadataKey)) {
            this.setMetadata(buildMap(metadataKey, additionalConfigurationMap.get(metadataKey)));
        }
        if (additionalConfigurationMap.containsKey(resourcesKey)) {
            this.setResources(buildMap(resourcesKey, additionalConfigurationMap.get(resourcesKey)));
        }
        if (additionalConfigurationMap.containsKey(variablesKey)) {
            this.setVariables(buildMap(variablesKey, additionalConfigurationMap.get(variablesKey)));
        }
        if (additionalConfigurationMap.containsKey(strategyKey)) {
            this.setStrategy(buildMap(strategyKey, additionalConfigurationMap.get(strategyKey)));
        }
    }

    private Map<String, Object> buildMap(String key, Object conf) {
        if (conf == null) {
            EmptySectionInAdditionalConfigurationException emptySectionException =
                    new EmptySectionInAdditionalConfigurationException();
            emptySectionException.addArg(key);
            throw emptySectionException;
        }
        if (conf instanceof Map<?, ?>) {
            return (Map<String, Object>) conf;
        } else {
            ObjectExpectedInAdditionalConfigurationException invalidObjectException =
                    new ObjectExpectedInAdditionalConfigurationException();
            invalidObjectException.addArg(key);
            invalidObjectException.addArg(conf.toString());
            throw invalidObjectException;
        }
    }

    private List<Object> buildList(String key, Object conf) {
        if (conf == null) {
            EmptySectionInAdditionalConfigurationException emptySectionException =
                    new EmptySectionInAdditionalConfigurationException();
            emptySectionException.addArg(key);
            throw emptySectionException;
        }
        if (conf instanceof List<?>) {
            return (List<Object>) conf;
        } else {
            ArrayExpectedInAdditionalConfigurationException invalidArrayException =
                    new ArrayExpectedInAdditionalConfigurationException();
            invalidArrayException.addArg(key);
            invalidArrayException.addArg(conf.toString());
            throw invalidArrayException;
        }
    }

    public Map<String, Object> getDefaults() {
        return defaults;
    }

    public void setDefaults(Map<String, Object> defaults) {
        this.defaults = defaults;
    }

    public List<Object> getHooks() {
        return hooks;
    }

    public void setHooks(List<Object> hooks) {
        this.hooks = hooks;
    }

    public Map<String, Object> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, Object> metadata) {
        this.metadata = metadata;
    }

    public Map<String, Object> getResources() {
        return resources;
    }

    public void setResources(Map<String, Object> resources) {
        this.resources = resources;
    }

    public Map<String, Object> getVariables() {
        return variables;
    }

    public void setVariables(Map<String, Object> variables) {
        this.variables = variables;
    }

    public Map<String, Object> getStrategy() {
        return strategy;
    }

    public void setStrategy(Map<String, Object> strategy) {
        this.strategy = strategy;
    }
}
