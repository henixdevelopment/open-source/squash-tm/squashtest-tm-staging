/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;
import org.jooq.CommonTableExpression;
import org.jooq.Record1;
import org.jooq.TableField;
import org.squashtest.tm.api.plugin.PluginType;
import org.squashtest.tm.domain.project.LibraryPluginBinding;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.jooq.domain.tables.records.ProjectRecord;
import org.squashtest.tm.service.internal.batchimport.ProjectLibrariesIds;
import org.squashtest.tm.service.internal.display.dto.automatedexecutionenvironments.ExecutionEnvironmentCountProjectInfoDto;

/**
 * @author Gregory Fouquet
 */
public interface CustomProjectDao {
    long countNonFoldersInProject(long projectId);

    long countNonFolderInActionWord(long projectId);

    List<String> findUsersWhoCreatedTestCases(List<Long> projectIds);

    List<String> findUsersWhoModifiedTestCases(List<Long> projectIds);

    List<String> findUsersWhoCreatedRequirementVersions(List<Long> projectIds);

    List<String> findUsersWhoModifiedRequirementVersions(List<Long> projectIds);

    List<String> findUsersWhoExecutedItpi(List<Long> projectIds);

    List<String> findUsersAssignedToItpi(List<Long> projectIds);

    List<String> findProjectNamesByIds(List<Long> projectIds);

    Long findIdByName(String name);

    /**
     * This method returns all projects IDs, excluding project templates
     *
     * @return list of project ids
     */
    List<Long> findAllProjectIds();

    /**
     * This method returns CTE with all projects IDs, excluding project templates to avoid
     * PROJECT.PROJECT_ID in (List of PROJECT_ID)
     *
     * @return CTE with field equals PROJECT.PROJECT_ID.getQualifiedName()
     */
    CommonTableExpression<Record1<Long>> findAllProjectIdsCte();

    /**
     * This method check if the CTE has at least one readable project id
     *
     * @param projectIdsCte the CTE with project ids
     * @return true if the CTE has at least one readable project id
     */
    boolean checkHasAtLeastOneReadableId(CommonTableExpression<Record1<Long>> projectIdsCte);

    /**
     * This method returns all projects IDs, excluding project templates
     *
     * @return list of project ids ordered by name
     */
    List<Long> findAllProjectIdsInListOrderedByName(List<Long> projectIds);

    /**
     * This method returns all projects IDs, including project templates
     *
     * @return list of project and project template ids
     */
    List<Long> findAllProjectAndTemplateIds();

    List<Long> findAllProjectIdsByPermission(List<Long> partyIds, int permission);

    CommonTableExpression<Record1<Long>> findAllProjectIdsByPermissionCte(
            List<Long> partyIds, int permission);

    List<Long> findAllProjectIdsByEligibleTCPermission(List<Long> partyIds, int permission);

    List<Long> findAllProjectIdsOrderedByName(List<Long> partyIds);

    /**
     * Find all project ids for given party ids and permission
     *
     * @param partyIds The ids of all concerned parties
     * @param permissionMask The int corresponding to the permission mask
     * @param className The full name, with package, of the class which the permission is requested
     * @return ths ids of {@link org.squashtest.tm.domain.project.Project} with given permission.
     *     {@link org.squashtest.tm.domain.project.ProjectTemplate} are excluded.
     */
    List<Long> findAllProjectIdsByPermissionMaskAndClassName(
            List<Long> partyIds,
            int permissionMask,
            String className,
            TableField<ProjectRecord, Long> libraryColumnField);

    List<Long> findAllProjectIdsForAutomationWriter(List<Long> partyIds);

    Integer countProjectsAllowAutomationWorkflow();

    LibraryPluginBinding findPluginForProject(Long projectId, PluginType pluginType);

    void removeLibraryPluginBindingProperty(Long libraryPluginBindingId);

    BigInteger countActivePluginInProject(long projectId);

    /**
     * Returns both projects and project templates given a list of potential project managers.
     *
     * @param partyIds users
     * @return a list of project and template IDs where at least one of the provided party has a role
     *     of project manager
     */
    List<Long> findAllManagedProjectIds(List<Long> partyIds);

    Project fetchForAutomatedExecutionCreation(long projectId);

    String findAutomationWorkflowTypeByProjectId(Long projectId);

    List<Long> findTestCaseLibraryNodesIds(Long projectId);

    List<Long> findRequirementLibraryNodesIds(Long projectId);

    List<Long> findCampaignLibraryNodesIds(Long projectId);

    List<ExecutionEnvironmentCountProjectInfoDto> findProjectsAndServersByProjectIds(
            Set<Long> projectIds);

    Project fetchByIterationIdForAutomatedExecutionCreation(long iterationId);

    Project fetchByTestSuiteForAutomatedExecutionCreation(long testSuiteId);

    List<ProjectLibrariesIds> getLibrariesIdsByNames(List<String> projectNames);

    Long findByRequirementLibraryId(long requirementLibraryId);
}
