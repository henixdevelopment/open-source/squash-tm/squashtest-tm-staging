/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate.loaders.common;

import javax.persistence.TypedQuery;
import org.hibernate.jpa.QueryHints;

public class HintOption {

    public static final String FETCH_GRAPH = QueryHints.HINT_FETCHGRAPH;

    public static final HintOption CACHEABLE = new HintOption(QueryHints.HINT_CACHEABLE, true);
    public static final HintOption READ_ONLY = new HintOption(QueryHints.HINT_READONLY, true);
    public static final HintOption NO_DISTINCT_IN_DB =
            new HintOption(QueryHints.HINT_PASS_DISTINCT_THROUGH, false);

    private final String hint;
    private final Object value;

    public HintOption(String hint, Object value) {
        this.hint = hint;
        this.value = value;
    }

    public <T> TypedQuery<T> apply(TypedQuery<T> query) {
        return query.setHint(hint, value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HintOption that = (HintOption) o;

        return hint.equals(that.hint);
    }

    @Override
    public int hashCode() {
        return hint.hashCode();
    }
}
