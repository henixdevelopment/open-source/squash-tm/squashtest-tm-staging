/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl.collectors;

import static org.jooq.impl.DSL.count;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.CLN_RELATIONSHIP;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT_GROUP;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.jooq.DSLContext;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TreeNodeCollector;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;

@Component
public class SprintGroupCollector extends AbstractTreeNodeCollector implements TreeNodeCollector {

    public SprintGroupCollector(
            DSLContext dsl,
            CustomFieldValueDisplayDao customFieldValueDisplayDao,
            ActiveMilestoneHolder activeMilestoneHolder,
            MilestoneDisplayDao milestoneDisplayDao) {
        super(dsl, customFieldValueDisplayDao, activeMilestoneHolder, milestoneDisplayDao);
    }

    @Override
    public NodeType getHandledEntityType() {
        return NodeType.SPRINT_GROUP;
    }

    @Override
    protected Map<Long, DataRow> doCollect(List<Long> ids) {
        return collectSprintGroups(ids);
    }

    private Map<Long, DataRow> collectSprintGroups(List<Long> ids) {
        return dsl
                .select(
                        // @formatter:off
                        CAMPAIGN_LIBRARY_NODE.CLN_ID,
                        CAMPAIGN_LIBRARY_NODE.NAME.as(NAME_ALIAS),
                        CAMPAIGN_LIBRARY_NODE.PROJECT_ID.as(PROJECT_ID_ALIAS),
                        count(CLN_RELATIONSHIP.ANCESTOR_ID).as(CHILD_COUNT_ALIAS))
                .from(CAMPAIGN_LIBRARY_NODE)
                .join(SPRINT_GROUP)
                .on(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(SPRINT_GROUP.CLN_ID))
                .leftJoin(CLN_RELATIONSHIP)
                .on(SPRINT_GROUP.CLN_ID.eq(CLN_RELATIONSHIP.ANCESTOR_ID))
                .where(CAMPAIGN_LIBRARY_NODE.CLN_ID.in(ids))
                .groupBy(CAMPAIGN_LIBRARY_NODE.CLN_ID)
                .fetch()
                .stream()
                // @formatter:on
                .collect(
                        Collectors.toMap(
                                tuple -> tuple.get(CAMPAIGN_LIBRARY_NODE.CLN_ID),
                                tuple -> {
                                    DataRow dataRow = new DataRow();
                                    dataRow.setId(
                                            new NodeReference(
                                                            NodeType.SPRINT_GROUP, tuple.get(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                                                    .toNodeId());
                                    dataRow.setProjectId(tuple.get(PROJECT_ID_ALIAS, Long.class));
                                    dataRow.setState(
                                            tuple.get(CHILD_COUNT_ALIAS, Integer.class) > 0
                                                    ? DataRow.State.closed
                                                    : DataRow.State.leaf);
                                    dataRow.setData(tuple.intoMap());

                                    return dataRow;
                                }));
    }
}
