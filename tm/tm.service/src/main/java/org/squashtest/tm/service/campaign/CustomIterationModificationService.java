/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.campaign;

import java.util.List;
import java.util.Map;
import org.squashtest.tm.api.plugin.UsedInPlugin;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.annotation.Ids;
import org.squashtest.tm.service.clipboard.model.ClipboardPayload;
import org.squashtest.tm.service.statistics.campaign.StatisticsBundle;

/**
 * Iteration modification services which cannot be dynamically generated.
 *
 * @author Gregory Fouquet
 */
public interface CustomIterationModificationService extends IterationFinder {

    String ITERATION_ID = "iterationId";

    /**
     * Adds a new iteration to a campaign.
     *
     * @param iteration The iteration to add
     * @param campaignId The id of the campaign to add the iteration to
     * @param customFieldValues The initial custom field values as a map of custom field id to raw
     *     value
     */
    Iteration addIterationToCampaign(
            Iteration iteration,
            @Id long campaignId,
            boolean copyTestPlan,
            Map<Long, RawValue> customFieldValues);

    Iteration addIterationToCampaignUnsecured(
            Iteration iteration,
            @Id long campaignId,
            boolean copyTestPlan,
            Map<Long, RawValue> customFieldValues);

    /**
     * Adds a new iteration to a campaign, initializing test plans items with a copy of the provided
     * ones.
     *
     * @param campaignId The id of the campaign to add the iteration to
     * @param name The name of the iteration
     * @param description The description of the iteration
     * @param itemTestPlanIds The ids of the test plan items to copy
     */
    void createIterationWithItemCopies(
            @Id long campaignId, String name, String description, List<Long> itemTestPlanIds);

    void rename(long iterationId, String newName);

    /**
     * [SQUASH-3682] dedicated method for manual execution. Due to the degenerated model, automated
     * test cases and manual ones are the same beast. Try to fix that expose us too massive
     * regressions so i fix the model with a dedicated service method... *sighs*
     *
     * @param testPlanItemId
     * @return
     */
    Execution addManualExecution(@Id long testPlanItemId);

    Execution addManualExecutionUnsecured(@Id long testPlanItemId);

    Execution addExecution(@Id long testPlanItemId);

    void addTestSuite(@Id long iterationId, TestSuite suite);

    @UsedInPlugin("campaignassistant")
    List<NamedReference> findAllTestSuitesAsNamedReferences(long iterationId);

    void changeTestSuitePosition(@Id long iterationId, int newIndex, List<Long> itemIds);

    /**
     * will create a copy of the test suites and their test plan , then associate them to the given
     * iteration<br>
     * will rename test suites if there is name conflict at destination
     *
     * @param testSuiteIds = list of test suites to copy
     * @param iterationId = iteration where to add the copy of the test suite
     */
    void copyPasteTestSuitesToIteration(
            @Ids("testSuiteIds") Long[] testSuiteIds,
            @Id(ITERATION_ID) long iterationId,
            ClipboardPayload clipboardPayload);

    StatisticsBundle gatherIterationStatisticsBundle(long iterationId, boolean isLastExecutionScope);

    Execution updateExecutionFromTc(long executionId);
}
