/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.supervision.model;

import java.util.List;

/**
 * A front representation of an automated execution overview used in automated tests supervision
 * popup. !Careful: in TF use case, progression is a percentage, in SquashAutom, it is the number of
 * terminated executions. This comes from the different execution creation mechanism, in TF use
 * case, we create all the executions immediately, whereas in SquashAutom they are created later
 * when the ExpectedSuiteDefinition is received by the tm-publisher service.
 *
 * @param progression percentage of progress when using TF, number of terminated executions using
 *     SquashAutom
 * @param executionViews list of AutomatedExecutionView
 * @param <V> type of AutomatedExecutionView
 */
public record ExecutionsOverview<V extends AutomatedExecutionView>(
        int progression, List<V> executionViews) {}
