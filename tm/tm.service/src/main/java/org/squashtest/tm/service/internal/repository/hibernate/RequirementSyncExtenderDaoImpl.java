/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.jooq.domain.Tables.BUGTRACKER;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_SYNC_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_COVERAGE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.THIRD_PARTY_SERVER;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_REQUIREMENT_SYNC_EXTENDERS_BY_KEYS_AND_PROJECT_ID;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_REQUIREMENT_SYNC_EXTENDERS_BY_KEYS_AND_SYNC_ID;
import static org.squashtest.tm.service.internal.repository.ParameterNames.PROJECT_ID;
import static org.squashtest.tm.service.internal.repository.ParameterNames.REMOTE_IDS;
import static org.squashtest.tm.service.internal.repository.ParameterNames.REMOTE_SYNCHRONISATION_ID;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.jooq.DSLContext;
import org.squashtest.tm.domain.requirement.RequirementSyncExtender;
import org.squashtest.tm.service.internal.repository.CustomRequirementSyncExtenderDao;

/**
 * Implementation of {@link CustomRequirementSyncExtenderDao}
 *
 * @author aguilhem
 */
public class RequirementSyncExtenderDaoImpl implements CustomRequirementSyncExtenderDao {
    @Inject DSLContext dsl;

    @PersistenceContext private EntityManager entityManager;

    @Override
    public List<String> findAllRemoteReqIdVerifiedByATestCaseByServerUrl(
            String serverUrl, Long testCaseId) {
        return dsl.select(REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_ID)
                .from(REQUIREMENT_SYNC_EXTENDER)
                .leftJoin(REQUIREMENT)
                .on(REQUIREMENT.RLN_ID.eq(REQUIREMENT_SYNC_EXTENDER.REQUIREMENT_ID))
                .leftJoin(REQUIREMENT_VERSION)
                .on(REQUIREMENT_VERSION.REQUIREMENT_ID.eq(REQUIREMENT.RLN_ID))
                .leftJoin(REQUIREMENT_VERSION_COVERAGE)
                .on(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.eq(REQUIREMENT_VERSION.RES_ID))
                .leftJoin(TEST_CASE)
                .on(TEST_CASE.TCLN_ID.eq(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID))
                .leftJoin(BUGTRACKER)
                .on(BUGTRACKER.BUGTRACKER_ID.eq(REQUIREMENT_SYNC_EXTENDER.SERVER_ID))
                .leftJoin(THIRD_PARTY_SERVER)
                .on(BUGTRACKER.BUGTRACKER_ID.eq(THIRD_PARTY_SERVER.SERVER_ID))
                .where(THIRD_PARTY_SERVER.URL.eq(serverUrl))
                .and(TEST_CASE.TCLN_ID.eq(testCaseId))
                .fetch(REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_ID);
    }

    @Override
    public Map<String, RequirementSyncExtender> findByRemoteKeysAndSyncId(
            Collection<String> remoteIds, Long remoteSyncId) {
        if (remoteIds == null || remoteIds.isEmpty()) {
            return Collections.emptyMap();
        }

        return entityManager
                .createQuery(
                        FIND_REQUIREMENT_SYNC_EXTENDERS_BY_KEYS_AND_SYNC_ID, RequirementSyncExtender.class)
                .setParameter(REMOTE_IDS, remoteIds)
                .setParameter(REMOTE_SYNCHRONISATION_ID, remoteSyncId)
                .getResultList()
                .stream()
                .collect(
                        Collectors.toMap(
                                RequirementSyncExtender::getRemoteReqId, java.util.function.Function.identity()));
    }

    @Override
    public Map<String, RequirementSyncExtender> findByRemoteKeysAndProjectId(
            Collection<String> remoteIds, Long projectId) {
        if (remoteIds == null || remoteIds.isEmpty()) {
            return Collections.emptyMap();
        }

        return entityManager
                .createQuery(
                        FIND_REQUIREMENT_SYNC_EXTENDERS_BY_KEYS_AND_PROJECT_ID, RequirementSyncExtender.class)
                .setParameter(REMOTE_IDS, remoteIds)
                .setParameter(PROJECT_ID, projectId)
                .getResultList()
                .stream()
                .collect(
                        Collectors.toMap(
                                RequirementSyncExtender::getRemoteReqId, java.util.function.Function.identity()));
    }

    @Override
    public Long findRequirementIdByIssueKeyAndRemoteSyncId(String issueKey, Long remoteSyncId) {
        return dsl.select(REQUIREMENT_SYNC_EXTENDER.REQUIREMENT_ID)
                .from(REQUIREMENT_SYNC_EXTENDER)
                .where(REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_ID.eq(issueKey))
                .and(REQUIREMENT_SYNC_EXTENDER.REMOTE_SYNCHRONISATION_ID.eq(remoteSyncId))
                .fetchOne(REQUIREMENT_SYNC_EXTENDER.REQUIREMENT_ID);
    }

    @Override
    public void resetRemoteLastUpdated(Long syncId) {
        dsl.update(REQUIREMENT_SYNC_EXTENDER)
                .set(REQUIREMENT_SYNC_EXTENDER.REMOTE_LAST_UPDATED, new Timestamp(0))
                .where(REQUIREMENT_SYNC_EXTENDER.REMOTE_SYNCHRONISATION_ID.eq(syncId))
                .execute();
    }
}
