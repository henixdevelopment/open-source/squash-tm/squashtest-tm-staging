/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity.testcaseworkspace;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.domain.testcase.TestCaseKind;
import org.squashtest.tm.domain.testcase.TestCaseStatus;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportField;
import org.squashtest.tm.service.internal.dto.projectimporterxray.XrayField;
import org.squashtest.tm.service.internal.dto.projectimporterxray.jooq.dto.CustomFieldXrayDto;
import org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.entity.AbstractPivotEntity;
import org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.mapping.XrayPriority;
import org.squashtest.tm.service.internal.dto.projectimporterxray.topivotdto.mapping.XrayStatus;

public abstract class CommonTestCase extends AbstractPivotEntity {
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @JsonProperty(JsonImportField.TEST_CASE_KIND)
    private TestCaseKind testCaseKind;

    @JsonProperty(JsonImportField.NAME)
    private String name;

    @JsonProperty(JsonImportField.DESCRIPTION)
    private String description;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(JsonImportField.REFERENCE)
    private String reference;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @JsonProperty(JsonImportField.IMPORTANCE)
    private TestCaseImportance importance;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @JsonProperty(JsonImportField.STATUS)
    private TestCaseStatus status;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(JsonImportField.NATURE)
    private String nature;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(JsonImportField.TYPE)
    private String type;

    @JsonProperty(JsonImportField.VERIFIED_REQUIREMENTS)
    private List<String> verifiedRequirements = new ArrayList<>();

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @JsonProperty(JsonImportField.PARENT_TYPE)
    private EntityType parentType;

    public void setParentTypeAndId(
            List<CustomFieldXrayDto> testRepositoryPath, Map<String, String> testCaseFolderIdMap) {
        if (Objects.nonNull(testRepositoryPath) && !testRepositoryPath.isEmpty()) {
            this.parentType = EntityType.TEST_CASE_FOLDER;
            // Only get first path
            String path =
                    StringUtils.trim(
                            testRepositoryPath.get(0).getValue().replaceAll("</?\\w+>", StringUtils.EMPTY));
            this.parentId = testCaseFolderIdMap.get(path);
        } else {
            this.parentType = EntityType.TEST_CASE_LIBRARY;
        }
    }

    @Override
    public BindableEntity getBindableEntity() {
        return BindableEntity.TEST_CASE;
    }

    // GETTER SETTER
    public void setPivotId(Integer id) {
        this.pivotId = String.format("%s%s", XrayField.BASE_PIVOT_ID_TEST, id);
    }

    public TestCaseKind getTestCaseKind() {
        return testCaseKind;
    }

    public void setTestCaseKind(TestCaseKind testCaseKind) {
        this.testCaseKind = testCaseKind;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public TestCaseImportance getImportance() {
        return importance;
    }

    public void setImportance(XrayPriority priority) {
        this.importance = priority.getTestCaseImportance();
    }

    public TestCaseStatus getStatus() {
        return status;
    }

    public void setStatus(XrayStatus status) {
        this.status = status.getTestCaseStatus();
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getVerifiedRequirements() {
        return verifiedRequirements;
    }

    public void setVerifiedRequirements(List<String> verifiedRequirements) {
        this.verifiedRequirements = verifiedRequirements;
    }

    public EntityType getParentType() {
        return parentType;
    }

    public void setParentType(EntityType parentType) {
        this.parentType = parentType;
    }
}
