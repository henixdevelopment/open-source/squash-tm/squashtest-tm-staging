/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search.filter;

import static org.squashtest.tm.domain.campaign.QIterationTestPlanItem.iterationTestPlanItem;

import com.google.common.collect.Sets;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.inject.Inject;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.jpql.ExtendedHibernateQuery;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.repository.CustomItpiLastExecutionFilterDao;

@Component
public class ItpiExecutionScopeFilterHandler implements FilterHandler {
    private static final List<EntityType> scopableEntityList =
            Arrays.asList(
                    EntityType.ITERATION,
                    EntityType.CAMPAIGN,
                    EntityType.CAMPAIGN_FOLDER,
                    EntityType.CAMPAIGN_LIBRARY,
                    EntityType.PROJECT,
                    EntityType.TEST_SUITE);

    @Inject CustomItpiLastExecutionFilterDao customItpiLastExecutionFilterDao;

    private final Set<String> handledPrototypes = Sets.newHashSet("ITEM_TEST_PLAN_EXECUTION_SCOPE");

    @Override
    public boolean canHandleFilter(GridFilterValue filter) {
        return this.handledPrototypes.contains(filter.getColumnPrototype());
    }

    @Override
    public void handleFilter(
            ExtendedHibernateQuery<?> query, GridFilterValue filter, GridRequest gridRequest) {
        ItpiExecutionScopeFilterValue executionScopeFilterValue = extractFilterValue(filter);

        if (executionScopeFilterValue.requireAdditionalFilterClause()) {
            addFilterClause(query, gridRequest.getScope(), gridRequest.getExcludedScope());
        }
    }

    private ItpiExecutionScopeFilterValue extractFilterValue(GridFilterValue filter) {
        List<String> values = filter.getValues();
        if (values.size() != 1) {
            throw new IllegalArgumentException("Invalid value for filter ITEM_TEST_PLAN_EXECUTION_SCOPE");
        }
        return ItpiExecutionScopeFilterValue.valueOf(values.get(0));
    }

    // LMU: run as separate queries because Hibernate and JPQL do not support window functions.
    // Workaround is to retrieve id list using native sql and jpql queries and provide result as
    // parameter
    // in the outer hibernate query
    private void addFilterClause(
            ExtendedHibernateQuery<?> query, List<String> scope, List<String> excludedScope) {
        // get id lists by entity type for all entities in perimeter
        Map<EntityType, List<Long>> scopableEntitiesMap = new EnumMap<>(EntityType.class);
        scopableEntityList.forEach(
                entityType -> extractEntityIdsFromScope(scope, scopableEntitiesMap, entityType));

        if (!excludedScope.isEmpty()) {
            extractEntityIdsFromScope(excludedScope, scopableEntitiesMap, EntityType.TEST_SUITE);
        }

        List<Long> filteredItpiIds =
                customItpiLastExecutionFilterDao.gatherLatestItpiIdsForTCInDynamicScope(
                        scopableEntitiesMap);
        if (!filteredItpiIds.isEmpty()) {
            query.where(iterationTestPlanItem.id.in(filteredItpiIds));
        }
    }

    public static void extractEntityIdsFromScope(
            List<String> scope, Map<EntityType, List<Long>> scopableEntitiesMap, EntityType entityType) {
        List<Long> scopedEntityIds =
                scope.stream()
                        .map(EntityReference::fromNodeId)
                        .filter(scopedEntity -> scopedEntity.getType().equals(entityType))
                        .map(EntityReference::getId)
                        .toList();

        if (!scopedEntityIds.isEmpty()) {
            scopableEntitiesMap.put(entityType, scopedEntityIds);
        }
    }

    public enum ItpiExecutionScopeFilterValue {
        ALL,
        LAST_EXECUTED;

        boolean requireAdditionalFilterClause() {
            return !this.equals(ALL);
        }
    }
}
