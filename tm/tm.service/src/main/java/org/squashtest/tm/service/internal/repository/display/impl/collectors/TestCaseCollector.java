/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl.collectors;

import static org.jooq.impl.DSL.countDistinct;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.INFO_LIST_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.KEYWORD_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_COVERAGE;
import static org.squashtest.tm.jooq.domain.Tables.SCRIPTED_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_STEPS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXPLORATORY_TEST_CASE_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.KEYWORD_TEST_CASE_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.SCRIPTED_TEST_CASE_ID;

import com.google.common.base.Strings;
import com.google.common.collect.Multimap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.jooq.DSLContext;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.domain.testcase.TestCaseKind;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TreeNodeCollector;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;

@Component
public class TestCaseCollector extends AbstractTreeNodeCollector implements TreeNodeCollector {

    public TestCaseCollector(
            DSLContext dsl,
            CustomFieldValueDisplayDao customFieldValueDisplayDao,
            ActiveMilestoneHolder activeMilestoneHolder,
            MilestoneDisplayDao milestoneDisplayDao) {
        super(dsl, customFieldValueDisplayDao, activeMilestoneHolder, milestoneDisplayDao);
    }

    @Override
    protected Map<Long, DataRow> doCollect(List<Long> ids) {
        // @formatter:off
        Map<Long, DataRow> testCaseRows =
                this.dsl
                        .select(
                                TEST_CASE_LIBRARY_NODE.TCLN_ID,
                                TEST_CASE_LIBRARY_NODE.NAME.as(NAME_ALIAS),
                                TEST_CASE_LIBRARY_NODE.PROJECT_ID.as(PROJECT_ID_ALIAS),
                                TEST_CASE.IMPORTANCE,
                                TEST_CASE.REFERENCE,
                                TEST_CASE.TC_STATUS,
                                SCRIPTED_TEST_CASE.TCLN_ID.as(SCRIPTED_TEST_CASE_ID),
                                KEYWORD_TEST_CASE.TCLN_ID.as(KEYWORD_TEST_CASE_ID),
                                EXPLORATORY_TEST_CASE.TCLN_ID.as(EXPLORATORY_TEST_CASE_ID),
                                INFO_LIST_ITEM.ICON_NAME.as("TC_NATURE_ICON"),
                                INFO_LIST_ITEM.LABEL.as("TC_NATURE_LABEL"),
                                INFO_LIST_ITEM.ITEM_TYPE.as("TC_NATURE_TYPE"),
                                countDistinct(TEST_CASE_STEPS.STEP_ID).as("STEP_COUNT"),
                                countDistinct(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID)
                                        .as("COVERAGE_COUNT"))
                        .from(TEST_CASE_LIBRARY_NODE)
                        .innerJoin(TEST_CASE)
                        .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
                        .innerJoin(INFO_LIST_ITEM)
                        .on(TEST_CASE.TC_NATURE.eq(INFO_LIST_ITEM.ITEM_ID))
                        .leftJoin(SCRIPTED_TEST_CASE)
                        .on(TEST_CASE.TCLN_ID.eq(SCRIPTED_TEST_CASE.TCLN_ID))
                        .leftJoin(KEYWORD_TEST_CASE)
                        .on(TEST_CASE.TCLN_ID.eq(KEYWORD_TEST_CASE.TCLN_ID))
                        .leftJoin(EXPLORATORY_TEST_CASE)
                        .on(TEST_CASE.TCLN_ID.eq(EXPLORATORY_TEST_CASE.TCLN_ID))
                        .leftJoin(TEST_CASE_STEPS)
                        .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE_STEPS.TEST_CASE_ID))
                        .leftJoin(REQUIREMENT_VERSION_COVERAGE)
                        .on(
                                TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(
                                        REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID))
                        .where(TEST_CASE_LIBRARY_NODE.TCLN_ID.in(ids))
                        .groupBy(
                                TEST_CASE_LIBRARY_NODE.TCLN_ID,
                                TEST_CASE.TCLN_ID,
                                SCRIPTED_TEST_CASE.TCLN_ID,
                                KEYWORD_TEST_CASE.TCLN_ID,
                                EXPLORATORY_TEST_CASE.TCLN_ID,
                                INFO_LIST_ITEM.ITEM_ID)
                        // @formatter:on
                        .fetch()
                        .stream()
                        .collect(
                                Collectors.toMap(
                                        record -> record.get(TEST_CASE_LIBRARY_NODE.TCLN_ID),
                                        record -> {
                                            DataRow dataRow = new DataRow();
                                            dataRow.setId(
                                                    new NodeReference(
                                                                    NodeType.TEST_CASE, record.get(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                                                            .toNodeId());
                                            dataRow.setProjectId(record.get(PROJECT_ID_ALIAS, Long.class));
                                            Map<String, Object> data = record.intoMap();
                                            data.put(
                                                    "TC_KIND",
                                                    TestCaseKind.fromLeftJoinIds(
                                                            record.get(SCRIPTED_TEST_CASE_ID, Long.class),
                                                            record.get(KEYWORD_TEST_CASE_ID, Long.class),
                                                            record.get(EXPLORATORY_TEST_CASE_ID, Long.class)));
                                            dataRow.setData(data);
                                            if (!Strings.isNullOrEmpty(record.get(TEST_CASE.REFERENCE, String.class))) {
                                                dataRow
                                                        .getData()
                                                        .replace(
                                                                NAME_ALIAS,
                                                                record.get(TEST_CASE.REFERENCE)
                                                                        + " - "
                                                                        + record.get(TEST_CASE_LIBRARY_NODE.NAME));
                                            }
                                            return dataRow;
                                        }));
        Multimap<Long, Long> milestoneByTestCaseId =
                this.milestoneDisplayDao.findMilestonesByTestCaseId(testCaseRows.keySet());
        testCaseRows.forEach(
                (testCaseId, dataRow) ->
                        dataRow.addData(MILESTONES_ALIAS, milestoneByTestCaseId.get(testCaseId)));
        return testCaseRows;
    }

    @Override
    public NodeType getHandledEntityType() {
        return NodeType.TEST_CASE;
    }
}
