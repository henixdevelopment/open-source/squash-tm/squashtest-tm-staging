/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto.projectimporter.campaignworkspace;

import java.util.Date;
import java.util.Objects;
import org.squashtest.tm.domain.campaign.ActualTimePeriod;
import org.squashtest.tm.domain.campaign.ScheduledTimePeriod;

public abstract class AbstractCampaignWorkspaceEntityWithTimePeriodInfo
        extends AbstractCampaignWorkspaceEntityBaseInfo {

    protected String reference;
    protected ScheduledTimePeriod scheduledTimePeriod = new ScheduledTimePeriod();
    protected ActualTimePeriod actualTimePeriod = new ActualTimePeriod();

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public void setScheduledStartDate(Date startDate) {
        scheduledTimePeriod.setScheduledStartDate(startDate);
    }

    public Date getScheduledStartDate() {
        return scheduledTimePeriod.getScheduledStartDate();
    }

    public void setScheduledEndDate(Date endDate) {
        scheduledTimePeriod.setScheduledEndDate(endDate);
    }

    public Date getScheduledEndDate() {
        return scheduledTimePeriod.getScheduledEndDate();
    }

    public Date getActualStartDate() {
        return actualTimePeriod.getActualStartDate();
    }

    public void setActualStartDate(Date actualStartDate) {
        actualTimePeriod.setActualStartDate(actualStartDate);
    }

    public Boolean isActualStartAuto() {
        return actualTimePeriod.isActualStartAuto();
    }

    public void setActualStartAuto(Boolean actualStartAuto) {
        if (Objects.nonNull(actualStartAuto)) {
            actualTimePeriod.setActualStartAuto(actualStartAuto);
        } else {
            actualTimePeriod.setActualStartAuto(false);
        }
    }

    public Date getActualEndDate() {
        return actualTimePeriod.getActualEndDate();
    }

    public void setActualEndDate(Date actualEndDate) {
        actualTimePeriod.setActualEndDate(actualEndDate);
    }

    public Boolean isActualEndAuto() {
        return actualTimePeriod.isActualEndAuto();
    }

    public void setActualEndAuto(Boolean actualEndAuto) {
        if (Objects.nonNull(actualEndAuto)) {
            actualTimePeriod.setActualEndAuto(actualEndAuto);
        } else {
            actualTimePeriod.setActualEndAuto(false);
        }
    }
}
