/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class AttachmentListDto {

    private Long id;
    private List<AttachmentDto> attachments = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<AttachmentDto> getAttachments() {
        return attachments;
    }

    public void addAttachments(List<AttachmentDto> attachments) {
        attachments.stream()
                .filter(attachmentDto -> Objects.nonNull(attachmentDto.getId()))
                .forEach(attachmentDto -> this.attachments.add(attachmentDto));
    }

    public static AttachmentListDto fromMapEntry(Map.Entry<Long, List<AttachmentDto>> entry) {
        AttachmentListDto attachmentListDto = new AttachmentListDto();
        attachmentListDto.setId(entry.getKey());
        attachmentListDto.addAttachments(entry.getValue());
        return attachmentListDto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AttachmentListDto that = (AttachmentListDto) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
