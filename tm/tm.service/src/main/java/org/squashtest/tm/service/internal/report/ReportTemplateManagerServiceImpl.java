/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.report;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.service.report.ReportTemplateManagerService;

@Service
public class ReportTemplateManagerServiceImpl implements ReportTemplateManagerService {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(ReportTemplateManagerServiceImpl.class);
    public static final String DOCX_FILE_EXTENSION = "docx";

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public boolean addATemplateToServer(
            String filePath, String folderPath, MultipartFile uploadedFile) {
        String fileExtension = FilenameUtils.getExtension(uploadedFile.getOriginalFilename());

        if (!DOCX_FILE_EXTENSION.equals(fileExtension)) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(
                        String.format(
                                "Uploaded file does not have a valid template file extension: %s", fileExtension));
            }
            return false;
        }

        try {
            if (Files.notExists(Paths.get(folderPath))) {
                checkPathAndCreateMissingFolders(folderPath);
            }

            uploadedFile.transferTo(Paths.get(filePath));
            return true;
        } catch (IOException e) {
            LOGGER.debug("An error occurred while adding report template file", e);
            return false;
        }
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void deleteTemplateFromServer(String filePath) {
        try {
            if (Files.exists(Paths.get(filePath))) {
                FileUtils.delete(new File(filePath));
            }
        } catch (IOException e) {
            LOGGER.debug("An error occurred while deleting report template file", e);
        }
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public boolean doesTemplateExistOnServer(String filePath) {
        return new File(filePath).exists();
    }

    private void checkPathAndCreateMissingFolders(String folderPath) throws IOException {
        String[] pathParts = folderPath.split("/");
        StringBuilder builder = new StringBuilder();
        int i = 0;
        while (i < pathParts.length && builder.toString().compareTo(folderPath) != 0) {
            builder.append(pathParts[i]);
            Path tempDirectoryPath = Paths.get(builder.toString());
            if (Files.notExists(tempDirectoryPath)) {
                Files.createDirectory(tempDirectoryPath);
            }
            i++;
            builder.append("/");
        }
    }
}
