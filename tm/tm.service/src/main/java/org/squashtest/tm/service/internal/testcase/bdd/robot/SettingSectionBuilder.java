/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.bdd.robot;

import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.NEW_LINE;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.FOUR_SPACES;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.MultiLineStringBuilder;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.TEST_SETUP;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.TEST_TEARDOWN;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.isTestCaseUsingDatasets;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;
import org.squashtest.tm.domain.project.AutomationWorkflowType;
import org.squashtest.tm.domain.testcase.KeywordTestCase;
import org.squashtest.tm.service.internal.testcase.bdd.TextGridFormatter;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;

public final class SettingSectionBuilder {

    private SettingSectionBuilder() {
        throw new UnsupportedOperationException("This class is not meant to be instantiated.");
    }

    public static String buildSettingsSection(KeywordTestCase testCase, MessageSource messageSource) {
        final boolean isDatasetEnabled = isTestCaseUsingDatasets(testCase);

        final Locale locale = getScriptLocale(testCase);

        final MultiLineStringBuilder settingsBuilder =
                new MultiLineStringBuilder()
                        .appendLine(RobotSyntaxHelpers.SETTING_SECTION_TITLE)
                        .append(formatDocumentation(testCase));

        final TextGridFormatter gridFormatter = new TextGridFormatter();
        appendMetadata(messageSource, testCase, locale, gridFormatter);
        appendSquashResources(gridFormatter);
        appendTFParamServiceLibrary(isDatasetEnabled, gridFormatter);
        appendTestSetupAndTestTeardown(gridFormatter);
        settingsBuilder.append(gridFormatter.format());

        settingsBuilder.appendNewLine().appendNewLine();

        return settingsBuilder.toString();
    }

    private static void appendMetadata(
            MessageSource messageSource,
            KeywordTestCase testCase,
            Locale locale,
            TextGridFormatter gridFormatter) {
        final String metadata = "Metadata";

        final String idLabel = messageSource.getMessage("label.id", null, locale);
        gridFormatter.addRow(metadata, idLabel, testCase.getId().toString());

        if (StringUtils.isNotBlank(testCase.getReference())) {
            final String referenceLabel =
                    messageSource.getMessage("test-case.reference.label", null, locale);
            gridFormatter.addRow(metadata, referenceLabel, testCase.getReference());
        }

        if (hasAutomationWorkflowEnabled(testCase)) {
            final String priorityLabel =
                    messageSource.getMessage("testcase.bdd.script.label.automation-priority", null, locale);
            final Integer priority = extractPriority(testCase);
            gridFormatter.addRow(
                    metadata, priorityLabel, priority == null ? "null" : priority.toString());
        }

        final String importanceLabel =
                messageSource.getMessage("testcase.bdd.script.label.test-case-importance", null, locale);
        final String importanceValue =
                messageSource.getMessage(testCase.getImportance().getI18nKey(), null, locale);
        gridFormatter.addRow(metadata, importanceLabel, importanceValue);
    }

    private static boolean hasAutomationWorkflowEnabled(KeywordTestCase testCase) {
        return !AutomationWorkflowType.NONE.equals(testCase.getProject().getAutomationWorkflowType());
    }

    private static Integer extractPriority(KeywordTestCase testCase) {
        if (testCase.getAutomationRequest() == null) {
            return null;
        }

        return testCase.getAutomationRequest().getAutomationPriority();
    }

    private static String formatDocumentation(KeywordTestCase testCase) {
        final List<String> documentationContent =
                new ArrayList<>(Collections.singletonList(testCase.getName()));
        final TextGridFormatter textGridFormatter = new TextGridFormatter();

        if (hasTestCaseDescription(testCase)) {
            documentationContent.add(NEW_LINE);
            final String content = formatTestCaseDescription(testCase);
            documentationContent.addAll(List.of(content.split("\\R")));
        }

        textGridFormatter.addRows(buildDocumentationLines(documentationContent));
        return textGridFormatter.format();
    }

    private static List<List<String>> buildDocumentationLines(List<String> content) {
        return RobotSyntaxHelpers.buildMultilineEntry("Documentation", content);
    }

    private static boolean hasTestCaseDescription(KeywordTestCase testCase) {
        return StringUtils.isNotBlank(formatTestCaseDescription(testCase));
    }

    private static String formatTestCaseDescription(KeywordTestCase testCase) {
        final String fullDescription = testCase.getDescription();
        final String asText = HTMLCleanupUtils.htmlToTrimmedText(fullDescription);
        return asText.replace("\t", FOUR_SPACES);
    }

    private static void appendSquashResources(TextGridFormatter gridFormatter) {
        gridFormatter.addRow("Resource", "squash_resources.resource");
    }

    private static void appendTFParamServiceLibrary(
            boolean includeSquashTfLibrary, TextGridFormatter gridFormatter) {
        if (includeSquashTfLibrary) {
            gridFormatter.addRow("Library", "squash_tf.TFParamService");
        }
    }

    private static void appendTestSetupAndTestTeardown(TextGridFormatter gridFormatter) {
        gridFormatter.addRow(TEST_SETUP, TEST_SETUP);
        gridFormatter.addRow(TEST_TEARDOWN, TEST_TEARDOWN);
    }

    private static Locale getScriptLocale(KeywordTestCase testCase) {
        return testCase.getProject().getBddScriptLanguage().getLocale();
    }
}
