/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.scripted.gherkin;

import gherkin.ast.Background;
import gherkin.ast.DataTable;
import gherkin.ast.DocString;
import gherkin.ast.Examples;
import gherkin.ast.Feature;
import gherkin.ast.GherkinDocument;
import gherkin.ast.Node;
import gherkin.ast.Scenario;
import gherkin.ast.ScenarioDefinition;
import gherkin.ast.ScenarioOutline;
import gherkin.ast.Step;
import gherkin.ast.TableCell;
import gherkin.ast.TableRow;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import org.apache.commons.lang3.StringUtils;

// This class resembles GherkinStepGenerator as it has similar concerns (transform a raw script into
// execution info) but
// GherkinStepGenerator deals with entities and formats everything in HTML, which is not what we
// want.
public class ScriptedExecutionModelGenerator {

    public Model buildModel(GherkinDocument gherkinDocument) {
        Feature feature = gherkinDocument.getFeature();

        if (feature == null) {
            return null;
        }

        Model model = new Model();

        model.scriptName = feature.getName();

        List<ScenarioDefinition> scenarioDefinitions = feature.getChildren();

        if (scenarioDefinitions.isEmpty()) {
            return model;
        }

        ScenarioDefinition potentialBackground = scenarioDefinitions.get(0);

        if (potentialBackground instanceof Background) {
            model.prerequisite = buildBackground((Background) potentialBackground);
        }

        for (ScenarioDefinition scenarioDefinition : scenarioDefinitions) {
            if (scenarioDefinition instanceof Scenario) {
                model.steps.add(buildScenarioStep(scenarioDefinition));
            } else if (scenarioDefinition instanceof ScenarioOutline) {
                model.steps.addAll(buildScenarioOutlineSteps((ScenarioOutline) scenarioDefinition));
            }
        }

        return model;
    }

    private ExecutionStep buildScenarioStep(ScenarioDefinition scenario) {
        ExecutionStep scenarioModel = new ExecutionStep();

        scenarioModel.name = scenario.getName();
        scenarioModel.keyword = scenario.getKeyword();
        scenarioModel.description = scenario.getDescription();

        scenarioModel.steps = scenario.getSteps().stream().map(this::buildStepLine).toList();

        return scenarioModel;
    }

    private List<ExecutionStep> buildScenarioOutlineSteps(ScenarioOutline scenario) {
        return scenario.getExamples().stream()
                .flatMap(examples -> buildExamples(examples, scenario).stream())
                .toList();
    }

    private List<ExecutionStep> buildExamples(Examples examples, ScenarioOutline scenario) {
        final List<ExecutionStep> executionSteps = new ArrayList<>();
        List<String> headers = getExampleHeaders(examples);

        final int columns = headers.size();
        final int rows = examples.getTableBody().size();

        for (int i = 0; i < rows; i++) {
            ExecutionStep row = new ExecutionStep();
            row.name = scenario.getName();
            row.keyword = scenario.getKeyword();
            row.description = scenario.getDescription();

            List<String> valuesForThisLine = getExampleLineValue(examples, i);
            Map<String, String> valueByHeader = new HashMap<>();
            IntStream.range(0, columns)
                    .forEach(j -> valueByHeader.put(headers.get(j), valuesForThisLine.get(j)));

            row.steps =
                    scenario.getSteps().stream().map(step -> appendStepLine(step, valueByHeader)).toList();

            executionSteps.add(row);
        }

        return executionSteps;
    }

    private List<String> getExampleLineValue(Examples example, int i) {
        return example.getTableBody().get(i).getCells().stream().map(TableCell::getValue).toList();
    }

    private List<String> getExampleHeaders(Examples example) {
        return example.getTableHeader().getCells().stream().map(TableCell::getValue).toList();
    }

    private List<StepModel> buildBackground(Background background) {
        return background.getSteps().stream().map(this::buildStepLine).toList();
    }

    private StepModel buildStepLine(Step step) {
        StepModel stepModel = new StepModel();
        stepModel.keyword = step.getKeyword();
        stepModel.text = step.getText();
        stepModel.argument = extractArgument(step);
        return stepModel;
    }

    // this method append steps lines in scenario outline mode (ie with Gherkin equivalent of dataset
    // so we must do param substitution)
    private StepModel appendStepLine(Step step, Map<String, String> valueByHeader) {
        StepModel stepModel = new StepModel();
        stepModel.keyword = step.getKeyword();
        stepModel.text = performParamSubstitution(valueByHeader, step.getText());
        stepModel.argument = extractArgument(step);
        return stepModel;
    }

    private ArgumentModel extractArgument(Step step) {
        Node argument = step.getArgument();

        if (argument == null) {
            return null;
        }

        ArgumentModel model = new ArgumentModel();

        if (argument instanceof DocString) {
            DocString docString = (DocString) argument;
            model.kind = ArgumentModel.Kind.DOCSTRING;
            model.value = docString.getContent();
            return model;
        } else if (argument instanceof DataTable) {
            DataTable dataTable = (DataTable) argument;
            model.kind = ArgumentModel.Kind.DATATABLE;

            DataTableModel dataTableModel = new DataTableModel();
            List<List<String>> cells = new ArrayList<>();

            dataTableModel.cells = cells;
            model.dataTable = dataTableModel;

            for (TableRow tableRow : dataTable.getRows()) {
                List<String> row = new ArrayList<>();
                cells.add(row);

                for (TableCell tableCell : tableRow.getCells()) {
                    row.add(tableCell.getValue());
                }
            }
        }

        return model;
    }

    private String performParamSubstitution(Map<String, String> valueByHeader, String text) {
        // now substitute each <param> by it's value, if not found inject a placeholder
        Pattern p = Pattern.compile("<[.*?[^>]]*>");
        Matcher m = p.matcher(text);
        while (m.find()) {
            String token = m.group();
            String header = token.substring(1, token.length() - 1);
            String value = valueByHeader.get(header);
            if (StringUtils.isBlank(value)) {
                value = "<NO_DATA>";
            }
            text = text.replace(token, value);
        }
        return text;
    }

    public static class DataTableModel {
        List<List<String>> cells;

        public List<List<String>> getCells() {
            return cells;
        }
    }

    public static class ArgumentModel {
        Kind kind;
        String value;
        DataTableModel dataTable;

        public enum Kind {
            DOCSTRING,
            DATATABLE,
        }

        public Kind getKind() {
            return kind;
        }

        public String getValue() {
            return value;
        }

        public DataTableModel getDataTable() {
            return dataTable;
        }
    }

    public static class StepModel {
        String keyword;
        String text;
        ArgumentModel argument;

        public String getKeyword() {
            return keyword;
        }

        public String getText() {
            return text;
        }

        public ArgumentModel getArgument() {
            return argument;
        }
    }

    public static class ExecutionStep {
        String keyword;
        String name;
        String description;
        List<StepModel> steps;

        public String getKeyword() {
            return keyword;
        }

        public void setKeyword(String keyword) {
            this.keyword = keyword;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public List<StepModel> getSteps() {
            return steps;
        }

        public void setSteps(List<StepModel> steps) {
            this.steps = steps;
        }
    }

    public static class Model {
        String scriptName;
        List<StepModel> prerequisite;
        List<ExecutionStep> steps = new ArrayList<>();

        public String getScriptName() {
            return scriptName;
        }

        public List<StepModel> getPrerequisite() {
            return prerequisite;
        }

        public List<ExecutionStep> getSteps() {
            return steps;
        }

        public boolean hasPrerequisite() {
            return prerequisite != null && !prerequisite.isEmpty();
        }
    }
}
