/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.bdd.robot.keywords;

import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.CREATE_DICTIONARY_BUILTIN_KEYWORD;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.RETURN_RESERVED_WORD;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.assignment;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.dictionaryVariable;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.scalarVariable;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.DATASET_VARIABLE_NAME;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.FOUR_SPACES;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.RETRIEVE_DATASET_KEYWORD_NAME;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.formatDocumentationSettingLines;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.isTestCaseUsingDatasets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import org.squashtest.tm.domain.bdd.ActionWordParameterValue;
import org.squashtest.tm.domain.testcase.KeywordTestCase;
import org.squashtest.tm.domain.testcase.KeywordTestStep;
import org.squashtest.tm.service.internal.testcase.bdd.TextGridFormatter;
import org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers;
import org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers;

public final class RetrieveDatasetKeywordBuilder {

    private static final List<String> RETRIEVE_DATASET_DOCUMENTATION_LINES =
            Arrays.asList(
                    "Retrieves Squash TM's datasets and stores them in a dictionary.",
                    "",
                    "For instance, datasets containing 3 parameters \"city\", \"country\" and \"currency\"",
                    "have been defined in Squash TM.",
                    "",
                    "First, this keyword retrieves parameter values from Squash TM",
                    "and stores them into variables, using the keyword 'Get Test Param':",
                    "${city} =    Get Test Param    DS_city",
                    "",
                    "Then, this keyword stores the parameters into the &{dataset} dictionary",
                    "with each parameter name as key, and each parameter value as value:",
                    "&{dataset} =    Create Dictionary    city=${city}    country=${country}    currency=${currency}");

    private RetrieveDatasetKeywordBuilder() {
        // Not meant to be instantiated
    }

    public static String buildRetrieveDataset(KeywordTestCase keywordTestCase) {
        final boolean isDatasetEnabled = isTestCaseUsingDatasets(keywordTestCase);

        if (isDatasetEnabled) {
            final SectionBuilderHelpers.MultiLineStringBuilder stringBuilder =
                    new SectionBuilderHelpers.MultiLineStringBuilder();
            final List<String> parameterNames = extractDistinctDatasetParameterNames(keywordTestCase);

            return stringBuilder
                    .appendNewLine()
                    .appendLine(RETRIEVE_DATASET_KEYWORD_NAME)
                    .append(formatDocumentationSettingLines(RETRIEVE_DATASET_DOCUMENTATION_LINES))
                    .appendNewLine()
                    .append(formatTestCaseParameters(parameterNames))
                    .appendNewLine()
                    .append(formatDatasetDictionaryCreation(parameterNames))
                    .appendNewLine()
                    .appendLine(
                            FOUR_SPACES
                                    + RETURN_RESERVED_WORD
                                    + FOUR_SPACES
                                    + dictionaryVariable(DATASET_VARIABLE_NAME))
                    .toString();
        } else {
            return "";
        }
    }

    private static String formatDatasetDictionaryCreation(List<String> parameterNames) {
        final int numPerColumn = 4;
        final int numLines = (int) Math.ceil((double) parameterNames.size() / numPerColumn);

        final TextGridFormatter textGridFormatter = new TextGridFormatter();

        for (int lineIndex = 0; lineIndex < numLines; ++lineIndex) {
            final List<String> row = new ArrayList<>();

            if (lineIndex == 0) {
                row.add(assignment(dictionaryVariable(DATASET_VARIABLE_NAME)));
                row.add(CREATE_DICTIONARY_BUILTIN_KEYWORD);
            } else {
                row.add("...");
                row.add("");
            }

            final int startIndex = lineIndex * numPerColumn;
            final int endIndex = Math.min(parameterNames.size(), startIndex + numPerColumn);
            final List<String> paramsForThisRow = parameterNames.subList(startIndex, endIndex);
            final List<String> dictionaryEntries =
                    paramsForThisRow.stream()
                            .map(paramName -> String.format("%s=%s", paramName, scalarVariable(paramName)))
                            .toList();

            row.addAll(dictionaryEntries);

            textGridFormatter.addRow(row);
        }

        return textGridFormatter.format(TextGridFormatter.withRowPrefix(FOUR_SPACES));
    }

    private static List<String> extractDistinctDatasetParameterNames(
            KeywordTestCase keywordTestCase) {
        return keywordTestCase.getSteps().stream()
                .flatMap(
                        step ->
                                ((KeywordTestStep) step)
                                        .getParamValues().stream()
                                                .sorted(Comparator.comparingDouble(ActionWordParameterValue::getId)))
                .filter(distinctByKey(ActionWordParameterValue::getValue))
                .filter(ActionWordParameterValue::isLinkedToTestCaseParam)
                .map(
                        paramValue -> {
                            String value = paramValue.getValue();
                            return value.substring(1, value.length() - 1);
                        })
                .toList();
    }

    private static String formatTestCaseParameters(List<String> parameterNames) {
        final TextGridFormatter textGridFormatter = new TextGridFormatter();
        textGridFormatter.addRows(buildTestCaseParametersLines(parameterNames));
        return textGridFormatter.format(TextGridFormatter.withRowPrefix(FOUR_SPACES));
    }

    private static List<List<String>> buildTestCaseParametersLines(List<String> parameterNames) {
        return parameterNames.stream()
                .map(
                        paramName ->
                                List.of(
                                        assignment(RobotSyntaxHelpers.scalarVariable(paramName)),
                                        "Get Test Param",
                                        String.format("DS_%s", paramName)))
                .toList();
    }

    /** Predicate used to filter a stream by distinct attribute. */
    private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = new HashSet<>();
        return t -> seen.add(keyExtractor.apply(t));
    }
}
