/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.servers;

import javax.validation.constraints.NotBlank;
import org.squashtest.tm.domain.servers.AuthenticationProtocol;
import org.squashtest.tm.service.servers.ServerAuthConfiguration;
import org.squashtest.tm.service.spi.GenericServerOAuth2Conf;

public class ServerOAuth2Conf implements ServerAuthConfiguration, GenericServerOAuth2Conf {

    @NotBlank private String grantType = "";
    @NotBlank private String clientId = "";
    @NotBlank private String clientSecret = "";
    @NotBlank private String authorizationUrl = "";
    @NotBlank private String requestTokenUrl = "";
    @NotBlank private String callbackUrl = "";
    @NotBlank private String scope = "";

    public ServerOAuth2Conf() {}

    public ServerOAuth2Conf(
            String grantType,
            String clientId,
            String clientSecret,
            String authorizationUrl,
            String requestTokenUrl,
            String callbackUrl,
            String scope) {
        this.grantType = grantType;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.authorizationUrl = authorizationUrl;
        this.requestTokenUrl = requestTokenUrl;
        this.callbackUrl = callbackUrl;
        this.scope = scope;
    }

    @Override
    public AuthenticationProtocol getImplementedProtocol() {
        return AuthenticationProtocol.OAUTH_2;
    }

    public String getGrantType() {
        return grantType;
    }

    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getAuthorizationUrl() {
        return authorizationUrl;
    }

    public void setAuthorizationUrl(String authorizationUrl) {
        this.authorizationUrl = authorizationUrl;
    }

    public String getRequestTokenUrl() {
        return requestTokenUrl;
    }

    public void setRequestTokenUrl(String requestTokenUrl) {
        this.requestTokenUrl = requestTokenUrl;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }
}
