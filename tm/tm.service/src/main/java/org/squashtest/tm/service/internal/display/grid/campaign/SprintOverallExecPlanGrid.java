/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.campaign;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.when;
import static org.squashtest.tm.domain.project.AutomationWorkflowType.NONE;
import static org.squashtest.tm.domain.testautomation.TestAutomationServerKind.jenkins;
import static org.squashtest.tm.domain.testautomation.TestAutomationServerKind.squashOrchestrator;
import static org.squashtest.tm.domain.testcase.TestCaseAutomatable.Y;
import static org.squashtest.tm.domain.tf.automationrequest.AutomationRequestStatus.AUTOMATED;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATION_REQUEST;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.jooq.domain.Tables.DATASET;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_SESSION_OVERVIEW;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.RESOURCE;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.TEST_AUTOMATION_SERVER;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_PLAN_ITEM;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ASSIGNEE_FULL_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ASSIGNEE_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ASSIGNEE_LOGIN;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.DATASET_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.DATASET_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_LAST_EXECUTED_ON;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_LATEST_STATUS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_STATUS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.IMPORTANCE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.INFERRED_EXECUTION_MODE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ITEM_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LAST_EXECUTION_MODE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LATEST_EXECUTION_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PROJECT_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PROJECT_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.SPRINT_REQ_VERSION_NAME_WITH_REF;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TCLN_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TC_KIND;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_CASE_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_CASE_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_CASE_REFERENCE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_PLAN_ITEM_ID;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.SelectHavingStep;
import org.jooq.SortField;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.impl.DSL;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.testcase.TestCaseExecutionMode;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.jooq.domain.tables.records.TestPlanItemRecord;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;
import org.squashtest.tm.service.internal.display.grid.columns.LevelEnumColumn;

public class SprintOverallExecPlanGrid extends AbstractTestPlanGrid {

    private final long sprintId;

    public SprintOverallExecPlanGrid(long sprintId, String userToRestrictTo) {
        super(userToRestrictTo);
        this.sprintId = sprintId;
    }

    @Override
    protected Table<TestPlanItemRecord> getItemTable() {
        return TEST_PLAN_ITEM;
    }

    @Override
    protected TableField<TestPlanItemRecord, Long> getItemIdColumn() {
        return TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID;
    }

    @Override
    protected TableField<TestPlanItemRecord, Long> getAssigneeIdColumn() {
        return TEST_PLAN_ITEM.ASSIGNEE_ID;
    }

    @Override
    protected TableField<TestPlanItemRecord, Long> getTestCaseIdColumn() {
        return TEST_PLAN_ITEM.TCLN_ID;
    }

    @Override
    protected List<GridColumn> getColumns() {
        return Arrays.asList(
                new GridColumn(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID.as(TEST_PLAN_ITEM_ID)),
                new GridColumn(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.as(PROJECT_ID)),
                new GridColumn(PROJECT.NAME.as(PROJECT_NAME), PROJECT.NAME),
                new GridColumn(TEST_CASE.REFERENCE.as(TEST_CASE_REFERENCE), TEST_CASE.REFERENCE),
                new GridColumn(TEST_CASE.TCLN_ID.as(TEST_CASE_ID)),
                new GridColumn(getTestCaseKind().field(TC_KIND)),
                new GridColumn(TEST_CASE_LIBRARY_NODE.NAME.as(TEST_CASE_NAME), TEST_CASE_LIBRARY_NODE.NAME),
                new LevelEnumColumn(TestCaseImportance.class, IMPORTANCE),
                new LevelEnumColumn(ExecutionStatus.class, EXECUTION_STATUS),
                new GridColumn(TEST_PLAN_ITEM.ASSIGNEE_ID.as(ASSIGNEE_ID)),
                new GridColumn(CORE_USER.LOGIN.as(ASSIGNEE_LOGIN)),
                new GridColumn(getUser().field(ASSIGNEE_FULL_NAME)),
                new GridColumn(TEST_PLAN_ITEM.LAST_EXECUTED_ON),
                new GridColumn(selectFromLatestExecutionForTestPlanItem().field(LATEST_EXECUTION_ID)),
                new GridColumn(selectFromLatestExecutionForTestPlanItem().field(LAST_EXECUTION_MODE)),
                new GridColumn(selectFromLatestExecutionForTestPlanItem().field(EXECUTION_LATEST_STATUS)),
                new GridColumn(
                        selectFromLatestExecutionForTestPlanItem().field(EXECUTION_LAST_EXECUTED_ON)),
                new GridColumn(DATASET.DATASET_ID.as(DATASET_ID)),
                new GridColumn(DATASET.NAME.as(DATASET_NAME), DATASET.NAME),
                new GridColumn(
                        Objects.requireNonNull(getInferredExecutionMode().field(INFERRED_EXECUTION_MODE))
                                .as(INFERRED_EXECUTION_MODE)),
                new GridColumn(EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID),
                new GridColumn(SPRINT_REQ_VERSION.SPRINT_REQ_VERSION_ID),
                new GridColumn(getSprintReqNameWithRef()));
    }

    @Override
    protected Table<?> getTable() {

        final SelectHavingStep<?> getUser = getUser();
        final SelectHavingStep<?> getInferredExecutionMode = getInferredExecutionMode();
        final SelectHavingStep<?> testCaseKind = getTestCaseKind();
        final SelectHavingStep<?> getLatestExecutionData = selectFromLatestExecutionForTestPlanItem();

        return getFilteredTableOnAssigneeIfNotExploratory(
                TEST_PLAN_ITEM
                        .leftJoin(TEST_CASE)
                        .on(TEST_CASE.TCLN_ID.eq(TEST_PLAN_ITEM.TCLN_ID))
                        .leftJoin(TEST_CASE_LIBRARY_NODE)
                        .on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_PLAN_ITEM.TCLN_ID))
                        .leftJoin(PROJECT)
                        .on(PROJECT.PROJECT_ID.eq(TEST_CASE_LIBRARY_NODE.PROJECT_ID))
                        .innerJoin(SPRINT_REQ_VERSION)
                        .on(SPRINT_REQ_VERSION.TEST_PLAN_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ID))
                        .leftJoin(REQUIREMENT_VERSION)
                        .on(REQUIREMENT_VERSION.RES_ID.eq(SPRINT_REQ_VERSION.REQ_VERSION_ID))
                        .leftJoin(RESOURCE)
                        .on(RESOURCE.RES_ID.eq(REQUIREMENT_VERSION.RES_ID))
                        .innerJoin(SPRINT)
                        .on(SPRINT.CLN_ID.eq(SPRINT_REQ_VERSION.SPRINT_ID))
                        .leftJoin(DATASET)
                        .on(DATASET.DATASET_ID.eq(TEST_PLAN_ITEM.DATASET_ID))
                        .leftJoin(AUTOMATION_REQUEST)
                        .on(AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID.eq(TEST_CASE.AUTOMATION_REQUEST_ID))
                        .leftJoin(TEST_AUTOMATION_SERVER)
                        .on(TEST_AUTOMATION_SERVER.SERVER_ID.eq(PROJECT.TA_SERVER_ID))
                        .innerJoin(CAMPAIGN_LIBRARY_NODE)
                        .on(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(SPRINT.CLN_ID))
                        .leftJoin(CORE_USER)
                        .on(CORE_USER.PARTY_ID.eq(TEST_PLAN_ITEM.ASSIGNEE_ID))
                        .leftJoin(getUser)
                        .on(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID.eq(getUser.field(ITEM_ID, Long.class)))
                        .leftJoin(EXPLORATORY_SESSION_OVERVIEW)
                        .on(EXPLORATORY_SESSION_OVERVIEW.TEST_PLAN_ITEM_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID))
                        .leftJoin(getInferredExecutionMode)
                        .on(
                                Objects.requireNonNull(getInferredExecutionMode.field(TCLN_ID, Long.class))
                                        .eq(TEST_CASE.TCLN_ID))
                        .leftJoin(getLatestExecutionData)
                        .on(
                                TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID.eq(
                                        getLatestExecutionData.field(TEST_PLAN_ITEM_ID, Long.class)))
                        .leftJoin(testCaseKind)
                        .on(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID.eq(testCaseKind.field(ITEM_ID, Long.class)))
                        .asTable());
    }

    /*
    If the sprint is ready or running, the sprint requirement version gets its data from the requirement version
    (tables REQUIREMENT_VERSION AND RESOURCE), except for the case of synchronized requirements that already
    have data in the table SPRINT_REQUIREMENT_VERSION.
    When the sprint is closed, data from the requirement version are denormalized in the SPRINT_REQUIREMENT_VERSION table.
     */
    private static Field<String> getSprintReqNameWithRef() {
        return when(
                        SPRINT_REQ_VERSION.NAME.isNull(),
                        when(
                                        REQUIREMENT_VERSION.REFERENCE.isNull().or(REQUIREMENT_VERSION.REFERENCE.eq("")),
                                        RESOURCE.NAME)
                                .otherwise(
                                        DSL.concat(REQUIREMENT_VERSION.REFERENCE, DSL.inline(" - "), RESOURCE.NAME)))
                .otherwise(
                        when(SPRINT_REQ_VERSION.REFERENCE.isNull(), SPRINT_REQ_VERSION.NAME)
                                .otherwise(
                                        DSL.concat(
                                                SPRINT_REQ_VERSION.REFERENCE, DSL.inline(" - "), SPRINT_REQ_VERSION.NAME)))
                .as(SPRINT_REQ_VERSION_NAME_WITH_REF);
    }

    private static SelectHavingStep<Record2<Long, String>> getInferredExecutionMode() {
        return DSL.select(
                        TEST_CASE.TCLN_ID,
                        when(
                                        TEST_CASE.EXECUTION_MODE.eq(TestCaseExecutionMode.EXPLORATORY.name()),
                                        TestCaseExecutionMode.EXPLORATORY.name())
                                .otherwise(
                                        when(
                                                        (PROJECT
                                                                        .AUTOMATION_WORKFLOW_TYPE
                                                                        .eq(NONE.name())
                                                                        .or(
                                                                                PROJECT
                                                                                        .AUTOMATION_WORKFLOW_TYPE
                                                                                        .ne(NONE.name())
                                                                                        .and(TEST_CASE.AUTOMATABLE.eq(Y.name()))
                                                                                        .and(
                                                                                                AUTOMATION_REQUEST.REQUEST_STATUS.eq(
                                                                                                        AUTOMATED.name()))))
                                                                // if Jenkins server, TestCase only has to be linked to an
                                                                // automation test
                                                                .and(
                                                                        (TEST_AUTOMATION_SERVER
                                                                                        .KIND
                                                                                        .eq(jenkins.name())
                                                                                        .and(TEST_CASE.TA_TEST.isNotNull()))
                                                                                // if Squash Autom server, then the 3 automation attributes
                                                                                // must
                                                                                // exist
                                                                                .or(
                                                                                        TEST_AUTOMATION_SERVER
                                                                                                .KIND
                                                                                                .eq(squashOrchestrator.name())
                                                                                                .and(
                                                                                                        TEST_CASE.AUTOMATED_TEST_TECHNOLOGY.isNotNull())
                                                                                                .and(TEST_CASE.AUTOMATED_TEST_REFERENCE.isNotNull())
                                                                                                .and(TEST_CASE.SCM_REPOSITORY_ID.isNotNull()))),
                                                        TestCaseExecutionMode.AUTOMATED.name())
                                                .otherwise(TestCaseExecutionMode.MANUAL.name()))
                                .as(INFERRED_EXECUTION_MODE))
                .from(TEST_CASE)
                .leftJoin(TEST_CASE_LIBRARY_NODE)
                .on(TEST_CASE.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                .leftJoin(PROJECT)
                .on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .leftJoin(AUTOMATION_REQUEST)
                .on(AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID.eq(TEST_CASE.AUTOMATION_REQUEST_ID))
                .leftJoin(TEST_AUTOMATION_SERVER)
                .on(TEST_AUTOMATION_SERVER.SERVER_ID.eq(PROJECT.TA_SERVER_ID));
    }

    @Override
    protected Field<?> getIdentifier() {
        return DSL.field(TEST_PLAN_ITEM_ID);
    }

    @Override
    protected Field<?> getProjectIdentifier() {
        return DSL.field(PROJECT_ID);
    }

    @Override
    protected SortField<?> getDefaultOrder() {
        return field(TEST_PLAN_ITEM_ID).asc();
    }

    @Override
    protected Condition craftInvariantFilter() {
        return SPRINT.CLN_ID.eq(sprintId);
    }
}
