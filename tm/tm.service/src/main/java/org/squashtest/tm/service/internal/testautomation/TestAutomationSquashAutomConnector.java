/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation;

import static java.util.Objects.isNull;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static org.squashtest.tm.domain.servers.AuthenticationProtocol.TOKEN_AUTH;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.squashtest.csp.core.bugtracker.core.UnsupportedAuthenticationModeException;
import org.squashtest.tm.api.plugin.EntityType;
import org.squashtest.tm.core.foundation.lang.Couple;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.servers.AuthenticationProtocol;
import org.squashtest.tm.domain.servers.Credentials;
import org.squashtest.tm.domain.servers.TokenAuthCredentials;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.domain.testautomation.AutomatedSuite;
import org.squashtest.tm.domain.testautomation.AutomatedSuiteWorkflow;
import org.squashtest.tm.domain.testautomation.AutomatedTest;
import org.squashtest.tm.domain.testautomation.TestAutomationProject;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.domain.testautomation.TestAutomationServerKind;
import org.squashtest.tm.domain.users.ApiTokenPermission;
import org.squashtest.tm.security.UserContextHolder;
import org.squashtest.tm.service.internal.configuration.CallbackUrlProvider;
import org.squashtest.tm.service.internal.dto.WorkflowDto;
import org.squashtest.tm.service.internal.repository.display.EntityPathHeaderDao;
import org.squashtest.tm.service.internal.testautomation.httpclient.BusClientFactory;
import org.squashtest.tm.service.internal.testautomation.httpclient.ClientRuntimeException;
import org.squashtest.tm.service.internal.testautomation.httpclient.MessageRefusedException;
import org.squashtest.tm.service.internal.testautomation.httpclient.SquashAutomWorkflowClient;
import org.squashtest.tm.service.internal.testautomation.httpclient.UnexpectedServerResponseException;
import org.squashtest.tm.service.internal.testautomation.httpclient.WorkflowClientFactory;
import org.squashtest.tm.service.internal.testautomation.model.IterationTestPlanItemWithCustomFields;
import org.squashtest.tm.service.internal.testautomation.model.TestAutomationServerAndNamespace;
import org.squashtest.tm.service.internal.testautomation.model.TestPlanContext;
import org.squashtest.tm.service.license.UltimateLicenseAvailabilityService;
import org.squashtest.tm.service.orchestrator.model.NameAndVersion;
import org.squashtest.tm.service.orchestrator.model.OrchestratorConfVersions;
import org.squashtest.tm.service.orchestrator.model.OrchestratorResponse;
import org.squashtest.tm.service.servers.CredentialsProvider;
import org.squashtest.tm.service.servers.StoredCredentialsManager;
import org.squashtest.tm.service.testautomation.AutomatedSuiteManagerService;
import org.squashtest.tm.service.testautomation.model.AutomatedExecutionEnvironment;
import org.squashtest.tm.service.testautomation.model.SquashAutomExecutionConfiguration;
import org.squashtest.tm.service.testautomation.spi.InvalidSquashOrchestratorConfigurationException;
import org.squashtest.tm.service.testautomation.spi.OutdatedSquashOrchestratorException;
import org.squashtest.tm.service.testautomation.spi.ServerConnectionFailed;
import org.squashtest.tm.service.testautomation.spi.TestAutomationConnector;
import org.squashtest.tm.service.testautomation.spi.TestAutomationException;
import org.squashtest.tm.service.testautomation.spi.TestAutomationServerNoCredentialsException;
import org.squashtest.tm.service.user.ApiTokenService;

@Service
public class TestAutomationSquashAutomConnector implements TestAutomationConnector {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(TestAutomationSquashAutomConnector.class);
    private static final TestAutomationServerKind CONNECTOR_KIND =
            TestAutomationServerKind.squashOrchestrator;
    private static final String GENERIC_REST_CALL_ERROR_MESSAGE =
            "Failed to manage REST call for technical reasons";
    private static final String AUTOMATED_SUITE_TOKEN_SUFFIX = "-token";
    private static final long MILLSECONDS_IN_THREE_DAYS = 3L * 24 * 60 * 60 * 1000;
    private static final String TEMPORARY_TOKEN_DATE_FORMAT = "yyyyMMddHHmmss";

    @Value("${squashtm.testautomationserver.timeout:15}")
    private int requestTimeoutSeconds;

    @Inject private CredentialsProvider credentialsProvider;

    @Inject private StoredCredentialsManager storedCredentialsManager;

    @Inject private MessageSource i18nHelper;

    @Inject private CallbackUrlProvider callbackUrlProvider;

    @Inject private ApiTokenService apiTokenService;

    @Inject private StartTestExecutionProvider startTestExecutionProvider;

    @Inject private AutomatedSuiteManagerService automatedSuiteManagerService;

    @Inject private EntityPathHeaderDao entityPathHeaderDao;

    @Value("${info.app.version:#{null}}")
    private String squashTMVersion;

    @Inject private UltimateLicenseAvailabilityService ultimateLicenseService;

    private final BusClientFactory busClientFactory;
    private final WorkflowClientFactory workflowClientFactory;

    public TestAutomationSquashAutomConnector() {
        busClientFactory = new BusClientFactory();
        workflowClientFactory = new WorkflowClientFactory();
    }

    private String getMessage(String i18nKey) {
        Locale locale = LocaleContextHolder.getLocale();
        return i18nHelper.getMessage(i18nKey, null, locale);
    }

    @Override
    public TestAutomationServerKind getConnectorKind() {
        return CONNECTOR_KIND;
    }

    @Override
    public boolean checkCredentials(
            TestAutomationServer testAutomationServer, String login, String password) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean checkCredentials(
            TestAutomationServer testAutomationServer, Credentials credentials) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Collection<TestAutomationProject> listProjectsOnServer(
            TestAutomationServer testAutomationServer, Credentials credentials) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Collection<AutomatedTest> listTestsInProject(
            TestAutomationProject testAutomationProject, String username) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void executeParameterizedTests(
            Collection<Couple<AutomatedExecutionExtender, Map<String, Object>>> tests,
            String externalId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<AutomatedSuiteWorkflow> executeParameterizedTestsBasedOnITPICollection(
            Collection<IterationTestPlanItemWithCustomFields> tests,
            String suiteId,
            Collection<SquashAutomExecutionConfiguration> configurations) {

        Map<Long, String> namespaceByTmProjectId = computeNamespaceByTmProjectId(configurations);

        Map<TestAutomationServerAndNamespace, List<IterationTestPlanItemWithCustomFields>>
                itemsByServerAndNamespace =
                        mapParametrizedItemsByServerAndNamespace(tests, namespaceByTmProjectId);

        List<BuildDef> buildDefList = mapToBuildDef(itemsByServerAndNamespace);

        String callbackUrl = callbackUrlProvider.getCallbackUrl().toExternalForm();

        TestPlanContext testPlanContext = buildTestPlanContext(suiteId);

        final String token = createTemporaryApiTokenForAutomatedSuite(suiteId);

        try {
            List<AutomatedSuiteWorkflow> workflows = new ArrayList<>();
            for (BuildDef buildDef : buildDefList) {
                Long projectId = buildDef.getProjectId();
                if (projectId == null) {
                    throw new IllegalArgumentException("Cannot have a null project id in workflow");
                }
                String workflowId =
                        startTestExecutionProvider
                                .create(
                                        buildDef,
                                        configurations,
                                        suiteId,
                                        testPlanContext,
                                        callbackUrl,
                                        token,
                                        workflowClientFactory,
                                        busClientFactory,
                                        entityPathHeaderDao,
                                        ultimateLicenseService.isAvailable())
                                .run();

                AutomatedSuiteWorkflow workflow = new AutomatedSuiteWorkflow(workflowId, projectId);
                workflows.add(workflow);
            }
            return workflows;
        } catch (MessageRefusedException ex) {
            LOGGER.error(GENERIC_REST_CALL_ERROR_MESSAGE, ex);
            Object[] args = new Object[] {ex.getEndpoint(), ex.getHttpErrorCode()};
            throw new ServerConnectionFailed(
                    i18nHelper.getMessage(
                            "testautomation.exceptions.connection.error",
                            args,
                            "the orchestrator is unreachable",
                            LocaleContextHolder.getLocale()));
        } catch (ClientRuntimeException ex) {
            LOGGER.error(GENERIC_REST_CALL_ERROR_MESSAGE, ex);
            Object[] args = new Object[] {ex.getEndpoint()};
            throw new ServerConnectionFailed(
                    i18nHelper.getMessage(
                            "testautomation.exceptions.connection.timeout",
                            args,
                            "the orchestrator is unreachable",
                            LocaleContextHolder.getLocale()));
        } catch (InvalidSquashOrchestratorConfigurationException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new InvalidSquashOrchestratorConfigurationException(ex.getMessage());
        } catch (OutdatedSquashOrchestratorException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new OutdatedSquashOrchestratorException(ex.getMessage());
        } catch (Exception ex) {
            throw new TestAutomationException(GENERIC_REST_CALL_ERROR_MESSAGE, ex);
        }
    }

    private String createTemporaryApiTokenForAutomatedSuite(String suiteId) {
        long nowMillis = System.currentTimeMillis();
        Date threeDaysFromNow = new Date(nowMillis + MILLSECONDS_IN_THREE_DAYS);

        return apiTokenService
                .generateApiToken(
                        generateTokenName(suiteId, nowMillis),
                        threeDaysFromNow,
                        String.valueOf(ApiTokenPermission.READ_WRITE))
                .generatedJwtToken();
    }

    private String generateTokenName(String suiteId, long nowMillis) {
        SimpleDateFormat formatter = new SimpleDateFormat(TEMPORARY_TOKEN_DATE_FORMAT);
        String timestamp = formatter.format(new Date(nowMillis));
        return timestamp + "-" + suiteId + AUTOMATED_SUITE_TOKEN_SUFFIX;
    }

    private TestPlanContext buildTestPlanContext(String suiteId) {
        AutomatedSuite automatedSuite = automatedSuiteManagerService.findById(suiteId);
        if (automatedSuite.getIteration() != null) {
            Iteration iteration = automatedSuite.getIteration();
            String path = entityPathHeaderDao.buildIterationPathHeader(iteration.getId());
            String entityName = iteration.getName();
            return new TestPlanContext(
                    EntityType.ITERATION, entityName, path, iteration.getUuid(), squashTMVersion);
        } else if (automatedSuite.getTestSuite() != null) {
            TestSuite testSuite = automatedSuite.getTestSuite();
            String path = entityPathHeaderDao.buildTestSuitePathHeader(testSuite.getId());
            String entityName = testSuite.getName();
            return new TestPlanContext(
                    EntityType.TEST_SUITE, entityName, path, testSuite.getUuid(), squashTMVersion);
        } else {
            throw new IllegalArgumentException(
                    "Automated suite is not attached to any iteration nor test suite.");
        }
    }

    /**
     * Given a Collection of {@link SquashAutomExecutionConfiguration}s, computes a Map which keys are
     * all the involved Tm Project ids and which values are the single namespace determined for this
     * project. The current implementation is a recursive pick of the namespace which is the most
     * requested in the configurations. It minimizes the number of different namespaces i.e. the
     * number of workflows which will be created.
     *
     * @param configurations the Collection of {@link SquashAutomExecutionConfiguration}s
     * @return the Map of (tm project id -> determined namespace)
     */
    private Map<Long, String> computeNamespaceByTmProjectId(
            Collection<SquashAutomExecutionConfiguration> configurations) {
        Map<String, List<Long>> projectIdsByNamespace = computeProjectIdsByNamespaceMap(configurations);

        List<Map.Entry<String, List<Long>>> projectIdsByNamespaceEntryList =
                new ArrayList<>(projectIdsByNamespace.entrySet());

        Map<Long, String> namespaceByProjectId = new HashMap<>();

        while (!projectIdsByNamespaceEntryList.isEmpty()) {
            Map.Entry<String, List<Long>> entryWithTheMostProjectIds =
                    removeEntryWithMostProjectIds(projectIdsByNamespaceEntryList);
            fillResultMapAndUpdateTransientList(
                    projectIdsByNamespaceEntryList, entryWithTheMostProjectIds, namespaceByProjectId);
        }
        return namespaceByProjectId;
    }

    /**
     * Given the entry with the most project ids, add entries (project id -> namespace) in the result
     * Map, then update the transient List of Entries by removing the project ids which were selected
     * in all Lists.
     *
     * @param projectIdsByNamespaceEntryList transient list
     * @param entryWithTheMostProjectIds entry of the Map(namespace -> project ids) with the most
     *     project ids
     * @param namespaceByProjectId the result Map(project id -> namespace)
     */
    private void fillResultMapAndUpdateTransientList(
            List<Map.Entry<String, List<Long>>> projectIdsByNamespaceEntryList,
            Map.Entry<String, List<Long>> entryWithTheMostProjectIds,
            Map<Long, String> namespaceByProjectId) {
        entryWithTheMostProjectIds
                .getValue()
                .forEach(
                        projectId -> {
                            namespaceByProjectId.put(projectId, entryWithTheMostProjectIds.getKey());
                            projectIdsByNamespaceEntryList.forEach(
                                    entry -> {
                                        List<Long> projectIdsList = entry.getValue();
                                        projectIdsList.remove(projectId);
                                    });
                            projectIdsByNamespaceEntryList.removeIf(entry -> entry.getValue().isEmpty());
                        });
    }

    /**
     * Given the entries List from the Map(namespace -> project ids), remove and return the entry
     * which has the most project ids.
     *
     * @param projectIdsByNamespaceEntryList entries List from the Map(namespace -> project ids)
     * @return the Entry with the most project ids
     */
    private Map.Entry<String, List<Long>> removeEntryWithMostProjectIds(
            List<Map.Entry<String, List<Long>>> projectIdsByNamespaceEntryList) {
        projectIdsByNamespaceEntryList.sort(
                Comparator.comparingInt((Map.Entry<String, List<Long>> entry) -> entry.getValue().size())
                        .reversed());
        return projectIdsByNamespaceEntryList.remove(0);
    }

    /**
     * Given the list of {@link SquashAutomExecutionConfiguration}s, computes a Map which keys are the
     * involved namespaces and the values are the Lists of Tm Project ids which requested to be
     * executed in this namespace.
     *
     * @param configurations the collection of Squash Autom configurations
     * @return the Map of namespace -> project ids
     */
    private Map<String, List<Long>> computeProjectIdsByNamespaceMap(
            Collection<SquashAutomExecutionConfiguration> configurations) {
        Map<String, List<Long>> projectIdsByNamespace = new HashMap<>();
        configurations.forEach(
                configuration -> {
                    Long projectId = configuration.getProjectId();
                    configuration
                            .getNamespaces()
                            .forEach(
                                    namespace -> {
                                        List<Long> projectIdsForTag = projectIdsByNamespace.get(namespace);
                                        if (isNull(projectIdsForTag)) {
                                            projectIdsByNamespace.put(namespace, new ArrayList<>());
                                            projectIdsForTag = projectIdsByNamespace.get(namespace);
                                        }
                                        projectIdsForTag.add(projectId);
                                    });
                });
        return projectIdsByNamespace;
    }

    @Override
    public URL findTestAutomationProjectURL(TestAutomationProject testAutomationProject) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean testListIsOrderGuaranteed(Collection<AutomatedTest> collection) {
        throw new UnsupportedOperationException();
    }

    @Override
    public AuthenticationProtocol[] getSupportedProtocols() {
        return new AuthenticationProtocol[] {TOKEN_AUTH};
    }

    private Map<TestAutomationServerAndNamespace, List<IterationTestPlanItemWithCustomFields>>
            mapParametrizedItemsByServerAndNamespace(
                    Collection<IterationTestPlanItemWithCustomFields> tests,
                    Map<Long, String> namespaceByProjectId) {

        // The equals method doesn't seem to do the job here, so ...
        Map<Long, TestAutomationServer> serverMap =
                tests.stream()
                        .map(
                                item ->
                                        item.getIterationTestPlanItem()
                                                .getReferencedTestCase()
                                                .getProject()
                                                .getTestAutomationServer())
                        .collect(
                                Collectors.toMap(
                                        TestAutomationServer::getId,
                                        server -> server,
                                        (existing, replacement) -> existing,
                                        LinkedHashMap::new));

        return tests.stream()
                .collect(
                        groupingBy(
                                item -> {
                                    org.squashtest.tm.domain.project.Project project =
                                            item.getIterationTestPlanItem().getReferencedTestCase().getProject();
                                    Long serverId = project.getTestAutomationServer().getId();
                                    String namespace = namespaceByProjectId.get(project.getId());
                                    return new TestAutomationServerAndNamespace(serverMap.get(serverId), namespace);
                                },
                                LinkedHashMap::new,
                                toList()));
    }

    private List<BuildDef> mapToBuildDef(
            Map<TestAutomationServerAndNamespace, List<IterationTestPlanItemWithCustomFields>>
                    itemsByServerAndNamespace) {
        return itemsByServerAndNamespace.entrySet().stream()
                .map(
                        entry -> {
                            String namespace = entry.getKey().getNamespace();
                            org.squashtest.tm.domain.project.Project project =
                                    entry
                                            .getValue()
                                            .get(0)
                                            .getIterationTestPlanItem()
                                            .getReferencedTestCase()
                                            .getProject();
                            TestAutomationServer automationServer = entry.getKey().getTestAutomationServer();
                            TokenAuthCredentials credentials;
                            Optional<TokenAuthCredentials> projectCredentials =
                                    getOptionalTmProjectCredentials(automationServer.getId(), project.getId());
                            credentials =
                                    projectCredentials.orElseGet(
                                            () -> getAutomationServerCredentials(automationServer));
                            return new BuildDef(automationServer, credentials, namespace, entry.getValue());
                        })
                .toList();
    }

    private TokenAuthCredentials getAutomationServerCredentials(
            TestAutomationServer automationServer) {
        Optional<Credentials> maybeCredentials =
                credentialsProvider.getAppLevelCredentials(automationServer);
        Supplier<TestAutomationServerNoCredentialsException> throwIfNull =
                () -> {
                    throw new TestAutomationServerNoCredentialsException(
                            String.format(
                                    getMessage("message.testAutomationServer.noCredentials"),
                                    automationServer.getName()));
                };
        Credentials credentials = maybeCredentials.orElseThrow(throwIfNull);
        return asTokenAuthCredentials(credentials);
    }

    private Optional<TokenAuthCredentials> getOptionalTmProjectCredentials(
            Long serverId, Long projectId) {
        return Optional.ofNullable(storedCredentialsManager.findProjectCredentials(serverId, projectId))
                .filter(TokenAuthCredentials.class::isInstance)
                .map(TokenAuthCredentials.class::cast);
    }

    private TokenAuthCredentials asTokenAuthCredentials(Credentials credentials) {
        AuthenticationProtocol protocol = credentials.getImplementedProtocol();
        if (!this.supports(protocol)) {
            throw new UnsupportedAuthenticationModeException(protocol.toString());
        }
        return (TokenAuthCredentials) credentials;
    }

    Authentication getUserAuthentication() {
        return UserContextHolder.getAuthentication();
    }

    @Override
    public List<AutomatedExecutionEnvironment> getAllAccessibleEnvironments(
            TestAutomationServer testAutomationServer) {
        final String token = getAutomationServerCredentials(testAutomationServer).getToken();
        checkConfigurationValidity(testAutomationServer, token);
        return getAllAccessibleEnvironmentsWithToken(testAutomationServer, token);
    }

    private void checkConfigurationValidity(TestAutomationServer testAutomationServer, String token) {
        ConfigurationVerifier.verify(token, testAutomationServer, i18nHelper);
        List<NameAndVersion> nameAndVersions =
                getOrchestratorConfVersionsWithToken(testAutomationServer, token)
                        .getResponse()
                        .getBaseImages();

        if (nameAndVersions == null) {
            LOGGER.warn("Squash orchestrator version could not be retrieved, which may cause issues.");
            return;
        }

        String squashOrchestratorAllinoneVersion = nameAndVersions.get(0).getVersion();
        ConfigurationVerifier.checkSquashOrchestratorVersionValidity(
                squashOrchestratorAllinoneVersion, squashTMVersion, i18nHelper);
    }

    @Override
    public List<AutomatedExecutionEnvironment> getAllAccessibleEnvironments(
            TestAutomationServer testAutomationServer, Credentials credentials) {
        final TokenAuthCredentials tokenAuthCredentials = asTokenAuthCredentials(credentials);
        checkConfigurationValidity(testAutomationServer, tokenAuthCredentials.getToken());
        return getAllAccessibleEnvironmentsWithToken(
                testAutomationServer, tokenAuthCredentials.getToken());
    }

    private List<AutomatedExecutionEnvironment> getAllAccessibleEnvironmentsWithToken(
            TestAutomationServer testAutomationServer, String token) {
        try {
            SquashAutomWorkflowClient workflowClient =
                    workflowClientFactory.getClient(
                            testAutomationServer.getUrl(),
                            testAutomationServer.getObserverUrl(),
                            testAutomationServer.getKillswitchUrl(),
                            token,
                            requestTimeoutSeconds);
            return workflowClient.getAllAccessibleEnvironments(testAutomationServer);
        } catch (MessageRefusedException ex) {
            throw new TestAutomationException(ex.getMessage(), ex);
        }
    }

    @Override
    public boolean supportsAutomatedExecutionEnvironments() {
        return true;
    }

    @Override
    public OrchestratorResponse<OrchestratorConfVersions> getOrchestratorConfVersions(
            TestAutomationServer server) {
        return getOrchestratorConfVersionsWithToken(
                server, getAutomationServerCredentials(server).getToken());
    }

    @Override
    public OrchestratorResponse<List<WorkflowDto>> getProjectWorkflows(
            TestAutomationServer server, Long projectId, Credentials credentials) {
        String token =
                Objects.nonNull(credentials)
                        ? asTokenAuthCredentials(credentials).getToken()
                        : getAutomationServerCredentials(server).getToken();
        try {
            SquashAutomWorkflowClient workflowClient =
                    workflowClientFactory.getClient(
                            server.getUrl(),
                            server.getObserverUrl(),
                            server.getKillswitchUrl(),
                            token,
                            requestTimeoutSeconds);
            return workflowClient.fetchProjectWorkflows(server, projectId);
        } catch (MessageRefusedException ex) {
            throw new TestAutomationException(ex.getMessage(), ex);
        }
    }

    @Override
    public OrchestratorResponse<String> getWorkflowLogs(
            TestAutomationServer server, TokenAuthCredentials credentials, String workflowId) {
        String token =
                Objects.nonNull(credentials)
                        ? asTokenAuthCredentials(credentials).getToken()
                        : getAutomationServerCredentials(server).getToken();
        try {
            SquashAutomWorkflowClient workflowClient =
                    workflowClientFactory.getClient(
                            server.getUrl(),
                            server.getObserverUrl(),
                            server.getKillswitchUrl(),
                            token,
                            requestTimeoutSeconds);
            return workflowClient.fetchWorkflowLogs(workflowId);
        } catch (MessageRefusedException ex) {
            throw new TestAutomationException(ex.getMessage(), ex);
        }
    }

    @Override
    public OrchestratorResponse<Void> killWorkflow(
            TestAutomationServer server,
            Long projectId,
            TokenAuthCredentials credentials,
            String workflowId) {
        String token =
                Objects.nonNull(credentials)
                        ? asTokenAuthCredentials(credentials).getToken()
                        : getAutomationServerCredentials(server).getToken();
        try {
            SquashAutomWorkflowClient workflowClient =
                    workflowClientFactory.getClient(
                            server.getUrl(),
                            server.getObserverUrl(),
                            server.getKillswitchUrl(),
                            token,
                            requestTimeoutSeconds);
            return workflowClient.killWorkflow(server, projectId, workflowId);
        } catch (MessageRefusedException ex) {
            int httpErrorCode = ex.getHttpErrorCode();
            if (httpErrorCode == 404) {
                LOGGER.error(
                        "An error occurred while stopping the workflow. The workflow was not found and the error code is {}.",
                        httpErrorCode);
            } else {
                LOGGER.error(
                        "An error occurred while stopping the workflow. Unauthorized access and the error code is {}.",
                        httpErrorCode);
            }
            throw new TestAutomationException(ex.getMessage(), ex);
        }
    }

    private OrchestratorResponse<OrchestratorConfVersions> getOrchestratorConfVersionsWithToken(
            TestAutomationServer server, String token) {
        try {
            SquashAutomWorkflowClient workflowClient =
                    workflowClientFactory.getClient(
                            server.getUrl(),
                            server.getObserverUrl(),
                            server.getKillswitchUrl(),
                            token,
                            requestTimeoutSeconds);
            return workflowClient.fetchOrchestratorConfVersions(server);
        } catch (MessageRefusedException | UnexpectedServerResponseException ex) {
            throw new TestAutomationException(ex.getMessage(), ex);
        }
    }
}
