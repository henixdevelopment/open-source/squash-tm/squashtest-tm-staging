/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.jooq.domain.Tables.ACL_CLASS;
import static org.squashtest.tm.jooq.domain.Tables.ACL_GROUP;
import static org.squashtest.tm.jooq.domain.Tables.ACL_GROUP_PERMISSION;
import static org.squashtest.tm.jooq.domain.Tables.ACL_RESPONSIBILITY_SCOPE_ENTRY;

import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.jooq.DSLContext;
import org.jooq.Row2;
import org.jooq.Row3;
import org.jooq.impl.DSL;
import org.squashtest.tm.service.internal.display.dto.ActivePermissionRecord;
import org.squashtest.tm.service.internal.display.dto.ProfileActivePermissionsRecord;
import org.squashtest.tm.service.internal.repository.CustomProfileDao;

public class ProfileDaoImpl implements CustomProfileDao {

    private final DSLContext dslContext;

    public ProfileDaoImpl(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    @Override
    public Long findByQualifiedName(String qualifiedName) {
        return dslContext
                .select(ACL_GROUP.ID)
                .from(ACL_GROUP)
                .where(ACL_GROUP.QUALIFIED_NAME.eq(qualifiedName))
                .fetchOneInto(Long.class);
    }

    @Override
    public boolean isProfileUsed(long profileId) {
        return dslContext.fetchExists(
                dslContext
                        .selectDistinct(ACL_RESPONSIBILITY_SCOPE_ENTRY.ACL_GROUP_ID)
                        .from(ACL_RESPONSIBILITY_SCOPE_ENTRY)
                        .where(ACL_RESPONSIBILITY_SCOPE_ENTRY.ACL_GROUP_ID.eq(profileId)));
    }

    @Override
    public Set<String> findProfileNamesByPartyId(long teamId) {
        return dslContext
                .select(ACL_GROUP.QUALIFIED_NAME)
                .from(ACL_GROUP)
                .join(ACL_RESPONSIBILITY_SCOPE_ENTRY)
                .on(ACL_GROUP.ID.eq(ACL_RESPONSIBILITY_SCOPE_ENTRY.ACL_GROUP_ID))
                .where(ACL_RESPONSIBILITY_SCOPE_ENTRY.PARTY_ID.eq(teamId))
                .fetchSet(ACL_GROUP.QUALIFIED_NAME);
    }

    public Map<Long, List<String>> findProfileNamesByPartyIds(List<Long> teamIds) {
        return dslContext
                .selectDistinct(ACL_RESPONSIBILITY_SCOPE_ENTRY.PARTY_ID, ACL_GROUP.QUALIFIED_NAME)
                .from(ACL_GROUP)
                .join(ACL_RESPONSIBILITY_SCOPE_ENTRY)
                .on(ACL_GROUP.ID.eq(ACL_RESPONSIBILITY_SCOPE_ENTRY.ACL_GROUP_ID))
                .where(ACL_RESPONSIBILITY_SCOPE_ENTRY.PARTY_ID.in(teamIds))
                .fetchGroups(ACL_RESPONSIBILITY_SCOPE_ENTRY.PARTY_ID, ACL_GROUP.QUALIFIED_NAME);
    }

    @Override
    public void updatePermissions(long profileId, List<ProfileActivePermissionsRecord> records) {
        Map<String, Long> aclClassByNames =
                dslContext.selectFrom(ACL_CLASS).fetchMap(ACL_CLASS.CLASSNAME, ACL_CLASS.ID);

        List<Pair<String, Integer>> toDelete = getPermissionsToDelete(records);

        List<Triple<Long, String, Integer>> toInsert = getPermissionsToInsert(profileId, records);

        if (!toInsert.isEmpty()) {
            insertPermissions(toInsert, aclClassByNames);
        }

        if (!toDelete.isEmpty()) {
            deletePermissions(profileId, toDelete, aclClassByNames);
        }
    }

    private List<Pair<String, Integer>> getPermissionsToDelete(
            List<ProfileActivePermissionsRecord> records) {
        return records.stream()
                .flatMap(
                        record ->
                                record.permissions().stream()
                                        .filter(permissionRecord -> !permissionRecord.active())
                                        .map(
                                                permissionRecord ->
                                                        Pair.of(record.className(), permissionRecord.permission().getMask())))
                .toList();
    }

    private List<Triple<Long, String, Integer>> getPermissionsToInsert(
            long profileId, List<ProfileActivePermissionsRecord> records) {
        return records.stream()
                .flatMap(
                        record ->
                                record.permissions().stream()
                                        .filter(ActivePermissionRecord::active)
                                        .map(
                                                permissionRecord ->
                                                        Triple.of(
                                                                profileId,
                                                                record.className(),
                                                                permissionRecord.permission().getMask())))
                .toList();
    }

    private void insertPermissions(
            List<Triple<Long, String, Integer>> toInsert, Map<String, Long> aclClassByNames) {
        List<Row3<Long, Long, Integer>> valuesToInsert =
                toInsert.stream()
                        .map(
                                triple ->
                                        DSL.row(
                                                triple.getLeft(),
                                                aclClassByNames.get(triple.getMiddle()),
                                                triple.getRight()))
                        .toList();
        dslContext
                .insertInto(
                        ACL_GROUP_PERMISSION,
                        ACL_GROUP_PERMISSION.ACL_GROUP_ID,
                        ACL_GROUP_PERMISSION.CLASS_ID,
                        ACL_GROUP_PERMISSION.PERMISSION_MASK)
                .valuesOfRows(valuesToInsert)
                .onDuplicateKeyIgnore()
                .execute();
    }

    private void deletePermissions(
            long profileId, List<Pair<String, Integer>> toDelete, Map<String, Long> aclClassByNames) {
        List<Row2<Long, Integer>> valuesToDelete =
                toDelete.stream()
                        .map(pair -> DSL.row(aclClassByNames.get(pair.getLeft()), pair.getRight()))
                        .toList();
        dslContext
                .deleteFrom(ACL_GROUP_PERMISSION)
                .where(
                        ACL_GROUP_PERMISSION
                                .ACL_GROUP_ID
                                .eq(profileId)
                                .and(
                                        DSL.row(ACL_GROUP_PERMISSION.CLASS_ID, ACL_GROUP_PERMISSION.PERMISSION_MASK)
                                                .in(valuesToDelete)))
                .execute();
    }
}
