/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion.jdbc;

import static org.squashtest.tm.jooq.domain.Tables.ATTACHMENT_LIST;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_VALUE;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_SESSION_OVERVIEW;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT_REQUIREMENT_SYNC_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.TEST_PLAN;
import static org.squashtest.tm.jooq.domain.Tables.TEST_PLAN_ITEM;

import javax.persistence.EntityManager;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Record3;
import org.jooq.SelectConditionStep;
import org.jooq.Table;
import org.squashtest.tm.service.internal.attachment.AttachmentRepository;

public abstract class AbstractSprintReqVersionDeletionHandler
        extends AbstractExecutionDeletionHandler {
    protected AbstractSprintReqVersionDeletionHandler(
            DSLContext dslContext,
            EntityManager entityManager,
            AttachmentRepository attachmentRepository,
            JdbcBatchReorderHelper reorderHelper,
            String operationId) {
        super(dslContext, entityManager, attachmentRepository, reorderHelper, operationId);
    }

    protected abstract SelectConditionStep<Record3<Long, String, String>>
            selectSprintRequirementSyncExtenders();

    protected abstract Condition getSprintReqVersionPredicate();

    protected void addSprintReqVersions() {
        workingTables.addEntity(
                SPRINT_REQ_VERSION.SPRINT_REQ_VERSION_ID,
                () ->
                        makeSelectClause(SPRINT_REQ_VERSION.SPRINT_REQ_VERSION_ID)
                                .from(SPRINT_REQ_VERSION)
                                .where(getSprintReqVersionPredicate()));
    }

    protected void addSprintReqVersionsTestPlans() {
        workingTables.addEntity(
                TEST_PLAN.TEST_PLAN_ID,
                () ->
                        makeSelectClause(TEST_PLAN.TEST_PLAN_ID)
                                .from(SPRINT_REQ_VERSION)
                                .join(TEST_PLAN)
                                .on(TEST_PLAN.TEST_PLAN_ID.eq(SPRINT_REQ_VERSION.TEST_PLAN_ID))
                                .where(getSprintReqVersionPredicate()));
    }

    protected void addSprintReqVersionsTestPlanItems() {
        workingTables.addEntity(
                TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID,
                () ->
                        makeSelectClause(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID)
                                .from(SPRINT_REQ_VERSION)
                                .join(TEST_PLAN_ITEM)
                                .on(TEST_PLAN_ITEM.TEST_PLAN_ID.eq(SPRINT_REQ_VERSION.TEST_PLAN_ID))
                                .where(getSprintReqVersionPredicate()));
    }

    protected void addSprintRequirementSyncExtenders() {
        workingTables.addEntity(
                SPRINT_REQUIREMENT_SYNC_EXTENDER.SPRINT_REQ_SYNC_ID,
                this::selectSprintRequirementSyncExtenders);
    }

    protected void addCustomFieldValues() {
        workingTables.addEntity(CUSTOM_FIELD_VALUE.CFV_ID, this::selectExecutionCustomFieldValues);
    }

    protected void addAttachmentList() {
        workingTables.addEntity(
                ATTACHMENT_LIST.ATTACHMENT_LIST_ID, this::selectExecutionAttachmentLists);
    }

    protected void deleteSprintVersionSyncExtenders() {
        workingTables.delete(
                SPRINT_REQUIREMENT_SYNC_EXTENDER.SPRINT_REQ_SYNC_ID,
                SPRINT_REQUIREMENT_SYNC_EXTENDER.SPRINT_REQ_SYNC_ID);
        logDelete(SPRINT_REQUIREMENT_SYNC_EXTENDER);
    }

    protected void deleteSprintReqVersions() {
        workingTables.delete(
                SPRINT_REQ_VERSION.SPRINT_REQ_VERSION_ID, SPRINT_REQ_VERSION.SPRINT_REQ_VERSION_ID);
        logDelete(SPRINT_REQ_VERSION);
    }

    protected void deleteSprintReqVersionTestPlansItems() {
        workingTables.delete(
                TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID, EXPLORATORY_SESSION_OVERVIEW.TEST_PLAN_ITEM_ID);
        workingTables.delete(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID, TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID);
        logDelete(TEST_PLAN_ITEM);
    }

    protected void deleteSprintReqVersionTestPlans() {
        workingTables.delete(TEST_PLAN.TEST_PLAN_ID, TEST_PLAN.TEST_PLAN_ID);
        logDelete(TEST_PLAN);
    }

    @Override
    protected Table<Record> joinToExecution() {
        return SPRINT_REQ_VERSION
                .join(TEST_PLAN_ITEM)
                .on(TEST_PLAN_ITEM.TEST_PLAN_ID.eq(SPRINT_REQ_VERSION.TEST_PLAN_ID))
                .join(EXECUTION)
                .on(EXECUTION.TEST_PLAN_ITEM_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID));
    }
}
