/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.column.testcase;

import org.squashtest.tm.service.internal.batchimport.column.ColumnProcessingMode;
import org.squashtest.tm.service.internal.batchimport.column.TemplateColumn;
import org.squashtest.tm.service.internal.batchimport.column.TemplateWorksheet;

/**
 * Enumerates columns in the test case worksheet
 *
 * @author Gregory Fouquet
 */
public enum TestCaseSheetColumn implements TemplateColumn {
    ACTION,
    PROJECT_ID(ColumnProcessingMode.IGNORED),
    PROJECT_NAME(ColumnProcessingMode.IGNORED),
    TC_PATH(ColumnProcessingMode.MANDATORY),
    TC_NUM,
    TC_ID(ColumnProcessingMode.IGNORED),
    TC_UUID,
    TC_REFERENCE,
    TC_NAME,
    TC_MILESTONE,
    TC_WEIGHT_AUTO,
    TC_WEIGHT,
    TC_NATURE,
    TC_TYPE,
    TC_STATUS,
    TC_DESCRIPTION,
    TC_PRE_REQUISITE,
    TC_NB_REQ("TC_#_REQ", ColumnProcessingMode.IGNORED),
    TC_NB_CALLED_BY("TC_#_CALLED_BY", ColumnProcessingMode.IGNORED),
    TC_NB_ATTACHMENT("TC_#_ATTACHMENT", ColumnProcessingMode.IGNORED),
    TC_AUTOMATABLE,
    TC_CREATED_ON,
    TC_CREATED_BY,
    TC_LAST_MODIFIED_ON(ColumnProcessingMode.IGNORED),
    TC_LAST_MODIFIED_BY(ColumnProcessingMode.IGNORED),
    DRAFTED_BY_AI,
    TC_KIND(ColumnProcessingMode.IGNORED),
    TC_SCRIPT(ColumnProcessingMode.IGNORED),
    TC_NB_MILESTONES("#_MIL", ColumnProcessingMode.IGNORED),
    TC_NB_STEPS("#_TEST_STEPS", ColumnProcessingMode.IGNORED),
    TC_NB_ITERATION("#_ITERATIONS", ColumnProcessingMode.IGNORED);

    public final String header;
    ; // NOSONAR immutable public field
    public final ColumnProcessingMode processingMode;
    ; // NOSONAR immutable public field

    TestCaseSheetColumn() {
        this.header = name();
        processingMode = ColumnProcessingMode.OPTIONAL;
    }

    TestCaseSheetColumn(String header, ColumnProcessingMode processingMode) {
        this.header = header;
        this.processingMode = processingMode;
    }

    private TestCaseSheetColumn(ColumnProcessingMode processingMode) {
        this.header = name();
        this.processingMode = processingMode;
    }

    /**
     * @see TemplateColumn#getHeader()
     */
    @Override
    public String getHeader() {
        return header;
    }

    /**
     * @see TemplateColumn#getProcessingMode()
     */
    @Override
    public ColumnProcessingMode getProcessingMode() {
        return processingMode;
    }

    @Override
    public TemplateWorksheet getWorksheet() {
        return TemplateWorksheet.TEST_CASES_SHEET;
    }

    @Override
    public String getFullName() {
        return getWorksheet().sheetName + "." + header;
    }
}
