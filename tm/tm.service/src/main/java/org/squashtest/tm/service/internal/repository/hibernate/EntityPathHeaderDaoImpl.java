/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.jooq.impl.DSL.concat;
import static org.jooq.impl.DSL.groupConcat;
import static org.jooq.impl.DSL.value;
import static org.jooq.impl.DSL.when;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.CLN_RELATIONSHIP_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXPLORATORY_SESSION_OVERVIEW;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_FOLDER;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.RESOURCE;
import static org.squashtest.tm.jooq.domain.Tables.RLN_RELATIONSHIP_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.RLN_RESOURCE;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.TCLN_RELATIONSHIP_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_FOLDER;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE_TEST_PLAN_ITEM;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.inject.Inject;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record4;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.jooq.domain.tables.TclnRelationshipClosure;
import org.squashtest.tm.service.internal.repository.display.EntityPathHeaderDao;

@Repository
public class EntityPathHeaderDaoImpl implements EntityPathHeaderDao {

    private static final String PATH_SEPARATOR = " > ";
    private static final String PROJECT_NAME = "PROJECT_NAME";
    private static final String NODE_NAME = "NODE_NAME";
    private static final String DEPTH = "DEPTH";
    private static final String PATH_TABLE = "PATH_TABLE";
    private static final String PATH = "PATH";
    private static final String TCLN_ID = "TCLN_ID";
    private static final String PROJECT_ID = "PROJECT_ID";
    private static final String FULL_PATH = "PROJECT_ID";

    @Inject private DSLContext dslContext;

    public String buildRequirementLibraryNodePathHeader(Long nodeId) {
        Table<?> nodeTable =
                dslContext
                        .select(
                                PROJECT.NAME.as(PROJECT_NAME),
                                RESOURCE.NAME.as(NODE_NAME),
                                RLN_RELATIONSHIP_CLOSURE.DEPTH.as(DEPTH))
                        .from(REQUIREMENT_LIBRARY_NODE)
                        .leftJoin(PROJECT)
                        .on(PROJECT.PROJECT_ID.eq(REQUIREMENT_LIBRARY_NODE.PROJECT_ID))
                        .leftJoin(RLN_RELATIONSHIP_CLOSURE)
                        .on(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.eq(REQUIREMENT_LIBRARY_NODE.RLN_ID))
                        .leftJoin(REQUIREMENT_VERSION)
                        .on(REQUIREMENT_VERSION.REQUIREMENT_ID.eq(REQUIREMENT_LIBRARY_NODE.RLN_ID))
                        .leftJoin(REQUIREMENT_FOLDER)
                        .on(REQUIREMENT_FOLDER.RLN_ID.eq(REQUIREMENT_LIBRARY_NODE.RLN_ID))
                        .leftJoin(RESOURCE)
                        .on(
                                RESOURCE
                                        .RES_ID
                                        .eq(REQUIREMENT_FOLDER.RES_ID)
                                        .or(RESOURCE.RES_ID.eq(REQUIREMENT_VERSION.RES_ID)))
                        .where(
                                REQUIREMENT_LIBRARY_NODE
                                        .RLN_ID
                                        .eq(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID)
                                        .and(RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID.eq(nodeId))
                                        .and(REQUIREMENT_LIBRARY_NODE.RLN_ID.notEqual(nodeId)))
                        .asTable();

        return buildPathHeader(
                nodeId, nodeTable, REQUIREMENT_LIBRARY_NODE.RLN_ID, REQUIREMENT_LIBRARY_NODE.PROJECT_ID);
    }

    @Override
    public Map<Long, String> buildRequirementPathByIds(Collection<Long> requirementIds) {
        return dslContext
                .select(
                        REQUIREMENT.RLN_ID,
                        concat(
                                        PROJECT.NAME,
                                        value("/"),
                                        groupConcat(RESOURCE.NAME)
                                                .orderBy(RLN_RELATIONSHIP_CLOSURE.DEPTH.desc())
                                                .separator("/"))
                                .cast(SQLDataType.VARCHAR(10000))
                                .as(PATH))
                .from(PROJECT)
                .innerJoin(REQUIREMENT_LIBRARY_NODE)
                .on(PROJECT.PROJECT_ID.eq(REQUIREMENT_LIBRARY_NODE.PROJECT_ID))
                .innerJoin(REQUIREMENT)
                .on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(REQUIREMENT.RLN_ID))
                .innerJoin(RLN_RELATIONSHIP_CLOSURE)
                .on(REQUIREMENT.RLN_ID.eq(RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID))
                .innerJoin(RLN_RESOURCE)
                .on(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.eq(RLN_RESOURCE.RLN_ID))
                .innerJoin(RESOURCE)
                .on(RLN_RESOURCE.RES_ID.eq(RESOURCE.RES_ID))
                .where(REQUIREMENT.RLN_ID.in(requirementIds))
                .groupBy(PROJECT.NAME, REQUIREMENT.RLN_ID)
                .fetchMap(REQUIREMENT.RLN_ID, DSL.field(PATH, String.class));
    }

    public String buildTestCaseLibraryNodePathHeader(Long nodeId) {
        Table<?> nodeTable =
                dslContext
                        .select(
                                PROJECT.NAME.as(PROJECT_NAME),
                                TEST_CASE_LIBRARY_NODE.NAME.as(NODE_NAME),
                                TCLN_RELATIONSHIP_CLOSURE.DEPTH.as(DEPTH))
                        .from(TEST_CASE_LIBRARY_NODE)
                        .leftJoin(PROJECT)
                        .on(PROJECT.PROJECT_ID.eq(TEST_CASE_LIBRARY_NODE.PROJECT_ID))
                        .leftJoin(TCLN_RELATIONSHIP_CLOSURE)
                        .on(TCLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                        .where(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TCLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID))
                        .and(TCLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID.eq(nodeId))
                        .and(TEST_CASE_LIBRARY_NODE.TCLN_ID.notEqual(nodeId))
                        .asTable();

        return buildPathHeader(
                nodeId, nodeTable, TEST_CASE_LIBRARY_NODE.TCLN_ID, TEST_CASE_LIBRARY_NODE.PROJECT_ID);
    }

    public String buildCampaignLibraryNodePathHeader(Long nodeId) {
        Table<?> nodeTable =
                dslContext
                        .select(
                                PROJECT.NAME.as(PROJECT_NAME),
                                CAMPAIGN_LIBRARY_NODE.NAME.as(NODE_NAME),
                                CLN_RELATIONSHIP_CLOSURE.DEPTH.as(DEPTH))
                        .from(CAMPAIGN_LIBRARY_NODE)
                        .leftJoin(PROJECT)
                        .on(PROJECT.PROJECT_ID.eq(CAMPAIGN_LIBRARY_NODE.PROJECT_ID))
                        .leftJoin(CLN_RELATIONSHIP_CLOSURE)
                        .on(CLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
                        .where(
                                CAMPAIGN_LIBRARY_NODE
                                        .CLN_ID
                                        .eq(CLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID)
                                        .and(CLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID.eq(nodeId))
                                        .and(CAMPAIGN_LIBRARY_NODE.CLN_ID.notEqual(nodeId)))
                        .asTable();

        return buildPathHeader(
                nodeId, nodeTable, CAMPAIGN_LIBRARY_NODE.CLN_ID, CAMPAIGN_LIBRARY_NODE.PROJECT_ID);
    }

    public String buildIterationPathHeader(Long iterationId) {
        Long campaignId =
                dslContext
                        .select(CAMPAIGN_ITERATION.CAMPAIGN_ID)
                        .from(CAMPAIGN_ITERATION)
                        .where(CAMPAIGN_ITERATION.ITERATION_ID.eq(iterationId))
                        .fetchOneInto(Long.class);

        return dslContext
                .select(
                        concatPathFromParent(
                                buildCampaignLibraryNodePathHeader(campaignId), CAMPAIGN_LIBRARY_NODE.NAME))
                .from(CAMPAIGN_LIBRARY_NODE)
                .where(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(campaignId))
                .fetchOneInto(String.class);
    }

    public String buildTestSuitePathHeader(Long testSuiteId) {
        Long iterationId =
                dslContext
                        .select(ITERATION_TEST_SUITE.ITERATION_ID)
                        .from(ITERATION_TEST_SUITE)
                        .where(ITERATION_TEST_SUITE.TEST_SUITE_ID.eq(testSuiteId))
                        .fetchOneInto(Long.class);

        return buildIterationChildPath(iterationId);
    }

    public String buildExecutionPathHeader(Long executionId) {

        if (executionIsInsideSprint(executionId)) {
            Long sprintId = getSprintIdForExecutionId(executionId);
            return buildSprintChildPathHeader(sprintId);
        }

        List<Long> testSuiteParentIds = getTestSuiteParentIdsForExecution(executionId);

        if (testSuiteParentIds.size() == 1) {
            return buildTestSuiteChildPathHeader(testSuiteParentIds);
        }

        Long iterationId = getIterationIdForExecutionId(executionId);

        return buildIterationChildPath(iterationId);
    }

    private boolean executionIsInsideSprint(Long executionId) {
        Long testPlanItemId =
                dslContext
                        .select(EXECUTION.TEST_PLAN_ITEM_ID)
                        .from(EXECUTION)
                        .where(EXECUTION.EXECUTION_ID.eq(executionId))
                        .fetchOneInto(Long.class);

        return Objects.nonNull(testPlanItemId);
    }

    private Long getSprintIdForExecutionId(Long executionId) {
        return dslContext
                .select(SPRINT.CLN_ID)
                .from(EXECUTION)
                .innerJoin(TEST_PLAN_ITEM)
                .on(EXECUTION.TEST_PLAN_ITEM_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID))
                .innerJoin(SPRINT_REQ_VERSION)
                .on(SPRINT_REQ_VERSION.TEST_PLAN_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ID))
                .innerJoin(SPRINT)
                .on(SPRINT.CLN_ID.eq(SPRINT_REQ_VERSION.SPRINT_ID))
                .where(EXECUTION.EXECUTION_ID.eq(executionId))
                .fetchOneInto(Long.class);
    }

    private List<Long> getTestSuiteParentIdsForExecution(Long executionId) {
        return dslContext
                .select(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID)
                .from(ITEM_TEST_PLAN_EXECUTION)
                .innerJoin(TEST_SUITE_TEST_PLAN_ITEM)
                .on(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID.eq(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID))
                .where(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID.eq(executionId))
                .fetchInto(Long.class);
    }

    private Long getIterationIdForExecutionId(Long executionId) {
        return dslContext
                .select(ITEM_TEST_PLAN_LIST.ITERATION_ID)
                .from(ITEM_TEST_PLAN_LIST)
                .innerJoin(ITEM_TEST_PLAN_EXECUTION)
                .on(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                .where(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID.eq(executionId))
                .fetchOneInto(Long.class);
    }

    private String buildTestSuiteChildPathHeader(List<Long> testSuiteParentIds) {
        return dslContext
                .select(
                        concatPathFromParent(
                                buildTestSuitePathHeader(testSuiteParentIds.get(0)), TEST_SUITE.NAME))
                .from(TEST_SUITE)
                .where(TEST_SUITE.ID.eq(testSuiteParentIds.get(0)))
                .fetchOneInto(String.class);
    }

    @Override
    public String buildExploratorySessionOverviewPathHeader(Long sessionOverviewId) {
        Long iterationId =
                dslContext
                        .select(ITEM_TEST_PLAN_LIST.ITERATION_ID)
                        .from(ITEM_TEST_PLAN_LIST)
                        .join(EXPLORATORY_SESSION_OVERVIEW)
                        .on(
                                EXPLORATORY_SESSION_OVERVIEW.ITEM_TEST_PLAN_ID.eq(
                                        ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
                        .where(EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.eq(sessionOverviewId))
                        .fetchOneInto(Long.class);

        if (iterationId != null) {
            return buildIterationChildPath(iterationId);
        } else {
            Long clnId =
                    dslContext
                            .select(SPRINT_REQ_VERSION.SPRINT_ID)
                            .from(EXPLORATORY_SESSION_OVERVIEW)
                            .join(TEST_PLAN_ITEM)
                            .on(
                                    EXPLORATORY_SESSION_OVERVIEW.TEST_PLAN_ITEM_ID.eq(
                                            TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID))
                            .join(SPRINT_REQ_VERSION)
                            .on(SPRINT_REQ_VERSION.TEST_PLAN_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ID))
                            .where(EXPLORATORY_SESSION_OVERVIEW.OVERVIEW_ID.eq(sessionOverviewId))
                            .fetchOneInto(Long.class);
            return buildSprintChildPathHeader(clnId);
        }
    }

    private String buildSprintChildPathHeader(Long clnId) {
        return dslContext
                .select(
                        concatPathFromParent(
                                buildCampaignLibraryNodePathHeader(clnId), CAMPAIGN_LIBRARY_NODE.NAME))
                .from(CAMPAIGN_LIBRARY_NODE)
                .where(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(clnId))
                .fetchOneInto(String.class);
    }

    private Field<String> concatPathFromParent(String parentPath, TableField<?, String> parentField) {
        return concat(DSL.value(parentPath), DSL.value(PATH_SEPARATOR), parentField);
    }

    private String buildIterationChildPath(Long iterationId) {
        return dslContext
                .select(concatPathFromParent(buildIterationPathHeader(iterationId), ITERATION.NAME))
                .from(ITERATION)
                .where(ITERATION.ITERATION_ID.eq(iterationId))
                .fetchOneInto(String.class);
    }

    private String buildPathHeader(
            Long nodeId,
            Table<?> nodeTable,
            TableField<?, Long> entityFieldId,
            TableField<?, Long> entityFieldProjectId) {
        String path = buildPathFromNodeTable(nodeTable);

        if (Objects.nonNull(path)) {
            return path;
        }

        return getDefaultProjectPath(entityFieldId, entityFieldProjectId, nodeId);
    }

    private String buildPathFromNodeTable(Table<?> nodeTable) {
        return dslContext
                .select(
                        concat(
                                nodeTable.field(PROJECT_NAME),
                                DSL.value(PATH_SEPARATOR),
                                DSL.groupConcat(nodeTable.field(NODE_NAME))
                                        .orderBy(nodeTable.field(DEPTH).desc())
                                        .separator(PATH_SEPARATOR)))
                .from(nodeTable)
                .groupBy(nodeTable.field(PROJECT_NAME))
                .fetchOneInto(String.class);
    }

    private String getDefaultProjectPath(
            TableField<?, Long> entityFieldId, TableField<?, Long> entityFieldProjectId, Long entityId) {
        return dslContext
                .select(PROJECT.NAME)
                .from(entityFieldId.getTable())
                .leftJoin(PROJECT)
                .on(PROJECT.PROJECT_ID.eq(entityFieldProjectId))
                .where(entityFieldId.eq(entityId))
                .fetchOneInto(String.class);
    }

    @Override
    public Map<Long, String> buildTestCasePathByIds(Collection<Long> testCaseIds) {
        return buildTestCasePathByIds(testCaseIds, PATH_SEPARATOR);
    }

    @Override
    public Map<Long, String> buildTestCasePathByIds(Collection<Long> testCaseIds, String separator) {
        short zero = 0;

        TclnRelationshipClosure descendant = TCLN_RELATIONSHIP_CLOSURE.as("descendant");

        Table<Record4<Long, String, Long, String>> path =
                DSL.select(
                                PROJECT.PROJECT_ID,
                                PROJECT.NAME,
                                TCLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID,
                                DSL.groupConcat(TEST_CASE_LIBRARY_NODE.NAME)
                                        .orderBy(TCLN_RELATIONSHIP_CLOSURE.DEPTH.desc())
                                        .separator(separator)
                                        .as(PATH))
                        .from(TCLN_RELATIONSHIP_CLOSURE)
                        .innerJoin(TEST_CASE_LIBRARY_NODE)
                        .on(TCLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                        .innerJoin(PROJECT)
                        .on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                        .innerJoin(descendant)
                        .on(TCLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID.eq(descendant.ANCESTOR_ID))
                        .leftJoin(TEST_CASE_FOLDER)
                        .on(
                                descendant
                                        .DESCENDANT_ID
                                        .eq(TEST_CASE_FOLDER.TCLN_ID)
                                        .and(TCLN_RELATIONSHIP_CLOSURE.DEPTH.ne(zero)))
                        .where(TEST_CASE_FOLDER.TCLN_ID.isNull())
                        .groupBy(PROJECT.PROJECT_ID, TCLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID)
                        .asTable(PATH_TABLE, PROJECT_ID, PROJECT_NAME, TCLN_ID, PATH);

        return dslContext
                .select(
                        path.field(TCLN_ID),
                        DSL.concat(
                                        path.field(PROJECT_NAME),
                                        DSL.value(separator),
                                        path.field(PATH),
                                        when(TEST_CASE_FOLDER.TCLN_ID.isNotNull(), DSL.value(separator)).otherwise(""))
                                .as(FULL_PATH))
                .from(path)
                .leftJoin(TEST_CASE_FOLDER)
                .on(path.field(TCLN_ID, Long.class).eq(TEST_CASE_FOLDER.TCLN_ID))
                .where(path.field(TCLN_ID, Long.class).in(testCaseIds))
                .fetchMap(DSL.field(TCLN_ID, Long.class), DSL.field(FULL_PATH, String.class));
    }
}
