/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository;

import java.util.List;
import org.squashtest.tm.api.plugin.UsedInPlugin;
import org.squashtest.tm.domain.campaign.SprintGroup;
import org.squashtest.tm.domain.library.LibraryNode;
import org.squashtest.tm.domain.library.NodeContainer;

public interface SprintGroupDao extends EntityDao<SprintGroup> {
    void setRemoteSyncIdsToNull(List<Long> syncIds);

    @UsedInPlugin("Xsquash4Jira & Xsquash4GitLab")
    void updateRemoteSyncId(Long clnId, Long syncId);

    Long findRootNodeIdByProjectIdAndName(Long projectId, String rootFolderName);

    @UsedInPlugin("Xsquash4Jira & Xsquash4GitLab")
    Long findIdByProjectIdAndRemoteSynchronisationId(Long projectId, Long remoteSynchronisationId);

    @UsedInPlugin("Xsquash4jira & Xsquash4GitLab")
    List<Long> findIdsByClnIds(List<Long> ids);

    <LN extends LibraryNode> NodeContainer<LN> findByContent(LN node);
}
