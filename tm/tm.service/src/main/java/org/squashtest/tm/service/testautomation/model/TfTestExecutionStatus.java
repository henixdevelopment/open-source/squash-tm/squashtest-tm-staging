/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.squashtest.tm.api.testautomation.execution.dto.ExecutionStatus;
import org.squashtest.tm.api.testautomation.execution.dto.TestExecutionStatus;

public class TfTestExecutionStatus implements Serializable {
    private static final long serialVersionUID = 7818656596437427978L;
    @NotNull private Date startTime;
    private Date endTime;
    @NotNull private ExecutionStatus status;
    private Integer duration;
    private String resultUrl = null;

    private List<String> failureDetails;

    public TfTestExecutionStatus() {
        // for test purposes
    }

    public TfTestExecutionStatus(
            Date startTime,
            Date endTime,
            ExecutionStatus status,
            Integer duration,
            String resultUrl,
            List<String> failureDetails) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.status = status;
        this.duration = duration;
        this.resultUrl = resultUrl;
        this.failureDetails = failureDetails;
    }

    public Date getStartTime() {
        return this.startTime;
    }

    public void setStartTime(Date startTime) {
        if (startTime == null) {
            throw new IllegalArgumentException("start time cannot be null.");
        } else {
            this.startTime = startTime;
        }
    }

    public Date getEndTime() {
        return this.endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public ExecutionStatus getStatus() {
        return this.status;
    }

    public void setStatus(ExecutionStatus status) {
        if (status == null) {
            throw new IllegalArgumentException("status cannot be null.");
        } else {
            this.status = status;
        }
    }

    public Integer getDuration() {
        return this.duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public void setResultUrl(String resultUrl) {
        this.resultUrl = resultUrl;
    }

    public String getResultUrl() {
        return this.resultUrl;
    }

    public List<String> getFailureDetails() {
        return failureDetails;
    }

    public TestExecutionStatus tfTestExecutionStatusToTmTestExecutionStatus() {
        TestExecutionStatus testExecutionStatus = new TestExecutionStatus();
        testExecutionStatus.setTestName("");
        testExecutionStatus.setTestGroupName("");
        testExecutionStatus.setStartTime(startTime);
        testExecutionStatus.setEndTime(endTime);
        testExecutionStatus.setStatus(status);
        testExecutionStatus.setDuration(duration);
        testExecutionStatus.setFailureDetails(failureDetails);
        testExecutionStatus.setStatusMessage("");
        testExecutionStatus.setResultUrl(null);
        return testExecutionStatus;
    }
}
