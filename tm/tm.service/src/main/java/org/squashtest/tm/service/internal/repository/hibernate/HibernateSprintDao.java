/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.jooq.domain.tables.Sprint.SPRINT;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_DISTINCT_SPRINT_ATTACHMENT_PROJECT_BY_IDS;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_DISTINCT_SPRINT_WITH_SRV_BY_IDS;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_DISTINCT_TEST_PLAN_OF_SRV_ATTACHMENT_BY_IDS;
import static org.squashtest.tm.service.internal.repository.ParameterNames.IDS;
import static org.squashtest.tm.service.internal.repository.ParameterNames.SPRINTS;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.hibernate.annotations.QueryHints;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.api.plugin.UsedInPlugin;
import org.squashtest.tm.domain.campaign.Sprint;
import org.squashtest.tm.domain.campaign.testplan.TestPlan;
import org.squashtest.tm.service.internal.repository.SprintDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;
import org.squashtest.tm.service.internal.repository.hibernate.loaders.EntityGraphQueryBuilder;

@Repository
public class HibernateSprintDao extends HibernateEntityDao<Sprint> implements SprintDao {

    private final DSLContext dslContext;

    public HibernateSprintDao(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    @Override
    public List<Sprint> findByRemoteSynchronisationIdAndRemoteSprintIds(
            Long remoteSynchronisationId, List<Long> remoteSprintIds) {
        return entityManager
                .createNamedQuery("Sprint.findByRemoteSynchronisationIdAndRemoteSprintIds", Sprint.class)
                .setParameter("remoteSynchronisationId", remoteSynchronisationId)
                .setParameter("remoteSprintIds", remoteSprintIds)
                .getResultList();
    }

    @Override
    public List<Long> findSquashSprintIdsHavingDeletedRemoteSprint(
            Long remoteSynchronisationId, List<Long> existingRemoteSprintIds) {
        return dslContext
                .select(SPRINT.CLN_ID.as(RequestAliasesConstants.ID))
                .from(SPRINT)
                .where(SPRINT.REMOTE_SYNCHRONISATION_ID.eq(remoteSynchronisationId))
                .and(SPRINT.REMOTE_SPRINT_ID.notIn(existingRemoteSprintIds))
                .fetchInto(Long.class);
    }

    @Override
    public void deleteRemoteSynchronisationFromSprint(List<Long> remoteSyncIds) {
        entityManager
                .createNamedQuery("Sprint.deleteRemoteSynchronisationFromSprint")
                .setParameter("remoteSyncIds", remoteSyncIds)
                .executeUpdate();
    }

    @Override
    public void deleteRemoteSynchronisationFromSprintId(long sprintId) {
        entityManager
                .createNamedQuery("Sprint.deleteRemoteSynchronisationFromSprintId")
                .setParameter("sprintId", sprintId)
                .executeUpdate();
    }

    @Override
    @UsedInPlugin("Xsquash4Jira & Xsquash4GitLab")
    public void updateRemoteStateOfSynchronisedSprintsByIds(List<Long> sprintIds, String status) {
        entityManager
                .createNamedQuery("Sprint.updateRemoteStateOfSynchronisedSprintsByIds")
                .setParameter("status", status)
                .setParameter("sprintIds", sprintIds)
                .executeUpdate();
    }

    @Override
    public List<Sprint> loadNodeForPaste(Collection<Long> ids) {
        List<Sprint> sprints =
                entityManager
                        .createQuery(FIND_DISTINCT_SPRINT_ATTACHMENT_PROJECT_BY_IDS, Sprint.class)
                        .setParameter(IDS, ids)
                        .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
                        .getResultList();
        // Avoid RequirementVersion EAGER loading with EntityGraph
        // Separate query from Sprint query because of duplicate rows issue between AttachmentList and
        // SprintReqVersion
        new EntityGraphQueryBuilder<>(entityManager, Sprint.class, FIND_DISTINCT_SPRINT_WITH_SRV_BY_IDS)
                .executeDistinctList(Map.of(SPRINTS, sprints));
        // Separate query from Sprint query because cartesian product issue between Sprint and TestPlan
        // - TestPlanItem
        entityManager
                .createQuery(FIND_DISTINCT_TEST_PLAN_OF_SRV_ATTACHMENT_BY_IDS, TestPlan.class)
                .setParameter(SPRINTS, sprints)
                .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
                .getResultList();
        return sprints;
    }
}
