/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.testcase;

import org.squashtest.tm.core.foundation.annotation.CleanHtml;

public abstract class ActionWordFragmentValueDto {

    public static final String TEXT = "TEXT";
    public static final String PARAMETER = "PARAMETER";

    private Long keywordStepId;
    protected String value;

    protected ActionWordFragmentValueDto(Long keywordStepId, String value) {
        this.keywordStepId = keywordStepId;
        this.value = value;
    }

    public abstract String getUnstyledAction();

    @CleanHtml
    public abstract String getStyledAction();

    public Long getKeywordStepId() {
        return keywordStepId;
    }

    public void setKeywordStepId(Long keywordStepId) {
        this.keywordStepId = keywordStepId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
