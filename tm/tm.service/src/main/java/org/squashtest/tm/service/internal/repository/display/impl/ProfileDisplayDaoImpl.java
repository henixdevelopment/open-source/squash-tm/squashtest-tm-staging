/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import static org.jooq.impl.DSL.concat;
import static org.jooq.impl.DSL.val;
import static org.jooq.impl.DSL.when;
import static org.jooq.tools.StringUtils.EMPTY;
import static org.squashtest.tm.jooq.domain.Tables.ACL_CLASS;
import static org.squashtest.tm.jooq.domain.Tables.ACL_GROUP;
import static org.squashtest.tm.jooq.domain.Tables.ACL_GROUP_PERMISSION;
import static org.squashtest.tm.jooq.domain.Tables.ACL_OBJECT_IDENTITY;
import static org.squashtest.tm.jooq.domain.Tables.ACL_RESPONSIBILITY_SCOPE_ENTRY;
import static org.squashtest.tm.jooq.domain.Tables.CORE_TEAM;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ACTIVE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CREATED_BY;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CREATED_ON;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.DESCRIPTION;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LAST_MODIFIED_BY;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LAST_MODIFIED_ON;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PARTY_COUNT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PARTY_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PARTY_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.QUALIFIED_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEAM;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record5;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.domain.acl.AclClass;
import org.squashtest.tm.domain.acl.AclGroup;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.project.ProjectTemplate;
import org.squashtest.tm.service.internal.display.dto.PartyProfileAuthorizationsDto;
import org.squashtest.tm.service.internal.display.dto.PermissionsDto;
import org.squashtest.tm.service.internal.display.dto.ProfileAdminViewDto;
import org.squashtest.tm.service.internal.display.dto.ProfileDto;
import org.squashtest.tm.service.internal.display.dto.ProfilePermissionsDto;
import org.squashtest.tm.service.internal.repository.display.ProfileDisplayDao;

@Repository
public class ProfileDisplayDaoImpl implements ProfileDisplayDao {
    private static final String SPACE_CHAR = " ";
    private static final String PARTY_NAME_LOGIN_START = " (";
    private static final String PARTY_NAME_LOGIN_END = ")";
    private final DSLContext dslContext;

    public ProfileDisplayDaoImpl(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    @Override
    public List<ProfileDto> findAll() {
        return dslContext
                .select(
                        ACL_GROUP.ID.as(ID),
                        ACL_GROUP.QUALIFIED_NAME.as(QUALIFIED_NAME),
                        DSL.countDistinct(ACL_RESPONSIBILITY_SCOPE_ENTRY.PARTY_ID).as(PARTY_COUNT),
                        ACL_GROUP.ACTIVE.as(ACTIVE))
                .from(ACL_GROUP)
                .leftJoin(ACL_RESPONSIBILITY_SCOPE_ENTRY)
                .on(ACL_GROUP.ID.eq(ACL_RESPONSIBILITY_SCOPE_ENTRY.ACL_GROUP_ID))
                .groupBy(ACL_GROUP.ID)
                .fetchInto(ProfileDto.class);
    }

    @Override
    public ProfileAdminViewDto findProfileAdminViewDto(long profileId) {
        return dslContext
                .select(
                        ACL_GROUP.ID.as(ID),
                        ACL_GROUP.QUALIFIED_NAME.as(QUALIFIED_NAME),
                        DSL.countDistinct(ACL_RESPONSIBILITY_SCOPE_ENTRY.PARTY_ID).as(PARTY_COUNT),
                        ACL_GROUP.DESCRIPTION.as(DESCRIPTION),
                        ACL_GROUP.ACTIVE.as(ACTIVE),
                        ACL_GROUP.CREATED_BY.as(CREATED_BY),
                        ACL_GROUP.CREATED_ON.as(CREATED_ON),
                        ACL_GROUP.LAST_MODIFIED_BY.as(LAST_MODIFIED_BY),
                        ACL_GROUP.LAST_MODIFIED_ON.as(LAST_MODIFIED_ON))
                .from(ACL_GROUP)
                .leftJoin(ACL_RESPONSIBILITY_SCOPE_ENTRY)
                .on(ACL_GROUP.ID.eq(ACL_RESPONSIBILITY_SCOPE_ENTRY.ACL_GROUP_ID))
                .where(ACL_GROUP.ID.eq(profileId))
                .groupBy(ACL_GROUP.ID)
                .fetchOneInto(ProfileAdminViewDto.class);
    }

    @Override
    public List<PermissionsDto> fetchProfilePermissions(long profileId) {
        List<PermissionsDto> profilePermissionsDtos = new ArrayList<>();
        List<AclClass> aclClasses = getFilteredAclClasses();

        Map<String, List<Integer>> permissions = fetchAclGroupPermissions(profileId);

        for (AclClass aclClass : aclClasses) {
            List<String> perms = retrievePermissionsForAclClass(aclClass, permissions);

            profilePermissionsDtos.add(
                    new PermissionsDto(aclClass.getClassName(), aclClass.getSimpleClassName(), perms));
        }

        return profilePermissionsDtos;
    }

    private List<AclClass> getFilteredAclClasses() {
        return Arrays.stream(AclClass.values())
                .filter(aclClass -> aclClass != AclClass.PROJECT_TEMPLATE)
                .toList();
    }

    private Map<String, List<Integer>> fetchAclGroupPermissions(long profileId) {
        return dslContext
                .select(ACL_CLASS.CLASSNAME, ACL_GROUP_PERMISSION.PERMISSION_MASK)
                .from(ACL_GROUP_PERMISSION)
                .join(ACL_CLASS)
                .on(ACL_GROUP_PERMISSION.CLASS_ID.eq(ACL_CLASS.ID))
                .where(ACL_GROUP_PERMISSION.ACL_GROUP_ID.eq(profileId))
                .fetchGroups(ACL_CLASS.CLASSNAME, ACL_GROUP_PERMISSION.PERMISSION_MASK);
    }

    private List<String> retrievePermissionsForAclClass(
            AclClass aclClass, Map<String, List<Integer>> permissions) {
        return Permissions.findByMasks(permissions.getOrDefault(aclClass.getClassName(), List.of()))
                .stream()
                .map(Enum::name)
                .toList();
    }

    @Override
    public List<ProfilePermissionsDto> fetchProfilesAndPermissions() {
        List<ProfilePermissionsDto> profilePermissionsDtos = new ArrayList<>();

        Map<String, AclClass> aclClasses =
                Arrays.stream(AclClass.values())
                        .collect(Collectors.toMap(AclClass::getClassName, Function.identity()));

        dslContext
                .select(
                        ACL_GROUP_PERMISSION.ACL_GROUP_ID,
                        ACL_GROUP.QUALIFIED_NAME,
                        ACL_GROUP.ACTIVE,
                        ACL_CLASS.CLASSNAME,
                        ACL_GROUP_PERMISSION.PERMISSION_MASK)
                .from(ACL_GROUP_PERMISSION)
                .join(ACL_CLASS)
                .on(ACL_GROUP_PERMISSION.CLASS_ID.eq(ACL_CLASS.ID))
                .join(ACL_GROUP)
                .on(ACL_GROUP_PERMISSION.ACL_GROUP_ID.eq(ACL_GROUP.ID))
                .forEach(
                        profilePermission ->
                                processProfilePermission(profilePermission, profilePermissionsDtos, aclClasses));

        return profilePermissionsDtos;
    }

    @Override
    public List<PartyProfileAuthorizationsDto> fetchPartyProfileAuthorizations(long profileId) {
        Field<String> partyName = getPartyNameField();
        Field<Boolean> isPartyActive = when(CORE_USER.LOGIN.isNull(), true).otherwise(CORE_USER.ACTIVE);
        Field<Boolean> isPartyTeam = when(CORE_USER.LOGIN.isNull(), true).otherwise(false);

        return dslContext
                .selectDistinct(
                        ACL_RESPONSIBILITY_SCOPE_ENTRY.PARTY_ID.as(PARTY_ID),
                        partyName.as(PARTY_NAME),
                        isPartyActive.as(ACTIVE),
                        isPartyTeam.as(TEAM))
                .from(ACL_GROUP)
                .join(ACL_RESPONSIBILITY_SCOPE_ENTRY)
                .on(ACL_GROUP.ID.eq(ACL_RESPONSIBILITY_SCOPE_ENTRY.ACL_GROUP_ID))
                .join(ACL_OBJECT_IDENTITY)
                .on(ACL_RESPONSIBILITY_SCOPE_ENTRY.OBJECT_IDENTITY_ID.eq(ACL_OBJECT_IDENTITY.ID))
                .join(ACL_CLASS)
                .on(ACL_OBJECT_IDENTITY.CLASS_ID.eq(ACL_CLASS.ID))
                .leftJoin(CORE_USER)
                .on(ACL_RESPONSIBILITY_SCOPE_ENTRY.PARTY_ID.eq(CORE_USER.PARTY_ID))
                .leftJoin(CORE_TEAM)
                .on(ACL_RESPONSIBILITY_SCOPE_ENTRY.PARTY_ID.eq(CORE_TEAM.PARTY_ID))
                .where(ACL_GROUP.ID.eq(profileId))
                .and(ACL_CLASS.CLASSNAME.in(Project.class.getName(), ProjectTemplate.class.getName()))
                .fetchInto(PartyProfileAuthorizationsDto.class);
    }

    private Field<String> getPartyNameField() {
        return when(CORE_USER.LOGIN.isNull(), CORE_TEAM.NAME)
                .otherwise(
                        when(
                                        CORE_USER.FIRST_NAME.isNull().or(CORE_USER.FIRST_NAME.eq(EMPTY)),
                                        concat(
                                                CORE_USER.LAST_NAME,
                                                val(PARTY_NAME_LOGIN_START),
                                                CORE_USER.LOGIN,
                                                val(PARTY_NAME_LOGIN_END)))
                                .otherwise(
                                        concat(
                                                CORE_USER.FIRST_NAME,
                                                val(SPACE_CHAR),
                                                CORE_USER.LAST_NAME,
                                                val(PARTY_NAME_LOGIN_START),
                                                CORE_USER.LOGIN,
                                                val(PARTY_NAME_LOGIN_END))));
    }

    private void processProfilePermission(
            Record5<Long, String, Boolean, String, Integer> profilePermission,
            List<ProfilePermissionsDto> profilePermissionsDtos,
            Map<String, AclClass> aclClasses) {
        ProfilePermissionsDto profilePermissionsDto =
                findOrCreateProfilePermissionsDto(profilePermission, profilePermissionsDtos);
        PermissionsDto permissionsDto =
                findOrCreatePermissionsDto(profilePermissionsDto, profilePermission, aclClasses);
        Permissions permissionMask =
                Permissions.findByMask(profilePermission.get(ACL_GROUP_PERMISSION.PERMISSION_MASK));
        if (Objects.nonNull(permissionMask)) {
            permissionsDto.permissions().add(permissionMask.name());
        }
    }

    private ProfilePermissionsDto findOrCreateProfilePermissionsDto(
            Record5<Long, String, Boolean, String, Integer> profilePermission,
            List<ProfilePermissionsDto> profilePermissionsDtos) {
        return profilePermissionsDtos.stream()
                .filter(it -> it.profileId() == profilePermission.get(ACL_GROUP_PERMISSION.ACL_GROUP_ID))
                .findFirst()
                .orElseGet(
                        () -> {
                            ProfilePermissionsDto newDto =
                                    new ProfilePermissionsDto(
                                            profilePermission.get(ACL_GROUP_PERMISSION.ACL_GROUP_ID),
                                            profilePermission.get(ACL_GROUP.QUALIFIED_NAME),
                                            profilePermission.get(ACL_GROUP.ACTIVE),
                                            AclGroup.isSystem(profilePermission.get(ACL_GROUP.QUALIFIED_NAME)),
                                            new ArrayList<>());
                            profilePermissionsDtos.add(newDto);
                            return newDto;
                        });
    }

    private PermissionsDto findOrCreatePermissionsDto(
            ProfilePermissionsDto profilePermissionsDto,
            Record5<Long, String, Boolean, String, Integer> profilePermission,
            Map<String, AclClass> aclClasses) {
        return profilePermissionsDto.permissions().stream()
                .filter(it -> it.className().equals(profilePermission.get(ACL_CLASS.CLASSNAME)))
                .findFirst()
                .orElseGet(
                        () -> {
                            PermissionsDto newPermissionsDto =
                                    new PermissionsDto(
                                            profilePermission.get(ACL_CLASS.CLASSNAME),
                                            aclClasses
                                                    .get(profilePermission.get(ACL_CLASS.CLASSNAME))
                                                    .getSimpleClassName(),
                                            new ArrayList<>());
                            profilePermissionsDto.permissions().add(newPermissionsDto);
                            return newPermissionsDto;
                        });
    }
}
