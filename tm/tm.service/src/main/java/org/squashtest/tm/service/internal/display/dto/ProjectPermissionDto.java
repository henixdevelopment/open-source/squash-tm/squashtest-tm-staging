/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import org.squashtest.tm.domain.acl.AclGroup;
import org.squashtest.tm.domain.project.ProjectPermission;

/**
 * Represents a user or team's permissions on projects. This class is typically used to list the
 * permissions for a given user/team so the party is implied. See {@link PartyProjectPermissionDto}
 * for a representation that includes the bound party.
 */
public class ProjectPermissionDto {

    Long projectId;

    String projectName;

    AclGroup permissionGroup;

    public static ProjectPermissionDto from(ProjectPermission projectPermission) {
        ProjectPermissionDto dto = new ProjectPermissionDto();
        dto.setPermissionGroup(projectPermission.getPermissionGroup());
        dto.setProjectId(projectPermission.getProject().getId());
        dto.setProjectName(projectPermission.getProject().getName());
        return dto;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public AclGroup getPermissionGroup() {
        return permissionGroup;
    }

    public void setPermissionGroup(AclGroup permissionGroup) {
        this.permissionGroup = permissionGroup;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}
