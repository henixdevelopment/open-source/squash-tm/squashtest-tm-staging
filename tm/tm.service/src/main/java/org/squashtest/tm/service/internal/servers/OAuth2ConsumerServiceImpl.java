/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.servers;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import javax.inject.Provider;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.csp.core.bugtracker.core.BugTrackerNoCredentialsException;
import org.squashtest.csp.core.bugtracker.spi.DefaultOAuth2FormValues;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.domain.servers.AuthenticationProtocol;
import org.squashtest.tm.domain.servers.OAuth2Credentials;
import org.squashtest.tm.exception.bugtracker.CannotObtainOauth2TokensException;
import org.squashtest.tm.exception.bugtracker.InvalidOauth2RequestException;
import org.squashtest.tm.service.bugtracker.BugTrackerFinderService;
import org.squashtest.tm.service.internal.bugtracker.BugTrackerConnectorFactory;
import org.squashtest.tm.service.servers.CredentialsProvider;
import org.squashtest.tm.service.servers.Oauth2Tokens;
import org.squashtest.tm.service.servers.ServerAuthConfiguration;
import org.squashtest.tm.service.servers.StoredCredentialsManager;
import org.squashtest.tm.service.spi.OAuth2ConfigurationHandler;

@Service("squashtest.tm.service.OAuth2ConsumerService")
public class OAuth2ConsumerServiceImpl implements OAuth2ConsumerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(OAuth2ConsumerServiceImpl.class);

    private final StoredCredentialsManager credManager;

    private final CredentialsProvider credProvider;

    private final BugTrackerConnectorFactory bugTrackerConnectorFactory;

    private final Provider<BugTrackerFinderService> bugTrackerFinderServiceProvider;

    private final HttpClient httpClient =
            HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).build();

    public OAuth2ConsumerServiceImpl(
            StoredCredentialsManager credManager,
            CredentialsProvider credProvider,
            BugTrackerConnectorFactory bugTrackerConnectorFactory,
            Provider<BugTrackerFinderService> bugTrackerFinderServiceProvider) {
        this.credManager = credManager;
        this.credProvider = credProvider;
        this.bugTrackerConnectorFactory = bugTrackerConnectorFactory;
        this.bugTrackerFinderServiceProvider = bugTrackerFinderServiceProvider;
    }

    @Override
    public String getOauth2AuthenticationUrl(BugTracker bugTracker) {
        ServerOAuth2Conf serverOAuth2Conf = loadOauth2Conf(bugTracker.getId());
        OAuth2ConfigurationHandler handler = bugTrackerConnectorFactory.findOauth2Handler(bugTracker);
        return handler.getOauth2AuthenticationUrl(bugTracker, serverOAuth2Conf);
    }

    @Override
    public void getOauth2token(Long serverId, String code, String username) {
        ServerOAuth2Conf serverOAuth2Conf = loadOauth2Conf(serverId);
        BugTracker bugTracker = bugTrackerFinderServiceProvider.get().findById(serverId);
        OAuth2ConfigurationHandler handler = bugTrackerConnectorFactory.findOauth2Handler(bugTracker);
        String uri = handler.getOauth2RequestTokenUrl(serverId, code, serverOAuth2Conf);

        requestAndSaveTokens(serverId, username, uri);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public OAuth2Credentials refreshOauth2Token(
            Long serverId, OAuth2Credentials creds, String username) {
        ServerOAuth2Conf serverOAuth2Conf = loadOauth2Conf(serverId);
        BugTracker bugTracker = bugTrackerFinderServiceProvider.get().findById(serverId);
        OAuth2ConfigurationHandler handler = bugTrackerConnectorFactory.findOauth2Handler(bugTracker);
        String uri = handler.getRefreshTokenUrl(serverOAuth2Conf, creds);

        Oauth2Tokens tokens = requestAndSaveTokens(serverId, username, uri);
        creds.setAccessToken(tokens.getAccessToken());
        creds.setRefreshToken(tokens.getRefreshToken());
        creds.setExpirationDate(tokens.getExpirationDate());

        return creds;
    }

    @Override
    public DefaultOAuth2FormValues getDefaultOAuth2FormValue(Long serverId) {
        BugTracker bugTracker = bugTrackerFinderServiceProvider.get().findById(serverId);
        OAuth2ConfigurationHandler handler = bugTrackerConnectorFactory.findOauth2Handler(bugTracker);
        String btURL = bugTracker.getUrl().replaceAll("\\/*$", "");
        return handler.getDefaultValueForOauth2Form(btURL);
    }

    @Override
    public void getCurrentUserOauth2token(long bugTrackerId, String code) {
        String user = credProvider.currentUser();
        getOauth2token(bugTrackerId, code, user);
    }

    private Oauth2Tokens requestAndSaveTokens(Long serverId, String username, String uri) {
        ObjectMapper mapper =
                new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        HttpRequest request;
        Oauth2Tokens tokens;
        final String responseBody;
        final int statusCode;
        try {
            request =
                    HttpRequest.newBuilder()
                            .header("Content-Type", "application/x-www-form-urlencoded")
                            .uri(new URI(uri))
                            .POST(HttpRequest.BodyPublishers.noBody())
                            .build();

            HttpResponse<String> httpResponse =
                    httpClient.send(request, HttpResponse.BodyHandlers.ofString());

            responseBody = httpResponse.body();
            statusCode = httpResponse.statusCode();

            if (statusCode == 200) {
                tokens = mapper.readValue(responseBody, Oauth2Tokens.class);
                tokens.setExpirationDate();
                saveOauth2TokenToDataBase(serverId, tokens, username);
            } else {
                throw new CannotObtainOauth2TokensException(responseBody);
            }

        } catch (IOException | InterruptedException | URISyntaxException e) {
            throw new InvalidOauth2RequestException(e);
        }

        return tokens;
    }

    private void saveOauth2TokenToDataBase(Long serverId, Oauth2Tokens token, String username) {
        if (username == null) {
            LOGGER.trace(
                    "Squash-TM is now authorized on server '{}'. Storing app level credentials in the database",
                    serverId);
            credManager.storeAppLevelCredentials(serverId, token);
        } else {
            LOGGER.trace(
                    "Squash-TM is now authorized by user '{}' on server '{}', now storing them in the database",
                    username,
                    serverId);
            credManager.storeUserCredentials(serverId, username, token);
        }
    }

    private ServerOAuth2Conf loadOauth2Conf(long serverId) {

        LOGGER.debug("loading oauth2 conf for server '{}'", serverId);

        ServerAuthConfiguration storedConf = credManager.unsecuredFindServerAuthConfiguration(serverId);
        if (storedConf == null
                || storedConf.getImplementedProtocol() != AuthenticationProtocol.OAUTH_2) {
            throw new BugTrackerNoCredentialsException("No OAuth 2 configuration available !", null);
        }

        return (ServerOAuth2Conf) storedConf;
    }
}
