/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.campaign;

import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.inject.Inject;
import org.apache.commons.collections.CollectionUtils;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.acls.model.ObjectIdentity;
import org.springframework.security.acls.model.ObjectIdentityRetrievalStrategy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.IdentifiersOrderComparator;
import org.squashtest.tm.domain.audit.AuditableMixin;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.security.UserContextHolder;
import org.squashtest.tm.service.annotation.CheckBlockingMilestone;
import org.squashtest.tm.service.annotation.Id;
import org.squashtest.tm.service.annotation.PreventConcurrent;
import org.squashtest.tm.service.audit.AuditModificationService;
import org.squashtest.tm.service.campaign.CreatedTestPlanItems;
import org.squashtest.tm.service.campaign.CustomTestSuiteModificationService;
import org.squashtest.tm.service.campaign.IterationTestPlanManagerService;
import org.squashtest.tm.service.execution.ExecutionModificationService;
import org.squashtest.tm.service.internal.dto.ExecutionSummaryDto;
import org.squashtest.tm.service.internal.repository.DatasetDao;
import org.squashtest.tm.service.internal.repository.IterationDao;
import org.squashtest.tm.service.internal.repository.IterationTestPlanDao;
import org.squashtest.tm.service.internal.repository.LibraryNodeDao;
import org.squashtest.tm.service.internal.repository.UserDao;
import org.squashtest.tm.service.internal.testcase.TestCaseNodeWalker;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.security.SecurityCheckableObject;
import org.squashtest.tm.service.security.acls.model.ObjectAclService;

@Service("squashtest.tm.service.IterationTestPlanManagerService")
@Transactional
public class IterationTestPlanManagerServiceImpl implements IterationTestPlanManagerService {

    private static final String LINK_ITERATION_OR_ROLE_ADMIN =
            "hasPermission(#iterationId, 'org.squashtest.tm.domain.campaign.Iteration', 'LINK')"
                    + OR_HAS_ROLE_ADMIN;

    private static final String ITERATION_ID = "iterationId";
    private static final String WRITE = "WRITE";

    @Inject private IterationDao iterationDao;

    @Inject private IterationTestPlanDao iterationTestPlanDao;

    @Inject private ObjectAclService aclService;

    @Inject private UserDao userDao;

    @Inject private DatasetDao datasetDao;

    @Inject private CampaignNodeDeletionHandler deletionHandler;

    @Inject private PermissionEvaluationService permissionEvaluationService;

    @Inject
    @Qualifier("squashtest.tm.repository.TestCaseLibraryNodeDao")
    private LibraryNodeDao<TestCaseLibraryNode> testCaseLibraryNodeDao;

    @Inject
    @Qualifier("squashtest.core.security.ObjectIdentityRetrievalStrategy")
    private ObjectIdentityRetrievalStrategy objIdRetrievalStrategy;

    @Inject private ActiveMilestoneHolder activeMilestoneHolder;

    @Inject private CustomTestSuiteModificationService customTestSuiteModificationService;

    @Inject private AuditModificationService auditModificationService;

    @Inject private ExecutionModificationService executionModService;

    @Inject DSLContext dslContext;

    /*
     * security note here : well what if we add test cases for which the user have no permissions on ? think of
     * something better.
     *
     * (non-Javadoc)
     *
     * @see org.squashtest.csp.tm.service.IterationTestPlanManagerService#addTestCasesToIteration(java.util.List, long)
     */

    @Override
    @PreAuthorize(LINK_ITERATION_OR_ROLE_ADMIN)
    @PreventConcurrent(entityType = Iteration.class, paramName = ITERATION_ID)
    @CheckBlockingMilestone(entityType = Iteration.class)
    public CreatedTestPlanItems addTestCasesToIteration(
            final List<Long> objectsIds, @Id long iterationId) {
        return addTestCasesToIterationUnsecured(objectsIds, iterationId);
    }

    @Override
    public CreatedTestPlanItems addTestCasesToIterationUnsecured(
            final List<Long> objectsIds, @Id long iterationId) {

        Iteration iteration = iterationDao.findById(iterationId);

        return addTestPlanItemsToIteration(objectsIds, iteration);
    }

    @PreventConcurrent(entityType = Iteration.class, paramName = ITERATION_ID)
    @Override
    @CheckBlockingMilestone(entityType = Iteration.class)
    @PreAuthorize(LINK_ITERATION_OR_ROLE_ADMIN)
    public void copyTestPlanItems(List<Long> iterationTestPlanIds, @Id long iterationId) {
        final Iteration iteration = iterationDao.findById(iterationId);
        final List<IterationTestPlanItem> items = findTestPlanItems(iterationTestPlanIds);

        for (IterationTestPlanItem item : items) {
            if (!item.isTestCaseDeleted()) {
                final IterationTestPlanItem copy =
                        new IterationTestPlanItem(item.getReferencedTestCase(), item.getReferencedDataset());
                iterationTestPlanDao.save(copy);
                iteration.addTestPlan(copy);
            }
        }
    }

    public CreatedTestPlanItems addTestPlanItemsToIteration(
            final List<Long> testNodesIds, Iteration iteration) {

        // nodes are returned unsorted
        List<TestCaseLibraryNode> nodes = testCaseLibraryNodeDao.findAllByIds(testNodesIds);

        // now we resort them according to the order in which the testcase ids were given
        IdentifiersOrderComparator comparator = new IdentifiersOrderComparator(testNodesIds);
        nodes.sort(comparator);

        List<TestCase> testCases = new TestCaseNodeWalker().walk(nodes);

        final Optional<Milestone> activeMilestone = activeMilestoneHolder.getActiveMilestone();

        activeMilestone.ifPresent(
                milestone ->
                        CollectionUtils.filter(
                                testCases, tc -> ((TestCase) tc).getMilestones().contains(milestone)));

        List<IterationTestPlanItem> testPlan = new LinkedList<>();
        boolean foundDatasets = false;

        for (TestCase testCase : testCases) {
            boolean thisTestCaseHasDatasets = addTestCaseToTestPlan(testCase, testPlan);
            foundDatasets = foundDatasets || thisTestCaseHasDatasets;
        }

        addTestPlanToIteration(testPlan, iteration.getId());

        List<Long> ids = testPlan.stream().map(IterationTestPlanItem::getId).toList();

        return new CreatedTestPlanItems(ids, foundDatasets);
    }

    private boolean addTestCaseToTestPlan(TestCase testCase, List<IterationTestPlanItem> testPlan) {
        testPlan.addAll(testCase.createTestPlanItems());
        return !CollectionUtils.isEmpty(testCase.getDatasets());
    }

    private void addTestPlanToIteration(List<IterationTestPlanItem> testPlan, @Id long iterationId) {
        Iteration iteration = iterationDao.findById(iterationId);
        for (IterationTestPlanItem itp : testPlan) {
            iteration.addTestPlan(itp);
            iterationTestPlanDao.save(itp);
        }
        auditModificationService.updateAuditable((AuditableMixin) iteration);
    }

    @Override
    @PreAuthorize(LINK_ITERATION_OR_ROLE_ADMIN)
    @PreventConcurrent(entityType = Iteration.class)
    @CheckBlockingMilestone(entityType = Iteration.class)
    public void changeTestPlanPosition(@Id long iterationId, int newPosition, List<Long> itemIds) {
        Iteration iteration = iterationDao.findById(iterationId);
        List<IterationTestPlanItem> items = iterationTestPlanDao.findAllByIdIn(itemIds);
        iteration.moveTestPlans(newPosition, items);
    }

    @Override
    @PreAuthorize(LINK_ITERATION_OR_ROLE_ADMIN)
    @PreventConcurrent(entityType = Iteration.class, paramName = ITERATION_ID)
    @CheckBlockingMilestone(entityType = Iteration.class)
    public boolean removeTestPlansFromIteration(List<Long> testPlanIds, @Id long iterationId) {
        Iteration it = iterationDao.findById(iterationId);

        return removeTestPlansFromIterationObj(testPlanIds, it);
    }

    @Override
    @PreventConcurrent(entityType = Iteration.class, paramName = ITERATION_ID)
    @CheckBlockingMilestone(entityType = Iteration.class)
    public void removeExecutionsFromTestPlanItem(List<Long> executionIds, @Id Long iterationId) {
        for (Long executionId : executionIds) {
            removeTestPlanExecution(executionId);
        }
    }

    @Override
    public Map<Long, List<ExecutionSummaryDto>> getLastExecutionStatuses(Collection<Long> itemId) {
        return executionModService.findIterationTestPlanItemLastExecStatuses(itemId);
    }

    private void removeTestPlanExecution(Long executionId) {
        Execution execution = executionModService.findById(executionId);
        IterationTestPlanItem testPlan = execution.getTestPlan();
        executionModService.deleteExecution(execution);

        if (testPlan.getExecutions().isEmpty()) {
            assignUserToTestPlanItem(testPlan.getId(), 0);
        }
    }

    @Override
    @PreAuthorize("hasPermission(#iteration, 'LINK') " + OR_HAS_ROLE_ADMIN)
    public boolean removeTestPlansFromIterationObj(List<Long> testPlanItemsIds, Iteration iteration) {
        boolean unauthorizedDeletion = false;

        List<IterationTestPlanItem> items = iterationTestPlanDao.findAllByIdIn(testPlanItemsIds);
        int sizeBeforeDeletion = iteration.getTestPlans().size();

        Set<TestSuite> testSuitesToUpdate = new HashSet<>();
        for (IterationTestPlanItem item : items) {
            testSuitesToUpdate.addAll(item.getTestSuites());
        }

        for (IterationTestPlanItem item : items) {
            // We do not allow deletion if there are execution and the user does not have sufficient
            // rights
            // so we keep track of whether at least one item couldn't be deleted
            unauthorizedDeletion =
                    removeTestPlanItemIfOkWithExecsAndRights(iteration, item) || unauthorizedDeletion;
        }

        // if an ITPI was deleted, we update the test suites status
        if (sizeBeforeDeletion > iteration.getTestPlans().size()) {
            customTestSuiteModificationService.updateExecutionStatus(testSuitesToUpdate);
        }

        return unauthorizedDeletion;
    }

    private boolean removeTestPlanItemIfOkWithExecsAndRights(
            Iteration iteration, IterationTestPlanItem item) {
        boolean unauhorized = false;

        if (item.getExecutions().isEmpty()) {
            doRemoveTestPlanItemFromIteration(iteration, item);
        } else {
            try {
                PermissionsUtils.checkPermission(
                        permissionEvaluationService, new SecurityCheckableObject(item, "EXTENDED_DELETE"));
                doRemoveTestPlanItemFromIteration(iteration, item);
            } catch (AccessDeniedException exception) { // NOSONAR
                // : this exception is part of the nominal use case
                unauhorized = true;
            }
        }
        return unauhorized;
    }

    private void doRemoveTestPlanItemFromIteration(Iteration iteration, IterationTestPlanItem item) {
        iteration.removeItemFromTestPlan(item);
        deletionHandler.deleteIterationTestPlanItem(item);
        auditModificationService.updateAuditable((AuditableMixin) iteration);
    }

    @Override
    @PreAuthorize("hasPermission(#testPlanItem, 'LINK')" + OR_HAS_ROLE_ADMIN)
    public boolean removeTestPlanFromIteration(IterationTestPlanItem testPlanItem) {
        Iteration iteration = testPlanItem.getIteration();

        // We do not allow deletion if there are execution and the user isn't authorized to do that
        return removeTestPlanItemIfOkWithExecsAndRights(iteration, testPlanItem);
    }

    @Override
    @PreAuthorize(
            "hasPermission(#item, 'EXECUTE') " + OR_HAS_ROLE_ADMIN + "or hasRole('ROLE_TA_API_CLIENT')")
    public void updateMetadata(IterationTestPlanItem item) {
        Execution execution = item.getLatestExecution();
        if (execution != null) {
            item.setLastExecutedBy(execution.getLastExecutedBy());
            item.setLastExecutedOn(execution.getLastExecutedOn());
            item.setUser(userDao.findUserByLogin(execution.getLastExecutedBy()));
        }
    }

    /**
     * Instead of using data from an actual execution (like {@link
     * #updateMetadata(IterationTestPlanItem)}, we update those data according to given parameters.
     *
     * @param item: the iteration test plan item to update the metadatas of
     * @param user : the user that will be set as last executor and assigne
     * @param date : the date to set lastExecutedOn property
     */
    private void arbitraryUpdateMetadata(IterationTestPlanItem item, User user, Date date) {

        item.setLastExecutedBy(user.getLogin());
        item.setLastExecutedOn(date);
        item.setUser(user);
    }

    @Override
    public List<User> findAssignableUsersForTestPlan(long iterationId) {

        Iteration iteration = iterationDao.findById(iterationId);

        List<ObjectIdentity> entityRefs = new ArrayList<>();

        ObjectIdentity oid = objIdRetrievalStrategy.getObjectIdentity(iteration);
        entityRefs.add(oid);

        List<String> loginList = aclService.findUsersWithExecutePermission(entityRefs);

        return userDao.findUsersByLoginList(loginList);
    }

    private void assignUserToTestPlanItem(long testPlanItemId, long userId) {
        User user = userId == 0 ? null : userDao.getReferenceById(userId);

        IterationTestPlanItem itp = iterationTestPlanDao.findByIdWithTestCase(testPlanItemId);
        if (!itp.isTestCaseDeleted()) {
            itp.setUser(user);
        }
    }

    /**
     * @see IterationTestPlanManagerService#assignUserToTestPlanItems(List, long)
     */
    @Override
    public void assignUserToTestPlanItems(List<Long> testPlanIds, long userId) {
        // check permission
        PermissionsUtils.checkPermission(
                permissionEvaluationService, testPlanIds, WRITE, IterationTestPlanItem.class.getName());
        List<IterationTestPlanItem> items = iterationTestPlanDao.findAllByIdIn(testPlanIds);

        User user = userId == 0 ? null : userDao.getReferenceById(userId);

        for (IterationTestPlanItem item : items) {
            if (!item.isTestCaseDeleted()) {
                item.setUser(user);
            }
        }
    }

    @Override
    public void removeTestPlanItemsAssignments(List<Long> testPlanIds) {
        // check permission
        PermissionsUtils.checkPermission(
                permissionEvaluationService, testPlanIds, WRITE, IterationTestPlanItem.class.getName());
        List<IterationTestPlanItem> items = iterationTestPlanDao.findAllByIdIn(testPlanIds);

        for (IterationTestPlanItem item : items) {
            if (!item.isTestCaseDeleted()) {
                item.setUser(null);
            }
        }
    }

    @Override
    @PreAuthorize(
            "hasPermission(#itemTestPlanId, 'org.squashtest.tm.domain.campaign.IterationTestPlanItem', 'READ') "
                    + OR_HAS_ROLE_ADMIN)
    public IterationTestPlanItem findTestPlanItem(long itemTestPlanId) {
        return iterationTestPlanDao.findByIdWithTestCase(itemTestPlanId);
    }

    /**
     * {@link IterationTestPlanManagerService}
     *
     * @return all testPlan Items
     */
    @Override
    public List<IterationTestPlanItem> forceExecutionStatus(
            List<Long> testPlanIds, String statusName) {
        PermissionsUtils.checkPermission(
                permissionEvaluationService, testPlanIds, WRITE, IterationTestPlanItem.class.getName());
        List<IterationTestPlanItem> testPlanItems = iterationTestPlanDao.findAllByIdIn(testPlanIds);
        ExecutionStatus status = ExecutionStatus.valueOf(statusName);
        String login = UserContextHolder.getUsername();
        User user = userDao.findUserByLogin(login);
        Date date = new Date();

        Set<TestSuite> testSuitesToUpdate = new HashSet<>();
        for (IterationTestPlanItem item : testPlanItems) {
            item.setExecutionStatus(status);
            arbitraryUpdateMetadata(item, user, date);
            testSuitesToUpdate.addAll(item.getTestSuites());
        }

        customTestSuiteModificationService.updateExecutionStatus(testSuitesToUpdate);
        return testPlanItems;
    }

    /**
     * Creates a fragment of test plan, containing either :
     *
     * <ul>
     *   <li>a unique item when the test case is not parameterized
     *   <li>one item per dataset when the test case is parameterized
     * </ul>
     *
     * <p><strong>Note :</strong> The returned test plan fragment is in a transient state.
     *
     * @return created test plan fragment
     */
    private Collection<IterationTestPlanItem> createTestPlanFragment(TestCase testCase) {
        List<IterationTestPlanItem> fragment = new ArrayList<>();
        addTestCaseToTestPlan(testCase, fragment);
        return fragment;
    }

    /**
     * @see IterationTestPlanManagerService#createTestPlanFragment(TestCase, User)
     */
    @Override
    public Collection<IterationTestPlanItem> createTestPlanFragment(
            TestCase testCase, User assignee) {
        Collection<IterationTestPlanItem> fragment = createTestPlanFragment(testCase);

        for (IterationTestPlanItem item : fragment) {
            item.setUser(assignee);
        }

        return fragment;
    }

    @Override
    @PreAuthorize(
            "hasPermission(#itemId, 'org.squashtest.tm.domain.campaign.IterationTestPlanItem', 'WRITE') "
                    + OR_HAS_ROLE_ADMIN)
    @CheckBlockingMilestone(entityType = IterationTestPlanItem.class)
    public void changeDataset(@Id long itemId, Long datasetId) {
        IterationTestPlanItem item = iterationTestPlanDao.findByIdWithTestCase(itemId);

        if (datasetId == null) {
            item.setReferencedDataset(null);
        } else if (!item.isTestCaseDeleted()) {
            TestCase tc = item.getReferencedTestCase();
            Dataset ds = datasetDao.getReferenceById(datasetId);
            if (!ds.getTestCase().equals(tc)) {
                throw new IllegalArgumentException(
                        "dataset [id:'"
                                + ds.getId()
                                + "', name:'"
                                + ds.getName()
                                + "'] doesn't belong to test case [id:'"
                                + tc.getId()
                                + "', name:'"
                                + tc.getName()
                                + "']");
            }
            item.setReferencedDataset(ds);
        }
    }

    @Override
    public List<IterationTestPlanItem> findTestPlanItems(List<Long> ids) {
        return iterationTestPlanDao.findAllByIdIn(ids);
    }
}
