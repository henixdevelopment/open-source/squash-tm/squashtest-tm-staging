/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.bdd.robot;

import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.NEW_LINE;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.memberAccess;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.scalarVariable;

import java.util.List;
import org.squashtest.tm.domain.testcase.KeywordTestCase;
import org.squashtest.tm.domain.testcase.KeywordTestStep;
import org.squashtest.tm.service.internal.testcase.bdd.TextGridFormatter;

public final class SectionBuilderHelpers {
    public static final String FOUR_SPACES = "    ";
    public static final String TEST_SETUP = "Test Setup";
    public static final String TEST_TEARDOWN = "Test Teardown";
    public static final String DATASET_VARIABLE_NAME = "dataset";
    public static final String RETRIEVE_DATASET_KEYWORD_NAME = "Retrieve Dataset";
    public static final String DATATABLES_VARIABLE_NAME = "datatables";
    private static final String DATATABLE_PARAM_FORMAT = "datatable_%s";
    public static final String RETRIEVE_DATATABLES_KEYWORD_NAME = "Retrieve Datatables";
    public static final String DOCSTRINGS_VARIABLE_NAME = "docstrings";

    public static final String DOCSTRING_NAME_FORMAT = "docstring_%s";

    public static final String RETRIEVE_DOCSTRINGS_KEYWORD_NAME = "Retrieve Docstrings";

    private SectionBuilderHelpers() {
        throw new UnsupportedOperationException("This class is not meant to be instantiated.");
    }

    /**
     * Builds a [Documentation] setting fragment from a multi-line documentation text. E.g.
     *
     * <p>[Documentation] first line of documentation ... second line of documentation
     *
     * <p>Because these fragments are only used inside Test Cases and Keywords, they are always
     * indented.
     *
     * @param content multi-line string content. Each line is expected to be a single line (e.g. not
     *     containing new line characters)
     * @return formatted String output
     */
    public static String formatDocumentationSettingLines(List<String> content) {
        return new TextGridFormatter()
                .addRows(buildDocumentationSettingLines(content))
                .format(TextGridFormatter.withRowPrefix(FOUR_SPACES));
    }

    private static List<List<String>> buildDocumentationSettingLines(List<String> content) {
        return RobotSyntaxHelpers.buildMultilineEntry(
                RobotSyntaxHelpers.documentationSetting(), content);
    }

    /**
     * Utility class that's just a wrapper around a StringBuilder with additional methods for new
     * lines.
     */
    public static final class MultiLineStringBuilder {
        final StringBuilder stringBuilder = new StringBuilder();

        public MultiLineStringBuilder append(String content) {
            stringBuilder.append(content);
            return this;
        }

        public MultiLineStringBuilder appendLine(String content) {
            stringBuilder.append(content).append(NEW_LINE);
            return this;
        }

        public MultiLineStringBuilder appendNewLine() {
            stringBuilder.append(NEW_LINE);
            return this;
        }

        public String toString() {
            return stringBuilder.toString();
        }
    }

    /**
     * Constructs a datatable identifier. E.g. "datatable_42"
     *
     * @param number datatable index (1 based)
     * @return a datatable identifier
     */
    public static String formatDatatableName(int number) {
        return String.format(DATATABLE_PARAM_FORMAT, number);
    }

    /**
     * Returns a datatable accessor from datatables dictionary. E.g "${datatables}[datatable_2]"
     *
     * @param number datatable index (1 based)
     * @return dictionary access expression for a datatable
     */
    public static String formatDatatableAccessor(int number) {
        return formatDictionaryAccessor(DATATABLES_VARIABLE_NAME, formatDatatableName(number));
    }

    /**
     * Constructs a docstring identifier. E.g. "docstring_42"
     *
     * @param number docstring index (1 based)
     * @return a docstring identifier
     */
    public static String formatDocstringName(int number) {
        return String.format(DOCSTRING_NAME_FORMAT, number);
    }

    /**
     * Returns a docstring accessor from docstrings dictionary. E.g "${docstrings}[docstring_2]"
     *
     * @param number docstring index (1 based)
     * @return dictionary access expression for a docstring
     */
    public static String formatDocstringAccessor(int number) {
        return formatDictionaryAccessor(DOCSTRINGS_VARIABLE_NAME, formatDocstringName(number));
    }

    private static String formatDictionaryAccessor(String dictionaryName, String key) {
        return scalarVariable(dictionaryName) + memberAccess(key);
    }

    /** Indirection layer to determine if the dataset sections should be generated. */
    public static boolean isTestCaseUsingDatasets(KeywordTestCase testCase) {
        return testCase.containsStepsUsingTcParam();
    }

    /** Indirection layer to determine if the datatable sections should be generated. */
    public static boolean isTestCaseUsingDatatables(KeywordTestCase testCase) {
        return testCase.containsStepsUsingDatatable();
    }

    /**
     * Docstrings sections are present if there's at least a step with a docstring but no datatable
     *
     * @param testCase source keyword test case
     * @return true if docstrings sections should be present
     */
    public static boolean isTestCaseUsingDocstrings(KeywordTestCase testCase) {
        return !extractUsedDocstrings(testCase).isEmpty();
    }

    public static List<String> extractUsedDocstrings(KeywordTestCase keywordTestCase) {
        return keywordTestCase.getSteps().stream()
                .filter(KeywordTestStep.class::isInstance)
                .filter(
                        step -> {
                            final String docstring = ((KeywordTestStep) step).getDocstring();
                            final String datatable = ((KeywordTestStep) step).getDatatable();
                            final boolean hasDocstring = docstring != null && !docstring.isEmpty();
                            final boolean hasDatatable = datatable != null && !datatable.isEmpty();
                            return hasDocstring && !hasDatatable;
                        })
                .map(step -> ((KeywordTestStep) step).getDocstring())
                .toList();
    }
}
