/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.resultpublisher;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.Map;
import java.util.Optional;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.NotNull;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.plugin.UsedInPlugin;
import org.squashtest.tm.api.testautomation.execution.dto.TestExecutionStatus;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.testautomation.AutomatedSuite;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.service.attachment.AttachmentManagerService;
import org.squashtest.tm.service.internal.display.dto.AttachmentDto;
import org.squashtest.tm.service.internal.repository.AttachmentDao;
import org.squashtest.tm.service.internal.repository.AutomatedSuiteDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.testautomation.model.Attachment;
import org.squashtest.tm.service.testautomation.model.AutomatedExecutionState;
import org.squashtest.tm.service.testautomation.model.RestUploadedData;

/**
 * @author lpoma
 */
@Service
@Transactional
public class AutomatedSuitePublisherService {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(AutomatedSuitePublisherService.class);
    private static final Map<ExecutionStatus, Integer> PRIORITY_MAP;

    @Inject private AutomatedSuiteDao autoSuiteDao;

    @Inject private AttachmentManagerService attachmentManagerService;

    @Inject private RestITPIManagerService restITPIManagerService;

    @Inject private PermissionEvaluationService permissionEvaluationService;

    @Inject private AttachmentDao attachmentDao;

    @PersistenceContext private EntityManager entityManager;

    static {
        EnumMap<ExecutionStatus, Integer> statusPriorityMap = new EnumMap<>(ExecutionStatus.class);
        statusPriorityMap.put(ExecutionStatus.READY, 1);
        statusPriorityMap.put(ExecutionStatus.RUNNING, 2);
        statusPriorityMap.put(ExecutionStatus.UNTESTABLE, 3);
        statusPriorityMap.put(ExecutionStatus.SETTLED, 4);
        statusPriorityMap.put(ExecutionStatus.SKIPPED, 5);
        statusPriorityMap.put(ExecutionStatus.SUCCESS, 6);
        statusPriorityMap.put(ExecutionStatus.FAILURE, 7);
        statusPriorityMap.put(ExecutionStatus.BLOCKED, 8);
        statusPriorityMap.put(ExecutionStatus.CANCELLED, 9);

        PRIORITY_MAP = Collections.unmodifiableMap(statusPriorityMap);
    }

    public void forceExecutionStatus(String suiteId, @NotNull AutomatedExecutionState stateChange) {
        AutomatedSuite autoSuite = autoSuiteDao.findById(suiteId);
        checkPermissionsToUpdateAutomatedSuite(autoSuite);
        Date date = new Date();
        arbitraryUpdateMetadata(autoSuite, date);
        updateSelfExecutionStatus(autoSuite, stateChange);

        Optional.ofNullable(stateChange.getAttachments())
                .ifPresent(
                        attachments -> {
                            for (Attachment attachment : attachments) {
                                updateAttachments(
                                        autoSuite.getAttachmentList().getId(), attachment, EntityType.AUTOMATED_SUITE);
                            }
                        });
    }

    private void arbitraryUpdateMetadata(AutomatedSuite autoSuite, Date date) {
        autoSuite.setLastModifiedOn(date);
    }

    private void updateSelfExecutionStatus(
            AutomatedSuite automatedSuite, AutomatedExecutionState stateChange) {
        TestExecutionStatus testExecutionStatus =
                stateChange.getTfTestExecutionStatus().tfTestExecutionStatusToTmTestExecutionStatus();
        ExecutionStatus formerStatus = automatedSuite.getExecutionStatus().getCanonicalStatus();
        ExecutionStatus newStatus =
                ExecutionStatus.valueOf(testExecutionStatus.getStatus().name()).getCanonicalStatus();

        if (PRIORITY_MAP.get(newStatus) > PRIORITY_MAP.get(formerStatus)) {
            automatedSuite.setExecutionStatus(newStatus);
        }
        entityManager.flush();
        entityManager.clear();
    }

    @UsedInPlugin("rest-api")
    public void addItpiLabelAndIdToAttachmentName(long testId, AutomatedExecutionState stateChange) {
        final IterationTestPlanItem itpi = restITPIManagerService.getById(testId);
        final TestCase testCase = itpi.getReferencedTestCase();
        final String itpiLabel = testCase == null ? itpi.getLabel() : testCase.getName();
        final Long itpiId = itpi.getId();

        Optional.ofNullable(stateChange.getAttachments())
                .ifPresent(
                        attachments ->
                                attachments.forEach(
                                        attachment ->
                                                attachment.setName(
                                                        itpiLabel + "[" + itpiId + "]" + "-" + attachment.getName())));
    }

    @UsedInPlugin("rest-api")
    public AttachmentDto attachReportToAutomatedSuite(String suiteId, Attachment allureUpdate) {
        AutomatedSuite autoSuite = autoSuiteDao.findById(suiteId);
        checkPermissionsToUpdateAutomatedSuite(autoSuite);
        Long attachmentListId = autoSuite.getAttachmentList().getId();
        return updateAttachments(attachmentListId, allureUpdate, EntityType.AUTOMATED_SUITE);
    }

    @UsedInPlugin("rest-api")
    public AttachmentDto updateAttachmentContent(Attachment testAutoAttachment, Long attachmentId) {
        try {
            org.squashtest.tm.domain.attachment.Attachment attachment =
                    attachmentDao.findById(attachmentId).orElseThrow(EntityNotFoundException::new);
            return attachmentManagerService.updateAttachmentContent(
                    attachment, testAutoAttachment.getContent());
        } catch (IOException e) {
            LOGGER.error("Unable to read attachment.", e);
            throw new NullPointerException();
        }
    }

    public AttachmentDto updateAttachments(
            Long attachmentListId, Attachment attachment, EntityType entityType) {
        try {
            InputStream stream = attachment.getContent();
            String name = attachment.getName();
            long sizeInBytes = attachment.getContent().available();
            RestUploadedData attachmentUploadedData = new RestUploadedData(stream, name, sizeInBytes);
            return attachmentManagerService.addAttachment(
                    attachmentListId, attachmentUploadedData, entityType);
        } catch (IOException e) {
            LOGGER.debug("Unable to read attachment.", e);
        }
        return null;
    }

    private void checkPermissionsToUpdateAutomatedSuite(AutomatedSuite suite) {
        boolean hasPermission = false;

        Iteration iteration = suite.getIteration();
        TestSuite testSuite = suite.getTestSuite();

        if (permissionEvaluationService.hasRole("ROLE_TA_API_CLIENT")
                || permissionEvaluationService.hasRole("ROLE_ADMIN")) {
            hasPermission = true;
        } else if (iteration != null) {
            hasPermission = permissionEvaluationService.hasPermissionOnObject("EXECUTE", iteration);
        } else if (testSuite != null) {
            hasPermission = permissionEvaluationService.hasPermissionOnObject("EXECUTE", testSuite);
        }

        if (!hasPermission) {
            throw new AccessDeniedException("Access is denied.");
        }
    }

    @UsedInPlugin("rest-api")
    public void updateAutomatedSuiteExecStatus(String suiteId, String status) {

        try {
            ExecutionStatus executionStatus = ExecutionStatus.valueOf(status);

            AutomatedSuite autoSuite = autoSuiteDao.findById(suiteId);
            autoSuite.setExecutionStatus(executionStatus);

            TestSuite testSuite = autoSuite.getTestSuite();
            if (testSuite != null) {
                testSuite.setExecutionStatus(executionStatus);
            }

            if (ExecutionStatus.BLOCKED.equals(executionStatus)) {
                autoSuite.setWorkflows(null);
            }

            entityManager.flush();
            entityManager.clear();
        } catch (IllegalArgumentException ex) {
            LOGGER.error("Received status {} is not a valid status.", status, ex);
        }
    }
}
