/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display;

import java.util.List;
import java.util.Map;
import java.util.Set;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.service.internal.display.dto.campaign.CampaignDto;
import org.squashtest.tm.service.internal.display.dto.campaign.CampaignFolderDto;
import org.squashtest.tm.service.internal.display.dto.campaign.CampaignLibraryDto;
import org.squashtest.tm.service.internal.display.dto.campaign.IterationPlanningDto;

public interface CampaignDisplayDao {
    CampaignLibraryDto getCampaignLibraryDtoById(Long id);

    CampaignFolderDto getCampaignFolderDtoById(long campaignFolderId);

    CampaignDto getCampaignDtoById(long campaignId);

    List<IterationPlanningDto> findIterationPlanningByCampaign(Long campaignId);

    int getNbTestPlanItem(Long campaignId, String login);

    List<String> retrieveFullNameByCampaignLibraryNodeIds(
            List<Long> campaignLibraryNodeIds, List<Long> projectIds);

    List<Long> findDistinctProjectIdsByCampaignLibraryIds(Set<Long> campaignLibraryIds);

    List<Long> findDistinctProjectIdsByCampaignLibraryNodeIds(Set<Long> campaignIds);

    List<String> findAllDisabledExecutionStatusByProjectIds(Set<Long> projectIds);

    List<NamedReference> findNamedReferences(List<Long> ids);

    Map<Long, List<Long>> findExecutionIdsByCampaignIds(List<Long> campaignIds);

    /**
     * Find the campaign IDs that match the given campaign library node IDs or that are contained in
     * the given campaign library node IDs.
     *
     * @param campaignLibraryNodeIds the campaign library node IDs
     */
    List<Long> findContainedCampaignIds(List<Long> campaignLibraryNodeIds);
}
