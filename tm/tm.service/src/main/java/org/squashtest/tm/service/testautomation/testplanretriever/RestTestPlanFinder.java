/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.testplanretriever;

import java.util.List;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.service.testautomation.model.AutomatedSuiteCreationSpecification;
import org.squashtest.tm.service.testautomation.testplanretriever.exception.NoITPIFoundException;
import org.squashtest.tm.service.testautomation.testplanretriever.exception.NotFoundTargetUUIDException;

public interface RestTestPlanFinder {

    Iteration findIterationByUuid(String uuid) throws NotFoundTargetUUIDException;

    TestSuite findTestSuiteByUuid(String uuid) throws NotFoundTargetUUIDException;

    List<Long> getItemTestPlanIdsByIterationUuid(String uuid) throws NoITPIFoundException;

    List<Long> getItemTestPlanIdsByTestSuiteUuid(String uuid) throws NoITPIFoundException;

    List<Long> getItemTestPlanIdsByIterationId(Long iterationId);

    List<Long> getItemTestPlanIdsByTestSuiteId(Long testSuiteId);

    List<Long> getItemTestPlanIdsFromSpecification(AutomatedSuiteCreationSpecification specification);
}
