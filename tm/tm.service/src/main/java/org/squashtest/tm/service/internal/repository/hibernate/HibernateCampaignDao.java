/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.service.internal.repository.EntityGraphName.ATTACHMENTS;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.ATTACHMENT_LIST;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.CONTENT;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.MILESTONES;
import static org.squashtest.tm.service.internal.repository.EntityGraphName.TEST_PLAN;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_CAMPAIGNS_BY_IDS;
import static org.squashtest.tm.service.internal.repository.JpaQueryString.FIND_CHILD_ENTITY_DTO_CAMPAIGNS_BY_IDS;
import static org.squashtest.tm.service.internal.repository.ParameterNames.ID;
import static org.squashtest.tm.service.internal.repository.ParameterNames.IDS;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.inject.Provider;
import org.hibernate.annotations.QueryHints;
import org.hibernate.query.Query;
import org.hibernate.type.LongType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.TestPlanStatistics;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.service.clipboard.model.ClipboardPayload;
import org.squashtest.tm.service.internal.copier.ChildEntityDtoResult;
import org.squashtest.tm.service.internal.repository.CampaignDao;
import org.squashtest.tm.service.internal.repository.ParameterNames;
import org.squashtest.tm.service.internal.repository.hibernate.loaders.EntityGraphQueryBuilder;
import org.squashtest.tm.service.statistics.CountOnEnum;

@Repository
public class HibernateCampaignDao extends HibernateEntityDao<Campaign> implements CampaignDao {
    private static final String NODE_IDS = "nodeIds";
    private static final String MILESTONE_ID = "milestoneId";
    private static final String CAMPAIGN_IDS = "campaignIds";

    private final Provider<HibernateIterationDao> iterationDaoProvider;

    @Autowired
    public HibernateCampaignDao(Provider<HibernateIterationDao> iterationDaoProvider) {
        this.iterationDaoProvider = iterationDaoProvider;
    }

    private List<Long> findAllCampaignIdsByLibraries(Collection<Long> libraryIds) {
        if (!libraryIds.isEmpty()) {
            Query query = currentSession().getNamedQuery("campaign.findAllCampaignIdsByLibraries");
            query.setParameterList("libraryIds", libraryIds, LongType.INSTANCE);
            return query.list();
        } else {
            return Collections.emptyList();
        }
    }

    public List<Long> findAllCampaignIdsByNodeIds(Collection<Long> nodeIds) {
        if (!nodeIds.isEmpty()) {
            Query query = currentSession().getNamedQuery("campaign.findAllCampaignIdsByNodeIds");
            query.setParameterList(NODE_IDS, nodeIds, LongType.INSTANCE);
            return query.list();
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public Map<Long, String> findAllCampaignIdsAndComputePathAsNameByLibraries(
            Collection<Long> libraryIds) {
        Map<Long, String> campaignNameMap = new HashMap<>();

        if (!libraryIds.isEmpty()) {
            List<Long> campaignIds = findAllCampaignIdsByLibraries(libraryIds);

            if (!campaignIds.isEmpty()) {
                List<Object[]> resultTuples =
                        findAllCampaignPathAsNameByCampaignIds(
                                "CampaignPathEdge.findPathsByCampaignIdsAndLibrary", campaignIds);

                // process tuples
                processTuplesIntoMap(resultTuples, campaignNameMap);
            }
        }
        return campaignNameMap;
    }

    private List<Object[]> findAllCampaignPathAsNameByCampaignIds(
            String queryName, List<Long> campaignIds) {
        Query campaignNameQuery = currentSession().getNamedQuery(queryName);
        campaignNameQuery.setParameterList(CAMPAIGN_IDS, campaignIds, LongType.INSTANCE);
        return campaignNameQuery.list();
    }

    private void processTuplesIntoMap(List<Object[]> tuples, Map<Long, String> map) {
        for (Object[] tuple : tuples) {
            validateTuple(tuple);
            Long key = (Long) tuple[0];
            String value = tuple[1].toString();
            map.put(key, value);
        }
    }

    private static void validateTuple(Object[] tuple) {
        if (Objects.isNull(tuple) || tuple.length < 2) {
            throw new IllegalArgumentException(
                    "Illegal Tuple. Null or tuples with less than two columns are rejected");
        }
    }

    @Override
    public Map<Long, String> findAllCampaignIdsAndComputePathAsNameByNodeIds(
            Collection<Long> nodeIds) {
        Map<Long, String> campaignNameMap = new HashMap<>();
        if (!nodeIds.isEmpty()) {
            List<Long> campaignIds = findAllCampaignIdsByNodeIds(nodeIds);

            if (!campaignIds.isEmpty()) {
                List<Object[]> results =
                        findAllCampaignPathAsNameByCampaignIds(
                                "CampaignPathEdge.findPathsByCampaignIdsAndNode", campaignIds);
                processTuplesIntoMap(results, campaignNameMap);
            }
        }
        return campaignNameMap;
    }

    // NOTE THIS RETURNS CAMPAIGN FULL NAME, DOES NOT COMPUTE PATH
    @Override
    public Map<Long, String> findAllCampaignIdsAndNameByLibraryIds(List<Long> libraryIds) {
        Map<Long, String> campaignNameMap = new HashMap<>();
        if (!libraryIds.isEmpty()) {
            Query query =
                    currentSession().getNamedQuery("campaign.findAllCampaignNamedReferenceByLibraries");
            query.setParameterList("libraryIds", libraryIds, LongType.INSTANCE);
            List<Object[]> results = query.list();

            processTuplesIntoMap(results, campaignNameMap);
        }
        return campaignNameMap;
    }

    @Override
    public Map<Long, String> findAllCampaignIdsAndNameByNodeIds(List<Long> nodeIds) {
        Map<Long, String> campaignNameMap = new HashMap<>();
        if (!nodeIds.isEmpty()) {
            Query query =
                    currentSession().getNamedQuery("campaign.findAllCampaignNamedReferenceByNodeIds");
            query.setParameterList(NODE_IDS, nodeIds, LongType.INSTANCE);
            List<Object[]> results = query.list();

            processTuplesIntoMap(results, campaignNameMap);
        }
        return campaignNameMap;
    }

    @Override
    public List<Long> filterByMilestone(Collection<Long> campaignIds, Long milestoneId) {
        List<Long> res;

        if (milestoneId == null) {
            res = new ArrayList(campaignIds);
        } else if (!campaignIds.isEmpty()) {
            Query query = currentSession().getNamedQuery("campaign.filterByMilestone");
            query.setParameterList(CAMPAIGN_IDS, campaignIds, LongType.INSTANCE);
            query.setParameter(MILESTONE_ID, milestoneId, LongType.INSTANCE);
            res = query.list();
        } else {
            res = Collections.emptyList();
        }

        return res;
    }

    @Override
    public List<Long> findAllIdsByMilestone(Long milestoneId) {

        if (milestoneId != null) {
            Query query = currentSession().getNamedQuery("campaign.findAllIdsByMilestoneId");
            query.setParameter(MILESTONE_ID, milestoneId, LongType.INSTANCE);
            return query.list();
        } else {
            throw new IllegalArgumentException("milestoneId should not be null");
        }
    }

    @Override
    public int countIterations(long campaignId) {
        Long count = executeEntityNamedQuery("campaign.countIterations", idParameter(campaignId));
        return count.intValue();
    }

    private SetQueryParametersCallback idParameter(long campaignId) {
        return new SetIdParameter(ParameterNames.CAMPAIGN_ID, campaignId);
    }

    /**
     * Compute TestPlanStatics for each itpi in scope. Only the last executed itpi for a tc/dataset
     * pair, or last created if no execution has ever been done, are listed
     *
     * @param campaignId the id of the concerned campaign
     * @param itpiIds the id of the itpi in last execution scope
     * @return the computed {@link TestPlanStatistics} out of each test-plan-item of each campaign's
     *     iteration
     */
    public TestPlanStatistics findCampaignStatisticsForTCLastExecutionScope(
            long campaignId, List<Long> itpiIds) {

        Query q = currentSession().getNamedQuery("campaign.countStatusesForLastExecutedTC");
        q.setParameter("campaignId", campaignId);
        q.setParameterList("itpiIds", itpiIds);
        List<Object[]> result = q.list();

        LinkedHashMap<ExecutionStatus, Integer> canonicalStatusMap =
                CountOnEnum.fromTuples(result, ExecutionStatus.class)
                        .getStatistics(
                                ExecutionStatus::getCanonicalStatus, ExecutionStatus.getCanonicalStatusSet());

        return new TestPlanStatistics(canonicalStatusMap);
    }

    @Override
    public List<Long> findNonBoundCampaign(Collection<Long> nodeIds, Long milestoneId) {
        Query q = currentSession().getNamedQuery("campaign.findNonBoundCampaign");
        q.setParameterList(NODE_IDS, nodeIds, LongType.INSTANCE);
        q.setParameter(MILESTONE_ID, milestoneId);
        return q.list();
    }

    @Override
    public List<Long> findCampaignIdsHavingMultipleMilestones(List<Long> nodeIds) {
        Query q = currentSession().getNamedQuery("campaign.findCampaignIdsHavingMultipleMilestones");
        q.setParameterList(NODE_IDS, nodeIds, LongType.INSTANCE);
        return q.list();
    }

    @Override
    public Campaign loadContainerForPaste(long id) {
        return entityManager
                .createQuery(
                        "select c from Campaign c left join fetch c.iterations where c.id = :id",
                        Campaign.class)
                .setParameter(ID, id)
                .getSingleResult();
    }

    @Override
    public List<Campaign> loadContainersForPaste(Collection<Long> ids) {
        return entityManager
                .createQuery(
                        "select distinct c from Campaign c left join fetch c.iterations where c.id in :ids",
                        Campaign.class)
                .setParameter(IDS, ids)
                .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
                .getResultList();
    }

    @Override
    public List<Campaign> loadNodeForPaste(Collection<Long> ids) {
        return new EntityGraphQueryBuilder<>(entityManager, Campaign.class, FIND_CAMPAIGNS_BY_IDS)
                .addAttributeNodes(TEST_PLAN, ATTACHMENT_LIST, MILESTONES)
                .addSubGraph(ATTACHMENT_LIST, ATTACHMENTS)
                .addSubgraphToSubgraph(ATTACHMENT_LIST, ATTACHMENTS, CONTENT)
                .executeDistinctList(Map.of(IDS, ids));
    }

    @Override
    public ChildEntityDtoResult loadChildForPaste(
            Collection<Long> ids, int maxResult, int offset, ClipboardPayload clipboardPayload) {
        return getChildEntityDtoForPaste(
                FIND_CHILD_ENTITY_DTO_CAMPAIGNS_BY_IDS,
                ids,
                maxResult,
                offset,
                clipboardPayload,
                (k, v) -> {
                    if (k.equals(EntityType.ITERATION)) {
                        iterationDaoProvider.get().loadNodeForPaste(v);
                    }
                });
    }
}
