/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.bdd.robot;

import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.KEYWORD_SECTION_TITLE;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.assignment;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.scalarVariable;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.FOUR_SPACES;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.MultiLineStringBuilder;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.formatDocumentationSettingLines;

import java.util.Arrays;
import java.util.List;
import org.squashtest.tm.domain.testcase.KeywordTestCase;
import org.squashtest.tm.service.internal.testcase.bdd.TextGridFormatter;
import org.squashtest.tm.service.internal.testcase.bdd.robot.keywords.RetrieveDatasetKeywordBuilder;
import org.squashtest.tm.service.internal.testcase.bdd.robot.keywords.RetrieveDatatablesKeywordBuilder;
import org.squashtest.tm.service.internal.testcase.bdd.robot.keywords.RetrieveDocstringsKeywordBuilder;

public final class KeywordSectionBuilder {
    private static final String DOLLAR_PARAM_FORMAT = "$%s";
    private static final String IS_NOT_NONE_FORMAT = "%s is not None";
    private static final String IF_KEYWORD = "IF";
    private static final String END_KEYWORD = "END";
    private static final String RUN_KEYWORD_KEYWORD = "Run Keyword";
    private static final String GET_VARIABLE_VALUE_KEYWORD = "Get Variable Value";

    private static final String TEST_SETUP = "TEST_SETUP";
    private static final String TEST_SETUP_VALUE = "TEST_SETUP_VALUE";
    private static final String TEST_SPECIFIC_SETUP_FORMAT = "TEST_%s_SETUP";

    private static final String TEST_TEARDOWN = "TEST_TEARDOWN";
    private static final String TEST_TEARDOWN_VALUE = "TEST_TEARDOWN_VALUE";
    private static final String TEST_SPECIFIC_TEARDOWN_FORMAT = "TEST_%s_TEARDOWN";

    private static final List<String> SETUP_DOCUMENTATION_FORMATS =
            Arrays.asList(
                    "test setup",
                    "You can define the %1$s variable with a keyword for setting up all your tests.",
                    "You can define the %2$s variable with a keyword for setting up this specific test.",
                    "If both are defined, %2$s will be run after %1$s.");

    private static final List<String> TEARDOWN_DOCUMENTATION_FORMATS =
            Arrays.asList(
                    "test teardown",
                    "You can define the %1$s variable with a keyword for tearing down all your tests.",
                    "You can define the %2$s variable with a keyword for tearing down this specific test.",
                    "If both are defined, %1$s will be run after %2$s.");

    private KeywordSectionBuilder() {
        throw new UnsupportedOperationException("This class is not meant to be instantiated.");
    }

    public static String buildKeywordsSection(KeywordTestCase keywordTestCase) {
        final Long testCaseId = keywordTestCase.getId();
        final SectionBuilderHelpers.MultiLineStringBuilder stringBuilder = new MultiLineStringBuilder();

        stringBuilder
                .appendLine(KEYWORD_SECTION_TITLE)
                .append(buildSetupDefinition(testCaseId))
                .append(buildTeardownDefinition(testCaseId))
                .append(RetrieveDatasetKeywordBuilder.buildRetrieveDataset(keywordTestCase))
                .append(RetrieveDatatablesKeywordBuilder.buildRetrieveDatatables(keywordTestCase))
                .append(RetrieveDocstringsKeywordBuilder.buildRetrieveDocstrings(keywordTestCase));

        return stringBuilder.toString();
    }

    private static String buildSetupDefinition(Long testCaseId) {
        final MultiLineStringBuilder stringBuilder =
                new MultiLineStringBuilder().appendLine(SectionBuilderHelpers.TEST_SETUP);

        final String testSpecificSetup = String.format(TEST_SPECIFIC_SETUP_FORMAT, testCaseId);
        final String testSpecificSetupValue = String.format("%s_VALUE", testSpecificSetup);

        appendSetupDocumentation(stringBuilder, testSpecificSetup);
        appendSetupValues(stringBuilder, testSpecificSetup, testSpecificSetupValue);
        appendSetupCalls(stringBuilder, testSpecificSetup, testSpecificSetupValue);

        return stringBuilder.appendNewLine().toString();
    }

    private static void appendSetupDocumentation(
            MultiLineStringBuilder stringBuilder, String testSpecificSetup) {
        stringBuilder
                .append(
                        formatDocumentationSettingLines(
                                SETUP_DOCUMENTATION_FORMATS.stream()
                                        .map(
                                                f ->
                                                        String.format(
                                                                f, scalarVariable(TEST_SETUP), scalarVariable(testSpecificSetup)))
                                        .toList()))
                .appendNewLine();
    }

    private static void appendSetupValues(
            MultiLineStringBuilder stringBuilder,
            String testSpecificSetup,
            String testSpecificSetupValue) {
        stringBuilder.append(
                new TextGridFormatter()
                        .addRow(
                                assignment(scalarVariable(TEST_SETUP_VALUE)),
                                GET_VARIABLE_VALUE_KEYWORD,
                                scalarVariable(TEST_SETUP))
                        .addRow(
                                assignment(scalarVariable(testSpecificSetupValue)),
                                GET_VARIABLE_VALUE_KEYWORD,
                                scalarVariable(testSpecificSetup))
                        .format(TextGridFormatter.withRowPrefix(FOUR_SPACES)));
    }

    private static void appendSetupCalls(
            MultiLineStringBuilder stringBuilder,
            String testSpecificSetup,
            String testSpecificSetupValue) {
        appendRunKeywordIfBlock(stringBuilder, TEST_SETUP_VALUE, TEST_SETUP);
        appendRunKeywordIfBlock(stringBuilder, testSpecificSetupValue, testSpecificSetup);
    }

    private static String buildTeardownDefinition(Long testCaseId) {
        final MultiLineStringBuilder stringBuilder =
                new MultiLineStringBuilder().appendLine(SectionBuilderHelpers.TEST_TEARDOWN);

        final String testSpecificTeardown = String.format(TEST_SPECIFIC_TEARDOWN_FORMAT, testCaseId);
        final String testSpecificTeardownValue = String.format("%s_VALUE", testSpecificTeardown);

        appendTeardownDocumentation(stringBuilder, testSpecificTeardown);
        appendTeardownValues(stringBuilder, testSpecificTeardown, testSpecificTeardownValue);
        appendTeardownCalls(stringBuilder, testSpecificTeardown, testSpecificTeardownValue);

        return stringBuilder.toString();
    }

    private static void appendTeardownDocumentation(
            MultiLineStringBuilder stringBuilder, String formattedTestSpecificTeardown) {
        stringBuilder
                .append(
                        formatDocumentationSettingLines(
                                TEARDOWN_DOCUMENTATION_FORMATS.stream()
                                        .map(
                                                f ->
                                                        String.format(
                                                                f,
                                                                scalarVariable(TEST_TEARDOWN),
                                                                scalarVariable(formattedTestSpecificTeardown)))
                                        .toList()))
                .appendNewLine();
    }

    private static void appendTeardownValues(
            MultiLineStringBuilder stringBuilder,
            String testSpecificTeardown,
            String testSpecificTeardownValue) {
        stringBuilder.append(
                new TextGridFormatter()
                        .addRow(
                                assignment(scalarVariable(testSpecificTeardownValue)),
                                GET_VARIABLE_VALUE_KEYWORD,
                                scalarVariable(testSpecificTeardown))
                        .addRow(
                                assignment(scalarVariable(TEST_TEARDOWN_VALUE)),
                                GET_VARIABLE_VALUE_KEYWORD,
                                scalarVariable(TEST_TEARDOWN))
                        .format(TextGridFormatter.withRowPrefix(FOUR_SPACES)));
    }

    private static void appendTeardownCalls(
            MultiLineStringBuilder stringBuilder,
            String testSpecificTeardown,
            String testSpecificTeardownValue) {
        appendRunKeywordIfBlock(stringBuilder, testSpecificTeardownValue, testSpecificTeardown);
        appendRunKeywordIfBlock(stringBuilder, TEST_TEARDOWN_VALUE, TEST_TEARDOWN);
    }

    private static void appendRunKeywordIfBlock(
            MultiLineStringBuilder stringBuilder, String condition, String keywordToRun) {
        stringBuilder
                .appendLine(
                        FOUR_SPACES
                                + IF_KEYWORD
                                + FOUR_SPACES
                                + String.format(IS_NOT_NONE_FORMAT, String.format(DOLLAR_PARAM_FORMAT, condition)))
                .appendLine(
                        FOUR_SPACES
                                + FOUR_SPACES
                                + RUN_KEYWORD_KEYWORD
                                + FOUR_SPACES
                                + scalarVariable(keywordToRun))
                .appendLine(FOUR_SPACES + END_KEYWORD);
    }
}
