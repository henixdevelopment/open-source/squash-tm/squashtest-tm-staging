/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase;

import java.util.Objects;
import org.squashtest.tm.domain.infolist.InfoList;
import org.squashtest.tm.domain.infolist.InfoListItem;
import org.squashtest.tm.domain.infolist.ListItemReference;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNodeVisitor;
import org.squashtest.tm.exception.InconsistentInfoListItemException;

public final class NatureTypeChainFixer implements TestCaseLibraryNodeVisitor {

    @Override
    public void visit(TestCaseFolder visited) {
        for (TestCaseLibraryNode node : visited.getContent()) {
            node.accept(this);
        }
    }

    @Override
    public void visit(TestCase visited) {
        replaceInfoListReferences(visited);
    }

    public static void fix(TestCase testCase) {
        replaceInfoListReferences(testCase);
    }

    public void fix(TestCaseFolder folder) {
        for (TestCaseLibraryNode node : folder.getContent()) {
            node.accept(this);
        }
    }

    /*
     * 12/04/16 : about Nature and Type, and ListItemReferences :
     *
     * cannot use services
     * 	- infoListItemService.isNatureConsistent
     * 	- infoListItemService.isTypeConsistent
     *  - infoListItemService.findReference
     *
     * anymore because this would trigger here an autoflush / persist
     * before we have a chance to replace the ListItemReference by actual entities
     */
    private static void replaceInfoListReferences(TestCase testCase) {
        replaceNatureInfoListReference(testCase);
        replaceTypeInfoListReference(testCase);
    }

    private static void replaceNatureInfoListReference(TestCase testCase) {
        InfoListItem nature = testCase.getNature();
        InfoList projectNatures = testCase.getProject().getTestCaseNatures();

        if (nature == null) {
            testCase.setNature(projectNatures.getDefaultItem());
        } else {
            // in case the item used here is merely a reference we need to
            // replace it with a persistent instance
            if (nature instanceof ListItemReference) {
                InfoListItem persistedItem = projectNatures.getItem(nature);
                if (Objects.nonNull(persistedItem)) {
                    testCase.setNature(projectNatures.getItem(nature));
                }
            }

            // validate the code
            if (!projectNatures.contains(nature)) {
                throw new InconsistentInfoListItemException("nature", nature.getCode());
            }
        }
    }

    private static void replaceTypeInfoListReference(TestCase testCase) {
        InfoListItem type = testCase.getType();
        InfoList projectTypes = testCase.getProject().getTestCaseTypes();

        if (type == null) {
            testCase.setType(projectTypes.getDefaultItem());
            return;
        }

        if (type instanceof ListItemReference) {
            InfoListItem persistedItem = projectTypes.getItem(type);
            if (Objects.nonNull(persistedItem)) {
                testCase.setType(projectTypes.getItem(type));
                return;
            }
        }

        if (!projectTypes.contains(type)) {
            throw new InconsistentInfoListItemException("type", type.getCode());
        }
    }
}
