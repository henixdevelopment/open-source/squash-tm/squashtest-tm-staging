/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.squashtest.tm.domain.environmentvariable.EVBindableEntity;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableBinding;

public interface EnvironmentVariableBindingDao
        extends JpaRepository<EnvironmentVariableBinding, Long> {

    @Query(
            "select evb from EnvironmentVariableBinding evb where evb.environmentVariable.id = :environmentVariableId"
                    + " and evb.entityId = :entityId and evb.entityType = :entityType")
    EnvironmentVariableBinding findByEntityIdTypeAndEvId(
            @Param("entityId") Long entityId,
            @Param("entityType") EVBindableEntity entityType,
            @Param("environmentVariableId") Long environmentVariableId);

    List<EnvironmentVariableBinding> findAllByEnvironmentVariable_Id(Long environmentVariableId);

    List<EnvironmentVariableBinding> findAllByEntityIdAndEntityType(
            Long entityId, EVBindableEntity entityType);

    @Query(
            "select evb.id from EnvironmentVariableBinding evb where evb.environmentVariable.id = :environmentVariableId ")
    List<Long> findAllBindingIdsByEnvironmentVariableId(
            @Param("environmentVariableId") Long environmentVariableId);

    @Query(
            "select evb from EnvironmentVariableBinding evb where evb.environmentVariable.id = :environmentVariableId"
                    + " and evb.value in :values")
    List<EnvironmentVariableBinding> findAllByEvIdAndValues(
            @Param("environmentVariableId") Long environmentVariableId,
            @Param("values") List<String> values);
}
