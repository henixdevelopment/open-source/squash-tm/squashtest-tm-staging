/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import org.squashtest.tm.api.workspace.WorkspaceType;
import org.squashtest.tm.core.foundation.annotation.CleanHtml;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.domain.customfield.BindableEntity;

public class ProjectDto {

    private Long id;
    private String name;
    private String label;
    private Long testCaseNatureId;
    private Long testCaseTypeId;
    private Long requirementCategoryId;
    private Boolean allowAutomationWorkflow;
    private Map<BindableEntity, Set<CufBindingDto>> customFieldBindings;
    private Map<String, Collection<String>> permissions;
    private BugTrackerBindingDto bugTrackerBinding;
    private List<MilestoneBindingDto> milestoneBindings = new ArrayList<>();
    private Long taServerId;
    private String automationWorkflowType;
    private List<String> disabledExecutionStatus = new ArrayList<>();
    private Long attachmentListId;
    private Date createdOn;
    private String createdBy;
    private Date lastModifiedOn;
    private String lastModifiedBy;
    private String linkedTemplate;
    private Long linkedTemplateId;
    private Integer taProjectCount;
    private boolean useTreeStructureInScmRepo;
    private String description;
    private Long scmRepositoryId;
    private boolean template;
    private String bddScriptLanguage;
    private String bddImplementationTechnology;
    private List<KeywordDto> keywords = new ArrayList<>();
    private boolean allowTcModifDuringExec;
    private Map<WorkspaceType, Set<String>> activatedPlugins;
    private Integer automatedSuitesLifetime;
    private Long aiServerId;
    private Long bugtrackerId;

    private List<NamedReference> allProjectBoundToTemplate;

    public ProjectDto() {
        this.customFieldBindings = initBindableEntityMap();
        this.activatedPlugins = initWorkspaceTypeMap();
    }

    public static Map<BindableEntity, Set<CufBindingDto>> initBindableEntityMap() {
        Map<BindableEntity, Set<CufBindingDto>> customFieldBindings = new HashMap<>();
        EnumSet<BindableEntity> bindableEntities = EnumSet.allOf(BindableEntity.class);
        bindableEntities.forEach(entity -> customFieldBindings.put(entity, new LinkedHashSet<>()));
        return customFieldBindings;
    }

    public static Map<WorkspaceType, Set<String>> initWorkspaceTypeMap() {
        Map<WorkspaceType, Set<String>> activatedPlugins = new HashMap<>();
        EnumSet<WorkspaceType> workspaceTypes =
                EnumSet.of(
                        WorkspaceType.TEST_CASE_WORKSPACE,
                        WorkspaceType.CAMPAIGN_WORKSPACE,
                        WorkspaceType.REQUIREMENT_WORKSPACE);
        workspaceTypes.forEach(workspace -> activatedPlugins.put(workspace, new LinkedHashSet<>()));
        return activatedPlugins;
    }

    public void addBinding(CufBindingDto binding) {
        Set<CufBindingDto> bindings = this.customFieldBindings.get(binding.getBindableEntity());
        Optional<CufBindingDto> optional =
                bindings.stream().filter(candidate -> candidate.equals(binding)).findFirst();
        if (optional.isPresent()) {
            optional.get().addRenderingLocations(binding.getRenderingLocations());
        } else {
            bindings.add(binding);
        }
    }

    public void addActivatedPlugin(String pluginId, WorkspaceType workspaceType) {
        Set<String> allActivatedPlugins = this.activatedPlugins.get(workspaceType);
        allActivatedPlugins.add(pluginId);
    }

    public Long getAttachmentListId() {
        return attachmentListId;
    }

    public void setAttachmentListId(Long attachmentListId) {
        this.attachmentListId = attachmentListId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Long getTestCaseNatureId() {
        return testCaseNatureId;
    }

    public void setTestCaseNatureId(Long testCaseNatureId) {
        this.testCaseNatureId = testCaseNatureId;
    }

    public Long getTestCaseTypeId() {
        return testCaseTypeId;
    }

    public void setTestCaseTypeId(Long testCaseTypeId) {
        this.testCaseTypeId = testCaseTypeId;
    }

    public Long getRequirementCategoryId() {
        return requirementCategoryId;
    }

    public void setRequirementCategoryId(Long requirementCategoryId) {
        this.requirementCategoryId = requirementCategoryId;
    }

    public Boolean getAllowAutomationWorkflow() {
        return allowAutomationWorkflow;
    }

    public void setAllowAutomationWorkflow(Boolean allowAutomationWorkflow) {
        this.allowAutomationWorkflow = allowAutomationWorkflow;
    }

    public Map<BindableEntity, Set<CufBindingDto>> getCustomFieldBindings() {
        return customFieldBindings;
    }

    public void setCustomFieldBindings(Map<BindableEntity, Set<CufBindingDto>> customFieldBindings) {
        this.customFieldBindings = customFieldBindings;
    }

    public Map<String, Collection<String>> getPermissions() {
        return permissions;
    }

    public void setPermissions(Map<String, Collection<String>> permissions) {
        this.permissions = permissions;
    }

    public BugTrackerBindingDto getBugTrackerBinding() {
        return bugTrackerBinding;
    }

    public void setBugTrackerBinding(BugTrackerBindingDto bugTrackerBinding) {
        this.bugTrackerBinding = bugTrackerBinding;
    }

    public List<MilestoneBindingDto> getMilestoneBindings() {
        return milestoneBindings;
    }

    public void setMilestoneBindings(List<MilestoneBindingDto> milestoneBindings) {
        this.milestoneBindings = milestoneBindings;
    }

    public Long getTaServerId() {
        return taServerId;
    }

    public void setTaServerId(Long taServerId) {
        this.taServerId = taServerId;
    }

    public String getAutomationWorkflowType() {
        return automationWorkflowType;
    }

    public void setAutomationWorkflowType(String automationWorflowType) {
        this.automationWorkflowType = automationWorflowType;
    }

    public List<String> getDisabledExecutionStatus() {
        return disabledExecutionStatus;
    }

    public void setDisabledExecutionStatus(List<String> disabledExecutionStatus) {
        this.disabledExecutionStatus = disabledExecutionStatus;
    }

    public Integer getTaProjectCount() {
        return taProjectCount;
    }

    public void setTaProjectCount(Integer taProjectCount) {
        this.taProjectCount = taProjectCount;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getLastModifiedOn() {
        return lastModifiedOn;
    }

    public void setLastModifiedOn(Date lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getLinkedTemplate() {
        return linkedTemplate;
    }

    public void setLinkedTemplate(String linkedTemplate) {
        this.linkedTemplate = linkedTemplate;
    }

    public Long getLinkedTemplateId() {
        return linkedTemplateId;
    }

    public void setLinkedTemplateId(Long linkedTemplateId) {
        this.linkedTemplateId = linkedTemplateId;
    }

    @CleanHtml
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getScmRepositoryId() {
        return scmRepositoryId;
    }

    public void setScmRepositoryId(Long scmRepositoryId) {
        this.scmRepositoryId = scmRepositoryId;
    }

    public boolean isUseTreeStructureInScmRepo() {
        return useTreeStructureInScmRepo;
    }

    public void setUseTreeStructureInScmRepo(boolean useTreeStructureInScmRepo) {
        this.useTreeStructureInScmRepo = useTreeStructureInScmRepo;
    }

    public boolean isTemplate() {
        return template;
    }

    public void setTemplate(boolean template) {
        this.template = template;
    }

    public String getBddScriptLanguage() {
        return bddScriptLanguage;
    }

    public void setBddScriptLanguage(String bddScriptLanguage) {
        this.bddScriptLanguage = bddScriptLanguage;
    }

    public String getBddImplementationTechnology() {
        return bddImplementationTechnology;
    }

    public void setBddImplementationTechnology(String bddImplementationTechnology) {
        this.bddImplementationTechnology = bddImplementationTechnology;
    }

    public List<KeywordDto> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<KeywordDto> keywords) {
        this.keywords = keywords;
    }

    public boolean isAllowTcModifDuringExec() {
        return allowTcModifDuringExec;
    }

    public void setAllowTcModifDuringExec(boolean allowTcModifDuringExec) {
        this.allowTcModifDuringExec = allowTcModifDuringExec;
    }

    public Map<WorkspaceType, Set<String>> getActivatedPlugins() {
        return activatedPlugins;
    }

    public void setActivatedPlugins(Map<WorkspaceType, Set<String>> activatedPlugins) {
        this.activatedPlugins = activatedPlugins;
    }

    public Integer getAutomatedSuitesLifetime() {
        return automatedSuitesLifetime;
    }

    public void setAutomatedSuitesLifetime(Integer automatedSuitesLifetime) {
        this.automatedSuitesLifetime = automatedSuitesLifetime;
    }

    public List<NamedReference> getAllProjectBoundToTemplate() {
        return allProjectBoundToTemplate;
    }

    public void setAllProjectBoundToTemplate(List<NamedReference> allProjectBoundToTemplate) {
        this.allProjectBoundToTemplate = allProjectBoundToTemplate;
    }

    public Long getAiServerId() {
        return aiServerId;
    }

    public void setAiServerId(Long aiServerId) {
        this.aiServerId = aiServerId;
    }

    public Long getBugtrackerId() {
        return bugtrackerId;
    }

    public void setBugtrackerId(Long bugtrackerId) {
        this.bugtrackerId = bugtrackerId;
    }
}
