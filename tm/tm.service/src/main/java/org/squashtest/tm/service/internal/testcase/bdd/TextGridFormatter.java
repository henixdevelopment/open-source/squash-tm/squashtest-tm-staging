/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.bdd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.lang.Nullable;

public class TextGridFormatter {

    private static final int MIN_COLUMN_SPACING = 4;
    private static final String LINE_SEPARATOR = "\n";

    private final List<Row> rows = new ArrayList<>();

    public boolean isEmpty() {
        return rows.isEmpty();
    }

    public TextGridFormatter addRow(String... cells) {
        addRow(Arrays.asList(cells));
        return this;
    }

    public TextGridFormatter addRow(List<String> cells) {
        rows.add(new Row(cells));
        return this;
    }

    public TextGridFormatter addRows(List<List<String>> rowsToAdd) {
        rows.addAll(rowsToAdd.stream().map(Row::new).toList());
        return this;
    }

    public String format() {
        return format(null);
    }

    public String format(@Nullable FormatOptions formatOptions) {
        final List<Integer> columnWidths = computeColumnWidths();
        return processRows(formatOptions, columnWidths);
    }

    private List<Integer> computeColumnWidths() {
        final List<Integer> columnWidths = new ArrayList<>();

        for (Row row : rows) {
            for (int column = 0; column < row.cells.size(); ++column) {
                while (columnWidths.size() <= column) {
                    columnWidths.add(0);
                }

                final int length = row.cells.get(column).length();
                final int max = Math.max(length, columnWidths.get(column));

                columnWidths.set(column, max);
            }
        }
        return columnWidths;
    }

    private String processRows(FormatOptions formatOptions, List<Integer> columnWidths) {
        final String rowPrefix = formatOptions == null ? "" : formatOptions.rowPrefix;
        final StringBuilder sb = new StringBuilder();

        for (Row row : rows) {
            for (int column = 0; column < row.cells.size(); ++column) {
                sb.append(processCell(columnWidths, rowPrefix, row, column));
            }

            sb.append(LINE_SEPARATOR);
        }

        return sb.toString();
    }

    private String processCell(List<Integer> columnWidths, String rowPrefix, Row row, int column) {
        final int minLength = columnWidths.get(column) + MIN_COLUMN_SPACING;
        String textContent = row.cells.get(column);

        if (column < row.cells.size() - 1) {
            textContent = padRight(textContent, minLength);
        }

        if (column == 0 && rowPrefix != null) {
            textContent = rowPrefix + textContent;
        }

        return textContent;
    }

    public static FormatOptions withRowPrefix(String prefix) {
        return new FormatOptions(prefix);
    }

    private String padRight(String input, int minLength) {
        final StringBuilder sb = new StringBuilder(input);

        while (sb.length() < minLength) {
            sb.append(" ");
        }

        return sb.toString();
    }

    public static class Row {
        private final List<String> cells;

        public Row(List<String> cells) {
            this.cells = cells;
        }
    }

    public static final class FormatOptions {
        private String rowPrefix;

        private FormatOptions(String rowPrefix) {
            this.rowPrefix = rowPrefix;
        }

        public String getRowPrefix() {
            return rowPrefix;
        }

        public void setRowPrefix(String rowPrefix) {
            this.rowPrefix = rowPrefix;
        }
    }
}
