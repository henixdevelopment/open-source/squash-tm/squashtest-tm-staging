/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_EXECUTION_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_ENVIRONMENT_TAG;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;

import javax.inject.Inject;
import org.jooq.AggregateFilterStep;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.dto.DenormalizedEnvironmentTagDto;
import org.squashtest.tm.service.internal.repository.CustomDenormalizedEnvironmentTagDao;

@Repository
public class CustomDenormalizedEnvironmentTagDaoImpl
        implements CustomDenormalizedEnvironmentTagDao {

    private static final String VALUE = "VALUE";

    @Inject DSLContext dslContext;

    @Override
    public DenormalizedEnvironmentTagDto getAllByExecutionId(Long executionId) {

        Condition containTags = DSL.count(DENORMALIZED_ENVIRONMENT_TAG.VALUE).greaterThan(0);
        Condition containTestTechnology =
                DSL.length(AUTOMATED_EXECUTION_EXTENDER.TEST_TECHNOLOGY).greaterThan(0);
        Condition containTagsOrTestTechnology =
                containTestTechnology.or(DENORMALIZED_ENVIRONMENT_TAG.VALUE.isNotNull());

        // tags are alphabetically ordered and test technology comes last
        AggregateFilterStep<String> aggregateTags =
                DSL.listAgg(DENORMALIZED_ENVIRONMENT_TAG.VALUE, ", ")
                        .withinGroupOrderBy(DENORMALIZED_ENVIRONMENT_TAG.VALUE);

        Field<String> withDenormalizedTags =
                DSL.when(
                                containTestTechnology,
                                aggregateTags.concat(DSL.val(", "), AUTOMATED_EXECUTION_EXTENDER.TEST_TECHNOLOGY))
                        .otherwise(aggregateTags);

        return dslContext
                .select(
                        DSL.when(containTags, withDenormalizedTags)
                                .otherwise(AUTOMATED_EXECUTION_EXTENDER.TEST_TECHNOLOGY)
                                .as(VALUE))
                .from(EXECUTION)
                .innerJoin(AUTOMATED_EXECUTION_EXTENDER)
                .on(EXECUTION.EXECUTION_ID.eq(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID))
                .leftJoin(DENORMALIZED_ENVIRONMENT_TAG)
                .on(DENORMALIZED_ENVIRONMENT_TAG.HOLDER_ID.eq(AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID))
                .where(EXECUTION.EXECUTION_ID.eq(executionId).and(containTagsOrTestTechnology))
                .groupBy(EXECUTION.EXECUTION_ID, AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID)
                .fetchOneInto(DenormalizedEnvironmentTagDto.class);
    }
}
