/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.campaign;

import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;

import java.util.List;
import org.jooq.DSLContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.core.foundation.exception.ActionException;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.exception.execution.TestSuiteTestPlanHasDeletedTestCaseException;
import org.squashtest.tm.service.internal.display.dto.execution.TestPlanResume;
import org.squashtest.tm.service.internal.display.dto.execution.TestPlanResume.TestSuiteTestPlanResume;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.campaign.TestSuiteTestPlanGrid;
import org.squashtest.tm.service.internal.repository.IterationTestPlanDao;
import org.squashtest.tm.service.internal.repository.TestSuiteDao;
import org.squashtest.tm.service.internal.repository.display.TestSuiteDisplayDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.user.UserAccountService;

@Service("squashtest.tm.service.TestSuiteExecutionProcessingService")
@Transactional
public class TestSuiteExecutionProcessingServiceImpl
        extends AbstractTestPlanExecutionProcessingService<TestSuite, TestSuiteTestPlanResume> {

    private static final String CAN_EXECUTE_BY_TESTSUITE_ID =
            "hasPermission(#testSuiteId, 'org.squashtest.tm.domain.campaign.TestSuite', 'EXECUTE')"
                    + OR_HAS_ROLE_ADMIN;

    private final TestSuiteDao suiteDao;
    private final TestSuiteDisplayDao testSuiteDisplayDao;

    TestSuiteExecutionProcessingServiceImpl(
            CampaignNodeDeletionHandler campaignDeletionHandler,
            IterationTestPlanManager testPlanManager,
            UserAccountService userService,
            PermissionEvaluationService permissionEvaluationService,
            IterationTestPlanDao iterationTestPlanDao,
            DSLContext dslContext,
            TestSuiteDao suiteDao,
            TestSuiteDisplayDao testSuiteDisplayDao) {
        super(
                campaignDeletionHandler,
                testPlanManager,
                userService,
                permissionEvaluationService,
                iterationTestPlanDao,
                dslContext);
        this.suiteDao = suiteDao;
        this.testSuiteDisplayDao = testSuiteDisplayDao;
    }

    @Override
    @PreAuthorize(CAN_EXECUTE_BY_TESTSUITE_ID)
    public TestSuiteTestPlanResume startResume(long testSuiteId) {
        return commonStartResume(testSuiteId);
    }

    @Override
    @PreAuthorize(CAN_EXECUTE_BY_TESTSUITE_ID)
    public TestSuiteTestPlanResume relaunch(long testSuiteId) {
        return super.relaunch(testSuiteId);
    }

    @Override
    @PreAuthorize(CAN_EXECUTE_BY_TESTSUITE_ID)
    public TestSuiteTestPlanResume startResumeNextExecution(long testSuiteId, long testPlanItemId) {
        return super.startResumeNextExecution(testSuiteId, testPlanItemId);
    }

    /**
     * @see
     *     org.squashtest.tm.service.campaign.TestPlanExecutionProcessingService#deleteAllExecutions(long)
     */
    @Override
    @PreAuthorize(CAN_EXECUTE_BY_TESTSUITE_ID)
    public void deleteAllExecutions(long testSuiteId) {
        super.deleteAllExecutions(testSuiteId);
    }

    @Override
    TestSuite getTestPlanOwner(long testSuiteId) {
        return suiteDao.loadForExecutionResume(testSuiteId);
    }

    @Override
    TestSuite createTransientTestPlanOwnerWithFilteredTestPlan(
            long testSuiteId, List<IterationTestPlanItem> filteredTestPLan) {
        TestSuite testSuite = getTestPlanOwner(testSuiteId);
        TestSuite copyTestSuite = testSuite.createCopy();
        copyTestSuite.getTestPlan().clear();
        copyTestSuite.getTestPlan().addAll(filteredTestPLan);
        return copyTestSuite;
    }

    @Override
    List<IterationTestPlanItem> getTestPlan(TestSuite suite) {
        return suite.getTestPlan();
    }

    @Override
    IterationTestPlanItem findFirstExecutableTestPlanItem(String testerLogin, TestSuite suite) {
        return suite.findFirstExecutableTestPlanItem(testerLogin);
    }

    @Override
    IterationTestPlanItem findNextExecutableTestPlanItem(
            TestSuite suite, long testPlanItemId, String testerLogin) {
        return suite.findNextExecutableTestPlanItem(testPlanItemId, testerLogin);
    }

    @Override
    IterationTestPlanItem findNextExecutableTestPlanItem(TestSuite suite, long testPlanItemId) {
        return suite.findNextExecutableTestPlanItem(testPlanItemId);
    }

    @Override
    List<IterationTestPlanItem> getFilteredTestPlan(long testSuiteId, List<GridFilterValue> filters) {
        TestSuiteTestPlanGrid testPlanGrid =
                new TestSuiteTestPlanGrid(testSuiteId, null, testSuiteDisplayDao);
        GridRequest gridRequest = prepareNonPaginatedGridRequest(filters);
        GridResponse gridResponse = testPlanGrid.getRows(gridRequest, this.dslContext);
        List<Long> itemIds = extractItemIdsFromGridResponse(gridResponse);
        return iterationTestPlanDao.findAllByIdIn(itemIds);
    }

    @Override
    @PreAuthorize(CAN_EXECUTE_BY_TESTSUITE_ID)
    public TestPlanResume resumeNextExecutionOfFilteredTestPlan(
            long testSuiteId, long testPlanItemId, List<Long> partialTestPlanItemIds) {
        return super.resumeNextExecutionOfFilteredTestPlan(
                testSuiteId, testPlanItemId, partialTestPlanItemIds);
    }

    @Override
    ActionException getTestPlanHasDeletedTestCaseException() {
        return new TestSuiteTestPlanHasDeletedTestCaseException();
    }

    @Override
    @PreAuthorize(CAN_EXECUTE_BY_TESTSUITE_ID)
    public TestPlanResume resumeWithFilteredTestPlan(
            long testSuiteId, List<GridFilterValue> filters) {
        return super.resumeWithFilteredTestPlan(testSuiteId, filters);
    }

    @Override
    @PreAuthorize(CAN_EXECUTE_BY_TESTSUITE_ID)
    public TestPlanResume relaunchFilteredTestPlan(long testSuiteId, List<GridFilterValue> filters) {
        return super.relaunchFilteredTestPlan(testSuiteId, filters);
    }

    @Override
    protected TestPlanResume createNewTestPlanResume(
            long testPlanOwnerId,
            Execution execution,
            boolean hasNextTestCase,
            List<Long> partialTestPlanItemIds) {
        return new TestSuiteTestPlanResume(
                testPlanOwnerId, execution, hasNextTestCase, partialTestPlanItemIds);
    }

    @Override
    protected TestSuiteTestPlanResume createNewTestPlanResume(
            long testPlanOwnerId, Execution execution, boolean hasNextTestCase) {
        return new TestSuiteTestPlanResume(testPlanOwnerId, execution, hasNextTestCase);
    }

    @Override
    public boolean hasDeletedTestCaseInTestPlan(long testPlanOwnerId) {
        return suiteDao.hasDeletedTestCaseInTestPlan(testPlanOwnerId);
    }
}
