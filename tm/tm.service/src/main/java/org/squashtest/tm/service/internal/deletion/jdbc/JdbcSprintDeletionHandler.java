/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion.jdbc;

import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT_REQUIREMENT_SYNC_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.TEST_PLAN_ITEM;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.EntityManager;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Record3;
import org.jooq.SelectConditionStep;
import org.jooq.TableOnConditionStep;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.service.internal.api.repository.HibernateSessionClearing;
import org.squashtest.tm.service.internal.attachment.AttachmentRepository;

/**
 * Delete sprints using fast and optimized sql, allowing to avoid the whole 'hibernate web' used as
 * domain. With a proper domain and some simple database cascade this class shouldn't exist...
 */
public class JdbcSprintDeletionHandler extends AbstractSprintReqVersionDeletionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(JdbcSprintDeletionHandler.class);

    private final Set<Long> sprintIds;

    public JdbcSprintDeletionHandler(
            Collection<Long> sprintIds,
            DSLContext dslContext,
            EntityManager entityManager,
            AttachmentRepository attachmentRepository,
            JdbcBatchReorderHelper reorderHelper,
            String operationId) {
        super(dslContext, entityManager, attachmentRepository, reorderHelper, operationId);
        this.sprintIds = new HashSet<>(sprintIds);
    }

    @Override
    protected TableOnConditionStep<Record> joinToExecution() {
        return SPRINT
                .join(SPRINT_REQ_VERSION)
                .on(SPRINT.CLN_ID.eq(SPRINT_REQ_VERSION.SPRINT_ID))
                .join(TEST_PLAN_ITEM)
                .on(TEST_PLAN_ITEM.TEST_PLAN_ID.eq(SPRINT_REQ_VERSION.TEST_PLAN_ID))
                .join(EXECUTION)
                .on(EXECUTION.TEST_PLAN_ITEM_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID));
    }

    @Override
    protected Condition getPredicate() {
        return SPRINT.CLN_ID.in(sprintIds);
    }

    @HibernateSessionClearing
    public void deleteSprints() {
        logStartProcess();
        clearPersistenceContext();
        storeEntitiesToDeleteIntoWorkingTable();
        performDeletions();
        cleanWorkingTable();
        logEndProcess();
    }

    private void logEndProcess() {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info(
                    String.format(
                            "Deleted Sprints %s. Time elapsed %s",
                            sprintIds, startDate.until(LocalDateTime.now(), ChronoUnit.MILLIS)));
        }
    }

    private void performDeletions() {
        performExecutionDeletions();
        deleteSprintVersionSyncExtenders();
        deleteSprintReqVersionTestPlansItems();
        deleteSprintReqVersions();
        deleteSprintReqVersionTestPlans();
        deleteCustomFieldValues();
        deleteAttachmentLists();
        deleteAttachmentContents();
        deleteSprintEntities();
    }

    private void deleteSprintEntities() {
        workingTables.delete(SPRINT.CLN_ID, SPRINT.CLN_ID);
        logDelete(SPRINT);
    }

    private void logStartProcess() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(
                    String.format(
                            "Init deletion process of sprints %s. Operation:  %s", sprintIds, operationId));
        }
    }

    private void storeEntitiesToDeleteIntoWorkingTable() {
        storeExecutionsToDeleteIntoWorkingTable();
        addSprintReqVersions();
        addSprintReqVersionsTestPlans();
        addSprintReqVersionsTestPlanItems();
        addSprintRequirementSyncExtenders();
        addCustomFieldValues();
        addAttachmentList();
        addSprintEntities();
        logReferenceEntitiesComplete();
    }

    private void addSprintEntities() {
        workingTables.addEntity(
                SPRINT.CLN_ID,
                () -> makeSelectClause(SPRINT.CLN_ID).from(SPRINT).where(SPRINT.CLN_ID.in(sprintIds)));
    }

    @Override
    protected SelectConditionStep<Record3<Long, String, String>>
            selectSprintRequirementSyncExtenders() {
        return makeSelectClause(SPRINT_REQUIREMENT_SYNC_EXTENDER.SPRINT_REQ_SYNC_ID)
                .from(SPRINT_REQUIREMENT_SYNC_EXTENDER)
                .innerJoin(SPRINT_REQ_VERSION)
                .on(
                        SPRINT_REQUIREMENT_SYNC_EXTENDER.SPRINT_REQ_VERSION_ID.eq(
                                SPRINT_REQ_VERSION.SPRINT_REQ_VERSION_ID))
                .where(SPRINT_REQ_VERSION.SPRINT_ID.in(sprintIds));
    }

    @Override
    protected Condition getSprintReqVersionPredicate() {
        return SPRINT_REQ_VERSION.SPRINT_ID.in(sprintIds);
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }
}
