/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.jooq.impl.DSL.field;
import static org.squashtest.tm.jooq.domain.Tables.ENVIRONMENT_VARIABLE;
import static org.squashtest.tm.jooq.domain.Tables.ENVIRONMENT_VARIABLE_BINDING;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;

import java.util.List;
import javax.inject.Inject;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.environmentvariable.EVBindableEntity;
import org.squashtest.tm.domain.environmentvariable.EVInputType;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariable;
import org.squashtest.tm.jooq.domain.tables.EnvironmentVariableBinding;
import org.squashtest.tm.service.internal.display.dto.BoundEnvironmentVariableDto;
import org.squashtest.tm.service.internal.repository.CustomEnvironmentVariableBindingDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

@Repository
public class CustomEnvironmentVariableBindingDaoImpl
        implements CustomEnvironmentVariableBindingDao {

    @Inject private DSLContext dsl;

    @Override
    public List<Long> findAllBindingIdsByEvIdsAndEntityIdType(
            List<Long> evIds, Long entityId, EVBindableEntity entityType) {
        return dsl.select(ENVIRONMENT_VARIABLE_BINDING.EVB_ID)
                .from(ENVIRONMENT_VARIABLE_BINDING)
                .where(
                        ENVIRONMENT_VARIABLE_BINDING
                                .ENTITY_ID
                                .eq(entityId)
                                .and(ENVIRONMENT_VARIABLE_BINDING.ENTITY_TYPE.eq(entityType.name()))
                                .and(ENVIRONMENT_VARIABLE_BINDING.EV_ID.in(evIds)))
                .fetchInto(Long.class);
    }

    @Override
    public List<BoundEnvironmentVariableDto> findAllBoundEnvironmentVariables(
            Long entityId, EVBindableEntity entityType) {

        Field<Boolean> isInterpretedField =
                field(ENVIRONMENT_VARIABLE.INPUT_TYPE.eq(EVInputType.INTERPRETED_TEXT.name()))
                        .as("IS_INTERPRETED");

        return dsl.select(
                        ENVIRONMENT_VARIABLE_BINDING.EV_ID.as("ID"),
                        ENVIRONMENT_VARIABLE.NAME,
                        ENVIRONMENT_VARIABLE.INPUT_TYPE,
                        ENVIRONMENT_VARIABLE_BINDING.EVB_ID.as("BINDING_ID"),
                        ENVIRONMENT_VARIABLE_BINDING.VALUE,
                        ENVIRONMENT_VARIABLE_BINDING.ENTITY_TYPE.as(("EV_BINDABLE_ENTITY")),
                        isInterpretedField)
                .from(ENVIRONMENT_VARIABLE_BINDING)
                .innerJoin(ENVIRONMENT_VARIABLE)
                .on(ENVIRONMENT_VARIABLE.EV_ID.eq(ENVIRONMENT_VARIABLE_BINDING.EV_ID))
                .where(
                        ENVIRONMENT_VARIABLE_BINDING
                                .ENTITY_ID
                                .eq(entityId)
                                .and(ENVIRONMENT_VARIABLE_BINDING.ENTITY_TYPE.eq(entityType.name())))
                .fetchInto(BoundEnvironmentVariableDto.class);
    }

    @Override
    public List<Long> findProjectsLinkedToServerWhereVariableIsNotBound(
            Long serverId, EnvironmentVariable environmentVariable) {
        EnvironmentVariableBinding projectEnvironmentVariableBinding =
                ENVIRONMENT_VARIABLE_BINDING.as("PROJECT_ENVIRONMENT_VARIABLE_BINDING");

        return dsl.select(PROJECT.PROJECT_ID)
                .from(PROJECT)
                .leftJoin(ENVIRONMENT_VARIABLE_BINDING)
                .on(
                        ENVIRONMENT_VARIABLE_BINDING
                                .ENTITY_ID
                                .eq(PROJECT.TA_SERVER_ID)
                                .and(
                                        ENVIRONMENT_VARIABLE_BINDING.ENTITY_TYPE.eq(
                                                EVBindableEntity.TEST_AUTOMATION_SERVER.name()))
                                .and(ENVIRONMENT_VARIABLE_BINDING.EV_ID.eq(environmentVariable.getId())))
                .leftJoin(projectEnvironmentVariableBinding)
                .on(
                        projectEnvironmentVariableBinding
                                .EV_ID
                                .eq(ENVIRONMENT_VARIABLE_BINDING.EV_ID)
                                .and(projectEnvironmentVariableBinding.ENTITY_ID.eq(PROJECT.PROJECT_ID)))
                .and(projectEnvironmentVariableBinding.ENTITY_TYPE.eq(EVBindableEntity.PROJECT.name()))
                .where(
                        PROJECT.TA_SERVER_ID.eq(serverId).and(projectEnvironmentVariableBinding.EV_ID.isNull()))
                .fetchInto(Long.class);
    }

    @Override
    public List<BoundEnvironmentVariableDto> getDefaultBoundEnvironmentVariablesByServerId(
            Long serverId) {
        return dsl.select(
                        ENVIRONMENT_VARIABLE_BINDING.EVB_ID.as("BINDING_ID"),
                        ENVIRONMENT_VARIABLE.NAME,
                        ENVIRONMENT_VARIABLE.EV_ID.as(RequestAliasesConstants.ID),
                        ENVIRONMENT_VARIABLE.INPUT_TYPE,
                        ENVIRONMENT_VARIABLE_BINDING.VALUE,
                        ENVIRONMENT_VARIABLE_BINDING.ENTITY_TYPE.as("EV_BINDABLE_ENTITY"))
                .from(ENVIRONMENT_VARIABLE_BINDING)
                .innerJoin(ENVIRONMENT_VARIABLE)
                .on(ENVIRONMENT_VARIABLE.EV_ID.eq(ENVIRONMENT_VARIABLE_BINDING.EV_ID))
                .where(
                        ENVIRONMENT_VARIABLE_BINDING
                                .ENTITY_ID
                                .eq(serverId)
                                .and(
                                        ENVIRONMENT_VARIABLE_BINDING.ENTITY_TYPE.eq(
                                                EVBindableEntity.TEST_AUTOMATION_SERVER.name())))
                .fetch()
                .into(BoundEnvironmentVariableDto.class);
    }
}
