/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.project;

import static org.jooq.impl.DSL.concat;
import static org.jooq.impl.DSL.val;
import static org.jooq.impl.DSL.when;
import static org.jooq.tools.StringUtils.EMPTY;
import static org.squashtest.tm.jooq.domain.Tables.ACL_CLASS;
import static org.squashtest.tm.jooq.domain.Tables.ACL_GROUP;
import static org.squashtest.tm.jooq.domain.Tables.ACL_OBJECT_IDENTITY;
import static org.squashtest.tm.jooq.domain.Tables.ACL_RESPONSIBILITY_SCOPE_ENTRY;
import static org.squashtest.tm.jooq.domain.Tables.CORE_TEAM;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.service.internal.display.dto.ProjectDataInfo.CANNOT_COERCE_INTO_TEMPLATE_I18N_KEY;
import static org.squashtest.tm.service.internal.display.dto.ProjectDataInfo.COERCE_INTO_TEMPLATE_I18N_KEY;
import static org.squashtest.tm.service.internal.display.dto.ProjectDataInfo.DELETABLE_PROJECT_I18N_KEY;
import static org.squashtest.tm.service.internal.display.dto.ProjectDataInfo.DELETABLE_PROJECT_I18N_KEY_WITH_LINKED_JOBS;
import static org.squashtest.tm.service.internal.display.dto.ProjectDataInfo.NO_DELETABLE_PROJECT_I18N_KEY;
import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.MANAGE_PROJECT_OR_ROLE_ADMIN;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.transaction.Transactional;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.domain.campaign.CampaignLibrary;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.project.AdministrableProject;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.project.ProjectTemplate;
import org.squashtest.tm.domain.projectimporter.PivotFormatImportStatus;
import org.squashtest.tm.service.display.project.ProjectDisplayService;
import org.squashtest.tm.service.display.scm.server.ScmServerDisplayService;
import org.squashtest.tm.service.internal.display.dto.BindMilestoneToProjectDialogData;
import org.squashtest.tm.service.internal.display.dto.MilestoneDto;
import org.squashtest.tm.service.internal.display.dto.PartyProjectPermissionDto;
import org.squashtest.tm.service.internal.display.dto.PivotFormatImportDto;
import org.squashtest.tm.service.internal.display.dto.ProfileDto;
import org.squashtest.tm.service.internal.display.dto.ProjectDataInfo;
import org.squashtest.tm.service.internal.display.dto.ProjectViewDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.administration.ProjectGrid;
import org.squashtest.tm.service.internal.dto.UserDto;
import org.squashtest.tm.service.internal.repository.ProjectDao;
import org.squashtest.tm.service.internal.repository.display.AclDisplayDao;
import org.squashtest.tm.service.internal.repository.display.AiServerDisplayDao;
import org.squashtest.tm.service.internal.repository.display.AttachmentDisplayDao;
import org.squashtest.tm.service.internal.repository.display.BugTrackerDisplayDao;
import org.squashtest.tm.service.internal.repository.display.CustomFieldDao;
import org.squashtest.tm.service.internal.repository.display.InfoListDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.ProfileDisplayDao;
import org.squashtest.tm.service.internal.repository.display.ProjectDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TestAutomationServerDisplayDao;
import org.squashtest.tm.service.license.UltimateLicenseAvailabilityService;
import org.squashtest.tm.service.milestone.MilestoneBindingManagerService;
import org.squashtest.tm.service.project.GenericProjectFinder;
import org.squashtest.tm.service.project.GenericProjectManagerService;
import org.squashtest.tm.service.project.ProjectManagerService;
import org.squashtest.tm.service.projectimporter.pivotimporter.GlobalProjectPivotImporterService;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.templateplugin.TemplateConfigurablePluginBindingService;
import org.squashtest.tm.service.testautomation.TestAutomationProjectManagerService;
import org.squashtest.tm.service.user.UserAccountService;

@Service
@Transactional
public class ProjectDisplayServiceImpl implements ProjectDisplayService {
    private static final String WORKFLOW_AUTOM_JIRA_PLUGIN =
            "henix.plugin.automation.workflow.automjira";
    private static final String SPACE_CHAR = " ";
    private static final String PARTY_NAME_LOGIN_START = " (";
    private static final String PARTY_NAME_LOGIN_END = ")";

    private final ProjectDisplayDao projectDisplayDao;
    private final AttachmentDisplayDao attachmentDisplayDao;
    private final DSLContext dsl;
    private final PermissionEvaluationService permissionEvaluationService;
    private final ProjectDao projectDao;
    private final UserAccountService userAccountService;
    private final BugTrackerDisplayDao bugTrackerDisplayDao;
    private final GenericProjectFinder projectFinder;
    private final GenericProjectManagerService genericProjectManager;
    private final InfoListDisplayDao infoListDisplayDao;
    private final CustomFieldDao customFieldDao;
    private final ScmServerDisplayService scmServerDisplayService;
    private final TestAutomationServerDisplayDao testAutomationServerDisplayDao;
    private final AiServerDisplayDao aiServerDisplayDao;
    private final TestAutomationProjectManagerService testAutomationProjectManagerService;
    private final MilestoneDisplayDao milestoneDisplayDao;
    private final MilestoneBindingManagerService milestoneBindingManagerService;
    private final TemplateConfigurablePluginBindingService templateConfigurablePluginBindingService;
    private final ProjectManagerService projectManager;
    private final AclDisplayDao aclDisplayDao;
    private final ProfileDisplayDao profileDisplayDao;
    private final UltimateLicenseAvailabilityService ultimateLicenseService;
    private final GlobalProjectPivotImporterService globalProjectPivotImporterService;

    @Inject
    ProjectDisplayServiceImpl(
            DSLContext dsl,
            PermissionEvaluationService permissionEvaluationService,
            ProjectDao projectDao,
            UserAccountService userAccountService,
            ProjectDisplayDao projectDisplayDao,
            AttachmentDisplayDao attachmentDisplayDao,
            BugTrackerDisplayDao bugTrackerDisplayDao,
            GenericProjectFinder projectFinder,
            GenericProjectManagerService genericProjectManager,
            InfoListDisplayDao infoListDisplayDao,
            CustomFieldDao customFieldDao,
            ScmServerDisplayService scmServerDisplayService,
            TestAutomationServerDisplayDao testAutomationServerDisplayDao,
            AiServerDisplayDao aiServerDisplayDao,
            MilestoneDisplayDao milestoneDisplayDao,
            MilestoneBindingManagerService milestoneBindingManagerService,
            TestAutomationProjectManagerService testAutomationProjectManagerService,
            TemplateConfigurablePluginBindingService templateConfigurablePluginBindingService,
            ProjectManagerService projectManager,
            AclDisplayDao aclDisplayDao,
            ProfileDisplayDao profileDisplayDao,
            UltimateLicenseAvailabilityService ultimateLicenseService,
            GlobalProjectPivotImporterService globalProjectPivotImporterService) {
        this.dsl = dsl;
        this.permissionEvaluationService = permissionEvaluationService;
        this.projectDao = projectDao;
        this.userAccountService = userAccountService;
        this.projectDisplayDao = projectDisplayDao;
        this.attachmentDisplayDao = attachmentDisplayDao;
        this.bugTrackerDisplayDao = bugTrackerDisplayDao;
        this.projectFinder = projectFinder;
        this.genericProjectManager = genericProjectManager;
        this.infoListDisplayDao = infoListDisplayDao;
        this.customFieldDao = customFieldDao;
        this.scmServerDisplayService = scmServerDisplayService;
        this.testAutomationServerDisplayDao = testAutomationServerDisplayDao;
        this.aiServerDisplayDao = aiServerDisplayDao;
        this.milestoneDisplayDao = milestoneDisplayDao;
        this.milestoneBindingManagerService = milestoneBindingManagerService;
        this.testAutomationProjectManagerService = testAutomationProjectManagerService;
        this.templateConfigurablePluginBindingService = templateConfigurablePluginBindingService;
        this.projectManager = projectManager;
        this.aclDisplayDao = aclDisplayDao;
        this.profileDisplayDao = profileDisplayDao;
        this.ultimateLicenseService = ultimateLicenseService;
        this.globalProjectPivotImporterService = globalProjectPivotImporterService;
    }

    @Override
    public GridResponse findAll(GridRequest request) {
        List<Long> projectIds;
        if (!permissionEvaluationService.hasRole(Roles.ROLE_ADMIN)) {
            UserDto currentUser = userAccountService.findCurrentUserDto();
            List<Long> partyIds = currentUser.getPartyIds();
            permissionEvaluationService.checkAtLeastOneProjectManagementPermissionOrAdmin(partyIds);

            projectIds = projectDao.findAllManagedProjectIds(partyIds);
        } else {
            projectIds = projectDao.findAllProjectAndTemplateIds();
        }
        ProjectGrid projectGrid = new ProjectGrid(projectIds);
        return projectGrid.getRows(request, dsl);
    }

    @PreAuthorize(HAS_ROLE_ADMIN)
    @Override
    public List<NamedReference> getTemplateNamedReferences() {
        return projectDisplayDao.findAllReferences();
    }

    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    @Override
    public ProjectViewDto getProjectView(long projectId) {
        AdministrableProject adminProject = projectFinder.findAdministrableProjectById(projectId);
        ProjectViewDto project = projectDisplayDao.getProjectOrTemplateById(projectId);

        project.setAttachmentList(
                attachmentDisplayDao.findAttachmentListById(project.getAttachmentListId()));
        project.setHasTemplateConfigurablePluginBinding(
                templateConfigurablePluginBindingService.isProjectConfigurationBoundToTemplate(projectId));

        if (project.isTemplate()) {
            final boolean isLinkedToProjects =
                    !projectDisplayDao.getProjectsLinkedToTemplate(projectId).isEmpty();
            project.setTemplateLinkedToProjects(isLinkedToProjects);
            project.setAllProjectBoundToTemplate(projectDisplayDao.findProjectsByTemplateId(projectId));
        }

        bugTrackerDisplayDao.appendBugTrackerBindings(Collections.singletonList(project));
        customFieldDao.appendCustomFieldBindings(Collections.singletonList(project));
        project.setPartyProjectPermissions(getPartyProjectPermissions(projectId));
        project.setPermissions(getProjectPermissions(projectId));
        project.setAvailableScmServers(scmServerDisplayService.getAllServersAndRepositories());
        project.setAvailableTestAutomationServers(testAutomationServerDisplayDao.findAll());
        project.setAvailableAiServers(aiServerDisplayDao.findAll());
        project.setBoundTestAutomationProjects(
                testAutomationProjectManagerService.findAllByTMProject(projectId));
        project.setAvailableBugtrackers(bugTrackerDisplayDao.findAllBugtrackerBindableToProject());
        project.setInfoLists(infoListDisplayDao.findAllWithItems());
        milestoneDisplayDao.appendBoundMilestoneInformation(Collections.singletonList(project));
        appendBugtrackerProjectNames(adminProject, project);
        appendAllowedStatuses(adminProject, project);
        project.setAllowTcModifDuringExec(adminProject.allowTcModifDuringExec());
        project.setExistingImports(getExistingImports(projectId));
        appendProfiles(project);
        return project;
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public Map<String, Boolean> getProjectStatusesInUse(long projectId) {
        Map<String, Boolean> statusesInUse = new HashMap<>();
        boolean settledIsUsed =
                genericProjectManager.projectUsesExecutionStatus(
                        projectId, ExecutionStatus.valueOf(ExecutionStatus.SETTLED.toString()));
        boolean untestableIsUsed =
                genericProjectManager.projectUsesExecutionStatus(
                        projectId, ExecutionStatus.valueOf(ExecutionStatus.UNTESTABLE.toString()));
        statusesInUse.put(ExecutionStatus.SETTLED.toString(), settledIsUsed);
        statusesInUse.put(ExecutionStatus.UNTESTABLE.toString(), untestableIsUsed);
        return statusesInUse;
    }

    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    @Override
    public List<PartyProjectPermissionDto> getPartyProjectPermissions(Long projectId) {
        Field<String> partyName = getPartyNameField();
        Field<Boolean> isPartyActive = when(CORE_USER.LOGIN.isNull(), true).otherwise(CORE_USER.ACTIVE);
        Field<Boolean> isPartyTeam = when(CORE_USER.LOGIN.isNull(), true).otherwise(false);

        return dsl.select(
                        val(projectId),
                        partyName,
                        ACL_RESPONSIBILITY_SCOPE_ENTRY.PARTY_ID,
                        isPartyActive,
                        ACL_GROUP.ID,
                        ACL_GROUP.QUALIFIED_NAME,
                        isPartyTeam)
                .from(ACL_GROUP)
                .join(ACL_RESPONSIBILITY_SCOPE_ENTRY)
                .on(ACL_GROUP.ID.eq(ACL_RESPONSIBILITY_SCOPE_ENTRY.ACL_GROUP_ID))
                .join(ACL_OBJECT_IDENTITY)
                .on(ACL_RESPONSIBILITY_SCOPE_ENTRY.OBJECT_IDENTITY_ID.eq(ACL_OBJECT_IDENTITY.ID))
                .join(ACL_CLASS)
                .on(ACL_OBJECT_IDENTITY.CLASS_ID.eq(ACL_CLASS.ID))
                .leftJoin(CORE_USER)
                .on(ACL_RESPONSIBILITY_SCOPE_ENTRY.PARTY_ID.eq(CORE_USER.PARTY_ID))
                .leftJoin(CORE_TEAM)
                .on(ACL_RESPONSIBILITY_SCOPE_ENTRY.PARTY_ID.eq(CORE_TEAM.PARTY_ID))
                .where(ACL_OBJECT_IDENTITY.IDENTITY.eq(projectId))
                .and(ACL_CLASS.CLASSNAME.in(Project.CLASS_NAME, ProjectTemplate.CLASS_NAME))
                .fetchInto(PartyProjectPermissionDto.class);
    }

    private Map<String, Collection<String>> getProjectPermissions(Long projectId) {
        UserDto currentUser = userAccountService.findCurrentUserDto();
        Map<Long, Multimap<String, String>> permissionsMultimap =
                aclDisplayDao.findPermissionsForProject(
                        Collections.singletonList(projectId), currentUser.getPartyIds());

        return permissionsMultimap.getOrDefault(projectId, ArrayListMultimap.create()).asMap();
    }

    private Field<String> getPartyNameField() {
        return when(CORE_USER.LOGIN.isNull(), CORE_TEAM.NAME)
                .otherwise(
                        when(
                                        CORE_USER.FIRST_NAME.isNull().or(CORE_USER.FIRST_NAME.eq(EMPTY)),
                                        concat(
                                                CORE_USER.LAST_NAME,
                                                val(PARTY_NAME_LOGIN_START),
                                                CORE_USER.LOGIN,
                                                val(PARTY_NAME_LOGIN_END)))
                                .otherwise(
                                        concat(
                                                CORE_USER.FIRST_NAME,
                                                val(SPACE_CHAR),
                                                CORE_USER.LAST_NAME,
                                                val(PARTY_NAME_LOGIN_START),
                                                CORE_USER.LOGIN,
                                                val(PARTY_NAME_LOGIN_END))));
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public BindMilestoneToProjectDialogData findAvailableMilestones(long projectId) {
        BindMilestoneToProjectDialogData dialogData = new BindMilestoneToProjectDialogData();
        dialogData.setGlobalMilestones(appendFilteredMilestonesListAccordingType(projectId, "global"));
        dialogData.setPersonalMilestones(
                appendFilteredMilestonesListAccordingType(projectId, "personal"));
        dialogData.setOtherMilestones(appendFilteredMilestonesListAccordingType(projectId, "other"));
        return dialogData;
    }

    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    @Override
    public ProjectDataInfo getProjectDataInfoOnAction(
            Long projectId, ProjectDataInfo.ProjectAction action) {
        boolean hasData = projectManager.hasProjectData(projectId);
        boolean isDeletionAction = ProjectDataInfo.ProjectAction.DELETION.equals(action);

        String i18nKeyMessage;

        if (hasData) {
            i18nKeyMessage = getWithDataI18nKeyActionMessage(isDeletionAction);
        } else {
            i18nKeyMessage = getWithoutDataI18nKeyActionMessage(projectId, isDeletionAction);
        }

        return new ProjectDataInfo(hasData, i18nKeyMessage);
    }

    @Override
    public boolean checkRemotePluginIsAvailableInProject(ProjectViewDto dto) {
        return dto.getAvailablePlugins().stream()
                .anyMatch(projectPluginDto -> WORKFLOW_AUTOM_JIRA_PLUGIN.equals(projectPluginDto.getId()));
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public List<String> fetchBugtrackerProjectNames(long projectId) {
        return projectDisplayDao.fetchBugtrackerProjectNames(projectId);
    }

    private String getWithDataI18nKeyActionMessage(boolean isDeletionAction) {
        if (isDeletionAction) {
            return NO_DELETABLE_PROJECT_I18N_KEY;
        }
        return CANNOT_COERCE_INTO_TEMPLATE_I18N_KEY;
    }

    private String getWithoutDataI18nKeyActionMessage(Long projectId, boolean isDeletionAction) {
        if (isDeletionAction) {
            if (haveExecutedTests(projectId)) {
                return DELETABLE_PROJECT_I18N_KEY_WITH_LINKED_JOBS;
            }
            return DELETABLE_PROJECT_I18N_KEY;
        } else {
            return COERCE_INTO_TEMPLATE_I18N_KEY;
        }
    }

    private boolean haveExecutedTests(Long projectId) {
        Collection<Long> taProjectIds =
                testAutomationProjectManagerService.findAllIdsByTMProject(projectId);
        if (!taProjectIds.isEmpty()) {
            return testAutomationProjectManagerService.haveExecutedTests(taProjectIds);
        }
        return false;
    }

    private void appendBugtrackerProjectNames(
            AdministrableProject adminProject, ProjectViewDto project) {
        GenericProject genericProject = adminProject.getProject();
        if (genericProject.isBoundToBugtracker()) {
            project.setBugtrackerProjectNames(genericProject.getBugtrackerProjectNames());
        }
    }

    private void appendAllowedStatuses(AdministrableProject adminProject, ProjectViewDto project) {
        CampaignLibrary cl = adminProject.getCampaignLibrary();
        Map<String, Boolean> allowedStatuses = new HashMap<>();
        allowedStatuses.put(
                ExecutionStatus.SETTLED.toString(), cl.allowsStatus(ExecutionStatus.SETTLED));
        allowedStatuses.put(
                ExecutionStatus.UNTESTABLE.toString(), cl.allowsStatus(ExecutionStatus.UNTESTABLE));
        project.setAllowedStatuses(allowedStatuses);
    }

    private List<MilestoneDto> appendFilteredMilestonesListAccordingType(
            long projectId, String type) {
        List<Milestone> filteredMilestones =
                milestoneBindingManagerService.getAllBindableMilestoneForProject(projectId, type);
        List<MilestoneDto> filteredMilestonesDto = new ArrayList<>();
        filteredMilestones.forEach(
                milestone -> {
                    MilestoneDto milestoneDto = new MilestoneDto();
                    milestoneDto.setId(milestone.getId());
                    milestoneDto.setLabel(milestone.getLabel());
                    milestoneDto.setDescription(milestone.getDescription());
                    milestoneDto.setRange(milestone.getRange().name());
                    milestoneDto.setEndDate(milestone.getEndDate());
                    milestoneDto.setStatus(milestone.getStatus().name());
                    milestoneDto.setOwnerFirstName(milestone.getOwner().getFirstName());
                    milestoneDto.setOwnerLastName(milestone.getOwner().getLastName());
                    milestoneDto.setOwnerLogin(milestone.getOwner().getLogin());
                    filteredMilestonesDto.add(milestoneDto);
                });
        return filteredMilestonesDto;
    }

    @Override
    @PreAuthorize(MANAGE_PROJECT_OR_ROLE_ADMIN)
    public List<PivotFormatImportDto> getExistingImports(long projectId) {
        List<PivotFormatImportDto> pivotFormatImportDtos =
                projectDisplayDao.getExistingImports(projectId);
        appendImportLogFilePath(pivotFormatImportDtos);
        return pivotFormatImportDtos;
    }

    private void appendImportLogFilePath(List<PivotFormatImportDto> pivotFormatImportDtos) {
        pivotFormatImportDtos.forEach(
                pivotFormatImportDto -> {
                    boolean hasWarningStatus =
                            PivotFormatImportStatus.WARNING.equals(pivotFormatImportDto.getStatus());

                    String importLogFilePath =
                            hasWarningStatus
                                    ? globalProjectPivotImporterService.getImportWarningLogFilePath(
                                            pivotFormatImportDto.getId(), pivotFormatImportDto.getType())
                                    : globalProjectPivotImporterService.getImportErrorLogFilePath(
                                            pivotFormatImportDto.getId(), pivotFormatImportDto.getType());
                    File file = new File(importLogFilePath);

                    if (file.exists()) {
                        pivotFormatImportDto.setImportLogFilePath(importLogFilePath);
                    }
                });
    }

    private void appendProfiles(ProjectViewDto project) {
        boolean isUltimateAvailable = ultimateLicenseService.isAvailable();

        List<ProfileDto> profiles =
                profileDisplayDao.findAll().stream()
                        .filter(profile -> isUltimateAvailable || profile.isSystem())
                        .filter(ProfileDto::isActive)
                        .toList();

        project.setAvailableProfiles(profiles);
    }
}
