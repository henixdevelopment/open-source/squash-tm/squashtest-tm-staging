/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto;

import java.util.EnumSet;
import org.squashtest.tm.api.security.acls.Permissions;

/**
 * Permissions which are used globally in the instance to give access to some application parts. The
 * user only has to have these permissions for one project to access them
 */
public enum AccessPermission {
    MANAGE_PROJECT(Permissions.MANAGE_PROJECT),
    MANAGE_MILESTONE(Permissions.MANAGE_MILESTONE),
    MANAGE_PROJECT_CLEARANCE(Permissions.MANAGE_PROJECT_CLEARANCE),
    READ(Permissions.READ),
    WRITE_AS_AUTOMATION(Permissions.WRITE_AS_AUTOMATION),
    WRITE_AS_FUNCTIONAL(Permissions.WRITE_AS_FUNCTIONAL);

    private final Permissions permissionWithMask;

    AccessPermission(Permissions permissionWithMask) {
        this.permissionWithMask = permissionWithMask;
    }

    public Permissions getPermissionWithMask() {
        return permissionWithMask;
    }

    public static AccessPermission findByMask(int mask) {
        EnumSet<AccessPermission> permissions = EnumSet.allOf(AccessPermission.class);

        for (AccessPermission permission : permissions) {
            if (permission.getPermissionWithMask().getMask() == mask) {
                return permission;
            }
        }
        return null;
    }
}
