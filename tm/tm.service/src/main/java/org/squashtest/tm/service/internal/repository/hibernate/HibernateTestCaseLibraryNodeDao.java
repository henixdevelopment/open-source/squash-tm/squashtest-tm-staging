/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.jooq.impl.DSL.concat;
import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.groupConcat;
import static org.jooq.impl.DSL.name;
import static org.jooq.impl.DSL.select;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.TCLN_RELATIONSHIP;
import static org.squashtest.tm.jooq.domain.Tables.TCLN_RELATIONSHIP_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_CONTENT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.hibernate.query.Query;
import org.jooq.AggregateFunction;
import org.jooq.CommonTableExpression;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.core.foundation.lang.PathUtils;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.service.internal.repository.ParameterNames;
import org.squashtest.tm.service.internal.repository.TestCaseLibraryNodeDao;

@Repository("squashtest.tm.repository.TestCaseLibraryNodeDao")
public class HibernateTestCaseLibraryNodeDao extends HibernateEntityDao<TestCaseLibraryNode>
        implements TestCaseLibraryNodeDao {

    private static final String UNCHECKED = "unchecked";

    @Inject private DSLContext dsl;

    @SuppressWarnings(UNCHECKED)
    @Override
    public List<String> getParentsName(long entityId) {
        Query query = currentSession().getNamedQuery("TestCasePathEdge.findSortedParentNames");
        query.setParameter(ParameterNames.NODE_ID, entityId);
        return query.list();
    }

    @SuppressWarnings(UNCHECKED)
    @Override
    public List<Long> getParentsIds(long entityId) {
        Query query = currentSession().getNamedQuery("TestCasePathEdge.findSortedParentIds");
        query.setParameter(ParameterNames.NODE_ID, entityId);
        return query.list();
    }

    @Override
    public List<TestCaseLibraryNode> findNodesByPath(List<String> path) {
        List<Long> ids = findNodeIdsByPath(path);
        List<TestCaseLibraryNode> result = findAllByIds(ids);

        // post process the result to ensure the correct order of the result
        TestCaseLibraryNode[] toReturn = new TestCaseLibraryNode[ids.size()];
        for (TestCaseLibraryNode node : result) {
            toReturn[ids.indexOf(node.getId())] = node;
        }

        return Arrays.asList(toReturn);
    }

    @SuppressWarnings(UNCHECKED)
    @Override
    public List<Long> findNodeIdsByPath(List<String> paths) {
        // we will make a select with jooq, flushing session as hibernate would do if the request was
        // made in hql
        entityManager.flush();
        if (!paths.isEmpty()) {
            // process the paths parameter : we don't want escaped '/' in there
            List<String> effectiveParameters = unescapeSlashes(paths);

            // get all the last node names
            List<String> tclnNames =
                    paths.stream()
                            .map(PathUtils::splitPath) // split path
                            .map(Arrays::asList) // into a stream of List<String>
                            // keeping only size > 1 ie more than just a project name
                            .filter(pathParts -> pathParts.size() > 1)
                            // want to keep only last element in names, ie the name of the last node in path
                            .map(pathParts -> pathParts.get(pathParts.size() - 1))
                            .map(this::unescapeSlashes)
                            .toList();

            if (tclnNames.isEmpty()) { // avoiding mariadb crash with empty list...
                return Collections.emptyList();
            }

            // now let's go for some SQL
            // the basic idea here is to concat all paths for descendants which have a name in terminal
            // list and only them
            // and thus comparing theses paths with our list in having clause
            org.squashtest.tm.jooq.domain.tables.TestCaseLibraryNode ancestor =
                    TEST_CASE_LIBRARY_NODE.as("ancestor");
            org.squashtest.tm.jooq.domain.tables.TestCaseLibraryNode descendant =
                    TEST_CASE_LIBRARY_NODE.as("descendant");
            AggregateFunction<String> groupConcatFunction =
                    groupConcat(ancestor.NAME).orderBy(TCLN_RELATIONSHIP_CLOSURE.DEPTH.desc()).separator("/");
            Field<String> concatPath =
                    concat(concat("/", PROJECT.NAME), concat("/", groupConcatFunction));

            Map<String, Long> idByPath =
                    dsl
                            .select(concatPath.as("path"), TCLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID)
                            .from(TCLN_RELATIONSHIP_CLOSURE)
                            .innerJoin(ancestor)
                            .on(TCLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.eq(ancestor.TCLN_ID))
                            .innerJoin(PROJECT)
                            .on(ancestor.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                            .innerJoin(descendant)
                            .on(TCLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID.eq(descendant.TCLN_ID))
                            .where(descendant.NAME.in(tclnNames))
                            .groupBy(TCLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID, PROJECT.NAME)
                            .having(concatPath.in(effectiveParameters))
                            .fetch()
                            .stream()
                            .collect(
                                    Collectors.toMap(
                                            r -> r.get("path", String.class),
                                            r -> r.get(TCLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID)));

            // this should preserve order of initial list
            return effectiveParameters.stream().map(idByPath::get).toList();
        } else {
            return Collections.emptyList();
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see
     *     org.squashtest.tm.service.internal.repository.LibraryNodeDao#findNodeIdByPath(java.lang.String)
     */
    @SuppressWarnings(UNCHECKED)
    @Override
    public Long findNodeIdByPath(String path) {
        return findNodeIdsByPath(Arrays.asList(path)).get(0);
    }

    /**
     * {@inheritDoc}
     *
     * @see
     *     org.squashtest.tm.service.internal.repository.LibraryNodeDao#findNodeByPath(java.lang.String)
     */
    @Override
    public TestCaseLibraryNode findNodeByPath(String path) {
        Long id = findNodeIdByPath(path);

        return (TestCaseLibraryNode)
                (id != null ? currentSession().load(TestCaseLibraryNode.class, id) : null);
    }

    @Override
    public TreeMap<String, Long> buildNodePathsTree(String projectName) {
        CommonTableExpression<Record2<Long, String>> cte =
                name("cte")
                        .fields("id", "path")
                        .as(
                                select(
                                                TEST_CASE_LIBRARY_NODE.TCLN_ID,
                                                concat(PROJECT.NAME, DSL.value("/"), TEST_CASE_LIBRARY_NODE.NAME)
                                                        .cast(SQLDataType.LONGVARCHAR))
                                        .from(PROJECT)
                                        .innerJoin(TEST_CASE_LIBRARY_CONTENT)
                                        .on(PROJECT.TCL_ID.eq(TEST_CASE_LIBRARY_CONTENT.LIBRARY_ID))
                                        .innerJoin(TEST_CASE_LIBRARY_NODE)
                                        .on(TEST_CASE_LIBRARY_CONTENT.CONTENT_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
                                        .where(PROJECT.NAME.eq(projectName))
                                        .union(
                                                select(
                                                                TCLN_RELATIONSHIP.DESCENDANT_ID,
                                                                field(name("cte", "path"))
                                                                        .concat("/")
                                                                        .concat(TEST_CASE_LIBRARY_NODE.NAME)
                                                                        .cast(SQLDataType.LONGVARCHAR))
                                                        .from(name("cte"))
                                                        .innerJoin(TCLN_RELATIONSHIP)
                                                        .on(field(name("cte", "id")).eq(TCLN_RELATIONSHIP.ANCESTOR_ID))
                                                        .innerJoin(TEST_CASE_LIBRARY_NODE)
                                                        .on(
                                                                TCLN_RELATIONSHIP.DESCENDANT_ID.eq(
                                                                        TEST_CASE_LIBRARY_NODE.TCLN_ID))));

        return dsl.withRecursive(cte)
                .selectFrom(cte)
                .orderBy(cte.field("path"))
                .fetchStream()
                .collect(
                        Collectors.toMap(
                                r -> r.get("path", String.class),
                                r -> r.get("id", Long.class),
                                (key1, key2) -> key1,
                                TreeMap::new));
    }

    @Override
    public int countSiblingsOfNode(long nodeId) {

        Query q;
        Integer count;

        q = currentSession().getNamedQuery("testCase.countSiblingsInFolder");
        q.setParameter(ParameterNames.NODE_ID, nodeId);
        count = (Integer) q.uniqueResult();

        if (count == null) {
            q = currentSession().getNamedQuery("testCase.countSiblingsInLibrary");
            q.setParameter(ParameterNames.NODE_ID, nodeId);
            count = (Integer) q.uniqueResult();
        }

        // if NPE here it's probably because nodeId corresponds to nothing. The +1 is because the
        // queries use 'maxindex' instead of 'count'
        return count + 1;
    }

    private List<String> unescapeSlashes(List<String> paths) {
        List<String> unescaped = new ArrayList<>(paths.size());
        for (String orig : paths) {
            unescaped.add(orig.replace("\\/", "/"));
        }
        return unescaped;
    }

    private String unescapeSlashes(String path) {
        return path.replace("\\/", "/");
    }
}
