/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.supervision.model;

import java.util.List;

public class AutomatedSuiteOverview {

    private final String suiteId;
    private final int tfPercentage;
    private final List<TfExecutionView> tfExecutions;
    private final int automTerminatedCount;
    private final List<SquashAutomExecutionView> automExecutions;
    private String errorMessage;
    private List<String> workflowsUUIDs;

    public AutomatedSuiteOverview(
            String suiteId,
            int tfPercentage,
            List<TfExecutionView> tfExecutions,
            int automTerminatedCount,
            List<SquashAutomExecutionView> automExecutions) {
        this.suiteId = suiteId;
        this.tfPercentage = tfPercentage;
        this.tfExecutions = tfExecutions;
        this.automTerminatedCount = automTerminatedCount;
        this.automExecutions = automExecutions;
    }

    public String getSuiteId() {
        return suiteId;
    }

    public int getTfPercentage() {
        return tfPercentage;
    }

    public List<TfExecutionView> getTfExecutions() {
        return tfExecutions;
    }

    public int getAutomTerminatedCount() {
        return automTerminatedCount;
    }

    public List<SquashAutomExecutionView> getAutomExecutions() {
        return automExecutions;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public List<String> getWorkflowsUUIDs() {
        return workflowsUUIDs;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public void setWorkflowsUUIDs(List<String> workflowsUUIDs) {
        this.workflowsUUIDs = workflowsUUIDs;
    }
}
