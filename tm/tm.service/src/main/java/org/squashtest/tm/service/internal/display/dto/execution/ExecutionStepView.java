/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.execution;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.squashtest.tm.core.foundation.annotation.CleanHtml;
import org.squashtest.tm.service.internal.display.dto.AttachmentListDto;
import org.squashtest.tm.service.internal.display.dto.CustomFieldValueDto;
import org.squashtest.tm.service.internal.display.dto.DenormalizedCustomFieldValueDto;

public class ExecutionStepView {
    private long id;
    private int order;
    private String executionStatus;
    private String action;
    private String expectedResult;
    private String comment;
    private AttachmentListDto attachmentList = new AttachmentListDto();
    private List<DenormalizedCustomFieldValueDto> denormalizedCustomFieldValues = new ArrayList<>();
    private List<CustomFieldValueDto> customFieldValues = new ArrayList<>();
    private Date lastExecutedOn;
    private String lastExecutedBy;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getExecutionStatus() {
        return executionStatus;
    }

    public void setExecutionStatus(String executionStatus) {
        this.executionStatus = executionStatus;
    }

    @CleanHtml
    public String getAction() {
        return action;
    }

    public void setAction(@CleanHtml String action) {
        this.action = action;
    }

    @CleanHtml
    public String getExpectedResult() {
        return expectedResult;
    }

    public void setExpectedResult(@CleanHtml String expectedResult) {
        this.expectedResult = expectedResult;
    }

    @CleanHtml
    public String getComment() {
        return comment;
    }

    public void setComment(@CleanHtml String comment) {
        this.comment = comment;
    }

    public AttachmentListDto getAttachmentList() {
        return attachmentList;
    }

    public void setAttachmentList(AttachmentListDto attachmentList) {
        this.attachmentList = attachmentList;
    }

    public List<DenormalizedCustomFieldValueDto> getDenormalizedCustomFieldValues() {
        return denormalizedCustomFieldValues;
    }

    public void setDenormalizedCustomFieldValues(
            List<DenormalizedCustomFieldValueDto> denormalizedCustomFieldValues) {
        this.denormalizedCustomFieldValues = denormalizedCustomFieldValues;
    }

    public List<CustomFieldValueDto> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<CustomFieldValueDto> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public Date getLastExecutedOn() {
        return lastExecutedOn;
    }

    public void setLastExecutedOn(Date lastExecutedOn) {
        this.lastExecutedOn = lastExecutedOn;
    }

    public String getLastExecutedBy() {
        return lastExecutedBy;
    }

    public void setLastExecutedBy(String lastExecutedBy) {
        this.lastExecutedBy = lastExecutedBy;
    }
}
