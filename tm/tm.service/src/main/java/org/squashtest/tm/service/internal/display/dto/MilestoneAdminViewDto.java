/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import java.util.ArrayList;
import java.util.List;

public class MilestoneAdminViewDto extends MilestoneDto {

    private boolean canEdit;
    private List<ProjectInfoForMilestoneAdminViewDto> boundProjectsInformation = new ArrayList<>();

    public static MilestoneAdminViewDto fromMilestoneDto(MilestoneDto milestoneDto) {
        MilestoneAdminViewDto milestoneAdminViewDto = new MilestoneAdminViewDto();
        milestoneAdminViewDto.setId(milestoneDto.getId());
        milestoneAdminViewDto.setLabel(milestoneDto.getLabel());
        milestoneAdminViewDto.setStatus(milestoneDto.getStatus());
        milestoneAdminViewDto.setRange(milestoneDto.getRange());
        milestoneAdminViewDto.setDescription(milestoneDto.getDescription());
        milestoneAdminViewDto.setEndDate(milestoneDto.getEndDate());
        milestoneAdminViewDto.setOwnerFirstName(milestoneDto.getOwnerFirstName());
        milestoneAdminViewDto.setOwnerLastName(milestoneDto.getOwnerLastName());
        milestoneAdminViewDto.setOwnerLogin(milestoneDto.getOwnerLogin());
        milestoneAdminViewDto.setCreatedBy(milestoneDto.getCreatedBy());
        milestoneAdminViewDto.setCreatedOn(milestoneDto.getCreatedOn());
        milestoneAdminViewDto.setLastModifiedBy(milestoneDto.getLastModifiedBy());
        milestoneAdminViewDto.setLastModifiedOn(milestoneDto.getLastModifiedOn());

        return milestoneAdminViewDto;
    }

    public boolean isCanEdit() {
        return canEdit;
    }

    public void setCanEdit(boolean canEdit) {
        this.canEdit = canEdit;
    }

    public List<ProjectInfoForMilestoneAdminViewDto> getBoundProjectsInformation() {
        return boundProjectsInformation;
    }

    public void setBoundProjectsInformation(
            List<ProjectInfoForMilestoneAdminViewDto> boundProjectsInformation) {
        this.boundProjectsInformation = boundProjectsInformation;
    }
}
