/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search;

import java.util.List;
import java.util.Map;
import org.jooq.DSLContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.service.display.search.ResearchResult;
import org.squashtest.tm.service.display.search.TestCaseThroughRequirementSearchService;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.search.TestCaseThroughRequirementGrid;

@Service
@Transactional(readOnly = true)
public class TestCaseThroughRequirementSearchServiceImpl
        implements TestCaseThroughRequirementSearchService {

    private final DSLContext dslContext;
    private final RequirementSearchServiceImpl requirementSearchService;

    public TestCaseThroughRequirementSearchServiceImpl(
            DSLContext dslContext, RequirementSearchServiceImpl requirementSearchService) {
        this.dslContext = dslContext;
        this.requirementSearchService = requirementSearchService;
    }

    @Override
    public ResearchResult search(GridRequest request) {
        Map<EntityType, List<Long>> entityReferenceFromScope =
                requirementSearchService.getEntityReferenceFromScope(request);
        TestCaseThroughRequirementGrid testCaseThroughRequirementGrid =
                new TestCaseThroughRequirementGrid(entityReferenceFromScope);
        GridResponse gridResponse = testCaseThroughRequirementGrid.getRows(request, dslContext);
        List<Long> testcaseIds =
                gridResponse.getDataRows().stream()
                        .mapToLong(row -> Long.parseLong(row.getId()))
                        .boxed()
                        .toList();
        return new ResearchResult(testcaseIds, gridResponse.getCount());
    }
}
