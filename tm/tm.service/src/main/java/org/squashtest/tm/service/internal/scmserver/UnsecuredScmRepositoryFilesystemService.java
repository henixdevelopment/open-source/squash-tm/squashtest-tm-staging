/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.scmserver;

import gherkin.ast.GherkinDocument;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.core.foundation.lang.PathUtils;
import org.squashtest.tm.core.foundation.lang.Wrapped;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.bdd.BddImplementationTechnology;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.scm.ScmRepository;
import org.squashtest.tm.domain.script.GherkinParser;
import org.squashtest.tm.domain.testautomation.AutomatedTestTechnology;
import org.squashtest.tm.domain.testcase.ExploratoryTestCase;
import org.squashtest.tm.domain.testcase.KeywordTestCase;
import org.squashtest.tm.domain.testcase.ScriptedTestCase;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseVisitor;
import org.squashtest.tm.exception.testcase.ScriptParsingException;
import org.squashtest.tm.service.internal.library.PathService;
import org.squashtest.tm.service.internal.testcase.event.TestCaseGherkinLocationChangeEvent;
import org.squashtest.tm.service.scmserver.ScmRepositoryFilesystemService;
import org.squashtest.tm.service.scmserver.ScmRepositoryManifest;
import org.squashtest.tm.service.testautomation.AutomatedTestTechnologyFinderService;
import org.squashtest.tm.service.testcase.TestCaseModificationService;
import org.squashtest.tm.service.testcase.bdd.KeywordTestCaseService;

@Service("ScmRepositoryFilesystemService")
@Transactional(readOnly = true)
public class UnsecuredScmRepositoryFilesystemService implements ScmRepositoryFilesystemService {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(UnsecuredScmRepositoryFilesystemService.class);

    private static final String TEST_CASE_PATH_ILLEGAL_PATTERN = "[^a-zA-Z0-9\\_\\-\\/]";

    private static final String CANNOT_TRANSMIT_A_STANDARD_TEST_CASE =
            "Cannot transmit a Standard TestCase.";

    @Inject private ApplicationEventPublisher eventPublisher;

    @Inject private PathService pathService;

    @Inject private KeywordTestCaseService keywordTestCaseService;

    @Inject private TestCaseModificationService testCaseModificationService;

    @Inject private AutomatedTestTechnologyFinderService automatedTestTechnologyFinderService;

    @PersistenceContext private EntityManager entityManager;

    @Override
    public void createWorkingFolderIfAbsent(ScmRepository scm) {
        File workingFolder = scm.getWorkingFolder();
        if (workingFolder.exists()) {
            LOGGER.trace("The working folder of repository '{}' already exists.", scm.getName());
            return;
        }
        try {
            scm.doWithLock(
                    () -> {
                        tryCreateFolders(workingFolder);
                        return null;
                    });
        } catch (IOException iOEx) {
            throw new RuntimeException("Error while creating the working folder in the repository", iOEx);
        }
    }

    @Override
    public void deleteLocalRepositories(List<ScmRepository> repositories) {
        repositories.forEach(this::deleteLocalRepository);
    }

    private void deleteLocalRepository(ScmRepository scm) {
        try {
            scm.doWithLock(
                    () -> {
                        FileUtils.deleteDirectory(scm.getBaseRepositoryFolder());
                        return null;
                    });
        } catch (IOException iOEx) {
            throw new RuntimeException("Error while deleting the repository", iOEx);
        }
    }

    @Override
    public void createOrUpdateScriptFile(ScmRepository scm, Collection<TestCase> testCases) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("committing {} files to repository '{}'", testCases.size(), scm.getName());
        }
        // exportToScm
        try {
            scm.doWithLock(
                    () -> {
                        LOGGER.trace("committing tests to scm : '{}'", scm.getName());
                        try {
                            ScmRepositoryManifest manifest = new ScmRepositoryManifest(scm);
                            for (TestCase testCase : testCases) {
                                File testFile = locateOrMoveOrCreateTestFile(manifest, testCase);
                                // at this point the file is created without error
                                // lets fill the file with the script content
                                printToFile(testFile, testCase);
                                // Update Squash Autom related fields
                                updateTestCaseAutomatedScriptInformation(testCase, testFile, manifest);
                            }
                            return null;
                        } catch (IOException ex) {
                            throw new RuntimeException(
                                    "Error while creating/updating files in the repository", ex);
                        }
                    });
        } catch (IOException ex) {
            throw new RuntimeException("Error while creating/updating files in the repository", ex);
        }
    }

    // **************** internal routines ********************************

    /********************************************************************************************************
     * /!\ /!\/!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\
     *
     * The following methods below this line are intended to run within the scope of the scm filelock.
     * This is hard to enforce so please be careful.
     *
     * /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\
     ********************************************************************************************************/

    /**
     * Attempt to locate the physical file corresponding to the given Test Case. If the file was
     * located, check if it needs to be move and/or renamed according to the Test Case state. If the
     * file does not exist yet, try to create it according to the Test Case, first attempting to write
     * it with a standard name, then with a backup name if it failed.
     *
     * @param manifest
     * @param testCase
     * @return
     * @throws IOException
     */
    public File locateOrMoveOrCreateTestFile(ScmRepositoryManifest manifest, TestCase testCase)
            throws IOException {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(
                    "attempting to locate physical script file for test '{}' in the scm", testCase.getId());
        }
        File testfile = null;
        String pattern = createTestCasePatternForResearch(testCase);
        Optional<File> maybeTestFile = manifest.locateTest(pattern, testCase.getId());
        if (maybeTestFile.isPresent()) {
            testfile = maybeTestFile.get();
            LOGGER.trace("found file : '{}'", testfile.getAbsolutePath());
            testfile = moveAndRenameFileIfNeeded(testCase, testfile, manifest.getScm());
        } else {
            LOGGER.trace("file not found, attempting to create a new one");
            // roundtrip 1 : attempt the creation with the normal filename
            try {
                testfile = createTestNominal(manifest.getScm(), testCase);
            } catch (IOException ex) {
                if (SystemUtils.IS_OS_WINDOWS) {
                    // maybe the failure is due to long absolute filename
                    LOGGER.trace(
                            "failed to create file due to IOException, attempting with the backup filename", ex);
                    // try again with the backup name
                    testfile = createTestBackup(manifest.getScm(), testCase);
                }
            }
        }
        return testfile;
    }

    @Override
    public String createTestCasePatternForResearch(TestCase testCase) {
        Wrapped<String> pattern = new Wrapped<>();
        TestCaseVisitor visitor =
                new TestCaseVisitor() {
                    @Override
                    public void visit(TestCase testCase) {
                        throw new IllegalArgumentException(CANNOT_TRANSMIT_A_STANDARD_TEST_CASE);
                    }

                    @Override
                    public void visit(KeywordTestCase keywordTestCase) {
                        pattern.setValue(keywordTestCaseService.buildFilenameMatchPattern(keywordTestCase));
                    }

                    @Override
                    public void visit(ScriptedTestCase scriptedTestCase) {
                        pattern.setValue(scriptedTestCase.buildFilenameMatchPattern());
                    }

                    @Override
                    public void visit(ExploratoryTestCase exploratoryTestCase) {
                        throw new IllegalArgumentException(CANNOT_TRANSMIT_A_STANDARD_TEST_CASE);
                    }
                };
        testCase.accept(visitor);
        return pattern.getValue();
    }

    @Override
    public boolean checkLocalRepositoryExists(String repositoryPath) {
        return FileUtils.getFile(repositoryPath).exists();
    }

    /**
     * Check if the given TestCase has been renamed and/or moved since it was last transmitted and
     * written into the File. If so, try to move the File to the correct destination. First attempting
     * to write the standard name, then with the backup name if it failed.
     *
     * @param testCase The Test Case being transmitted
     * @param originalFile The File corresponding to the given Test Case that was found (but
     *     potentially at the wrong place)
     * @param scmRepository The Scm Repository in which operations are occurring
     * @return The moved/renamed File if such operations were needed. The original File it was not
     *     moved/renamed
     */
    private File moveAndRenameFileIfNeeded(
            TestCase testCase, File originalFile, ScmRepository scmRepository) {

        File workingDirectory = scmRepository.getWorkingFolder();
        // Determine CORRECT folders path
        String foldersPath = getFoldersPath(testCase);
        // Determine CORRECT relative path of the given TestCase with the STANDARD name
        String correctStandardRelativePath = buildTestCaseStandardRelativePath(foldersPath, testCase);
        // Determine CORRECT relative path of the given TestCase with the BACKUP name
        String correctBackupRelativePath = buildTestCaseBackUpRelativePath(foldersPath, testCase);
        // Get the CURRENT relative path
        String currentPath = workingDirectory.toURI().relativize(originalFile.toURI()).toString();

        // Compare them
        if (!correctStandardRelativePath.equals(currentPath)
                && !correctBackupRelativePath.equals(currentPath)) {
            // Try to move/rename with the standard name
            File targetStandardFile = new File(workingDirectory, correctStandardRelativePath);

            try {
                tryMoveFile(originalFile, targetStandardFile);
                eventPublisher.publishEvent(
                        new TestCaseGherkinLocationChangeEvent(
                                testCase.getId(),
                                scmRepository.getScmServer().getUrl()
                                        + "/"
                                        + scmRepository.getName()
                                        + "/"
                                        + correctStandardRelativePath));
                return targetStandardFile;
            } catch (IOException ex) {
                LOGGER.warn("Error while trying to move file, will try with the backup name", ex);
                // Operation failed, try with the backup name
                File targetBackUpFile = new File(workingDirectory, correctBackupRelativePath);
                try {
                    tryMoveFile(originalFile, targetBackUpFile);
                    eventPublisher.publishEvent(
                            new TestCaseGherkinLocationChangeEvent(
                                    testCase.getId(),
                                    scmRepository.getScmServer().getUrl()
                                            + "/"
                                            + scmRepository.getName()
                                            + "/"
                                            + correctBackupRelativePath));
                    return targetBackUpFile;
                } catch (IOException ioEx) {
                    throw new RuntimeException("Could not move file due to IOException", ioEx);
                }
            }
        }
        // If the test case was not moved/rename, file remains the same
        return originalFile;
    }

    /**
     * Given a TestCase and its corresponding file's foldersPath, build the file's relative path with
     * the STANDARD name.
     *
     * @param foldersPath The folders path of the Test Case's corresponding File
     * @param testCase The Test Case which path is to build
     * @return The relative path with the Standard name
     */
    private String buildTestCaseStandardRelativePath(String foldersPath, TestCase testCase) {
        Wrapped<String> standardName = new Wrapped<>();
        TestCaseVisitor visitor =
                new TestCaseVisitor() {
                    @Override
                    public void visit(TestCase testCase) {
                        throw new IllegalArgumentException(CANNOT_TRANSMIT_A_STANDARD_TEST_CASE);
                    }

                    @Override
                    public void visit(KeywordTestCase keywordTestCase) {
                        standardName.setValue(keywordTestCaseService.createFileName(keywordTestCase));
                    }

                    @Override
                    public void visit(ScriptedTestCase scriptedTestCase) {
                        standardName.setValue(scriptedTestCase.createFilename());
                    }

                    @Override
                    public void visit(ExploratoryTestCase exploratoryTestCase) {
                        throw new IllegalArgumentException(CANNOT_TRANSMIT_A_STANDARD_TEST_CASE);
                    }
                };
        testCase.accept(visitor);
        return foldersPath + standardName.getValue();
    }

    /**
     * Same method as {@link #buildTestCaseStandardRelativePath(String, TestCase)} but with the BackUp
     * name.
     *
     * @param foldersPath The folders path of the Test Case's corresponding File
     * @param testCase The Test Case which path is to build
     * @return The relative path with the Standard name
     */
    private String buildTestCaseBackUpRelativePath(String foldersPath, TestCase testCase) {
        Wrapped<String> backupName = new Wrapped<>();
        TestCaseVisitor visitor =
                new TestCaseVisitor() {
                    @Override
                    public void visit(TestCase testCase) {
                        throw new IllegalArgumentException(CANNOT_TRANSMIT_A_STANDARD_TEST_CASE);
                    }

                    @Override
                    public void visit(KeywordTestCase keywordTestCase) {
                        backupName.setValue(keywordTestCaseService.createBackupFileName(keywordTestCase));
                    }

                    @Override
                    public void visit(ScriptedTestCase scriptedTestCase) {
                        backupName.setValue(scriptedTestCase.createBackupFileName());
                    }

                    @Override
                    public void visit(ExploratoryTestCase exploratoryTestCase) {
                        throw new IllegalArgumentException(CANNOT_TRANSMIT_A_STANDARD_TEST_CASE);
                    }
                };

        testCase.accept(visitor);
        return foldersPath + backupName.getValue();
    }

    /**
     * Given a Test Case, compute its corresponding file's normalized folders path. The folders path
     * is the relative path from the working directory but not containing the name of the file, so it
     * only contains the folders. Ex: A test case in 'Project 2/main folder/sub folder/test case 7'
     * will compute the path 'main_folder/sub_folder/' Ex: A test case in the root of a library will
     * compute an empty string
     *
     * @param testCase The Test Case which path is to compute
     * @return The folders path of the given Test Case
     */
    private String getFoldersPath(TestCase testCase) {
        Project project = entityManager.find(Project.class, testCase.getProject().getId());

        if (!project.isUseTreeStructureInScmRepo()) {
            // If flat structure is used, no need of folders path
            return "";
        } else {
            // If the tree structure of TM is used
            String testCaseFoldersPath = pathService.buildTestCaseFoldersPath(testCase.getId());
            if (testCaseFoldersPath != null) {
                return normalizeFilePath(testCaseFoldersPath) + "/";
            } else {
                return "";
            }
        }
    }

    /** Remove accents and replace illegal characters by '_'. */
    private String normalizeFilePath(String path) {
        return StringUtils.stripAccents(path).replaceAll(TEST_CASE_PATH_ILLEGAL_PATTERN, "_");
    }

    /**
     * Attempts to create the test file with the preferred name.
     *
     * @param testCase
     * @return
     * @throws IOException
     */
    private File createTestNominal(ScmRepository scm, TestCase testCase) throws IOException {
        String foldersPath = getFoldersPath(testCase);
        String fileRelativePath = buildTestCaseStandardRelativePath(foldersPath, testCase);
        return doCreateTestFile(scm, fileRelativePath);
    }

    /**
     * Attempts to create the test file with the backup name. Backup means : this is the filename we
     * may need in the Windows world.
     *
     * @param testCase
     * @return
     * @throws IOException
     */
    public File createTestBackup(ScmRepository scm, TestCase testCase) throws IOException {
        String foldersPath = getFoldersPath(testCase);
        String fileRelativePath = buildTestCaseBackUpRelativePath(foldersPath, testCase);
        return doCreateTestFile(scm, fileRelativePath);
    }

    /**
     * Creates a new file with the given filename in the working folder of the scm
     *
     * @param scm
     * @param filename
     * @return
     */
    private File doCreateTestFile(ScmRepository scm, String filename) throws IOException {
        File workfolder = scm.getWorkingFolder();
        File newFile = new File(workfolder, filename);
        if (newFile.exists()) {
            LOGGER.warn(
                    "retrieved physical file '{}' while in the file creation routine... it should have been detected earlier. This is an abnormal situation. "
                            + "Anyway, this file ",
                    newFile.getAbsolutePath());
        } else {
            tryCreateFolders(newFile.getParentFile());
            newFile.createNewFile();
            LOGGER.trace("new file created : '{}'", newFile.getAbsolutePath());
        }
        return newFile;
    }

    private void printToFile(File dest, TestCase testCase) throws IOException {
        Wrapped<String> content = new Wrapped<>();
        TestCaseVisitor visitor =
                new TestCaseVisitor() {
                    @Override
                    public void visit(TestCase testCase) {
                        throw new IllegalArgumentException("Cannot print STANDARD test case to file");
                    }

                    @Override
                    public void visit(KeywordTestCase keywordTestCase) {
                        content.setValue(
                                keywordTestCaseService.writeScriptFromTestCase(keywordTestCase.getId(), false));
                    }

                    @Override
                    public void visit(ScriptedTestCase scriptedTestCase) {
                        content.setValue(scriptedTestCase.getScript());
                    }

                    @Override
                    public void visit(ExploratoryTestCase exploratoryTestCase) {
                        throw new IllegalArgumentException("Cannot print EXPLORATORY test case to file");
                    }
                };

        testCase.accept(visitor);

        try {
            // try first with UTF-8
            FileUtils.write(dest, content.getValue(), Charset.forName("UTF-8"));
        } catch (UnsupportedCharsetException ex) {
            LOGGER.debug("Could not print to file", ex);
            // try again with default charset
            FileUtils.write(dest, content.getValue(), Charset.defaultCharset());
        }
    }

    /**
     * Try to create the folder and all the absent parent folders represented by the given abstract
     * pathname.
     *
     * @param folder The abstract folder to create
     * @throws IOException If the folder could not be created
     */
    private void tryCreateFolders(File folder) throws IOException {
        if (!folder.mkdirs()) {
            if (folder.isDirectory()) {
                LOGGER.trace("directory at path {} already exists.", folder.toString());
            } else {
                throw new IOException("directory could not be created at path " + folder.toString());
            }
        } else {
            LOGGER.trace("directory at path {} has been created.", folder.toString());
        }
    }

    /**
     * Try to move a File from a location to another, taking charge of renaming the file if needed. If
     * the target File path contains non-existent folders, try to create them.
     *
     * @param sourceFile The source file to move and/or rename
     * @param targetFile The target file
     * @return The moved File.
     * @throws IOException If an error occurred during the operation
     */
    private File tryMoveFile(File sourceFile, File targetFile) throws IOException {
        File targetFileParent = targetFile.getParentFile();
        tryCreateFolders(targetFileParent);
        if (sourceFile.renameTo(targetFile)) {
            LOGGER.trace("file with path {} has been renamed to {}", sourceFile, targetFile);
            return targetFile;
        } else {
            throw new IOException(
                    "file with path "
                            + sourceFile
                            + " could not be move/renamed to file with path "
                            + targetFile);
        }
    }

    private void updateTestCaseAutomatedScriptInformation(
            TestCase testCase, File testCaseFile, ScmRepositoryManifest manifest) {
        testCaseModificationService.changeSourceCodeRepository(
                testCase.getId(), manifest.getScm().getId());

        final Project project = entityManager.find(Project.class, testCase.getProject().getId());
        String featureOrTestCaseName = testCase.getName();

        AutomatedTestTechnology technology = null;
        if (testCase instanceof KeywordTestCase) {
            BddImplementationTechnology bddImplementationTechnology =
                    project.getBddImplementationTechnology();
            technology =
                    switch (bddImplementationTechnology) {
                        case ROBOT -> automatedTestTechnologyFinderService.findByName("Robot Framework");
                        case CUCUMBER_4 -> automatedTestTechnologyFinderService.findByName("Cucumber 4");
                        case CUCUMBER_5_PLUS -> automatedTestTechnologyFinderService.findByName("Cucumber 5+");
                    };
        } else if (testCase instanceof ScriptedTestCase) {
            featureOrTestCaseName = extractFeatureName((ScriptedTestCase) testCase);

            BddImplementationTechnology bddImplementationTechnology =
                    project.getBddImplementationTechnology();
            technology =
                    switch (bddImplementationTechnology) {
                        case CUCUMBER_4 -> automatedTestTechnologyFinderService.findByName("Cucumber 4");
                        case ROBOT, CUCUMBER_5_PLUS ->
                                automatedTestTechnologyFinderService.findByName("Cucumber 5+");
                    };
        }
        if (technology != null) {
            testCaseModificationService.changeAutomatedTestTechnology(
                    testCase.getId(), technology.getId());
        }

        String automatedTestReference =
                String.format(
                        "%s/%s/%s#%s",
                        manifest.getScm().getName(),
                        manifest.getScm().getWorkingFolderPath(),
                        manifest.getRelativePath(testCaseFile),
                        featureOrTestCaseName);

        testCaseModificationService.changeAutomatedTestReference(
                testCase.getId(), PathUtils.cleanMultipleSlashes(automatedTestReference));
    }

    private String extractFeatureName(ScriptedTestCase testCase) {
        try {
            GherkinDocument gherkinDocument = GherkinParser.parseDocument(testCase.getScript());
            return gherkinDocument.getFeature().getName();
        } catch (ScriptParsingException spe) {
            LOGGER.warn("Could not extract feature name because of parsing error.", spe);
            return testCase.getName();
        }
    }
}
