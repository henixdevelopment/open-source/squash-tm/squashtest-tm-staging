/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.bugtracker.knownissues.local;

import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_ISSUES_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.ISSUE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.SPRINT_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.TEST_PLAN;
import static org.squashtest.tm.jooq.domain.Tables.TEST_PLAN_ITEM;

import org.jooq.DSLContext;
import org.jooq.Record4;
import org.jooq.SelectHavingStep;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.service.bugtracker.knownissues.local.LocalKnownIssue;

@Repository
@Transactional(readOnly = true)
public class SprintReqVersionKnownIssueFinder
        extends BaseLocalKnownIssueFinder<Record4<Long, Long, String, String>> {

    public SprintReqVersionKnownIssueFinder(DSLContext dsl) {
        super(dsl);
    }

    @Override
    protected SelectHavingStep<Record4<Long, Long, String, String>> selectKnownIssues(
            long sprintReqVersionId) {
        return dsl.select(
                        PROJECT.PROJECT_ID,
                        ISSUE.BUGTRACKER_ID,
                        ISSUE.REMOTE_ISSUE_ID,
                        DSL.groupConcatDistinct(EXECUTION.EXECUTION_ID))
                .from(ISSUE)
                .innerJoin(EXECUTION_ISSUES_CLOSURE)
                .on(ISSUE.ISSUE_ID.eq(EXECUTION_ISSUES_CLOSURE.ISSUE_ID))
                .innerJoin(EXECUTION)
                .on(EXECUTION.EXECUTION_ID.eq(EXECUTION_ISSUES_CLOSURE.EXECUTION_ID))
                .innerJoin(TEST_PLAN_ITEM)
                .on(EXECUTION.TEST_PLAN_ITEM_ID.eq(TEST_PLAN_ITEM.TEST_PLAN_ITEM_ID))
                .innerJoin(TEST_PLAN)
                .on(TEST_PLAN_ITEM.TEST_PLAN_ID.eq(TEST_PLAN.TEST_PLAN_ID))
                .innerJoin(PROJECT)
                .on(TEST_PLAN.CL_ID.eq(PROJECT.CL_ID))
                .innerJoin(SPRINT_REQ_VERSION)
                .on(TEST_PLAN.TEST_PLAN_ID.eq(SPRINT_REQ_VERSION.TEST_PLAN_ID))
                .where(SPRINT_REQ_VERSION.SPRINT_REQ_VERSION_ID.eq(sprintReqVersionId))
                .and(ISSUE.BUGTRACKER_ID.eq(PROJECT.BUGTRACKER_ID))
                .groupBy(PROJECT.PROJECT_ID, ISSUE.BUGTRACKER_ID, ISSUE.REMOTE_ISSUE_ID);
    }

    @Override
    public int countKnownIssues(Long sprintReqVersionId) {
        return selectKnownIssues(sprintReqVersionId).fetch().size();
    }

    @Override
    protected LocalKnownIssue buildIssueFromRecord(Record4<Long, Long, String, String> record) {
        return new LocalKnownIssue(
                record.component1(),
                record.component2(),
                record.component3(),
                LocalKnownIssueFinderHelper.parseLongsAndSortDesc(record.component4()));
    }
}
