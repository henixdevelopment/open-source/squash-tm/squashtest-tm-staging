/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation;

import java.net.URL;
import java.util.Collection;
import java.util.List;
import org.squashtest.tm.domain.testautomation.TestAutomationProject;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.service.internal.display.dto.TestAutomationProjectDto;

public interface TestAutomationProjectFinderService {

    TestAutomationProject findProjectById(long projectId);

    /**
     * @param server - the name of the server
     * @return the list of projects available on the server
     */
    Collection<TestAutomationProject> listProjectsOnServer(TestAutomationServer server);

    /**
     * Will list {@link TestAutomationProject} available on the given {@link TestAutomationServer}.
     * Credentials use for connection to the server will be the ones saved at user level for the
     * current user and given server.
     *
     * @param server : the {@link TestAutomationServer} to get the {@link TestAutomationProject} of
     * @return : the collection of {@link TestAutomationProject} available on the given {@link
     *     TestAutomationServer} and accessible with the current user's stored credentials for the
     *     server.
     */
    Collection<TestAutomationProject> listProjectsOnServerForCurrentUser(TestAutomationServer server);

    /**
     * @param taProjectIds
     * @return <code>true</code> if the project have been executed, <code>false</code> otherwise
     */
    boolean haveExecutedTests(Collection<Long> taProjectIds);

    URL findProjectURL(TestAutomationProject project);

    List<TestAutomationProjectDto> findAllByTMProject(long tmProjectId);

    void setUniqueBddProject(long projectId);
}
