/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.bugtracker;

import gherkin.ast.GherkinDocument;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.jooq.tools.StringUtils;
import org.springframework.context.MessageSource;
import org.squashtest.tm.bugtracker.definition.context.ExecutionInfo;
import org.squashtest.tm.bugtracker.definition.context.ExecutionSubItemInfo;
import org.squashtest.tm.bugtracker.definition.context.ExploratorySessionNoteInfo;
import org.squashtest.tm.bugtracker.definition.context.KeywordExecutionStepInfo;
import org.squashtest.tm.bugtracker.definition.context.RemoteIssueContext;
import org.squashtest.tm.bugtracker.definition.context.ScriptedExecutionStepInfo;
import org.squashtest.tm.bugtracker.definition.context.StandardExecutionStepInfo;
import org.squashtest.tm.bugtracker.definition.context.TestCaseInfo;
import org.squashtest.tm.bugtracker.definition.context.formatter.DefaultRemoteIssueContextFormatter;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.execution.ConsumerForExploratoryExecution;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.execution.SessionNote;
import org.squashtest.tm.domain.script.GherkinParser;
import org.squashtest.tm.domain.testautomation.FailureDetail;
import org.squashtest.tm.domain.testcase.ConsumerForScriptedTestCaseVisitor;
import org.squashtest.tm.domain.testcase.ScriptedTestCase;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseKind;
import org.squashtest.tm.service.internal.testcase.scripted.gherkin.ScriptedExecutionModelGenerator;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;

public final class RemoteIssueContextHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(RemoteIssueContextHelper.class);

    private RemoteIssueContextHelper() {
        throw new IllegalStateException("Utility class");
    }

    public static RemoteIssueContext getRemoteIssueContext(
            Execution execution, String squashPublicUrl, MessageSource messageSource) {
        final RemoteIssueContext ctx =
                new RemoteIssueContext(
                        getExecutionStepsInfo(execution),
                        null,
                        getTestCaseInfo(execution.getReferencedTestCase()),
                        getExecutionInfo(execution, squashPublicUrl),
                        null);

        final String defaultDescription =
                DefaultRemoteIssueContextFormatter.getDefaultDescription(ctx, messageSource);
        return ctx.withDefaultDescription(defaultDescription);
    }

    public static RemoteIssueContext getRemoteIssueContext(
            ExecutionStep buggedStep, String squashPublicUrl, MessageSource messageSource) {
        return getRemoteIssueContextFromExecutionItem(
                squashPublicUrl, messageSource, buggedStep.getExecution(), buggedStep.getId());
    }

    public static RemoteIssueContext getRemoteIssueContext(
            SessionNote sessionNote, String squashPublicUrl, MessageSource messageSource) {
        return getRemoteIssueContextFromExecutionItem(
                squashPublicUrl, messageSource, sessionNote.getExecution(), sessionNote.getId());
    }

    public static RemoteIssueContext getRemoteIssueContext(
            String squashPublicUrl,
            FailureDetail failureDetail,
            Execution execution,
            MessageSource messageSource) {
        return getRemoteIssueContextFromFailureDetail(
                squashPublicUrl, messageSource, failureDetail, execution);
    }

    private static RemoteIssueContext getRemoteIssueContextFromExecutionItem(
            String squashPublicUrl, MessageSource messageSource, Execution parentExecution, Long itemId) {
        final RemoteIssueContext ctx =
                new RemoteIssueContext(
                        getExecutionStepsInfo(parentExecution),
                        itemId,
                        getTestCaseInfo(parentExecution.getReferencedTestCase()),
                        getExecutionInfo(parentExecution, squashPublicUrl),
                        null);

        final String defaultDescription =
                DefaultRemoteIssueContextFormatter.getDefaultDescription(ctx, messageSource);
        return ctx.withDefaultDescription(defaultDescription);
    }

    private static RemoteIssueContext getRemoteIssueContextFromFailureDetail(
            String squashPublicUrl,
            MessageSource messageSource,
            FailureDetail failureDetail,
            Execution execution) {
        final RemoteIssueContext ctx =
                new RemoteIssueContext(
                        getExecutionStepsInfo(execution),
                        null,
                        getTestCaseInfo(failureDetail.getReferencedTestCase()),
                        getExecutionInfo(execution, squashPublicUrl),
                        failureDetail.getMessage());

        final String defaultDescription =
                DefaultRemoteIssueContextFormatter.getDefaultDescription(ctx, messageSource);
        return ctx.withDefaultDescription(defaultDescription);
    }

    private static ExecutionInfo getExecutionInfo(Execution execution, String squashPublicUrl) {
        return new ExecutionInfo(
                execution.getId(),
                getExecutionUrl(execution, squashPublicUrl),
                HTMLCleanupUtils.htmlToText(getExecutionPrerequisite(execution)),
                getExecutionPrerequisite(execution));
    }

    private static String getExecutionPrerequisite(Execution execution) {
        TestCaseKind kind = TestCaseKind.fromTestCase(execution.getReferencedTestCase());

        if (kind.isScripted()) {
            // Using a string builder only because we cannot assign a string from the arrow function
            final StringBuilder stringBuilder = new StringBuilder();

            execution
                    .getReferencedTestCase()
                    .accept(
                            new ConsumerForScriptedTestCaseVisitor(
                                    scriptedTestCase -> {
                                        final GherkinDocument doc =
                                                GherkinParser.parseDocument(scriptedTestCase.getScript());
                                        final ScriptedExecutionModelGenerator.Model model =
                                                new ScriptedExecutionModelGenerator().buildModel(doc);
                                        stringBuilder.append(buildScriptedExecutionPrerequisite(model));
                                    }));

            return stringBuilder.toString();
        }

        return execution.getPrerequisite();
    }

    private static String getExecutionUrl(Execution execution, String squashPublicUrl) {
        final String url =
                squashPublicUrl.endsWith("/")
                        ? squashPublicUrl + "execution/" + execution.getId()
                        : squashPublicUrl + "/execution/" + execution.getId();

        try {
            return new URI(url).normalize().toString();
        } catch (URISyntaxException e) {
            final String errorMessage =
                    String.format("Cannot normalize candidate execution URL. Proceed with '%s'.", url);
            LOGGER.error(errorMessage, e);
            return url;
        }
    }

    private static TestCaseInfo getTestCaseInfo(TestCase referencedTestCase) {
        return new TestCaseInfo(
                referencedTestCase.getId(),
                referencedTestCase.getReference(),
                referencedTestCase.getName(),
                getTestCaseKind(referencedTestCase));
    }

    private static TestCaseInfo.Kind getTestCaseKind(TestCase referencedTestCase) {
        TestCaseKind testCaseKind = TestCaseKind.fromTestCase(referencedTestCase);
        return TestCaseInfo.Kind.valueOf(testCaseKind.name());
    }

    private static List<ExecutionSubItemInfo> getExecutionStepsInfo(Execution execution) {
        List<ExecutionSubItemInfo> executionStepInfos = new ArrayList<>();
        TestCaseKind testCaseKind = TestCaseKind.fromTestCase(execution.getReferencedTestCase());
        List<ExecutionStep> executionSteps = execution.getSteps();

        switch (testCaseKind) {
            case STANDARD -> {
                for (ExecutionStep step : executionSteps) {
                    addStandardStepToExecutionSubItemInfo(executionStepInfos, step);
                }
            }
            case GHERKIN ->
                    execution
                            .getReferencedTestCase()
                            .accept(
                                    new ConsumerForScriptedTestCaseVisitor(
                                            scriptedTestCase ->
                                                    addScriptedStepToExecutionSubItemInfo(
                                                            executionStepInfos, executionSteps, scriptedTestCase)));
            case KEYWORD -> {
                for (ExecutionStep step : executionSteps) {
                    addKeywordStepToExecutionSubItemInfo(executionStepInfos, step);
                }
            }
            case EXPLORATORY ->
                    execution.accept(
                            new ConsumerForExploratoryExecution(
                                    exploratoryExecution -> {
                                        for (SessionNote note : exploratoryExecution.getSessionNotes()) {
                                            addSessionNoteToExecutionStepInfo(executionStepInfos, note);
                                        }
                                    }));
            default ->
                    throw new IllegalArgumentException(
                            "The kind " + testCaseKind + " for a Test Case is not handled.");
        }

        return executionStepInfos;
    }

    private static void addStandardStepToExecutionSubItemInfo(
            List<ExecutionSubItemInfo> executionStepInfos, ExecutionStep step) {
        executionStepInfos.add(
                new StandardExecutionStepInfo(
                        step.getId(),
                        step.getExecutionStepOrder(),
                        HTMLCleanupUtils.htmlToText(step.getAction()),
                        step.getAction(),
                        HTMLCleanupUtils.htmlToText(step.getExpectedResult()),
                        step.getExpectedResult()));
    }

    private static void addScriptedStepToExecutionSubItemInfo(
            List<ExecutionSubItemInfo> executionStepInfos,
            List<ExecutionStep> executionSteps,
            ScriptedTestCase scriptedTestCase) {
        final GherkinDocument doc = GherkinParser.parseDocument(scriptedTestCase.getScript());
        final ScriptedExecutionModelGenerator.Model model =
                new ScriptedExecutionModelGenerator().buildModel(doc);

        for (int i = 0; i < executionSteps.size(); ++i) {
            final ExecutionStep step = executionSteps.get(i);
            final String script = buildExecutionStepScript(model.getSteps().get(i));
            final int executionStepOrder = step.getExecutionStepOrder();
            executionStepInfos.add(
                    new ScriptedExecutionStepInfo(step.getId(), executionStepOrder, script));
        }
    }

    private static void addKeywordStepToExecutionSubItemInfo(
            List<ExecutionSubItemInfo> executionStepInfos, ExecutionStep step) {
        executionStepInfos.add(
                new KeywordExecutionStepInfo(
                        step.getId(),
                        step.getExecutionStepOrder(),
                        step.getAction(),
                        HTMLCleanupUtils.htmlToText(step.getAction())));
    }

    private static void addSessionNoteToExecutionStepInfo(
            List<ExecutionSubItemInfo> executionStepInfos, SessionNote note) {
        executionStepInfos.add(
                new ExploratorySessionNoteInfo(
                        note.getId(),
                        HTMLCleanupUtils.htmlToText(note.getContent()),
                        note.getContent(),
                        ExploratorySessionNoteInfo.Kind.valueOf(note.getKind().name())));
    }

    private static String buildExecutionStepScript(
            ScriptedExecutionModelGenerator.ExecutionStep executionStep) {
        final String stepPrefix = "\t";
        final List<String> lines =
                executionStep.getSteps().stream()
                        .map(
                                step ->
                                        String.join(
                                                " ",
                                                step.getKeyword().trim(),
                                                ":",
                                                step.getText().trim(),
                                                buildArgument(step.getArgument(), stepPrefix)))
                        .map(str -> stepPrefix + str)
                        .collect(Collectors.toList());

        final List<String> firstLineElements =
                Stream.of(
                                executionStep.getKeyword(),
                                ":",
                                executionStep.getName(),
                                executionStep.getDescription())
                        .filter(Objects::nonNull)
                        .toList();

        final String first = String.join(" ", firstLineElements);
        lines.add(0, first);
        return String.join("\n", lines);
    }

    private static String buildArgument(
            ScriptedExecutionModelGenerator.ArgumentModel argument, String prefix) {
        if (argument == null) {
            return "";
        }

        return switch (argument.getKind()) {
            case DOCSTRING -> "\n" + prefix + argument.getValue();
            case DATATABLE ->
                    "\n" + prefix + String.join("\n" + prefix, prettyPrintDataTable(argument.getDataTable()));
        };
    }

    private static List<String> prettyPrintDataTable(
            ScriptedExecutionModelGenerator.DataTableModel dataTable) {
        Map<Integer, Integer> maxContentLengthByColumnIndex = new HashMap<>();

        for (List<String> row : dataTable.getCells()) {
            for (int c = 0; c < row.size(); ++c) {
                final int currentMax = maxContentLengthByColumnIndex.getOrDefault(c, 0);
                maxContentLengthByColumnIndex.put(c, Math.max(currentMax, row.get(c).length()));
            }
        }

        final List<String> processedRows = new ArrayList<>();

        for (List<String> row : dataTable.getCells()) {
            final List<String> processedCells = new ArrayList<>();

            for (int c = 0; c < row.size(); ++c) {
                processedCells.add(
                        StringUtils.rightPad(row.get(c), maxContentLengthByColumnIndex.get(c) + 1));
            }

            processedRows.add("| " + String.join("| ", processedCells));
        }

        return processedRows;
    }

    private static String buildScriptedExecutionPrerequisite(
            ScriptedExecutionModelGenerator.Model model) {
        if (model.getPrerequisite() == null) {
            return "";
        }

        final List<String> lines =
                model.getPrerequisite().stream()
                        .map(step -> String.join("", step.getKeyword(), step.getText()))
                        .toList();

        return String.join("\n", lines);
    }
}
