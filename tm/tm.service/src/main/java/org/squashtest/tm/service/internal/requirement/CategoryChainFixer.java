/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.requirement;

import org.squashtest.tm.domain.infolist.InfoList;
import org.squashtest.tm.domain.infolist.InfoListItem;
import org.squashtest.tm.domain.infolist.ListItemReference;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.requirement.RequirementLibraryNode;
import org.squashtest.tm.domain.requirement.RequirementLibraryNodeVisitor;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.exception.InconsistentInfoListItemException;

public final class CategoryChainFixer implements RequirementLibraryNodeVisitor {

    @Override
    public void visit(Requirement visited) {
        replaceInfoListReferences(visited);
    }

    @Override
    public void visit(RequirementFolder visited) {
        fix(visited);
    }

    public void fix(RequirementFolder folder) {
        for (RequirementLibraryNode node : folder.getContent()) {
            node.accept(this);
        }
    }

    public static void fix(Requirement req) {
        replaceInfoListReferences(req);
    }

    private static void replaceInfoListReferences(Requirement requirement) {
        InfoList projectCategories = requirement.getProject().getRequirementCategories();

        for (RequirementVersion version : requirement.getRequirementVersions()) {
            replaceInfoListReferences(version, projectCategories);
        }

        for (Requirement insider : requirement.getContent()) {
            replaceInfoListReferences(insider);
        }
    }

    /*
     * 12/04/16 : about Category and ListItemReferences :
     *
     * cannot use services
     * 	- infoListItemService.isCategoryConsistent
     *  - infoListItemService.findReference
     *
     * anymore because this would trigger here an autoflush / persist
     * before we have a chance to replace the ListItemReference by actual entities
     *
     * JThebault 10/11/2020 : That's why you should NEVER HAVE CREATED INVALID DOMAIN OBJECTS in the first place...
     * A domain object should always be correct or be a valid hibernate proxy that can be fully initialized by hibernate when necessary
     */
    private static void replaceInfoListReferences(
            RequirementVersion version, InfoList projectCategories) {

        RequirementVersion.PropertiesSetter ppt = version.getPropertySetter();

        InfoListItem category = version.getCategory();

        // if no category set -> set the default one
        if (category == null) {
            ppt.setCategory(projectCategories.getDefaultItem());
        } else {
            // validate the code
            if (!projectCategories.contains(category)) {
                throw new InconsistentInfoListItemException("category", category.getCode());
            }

            // in case the item used here is merely a reference we need to replace it with a persistent
            // instance
            if (category instanceof ListItemReference) {
                ppt.setCategory(projectCategories.getItem(category));
            }
        }
    }
}
