/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchexport.models;

import java.util.Comparator;

public final class ParameterModel {
    public static final Comparator<ParameterModel> COMPARATOR =
            new Comparator<ParameterModel>() {
                @Override
                public int compare(ParameterModel o1, ParameterModel o2) {
                    int comp1 = o1.getTcOwnerPath().compareTo(o2.getTcOwnerPath());
                    if (comp1 == 0) {
                        return o1.getName().compareTo(o2.getName());
                    } else {
                        return comp1;
                    }
                }
            };

    private String tcOwnerPath;
    private long tcOwnerId;
    private long id;
    private String name;
    private String description;

    public ParameterModel(long tcOwnerId, long id, String name, String description) {
        super();
        this.tcOwnerId = tcOwnerId;
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public String getTcOwnerPath() {
        return tcOwnerPath;
    }

    public void setTcOwnerPath(String tcOwnerPath) {
        this.tcOwnerPath = tcOwnerPath;
    }

    public long getTcOwnerId() {
        return tcOwnerId;
    }

    public void setTcOwnerId(long tcOwnerId) {
        this.tcOwnerId = tcOwnerId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
