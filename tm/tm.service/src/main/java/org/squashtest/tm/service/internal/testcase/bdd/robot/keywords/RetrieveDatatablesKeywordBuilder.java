/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase.bdd.robot.keywords;

import static org.apache.logging.log4j.util.Strings.isBlank;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.CREATE_DICTIONARY_BUILTIN_KEYWORD;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.CREATE_LIST_BUILTIN_KEYWORD;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.RETURN_RESERVED_WORD;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.assignment;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.dictionaryVariable;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.listVariable;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.RobotSyntaxHelpers.scalarVariable;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.DATATABLES_VARIABLE_NAME;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.FOUR_SPACES;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.RETRIEVE_DATATABLES_KEYWORD_NAME;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.formatDatatableName;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.formatDocumentationSettingLines;
import static org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.isTestCaseUsingDatatables;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.squashtest.tm.domain.testcase.KeywordTestCase;
import org.squashtest.tm.domain.testcase.KeywordTestStep;
import org.squashtest.tm.service.internal.testcase.bdd.TextGridFormatter;
import org.squashtest.tm.service.internal.testcase.bdd.robot.SectionBuilderHelpers.MultiLineStringBuilder;

public final class RetrieveDatatablesKeywordBuilder {
    private static final String DATATABLE_ROW_PARAM_FORMAT = "row_%s_%s";

    private static final List<String> RETRIEVE_DATATABLES_DOCUMENTATION_LINES =
            Arrays.asList(
                    "Retrieves Squash TM's datatables and stores them in a dictionary.",
                    "",
                    "For instance, 2 datatables have been defined in Squash TM,",
                    "the first one containing data:",
                    "| name | firstName |",
                    "| Bob  |   Smith   |",
                    "the second one containing data",
                    "| name  | firstName | age |",
                    "| Alice |   Smith   | 45  |",
                    "",
                    "First, for each datatable, this keyword retrieves the values of each row",
                    "and stores them in a list, as follows:",
                    "@{row_1_1} =    Create List    name    firstName",
                    "",
                    "Then, for each datatable, this keyword creates a list containing all the rows,",
                    "as lists themselves, as follows:",
                    "@{datatable_1} =    Create List    ${row_1_1}    ${row_1_2}",
                    "",
                    "Finally, this keyword stores the datatables into the &{datatables} dictionary",
                    "with each datatable name as key, and each datatable list as value :",
                    "&{datatables} =    Create Dictionary    datatable_1=${datatable_1}    datatable_2=${datatable_2}");

    private RetrieveDatatablesKeywordBuilder() {
        // Not meant to be instantiated
    }

    public static String buildRetrieveDatatables(KeywordTestCase keywordTestCase) {
        final boolean isDatatableEnabled = isTestCaseUsingDatatables(keywordTestCase);

        if (isDatatableEnabled) {
            final MultiLineStringBuilder stringBuilder = new MultiLineStringBuilder();

            final List<String> datatables = extractNonEmptyDatatables(keywordTestCase);

            return stringBuilder
                    .appendNewLine()
                    .appendLine(RETRIEVE_DATATABLES_KEYWORD_NAME)
                    .append(formatDocumentationSettingLines(RETRIEVE_DATATABLES_DOCUMENTATION_LINES))
                    .appendNewLine()
                    .append(formatDatatables(datatables))
                    .append(formatDatatablesDictionaryDeclaration(datatables))
                    .appendNewLine()
                    .appendLine(
                            FOUR_SPACES
                                    + RETURN_RESERVED_WORD
                                    + FOUR_SPACES
                                    + dictionaryVariable(DATATABLES_VARIABLE_NAME))
                    .toString();
        } else {
            return "";
        }
    }

    private static String formatDatatables(List<String> datatables) {
        final MultiLineStringBuilder multiLineStringBuilder = new MultiLineStringBuilder();

        for (int i = 0; i < datatables.size(); ++i) {
            final String datatable = datatables.get(i);
            final int tableNumber = i + 1;
            multiLineStringBuilder.append(buildDatatableRowsDefinition(datatable, tableNumber));
            multiLineStringBuilder.appendNewLine();
        }

        return multiLineStringBuilder.toString();
    }

    private static String buildDatatableRowsDefinition(String datatable, int tableNumber) {
        final TextGridFormatter rowsTextGridFormatter = new TextGridFormatter();

        // The final list assignment for all rows
        final List<String> tableVariableCells =
                new ArrayList<>(
                        List.of(
                                assignment(listVariable(formatDatatableName(tableNumber))),
                                CREATE_LIST_BUILTIN_KEYWORD));

        final List<List<String>> rows = extractRowsFromDataTable(datatable);
        int rowNumber = 1;

        for (List<String> row : rows) {
            final String datatableRowIdentifier =
                    String.format(DATATABLE_ROW_PARAM_FORMAT, tableNumber, rowNumber);

            tableVariableCells.add(scalarVariable(datatableRowIdentifier));

            List<String> rowVariables =
                    new ArrayList<>(
                            List.of(
                                    assignment(listVariable(datatableRowIdentifier)), CREATE_LIST_BUILTIN_KEYWORD));

            rowVariables.addAll(row);

            // @{row_1_2} =        Create List    Expresso    0.40
            rowsTextGridFormatter.addRow(rowVariables);

            rowNumber++;
        }

        //  @{datatable_2} =    Create List    ${row_2_1} ...
        final String formattedDatatableAssignment =
                new TextGridFormatter()
                        .addRow(tableVariableCells)
                        .format(TextGridFormatter.withRowPrefix(FOUR_SPACES));

        return new MultiLineStringBuilder()
                .append(rowsTextGridFormatter.format(TextGridFormatter.withRowPrefix(FOUR_SPACES)))
                .append(formattedDatatableAssignment)
                .toString();
    }

    private static List<String> extractNonEmptyDatatables(KeywordTestCase keywordTestCase) {
        return keywordTestCase.getSteps().stream()
                .map(step -> ((KeywordTestStep) step).getDatatable())
                .filter(datatable -> !isBlank(datatable))
                .toList();
    }

    private static List<List<String>> extractRowsFromDataTable(String datatableAsString) {
        String[] rowsAsString = datatableAsString.split("\n");
        List<List<String>> dataTable = new ArrayList<>(rowsAsString.length);

        for (String rowAsString : rowsAsString) {
            String[] rowParts = splitOnUnescapedPipes(rowAsString);
            List<String> dataRow =
                    Arrays.stream(rowParts)
                            .filter(part -> !isBlank(part))
                            .map(String::trim)
                            .map(RetrieveDatatablesKeywordBuilder::withUnescapedPipeCharacters)
                            .map(RetrieveDatatablesKeywordBuilder::withEscapedSpecialCharacters)
                            .toList();
            dataTable.add(dataRow);
        }

        return dataTable;
    }

    private static String withUnescapedPipeCharacters(String input) {
        final StringBuilder sb = new StringBuilder();

        int position = 0;

        while (position < input.length()) {
            final char c = input.charAt(position);
            final boolean nextIsPipe = position + 1 < input.length() && input.charAt(position + 1) == '|';

            if (c == '\\' && nextIsPipe) {
                sb.append('|');
                position += 2;
            } else {
                sb.append(c);
                position++;
            }
        }

        return sb.toString();
    }

    private static String withEscapedSpecialCharacters(String input) {
        final StringBuilder sb = new StringBuilder();

        for (int position = 0; position < input.length(); position++) {
            final char c = input.charAt(position);

            if (isSpecialCharacter(c)) {
                sb.append('\\');
            }

            sb.append(c);
        }

        return sb.toString();
    }

    private static boolean isSpecialCharacter(char c) {
        // Based on
        // https://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#rules-for-parsing-the-data
        // Pipes can be escaped in pipe-separated format. We're using space delimiters, so we treat
        // the pipe as a regular character
        return "$@&%#=\\".indexOf(c) >= 0;
    }

    private static String[] splitOnUnescapedPipes(String rowAsString) {
        return rowAsString.split("(?<!\\\\)\\|");
    }

    private static String formatDatatablesDictionaryDeclaration(List<String> datatables) {
        final List<String> cells = new ArrayList<>();

        cells.add(assignment(dictionaryVariable(DATATABLES_VARIABLE_NAME)));
        cells.add(CREATE_DICTIONARY_BUILTIN_KEYWORD);

        for (int i = 0; i < datatables.size(); ++i) {
            final int tableNumber = i + 1;
            final String datatableName = formatDatatableName(tableNumber);
            final String entry = datatableName + "=" + scalarVariable(datatableName);
            cells.add(entry);
        }

        final TextGridFormatter textGridFormatter = new TextGridFormatter();
        textGridFormatter.addRow(cells);
        return textGridFormatter.format(TextGridFormatter.withRowPrefix(FOUR_SPACES));
    }
}
