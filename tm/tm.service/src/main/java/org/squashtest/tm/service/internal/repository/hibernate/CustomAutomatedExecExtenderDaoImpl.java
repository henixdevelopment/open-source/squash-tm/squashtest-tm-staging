/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_EXECUTION_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_EXECUTION_FAILURE_DETAIL;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.FAILURE_DETAIL;
import static org.squashtest.tm.jooq.domain.Tables.ISSUE;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.service.internal.repository.CustomAutomatedExecExtenderDao;

@Repository
public class CustomAutomatedExecExtenderDaoImpl implements CustomAutomatedExecExtenderDao {

    private final DSLContext dslContext;

    public CustomAutomatedExecExtenderDaoImpl(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    @Override
    public String findExecutionStatusByExtenderId(long extenderId) {
        return dslContext
                .select(EXECUTION.EXECUTION_STATUS)
                .from(AUTOMATED_EXECUTION_EXTENDER)
                .join(EXECUTION)
                .on(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                .where(AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID.eq(extenderId))
                .fetchOneInto(String.class);
    }

    @Override
    public boolean areAllFailureDetailLinkedToIssueByExtenderId(long extenderId) {
        Integer unmatchedCount =
                dslContext
                        .selectCount()
                        .from(AUTOMATED_EXECUTION_FAILURE_DETAIL)
                        .join(FAILURE_DETAIL)
                        .on(
                                AUTOMATED_EXECUTION_FAILURE_DETAIL.FAILURE_DETAIL_ID.eq(
                                        FAILURE_DETAIL.FAILURE_DETAIL_ID))
                        .leftJoin(ISSUE)
                        .on(FAILURE_DETAIL.ISSUE_LIST_ID.eq(ISSUE.ISSUE_LIST_ID))
                        .where(AUTOMATED_EXECUTION_FAILURE_DETAIL.EXECUTION_EXTENDER_ID.eq(extenderId))
                        .and(ISSUE.ISSUE_ID.isNull())
                        .fetchOneInto(Integer.class);

        unmatchedCount = Objects.requireNonNullElse(unmatchedCount, 0);
        return unmatchedCount == 0;
    }

    @Override
    public boolean isAnyFailureDetailLinkedToIssueByExtenderId(long extenderId) {
        Integer linkedCount =
                dslContext
                        .selectCount()
                        .from(AUTOMATED_EXECUTION_FAILURE_DETAIL)
                        .join(FAILURE_DETAIL)
                        .on(
                                AUTOMATED_EXECUTION_FAILURE_DETAIL.FAILURE_DETAIL_ID.eq(
                                        FAILURE_DETAIL.FAILURE_DETAIL_ID))
                        .join(ISSUE)
                        .on(FAILURE_DETAIL.ISSUE_LIST_ID.eq(ISSUE.ISSUE_LIST_ID))
                        .where(AUTOMATED_EXECUTION_FAILURE_DETAIL.EXECUTION_EXTENDER_ID.eq(extenderId))
                        .fetchOneInto(Integer.class);

        linkedCount = Objects.requireNonNullElse(linkedCount, 0);
        return linkedCount > 0;
    }

    @Override
    public List<Long> findRecentExtenderIdsByExtenderId(long extenderId, int recentCount) {
        Long itemTestPlanId =
                dslContext
                        .select(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID)
                        .from(AUTOMATED_EXECUTION_EXTENDER)
                        .join(EXECUTION)
                        .on(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                        .join(ITEM_TEST_PLAN_EXECUTION)
                        .on(EXECUTION.EXECUTION_ID.eq(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID))
                        .where(AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID.eq(extenderId))
                        .fetchOneInto(Long.class);

        return dslContext
                .select(AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID)
                .from(ITEM_TEST_PLAN_EXECUTION)
                .join(EXECUTION)
                .on(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                .join(AUTOMATED_EXECUTION_EXTENDER)
                .on(EXECUTION.EXECUTION_ID.eq(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID))
                .where(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(itemTestPlanId))
                .orderBy(EXECUTION.LAST_EXECUTED_ON.desc())
                .limit(recentCount)
                .fetchInto(Long.class);
    }

    @Override
    public Map<Long, ExecutionStatus> findExecutionStatusesByExtenderIds(List<Long> extenderIds) {
        return dslContext
                .select(AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID, EXECUTION.EXECUTION_STATUS)
                .from(AUTOMATED_EXECUTION_EXTENDER)
                .join(EXECUTION)
                .on(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
                .where(AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID.in(extenderIds))
                .fetchMap(
                        record -> record.get(AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID),
                        record -> ExecutionStatus.valueOf(record.get(EXECUTION.EXECUTION_STATUS)));
    }

    @Override
    public Long findLastExtenderIdByExtenderId(long extenderId) {
        List<Long> recentExtenderIds = findRecentExtenderIdsByExtenderId(extenderId, 2);
        return (recentExtenderIds.size() >= 2) ? recentExtenderIds.get(1) : null;
    }

    @Override
    public List<Long> findExtenderIdsByIssueId(long issueId) {
        return dslContext
                .select(AUTOMATED_EXECUTION_FAILURE_DETAIL.EXECUTION_EXTENDER_ID)
                .from(AUTOMATED_EXECUTION_FAILURE_DETAIL)
                .join(FAILURE_DETAIL)
                .on(
                        AUTOMATED_EXECUTION_FAILURE_DETAIL.FAILURE_DETAIL_ID.eq(
                                FAILURE_DETAIL.FAILURE_DETAIL_ID))
                .join(ISSUE)
                .on(FAILURE_DETAIL.ISSUE_LIST_ID.eq(ISSUE.ISSUE_LIST_ID))
                .where(ISSUE.ISSUE_ID.eq(issueId))
                .fetchInto(Long.class);
    }

    @Override
    public Map<Long, Boolean> getSomeFailureDetailDoesNotHaveIssueByExtenderId(
            List<Long> extenderIds) {
        Map<Long, Integer> unmatchedCounts =
                dslContext
                        .select(
                                AUTOMATED_EXECUTION_FAILURE_DETAIL.EXECUTION_EXTENDER_ID,
                                DSL.count().as("unmatchedCount"))
                        .from(AUTOMATED_EXECUTION_FAILURE_DETAIL)
                        .join(FAILURE_DETAIL)
                        .on(
                                AUTOMATED_EXECUTION_FAILURE_DETAIL.FAILURE_DETAIL_ID.eq(
                                        FAILURE_DETAIL.FAILURE_DETAIL_ID))
                        .leftJoin(ISSUE)
                        .on(FAILURE_DETAIL.ISSUE_LIST_ID.eq(ISSUE.ISSUE_LIST_ID))
                        .where(AUTOMATED_EXECUTION_FAILURE_DETAIL.EXECUTION_EXTENDER_ID.in(extenderIds))
                        .and(ISSUE.ISSUE_ID.isNull())
                        .groupBy(AUTOMATED_EXECUTION_FAILURE_DETAIL.EXECUTION_EXTENDER_ID)
                        .fetchMap(
                                record -> record.get(AUTOMATED_EXECUTION_FAILURE_DETAIL.EXECUTION_EXTENDER_ID),
                                record -> record.get("unmatchedCount", Integer.class));

        return extenderIds.stream()
                .collect(
                        Collectors.toMap(
                                extenderId -> extenderId,
                                extenderId -> unmatchedCounts.getOrDefault(extenderId, 0) != 0));
    }
}
