/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.projectimporter.pivotimporter;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.CustomField;
import org.squashtest.tm.domain.projectimporter.PivotFormatImport;
import org.squashtest.tm.exception.pivotformatimport.CouldNotBindCustomFieldDuringImportException;
import org.squashtest.tm.service.customfield.CustomCustomFieldManagerService;
import org.squashtest.tm.service.customfield.CustomFieldBindingModificationService;
import org.squashtest.tm.service.customfield.CustomFieldFinderService;
import org.squashtest.tm.service.internal.dto.projectimporter.CustomFieldToImport;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportField;
import org.squashtest.tm.service.internal.dto.projectimporter.JsonImportFile;
import org.squashtest.tm.service.internal.dto.projectimporter.PivotImportMetadata;
import org.squashtest.tm.service.internal.dto.projectimporter.ProjectIdsReferences;
import org.squashtest.tm.service.internal.dto.projectimporter.SquashCustomFieldInfo;
import org.squashtest.tm.service.projectimporter.pivotimporter.CustomFieldPivotImporterService;
import org.squashtest.tm.service.projectimporter.pivotimporter.GlobalProjectPivotImporterService;
import org.squashtest.tm.service.projectimporter.pivotimporter.PivotJsonParsingHelper;
import org.squashtest.tm.service.projectimporter.pivotimporter.parsers.CustomFieldParser;

@Service("CustomFieldPivotImporterService")
public class CustomFieldPivotImporterServiceImpl implements CustomFieldPivotImporterService {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(CustomFieldPivotImporterServiceImpl.class);
    private final CustomCustomFieldManagerService customFieldManagerService;
    private final CustomFieldBindingModificationService customFieldBindingModificationService;
    private final CustomFieldFinderService customFieldFinderService;
    private final CustomFieldParser customFieldParser;

    @PersistenceContext private EntityManager entityManager;

    public CustomFieldPivotImporterServiceImpl(
            CustomCustomFieldManagerService customFieldManagerService,
            CustomFieldBindingModificationService customFieldBindingModificationService,
            CustomFieldFinderService customFieldFinderService,
            CustomFieldParser customFieldParser) {
        this.customFieldManagerService = customFieldManagerService;
        this.customFieldBindingModificationService = customFieldBindingModificationService;
        this.customFieldFinderService = customFieldFinderService;
        this.customFieldParser = customFieldParser;
    }

    @Override
    public void importCustomFieldsFromZipArchive(
            ZipFile zipFile,
            ProjectIdsReferences projectIdsReferences,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        PivotFormatLoggerHelper.logImportStartedForEntitiesKind(
                LOGGER, zipFile.getName(), PivotFormatLoggerHelper.CUSTOM_FIELDS, pivotFormatImport);
        ZipEntry entry = zipFile.getEntry(JsonImportFile.CUSTOM_FIELDS.getFileName());
        if (Objects.nonNull(entry)) {
            try (InputStream jsonInputStream = zipFile.getInputStream(entry)) {
                handleCustomFieldsInJsonFile(
                        jsonInputStream, projectIdsReferences, pivotImportMetadata, pivotFormatImport);
                PivotFormatLoggerHelper.logImportSuccessForEntitiesKind(
                        LOGGER, PivotFormatLoggerHelper.CUSTOM_FIELDS, pivotFormatImport);
            } catch (Exception e) {
                PivotFormatLoggerHelper.logImportFailureForEntitiesKind(
                        LOGGER, PivotFormatLoggerHelper.CUSTOM_FIELDS, pivotFormatImport);
                throw e;
            }
        }
    }

    private void handleCustomFieldsInJsonFile(
            InputStream jsonInputStream,
            ProjectIdsReferences projectIdsReferences,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        JsonFactory jsonFactory = new JsonFactory();
        try (JsonParser jsonParser = jsonFactory.createParser(jsonInputStream)) {
            while (jsonParser.nextToken() != null) {
                parseCustomFieldArray(
                        jsonParser, projectIdsReferences, pivotImportMetadata, pivotFormatImport);
            }
        }
    }

    private void parseCustomFieldArray(
            JsonParser jsonParser,
            ProjectIdsReferences projectIdsReferences,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport)
            throws IOException {
        List<CustomFieldToImport> customFieldsToImport = new ArrayList<>();
        if (!JsonImportField.CUSTOM_FIELDS.equals(jsonParser.getCurrentName())) {
            return;
        }

        while (PivotJsonParsingHelper.isNotTheEndOfParsedArray(jsonParser)) {
            if (PivotJsonParsingHelper.isStartingToParseNewObject(jsonParser)) {
                CustomFieldToImport customFieldToImport =
                        customFieldParser.parseCustomField(jsonParser, pivotFormatImport);

                addParsedCustomFieldToCustomFieldsToImport(
                        customFieldsToImport,
                        pivotImportMetadata,
                        projectIdsReferences,
                        pivotFormatImport,
                        customFieldToImport);
            }

            if (customFieldsToImport.size() == GlobalProjectPivotImporterService.BATCH_SIZE) {
                createCustomFields(
                        customFieldsToImport, projectIdsReferences, pivotImportMetadata, pivotFormatImport);
            }
        }

        if (!customFieldsToImport.isEmpty()) {
            createCustomFields(
                    customFieldsToImport, projectIdsReferences, pivotImportMetadata, pivotFormatImport);
        }
    }

    private void addParsedCustomFieldToCustomFieldsToImport(
            List<CustomFieldToImport> customFieldsToImport,
            PivotImportMetadata pivotImportMetadata,
            ProjectIdsReferences projectIdsReferences,
            PivotFormatImport pivotFormatImport,
            CustomFieldToImport customFieldToImport) {

        CustomField existingCustomField =
                customFieldFinderService.findByCodeAndInputType(
                        customFieldToImport.getCustomField().getCode(),
                        customFieldToImport.getCustomField().getInputType());

        if (Objects.nonNull(existingCustomField)) {
            handleExistingCustomField(
                    pivotImportMetadata,
                    projectIdsReferences,
                    customFieldToImport,
                    existingCustomField,
                    pivotFormatImport);
        } else {
            customFieldsToImport.add(customFieldToImport);
        }
    }

    private void handleExistingCustomField(
            PivotImportMetadata pivotImportMetadata,
            ProjectIdsReferences projectIdsReferences,
            CustomFieldToImport customFieldToImport,
            CustomField existingCustomField,
            PivotFormatImport pivotFormatImport) {
        logCustomFieldAlreadyExistsInSquash(customFieldToImport, pivotFormatImport);

        pivotImportMetadata
                .getCustomFieldIdsMap()
                .put(
                        customFieldToImport.getInternalId(),
                        new SquashCustomFieldInfo(
                                existingCustomField.getId(), existingCustomField.getInputType()));

        addCustomFieldBindingsToProject(
                existingCustomField,
                projectIdsReferences,
                customFieldToImport.getBoundEntities(),
                pivotFormatImport);
    }

    private static void logCustomFieldAlreadyExistsInSquash(
            CustomFieldToImport customFieldToImport, PivotFormatImport pivotFormatImport) {
        LOGGER.info(
                "CustomFieldPivotImporterService - Import id {} - Custom field \"{}\" with internal id {} already exists in Squash. Import will use the existing custom field for entity bindings",
                pivotFormatImport.getId(),
                customFieldToImport.getCustomField().getName(),
                customFieldToImport.getInternalId());
    }

    private void createCustomFields(
            List<CustomFieldToImport> customFieldsToImport,
            ProjectIdsReferences projectIdsReferences,
            PivotImportMetadata pivotImportMetadata,
            PivotFormatImport pivotFormatImport) {
        for (CustomFieldToImport customFieldToImport : customFieldsToImport) {
            PivotFormatLoggerHelper.logEntityCreationStarted(
                    LOGGER,
                    PivotFormatLoggerHelper.CUSTOM_FIELD,
                    customFieldToImport.getInternalId(),
                    pivotFormatImport);

            try {
                CustomField customField =
                        customFieldManagerService.persistUnsecured(customFieldToImport.getCustomField());
                addCustomFieldBindingsToProject(
                        customFieldToImport.getCustomField(),
                        projectIdsReferences,
                        customFieldToImport.getBoundEntities(),
                        pivotFormatImport);
                pivotImportMetadata
                        .getCustomFieldIdsMap()
                        .put(
                                customFieldToImport.getInternalId(),
                                new SquashCustomFieldInfo(customField.getId(), customField.getInputType()));
                PivotFormatLoggerHelper.logEntityCreatedSuccessfully(
                        LOGGER,
                        PivotFormatLoggerHelper.CUSTOM_FIELD,
                        customFieldToImport.getCustomField().getName(),
                        customFieldToImport.getInternalId(),
                        pivotFormatImport);
            } catch (Exception e) {
                PivotFormatLoggerHelper.handleEntityCreationFailed(
                        LOGGER,
                        PivotFormatLoggerHelper.CUSTOM_FIELD,
                        customFieldToImport.getCustomField().getName(),
                        customFieldToImport.getInternalId(),
                        pivotFormatImport,
                        e);
            }
        }

        customFieldsToImport.clear();
        entityManager.flush();
        entityManager.clear();
    }

    private void addCustomFieldBindingsToProject(
            CustomField customField,
            ProjectIdsReferences projectIdsReferences,
            List<BindableEntity> boundEntities,
            PivotFormatImport pivotFormatImport) {
        logAddCustomFieldBindings(customField, projectIdsReferences, pivotFormatImport);

        boundEntities.forEach(
                boundEntity -> {
                    try {
                        customFieldBindingModificationService.addNewCustomFieldBindingUnsecured(
                                projectIdsReferences.getId(), boundEntity, customField.getId());
                    } catch (Exception e) {
                        String message =
                                String.format(
                                        "Import Id: %s - Project id: %s - Failed to add custom field binding to entity \"%s\" for the custom field \"%s\"",
                                        pivotFormatImport.getId(),
                                        projectIdsReferences.getId(),
                                        boundEntity,
                                        customField.getName());
                        LOGGER.error(message);
                        throw new CouldNotBindCustomFieldDuringImportException(message, e);
                    }
                });
    }

    private static void logAddCustomFieldBindings(
            CustomField customField,
            ProjectIdsReferences projectIdsReferences,
            PivotFormatImport pivotFormatImport) {
        LOGGER.info(
                "Import Id: {} - Adding custom field bindings to project (id: {}) for the custom field \"{}\"",
                pivotFormatImport.getId(),
                projectIdsReferences.getId(),
                customField.getName());
    }
}
