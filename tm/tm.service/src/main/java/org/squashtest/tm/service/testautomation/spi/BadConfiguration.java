/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.spi;

import java.io.Serial;

public class BadConfiguration extends TestAutomationException {

    @Serial private static final long serialVersionUID = 1L;

    private static final String BAD_CONF = "testautomation.exceptions.badconfiguration";

    private final String badPropertyName;

    public BadConfiguration() {
        super();
        this.badPropertyName = null;
    }

    public BadConfiguration(String message, Throwable cause) {
        super(message, cause);
        this.badPropertyName = null;
    }

    public BadConfiguration(String message, Throwable cause, String badPropertyName) {
        super(message, cause);
        this.badPropertyName = badPropertyName;
    }

    public BadConfiguration(String message) {
        super(message);
        this.badPropertyName = null;
    }

    public BadConfiguration(Throwable cause) {
        super(cause);
        this.badPropertyName = null;
    }

    @Override
    public Object[] messageArgs() {
        return new Object[] {badPropertyName};
    }

    @Override
    public String getI18nKey() {
        return BAD_CONF;
    }
}
