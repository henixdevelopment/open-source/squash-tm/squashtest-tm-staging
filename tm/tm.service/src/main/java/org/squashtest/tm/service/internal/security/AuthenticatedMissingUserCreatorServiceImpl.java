/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.security;

import java.util.ArrayList;
import java.util.Collection;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;
import org.squashtest.tm.api.security.authentication.ExtraAccountInformationAuthentication;
import org.squashtest.tm.core.foundation.logger.Logger;
import org.squashtest.tm.core.foundation.logger.LoggerFactory;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.domain.users.UsersGroup;
import org.squashtest.tm.exception.user.LoginAlreadyExistsException;
import org.squashtest.tm.service.security.AuthenticatedMissingUserCreatorService;
import org.squashtest.tm.service.user.UserAdministrationService;

@Service
public class AuthenticatedMissingUserCreatorServiceImpl
        implements AuthenticatedMissingUserCreatorService {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(AuthenticatedMissingUserCreatorServiceImpl.class);

    @Inject private UserAdministrationService userAccountManager;

    @Inject private UserDetailsManager userDetailsManager;

    @Override
    public void createMissingUser(Authentication principal) {
        LOGGER.debug("Will try to create user from principal if it does not exist");

        try {
            userAccountManager.checkLoginAvailability(principal.getName());
        } catch (LoginAlreadyExistsException ex) {
            LOGGER.debug(
                    "Authenticated principal matches an existing User, no new user will be created.", ex);
            return;
        }

        LOGGER.info("Authenticated principal does not match any User, a new User will be created");
        createUserFromPrincipal(principal);
    }

    private void createUserFromPrincipal(Authentication principal) {
        try {
            // first create the user account (CORE_USER), otherwise the spring security account will not
            // work
            createUserAccount(principal);

            // create the spring security user (AUTH_USER)
            createSpringSecAccount(principal);

        } catch (LoginAlreadyExistsException e) {
            LOGGER.warn("Something went wrong while trying to create missing authenticated user", e);
        }
    }

    private void createUserAccount(Authentication principal) {
        String username = principal.getName().trim();

        LOGGER.debug("creating user {}", username);

        // create the user account (CORE_USER)
        User user = User.createFromLogin(username);

        // populate it if the Authentication implements ExtraAccountInformationAuthentication
        if (ExtraAccountInformationAuthentication.class.isAssignableFrom(principal.getClass())) {
            populateExtraAccountInformationAuthentication(principal, user);
        }

        userAccountManager.createUserWithoutCredentials(user, UsersGroup.USER);
    }

    private void populateExtraAccountInformationAuthentication(
            Authentication authentication, User newUser) {
        LOGGER.debug(
                "Extra account information were found in the principal : the user account will be populated with them");

        ExtraAccountInformationAuthentication extra =
                (ExtraAccountInformationAuthentication) authentication;

        newUser.setFirstName(extra.getFirstName());
        newUser.setEmail(extra.getEmail());

        //  [Issue #8102] : guard against empty lastnames
        String lastName = extra.getLastName();
        if (!StringUtils.isBlank(lastName)) {
            newUser.setLastName(extra.getLastName());
        }
    }

    private void createSpringSecAccount(Authentication principal) {
        String username = principal.getName().trim();
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        org.springframework.security.core.userdetails.User springUser =
                new org.springframework.security.core.userdetails.User(username, "", authorities);
        userDetailsManager.createUser(springUser);
    }
}
