/*
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.useraccount;

import static org.squashtest.tm.jooq.domain.Tables.API_TOKEN;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CREATED_BY;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.CREATED_ON;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXPIRY_DATE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.LAST_USAGE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PERMISSIONS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TOKEN_ID;

import java.util.Arrays;
import java.util.List;
import org.jooq.Field;
import org.jooq.SortField;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;

public class PersonalApiTokenGrid extends AbstractGrid {

    private final Long userId;

    public PersonalApiTokenGrid(Long userId) {
        this.userId = userId;
    }

    @Override
    protected List<GridColumn> getColumns() {
        return Arrays.asList(
                new GridColumn(DSL.field(TOKEN_ID)),
                new GridColumn(DSL.field(NAME)),
                new GridColumn(DSL.field(PERMISSIONS)),
                new GridColumn(DSL.field(CREATED_ON)),
                new GridColumn(DSL.field(CREATED_BY)),
                new GridColumn(DSL.field(LAST_USAGE)),
                new GridColumn(DSL.field(EXPIRY_DATE)));
    }

    @Override
    protected Table<?> getTable() {
        return DSL.select(
                        API_TOKEN.TOKEN_ID,
                        API_TOKEN.NAME,
                        API_TOKEN.PERMISSIONS,
                        API_TOKEN.CREATED_ON,
                        API_TOKEN.CREATED_BY,
                        API_TOKEN.LAST_USAGE,
                        API_TOKEN.EXPIRY_DATE)
                .from(API_TOKEN)
                .where(API_TOKEN.USER_ID.eq(userId))
                .asTable();
    }

    @Override
    protected Field<?> getIdentifier() {
        return DSL.field(API_TOKEN.TOKEN_ID);
    }

    @Override
    protected Field<?> getProjectIdentifier() {
        return null;
    }

    @Override
    protected SortField<?> getDefaultOrder() {
        return DSL.field(CREATED_ON).desc();
    }
}
